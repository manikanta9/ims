package com.clifton.investment.instrument.event;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventAction;

import java.math.BigDecimal;


/**
 * The <code>InvestmentSecurityEventType</code> class defines investment security event types.
 * For example, Stock Split, Dividend Payment, Factor Change, Coupon Payment, etc.
 *
 * @author vgomelsky
 */
@CacheByName
public class InvestmentSecurityEventType extends NamedEntity<Short> {

	public static final String CASH_COUPON_PAYMENT = "Cash Coupon Payment";
	public static final String CASH_DIVIDEND_PAYMENT = "Cash Dividend Payment";
	public static final String FACTOR_CHANGE = "Factor Change";
	public static final String SECURITY_MATURITY = "Security Maturity";
	public static final String STOCK_DIVIDEND_PAYMENT = "Stock Dividend Payment";
	public static final String STOCK_SPLIT = "Stock Split";
	public static final String STOCK_SPINOFF = "Stock Spinoff";
	public static final String SYMBOL_CHANGE = "Symbol Change";
	public static final String MERGER = "Merger";
	public static final String EXCHANGE = "Exchange";
	public static final String CASH_DIVIDEND_DRIP = "Cash Dividend (with DRIP)";
	public static final String DIVIDEND_SCRIP = "Scrip Dividend";
	public static final String RETURN_OF_CAPITAL = "Return of Capital";
	public static final String INTEREST_SHORTFALL_PAYMENT = "Interest Shortfall Payment";
	public static final String INTEREST_SHORTFALL_REIMBURSEMENT_PAYMENT = "Interest Shortfall Reimbursement Payment";

	// swap reset events: each leg has a separate event
	public static final String EQUITY_LEG_PAYMENT = "Equity Leg Payment";
	public static final String INTEREST_LEG_PAYMENT = "Interest Leg Payment";
	public static final String DIVIDEND_LEG_PAYMENT = "Dividend Leg Payment";
	public static final String PREMIUM_LEG_PAYMENT = "Premium Leg Payment";
	public static final String FLOATING_LEG_PAYMENT = "Floating Leg Payment";
	public static final String FIXED_LEG_PAYMENT = "Fixed Leg Payment";
	public static final String CREDIT_EVENT = "Credit Event";
	public static final String TENDER_OFFER = "Tender Offer";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Specifies whether values before and after event are the same: dividend, coupon payments.
	 * They're different for stock splits, factor changes, etc.
	 */
	private boolean beforeSameAsAfter;

	/**
	 * Specifies whether value fields are a percent of notional rather than the absolute amount (value of 1 is 100%).
	 * Cash Coupon payment is not a percent $5 per share vs Cash Dividend Payment is percent 5% of position.
	 */
	private boolean valuePercent;

	/**
	 * Specifies whether {@link InvestmentSecurityEvent#additionalSecurity} field can be populated.
	 * {@link #additionalSecurityCurrency} and {@link #newSecurityCreated} can optionally define additional rules.
	 */
	private boolean additionalSecurityAllowed;
	/**
	 * Specifies whether additionalSecurity field on InvestmentSecurityEvent is a currency.
	 * For example, it can be used to specify Dividend Currency for a Dividend Payment event.
	 */
	private boolean additionalSecurityCurrency;
	/**
	 * When Additional Security is Currency, specifies if the Before and After Value are in the Event currency units or security currency units.
	 * For example, "Cash Dividend Payment" event type will have this field set to true, meaning the dividend per share amount (Before/After Value)
	 * is in Additional Security units (Event currency) and the exchange rate is from Event currency to Client Account base currency.
	 */
	private boolean eventValueInEventCurrencyUnits;
	/**
	 * Some events create a new security: stock spin offs or symbol changes
	 */
	private boolean newSecurityCreated;

	/**
	 * Specifies whether a new position is created and an existing position is closed: Stock Split.
	 * Bond factor change will have this as false as it does not create a new position but rather closes a portion of existing position.
	 */
	private boolean newPositionCreated;
	/**
	 * Specifies whether events of this type allow payment delay (for example Cash Dividend Payment can be delayed: accrue on Ex Date and reverse accrual and pay on Event Date).
	 * Some event types (Stock Split, Stock Spinoff, Security Maturity) should never allow this delay. Ex Date is usually the day after Event Date.
	 */
	private boolean paymentDelayAllowed;
	/**
	 * The number of decimal places allowed for values
	 */
	private int decimalPrecision;
	private BigDecimal minValue;
	private BigDecimal maxValue;
	/**
	 * If a security has multiple events on the same ExDate then need to use an order of precedence to determine which to process first.
	 * This is important specifically if a coupon payment and a factor payment happens since the factor reduced principal and the coupon is interest on the principal.
	 */
	private int eventOrder;
	/**
	 * Only one event of this type for a given event date.
	 */
	private boolean onePerEventDate;
	/**
	 * Specifies if events are allowed before security start date and if so, should there only be one
	 */
	private EventBeforeStartTypes eventBeforeStartType;

	/**
	 * Ability to exclude the description as a part of the natural key look up for event copy
	 * By default we uniquely identify events by event date, type, and description.
	 */
	private boolean descriptionIncludedInNaturalKey;

	/**
	 * Specifies whether {@link InvestmentSecurityEventAction}(s) are allowed for this event type.
	 */
	private boolean actionAllowed;

	/**
	 * Indicates whether events of this type have payouts.  Most events (all of those processed by the system) have payouts.
	 * However, some events (changes to domicile, call for shareholder approval) do not have any payout associated with them,
	 * but still offer valuable information to the holder.
	 */
	private boolean withoutPayout;
	/**
	 * Indicates whether events of this type always have a single payout. When the value is true, then all event information is
	 * located in {@link InvestmentSecurityEvent} table/object.  When it's false, then {@link com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout}
	 * will provide additional details about elections and payouts.
	 */
	private boolean singlePayoutOnly;

	/**
	 * Identifies event types that do not go through a regular lifecycle enabled by {@link InvestmentSecurityEventStatus}.
	 * For example, Cash Coupon Payments, Swap Resets, Credit Events, etc. These events are considered final and would have "Approved" status.
	 * Equity corporate actions data is usually supplied by external Corporate Actions Data provider (such as Markit) and have this
	 * field set to null indicating that the status is provided by the data provider.
	 */
	private InvestmentSecurityEventStatus forceEventStatus;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentSecurityEventType ofTypeName(String typeName) {
		InvestmentSecurityEventType result = new InvestmentSecurityEventType();
		result.setName(typeName);
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isBeforeEventAllowed() {
		return EventBeforeStartTypes.NONE != getEventBeforeStartType();
	}


	public boolean isBeforeSameAsAfter() {
		return this.beforeSameAsAfter;
	}


	public void setBeforeSameAsAfter(boolean beforeSameAsAfter) {
		this.beforeSameAsAfter = beforeSameAsAfter;
	}


	public boolean isNewSecurityCreated() {
		return this.newSecurityCreated;
	}


	public void setNewSecurityCreated(boolean newSecurityCreated) {
		this.newSecurityCreated = newSecurityCreated;
	}


	public int getDecimalPrecision() {
		return this.decimalPrecision;
	}


	public void setDecimalPrecision(int decimalPrecision) {
		this.decimalPrecision = decimalPrecision;
	}


	public BigDecimal getMinValue() {
		return this.minValue;
	}


	public void setMinValue(BigDecimal minValue) {
		this.minValue = minValue;
	}


	public BigDecimal getMaxValue() {
		return this.maxValue;
	}


	public void setMaxValue(BigDecimal maxValue) {
		this.maxValue = maxValue;
	}


	public boolean isValuePercent() {
		return this.valuePercent;
	}


	public void setValuePercent(boolean valuePercent) {
		this.valuePercent = valuePercent;
	}


	public boolean isNewPositionCreated() {
		return this.newPositionCreated;
	}


	public void setNewPositionCreated(boolean newPositionCreated) {
		this.newPositionCreated = newPositionCreated;
	}


	public boolean isPaymentDelayAllowed() {
		return this.paymentDelayAllowed;
	}


	public void setPaymentDelayAllowed(boolean paymentDelayAllowed) {
		this.paymentDelayAllowed = paymentDelayAllowed;
	}


	public int getEventOrder() {
		return this.eventOrder;
	}


	public void setEventOrder(int eventOrder) {
		this.eventOrder = eventOrder;
	}


	public boolean isOnePerEventDate() {
		return this.onePerEventDate;
	}


	public void setOnePerEventDate(boolean onePerEventDate) {
		this.onePerEventDate = onePerEventDate;
	}


	public EventBeforeStartTypes getEventBeforeStartType() {
		return this.eventBeforeStartType;
	}


	public void setEventBeforeStartType(EventBeforeStartTypes eventBeforeStartType) {
		this.eventBeforeStartType = eventBeforeStartType;
	}


	public boolean isAdditionalSecurityAllowed() {
		return this.additionalSecurityAllowed;
	}


	public void setAdditionalSecurityAllowed(boolean additionalSecurityAllowed) {
		this.additionalSecurityAllowed = additionalSecurityAllowed;
	}


	public boolean isAdditionalSecurityCurrency() {
		return this.additionalSecurityCurrency;
	}


	public void setAdditionalSecurityCurrency(boolean additionalSecurityCurrency) {
		this.additionalSecurityCurrency = additionalSecurityCurrency;
	}


	public boolean isEventValueInEventCurrencyUnits() {
		return this.eventValueInEventCurrencyUnits;
	}


	public void setEventValueInEventCurrencyUnits(boolean eventValueInEventCurrencyUnits) {
		this.eventValueInEventCurrencyUnits = eventValueInEventCurrencyUnits;
	}


	public boolean isDescriptionIncludedInNaturalKey() {
		return this.descriptionIncludedInNaturalKey;
	}


	public void setDescriptionIncludedInNaturalKey(boolean descriptionIncludedInNaturalKey) {
		this.descriptionIncludedInNaturalKey = descriptionIncludedInNaturalKey;
	}


	public boolean isActionAllowed() {
		return this.actionAllowed;
	}


	public void setActionAllowed(boolean actionAllowed) {
		this.actionAllowed = actionAllowed;
	}


	public boolean isWithoutPayout() {
		return this.withoutPayout;
	}


	public void setWithoutPayout(boolean withoutPayout) {
		this.withoutPayout = withoutPayout;
	}


	public boolean isSinglePayoutOnly() {
		return this.singlePayoutOnly;
	}


	public void setSinglePayoutOnly(boolean singlePayoutOnly) {
		this.singlePayoutOnly = singlePayoutOnly;
	}


	public InvestmentSecurityEventStatus getForceEventStatus() {
		return this.forceEventStatus;
	}


	public void setForceEventStatus(InvestmentSecurityEventStatus forceEventStatus) {
		this.forceEventStatus = forceEventStatus;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Classifies for an event type where events before start date are allow and if only one or many.
	 * Currently used for Credit Events (ONE) where we need the last factor prior to the security start date for CDS
	 * Comparison is Event Date vs. InvestmentSecurity.StartDate
	 */
	public enum EventBeforeStartTypes {

		NONE, // Do not allow any before start date
		ONE, // Allow only one - last one before start
		MANY // Allow any amount before start
	}
}
