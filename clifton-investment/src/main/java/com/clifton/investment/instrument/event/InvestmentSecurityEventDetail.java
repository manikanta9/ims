package com.clifton.investment.instrument.event;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentSecurityEventDetail</code> class defines details for security events that may have their
 * parameters change during event period.  For example, Total Return Swap resets could be paid monthly: one event
 * per month.  But accrual rate can change every day or every week.  Spread could also change.
 * <p/>
 * This information is necessary in order to calculate accruals as well as corresponding payments. It can either
 * be populated automatically by a batch job from corresponding market data or manually by the user.
 * <p/>
 * NOTE: referenceRate and spread can be null when they're no known for future dates but when the effectiveDate is known.
 * They will usually be populated by a batch job when the rate becomes known.
 * Accrual calculators should ignore records with null value.
 *
 * @author vgomelsky
 */
public class InvestmentSecurityEventDetail extends BaseEntity<Integer> {

	private InvestmentSecurityEvent event;

	/**
	 * The date when new rate/spread become effective. Prior to this date, previous rate/spread are used.
	 */
	private Date effectiveDate;

	/**
	 * Reference rate in %.  For example, 5 means 5%.
	 */
	private BigDecimal referenceRate;
	/**
	 * Spread in BPS for example, 5 means 0.05%.
	 */
	private BigDecimal spread;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sums and returns Reference Rate + Spread (the spread is normalized by dividing by 100)
	 */
	public BigDecimal getEffectiveRate() {
		return MathUtils.add(this.referenceRate, this.spread == null ? null : MathUtils.divide(this.spread, 100));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEvent getEvent() {
		return this.event;
	}


	public void setEvent(InvestmentSecurityEvent event) {
		this.event = event;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public BigDecimal getReferenceRate() {
		return this.referenceRate;
	}


	public void setReferenceRate(BigDecimal referenceRate) {
		this.referenceRate = referenceRate;
	}


	public BigDecimal getSpread() {
		return this.spread;
	}


	public void setSpread(BigDecimal spread) {
		this.spread = spread;
	}
}
