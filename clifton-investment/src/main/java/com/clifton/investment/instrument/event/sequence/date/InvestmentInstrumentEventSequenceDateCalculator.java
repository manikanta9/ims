package com.clifton.investment.instrument.event.sequence.date;


import com.clifton.calendar.schedule.api.Schedule;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.sequence.EventSequenceDefinition;

import java.util.Date;


/**
 * The <code>InvestmentInstrumentEventSequenceDateCalculator</code> calculates the exDate, recordDate, eventDate and additionalDate for
 * the provided event and sequence definition.
 *
 * @author mwacker
 */
public interface InvestmentInstrumentEventSequenceDateCalculator {

	public InvestmentSecurityEvent calculateDates(Date declareDate, Date nextDeclareDate, InvestmentSecurityEvent event, EventSequenceDefinition sequenceDefinition, Schedule schedule);
}
