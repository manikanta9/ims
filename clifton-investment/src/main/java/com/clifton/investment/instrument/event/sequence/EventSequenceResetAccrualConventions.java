package com.clifton.investment.instrument.event.sequence;


/**
 * The <code>EventSequenceResetAccrualConventions</code> defines the types of reset accruals
 * for event sequences.
 *
 * @author mwacker
 */
public enum EventSequenceResetAccrualConventions {
	DEFAULT,
	/**
	 * Interest accrues from prior month end to current month end.
	 */
	INTEREST_ACCRUES_ON_MONTH_END,
	/**
	 * Interest accrues from prior valuation date to current valuation date.
	 */
	INTEREST_ACCRUES_ON_VALUATION_DATE
}
