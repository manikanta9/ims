package com.clifton.investment.instrument.event;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentSecurityEvent</code> class represents an instance of a specific event type for a specific security.
 * For example, specific stock split, dividend payment, coupon payment, factor change, security maturity, etc.
 * <p/>
 * Industry standard term for equity events is "corporate action". We use more generic name because our events apply to wider range or securities/actions.
 *
 * @author vgomelsky
 */
public class InvestmentSecurityEvent extends BaseEntity<Integer> implements LabeledObject {

	private InvestmentSecurityEventType type;
	private InvestmentSecurityEventStatus status;
	private InvestmentSecurity security;

	/**
	 * A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events.
	 * Markit uses 16 digit identifiers.
	 */
	private Long corporateActionIdentifier;
	/**
	 * Stock split: before = shares before split, after = shares after split
	 * Factor change: before = factor before change, after = factor after change
	 * Dividend: both values are the same
	 */
	private BigDecimal beforeEventValue;
	private BigDecimal afterEventValue;
	/**
	 * Some events may require additional value (LossPercent for a bond Factor Change event)
	 */
	private BigDecimal additionalEventValue;
	/**
	 * Some events may optionally create a new security: stock spin offs and symbol changes.
	 * Also, a security maybe necessary to specify payment currency for events: Dividend Payment (some pay in foreign currencies)
	 */
	private InvestmentSecurity additionalSecurity;

	/**
	 * The date when this event and all related information is announced.
	 */
	private Date declareDate;
	/**
	 * Prior to this date (usually 2 business date before recordDate) the seller of the security is entitled to payment (dividend, etc.)
	 * The party that has position (based on trade date) one day before the Ex Date, receives the payment.
	 */
	private Date exDate;
	/**
	 * Parties holding positions (based on settlement date) in security at the end of this date will be effected by the event.
	 * Ex Date instead of Record Date must be used to determine whether one gets the payment.
	 */
	private Date recordDate;
	/**
	 * Actual date when the event takes place: dividend is payed out (payment date), split occurs (change in shares), etc.
	 */
	private Date eventDate;

	/**
	 * Some events may require additional date (Reset Date for Equity/Interest Leg Payment for Total Return Swaps)
	 */
	private Date additionalDate;

	/**
	 * Actual settlement date, differs from contractual settlement date.
	 */
	private Date actualSettlementDate;

	/**
	 * Short description of the event
	 */
	private String eventDescription;

	/**
	 * Optionally overrides the booking order specified on accounting event journal types.
	 * When more than one event occurs on the same date, the booking order determines which event needs to be booked first
	 * because there maybe dependencies between events. In rare cases when the order of events should be different than the default,
	 * this field should be used to override the order.
	 */
	private Short bookingOrderOverride;

	/**
	 * Specifies whether the holder of the security has an option to choose to participate or not participate in this event.
	 */
	private boolean voluntary;
	/**
	 * Specifies whether this event is likely subject to additional taxes affecting net payout amounts and booking rules.
	 */
	private boolean taxable;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @throws FieldValidationException if event information is incomplete: one of fields required for event journal generation is missing.
	 */
	public void validateRequiredFields() {
		String label = getLabel();
		ValidationUtils.assertNotNull(getBeforeEventValue(), "Before Event Value field value is required for events in current status: " + label, "beforeEventValue");
		ValidationUtils.assertNotNull(getAfterEventValue(), "After Event Value field value is required for events in current status: " + label, "afterEventValue");
		ValidationUtils.assertNotNull(getExDate(), "Ex Date field value is required for events in current status: " + label, "exDate");
		ValidationUtils.assertNotNull(getRecordDate(), "Record Date field value is required for events in current status: " + label, "recordDate");
		ValidationUtils.assertNotNull(getEventDate(), "Event Date field value is required for events in current status: " + label, "eventDate");
	}


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder(getLabelShort());
		result.append(" for ");
		result.append(this.security == null ? null : this.security.getSymbol());
		if (InvestmentSecurityEventType.TENDER_OFFER.equals(getType().getName())) {
			result.append(" expiring ");
		}
		result.append(" on ");
		result.append(DateUtils.fromDateShort(this.eventDate));
		return result.toString();
	}


	public String getLabelShort() {
		if (this.type != null && InvestmentSecurityEventType.SECURITY_MATURITY.equals(this.type.getName())) {
			return InvestmentSecurityEventType.SECURITY_MATURITY;
		}

		StringBuilder result = new StringBuilder(100);
		boolean beforeSameAsAfter = false;
		int precision = 4;
		if (this.type != null) {
			beforeSameAsAfter = this.type.isBeforeSameAsAfter();
			precision = this.type.getDecimalPrecision();
		}
		if (!InvestmentSecurityEventType.TENDER_OFFER.equals(getType().getName())) {
			result.append(CoreMathUtils.formatNumberDecimal(MathUtils.round(this.beforeEventValue, precision)));
		}
		if (this.type != null && this.type.isValuePercent()) {
			result.append("% for ");
			result.append(DateUtils.getDaysDifference(getAccrualEndDate(), getAccrualStartDate())); // End Of Day (exclude start date and include end date)
			result.append(" days");
		}
		if (!beforeSameAsAfter) {
			result.append(" to ");
			result.append(CoreMathUtils.formatNumberDecimal(MathUtils.round(this.afterEventValue, precision)));
			if (this.type != null && this.type.isValuePercent()) {
				result.append("%");
			}
		}
		result.append(' ');
		// check if paying in different currency than currency denomination of security
		if (this.type != null && (this.type.isAdditionalSecurityCurrency() || this.type.isNewPositionCreated()) && this.additionalSecurity != null) {
			if (this.security != null) {
				InvestmentSecurity currencyDenomination = this.security.getInstrument().getTradingCurrency();
				if (!this.additionalSecurity.equals(currencyDenomination)) {
					result.append(this.additionalSecurity.getSymbol());
					result.append(' ');
				}
			}
		}
		result.append(this.type == null ? null : this.type.getName());
		return result.toString();
	}


	/**
	 * Events usually settle in Currency Denomination of event's security.
	 * However, if event type allows settlement in a different currency and it is specified, then that currency will be returned.
	 */
	public InvestmentSecurity getSettlementCurrency() {
		InvestmentSecurityEventType eventType = getType();
		if (eventType != null && eventType.isAdditionalSecurityAllowed() && eventType.isAdditionalSecurityCurrency() && getAdditionalSecurity() != null) {
			return getAdditionalSecurity();
		}
		if (getSecurity() != null && getSecurity().getInstrument() != null) {
			return getSecurity().getInstrument().getTradingCurrency();
		}
		return null;
	}


	/**
	 * Event values are usually specified in Currency Denomination of security (event when additional security is specified on the event).
	 * However, for some events (Cash Dividend Payment), values and corresponding payment can be represented as additional security.
	 */
	public InvestmentSecurity getValuationCurrency() {
		InvestmentSecurityEventType eventType = getType();
		if (eventType != null && eventType.isAdditionalSecurityAllowed() && eventType.isAdditionalSecurityCurrency() && eventType.isEventValueInEventCurrencyUnits() && getAdditionalSecurity() != null) {
			return getAdditionalSecurity();
		}
		if (getSecurity() != null && getSecurity().getInstrument() != null) {
			return getSecurity().getInstrument().getTradingCurrency();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventType getType() {
		return this.type;
	}


	public void setType(InvestmentSecurityEventType type) {
		this.type = type;
	}


	public InvestmentSecurityEventStatus getStatus() {
		return this.status;
	}


	public void setStatus(InvestmentSecurityEventStatus status) {
		this.status = status;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public Long getCorporateActionIdentifier() {
		return this.corporateActionIdentifier;
	}


	public void setCorporateActionIdentifier(Long corporateActionIdentifier) {
		this.corporateActionIdentifier = corporateActionIdentifier;
	}


	public Date getDeclareDate() {
		return this.declareDate;
	}


	public void setDeclareDate(Date declareDate) {
		this.declareDate = declareDate;
	}


	public Date getExDate() {
		return this.exDate;
	}


	public void setExDate(Date exDate) {
		this.exDate = exDate;
	}


	public Date getRecordDate() {
		return this.recordDate;
	}


	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public BigDecimal getBeforeEventValue() {
		return this.beforeEventValue;
	}


	public void setBeforeEventValue(BigDecimal beforeEventValue) {
		this.beforeEventValue = beforeEventValue;
	}


	/**
	 * Sets both before and after event values to the same value specified.
	 */
	public void setBeforeAndAfterEventValue(BigDecimal eventValue) {
		setBeforeEventValue(eventValue);
		setAfterEventValue(eventValue);
	}


	public BigDecimal getAfterEventValue() {
		return this.afterEventValue;
	}


	public void setAfterEventValue(BigDecimal afterEventValue) {
		this.afterEventValue = afterEventValue;
	}


	public InvestmentSecurity getAdditionalSecurity() {
		return this.additionalSecurity;
	}


	public void setAdditionalSecurity(InvestmentSecurity additionalSecurity) {
		this.additionalSecurity = additionalSecurity;
	}


	public String getEventDescription() {
		return this.eventDescription;
	}


	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////  Bond Specific Helper Methods  //////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the Declare Date.  Accrual Start Date uses "End Of Day" industry convention.
	 * Accrued Interest is NOT included on the Start Date: it starts accruing the day after.
	 */
	public Date getAccrualStartDate() {
		return getDeclareDate();
	}


	public void setAccrualStartDate(Date startDate) {
		setDeclareDate(startDate);
	}


	/**
	 * Returns the Record Date.  Accrual End Date uses "End Of Day" industry convention.
	 * Accrued interest IS included on the End Date: ends accrual on End Date inclusive of End Date.
	 */
	public Date getAccrualEndDate() {
		return getRecordDate();
	}


	public void setAccrualEndDate(Date endDate) {
		setRecordDate(endDate);
		if (this.exDate == null) {
			setExDate(DateUtils.addDays(endDate, 1));
		}
	}


	public Date getPaymentDate() {
		return getEventDate();
	}


	public void setPaymentDate(Date paymentDate) {
		setEventDate(paymentDate);
	}


	/**
	 * Returns the day before Ex Date. The event applies to each position open at the end of this date (one day before Ex).
	 */
	public Date getDayBeforeExDate() {
		if (getExDate() == null) {
			return null;
		}
		return DateUtils.addDays(getExDate(), -1);
	}


	/**
	 * Returns true if event payment is delayed: need to accrue starting the day before Ex Date and then reverse on Payment Date.
	 */
	public boolean isPaymentDelayed() {
		if (getType() != null && getType().isPaymentDelayAllowed()) {
			return (DateUtils.compare(getDayBeforeExDate(), getPaymentDate(), false) < 0);
		}
		return false;
	}


	public BigDecimal getAdditionalEventValue() {
		return this.additionalEventValue;
	}


	public void setAdditionalEventValue(BigDecimal additionalEventValue) {
		this.additionalEventValue = additionalEventValue;
	}


	public Date getAdditionalDate() {
		return this.additionalDate;
	}


	public void setAdditionalDate(Date additionalDate) {
		this.additionalDate = additionalDate;
	}


	public Date getActualSettlementDate() {
		return this.actualSettlementDate;
	}


	public void setActualSettlementDate(Date actualSettlementDate) {
		this.actualSettlementDate = actualSettlementDate;
	}


	public Short getBookingOrderOverride() {
		return this.bookingOrderOverride;
	}


	public void setBookingOrderOverride(Short bookingOrderOverride) {
		this.bookingOrderOverride = bookingOrderOverride;
	}


	public boolean isVoluntary() {
		return this.voluntary;
	}


	public void setVoluntary(boolean voluntary) {
		this.voluntary = voluntary;
	}


	public boolean isTaxable() {
		return this.taxable;
	}


	public void setTaxable(boolean taxable) {
		this.taxable = taxable;
	}
}
