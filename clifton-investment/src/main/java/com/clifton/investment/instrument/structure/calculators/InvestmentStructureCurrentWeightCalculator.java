package com.clifton.investment.instrument.structure.calculators;


import com.clifton.investment.instrument.structure.InvestmentInstrumentStructure;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureWeight;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentInstrumentStructureWeightCalculator</code> ...
 *
 * @author manderson
 */
public interface InvestmentStructureCurrentWeightCalculator {

	/**
	 * Not always necessary, but some calculators need to validate specific data - i.e. any calculator that uses sectors should require
	 * sectors are selected for each instrument weight
	 *
	 * @param instrumentWeight
	 */
	public void validateInvestmentInstrumentStructureWeight(InvestmentInstrumentStructureWeight instrumentWeight);


	/**
	 * Populates current weights on each of the security allocations in the list of {@link InvestmentSecurityStructureAllocation}
	 *
	 * @param structure
	 * @param securityAllocationList
	 * @param date
	 */
	public void calculateCurrentWeights(InvestmentInstrumentStructure structure, List<InvestmentSecurityStructureAllocation> securityAllocationList, Date date);
}
