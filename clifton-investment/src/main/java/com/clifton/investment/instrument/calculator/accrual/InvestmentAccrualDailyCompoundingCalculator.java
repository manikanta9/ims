package com.clifton.investment.instrument.calculator.accrual;

import com.clifton.core.util.MathUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.math.BigDecimal;


/**
 * The InvestmentAccrualDailyCompoundingCalculator class calculates the reference rate using
 * daily compounding interest on the initial notional and also on the accumulated interest from prior interest rates.
 * Supports interest rate changes during a single payment period.
 * <p>
 * Accrual Amount = Notional * (PRODUCT_FOR_EACH_ACCRUAL_DATE(POWER(1 + Ri, 1/DayCountDenominator)) - 1)
 * <p>
 * NOTE: used by Brazilian Zero Coupon Swaps (a variation of Interest Rate Swaps).
 *
 * @author vgomelsky
 */
public class InvestmentAccrualDailyCompoundingCalculator extends InvestmentAccrualDailyRateCalculator {

	@Override
	protected BigDecimal calculateReferenceRateAmount(@SuppressWarnings("unused") InvestmentSecurityEvent paymentEvent, BigDecimal notional, AccrualDailyRate[] dailyRates, AccrualConfig accrualConfig) {
		// Accrual Amount = Notional * (PRODUCT_FOR_EACH_ACCRUAL_DATE(POWER(1 + Ri, 1/252)) - 1)
		// Where Ri is Reference rate for PREVIOUS day
		BigDecimal result = BigDecimal.ONE;
		int daysInYear = accrualConfig.getDayCountConvention().getDaysInYear();
		for (AccrualDailyRate accrualDailyRate : dailyRates) {
			BigDecimal dailyRate = accrualDailyRate.getRate();
			// normalizeDailyRate
			dailyRate = dailyRate.movePointLeft(2);
			BigDecimal adjustedDailyRate = BigDecimal.valueOf(Math.pow(MathUtils.add(BigDecimal.ONE, dailyRate).doubleValue(), 1.0 / daysInYear)); //POWER((1+Ri),1/252)
			result = MathUtils.multiply(result, adjustedDailyRate);
		}
		return MathUtils.multiply(notional, MathUtils.subtract(result, 1));
	}
}
