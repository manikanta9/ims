package com.clifton.investment.instrument.event.action;

import com.clifton.investment.instrument.event.action.search.InvestmentSecurityEventActionSearchForm;
import com.clifton.investment.instrument.event.action.search.InvestmentSecurityEventActionTypeSearchForm;

import java.util.List;


/**
 * The InvestmentSecurityEventActionService interface defines methods for managing investments security event actions.
 *
 * @author vgomelsky
 */
public interface InvestmentSecurityEventActionService {

	////////////////////////////////////////////////////////////////////////////
	///////       Investment Security Event Action Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventAction getInvestmentSecurityEventAction(int id);


	public List<InvestmentSecurityEventAction> getInvestmentSecurityEventActionList(InvestmentSecurityEventActionSearchForm searchForm);


	public InvestmentSecurityEventAction saveInvestmentSecurityEventAction(InvestmentSecurityEventAction eventAction);


	public void deleteInvestmentSecurityEventAction(int id);


	////////////////////////////////////////////////////////////////////////////
	//////       Investment Security Event Action Type Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventActionType getInvestmentSecurityEventActionType(short id);


	public List<InvestmentSecurityEventActionType> getInvestmentSecurityEventActionTypeList(InvestmentSecurityEventActionTypeSearchForm searchForm);


	public InvestmentSecurityEventActionType saveInvestmentSecurityEventActionType(InvestmentSecurityEventActionType actionType);


	public void deleteInvestmentSecurityEventActionType(short id);
}
