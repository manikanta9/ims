package com.clifton.investment.instrument.calculator.accrual;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;

import java.math.BigDecimal;


/**
 * <code>InvestmentAccrualDailyCompoundedAverageCalculator</code> is a daily accrual calculator for a period that is
 * calculated as an aggregation of daily accruals calculated using the compounded average between dates utilizing a
 * compounded interest rate index with daily rates (e.g., SOFR Compounded Index.
 * <p>
 * Daily average rate between X and Y = (IndexRateY/IndexRateX - 1) * (DayCountDenominator/Dc);
 * X = start date of period, Y = end date of period, Dc = calendar days in period
 * <p>
 * Daily accrual uses the average rate for the day in the period = Notional * DailyAverageRate * (DayCount/DayCountDenominator)
 *
 * @author NickK
 */
public class InvestmentAccrualDailyCompoundedAverageCalculator extends InvestmentAccrualDailyRateCalculator {

	@Override
	protected BigDecimal calculateReferenceRateAmount(InvestmentSecurityEvent paymentEvent, BigDecimal notional, AccrualDailyRate[] dailyRates, AccrualConfig accrualConfig) {
		BigDecimal accrual = BigDecimal.ZERO;
		int daysInYear = accrualConfig.getDayCountConvention().getDaysInYear();
		// Rates in the array are populated from fixing rate with each daily rate after.
		AccrualDailyRate fixingRate = AccrualDailyRate.forRateOnDate(paymentEvent.getAfterEventValue(), paymentEvent.getAdditionalDate());
		for (int i = 0; i < dailyRates.length; i++) {
			AccrualDailyRate dailyRate = dailyRates[i];
			BigDecimal periodDailyAverageRate = calculateDailyCompoundedAverage(fixingRate, dailyRate, accrualConfig);
			int dayInPeriod = i + 1;
			BigDecimal dayCountMultiplier = MathUtils.divide(dayInPeriod, daysInYear);
			BigDecimal dailyAccrual = MathUtils.multiply(
					MathUtils.multiply(notional, periodDailyAverageRate),
					dayCountMultiplier);
			// dailyRate = dailyRate.movePointLeft(2);
			accrual = MathUtils.add(accrual, dailyAccrual);
		}
		return accrual;
	}


	private BigDecimal calculateDailyCompoundedAverage(AccrualDailyRate fixingRate, AccrualDailyRate dailyRate, AccrualConfig accrualConfig) {
		int rateDayFromFixingDate = DateUtils.getDaysDifference(dailyRate.getRateDate(), fixingRate.getRateDate());
		if (rateDayFromFixingDate == 0) {
			return BigDecimal.ZERO;
		}
		int daysInYear = accrualConfig.getDayCountConvention().getDaysInYear();
		BigDecimal dayCountMultiplier = MathUtils.divide(daysInYear, rateDayFromFixingDate);
		return MathUtils.multiply(
				MathUtils.subtract(MathUtils.divide(dailyRate.getRate(), fixingRate.getRate()), 1),
				dayCountMultiplier);
	}
}
