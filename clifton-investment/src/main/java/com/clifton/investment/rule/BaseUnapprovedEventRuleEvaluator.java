package com.clifton.investment.rule;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.calendar.search.InvestmentEventSearchForm;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorContext;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author mitchellf
 */
public abstract class BaseUnapprovedEventRuleEvaluator<T extends IdentityObject, K extends RuleEvaluatorContext> extends BaseRuleEvaluator<T, K> {


	private String[] workflowStatusNames;
	private boolean limitToEventsOnOrBeforeDate;
	private Integer evaluationDayAdjustment;
	private boolean generateMultipleViolations;
	private Short[] eventTag;


	private InvestmentCalendarService investmentCalendarService;
	private SystemSchemaService systemSchemaService;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(T evaluationObject, RuleConfig ruleConfig, K evaluatorContext) {
		InvestmentAccount clientAccount = getEvaluationObjectClientAccount(evaluationObject, evaluatorContext);
		if (isObjectExcluded(evaluationObject, evaluatorContext)) {
			return new ArrayList<>();
		}
		return findViolations(evaluationObject, clientAccount.getId(), ruleConfig, evaluatorContext);
	}


	protected abstract InvestmentAccount getEvaluationObjectClientAccount(T evaluationObject, K evaluatorContext);


	protected abstract Date getEntityEvaluationDate(T evaluationObject, K evaluatorContext);


	/**
	 * Determines whether an object is excluded from evaluation.  In contrast to the fields {@link com.clifton.rule.definition.RuleAssignment#excluded} or {@link com.clifton.compliance.rule.assignment.ComplianceRuleAssignment#exclusionAssignment},
	 * this method is for determining inclusion/exclusion in a customized, more complex manner than just excluding the entity outright.
	 */
	protected abstract boolean isObjectExcluded(T evaluationObject, K evaluatorContext);


	private List<RuleViolation> findViolations(T evalObject, Integer clientAccountId, RuleConfig ruleConfig, K evaluatorContext) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(MathUtils.getNumberAsLong(clientAccountId));
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<InvestmentEvent> eventList = getInvestmentEventList(clientAccountId, evalObject, evaluatorContext);
			if (!CollectionUtils.isEmpty(eventList)) {
				if (isGenerateMultipleViolations() || eventList.size() == 1) {
					SystemTable causeTable = getSystemSchemaService().getSystemTableByName("InvestmentEvent");
					for (InvestmentEvent event : eventList) {
						Map<String, Object> contextValueMap = Collections.singletonMap("eventList", Collections.singletonList(event));
						ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, evalObject.getIdentity(), event.getId(), causeTable, contextValueMap));
					}
				}
				else {
					Map<String, Object> contextValueMap = new HashMap<>();
					contextValueMap.put("eventList", eventList);
					ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, evalObject.getIdentity(), contextValueMap));
				}
			}
		}

		return ruleViolationList;
	}


	private List<InvestmentEvent> getInvestmentEventList(Integer clientAccountId, T evalObject, K evaluatorContext) {
		if (clientAccountId != null) {
			InvestmentEventSearchForm searchForm = new InvestmentEventSearchForm();
			searchForm.setInvestmentAccountId(clientAccountId);
			searchForm.setWorkflowStatusNames(getWorkflowStatusNames());
			if (getEventTag() != null && !ArrayUtils.isEmpty(getEventTag())) {
				searchForm.setCategoryHierarchyIds(getEventTag());
				searchForm.setCategoryName("Investment Calendar Tags");
				searchForm.setCategoryTableName("InvestmentEventType");
				searchForm.setCategoryLinkFieldPath("eventType");
			}
			if (isLimitToEventsOnOrBeforeDate()) {
				Date evalDate = getEntityEvaluationDate(evalObject, evaluatorContext);
				if (getEvaluationDayAdjustment() != null) {
					evalDate = DateUtils.addDays(evalDate, getEvaluationDayAdjustment());
				}
				searchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.LESS_THAN_OR_EQUALS_OR_IS_NULL, DateUtils.clearTime(evalDate)));
			}

			return getInvestmentCalendarService().getInvestmentEventList(searchForm);
		}
		return new ArrayList<>();
	}

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String[] getWorkflowStatusNames() {
		return this.workflowStatusNames;
	}


	public void setWorkflowStatusNames(String[] workflowStatusNames) {
		this.workflowStatusNames = workflowStatusNames;
	}


	public boolean isLimitToEventsOnOrBeforeDate() {
		return this.limitToEventsOnOrBeforeDate;
	}


	public void setLimitToEventsOnOrBeforeDate(boolean limitToEventsOnOrBeforeDate) {
		this.limitToEventsOnOrBeforeDate = limitToEventsOnOrBeforeDate;
	}


	public Integer getEvaluationDayAdjustment() {
		return this.evaluationDayAdjustment;
	}


	public void setEvaluationDayAdjustment(Integer evaluationDayAdjustment) {
		this.evaluationDayAdjustment = evaluationDayAdjustment;
	}


	public boolean isGenerateMultipleViolations() {
		return this.generateMultipleViolations;
	}


	public void setGenerateMultipleViolations(boolean generateMultipleViolations) {
		this.generateMultipleViolations = generateMultipleViolations;
	}


	public Short[] getEventTag() {
		return this.eventTag;
	}


	public void setEventTag(Short[] eventTag) {
		this.eventTag = eventTag;
	}


	public InvestmentCalendarService getInvestmentCalendarService() {
		return this.investmentCalendarService;
	}


	public void setInvestmentCalendarService(InvestmentCalendarService investmentCalendarService) {
		this.investmentCalendarService = investmentCalendarService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
