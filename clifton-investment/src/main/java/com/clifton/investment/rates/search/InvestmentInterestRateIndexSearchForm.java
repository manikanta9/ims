package com.clifton.investment.rates.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


public class InvestmentInterestRateIndexSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "symbol,name", sortField = "symbol")
	private String searchPattern;

	@SearchField
	private String symbol;

	@SearchField
	private String name;

	@SearchField
	private String isdaName;

	@SearchField
	private String description;

	@SearchField
	private Integer order;

	@SearchField
	private String fixingTerm;

	@SearchField
	private Integer numberOfDays;

	@SearchField(searchFieldPath = "calendar", searchField = "name")
	private String calendarName;

	@SearchField(searchField = "currencyDenomination.id")
	private Integer currencyId;

	@SearchField(searchFieldPath = "currencyDenomination", searchField = "symbol,name", sortField = "symbol")
	private String currencyLabel;

	@SearchField(searchField = "groupList.referenceOne.name", comparisonConditions = ComparisonConditions.EXISTS)
	private String groupName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getIsdaName() {
		return this.isdaName;
	}


	public void setIsdaName(String isdaName) {
		this.isdaName = isdaName;
	}


	public String getFixingTerm() {
		return this.fixingTerm;
	}


	public void setFixingTerm(String fixingTerm) {
		this.fixingTerm = fixingTerm;
	}


	public Integer getCurrencyId() {
		return this.currencyId;
	}


	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getCurrencyLabel() {
		return this.currencyLabel;
	}


	public void setCurrencyLabel(String currencyLabel) {
		this.currencyLabel = currencyLabel;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCalendarName() {
		return this.calendarName;
	}


	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Integer getNumberOfDays() {
		return this.numberOfDays;
	}


	public void setNumberOfDays(Integer numberOfDays) {
		this.numberOfDays = numberOfDays;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getGroupName() {
		return this.groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}
