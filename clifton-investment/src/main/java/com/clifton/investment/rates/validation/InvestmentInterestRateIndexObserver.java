package com.clifton.investment.rates.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.investment.rates.InvestmentInterestRateIndexGroupIndex;
import com.clifton.investment.rates.InvestmentRatesService;
import com.clifton.investment.rates.search.InvestmentInterestRateIndexGroupIndexSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>InvestmentInterestRateIndexValidator</code> validates date ranges for indices.
 *
 * @author manderson
 */
@Component
public class InvestmentInterestRateIndexObserver extends SelfRegisteringDaoValidator<InvestmentInterestRateIndex> {

	private InvestmentRatesService investmentRatesService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentInterestRateIndex bean, DaoEventTypes config) throws ValidationException {
		if (bean.getStartDate() != null) {
			ValidationUtils.assertBefore(bean.getStartDate(), bean.getEndDate(), "endDate");
		}
		// De-Activating
		if (config.isUpdate()) {
			InvestmentInterestRateIndex originalBean = getOriginalBean(bean);
			if (bean.getStartDate() != null || bean.getEndDate() != null) {
				List<String> notEqualProperties = CoreCompareUtils.getNoEqualProperties(bean, originalBean, false);
				if (!CollectionUtils.isEmpty(notEqualProperties) && (notEqualProperties.contains("startDate") || notEqualProperties.contains("endDate"))) {
					InvestmentInterestRateIndexGroupIndexSearchForm searchForm = new InvestmentInterestRateIndexGroupIndexSearchForm();
					searchForm.setIndexId(bean.getId());
					List<InvestmentInterestRateIndexGroupIndex> groupIndexList = getInvestmentRatesService().getInvestmentInterestRateIndexGroupIndexList(searchForm);
					List<InvestmentInterestRateIndexGroupIndex> invalidList = new ArrayList<>();
					for (InvestmentInterestRateIndexGroupIndex groupIndex : CollectionUtils.getIterable(groupIndexList)) {
						boolean invalid = false;
						if (bean.getStartDate() != null && groupIndex.getStartDate() != null) {
							if (DateUtils.compare(groupIndex.getStartDate(), bean.getStartDate(), false) < 0) {
								invalid = true;
							}
						}
						if (bean.getEndDate() != null && groupIndex.getEndDate() != null) {
							if (DateUtils.compare(groupIndex.getEndDate(), bean.getEndDate(), false) > 0) {
								invalid = true;
							}
						}
						if (invalid) {
							invalidList.add(groupIndex);
						}
					}
					if (!CollectionUtils.isEmpty(invalidList)) {
						throw new ValidationException(
								"Invalid Date Range:  There are index mappings to the following group(s) that have start and/or end dates outside of selected start/end dates for the index: "
										+ BeanUtils.getPropertyValues(invalidList, "referenceOne.name", ","));
					}
				}
			}
		}
	}


	public InvestmentRatesService getInvestmentRatesService() {
		return this.investmentRatesService;
	}


	public void setInvestmentRatesService(InvestmentRatesService investmentRatesService) {
		this.investmentRatesService = investmentRatesService;
	}
}
