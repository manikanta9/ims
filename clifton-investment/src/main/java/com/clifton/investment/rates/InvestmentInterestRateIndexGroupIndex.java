package com.clifton.investment.rates;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>InvestmentInterestRateIndexGroupIndex</code> maps {@link InvestmentInterestRateIndexGroup}s to {@link InvestmentInterestRateIndex}s
 *
 * @author manderson
 */
public class InvestmentInterestRateIndexGroupIndex extends ManyToManyEntity<InvestmentInterestRateIndexGroup, InvestmentInterestRateIndex, Integer> {

	private Date startDate;
	private Date endDate;


	/////////////////////////////////////////////////


	/**
	 * Considered active as long as both the mapping and the index are active - otherwise it's inactive
	 */
	public boolean isActive() {
		return isIndexActive() && DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	public boolean isIndexActive() {
		if (getReferenceTwo() != null) {
			return getReferenceTwo().isActive();
		}
		return false;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/////////////////////////////////////////////////
}
