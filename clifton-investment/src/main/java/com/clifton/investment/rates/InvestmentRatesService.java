package com.clifton.investment.rates;


import com.clifton.investment.rates.search.InvestmentInterestRateIndexGroupIndexSearchForm;
import com.clifton.investment.rates.search.InvestmentInterestRateIndexGroupSearchForm;
import com.clifton.investment.rates.search.InvestmentInterestRateIndexSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentRatesService</code> interface defines methods for working with
 * interest rates indices.
 *
 * @author manderson
 */
public interface InvestmentRatesService {

	////////////////////////////////////////////////////////////////////////////
	////////   Investment Interest Rate Index Group Business Methods   /////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInterestRateIndexGroup getInvestmentInterestRateIndexGroup(short id);


	public InvestmentInterestRateIndexGroup getInvestmentInterestRateIndexGroupByName(String name);


	public List<InvestmentInterestRateIndexGroup> getInvestmentInterestRateIndexGroupList(InvestmentInterestRateIndexGroupSearchForm searchForm);


	public InvestmentInterestRateIndexGroup saveInvestmentInterestRateIndexGroup(InvestmentInterestRateIndexGroup bean);


	public void deleteInvestmentInterestRateIndexGroup(short id);

	////////////////////////////////////////////////////////////////////////////
	///////         Investment Interest Rate Index Business Methods    /////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInterestRateIndex getInvestmentInterestRateIndex(int id);


	public InvestmentInterestRateIndex getInvestmentInterestRateIndexBySymbol(String indexSymbol);


	/**
	 * Returns the InvestmentInterestRateIndex that matches groupName & currency
	 * and has the closest numberOfDays to that passed
	 */
	public InvestmentInterestRateIndex getInvestmentInterestRateIndexClosest(String groupName, int currencyId, int numberOfDays, Date balanceDate);


	public List<InvestmentInterestRateIndex> getInvestmentInterestRateIndexList(InvestmentInterestRateIndexSearchForm searchForm);


	public InvestmentInterestRateIndex saveInvestmentInterestRateIndex(InvestmentInterestRateIndex bean);


	public void deleteInvestmentInterestRateIndex(int id);

	////////////////////////////////////////////////////////////////////////////
	//////  Investment Interest Rate Index Group Index Business Methods  ///////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInterestRateIndexGroupIndex getInvestmentInterestRateIndexGroupIndex(int id);


	public List<InvestmentInterestRateIndexGroupIndex> getInvestmentInterestRateIndexGroupIndexList(InvestmentInterestRateIndexGroupIndexSearchForm searchForm);


	public void linkInvestmentInterestRateIndexGroupIndex(short groupId, int indexId);


	public InvestmentInterestRateIndexGroupIndex saveInvestmentInterestRateIndexGroupIndex(InvestmentInterestRateIndexGroupIndex bean);


	public void deleteInvestmentInterestRateIndexGroupIndex(int id);
}
