package com.clifton.investment.rates.cache;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The <code>InvestmentInterestRateIndexClosestCacheImpl</code> class caches {@link InvestmentInterestRateIndex} objects with
 * the closest number of days to the specified argument and that belong to the specified group.
 * <p>
 * NOTE: make sure to register it as an observer for InvestmentInterestRateIndex, InvestmentInterestRateIndexGroup and InvestmentInterestRateIndexGroupIndex DAO's.
 *
 * @author vgomelsky
 */
@Component
public class InvestmentInterestRateIndexClosestCacheImpl<T extends IdentityObject> extends BaseDaoEventObserver<T> implements CustomCache<String, InvestmentInterestRateIndex>, InvestmentInterestRateIndexClosestCache {

	private CacheHandler<String, InvestmentInterestRateIndex> cacheHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInterestRateIndex getInvestmentInterestRateIndex(String groupName, int currencyId, int numberOfDays, Date balanceDate) {
		return getCacheHandler().get(getCacheName(), getKey(groupName, currencyId, numberOfDays, balanceDate));
	}


	@Override
	public void storeInvestmentInterestRateIndex(String groupName, int currencyId, int numberOfDays, Date balanceDate, InvestmentInterestRateIndex index) {
		getCacheHandler().put(getCacheName(), getKey(groupName, currencyId, numberOfDays, balanceDate), index);
	}


	private String getKey(String groupName, int currencyId, int numberOfDays, Date balanceDate) {
		StringBuilder key = new StringBuilder(32);
		key.append(currencyId);
		key.append(groupName);
		key.append(numberOfDays);
		key.append(DateUtils.fromDateISO(balanceDate));
		return key.toString();
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unused")
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			getCacheHandler().clear(getCacheName());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	//////                   Getter and Setter Methods                  ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<String, InvestmentInterestRateIndex> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, InvestmentInterestRateIndex> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
