package com.clifton.investment.rates.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.rates.InvestmentInterestRateIndexGroup;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.investment.replication.search.InvestmentReplicationTypeSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentInterestRateIndexGroupValidator</code> disallows edits to system defined groups, date range, and
 * disallows deactivation of groups that are being used.
 *
 * @author manderson
 */
@Component
public class InvestmentInterestRateIndexGroupValidator extends SelfRegisteringDaoValidator<InvestmentInterestRateIndexGroup> {

	private InvestmentReplicationService investmentReplicationService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(InvestmentInterestRateIndexGroup bean, DaoEventTypes config) throws ValidationException {
		validateSystemDefined(bean, config);
		validateStartEndDates(bean, config);
	}


	private void validateSystemDefined(InvestmentInterestRateIndexGroup bean, DaoEventTypes config) {
		// Inserts or Deletes
		if (config.isInsert() || config.isDelete()) {
			ValidationUtils.assertFalse(bean.isSystemDefined(), "System Defined groups cannot be " + (config.isInsert() ? "added" : "deleted"));
		}
		// Updates
		else {
			InvestmentInterestRateIndexGroup originalBean = getOriginalBean(bean);
			ValidationUtils.assertTrue(originalBean.isSystemDefined() == bean.isSystemDefined(), "System Defined flag cannot be changed.", "systemDefined");
			if (originalBean.isSystemDefined()) {
				ValidationUtils.assertTrue(originalBean.getName().equals(bean.getName()), "System Defined Group Name [" + originalBean.getName() + "] cannot be changed.", "name");
			}
		}
	}


	private void validateStartEndDates(InvestmentInterestRateIndexGroup bean, DaoEventTypes config) {
		if (config.isInsert() || config.isUpdate()) {
			if (bean.getStartDate() != null) {
				ValidationUtils.assertBefore(bean.getStartDate(), bean.getEndDate(), "endDate");
			}

			if (config.isUpdate()) {
				InvestmentInterestRateIndexGroup originalBean = getOriginalBean(bean);
				if (originalBean.isActive() && !bean.isActive()) {
					InvestmentReplicationTypeSearchForm searchForm = new InvestmentReplicationTypeSearchForm();
					searchForm.setInterestRateIndexGroupId(bean.getId());
					List<InvestmentReplicationType> repTypeList = getInvestmentReplicationService().getInvestmentReplicationTypeList(searchForm);
					ValidationUtils.assertEmpty(
							repTypeList,
							"Cannot de-activate group [" + bean.getName() + "] because it is currently being used by the following replication type(s): "
									+ BeanUtils.getPropertyValues(repTypeList, "name", ","));
				}
			}
		}
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}
}
