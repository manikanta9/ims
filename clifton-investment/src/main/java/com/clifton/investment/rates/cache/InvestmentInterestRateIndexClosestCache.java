package com.clifton.investment.rates.cache;

import com.clifton.investment.rates.InvestmentInterestRateIndex;

import java.util.Date;


/**
 * The <code>InvestmentInterestRateIndexClosestCache</code> class caches {@link InvestmentInterestRateIndex} objects with
 * the closest number of days to the specified argument and that belong to the specified group.
 * <p>
 * NOTE: make sure to register it as an observer for InvestmentInterestRateIndex, InvestmentInterestRateIndexGroup and InvestmentInterestRateIndexGroupIndex DAO's.
 *
 * @author vgomelsky
 */
public interface InvestmentInterestRateIndexClosestCache {


	public InvestmentInterestRateIndex getInvestmentInterestRateIndex(String groupName, int currencyId, int numberOfDays, Date balanceDate);


	public void storeInvestmentInterestRateIndex(String groupName, int currencyId, int numberOfDays, Date balanceDate, InvestmentInterestRateIndex index);
}

