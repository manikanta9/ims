package com.clifton.investment.rates;


import com.clifton.calendar.setup.Calendar;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;


/**
 * The <code>InvestmentInterestRateIndex</code> class represents a rate that is charged or paid for the use of money.  Index rates change over time.
 * Rates of major indices are used to derive valuation of various investment securities.
 *
 * @author Mary Anderson
 */
public class InvestmentInterestRateIndex extends NamedEntity<Integer> {

	// The International Swaps and Derivatives Association (ISDA) Definition for the interest or swap rate reference
	private String isdaName;

	//The ticker symbol that this interest rate is commonly known as
	private String symbol;

	private int order;

	// The maturity of the interest or swap rate e.g. 28D, 1M, 3M, 6M, 1Y
	private String fixingTerm;
	private Integer numberOfDays;

	// The currency denomination this index is associated with
	private InvestmentSecurity currencyDenomination;

	/**
	 * Defines the holidays for this index and if rates are missing for a given holiday
	 * will return the rate of the previous business day.
	 */
	private Calendar calendar;

	private Date startDate;
	private Date endDate;


	// To determine if we are using US Secured Overnight Financing Rate (SOFR) Compounded Index for accrual calculations
	private boolean compounded;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return isActiveOnDate(null);
	}


	public boolean isActiveOnDate(Date date) {
		if (date == null) {
			date = new Date();
		}
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}


	public String getLabelWithActiveDates() {
		return getLabel() + " " + DateUtils.fromDateRange(getStartDate(), getEndDate(), true, false);
	}


	@Override
	public String getLabel() {
		if (getSymbol() != null) {
			return getSymbol() + ": " + getName();
		}
		return getName();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getIsdaName() {
		return this.isdaName;
	}


	public void setIsdaName(String isdaName) {
		this.isdaName = isdaName;
	}


	public String getFixingTerm() {
		return this.fixingTerm;
	}


	public void setFixingTerm(String fixingTerm) {
		this.fixingTerm = fixingTerm;
	}


	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String interestRateSymbol) {
		this.symbol = interestRateSymbol;
	}


	public Integer getNumberOfDays() {
		return this.numberOfDays;
	}


	public void setNumberOfDays(Integer numberOfDays) {
		this.numberOfDays = numberOfDays;
	}


	public InvestmentSecurity getCurrencyDenomination() {
		return this.currencyDenomination;
	}


	public void setCurrencyDenomination(InvestmentSecurity currencyDenomination) {
		this.currencyDenomination = currencyDenomination;
	}


	public Calendar getCalendar() {
		return this.calendar;
	}


	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public boolean isCompounded() {
		return this.compounded;
	}


	public void setCompounded(boolean compounded) {
		this.compounded = compounded;
	}
}
