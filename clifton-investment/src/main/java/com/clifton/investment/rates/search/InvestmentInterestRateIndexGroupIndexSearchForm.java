package com.clifton.investment.rates.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * The <code>InvestmentInterestRateIndexGroupIndexSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class InvestmentInterestRateIndexGroupIndexSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Short groupId;

	@SearchField(searchFieldPath = "referenceOne", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String groupName;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "name")
	private String indexName;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "symbol")
	private String indexSymbol;

	@SearchField(searchField = "referenceTwo.id")
	private Integer indexId;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "currencyDenomination.id")
	private Integer currencyId;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "numberOfDays", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Integer minNumberOfDays;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "numberOfDays", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Integer maxNumberOfDays;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "numberOfDays")
	private Integer numberOfDays;

	// Custom Search Field - Considered active if the mapping and the index are both active
	private Boolean active;

	// CUSTOM FILTER: (startDate IS NULL OR startDate >= activeOnDate) AND (endDate IS NULL OR COALESCE(earlyTerminationDate, endDate) <= activeOnDate)
	private Date activeOnDate;


	public Short getGroupId() {
		return this.groupId;
	}


	public void setGroupId(Short groupId) {
		this.groupId = groupId;
	}


	public String getGroupName() {
		return this.groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public Integer getIndexId() {
		return this.indexId;
	}


	public void setIndexId(Integer indexId) {
		this.indexId = indexId;
	}


	public Integer getCurrencyId() {
		return this.currencyId;
	}


	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}


	public Integer getMinNumberOfDays() {
		return this.minNumberOfDays;
	}


	public void setMinNumberOfDays(Integer minNumberOfDays) {
		this.minNumberOfDays = minNumberOfDays;
	}


	public Integer getMaxNumberOfDays() {
		return this.maxNumberOfDays;
	}


	public void setMaxNumberOfDays(Integer maxNumberOfDays) {
		this.maxNumberOfDays = maxNumberOfDays;
	}


	public Integer getNumberOfDays() {
		return this.numberOfDays;
	}


	public void setNumberOfDays(Integer numberOfDays) {
		this.numberOfDays = numberOfDays;
	}


	public String getIndexName() {
		return this.indexName;
	}


	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}


	public String getIndexSymbol() {
		return this.indexSymbol;
	}


	public void setIndexSymbol(String indexSymbol) {
		this.indexSymbol = indexSymbol;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	public Date getActiveOnDate() {
		return this.activeOnDate;
	}


	public void setActiveOnDate(Date activeOnDate) {
		this.activeOnDate = activeOnDate;
	}
}

