package com.clifton.investment.rates;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.rates.cache.InvestmentInterestRateIndexClosestCache;
import com.clifton.investment.rates.search.InvestmentInterestRateIndexGroupIndexSearchForm;
import com.clifton.investment.rates.search.InvestmentInterestRateIndexGroupSearchForm;
import com.clifton.investment.rates.search.InvestmentInterestRateIndexSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentRatesServiceImpl</code> class provides basic implementation of InvestmentRatesService interface.
 *
 * @author manderson
 */
@Service
public class InvestmentRatesServiceImpl implements InvestmentRatesService {

	private AdvancedUpdatableDAO<InvestmentInterestRateIndexGroup, Criteria> investmentInterestRateIndexGroupDAO;
	private AdvancedUpdatableDAO<InvestmentInterestRateIndex, Criteria> investmentInterestRateIndexDAO;
	private AdvancedUpdatableDAO<InvestmentInterestRateIndexGroupIndex, Criteria> investmentInterestRateIndexGroupIndexDAO;

	private InvestmentInterestRateIndexClosestCache investmentInterestRateIndexClosestCache;

	////////////////////////////////////////////////////////////////////////////
	///////   Market Data Interest Rate Index Group Business Methods   /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInterestRateIndexGroup getInvestmentInterestRateIndexGroup(short id) {
		return getInvestmentInterestRateIndexGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentInterestRateIndexGroup getInvestmentInterestRateIndexGroupByName(String name) {
		return getInvestmentInterestRateIndexGroupDAO().findOneByField("name", name);
	}


	@Override
	public List<InvestmentInterestRateIndexGroup> getInvestmentInterestRateIndexGroupList(InvestmentInterestRateIndexGroupSearchForm searchForm) {
		return getInvestmentInterestRateIndexGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	// See {@link InvestmentInterestRateIndexGroupValidator} for Validation (can't edit a system defined group name)
	@Override
	public InvestmentInterestRateIndexGroup saveInvestmentInterestRateIndexGroup(InvestmentInterestRateIndexGroup bean) {
		return getInvestmentInterestRateIndexGroupDAO().save(bean);
	}


	// See {@link InvestmentInterestRateIndexGroupValidator} for Validation (can't delete a system defined group name)
	@Override
	public void deleteInvestmentInterestRateIndexGroup(short id) {
		getInvestmentInterestRateIndexGroupDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	//////         Market Data Interest Rate Index Business Methods    /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInterestRateIndex getInvestmentInterestRateIndex(int id) {
		return getInvestmentInterestRateIndexDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentInterestRateIndex getInvestmentInterestRateIndexBySymbol(String indexSymbol) {
		return getInvestmentInterestRateIndexDAO().findOneByField("symbol", indexSymbol);
	}


	@Override
	public InvestmentInterestRateIndex getInvestmentInterestRateIndexClosest(String groupName, int currencyId, int numberOfDays, Date balanceDate) {
		ValidationUtils.assertFalse(StringUtils.isEmpty(groupName), "Interest Rate Index Group Name is Required");

		InvestmentInterestRateIndex result = getInvestmentInterestRateIndexClosestCache().getInvestmentInterestRateIndex(groupName, currencyId, numberOfDays, balanceDate);
		if (result == null) {
			InvestmentInterestRateIndexGroupIndexSearchForm lowSearch = new InvestmentInterestRateIndexGroupIndexSearchForm();
			lowSearch.setGroupName(groupName);
			lowSearch.setCurrencyId(currencyId);
			lowSearch.setMaxNumberOfDays(numberOfDays);
			lowSearch.setActiveOnDate(balanceDate);
			lowSearch.setOrderBy("maxNumberOfDays:desc");
			lowSearch.setLimit(1);

			Integer daysDiff = null;
			InvestmentInterestRateIndexGroupIndex low = CollectionUtils.getFirstElement(getInvestmentInterestRateIndexGroupIndexList(lowSearch));
			if (low != null) {
				result = low.getReferenceTwo();
			}
			if (result != null) {
				if (numberOfDays == result.getNumberOfDays()) {
					daysDiff = 0;
				}
				else {
					daysDiff = numberOfDays - result.getNumberOfDays();
				}
			}
			if (daysDiff == null || daysDiff != 0) {
				InvestmentInterestRateIndexGroupIndexSearchForm highSearch = new InvestmentInterestRateIndexGroupIndexSearchForm();
				highSearch.setActiveOnDate(balanceDate);
				highSearch.setGroupName(groupName);
				highSearch.setCurrencyId(currencyId);
				highSearch.setMinNumberOfDays(numberOfDays);
				highSearch.setOrderBy("minNumberOfDays:asc");
				highSearch.setLimit(1);

				InvestmentInterestRateIndexGroupIndex high = CollectionUtils.getFirstElement(getInvestmentInterestRateIndexGroupIndexList(highSearch));
				if (high != null) {
					if ((result == null) || (high.getReferenceTwo().getNumberOfDays() - numberOfDays < daysDiff)) {
						result = high.getReferenceTwo();
					}
				}
			}
			getInvestmentInterestRateIndexClosestCache().storeInvestmentInterestRateIndex(groupName, currencyId, numberOfDays, balanceDate, result);
		}
		return result;
	}


	@Override
	public List<InvestmentInterestRateIndex> getInvestmentInterestRateIndexList(final InvestmentInterestRateIndexSearchForm searchForm) {
		return getInvestmentInterestRateIndexDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentInterestRateIndex saveInvestmentInterestRateIndex(InvestmentInterestRateIndex bean) {
		return getInvestmentInterestRateIndexDAO().save(bean);
	}


	@Override
	public void deleteInvestmentInterestRateIndex(int id) {
		getInvestmentInterestRateIndexDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	/////  Market Data Interest Rate Index Group Index Business Methods  ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentInterestRateIndexGroupIndex getInvestmentInterestRateIndexGroupIndex(int id) {
		return getInvestmentInterestRateIndexGroupIndexDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentInterestRateIndexGroupIndex> getInvestmentInterestRateIndexGroupIndexList(final InvestmentInterestRateIndexGroupIndexSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getActive() != null && searchForm.getActiveOnDate() != null) {
					searchForm.setActiveOnDate(DateUtils.clearTime(new Date()));
				}
				if (searchForm.getActiveOnDate() != null) {
					if (searchForm.getActive() == null) {
						searchForm.setActive(true);
					}
					// If Looking for Active - checks where the mapping and the index are both active;
					// If Looking for Inactive - checks where the mapping OR the index are inactive
					ActiveExpressionForDates mappingFilter = ActiveExpressionForDates.forActiveOnDate(searchForm.getActive(), searchForm.getActiveOnDate());
					ActiveExpressionForDates indexFilter = ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(searchForm.getActive(), getPathAlias("referenceTwo", criteria), null, null, searchForm.getActiveOnDate());
					if (searchForm.getActive()) {
						criteria.add(Restrictions.and(mappingFilter, indexFilter));
					}
					else {
						criteria.add(Restrictions.or(mappingFilter, indexFilter));
					}
				}
			}
		};
		return getInvestmentInterestRateIndexGroupIndexDAO().findBySearchCriteria(config);
	}


	@Override
	public void linkInvestmentInterestRateIndexGroupIndex(short groupId, int indexId) {
		InvestmentInterestRateIndexGroupIndex bean = new InvestmentInterestRateIndexGroupIndex();
		bean.setReferenceOne(getInvestmentInterestRateIndexGroup(groupId));
		bean.setReferenceTwo(getInvestmentInterestRateIndex(indexId));
		getInvestmentInterestRateIndexGroupIndexDAO().save(bean);
	}


	@Override
	public InvestmentInterestRateIndexGroupIndex saveInvestmentInterestRateIndexGroupIndex(InvestmentInterestRateIndexGroupIndex bean) {
		return getInvestmentInterestRateIndexGroupIndexDAO().save(bean);
	}


	@Override
	public void deleteInvestmentInterestRateIndexGroupIndex(int id) {
		getInvestmentInterestRateIndexGroupIndexDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentInterestRateIndexGroup, Criteria> getInvestmentInterestRateIndexGroupDAO() {
		return this.investmentInterestRateIndexGroupDAO;
	}


	public void setInvestmentInterestRateIndexGroupDAO(AdvancedUpdatableDAO<InvestmentInterestRateIndexGroup, Criteria> investmentInterestRateIndexGroupDAO) {
		this.investmentInterestRateIndexGroupDAO = investmentInterestRateIndexGroupDAO;
	}


	public AdvancedUpdatableDAO<InvestmentInterestRateIndex, Criteria> getInvestmentInterestRateIndexDAO() {
		return this.investmentInterestRateIndexDAO;
	}


	public void setInvestmentInterestRateIndexDAO(AdvancedUpdatableDAO<InvestmentInterestRateIndex, Criteria> investmentInterestRateIndexDAO) {
		this.investmentInterestRateIndexDAO = investmentInterestRateIndexDAO;
	}


	public AdvancedUpdatableDAO<InvestmentInterestRateIndexGroupIndex, Criteria> getInvestmentInterestRateIndexGroupIndexDAO() {
		return this.investmentInterestRateIndexGroupIndexDAO;
	}


	public void setInvestmentInterestRateIndexGroupIndexDAO(AdvancedUpdatableDAO<InvestmentInterestRateIndexGroupIndex, Criteria> investmentInterestRateIndexGroupIndexDAO) {
		this.investmentInterestRateIndexGroupIndexDAO = investmentInterestRateIndexGroupIndexDAO;
	}


	public InvestmentInterestRateIndexClosestCache getInvestmentInterestRateIndexClosestCache() {
		return this.investmentInterestRateIndexClosestCache;
	}


	public void setInvestmentInterestRateIndexClosestCache(InvestmentInterestRateIndexClosestCache investmentInterestRateIndexClosestCache) {
		this.investmentInterestRateIndexClosestCache = investmentInterestRateIndexClosestCache;
	}
}
