package com.clifton.investment.rates;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>InvestmentInterestRateIndexGroup</code> ...
 *
 * @author Mary Anderson
 */
public class InvestmentInterestRateIndexGroup extends NamedEntity<Short> {

	private boolean systemDefined;

	private Date startDate;
	private Date endDate;


	/////////////////////////////////////////////////


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	/////////////////////////////////////////////////


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
