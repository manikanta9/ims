package com.clifton.investment.rates.validation;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.rates.InvestmentInterestRateIndexGroupIndex;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentInterestRateIndexGroupIndexValidator</code> class validates dates and date ranges.
 *
 * @author manderson
 */
@Component
public class InvestmentInterestRateIndexGroupIndexValidator extends SelfRegisteringDaoValidator<InvestmentInterestRateIndexGroupIndex> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(InvestmentInterestRateIndexGroupIndex bean, DaoEventTypes config) throws ValidationException {
		// USE DAO METHOD
	}


	@Override
	public void validate(InvestmentInterestRateIndexGroupIndex bean, DaoEventTypes config, ReadOnlyDAO<InvestmentInterestRateIndexGroupIndex> dao) throws ValidationException {
		if (bean.getStartDate() != null) {
			ValidationUtils.assertBefore(bean.getStartDate(), bean.getEndDate(), "endDate");
		}

		List<InvestmentInterestRateIndexGroupIndex> sameGroupIndexList = dao.findByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{bean.getReferenceOne().getId(),
				bean.getReferenceTwo().getId()});
		for (InvestmentInterestRateIndexGroupIndex sameGroupIndex : CollectionUtils.getIterable(sameGroupIndexList)) {
			if (DateUtils.isOverlapInDates(bean.getStartDate(), bean.getEndDate(), sameGroupIndex.getStartDate(), sameGroupIndex.getEndDate())) {
				if (config.isInsert() || !bean.equals(sameGroupIndex)) {
					throw new ValidationException("Group - Index Mapping [" + bean.getReferenceOne().getName() + " - " + bean.getReferenceTwo().getName()
							+ "] already exists for overlapping date range " + DateUtils.fromDateRange(sameGroupIndex.getStartDate(), sameGroupIndex.getEndDate(), true, false));
				}
			}
		}

		// Validation Start Date is Not BEFORE Index Start Date
		// Note - Not enforcing on the group - because if the group is inactive then wouldn't even look at the mappings...
		if (bean.getStartDate() != null && bean.getReferenceTwo().getStartDate() != null) {
			if (DateUtils.compare(bean.getStartDate(), bean.getReferenceTwo().getStartDate(), false) < 0) {
				throw new ValidationException("Group - Index Mapping Start Date [" + DateUtils.fromDateShort(bean.getStartDate()) + "] cannot be before the Index " + bean.getReferenceTwo().getName()
						+ " Start Date [" + DateUtils.fromDateShort(bean.getReferenceTwo().getStartDate()) + "]");
			}
		}
		// Validation End Date is Not AFTER Index End Date
		if (bean.getEndDate() != null && bean.getReferenceTwo().getEndDate() != null) {
			if (DateUtils.compare(bean.getEndDate(), bean.getReferenceTwo().getEndDate(), false) > 0) {
				throw new ValidationException("Group - Index Mapping End Date [" + DateUtils.fromDateShort(bean.getEndDate()) + "] cannot be after the Index " + bean.getReferenceTwo().getName()
						+ " End Date [" + DateUtils.fromDateShort(bean.getReferenceTwo().getEndDate()) + "]");
			}
		}
	}
}
