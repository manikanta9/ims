package com.clifton.investment.workflow.task.generator;

import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.generator.WorkflowCalendarScheduleTaskGenerator;

import java.util.Date;
import java.util.List;


/**
 * The InvestmentAccountGroupWorkflowTaskGenerator class is responsible for generation of new {@link WorkflowTask} objects
 * with due dates based on a custom {@link CalendarSchedule}. One task is generated for each {@linke InvestmentAccount} in the specified account group.
 *
 * @author vgomelsky
 */
public class InvestmentAccountGroupWorkflowTaskGenerator extends WorkflowCalendarScheduleTaskGenerator {

	private Integer investmentAccountGroupId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void validate(WorkflowTaskDefinition taskDefinition, Date fromDate, Date toDate) {
		super.validate(taskDefinition, fromDate, toDate);

		ValidationUtils.assertNotNull(getInvestmentAccountGroupId(), "Required field 'Account Group' is not defined.");
		ValidationUtils.assertNotNull(taskDefinition.getLinkedEntityTable(), "'Linked Entity Table' is required for definitions using: InvestmentAccountGroupWorkflowTaskGenerator.");
		ValidationUtils.assertEquals(taskDefinition.getLinkedEntityTable().getName(), "InvestmentAccount", "Task Definition using InvestmentAccountGroupWorkflowTaskGenerator must be linked to InvestmentAccount table.");
	}


	@Override
	protected List<? extends IdentityObject> getLinkedEntityList() {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setInvestmentAccountGroupId(getInvestmentAccountGroupId());
		return getInvestmentAccountService().getInvestmentAccountList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
