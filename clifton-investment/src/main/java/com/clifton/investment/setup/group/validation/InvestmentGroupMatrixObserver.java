package com.clifton.investment.setup.group.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupItem;
import com.clifton.investment.setup.group.InvestmentGroupMatrix;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.search.InvestmentGroupItemSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>InvestmentGroupMatrixValidator</code> handles validating {@link InvestmentGroupMatrix} prior to
 * saving.
 *
 * @author manderson
 */
@Component
public class InvestmentGroupMatrixObserver extends SelfRegisteringDaoObserver<InvestmentGroupMatrix> {

	private InvestmentGroupService investmentGroupService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<InvestmentGroupMatrix> dao, DaoEventTypes event, InvestmentGroupMatrix bean) {
		// Validation for Groups that don't use a template only - otherwise after call makes the same update to the groups that use this as its template
		InvestmentGroupItem item = bean.getGroupItem();
		InvestmentGroup group = item.getGroup();
		if (group.getTemplateInvestmentGroup() == null) {

			// Call get original bean so we have it
			if (event.isUpdate()) {
				getOriginalBean(dao, bean);
			}

			// Validate if Group IsLeafAssignmentOnly is set, that the group item selected is a leaf item
			if (item.getGroup().isLeafAssignmentOnly()) {
				InvestmentGroupItemSearchForm searchForm = new InvestmentGroupItemSearchForm();
				searchForm.setGroupId(item.getGroup().getId());
				searchForm.setParentId(item.getId());
				if (!CollectionUtils.isEmpty(getInvestmentGroupService().getInvestmentGroupItemList(searchForm))) {
					throw new FieldValidationException("Cannot create assignments for item [" + item.getNameExpanded() + "].  Group [" + item.getGroup().getName() + "] allows leaf assignments only.",
							"groupItem");
				}
			}

			// Check if at least Hierarchy OR Instrument OR Type is Selected, but not combinations of these
			String errorMsg = "Either a hierarchy, type, or instrument must be selected, but not a combination of these fields.";
			if (bean.getHierarchy() != null) {
				ValidationUtils.assertTrue(bean.getType() == null && bean.getInstrument() == null, errorMsg, "hierarchy.id");
			}
			else if (bean.getType() != null) {
				ValidationUtils.assertTrue(bean.getInstrument() == null, errorMsg, "type.id");
			}
			else {
				ValidationUtils.assertNotNull(bean.getInstrument() != null, errorMsg, "instrument.id");
			}

			// Validate Tradable Securities Only
			if (!bean.isExcluded() && bean.getGroupItem().getGroup().isTradableSecurityOnly()) {
				// If it includes sub hierarchies let it pass here because don't know which children may may not be valid selections
				if (bean.getHierarchy() != null && !bean.isIncludeSubHierarchies()) {
					ValidationUtils.assertFalse(bean.getHierarchy().isTradingDisallowed(), "Selected Hierarchy [" + bean.getHierarchy().getNameExpanded()
							+ "] is not a valid selection because the group allows tradable securities only and this hierarchy does not allow trading.");
				}
				else if (bean.getInstrument() != null) {
					ValidationUtils.assertFalse(bean.getInstrument().getHierarchy().isTradingDisallowed(), "Selected Instrument [" + bean.getInstrument().getLabel()
							+ "] is not a valid selection because it belongs to hierarchy [" + bean.getInstrument().getHierarchy().getNameExpanded()
							+ "] which is not a valid selection because the group allows tradable securities only and this hierarchy does not allow trading.");
				}
			}
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<InvestmentGroupMatrix> dao, DaoEventTypes event, InvestmentGroupMatrix bean, Throwable e) {
		if (e == null) {
			InvestmentGroup group = bean.getGroupItem().getGroup();
			// If not using a template, check if being used as a template and if so, apply the same change to the groups that use this as its template
			if (group.getTemplateInvestmentGroup() == null) {
				// NOTE: GROUP ITEM CANNOT BE UPDATED VIA UI - USING THIS ASSUMPTION IN THE TEMPLATE UPDATE
				if (event.isInsert() || event.isUpdate()) {
					List<InvestmentGroupItem> childItems = getInvestmentGroupService().getInvestmentGroupItemUsingItemAsTemplate(bean.getGroupItem());
					List<InvestmentGroupMatrix> updateList = new ArrayList<>();
					if (event.isInsert()) {
						for (InvestmentGroupItem ci : CollectionUtils.getIterable(childItems)) {
							InvestmentGroupMatrix childMatrix = BeanUtils.cloneBean(bean, false, false);
							childMatrix.setGroupItem(ci);
							updateList.add(childMatrix);
						}
					}
					else {
						// Passes original bean so we get matching before the update
						InvestmentGroupMatrix originalBean = getOriginalBean(dao, bean);
						if (originalBean != null) {
							List<InvestmentGroupMatrix> childMatrixList = getInvestmentGroupService().getInvestmentGroupMatrixUsingMatrixAsTemplate(originalBean);
							for (InvestmentGroupMatrix cm : CollectionUtils.getIterable(childMatrixList)) {
								InvestmentGroupMatrix newCm = BeanUtils.cloneBean(bean, false, false);
								newCm.setId(cm.getId());
								newCm.setGroupItem(cm.getGroupItem());
								updateList.add(newCm);
							}
						}
					}
					if (!CollectionUtils.isEmpty(updateList)) {
						((UpdatableDAO<InvestmentGroupMatrix>) dao).saveList(updateList);
					}
				}
				else { // Deletes
					List<InvestmentGroupMatrix> childMatrixList = getInvestmentGroupService().getInvestmentGroupMatrixUsingMatrixAsTemplate(bean);
					((UpdatableDAO<InvestmentGroupMatrix>) dao).deleteList(childMatrixList);
				}
			}
		}
	}


	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}
}
