package com.clifton.investment.setup.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.setup.InvestmentAssetClass;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentAssetClassValidator</code> ...
 *
 * @author manderson
 */
@Component
public class InvestmentAssetClassValidator extends SelfRegisteringDaoValidator<InvestmentAssetClass> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(InvestmentAssetClass bean, DaoEventTypes config) throws ValidationException {
		// nothing here - uses dao method instead
	}


	@Override
	public void validate(InvestmentAssetClass bean, DaoEventTypes config, ReadOnlyDAO<InvestmentAssetClass> dao) throws ValidationException {
		if (config.isUpdate()) {
			InvestmentAssetClass originalBean = getOriginalBean(bean);
			// Change Master Flag To NO - Check if it's used.
			if (originalBean != null && originalBean.isMaster() && !bean.isMaster()) {
				List<InvestmentAssetClass> list = dao.findByField("masterAssetClass.id", bean.getId());
				if (!CollectionUtils.isEmpty(list)) {
					throw new ValidationException("Cannot change asset class [" + bean.getName() + "] to a non-master asset class because it is already selected as the master asset class of ["
							+ list.size() + "] asset classes: " + BeanUtils.getPropertyValues(list, "name", ",", "", "", ",,,", 5));
				}
			}
		}

		// If it is a master asset class, master asset class selection should be null
		if (bean.isMaster()) {
			bean.setMasterAssetClass(null);
		}
		// Otherwise master asset class selection is required
		else {
			ValidationUtils.assertNotNull(bean.getMasterAssetClass(), "Master Asset Class selection is required for non-master asset classes.", "masterAssetClass");
			// Should never happen because UI already filters
			ValidationUtils.assertTrue(bean.getMasterAssetClass().isMaster(), "Selected Master Asset Class [" + bean.getMasterAssetClass().getName()
					+ "] is not flagged as a master asset class.  Please select a valid master asset class.", "masterAssetClass");
		}
	}
}
