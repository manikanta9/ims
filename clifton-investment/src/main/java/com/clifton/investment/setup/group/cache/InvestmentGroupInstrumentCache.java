package com.clifton.investment.setup.group.cache;

/**
 * The <code>InvestmentGroupSecurityCache</code> contains a lazily built cache for
 * a mapping of investment groups to a set of the instruments that fall within them.
 *
 * @author apopp
 */
public interface InvestmentGroupInstrumentCache {

	public boolean isInvestmentInstrumentInGroup(final String groupName, final int instrumentId);


	public void clearGroupCache(final String groupName);
}
