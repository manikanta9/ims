package com.clifton.investment.setup.group.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;

import java.util.Date;


public class InvestmentSecurityGroupSecuritySearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField(searchField = "referenceOne.name,referenceTwo.symbol,referenceTwo.cusip,referenceTwo.isin,referenceTwo.sedol,referenceTwo.name")
	private String searchPattern;


	@SearchField(searchField = "referenceOne.id")
	private Short investmentSecurityGroupId;

	@SearchField(searchField = "name", searchFieldPath = "referenceOne")
	private String investmentSecurityGroupName;

	// Custom Filter that identifies other security group items (from different groups) that have the same securities
	private Short overlappingSecurityGroupId;


	@SearchField(searchField = "referenceTwo.id")
	private Integer investmentSecurityId;

	@SearchField(searchFieldPath = "referenceTwo")
	private String symbol;

	@SearchField(searchFieldPath = "referenceTwo")
	private String cusip;

	@SearchField(searchFieldPath = "referenceTwo")
	private String isin;

	@SearchField(searchFieldPath = "referenceTwo")
	private String sedol;

	@SearchField(searchFieldPath = "referenceTwo")
	private String figi;

	@SearchField(searchFieldPath = "referenceTwo")
	private String occSymbol;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "name")
	private String securityName;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "referenceTwo.instrument")
	private Integer currencyDenominationId;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "endDate")
	private Date securityEndDate;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo.instrument.hierarchy")
	private String hierarchyName;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "referenceTwo.instrument.hierarchy")
	private Short investmentTypeId;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo.instrument.hierarchy.investmentType")
	private String investmentType;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo.instrument.hierarchy.investmentTypeSubType")
	private String investmentTypeSubType;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo.instrument.hierarchy.investmentTypeSubType2")
	private String investmentTypeSubType2;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "InvestmentSecurityGroupSecurity";
	}


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getInvestmentSecurityGroupId() {
		return this.investmentSecurityGroupId;
	}


	public void setInvestmentSecurityGroupId(Short investmentSecurityGroupId) {
		this.investmentSecurityGroupId = investmentSecurityGroupId;
	}


	public String getInvestmentSecurityGroupName() {
		return this.investmentSecurityGroupName;
	}


	public void setInvestmentSecurityGroupName(String investmentSecurityGroupName) {
		this.investmentSecurityGroupName = investmentSecurityGroupName;
	}


	public Short getOverlappingSecurityGroupId() {
		return this.overlappingSecurityGroupId;
	}


	public void setOverlappingSecurityGroupId(Short overlappingSecurityGroupId) {
		this.overlappingSecurityGroupId = overlappingSecurityGroupId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public String getIsin() {
		return this.isin;
	}


	public void setIsin(String isin) {
		this.isin = isin;
	}


	public String getSedol() {
		return this.sedol;
	}


	public void setSedol(String sedol) {
		this.sedol = sedol;
	}


	public String getFigi() {
		return this.figi;
	}


	public void setFigi(String figi) {
		this.figi = figi;
	}


	public String getOccSymbol() {
		return this.occSymbol;
	}


	public void setOccSymbol(String occSymbol) {
		this.occSymbol = occSymbol;
	}


	public String getSecurityName() {
		return this.securityName;
	}


	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}


	public Integer getCurrencyDenominationId() {
		return this.currencyDenominationId;
	}


	public void setCurrencyDenominationId(Integer currencyDenominationId) {
		this.currencyDenominationId = currencyDenominationId;
	}


	public Date getSecurityEndDate() {
		return this.securityEndDate;
	}


	public void setSecurityEndDate(Date securityEndDate) {
		this.securityEndDate = securityEndDate;
	}


	public String getHierarchyName() {
		return this.hierarchyName;
	}


	public void setHierarchyName(String hierarchyName) {
		this.hierarchyName = hierarchyName;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public String getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(String investmentType) {
		this.investmentType = investmentType;
	}


	public String getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(String investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public String getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(String investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}
}
