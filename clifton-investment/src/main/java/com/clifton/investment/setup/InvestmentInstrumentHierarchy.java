package com.clifton.investment.setup;


import com.clifton.business.company.BusinessCompanyType;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.util.ObjectUtils;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentPricingFrequencies;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationTypes;
import com.clifton.investment.instrument.calculator.InvestmentM2MCalculatorTypes;
import com.clifton.investment.instrument.calculator.accrual.AccrualMethods;
import com.clifton.investment.instrument.calculator.accrual.date.AccrualDateCalculators;
import com.clifton.investment.instrument.calculator.accrual.sign.AccrualSignCalculators;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.schema.column.SystemColumnCustom;

import java.math.BigDecimal;


/**
 * The <code>InvestmentInstrumentHierarchy</code> class classifies all investment instruments into hierarchical structure.
 * Example: Fixed Income / Futures, Options, Swaps; Equity / Futures, Options; Currency / Futures, Options; etc.
 *
 * @author vgomelsky
 */
public class InvestmentInstrumentHierarchy extends NamedHierarchicalEntity<InvestmentInstrumentHierarchy, Short> {

	private InvestmentType investmentType;

	private InvestmentTypeSubType investmentTypeSubType; // optional sub-type if available for selected type
	private InvestmentTypeSubType2 investmentTypeSubType2; // optional sub-type 2 if available for selected type
	/**
	 * Specifies whether assignment of instruments is allowed to this node.
	 * If true, the type must be selected.  Otherwise no type can be selected.
	 */
	private boolean assignmentAllowed;
	/**
	 * Different default screens and corresponding get/save/delete methods
	 */
	private boolean oneSecurityPerInstrument;
	/**
	 * Specifies the screen to use for the instruments under this hierarchy
	 */
	private String instrumentScreen;
	/**
	 * Specifies whether this hierarchy holds currencies or non-currency instruments
	 */
	private boolean currency;
	/**
	 * Specifies whether security's settlement currency can be different than its currency denomination.
	 * This is used in very rare cases for OTC securities only (TRS, ZCS, IRS).
	 */
	private boolean differentSettlementCurrencyAllowed;
	/**
	 * Specifies whether this hierarchy holds securities that can be traded.
	 * Non-tradable securities are benchmarks and indices.
	 */
	private boolean tradingDisallowed;
	/**
	 * Allowed only when tradingDisallowed = true
	 * Indicates that these are not real securities, i.e. Benchmarks: Commodities (Gold, Natural Gas), CPI Ratios, Inflation Indices
	 * and they do not have prices
	 */
	private boolean notASecurity;
	/**
	 * Securities that belong to this hierarchy do not require a payment when opening a position.
	 * For example, Futures.
	 */
	private boolean noPaymentOnOpen;
	/**
	 * Some securities cannot be closed by going in the opposite direction.  Instead they close at a maturity date only: Currency Forwards and LME.
	 */
	private boolean closeOnMaturityOnly;
	/**
	 * For most securities the sign on Quantity and position Cost Basis fields will be the same.
	 * If you buy something you get something and if you sell something you give it.
	 * Credit Default Swaps with prices over 100 behave differently. In some cases one can sell protection and pay for it.
	 */
	private boolean differentQuantityAndCostBasisSignAllowed;

	/**
	 * Pricing frequency is used to determine how often the securities are priced.  This can be used for notifications or allocated security rebuilds
	 * to determine if the latest price available is valid for a given date.
	 */
	private InvestmentPricingFrequencies pricingFrequency;

	/**
	 * If populated, securities in this hierarchy support allocations of securities
	 * The allocation type will determine the calculation used to price the security
	 */
	private InvestmentSecurityAllocationTypes securityAllocationType;

	/**
	 * Used when securityAllocationType is not null.  This indicates the allocations and weights
	 * on a specific date can be dynamically calculated - not taken as hand entered.
	 */
	private SystemBean dynamicAllocationCalculatorSystemBean;

	/**
	 * Specifies whether securities in this hierarchy are classified as OTC
	 */
	private boolean otc;
	/**
	 * Specifies whether securities in this hierarchy require collateral.
	 */
	private boolean collateralUsed;
	/**
	 * Illiquid securities cannot be easily sold or exchanged for cash without a substantial loss in value.
	 * This indicator can be used by compliance rules: 40 Act Fund 15% Illiquid Securities Test.
	 * If set, every security in this hierarchy must be illiquid.
	 */
	private boolean illiquid;
	/**
	 * If true, all securities under an instrument have the same events:
	 * Same Dates, Amounts, etc. (As long as Payment Date after its start date)
	 * Observer on event add/edit/remove handles copying the same changes to all other
	 * securities for that instrument
	 * Currently used for Credit Default Swaps
	 */
	private boolean eventSameForInstrumentSecurities;
	/**
	 * Specifies whether securities in this hierarchy can be traded on Maturity Date.
	 * For example, T-Bills cannot be traded so maturity event will have Ex Date = End Date (vs one day after).
	 */
	private boolean tradingOnEndDateAllowed;


	/**
	 * Indicates if a security of this hierarchy can have a reference security defined. FLEX options
	 * will use a Listed Option as a reference for some market data values for theoretical pricing.
	 */
	private boolean referenceSecurityAllowed;
	/**
	 * When reference security is defined, specifies whether it must be from the same Investment Instrument: limit UI choices and add validation.
	 * For example, TRS upsize can use a new security symbol but reference the original opening security.
	 */
	private boolean referenceSecurityFromSameInstrument;
	/**
	 * Specifies whether Settlement Instructions for Investment Security Events should be generated for children.
	 * For example, resets for TRS with multiple upsizes should be instructed together for the original (parent) security
	 * and all reference children should be ignored in order to avoid double instructing.
	 */
	private boolean referenceSecurityChildNotInstructedForEvents;
	/**
	 * Reference security field can be used for different purposes in different hierarchies.  Defines the label and tooltip for this specific hierarchy.
	 */
	private String referenceSecurityLabel;
	private String referenceSecurityTooltip;


	private InstrumentDeliverableTypes instrumentDeliverableType;
	/**
	 * The type of calculator to use to calculate costs for securities in this hierarchy. Overrides default calculator.
	 */
	private InvestmentNotionalCalculatorTypes costCalculator;
	/**
	 * The type of calculator to use to calculate mark to market.
	 */
	private InvestmentM2MCalculatorTypes m2mCalculator;
	/**
	 * Default exchange for securities in this hierarchy (use instrument specific to override this selection)
	 */
	private InvestmentExchange defaultExchange;

	/**
	 * Some securities use a settlement calendar custom column value defined by the hierarchy.  For those securities, prices
	 * would follow the settlement calendar business day logic, not the exchange business day logic.
	 * i.e. OTC CDS
	 */
	private SystemColumnCustom settlementCalendarColumn;
	/**
	 * Specifies the number of days it may take to settle a security of this type.
	 * If not specified, will use the same field from corresponding InvestmentType.
	 */
	private Integer daysToSettle;
	/**
	 * Optional price multiplier to be used for all securities in this hierarchy.
	 * Validation will not allow an instrument in this hierarchy to use a different multiplier.
	 */
	private BigDecimal priceMultiplier;

	/**
	 * When hierarchy price multiplier isn't set, it can be set on the instrument
	 * some securities will need to override the instrument level price multiplier
	 * EXAMPLE: FNH6  UK security where each security for the instrument differs based on number of days in the month
	 * This can also be set for 1:1 hierarchies that have a price multiplier set on the hierarchy, but allow overriding it on the instrument.
	 * When it's not checked and hierarchy multiplier is set, than instruments must always use the same price multiplier as the hierarchy
	 */
	private boolean securityPriceMultiplierOverrideAllowed;

	/**
	 * Specifies an override for what holding account to use for Cash when booking to the General Ledger.  By default will be the same holding account as the one used for Position.
	 * If {@link #cashLocationPurpose2} is specified, it will be used for lookup only if a holding account for this purpose cannot be found.
	 */
	private InvestmentAccountRelationshipPurpose cashLocationPurpose;
	/**
	 * Used to allow overrides for {@link #cashLocationPurpose} field. The logic is COALESCE(Cash Location Purpose, Cash Location Purpose 2).
	 * For example, Cash Location Purpose = Cash Location Override, Cash Location Purpose 2 = Custodian.
	 */
	private InvestmentAccountRelationshipPurpose cashLocationPurpose2;
	/**
	 * When selected, the underlying instrument must be from this hierarchy. Can be used independently or with group selection below.
	 * Example: Credit Default Swaps underlying always comes from Benchmarks / Indices hierarchy
	 * allows for filtering in selections and context menu right click Add New
	 */
	private InvestmentInstrumentHierarchy underlyingHierarchy;
	/**
	 * When selected, the underlying instrument must be in this group. Can be used independently or with hierarchy section above.
	 * Should be used ONLY for cases where underlying can be selected across hierarchies.
	 * Example: Benchmarks / Credit / Listed / Tranche and Benchmarks / Credit / Custom / Tranche
	 * needs to be set as underlying for Fixed Income / Swaps - Cleared / Credit Default Swaps / Tranche
	 * allows for filtering in selections
	 */
	private InvestmentGroup underlyingInvestmentGroup;

	/**
	 * Optionally specifies the type of Holding Account for every security in this hierarchy: OTC Account, OTC Cleared, etc.
	 */
	private InvestmentAccountType holdingAccountType;
	/**
	 * Optionally specifies the type of businessCompany (issuer/counter-party) for every security in this hierarchy: "Registered Investment Company" for ETF's, etc.
	 */
	private BusinessCompanyType businessCompanyType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Optional SystemBean that implements the {@link NotionalMultiplierRetriever} interface.
	 * Used by Inflation Linked Securities and Zero Coupon Swaps to retrieve security (and possibly date) specific Notional Multiplier.
	 */
	private SystemBean notionalMultiplierRetriever;

	/**
	 * When notionalMultiplierRetriever is defined, specifies whether the prices is already adjusted for the multiplier or not.
	 * Most of the time, the price is not adjusted (UK-Old bonds are). If adjusted, do not double use the multiplier in notional and dirty price calculations.
	 */
	private boolean notionalMultiplierForPriceNotUsed;

	/**
	 * When notionalMultiplierRetriever is defined, specifies whether accrual basis (Quantity or Notional) is already adjusted for teh multiplier or not.
	 * Most of the time, the accrual basis is not adjusted (Zero Coupon Swaps are). If adjusted, do not double use the multiplier in accrual calculations.
	 */
	private boolean notionalMultiplierForAccrualNotUsed;


	/**
	 * When determining market value, need to adjust quantity by the index ratio (used for TIPS and foreign inflation-linked securities).
	 * <p>
	 * NOTE: it maybe a better design to use InvestmentIndexRatioCalculatorTypes instead of all these index ratio specific fields.
	 */
	private boolean indexRatioAdjusted;

	/**
	 * Index Ratio = Current CPI / Base CPI.
	 * Usually Current CPI is as of Settlement Date of the trade or as of today for accrual.
	 * If this field is set to true then Current CPI is as of the first day of the month following the date of previous coupon payment.
	 */
	private boolean indexRatioCalculatedUsingMonthStartAfterPrevCoupon;
	/**
	 * The number of decimal places that Index Ratio must be rounded to.  Don't round if null.
	 */
	private Integer indexRatioPrecision;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Some securities can have "Factor Change" event (asset backed bonds, credit default swaps, etc.)
	 * If set, then factor change is allowed and the following event type is used to process factor changes.
	 */
	private InvestmentSecurityEventType factorChangeEventType;

	/**
	 * For hierarchies with securities that have periodic payments based on an interest rate (Cash Coupon Payment for bonds),
	 * identifies the InvestmentSecurityEventType that has payment information.  The same info is also used to calculate accrued interest.
	 */
	private InvestmentSecurityEventType accrualSecurityEventType;
	/**
	 * In rare cases, there maybe 2 separate accrual events: specifies optional second event (IRS: fixed and floating legs).
	 */
	private InvestmentSecurityEventType accrualSecurityEventType2;

	/**
	 * Defines accrual method that should be used when calculating accrual for securities in this hierarchy.
	 * Most of the time it's DAYCOUNT. However, some swaps may use custom calculations.
	 * This setting can be overridden at instrument level. Can only be set when accrualSecurityEventType is defined.
	 */
	private AccrualMethods accrualMethod;
	/**
	 * In rare cases, there maybe 2 separate accrual events: specifies optional second event (IRS: fixed and floating legs).
	 */
	private AccrualMethods accrualMethod2;

	/**
	 * Defines accrual date calculator to be used for event type events for securities in this hierarchy. If not specified,
	 * accrual date is the same as transaction date. Usually want to use Settlement date.
	 */
	private AccrualDateCalculators accrualDateCalculator;
	/**
	 * Defines accrual date calculator to be used for event type 2 events for securities in this hierarchy. If not specified,
	 * accrual date is the same as transaction date. Usually want to use Settlement date.
	 */
	private AccrualDateCalculators accrualDateCalculator2;

	/**
	 * Defines calculator that determines the sign for accrual amount.
	 * Usually the sign is positive (no change) but for Total Return Swaps we need to NEGATE it
	 * and for IRS we need to use "Structure" to determine the sign for each leg.
	 */
	private AccrualSignCalculators accrualSignCalculator;
	/**
	 * Specifies whether accrual calculation for {@link #accrualSecurityEventType} is based on Notional value of a position (swaps)
	 * or Quantity (bond Face).
	 */
	private boolean accrualBasedOnNotional;
	/**
	 * Specifies whether accrual calculation for {@link #accrualSecurityEventType2} is based on Notional value of a position (swaps)
	 * or Quantity (bond Face).
	 */
	private boolean accrual2BasedOnNotional;
	/**
	 * Some securities (TRS) use base trade accrual on opening notional. For vanilla TRS, there is no accrual on opening and closing accrual
	 * is calculated based on the corresponding notional being closed.
	 * For iBoxx, opening trade accrual is based on notional and closing trade accrual is based on opening notional being closed.
	 */
	private boolean tradeAccrualBasedOnOpeningNotional;

	/**
	 * Some securities (TRS (but not iBoxx) do not have accrued interest on opening but pay or receive it when closing a position.
	 */
	private boolean tradeAccrualOnOpeningAbsent;

	/**
	 * Most of the time Ex Date is one day after Accrual End Date and therefore there's no accrual after Ex Date.
	 * <p>
	 * British Linkers (Gilts) have the concept of Ex Date which is 8 days before period end date.
	 * One is entitled to full dividend for a position at close of day before Ex Date.  From Ex Date to Accrual Period End Date, we're accruing negative interest.
	 * <p>
	 * Total Return Swaps need to have this field set because they continue accruing until the end of the period (even after Ex Date).
	 */
	private boolean accrueAfterExDate;
	/**
	 * If bond's issue date doesn't match regular accrual period and first accrual period is partial, then
	 * only payment for the partial period will be made.  Use this flag to make the full payment (Example: TIPS = 912828SQ4)
	 */
	private boolean fullPaymentForFirstPartialPeriod;


	/**
	 * Should accrued payments for interest or dividend legs be included with the accrual amount.
	 */
	private boolean includeAccrualReceivables;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentHierarchy() {
		super();
	}


	public InvestmentInstrumentHierarchy(String name) {
		super();
		setName(name);
	}


	public boolean isAllocationOfSecurities() {
		return getSecurityAllocationType() != null;
	}


	public boolean isAllocationOfSecuritiesDynamic() {
		return getDynamicAllocationCalculatorSystemBean() != null;
	}


	public String getSecurityAllocationTypeLabel() {
		if (getSecurityAllocationType() != null) {
			return getSecurityAllocationType().getLabel();
		}
		return null;
	}


	public String getSecurityAllocationTypeWeightLabel() {
		if (getSecurityAllocationType() != null) {
			return getSecurityAllocationType().getWeightLabel();
		}
		return null;
	}


	public String getSecurityAllocationTypeSharesLabel() {
		if (getSecurityAllocationType() != null) {
			return getSecurityAllocationType().getSharesLabel();
		}
		return null;
	}


	public boolean isSecurityAllocationAutoRebalancingSupported() {
		if (getSecurityAllocationType() != null) {
			return getSecurityAllocationType().isAutoRebalancingSupported();
		}
		return false;
	}


	public boolean isAccrualAllowed() {
		return (this.accrualSecurityEventType != null);
	}


	public boolean isUnderlyingAllowed() {
		return ObjectUtils.isNotNullPresent(getUnderlyingHierarchy(), getUnderlyingInvestmentGroup());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isAssignmentAllowed() {
		return this.assignmentAllowed;
	}


	public void setAssignmentAllowed(boolean assignmentAllowed) {
		this.assignmentAllowed = assignmentAllowed;
	}


	public boolean isOneSecurityPerInstrument() {
		return this.oneSecurityPerInstrument;
	}


	public void setOneSecurityPerInstrument(boolean oneSecurityPerInstrument) {
		this.oneSecurityPerInstrument = oneSecurityPerInstrument;
	}


	public String getInstrumentScreen() {
		return this.instrumentScreen;
	}


	public void setInstrumentScreen(String instrumentScreen) {
		this.instrumentScreen = instrumentScreen;
	}


	public boolean isCurrency() {
		return this.currency;
	}


	public void setCurrency(boolean currency) {
		this.currency = currency;
	}


	public boolean isDifferentSettlementCurrencyAllowed() {
		return this.differentSettlementCurrencyAllowed;
	}


	public void setDifferentSettlementCurrencyAllowed(boolean differentSettlementCurrencyAllowed) {
		this.differentSettlementCurrencyAllowed = differentSettlementCurrencyAllowed;
	}


	public boolean isTradingDisallowed() {
		return this.tradingDisallowed;
	}


	public void setTradingDisallowed(boolean tradingDisallowed) {
		this.tradingDisallowed = tradingDisallowed;
	}


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}


	public boolean isNoPaymentOnOpen() {
		return this.noPaymentOnOpen;
	}


	public void setNoPaymentOnOpen(boolean noPaymentOnOpen) {
		this.noPaymentOnOpen = noPaymentOnOpen;
	}


	public boolean isCloseOnMaturityOnly() {
		return this.closeOnMaturityOnly;
	}


	public void setCloseOnMaturityOnly(boolean closeOnMaturityOnly) {
		this.closeOnMaturityOnly = closeOnMaturityOnly;
	}


	public InvestmentNotionalCalculatorTypes getCostCalculator() {
		return this.costCalculator;
	}


	public void setCostCalculator(InvestmentNotionalCalculatorTypes costCalculator) {
		this.costCalculator = costCalculator;
	}


	public Integer getDaysToSettle() {
		return this.daysToSettle;
	}


	public void setDaysToSettle(Integer daysToSettle) {
		this.daysToSettle = daysToSettle;
	}


	public InvestmentAccountRelationshipPurpose getCashLocationPurpose() {
		return this.cashLocationPurpose;
	}


	public void setCashLocationPurpose(InvestmentAccountRelationshipPurpose cashLocationPurpose) {
		this.cashLocationPurpose = cashLocationPurpose;
	}


	public InvestmentAccountRelationshipPurpose getCashLocationPurpose2() {
		return this.cashLocationPurpose2;
	}


	public void setCashLocationPurpose2(InvestmentAccountRelationshipPurpose cashLocationPurpose2) {
		this.cashLocationPurpose2 = cashLocationPurpose2;
	}


	public boolean isOtc() {
		return this.otc;
	}


	public void setOtc(boolean otc) {
		this.otc = otc;
	}


	public boolean isCollateralUsed() {
		return this.collateralUsed;
	}


	public void setCollateralUsed(boolean collateralUsed) {
		this.collateralUsed = collateralUsed;
	}


	public boolean isTradingOnEndDateAllowed() {
		return this.tradingOnEndDateAllowed;
	}


	public void setTradingOnEndDateAllowed(boolean tradingOnEndDateAllowed) {
		this.tradingOnEndDateAllowed = tradingOnEndDateAllowed;
	}


	public boolean isReferenceSecurityAllowed() {
		return this.referenceSecurityAllowed;
	}


	public void setReferenceSecurityAllowed(boolean referenceSecurityAllowed) {
		this.referenceSecurityAllowed = referenceSecurityAllowed;
	}


	public boolean isReferenceSecurityFromSameInstrument() {
		return this.referenceSecurityFromSameInstrument;
	}


	public void setReferenceSecurityFromSameInstrument(boolean referenceSecurityFromSameInstrument) {
		this.referenceSecurityFromSameInstrument = referenceSecurityFromSameInstrument;
	}


	public boolean isReferenceSecurityChildNotInstructedForEvents() {
		return this.referenceSecurityChildNotInstructedForEvents;
	}


	public void setReferenceSecurityChildNotInstructedForEvents(boolean referenceSecurityChildNotInstructedForEvents) {
		this.referenceSecurityChildNotInstructedForEvents = referenceSecurityChildNotInstructedForEvents;
	}


	public String getReferenceSecurityLabel() {
		return this.referenceSecurityLabel;
	}


	public void setReferenceSecurityLabel(String referenceSecurityLabel) {
		this.referenceSecurityLabel = referenceSecurityLabel;
	}


	public String getReferenceSecurityTooltip() {
		return this.referenceSecurityTooltip;
	}


	public void setReferenceSecurityTooltip(String referenceSecurityTooltip) {
		this.referenceSecurityTooltip = referenceSecurityTooltip;
	}


	public InvestmentM2MCalculatorTypes getM2mCalculator() {
		return this.m2mCalculator;
	}


	public void setM2mCalculator(InvestmentM2MCalculatorTypes m2mCalculator) {
		this.m2mCalculator = m2mCalculator;
	}


	public boolean isNotionalMultiplierUsed() {
		return (getNotionalMultiplierRetriever() != null);
	}


	public SystemBean getNotionalMultiplierRetriever() {
		return this.notionalMultiplierRetriever;
	}


	public void setNotionalMultiplierRetriever(SystemBean notionalMultiplierRetriever) {
		this.notionalMultiplierRetriever = notionalMultiplierRetriever;
	}


	public boolean isNotionalMultiplierForPriceNotUsed() {
		return this.notionalMultiplierForPriceNotUsed;
	}


	public void setNotionalMultiplierForPriceNotUsed(boolean notionalMultiplierForPriceNotUsed) {
		this.notionalMultiplierForPriceNotUsed = notionalMultiplierForPriceNotUsed;
	}


	public boolean isNotionalMultiplierForAccrualNotUsed() {
		return this.notionalMultiplierForAccrualNotUsed;
	}


	public void setNotionalMultiplierForAccrualNotUsed(boolean notionalMultiplierForAccrualNotUsed) {
		this.notionalMultiplierForAccrualNotUsed = notionalMultiplierForAccrualNotUsed;
	}


	public boolean isIndexRatioAdjusted() {
		return this.indexRatioAdjusted;
	}


	public void setIndexRatioAdjusted(boolean indexRatioAdjusted) {
		this.indexRatioAdjusted = indexRatioAdjusted;
	}


	public Integer getIndexRatioPrecision() {
		return this.indexRatioPrecision;
	}


	public void setIndexRatioPrecision(Integer indexRatioPrecision) {
		this.indexRatioPrecision = indexRatioPrecision;
	}


	public boolean isIndexRatioCalculatedUsingMonthStartAfterPrevCoupon() {
		return this.indexRatioCalculatedUsingMonthStartAfterPrevCoupon;
	}


	public void setIndexRatioCalculatedUsingMonthStartAfterPrevCoupon(boolean indexRatioCalculatedUsingMonthStartAfterPrevCoupon) {
		this.indexRatioCalculatedUsingMonthStartAfterPrevCoupon = indexRatioCalculatedUsingMonthStartAfterPrevCoupon;
	}


	public boolean isFullPaymentForFirstPartialPeriod() {
		return this.fullPaymentForFirstPartialPeriod;
	}


	public void setFullPaymentForFirstPartialPeriod(boolean fullPaymentForFirstPartialPeriod) {
		this.fullPaymentForFirstPartialPeriod = fullPaymentForFirstPartialPeriod;
	}


	public boolean isIncludeAccrualReceivables() {
		return this.includeAccrualReceivables;
	}


	public void setIncludeAccrualReceivables(boolean includeAccrualReceivables) {
		this.includeAccrualReceivables = includeAccrualReceivables;
	}


	public InvestmentSecurityEventType getAccrualSecurityEventType() {
		return this.accrualSecurityEventType;
	}


	public void setAccrualSecurityEventType(InvestmentSecurityEventType accrualSecurityEventType) {
		this.accrualSecurityEventType = accrualSecurityEventType;
	}


	public boolean isAccrualBasedOnNotional() {
		return this.accrualBasedOnNotional;
	}


	public void setAccrualBasedOnNotional(boolean accrualBasedOnNotional) {
		this.accrualBasedOnNotional = accrualBasedOnNotional;
	}


	public boolean isAccrual2BasedOnNotional() {
		return this.accrual2BasedOnNotional;
	}


	public void setAccrual2BasedOnNotional(boolean accrual2BasedOnNotional) {
		this.accrual2BasedOnNotional = accrual2BasedOnNotional;
	}


	public boolean isTradeAccrualBasedOnOpeningNotional() {
		return this.tradeAccrualBasedOnOpeningNotional;
	}


	public void setTradeAccrualBasedOnOpeningNotional(boolean tradeAccrualBasedOnOpeningNotional) {
		this.tradeAccrualBasedOnOpeningNotional = tradeAccrualBasedOnOpeningNotional;
	}


	public boolean isTradeAccrualOnOpeningAbsent() {
		return this.tradeAccrualOnOpeningAbsent;
	}


	public void setTradeAccrualOnOpeningAbsent(boolean tradeAccrualOnOpeningAbsent) {
		this.tradeAccrualOnOpeningAbsent = tradeAccrualOnOpeningAbsent;
	}


	public AccrualDateCalculators getAccrualDateCalculator() {
		return this.accrualDateCalculator;
	}


	public void setAccrualDateCalculator(AccrualDateCalculators accrualDateCalculator) {
		this.accrualDateCalculator = accrualDateCalculator;
	}


	public AccrualDateCalculators getAccrualDateCalculator2() {
		return this.accrualDateCalculator2;
	}


	public void setAccrualDateCalculator2(AccrualDateCalculators accrualDateCalculator2) {
		this.accrualDateCalculator2 = accrualDateCalculator2;
	}


	public boolean isAccrueAfterExDate() {
		return this.accrueAfterExDate;
	}


	public void setAccrueAfterExDate(boolean accrueAfterExDate) {
		this.accrueAfterExDate = accrueAfterExDate;
	}


	public InvestmentSecurityEventType getFactorChangeEventType() {
		return this.factorChangeEventType;
	}


	public void setFactorChangeEventType(InvestmentSecurityEventType factorChangeEventType) {
		this.factorChangeEventType = factorChangeEventType;
	}


	public boolean isDifferentQuantityAndCostBasisSignAllowed() {
		return this.differentQuantityAndCostBasisSignAllowed;
	}


	public void setDifferentQuantityAndCostBasisSignAllowed(boolean differentQuantityAndCostBasisSignAllowed) {
		this.differentQuantityAndCostBasisSignAllowed = differentQuantityAndCostBasisSignAllowed;
	}


	public InvestmentExchange getDefaultExchange() {
		return this.defaultExchange;
	}


	public void setDefaultExchange(InvestmentExchange defaultExchange) {
		this.defaultExchange = defaultExchange;
	}


	public BigDecimal getPriceMultiplier() {
		return this.priceMultiplier;
	}


	public void setPriceMultiplier(BigDecimal priceMultiplier) {
		this.priceMultiplier = priceMultiplier;
	}


	public InvestmentSecurityEventType getAccrualSecurityEventType2() {
		return this.accrualSecurityEventType2;
	}


	public void setAccrualSecurityEventType2(InvestmentSecurityEventType accrualSecurityEventType2) {
		this.accrualSecurityEventType2 = accrualSecurityEventType2;
	}


	public AccrualMethods getAccrualMethod() {
		return this.accrualMethod;
	}


	public void setAccrualMethod(AccrualMethods accrualMethod) {
		this.accrualMethod = accrualMethod;
	}


	public AccrualMethods getAccrualMethod2() {
		return this.accrualMethod2;
	}


	public void setAccrualMethod2(AccrualMethods accrualMethod2) {
		this.accrualMethod2 = accrualMethod2;
	}


	public boolean isEventSameForInstrumentSecurities() {
		return this.eventSameForInstrumentSecurities;
	}


	public void setEventSameForInstrumentSecurities(boolean eventSameForInstrumentSecurities) {
		this.eventSameForInstrumentSecurities = eventSameForInstrumentSecurities;
	}


	public InvestmentInstrumentHierarchy getUnderlyingHierarchy() {
		return this.underlyingHierarchy;
	}


	public void setUnderlyingHierarchy(InvestmentInstrumentHierarchy underlyingHierarchy) {
		this.underlyingHierarchy = underlyingHierarchy;
	}


	public InvestmentGroup getUnderlyingInvestmentGroup() {
		return this.underlyingInvestmentGroup;
	}


	public void setUnderlyingInvestmentGroup(InvestmentGroup underlyingInvestmentGroup) {
		this.underlyingInvestmentGroup = underlyingInvestmentGroup;
	}


	public InvestmentAccountType getHoldingAccountType() {
		return this.holdingAccountType;
	}


	public void setHoldingAccountType(InvestmentAccountType holdingAccountType) {
		this.holdingAccountType = holdingAccountType;
	}


	public AccrualSignCalculators getAccrualSignCalculator() {
		return this.accrualSignCalculator;
	}


	public void setAccrualSignCalculator(AccrualSignCalculators accrualSignCalculator) {
		this.accrualSignCalculator = accrualSignCalculator;
	}


	public BusinessCompanyType getBusinessCompanyType() {
		return this.businessCompanyType;
	}


	public void setBusinessCompanyType(BusinessCompanyType businessCompanyType) {
		this.businessCompanyType = businessCompanyType;
	}


	public boolean isIlliquid() {
		return this.illiquid;
	}


	public void setIlliquid(boolean illiquid) {
		this.illiquid = illiquid;
	}


	public InvestmentSecurityAllocationTypes getSecurityAllocationType() {
		return this.securityAllocationType;
	}


	public void setSecurityAllocationType(InvestmentSecurityAllocationTypes securityAllocationType) {
		this.securityAllocationType = securityAllocationType;
	}


	public InstrumentDeliverableTypes getInstrumentDeliverableType() {
		return this.instrumentDeliverableType;
	}


	public void setInstrumentDeliverableType(InstrumentDeliverableTypes instrumentDeliverableType) {
		this.instrumentDeliverableType = instrumentDeliverableType;
	}


	public InvestmentTypeSubType getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(InvestmentTypeSubType investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public InvestmentTypeSubType2 getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(InvestmentTypeSubType2 investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public boolean isNotASecurity() {
		return this.notASecurity;
	}


	public void setNotASecurity(boolean notASecurity) {
		this.notASecurity = notASecurity;
	}


	public SystemColumnCustom getSettlementCalendarColumn() {
		return this.settlementCalendarColumn;
	}


	public void setSettlementCalendarColumn(SystemColumnCustom settlementCalendarColumn) {
		this.settlementCalendarColumn = settlementCalendarColumn;
	}


	public SystemBean getDynamicAllocationCalculatorSystemBean() {
		return this.dynamicAllocationCalculatorSystemBean;
	}


	public void setDynamicAllocationCalculatorSystemBean(SystemBean dynamicAllocationCalculatorSystemBean) {
		this.dynamicAllocationCalculatorSystemBean = dynamicAllocationCalculatorSystemBean;
	}


	public boolean isSecurityPriceMultiplierOverrideAllowed() {
		return this.securityPriceMultiplierOverrideAllowed;
	}


	public void setSecurityPriceMultiplierOverrideAllowed(boolean securityPriceMultiplierOverrideAllowed) {
		this.securityPriceMultiplierOverrideAllowed = securityPriceMultiplierOverrideAllowed;
	}


	public InvestmentPricingFrequencies getPricingFrequency() {
		return this.pricingFrequency;
	}


	public void setPricingFrequency(InvestmentPricingFrequencies pricingFrequency) {
		this.pricingFrequency = pricingFrequency;
	}


	/**
	 * Classifies for a hierarchy if it contains deliverable securities
	 * (Option on the instrument)
	 */
	public enum InstrumentDeliverableTypes {

		ALWAYS, // All securities in this hierarchy are IsDeliverable = true
		NEVER, // All securities in this hierarchy are IsDeliverable = false
		BOTH // Allow both Deliverable and Non-Deliverable securities
	}
}
