package com.clifton.investment.setup.group.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author MitchellF
 */
public class InvestmentGroupTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private Integer levelsDeep;

	@SearchField
	private String currencyType;

	@SearchField
	private Boolean template;

	@SearchField
	private Boolean basedOnTemplate;

	@SearchField
	private Boolean singleGroupItem;

	@SearchField
	private Boolean baseCurrencyRequired;

	@SearchField
	private Boolean missingNotAllowed;

	@SearchField
	private Boolean leafAssignmentsOnly;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Integer getLevelsDeep() {
		return this.levelsDeep;
	}


	public void setLevelsDeep(Integer levelsDeep) {
		this.levelsDeep = levelsDeep;
	}


	public String getCurrencyType() {
		return this.currencyType;
	}


	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}


	public Boolean getTemplate() {
		return this.template;
	}


	public void setTemplate(Boolean template) {
		this.template = template;
	}


	public Boolean getBasedOnTemplate() {
		return this.basedOnTemplate;
	}


	public void setBasedOnTemplate(Boolean basedOnTemplate) {
		this.basedOnTemplate = basedOnTemplate;
	}


	public Boolean getSingleGroupItem() {
		return this.singleGroupItem;
	}


	public void setSingleGroupItem(Boolean singleGroupItem) {
		this.singleGroupItem = singleGroupItem;
	}


	public Boolean getBaseCurrencyRequired() {
		return this.baseCurrencyRequired;
	}


	public void setBaseCurrencyRequired(Boolean baseCurrencyRequired) {
		this.baseCurrencyRequired = baseCurrencyRequired;
	}


	public Boolean getMissingNotAllowed() {
		return this.missingNotAllowed;
	}


	public void setMissingNotAllowed(Boolean missingNotAllowed) {
		this.missingNotAllowed = missingNotAllowed;
	}


	public Boolean getLeafAssignmentsOnly() {
		return this.leafAssignmentsOnly;
	}


	public void setLeafAssignmentsOnly(Boolean leafAssignmentsOnly) {
		this.leafAssignmentsOnly = leafAssignmentsOnly;
	}
}
