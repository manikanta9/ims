package com.clifton.investment.setup.group.jobs;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupRebuildCommand;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.investment.setup.group.search.InvestmentSecurityGroupSearchForm;

import java.util.List;
import java.util.Map;


/**
 * For MSIM security restriction lists, we need to be able to rebuild {@link com.clifton.investment.setup.group.InvestmentSecurityGroup}s on periodic basis (could be as frequently
 * as every 15 minutes). This job will allow for limiting the security groups that are rebuilt based on the optional parameters.
 * In addition to optional parameters, the job will limit security groups to isSystemManaged().
 *
 * @author michaelm
 */
public class InvestmentSecurityGroupRebuildJob implements Task, StatusHolderObjectAware<Status> {


	private InvestmentSecurityGroupService investmentSecurityGroupService;

	private StatusHolderObject<Status> statusHolderObject;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Short[] investmentSecurityGroupIds;

	/**
	 * This flag causes the Investment Security Groups to be excluded from the rebuild instead of included. For example "rebuild groups for tag 'x' excluding these groups" or "
	 * rebuild all system managed groups excluding these groups" when no tag is specified.
	 */
	private boolean excludeInvestmentSecurityGroupIds;

	private Short[] systemHierarchyTagIds; // see security group detail window for the category.  likely will use the tag to identify them


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolderObject.getStatus();
		List<InvestmentSecurityGroup> securityGroupList = getInvestmentSecurityGroupList();
		Short[] securityGroupIds = CollectionUtils.getConverted(securityGroupList, InvestmentSecurityGroup::getId).toArray(new Short[0]);
		return getInvestmentSecurityGroupService().rebuildInvestmentSecurityGroupSystemManaged(InvestmentSecurityGroupRebuildCommand.forSystemManagedSecurityGroupsWithStatus(false, status, securityGroupIds));
	}


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<InvestmentSecurityGroup> getInvestmentSecurityGroupList() {
		InvestmentSecurityGroupSearchForm groupSearchForm = new InvestmentSecurityGroupSearchForm();
		groupSearchForm.setSystemManaged(true);
		if (!ArrayUtils.isEmpty(getInvestmentSecurityGroupIds())) {
			if (this.excludeInvestmentSecurityGroupIds) {
				groupSearchForm.setExcludeIds(getInvestmentSecurityGroupIds());
			}
			else {
				groupSearchForm.setIds(getInvestmentSecurityGroupIds());
			}
		}
		if (getSystemHierarchyTagIds() != null) {
			groupSearchForm.setCategoryHierarchyIds(getSystemHierarchyTagIds());
		}
		return getInvestmentSecurityGroupService().getInvestmentSecurityGroupList(groupSearchForm);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public Short[] getInvestmentSecurityGroupIds() {
		return this.investmentSecurityGroupIds;
	}


	public void setInvestmentSecurityGroupIds(Short[] investmentSecurityGroupIds) {
		this.investmentSecurityGroupIds = investmentSecurityGroupIds;
	}


	public boolean isExcludeInvestmentSecurityGroupIds() {
		return this.excludeInvestmentSecurityGroupIds;
	}


	public void setExcludeInvestmentSecurityGroupIds(boolean excludeInvestmentSecurityGroupIds) {
		this.excludeInvestmentSecurityGroupIds = excludeInvestmentSecurityGroupIds;
	}


	public Short[] getSystemHierarchyTagIds() {
		return this.systemHierarchyTagIds;
	}


	public void setSystemHierarchyTagIds(Short[] systemHierarchyTagIds) {
		this.systemHierarchyTagIds = systemHierarchyTagIds;
	}
}
