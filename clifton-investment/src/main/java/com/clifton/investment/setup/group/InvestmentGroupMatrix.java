package com.clifton.investment.setup.group;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>InvestmentGroupMatrix</code> defines the relationships between
 * {@link InvestmentInstrumentHierarchy}, or {@link InvestmentInstrument} with
 * an {@link InvestmentGroupItem}
 *
 * @author manderson
 */
public class InvestmentGroupMatrix extends BaseEntity<Integer> implements SystemEntityModifyConditionAware {

	private InvestmentGroupItem groupItem;

	/**
	 * Defines a type of an instrument: Stock, Future, Bond, etc.
	 */
	private InvestmentType type;

	/**
	 * A sub type classification for similar instruments falling under different investment types.
	 * <p/>
	 * Examples, Total Return Swaps, Interest Rate Swaps, Credit Default Swaps.
	 */
	private InvestmentTypeSubType subType;

	/**
	 * A second sub type classification for similar instruments falling under different investment types.
	 * Examples, Centrally Cleared Swaps, Over the Counter Swaps
	 **/
	private InvestmentTypeSubType2 subType2;

	private InvestmentInstrumentHierarchy hierarchy;
	private boolean includeSubHierarchies;

	private InvestmentInstrument instrument;

	/**
	 * Determines if a trading currency should be included/excluded
	 */
	private InvestmentSecurity tradingCurrency;

	/**
	 * Determines if this ultimate issuer should be included/excluded
	 * Filter all instruments with a one to one security mapping by the ultimate issuer of it's designated security.
	 */
	private BusinessCompany ultimateBusinessCompany;

	/**
	 * Includes securities that fall under this country of risk
	 */
	private SystemListItem countryOfRisk;

	/**
	 * Determines whether or not the relationship should be excluded
	 */
	private boolean excluded;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		if (getInstrument() != null) {
			return "Investment Instrument: " + getInstrument().getName();
		}
		else if (getHierarchy() != null) {
			return "Investment Hierarchy: " + getHierarchy().getNameExpanded();
		}
		else if (getType() != null) {
			StringBuilder lbl = new StringBuilder(16);
			lbl.append("Investment Type: ").append(getType().getName());
			if (getSubType() != null) {
				lbl.append(", Sub-Type: ").append(getSubType().getName());
			}
			if (getSubType2() != null) {
				lbl.append(", Sub-Type2: ").append(getSubType2().getName());
			}
			return lbl.toString();
		}
		return null;
	}


	public String getLabelWithTradingCurrency() {
		if (getTradingCurrency() != null) {
			return getLabel() + " Trading CCY: " + getTradingCurrency().getSymbol();
		}
		return getLabel();
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return (getGroupItem() == null) ? null : getGroupItem().getEntityModifyCondition();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrument getInstrument() {
		return this.instrument;
	}


	public void setInstrument(InvestmentInstrument instrument) {
		this.instrument = instrument;
	}


	public InvestmentGroupItem getGroupItem() {
		return this.groupItem;
	}


	public void setGroupItem(InvestmentGroupItem groupItem) {
		this.groupItem = groupItem;
	}


	public boolean isExcluded() {
		return this.excluded;
	}


	public void setExcluded(boolean excluded) {
		this.excluded = excluded;
	}


	public InvestmentSecurity getTradingCurrency() {
		return this.tradingCurrency;
	}


	public void setTradingCurrency(InvestmentSecurity tradingCurrency) {
		this.tradingCurrency = tradingCurrency;
	}


	public InvestmentInstrumentHierarchy getHierarchy() {
		return this.hierarchy;
	}


	public void setHierarchy(InvestmentInstrumentHierarchy hierarchy) {
		this.hierarchy = hierarchy;
	}


	public boolean isIncludeSubHierarchies() {
		return this.includeSubHierarchies;
	}


	public void setIncludeSubHierarchies(boolean includeSubHierarchies) {
		this.includeSubHierarchies = includeSubHierarchies;
	}


	public InvestmentType getType() {
		return this.type;
	}


	public void setType(InvestmentType type) {
		this.type = type;
	}


	public BusinessCompany getUltimateBusinessCompany() {
		return this.ultimateBusinessCompany;
	}


	public void setUltimateBusinessCompany(BusinessCompany ultimateBusinessCompany) {
		this.ultimateBusinessCompany = ultimateBusinessCompany;
	}


	public InvestmentTypeSubType getSubType() {
		return this.subType;
	}


	public void setSubType(InvestmentTypeSubType subType) {
		this.subType = subType;
	}


	public InvestmentTypeSubType2 getSubType2() {
		return this.subType2;
	}


	public void setSubType2(InvestmentTypeSubType2 subType2) {
		this.subType2 = subType2;
	}


	public SystemListItem getCountryOfRisk() {
		return this.countryOfRisk;
	}


	public void setCountryOfRisk(SystemListItem countryOfRisk) {
		this.countryOfRisk = countryOfRisk;
	}
}
