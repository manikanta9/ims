package com.clifton.investment.setup.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.instrument.InvestmentPricingFrequencies;
import com.clifton.investment.instrument.calculator.accrual.AccrualMethods;
import com.clifton.investment.instrument.calculator.accrual.date.AccrualDateCalculators;
import com.clifton.investment.instrument.calculator.accrual.sign.AccrualSignCalculators;


/**
 * The <code>InstrumentHierarchySearchForm</code> class defines search configuration for InvestmentInstrumentHierarchy objects.
 *
 * @author vgomelsky
 */
public class InstrumentHierarchySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "label,description")
	private String searchPattern;

	@SearchField
	private Short id;

	@SearchField(searchField = "parent.id")
	private Short parentId;

	@SearchField(searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField(searchField = "name", searchFieldPath = "investmentType", comparisonConditions = {ComparisonConditions.EQUALS})
	private String investmentTypeName;

	@SearchField(searchField = "investmentTypeSubType.id")
	private Short investmentTypeSubTypeId;

	@SearchField(searchField = "name", searchFieldPath = "investmentTypeSubType")
	private String investmentTypeSubType;

	@SearchField(searchField = "investmentTypeSubType2.id")
	private Short investmentTypeSubType2Id;

	@SearchField(searchField = "name", searchFieldPath = "investmentTypeSubType2")
	private String investmentTypeSubType2;

	@SearchField(searchField = "underlyingHierarchy.id")
	private Short underlyingHierarchyId;

	@SearchField(searchField = "underlyingInvestmentGroup.id")
	private Short underlyingInvestmentGroupId;

	@SearchField(searchField = "holdingAccountType.id")
	private Short holdingAccountTypeId;

	@SearchField(searchField = "businessCompanyType.id")
	private Short businessCompanyTypeId;

	@SearchField(searchField = "factorChangeEventType.id")
	private Short factorChangeEventTypeId;

	@SearchField(searchField = "cashLocationPurpose.id")
	private Short cashLocationPurposeId;

	@SearchField(searchField = "cashLocationPurpose2.id")
	private Short cashLocationPurpose2Id;

	@SearchField(searchField = "eventTypeList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short eventTypeId;

	@SearchField
	private Boolean eventSameForInstrumentSecurities;

	@SearchField(searchField = "parent.id", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean rootNodesOnly;

	@SearchField
	private Boolean assignmentAllowed;

	@SearchField
	private Boolean oneSecurityPerInstrument;

	@SearchField
	private Boolean noPaymentOnOpen;

	@SearchField
	private Boolean otc;

	@SearchField
	private Boolean currency;

	@SearchField
	private Boolean differentSettlementCurrencyAllowed;

	@SearchField
	private Boolean tradingDisallowed;

	@SearchField
	private Boolean notASecurity;

	@SearchField
	private Boolean closeOnMaturityOnly;

	@SearchField
	private Boolean differentQuantityAndCostBasisSignAllowed;

	@SearchField
	private InvestmentPricingFrequencies pricingFrequency;

	@SearchField
	private Boolean collateralUsed;

	@SearchField
	private Boolean illiquid;

	@SearchField(searchField = "securityAllocationType", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean allocationOfSecurities;

	@SearchField
	private Boolean tradingOnEndDateAllowed;

	@SearchField
	private Boolean securityPriceMultiplierOverrideAllowed;

	@SearchField
	private Boolean referenceSecurityAllowed;
	@SearchField
	private Boolean referenceSecurityFromSameInstrument;
	@SearchField
	private Boolean referenceSecurityChildNotInstructedForEvents;
	@SearchField
	private String referenceSecurityLabel;
	@SearchField
	private String referenceSecurityTooltip;

	@SearchField(searchFieldPath = "accrualSecurityEventType", searchField = "name")
	private String accrualSecurityEventType;
	@SearchField(searchFieldPath = "accrualSecurityEventType2", searchField = "name")
	private String accrualSecurityEventType2;
	@SearchField
	private AccrualMethods accrualMethod;
	@SearchField
	private AccrualMethods accrualMethod2;
	@SearchField
	private AccrualDateCalculators accrualDateCalculator;
	@SearchField
	private AccrualSignCalculators accrualSignCalculator;
	@SearchField
	private Boolean accrualBasedOnNotional;
	@SearchField
	private Boolean tradeAccrualBasedOnOpeningNotional;
	@SearchField
	private Boolean tradeAccrualOnOpeningAbsent;
	@SearchField
	private Boolean accrueAfterExDate;
	@SearchField
	private Boolean fullPaymentForFirstPartialPeriod;

	@SearchField(searchField = "name", searchFieldPath = "notionalMultiplierRetriever")
	private String notionalMultiplierRetriever;
	@SearchField
	private Boolean notionalMultiplierForPriceNotUsed;
	@SearchField
	private Boolean notionalMultiplierForAccrualNotUsed;

	@SearchField
	private Boolean indexRatioAdjusted;
	@SearchField
	private Boolean indexRatioCalculatedUsingMonthStartAfterPrevCoupon;
	@SearchField
	private Integer indexRatioPrecision;

	// Hierarchies are currently 7 levels max deep
	@SearchField(searchField = "parent.parent.parent.parent.parent.parent.label,parent.parent.parent.parent.parent.label,parent.parent.parent.parent.label,parent.parent.parent.label,parent.parent.label,parent.label,label", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private String labelExpanded;

	// Hierarchies are currently 7 levels max deep
	@SearchField(searchField = "parent.parent.parent.parent.parent.parent.name,parent.parent.parent.parent.parent.name,parent.parent.parent.parent.name,parent.parent.parent.name,parent.parent.name,parent.name,name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private String nameExpanded;

	@SearchField
	private Boolean includeAccrualReceivables;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Short getParentId() {
		return this.parentId;
	}


	public void setParentId(Short parentId) {
		this.parentId = parentId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public String getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(String investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public String getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(String investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public Short getUnderlyingHierarchyId() {
		return this.underlyingHierarchyId;
	}


	public void setUnderlyingHierarchyId(Short underlyingHierarchyId) {
		this.underlyingHierarchyId = underlyingHierarchyId;
	}


	public Short getUnderlyingInvestmentGroupId() {
		return this.underlyingInvestmentGroupId;
	}


	public void setUnderlyingInvestmentGroupId(Short underlyingInvestmentGroupId) {
		this.underlyingInvestmentGroupId = underlyingInvestmentGroupId;
	}


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public Short getBusinessCompanyTypeId() {
		return this.businessCompanyTypeId;
	}


	public void setBusinessCompanyTypeId(Short businessCompanyTypeId) {
		this.businessCompanyTypeId = businessCompanyTypeId;
	}


	public Boolean getAssignmentAllowed() {
		return this.assignmentAllowed;
	}


	public void setAssignmentAllowed(Boolean assignmentAllowed) {
		this.assignmentAllowed = assignmentAllowed;
	}


	public Boolean getRootNodesOnly() {
		return this.rootNodesOnly;
	}


	public void setRootNodesOnly(Boolean rootNodesOnly) {
		this.rootNodesOnly = rootNodesOnly;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public String getInvestmentTypeName() {
		return this.investmentTypeName;
	}


	public void setInvestmentTypeName(String investmentTypeName) {
		this.investmentTypeName = investmentTypeName;
	}


	public Short getEventTypeId() {
		return this.eventTypeId;
	}


	public void setEventTypeId(Short eventTypeId) {
		this.eventTypeId = eventTypeId;
	}


	public Boolean getNoPaymentOnOpen() {
		return this.noPaymentOnOpen;
	}


	public void setNoPaymentOnOpen(Boolean noPaymentOnOpen) {
		this.noPaymentOnOpen = noPaymentOnOpen;
	}


	public Boolean getEventSameForInstrumentSecurities() {
		return this.eventSameForInstrumentSecurities;
	}


	public void setEventSameForInstrumentSecurities(Boolean eventSameForInstrumentSecurities) {
		this.eventSameForInstrumentSecurities = eventSameForInstrumentSecurities;
	}


	public Boolean getIlliquid() {
		return this.illiquid;
	}


	public void setIlliquid(Boolean illiquid) {
		this.illiquid = illiquid;
	}


	public Boolean getAllocationOfSecurities() {
		return this.allocationOfSecurities;
	}


	public void setAllocationOfSecurities(Boolean allocationOfSecurities) {
		this.allocationOfSecurities = allocationOfSecurities;
	}


	public Boolean getTradingOnEndDateAllowed() {
		return this.tradingOnEndDateAllowed;
	}


	public void setTradingOnEndDateAllowed(Boolean tradingOnEndDateAllowed) {
		this.tradingOnEndDateAllowed = tradingOnEndDateAllowed;
	}


	public String getLabelExpanded() {
		return this.labelExpanded;
	}


	public void setLabelExpanded(String labelExpanded) {
		this.labelExpanded = labelExpanded;
	}


	public Boolean getOneSecurityPerInstrument() {
		return this.oneSecurityPerInstrument;
	}


	public void setOneSecurityPerInstrument(Boolean oneSecurityPerInstrument) {
		this.oneSecurityPerInstrument = oneSecurityPerInstrument;
	}


	public Boolean getOtc() {
		return this.otc;
	}


	public void setOtc(Boolean otc) {
		this.otc = otc;
	}


	public Boolean getCurrency() {
		return this.currency;
	}


	public void setCurrency(Boolean currency) {
		this.currency = currency;
	}


	public Boolean getDifferentSettlementCurrencyAllowed() {
		return this.differentSettlementCurrencyAllowed;
	}


	public void setDifferentSettlementCurrencyAllowed(Boolean differentSettlementCurrencyAllowed) {
		this.differentSettlementCurrencyAllowed = differentSettlementCurrencyAllowed;
	}


	public Boolean getTradingDisallowed() {
		return this.tradingDisallowed;
	}


	public void setTradingDisallowed(Boolean tradingDisallowed) {
		this.tradingDisallowed = tradingDisallowed;
	}


	public Boolean getNotASecurity() {
		return this.notASecurity;
	}


	public void setNotASecurity(Boolean notASecurity) {
		this.notASecurity = notASecurity;
	}


	public Boolean getCloseOnMaturityOnly() {
		return this.closeOnMaturityOnly;
	}


	public void setCloseOnMaturityOnly(Boolean closeOnMaturityOnly) {
		this.closeOnMaturityOnly = closeOnMaturityOnly;
	}


	public Boolean getDifferentQuantityAndCostBasisSignAllowed() {
		return this.differentQuantityAndCostBasisSignAllowed;
	}


	public void setDifferentQuantityAndCostBasisSignAllowed(Boolean differentQuantityAndCostBasisSignAllowed) {
		this.differentQuantityAndCostBasisSignAllowed = differentQuantityAndCostBasisSignAllowed;
	}


	public InvestmentPricingFrequencies getPricingFrequency() {
		return this.pricingFrequency;
	}


	public void setPricingFrequency(InvestmentPricingFrequencies pricingFrequency) {
		this.pricingFrequency = pricingFrequency;
	}


	public Boolean getCollateralUsed() {
		return this.collateralUsed;
	}


	public void setCollateralUsed(Boolean collateralUsed) {
		this.collateralUsed = collateralUsed;
	}


	public Boolean getSecurityPriceMultiplierOverrideAllowed() {
		return this.securityPriceMultiplierOverrideAllowed;
	}


	public void setSecurityPriceMultiplierOverrideAllowed(Boolean securityPriceMultiplierOverrideAllowed) {
		this.securityPriceMultiplierOverrideAllowed = securityPriceMultiplierOverrideAllowed;
	}


	public Boolean getReferenceSecurityAllowed() {
		return this.referenceSecurityAllowed;
	}


	public void setReferenceSecurityAllowed(Boolean referenceSecurityAllowed) {
		this.referenceSecurityAllowed = referenceSecurityAllowed;
	}


	public Boolean getReferenceSecurityFromSameInstrument() {
		return this.referenceSecurityFromSameInstrument;
	}


	public void setReferenceSecurityFromSameInstrument(Boolean referenceSecurityFromSameInstrument) {
		this.referenceSecurityFromSameInstrument = referenceSecurityFromSameInstrument;
	}


	public Boolean getReferenceSecurityChildNotInstructedForEvents() {
		return this.referenceSecurityChildNotInstructedForEvents;
	}


	public void setReferenceSecurityChildNotInstructedForEvents(Boolean referenceSecurityChildNotInstructedForEvents) {
		this.referenceSecurityChildNotInstructedForEvents = referenceSecurityChildNotInstructedForEvents;
	}


	public String getReferenceSecurityLabel() {
		return this.referenceSecurityLabel;
	}


	public void setReferenceSecurityLabel(String referenceSecurityLabel) {
		this.referenceSecurityLabel = referenceSecurityLabel;
	}


	public String getReferenceSecurityTooltip() {
		return this.referenceSecurityTooltip;
	}


	public void setReferenceSecurityTooltip(String referenceSecurityTooltip) {
		this.referenceSecurityTooltip = referenceSecurityTooltip;
	}


	public String getAccrualSecurityEventType() {
		return this.accrualSecurityEventType;
	}


	public void setAccrualSecurityEventType(String accrualSecurityEventType) {
		this.accrualSecurityEventType = accrualSecurityEventType;
	}


	public String getAccrualSecurityEventType2() {
		return this.accrualSecurityEventType2;
	}


	public void setAccrualSecurityEventType2(String accrualSecurityEventType2) {
		this.accrualSecurityEventType2 = accrualSecurityEventType2;
	}


	public AccrualMethods getAccrualMethod() {
		return this.accrualMethod;
	}


	public void setAccrualMethod(AccrualMethods accrualMethod) {
		this.accrualMethod = accrualMethod;
	}


	public AccrualMethods getAccrualMethod2() {
		return this.accrualMethod2;
	}


	public void setAccrualMethod2(AccrualMethods accrualMethod2) {
		this.accrualMethod2 = accrualMethod2;
	}


	public AccrualDateCalculators getAccrualDateCalculator() {
		return this.accrualDateCalculator;
	}


	public void setAccrualDateCalculator(AccrualDateCalculators accrualDateCalculator) {
		this.accrualDateCalculator = accrualDateCalculator;
	}


	public AccrualSignCalculators getAccrualSignCalculator() {
		return this.accrualSignCalculator;
	}


	public void setAccrualSignCalculator(AccrualSignCalculators accrualSignCalculator) {
		this.accrualSignCalculator = accrualSignCalculator;
	}


	public Boolean getAccrualBasedOnNotional() {
		return this.accrualBasedOnNotional;
	}


	public void setAccrualBasedOnNotional(Boolean accrualBasedOnNotional) {
		this.accrualBasedOnNotional = accrualBasedOnNotional;
	}


	public Boolean getTradeAccrualBasedOnOpeningNotional() {
		return this.tradeAccrualBasedOnOpeningNotional;
	}


	public void setTradeAccrualBasedOnOpeningNotional(Boolean tradeAccrualBasedOnOpeningNotional) {
		this.tradeAccrualBasedOnOpeningNotional = tradeAccrualBasedOnOpeningNotional;
	}


	public Boolean getTradeAccrualOnOpeningAbsent() {
		return this.tradeAccrualOnOpeningAbsent;
	}


	public void setTradeAccrualOnOpeningAbsent(Boolean tradeAccrualOnOpeningAbsent) {
		this.tradeAccrualOnOpeningAbsent = tradeAccrualOnOpeningAbsent;
	}


	public Boolean getAccrueAfterExDate() {
		return this.accrueAfterExDate;
	}


	public void setAccrueAfterExDate(Boolean accrueAfterExDate) {
		this.accrueAfterExDate = accrueAfterExDate;
	}


	public Boolean getFullPaymentForFirstPartialPeriod() {
		return this.fullPaymentForFirstPartialPeriod;
	}


	public void setFullPaymentForFirstPartialPeriod(Boolean fullPaymentForFirstPartialPeriod) {
		this.fullPaymentForFirstPartialPeriod = fullPaymentForFirstPartialPeriod;
	}


	public String getNotionalMultiplierRetriever() {
		return this.notionalMultiplierRetriever;
	}


	public void setNotionalMultiplierRetriever(String notionalMultiplierRetriever) {
		this.notionalMultiplierRetriever = notionalMultiplierRetriever;
	}


	public Boolean getNotionalMultiplierForPriceNotUsed() {
		return this.notionalMultiplierForPriceNotUsed;
	}


	public void setNotionalMultiplierForPriceNotUsed(Boolean notionalMultiplierForPriceNotUsed) {
		this.notionalMultiplierForPriceNotUsed = notionalMultiplierForPriceNotUsed;
	}


	public Boolean getNotionalMultiplierForAccrualNotUsed() {
		return this.notionalMultiplierForAccrualNotUsed;
	}


	public void setNotionalMultiplierForAccrualNotUsed(Boolean notionalMultiplierForAccrualNotUsed) {
		this.notionalMultiplierForAccrualNotUsed = notionalMultiplierForAccrualNotUsed;
	}


	public Boolean getIndexRatioAdjusted() {
		return this.indexRatioAdjusted;
	}


	public void setIndexRatioAdjusted(Boolean indexRatioAdjusted) {
		this.indexRatioAdjusted = indexRatioAdjusted;
	}


	public Boolean getIndexRatioCalculatedUsingMonthStartAfterPrevCoupon() {
		return this.indexRatioCalculatedUsingMonthStartAfterPrevCoupon;
	}


	public void setIndexRatioCalculatedUsingMonthStartAfterPrevCoupon(Boolean indexRatioCalculatedUsingMonthStartAfterPrevCoupon) {
		this.indexRatioCalculatedUsingMonthStartAfterPrevCoupon = indexRatioCalculatedUsingMonthStartAfterPrevCoupon;
	}


	public Integer getIndexRatioPrecision() {
		return this.indexRatioPrecision;
	}


	public void setIndexRatioPrecision(Integer indexRatioPrecision) {
		this.indexRatioPrecision = indexRatioPrecision;
	}


	public String getNameExpanded() {
		return this.nameExpanded;
	}


	public void setNameExpanded(String nameExpanded) {
		this.nameExpanded = nameExpanded;
	}


	public Short getFactorChangeEventTypeId() {
		return this.factorChangeEventTypeId;
	}


	public void setFactorChangeEventTypeId(Short factorChangeEventTypeId) {
		this.factorChangeEventTypeId = factorChangeEventTypeId;
	}


	public Short getCashLocationPurposeId() {
		return this.cashLocationPurposeId;
	}


	public void setCashLocationPurposeId(Short cashLocationPurposeId) {
		this.cashLocationPurposeId = cashLocationPurposeId;
	}


	public Short getCashLocationPurpose2Id() {
		return this.cashLocationPurpose2Id;
	}


	public void setCashLocationPurpose2Id(Short cashLocationPurpose2Id) {
		this.cashLocationPurpose2Id = cashLocationPurpose2Id;
	}


	public Boolean getIncludeAccrualReceivables() {
		return this.includeAccrualReceivables;
	}


	public void setIncludeAccrualReceivables(Boolean includeAccrualReceivables) {
		this.includeAccrualReceivables = includeAccrualReceivables;
	}
}
