package com.clifton.investment.setup.retriever;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;

import java.math.BigDecimal;
import java.util.Date;
import java.util.function.Supplier;


/**
 * The NotionalMultiplierRetriever interface defines the method for retrieving {@InvestmentSecurity} Notional Multiplier
 * used in Notional calculations.
 *
 * @author vgomelsky
 */
public interface NotionalMultiplierRetriever {


	/**
	 * Returns the Notional Multiplier for the specified security on the specified date.
	 * Some implementations use static value that does not change over time and ignore date supplier.
	 */
	public BigDecimal retrieveNotionalMultiplier(InvestmentSecurity security, Supplier<Date> dateSupplier, boolean exceptionIfMissing);


	/**
	 * Validates the bean for the given hierarchy
	 */
	public void validate(InvestmentInstrumentHierarchy hierarchy);
}
