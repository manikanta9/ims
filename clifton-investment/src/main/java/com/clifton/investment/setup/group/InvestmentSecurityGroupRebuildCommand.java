package com.clifton.investment.setup.group;

import com.clifton.core.util.status.Status;


/**
 * The <code>InvestmentSecurityGroupRebuildCommand</code> is a object that contains the information necessary to call the
 * {@link InvestmentSecurityGroupService} and rebuild all, just new or a given list of investment security groups
 *
 * @author JasonS
 */
public class InvestmentSecurityGroupRebuildCommand {

	private boolean limitRebuildForNewSecurityInsert;
	private Short[] securityGroupIds;
	private Status status;

	public static InvestmentSecurityGroupRebuildCommand forSystemManagedSecurityGroups(boolean limitRebuildForNewSecurityInsert, Short... securityGroupIds) {
		InvestmentSecurityGroupRebuildCommand command = new InvestmentSecurityGroupRebuildCommand();
		command.setLimitRebuildForNewSecurityInsert(limitRebuildForNewSecurityInsert);
		command.setSecurityGroupIds(securityGroupIds);
		command.setStatus(Status.ofEmptyMessage());
		return command;
	}

	public static InvestmentSecurityGroupRebuildCommand forSystemManagedSecurityGroupsWithStatus(boolean limitRebuildForNewSecurityInsert, Status status, Short... securityGroupIds) {
		InvestmentSecurityGroupRebuildCommand command = new InvestmentSecurityGroupRebuildCommand();
		command.setLimitRebuildForNewSecurityInsert(limitRebuildForNewSecurityInsert);
		command.setSecurityGroupIds(securityGroupIds);
		command.setStatus(status);
		return command;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////



	public boolean isLimitRebuildForNewSecurityInsert() {
		return this.limitRebuildForNewSecurityInsert;
	}


	public void setLimitRebuildForNewSecurityInsert(boolean limitRebuildForNewSecurityInsert) {
		this.limitRebuildForNewSecurityInsert = limitRebuildForNewSecurityInsert;
	}


	public Short[] getSecurityGroupIds() {
		return this.securityGroupIds;
	}


	public void setSecurityGroupIds(Short[] securityGroupIds) {
		this.securityGroupIds = securityGroupIds;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
