package com.clifton.investment.setup.group;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.setup.group.search.InvestmentGroupItemInstrumentSearchForm;
import com.clifton.investment.setup.group.search.InvestmentGroupItemSearchForm;
import com.clifton.investment.setup.group.search.InvestmentGroupMatrixSearchForm;
import com.clifton.investment.setup.group.search.InvestmentGroupSearchForm;
import com.clifton.investment.setup.group.search.InvestmentGroupTypeSearchForm;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * The <code>InvestmentGroupService</code> defines the methods for working with InvestmentGroup related objects
 *
 * @author manderson
 */
public interface InvestmentGroupService {

	//////////////////////////////////////////////////////////////////////////// 
	////////                     Lookup Methods                      /////////// 
	////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public boolean isInvestmentSecurityInGroup(final short groupId, final int securityId);


	@ResponseBody
	@SecureMethod(dtoClass = InvestmentGroup.class)
	public boolean isInvestmentSecurityInGroup(final String groupName, final int securityId);


	@DoNotAddRequestMapping
	public boolean isInvestmentInstrumentInGroup(final short groupId, final int instrumentId);


	@ResponseBody
	@SecureMethod(dtoClass = InvestmentGroup.class)
	public boolean isInvestmentInstrumentInGroup(final String groupName, final int instrumentId);


	public List<InvestmentGroup> getInvestmentGroupListForSecurity(final int securityId);


	//////////////////////////////////////////////////////////////////////////// 
	////////          Investment Group Business Methods              /////////// 
	////////////////////////////////////////////////////////////////////////////


	public InvestmentGroup getInvestmentGroup(short id);


	public InvestmentGroup getInvestmentGroupByName(String name);


	public List<InvestmentGroup> getInvestmentGroupList(InvestmentGroupSearchForm searchForm);


	public InvestmentGroup saveInvestmentGroup(InvestmentGroup bean);

	////////////////////////////////////////////////////////////////////////////
	////////          Investment Group Type Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentGroupType getInvestmentGroupType(short id);


	public InvestmentGroupType getInvestmentGroupTypeByName(String name);


	public List<InvestmentGroupType> getInvestmentGroupTypeList(InvestmentGroupTypeSearchForm searchForm);


	public InvestmentGroupType saveInvestmentGroupType(InvestmentGroupType bean);


	public void deleteInvestmentGroupType(short id);


	/**
	 * Create a copy of InvestmentGroup with the specified id using the specified name.
	 * The copy will have all group items and corresponding matrix assignments.
	 *
	 * @param id
	 * @param name
	 */
	public void copyInvestmentGroup(short id, String name);


	public void deleteInvestmentGroup(short id);


	//////////////////////////////////////////////////////////////////////////// 
	///////         Investment Group Item Business Methods             ///////// 
	////////////////////////////////////////////////////////////////////////////


	public InvestmentGroupItem getInvestmentGroupItem(int id);


	public List<InvestmentGroupItem> getInvestmentGroupItemListByGroup(short groupId);


	/**
	 * Used by observer to find matching group items that are needed to apply same inserts/updates/deletes
	 * Passes the bean, because checks properties and for updates need to pass the original bean
	 *
	 * @param groupItem
	 */
	@DoNotAddRequestMapping
	public List<InvestmentGroupItem> getInvestmentGroupItemUsingItemAsTemplate(InvestmentGroupItem groupItem);


	public List<InvestmentGroupItem> getInvestmentGroupItemList(InvestmentGroupItemSearchForm searchForm);


	public InvestmentGroupItem saveInvestmentGroupItem(InvestmentGroupItem bean);


	public void deleteInvestmentGroupItem(int id);


	/**
	 * Rebuilds Investment Group Items for groups that use a template
	 * NOTE: DOES A FULL REBUILD!
	 *
	 * @param groupId
	 */
	public void rebuildInvestmentGroupItemList(short groupId);


	//////////////////////////////////////////////////////////////////////////// 
	//////          Investment Group Matrix Business Methods              ////// 
	////////////////////////////////////////////////////////////////////////////


	public InvestmentGroupMatrix getInvestmentGroupMatrix(int id);


	public List<InvestmentGroupMatrix> getInvestmentGroupMatrixList(InvestmentGroupMatrixSearchForm searchForm);


	/**
	 * Used by observer to find matching group matrix that are needed to apply same inserts/updates/deletes
	 * Passes the bean, because checks properties and for updates need to pass the original bean
	 *
	 * @param matrix
	 */
	public List<InvestmentGroupMatrix> getInvestmentGroupMatrixUsingMatrixAsTemplate(InvestmentGroupMatrix matrix);


	public List<InvestmentGroupMatrix> getInvestmentGroupMatrixListByGroupItem(int groupItemId);


	public List<InvestmentGroupMatrix> getInvestmentGroupMatrixListByGroup(short groupId);


	public InvestmentGroupMatrix saveInvestmentGroupMatrix(InvestmentGroupMatrix bean);


	public void deleteInvestmentGroupMatrix(int id);


	public void deleteInvestmentGroupMatrixListByInstrument(int instrumentId);


	//////////////////////////////////////////////////////////////////////////// 
	/////     Investment Group Item Instrument Business Methods            ///// 
	////////////////////////////////////////////////////////////////////////////


	public List<InvestmentGroupItemInstrument> getInvestmentGroupItemInstrumentListByGroup(final short groupId);


	public List<InvestmentGroupItemInstrument> getInvestmentGroupItemInstrumentList(InvestmentGroupItemInstrumentSearchForm searchForm);


	/**
	 * Rebuilds the data held in the InvestmentGroupItemInstrument table
	 * for ALL groups, and saves changes, if any.
	 */
	public Status rebuildInvestmentGroupItemInstrumentList();


	/**
	 * Rebuilds the data held in the InvestmentGroupItemInstrument table
	 * for all system defined groups, and saves changes, if any.
	 */
	public Status rebuildInvestmentGroupItemInstrumentListBySystemDefined();


	/**
	 * Rebuilds the data held in the InvestmentGroupItemInstrument table
	 * for this specific group only and saves changes, if any.
	 */
	public void rebuildInvestmentGroupItemInstrumentListByGroup(short groupId);


	//////////////////////////////////////////////////////////////////////////// 
	///////   Investment Instrument - Group Related Business Methods   ///////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a list of {@link InvestmentInstrument} objects that are missing {@link InvestmentGroupItemInstrument}
	 * objects for the specified {@link InvestmentGroup}
	 *
	 * @param searchForm
	 * @param groupId
	 */
	public List<InvestmentInstrument> getInvestmentInstrumentMissingByGroupList(InstrumentSearchForm searchForm, final short groupId);
}
