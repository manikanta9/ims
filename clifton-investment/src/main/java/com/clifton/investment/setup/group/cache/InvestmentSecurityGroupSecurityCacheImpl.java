package com.clifton.investment.setup.group.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupSecurity;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentSecurityGroupSecurityCacheImpl</code> class provides caching and retrieval from cache of the list of {@InvestmentSecurity} objects
 * that belong to a {@link InvestmentSecurityGroup} by group id.
 * <p>
 * This class also clears the group cache when a security in that group is updated.  When {@link InvestmentSecurityGroupSecurity} objects are updated/added/removed
 * the cache is manually cleared from the service class since many groups are system managed and updated via SQL.
 *
 * @author manderson
 */
@Component
public class InvestmentSecurityGroupSecurityCacheImpl extends SelfRegisteringSimpleDaoCache<InvestmentSecurity, Short, List<InvestmentSecurity>> implements InvestmentSecurityGroupSecurityCache {

	private ReadOnlyDAO<InvestmentSecurityGroupSecurity> investmentSecurityGroupSecurityDAO;


	//////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentSecurity> getInvestmentSecurityList(Short groupId) {
		return getCacheHandler().get(getCacheName(), groupId);
	}


	@Override
	public void setInvestmentSecurityList(Short groupId, List<InvestmentSecurity> list) {
		getCacheHandler().put(getCacheName(), groupId, list);
	}


	@Override
	public void clearInvestmentSecurityList(Short groupId) {
		getCacheHandler().remove(getCacheName(), groupId);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentSecurity> dao, @SuppressWarnings("unused") DaoEventTypes event, InvestmentSecurity bean, Throwable e) {
		// if there's an exception, everything will be rolled back so no need to clear cache: would get "don't flush the Session after exception"
		if (e == null) {
			// Lookup Groups for that security and clear their cache
			List<InvestmentSecurityGroupSecurity> list = getInvestmentSecurityGroupSecurityDAO().findByField("referenceTwo.id", bean.getId());
			for (InvestmentSecurityGroupSecurity grp : CollectionUtils.getIterable(list)) {
				clearInvestmentSecurityList(grp.getReferenceOne().getId());
			}
		}
	}


	//////////////////////////////////////////////////////////////////////////// 
	////////              Getter and Setter Methods                  /////////// 
	////////////////////////////////////////////////////////////////////////////


	public ReadOnlyDAO<InvestmentSecurityGroupSecurity> getInvestmentSecurityGroupSecurityDAO() {
		return this.investmentSecurityGroupSecurityDAO;
	}


	public void setInvestmentSecurityGroupSecurityDAO(ReadOnlyDAO<InvestmentSecurityGroupSecurity> investmentSecurityGroupSecurityDAO) {
		this.investmentSecurityGroupSecurityDAO = investmentSecurityGroupSecurityDAO;
	}
}
