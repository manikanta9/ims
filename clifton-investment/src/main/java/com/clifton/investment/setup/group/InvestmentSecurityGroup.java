package com.clifton.investment.setup.group;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAware;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>InvestmentSecurityGroup</code> class defines a collection of InvestmentSecurity
 * objects: either manually created or dynamically rebuilt using rebuild SystemQuery.
 * <p>
 * Security groups can be used in various parts of the system and can define groups for
 * option PUTS, CALLS, Current Contract (for futures), etc.
 *
 * @author Mary Anderson
 */
@CacheByName
public class InvestmentSecurityGroup extends NamedEntity<Short> implements SystemEntityModifyConditionAware, EntityGroupRebuildAware {

	public static final String INVESTMENT_SECURITY_GROUP_TABLE_NAME = "InvestmentSecurityGroup";
	public static final String CLS_SETTLEMENT_CURRENCY_SECURITY_GROUP = "CLS Settlement Currency";

	////////////////////////////////////////////////////////////////////////////////

	private boolean systemDefined;
	private boolean securityFromSameInstrumentAllowed;

	/**
	 * A {@link SystemBean} that is used to rebuild the security group's InvestmentSecurity mapping
	 */
	private SystemBean rebuildSystemBean;
	/**
	 * Controls whether or not this group is immediately rebuilt by the {@link com.clifton.investment.instrument.InvestmentSecurityObserver} when a new
	 * {@link com.clifton.investment.instrument.InvestmentSecurity} is inserted.
	 * Validation logic does not allow setting this field to true unless the group is system managed (has a rebuild bean).
	 */
	private boolean rebuildOnNewSecurityInsert;

	/**
	 * For Non-Admin users - specific condition that must evaluate to true in order for a user to be able to edit this category, its hierarchies, or assignments
	 */
	private SystemCondition entityModifyCondition;


	////////////////////////////////////////////////////////////////////////////
	////////////         InvestmentSecurityGroup Methods           /////////////
	////////////////////////////////////////////////////////////////////////////



	/**
	 * System Managed means that users cannot add securities to the groups themselves
	 * it is maintained by executing the rebuildSystemBean.
	 */
	public boolean isSystemManaged() {
		return getRebuildSystemBean() != null;
	}


	@Override
	public String getRebuildTableName() {
		return "InvestmentSecurityGroupSecurity";
	}

	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSecurityFromSameInstrumentAllowed() {
		return this.securityFromSameInstrumentAllowed;
	}


	public void setSecurityFromSameInstrumentAllowed(boolean securityFromSameInstrumentAllowed) {
		this.securityFromSameInstrumentAllowed = securityFromSameInstrumentAllowed;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	@Override
	public SystemBean getRebuildSystemBean() {
		return this.rebuildSystemBean;
	}


	public void setRebuildSystemBean(SystemBean rebuildSystemBean) {
		this.rebuildSystemBean = rebuildSystemBean;
	}


	public boolean isRebuildOnNewSecurityInsert() {
		return this.rebuildOnNewSecurityInsert;
	}


	public void setRebuildOnNewSecurityInsert(boolean rebuildOnNewSecurityInsert) {
		this.rebuildOnNewSecurityInsert = rebuildOnNewSecurityInsert;
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}
}
