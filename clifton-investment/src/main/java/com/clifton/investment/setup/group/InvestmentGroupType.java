package com.clifton.investment.setup.group;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * @author MitchellF
 */
@CacheByName
public class InvestmentGroupType extends NamedEntity<Short> {

	private Integer levelsDeep;

	private InvestmentCurrencyTypes currencyType;

	private boolean template;
	private boolean basedOnTemplate;
	private boolean singleGroupItem;
	private boolean baseCurrencyRequired;
	private boolean missingNotAllowed;
	private boolean leafAssignmentsOnly;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getLevelsDeep() {
		return this.levelsDeep;
	}


	public void setLevelsDeep(Integer levelsDeep) {
		this.levelsDeep = levelsDeep;
	}


	public InvestmentCurrencyTypes getCurrencyType() {
		return this.currencyType;
	}


	public void setCurrencyType(InvestmentCurrencyTypes currencyType) {
		this.currencyType = currencyType;
	}


	public boolean isTemplate() {
		return this.template;
	}


	public void setTemplate(boolean template) {
		this.template = template;
	}


	public boolean isBasedOnTemplate() {
		return this.basedOnTemplate;
	}


	public void setBasedOnTemplate(boolean basedOnTemplate) {
		this.basedOnTemplate = basedOnTemplate;
	}


	public boolean isSingleGroupItem() {
		return this.singleGroupItem;
	}


	public void setSingleGroupItem(boolean singleGroupItem) {
		this.singleGroupItem = singleGroupItem;
	}


	public boolean isBaseCurrencyRequired() {
		return this.baseCurrencyRequired;
	}


	public void setBaseCurrencyRequired(boolean baseCurrencyRequired) {
		this.baseCurrencyRequired = baseCurrencyRequired;
	}


	public boolean isMissingNotAllowed() {
		return this.missingNotAllowed;
	}


	public void setMissingNotAllowed(boolean missingNotAllowed) {
		this.missingNotAllowed = missingNotAllowed;
	}


	public boolean isLeafAssignmentsOnly() {
		return this.leafAssignmentsOnly;
	}


	public void setLeafAssignmentsOnly(boolean leafAssignmentsOnly) {
		this.leafAssignmentsOnly = leafAssignmentsOnly;
	}
}
