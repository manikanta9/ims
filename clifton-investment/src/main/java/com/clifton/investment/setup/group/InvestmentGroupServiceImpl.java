package com.clifton.investment.setup.group;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.group.cache.InvestmentGroupInstrumentCache;
import com.clifton.investment.setup.group.search.InvestmentGroupItemInstrumentSearchForm;
import com.clifton.investment.setup.group.search.InvestmentGroupItemSearchForm;
import com.clifton.investment.setup.group.search.InvestmentGroupMatrixSearchForm;
import com.clifton.investment.setup.group.search.InvestmentGroupSearchForm;
import com.clifton.investment.setup.group.search.InvestmentGroupTypeSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentGroupServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class InvestmentGroupServiceImpl implements InvestmentGroupService {

	public static final String INVESTMENT_SECURITY_CURRENT_GROUP = "Current Securities";

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSetupService investmentSetupService;


	private AdvancedUpdatableDAO<InvestmentGroup, Criteria> investmentGroupDAO;
	private AdvancedUpdatableDAO<InvestmentGroupType, Criteria> investmentGroupTypeDAO;
	private AdvancedUpdatableDAO<InvestmentGroupItem, Criteria> investmentGroupItemDAO;
	private AdvancedUpdatableDAO<InvestmentGroupMatrix, Criteria> investmentGroupMatrixDAO;
	private AdvancedUpdatableDAO<InvestmentGroupItemInstrument, Criteria> investmentGroupItemInstrumentDAO;

	private DaoNamedEntityCache<InvestmentGroup> investmentGroupCache;
	private DaoNamedEntityCache<InvestmentGroupType> investmentGroupTypeCache;
	private InvestmentGroupInstrumentCache investmentGroupInstrumentCache;

	////////////////////////////////////////////////////////////////////////////
	////////                     Lookup Methods                      ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isInvestmentSecurityInGroup(final short groupId, final int securityId) {
		InvestmentGroup group = getInvestmentGroup(groupId);
		ValidationUtils.assertNotNull(group, "Cannot find Investment Group for id = " + groupId);
		return isInvestmentSecurityInGroup(group.getName(), securityId);
	}


	@Override
	public boolean isInvestmentSecurityInGroup(final String groupName, final int securityId) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);

		if (security == null) {
			return false;
		}

		return isInvestmentInstrumentInGroup(groupName, security.getInstrument().getId());
	}


	@Override
	public boolean isInvestmentInstrumentInGroup(final short groupId, final int instrumentId) {
		InvestmentGroup group = getInvestmentGroup(groupId);
		ValidationUtils.assertNotNull(group, "Cannot find Investment Group for id = " + groupId);
		return isInvestmentInstrumentInGroup(group.getName(), instrumentId);
	}


	@Override
	public boolean isInvestmentInstrumentInGroup(final String groupName, final int instrumentId) {
		return getInvestmentGroupInstrumentCache().isInvestmentInstrumentInGroup(groupName, instrumentId);
	}


	@Override
	public List<InvestmentGroup> getInvestmentGroupListForSecurity(final int securityId) {
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.createCriteria("itemList").createCriteria("instrumentList").createCriteria("referenceTwo").createCriteria("securityList", "s").add(Restrictions.eq("id", securityId));
		return getInvestmentGroupDAO().findBySearchCriteria(searchConfigurer);
	}

	////////////////////////////////////////////////////////////////////////////
	////////          Investment Group Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentGroup getInvestmentGroup(short id) {
		return getInvestmentGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentGroup getInvestmentGroupByName(String name) {
		// Note: This DOES NOT enforce that the group with the given name exists
		return getInvestmentGroupCache().getBeanForKeyValue(getInvestmentGroupDAO(), name);
	}


	@Override
	public List<InvestmentGroup> getInvestmentGroupList(final InvestmentGroupSearchForm searchForm) {
		HibernateSearchFormConfigurer searchFormConfigurer = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getMissingInstrumentId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(InvestmentGroupItemInstrument.class, "gii");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("referenceOne", "gi");

					sub.add(Restrictions.eq("referenceTwo.id", searchForm.getMissingInstrumentId()));
					sub.add(Restrictions.eqProperty("gi.group.id", criteria.getAlias() + ".id"));
					criteria.add(Subqueries.notExists(sub));
				}
			}
		};
		return getInvestmentGroupDAO().findBySearchCriteria(searchFormConfigurer);
	}


	@Override
	public InvestmentGroup saveInvestmentGroup(InvestmentGroup bean) {
		return getInvestmentGroupDAO().save(bean);
	}


	@Override
	@Transactional
	public void copyInvestmentGroup(short id, String name) {
		InvestmentGroup group = getInvestmentGroup(id);
		InvestmentGroup newGroup = BeanUtils.cloneBean(group, false, false);
		newGroup.setName(name);
		newGroup.setSystemDefined(false);

		copyInvestmentGroupImpl(group, newGroup);
	}


	private void copyInvestmentGroupImpl(InvestmentGroup group, InvestmentGroup newGroup) {
		List<InvestmentGroupItem> groupItemList = getInvestmentGroupItemListByGroup(group.getId());

		// Need the map because we can have hierarchical group items so we need to know which one the parent is
		Map<Integer, InvestmentGroupItem> oldIdToNewGroupItemMap = new HashMap<>();
		List<InvestmentGroupItem> newGroupItemList = new ArrayList<>();
		List<InvestmentGroupMatrix> newMatrixList = new ArrayList<>();
		for (InvestmentGroupItem gi : CollectionUtils.getIterable(groupItemList)) {
			copyInvestmentGroupItem(newGroup, newGroupItemList, newMatrixList, gi, oldIdToNewGroupItemMap);
		}

		saveInvestmentGroup(newGroup);
		getInvestmentGroupItemDAO().saveList(newGroupItemList);
		getInvestmentGroupMatrixDAO().saveList(newMatrixList);
	}


	private void copyInvestmentGroupItem(InvestmentGroup newGroup, List<InvestmentGroupItem> newGroupItemList, List<InvestmentGroupMatrix> newMatrixList, InvestmentGroupItem gi,
	                                     Map<Integer, InvestmentGroupItem> oldIdToNewGroupItemMap) {
		if (!oldIdToNewGroupItemMap.containsKey(gi.getId())) {
			InvestmentGroupItem newGI = BeanUtils.cloneBean(gi, false, false);
			newGI.setGroup(newGroup);

			if (gi.getParent() != null) {
				copyInvestmentGroupItem(newGroup, newGroupItemList, newMatrixList, gi.getParent(), oldIdToNewGroupItemMap);
				newGI.setParent(oldIdToNewGroupItemMap.get(gi.getParent().getId()));
			}
			oldIdToNewGroupItemMap.put(gi.getId(), newGI);
			newGroupItemList.add(newGI); // order of saving is important (parents first)

			// also copy and map matrices to new group items
			List<InvestmentGroupMatrix> matrixList = getInvestmentGroupMatrixListByGroupItem(gi.getId());
			if (!CollectionUtils.isEmpty(matrixList)) {
				for (InvestmentGroupMatrix matrix : matrixList) {
					InvestmentGroupMatrix newMatrix = BeanUtils.cloneBean(matrix, false, false);
					newMatrix.setGroupItem(newGI);
					newMatrixList.add(newMatrix);
				}
			}
		}
	}


	@Override
	public void deleteInvestmentGroup(short id) {
		getInvestmentGroupDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////          Investment Group Type Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentGroupType getInvestmentGroupType(short id) {
		return getInvestmentGroupTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentGroupType getInvestmentGroupTypeByName(String name) {
		return getInvestmentGroupTypeCache().getBeanForKeyValue(getInvestmentGroupTypeDAO(), name);
	}


	@Override
	public List<InvestmentGroupType> getInvestmentGroupTypeList(InvestmentGroupTypeSearchForm searchForm) {
		return getInvestmentGroupTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentGroupType saveInvestmentGroupType(InvestmentGroupType bean) {
		return getInvestmentGroupTypeDAO().save(bean);
	}


	@Override
	public void deleteInvestmentGroupType(short id) {
		getInvestmentGroupTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	///////         Investment Group Item Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentGroupItem getInvestmentGroupItem(int id) {
		return getInvestmentGroupItemDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentGroupItem> getInvestmentGroupItemListByGroup(short groupId) {
		return getInvestmentGroupItemDAO().findByField("group.id", groupId);
	}


	@Override
	public List<InvestmentGroupItem> getInvestmentGroupItemUsingItemAsTemplate(InvestmentGroupItem groupItem) {
		InvestmentGroupItemSearchForm searchForm = new InvestmentGroupItemSearchForm();
		searchForm.setGroupTemplateId(groupItem.getGroup().getId());
		searchForm.setNameEquals(groupItem.getName());

		List<InvestmentGroupItem> itemList = getInvestmentGroupItemList(searchForm);

		// Additional Filter - Filter on Expanded Name to ensure we have the correct items in case same item name is reused in a different location
		return BeanUtils.filter(itemList, InvestmentGroupItem::getNameExpanded, groupItem.getNameExpanded());
	}


	@Override
	public List<InvestmentGroupItem> getInvestmentGroupItemList(InvestmentGroupItemSearchForm searchForm) {
		return getInvestmentGroupItemDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentGroupItem saveInvestmentGroupItem(InvestmentGroupItem bean) {
		validateInvestmentGroupEditable(bean.getGroup());
		return getInvestmentGroupItemDAO().save(bean);
	}


	@Override
	public void deleteInvestmentGroupItem(int id) {
		InvestmentGroupItem item = getInvestmentGroupItem(id);
		if (item != null) {
			validateInvestmentGroupEditable(item.getGroup());
			getInvestmentGroupItemDAO().delete(id);
		}
	}


	@Override
	@Transactional
	public void rebuildInvestmentGroupItemList(short groupId) {
		InvestmentGroup group = getInvestmentGroup(groupId);
		ValidationUtils.assertNotNull(group.getTemplateInvestmentGroup(), "Rebuilding Investment Group Items only applies if the group has a template");
		// Remove everything for the group (if it exists)
		List<InvestmentGroupItemInstrument> instrumentList = getInvestmentGroupItemInstrumentListByGroup(groupId);
		getInvestmentGroupItemInstrumentDAO().deleteList(instrumentList);
		List<InvestmentGroupMatrix> matrixList = getInvestmentGroupMatrixListByGroup(groupId);
		getInvestmentGroupMatrixDAO().deleteList(matrixList);
		List<InvestmentGroupItem> itemList = getInvestmentGroupItemListByGroup(groupId);
		getInvestmentGroupItemDAO().deleteList(itemList);

		copyInvestmentGroupImpl(group.getTemplateInvestmentGroup(), group);
		rebuildInvestmentGroupItemInstrumentListByGroup(groupId);
	}

	////////////////////////////////////////////////////////////////////////////
	//////          Investment Group Matrix Business Methods              //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentGroupMatrix getInvestmentGroupMatrix(int id) {
		return getInvestmentGroupMatrixDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentGroupMatrix> getInvestmentGroupMatrixList(InvestmentGroupMatrixSearchForm searchForm) {
		return getInvestmentGroupMatrixDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<InvestmentGroupMatrix> getInvestmentGroupMatrixListByGroupItem(int groupItemId) {
		return getInvestmentGroupMatrixDAO().findByField("groupItem.id", groupItemId);
	}


	@Override
	public List<InvestmentGroupMatrix> getInvestmentGroupMatrixListByGroup(final short groupId) {
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.createAlias("groupItem", "gi").add(Restrictions.eq("gi.group.id", groupId));
		return getInvestmentGroupMatrixDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public List<InvestmentGroupMatrix> getInvestmentGroupMatrixUsingMatrixAsTemplate(InvestmentGroupMatrix matrix) {
		List<InvestmentGroupItem> itemList = getInvestmentGroupItemUsingItemAsTemplate(matrix.getGroupItem());
		List<InvestmentGroupMatrix> matrixList = new ArrayList<>();
		for (InvestmentGroupItem item : CollectionUtils.getIterable(itemList)) {
			List<InvestmentGroupMatrix> itemMatrixList = getInvestmentGroupMatrixListByGroupItem(item.getId());
			InvestmentGroupMatrix matchingMatrix = CollectionUtils.getOnlyElement(BeanUtils.filter(itemMatrixList, InvestmentGroupMatrix::getLabelWithTradingCurrency, matrix.getLabelWithTradingCurrency()));
			if (matchingMatrix != null) {
				matrixList.add(matchingMatrix);
			}
		}
		return matrixList;
	}


	@Override
	public InvestmentGroupMatrix saveInvestmentGroupMatrix(InvestmentGroupMatrix bean) {
		validateInvestmentGroupEditable(bean.getGroupItem().getGroup());
		return getInvestmentGroupMatrixDAO().save(bean);
	}


	@Override
	public void deleteInvestmentGroupMatrix(int id) {
		InvestmentGroupMatrix bean = getInvestmentGroupMatrix(id);
		if (bean != null) {
			validateInvestmentGroupEditable(bean.getGroupItem().getGroup());
			getInvestmentGroupMatrixDAO().delete(id);
		}
	}


	@Override
	@Transactional
	public void deleteInvestmentGroupMatrixListByInstrument(int instrumentId) {
		// If any explicit group matrix mappings - delete them
		getInvestmentGroupMatrixDAO().deleteList(getInvestmentGroupMatrixDAO().findByField("instrument.id", instrumentId));
		// Also delete from expanded table
		getInvestmentGroupItemInstrumentDAO().deleteList(getInvestmentGroupItemInstrumentDAO().findByField("referenceTwo.id", instrumentId));
	}


	private void validateInvestmentGroupEditable(InvestmentGroup group) {
		if (group.getTemplateInvestmentGroup() != null) {
			throw new ValidationException(
					"This group is using a template to build and maintain group items and assignments.  These can only be edited through the group that is being used as the template.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////     Investment Group Item Instrument Business Methods            /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentGroupItemInstrument> getInvestmentGroupItemInstrumentListByGroup(final short groupId) {
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.createAlias("referenceOne", "gi").add(Restrictions.eq("gi.group.id", groupId));
		return getInvestmentGroupItemInstrumentDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public List<InvestmentGroupItemInstrument> getInvestmentGroupItemInstrumentList(InvestmentGroupItemInstrumentSearchForm searchForm) {
		return getInvestmentGroupItemInstrumentDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public Status rebuildInvestmentGroupItemInstrumentList() {
		return rebuildInvestmentGroupItemInstrumentListImpl(getInvestmentGroupDAO().findAll());
	}


	@Override
	public Status rebuildInvestmentGroupItemInstrumentListBySystemDefined() {
		return rebuildInvestmentGroupItemInstrumentListImpl(getInvestmentGroupDAO().findByField("systemDefined", true));
	}


	private Status rebuildInvestmentGroupItemInstrumentListImpl(List<InvestmentGroup> groupList) {
		int count = 0;
		int failedCount = 0;
		Status statusToReturn = Status.ofEmptyMessage();
		for (InvestmentGroup group : CollectionUtils.getIterable(groupList)) {
			try {
				count++;
				rebuildInvestmentGroupItemInstrumentListByGroup(group.getId());
			}
			catch (Exception e) {
				failedCount++;
				String msg = StringUtils.NEW_LINE + "[" + group.getName() + "]: " + ExceptionUtils.getOriginalMessage(e);
				statusToReturn.addError(msg);
				LogUtils.errorOrInfo(getClass(), "Error rebuilding investment group " + group.getName(), e);
			}
		}
		statusToReturn.setMessage(count + " Total Groups. " + (count - failedCount) + " Successfully Rebuilt. Refer to status details for failed group rebuild details.");
		statusToReturn.setActionPerformed(count != 0);
		return statusToReturn;
	}


	@Override
	public void rebuildInvestmentGroupItemInstrumentListByGroup(short groupId) {
		List<InvestmentGroupItemInstrument> existingList = getInvestmentGroupItemInstrumentListByGroup(groupId);
		if (existingList == null) {
			existingList = new ArrayList<>();
		}

		Map<Integer, InvestmentGroupItem> instrumentGroupItemMap = new HashMap<>();
		List<InvestmentGroupItemInstrument> newList = new ArrayList<>();

		List<InvestmentGroupMatrix> matrixList = getExpandedInvestmentGroupMatrixListByGroup(groupId);
		InvestmentGroup group = getInvestmentGroup(groupId);
		for (InvestmentGroupMatrix matrix : CollectionUtils.getIterable(matrixList)) {
			// Verify where necessary no group duplications for investment instruments
			if (!group.isDuplicationAllowed()) {
				if (instrumentGroupItemMap.containsKey(matrix.getInstrument().getId())) {
					throw new ValidationException("Instrument [" + matrix.getInstrument().getLabel() + "] is tied to multiple items ["
							+ instrumentGroupItemMap.get(matrix.getInstrument().getId()).getNameExpanded() + ", " + matrix.getGroupItem().getNameExpanded() + "] for group [" + group.getName()
							+ "].  This group does not allow duplicates.");
				}
				instrumentGroupItemMap.put(matrix.getInstrument().getId(), matrix.getGroupItem());
			}

			InvestmentGroupItemInstrument item = new InvestmentGroupItemInstrument();
			item.setReferenceOne(matrix.getGroupItem());
			item.setReferenceTwo(matrix.getInstrument());
			if (!existingList.remove(item)) {
				// new item found
				newList.add(item);
			}
		}

		saveInvestmentGroupItemInstrumentLists(existingList, newList);
		getInvestmentGroupInstrumentCache().clearGroupCache(group.getName());
	}


	@Transactional
	protected void saveInvestmentGroupItemInstrumentLists(List<InvestmentGroupItemInstrument> deleteList, List<InvestmentGroupItemInstrument> saveList) {
		// Delete the items that no longer exist.
		getInvestmentGroupItemInstrumentDAO().deleteList(deleteList);
		// Add the new items
		getInvestmentGroupItemInstrumentDAO().saveList(saveList);
	}


	/**
	 * Returns a list of {@link InvestmentGroupMatrix} items expanded so that only the
	 * {@link InvestmentInstrument} values are populated.  If the original matrix item
	 * doesn't have an explicit instrument selected, the new matrix items have instruments
	 * that are determined by the {@link InvestmentInstrumentHierarchy}, includeSubHierarchies, and
	 * {@link InvestmentInstrument} currency fields.
	 */
	@Transactional(readOnly = true)
	protected List<InvestmentGroupMatrix> getExpandedInvestmentGroupMatrixListByGroup(short groupId) {
		List<InvestmentGroupMatrix> temporaryMatrixList = new ArrayList<>();

		List<InvestmentGroupMatrix> matrixList = getInvestmentGroupMatrixListByGroup(groupId);
		for (final InvestmentGroupMatrix matrix : CollectionUtils.getIterable(matrixList)) {
			InvestmentCurrencyTypes currencyType = matrix.getGroupItem().getCurrencyType();
			InvestmentSecurity baseCCY = matrix.getGroupItem().getGroup().getBaseCurrency();

			if (matrix.getInstrument() != null) {
				if (isInstrumentIncludedBasedOnCurrencyType(currencyType, baseCCY, matrix.getInstrument())) {
					temporaryMatrixList.add(matrix);
				}
			}
			else {
				InstrumentSearchForm searchForm = new InstrumentSearchForm();
				searchForm.setSkipDefaultSorting(true); // skip sorting to improve performance

				if (matrix.getHierarchy() != null) {
					if (matrix.isIncludeSubHierarchies()) {
						// Grab out id's for each hierarchy, this used to use BeanUtils.getPropertyValues but
						//  we need to have a Short[] and not Object[]
						List<InvestmentInstrumentHierarchy> hierarchyList = getInvestmentSetupService().getInvestmentInstrumentHierarchyListDeep(matrix.getHierarchy().getId());
						List<Short> hierarchyIds = new ArrayList<>();
						for (InvestmentInstrumentHierarchy hierarchy : CollectionUtils.getIterable(hierarchyList)) {
							hierarchyIds.add(hierarchy.getId());
						}

						searchForm.setHierarchyIds(hierarchyIds.toArray(new Short[0]));
					}
					else {
						searchForm.setHierarchyId(matrix.getHierarchy().getId());
					}
				}
				else if (matrix.getType() != null) {
					searchForm.setInvestmentTypeId(matrix.getType().getId());

					if (matrix.getSubType() != null) {
						searchForm.setInvestmentTypeSubTypeId(matrix.getSubType().getId());
					}
					if (matrix.getSubType2() != null) {
						searchForm.setInvestmentTypeSubType2Id(matrix.getSubType2().getId());
					}
				}
				if (matrix.getTradingCurrency() != null) {
					searchForm.setTradingCurrencyId(matrix.getTradingCurrency().getId());
				}

				if (matrix.getUltimateBusinessCompany() != null) {
					searchForm.setOneSecurityPerInstrument(true);
				}

				if (matrix.getCountryOfRisk() != null) {
					searchForm.setCountryOfRiskId(matrix.getCountryOfRisk().getId());
				}

				List<InvestmentInstrument> instrumentList = getInvestmentInstrumentService().getInvestmentInstrumentList(searchForm);

				for (InvestmentInstrument instrument : CollectionUtils.getIterable(instrumentList)) {
					if (isInstrumentIncludedBasedOnCurrencyType(currencyType, baseCCY, instrument) && validateUltimateBusinessCompany(matrix.getUltimateBusinessCompany(), instrument)) {
						InvestmentGroupMatrix bean = new InvestmentGroupMatrix();
						bean.setGroupItem(matrix.getGroupItem());
						bean.setInstrument(instrument);
						bean.setExcluded(matrix.isExcluded());
						temporaryMatrixList.add(bean);
					}
				}
			}
		}

		List<InvestmentGroupMatrix> includeMatrixList = BeanUtils.filter(temporaryMatrixList, groupMatrix -> !groupMatrix.isExcluded());
		List<InvestmentGroupMatrix> excludeMatrixList = BeanUtils.filter(temporaryMatrixList, InvestmentGroupMatrix::isExcluded);

		matrixList = new ArrayList<>();

		InvestmentGroup group = getInvestmentGroup(groupId);
		for (InvestmentGroupMatrix matrix : CollectionUtils.getIterable(includeMatrixList)) {
			// Skip Trading Disallowed Instruments if Tradable Only
			if (group.isTradableSecurityOnly() && matrix.getInstrument().getHierarchy().isTradingDisallowed()) {
				continue;
			}
			boolean excluded = false;
			for (InvestmentGroupMatrix exclude : CollectionUtils.getIterable(excludeMatrixList)) {
				if (matrix.getInstrument().equals(exclude.getInstrument()) && (matrix.getGroupItem().equals(exclude.getGroupItem()))) {
					// Include was explicit, then don't exclude
					if (matrix.getId() == null) {
						excluded = true;
						break;
					}
				}
			}
			if (!excluded) {
				matrixList.add(matrix);
			}
		}
		return matrixList;
	}


	private boolean isInstrumentIncludedBasedOnCurrencyType(InvestmentCurrencyTypes ccyType, InvestmentSecurity baseCCY, InvestmentInstrument instrument) {
		if (ccyType == null || (ccyType.isIncludeBaseCurrency() && ccyType.isIncludeForeignCurrency())) {
			return true;
		}
		if (baseCCY == null) {
			return true;
		}

		boolean foreign = InvestmentUtils.isInternationalInstrument(instrument, baseCCY);
		if (ccyType.isIncludeBaseCurrency()) {
			return !foreign;
		}
		if (ccyType.isIncludeForeignCurrency()) {
			return foreign;
		}
		return false;
	}


	/**
	 * Given a business company and a 1 to 1 instrument
	 * this method will validate if the ultimate issuer of the underlying security for that instrument
	 * is equal to the supplied ultimate business company
	 *
	 * @param ultimateBusinessCompany
	 * @param instrument
	 */
	private boolean validateUltimateBusinessCompany(BusinessCompany ultimateBusinessCompany, InvestmentInstrument instrument) {
		if (ultimateBusinessCompany == null) {
			return true;
		}
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurityByInstrument(instrument.getId());
		return security != null && security.getBusinessCompany() != null && security.getBusinessCompany().getRootParent().getId().equals(ultimateBusinessCompany.getId());
	}

	////////////////////////////////////////////////////////////////////////////
	///////   Investment Instrument - Group Related Business Methods   /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentInstrument> getInvestmentInstrumentMissingByGroupList(InstrumentSearchForm searchForm, final short groupId) {
		InvestmentGroup group = getInvestmentGroup(groupId);
		if (group.isTradableSecurityOnly()) {
			searchForm.setTradingDisallowed(false);
		}

		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				DetachedCriteria sub = DetachedCriteria.forClass(InvestmentGroupItemInstrument.class, "gii");
				sub.setProjection(Projections.property("id"));
				sub.createAlias("referenceOne", "gi");

				sub.add(Restrictions.eq("gi.group.id", groupId));
				sub.add(Restrictions.eqProperty("referenceTwo.id", criteria.getAlias() + ".id"));

				criteria.add(Subqueries.notExists(sub));
			}
		};
		return getInvestmentInstrumentService().getInvestmentInstrumentList(searchConfigurer);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////              Getter & Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentGroup, Criteria> getInvestmentGroupDAO() {
		return this.investmentGroupDAO;
	}


	public void setInvestmentGroupDAO(AdvancedUpdatableDAO<InvestmentGroup, Criteria> investmentGroupDAO) {
		this.investmentGroupDAO = investmentGroupDAO;
	}


	public AdvancedUpdatableDAO<InvestmentGroupType, Criteria> getInvestmentGroupTypeDAO() {
		return this.investmentGroupTypeDAO;
	}


	public void setInvestmentGroupTypeDAO(AdvancedUpdatableDAO<InvestmentGroupType, Criteria> investmentGroupTypeDAO) {
		this.investmentGroupTypeDAO = investmentGroupTypeDAO;
	}


	public AdvancedUpdatableDAO<InvestmentGroupItem, Criteria> getInvestmentGroupItemDAO() {
		return this.investmentGroupItemDAO;
	}


	public void setInvestmentGroupItemDAO(AdvancedUpdatableDAO<InvestmentGroupItem, Criteria> investmentGroupItemDAO) {
		this.investmentGroupItemDAO = investmentGroupItemDAO;
	}


	public AdvancedUpdatableDAO<InvestmentGroupItemInstrument, Criteria> getInvestmentGroupItemInstrumentDAO() {
		return this.investmentGroupItemInstrumentDAO;
	}


	public void setInvestmentGroupItemInstrumentDAO(AdvancedUpdatableDAO<InvestmentGroupItemInstrument, Criteria> investmentGroupItemInstrumentDAO) {
		this.investmentGroupItemInstrumentDAO = investmentGroupItemInstrumentDAO;
	}


	public AdvancedUpdatableDAO<InvestmentGroupMatrix, Criteria> getInvestmentGroupMatrixDAO() {
		return this.investmentGroupMatrixDAO;
	}


	public void setInvestmentGroupMatrixDAO(AdvancedUpdatableDAO<InvestmentGroupMatrix, Criteria> investmentGroupMatrixDAO) {
		this.investmentGroupMatrixDAO = investmentGroupMatrixDAO;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public InvestmentGroupInstrumentCache getInvestmentGroupInstrumentCache() {
		return this.investmentGroupInstrumentCache;
	}


	public void setInvestmentGroupInstrumentCache(InvestmentGroupInstrumentCache investmentGroupInstrumentCache) {
		this.investmentGroupInstrumentCache = investmentGroupInstrumentCache;
	}


	public DaoNamedEntityCache<InvestmentGroup> getInvestmentGroupCache() {
		return this.investmentGroupCache;
	}


	public void setInvestmentGroupCache(DaoNamedEntityCache<InvestmentGroup> investmentGroupCache) {
		this.investmentGroupCache = investmentGroupCache;
	}


	public DaoNamedEntityCache<InvestmentGroupType> getInvestmentGroupTypeCache() {
		return this.investmentGroupTypeCache;
	}


	public void setInvestmentGroupTypeCache(DaoNamedEntityCache<InvestmentGroupType> investmentGroupTypeCache) {
		this.investmentGroupTypeCache = investmentGroupTypeCache;
	}
}
