package com.clifton.investment.setup;


import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>InvestmentType</code> defines a type of an instrument: Stock, Future, Bond, etc.
 * This is the highest level of classification for investment securities:
 * Security => Instrument => Hierarchy => Type
 *
 * @author vgomelsky
 */
@CacheByName
public class InvestmentType extends NamedEntityWithoutLabel<Short> {

	public static final String BONDS = "Bonds";
	public static final String CURRENCY = "Currency";
	public static final String FORWARDS = "Forwards";
	public static final String FUTURES = "Futures";
	public static final String BENCHMARKS = "Benchmarks";
	public static final String OPTIONS = "Options";
	public static final String STOCKS = "Stocks";
	public static final String FUNDS = "Funds";
	public static final String NOTES = "Notes";
	public static final String SWAPS = "Swaps";
	public static final String SWAPTIONS = "Swaptions";
	public static final String INDEX = "Index";
	public static final String CASH = "Cash";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Used to differentiate physicals (Bonds, CCY, Funds, Notes, Stocks) from non-physicals/derivatives (i.e. Futures, Options)
	 * A physical securities is a non-derivative security that is paid with cash at open/close
	 */
	private boolean physicalSecurity;

	/**
	 * Industry standard name for "Quantity" (remaining quantity) field for securities of this type.
	 * For example, "Contracts" for Futures, "Shares" for Options, "Face" for bonds, etc.
	 * NOTE: these values can be overridden at Investment Sub Type and Investment Sub Type 2.
	 */
	private String quantityName;

	/**
	 * For securities with factor changes (ABS and CDS) it is the Original Face/Original Notional (Opening Quantity; not adjusted by current factor).
	 * For other securities it is the same as Remaining Quantity or Quantity (current number of units).
	 * NOTE: these values can be overridden at Investment Sub Type and Investment Sub Type 2.
	 */
	private String unadjustedQuantityName;

	// When applicable, define Investment Type specific terminology for the following fields.
	private String costFieldName;
	private String costBasisFieldName;
	private String notionalFieldName;
	private String marketValueFieldName;

	/**
	 * The decimal precision for the quantity on trades and positions.  For example, for Futures it would be
	 * 0 and Bonds would be 2.  This value can be overridden at Investment Sub Type or Investment Sub Type 2.
	 */
	private Short quantityDecimalPrecision;

	/**
	 * Specifies the number of days it may take to settle a security of this type.
	 * NOTE: this value can be overridden at Investment Hierarchy or security level (custom fields).
	 */
	private int daysToSettle;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getDaysToSettle() {
		return this.daysToSettle;
	}


	public void setDaysToSettle(int daysToSettle) {
		this.daysToSettle = daysToSettle;
	}


	public String getQuantityName() {
		return this.quantityName;
	}


	public void setQuantityName(String quantityName) {
		this.quantityName = quantityName;
	}


	public String getUnadjustedQuantityName() {
		return this.unadjustedQuantityName;
	}


	public void setUnadjustedQuantityName(String unadjustedQuantityName) {
		this.unadjustedQuantityName = unadjustedQuantityName;
	}


	public String getCostFieldName() {
		return this.costFieldName;
	}


	public void setCostFieldName(String costFieldName) {
		this.costFieldName = costFieldName;
	}


	public String getCostBasisFieldName() {
		return this.costBasisFieldName;
	}


	public void setCostBasisFieldName(String costBasisFieldName) {
		this.costBasisFieldName = costBasisFieldName;
	}


	public String getNotionalFieldName() {
		return this.notionalFieldName;
	}


	public void setNotionalFieldName(String notionalFieldName) {
		this.notionalFieldName = notionalFieldName;
	}


	public String getMarketValueFieldName() {
		return this.marketValueFieldName;
	}


	public void setMarketValueFieldName(String marketValueFieldName) {
		this.marketValueFieldName = marketValueFieldName;
	}


	public Short getQuantityDecimalPrecision() {
		return this.quantityDecimalPrecision;
	}


	public void setQuantityDecimalPrecision(Short quantityDecimalPrecision) {
		this.quantityDecimalPrecision = quantityDecimalPrecision;
	}


	public boolean isPhysicalSecurity() {
		return this.physicalSecurity;
	}


	public void setPhysicalSecurity(boolean physicalSecurity) {
		this.physicalSecurity = physicalSecurity;
	}
}
