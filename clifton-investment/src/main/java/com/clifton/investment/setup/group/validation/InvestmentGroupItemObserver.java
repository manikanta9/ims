package com.clifton.investment.setup.group.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupItem;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.search.InvestmentGroupSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>InvestmentGroupItemValidator</code> handles validating an {@link InvestmentGroupItem} prior to saving.
 *
 * @author manderson
 */
@Component
public class InvestmentGroupItemObserver extends SelfRegisteringDaoObserver<InvestmentGroupItem> {

	private InvestmentGroupService investmentGroupService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<InvestmentGroupItem> dao, DaoEventTypes event, InvestmentGroupItem bean) {
		if (event.isInsert() || event.isUpdate()) {
			InvestmentGroup group = bean.getGroup();

			// Call get original bean so we have it
			if (event.isUpdate()) {
				getOriginalBean(dao, bean);
			}

			// Only Validate if Not Using a Template
			if (group.getTemplateInvestmentGroup() == null) {
				if (group.getLevelsDeep() < bean.getLevel()) {
					throw new ValidationException("Items for group [" + group.getName() + "] can be a maximum of [" + group.getLevelsDeep() + "] levels deep.  This group item is [" + bean.getLevel()
							+ "] levels deep.");
				}

				// Validate if group IsLeafAssignmentOnly, that the parent is not an existing leaf with assignments
				if (group.isLeafAssignmentOnly() && (bean.getParent() != null && bean.getParent().getId() != null)) { // Verify parent.getId() is not null, not just parent because when we copy we set the parent, but it may not exist in the db yet
					if (!CollectionUtils.isEmpty(getInvestmentGroupService().getInvestmentGroupMatrixListByGroupItem(bean.getParent().getId()))) {
						throw new FieldValidationException("You cannot add a group item as a child of item [" + bean.getParent().getNameExpanded() + "] as it already has assignments and group ["
								+ bean.getGroup().getName() + "] only allows assignments to leaf items.", "parent.id");
					}
				}

				if (group.getInvestmentGroupType().getCurrencyType() != null) {
					ValidationUtils.assertEquals(group.getInvestmentGroupType().getCurrencyType(), bean.getCurrencyType(), "Currency type must be the same as Group Type currency type. Item currency type: [" + bean.getCurrencyType() + "], Group Type currency type: [" + group.getInvestmentGroupType().getCurrencyType() + "].");
				}

				if (group.getBaseCurrency() != null) {
					if (bean.isLeaf()) {
						ValidationUtils.assertNotNull(bean.getCurrencyType(), "Currency Type selection is required for leaf items when the group has a base currency defined.", "currencyType");
					}
					else {
						ValidationUtils.assertNull(bean.getCurrencyType(), "Currency Type selection is not allowed for non-leaf group items.", "currencyType");
					}
				}
				else if (group.getInvestmentGroupType().getCurrencyType() == null) {
					ValidationUtils.assertNull(bean.getCurrencyType(), "Currency Type selection is not allowed if the group does not have a base currency defined.", "currencyType");
				}

				// If the type is SingleGroupItem, make sure this is the only item
				if (group.getInvestmentGroupType().isSingleGroupItem()) {
					List<InvestmentGroupItem> otherItems = this.investmentGroupService.getInvestmentGroupItemListByGroup(group.getId());
					if (!CollectionUtils.isEmpty(otherItems)) {
						if (otherItems.size() > 1) {
							throw new ValidationException("Single Group Item cannot have more than one item.");
						}
						ValidationUtils.assertEquals(bean.getId(), CollectionUtils.getOnlyElement(otherItems).getId(), "SingleGroupItem group cannot contain more than one item.");
					}
				}
			}
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<InvestmentGroupItem> dao, DaoEventTypes event, InvestmentGroupItem bean, Throwable e) {
		if (e == null) {
			InvestmentGroup group = bean.getGroup();
			// If not using a template, check if being used as a template and if so, apply the same change to the groups that use this as its template
			if (group.getTemplateInvestmentGroup() == null) {
				if (event.isInsert() || event.isUpdate()) {
					List<InvestmentGroupItem> updateList = new ArrayList<>();
					List<InvestmentGroupItem> parentList = new ArrayList<>();
					if (bean.getParent() != null) {
						parentList = getInvestmentGroupService().getInvestmentGroupItemUsingItemAsTemplate(bean.getParent());
					}
					if (event.isInsert()) {
						InvestmentGroupSearchForm searchForm = new InvestmentGroupSearchForm();
						searchForm.setTemplateInvestmentGroupId(group.getId());
						List<InvestmentGroup> groupList = getInvestmentGroupService().getInvestmentGroupList(searchForm);
						for (InvestmentGroup childGroup : CollectionUtils.getIterable(groupList)) {
							InvestmentGroupItem newItem = BeanUtils.cloneBean(bean, false, false);
							newItem.setGroup(childGroup);
							if (bean.getParent() != null) {
								newItem.setParent(CollectionUtils.getFirstElement(BeanUtils.filter(parentList, InvestmentGroupItem::getGroup, childGroup)));
							}
							updateList.add(newItem);
						}
					}
					else if (event.isUpdate()) {
						// Get the matching based on original bean
						InvestmentGroupItem originalBean = getOriginalBean(dao, bean);
						if (originalBean != null) {
							List<InvestmentGroupItem> itemList = getInvestmentGroupService().getInvestmentGroupItemUsingItemAsTemplate(originalBean);
							for (InvestmentGroupItem item : CollectionUtils.getIterable(itemList)) {
								InvestmentGroupItem newItem = BeanUtils.cloneBean(bean, false, false);
								newItem.setGroup(item.getGroup());
								newItem.setId(item.getId()); // Use Same ID for updates

								if (bean.getParent() != null) {
									if (!bean.getParent().getNameExpanded().equals(item.getParent().getNameExpanded())) {
										newItem.setParent(CollectionUtils.getFirstElement(BeanUtils.filter(parentList, InvestmentGroupItem::getGroup, item.getGroup())));
									}
									else {
										newItem.setParent(item.getParent());
									}
								}
								updateList.add(newItem);
							}
						}
					}

					if (!CollectionUtils.isEmpty(updateList)) {
						((UpdatableDAO<InvestmentGroupItem>) dao).saveList(updateList);
					}
				}
				else { // Deletes
					List<InvestmentGroupItem> itemList = getInvestmentGroupService().getInvestmentGroupItemUsingItemAsTemplate(bean);
					((UpdatableDAO<InvestmentGroupItem>) dao).deleteList(itemList);
				}
			}
		}
	}


	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}
}
