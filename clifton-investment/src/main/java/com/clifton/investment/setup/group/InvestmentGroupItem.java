package com.clifton.investment.setup.group;


import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>InvestmentGroupItem</code> is a hierarchical level within a specified
 * {@link InvestmentGroup} object.  As with Investment Groups, Group items are used to classify
 * {@link InvestmentInstrument}s into asset classes.
 *
 * @author manderson
 */
public class InvestmentGroupItem extends NamedHierarchicalEntity<InvestmentGroupItem, Integer> implements SystemEntityModifyConditionAware {

	private InvestmentGroup group;

	/**
	 * Optional group item type which could be used in UI/Reporting to show different columns for different types, etc.
	 */
	private String itemType;
	private int order;

	private InvestmentCurrencyTypes currencyType;

	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	@Override
	public Integer getMaxDepth() {
		return getGroup().getLevelsDeep();
	}


	public InvestmentGroup getGroup() {
		return this.group;
	}


	public void setGroup(InvestmentGroup group) {
		this.group = group;
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return (getGroup() == null) ? null : getGroup().getEntityModifyCondition();
	}


	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}


	public String getItemType() {
		return this.itemType;
	}


	public void setItemType(String itemType) {
		this.itemType = itemType;
	}


	public InvestmentCurrencyTypes getCurrencyType() {
		return this.currencyType;
	}


	public void setCurrencyType(InvestmentCurrencyTypes currencyType) {
		this.currencyType = currencyType;
	}
}
