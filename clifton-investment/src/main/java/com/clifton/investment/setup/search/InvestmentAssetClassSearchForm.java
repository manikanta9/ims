package com.clifton.investment.setup.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>InvestmentAssetClassSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class InvestmentAssetClassSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField
	private Boolean cash;

	@SearchField
	private Boolean mainCash;

	@SearchField
	private Boolean master;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String name;

	@SearchField(searchFieldPath = "parent", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String parentName;

	@SearchField(searchField = "masterAssetClass.id", sortField = "masterAssetClass.name")
	private Short masterAssetClassId;

	@SearchField(searchField = "parent.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullParent;

	@SearchField(searchField = "accountAssetClassList.account.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer accountId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getCash() {
		return this.cash;
	}


	public void setCash(Boolean cash) {
		this.cash = cash;
	}


	public Boolean getMainCash() {
		return this.mainCash;
	}


	public void setMainCash(Boolean mainCash) {
		this.mainCash = mainCash;
	}


	public Integer getAccountId() {
		return this.accountId;
	}


	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getParentName() {
		return this.parentName;
	}


	public void setParentName(String parentName) {
		this.parentName = parentName;
	}


	public Boolean getNullParent() {
		return this.nullParent;
	}


	public void setNullParent(Boolean nullParent) {
		this.nullParent = nullParent;
	}


	public Short getMasterAssetClassId() {
		return this.masterAssetClassId;
	}


	public void setMasterAssetClassId(Short masterAssetClassId) {
		this.masterAssetClassId = masterAssetClassId;
	}


	public Boolean getMaster() {
		return this.master;
	}


	public void setMaster(Boolean master) {
		this.master = master;
	}
}
