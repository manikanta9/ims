package com.clifton.investment.setup.group.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;


/**
 * The <code>InvestmentSecurityGroupSearchForm</code> ...
 *
 * @author manderson
 */
public class InvestmentSecurityGroupSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField(searchField = "name,description", sortField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Boolean systemDefined;

	@SearchField(searchField = "id")
	private Short[] ids;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Short[] excludeIds;

	@SearchField(searchField = "securityList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer securityId;

	@SearchField(searchField = "rebuildSystemBean.id", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean systemManaged;

	@SearchField
	private Boolean rebuildOnNewSecurityInsert;

	@SearchField
	private Boolean securityFromSameInstrumentAllowed;

	@SearchField(searchField = "name", searchFieldPath = "entityModifyCondition")
	private String entityModifyConditionName;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return InvestmentSecurityGroup.INVESTMENT_SECURITY_GROUP_TABLE_NAME;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Boolean getRebuildOnNewSecurityInsert() {
		return this.rebuildOnNewSecurityInsert;
	}


	public void setRebuildOnNewSecurityInsert(Boolean rebuildOnNewSecurityInsert) {
		this.rebuildOnNewSecurityInsert = rebuildOnNewSecurityInsert;
	}


	public Boolean getSecurityFromSameInstrumentAllowed() {
		return this.securityFromSameInstrumentAllowed;
	}


	public void setSecurityFromSameInstrumentAllowed(Boolean securityFromSameInstrumentAllowed) {
		this.securityFromSameInstrumentAllowed = securityFromSameInstrumentAllowed;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Short[] getIds() {
		return this.ids;
	}


	public void setIds(Short[] ids) {
		this.ids = ids;
	}


	public Short[] getExcludeIds() {
		return this.excludeIds;
	}


	public void setExcludeIds(Short[] excludeIds) {
		this.excludeIds = excludeIds;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public String getEntityModifyConditionName() {
		return this.entityModifyConditionName;
	}


	public void setEntityModifyConditionName(String entityModifyConditionName) {
		this.entityModifyConditionName = entityModifyConditionName;
	}


	public Boolean getSystemManaged() {
		return this.systemManaged;
	}


	public void setSystemManaged(Boolean systemManaged) {
		this.systemManaged = systemManaged;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
