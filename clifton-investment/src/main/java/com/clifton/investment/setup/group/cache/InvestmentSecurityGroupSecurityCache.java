package com.clifton.investment.setup.group.cache;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupSecurity;

import java.util.List;


/**
 * The <code>InvestmentSecurityGroupSecurityCache</code> class provides caching and retrieval from cache of the list of {@InvestmentSecurity} objects
 * that belong to a {@link InvestmentSecurityGroup} by group id.
 * <p>
 * This class also clears the group cache when a security in that group is updated.  When {@link InvestmentSecurityGroupSecurity} objects are updated/added/removed
 * the cache is manually cleared from the service class since many groups are system managed and updated via SQL.
 *
 * @author manderson
 */
public interface InvestmentSecurityGroupSecurityCache {


	public List<InvestmentSecurity> getInvestmentSecurityList(Short groupId);


	public void setInvestmentSecurityList(Short groupId, List<InvestmentSecurity> list);


	public void clearInvestmentSecurityList(Short groupId);
}
