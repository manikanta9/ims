package com.clifton.investment.setup.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.setup.InvestmentTypeSubType;


@SearchForm(ormDtoClass = InvestmentTypeSubType.class)
public class InvestmentSubTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField
	private String description;

	@SearchField(searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField(searchField = "investmentType.id")
	private Short[] investmentTypeIds;

	@SearchField
	private String quantityName;

	@SearchField
	private String unadjustedQuantityName;

	@SearchField
	private Short quantityDecimalPrecision;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short[] getInvestmentTypeIds() {
		return this.investmentTypeIds;
	}


	public void setInvestmentTypeIds(Short[] investmentTypeIds) {
		this.investmentTypeIds = investmentTypeIds;
	}


	public String getQuantityName() {
		return this.quantityName;
	}


	public void setQuantityName(String quantityName) {
		this.quantityName = quantityName;
	}


	public String getUnadjustedQuantityName() {
		return this.unadjustedQuantityName;
	}


	public void setUnadjustedQuantityName(String unadjustedQuantityName) {
		this.unadjustedQuantityName = unadjustedQuantityName;
	}


	public Short getQuantityDecimalPrecision() {
		return this.quantityDecimalPrecision;
	}


	public void setQuantityDecimalPrecision(Short quantityDecimalPrecision) {
		this.quantityDecimalPrecision = quantityDecimalPrecision;
	}
}
