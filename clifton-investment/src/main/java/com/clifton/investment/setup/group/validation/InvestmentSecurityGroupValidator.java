package com.clifton.investment.setup.group.validation;


import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupSecurity;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>InvestmentSecurityGroupValidator</code> ...
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentSecurityGroupValidator extends SelfRegisteringDaoValidator<InvestmentSecurityGroup> {

	private AdvancedReadOnlyDAO<InvestmentSecurityGroupSecurity, Criteria> investmentSecurityGroupSecurityDAO;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	/////////        InvestmentSecurityGroupValidator Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(InvestmentSecurityGroup bean, DaoEventTypes config) throws ValidationException {
		if (bean.isSystemDefined()) {
			if (config.isDelete()) {
				throw new ValidationException("Deleting System Defined Investment Security Groups is not allowed.");
			}
			if (config.isInsert()) {
				throw new ValidationException("Adding new System Defined Investment Security Groups is not allowed.");
			}
		}
		if (config.isUpdate()) {
			InvestmentSecurityGroup original = getOriginalBean(bean);
			if (original.isSystemDefined() != bean.isSystemDefined()) {
				throw new FieldValidationException("You cannot edit the System Defined field for Investment Security Groups", "systemDefined");
			}
			if (original.isSystemDefined()) {
				ValidationUtils.assertTrue(original.getName().equals(bean.getName()), "System Defined Investment Security Group Names cannot be changed.", "name");
			}
			// Was allowing duplicate assignments, but now not going to - need to make sure there are not duplicate assignments already
			if (original.isSecurityFromSameInstrumentAllowed() && !bean.isSecurityFromSameInstrumentAllowed()) {
				List<String> list = getDuplicateInstrumentsForGroup(bean.getId());
				if (!CollectionUtils.isEmpty(list)) {
					throw new FieldValidationException(
							"Cannot change this group to not allow more than one security per instrument because the following instrument(s) currently have multiple securities assigned ["
									+ StringUtils.collectionToCommaDelimitedString(list) + "]", "securityFromSameInstrumentAllowed");
				}
			}
		}
		if (config.isInsert() || config.isUpdate()) {
			if (bean.isSystemManaged()) {
				SystemBean rebuildBean = bean.getRebuildSystemBean();
				if (rebuildBean != null) {
					((EntityGroupRebuildAwareExecutor) getSystemBeanService().getBeanInstance(rebuildBean)).validate(bean);
				}
			}
			else {
				ValidationUtils.assertFalse(bean.isRebuildOnNewSecurityInsert(), "Investment Security Group must have a Rebuild Bean in order to rebuild on new security inserts.");
			}
		}
	}


	private List<String> getDuplicateInstrumentsForGroup(final short groupId) {
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			criteria.createAlias("referenceTwo", "so");
			criteria.createAlias("so.instrument", "io");
			criteria.add(Restrictions.eq("referenceOne.id", groupId));

			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentSecurityGroupSecurity.class, "gs");
			sub.setProjection(Projections.property("id"));
			sub.createAlias("referenceTwo", "si");
			sub.createAlias("si.instrument", "ii");
			sub.add(Restrictions.eqProperty("ii.id", "io.id"));
			sub.add(Restrictions.neProperty("si.id", "so.id"));
			sub.add(Restrictions.eq("referenceOne.id", groupId));
			criteria.add(Subqueries.exists(sub));
		};
		List<InvestmentSecurityGroupSecurity> list = getInvestmentSecurityGroupSecurityDAO().findBySearchCriteria(searchConfigurer);
		List<String> instrumentLabels = new ArrayList<>();
		for (InvestmentSecurityGroupSecurity gs : CollectionUtils.getIterable(list)) {
			if (!instrumentLabels.contains(gs.getReferenceTwo().getInstrument().getLabel())) {
				instrumentLabels.add(gs.getReferenceTwo().getInstrument().getLabel());
			}
		}
		return instrumentLabels;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedReadOnlyDAO<InvestmentSecurityGroupSecurity, Criteria> getInvestmentSecurityGroupSecurityDAO() {
		return this.investmentSecurityGroupSecurityDAO;
	}


	public void setInvestmentSecurityGroupSecurityDAO(AdvancedReadOnlyDAO<InvestmentSecurityGroupSecurity, Criteria> investmentSecurityGroupSecurityDAO) {
		this.investmentSecurityGroupSecurityDAO = investmentSecurityGroupSecurityDAO;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
