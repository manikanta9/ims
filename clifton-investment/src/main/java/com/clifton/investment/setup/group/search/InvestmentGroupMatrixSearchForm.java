package com.clifton.investment.setup.group.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>InvestmentGroupMatrixSearchForm</code> class defines search configuration for InvestmentGroupMatrix objects
 *
 * @author vgomelsky
 */
public class InvestmentGroupMatrixSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "groupItem.id")
	private Integer groupItemId;

	@SearchField(searchField = "hierarchy.id")
	private Short hierarchyId;
	@SearchField
	private Boolean includeSubHierarchies;

	@SearchField(searchField = "instrument.id")
	private Integer instrumentId;

	@SearchField(searchField = "type.id")
	private Short typeId;

	@SearchField(searchField = "subType.id")
	private Short subTypeId;

	@SearchField(searchField = "subType2.id")
	private Short subType2Id;

	@SearchField(searchField = "tradingCurrency.id")
	private Integer tradingCurrencyId;

	@SearchField(searchField = "ultimateBusinessCompany.id")
	private Integer ultimateBusinessCompanyId;


	@SearchField
	private Boolean excluded;


	public Integer getGroupItemId() {
		return this.groupItemId;
	}


	public void setGroupItemId(Integer groupItemId) {
		this.groupItemId = groupItemId;
	}


	public Short getHierarchyId() {
		return this.hierarchyId;
	}


	public void setHierarchyId(Short hierarchyId) {
		this.hierarchyId = hierarchyId;
	}


	public Boolean getIncludeSubHierarchies() {
		return this.includeSubHierarchies;
	}


	public void setIncludeSubHierarchies(Boolean includeSubHierarchies) {
		this.includeSubHierarchies = includeSubHierarchies;
	}


	public Integer getInstrumentId() {
		return this.instrumentId;
	}


	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}


	public Integer getTradingCurrencyId() {
		return this.tradingCurrencyId;
	}


	public void setTradingCurrencyId(Integer tradingCurrencyId) {
		this.tradingCurrencyId = tradingCurrencyId;
	}


	public Boolean getExcluded() {
		return this.excluded;
	}


	public void setExcluded(Boolean excluded) {
		this.excluded = excluded;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public Short getSubTypeId() {
		return this.subTypeId;
	}


	public void setSubTypeId(Short subTypeId) {
		this.subTypeId = subTypeId;
	}


	public Short getSubType2Id() {
		return this.subType2Id;
	}


	public void setSubType2Id(Short subType2Id) {
		this.subType2Id = subType2Id;
	}


	public Integer getUltimateBusinessCompanyId() {
		return this.ultimateBusinessCompanyId;
	}


	public void setUltimateBusinessCompanyId(Integer ultimateBusinessCompanyId) {
		this.ultimateBusinessCompanyId = ultimateBusinessCompanyId;
	}
}
