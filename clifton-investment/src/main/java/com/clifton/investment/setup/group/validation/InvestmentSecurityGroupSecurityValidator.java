package com.clifton.investment.setup.group.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.setup.group.InvestmentSecurityGroupSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentSecurityGroupSecurityValidator</code> ...
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentSecurityGroupSecurityValidator extends SelfRegisteringDaoValidator<InvestmentSecurityGroupSecurity> {

	private InvestmentSecurityGroupService investmentSecurityGroupService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentSecurityGroupSecurity bean, DaoEventTypes config) throws ValidationException {
		if (!bean.getReferenceOne().isSecurityFromSameInstrumentAllowed()) {
			InvestmentSecurityGroupSecurity original = null;
			if (config.isUpdate()) {
				original = getOriginalBean(bean);
			}

			if (original == null || (!original.getReferenceOne().equals(bean.getReferenceOne()) || !original.getReferenceTwo().equals(bean.getReferenceTwo()))) {
				List<InvestmentSecurityGroupSecurity> list = getInvestmentSecurityGroupService().getInvestmentSecurityGroupSecurityListByInstrument(bean.getReferenceOne().getId(),
						bean.getReferenceTwo().getInstrument().getId());
				if (!CollectionUtils.isEmpty(list)) {
					// Should Only Be One - If not - something already failed validation and we need to know this
					InvestmentSecurityGroupSecurity existing = CollectionUtils.getOnlyElement(list);
					if (original == null || (existing != null && !existing.equals(bean))) {
						throw new ValidationException("The security instrument [" + bean.getReferenceTwo().getInstrument().getLabel()
								+ "] is already assigned to this group.  According to the group's setup, only one security per instrument is allowed");
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////////             Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}
}
