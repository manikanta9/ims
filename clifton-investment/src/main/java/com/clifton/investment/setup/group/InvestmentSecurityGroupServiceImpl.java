package com.clifton.investment.setup.group;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.group.cache.InvestmentSecurityGroupSecurityCache;
import com.clifton.investment.setup.group.search.InvestmentSecurityGroupSearchForm;
import com.clifton.investment.setup.group.search.InvestmentSecurityGroupSecuritySearchForm;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * The <code>InvestmentSecurityGroupServiceImpl</code> maintains the implementation for the security group service.
 * It has methods for working with investment security groups.
 *
 * @author apopp
 */
@Service
public class InvestmentSecurityGroupServiceImpl implements InvestmentSecurityGroupService {

	public static final String INVESTMENT_SECURITY_CURRENT_GROUP = "Current Securities";

	private AdvancedUpdatableDAO<InvestmentSecurityGroup, Criteria> investmentSecurityGroupDAO;
	private AdvancedUpdatableDAO<InvestmentSecurityGroupSecurity, Criteria> investmentSecurityGroupSecurityDAO;

	private InvestmentInstrumentService investmentInstrumentService;
	private DaoNamedEntityCache<InvestmentSecurityGroup> investmentSecurityGroupCache;
	private InvestmentSecurityGroupSecurityCache investmentSecurityGroupSecurityCache;
	private SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentSecurity> getInvestmentSecurityListForSecurityGroup(short securityGroupId, Integer instrumentId, Date activeOnDate) {
		List<InvestmentSecurity> list = getInvestmentSecurityListForSecurityGroup(securityGroupId);

		// Filter on instrument first if possible since should narrow down the list the most
		if (instrumentId != null) {
			list = BeanUtils.filter(list, investmentSecurity -> investmentSecurity.getInstrument().getId(), instrumentId);
		}
		return filterInvestmentSecurityListByActiveOnDate(list, activeOnDate);
	}


	private List<InvestmentSecurity> getInvestmentSecurityListForSecurityGroup(short securityGroupId) {
		List<InvestmentSecurity> list = getInvestmentSecurityGroupSecurityCache().getInvestmentSecurityList(securityGroupId);
		if (list == null) {
			SecuritySearchForm searchForm = new SecuritySearchForm();
			searchForm.setInvestmentSecurityGroupId(securityGroupId);
			list = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);

			// If null set as an empty list so when cache we don't try to look it up again
			if (list == null) {
				list = new ArrayList<>();
			}
			getInvestmentSecurityGroupSecurityCache().setInvestmentSecurityList(securityGroupId, list);
		}
		return list;
	}


	private List<InvestmentSecurity> filterInvestmentSecurityListByActiveOnDate(List<InvestmentSecurity> list, Date activeOnDate) {
		if (CollectionUtils.isEmpty(list) || activeOnDate == null) {
			return list;
		}
		List<InvestmentSecurity> result = new ArrayList<>();
		for (InvestmentSecurity bean : list) {
			if (DateUtils.isDateBetween(activeOnDate, bean.getStartDate(), bean.getEndDate(), false)) {
				result.add(bean);
			}
		}
		return result;
	}


	@Override
	public List<InvestmentSecurity> getInvestmentSecurityCurrentList(Integer instrumentId, Date activeOnDate) {
		return getInvestmentSecurityListForSecurityGroup(getInvestmentSecurityGroupByName(INVESTMENT_SECURITY_CURRENT_GROUP).getId(), instrumentId, activeOnDate);
	}


	@Override
	public InvestmentSecurity getInvestmentSecurityCurrentByInstrument(int instrumentId, Date activeOnDate, boolean existsInGroupOnly) {
		if (activeOnDate == null) {
			activeOnDate = new Date();
		}
		InvestmentSecurity current = CollectionUtils.getFirstElement(getInvestmentSecurityCurrentList(instrumentId, activeOnDate));
		if (current == null && !existsInGroupOnly) {
			// Get all active securities for this instrument
			SecuritySearchForm searchForm = new SecuritySearchForm();
			searchForm.setActiveOnDate(activeOnDate);
			searchForm.setInstrumentId(instrumentId);
			List<InvestmentSecurity> securities = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
			securities = BeanUtils.sortWithFunction(securities, InvestmentSecurity::getEndDate, true);
			current = CollectionUtils.getFirstElement(securities);
		}
		return current;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////        Investment Security Group Methods             ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isInvestmentSecurityInSecurityGroup(int securityId, short securityGroupId) {
		List<InvestmentSecurity> list = getInvestmentSecurityListForSecurityGroup(securityGroupId);
		for (InvestmentSecurity security : CollectionUtils.getIterable(list)) {
			if (security.getId() == securityId) {
				return true;
			}
		}
		return false;
	}


	@Override
	public boolean isInvestmentSecurityInSecurityGroup(int securityId, String securityGroupName) {
		InvestmentSecurityGroup investmentSecurityGroup = getInvestmentSecurityGroupByName(securityGroupName);
		if (investmentSecurityGroup != null) {
			return isInvestmentSecurityInSecurityGroup(securityId, investmentSecurityGroup.getId());
		}
		return false;
	}


	@Override
	public InvestmentSecurityGroup getInvestmentSecurityGroup(short id) {
		return getInvestmentSecurityGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentSecurityGroup getInvestmentSecurityGroupByName(String name) {
		return getInvestmentSecurityGroupCache().getBeanForKeyValueStrict(getInvestmentSecurityGroupDAO(), name);
	}


	@Override
	public List<InvestmentSecurityGroup> getInvestmentSecurityGroupList(final InvestmentSecurityGroupSearchForm searchForm) {
		return getInvestmentSecurityGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentSecurityGroup saveInvestmentSecurityGroup(InvestmentSecurityGroup bean) {
		return getInvestmentSecurityGroupDAO().save(bean);
	}


	@Override
	public void deleteInvestmentSecurityGroup(short id) {
		getInvestmentSecurityGroupDAO().delete(id);
	}


	@Override
	public Status rebuildInvestmentSecurityGroup(short groupId) {
		Status status = Status.ofMessage("Rebuilding Security Group with an ID of " + groupId);
		rebuildInvestmentSecurityGroupImpl(status, getInvestmentSecurityGroup(groupId));
		return status;
	}


	private Status rebuildInvestmentSecurityGroupImpl(Status status, InvestmentSecurityGroup group) {
		ValidationUtils.assertNotNull(group, "Investment Security Group is required in order to rebuild.");
		ValidationUtils.assertTrue(group.isSystemManaged(), "Investment Security Group [" + group.getName() + "] is not system managed.  Securities are manually maintained and cannot be automatically rebuilt.");
		EntityGroupRebuildAwareExecutor rebuildBean = (EntityGroupRebuildAwareExecutor) getSystemBeanService().getBeanInstance(group.getRebuildSystemBean());
		rebuildBean.executeRebuild(group, status);
		getInvestmentSecurityGroupSecurityCache().clearInvestmentSecurityList(group.getId());
		status.setMessage("Rebuilt Security Group - " + group.getName());
		return status;
	}


	@Override
	public Status rebuildInvestmentSecurityGroupSystemManaged(InvestmentSecurityGroupRebuildCommand command) {
		if (command.getStatus() == null) {
			command.setStatus(Status.ofEmptyMessage());
		}

		if (ArrayUtils.isEmpty(command.getSecurityGroupIds())) {
			InvestmentSecurityGroupSearchForm groupSearchForm = new InvestmentSecurityGroupSearchForm();
			groupSearchForm.setSystemManaged(true);
			List<InvestmentSecurityGroup> grpList = getInvestmentSecurityGroupList(groupSearchForm);
			command.setSecurityGroupIds(CollectionUtils.getConverted(grpList, InvestmentSecurityGroup::getId).toArray(new Short[0]));
		}

		int failedCount = 0;
		int processedCount = 0;
		int totalGroups = ArrayUtils.getLength(command.getSecurityGroupIds());

		for (int i = 0; i < ArrayUtils.getLength(command.getSecurityGroupIds()); i++) {
			InvestmentSecurityGroup group = getInvestmentSecurityGroup(command.getSecurityGroupIds()[i]);
			try {
				ValidationUtils.assertTrue(group.isSystemManaged(), String.format("Cannot perform System Managed rebuild for Investment Security Group [%s] because it is not System Managed", group.getLabel()));
				if (!command.isLimitRebuildForNewSecurityInsert() || group.isRebuildOnNewSecurityInsert()) {
					rebuildInvestmentSecurityGroupImpl(command.getStatus(), group);
					processedCount++;
				}
			}
			catch (Exception e) {
				failedCount++;
				command.getStatus().addError("Error rebuilding investment security group " + group.getName());
			}
		}
		command.getStatus().setMessage(String.format("%d Total Groups.  %d Successfully Rebuilt.  %d Skipped.  %d Failed.", totalGroups, processedCount, totalGroups - processedCount, failedCount));
		return command.getStatus();
	}


	////////////////////////////////////////////////////////////////////////////
	/////      Investment Security Group Security Business Methods         /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityGroupSecurity getInvestmentSecurityGroupSecurityByGroupAndSecurity(short groupId, int securityId) {
		return getInvestmentSecurityGroupSecurityDAO().findOneByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{groupId, securityId});
	}


	@Override
	public List<InvestmentSecurityGroupSecurity> getInvestmentSecurityGroupSecurityList(InvestmentSecurityGroupSecuritySearchForm searchForm) {
		SearchConfigurer<Criteria> searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getOverlappingSecurityGroupId() != null) {
					String overlapGroupAlias = super.getPathAlias("referenceTwo.groupItemList", criteria);
					criteria.add(Restrictions.eq(overlapGroupAlias + ".referenceOne.id", searchForm.getOverlappingSecurityGroupId()));
					criteria.add(Restrictions.neProperty(criteria.getAlias() + ".referenceOne.id", overlapGroupAlias + ".referenceOne.id"));
				}
			}
		};

		return getInvestmentSecurityGroupSecurityDAO().findBySearchCriteria(searchConfigurer);
	}

	@Override
	public List<InvestmentSecurityGroupSecurity> getInvestmentSecurityGroupSecurityListByGroup(short groupId) {
		return getInvestmentSecurityGroupSecurityDAO().findByField("referenceOne.id", groupId);
	}


	@Override
	public List<InvestmentSecurityGroupSecurity> getInvestmentSecurityGroupSecurityListByInstrument(final short groupId, final int instrumentId) {
		HibernateSearchConfigurer config = criteria -> {
			criteria.createAlias("referenceTwo", "s");
			criteria.createAlias("s.instrument", "i");
			criteria.add(Restrictions.eq("referenceOne.id", groupId));
			criteria.add(Restrictions.eq("i.id", instrumentId));
		};
		return getInvestmentSecurityGroupSecurityDAO().findBySearchCriteria(config);
	}


	@Override
	public void linkInvestmentSecurityGroupToSecurity(short groupId, int securityId) {
		InvestmentSecurityGroup grp = getInvestmentSecurityGroup(groupId);
		ValidationUtils.assertFalse(grp.isSystemManaged(), "Investment Security Group [" + grp.getName() + "] is system managed.  You cannot manually link securities.");
		InvestmentSecurityGroupSecurity groupSecurity = new InvestmentSecurityGroupSecurity();
		groupSecurity.setReferenceOne(grp);
		groupSecurity.setReferenceTwo(getInvestmentInstrumentService().getInvestmentSecurity(securityId));
		saveInvestmentSecurityGroupSecurity(groupSecurity);
	}


	private void saveInvestmentSecurityGroupSecurity(InvestmentSecurityGroupSecurity bean) {
		ValidationUtils.assertFalse(bean.getReferenceOne().isSystemManaged(), "Investment Security Group [" + bean.getReferenceOne().getName() + "] is system managed.  You cannot manually remove securities.");
		getInvestmentSecurityGroupSecurityDAO().save(bean);
		getInvestmentSecurityGroupSecurityCache().clearInvestmentSecurityList(bean.getReferenceOne().getId());
	}


	@Override
	@Transactional
	public void linkInvestmentSecurityGroupToSecurityList(InvestmentSecurityGroup group, List<InvestmentSecurity> securityList) {
		if (group != null && !CollectionUtils.isEmpty(securityList)) {
			securityList.stream().filter(Objects::nonNull).map(security -> {
				InvestmentSecurityGroupSecurity groupSecurity = new InvestmentSecurityGroupSecurity();
				groupSecurity.setReferenceOne(group);
				groupSecurity.setReferenceTwo(security);
				return groupSecurity;
			}).forEach(getInvestmentSecurityGroupSecurityDAO()::save);
			getInvestmentSecurityGroupSecurityCache().clearInvestmentSecurityList(group.getId());
		}
	}


	@Override
	public void deleteInvestmentSecurityGroupSecurity(short groupId, int securityId) {
		InvestmentSecurityGroup grp = getInvestmentSecurityGroup(groupId);
		ValidationUtils.assertFalse(grp.isSystemManaged(), "Investment Security Group [" + grp.getName() + "] is system managed.  You cannot manually remove securities.");
		InvestmentSecurityGroupSecurity bean = getInvestmentSecurityGroupSecurityDAO().findOneByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{groupId, securityId});
		deleteInvestmentSecurityGroupSecurity(bean);
	}


	@Override
	public void deleteInvestmentSecurityGroupSecurity(InvestmentSecurityGroupSecurity bean) {
		if (bean != null) {
			getInvestmentSecurityGroupSecurityDAO().delete(bean.getId());
			getInvestmentSecurityGroupSecurityCache().clearInvestmentSecurityList(bean.getReferenceOne().getId());
		}
	}


	@Override
	public void deleteInvestmentSecurityGroupSecurityListForSecurity(int securityId) {
		getInvestmentSecurityGroupSecurityDAO().deleteList(getInvestmentSecurityGroupSecurityDAO().findByField("referenceTwo.id", securityId));
	}


	@Override
	public boolean updateInvestmentSecurityInGroup(InvestmentSecurityGroup securityGroup, InvestmentSecurity from, InvestmentSecurity to) {
		if (securityGroup != null && from != null && to != null) {
			InvestmentSecurityGroupSecurity gs = getInvestmentSecurityGroupSecurityByGroupAndSecurity(securityGroup.getId(), from.getId());
			if (gs != null) {
				gs.setReferenceTwo(to);
				saveInvestmentSecurityGroupSecurity(gs);
				return true;
			}
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentSecurityGroup, Criteria> getInvestmentSecurityGroupDAO() {
		return this.investmentSecurityGroupDAO;
	}


	public void setInvestmentSecurityGroupDAO(AdvancedUpdatableDAO<InvestmentSecurityGroup, Criteria> investmentSecurityGroupDAO) {
		this.investmentSecurityGroupDAO = investmentSecurityGroupDAO;
	}


	public AdvancedUpdatableDAO<InvestmentSecurityGroupSecurity, Criteria> getInvestmentSecurityGroupSecurityDAO() {
		return this.investmentSecurityGroupSecurityDAO;
	}


	public void setInvestmentSecurityGroupSecurityDAO(AdvancedUpdatableDAO<InvestmentSecurityGroupSecurity, Criteria> investmentSecurityGroupSecurityDAO) {
		this.investmentSecurityGroupSecurityDAO = investmentSecurityGroupSecurityDAO;
	}


	public DaoNamedEntityCache<InvestmentSecurityGroup> getInvestmentSecurityGroupCache() {
		return this.investmentSecurityGroupCache;
	}


	public void setInvestmentSecurityGroupCache(DaoNamedEntityCache<InvestmentSecurityGroup> investmentSecurityGroupCache) {
		this.investmentSecurityGroupCache = investmentSecurityGroupCache;
	}


	public InvestmentSecurityGroupSecurityCache getInvestmentSecurityGroupSecurityCache() {
		return this.investmentSecurityGroupSecurityCache;
	}


	public void setInvestmentSecurityGroupSecurityCache(InvestmentSecurityGroupSecurityCache investmentSecurityGroupSecurityCache) {
		this.investmentSecurityGroupSecurityCache = investmentSecurityGroupSecurityCache;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
