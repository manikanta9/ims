package com.clifton.investment.setup.group.rebuild;

import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.file.ExcelFileUtils;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.json.custom.CustomJsonObjectList;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAware;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;


/**
 * The <code>InvestmentSecurityGroupFileRebuildAwareExecutor</code> is an {@link EntityGroupRebuildAwareExecutor} bean that
 * reads a file to determine the {@link InvestmentSecurity} list to include in an {@link InvestmentSecurityGroup}. Securities
 * can be looked up by matching CUSIP, ISIN, SEDOL, or symbol from rows in an Excel or CSV file.
 *
 * @author nickk
 */
public class InvestmentSecurityGroupFileRebuildAwareExecutor implements EntityGroupRebuildAwareExecutor, ValidationAware {

	public static final int DEFAULT_MAX_MESSAGE_COUNT = 10000;

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	/**
	 * File directory and name to look for. If file not found, do nothing.
	 */
	private String securityListSourceFilePath;
	private String securityListSourceFileName;
	/**
	 * If a file directory contains many dated files matching the file name, these properties assist in ordering the files in order to process the latest one.
	 */
	private String fileNameDatePattern;
	private String fileNameDateFormat;

	/**
	 * Custom table of {@link SecurityIdentifierTypes} and the corresponding column index for each.
	 */
	private CustomJsonObjectList securityIdentifierToColumnIndexList;

	/**
	 * Applicable for excel file only. Defines the sheet of the file to parse.
	 */
	private int sheetIndex;

	/**
	 * Applicable for CSV file only. Defines the delimiter for the CSV column values.
	 */
	private String csvDelimiterCharacter;

	/**
	 * File directory to archive processed files. If blank, no archiving will be done.
	 */
	private String archiveFilePath;

	/**
	 * Flag to filter to active securities when finding matching securities. Default is to include active and inactive.
	 */
	private boolean activeSecuritiesOnly;
	/**
	 * Flag to indicate if multiple securities matching an identifier are not allowed. Default will be to allow.
	 */
	private boolean disallowMultipleSecurities;
	/**
	 * Flag to ignore not found securities to allow processing to finish instead of throwing an error.
	 */
	private boolean ignoreNotFound;
	/**
	 * Flag to define whether the processed file has a header row (first row). If true, processes from row 0, otherwise row 1.
	 */
	private boolean noHeaderRow;
	/**
	 * Flag to define whether all security identifier entries should be attempted to match securities. Default is to stop after first match per row.
	 */
	private boolean attemptEachSecurityIdentifier;
	/**
	 * Flag to delete processed file after copied and processed.
	 */
	private boolean deleteFileAfterProcessed;
	/**
	 * Flag to include of exclude blank rows when processing a file.
	 */
	private boolean disableSkipBlankRow;
	/**
	 * Changes the maximum number of status messages allowed while processing a file.
	 */
	private Integer maxStatusMessageCount;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotEmpty(getSecurityListSourceFilePath(), "The security list file path where the security list file can be found is required");
		ValidationUtils.assertTrue(FileContainerFactory.getFileContainer(FilePath.forPath(getSecurityListSourceFilePath())).isDirectory(), "The security list file path where the security list file can be found must be a directory");

		ValidationUtils.assertNotEmpty(getSecurityListSourceFileName(), "The security list file name for the security list file is required");
		String fileExtension = FileUtils.getFileExtension(getSecurityListSourceFileName());
		ValidationUtils.assertNotEmpty(fileExtension, "The security list file name for the security list file is missing a file extension");

		try {
			Pattern.compile(getSecurityListSourceFileName());
		}
		catch (PatternSyntaxException e) {
			throw new ValidationException("The security list file name cannot be parsed.", e);
		}

		if (!StringUtils.isEmpty(getFileNameDatePattern())) {
			try {
				Pattern.compile(getSecurityListSourceFileName());
			}
			catch (PatternSyntaxException e) {
				throw new ValidationException("The file name date pattern cannot be parsed.", e);
			}
			ValidationUtils.assertNotEmpty(getFileNameDateFormat(), "File name date format is required when file name date pattern is defined");
		}

		if (!StringUtils.isEmpty(getArchiveFilePath())) {
			FileContainer archiveDirectory = FileContainerFactory.getFileContainer(getArchiveFilePath());
			if (archiveDirectory.exists()) {
				ValidationUtils.assertTrue(archiveDirectory.isDirectory(), "The archive directory path does not resolve to a directory");
			}
		}

		ValidationUtils.assertTrue(CollectionUtils.createHashSet("csv", "xls", "xlsx").contains(fileExtension), "The source file name for the security list file is using an unsupported file extension: " + fileExtension);
		if ("csv".equals(fileExtension)) {
			if (!StringUtils.isEmpty(getCsvDelimiterCharacter())) {
				ValidationUtils.assertTrue(getCsvDelimiterCharacter().length() == 1, "The CSV delimiter can only be one character");
			}
		}
		else {
			ValidationUtils.assertTrue(getSheetIndex() > -1, "The source file sheet index must be 0 or greater");
		}

		getSecurityIdentifierToColumnIndexList().forEach(this::getSecurityIdentifierColumnIndex);


		if (getMaxStatusMessageCount() == null || getMaxStatusMessageCount().intValue() < 0) {
			setMaxStatusMessageCount(DEFAULT_MAX_MESSAGE_COUNT);
		}
	}


	@Override
	public void validate(EntityGroupRebuildAware groupEntity) {
		validate();
		ValidationUtils.assertTrue(groupEntity instanceof InvestmentSecurityGroup, "Rebuild executor applies to InvestmentSecurityGroups only.");
	}


	@Override
	public Status executeRebuild(EntityGroupRebuildAware groupEntity, Status status) {
		validate(groupEntity);

		status.setMaxDetailCount(getMaxStatusMessageCount());

		InvestmentSecurityGroup securityGroup = (InvestmentSecurityGroup) groupEntity;

		SecurityListFile securityListFile = getSecurityListFileToProcess(status);
		if (securityListFile.exists()) {
			Set<InvestmentSecurity> rebuildSecuritySet = getRebuildInvestmentSecurityList(securityListFile, status);
			updateSecurityGroupEntries(securityGroup, rebuildSecuritySet, status);

			archiveSecuritySourceFile(securityListFile);
			if (isDeleteFileAfterProcessed()) {
				deleteSecurityListSourceFile(securityListFile);
			}
		}
		else {
			status.addMessage(String.format("Rebuild skipped because file did not exist (path [%s}, fileName/regex [%s])", getSecurityListSourceFilePath(), getSecurityListSourceFileName()));
		}
		return status;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private SecurityListFile getSecurityListFileToProcess(Status status) {
		Pattern fileNamePattern = Pattern.compile(getSecurityListSourceFileName());
		FileContainer securityListDirectory = FileContainerFactory.getFileContainer(getSecurityListSourceFilePath());
		try {
			List<FileContainer> matchingSecurityListFileList = securityListDirectory.list("*", fileName -> fileNamePattern.matcher(fileName).matches())
					.map(fileName -> FileContainerFactory.getFileContainer(FileUtils.combinePath(getSecurityListSourceFilePath(), fileName)))
					.collect(Collectors.toList());
			return findSecurityListFileToProcess(matchingSecurityListFileList, status);
		}
		catch (IOException io) {
			String message = String.format("Failed to find applicable security list file using path [%s] and name [%s]", getSecurityListSourceFilePath(), getSecurityListSourceFileName());
			status.addError(message);
			throw new RuntimeException(message, io);
		}
	}


	private SecurityListFile findSecurityListFileToProcess(List<FileContainer> possibleSecurityFileList, Status status) {
		int size = possibleSecurityFileList.size();
		if (size == 0) {
			status.addMessage(String.format("File not found (path [%s}, fileName/regex [%s])", getSecurityListSourceFilePath(), getSecurityListSourceFileName()));
			return SecurityListFile.newEmptyMatch();
		}
		else if (size == 1) {
			status.addMessage(String.format("Found single file match (path [%s}, fileName/regex [%s])", getSecurityListSourceFilePath(), getSecurityListSourceFileName()));
			return SecurityListFile.newSingleMatch(CollectionUtils.getFirstElement(possibleSecurityFileList));
		}

		if (!StringUtils.isEmpty(getFileNameDatePattern())) {
			status.addMessage(String.format("Found multiple file matches applying date filter sorting (path [%s}, fileName/regex [%s], datePatter [%s], dateFormat [%s])", getSecurityListSourceFilePath(), getSecurityListSourceFileName(), getFileNameDatePattern(), getFileNameDateFormat()));
			Pattern dataPattern = Pattern.compile(getFileNameDatePattern());
			Map<Date, FileContainer> dateToFileNameMap = new TreeMap<>(Collections.reverseOrder());
			for (FileContainer possibleFile : possibleSecurityFileList) {
				Matcher matcher = dataPattern.matcher(possibleFile.getName());
				if (matcher.matches()) {
					Date date = DateUtils.toDate(matcher.group(1), getFileNameDateFormat());
					dateToFileNameMap.put(date, possibleFile);
				}
			}
			Iterator<Map.Entry<Date, FileContainer>> sortedFileMatchIterator = dateToFileNameMap.entrySet().iterator();
			List<FileContainer> otherMatchingFiles = new ArrayList<>();
			FileContainer toProcess = sortedFileMatchIterator.next().getValue();
			while (sortedFileMatchIterator.hasNext()) {
				otherMatchingFiles.add(sortedFileMatchIterator.next().getValue());
			}
			return SecurityListFile.newWithMultipleMatches(toProcess, otherMatchingFiles);
		}

		String message = String.format("Found multiple security list files using path [%s] and name [%s]. If applicable use the file name date pattern and format fields.", getSecurityListSourceFilePath(), getSecurityListSourceFileName());
		status.addError(message);
		throw new RuntimeException(message);
	}


	private Set<InvestmentSecurity> getRebuildInvestmentSecurityList(SecurityListFile securityListFile, Status status) {
		FileContainer fileToProcess = securityListFile.getFileToProcess();
		String fileExtension = FileUtils.getFileExtension(fileToProcess.getName());

		Map<String, List<InvestmentSecurity>> securityListMap = new HashMap<>();
		if ("csv".equals(fileExtension)) {
			addSecuritiesFromCsvFile(fileToProcess, securityListMap, status);
		}
		else { // excel
			addSecuritiesFromExcelFile(fileToProcess, securityListMap, status);
		}
		Set<InvestmentSecurity> securitySet = securityListMap.values().stream()
				.flatMap(CollectionUtils::getStream)
				.collect(Collectors.toSet());
		if (CollectionUtils.isEmpty(securitySet)) {
			status.addMessage(String.format("Rebuild resulted in no security matches (path [%s], fileName/regex [%s])", getSecurityListSourceFilePath(), getSecurityListSourceFileName()));
		}
		return securitySet;
	}


	@Transactional
	protected void updateSecurityGroupEntries(InvestmentSecurityGroup securityGroup, Set<InvestmentSecurity> rebuildSecuritySet, Status status) {
		Set<InvestmentSecurity> securitiesToAdd = new LinkedHashSet<>(rebuildSecuritySet);
		Set<InvestmentSecurityGroupSecurity> securityEntriesToRemove = new LinkedHashSet<>();

		List<InvestmentSecurityGroupSecurity> currentInvestmentSecurityGroupSecurityList = getInvestmentSecurityGroupService().getInvestmentSecurityGroupSecurityListByGroup(securityGroup.getId());
		for (InvestmentSecurityGroupSecurity securityGroupSecurity : CollectionUtils.getIterable(currentInvestmentSecurityGroupSecurityList)) {
			// Remove the security from securities to add list because it is already included in the security group to avoid duplicate entries
			if (!securitiesToAdd.remove(securityGroupSecurity.getReferenceTwo())) {
				securityEntriesToRemove.add(securityGroupSecurity);
			}
		}
		// Add securities to the security group that previously did not exist in the group before removing items
		if (!CollectionUtils.isEmpty(securitiesToAdd)) {
			List<InvestmentSecurity> rebuildSecurityList = new ArrayList<>(securitiesToAdd);
			getInvestmentSecurityGroupService().linkInvestmentSecurityGroupToSecurityList(securityGroup, rebuildSecurityList);
		}
		// Delete security entries no longer needed last
		if (!CollectionUtils.isEmpty(securityEntriesToRemove)) {
			securityEntriesToRemove.forEach(getInvestmentSecurityGroupService()::deleteInvestmentSecurityGroupSecurity);
		}
		status.addMessage(String.format("Rebuild resulted in %d security additions and %d security removals (path [%s], fileName/regex [%s])", securitiesToAdd.size(), securityEntriesToRemove.size(), getSecurityListSourceFilePath(), getSecurityListSourceFileName()));
	}


	private void addSecuritiesFromCsvFile(FileContainer securityListFile, Map<String, List<InvestmentSecurity>> securityListMap, Status status) {
		char delimiter = ',';
		if (!StringUtils.isEmpty(getCsvDelimiterCharacter())) {
			delimiter = getCsvDelimiterCharacter().charAt(0);
		}
		CSVFormat format = CSVFormat.newFormat(delimiter)
				.withQuote('"')
				.withTrim();
		if (!isNoHeaderRow()) {
			format = format.withFirstRecordAsHeader();
		}

		List<SecurityIdentifierColumnIndex> securityIdentifierColumnIndexList = getSecurityIdentifierColumnIndexList();
		try (final CSVParser parser = CSVParser.parse(securityListFile.getInputStream(), StandardCharsets.UTF_8, format)) {
			for (final CSVRecord record : parser) {
				if (record.size() > 1) {
					if (!"[, , , ]".equals(ArrayUtils.toString(record.toMap().values())) || isDisableSkipBlankRow()) {
						loadSecurityForRow(securityIdentifierColumnIndexList, record::get, record.getRecordNumber(), securityListMap, status);
					}
				}
			}
		}
		catch (Exception e) {

			throw new RuntimeException("Failed to process csv file.", e);
		}
	}


	private void addSecuritiesFromExcelFile(FileContainer securityListFile, Map<String, List<InvestmentSecurity>> securityListMap, Status status) {
		Workbook workbook = ExcelFileUtils.createWorkbook(securityListFile);
		Sheet sheet = workbook.getSheetAt(getSheetIndex());
		// Actual Data starts at index 1
		int rowIndex = isNoHeaderRow() ? 0 : 1;
		List<SecurityIdentifierColumnIndex> securityIdentifierColumnIndexList = getSecurityIdentifierColumnIndexList();
		ObjectWrapper<Row> row = new ObjectWrapper<>(ExcelFileUtils.getRow(sheet, rowIndex, false));
		while (row.getObject() != null) {
			boolean isBlankRow = false;
			if (row.getObject().getLastCellNum() == 1 && (row.getObject().getCell(0) == null || StringUtils.isEmpty(row.getObject().getCell(0).getStringCellValue()))) {
				isBlankRow = true;
			}
			if (!isBlankRow || isDisableSkipBlankRow()) {
				IntFunction<String> columnToIdentifierFunction = columnIndex -> {
					Cell cell = ExcelFileUtils.getCell(row.getObject(), columnIndex, false);
					String identifier = (cell == null || CellType.BLANK == cell.getCellTypeEnum()) ? null : cell.getStringCellValue();
					return StringUtils.trim(identifier);
				};
				loadSecurityForRow(securityIdentifierColumnIndexList, columnToIdentifierFunction, rowIndex, securityListMap, status);
			}
			row.setObject(ExcelFileUtils.getRow(sheet, ++rowIndex, false));
		}
	}


	@Transactional(readOnly = true)
	protected boolean loadSecurityForRow(List<SecurityIdentifierColumnIndex> securityIdentifierColumnIndexList, IntFunction<String> rowColumnIndexIdentifierFunction, long rowIndex, Map<String, List<InvestmentSecurity>> securityListMap, Status status) {
		boolean found = false;
		Map<String, String> failedLookupProperties = new HashMap<>();
		for (SecurityIdentifierColumnIndex identifierColumnIndex : securityIdentifierColumnIndexList) {
			SecurityIdentifierTypes identifierType = identifierColumnIndex.getType();
			String securityIdentifier = rowColumnIndexIdentifierFunction.apply(identifierColumnIndex.getColumnIndex());
			List<InvestmentSecurity> securityList = getSecurityByIdentifierForType(securityIdentifier, identifierType, securityListMap);
			if (!CollectionUtils.isEmpty(securityList)) {
				found = true;
				if (!isAttemptEachSecurityIdentifier()) {
					break;
				}
			}
			else {
				failedLookupProperties.put(identifierType.toString(), securityIdentifier);
			}
		}
		if (!found) {
			String message = "Row " + rowIndex + " no match";
			if (!isIgnoreNotFound()) {
				status.addError(message, failedLookupProperties);
				throw new RuntimeException(message);
			}
			status.addSkipped(message, failedLookupProperties);
		}
		return found;
	}


	private List<SecurityIdentifierColumnIndex> getSecurityIdentifierColumnIndexList() {
		return getSecurityIdentifierToColumnIndexList().getEntityList().stream()
				.map(this::getSecurityIdentifierColumnIndex)
				.sorted(Comparator.comparing(SecurityIdentifierColumnIndex::getOrder))
				.collect(Collectors.toList());
	}


	private SecurityIdentifierColumnIndex getSecurityIdentifierColumnIndex(Map<String, Object> securityIdentifierEntry) {
		String identifierTypeString = (String) securityIdentifierEntry.get("identifierType");
		SecurityIdentifierTypes identifierType = SecurityIdentifierTypes.getType(identifierTypeString);
		Integer index = (Integer) securityIdentifierEntry.get("column");
		if (index == null || index < 0) {
			throw new ValidationException("Security identifier column index must be defined and cannot be negative for identifier: " + identifierTypeString);
		}
		Integer order = (Integer) securityIdentifierEntry.get("order");
		return new SecurityIdentifierColumnIndex(identifierType, index, order == null ? 0 : order);
	}


	private List<InvestmentSecurity> getSecurityByIdentifierForType(String securityIdentifier, SecurityIdentifierTypes type, Map<String, List<InvestmentSecurity>> securityListMap) {
		if (StringUtils.isEmpty(securityIdentifier)) {
			return null;
		}
		String cacheKey = type.getCacheKey(securityIdentifier);
		return securityListMap.computeIfAbsent(cacheKey, key -> {
			List<InvestmentSecurity> securityList = type.retrieveSecurityList(securityIdentifier, getInvestmentInstrumentService());
			if (isActiveSecuritiesOnly()) {
				securityList = CollectionUtils.getStream(securityList).filter(InvestmentSecurity::isActive).collect(Collectors.toList());
			}
			if (CollectionUtils.getSize(securityList) > 1 && isDisallowMultipleSecurities()) {
				ValidationUtils.assertFalse(isDisallowMultipleSecurities(), () -> String.format("Found more than one security for identifier type [%s] and type [%s] and the rebuild is configured to disallow multiple.", securityIdentifier, type));
			}
			return securityList;
		});
	}


	private void archiveSecuritySourceFile(SecurityListFile securityListFile) {
		if (securityListFile.exists() && !StringUtils.isEmpty(getArchiveFilePath())) {
			if (createArchiveDirectory()) {
				securityListFile.performAction(this::moveFileOverwrite);
			}
		}
	}


	private void moveFileOverwrite(FileContainer fileToMove) {
		FileContainer archiveFile = FileContainerFactory.getFileContainer(FileUtils.combinePath(getArchiveFilePath(), fileToMove.getName()));
		try {
			FileUtils.moveFileOverwrite(fileToMove, archiveFile);
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to archive security list file: " + archiveFile.getPath(), e);
		}
	}


	private boolean createArchiveDirectory() {
		if (!StringUtils.isEmpty(getArchiveFilePath())) {
			try {
				FileContainer archiveDirectory = FileContainerFactory.getFileContainer(getArchiveFilePath());
				if (archiveDirectory.exists()) {
					return true;
				}
				return FileUtils.createDirectory(getArchiveFilePath());
			}
			catch (Exception e) {
				throw new RuntimeException("Failed to create security list file archive directory: " + getArchiveFilePath(), e);
			}
		}
		return false;
	}


	private void deleteSecurityListSourceFile(SecurityListFile securityListFile) {
		if (securityListFile.exists() && isDeleteFileAfterProcessed()) {
			securityListFile.performAction(this::deleteFile);
		}
	}


	private void deleteFile(FileContainer fileToDelete) {
		try {
			fileToDelete.deleteFile();
		}
		catch (IOException io) {
			throw new RuntimeException("Failed to delete security list file: " + fileToDelete.getPath(), io);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private static final class SecurityIdentifierColumnIndex {

		private final SecurityIdentifierTypes type;
		private final int columnIndex;
		private final int order;


		SecurityIdentifierColumnIndex(SecurityIdentifierTypes type, int columnIndex, int order) {
			this.type = type;
			this.columnIndex = columnIndex;
			this.order = order;
		}


		public SecurityIdentifierTypes getType() {
			return this.type;
		}


		public int getColumnIndex() {
			return this.columnIndex;
		}


		public int getOrder() {
			return this.order;
		}
	}

	private enum SecurityIdentifierTypes {
		CUSIP {
			@Override
			public List<InvestmentSecurity> retrieveSecurityList(String securityIdentifierValue, InvestmentInstrumentService service) {
				if (StringUtils.isEmpty(securityIdentifierValue)) {
					return null;
				}
				return service.getInvestmentSecurityListByCusip(securityIdentifierValue);
			}
		},
		SYMBOL {
			@Override
			public List<InvestmentSecurity> retrieveSecurityList(String securityIdentifierValue, InvestmentInstrumentService service) {
				if (StringUtils.isEmpty(securityIdentifierValue)) {
					return null;
				}
				return service.getInvestmentSecurityListBySymbol(securityIdentifierValue);
			}
		},
		SEDOL {
			@Override
			public List<InvestmentSecurity> retrieveSecurityList(String securityIdentifierValue, InvestmentInstrumentService service) {
				if (StringUtils.isEmpty(securityIdentifierValue)) {
					return null;
				}
				return service.getInvestmentSecurityListBySedol(securityIdentifierValue);
			}
		},
		ISIN {
			@Override
			public List<InvestmentSecurity> retrieveSecurityList(String securityIdentifierValue, InvestmentInstrumentService service) {
				if (StringUtils.isEmpty(securityIdentifierValue)) {
					return null;
				}
				return service.getInvestmentSecurityListByIsin(securityIdentifierValue);
			}
		};
		// add once we have master security ID
		// MASTER_ID;


		public static SecurityIdentifierTypes getType(String identifierTypeString) {
			if (CUSIP.name().equals(identifierTypeString)) {
				return CUSIP;
			}
			else if (SYMBOL.name().equals(identifierTypeString)) {
				return SYMBOL;
			}
			else if (SEDOL.name().equals(identifierTypeString)) {
				return SEDOL;
			}
			else if (ISIN.name().equals(identifierTypeString)) {
				return ISIN;
			}

			throw new ValidationException("Unable to find a security identification type for: " + identifierTypeString);
		}


		public String getCacheKey(String securityIdentifierValue) {
			return this.name() + "_" + securityIdentifierValue;
		}


		public abstract List<InvestmentSecurity> retrieveSecurityList(String securityIdentifierValue, InvestmentInstrumentService service);
	}

	private static final class SecurityListFile {

		private final FileContainer fileToProcess;
		private final List<FileContainer> matchingFileList;


		private SecurityListFile(FileContainer fileToProcess, List<FileContainer> matchingFileList) {
			this.fileToProcess = fileToProcess;
			this.matchingFileList = matchingFileList;
		}


		static SecurityListFile newEmptyMatch() {
			return new SecurityListFile(null, Collections.emptyList());
		}


		static SecurityListFile newSingleMatch(FileContainer fileToProcess) {
			return new SecurityListFile(fileToProcess, Collections.emptyList());
		}


		static SecurityListFile newWithMultipleMatches(FileContainer fileToProcess, List<FileContainer> otherMatchingFiles) {
			return new SecurityListFile(fileToProcess, otherMatchingFiles);
		}


		public boolean exists() {
			return getFileToProcess() != null && getFileToProcess().exists();
		}


		public void performAction(Consumer<FileContainer> fileContainerAction) {
			if (fileContainerAction != null && exists()) {
				fileContainerAction.accept(getFileToProcess());
				// Filter matching for non-null and those not matching processed file name to avoid redundant action
				CollectionUtils.getStream(getMatchingFileList())
						.filter(Objects::nonNull)
						.filter(file -> !file.getName().equals(getFileToProcess().getName()))
						.forEach(fileContainerAction);
			}
		}


		public FileContainer getFileToProcess() {
			return this.fileToProcess;
		}


		public List<FileContainer> getMatchingFileList() {
			return this.matchingFileList;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getSecurityListSourceFilePath() {
		return this.securityListSourceFilePath;
	}


	public void setSecurityListSourceFilePath(String securityListSourceFilePath) {
		this.securityListSourceFilePath = securityListSourceFilePath;
	}


	public String getSecurityListSourceFileName() {
		return this.securityListSourceFileName;
	}


	public void setSecurityListSourceFileName(String securityListSourceFileName) {
		this.securityListSourceFileName = securityListSourceFileName;
	}


	public String getFileNameDatePattern() {
		return this.fileNameDatePattern;
	}


	public void setFileNameDatePattern(String fileNameDatePattern) {
		this.fileNameDatePattern = fileNameDatePattern;
	}


	public String getFileNameDateFormat() {
		return this.fileNameDateFormat;
	}


	public void setFileNameDateFormat(String fileNameDateFormat) {
		this.fileNameDateFormat = fileNameDateFormat;
	}


	public CustomJsonObjectList getSecurityIdentifierToColumnIndexList() {
		return this.securityIdentifierToColumnIndexList;
	}


	public void setSecurityIdentifierToColumnIndexList(CustomJsonObjectList securityIdentifierToColumnIndexList) {
		this.securityIdentifierToColumnIndexList = securityIdentifierToColumnIndexList;
	}


	public int getSheetIndex() {
		return this.sheetIndex;
	}


	public String getCsvDelimiterCharacter() {
		return this.csvDelimiterCharacter;
	}


	public void setCsvDelimiterCharacter(String csvDelimiterCharacter) {
		this.csvDelimiterCharacter = csvDelimiterCharacter;
	}


	public void setSheetIndex(int sheetIndex) {
		this.sheetIndex = sheetIndex;
	}


	public String getArchiveFilePath() {
		return this.archiveFilePath;
	}


	public void setArchiveFilePath(String archiveFilePath) {
		this.archiveFilePath = archiveFilePath;
	}


	public boolean isActiveSecuritiesOnly() {
		return this.activeSecuritiesOnly;
	}


	public void setActiveSecuritiesOnly(boolean activeSecuritiesOnly) {
		this.activeSecuritiesOnly = activeSecuritiesOnly;
	}


	public boolean isDisallowMultipleSecurities() {
		return this.disallowMultipleSecurities;
	}


	public void setDisallowMultipleSecurities(boolean disallowMultipleSecurities) {
		this.disallowMultipleSecurities = disallowMultipleSecurities;
	}


	public boolean isIgnoreNotFound() {
		return this.ignoreNotFound;
	}


	public void setIgnoreNotFound(boolean ignoreNotFound) {
		this.ignoreNotFound = ignoreNotFound;
	}


	public boolean isNoHeaderRow() {
		return this.noHeaderRow;
	}


	public void setNoHeaderRow(boolean noHeaderRow) {
		this.noHeaderRow = noHeaderRow;
	}


	public boolean isAttemptEachSecurityIdentifier() {
		return this.attemptEachSecurityIdentifier;
	}


	public void setAttemptEachSecurityIdentifier(boolean attemptEachSecurityIdentifier) {
		this.attemptEachSecurityIdentifier = attemptEachSecurityIdentifier;
	}


	public boolean isDeleteFileAfterProcessed() {
		return this.deleteFileAfterProcessed;
	}


	public void setDeleteFileAfterProcessed(boolean deleteFileAfterProcessed) {
		this.deleteFileAfterProcessed = deleteFileAfterProcessed;
	}


	public boolean isDisableSkipBlankRow() {
		return this.disableSkipBlankRow;
	}


	public void setDisableSkipBlankRow(boolean disableSkipBlankRow) {
		this.disableSkipBlankRow = disableSkipBlankRow;
	}


	public Integer getMaxStatusMessageCount() {
		return this.maxStatusMessageCount;
	}


	public void setMaxStatusMessageCount(Integer maxStatusMessageCount) {
		this.maxStatusMessageCount = maxStatusMessageCount;
	}
}
