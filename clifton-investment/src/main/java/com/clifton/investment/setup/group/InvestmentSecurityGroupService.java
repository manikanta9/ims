package com.clifton.investment.setup.group;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.group.search.InvestmentSecurityGroupSearchForm;
import com.clifton.investment.setup.group.search.InvestmentSecurityGroupSecuritySearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentSecurityGroupService</code> has methods for working with investment security groups.
 *
 * @author apopp
 */
public interface InvestmentSecurityGroupService {

	/**
	 * Returns a list of securities in the specified group
	 *
	 * @param securityGroupId
	 * @param instrumentId    - optional - if set filters out securities that are tied to that instrument
	 * @param activeOnDate    - optional - if set filters out securities that startDate <= activeOnDate <= endDate
	 */
	public List<InvestmentSecurity> getInvestmentSecurityListForSecurityGroup(short securityGroupId, Integer instrumentId, Date activeOnDate);


	/**
	 * Returns a list of securities in the "Current Securities" group
	 *
	 * @param -            instrumentId - optional - if set filters out securities that are tied to that instrument
	 * @param activeOnDate - optional - if set filters out securities that startDate <= activeOnDate <= endDate
	 */
	public List<InvestmentSecurity> getInvestmentSecurityCurrentList(Integer instrumentId, Date activeOnDate);


	/**
	 * First checks Security Group "Current Securities" to see if a current security is defined.  If there is one
	 * and the security is active on the date - will return that security.
	 * <p>
	 * If nothing in the group defined for the instrument, or it's inactive and existsInGroupOnly is false then will return the
	 * first active security for the instrument with the closest end date.
	 *
	 * @param instrumentId
	 */
	public InvestmentSecurity getInvestmentSecurityCurrentByInstrument(int instrumentId, Date activeOnDate, boolean existsInGroupOnly);


	////////////////////////////////////////////////////////////////////////////
	//////////        Investment Security Group Methods             ////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if the specified security belongs to the specified group.
	 *
	 * @param securityId
	 * @param securityGroupId
	 */
	@SecureMethod(dtoClass = InvestmentSecurityGroup.class)
	public boolean isInvestmentSecurityInSecurityGroup(int securityId, short securityGroupId);


	/**
	 * Returns true if the specified security belongs to the InvestmentSecurityGroup with the specified name.
	 */
	@DoNotAddRequestMapping
	public boolean isInvestmentSecurityInSecurityGroup(int securityId, String securityGroupName);


	public InvestmentSecurityGroup getInvestmentSecurityGroup(short id);


	public InvestmentSecurityGroup getInvestmentSecurityGroupByName(String name);


	public List<InvestmentSecurityGroup> getInvestmentSecurityGroupList(InvestmentSecurityGroupSearchForm searchForm);


	public InvestmentSecurityGroup saveInvestmentSecurityGroup(InvestmentSecurityGroup bean);


	public void deleteInvestmentSecurityGroup(short id);


	@SecureMethod(dtoClass = InvestmentSecurityGroupSecurity.class)
	public Status rebuildInvestmentSecurityGroup(short groupId);


	/**
	 * Optionally limits rebuilding based on {@link InvestmentSecurityGroup#isRebuildOnNewSecurityInsert()} which is currently only used in the
	 * {@link com.clifton.investment.instrument.InvestmentSecurityObserver} because rebuilds are very slow and consume lots of resources. Since this process runs on each security
	 * insert, we want to limit the scope of real time rebuilds and will create a batch job that rebuilds everything each night.
	 */
	@SecureMethod(dtoClass = InvestmentSecurityGroupSecurity.class)
	public Status rebuildInvestmentSecurityGroupSystemManaged(InvestmentSecurityGroupRebuildCommand command);


	////////////////////////////////////////////////////////////////////////////
	/////      Investment Security Group Security Business Methods         /////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityGroupSecurity getInvestmentSecurityGroupSecurityByGroupAndSecurity(short groupId, int securityId);


	public List<InvestmentSecurityGroupSecurity> getInvestmentSecurityGroupSecurityList(InvestmentSecurityGroupSecuritySearchForm searchForm);


	public List<InvestmentSecurityGroupSecurity> getInvestmentSecurityGroupSecurityListByGroup(short groupId);


	public List<InvestmentSecurityGroupSecurity> getInvestmentSecurityGroupSecurityListByInstrument(short groupId, int instrumentId);


	@SecureMethod(dtoClass = InvestmentSecurityGroupSecurity.class)
	public void linkInvestmentSecurityGroupToSecurity(short groupId, int securityId);


	@DoNotAddRequestMapping
	public void linkInvestmentSecurityGroupToSecurityList(InvestmentSecurityGroup group, List<InvestmentSecurity> securityList);


	public void deleteInvestmentSecurityGroupSecurity(short groupId, int securityId);


	@DoNotAddRequestMapping
	public void deleteInvestmentSecurityGroupSecurity(InvestmentSecurityGroupSecurity bean);


	/**
	 * Called prior to deleting a security - deletes all references to in InvestmentSecurityGroupSecurity
	 */
	@DoNotAddRequestMapping
	public void deleteInvestmentSecurityGroupSecurityListForSecurity(int securityId);


	/**
	 * Updates the security in selected group (if it exists) to the new security
	 *
	 * @return Returns true if the update was successful
	 */
	@DoNotAddRequestMapping
	public boolean updateInvestmentSecurityInGroup(InvestmentSecurityGroup securityGroup, InvestmentSecurity from, InvestmentSecurity to);
}
