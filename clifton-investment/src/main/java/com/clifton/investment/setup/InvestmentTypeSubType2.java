package com.clifton.investment.setup;

import com.clifton.core.beans.NamedEntityWithoutLabel;


/**
 * The <code>InvestmentTypeSubType2</code> class defines investment sub-types. It is similar to InvestmentTypeSubType
 * but only another optional way to categorize investment instruments.
 * <p>
 * Sub-type 2 is used only when there already exist sub-type and there is another way to classify investment instruments of that type.
 * <p>
 * For Example:
 * sub-type: Total Return Swaps, Interest Rate Swaps, Credit Default Swaps.
 * sub-type2: Bespoke, Index, Single Name, Tranche
 *
 * @author vgomelsky
 */
public class InvestmentTypeSubType2 extends NamedEntityWithoutLabel<Short> {

	// Options sub types:
	public static final String OPTIONS_CUSTOM = "Custom Options";
	public static final String OPTIONS_DAILY_MARGIN = "Daily Margin";
	public static final String OPTIONS_FLEX = "FLEX Options";
	public static final String OPTIONS_LISTED = "Listed Options";
	public static final String OPTIONS_LOOK_ALIKE = "Look Alike Options";

	// Swaps sub types:
	public static final String SWAPS_BESPOKE = "Bespoke";
	public static final String SWAPS_INDEX = "Index";
	public static final String SWAPS_SINGLE_NAME = "Single Name";
	public static final String SWAPS_TRANCHE = "Tranche";

	////////////////////////////////////////////////////////////////////////////

	private InvestmentType investmentType;

	/**
	 * Industry standard name for "Quantity" (remaining quantity) field for securities of this type.
	 * For example, "Contracts" for Futures, "Shares" for Options, "Face" for bonds, etc.
	 * NOTE: these values override Investment Type and Investment Sub Type settings.
	 */
	private String quantityName;

	/**
	 * For securities with factor changes (ABS and CDS) it is the Original Face/Original Notional (Opening Quantity; not adjusted by current factor).
	 * For other securities it is the same as Remaining Quantity or Quantity (current number of units).
	 * NOTE: these values override Investment Type and Investment Sub Type settings.
	 */
	private String unadjustedQuantityName;

	// When applicable, define Investment Sub Type 2 specific terminology for the following fields (optional overrides Investment Type and Sub Type names).
	private String costFieldName;
	private String costBasisFieldName;
	private String notionalFieldName;
	private String marketValueFieldName;

	/**
	 * The decimal precision for the quantity on trades and positions.  For example, for Futures it would be
	 * 0 and Bonds would be 2.  This value overrides Investment Type and Investment Sub Type setting.
	 */
	private Short quantityDecimalPrecision;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (this.investmentType == null) {
			return super.getLabel();
		}
		return super.getLabel() + " (" + this.investmentType.getLabel() + ")";
	}


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}


	public String getQuantityName() {
		return this.quantityName;
	}


	public void setQuantityName(String quantityName) {
		this.quantityName = quantityName;
	}


	public String getUnadjustedQuantityName() {
		return this.unadjustedQuantityName;
	}


	public void setUnadjustedQuantityName(String unadjustedQuantityName) {
		this.unadjustedQuantityName = unadjustedQuantityName;
	}


	public String getCostFieldName() {
		return this.costFieldName;
	}


	public void setCostFieldName(String costFieldName) {
		this.costFieldName = costFieldName;
	}


	public String getCostBasisFieldName() {
		return this.costBasisFieldName;
	}


	public void setCostBasisFieldName(String costBasisFieldName) {
		this.costBasisFieldName = costBasisFieldName;
	}


	public String getNotionalFieldName() {
		return this.notionalFieldName;
	}


	public void setNotionalFieldName(String notionalFieldName) {
		this.notionalFieldName = notionalFieldName;
	}


	public String getMarketValueFieldName() {
		return this.marketValueFieldName;
	}


	public void setMarketValueFieldName(String marketValueFieldName) {
		this.marketValueFieldName = marketValueFieldName;
	}


	public Short getQuantityDecimalPrecision() {
		return this.quantityDecimalPrecision;
	}


	public void setQuantityDecimalPrecision(Short quantityDecimalPrecision) {
		this.quantityDecimalPrecision = quantityDecimalPrecision;
	}
}
