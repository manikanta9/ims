package com.clifton.investment.setup.group;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.investment.instrument.InvestmentInstrument;

import java.util.Objects;


/**
 * The <code>InvestmentGroupItemInstrument</code> defines the relationship between
 * {@link InvestmentGroupItem} and {@link InvestmentInstrument}s specifically.
 * <p/>
 * The data held in these objects is created/rebuilt by the data held in the {@InvestmentGroupMatrix}
 *
 * @author manderson
 */
public class InvestmentGroupItemInstrument extends ManyToManyEntity<InvestmentGroupItem, InvestmentInstrument, Integer> {

	@Override
	public boolean equals(Object o) {
		// Two InvestmentGroupItemInstrument objects are equal if their references are equal
		if (!(o instanceof InvestmentGroupItemInstrument)) {
			return false;
		}
		InvestmentGroupItemInstrument oBean = (InvestmentGroupItemInstrument) o;
		if (this.getReferenceOne() == oBean.getReferenceOne() || (this.getReferenceOne() != null && this.getReferenceOne().equals(oBean.getReferenceOne()))) {
			if (this.getReferenceTwo() == oBean.getReferenceTwo() || (this.getReferenceTwo() != null && this.getReferenceTwo().equals(oBean.getReferenceTwo()))) {
				return true;
			}
		}
		return false;
	}


	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getReferenceOne(), getReferenceTwo());
	}
}
