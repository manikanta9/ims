package com.clifton.investment.setup;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventTypeHierarchy;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.setup.search.InstrumentHierarchySearchForm;
import com.clifton.investment.setup.search.InvestmentAssetClassSearchForm;
import com.clifton.investment.setup.search.InvestmentSubTypeSearchForm;
import com.clifton.system.schema.column.SystemColumnService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;


/**
 * The <code>InvestmentSetupServiceImpl</code> implements the InvestmentSetupService
 *
 * @author manderson
 */
@Service
public class InvestmentSetupServiceImpl implements InvestmentSetupService {

	private AdvancedUpdatableDAO<InvestmentAssetClass, Criteria> investmentAssetClassDAO;

	private AdvancedUpdatableDAO<InvestmentInstrumentHierarchy, Criteria> investmentInstrumentHierarchyDAO;
	private ReadOnlyDAO<InvestmentType> investmentTypeDAO;
	private AdvancedUpdatableDAO<InvestmentTypeSubType, Criteria> investmentTypeSubTypeDAO;
	private AdvancedUpdatableDAO<InvestmentTypeSubType2, Criteria> investmentTypeSubType2DAO;

	private DaoNamedEntityCache<InvestmentType> investmentTypeCache;
	private DaoSingleKeyListCache<InvestmentInstrumentHierarchy, Short> investmentInstrumentHierarchyByParentCache;

	private InvestmentSecurityEventService investmentSecurityEventService;
	private SystemColumnService systemColumnService;

	////////////////////////////////////////////////////////////////////////////
	///////         Investment Type Business Methods                  //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentType getInvestmentType(short id) {
		return getInvestmentTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentType getInvestmentTypeByName(String name) {
		return getInvestmentTypeCache().getBeanForKeyValueStrict(getInvestmentTypeDAO(), name);
	}


	@Override
	public List<InvestmentType> getInvestmentTypeList() {
		return getInvestmentTypeDAO().findAll();
	}


	////////////////////////////////////////////////////////////////////////////
	///////         Investment Sub Type Business Methods              //////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public InvestmentTypeSubType getInvestmentTypeSubType(short id) {
		return getInvestmentTypeSubTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentTypeSubType> getInvestmentTypeSubTypeListByType(short investmentTypeId) {
		return getInvestmentTypeSubTypeDAO().findByField("investmentType.id", investmentTypeId);
	}


	@Override
	public List<InvestmentTypeSubType> getInvestmentTypeSubTypeList(InvestmentSubTypeSearchForm searchForm) {
		return getInvestmentTypeSubTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	///////         Investment Sub Type 2 Business Methods            //////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public InvestmentTypeSubType2 getInvestmentTypeSubType2(short id) {
		return getInvestmentTypeSubType2DAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentTypeSubType2> getInvestmentTypeSubType2ListByType(short investmentTypeId) {
		return getInvestmentTypeSubType2DAO().findByField("investmentType.id", investmentTypeId);
	}


	@Override
	public List<InvestmentTypeSubType2> getInvestmentTypeSubType2List(InvestmentSubTypeSearchForm searchForm) {
		return getInvestmentTypeSubType2DAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	///////         Investment Hierarchy Business Methods             //////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public InvestmentInstrumentHierarchy getInvestmentInstrumentHierarchy(short id) {
		return getInvestmentInstrumentHierarchyDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentInstrumentHierarchy> getInvestmentInstrumentHierarchyListByIds(List<Short> ids) {
		if (!CollectionUtils.isEmpty(ids)) {
			return getInvestmentInstrumentHierarchyDAO().findByPrimaryKeys(ids.toArray(new Short[0]));
		}
		return Collections.emptyList();
	}


	@Override
	public List<InvestmentInstrumentHierarchy> getInvestmentInstrumentHierarchyList(final InstrumentHierarchySearchForm searchForm) {
		// Default Sorting if none defined (an not an explicit parent to avoid extra unnecessary joins)
		if (StringUtils.isEmpty(searchForm.getOrderBy()) && searchForm.getParentId() == null) {
			searchForm.setOrderBy("labelExpanded");
		}
		return getInvestmentInstrumentHierarchyDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional(readOnly = true)
	public List<InvestmentInstrumentHierarchy> getInvestmentInstrumentHierarchyListDeep(short hierarchyId) {
		List<InvestmentInstrumentHierarchy> result = CollectionUtils.createList(getInvestmentInstrumentHierarchy(hierarchyId));

		Serializable[] childIds = getInvestmentInstrumentHierarchyByParentCache().getBeanIdListForKeyValue(getInvestmentInstrumentHierarchyDAO(), hierarchyId);
		ArrayUtils.getStream(childIds).forEach(childId -> result.addAll(getInvestmentInstrumentHierarchyListDeep((Short) childId)));

		return result;
	}


	@Override
	public InvestmentInstrumentHierarchy saveInvestmentInstrumentHierarchy(InvestmentInstrumentHierarchy hierarchy) {
		if (hierarchy.isCurrency() && !hierarchy.isOneSecurityPerInstrument()) {
			throw new FieldValidationException("Physical currencies must be marked as having one security per instrument.", "currency");
		}
		if (hierarchy.isTradingDisallowed() && (!hierarchy.isOneSecurityPerInstrument() || hierarchy.isCurrency())) {
			throw new FieldValidationException("A security that cannot be traded must be marked as having one security per instrument and cannot be a currency.", "tradingDisallowed");
		}
		if (hierarchy.isAssignmentAllowed()) {
			ValidationUtils.assertNotNull(hierarchy.getInvestmentType(), "Investment type is required for hierarchies that allow instrument assignment.", "investmentType");
			ValidationUtils.assertNotNull(hierarchy.getInstrumentScreen(), "Investment screen is required for hierarchies that allow instrument assignment.", "investmentType");
			ValidationUtils.assertNotNull(hierarchy.getInstrumentDeliverableType(), "Deliverable Type is required for hierarchies that allow instrument assignment.", "instrumentDeliverableType");
			ValidationUtils.assertNotNull(hierarchy.getPricingFrequency(), "Pricing Frequency is required for hierarchies that allow instrument assignment", "pricingFrequency");
		}
		if (hierarchy.isReferenceSecurityAllowed()) {
			ValidationUtils.assertNotNull(hierarchy.getReferenceSecurityLabel(), "Reference Security Label is required when Reference Security is Allowed", "referenceSecurityLabel");
			ValidationUtils.assertNotNull(hierarchy.getReferenceSecurityTooltip(), "Reference Security Tooltip is required when Reference Security is Allowed", "referenceSecurityTooltip");
		}
		else {
			ValidationUtils.assertFalse(hierarchy.isReferenceSecurityFromSameInstrument(), "Reference Security From Same Instrument flag is not allowed when Reference Security is not Allowed");
			ValidationUtils.assertFalse(hierarchy.isReferenceSecurityChildNotInstructedForEvents(), "Reference Security Child Not Instructed For Events flag is not allowed when Reference Security is not Allowed");
			ValidationUtils.assertNull(hierarchy.getReferenceSecurityLabel(), "Reference Security Label is not allowed when Reference Security is not Allowed", "referenceSecurityLabel");
			ValidationUtils.assertNull(hierarchy.getReferenceSecurityTooltip(), "Reference Security Tooltip is not allowed when Reference Security is not Allowed", "referenceSecurityTooltip");
		}
		if (hierarchy.getAccrualSecurityEventType2() != null) {
			ValidationUtils.assertNotNull(hierarchy.getAccrualSecurityEventType(), "Accrual Security Event Type 2 can only be specified when there is Accrual Security Event Type specified.",
					"accrualSecurityEventType2");
		}
		if (hierarchy.isNotASecurity()) {
			ValidationUtils.assertTrue(hierarchy.isTradingDisallowed(), "In order to flag the hierarchy as not containing securities, the hierarchy must also be flagged as non-tradable.",
					"tradingDisallowed");
		}
		if (hierarchy.getDynamicAllocationCalculatorSystemBean() != null) {
			ValidationUtils.assertTrue(hierarchy.isAllocationOfSecurities(), "Dynamic Allocation Calculator Bean is not allowed if the hierarchy is not an allocation of securities.");
			ValidationUtils.assertFalse(hierarchy.getSecurityAllocationType().isAutoRebalancingSupported(), "Dynamic Allocation Calculator Bean is not allowed if the security allocation type supports automatic rebalancing.");
		}
		if (hierarchy.isSecurityPriceMultiplierOverrideAllowed()) {
			if (hierarchy.isOneSecurityPerInstrument()) {
				ValidationUtils.assertNotNull(hierarchy.getPriceMultiplier(), "For one-to-one securities, price multiplier overrides are only applicable if there is a price multiplier entered on the hierarchy.");
			}
			else {
				ValidationUtils.assertNull(hierarchy.getPriceMultiplier(), "For one-to-many securities, Price Multiplier cannot be entered on the hierarchy if allowing security level price multiplier overrides.");
			}
		}
		if (hierarchy.getCashLocationPurpose2() != null) {
			ValidationUtils.assertNotNull(hierarchy.getCashLocationPurpose(), "'Cash Location Purpose' is required when 'Cash Location Purpose 2' is defined.", "cashLocationPurpose");
		}
		InvestmentUtils.validatePriceMultiplierValue(hierarchy.getPriceMultiplier());
		return getInvestmentInstrumentHierarchyDAO().save(hierarchy);
	}


	@Override
	@Transactional
	public InvestmentInstrumentHierarchy copyInvestmentInstrumentHierarchy(short fromHierarchyId, short newParentHierarchyId, String newName, String newLabel, String newDescription) {
		// validate inputs first
		InvestmentInstrumentHierarchy hierarchy = getInvestmentInstrumentHierarchy(fromHierarchyId);
		ValidationUtils.assertNotNull(hierarchy, "Cannot find InvestmentInstrumentHierarchy with id = " + fromHierarchyId);
		InvestmentInstrumentHierarchy newParent = getInvestmentInstrumentHierarchy(newParentHierarchyId);
		ValidationUtils.assertNotNull(newParent, "Cannot find new parent InvestmentInstrumentHierarchy with id = " + newParentHierarchyId);
		ValidationUtils.assertNotEmpty(newName, "'New Name' is required to copy a hierarchy");
		ValidationUtils.assertNotEmpty(newLabel, "'New Label' is required to copy a hierarchy");
		ValidationUtils.assertNotEmpty(newDescription, "'New Description' is required to copy a hierarchy");

		// create a copy of hierarchy with new attributes
		InvestmentInstrumentHierarchy newHierarchy = BeanUtils.cloneBean(hierarchy, false, false);
		newHierarchy.setParent(newParent);
		newHierarchy.setName(newName);
		newHierarchy.setLabel(newLabel);
		newHierarchy.setDescription(newDescription);
		newHierarchy = getInvestmentInstrumentHierarchyDAO().save(newHierarchy);

		// also copy custom fields if any: security + instrument
		getSystemColumnService().copySystemColumnCustomListForLinkedValue(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, hierarchy.getId().toString(), newHierarchy.getId().toString(), newHierarchy.getLabelExpanded());
		getSystemColumnService().copySystemColumnCustomListForLinkedValue(InvestmentInstrument.INSTRUMENT_CUSTOM_FIELDS_GROUP_NAME, hierarchy.getId().toString(), newHierarchy.getId().toString(), newHierarchy.getLabelExpanded());

		// also copy event type mappings
		List<InvestmentSecurityEventTypeHierarchy> mappingList = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeHierarchyListByHierarchy(fromHierarchyId);
		for (InvestmentSecurityEventTypeHierarchy mapping : CollectionUtils.getIterable(mappingList)) {
			InvestmentSecurityEventTypeHierarchy newMapping = BeanUtils.cloneBean(mapping, false, false);
			newMapping.setReferenceTwo(newHierarchy);
			getInvestmentSecurityEventService().saveInvestmentSecurityEventTypeHierarchy(newMapping);
		}

		return newHierarchy;
	}


	@Override
	public void validateInvestmentInstrumentHierarchyMove(int instrumentId, InvestmentInstrumentHierarchy fromHierarchy, InvestmentInstrumentHierarchy toHierarchy, boolean bypassCustomColumnValidation) {
		ValidationUtils.assertNotNull(fromHierarchy, "From Hierarchy is missing");
		ValidationUtils.assertNotNull(toHierarchy, "To Hierarchy is missing");
		ValidationUtils.assertFalse(fromHierarchy.equals(toHierarchy), "From and To Hierarchies are the same.  Nothing to move.");

		String messagePrefix = "Cannot move from hierarchy [" + fromHierarchy.getNameExpanded() + "] to hierarchy [" + toHierarchy.getNameExpanded() + "] because: ";
		ValidationUtils.assertEquals(fromHierarchy.isOneSecurityPerInstrument(), toHierarchy.isOneSecurityPerInstrument(), messagePrefix + " both hierarchies do not match on option [Each instrument always has one security: One-to-One].");

		// VALIDATE EVENT TYPE MAPPINGS
		List<InvestmentSecurityEventTypeHierarchy> fromEventMappingList = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeHierarchyListByHierarchy(fromHierarchy.getId());
		// NOTE - EVENT TYPE MAPPINGS DON'T HAVE TO BE THE SAME, BUT TO HIERARCHY MUST HAVE THE SAME OR GREATER EVENT TYPES MAPPED
		// So, if from doesn't have any - no additional validation for event types at that point
		// If from does - compare event types to make sure everything from has, to has as well
		if (!CollectionUtils.isEmpty(fromEventMappingList)) {
			List<InvestmentSecurityEventTypeHierarchy> toEventMappingList = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeHierarchyListByHierarchy(toHierarchy.getId());
			for (InvestmentSecurityEventTypeHierarchy fromMapping : CollectionUtils.getIterable(fromEventMappingList)) {
				boolean found = false;
				for (InvestmentSecurityEventTypeHierarchy toMapping : CollectionUtils.getIterable(toEventMappingList)) {
					if (fromMapping.getReferenceOne().equals(toMapping.getReferenceOne())) {
						found = true;
						break;
					}
				}
				// If not found, then only throw exception if there are any actual events that would be affected
				if (!found) {
					InvestmentSecurityEventSearchForm eventSearchForm = new InvestmentSecurityEventSearchForm();
					eventSearchForm.setSecurityInstrumentId(instrumentId);
					eventSearchForm.setTypeId(fromMapping.getReferenceOne().getId());
					if (!CollectionUtils.isEmpty(getInvestmentSecurityEventService().getInvestmentSecurityEventList(eventSearchForm))) {
						throw new ValidationException(messagePrefix + " from hierarchy has event type mapping for [" + fromMapping.getReferenceOne().getName() + "] with events associated with it on the selected instrument, but to hierarchy does not.");
					}
				}
			}
		}
		if (!bypassCustomColumnValidation) {
			try {
				// Note: This validation is also done when the fields are actually moved, but better to check first before we start updating
				getSystemColumnService().validateSystemColumnCustomListSameForLinkedValues(InvestmentInstrument.INSTRUMENT_CUSTOM_FIELDS_GROUP_NAME, fromHierarchy.getId() + "", toHierarchy.getId() + "", messagePrefix, bypassCustomColumnValidation);
				getSystemColumnService().validateSystemColumnCustomListSameForLinkedValues(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, fromHierarchy.getId() + "", toHierarchy.getId() + "", messagePrefix, bypassCustomColumnValidation);
			}
			catch (ValidationException e) {
				throw new UserIgnorableValidationException(e.getMessage());
			}
		}
	}


	@Override
	public void deleteInvestmentInstrumentHierarchy(short id) {
		getInvestmentInstrumentHierarchyDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////        Investment Asset Class Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public InvestmentAssetClass getInvestmentAssetClass(short id) {
		return getInvestmentAssetClassDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentAssetClass> getInvestmentAssetClassList(InvestmentAssetClassSearchForm searchForm) {
		return getInvestmentAssetClassDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	// Validation: See {@link InvestmentAssetClassValidator}
	@Override
	public InvestmentAssetClass saveInvestmentAssetClass(InvestmentAssetClass bean) {
		return getInvestmentAssetClassDAO().save(bean);
	}


	@Override
	public void deleteInvestmentAssetClass(short id) {
		getInvestmentAssetClassDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	///////                 Getter/Setter Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentInstrumentHierarchy, Criteria> getInvestmentInstrumentHierarchyDAO() {
		return this.investmentInstrumentHierarchyDAO;
	}


	public void setInvestmentInstrumentHierarchyDAO(AdvancedUpdatableDAO<InvestmentInstrumentHierarchy, Criteria> investmentInstrumentHierarchyDAO) {
		this.investmentInstrumentHierarchyDAO = investmentInstrumentHierarchyDAO;
	}


	public AdvancedUpdatableDAO<InvestmentAssetClass, Criteria> getInvestmentAssetClassDAO() {
		return this.investmentAssetClassDAO;
	}


	public void setInvestmentAssetClassDAO(AdvancedUpdatableDAO<InvestmentAssetClass, Criteria> investmentAssetClassDAO) {
		this.investmentAssetClassDAO = investmentAssetClassDAO;
	}


	public ReadOnlyDAO<InvestmentType> getInvestmentTypeDAO() {
		return this.investmentTypeDAO;
	}


	public void setInvestmentTypeDAO(ReadOnlyDAO<InvestmentType> investmentTypeDAO) {
		this.investmentTypeDAO = investmentTypeDAO;
	}


	public AdvancedUpdatableDAO<InvestmentTypeSubType, Criteria> getInvestmentTypeSubTypeDAO() {
		return this.investmentTypeSubTypeDAO;
	}


	public void setInvestmentTypeSubTypeDAO(AdvancedUpdatableDAO<InvestmentTypeSubType, Criteria> investmentTypeSubTypeDAO) {
		this.investmentTypeSubTypeDAO = investmentTypeSubTypeDAO;
	}


	public AdvancedUpdatableDAO<InvestmentTypeSubType2, Criteria> getInvestmentTypeSubType2DAO() {
		return this.investmentTypeSubType2DAO;
	}


	public void setInvestmentTypeSubType2DAO(AdvancedUpdatableDAO<InvestmentTypeSubType2, Criteria> investmentTypeSubType2DAO) {
		this.investmentTypeSubType2DAO = investmentTypeSubType2DAO;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public DaoNamedEntityCache<InvestmentType> getInvestmentTypeCache() {
		return this.investmentTypeCache;
	}


	public void setInvestmentTypeCache(DaoNamedEntityCache<InvestmentType> investmentTypeCache) {
		this.investmentTypeCache = investmentTypeCache;
	}


	public DaoSingleKeyListCache<InvestmentInstrumentHierarchy, Short> getInvestmentInstrumentHierarchyByParentCache() {
		return this.investmentInstrumentHierarchyByParentCache;
	}


	public void setInvestmentInstrumentHierarchyByParentCache(DaoSingleKeyListCache<InvestmentInstrumentHierarchy, Short> investmentInstrumentHierarchyByParentCache) {
		this.investmentInstrumentHierarchyByParentCache = investmentInstrumentHierarchyByParentCache;
	}
}
