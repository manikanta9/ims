package com.clifton.investment.setup;


import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;


/**
 * The <code>InvestmentAssetClass</code> ...
 * <p/>
 * NOTE: The concept of "Parent" Asset class here is not really used, however we can't delete because it's included in the unique index.
 * At this time it's not possible to merge those with the same name together, because Lorillard for example uses the same "asset class name"
 * Global Equity (as Global Equity and Global Equity / Global Equity) as two separate account asset classes, which cannot reference the same asset class.
 *
 * @author Mary Anderson
 */
public class InvestmentAssetClass extends NamedHierarchicalEntity<InvestmentAssetClass, Short> {

	/**
	 * Master asset classes are used to group asset classes into smaller groups
	 * for global reporting purposes so that we can show breakdowns across accounts as a more consolidates grouping level
	 */
	private InvestmentAssetClass masterAssetClass;

	/**
	 * If true, this is a master asset class, else master asset class selection is required
	 */
	private boolean master;

	private String shortLabel;

	private boolean cash;

	/**
	 * For PIOS: Main cash asset class is used to accumulate cash balances from non-cash asset classes
	 */
	private boolean mainCash;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isCash() {
		return this.cash;
	}


	public void setCash(boolean cash) {
		this.cash = cash;
	}


	public boolean isMainCash() {
		return this.mainCash;
	}


	public void setMainCash(boolean mainCash) {
		this.mainCash = mainCash;
	}


	public String getShortLabel() {
		return this.shortLabel;
	}


	public void setShortLabel(String shortLabel) {
		this.shortLabel = shortLabel;
	}


	public InvestmentAssetClass getMasterAssetClass() {
		return this.masterAssetClass;
	}


	public void setMasterAssetClass(InvestmentAssetClass masterAssetClass) {
		this.masterAssetClass = masterAssetClass;
	}


	public boolean isMaster() {
		return this.master;
	}


	public void setMaster(boolean master) {
		this.master = master;
	}
}
