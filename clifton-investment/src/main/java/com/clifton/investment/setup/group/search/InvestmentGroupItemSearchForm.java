package com.clifton.investment.setup.group.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>InvestmentGroupItemSearchForm</code> class defines search configuration for InvestmentGroupItem objects.
 *
 * @author Mary Anderson
 */
public class InvestmentGroupItemSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description,group.name", sortField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField
	private Integer order;

	@SearchField(searchField = "group.id")
	private Short groupId;

	@SearchField(searchFieldPath = "group", searchField = "templateInvestmentGroup.id")
	private Short groupTemplateId;

	@SearchField(searchField = "parent.id")
	private Integer parentId;

	@SearchField(searchField = "parent.id", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean rootNodesOnly;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getGroupId() {
		return this.groupId;
	}


	public void setGroupId(Short groupId) {
		this.groupId = groupId;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public Boolean getRootNodesOnly() {
		return this.rootNodesOnly;
	}


	public void setRootNodesOnly(Boolean rootNodesOnly) {
		this.rootNodesOnly = rootNodesOnly;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Short getGroupTemplateId() {
		return this.groupTemplateId;
	}


	public void setGroupTemplateId(Short groupTemplateId) {
		this.groupTemplateId = groupTemplateId;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}
}
