package com.clifton.investment.setup.group;

public enum InvestmentCurrencyTypes {
	DOMESTIC(true, false), INTERNATIONAL(false, true), BOTH(true, true);


	InvestmentCurrencyTypes(boolean includeBaseCurrency, boolean includeForeignCurrency) {
		this.includeBaseCurrency = includeBaseCurrency;
		this.includeForeignCurrency = includeForeignCurrency;
	}


	private final boolean includeBaseCurrency;
	private final boolean includeForeignCurrency;


	public boolean isIncludeBaseCurrency() {
		return this.includeBaseCurrency;
	}


	public boolean isIncludeForeignCurrency() {
		return this.includeForeignCurrency;
	}
}
