package com.clifton.investment.setup.group.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupItem;
import com.clifton.investment.setup.group.InvestmentGroupMatrix;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.InvestmentGroupType;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>InvestmentGroupValidator</code> handles validating an {@link InvestmentGroup} prior to saving in the database.
 *
 * @author manderson
 */
@Component
public class InvestmentGroupObserver extends SelfRegisteringDaoObserver<InvestmentGroup> {

	private UpdatableDAO<InvestmentGroupItem> investmentGroupItemDAO;
	private InvestmentGroupService investmentGroupService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<InvestmentGroup> dao, DaoEventTypes event, InvestmentGroup bean) {
		InvestmentGroup originalBean = null;
		if (event.isUpdate()) {
			originalBean = getOriginalBean(dao, bean);
		}

		validateInvestmentGroup(bean, originalBean, event);
	}


	///////////////////////////////////////////////////////


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<InvestmentGroup> dao, DaoEventTypes event, InvestmentGroup bean, Throwable e) {
		// Check Template for Rebuilding as long as no error and not deleting
		if (e == null && !event.isDelete()) {

			if (event.isInsert() && bean.getInvestmentGroupType().getLevelsDeep() != null && bean.getInvestmentGroupType().getLevelsDeep() == 1 && bean.getInvestmentGroupType().isSingleGroupItem()) {
				InvestmentGroupItem allInstruments = new InvestmentGroupItem();
				allInstruments.setName("All Instruments");
				allInstruments.setGroup(bean);
				allInstruments.setCurrencyType(bean.getInvestmentGroupType().getCurrencyType());
				this.investmentGroupService.saveInvestmentGroupItem(allInstruments);
			}

			// If new bean using a template, or template was added/changed need to rebuild
			if (bean.getTemplateInvestmentGroup() != null) {
				InvestmentGroup originalBean = null;
				if (event.isUpdate()) {
					originalBean = getOriginalBean(dao, bean);
				}
				if (originalBean == null || (!bean.getTemplateInvestmentGroup().equals(originalBean.getTemplateInvestmentGroup()))) {
					getInvestmentGroupService().rebuildInvestmentGroupItemList(bean.getId());
				}
			}
			// Otherwise, if and update and used as a template - copy changes (done by saving the group that uses it - validation below verifies changes are good and copies properties managed on the template)
			else if (event.isUpdate()) {
				List<InvestmentGroup> usedByList = dao.findByField("templateInvestmentGroup.id", bean.getId());
				for (InvestmentGroup usedBy : CollectionUtils.getIterable(usedByList)) {
					((UpdatableDAO<InvestmentGroup>) dao).save(usedBy);
				}
			}
		}
	}


	///////////////////////////////////////////////////////
	//////               Validate Methods           ///////
	///////////////////////////////////////////////////////


	private void validateInvestmentGroup(InvestmentGroup bean, InvestmentGroup originalBean, DaoEventTypes event) {
		// System Defined Validation - Applies to all groups
		validateSystemDefined(bean, originalBean, event);

		validateGroupTypeFields(bean, originalBean, event);

		// Validate Group Properties When NOT using a Template
		if (bean.getTemplateInvestmentGroup() == null) {
			validateInvestmentGroupProperties(bean, originalBean, event);
		}
		else {
			validateInvestmentGroupWithTemplateProperties(bean, event);
		}
	}


	private void validateGroupTypeFields(InvestmentGroup bean, InvestmentGroup originalBean, DaoEventTypes event) {
		InvestmentGroupType groupType = bean.getInvestmentGroupType();
		ValidationUtils.assertNotNull(groupType, "Group must have a Group Type defined.");
		// if new event, copy properties from group type
		if (event.isInsert()) {
			if (groupType.isSingleGroupItem()) {
				bean.setLevelsDeep(1);
			}
			else if (groupType.getLevelsDeep() != null) {
				bean.setLevelsDeep(groupType.getLevelsDeep());
			}
			bean.setMissingInstrumentNotAllowed(groupType.isMissingNotAllowed());
			bean.setLeafAssignmentOnly(groupType.isLeafAssignmentsOnly());
		}
		// If update, make sure group type wasn't changed
		else if (event.isUpdate()) {
			ValidationUtils.assertEquals(groupType.getId(), originalBean.getInvestmentGroupType().getId(), "Group type cannot be changed once group is created.");

			// If update, ensure group properties match group type properties
			// Make sure that if group type has a field set to true, the group can't override it.
			String message = "Investment Group property cannot override type property: Group: [" + bean + "], Group Type: [" + groupType + "].";
			if (groupType.isSingleGroupItem()) {
				ValidationUtils.assertEquals(bean.getLevelsDeep(), 1, "Group of type: [" + groupType.getName() + "] must only be 1 level deep: [" + bean + "].");
			}
			else if (groupType.getLevelsDeep() != null) {
				ValidationUtils.assertEquals(bean.getLevelsDeep(), groupType.getLevelsDeep(), message);
			}
			if (groupType.isLeafAssignmentsOnly()) {
				ValidationUtils.assertTrue(bean.isLeafAssignmentOnly(), message);
			}
			if (groupType.isMissingNotAllowed()) {
				ValidationUtils.assertTrue(bean.isMissingInstrumentNotAllowed(), message);
			}
		}
		if (groupType.isBasedOnTemplate()) {
			ValidationUtils.assertNotNull(bean.getTemplateInvestmentGroup(), "Template is required for groups of type: " + groupType.getLabel());
		}
		else {
			ValidationUtils.assertNull(bean.getTemplateInvestmentGroup(), "Template is not allowed for groups of type: " + groupType.getLabel());
		}

		if (groupType.isBaseCurrencyRequired()) {
			ValidationUtils.assertNotNull(bean.getBaseCurrency(), "Base currency is required for groups of type: " + groupType.getLabel());
		}
	}


	private void validateInvestmentGroupProperties(InvestmentGroup bean, InvestmentGroup originalBean, DaoEventTypes event) {
		// Validate Updates to InvestmentGroups that don't use a template
		if (event.isUpdate()) {
			// Need to validate levels deep only if being updated to a value that is less than what it was.
			if (originalBean.getLevelsDeep() > bean.getLevelsDeep()) {
				List<InvestmentGroupItem> itemList = getInvestmentGroupItemDAO().findByField("group.id", bean.getId());
				for (InvestmentGroupItem item : CollectionUtils.getIterable(itemList)) {
					if (item.getLevel() > bean.getLevelsDeep()) {
						throw new FieldValidationException("There exists group items that are levels greater than the entered maximum of [" + bean.getLevelsDeep() + "]", "levelsDeep");
					}
				}
			}

			// If changing to IsLeafAssignmentOnly and assignments exists that are tied to non leaf items - throw an exception
			//
			// By the time we get here, we will have already validated that that the group and group type properties match/override, so we can just look at the bean's value.
			// At this point, we know that bean's LeafAssignmentOnly field is valid, so we only need to see if it's changed
			if (bean.isLeafAssignmentOnly() && !originalBean.isLeafAssignmentOnly()) {
				List<InvestmentGroupMatrix> matrixList = this.investmentGroupService.getInvestmentGroupMatrixListByGroup(bean.getId());
				if (!CollectionUtils.isEmpty(matrixList)) {
					List<InvestmentGroupItem> itemList = this.investmentGroupItemDAO.findByField("parent.id", BeanUtils.getPropertyValues(matrixList, "groupItem.id"));
					if (!CollectionUtils.isEmpty(itemList)) {
						throw new FieldValidationException("There are assignments that are currently tied to non-leaf group items.  Cannot change this group to leaf assignments only",
								"leafAssignmentOnly");
					}
				}
			}

			// If changing to IsTradableSecurityOnly and assignments exists that are tied to trading disallowed hierarchies = throw an exception
			if (bean.isTradableSecurityOnly() && !originalBean.isTradableSecurityOnly()) {
				// Any item not tied to a type and not excluded needs to be validated
				List<InvestmentGroupMatrix> matrixList = this.investmentGroupService.getInvestmentGroupMatrixListByGroup(bean.getId());
				matrixList = BeanUtils.filter(matrixList, matrix -> !matrix.isExcluded());
				List<InvestmentGroupMatrix> hierarchyList = BeanUtils.filter(matrixList, matrix -> matrix.getHierarchy() != null && matrix.getHierarchy().isTradingDisallowed());
				List<InvestmentGroupMatrix> instrumentList = BeanUtils.filter(matrixList, matrix -> matrix.getInstrument() != null && matrix.getInstrument().getHierarchy().isTradingDisallowed());

				if (!CollectionUtils.isEmpty(hierarchyList)) {
					throw new FieldValidationException("Cannot change this group to tradable security only. There are assignments that are currently tied to hierarchies that do not allow trading: ["
							+ BeanUtils.getPropertyValues(hierarchyList, "nameExpanded", ",", "", "", "...", 5) + "].", "tradableSecurityOnly");
				}
				if (!CollectionUtils.isEmpty(instrumentList)) {
					throw new FieldValidationException("Cannot change this group to tradable security only. There are assignments that are currently tied to instruments that do not allow trading: ["
							+ BeanUtils.getPropertyValues(instrumentList, "label", ",", "", "", "...", 5) + "].", "tradableSecurityOnly");
				}
			}
		}
	}


	private void validateSystemDefined(InvestmentGroup bean, InvestmentGroup originalBean, DaoEventTypes event) throws ValidationException {
		if (bean.isSystemDefined()) {
			if (event.isDelete()) {
				throw new ValidationException("Deleting System Defined Investment Groups is not allowed.");
			}
			if (event.isInsert()) {
				throw new ValidationException("Adding new System Defined Investment Groups is not allowed.");
			}
		}
		if (event.isUpdate()) {
			if (originalBean.isSystemDefined() != bean.isSystemDefined()) {
				throw new FieldValidationException("You cannot edit the System Defined field for Investment Groups", "systemDefined");
			}
			if (originalBean.isSystemDefined()) {
				ValidationUtils.assertTrue(originalBean.getName().equals(bean.getName()), "System Defined Investment Group Names cannot be changed.", "name");
			}
		}
	}


	private void validateInvestmentGroupWithTemplateProperties(InvestmentGroup bean, DaoEventTypes event) {
		// No Special Additional Validation
		if (event.isDelete()) {
			return;
		}
		InvestmentGroup template = bean.getTemplateInvestmentGroup();

		ValidationUtils.assertNull(template.getTemplateInvestmentGroup(), "Cannot use investment group [" + template.getName() + "] as a template because it uses a template.");
		ValidationUtils.assertNotNull(template.getBaseCurrency(), "Cannot use investment group [" + template.getName() + "] as a template because is does not have a base CCY defined.");
		ValidationUtils.assertNotNull(bean.getBaseCurrency(), "Base CCY is required for groups that use a template.", "baseCurrency.id");
		ValidationUtils.assertFalse(template.getBaseCurrency().equals(bean.getBaseCurrency()), "Base CCY cannot be the same as the template.  Please select a different Base CCY", "baseCurrency.id");

		copyInvestmentGroupTemplateProperties(template, bean);
	}


	private void copyInvestmentGroupTemplateProperties(InvestmentGroup template, InvestmentGroup group) {
		// Copy Properties Managed on the Template
		group.setDuplicationAllowed(template.isDuplicationAllowed());
		group.setLeafAssignmentOnly(template.isLeafAssignmentOnly());
		group.setLevelsDeep(template.getLevelsDeep());
		group.setTradableSecurityOnly(template.isTradableSecurityOnly());
		group.setMissingInstrumentNotAllowed(template.isMissingInstrumentNotAllowed());
	}


	///////////////////////////////////////////////////////
	/////             Getter & Setter Methods        //////
	///////////////////////////////////////////////////////


	public UpdatableDAO<InvestmentGroupItem> getInvestmentGroupItemDAO() {
		return this.investmentGroupItemDAO;
	}


	public void setInvestmentGroupItemDAO(UpdatableDAO<InvestmentGroupItem> investmentGroupItemDAO) {
		this.investmentGroupItemDAO = investmentGroupItemDAO;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}
}
