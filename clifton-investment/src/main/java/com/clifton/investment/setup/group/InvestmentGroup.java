package com.clifton.investment.setup.group;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>InvestmentGroup</code> are used to classify {@link InvestmentInstrument}s for reporting purposes.
 * <p/>
 * Asset Classes use groups for classification.
 *
 * @author manderson
 */
@CacheByName
public class InvestmentGroup extends NamedEntity<Short> implements SystemEntityModifyConditionAware {

	public static final String INVESTMENT_GROUP_TABLE_NAME = "InvestmentGroup";

	////////////////////////////////////////////////////////////////////////////////

	private InvestmentGroupType investmentGroupType;


	/**
	 * How many levels deep the items for this
	 * group can be.
	 */
	private int levelsDeep;

	private boolean duplicationAllowed;

	private boolean systemDefined;

	/**
	 * If true, then group matrix entries can only be tied to
	 * group items that are "leaves"
	 */
	private boolean leafAssignmentOnly;

	/**
	 * If true, limits the assignments for this investment group to
	 * be in a hierarchy where IsTradingDisallowed = false
	 */
	private boolean tradableSecurityOnly;

	/**
	 * If true, do not allow missing instrument (map everything)
	 */
	private boolean missingInstrumentNotAllowed;

	/**
	 * Available when leafAssignmentOnly = true
	 * Allows creating the group for a specific base currency.  Can reference a template (which copies the same group items and uses CurrencyType to determine whether or not to include it)
	 * Or can be used as a template for other groups.
	 */
	private InvestmentSecurity baseCurrency;

	/**
	 * The template group that is used to maintain group items.
	 * Both this group and it's template must have a base currency selected
	 * When selected - the group items are automatically created/updated/deleted when the
	 * template group items change.
	 * <p/>
	 * Example: Accounting Reporting USD - Template that uses USD as base currency
	 * Accounting Reporting CAD - Uses USD as it's template, but uses CAD as base currency.
	 * Group items are flagged with DOMESTIC, INT'L, or BOTH so when rebuilding the groups the currency type and the base currency is used to determine which instruments actually apply
	 */
	private InvestmentGroup templateInvestmentGroup;

	/**
	 * For Non-Admin users - specific condition that must evaluate to true in order for a user to be able to edit this category, its hierarchies, or assignments
	 */
	private SystemCondition entityModifyCondition;


	//////////////////////////////////////
	//////////////////////////////////////



	public boolean usesTemplate() {
		return getTemplateInvestmentGroup() != null;
	}


	//////////////////////////////////////
	//////////////////////////////////////


	public InvestmentGroupType getInvestmentGroupType() {
		return this.investmentGroupType;
	}


	public void setInvestmentGroupType(InvestmentGroupType investmentGroupType) {
		this.investmentGroupType = investmentGroupType;
	}


	public int getLevelsDeep() {
		return this.levelsDeep;
	}


	public void setLevelsDeep(int levelsDeep) {
		this.levelsDeep = levelsDeep;
	}


	public boolean isDuplicationAllowed() {
		return this.duplicationAllowed;
	}


	public void setDuplicationAllowed(boolean duplicationAllowed) {
		this.duplicationAllowed = duplicationAllowed;
	}


	public boolean isLeafAssignmentOnly() {
		return this.leafAssignmentOnly;
	}


	public void setLeafAssignmentOnly(boolean leafAssignmentOnly) {
		this.leafAssignmentOnly = leafAssignmentOnly;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isTradableSecurityOnly() {
		return this.tradableSecurityOnly;
	}


	public void setTradableSecurityOnly(boolean tradableSecurityOnly) {
		this.tradableSecurityOnly = tradableSecurityOnly;
	}


	public boolean isMissingInstrumentNotAllowed() {
		return this.missingInstrumentNotAllowed;
	}


	public void setMissingInstrumentNotAllowed(boolean missingInstrumentNotAllowed) {
		this.missingInstrumentNotAllowed = missingInstrumentNotAllowed;
	}


	public InvestmentSecurity getBaseCurrency() {
		return this.baseCurrency;
	}


	public void setBaseCurrency(InvestmentSecurity baseCurrency) {
		this.baseCurrency = baseCurrency;
	}


	public InvestmentGroup getTemplateInvestmentGroup() {
		return this.templateInvestmentGroup;
	}


	public void setTemplateInvestmentGroup(InvestmentGroup templateInvestmentGroup) {
		this.templateInvestmentGroup = templateInvestmentGroup;
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}
}
