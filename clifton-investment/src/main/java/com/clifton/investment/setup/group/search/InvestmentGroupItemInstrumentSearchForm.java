package com.clifton.investment.setup.group.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>InvestmentGroupItemInstrumentSearchForm</code> class defines search configuration for InvestmentGroupItemInstrument objects
 *
 * @author vgomelsky
 */
public class InvestmentGroupItemInstrumentSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceOne.name,referenceOne.group.name,referenceTwo.identifierPrefix,referenceTwo.name")
	private String searchPattern;

	@SearchField(searchField = "referenceOne.id")
	private Integer groupItemId;

	@SearchField(searchField = "group.id", searchFieldPath = "referenceOne")
	private Short groupId;

	@SearchField(searchField = "group.name", searchFieldPath = "referenceOne", comparisonConditions = {ComparisonConditions.EQUALS})
	private String groupName; // must use EXACT search to avoid multiple groups

	@SearchField(searchField = "group.name", searchFieldPath = "referenceOne")
	private String groupNameContains;

	@SearchField(searchField = "hierarchy.id", searchFieldPath = "referenceTwo")
	private Short instrumentHierarchyId;

	@SearchField(searchField = "referenceTwo.id")
	private Integer instrumentId;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo")
	private String instrumentName;

	@SearchField(searchField = "description", searchFieldPath = "referenceTwo")
	private String instrumentDescription;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "referenceTwo.hierarchy")
	private Short investmentTypeId;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "referenceTwo.hierarchy")
	private Short[] investmentTypeIds;


	///////////////////////////////////////////////////////////////
	/////////        Getter and Setter Methods           //////////
	///////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getGroupItemId() {
		return this.groupItemId;
	}


	public void setGroupItemId(Integer groupItemId) {
		this.groupItemId = groupItemId;
	}


	public Short getGroupId() {
		return this.groupId;
	}


	public void setGroupId(Short groupId) {
		this.groupId = groupId;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Integer getInstrumentId() {
		return this.instrumentId;
	}


	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}


	public String getInstrumentName() {
		return this.instrumentName;
	}


	public void setInstrumentName(String instrumentName) {
		this.instrumentName = instrumentName;
	}


	public String getInstrumentDescription() {
		return this.instrumentDescription;
	}


	public void setInstrumentDescription(String instrumentDescription) {
		this.instrumentDescription = instrumentDescription;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short[] getInvestmentTypeIds() {
		return this.investmentTypeIds;
	}


	public void setInvestmentTypeIds(Short[] investmentTypeIds) {
		this.investmentTypeIds = investmentTypeIds;
	}


	public String getGroupName() {
		return this.groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public String getGroupNameContains() {
		return this.groupNameContains;
	}


	public void setGroupNameContains(String groupNameContains) {
		this.groupNameContains = groupNameContains;
	}
}
