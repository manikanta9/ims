package com.clifton.investment.setup.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>InvestmentInstrumentHierarchyValidator</code> validates hierarchies:
 * On UPDATES - when an underlying hierarchy is selected or changed verifies there are no existing violations
 *
 * @author manderson
 */
@Component
public class InvestmentInstrumentHierarchyValidator extends SelfRegisteringDaoValidator<InvestmentInstrumentHierarchy> {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentGroupService investmentGroupService;
	private SystemBeanService systemBeanService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}


	@Override
	public void validate(InvestmentInstrumentHierarchy bean, DaoEventTypes config) throws ValidationException {
		// Only registered for updates, but in case ever changes this check applies to updates only
		if (config.isUpdate()) {
			InvestmentInstrumentHierarchy originalBean = getOriginalBean(bean);
			if (bean.getUnderlyingHierarchy() != null) {
				if (originalBean.getUnderlyingHierarchy() == null || !bean.getUnderlyingHierarchy().equals(originalBean.getUnderlyingHierarchy())) {
					InstrumentSearchForm searchForm = new InstrumentSearchForm();
					searchForm.setHierarchyId(bean.getId());
					searchForm.setUnderlyingHierarchyIdNotEqual(bean.getUnderlyingHierarchy().getId());
					List<InvestmentInstrument> list = getInvestmentInstrumentService().getInvestmentInstrumentList(searchForm);
					if (!CollectionUtils.isEmpty(list)) {
						throw new ValidationException("Cannot set underlying hierarchy to [" + bean.getUnderlyingHierarchy().getLabelExpanded() + "] because there are ["
								+ CollectionUtils.getSize(list) + "] instruments that have underlying instruments assigned to other hierarchies. Check the following instruments: "
								+ BeanUtils.getPropertyValues(list, "identifierPrefix", ",", null, null, "...", 5));
					}
				}
			}
			if (bean.getUnderlyingInvestmentGroup() != null) {
				if (originalBean.getUnderlyingInvestmentGroup() == null || !bean.getUnderlyingInvestmentGroup().equals(originalBean.getUnderlyingInvestmentGroup())) {
					InstrumentSearchForm searchForm = new InstrumentSearchForm();
					searchForm.setHierarchyId(bean.getId());
					List<InvestmentInstrument> list = getInvestmentInstrumentService().getInvestmentInstrumentList(searchForm);
					if (!CollectionUtils.isEmpty(list)) {
						List<InvestmentInstrument> violationList = new ArrayList<>();
						for (InvestmentInstrument instrument : list) {
							if (instrument.getUnderlyingInstrument() != null && !getInvestmentGroupService().isInvestmentInstrumentInGroup(bean.getUnderlyingInvestmentGroup().getName(), instrument.getUnderlyingInstrument().getId())) {
								violationList.add(instrument);
							}
						}
						if (!violationList.isEmpty()) {
							throw new ValidationException("Cannot set underlying instrument group to [" + bean.getUnderlyingInvestmentGroup().getName() + "] because there are ["
									+ CollectionUtils.getSize(violationList) + "] instruments that have underlying instruments not in that group. Check the following instruments: "
									+ BeanUtils.getPropertyValues(violationList, "identifierPrefix", ",", null, null, "...", 5));
						}
					}
				}
			}
			if (!bean.isSecurityPriceMultiplierOverrideAllowed()) {
				if (originalBean.isSecurityPriceMultiplierOverrideAllowed()) {
					// If One-To-Many and turning off price multiplier overrides, then make sure there aren't any securities with the override populated
					if (!bean.isOneSecurityPerInstrument()) {
						SecuritySearchForm securitySearchForm = new SecuritySearchForm();
						securitySearchForm.setHierarchyId(bean.getId());
						securitySearchForm.setPriceMultiplierOverridePopulated(true);
						List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(securitySearchForm);
						if (!CollectionUtils.isEmpty(securityList)) {
							throw new ValidationException("Cannot turn off security price multiplier override option because there are ["
									+ CollectionUtils.getSize(securityList) + "] securities that have price multiplier overrides populated. Check the following securities: "
									+ BeanUtils.getPropertyValues(securityList, "symbol", ",", null, null, "...", 5));
						}
					}
					// Else if One-To-One and turning price multiplier overrides off - and hierarchy price multiplier is still set - make sure all instruments have the same price multiplier as the hierarchy
					else if (bean.getPriceMultiplier() != null) {
						InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
						instrumentSearchForm.setHierarchyId(bean.getId());
						instrumentSearchForm.addSearchRestriction(new SearchRestriction("priceMultiplier", ComparisonConditions.NOT_EQUALS, bean.getPriceMultiplier()));
						List<InvestmentInstrument> instrumentList = getInvestmentInstrumentService().getInvestmentInstrumentList(instrumentSearchForm);
						if (!CollectionUtils.isEmpty(instrumentList)) {
							throw new ValidationException("Cannot turn off price multiplier override option with hierarchy price multiplier value of [" + CoreMathUtils.formatNumberDecimal(bean.getPriceMultiplier()) + "] because there are ["
									+ CollectionUtils.getSize(instrumentList) + "] instruments that have price multiplier values different than the hierarchy. Check the following instruments: "
									+ BeanUtils.getPropertyValues(instrumentList, "name", ",", null, null, "...", 5));
						}
					}
				}
			}

			NotionalMultiplierRetriever retriever = null;
			if (bean.getNotionalMultiplierRetriever() != null) {
				retriever = (NotionalMultiplierRetriever) getSystemBeanService().getBeanInstance(bean.getNotionalMultiplierRetriever());
			}

			if (bean.isIndexRatioAdjusted()) {
				// For index ratio adjusted securities, a notional multiplier retriever that calculates Index Ratio values is required.
				if (retriever == null) {
					throw new ValidationException("A Notional Multiplier Retriever which calculates an Index Ratio must be selected for inflation-linked securities.");
				}
			}

			// non-index-ratio-adjusted securities should only use Notional Multiplier Retrievers that calculate other values such as the "FV Notional Multiplier" for Zero Coupon Swaps.
			if (retriever != null) {
				retriever.validate(bean);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////          Getter and Setter Methods            ////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
