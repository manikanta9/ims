package com.clifton.investment.setup.group.cache;


import com.clifton.core.cache.LazilyBuiltCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.setup.group.InvestmentGroupItemInstrument;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.search.InvestmentGroupItemInstrumentSearchForm;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>InvestmentGroupSecurityCache</code> contains a lazily built cache for
 * a mapping of investment groups to a set of the instruments that fall within them.
 *
 * @author apopp
 */
@Component
public class InvestmentGroupInstrumentCacheImpl extends LazilyBuiltCache<Map<String, Set<Integer>>> implements InvestmentGroupInstrumentCache {

	private InvestmentGroupService investmentGroupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isInvestmentInstrumentInGroup(final String groupName, final int instrumentId) {
		return getGroupCache(groupName).contains(instrumentId);
	}


	@Override
	public void clearGroupCache(final String groupName) {
		getOrBuildCache().remove(groupName);
	}


	////////////////////////////////////////////////////////////////////////////////


	private Set<Integer> getGroupCache(final String groupName) {
		Set<Integer> groupCache = getOrBuildCache().get(groupName);

		if (groupCache == null) {
			buildGroupCache(groupName);
			return getOrBuildCache().get(groupName);
		}

		return groupCache;
	}


	private synchronized void buildGroupCache(final String groupName) {
		Set<Integer> groupCache = getOrBuildCache().get(groupName);

		if (groupCache == null) {
			Set<Integer> instrumentSet = Collections.synchronizedSet(new HashSet<>());
			InvestmentGroupItemInstrumentSearchForm searchForm = new InvestmentGroupItemInstrumentSearchForm();
			searchForm.setGroupName(groupName);

			List<InvestmentGroupItemInstrument> instrumentsInGroupList = getInvestmentGroupService().getInvestmentGroupItemInstrumentList(searchForm);

			for (InvestmentGroupItemInstrument itemInstrument : CollectionUtils.getIterable(instrumentsInGroupList)) {
				instrumentSet.add(itemInstrument.getReferenceTwo().getId());
			}

			getOrBuildCache().put(groupName, instrumentSet);
		}
	}


	@Override
	protected Map<String, Set<Integer>> buildCache() {
		return new ConcurrentHashMap<>();
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}
}
