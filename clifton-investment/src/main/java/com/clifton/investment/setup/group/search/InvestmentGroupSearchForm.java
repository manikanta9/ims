package com.clifton.investment.setup.group.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;


public class InvestmentGroupSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField(searchField = "name,description", sortField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = {ComparisonConditions.EQUALS})
	private String nameExact;

	@SearchField(searchField = "name", searchFieldPath = "entityModifyCondition")
	private String entityModifyCondition;

	@SearchField
	private String description;

	@SearchField(searchField = "investmentGroupType.id")
	private Short investmentGroupTypeId;

	@SearchField
	private Integer levelsDeep;

	@SearchField
	private Boolean duplicationAllowed;

	@SearchField
	private Boolean systemDefined;

	@SearchField
	private Boolean leafAssignmentOnly;

	@SearchField
	private Boolean tradableSecurityOnly;

	@SearchField
	private Boolean missingInstrumentNotAllowed;

	@SearchField(searchField = "baseCurrency.id")
	private Integer baseCurrencyId;

	@SearchField(searchFieldPath = "baseCurrency", searchField = "symbol")
	private String baseCurrencySymbol;

	@SearchField(searchField = "templateInvestmentGroup.id")
	private Short templateInvestmentGroupId;

	@SearchField(searchFieldPath = "templateInvestmentGroup", searchField = "name")
	private String templateInvestmentGroupName;

	@SearchField(searchField = "templateInvestmentGroup.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean hasTemplate;

	@SearchField(searchField = "baseCurrency.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean hasBaseCurrency;

	// Custom Search Field - Where Not Exists
	private Integer missingInstrumentId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return InvestmentGroup.INVESTMENT_GROUP_TABLE_NAME;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getInvestmentGroupTypeId() {
		return this.investmentGroupTypeId;
	}


	public void setInvestmentGroupTypeId(Short investmentGroupTypeId) {
		this.investmentGroupTypeId = investmentGroupTypeId;
	}


	public Integer getLevelsDeep() {
		return this.levelsDeep;
	}


	public void setLevelsDeep(Integer levelsDeep) {
		this.levelsDeep = levelsDeep;
	}


	public Boolean getDuplicationAllowed() {
		return this.duplicationAllowed;
	}


	public void setDuplicationAllowed(Boolean duplicationAllowed) {
		this.duplicationAllowed = duplicationAllowed;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getLeafAssignmentOnly() {
		return this.leafAssignmentOnly;
	}


	public void setLeafAssignmentOnly(Boolean leafAssignmentOnly) {
		this.leafAssignmentOnly = leafAssignmentOnly;
	}


	public Boolean getTradableSecurityOnly() {
		return this.tradableSecurityOnly;
	}


	public void setTradableSecurityOnly(Boolean tradableSecurityOnly) {
		this.tradableSecurityOnly = tradableSecurityOnly;
	}


	public Boolean getMissingInstrumentNotAllowed() {
		return this.missingInstrumentNotAllowed;
	}


	public void setMissingInstrumentNotAllowed(Boolean missingInstrumentNotAllowed) {
		this.missingInstrumentNotAllowed = missingInstrumentNotAllowed;
	}


	public Integer getBaseCurrencyId() {
		return this.baseCurrencyId;
	}


	public void setBaseCurrencyId(Integer baseCurrencyId) {
		this.baseCurrencyId = baseCurrencyId;
	}


	public String getBaseCurrencySymbol() {
		return this.baseCurrencySymbol;
	}


	public void setBaseCurrencySymbol(String baseCurrencySymbol) {
		this.baseCurrencySymbol = baseCurrencySymbol;
	}


	public Boolean getHasTemplate() {
		return this.hasTemplate;
	}


	public void setHasTemplate(Boolean hasTemplate) {
		this.hasTemplate = hasTemplate;
	}


	public Boolean getHasBaseCurrency() {
		return this.hasBaseCurrency;
	}


	public void setHasBaseCurrency(Boolean hasBaseCurrency) {
		this.hasBaseCurrency = hasBaseCurrency;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getTemplateInvestmentGroupName() {
		return this.templateInvestmentGroupName;
	}


	public void setTemplateInvestmentGroupName(String templateInvestmentGroupName) {
		this.templateInvestmentGroupName = templateInvestmentGroupName;
	}


	public Short getTemplateInvestmentGroupId() {
		return this.templateInvestmentGroupId;
	}


	public void setTemplateInvestmentGroupId(Short templateInvestmentGroupId) {
		this.templateInvestmentGroupId = templateInvestmentGroupId;
	}


	public String getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(String entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public String getNameExact() {
		return this.nameExact;
	}


	public void setNameExact(String nameExact) {
		this.nameExact = nameExact;
	}


	public Integer getMissingInstrumentId() {
		return this.missingInstrumentId;
	}


	public void setMissingInstrumentId(Integer missingInstrumentId) {
		this.missingInstrumentId = missingInstrumentId;
	}
}
