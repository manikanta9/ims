package com.clifton.investment.setup.group;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>InvestmentSecurityGroupInvestmentSecurity</code> maps
 * {@link InvestmentSecurityGroup} objects to {@link InvestmentSecurity} objects
 *
 * @author Mary Anderson
 */
public class InvestmentSecurityGroupSecurity extends ManyToManyEntity<InvestmentSecurityGroup, InvestmentSecurity, Integer> implements SystemEntityModifyConditionAware {

	/**
	 * Entity inherits the parent's {@link InvestmentSecurityGroup#getEntityModifyCondition()} if the parent group is not system managed.
	 * If the parent group is system managed, the modify condition is ignored (null) allowing the group to be rebuilt after individual {@link InvestmentSecurity} additions.
	 */
	@Override
	public SystemCondition getEntityModifyCondition() {
		return isEntityModifyConditionApplicable() ? getReferenceOne().getEntityModifyCondition() : null;
	}


	private boolean isEntityModifyConditionApplicable() {
		return (getReferenceOne() != null) && !getReferenceOne().isSystemManaged();
	}
}
