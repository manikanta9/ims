package com.clifton.investment.setup;


import com.clifton.core.beans.NamedEntityWithoutLabel;


/**
 * The <code>InvestmentTypeSubType</code> class defines investment sub-types.  Investment sub-types are optional.
 * Not every investment type has sub-types.  If investment sub-types are available for an investment type, then
 * investment sub-type can be selected at InvestmentInstrumentHierarchy.
 * <p>
 * Investment sub-types can be used in reporting and UI to identify similar securities that reside in different hierarchies.
 * <p>
 * Examples, Total Return Swaps, Interest Rate Swaps, Credit Default Swaps.
 *
 * @author vgomelsky
 */
public class InvestmentTypeSubType extends NamedEntityWithoutLabel<Short> {

	// Forwards sub types:
	public static final String DELIVERABLE_FORWARDS = "Deliverable Forwards";
	public static final String NON_DELIVERABLE_FORWARDS = "Non Deliverable Forwards";

	// Options sub types:
	public static final String OPTIONS_FUNDS = "Options On Funds";
	public static final String OPTIONS_INDEX = "Options On Indices";
	public static final String OPTIONS_ON_FUTURES = "Options On Futures";
	public static final String OPTIONS_SINGLE_NAME = "Options On Single Names";

	// Stocks sub types:
	public static final String CLOSED_END_FUNDS = "Closed End Funds";
	public static final String COMMON_STOCKS = "Common Stocks";
	public static final String PREFERRED_STOCKS = "Preferred Stocks";
	public static final String EQUITY_LINKED_NOTES = "Equity Linked Notes";
	public static final String EXCHANGE_TRADED_FUNDS = "Exchange Traded Funds";
	public static final String EXCHANGE_TRADED_NOTES = "Exchange Traded Notes";

	// Swaps sub types:
	public static final String CREDIT_DEFAULT_SWAPS = "Credit Default Swaps";
	public static final String INTEREST_RATE_SWAPS = "Interest Rate Swaps";
	public static final String TOTAL_RETURN_SWAPS = "Total Return Swaps";
	public static final String ZERO_COUPON_SWAPS = "Zero Coupon Swaps";

	////////////////////////////////////////////////////////////////////////////

	private InvestmentType investmentType;

	/**
	 * Industry standard name for "Quantity" (remaining quantity) field for securities of this type.
	 * For example, "Contracts" for Futures, "Shares" for Options, "Face" for bonds, etc.
	 * NOTE: these values override Investment Type and can be overridden at Investment Sub Type 2.
	 */
	private String quantityName;

	/**
	 * For securities with factor changes (ABS and CDS) it is the Original Face/Original Notional (Opening Quantity; not adjusted by current factor).
	 * For other securities it is the same as Remaining Quantity or Quantity (current number of units).
	 * NOTE: these values override Investment Type and can be overridden at Investment Sub Type 2.
	 */
	private String unadjustedQuantityName;

	// When applicable, define Investment Sub Type specific terminology for the following fields (optional overrides Investment Type names).
	private String costFieldName;
	private String costBasisFieldName;
	private String notionalFieldName;
	private String marketValueFieldName;

	/**
	 * The decimal precision for the quantity on trades and positions.  For example, for Futures it would be
	 * 0 and Bonds would be 2.  This value overrides Investment Type setting and can be overridden Investment Sub Type 2.
	 */
	private Short quantityDecimalPrecision;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentTypeSubType() {
		super();
	}


	public InvestmentTypeSubType(String name) {
		super();
		setName(name);
	}


	@Override
	public String getLabel() {
		if (this.investmentType == null) {
			return super.getLabel();
		}
		return super.getLabel() + " (" + this.investmentType.getLabel() + ")";
	}


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}


	public String getQuantityName() {
		return this.quantityName;
	}


	public void setQuantityName(String quantityName) {
		this.quantityName = quantityName;
	}


	public String getUnadjustedQuantityName() {
		return this.unadjustedQuantityName;
	}


	public void setUnadjustedQuantityName(String unadjustedQuantityName) {
		this.unadjustedQuantityName = unadjustedQuantityName;
	}


	public String getCostFieldName() {
		return this.costFieldName;
	}


	public void setCostFieldName(String costFieldName) {
		this.costFieldName = costFieldName;
	}


	public String getCostBasisFieldName() {
		return this.costBasisFieldName;
	}


	public void setCostBasisFieldName(String costBasisFieldName) {
		this.costBasisFieldName = costBasisFieldName;
	}


	public String getNotionalFieldName() {
		return this.notionalFieldName;
	}


	public void setNotionalFieldName(String notionalFieldName) {
		this.notionalFieldName = notionalFieldName;
	}


	public String getMarketValueFieldName() {
		return this.marketValueFieldName;
	}


	public void setMarketValueFieldName(String marketValueFieldName) {
		this.marketValueFieldName = marketValueFieldName;
	}


	public Short getQuantityDecimalPrecision() {
		return this.quantityDecimalPrecision;
	}


	public void setQuantityDecimalPrecision(Short quantityDecimalPrecision) {
		this.quantityDecimalPrecision = quantityDecimalPrecision;
	}
}
