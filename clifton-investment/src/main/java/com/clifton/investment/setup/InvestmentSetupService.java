package com.clifton.investment.setup;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.investment.setup.search.InstrumentHierarchySearchForm;
import com.clifton.investment.setup.search.InvestmentAssetClassSearchForm;
import com.clifton.investment.setup.search.InvestmentSubTypeSearchForm;

import java.util.List;


/**
 * The <code>InvestmentSetupService</code> defines the methods necessary for setting up Investments.
 *
 * @author manderson
 */
public interface InvestmentSetupService {

	//////////////////////////////////////////////////////////////////////////// 
	///////         Investment Type Business Methods                  ////////// 
	////////////////////////////////////////////////////////////////////////////


	public InvestmentType getInvestmentType(short id);


	public InvestmentType getInvestmentTypeByName(String name);

	public List<InvestmentType> getInvestmentTypeList();

	////////////////////////////////////////////////////////////////////////////
	///////         Investment Sub Type Business Methods              //////////
	////////////////////////////////////////////////////////////////////////////
	public InvestmentTypeSubType getInvestmentTypeSubType(short id);


	public List<InvestmentTypeSubType> getInvestmentTypeSubTypeListByType(short investmentTypeId);


	public List<InvestmentTypeSubType> getInvestmentTypeSubTypeList(InvestmentSubTypeSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	///////         Investment Sub Type 2 Business Methods            //////////
	////////////////////////////////////////////////////////////////////////////
	public InvestmentTypeSubType2 getInvestmentTypeSubType2(short id);


	public List<InvestmentTypeSubType2> getInvestmentTypeSubType2ListByType(short investmentTypeId);


	public List<InvestmentTypeSubType2> getInvestmentTypeSubType2List(InvestmentSubTypeSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	///////         Investment Hierarchy Business Methods             //////////
	////////////////////////////////////////////////////////////////////////////
	public InvestmentInstrumentHierarchy getInvestmentInstrumentHierarchy(short id);


	public List<InvestmentInstrumentHierarchy> getInvestmentInstrumentHierarchyListByIds(List<Short> ids);


	public List<InvestmentInstrumentHierarchy> getInvestmentInstrumentHierarchyList(InstrumentHierarchySearchForm searchForm);


	/**
	 * Returns a List of InvestmentInstrumentHierarchy objects, including the hierarchy for the given id
	 * that are direct children, or children of children of the given hierarchy.  Returns ALL children regardless
	 * of how deep the hierarchy is.
	 */
	public List<InvestmentInstrumentHierarchy> getInvestmentInstrumentHierarchyListDeep(short hierarchyId);


	public InvestmentInstrumentHierarchy saveInvestmentInstrumentHierarchy(InvestmentInstrumentHierarchy hierarchy);


	/**
	 * Creates a copy of the hierarchy specified by fromHierarchyId with the specified parameters.
	 * Also, copies custom fields and event type mappings from source hierarchy.
	 */
	public InvestmentInstrumentHierarchy copyInvestmentInstrumentHierarchy(short fromHierarchyId, short newParentHierarchyId, String newName, String newLabel, String newDescription);


	public void deleteInvestmentInstrumentHierarchy(short id);


	/**
	 * Used to validate hierarchies are the same (custom fields on instruments and securities) and event type mappings
	 * when moving an instrument to a different hierarchy
	 * <p>
	 * Missing event types are ignored if selected instrument doesn't have any actual events of that type
	 * <p>
	 * Not exposed via UI - called from {@link com.clifton.investment.instrument.InvestmentInstrumentService}
	 * If custom fields are being validated, and they don't match, a UserIgnorableValidationException is thrown, which allows calling method to bypass the validation
	 */
	@DoNotAddRequestMapping
	public void validateInvestmentInstrumentHierarchyMove(int instrumentId, InvestmentInstrumentHierarchy fromHierarchy, InvestmentInstrumentHierarchy toHierarchy, boolean bypassCustomColumnValidation);


	////////////////////////////////////////////////////////////////////////////
	///////        Investment Asset Class Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////
	public InvestmentAssetClass getInvestmentAssetClass(short id);


	public List<InvestmentAssetClass> getInvestmentAssetClassList(InvestmentAssetClassSearchForm searchForm);


	public InvestmentAssetClass saveInvestmentAssetClass(InvestmentAssetClass bean);


	public void deleteInvestmentAssetClass(short id);
}
