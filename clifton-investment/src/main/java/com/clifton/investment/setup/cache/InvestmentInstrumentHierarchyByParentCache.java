package com.clifton.investment.setup.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentInstrumentHierarchyByParentCache</code> caches a list of {@link InvestmentInstrumentHierarchy} objects by parent hierarchy.
 * *
 *
 * @author vgomelsky
 */
@Component
public class InvestmentInstrumentHierarchyByParentCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentInstrumentHierarchy, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "parent.id";
	}


	@Override
	protected Short getBeanKeyValue(InvestmentInstrumentHierarchy bean) {
		if (bean.getParent() != null) {
			return bean.getParent().getId();
		}
		return null;
	}
}

