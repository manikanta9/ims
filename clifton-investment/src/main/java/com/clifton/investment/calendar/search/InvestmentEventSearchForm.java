package com.clifton.investment.calendar.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.workflow.search.BaseWorkflowAwareSystemHierarchyItemSearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class InvestmentEventSearchForm extends BaseWorkflowAwareSystemHierarchyItemSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "eventType.id")
	private Short eventTypeId;

	@SearchField(searchField = "eventType.id")
	private Short[] eventTypeIds;

	@SearchField(searchField = "name", searchFieldPath = "eventType", comparisonConditions = {ComparisonConditions.EQUALS})
	private String eventTypeName;

	@SearchField(searchField = "order", searchFieldPath = "eventType")
	private Integer eventTypeOrder;

	@SearchField(searchField = "businessClient.id", searchFieldPath = "investmentAccount", sortField = "businessClient.name")
	private Integer businessClientId;

	@SearchField(searchField = "investmentAccount.id")
	private Integer investmentAccountId;

	// Custom Search Field 
	private Integer investmentAccountIdOrSubAccount;

	@SearchField(searchField = "number", searchFieldPath = "investmentAccount")
	private String investmentAccountNumber;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "investmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountGroupId;

	@SearchField(searchField = "investmentAccount.teamSecurityGroup.id,assigneeSecurityGroup.id", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.COALESCE_NULL)
	private Short teamSecurityGroupId;

	@SearchField(searchField = "investmentAccount.number,investmentAccount.shortName,investmentAccount.name,assigneeSecurityGroup.name", leftJoin = true)
	private String eventScope;

	@SearchField(searchField = "recurrenceSchedule.id", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean recurringEvent;

	@SearchField(searchField = "recurrenceSchedule.id", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean nonRecurringEvent;

	@SearchField(searchField = "startDate", searchFieldPath = "recurrenceSchedule")
	private Date recurrenceScheduleStartDate;

	@SearchField(searchField = "endDate", searchFieldPath = "recurrenceSchedule")
	private Date recurrenceScheduleEndDate;

	@SearchField(searchField = "seriesDefinition.id")
	private Integer seriesDefinitionId;

	@SearchField(searchField = "seriesDefinition.id", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean seriesDefinitionPresent;

	@SearchField
	private Date eventDate;

	@SearchField
	private Date seriesDate;

	@SearchField
	private String summary;
	@SearchField
	private String details;

	@SearchField
	private BigDecimal amount;
	@SearchField(searchFieldPath = "eventType")
	private Boolean marketOnClose;

	@SearchField
	private Boolean completedEvent;
	@SearchField
	private String resolution;
	@SearchField
	private Boolean deletedEvent;

	@SearchField
	private Short documentFileCount;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "InvestmentEvent";
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getEventTypeId() {
		return this.eventTypeId;
	}


	public void setEventTypeId(Short eventTypeId) {
		this.eventTypeId = eventTypeId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public String getSummary() {
		return this.summary;
	}


	public void setSummary(String summary) {
		this.summary = summary;
	}


	public String getDetails() {
		return this.details;
	}


	public void setDetails(String details) {
		this.details = details;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public Boolean getMarketOnClose() {
		return this.marketOnClose;
	}


	public void setMarketOnClose(Boolean marketOnClose) {
		this.marketOnClose = marketOnClose;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Boolean getRecurringEvent() {
		return this.recurringEvent;
	}


	public void setRecurringEvent(Boolean recurringEvent) {
		this.recurringEvent = recurringEvent;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public String getEventTypeName() {
		return this.eventTypeName;
	}


	public void setEventTypeName(String eventTypeName) {
		this.eventTypeName = eventTypeName;
	}


	public Boolean getNonRecurringEvent() {
		return this.nonRecurringEvent;
	}


	public void setNonRecurringEvent(Boolean nonRecurringEvent) {
		this.nonRecurringEvent = nonRecurringEvent;
	}


	public Integer getSeriesDefinitionId() {
		return this.seriesDefinitionId;
	}


	public void setSeriesDefinitionId(Integer seriesDefinitionId) {
		this.seriesDefinitionId = seriesDefinitionId;
	}


	public Boolean getSeriesDefinitionPresent() {
		return this.seriesDefinitionPresent;
	}


	public void setSeriesDefinitionPresent(Boolean seriesDefinitionPresent) {
		this.seriesDefinitionPresent = seriesDefinitionPresent;
	}


	public Date getRecurrenceScheduleStartDate() {
		return this.recurrenceScheduleStartDate;
	}


	public void setRecurrenceScheduleStartDate(Date recurrenceScheduleStartDate) {
		this.recurrenceScheduleStartDate = recurrenceScheduleStartDate;
	}


	public Date getRecurrenceScheduleEndDate() {
		return this.recurrenceScheduleEndDate;
	}


	public void setRecurrenceScheduleEndDate(Date recurrenceScheduleEndDate) {
		this.recurrenceScheduleEndDate = recurrenceScheduleEndDate;
	}


	public Boolean getCompletedEvent() {
		return this.completedEvent;
	}


	public void setCompletedEvent(Boolean completedEvent) {
		this.completedEvent = completedEvent;
	}


	public Boolean getDeletedEvent() {
		return this.deletedEvent;
	}


	public void setDeletedEvent(Boolean deletedEvent) {
		this.deletedEvent = deletedEvent;
	}


	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}


	public String getResolution() {
		return this.resolution;
	}


	public void setResolution(String resolution) {
		this.resolution = resolution;
	}


	public Integer getEventTypeOrder() {
		return this.eventTypeOrder;
	}


	public void setEventTypeOrder(Integer eventTypeOrder) {
		this.eventTypeOrder = eventTypeOrder;
	}


	public String getInvestmentAccountNumber() {
		return this.investmentAccountNumber;
	}


	public void setInvestmentAccountNumber(String investmentAccountNumber) {
		this.investmentAccountNumber = investmentAccountNumber;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Date getSeriesDate() {
		return this.seriesDate;
	}


	public void setSeriesDate(Date seriesDate) {
		this.seriesDate = seriesDate;
	}


	public Integer getInvestmentAccountIdOrSubAccount() {
		return this.investmentAccountIdOrSubAccount;
	}


	public void setInvestmentAccountIdOrSubAccount(Integer investmentAccountIdOrSubAccount) {
		this.investmentAccountIdOrSubAccount = investmentAccountIdOrSubAccount;
	}


	public Integer getBusinessClientId() {
		return this.businessClientId;
	}


	public void setBusinessClientId(Integer businessClientId) {
		this.businessClientId = businessClientId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Short[] getEventTypeIds() {
		return this.eventTypeIds;
	}


	public void setEventTypeIds(Short[] eventTypeIds) {
		this.eventTypeIds = eventTypeIds;
	}


	public String getEventScope() {
		return this.eventScope;
	}


	public void setEventScope(String eventScope) {
		this.eventScope = eventScope;
	}
}
