package com.clifton.investment.calendar.rebuild;

import com.clifton.core.beans.BatchEntityHolder;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.calendar.InvestmentEventType;


/**
 * The <code>InvestmentCalendarRebuildExecutor</code> interface defines the methods for rebuilding {@link com.clifton.investment.calendar.InvestmentEvent}
 *
 * @author manderson
 */
public interface InvestmentCalendarRebuildExecutor {


	/**
	 * Rebuild events for given event type and date range
	 * Returns the summary of inserts, updates, and deletes to be performed, which calling method should handle
	 */
	public BatchEntityHolder<InvestmentEvent> rebuildInvestmentEventList(InvestmentEventType eventType, InvestmentCalendarRebuildContext rebuildContext);
}
