package com.clifton.investment.calendar.rebuild;

import com.clifton.core.beans.BatchEntityHolder;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.calendar.InvestmentEventType;
import com.clifton.investment.calendar.search.InvestmentEventSearchForm;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentCalendarRebuildContext</code> can be used to define options for rebuilds, as well as cache data for re-use across event type rebuilds.
 *
 * @author manderson
 */
public class InvestmentCalendarRebuildContext {

	/**
	 * If start/end date are not defined, the rebuild executor will default to the dates as defined by rebuild days back and rebuild days to rebuild on the event type
	 */
	private Date startDate;
	private Date endDate;

	private boolean errors;
	private StringBuilder resultString = new StringBuilder(50);


	/**
	 * Context can be used by rebuild executor beans to store information that other rebuild executors can use
	 * For example: Positions On/Off are two separate event types, but they review the same data which can be intensive
	 * to find all positions for each day/each account - so once one of them does those retrievals, the other can re-use that data
	 */
	private Map<String, Object> context = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getStartDateForEventType(InvestmentEventType eventType) {
		if (getStartDate() == null) {
			return DateUtils.addDays(DateUtils.clearTime(new Date()), -eventType.getRebuildDefaultDaysBack());
		}
		return getStartDate();
	}


	public Date getEndDateForEventType(InvestmentEventType eventType, Date startDate) {
		if (getEndDate() == null) {
			return DateUtils.addDays(startDate, eventType.getRebuildDefaultDaysToRebuild());
		}
		return getEndDate();
	}


	public List<InvestmentEvent> getExistingInvestmentEventList(InvestmentCalendarService investmentCalendarService, InvestmentEventType eventType, Date startDate, Date endDate) {
		InvestmentEventSearchForm eventSearchForm = new InvestmentEventSearchForm();
		eventSearchForm.setEventTypeId(eventType.getId());
		eventSearchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		eventSearchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));
		return investmentCalendarService.getInvestmentEventList(eventSearchForm);
	}


	public void appendResults(BatchEntityHolder<InvestmentEvent> batchResult) {
		this.resultString.append(batchResult.toString());
		this.resultString.append(StringUtils.NEW_LINE);
	}


	public Object getObjectFromContext(String key) {
		return this.context.get(key);
	}


	public void setObjectInContext(String key, Object object) {
		this.context.put(key, object);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public StringBuilder getResultString() {
		return this.resultString;
	}


	public void setResultString(StringBuilder resultString) {
		this.resultString = resultString;
	}


	public boolean isErrors() {
		return this.errors;
	}


	public void setErrors(boolean errors) {
		this.errors = errors;
	}
}
