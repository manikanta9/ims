package com.clifton.investment.calendar.updater;


import com.clifton.core.beans.BatchEntityHolder;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.calendar.rebuild.InvestmentCalendarRebuildCommand;
import com.clifton.investment.calendar.rebuild.InvestmentCalendarRebuildService;
import com.clifton.investment.calendar.search.InvestmentEventSearchForm;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareObserver;
import com.clifton.workflow.WorkflowManagerObserver;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentCalendarUpdaterJob</code> class is Task that can be scheduled as a
 * BatchJob to rebuild system defined and recurring InvestmentEvent objects based on the
 * specified parameters.
 *
 * @author vgomelsky
 */
public class InvestmentCalendarUpdaterJob implements Task {


	private InvestmentCalendarService investmentCalendarService;
	private InvestmentCalendarRebuildService investmentCalendarRebuildService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	// used to calculate the "fromDate" = today - daysBackFromToday
	private int recurrenceDaysBackFromToday = 0;
	// used to calculate the "toDate" = fromDate + daysToUpdate
	private int recurrenceDaysToUpdate = 365;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		InvestmentCalendarRebuildCommand rebuildCommand = new InvestmentCalendarRebuildCommand();
		rebuildCommand.setRebuildAllSystemManaged(true);
		rebuildCommand.setSynchronous(true);
		getInvestmentCalendarRebuildService().rebuildInvestmentEventListSystemManaged(rebuildCommand);

		Status status = Status.ofMessage(rebuildCommand.getResultMessage() == null ? "" : rebuildCommand.getResultMessage().toString());

		if (rebuildCommand.isErrors()) {
			status.addError("Error during calendar rebuild");
		}

		updateRecurringEvents(status);
		return status;
	}


	private BatchEntityHolder<InvestmentEvent> updateRecurringEvents(Status status) {
		BatchEntityHolder<InvestmentEvent> result = getCalendarEventUpdates(status);

		// insert/update/delete
		for (InvestmentEvent e : result.getSaveList()) {
			WorkflowState approvedWorkflowState = e.getEventType().getApprovedWorkflowState();
			if (approvedWorkflowState != null) {
				e.setWorkflowState(approvedWorkflowState);
				e.setWorkflowStatus(approvedWorkflowState.getStatus());
			}
			DaoUtils.executeWithSpecificObserversDisabled(() -> getInvestmentCalendarService().saveInvestmentEvent(e, true), WorkflowManagerObserver.class, SystemEntityModifyConditionAwareObserver.class);
		}
		for (InvestmentEvent e : result.getDeleteList()) {
			DaoUtils.executeWithSpecificObserversDisabled(() -> getInvestmentCalendarService().deleteInvestmentEvent(e.getId(), true, false), WorkflowManagerObserver.class, SystemEntityModifyConditionAwareObserver.class);
		}
		status.appendToMessage(result.toString());
		return result;
	}


	private BatchEntityHolder<InvestmentEvent> getCalendarEventUpdates(Status status) {
		Date now = DateUtils.clearTime(new Date());
		Date fromDate = DateUtils.addDays(now, getRecurrenceDaysBackFromToday());
		Date toDate = DateUtils.addDays(fromDate, getRecurrenceDaysToUpdate());

		BatchEntityHolder<InvestmentEvent> result = new BatchEntityHolder<>("Recurrences");

		// get series definitions
		InvestmentEventSearchForm eventSearchForm = new InvestmentEventSearchForm();
		eventSearchForm.addSearchRestriction(new SearchRestriction("recurrenceScheduleStartDate", ComparisonConditions.LESS_THAN_OR_EQUALS_OR_IS_NULL, fromDate));
		eventSearchForm.addSearchRestriction(new SearchRestriction("recurrenceScheduleEndDate", ComparisonConditions.GREATER_THAN_OR_EQUALS_OR_IS_NULL, fromDate));
		List<InvestmentEvent> seriesList = getInvestmentCalendarService().getInvestmentEventList(eventSearchForm);

		int activeSeriesUpdates = 0;
		int deletedSeriesUpdates = 0;
		int pendingSeriesUpdates = 0;
		for (InvestmentEvent seriesDefinition : CollectionUtils.getIterable(seriesList)) {
			WorkflowStatus seriesStatus = seriesDefinition.getWorkflowStatus();
			// Series without workflow or in Active status may have recurrence updates. Deleted series may have recurrences to remove. Series in Draft or Pending should be ignored.
			boolean seriesActive = seriesStatus == null || WorkflowStatus.STATUS_APPROVED.equals(seriesStatus.getName()) || WorkflowStatus.STATUS_ACTIVE.equals(seriesStatus.getName());
			if (seriesActive || WorkflowStatus.STATUS_CANCELED.equals(seriesStatus.getName())) {
				if (seriesActive) {
					activeSeriesUpdates++;
				}
				else {
					deletedSeriesUpdates++;
				}

				try {
					result.addAll(getInvestmentCalendarService().getInvestmentEventSeriesUpdates(seriesDefinition, fromDate, toDate));
				}
				catch (Exception e) {
					StringBuilder errorMessage = new StringBuilder(ExceptionUtils.getOriginalException(e).toString())
							.append("\nThis error was generated when processing ")
							.append(seriesDefinition.getSummary())
							.append(" for recurrence schedule ")
							.append(seriesDefinition.getRecurrenceSchedule().getName())
							.append(" with frequency ")
							.append(seriesDefinition.getRecurrenceSchedule().getFrequency())
							.append(" generating occurrences from ")
							.append(fromDate)
							.append(" to ")
							.append(toDate);
					status.addError(errorMessage.toString());
				}
			}
			else {
				pendingSeriesUpdates++;
			}
		}

		status.appendToMessage("Event Series Updates: " + activeSeriesUpdates + " active, " + deletedSeriesUpdates + " deleted, " + pendingSeriesUpdates + " skipped/pending.");
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public int getRecurrenceDaysBackFromToday() {
		return this.recurrenceDaysBackFromToday;
	}


	public void setRecurrenceDaysBackFromToday(int recurrenceDaysBackFromToday) {
		this.recurrenceDaysBackFromToday = recurrenceDaysBackFromToday;
	}


	public int getRecurrenceDaysToUpdate() {
		return this.recurrenceDaysToUpdate;
	}


	public void setRecurrenceDaysToUpdate(int recurrenceDaysToUpdate) {
		this.recurrenceDaysToUpdate = recurrenceDaysToUpdate;
	}


	public InvestmentCalendarService getInvestmentCalendarService() {
		return this.investmentCalendarService;
	}


	public void setInvestmentCalendarService(InvestmentCalendarService investmentCalendarService) {
		this.investmentCalendarService = investmentCalendarService;
	}


	public InvestmentCalendarRebuildService getInvestmentCalendarRebuildService() {
		return this.investmentCalendarRebuildService;
	}


	public void setInvestmentCalendarRebuildService(InvestmentCalendarRebuildService investmentCalendarRebuildService) {
		this.investmentCalendarRebuildService = investmentCalendarRebuildService;
	}
}
