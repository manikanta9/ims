package com.clifton.investment.calendar.rebuild;

import com.clifton.calendar.holiday.CalendarHoliday;
import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.holiday.search.CalendarHolidayDaySearchForm;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.beans.BatchEntityHolder;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.calendar.InvestmentEventType;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>InvestmentCalendarHolidayRebuildExecutorImpl</code> finds all of the {@link com.clifton.calendar.holiday.CalendarHoliday}s in the system
 * for the given date range and updates {@link InvestmentEvent}s accordingly
 *
 * @author manderson
 */
public class InvestmentCalendarHolidayRebuildExecutorImpl implements InvestmentCalendarRebuildExecutor {

	private CalendarHolidayService calendarHolidayService;

	private InvestmentCalendarService investmentCalendarService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BatchEntityHolder<InvestmentEvent> rebuildInvestmentEventList(InvestmentEventType eventType, InvestmentCalendarRebuildContext rebuildContext) {
		Date startDate = rebuildContext.getStartDateForEventType(eventType);
		Date endDate = rebuildContext.getEndDateForEventType(eventType, startDate);

		BatchEntityHolder<InvestmentEvent> result = new BatchEntityHolder<>(eventType.getName() + ": " + DateUtils.fromDateRange(startDate, endDate, true, true));

		// get all holidays and group them by day: one entry per day
		CalendarHolidayDaySearchForm searchForm = new CalendarHolidayDaySearchForm();
		searchForm.setRollupCalendar(false); // skip rollup calendars
		searchForm.addSearchRestriction(new SearchRestriction("startDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		searchForm.addSearchRestriction(new SearchRestriction("startDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));
		List<CalendarHolidayDay> holidayDayList = getCalendarHolidayService().getCalendarHolidayDayList(searchForm);
		Map<Date, List<CalendarHolidayDay>> holidayDayMap = BeanUtils.getBeansMap(holidayDayList, "day.startDate");

		// get existing calendar entries
		List<InvestmentEvent> existingEventList = rebuildContext.getExistingInvestmentEventList(getInvestmentCalendarService(), eventType, startDate, endDate);

		// check existing vs new for updates/deletes
		for (InvestmentEvent event : CollectionUtils.getIterable(existingEventList)) {
			List<CalendarHolidayDay> dayList = holidayDayMap.remove(event.getEventDate());
			if (CollectionUtils.isEmpty(dayList)) {
				result.addToDelete(event);
			}
			else {
				InvestmentEvent newEvent = newInvestmentEvent(dayList, eventType);
				if (StringUtils.compare(event.getSummary(), newEvent.getSummary()) != 0 || StringUtils.compare(event.getDetails(), newEvent.getDetails()) != 0) {
					event.setSummary(newEvent.getSummary());
					event.setDetails(newEvent.getDetails());
					result.addToUpdate(event);
				}
			}
		}

		// add remaining new entries
		for (List<CalendarHolidayDay> dayList : holidayDayMap.values()) {
			result.addToInsert(newInvestmentEvent(dayList, eventType));
		}

		return result;
	}


	private InvestmentEvent newInvestmentEvent(List<CalendarHolidayDay> dayList, InvestmentEventType eventType) {
		InvestmentEvent result = new InvestmentEvent();
		result.setEventType(eventType);
		result.setEventDate(DateUtils.clearTime(dayList.get(0).getDay().getStartDate()));

		// summary: sorted list of calendar names
		Set<Calendar> calendarSet = BeanUtils.getBeansMap(dayList, CalendarHolidayDay::getCalendar).keySet();
		String summary = BeanUtils.getPropertyValues(BeanUtils.sortWithFunction(CollectionUtils.toArrayList(calendarSet), Calendar::getName, true), "name", ", ");
		// summary cannot exceed 500 characters
		result.setSummary(StringUtils.formatStringUpToNCharsWithDots(summary, DataTypes.DESCRIPTION.getLength(), true));

		// details: sorted list of holidays: sorted list of corresponding calendar names
		Map<CalendarHoliday, List<CalendarHolidayDay>> holidayMap = BeanUtils.getBeansMap(dayList, "holiday");
		List<CalendarHoliday> sortedHolidayList = BeanUtils.sortWithFunction(CollectionUtils.toArrayList((holidayMap.keySet())), CalendarHoliday::getName, true);
		StringBuilder details = new StringBuilder(256);
		for (CalendarHoliday holiday : sortedHolidayList) {
			details.append(holiday.getName());
			details.append(": ");
			List<CalendarHolidayDay> sortedCalendarList = BeanUtils.sortWithFunction(holidayMap.get(holiday), calendarHolidayDay -> calendarHolidayDay.getCalendar().getName(), true);
			details.append(BeanUtils.getPropertyValues(sortedCalendarList, "calendar.name", ", "));
			details.append("\n\n");
		}
		details.setLength(details.length() - 2);
		result.setDetails(details.toString());

		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public CalendarHolidayService getCalendarHolidayService() {
		return this.calendarHolidayService;
	}


	public void setCalendarHolidayService(CalendarHolidayService calendarHolidayService) {
		this.calendarHolidayService = calendarHolidayService;
	}


	public InvestmentCalendarService getInvestmentCalendarService() {
		return this.investmentCalendarService;
	}


	public void setInvestmentCalendarService(InvestmentCalendarService investmentCalendarService) {
		this.investmentCalendarService = investmentCalendarService;
	}
}
