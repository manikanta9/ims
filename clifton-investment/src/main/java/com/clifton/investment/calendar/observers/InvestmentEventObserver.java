package com.clifton.investment.calendar.observers;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BatchEntityHolder;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.calendar.InvestmentEventManagerAdjustment;
import com.clifton.investment.calendar.InvestmentEventType;
import com.clifton.investment.calendar.search.InvestmentEventManagerAdjustmentSearchForm;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceServiceImpl;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareObserver;
import com.clifton.workflow.WorkflowManagerObserver;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;


/**
 * The <code>InvestmentEventObserver</code> handles:
 * <p/>
 * On Updates to Event Date from Today to Another Date For Mgr Adj Event Types - Deletes manager adjustments from manager table
 * - Need this here and not the the {@link InvestmentEventManagerAdjustmentObserver} because event dates are on the event, not on the adjustment so changes aren't picked up there
 *
 * @author manderson
 */
@Component
public class InvestmentEventObserver extends SelfRegisteringDaoObserver<InvestmentEvent> {

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentCalendarService investmentCalendarService;
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<InvestmentEvent> dao, DaoEventTypes event, InvestmentEvent bean) {
		if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}
		if (((event.isInsert() || event.isUpdate()) && bean.getRecurrenceSchedule() != null)
				|| (event.isInsert() && bean.getSeriesDefinition() != null)) {
			// we know this is a series definition or a new recurrence, and as such we are forcing it to be not-completed
			bean.setCompletedEvent(false);
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<InvestmentEvent> dao, DaoEventTypes event, InvestmentEvent bean, Throwable e) {
		if (e == null) {
			InvestmentEvent originalBean = getOriginalBean(dao, bean);
			if (event.isUpdate() && bean.getEventType().isManagerBalanceAdjustment()) {
				// Only need to update balances if event was for today and date changed
				// If date is changed to today - event manager adjustment observer will pick it up
				Date today = new Date();
				if ((DateUtils.compare(today, originalBean.getEventDate(), false) == 0) && (DateUtils.compare(today, bean.getEventDate(), false) != 0)) {
					// DELETE ALL MANAGER ADJUSTMENTS FOR THIS EVENT 
					// Events for "today" are applied to manager balances from the previous business day
					// because we run PIOS today for everything for the previous business day
					Date previousBusinessDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -1);

					InvestmentEventManagerAdjustmentSearchForm searchForm = new InvestmentEventManagerAdjustmentSearchForm();
					searchForm.setEventId(bean.getId());

					for (InvestmentEventManagerAdjustment mgrAdj : CollectionUtils.getIterable(getInvestmentCalendarService().getInvestmentEventManagerAdjustmentList(searchForm))) {
						InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(mgrAdj.getManagerAccount().getId(),
								previousBusinessDay, true);
						// If a balance exists
						if (balance != null) {
							InvestmentManagerAccountBalanceAdjustment existingAdj = null;
							List<InvestmentManagerAccountBalanceAdjustment> newAdjustmentList = new ArrayList<>();

							// Find the existing adjustment, if exists and keep all other adjustments
							for (InvestmentManagerAccountBalanceAdjustment adj : CollectionUtils.getIterable(balance.getAdjustmentList())) {
								if (adj.getSourceSystemTable() != null
										&& InvestmentManagerAccountBalanceServiceImpl.INVESTMENT_EVENT_MANAGER_ADJUSTMENT_TABLE.equals(adj.getSourceSystemTable().getName())
										&& mgrAdj.getId().equals(adj.getSourceFkFieldId())) {
									existingAdj = adj;
								}
								else {
									newAdjustmentList.add(adj);
								}
							}
							if (existingAdj != null) {
								balance.setAdjustmentList(newAdjustmentList);
								getInvestmentManagerAccountBalanceService().saveInvestmentManagerAccountBalance(balance);
							}
							// If it's not there - nothing to do
						}
					}
				}
			}

			regenerateRecurrenceEvents(bean, originalBean);
		}
	}


	private void regenerateRecurrenceEvents(InvestmentEvent event, InvestmentEvent originalEvent) {
		// Regenerate recurrences if the series event is not managed by workflow, or the event is managed by workflow and was just approved or closed/canceled.
		InvestmentEventType eventType = event.getEventType();
		boolean regenerateRecurrences = eventType.getWorkflow() == null
				|| (originalEvent != null && !CompareUtils.isEqual(event.getWorkflowState(), originalEvent.getWorkflowState()) && (event.isApproved() || event.isClosed()));
		if (event.getRecurrenceSchedule() != null && regenerateRecurrences) {
			// if series definition is created or existing schedule is modified, need to rebuild future occurrences
			Date now = DateUtils.clearTime(new Date());

			BatchEntityHolder<InvestmentEvent> updates = getInvestmentCalendarService().getInvestmentEventSeriesUpdates(event, now, DateUtils.addYears(now, 1));

			Consumer<InvestmentEvent> approvedStatePopulator = eventType.getApprovedWorkflowState() == null ? e -> {} : e -> {
				e.setWorkflowState(eventType.getApprovedWorkflowState());
				e.setWorkflowStatus(eventType.getApprovedWorkflowState().getStatus());
			};

			// insert/update/delete
			for (InvestmentEvent e : updates.getSaveList()) {
				approvedStatePopulator.accept(e);
				DaoUtils.executeWithSpecificObserversDisabled(() -> getInvestmentCalendarService().saveInvestmentEvent(e, true), WorkflowManagerObserver.class, SystemEntityModifyConditionAwareObserver.class);
			}
			for (InvestmentEvent e : updates.getDeleteList()) {
				DaoUtils.executeWithSpecificObserversDisabled(() -> getInvestmentCalendarService().deleteInvestmentEvent(e.getId(), true, false), WorkflowManagerObserver.class, SystemEntityModifyConditionAwareObserver.class);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentCalendarService getInvestmentCalendarService() {
		return this.investmentCalendarService;
	}


	public void setInvestmentCalendarService(InvestmentCalendarService investmentCalendarService) {
		this.investmentCalendarService = investmentCalendarService;
	}
}
