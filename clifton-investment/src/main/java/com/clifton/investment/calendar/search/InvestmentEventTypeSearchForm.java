package com.clifton.investment.calendar.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;


public class InvestmentEventTypeSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "id")
	private Short[] ids;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private String cssStyle;

	@SearchField
	private Boolean systemDefined;

	@SearchField
	private Boolean marketOnClose;

	@SearchField
	private Boolean managerBalanceAdjustment;

	@SearchField
	private Boolean eventAmountAllowed;

	@SearchField
	private Boolean eventCompletionAllowed;

	@SearchField
	private Boolean fileAttachmentAllowed;

	@SearchField
	private String eventScope;

	@SearchField
	private Integer order;

	@SearchField(searchField = "holidayCalendar.id")
	private Short holidayCalendarId;

	@SearchField
	private String approvedStateEditableFields;

	@SearchField(searchField = "rebuildSystemBean.id", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean systemManaged;

	@SearchField(searchField = "approvedWorkflowState.id")
	private Short approvedWorkflowStateId;

	@SearchField(searchField = "editableWorkflowStatus.id")
	private Short editableWorkflowStatusId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "InvestmentEventType";
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getManagerBalanceAdjustment() {
		return this.managerBalanceAdjustment;
	}


	public void setManagerBalanceAdjustment(Boolean managerBalanceAdjustment) {
		this.managerBalanceAdjustment = managerBalanceAdjustment;
	}


	public String getCssStyle() {
		return this.cssStyle;
	}


	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getMarketOnClose() {
		return this.marketOnClose;
	}


	public void setMarketOnClose(Boolean marketOnClose) {
		this.marketOnClose = marketOnClose;
	}


	public Boolean getEventAmountAllowed() {
		return this.eventAmountAllowed;
	}


	public void setEventAmountAllowed(Boolean eventAmountAllowed) {
		this.eventAmountAllowed = eventAmountAllowed;
	}


	public Boolean getEventCompletionAllowed() {
		return this.eventCompletionAllowed;
	}


	public void setEventCompletionAllowed(Boolean eventCompletionAllowed) {
		this.eventCompletionAllowed = eventCompletionAllowed;
	}


	public Boolean getFileAttachmentAllowed() {
		return this.fileAttachmentAllowed;
	}


	public void setFileAttachmentAllowed(Boolean fileAttachmentAllowed) {
		this.fileAttachmentAllowed = fileAttachmentAllowed;
	}


	public String getEventScope() {
		return this.eventScope;
	}


	public void setEventScope(String eventScope) {
		this.eventScope = eventScope;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Short getHolidayCalendarId() {
		return this.holidayCalendarId;
	}


	public void setHolidayCalendarId(Short holidayCalendarId) {
		this.holidayCalendarId = holidayCalendarId;
	}


	public String getApprovedStateEditableFields() {
		return this.approvedStateEditableFields;
	}


	public void setApprovedStateEditableFields(String approvedStateEditableFields) {
		this.approvedStateEditableFields = approvedStateEditableFields;
	}


	public Boolean getSystemManaged() {
		return this.systemManaged;
	}


	public void setSystemManaged(Boolean systemManaged) {
		this.systemManaged = systemManaged;
	}


	public Short[] getIds() {
		return this.ids;
	}


	public void setIds(Short[] ids) {
		this.ids = ids;
	}


	public Short getApprovedWorkflowStateId() {
		return this.approvedWorkflowStateId;
	}


	public void setApprovedWorkflowStateId(Short approvedWorkflowStateId) {
		this.approvedWorkflowStateId = approvedWorkflowStateId;
	}


	public Short getEditableWorkflowStatusId() {
		return this.editableWorkflowStatusId;
	}


	public void setEditableWorkflowStatusId(Short editableWorkflowStatusId) {
		this.editableWorkflowStatusId = editableWorkflowStatusId;
	}
}
