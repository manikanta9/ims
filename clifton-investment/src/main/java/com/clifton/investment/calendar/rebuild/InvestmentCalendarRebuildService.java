package com.clifton.investment.calendar.rebuild;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.calendar.InvestmentEvent;
import org.springframework.web.bind.annotation.ModelAttribute;


/**
 * @author manderson
 */
public interface InvestmentCalendarRebuildService {


	////////////////////////////////////////////////////////////////////////////
	///////             Investment Event Rebuild Methods              //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Rebuilds all system defined event types using default dates as defined by the event type
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = InvestmentEvent.class)
	public String rebuildInvestmentEventListSystemManaged(InvestmentCalendarRebuildCommand rebuildCommand);
}
