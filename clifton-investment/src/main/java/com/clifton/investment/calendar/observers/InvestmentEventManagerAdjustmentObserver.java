package com.clifton.investment.calendar.observers;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.calendar.InvestmentEventManagerAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceServiceImpl;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentEventManagerAdjustmentObserver</code> checks if the event was for "today".  If so, then checks if the manager
 * balance already exists. If it does it will insert/update/delete the adjustment on the balance accordingly.
 *
 * @author manderson
 */
@Component
public class InvestmentEventManagerAdjustmentObserver extends SelfRegisteringDaoObserver<InvestmentEventManagerAdjustment> {

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	private enum AdjustmentAction {
		INSERT_OR_UPDATE, DELETE, NONE
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<InvestmentEventManagerAdjustment> dao, DaoEventTypes event, InvestmentEventManagerAdjustment bean) {
		if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}
	}


	/**
	 * Needs to be done in the after call because for inserts we need the id of the event adjustment
	 */
	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<InvestmentEventManagerAdjustment> dao, DaoEventTypes event, InvestmentEventManagerAdjustment bean, Throwable e) {
		if (e == null) {
			// Determine what action, if any, is necessary
			AdjustmentAction action = getManagerBalanceAdjustmentChange(dao, event, bean);

			if (AdjustmentAction.NONE != action) {
				// Events for "today" are applied to manager balances from the previous business day
				// because we run PIOS today for everything for the previous business day
				Date previousBusinessDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -1);
				InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(bean.getManagerAccount().getId(),
						previousBusinessDay, true);
				// If a balance exists
				if (balance != null) {
					InvestmentManagerAccountBalanceAdjustment existingAdj = null;
					List<InvestmentManagerAccountBalanceAdjustment> newAdjustmentList = new ArrayList<>();

					// Find the existing adjustment, if exists and keep all other adjustments
					for (InvestmentManagerAccountBalanceAdjustment adj : CollectionUtils.getIterable(balance.getAdjustmentList())) {
						if (adj.getSourceSystemTable() != null && InvestmentManagerAccountBalanceServiceImpl.INVESTMENT_EVENT_MANAGER_ADJUSTMENT_TABLE.equals(adj.getSourceSystemTable().getName())
								&& bean.getId().equals(adj.getSourceFkFieldId())) {
							existingAdj = adj;
						}
						else {
							newAdjustmentList.add(adj);
						}
					}

					// If DELETE action, only need to update the balance if the adjustment exists
					if (AdjustmentAction.DELETE == action) {
						if (existingAdj != null) {
							balance.setAdjustmentList(newAdjustmentList);
							getInvestmentManagerAccountBalanceService().saveInvestmentManagerAccountBalance(balance);
						}
					}
					else {
						// INSERT OR UPDATE
						// Create new or update adjustment from the event adjustment
						InvestmentManagerAccountBalanceAdjustment newAdj = getInvestmentManagerAccountBalanceService().createInvestmentManagerAccountBalanceAdjustmentFromEvent(bean);
						// If already existed, copy the id so it just updates the existing adjustment instead of having to delete/insert
						if (existingAdj != null) {
							newAdj.setId(existingAdj.getId());
						}
						newAdj.setManagerAccountBalance(balance);
						newAdjustmentList.add(newAdj);
						balance.setAdjustmentList(newAdjustmentList);
						getInvestmentManagerAccountBalanceService().saveInvestmentManagerAccountBalance(balance);
					}
				}
			}
		}
	}


	private AdjustmentAction getManagerBalanceAdjustmentChange(ReadOnlyDAO<InvestmentEventManagerAdjustment> dao, DaoEventTypes event, InvestmentEventManagerAdjustment bean) {
		InvestmentEventManagerAdjustment originalBean = null;
		if (event.isUpdate()) {
			originalBean = getOriginalBean(dao, bean);
		}

		// Only need to update balances if event is for today (or was for today - then needs to be deleted)
		// Recurring events start out with a date required, and then when you add the schedule the main event definition event date is cleared
		// but the event adjustment was already added to the manager.  Then, when each occurrence it added, the adjustment would be entered again possibly allowing duplicates.
		Date today = new Date();
		if (DateUtils.compare(today, bean.getInvestmentEvent().getEventDate(), false) == 0) {
			// If inserting... 
			if (event.isInsert()) {
				// Only add it if NOT marked as ignored
				if (!bean.isIgnored()) {
					return AdjustmentAction.INSERT_OR_UPDATE;
				}
			}
			// If updating... 
			else if (event.isUpdate()) {
				// If event is part of a series and was deleted and now isn't deleted - add/update it
				if ((originalBean == null || originalBean.getInvestmentEvent().isDeletedEvent()) && !bean.getInvestmentEvent().isDeletedEvent()) {
					return AdjustmentAction.INSERT_OR_UPDATE;
				}
				// If wasn't flagged as "deleted", but now is...delete adjustments from it
				if ((originalBean != null && !originalBean.getInvestmentEvent().isDeletedEvent()) && bean.getInvestmentEvent().isDeletedEvent()) {
					return AdjustmentAction.DELETE;
				}
				// If was ignored and now isn't ignored - add/update it
				if ((originalBean == null || originalBean.isIgnored()) && !bean.isIgnored()) {
					return AdjustmentAction.INSERT_OR_UPDATE;
				}
				// If wasn't ignored, but now is...delete it
				if ((originalBean != null && !originalBean.isIgnored()) && bean.isIgnored()) {
					return AdjustmentAction.DELETE;
				}
				// If not ignored - add/update it
				if (!bean.isIgnored()) {
					return AdjustmentAction.INSERT_OR_UPDATE;
				}
			}
			// If delete...
			else if (event.isDelete()) {
				// If ignored - do nothing
				if (bean.isIgnored()) {
					return AdjustmentAction.NONE;
				}
				// Otherwise delete it
				return AdjustmentAction.DELETE;
			}
		}

		// SEE: {@link InvestmentEventObserver} for additional check: If originally the event date was today and now it's not then delete the adjustment
		return AdjustmentAction.NONE;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}
}
