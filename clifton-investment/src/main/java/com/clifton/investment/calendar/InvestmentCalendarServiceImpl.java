package com.clifton.investment.calendar;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.calendar.setup.CalendarDay;
import com.clifton.core.beans.BatchEntityHolder;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.calendar.search.InvestmentEventManagerAdjustmentSearchForm;
import com.clifton.investment.calendar.search.InvestmentEventSearchForm;
import com.clifton.investment.calendar.search.InvestmentEventTypeSearchForm;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.workflow.WorkflowManagerObserver;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class InvestmentCalendarServiceImpl implements InvestmentCalendarService {

	private AdvancedUpdatableDAO<InvestmentEvent, Criteria> investmentEventDAO;
	private AdvancedUpdatableDAO<InvestmentEventType, Criteria> investmentEventTypeDAO;
	private AdvancedUpdatableDAO<InvestmentEventManagerAdjustment, Criteria> investmentEventManagerAdjustmentDAO;

	private DaoNamedEntityCache<InvestmentEventType> investmentEventTypeCache;

	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarScheduleService calendarScheduleService;
	private CalendarHolidayService calendarHolidayService;
	private ScheduleApiService scheduleApiService;

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private InvestmentManagerAccountService investmentManagerAccountService;


	////////////////////////////////////////////////////////////////////////////
	///////         Investment Event Type Business Methods            //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentEventType getInvestmentEventType(short id) {
		return getInvestmentEventTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentEventType getInvestmentEventTypeByName(String name) {
		return getInvestmentEventTypeCache().getBeanForKeyValue(getInvestmentEventTypeDAO(), name);
	}


	@Override
	public List<InvestmentEventType> getInvestmentEventTypeList(InvestmentEventTypeSearchForm searchForm) {
		return getInvestmentEventTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentEventType saveInvestmentEventType(InvestmentEventType bean) {
		if (bean.getRebuildSystemBean() != null) {
			ValidationUtils.assertNotNull(bean.getRebuildDefaultDaysBack(), "Rebuild Start Days Back is Required for System Managed Event Types");
			ValidationUtils.assertNotNull(bean.getRebuildDefaultDaysToRebuild(), "Rebuild Days To Rebuild is Required for System Managed Event Types");
		}
		return getInvestmentEventTypeDAO().save(bean);
	}


	@Override
	public void deleteInvestmentEventType(short id) {
		getInvestmentEventTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////            Investment Event Business Methods              //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentEvent getInvestmentEvent(int id) {
		InvestmentEvent result = getInvestmentEventDAO().findByPrimaryKey(id);
		if (result != null && result.getEventType().isManagerBalanceAdjustment()) {
			result.setManagerAdjustmentList(getInvestmentEventManagerAdjustmentDAO().findByField("investmentEvent.id", id));
		}
		return result;
	}


	@Override
	public List<InvestmentEvent> getInvestmentEventList(final InvestmentEventSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getInvestmentAccountIdOrSubAccount() != null) {
					List<InvestmentAccountRelationship> relList = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipListForPurposeActive(
							searchForm.getInvestmentAccountIdOrSubAccount(), InvestmentAccountRelationshipPurpose.CLIFTON_SUB_ACCOUNT_PURPOSE_NAME, true);
					if (CollectionUtils.isEmpty(relList)) {
						criteria.add(Restrictions.eq("investmentAccount.id", searchForm.getInvestmentAccountIdOrSubAccount()));
					}
					else {
						Object[] accountIds = BeanUtils.getPropertyValues(relList, "referenceTwo.id");
						criteria.add(Restrictions.in("investmentAccount.id", ArrayUtils.add(accountIds, searchForm.getInvestmentAccountIdOrSubAccount())));
					}
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				if (!super.configureOrderBy(criteria)) {
					// define default search
					criteria.addOrder(Order.asc(getOrderByFieldName(new OrderByField("eventDate"), criteria)));
					criteria.addOrder(Order.asc(getOrderByFieldName(new OrderByField("eventTypeOrder"), criteria)));
					criteria.addOrder(Order.asc(getOrderByFieldName(new OrderByField("investmentAccountNumber"), criteria)));
				}
				return true;
			}
		};
		return getInvestmentEventDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public InvestmentEvent saveInvestmentEvent(InvestmentEvent event, boolean ignoreValidation) {
		if (event.getEventDate() != null && DateUtils.compare(event.getEventDate(), getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -5), false) < 0) {
			throw new UserIgnorableValidationException(getClass(), "saveInvestmentEventHistorical", "Cannot " + (event.isNewBean() ? "insert" : "update") + " historic event on [" + DateUtils.fromDateShort(event.getEventDate())
					+ "].  Past events can only be added/updated if it is within 5 business days of today.");
		}


		if (event.getEventType().getHolidayCalendar() != null && event.getEventDate() != null) {
			List<Date> holidays = getCalendarHolidayService().getCalendarHolidayDayListByCalendar(event.getEventType().getHolidayCalendar().getId())
					.stream()
					.map(CalendarHolidayDay::getDay)
					.map(CalendarDay::getStartDate)
					.filter(d -> DateUtils.isEqualWithoutTime(d, event.getEventDate()))
					.collect(Collectors.toList());

			if (!CollectionUtils.isEmpty(holidays) && !ignoreValidation) {
				throw new UserIgnorableValidationException((event.isNewBean() ? "Inserting" : "Updating") + " event on [" + DateUtils.fromDateShort(event.getEventDate())
						+ "] is a Holiday for Calendar [" + event.getEventType().getHolidayCalendar().getLabel() + "].");
			}
		}

		if (event.isNewBean()) {
			// new validation for making sure use does not create duplicate events (same date, type, and client acct)
			InvestmentEventSearchForm searchForm = new InvestmentEventSearchForm();
			searchForm.setEventDate(event.getEventDate());
			if (event.getEventType() != null) {
				searchForm.setEventTypeId(event.getEventType().getId());
			}
			if (event.getInvestmentAccount() != null) {
				searchForm.setInvestmentAccountId(event.getInvestmentAccount().getId());
			}
			if (event.getAssigneeSecurityGroup() != null) {
				searchForm.setTeamSecurityGroupId(event.getAssigneeSecurityGroup().getId());
			}
			List<InvestmentEvent> eventList = getInvestmentEventList(searchForm);

			if (!CollectionUtils.isEmpty(eventList) && !ignoreValidation) {
				throw new UserIgnorableValidationException("An event already exists for Date [" + event.getEventDate() + "], Event Type [" + event.getEventType().getName()
						+ "], and " + (event.getInvestmentAccount() != null ? "Client Account [" + event.getInvestmentAccount().getLabel() : "Team [" + event.getAssigneeSecurityGroup().getLabel()) + "].");
			}
		}

		return saveInvestmentEventHistorical(event);
	}


	@Override
	public InvestmentEvent saveInvestmentEventHistorical(InvestmentEvent event) {
		ValidationUtils.assertFalse(event.getEventType().isSystemDefined(), "Cannot modify an event that is system defined.");
		// Prevent Users from adding new Events that are system managed - System Managed events that are generated through code use the system defined save method
		ValidationUtils.assertFalse(event.isNewBean() && event.getEventType().isSystemManaged(), "Cannot manually create an event this is system managed.  Please use the administration screen to rebuild the events.");
		return saveInvestmentEventSystemDefined(event);
	}


	@Override
	@Transactional
	public InvestmentEvent saveInvestmentEventSystemDefined(InvestmentEvent event) {
		InvestmentEvent oldEvent = null;
		if (!event.isNewBean()) {
			oldEvent = getInvestmentEvent(event.getId());
		}

		validateInvestmentEvent(event, oldEvent);

		if (oldEvent != null && oldEvent.getRecurrenceSchedule() != null && event.getRecurrenceSchedule() != null) {
			// if series definition was modified, need to update current and all future occurrences
			boolean eventChanged = !CoreCompareUtils.isEqual(oldEvent, event, new String[]{"amount", "summary", "details"});
			boolean adjustmentsChanged = !CoreCompareUtils.isEqualProperties(oldEvent.getManagerAdjustmentList(), event.getManagerAdjustmentList());
			if (eventChanged || adjustmentsChanged) {
				// get all future events in this series that haven't been deleted
				InvestmentEventSearchForm searchForm = new InvestmentEventSearchForm();
				searchForm.setSeriesDefinitionId(event.getId());
				searchForm.setDeletedEvent(false);
				searchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, DateUtils.clearTime(new Date())));
				List<InvestmentEvent> childList = getInvestmentEventList(searchForm);
				for (InvestmentEvent child : CollectionUtils.getIterable(childList)) {
					if (eventChanged) {
						child.setAmount(event.getAmount());
						child.setSummary(event.getSummary());
						child.setDetails(event.getDetails());
						DaoUtils.executeWithSpecificObserversDisabled(() -> getInvestmentEventDAO().save(child), WorkflowManagerObserver.class);
					}
					if (adjustmentsChanged) {
						List<InvestmentEventManagerAdjustment> newList = new ArrayList<>();
						for (InvestmentEventManagerAdjustment adjustment : CollectionUtils.getIterable(event.getManagerAdjustmentList())) {
							InvestmentEventManagerAdjustment newAdjustment = new InvestmentEventManagerAdjustment();
							BeanUtils.copyPropertiesExceptAudit(adjustment, newAdjustment);
							newAdjustment.setId(null);
							newAdjustment.setInvestmentEvent(child);
							newList.add(newAdjustment);
						}
						getInvestmentEventManagerAdjustmentDAO().saveList(newList, getInvestmentEventManagerAdjustmentDAO().findByField("investmentEvent.id", child.getId()));
					}
				}
			}
		}

		List<InvestmentEventManagerAdjustment> managerAdjustmentList = event.getManagerAdjustmentList();
		event = getInvestmentEventDAO().save(event);
		getInvestmentEventManagerAdjustmentDAO().saveList(managerAdjustmentList, (oldEvent == null) ? null : oldEvent.getManagerAdjustmentList());
		event.setManagerAdjustmentList(managerAdjustmentList);
		return event;
	}


	private void validateInvestmentEvent(InvestmentEvent event, InvestmentEvent oldEvent) {
		List<InvestmentEventManagerAdjustment> newAdjustments = event.getManagerAdjustmentList();
		if (event.getEventType().isManagerBalanceAdjustment()) {
			ValidationUtils.assertNotEmpty(newAdjustments, "Events of type '" + event.getEventType().getName() + "' require at least one Manager Balance Adjustment.");
		}
		else {
			ValidationUtils.assertEmpty(newAdjustments, "Events of type '" + event.getEventType().getName() + "' do not allow Manager Balance Adjustments.");
		}

		if (event.isCompletedEvent()) {
			if (event.getEventType().getResolutionSystemList() == null) {
				ValidationUtils.assertNull(event.getResolution(), "Events of type '" + event.getEventType().getName() + "' cannot have a resolution.", "resolution");
			}
			else {
				ValidationUtils.assertNotNull(event.getResolution(), "Events of type '" + event.getEventType().getName() + "' must have a resolution.", "resolution");
			}
		}

		if (event.getRecurrenceSchedule() == null) {
			ValidationUtils.assertNotNull(event.getEventDate(), "Event Date is required.", "eventDate");
			// Allow Historic Updates for System Managed Events
			if (oldEvent != null && !event.getEventType().isSystemManaged()) {
				if (DateUtils.compare(oldEvent.getEventDate(), getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -5), false) < 0) {
					throw new FieldValidationException("Cannot update historic event on [" + event.getEventDate() + "].  Past events can only be updated if it is within 5 business days of today.",
							"eventDate");
				}
			}
		}
		else {
			ValidationUtils.assertNull(event.getEventDate(), "Event Date must not be set for series definition.", "eventDate");
			ValidationUtils.assertNull(event.getSeriesDefinition(), "A Series Definition cannot be associated with another series.", "seriesDefinition");
			if (oldEvent != null) {
				ValidationUtils.assertNull(oldEvent.getSeriesDefinition(), "A Series Definition cannot be cleared from an event that belongs to a series.", "seriesDefinition");
				if (!CompareUtils.isEqual(oldEvent.getInvestmentAccount(), event.getInvestmentAccount())) {
					throw new FieldValidationException("Cannot change investment account for event series.", "investmentAccount");
				}
			}
		}

		// new validation for newly added fields
		InvestmentEventType investmentEventType = event.getEventType();

		if (investmentEventType.getEventScope() == null) {
			ValidationUtils.assertNull(event.getInvestmentAccount(), "Investment account cannot be added to an event of type: " + investmentEventType, "investmentAccount");
			ValidationUtils.assertNull(event.getAssigneeSecurityGroup(), "Security group cannot be added to an event of type: " + investmentEventType, "assigneeSecurityGroup");
		}
		else if (!investmentEventType.getEventScope().equals(InvestmentEventScopes.TEAM_REQUIRED)) {
			ValidationUtils.assertNull(event.getAssigneeSecurityGroup(), "Security group cannot be added to an event of type: " + investmentEventType, "assigneeSecurityGroup");
			if (investmentEventType.getEventScope().equals(InvestmentEventScopes.INVESTMENT_ACCOUNT_REQUIRED)) {
				ValidationUtils.assertNotNull(event.getInvestmentAccount(), "Investment Account is required for an event of type: " + investmentEventType, "investmentAccount");
			}
		}
		else {
			ValidationUtils.assertNotNull(event.getAssigneeSecurityGroup(), "Team is required for an event of type: " + investmentEventType, "assigneeSecurityGroup");
			ValidationUtils.assertNull(event.getInvestmentAccount(), "Investment account cannot be added to an event of type: " + investmentEventType, "investmentAccount");
		}

		if (!investmentEventType.isEventAmountAllowed()) {
			ValidationUtils.assertNull(event.getAmount(), "Event amount cannot be added to an event of type: " + investmentEventType, "amount");
		}
		if (!investmentEventType.isEventCompletionAllowed()) {
			ValidationUtils.assertFalse(event.isCompletedEvent(), "Event completion is not allowed for an event of type: " + investmentEventType, "completedEvent");
		}
		if (!investmentEventType.isFileAttachmentAllowed()) {
			if (event.getDocumentFileCount() != null) {
				ValidationUtils.assertEquals(event.getDocumentFileCount(), (short) 0, "File attachment is not allowed for event of type: " + investmentEventType);
			}
		}


		// link adjustments to the event
		for (InvestmentEventManagerAdjustment adjustment : CollectionUtils.getIterable(newAdjustments)) {

			if (adjustment.getManagerAccount() != null && event.getInvestmentAccount() != null) {

				InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();
				searchForm.setClientId(event.getInvestmentAccount().getBusinessClient().getId());
				List<Integer> ids = getInvestmentManagerAccountService().getInvestmentManagerAccountList(searchForm).stream().map(InvestmentManagerAccount::getId).collect(Collectors.toList());

				if (!CollectionUtils.contains(ids, adjustment.getManagerAccount().getId())) {
					throw new ValidationException("No applicable assignment exists for Client Account [" + event.getInvestmentAccount() + "] and Manager Account [" + adjustment.getManagerAccount() + "].");
				}
			}

			adjustment.setInvestmentEvent(event);
			if (adjustment.getCashValue() == null) {
				adjustment.setCashValue(BigDecimal.ZERO);
			}
			if (adjustment.getSecuritiesValue() == null) {
				adjustment.setSecuritiesValue(BigDecimal.ZERO);
			}
			ValidationUtils.assertNotNull(adjustment.getAdjustmentType(), "Adjustment Type is required for each adjustment.", "adjustmentType");
			ValidationUtils.assertTrue(adjustment.getAdjustmentType().isMarketOnClose() == event.getEventType().isMarketOnClose(),
					"MOC event can only have MOC manager balance adjustments and vice versa.");
		}
	}


	@Override
	public void deleteInvestmentEvent(int id, boolean forceSeriesEventDeletion, boolean allowSystemDefinedDeletion) {
		deleteInvestmentEventImpl(id, forceSeriesEventDeletion, allowSystemDefinedDeletion, false);
	}


	@Override
	public void deleteInvestmentEventHistorical(int id, boolean forceSeriesEventDeletion, boolean allowSystemDefinedDeletion) {
		deleteInvestmentEventImpl(id, forceSeriesEventDeletion, allowSystemDefinedDeletion, true);
	}


	@Transactional
	protected void deleteInvestmentEventImpl(int id, boolean forceSeriesEventDeletion, boolean allowSystemDefinedDeletion, boolean allowHistorical) {
		// validate deletion first
		InvestmentEvent event = getInvestmentEvent(id);
		ValidationUtils.assertNotNull(event, "Cannot find Investment Event with id = " + id);
		ValidationUtils.assertFalse(event.isCompletedEvent(), "Cannot delete Investment Event that has been completed.", "completedEvent");

		if (!allowSystemDefinedDeletion) {
			ValidationUtils.assertFalse(event.getEventType().isSystemDefined(), "Cannot delete Investment Event that is of system defined type.");
		}

		Date now = DateUtils.clearTime(new Date());
		CalendarSchedule recurrenceSchedule = event.getRecurrenceSchedule();
		if (recurrenceSchedule == null && DateUtils.compare(event.getEventDate(), now, false) < 0 && !event.getEventType().isSystemManaged()) {
			if (!allowHistorical) {
				throw new UserIgnorableValidationException(getClass(), "deleteInvestmentEventHistorical", "Cannot delete an event that is in the past");
			}
		}

		if (recurrenceSchedule != null) {
			// deleting the full recurrence schedule
			boolean havePastEvents = false;
			List<InvestmentEvent> childList = getInvestmentEventDAO().findByField("seriesDefinition.id", id);
			for (InvestmentEvent child : CollectionUtils.getIterable(childList)) {
				if (DateUtils.compare(child.getEventDate(), now, false) < 0) {
					havePastEvents = true;
				}
				else {
					// delete current or future child event in deleted series
					getInvestmentEventManagerAdjustmentDAO().deleteList(getInvestmentEventManagerAdjustmentDAO().findByField("investmentEvent.id", child.getId()));
					getInvestmentEventDAO().delete(child);
				}
			}

			if (havePastEvents) {
				// end the schedule but keep it because of old children
				recurrenceSchedule.setEndDate(DateUtils.addDays(now, -1));
				getCalendarScheduleService().saveCalendarSchedule(recurrenceSchedule);
			}
			else {
				// delete everything
				getCalendarScheduleService().deleteCalendarSchedule(recurrenceSchedule.getId());
				getInvestmentEventManagerAdjustmentDAO().deleteList(event.getManagerAdjustmentList());
				getInvestmentEventDAO().delete(event);
			}
		}
		else if (event.getSeriesDefinition() != null && !forceSeriesEventDeletion) {
			// delete an instance of a recurring event: mark as deleted so that we know not to regenerate it in the future
			event.setDeletedEvent(true);
			// NOTE: Need to Save the Manager Adjustment List so the observer picks up the "deleted" flag on the event
			getInvestmentEventManagerAdjustmentDAO().saveList(event.getManagerAdjustmentList());
			getInvestmentEventDAO().save(event);
		}
		else {
			// delete a simple non-recurring event
			getInvestmentEventManagerAdjustmentDAO().deleteList(event.getManagerAdjustmentList());
			getInvestmentEventDAO().delete(event);
		}
	}


	@Override
	@Transactional(readOnly = true)
	public BatchEntityHolder<InvestmentEvent> getInvestmentEventSeriesUpdates(InvestmentEvent seriesDefinition, Date fromDate, Date toDate) {
		BatchEntityHolder<InvestmentEvent> result = new BatchEntityHolder<>("Recurrences");

		// get existing events in this series
		InvestmentEventSearchForm eventSearchForm = new InvestmentEventSearchForm();
		eventSearchForm.setSeriesDefinitionId(seriesDefinition.getId());
		eventSearchForm.addSearchRestriction(new SearchRestriction("seriesDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, fromDate));
		eventSearchForm.addSearchRestriction(new SearchRestriction("seriesDate", ComparisonConditions.LESS_THAN_OR_EQUALS, toDate));
		List<InvestmentEvent> eventList = getInvestmentEventList(eventSearchForm);

		Date currentDate = DateUtils.clearTime(new Date());
		if (seriesDefinition.isClosed()) {
			// all future recurrences should be deleted for cancelled/closed series definitions
			//    An exception is if a future/current event is already completed - it will be excluded from deletes.
			List<InvestmentEvent> eventRecurrencesToDelete = CollectionUtils.getStream(eventList)
					.filter(event -> DateUtils.isDateAfterOrEqual(event.getEventDate(), currentDate) && !event.isCompletedEvent())
					.collect(Collectors.toList());
			result.addAllToDelete(eventRecurrencesToDelete);
		}
		else {
			// find all dates within the specified range
			Date startDate = fromDate;
			Date endDate = toDate;
			CalendarSchedule schedule = seriesDefinition.getRecurrenceSchedule();
			if (schedule.getStartDate() != null && DateUtils.compare(startDate, schedule.getStartDate(), false) < 0) {
				startDate = schedule.getStartDate();
			}
			if (schedule.getEndDate() != null && DateUtils.compare(schedule.getEndDate(), endDate, false) < 0) {
				endDate = schedule.getEndDate();
			}
			List<Date> dateList = getScheduleApiService().getScheduleOccurrences(ScheduleOccurrenceCommand.forOccurrencesBetween(schedule.toSchedule(), startDate, endDate));

			Map<Date, InvestmentEvent> eventMap = BeanUtils.getBeanMap(eventList, InvestmentEvent::getSeriesDate);
			for (Date date : CollectionUtils.getIterable(dateList)) {
				date = DateUtils.clearTime(date);
				if (eventMap.containsKey(date)) {
					eventMap.remove(date); // skip existing (do we need to update?)
				}
				else {
					// add new event
					InvestmentEvent newEvent = new InvestmentEvent();
					BeanUtils.copyPropertiesExceptAudit(seriesDefinition, newEvent);
					newEvent.setId(null);
					newEvent.setDocumentFileCount(null);
					newEvent.setRecurrenceSchedule(null);
					newEvent.setSeriesDefinition(seriesDefinition);
					newEvent.setEventDate(date);
					newEvent.setSeriesDate(date);
					// Make sure all newly created events are not marked complete
					newEvent.setCompletedEvent(false);
					result.addToInsert(newEvent);
					// also copy manager balance adjustments if any
					if (seriesDefinition.getEventType().isManagerBalanceAdjustment()) {
						newEvent.setManagerAdjustmentList(new ArrayList<>());
						seriesDefinition = getInvestmentEvent(seriesDefinition.getId());
						for (InvestmentEventManagerAdjustment adjustment : CollectionUtils.getIterable(seriesDefinition.getManagerAdjustmentList())) {
							InvestmentEventManagerAdjustment newAdjustment = new InvestmentEventManagerAdjustment();
							BeanUtils.copyPropertiesExceptAudit(adjustment, newAdjustment);
							newAdjustment.setId(null);
							newAdjustment.setInvestmentEvent(newEvent);
							newEvent.getManagerAdjustmentList().add(newAdjustment);
						}
					}
				}
			}

			// delete remaining events (but filter out completed events)
			result.addAllToDelete(eventMap.values().stream().filter(event -> !event.isCompletedEvent()).collect(Collectors.toList()));
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	/////  Investment Event Manager Adjustment Lookup Business Methods    //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentEventManagerAdjustment getInvestmentEventManagerAdjustment(int id) {
		return getInvestmentEventManagerAdjustmentDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentEventManagerAdjustment> getInvestmentEventManagerAdjustmentList(InvestmentEventManagerAdjustmentSearchForm searchForm) {
		return getInvestmentEventManagerAdjustmentDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	///////                 Getter/Setter Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentEventType, Criteria> getInvestmentEventTypeDAO() {
		return this.investmentEventTypeDAO;
	}


	public void setInvestmentEventTypeDAO(AdvancedUpdatableDAO<InvestmentEventType, Criteria> investmentEventTypeDAO) {
		this.investmentEventTypeDAO = investmentEventTypeDAO;
	}


	public AdvancedUpdatableDAO<InvestmentEvent, Criteria> getInvestmentEventDAO() {
		return this.investmentEventDAO;
	}


	public void setInvestmentEventDAO(AdvancedUpdatableDAO<InvestmentEvent, Criteria> investmentEventDAO) {
		this.investmentEventDAO = investmentEventDAO;
	}


	public AdvancedUpdatableDAO<InvestmentEventManagerAdjustment, Criteria> getInvestmentEventManagerAdjustmentDAO() {
		return this.investmentEventManagerAdjustmentDAO;
	}


	public void setInvestmentEventManagerAdjustmentDAO(AdvancedUpdatableDAO<InvestmentEventManagerAdjustment, Criteria> investmentEventManagerAdjustmentDAO) {
		this.investmentEventManagerAdjustmentDAO = investmentEventManagerAdjustmentDAO;
	}


	public CalendarScheduleService getCalendarScheduleService() {
		return this.calendarScheduleService;
	}


	public void setCalendarScheduleService(CalendarScheduleService calendarScheduleService) {
		this.calendarScheduleService = calendarScheduleService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarHolidayService getCalendarHolidayService() {
		return this.calendarHolidayService;
	}


	public void setCalendarHolidayService(CalendarHolidayService calendarHolidayService) {
		this.calendarHolidayService = calendarHolidayService;
	}


	public ScheduleApiService getScheduleApiService() {
		return this.scheduleApiService;
	}


	public void setScheduleApiService(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public DaoNamedEntityCache<InvestmentEventType> getInvestmentEventTypeCache() {
		return this.investmentEventTypeCache;
	}


	public void setInvestmentEventTypeCache(DaoNamedEntityCache<InvestmentEventType> investmentEventTypeCache) {
		this.investmentEventTypeCache = investmentEventTypeCache;
	}


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}
}
