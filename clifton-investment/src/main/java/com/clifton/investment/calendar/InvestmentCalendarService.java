package com.clifton.investment.calendar;


import com.clifton.core.beans.BatchEntityHolder;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.validation.UserIgnorableValidation;
import com.clifton.investment.calendar.search.InvestmentEventManagerAdjustmentSearchForm;
import com.clifton.investment.calendar.search.InvestmentEventSearchForm;
import com.clifton.investment.calendar.search.InvestmentEventTypeSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentCalendarService</code> interface defines methods for working with investment security
 * events: client reminders, manager balance adjustments, etc.
 *
 * @author vgomelsky
 */
public interface InvestmentCalendarService {

	//////////////////////////////////////////////////////////////////////////// 
	///////         Investment Event Type Business Methods            ////////// 
	////////////////////////////////////////////////////////////////////////////


	public InvestmentEventType getInvestmentEventType(short id);


	public InvestmentEventType getInvestmentEventTypeByName(String name);


	public List<InvestmentEventType> getInvestmentEventTypeList(InvestmentEventTypeSearchForm searchForm);


	public InvestmentEventType saveInvestmentEventType(InvestmentEventType bean);


	public void deleteInvestmentEventType(short id);


	//////////////////////////////////////////////////////////////////////////// 
	///////            Investment Event Business Methods              ////////// 
	////////////////////////////////////////////////////////////////////////////


	public InvestmentEvent getInvestmentEvent(int id);


	public List<InvestmentEvent> getInvestmentEventList(InvestmentEventSearchForm searchForm);


	/**
	 * Returns BatchEntityHolder for the specified event series and date range that has all events that need to be
	 * inserted/updated/deleted for the specified series (in case the series changed).
	 */
	@DoNotAddRequestMapping
	public BatchEntityHolder<InvestmentEvent> getInvestmentEventSeriesUpdates(InvestmentEvent seriesDefinition, Date fromDate, Date toDate);


	@UserIgnorableValidation
	public InvestmentEvent saveInvestmentEvent(InvestmentEvent event, boolean ignoreValidation);


	/**
	 * By default, events older than 5 business days cannot be edited.  When this happens
	 * users will get a {@link com.clifton.core.validation.UserIgnorableValidationException}, and then can choose to still save the event
	 * which calls this method and bypasses that 5 business day check
	 */
	public InvestmentEvent saveInvestmentEventHistorical(InvestmentEvent event);


	/**
	 * Allows updates to system defined events.
	 */
	@DoNotAddRequestMapping
	public InvestmentEvent saveInvestmentEventSystemDefined(InvestmentEvent event);


	/**
	 * Deletes the specified event and corresponding manager adjustments if this is allowed.
	 * <p/>
	 * By default, series events are not deleted but marked as "deletedEvent = true".
	 * Pass forceSeriesEventDeletion = true in order to force deletion: rebuild from change in series schedule.
	 */
	public void deleteInvestmentEvent(int id, boolean forceSeriesEventDeletion, boolean allowSystemDefinedDeletion);


	/**
	 * Same as deleteInvestmentEvent, but allows deleting historical events
	 */
	@SecureMethod(dtoClass = InvestmentEvent.class)
	public void deleteInvestmentEventHistorical(int id, boolean forceSeriesEventDeletion, boolean allowSystemDefinedDeletion);


	//////////////////////////////////////////////////////////////////////////// 
	/////  Investment Event Manager Adjustment Lookup Business Methods    ////// 
	////////////////////////////////////////////////////////////////////////////


	public InvestmentEventManagerAdjustment getInvestmentEventManagerAdjustment(int id);


	public List<InvestmentEventManagerAdjustment> getInvestmentEventManagerAdjustmentList(InvestmentEventManagerAdjustmentSearchForm searchForm);
}
