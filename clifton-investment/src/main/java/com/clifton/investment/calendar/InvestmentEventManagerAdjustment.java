package com.clifton.investment.calendar;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustmentType;

import java.math.BigDecimal;


/**
 * The <code>InvestmentEventManagerAdjustment</code> class represents a single manager adjustment investment event entry.
 * <p/>
 * InvestmentEvent can have multiple manager adjustments.
 *
 * @author vgomelsky
 */
public class InvestmentEventManagerAdjustment extends BaseEntity<Integer> {

	private InvestmentEvent investmentEvent;

	private InvestmentManagerAccount managerAccount;
	private InvestmentManagerAccountBalanceAdjustmentType adjustmentType;

	private BigDecimal cashValue;
	private BigDecimal securitiesValue;

	private String note;
	/**
	 * If set this adjustment will not be applied to manager's balance.  It's informative only.
	 */
	private boolean ignored;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccount getManagerAccount() {
		return this.managerAccount;
	}


	public void setManagerAccount(InvestmentManagerAccount managerAccount) {
		this.managerAccount = managerAccount;
	}


	public InvestmentManagerAccountBalanceAdjustmentType getAdjustmentType() {
		return this.adjustmentType;
	}


	public void setAdjustmentType(InvestmentManagerAccountBalanceAdjustmentType adjustmentType) {
		this.adjustmentType = adjustmentType;
	}


	public BigDecimal getCashValue() {
		return this.cashValue;
	}


	public void setCashValue(BigDecimal cashValue) {
		this.cashValue = cashValue;
	}


	public BigDecimal getSecuritiesValue() {
		return this.securitiesValue;
	}


	public void setSecuritiesValue(BigDecimal securitiesValue) {
		this.securitiesValue = securitiesValue;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public InvestmentEvent getInvestmentEvent() {
		return this.investmentEvent;
	}


	public void setInvestmentEvent(InvestmentEvent investmentEvent) {
		this.investmentEvent = investmentEvent;
	}


	public boolean isIgnored() {
		return this.ignored;
	}


	public void setIgnored(boolean ignored) {
		this.ignored = ignored;
	}
}
