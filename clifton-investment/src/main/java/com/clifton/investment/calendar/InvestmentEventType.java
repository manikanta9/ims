package com.clifton.investment.calendar;


import com.clifton.calendar.setup.Calendar;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.list.SystemList;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;


/**
 * The <code>InvestmentEventType</code> class defines types of investment events: "Manager Adjustment", "Client Reminder", "Holiday", etc.
 *
 * @author vgomelsky
 */
@CacheByName
public class InvestmentEventType extends NamedEntity<Short> {

	public static final String EVENT_TYPE_HOLIDAY = "Holiday";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Optional SystemList of available resolution options for completed events.
	 */
	private SystemList resolutionSystemList;

	private String cssStyle;

	private InvestmentEventScopes eventScope;

	/**
	 * End of day (MOC) vs beginning of day event.
	 */
	private boolean marketOnClose;

	/**
	 * Specifies whether this event type is a manager balance adjustment: uses InvestmentEventManagerAdjustment
	 */
	private boolean managerBalanceAdjustment;

	/**
	 * System defined event types cannot be modified.
	 */
	private boolean systemDefined;

	/**
	 * Sort order for events of this type.
	 */
	private Integer order;


	/**
	 * A {@link SystemBean} that is used to rebuild the events for this event type
	 * Beans implement {@link com.clifton.investment.calendar.rebuild.InvestmentCalendarRebuildExecutor} interface
	 */
	private SystemBean rebuildSystemBean;
	/**
	 * When the rebuildSystemBean is executed, will use the following parameters by default
	 * for rebuilds.  This is used to default the admin screen, as well as for batch jobs to rebuild, since
	 * it's not standard across all event types.  For example, calendar holidays we go from today to 1 year in the future
	 * but for cases like positions on/off we can only look at the past x days.
	 */
	private Short rebuildDefaultDaysBack;
	private Short rebuildDefaultDaysToRebuild;

	/**
	 * Boolean flags to indicate which fields are allowed to be set
	 */
	private boolean eventAmountAllowed;

	/**
	 * This field will allow the event to be completed. If an event is saved with the the "completed" field set to true, and this field is false, it will generate a validation error
	 * Additionally, this UI will not show the "Completed" checkbox on the event window if this is false
	 */
	private boolean eventCompletionAllowed;

	/**
	 * This field exist to indicate if events of this type are required to be completed
	 * <p>
	 * At a later time, the system will be updated to generate a notification if events of type with 'eventCompletionRequired' = true have not been completed
	 */
	private boolean eventCompletionRequired;

	private boolean fileAttachmentAllowed;

	/**
	 * The workflow to be used to manage events of this type
	 */
	private Workflow workflow;

	/**
	 * The initial approved state for a created {@link InvestmentEvent}.
	 * <p>
	 * Used for event types with workflow and creation of a series for an event. Rather than have to approve
	 * all generated events for an approved series event, they can automatically put in this state.
	 */
	private WorkflowState approvedWorkflowState;

	/**
	 * The status defining if an {@link InvestmentEvent} of this type is editable.
	 */
	private WorkflowStatus editableWorkflowStatus;

	/**
	 * Comma delimited list of name of fields on {@link InvestmentEvent} that are editable in Approved state
	 */
	private String approvedStateEditableFields;


	private Calendar holidayCalendar;


	private SystemCondition eventEntityModifyCondition;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isSystemManaged() {
		return getRebuildSystemBean() != null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isManagerBalanceAdjustment() {
		return this.managerBalanceAdjustment;
	}


	public void setManagerBalanceAdjustment(boolean managerBalanceAdjustment) {
		this.managerBalanceAdjustment = managerBalanceAdjustment;
	}


	public String getCssStyle() {
		return this.cssStyle;
	}


	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isMarketOnClose() {
		return this.marketOnClose;
	}


	public void setMarketOnClose(boolean marketOnClose) {
		this.marketOnClose = marketOnClose;
	}


	public SystemList getResolutionSystemList() {
		return this.resolutionSystemList;
	}


	public void setResolutionSystemList(SystemList resolutionSystemList) {
		this.resolutionSystemList = resolutionSystemList;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public SystemBean getRebuildSystemBean() {
		return this.rebuildSystemBean;
	}


	public void setRebuildSystemBean(SystemBean rebuildSystemBean) {
		this.rebuildSystemBean = rebuildSystemBean;
	}


	public Short getRebuildDefaultDaysBack() {
		return this.rebuildDefaultDaysBack;
	}


	public void setRebuildDefaultDaysBack(Short rebuildDefaultDaysBack) {
		this.rebuildDefaultDaysBack = rebuildDefaultDaysBack;
	}


	public Short getRebuildDefaultDaysToRebuild() {
		return this.rebuildDefaultDaysToRebuild;
	}


	public void setRebuildDefaultDaysToRebuild(Short rebuildDefaultDaysToRebuild) {
		this.rebuildDefaultDaysToRebuild = rebuildDefaultDaysToRebuild;
	}


	public boolean isEventAmountAllowed() {
		return this.eventAmountAllowed;
	}


	public void setEventAmountAllowed(boolean eventAmountAllowed) {
		this.eventAmountAllowed = eventAmountAllowed;
	}


	public boolean isEventCompletionAllowed() {
		return this.eventCompletionAllowed;
	}


	public void setEventCompletionAllowed(boolean eventCompletionAllowed) {
		this.eventCompletionAllowed = eventCompletionAllowed;
	}


	public boolean isEventCompletionRequired() {
		return this.eventCompletionRequired;
	}


	public void setEventCompletionRequired(boolean eventCompletionRequired) {
		this.eventCompletionRequired = eventCompletionRequired;
	}


	public boolean isFileAttachmentAllowed() {
		return this.fileAttachmentAllowed;
	}


	public void setFileAttachmentAllowed(boolean fileAttachmentAllowed) {
		this.fileAttachmentAllowed = fileAttachmentAllowed;
	}


	public InvestmentEventScopes getEventScope() {
		return this.eventScope;
	}


	public void setEventScope(InvestmentEventScopes eventScope) {
		this.eventScope = eventScope;
	}


	public Workflow getWorkflow() {
		return this.workflow;
	}


	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}


	public WorkflowState getApprovedWorkflowState() {
		return this.approvedWorkflowState;
	}


	public void setApprovedWorkflowState(WorkflowState approvedWorkflowState) {
		this.approvedWorkflowState = approvedWorkflowState;
	}


	public WorkflowStatus getEditableWorkflowStatus() {
		return this.editableWorkflowStatus;
	}


	public void setEditableWorkflowStatus(WorkflowStatus editableWorkflowStatus) {
		this.editableWorkflowStatus = editableWorkflowStatus;
	}


	public String getApprovedStateEditableFields() {
		return this.approvedStateEditableFields;
	}


	public void setApprovedStateEditableFields(String approvedStateEditableFields) {
		this.approvedStateEditableFields = approvedStateEditableFields;
	}


	public Calendar getHolidayCalendar() {
		return this.holidayCalendar;
	}


	public void setHolidayCalendar(Calendar holidayCalendar) {
		this.holidayCalendar = holidayCalendar;
	}


	public SystemCondition getEventEntityModifyCondition() {
		return this.eventEntityModifyCondition;
	}


	public void setEventEntityModifyCondition(SystemCondition eventEntityModifyCondition) {
		this.eventEntityModifyCondition = eventEntityModifyCondition;
	}
}
