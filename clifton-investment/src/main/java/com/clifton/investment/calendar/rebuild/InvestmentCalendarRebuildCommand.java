package com.clifton.investment.calendar.rebuild;

import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * @author manderson
 */
public class InvestmentCalendarRebuildCommand {

	private boolean rebuildAllSystemManaged;

	private boolean synchronous;

	private Short[] eventTypeIds;

	private Date startDate;

	private Date endDate;

	private StringBuilder resultMessage = new StringBuilder(50);

	private boolean errors;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getRunId() {
		StringBuilder runId = new StringBuilder(10);
		if (isRebuildAllSystemManaged()) {
			runId.append("ALL_");
		}
		else {
			runId.append("TYPES_");
			for (Short eventTypeId : getEventTypeIds()) {
				runId.append(eventTypeId).append("_");
			}
		}
		if (getStartDate() != null) {
			runId.append(DateUtils.fromDateShort(getStartDate())).append("-");
			if (getEndDate() != null) {
				runId.append(DateUtils.fromDateShort(getEndDate()));
			}
		}
		else {
			runId.append("ALL");
		}
		return runId.toString();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isRebuildAllSystemManaged() {
		return this.rebuildAllSystemManaged;
	}


	public void setRebuildAllSystemManaged(boolean rebuildAllSystemManaged) {
		this.rebuildAllSystemManaged = rebuildAllSystemManaged;
	}


	public Short[] getEventTypeIds() {
		return this.eventTypeIds;
	}


	public void setEventTypeIds(Short[] eventTypeIds) {
		this.eventTypeIds = eventTypeIds;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public StringBuilder getResultMessage() {
		return this.resultMessage;
	}


	public void setResultMessage(StringBuilder resultMessage) {
		this.resultMessage = resultMessage;
	}


	public boolean isErrors() {
		return this.errors;
	}


	public void setErrors(boolean errors) {
		this.errors = errors;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}
}
