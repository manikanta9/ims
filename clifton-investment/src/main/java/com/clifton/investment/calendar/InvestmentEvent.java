package com.clifton.investment.calendar;


import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.document.DocumentFileCountAware;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.security.user.SecurityGroup;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;
import com.clifton.workflow.BaseWorkflowAwareEntity;
import com.clifton.workflow.WorkflowConfig;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentEvent</code> class represents a calendar event: a manager balance adjustment, client specific reminder, etc.
 *
 * @author vgomelsky
 */
@WorkflowConfig(workflowBeanPropertyName = "eventType.workflow", required = false)
public class InvestmentEvent extends BaseWorkflowAwareEntity<Integer> implements LabeledObject, DocumentFileCountAware, SystemEntityModifyConditionAware {

	public final static String TABLE_NAME = "InvestmentEvent";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentEventType eventType;
	private InvestmentAccount investmentAccount;
	private SecurityGroup assigneeSecurityGroup;


	/**
	 * Optional recurrence schedule: an event with seriesDefinition pointing to this event
	 * will be created for each recurrence date.
	 */
	private CalendarSchedule recurrenceSchedule;
	private InvestmentEvent seriesDefinition;

	private Date eventDate;
	/**
	 * Most of the time the same as eventDate. It's used to determine if an event is a part of standard
	 * series schedule.  If eventDate is modified for a single series occurrence, the seriesDate stays
	 * unchanged in order to be able to keep this event during rebuild and not recreate a new one.
	 */
	private Date seriesDate;

	private String summary;
	private String details;

	private BigDecimal amount;

	private boolean completedEvent;
	/**
	 * Optional resolution for completed events. See eventType.resolutionSystemList for available options.
	 */
	private String resolution;

	/**
	 * Recurring events are marked as deleted as opposed to actual deletion in order to know
	 * not to re-create them during rebuild.
	 */
	private boolean deletedEvent;

	// applies only if InvestmentEventType.managerBalanceAdjustment == true
	private List<InvestmentEventManagerAdjustment> managerAdjustmentList;

	/**
	 * Total number of attachments tied to the event
	 */
	private Short documentFileCount;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Includes the count of files associated with this event as well as its seriesDefinition.
	 */
	public Short getFullDocumentFileCount() {
		if (this.seriesDefinition != null && this.seriesDefinition.getDocumentFileCount() != null) {
			if (getDocumentFileCount() == null) {
				return this.seriesDefinition.getDocumentFileCount();
			}
			return (short) (getDocumentFileCount() + this.seriesDefinition.getDocumentFileCount());
		}
		return getDocumentFileCount();
	}



	@Override
	public String getLabel() {
		if (this.summary == null) {
			StringBuilder result = new StringBuilder();
			if (this.amount != null) {
				result.append(CoreMathUtils.formatNumberMoney(this.amount));
				result.append(' ');
			}
			result.append(this.eventType == null ? "UNDEFINED TYPE" : this.eventType.getName());
			if (this.eventDate != null) {
				result.append(" on ");
				result.append(DateUtils.fromDateShort(this.eventDate));
			}
			return result.toString();
		}
		return this.summary;
	}


	public String getLabelForGrouping() {
		StringBuilder result = new StringBuilder(64);
		result.append(this.eventDate == null ? "N/A" : DateUtils.fromDate(this.eventDate, DateUtils.DATE_FORMAT_SPELLED));
		if (this.eventType != null && this.eventType.isMarketOnClose()) {
			result.append(" - MOC");
		}
		return result.toString();
	}


	public Date getDateWithTime() {
		if (this.eventDate == null) {
			return null;
		}
		if (this.eventType != null && this.eventType.isMarketOnClose()) {
			return DateUtils.addSeconds(this.eventDate, 60 * 60 * 15); // 3PM
		}
		return this.eventDate;
	}


	public boolean isSeriesDefinitionPresent() {
		return (this.seriesDefinition != null);
	}


	/**
	 * Returns true if this is a series definition: has child events
	 */
	public boolean isParentSeriesDefinition() {
		return (getSeriesDefinition() == null) && (getEventDate() == null);
	}


	/**
	 * Returns true if this entity is not managed by workflow, or is managed by workflow and is in the configured approved state.
	 *
	 * @see InvestmentEventType#approvedWorkflowState
	 */
	public boolean isApproved() {
		return getEventType().getWorkflow() == null || (getEventType().getApprovedWorkflowState() != null && getEventType().getApprovedWorkflowState().equals(getWorkflowState()));
	}


	/**
	 * Returns true if this entity is not managed by workflow, or is managed by workflow and is in the configured editable status.
	 *
	 * @see InvestmentEventType#editableWorkflowStatus
	 */
	public boolean isEditable() {
		return getEventType().getWorkflow() == null || (getEventType().getEditableWorkflowStatus() != null && getEventType().getEditableWorkflowStatus().equals(getWorkflowStatus()));
	}


	/**
	 * Returns true if this entity is managed by workflow and is in a {@link WorkflowStatus#STATUS_CLOSED} or {@link WorkflowStatus#STATUS_CANCELED} status.
	 */
	public boolean isClosed() {
		return getWorkflowStatus() != null && (WorkflowStatus.STATUS_CLOSED.equals(getWorkflowStatus().getName()) || WorkflowStatus.STATUS_CANCELED.equals(getWorkflowStatus().getName()));
	}


	public String getCoalesceAssigneeGroupInvestmentAccountName() {
		if (getAssigneeSecurityGroup() != null) {
			return getAssigneeSecurityGroup().getName();
		}
		else if (getInvestmentAccount() != null) {
			return getInvestmentAccount().getLabel();
		}
		return null;
	}


	public String getEventScope() {
		if (getAssigneeSecurityGroup() != null) {
			return getAssigneeSecurityGroup().getName();
		}
		else if (getInvestmentAccount() != null) {
			return getInvestmentAccount().getNumber();
		}
		return null;
	}


	public SecurityGroup getCoalesceTeam() {
		if (getAssigneeSecurityGroup() != null) {
			return getAssigneeSecurityGroup();
		}
		else if (getInvestmentAccount() != null) {
			return getInvestmentAccount().getTeamSecurityGroup();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentEventType getEventType() {
		return this.eventType;
	}


	public void setEventType(InvestmentEventType eventType) {
		this.eventType = eventType;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public String getSummary() {
		return this.summary;
	}


	public void setSummary(String summary) {
		this.summary = summary;
	}


	public String getDetails() {
		return this.details;
	}


	public void setDetails(String details) {
		this.details = details;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public List<InvestmentEventManagerAdjustment> getManagerAdjustmentList() {
		return this.managerAdjustmentList;
	}


	public void setManagerAdjustmentList(List<InvestmentEventManagerAdjustment> managerAdjustmentList) {
		this.managerAdjustmentList = managerAdjustmentList;
	}


	public CalendarSchedule getRecurrenceSchedule() {
		return this.recurrenceSchedule;
	}


	public void setRecurrenceSchedule(CalendarSchedule recurrenceSchedule) {
		this.recurrenceSchedule = recurrenceSchedule;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public boolean isCompletedEvent() {
		return this.completedEvent;
	}


	public void setCompletedEvent(boolean completedEvent) {
		this.completedEvent = completedEvent;
	}


	@Override
	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	@Override
	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}


	public SecurityGroup getAssigneeSecurityGroup() {
		return this.assigneeSecurityGroup;
	}


	public void setAssigneeSecurityGroup(SecurityGroup assigneeSecurityGroup) {
		this.assigneeSecurityGroup = assigneeSecurityGroup;
	}


	public InvestmentEvent getSeriesDefinition() {
		return this.seriesDefinition;
	}


	public void setSeriesDefinition(InvestmentEvent seriesDefinition) {
		this.seriesDefinition = seriesDefinition;
	}


	public boolean isDeletedEvent() {
		return this.deletedEvent;
	}


	public void setDeletedEvent(boolean deletedEvent) {
		this.deletedEvent = deletedEvent;
	}


	public String getResolution() {
		return this.resolution;
	}


	public void setResolution(String resolution) {
		this.resolution = resolution;
	}


	public Date getSeriesDate() {
		return this.seriesDate;
	}


	public void setSeriesDate(Date seriesDate) {
		this.seriesDate = seriesDate;
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.eventType != null ? this.eventType.getEventEntityModifyCondition() : null;
	}
}
