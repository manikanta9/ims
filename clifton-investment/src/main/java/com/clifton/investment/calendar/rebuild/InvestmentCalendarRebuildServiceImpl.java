package com.clifton.investment.calendar.rebuild;

import com.clifton.core.beans.BatchEntityHolder;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.calendar.InvestmentEventType;
import com.clifton.investment.calendar.search.InvestmentEventTypeSearchForm;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@Service
public class InvestmentCalendarRebuildServiceImpl implements InvestmentCalendarRebuildService {

	private InvestmentCalendarService investmentCalendarService;
	private RunnerHandler runnerHandler;
	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	///////             Investment Event Rebuild Methods              //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String rebuildInvestmentEventListSystemManaged(InvestmentCalendarRebuildCommand rebuildCommand) {
		if (!rebuildCommand.isRebuildAllSystemManaged()) {
			if (rebuildCommand.getEventTypeIds() == null || rebuildCommand.getEventTypeIds().length == 0) {
				throw new ValidationException("No Investment Event Types selected to rebuild.");
			}
		}

		if (rebuildCommand.isSynchronous()) {
			return doRebuildInvestmentEventListSystemManaged(rebuildCommand, null);
		}
		// asynchronous run support
		final String runId = rebuildCommand.getRunId();
		final Date now = new Date();
		Runner runner = new AbstractStatusAwareRunner("INVESTMENT-CALENDAR", runId, now) {

			@Override
			public void run() {
				try {
					getStatus().setMessage(doRebuildInvestmentEventListSystemManaged(rebuildCommand, getStatus()));
				}
				catch (Throwable e) {
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error rebuilding Investment Calendar Events for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);
		return "Started Investment Calendar rebuild: " + runId + ". Processing will be completed shortly.";
	}


	private String doRebuildInvestmentEventListSystemManaged(InvestmentCalendarRebuildCommand rebuildCommand, Status status) {
		InvestmentEventTypeSearchForm searchForm = new InvestmentEventTypeSearchForm();
		searchForm.setSystemManaged(true);
		if (!rebuildCommand.isRebuildAllSystemManaged()) {
			searchForm.setIds(rebuildCommand.getEventTypeIds());
		}

		List<InvestmentEventType> eventTypeList = getInvestmentCalendarService().getInvestmentEventTypeList(searchForm);

		InvestmentCalendarRebuildContext rebuildContext = new InvestmentCalendarRebuildContext();
		rebuildContext.setStartDate(rebuildCommand.getStartDate());
		rebuildContext.setEndDate(rebuildCommand.getEndDate());

		int processedEventTypes = 0;
		for (InvestmentEventType eventType : CollectionUtils.getIterable(eventTypeList)) {
			try {
				rebuildInvestmentEventListForEventType(eventType, rebuildCommand, rebuildContext);
				processedEventTypes++;
				if (status != null) {
					status.setMessage("Total Event Types: " + CollectionUtils.getSize(eventTypeList) + "; " + processedEventTypes + " Processed; Current Results: " + rebuildCommand.getResultMessage().toString());
				}
			}
			catch (Throwable e) {
				rebuildCommand.setErrors(true);
				String errorMessage = "Error rebuilding events for [" + eventType.getName() + "]: " + ExceptionUtils.getOriginalMessage(e);
				if (status != null) {
					status.addError(errorMessage);
				}
				rebuildCommand.getResultMessage().append(errorMessage).append(StringUtils.NEW_LINE);
				LogUtils.errorOrInfo(getClass(), errorMessage, e);
			}
		}
		return ((status != null) ? "Processing completed. " : "") + rebuildCommand.getResultMessage().toString();
	}


	private void rebuildInvestmentEventListForEventType(InvestmentEventType eventType, InvestmentCalendarRebuildCommand rebuildCommand, InvestmentCalendarRebuildContext rebuildContext) {
		InvestmentCalendarRebuildExecutor rebuildExecutor = (InvestmentCalendarRebuildExecutor) getSystemBeanService().getBeanInstance(eventType.getRebuildSystemBean());
		BatchEntityHolder<InvestmentEvent> result = rebuildExecutor.rebuildInvestmentEventList(eventType, rebuildContext);
		saveInvestmentEventBatchResults(result);
		rebuildCommand.getResultMessage().append(result.toString()).append(StringUtils.NEW_LINE);
	}


	@Transactional
	protected void saveInvestmentEventBatchResults(BatchEntityHolder<InvestmentEvent> result) {
		// NOTE: ALWAYS USE THE SYSTEM DEFINED SAVE FOR SYSTEM MANAGED
		// insert/update/delete
		for (InvestmentEvent event : result.getSaveList()) {
			getInvestmentCalendarService().saveInvestmentEventSystemDefined(event);
		}
		for (InvestmentEvent event : result.getDeleteList()) {
			getInvestmentCalendarService().deleteInvestmentEvent(event.getId(), true, true);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	///////                 Getter/Setter Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalendarService getInvestmentCalendarService() {
		return this.investmentCalendarService;
	}


	public void setInvestmentCalendarService(InvestmentCalendarService investmentCalendarService) {
		this.investmentCalendarService = investmentCalendarService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
