package com.clifton.investment.calendar;

/**
 * This class represents different ways to differentiate assigning {@link InvestmentEventType}s to client accounts and/or teams.
 * The INVESTMENT_ACCOUNT type indicates that an investment account is allowed for events of the selected type.
 * The INVESTMENT_ACCOUNT_ALLOWED type indicates that an investment account is required for events of the selected type.
 * The TEAM_REQUIRED type indicates that a team (assigneeSecurityGroup field) is required for events of the selected type.
 * <p>
 * A new type should be added if there another way to differential event types is needed, that is mutually exclusive with the types listed below.
 *
 * @author MitchellF
 */
public enum InvestmentEventScopes {

	INVESTMENT_ACCOUNT(false), INVESTMENT_ACCOUNT_REQUIRED(true), TEAM_REQUIRED(true);


	InvestmentEventScopes(boolean required) {
		this.required = required;
	}


	private final boolean required;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isRequired() {
		return this.required;
	}
}
