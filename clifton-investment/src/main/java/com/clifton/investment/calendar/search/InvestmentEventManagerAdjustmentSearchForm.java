package com.clifton.investment.calendar.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentEventManagerAdjustmentSearchForm</code> ...
 *
 * @author manderson
 */
public class InvestmentEventManagerAdjustmentSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "investmentEvent.id")
	private Integer eventId;

	@SearchField(searchFieldPath = "investmentEvent", searchField = "eventDate")
	private Date eventDate;

	@SearchField(searchField = "managerAccount.id")
	private Integer managerAccountId;

	@SearchField(searchField = "adjustmentType.id")
	private Short adjustmentTypeId;

	@SearchField
	private BigDecimal cashValue;

	@SearchField
	private BigDecimal securitiesValue;

	@SearchField
	private String note;

	@SearchField
	private Boolean ignored;

	@SearchField(searchFieldPath = "investmentEvent", searchField = "deletedEvent")
	private Boolean deletedEvent;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public Integer getManagerAccountId() {
		return this.managerAccountId;
	}


	public void setManagerAccountId(Integer managerAccountId) {
		this.managerAccountId = managerAccountId;
	}


	public Short getAdjustmentTypeId() {
		return this.adjustmentTypeId;
	}


	public void setAdjustmentTypeId(Short adjustmentTypeId) {
		this.adjustmentTypeId = adjustmentTypeId;
	}


	public BigDecimal getCashValue() {
		return this.cashValue;
	}


	public void setCashValue(BigDecimal cashValue) {
		this.cashValue = cashValue;
	}


	public BigDecimal getSecuritiesValue() {
		return this.securitiesValue;
	}


	public void setSecuritiesValue(BigDecimal securitiesValue) {
		this.securitiesValue = securitiesValue;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Boolean getIgnored() {
		return this.ignored;
	}


	public void setIgnored(Boolean ignored) {
		this.ignored = ignored;
	}


	public Boolean getDeletedEvent() {
		return this.deletedEvent;
	}


	public void setDeletedEvent(Boolean deletedEvent) {
		this.deletedEvent = deletedEvent;
	}


	public Integer getEventId() {
		return this.eventId;
	}


	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
}
