package com.clifton.investment.exchange;

import com.clifton.core.util.ObjectUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The InvestmentExchangeTypes enum defines different types of exchanges: Primary, Composite, etc.
 *
 * @author vgomelsky
 */
public enum InvestmentExchangeTypes {

	/**
	 * Primary Exchange: The most important stock exchange in a given country. Common characteristics of a primary exchange include a long history,
	 * primary listings of the country's top companies, listings of many important foreign corporations, large total market capitalization and a
	 * large trade value. A country may have other important stock exchanges in addition to its primary exchange.
	 */
	PRIMARY_EXCHANGE {
		@Override
		public String getSecurityExchangeCode(InvestmentSecurity security) {
			if (security != null) {
				InvestmentInstrument instrument = security.getInstrument();
				if (instrument != null) {
					InvestmentExchange exchange = instrument.getExchange();
					if (exchange != null) {
						return exchange.getExchangeCode();
					}
				}
			}
			return null;
		}
	},

	/**
	 * Composite Exchange is not a real exchange. It is used to calculate average or standardized prices for a security across all exchanges where it trades.
	 * Brokers and Custodians usually use composite (as opposed to primary) exchange pricing.
	 */
	COMPOSITE_EXCHANGE {
		@Override
		public String getSecurityExchangeCode(InvestmentSecurity security) {
			if (security != null) {
				InvestmentInstrument instrument = security.getInstrument();
				if (instrument != null) {
					InvestmentExchange compositeExchange = instrument.getCompositeExchange();
					if (compositeExchange != null) {
						return compositeExchange.getExchangeCode();
					}
				}
			}
			return null;
		}
	},


	/**
	 * COALESCE(COMPOSITE_EXCHANGE, PRIMARY_EXCHANGE)
	 */
	COMPOSITE_OR_PRIMARY_EXCHANGE {
		@Override
		public String getSecurityExchangeCode(InvestmentSecurity security) {
			return ObjectUtils.coalesce(COMPOSITE_EXCHANGE.getSecurityExchangeCode(security), PRIMARY_EXCHANGE.getSecurityExchangeCode(security));
		}
	};


	/**
	 * Returns type specific exchange code for the specified security or null if one is not set.
	 */
	public abstract String getSecurityExchangeCode(InvestmentSecurity security);
}
