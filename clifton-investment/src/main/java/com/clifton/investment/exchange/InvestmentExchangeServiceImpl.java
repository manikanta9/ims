package com.clifton.investment.exchange;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>InvestmentExchangeServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class InvestmentExchangeServiceImpl implements InvestmentExchangeService {

	private AdvancedUpdatableDAO<InvestmentExchange, Criteria> investmentExchangeDAO;
	private DaoNamedEntityCache<InvestmentExchange> investmentExchangeCache;

	////////////////////////////////////////////////////////////////////////////
	///////          Investment Exchange Business Methods               ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentExchange getInvestmentExchange(short id) {
		return getInvestmentExchangeDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentExchange getInvestmentExchangeByName(String name) {
		return getInvestmentExchangeCache().getBeanForKeyValue(getInvestmentExchangeDAO(), name);
	}


	@Override
	public List<InvestmentExchange> getInvestmentExchangeList(InvestmentExchangeSearchForm searchForm) {
		return getInvestmentExchangeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentExchange saveInvestmentExchange(InvestmentExchange bean) {
		if (bean.getOpenTime() != null || bean.getCloseTime() != null) {
			ValidationUtils.assertNotNull(bean.getTimeZone(), "Time Zone is required if open and/or close times are entered.", "timeZone");
		}

		// validate uniqueness of exchange code
		if (!StringUtils.isEmpty(bean.getExchangeCode())) {
			InvestmentExchangeSearchForm searchForm = new InvestmentExchangeSearchForm();
			searchForm.setExchangeCode(bean.getExchangeCode());
			List<InvestmentExchange> dupeList = getInvestmentExchangeList(searchForm);
			InvestmentExchange dupe = CollectionUtils.getFirstElement(dupeList);
			if (dupe != null) {
				if (!dupe.getId().equals(bean.getId())) {
					throw new FieldValidationException("This exchange code is already used by: " + dupe.getLabel(), "exchangeCode");
				}
			}
		}

		return getInvestmentExchangeDAO().save(bean);
	}


	@Override
	public void deleteInvestmentExchange(short id) {
		getInvestmentExchangeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////          Getter and Setter Methods               //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentExchange, Criteria> getInvestmentExchangeDAO() {
		return this.investmentExchangeDAO;
	}


	public void setInvestmentExchangeDAO(AdvancedUpdatableDAO<InvestmentExchange, Criteria> investmentExchangeDAO) {
		this.investmentExchangeDAO = investmentExchangeDAO;
	}


	public DaoNamedEntityCache<InvestmentExchange> getInvestmentExchangeCache() {
		return this.investmentExchangeCache;
	}


	public void setInvestmentExchangeCache(DaoNamedEntityCache<InvestmentExchange> investmentExchangeCache) {
		this.investmentExchangeCache = investmentExchangeCache;
	}
}
