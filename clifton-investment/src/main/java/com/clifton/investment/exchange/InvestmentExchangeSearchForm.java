package com.clifton.investment.exchange;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class InvestmentExchangeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,exchangeCode,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String exchangeCode;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String marketIdentifierCode;

	@SearchField(searchField = "marketIdentifierCode", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean marketIdentifierCodeNotNull;

	@SearchField
	private Boolean compositeExchange;

	@SearchField(searchField = "parent.id")
	private Short parentId;

	@SearchField(searchField = "baseCurrency.id")
	private Integer baseCurrencyId;

	@SearchField(searchField = "calendar.id")
	private Short calendarId;

	@SearchField(searchFieldPath = "country", searchField = "text")
	private String country;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getMarketIdentifierCode() {
		return this.marketIdentifierCode;
	}


	public void setMarketIdentifierCode(String marketIdentifierCode) {
		this.marketIdentifierCode = marketIdentifierCode;
	}


	public Boolean getMarketIdentifierCodeNotNull() {
		return this.marketIdentifierCodeNotNull;
	}


	public void setMarketIdentifierCodeNotNull(Boolean marketIdentifierCodeNotNull) {
		this.marketIdentifierCodeNotNull = marketIdentifierCodeNotNull;
	}


	public Boolean getCompositeExchange() {
		return this.compositeExchange;
	}


	public void setCompositeExchange(Boolean compositeExchange) {
		this.compositeExchange = compositeExchange;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getExchangeCode() {
		return this.exchangeCode;
	}


	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}


	public Integer getBaseCurrencyId() {
		return this.baseCurrencyId;
	}


	public void setBaseCurrencyId(Integer baseCurrencyId) {
		this.baseCurrencyId = baseCurrencyId;
	}


	public Short getCalendarId() {
		return this.calendarId;
	}


	public void setCalendarId(Short calendarId) {
		this.calendarId = calendarId;
	}


	public String getCountry() {
		return this.country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public Short getParentId() {
		return this.parentId;
	}


	public void setParentId(Short parentId) {
		this.parentId = parentId;
	}
}
