package com.clifton.investment.exchange;


import java.util.List;


/**
 * The <code>InvestmentExchangeService</code> ...
 *
 * @author manderson
 */
public interface InvestmentExchangeService {

	////////////////////////////////////////////////////////////////////////////
	///////          Investment Exchange Business Methods               ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentExchange getInvestmentExchange(short id);


	public InvestmentExchange getInvestmentExchangeByName(String name);


	public List<InvestmentExchange> getInvestmentExchangeList(InvestmentExchangeSearchForm searchForm);


	public InvestmentExchange saveInvestmentExchange(InvestmentExchange bean);


	public void deleteInvestmentExchange(short id);
}
