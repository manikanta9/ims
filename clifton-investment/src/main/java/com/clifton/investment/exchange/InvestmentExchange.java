package com.clifton.investment.exchange;


import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarTimeZone;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.list.SystemListItem;

import java.util.Date;
import java.util.TimeZone;


/**
 * The <code>InvestmentExchange</code> class represents a specific investment exchange platform.
 * An exchange is a marketplace in which financial instruments are traded. Exchanges give companies, governments and other groups
 * a platform to sell securities to the investing public and provides regulation to ensure fair and orderly trading, as well as
 * efficient dissemination of price information for any securities trading on that exchange.  It may or may not be a physical
 * location, but could also be an electronic platform.
 * <p>
 * An exchange can be owned by another (parent) exchange. Examples: NYSE - New York Stock Exchange
 *
 * @author manderson
 */
@CacheByName
public class InvestmentExchange extends NamedHierarchicalEntity<InvestmentExchange, Short> {

	/**
	 * Short exchange code/abbreviation (CME, CBO, JP, UN, etc.)  Usually the same as Bloomberg's exchange code.
	 */
	private String exchangeCode;

	/**
	 * ISO 10383 Market Identifier Code (MIC)
	 */
	private String marketIdentifierCode;

	/**
	 * Composite Exchange is not a real exchange. It is used to calculate average or standardized prices for a security across all exchanges where it trades.
	 */
	private boolean compositeExchange;

	/**
	 * The base currency for this exchange platform
	 */
	private InvestmentSecurity baseCurrency;

	/**
	 * Specifies the country of the exchange
	 */
	private SystemListItem country;


	/**
	 * Used to Determine the Holiday Schedule for this exchange platform
	 */
	private Calendar calendar;

	/**
	 * Specifies the time zone of the exchange (trading hours are in this time zone)
	 */
	private CalendarTimeZone timeZone;

	/**
	 * The time when this Exchange opens and closes for a full day of work: a day that is not full or partial holiday.
	 */
	private Time openTime;
	private Time closeTime;

	/**
	 * The time when this Exchange opens and closes on a Partial Holiday: corresponding Calendar has a holiday on this day and the Holiday is NOT marked as Full Day.
	 */
	private Time openTimePartialDay;
	private Time closeTimePartialDay;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (this.exchangeCode == null) {
			return getName();
		}
		return getName() + " (" + this.exchangeCode + ')';
	}


	public boolean isOpen() {
		if (getOpenTime() == null || getCloseTime() == null) {
			return true;
		}
		// Convert to local time
		Time ot = getOpenTimeLocal();
		if (ot != null) {
			Time ct = getCloseTimeLocal();
			if (ct != null) {
				Time now = new Time(new Date());
				if (ot.before(ct)) {
					if (ot.before(now) && ct.after(now)) {
						return true;
					}
				}
				// Opening Time is after Close time because it opens later in the day
				// for trading on the next day.
				else {
					if (ot.before(now) || ct.after(now)) {
						return true;
					}
				}
				return false;
			}
		}
		return true;
	}


	/**
	 * Uses Partial Day open/close times to determine if the exchange is open.
	 * When partial day open/close is not defined, uses full day open/close logic.
	 */
	public boolean isOpenPartialDay() {
		if (getOpenTimePartialDay() == null || getCloseTimePartialDay() == null) {
			return isOpen();
		}
		// Convert to local time
		Time ot = getOpenTimePartialDayLocal();
		if (ot != null) {
			Time ct = getCloseTimePartialDayLocal();
			if (ct != null) {
				Time now = new Time(new Date());
				if (ot.before(ct)) {
					if (ot.before(now) && ct.after(now)) {
						return true;
					}
				}
				// Opening Time is after Close time because it opens later in the day
				// for trading on the next day.
				else {
					if (ot.before(now) || ct.after(now)) {
						return true;
					}
				}
				return false;
			}
		}
		return true;
	}


	public Time getOpenTimeLocal() {
		return convertToLocalTime(getOpenTime());
	}


	public Time getCloseTimeLocal() {
		return convertToLocalTime(getCloseTime());
	}


	public Time getOpenTimePartialDayLocal() {
		return convertToLocalTime(getOpenTimePartialDay());
	}


	public Time getCloseTimePartialDayLocal() {
		return convertToLocalTime(getCloseTimePartialDay());
	}


	public String getLocalTimeZoneLabel() {
		java.util.Calendar now = java.util.Calendar.getInstance();
		return "Local Time (" + now.getTimeZone().getID() + ")";
	}


	private Time convertToLocalTime(Time time) {
		CalendarTimeZone localZone = getTimeZone();
		if (time == null || localZone == null) {
			return null;
		}
		String timeZoneName = localZone.getName();
		if (timeZoneName == null) {
			return null;
		}

		java.util.Calendar now = java.util.Calendar.getInstance();
		int localOffSet = now.getTimeZone().getOffset(now.getTimeInMillis()) / 1000 / 60;
		boolean dst = localZone.isDstOn();
		int zoneOffSet;
		if (dst) {
			zoneOffSet = TimeZone.getTimeZone(timeZoneName).getOffset(now.getTimeInMillis()) / 1000 / 60;
		}
		else {
			zoneOffSet = TimeZone.getTimeZone(timeZoneName).getRawOffset() / 1000 / 60;
		}
		return new Time(DateUtils.addMinutes(time.getDate(), localOffSet - zoneOffSet));
	}


	/**
	 * When determining if the exchange is open - we first determine if it's a business day
	 * if it is, then we need to know if it's a partial business day.  This additional check is only necessary if the exchange has partial times populated and different from regular operating hours
	 */
	public boolean isPartialDayHoursDefinedAndDifferent() {
		if (getOpenTimePartialDay() != null && !CompareUtils.isEqual(getOpenTime(), getOpenTimePartialDay())) {
			return true;
		}
		if (getCloseTimePartialDay() != null && !CompareUtils.isEqual(getCloseTime(), getCloseTimePartialDay())) {
			return true;
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getMarketIdentifierCode() {
		return this.marketIdentifierCode;
	}


	public void setMarketIdentifierCode(String marketIdentifierCode) {
		this.marketIdentifierCode = marketIdentifierCode;
	}


	public boolean isCompositeExchange() {
		return this.compositeExchange;
	}


	public void setCompositeExchange(boolean compositeExchange) {
		this.compositeExchange = compositeExchange;
	}


	public Calendar getCalendar() {
		return this.calendar;
	}


	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}


	public InvestmentSecurity getBaseCurrency() {
		return this.baseCurrency;
	}


	public void setBaseCurrency(InvestmentSecurity baseCurrency) {
		this.baseCurrency = baseCurrency;
	}


	public CalendarTimeZone getTimeZone() {
		return this.timeZone;
	}


	public void setTimeZone(CalendarTimeZone timeZone) {
		this.timeZone = timeZone;
	}


	public SystemListItem getCountry() {
		return this.country;
	}


	public void setCountry(SystemListItem country) {
		this.country = country;
	}


	public Time getOpenTime() {
		return this.openTime;
	}


	public void setOpenTime(Time openTime) {
		this.openTime = openTime;
	}


	public Time getCloseTime() {
		return this.closeTime;
	}


	public void setCloseTime(Time closeTime) {
		this.closeTime = closeTime;
	}


	public Time getOpenTimePartialDay() {
		return this.openTimePartialDay;
	}


	public void setOpenTimePartialDay(Time openTimePartialDay) {
		this.openTimePartialDay = openTimePartialDay;
	}


	public Time getCloseTimePartialDay() {
		return this.closeTimePartialDay;
	}


	public void setCloseTimePartialDay(Time closeTimePartialDay) {
		this.closeTimePartialDay = closeTimePartialDay;
	}


	public String getExchangeCode() {
		return this.exchangeCode;
	}


	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}
}
