package com.clifton.investment.specificity.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.specificity.InvestmentSpecificityEntryPropertyType;
import org.springframework.stereotype.Component;


/**
 * Cache for caching {@link InvestmentSpecificityEntryPropertyType} lists by definition ID.
 *
 * @author MikeH
 */
@Component
public class InvestmentSpecificityEntryPropertyTypeCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentSpecificityEntryPropertyType, Short> {

	@Override
	protected String getBeanKeyProperty() {
		return "definition.id";
	}


	@Override
	protected Short getBeanKeyValue(InvestmentSpecificityEntryPropertyType bean) {
		return BeanUtils.getBeanIdentity(bean.getDefinition());
	}
}
