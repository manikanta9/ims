package com.clifton.investment.specificity.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.specificity.BaseInvestmentSpecificityBean;
import com.clifton.investment.specificity.InvestmentSpecificityDefinition;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityUtilHandler;
import org.springframework.stereotype.Component;


/**
 * The DAO validator for {@link InvestmentSpecificityEntry} entities.
 */
@Component
public class InvestmentSpecificityEntryValidator extends SelfRegisteringDaoValidator<InvestmentSpecificityEntry> {

	private InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(InvestmentSpecificityEntry bean, DaoEventTypes config) throws ValidationException {
		validateSpecificityEntry(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates the given entry against applicable constraints.
	 *
	 * @param entry the entry to validate
	 * @throws ValidationException on validation failure
	 */
	private void validateSpecificityEntry(InvestmentSpecificityEntry entry) {
		// Validate allowed properties
		InvestmentSpecificityDefinition definition = entry.getField().getDefinition();
		ValidationUtils.assertTrue(definition.isInstrumentAllowed() || entry.getInstrument() == null, "The instrument qualifier is not allowed by this specificity definition.", "investmentInstrument");
		ValidationUtils.assertTrue(definition.isCurrencyDenominationAllowed() || entry.getCurrencyDenomination() == null, "The currency denomination qualifier is not allowed by this specificity definition.", "currencyDenomination");
		ValidationUtils.assertTrue(definition.isIssuerCompanyAllowed() || entry.getIssuerCompany() == null, "The issuer company qualifier is not allowed by this specificity definition.", "issuerCompany");

		// Validate that a type specification does not exist if a hierarchy or instrument are provided
		boolean hasHierarchyQualifier = entry.getHierarchy() != null;
		boolean hasInstrumentQualifier = entry.getInstrument() != null;
		boolean hasTypeQualifier = entry.getInvestmentType() != null || entry.getInvestmentTypeSubType() != null || entry.getInvestmentTypeSubType2() != null;
		ValidationUtils.assertFalse(hasHierarchyQualifier && hasInstrumentQualifier, "The hierarchy and instrument qualifiers may not be simultaneously defined.");
		ValidationUtils.assertFalse((hasHierarchyQualifier || hasInstrumentQualifier) && hasTypeQualifier, "A hierarchy or instrument may not be defined if categorical parameters, such as type and sub type, are defined.");

		// Validate property types
		ValidationUtils.assertTrue(entry.getCurrencyDenomination() == null || entry.getCurrencyDenomination().isCurrency(), "The selected security is not a currency denomination.", "currencyDenomination");
		ValidationUtils.assertTrue(entry.getIssuerCompany() == null || entry.getIssuerCompany().getType().isIssuer(), "The selected company is not an issuer.", "issuerCompany");

		// Perform custom validation if applicable
		Class<?> beanClass = CoreClassUtils.getClass(entry.getField().getDefinition().getEntryBeanClassName());
		if (ValidationAware.class.isAssignableFrom(beanClass)) {
			BaseInvestmentSpecificityBean beanInstance = getInvestmentSpecificityUtilHandler().generatePopulatedSpecificityBean(entry);
			((ValidationAware) beanInstance).validate();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityUtilHandler getInvestmentSpecificityUtilHandler() {
		return this.investmentSpecificityUtilHandler;
	}


	public void setInvestmentSpecificityUtilHandler(InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler) {
		this.investmentSpecificityUtilHandler = investmentSpecificityUtilHandler;
	}
}
