package com.clifton.investment.specificity.converter;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.specificity.cache.InvestmentSpecificityEntrySource;
import org.springframework.stereotype.Component;


/**
 * {@link Converter} to convert {@link InvestmentSecurity} entities into {@link InvestmentSpecificityEntrySource} entities.
 */
@Component
public class SecurityToSpecificityEntrySourceConverter implements Converter<InvestmentSecurity, InvestmentSpecificityEntrySource> {

	@Override
	public InvestmentSpecificityEntrySource convert(InvestmentSecurity from) {
		InvestmentSpecificityEntrySource entrySource = new InvestmentSpecificityEntrySource();
		entrySource.setCurrencyDenominationId(BeanUtils.getBeanIdentity(from.getInstrument().getTradingCurrency()));
		entrySource.setIssuerCompanyId(BeanUtils.getBeanIdentity(from.getBusinessCompany()));
		entrySource.setInvestmentTypeId(BeanUtils.getBeanIdentity(from.getInstrument().getHierarchy().getInvestmentType()));
		entrySource.setInvestmentTypeSubTypeId(BeanUtils.getBeanIdentity(from.getInstrument().getHierarchy().getInvestmentTypeSubType()));
		entrySource.setInvestmentTypeSubType2Id(BeanUtils.getBeanIdentity(from.getInstrument().getHierarchy().getInvestmentTypeSubType2()));
		entrySource.setInvestmentInstrumentHierarchyId(BeanUtils.getBeanIdentity(from.getInstrument().getHierarchy()));
		entrySource.setInvestmentInstrumentId(BeanUtils.getBeanIdentity(from.getInstrument()));
		return entrySource;
	}
}
