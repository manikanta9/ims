package com.clifton.investment.specificity.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.AbstractSpecificityCache;
import com.clifton.core.cache.specificity.ClearSpecificityCacheUpdater;
import com.clifton.core.cache.specificity.SimpleSpecificityScope;
import com.clifton.core.cache.specificity.SpecificityCacheTypes;
import com.clifton.core.cache.specificity.SpecificityCacheUpdater;
import com.clifton.core.cache.specificity.SpecificityScope;
import com.clifton.core.cache.specificity.key.SimpleOrderingSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.search.InstrumentHierarchySearchForm;
import com.clifton.investment.specificity.InvestmentSpecificityDefinition;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityField;
import com.clifton.investment.specificity.InvestmentSpecificityService;
import com.clifton.investment.specificity.search.InvestmentSpecificityEntrySearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Specificity cache for {@link InvestmentSpecificityEntry} objects.
 * <p>
 * The specificity criteria are designed for generic use with securities. Their values are set within the {@link InvestmentSpecificityEntry} objects.
 * <p>
 * {@link InvestmentSpecificityDefinition} objects may have any number of {@link InvestmentSpecificityField} objects, which may have any number of {@link
 * InvestmentSpecificityEntry} objects. Each security may have up to one mapping applied per field per definition. Behavior for these entry mappings is generally assigned based on
 * the definition.
 * <p>
 * For example, in the case of a label-mapping definition, behavior could be assigned such that field-label overrides should occur based on the entries under this definition. In
 * this example, a potential specificity entry data set could be loosely described as so:
 * <pre><code>
 * [{
 *     // {@link InvestmentSpecificityDefinition} definition
 *     name: "Label Mapping",
 *     propertyTypes: [{
 *         name: "Label Mapping Override",
 *         systemPropertyName: "labelMappingOverride",
 *         type: "String"
 *     }],
 *     fields: [{
 *         name: "startDate",
 *         values: [{
 *             type: "Futures",
 *             properties: [{
 *                 name: "labelMappingOverride",
 *                 value: "Start"
 *             }]
 *         }, {
 *             type: "Futures",
 *             subType: "Commodities",
 *             properties: [{
 *                 name: "labelMappingOverride",
 *                 value: "Beginning"
 *             }]
 *         }]
 *     }, {
 *         name: "endDate",
 *         values: [{
 *             type: "Futures",
 *             properties: [{
 *                 name: "labelMappingOverride",
 *                 value: "Finish"
 *             }]
 *         }, {
 *             type: "Futures",
 *             subType: "Commodities",
 *             properties: [{
 *                 name: "labelMappingOverride",
 *                 value: "End"
 *             }]
 *         }]
 *     }]
 * }, ... (additional definitions) ... ]
 * </code></pre>
 * In this case, all futures might use the label "Start" for the <code>startDate</code> field and "Finish" for the <code>endDate</code> field, except for "Commodities" futures
 * which would override this setting and use the labels "Beginning" and "End", respectively, instead.
 */
@Component
public class InvestmentSpecificityEntrySpecificityCache
		extends AbstractSpecificityCache<InvestmentSpecificityEntrySource, InvestmentSpecificityEntry, InvestmentSpecificityEntryTargetHolder> {

	private InvestmentSpecificityService investmentSpecificityService;
	private InvestmentSetupService investmentSetupService;

	private Converter<InvestmentSpecificityEntry, InvestmentSpecificityEntrySource> specificityEntryToSpecificityEntrySourceConverter;

	private final SpecificityCacheUpdater<InvestmentSpecificityEntryTargetHolder> cacheUpdater = new ClearSpecificityCacheUpdater<>(InvestmentSpecificityDefinition.class, InvestmentSpecificityEntry.class);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityEntrySpecificityCache() {
		super(SpecificityCacheTypes.ONE_TO_ONE_RESULT, new SimpleOrderingSpecificityKeyGenerator());
	}


	@Override
	protected boolean isMatch(InvestmentSpecificityEntrySource source, InvestmentSpecificityEntryTargetHolder holder) {
		return true;
	}


	@Override
	protected InvestmentSpecificityEntry getTarget(InvestmentSpecificityEntryTargetHolder targetHolder) {
		return getInvestmentSpecificityService().getInvestmentSpecificityEntry(targetHolder.getId());
	}


	@Override
	protected Collection<InvestmentSpecificityEntryTargetHolder> getTargetHolders() {
		// TODO: Speed up generation -- slow!

		// Get all existing entries
		InvestmentSpecificityEntrySearchForm searchForm = new InvestmentSpecificityEntrySearchForm();
		List<InvestmentSpecificityEntry> investmentSpecificityEntryList =
				getInvestmentSpecificityService().getInvestmentSpecificityEntryList(searchForm);

		// Generate the list of target holders for all entries
		return investmentSpecificityEntryList.stream()
				.flatMap(investmentSpecificityEntry -> {
					List<InvestmentSpecificityEntryTargetHolder> targetHolders = new ArrayList<>();
					targetHolders.add(generateTargetHolder(investmentSpecificityEntry));

					// Recursively apply this entry to any descendant hierarchies which do not override this entry
					if (investmentSpecificityEntry.getHierarchy() != null) {
						collectSubHierarchyTargetHolders(investmentSpecificityEntry, targetHolders);
					}
					return targetHolders.stream();
				})
				.collect(Collectors.toList());
	}


	@Override
	protected SpecificityKeySource getKeySource(InvestmentSpecificityEntrySource source) {
		return generateKeySource(source);
	}


	@Override
	public SpecificityCacheUpdater<InvestmentSpecificityEntryTargetHolder> getCacheUpdater() {
		return this.cacheUpdater;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Collects sub-hierarchy target holders for the given specificity entry into the given collection of target holders.
	 * <p>
	 * This allows the hierarchy inherent to {@link InvestmentInstrumentHierarchy} entities to be considered in this specificity cache. For example, if an entry is created using
	 * the top-level hierarchy of "Fixed Income", then that entry should also apply to the hierarchy for "Fixed Income / Options" (unless overridden by a more specific entry).
	 * <p>
	 * This method recursively searches sub-hierarchies for any {@link InvestmentSpecificityEntry} objects which would override the given object. If no such override exists, then a
	 * target holder is created for a clone of the given entry, as if a duplicate entry existed which explicitly specified the sub-hierarchy rather than its own hierarchy in its
	 * specificity criteria.
	 *
	 * @param investmentSpecificityEntry the entry which contains the super-hierarchy
	 * @param targetHolders              the list of target holders to which sub-hierarchy target holders should be added
	 */
	private void collectSubHierarchyTargetHolders(InvestmentSpecificityEntry investmentSpecificityEntry,
	                                              List<InvestmentSpecificityEntryTargetHolder> targetHolders) {
		// Get sub-hierarchies
		Short parentId = BeanUtils.getBeanIdentity(investmentSpecificityEntry.getHierarchy());
		InstrumentHierarchySearchForm searchForm = new InstrumentHierarchySearchForm();
		searchForm.setParentId(parentId);
		List<InvestmentInstrumentHierarchy> hierarchyList = getInvestmentSetupService().getInvestmentInstrumentHierarchyList(searchForm);

		for (InvestmentInstrumentHierarchy hierarchy : hierarchyList) {
			// Check that a entry for the field is not explicitly defined at this hierarchy level
			InvestmentSpecificityEntrySearchForm entrySearchForm = new InvestmentSpecificityEntrySearchForm();
			entrySearchForm.setDefinitionId(investmentSpecificityEntry.getField().getDefinition().getId());
			entrySearchForm.setHierarchyId(hierarchy.getId());
			entrySearchForm.setFieldId(BeanUtils.getBeanIdentity(investmentSpecificityEntry.getField()));
			entrySearchForm.setLimit(1);
			List<InvestmentSpecificityEntry> entryList = getInvestmentSpecificityService().getInvestmentSpecificityEntryList(entrySearchForm);

			if (CollectionUtils.isEmpty(entryList)) {
				/*
				 * No entry is explicitly defined for this hierarchy; propagate the current mapping to current
				 * hierarchy and descendants.
				 */
				InvestmentSpecificityEntry specificityEntryClone = BeanUtils.cloneBean(investmentSpecificityEntry, false, true);
				specificityEntryClone.setHierarchy(hierarchy);
				targetHolders.add(generateTargetHolder(specificityEntryClone));
				collectSubHierarchyTargetHolders(specificityEntryClone, targetHolders);
			}
		}
	}


	/**
	 * Generates a target holder object from a given entry object.
	 *
	 * @param investmentSpecificityEntry the entry object
	 * @return the target holder object referring to the given entry object
	 */
	private InvestmentSpecificityEntryTargetHolder generateTargetHolder(InvestmentSpecificityEntry investmentSpecificityEntry) {
		InvestmentSpecificityEntrySource source = getSpecificityEntryToSpecificityEntrySourceConverter().convert(investmentSpecificityEntry);
		String key = getKeyGenerator().generateKeyForTarget(generateKeySource(source));
		SpecificityScope scope = new SimpleSpecificityScope(BeanUtils.getIdentityAsInteger(investmentSpecificityEntry.getField()));
		return new InvestmentSpecificityEntryTargetHolder(key, investmentSpecificityEntry, scope);
	}


	/**
	 * Generates the key source object from a given entry source.
	 *
	 * @param investmentSpecificityEntrySource the entry source
	 * @return the generated key source object
	 */
	private static SpecificityKeySource generateKeySource(InvestmentSpecificityEntrySource investmentSpecificityEntrySource) {
		/*
		 * Each hierarchy entry indicates an individual tree structure. For example, an instrument is always a descendant of some instrument hierarchy.
		 *
		 * For parallel trees, additional hierarchy entries must be added. For example, sub-type and sub-type 2 are not contingent upon one another, so they must be added as
		 * separate hierarchies. The type is added as a separate hierarchy, rather than part of the sub-type and sub-type 2 hierarchies, for two reasons: (1) this reduces the
		 * maximum number of possible keys (2 * 2 * 2 = 8 keys, vs. 3 * 3 = 9 keys), and (2), more importantly, including the type enforces the constraint that both the sub-type(s)
		 * and the type are matched, even if a data change has occurred or validation was missed such that the sub-type(s) are not correctly tied to the type.
		 *
		 * The order of these hierarchies is in increasing "specificity entry", where a higher specificity entry is, in fact, less specific.
		 */
		return SpecificityKeySource
				.builder(investmentSpecificityEntrySource.getInvestmentInstrumentId(), investmentSpecificityEntrySource.getInvestmentInstrumentHierarchyId(), null)
				.addHierarchy(investmentSpecificityEntrySource.getInvestmentTypeSubType2Id(), null)
				.addHierarchy(investmentSpecificityEntrySource.getInvestmentTypeSubTypeId(), null)
				.addHierarchy(investmentSpecificityEntrySource.getInvestmentTypeId(), null)
				.addHierarchy(investmentSpecificityEntrySource.getIssuerCompanyId(), null)
				.addHierarchy(investmentSpecificityEntrySource.getCurrencyDenominationId(), null)
				.setValuesToAppend(investmentSpecificityEntrySource.getInvestmentSpecificityDefinitionId())
				.build();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityService getInvestmentSpecificityService() {
		return this.investmentSpecificityService;
	}


	public void setInvestmentSpecificityService(InvestmentSpecificityService investmentSpecificityService) {
		this.investmentSpecificityService = investmentSpecificityService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public Converter<InvestmentSpecificityEntry, InvestmentSpecificityEntrySource> getSpecificityEntryToSpecificityEntrySourceConverter() {
		return this.specificityEntryToSpecificityEntrySourceConverter;
	}


	public void setSpecificityEntryToSpecificityEntrySourceConverter(Converter<InvestmentSpecificityEntry, InvestmentSpecificityEntrySource> specificityEntryToSpecificityEntrySourceConverter) {
		this.specificityEntryToSpecificityEntrySourceConverter = specificityEntryToSpecificityEntrySourceConverter;
	}
}
