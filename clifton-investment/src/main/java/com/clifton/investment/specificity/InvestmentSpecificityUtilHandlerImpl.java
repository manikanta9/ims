package com.clifton.investment.specificity;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.SpecificityCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.specificity.cache.InvestmentSpecificityEntrySource;
import com.clifton.investment.specificity.search.InvestmentSpecificityDefinitionSearchForm;
import com.clifton.system.schema.SystemDataTypeConverter;
import com.clifton.system.userinterface.SystemUserInterfaceMultiValueAware;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * Implementor for {@link InvestmentSpecificityUtilHandler}.
 *
 * @see InvestmentSpecificityUtilHandler
 */
@Component
public class InvestmentSpecificityUtilHandlerImpl implements InvestmentSpecificityUtilHandler {

	private InvestmentSpecificityService investmentSpecificityService;
	private SpecificityCache<InvestmentSpecificityEntrySource, InvestmentSpecificityEntry> investmentSpecificityEntrySpecificityCache;

	private Converter<InvestmentSecurity, InvestmentSpecificityEntrySource> securityToSpecificityEntrySourceConverter;
	private Converter<InvestmentInstrument, InvestmentSpecificityEntrySource> instrumentToSpecificityEntrySourceConverter;
	private Converter<InvestmentInstrumentHierarchy, InvestmentSpecificityEntrySource> investmentInstrumentHierarchyToSpecificityEntrySourceConverter;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryListForSecurity(String definitionName, InvestmentSecurity security) {
		InvestmentSpecificityEntrySource entrySource = getSecurityToSpecificityEntrySourceConverter().convert(security);
		return getInvestmentSpecificityEntryListForEntrySource(definitionName, entrySource);
	}


	@Override
	public List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryListForInstrument(String definitionName, InvestmentInstrument instrument) {
		InvestmentSpecificityEntrySource entrySource = getInstrumentToSpecificityEntrySourceConverter().convert(instrument);
		return getInvestmentSpecificityEntryListForEntrySource(definitionName, entrySource);
	}


	@Override
	public List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryListForHierarchy(String definitionName, InvestmentInstrumentHierarchy hierarchy) {
		InvestmentSpecificityEntrySource entrySource = getInvestmentInstrumentHierarchyToSpecificityEntrySourceConverter().convert(hierarchy);
		return getInvestmentSpecificityEntryListForEntrySource(definitionName, entrySource);
	}


	@Override
	public <T extends BaseInvestmentSpecificityBean> T generatePopulatedSpecificityBean(InvestmentSpecificityEntry entry) {
		Class<?> beanClass = CoreClassUtils.getClass(entry.getField().getDefinition().getEntryBeanClassName());
		@SuppressWarnings("unchecked")
		T beanInstance = (T) BeanUtils.newInstance(beanClass);
		beanInstance.setEntry(entry);
		for (InvestmentSpecificityEntryProperty property : CollectionUtils.getIterable(entry.getPropertyList())) {
			if (!property.getType().isVirtualProperty()) {
				Object convertedPropertyValue = convertEntryPropertyToValue(property);
				BeanUtils.setPropertyValue(beanInstance, property.getType().getEntryBeanPropertyName(), convertedPropertyValue);
			}
		}
		return beanInstance;
	}


	@Override
	public void populateVirtualPropertyListForList(List<InvestmentSpecificityEntry> entryList) {
		// Get the ordered lists of properties, grouped by definition
		Map<InvestmentSpecificityDefinition, Collection<InvestmentSpecificityEntryPropertyType>> propertyTypeListByDefinition = CollectionUtils.getStream(entryList)
				.map(InvestmentSpecificityEntry::getField)
				.map(InvestmentSpecificityField::getDefinition)
				.distinct()
				.collect(Collectors.toMap(Function.identity(), getInvestmentSpecificityService()::getInvestmentSpecificityEntryPropertyTypesForDefinition));

		// For each entry, create a property value for each property type
		for (InvestmentSpecificityEntry entry : entryList) {
			Collection<InvestmentSpecificityEntryPropertyType> propertyTypesForEntry = propertyTypeListByDefinition.get(entry.getField().getDefinition());
			populateVirtualPropertyList(entry, propertyTypesForEntry);
		}
	}


	@Override
	public void populateVirtualPropertyList(InvestmentSpecificityEntry entry, Collection<InvestmentSpecificityEntryPropertyType> propertyTypesForEntry) {
		Map<Integer, InvestmentSpecificityEntryProperty> propertyByPropertyTypeId =
				BeanUtils.getBeanMapUnique(entry.getPropertyList(), property -> property.getType().getId());
		List<InvestmentSpecificityEntryProperty> virtualPropertyList = new ArrayList<>();
		for (InvestmentSpecificityEntryPropertyType propertyType : propertyTypesForEntry) {
			InvestmentSpecificityEntryProperty entryProperty = propertyByPropertyTypeId.get(propertyType.getId());
			InvestmentSpecificityEntryProperty virtualProperty = new InvestmentSpecificityEntryProperty();
			virtualProperty.setText(entryProperty == null ? "" : entryProperty.getText());
			virtualProperty.setValue(entryProperty == null ? "" : entryProperty.getValue());
			virtualPropertyList.add(virtualProperty);
		}
		entry.setVirtualPropertyList(virtualPropertyList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Converts the given property object into the appropriate application-side value.
	 *
	 * @param property the property to convert
	 * @return the converted property value
	 */
	private Object convertEntryPropertyToValue(InvestmentSpecificityEntryProperty property) {
		Object convertedValue;
		SystemDataTypeConverter dataTypeConverter = new SystemDataTypeConverter(property.getType().getDataType());
		try {
			if (property.isMultipleValuesAllowed()) {
				List<String> valueList = StringUtils.delimitedStringToList(property.getValue(), SystemUserInterfaceMultiValueAware.MULTI_VALUE_LIST_DELIMITER);
				convertedValue = CollectionUtils.getStream(valueList)
						.map(dataTypeConverter::convert)
						.filter(Objects::nonNull)
						.collect(Collectors.toList());
			}
			else {
				convertedValue = dataTypeConverter.convert(property.getValue());
			}
		}
		catch (Throwable e) {
			throw new ValidationException(String.format("Cannot convert value [%s] to data type [%s].", property.getValue(), property.getType()), e);
		}
		return convertedValue;
	}


	/**
	 * Gets the list of specificity entries which result when performing the specificity evaluation using the given entry source.
	 *
	 * @param definitionName the specificity definition name filter, or {@code null} if entries for all definitions should be returned
	 * @param entrySource    the entry source
	 * @return the list of applicable objects
	 */
	private List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryListForEntrySource(String definitionName, InvestmentSpecificityEntrySource entrySource) {
		// Get definition(s) to search
		List<InvestmentSpecificityDefinition> definitionList = new ArrayList<>();
		if (definitionName != null) {
			InvestmentSpecificityDefinition definition = getInvestmentSpecificityService().getInvestmentSpecificityDefinitionByName(definitionName);
			ObjectUtils.doIfPresent(definition, definitionList::add);
		}
		else {
			definitionList.addAll(getInvestmentSpecificityService().getInvestmentSpecificityDefinitionList(new InvestmentSpecificityDefinitionSearchForm()));
		}

		// Build list of entries
		List<InvestmentSpecificityEntry> entryList = new PagingArrayList<>();
		for (InvestmentSpecificityDefinition definition : definitionList) {
			entrySource.setInvestmentSpecificityDefinitionId(definition.getId());
			entryList.addAll(getInvestmentSpecificityEntrySpecificityCache().getMostSpecificResultList(entrySource));
		}
		return entryList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityService getInvestmentSpecificityService() {
		return this.investmentSpecificityService;
	}


	public void setInvestmentSpecificityService(InvestmentSpecificityService investmentSpecificityService) {
		this.investmentSpecificityService = investmentSpecificityService;
	}


	public SpecificityCache<InvestmentSpecificityEntrySource, InvestmentSpecificityEntry> getInvestmentSpecificityEntrySpecificityCache() {
		return this.investmentSpecificityEntrySpecificityCache;
	}


	public void setInvestmentSpecificityEntrySpecificityCache(SpecificityCache<InvestmentSpecificityEntrySource, InvestmentSpecificityEntry> investmentSpecificityEntrySpecificityCache) {
		this.investmentSpecificityEntrySpecificityCache = investmentSpecificityEntrySpecificityCache;
	}


	public Converter<InvestmentSecurity, InvestmentSpecificityEntrySource> getSecurityToSpecificityEntrySourceConverter() {
		return this.securityToSpecificityEntrySourceConverter;
	}


	public void setSecurityToSpecificityEntrySourceConverter(Converter<InvestmentSecurity, InvestmentSpecificityEntrySource> securityToSpecificityEntrySourceConverter) {
		this.securityToSpecificityEntrySourceConverter = securityToSpecificityEntrySourceConverter;
	}


	public Converter<InvestmentInstrument, InvestmentSpecificityEntrySource> getInstrumentToSpecificityEntrySourceConverter() {
		return this.instrumentToSpecificityEntrySourceConverter;
	}


	public void setInstrumentToSpecificityEntrySourceConverter(Converter<InvestmentInstrument, InvestmentSpecificityEntrySource> instrumentToSpecificityEntrySourceConverter) {
		this.instrumentToSpecificityEntrySourceConverter = instrumentToSpecificityEntrySourceConverter;
	}


	public Converter<InvestmentInstrumentHierarchy, InvestmentSpecificityEntrySource> getInvestmentInstrumentHierarchyToSpecificityEntrySourceConverter() {
		return this.investmentInstrumentHierarchyToSpecificityEntrySourceConverter;
	}


	public void setInvestmentInstrumentHierarchyToSpecificityEntrySourceConverter(Converter<InvestmentInstrumentHierarchy, InvestmentSpecificityEntrySource> investmentInstrumentHierarchyToSpecificityEntrySourceConverter) {
		this.investmentInstrumentHierarchyToSpecificityEntrySourceConverter = investmentInstrumentHierarchyToSpecificityEntrySourceConverter;
	}
}
