package com.clifton.investment.specificity.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.validation.CoreValidationUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.specificity.BaseInvestmentSpecificityBean;
import com.clifton.investment.specificity.InvestmentSpecificityDefinition;
import com.clifton.investment.specificity.InvestmentSpecificityField;
import com.clifton.investment.specificity.InvestmentSpecificityService;
import com.clifton.investment.specificity.search.InvestmentSpecificityFieldSearchForm;
import org.springframework.stereotype.Component;

import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Objects;


/**
 * The DAO validator for {@link InvestmentSpecificityDefinition} entities.
 */
@Component
public class InvestmentSpecificityDefinitionValidator extends SelfRegisteringDaoValidator<InvestmentSpecificityDefinition> {

	private static final String[] MODIFIABLE_FIELDS = new String[]{"description", "entityModifyCondition"};

	private InvestmentSpecificityService investmentSpecificityService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(InvestmentSpecificityDefinition bean, DaoEventTypes config) throws ValidationException {
		// Guard-clause: Disallow insertions and deletions
		if (config.isDelete() || config.isInsert()) {
			throw new ValidationException("Creation and deletion is not permitted for Investment Specificity Definition entities.");
		}

		CoreValidationUtils.assertAllowedModifiedFields(bean, getOriginalBean(bean), MODIFIABLE_FIELDS);
		validateSpecificityDefinition(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateSpecificityDefinition(InvestmentSpecificityDefinition bean) {
		// Guard-clause: Skip validation for insertions
		if (bean.getId() == null) {
			return;
		}

		// Validate SystemColumnID/FieldName constraint for field beans based on presence of SystemTableID in definition bean
		InvestmentSpecificityFieldSearchForm searchForm = new InvestmentSpecificityFieldSearchForm();
		searchForm.setDefinitionId(bean.getId());
		List<InvestmentSpecificityField> fieldList = getInvestmentSpecificityService().getInvestmentSpecificityFieldList(searchForm);
		for (InvestmentSpecificityField field : fieldList) {
			if (field.getSystemColumn() != null && !Objects.equals(field.getSystemColumn().getTable(), bean.getSystemTable())) {
				throw new FieldValidationException("One or more child field objects exist with a SystemColumn not belonging to the SystemTable being saved with this definition. This definition cannot be saved until these field objects or their SystemColumn specifications are removed.", "systemTable");
			}
		}

		// Validate bean class name type
		ValidationUtils.assertNotEmpty(bean.getEntryBeanClassName(), "The entry bean class name is required.", "entryBeanClassName");
		Class<?> entryBeanClass = CoreClassUtils.getClass(bean.getEntryBeanClassName());
		int beanModifiers = entryBeanClass.getModifiers();
		ValidationUtils.assertFalse(Modifier.isAbstract(beanModifiers) || Modifier.isInterface(beanModifiers) || !Modifier.isPublic(beanModifiers),
				String.format("The entry bean class [%s] must be public and concrete.", bean.getEntryBeanClassName()), "entryBeanClassName");
		ValidationUtils.assertTrue(BaseInvestmentSpecificityBean.class.isAssignableFrom(CoreClassUtils.getClass(bean.getEntryBeanClassName())),
				String.format("The entry bean class [%s] is not of a valid type. Entry bean classes must be of type [%s].", bean.getEntryBeanClassName(), BaseInvestmentSpecificityBean.class.getName()),
				"entryBeanClassName");

		// Verify bean class can be instantiated
		try {
			entryBeanClass.newInstance();
		}
		catch (Throwable e) {
			throw new ValidationException(String.format("Unable to instantiate bean of class [%s].", entryBeanClass.getName()), e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityService getInvestmentSpecificityService() {
		return this.investmentSpecificityService;
	}


	public void setInvestmentSpecificityService(InvestmentSpecificityService investmentSpecificityService) {
		this.investmentSpecificityService = investmentSpecificityService;
	}
}
