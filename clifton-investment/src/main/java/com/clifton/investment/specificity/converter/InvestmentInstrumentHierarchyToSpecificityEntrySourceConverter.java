package com.clifton.investment.specificity.converter;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.specificity.cache.InvestmentSpecificityEntrySource;
import org.springframework.stereotype.Component;


/**
 * {@link Converter} to convert {@link InvestmentInstrumentHierarchy} entities into {@link InvestmentSpecificityEntrySource} entities.
 */
@Component
public class InvestmentInstrumentHierarchyToSpecificityEntrySourceConverter implements Converter<InvestmentInstrumentHierarchy, InvestmentSpecificityEntrySource> {

	@Override
	public InvestmentSpecificityEntrySource convert(InvestmentInstrumentHierarchy from) {
		InvestmentSpecificityEntrySource entrySource = new InvestmentSpecificityEntrySource();
		entrySource.setInvestmentTypeId(BeanUtils.getBeanIdentity(from.getInvestmentType()));
		entrySource.setInvestmentTypeSubTypeId(BeanUtils.getBeanIdentity(from.getInvestmentTypeSubType()));
		entrySource.setInvestmentTypeSubType2Id(BeanUtils.getBeanIdentity(from.getInvestmentTypeSubType2()));
		entrySource.setInvestmentInstrumentHierarchyId(BeanUtils.getBeanIdentity(from));
		return entrySource;
	}
}
