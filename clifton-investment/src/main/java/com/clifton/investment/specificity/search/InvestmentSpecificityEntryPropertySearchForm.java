package com.clifton.investment.specificity.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.specificity.InvestmentSpecificityEntryProperty;
import com.clifton.investment.specificity.InvestmentSpecificityService;


/**
 * The search form for {@link InvestmentSpecificityEntryProperty} objects.
 *
 * @see InvestmentSpecificityService
 */
public class InvestmentSpecificityEntryPropertySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "value,text,type.name")
	private String searchPattern;

	@SearchField(searchField = "entry.id")
	private Integer entryId;
	@SearchField(searchField = "type.id")
	private Integer typeId;
	@SearchField
	private String value;
	@SearchField
	private String text;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getEntryId() {
		return this.entryId;
	}


	public void setEntryId(Integer entryId) {
		this.entryId = entryId;
	}


	public Integer getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}
}
