package com.clifton.investment.specificity;

import com.clifton.system.bean.SystemBean;


/**
 * The {@link BaseInvestmentSpecificityBean} class is the base implementation for investment specificity beans. Investment specificity beans provide a
 * strictly-typed entity which implements fields for the {@link InvestmentSpecificityEntryProperty properties} of a particular {@link
 * InvestmentSpecificityDefinition definition}.
 * <p>
 * The concept around these beans is similar to the concept around instantiated objects from {@link SystemBean} configurations.
 *
 * @see InvestmentSpecificityUtilHandler#generatePopulatedSpecificityBean(InvestmentSpecificityEntry)
 * @see InvestmentSpecificityDefinition
 */
public abstract class BaseInvestmentSpecificityBean {

	/**
	 * The entry from which this bean was generated.
	 */
	private InvestmentSpecificityEntry entry;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityEntry getEntry() {
		return this.entry;
	}


	public void setEntry(InvestmentSpecificityEntry entry) {
		this.entry = entry;
	}
}
