package com.clifton.investment.specificity.cache;

import com.clifton.core.cache.specificity.SpecificityScope;
import com.clifton.core.cache.specificity.SpecificityTargetHolder;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;


/**
 * Target holder for {@link InvestmentSpecificityEntry} objects. This contains all of the information necessary to retrieve a {@link
 * InvestmentSpecificityEntry}, as well as the specificity key for which this target holder applies.
 *
 * @see InvestmentSpecificityEntrySpecificityCache
 */
public class InvestmentSpecificityEntryTargetHolder implements SpecificityTargetHolder {

	private final String key;
	private final Integer id;
	private final SpecificityScope scope;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityEntryTargetHolder(String key, InvestmentSpecificityEntry investmentSpecificityEntry, SpecificityScope scope) {
		this.key = key;
		this.id = investmentSpecificityEntry.getId();
		this.scope = scope;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getKey() {
		return this.key;
	}


	public Integer getId() {
		return this.id;
	}


	@Override
	public SpecificityScope getScope() {
		return this.scope;
	}
}
