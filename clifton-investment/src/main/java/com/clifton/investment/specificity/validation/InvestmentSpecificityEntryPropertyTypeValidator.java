package com.clifton.investment.specificity.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.CoreValidationUtils;
import com.clifton.investment.specificity.InvestmentSpecificityEntryPropertyType;
import org.springframework.stereotype.Component;


/**
 * The DAO validator for {@link InvestmentSpecificityEntryPropertyType} entities.
 */
@Component
public class InvestmentSpecificityEntryPropertyTypeValidator extends SelfRegisteringDaoValidator<InvestmentSpecificityEntryPropertyType> {

	private static final String[] MODIFIABLE_FIELDS = new String[]{"name", "label", "description", "order", "valueListUrl", "valueTable", "userInterfaceConfig"};


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(InvestmentSpecificityEntryPropertyType bean, DaoEventTypes config) throws ValidationException {
		// Guard-clause: Disallow insertions and deletions
		if (config.isDelete() || config.isInsert()) {
			throw new ValidationException("Creation and deletion is not permitted for Investment Specificity Entry Property Type entities.");
		}

		CoreValidationUtils.assertAllowedModifiedFields(bean, getOriginalBean(bean), MODIFIABLE_FIELDS);
	}
}
