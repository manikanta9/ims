package com.clifton.investment.specificity.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.CoreValidationUtils;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityField;
import com.clifton.investment.specificity.InvestmentSpecificityService;
import com.clifton.investment.specificity.search.InvestmentSpecificityEntrySearchForm;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;


/**
 * The DAO validator for {@link InvestmentSpecificityField} entities.
 */
@Component
public class InvestmentSpecificityFieldValidator extends SelfRegisteringDaoValidator<InvestmentSpecificityField> {

	private static final String[] MODIFIABLE_FIELDS = new String[]{"description", "systemDefined"};

	private InvestmentSpecificityService investmentSpecificityService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(InvestmentSpecificityField bean, DaoEventTypes config) throws ValidationException {
		// Guard-clause: Disallow deletions of system defined beans.
		if (config.isDelete() && bean.isSystemDefined()) {
			throw new ValidationException("Deletion is not permitted for System Defined Investment Specificity Field entities.");
		}
		if (config.isDelete() && !bean.isSystemDefined()) {
			validateSpecificityFieldEntryAbsence(bean);
		}

		if (config.isUpdate()) {
			CoreValidationUtils.assertAllowedModifiedFields(bean, getOriginalBean(bean), MODIFIABLE_FIELDS);
		}
		validateSpecificityField(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateSpecificityField(InvestmentSpecificityField bean) {
		// Guard-clause: Allow DB-validation if no definition was provided
		if (bean.getDefinition() == null) {
			return;
		}

		boolean hasSystemColumn = bean.getSystemColumn() != null;
		boolean hasSystemTable = bean.getDefinition().getSystemTable() != null;

		// Validate presence rules for SystemColumnID/FieldName
		ValidationUtils.assertMutuallyExclusive("One and only one of the fieldName and systemColumn fields must be specified.", bean.getName(), bean.getSystemColumn());
		if (hasSystemTable && !hasSystemColumn) {
			throw new FieldValidationException("A system column must be specified. The corresponding definition specifies a system table.", "systemColumn");
		}
		else if (!hasSystemTable && hasSystemColumn) {
			throw new FieldValidationException("A system column may not be specified. The corresponding definition does not specify a system table.", "systemColumn");
		}

		// Validate specified column belongs to definition table
		if (hasSystemColumn && !Objects.equals(bean.getSystemColumn().getTable(), bean.getDefinition().getSystemTable())) {
			throw new FieldValidationException("The system column specified must belong to the system table specified by the field definition.", "systemColumn");
		}
	}


	private void validateSpecificityFieldEntryAbsence(InvestmentSpecificityField entry) {
		InvestmentSpecificityEntrySearchForm searchForm = new InvestmentSpecificityEntrySearchForm();
		searchForm.setDefinitionId(entry.getDefinition().getId());
		searchForm.setFieldId(entry.getId());
		List<InvestmentSpecificityEntry> entries = getInvestmentSpecificityService().getInvestmentSpecificityEntryList(searchForm);
		ValidationUtils.assertEmpty(entries, "Cannot delete a field with entries.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityService getInvestmentSpecificityService() {
		return this.investmentSpecificityService;
	}


	public void setInvestmentSpecificityService(InvestmentSpecificityService investmentSpecificityService) {
		this.investmentSpecificityService = investmentSpecificityService;
	}
}
