package com.clifton.investment.specificity.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.specificity.InvestmentSpecificityEntryProperty;
import org.springframework.stereotype.Component;


/**
 * Cache for caching {@link InvestmentSpecificityEntryProperty} lists by entry ID.
 *
 * @author MikeH
 */
@Component
public class InvestmentSpecificityEntryPropertyCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentSpecificityEntryProperty, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "entry.id";
	}


	@Override
	protected Integer getBeanKeyValue(InvestmentSpecificityEntryProperty bean) {
		return BeanUtils.getBeanIdentity(bean.getEntry());
	}
}
