package com.clifton.investment.specificity;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.system.userinterface.SystemUserInterfaceMultiValueAware;


/**
 * The {@link InvestmentSpecificityEntryProperty} class represents the value of a {@link #type property field} for an {@link InvestmentSpecificityEntry}.
 *
 * @see InvestmentSpecificityEntryPropertyType
 */
public class InvestmentSpecificityEntryProperty extends BaseEntity<Integer> implements SystemUserInterfaceMultiValueAware<InvestmentSpecificityEntryPropertyType>, LabeledObject {

	/**
	 * The entry for which this object carries a value.
	 */
	private InvestmentSpecificityEntry entry;

	/**
	 * The property field of this value.
	 */
	private InvestmentSpecificityEntryPropertyType type;

	/**
	 * The actual value of the property.
	 */
	private String value;

	/**
	 * The textual representation of the value of the property.
	 */
	private String text;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return (getEntry() == null ? "[null]" : getEntry().getLabel())
				+ ": " + (getType() == null ? "[null]" : getType().getName());
	}


	@Override
	public InvestmentSpecificityEntryPropertyType getParameter() {
		return getType();
	}


	public void setParameter(InvestmentSpecificityEntryPropertyType type) {
		setType(type);
	}


	@Override
	public boolean isMultipleValuesAllowed() {
		return getType() != null && getType().isMultipleValuesAllowed();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityEntry getEntry() {
		return this.entry;
	}


	public void setEntry(InvestmentSpecificityEntry entry) {
		this.entry = entry;
	}


	public InvestmentSpecificityEntryPropertyType getType() {
		return this.type;
	}


	public void setType(InvestmentSpecificityEntryPropertyType type) {
		this.type = type;
	}


	@Override
	public String getValue() {
		return this.value;
	}


	@Override
	public void setValue(String value) {
		this.value = value;
	}


	@Override
	public String getText() {
		return this.text;
	}


	@Override
	public void setText(String text) {
		this.text = text;
	}
}
