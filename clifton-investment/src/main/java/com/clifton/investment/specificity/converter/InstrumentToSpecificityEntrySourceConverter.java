package com.clifton.investment.specificity.converter;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.specificity.cache.InvestmentSpecificityEntrySource;
import org.springframework.stereotype.Component;


/**
 * {@link Converter} to convert {@link InvestmentInstrument} entities into {@link InvestmentSpecificityEntrySource} entities.
 */
@Component
public class InstrumentToSpecificityEntrySourceConverter implements Converter<InvestmentInstrument, InvestmentSpecificityEntrySource> {

	@Override
	public InvestmentSpecificityEntrySource convert(InvestmentInstrument from) {
		InvestmentSpecificityEntrySource entrySource = new InvestmentSpecificityEntrySource();
		entrySource.setCurrencyDenominationId(BeanUtils.getBeanIdentity(from.getTradingCurrency()));
		entrySource.setInvestmentTypeId(BeanUtils.getBeanIdentity(from.getHierarchy().getInvestmentType()));
		entrySource.setInvestmentTypeSubTypeId(BeanUtils.getBeanIdentity(from.getHierarchy().getInvestmentTypeSubType()));
		entrySource.setInvestmentTypeSubType2Id(BeanUtils.getBeanIdentity(from.getHierarchy().getInvestmentTypeSubType2()));
		entrySource.setInvestmentInstrumentHierarchyId(BeanUtils.getBeanIdentity(from.getHierarchy()));
		entrySource.setInvestmentInstrumentId(BeanUtils.getBeanIdentity(from));
		return entrySource;
	}
}
