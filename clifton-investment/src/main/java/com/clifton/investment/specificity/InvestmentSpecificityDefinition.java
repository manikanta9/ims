package com.clifton.investment.specificity;


import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.investment.specificity.cache.InvestmentSpecificityEntrySpecificityCache;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;


/**
 * The InvestmentSpecificityDefinition class can be used to define entries for investment security fields based on investment security specificity.
 * <p>
 * For example, the "Investment Security Columns" definition for standard Investment Security columns defines optional Label Override and Value Applicability.
 * This definition can be used to change labels and validation (e.g., Optional, Required, or Not Applicable) for standard security fields within the Investment
 * Security detail window.
 *
 * @see InvestmentSpecificityEntrySpecificityCache
 */
@CacheByName
public class InvestmentSpecificityDefinition extends NamedEntityWithoutLabel<Short> {

	// Definition name constants
	public static final String DEFINITION_SECURITY_UI_OVERRIDE = "Investment Security Columns";
	public static final String DEFINITION_INSTRUMENT_UI_OVERRIDE = "Investment Instrument Columns";
	public static final String DEFINITION_SECURITY_VALIDATOR = "Investment Security Validator";
	public static final String DEFINITION_INSTRUMENT_VALIDATOR = "Investment Instrument Validator";

	private SystemCondition entityModifyCondition;
	private SystemTable systemTable;
	/**
	 * The name of the backing class for entries under this definition.
	 * <p>
	 * When an applicable {@link InvestmentSpecificityEntry} is found, its data may be used to instantiate a bean of the given type (see {@link
	 * InvestmentSpecificityUtilHandler#generatePopulatedSpecificityBean(InvestmentSpecificityEntry) generatePopulatedSpecificityBean}).
	 * <p>
	 * The backing class must extend {@link BaseInvestmentSpecificityBean}.
	 */
	private String entryBeanClassName;

	private boolean instrumentAllowed;
	private boolean currencyDenominationAllowed;
	private boolean issuerCompanyAllowed;


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public SystemTable getSystemTable() {
		return this.systemTable;
	}


	public void setSystemTable(SystemTable systemTable) {
		this.systemTable = systemTable;
	}


	public String getEntryBeanClassName() {
		return this.entryBeanClassName;
	}


	public void setEntryBeanClassName(String entryBeanClassName) {
		this.entryBeanClassName = entryBeanClassName;
	}


	public boolean isInstrumentAllowed() {
		return this.instrumentAllowed;
	}


	public void setInstrumentAllowed(boolean instrumentAllowed) {
		this.instrumentAllowed = instrumentAllowed;
	}


	public boolean isCurrencyDenominationAllowed() {
		return this.currencyDenominationAllowed;
	}


	public void setCurrencyDenominationAllowed(boolean currencyDenominationAllowed) {
		this.currencyDenominationAllowed = currencyDenominationAllowed;
	}


	public boolean isIssuerCompanyAllowed() {
		return this.issuerCompanyAllowed;
	}


	public void setIssuerCompanyAllowed(boolean issuerCompanyAllowed) {
		this.issuerCompanyAllowed = issuerCompanyAllowed;
	}
}
