package com.clifton.investment.specificity.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityService;


/**
 * The search form for {@link InvestmentSpecificityEntry} objects.
 *
 * @see InvestmentSpecificityService
 */
public class InvestmentSpecificityEntrySearchForm extends BaseAuditableEntitySearchForm {

	// Property search fields
	@SearchField(searchField = "definition.id", searchFieldPath = "field")
	private Short definitionId;
	@SearchField(searchField = "definition.name", searchFieldPath = "field")
	private String definition;
	@SearchField(searchField = "definition.description", searchFieldPath = "field")
	private String definitionDescription;
	@SearchField(searchField = "field.id")
	private Short fieldId;
	@SearchField(searchField = "field.name")
	private String field;
	@SearchField(searchField = "field.description")
	private String fieldDescription;

	@SearchField(searchField = "currencyDenomination.name")
	private String currencyDenomination;
	@SearchField(searchField = "issuerCompany.name")
	private String issuerCompany;
	@SearchField(searchField = "investmentType.id")
	private Short investmentTypeId;
	@SearchField(searchField = "investmentType.name")
	private String investmentType;
	@SearchField(searchField = "investmentTypeSubType.id")
	private Short investmentTypeSubTypeId;
	@SearchField(searchField = "investmentTypeSubType.name")
	private String investmentTypeSubType;
	@SearchField(searchField = "investmentTypeSubType2.id")
	private Short investmentTypeSubType2Id;
	@SearchField(searchField = "investmentTypeSubType2.name")
	private String investmentTypeSubType2;
	@SearchField(searchField = "hierarchy.id")
	private Short hierarchyId;
	@SearchField(searchField = "instrument.id")
	private Integer instrumentId;


	// Custom search fields
	/**
	 * A value which will be broadly searched for across several entity properties.
	 *
	 * @see InvestmentSpecificityEntrySearchFormConfigurer
	 */
	private String searchPattern;

	/**
	 * If {@code true}, the {@link InvestmentSpecificityEntry#virtualPropertyList virtual property list} for each entity will be populated.
	 */
	private Boolean includeVirtualPropertyList;

	/**
	 * Security ID for searching entries which will be applied (via specificity rules) to a given security.
	 */
	private Integer appliesToSecurityId;

	/**
	 * Instrument ID for searching entries which will be applied (via specificity rules) to a given instrument.
	 */
	private Integer appliesToInstrumentId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Short definitionId) {
		this.definitionId = definitionId;
	}


	public String getDefinition() {
		return this.definition;
	}


	public void setDefinition(String definition) {
		this.definition = definition;
	}


	public String getDefinitionDescription() {
		return this.definitionDescription;
	}


	public void setDefinitionDescription(String definitionDescription) {
		this.definitionDescription = definitionDescription;
	}


	public Short getFieldId() {
		return this.fieldId;
	}


	public void setFieldId(Short fieldId) {
		this.fieldId = fieldId;
	}


	public String getField() {
		return this.field;
	}


	public void setField(String field) {
		this.field = field;
	}


	public String getFieldDescription() {
		return this.fieldDescription;
	}


	public void setFieldDescription(String fieldDescription) {
		this.fieldDescription = fieldDescription;
	}


	public String getCurrencyDenomination() {
		return this.currencyDenomination;
	}


	public void setCurrencyDenomination(String currencyDenomination) {
		this.currencyDenomination = currencyDenomination;
	}


	public String getIssuerCompany() {
		return this.issuerCompany;
	}


	public void setIssuerCompany(String issuerCompany) {
		this.issuerCompany = issuerCompany;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public String getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(String investmentType) {
		this.investmentType = investmentType;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public String getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(String investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public String getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(String investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public Short getHierarchyId() {
		return this.hierarchyId;
	}


	public void setHierarchyId(Short hierarchyId) {
		this.hierarchyId = hierarchyId;
	}


	public Integer getInstrumentId() {
		return this.instrumentId;
	}


	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getIncludeVirtualPropertyList() {
		return this.includeVirtualPropertyList;
	}


	public void setIncludeVirtualPropertyList(Boolean includeVirtualPropertyList) {
		this.includeVirtualPropertyList = includeVirtualPropertyList;
	}


	public Integer getAppliesToSecurityId() {
		return this.appliesToSecurityId;
	}


	public void setAppliesToSecurityId(Integer appliesToSecurityId) {
		this.appliesToSecurityId = appliesToSecurityId;
	}


	public Integer getAppliesToInstrumentId() {
		return this.appliesToInstrumentId;
	}


	public void setAppliesToInstrumentId(Integer appliesToInstrumentId) {
		this.appliesToInstrumentId = appliesToInstrumentId;
	}
}
