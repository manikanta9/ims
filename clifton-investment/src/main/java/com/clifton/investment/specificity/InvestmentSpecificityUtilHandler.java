package com.clifton.investment.specificity;

import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;

import java.util.Collection;
import java.util.List;


/**
 * The handler component for investment specificity mappings.
 */
public interface InvestmentSpecificityUtilHandler {

	/**
	 * Gets the list of specificity entries which apply to the given security.
	 *
	 * @param definitionName the specificity definition name filter
	 * @param security       the security
	 * @return the list of applicable objects
	 */
	public List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryListForSecurity(String definitionName, InvestmentSecurity security);


	/**
	 * Gets the list of specificity entries which apply to the given instrument.
	 *
	 * @param definitionName the specificity definition name filter
	 * @param instrument     the instrument
	 * @return the list of applicable objects
	 */
	public List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryListForInstrument(String definitionName, InvestmentInstrument instrument);


	/**
	 * Gets the list of specificity entries which apply to the given hierarchy.
	 *
	 * @param definitionName the specificity definition name filter
	 * @param hierarchy      the hierarchy
	 * @return the list of applicable objects
	 */
	List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryListForHierarchy(String definitionName, InvestmentInstrumentHierarchy hierarchy);


	/**
	 * Generates a populated {@link BaseInvestmentSpecificityBean} instance from the given entry and its properties.
	 *
	 * @param entry the entry from which to generate the bean
	 * @param <T>   the type of the bean
	 * @return the generated bean instance
	 */
	public <T extends BaseInvestmentSpecificityBean> T generatePopulatedSpecificityBean(InvestmentSpecificityEntry entry);


	/**
	 * Populates the virtual property lists for each of the given entries using the property types for their corresponding definitions.
	 *
	 * @param entryList the list of entries whose virtual property lists shall be populated
	 * @see InvestmentSpecificityEntry#virtualPropertyList
	 */
	public void populateVirtualPropertyListForList(List<InvestmentSpecificityEntry> entryList);


	/**
	 * Populates the virtual property list for the given entry using the given list of property types.
	 *
	 * @param entry                 the entry whose virtual property list shall be populated
	 * @param propertyTypesForEntry the list of property types for which the entry's virtual property list should be populated
	 * @see InvestmentSpecificityEntry#virtualPropertyList
	 */
	public void populateVirtualPropertyList(InvestmentSpecificityEntry entry, Collection<InvestmentSpecificityEntryPropertyType> propertyTypesForEntry);
}
