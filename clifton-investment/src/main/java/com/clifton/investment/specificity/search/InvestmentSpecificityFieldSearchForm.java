package com.clifton.investment.specificity.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.specificity.InvestmentSpecificityField;
import com.clifton.investment.specificity.InvestmentSpecificityService;


/**
 * The search form for {@link InvestmentSpecificityField} objects.
 *
 * @see InvestmentSpecificityService
 */
public class InvestmentSpecificityFieldSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,systemColumn.name,description", leftJoin = true)
	private String searchPattern;

	@SearchField(searchField = "name,systemColumn.name")
	private String name;

	@SearchField(searchField = "name")
	private String fieldName;

	@SearchField(searchField = "definition.id")
	private Short definitionId;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Short definitionId) {
		this.definitionId = definitionId;
	}


	public String getFieldName() {
		return this.fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
