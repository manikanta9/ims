package com.clifton.investment.specificity;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.specificity.search.InvestmentSpecificityDefinitionSearchForm;
import com.clifton.investment.specificity.search.InvestmentSpecificityEntryPropertySearchForm;
import com.clifton.investment.specificity.search.InvestmentSpecificityEntryPropertyTypeSearchForm;
import com.clifton.investment.specificity.search.InvestmentSpecificityEntrySearchForm;
import com.clifton.investment.specificity.search.InvestmentSpecificityEntrySearchFormConfigurer;
import com.clifton.investment.specificity.search.InvestmentSpecificityFieldSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Implementor for {@link InvestmentSpecificityService}.
 *
 * @see InvestmentSpecificityService
 */
@Service
public class InvestmentSpecificityServiceImpl implements InvestmentSpecificityService {

	private AdvancedUpdatableDAO<InvestmentSpecificityDefinition, Criteria> investmentSpecificityDefinitionDAO;
	private AdvancedUpdatableDAO<InvestmentSpecificityField, Criteria> investmentSpecificityFieldDAO;
	private AdvancedUpdatableDAO<InvestmentSpecificityEntry, Criteria> investmentSpecificityEntryDAO;
	private AdvancedUpdatableDAO<InvestmentSpecificityEntryPropertyType, Criteria> investmentSpecificityEntryPropertyTypeDAO;
	private AdvancedUpdatableDAO<InvestmentSpecificityEntryProperty, Criteria> investmentSpecificityEntryPropertyDAO;

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSetupService investmentSetupService;
	private InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler;

	private DaoNamedEntityCache<InvestmentSpecificityDefinition> investmentSpecificityDefinitionCache;
	private DaoSingleKeyListCache<InvestmentSpecificityEntryPropertyType, Short> investmentSpecificityEntryPropertyTypeCache;
	private DaoSingleKeyListCache<InvestmentSpecificityEntryProperty, Integer> investmentSpecificityEntryPropertyCache;


	////////////////////////////////////////////////////////////////////////////
	////////            Investment Specificity Definitions              ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSpecificityDefinition getInvestmentSpecificityDefinition(short id) {
		return getInvestmentSpecificityDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentSpecificityDefinition getInvestmentSpecificityDefinitionByName(String name) {
		return getInvestmentSpecificityDefinitionCache().getBeanForKeyValue(getInvestmentSpecificityDefinitionDAO(), name);
	}


	@Override
	public List<InvestmentSpecificityDefinition> getInvestmentSpecificityDefinitionList(InvestmentSpecificityDefinitionSearchForm searchForm) {
		return getInvestmentSpecificityDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentSpecificityDefinition saveInvestmentSpecificityDefinition(InvestmentSpecificityDefinition bean) {
		return getInvestmentSpecificityDefinitionDAO().save(bean);
	}


	@Override
	public void deleteInvestmentSpecificityDefinition(short id) {
		getInvestmentSpecificityDefinitionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Investment Specificity Fields                   ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSpecificityField getInvestmentSpecificityField(short id) {
		return getInvestmentSpecificityFieldDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentSpecificityField> getInvestmentSpecificityFieldList(InvestmentSpecificityFieldSearchForm searchForm) {
		return getInvestmentSpecificityFieldDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentSpecificityField saveInvestmentSpecificityField(InvestmentSpecificityField bean) {
		return getInvestmentSpecificityFieldDAO().save(bean);
	}


	@Override
	public void deleteInvestmentSpecificityField(short id) {
		getInvestmentSpecificityFieldDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Investment Specificity Entries                  ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSpecificityEntry getInvestmentSpecificityEntry(int id) {
		InvestmentSpecificityEntry entry = getInvestmentSpecificityEntryDAO().findByPrimaryKey(id);
		if (entry != null) {
			entry.setPropertyList(getInvestmentSpecificityEntryPropertiesForEntry(entry));
		}
		return entry;
	}


	@Override
	public List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryList(InvestmentSpecificityEntrySearchForm searchForm) {
		List<InvestmentSpecificityEntry> entryList;
		if (searchForm.getAppliesToSecurityId() != null || searchForm.getAppliesToInstrumentId() != null) {
			// Retrieve all entries for security (force client-side filtering rather than performing search-form-based querying)
			String definitionName = searchForm.getDefinition();
			if (searchForm.getDefinitionId() != null) {
				InvestmentSpecificityDefinition definition = getInvestmentSpecificityDefinition(searchForm.getDefinitionId());
				definitionName = definition.getName();
			}
			if (searchForm.getAppliesToSecurityId() != null) {
				entryList = getInvestmentSpecificityEntryListForSecurity(definitionName, searchForm.getAppliesToSecurityId());
			}
			else {
				entryList = getInvestmentSpecificityEntryListForInstrument(definitionName, searchForm.getAppliesToInstrumentId());
			}
			// post filter entryList based on field
			final List<Short> fieldList = searchForm.getFieldId() == null ? new ArrayList<>() : CollectionUtils.createList(searchForm.getFieldId());
			if (searchForm.getFieldId() == null && !StringUtils.isEmpty(searchForm.getField())) {
				InvestmentSpecificityFieldSearchForm fieldSearchForm = new InvestmentSpecificityFieldSearchForm();
				fieldSearchForm.setFieldName(searchForm.getField());
				List<InvestmentSpecificityField> fields = CollectionUtils.asNonNullList(getInvestmentSpecificityFieldList(fieldSearchForm));
				fieldList.addAll(fields.stream().map(InvestmentSpecificityField::getId).collect(Collectors.toList()));
			}
			if (!fieldList.isEmpty()) {
				entryList = CollectionUtils.getStream(entryList).filter(e -> fieldList.contains(e.getField().getId())).collect(Collectors.toList());
			}
		}
		else {
			// Perform standard search-form-based querying
			entryList = getInvestmentSpecificityEntryDAO().findBySearchCriteria(new InvestmentSpecificityEntrySearchFormConfigurer(searchForm));
		}

		// If requested, populate virtual property list
		if (BooleanUtils.isTrue(searchForm.getIncludeVirtualPropertyList())) {
			for (InvestmentSpecificityEntry entry : entryList) {
				entry.setPropertyList(getInvestmentSpecificityEntryPropertiesForEntry(entry));
			}
			getInvestmentSpecificityUtilHandler().populateVirtualPropertyListForList(entryList);
		}
		return entryList;
	}


	@Override
	public List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryListForSecurity(String definitionName, int securityId) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		ValidationUtils.assertNotNull(security, "No security was found for ID [" + securityId + "].", "securityId");
		return getInvestmentSpecificityUtilHandler().getInvestmentSpecificityEntryListForSecurity(definitionName, security);
	}


	@Override
	public List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryListForInstrument(String definitionName, int instrumentId) {
		InvestmentInstrument instrument = getInvestmentInstrumentService().getInvestmentInstrument(instrumentId);
		ValidationUtils.assertNotNull(instrument, "No instrument was found for ID [" + instrumentId + "].", "instrumentId");
		return getInvestmentSpecificityUtilHandler().getInvestmentSpecificityEntryListForInstrument(definitionName, instrument);
	}


	@Override
	public List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryListForHierarchy(String definitionName, short hierarchyId) {
		InvestmentInstrumentHierarchy hierarchy = getInvestmentSetupService().getInvestmentInstrumentHierarchy(hierarchyId);
		ValidationUtils.assertNotNull(hierarchy, "No instrument hierarchy found for ID [" + hierarchyId + "].", "hierarchyId");
		return getInvestmentSpecificityUtilHandler().getInvestmentSpecificityEntryListForHierarchy(definitionName, hierarchy);
	}


	@Override
	public InvestmentSpecificityEntry saveInvestmentSpecificityEntry(InvestmentSpecificityEntry bean) {
		return getInvestmentSpecificityEntryDAO().save(bean);
	}


	/*
	 * This @Transactional reference is necessary as of time this note was written.
	 *
	 * The UpdatableDAO#delete(java.io.Serializable) method normally performs the read and the delete operations in separate transactions. In most cases, this
	 * works without issue. However, an observer is registered for Entry objects which queries for properties, thus storing an instance of the Entry in the
	 * first-level cache for the delete transaction. Since the read operation is in a separate transaction, it produces a separate instance of the Entry bean,
	 * but it is this instance which the delete operation will attempt to delete. During the delete operation, Hibernate finds that the passed reference (from
	 * the read operation) is not the same instance (==) as the first-level cache reference (from the property query), and thus throws a Hibernate
	 * NonUniqueObjectException.
	 *
	 * Wrapping this process in @Transactional here produces a parent transaction for the read and delete operations, allowing them to share a cache so that a
	 * single reference to the Entry bean is used across all operations.
	 */
	@Transactional
	@Override
	public void deleteInvestmentSpecificityEntry(int id) {
		getInvestmentSpecificityEntryDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Investment Specificity Entry Property Types     ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSpecificityEntryPropertyType getInvestmentSpecificityEntryPropertyType(int id) {
		return getInvestmentSpecificityEntryPropertyTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentSpecificityEntryPropertyType> getInvestmentSpecificityEntryPropertyTypeList(InvestmentSpecificityEntryPropertyTypeSearchForm searchForm) {
		return getInvestmentSpecificityEntryPropertyTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<InvestmentSpecificityEntryPropertyType> getInvestmentSpecificityEntryPropertyTypesForDefinition(InvestmentSpecificityDefinition bean) {
		return getInvestmentSpecificityEntryPropertyTypeCache().getBeanListForKeyValue(getInvestmentSpecificityEntryPropertyTypeDAO(), BeanUtils.getBeanIdentity(bean));
	}


	@Override
	public InvestmentSpecificityEntryPropertyType saveInvestmentSpecificityEntryPropertyType(InvestmentSpecificityEntryPropertyType bean) {
		return getInvestmentSpecificityEntryPropertyTypeDAO().save(bean);
	}


	@Override
	public void deleteInvestmentSpecificityEntryPropertyType(int id) {
		getInvestmentSpecificityEntryPropertyTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Investment Specificity Entry Properties         ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSpecificityEntryProperty getInvestmentSpecificityEntryProperty(int id) {
		return getInvestmentSpecificityEntryPropertyDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentSpecificityEntryProperty> getInvestmentSpecificityEntryPropertyList(InvestmentSpecificityEntryPropertySearchForm searchForm) {
		return getInvestmentSpecificityEntryPropertyDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<InvestmentSpecificityEntryProperty> getInvestmentSpecificityEntryPropertyListForEntry(InvestmentSpecificityEntrySearchForm searchForm) {
		searchForm.setIncludeVirtualPropertyList(true);
		return CollectionUtils.getStream(getInvestmentSpecificityEntryList(searchForm))
				.flatMap(e -> CollectionUtils.getStream(e.getPropertyList()))
				.collect(Collectors.toList());
	}


	@Override
	public List<InvestmentSpecificityEntryProperty> getInvestmentSpecificityEntryPropertiesForEntry(InvestmentSpecificityEntry bean) {
		return getInvestmentSpecificityEntryPropertyCache().getBeanListForKeyValue(getInvestmentSpecificityEntryPropertyDAO(), BeanUtils.getBeanIdentity(bean));
	}


	@Override
	public InvestmentSpecificityEntryProperty saveInvestmentSpecificityEntryProperty(InvestmentSpecificityEntryProperty bean) {
		return getInvestmentSpecificityEntryPropertyDAO().save(bean);
	}


	@Override
	public void saveInvestmentSpecificityEntryPropertiesForEntry(InvestmentSpecificityEntry bean) {
		CollectionUtils.getStream(bean.getPropertyList()).forEach(prop -> prop.setEntry(bean));
		getInvestmentSpecificityEntryPropertyDAO().saveList(bean.getPropertyList(), getInvestmentSpecificityEntryPropertiesForEntry(bean));
	}


	@Override
	public void deleteInvestmentSpecificityEntryProperty(int id) {
		getInvestmentSpecificityEntryPropertyDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentSpecificityDefinition, Criteria> getInvestmentSpecificityDefinitionDAO() {
		return this.investmentSpecificityDefinitionDAO;
	}


	public void setInvestmentSpecificityDefinitionDAO(AdvancedUpdatableDAO<InvestmentSpecificityDefinition, Criteria> investmentSpecificityDefinitionDAO) {
		this.investmentSpecificityDefinitionDAO = investmentSpecificityDefinitionDAO;
	}


	public AdvancedUpdatableDAO<InvestmentSpecificityField, Criteria> getInvestmentSpecificityFieldDAO() {
		return this.investmentSpecificityFieldDAO;
	}


	public void setInvestmentSpecificityFieldDAO(AdvancedUpdatableDAO<InvestmentSpecificityField, Criteria> investmentSpecificityFieldDAO) {
		this.investmentSpecificityFieldDAO = investmentSpecificityFieldDAO;
	}


	public AdvancedUpdatableDAO<InvestmentSpecificityEntry, Criteria> getInvestmentSpecificityEntryDAO() {
		return this.investmentSpecificityEntryDAO;
	}


	public void setInvestmentSpecificityEntryDAO(AdvancedUpdatableDAO<InvestmentSpecificityEntry, Criteria> investmentSpecificityEntryDAO) {
		this.investmentSpecificityEntryDAO = investmentSpecificityEntryDAO;
	}


	public AdvancedUpdatableDAO<InvestmentSpecificityEntryPropertyType, Criteria> getInvestmentSpecificityEntryPropertyTypeDAO() {
		return this.investmentSpecificityEntryPropertyTypeDAO;
	}


	public void setInvestmentSpecificityEntryPropertyTypeDAO(AdvancedUpdatableDAO<InvestmentSpecificityEntryPropertyType, Criteria> investmentSpecificityEntryPropertyTypeDAO) {
		this.investmentSpecificityEntryPropertyTypeDAO = investmentSpecificityEntryPropertyTypeDAO;
	}


	public AdvancedUpdatableDAO<InvestmentSpecificityEntryProperty, Criteria> getInvestmentSpecificityEntryPropertyDAO() {
		return this.investmentSpecificityEntryPropertyDAO;
	}


	public void setInvestmentSpecificityEntryPropertyDAO(AdvancedUpdatableDAO<InvestmentSpecificityEntryProperty, Criteria> investmentSpecificityEntryPropertyDAO) {
		this.investmentSpecificityEntryPropertyDAO = investmentSpecificityEntryPropertyDAO;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public InvestmentSpecificityUtilHandler getInvestmentSpecificityUtilHandler() {
		return this.investmentSpecificityUtilHandler;
	}


	public void setInvestmentSpecificityUtilHandler(InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler) {
		this.investmentSpecificityUtilHandler = investmentSpecificityUtilHandler;
	}


	public DaoNamedEntityCache<InvestmentSpecificityDefinition> getInvestmentSpecificityDefinitionCache() {
		return this.investmentSpecificityDefinitionCache;
	}


	public void setInvestmentSpecificityDefinitionCache(DaoNamedEntityCache<InvestmentSpecificityDefinition> investmentSpecificityDefinitionCache) {
		this.investmentSpecificityDefinitionCache = investmentSpecificityDefinitionCache;
	}


	public DaoSingleKeyListCache<InvestmentSpecificityEntryPropertyType, Short> getInvestmentSpecificityEntryPropertyTypeCache() {
		return this.investmentSpecificityEntryPropertyTypeCache;
	}


	public void setInvestmentSpecificityEntryPropertyTypeCache(DaoSingleKeyListCache<InvestmentSpecificityEntryPropertyType, Short> investmentSpecificityEntryPropertyTypeCache) {
		this.investmentSpecificityEntryPropertyTypeCache = investmentSpecificityEntryPropertyTypeCache;
	}


	public DaoSingleKeyListCache<InvestmentSpecificityEntryProperty, Integer> getInvestmentSpecificityEntryPropertyCache() {
		return this.investmentSpecificityEntryPropertyCache;
	}


	public void setInvestmentSpecificityEntryPropertyCache(DaoSingleKeyListCache<InvestmentSpecificityEntryProperty, Integer> investmentSpecificityEntryPropertyCache) {
		this.investmentSpecificityEntryPropertyCache = investmentSpecificityEntryPropertyCache;
	}
}
