package com.clifton.investment.specificity.cache;

import com.clifton.investment.specificity.InvestmentSpecificityEntry;


/**
 * Source object for {@link InvestmentSpecificityEntry} objects. This contains the data necessary for the construction of specificity keys when searching
 * through the associated specificity cache.
 *
 * @see InvestmentSpecificityEntrySpecificityCache
 */
public class InvestmentSpecificityEntrySource {

	private Short investmentSpecificityDefinitionId;
	private Integer currencyDenominationId;
	private Integer issuerCompanyId;
	private Short investmentTypeId;
	private Short investmentTypeSubTypeId;
	private Short investmentTypeSubType2Id;
	private Short investmentInstrumentHierarchyId;
	private Integer investmentInstrumentId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getInvestmentSpecificityDefinitionId() {
		return this.investmentSpecificityDefinitionId;
	}


	public void setInvestmentSpecificityDefinitionId(Short investmentSpecificityDefinitionId) {
		this.investmentSpecificityDefinitionId = investmentSpecificityDefinitionId;
	}


	public Integer getCurrencyDenominationId() {
		return this.currencyDenominationId;
	}


	public void setCurrencyDenominationId(Integer currencyDenominationId) {
		this.currencyDenominationId = currencyDenominationId;
	}


	public Integer getIssuerCompanyId() {
		return this.issuerCompanyId;
	}


	public void setIssuerCompanyId(Integer issuerCompanyId) {
		this.issuerCompanyId = issuerCompanyId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public Short getInvestmentInstrumentHierarchyId() {
		return this.investmentInstrumentHierarchyId;
	}


	public void setInvestmentInstrumentHierarchyId(Short investmentInstrumentHierarchyId) {
		this.investmentInstrumentHierarchyId = investmentInstrumentHierarchyId;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}
}
