package com.clifton.investment.specificity.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.specificity.InvestmentSpecificityService;


/**
 * The search form for {@link com.clifton.investment.specificity.InvestmentSpecificityEntryPropertyType} objects.
 *
 * @see InvestmentSpecificityService
 */
public class InvestmentSpecificityEntryPropertyTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,entryBeanPropertyName")
	private String searchPattern;

	@SearchField
	private String name;
	@SearchField(searchField = "entryBeanPropertyName")
	private String propertyName;
	@SearchField(searchField = "definition.id")
	private Short definitionId;
	@SearchField(searchField = "dataType.id")
	private Short dataTypeId;
	@SearchField
	private Boolean multipleValuesAllowed;
	@SearchField
	private Boolean required;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPropertyName() {
		return this.propertyName;
	}


	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}


	public Short getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Short definitionId) {
		this.definitionId = definitionId;
	}


	public Short getDataTypeId() {
		return this.dataTypeId;
	}


	public void setDataTypeId(Short dataTypeId) {
		this.dataTypeId = dataTypeId;
	}


	public Boolean getMultipleValuesAllowed() {
		return this.multipleValuesAllowed;
	}


	public void setMultipleValuesAllowed(Boolean multipleValuesAllowed) {
		this.multipleValuesAllowed = multipleValuesAllowed;
	}


	public Boolean getRequired() {
		return this.required;
	}


	public void setRequired(Boolean required) {
		this.required = required;
	}
}
