package com.clifton.investment.specificity;


import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.investment.specificity.cache.InvestmentSpecificityEntrySpecificityCache;
import com.clifton.system.schema.column.SystemColumn;


/**
 * The InvestmentSpecificityField class defines a field that supports specificity based entries for selected Specificity Definition. The field can either be a
 * System Column (when the definition is linked to a System Table) or a user-defined field.
 *
 * @see InvestmentSpecificityEntrySpecificityCache
 */
public class InvestmentSpecificityField extends NamedEntityWithoutLabel<Short> {

	private InvestmentSpecificityDefinition definition;
	private SystemColumn systemColumn;
	private boolean systemDefined;


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(InvestmentSpecificityDefinition definition) {
		this.definition = definition;
	}


	public SystemColumn getSystemColumn() {
		return this.systemColumn;
	}


	public void setSystemColumn(SystemColumn systemColumn) {
		this.systemColumn = systemColumn;
	}


	@Override
	public String getLabel() {
		return getSystemColumn() != null ? getSystemColumn().getLabel() : super.getLabel();
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}
}
