package com.clifton.investment.specificity;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.ObjectUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.investment.specificity.cache.InvestmentSpecificityEntrySpecificityCache;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;

import java.util.Collection;
import java.util.List;
import java.util.StringJoiner;


/**
 * The {@link InvestmentSpecificityEntry} class defines properties for the specified Field and the selected Specificity Scope. Specificity Scope fields are
 * displayed in the order of specificity. For each definition, the most specific entries for each field will be used.
 *
 * @see InvestmentSpecificityEntrySpecificityCache
 */
public class InvestmentSpecificityEntry extends BaseEntity<Integer> implements LabeledObject, SystemEntityModifyConditionAwareAdminRequired {

	private InvestmentSpecificityField field;

	private InvestmentSecurity currencyDenomination;
	private BusinessCompany issuerCompany;
	private InvestmentType investmentType;
	private InvestmentTypeSubType investmentTypeSubType;
	private InvestmentTypeSubType2 investmentTypeSubType2;
	private InvestmentInstrumentHierarchy hierarchy;
	private InvestmentInstrument instrument;

	/**
	 * The list of properties for the entity. A link has been created for this property for SQL navigation, but this field is not automatically populated.
	 */
	private List<InvestmentSpecificityEntryProperty> propertyList;

	/**
	 * If populated, this list shall contain an element for each property type for the definition of the entry. Property types for which there is no existing
	 * property shall be represented by an empty property.
	 * <p>
	 * Given that the standard property list is mapped via Hibernate as bags, the property order is not guaranteed on retrieval. This field provides a
	 * consistent method of representing all possible properties for an entry, including those which are not populated, in a predictable order.
	 * <p>
	 * This is a transient field and is not automatically populated.
	 *
	 * @see InvestmentSpecificityUtilHandler#populateVirtualPropertyList(InvestmentSpecificityEntry, Collection)
	 */
	private List<InvestmentSpecificityEntryProperty> virtualPropertyList;

	////////////////////////////////////////////////////////////////////////////
	////////            Custom Accessors                                ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder labelSb = new StringBuilder(50);

		// Add hierarchy to label
		ObjectUtils.doIfPresent(getInstrument(), obj -> labelSb.append(obj.getLabel()))
				.orElseIfPresent(getHierarchy(), obj -> labelSb.append(obj.getLabel()))
				.orElseIfPresent(getInvestmentType(), type -> {
					labelSb.append(type.getName());
					ObjectUtils.doIfPresent(getInvestmentTypeSubType(), obj -> labelSb.append('-').append(obj.getName()));
					ObjectUtils.doIfPresent(getInvestmentTypeSubType2(), obj -> labelSb.append('-').append(obj.getName()));
				});

		// Add sub-criteria to label
		StringJoiner subCriteriaSj = new StringJoiner(", ");
		ObjectUtils.doIfPresent(getIssuerCompany(), obj -> subCriteriaSj.add(obj.getLabel()));
		ObjectUtils.doIfPresent(getCurrencyDenomination(), obj -> subCriteriaSj.add(obj.getLabel()));
		if (subCriteriaSj.length() > 0) {
			labelSb.append(" - ").append(subCriteriaSj);
		}

		// Add fallback label prefix
		if (labelSb.length() < 1) {
			labelSb.append("[All Securities]");
		}

		// Add field to label
		ObjectUtils.doIfPresent(getField(), fld -> {
			labelSb.append(": ");
			ObjectUtils.doIfPresent(fld.getSystemColumn(), obj -> labelSb.append(obj.getLabel()))
					.orElseIfPresent(fld.getName(), labelSb::append);
		});

		return labelSb.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Entity Modify Condition Overrides               ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return getField().getDefinition().getEntityModifyCondition();
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityField getField() {
		return this.field;
	}


	public void setField(InvestmentSpecificityField field) {
		this.field = field;
	}


	public InvestmentSecurity getCurrencyDenomination() {
		return this.currencyDenomination;
	}


	public void setCurrencyDenomination(InvestmentSecurity currencyDenomination) {
		this.currencyDenomination = currencyDenomination;
	}


	public BusinessCompany getIssuerCompany() {
		return this.issuerCompany;
	}


	public void setIssuerCompany(BusinessCompany issuerCompany) {
		this.issuerCompany = issuerCompany;
	}


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}


	public InvestmentTypeSubType getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(InvestmentTypeSubType investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public InvestmentTypeSubType2 getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(InvestmentTypeSubType2 investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public InvestmentInstrumentHierarchy getHierarchy() {
		return this.hierarchy;
	}


	public void setHierarchy(InvestmentInstrumentHierarchy hierarchy) {
		this.hierarchy = hierarchy;
	}


	public InvestmentInstrument getInstrument() {
		return this.instrument;
	}


	public void setInstrument(InvestmentInstrument instrument) {
		this.instrument = instrument;
	}


	public List<InvestmentSpecificityEntryProperty> getPropertyList() {
		return this.propertyList;
	}


	public void setPropertyList(List<InvestmentSpecificityEntryProperty> propertyList) {
		this.propertyList = propertyList;
	}


	public List<InvestmentSpecificityEntryProperty> getVirtualPropertyList() {
		return this.virtualPropertyList;
	}


	public void setVirtualPropertyList(List<InvestmentSpecificityEntryProperty> virtualPropertyList) {
		this.virtualPropertyList = virtualPropertyList;
	}
}
