package com.clifton.investment.specificity;

import com.clifton.investment.specificity.cache.InvestmentSpecificityEntrySpecificityCache;
import com.clifton.investment.specificity.search.InvestmentSpecificityDefinitionSearchForm;
import com.clifton.investment.specificity.search.InvestmentSpecificityEntryPropertySearchForm;
import com.clifton.investment.specificity.search.InvestmentSpecificityEntryPropertyTypeSearchForm;
import com.clifton.investment.specificity.search.InvestmentSpecificityEntrySearchForm;
import com.clifton.investment.specificity.search.InvestmentSpecificityFieldSearchForm;

import java.util.List;


/**
 * The service for querying objects related to investment specificity mappings.
 */
public interface InvestmentSpecificityService {


	public static final String SPECIFICITY_GENERIC_MAPPING_DEFINITION = "Investment Instrument Generic Mapping";
	public static final String SPECIFICITY_INSTRUMENT_TO_COMPANY_DEFINITION = "Investment Instrument to Company Mapping";
	public static final String SPECIFICITY_ISITC_CODE_FIELD = "ISITC Classification Code";
	public static final String SPECIFICITY_SETTLEMENT_COMPANY_FIELD = "Settlement Company";
	public static final String SPECIFICITY_FIELD_VALUE = "Specificity Field Value";
	public static final String SPECIFICITY_BUSINESS_COMPANY_PROPERTY_TYPE = "Business Company";
	public static final String SPECIFICITY_BLOOMBERG_TYPE_MAPPING_DEFINITION = "Investment Instrument Bloomberg Type Mapping";

	////////////////////////////////////////////////////////////////////////////
	////////            Investment Specificity Definitions              ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityDefinition getInvestmentSpecificityDefinition(short id);


	public InvestmentSpecificityDefinition getInvestmentSpecificityDefinitionByName(String name);


	public List<InvestmentSpecificityDefinition> getInvestmentSpecificityDefinitionList(InvestmentSpecificityDefinitionSearchForm searchForm);


	public InvestmentSpecificityDefinition saveInvestmentSpecificityDefinition(InvestmentSpecificityDefinition bean);


	public void deleteInvestmentSpecificityDefinition(short id);


	////////////////////////////////////////////////////////////////////////////
	////////            Investment Specificity Fields                   ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityField getInvestmentSpecificityField(short id);


	public List<InvestmentSpecificityField> getInvestmentSpecificityFieldList(InvestmentSpecificityFieldSearchForm searchForm);


	public InvestmentSpecificityField saveInvestmentSpecificityField(InvestmentSpecificityField bean);


	public void deleteInvestmentSpecificityField(short id);


	////////////////////////////////////////////////////////////////////////////
	////////            Investment Specificity Entries                  ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityEntry getInvestmentSpecificityEntry(int id);


	public List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryList(InvestmentSpecificityEntrySearchForm searchForm);


	/**
	 * Gets the list of entries which apply to the given security. Up to one entry for each {@link InvestmentSpecificityField} under the given {@link
	 * InvestmentSpecificityDefinition} may be returned.
	 *
	 * @param definitionName the specificity definition name filter
	 * @param securityId     the security ID
	 * @return the list of applicable entries
	 * @see InvestmentSpecificityEntrySpecificityCache
	 */
	public List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryListForSecurity(String definitionName, int securityId);


	/**
	 * Gets the list of objects which apply to the given instrument. Up to one entry for each {@link InvestmentSpecificityField} under the given {@link
	 * InvestmentSpecificityDefinition} may be returned.
	 *
	 * @param definitionName the specificity definition name filter
	 * @param instrumentId   the instrument ID
	 * @return the list of applicable objects
	 * @see InvestmentSpecificityEntrySpecificityCache
	 */
	public List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryListForInstrument(String definitionName, int instrumentId);


	/**
	 * Gets the list of objects which apply to the given hierarchy.
	 *
	 * @param definitionName the specificity definition name filter
	 * @param hierarchyId    investment instrument hierarchy
	 * @return the list of applicable objects
	 * @see InvestmentSpecificityEntrySpecificityCache
	 */
	public List<InvestmentSpecificityEntry> getInvestmentSpecificityEntryListForHierarchy(String definitionName, short hierarchyId);


	public InvestmentSpecificityEntry saveInvestmentSpecificityEntry(InvestmentSpecificityEntry bean);


	public void deleteInvestmentSpecificityEntry(int id);


	////////////////////////////////////////////////////////////////////////////
	////////            Investment Specificity Entry Property Types     ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityEntryPropertyType getInvestmentSpecificityEntryPropertyType(int id);


	public List<InvestmentSpecificityEntryPropertyType> getInvestmentSpecificityEntryPropertyTypeList(InvestmentSpecificityEntryPropertyTypeSearchForm searchForm);


	public List<InvestmentSpecificityEntryPropertyType> getInvestmentSpecificityEntryPropertyTypesForDefinition(InvestmentSpecificityDefinition bean);


	public InvestmentSpecificityEntryPropertyType saveInvestmentSpecificityEntryPropertyType(InvestmentSpecificityEntryPropertyType bean);


	public void deleteInvestmentSpecificityEntryPropertyType(int id);


	////////////////////////////////////////////////////////////////////////////
	////////            Investment Specificity Entry Properties         ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityEntryProperty getInvestmentSpecificityEntryProperty(int id);


	public List<InvestmentSpecificityEntryProperty> getInvestmentSpecificityEntryPropertyList(InvestmentSpecificityEntryPropertySearchForm searchForm);


	public List<InvestmentSpecificityEntryProperty> getInvestmentSpecificityEntryPropertyListForEntry(InvestmentSpecificityEntrySearchForm searchForm);


	public List<InvestmentSpecificityEntryProperty> getInvestmentSpecificityEntryPropertiesForEntry(InvestmentSpecificityEntry bean);


	public InvestmentSpecificityEntryProperty saveInvestmentSpecificityEntryProperty(InvestmentSpecificityEntryProperty bean);


	/**
	 * Saves the entire property list for the given entry. The list attached to the entry will be compared with the saved property list. Any existing properties
	 * will be updated or removed.
	 */
	public void saveInvestmentSpecificityEntryPropertiesForEntry(InvestmentSpecificityEntry bean);


	public void deleteInvestmentSpecificityEntryProperty(int id);
}
