package com.clifton.investment.specificity.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.specificity.InvestmentSpecificityDefinition;
import com.clifton.investment.specificity.InvestmentSpecificityService;


/**
 * The search form for {@link InvestmentSpecificityDefinition} objects.
 *
 * @see InvestmentSpecificityService
 */
public class InvestmentSpecificityDefinitionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private Short id;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "systemTable.id")
	private Short systemTableId;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getSystemTableId() {
		return this.systemTableId;
	}


	public void setSystemTableId(Short systemTableId) {
		this.systemTableId = systemTableId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
