package com.clifton.investment.specificity.validation;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityEntryProperty;
import com.clifton.investment.specificity.InvestmentSpecificityService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The DAO observer for {@link InvestmentSpecificityEntry} entities. This class provides automated functionality which should occur on DAO modification, such as
 * saving or deleting associated {@link InvestmentSpecificityEntryProperty} entities.
 */
@Component
public class InvestmentSpecificityEntryObserver extends SelfRegisteringDaoObserver<InvestmentSpecificityEntry> {

	private InvestmentSpecificityService investmentSpecificityService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void configurePersistedInstance(InvestmentSpecificityEntry persistedInstance, InvestmentSpecificityEntry originalInstance) {
		super.configurePersistedInstance(persistedInstance, originalInstance);

		// On save, migrate property list from request to persisted instance so that we can cascade property saves in observer event handlers
		if (persistedInstance != null) {
			persistedInstance.setPropertyList(originalInstance.getPropertyList());
		}
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<InvestmentSpecificityEntry> dao, DaoEventTypes event, InvestmentSpecificityEntry entry) {
		// On deletion, delete any corresponding properties for this entry
		if (event.isDelete()) {
			InvestmentSpecificityEntry originalEntry = getOriginalBean(dao, entry);
			List<InvestmentSpecificityEntryProperty> propertyList = getInvestmentSpecificityService().getInvestmentSpecificityEntryPropertiesForEntry(originalEntry);
			for (InvestmentSpecificityEntryProperty property : CollectionUtils.getIterable(propertyList)) {
				getInvestmentSpecificityService().deleteInvestmentSpecificityEntryProperty(property.getId());
			}
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<InvestmentSpecificityEntry> dao, DaoEventTypes event, InvestmentSpecificityEntry entry, Throwable e) {
		// Guard-clause: Exception hit; no action necessary
		if (e != null) {
			return;
		}

		if (event.isInsert() || event.isUpdate()) {
			getInvestmentSpecificityService().saveInvestmentSpecificityEntryPropertiesForEntry(entry);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityService getInvestmentSpecificityService() {
		return this.investmentSpecificityService;
	}


	public void setInvestmentSpecificityService(InvestmentSpecificityService investmentSpecificityService) {
		this.investmentSpecificityService = investmentSpecificityService;
	}
}
