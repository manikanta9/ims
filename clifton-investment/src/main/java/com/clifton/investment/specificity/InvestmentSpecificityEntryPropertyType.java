package com.clifton.investment.specificity;


import com.clifton.system.userinterface.BaseNamedSystemUserInterfaceAwareVirtualEntity;
import com.clifton.system.userinterface.SystemUserInterfaceUsedByAware;
import com.clifton.system.userinterface.SystemUserInterfaceValueUsedByConfig;


/**
 * The {@link InvestmentSpecificityEntryPropertyType} class defines a single property field for a {@link InvestmentSpecificityDefinition}. This class contains
 * fields for configuring the data type, UI configuration, and associated bean property name for the property field, among other attributes.
 *
 * @see InvestmentSpecificityEntryProperty
 */
public class InvestmentSpecificityEntryPropertyType extends BaseNamedSystemUserInterfaceAwareVirtualEntity<InvestmentSpecificityEntryPropertyType> implements SystemUserInterfaceUsedByAware {

	/**
	 * The definition to which this property type belongs.
	 */
	private InvestmentSpecificityDefinition definition;

	/**
	 * The field name of the {@link InvestmentSpecificityDefinition#entryBeanClassName bean class} for this property for the associated {@link #definition}.
	 */
	private String entryBeanPropertyName;

	/**
	 * Defines whether or not multiple values are allowed for this property type.
	 */
	private boolean multipleValuesAllowed;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemUserInterfaceValueUsedByConfig getSystemUserInterfaceValueConfig() {
		return new SystemUserInterfaceValueUsedByConfig("InvestmentSpecificityEntryProperty", "value", "type", "entry", "InvestmentSpecificityEntry", null);
	}


	@Override
	public String getUsedByLabel() {
		String definitionName = (getDefinition() != null ? getDefinition().getLabel() : "[UNKNOWN SPECIFICITY DEFINITION]");
		return String.format("Investment Specificity Entry Property Type: %s.%s", definitionName, getLabel());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(InvestmentSpecificityDefinition definition) {
		this.definition = definition;
	}


	public String getEntryBeanPropertyName() {
		return this.entryBeanPropertyName;
	}


	public void setEntryBeanPropertyName(String entryBeanPropertyName) {
		this.entryBeanPropertyName = entryBeanPropertyName;
	}


	public boolean isMultipleValuesAllowed() {
		return this.multipleValuesAllowed;
	}


	public void setMultipleValuesAllowed(boolean multipleValuesAllowed) {
		this.multipleValuesAllowed = multipleValuesAllowed;
	}
}
