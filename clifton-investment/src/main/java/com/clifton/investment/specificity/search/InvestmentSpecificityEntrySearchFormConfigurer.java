package com.clifton.investment.specificity.search;

import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * Search configurer for {@link InvestmentSpecificityEntry} queries.
 */
public class InvestmentSpecificityEntrySearchFormConfigurer extends HibernateSearchFormConfigurer {

	/**
	 * The list of fields which will be searched when evaluating the {@code searchPattern} parameter.
	 */
	private static final String[] SEARCH_PATTERN_FIELDS = new String[]{"field.definition.name", "field.name", "field.systemColumn.label", "investmentType.name", "investmentTypeSubType.name", "investmentTypeSubType2.name", "hierarchy.label", "instrument.name", "propertyList.text"};


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSpecificityEntrySearchFormConfigurer(InvestmentSpecificityEntrySearchForm searchForm) {
		super(searchForm);
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);
		InvestmentSpecificityEntrySearchForm searchForm = (InvestmentSpecificityEntrySearchForm) getSortableSearchForm();

		// Use a sub-query expression for search patterns so that all linked elements for the entity (e.g., propertyList) will still be retrieved
		if (searchForm.getSearchPattern() != null) {
			// Generate search pattern conditions
			String searchPatternAliasPrefix = "sp";
			DetachedCriteria searchPatternCriteria = DetachedCriteria.forClass(InvestmentSpecificityEntry.class, searchPatternAliasPrefix);
			String searchPatternParam = '%' + searchForm.getSearchPattern() + '%';
			Map<String, Criteria> criteriaPathMap = new HashMap<>();
			Criterion[] searchPatternConditions = Arrays.stream(SEARCH_PATTERN_FIELDS)
					.map(path -> {
						String fieldParent = StringUtils.substringBeforeLast(path, ".");
						String alias = HibernateUtils.getPathAlias(fieldParent, searchPatternCriteria,
								searchPatternAliasPrefix, JoinType.LEFT_OUTER_JOIN, criteriaPathMap);
						String field = (alias == null ? "" : ".") + StringUtils.substringAfterLast(path, ".");
						return Restrictions.like(alias + field, searchPatternParam);
					})
					.toArray(Criterion[]::new);

			// Apply restrictions to criteria
			searchPatternCriteria.setProjection(Projections.id());
			searchPatternCriteria.add(Restrictions.or(searchPatternConditions));
			searchPatternCriteria.add(Restrictions.eqProperty("id", criteria.getAlias() + ".id"));
			criteria.add(Subqueries.exists(searchPatternCriteria));
		}
	}
}
