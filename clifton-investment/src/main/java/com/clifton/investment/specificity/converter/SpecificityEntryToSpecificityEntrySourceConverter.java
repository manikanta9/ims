package com.clifton.investment.specificity.converter;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.cache.InvestmentSpecificityEntrySource;
import org.springframework.stereotype.Component;


/**
 * {@link Converter} to convert {@link InvestmentSpecificityEntry} entities into {@link InvestmentSpecificityEntrySource} entities.
 */
@Component
public class SpecificityEntryToSpecificityEntrySourceConverter implements Converter<InvestmentSpecificityEntry, InvestmentSpecificityEntrySource> {

	@Override
	public InvestmentSpecificityEntrySource convert(InvestmentSpecificityEntry from) {
		InvestmentSpecificityEntrySource entrySource = new InvestmentSpecificityEntrySource();
		entrySource.setInvestmentSpecificityDefinitionId(BeanUtils.getBeanIdentity(from.getField().getDefinition()));
		entrySource.setCurrencyDenominationId(BeanUtils.getBeanIdentity(from.getCurrencyDenomination()));
		entrySource.setIssuerCompanyId(BeanUtils.getBeanIdentity(from.getIssuerCompany()));
		entrySource.setInvestmentTypeId(BeanUtils.getBeanIdentity(from.getInvestmentType()));
		entrySource.setInvestmentTypeSubTypeId(BeanUtils.getBeanIdentity(from.getInvestmentTypeSubType()));
		entrySource.setInvestmentTypeSubType2Id(BeanUtils.getBeanIdentity(from.getInvestmentTypeSubType2()));
		entrySource.setInvestmentInstrumentHierarchyId(BeanUtils.getBeanIdentity(from.getHierarchy()));
		entrySource.setInvestmentInstrumentId(BeanUtils.getBeanIdentity(from.getInstrument()));
		return entrySource;
	}
}
