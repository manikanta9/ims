package com.clifton.investment.replication.dynamic;


import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureService;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureWeight;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.system.bean.SystemBeanService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentReplicationCalculatorSecurityStructure</code> generates the replication allocation list
 * dynamically for a security that uses {@link InvestmentSecurityStructureWeight} to dynamically create security list/current security
 * and dynamic weights.
 *
 * @author manderson
 */
public class InvestmentReplicationCalculatorSecurityStructure extends BaseInvestmentReplicationDynamicCalculator {

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentInstrumentStructureService investmentInstrumentStructureService;

	private SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * For structured index types, this is OK because the weights are all dynamic and not saved.
	 * For model portfolios, this can cause issues because values are saved and would skew the results.
	 * There is validation in the calculation that prevents as well
	 * NOTE: THIS OPTION SHOULD LIKELY BE REMOVED, LEAVING HERE FOR BACKWARDS COMPATIBILITY WITH HOW IT USED TO WORK
	 */
	private boolean useNextBusinessDayForStructuredIndexCalculator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateInvestmentReplicationAllocation(InvestmentReplicationAllocation allocation, Date balanceDate) {
		ValidationUtils.assertNull(allocation.getReplicationSecurityGroup(), "Security Group selection is not supported for Dynamic Security Structure Replication Calculator.");
		ValidationUtils.assertNull(allocation.getReplicationInstrument(), "Instrument selection is not supported for Dynamic Security Structure Replication Calculator.");
		ValidationUtils.assertNotNull(allocation.getReplicationSecurity(), "Security selection is required for Dynamic Security Structure Replication Calculator.");
		ValidationUtils.assertFalse(allocation.isSecurityUnderlying(), "Security Is Underlying selection is not supported for Dynamic Security Structure Replication Calculator.");
		// Calculation for NOT a specific date will just ensure structure is supported on the instrument itself
		if (balanceDate == null) {
			ValidationUtils.assertNotEmpty(getInvestmentInstrumentStructureService().getInvestmentInstrumentStructureListByInstrument(allocation.getReplicationSecurity().getInstrument().getId()),
					"Security [" + allocation.getReplicationSecurity().getLabel() + "] instrument is not set up to support structures.");
		}
		// Otherwise calculation implementation will need to pull the data in order to calculate
		// so to avoid double look ups will verify there that the security supports structures on that date
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentReplicationAllocation> calculateImpl(InvestmentReplication bean, Date balanceDate) {
		// Already Validated that there is only one
		InvestmentReplicationAllocation allocation = bean.getAllocationList().get(0);
		List<InvestmentReplicationAllocation> repAllocationList = new ArrayList<>();

		List<InvestmentSecurityStructureAllocation> securityAllocationList = getSecurityAllocationListForReplicationAllocation(allocation, balanceDate);
		for (InvestmentSecurityStructureAllocation structureAllocation : CollectionUtils.getIterable(securityAllocationList)) {
			repAllocationList.add(createReplicationAllocation(allocation, structureAllocation));
		}
		return repAllocationList;
	}


	protected List<InvestmentSecurityStructureAllocation> getSecurityAllocationListForReplicationAllocation(InvestmentReplicationAllocation allocation, Date balanceDate) {
		// Get method also validates setup is configured and populated
		return getInvestmentInstrumentStructureService().getInvestmentSecurityStructureAllocationListForSecurityAndDate(null, allocation.getReplicationSecurity().getId(), balanceDate, isUseNextBusinessDayForStructuredIndexCalculator());
	}


	protected InvestmentReplicationAllocation createReplicationAllocation(InvestmentReplicationAllocation allocation, InvestmentSecurityStructureAllocation structureAllocation) {

		InvestmentSecurityStructureWeight securityWeight = structureAllocation.getSecurityStructureWeight();

		InvestmentReplicationAllocation repAlloc = new InvestmentReplicationAllocation();
		repAlloc.setReplication(allocation.getReplication());
		repAlloc.setId(allocation.getId());
		repAlloc.setOrder(securityWeight.getInstrumentWeight().getOrder() == null ? null : securityWeight.getInstrumentWeight().getOrder().shortValue());

		// Set at the instrument level in case we hold multiple - current security will also be set
		repAlloc.setReplicationInstrument(structureAllocation.getCurrentSecurity().getInstrument());
		repAlloc.setAllocationWeight(structureAllocation.getAllocationWeight());
		repAlloc.setCurrentSecurityCalculatorBean(structureAllocation.getSecurityStructureWeight().getCurrentSecurityCalculatorBean());
		repAlloc.setCurrentSecurityTargetAdjustmentType(structureAllocation.getSecurityStructureWeight().getCurrentSecurityTargetAdjustmentType());
		repAlloc.setAlwaysIncludeCurrentSecurity(structureAllocation.getSecurityStructureWeight().isAlwaysIncludeCurrentSecurity());

		return repAlloc;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isUseNextBusinessDayForStructuredIndexCalculator() {
		return this.useNextBusinessDayForStructuredIndexCalculator;
	}


	public void setUseNextBusinessDayForStructuredIndexCalculator(boolean useNextBusinessDayForStructuredIndexCalculator) {
		this.useNextBusinessDayForStructuredIndexCalculator = useNextBusinessDayForStructuredIndexCalculator;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentInstrumentStructureService getInvestmentInstrumentStructureService() {
		return this.investmentInstrumentStructureService;
	}


	public void setInvestmentInstrumentStructureService(InvestmentInstrumentStructureService investmentInstrumentStructureService) {
		this.investmentInstrumentStructureService = investmentInstrumentStructureService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
