package com.clifton.investment.replication.dynamic;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory;
import com.clifton.investment.replication.history.InvestmentReplicationHistory;
import com.clifton.investment.replication.history.rebuild.InvestmentReplicationHistoryRebuildService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentReplicationCalculatorBlended</code> is a {@link BaseInvestmentReplicationDynamicCalculator} that acts as a rollup of 1 or more child replications at
 * specified percentages.
 * <p>
 * An example of the Blended Replication use for FX is blending 2 currency pair Replications. There would be two child replications, one for each currency pair to include in the
 * replication. Each has 3 replication allocations (one for each tranche using the current security calculator you previously created for different occurrences). The blended
 * replication would then group and summarize the 6 replications from the 2 child replications.
 *
 * @author michaelm
 */
public class InvestmentReplicationCalculatorBlended extends BaseInvestmentReplicationDynamicCalculator {

	public static final String BLENDED_REPLICATION_SYSTEM_BEAN_TYPE_NAME = "Blended Replication Calculator";

	private InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService;

	private boolean copyCurrentSecurityFromChildAllocationHistory = false;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void validateReplicationTypeSupported(InvestmentReplication replication) {
		InvestmentReplicationType replicationType = replication.getType();
		ValidationUtils.assertNotNull(replicationType, "Replication Type is required for Dynamic Replication Calculators.");
		ValidationUtils.assertTrue(replicationType.isRollupReplication(), String.format("Replication Type [%s] is not supported because Blended Replication Calculators only support Replication rollups.", replicationType.getName()));
	}


	@Override
	protected Integer getMaxAllocationListSize() {
		return null; // allow more than 1 allocation
	}


	@Override
	public void validateInvestmentReplicationAllocation(InvestmentReplicationAllocation allocation, @SuppressWarnings("unused") Date date) {
		InvestmentReplication childReplication = allocation.getChildReplication();
		ValidationUtils.assertNotNull(childReplication, "Blended Replications only support Replication Allocations that have a Child Replication.");
		ValidationUtils.assertFalse(childReplication.getType().isRollupReplication(), "Blended Replications do not support Child Replications that are rollups.");
		ValidationUtils.assertNull(childReplication.getDynamicCalculatorBean(), "Blended Replications do not support Child Replications that use a Dynamic Calculator.");
		ValidationUtils.assertFalse(childReplication.isReverseExposureSign(), "Blended Replications do not support Child Replications that have a reversed exposure sign.");
		validateChildAllocations(childReplication);
	}


	private void validateChildAllocations(InvestmentReplication childReplication) {
		for (InvestmentReplicationAllocation allocation : CollectionUtils.getIterable(childReplication.getAllocationList())) {
			ValidationUtils.assertFalse(allocation.isPortfolioSelectedCurrentSecurity(), "Blended Replications do not support Child Replications that have an allocation with a portfolio defined Current Security.");
			ValidationUtils.assertNull(allocation.getMatchingReplication(), "Blended Replications do not support Child Replications that have an allocation with a Matching Replication.");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentReplicationAllocation> calculateImpl(InvestmentReplication replication, Date balanceDate) {
		Map<String, InvestmentReplicationAllocation> uniqueStringToBlendedAllocationMap = new HashMap<>();
		for (InvestmentReplicationAllocation allocation : CollectionUtils.getIterable(BeanUtils.sortWithFunction(replication.getAllocationList(), InvestmentReplicationAllocation::getOrder, true))) {
			populateBlendedAllocationMap(uniqueStringToBlendedAllocationMap, replication, allocation, balanceDate, (short) (CollectionUtils.getSize(uniqueStringToBlendedAllocationMap.values()) + 1));
		}
		return new ArrayList<>(uniqueStringToBlendedAllocationMap.values());
	}


	private void populateBlendedAllocationMap(Map<String, InvestmentReplicationAllocation> uniqueStringToBlendedAllocationMap, InvestmentReplication replication, InvestmentReplicationAllocation allocation, Date balanceDate, short currentOrder) {
		InvestmentReplicationHistory childReplicationHistory = getChildReplicationHistory(allocation, balanceDate);
		if (childReplicationHistory != null) {
			for (InvestmentReplicationAllocationHistory childAllocationHistory : BeanUtils.sortWithFunction(childReplicationHistory.getReplicationAllocationHistoryList(), allocationHistory -> allocationHistory.getReplicationAllocation().getOrder(), true)) {
				InvestmentReplicationAllocation blendedAllocation = createBlendedAllocation(replication, allocation, childAllocationHistory.getReplicationAllocation(), currentOrder++);
				if (isCopyCurrentSecurityFromChildAllocationHistory() && childAllocationHistory.getCurrentInvestmentSecurity() != null) {
					blendedAllocation.setReplicationSecurity(childAllocationHistory.getCurrentInvestmentSecurity());
					blendedAllocation.setCurrentSecurityCalculatorBean(null);
				}
				InvestmentReplicationAllocation existingAllocation = uniqueStringToBlendedAllocationMap.get(blendedAllocation.getAllocationUniqueString());
				if (existingAllocation == null) {
					uniqueStringToBlendedAllocationMap.put(blendedAllocation.getAllocationUniqueString(), blendedAllocation);
				}
				else {
					existingAllocation.setAllocationWeight(MathUtils.add(existingAllocation.getAllocationWeight(), blendedAllocation.getAllocationWeight()));
					existingAllocation.setOrder((short) Math.min(existingAllocation.getOrder(), blendedAllocation.getOrder()));
				}
			}
		}
	}


	private InvestmentReplicationAllocation createBlendedAllocation(InvestmentReplication replication, InvestmentReplicationAllocation allocation, InvestmentReplicationAllocation childAllocation, short currentOrder) {
		InvestmentReplicationAllocation blendedAllocation = new InvestmentReplicationAllocation();
		BeanUtils.copyProperties(childAllocation, blendedAllocation);
		blendedAllocation.setId(null);
		blendedAllocation.setEndDate(null);
		blendedAllocation.setStartDate(null);
		blendedAllocation.setReplication(replication);
		blendedAllocation.setOrder(childAllocation.getOrder() != null ? (short) (childAllocation.getOrder() + currentOrder) : currentOrder);
		blendedAllocation.setAllocationWeight(calculateBlendedAllocationWeight(allocation, childAllocation));
		if (allocation.getReplicationTypeOverride() != null) {
			blendedAllocation.setReplicationTypeOverride(allocation.getReplicationTypeOverride());
		}
		else {
			blendedAllocation.setReplicationTypeOverride(childAllocation.getCoalesceInvestmentReplicationTypeOverride());
		}
		return blendedAllocation;
	}


	private InvestmentReplicationHistory getChildReplicationHistory(InvestmentReplicationAllocation allocation, Date balanceDate) {
		InvestmentReplication childReplication = allocation.getChildReplication();
		ValidationUtils.assertNotNull(childReplication, "Child Replication is required to calculate Blended Replication Allocations.");
		return getInvestmentReplicationHistoryRebuildService().getOrBuildInvestmentReplicationHistory(childReplication.getId(), balanceDate);
	}


	private BigDecimal calculateBlendedAllocationWeight(InvestmentReplicationAllocation allocation, InvestmentReplicationAllocation childAllocation) {
		return MathUtils.round(MathUtils.divide(MathUtils.multiply(allocation.getAllocationWeight(), childAllocation.getAllocationWeight()), BigDecimal.valueOf(100)), 5);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationHistoryRebuildService getInvestmentReplicationHistoryRebuildService() {
		return this.investmentReplicationHistoryRebuildService;
	}


	public void setInvestmentReplicationHistoryRebuildService(InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService) {
		this.investmentReplicationHistoryRebuildService = investmentReplicationHistoryRebuildService;
	}


	public boolean isCopyCurrentSecurityFromChildAllocationHistory() {
		return this.copyCurrentSecurityFromChildAllocationHistory;
	}


	public void setCopyCurrentSecurityFromChildAllocationHistory(boolean copyCurrentSecurityFromChildAllocationHistory) {
		this.copyCurrentSecurityFromChildAllocationHistory = copyCurrentSecurityFromChildAllocationHistory;
	}
}
