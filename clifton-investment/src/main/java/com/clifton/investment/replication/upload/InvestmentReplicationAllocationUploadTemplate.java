package com.clifton.investment.replication.upload;

import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;

import java.math.BigDecimal;


/**
 * The InvestmentReplicationAllocationUploadTemplate is a simplified class that is used to easily upload new replication allocation weights, order, and/or current security calculator
 * based on existing allocations.
 * Created by Mary Anderson
 */
public class InvestmentReplicationAllocationUploadTemplate {


	private String replicationName;
	private String instrument;
	private String security;
	private boolean securityUnderlying;
	private String securityGroup;

	private Short order;
	private BigDecimal allocation;

	private String currentSecurityCalculator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUniqueKey() {
		if (!StringUtils.isEmpty(getSecurity())) {
			if (isSecurityUnderlying()) {
				return "Security (Underlying): " + getSecurity();
			}
			return "Security: " + getSecurity();
		}
		if (!StringUtils.isEmpty(getInstrument())) {
			if (!StringUtils.isEmpty(getSecurityGroup())) {
				return "Instrument: " + getInstrument() + ", Security Group: " + getSecurityGroup();
			}
			return "Instrument: " + getInstrument();
		}
		if (!StringUtils.isEmpty(getSecurityGroup())) {
			return "Security Group: " + getSecurityGroup();
		}
		return null;
	}


	public boolean isSameAllocation(InvestmentReplicationAllocation allocation) {
		// Already filtered by Replication
		return (isSecuritySame(allocation.getReplicationSecurity()) && isInstrumentSame(allocation.getReplicationInstrument()) && isSecurityGroupSame(allocation.getReplicationSecurityGroup()));
	}


	private boolean isSecuritySame(InvestmentSecurity replicationSecurity) {
		if (replicationSecurity == null) {
			return StringUtils.isEmpty(getSecurity());
		}
		if (StringUtils.isEmpty(getSecurity())) {
			return false;
		}
		return StringUtils.isEqual(getSecurity(), replicationSecurity.getLabel()) || StringUtils.isEqual(getSecurity(), replicationSecurity.getSymbol()) || StringUtils.isEqual(getSecurity(), replicationSecurity.getName());
	}


	private boolean isInstrumentSame(InvestmentInstrument replicationInstrument) {
		if (replicationInstrument == null) {
			return StringUtils.isEmpty(getInstrument());
		}
		if (StringUtils.isEmpty(getInstrument())) {
			return false;
		}
		return StringUtils.isEqual(getInstrument(), replicationInstrument.getLabel()) || StringUtils.isEqual(getInstrument(), replicationInstrument.getIdentifierPrefix()) || StringUtils.isEqual(getInstrument(), replicationInstrument.getName());
	}


	private boolean isSecurityGroupSame(InvestmentSecurityGroup replicationSecurityGroup) {
		if (replicationSecurityGroup == null) {
			return StringUtils.isEmpty(getSecurityGroup());
		}
		if (StringUtils.isEmpty(getSecurityGroup())) {
			return false;
		}
		return StringUtils.isEqual(getSecurityGroup(), replicationSecurityGroup.getName());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getReplicationName() {
		return this.replicationName;
	}


	public void setReplicationName(String replicationName) {
		this.replicationName = replicationName;
	}


	public String getInstrument() {
		return this.instrument;
	}


	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}


	public String getSecurity() {
		return this.security;
	}


	public void setSecurity(String security) {
		this.security = security;
	}


	public boolean isSecurityUnderlying() {
		return this.securityUnderlying;
	}


	public void setSecurityUnderlying(boolean securityUnderlying) {
		this.securityUnderlying = securityUnderlying;
	}


	public String getSecurityGroup() {
		return this.securityGroup;
	}


	public void setSecurityGroup(String securityGroup) {
		this.securityGroup = securityGroup;
	}


	public Short getOrder() {
		return this.order;
	}


	public void setOrder(Short order) {
		this.order = order;
	}


	public BigDecimal getAllocation() {
		return this.allocation;
	}


	public void setAllocation(BigDecimal allocation) {
		this.allocation = allocation;
	}


	public String getCurrentSecurityCalculator() {
		return this.currentSecurityCalculator;
	}


	public void setCurrentSecurityCalculator(String currentSecurityCalculator) {
		this.currentSecurityCalculator = currentSecurityCalculator;
	}
}
