package com.clifton.investment.replication;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.replication.calculators.InvestmentReplicationCalculator;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Component;


/**
 * The <code>InvestmentReplicationTypeValidator</code> ...
 *
 * @author Mary Anderson
 */
@Component
public class InvestmentReplicationTypeValidator extends SelfRegisteringDaoValidator<InvestmentReplicationType> {


	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate(InvestmentReplicationType bean, DaoEventTypes config) throws ValidationException {
		if (config.isUpdate()) {
			InvestmentReplicationType originalBean = getOriginalBean(bean);
			if (!originalBean.getName().equals(bean.getName())) {
				if (InvestmentReplicationType.DEFAULT_REPLICATION_TYPE_NAME.equals(originalBean.getName())) {
					throw new ValidationException("Replication Type name is used as a default selection and used by name. You cannot change the replication type name from [" + originalBean.getName() + "] to [" + bean.getName() + "].");
				}
			}
		}

		ValidationUtils.assertNotNull(bean.getCalculatorBean(), "Replication Calculator selection is required", "calculatorBean");
		InvestmentReplicationCalculator calculator = (InvestmentReplicationCalculator) getSystemBeanService().getBeanInstance(bean.getCalculatorBean());
		calculator.validateInvestmentReplicationType(bean);

		if (bean.isDoNotApplyCurrencyOtherAllocation()) {
			ValidationUtils.assertTrue(bean.isCurrency(), "Do not apply currency other allocation selection is only valid for currency replication types.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
