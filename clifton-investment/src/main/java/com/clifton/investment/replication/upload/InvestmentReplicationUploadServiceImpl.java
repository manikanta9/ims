package com.clifton.investment.replication.upload;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.upload.FileUploadHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.search.InvestmentReplicationSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * InvestmentReplicationUploadServiceImpl handles uploading replication allocation information for replications
 * <p>
 * Created by @Mary Anderson
 */
@Service
public class InvestmentReplicationUploadServiceImpl implements InvestmentReplicationUploadService {

	private InvestmentReplicationService investmentReplicationService;

	private SystemBeanService systemBeanService;

	private FileUploadHandler fileUploadHandler;


	////////////////////////////////////////////////////////////////////////////////
	/////////         Investment Replication Allocation Uploads          ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadInvestmentReplicationAllocationUploadFile(InvestmentReplicationAllocationUploadCommand uploadCommand) {
		uploadCommand.setUploadResultsString("");
		List<InvestmentReplicationAllocationUploadTemplate> beanList = getFileUploadHandler().convertFileUploadFileToBeanList(uploadCommand);
		ValidationUtils.assertNotEmpty(beanList, "No replication allocations found to update.");

		boolean errors = false;
		Map<String, List<InvestmentReplicationAllocationUploadTemplate>> replicationUpdateMap = BeanUtils.getBeansMap(beanList, InvestmentReplicationAllocationUploadTemplate::getReplicationName);
		ValidationUtils.assertNotEmpty(replicationUpdateMap.keySet(), "No replication allocations found to update.");
		Set<String> replicationNames = replicationUpdateMap.keySet();
		Map<String, InvestmentReplication> existingReplicationMap = getExistingReplicationsMap(replicationNames);

		List<InvestmentReplication> saveList = new ArrayList<>();
		for (Map.Entry<String, List<InvestmentReplicationAllocationUploadTemplate>> stringListEntry : replicationUpdateMap.entrySet()) {
			InvestmentReplication replication = existingReplicationMap.get(stringListEntry.getKey());
			if (replication == null) {
				uploadCommand.appendResult("Cannot find a replication with name [" + stringListEntry.getKey() + "] in the system to update.");
				errors = true;
				continue;
			}

			List<InvestmentReplicationAllocationUploadTemplate> newList = stringListEntry.getValue();
			List<String> processedAllocations = new ArrayList<>();
			List<String> allocationErrors = new ArrayList<>();

			// Pull Replication By ID so current allocations are populated
			replication = getInvestmentReplicationService().getInvestmentReplication(replication.getId());
			List<InvestmentReplicationAllocation> saveAllocationList = new ArrayList<>();
			for (InvestmentReplicationAllocationUploadTemplate newAllocation : newList) {
				InvestmentReplicationAllocation allocation = getExistingInvestmentReplicationAllocation(uploadCommand, processedAllocations, newAllocation, replication, allocationErrors);
				if (allocation != null) {
					SystemBean calculator = null;
					if ((allocation.getReplicationSecurity() == null || allocation.isSecurityUnderlying()) && !StringUtils.isEmpty(newAllocation.getCurrentSecurityCalculator())) {
						try {
							calculator = getSystemBeanService().getSystemBeanByName(newAllocation.getCurrentSecurityCalculator());
						}
						catch (ValidationException e) {
							allocationErrors.add("Cannot find current security calculator with name [" + newAllocation.getCurrentSecurityCalculator() + "] used by allocation [" + newAllocation.getUniqueKey() + "].");
						}
					}
					if (CollectionUtils.isEmpty(allocationErrors)) {
						if (newAllocation.getAllocation() != null) {
							allocation.setAllocationWeight(newAllocation.getAllocation());
						}
						if (newAllocation.getOrder() != null) {
							allocation.setOrder(newAllocation.getOrder());
						}
						if (calculator != null) {
							allocation.setCurrentSecurityCalculatorBean(calculator);
						}

						saveAllocationList.add(allocation);
					}
				}
			}
			if (CollectionUtils.isEmpty(allocationErrors)) {
				if (!replication.isPartiallyAllocated()) {
					BigDecimal total = CoreMathUtils.sumProperty(saveAllocationList, InvestmentReplicationAllocation::getAllocationWeight);
					if (!MathUtils.isEqual(total, MathUtils.BIG_DECIMAL_ONE_HUNDRED)) {
						uploadCommand.appendResult("Replication [" + stringListEntry.getKey() + "] total allocation is [" + CoreMathUtils.formatNumberDecimal(total) + "] but must total 100 %.");
						errors = true;
						continue;
					}
				}
				replication.setAllocationList(saveAllocationList);
				saveList.add(replication);
			}
			else {
				errors = true;
				uploadCommand.appendResult("Replication [" + stringListEntry.getKey() + "] Errors: " + StringUtils.collectionToCommaDelimitedString(allocationErrors));
			}
		}

		if (!errors || uploadCommand.isPartialUploadAllowed()) {
			saveReplicationList(saveList);
			uploadCommand.prependResult(CollectionUtils.getSize(saveList) + " Replications Updated.");
		}
		else {
			uploadCommand.prependResult("No Replications Updated.");
		}
	}


	@Transactional
	protected void saveReplicationList(List<InvestmentReplication> saveList) {
		for (InvestmentReplication replication : CollectionUtils.getIterable(saveList)) {
			getInvestmentReplicationService().saveInvestmentReplication(replication);
		}
	}


	private InvestmentReplicationAllocation getExistingInvestmentReplicationAllocation(InvestmentReplicationAllocationUploadCommand uploadCommand, List<String> processedAllocations, InvestmentReplicationAllocationUploadTemplate newAllocation, InvestmentReplication existingReplication, List<String> allocationErrors) {
		String key = newAllocation.getUniqueKey();
		if (key == null) {
			allocationErrors.add("Found row in the file missing a security, instrument, or security group selection.");
			return null;
		}
		else if (processedAllocations.contains(key)) {
			uploadCommand.appendResult("Found duplicate allocations for [" + key + "]");
			return null;
		}
		for (InvestmentReplicationAllocation existingAllocation : existingReplication.getAllocationList()) {
			if (newAllocation.isSameAllocation(existingAllocation)) {
				processedAllocations.add(key);
				if (uploadCommand.getMaxAllocationChange() != null && newAllocation.getAllocation() != null) {
					BigDecimal percentChange = MathUtils.getPercentChange(existingAllocation.getAllocationWeight(), newAllocation.getAllocation(), true);
					if (MathUtils.isGreaterThan(MathUtils.abs(percentChange), uploadCommand.getMaxAllocationChange())) {
						allocationErrors.add("Allocation [" + key + "]: Too high percent change (" + CoreMathUtils.formatNumberMoney(percentChange) + "%) from [" + CoreMathUtils.formatNumberDecimal(existingAllocation.getAllocationWeight()) + "] to [" + CoreMathUtils.formatNumberDecimal(newAllocation.getAllocation()) + "].");
						return null;
					}
				}
				return existingAllocation;
			}
		}
		allocationErrors.add("Cannot find existing allocation for [" + key + "].");
		return null;
	}


	private Map<String, InvestmentReplication> getExistingReplicationsMap(Set<String> replicationNames) {
		InvestmentReplicationSearchForm searchForm = new InvestmentReplicationSearchForm();
		searchForm.setNames(replicationNames.toArray(new String[0]));
		List<InvestmentReplication> repList = getInvestmentReplicationService().getInvestmentReplicationList(searchForm);
		return BeanUtils.getBeanMap(repList, InvestmentReplication::getName);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public FileUploadHandler getFileUploadHandler() {
		return this.fileUploadHandler;
	}


	public void setFileUploadHandler(FileUploadHandler fileUploadHandler) {
		this.fileUploadHandler = fileUploadHandler;
	}
}
