package com.clifton.investment.replication.calculators;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * Base implementation of {@link InvestmentReplicationSecurityAllocation}.  Contains some common fields and functionality that are applicable accross all implementations in other
 * modules.
 *
 * @author mitchellf
 */
public abstract class BaseInvestmentReplicationSecurityAllocation<T extends Serializable> extends BaseSimpleEntity<T> implements InvestmentReplicationSecurityAllocation {

	private InvestmentReplication replication;
	private InvestmentSecurity security;

	private InvestmentReplicationAllocationHistory replicationAllocationHistory;

	@NonPersistentField
	private boolean matchingAllocation;

	// Contract Value
	private BigDecimal value;

	private BigDecimal actualContracts = BigDecimal.ZERO; // existing position
	private BigDecimal virtualContracts;

	private BigDecimal securityPrice;
	private BigDecimal underlyingSecurityPrice;
	private BigDecimal securityDirtyPrice; // Used for Bond Replications to calculate contract value.

	private BigDecimal allocationWeightAdjusted;

	private BigDecimal targetExposure;
	private BigDecimal targetExposureAdjusted;

	/**
	 * Additional exposure is currently populated from a manager's security balance that is selected
	 * to be allocated to the replications. (selected via a SecuritiesBalanceReplicationAllocatorBean on the {@link InvestmentManagerAccountAssignment})
	 * This is currently used for the PPA Commodity Fund where the Commodities Asset class contains Commodity Swaps, but the account also holds DJP ETN whose market value
	 * needs to be allocated to the replications matching by the DJ-UBS structured index weights.  i.e. Natural Gas is allocated ~ 14% of DJP Market Value
	 */
	private BigDecimal additionalExposure;

	/**
	 * SIMILAR TO additionalExposure, but allows putting in a different bucket.  TEMPORARY.
	 * Currently populated from a manager's security balance that is selected
	 * to be allocated to the replications. (selected via a SecuritiesBalanceReplicationAllocatorBean on the {@link InvestmentManagerAccountAssignment})
	 * This is currently used for the PPA Commodity Fund where the Commodities Asset class contains Commodity Swaps, but the account also holds a basket swap
	 * that needs to be allocated to the replications matching by the underlying commodities in the custom basket swap. i.e. Natural Gas is allocated ~ 7% of Basket Swap
	 */
	private BigDecimal additionalOverlayExposure;

	/**
	 * Used for Currency Replications where the actual currency is the amount held in the currency, and other
	 * allocation are those held in currencies not in the replication.  They are spread out across by  weighted average.
	 * Actuals are stored in local currency and exchange rate is used to display values in the account's base currency
	 * other is stored in base amount
	 */
	private BigDecimal currencyActualLocalAmount;
	private BigDecimal currencyUnrealizedLocalAmount;
	private BigDecimal currencyOtherBaseAmount;

	/**
	 * Populated here instead of other base amount when:
	 * Underlying CCY = Base CCY of the Account
	 * And amount is for the CCY Denomination of the Currency Futures.
	 * i.e CAD Base Account with CD Future would allocate USD currency exposure to this replication
	 */
	private BigDecimal currencyDenominationBaseAmount;
	private BigDecimal currencyExchangeRate;

	private BigDecimal exchangeRate; // Exchange Rate of the Currency Denom to the Account's Base Currency ( = 1 if the same)
	private BigDecimal interestRate;
	private BigDecimal duration;// USED FOR FIXED INCOME
	private BigDecimal syntheticAdjustmentFactor; // USED FOR EQUITIES: Interest Rate and then we use it along with days to maturity to calculate Syn Adj Factor
	private BigDecimal indexRatio; // USED FOR BONDS - but by default always set to 1.  (security.instrument.hierarchy has a flag is Index Ratio Adjusted and will likely be set to something other than one in these cases only)
	private BigDecimal delta; // USED FOR OPTIONS
	private BigDecimal factor;

	////////////////////////////////////////////////////////////////////////////////

	// So we know which price to display on Trade Creation Screen
	@NonPersistentField
	private String contractValuePriceField;
	// If not stored elsewhere in the replication - i.e. Some Option Replications using Strike Price which is a custom field value
	// and doesn't have a different value for previous close or trading
	@NonPersistentField
	private BigDecimal contractValuePrice;

	@NonPersistentField
	private BigDecimal fairValueAdjustment;
	@NonPersistentField
	private BigDecimal previousFairValueAdjustment; // Previous Week Day Fair Value Adjustment value - used to give users warnings when this value changes more than 10% from previous to ensure data integrity

	@NonPersistentField
	private BigDecimal currencyCurrentLocalAmount;
	@NonPersistentField
	private BigDecimal currencyCurrentOtherBaseAmount;
	@NonPersistentField
	private BigDecimal currencyCurrentDenominationBaseAmount;
	@NonPersistentField
	private BigDecimal currencyCurrentUnrealizedLocalAmount;

	private BigDecimal tradeSecurityPrice; // NOTE: This value is stored in the database to allow for overriding price
	@NonPersistentField
	private Date tradeSecurityPriceDate;

	private BigDecimal tradeUnderlyingSecurityPrice; // NOTE: This value is stored in the database to allow for overriding price
	@NonPersistentField
	private Date tradeUnderlyingSecurityPriceDate;

	@NonPersistentField
	private BigDecimal tradeSecurityDirtyPrice;
	@NonPersistentField
	private Date tradeSecurityDirtyPriceDate;

	@NonPersistentField
	private BigDecimal tradeExchangeRate;
	@NonPersistentField
	private Date tradeExchangeRateDate;

	@NonPersistentField
	private BigDecimal tradeFairValueAdjustment; // Used when Update with live prices
	@NonPersistentField
	private Date tradeFairValueAdjustmentDate;
	@NonPersistentField
	private BigDecimal tradeCurrencyExchangeRate;
	@NonPersistentField
	private Date tradeCurrencyExchangeRateDate;

	@NonPersistentField
	private BigDecimal tradeValue; // Contract Value using trade prices/fx rates, etc.
	/**
	 * Currency used for the trade value. The default is the client account base currency.
	 * Some trade creation windows can adjust the currency to view values in.
	 */
	@NonPersistentField
	private InvestmentSecurity tradeValueCurrency;

	@NonPersistentField
	private BigDecimal tradeDelta;

	/**
	 * Set by the calculator so on Trade Creation Screen we can show one column with the price value that affects the contract value change
	 */
	@NonPersistentField
	private String priceFieldName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void copySecurityInfo(InvestmentReplicationSecurityAllocation from) {
		setSecurity(from.getSecurity());
		setSecurityPrice(from.getSecurityPrice());
		setSecurityDirtyPrice(from.getSecurityDirtyPrice());

		setUnderlyingSecurityPrice(from.getUnderlyingSecurityPrice());

		setExchangeRate(from.getExchangeRate());
		setInterestRate(from.getInterestRate());
		setDuration(from.getDuration());
		setSyntheticAdjustmentFactor(from.getSyntheticAdjustmentFactor());
		setIndexRatio(from.getIndexRatio());
		setDelta(from.getDelta());
		setFactor(from.getFactor());

		setPreviousFairValueAdjustment(from.getPreviousFairValueAdjustment());
		setFairValueAdjustment(from.getFairValueAdjustment());

		setValue(from.getValue());
	}


	public void clearNonPersistentFields() {
		this.contractValuePriceField = null;
		this.contractValuePrice = null;
		this.fairValueAdjustment = null;
		this.previousFairValueAdjustment = null;
		this.priceFieldName = null;

		this.currencyCurrentLocalAmount = null;
		this.currencyCurrentOtherBaseAmount = null;
		this.currencyCurrentDenominationBaseAmount = null;
		this.currencyCurrentUnrealizedLocalAmount = null;

		this.tradeSecurityPrice = null;
		this.tradeSecurityPriceDate = null;
		this.tradeUnderlyingSecurityPrice = null;
		this.tradeUnderlyingSecurityPriceDate = null;
		this.tradeSecurityDirtyPrice = null;
		this.tradeSecurityDirtyPriceDate = null;
		this.tradeExchangeRate = null;
		this.tradeExchangeRateDate = null;
		this.tradeFairValueAdjustment = null;
		this.tradeFairValueAdjustmentDate = null;
		this.tradeCurrencyExchangeRate = null;
		this.tradeCurrencyExchangeRateDate = null;
		this.tradeValue = null;
		this.tradeValueCurrency = null;
		this.tradeDelta = null;
	}


	@Override
	public boolean isTradePropertyValueManuallyOverridden(String valueFieldName) {
		Date currentValueDate = BeanUtils.getFieldValue(this, valueFieldName + "Date");
		BigDecimal currentValue = BeanUtils.getFieldValue(this, valueFieldName);

		// If there is a Value with No Date, then Value was manually overridden!
		return (currentValueDate == null && currentValue != null);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Short getOrder() {
		if (getReplicationAllocation() != null) {
			return getReplicationAllocation().getOrder();
		}
		return null;
	}


	@Override
	public boolean isReverseExposureSign() {
		return getReplication().isReverseExposureSign();
	}


	/**
	 * If the security is the same as the Client Account base CCY, then this is considered to be
	 * cash exposure that is handled differently (See Jira: PRODUCT-243) which disables trade entry,
	 * is excluded from billing basis and aum calculations, and will back into overlay exposure based on other allocations (if any)
	 * else based on asset class target (if the only allocation)
	 */
	@Override
	public boolean isCashExposure() {
		if (getSecurity() != null && getClientAccount() != null && getClientAccount().getBaseCurrency() != null) {
			return getClientAccount().getBaseCurrency().equals(getSecurity());
		}
		return false;
	}


	@Override
	public boolean isCurrencyReplication() {
		if (getReplicationType() != null) {
			return getReplicationType().isCurrency();
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getActualContractsAdjusted() {
		return MathUtils.add(getActualContracts(), getVirtualContracts());
	}


	@Override
	public BigDecimal getTotalAdditionalExposure() {
		return MathUtils.add(getAdditionalExposure(), getAdditionalOverlayExposure());
	}


	// Target Contracts
	@Override
	public BigDecimal getTargetContracts() {
		BigDecimal val = getValue();
		if (MathUtils.isNullOrZero(val)) {
			return BigDecimal.ZERO;
		}

		// Include Currency (will be null where it doesn't apply)
		// Target + CCY Denom - CCY Base Total
		BigDecimal target = MathUtils.subtract(MathUtils.add(getTargetExposure(), getCurrencyDenominationBaseAmount()), getCurrencyTotalBaseAmount());
		// Remove Additional Exposure
		target = MathUtils.subtract(target, getTotalAdditionalExposure());
		BigDecimal contracts = MathUtils.divide(target, val);
		if (isReverseExposureSign()) {
			contracts = MathUtils.negate(contracts);
		}
		return contracts;
	}


	@Override
	public BigDecimal getTargetContractsAdjusted() {
		BigDecimal val = getValue();
		if (MathUtils.isNullOrZero(val)) {
			return BigDecimal.ZERO;
		}
		// Include Currency (will be null where it doesn't apply)
		BigDecimal contracts = MathUtils.divide(getNetAllocation(), val);

		if (isReverseExposureSign()) {
			contracts = MathUtils.negate(contracts);
		}
		return contracts;
	}


	@Override
	public Integer getDaysToMaturity() {
		if (getSecurity() == null || getSecurity().getEndDate() == null) {
			return null;
		}
		return DateUtils.getDaysDifference(getSecurity().getEndDate(), getBalanceDate());
	}


	@Override
	public InvestmentReplicationType getReplicationType() {
		if (getReplicationAllocation() != null && getReplicationAllocation().getReplicationTypeOverride() != null) {
			return getReplicationAllocation().getReplicationTypeOverride();
		}
		if (getReplication() != null) {
			return getReplication().getType();
		}
		return null;
	}


	@Override
	public InvestmentReplicationAllocation getReplicationAllocation() {
		if (getReplicationAllocationHistory() != null) {
			return getReplicationAllocationHistory().getReplicationAllocation();
		}
		return null;
	}


	@Override
	public InvestmentSecurity getUnderlyingSecurity() {
		if (getSecurity() != null) {
			if (getReplicationType() != null && getReplicationType().isUltimateUnderlyingUsed()) {
				return InvestmentUtils.getUltimateUnderlyingSecurity(getSecurity());
			}
			return getSecurity().getUnderlyingSecurity();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getNetAllocation() {
		// Target + CCY Denom - CCY Base Total - Additional Exposure
		return MathUtils.subtract(MathUtils.subtract(MathUtils.add(getTargetExposureAdjusted(), getCurrencyDenominationBaseAmount()), getCurrencyTotalBaseAmount()), getTotalAdditionalExposure());
	}


	@Override
	public BigDecimal getCurrencyActualBaseAmount() {
		return MathUtils.multiply(getCurrencyActualLocalAmount(), getCurrencyExchangeRate());
	}


	@Override
	public BigDecimal getCurrencyUnrealizedBaseAmount() {
		return MathUtils.multiply(getCurrencyUnrealizedLocalAmount(), getCurrencyExchangeRate());
	}


	@Override
	public BigDecimal getCurrencyTotalActualBaseAmount() {
		return MathUtils.add(getCurrencyActualBaseAmount(), getCurrencyUnrealizedBaseAmount());
	}


	@Override
	public BigDecimal getCurrencyActualTradeBaseAmount() {
		return MathUtils.multiply(getCurrencyActualLocalAmount(), getTradeCurrencyExchangeRate());
	}


	@Override
	public BigDecimal getCurrencyUnrealizedTradeBaseAmount() {
		return MathUtils.multiply(getCurrencyUnrealizedLocalAmount(), getTradeCurrencyExchangeRate());
	}


	@Override
	public BigDecimal getCurrencyCurrentTradeBaseAmount() {
		return MathUtils.multiply(getCurrencyCurrentLocalAmount(), getTradeCurrencyExchangeRate());
	}


	@Override
	public BigDecimal getCurrencyCurrentUnrealizedTradeBaseAmount() {
		return MathUtils.multiply(getCurrencyCurrentUnrealizedLocalAmount(), getTradeCurrencyExchangeRate());
	}


	@Override
	public BigDecimal getCurrencyTotalBaseAmount() {
		if (!isCurrencyReplication()) {
			return null;
		}
		BigDecimal total = MathUtils.add(getCurrencyActualBaseAmount(), getCurrencyOtherBaseAmount());
		return MathUtils.add(total, getCurrencyUnrealizedBaseAmount());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public BigDecimal getTradeValue() {
		if (MathUtils.isNullOrZero(this.tradeValue)) {
			return getValue();
		}
		return this.tradeValue;
	}


	@Override
	public void setTradeValue(BigDecimal tradeValue) {
		this.tradeValue = tradeValue;
	}


	@Override
	public InvestmentSecurity getTradeValueCurrency() {
		return this.tradeValueCurrency;
	}


	@Override
	public void setTradeValueCurrency(InvestmentSecurity tradeValueCurrency) {
		this.tradeValueCurrency = tradeValueCurrency;
	}


	@Override
	public BigDecimal getTradeCurrencyExchangeRate() {
		if (MathUtils.isNullOrZero(this.tradeCurrencyExchangeRate)) {
			return getCurrencyExchangeRate();
		}
		return this.tradeCurrencyExchangeRate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentReplication getReplication() {
		return this.replication;
	}


	@Override
	public void setReplication(InvestmentReplication replication) {
		this.replication = replication;
	}


	@Override
	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	@Override
	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	@Override
	public InvestmentReplicationAllocationHistory getReplicationAllocationHistory() {
		return this.replicationAllocationHistory;
	}


	public void setReplicationAllocationHistory(InvestmentReplicationAllocationHistory replicationAllocationHistory) {
		this.replicationAllocationHistory = replicationAllocationHistory;
	}


	@Override
	public boolean isMatchingAllocation() {
		return this.matchingAllocation;
	}


	public void setMatchingAllocation(boolean matchingAllocation) {
		this.matchingAllocation = matchingAllocation;
	}


	@Override
	public BigDecimal getActualContracts() {
		return this.actualContracts;
	}


	@Override
	public void setActualContracts(BigDecimal actualContracts) {
		this.actualContracts = actualContracts;
	}


	@Override
	public BigDecimal getSecurityPrice() {
		return this.securityPrice;
	}


	@Override
	public void setSecurityPrice(BigDecimal securityPrice) {
		this.securityPrice = securityPrice;
	}


	@Override
	public BigDecimal getDuration() {
		return this.duration;
	}


	@Override
	public void setDuration(BigDecimal duration) {
		this.duration = duration;
	}


	@Override
	public BigDecimal getSyntheticAdjustmentFactor() {
		return this.syntheticAdjustmentFactor;
	}


	@Override
	public void setSyntheticAdjustmentFactor(BigDecimal syntheticAdjustmentFactor) {
		this.syntheticAdjustmentFactor = syntheticAdjustmentFactor;
	}


	public BigDecimal getAllocationWeightAdjusted() {
		return this.allocationWeightAdjusted;
	}


	public void setAllocationWeightAdjusted(BigDecimal allocationWeightAdjusted) {
		this.allocationWeightAdjusted = allocationWeightAdjusted;
	}


	@Override
	public BigDecimal getUnderlyingSecurityPrice() {
		return this.underlyingSecurityPrice;
	}


	@Override
	public void setUnderlyingSecurityPrice(BigDecimal underlyingSecurityPrice) {
		this.underlyingSecurityPrice = underlyingSecurityPrice;
	}


	@Override
	public BigDecimal getInterestRate() {
		return this.interestRate;
	}


	@Override
	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}


	@Override
	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}


	@Override
	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}


	@Override
	public BigDecimal getTargetExposure() {
		return this.targetExposure;
	}


	@Override
	public void setTargetExposure(BigDecimal targetExposure) {
		this.targetExposure = targetExposure;
	}


	@Override
	public BigDecimal getTargetExposureAdjusted() {
		return this.targetExposureAdjusted;
	}


	@Override
	public void setTargetExposureAdjusted(BigDecimal targetExposureAdjusted) {
		this.targetExposureAdjusted = targetExposureAdjusted;
	}


	@Override
	public BigDecimal getCurrencyActualLocalAmount() {
		return this.currencyActualLocalAmount;
	}


	public void setCurrencyActualLocalAmount(BigDecimal currencyActualLocalAmount) {
		this.currencyActualLocalAmount = currencyActualLocalAmount;
	}


	@Override
	public BigDecimal getCurrencyUnrealizedLocalAmount() {
		return this.currencyUnrealizedLocalAmount;
	}


	public void setCurrencyUnrealizedLocalAmount(BigDecimal currencyUnrealizedLocalAmount) {
		this.currencyUnrealizedLocalAmount = currencyUnrealizedLocalAmount;
	}


	@Override
	public BigDecimal getCurrencyOtherBaseAmount() {
		return this.currencyOtherBaseAmount;
	}


	public void setCurrencyOtherBaseAmount(BigDecimal currencyOtherBaseAmount) {
		this.currencyOtherBaseAmount = currencyOtherBaseAmount;
	}


	@Override
	public BigDecimal getCurrencyExchangeRate() {
		return this.currencyExchangeRate;
	}


	@Override
	public void setCurrencyExchangeRate(BigDecimal currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}


	@Override
	public BigDecimal getVirtualContracts() {
		return this.virtualContracts;
	}


	@Override
	public void setVirtualContracts(BigDecimal virtualContracts) {
		this.virtualContracts = virtualContracts;
	}


	@Override
	public BigDecimal getValue() {
		return this.value;
	}


	@Override
	public void setValue(BigDecimal value) {
		this.value = value;
	}


	@Override
	public BigDecimal getIndexRatio() {
		return this.indexRatio;
	}


	@Override
	public void setIndexRatio(BigDecimal indexRatio) {
		this.indexRatio = indexRatio;
	}


	@Override
	public BigDecimal getSecurityDirtyPrice() {
		return this.securityDirtyPrice;
	}


	@Override
	public void setSecurityDirtyPrice(BigDecimal securityDirtyPrice) {
		this.securityDirtyPrice = securityDirtyPrice;
	}


	@Override
	public BigDecimal getDelta() {
		return this.delta;
	}


	@Override
	public void setDelta(BigDecimal delta) {
		this.delta = delta;
	}


	@Override
	public BigDecimal getFactor() {
		return this.factor;
	}


	@Override
	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}


	@Override
	public String getContractValuePriceField() {
		return this.contractValuePriceField;
	}


	@Override
	public void setContractValuePriceField(String contractValuePriceField) {
		this.contractValuePriceField = contractValuePriceField;
	}


	@Override
	public BigDecimal getContractValuePrice() {
		return this.contractValuePrice;
	}


	@Override
	public void setContractValuePrice(BigDecimal contractValuePrice) {
		this.contractValuePrice = contractValuePrice;
	}


	@Override
	public BigDecimal getCurrencyDenominationBaseAmount() {
		return this.currencyDenominationBaseAmount;
	}


	@Override
	public void setCurrencyDenominationBaseAmount(BigDecimal currencyDenominationBaseAmount) {
		this.currencyDenominationBaseAmount = currencyDenominationBaseAmount;
	}


	@Override
	public BigDecimal getAdditionalExposure() {
		return this.additionalExposure;
	}


	@Override
	public void setAdditionalExposure(BigDecimal additionalExposure) {
		this.additionalExposure = additionalExposure;
	}


	@Override
	public BigDecimal getAdditionalOverlayExposure() {
		return this.additionalOverlayExposure;
	}


	@Override
	public void setAdditionalOverlayExposure(BigDecimal additionalOverlayExposure) {
		this.additionalOverlayExposure = additionalOverlayExposure;
	}


	@Override
	public BigDecimal getFairValueAdjustment() {
		return this.fairValueAdjustment;
	}


	@Override
	public void setFairValueAdjustment(BigDecimal fairValueAdjustment) {
		this.fairValueAdjustment = fairValueAdjustment;
	}


	@Override
	public BigDecimal getPreviousFairValueAdjustment() {
		return this.previousFairValueAdjustment;
	}


	@Override
	public void setPreviousFairValueAdjustment(BigDecimal previousFairValueAdjustment) {
		this.previousFairValueAdjustment = previousFairValueAdjustment;
	}


	@Override
	public BigDecimal getCurrencyCurrentLocalAmount() {
		return this.currencyCurrentLocalAmount;
	}


	@Override
	public void setCurrencyCurrentLocalAmount(BigDecimal currencyCurrentLocalAmount) {
		this.currencyCurrentLocalAmount = currencyCurrentLocalAmount;
	}


	@Override
	public BigDecimal getCurrencyCurrentOtherBaseAmount() {
		return this.currencyCurrentOtherBaseAmount;
	}


	@Override
	public void setCurrencyCurrentOtherBaseAmount(BigDecimal currencyCurrentOtherBaseAmount) {
		this.currencyCurrentOtherBaseAmount = currencyCurrentOtherBaseAmount;
	}


	@Override
	public BigDecimal getCurrencyCurrentDenominationBaseAmount() {
		return this.currencyCurrentDenominationBaseAmount;
	}


	@Override
	public void setCurrencyCurrentDenominationBaseAmount(BigDecimal currencyCurrentDenominationBaseAmount) {
		this.currencyCurrentDenominationBaseAmount = currencyCurrentDenominationBaseAmount;
	}


	@Override
	public BigDecimal getCurrencyCurrentUnrealizedLocalAmount() {
		return this.currencyCurrentUnrealizedLocalAmount;
	}


	@Override
	public void setCurrencyCurrentUnrealizedLocalAmount(BigDecimal currencyCurrentUnrealizedLocalAmount) {
		this.currencyCurrentUnrealizedLocalAmount = currencyCurrentUnrealizedLocalAmount;
	}


	@Override
	public BigDecimal getTradeSecurityPrice() {
		return this.tradeSecurityPrice;
	}


	@Override
	public void setTradeSecurityPrice(BigDecimal tradeSecurityPrice) {
		this.tradeSecurityPrice = tradeSecurityPrice;
	}


	@Override
	public Date getTradeSecurityPriceDate() {
		return this.tradeSecurityPriceDate;
	}


	@Override
	public void setTradeSecurityPriceDate(Date tradeSecurityPriceDate) {
		this.tradeSecurityPriceDate = tradeSecurityPriceDate;
	}


	@Override
	public BigDecimal getTradeUnderlyingSecurityPrice() {
		return this.tradeUnderlyingSecurityPrice;
	}


	@Override
	public void setTradeUnderlyingSecurityPrice(BigDecimal tradeUnderlyingSecurityPrice) {
		this.tradeUnderlyingSecurityPrice = tradeUnderlyingSecurityPrice;
	}


	@Override
	public Date getTradeUnderlyingSecurityPriceDate() {
		return this.tradeUnderlyingSecurityPriceDate;
	}


	@Override
	public void setTradeUnderlyingSecurityPriceDate(Date tradeUnderlyingSecurityPriceDate) {
		this.tradeUnderlyingSecurityPriceDate = tradeUnderlyingSecurityPriceDate;
	}


	@Override
	public BigDecimal getTradeSecurityDirtyPrice() {
		return this.tradeSecurityDirtyPrice;
	}


	@Override
	public void setTradeSecurityDirtyPrice(BigDecimal tradeSecurityDirtyPrice) {
		this.tradeSecurityDirtyPrice = tradeSecurityDirtyPrice;
	}


	@Override
	public Date getTradeSecurityDirtyPriceDate() {
		return this.tradeSecurityDirtyPriceDate;
	}


	@Override
	public void setTradeSecurityDirtyPriceDate(Date tradeSecurityDirtyPriceDate) {
		this.tradeSecurityDirtyPriceDate = tradeSecurityDirtyPriceDate;
	}


	@Override
	public BigDecimal getTradeExchangeRate() {
		return this.tradeExchangeRate;
	}


	@Override
	public void setTradeExchangeRate(BigDecimal tradeExchangeRate) {
		this.tradeExchangeRate = tradeExchangeRate;
	}


	@Override
	public Date getTradeExchangeRateDate() {
		return this.tradeExchangeRateDate;
	}


	@Override
	public void setTradeExchangeRateDate(Date tradeExchangeRateDate) {
		this.tradeExchangeRateDate = tradeExchangeRateDate;
	}


	@Override
	public BigDecimal getTradeFairValueAdjustment() {
		return this.tradeFairValueAdjustment;
	}


	@Override
	public void setTradeFairValueAdjustment(BigDecimal tradeFairValueAdjustment) {
		this.tradeFairValueAdjustment = tradeFairValueAdjustment;
	}


	@Override
	public Date getTradeFairValueAdjustmentDate() {
		return this.tradeFairValueAdjustmentDate;
	}


	@Override
	public void setTradeFairValueAdjustmentDate(Date tradeFairValueAdjustmentDate) {
		this.tradeFairValueAdjustmentDate = tradeFairValueAdjustmentDate;
	}


	@Override
	public void setTradeCurrencyExchangeRate(BigDecimal tradeCurrencyExchangeRate) {
		this.tradeCurrencyExchangeRate = tradeCurrencyExchangeRate;
	}


	@Override
	public Date getTradeCurrencyExchangeRateDate() {
		return this.tradeCurrencyExchangeRateDate;
	}


	@Override
	public void setTradeCurrencyExchangeRateDate(Date tradeCurrencyExchangeRateDate) {
		this.tradeCurrencyExchangeRateDate = tradeCurrencyExchangeRateDate;
	}


	@Override
	public BigDecimal getTradeDelta() {
		return this.tradeDelta;
	}


	@Override
	public void setTradeDelta(BigDecimal tradeDelta) {
		this.tradeDelta = tradeDelta;
	}


	public String getPriceFieldName() {
		return this.priceFieldName;
	}


	public void setPriceFieldName(String priceFieldName) {
		this.priceFieldName = priceFieldName;
	}
}
