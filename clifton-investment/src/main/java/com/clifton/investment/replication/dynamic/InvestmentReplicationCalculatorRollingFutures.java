package com.clifton.investment.replication.dynamic;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentReplicationCalculatorRollingFutures</code> is a dynamic futures replication for a future that mimics a VIX index.
 * There are always X securities, corresponding to X consecutive months, where the middle 2 are weighted at 1/(X-1) and the last 1/(X-1) rolls daily from first allocation to last allocation.
 * <p>
 * ForUsing the replication date we find the last security for the instrument that matured and the next security that will mature.
 * For 'UX' on 1/5/2012 that would be UXZ1 and UXF2.  The first security in the replication is 3 months ahead of the next maturing
 * security which is UXJ2 in this case.  So, the 4 securities that are being allocated, in order, are UXJ2, UXK2, UXM2 and UXN2.
 * <p>
 * Symbol - EndDate
 * UXN2 - 7/17/12 - Active
 * UXM2 - 6/19/12 - Active
 * UXK2 - 5/15/12 - Active
 * UXJ2 - 4/17/12 - Active
 * UXH2 - 3/20/12
 * UXG2 - 2/14/12
 * UXF2 - 1/17/12
 * Sample Date - 1/5/12
 * UXZ1 - 12/20/11
 * <p>
 * The goal is to roll the allocation of the first security (UXJ2) to last (UXN2) based on the number of business days that have passed in the term between
 * the maturing securities (UXZ1 and UXF2).  The allocation of the last security is
 * <p>
 * 1/3*(# of business days since maturity of UXZ1)/(# of business days since maturity between UXZ1 as UXF2)
 * <p>
 * Assuming 20 business days, it looks like:
 * <p>
 * Day	UXJ2	UXK2	UXM2	UXN2
 * 0	33.33%	33.33%	33.33%	0.00%
 * 1	31.67%	33.33%	33.33%	1.67%
 * 2	30.00%	33.33%	33.33%	3.33%
 * 3	28.33%	33.33%	33.33%	5.00%
 * 4	26.67%	33.33%	33.33%	6.67%
 * 5	25.00%	33.33%	33.33%	8.33%
 * 6	23.33%	33.33%	33.33%	10.00%
 * 7	21.67%	33.33%	33.33%	11.67%
 * 8	20.00%	33.33%	33.33%	13.33%
 * 9	18.33%	33.33%	33.33%	15.00%
 * 10	16.67%	33.33%	33.33%	16.67%
 * 11	15.00%	33.33%	33.33%	18.33%
 * 12	13.33%	33.33%	33.33%	20.00%
 * 13	11.67%	33.33%	33.33%	21.67%
 * 14	10.00%	33.33%	33.33%	23.33%
 * 15	8.33%	33.33%	33.33%	25.00%
 * 16	6.67%	33.33%	33.33%	26.67%
 * 17	5.00%	33.33%	33.33%	28.33%
 * 18	3.33%	33.33%	33.33%	30.00%
 * 19	1.67%	33.33%	33.33%	31.67%
 * 20	0.00%	33.33%	33.33%	33.33%
 * <p>
 * NOTE: The allocation is always calculated for the next business day so that we trade to match what we want to hold the day after the replication.
 *
 * @author mwacker
 */
public class InvestmentReplicationCalculatorRollingFutures extends BaseInvestmentReplicationDynamicCalculator {

	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarSetupService calendarSetupService;

	private InvestmentInstrumentService investmentInstrumentService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The number of months to the first contract from the current replication date.
	 */
	private int monthsToFirstContact;
	/**
	 * The number of contracts to hold at one time.
	 */
	private int numberOfContacts;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateInvestmentReplicationAllocation(InvestmentReplicationAllocation allocation, @SuppressWarnings("unused") Date date) {
		ValidationUtils.assertNull(allocation.getReplicationSecurityGroup(), "Security Group selection is not supported for Dynamic VIX Replication Calculator.");
		// get the instrument the VIX replication is being applied to
		InvestmentInstrument instrument = allocation.getReplicationInstrument() != null ? allocation.getReplicationInstrument() : allocation.getReplicationSecurity().getInstrument();
		ValidationUtils.assertNotNull(instrument,
				"Dynamic VIX Replication Calculator requires an instrument or security to get the instrument from.  Please select either an instrument or a security.");
		ValidationUtils.assertNull(allocation.getCurrentSecurityCalculatorBean(), "Current Security Calculator Bean selection is not allowed for the Dynamic VIX Replication Calculator.");
		ValidationUtils.assertFalse(allocation.isSecurityUnderlying(), "Security Is Underlying selection is not supported for the Dynamic VIX Replication Calculator.");
		ValidationUtils.assertTrue(getNumberOfContacts() > 0, "[numberOfContacts] must be greater than 0");
		ValidationUtils.assertTrue(getMonthsToFirstContact() > 0, "[monthsToFirstContact] must be greater than 0");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentReplicationAllocation> calculateImpl(InvestmentReplication bean, Date balanceDate) {
		return doCalculate(bean, balanceDate, true);
	}


	private List<InvestmentReplicationAllocation> doCalculate(InvestmentReplication bean, Date balanceDate, boolean compareToPrevious) {
		if (bean.getAllocationList() == null) {
			bean = getInvestmentReplicationService().getInvestmentReplication(bean.getId());
		}
		validateInvestmentReplication(bean);
		InvestmentReplicationAllocation firstAllocation = CollectionUtils.getOnlyElementStrict(bean.getAllocationList());
		// get the instrument the VIX replication is being applied to
		InvestmentInstrument instrument = firstAllocation.getReplicationInstrument() != null ? firstAllocation.getReplicationInstrument() : firstAllocation.getReplicationSecurity().getInstrument();
		Calendar calendar = getCalendarSetupService().getDefaultCalendar();

		List<InvestmentReplicationAllocation> allocationList = new ArrayList<>();
		Date nextReplicationDate = getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(balanceDate, calendar.getId()));

		InvestmentSecurity lastMaturingSecurity = getLastMaturingSecurity(instrument, nextReplicationDate);
		InvestmentSecurity nextMaturingSecurity = getNextMaturingSecurity(instrument, nextReplicationDate);

		int businessDaysBetween = getCalendarBusinessDayService().getBusinessDaysBetween(lastMaturingSecurity.getEndDate(), nextMaturingSecurity.getEndDate(), null, calendar.getId());
		int periodBusinessDays = getCalendarBusinessDayService().getBusinessDaysBetween(lastMaturingSecurity.getEndDate(), nextReplicationDate, null, calendar.getId());

		if (getNumberOfContacts() > 1) {
			// calculate the allocation of the last security
			BigDecimal lastAllocation = new BigDecimal(periodBusinessDays).divide(new BigDecimal(businessDaysBetween), 4, RoundingMode.HALF_UP).divide(
					new BigDecimal(getNumberOfContacts() <= 1 ? 1 : getNumberOfContacts() - 1), 4, RoundingMode.HALF_UP);

			InvestmentSecurity security = getFirstSecurity(instrument, nextReplicationDate, getMonthsToFirstContact());
			BigDecimal totalOfLastThree = BigDecimal.ZERO;
			for (int i = 0; i < getNumberOfContacts(); i++) {
				InvestmentReplicationAllocation allocation = new InvestmentReplicationAllocation();
				allocation.setReplication(bean);
				allocation.setReplicationSecurity(security);
				allocation.setOrder((short) (i + 10));

				BigDecimal weight = BigDecimal.ZERO;
				if (i == (getNumberOfContacts() - 1)) {
					// set the last weights
					weight = lastAllocation.multiply(BigDecimal.valueOf(100.00));
					totalOfLastThree = totalOfLastThree.add(weight);
				}
				else if (i != 0) {
					// set the middle 2 weights
					weight = new BigDecimal(1).divide(new BigDecimal(getNumberOfContacts() - 1), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
					totalOfLastThree = totalOfLastThree.add(weight);
				}
				allocation.setAllocationWeight(weight);

				allocationList.add(allocation);

				if (i < 3) {
					security = getNextSecurity(instrument, security.getEndDate());
				}
			}
			// set the allocation weight of the first security using the total of the last three to avoid rounding issues
			allocationList.get(0).setAllocationWeight(MathUtils.BIG_DECIMAL_ONE_HUNDRED.subtract(totalOfLastThree));
		}
		else {
			InvestmentSecurity security = getFirstSecurity(instrument, nextReplicationDate, getMonthsToFirstContact());

			InvestmentReplicationAllocation allocation = new InvestmentReplicationAllocation();
			allocation.setReplication(bean);
			allocation.setReplicationSecurity(security);
			allocation.setOrder((short) 1);

			allocation.setAllocationWeight(new BigDecimal(100));
			allocationList.add(allocation);
		}

		if (compareToPrevious) {
			InvestmentSecurity previousSecurity = getPreviousSecurity(allocationList.get(0).getReplicationSecurity());
			// If there is not a previous security, then nothing we'd roll out of so only include if the previous is still active on balance date (would have a price)
			if (previousSecurity != null && DateUtils.isDateBetween(balanceDate, previousSecurity.getStartDate(), previousSecurity.getEndDate(), false)) {
				// set a 0 allocation for any securities that have been removed
				InvestmentReplicationAllocation allocation = new InvestmentReplicationAllocation();
				allocation.setOrder((short) 0);
				allocation.setReplicationInstrument(instrument);
				allocation.setAllocationWeight(BigDecimal.ZERO);
				allocation.setReplicationSecurity(getPreviousSecurity(allocationList.get(0).getReplicationSecurity()));
				allocationList.add(allocation);
			}
		}
		return allocationList;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////                    Helper Methods                    ////////////
	////////////////////////////////////////////////////////////////////////////


	protected InvestmentSecurity getLastMaturingSecurity(InvestmentInstrument instrument, Date replicationDate) {
		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setInstrumentId(instrument.getId());
		searchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.LESS_THAN_OR_EQUALS, replicationDate));
		searchForm.setOrderBy("endDate:desc");
		searchForm.setLimit(1);

		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
		if (securityList.size() == 1) {
			return securityList.get(0);
		}
		throw new RuntimeException("Cannot find last maturing security for instrument [" + instrument.getLabel() + "] before [" + DateUtils.fromDate(replicationDate) + "].");
	}


	protected InvestmentSecurity getNextMaturingSecurity(InvestmentInstrument instrument, Date replicationDate) {
		InvestmentSecurity result = doGetSecurity(instrument, replicationDate, 1);
		if (result == null) {
			throw new RuntimeException("Cannot find next maturing security for instrument [" + instrument.getLabel() + "] after [" + DateUtils.fromDate(replicationDate) + "].");
		}
		return result;
	}


	protected InvestmentSecurity getFirstSecurity(InvestmentInstrument instrument, Date replicationDate, int numberOfMonthsToFirstContact) {
		InvestmentSecurity result = doGetSecurity(instrument, replicationDate, numberOfMonthsToFirstContact);
		if (result == null) {
			throw new RuntimeException("Cannot find first security for instrument [" + instrument.getLabel() + "] after [" + DateUtils.fromDate(replicationDate) + "].");
		}
		return result;
	}


	protected InvestmentSecurity getNextSecurity(InvestmentInstrument instrument, Date lastEndDate) {
		InvestmentSecurity result = doGetSecurity(instrument, lastEndDate, 1);
		if (result == null) {
			throw new RuntimeException("Cannot find next security for instrument [" + instrument.getLabel() + "] after [" + DateUtils.fromDate(lastEndDate) + "].");
		}
		return result;
	}


	private InvestmentSecurity doGetSecurity(InvestmentInstrument instrument, Date date, int numberOfMonthsToFirstContact) {
		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setInstrumentId(instrument.getId());
		searchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.GREATER_THAN, date));
		searchForm.setOrderBy("endDate:asc");
		searchForm.setLimit(numberOfMonthsToFirstContact);

		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
		if (securityList.size() == numberOfMonthsToFirstContact) {
			return securityList.get(numberOfMonthsToFirstContact - 1);
		}
		return null;
	}


	/**
	 * Used to mark as current for the allocation that we are rolling OUT of
	 */
	protected InvestmentSecurity getPreviousSecurity(InvestmentSecurity security) {
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setInstrumentId(security.getInstrument().getId());
		securitySearchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.LESS_THAN, security.getEndDate()));
		securitySearchForm.setOrderBy("endDate:desc");
		securitySearchForm.setLimit(1);
		return CollectionUtils.getFirstElement(getInvestmentInstrumentService().getInvestmentSecurityList(securitySearchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public int getNumberOfContacts() {
		return this.numberOfContacts;
	}


	public void setNumberOfContacts(int numberOfContacts) {
		this.numberOfContacts = numberOfContacts;
	}


	public int getMonthsToFirstContact() {
		return this.monthsToFirstContact;
	}


	public void setMonthsToFirstContact(int monthsToFirstContact) {
		this.monthsToFirstContact = monthsToFirstContact;
	}
}
