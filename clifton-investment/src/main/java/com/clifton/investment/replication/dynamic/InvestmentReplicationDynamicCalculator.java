package com.clifton.investment.replication.dynamic;


import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;

import java.util.Date;
import java.util.List;


public interface InvestmentReplicationDynamicCalculator {

	/**
	 * Returns a list of {@link InvestmentReplicationAllocation} objects
	 * in which each represents an allocation for the replication
	 */
	public List<InvestmentReplicationAllocation> calculate(InvestmentReplication replication, Date balanceDate);


	/**
	 * When selecting a dynamic calculator allocations may or may not be required and they may need
	 * specific options selected or not allow other options
	 */
	public void validateInvestmentReplication(InvestmentReplication replication);
}
