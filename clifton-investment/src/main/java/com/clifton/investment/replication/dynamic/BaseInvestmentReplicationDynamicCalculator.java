package com.clifton.investment.replication.dynamic;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.InvestmentReplicationType;

import java.util.Date;
import java.util.List;


/**
 * The <code>BaseInvestmentReplicationCalculator</code> ...
 *
 * @author manderson
 */
public abstract class BaseInvestmentReplicationDynamicCalculator implements InvestmentReplicationDynamicCalculator {

	private InvestmentReplicationService investmentReplicationService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * By default, will enforce min of one allocation selection only.
	 */
	protected Integer getMinAllocationListSize() {
		return 1;
	}


	/**
	 * By default, will enforce max of one allocation selection only.
	 */
	protected Integer getMaxAllocationListSize() {
		return 1;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public final void validateInvestmentReplication(InvestmentReplication replication) {
		validateInvestmentReplicationImpl(replication, null);
	}


	/**
	 * Supports date specific validate used prior to actually calculating the dynamic replication.
	 */
	private void validateInvestmentReplicationImpl(InvestmentReplication replication, Date date) {
		validateReplicationTypeSupported(replication);
		validateAllocationListCount(replication);
		validateAllocations(replication, date);
	}


	protected void validateReplicationTypeSupported(InvestmentReplication replication) {
		InvestmentReplicationType replicationType = replication.getType();
		ValidationUtils.assertNotNull(replicationType, "Replication Type is required for Dynamic Replication Calculators.");
		ValidationUtils.assertFalse(replicationType.isRollupReplication(), String.format("Replication Type [%s] is not supported because this Dynamic Calculator does not support Replication rollups.", replicationType.getName()));
	}


	protected void validateAllocationListCount(InvestmentReplication replication) {
		// Validate total allocation count
		List<InvestmentReplicationAllocation> allocationList = replication.getAllocationList();
		int size = CollectionUtils.getSize(allocationList);
		if (getMinAllocationListSize() != null) {
			ValidationUtils.assertTrue(MathUtils.isGreaterThanOrEqual(size, getMinAllocationListSize()), "Selected Dynamic Replication Calculator requires a minimum of [" + getMinAllocationListSize()
					+ "] allocation(s) defined.");
		}
		if (getMaxAllocationListSize() != null) {
			ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(size, getMaxAllocationListSize()), "Selected Dynamic Replication Calculator requires a maximum of [" + getMaxAllocationListSize()
					+ "] allocation(s) defined.");
		}
	}


	protected void validateAllocations(InvestmentReplication replication, Date date) {
		// Then validate each allocation
		for (InvestmentReplicationAllocation allocation : CollectionUtils.getIterable(replication.getAllocationList())) {
			validateInvestmentReplicationAllocation(allocation, date);
		}
	}


	public abstract void validateInvestmentReplicationAllocation(InvestmentReplicationAllocation allocation, Date balanceDate);


	@Override
	public final List<InvestmentReplicationAllocation> calculate(InvestmentReplication bean, Date balanceDate) {
		if (bean.getAllocationList() == null) {
			bean = getInvestmentReplicationService().getInvestmentReplication(bean.getId());
		}
		validateInvestmentReplicationImpl(bean, balanceDate);
		return calculateImpl(bean, balanceDate);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract List<InvestmentReplicationAllocation> calculateImpl(InvestmentReplication bean, Date balanceDate);


	////////////////////////////////////////////////////////////////////////////
	////////               Getter and Setter Methods                    ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}
}
