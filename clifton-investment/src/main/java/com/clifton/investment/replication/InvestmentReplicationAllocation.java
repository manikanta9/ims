package com.clifton.investment.replication;


import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityTargetAdjustmentTypes;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.system.bean.SystemBean;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentReplicationAllocation</code> class represents replication's allocation percent to {@link InvestmentInstrument} or {@link InvestmentSecurity}.
 * In general, the sum of all allocation percents for a given {@link InvestmentReplication} must be 100%. There are some exceptions to this rule (e.g. Blended Replications).
 *
 * @author vgomelsky
 */
public class InvestmentReplicationAllocation extends BaseEntity<Integer> implements LabeledObject {

	private InvestmentReplication replication;

	/**
	 * Can be used to implement replication "rollup" functionality. Current use case is Blended Replications.
	 */
	private InvestmentReplication childReplication;

	/**
	 * Type is driven by the replication, however in some cases we want to combine different types of securities into the same replication
	 * example case (Fairfax 227000: Fixed Income Replication including IRS Swaps) - idea is that they use different calculations to calculate contract value
	 */
	private InvestmentReplicationType replicationTypeOverride;

	/**
	 * At least one of the following must be selected.  Security Group and instrument can both be populated to further filter list of securities.
	 */
	private InvestmentSecurityGroup replicationSecurityGroup;
	private InvestmentInstrument replicationInstrument;
	private InvestmentSecurity replicationSecurity;

	/**
	 * Indicates that the {@link #replicationSecurity} is the Underlying Security. This is useful when the goal is to include Flex, Listed and OTC securities.
	 * For example, using security group Put Options and specifying SPX as the Underlying Security to get SPX FLEX, SPXW, etc.
	 */
	private boolean securityUnderlying;

	private Short order;

	/**
	 * For fully allocated replications, the sum of allocation weights must be 1.
	 */
	private BigDecimal allocationWeight;

	/**
	 * Applies to Fixed Income Replications ONLY
	 * <p>
	 * Optionally can override duration for fixed income securities (not required and needs a few decimal places: 5?)
	 * Or can use Benchmark Duration (Useful if replication is Fixed Income, however has a Commodity in it - set Duration equal to the Benchmark so value isn't adjusted)
	 */
	private BigDecimal durationOverride;
	private boolean useBenchmarkDuration;

	/**
	 * A replication can be marked to identify that it can be used as a matching replication by security allocations of another replication.
	 * The matching replication overlays the same notional amount that is used by the underlying security allocation.  Matching replications
	 * are usually currency replication baskets that are linked to corresponding foreign equity replication allocations.
	 * An example of matching replication is "Optimized Currency Basket" which is weighted allocation of currencies.
	 * "Optimized Basket" is often used as a secondary replication for foreign equity exposure.
	 * Each security allocation in "Optimized Basked": SPI 200, TOPIX, Euro Stoxx 50, and FT-SE 100 needs to be associated with
	 * "Optimized Currency Basket" replication in order to provide proper exchange rate exposure.
	 */
	private InvestmentReplication matchingReplication;


	/**
	 * Can be used to apply specific logic when calculating what the current security is for this allocation.
	 * Required for non-dynamic replication allocations that do not explicitly define the security to use
	 */
	private SystemBean currentSecurityCalculatorBean;

	/**
	 * Required when current security calculator bean is defined.  Determines how the portfolio adjusts the target for the current security when non-current securities are held
	 * (i.e. usually when rolling) - give non current TFA and current difference, give current difference only if suggested to open new positions
	 */
	private InvestmentCurrentSecurityTargetAdjustmentTypes currentSecurityTargetAdjustmentType;

	/**
	 * This flag determines whether or not a current security is always included during portfolio run processing, even if it results in a 0 allocation (0 target, 0 exposure). If FALSE, the current security will not be included if it results in a 0 allocation.
	 * Can be applied for Security specific replication allocations, however only really comes into play when the target is 0
	 */
	private boolean alwaysIncludeCurrentSecurity;

	/**
	 * Allowed only when ONLY security group is selected.
	 * This is useful for allocations using Security Groups to include all securities that have an open position within the portfolio.
	 * If TRUE, current security is determined by existing positions within the portfolio during portfolio run processing, overriding the initial current security determined by the replication allocation settings.
	 * If FALSE, the current security will always be the initial current security determined by the replication allocation settings.
	 */
	private boolean portfolioSelectedCurrentSecurity;

	/**
	 * All values are entered/stored as positive values.
	 * Min values are actually negative values, that we can convert when
	 * we need to use it.
	 * <p>
	 * {@link ReplicationRebalanceTriggerTypes} is stored on the replication itself.
	 * {@link ReplicationRebalanceTriggerTypes#PROPORTIONAL} means the actual percentage entered is a percentage of the target.
	 */
	private BigDecimal rebalanceAllocationMin;
	private BigDecimal rebalanceAllocationMax;

	/**
	 * Start/End Dates with time when the allocation was active
	 * Set automatically on the back end, and new replication allocation may be created for cases where an existing allocation is updated
	 * but as it was already used by a portfolio run - need to preserve that history.
	 */
	private Date startDate;
	private Date endDate;

	/**
	 * For dynamic replications only - indicates this allocation was generated
	 * by the calculator as the resulting allocations for the replication
	 * and not part of the setup
	 * <p>
	 * this is the real allocation result that is used by portfolio run replications.
	 */
	private boolean dynamicResult;

	/**
	 * An optional schedule for replication tranches. This property is defined with {@link #trancheCount}
	 * and is used with a dynamic replication calculator to populate the allocations.
	 */
	private CalendarSchedule trancheSchedule;

	/**
	 * An optional number of slices the holdings towards this allocation should be split into.
	 * An example usage is for DE where tranches are used to slice options holdings by expiration date.
	 * <p>
	 * This property is used by current security calculators to calculate security allcoations.
	 */
	private Short trancheCount;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder label = new StringBuilder();
		if (this.replication != null) {
			label.append(this.replication.getName());
			label.append(":");
		}
		label.append(getAllocationLabel());
		return label.toString();
	}


	public String getAllocationUniqueString() {
		StringBuilder sb = new StringBuilder(16);
		if (getReplication() != null) {
			sb.append(getReplication().getId());
		}
		sb.append("_");
		if (getReplicationInstrument() != null) {
			sb.append(getReplicationInstrument().getId());
		}
		sb.append("_");
		if (getReplicationSecurity() != null) {
			sb.append(getReplicationSecurity().getId());
			sb.append("_").append(isSecurityUnderlying());
		}
		sb.append("_");
		if (getReplicationSecurityGroup() != null) {
			sb.append(getReplicationSecurityGroup().getId());
		}
		sb.append("_");
		if (getCurrentSecurityCalculatorBean() != null) {
			sb.append(getCurrentSecurityCalculatorBean().getId());
		}
		return sb.toString();
	}


	public String getAllocationLabel() {
		StringBuilder label = new StringBuilder();
		if (this.replicationInstrument != null) {
			label.append("Instrument [").append(this.replicationInstrument.getLabel()).append("] ");
		}
		if (this.replicationSecurity != null) {
			label.append("Security [").append(this.replicationSecurity.getLabel()).append("]");
		}
		if (this.replicationSecurityGroup != null) {
			label.append("Security Group [").append(this.replicationSecurityGroup.getLabel()).append("]");
		}
		return label.toString();
	}


	public boolean isRebalancingUsed() {
		return getRebalanceAllocationMin() != null;
	}


	public InvestmentReplicationType getCoalesceInvestmentReplicationTypeOverride() {
		if (getReplicationTypeOverride() != null) {
			return getReplicationTypeOverride();
		}
		return getReplication().getType();
	}


	public String getCurrentSecurityTargetAdjustmentTypeLabel() {
		if (getCurrentSecurityTargetAdjustmentType() != null) {
			return getCurrentSecurityTargetAdjustmentType().getLabel();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentReplication getReplication() {
		return this.replication;
	}


	public void setReplication(InvestmentReplication replication) {
		this.replication = replication;
	}


	public InvestmentReplication getChildReplication() {
		return this.childReplication;
	}


	public void setChildReplication(InvestmentReplication childReplication) {
		this.childReplication = childReplication;
	}


	public InvestmentReplication getMatchingReplication() {
		return this.matchingReplication;
	}


	public void setMatchingReplication(InvestmentReplication matchingReplication) {
		this.matchingReplication = matchingReplication;
	}


	public InvestmentInstrument getReplicationInstrument() {
		return this.replicationInstrument;
	}


	public void setReplicationInstrument(InvestmentInstrument replicationInstrument) {
		this.replicationInstrument = replicationInstrument;
	}


	public BigDecimal getAllocationWeight() {
		return this.allocationWeight;
	}


	public void setAllocationWeight(BigDecimal allocationWeight) {
		this.allocationWeight = allocationWeight;
	}


	public InvestmentSecurity getReplicationSecurity() {
		return this.replicationSecurity;
	}


	public void setReplicationSecurity(InvestmentSecurity replicationSecurity) {
		this.replicationSecurity = replicationSecurity;
	}


	public boolean isSecurityUnderlying() {
		return this.securityUnderlying;
	}


	public void setSecurityUnderlying(boolean securityUnderlying) {
		this.securityUnderlying = securityUnderlying;
	}


	public Short getOrder() {
		return this.order;
	}


	public void setOrder(Short order) {
		this.order = order;
	}


	public BigDecimal getDurationOverride() {
		return this.durationOverride;
	}


	public void setDurationOverride(BigDecimal durationOverride) {
		this.durationOverride = durationOverride;
	}


	public boolean isUseBenchmarkDuration() {
		return this.useBenchmarkDuration;
	}


	public void setUseBenchmarkDuration(boolean useBenchmarkDuration) {
		this.useBenchmarkDuration = useBenchmarkDuration;
	}


	public InvestmentSecurityGroup getReplicationSecurityGroup() {
		return this.replicationSecurityGroup;
	}


	public void setReplicationSecurityGroup(InvestmentSecurityGroup replicationSecurityGroup) {
		this.replicationSecurityGroup = replicationSecurityGroup;
	}


	public SystemBean getCurrentSecurityCalculatorBean() {
		return this.currentSecurityCalculatorBean;
	}


	public void setCurrentSecurityCalculatorBean(SystemBean currentSecurityCalculatorBean) {
		this.currentSecurityCalculatorBean = currentSecurityCalculatorBean;
	}


	public BigDecimal getRebalanceAllocationMin() {
		return this.rebalanceAllocationMin;
	}


	public void setRebalanceAllocationMin(BigDecimal rebalanceAllocationMin) {
		this.rebalanceAllocationMin = rebalanceAllocationMin;
	}


	public BigDecimal getRebalanceAllocationMax() {
		return this.rebalanceAllocationMax;
	}


	public void setRebalanceAllocationMax(BigDecimal rebalanceAllocationMax) {
		this.rebalanceAllocationMax = rebalanceAllocationMax;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public boolean isDynamicResult() {
		return this.dynamicResult;
	}


	public void setDynamicResult(boolean dynamicResult) {
		this.dynamicResult = dynamicResult;
	}


	public InvestmentReplicationType getReplicationTypeOverride() {
		return this.replicationTypeOverride;
	}


	public void setReplicationTypeOverride(InvestmentReplicationType replicationTypeOverride) {
		this.replicationTypeOverride = replicationTypeOverride;
	}


	public InvestmentCurrentSecurityTargetAdjustmentTypes getCurrentSecurityTargetAdjustmentType() {
		return this.currentSecurityTargetAdjustmentType;
	}


	public void setCurrentSecurityTargetAdjustmentType(InvestmentCurrentSecurityTargetAdjustmentTypes currentSecurityTargetAdjustmentType) {
		this.currentSecurityTargetAdjustmentType = currentSecurityTargetAdjustmentType;
	}


	public boolean isAlwaysIncludeCurrentSecurity() {
		return this.alwaysIncludeCurrentSecurity;
	}


	public void setAlwaysIncludeCurrentSecurity(boolean alwaysIncludeCurrentSecurity) {
		this.alwaysIncludeCurrentSecurity = alwaysIncludeCurrentSecurity;
	}


	public boolean isPortfolioSelectedCurrentSecurity() {
		return this.portfolioSelectedCurrentSecurity;
	}


	public void setPortfolioSelectedCurrentSecurity(boolean portfolioSelectedCurrentSecurity) {
		this.portfolioSelectedCurrentSecurity = portfolioSelectedCurrentSecurity;
	}


	public CalendarSchedule getTrancheSchedule() {
		return this.trancheSchedule;
	}


	public void setTrancheSchedule(CalendarSchedule trancheSchedule) {
		this.trancheSchedule = trancheSchedule;
	}


	public Short getTrancheCount() {
		return this.trancheCount;
	}


	public void setTrancheCount(Short trancheCount) {
		this.trancheCount = trancheCount;
	}
}
