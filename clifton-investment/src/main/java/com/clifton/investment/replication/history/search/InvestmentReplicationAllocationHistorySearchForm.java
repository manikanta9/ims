package com.clifton.investment.replication.history.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityTargetAdjustmentTypes;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
public class InvestmentReplicationAllocationHistorySearchForm extends BaseEntitySearchForm {

	@SearchField(searchFieldPath = "replicationHistory", searchField = "replication.id")
	private Integer replicationId;

	@SearchField(searchFieldPath = "replicationHistory", searchField = "replication.id")
	private Integer[] replicationIds;

	@SearchField(searchField = "replicationHistory.id")
	private Integer replicationHistoryId;


	@SearchField(searchFieldPath = "replicationHistory", searchField = "startDate")
	private Date startDate;

	@SearchField(searchFieldPath = "replicationHistory", searchField = "endDate")
	private Date endDate;

	// Custom Search Filter
	private Date activeOnDate;

	// Custom Search Filter
	private Date activeOnStartDate;

	// Custom Search Filter
	private Date activeOnEndDate;

	@SearchField(searchFieldPath = "replicationAllocation", searchField = "allocationWeight")
	private BigDecimal allocationWeight;

	@SearchField(searchFieldPath = "replicationAllocation", searchField = "order")
	private Short allocationOrder;

	@SearchField(searchField = "replicationAllocation.replicationInstrument.identifierPrefix,replicationAllocation.replicationInstrument.name,replicationAllocation.replicationSecurity.symbol,replicationAllocation.replicationSecurity.name,replicationAllocation.replicationSecurityGroup.name", leftJoin = true)
	private String allocationLabel;

	@SearchField(searchField = "currentInvestmentSecurity.id", sortField = "currentInvestmentSecurity.symbol")
	private Integer currentInvestmentSecurityId;

	@SearchField(searchFieldPath = "currentInvestmentSecurity", searchField = "symbol,name")
	private String currentInvestmentSecurityLabel;

	@SearchField(searchFieldPath = "replicationAllocation", searchField = "currentSecurityTargetAdjustmentType")
	private InvestmentCurrentSecurityTargetAdjustmentTypes currentSecurityTargetAdjustmentType;

	@SearchField(searchFieldPath = "replicationAllocation", searchField = "alwaysIncludeCurrentSecurity")
	private Boolean alwaysIncludeCurrentSecurity;

	@SearchField(searchFieldPath = "replicationAllocation", searchField = "portfolioSelectedCurrentSecurity")
	private Boolean portfolioSelectedCurrentSecurity;


	@SearchField(searchFieldPath = "replicationHistory", searchField = "createDate", dateFieldIncludesTime = true)
	private Date createDate;

	@SearchField(searchFieldPath = "replicationHistory", searchField = "createUserId")
	private Short createUserId;

	@SearchField(searchFieldPath = "replicationHistory", searchField = "updateDate", dateFieldIncludesTime = true)
	private Date updateDate;

	@SearchField(searchFieldPath = "replicationHistory", searchField = "updateUserId")
	private Short updateUserId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getReplicationId() {
		return this.replicationId;
	}


	public void setReplicationId(Integer replicationId) {
		this.replicationId = replicationId;
	}


	public Integer[] getReplicationIds() {
		return this.replicationIds;
	}


	public void setReplicationIds(Integer[] replicationIds) {
		this.replicationIds = replicationIds;
	}


	public Integer getReplicationHistoryId() {
		return this.replicationHistoryId;
	}


	public void setReplicationHistoryId(Integer replicationHistoryId) {
		this.replicationHistoryId = replicationHistoryId;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Date getActiveOnStartDate() {
		return this.activeOnStartDate;
	}


	public void setActiveOnStartDate(Date activeOnStartDate) {
		this.activeOnStartDate = activeOnStartDate;
	}


	public Date getActiveOnEndDate() {
		return this.activeOnEndDate;
	}


	public void setActiveOnEndDate(Date activeOnEndDate) {
		this.activeOnEndDate = activeOnEndDate;
	}


	public Date getActiveOnDate() {
		return this.activeOnDate;
	}


	public void setActiveOnDate(Date activeOnDate) {
		this.activeOnDate = activeOnDate;
	}


	public String getAllocationLabel() {
		return this.allocationLabel;
	}


	public void setAllocationLabel(String allocationLabel) {
		this.allocationLabel = allocationLabel;
	}


	public Integer getCurrentInvestmentSecurityId() {
		return this.currentInvestmentSecurityId;
	}


	public void setCurrentInvestmentSecurityId(Integer currentInvestmentSecurityId) {
		this.currentInvestmentSecurityId = currentInvestmentSecurityId;
	}


	public String getCurrentInvestmentSecurityLabel() {
		return this.currentInvestmentSecurityLabel;
	}


	public void setCurrentInvestmentSecurityLabel(String currentInvestmentSecurityLabel) {
		this.currentInvestmentSecurityLabel = currentInvestmentSecurityLabel;
	}


	public BigDecimal getAllocationWeight() {
		return this.allocationWeight;
	}


	public void setAllocationWeight(BigDecimal allocationWeight) {
		this.allocationWeight = allocationWeight;
	}


	public Short getAllocationOrder() {
		return this.allocationOrder;
	}


	public void setAllocationOrder(Short allocationOrder) {
		this.allocationOrder = allocationOrder;
	}


	public InvestmentCurrentSecurityTargetAdjustmentTypes getCurrentSecurityTargetAdjustmentType() {
		return this.currentSecurityTargetAdjustmentType;
	}


	public void setCurrentSecurityTargetAdjustmentType(InvestmentCurrentSecurityTargetAdjustmentTypes currentSecurityTargetAdjustmentType) {
		this.currentSecurityTargetAdjustmentType = currentSecurityTargetAdjustmentType;
	}


	public Boolean getAlwaysIncludeCurrentSecurity() {
		return this.alwaysIncludeCurrentSecurity;
	}


	public void setAlwaysIncludeCurrentSecurity(Boolean alwaysIncludeCurrentSecurity) {
		this.alwaysIncludeCurrentSecurity = alwaysIncludeCurrentSecurity;
	}


	public Boolean getPortfolioSelectedCurrentSecurity() {
		return this.portfolioSelectedCurrentSecurity;
	}


	public void setPortfolioSelectedCurrentSecurity(Boolean portfolioSelectedCurrentSecurity) {
		this.portfolioSelectedCurrentSecurity = portfolioSelectedCurrentSecurity;
	}


	public Date getCreateDate() {
		return this.createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public Short getCreateUserId() {
		return this.createUserId;
	}


	public void setCreateUserId(Short createUserId) {
		this.createUserId = createUserId;
	}


	public Date getUpdateDate() {
		return this.updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public Short getUpdateUserId() {
		return this.updateUserId;
	}


	public void setUpdateUserId(Short updateUserId) {
		this.updateUserId = updateUserId;
	}
}
