package com.clifton.investment.replication.history;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplicationAllocation;


/**
 * The <code>InvestmentReplicationAllocationHistory</code> represents the state of a replication allocation for a given replication and balance date.
 * This specifically includes the calculated current security
 * <p>
 *
 * @author manderson
 */
public class InvestmentReplicationAllocationHistory extends BaseSimpleEntity<Integer> {

	/**
	 * The historical replication run this allocation is associated with
	 */
	private InvestmentReplicationHistory replicationHistory;

	/**
	 * The source replication allocation this result is for
	 */
	private InvestmentReplicationAllocation replicationAllocation;

	/**
	 * The current security for this allocation
	 */
	private InvestmentSecurity currentInvestmentSecurity;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationHistory getReplicationHistory() {
		return this.replicationHistory;
	}


	public void setReplicationHistory(InvestmentReplicationHistory replicationHistory) {
		this.replicationHistory = replicationHistory;
	}


	public InvestmentReplicationAllocation getReplicationAllocation() {
		return this.replicationAllocation;
	}


	public void setReplicationAllocation(InvestmentReplicationAllocation replicationAllocation) {
		this.replicationAllocation = replicationAllocation;
	}


	public InvestmentSecurity getCurrentInvestmentSecurity() {
		return this.currentInvestmentSecurity;
	}


	public void setCurrentInvestmentSecurity(InvestmentSecurity currentInvestmentSecurity) {
		this.currentInvestmentSecurity = currentInvestmentSecurity;
	}
}
