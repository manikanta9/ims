package com.clifton.investment.replication.history.rebuild;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;

import java.util.Date;


/**
 * @author manderson
 */
public class InvestmentReplicationHistoryRebuildCommand {


	private Integer replicationId;

	private Date startBalanceDate;

	private Date endBalanceDate;

	private boolean synchronous;

	private Integer runSecondsDelay; // If asynchronous can delay the rebuild for x seconds (i.e. saving allocations allows all changes to save before running rebuild)

	private Status status;

	/**
	 * If true, will rebuild existing, if different will save results
	 * If false, will only rebuild if missing on balance date otherwise will skip
	 */
	private boolean rebuildExisting;

	/**
	 * Ability, when rebuilding history, to use the allocations as they are set up NOW vs how they were set up on balance date + 1 (EOD)
	 */
	private boolean useCurrentlyActiveAllocations;

	/**
	 * For some cases (i.e. running a historical run, we may want to allow rebuilding an inactive replication
	 * If true, then rebuildExisting must be false
	 */
	private boolean allowInactive;


	/**
	 * Option to exclude dynamic replications
	 * Would only include "standard" i.e. not dynamic replications
	 * Note: Cannot set both  excludeDynamic and excludeStandard
	 */
	private boolean excludeDynamic;

	/**
	 * Options to exclude standard replications
	 * Would only include dynamic replications
	 * <p>
	 * Note: Cannot set both  excludeDynamic and excludeStandard
	 */
	private boolean excludeStandard;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getRunId() {
		String dateString = DateUtils.fromDateShort(getStartBalanceDate()) + "_" + DateUtils.fromDateShort(getEndBalanceDate());
		if (getReplicationId() != null) {
			return getReplicationId() + "_" + dateString;
		}
		return dateString;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getReplicationId() {
		return this.replicationId;
	}


	public void setReplicationId(Integer replicationId) {
		this.replicationId = replicationId;
	}


	public Date getStartBalanceDate() {
		return this.startBalanceDate;
	}


	public void setStartBalanceDate(Date startBalanceDate) {
		this.startBalanceDate = startBalanceDate;
	}


	public Date getEndBalanceDate() {
		return this.endBalanceDate;
	}


	public void setEndBalanceDate(Date endBalanceDate) {
		this.endBalanceDate = endBalanceDate;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public boolean isRebuildExisting() {
		return this.rebuildExisting;
	}


	public void setRebuildExisting(boolean rebuildExisting) {
		this.rebuildExisting = rebuildExisting;
	}


	public boolean isAllowInactive() {
		return this.allowInactive;
	}


	public void setAllowInactive(boolean allowInactive) {
		this.allowInactive = allowInactive;
	}


	public boolean isUseCurrentlyActiveAllocations() {
		return this.useCurrentlyActiveAllocations;
	}


	public void setUseCurrentlyActiveAllocations(boolean useCurrentlyActiveAllocations) {
		this.useCurrentlyActiveAllocations = useCurrentlyActiveAllocations;
	}


	public Integer getRunSecondsDelay() {
		return this.runSecondsDelay;
	}


	public void setRunSecondsDelay(Integer runSecondsDelay) {
		this.runSecondsDelay = runSecondsDelay;
	}


	public boolean isExcludeDynamic() {
		return this.excludeDynamic;
	}


	public void setExcludeDynamic(boolean excludeDynamic) {
		this.excludeDynamic = excludeDynamic;
	}


	public boolean isExcludeStandard() {
		return this.excludeStandard;
	}


	public void setExcludeStandard(boolean excludeStandard) {
		this.excludeStandard = excludeStandard;
	}
}
