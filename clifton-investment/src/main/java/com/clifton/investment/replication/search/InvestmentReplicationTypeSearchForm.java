package com.clifton.investment.replication.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>InvestmentReplicationTypeSearchForm</code> ...
 *
 * @author manderson
 */
public class InvestmentReplicationTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Integer order;

	@SearchField(searchFieldPath = "financingRateIndex", searchField = "symbol,name", sortField = "symbol")
	private String financingRateIndexName;

	@SearchField(searchFieldPath = "calculatorBean", searchField = "name")
	private String calculatorBeanName;

	@SearchField(searchField = "calculatorBean.id", sortField = "calculatorBean.name")
	private Integer calculatorBeanId;

	@SearchField(searchFieldPath = "interestRateIndexGroup", searchField = "name")
	private String interestRateIndexGroupName;

	@SearchField(searchField = "interestRateIndexGroup.id")
	private Short interestRateIndexGroupId;

	@SearchField
	private Boolean creditDurationSupported;

	@SearchField
	private Boolean durationSupported;

	@SearchField
	private Boolean syntheticAdjustmentFactorSupported;

	@SearchField
	private Boolean currency;

	@SearchField
	private Boolean doNotApplyCurrencyOtherAllocation;

	@SearchField
	private Boolean indexRatioSupported;

	@SearchField
	private Boolean doNotApplyExposureMultiplier;

	@SearchField
	private Boolean dirtyPriceSupported;

	@SearchField
	private Boolean deltaSupported;

	@SearchField
	private Boolean doNotApplyM2MAdjustment;

	@SearchField
	private Boolean ultimateUnderlyingUsed;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public String getFinancingRateIndexName() {
		return this.financingRateIndexName;
	}


	public void setFinancingRateIndexName(String financingRateIndexName) {
		this.financingRateIndexName = financingRateIndexName;
	}


	public Integer getCalculatorBeanId() {
		return this.calculatorBeanId;
	}


	public void setCalculatorBeanId(Integer calculatorBeanId) {
		this.calculatorBeanId = calculatorBeanId;
	}


	public String getCalculatorBeanName() {
		return this.calculatorBeanName;
	}


	public void setCalculatorBeanName(String calculatorBeanName) {
		this.calculatorBeanName = calculatorBeanName;
	}


	public Boolean getCreditDurationSupported() {
		return this.creditDurationSupported;
	}


	public void setCreditDurationSupported(Boolean creditDurationSupported) {
		this.creditDurationSupported = creditDurationSupported;
	}


	public Boolean getDurationSupported() {
		return this.durationSupported;
	}


	public void setDurationSupported(Boolean durationSupported) {
		this.durationSupported = durationSupported;
	}


	public Boolean getSyntheticAdjustmentFactorSupported() {
		return this.syntheticAdjustmentFactorSupported;
	}


	public void setSyntheticAdjustmentFactorSupported(Boolean syntheticAdjustmentFactorSupported) {
		this.syntheticAdjustmentFactorSupported = syntheticAdjustmentFactorSupported;
	}


	public Boolean getCurrency() {
		return this.currency;
	}


	public void setCurrency(Boolean currency) {
		this.currency = currency;
	}


	public Boolean getIndexRatioSupported() {
		return this.indexRatioSupported;
	}


	public void setIndexRatioSupported(Boolean indexRatioSupported) {
		this.indexRatioSupported = indexRatioSupported;
	}


	public Boolean getDirtyPriceSupported() {
		return this.dirtyPriceSupported;
	}


	public void setDirtyPriceSupported(Boolean dirtyPriceSupported) {
		this.dirtyPriceSupported = dirtyPriceSupported;
	}


	public Boolean getDeltaSupported() {
		return this.deltaSupported;
	}


	public void setDeltaSupported(Boolean deltaSupported) {
		this.deltaSupported = deltaSupported;
	}


	public Boolean getDoNotApplyCurrencyOtherAllocation() {
		return this.doNotApplyCurrencyOtherAllocation;
	}


	public void setDoNotApplyCurrencyOtherAllocation(Boolean doNotApplyCurrencyOtherAllocation) {
		this.doNotApplyCurrencyOtherAllocation = doNotApplyCurrencyOtherAllocation;
	}


	public Boolean getDoNotApplyExposureMultiplier() {
		return this.doNotApplyExposureMultiplier;
	}


	public void setDoNotApplyExposureMultiplier(Boolean doNotApplyExposureMultiplier) {
		this.doNotApplyExposureMultiplier = doNotApplyExposureMultiplier;
	}


	public String getInterestRateIndexGroupName() {
		return this.interestRateIndexGroupName;
	}


	public void setInterestRateIndexGroupName(String interestRateIndexGroupName) {
		this.interestRateIndexGroupName = interestRateIndexGroupName;
	}


	public Short getInterestRateIndexGroupId() {
		return this.interestRateIndexGroupId;
	}


	public void setInterestRateIndexGroupId(Short interestRateIndexGroupId) {
		this.interestRateIndexGroupId = interestRateIndexGroupId;
	}


	public Boolean getDoNotApplyM2MAdjustment() {
		return this.doNotApplyM2MAdjustment;
	}


	public void setDoNotApplyM2MAdjustment(Boolean doNotApplyM2MAdjustment) {
		this.doNotApplyM2MAdjustment = doNotApplyM2MAdjustment;
	}


	public Boolean getUltimateUnderlyingUsed() {
		return this.ultimateUnderlyingUsed;
	}


	public void setUltimateUnderlyingUsed(Boolean ultimateUnderlyingUsed) {
		this.ultimateUnderlyingUsed = ultimateUnderlyingUsed;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
