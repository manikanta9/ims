package com.clifton.investment.replication.history;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.investment.replication.history.search.InvestmentReplicationAllocationHistorySearchForm;
import com.clifton.investment.replication.history.search.InvestmentReplicationHistorySearchForm;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public interface InvestmentReplicationHistoryService {


	////////////////////////////////////////////////////////////////////////////
	////////       InvestmentReplicationHistory Business Methods        ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If true, will also populate the allocation history list
	 */
	public InvestmentReplicationHistory getInvestmentReplicationHistory(int id, boolean populateAllocations);


	/**
	 * Returns the history record for the given replication and balance date.  If multiple apply for a balance date, returns the one with the last create date
	 * If true, will also populate the allocation history list
	 */
	public InvestmentReplicationHistory getInvestmentReplicationHistoryForBalanceDate(int replicationId, Date balanceDate, boolean populateAllocations);


	public List<InvestmentReplicationHistory> getInvestmentReplicationHistoryList(InvestmentReplicationHistorySearchForm searchForm);


	/**
	 * Performs a "smart" save - if the results (including allocations) match the previous balance date record will just update the previous record's balance date
	 * Otherwise will save the full record.  Returns the applicable history record.
	 * Can only be called from the rebuild service and allocation history MUST be populated on the bean
	 */
	@DoNotAddRequestMapping
	public InvestmentReplicationHistory saveInvestmentReplicationHistory(InvestmentReplicationHistory bean, Date balanceDate);


	/**
	 * Provides the ability to fully clear replication history.  If any history for the replication is used (i.e. by portfolio runs) the FK will prevent any history from being deleted.
	 * Useful when setting up new replications and getting them as desired before actually using them.
	 */
	public void clearInvestmentReplicationHistoryList(int replicationId);


	////////////////////////////////////////////////////////////////////////////
	//////   InvestmentReplicationAllocationHistory Business Methods     ///////
	////////////////////////////////////////////////////////////////////////////


	public List<InvestmentReplicationAllocationHistory> getInvestmentReplicationAllocationHistoryList(InvestmentReplicationAllocationHistorySearchForm searchForm);
}
