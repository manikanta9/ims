package com.clifton.investment.replication;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.bean.SystemBean;

import java.util.List;


/**
 * The <code>InvestmentReplication</code> class represents a weighted allocation of securities that is trying to mimic a benchmark.
 *
 * @author vgomelsky
 */
public class InvestmentReplication extends NamedEntity<Integer> {

	private InvestmentReplicationType type;

	/**
	 * Specifies whether allocations for this replication must add up to 100%.
	 */
	private boolean partiallyAllocated;

	/**
	 * Specifies whether this replication can be used as a matching replication by other replication allocations.
	 * Matching replications replicate the same notional amount as the corresponding replication allocation.
	 */
	private boolean allowedForMatching;

	private boolean inactive;

	/**
	 * Replication - Overlay Exposure will be reversed (most cases would be negative and want to make it positive)
	 * Sample case needed for is an Equity Replication with Options.  We want the exposure to be represented as a positive value.
	 */
	private boolean reverseExposureSign;

	/**
	 * The SystemBean that implements a replication calculator.
	 * These dynamic calculators can generate allocations, and/or
	 * redistribute weights across allocations based on specific logic, i.e. based on prices, other replications, maturity date of the security, etc.
	 */
	private SystemBean dynamicCalculatorBean;

	/**
	 * When used exposes rebalance min/max fields on allocations
	 * Determines the type of trigger - proportional or fixed
	 */
	private ReplicationRebalanceTriggerTypes rebalanceTriggerType;

	private List<InvestmentReplicationAllocation> allocationList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isDynamic() {
		return getDynamicCalculatorBean() != null;
	}


	public boolean isRebalanceTriggersUsed() {
		return getRebalanceTriggerType() != null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<InvestmentReplicationAllocation> getAllocationList() {
		return this.allocationList;
	}


	public void setAllocationList(List<InvestmentReplicationAllocation> allocationList) {
		this.allocationList = allocationList;
	}


	public boolean isAllowedForMatching() {
		return this.allowedForMatching;
	}


	public void setAllowedForMatching(boolean allowedForMatching) {
		this.allowedForMatching = allowedForMatching;
	}


	public boolean isPartiallyAllocated() {
		return this.partiallyAllocated;
	}


	public void setPartiallyAllocated(boolean partiallyAllocated) {
		this.partiallyAllocated = partiallyAllocated;
	}


	public boolean isInactive() {
		return this.inactive;
	}


	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}


	public InvestmentReplicationType getType() {
		return this.type;
	}


	public void setType(InvestmentReplicationType type) {
		this.type = type;
	}


	public boolean isReverseExposureSign() {
		return this.reverseExposureSign;
	}


	public void setReverseExposureSign(boolean reverseExposureSign) {
		this.reverseExposureSign = reverseExposureSign;
	}


	public SystemBean getDynamicCalculatorBean() {
		return this.dynamicCalculatorBean;
	}


	public void setDynamicCalculatorBean(SystemBean dynamicCalculatorBean) {
		this.dynamicCalculatorBean = dynamicCalculatorBean;
	}


	public ReplicationRebalanceTriggerTypes getRebalanceTriggerType() {
		return this.rebalanceTriggerType;
	}


	public void setRebalanceTriggerType(ReplicationRebalanceTriggerTypes rebalanceTriggerType) {
		this.rebalanceTriggerType = rebalanceTriggerType;
	}
}
