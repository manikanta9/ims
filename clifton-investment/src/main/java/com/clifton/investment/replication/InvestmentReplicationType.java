package com.clifton.investment.replication;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.investment.rates.InvestmentInterestRateIndexGroup;
import com.clifton.system.bean.SystemBean;


/**
 * The <code>InvestmentReplicationType</code> ...
 *
 * @author Mary Anderson
 */
@CacheByName
public class InvestmentReplicationType extends NamedEntity<Short> {

	/**
	 * Special Default Replication Used as the "default" (Used for primary instrument uses on allocations that always use the "Replication" type for calculations)
	 */
	public static final String DEFAULT_REPLICATION_TYPE_NAME = "Replication";

	public static final String BLENDED_REPLICATION_TYPE_NAME = "Blended";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private Integer order;

	/**
	 * Financing rate for this replication type - used for performance summaries
	 */
	private InvestmentInterestRateIndex financingRateIndex;


	/**
	 * System Bean that handles the replication calculations
	 * i.e. Notional, Notional Using Underlying Price, Notional Using Strike Price, Duration Adjusted
	 * Calculates contract value as well as sets additional information needed that relates to the calculation
	 */
	private SystemBean calculatorBean;


	/**
	 * Interest Rates are grouped together into related indices
	 * Whether or not a calculation type requires an interest rate for it's calculation
	 * many times we calculate the interest rate and save/display it on the replication.
	 * This group defines which interest rate indices apply to the replication type for this calculation
	 * which is then further filtered based on the days to maturity vs. number of days for the interest rate
	 * to determine the "closest" rate to use.
	 * <p>
	 * Fixed Income: Duration
	 * Commodity: Commodity
	 * Equity: Synthetic Adjustment Factor
	 */
	private InvestmentInterestRateIndexGroup interestRateIndexGroup;

	/**
	 * Required to be true if the selected calculator requires credit duration,
	 * if not required, can optionally be selected to calculate/display duration even if
	 * not used to calculate contract value
	 */
	private boolean creditDurationSupported;

	/**
	 * Required to be true if the selected calculator requires duration,
	 * if not required, can optionally be selected to calculate/display duration even if
	 * not used to calculate contract value
	 */
	private boolean durationSupported;

	/**
	 * Required to be true if the selected calculator requires synthetic adjustment factor,
	 * if not required, can optionally be selected to calculate/display synthetic adjustment factor even if
	 * not used to calculate contract value
	 */
	private boolean syntheticAdjustmentFactorSupported;

	/**
	 * Used to flag this replication as a "Currency" replication which in PIOS
	 * has special meaning
	 */
	private boolean currency;

	/**
	 * Used for "currency = true" replication types.
	 * Can optionally be set so when loading "Currency Other" the allocation from Currency Other Physical Balance and Unrealized Currency Other is not allocated to this replication
	 */
	private boolean doNotApplyCurrencyOtherAllocation;

	/**
	 * Required to be true if the calculator uses Dirty Price (inherently used to calculate dirty price)
	 */
	private boolean indexRatioSupported;

	/**
	 * Required to be true if the calculator uses Dirty Price
	 */
	private boolean dirtyPriceSupported;

	/**
	 * Used for Options
	 */
	private boolean deltaSupported;

	/**
	 * Option to turn off applying the exposure Multiplier (currently defined for Euro Dollar and Euro Yen)
	 */
	private boolean doNotApplyExposureMultiplier;


	/**
	 * M2M Adjustment on the Trading Screen based on price change (when update with live prices)
	 * Currently doesn't apply to Options replication calc because change in price doesn't mean anything.
	 * If becomes applicable to other replications, or users want to turn on and off can add field to replication type
	 * to optionally turn it on and off.
	 */
	private boolean doNotApplyM2MAdjustment;


	/**
	 * For securities with factor change events, the current factor is not applied by default to match historic behavior.
	 * This flag allows for including the current factor in the contract value calculation. Using the factor allows for
	 * values to be presented in the replications and trade creation screen in original face rather than adjusted quantity.
	 */
	private boolean applyCurrentFactor;


	/**
	 * When not set, the replication will use the underlying security as it is, but when set, it will use the "ultimate" or "top level" underlying security.
	 * This is used to both display that security as the underlying, as well as that security's price when the contract value calculator uses the underlying security price in its calculations
	 * Example: S&P 500 Big Future Option securities have the future security as it's underlying, but using the ultimate underlying would use SPX
	 */
	private boolean ultimateUnderlyingUsed;


	/**
	 * Indicates whether or not this Replication has child Replications.
	 */
	private boolean rollupReplication;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isInterestRateSupported() {
		return getInterestRateIndexGroup() != null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public InvestmentInterestRateIndex getFinancingRateIndex() {
		return this.financingRateIndex;
	}


	public void setFinancingRateIndex(InvestmentInterestRateIndex financingRateIndex) {
		this.financingRateIndex = financingRateIndex;
	}


	public InvestmentInterestRateIndexGroup getInterestRateIndexGroup() {
		return this.interestRateIndexGroup;
	}


	public void setInterestRateIndexGroup(InvestmentInterestRateIndexGroup interestRateIndexGroup) {
		this.interestRateIndexGroup = interestRateIndexGroup;
	}


	public boolean isCreditDurationSupported() {
		return this.creditDurationSupported;
	}


	public void setCreditDurationSupported(boolean creditDurationSupported) {
		this.creditDurationSupported = creditDurationSupported;
	}


	public boolean isDurationSupported() {
		return this.durationSupported;
	}


	public void setDurationSupported(boolean durationSupported) {
		this.durationSupported = durationSupported;
	}


	public boolean isSyntheticAdjustmentFactorSupported() {
		return this.syntheticAdjustmentFactorSupported;
	}


	public void setSyntheticAdjustmentFactorSupported(boolean syntheticAdjustmentFactorSupported) {
		this.syntheticAdjustmentFactorSupported = syntheticAdjustmentFactorSupported;
	}


	public boolean isCurrency() {
		return this.currency;
	}


	public void setCurrency(boolean currency) {
		this.currency = currency;
	}


	public boolean isIndexRatioSupported() {
		return this.indexRatioSupported;
	}


	public void setIndexRatioSupported(boolean indexRatioSupported) {
		this.indexRatioSupported = indexRatioSupported;
	}


	public boolean isDirtyPriceSupported() {
		return this.dirtyPriceSupported;
	}


	public void setDirtyPriceSupported(boolean dirtyPriceSupported) {
		this.dirtyPriceSupported = dirtyPriceSupported;
	}


	public boolean isDeltaSupported() {
		return this.deltaSupported;
	}


	public void setDeltaSupported(boolean deltaSupported) {
		this.deltaSupported = deltaSupported;
	}


	public boolean isDoNotApplyCurrencyOtherAllocation() {
		return this.doNotApplyCurrencyOtherAllocation;
	}


	public void setDoNotApplyCurrencyOtherAllocation(boolean doNotApplyCurrencyOtherAllocation) {
		this.doNotApplyCurrencyOtherAllocation = doNotApplyCurrencyOtherAllocation;
	}


	public boolean isDoNotApplyExposureMultiplier() {
		return this.doNotApplyExposureMultiplier;
	}


	public void setDoNotApplyExposureMultiplier(boolean doNotApplyExposureMultiplier) {
		this.doNotApplyExposureMultiplier = doNotApplyExposureMultiplier;
	}


	public boolean isDoNotApplyM2MAdjustment() {
		return this.doNotApplyM2MAdjustment;
	}


	public void setDoNotApplyM2MAdjustment(boolean doNotApplyM2MAdjustment) {
		this.doNotApplyM2MAdjustment = doNotApplyM2MAdjustment;
	}


	public boolean isApplyCurrentFactor() {
		return this.applyCurrentFactor;
	}


	public void setApplyCurrentFactor(boolean applyCurrentFactor) {
		this.applyCurrentFactor = applyCurrentFactor;
	}


	public SystemBean getCalculatorBean() {
		return this.calculatorBean;
	}


	public void setCalculatorBean(SystemBean calculatorBean) {
		this.calculatorBean = calculatorBean;
	}


	public boolean isUltimateUnderlyingUsed() {
		return this.ultimateUnderlyingUsed;
	}


	public void setUltimateUnderlyingUsed(boolean ultimateUnderlyingUsed) {
		this.ultimateUnderlyingUsed = ultimateUnderlyingUsed;
	}


	public boolean isRollupReplication() {
		return this.rollupReplication;
	}


	public void setRollupReplication(boolean rollupReplication) {
		this.rollupReplication = rollupReplication;
	}
}
