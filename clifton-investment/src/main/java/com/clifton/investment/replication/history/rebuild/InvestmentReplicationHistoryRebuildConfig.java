package com.clifton.investment.replication.history.rebuild;

import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityCalculator;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>InvestmentReplicationHistoryRebuildConfig</code> class holds common properties used during replication history rebuilds
 * Which makes processing faster when running across multiple replications
 *
 * @author manderson
 */
public class InvestmentReplicationHistoryRebuildConfig {


	private final InvestmentReplicationHistoryRebuildCommand rebuildCommand;

	private final Map<Integer, InvestmentCurrentSecurityCalculator> currentSecurityCalculatorBeanMap;


	/**
	 * Date specific properties that are cleared for each date processed
	 */
	private Date balanceDate;

	/**
	 * Balance Date + 1 Weekday (End of Day)
	 */
	private Date replicationAllocationActiveOnDate;

	/**
	 * Balance Date + 1 Weekday (No Time)
	 */
	private Date currentSecurityCalculatorDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationHistoryRebuildConfig(InvestmentReplicationHistoryRebuildCommand rebuildCommand) {
		this.rebuildCommand = rebuildCommand;
		this.currentSecurityCalculatorBeanMap = new HashMap<>();
	}


	public void resetForBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
		this.replicationAllocationActiveOnDate = (DateUtils.getEndOfDay(getRebuildCommand().isUseCurrentlyActiveAllocations() ? new Date() : DateUtils.addWeekDays(balanceDate, 1)));
		this.currentSecurityCalculatorDate = DateUtils.addWeekDays(balanceDate, 1);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCurrentSecurityCalculator getInvestmentCurrentSecurityCalculator(SystemBean currentSecurityCalculatorBean, SystemBeanService systemBeanService) {
		return this.currentSecurityCalculatorBeanMap.computeIfAbsent(currentSecurityCalculatorBean.getId(), key -> (InvestmentCurrentSecurityCalculator) systemBeanService.getBeanInstance(currentSecurityCalculatorBean));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationHistoryRebuildCommand getRebuildCommand() {
		return this.rebuildCommand;
	}


	public Date getReplicationAllocationActiveOnDate() {
		return this.replicationAllocationActiveOnDate;
	}


	public Map<Integer, InvestmentCurrentSecurityCalculator> getCurrentSecurityCalculatorBeanMap() {
		return this.currentSecurityCalculatorBeanMap;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public Date getCurrentSecurityCalculatorDate() {
		return this.currentSecurityCalculatorDate;
	}
}
