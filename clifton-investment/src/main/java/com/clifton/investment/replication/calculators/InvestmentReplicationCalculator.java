package com.clifton.investment.replication.calculators;

import com.clifton.investment.replication.InvestmentReplicationType;


/**
 * InvestmentReplicationCalculator is used to calculate each replication for a security
 * Calculates the contract value, and sets necessary data (i.e. security prices, duration, delta)
 * <p>
 * Note: This is a basic interface, the actual calculating interface is in product project and is the interface the group name uses that extends this one
 *
 * @author manderson
 */
public interface InvestmentReplicationCalculator {


	/**
	 * There are options available on the replication type that are driven by the calculation.
	 * i.e. If Duration is required to calculate the contract value, then the replication type must support duration
	 */
	public void validateInvestmentReplicationType(InvestmentReplicationType replicationType);


	/**
	 * securityPrice, underlyingSecurityPrice, exchangeRate, duration, etc.
	 * <p>
	 * Calculates/sets all security related information (i.e. security prices, exchange rates, interest rates, etc.)
	 * exchangeRateDataSourceName - This is the data source for the account to pull exchange rates from (if not live);
	 * Will also calculate and set the contractValue "value" on the replication
	 */
	public void calculate(InvestmentReplicationSecurityAllocation config, String exchangeRateDataSourceName, boolean excludeMispricing);


	/**
	 * Recalculates/Populates Values that are dependent on Target Changes.
	 * This is currently used only for those that use Delta values where the delta sign is dependent on the Put/Call Long/Short targets.  Since the delta value
	 * doesn't affect the contract value, this does not recalculate contract value at this time.
	 */
	public void calculateForTargetUpdates(InvestmentReplicationSecurityAllocation config);


	/**
	 * Recalculates Contract Value ONLY for "tradeValue", and sets any trade related fields needed for display,
	 * i.e. price field name so the screen knows what to display, original price and current price
	 * <p>
	 * Does not update any security related information that would be saved on the database, uses what is there or looks up live prices from bloomberg.
	 * <p>
	 * The {@link InvestmentReplicationCurrencyTypes} will define the currency to use for the trading values.
	 * <p>
	 * Even if livePrices is false to clear those vales, will recalc trade value if there are any manual price overrides so the updated value is displayed on the trading screen
	 */
	public void calculateForTrading(InvestmentReplicationSecurityAllocation config, InvestmentReplicationCurrencyTypes currencyType, boolean livePrices, boolean excludeMispricing);


	/**
	 * Calculates and Returns the ContractValue using the information already on the replication - doesn't perform any additional look ups in the database
	 * Allows for overriding the exposure multiplier option
	 * <p>
	 * Used for Billing - when billing on a security with an exposure multiplier we need to be able to recalculate the contract value ensuring
	 * that we don't use the exposure multiplier as we never bill on the contract value adjusted for it.
	 */
	public void recalculateContractValue(InvestmentReplicationSecurityAllocation config, boolean doNotUseExposureMultiplier);


	/**
	 * There are other places for runs that need to know if a duration adjustment is applied - i.e. duration is required
	 */
	public boolean isDurationRequired(InvestmentReplicationSecurityAllocation config);
}
