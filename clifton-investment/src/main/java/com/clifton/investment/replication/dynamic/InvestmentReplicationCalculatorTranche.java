package com.clifton.investment.replication.dynamic;

import com.clifton.calendar.calculator.CalendarCalculationResult;
import com.clifton.calendar.calculator.CalendarListCalculator;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.schedule.api.Schedule;
import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.copy.InvestmentInstrumentCopyService;
import com.clifton.investment.instrument.copy.InvestmentSecurityCopyCommand;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * <code>InvestmentReplicationCalculatorTranche</code> is a dynamic replication calculator for calculating replication allocation
 * tranches that follow a schedule, such as securities that mature on the last business day of the month. This calculator
 * is similar to {@link com.clifton.investment.instrument.calculator.current.InvestmentScheduleBasedCurrentSecurityCalculator}
 * but calculates each allocation and current security within a replication.
 * <p>
 * The replication can define the dynamic tranche calculator with the tranche schedule to use, and allocations with corresponding weights.
 * The calculator will then calculate the current security for each tranche and create an allocation for the security with the appropriate
 * tranche weight (allocation weight / tranche count).
 * <p>
 * Example: Currency hedging trades forwards for many currencies that mature on the last business day of the month for the next three months.
 * The dynamic tranche replication calculator can define the tranche schedule for the last business day of the month, tranche count of three,
 * and other necessary details. Then the replication allocations can be created for the currency pair instrument with respected weights.
 * AUD/GBP: -3.26064%
 * CAD/GBP: -2.75055%
 * CHF/GBP: -1.31025%
 * EUR/GBP: -11.2135%
 * HKD/GBP: -5.67114%
 * JPY/GBP: -10.6109%
 * SEK/GBP: -2.20044%
 * SGD/GBP: -3.30066%
 * USD/GBP: -54.7916%
 * <p>
 * When the replication is processed, the current security for each tranche occurrences will be found and an allocation will be created for it.
 * AUD/GBP20201230 (AUD/GBP Curr Fwd 12/30/2020)	-1.0869%
 * AUD/GBP20210129 (AUD/GBP Curr Fwd 01/29/2021)	-1.0869%
 * AUD/GBP20210226 (AUD/GBP Curr Fwd 02/26/2021)	-1.0869%
 * CAD/GBP20201230 (CAD/GBP Curr Fwd 12/30/2020)	-0.9169%
 * CAD/GBP20210129 (CAD/GBP Curr Fwd 01/29/2021)	-0.9169%
 * CAD/GBP20210226 (CAD/GBP Curr Fwd 02/26/2021)	-0.9169%
 * CHF/GBP20201230 (CHF/GBP Curr Fwd 12/30/2020)	-0.4368%
 * CHF/GBP20210129 (CHF/GBP Curr Fwd 01/29/2021)	-0.4368%
 * CHF/GBP20210226 (CHF/GBP Curr Fwd 02/26/2021)	-0.4368%
 * EUR/GBP20201230 (EUR/GBP Curr Fwd 12/30/2020)	-3.7378%
 * EUR/GBP20210129 (EUR/GBP Curr Fwd 01/29/2021)	-3.7378%
 * EUR/GBP20210226 (EUR/GBP Curr Fwd 02/26/2021)	-3.7378%
 * HKD/GBP20201230 (HKD/GBP Curr Fwd 12/30/2020)	-1.8904%
 * HKD/GBP20210129 (HKD/GBP Curr Fwd 01/29/2021)	-1.8904%
 * HKD/GBP20210226 (HKD/GBP Curr Fwd 02/26/2021)	-1.8904%
 * JPY/GBP20201230 (JPY/GBP Curr Fwd 12/30/2020)	-3.5370%
 * JPY/GBP20210129 (JPY/GBP Curr Fwd 01/29/2021)	-3.5370%
 * JPY/GBP20210226 (JPY/GBP Curr Fwd 02/26/2021)	-3.5370%
 * SEK/GBP20201230 (SEK/GBP Curr Fwd 12/30/2020)	-0.7335%
 * SEK/GBP20210129 (SEK/GBP Curr Fwd 01/29/2021)	-0.7335%
 * SEK/GBP20210226 (SEK/GBP Curr Fwd 02/26/2021)	-0.7335%
 * SGD/GBP20201230 (SGD/GBP Curr Fwd 12/30/2020)	-1.1002%
 * SGD/GBP20210129 (SGD/GBP Curr Fwd 01/29/2021)	-1.1002%
 * SGD/GBP20210226 (SGD/GBP Curr Fwd 02/26/2021)	-1.1002%
 * USD/GBP20201230 (USD/GBP Curr Fwd 12/30/2020)	-18.2639%
 * USD/GBP20210129 (USD/GBP Curr Fwd 01/29/2021)	-18.2639%
 * USD/GBP20210226 (USD/GBP Curr Fwd 02/26/2021)	-18.2639%
 *
 * @author nickk
 */
public class InvestmentReplicationCalculatorTranche extends BaseInvestmentReplicationDynamicCalculator {

	private InvestmentInstrumentService investmentInstrumentService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private ScheduleApiService scheduleApiService;
	private SystemBeanService systemBeanService;
	private InvestmentInstrumentCopyService investmentInstrumentCopyService;

	/**
	 * The tranche schedule to use in calculating replication allocations. Useful when there are multiple allocations for same schedule.
	 * An allocation's tranche schedule will override this value.
	 */
	private Integer trancheScheduleId;

	/**
	 * The tranche count to use in calculating replication allocation. Useful when there are multiple allocations for the same tranche count.
	 * An allocation's tranche count will override this value.
	 */
	private Short trancheCount;

	/**
	 * An optional calendar list calculator bean to override the schedule occurrence calculation in the tranche schedule.
	 */
	private Integer trancheScheduleCalendarListBeanId;

	/**
	 * This property dictates how long the current security remains current. It defaults to Current Security Schedule and can be adjusted further by Effective on BusinessDay
	 * Adjustment, Business Day Calendar, and Use Calendar Days.
	 */
	private Integer effectiveOnScheduleId;

	/**
	 * Changes when the new current security becomes effective by adjusting business days following the Business Day Calendar.
	 */
	private Integer effectiveBusinessDayAdjustment;

	/**
	 * Holiday calendar to be used with Effective on Business Day Adjustment to support Trading adjusting backwards for country holidays when rolling forwards.
	 */
	private Short businessDayCalendarId;


	/**
	 * Date Result for the Current Security Schedule will be End Date unless this property is enabled.
	 */
	private boolean dateResultIsSecurityLastDeliveryDate;
	/**
	 * Flag to include the first passed occurrence tranche allocation. This can be useful to view the security allocation when rolling.
	 * The passed occurrence will have an allocation weight of 0%.
	 */
	private boolean includePreviousTranche;

	private boolean createSecurityIfMissing;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected Integer getMaxAllocationListSize() {
		// return null to allow more than one allocation
		return null;
	}


	@Override
	public void validateInvestmentReplicationAllocation(InvestmentReplicationAllocation allocation, Date balanceDate) {
		Short allocationTrancheCount = ObjectUtils.coalesce(allocation.getTrancheCount(), getTrancheCount());
		ValidationUtils.assertNotNull(allocationTrancheCount, "Tranche Count is required for Dynamic Tranche Replication Calculator");
		Schedule trancheSchedule = getTrancheScheduleId(allocation);
		ValidationUtils.assertNotNull(trancheSchedule, "Tranche Schedule is required on this Dynamic Tranche Replication Calculator or the allocation [" + allocation + "]");
		ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(trancheSchedule.getEndDate(), balanceDate), "Tranche Schedule [" + trancheSchedule + "] is not active on " + DateUtils.fromDateShort(balanceDate));
	}


	@Override
	public List<InvestmentReplicationAllocation> calculateImpl(InvestmentReplication replication, Date balanceDate) {
		List<InvestmentReplicationAllocation> resultAllocationList = new ArrayList<>();
		List<Short> calendarIdOverrideList = getCalendarOverrideList(balanceDate);
		short allocationNumber = 1;
		for (InvestmentReplicationAllocation allocation : CollectionUtils.getIterable(replication.getAllocationList())) {
			Short allocationTrancheCount = ObjectUtils.coalesce(allocation.getTrancheCount(), getTrancheCount());
			List<TrancheDate> currentSecurityDateList = calculateCurrentSecurityDateList(allocation, allocationTrancheCount, balanceDate, calendarIdOverrideList);
			BigDecimal weight = MathUtils.round(MathUtils.divide(allocation.getAllocationWeight(), allocationTrancheCount), DataTypes.PERCENT.getPrecision(), RoundingMode.DOWN);
			int i = 1;
			int allocationBase = (allocation.getOrder() == null ? allocationNumber : allocation.getOrder()) * 10;
			for (TrancheDate date : currentSecurityDateList) {
				if (date != null) {
					Date effectiveDate = calculateFirstEffectiveOnDate(date.getDate());
					InvestmentSecurity allocationSecurity = getAllocationSecurity(allocation, date.getDate());
					if (!date.isPrevious() || allocationSecurity.isActiveOn(balanceDate)) {
						InvestmentReplicationAllocation dateBasedAllocation = new InvestmentReplicationAllocation();
						dateBasedAllocation.setReplication(replication);
						dateBasedAllocation.setReplicationSecurity(allocationSecurity);
						dateBasedAllocation.setAllocationWeight(DateUtils.isDateBefore(effectiveDate, balanceDate, false) ? BigDecimal.ZERO : weight);
						dateBasedAllocation.setOrder((short) (allocationBase + i));
						resultAllocationList.add(dateBasedAllocation);
					}
					i++;
				}
			}
			allocationNumber++;
		}
		return resultAllocationList;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private Schedule getTrancheScheduleId(InvestmentReplicationAllocation allocation) {
		if (allocation.getTrancheSchedule() != null) {
			return allocation.getTrancheSchedule().toSchedule();
		}
		return getScheduleApiService().getSchedule(getTrancheScheduleId());
	}


	private List<Short> getCalendarOverrideList(Date balanceDate) {
		CalendarListCalculator calendarListCalculator = getCalendarListBean();
		if (calendarListCalculator == null) {
			return null;
		}
		CalendarCalculationResult result = calendarListCalculator.calculate(balanceDate);
		return CollectionUtils.getConverted(result.getCalendarList(), BaseSimpleEntity::getId);
	}


	private CalendarListCalculator getCalendarListBean() {
		if (getTrancheScheduleCalendarListBeanId() == null) {
			return null;
		}
		SystemBean systemBean = getSystemBeanService().getSystemBean(getTrancheScheduleCalendarListBeanId());
		ValidationUtils.assertNotNull(systemBean, "Unable to retrieved Tranche Schedule Calendar List Bean with ID: " + getTrancheScheduleCalendarListBeanId());
		return (CalendarListCalculator) getSystemBeanService().getBeanInstance(systemBean);
	}


	private List<TrancheDate> calculateCurrentSecurityDateList(InvestmentReplicationAllocation allocation, Short trancheCount, Date balanceDate, List<Short> calendarIdOverrideList) {
		Schedule trancheSchedule = getTrancheScheduleId(allocation);
		List<TrancheDate> futureOccurrenceDates = getFutureOccurrenceDates(allocation, trancheSchedule, trancheCount, balanceDate, calendarIdOverrideList);
		TrancheDate firstOccurrence = CollectionUtils.getFirstElement(futureOccurrenceDates);
		Date firstEffectiveOnDate = firstOccurrence == null ? null : calculateFirstEffectiveOnDate(firstOccurrence.getDate());
		boolean firstOccurrenceHasPassed = DateUtils.isDateBefore(firstEffectiveOnDate, balanceDate, false);

		if (firstOccurrenceHasPassed) {
			if (isIncludePreviousTranche()) {
				return futureOccurrenceDates;
			}
			return futureOccurrenceDates.subList(1, futureOccurrenceDates.size());
		}

		if (isIncludePreviousTranche()) {
			Date firstPassedOccurrence = getPreviousOccurrenceDate(trancheSchedule, balanceDate, calendarIdOverrideList);
			if (firstPassedOccurrence != null) {
				futureOccurrenceDates.add(0, new TrancheDate(firstPassedOccurrence, true));
			}
		}
		return futureOccurrenceDates.subList(0, futureOccurrenceDates.size() - 1);
	}


	private List<TrancheDate> getFutureOccurrenceDates(InvestmentReplicationAllocation allocation, Schedule trancheSchedule, Short trancheCount, Date balanceDate, List<Short> calendarIdOverrideList) {
		int futureOccurrences = trancheCount + 1;
		ScheduleOccurrenceCommand command = ScheduleOccurrenceCommand.forOccurrences(trancheSchedule, futureOccurrences, balanceDate)
				.withOverrideCalendarIdList(calendarIdOverrideList);
		List<Date> occurrences = getScheduleApiService().getScheduleOccurrences(command);
		if (CollectionUtils.isEmpty(occurrences) || occurrences.size() < futureOccurrences) {
			throw new ValidationException(String.format("Expected at least %d future occurrence for allocation [%s] on or after [%s] but only found %d.", futureOccurrences, allocation, DateUtils.fromDate(balanceDate), occurrences.size()));
		}
		return CollectionUtils.getConverted(occurrences, date -> new TrancheDate(date, false));
	}


	private Date getPreviousOccurrenceDate(Schedule trancheSchedule, Date balanceDate, List<Short> calendarIdOverrideList) {
		ScheduleOccurrenceCommand command = ScheduleOccurrenceCommand.forOccurrences(trancheSchedule, -1, balanceDate)
				.withOverrideCalendarIdList(calendarIdOverrideList);
		return getScheduleApiService().getScheduleOccurrence(command);
	}


	private Date calculateFirstEffectiveOnDate(Date firstFutureOccurrenceDate) {
		Date effectiveOnDate = firstFutureOccurrenceDate;
		if (getEffectiveOnScheduleId() != null) {
			ScheduleOccurrenceCommand previousEffectiveDateCommand = ScheduleOccurrenceCommand.forOccurrences(getEffectiveOnScheduleId(), -1, firstFutureOccurrenceDate);
			Date calculatedEffectiveOnDate = getScheduleApiService().getScheduleOccurrence(previousEffectiveDateCommand);
			effectiveOnDate = ObjectUtils.coalesce(calculatedEffectiveOnDate, effectiveOnDate);
		}
		return adjustEffectiveOnDate(effectiveOnDate);
	}


	private Date adjustEffectiveOnDate(Date effectiveOnDate) {
		if (getEffectiveBusinessDayAdjustment() != null) {
			if (getBusinessDayCalendarId() != null) {
				CalendarBusinessDayCommand businessDayCommand = CalendarBusinessDayCommand.forDate(effectiveOnDate, getBusinessDayCalendarId());
				effectiveOnDate = getCalendarBusinessDayService().getBusinessDayFrom(businessDayCommand, getEffectiveBusinessDayAdjustment());
			}
			else {
				effectiveOnDate = DateUtils.addWeekDays(effectiveOnDate, getEffectiveBusinessDayAdjustment());
			}
		}
		return DateUtils.clearTime(effectiveOnDate);
	}


	private InvestmentSecurity getAllocationSecurity(InvestmentReplicationAllocation allocation, Date securityOccurrenceEndDate) {

		List<InvestmentSecurity> securityList = lookupExistingSecurityList(allocation, securityOccurrenceEndDate);

		if (!CollectionUtils.isEmpty(securityList)) {
			ValidationUtils.assertEquals(CollectionUtils.getSize(securityList), 1,
					String.format("Expected exactly 1 current security using Schedule Based Current Security Calculator but found %d for %s.",
							CollectionUtils.getSize(securityList), getUserFriendlyAllocationSecurityMessage(allocation, securityOccurrenceEndDate)));

			return CollectionUtils.getFirstElementStrict(securityList);
		}

		// if no security is found, create one using most recent security for the provided instrument as a template
		if (isCreateSecurityIfMissing()) {
			InvestmentSecurityCopyCommand copyCommand = new InvestmentSecurityCopyCommand();
			copyCommand.setInstrument(allocation.getReplicationInstrument());
			copyCommand.setStartDate(allocation.getStartDate());
			copyCommand.setSettlementDate(securityOccurrenceEndDate);

			return getInvestmentInstrumentCopyService().saveInvestmentSecurityForwardWithCommand(copyCommand);
		}

		throw new ValidationException(String.format("Expected exactly 1 current security using Schedule Based Current Security Calculator but found %d for %s.",
				CollectionUtils.getSize(securityList), getUserFriendlyAllocationSecurityMessage(allocation, securityOccurrenceEndDate)));
	}


	private List<InvestmentSecurity> lookupExistingSecurityList(InvestmentReplicationAllocation allocation, Date securityOccurrenceEndDate) {
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		if (isDateResultIsSecurityLastDeliveryDate()) {
			securitySearchForm.setLastDeliveryDate(securityOccurrenceEndDate);
		}
		else {
			securitySearchForm.setEndDate(securityOccurrenceEndDate);
		}
		securitySearchForm.setSecurityGroupId(allocation.getReplicationSecurityGroup() != null ? allocation.getReplicationSecurityGroup().getId() : null);
		if (allocation.getReplicationInstrument() != null) {
			securitySearchForm.setInstrumentId(allocation.getReplicationInstrument().getId());
		}
		else if (allocation.getReplicationSecurity() != null) {
			if (allocation.isSecurityUnderlying()) {
				securitySearchForm.setUnderlyingSecurityId(allocation.getReplicationSecurity().getId());
			}
			securitySearchForm.setInstrumentId(allocation.getReplicationSecurity().getInstrument().getId());
		}
		return getInvestmentInstrumentService().getInvestmentSecurityList(securitySearchForm);
	}


	private String getUserFriendlyAllocationSecurityMessage(InvestmentReplicationAllocation allocation, Date currentSecurityDateResult) {
		return new StringBuilder(allocation.toString())
				.append(isDateResultIsSecurityLastDeliveryDate() ? " with Last Delivery Date " : " with End Date ")
				.append(DateUtils.fromDateShort(currentSecurityDateResult))
				.toString();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	private static final class TrancheDate {

		private final Date date;
		private final boolean previous;


		TrancheDate(Date date, boolean previous) {
			this.date = date;
			this.previous = previous;
		}


		public Date getDate() {
			return this.date;
		}


		public boolean isPrevious() {
			return this.previous;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public ScheduleApiService getScheduleApiService() {
		return this.scheduleApiService;
	}


	public void setScheduleApiService(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public InvestmentInstrumentCopyService getInvestmentInstrumentCopyService() {
		return this.investmentInstrumentCopyService;
	}


	public void setInvestmentInstrumentCopyService(InvestmentInstrumentCopyService investmentInstrumentCopyService) {
		this.investmentInstrumentCopyService = investmentInstrumentCopyService;
	}


	public Integer getTrancheScheduleId() {
		return this.trancheScheduleId;
	}


	public void setTrancheScheduleId(Integer trancheScheduleId) {
		this.trancheScheduleId = trancheScheduleId;
	}


	public Short getTrancheCount() {
		return this.trancheCount;
	}


	public void setTrancheCount(Short trancheCount) {
		this.trancheCount = trancheCount;
	}


	public Integer getTrancheScheduleCalendarListBeanId() {
		return this.trancheScheduleCalendarListBeanId;
	}


	public void setTrancheScheduleCalendarListBeanId(Integer trancheScheduleCalendarListBeanId) {
		this.trancheScheduleCalendarListBeanId = trancheScheduleCalendarListBeanId;
	}


	public Integer getEffectiveOnScheduleId() {
		return this.effectiveOnScheduleId;
	}


	public void setEffectiveOnScheduleId(Integer effectiveOnScheduleId) {
		this.effectiveOnScheduleId = effectiveOnScheduleId;
	}


	public Integer getEffectiveBusinessDayAdjustment() {
		return this.effectiveBusinessDayAdjustment;
	}


	public void setEffectiveBusinessDayAdjustment(Integer effectiveBusinessDayAdjustment) {
		this.effectiveBusinessDayAdjustment = effectiveBusinessDayAdjustment;
	}


	public Short getBusinessDayCalendarId() {
		return this.businessDayCalendarId;
	}


	public void setBusinessDayCalendarId(Short businessDayCalendarId) {
		this.businessDayCalendarId = businessDayCalendarId;
	}


	public boolean isDateResultIsSecurityLastDeliveryDate() {
		return this.dateResultIsSecurityLastDeliveryDate;
	}


	public void setDateResultIsSecurityLastDeliveryDate(boolean dateResultIsSecurityLastDeliveryDate) {
		this.dateResultIsSecurityLastDeliveryDate = dateResultIsSecurityLastDeliveryDate;
	}


	public boolean isIncludePreviousTranche() {
		return this.includePreviousTranche;
	}


	public void setIncludePreviousTranche(boolean includePreviousTranche) {
		this.includePreviousTranche = includePreviousTranche;
	}


	public boolean isCreateSecurityIfMissing() {
		return this.createSecurityIfMissing;
	}


	public void setCreateSecurityIfMissing(boolean createSecurityIfMissing) {
		this.createSecurityIfMissing = createSecurityIfMissing;
	}
}
