package com.clifton.investment.replication.calculators;

import com.clifton.core.beans.LabeledObject;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory;

import java.math.BigDecimal;
import java.util.Date;


/**
 * This interface serves as a base for entities that represent allocations for specific {@link InvestmentSecurity}s and {@link InvestmentReplication}s.
 * <p>
 * Created with the intent to be used in {@link InvestmentReplicationCalculator} and its implementations.
 *
 * @author mitchellf
 */
public interface InvestmentReplicationSecurityAllocation extends LabeledObject {

	public InvestmentAccount getClientAccount();


	public Date getBalanceDate();


	public InvestmentReplication getReplication();


	public void setReplication(InvestmentReplication replication);


	/**
	 * Calculated getter for this allocation that coalesces the {@link com.clifton.investment.replication.InvestmentReplicationAllocation}
	 * type override and the result of the replication's type.
	 */
	public InvestmentReplicationType getReplicationType();


	public InvestmentSecurity getSecurity();


	public void setSecurity(InvestmentSecurity security);


	public InvestmentReplicationAllocationHistory getReplicationAllocationHistory();


	public InvestmentReplicationAllocation getReplicationAllocation();


	public Short getOrder();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isMatchingAllocation();


	public boolean isCashExposure();


	public boolean isCurrencyReplication();


	public boolean isReverseExposureSign();


	public boolean isTradePropertyValueManuallyOverridden(String valueFieldName);


	public InvestmentSecurity getBenchmarkDurationSecurity();


	public BigDecimal getBenchmarkDuration();


	public BigDecimal getBenchmarkCreditDuration();


	public String getDurationFieldNameOverride();


	public Boolean getDoNotAdjustContractValue();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getTargetExposure();


	public void setTargetExposure(BigDecimal targetExposure);


	public BigDecimal getTargetExposureAdjusted();


	public void setTargetExposureAdjusted(BigDecimal targetExposureAdjusted);


	public BigDecimal getTotalAdditionalExposure();


	public BigDecimal getAdditionalExposure();


	public void setAdditionalExposure(BigDecimal additionalExposure);


	public BigDecimal getAdditionalOverlayExposure();


	public void setAdditionalOverlayExposure(BigDecimal additionalOverlayExposure);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getValue();


	public void setValue(BigDecimal value);


	public BigDecimal getNetAllocation();


	public BigDecimal getTargetContracts();


	public BigDecimal getTargetContractsAdjusted();


	public BigDecimal getActualContractsAdjusted();


	public BigDecimal getActualContracts();


	public void setActualContracts(BigDecimal actualContracts);


	public BigDecimal getVirtualContracts();


	public void setVirtualContracts(BigDecimal virtualContracts);


	public String getContractValuePriceField();


	public void setContractValuePriceField(String contractValuePriceField);


	public BigDecimal getContractValuePrice();


	public void setContractValuePrice(BigDecimal contractValuePrice);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getCurrencyActualLocalAmount();


	public BigDecimal getCurrencyUnrealizedLocalAmount();


	public BigDecimal getCurrencyOtherBaseAmount();


	public BigDecimal getCurrencyActualBaseAmount();


	public BigDecimal getCurrencyUnrealizedBaseAmount();


	public BigDecimal getCurrencyTotalActualBaseAmount();


	public BigDecimal getCurrencyActualTradeBaseAmount();


	public BigDecimal getCurrencyUnrealizedTradeBaseAmount();


	public BigDecimal getCurrencyCurrentTradeBaseAmount();


	public BigDecimal getCurrencyCurrentUnrealizedTradeBaseAmount();


	public BigDecimal getCurrencyCurrentOtherBaseAmount();


	public void setCurrencyCurrentOtherBaseAmount(BigDecimal currencyCurrentOtherBaseAmount);


	public BigDecimal getCurrencyCurrentDenominationBaseAmount();


	public void setCurrencyCurrentDenominationBaseAmount(BigDecimal currencyCurrentDenominationBaseAmount);


	public BigDecimal getCurrencyDenominationBaseAmount();


	public void setCurrencyDenominationBaseAmount(BigDecimal currencyDenominationBaseAmount);


	public BigDecimal getCurrencyTotalBaseAmount();


	public BigDecimal getCurrencyCurrentLocalAmount();


	public void setCurrencyCurrentLocalAmount(BigDecimal currencyCurrentLocalAmount);


	public BigDecimal getCurrencyCurrentUnrealizedLocalAmount();


	public void setCurrencyCurrentUnrealizedLocalAmount(BigDecimal currencyCurrentUnrealizedLocalAmount);


	public BigDecimal getCurrencyExchangeRate();


	public void setCurrencyExchangeRate(BigDecimal currencyExchangeRate);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getDaysToMaturity();


	public BigDecimal getSecurityPrice();


	public void setSecurityPrice(BigDecimal securityPrice);


	public InvestmentSecurity getUnderlyingSecurity();


	public BigDecimal getUnderlyingSecurityPrice();


	public void setUnderlyingSecurityPrice(BigDecimal underlyingSecurityPrice);


	public BigDecimal getExchangeRate();


	public void setExchangeRate(BigDecimal exchangeRate);


	public BigDecimal getDuration();


	public void setDuration(BigDecimal duration);


	public BigDecimal getInterestRate();


	public void setInterestRate(BigDecimal interestRate);


	public BigDecimal getSyntheticAdjustmentFactor();


	public void setSyntheticAdjustmentFactor(BigDecimal syntheticAdjustmentFactor);


	public BigDecimal getIndexRatio();


	public void setIndexRatio(BigDecimal indexRatio);


	public BigDecimal getDelta();


	public void setDelta(BigDecimal delta);


	public BigDecimal getFactor();


	public void setFactor(BigDecimal factor);


	public BigDecimal getSecurityDirtyPrice();


	public void setSecurityDirtyPrice(BigDecimal securityDirtyPrice);


	public BigDecimal getPreviousFairValueAdjustment();


	public void setPreviousFairValueAdjustment(BigDecimal previousFairValueAdjustment);


	public BigDecimal getFairValueAdjustment();


	public void setFairValueAdjustment(BigDecimal fairValueAdjustment);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getTradeSecurityPrice();


	public void setTradeSecurityPrice(BigDecimal tradeSecurityPrice);


	public Date getTradeSecurityPriceDate();


	public void setTradeSecurityPriceDate(Date tradeSecurityPriceDate);


	public BigDecimal getTradeValue();


	public void setTradeValue(BigDecimal tradeValue);


	public InvestmentSecurity getTradeValueCurrency();


	public void setTradeValueCurrency(InvestmentSecurity tradeValueCurrency);


	public BigDecimal getTradeUnderlyingSecurityPrice();


	public void setTradeUnderlyingSecurityPrice(BigDecimal tradeUnderlyingSecurityPrice);


	public Date getTradeUnderlyingSecurityPriceDate();


	public void setTradeUnderlyingSecurityPriceDate(Date tradeUnderlyingSecurityPriceDate);


	public BigDecimal getTradeSecurityDirtyPrice();


	public void setTradeSecurityDirtyPrice(BigDecimal tradeSecurityDirtyPrice);


	public Date getTradeSecurityDirtyPriceDate();


	public void setTradeSecurityDirtyPriceDate(Date tradeSecurityDirtyPriceDate);


	public BigDecimal getTradeExchangeRate();


	public void setTradeExchangeRate(BigDecimal tradeExchangeRate);


	public Date getTradeExchangeRateDate();


	public void setTradeExchangeRateDate(Date tradeExchangeRateDate);


	public BigDecimal getTradeCurrencyExchangeRate();


	public void setTradeCurrencyExchangeRate(BigDecimal tradeCurrencyExchangeRate);


	public Date getTradeCurrencyExchangeRateDate();


	public void setTradeCurrencyExchangeRateDate(Date tradeCurrencyExchangeRateDate);


	public BigDecimal getTradeFairValueAdjustment();


	public void setTradeFairValueAdjustment(BigDecimal tradeFairValueAdjustment);


	public Date getTradeFairValueAdjustmentDate();


	public void setTradeFairValueAdjustmentDate(Date tradeFairValueAdjustmentDate);


	public BigDecimal getTradeDelta();


	public void setTradeDelta(BigDecimal tradeDelta);
}
