package com.clifton.investment.replication.history.rebuild;

import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityCalculator;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory;
import com.clifton.investment.replication.history.InvestmentReplicationHistory;
import com.clifton.investment.replication.history.InvestmentReplicationHistoryService;
import com.clifton.investment.replication.search.InvestmentReplicationSearchForm;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.clifton.core.util.date.DateUtils.fromDate;
import static com.clifton.core.util.status.Status.ofMessage;


/**
 * @author manderson
 */
@Service
public class InvestmentReplicationHistoryRebuildServiceImpl implements InvestmentReplicationHistoryRebuildService {

	private InvestmentReplicationService investmentReplicationService;

	private InvestmentReplicationHistoryService investmentReplicationHistoryService;

	private RunnerHandler runnerHandler;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status rebuildInvestmentReplicationHistory(final InvestmentReplicationHistoryRebuildCommand command) {
		// Command Validation
		ValidationUtils.assertNotNull(command.getStartBalanceDate(), "Start Balance Date is required.");
		ValidationUtils.assertNotNull(command.getEndBalanceDate(), "End Balance Date is required.");
		ValidationUtils.assertTrue(DateUtils.isDateBeforeOrEqual(command.getStartBalanceDate(), command.getEndBalanceDate(), false), "Start Date must be on or before end date.");
		ValidationUtils.assertTrue(DateUtils.isDateBeforeOrEqual(command.getEndBalanceDate(), new Date(), false), "Balance Date cannot be in the future.");
		ValidationUtils.assertFalse(command.isExcludeDynamic() && command.isExcludeStandard(), "You cannot exclude both dynamic and standard replications.");

		if (command.isAllowInactive()) {
			ValidationUtils.assertNotNull(command.getReplicationId(), "Replication is required if allowing inactive replications.");
		}

		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : ofMessage("Synchronously running for " + command);
			doRebuildInvestmentReplicationHistory(command, status);
			return status;
		}

		// asynchronous run support
		final String runId = command.getRunId();
		final Date now = (command.getRunSecondsDelay() == null ? new Date() : DateUtils.addSeconds(new Date(), command.getRunSecondsDelay()));

		final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + fromDate(now));
		Runner runner = new AbstractStatusAwareRunner("REPLICATION-HISTORY", runId, now, statusHolder) {

			@Override
			public void run() {
				try {
					doRebuildInvestmentReplicationHistory(command, statusHolder.getStatus());
				}
				catch (Throwable e) {
					getStatus().setMessage("Error rebuilding replication history for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error rebuilding replication history for " + runId, e);
				}
			}
		};
		getRunnerHandler().rescheduleRunner(runner);

		return Status.ofMessage("Started replication history rebuild: " + runId + ". Processing will be completed shortly.");
	}


	protected void doRebuildInvestmentReplicationHistory(InvestmentReplicationHistoryRebuildCommand command, Status status) {
		List<InvestmentReplication> replicationList = getInvestmentReplicationListForCommand(command);

		InvestmentReplicationHistoryRebuildConfig rebuildConfig = new InvestmentReplicationHistoryRebuildConfig(command);

		Date date = command.getStartBalanceDate();
		if (!DateUtils.isWeekday(date)) {
			date = DateUtils.getNextWeekday(date);
		}
		int processCount = 0;
		int skipCount = 0;
		while (DateUtils.isDateBeforeOrEqual(date, command.getEndBalanceDate(), false)) {
			rebuildConfig.resetForBalanceDate(date);
			status.setMessage("Processing " + DateUtils.fromDateShort(date));
			for (InvestmentReplication replication : CollectionUtils.getIterable(replicationList)) {
				if (!command.isRebuildExisting()) {
					InvestmentReplicationHistory replicationHistory = getInvestmentReplicationHistoryService().getInvestmentReplicationHistoryForBalanceDate(replication.getId(), date, false);
					if (replicationHistory != null) {
						status.addSkipped(replication.getLabel() + " - " + DateUtils.fromDateShort(date));
						skipCount++;
						continue;
					}
				}
				try {
					rebuildInvestmentReplicationHistoryImpl(replication, rebuildConfig);
					status.setActionPerformed(true);
					processCount++;
				}
				catch (Throwable e) {
					String message = ExceptionUtils.getOriginalMessage(e);
					status.addError(replication.getLabel() + " - " + DateUtils.fromDateShort(date) + ": " + message);
					LogUtils.errorOrInfo(LogCommand.ofThrowable(getClass(), e));
				}
			}
			date = DateUtils.getNextWeekday(date);
		}
		status.setMessage("Processing Complete. " + CollectionUtils.getSize(replicationList) + " Total Replications.  " + DateUtils.fromDateShort(command.getStartBalanceDate()) + " - " + DateUtils.fromDateShort(command.getEndBalanceDate()) + ": Processed: " + processCount + ", Skipped: " + skipCount + ", Failed: " + status.getErrorCount());
	}


	protected InvestmentReplicationHistory rebuildInvestmentReplicationHistoryImpl(InvestmentReplication replication, InvestmentReplicationHistoryRebuildConfig rebuildConfig) {
		List<InvestmentReplicationAllocation> allocationList;
		// If it's a dynamic replication - re-build the dynamic results
		if (replication.isDynamic()) {
			allocationList = getInvestmentReplicationService().rebuildInvestmentReplicationAllocationDynamicResultList(replication, rebuildConfig.getBalanceDate());
		}
		else {
			allocationList = getInvestmentReplicationService().getInvestmentReplicationAllocationListByReplicationActive(replication.getId(), rebuildConfig.getReplicationAllocationActiveOnDate(), false);
		}
		ValidationUtils.assertNotEmpty(allocationList, "Missing active allocations using active as of " + DateUtils.fromDateSmart(rebuildConfig.getReplicationAllocationActiveOnDate()));

		InvestmentReplicationHistory replicationHistory = new InvestmentReplicationHistory();
		replicationHistory.setReplication(replication);
		replicationHistory.setReplicationAllocationHistoryList(new ArrayList<>());

		for (InvestmentReplicationAllocation replicationAllocation : allocationList) {
			InvestmentReplicationAllocationHistory allocationHistory = new InvestmentReplicationAllocationHistory();
			allocationHistory.setReplicationHistory(replicationHistory);
			allocationHistory.setReplicationAllocation(replicationAllocation);
			if (replicationAllocation.getReplicationSecurity() != null && !replicationAllocation.isSecurityUnderlying()) {
				allocationHistory.setCurrentInvestmentSecurity(replicationAllocation.getReplicationSecurity());
			}
			else {
				ValidationUtils.assertNotNull(replicationAllocation.getCurrentSecurityCalculatorBean(), "Replication Allocation " + replicationAllocation.getAllocationLabel() + " is missing a current security calculator.");
				InvestmentCurrentSecurityCalculator currentSecurityCalculator = rebuildConfig.getInvestmentCurrentSecurityCalculator(replicationAllocation.getCurrentSecurityCalculatorBean(), getSystemBeanService());
				InvestmentSecurity currentSecurity = currentSecurityCalculator.calculateCurrentSecurity(replicationAllocation, rebuildConfig.getCurrentSecurityCalculatorDate());
				ValidationUtils.assertNotNull(currentSecurity, "Replication Allocation " + replicationAllocation.getAllocationLabel() + ": Unable to calculate a current security for " + DateUtils.fromDateShort(rebuildConfig.getCurrentSecurityCalculatorDate()));
				allocationHistory.setCurrentInvestmentSecurity(currentSecurity);
			}
			replicationHistory.getReplicationAllocationHistoryList().add(allocationHistory);
		}
		return getInvestmentReplicationHistoryService().saveInvestmentReplicationHistory(replicationHistory, rebuildConfig.getBalanceDate());
	}


	private List<InvestmentReplication> getInvestmentReplicationListForCommand(InvestmentReplicationHistoryRebuildCommand command) {
		if (command.getReplicationId() != null) {
			return CollectionUtils.createList(getInvestmentReplicationService().getInvestmentReplication(command.getReplicationId()));
		}
		InvestmentReplicationSearchForm searchForm = new InvestmentReplicationSearchForm();
		searchForm.setInactive(false);
		if (command.isExcludeDynamic()) {
			searchForm.setDynamic(false);
		}
		else if (command.isExcludeStandard()) {
			searchForm.setDynamic(true);
		}
		searchForm.setOrderBy("typeOrder:ASC");
		return getInvestmentReplicationService().getInvestmentReplicationList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentReplicationHistory getOrBuildInvestmentReplicationHistory(int replicationId, Date balanceDate) {
		InvestmentReplicationHistory replicationHistory = getInvestmentReplicationHistoryService().getInvestmentReplicationHistoryForBalanceDate(replicationId, balanceDate, true);
		if (replicationHistory == null) {
			InvestmentReplicationHistoryRebuildCommand rebuildCommand = new InvestmentReplicationHistoryRebuildCommand();
			rebuildCommand.setReplicationId(replicationId);
			rebuildCommand.setStartBalanceDate(balanceDate);
			rebuildCommand.setEndBalanceDate(balanceDate);
			rebuildCommand.setSynchronous(true);
			Status status = rebuildInvestmentReplicationHistory(rebuildCommand);
			replicationHistory = getInvestmentReplicationHistoryService().getInvestmentReplicationHistoryForBalanceDate(replicationId, balanceDate, true);
			ValidationUtils.assertFalse(replicationHistory == null && !CollectionUtils.isEmpty(status.getErrorList()), () -> {
				String replicationName = getInvestmentReplicationService().getInvestmentReplication(replicationId).getName();
				return "Replication history rebuild for replication " + replicationName + " finished with error for balance date "
						+ DateUtils.fromDateShort(balanceDate) + "\n" + StringUtils.collectionToDelimitedString(status.getErrorList(), "\n");
			});
		}
		return replicationHistory;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public InvestmentReplicationHistoryService getInvestmentReplicationHistoryService() {
		return this.investmentReplicationHistoryService;
	}


	public void setInvestmentReplicationHistoryService(InvestmentReplicationHistoryService investmentReplicationHistoryService) {
		this.investmentReplicationHistoryService = investmentReplicationHistoryService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
