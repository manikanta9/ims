package com.clifton.investment.replication.history.cache;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.investment.replication.history.InvestmentReplicationHistory;

import java.util.Date;


/**
 * The <code>InvestmentReplicationHistoryBalanceDateCache</code> caches key ReplicationID_BalanceDate to the history record id that is the last available history for that balance date
 *
 * @author manderson
 */
public interface InvestmentReplicationHistoryBalanceDateCache {

	public InvestmentReplicationHistory getInvestmentReplicationHistoryLastForReplicationAndBalanceDate(ReadOnlyDAO<InvestmentReplicationHistory> dao, Integer replicationId, Date balanceDate);
}
