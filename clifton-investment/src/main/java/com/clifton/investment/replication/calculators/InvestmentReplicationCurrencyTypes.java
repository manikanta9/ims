package com.clifton.investment.replication.calculators;

/**
 * <code>InvestmentReplicationCurrencyTypes</code> defines the currency to use for calculating replication details.
 * In most cases the default of the client account base currency is used. Some uses can request
 * different currency views for the security replications to facilitate trading contract value updates.
 * <p>
 * The currency type will dictate the currency to use when displaying and trading for a replication.
 *
 * @author nickk
 */
public enum InvestmentReplicationCurrencyTypes {
	/*
	 * The default replication currency, which is the replication's client account's base currency.
	 * Trade amounts, when applicable, will be local/transaction currency or units depending on the security.
	 */
	DEFAULT_CURRENCY(true),
	/*
	 * The base currency of the replication's client account.
	 * Similar to the default currency type except trade amounts, when applicable, will be in base currency or units depending on the security.
	 */
	BASE_CURRENCY(true),
	/*
	 * The transaction currency of the replication's security. For Forwards, this is the forward's underlying currency.
	 * Trade amounts, when applicable, will be local/transaction currency or units depending on the security like the default currency type.
	 */
	TRANSACTION_CURRENCY(false),
	/*
	 * Currency denomination of the replication's security. For forwards, this is the forward's denominating currency.
	 * Trade amounts, when applicable, will be denominating currency or units depending on the security.
	 */
	SETTLE_CURRENCY(false);

	private final boolean defaultCurrencyType;


	InvestmentReplicationCurrencyTypes(boolean defaultType) {
		this.defaultCurrencyType = defaultType;
	}


	/**
	 * Returns true if the currency type is to reflect base currency amounts.
	 */
	public boolean isBaseCurrencyType() {
		return this.defaultCurrencyType;
	}


	/**
	 * Returns the default currency type for display and calculations.
	 */
	public static InvestmentReplicationCurrencyTypes getDefaultCurrencyType() {
		return DEFAULT_CURRENCY;
	}
}
