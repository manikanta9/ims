package com.clifton.investment.replication;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.search.InvestmentReplicationAllocationSearchForm;
import com.clifton.investment.replication.search.InvestmentReplicationSearchForm;
import com.clifton.investment.replication.search.InvestmentReplicationTypeSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentReplicationService</code> interface defines methods for managing
 * InvestmentReplication and InvestmentBenchmark objects.
 *
 * @author vgomelsky
 */
public interface InvestmentReplicationService {

	////////////////////////////////////////////////////////////////////////////
	///////          InvestmentReplication Business Methods           //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns fully populated InvestmentReplication object for the specified id.
	 * The object includes the list of corresponding allocations (that are currently active right now).
	 */
	public InvestmentReplication getInvestmentReplication(int id);


	public List<InvestmentReplication> getInvestmentReplicationList(InvestmentReplicationSearchForm searchForm);


	/**
	 * Saves the specified InvestmentReplication object and its allocations in the database.
	 * Validates the object to make sure that the sum of allocation percentages is 100%.
	 */
	public InvestmentReplication saveInvestmentReplication(InvestmentReplication bean);


	/**
	 * Deletes the specified InvestmentReplication and its allocations from the database.
	 */
	public void deleteInvestmentReplication(int id);

	@DoNotAddRequestMapping
	public boolean isInvestmentSecurityInReplication(InvestmentReplication replication, InvestmentSecurity security);

	////////////////////////////////////////////////////////////////////////////
	/////    Investment Replication Allocation Security Business Methods   /////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationAllocation getInvestmentReplicationAllocation(int id);


	public List<InvestmentReplicationAllocation> getInvestmentReplicationAllocationList(InvestmentReplicationAllocationSearchForm searchForm);


	public List<InvestmentReplicationAllocation> getInvestmentReplicationAllocationListByReplicationActive(int replicationId, Date activeOnDate, boolean dynamicResult);


	public InvestmentReplicationAllocation saveInvestmentReplicationAllocation(InvestmentReplicationAllocation bean);


	/**
	 * Validates that the {@link InvestmentReplicationAllocation} has a valid setup for generating a list of {@link InvestmentSecurity}s.
	 */
	public void validateInvestmentReplicationAllocationSecuritySpecificity(InvestmentReplicationAllocation replicationAllocation);


	/**
	 * One place where this is useful is DE Tranche Processing, where the team wants to maintain a {@link com.clifton.investment.setup.group.InvestmentSecurityGroup} of Put Options
	 * and then configure an {@link InvestmentReplicationAllocation} to generate a list of Flex, Listed and OTC securities. For example, specifying SPX as the Underlying Security and
	 * getting SPX FLEX, SPXW, etc.
	 */
	public List<InvestmentSecurity> getInvestmentReplicationAllocationSecurityList(InvestmentReplicationAllocation replicationAllocation, Date date);


	@DoNotAddRequestMapping
	public List<InvestmentReplicationAllocation> rebuildInvestmentReplicationAllocationDynamicResultList(InvestmentReplication replication, Date balanceDate);


	/**
	 * Returns true if the given security is a valid selection for the given allocation
	 */
	@DoNotAddRequestMapping
	public boolean isInvestmentReplicationAllocationSecurityValid(InvestmentReplicationAllocation allocation, Date date, InvestmentSecurity security);


	@DoNotAddRequestMapping
	public List<InvestmentSecurity> getInvestmentReplicationAllocationSecurityList(InvestmentReplicationAllocation allocation, Date date, List<InvestmentSecurity> securityList);

	////////////////////////////////////////////////////////////////////////////
	///////        InvestmentReplicationType Business Methods           ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationType getInvestmentReplicationType(short id);


	public InvestmentReplicationType getInvestmentReplicationTypeByName(String name);


	public List<InvestmentReplicationType> getInvestmentReplicationTypeList();


	public List<InvestmentReplicationType> getInvestmentReplicationTypeList(InvestmentReplicationTypeSearchForm searchForm);


	public InvestmentReplicationType saveInvestmentReplicationType(InvestmentReplicationType bean);
}
