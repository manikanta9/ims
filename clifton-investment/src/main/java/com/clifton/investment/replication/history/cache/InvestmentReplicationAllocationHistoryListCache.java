package com.clifton.investment.replication.history.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory;
import org.springframework.stereotype.Component;


/**
 * An entity cache for retrieving lists of {@link InvestmentReplicationAllocationHistory} entities by {@link com.clifton.investment.replication.history.InvestmentReplicationHistory} ID.
 *
 * @author manderson
 */
@Component
public class InvestmentReplicationAllocationHistoryListCache extends SelfRegisteringSingleKeyDaoListCache<InvestmentReplicationAllocationHistory, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "replicationHistory.id";
	}


	@Override
	protected Integer getBeanKeyValue(InvestmentReplicationAllocationHistory bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getReplicationHistory());
		}
		return null;
	}
}
