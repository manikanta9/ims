package com.clifton.investment.replication.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>InvestmentReplicationSearchForm</code> class defines search configuration for InvestmentReplication objects.
 *
 * @author vgomelsky
 */
public class InvestmentReplicationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField(searchField = "name")
	private String[] names;

	@SearchField(searchField = "type.id", sortField = "type.name")
	private Short typeId;

	@SearchField(searchField = "type.order")
	private Integer typeOrder;

	@SearchField(searchFieldPath = "type.calculatorBean", searchField = "name")
	private String calculatorBeanName;

	@SearchField(searchFieldPath = "type", searchField = "calculatorBean.id", sortField = "calculatorBean.name")
	private Integer calculatorBeanId;

	@SearchField(searchFieldPath = "type", searchField = "currency")
	private Boolean currency;

	@SearchField
	private Boolean partiallyAllocated;

	@SearchField
	private Boolean allowedForMatching;

	@SearchField
	private Boolean inactive;

	@SearchField
	private Boolean reverseExposureSign;

	@SearchField(searchField = "dynamicCalculatorBean.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean dynamic;

	@SearchField(searchFieldPath = "dynamicCalculatorBean", searchField = "name")
	private String dynamicCalculatorBeanName;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getPartiallyAllocated() {
		return this.partiallyAllocated;
	}


	public void setPartiallyAllocated(Boolean partiallyAllocated) {
		this.partiallyAllocated = partiallyAllocated;
	}


	public Boolean getAllowedForMatching() {
		return this.allowedForMatching;
	}


	public void setAllowedForMatching(Boolean allowedForMatching) {
		this.allowedForMatching = allowedForMatching;
	}


	public Boolean getInactive() {
		return this.inactive;
	}


	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public Integer getTypeOrder() {
		return this.typeOrder;
	}


	public void setTypeOrder(Integer typeOrder) {
		this.typeOrder = typeOrder;
	}


	public Boolean getDynamic() {
		return this.dynamic;
	}


	public void setDynamic(Boolean dynamic) {
		this.dynamic = dynamic;
	}


	public Integer getCalculatorBeanId() {
		return this.calculatorBeanId;
	}


	public void setCalculatorBeanId(Integer calculatorBeanId) {
		this.calculatorBeanId = calculatorBeanId;
	}


	public String getCalculatorBeanName() {
		return this.calculatorBeanName;
	}


	public void setCalculatorBeanName(String calculatorBeanName) {
		this.calculatorBeanName = calculatorBeanName;
	}


	public String getDynamicCalculatorBeanName() {
		return this.dynamicCalculatorBeanName;
	}


	public void setDynamicCalculatorBeanName(String dynamicCalculatorBeanName) {
		this.dynamicCalculatorBeanName = dynamicCalculatorBeanName;
	}


	public String[] getNames() {
		return this.names;
	}


	public void setNames(String[] names) {
		this.names = names;
	}


	public Boolean getCurrency() {
		return this.currency;
	}


	public void setCurrency(Boolean currency) {
		this.currency = currency;
	}


	public Boolean getReverseExposureSign() {
		return this.reverseExposureSign;
	}


	public void setReverseExposureSign(Boolean reverseExposureSign) {
		this.reverseExposureSign = reverseExposureSign;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}
}
