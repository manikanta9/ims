package com.clifton.investment.replication.history;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.replication.InvestmentReplication;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentReplicationHistory</code> represents the state of a replication for a given balance date.
 * <p>
 * If there are multiple historical records for a given balance date the last one created would "win" for that balance date when retrieving
 * This can happen when a replication is used for a given balance date, then updated (today's updates apply to previous weekday's balance date)
 *
 * @author manderson
 */
public class InvestmentReplicationHistory extends BaseEntity<Integer> {

	/**
	 * The replication this historical record is for.
	 */
	private InvestmentReplication replication;

	/**
	 * For replications that do not change very often and their current securities on their allocations only change every few months
	 * using balance date ranges makes one record available to apply to a set of dates vs. saving every date.
	 * BALANCE DATE start date
	 */
	private Date startDate;

	/**
	 * End Date is always required, however after processing if next balance date results match previous balance date results instead of saving as a new record
	 * the existing record's end date is just moved forward.
	 * <p>
	 * BALANCE DATE end date
	 */
	private Date endDate;


	private List<InvestmentReplicationAllocationHistory> replicationAllocationHistoryList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		if (getReplication() != null) {
			return getReplication().getLabel() + " " + DateUtils.fromDateRange(getStartDate(), getEndDate(), true, true);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplication getReplication() {
		return this.replication;
	}


	public void setReplication(InvestmentReplication replication) {
		this.replication = replication;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public List<InvestmentReplicationAllocationHistory> getReplicationAllocationHistoryList() {
		return this.replicationAllocationHistoryList;
	}


	public void setReplicationAllocationHistoryList(List<InvestmentReplicationAllocationHistory> replicationAllocationHistoryList) {
		this.replicationAllocationHistoryList = replicationAllocationHistoryList;
	}
}
