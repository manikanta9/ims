package com.clifton.investment.replication;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.replication.history.rebuild.InvestmentReplicationHistoryRebuildCommand;
import com.clifton.investment.replication.history.rebuild.InvestmentReplicationHistoryRebuildService;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * On changes to allocations (not dynamic results) will schedule the replication history to be rebuilt for previous weekday (if replication is active)
 *
 * @author manderson
 */
@Component
public class InvestmentReplicationAllocationObserver extends SelfRegisteringDaoObserver<InvestmentReplicationAllocation> {

	private InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<InvestmentReplicationAllocation> dao, DaoEventTypes event, InvestmentReplicationAllocation bean, Throwable e) {
		if (e == null && !bean.isDynamicResult() && !bean.getReplication().isInactive()) {
			InvestmentReplicationHistoryRebuildCommand rebuildCommand = new InvestmentReplicationHistoryRebuildCommand();
			rebuildCommand.setReplicationId(bean.getReplication().getId());
			rebuildCommand.setStartBalanceDate(DateUtils.getPreviousWeekday(DateUtils.clearTime(new Date())));
			rebuildCommand.setEndBalanceDate(rebuildCommand.getStartBalanceDate());
			rebuildCommand.setRebuildExisting(true);
			rebuildCommand.setRunSecondsDelay(1); // 1 second delay so all allocation changes are saved
			getInvestmentReplicationHistoryRebuildService().rebuildInvestmentReplicationHistory(rebuildCommand);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationHistoryRebuildService getInvestmentReplicationHistoryRebuildService() {
		return this.investmentReplicationHistoryRebuildService;
	}


	public void setInvestmentReplicationHistoryRebuildService(InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService) {
		this.investmentReplicationHistoryRebuildService = investmentReplicationHistoryRebuildService;
	}
}
