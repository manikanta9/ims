package com.clifton.investment.replication.upload;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.replication.InvestmentReplicationAllocation;


/**
 * InvestmentReplicationUploadServiceImpl handles uploading replication allocation information for replications
 * Created by @Mary Anderson
 */
public interface InvestmentReplicationUploadService {


	////////////////////////////////////////////////////////////////
	///////   Investment Replication Allocation Uploads    /////////
	////////////////////////////////////////////////////////////////


	/**
	 * Custom upload to update existing allocations (weights and optionally order and current security calculator)
	 */
	@SecureMethod(dtoClass = InvestmentReplicationAllocation.class)
	public void uploadInvestmentReplicationAllocationUploadFile(InvestmentReplicationAllocationUploadCommand uploadCommand);
}
