package com.clifton.investment.replication.upload;

import com.clifton.core.dataaccess.file.upload.FileUploadCommand;

import java.math.BigDecimal;


/**
 * The <code>InvestmentReplicationAllocationUploadCommand</code> extends the System Upload
 * however has some replication specific options
 * <p>
 * Note: Upload is really a list of allocations, but we apply them to the replication and save at that level
 *
 * @author Mary Anderson
 */
public class InvestmentReplicationAllocationUploadCommand extends FileUploadCommand<InvestmentReplicationAllocationUploadTemplate> {


	public static final String[] REQUIRED_PROPERTIES = {"replicationName"};

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * When updating allocations, if previous only found (matched by instrument, security group, security)
	 * will verify the percentage change from previous to new does not exceed +/- specified percentage
	 */
	private BigDecimal maxAllocationChange;


	/**
	 * True indicates that if not all rows can be uploaded,
	 * upload the successful rows and ignore the unsuccessful rows
	 * else don't upload any rows
	 */
	private boolean partialUploadAllowed;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<InvestmentReplicationAllocationUploadTemplate> getUploadBeanClass() {
		return InvestmentReplicationAllocationUploadTemplate.class;
	}


	@Override
	public String[] getRequiredProperties() {
		return REQUIRED_PROPERTIES;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getMaxAllocationChange() {
		return this.maxAllocationChange;
	}


	public void setMaxAllocationChange(BigDecimal maxAllocationChange) {
		this.maxAllocationChange = maxAllocationChange;
	}


	public boolean isPartialUploadAllowed() {
		return this.partialUploadAllowed;
	}


	public void setPartialUploadAllowed(boolean partialUploadAllowed) {
		this.partialUploadAllowed = partialUploadAllowed;
	}
}
