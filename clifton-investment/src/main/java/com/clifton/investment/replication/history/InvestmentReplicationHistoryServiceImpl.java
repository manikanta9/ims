package com.clifton.investment.replication.history;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.replication.history.cache.InvestmentReplicationHistoryBalanceDateCache;
import com.clifton.investment.replication.history.search.InvestmentReplicationAllocationHistorySearchForm;
import com.clifton.investment.replication.history.search.InvestmentReplicationHistorySearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
@Service
public class InvestmentReplicationHistoryServiceImpl implements InvestmentReplicationHistoryService {

	private AdvancedUpdatableDAO<InvestmentReplicationHistory, Criteria> investmentReplicationHistoryDAO;
	private AdvancedUpdatableDAO<InvestmentReplicationAllocationHistory, Criteria> investmentReplicationAllocationHistoryDAO;

	private InvestmentReplicationHistoryBalanceDateCache investmentReplicationHistoryBalanceDateCache;

	private CalendarBusinessDayService calendarBusinessDayService;

	private DaoSingleKeyListCache<InvestmentReplicationAllocationHistory, Integer> investmentReplicationAllocationHistoryListCache;


	////////////////////////////////////////////////////////////////////////////
	////////     Investment Replication History Business Methods        ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentReplicationHistory getInvestmentReplicationHistory(int id, boolean populateAllocations) {
		InvestmentReplicationHistory history = getInvestmentReplicationHistoryDAO().findByPrimaryKey(id);
		if (history != null && populateAllocations) {
			history.setReplicationAllocationHistoryList(getInvestmentReplicationAllocationHistoryForReplicationHistory(id));
		}
		return history;
	}


	@Override
	public InvestmentReplicationHistory getInvestmentReplicationHistoryForBalanceDate(int replicationId, Date balanceDate, boolean populateAllocations) {
		InvestmentReplicationHistory history = getInvestmentReplicationHistoryBalanceDateCache().getInvestmentReplicationHistoryLastForReplicationAndBalanceDate(getInvestmentReplicationHistoryDAO(), replicationId, balanceDate);
		if (history != null && populateAllocations) {
			history.setReplicationAllocationHistoryList(getInvestmentReplicationAllocationHistoryForReplicationHistory(history.getId()));
		}
		return history;
	}


	@Override
	public List<InvestmentReplicationHistory> getInvestmentReplicationHistoryList(InvestmentReplicationHistorySearchForm searchForm) {
		return getInvestmentReplicationHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Transactional
	@Override
	public InvestmentReplicationHistory saveInvestmentReplicationHistory(InvestmentReplicationHistory bean, Date balanceDate) {
		ValidationUtils.assertTrue(bean.isNewBean(), "Direct updates to existing replication history records are not allowed");
		ValidationUtils.assertNotEmpty(bean.getReplicationAllocationHistoryList(), "Allocation History is required");
		InvestmentReplicationHistory existingHistory = getInvestmentReplicationHistoryForBalanceDate(bean.getReplication().getId(), balanceDate, true);

		// If missing - get value for previous weekday
		if (existingHistory == null) {
			existingHistory = getInvestmentReplicationHistoryForBalanceDate(bean.getReplication().getId(), DateUtils.getPreviousWeekday(balanceDate), true);
		}

		// If still missing and previous weekday is a holiday (default calendar) - get existing from previous business day
		if (existingHistory == null && !getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(DateUtils.getPreviousWeekday(balanceDate)))) {
			existingHistory = getInvestmentReplicationHistoryForBalanceDate(bean.getReplication().getId(), getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(balanceDate)), true);
		}

		boolean insert = existingHistory == null || isReplicationHistoryDifferent(bean, existingHistory);

		if (insert) {
			if (existingHistory != null && DateUtils.isDateAfter(existingHistory.getEndDate(), balanceDate)) {
				existingHistory.setEndDate(balanceDate);
				getInvestmentReplicationHistoryDAO().save(existingHistory);
			}
			bean.setStartDate(balanceDate);
			bean.setEndDate(balanceDate);
			List<InvestmentReplicationAllocationHistory> allocationHistoryList = bean.getReplicationAllocationHistoryList();
			bean = getInvestmentReplicationHistoryDAO().save(bean);

			for (InvestmentReplicationAllocationHistory allocationHistory : allocationHistoryList) {
				allocationHistory.setReplicationHistory(bean);
			}
			getInvestmentReplicationAllocationHistoryDAO().saveList(allocationHistoryList);
			bean.setReplicationAllocationHistoryList(allocationHistoryList);
			return bean;
		}
		else {
			existingHistory.setEndDate(balanceDate);
			return getInvestmentReplicationHistoryDAO().save(existingHistory);
		}
	}


	@Transactional
	@Override
	public void clearInvestmentReplicationHistoryList(int replicationId) {
		InvestmentReplicationAllocationHistorySearchForm allocationHistorySearchForm = new InvestmentReplicationAllocationHistorySearchForm();
		allocationHistorySearchForm.setReplicationId(replicationId);

		List<InvestmentReplicationAllocationHistory> allocationHistoryList = getInvestmentReplicationAllocationHistoryList(allocationHistorySearchForm);

		InvestmentReplicationHistorySearchForm searchForm = new InvestmentReplicationHistorySearchForm();
		searchForm.setReplicationId(replicationId);
		List<InvestmentReplicationHistory> replicationHistoryList = getInvestmentReplicationHistoryList(searchForm);

		getInvestmentReplicationAllocationHistoryDAO().deleteList(allocationHistoryList);
		getInvestmentReplicationHistoryDAO().deleteList(replicationHistoryList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Two replication history records are the same if their allocations are the same
	 */
	private boolean isReplicationHistoryDifferent(InvestmentReplicationHistory newHistory, InvestmentReplicationHistory existingHistory) {
		List<InvestmentReplicationAllocationHistory> newAllocationHistoryList = newHistory.getReplicationAllocationHistoryList();
		List<InvestmentReplicationAllocationHistory> existingAllocationHistoryList = existingHistory.getReplicationAllocationHistoryList();

		if (!MathUtils.isEqual(CollectionUtils.getSize(newAllocationHistoryList), CollectionUtils.getSize(existingAllocationHistoryList))) {
			return true;
		}

		for (InvestmentReplicationAllocationHistory existingAllocationHistory : existingAllocationHistoryList) {
			boolean found = false;
			for (InvestmentReplicationAllocationHistory newAllocationHistory : newAllocationHistoryList) {
				if (existingAllocationHistory.getReplicationAllocation().equals(newAllocationHistory.getReplicationAllocation())) {
					if (existingAllocationHistory.getCurrentInvestmentSecurity().equals(newAllocationHistory.getCurrentInvestmentSecurity())) {
						found = true;
						break;
					}
				}
			}
			if (!found) {
				return true;
			}
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	//////// Investment Replication Allocation History Business Methods ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentReplicationAllocationHistory> getInvestmentReplicationAllocationHistoryList(InvestmentReplicationAllocationHistorySearchForm searchForm) {
		return getInvestmentReplicationAllocationHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getActiveOnDate() != null) {
					String alias = getPathAlias("replicationHistory", criteria);
					criteria.add(ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(true, alias, "startDate", "endDate", searchForm.getActiveOnDate()));
				}
				if (searchForm.getActiveOnStartDate() != null || searchForm.getActiveOnEndDate() != null) {
					String alias = getPathAlias("replicationHistory", criteria);
					criteria.add(ActiveExpressionForDates.forActiveOnDateRangeWithAliasAndProperties(true, alias, "startDate", "endDate", searchForm.getActiveOnStartDate(), searchForm.getActiveOnEndDate()));
				}
				if (searchForm.getOrderBy() == null) {
					if (searchForm.getReplicationHistoryId() != null) {
						searchForm.setOrderBy("allocationOrder:ASC");
					}
					else if (searchForm.getActiveOnDate() != null) {
						searchForm.setOrderBy("createDate:DESC#allocationOrder:ASC");
					}
					else {
						searchForm.setOrderBy("endDate:DESC#createDate:DESC#allocationOrder:ASC");
					}
				}
			}
		});
	}


	protected List<InvestmentReplicationAllocationHistory> getInvestmentReplicationAllocationHistoryForReplicationHistory(int replicationHistoryId) {
		return CollectionUtils.getStream(getInvestmentReplicationAllocationHistoryListCache().getBeanListForKeyValue(getInvestmentReplicationAllocationHistoryDAO(), replicationHistoryId)).sorted(Comparator.comparing(allocationHistory -> ObjectUtils.coalesce(allocationHistory.getReplicationAllocation().getOrder(), (short) 0))).collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentReplicationHistory, Criteria> getInvestmentReplicationHistoryDAO() {
		return this.investmentReplicationHistoryDAO;
	}


	public void setInvestmentReplicationHistoryDAO(AdvancedUpdatableDAO<InvestmentReplicationHistory, Criteria> investmentReplicationHistoryDAO) {
		this.investmentReplicationHistoryDAO = investmentReplicationHistoryDAO;
	}


	public AdvancedUpdatableDAO<InvestmentReplicationAllocationHistory, Criteria> getInvestmentReplicationAllocationHistoryDAO() {
		return this.investmentReplicationAllocationHistoryDAO;
	}


	public void setInvestmentReplicationAllocationHistoryDAO(AdvancedUpdatableDAO<InvestmentReplicationAllocationHistory, Criteria> investmentReplicationAllocationHistoryDAO) {
		this.investmentReplicationAllocationHistoryDAO = investmentReplicationAllocationHistoryDAO;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentReplicationHistoryBalanceDateCache getInvestmentReplicationHistoryBalanceDateCache() {
		return this.investmentReplicationHistoryBalanceDateCache;
	}


	public void setInvestmentReplicationHistoryBalanceDateCache(InvestmentReplicationHistoryBalanceDateCache investmentReplicationHistoryBalanceDateCache) {
		this.investmentReplicationHistoryBalanceDateCache = investmentReplicationHistoryBalanceDateCache;
	}


	public DaoSingleKeyListCache<InvestmentReplicationAllocationHistory, Integer> getInvestmentReplicationAllocationHistoryListCache() {
		return this.investmentReplicationAllocationHistoryListCache;
	}


	public void setInvestmentReplicationAllocationHistoryListCache(DaoSingleKeyListCache<InvestmentReplicationAllocationHistory, Integer> investmentReplicationAllocationHistoryListCache) {
		this.investmentReplicationAllocationHistoryListCache = investmentReplicationAllocationHistoryListCache;
	}
}
