package com.clifton.investment.replication;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityCalculator;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.replication.dynamic.InvestmentReplicationDynamicCalculator;
import com.clifton.investment.replication.search.InvestmentReplicationAllocationSearchForm;
import com.clifton.investment.replication.search.InvestmentReplicationSearchForm;
import com.clifton.investment.replication.search.InvestmentReplicationTypeSearchForm;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>InvestmentReplicationServiceImpl</code> class provides basic implementation of InvestmentReplicationService interface.
 *
 * @author vgomelsky
 */
@Service
public class InvestmentReplicationServiceImpl implements InvestmentReplicationService {

	private AdvancedUpdatableDAO<InvestmentReplication, Criteria> investmentReplicationDAO;
	private AdvancedUpdatableDAO<InvestmentReplicationAllocation, Criteria> investmentReplicationAllocationDAO;
	private AdvancedUpdatableDAO<InvestmentReplicationType, Criteria> investmentReplicationTypeDAO;

	private SystemBeanService systemBeanService;

	private DaoNamedEntityCache<InvestmentReplicationType> investmentReplicationTypeCache;

	private InvestmentSecurityGroupService investmentSecurityGroupService;
	private InvestmentInstrumentService investmentInstrumentService;

	////////////////////////////////////////////////////////////////////////////
	///////          InvestmentReplication Business Methods           //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentReplication getInvestmentReplication(int id) {
		InvestmentReplication result = getInvestmentReplicationDAO().findByPrimaryKey(id);
		if (result != null) {
			result.setAllocationList(getInvestmentReplicationAllocationListByReplicationActive(id, new Date(), false));
		}
		return result;
	}


	@Override
	public List<InvestmentReplication> getInvestmentReplicationList(InvestmentReplicationSearchForm searchForm) {
		return getInvestmentReplicationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public InvestmentReplication saveInvestmentReplication(InvestmentReplication bean) {
		// If replication is inactive, do not need to validate the allocations
		boolean validateAllocations = !bean.isInactive();
		List<InvestmentReplicationAllocation> allocations = bean.getAllocationList();

		if (validateAllocations) {
			validateReplicationAllocations(bean);
		}
		ValidationUtils.assertTrue(bean.getType() == null || !bean.getType().isRollupReplication() || bean.isDynamic(), String.format("Replication [%s] must have a Dynamic Calculator because Replication Type [%s] is a rollup Replication Type.", bean.getLabel(), bean.getType().getName()));

		// save the replication and its allocations
		boolean newReplication = bean.isNewBean();
		bean = getInvestmentReplicationDAO().save(bean);
		saveInvestmentReplicationAllocationActiveList(bean, newReplication, false, allocations, new Date());
		bean.setAllocationList(allocations);
		return bean;
	}


	private void validateReplicationAllocations(InvestmentReplication bean) {
		List<InvestmentReplicationAllocation> allocations = bean.getAllocationList();
		ValidationUtils.assertNotEmpty(allocations, "Investment replication must have at least one allocation.");

		BigDecimal totalWeight = BigDecimal.ZERO;
		Map<Integer, InvestmentCurrentSecurityCalculator> beanMap = new HashMap<>();
		Set<Integer> childReplicationIdSet = new HashSet<>();

		for (InvestmentReplicationAllocation allocation : CollectionUtils.getIterable(allocations)) {
			ValidationUtils.assertNotNull(allocation.getAllocationWeight(), "Allocation Weight is required for each allocation");
			ValidationUtils.assertFalse(allocation.getReplicationInstrument() == null && allocation.getReplicationSecurity() == null && allocation.getReplicationSecurityGroup() == null && !bean.getType().isRollupReplication(),
					"At least one of security, instrument, or security group must be selected for allocation");
			ValidationUtils.assertFalse((allocation.getReplicationSecurity() != null && !allocation.isSecurityUnderlying()) && (allocation.getReplicationInstrument() != null || allocation.getReplicationSecurityGroup() != null),
					"The Instrument and Security Group fields can only be used with the Security field if the Security Is Underlying.");
			validateInvestmentReplicationAllocationSecuritySpecificity(allocation);
			// Allow resetting IFF security group is selected
			ValidationUtils.assertTrue(!allocation.isPortfolioSelectedCurrentSecurity() || (allocation.getReplicationSecurityGroup() != null && allocation.getReplicationInstrument() == null),
					"Allocation [" + allocation.getLabel() + "]: Portfolio Defines Current Security selection is only allowed if and only if security group selection is populated.  Instrument and Security selections do not apply.");

			// Same override as actual replication type - clear the value no override necessary
			if (allocation.getReplicationTypeOverride() != null) {
				if (allocation.getReplicationTypeOverride().equals(bean.getType())) {
					allocation.setReplicationTypeOverride(null);
				}
				else if (bean.getType().isRollupReplication()) {
					ValidationUtils.assertFalse(allocation.getReplicationTypeOverride().isRollupReplication(), "Rollup Replications cannot have an allocation with a rollup Type Override.");
				}
			}

			if (bean.getType().isRollupReplication()) {
				ValidationUtils.assertNotNull(allocation.getChildReplication(), "Rollup Replications must have a Child Replication defined for each Allocation.");
				ValidationUtils.assertTrue(childReplicationIdSet.add(allocation.getChildReplication().getId()), "Rollup Replications cannot have duplicate Child Replications. Duplicate child is " + allocation.getChildReplication().getLabel());
			}

			// Only Applies to Fixed Income Replications
			if (!bean.getType().isDurationSupported()) {
				allocation.setDurationOverride(null);
				allocation.setUseBenchmarkDuration(false);
			}

			// NOTE: Current Security Calculator will not be used currently if the replication is dynamic
			if (bean.isDynamic()) {
				allocation.setCurrentSecurityCalculatorBean(null);
			}
			// Calculator doesn't make sense if a specific security is selected
			else if (allocation.getReplicationSecurity() != null && !allocation.isSecurityUnderlying()) {
				ValidationUtils.assertNull(allocation.getCurrentSecurityCalculatorBean(), "Allocation [" + allocation.getLabel()
						+ "]: When a specific security is selected for an allocation, the current security calculator does not apply.");
			}
			else {
				ValidationUtils.assertNotNull(allocation.getCurrentSecurityCalculatorBean(), "Allocation [" + allocation.getLabel() + "]: Current security calculator is required.");
				ValidationUtils.assertNotNull(allocation.getCurrentSecurityTargetAdjustmentType(), "Allocation [" + allocation.getLabel() + "]: Current security target adjustment type is required.");
				InvestmentCurrentSecurityCalculator currentCalc;
				if (beanMap.containsKey(allocation.getCurrentSecurityCalculatorBean().getId())) {
					currentCalc = beanMap.get(allocation.getCurrentSecurityCalculatorBean().getId());
				}
				else {
					currentCalc = (InvestmentCurrentSecurityCalculator) getSystemBeanService().getBeanInstance(allocation.getCurrentSecurityCalculatorBean());
					beanMap.put(allocation.getCurrentSecurityCalculatorBean().getId(), currentCalc);
				}
				currentCalc.validateInvestmentReplicationAllocation(allocation);
			}
			totalWeight = totalWeight.add(allocation.getAllocationWeight());

			if (allocation.getMatchingReplication() != null && !allocation.getMatchingReplication().isAllowedForMatching()) {
				throw new FieldValidationException("Can only used replications that are 'Allowed for Matching' as a matching replication", "matchingReplication");
			}
			// Prevent an allocation on a replication to use itself as it's matching replication
			if (allocation.getMatchingReplication() != null && CompareUtils.isEqual(allocation.getMatchingReplication(), bean)) {
				throw new FieldValidationException("Cannot use same replication [" + bean.getName() + "] as the matching replication for allocation [" + allocation.getAllocationLabel() + "].", "matchingReplication");
			}

			// Not required for each allocation when used, so only validate if the replication has the option turned on
			if (bean.isRebalanceTriggersUsed()) {
				// If min or max Rebalance Triggers is entered, then they both are required:
				if (allocation.getRebalanceAllocationMin() != null || allocation.getRebalanceAllocationMax() != null) {
					ValidationUtils.assertNotNull(allocation.getRebalanceAllocationMin(),
							"If min or max rebalancing trigger is entered, then they both must be populated.  Please enter a value for the Rebalance Allocation Minimum", "rebalanceAllocationMin");
					ValidationUtils.assertNotNull(allocation.getRebalanceAllocationMax(),
							"If min or max rebalancing trigger is entered, then they both must be populated.  Please enter a value for the Rebalance Allocation Maximum", "rebalanceAllocationMax");

					ValidationUtils.assertFalse(MathUtils.isGreaterThan(0, allocation.getRebalanceAllocationMin()) || MathUtils.isGreaterThan(0, allocation.getRebalanceAllocationMax()),
							"All rebalancing targets must be entered as positive values.");
				}
			}
			else {
				allocation.setRebalanceAllocationMin(null);
				allocation.setRebalanceAllocationMax(null);
			}
		}
		if (!bean.isPartiallyAllocated() && !MathUtils.isEqual(totalWeight, 100) && (bean.getType() == null || !bean.getType().isRollupReplication())) {
			throw new FieldValidationException("The sum of all allocation weights is " + totalWeight + " but must be 100%", "allocationWeight");
		}

		// Extra specialized validation for dynamic calculators
		if (bean.isDynamic()) {
			InvestmentReplicationDynamicCalculator dynamicCalculator = (InvestmentReplicationDynamicCalculator) getSystemBeanService().getBeanInstance(bean.getDynamicCalculatorBean());
			dynamicCalculator.validateInvestmentReplication(bean);
		}
	}


	@Override
	@Transactional
	public void deleteInvestmentReplication(int id) {
		InvestmentReplication replication = getInvestmentReplication(id);
		if (replication != null) {
			getInvestmentReplicationAllocationDAO().deleteList(getInvestmentReplicationAllocationDAO().findByField("replication.id", replication.getId()));
		}
		getInvestmentReplicationDAO().delete(replication);
	}


	@Override
	public boolean isInvestmentSecurityInReplication(InvestmentReplication replication, InvestmentSecurity security) {
		List<InvestmentReplicationAllocation> allocations = replication.getAllocationList()
				.stream()
				.filter(alloc -> {
					if (alloc.getReplicationSecurity() != null) {
						if (alloc.isSecurityUnderlying()) {
							if (security.getUnderlyingSecurity() == null || !security.getUnderlyingSecurity().equals(alloc.getReplicationSecurity())) {
								return false;
							}
						}
						else {
							if (!alloc.getReplicationSecurity().equals(security)) {
								return false;
							}
						}
					}
					if (alloc.getReplicationInstrument() != null && !security.getInstrument().equals(alloc.getReplicationInstrument())) {
						return false;
					}
					if (alloc.getReplicationSecurityGroup() != null && !getInvestmentSecurityGroupService().isInvestmentSecurityInSecurityGroup(security.getId(), alloc.getReplicationSecurityGroup().getId())) {
						return false;
					}
					return true;
				}).collect(Collectors.toList());

		return !CollectionUtils.isEmpty(allocations);
	}

	////////////////////////////////////////////////////////////////////////////
	/////    Investment Replication Allocation Security Business Methods   /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentReplicationAllocation getInvestmentReplicationAllocation(int id) {
		return getInvestmentReplicationAllocationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<InvestmentReplicationAllocation> getInvestmentReplicationAllocationListByReplicationActive(int replicationId, Date activeOnDate, boolean dynamicResult) {
		InvestmentReplicationAllocationSearchForm searchForm = new InvestmentReplicationAllocationSearchForm();
		searchForm.setReplicationId(replicationId);
		searchForm.setActiveOnDate(activeOnDate);
		searchForm.setDynamicResult(dynamicResult);
		searchForm.setOrderBy("order");
		return getInvestmentReplicationAllocationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<InvestmentReplicationAllocation> getInvestmentReplicationAllocationList(InvestmentReplicationAllocationSearchForm searchForm) {
		return getInvestmentReplicationAllocationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<InvestmentReplicationAllocation> rebuildInvestmentReplicationAllocationDynamicResultList(InvestmentReplication replication, Date balanceDate) {
		InvestmentReplicationDynamicCalculator calculator = (InvestmentReplicationDynamicCalculator) getSystemBeanService().getBeanInstance(replication.getDynamicCalculatorBean());
		List<InvestmentReplicationAllocation> result = calculator.calculate(replication, balanceDate);
		return saveInvestmentReplicationAllocationActiveList(replication, false, true, result, balanceDate);
	}


	@Transactional
	protected List<InvestmentReplicationAllocation> saveInvestmentReplicationAllocationActiveList(InvestmentReplication replication, boolean newReplication, boolean dynamicResult,
	                                                                                              List<InvestmentReplicationAllocation> allocationList, Date balanceDate) {
		if (dynamicResult) {
			balanceDate = DateUtils.clearTime(balanceDate);
		}
		Date dynamicResultEndDate = DateUtils.getNextWeekday(balanceDate);

		// get existing allocations so that we know what to end
		List<InvestmentReplicationAllocation> saveList = new ArrayList<>();
		List<InvestmentReplicationAllocation> oldAllocations = null;
		if (!newReplication) {
			oldAllocations = getInvestmentReplicationAllocationListByReplicationActive(replication.getId(), balanceDate, dynamicResult);
		}

		for (InvestmentReplicationAllocation allocation : CollectionUtils.getIterable(allocationList)) {
			allocation.setReplication(replication);
			allocation.setDynamicResult(dynamicResult);
			// Dynamic Allocation Results may point to the original id for some properties, but for saves, clear it
			if (dynamicResult) {
				allocation.setId(null);
			}

			// POPULATE START/END DATES - ONLY IF IT IS NOT A NEW REPLICATION
			if (!newReplication) {
				InvestmentReplicationAllocation originalAllocation = getOriginalAllocationFromList(allocation, oldAllocations);
				// New Allocation on an existing replication - set start date
				if (originalAllocation == null) {
					allocation.setStartDate(balanceDate);
					if (dynamicResult) {
						allocation.setEndDate(dynamicResultEndDate);
					}
				}
				else {
					if (dynamicResult) {
						allocation.setId(originalAllocation.getId());
						allocation.setStartDate(originalAllocation.getStartDate());
						if (DateUtils.isDateAfter(dynamicResultEndDate, originalAllocation.getEndDate())) {
							allocation.setEndDate(dynamicResultEndDate);
						}
						else {
							allocation.setEndDate(originalAllocation.getEndDate());
						}
						// Needs to be the same to update existing bean so save doesn't throw concurrent update exception
						allocation.setRv(originalAllocation.getRv());
					}
					if (isAllocationPropsDifferent(originalAllocation, allocation)) {
						originalAllocation.setEndDate(balanceDate);
						saveList.add(originalAllocation);
						allocation.setId(null);
						allocation.setStartDate(balanceDate);
					}
					// Otherwise - no changes - leave it as is
				}
			}
			saveList.add(allocation);
		}
		// Go through each allocation in original and save list - if original is not in save list - end it
		for (InvestmentReplicationAllocation originalAllocation : CollectionUtils.getIterable(oldAllocations)) {
			if (CollectionUtils.isEmpty(BeanUtils.filter(saveList, InvestmentReplicationAllocation::getId, originalAllocation.getId()))) {
				originalAllocation.setEndDate(balanceDate);
				saveList.add(originalAllocation);
			}
		}
		getInvestmentReplicationAllocationDAO().saveList(saveList);
		return allocationList;
	}


	/**
	 * @return true if the <code>newAllocation</code> has different properties than the <code>originalAllocation</code> (excluding endDate).
	 */
	private boolean isAllocationPropsDifferent(InvestmentReplicationAllocation originalAllocation, InvestmentReplicationAllocation newAllocation) {
		return !CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualProperties(newAllocation, originalAllocation, false, "endDate"));
	}


	private InvestmentReplicationAllocation getOriginalAllocationFromList(InvestmentReplicationAllocation newAllocation, List<InvestmentReplicationAllocation> originalList) {
		if (newAllocation.isNewBean()) {
			// Match by result - for dynamic replications no id doesn't mean it's really a new allocation
			if (newAllocation.isDynamicResult()) {
				// Match by the Replication Allocation Label - i.e. Instrument + Security + SecurityGroup + Current Security Calculator
				// Return the first one sorted by allocation id desc as if you process multiple in a row the time stamp may not be enough
				return CollectionUtils.getFirstElement(BeanUtils.sortWithFunction(BeanUtils.filter(originalList, InvestmentReplicationAllocation::getAllocationUniqueString, newAllocation.getAllocationUniqueString()), InvestmentReplicationAllocation::getId, false));
			}
			return null;
		}
		// Otherwise match by id
		return CollectionUtils.getOnlyElement(BeanUtils.filter(originalList, InvestmentReplicationAllocation::getId, newAllocation.getId()));
	}


	@Override
	public InvestmentReplicationAllocation saveInvestmentReplicationAllocation(InvestmentReplicationAllocation bean) {
		return getInvestmentReplicationAllocationDAO().save(bean);
	}


	@Override
	public void validateInvestmentReplicationAllocationSecuritySpecificity(InvestmentReplicationAllocation replicationAllocation) {
		ValidationUtils.assertNotNull(replicationAllocation, "Replication Allocation is required.");
		ValidationUtils.assertFalse(replicationAllocation.isSecurityUnderlying() && replicationAllocation.getReplicationSecurity() == null,
				"Replication Security must be specified when Security Is Underlying.");
	}


	@Override
	public List<InvestmentSecurity> getInvestmentReplicationAllocationSecurityList(InvestmentReplicationAllocation replicationAllocation, Date date) {
		validateInvestmentReplicationAllocationSecuritySpecificity(replicationAllocation);
		return getActiveSecuritiesForAllocation(replicationAllocation, date);
	}


	private List<InvestmentSecurity> getActiveSecuritiesForAllocation(InvestmentReplicationAllocation replicationAllocation, Date date) {
		SecuritySearchForm searchForm = new SecuritySearchForm();
		if (replicationAllocation.getReplicationSecurity() != null) {
			if (replicationAllocation.isSecurityUnderlying()) {
				searchForm.setUnderlyingSecurityId(replicationAllocation.getReplicationSecurity().getId());
			}
			else {
				return CollectionUtils.createList(replicationAllocation.getReplicationSecurity());
			}
		}
		InvestmentSecurityGroup securityGroup = replicationAllocation.getReplicationSecurityGroup();
		InvestmentInstrument instrument = replicationAllocation.getReplicationInstrument();
		searchForm.setActiveOnDate(date);
		searchForm.setInstrumentId(instrument != null ? instrument.getId() : null);
		searchForm.setSecurityGroupId(securityGroup != null ? securityGroup.getId() : null);
		return getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
	}


	@Override
	public boolean isInvestmentReplicationAllocationSecurityValid(InvestmentReplicationAllocation allocation, Date date, InvestmentSecurity security) {
		List<InvestmentSecurity> securityList = getInvestmentReplicationAllocationSecurityList(allocation, date, CollectionUtils.createList(security));
		return securityList != null && securityList.contains(security);
	}


	@Override
	public List<InvestmentSecurity> getInvestmentReplicationAllocationSecurityList(InvestmentReplicationAllocation allocation, Date date, List<InvestmentSecurity> securityList) {
		if (allocation.getReplicationSecurity() != null && !allocation.isSecurityUnderlying()) {
			return CollectionUtils.createList(allocation.getReplicationSecurity());
		}

		return CollectionUtils.getIntersection(securityList, getInvestmentReplicationAllocationSecurityList(allocation, date));
	}

	////////////////////////////////////////////////////////////////////////////
	///////        InvestmentReplicationType Business Methods           ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentReplicationType getInvestmentReplicationType(short id) {
		return getInvestmentReplicationTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public InvestmentReplicationType getInvestmentReplicationTypeByName(String name) {
		return getInvestmentReplicationTypeCache().getBeanForKeyValueStrict(getInvestmentReplicationTypeDAO(), name);
	}


	@Override
	public List<InvestmentReplicationType> getInvestmentReplicationTypeList() {
		return getInvestmentReplicationTypeDAO().findAll();
	}


	@Override
	public List<InvestmentReplicationType> getInvestmentReplicationTypeList(InvestmentReplicationTypeSearchForm searchForm) {
		return getInvestmentReplicationTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public InvestmentReplicationType saveInvestmentReplicationType(InvestmentReplicationType bean) {
		return getInvestmentReplicationTypeDAO().save(bean);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<InvestmentReplication, Criteria> getInvestmentReplicationDAO() {
		return this.investmentReplicationDAO;
	}


	public void setInvestmentReplicationDAO(AdvancedUpdatableDAO<InvestmentReplication, Criteria> investmentReplicationDAO) {
		this.investmentReplicationDAO = investmentReplicationDAO;
	}


	public AdvancedUpdatableDAO<InvestmentReplicationAllocation, Criteria> getInvestmentReplicationAllocationDAO() {
		return this.investmentReplicationAllocationDAO;
	}


	public void setInvestmentReplicationAllocationDAO(AdvancedUpdatableDAO<InvestmentReplicationAllocation, Criteria> investmentReplicationAllocationDAO) {
		this.investmentReplicationAllocationDAO = investmentReplicationAllocationDAO;
	}


	public AdvancedUpdatableDAO<InvestmentReplicationType, Criteria> getInvestmentReplicationTypeDAO() {
		return this.investmentReplicationTypeDAO;
	}


	public void setInvestmentReplicationTypeDAO(AdvancedUpdatableDAO<InvestmentReplicationType, Criteria> investmentReplicationTypeDAO) {
		this.investmentReplicationTypeDAO = investmentReplicationTypeDAO;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public DaoNamedEntityCache<InvestmentReplicationType> getInvestmentReplicationTypeCache() {
		return this.investmentReplicationTypeCache;
	}


	public void setInvestmentReplicationTypeCache(DaoNamedEntityCache<InvestmentReplicationType> investmentReplicationTypeCache) {
		this.investmentReplicationTypeCache = investmentReplicationTypeCache;
	}
}
