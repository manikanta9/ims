package com.clifton.investment.replication;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.replication.history.InvestmentReplicationHistoryService;
import com.clifton.investment.replication.history.search.InvestmentReplicationHistorySearchForm;
import org.springframework.stereotype.Component;


/**
 * On updates to replications allows only some replication data to be updated if the replication already has history
 *
 * @author manderson
 */
@Component
public class InvestmentReplicationObserver extends SelfRegisteringDaoObserver<InvestmentReplication> {


	private InvestmentReplicationHistoryService investmentReplicationHistoryService;


	private static final String[] REPLICATION_READ_ONLY_FIELDS = new String[]{"type", "reverseExposureSign", "dynamicCalculatorBean", "rebalanceTriggerType"};


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<InvestmentReplication> dao, DaoEventTypes event, InvestmentReplication bean) {
		if (event.isUpdate()) {
			InvestmentReplication originalBean = getOriginalBean(dao, bean);
			// If there is history, the following fields cannot be changed: type, reverseExposureSign, dynamicCalculatorBean, rebalanceTriggerType
			// Compare on the replication first - if changed, then check history table
			if (!CoreCompareUtils.isEqual(bean, originalBean, REPLICATION_READ_ONLY_FIELDS)) {
				InvestmentReplicationHistorySearchForm searchForm = new InvestmentReplicationHistorySearchForm();
				searchForm.setReplicationId(bean.getId());
				searchForm.setLimit(1);
				if (!CollectionUtils.isEmpty(getInvestmentReplicationHistoryService().getInvestmentReplicationHistoryList(searchForm))) {
					throw new ValidationException("Replication " + bean.getLabel() + " has history associated with it.  The following fields cannot be changed: Replication Type, Reverse Exposure Sign, Dynamic Calculator Bean, Rebalance Trigger Type");
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationHistoryService getInvestmentReplicationHistoryService() {
		return this.investmentReplicationHistoryService;
	}


	public void setInvestmentReplicationHistoryService(InvestmentReplicationHistoryService investmentReplicationHistoryService) {
		this.investmentReplicationHistoryService = investmentReplicationHistoryService;
	}
}
