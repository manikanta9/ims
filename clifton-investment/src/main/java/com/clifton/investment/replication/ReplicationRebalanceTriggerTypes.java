package com.clifton.investment.replication;


/**
 * The <code>ReplicationRebalanceTriggerTypes</code> ...
 *
 * @author manderson
 */
public enum ReplicationRebalanceTriggerTypes {

	PROPORTIONAL, // Proportional means percentage entered is a percentage of the target i.e. 10% of 20% target = 2%
	FIXED // Fixed means percentage entered is absolute i.e. 10% = 10%
}
