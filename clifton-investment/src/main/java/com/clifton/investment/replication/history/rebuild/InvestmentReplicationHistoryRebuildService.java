package com.clifton.investment.replication.history.rebuild;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.util.status.Status;
import com.clifton.investment.replication.history.InvestmentReplicationHistory;

import java.util.Date;


/**
 * @author manderson
 */
public interface InvestmentReplicationHistoryRebuildService {


	public Status rebuildInvestmentReplicationHistory(InvestmentReplicationHistoryRebuildCommand command);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Called during portfolio processing, etc.  Gets the history result - if it exists, if it's missing then will attempt to rebuild it
	 */
	@DoNotAddRequestMapping
	public InvestmentReplicationHistory getOrBuildInvestmentReplicationHistory(int replicationId, Date balanceDate);
}
