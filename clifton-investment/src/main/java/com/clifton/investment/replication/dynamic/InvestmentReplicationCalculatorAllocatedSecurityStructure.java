package com.clifton.investment.replication.dynamic;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;
import com.clifton.investment.instrument.structure.calculators.BaseInvestmentStructureCurrentWeightCalculator;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentReplicationCalculatorAllocatedSecurityStructure</code> calculator
 * allows "merging" specified percentages of multiple security structures into one result.
 * <p>
 * Example: 50 DJUBS / 50 GSCI - Threshold 0.5%
 * 1. Calculate DJUBS weights
 * 2. Calculate GSCI weights
 * 3. Take 50% of DJUBS + 50% of GSCI
 * 4. Remove anything less than threshold (0.5%) and allocate proportionally to same sector
 * 5. Return Replication Allocation Security Config with merged weights from above using "current security" from DJ-UBS (1st allocation)
 * <p>
 * Note: Allocation Validation is same as Calculator for 1 Security Structure with the exception that it applies to 2 or more allocations which is valid because of getter override to return 2 for min and null for max allocation list size.
 *
 * @author manderson
 */
public class InvestmentReplicationCalculatorAllocatedSecurityStructure extends InvestmentReplicationCalculatorSecurityStructure {

	public static final int DEFAULT_WEIGHT_PRECISION = BaseInvestmentStructureCurrentWeightCalculator.DEFAULT_WEIGHT_PRECISION;
	private static final BigDecimal DEFAULT_THRESHOLD_PERCENTAGE = BigDecimal.valueOf(0.5);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * If true, anything excluded because falls below threshold will be applied proportionally to all in the same sector
	 * If false, will apply proportionally across all
	 */
	private boolean applyUnderThresholdProportionallySameSector;

	/**
	 * Defaults to 0.5% as the threshold.
	 */
	private BigDecimal thresholdPercentage;

	/**
	 * Allows users to override if they want more precision in the weight calculations
	 */
	private Integer weightPrecision;

	/**
	 * Comma delimited list of instrument identifierPrefix:identifierPrefix to merge
	 * from/to
	 * Example: 50/50 DJUBS F2, GSCI F2 merge, but we use only one copper (HG) so the LP copper from GSCI calculates
	 * it's weight, but it's then merged into the HG final weight
	 * NOTE: Only Applies if the merge from is not part of the main allocation and the merge to IS part of the main allocation
	 */
	private String mergeInstrumentList;
	/**
	 * Generated from the mergeInstrumentList - setup once the first time it's called
	 */
	private Map<String, String> mergeInstrumentMap;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected Integer getMinAllocationListSize() {
		return 2;
	}


	@Override
	protected Integer getMaxAllocationListSize() {
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentReplicationAllocation> calculateImpl(InvestmentReplication bean, Date balanceDate) {

		// First one is the "main one" so use that as the standard list
		InvestmentReplicationAllocation mainAllocation = BeanUtils.sortWithFunction(bean.getAllocationList(), InvestmentReplicationAllocation::getOrder, true).get(0);
		List<InvestmentSecurityStructureAllocation> securityAllocationList = getSecurityAllocationListForReplicationAllocation(mainAllocation, balanceDate);

		for (InvestmentReplicationAllocation alloc : CollectionUtils.getIterable(bean.getAllocationList())) {
			if (MathUtils.isEqual(mainAllocation.getId(), alloc.getId())) {
				continue;
			}
			// Get this allocation's weights and merge them in the final list
			List<InvestmentSecurityStructureAllocation> allocationSecurityAllocationList = getSecurityAllocationListForReplicationAllocation(alloc, balanceDate);
			mergeSecurityStructureAllocationLists(securityAllocationList, allocationSecurityAllocationList);
		}

		// Remove where < threshold and apply proportionally back in - returns a clean list
		securityAllocationList = cleanUpSecurityStructureAllocationList(securityAllocationList);

		// Final Rounding - may have already been done within the sector within cleanup method, but overall needs to be cleaned up
		BigDecimal total = CoreMathUtils.sumProperty(securityAllocationList, InvestmentSecurityStructureAllocation::getAllocationWeight);
		for (InvestmentSecurityStructureAllocation alloc : CollectionUtils.getIterable(securityAllocationList)) {
			alloc.setAllocationWeight(MathUtils.round(alloc.getAllocationWeight(), getCoalesceWeightPrecision()));
		}
		CoreMathUtils.applySumPropertyDifference(securityAllocationList, InvestmentSecurityStructureAllocation::getAllocationWeight, InvestmentSecurityStructureAllocation::setAllocationWeight, total, true, a -> true);

		List<InvestmentReplicationAllocation> repAllocList = new ArrayList<>();
		for (InvestmentSecurityStructureAllocation structureAllocation : CollectionUtils.getIterable(securityAllocationList)) {
			// Create Final using main allocation as the reference
			InvestmentReplicationAllocation securityAllocation = createReplicationAllocation(mainAllocation, structureAllocation);
			repAllocList.add(securityAllocation);
		}
		return repAllocList;
	}


	@Override
	protected List<InvestmentSecurityStructureAllocation> getSecurityAllocationListForReplicationAllocation(InvestmentReplicationAllocation allocation, Date balanceDate) {
		List<InvestmentSecurityStructureAllocation> securityAllocationList = super.getSecurityAllocationListForReplicationAllocation(allocation, balanceDate);
		for (InvestmentSecurityStructureAllocation securityAlloc : CollectionUtils.getIterable(securityAllocationList)) {
			securityAlloc.setAllocationWeight(MathUtils.getPercentageOf(allocation.getAllocationWeight(), securityAlloc.getAllocationWeight(), true));
		}
		// Ensure rounding issues are handled within each
		CoreMathUtils.applySumPropertyDifference(securityAllocationList, InvestmentSecurityStructureAllocation::getAllocationWeight, InvestmentSecurityStructureAllocation::setAllocationWeight, allocation.getAllocationWeight(), true, a -> true);
		return securityAllocationList;
	}


	private void mergeSecurityStructureAllocationLists(List<InvestmentSecurityStructureAllocation> primaryList, List<InvestmentSecurityStructureAllocation> secondaryList) {
		for (InvestmentSecurityStructureAllocation secondarySecurityAllocation : CollectionUtils.getIterable(secondaryList)) {
			boolean found = false;
			InvestmentInstrument instrument = secondarySecurityAllocation.getInstrumentAssignment();
			if (instrument != null) {
				String applyToInstrument = null;
				if (getMergeInstrumentMap().containsKey(instrument.getIdentifierPrefix())) {
					applyToInstrument = getMergeInstrumentMap().get(instrument.getIdentifierPrefix());
				}
				for (InvestmentSecurityStructureAllocation primarySecurityAllocation : CollectionUtils.getIterable(primaryList)) {
					if (applyToInstrument != null) {
						if (CompareUtils.isEqual(applyToInstrument, primarySecurityAllocation.getInstrumentAssignment().getIdentifierPrefix())) {
							found = true;
							primarySecurityAllocation.setAllocationWeight(MathUtils.add(primarySecurityAllocation.getAllocationWeight(), secondarySecurityAllocation.getAllocationWeight()));
						}
					}
					else {
						if (MathUtils.isEqual(instrument.getId(), primarySecurityAllocation.getInstrumentAssignment().getId())) {
							found = true;
							primarySecurityAllocation.setAllocationWeight(MathUtils.add(primarySecurityAllocation.getAllocationWeight(), secondarySecurityAllocation.getAllocationWeight()));
						}
					}
					if (found) {
						break;
					}
				}
			}
			// If not on the primary one - add it
			if (!found) {
				primaryList.add(secondarySecurityAllocation);
			}
		}
	}


	private List<InvestmentSecurityStructureAllocation> cleanUpSecurityStructureAllocationList(List<InvestmentSecurityStructureAllocation> securityAllocationList) {
		// No threshold - nothing to filter out - return full list
		BigDecimal threshold = getCoalesceThresholdPercentage();
		if (MathUtils.isNullOrZero(threshold)) {
			return securityAllocationList;
		}

		List<InvestmentSecurityStructureAllocation> keepList = new ArrayList<>();

		Map<String, BigDecimal> applyProportionalMap = new HashMap<>();
		for (InvestmentSecurityStructureAllocation alloc : CollectionUtils.getIterable(securityAllocationList)) {
			// Using abs in case allocated -50% and -50%
			if (MathUtils.isLessThan(MathUtils.abs(alloc.getAllocationWeight()), threshold)) {
				BigDecimal sectorAmount = (applyProportionalMap.getOrDefault(alloc.getSector(), BigDecimal.ZERO));
				applyProportionalMap.put(alloc.getSector(), MathUtils.add(sectorAmount, alloc.getAllocationWeight()));
			}
			else {
				keepList.add(alloc);
			}
		}
		if (!applyProportionalMap.isEmpty()) {
			BigDecimal totalToAdd = CoreMathUtils.sum(applyProportionalMap.values());
			BigDecimal totalApplied = CoreMathUtils.sumProperty(keepList, InvestmentSecurityStructureAllocation::getAllocationWeight);

			if (isApplyUnderThresholdProportionallySameSector()) {

				for (Map.Entry<String, BigDecimal> stringBigDecimalEntry : applyProportionalMap.entrySet()) {
					List<InvestmentSecurityStructureAllocation> sectorList = BeanUtils.filter(keepList, InvestmentSecurityStructureAllocation::getSector, stringBigDecimalEntry.getKey());
					BigDecimal sectorTotal = CoreMathUtils.sumProperty(sectorList, InvestmentSecurityStructureAllocation::getAllocationWeight);
					for (InvestmentSecurityStructureAllocation alloc : CollectionUtils.getIterable(keepList)) {
						if ((stringBigDecimalEntry.getKey()).equals(alloc.getSector())) {
							alloc.setAllocationWeight(MathUtils.add(alloc.getAllocationWeight(),
									MathUtils.getPercentageOf(CoreMathUtils.getPercentValue(alloc.getAllocationWeight(), sectorTotal, true), stringBigDecimalEntry.getValue(), true)));
							alloc.setAllocationWeight(MathUtils.round(alloc.getAllocationWeight(), getCoalesceWeightPrecision()));
						}
					}
					CoreMathUtils.applySumPropertyDifference(sectorList, InvestmentSecurityStructureAllocation::getAllocationWeight, InvestmentSecurityStructureAllocation::setAllocationWeight, MathUtils.add(stringBigDecimalEntry.getValue(), sectorTotal), true, a -> true);
				}
			}
			else {
				for (InvestmentSecurityStructureAllocation alloc : CollectionUtils.getIterable(keepList)) {
					alloc.setAllocationWeight(MathUtils.add(alloc.getAllocationWeight(), MathUtils.getPercentageOf(CoreMathUtils.getPercentValue(alloc.getAllocationWeight(), totalApplied, true), totalToAdd, true)));
				}
			}
		}
		return keepList;
	}


	private Map<String, String> getMergeInstrumentMap() {
		if (this.mergeInstrumentMap == null) {
			Map<String, String> result = new HashMap<>();
			String mergeList = getMergeInstrumentList();
			if (!StringUtils.isEmpty(mergeList)) {
				String[] mapItems = mergeList.split(",");
				if (mapItems.length > 0) {
					for (String mapItem : mapItems) {
						String[] mapElement = mapItem.split(":");
						if (mapElement.length == 2) {
							String from = mapElement[0];
							String to = mapElement[1];
							if (from != null && to != null) {
								result.put(from.trim(), to.trim());
							}
						}
					}
				}
			}
			this.mergeInstrumentMap = result;
		}
		return this.mergeInstrumentMap;
	}


	private BigDecimal getCoalesceThresholdPercentage() {
		return ObjectUtils.coalesce(getThresholdPercentage(), DEFAULT_THRESHOLD_PERCENTAGE);
	}


	public Integer getCoalesceWeightPrecision() {
		return ObjectUtils.coalesce(getWeightPrecision(), DEFAULT_WEIGHT_PRECISION);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isApplyUnderThresholdProportionallySameSector() {
		return this.applyUnderThresholdProportionallySameSector;
	}


	public void setApplyUnderThresholdProportionallySameSector(boolean applyUnderThresholdProportionallySameSector) {
		this.applyUnderThresholdProportionallySameSector = applyUnderThresholdProportionallySameSector;
	}


	public BigDecimal getThresholdPercentage() {
		return this.thresholdPercentage;
	}


	public void setThresholdPercentage(BigDecimal thresholdPercentage) {
		this.thresholdPercentage = thresholdPercentage;
	}


	public Integer getWeightPrecision() {
		return this.weightPrecision;
	}


	public void setWeightPrecision(Integer weightPrecision) {
		this.weightPrecision = weightPrecision;
	}


	public String getMergeInstrumentList() {
		return this.mergeInstrumentList;
	}


	public void setMergeInstrumentList(String mergeInstrumentList) {
		this.mergeInstrumentList = mergeInstrumentList;
	}
}
