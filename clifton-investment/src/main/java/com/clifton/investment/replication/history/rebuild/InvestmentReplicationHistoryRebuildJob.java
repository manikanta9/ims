package com.clifton.investment.replication.history.rebuild;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;

import java.util.Date;
import java.util.Map;


/**
 * @author manderson
 */
public class InvestmentReplicationHistoryRebuildJob implements Task, StatusHolderObjectAware<Status> {

	private InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService;


	private StatusHolderObject<Status> statusHolderObject;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The job will either be set up for:
	 * false: Run EOD to run for balance date = today
	 * true: Run next weekday (morning) to run for balance date = previous week day
	 */
	private boolean previousWeekday;

	/**
	 * Option to exclude dynamic replications
	 * Would only include "standard" i.e. not dynamic replications
	 * Note: Cannot set both  excludeDynamic and excludeStandard
	 */
	private boolean excludeDynamic;

	/**
	 * Options to exclude standard replications
	 * Would only include dynamic replications
	 * <p>
	 * Note: Cannot set both  excludeDynamic and excludeStandard
	 */
	private boolean excludeStandard;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolderObject.getStatus();

		Date balanceDate = DateUtils.clearTime(new Date());
		if (isPreviousWeekday()) {
			balanceDate = DateUtils.addWeekDays(balanceDate, -1);
		}


		// Common properties
		InvestmentReplicationHistoryRebuildCommand command = new InvestmentReplicationHistoryRebuildCommand();
		command.setSynchronous(true);
		command.setStatus(status);
		command.setStartBalanceDate(balanceDate);
		command.setEndBalanceDate(balanceDate);
		command.setExcludeDynamic(isExcludeDynamic());
		command.setExcludeStandard(isExcludeStandard());
		command.setStatus(status);

		return getInvestmentReplicationHistoryRebuildService().rebuildInvestmentReplicationHistory(command);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationHistoryRebuildService getInvestmentReplicationHistoryRebuildService() {
		return this.investmentReplicationHistoryRebuildService;
	}


	public void setInvestmentReplicationHistoryRebuildService(InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService) {
		this.investmentReplicationHistoryRebuildService = investmentReplicationHistoryRebuildService;
	}


	public StatusHolderObject<Status> getStatusHolderObject() {
		return this.statusHolderObject;
	}


	public boolean isPreviousWeekday() {
		return this.previousWeekday;
	}


	public void setPreviousWeekday(boolean previousWeekday) {
		this.previousWeekday = previousWeekday;
	}


	public boolean isExcludeDynamic() {
		return this.excludeDynamic;
	}


	public void setExcludeDynamic(boolean excludeDynamic) {
		this.excludeDynamic = excludeDynamic;
	}


	public boolean isExcludeStandard() {
		return this.excludeStandard;
	}


	public void setExcludeStandard(boolean excludeStandard) {
		this.excludeStandard = excludeStandard;
	}
}
