package com.clifton.investment.replication.history.cache;


import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.replication.history.InvestmentReplicationHistory;
import com.clifton.investment.replication.history.search.InvestmentReplicationHistorySearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentReplicationHistoryBalanceDateCache</code> caches key ReplicationID_BalanceDate to the history record id that is the last available history for that balance date
 *
 * @author manderson
 */
@Component
public class InvestmentReplicationHistoryBalanceDateCacheImpl extends SelfRegisteringSimpleDaoCache<InvestmentReplicationHistory, String, ObjectWrapper<Integer>> implements InvestmentReplicationHistoryBalanceDateCache {


	private String getBeanKeyForReplicationAndDate(int replicationId, Date date) {
		return replicationId + "_" + DateUtils.fromDateShort(date);
	}


	@Override
	public InvestmentReplicationHistory getInvestmentReplicationHistoryLastForReplicationAndBalanceDate(ReadOnlyDAO<InvestmentReplicationHistory> dao, Integer replicationId, Date balanceDate) {
		ObjectWrapper<Integer> id = getCacheHandler().get(getCacheName(), getBeanKeyForReplicationAndDate(replicationId, balanceDate));
		if (id != null) {
			if (!id.isPresent()) {
				// id is not null, but not present then there is NO history for the replication and date don't look it up in the database again
				return null;
			}
			InvestmentReplicationHistory bean = dao.findByPrimaryKey(id.getObject());
			// If bean is null, then transaction was rolled back, look it up again and reset the cache
			if (bean != null) {
				return bean;
			}
		}
		InvestmentReplicationHistory bean = lookupBean(dao, replicationId, balanceDate);
		setBean(bean, replicationId, balanceDate);
		return bean;
	}


	protected InvestmentReplicationHistory lookupBean(ReadOnlyDAO<InvestmentReplicationHistory> dao, int replicationId, Date balanceDate) {
		InvestmentReplicationHistorySearchForm searchForm = new InvestmentReplicationHistorySearchForm();
		searchForm.setReplicationId(replicationId);
		searchForm.setActiveOnDate(balanceDate);
		searchForm.setOrderBy("createDate:DESC");
		searchForm.setLimit(1);
		List<InvestmentReplicationHistory> list = ((AdvancedReadOnlyDAO<InvestmentReplicationHistory, Criteria>) dao).findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		return CollectionUtils.getFirstElement(list);
	}


	protected void setBean(InvestmentReplicationHistory bean, int replicationId, Date balanceDate) {
		ObjectWrapper<Integer> id = (bean != null ? new ObjectWrapper<>(bean.getId()) : new ObjectWrapper<>());
		getCacheHandler().put(getCacheName(), getBeanKeyForReplicationAndDate(replicationId, balanceDate), id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<InvestmentReplicationHistory> dao, DaoEventTypes event, InvestmentReplicationHistory bean) {
		if (event.isUpdate()) { // Only needed for updates as we only need to clear the dates that have changes (i.e. could be the same result for 3 months and then we add a day, only clear for the day added)
			// Call it so we have the original
			getOriginalBean(dao, bean);
		}
	}


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<InvestmentReplicationHistory> dao, @SuppressWarnings("unused") DaoEventTypes event, InvestmentReplicationHistory bean, Throwable e) {
		if (e == null) {
			InvestmentReplicationHistory originalBean = event.isUpdate() ? getOriginalBean(dao, bean) : null;
			if (originalBean == null) {
				removeFromCacheForDateRange(bean.getReplication().getId(), bean.getStartDate(), bean.getEndDate());
			}
			// On updates just clear for the dates that have changed - updates should only be extending the balance end date so we don't need to clear everything
			else {
				Date minStartDate = DateUtils.isDateBeforeOrEqual(originalBean.getStartDate(), bean.getStartDate(), false) ? originalBean.getStartDate() : bean.getStartDate();
				Date maxStartDate = DateUtils.isDateBeforeOrEqual(originalBean.getStartDate(), bean.getStartDate(), false) ? bean.getStartDate() : originalBean.getStartDate();
				if (!DateUtils.isEqualWithoutTime(minStartDate, maxStartDate)) {
					removeFromCacheForDateRange(bean.getReplication().getId(), minStartDate, DateUtils.getPreviousWeekday(maxStartDate));
				}


				Date minEndDate = DateUtils.isDateAfterOrEqual(originalBean.getEndDate(), bean.getEndDate()) ? bean.getEndDate() : originalBean.getEndDate();
				Date maxEndDate = DateUtils.isDateAfterOrEqual(originalBean.getEndDate(), bean.getEndDate()) ? originalBean.getEndDate() : bean.getEndDate();
				if (!DateUtils.isEqualWithoutTime(minEndDate, maxEndDate)) {
					removeFromCacheForDateRange(bean.getReplication().getId(), DateUtils.getNextWeekday(minEndDate), maxEndDate);
				}
			}
		}
	}


	private void removeFromCacheForDateRange(int replicationId, Date startDate, Date endDate) {
		Date date = startDate;

		while (DateUtils.isDateBeforeOrEqual(date, endDate, false)) {
			getCacheHandler().remove(getCacheName(), getBeanKeyForReplicationAndDate(replicationId, date));
			date = DateUtils.getNextWeekday(date);
		}
	}
}
