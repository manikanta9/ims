package com.clifton.investment.replication.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithTimeSearchForm;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityTargetAdjustmentTypes;

import java.math.BigDecimal;


public class InvestmentReplicationAllocationSearchForm extends BaseAuditableEntityDateRangeWithTimeSearchForm {

	@SearchField(searchField = "replication.id")
	private Integer replicationId;

	@SearchField(searchField = "childReplication.id")
	private Integer childReplicationId;

	@SearchField(searchField = "replicationSecurityGroup.id")
	private Short replicationSecurityGroupId;

	@SearchField(searchField = "replicationInstrument.id")
	private Integer replicationInstrumentId;

	@SearchField(searchField = "replicationSecurity.id")
	private Integer replicationSecurityId;

	@SearchField(searchField = "replicationInstrument.identifierPrefix,replicationInstrument.name,replicationSecurity.symbol,replicationSecurity.name,replicationSecurityGroup.name", leftJoin = true)
	private String allocationLabel;

	@SearchField
	private Short order;

	@SearchField
	private BigDecimal allocationWeight;

	@SearchField
	private BigDecimal durationOverride;

	@SearchField
	private Boolean useBenchmarkDuration;

	@SearchField(searchField = "matchingReplication.id")
	private Integer matchingReplicationId;


	@SearchField(searchField = "currentSecurityCalculatorBean.id")
	private Integer currentSecurityCalculatorBeanId;

	@SearchField
	private InvestmentCurrentSecurityTargetAdjustmentTypes currentSecurityTargetAdjustmentType;

	@SearchField
	private Boolean alwaysIncludeCurrentSecurity;

	@SearchField
	private Boolean portfolioSelectedCurrentSecurity;

	@SearchField
	private BigDecimal rebalanceAllocationMin;

	@SearchField
	private BigDecimal rebalanceAllocationMax;

	@SearchField
	private Boolean dynamicResult;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getReplicationId() {
		return this.replicationId;
	}


	public void setReplicationId(Integer replicationId) {
		this.replicationId = replicationId;
	}


	public Integer getChildReplicationId() {
		return this.childReplicationId;
	}


	public void setChildReplicationId(Integer childReplicationId) {
		this.childReplicationId = childReplicationId;
	}


	public Short getReplicationSecurityGroupId() {
		return this.replicationSecurityGroupId;
	}


	public void setReplicationSecurityGroupId(Short replicationSecurityGroupId) {
		this.replicationSecurityGroupId = replicationSecurityGroupId;
	}


	public Integer getReplicationInstrumentId() {
		return this.replicationInstrumentId;
	}


	public void setReplicationInstrumentId(Integer replicationInstrumentId) {
		this.replicationInstrumentId = replicationInstrumentId;
	}


	public Integer getReplicationSecurityId() {
		return this.replicationSecurityId;
	}


	public void setReplicationSecurityId(Integer replicationSecurityId) {
		this.replicationSecurityId = replicationSecurityId;
	}


	public String getAllocationLabel() {
		return this.allocationLabel;
	}


	public void setAllocationLabel(String allocationLabel) {
		this.allocationLabel = allocationLabel;
	}


	public Short getOrder() {
		return this.order;
	}


	public void setOrder(Short order) {
		this.order = order;
	}


	public BigDecimal getAllocationWeight() {
		return this.allocationWeight;
	}


	public void setAllocationWeight(BigDecimal allocationWeight) {
		this.allocationWeight = allocationWeight;
	}


	public BigDecimal getDurationOverride() {
		return this.durationOverride;
	}


	public void setDurationOverride(BigDecimal durationOverride) {
		this.durationOverride = durationOverride;
	}


	public Boolean getUseBenchmarkDuration() {
		return this.useBenchmarkDuration;
	}


	public void setUseBenchmarkDuration(Boolean useBenchmarkDuration) {
		this.useBenchmarkDuration = useBenchmarkDuration;
	}


	public Integer getMatchingReplicationId() {
		return this.matchingReplicationId;
	}


	public void setMatchingReplicationId(Integer matchingReplicationId) {
		this.matchingReplicationId = matchingReplicationId;
	}


	public Integer getCurrentSecurityCalculatorBeanId() {
		return this.currentSecurityCalculatorBeanId;
	}


	public void setCurrentSecurityCalculatorBeanId(Integer currentSecurityCalculatorBeanId) {
		this.currentSecurityCalculatorBeanId = currentSecurityCalculatorBeanId;
	}


	public BigDecimal getRebalanceAllocationMin() {
		return this.rebalanceAllocationMin;
	}


	public void setRebalanceAllocationMin(BigDecimal rebalanceAllocationMin) {
		this.rebalanceAllocationMin = rebalanceAllocationMin;
	}


	public BigDecimal getRebalanceAllocationMax() {
		return this.rebalanceAllocationMax;
	}


	public void setRebalanceAllocationMax(BigDecimal rebalanceAllocationMax) {
		this.rebalanceAllocationMax = rebalanceAllocationMax;
	}


	public Boolean getDynamicResult() {
		return this.dynamicResult;
	}


	public void setDynamicResult(Boolean dynamicResult) {
		this.dynamicResult = dynamicResult;
	}


	public InvestmentCurrentSecurityTargetAdjustmentTypes getCurrentSecurityTargetAdjustmentType() {
		return this.currentSecurityTargetAdjustmentType;
	}


	public void setCurrentSecurityTargetAdjustmentType(InvestmentCurrentSecurityTargetAdjustmentTypes currentSecurityTargetAdjustmentType) {
		this.currentSecurityTargetAdjustmentType = currentSecurityTargetAdjustmentType;
	}


	public Boolean getAlwaysIncludeCurrentSecurity() {
		return this.alwaysIncludeCurrentSecurity;
	}


	public void setAlwaysIncludeCurrentSecurity(Boolean alwaysIncludeCurrentSecurity) {
		this.alwaysIncludeCurrentSecurity = alwaysIncludeCurrentSecurity;
	}


	public Boolean getPortfolioSelectedCurrentSecurity() {
		return this.portfolioSelectedCurrentSecurity;
	}


	public void setPortfolioSelectedCurrentSecurity(Boolean portfolioSelectedCurrentSecurity) {
		this.portfolioSelectedCurrentSecurity = portfolioSelectedCurrentSecurity;
	}
}
