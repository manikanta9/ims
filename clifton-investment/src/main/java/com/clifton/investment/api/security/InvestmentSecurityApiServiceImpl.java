package com.clifton.investment.api.security;

import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.shared.Security;
import org.springframework.stereotype.Component;


/**
 * @author vgomelsky
 */
@Component
public class InvestmentSecurityApiServiceImpl implements InvestmentSecurityApiService {

	private InvestmentInstrumentService investmentInstrumentService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Security getCurrencyBySymbol(String currencySymbol) {
		InvestmentSecurity currency = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(currencySymbol, true);
		if (currency == null) {
			throw new IllegalArgumentException("Cannot find currency for symbol: " + currencySymbol);
		}
		if (!currency.isCurrency()) {
			throw new IllegalArgumentException("Security is not a Currency for symbol: " + currencySymbol);
		}

		return currency.toSecurity();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
