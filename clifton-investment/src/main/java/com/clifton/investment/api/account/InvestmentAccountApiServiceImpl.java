package com.clifton.investment.api.account;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.shared.ClientAccount;
import com.clifton.investment.shared.HoldingAccount;
import org.springframework.stereotype.Component;


/**
 * @author vgomelsky
 */
@Component
public class InvestmentAccountApiServiceImpl implements InvestmentAccountApiService {

	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ClientAccount getClientAccount(int id) {
		InvestmentAccount clientAccount = getInvestmentAccountService().getInvestmentAccount(id);
		if (clientAccount == null) {
			throw new IllegalArgumentException("Cannot find Client Account with id = " + id);
		}
		if (!clientAccount.getType().isOurAccount()) {
			throw new IllegalArgumentException("Holding Account instead of Client Account found for id = " + id);
		}
		return clientAccount.toClientAccount();
	}


	@Override
	public HoldingAccount getHoldingAccount(int id) {
		InvestmentAccount holdingAccount = getInvestmentAccountService().getInvestmentAccount(id);
		if (holdingAccount == null) {
			throw new IllegalArgumentException("Cannot find Holding Account with id = " + id);
		}
		if (holdingAccount.getType().isOurAccount()) {
			throw new IllegalArgumentException("Client Account instead of Holding Account found for id = " + id);
		}
		return holdingAccount.toHoldingAccount();
	}

	////////////////////////////////////////////////////////////////////////////
	//////                  Getters and Setters                           //////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
