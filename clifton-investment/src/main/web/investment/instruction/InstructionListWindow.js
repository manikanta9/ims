Clifton.investment.instruction.InstructionListWindow = Ext.extend(TCG.app.Window, {
	id: 'investmentInstructionListWindow',
	title: 'Trade & Cash Instructions',
	iconCls: 'fax',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{
				title: 'Instructions',
				items: [{
					xtype: 'investment-instruction-grid'
				}]
			},


			{
				title: 'Instruction Definitions',
				items: [{
					name: 'investmentInstructionDefinitionListFind',
					wikiPage: 'IT/Cash and Trade Settlement Instructions',
					xtype: 'gridpanel',
					instructions: 'Instruction definitions define the subset of data for a category, the instruction output as well as which report to use when generating instruction letters.  Each entity in a table can apply to only one definition, and only Active definitions generate instructions. When an instruction is generated, definition\' Selector Bean is used to lookup relevant entities as well as identify the recipient.',
					groupField: 'category.name',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Definitions" : "Definition"]}',

					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Investment Instruction Tags'},
							{xtype: 'label', html: '&nbsp;&nbsp;'},
							{boxLabel: 'Include Inactive', xtype: 'toolbar-checkbox', name: 'includeInactive'}
						];
					},

					getLoadParams: function(firstLoad) {
						const lp = {};
						const includeInactive = TCG.getChildByName(this.getTopToolbar(), 'includeInactive').getValue();

						if (includeInactive !== true) {
							lp.excludeWorkflowStatusName = 'Inactive';
						}
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							lp.categoryName = 'Investment Instruction Tags';
							lp.categoryHierarchyId = tag.getValue();
						}
						return lp;
					},

					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Category', width: 60, dataIndex: 'category.name', filter: {searchFieldName: 'categoryName'}, hidden: true},
						{header: 'Definition Name', width: 200, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Cash', width: 30, dataIndex: 'category.cash', type: 'boolean', filter: {searchFieldName: 'cash'}},
						{header: 'Client', width: 30, dataIndex: 'category.client', type: 'boolean', filter: {searchFieldName: 'client'}},
						{header: 'Report', width: 150, dataIndex: 'report.name', filter: {searchFieldName: 'reportName'}},
						{header: 'Item Group Populator', width: 150, dataIndex: 'itemGroupPopulatorBean.name', filter: {searchFieldName: 'itemGroupPopulatorBeanName'}},
						{
							header: 'Workflow State', width: 75, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'},
							renderer: function(v, metaData, r) {
								const sts = r.data['workflowStatus.name'];
								if (sts === 'Open' || sts === 'Pending') {
									metaData.css = 'amountAdjusted';
								}
								else if (sts === 'Active') {
									metaData.css = 'amountPositive';
								}
								else {
									metaData.css = 'amountNegative';
								}
								return v;
							}
						},
						{
							header: 'Workflow Status', width: 75, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'},
							renderer: function(v, metaData, r) {
								if (v === 'Open' || v === 'Pending') {
									metaData.css = 'amountAdjusted';
								}
								else if (v === 'Active') {
									metaData.css = 'amountPositive';
								}
								else {
									metaData.css = 'amountNegative';
								}
								return v;
							}
						},
						{
							header: 'Contact Group',
							width: 40,
							dataIndex: 'contactGroup.name',
							hidden: true
						},
						{
							header: 'Delivery Type',
							width: 150,
							dataIndex: 'deliveryType.name',
							filter: {type: 'combo', searchFieldName: 'deliveryTypeId', loadAll: true, url: 'investmentInstructionDeliveryTypeListFind.json'},
							hidden: true
						}
					],
					plugins: {ptype: 'gridsummary'},
					editor: {
						detailPageClass: 'Clifton.investment.instruction.setup.DefinitionWindow',
						copyURL: 'investmentInstructionDefinitionCopy.json',

						addToolbarAddButton: function(toolBar, gridPanel) {
							toolBar.add(new TCG.form.ComboBox({name: 'category', url: 'investmentInstructionCategoryListFind.json', width: 150, listWidth: 200, emptyText: '< Select Category >'}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Create a new definition for selected category',
								iconCls: 'add',
								scope: this,
								handler: function() {
									const category = TCG.getChildByName(toolBar, 'category');
									const categoryId = category.getValue();
									if (TCG.isBlank(categoryId)) {
										TCG.showError('You must first select desired category from the list.');
									}
									else {
										const categoryObj = TCG.data.getData('investmentInstructionCategory.json?id=' + categoryId, this);
										const cmpId = TCG.getComponentId(this.getDetailPageClass());
										TCG.createComponent(this.getDetailPageClass(), {
											id: cmpId,
											defaultData: {category: categoryObj},
											openerCt: gridPanel,
											defaultIconCls: gridPanel.getWindow().iconCls
										});
									}
								}
							});
							toolBar.add('-');
						},
						addEditButtons: function(t, gridPanel) {
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
							t.add({
								text: 'Revise',
								tooltip: 'For Active Definitions, create a new definition that is a revision of the selected definition (Copies selected definition and sets parent field to trace revisions)',
								iconCls: 'pencil',
								scope: this,
								handler: function() {
									this.revisionHandler(gridPanel);
								}
							});
							t.add('-');
						},
						revisionHandler: function(gridPanel) {
							const sm = gridPanel.grid.getSelectionModel();
							if (sm.getCount() === 0) {
								TCG.showError('Please select a row to revise.', 'No Row(s) Selected');
							}
							else if (sm.getCount() !== 1) {
								TCG.showError('Multi-selection revisions are not supported.  Please select one row.', 'NOT SUPPORTED');
							}
							else {
								if (sm.getSelected().get('workflowState.name') !== 'Active') {
									TCG.showError('Selected definition is not currently active.  You can only revise an active definition.  To copy a definition please use the copy button', 'Invalid Selection');
									return;
								}
								Ext.Msg.confirm('Revise Selected', 'Are you sure you would like to revise selected definition?  Selected definition will be moved to Pending Revision state and will be de-activated when the new definition is Approved.<br><br><b>NOTE:</b> Please use the Copy button if you just want to copy the definition.', function(a) {
									if (a === 'yes') {
										const params = {id: sm.getSelected().id};
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Creating Revision...',
											params: params,
											conf: params,
											onLoad: function(record, conf) {
												gridPanel.reload();
											}
										});
										loader.load('investmentInstructionDefinitionRevise.json');
									}
								});
							}
						}
					}
				}]
			},


			{
				title: 'Instruction Categories',
				items: [{
					name: 'investmentInstructionCategoryListFind',
					wikiPage: 'IT/Cash and Trade Settlement Instructions',
					xtype: 'gridpanel',
					instructions: 'Categories are defined for a table in the system and define how we instruct on a particular entity.  We can have multiple categories for a given table so that we can support both instructing both ways (client vs. counterparty) as well as instruct on different parts of the entity cash vs. securities. The only restriction is that an entity can be assigned to only one definition in the category.',
					groupField: 'table.label',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Categories" : "Category"]}',

					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Table', width: 75, dataIndex: 'table.label', filter: {searchFieldName: 'tableName'}, hidden: true, defaultSortColumn: true},
						{header: 'Category Name', width: 75, dataIndex: 'name'},
						{header: 'Date Label', width: 40, dataIndex: 'dateLabel'},
						{header: 'Cash', width: 25, dataIndex: 'cash', type: 'boolean'},
						{header: 'Client', width: 25, dataIndex: 'client', type: 'boolean'},
						{header: 'Required', width: 25, dataIndex: 'required', type: 'boolean'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Selector Bean Type', width: 100, dataIndex: 'selectorBeanType.name', filter: {searchFieldName: 'selectorBeanTypeName'}, hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instruction.setup.CategoryWindow'
					}
				}]
			},


			{
				title: 'Instruction Contact Groups',
				items: [{
					name: 'investmentInstructionContactGroupListFind',
					instructions: 'Instructions are usually sent to multiple parties. Contact Groups define named collections of contacts that can be shared across multiple definitions. An instruction is only sent to group\'s contacts that have recipient company match this on instruction.',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 30, dataIndex: 'name'},
						{header: 'Group Label', width: 30, dataIndex: 'label'},
						{header: 'Group Description', width: 100, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instruction.setup.ContactGroupWindow',
						getDeleteURL: function() {
							return 'investmentInstructionContactGroupDelete.json';
						}
					}
				}]
			},


			{
				title: 'Instruction Contacts',
				items: [{
					name: 'investmentInstructionContactListFind',
					instructions: 'Instruction Contacts define the contact(s) that an instruction will be sent to using the following matching criteria: contact\'s recipient company must match this on instruction and contact\'s group must match this on instruction definition. Contacts may be added or removed in the definition window.',
					wikiPage: 'IT/Instruction+Contacts',
					xtype: 'gridpanel',
					groupField: 'group.name',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Company', width: 35, dataIndex: 'company.labelExpanded', filter: {searchFieldName: 'companyLabel'}},
						{header: 'Group Name', width: 15, dataIndex: 'group.name'},
						{header: 'Destination Type', width: 15, dataIndex: 'destinationSystemBean.type.name', filter: {searchFieldName: 'destinationSystemBeanTypeName'}},
						{header: 'Destination', width: 35, dataIndex: 'destinationSystemBean.description', filter: {searchFieldName: 'destinationSystemBeanDescription'}},
						{header: 'Start Date', width: 10, dataIndex: 'startDate'},
						{header: 'End Date', width: 10, dataIndex: 'endDate'},
						{header: 'Active', width: 10, dataIndex: 'active', type: 'boolean'}
					],
					editor: {
						addEnabled: false,
						deleteEnabled: false,
						detailPageClass: 'Clifton.investment.instruction.setup.ContactWindow'
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('active', true);
						}
					}
				}]
			},


			{
				title: 'Instruction Runs',
				items: [{
					instructions: 'An Instruction Run is a collection of items to be sent to the assigned contacts. To create a run select an instruction or item and click the send button.',
					xtype: 'investment-instruction-run-grid',
					columnOverrides: [
						{dataIndex: 'detached', hidden: true},
						{dataIndex: 'scheduledDate', hidden: false}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('startDate', {'after': new Date().add(Date.DAY, -10)});
						}
					}
				}]
			},


			{
				title: 'Missing Instructions',
				items: [{
					xtype: 'gridpanel',
					instructions: 'The following entities are missing an instruction.',
					name: 'investmentInstructionSelectionMissingListFind',
					standardColumns: [],
					useBufferView: false, // allow cell wrapping
					groupField: 'recipientCompany.name',
					groupTextTpl: 'Recipient: {[values.group == undefined ? "Unknown" : values.group]} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})',

					isPagingEnabled: function() {
						return false;
					},
					columns: [
						{header: 'Recipient', dataIndex: 'recipientCompany.name', width: 200, filter: false, hidden: true},
						{header: 'RecipientCompanyID', dataIndex: 'recipientCompany.id', width: 5, filter: false, hidden: true},
						{
							header: '', width: 10, filter: false, sortable: false, dataIndex: 'errorMessage',
							renderer: function(v, metaData, r) {
								if (TCG.isNotBlank(r.data.errorMessage)) {
									const imgCls = 'flag-red';
									const tooltip = '<b>Instruction can not be fully generated:</b><br/>' + r.data['errorMessage'];
									return '<span ext:qtip="' + tooltip + '"><span style="width: 16px; height: 16px; float: left;" class="' + imgCls + '" ext:qtip="' + tooltip + '">&nbsp;</span> </span>';
								}
							}
						},
						{header: 'FKFieldID', dataIndex: 'fkFieldId', type: 'int', width: 70, filter: false, hidden: true},
						{header: 'Client Account', dataIndex: 'clientAccount.label', width: 150, filter: {searchFieldName: 'clientAccountLabel'}, sortable: false},
						{header: 'Holding Account', dataIndex: 'holdingAccount.label', width: 150, filter: {searchFieldName: 'holdingAccountLabel'}, sortable: false},
						{header: 'Security', dataIndex: 'security.symbol', width: 75, filter: {searchFieldName: 'securityLabel'}, sortable: false},
						{header: 'Booked', dataIndex: 'booked', width: 50, type: 'boolean', sortable: false},
						{header: 'Description', dataIndex: 'description', width: 150, sortable: false}
					],
					plugins: {ptype: 'gridsummary'},

					editor: {
						drillDownOnly: true,
						getDetailPageClass: function(grid, row) {
							const cat = TCG.getChildByName(grid.ownerGridPanel.getTopToolbar(), 'categoryId').getValue();
							if (TCG.isNotBlank(cat)) {
								const catObj = TCG.data.getData('investmentInstructionCategory.json?id=' + cat, grid, 'investment.instruction.category.' + cat);
								if (TCG.isNotNull(catObj) && catObj.table && catObj.table.detailScreenClass) {
									return catObj.table.detailScreenClass;
								}
							}
							return false;
						},
						getDetailPageId: function(gridPanel, row) {
							return row.json.fkFieldId;
						}
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Category', xtype: 'toolbar-combo', name: 'categoryId', width: 225, url: 'investmentInstructionCategoryListFind.json', allowBlank: false},
							{fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'date', value: Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y'), allowBlank: false}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// Category and Date can be defaulted by the caller of window open
							const dd = this.getWindow().defaultData;
							if (dd && dd.date) {
								TCG.getChildByName(this.getTopToolbar(), 'date').setValue(TCG.parseDate(dd.date).format('m/d/Y'));
							}
							if (dd && dd.categoryName) {
								const gp = this;
								return TCG.data.getDataPromiseUsingCaching('investmentInstructionCategoryByName.json?name=' + dd.categoryName, this, 'investment.instruction.category.' + dd.categoryName)
									.then(function(categoryObject) {
										TCG.getChildByName(gp.getTopToolbar(), 'categoryId').setValue({value: categoryObject.id, text: categoryObject.name});
										// Return LP using not first load so it uses top toolbar set filters
										return gp.getLoadParams(false);
									});
							}
						}

						const cat = TCG.getChildByName(this.getTopToolbar(), 'categoryId').getValue();
						const dt = TCG.getChildByName(this.getTopToolbar(), 'date').getValue();
						if (TCG.isNotBlank(cat)) {
							return {
								readUncommittedRequested: true,
								enableOpenSessionInView: true,
								categoryId: cat,
								date: (TCG.isNotBlank(dt) ? dt.format('m/d/Y') : '')
							};
						}
						return false;
					},
					addFirstToolbarButtons: function(toolBar, gridPanel) {
						toolBar.add({
							text: 'Generate...',
							tooltip: 'Generate missing instructions.',
							iconCls: 'run',
							scope: this,
							handler: function() {
								const dd = {};

								const cat = TCG.getChildByName(this.getTopToolbar(), 'categoryId').getValue();
								if (TCG.isNotBlank(cat)) {
									const catObj = TCG.data.getData('investmentInstructionCategory.json?id=' + cat, toolBar, 'investment.instruction.category.' + cat);
									if (catObj) {
										dd.categoryId = catObj.id;
										dd.categoryName = catObj.name;
									}
								}
								const dt = TCG.getChildByName(this.getTopToolbar(), 'date').getValue();
								if (TCG.isNotBlank(dt)) {
									dd.date = dt.format('Y-m-d 00:00:00');
								}
								TCG.createComponent('Clifton.investment.instruction.InstructionGenerateWindow', {
									defaultData: dd,
									openerCt: gridPanel
								});
							}
						});
						toolBar.add('-');
					}
				}]
			},


			{
				title: 'Administration',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						labelWidth: 150,
						loadValidation: false, // using the form only to get background color/padding
						height: 150,
						buttonAlign: 'right',
						instructions: 'Select criteria below to filter which category and date to process. Processing will create/update corresponding instructions on the specified date. Drill into run status below for full details.',
						listeners: {
							afterrender: function(fp) {
								const f = this.getForm();
								const bd = f.findField('date');
								if (TCG.isBlank(bd.getValue())) {
									const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
									const dateValue = prevBD.format('m/d/Y');
									bd.setValue(dateValue);
								}
							}
						},
						items: [
							{fieldLabel: 'Instruction Category', name: 'categoryName', hiddenName: 'categoryId', xtype: 'combo', loadAll: true, url: 'investmentInstructionCategoryListFind.json', allowBlank: false},
							{fieldLabel: 'Date', xtype: 'datefield', name: 'date', allowBlank: false}
						],

						buttons: [{
							text: 'Process Instructions',
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								form.submit(Ext.applyIf({
									url: 'investmentInstructionListProcess.json',
									waitMsg: 'Processing...',
									success: function(form, action) {
										Ext.Msg.alert('Instructions Processing', action.result.data.message, function() {
											const grid = owner.ownerCt.items.get(1);
											grid.reload(); // uses defaults of every 3s for 30s
										});
									}
								}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
							}
						}]
					},
					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'INVESTMENT-INSTRUCTIONS',
						instantRunner: true,
						instructions: 'The following instructions are being processed right now.',
						title: 'Current Investment Instructions Processing',
						flex: 1
					}
				]
			}
		]
	}]
});
