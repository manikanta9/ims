Clifton.investment.instruction.InstructionItemWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Settlement Instruction Item',
	iconCls: 'fax',
	width: 700,
	height: 300,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		url: 'investmentInstructionItemPopulated.json',
		labelFieldName: 'id',
		readOnly: true,

		items: [
			{fieldLabel: 'Instruction', xtype: 'linkfield', name: 'instruction.label', detailIdField: 'instruction.id', detailPageClass: 'Clifton.investment.instruction.InstructionWindow'},
			{
				fieldLabel: 'Linked Entity', xtype: 'linkfield', name: 'fkFieldLabel', detailIdField: 'fkFieldId',
				getDetailPageClass: function(fp) {
					return fp.getFormValue('instruction.definition.category.table.detailScreenClass');
				}
			},
			{fieldLabel: 'Status', name: 'status.name'},
			{fieldLabel: 'Item Group', name: 'itemGroup.id'},
			{boxLabel: 'Ready', name: 'ready', xtype: 'checkbox', qtip: 'Ready usually indicates the item is booked - but some definition selectors allow other conditions (Booked or (Amount = 0 and Reconciled))'}
		]
	}]
});
