Clifton.investment.instruction.InstructionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Settlement Instruction',
	iconCls: 'fax',
	height: 700,
	width: 950,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				tbar: [{
					text: 'View Report',
					tooltip: 'View report for all items in this instruction.',
					iconCls: 'pdf',
					handler: function() {
						const fp = this.ownerCt.ownerCt.items.get(0);
						const url = 'investmentInstructionReportDownload.json?instructionId=' + fp.getWindow().getMainFormId();
						TCG.downloadFile(url, null, this);
					}
				}],

				items: [
					{
						xtype: 'formpanel',
						labelWidth: 125,
						url: 'investmentInstruction.json',
						listeners: {
							afterload: function(panel) {
								panel.customizeInstructionUI();
							}
						},
						customizeInstructionUI: function() {
							this.setFieldLabel('instructionDateLabel', this.getFormValue('definition.category.dateLabel') + ':');

							const gp = TCG.getChildByName(this.getWindow(), 'investmentInstructionItemListPopulatedFind');
							const grpUsed = TCG.isNotNull(this.getFormValue('definition.itemGroupPopulatorBean.id'));
							if (!grpUsed) {
								gp.grid.getStore().groupBy('', false);
							}
						},
						updateContactGroup: function(contactGroup) {
							const instructionFormValues = this.getWindow().getMainForm().formValues;
							instructionFormValues.definition.contactGroup = contactGroup;
						},
						items: [
							{
								xtype: 'formtable',
								columns: 5,
								items: [
									{html: 'Instruction Category:', cellWidth: '130px'},
									{colspan: 2, name: 'definition.category.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instruction.setup.CategoryWindow', detailIdField: 'definition.category.id', submitDetailIdField: false},
									{html: 'Date:', name: 'instructionDateLabel', cellWidth: '85px'},
									{name: 'instructionDate', xtype: 'datefield', readOnly: true, width: 85, cellWidth: '100px'},

									{html: 'Instruction Definition:'},
									{
										name: 'definition.name',
										xtype: 'linkfield',
										detailPageClass: 'Clifton.investment.instruction.setup.DefinitionWindow',
										detailIdField: 'definition.id',
										submitDetailIdField: false
									},
									{html: ''},
									{html: 'Status:'},
									{name: 'status.name', xtype: 'textfield', width: 85, readOnly: true}
								]
							},
							{fieldLabel: 'Recipient Company', name: 'recipientCompany.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'recipientCompany.id'}
						]
					},

					{
						xtype: 'panel',
						flex: 1,
						layout: 'fit',
						layoutConfig: {align: 'stretch'},
						items: [
							{
								xtype: 'gridpanel',
								name: 'investmentInstructionItemListPopulatedFind',
								rowSelectionModel: 'multiple',
								layout: 'vbox',
								additionalPropertiesToRequest: 'fkFieldId|itemGroup.id',
								instructions: 'The following items were included in this instruction.  Each item in the instruction can be sent/completed individually unless it is grouped with other items.  Grouped items are also grouped in the report.',
								wikiPage: 'IT/Automated+Sending+of+Settlement+Instructions',
								viewConfig: {
									startCollapsed: false,
									processEvent: function(name, e) {
										if (name === 'mousedown' && e && e.target && e.target.getAttribute('class') === 'pdf') {
											const itemGroupId = e.target.getAttribute('itemGroupId');
											const rows = this.grid.store.data.items;
											for (let i = 0; i < rows.length; i++) {
												if (rows[i].json.itemGroup.id === itemGroupId) {
													const url = 'investmentInstructionItemReportDownload.json?instructionItemId=' + rows[i].json.id;
													TCG.downloadFile(url, null, this.grid);
													return;
												}
											}
										}
										else {
											Ext.grid.GroupingView.prototype.processEvent.call(this, name, e);
										}
									}
								},
								cancelCategories: [],
								isCancelledCategory: function(grid) {
									const fp = TCG.getParentTabPanel(grid).getWindow().getMainFormPanel();
									const category = fp.getForm().findField('definition.category.name').getValue();
									return this.cancelCategories.indexOf(category) >= 0;
								},
								setCancelledButtonState: function() {
									const cancel = TCG.getChildByName(this.getTopToolbar(), 'cancelInstruction');
									const selections = this.grid.getSelectionModel().getSelections();
									const enable = (selections.length > 0) && this.isCancelledCategory(this.grid) && selections.every(function(select) {
										return select.json.status.name === 'COMPLETED';
									});
									if (enable === true) {
										cancel.enable();
									}
									else {
										cancel.disable();
									}
								},
								groupField: 'itemGroup.name',
								groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})',
								getGroup: function(v, r, groupRenderer, rowIndex, colIndex, ds) {
									const column = this.cm.config[colIndex];
									let g = groupRenderer ? groupRenderer.call(column.scope, v, r, rowIndex, colIndex, ds) : String(v);
									if (g === '' || g === '&#160;') {
										g = column.emptyGroupText || this.emptyGroupText;
									}
									return g;
								},
								columns: [
									{
										header: 'Group', width: 50, dataIndex: 'itemGroup.name', hidden: true,
										groupRenderer: function(v, r, rowIndex, colIndex, ds) {
											const groupName = r.data['itemGroup.name'];
											if (TCG.isNotBlank(groupName)) {
												return '<span class="action-column" ext:qtip="View Report (Includes All Items In Group)"><span style="width: 16px; height: 16px; float: left;" class="pdf" itemGroupId="' + r.json.itemGroup.id + '" ext:qtip="View Report (Includes All Items In Group)">&nbsp;</span> ' + v + '</span>';
											}
											return v;
										}
									},
									{
										header: '', width: 5,
										renderer: function(v, args, r) {
											if (TCG.isBlank(r.data['itemGroup.name'])) {
												return TCG.renderActionColumn('pdf', '', 'View Report', 'report');
											}
										}
									},
									{header: 'ID', width: 20, dataIndex: 'id', hidden: true},
									{header: 'Source Item ID', width: 30, dataIndex: 'fkFieldId', hidden: true},
									{header: 'Instruction Item', width: 200, dataIndex: 'fkFieldLabel', sortable: false},
									{header: 'Ready', width: 25, dataIndex: 'ready', filter: false, sortable: false, type: 'boolean', tooltip: 'Ready usually indicates the item is booked - but some definition selectors allow other conditions (Booked or (Amount = 0 and Reconciled))'},
									{
										header: 'Status', width: 40, dataIndex: 'status.name', defaultSortColumn: true, defaultSortDirection: 'DESC',
										filter: {searchFieldName: 'instructionItemStatusList', type: 'list', options: Clifton.investment.instruction.StatusList},
										renderer: function(v, m, r) {
											return (Clifton.investment.instruction.renderStatus(v, m, r));
										}
									},
									{header: 'Referenced Item ID', width: 20, dataIndex: 'referencedInvestmentInstructionItem.id', hidden: true}
								],
								gridConfig: {
									listeners: {
										rowclick: function(grid, rowIndex, evt) {
											const gp = TCG.getParentByClass(grid, TCG.grid.GridPanel);
											if (TCG.isActionColumn(evt.target)) {
												const eventName = TCG.getActionColumnEventName(evt);
												const row = grid.store.data.items[rowIndex];
												if (eventName === 'report') {
													const url = 'investmentInstructionItemReportDownload.json?instructionItemId=' + row.json.id;
													TCG.downloadFile(url, null, grid);
												}
											}
											gp.setCancelledButtonState();
										}
									}
								},
								updateCount: function() {
									TCG.grid.GridPanel.prototype.updateCount.call(this, arguments);
									this.setCancelledButtonState();
								},
								getLoadParams: function(firstLoad) {
									//Un-comment if only OPEN items should show in the grid onload
									if (firstLoad) {
										this.setFilterValue('status.name', ['OPEN', 'ERROR', 'SEND', 'SENDING', 'SENT', 'CANCELED', 'PENDING_CANCEL', 'SENT_CANCEL', 'ERROR_CANCEL']);
										this.grid.filters.getFilter('status.name').setActive(true, true);
									}
									return {instructionId: this.getWindow().params.id};
								},
								isPagingEnabled: function() {
									return false;
								},
								editor: {
									addEnabled: false,
									deleteEnabled: false,
									detailPageClass: 'Overridden by function',
									getDetailPageClass: function(grid, row) {
										const fv = TCG.getParentTabPanel(grid).getWindow().getMainForm().formValues;
										return TCG.getValue('definition.category.table.detailScreenClass', fv);
									},
									getDetailPageId: function(gridPanel, row) {
										return row.json.fkFieldId;
									},
									openWindowFromContextMenu: function(grid, rowIndex) {
										const gridPanel = this.getGridPanel();
										const row = grid.store.data.items[rowIndex];
										const status = row.json.status.name;
										const clazz = 'Clifton.investment.instruction.run.RunWindow';
										const id = row.id;
										if (status !== 'OPEN') {
											const loader = new TCG.data.JsonLoader({
												waitTarget: gridPanel,
												params: {
													id: id
												},
												onLoad: function(record, conf) {
													if (record) {
														const runId = record.id;
														const cmpId = TCG.getComponentId(clazz, runId);
														TCG.createComponent(clazz, {
															id: cmpId,
															params: {id: runId},
															openerCt: gridPanel,
															defaultIconCls: gridPanel.getWindow().iconCls
														});
													}
													else {
														TCG.showError('The selected item does not have an associated run.', 'No runs found.');
													}
												}
											});
											loader.load('investmentInstructionRunForItemLatest.json');
										}
										else {
											TCG.showError('The selected item does not have an associated run.', 'No runs found.');
										}
									}
								},
								listeners: {
									afterrender: function(gridPanel) {
										const el = gridPanel.getEl();
										el.on('contextmenu', function(e, target) {
											const g = gridPanel.grid;
											g.contextRowIndex = g.view.findRowIndex(target);
											e.preventDefault();
											if (!g.drillDownMenu) {
												g.drillDownMenu = new Ext.menu.Menu({
													items: [
														{
															text: 'Open Latest Run', iconCls: 'run',
															handler: function() {
																gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex);
															}
														}
													]
												});
											}
											g.drillDownMenu.showAt(e.getXY());
										}, gridPanel);
										TCG.data.getDataPromise('investmentInstructionCategoryListByDestinationType.json?destinationType=Instruction SWIFT&requestedPropertiesRoot=data&requestedProperties=name', gridPanel)
											.then(function(categories) {
												const tmp = [];
												categories.forEach(function(c) {
													if (TCG.isNotBlank(c.name)) {
														tmp.push(c.name);
													}
												});
												gridPanel.cancelCategories = tmp;
											});
									}
								},
								addToolbarButtons: function(toolBar, gridPanel) {
									toolBar.add({
										iconCls: 'expand-all',
										tooltip: 'Expand or Collapse all accounts',
										scope: this,
										handler: function() {
											gridPanel.grid.collapsed = !gridPanel.grid.collapsed;
											gridPanel.grid.view.toggleAllGroups(!gridPanel.grid.collapsed);
										}
									});
									toolBar.add('-');
									toolBar.add({xtype: 'displayfield', value: 'Wire Date:&nbsp;&nbsp;', qtip: 'Sets Wire Date on selected instructions.'});
									toolBar.add({name: 'toolbarWireDate', xtype: 'datefield'});
									toolBar.add('-');
									toolBar.add({
										text: 'Send',
										tooltip: 'Create an Instruction Run and send the selected item(s) to the corresponding Instruction Contact(s)',
										iconCls: 'run',
										scope: this,
										handler: function() {
											const grid = this.grid;
											const sm = grid.getSelectionModel();
											const toolbarWireDate = TCG.getChildByName(toolBar, 'toolbarWireDate');

											const records = sm.getSelections();

											if (records.length === 0) {
												TCG.showError('Please select item(s) to be sent.', 'No item(s) selected.');
											}
											else {
												Ext.Msg.confirm('Send selected item(s)', 'Would you like to send all of the selected item(s)?', function(a) {
													if (a === 'yes') {
														const itemIds = [];
														const errorMessages = [];
														for (let i = 0; i < records.length; i++) {
															const record = records[i];
															if (record.json.status.name === 'OPEN' || record.json.status.name === 'ERROR' || record.json.status.name === 'PENDING_CANCEL' || record.json.status.name === 'ERROR_CANCEL') {
																if (!record.json.ready) {
																	errorMessages.push('&#8226; Item [' + record.id + '] "' + record.json.fkFieldLabel + '" will not be sent because it is not marked as ready. This usually indicates it has not been booked.');
																}
																else {
																	itemIds.push(record.id);
																}
															}
															else {
																errorMessages.push('&#8226; Item[' + record.id + '] "' + record.json.fkFieldLabel + '" will not be sent because it does not have a status of OPEN or ERROR or PENDING_CANCEL.');
															}
														}
														if (itemIds.length > 0) {
															const loader = new TCG.data.JsonLoader({
																waitTarget: gridPanel,
																params: {
																	itemIds: itemIds,
																	wireDate: toolbarWireDate.getValue() ? toolbarWireDate.getValue().format('m/d/Y') : undefined
																},
																onLoad: function(record, conf) {
																	gridPanel.reload();
																}
															});
															loader.load('investmentInstructionItemListSend.json');
														}
														if (errorMessages.length > 0) {
															TCG.showError(errorMessages.join('\<br /\>'), 'Error Occurred');
														}
													}
												});
											}
										}
									});
									toolBar.add('-');
									toolBar.add({
										text: 'Mark Complete',
										tooltip: 'Mark selected instruction item(s) as completed.',
										iconCls: 'row_checked',
										scope: this,
										handler: function() {
											const grid = this.grid;
											const sm = grid.getSelectionModel();
											const toolbarWireDate = TCG.getChildByName(toolBar, 'toolbarWireDate');
											if (sm.getCount() === 0) {
												TCG.showError('Please select an instruction item.', 'No Row(s) Selected');
											}
											else {
												const ids = [];
												const ut = sm.getSelections();
												for (let i = 0; i < ut.length; i++) {
													ids.push(ut[i].json.id);
												}
												const listLoader = new TCG.data.JsonLoader({
													waitTarget: gridPanel,
													params: {
														itemIds: ids,
														wireDate: toolbarWireDate.getValue() ? toolbarWireDate.getValue().format('m/d/Y') : undefined
													},
													onLoad: function(record, conf) {
														gridPanel.reload();
													}
												});
												listLoader.load('investmentInstructionItemListCompleteSave.json');
											}
										}
									});
									toolBar.add('-');
									toolBar.add({
										text: 'Cancel',
										tooltip: 'Mark selected instruction item(s) as cancelled. Instructions must be [COMPLETED] and have a SWIFT destination contact.',
										iconCls: 'cancel',
										scope: this,
										name: 'cancelInstruction',
										disabled: true,
										handler: function() {
											const grid = this.grid;
											const sm = grid.getSelectionModel();
											if (sm.getCount() === 0) {
												TCG.showError('Please select an instruction item.', 'No Row(s) Selected');
											}
											else {
												const errorMessages = [];
												if (!gridPanel.isCancelledCategory(grid)) {
													errorMessages.push('Only [' + gridPanel.cancelCategories + '] Instruction Categories can be cancelled.');
												}
												else {
													const ids = [];
													const ut = sm.getSelections();
													for (let i = 0; i < ut.length; i++) {
														const record = ut[i].json;
														if (record.status.name === 'COMPLETED') {
															ids.push(record.id);
														}
														else {
															errorMessages.push('&#8226; Instruction[' + record.id + '] "' + record.fkFieldLabel + '" will not be cancelled because it does not have a COMPLETED status.');
														}
													}
													if (ids.length > 0) {
														const loader = new TCG.data.JsonLoader({
															waitTarget: gridPanel,
															params: {
																itemIds: ids
															},
															onLoad: function(record, conf) {
																gridPanel.reload();
															}
														});
														loader.load('investmentInstructionItemListCancel.json');
													}
												}

												if (errorMessages.length > 0) {
													TCG.showError(errorMessages.join('\<br /\>'), 'Error Occurred');
												}
											}
										}
									});
									toolBar.add('-');
								},
								configureToolsMenu: function(menu) {
									const gp = this;
									menu.add('-');
									menu.add({
										text: 'Rebuild Groupings',
										iconCls: 'group',
										tooltip: 'Rebuild Groupings for Items in this Instruction - Useful when Group Item Populator on the Definition is Changed.',
										scope: this,
										handler: function() {
											const id = gp.getWindow().getMainFormId();
											const loader = new TCG.data.JsonLoader({
												waitTarget: gp,
												params: {instructionId: id},
												onLoad: function(record, conf) {
													gp.reload();
												}
											});
											loader.load('investmentInstrumentItemGroupsForInstructionProcess.json');
										}
									});
									menu.add({
										text: 'Mark Error',
										iconCls: 'stop',
										tooltip: 'Mark selected instruction item(s) as [ERROR]. Instructions must be [SENDING, SENT or SEND]',
										scope: this,
										handler: function() {
											const grid = this.grid;
											const sm = grid.getSelectionModel();
											if (sm.getCount() === 0) {
												TCG.showError('Please select an instruction item.', 'No Row(s) Selected');
											}
											else {
												const errorMessages = [];
												if (!gp.isCancelledCategory(grid)) {
													errorMessages.push('Only [' + gp.cancelCategories + '] Instruction Categories can be changed to [ERROR].');
												}
												else {
													const ids = [];
													const ut = sm.getSelections();
													for (let i = 0; i < ut.length; i++) {
														const record = ut[i].json;
														if (record.status.name === 'SENDING' || record.status.name === 'SENT' || record.status.name === 'SEND') {
															ids.push(record.id);
														}
														else {
															errorMessages.push('&#8226; Instruction[' + record.id + '] "' + record.fkFieldLabel + '" will not be updated to [ERROR] because it does not have a [SENDING, SENT or SEND] status.');
														}
													}
													if (ids.length > 0) {
														const loader = new TCG.data.JsonLoader({
															waitTarget: gp,
															params: {
																itemIds: ids
															},
															onLoad: function(record, conf) {
																gp.reload();
																Clifton.investment.instruction.run.openInstructionRunErrorWindow(ids, gp);
															}
														});
														loader.load('investmentInstructionItemListErrorSave.json');
													}
												}

												if (errorMessages.length > 0) {
													TCG.showError(errorMessages.join('\<br /\>'), 'Error Occurred');
												}
											}
										}
									});
								}
							}
						]
					}]
			},


			{
				title: 'Recipient Contacts',
				items: [{
					name: 'investmentInstructionContactListFind',
					xtype: 'gridpanel',
					instructions: 'Instruction Contacts define the contact that an instruction will be sent to for a given definition and company. The following contacts come from the contact group of this instruction\'s definition and match on recipient company.',
					wikiPage: 'IT/Instruction+Contacts',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition', width: 20, dataIndex: 'definition.labelExpanded', hidden: true},
						{header: 'Company', width: 20, dataIndex: 'company.labelExpanded', filter: {searchFieldName: 'companyLabel'}},
						{header: 'Destination', width: 20, dataIndex: 'destinationSystemBean.description', filter: {searchFieldName: 'destinationSystemBeanDescription'}},
						{header: 'Destination Type', width: 10, dataIndex: 'destinationSystemBean.type.name', filter: {searchFieldName: 'destinationSystemBeanTypeName'}},
						{header: 'Start Date', width: 10, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 10, dataIndex: 'endDate', hidden: true},
						{header: 'Active', width: 5, dataIndex: 'active', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instruction.setup.ContactWindow',
						getDefaultData: function(gridPanel) {
							const formValues = gridPanel.getWindow().getMainForm().formValues;
							return {
								group: formValues.definition.contactGroup,
								company: formValues.recipientCompany
							};
						}
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('active', true);
						}
						const instructionFormValues = this.getWindow().getMainForm().formValues;
						const contactGroup = instructionFormValues.definition.contactGroup;
						const recipientCompany = instructionFormValues.recipientCompany.id;
						const result = {
							companyId: recipientCompany
						};

						if (contactGroup) {
							result.contactGroupId = contactGroup.id;
						}
						else {
							result.contactGroupId = 0;
							this.grid.store.removeAll();
							return false;
						}
						return result;
					}
				}]
			},


			{
				title: 'Instruction Runs',
				items: [{
					xtype: 'investment-instruction-run-grid',
					editor: {
						addEnabled: false,
						deleteEnabled: false,
						detailPageClass: 'Clifton.investment.instruction.run.RunWindow',
						detailIdField: 'definition.category.id'
					}
				}]
			}
		]
	}]
});
