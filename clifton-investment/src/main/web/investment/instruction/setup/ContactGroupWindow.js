Clifton.investment.instruction.setup.ContactGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Instruction Contact Group',
	iconCls: 'group',
	width: 600,
	height: 400,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Instructions are usually sent to multiple parties. Contact Groups define named collections of contacts that can be shared across multiple definitions. An instruction is only sent to group\'s contacts that have recipient company match this on instruction.',
					labelWidth: 150,
					url: 'investmentInstructionContactGroup.json',
					items: [
						{fieldLabel: 'Group Name', name: 'name'},
						{fieldLabel: 'Group Label', name: 'label'},
						{fieldLabel: 'Group Description', name: 'description', xtype: 'textarea'}
					]
				}]
			},


			{
				title: 'Group Contacts',
				items: [{
					xtype: 'gridpanel',
					name: 'investmentInstructionContactListFind',
					instructions: 'The contacts that are in this group.',
					groupField: 'company.labelExpanded',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Contact Group', width: 20, dataIndex: 'group.name', hidden: true},
						{header: 'Company', width: 30, dataIndex: 'company.labelExpanded', filter: {searchFieldName: 'companyLabel'}, hidden: true},
						{header: 'Destination', width: 30, dataIndex: 'destinationSystemBean.description', filter: {searchFieldName: 'destinationSystemBeanDescription'}},
						{header: 'Destination Type', width: 20, dataIndex: 'destinationSystemBean.type.name', filter: {searchFieldName: 'destinationSystemBeanTypeName'}},
						{header: 'Start Date', width: 10, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 10, dataIndex: 'endDate', hidden: true},
						{header: 'Active', width: 10, dataIndex: 'active', type: 'boolean'}
					],
					isPagingEnabled: function() {
						return false;
					},
					editor: {
						detailPageClass: 'Clifton.investment.instruction.setup.ContactWindow',
						getDefaultData: function(gridPanel) {
							const formValues = gridPanel.getWindow().getMainForm().formValues;
							return {
								group: formValues
							};
						}
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('active', true);
						}
						const instructionFormValues = this.getWindow().getMainForm().formValues;
						if (instructionFormValues) {
							return {
								contactGroupId: instructionFormValues.id
							};
						}
					}
				}]
			},


			{
				title: 'Instruction Definitions',
				items: [{
					xtype: 'gridpanel',
					name: 'investmentInstructionDefinitionListFind',
					instructions: 'The definitions that use this group.',
					groupField: 'category.name',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Definitions" : "Definition"]}',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Category', width: 60, dataIndex: 'category.name', filter: {searchFieldName: 'categoryName'}, hidden: true},
						{header: 'Definition Name', width: 200, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Cash', width: 30, dataIndex: 'category.cash', type: 'boolean', filter: {searchFieldName: 'cash'}, hidden: true},
						{header: 'Client', width: 30, dataIndex: 'category.client', type: 'boolean', filter: {searchFieldName: 'client'}, hidden: true},
						{header: 'Report', width: 150, dataIndex: 'report.name', filter: {searchFieldName: 'reportName'}},
						{header: 'Item Group Populator', width: 150, dataIndex: 'itemGroupPopulatorBean.name', filter: {searchFieldName: 'itemGroupPopulatorBeanName'}},
						{header: 'Delivery Type', width: 150, dataIndex: 'deliveryType.name', filter: {type: 'combo', searchFieldName: 'deliveryTypeId', loadAll: true, url: 'investmentInstructionDeliveryTypeListFind.json'}, hidden: true},
						{
							header: 'Workflow State', width: 75, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'},
							renderer: function(v, metaData, r) {
								const sts = r.data['workflowStatus.name'];
								if (sts === 'Open' || sts === 'Pending') {
									metaData.css = 'amountAdjusted';
								}
								else if (sts === 'Active') {
									metaData.css = 'amountPositive';
								}
								else {
									metaData.css = 'amountNegative';
								}
								return v;
							}
						},
						{
							header: 'Workflow Status', width: 75, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'},
							renderer: function(v, metaData, r) {
								if (v === 'Open' || v === 'Pending') {
									metaData.css = 'amountAdjusted';
								}
								else if (v === 'Active') {
									metaData.css = 'amountPositive';
								}
								else {
									metaData.css = 'amountNegative';
								}
								return v;
							}
						},
						{
							header: 'Contact Group',
							width: 40,
							dataIndex: 'contactGroup.name',
							hidden: true
						}
					],
					isPagingEnabled: function() {
						return false;
					},
					editor: {
						addEnabled: false,
						deleteEnabled: false,
						detailPageClass: 'Clifton.investment.instruction.setup.DefinitionWindow'
					},
					getLoadParams: function() {
						const instructionFormValues = this.getWindow().getMainForm().formValues;
						if (instructionFormValues) {
							return {
								contactGroupId: instructionFormValues.id
							};
						}
					}
				}]
			}
		]
	}]
});
