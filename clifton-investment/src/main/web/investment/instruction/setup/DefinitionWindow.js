Clifton.investment.instruction.setup.DefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Instruction Definition',
	iconCls: 'fax',
	height: 650,
	width: 900,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Definition',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'InvestmentInstructionDefinition',
					finalState: 'Cancelled',
					reloadFormPanelAfterTransition: true
				},

				items: [{
					xtype: 'formwithdynamicfields',
					loadValidation: false,
					url: 'investmentInstructionDefinition.json',

					dynamicTriggerFieldName: 'category.name',
					dynamicTriggerValueFieldName: 'category.selectorBeanType.id',
					dynamicFieldTypePropertyName: 'type',
					dynamicFieldListPropertyName: 'selectorBean.propertyList',
					dynamicFieldsUrl: 'systemBeanPropertyTypeListByType.json',
					dynamicFieldsUrlParameterName: 'typeId',
					dynamicFieldFormFragment: 'selectorBeanProperties',

					labelWidth: 150,

					listeners: {
						afterload: function(panel) {
							// If workflow status != Open disable selector bean properties and output fields
							const f = panel.getForm();
							const statusName = TCG.getValue('workflowStatus.name', f.formValues);
							const readOnly = (statusName && (statusName !== '' && statusName !== 'Open'));
							panel.setReadOnlyMode.defer(20, panel, [readOnly]);

							const win = panel.getWindow();
							const opener = win.openerCt;
							if (opener && !opener.isDestroyed && opener.updateContactGroup) {
								opener.updateContactGroup(this.getForm().formValues.contactGroup);
							}
						}
					},

					reload: function() {
						const w = this.getWindow();
						this.getForm().setValues(TCG.data.getData('investmentInstructionDefinition.json?id=' + w.getMainFormId(), this), true);
						this.fireEvent('afterload', this);
					},

					setReadOnlyMode: function(readOnly) {
						if (TCG.isTrue(readOnly)) {
							this.setReadOnly(true);
							// Take read only off of name and description
							this.setReadOnlyField('name', false);
							this.setReadOnlyField('description', false);

							TCG.getChildByName(this.ownerCt, 'writableOutputFields').setVisible(false);
							TCG.getChildByName(this.ownerCt, 'readOnlyOutputFields').setVisible(true);
						}
						else {
							this.setReadOnly(false);
							TCG.getChildByName(this.ownerCt, 'readOnlyOutputFields').setVisible(false);
							TCG.getChildByName(this.ownerCt, 'writableOutputFields').setVisible(true);
						}

						let fs = TCG.getChildByName(this.ownerCt, 'selectorBeanProperties');
						fs.setDisabled(readOnly === true);

						// This needs to be done after the tags are loaded otherwise does not disable correctly
						fs = TCG.getChildByName(this.ownerCt, 'tags-fieldset');
						fs.on('afterLoadTags', () => fs.setDisabled(readOnly === true), fs);
					},

					setNameWithCalculatedValue: function() {
						let calcName = '';

						if (this.dynamicFields) {
							for (let i = 0; i < this.dynamicFields.length; i++) {
								const field = this.dynamicFields[i];
								// do not include fields with no values
								if (TCG.isNotNull(field) && field.getValue() !== '' && field.submitDetailField !== false) {
									let text = field.getValue();
									if (field.xtype === 'combo') {
										text = field.lastSelectionText;
									}
									calcName += text + ' - ';
								}
							}
						}
						if (calcName.length > 0) {
							calcName = calcName.substring(0, calcName.length - 3);
							this.setFormValue('name', calcName);
						}
						else {
							TCG.showError('No selections made to auto-generate a name with.', 'Missing Selections');
						}
					},

					openPreviewWindow: function() {
						const w = this.getWindow();
						if (w.isModified() || !w.isMainFormSaved()) {
							TCG.showError('Please save your changes before viewing preview.');
							return false;
						}

						const className = 'Clifton.investment.instruction.setup.SelectionPreviewWindow';
						const id = w.getMainFormId();
						const dc = TCG.getValue('category.table.detailScreenClass', w.getMainForm().formValues);
						const cmpId = TCG.getComponentId(className, id);
						TCG.createComponent(className, {
							id: cmpId,
							params: {definitionId: id, detailScreenClass: dc, dateLabel: TCG.getValue('category.dateLabel', w.getMainForm().formValues)},
							openerCt: this
						});
					},

					getWarningMessage: function(f) {
						const pName = f.findField('parent.label').getValue();
						if (TCG.isNotBlank(pName)) {
							return [
								{xtype: 'label', html: 'This definition has been revised by&nbsp;'},
								{xtype: 'linkfield', style: 'padding: 0', value: pName, detailPageClass: 'Clifton.investment.instruction.setup.DefinitionWindow', detailIdField: 'parent.id'}
							];
						}
						const wsName = TCG.getValue('workflowStatus.name', f.formValues);
						if (wsName !== '' && wsName !== 'Open') {
							return 'This definition is currently in a ' + wsName + ' status.  Name and Description only can be modified.';
						}
						return undefined;
					},

					items: [
						{name: 'parent.id', xtype: 'hidden'},
						{name: 'parent.label', xtype: 'hidden', submitValue: false},
						// Note: This is Currently Used for Collateral Cash Category By System Bean Properties to Know if the definition is for cash or not so we can dynamically include option to include 0 transfer amounts
						{name: 'category.cash', xtype: 'hidden', submitValue: false},
						{name: 'category.dateLabel', xtype: 'hidden', submitValue: false},
						{fieldLabel: 'Category', name: 'category.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instruction.setup.CategoryWindow', detailIdField: 'category.id', submitDetailIdField: false},
						{fieldLabel: 'Definition Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
						{
							fieldLabel: 'Report', name: 'report.name', hiddenName: 'report.id', xtype: 'combo',
							url: 'reportListFind.json?categoryName=Report Template Tags&categoryTableName=ReportTemplate&categoryLinkFieldPath=reportTemplate&categoryHierarchyName=Settlement Instructions',
							detailPageClass: 'Clifton.report.ReportWindow'
						},
						{
							fieldLabel: 'Item Group Populator', name: 'itemGroupPopulatorBean.name', hiddenName: 'itemGroupPopulatorBean.id', xtype: 'combo',
							requiredFields: ['report.id'],
							url: 'systemBeanListFind.json?groupName=Investment Instruction Item Group Populator',
							instructions: 'Optionally groups instruction items based on the logic defined in selected item group populator system bean. When grouping is used, there will be a separate report generated for each group as opposed to each item.',
							detailPageClass: 'Clifton.system.bean.BeanWindow',
							getDefaultData: function() {
								return {type: {group: {name: 'Investment Instruction Item Group Populator'}}};
							},
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const f = combo.getParentForm().getForm();
								combo.store.baseParams = {systemPropertyName: 'reportIds', propertyValue: f.findField('report.id').getValue()};
							}
						},
						{
							fieldLabel: 'From Contact',
							name: 'fromContact.label',
							displayField: 'label',
							hiddenName: 'fromContact.id',
							xtype: 'combo',
							loadAll: false,
							url: 'businessContactListFind.json',
							detailPageClass: 'Clifton.business.contact.ContactWindow',
							qtip: 'The Business Contact that instructions will be sent from'
						},
						{
							fieldLabel: 'Contact Group',
							name: 'contactGroup.name',
							hiddenName: 'contactGroup.id',
							xtype: 'combo',
							url: 'investmentInstructionContactGroupListAll.json',
							loadAll: true,
							detailPageClass: 'Clifton.investment.instruction.setup.ContactGroupWindow',
							qtip: 'The group of contacts that the generated instructions will be sent to'
						},
						{
							fieldLabel: 'Delivery Type',
							name: 'deliveryType.name',
							hiddenName: 'deliveryType.id',
							xtype: 'combo',
							url: 'investmentInstructionDeliveryTypeListFind.json',
							loadAll: true,
							detailPageClass: 'Clifton.investment.instruction.delivery.InstructionDeliveryTypeWindow'
						},
						{
							fieldLabel: 'Message Subject',
							name: 'messageSubjectContent',
							qtip: 'Alter the subject of emails that are sent automatically with the instruction attached.'
						},
						{
							fieldLabel: 'Message Body',
							name: 'messageBodyContent',
							xtype: 'htmleditor',
							height: 175,
							plugins: [new TCG.form.HtmlEditorFreemarker()],
							qtip: 'Alter the body of emails that are sent automatically with the instruction attached.'
						},
						{
							fieldLabel: 'Filename Body',
							name: 'filenameTemplate',
							qtip: 'Alters the body of the filename. NOTE: This field is Freemarker enabled and the following items are currently accessible: CATEGORY, DEFINITION, INSTRUCTION, RECIPIENT_COMPANY, and REPORT. Ex: ${instruction.getRecipientCompany().getName())} would result in a file with the name of 2017.07.21_Bank_of_New_York_Mellon_125231-124245.pdf'
						},
						{
							boxLabel: 'Wire Date Required?',
							xtype: 'checkbox',
							name: 'requiresWireDate',
							qtip: 'If Yes, instructions cannot be Sent or Marked Complete until a Wire Date is populated on the transaction.'
						},
						{xtype: 'hidden', name: 'category.selectorBeanType.id', submitValue: false},
						{xtype: 'hidden', name: 'selectorBean.id'},
						{
							xtype: 'system-tags-fieldset',
							name: 'tags-fieldset',
							title: 'Tags',
							tableName: 'InvestmentInstructionDefinition',
							hierarchyCategoryName: 'Investment Instruction Tags'
						},
						{
							xtype: 'fieldset',
							title: 'Selections',
							tbar: [
								{
									text: 'Auto Generate Name',
									tooltip: 'Auto generates the name of the instruction definition based on selections below.',
									iconCls: 'copy',
									handler: function() {
										const fp = TCG.getParentFormPanel(this);
										fp.setNameWithCalculatedValue();
									}
								}, '-',
								{
									text: 'Preview',
									tooltip: 'Preview the list of entities and the recipients that match this selection.',
									iconCls: 'preview',
									handler: function() {
										const fp = TCG.getParentFormPanel(this);
										fp.openPreviewWindow();
									}
								}, '-'],
							items: [{
								xtype: 'formfragment',
								frame: false,
								name: 'selectorBeanProperties',
								labelWidth: 185,
								items: []
							}]
						},
						{
							xtype: 'fieldset',
							title: 'Instruction Output',

							instructions: 'The following information will be supplied as output to the selected report for entities using this instruction. NOTE: Only the <i><b>Value</b></i> will be displayed on the report. Label and Description are for internal use only.',
							defaults: {
								anchor: '0'
							},
							items: [
								{
									xtype: 'formgrid-scroll',
									storeRoot: 'fieldList',
									name: 'writableOutputFields',
									dtoClass: 'com.clifton.investment.instruction.setup.InvestmentInstructionDefinitionField',
									columnsConfig: [
										{
											header: 'Field Label', width: 100, dataIndex: 'name', editor: {xtype: 'textfield'},
											tooltip: 'Optional label for this field.  This information is for internal use only and is not included in instructions output.'
										},
										{
											header: 'Field Value', width: 330, dataIndex: 'value', editor: {xtype: 'textfield'},
											tooltip: 'Field Value to display'
										},
										{
											header: 'Description', width: 280, dataIndex: 'description', editor: {xtype: 'textfield'},
											tooltip: 'Description/Comments for the field.  This information is for internal use only and is not included in instructions output.'
										},
										{
											header: 'Order', width: 55, dataIndex: 'fieldOrder', editor: {xtype: 'spinnerfield'}, type: 'int',
											tooltip: 'Field display order'
										}
									]
								},

								{
									xtype: 'formgrid-scroll',
									storeRoot: 'fieldList',
									name: 'readOnlyOutputFields',
									hidden: true,
									readOnly: true,
									submitValue: false,
									dtoClass: 'com.clifton.investment.instruction.setup.InvestmentInstructionDefinitionField',
									columnsConfig: [
										{header: 'Label', width: 100, dataIndex: 'name'},
										{header: 'Value', width: 330, dataIndex: 'value'},
										{header: 'Description', width: 280, dataIndex: 'description'},
										{header: 'Order', width: 55, dataIndex: 'fieldOrder', type: 'int'}
									]
								}
							]
						}
					]
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'InvestmentInstructionDefinition'
				}]
			},


			{
				title: 'Instructions',
				items: [{
					xtype: 'investment-instruction-grid',
					showSetupAndGenerateButtons: false,
					groupField: undefined,
					getTopToolbarFilters: function(toolbar) {
						return [];
					},
					// Overrides default, because we are showing all, not just for a specific table
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// Default to Last 30 days
							this.setFilterValue('instructionDate', {'after': new Date().add(Date.DAY, -30)});


							// Change Date Column Header
							let dateLabel = TCG.getValue('category.dateLabel', this.getWindow().getMainForm().formValues);
							if (TCG.isBlank(dateLabel)) {
								dateLabel = 'Date';
							}
							const cm = this.grid.getColumnModel();
							const colIndex = cm.findColumnIndex('instructionDate');
							if (colIndex !== -1) {
								cm.setColumnHeader(colIndex, dateLabel);
							}

						}
						return {definitionId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Contacts',
				items: [{
					name: 'investmentInstructionContactListFind',
					xtype: 'gridpanel',
					instructions: 'A list of contacts that instructions of this definition should be sent to. These contacts come from the Contact Group on the definition.',
					groupField: 'company.labelExpanded',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Contact Group', width: 30, dataIndex: 'group.name', hidden: true},
						{header: 'Company', width: 30, dataIndex: 'company.labelExpanded', filter: {searchFieldName: 'companyLabel'}, hidden: true},
						{header: 'Destination', width: 30, dataIndex: 'destinationSystemBean.description', filter: {searchFieldName: 'destinationSystemBeanDescription'}},
						{header: 'Destination Type', width: 20, dataIndex: 'destinationSystemBean.type.name', filter: {searchFieldName: 'destinationSystemBeanTypeName'}},
						{header: 'Start Date', width: 10, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 10, dataIndex: 'endDate', hidden: true},
						{header: 'Active', width: 7, dataIndex: 'active', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instruction.setup.ContactWindow',
						getDefaultData: function(gridPanel) {
							return {
								group: gridPanel.getWindow().getMainForm().formValues.contactGroup
							};
						}
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('active', true);
						}
						const group = this.getWindow().getMainForm().formValues.contactGroup;
						if (group) {
							return {contactGroupId: group.id};
						}
						else {
							return false;
						}
					}
				}]
			}
		]
	}]
});


Clifton.investment.instruction.setup.SelectionPreviewWindow = Ext.extend(TCG.app.Window, {
	title: 'Instruction Definition Selection Preview',
	iconCls: 'fax',
	width: 800,

	items: [{
		xtype: 'gridpanel',
		instructions: 'The following entities are currently selected for this instruction definition.',
		name: 'investmentInstructionSelectionList',
		standardColumns: [],

		groupField: 'recipientCompany.name',
		groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]}',

		columns: [
			{header: 'Recipient', dataIndex: 'recipientCompany.name', width: 200, filter: false, hidden: true},
			{header: 'RecipientCompanyID', dataIndex: 'recipientCompany.id', width: 5, filter: false, hidden: true},
			{header: 'FKFieldID', dataIndex: 'fkFieldId', type: 'int', width: 70, filter: false, hidden: true},
			{header: 'Item', dataIndex: 'fkFieldLabel', width: 300, filter: false},
			{
				header: '', width: 15,
				renderer: function(v, args, r) {
					return TCG.renderActionColumn('pdf', '', 'Preview Report', 'report');
				}
			}
		],
		plugins: {ptype: 'gridsummary'},

		gridConfig: {
			listeners: {
				'rowclick': function(grid, rowIndex, evt) {
					if (TCG.isActionColumn(evt.target)) {
						const eventName = TCG.getActionColumnEventName(evt);
						const row = grid.store.data.items[rowIndex];
						if (eventName === 'report') {
							// instructionDefinitionId, fkFieldId, instructionDate, recipientCompanyId);
							let url = 'investmentInstructionItemReportPreviewDownload.json';
							url += '?instructionDefinitionId=' + grid.ownerGridPanel.getWindow().params.definitionId;
							url += '&fkFieldId=' + row.json.fkFieldId;
							url += '&instructionDate=' + TCG.getChildByName(grid.ownerGridPanel.getTopToolbar(), 'previewDate').getValue().format('m/d/Y');
							url += '&recipientCompanyId=' + row.json.recipientCompany.id;
							TCG.downloadFile(url, null, grid);
						}
					}
				}
			}
		},

		editor: {
			drillDownOnly: true,
			getDetailPageClass: function(grid, row) {
				const w = grid.ownerGridPanel.getWindow();
				if (w.params && w.params.detailScreenClass) {
					return w.params.detailScreenClass;
				}
				return false;
			},
			getDetailPageId: function(gridPanel, row) {
				return row.json.fkFieldId;
			}
		},
		getTopToolbarFilters: function(toolbar) {
			let dtLabel = 'Date';
			const w = this.getWindow();
			if (w.params && w.params.dateLabel) {
				dtLabel = w.params.dateLabel;
			}
			return [{fieldLabel: dtLabel, xtype: 'toolbar-datefield', name: 'previewDate', value: Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y')}];
		},
		getLoadParams: function() {
			const w = this.getWindow();
			if (w.params) {
				const dt = TCG.getChildByName(this.getTopToolbar(), 'previewDate').getValue().format('m/d/Y');
				return {
					readUncommittedRequested: true,
					enableOpenSessionInView: true,
					definitionId: w.params.definitionId,
					date: dt
				};
			}
			return false;
		}
	}]
});
