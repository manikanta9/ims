Clifton.investment.instruction.setup.ContactWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Instruction Contact',
	iconCls: 'group',
	width: 550,
	height: 330,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formwithdynamicfields',
		instructions: 'Note, when adding a contact to a group, it will apply to all instruction definitions that use this group. When instructions are sent via Instruction Runs,' +
			' the system will find all contacts from corresponding instruction definition\'s group that match instruction\'s recipient company.',
		labelWidth: 155,
		labelFieldName: 'company.name',
		url: 'investmentInstructionContact.json',
		dynamicTriggerFieldName: 'destinationSystemBean.type.name',
		dynamicTriggerValueFieldName: 'destinationSystemBean.type.id',
		dynamicFieldTypePropertyName: 'type',
		dynamicFieldListPropertyName: 'destinationSystemBean.propertyList',
		dynamicFieldsUrl: 'systemBeanPropertyTypeListByType.json',
		dynamicFieldsUrlParameterName: 'typeId',
		items: [
			{
				fieldLabel: 'Contact Group',
				name: 'group.name',
				hiddenName: 'group.id',
				xtype: 'linkfield',
				detailPageClass: 'Clifton.investment.instruction.setup.ContactGroupWindow',
				detailIdField: 'group.id',
				readOnly: true
			},
			{
				fieldLabel: 'Recipient Company',
				name: 'company.name',
				hiddenName: 'company.id',
				xtype: 'combo',
				loadAll: false,
				url: 'businessCompanyListFind.json',
				detailPageClass: 'Clifton.business.company.CompanyWindow',
				qtip: 'The company that the contact should receive the instruction on behalf of'
			},
			{
				fieldLabel: 'Start Date',
				name: 'startDate',
				xtype: 'datefield',
				qtip: 'The date the contact will start receiving instructions',
				value: Clifton.calendar.getBusinessDayFrom(0).format('m/d/Y')
			},
			{
				fieldLabel: 'End Date',
				name: 'endDate',
				xtype: 'datefield',
				qtip: 'The date the contact will stop receiving instructions. Leave open to continue indefinitely.'
			},
			{
				fieldLabel: 'Destination Type',
				name: 'destinationSystemBean.type.name',
				hiddenName: 'destinationSystemBean.type.id',
				xtype: 'combo',
				allowBlank: false,
				url: 'systemBeanTypeListFind.json?groupName=Instruction Destination Generator',
				detailPageClass: 'Clifton.system.bean.TypeWindow',
				listeners: {
					// reset object bean property fields on changes to bean type (triggerField)
					select: function(combo, record, index) {
						combo.ownerCt.resetDynamicFields(combo, false);
					}
				}
			}
		]
	}]
});
