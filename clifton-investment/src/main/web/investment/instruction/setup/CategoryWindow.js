Clifton.investment.instruction.setup.CategoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Instruction Category',
	iconCls: 'fax',
	height: 550,
	width: 720,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Category',
				items: [
					{
						xtype: 'formpanel',
						loadValidation: false,
						labelWidth: 120,
						url: 'investmentInstructionCategory.json',
						items: [
							{fieldLabel: 'Table', name: 'table.name', hiddenName: 'table.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', disableAddNewItem: true, detailPageClass: 'Clifton.system.schema.TableWindow'},
							{fieldLabel: 'Category Name', name: 'name'},
							{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
							{fieldLabel: 'Date Label', name: 'dateLabel'},
							{
								boxLabel: 'Required', xtype: 'checkbox', name: 'required',
								qtip: 'Note: Not currently used but intended to validate that every entity has an instruction item associated with it.'
							},
							{xtype: 'label', html: '<hr/>'},
							{
								xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'cash',
								items: [
									{boxLabel: 'Cash', xtype: 'radio', name: 'cash', inputValue: true},
									{boxLabel: 'Securities', xtype: 'radio', name: 'cash', inputValue: false}
								]
							},
							{xtype: 'label', html: '<hr/>'},
							{
								xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'client',
								items: [
									{boxLabel: '<b>Client</b>: Instructions are defined for the Custodian and sent to the Counterparty', xtype: 'radio', name: 'client', inputValue: true},
									{boxLabel: '<b>Counterparty</b>: Instructions are defined for the Counterparty and sent to the Custodian', xtype: 'radio', name: 'client', inputValue: false}
								]
							},
							{xtype: 'label', html: '<hr/>'},
							{
								fieldLabel: 'Selector Bean Type', name: 'selectorBeanType.name', hiddenName: 'selectorBeanType.id', xtype: 'combo',
								url: 'systemBeanTypeListFind.json?groupName=Investment Instruction Definition Selector', detailPageClass: 'Clifton.system.bean.TypeWindow',
								qtip: 'For each instruction definition of this category, a system bean of this type will be created that selects appropriate instruction items for the table as well as provides the logic to select recipient company for instruction.'
							},
							{
								fieldLabel: 'From Contact', name: 'fromContact.label', hiddenName: 'fromContact.id', xtype: 'combo',
								url: 'businessContactListFind.json', displayField: 'label', detailPageClass: 'Clifton.business.contact.ContactWindow',
								qtip: 'The Business Contact that instructions will be sent from'
							}
						]
					}
				]
			},


			{
				title: 'Instruction Definitions',
				name: 'investmentInstructionDefinitionListFind',
				xtype: 'gridpanel',

				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Definition Name', width: 200, dataIndex: 'name', defaultSortColumn: true},
					{header: 'Report', width: 200, dataIndex: 'report.name', filter: {searchFieldName: 'reportName'}},
					{header: 'Item Group Populator', width: 150, dataIndex: 'itemGroupPopulatorBean.name', filter: {searchFieldName: 'itemGroupPopulatorBeanName'}, hidden: true},
					{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
					{header: 'Delivery Type', width: 150, dataIndex: 'deliveryType.name', filter: {type: 'combo', searchFieldName: 'deliveryTypeId', loadAll: true, url: 'investmentInstructionDeliveryTypeListFind.json'}, hidden: true},
					{
						header: 'Workflow State', width: 75, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'},
						renderer: function(v, metaData, r) {
							const sts = r.data['workflowStatus.name'];
							if (sts === 'Open' || sts === 'Pending') {
								metaData.css = 'amountAdjusted';
							}
							else if (sts === 'Active') {
								metaData.css = 'amountPositive';
							}
							else {
								metaData.css = 'amountNegative';
							}
							return v;
						}
					},
					{
						header: 'Workflow Status', width: 75, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'},
						renderer: function(v, metaData, r) {
							if (v === 'Open' || v === 'Pending') {
								metaData.css = 'amountAdjusted';
							}
							else if (v === 'Active') {
								metaData.css = 'amountPositive';
							}
							else {
								metaData.css = 'amountNegative';
							}
							return v;
						}
					}
				],
				editor: {
					detailPageClass: 'Clifton.investment.instruction.setup.DefinitionWindow',
					copyURL: 'investmentInstructionDefinitionCopy.json',
					getDefaultData: function(gridPanel) {
						return {
							category: gridPanel.getWindow().getMainForm().formValues
						};
					}
				},
				getLoadParams: function(firstLoad) {
					return {instructionCategoryId: this.getWindow().getMainFormId()};
				}
			}
		]
	}]
});
