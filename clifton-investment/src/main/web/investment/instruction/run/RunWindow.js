Clifton.investment.instruction.run.RunWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Instruction Run',
	iconCls: 'fax',
	height: 600,
	width: 750,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Run',
				items: [{
					xtype: 'formpanel',
					labelWidth: 120,
					url: 'investmentInstructionRun.json',
					requestedPropertiesRoot: 'data',
					labelFieldName: 'id',
					readOnly: true,
					items: [
						{fieldLabel: 'Instruction', name: 'investmentInstruction.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instruction.InstructionWindow', detailIdField: 'investmentInstruction.id', submitDetailIdField: false},
						{fieldLabel: 'Run Type', name: 'runType'},
						{fieldLabel: 'Run Status', name: 'runStatus.investmentInstructionStatusName'},
						{fieldLabel: 'Detached', name: 'detached', xtype: 'checkbox', boxLabel: 'The run will not update the status of the Instruction Item or Instruction that it was created for'},
						{fieldLabel: 'Scheduled Date', name: 'scheduledDate'},
						{fieldLabel: 'Start Date', name: 'startDate'},
						{fieldLabel: 'End Date', name: 'endDate'},
						{fieldLabel: 'History', name: 'historyDescription', xtype: 'textarea', anchor: '-35 -200'}
					]
				}]
			},


			{
				title: 'Details',
				xtype: 'integration-outgoing-exportGrid',
				rowSelectionModel: 'single',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Export Source', width: 70, dataIndex: 'integrationSource.label', filter: {searchFieldName: 'integrationSourceId', type: 'combo', url: 'integrationSourceListFind.json'}},
					{header: 'Source Identifier', dataIndex: 'sourceSystemIdentifier', width: 40, hidden: true},
					{header: 'Destination Type', dataIndex: 'destinationType.name', width: 40},
					{
						header: 'Status', width: 60, dataIndex: 'status.name', filter: {type: 'list', options: [['RECEIVED', 'RECEIVED'], ['PROCESSED', 'PROCESSED'], ['REPROCESSED', 'REPROCESSED'], ['FAILED', 'FAILED']]},
						renderer: function(v, m, r) {
							return (Clifton.integration.outgoing.renderStatus(v, m, r));
						}
					},
					{header: 'Message Subject', dataIndex: 'messageSubject', width: 100},
					{header: 'Retry Count', dataIndex: 'retryCount', width: 60, type: 'int', title: 'Number of times to retry in case of failure', hidden: true, useNull: true},
					{header: 'Retry Delay In Seconds', dataIndex: 'retryDelayInSeconds', width: 60, type: 'int', title: 'Time in seconds before a retry is attempted again', hidden: true, useNull: true},
					{header: 'Received Date', width: 70, dataIndex: 'receivedDate', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},
					{header: 'Sent Date', width: 70, dataIndex: 'sentDate', type: 'date'}
				],
				getLoadParams: function() {
					return {
						'integrationSourceName': 'IMS_INSTRUCTION',
						'sourceSystemIdentifier': this.getWindow().getMainFormId()
					};
				},
				editor: {
					detailPageClass: 'Clifton.integration.outgoing.IntegrationExportWindow',
					drillDownOnly: true
				}
			},


			{
				title: 'Items',
				instructions: 'These items were included in this Instruction Run.',
				wikiPage: 'IT/Instruction+Runs',
				items: [{
					name: 'investmentInstructionItemListPopulatedFind',
					instructions: 'Run Items are the Instruction Items that were bundled into this run.',
					xtype: 'gridpanel',
					columns: [
						{
							header: '', width: 40,
							renderer: function(v, args, r) {
								return TCG.renderActionColumn('pdf', '', 'View Report', 'report');
							}
						},
						{header: 'FkFieldID', width: 30, dataIndex: 'fkFieldId', hidden: true},
						{header: 'Entity', dataIndex: 'fkFieldLabel', width: 500},
						{
							header: 'Status', width: 70, dataIndex: 'status.name',
							renderer: function(v, m, r) {
								return (Clifton.investment.instruction.renderStatus(v, m, r));
							}
						}
					],
					editor: {
						addEnabled: false,
						deleteEnabled: false
					},
					getLoadParams: function() {
						const instructionRunId = this.getWindow().params.id;
						return {runId: instructionRunId};
					},
					gridConfig: {
						listeners: {
							rowclick: function(grid, rowIndex, evt) {
								if (TCG.isActionColumn(evt.target)) {
									const eventName = TCG.getActionColumnEventName(evt);
									const row = grid.store.data.items[rowIndex];
									if (eventName === 'report') {
										const url = 'investmentInstructionItemReportDownload.json?instructionItemId=' + row.json.id;
										TCG.downloadFile(url, null, grid);
									}
								}
							}
						}
					}
				}]
			},


			{
				title: 'Contacts',
				items: [{
					name: 'investmentInstructionContactListFind',
					instructions: 'The contacts that the items in this run were sent to',
					additionalPropertiesToRequest: 'contact.id',
					xtype: 'gridpanel',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition', width: 20, dataIndex: 'definition.labelExpanded', hidden: true},
						{header: 'Company', width: 20, dataIndex: 'company.labelExpanded', filter: {searchFieldName: 'companyLabel'}},
						{header: 'Destination', width: 20, dataIndex: 'destinationSystemBean.description', filter: {searchFieldName: 'destinationSystemBeanDescription'}},
						{header: 'Destination Type', width: 20, dataIndex: 'destinationSystemBean.type.name', filter: {searchFieldName: 'destinationSystemBeanTypeName'}},
						{header: 'Start Date', width: 15, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 15, dataIndex: 'endDate', hidden: true}
					],
					editor: {
						addEnabled: false,
						deleteEnabled: false,
						detailPageClass: 'Clifton.investment.instruction.setup.ContactWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.id;
						}
					},
					getLoadParams: function() {
						const formValues = this.getWindow().getMainForm().formValues;
						const instruction = formValues.investmentInstruction;
						const definition = instruction.definition;
						const contactGroup = definition.contactGroup;
						const activeOnDate = formValues.startDate ? TCG.parseDate(formValues.startDate).format('m/d/Y') : formValues.endDate ? TCG.parseDate(formValues.endDate).format('m/d/Y') : TCG.parseDate(formValues.scheduledDate).format('m/d/Y');
						const destinationSystemBeanTypeName = formValues.runType;
						const result = {
							companyId: instruction.recipientCompany.id,
							activeOnDate: activeOnDate,
							destinationSystemBeanTypeName: destinationSystemBeanTypeName
						};

						if (contactGroup) {
							result.contactGroupId = contactGroup.id;
						}

						return result;
					}
				}]
			}
		]
	}]
});
