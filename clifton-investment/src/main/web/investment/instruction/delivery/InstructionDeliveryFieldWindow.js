Clifton.investment.instruction.delivery.InstructionDeliveryFieldWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Delivery Instruction Field',
	iconCls: 'email',
	width: 700,
	height: 550,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'The following information will be supplied as output to the selected report for entities using this instruction. NOTE: Only the <i><b>Value</b></i> will be displayed on the report. Label and Description are for internal use only.',
		url: 'investmentInstructionDeliveryField.json',
		labelFieldName: 'value',
		labelWidth: 130,
		items: [
			{fieldLabel: 'Instruction Delivery', name: 'delivery.name', xtype: 'linkfield', detailIdField: 'delivery.id', detailPageClass: 'Clifton.investment.instruction.delivery.InstructionDeliveryWindow'},
			{
				fieldLabel: 'Field Type', name: 'type.name', hiddenName: 'type.id', xtype: 'combo', url: 'investmentInstructionDeliveryFieldTypeListFind.json', loadAll: true, allowBlank: false, listeners: {
					change: function(field, newValue, oldValue) {
						const fp = TCG.getParentFormPanel(field);
						fp.setFieldState(fp.getForm());
					},
					select: function(field, newValue) {
						const fp = TCG.getParentFormPanel(field);
						fp.setFieldState(fp.getForm());
					}
				}
			},
			{fieldLabel: 'Label', name: 'name'},
			{fieldLabel: 'Value', name: 'value'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Order', name: 'fieldOrder', xtype: 'spinnerfield', allowBlank: true},
			{
				xtype: 'fieldset',
				collapsible: false,
				title: 'Instruction Field Selection',
				instructions: 'Determines whether the field will appear on a report or used for additional parties.  The selections are mutually exclusive, either type, holding account group, holding account or client account is selected.',
				labelWidth: 140,
				selections: {},
				items: [
					{
						fieldLabel: 'Delivery Type', name: 'deliveryType.name', hiddenName: 'deliveryType.id', xtype: 'combo', url: 'investmentInstructionDeliveryTypeListFind.json', disableAddNewItem: true, loadAll: true,
						mutuallyExclusiveFields: ['holdingAccountGroup.name', 'holdingAccount.label', 'clientAccount.label']
					},
					{
						fieldLabel: 'Holding Account Group', name: 'holdingAccountGroup.name', hiddenName: 'holdingAccountGroup.id', xtype: 'combo', url: 'investmentAccountGroupListFind.json?accountTypeNameNotEqualsOrNull=Client', detailPageClass: 'Clifton.investment.account.AccountGroupWindow',
						mutuallyExclusiveFields: ['deliveryType.name', 'holdingAccount.label', 'clientAccount.label']
					},
					{
						fieldLabel: 'Holding Account', name: 'holdingAccount.label', hiddenName: 'holdingAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=false', detailPageClass: 'Clifton.investment.account.AccountWindow',
						mutuallyExclusiveFields: ['deliveryType.name', 'holdingAccountGroup.name', 'clientAccount.label']
					},
					{
						fieldLabel: 'Client Account', name: 'clientAccount.label', hiddenName: 'clientAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?accountType=Client', detailPageClass: 'Clifton.investment.account.AccountWindow',
						qtip: 'Limit the selected holding account.', mutuallyExclusiveFields: ['deliveryType.name', 'holdingAccountGroup.name', 'holdingAccount.label']
					}
				],
				listeners: {
					afterrender: function() {
						this.selections = {};
					}
				}
			}
		],
		listeners: {
			afterload: function(form, isClosing) {
				const panel = this;
				panel.setFieldState(this.getForm());
			}
		},
		setFieldState(form) {
			const typeField = form.findField('type.name');
			const orderField = form.findField('fieldOrder');
			orderField.allowBlank = TCG.isEquals('Interested Parties', typeField.lastSelectionText);
		}
	}]
});
