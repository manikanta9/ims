Clifton.investment.instruction.delivery.InstructionInstructionDeliveryBusinessCompanyWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Instruction Delivery Linkage',
	iconCls: 'email',
	width: 650,
	height: 250,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'Selected broker uses the following bank for its wire transfers.',
		url: 'investmentInstructionDeliveryBusinessCompany.json',
		items: [
			{fieldLabel: 'Company', name: 'referenceTwo.name', xtype: 'combo', hiddenName: 'referenceTwo.id', detailPageClass: 'Clifton.business.company.CompanyWindow', url: 'businessCompanyListFind.json?companyTypeNames=Custodian,Broker', allowBlank: false},
			{fieldLabel: 'Delivery Type', name: 'investmentInstructionDeliveryType.name', hiddenName: 'investmentInstructionDeliveryType.id', xtype: 'combo', loadAll: true, url: 'investmentInstructionDeliveryTypeListFind.json', detailPageClass: 'Clifton.investment.instruction.delivery.InstructionDeliveryTypeWindow', allowBlank: false},
			{fieldLabel: 'Currency', name: 'deliveryCurrency.label', hiddenName: 'deliveryCurrency.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: false},
			{fieldLabel: 'Delivery Instruction', name: 'referenceOne.name', hiddenName: 'referenceOne.id', xtype: 'combo', loadAll: true, url: 'investmentInstructionDeliveryListFind.json', detailPageClass: 'Clifton.investment.instruction.delivery.InstructionDeliveryWindow', allowBlank: false}
		],
		listeners: {
			afterrender: function(panel) {
				const defaultData = panel.getDefaultData(panel.getWindow());
				if (defaultData && defaultData.hasOwnProperty('referenceOne')) {
					panel.setReadOnlyField('referenceOne.name', true);
				}
				else if (defaultData && defaultData.hasOwnProperty('referenceTwo')) {
					panel.setReadOnlyField('referenceTwo.name', true);
				}
			}
		}
	}]
});
