Clifton.investment.instruction.delivery.InstructionDeliveryTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Instruction Delivery Type',
	iconCls: 'email',
	width: 600,
	height: 220,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		url: 'investmentInstructionDeliveryType.json',
		items: [
			{fieldLabel: 'Name', name: 'name', allowBlank: false},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', allowBlank: false},
			{boxLabel: 'Default Delivery', name: 'defaultDeliveryType', xtype: 'checkbox'}
		]
	}]
});
