Clifton.investment.instruction.delivery.InstructionDeliveryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Delivery Instruction',
	iconCls: 'email',
	width: 800,
	height: 550,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Delivery Instructions for a specific bank wire transfers.',
					url: 'investmentInstructionDelivery.json',
					items: [
						{fieldLabel: 'Instruction Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Bank', name: 'deliveryCompany.name', hiddenName: 'deliveryCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true', detailPageClass: 'Clifton.business.company.CompanyWindow'},
						{fieldLabel: 'Account Number', name: 'deliveryAccountNumber'},
						{fieldLabel: 'Account Name', name: 'deliveryAccountName'},
						{fieldLabel: 'For Further Credit', name: 'deliveryForFurtherCredit'},
						{fieldLabel: 'ABA', name: 'deliveryABA'},
						{fieldLabel: 'SWIFT Code', name: 'deliverySwiftCode'},
						{fieldLabel: 'Delivery Text', name: 'deliveryText', xtype: 'textarea'},
						{boxLabel: 'Generate a separate wire transfer for Collateral', name: 'separateWireForCollateral', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Instruction Fields',
				items: [{
					xtype: 'gridpanel',
					appendStandardColumns: false,
					name: 'investmentInstructionDeliveryFieldListFind',
					instructions: 'The following information will be supplied as output to the selected report for entities using this instruction. NOTE: Only the <i><b>Value</b></i> will be displayed on the report. Label and Description are for internal use only.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Field Type', width: 100, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', url: 'investmentInstructionDeliveryFieldTypeListFind.json', loadAll: true}},
						{header: 'Label', width: 100, dataIndex: 'name', hidden: true, filter: {searchFieldName: 'name'}},
						{header: 'Value', width: 220, dataIndex: 'value'},
						{header: 'Description', width: 280, dataIndex: 'description', hidden: true},
						{header: 'Order', width: 55, dataIndex: 'fieldOrder', type: 'int', useNull: true, defaultSortColumn: true},

						{header: 'Delivery Type', width: 100, dataIndex: 'deliveryType.name', filter: {type: 'combo', searchFieldName: 'deliveryTypeId', url: 'investmentInstructionDeliveryTypeListFind.json'}},
						{header: 'Holding Account Group', width: 125, dataIndex: 'holdingAccountGroup.name', filter: {type: 'combo', searchFieldName: 'holdingAccountGroupId', url: 'investmentAccountGroupListFind.json'}},
						{header: 'Holding Account', width: 100, dataIndex: 'holdingAccount.label', filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'label', url: 'investmentAccountListFind.json'}}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instruction.delivery.InstructionDeliveryFieldWindow',
						getDefaultData: function(gridPanel) {
							return {delivery: gridPanel.getWindow().getMainForm().formValues};
						}
					},
					getLoadParams: function(firstLoad) {
						return {deliveryId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'InvestmentInstructionDelivery'
				}]
			},


			{
				title: 'Companies',
				items: [{
					xtype: 'gridpanel',
					appendStandardColumns: false,
					name: 'investmentInstructionDeliveryBusinessCompanyListFind',
					instructions: 'Business Companies utilizing the selected delivery instruction.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Company Type', width: 70, dataIndex: 'referenceTwo.type.label', hiddenName: 'referenceTwo.type.id', filter: {type: 'combo', searchFieldName: 'businessCompanyTypeId', url: 'businessCompanyTypeListFind.json'}},
						{header: 'Company Name', width: 100, dataIndex: 'referenceTwo.name', filter: {type: 'combo', searchFieldName: 'businessCompanyId', displayField: 'name', url: 'businessCompanyListFind.json'}},
						{header: 'Delivery Type', width: 100, dataIndex: 'investmentInstructionDeliveryType.name', hiddenName: 'investmentInstructionDeliveryType.id', filter: {type: 'combo', searchFieldName: 'typeId', url: 'investmentInstructionDeliveryTypeListFind.json'}},
						{header: 'Currency', width: 50, dataIndex: 'deliveryCurrency.symbol', hiddenName: 'deliveryCurrency.id', filter: {type: 'combo', searchFieldName: 'deliveryCurrencyId', displayField: 'symbol', dataIndex: 'id', url: 'investmentSecurityListFind.json?currency=true'}}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instruction.delivery.InstructionInstructionDeliveryBusinessCompanyWindow',
						getDefaultData: function(gridPanel) {
							return {referenceOne: gridPanel.getWindow().getMainForm().formValues};
						}
					},
					getLoadParams: function(firstLoad) {
						return {investmentInstructionDelivery: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
