Clifton.investment.account.securitytarget.SecurityTargetListWindow = Ext.extend(TCG.app.Window, {
	id: 'securityTargetListWindow',
	title: 'Security Target Setup',
	iconCls: 'account',
	width: 1700,
	height: 700,

	items: [
		{
			xtype: 'tabpanel',

			listeners: {
				beforerender: function() {
					const tabs = this;
					Clifton.investment.account.securitytarget.SecurityTargetListWindowAdditionalTabs.forEach(tab => tabs.add(tab));
				}
			},

			items: [
				{
					title: 'Security Targets',
					items: [{
						xtype: 'security-target-gridpanel',
						columnOverrides: [
							{dataIndex: 'securityTargetNote', hidden: true}
						]
					}]
				},
				{
					title: 'Security Target Freeze Types',
					items: [{
						xtype: 'gridpanel',
						name: 'investmentAccountSecurityTargetFreezeTypeListFind',
						columns: [
							{header: 'Type Name', dataIndex: 'name', width: 100},
							{header: 'Description', dataIndex: 'description', width: 500},
							{header: 'Note Required', dataIndex: 'noteRequired', type: 'boolean', width: 50},
							{header: 'Cause Required', dataIndex: 'causeRequired', type: 'boolean', width: 50},
							{header: 'System Defined', dataIndex: 'systemDefined', type: 'boolean', width: 50}
						],
						editor: {
							detailPageClass: 'Clifton.investment.account.securitytarget.SecurityTargetFreezeType',
							drillDownOnly: true
						}
					}]
				}
			]
		}
	]
});


Clifton.investment.account.securitytarget.SecurityTargetFreezeType = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Target Freeze Type',
	iconCls: 'account',
	height: 275,
	width: 700,

	items: [{
		xtype: 'formpanel',
		url: 'investmentAccountSecurityTargetFreezeType.json',
		instructions: '',
		readOnly: true,
		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
			{boxLabel: 'Require Note for a Security Target Freeze entry of this type', name: 'noteRequired', xtype: 'checkbox'},
			{boxLabel: 'Require Cause FK for a Security Target Freeze entry of this type', name: 'causeRequired', xtype: 'checkbox'},
			{boxLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox'}
		]
	}]
});
