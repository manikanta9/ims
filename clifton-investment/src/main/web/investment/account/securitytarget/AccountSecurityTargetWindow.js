Clifton.investment.account.securitytarget.AccountSecurityTargetWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Account Security Target',
	iconCls: 'grouping',
	width: 1000,
	height: 525,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: ' Info',
				items: [{
					xtype: 'formpanel',
					url: 'investmentAccountSecurityTarget.json',
					instructions: 'Define a target quantity or notional of an underlying security to hold for a client account. The target can have a start and end date defined to control the target\'s duration.',
					labelWidth: 150,

					// This value hold a message to be displayed if there are active restrictions on the account associated with this target
					postWarningMsg: undefined,

					getWarningMessage: function(f) {
						this.loadPostingWarningMessage(f);
					},

					listeners: {
						afterload: function(formPanel) {
							const formValues = formPanel.getForm().formValues;
							if (TCG.isNotBlank(formValues.clientInvestmentAccount.label)) {
								formPanel.setClientAccountReadOnly(true);
							}
							if (TCG.isBlank(formValues.existingId)) {
								const form = this.getForm();
								// hide existing label and checkbox if the existing is not available
								form.findField('existingLabel').hide();
								// update the end existing box label to specify current date for the updated security target
								form.findField('endExisting').wrap.child('.x-form-cb-label').update('End Existing Security Target on Current Date - 1');
							}
						},
						fieldchange: function(field, newValue) {
							// If any observed field changes, we want to set the endExisting checkbox so we can end the current and add a new security target on save.
							// This is necessary to keep a history of security targets on any given day. Client Account is read only and not included in observed fields.
							const observedFieldArray = [
								'targetUnderlyingSecurity.label',
								'startDate',
								'targetUnderlyingQuantity',
								'targetUnderlyingNotional',
								'targetUnderlyingManagerAccount.label',
								'targetUnderlyingMultiplier',
								'trancheCount',
								'cycleDurationWeeks'
							];
							const form = this.getForm();
							const startDate = form.findField('startDate').getValue();
							if (observedFieldArray.indexOf(field.name, 0) > -1 && startDate) {
								const today = new Date();
								const endDate = form.findField('endDate').getValue();
								if (TCG.isDateAfter(today, startDate, true) && TCG.isDateAfter(endDate, today, true)) {
									this.getForm().findField('endExisting').setValue(true);
								}
							}
						}
					},

					loadPostingWarningMessage: function(f) {
						const fp = this;
						const formValues = fp.getForm().formValues;
						let msg = undefined;

						TCG.data.getDataPromise('tradeRestrictionListFind.json?requestedPropertiesRoot=data&requestedProperties=id&active=true&clientAccountId=' + formValues.clientInvestmentAccount.id, this)
							.then(activeRestrictions => {
								if (activeRestrictions.length > 0) {
									msg = '<b>Note</b>: This account has active restrictions.';
									if (fp.postWarningMsg !== msg) {
										fp.postWarningMsg = msg;
										const postWarnId = fp.id + '-postWarn';
										const postWarn = Ext.get(postWarnId);
										if (TCG.isNotNull(postWarn)) {
											fp.remove(postWarnId);
											Ext.destroy(postWarn);
											fp.doLayout();
										}
										const html = msg;
										msg = [{xtype: 'label', html: html}];
										fp.insert(0, {xtype: 'container', layout: 'hbox', id: postWarnId, autoEl: 'div', cls: 'warning-msg', items: msg});
										fp.doLayout();
									}
								}
							});
					},

					getDefaultData: function(window) {
						const defaultData = window.defaultData || {};
						if (window.defaultDataIsReal) {
							return defaultData;
						}
						if (TCG.isNotNull(defaultData['clientInvestmentAccount'])) {
							this.setClientAccountReadOnly(true);
						}
						defaultData['startDate'] = new Date().format('Y-m-d 00:00:00');
						return defaultData;
					},

					getFormLabel: function() {
						let prefix = 'Target ';
						let target = this.getFormValue('targetUnderlyingQuantity');
						const targetMultiplier = this.getFormValue('targetUnderlyingMultiplier');
						if (TCG.isBlank(target)) {
							target = this.getFormValue('targetUnderlyingNotional');
							prefix += 'Notional ';
						}
						else {
							prefix += 'Quantity ';
						}
						target = (target * targetMultiplier).toString();
						return prefix + target + ' of ' + this.getFormValue('targetUnderlyingSecurity.label');
					},

					setClientAccountReadOnly: function(readOnly) {
						this.getForm().findField('clientInvestmentAccount.label').setReadOnly(readOnly);
					},

					getSaveURL: function() {
						const f = this.getForm();
						if (TCG.isTrue(f.findField('endExisting').getValue())) {
							// check if we are updating an existing entry and will end it and and start new from detail window
							// or if we are ending an existing and adding new from template from the list window to track changes
							if ((TCG.isFalse(f.findField('existingLabel').isVisible()) && f.idFieldValue)
								|| (!f.idFieldValue && TCG.isNotBlank(f.findField('existingId').getValue()))) {
								return 'investmentAccountSecurityTargetFromExistingSave.json';
							}
						}
						return 'investmentAccountSecurityTargetSave.json';
					},

					tbar: Clifton.investment.account.securitytarget.SecurityTargetToolbarItems.length < 1 ? void 0 : TCG.clone(Clifton.investment.account.securitytarget.SecurityTargetToolbarItems),

					items: [
						{xtype: 'hidden', name: 'existingId'},
						{xtype: 'displayfield', name: 'existingLabel', fieldLabel: 'Existing Security Target'},
						{
							xtype: 'checkbox', name: 'endExisting', fieldLabel: '', boxLabel: 'End Existing Security Target on Selected Start Date - 1',
							qtip: 'If checked, an existing Security Target will be ended at Start Date - 1 and a new Security Target will be added. An updated Security Target will use Current Date instead of Start Date to keep a history of Security Targets active on specific days.'
						},
						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&excludeWorkflowStatusName=Closed&serviceProcessingType=SECURITY_TARGET', allowBlank: false, detailPageClass: 'Clifton.investment.account.AccountWindow', disableAddNewItem: true},
						{fieldLabel: 'Target Note', name: 'securityTargetNote', xtype: 'textarea'},
						{
							xtype: 'panel',
							layout: 'column',
							anchor: '-13',
							defaults: {layout: 'form', columnWidth: .50, defaults: {xtype: 'datefield', anchor: '-20'}},
							items: [
								{
									items: [
										{fieldLabel: 'Start Date', name: 'startDate'},
										{fieldLabel: 'End Date', name: 'endDate'}
									]
								},
								{
									items: [
										{fieldLabel: 'Last Buy Trade Date', name: 'lastBuyTradeDate', qtip: 'Date for the last buy trade applicable to this Security Target'},
										{fieldLabel: 'Last Sell Trade Date', name: 'lastSellTradeDate', qtip: 'Date for the last sell trade applicable to this Security Target'}
									]
								}
							]
						},
						{xtype: 'sectionheaderfield', header: 'Underlying Target'},
						{fieldLabel: 'Target Underlying Security', name: 'targetUnderlyingSecurity.label', hiddenName: 'targetUnderlyingSecurity.id', displayField: 'label', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{
							xtype: 'panel',
							layout: 'column',
							anchor: '-13',
							defaults: {layout: 'form', defaults: {anchor: '-20'}},
							items: [
								{
									columnWidth: .63,
									items: [
										{
											fieldLabel: 'Target Quantity', name: 'targetUnderlyingQuantity', xtype: 'currencyfield', mutuallyExclusiveFields: ['targetUnderlyingNotional', 'targetUnderlyingManagerAccount.label'], allowBlank: false,
											qtip: 'The target quantity of the underlying security to hold for this client account. Will be blank if notional is used. Negative amounts target short positions; positive amounts target long positions.'
										},
										{
											fieldLabel: 'Target Notional', name: 'targetUnderlyingNotional', xtype: 'currencyfield', mutuallyExclusiveFields: ['targetUnderlyingQuantity', 'targetUnderlyingManagerAccount.label'], allowBlank: false,
											qtip: 'The target notional of the underlying security to hold for this client account. Will be blank if quantity is used. Negative amounts target short positions; positive amounts target long positions.'
										},
										{
											fieldLabel: 'Target Manager', name: 'targetUnderlyingManagerAccount.label', hiddenName: 'targetUnderlyingManagerAccount.id', xtype: 'combo', url: 'investmentManagerAccountListFind.json', detailPageClass: 'Clifton.investment.manager.AccountWindow', displayField: 'label',
											mutuallyExclusiveFields: ['targetUnderlyingQuantity', 'targetUnderlyingNotional'], allowBlank: false,
											qtip: 'The Manager Account to use for the target of the underlying security to hold for this client account. The manager\'s balance will be used for the target.',
											beforequery: function(queryEvent) {
												const form = queryEvent.combo.getParentForm().getForm();
												queryEvent.combo.store.baseParams = {
													clientIdOrRelatedClient: TCG.getValue('clientInvestmentAccount.businessClient.id', form.formValues),
													inactive: false
												};
											},
											getDefaultData: function(form) { // defaults client
												return {
													client: TCG.getValue('clientInvestmentAccount.businessClient', form.formValues)
												};
											}
										}
									]
								},
								{columnWidth: .02, items: [{html: '&nbsp;'}]},
								{
									columnWidth: .35,
									labelWidth: 100,
									items: [
										{
											fieldLabel: 'Target Multiplier', name: 'targetUnderlyingMultiplier', xtype: 'floatfield', value: '-1', // default value to 1
											qtip: 'The target multiplier to use for the target of underlying security. The default is 1, which means 100% of the defined target should be held. Using a negative number will negate the defined target (e.g. define a target for a long equity position of SPX held by the client, and write call options against this value).'
										},
										{
											fieldLabel: 'Tranche Count', name: 'trancheCount', xtype: 'combo', valueField: 'value', displayField: 'text', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Portfolio: Security Target Tranche Counts', detailPageClass: 'Clifton.system.list.ItemWindow',
											qtip: 'An optional field defining a number of approximately equal slices the target should be sliced into. For example, some option trading strategies will use tranches defined by option expiration date. Values are obtained from the \'Portfolio: Security Target Tranche Counts\' System List.'
										},
										{
											fieldLabel: 'Cycle Duration', name: 'cycleDurationWeeks', xtype: 'combo', valueField: 'value', displayField: 'text', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Portfolio: Security Target Cycle Duration Weeks', detailPageClass: 'Clifton.system.list.ItemWindow',
											qtip: 'The duration (in weeks) of a service\'s trading cycle.  An example would be the OARS service, that has 12 tranches per cycle with 3 tranches traded each week. The cycle duration in this case is 4 weeks. This field may be left blank if cycle duration is not applicable. Values are obtained from the \'Portfolio: Security Target Cycle Duration Weeks\' System List.'
										}
									]
								}
							]
						}
					]
				}]
			},


			{
				title: 'Frozen Allocations',
				items: [{xtype: 'accounting-security-target-frozen-allocation-gridpanel'}]
			},


			{
				title: 'History',
				items: [{
					xtype: 'security-target-history-gridpanel',
					instructions: 'The following is a history of security targets that have existed for the given combination of Client Account and Underlying Security',
					columnOverrides: [
						{dataIndex: 'securityTarget.clientInvestmentAccount.label', hidden: true},
						{dataIndex: 'securityTarget.clientInvestmentAccount.teamSecurityGroup.name', hidden: true},
						{dataIndex: 'securityTarget.clientInvestmentAccount.coalesceBusinessServiceGroupName', hidden: true},
						{dataIndex: 'securityTarget.targetUnderlyingSecurity.label', hidden: true}
					],
					getLoadParams: function(firstLoad) {
						const defaultParams = {};
						const clientInvestmentAccountId = this.getWindow().getMainForm().formValues.clientInvestmentAccount.id;
						if (TCG.isNotBlank(clientInvestmentAccountId)) {
							defaultParams['clientInvestmentAccountId'] = clientInvestmentAccountId;
						}
						const targetUnderlyingSecurityId = this.getWindow().getMainForm().formValues.targetUnderlyingSecurity.id;
						if (TCG.isNotBlank(targetUnderlyingSecurityId)) {
							defaultParams['targetUnderlyingSecurityId'] = targetUnderlyingSecurityId;
						}
						return defaultParams;
					},
					getTopToolbarFilters: function(toolbar) {
						return {};
					}
				}]
			},


			{
				title: 'Trade Restrictions',
				items: [{
					xtype: 'trade-restriction-grid',

					getDefaultClientAccount: function(f) {
						return this.getWindow().getMainForm().formValues.clientInvestmentAccount;
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'InvestmentAccountSecurityTarget'
				}]
			}
		]
	}]
});


Clifton.investment.account.securitytarget.AccountSecurityTargetFrozenAllocationGridPanel = Ext.extend(TCG.grid.EditorGridPanel, {
	name: 'investmentAccountSecurityTargetFreezeListFind',
	saveURL: 'investmentAccountSecurityTargetFreezeSave.json',
	instructions: 'The following allocations are tied to this security target.',
	requestIdDataIndex: true,
	columns: [
		{header: 'ID', width: 10, dataIndex: 'id', hidden: true},
		{header: 'Client Account', width: 80, dataIndex: 'clientInvestmentAccount.label', filter: {searchFieldName: 'clientInvestmentAccountId'}, hidden: true},
		{header: 'Underlying Target', width: 80, dataIndex: 'targetUnderlyingSecurity.name', filter: {searchFieldName: 'targetUnderlyingSecurityId'}, hidden: true},
		{header: 'Type', width: 40, dataIndex: 'freezeType.name', idDataIndex: 'freezeType.id'},
		{header: 'Security', width: 80, dataIndex: 'causeInvestmentSecurity.symbol', idDataIndex: 'causeInvestmentSecurity.id', filter: {searchFieldName: 'causeInvestmentSecurityId'}},
		{
			header: 'Quantity', width: 50, dataIndex: 'freezeUnderlyingQuantity', type: 'float',
			tooltip: 'Frozen underlying quantity tied to the security and applicable to the client account\'s underlying security target. Quantity is always used, and if the target is notional, the notional is calculated from the quantity.',
			editor: {xtype: 'floatfield'}
		},
		{header: 'Note', width: 100, dataIndex: 'freezeNote', renderer: TCG.renderValueWithTooltip},
		{header: 'Cause Table', width: 50, dataIndex: 'causeSystemTable.label', idDataIndex: 'causeSystemTable.id', hidden: true, filter: {type: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded'}},
		{header: 'Cause FK ID', width: 50, dataIndex: 'causeFKFieldId', hidden: true, filter: false},
		{
			header: 'Cause', width: 40, dataIndex: 'causeSystemTable.detailScreenClass', filter: false, sortable: false,
			renderer: function(value, metadata, record) {
				if (TCG.isNotBlank(value) && TCG.isNotBlank(record.data.causeFKFieldId)) {
					return TCG.renderActionColumn('view', 'View', 'View ' + TCG.getValue('causeSystemTable.label', record.json) + ' that is linked to this freeze entry', 'ShowCause');
				}
				return 'N/A';
			},
			eventListeners: {
				'click': function(column, grid, rowIndex, event) {
					const row = grid.store.data.items[rowIndex];
					const clz = row.json.causeSystemTable.detailScreenClass;
					const id = row.json.causeFKFieldId;
					const gridPanel = grid.ownerGridPanel;
					gridPanel.editor.openDetailPage(clz, gridPanel, id, row);
				}
			}
		},
		{header: 'Start Date', width: 50, dataIndex: 'startDate'},
		{header: 'End Date', width: 50, dataIndex: 'endDate'}
	],
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Active On', name: 'activeOnDate', xtype: 'toolbar-datefield'}
		];
	},
	applyTopToolbarFilterLoadParams: function(params) {
		const returnParams = TCG.isNotNull(params) ? params : {};
		const t = this.getTopToolbar();
		const filter = TCG.getChildByName(t, 'activeOnDate');
		if (TCG.isNotBlank(filter.getValue())) {
			returnParams.activeOnDate = filter.getValue().format('m/d/Y');
		}
		return returnParams;
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			TCG.getChildByName(this.getTopToolbar(), 'activeOnDate').setValue(new Date());
		}
		const defaultParams = {
			targetUnderlyingSecurityId: this.getTargetUnderlyingSecurityId(this),
			clientInvestmentAccountId: this.getTargetClientAccountId(this)
		};
		return this.applyTopToolbarFilterLoadParams(defaultParams);
	},
	getClientSecurityTargetId: function(gridPanel) {
		return gridPanel.getWindow().getMainFormId();
	},
	getTargetUnderlyingSecurityId: function(gridPanel) {
		return gridPanel.getWindow().getMainForm().formValues.targetUnderlyingSecurity.id;
	},
	getTargetClientAccountId: function(gridPanel) {
		return gridPanel.getWindow().getMainForm().formValues.clientInvestmentAccount.id;
	},
	getTargetClientAccountServiceName: function(gridPanel) {
		return gridPanel.getWindow().getMainForm().formValues.clientInvestmentAccount.businessService.name;
	},
	addFrozenQuantity: function(addData) {
		const gridPanel = this;
		if (TCG.isNotNull(addData) && Object.keys(addData).length > 0) {
			TCG.data.getDataPromiseUsingCaching('investmentAccountSecurityTargetFreezeTypeByName.json', this, 'securityTarget.freezeType.Manual', {params: {name: 'Manual'}})
				.then(function(freezeType) {
					if (freezeType) {
						addData['freezeType.id'] = freezeType.id;
						const loader = new TCG.data.JsonLoader({
							waitTarget: gridPanel,
							waitMsg: 'Adding...',
							params: addData,
							onLoad: function(record, conf) {
								gridPanel.reload();
							}
						});
						loader.load('investmentAccountSecurityTargetFreezeSave.json');
					}
					else {
						TCG.showError('Unable to fine freeze type with name: Manual', 'Freeze Type Missing');
					}
				});
		}
	},
	editor: {
		detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
		endURL: 'investmentAccountSecurityTargetFreezeEnd.json',
		deleteURL: 'investmentAccountSecurityTargetFreezeDelete.json',
		getDetailPageId: function(gridPanel, row) {
			if (TCG.isNull(row.json.causeInvestmentSecurity)) {
				return this.getGridPanel().getTargetUnderlyingSecurityId(this.getGridPanel());
			}
			return row.json.causeInvestmentSecurity.id;
		},
		addToolbarAddButton: function(toolBar) {
			const gridPanel = this.getGridPanel();
			toolBar.add({
				text: 'Add',
				tooltip: 'Add Frozen Quantity',
				iconCls: 'add',
				menu: new Ext.menu.Menu({
					items: [{
						text: 'Add Freeze For Underlying Quantity',
						iconCls: 'add',
						handler: function() {
							TCG.createComponent('Clifton.investment.account.securitytarget.AddSecurityTargetFrozenAllocationForUnderlyingWindow', {
								defaultData: {
									'clientInvestmentAccount.id': gridPanel.getTargetClientAccountId(gridPanel),
									'targetUnderlyingSecurity.id': gridPanel.getTargetUnderlyingSecurityId(gridPanel),
									startDate: new Date().format('Y-m-d 00:00:00'),
									endDate: '',
									disableValidatingBindingValidation: true
								},
								openerCt: gridPanel
							});
						}
					}, {
						text: 'Add Freeze For Security Of Underlying',
						iconCls: 'add',
						handler: function() {
							TCG.createComponent('Clifton.investment.account.securitytarget.AddSecurityTargetFrozenAllocationForSecurityWindow', {
								defaultData: {
									'clientInvestmentAccount.id': gridPanel.getTargetClientAccountId(gridPanel),
									'targetUnderlyingSecurity.id': gridPanel.getTargetUnderlyingSecurityId(gridPanel),
									startDate: new Date().format('Y-m-d 00:00:00'),
									endDate: '',
									disableValidatingBindingValidation: true
								},
								openerCt: gridPanel
							});
						}
					}]
				})
			});
			toolBar.add('-');
		},
		addToolbarDeleteButton: function(toolBar, gridPanel) {
			const editor = this;
			const deleteHandler = (action, deleteUrl) => {
				const sm = editor.grid.getSelectionModel();
				const confirmSuffix = action && typeof action === 'string' && 'Delete'.toUpperCase() === action.toUpperCase()
					? ' The preferred method of deactivating a Frozen Allocation is to End it so historic Security Target usage can be accurate.' : '';
				if (sm.getCount() === 0) {
					TCG.showError('Please select a Frozen Allocation to ' + action + '.', 'No Frozen Allocation(s) Selected');
				}
				else if (sm.getCount() !== 1) {
					if (editor.allowToDeleteMultiple) {
						Ext.Msg.confirm(action + ' Selected Frozen Allocation(s)', 'Would you like to ' + action + ' all selected Frozen Allocation(s)?' + confirmSuffix, function(a) {
							if (a === 'yes') {
								editor.deleteSelection(sm, gridPanel, deleteUrl);
							}
						});
					}
					else {
						TCG.showError('Multi-selection is not supported yet.  Please select one Frozen Allocation.', 'NOT SUPPORTED');
					}
				}
				else {
					Ext.Msg.confirm(action + ' Selected Frozen Allocation', 'Would you like to ' + action + ' selected Frozen Allocation?' + confirmSuffix, function(a) {
						if (a === 'yes') {
							editor.deleteSelection(sm, gridPanel, deleteUrl);
						}
					});
				}
			};

			toolBar.add({
				text: 'End',
				xtype: 'splitbutton',
				tooltip: 'End selected item(s) for Current Day - 1',
				iconCls: 'remove',
				handler: function() {
					deleteHandler('End', editor.endURL);
				},
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'End',
							tooltip: 'End selected item(s) for Current Day - 1',
							iconCls: 'remove',
							handler: function() {
								deleteHandler('End', editor.endURL);
							}
						},
						{
							text: 'Delete',
							tooltip: 'Delete selected item(s)',
							iconCls: 'remove',
							handler: function() {
								deleteHandler('Delete', editor.deleteURL);
							}
						}
					]
				})
			});
			toolBar.add('-');
		}
	}
});
Ext.reg('accounting-security-target-frozen-allocation-gridpanel', Clifton.investment.account.securitytarget.AccountSecurityTargetFrozenAllocationGridPanel);

Clifton.investment.account.securitytarget.BaseAddWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'OVERRIDE',
	iconCls: 'shopping-cart',
	height: 300,
	width: 400,
	modal: true,
	okButtonText: 'Add',
	okButtonTooltip: 'Add the specified freeze quantity for the underlying security target',

	items: [],

	getFormDataForSubmit: function(form) {
		return {};
	},

	saveWindow: function() {
		const window = this;

		const forms = window.getFormPanels(!this.forceModified),
			panel = forms[0],
			form = panel.getForm(),
			invalidField = panel.getFirstInValidField();

		if (TCG.isNull(invalidField)) {
			const openerWindow = window.openerCt;
			const formData = window.getFormDataForSubmit(form);
			if (form.isDirty() && TCG.isNotNull(formData) && Object.keys(formData).length > 0) {
				if (openerWindow && openerWindow.addFrozenQuantity) {
					openerWindow.addFrozenQuantity.call(openerWindow, Ext.applyIf(formData, window.defaultData));
					this.close();
				}
				else {
					TCG.showError('Opener component must be defined and must have addFrozenQuantity(params) method.', 'Callback Missing');
				}
			}
			else {
				this.close();
			}
		}
		else {
			TCG.showError('Please correct validation error(s) before submitting.', 'Validation Error(s)');
		}
	}
});

Clifton.investment.account.securitytarget.AddSecurityTargetFrozenAllocationForUnderlyingWindow = Ext.extend(Clifton.investment.account.securitytarget.BaseAddWindow, {
	title: 'Add Underlying Freeze Quantity',

	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'Specify the quantity of the underlying security to freeze',
			defaults: {
				anchor: '-20'
			},

			getDefaultData: function(window) {
				return window.defaultData;
			},

			items: [
				{fieldLabel: 'Quantity', name: 'quantity', xtype: 'currencyfield', allowBlank: false, qtip: 'Enter the quantity of the underlying security target to freeze'},
				{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', allowBlank: false},
				{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', allowBlank: true},
				{fieldLabel: 'Freeze Note', name: 'freezeNote', xtype: 'textarea', allowBlank: false, qtip: 'Enter a note associated to this frozen quantity'}
			]
		}
	],

	getFormDataForSubmit: function(form) {
		let endDate = form.findField('endDate').getValue();
		if (TCG.isNotBlank(endDate)) {
			endDate = endDate.format('m/d/Y');
		}
		return {
			freezeUnderlyingQuantity: form.findField('quantity').getValue(),
			freezeNote: form.findField('freezeNote').getValue(),
			startDate: form.findField('startDate').getValue().format('m/d/Y'),
			endDate: endDate
		};
	}
});

Clifton.investment.account.securitytarget.AddSecurityTargetFrozenAllocationForSecurityWindow = Ext.extend(Clifton.investment.account.securitytarget.BaseAddWindow, {
	title: 'Add Security Freeze Quantity',
	height: 325,

	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'Specify the quantity of the underlying security to freeze',
			defaults: {
				anchor: '-20'
			},

			getDefaultData: function(window) {
				return window.defaultData;
			},

			items: [
				{
					fieldLabel: 'Security', name: 'security.symbol', hiddenName: 'security.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', allowBlank: false,
					beforequery: function(queryEvent) {
						const gridPanel = TCG.getParentFormPanel(queryEvent.combo).ownerCt.openerCt;
						queryEvent.combo.store.baseParams = {
							underlyingSecurityId: gridPanel.getTargetUnderlyingSecurityId(gridPanel)
						};
						const clientServiceName = gridPanel.getTargetClientAccountServiceName(gridPanel);
						if (clientServiceName.includes('DeltaShift')) {
							// DeltaShift is a covered call writing strategy
							queryEvent.combo.store.setBaseParam('optionType', 'CALL');
						}
					}
				},
				{fieldLabel: 'Quantity', name: 'quantity', xtype: 'currencyfield', allowBlank: false, qtip: 'Enter the quantity of the specified security to freeze. The quantity will be normalized to the underlying security quantity using the security multiplier.'},
				{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', allowBlank: false},
				{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', allowBlank: true},
				{fieldLabel: 'Freeze Note', name: 'freezeNote', xtype: 'textarea', allowBlank: false, qtip: 'Enter a note associated to this frozen quantity'}
			]
		}
	],

	getFormDataForSubmit: function(form) {
		let endDate = form.findField('endDate').getValue();
		if (TCG.isNotBlank(endDate)) {
			endDate = endDate.format('m/d/Y');
		}
		return {
			'causeInvestmentSecurity.id': form.findField('security.id').getValue(),
			freezeCauseSecurityQuantity: form.findField('quantity').getValue(),
			freezeNote: form.findField('freezeNote').getValue(),
			startDate: form.findField('startDate').getValue().format('m/d/Y'),
			endDate: endDate
		};
	}
});
