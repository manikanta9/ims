Clifton.investment.account.AccountGroupTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Account Group Type',
	iconCls: 'account',
	width: 800,
	height: 500,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Investment Account Group Types can be used to classify account groups.  If a type is selected for a group, then the accounts associated with that group must be the selected type.',
					url: 'investmentAccountGroupType.json',
					getDefaultData: function(form) {
						return {accountAllowedInMultipleGroups: true};
					},
					items: [
						{fieldLabel: 'Group Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{boxLabel: 'Allow a primary account to be selected for this group.', name: 'primaryAccountAllowed', xtype: 'checkbox'},
						{boxLabel: 'Allow a single account to be assigned to multiple groups of this type.', name: 'accountAllowedInMultipleGroups', xtype: 'checkbox'},
						{boxLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox', disabled: true}
					]
				}]
			},


			{
				title: 'Account Groups',
				items: [{
					xtype: 'investment-account-group-grid',
					configureAdditionalLoadParams: function(params) {
						params.typeId = this.getWindow().getMainFormId();
					}
				}]
			}
		]
	}]
});
