// not the actual window but a window selector based on Account.Type.IsOurAccount meta-data
Clifton.investment.account.AccountWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'investmentAccount.json',

	getClassName: function(config, entity) {
		const our = entity ? entity.type.ourAccount : config.defaultData.ourAccount;
		if (our) {
			if (config.defaultData && TCG.isNotBlank(config.defaultData.processingType)) {
				config.processingType = config.defaultData.processingType;
			}
			return 'Clifton.investment.account.CliftonAccountWindow';
		}
		return 'Clifton.investment.account.NonCliftonAccountWindow';
	}

});
