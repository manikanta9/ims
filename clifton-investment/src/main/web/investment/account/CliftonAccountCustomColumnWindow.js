Clifton.investment.account.CliftonAccountCustomColumnWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Account',
	iconCls: 'account',
	width: 750,
	height: 600,

	title: 'Account Setup',
	items: [{
		xtype: 'formpanel-custom-fields',
		labelFieldName: 'label',
		url: 'investmentAccount.json',
		getSaveURL: function() {
			return 'systemColumnValueListSave.json';
		},
		labelWidth: 250,
		loadValidation: false,

		items: [
			{name: 'columnGroupName', xtype: 'hidden'},
			{name: 'type.ourAccount', xtype: 'hidden', submitValue: false},
			{fieldLabel: 'Client', name: 'businessClient.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailIdField: 'businessClient.id', submitValue: false},
			{fieldLabel: 'Account Number', name: 'number', xtype: 'displayfield'},
			{fieldLabel: 'Account Name', name: 'name', xtype: 'displayfield'},
			{xtype: 'label', html: '<hr/>'}
		]
	}]
});
