Clifton.investment.account.AccountRelationshipMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Account Relationship Mapping',
	iconCls: 'link',
	width: 700,
	height: 475,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Mapping',
				items: [{
					xtype: 'formpanel',
					instructions: 'Relationships between investment accounts can be created only if there is a mapping here that allows that type of a relationship.',
					url: 'investmentAccountRelationshipMapping.json',
					labelWidth: 170,
					getWarningMessage: function(form) {
						let msg = undefined;
						if (TCG.isFalse(form.formValues.active)) {
							msg = 'This relationship mapping is no longer active!';
						}
						return msg;
					},
					items: [
						{fieldLabel: 'Main Account Type', name: 'mainAccountType.name', hiddenName: 'mainAccountType.id', xtype: 'combo', url: 'investmentAccountTypeListFind.json'},
						{fieldLabel: 'Related Account Type', name: 'relatedAccountType.name', hiddenName: 'relatedAccountType.id', xtype: 'combo', url: 'investmentAccountTypeListFind.json'},
						{fieldLabel: 'Purpose', name: 'purpose.name', hiddenName: 'purpose.id', xtype: 'combo', url: 'investmentAccountRelationshipPurposeListFind.json'},
						{
							fieldLabel: 'Relationship Modify Condition', name: 'relationshipEntityModifyCondition.name', hiddenName: 'relationshipEntityModifyCondition.id', xtype: 'system-condition-combo',
							qtip: 'When adding/editing/deleting relationships for this mapping, the following modify condition must evaluate to true.  This can be used to allow certain users access to modifying specific relationships.'
						},
						{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', allowBlank: 'true'},
						{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', allowBlank: 'true'},
						{fieldLabel: 'Note', name: 'note', xtype: 'textarea'},
						{boxLabel: 'Allow only one relationship for this purpose <b>from main</b> account', name: 'onlyOneAllowedFromMain', xtype: 'checkbox'},
						{boxLabel: 'Allow only one relationship for this purpose <b>to related</b> account', name: 'onlyOneAllowedToRelated', xtype: 'checkbox'},
						{boxLabel: 'Allow Asset Class selection for this relationship', name: 'assetClassAllowed', xtype: 'checkbox'}
					]
				}]
			},
			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'InvestmentAccountRelationshipMapping'
				}]
			}
		]
	}]
});
