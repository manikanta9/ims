Clifton.investment.account.mapping.AccountMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Account Mapping',
	iconCls: 'account',
	width: 600,
	height: 300,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'Allows for the adding of new account mappings. Select an investment account and mapping purpose, then enter a value for the mapping.',
		url: 'investmentAccountMapping.json',
		labelWidth: 150,
		items: [
			{fieldLabel: 'Investment Account', name: 'investmentAccount.label', hiddenName: 'investmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{
				fieldLabel: 'Mapping Purpose', name: 'investmentAccountMappingPurpose.name', hiddenName: 'investmentAccountMappingPurpose.id', xtype: 'combo', requestedProps: 'fieldName|description',
				url: 'investmentAccountMappingPurposeListFind.json',
				listeners: {
					select: function(combo, record, index) {
						const fieldName = record.json.fieldName;
						if (TCG.isNotBlank(fieldName)) {
							const mappedValueField = this.getParentForm().getForm().findField('fieldValue');
							if (TCG.isNotNull(mappedValueField)) {
								mappedValueField.setFieldLabel(fieldName + ': ');
							}
						}
					}
				}
			},
			{fieldLabel: 'Mapped Value', name: 'fieldValue'}
		],
		listeners: {
			afterload: function(window) {
				const fp = this;
				const fieldName = fp.getFormValue('investmentAccountMappingPurpose.fieldName');
				const mappedValueField = fp.getForm().findField('fieldValue');
				if (TCG.isNotNull(mappedValueField)) {
					mappedValueField.setFieldLabel(fieldName + ': ');
				}
			}
		}
	}]
});
