Clifton.investment.account.mapping.AccountMappingPurposeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Account Mapping Purpose',
	iconCls: 'account',
	width: 600,
	height: 300,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'Creates a new Investment Account Mapping Data Purpose entry for use in account mapping. ',
		url: 'investmentAccountMappingPurpose.json',
		items: [
			{fieldLabel: 'Purpose Name', name: 'name', qtip: 'Name used to specifically identify this mapping purpose.'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Field Name', name: 'fieldName', qtip: 'A general name to categorize the mapping purpose.'},
			{boxLabel: 'Allows only 1:1 mapping', name: 'oneAccountValueMappingPerPurpose', xtype: 'checkbox', qtip: 'Check this box to enforce 1:1 mapping of account to a unique value.'}
		]
	}]
});
