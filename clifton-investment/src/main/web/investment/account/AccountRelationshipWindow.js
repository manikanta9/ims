Clifton.investment.account.AccountRelationshipWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Account Relationship',
	iconCls: 'link',
	width: 650,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'An account relationship associates an investment account with another investment account for a particular purpose.  If an asset class is selected then the related account applies only to the selected asset class.',
		url: 'investmentAccountRelationship.json',
		items: [
			{fieldLabel: 'Main Account', name: 'referenceOne.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'referenceOne.id'},
			{
				fieldLabel: 'Purpose', name: 'purpose.name', hiddenName: 'purpose.id', xtype: 'combo', url: 'investmentAccountRelationshipPurposeListFind.json', qtip: 'Displays only those relationship purposes between investment accounts that are allowed by relationship mappings from account type of selected main account.',
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					queryEvent.combo.store.baseParams = {
						mainAccountTypeId: TCG.getValue('referenceOne.type.id', f.formValues)
					};
				}
			},
			{
				fieldLabel: 'Related Account', name: 'referenceTwo.label', hiddenName: 'referenceTwo.id', requiredFields: ['purpose.id'], xtype: 'combo', url: 'investmentAccountListFind.json', valueField: 'id', displayField: 'label',
				detailPageClass: 'Clifton.investment.account.AccountWindow',
				addNew: false,
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					queryEvent.combo.store.baseParams = {
						clientIdOrRelatedClient: TCG.getValue('referenceOne.businessClient.id', f.formValues),
						mappingMainAccountTypeId: TCG.getValue('referenceOne.type.id', f.formValues),
						mappingPurposeId: f.findField('purpose.id').getValue()
					};
				},
				getDefaultData: function(f) { // defaults client for "add new" drill down
					return {
						ourAccount: false,
						businessClient: TCG.getValue('referenceOne.businessClient', f.formValues)
					};
				}
			},
			{
				fieldLabel: 'Security Group', name: 'securityGroup.name', hiddenName: 'securityGroup.id', xtype: 'combo', url: 'investmentSecurityGroupListFind.json'
				, qtip: 'Optionally limits investment securities to the specified group. For example, PUT Options for a particular Custodian relationship while CALL Options to another.  For Sub-Account relationships this can be used to filter which positions should be included.  This is already often filtered by a specific replication depending on its use, but can limit which securities are available to include for exposure.'
			},
			{
				fieldLabel: 'Asset Class', name: 'accountAssetClass.label', hiddenName: 'accountAssetClass.id', xtype: 'combo', url: 'investmentAccountAssetClassListFind.json', displayField: 'label',
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					queryEvent.combo.store.baseParams = {
						accountId: TCG.getValue('referenceOne.id', f.formValues),
						rollupAssetClass: false,
						excludeFromExposure: false
					};
				}
			},
			{fieldLabel: 'Relationship Start', name: 'startDate', xtype: 'datefield'},
			{fieldLabel: 'Relationship End', name: 'endDate', xtype: 'datefield'}
		]
	}]
});
