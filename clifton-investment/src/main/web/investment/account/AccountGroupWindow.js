Clifton.investment.account.AccountGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Account Group',
	iconCls: 'account',
	width: 850,
	height: 500,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Investment Account Groups can be used to classify accounts by similarities.  If a type is selected for a group, then the accounts associated with that group must be the selected type.  System Defined Groups cannot be deleted, and their names cannot be edited.',
					url: 'investmentAccountGroup.json',
					getDefaultData: function(win) {
						const fp = this;
						const dd = win.defaultData || {};
						if (win.defaultDataIsReal) {
							return dd;
						}
						if (dd.type && dd.type.id) {
							fp.disableField('type.name');
						}
						dd.accountAllowedInMultipleGroups = true;
						return dd;
					},
					items: [
						{fieldLabel: 'Group Type', name: 'type.name', hiddenName: 'type.id', xtype: 'combo', url: 'investmentAccountGroupTypeListFind.json'},
						{fieldLabel: 'Group Name', name: 'name'},
						{fieldLabel: 'Group Alias', name: 'groupAlias'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Account Type', name: 'accountType.name', hiddenName: 'accountType.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountTypeListFind.json'},
						{xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: false},
						{
							fieldLabel: 'Rebuild Bean', name: 'rebuildSystemBean.name', hiddenName: 'rebuildSystemBean.id', xtype: 'combo', detailPageClass: 'Clifton.system.bean.BeanWindow', url: 'systemBeanListFind.json?groupName=Investment Account Group Rebuild Executor',
							getDefaultData: function() {
								return {type: {group: {name: 'Investment Account Group Rebuild Executor'}}};
							}
						},
						{fieldLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox', disabled: true},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'InvestmentAccountGroup',
							hierarchyCategoryName: 'Investment Account Group Tags'
						}
					]
				}]
			},


			{
				title: 'Investment Accounts',
				items: [{
					name: 'investmentAccountGroupAccountListFind',
					xtype: 'gridpanel',
					instructions: 'Selected group has the following accounts associated with it. To add another account to this group, select desired account from the list and click the [Add] button.',
					getLoadParams: function() {
						const f = this.getWindow().getMainFormPanel();
						const type = f.getFormValue('type');
						const cm = this.getColumnModel();
						if (type && type.primaryAccountAllowed === true) {
							cm.setHidden(cm.findColumnIndex('primary'), false);
						}
						else {
							cm.setHidden(cm.findColumnIndex('primary'), true);
						}

						return {groupId: this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'AccountID', width: 15, dataIndex: 'referenceTwo.id', type: 'int', filter: {searchFieldName: 'accountId'}, hidden: true},
						{header: 'Client', width: 150, dataIndex: 'referenceTwo.businessClient.name', filter: {searchFieldName: 'clientName'}},
						{header: 'Account Type', width: 75, dataIndex: 'referenceTwo.type.name', filter: {searchFieldName: 'accountTypeName'}},
						{header: 'Account', width: 170, dataIndex: 'referenceTwo.label', filter: {searchFieldName: 'accountLabel'}, defaultSortColumn: true},
						{header: 'Service', width: 110, dataIndex: 'referenceTwo.businessService.name', filter: {type: 'combo', queryParam: 'nameExpanded', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true},
						{header: 'Team', width: 75, dataIndex: 'referenceTwo.teamSecurityGroup.name', filter: {searchFieldName: 'teamName'}},
						{header: 'Base CCY', width: 60, dataIndex: 'referenceTwo.baseCurrency.symbol', filter: {searchFieldName: 'baseCurrencyId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true'}, hidden: true},
						{header: 'Workflow State', width: 75, dataIndex: 'referenceTwo.workflowState.name', filter: {searchFieldName: 'workflowStateName', comparison: 'BEGINS_WITH'}},
						{header: 'Workflow Status', width: 70, dataIndex: 'referenceTwo.workflowStatus.name', filter: {searchFieldName: 'workflowStatusName', comparison: 'BEGINS_WITH'}, hidden: true},
						{header: 'Issued By', width: 100, dataIndex: 'referenceTwo.issuingCompany.name', filter: {searchFieldName: 'issuingCompanyName'}},
						{header: 'Primary', width: 40, dataIndex: 'primary', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.account.AccountWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.referenceTwo.id;
						},
						deleteURL: 'investmentAccountGroupAccountDelete.json',
						getDeleteParams: function(selectionModel) {
							return {
								groupId: this.getWindow().getMainFormId(),
								accountId: selectionModel.getSelected().get('referenceTwo.id')
							};
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							const grpId = gridPanel.getWindow().getMainFormId();
							const f = gridPanel.getWindow().getMainForm();

							const systemManaged = TCG.getValue('systemManaged', gridPanel.getWindow().getMainForm().formValues);
							if (systemManaged === true) {
								toolBar.add({
									text: 'Rebuild',
									tooltip: 'Rebuild accounts for this group',
									iconCls: 'run',
									handler: function() {
										TCG.data.getDataPromise('investmentAccountGroupRebuild.json', gridPanel, {
											waitMsg: 'Rebuilding...',
											params: {groupId: grpId}
										}).then(result => {
											TCG.createComponent('Clifton.core.StatusWindow', {
												title: 'Investment Account Group Rebuild Status',
												defaultData: {status: result}
											});
											gridPanel.reload();
										});
									}
								});
								toolBar.add('-');
							}
							else {
								let url = 'investmentAccountListFind.json?excludeInvestmentAccountGroupId=' + TCG.getValue('id', f.formValues);

								const accountType = TCG.getValue('accountType.id', f.formValues);
								if (accountType) {
									url += '&accountTypeId=' + accountType;
								}

								toolBar.add(new TCG.form.ComboBox({name: 'account', displayField: 'label', url: url, width: 150, listWidth: 230}));
								toolBar.add({
									text: 'Add',
									tooltip: 'Add the account to selected group',
									iconCls: 'add',
									handler: function() {
										const accountId = TCG.getChildByName(toolBar, 'account').getValue();
										if (accountId === '') {
											TCG.showError('You must first select desired account from the list.');
										}
										else {
											const groupId = gridPanel.getWindow().getMainFormId();
											const loader = new TCG.data.JsonLoader({
												waitTarget: gridPanel,
												waitMsg: 'Linking...',
												params: {groupId: groupId, accountId: accountId},
												onLoad: function(record, conf) {
													gridPanel.reload();
													TCG.getChildByName(toolBar, 'account').reset();
												}
											});
											loader.load('investmentAccountGroupToAccountLink.json');
										}
									}
								});
								toolBar.add('-');
								const type = TCG.getValue('type', this.getWindow().getMainForm().formValues);
								if (type && type.primaryAccountAllowed === true) {
									toolBar.add({
										text: 'Primary Account',
										tooltip: 'Set the selected account as the primary account.',
										iconCls: 'run',
										handler: function() {
											Ext.Msg.confirm('Account Group', 'This will remove the current primary account designation, and set the selected account as primary.  If the selected account is the primary, then the primary designation will be removed.<br/>Would you like to continue?', function(a) {
												if (a === 'yes') {
													const sm = gridPanel.grid.getSelectionModel();
													if (sm.getCount() !== 1) {
														TCG.showError('Please select a single order to allocate.', 'Incorrect Selection');
													}
													else {
														const link = sm.getSelections()[0].json;
														link.primary = !(link.primary && link.primary === true);
														const loader = new TCG.data.JsonLoader({
															waitTarget: gridPanel,
															waitMsg: 'Saving...',
															params: {id: link.id, primary: link.primary},
															onLoad: function(record, conf) {
																gridPanel.reload();
																TCG.getChildByName(toolBar, 'account').reset();
															}
														});
														loader.load('investmentAccountGroupAccountSave.json');
													}
												}
											});
										}
									});
									toolBar.add('-');
								}
							}
						}
					}
				}]
			}
		]
	}]
});
