TCG.use('Clifton.investment.account.targetexposure.TargetExposureAdjustmentFieldSet');

Clifton.investment.account.assetclass.AssetClassWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Account Asset Class',
	iconCls: 'grouping',
	width: 800,
	height: 600,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel-custom-fields',
					columnGroupName: 'Account Asset Class Custom Fields',
					dynamicFieldFormFragment: 'investmentAccountAssetClassCustomFields',
					url: 'investmentAccountAssetClass.json?requestedPropertiesToExcludeGlobally=targetExposureAssignmentCombinedList|jsonValue',
					labelFieldName: 'account.label',
					labelWidth: 140,
					loadDefaultDataAfterRender: true,

					getWarningMessage: function(form) {
						let msg = undefined;
						if (TCG.isTrue(form.formValues.inactive)) {
							msg = 'This account asset class is no longer active for this account.';
						}
						return msg;
					},

					listeners: {
						afterload: function(panel) {
							this.setResetExcludeFromExposureFields();
						}
					},

					setResetClientDirectedCashFields: function() {
						// If anything but "NONE" is selected, enable cash amount/date fields
						const f = this.getForm();
						const cd = f.findField('clientDirectedCashOption');
						const cdco = cd.getGroupValue();

						const cdd = f.findField('clientDirectedCashDate');
						const cda = f.findField('clientDirectedCashAmount');
						if (cdco === 'TARGET' || cdco === 'TARGET_PREVIOUS_DURATION') {
							cda.el.up('.x-form-item', 10, true).child('.x-form-item-label').update('Overlay Target:');
							cda.fieldLabel = 'Overlay Target';
							cda.setDisabled(false);
							cda.setVisible(true);

							cdd.setDisabled(false);
							cdd.setVisible(true);
						}
						else if (cdco === 'CONSTANT') {
							cda.el.up('.x-form-item', 10, true).child('.x-form-item-label').update('Overlay Target Adjustment:');
							cda.fieldLabel = 'Overlay Target Adjustment';
							cda.setDisabled(false);
							cda.setVisible(true);

							cdd.setDisabled(false);
							cdd.setVisible(true);
						}
						else {
							cda.setDisabled(true);
							cda.setVisible(false);

							cdd.setDisabled(true);
							cdd.setVisible(false);
						}
					},

					setResetExcludeFromExposureFields: function() {
						// If Exclude in Exposure Checkbox is selected - Need to disable most fields, if unchecked - enable them
						// Just disable the fieldsets - code will handle clearing values and setting targets to zero
						const f = this.getForm();
						const exclude = f.findField('excludeFromExposure').getValue();
						const excludeFromOverlay = f.findField('excludeFromOverlayExposure').getValue();

						const benchmarkField = f.findField('benchmarkSecurity.label');
						const benchmarkDurationField = f.findField('benchmarkDurationSecurity.label');
						const percentField = f.findField('assetClassPercentage');
						const cashPercentField = f.findField('cashPercentage');
						const clientDirectedFieldset = TCG.getChildByName(this.ownerCt, 'clientDirectedCashUsedFieldset');
						const targetExposureFieldset = TCG.getChildByName(this.ownerCt, 'targetExposureUsedFieldset');
						const replicationFieldset = TCG.getChildByName(this.ownerCt, 'replicationUsedFieldset');
						const rebalancingFieldset = TCG.getChildByName(this.ownerCt, 'rebalancingUsedFieldset');
						const performanceFieldset = TCG.getChildByName(this.ownerCt, 'performanceFieldset');

						if (exclude === true || excludeFromOverlay === true) {
							if (clientDirectedFieldset.checkbox) {
								clientDirectedFieldset.checkbox.dom.checked = false;
								clientDirectedFieldset.onCheckClick();
							}
						}
						clientDirectedFieldset.setDisabled(exclude === true || excludeFromOverlay === true);

						if (exclude === true) {
							benchmarkField.reset();
							benchmarkDurationField.reset();
							percentField.setValue(0);
							cashPercentField.setValue(0);

							if (targetExposureFieldset.checkbox) {
								targetExposureFieldset.checkbox.dom.checked = false;
								targetExposureFieldset.onCheckClick();
							}
							if (replicationFieldset.checkbox) {
								replicationFieldset.checkbox.dom.checked = false;
								replicationFieldset.onCheckClick();
							}
							if (rebalancingFieldset.checkbox) {
								rebalancingFieldset.checkbox.dom.checked = false;
								rebalancingFieldset.onCheckClick();
							}
						}
						benchmarkField.setDisabled(exclude);
						benchmarkDurationField.setDisabled(exclude);
						percentField.setDisabled(exclude);
						cashPercentField.setDisabled(exclude);
						targetExposureFieldset.setDisabled(exclude);
						replicationFieldset.setDisabled(exclude);
						rebalancingFieldset.setDisabled(exclude);
						performanceFieldset.setDisabled(exclude);
					},

					items: [
						{fieldLabel: 'Client Account', name: 'account.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'account.id'},

						{fieldLabel: 'Asset Class', name: 'assetClass.name', hiddenName: 'assetClass.id', xtype: 'combo', url: 'investmentAssetClassListFind.json', detailPageClass: 'Clifton.investment.setup.AssetClassWindow'},
						{fieldLabel: 'Display Name', name: 'displayName', qtip: 'A name for this Account Asset Class that, if defined, replaces the Asset Class Name on reports, messages, and within displayed user interface components.'},
						{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield', minValue: 0, maxValue: 1000, qtip: 'Sort order used to display asset classes. For nexted asset classes, use hundreds for parent asset classes and corresponding tens for child asset classes.'},
						{
							boxLabel: 'Exclude this asset class from account exposure', name: 'excludeFromExposure', xtype: 'checkbox',
							listeners: {
								check: function(f) {
									const p = TCG.getParentFormPanel(f);
									p.setResetExcludeFromExposureFields();
								}
							},
							qtip: 'If checked, all other fields (replications, rebalancing, etc) are cleared, targets are set to 0. The asset class can be allocated to by managers, but can\'t be used by other asset classes for TFA, etc. During Portfolio runs, it will be skipped in asset class processing and will not show up in exposure.  Any amounts allocated to it is ignored.'
						},
						{
							boxLabel: 'Exclude this asset class from overlay exposure', name: 'excludeFromOverlayExposure', xtype: 'checkbox',
							listeners: {
								check: function(f) {
									const p = TCG.getParentFormPanel(f);
									p.setResetExcludeFromExposureFields();
								}
							},
							qtip: 'If checked, overlay exposure for the asset class is always set to 0.'
						},
						{
							boxLabel: 'Private Asset Class (Exclude From Reports)', name: 'privateAssetClass', xtype: 'checkbox',
							qtip: 'If checked, then this asset class should be excluded from reports.  This option has no affect on processing of Portfolio runs.'
						},
						{fieldLabel: 'Benchmark', name: 'benchmarkSecurity.label', hiddenName: 'benchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Benchmark Duration', name: 'benchmarkDurationSecurity.label', hiddenName: 'benchmarkDurationSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Duration Field Override', name: 'durationFieldNameOverride', xtype: 'system-list-combo', listName: 'Portfolio: Duration Field', qtip: 'Optional duration field name override to use for the Benchmark Duration and replications of this asset class. If blank, \'Duration\' is used.'},
						{fieldLabel: 'Target Exposure %', name: 'assetClassPercentage', xtype: 'currencyfield', decimalPrecision: 4},
						{fieldLabel: 'Fund Cash %', name: 'cashPercentage', xtype: 'currencyfield', decimalPrecision: 10, qtip: 'Target the following exposure for the Fund Cash bucket. Usually the same as Target Exposure but may change based on how alternative asset class(es) should be allocated.'},

						{
							xtype: 'fieldset-checkbox',
							title: 'Client Directed Trading',
							checkboxName: 'clientDirectedCashUsed',
							name: 'clientDirectedCashUsedFieldset',
							instructions: 'Uses the following selection to apply Client Directed Cash to this asset class in order to adjust or set overlay target.',
							labelWidth: 10,

							items: [
								{
									xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'clientDirectedCashOption-Group',
									anchor: '0',
									items: [
										{boxLabel: 'Do not use', name: 'clientDirectedCashOption', inputValue: 'NONE'},
										{
											boxLabel: 'Use to fulfill specified Overlay Target on specified date and afterwards use target = actual:', name: 'clientDirectedCashOption', inputValue: 'TARGET',
											qtip: '1.  On the Portfolio run with the balance date of the given date, the asset class will adjust its overlay target by setting the client directed cash in order to fulfill the entered target amount.' +
												'<br/><br/>' +
												'2.  For Portfolio runs with balance dates following the given date, client directed cash will be entered in order to fulfill Overlay Target = Overlay Exposure.'
										},
										{
											boxLabel: 'Use to fulfill specified Overlay Target on specified date and afterwards calculate target = actual using previous duration:', name: 'clientDirectedCashOption', inputValue: 'TARGET_PREVIOUS_DURATION',
											qtip: '1.  On the Portfolio run with the balance date of the given date, the asset class will adjust its overlay target by setting the client directed cash in order to fulfill the entered target amount.' +
												'<br/><br/>' +
												'2.  For Portfolio runs with balance dates following the given date, client directed cash will be entered in order to fulfill Overlay Target = Recalculated Overlay Exposure using Previous Duration Adjustment.' +
												'<br><br><b>NOTE:</b> Previous Run is determined as <br>' +
												'1. If there are any MOC runs on the previous day - will use the last MOC run submitted to trading on the previous day.<br>' +
												'2. If there are not any MOCs runs on the previous day - will use the previous day\'s Main run.'
										},
										{
											boxLabel: 'Use the following constant amount on specified date only:', name: 'clientDirectedCashOption', inputValue: 'CONSTANT',
											qtip: '1.  On the Portfolio run with the balance date of the given date, the asset class will adjust its overlay target by using the entered amount in the Client Directed Cash bucket.' +
												'<br/><br/>' +
												'2.  For Portfolio runs with balance dates following the given date, client directed cash will not be used.'
										},
										{
											xtype: 'formfragment',
											frame: false,
											bodyStyle: 'padding-left: 30px',
											labelWidth: 200,
											items: [
												{fieldLabel: 'Cash Amount', name: 'clientDirectedCashAmount', xtype: 'currencyfield'},
												{fieldLabel: 'Balance Date', name: 'clientDirectedCashDate', xtype: 'datefield'}
											]
										}
									],
									listeners: {
										change: function(f) {
											const p = TCG.getParentFormPanel(f);
											p.setResetClientDirectedCashFields();
										}
									}
								}
							]
						},

						{
							xtype: 'investment-account-targetexposure-adjustment-fieldset'
						},

						{
							xtype: 'investment-account-targetexposure-adjustment-fieldset',
							name: 'cashAdjustmentFormFragment',
							allowedOptions: ['NONE', 'MANAGER_MARKET_VALUE', 'PERCENT_OF_TARGET', 'DYNAMIC_TARGET_PERCENT'],
							fieldSetTitle: 'Fund Cash Adjustments',
							fieldSetCheckboxName: 'cashAdjustmentUsed',
							fieldSetName: 'cashExposureUsedFieldSet',
							fieldSetFieldPrefix: 'cashAdjustment',
							cashAdjustment: true,
							fieldSetInstructions: 'Use the following selection to change cash/fund exposure for this asset class and redistribute the difference across the following asset classes either proportionally or using specified percentage allocation that adds up to 100%.  Proportional allocation uses the asset classes current targets to determine its share of the adjustment.  For asset classes that do not have a target, then a percentage must be specified.',
							fieldSetCashAssignmentGridStoreRoot: 'cashAdjustmentAssignmentList'
						},

						{
							xtype: 'fieldset-checkbox',
							title: 'Replications',
							checkboxName: 'replicationUsed',
							name: 'replicationUsedFieldset',
							instructions: 'Secondary Replication Amount specifies the amount of daily notional that should be allocated to the secondary replication in addition to existing position.  It is reset to 0 every night.  The rest of the notional is allocated to the primary replication.',
							labelWidth: 130,
							items: [
								{fieldLabel: 'Primary', name: 'primaryReplication.name', hiddenName: 'primaryReplication.id', xtype: 'combo', url: 'investmentReplicationListFind.json', detailPageClass: 'Clifton.investment.replication.ReplicationWindow'},
								{fieldLabel: 'Secondary', name: 'secondaryReplication.name', hiddenName: 'secondaryReplication.id', xtype: 'combo', url: 'investmentReplicationListFind.json', detailPageClass: 'Clifton.investment.replication.ReplicationWindow', requiredFields: ['primaryReplication.id'], doNotClearIfRequiredChanges: true},
								{fieldLabel: 'Secondary Amount', name: 'secondaryReplicationAmount', xtype: 'currencyfield', requiredFields: ['secondaryReplication.id'], doNotClearIfRequiredChanges: true},
								{
									boxLabel: 'Exclude positions allocated to this Replication from Count', name: 'replicationPositionExcludedFromCount', xtype: 'checkbox', requiredFields: ['primaryReplication.id'], doNotClearIfRequiredChanges: true,
									qtip: 'Can be used for cases where a contract is duplicated across asset classes.  For example, for Defensive Equity the Options exposure is included in a matching replication, however, the market value is included in a separate asset class for the same contracts.' +
										'<br/></br/>When checked, positions allocated to this replication are excluded from:' +
										'<br/>&nbsp;&nbsp;1. the total position count in the run vs. account positions' +
										'<br/>&nbsp;&nbsp;2. contract splitting with other asset classes that do not have this flag checked' +
										'<br/>&nbsp;&nbsp;3. trade entry.  Trades are assumed to be entered in other asset class replications.  Trade entry screen automatically applies buys/sells when entered elsewhere to see trade impact in this asset class.'
								},
								{boxLabel: 'Use Actual for Matching Replication Targets (If unchecked uses target)', name: 'useActualForMatchingReplication', xtype: 'checkbox', requiredFields: ['primaryReplication.id'], doNotClearIfRequiredChanges: true},
								{
									boxLabel: 'Do not adjust contract value (Notional = Price*Qty*Multiplier)', name: 'doNotAdjustContractValue', xtype: 'checkbox', requiredFields: ['primaryReplication.id'], doNotClearIfRequiredChanges: true,
									qtip: 'If checked, does not apply synthetic adjustment or duration adjustments (determined by replication types) for calculating contract values'
								}
							]
						},

						{
							xtype: 'fieldset-checkbox',
							title: 'Rebalancing Configuration',
							checkboxName: 'rebalancingUsed',
							name: 'rebalancingUsedFieldset',
							instructions: 'All rebalancing targets and absolutes should be entered as positive values.  All absolute values must be greater than or equal to their target counterparts.',
							labelWidth: 130,
							items: [
								{
									fieldLabel: 'Exposure Target (%)', xtype: 'currencyfield', name: 'rebalanceExposureTarget', minValue: 0, maxValue: 100, decimalPrecision: 4,
									qtip: 'Defines rebalancing targets only for a sub-set of asset classes (investable asset classes that we do replicate). Percentage value is used as target exposure for Portfolio exposure reports that only report against these asset classes.  During Portfolio runs, if set for any asset class, sum across all asset classes must equal 100%'
								},
								{
									fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
									defaults: {
										xtype: 'displayfield',
										flex: 1
									},
									items: [
										{value: 'Minimum %'},
										{value: 'Maximum %'}
									]
								},
								{
									fieldLabel: 'Trigger Range', xtype: 'compositefield',
									qtip: 'Triggers are used to maintain a band where an asset class can fluctuate before rebalancing is recommended. When this happens warnings are generated on the PIOS run.',
									defaults: {
										xtype: 'currencyfield',
										flex: 1
									},
									items: [
										{name: 'rebalanceAllocationMin', minValue: 0},
										{name: 'rebalanceAllocationMax', minValue: 0}
									]
								},
								{
									fieldLabel: 'Absolute Trigger', xtype: 'compositefield',
									qtip: 'Similar to Triggers, the Absolute Triggers can be used to define the <b>most</b> out of balance an asset class can be defined by the client.',
									defaults: {
										xtype: 'currencyfield',
										flex: 1
									},
									items: [
										{name: 'rebalanceAllocationAbsoluteMin', minValue: 0},
										{name: 'rebalanceAllocationAbsoluteMax', minValue: 0}
									]
								},
								{fieldLabel: 'Rebalance Benchmark', name: 'rebalanceBenchmarkSecurity.label', hiddenName: 'rebalanceBenchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow', mutuallyExclusiveFields: ['skipRebalanceCashAdjustment']},
								{boxLabel: 'Do not adjust rebalance cash based on benchmark performance', name: 'skipRebalanceCashAdjustment', xtype: 'checkbox', mutuallyExclusiveFields: ['rebalanceBenchmarkSecurity.id']},
								{
									fieldLabel: 'Trigger Action', name: 'rebalanceTriggerActionLabel', hiddenName: 'rebalanceTriggerAction', displayField: 'name', valueField: 'triggerAction', mode: 'local', xtype: 'combo',
									store: {
										xtype: 'arraystore',
										fields: ['name', 'triggerAction', 'description'],
										data: Clifton.investment.account.assetclass.RebalanceActions
									}
								},
								{
									boxLabel: 'Apply Values as Absolute Bands (if left unchecked rebalance triggers will be calculated Proportionally)', name: 'rebalanceAllocationFixed', xtype: 'checkbox',
									qtip: 'Absolute Bands means the percentages entered are used as is.  Proportional bands are calculated as the given percent of the adjusted target.<br><br>Example: A percentage of 1% used as absolute bands is 1% of the Total Portfolio Value.  A percentage of 1% calculated proportionally for an asset class with a 25% target would result in a band that is 0.25% of the total portfolio value.'
								}
							]
						},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							collapsed: true,
							tableName: 'InvestmentAccountAssetClass'
						},
						{
							xtype: 'fieldset',
							title: 'Custom Fields',
							collapsed: true,
							items: [{
								xtype: 'formfragment',
								frame: false,
								name: 'investmentAccountAssetClassCustomFields',
								items: []
							}]
						},
						{
							xtype: 'fieldset',
							title: 'Performance Summaries',
							name: 'performanceFieldset',
							collapsed: true,
							items: [
								{
									fieldLabel: 'Overlay Target Source', name: 'performanceOverlayTargetSourceLabel', hiddenName: 'performanceOverlayTargetSource', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', value: 'TARGET_EXPOSURE',
									store: {
										xtype: 'arraystore',
										fields: ['name', 'value', 'description'],
										data: [
											['Target Exposure', 'TARGET_EXPOSURE', 'Target Exposure'],
											['Actual Exposure', 'ACTUAL_EXPOSURE', 'Actual Exposure'],
											['Market Value Include Cash', 'MARKET_VALUE_INCLUDE_CASH', 'Value is calculated as the total market value of the account (positions + cash) and can only be used for accounts with one asset class. Generally used for fully funded accounts where our run targets are based off of custodian values, but for performance we use total market value.']
										]
									}
								}
							]

						}
					]
				}]
			},


			{
				title: 'Manager Account Allocations',
				items: [{
					name: 'investmentManagerAccountAllocationListFind',
					xtype: 'gridpanel',
					instructions: 'The following manager account assignments have been allocated to selected client account asset class.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'AssignmentID', width: 15, dataIndex: 'managerAccountAssignment.id', hidden: true},
						{header: 'Manager', width: 100, dataIndex: 'managerAccountAssignment.managerLabel'},
						{header: 'Account', width: 80, hidden: true, dataIndex: 'managerAccountAssignment.referenceOne.labelShort'},
						{header: 'Type', dataIndex: 'managerAccountAssignment.referenceOne.managerType', width: 40},
						{header: 'Overlay Type', dataIndex: 'managerAccountAssignment.cashOverlayType', width: 80},
						{header: 'Custom Cash Allocation', width: 35, dataIndex: 'customCashOverlay', type: 'boolean', hidden: true},
						{header: 'Assignment Cash Type', width: 50, dataIndex: 'managerAccountAssignment.cashType', hidden: true},
						{
							header: 'Cash Type', width: 50, dataIndex: 'cashType',
							renderer: function(v, metaData, r) {
								if (TCG.isNotBlank(v)) {
									return v;
								}
								else if (r.data['managerAccountAssignment.cashOverlayType'] !== 'CUSTOM' && r.data['managerAccountAssignment.cashOverlayType'] !== 'NONE') {
									return r.data['managerAccountAssignment.cashType'];
								}
								else {
									return null;
								}
							}
						},
						{header: 'Allocation %', width: 50, dataIndex: 'allocationPercent', type: 'percent', numberFormat: '0,000.0000000000'},
						{header: 'Private', width: 35, dataIndex: 'privateAssignment', type: 'boolean'},
						{header: 'Inactive', width: 35, dataIndex: 'managerAccountAssignment.inactive', type: 'boolean', filter: {searchFieldName: 'inactiveAssignment'}},
						{
							width: 25, fixed: true, dataIndex: 'managerAccountAssignment.referenceOne.id', filter: false, sortable: false,
							renderer: function(v, args, r) {
								return TCG.renderActionColumn('manager', '', 'Open Manager Account', 'OpenManagerAccount');
							}
						}
					],
					editor: {
						detailPageClass: 'Clifton.investment.manager.assignment.AssignmentWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.managerAccountAssignment.id;
						}
					},
					gridConfig: {
						listeners: {
							'rowclick': function(grid, rowIndex, evt) {
								if (TCG.isActionColumn(evt.target)) {
									const row = grid.store.data.items[rowIndex];
									const mgrId = row.json.managerAccountAssignment.referenceOne.id;
									const gridPanel = grid.ownerGridPanel;
									gridPanel.editor.openDetailPage('Clifton.investment.manager.AccountWindow', gridPanel, mgrId, row);
								}
							}
						}
					},
					getLoadParams: function() {
						const fv = this.getWindow().getMainForm().formValues;
						return {
							assetClassId: fv.assetClass.id,
							investmentAccountId: fv.account.id
						};
					}
				}]
			},


			{
				title: 'Rebalance History',
				items: [{
					name: 'investmentAccountRebalanceAssetClassHistoryListFind',
					xtype: 'gridpanel',
					instructions: 'The following contains records for all asset class rebalance history for this asset class.',
					wikiPage: 'IT/Investment Rebalancing',
					columns: [
						{header: 'HistoryID', width: 15, dataIndex: 'rebalanceHistory.id', hidden: true, filter: false, sortable: false},
						{header: 'Date', dataIndex: 'rebalanceHistory.rebalanceDate', width: 50, filter: {searchFieldName: 'rebalanceDate'}},
						{header: 'Calculation Type', dataIndex: 'rebalanceHistory.rebalanceCalculationTypeLabel', width: 75, filter: false},
						{header: 'Before Rebalance Value', width: 75, dataIndex: 'beforeRebalanceCash', type: 'currency'},
						{header: 'After Rebalance Value', width: 75, dataIndex: 'afterRebalanceCash', type: 'currency'},
						{header: 'Create Date', width: 75, dataIndex: 'rebalanceHistory.createDate', filter: {searchFieldName: 'historyCreateDate'}}
					],
					editor: {
						detailPageClass: 'Clifton.investment.account.assetclass.rebalance.HistoryWindow',
						drillDownOnly: true,
						getDetailPageId: function(grid, row) {
							return row.json.rebalanceHistory.id;
						}
					},
					getLoadParams: function() {
						return {
							accountAssetClassId: this.getWindow().getMainFormId()
						};
					}
				}]
			},


			{
				title: 'Performance Benchmarks',
				items: [{
					name: 'investmentAccountAssetClassBenchmarkListFind',
					xtype: 'gridpanel',
					instructions: 'The following performance benchmarks should be used for the given time periods.  Start/End dates cannot overlap.  If multiple performance benchmarks are applied for a given month, the system will calculate benchmark returns based on each date range.  If nothing is defined in this table for the given period, the system will use the asset class benchmark for performance calculations.  Note: A blank security in the below list implies the client account asset class return is used as the benchmark return.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Security', width: 150, dataIndex: 'benchmarkLabel'},
						{header: 'Start Date', width: 50, dataIndex: 'startDate'},
						{header: 'End Date', width: 50, dataIndex: 'endDate'},
						{header: 'Active', width: 35, dataIndex: 'active', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.account.assetclass.BenchmarkWindow',
						getDefaultData: function(gridPanel) {
							return {
								referenceOne: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function() {
						return {
							referenceOneId: this.getWindow().getMainFormId()
						};
					}
				}]

			},


			{
				title: 'Position Allocation',
				items: [{
					xtype: 'investment-accountAssetClass-PositionAllocation',
					showAccountColumn: false,
					showAssetClassColumn: false,
					getAccountAssetClass: function() {
						return this.getWindow().getMainForm().formValues;
					}
				}]

			}
		]
	}]
});
