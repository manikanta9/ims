Clifton.investment.account.assetclass.position.PositionAllocationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Position Allocation',
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		labelWidth: 100,
		url: 'investmentAccountAssetClassPositionAllocation.json',
		instructions: 'Asset Class Position Allocations allow explicitly defining contract splitting when processing portfolio runs.  Position allocations can be defined for up to one less than the total asset classes the security is split between.  The remaining asset class always gets the rest of the allocation, or if multiple the system will calculate the split.  Order allows defining which asset classes are filled first and are useful for more than 2 asset class splits with long and short positions where virtual contracts are used to fulfill the specified quantity.  Replication selection is necessary only if a contract is split between replications in the same asset class.',
		items: [
			{fieldLabel: 'Client Account', name: 'accountAssetClass.account.label', hiddenName: 'accountAssetClass.account.id', detailPageClass: 'Clifton.investment.account.AccountWindow', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', submitValue: false, displayField: 'label'},
			{
				fieldLabel: 'Asset Class', name: 'accountAssetClass.label', hiddenName: 'accountAssetClass.id', xtype: 'combo', detailPageClass: 'Clifton.investment.account.assetclass.AssetClassWindowSelector', url: 'investmentAccountAssetClassListFind.json', displayField: 'label',
				requiredFields: ['accountAssetClass.account.label'],
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					queryEvent.combo.store.baseParams = {
						accountId: f.findField('accountAssetClass.account.id').value,
						usesPrimaryReplication: true
					};
				}
			},
			{
				fieldLabel: 'Replication', name: 'replication.label', hiddenName: 'replication.id', detailPageClass: 'Clifton.investment.replication.ReplicationWindow', xtype: 'combo', url: 'investmentReplicationListForAccountAssetClass.json', displayField: 'label',
				tooltip: 'Select a replication only if the security is split within the same asset class (i.e. Used by both Primary and Secondary replication)',
				requiredFields: ['accountAssetClass.id'],
				loadAll: true,
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					queryEvent.combo.store.baseParams = {
						accountAssetClassId: f.findField('accountAssetClass.id').value
					};
				}
			},

			{fieldLabel: 'Security', name: 'security.label', hiddenName: 'security.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=false', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', displayField: 'label'},

			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
			{fieldLabel: 'Order', name: 'allocationOrder', xtype: 'spinnerfield', minValue: 1, maxValue: 1000},
			{fieldLabel: 'Quantity', name: 'allocationQuantity', xtype: 'integerfield'}
		]
	}]
});
