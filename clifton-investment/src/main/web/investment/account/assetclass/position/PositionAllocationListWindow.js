Clifton.investment.account.assetclass.position.PositionAllocationListWindow = Ext.extend(TCG.app.Window, {
	id: 'investmentAccountAssetClassPositionAllocationListWindow',
	title: 'Investment Account Asset Class Position Allocations',
	iconCls: 'chart-pie',
	width: 1400,
	height: 700,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.investment.account.assetclass.position.PositionAllocationListWindowAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			const o = arr[i];
			tabs.add((typeof o == 'function') ? o(w) : o);
		}
	},

	items: [{
		xtype: 'tabpanel',
		items: [{
			title: 'Position Allocation',
			items: [{
				xtype: 'investment-accountAssetClass-PositionAllocation'
			}]
		}]
	}]
});
