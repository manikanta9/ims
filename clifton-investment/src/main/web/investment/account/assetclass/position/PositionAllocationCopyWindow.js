Clifton.investment.account.assetclass.position.PositionAllocationCopyWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Copy Position Allocation',
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		labelWidth: 100,
		url: 'investmentAccountAssetClassPositionAllocation.json',
		instructions: 'Please enter a new start and/or end date and new quantity.  You can also select a new security for the same instrument if you are rolling quantity to a new contract.  Asset Class/Security will be copied to the new allocation.  When copying the existing allocation, if it has no end date, or ends after this allocation\'s start date it will automatically be updated to end on the previous day.',
		items: [
			{fieldLabel: 'Copy Allocation', name: 'label', xtype: 'displayfield', submitValue: false},
			{fieldLabel: 'Start Date', name: 'newStartDate', xtype: 'datefield'},
			{fieldLabel: 'End Date', name: 'newEndDate', xtype: 'datefield'},
			{fieldLabel: 'Instrument', name: 'security.instrument.label', detailIdField: 'security.instrument.id', xtype: 'linkfield', submitDetailField: false, detailPageClass: 'Clifton.investment.instrument.InstrumentWindow'},
			{
				fieldLabel: 'Security', name: 'security.label', hiddenName: 'security.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=false', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', displayField: 'label',
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					queryEvent.combo.store.baseParams = {
						instrumentId: f.findField('security.instrument.id').value
					};
				}
			},
			{fieldLabel: 'Quantity', name: 'newQuantity', xtype: 'integerfield', allowBlank: false}
		],
		getSaveURL: function() {
			return 'investmentAccountAssetClassPositionAllocationCopy.json';
		}
	}]
});
