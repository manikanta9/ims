Clifton.investment.account.assetclass.RollupAssetClassWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Account Rollup Asset Class',
	iconCls: 'hierarchy',
	width: 800,
	height: 600,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					loadValidation: false,
					url: 'investmentAccountAssetClass.json',
					labelFieldName: 'account.label',
					labelWidth: 120,
					submitEmptyText: true,

					getWarningMessage: function(form) {
						let msg = undefined;
						if (TCG.isTrue(form.formValues.inactive)) {
							msg = 'This account asset class is no longer active for this account.';
						}
						return msg;
					},

					items: [
						{name: 'rollupAssetClass', xtype: 'hidden', value: 'true'},
						{name: 'targetExposureAdjustmentType', xtype: 'hidden', value: 'NONE'},
						{fieldLabel: 'Client Account', name: 'account.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'account.id'},

						{fieldLabel: 'Asset Class', name: 'assetClass.name', hiddenName: 'assetClass.id', allowBlank: false, xtype: 'combo', url: 'investmentAssetClassListFind.json', detailPageClass: 'Clifton.investment.setup.AssetClassWindow'},
						{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield', minValue: 0, maxValue: 1000, qtip: 'Sort order used to display asset classes. For nexted asset classes, use hundreds for parent asset classes and corresponding tens for child asset classes.'},
						{fieldLabel: 'Target Exposure %', name: 'assetClassPercentage', xtype: 'currencyfield', disabled: true, emptyText: '0.0', decimalPrecision: 4},
						{fieldLabel: 'Fund Cash %', name: 'cashPercentage', xtype: 'currencyfield', disabled: true, emptyText: '0.0', decimalPrecision: 10, qtip: 'Target the following exposure for the Fund Cash bucket. Usually the same as Target Exposure but may change based on how alternative asset class(es) should be allocated.'},

						{
							xtype: 'fieldset-checkbox',
							title: 'Rebalancing Configuration',
							checkboxName: 'rebalancingUsed',
							instructions: 'All rebalancing targets and absolutes should be entered as positive values.  All absolute values must be greater than or equal to their target counterparts.',
							items: [
								{fieldLabel: 'Exposure Target (%)', xtype: 'currencyfield', name: 'rebalanceExposureTarget', minValue: 0, maxValue: 100, disabled: true},
								{
									fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
									defaults: {
										xtype: 'displayfield',
										flex: 1
									},
									items: [
										{value: 'Minimum %'},
										{value: 'Maximum %'}
									]
								},
								{
									fieldLabel: 'Trigger Range', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										flex: 1
									},
									items: [
										{name: 'rebalanceAllocationMin', minValue: 0},
										{name: 'rebalanceAllocationMax', minValue: 0}
									]
								},
								{
									fieldLabel: 'Absolute Trigger', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										flex: 1
									},
									items: [
										{name: 'rebalanceAllocationAbsoluteMin', minValue: 0},
										{name: 'rebalanceAllocationAbsoluteMax', minValue: 0}
									]
								},
								{fieldLabel: 'Rebalance Benchmark', name: 'rebalanceBenchmarkSecurity.label', hiddenName: 'rebalanceBenchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow', mutuallyExclusiveFields: ['skipRebalanceCashAdjustment']},
								{boxLabel: 'Do not adjust rebalance cash based on benchmark performance', name: 'skipRebalanceCashAdjustment', xtype: 'checkbox', mutuallyExclusiveFields: ['rebalanceBenchmarkSecurity.id']},
								{
									fieldLabel: 'Trigger Action', name: 'rebalanceTriggerActionLabel', hiddenName: 'rebalanceTriggerAction', displayField: 'name', valueField: 'triggerAction', mode: 'local', xtype: 'combo',
									store: {
										xtype: 'arraystore',
										fields: ['name', 'triggerAction', 'description'],
										data: Clifton.investment.account.assetclass.RebalanceActions
									}
								},
								{
									boxLabel: 'Apply Values as Absolute Bands (if left unchecked rebalance triggers will be calculated Proportionally)', name: 'rebalanceAllocationFixed', xtype: 'checkbox',
									qtip: 'Absolute Bands means the percentages entered are used as is.  Proportional bands are calculated as the given percent of the adjusted target.<br><br>Example: A percentage of 1% used as absolute bands is 1% of the Total Portfolio Value.  A percentage of 1% calculated proportionally for an asset class with a 25% target would result in a band that is 0.25% of the total portfolio value.'
								}
							]
						},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'InvestmentAccountAssetClass'
						}
					]
				}]
			},


			{

				title: 'Child Asset Classes',
				items: [{
					name: 'investmentAccountAssetClassListFind',
					xtype: 'gridpanel',
					instructions: 'The following account asset classes are children of this account asset class.',
					getLoadParams: function() {
						return {parentId: this.getWindow().getMainForm().formValues.id};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Asset Class', width: 120, dataIndex: 'label'},
						{header: 'Order', width: 50, dataIndex: 'order', type: 'int', hidden: true},
						{header: 'Benchmark', width: 150, dataIndex: 'benchmarkSecurity.label'},
						{header: 'Primary Replication', width: 120, dataIndex: 'primaryReplication.name'},
						{header: 'Secondary Replication', width: 120, dataIndex: 'secondaryReplication.name', hidden: true},
						{header: 'Sec Replication Amount', dataIndex: 'secondaryReplicationAmount', width: 100, type: 'currency', hidden: true},
						{header: 'Target', dataIndex: 'targetExposureAdjustmentType', width: 75},
						{header: 'Target Exposure', dataIndex: 'assetClassPercentage', width: 90, type: 'percent', summaryType: 'sum', numberFormat: '0,000.0000'},
						{header: 'Fund Cash', dataIndex: 'cashPercentage', width: 90, type: 'percent', summaryType: 'sum', numberFormat: '0,000.0000000000'}
					],
					plugins: {ptype: 'gridsummary'},

					editor: {
						detailPageClass: 'Clifton.investment.account.assetclass.AssetClassWindow',
						deleteURL: 'investmentAccountAssetClassRollupDelete.json',
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							const mainForm = gridPanel.getWindow().getMainForm();
							const account = TCG.getValue('account.id', mainForm.formValues);
							toolBar.add(new TCG.form.ComboBox({
								name: 'child', url: 'investmentAccountAssetClassListFind.json', displayField: 'label', width: 150, listWidth: 230,
								beforequery: function(queryEvent) {
									queryEvent.combo.store.baseParams = {accountId: account, emptyParentOnly: true, excludeFromExposure: false};
								}
							}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add selected child account asset class',
								iconCls: 'add',
								handler: function() {
									const childId = TCG.getChildByName(toolBar, 'child').getValue();
									if (TCG.isBlank(childId)) {
										TCG.showError('You must first select and account asset class from the list.');
									}
									else {
										const parentId = gridPanel.getWindow().getMainFormId();
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Linking...',
											params: {parentId: parentId, childId: childId},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'child').reset();
											}
										});
										loader.load('investmentAccountAssetClassRollupLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			}
		]
	}]
});
