Clifton.investment.account.assetclass.rebalance.HistoryWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Rebalance History Record',
	iconCls: 'grouping',
	height: 550,

	items: [{
		xtype: 'formpanel',
		readOnly: true,
		instructions: 'A rebalance history record contains a snapshot of rebalance cash when a rebalance was performed.  Either automatically by the system, or manually, and the rebalance cash before/after rebalancing was done.',
		url: 'investmentAccountRebalanceHistory.json',
		wikiPage: 'IT/Investment Rebalancing',
		items: [
			{fieldLabel: 'Account', name: 'investmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'investmentAccount.id'},
			{fieldLabel: 'Calculation Type', name: 'rebalanceCalculationTypeLabel', xtype: 'displayfield'},
			{fieldLabel: 'Rebalance Date', name: 'rebalanceDate', xtype: 'datefield'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},

			{
				xtype: 'formgrid',
				storeRoot: 'assetClassHistoryList',
				dtoClass: 'com.clifton.investment.account.assetclass.InvestmentAccountRebalanceAssetClassHistory',
				readOnly: true,
				columnsConfig: [
					{header: 'Asset Class', width: 175, dataIndex: 'accountAssetClass.label'},
					{header: 'Before Value', qtip: 'Rebalance Cash Adjusted value prior to rebalancing', width: 100, dataIndex: 'beforeRebalanceCash', type: 'currency', summaryType: 'sum'},
					{header: 'After Value', qtip: 'Rebalance Cash value after rebalancing', width: 100, dataIndex: 'afterRebalanceCash', type: 'currency', summaryType: 'sum'},
					{header: 'Adjustment', width: 100, dataIndex: 'adjustmentValue', type: 'currency', summaryType: 'sum'}
				],
				plugins: {ptype: 'gridsummary'}
			}
		]
	}]
});
