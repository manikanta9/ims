Clifton.investment.account.assetclass.AssetClassUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'investmentAccountAssetClassUploadWindow',
	title: 'Investment Account Asset Class Import',
	iconCls: 'import',
	height: 600,
	hideOKButton: true,

	tbar: [
		{
			text: 'Sample File (Simple)',
			iconCls: 'excel',
			tooltip: 'Download Simple Excel Sample File.  If an account is selected the data in the sample will be limited to the selected account. Simple version limits a lot of the additional foreign key field references that are not usually necessary.  For example, if you need to specify a security and the symbol is not unique enough, you need to use the expanded version.',
			handler: function() {
				const f = this.ownerCt.ownerCt.items.get(0);
				f.getWindow().getMainFormPanel().downloadSampleFile(false);
			}
		},
		{
			text: 'Sample File (Expanded)',
			iconCls: 'excel',
			tooltip: 'Download Expanded Excel Sample File.  Includes additional columns that may be necessary for some cases. For example, if you need to specify a security and the symbol is not unique enough, you need to use the expanded version.',
			handler: function() {
				const f = this.ownerCt.ownerCt.items.get(0);
				f.getWindow().getMainFormPanel().downloadSampleFile(true);
			}
		}
	],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Investment Account Asset Class Uploads allow you to update existing account asset class information: <br/>' +
			'1. The account asset class must exist and be currently active (i.e. you can only update selected properties).<br/>' +
			'2. The account asset class cannot be a rollup asset class.<br/>' +
			'<b>Note:</b>After downloading the sample file you should remove any columns from the file that you do not want to update.  Any columns not included will not affect existing values.',

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},

			{
				fieldLabel: 'Client Account', name: 'investmentAccount.label', hiddenName: 'investmentAccount.id', xtype: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true',
				qtip: 'If selected, then the account does not have to be specified in the file.  If it is specified in the file, then the file value will be used.  Also, if selected prior to downloading a sample file, the sample file will be restricted to asset classes for the selected account.'
			},
			{
				xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
				items: [
					{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
				]
			}
		],

		getSaveURL: function() {
			return 'investmentAccountAssetClassUploadFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true';
		},

		downloadSampleFile: function(showAllColumns) {
			const accountId = this.getForm().findField('investmentAccount.id').value;
			// FileName - usually uses tableName but we include account label in the title
			let fileName = 'InvestmentAccountAssetClass-';
			if (!TCG.isBlank(accountId)) {
				fileName = fileName + this.getForm().findField('investmentAccount.id').getRawValue();
			}
			const params = this.getFormValuesFormatted(true);
			params.enableValidatingBinding = true;
			params.showAllColumns = showAllColumns;
			params.fileName = fileName;
			params.outputFormat = 'xls';
			TCG.downloadFile('investmentAccountAssetClassUploadFileSampleDownload.json', params, this);
		}
	}]
});
