Clifton.investment.account.assetclass.BenchmarkWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Performance Benchmark',
	iconCls: 'grouping',
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'A performance benchmark can apply to a specific time period.  Start/End dates cannot overlap.  If multiple performance benchmarks are applied for a given month, the system will calculate benchmark returns based on each date range.  If nothing is defined in this table for the given period, the system will use the benchmark defined for the asset class.',
		url: 'investmentAccountAssetClassBenchmark.json',
		items: [
			{name: 'referenceOne.id', xtype: 'hidden'},
			{fieldLabel: 'Account', name: 'referenceOne.account.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'referenceOne.account.id'},
			{fieldLabel: 'Asset Class', name: 'referenceOne.label', detailIdField: 'referenceOne.assetClass.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.AssetClassWindow'},
			{xtype: 'label', html: '<hr />'},
			{fieldLabel: 'Benchmark', name: 'referenceTwo.label', hiddenName: 'referenceTwo.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow', mutuallyExclusiveFields: ['useAccountAssetClassReturn']},
			{boxLabel: 'No Benchmark Security. Use Client Account Asset Class Return for Benchmark Return', xtype: 'checkbox', name: 'useAccountAssetClassReturn', mutuallyExclusiveFields: ['referenceTwo.id']},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
		]
	}]
});
