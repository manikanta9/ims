// not the actual window but a window selector:
//   - rollup asset classes get RollupAssetClassWindow
//   - the rest get AssetClassWindow

Clifton.investment.account.assetclass.AssetClassWindowSelector = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'investmentAccountAssetClass.json',

	getClassName: function(config, entity) {
		if (entity && entity.rollupAssetClass) {
			return 'Clifton.investment.account.assetclass.RollupAssetClassWindow';
		}
		return 'Clifton.investment.account.assetclass.AssetClassWindow';
	}
});
