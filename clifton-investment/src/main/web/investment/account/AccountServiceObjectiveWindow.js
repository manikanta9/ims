Clifton.investment.account.AccountServiceObjectiveWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Account Service Objective',
	iconCls: 'services',
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'An account service objects associates an investment account with a business service objective.  Description can be overridden to specify an account specific objective.',
		url: 'investmentAccountServiceObjective.json',
		items: [
			{fieldLabel: 'Client Account', name: 'referenceOne.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'referenceOne.id'},
			{fieldLabel: 'Business Service', name: 'referenceOne.businessService.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.service.ServiceWindow', detailIdField: 'referenceOne.businessService.id', submitDetailIdField: false},
			{
				fieldLabel: 'Objective', name: 'referenceTwo.name', hiddenName: 'referenceTwo.id', xtype: 'combo', url: 'businessServiceObjectiveListFind.json', displayField: 'label',
				detailPageClass: 'Clifton.business.service.ObjectiveWindow',
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					queryEvent.combo.store.baseParams = {
						businessServiceId: TCG.getValue('referenceOne.businessService.id', f.formValues)
					};
				},
				getDefaultData: function(f) { // defaults service for "add new" drill down
					return {
						businessService: TCG.getValue('referenceOne.businessService', f.formValues)
					};
				},
				listeners: {
					select: function(combo, record, index) {
						// make objective description visible on screen so users can decide if they want to override it
						const fp = combo.getParentForm();
						const form = fp.getForm();
						const descField = form.findField('referenceTwo.description');
						const val = record.json.description;
						descField.setValue(val);
					}
				}
			},
			{xtype: 'label', html: '<hr />'},
			{fieldLabel: 'Description', name: 'referenceTwo.description', xtype: 'displayfield', submitValue: false},
			{xtype: 'label', html: 'To override the above description, please enter the account specific description for this business service type objective below:'},
			{name: 'description', xtype: 'textarea'}
		]
	}]
});
