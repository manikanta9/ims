Clifton.investment.account.targetexposure.TargetExposureAdjustmentFieldSet = Ext.extend(TCG.form.FormPanelFragment, {

		frame: false,
		name: 'targetExposureFormFragment',
		bodyStyle: 'padding: 0px 0px 0px 0px',

		// Field Set Info
		fieldSetTitle: 'Target Exposure Adjustment',
		fieldSetCheckboxName: 'targetExposureAdjustmentUsed',
		fieldSetName: 'targetExposureUsedFieldset',
		fieldSetFieldPrefix: 'targetExposureAdjustment',
		fieldSetInstructions: 'Use the following selection to change target/actual exposure for this asset class and redistribute the difference between target and actual exposures across the following asset classes either proportionally or using specified percentage allocation that adds up to 100%.  Proportional allocation uses the asset classes current targets to determine its share of the adjustment.  For asset classes that do not have a target, then a percentage must be specified.',
		cashAdjustment: false,

		allowedOptions: ['NONE', 'TFA', 'AFT', 'CONSTANT', 'MANAGER_MARKET_VALUE', 'PERCENT_OF_TARGET', 'ABSOLUTE_PERCENT', 'DYNAMIC_TARGET_PERCENT'],
		assignmentGrid: {
			xtype: 'formgrid',
			storeRoot: 'targetAdjustmentAssignmentList',
			dtoClass: 'com.clifton.investment.account.assetclass.InvestmentAccountAssetClassAssignment',
			columnsConfig: [
				{
					header: 'Asset Class Assignment', width: 280, dataIndex: 'referenceTwo.label', idDataIndex: 'referenceTwo.id',
					editor: {
						xtype: 'combo', url: 'investmentAccountAssetClassListFind.json', displayField: 'label',
						beforequery: function(queryEvent) {
							const grid = queryEvent.combo.gridEditor.containerGrid;
							const f = TCG.getParentFormPanel(grid).getForm();
							queryEvent.combo.store.baseParams = {targetExposureAdjustmentUsed: 'false', accountId: TCG.getValue('account.id', f.formValues), rollupAssetClass: false, excludeFromExposure: false};
						}
					}
				},
				{header: 'Proportional', width: 90, dataIndex: 'proportionalAllocation', type: 'boolean', editor: {xtype: 'checkbox'}},
				{header: 'Cash', width: 90, dataIndex: 'cashAllocation', type: 'boolean', hidden: true},
				{
					header: 'Percentage', width: 90, dataIndex: 'allocationPercent', type: 'percent', useNull: true, numberFormat: '0,000.0000',
					editor: {
						xtype: 'currencyfield', decimalPrecision: 4
					}
				}
			]
		},

		labelWidth: 10,
		listeners: {
			afterRender: function() {
				const ff = this;
				const fp = TCG.getParentFormPanel(ff);
				fp.on('afterload', function() {
					ff.setResetTargetExposureFields();
				});
			}
		},

		initComponent: function() {
			const currentItems = [];
			const fs = this;

			const fsItems = [];
			const addPrefixToItemName = function(item) {
				if (item.name && item.name.startsWith('.')) {
					item.name = fs.fieldSetFieldPrefix + item.name;
				}
				if (item.hiddenName && item.hiddenName.startsWith('.')) {
					item.hiddenName = fs.fieldSetFieldPrefix + item.hiddenName;
				}
			};
			Ext.each(fs.targetItems, function(ti) {
				const f = TCG.clone(ti);
				if (f.name === 'targetExposureAdjustmentType-Group') {
					Ext.each(f.items, function(g) {
						addPrefixToItemName(g);
						if (g.name === fs.fieldSetFieldPrefix + '.targetExposureAdjustmentType' && g.inputValue && fs.allowedOptions.indexOf(g.inputValue) < 0) {
							g.hidden = true;
						}
						if (g.xtype === 'formfragment') {
							Ext.each(g.items, function(ffi) {
								addPrefixToItemName(ffi);
							});
						}
					});
				}
				else if (f.name.startsWith('.')) {
					addPrefixToItemName(f);
				}
				fsItems.push(f);
			});
			Ext.each(fs.additionalItems, function(f) {
				fsItems.push(f);
			});
			const grid = TCG.clone(this.assignmentGrid);
			if (TCG.isNotBlank(fs.fieldSetCashAssignmentGridStoreRoot)) {
				grid.storeRoot = fs.fieldSetCashAssignmentGridStoreRoot;
			}
			grid['getNewRowDefaults'] = function() {
				return {cashAllocation: TCG.isTrue(fs.cashAdjustment)};
			};
			fsItems.push(grid);

			currentItems.push(new TCG.form.FieldSetCheckbox({
				title: this.fieldSetTitle,
				checkboxName: this.fieldSetCheckboxName,
				items: fsItems,
				name: this.fieldSetName,
				instructions: this.fieldSetInstructions
			}));
			this.items = currentItems;
			Clifton.investment.account.targetexposure.TargetExposureAdjustmentFieldSet.superclass.initComponent.call(this);
		},

		setResetTargetExposureFields: function() {
			// If an max value or percentage is entered, enable apply cash over limit to securities checkbox
			const fs = this;
			const f = TCG.getParentFormPanel(fs).getForm();

			const te = f.findField(fs.fieldSetFieldPrefix + '.targetExposureAdjustmentType');

			let showAmountField = false;
			let amountFieldLabel = 'Amount';
			let showManagerField = false;
			let showSystemBeanField = false;

			if (te.getGroupValue() === 'CONSTANT') {
				showAmountField = true;
				amountFieldLabel = 'Target Exposure Amount';
			}
			else if (te.getGroupValue() === 'PERCENT_OF_TARGET' || te.getGroupValue() === 'ABSOLUTE_PERCENT') {
				showAmountField = true;
				amountFieldLabel = 'Percentage';
			}
			else if (te.getGroupValue() === 'MANAGER_MARKET_VALUE') {
				showManagerField = true;
			}
			else if (te.getGroupValue() === 'DYNAMIC_TARGET_PERCENT') {
				showSystemBeanField = true;
			}

			const tea = f.findField(fs.fieldSetFieldPrefix + '.targetExposureAmount');
			if (showAmountField === true) {
				tea.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(amountFieldLabel + ':');
				tea.fieldLabel = amountFieldLabel;
				tea.setDisabled(false);
				tea.setVisible(true);
			}
			else {
				tea.setValue('');
				tea.setDisabled(true);
				tea.setVisible(false);
			}

			const tem = f.findField(fs.fieldSetFieldPrefix + '.targetExposureManagerAccount.label');
			if (showManagerField === true) {
				tem.setDisabled(false);
				tem.setVisible(true);
			}
			else {
				tem.reset();
				tem.clearValue();

				tem.setDisabled(true);
				tem.setVisible(false);
			}

			const teb = f.findField(fs.fieldSetFieldPrefix + '.dynamicTargetCalculatorBean.name');
			if (showSystemBeanField === true) {
				teb.setDisabled(false);
				teb.setVisible(true);
			}
			else {
				teb.reset();
				teb.clearValue();

				teb.setDisabled(true);
				teb.setVisible(false);
			}
		},

		additionalItems: [], // Override to add additional items after targetItems
		targetItems: [
			{
				xtype: 'hidden', name: '.cashAdjustment',
				listeners: {
					afterrender: function(field) {
						const parentFormFragment = TCG.getParentByClass(field, Clifton.investment.account.targetexposure.TargetExposureAdjustmentFieldSet);
						field.setValue(TCG.isTrue(parentFormFragment.cashAdjustment));
						field.originalValue = TCG.isTrue(parentFormFragment.cashAdjustment); // set original value to prevent form modified
					}
				}
			},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'targetExposureAdjustmentType-Group',
				anchor: '0',
				items: [
					/**
					 * Item names will be prefixed with the fieldset's fieldSetFieldPrefix on initialization
					 */
					{boxLabel: 'None (Do Not Adjust Targets)', name: '.targetExposureAdjustmentType', inputValue: 'NONE', checked: true},
					{boxLabel: 'Do not change actual exposure: Target Follows Actual (TFA)', name: '.targetExposureAdjustmentType', inputValue: 'TFA'},
					{boxLabel: 'Change actual exposure to the target: Actual Follows Target (AFT)', name: '.targetExposureAdjustmentType', inputValue: 'AFT'},
					{boxLabel: 'Set target exposure to the following constant amount:', name: '.targetExposureAdjustmentType', inputValue: 'CONSTANT'},
					{
						boxLabel: 'Set target exposure to the following manager account balance:', name: '.targetExposureAdjustmentType', inputValue: 'MANAGER_MARKET_VALUE',
						qtip: 'Allows dynamically setting target equal to the Total Market Value (Cash & Securities) from a Manager Balance.'
					},
					{
						boxLabel: 'Increase target exposure by the following percentage of the target:', tooltip: 'Enter 5% as 5. Use negative values to decrease target. Adjusted target will be x% of asset class target', name: '.targetExposureAdjustmentType', inputValue: 'PERCENT_OF_TARGET',
						qtip: 'This one is proportional and adjusts the target by x% of the actual target amount. So, if your target is 10% and 500,000 and you enter adjustment of 5% the target will be adjusted by 25000, or 0.5%. = New target of 525,000 or 10.5%. Enter values as whole numbers (i.e. 5% = 5), using negative values to decrease the target.'
					},
					{
						boxLabel: 'Increase target exposure by the absolute percentage:', name: '.targetExposureAdjustmentType', inputValue: 'ABSOLUTE_PERCENT',
						qtip: 'Absolute Percent would not take a percentage of the actual target, but adjust the target percentage by the given amount. So, if your target is 10% and 500,000 and you enter adjustment of 5% the target will be adjusted by 250,000, or 5%. = New target of 750,000 or 15%.  Enter values as whole numbers (i.e. 5% = 5), using negative values to decrease the target.'
					},
					{
						boxLabel: 'Adjust the Target Exposure % by the Following Calculated Value:', name: '.targetExposureAdjustmentType', inputValue: 'DYNAMIC_TARGET_PERCENT',
						qtip: 'Allows dynamically setting, or adjusting, target by the result of a Dynamic Target Calculator.'
					},
					{
						xtype: 'formfragment',
						frame: false,
						bodyStyle: 'padding-left: 30px',
						labelWidth: 200,
						items: [
							{fieldLabel: 'Target Exposure Amount', name: '.targetExposureAmount', xtype: 'currencyfield'},
							{
								fieldLabel: 'Manager Account', name: '.targetExposureManagerAccount.label', hiddenName: '.targetExposureManagerAccount.id', xtype: 'combo', url: 'investmentManagerAccountListFind.json', displayField: 'label',
								detailPageClass: 'Clifton.investment.manager.AccountWindow',
								beforequery: function(queryEvent) {
									const f = queryEvent.combo.getParentForm().getForm();
									queryEvent.combo.store.baseParams = {
										clientId: TCG.getValue('account.businessClient.id', f.formValues)
									};
								}
							},
							{
								fieldLabel: 'Target Calculator', name: '.dynamicTargetCalculatorBean.name', hiddenName: '.dynamicTargetCalculatorBean.id', xtype: 'combo', url: 'systemBeanListFind.json', displayField: 'label',
								detailPageClass: 'Clifton.system.bean.BeanWindow',
								beforequery: function(queryEvent) {
									queryEvent.combo.store.baseParams = {
										groupName: 'Investment Account Target Exposure Calculator'
									};
								}
							}
						]
					}

				],
				listeners: {
					change: function(f) {
						const p = TCG.getParentByClass(f, TCG.form.FormPanelFragment);
						p.setResetTargetExposureFields();
					}
				}
			}
		]
	}
);
Ext.reg('investment-account-targetexposure-adjustment-fieldset', Clifton.investment.account.targetexposure.TargetExposureAdjustmentFieldSet);
