Clifton.investment.account.CliftonAccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Client Account',
	iconCls: 'account',
	width: 1200,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		tbar: {xtype: 'container', layout: 'hbox', autoEl: 'div', cls: 'warning-msg', items: [{xtype: 'label', html: 'SET WARNING MESSAGE HERE'}], hidden: true},
		items: [
			{
				title: 'Info',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'InvestmentAccount',
					finalState: true,
					finalWorkflowMessage: 'Use <i>Workflow State</i> and <i>Effective Start</i> fields below for transitions.',
					addAdditionalButtons: function(toolbar) {
						const formPanel = this.getFormPanel();
						const data = formPanel.getForm().formValues;
						toolbar.add('-');
						toolbar.add({
							text: 'Reports',
							xtype: 'button',
							iconCls: 'report',
							tooltip: 'Export Selected Report for this Client Account. Available reports are only those that accept Client Account and Report Date as parameters. (Template tag = Client Account Report)',
							scope: formPanel,
							handler: function(btn) {
								const clientAccountId = data.id;
								const clz = 'Clifton.report.exports.ReportExportWindow';
								const cmpId = TCG.getComponentId(clz + ':ClientAccountReport', clientAccountId);
								TCG.createComponent(clz, {
									id: cmpId,
									exportType: 'ClientAccountReport',
									defaultData: {
										'clientAccountId': clientAccountId,
										'clientAccountLabel': data.label
									},
									openerCt: this
								});
							}
						});
						toolbar.add('-');
					}
				},
				items: [{
					xtype: 'formpanel',
					labelFieldName: 'label',
					labelWidth: 120,
					url: 'investmentAccount.json',
					childTables: 'InvestmentAccountBusinessService', // used by audit trail

					// Insert Service Type Specific Portfolio Tabs and Other Additional Tabs
					listeners: {
						afterload: function(fp) {
							const w = fp.getWindow();
							const tabs = w.items.get(0);
							const additionalTabs = {};
							Clifton.investment.account.ClientAccountWindowAdditionalTabs.forEach(t => additionalTabs[t.title] = t);

							// INSERT ACCOUNT PORTFOLIO SPECIFIC TABS
							const accountProcessingType = fp.getFormValue('serviceProcessingType.processingType');
							let processingType = w.initialConfig.processingType || void 0;
							let processingTypeWarningMsg = void 0;
							if (TCG.isBlank(processingType)) {
								processingType = accountProcessingType;
							}
							else if (TCG.isNotEquals(processingType, accountProcessingType)) {
								processingTypeWarningMsg = 'This is a historic view of the client account based on navigation from a previous window (e.g. Portfolio Run window), so it may not not reflect current values.';
							}
							if (TCG.isNotBlank(processingTypeWarningMsg)) {
								const tbar = TCG.getParentByClass(fp, TCG.TabPanel).getTopToolbar();
								tbar.items.get(0).setText(processingTypeWarningMsg);
								tbar.show();
							}
							if (TCG.isNotBlank(processingType) && fp.processingTabsAdded !== true && Clifton.investment.account.CliftonAccountPortfolioTabs[processingType]) {
								// INSERT THEM IN (ALWAYS START AT POSITION # 3)
								fp.processingTabsAdded = true;
								for (let i = 0; i < Clifton.investment.account.CliftonAccountPortfolioTabs[processingType].length; i++) {
									const tab = Clifton.investment.account.CliftonAccountPortfolioTabs[processingType][i];
									if (TCG.isNotNull(additionalTabs[tab.title])) {
										additionalTabs[tab.title] = tab; // override additional tab to preserve location location
									}
									else {
										tabs.insert(i + 3, tab);
									}
								}
							}

							// INSERT ADDITIONAL TABS
							Clifton.investment.account.ClientAccountWindowAdditionalTabs.forEach(t => tabs.add(additionalTabs[t.title]));
						}
					},

					items: [
						{name: 'type.id', xtype: 'hidden'},
						{fieldLabel: 'Client', name: 'businessClient.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailIdField: 'businessClient.id'},

						{
							xtype: 'columnpanel',
							columns: [{
								rows: [
									{fieldLabel: 'Account Number', name: 'number', xtype: 'textfield'},
									{fieldLabel: 'Base Currency', name: 'baseCurrency.label', hiddenName: 'baseCurrency.id', displayField: 'label', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true'},
									{
										fieldLabel: 'FX Source', name: 'defaultExchangeRateSourceCompany.name', hiddenName: 'defaultExchangeRateSourceCompany.id', xtype: 'combo', url: 'marketDataCompanyForExchangeRatesList.json', loadAll: true, detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true,
										qtip: 'The default company (uses parent name if applies) to use for retrieving FX rates. Applies to both Client and Holding Accounts and can be used to override default behaviour.<br/>Whenever possible, holding account FX Source is used: COALESCE(Holding Account FX Source, Client Account FX Source, Holding Account Issuer, Default: Goldman Sachs)<br/>When holding account is not available (manager balances, billing, etc.), client account FX Source is used: COALESCE(Client Account FX Source, Default: Goldman Sachs).'
									},
									{
										fieldLabel: 'Closing Method', name: 'accountingClosingMethodOverride', hiddenName: 'accountingClosingMethodOverride', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
										store: {xtype: 'arraystore', fields: ['name', 'value', 'description'], data: Clifton.business.service.AccountingClosingMethods},
										qtip: 'Optionally overrides Accounting Closing Method (FIFO, LIFO, etc.) specified for account\'s Business Service.'
									},
									{
										fieldLabel: 'Workflow State', xtype: 'workflow-state-combo', tableName: 'InvestmentAccount',
										listeners: {
											afterrender: function(combo) {
												// store previous selected for later use
												combo.previouslySelectedValue = {value: combo.getValue(), text: combo.lastSelectionText};
											},
											// Reset Effective Start Date on State Change
											select: async function(combo, record, index) {
												if (record.json.name === 'Terminated') {
													const result = await TCG.showConfirm('Do you want to terminate this account ?', 'Confirm Account Termination');
													if (result === 'no') {
														combo.setValue(combo.previouslySelectedValue);
														return;
													}
												}

												const form = combo.getParentForm().getForm();
												const efDateField = form.findField('workflowStateEffectiveStartDate');
												efDateField.setValue('');
												combo.previouslySelectedValue = {value: combo.getValue(), text: combo.lastSelectionText};
											}
										}
									},
									{fieldLabel: 'Positions On Date', xtype: 'datefield', name: 'inceptionDate', qtip: 'Date when positions were first put on for this account. NOTE: the Data Warehouse will not rebuild prior to this date or if it is not populated.'}
								]
							}, {
								rows: [
									{fieldLabel: 'Account Name', name: 'name', xtype: 'textfield'},
									{fieldLabel: 'Short Name', name: 'shortName', xtype: 'textfield', qtip: 'Optional short name for accounts with long legal names. When populated, will be used within UI for account labels.'},
									{fieldLabel: 'Issued By', name: 'issuingCompany.name', hiddenName: 'issuingCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourCompany=true', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true},
									{fieldLabel: 'Servicing Team', name: 'teamSecurityGroup.name', hiddenName: 'teamSecurityGroup.id', xtype: 'combo', url: 'securityGroupTeamList.json', loadAll: true, detailPageClass: 'Clifton.security.user.GroupWindow'},
									{fieldLabel: 'Workflow Effective Start', xtype: 'datefield', name: 'workflowStateEffectiveStartDate', qtip: 'Date indicating when the Workflow State was changed. Used most commonly to determine if the account is active for a given date.'},
									{fieldLabel: 'Performance Start Date', xtype: 'datefield', name: 'performanceInceptionDate', qtip: 'Date when we started recording performance for this account.  Most cases this is the same as the Positions On Date.'}
								],
								config: {labelWidth: 155}
							}]
						},
						{xtype: 'label', html: '<hr />'},
						{
							fieldLabel: 'IMA', name: 'businessContract.label', hiddenName: 'businessContract.id', xtype: 'combo', url: 'businessContractListFind.json', displayField: 'label', detailPageClass: 'Clifton.business.contract.ContractWindow', disableAddNewItem: true, qtip: 'Investment Management Agreement that defines investment policies for this account',
							beforequery: function(queryEvent) {
								const companyId = TCG.getValue('businessClient.company.id', queryEvent.combo.getParentForm().getForm().formValues);
								const s = queryEvent.combo.store;
								s.setBaseParam('companyIdOrRelatedCompany', companyId);
								s.setBaseParam('beginsWithContractTypeName', 'IMA');
							}
						},
						{
							xtype: 'columnpanel',
							columns: [{
								rows: [
									{
										fieldLabel: 'Service', name: 'businessService.name', hiddenName: 'businessService.id', xtype: 'combo', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?serviceLevelType=SERVICE', detailPageClass: 'Clifton.business.service.ServiceWindow', listWidth: 500,
										listeners: {
											beforequery: function(queryEvent) {
												let inceptionDate = TCG.getValue('inceptionDate', queryEvent.combo.getParentForm().getForm().formValues);
												if (TCG.isNotBlank(inceptionDate)) {
													inceptionDate = TCG.parseDate(inceptionDate).format('m/d/Y');
												}
												else {
													inceptionDate = new Date().format('m/d/Y');
												}
												const s = queryEvent.combo.store;
												s.setBaseParam('activeOnDate', inceptionDate);
											},
											select: function(combo, record, index) {
												const fp = this.getParentForm();
												const serviceProcessingTypeCombo = TCG.getChildByName(fp, 'serviceProcessingType.name');
												serviceProcessingTypeCombo.store.removeAll();
												serviceProcessingTypeCombo.lastQuery = null;
												serviceProcessingTypeCombo.doQuery();
											}
										}
									},
									{fieldLabel: 'AUM Calculator', name: 'businessService.aumCalculatorBean.labelShort', xtype: 'displayfield', qtip: 'AUM calculator as defined by the service.  Can be overridden for this account.'},
									{fieldLabel: 'Pooled Vehicle Type', name: 'clientAccountType', xtype: 'system-list-combo', listName: 'Client Account Types', qtip: 'Client Account Type - Used to define types specifically for our client accounts'},
									{fieldLabel: 'EV Portfolio Code', name: 'externalPortfolioCode.text', hiddenName: 'externalPortfolioCode.id', xtype: 'system-list-combo', listName: 'Client Account: Portfolio Codes', valueField: 'id', qtip: 'EV Portfolio/Product Codes - used to classify our client accounts for Eaton Vance'},
									{fieldLabel: 'Channel', name: 'clientAccountChannel.text', hiddenName: 'clientAccountChannel.id', xtype: 'system-list-combo', listName: 'Client Account: Channels', valueField: 'id', qtip: 'Designated unit to be credited with the "sale" of the product used by the client as indicated on the Client Account.', allowBlank: false}
								]
							}, {
								rows: [
									{
										fieldLabel: 'Processing Type', name: 'serviceProcessingType.name', hiddenName: 'serviceProcessingType.id', xtype: 'combo', url: 'businessServiceProcessingTypeListFind.json', requiredFields: ['businessService.name'], detailPageClass: 'Clifton.business.service.ProcessingTypeWindow',
										qtip: 'Defines the processing type that should be used by this Client Account for its portfolio management. Different processing types may use additional data (tables) specific to them and will drive corresponding UI (usually by adding tabs to Client Account).',
										listeners: {
											afterrender: function(combo) {
												this.store.on('load', function(store, records) {
													if (records.length === 1) {
														combo.setValue(records[0].id);
													}
												});
											},

											beforequery: function(queryEvent) {
												const fp = this.getParentForm();
												const businessServiceId = TCG.getChildByName(fp, 'businessService.name').getValue();
												if (TCG.isBlank(businessServiceId)) {
													return;
												}
												const loader = new TCG.data.JsonLoader({
													waitTarget: fp,
													params: {
														businessServiceId: businessServiceId
													},
													onLoad: function(recArray, conf) {
														if (TCG.isNotBlank(recArray)) {
															const allowedServiceProcessingTypeIdList = [];
															for (const rec of recArray) {
																if (TCG.isNotBlank(rec.businessServiceProcessingType.id)) {
																	allowedServiceProcessingTypeIdList.push(rec.businessServiceProcessingType.id);
																}
															}

															if (allowedServiceProcessingTypeIdList.length === 0) {
																TCG.showError('Cannot find any business service processing types for the selected business service.');
																return;
															}
															queryEvent.combo.store.baseParams = {ids: allowedServiceProcessingTypeIdList};
														}
													}
												});
												loader.loadSynchronous('businessServiceBusinessServiceProcessingTypeListFind.json');
											}
										}
									},
									{fieldLabel: 'AUM Calculator Override', beanName: 'aumCalculatorOverrideBean', xtype: 'system-bean-combo', groupName: 'Investment Account Calculation Snapshot Calculator', qtip: 'If the account should use a different AUM Calculator than what the service uses by default, that selection can be made here.'},
									{fieldLabel: 'GIPS (Account Discretion)', name: 'discretionType', xtype: 'system-list-combo', listName: 'Client Account Discretion Options (GIPS)'},
									{fieldLabel: 'EV Product Code', name: 'externalProductCode.text', hiddenName: 'externalProductCode.id', xtype: 'system-list-combo', listName: 'Client Account: Product Codes', valueField: 'id'}
								],
								config: {labelWidth: 155}
							}]
						},
						{
							xtype: 'formgrid',
							title: 'Service Components',
							storeRoot: 'businessServiceList',
							dtoClass: 'com.clifton.investment.account.InvestmentAccountBusinessService',
							fieldLabel: '&nbsp;',
							labelSeparator: '',
							collapsed: false,
							columnsConfig: [
								{
									header: 'Service Component', width: 600, dataIndex: 'businessService.name', idDataIndex: 'businessService.id',
									editor: {
										xtype: 'combo', url: 'businessServiceListFind.json', displayField: 'name',
										beforequery: function(queryEvent) {
											const grid = queryEvent.combo.gridEditor.containerGrid;
											const f = TCG.getParentFormPanel(grid);
											const serviceId = f.getFormValue('businessService.id');
											if (serviceId) {
												const store = queryEvent.combo.store;
												store.setBaseParam('parentId', serviceId);
												store.setBaseParam('serviceLevelType', 'SERVICE_COMPONENT');
											}
											else {
												TCG.showError('Main Service must be selected before entering additional services.', 'Missing Service');
												return false;
											}
										}
									}
								},
								{header: 'Start Date', dataIndex: 'startDate', width: 120, editor: {xtype: 'datefield', allowBlank: false}},
								{header: 'End Date', dataIndex: 'endDate', width: 120, editor: {xtype: 'datefield'}},
								{header: 'Active', dataIndex: 'active', type: 'boolean', width: 100},
								{header: 'Created On', dataIndex: 'createDate', width: 110, hidden: true},
								{header: 'Updated On', dataIndex: 'updateDate', width: 110, hidden: true}
							],
							listeners: {
								afterload: function(grid) {
									if (grid.getStore().getCount() === 0) {
										grid.collapse();
									}
									grid.getWindow().getMainFormPanel().doLayout(true);
								}
							}
						}
					]
				}]
			},


			{
				title: 'Related Accounts',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				items: [
					{
						title: 'Related Accounts used by this Account',
						xtype: 'investment-account-relationship-grid',
						border: 1,
						flex: 1,

						instructions: 'The following accounts are related to this investment account. More than one relationship may exist between two accounts for different purposes (use cases).',
						showClient: false,
						showDrillDowns: true,
						showStartEndDates: false,
						excludeMainInfo: true,

						editor: {
							detailPageClass: {
								getItemText: function(rowItem) {
									return 'New Relationship';
								},
								items: [
									{text: 'New Account', iconCls: 'account', className: 'Clifton.investment.account.NonCliftonAccountWindow'},
									{text: 'New Relationship', iconCls: 'link', className: 'Clifton.investment.account.AccountRelationshipWindow'}
								]
							},
							getDefaultData: function(gridPanel, row, className) { // defaults client id for the detail page
								const fv = gridPanel.getWindow().getMainForm().formValues;
								if (className === 'Clifton.investment.account.NonCliftonAccountWindow') {
									return {
										ourAccount: false,
										businessClient: TCG.getValue('businessClient', fv)
									};
								}
								return {
									referenceOne: fv
								};
							},
							deleteURL: 'investmentAccountRelationshipDelete.json'
						},
						getLoadParams: function() {
							return {mainAccountId: this.getWindow().getMainFormId()};
						}
					},

					{flex: 0.02},

					{
						title: 'Main Accounts that use this Account',
						xtype: 'investment-account-relationship-grid',
						border: 1,
						flex: 1,

						instructions: 'This account is related to following accounts for the specified purpose. More than one relationship may exist between two accounts for different purposes (use cases).',
						showDrillDowns: true,
						showClient: false,
						showStartEndDates: false,
						excludeRelatedInfo: true,
						getLoadParams: function() {
							return {relatedAccountId: this.getWindow().getMainFormId()};
						}
					}
				]
			}
		]
	}]
});
