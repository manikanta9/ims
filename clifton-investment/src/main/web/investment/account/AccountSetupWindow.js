Clifton.investment.account.AccountSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'investmentAccountSetupWindow',
	title: 'Investment Account Setup',
	iconCls: 'account',
	width: 1700,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Accounts',
				items: [{
					xtype: 'investment-account-grid'
				}]
			},


			{
				title: 'Account Groups',
				items: [{
					xtype: 'investment-account-group-grid'
				}]
			},


			{
				title: 'Relationships',
				items: [{
					xtype: 'investment-account-relationship-grid'
				}]
			},


			{
				title: 'Relationship Mappings',
				items: [{
					xtype: 'gridpanel',
					name: 'investmentAccountRelationshipMappingListFind',
					instructions: 'A list of all allowed mappings between investment account types for a specific purpose. Relationships between investment accounts can be created only if there is a mapping here that allows that type of a relationship.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Main Account Type', width: 175, dataIndex: 'mainAccountType.name', filter: {type: 'combo', searchFieldName: 'mainAccountTypeId', displayField: 'name', url: 'investmentAccountTypeListFind.json'}, defaultSortColumn: true},
						{header: 'Related Account Type', width: 175, dataIndex: 'relatedAccountType.name', filter: {type: 'combo', searchFieldName: 'relatedAccountTypeId', displayField: 'name', url: 'investmentAccountTypeListFind.json'}},
						{header: 'Purpose', dataIndex: 'purpose.name', width: 175, filter: {type: 'combo', searchFieldName: 'purposeId', url: 'investmentAccountRelationshipPurposeListFind.json'}},
						{header: 'Relationship Modify Condition', width: 200, dataIndex: 'relationshipEntityModifyCondition.name', renderer: TCG.renderValueWithTooltip, filter: {type: 'combo', searchFieldName: 'relationshipEntityModifyConditionId', url: 'systemConditionListFind.json'}},
						{header: 'Note', dataIndex: 'note', width: 200, renderer: TCG.renderValueWithTooltip},
						{header: 'One From Main', dataIndex: 'onlyOneAllowedFromMain', width: 100, type: 'boolean'},
						{header: 'One To Related', dataIndex: 'onlyOneAllowedToRelated', width: 100, type: 'boolean'},
						{header: 'Allow Asset Class', dataIndex: 'assetClassAllowed', width: 100, type: 'boolean'},
						{header: 'Start Date', dataIndex: 'startDate', hidden: true},
						{header: 'End Date', dataIndex: 'endDate', hidden: true},
						{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean', sortable: false}
					],
					editor: {
						detailPageClass: 'Clifton.investment.account.AccountRelationshipMappingWindow'
					}
				}]
			},


			{
				title: 'Relationship Purposes',
				items: [{
					xtype: 'gridpanel',
					name: 'investmentAccountRelationshipPurposeListFind',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'An investment account can relate to (use) another investment account for one of specific purposes defined below.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'Relationship Name', dataIndex: 'name', width: 60, defaultSortColumn: true},
						{header: 'Collateral Purpose', dataIndex: 'collateralPurpose.name', width: 60},
						{header: 'Executing Purpose', dataIndex: 'tradingExecutingPurpose.name', width: 60},
						{header: 'Investment Type', dataIndex: 'investmentType.name', width: 45},
						{header: 'Description', dataIndex: 'description', width: 200}
					]
				}]
			},


			{
				title: 'Account Types',
				items: [{
					name: 'investmentAccountTypeListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'Each investment account is of one of the following types. It inherits rules and behavior of its type.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 50, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 150, dataIndex: 'description', hidden: true},
						{header: 'Account Number Label', width: 40, dataIndex: 'accountNumberLabel'},
						{header: 'Account Number2 Label', width: 40, dataIndex: 'accountNumber2Label'},
						{header: 'Contract Category', width: 50, dataIndex: 'contractCategory', tooltip: 'For account types that can be linked to a contract, identifies contract\'s category: IMA, ISDA, REPO, etc.  Can be used instead of contract type for larger selection of contracts.'},
						{header: 'Contract Type', width: 60, dataIndex: 'contractType.name', tooltip: 'For account types that can be linked to a contract, identifies specific contract\'s type: IMA, IMA Amendment, ISDA, etc.'},
						{header: 'Issuer Role', width: 45, dataIndex: 'issuerContractPartyRole.name', tooltip: 'Optionally restricts the issue of the account to the following party role for selected contract: Counterparty, Client, etc.'},
						{header: 'Our Account', width: 25, dataIndex: 'ourAccount', type: 'boolean'},
						{header: 'Exclude Account', width: 25, dataIndex: 'excludedAccount', type: 'boolean', tooltip: 'Exclude accounts of this type from asset calculations: not counted towards AUM, billing, etc.'},
						{header: 'Alias Allowed', width: 25, dataIndex: 'aliasAllowed', type: 'boolean'},
						{header: 'OTC', width: 20, dataIndex: 'otc', type: 'boolean'},
						{header: 'Require Contract', width: 25, dataIndex: 'contractRequired', type: 'boolean'},
						{header: 'Directed', width: 25, dataIndex: 'executionWithIssuerRequired', type: 'boolean', tooltip: 'Directed accounts can only execute trades with the Issuing Company of the account'},
						{header: 'FX from Issuer', width: 25, dataIndex: 'defaultExchangeRateSourceCompanySameAsIssuer', type: 'boolean', tooltip: 'Default FX Rate source company must be the same as holding account\'s issuing company'},
						{header: 'Close same Journal', dataIndex: 'requirePositionClosingFromSameJournalType', width: 30, type: 'boolean', tooltip: 'Specifies that accounts of this type require positions to be closed by the same journal type that they were opened with (i.e. if a position was opened by a transfer is has to be closed by a transfer).'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.investment.account.AccountTypeWindow'
					}
				}]
			},


			{
				title: 'Group Types',
				items: [{
					xtype: 'gridpanel',
					name: 'investmentAccountGroupTypeListFind',
					instructions: 'Investment account group types are used to organize related groups. Account groups can be limited to a specific group type withing various sections of the system.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 50, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 130, dataIndex: 'description'},
						{header: 'Primary Account Allowed', width: 35, dataIndex: 'primaryAccountAllowed', type: 'boolean', tooltip: 'Indicates if the account group type allows for primary a primary account to be selected.'},
						{header: 'Multiple Groups Allowed', width: 35, dataIndex: 'accountAllowedInMultipleGroups', type: 'boolean', tooltip: 'Indicates that a single account can be in multiple groups of this type.'},
						{header: 'System', width: 15, dataIndex: 'systemDefined', type: 'boolean', tooltip: 'Indicates that a single account can be in multiple groups of this type.'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.account.AccountGroupTypeWindow'
					}
				}]
			},


			{
				title: 'Account Mapping',
				items: [{
					xtype: 'investment-account-mapping-grid',
					editor: {
						detailPageClass: 'Clifton.investment.account.mapping.AccountMappingWindow',
						getDefaultData: function(gridPanel) {
							const investmentAccount = TCG.getValue('investmentAccount', gridPanel.grid.getSelectionModel().getSelected().json);
							return {investmentAccount: investmentAccount};
						}
					}
				}]
			},


			{
				title: 'Mapping Purposes',
				items: [{
					name: 'investmentAccountMappingPurposeListFind',
					xtype: 'gridpanel',
					instructions: 'Displays a list of mapping purposes used by the Investment Account Mapping facility.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Purpose Name', width: 50, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 150, dataIndex: 'description'},
						{header: 'Field Name', width: 50, dataIndex: 'fieldName'},
						{header: '1:1 Mapping', type: 'boolean', width: 20, dataIndex: 'oneAccountValueMappingPerPurpose'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPatternControl'}
						];
					},
					getLoadParams: function(firstLoad) {
						const params = {};

						const searchPatternControl = TCG.getChildByName(this.getTopToolbar(), 'searchPatternControl');
						if (TCG.isNotBlank(searchPatternControl.getValue())) {
							params[this.topToolbarSearchParameter] = searchPatternControl.getValue();
						}

						return params;
					},
					editor: {
						detailPageClass: 'Clifton.investment.account.mapping.AccountMappingPurposeWindow'
					}
				}]
			}]
	}]
});

