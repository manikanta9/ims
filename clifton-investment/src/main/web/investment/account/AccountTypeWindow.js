Clifton.investment.account.AccountTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Account Type',
	iconCls: 'account',
	width: 850,
	height: 570,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Investment Account Types classify Investment Accounts. They define rules and behavior for corresponding accounts.',
					url: 'investmentAccountType.json',
					labelWidth: 140,
					readOnly: true,
					items: [
						{fieldLabel: 'Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Account Number Label', name: 'accountNumberLabel'},
						{fieldLabel: 'Account Number2 Label', name: 'accountNumber2Label'},
						{fieldLabel: 'Contract Category', name: 'contractCategory', qtip: 'For account types that can be linked to a contract, identifies contract\'s category: IMA, ISDA, REPO, etc.  Allow selection of various contract types within the category.  Use the type selection to allow only one contract type selection.'},
						{fieldLabel: 'Contract Type', name: 'contractType.name', xtype: 'linkfield', detailIdField: 'contractType.id', detailPageClass: 'Clifton.business.contract.TypeWindow', qtip: 'For account types that can be linked to a contract, identifies contract\'s type: IMA, IMA Amendment, ISDA, etc.'},
						{fieldLabel: 'Issuer Role', name: 'issuerContractPartyRole.name', xtype: 'linkfield', detailIdField: 'issuerContractPartyRole.id', detailPageClass: 'Clifton.business.contract.PartyRoleWindow', qtip: 'Optionally restricts the issue of the account to the following party role for selected contract: Counterparty, Client, etc.'},
						{boxLabel: 'This is our account: issued by us (our company)', name: 'ourAccount', xtype: 'checkbox'},
						{boxLabel: 'Exclude accounts of this type from asset calculations: AUM, Billing, etc.', name: 'excludedAccount', xtype: 'checkbox'},
						{boxLabel: 'Require a Contract to be associated with accounts of this type', name: 'contractRequired', xtype: 'checkbox'},
						{boxLabel: 'Allowed alias to be used with account of this type (assigned by issuing company and used as a reference)', name: 'aliasAllowed', xtype: 'checkbox'},
						{boxLabel: 'This account can only hold OTC securities (other than Collateral)', name: 'otc', xtype: 'checkbox'},
						{boxLabel: 'Accounts of this type can only execute trades with the account\'s defined issuing company', name: 'executionWithIssuerRequired', xtype: 'checkbox'},
						{boxLabel: 'Default FX Rate source company must be the same as holding account\'s issuing company', name: 'defaultExchangeRateSourceCompanySameAsIssuer', xtype: 'checkbox'},
						{boxLabel: 'Only allow closing positions that were opened from the same journal type.', name: 'requirePositionClosingFromSameJournalType', xtype: 'checkbox', qtip: 'Specifies that accounts of this type require positions to be closed by the same journal type that they were opened with (i.e. if a position was opened by a transfer is has to be closed by a transfer).'}
					]
				}]
			},


			{
				title: 'Related Accounts',
				items: [{
					name: 'investmentAccountListFind',
					xtype: 'gridpanel',
					instructions: 'The selected Account Type is associated with the following accounts',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Client Relationship', width: 150, hidden: true, dataIndex: 'businessClient.clientRelationship.name', filter: {type: 'combo', searchFieldName: 'clientRelationshipId', displayField: 'name', url: 'businessClientRelationshipListFind.json'}},
						{header: 'Relationship Category', width: 70, hidden: true, dataIndex: 'businessClient.clientRelationship.relationshipCategory.name', filter: {searchFieldName: 'clientRelationshipCategoryId', type: 'combo', url: 'businessClientRelationshipCategoryListFind.json'}},
						{header: 'Retail', width: 35, dataIndex: 'businessClient.clientRelationship.relationshipCategory.retail', type: 'boolean', filter: {searchFieldName: 'clientRelationshipCategoryRetail'}},
						{header: 'Client', width: 150, dataIndex: 'businessClient.name', filter: {type: 'combo', searchFieldName: 'clientId', displayField: 'name', url: 'businessClientListFind.json'}},
						{header: 'Account Type', width: 75, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'accountTypeId', displayField: 'name', url: 'investmentAccountTypeListFind.json'}, hidden: true},
						{header: 'Account Number', width: 80, dataIndex: 'number'},
						{header: 'Account Name', width: 170, dataIndex: 'name'},
						{header: 'Service', width: 100, dataIndex: 'businessService.name', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true},
						{header: 'Team', width: 70, dataIndex: 'teamSecurityGroup.name', filter: {searchFieldName: 'teamSecurityGroupId', type: 'combo', url: 'securityGroupTeamList.json', loadAll: true}, hidden: true},
						{header: 'Base CCY', width: 50, dataIndex: 'baseCurrency.symbol', filter: {searchFieldName: 'baseCurrencyId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Workflow State', width: 75, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName', comparison: 'BEGINS_WITH'}},
						{header: 'Workflow Status', width: 70, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName', comparison: 'BEGINS_WITH'}, hidden: true},
						{header: 'Effective Start', width: 70, dataIndex: 'workflowStateEffectiveStartDate', filter: {searchFieldName: 'workflowStateEffectiveStartDate'}, hidden: true},
						{header: 'Federal Tax #', width: 40, dataIndex: 'businessClient.federalTaxNumber', filter: {searchFieldName: 'federalTaxNumber'}, hidden: true},
						{header: 'State Tax #', width: 40, dataIndex: 'businessClient.stateTaxNumber', filter: {searchFieldName: 'stateTaxNumber'}, hidden: true},
						{header: 'City', width: 75, dataIndex: 'businessClient.company.city', hidden: true, filter: {searchFieldName: 'city'}},
						{header: 'State', width: 50, dataIndex: 'businessClient.company.state', hidden: true, filter: {searchFieldName: 'state'}},
						{header: 'Postal Code', width: 50, dataIndex: 'businessClient.company.postalCode', hidden: true, filter: {searchFieldName: 'postalCode'}},
						{header: 'Country', width: 75, dataIndex: 'businessClient.company.country', hidden: true, filter: {searchFieldName: 'country'}},
						{header: 'Issued By', width: 100, dataIndex: 'issuingCompany.name', filter: {type: 'combo', searchFieldName: 'issuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{
							header: 'Client Directed', width: 80, dataIndex: 'clientDirected', type: 'boolean', hidden: true,
							tooltip: 'Trading for this account is Directed by the Client (used by Holding Accounts only)'
						}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.investment.account.AccountWindow'
					},
					getLoadParams: function() {
						return {accountTypeId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
