Clifton.investment.account.NonCliftonAccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Holding Account',
	iconCls: 'account',
	width: 1000,
	height: 620,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Account Details',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'InvestmentAccount',
					finalState: true,
					finalWorkflowMessage: 'Use <i>Workflow State</i> and <i>Effective Start</i> fields below for transitions.'
				},
				items: [{
					xtype: 'formpanel-custom-fields',
					columnGroupName: 'Holding Account Fields',
					dynamicFieldFormFragment: 'holdingAccountCustomFields',
					labelFieldName: 'label',
					labelWidth: 130,
					url: 'investmentAccount.json',

					toggleAccountNumbers: function() {
						let label = this.getFormValue('type.accountNumberLabel');
						this.setFieldLabel('number', (label || 'Account Number') + ':');

						label = this.getFormValue('type.accountNumber2Label');
						const o = this.getForm().findField('number2');
						if (TCG.isNull(label)) {
							o.hide();
						}
						else {
							o.setFieldLabel(label + ':');
							o.show();
						}
					},
					listeners: {
						afterload: function(fp) {
							fp.toggleAccountNumbers();

							// Add Additional Tabs starting at position 2 (after related accounts)
							const tabs = fp.getWindow().items.get(0);
							let i = 0;
							Clifton.investment.account.HoldingAccountWindowAdditionalTabs.forEach(t => {
								tabs.insert(i + 2, t);
								i++;
							});
						}
					},
					items: [
						{fieldLabel: 'Client', name: 'businessClient.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailIdField: 'businessClient.id'},
						{
							xtype: 'columnpanel',
							columns: [{
								rows: [
									{
										fieldLabel: 'Account Type', name: 'type.label', hiddenName: 'type.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountTypeListFind.json?ourAccount=false', limitRequestedProperties: false, detailPageClass: 'Clifton.investment.account.AccountTypeWindow', disableAddNewItem: true,
										listeners: {
											select: function(combo, record, index) {
												const fp = combo.getParentForm();
												const form = fp.getForm();
												if (!form.formValues) {
													form.formValues = {};
												}
												form.formValues.type = record.json;
												fp.toggleAccountNumbers();
											}
										}
									},
									{fieldLabel: 'Account Number', name: 'number', xtype: 'textfield'},
									{fieldLabel: 'Account Number2', name: 'number2', xtype: 'textfield', hidden: true}
								],
								config: {columnWidth: 0.37}
							}, {
								rows: [
									{fieldLabel: 'Holding Account ID', xtype: 'displayfield', name: 'id', qtip: 'Used by ALERT platform to help locate account-specific Standard Settlement Instructions (SSIs)'},
									{fieldLabel: 'Account Name', name: 'name', xtype: 'textfield'},
									{fieldLabel: 'Short Name', name: 'shortName', xtype: 'textfield', qtip: 'Optional short name for accounts with long legal names. When populated, will be used within UI for account labels.'}
								],
								config: {columnWidth: 0.63, labelWidth: 150}
							}]
						},
						{
							xtype: 'columnpanel',
							columns: [{
								rows: [
									{fieldLabel: 'Base Currency', name: 'baseCurrency.label', hiddenName: 'baseCurrency.id', displayField: 'label', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', disableAddNewItem: true},
									{
										fieldLabel: 'Platform', xtype: 'combo', name: 'companyPlatform.name', hiddenName: 'companyPlatform.id', url: 'businessCompanyPlatformListFind.json?active=true', detailPageClass: 'Clifton.business.company.CompanyPlatformWindow',
										qtip: 'A company platform is a Broker-Dealer or RIA program providing access to a variety of investment alternatives including third party investment managers\' strategies. Each platform is unique to a particular company, and the only selections allowed are those associated with the issuing company of this account.',
										requiredFields: ['issuingCompany.id'],
										beforequery: function(queryEvent) {
											const fp = queryEvent.combo.getParentForm();
											const businessCompanyId = fp.getFormValue('issuingCompany.id', true);
											queryEvent.combo.store.setBaseParam('businessCompanyId', businessCompanyId);
										},
										getDefaultData: function(f) { // defaults client/counterparty/isda
											const fp = TCG.getParentFormPanel(this);
											const issuingCompany = fp.getFormValue('issuingCompany');
											return {'businessCompany': issuingCompany};
										}
									},
									{
										boxLabel: 'Wrap Account', xtype: 'checkbox', name: 'wrapAccount', fieldLabel: '', requiredFields: ['companyPlatform.id'],
										qtip: 'Applies only if a platform is selected. Differentiates accounts where the client pays a periodic account fee rather than trade commissions.  Is tied to ‘Platform’ in IMS so that we may differentiate between clients who are vs. are not utilizing the platform WRAP feature: Retail clients generally do while Institutional clients generally do not participate in the WRAP feature/account structure on a platform.'
									},
									{
										fieldLabel: 'Workflow State', xtype: 'workflow-state-combo', tableName: 'InvestmentAccount',
										listeners: {
											// Reset Effective Start Date on State Change
											select: function(combo, record, index) {
												const form = combo.getParentForm().getForm();
												const efDateField = form.findField('workflowStateEffectiveStartDate');
												efDateField.setValue('');
											}
										}
									}
								],
								config: {columnWidth: 0.37}
							}, {
								rows: [
									{
										fieldLabel: 'Issued By', name: 'issuingCompany.name', hiddenName: 'issuingCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourCompany=false', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true,
										beforequery: function(queryEvent) {
											const fp = queryEvent.combo.getParentForm();
											// If Issuer Must be a specific role - include role and contract type/category filters
											const role = fp.getFormValue('type.issuerContractPartyRole');
											if (role) {
												// Client Company
												const clientCompanyId = fp.getFormValue('businessClient.company.id', true);
												const params = {contractCompanyId: clientCompanyId};

												const category = fp.getFormValue('type.contractCategory');
												if (category) {
													params.contractCategoryName = category;
												}
												const type = fp.getFormValue('type.contractType.name');
												if (type) {
													params.contractTypeName = type;
												}
												queryEvent.combo.store.baseParams = params;
											}
										},
										listeners: {
											change: function(field, newValue, oldValue) {
												const fp = TCG.getParentFormPanel(field);
												if (TCG.isTrue(fp.getFormValue('type.defaultExchangeRateSourceCompanySameAsIssuer'))) {
													fp.setFormValue('defaultExchangeRateSourceCompany.id', {value: newValue, text: field.getRawValue()});
												}
											}
										}
									},
									{
										fieldLabel: 'FX Source', name: 'defaultExchangeRateSourceCompany.name', hiddenName: 'defaultExchangeRateSourceCompany.id', xtype: 'combo', url: 'marketDataCompanyForExchangeRatesList.json', loadAll: true, detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true,
										qtip: 'The default company (uses parent name if applies) to use for retrieving FX rates. Applies to both Client and Holding Accounts and can be used to override default behaviour.<br/>Whenever possible, holding account FX Source is used: COALESCE(Holding Account FX Source, Client Account FX Source, Holding Account Issuer, Default: Goldman Sachs)<br/>When holding account is not available (manager balances, billing, etc.), client account FX Source is used: COALESCE(Client Account FX Source, Default: Goldman Sachs).'
									},
									{fieldLabel: 'Initial Margin Multiplier', name: 'initialMarginMultiplier', xtype: 'floatfield', value: '1', qtip: 'This field applies to holding accounts and is usually equal to 1 (no additional requirements: use standard). However, a broker may deem a certain account to be more risky and impose additional requirement: 1.5, 2.5, 3.25, etc.'},
									{fieldLabel: 'Workflow Effective Start', xtype: 'datefield', name: 'workflowStateEffectiveStartDate', qtip: 'Date when the workflow state became effective'}
								],
								config: {columnWidth: 0.63, labelWidth: 150}
							}]
						},
						{
							fieldLabel: 'Contract', name: 'businessContract.label', hiddenName: 'businessContract.id', xtype: 'combo', url: 'businessContractListFind.json', displayField: 'label', detailPageClass: 'Clifton.business.contract.ContractWindow', disableAddNewItem: true,
							beforequery: function(queryEvent) {
								const fp = queryEvent.combo.getParentForm();
								const companyId = fp.getFormValue('businessClient.company.id');
								const params = {companyIdOrRelatedCompany: companyId};
								const category = fp.getFormValue('type.contractCategory');
								if (category) {
									params.categoryName = category;
								}
								const typeId = fp.getFormValue('type.contractType.id');
								if (typeId) {
									params.contractTypeId = typeId;
								}
								const role = fp.getFormValue('type.issuerContractPartyRole');
								if (role) {
									const issuerId = fp.getFormValue('issuingCompany.id', true);
									if (issuerId) {
										params.partyRoleId = role.id;
										params.partyCompanyId = issuerId;
									}
								}
								queryEvent.combo.store.baseParams = params;
							}
						},
						{boxLabel: 'This account is used for speculation rather than hedging (may require higher margin)', xtype: 'checkbox', name: 'usedForSpeculation'},
						{boxLabel: 'Trading for this account is Directed by the Client', xtype: 'checkbox', name: 'clientDirected'},

						{
							xtype: 'fieldset',
							title: 'Advanced Settings',
							collapsed: true,
							items: [{
								xtype: 'formfragment',
								frame: false,
								name: 'holdingAccountCustomFields',
								labelWidth: 165,
								items: []
							}]
						}
					]
				}]
			},


			{
				title: 'Related Accounts',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				items: [
					{
						title: 'Related Accounts used by this Account',
						xtype: 'investment-account-relationship-grid',
						border: 1,
						flex: 1,

						instructions: 'The following accounts are related to this investment account. More than one relationship may exist between two accounts for different purposes (use cases).',
						showClient: false,
						showDrillDowns: true,
						showStartEndDates: false,
						excludeMainInfo: true,
						editor: {
							detailPageClass: 'Clifton.investment.account.AccountRelationshipWindow',
							getDefaultData: function(gridPanel) { // defaults client id for the detail page
								return {
									referenceOne: gridPanel.getWindow().getMainForm().formValues
								};
							},
							deleteURL: 'investmentAccountRelationshipDelete.json'
						},
						getLoadParams: function() {
							return {mainAccountId: this.getWindow().getMainFormId()};
						}
					},

					{flex: 0.02},

					{
						title: 'Main Accounts that use this Account',
						xtype: 'investment-account-relationship-grid',
						border: 1,
						flex: 1,

						instructions: 'This account is related to following accounts for the specified purpose. More than one relationship may exist between two accounts for different purposes (use cases).',
						showDrillDowns: true,
						showClient: false,
						showStartEndDates: false,
						excludeRelatedInfo: true,
						getLoadParams: function() {
							return {relatedAccountId: this.getWindow().getMainFormId()};
						}
					}
				]
			},


			{
				title: 'Contacts',
				items: [{
					xtype: 'business-contact-assignments-grid',
					entityTableNameFilter: 'InvestmentAccount',
					columnOverrides: [
						{dataIndex: 'assignmentEntityLabel', hidden: true}
					]
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'InvestmentAccount',
					showOrderInfo: true
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'InvestmentAccount',
					addTaskCategoryFilter: true
				}]
			},


			{
				title: 'Account Groups',
				items: [{
					xtype: 'investment-account-group-grid',
					instructions: 'This Investment Account belongs to the following Investment Account Group(s).',
					configureAdditionalLoadParams: function(params) {
						params.investmentAccountId = this.getWindow().getMainFormId();
					},
					editor: {
						detailPageClass: 'Clifton.investment.account.AccountGroupWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Account Mappings',
				items: [{
					xtype: 'investment-account-mapping-grid',
					getLoadParams: function(firstLoad) {
						const params = {};

						const searchPatternControl = TCG.getChildByName(this.getTopToolbar(), 'searchPatternControl');
						if (TCG.isNotBlank(searchPatternControl.getValue())) {
							params[this.topToolbarSearchParameter] = searchPatternControl.getValue();
						}

						// get accountID from parent
						const win = this.getWindow();
						const accountId = win.getMainFormId();
						if (TCG.isNotBlank(accountId)) {
							params['investmentAccountId'] = accountId;
						}

						return params;
					}
				}]
			}
		]
	}]
});
