Clifton.investment.account.calculation.CalculationSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'investmentAccountCalculationSetupWindow',
	title: 'Investment Account Calculations',
	iconCls: 'calculator',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Snapshots',
				items: [{
					name: 'investmentAccountCalculationSnapshotListFind',
					xtype: 'gridpanel',
					viewNames: ['AUM View', 'Show All'],
					defaultViewName: 'AUM View',
					reloadOnRender: false, // This is off because after render we switch view which triggers the load with calculation type selected
					additionalPropertiesToRequest: 'investmentAccount.businessService.aumCalculatorBean.labelShort',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Calculation Type', width: 35, hidden: true, dataIndex: 'calculationType.name', filter: {searchFieldName: 'calculationTypeId', type: 'combo', url: 'investmentAccountCalculationTypeList.json', loadAll: true}, viewNames: ['Show All']},
						{header: 'Snapshot Date', width: 45, dataIndex: 'snapshotDate'},
						{header: 'Client Relationship', width: 150, dataIndex: 'investmentAccount.businessClient.clientRelationship.name', filter: {searchFieldName: 'clientRelationshipName'}, hidden: true},
						{header: 'Client', width: 150, dataIndex: 'investmentAccount.businessClient.label', filter: {searchFieldName: 'clientName'}, hidden: true},
						{header: 'Investment Account', width: 150, dataIndex: 'investmentAccount.label', filter: {searchFieldName: 'investmentAccountId', type: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'}},
						{header: 'Service', width: 120, dataIndex: 'investmentAccount.coalesceBusinessServiceGroupName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true},
						{header: 'Team', width: 70, dataIndex: 'investmentAccount.teamSecurityGroup.name', filter: {searchFieldName: 'teamSecurityGroupId', type: 'combo', url: 'securityGroupTeamList.json', loadAll: true}, hidden: true},
						{
							header: 'AUM Calculator', width: 80, dataIndex: 'investmentAccount.aumCalculatorBean.labelShort', filter: {searchFieldName: 'investmentAccountAumCalculatorBeanId', type: 'combo', displayField: 'labelShort', url: 'systemBeanListFind.json?groupName=Investment Account Calculation Snapshot Calculator'},
							renderer: function(v, metaData, r) {
								const serviceCalculator = TCG.getValue('investmentAccount.businessService.aumCalculatorBean.labelShort', r.json);
								if (TCG.isNotBlank(v) && TCG.isNotEquals(v, serviceCalculator)) {
									metaData.css = 'amountAdjusted';
									metaData.attr = 'qtip=\'Account Override.  Service AUM Calculator is [' + serviceCalculator + ']\'';
								}
								return v;
							}, viewNames: ['AUM View']
						},
						{header: 'Calculated Value', width: 50, dataIndex: 'calculatedValue', type: 'currency'},
						{header: 'Calculated Value CCY', width: 50, dataIndex: 'calculatedValueCurrency.symbol', filter: {searchFieldName: 'calculatedValueCurrencyId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true', displayField: 'symbol'}},
						{header: 'FX Rate to Base', width: 50, dataIndex: 'fxRateToBase', type: 'float'},
						{header: 'Calculated Value Base', width: 50, dataIndex: 'calculatedValueBase', type: 'currency'},
						{header: 'Base Currency', width: 35, hidden: true, dataIndex: 'calculationType.baseCurrency.symbol', filter: {searchFieldName: 'baseCurrencyId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true', displayField: 'symbol'}, viewNames: ['Show All']}
					],
					listeners: {
						afterrender: function() {
							this.switchToView(this.defaultViewName);
						}
					},
					switchToViewBeforeReload: function(viewName) {
						// Default Calculation Type filter
						if (TCG.isEquals('AUM View', viewName)) {
							const calcTypeField = TCG.getChildByName(this.getTopToolbar(), 'calculationTypeId');
							TCG.data.getDataPromiseUsingCaching('investmentAccountCalculationTypeByName.json?requestedPropertiesRoot=data&requestedProperties=id|name|labelWithCurrency&name=AUM', this, 'investment.account.calculation.type.AUM')
								.then(function(data) {
									const record = {text: data.labelWithCurrency, value: data.id};
									calcTypeField.setValue(record);
									calcTypeField.fireEvent('select', calcTypeField, record);
								});
							return true; // cancel reload: select event will reload
						}
					},
					getLoadParams: function(firstLoad) {
						const loadParams = {};
						if (firstLoad) {
							// Default to 2 business days back - same as admin tab because runs are a day behind?
							this.setFilterValue('snapshotDate', {'on': Clifton.calendar.getBusinessDayFrom(-2)});
						}

						const accountGroupId = TCG.getChildByName(this.getTopToolbar(), 'investmentAccountGroupId').getValue();
						if (TCG.isNotBlank(accountGroupId)) {
							loadParams.investmentAccountGroupId = accountGroupId;
						}
						return loadParams;
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Calculation Type', xtype: 'toolbar-combo', name: 'calculationTypeId', width: 90, url: 'investmentAccountCalculationTypeList.json', loadAll: true, linkedFilter: 'calculationType.name', displayField: 'labelWithCurrency', detailPageClass: 'Clifton.investment.account.calculation.CalculationTypeWindow'},
							{fieldLabel: 'Client Account Group', xtype: 'toolbar-combo', name: 'investmentAccountGroupId', width: 145, url: 'investmentAccountGroupListFind.json?accountTypeNameEqualsOrNull=Client', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'}
						];
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.investment.account.calculation.SnapshotWindow'
					}
				}]
			},


			{
				title: 'Snapshot Details',
				items: [{
					name: 'investmentAccountCalculationSnapshotDetailListFind',
					xtype: 'gridpanel',
					viewNames: ['AUM View', 'Show All'],
					defaultViewName: 'AUM View',
					reloadOnRender: false, // This is off because after render we switch view which triggers the load with calculation type selected
					plugins: {ptype: 'gridsummary'},
					additionalPropertiesToRequest: 'calculationSnapshot.investmentAccount.businessService.aumCalculatorBean.labelShort|child',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'ParentID', dataIndex: 'parent.id', width: 15, hidden: true},
						{header: 'Calculation Type', hidden: true, width: 35, dataIndex: 'calculationSnapshot.calculationType.name', filter: {searchFieldName: 'calculationTypeId', type: 'combo', url: 'investmentAccountCalculationTypeList.json', loadAll: true}, viewNames: ['Show All']},
						{header: 'Snapshot Date', width: 50, dataIndex: 'calculationSnapshot.snapshotDate', filter: {searchFieldName: 'snapshotDate'}},
						{header: 'Client Relationship', width: 150, dataIndex: 'calculationSnapshot.investmentAccount.businessClient.clientRelationship.name', filter: {searchFieldName: 'clientRelationshipName'}, hidden: true},
						{header: 'Client', width: 150, dataIndex: 'calculationSnapshot.investmentAccount.businessClient.label', filter: {searchFieldName: 'clientName'}, hidden: true},
						{header: 'Investment Account', width: 150, dataIndex: 'calculationSnapshot.investmentAccount.label', filter: {searchFieldName: 'investmentAccountId', type: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'}},
						{header: 'Service', width: 120, dataIndex: 'calculationSnapshot.investmentAccount.coalesceBusinessServiceGroupName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true},
						{header: 'Team', width: 70, dataIndex: 'calculationSnapshot.investmentAccount.teamSecurityGroup.name', filter: {searchFieldName: 'teamSecurityGroupId', type: 'combo', url: 'securityGroupTeamList.json', loadAll: true}, hidden: true},
						{
							header: 'AUM Calculator', width: 80, dataIndex: 'calculationSnapshot.investmentAccount.aumCalculatorBean.labelShort', filter: {searchFieldName: 'investmentAccountAumCalculatorBeanId', type: 'combo', displayField: 'labelShort', url: 'systemBeanListFind.json?groupName=Investment Account Calculation Snapshot Calculator'},
							renderer: function(v, metaData, r) {
								const serviceCalculator = TCG.getValue('calculationSnapshot.investmentAccount.businessService.aumCalculatorBean.labelShort', r.json);
								if (TCG.isNotBlank(v) && TCG.isNotEquals(v, serviceCalculator)) {
									metaData.css = 'amountAdjusted';
									metaData.attr = 'qtip=\'Account Override.  Service AUM Calculator is [' + serviceCalculator + ']\'';
								}
								return v;
							}, viewNames: ['AUM View']
						},
						{
							header: 'Asset Class', width: 50, dataIndex: 'investmentAssetClass.label', filter: {searchFieldName: 'investmentAssetClassId', type: 'combo', url: 'investmentAssetClassListFind.json', displayField: 'label'},
							renderer: function(v, metaData, r) {
								if (TCG.isTrue(r.json.child) && TCG.isNotBlank(v)) {
									return '<div style="PADDING-LEFT: 15px">' + v + '</div>';
								}
								return v;
							}
						},

						{header: 'Investment Type', width: 50, hidden: true, dataIndex: 'investmentSecurity.instrument.hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Investment Sub Type', width: 50, hidden: true, dataIndex: 'investmentSecurity.instrument.hierarchy.investmentTypeSubType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeSubTypeId', url: 'investmentTypeSubTypeListFind.json'}},
						{header: 'Investment Sub Type 2', width: 50, hidden: true, dataIndex: 'investmentSecurity.instrument.hierarchy.investmentTypeSubType2.name', filter: {type: 'combo', searchFieldName: 'investmentTypeSubType2Id', url: 'investmentTypeSubType2ListFind.json'}},
						{header: 'Investment Hierarchy', width: 150, hidden: true, dataIndex: 'investmentSecurity.instrument.hierarchy.labelExpanded', filter: {type: 'combo', queryParam: 'labelExpanded', searchFieldName: 'investmentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', listWidth: 600}},
						{header: 'Investment Instrument', width: 50, hidden: true, dataIndex: 'investmentSecurity.instrument.label', filter: {searchFieldName: 'investmentInstrumentId', type: 'combo', url: 'investmentInstrumentListFind.json', displayField: 'label'}},
						{header: 'Investment Security', width: 50, dataIndex: 'investmentSecurity.label', filter: {searchFieldName: 'investmentSecurityId', type: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label'}},
						{header: 'Qty', width: 35, hidden: true, dataIndex: 'quantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true},
						{header: 'Calculated Value CCY', width: 50, dataIndex: 'calculationSnapshot.calculatedValueCurrency.symbol', filter: {searchFieldName: 'calculatedValueCurrencyId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true', displayField: 'symbol'}},
						{header: 'Prelim Calculated Value', width: 50, hidden: true, dataIndex: 'preliminaryCalculatedValue', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true},
						{header: 'Calculated Value', width: 50, dataIndex: 'calculatedValue', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true},
						{header: 'FX Rate to Base', width: 50, dataIndex: 'calculationSnapshot.fxRateToBase', type: 'float', filter: {searchFieldName: 'fxRateToBase'}},
						{
							header: 'Calculated Value Base', width: 50, dataIndex: 'calculatedValueBase', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, summaryType: 'sum',
							summaryTotalCondition: function(data) {
								return TCG.isNull(data.parent);
							}
						},
						{header: 'Base Currency', width: 35, hidden: true, dataIndex: 'calculationSnapshot.calculationType.baseCurrency.symbol', filter: {searchFieldName: 'baseCurrencyId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true', displayField: 'symbol'}, viewNames: ['Show All']}
					],
					viewConfig: {
						getRowClass: function(record) {
							const parent = record.get('parent.id');
							if (TCG.isNotBlank(parent)) {
								// Child
								return 'x-grid3-child-row';
							}
							return ''; // default
						}
					},
					listeners: {
						afterrender: function() {
							this.switchToView(this.defaultViewName);
						}
					},
					switchToViewBeforeReload: function(viewName) {
						// Default Calculation Type filter
						if (TCG.isEquals('AUM View', viewName)) {
							const calcTypeField = TCG.getChildByName(this.getTopToolbar(), 'calculationTypeId');
							TCG.data.getDataPromiseUsingCaching('investmentAccountCalculationTypeByName.json?requestedPropertiesRoot=data&requestedProperties=id|name|labelWithCurrency&name=AUM', this, 'investment.account.calculation.type.AUM')
								.then(function(data) {
									const record = {text: data.labelWithCurrency, value: data.id};
									calcTypeField.setValue(record);
									calcTypeField.fireEvent('select', calcTypeField, record);
								});
							return true; // cancel reload: select event will reload
						}
					},
					getLoadParams: function(firstLoad) {
						const loadParams = {};

						if (firstLoad) {
							// Default to 2 business days back - same as admin tab because runs are a day behind?
							this.setFilterValue('calculationSnapshot.snapshotDate', {'on': Clifton.calendar.getBusinessDayFrom(-2)});
						}

						const accountGroupId = TCG.getChildByName(this.getTopToolbar(), 'investmentAccountGroupId').getValue();
						if (TCG.isNotBlank(accountGroupId)) {
							loadParams.investmentAccountGroupId = accountGroupId;
						}
						const investmentGroupId = TCG.getChildByName(this.getTopToolbar(), 'investmentGroupId').getValue();
						if (TCG.isNotBlank(investmentGroupId)) {
							loadParams.investmentGroupId = investmentGroupId;
						}
						const securityGroupId = TCG.getChildByName(this.getTopToolbar(), 'investmentSecurityGroupId').getValue();
						if (TCG.isNotBlank(securityGroupId)) {
							loadParams.investmentSecurityGroupId = securityGroupId;
						}

						return loadParams;
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Client Account Group', xtype: 'toolbar-combo', name: 'investmentAccountGroupId', width: 145, url: 'investmentAccountGroupListFind.json?accountTypeNameEqualsOrNull=Client', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 145, url: 'investmentGroupListFind.json', detailPageClass: 'Clifton.investment.setup.GroupWindow'},
							{fieldLabel: 'Calculation Type', xtype: 'toolbar-combo', name: 'calculationTypeId', width: 90, url: 'investmentAccountCalculationTypeList.json', loadAll: true, linkedFilter: 'calculationSnapshot.calculationType.name', displayField: 'labelWithCurrency', detailPageClass: 'Clifton.investment.account.calculation.CalculationTypeWindow'},
							{fieldLabel: 'Security Group', xtype: 'toolbar-combo', name: 'investmentSecurityGroupId', width: 145, url: 'investmentSecurityGroupListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityGroupWindow'}
						];
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.investment.account.calculation.SnapshotDetailWindow'
					}
				}]
			},


			{
				title: 'Administration',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						loadValidation: false, // using the form only to get background color/padding
						height: 200,
						labelWidth: 180,
						instructions: 'Process account calculation snapshots for a given date range with selected options.  When going through a date range, every weekday plus month end will be included.  If the date to calculate does not fall on a week day or business day, the calculation may use the previous weekday\'s value if it cannot calculate on that specific date.  For example, any calculation that uses runs would only apply to week day calculations, and holiday runs are often behind.  If you select a specific account or account group the set of accounts will be limited to those selections, however they will only be included in the processing if the account still applies (i.e. active on date)',
						listeners: {
							// Default to 2 business days behind?  Runs today are for previous day so we can run at night for previous day but not in the morning?  At least not for AUM
							afterrender: function() {
								const fp = this;
								fp.setFormValue('startSnapshotDate', Clifton.calendar.getBusinessDayFrom(-2).format('m/d/Y'), true);
								fp.setFormValue('endSnapshotDate', Clifton.calendar.getBusinessDayFrom(-2).format('m/d/Y'), true);

								TCG.data.getDataPromiseUsingCaching('investmentAccountCalculationTypeByName.json?requestedPropertiesRoot=data&requestedProperties=id|name|labelWithCurrency&name=AUM', this, 'investment.account.calculation.type.AUM')
									.then(function(data) {
										fp.setFormValue('calculationTypeLabel', data.name, true);
										fp.setFormValue('calculationTypeId', data.id, true);
									});
							}
						},
						items: [{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .60,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Calculation Type', name: 'calculationTypeLabel', hiddenName: 'calculationTypeId', xtype: 'combo', url: 'investmentAccountCalculationTypeList.json', loadAll: true, allowBlank: false, detailPageClass: 'Clifton.investment.account.calculation.CalculationTypeWindow'},
											{fieldLabel: 'Client Account Group', name: 'investmentAccountGroupLabel', hiddenName: 'investmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json?accountTypeNameEqualsOrNull=Client', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
											{fieldLabel: 'Investment Account', name: 'investmentAccountLabel', hiddenName: 'investmentAccountId', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount-true', displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow'}
										]
									}]
								},
								{columnWidth: .15, items: [{xtype: 'label', html: '&nbsp;'}]},
								{
									columnWidth: .25,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 100,
										items: [
											{fieldLabel: 'Start Date', name: 'startSnapshotDate', xtype: 'datefield', allowBlank: false},
											{fieldLabel: 'End Date', name: 'endSnapshotDate', xtype: 'datefield', allowBlank: false},
											{fieldLabel: '', boxLabel: 'Re-process Existing', name: 'reprocessExisting', xtype: 'checkbox', qtip: 'If snapshots already exist for a given calculation type, account and date, reprocess the account. Leave blank to skip existing.'}
										]
									}]
								}
							]
						}],

						buttons: [
							{
								text: 'Preview Snapshot',
								tooltip: 'Preview Account Snapshot for given date.  Must select a single account to preview for. Start and End Snapshot Dates must be the same.',
								iconCls: 'preview',
								width: 170,
								handler: function() {
									const owner = this.findParentByType('formpanel');
									const form = owner.getForm();
									form.submit(Ext.applyIf({
										url: 'investmentAccountCalculationSnapshotPreview.json',
										waitMsg: 'Processing...',
										success: function(form, action) {
											const data = action.result.data;
											TCG.createComponent('Clifton.investment.account.calculation.SnapshotWindow', {
												defaultData: data,
												defaultDataIsReal: true,
												openerCt: this
											});
										}
									}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
								}
							},
							{
								text: 'Process Snapshots',
								tooltip: 'Process account calculation snapshots',
								iconCls: 'run',
								width: 170,
								handler: function() {
									const formPanel = this.findParentByType('formpanel');
									const form = formPanel.getForm();
									const reProcessExisting = form.findField('reprocessExisting').getValue();
									const groupId = form.findField('investmentAccountGroupId').getValue();
									const accountId = form.findField('investmentAccountId').getValue();
									if (TCG.isTrue(reProcessExisting) && TCG.isBlank(groupId) && TCG.isBlank(accountId)) {
										Ext.Msg.confirm('Re-Process ALL Accounts for Date Range', 'There is no account group or account selected. Are you sure you would like to re-process all account snapshots for the date range?', function(a) {
											if (a === 'yes') {
												formPanel.processSnapshots();
											}
										});
									}
									else {
										formPanel.processSnapshots();
									}
								}
							}
						],

						processSnapshots: function() {
							const url = 'investmentAccountCalculationSnapshotListProcess.json';
							const formPanel = this;
							formPanel.getForm().submit(Ext.applyIf({
								url: encodeURI(url),
								waitMsg: 'Processing...',
								success: function(form, action) {
									Ext.Msg.alert('Processing Started', action.result.result.message, function() {
										const grid = formPanel.ownerCt.items.get(1);
										grid.reload.defer(300, grid);
									});
								}
							}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
						}
					},


					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'INVESTMENT-ACCOUNT-CALCULATION-SNAPSHOTS',
						instantRunner: true,
						title: 'Current Processing',
						flex: 1
					}
				]
			},


			{
				title: 'Calculation Types',
				items: [{
					name: 'investmentAccountCalculationTypeList',
					xtype: 'gridpanel',
					instructions: 'A calculation type represents a specific type of valuation we calculate for an account. Example: AUM',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Calculation Type Name', width: 60, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Base Currency', width: 35, dataIndex: 'baseCurrency.symbol'},
						{header: 'Processor Bean', width: 75, dataIndex: 'processorSystemBean.name'},
						{header: 'Description', width: 150, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.account.calculation.CalculationTypeWindow'
					}
				}]
			},


			{
				title: 'Account Calculators',
				items: [{
					xtype: 'gridpanel',
					name: 'systemBeanListFind',
					instructions: 'The following are account specific calculators that are defined for the account for how the value (snapshot and details) is calculated.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Calculator Name', width: 150, dataIndex: 'labelShort', filter: {searchFieldName: 'labelShort'}, defaultSortColumn: true},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'Type', width: 200, hidden: true, dataIndex: 'type.name', filter: {searchFieldName: 'typeId', type: 'combo', url: 'systemBeanTypeListFind.json?groupName=Investment Account Calculation Snapshot Calculator'}},
						{header: 'Group', width: 150, dataIndex: 'type.group.name', hidden: true, filter: {searchFieldName: 'groupId', type: 'combo', url: 'systemBeanGroupListFind.json'}}
					],
					editor: {
						detailPageClass: 'Clifton.system.bean.BeanWindow',
						getDefaultData: function(gridPanel) {
							const grp = TCG.data.getData('systemBeanGroupByName.json?name=Investment Account Calculation Snapshot Calculator', gridPanel, 'system.bean.group.' + 'Investment Account Calculation Snapshot Calculator');
							return {type: {group: grp}};
						}
					},
					getLoadParams: function(firstLoad) {
						return {groupName: 'Investment Account Calculation Snapshot Calculator'};
					}
				}]
			}
		]
	}]
});

