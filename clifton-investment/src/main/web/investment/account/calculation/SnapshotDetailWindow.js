// not the actual window but looks up the snapshot for the detail and opens that window
Clifton.investment.account.calculation.SnapshotDetailWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'investmentAccountCalculationSnapshotDetail.json',

	openEntityWindow: function(config, entity) {
		const className = 'Clifton.investment.account.calculation.SnapshotWindow';
		if (entity) {
			config.params.id = entity.calculationSnapshot.id;
			entity = TCG.data.getData('investmentAccountCalculationSnapshot.json?id=' + entity.calculationSnapshot.id, this);
		}
		this.doOpenEntityWindow(config, entity, className);
	}
});
