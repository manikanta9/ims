Clifton.investment.account.calculation.CalculationTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Calculation Type',
	iconCls: 'calculator',
	width: 600,
	height: 300,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'A calculation type represents a specific type of analytic we calculate for accounts.  Example: AUM',
		url: 'investmentAccountCalculationType.json',
		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 35, grow: true},
			{fieldLabel: 'Base CCY', name: 'baseCurrency.label', hiddenName: 'baseCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', qtip: 'The base currency for this calculation.  Usually USD and used so we can report across accounts in the same currency for a given calculation type.', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', disableAddNewItem: true},
			{
				fieldLabel: 'Processor Bean', name: 'processorSystemBean.name', hiddenName: 'processorSystemBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Investment Account Calculation Processor',
				detailPageClass: 'Clifton.system.bean.BeanWindow',
				getDefaultData: function() {
					return {type: {group: {name: 'Investment Account Calculation Processor'}}};
				}
			}
		]
	}]
});
