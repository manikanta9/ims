Clifton.investment.account.calculation.SnapshotWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Calculation Snapshot',
	iconCls: 'calculator',
	width: 1000,
	height: 700,

	items: [{
		xtype: 'formpanel',
		url: 'investmentAccountCalculationSnapshot.json',
		labelWidth: 160,
		readOnly: true,
		items: [
			{fieldLabel: 'Calculation Type', name: 'calculationType.name', detailIdField: 'calculationType.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.calculation.CalculationTypeWindow'},
			{fieldLabel: 'Investment Account', name: 'investmentAccount.label', detailIdField: 'investmentAccount.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{fieldLabel: 'Snapshot Date', name: 'snapshotDate', xtype: 'datefield'},
			{xtype: 'label', html: '<hr/>'},
			{
				fieldLabel: 'Calculated Value', xtype: 'compositefield',
				items: [
					{name: 'calculatedValue', xtype: 'currencyfield'},
					{name: 'calculatedValueCurrency.label', xtype: 'displayfield'}
				]
			},
			{
				fieldLabel: 'FX Rate to Base', xtype: 'compositefield',
				items: [
					{name: 'fxRateToBase', xtype: 'floatfield'}
				]
			},
			{
				fieldLabel: 'Calculated Value (Base)', xtype: 'compositefield',
				items: [
					{name: 'calculatedValueBase', xtype: 'currencyfield'},
					{name: 'calculationType.baseCurrency.label', xtype: 'displayfield'}
				]
			},
			{
				xtype: 'formgrid-scroll',
				title: 'Calculation Details',
				storeRoot: 'detailList',
				readOnly: true,
				height: 425,
				heightResized: true,
				plugins: {ptype: 'gridsummary'},
				columnsConfig: [
					{header: 'ID', dataIndex: 'id', width: 15, hidden: true},
					{header: 'ParentID', dataIndex: 'parent.id', width: 15, hidden: true},
					{header: 'Child', dataIndex: 'child', width: 15, type: 'boolean', hidden: true},
					{
						header: 'Asset Class', dataIndex: 'investmentAssetClass.label', width: 150,
						renderer: function(v, metaData, r) {
							if (TCG.isTrue(r.json.child) && TCG.isNotBlank(v)) {
								return '<div style="PADDING-LEFT: 15px">' + v + '</div>';
							}
							return v;
						}
					},
					{header: 'Security', dataIndex: 'investmentSecurity.label', width: 200},
					{header: 'Qty', dataIndex: 'quantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, width: 75},
					{header: 'Prelim Calculated Value', dataIndex: 'preliminaryCalculatedValue', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, width: 150},
					{
						header: 'Calculated Value', dataIndex: 'calculatedValue', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, width: 150, summaryType: 'sum',
						summaryTotalCondition: function(data) {
							return TCG.isFalse(data.child);
						}
					},
					{
						header: 'Calculated Value (Base)', dataIndex: 'calculatedValueBase', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, width: 150, summaryType: 'sum',
						summaryTotalCondition: function(data) {
							return TCG.isFalse(data.child);
						}
					}
				],
				viewConfig: {
					getRowClass: function(record) {
						const child = record.get('child');
						if (TCG.isTrue(child)) {
							// Child
							return 'x-grid3-child-row';
						}
						return ''; // default
					}
				}
			}
		]
	}]
});
