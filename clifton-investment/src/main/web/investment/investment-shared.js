Ext.ns('Clifton.investment', 'Clifton.investment.account', 'Clifton.investment.account.mapping', 'Clifton.investment.calendar', 'Clifton.investment.account.assetclass', 'Clifton.investment.account.group', 'Clifton.investment.account.assetclass.rebalance', 'Clifton.investment.instrument.currency', 'Clifton.investment.instrument.event', 'Clifton.investment.instrument.event.action', 'Clifton.investment.instrument.event.payout', 'Clifton.investment.instrument.structure', 'Clifton.investment.manager', 'Clifton.investment.manager.assignment', 'Clifton.investment.manager.upload', 'Clifton.investment.manager.rule', 'Clifton.investment.manager.custodian', 'Clifton.investment.rates', 'Clifton.investment.replication', 'Clifton.investment.replication.history', 'Clifton.investment.replication.upload', 'Clifton.investment.setup', 'Clifton.investment.account.assetclass.position', 'Clifton.investment.instrument.copy', 'Clifton.investment.instruction', 'Clifton.investment.instruction.delivery', 'Clifton.investment.instruction.setup', 'Clifton.investment.instrument.upload', 'Clifton.investment.instrument.allocation', 'Clifton.investment.manager.balance', 'Clifton.investment.manager.balance.stale', 'Clifton.investment.account.targetexposure', 'Clifton.investment.instruction.run', 'Clifton.investment.specificity', 'Clifton.investment.account.calculation', 'Clifton.investment.account.securitytarget');

/*
 * Constants
 */

// UI Overrides for other Projects
Clifton.business.company.CompanyWindowOverrides['Broker'] = {className: 'Clifton.investment.manager.BrokerCompanyWindow'};
Clifton.business.company.CompanyWindowOverrides['Custodian'] = {className: 'Clifton.investment.manager.CustodianCompanyWindow'};
Clifton.business.company.CompanyWindowOverrides['Investment Manager'] = {className: 'Clifton.investment.manager.ManagerCompanyWindow'};
Clifton.business.company.CompanyWindowOverrides['Our Companies'] = {className: 'Clifton.investment.manager.ManagerCompanyWindow'};
Clifton.business.service.ServiceWindowAdditionalTabs.push({
	title: 'Client Accounts',
	items: [{
		xtype: 'investment-account-grid',
		getLoadParams: function() {
			return {
				...TCG.grid.GridPanel.prototype.getLoadParams.apply(this, arguments),
				businessServiceOrParentId: this.getWindow().getMainFormId()
			};
		},
		getTopToolbarFilters: () => ([{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}]),
		getTopToolbarInitialLoadParams: () => ({}),
		getColumnOverrides: () => ([{dataIndex: 'businessService.name', hidden: true}]),
		switchToViewBeforeReload: () => true,
		listeners: {afterrender: panel => panel.setDefaultView('Simple View')}
	}]
});


// Simple Uploads
Clifton.system.schema.SimpleUploadWindows['InvestmentSecurity'] = 'Clifton.investment.instrument.upload.SecurityUploadWindow';
Clifton.system.schema.SimpleUploadWindows['InvestmentSecurityAllocation'] = 'Clifton.investment.instrument.upload.SecurityAllocationUploadWindow';

Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs = {};
Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs['DEFAULT'] = [];
Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs['DEFAULT'].push({
	title: 'Allocations',
	items: [
		{xtype: 'investment-security-allocations-grid'}
	]
});

Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs['DEFAULT'].push({
	title: 'Rebalance Dates',
	items: [
		{xtype: 'investment-security-allocations-rebalanceDates-grid'}
	]
});

Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs['DEFAULT'].push({
	title: 'Security Events',
	items: [
		{xtype: 'investment-security-allocations-events-grid'}
	]
});

Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs['WEIGHTED_BASKET_NO_PRICE'] = [];
// Default to Custom view for allocations, No Need for Rebalance Dates and Security Events tabs - don't apply
Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs['WEIGHTED_BASKET_NO_PRICE'].push({
	title: 'Allocations',
	items: [
		{
			xtype: 'investment-security-allocations-grid',
			defaultView: 'Portfolio Details'
		}
	]
});

Clifton.investment.account.securitytarget.SecurityTargetToolbarItems = [];

Clifton.investment.account.securitytarget.SecurityTargetListWindowAdditionalTabs = [];

/**
 * Array of instrument window standard tab overrides. Overrides are matched by the title attribute and replace the existing/overridden standard tab.
 * @type {*[]}
 */
Clifton.investment.instrument.InstrumentStandardTabOverrides = [];

/**
 * Array of additional instrument window standard tabs. This can be used to add tabs from other modules to avoid dependency issues.
 * @type {*[]}
 */
Clifton.investment.instrument.InstrumentStandardAdditionalTabs = [];

/**
 * A map of additional instrument window custom tab arrays by security type. This can be used to add tabs from other modules to avoid dependency issues.
 * Custom tabs support the use of an index property for ordering.
 */
Clifton.investment.instrument.InstrumentCustomAdditionalTabsByType = {};

/**
 * A map of additional tab definitions by security window. The key of 'DEFAULT' can be used if window specific options are not available.
 */
Clifton.investment.instrument.SecurityAdditionalTabsByType = {};

Clifton.investment.instrument.CurrencyTypes = [
	['DOMESTIC', 'Domestic', 'Domestic instruments. An instrument is considered domestic if its country of risk is the same as that of the base currency for the account or investment group. An instrument without a country of risk is considered domestic if its currency denomination is the same as the base currency for the account or investment group.'],
	['INTERNATIONAL', 'International', 'International instruments. An instrument is considered international if its country of risk differs from that of the base currency for the account or investment group. An instrument without a country of risk is considered international if its currency denomination differs from the base currency for the account or investment group.'],
	['BOTH', 'All', 'All instruments, regardless of domicile.']
];


Clifton.investment.manager.balance.MANAGER_BALANCE_TYPES = [
	['CASH', 'Cash', 'Include cash balances only.'],
	['CASH_ADJUSTED', 'Cash (Adjusted)', 'Include adjusted cash balances only.'],
	['CASH_MARKET_ON_CLOSE', 'Cash (Market on Close)', 'Include market-on-close cash balances only.'],
	['SECURITIES', 'Securities', 'Include security balances only.'],
	['SECURITIES_ADJUSTED', 'Securities (Adjusted)', 'Include adjusted security balances only.'],
	['SECURITIES_MARKET_ON_CLOSE', 'Securities (Market on Close)', 'Include market-on-close security balances only.'],
	['TOTAL', 'Total', 'Include entire manager account balance.'],
	['TOTAL_ADJUSTED', 'Total (Adjusted)', 'Include entire adjusted manager account balance.'],
	['TOTAL_MARKET_ON_CLOSE', 'Total (Market on Close)', 'Include entire market-on-close manager account balance.']
];

/**
 * Map to use for specifying form overrides by processing type
 * @type {{*}}
 */
Clifton.investment.manager.assignment.MANAGER_ASSIGNMENT_FORM_PROCESSING_TYPE_OVERRIDES = {};

Clifton.investment.instruction.RunTypes = [
	['EMAIL', 'EMAIL'],
	['FAX', 'FAX'],
	['FTP', 'FTP'],
	['SWIFT', 'SWIFT']
];

Clifton.investment.instruction.StatusList = [
	['ERROR', 'ERROR', 'An error occurred while attempting to send the instruction or items. More information is available in the Runs tab for the given instruction. If an instruction or items has an ERROR status but there are no Runs, an error has occurred while generating the file to be sent and the IMS error logs should be reviewed.'],
	['OPEN', 'OPEN', 'The instruction or item may be sent. Note: Instructions and Item must also be "ready" for sending before they can be sent.'],
	['SEND', 'SEND', 'The instruction or item has been marked as ready to be sent. IMS will begin processing the items shortly.'],
	['SENDING', 'SENDING', 'Our system is currently processing the instruction items and is preparing to transfer it to the Integration server'],
	['SENT', 'SENT', 'The instruction or item has been sent to the Integration server for transmission'],
	['COMPLETED', 'COMPLETED', 'The instruction or item has successfully been sent or marked as such by a user.'],
	['CANCELED', 'CANCELED', 'The item was ammended or moved to another instruction but was already sent.'],
	['PENDING_CANCEL', 'PENDING CANCEL', 'In the process of being cancelled.'],
	['SENT_CANCEL', 'SENT_CANCEL', 'The cancelled instruction or item has been sent to the Integration server for transmission'],
	['ERROR_CANCEL', 'ERROR_CANCEL', 'An error occurred while attempting to send the cancellation instruction or items.']
];

// Note: Enum is in IMS: InvestmentAccountPositionValueCalculatorImpl
Clifton.investment.account.calculation.PositionValueTypes = [
	['MARKET_VALUE', 'MARKET_VALUE', 'Base Market Value of the Position'],
	['NOTIONAL', 'NOTIONAL', 'Base Notional of the Position'],
	['DIRTY_PRICE_NOTIONAL', 'DIRTY_PRICE_NOTIONAL', 'Quantity * Dirty Price * Price Multiplier * FX Rate (To Account Base Currency).'],
	['DURATION_ADJUSTED_NOTIONAL', 'DURATION_ADJUSTED_NOTIONAL', 'Duration Adjusted Base Notional of the Position. Duration adjustment = Security Duration / Benchmark Duration.  The benchmark duration is the largest duration found from currently active managers assigned to the account with a benchmark defined.'],
	['QUANTITY_UNADJUSTED', 'QUANTITY_UNADJUSTED', 'Original quantity or Unadjusted Face. Returns "Remaining Quantity" for all securities that do not have Factor Change Event. For securities with Factor Change Event (CDS and ABS), returns opening quantity'],
	['QUANTITY_UNADJUSTED_FX_RATE', 'QUANTITY_UNADJUSTED_FX_RATE', 'FX Rate to Base multiplied by Original quantity or Unadjusted Face. Returns "Remaining Quantity" for all securities that do not have Factor Change Event. For securities with Factor Change Event (CDS and ABS), returns opening quantity'],
	['QUANTITY', 'QUANTITY', 'Remaining quantity of the position'],
	['QUANTITY_FX_RATE', 'QUANTITY_FX_RATE', 'FX Rate to Base multipled by Remaining quantity of the position'],
	['STRIKE_PRICE_NOTIONAL', 'STRIKE_PRICE_NOTIONAL', 'Quantity * Strike Price * Price Multiplier * FX Rate (To Account Base Currency).'],
	['UNDERLYING_PRICE_ADJUSTED_NOTIONAL', 'UNDERLYING_PRICE_ADJUSTED_NOTIONAL', 'Quantity * Underlying Adjusted Price * Price Multiplier * FX Rate (To Account Base Currency).  If the security does not have an underlying security, the security price is used.  Current use case is for Options.'],
	['VEGA_NOTIONAL', 'VEGA_NOTIONAL', 'Used for Variance Swaps. Position Value is the Custom Field Value from the Security for Vega Notional Amount * FX Rate (To Account Base Currency).']
];

// Note: Enum is in IMS: InvestmentAccountPositionValueCalculatorImpl
Clifton.investment.account.calculation.PositionSummationTypes = [
	['GROSS', 'GROSS', 'Each Calculated Value is ABS of its Value'],
	['NET', 'NET', 'Each Calculated Value is as is'],
	['GROSS_NET_BY_SECURITY', 'GROSS_NET_BY_SECURITY', 'If there is one record for a security calculated value is ABS of its value.  If there are multiple, a "rollup" record is created with the gross of the net of the sum of the individual security amounts'],
	['GROSS_NET_BY_UNDERLYING', 'GROSS_NET_BY_UNDERLYING', 'If there is one record for an underlying security, the calculated value is ABS of its value.  If there are multiple, a "rollup" record is created with the gross of the net of the sum of the individual security amounts']
];

Clifton.investment.calendar.EventScopes = [
	['Allow Investment Account', 'INVESTMENT_ACCOUNT', 'Allow an investment account to be assigned to events of this type'],
	['Require Investment Account', 'INVESTMENT_ACCOUNT_REQUIRED', 'Require an investment account to be assigned to events of this type'],
	['Require Team', 'TEAM_REQUIRED', 'Require a team (Security Group) to be assigned to events of this type']
];

/*
 * Classes
 */
Clifton.investment.manager.StaleBalanceSummaryGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentManagerAccountBalanceStaleSummaryListFind',
	xtype: 'gridpanel',
	additionalPropertiesToRequest: 'custodianCompany.id',
	instructions: 'Summary of stale accounts (absolute value of balance difference < 1) on the provided balance date compared to the average number of stale accounts for last 90 days.  NOTE: Only account with adjusted cash value > 0 are counted.',
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Balance Date', xtype: 'toolbar-datefield', name: 'balanceDate'}
		];
	},
	getLoadParams: function(firstLoad) {
		// default to yesterday
		const t = this.getTopToolbar();
		let dateValue;
		const bd = TCG.getChildByName(t, 'balanceDate');
		if (TCG.isNotBlank(bd.getValue())) {
			dateValue = (bd.getValue()).format('m/d/Y');
		}
		else {
			const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
			dateValue = prevBD.format('m/d/Y');
			bd.setValue(dateValue);
		}
		return {
			balanceDate: dateValue,
			numberOfDaysForAverage: 90,
			managerType: 'STANDARD',
			adjustedCashValueGreaterThanZero: true,
			requestedMaxDepth: 4
		};
	},
	pageSize: 200,
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Custodian', width: 100, dataIndex: 'custodianCompany.name', filter: {type: 'combo', searchFieldName: 'custodianCompanyId', displayField: 'label', url: 'businessCompanyListFind.json?companyType=Custodian'}},
		{header: 'Stale Accounts', width: 20, dataIndex: 'staleCount', type: 'int'},
		{header: 'Average Stale Accounts', width: 20, dataIndex: 'averageStaleCount', type: 'int'}
	],
	editor: {
		detailPageClass: 'Clifton.investment.manager.StaleBalanceDetailListWindow',
		drillDownOnly: true,
		getDefaultData: function(gridPanel, row) { // defaults investment account
			const data = row.json;
			let dateValue;
			const bd = TCG.getChildByName(gridPanel.getTopToolbar(), 'balanceDate');
			if (TCG.isNotBlank(bd.getValue())) {
				dateValue = (bd.getValue()).format('m/d/Y');
			}
			else {
				const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
				dateValue = prevBD.format('m/d/Y');
				bd.setValue(dateValue);
			}
			return {
				custodianCompanyId: data.custodianCompany.id,
				balanceDate: dateValue
			};
		}
	}
});
Ext.reg('investment-manager-staleBalanceSummaryGrid', Clifton.investment.manager.StaleBalanceSummaryGrid);

// Used by various Client Account windows - columns are overridden/different based on the service the account uses
Clifton.investment.manager.assignment.ManagerAssignmentByClientAccountGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentManagerAccountAssignmentListByAccount',
	xtype: 'gridpanel',
	instructions: 'The following manager accounts are assigned to this investment account. Removing a row will only remove selected assignment but will keep the manager for the client.',
	importTableName: [
		{table: 'InvestmentManagerAccountAllocation', label: 'Manager Allocations', importComponentName: 'Clifton.investment.manager.upload.AccountAllocationUploadWindow'}
	],
	getLoadParams: function() {
		return {
			accountId: this.getWindow().getMainFormId()
		};
	},
	initComponent: function() {
		const currentItems = [];
		Ext.each(this.managerAssignmentColumns, function(f) {
			currentItems.push(f);
		});
		Ext.each(this.afterColumns, function(f) {
			currentItems.push(f);
		});
		this.columns = currentItems;
		Clifton.investment.manager.assignment.ManagerAssignmentByClientAccountGrid.superclass.initComponent.call(this);
	},
	managerAssignmentColumns: [],
	afterColumns: [
		{
			width: 25, fixed: true, dataIndex: 'referenceOne.id', filter: false, sortable: false,
			renderer: function(v, args, r) {
				return TCG.renderActionColumn('manager', '', 'Open Manager Account', 'OpenManagerAccount');
			}
		}
	],
	editor: {
		detailPageClass: 'Clifton.investment.manager.assignment.AssignmentWindow',
		deleteURL: 'investmentManagerAccountAssignmentDelete.json',
		getDefaultData: function(gridPanel) { // defaults client account for the detail page
			return {
				referenceTwo: gridPanel.getWindow().getMainForm().formValues
			};
		}
	},
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const row = grid.store.data.items[rowIndex];
					const mgrId = row.json.referenceOne.id;
					const gridPanel = grid.ownerGridPanel;
					gridPanel.editor.openDetailPage('Clifton.investment.manager.AccountWindow', gridPanel, mgrId, row);
				}
			}
		}
	}
});
Ext.reg('investment-manager-assignment-by-client-account-grid', Clifton.investment.manager.assignment.ManagerAssignmentByClientAccountGrid);


Clifton.business.client.ClientWindowBaseAdditionalTabs.push({
	addAdditionalTabs: function(w) {
		const tabs = w.items.get(0);
		const pos = w.getTabPosition(w, 'Contracts');
		tabs.insert(pos + 1, this.accountsTab);
		tabs.insert(pos + 2, this.managerAccountsTab);
	},
	accountsTab: {
		title: 'Investment Accounts',
		items: [{
			xtype: 'investment-accountsGrid_byClient',
			isIncludeRelatedClientCheckbox: function() {
				return this.getWindow().isIncludeRelatedCheckbox();
			},
			getRelatedClientCheckboxLabel: function() {
				return this.getWindow().getRelatedCheckboxLabel('Accounts');
			}
		}]
	},
	managerAccountsTab: {
		title: 'Manager Accounts',
		items: [{
			xtype: 'investment-managerAccountsGrid_byClient',
			isIncludeRelatedClientCheckbox: function() {
				return this.getWindow().isIncludeRelatedCheckbox();
			},
			getRelatedClientCheckboxLabel: function() {
				return this.getWindow().getRelatedCheckboxLabel('Manager Accounts');
			}
		}]
	}
});

Clifton.investment.account.openCliftonAccountCustomFieldWindow = function(columnGroupName, accountId, dynamicTriggerFieldValueOverride) {
	const customDetailClass = 'Clifton.investment.account.CliftonAccountCustomColumnWindow';
	const cmpId = TCG.getComponentId(customDetailClass, columnGroupName + accountId + dynamicTriggerFieldValueOverride);
	TCG.createComponent(customDetailClass, {
		id: cmpId,
		params: {id: accountId, columnGroupName: columnGroupName, dynamicTriggerFieldValueOverride: dynamicTriggerFieldValueOverride}
	});
};

//use to insert portfolio specific tabs based on service type processing type
Clifton.investment.account.CliftonAccountPortfolioTabs = {};

Clifton.investment.account.CliftonAccountPortfolioTabs['PORTFOLIO_RUNS'] = [];
Clifton.investment.account.CliftonAccountPortfolioTabs['PORTFOLIO_RUNS'].push(
	{
		title: 'Managers',
		items: [{
			name: 'investmentManagerAccountAssignmentListByAccount',
			xtype: 'investment-manager-assignment-by-client-account-grid',
			viewNames: ['Manager View', 'Balance Options', 'Overlay Options', 'Rebalancing Options'],
			managerAssignmentColumns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Client', width: 85, dataIndex: 'referenceOne.client.name', hidden: true},
				{header: 'Client Account', width: 85, dataIndex: 'referenceTwo.label', hidden: true},
				{header: 'Manager Company', width: 200, dataIndex: 'referenceOne.managerCompany.name', viewNames: ['Manager View'], defaultSortColumn: true},
				{header: 'Manager Account (Short)', width: 150, dataIndex: 'referenceOne.labelShort', viewNames: ['Manager View'], tooltip: 'Includes manager account number and, when available, account name.'},
				{header: 'Display Name', width: 150, dataIndex: 'displayName', viewNames: ['Manager View']},
				{header: 'Manager Account', width: 150, hidden: true, dataIndex: 'referenceOne.label', viewNames: ['Balance Options', 'Overlay Options', 'Rebalancing Options']},
				{header: 'Custodial Account', width: 200, dataIndex: 'referenceOne.custodianLabel', viewNames: ['Manager View', 'Balance Options']},
				{header: 'Type', dataIndex: 'referenceOne.managerType', width: 80, viewNames: ['Manager View', 'Balance Options']},
				{header: 'Cash Type', dataIndex: 'cashTypeLabel', width: 60, hidden: true, viewNames: ['Overlay Options']},
				{header: 'Cash Overlay Type', dataIndex: 'cashOverlayTypeLabel', width: 60, hidden: true, viewNames: ['Overlay Options']},
				{header: 'Cash Adj Type', dataIndex: 'referenceOne.cashAdjustmentType', width: 85, viewNames: ['Manager View', 'Balance Options']},
				{header: 'Custom Cash Date', dataIndex: 'referenceOne.customCashValueUpdateDate', type: 'date', width: 60, hidden: true},
				{
					header: 'Custom Cash Allocation', dataIndex: 'referenceOne.customCashValue', width: 100, type: 'currency', useNull: true, hidden: true, viewNames: ['Balance Options'],
					renderer: function(v, metaData, r) {
						if (TCG.isNotBlank(r.data['referenceOne.customCashValueUpdateDate'])) {
							metaData.css = 'amountAdjusted';
							metaData.attr = 'qtip=\'' + 'Last Updated: ' + TCG.renderDate(r.data['referenceOne.customCashValueUpdateDate']) + '\'';
						}
						if (r.data['referenceOne.cashAdjustmentType'] === 'CUSTOM_PERCENT') {
							return TCG.renderAmount(r.data['referenceOne.customCashValue'], false, '0,000.0000 %');
						}
						return TCG.renderAmount(r.data['referenceOne.customCashValue'], false, '0,000.00');
					}
				},
				{
					header: 'Allocate Securities', width: 110, dataIndex: 'allocateSecuritiesBalanceToReplications', type: 'boolean',
					tooltip: 'Allocate Securities Balance Allocations to Replication\'s Additional Exposure',
					viewNames: ['Overlay Options']
				},

				{header: 'Proxy Date', dataIndex: 'referenceOne.proxyValueDate', type: 'date', width: 60, hidden: true},
				{
					header: 'Proxy Value', dataIndex: 'referenceOne.proxyValue', useNull: true, width: 100, type: 'currency', hidden: true, viewNames: ['Balance Options'],
					renderer: function(v, metaData, r) {
						if (TCG.isNotBlank(r.data['referenceOne.proxyValueDate'])) {
							metaData.css = 'amountAdjusted';
							metaData.attr = 'qtip=\'' + 'Last Updated: ' + TCG.renderDate(r.data['referenceOne.proxyValueDate']) + '\'';
						}
						return TCG.renderAmount(r.data['referenceOne.proxyValue'], false, '0,000.00');
					}
				},
				{header: 'Add M2M', dataIndex: 'addMarkToMarket', type: 'boolean', width: 60, viewNames: ['Manager View', 'Balance Options']},
				{header: 'Inactive', width: 50, dataIndex: 'inactive', type: 'boolean', viewNames: ['Manager View', 'Balance Options', 'Overlay Options', 'Rebalancing Options']},
				{header: 'Target Exposure', dataIndex: 'targetAllocationPercent', useNull: true, width: 60, hidden: true, type: 'currency', numberFormat: '0,000.0000 %', viewNames: ['Rebalancing Options']},
				{header: 'Rebalance Minimum', dataIndex: 'rebalanceAllocationMin', useNull: true, width: 60, hidden: true, type: 'currency', numberFormat: '0,000.0000 %', viewNames: ['Rebalancing Options']},
				{header: 'Rebalance Maximum', dataIndex: 'rebalanceAllocationMax', useNull: true, width: 60, hidden: true, type: 'currency', numberFormat: '0,000.0000 %', viewNames: ['Rebalancing Options']},
				{header: 'Rebalance - Apply as Absolute Bands', hidden: true, width: 60, dataIndex: 'rebalanceAllocationFixed', type: 'boolean', viewNames: ['Rebalancing Options']}
			]
		}]
	}
);

Clifton.investment.account.CliftonAccountPortfolioTabs['PORTFOLIO_RUNS'].push(
	{
		title: 'Account Rules',
		items: [{
			xtype: 'rule-assignment-grid-forAdditionalScope',
			scopeTableName: 'InvestmentAccount',
			defaultCategoryName: 'Portfolio Run Rules'
		}]
	}
);

Clifton.investment.account.CliftonAccountPortfolioTabs['PORTFOLIO_RUNS'].push(
	{
		title: 'Asset Classes',
		items: [{
			name: 'investmentAccountAssetClassListByAccount',
			xtype: 'gridpanel',
			instructions: 'The following asset class allocation and replication rules are defined for this investment account.',

			importTableName: 'InvestmentAccountAssetClass',
			importComponentName: 'Clifton.investment.account.assetclass.AssetClassUploadWindow',
			getImportComponentDefaultData: function() {
				return {
					investmentAccount: this.getWindow().getMainForm().formValues
				};
			},

			getLoadParams: function() {
				return {accountId: this.getWindow().getMainForm().formValues.id};
			},
			getTopToolbarFilters: function(toolBar) {
				const gridPanel = this;
				const menu = new Ext.menu.Menu();
				menu.add({
					text: 'Portfolio Setup',
					handler: function() {
						gridPanel.openCustomFieldWindow('PIOS Processing');
					}
				});
				menu.add('-');

				menu.add({
					text: 'Account Liability',
					handler: function() {
						gridPanel.openCustomFieldWindow('Account Liability');
					}
				});
				menu.add('-');

				menu.add({
					text: 'Performance Summary Setup',
					handler: function() {
						gridPanel.openCustomFieldWindow('Performance Summary Fields');
					}
				});

				toolBar.add({
					text: 'Setup',
					tooltip: 'Account Setup',
					iconCls: 'account',
					menu: menu
				});
				toolBar.add('-');
			},
			openCustomFieldWindow: function(groupName) {
				const accountId = this.getWindow().getMainFormId();
				Clifton.investment.account.openCliftonAccountCustomFieldWindow(groupName, accountId);
			},
			viewNames: ['Default View', 'Replications', 'Client Directed Trading', 'Exposure', 'Rebalancing'],
			additionalPropertiesToRequest: 'level',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Master Asset Class', width: 120, dataIndex: 'assetClass.masterAssetClass.name', hidden: true},
				{header: 'Parent Asset Class', width: 120, dataIndex: 'parent.label', hidden: true},
				{
					header: 'Asset Class', width: 120, dataIndex: 'assetClass.name', viewNames: ['Default View', 'Replications', 'Client Directed Trading', 'Exposure', 'Rebalancing'],
					renderer: function(v, p, r) {
						const depth = r.json.level - 1;
						if (r.data['rollupAssetClass'] === true) {
							return '<div style="FONT-WEIGHT: bold; COLOR: #' + ((depth > 0) ? '777777' : '000000') + '; PADDING-LEFT: ' + 15 * depth + 'px">' + v + '</div>';
						}
						else if (depth > 0) {
							return '<div style="COLOR: #777777; PADDING-LEFT: ' + 15 * depth + 'px">' + v + '</div>';
						}
						return v;
					}
				},
				{header: 'Display Name', width: 120, dataIndex: 'displayName', tooltip: 'A name for this Account Asset Class that, if defined, replaces the Asset Class Name on reports, messages, and within displayed user interface components.'},
				{header: 'Order', width: 50, dataIndex: 'order', type: 'int', useNull: true, hidden: true, tooltip: 'Sort order used to display asset classes. For nexted asset classes, use hundreds for parent asset classes and corresponding tens for child asset classes.'},
				{header: 'Benchmark', width: 150, dataIndex: 'benchmarkSecurity.label', viewNames: ['Default View']},
				{header: 'Benchmark Duration', width: 150, dataIndex: 'benchmarkDurationSecurity.label', hidden: true},

				{header: 'Primary Replication', width: 120, dataIndex: 'primaryReplication.name', viewNames: ['Default View', 'Replications']},
				{header: 'Secondary Replication', width: 120, dataIndex: 'secondaryReplication.name', hidden: true, viewNames: ['Replications']},
				{header: 'Sec Replication Amount', dataIndex: 'secondaryReplicationAmount', width: 75, type: 'currency', useNull: true, hidden: true, viewNames: ['Replications']},
				{
					header: 'Do Not Adjust Contract Value', dataIndex: 'doNotAdjustContractValue', width: 60, type: 'boolean', hidden: true, viewNames: ['Replications'],
					tooltip: 'If checked, does not apply synthetic or duration adjustments when calculating contract value.'
				},
				{
					header: 'Use Actual for Matching', dataIndex: 'useActualForMatchingReplication', width: 60, type: 'boolean', hidden: true, viewNames: ['Replications'],
					tooltip: 'If checked, will set targets for matching replications based on overlay exposure for what it is matching instead of the overlay target.'
				},
				{header: 'Duration Field', width: 60, hidden: true, dataIndex: 'durationFieldNameOverride', type: 'string', filter: {xtype: 'system-list-combo', listName: 'Portfolio: Duration Field', searchFieldName: 'durationFieldNameOverride'}},
				{header: 'Client Directed Trading Option', dataIndex: 'clientDirectedCashOption', width: 70, hidden: true, viewNames: ['Client Directed Trading']},
				{header: 'Client Directed Trading Amount', dataIndex: 'clientDirectedCashAmount', width: 70, type: 'currency', summaryType: 'sum', hidden: true, useNull: true, viewNames: ['Client Directed Trading']},
				{header: 'Client Directed Trading Date', dataIndex: 'clientDirectedCashDate', width: 70, hidden: true, viewNames: ['Client Directed Trading']},
				{
					header: 'Target Exposure', dataIndex: 'assetClassPercentage', width: 90, type: 'percent', summaryType: 'sum', viewNames: ['Default View', 'Exposure'],
					renderer: function(v, p, r) {
						let style = '';
						let vv = v;
						if (r.data['rollupAssetClass'] === true) {
							style += 'FONT-WEIGHT: bold;';
						}
						if (r.data['parent.label']) {
							style += 'COLOR: #777777;';
							vv = r.data['assetClassPercentageSAVED'];
							if (!vv) {
								r.data['assetClassPercentageSAVED'] = r.data['assetClassPercentage'];
								r.data['assetClassPercentage'] = 0;
								vv = v;
							}
						}
						vv = Ext.util.Format.number(vv, '0,000.0000 %');
						return (TCG.isBlank(style)) ? vv : '<div style="' + style + '">' + vv + '</div>';
					}
				},
				{
					header: 'Fund Cash', dataIndex: 'cashPercentage', width: 90, type: 'percent', summaryType: 'sum', viewNames: ['Default View'],
					renderer: function(v, p, r) {
						let style = '';
						let vv = v;
						if (r.data['rollupAssetClass'] === true) {
							style += 'FONT-WEIGHT: bold;';
						}
						if (r.data['parent.label']) {
							style += 'COLOR: #777777;';
							vv = r.data['cashPercentageSAVED'];
							if (!vv) {
								r.data['cashPercentageSAVED'] = r.data['cashPercentage'];
								r.data['cashPercentage'] = 0;
								vv = v;
							}
						}
						vv = Ext.util.Format.number(vv, '0,000.0000000000 %');
						return (TCG.isBlank(style)) ? vv : '<div style="' + style + '">' + vv + '</div>';
					}
				},
				{header: 'Target Adjustment Type', dataIndex: 'targetExposureAdjustmentType', width: 75, hidden: true, viewNames: ['Exposure']},
				{header: 'Target Adjustment', dataIndex: 'targetExposureAmount', width: 75, type: 'currency', useNull: true, hidden: true, viewNames: ['Exposure']},
				{
					header: 'Performance Summary Overlay Target Source', dataIndex: 'performanceOverlayTargetSourceLabel', width: 75, hidden: true,
					tooltip: 'When processing performance summaries for this account asset class, the overlay target is populated by the selected source from Portfolio runs.'
				},
				{header: 'Rollup', dataIndex: 'rollupAssetClass', width: 50, type: 'boolean', hidden: true},
				{
					header: 'Private', dataIndex: 'privateAssetClass', width: 50, type: 'boolean', viewNames: ['Default View'],
					tooltip: 'Private Asset Classes are used for processing, but are not displayed on the report'
				},
				{
					header: 'Exclude from Account Exposure', dataIndex: 'excludeFromExposure', width: 60, type: 'boolean', hidden: true, viewNames: ['Exposure'],
					tooltip: 'If checked, all other fields (replications, rebalancing, etc) are cleared, targets are set to 0. The asset class can be allocated to by managers, but can\'t be used by other asset classes for TFA, etc. During Portfolio runs, it will be skipped in asset class processing and will not show up in exposure.  Any amounts allocated to it is ignored.'
				},
				{
					header: 'Exclude from Overlay Exposure', dataIndex: 'excludeFromOverlayExposure', width: 60, type: 'boolean', hidden: true, viewNames: ['Exposure'],
					tooltip: 'If checked, overlay exposure for the asset class is always set to 0.'
				},
				{
					header: 'Rebalance Exposure Target', dataIndex: 'rebalanceExposureTarget', width: 75, type: 'percent', useNull: true, hidden: true, viewNames: ['Rebalancing'], summaryType: 'sum',
					tooltip: 'Defines rebalancing targets only for a sub-set of asset classes (investable asset classes that we do replicate). Percentage value is used as target exposure for Portfolio exposure reports that only report against these asset classes. During Portfolio runs, if set for any asset class, sum across all asset classes must equal 100%.',
					renderer: function(v, p, r) {
						let style = '';
						let vv = v;
						if (r.data['rollupAssetClass'] === true) {
							style += 'FONT-WEIGHT: bold;';
						}
						if (r.data['parent.label']) {
							style += 'COLOR: #777777;';
							vv = r.data['rebalanceExposureTargetSAVED'];
							if (!vv) {
								r.data['rebalanceExposureTargetSAVED'] = r.data['rebalanceExposureTarget'];
								r.data['rebalanceExposureTarget'] = 0;
								vv = v;
							}
						}
						vv = Ext.util.Format.number(vv, '0,000.0000 %');
						return (TCG.isBlank(style)) ? vv : '<div style="' + style + '">' + vv + '</div>';
					}
				},
				{header: 'Min Rebalance Trigger', dataIndex: 'rebalanceAllocationMin', width: 60, type: 'percent', useNull: true, hidden: true, viewNames: ['Rebalancing']},
				{header: 'Max Rebalance Trigger', dataIndex: 'rebalanceAllocationMax', width: 60, type: 'percent', useNull: true, hidden: true, viewNames: ['Rebalancing']},
				{header: 'Absolute Min Rebalance Trigger', dataIndex: 'rebalanceAllocationAbsoluteMin', width: 60, type: 'percent', useNull: true, hidden: true, viewNames: ['Rebalancing']},
				{header: 'Absolute Max Rebalance Trigger', dataIndex: 'rebalanceAllocationAbsoluteMax', width: 60, type: 'percent', useNull: true, hidden: true, viewNames: ['Rebalancing']},
				{
					header: 'Apply Triggers as Absolute Bands', dataIndex: 'rebalanceAllocationFixed', width: 60, type: 'boolean', hidden: true, viewNames: ['Rebalancing'],
					tooltip: 'If checked, rebalance trigger percentages are as entered, otherwise they are calculated proportionally.'
				},
				{header: 'Rebalance Trigger Action', dataIndex: 'rebalanceTriggerAction', width: 75, hidden: true, viewNames: ['Rebalancing']},
				{
					header: 'Rebalance Benchmark', width: 100, dataIndex: 'rebalanceBenchmarkSecurity.label', viewNames: ['Rebalancing'], hidden: true,
					tooltip: 'Rebalance Cash is adjusted from the last Rebalance Date based on the performance of this benchmark. If not defined, will use the asset class benchmark security by default.'
				},
				{
					header: 'Do Not Adjust Rebalance Cash', dataIndex: 'skipRebalanceCashAdjustment', width: 60, type: 'boolean', hidden: true, viewNames: ['Rebalancing'],
					tooltip: 'If checked, rebalance cash is not adjusted based on rebalance benchmark performance.'
				}
			],
			plugins: {ptype: 'gridsummary'},
			editor: {
				detailPageClass: {
					getItemText: function(rowItem) {
						return (rowItem.get('rollupAssetClass') === true) ? 'Rollup Asset Class' : 'Asset Class';
					},
					items: [
						{text: 'Asset Class', iconCls: 'grouping', className: 'Clifton.investment.account.assetclass.AssetClassWindow'},
						{text: 'Rollup Asset Class', iconCls: 'hierarchy', className: 'Clifton.investment.account.assetclass.RollupAssetClassWindow'}
					]
				},
				deleteURL: 'investmentAccountAssetClassDelete.json',
				getDefaultDataForExisting: function(gridPanel) {
					return undefined;
				},
				getDefaultData: function(gridPanel) { // defaults client account for the detail page
					return {
						account: gridPanel.getWindow().getMainForm().formValues,
						assetClassPercentage: 0,
						cashPercentage: 0
					};
				}
			}
		}]
	}
);

Clifton.investment.account.CliftonAccountPortfolioTabs['PORTFOLIO_TARGET'] = [...TCG.clone(Clifton.investment.account.CliftonAccountPortfolioTabs['PORTFOLIO_RUNS'])];

Clifton.investment.account.CliftonAccountPortfolioTabs['LDI'] = [];
Clifton.investment.account.CliftonAccountPortfolioTabs['LDI'].push(
	{
		title: 'Manager Accounts',
		items: [{
			xtype: 'investment-manager-assignment-by-client-account-grid',
			managerAssignmentColumns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Manager Company', width: 100, dataIndex: 'referenceOne.managerCompany.name', defaultSortColumn: true},
				{header: 'Account', width: 70, dataIndex: 'referenceOne.labelShort'},
				{header: 'Cash Type', dataIndex: 'cashTypeLabel', width: 60},
				{header: 'Display Name', width: 100, dataIndex: 'displayName'},
				{header: 'Custodial Account', width: 100, dataIndex: 'referenceOne.custodianLabel'},
				{header: 'Benchmark', width: 100, dataIndex: 'benchmarkSecurity.label'},
				{header: 'Inactive', width: 50, dataIndex: 'inactive', type: 'boolean'}
			]
		}]
	}
);

Clifton.investment.account.CliftonAccountPortfolioTabs['LDI'].push(
	{
		title: 'Account Rules',
		items: [{
			xtype: 'rule-assignment-grid-forAdditionalScope',
			scopeTableName: 'InvestmentAccount',
			defaultCategoryName: 'Portfolio Run Rules'
		}]
	}
);


Clifton.investment.account.CliftonAccountPortfolioTabs['DURATION_MANAGEMENT'] = [];
Clifton.investment.account.CliftonAccountPortfolioTabs['DURATION_MANAGEMENT'].push(
	{
		title: 'Manager Accounts',
		items: [{
			xtype: 'investment-manager-assignment-by-client-account-grid',
			managerAssignmentColumns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Manager Company', width: 100, dataIndex: 'referenceOne.managerCompany.name', defaultSortColumn: true},
				{header: 'Account', width: 70, dataIndex: 'referenceOne.labelShort'},
				{header: 'Display Name', width: 100, dataIndex: 'displayName'},
				{header: 'Custodial Account', width: 100, dataIndex: 'referenceOne.custodianLabel'},
				{header: 'Benchmark', width: 100, dataIndex: 'benchmarkSecurity.label'},
				{header: 'Inactive', width: 50, dataIndex: 'inactive', type: 'boolean'}
			]
		}]
	}
);

Clifton.investment.account.CliftonAccountPortfolioTabs['HEDGING'] = [];

Clifton.investment.account.CliftonAccountPortfolioTabs['SECURITY_TARGET'] = [];
Clifton.investment.account.CliftonAccountPortfolioTabs['SECURITY_TARGET'].push(
	{
		title: 'Manager Accounts',
		items: [{
			xtype: 'investment-manager-assignment-by-client-account-grid',
			managerAssignmentColumns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Manager Company', width: 100, dataIndex: 'referenceOne.managerCompany.name', defaultSortColumn: true},
				{header: 'Account', width: 70, dataIndex: 'referenceOne.labelShort'},
				{header: 'Display Name', width: 100, dataIndex: 'displayName'},
				{header: 'Custodial Account', width: 100, dataIndex: 'referenceOne.custodianLabel'},
				{header: 'Inactive', width: 50, dataIndex: 'inactive', type: 'boolean'}
			]
		}]
	}
);

Clifton.investment.account.CliftonAccountPortfolioTabs['SECURITY_TARGET'].push(
	{
		title: 'Security Targets',
		items: [{
			xtype: 'security-target-gridpanel',
			columnOverrides: [
				{dataIndex: 'clientInvestmentAccount.label', hidden: true},
				{dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', hidden: true},
				{dataIndex: 'clientInvestmentAccount.businessService.name', hidden: true}
			]
		}]
	}
);

//use to override manager balance window in product project so process adjustments button can be added to form grid
Clifton.investment.manager.BalanceWindowOverride = 'Clifton.investment.manager.BaseBalanceWindow';


//use to override interest rate index window in market data project so interest rates/chart can be added to the window
Clifton.investment.rates.InterestRateIndexWindowOverride = 'Clifton.investment.rates.BaseInterestRateIndexWindow';


Clifton.investment.setup.InvestmentListWindowAdditionalTabs = [];

Clifton.investment.account.assetclass.position.PositionAllocationListWindowAdditionalTabs = [];


// add/override security screen name to corresponding class mapping
Clifton.investment.instrument.SecurityWindowClassMap = {
	'Benchmark': 'Clifton.investment.instrument.BenchmarkWindow',
	'Benchmark with Client Account': 'Clifton.investment.instrument.BenchmarkWithClientAccountWindow',
	'Bond': 'Clifton.investment.instrument.BondWindow',
	'Currency': 'Clifton.investment.instrument.CurrencyWindow',
	'Future': 'Clifton.investment.instrument.GenericSecurityWindow',
	'GenericSecurity': 'Clifton.investment.instrument.GenericSecurityWindow',
	'OTC Security': 'Clifton.investment.instrument.OTCSecurityWindow',
	'Other: One-to-One': 'Clifton.investment.instrument.OneToOneWindow',
	'Proprietary': 'Clifton.investment.instrument.ProprietarySecurityWindow',
	'Stock': 'Clifton.investment.instrument.StockWindow',
	'Structured Index': 'Clifton.investment.instrument.structure.StructuredSecurityWindow',
	'Structured Index - Model Portfolio': 'Clifton.investment.instrument.structure.StructuredSecurityModelPortfolioWindow',
	'Swap': 'Clifton.investment.instrument.SwapWindow',
	'Swap - Cleared': 'Clifton.investment.instrument.SwapClearedWindow',
	'Swap - Cleared: One-to-Many': 'Clifton.investment.instrument.SwapClearedWindow'
};


//add/override instrument screen name to corresponding class mapping
Clifton.investment.instrument.InstrumentWindowClassMap = {
	'Future': 'Clifton.investment.instrument.FutureWindow',
	'OTC Security': 'Clifton.investment.instrument.OneToManySimpleWindow',
	'Other: One-to-Many': 'Clifton.investment.instrument.OneToManyWindow',
	'Other: One-to-Many-Simple': 'Clifton.investment.instrument.OneToManySimpleWindow',
	'Structured Index': 'Clifton.investment.instrument.structure.StructuredInstrumentWindow',
	'Structured Index - Model Portfolio': 'Clifton.investment.instrument.structure.StructuredInstrumentWindow',
	'Swap': 'Clifton.investment.instrument.OneToManySimpleWindow',
	'Swap - Cleared: One-to-Many': 'Clifton.investment.instrument.OneToManySimpleWindow'
};

// add/override to create Add from Template button on security list for an instrument
// used in otc project to allow creating CDS and TRS swaps by copying properties of an existing one
// defined by the Hierarchy "Investment Sub Type Name" of the instrument, if nothing defined, then attempts by Investment Type name
Clifton.investment.instrument.copy.SecurityHierarchyCopyWindowClassMap = {};
Clifton.investment.instrument.copy.SecurityHierarchyCopyWindowClassMap['Options'] = 'Clifton.investment.instrument.copy.OptionCopyWindow';
Clifton.investment.instrument.copy.SecurityHierarchyCopyWindowClassMap['Forwards'] = 'Clifton.investment.instrument.copy.ForwardCopyWindow';


//add/override event type screen name to corresponding class mapping
// Note: Screen looks up first one found for:
//    1. SecurityType_SecurityTypeSubType_EventType, then
//    2. SecurityType_EventType, then
//    3. EventType
// Which allows for sub-type specific overrides used for Swap Resets that combine multiple events into one and the windows are different for different swap sub-types (See OTC project for overrides)
// If none found, uses 'Clifton.investment.instrument.event.GenericEventWindow'; as the default
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides = {};
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Cash Coupon Payment'] = 'Clifton.investment.instrument.event.CouponPaymentEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Premium Leg Payment'] = 'Clifton.investment.instrument.event.CouponPaymentEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Interest Leg Payment'] = 'Clifton.investment.instrument.event.CouponPaymentEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Fixed Leg Payment'] = 'Clifton.investment.instrument.event.CouponPaymentEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Floating Leg Payment'] = 'Clifton.investment.instrument.event.CouponPaymentEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Interest Shortfall Payment'] = 'Clifton.investment.instrument.event.InterestShortfallPaymentEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Interest Shortfall Reimbursement Payment'] = 'Clifton.investment.instrument.event.InterestShortfallPaymentEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Cash Dividend Payment'] = 'Clifton.investment.instrument.event.DividendPaymentEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Dividend Leg Payment'] = 'Clifton.investment.instrument.event.DividendPaymentEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Equity Leg Payment'] = 'Clifton.investment.instrument.event.EquityLegPaymentEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Factor Change'] = 'Clifton.investment.instrument.event.FactorChangeEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Credit Event'] = 'Clifton.investment.instrument.event.CreditEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Stock Dividend Payment'] = 'Clifton.investment.instrument.event.DividendPaymentEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Scrip Dividend'] = 'Clifton.investment.instrument.event.ScripDividendEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Cash Dividend (with DRIP)'] = 'Clifton.investment.instrument.event.ScripDividendEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Stock Spinoff'] = 'Clifton.investment.instrument.event.StockSpinoffEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Stock Split'] = 'Clifton.investment.instrument.event.StockSplitEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Stock Split (Reverse)'] = 'Clifton.investment.instrument.event.StockSplitEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Symbol Change'] = 'Clifton.investment.instrument.event.SymbolChangeEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Change of Exchange'] = 'Clifton.investment.instrument.event.ExchangeEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Identifier Change'] = 'Clifton.investment.instrument.event.ExchangeEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Company Info Change'] = 'Clifton.investment.instrument.event.ExchangeEventWindow';
Clifton.investment.instrument.event.SecurityEventWindowClassOverrides['Tender Offer'] = 'Clifton.investment.instrument.event.TenderOfferWindow';


Clifton.investment.instrument.event.renderEventStatus = function(v, metaData, record) {
	let result = v;
	if (v === 'Cancelled' || v === 'Deleted') {
		result = '<div class="amountNegative">' + v + '</div>';
	}
	else if (v === 'Approved') {
		result = '<div class="amountPositive">' + v + '</div>';
	}
	else if (v === 'In Conflict') {
		result = '<div class="amountAdjusted">' + v + '</div>';
	}
	return TCG.renderValueWithTooltip(result, metaData);
};

Clifton.investment.instrument.event.FractionalShares = [
	['CASH_IN_LIEU', 'Cash in Lieu'],
	['ROUND_UP', 'Round Up'],
	['ROUND_HALF_UP', 'Round Half Up'],
	['ROUND_DOWN', 'Round Down']
];

// add/override screens  for SecurityEventPayout  with different payout types.
// TODO - Update these as customized windows are added
Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides = {};
Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides['Default'] = {};
Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides.Default['Currency'] = 'Clifton.investment.instrument.event.payout.CurrencySecurityEventPayoutWindow';
Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides.Default['Currency (with Franking Credit)'] = 'Clifton.investment.instrument.event.payout.CurrencyFrankingCreditSecurityEventPayoutWindow';
Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides.Default['Security (New)'] = 'Clifton.investment.instrument.event.payout.NewSecurityEventPayoutWindow';
Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides.Default['Security (Existing)'] = 'Clifton.investment.instrument.event.payout.ExistingSecurityEventPayoutWindow';
Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides.Default['Security (with Franking Credit)'] = 'Clifton.investment.instrument.event.payout.FrankingCreditSecurityEventPayoutWindow';
Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides.Default['Security (Stock Split)'] = 'Clifton.investment.instrument.event.payout.StockSplitSecurityEventPayoutWindow';
Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides.Default['Security (Stock Dividend)'] = 'Clifton.investment.instrument.event.payout.StockDividendSecurityEventPayoutWindow';
Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides.Default['Dutch Auction Payout'] = 'Clifton.investment.instrument.event.payout.AuctionEventPayoutWindow';

Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides['Tender Offer'] = {};
Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides['Tender Offer']['Currency'] = 'Clifton.investment.instrument.event.payout.TenderOfferCurrencyEventPayoutWindow';
Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides['Tender Offer']['Security (New)'] = 'Clifton.investment.instrument.event.payout.TenderOfferSecurityEventPayoutWindow';

// add/override screens
Clifton.investment.setup.InstrumentHierarchyScreens = [
	['Benchmark', 'Benchmark', 'The screen for benchmarks and indices: securities that cannot be traded'],
	['Bond', 'Bond', 'The screen for bonds'],
	['Currency', 'Currency', 'The screen for physical currency'],
	['Future', 'Future', 'The screen for futures'],
	['OTC Security', 'OTC Security', 'Over The Counter (OTC) securities generic screen that supports custom fields and links security to corresponding ISDA.'],
	['Other: One-to-Many', 'Other: One-to-Many', 'Instruments that have multiple securities (futures, options, etc.) but do not have a custom screen built for them'],
	['Other: One-to-Many-Simple', 'Other: One-to-Many-Simple', 'Instruments that have multiple securities but use only few major fields.'],
	['Other: One-to-One', 'Other: One-to-One', 'Instruments that have a single security (bond, stock, etc.) but do not have a custom screen built for them'],
	['Proprietary', 'Proprietary', 'The screen for securities that map to our proprietary funds: Defensive Equity, Commodity LP, etc.'],
	['Stock', 'Stock', 'The screen for common and preferred stocks of public companies.'],
	['Structured Index', 'Structured Index', 'The screen for instruments that define a specific structure to calculate its allocation'],
	['Structured Index - Model Portfolio', 'Structured Index - Model Portfolio', 'The screen for instruments that define a specific structure to calculate its allocation'],
	['Swap', 'Swap', 'The screen for generic Swaps that are OTC traded.'],
	['Swap - Cleared', 'Swap - Cleared', 'The screen for Centrally Cleared Swaps that have one security per instrument.'],
	['Swap - Cleared: One-to-Many', 'Swap - Cleared: One-to-Many', 'Cleared Swaps that have multiple securities per instrument (Inflation Swaps).']
];


Clifton.investment.instrument.NotionalCalculators = [
	['Round Total to 2 Decimals', 'MONEY_HALF_UP', 'Notional = ROUND(Price * Multiplier * Quantity, 2)'],
	['Round Total to 2 Decimals (Divide by Price)', 'MONEY_HALF_UP_DIVIDE', 'Notional = ROUND(Quantity / (Price * Multiplier), 2)'],
	['Round Total to 0 Decimals', 'INTEGER_HALF_UP', 'Notional = ROUND(Price * Multiplier * Quantity, 0)'],
	['Round Total to 0 Decimals (Divide by Price)', 'INTEGER_HALF_UP_DIVIDE', 'Notional = ROUND(Quantity / (Price * Multiplier), 0)'],
	['Round Total Down to 0 Decimals', 'INTEGER_DOWN', 'Notional = ROUND_DOWN(Price * Multiplier * Quantity, 0)'],
	['Round Unit Price to 2 Decimals', 'MONEY_UNIT_HALF_UP', 'Notional = ROUND(ROUND(Price * Multiplier, 2) * Quantity, 2)'],
	['Round Unit Price to 0 Decimals', 'INTEGER_UNIT_HALF_UP', 'Notional = ROUND(ROUND(Price * Multiplier, 0) * Quantity, 0)'],
	['Round Unit Price Down to 0 Decimals', 'INTEGER_UNIT_DOWN', 'Notional = ROUND(ROUND_DOWN(Price * Multiplier, 0) * Quantity, 0)'],
	['BPS Discount Rounded to 2 Decimals', 'BPS_DISCOUNT_MONEY_HALF_UP', 'Notional = ROUND((1 - Price * Multiplier) * Quantity, 2)'],
	['BPS Discount Rounded to 2 Decimals (Negative Quantity)', 'BPS_DISCOUNT_MONEY_HALF_UP_NEGATIVE_QTY', 'Notional = ROUND((1 - Price * Multiplier) * (Quantity > 0 ? : -Quantity : Quantity), 2)'],
	['BPS Discount Rounded to 0 Decimals', 'BPS_DISCOUNT_INTEGER_HALF_UP', 'Notional = ROUND((1 - Price * Multiplier) * Quantity, 0)'],
	['BPS Discount Rounded to 0 Decimals (Negative Quantity)', 'BPS_DISCOUNT_INTEGER_HALF_UP_NEGATIVE_QTY', 'Notional = ROUND((1 - Price * Multiplier) * (Quantity > 0 ? : -Quantity : Quantity), 0)'],
	['SFE 3 Year Australian Bond Future', 'SFE_3YEAR_6PERCENT', '"Unusual" formula that uses 6 payment periods (semi-annual) and 6% annual yield'],
	['SFE 10 Year Australian Bond Future', 'SFE_10YEAR_6PERCENT', '"Unusual" formula that uses 20 payment periods (semi-annual) and 6% annual yield'],
	['IR 90 Day Australian Bank Bills', 'IR_90DAY', 'Formula that calculates Australian 90-day Bank Bill Futures']
];

Clifton.investment.instrument.M2MCalculators = [
	['Cost and Realized', 'COST_AND_REALIZED', 'Realized - Commission  + (Remaining Cost Basis - Prior Remaining Cost Basis)\n\n Cash flows: payment for cost + commission on open (0 for LME); payment of principal + commission on close.'],
	['Daily Gain / Loss', 'DAILY_GAIN_LOSS', 'Mark to Market = Realized + Unrealized + Accruals'],
	['Daily NPV Change', 'DAILY_NPV_CHANGE', 'Mark to Market = BASE NPV + Realized - Commission - Prior BASE NPV'],
	['Daily NPV Change (Realized on Transaction)', 'DAILY_NPV_CHANGE_REALIZED_ON_TRANSACTION', 'Mark to Market = BASE NPV + Realized - Commission - Prior BASE NPV + Realized Adjustment\n\nRealized gain/loss from Coupons and Credit Events is included one day earlier: Transaction Date vs Settlement Date'],
	['Daily OTE Change', 'DAILY_OTE_CHANGE', 'Mark to Market = OTE + Realized - Commission - Prior OTE'],
	['Do Not Mark', 'NONE', 'Mark to Market = 0']
];

Clifton.investment.instrument.AccrualMethods = [
	['DAYCOUNT', 'Day Count', 'Default accrual convention based on [Coupon Frequency] and [Day Count] custom fields. It\'s used by most instruments including all bonds and most swaps.'],
	['DAYCOUNT_COMPOUNDED', 'Day Count Compounded', 'Similar to DAYCOUNT but support multiple interest rate changes during a single payment period. Accrued interest from the first period is compounded during the second period (added to the notional), etc.  InvestmentSecurityEventDetail are used to specify rate changes. It\'s usually used with the second Accrual Event Type (Floating Leg for Interest Rate Swaps)<br /><br />NOTE: uses [Coupon Frequency] and [Day Count] custom fields for Accrual Event Type and [Coupon Frequency 2] and [Day Count 2] custom fields for Accrual Event Type 2.'],
	['DAILY_COMPOUNDING', 'Daily Compounding', 'Daily compounding interest is calculated on the initial notional and also on the accumulated interest from prior interest rates. Supports interest rate changes during a single payment period.<br /><br />Accrual Amount = Notional * (PRODUCT_FOR_EACH_ACCRUAL_DATE(POWER(1 + Ri, 1/DayCountDenominator)) - 1)<br /><br />NOTE: Uses [Coupon Frequency] and [Day Count] custom fields for Accrual Event Type and [Coupon Frequency 2] and [Day Count 2] custom fields for Accrual Event Type2.'],
	['DAILY_NON_COMPOUNDING', 'Daily Non-Compounding', 'Accrual amount is calculated on the sum of daily Ri * Notional. This is a non-compounding, simple interest calculation.<br /><br />Accrual Amount = Sum (Notional * AdjustedDailyRate) where AdjustedDailyRate = Ri / DayCountDenominator<br /><br />NOTE: Uses [Coupon Frequency] and [Day Count] custom fields for Accrual Event Type and [Coupon Frequency 2] and [Day Count 2] custom fields for Accrual Event Type2.'],
	['DAILY_OIS_COMPOUND', 'Daily OIS Compound', 'Accrual Amount = Notional * (PRODUCT_FOR_EACH_ACCRUAL_DATE(1 + Ri/360) - 1)<br />&nbsp;Where Ri is Reference Rate on day i and d is the total number of days in current period<br />OIS == Overnight Index Swap; Reference Rate could be daily FEDFUND rate.<br /><br />NOTE: uses [Coupon Frequency] and [Day Count] custom fields for Accrual Event Type and [Coupon Frequency 2] and [Day Count 2] custom fields for Accrual Event Type 2.'],
	['DAILY_HIGH_RATE', 'Daily High Rate', 'Accrual Amount = Notional * (PRODUCT_FOR_EACH_ACCRUAL_DATE(POWER(1 - Ri*91/360, -1/91)) - 1)<br />&nbsp;Where Ri is Reference rate for PREVIOUS day<br />This calculator is used for USD_TBILL Auction High Rate (91 because it\'s a 3 months T-bill?)<br /><br />NOTE: uses [Coupon Frequency] and [Day Count] custom fields for Accrual Event Type and [Coupon Frequency 2] and [Day Count 2] custom fields for Accrual Event Type 2.'],
	['DAILY_COMPOUNDED_AVERAGE', 'Daily Compounded Average', 'Accrual for period is calculated as an aggregation of daily accruals calculated using the compounded average between dates utilizing a compounded interest rate index with daily rates.<br /><br />Daily average rate between X and Y = (IndexRateY/IndexRateX - 1) * (DayCountDenominator/Dc);<br />&nbsp;X = start date of period, Y = end date of period, Dc = calendar days in period<br /><br />Daily accrual uses the average rate for the day in the period = Notional * DailyAverageRate * (DayCount/DayCountDenominator)'],
	['LATEST_DETAIL_RATE', 'Latest Detail Rate', 'Accrual Amount = Notional * (LATEST EVENT DETAIL EFFECTIVE RATE)<br />&nbsp;Looks up accrual detail with Effective Date closest to Accrue Up To Date and multiplies detail\'s Effective Rate by the Notional (or Quantity).']
];

Clifton.investment.instrument.AccrualDateCalculators = [
	['DO_NOT_ADJUST', 'From Period Start To Transaction Date', 'Start Accrual on first day of current Accrual Period (exclude Start Date: End of Day convention) and accrue up to and including Transaction/Valuation Date.'],
	['ADD_ONE_DAY', 'From Period Start To Transaction Date + 1', 'Start Accrual on first day of current Accrual Period (exclude Start Date: End of Day convention) and accrue up to and including Transaction/Valuation Date + 1 Calendar Day.'],
	['DO_NOT_ADJUST_NO_EX', 'From Period Start To Transaction Date (Zero After Ex)', 'Start Accrual on first day of current Accrual Period (exclude Start Date: End of Day convention) and accrue up to and including Transaction/Valuation Date. Accrual is ZERO from the day before EX DATE until next period start (Cleared CDS to avoid double counting accrual and receivable).'],
	['SETTLEMENT_ON_OPEN', 'From Period Start To Transaction Date (Settlement on Open)', 'Start Accrual on first day of current Accrual Period (exclude Start Date: End of Day convention) and accrue up to and including Transaction/Valuation Date. If Settlement Date of position opening is after Transaction/Valuation Date, accrue up to the Settlement Date of the open.'],
	['DEFAULT_SETTLEMENT', 'From Period Start To Default Settlement Date', 'Start Accrual on first day of current Accrual Period (exclude Start Date: End of Day convention) and accrue up to and including Settlement/Valuation Date (uses instrument/hierarchy Days To Settle).'],
	['SETTLEMENT_CYCLE', 'From Period Start To Settlement Cycle', 'Start Accrual on first day of current Accrual Period (exclude Start Date: End of Day convention) and accrue up to and including Settlement/Valuation Date (uses security Settlement Cycle fields).'],
	['SETTLEMENT_CYCLE_FROM_SETTLEMENT', 'From Settlement Date + 1 To Settlement Cycle', 'Start Accrual the day after opening Settlement date and accrue up to and including Settlement/Valuation Date (uses Settlement Cycle Fields).'],
	['SETTLEMENT_CYCLE_FROM_TRANSACTION', 'From Transaction Date + 1  To Settlement Cycle', 'Start Accrual the day after opening Transaction date and accrue up to and including Settlement/Valuation Date (uses Settlement Cycle Fields).'],
	['DO_NOT_ADJUST_FROM_TRANSACTION', 'From Transaction Date + 1 To Transaction Date', 'Start Accrual the day after opening Transaction date and accrue up to and including Transaction/Valuation Date.']
];

Clifton.investment.instrument.AccrualSignCalculators = [
	['NEGATE', 'Negate', 'Negate the sign of accrued amount: Total Return Swaps, etc.'],
	['NEGATE_EXCEPT_ON_OPENING_TRADE', 'Negate (except on Opening Trade)', 'Negate accrued interest for all calculations except for the accrued interest on the opening trade.'],
	['NEGATE_COUPON_FREQUENCY', 'Negate Coupon Frequency', 'Negate accrued interest for event that uses CUSTOM_FIELD_COUPON_FREQUENCY field and leave it unchanged for other fields.'],
	['NEGATE_NON_COUPON_FREQUENCY', 'Negate Non Coupon Frequency', 'Negate accrued interest for event that DO NOT uses CUSTOM_FIELD_COUPON_FREQUENCY field and leave it unchanged for other fields.'],
	['IRS_STRUCTURE', 'IRS Structure', 'Calculate the sign of accrual amount for Interest Rate Swaps based on "Structure" field. Assumes "Accrual Event Type" is used for Fixed Leg and "Accrual Event Type 2" is used for floating leg.']
];

Clifton.investment.instrument.SecurityAllocationTypes = [
	['Weighted Return', 'WEIGHTED_RETURN', 'Price on start date is 100.  Following the start date the price is calculated as the weighted return from the current price to the start date price.'],
	['Weighted Return (Rebalanced Daily)', 'WEIGHTED_RETURN_ADJUST_DAILY', 'Price on start date is 100.  Following the start date the price is calculated as the weighted return from the current price to the previous price.'],
	['Weighted Return (Rebalanced Monthly)', 'WEIGHTED_RETURN_ADJUST_MONTHLY', 'Price on start date is 100.  Following the start date the price is calculated as the weighted return from the current price to the previous month end price. Note: Optional business day of month is available to indicate when rebalancing is performed.'],
	['Share Price', 'SHARE_PRICE', 'Security Price is calculated as the sum of the notional (number of shares, security price) of each allocation (adjusted where necessary for FX Rate) divided by index divisor.'],
	['Monthly Return (Weighted Average)', 'MONTHLY_RETURN_WEIGHTED_AVERAGE', 'Security Monthly Return Value is a weighted average of its allocations monthly returns for that month.  If monthly return field is not explicitly set for an allocation, it is calculated via price changes.'],
	['Weighted Basket (Not Priced)', 'WEIGHTED_BASKET_NO_PRICE', 'Security stores weighted allocations, but the system does not calculate any market data.  Can be used for cases where the allocations do not have prices in the system, but we store the list of its members. Prices are expected to be imported from another source.']
];


Clifton.investment.instrument.InstrumentDeliverableTypes = [
	['Always (Physical Delivery)', 'ALWAYS', 'All Instruments/Securities in this hierarchy are physically deliverable.'],
	['Never (Cash Settle)', 'NEVER', 'No Instruments/Securities in this hierarchy are physically deliverable. All of them are Cash Settled.'],
	['Both (Physical Delivery or Cash Settle)', 'BOTH', 'Instruments/Securities in this hierarchy can be deliverable or non-deliverable.']
];

Clifton.investment.instrument.InvestmentPricingFrequencies = [
	['Daily', 'DAILY', 'All Instruments/Securities in this hierarchy should have prices daily on all business days.'],
	['Daily Flexible', 'DAILY_FLEXIBLE', 'All Instruments/Securities in this hierarchy should have prices daily on all business days going forward, but historically we may have only had prices monthly.'],
	['Monthly', 'MONTHLY', 'All Instruments/Securities in this hierarchy should have prices monthly.  When new positions are added, if pricing is not available yet, position price will be used.'],
	['Quarterly', 'QUARTERLY', 'All Instruments/Securities in this hierarchy should have prices quarterly.  When new positions are added, if pricing is not available yet, position price will be used.']
];

Clifton.investment.manager.rule.ManagerRuleWarningTypes = [
	['Amount', 'AMOUNT', 'Range for Manager Cash/Securities/Both'],
	['Percent of Manager Total', 'PERCENT_OF_MANAGER', 'Range for Manager Cash or Securities Amount as a Percent of Manager Balance total'],
	['Percent of Total Portfolio', 'PERCENT_OF_TOTAL_PORTFOLIO', 'Range for Manager Cash/Securities/Both Amount as a Percent of total portfolio']
];

// additional tabs to be appended to security event windows can be added to this array
Clifton.investment.instrument.event.AdditionalTabs = [];

// additional tabs to be appended to client account windows can be added to this array
Clifton.investment.account.ClientAccountWindowAdditionalTabs = [];

// additional tabs to be appended to holding account windows can be added to this array
Clifton.investment.account.HoldingAccountWindowAdditionalTabs = [];

// InvestmentAccountAssetClass - Rebalance Trigger Actions
Clifton.investment.account.assetclass.RebalanceActions = [
	['Do Nothing', 'DO_NOTHING', 'Do not rebalance'],
	['Contact Client', 'CONTACT_CLIENT', 'Contact Client for instructions on whether or not to rebalance.'],
	['Rebalance', 'REBALANCE', 'Rebalance']
];

Clifton.investment.manager.ManagerTypes = [
	['Standard', 'STANDARD', 'Standard Manager'],
	['Proxy', 'PROXY', 'Proxy Manager'],
	['Rollup', 'ROLLUP', 'Rollup Manager'],
	['Linked', 'LINKED', 'Linked Manager']
];


Clifton.investment.manager.ManagerCashAdjustmentTypes = [
	['None', 'NONE', 'Do Not Change Cash Allocation'],
	['Cash', 'CASH', 'Allocate full manager account balance to cash (nothing to securities)'],
	['Securities', 'SECURITIES', 'Allocate full manager account balance to securities (nothing to cash)'],
	['Ignore Cash', 'IGNORE_CASH', 'Ignore cash balance but do not change securities allocation'],
	['Ignore Securities', 'IGNORE_SECURITIES', 'Ignore securities balance but do not change cash allocation'],
	['Custom Percentage', 'CUSTOM_PERCENT', 'Use specified custom cash allocation percentage (remainder to securities)'],
	['Custom Value', 'CUSTOM_VALUE', 'Use specified custom cash value (remainder to securities)'],
	['Manager', 'MANAGER', 'Use cash allocation percentage from specified manager']
];


Clifton.investment.manager.LinkedManagerTypes = [
	['Account Overlay Target', 'ACCOUNT_OVERLAY_TARGET', 'This manager\'s balance (Securities Only) comes from Replication Overlay Target (for securities that match selected asset class/security options'],
	['Account Synthetic Exposure', 'ACCOUNT_SYNTHETIC_EXPOSURE', 'This manager\'s balance (Securities Only) comes from Overlay Exposure (our overlay positions)'],
	['Accrued Interest For REPOs', 'ACCRUED_INTEREST_REPOS', 'This manager\'s balance (Cash Only) comes from the interest from REPOs for the selected Client Account and optionally holding account. Open vs. Term interest is calculated based on its type.'],
	['Cash Balance', 'CASH_MARKET_VALUE', 'This manager\'s balance (Cash Only) comes from the General Ledger cash balance held in the specified client account and broker account (optional).'],
	['General Ledger Balance', 'GENERAL_LEDGER_BALANCE', 'This manager\'s balance (Cash Only) comes from the General Ledger account balance held in the specified client account and broker account (optional) for a specific GL account/account type/account group. Note: Position accounting accounts will either return zero or a meaningless number.'],
	['Manager Balance (Proprietary Fund Portion)', 'MANAGER_BALANCE_PROPRIETARY_FUND_PORTION', 'Selected Manager\'s Cash and Securities Balance * Market Value of Fund Holdings in Client Account / Total Market Value of the Fund'],
	['Manager Market Value', 'MANAGER_MARKET_VALUE', 'This manager\'s balance (Cash + Securities) comes from Manager Allocations'],
	['Manager Cash Market Value', 'MANAGER_CASH_MARKET_VALUE', 'This manager\'s balance (Cash Only) comes from Manager Allocations'],
	['Manager Securities Market Value', 'MANAGER_SECURITIES_MARKET_VALUE', 'This manager\'s balance (Securities Only) comes from Manager Allocations'],
	['Option Positions Exposure', 'OPTION_POSITIONS_EXPOSURE', 'This manager\'s balance (Securities Only) comes from the exposure of our options positions held in the specified account.  Calculated as Contracts * Underlying Index Price * Delta * Option Price Multiplier.'],
	// REMOVING FROM SELECTABLE LIST - NO LONGER SUPPORTED - USE GENERAL LEDGER BALANCE TYPE ['Outstanding Receivables', 'OUTSTANDING_RECEIVABLES', 'This manager\'s balance (Cash Only) comes from the market value of the receivable positions held in the specified account.'],
	['Pending Event Journal Balance', 'PENDING_EVENT_JOURNAL_BALANCE', 'This manager\'s balance (Cash Only) comes from unbooked accounting event journals for the selected account/type/investment group options.'],
	['Pending General Ledger Balance', 'PENDING_GENERAL_LEDGER_BALANCE', 'The manager\'s balance (Cash Only) comes from the pending balance (Transaction Date <= Balance Date and Settlement Date > Balance Date) for the selected GL Account, Holding Account, Currency, etc.'],
	['Positions Market Value', 'POSITIONS_MARKET_VALUE', 'This manager\'s balance (Securities Only) comes from the market value of our positions held in the specified account. When the manager base CCY is the same as the account base CCY, base market value is used.  Otherwise if the position is denominated in the manager CCY, the local market value is used, otherwise the local value is converted to the manager base CCY.'],
	['Positions Market Value (Duration Adjusted)', 'POSITIONS_MARKET_VALUE_DURATION_ADJUSTED', 'This manager\'s balance (Securities Only) comes from the market value (duration adjusted) of our positions held in the specified account. Duration Adjustments apply the following calculation: market value * security duration / benchmark duration.'],
	['Positions Notional', 'POSITIONS_NOTIONAL', 'This manager\'s balance (Securities Only) comes from the notional value of our positions held in the specified account. When the manager base CCY is the same as the account base CCY, base notional is used.  Otherwise if the position is denominated in the manager CCY, the local notional is used, otherwise the local value is converted to the manager base CCY.'],
	['Positions Quantity (Unadjusted)', 'POSITIONS_QUANTITY_UNADJUSTED', 'This manager\'s balance (Securities Only) comes from the unadjusted quantity of our positions held in the specified account. Uses "Remaining Quantity" for all securities that do not have Factor Change Event. For securities with Factor Change Event (CDS and ABS), returns opening quantity (Original Notional or Face). Quantity can have a special meaning and you are required to enter the CCY that quantity is entered in. Example: CCY Forwards quantity is the amount in the underlying security of the CCY Forward. When the manager base CCY doesn\'t match the selected CCY, the amount is converted using current FX rate'],
	['Positions Replication Exposure', 'POSITIONS_REPLICATION_EXPOSURE', 'This manager\'s balance (Securities Only) comes from the calculated exposure value of our positions held in the specified account and asset class.  Can optionally limit exposure to specific positions within the asset class.  Useful for linking manager to run exposure in same account (prevents circular dependency). WARNING: Does not perform any contract splitting or include any virtual exposure. Recalculates exposure and does not pull values from the run (avoids circular dependency)'],
	['Security Gain/Loss', 'SECURITY_GAIN_LOSS', 'This manager\'s balance (Securities) comes from the cumulative Gain/Loss from matching positions for the selected account since the given start date.  Balances on that start date are always zero.'],
	['Total Market Value', 'TOTAL_MARKET_VALUE', 'This manager\'s balance (Cash & Securities) comes from the General Ledger cash balance (Cash) and Positions Market Value (Securities) held in the specified client account and broker account (optional).']
];


Clifton.investment.manager.ManagerM2MAdjustmentTypes = [
	['Gain/Loss (Futures Only)', 'GAIN_LOSS_FUTURES_ONLY', 'Includes Holiday M2M. M2M Adjustment is calculated from Accounting Positions Daily for Futures only. Because Foreign futures include currency M2M within it, calculation is performed off of local and uses FX Rate to get foreign futures without the currency M2M.	Calculation is: ((OTE Local + Realized Local - Previous OTE Local - Commission Local) * FX Rate) '],
	['Gain/Loss (Futures Only) - Exclude Holidays', 'GAIN_LOSS_FUTURES_ONLY_EXCLUDE_HOLIDAYS', 'Excludes Holiday M2M. M2M Adjustment is calculated from Accounting Positions Daily for Futures only. Because Foreign futures include currency M2M within it, calculation is performed off of local and uses FX Rate to get foreign futures without the currency M2M.	Calculation is: ((OTE Local + Realized Local - Previous OTE Local - Commission Local) * FX Rate) '],
	['Our M2M Amount', 'OUR_M2M', 'Includes Holiday M2M. Our M2M Calculated amount for selected date(s).'],
	['Our M2M Amount - Exclude Holidays', 'OUR_M2M_EXCLUDE_HOLIDAYS', 'Excludes Holiday M2M. Our M2M Calculated amount for selected date(s).'],
	['Transferred M2M Amount', 'TRANSFERRED_M2M', 'Includes Holiday M2M. Actual M2M Transfer amount for selected date(s).  Note: If M2M not reconciled yet, will use Our M2M amount.'],
	['Transferred M2M Amount - Exclude Holidays', 'TRANSFERRED_M2M_EXCLUDE_HOLIDAYS', 'Excludes Holiday M2M. Actual M2M Transfer amount for selected date(s).  Note: If M2M not reconciled yet, will use Our M2M amount.']
];

Clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes = [
	['Proportionally Using Rebalance Cash', 'PROPORTIONAL_REBALANCE_CASH', 'Allocates rebalance cash rebalancing proportionally using rebalance cash values'],
	['Proportionally Using Fund Cash Weights', 'PROPORTIONAL_FUND_WEIGHT', 'Allocates rebalance cash rebalancing proportionally using fund cash weights']
];


Clifton.investment.setup.DeliveryMonthCodes = [
	['January', 'F'],
	['February', 'G'],
	['March', 'H'],
	['April', 'J'],
	['May', 'K'],
	['June', 'M'],
	['July', 'N'],
	['August', 'Q'],
	['September', 'U'],
	['October', 'V'],
	['November', 'X'],
	['December', 'Z']
];


Clifton.investment.instrument.CurrentSecurityTargetAdjustmentTypes = [
	['DIFFERENCE_ALL', 'Difference - All', 'When a non-current security is held, it will follow its own exposure using the "Target Follows Actual" approach (TFA). The current security target will be the remaining difference between the allocation target and the non-current security exposure-regardless if we need to open/close positions or are over/underweight.'],
	['DIFFERENCE_OPENING', 'Difference - Opening', 'When a non-current security is held and we are overweight relative to the allocation target, its target will be adjusted to indicate necessary position closings by receiving the full allocation while the current security will be adjusted to a target of 0.  Alternatively, when a non-current security is held and we are underweight relative to the allocation target, its target will follow its own exposure using the "Target Follows Actual" approach (TFA). The current security target will be the remaining difference between the allocation target and the non-current security exposure, indicating necessary position openings.'],
	['ALL', 'All', 'When a non-current security is held, its target will always be set to 0 to indicate a closing trade. The current security target will always be 100% of the allocation target.']
];


Clifton.investment.specificity.DefinitionNames = {
	SECURITY_UI_OVERRIDE: 'Investment Security Columns',
	INSTRUMENT_UI_OVERRIDE: 'Investment Instrument Columns',
	SECURITY_VALIDATOR: 'Investment Security Validator',
	INSTRUMENT_VALIDATOR: 'Investment Instrument Validator'
};


// Because there are so many security types with different windows easier to manage default grid panel config options in one place
Clifton.investment.instrument.SecurityNoteGridPanel = Ext.extend(Clifton.system.note.NoteGridPanel, {
	tableName: 'InvestmentSecurity',
	showAttachmentInfo: true,
	showInternalInfo: false,
	showPrivateInfo: false,
	defaultActiveFilter: false,
	showDisplayFilter: false,
	// Set to true to see notes where used as related entity - i.e. on security also see all trade notes for that security
	includeNotesAsLinkedEntity: true
});
Ext.reg('investment-security-system-note-grid', Clifton.investment.instrument.SecurityNoteGridPanel);


Clifton.investment.instrument.SecurityFormWithDynamicFields = Ext.extend(Clifton.system.schema.FormPanelWithCustomFields, {
	url: 'investmentSecurity.json',
	columnGroupName: 'Security Custom Fields',
	labelFieldName: 'symbol',
	labelWidth: 140,
	getInfoWindowAuditTrailLoadParams: function(fp) {
		if (TCG.isTrue(fp.getFormValue('instrument.hierarchy.oneSecurityPerInstrument'))) {
			// include both security and instrument fields in audit trail
			return {
				tableName: 'InvestmentInstrument',
				childTables: 'InvestmentSecurity',
				entityId: fp.getFormValue('instrument.id')
			};
		}
		return {
			tableName: 'InvestmentSecurity',
			entityId: fp.getFormValue('id')
		};
	},
	listeners: {
		afterrender: async function(panel) {
			const f = panel.getForm();
			const w = panel.getWindow();
			const hierarchy = TCG.getValue('instrument.hierarchy', f.formValues);
			// Display Price Multiplier Only If Hierarchy Isn't Set, or Hierarchy Allows Overrides
			if (TCG.getValue('oneSecurityPerInstrument', hierarchy) === true) {
				const priceMultiplierField = f.findField('instrument.priceMultiplier');
				const showPriceMultiplier = (TCG.getValue('securityPriceMultiplierOverrideAllowed', hierarchy) || TCG.isBlank(TCG.getValue('priceMultiplier', hierarchy)));
				if (priceMultiplierField) {
					priceMultiplierField.setVisible(showPriceMultiplier);
				}
			}

			if (TCG.getValue('accrualSecurityEventType', hierarchy) || TCG.getValue('instrument.hierarchy.accrualSecurityEventType', w.defaultData)) {
				// Put the Accrual Method field before Settlement Currency
				const index = panel.items.findIndexBy(item => item.name === 'settlementCurrency.label');
				const defaultDataHierarchy = TCG.getValue('instrument.hierarchy', w.defaultData);

				const addFieldWithFormValueFunction = componentConfig => {
					const component = panel.insert(index, componentConfig);

					const formValue = (TCG.getValue('accrualSecurityEventType', hierarchy)) ? f.formValues[component.name] : TCG.getValue(component.name, defaultDataHierarchy);
					component.setValue(formValue);
				};
				// Inserting accrualMethod2 first, because inserting both at 'index' will display accrualMethod before accrualMethod2
				addFieldWithFormValueFunction({fieldLabel: 'Accrual Method 2', name: 'accrualMethod2', hiddenName: 'accrualMethod2', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', store: {xtype: 'arraystore', fields: ['value', 'name', 'description'], data: Clifton.investment.instrument.AccrualMethods}});
				addFieldWithFormValueFunction({fieldLabel: 'Accrual Method', name: 'accrualMethod', hiddenName: 'accrualMethod', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', store: {xtype: 'arraystore', fields: ['value', 'name', 'description'], data: Clifton.investment.instrument.AccrualMethods}});
			}

			await panel.processAfterRenderFieldVisibility(panel);
			Clifton.investment.specificity.SpecificityHandler.applySpecificityOverrides(this, Clifton.investment.specificity.SpecificityHandler.specificitySecurity);
		}
	},

	processAfterRenderFieldVisibility: function(formPanel) {
		const f = formPanel.getForm();

		//Hide FIGI field for OTC securities
		const fld = f.findField('figi');
		if (fld) {
			const isOtc = TCG.getValue('instrument.hierarchy.otc', f.formValues);
			fld.setVisible(isOtc !== true && fld.hidden === false);
		}
	}
});
Ext.reg('investment-security-forms-with-dynamic-fields', Clifton.investment.instrument.SecurityFormWithDynamicFields);


Clifton.investment.instrument.InstrumentFormWithDynamicFields = Ext.extend(Clifton.system.schema.FormPanelWithCustomJsonFields, {
	url: 'investmentInstrument.json',
	columnGroupName: 'Instrument Custom Fields',
	listeners: {
		afterrender: function(panel) {
			// Check/Uncheck, Disable/Enable Deliverable field based on hierarchy deliverable type
			const f = panel.getForm();
			if (TCG.getValue('hierarchy.investmentTypeSubType2.name', f.formValues) === 'Centrally Cleared Swaps') {
				this.setFieldLabel('exchange.label', 'CCP');
				f.findField('exchange.label').allowBlank = false;
				this.hideField('compositeExchange.label');
			}
			const dt = TCG.getValue('hierarchy.instrumentDeliverableType', f.formValues);
			if (dt === 'ALWAYS' || dt === 'NEVER') {
				const field = f.findField('deliverable');
				if (field) {
					field.setValue(dt === 'ALWAYS');
					field.setDisabled(true);
					if (dt === 'NEVER') {
						field.setVisible(false);
					}
				}
			}
			Clifton.investment.specificity.SpecificityHandler.applySpecificityOverrides(this, Clifton.investment.specificity.SpecificityHandler.specificityInstrument);
		}
	}
});
Ext.reg('investment-instrument-forms-with-dynamic-fields', Clifton.investment.instrument.InstrumentFormWithDynamicFields);


/**
 * The identifier prefix field for instrument windows. This field includes a split button to provide additional actions for the instrument, such as viewing
 * column overrides from investment specificity entries.
 */
Clifton.investment.instrument.IdentifierPrefix = Ext.extend(Ext.form.CompositeField, {
	fieldLabel: 'Identifier Prefix',
	items: [{
		name: 'identifierPrefix',
		xtype: 'textfield',
		flex: 1
	}, {
		xtype: 'splitbutton', iconCls: 'info', width: 40,
		tooltip: 'Click to view Investment Specificity entries applicable for this instrument.',
		handler: function(btn) {
			btn.findParentByType('compositefield').openFieldMappingInformation();
		},
		menu: {
			xtype: 'menu',
			items: [{
				text: 'Show Applicable Investment Specificity Entries',
				iconCls: 'grouping',
				tooltip: 'Click to view Investment Specificity entries applicable for this instrument.',
				handler: function(btn) {
					btn.findParentByType('compositefield').openFieldMappingInformation();
				}
			}]
		}
	}],

	/**
	 * Opens the field mappings dialog for the current instrument.
	 */
	openFieldMappingInformation: function() {
		const parentForm = TCG.getParentFormPanel(this);
		const entity = parentForm.getWindow().getMainForm().formValues;
		const className = 'Clifton.investment.specificity.InvestmentSpecificityEntryListWindow';

		// Guard-clause
		if (!entity.id) {
			TCG.showError('Please save changes first. The instrument does not yet exist.');
			return;
		}

		TCG.createComponent(className, {
			id: TCG.getComponentId(className, entity.id + ' (Instrument)'),
			title: entity.label,
			instrumentScope: entity,
			showSearchPatternFilter: false,
			showSecurityFilter: false,
			openerCt: parentForm
		});
	}
});
Ext.reg('investment-instrument-identifier-prefix', Clifton.investment.instrument.IdentifierPrefix);


Clifton.investment.instrument.SecurityTicker = Ext.extend(Ext.form.CompositeField, {
	fieldLabel: 'Ticker Symbol',
	hierarchyFormFieldValue: 'instrument.hierarchy.id',
	listeners: {
		afterrender: function(panel) {
			panel.setupSymbolField();
		}
	},
	setupSymbolField: function() {
		const fp = this.getParentForm();
		const parent = this;

		const cs = this.innerCt.items.items;
		let i = 0;
		const len = cs.length;
		for (; i < len; i++) {
			const fld = cs[i];
			fld.parentField = parent;
			if (fld.menu) {
				const csMenu = fld.menu.items.items;
				for (let j = 0; j < csMenu.length; j++) {
					csMenu[j].parentField = parent;
				}
			}
		}

		parent.symbolField = fp.getForm().findField('symbol');
		fp.focusOnFirstField = function() {
			// set focus to first non read-only field
			const field = fp.getForm().findField('symbol');
			field.focus(false, 500);
		};
	},
	items: [
		{name: 'symbol', xtype: 'textfield', flex: 1, height: 22},
		{
			xtype: 'splitbutton', name: 'securityTickerDataButton', iconCls: 'bloomberg', width: 40, tooltip: 'Click to load values from Bloomberg for this Ticker Symbol',
			handler: function(btn) {
				return btn.parentField.lookupSecurityData();
			},
			menu: {
				xtype: 'menu',
				items: [{
					text: 'Show Field Mappings',
					iconCls: 'bloomberg',
					handler: function(btn) {
						return btn.parentField.openSecurityDataPreview('Clifton.marketdata.security.InvestmentSecurityFieldMappingWindow');
					}
				}, {
					text: 'Show New Values',
					iconCls: 'bloomberg',
					handler: function(btn) {
						return btn.parentField.openSecurityDataPreview('Clifton.marketdata.security.InvestmentSecurityUpdaterPreviewWindow');
					}
				}]
			}
		},
		{
			xtype: 'splitbutton', iconCls: 'info', width: 40, tooltip: 'Click to view Instrument Field Mappings and Instrument Price Field Mappings (Market Data Setup)',
			handler: function(btn) {
				btn.parentField.openInstrumentMappingPreview();
			},
			menu: {
				xtype: 'menu',
				items: [{
					/* TODO: Remove or replace me at some point. This is currently a duplicate of the primary button functionality for clarity's sake. */
					text: 'Show Instrument and Price Field Mappings',
					iconCls: 'info',
					tooltip: 'Click to view Instrument Field Mappings and Instrument Price Field Mappings (Market Data Setup)',
					handler: function(btn) {
						btn.parentField.openInstrumentMappingPreview();
					}
				}, {
					text: 'Show Applicable Investment Specificity Entries',
					iconCls: 'grouping',
					tooltip: 'Click to view Investment Specificity entries applicable for this security.',
					handler: function(btn) {
						btn.parentField.openFieldMappingInformation();
					}
				}]
			}
		}
	],
	getParentForm: function() {
		const parent = TCG.getParentFormPanel(this);
		return parent || this.ownerCt;
	},
	openInstrumentMappingPreview: function() {
		const fp = this.getParentForm();
		const instrumentId = fp.getFormValue('instrument.id');
		if (!instrumentId) {
			TCG.showError('Please save changes first. Instrument does not exist yet');
			return;
		}
		const className = 'Clifton.marketdata.field.InstrumentDataFieldMappingListWindow';
		const cmpId = TCG.getComponentId(className, instrumentId);
		TCG.createComponent(className, {
			id: cmpId,
			defaultData: {instrumentId: instrumentId, instrumentLabel: fp.getFormValue('instrument.label')},
			openerCt: fp
		});
	},
	getTicker: function() {
		return this.symbolField.getValue();
	},
	openSecurityDataPreview: function(className) {
		const ticker = this.getTicker();
		if (ticker && ticker !== '') {
			const fp = this.getParentForm();
			const win = fp.getWindow();
			const params = {
				symbol: ticker,
				investmentSecurityId: win.getMainFormId(),
				instrumentHierarchyId: fp.getFormValue('instrument.hierarchy.id'),
				instrumentId: fp.getFormValue('instrument.id'),
				label: ticker
			};
			const cmpId = TCG.getComponentId(className, ticker);
			TCG.createComponent(className, {
				id: cmpId,
				defaultData: params,
				params: params,
				openerCt: fp,
				defaultIconCls: fp.getWindow().iconCls
			});
		}
		else {
			TCG.showInfo('Please enter a bloomberg ticker.', 'Bloomberg Lookup');
		}
	},
	lookupSecurityData: function() {
		const ticker = this.getTicker();
		if (ticker && ticker !== '') {
			const fp = this.getParentForm();
			const win = fp.getWindow();
			const params = {
				symbol: ticker,
				investmentSecurityId: win.getMainFormId(),
				instrumentHierarchyId: fp.getFormValue('instrument.hierarchy.id'),
				instrumentId: fp.getFormValue('instrument.id')
			};
			const loader = new TCG.data.JsonLoader({
				params: params,
				waitTarget: fp,
				waitMsg: 'Loading fields...',
				onLoad: function(record, conf) {
					const error = record.error;
					const rec = record.newSecurity;
					try {
						fp.getForm().trackResetOnLoad = false;
						fp.getForm().setValues(rec);
						if (rec.columnValueList && rec.columnValueList.length > 0) {
							fp.fieldValueList = rec.columnValueList;
							fp.setDynamicFieldsValues();
						}
					}
					finally {
						fp.getForm().trackResetOnLoad = true;
					}
					if (error) {
						TCG.showError(error, 'Investment Security Lookup');
					}
				}
			});
			loader.load('marketDataInvestmentSecurity.json');
		}
		else {
			TCG.showInfo('Please enter a bloomberg ticker.', 'Bloomberg Lookup');
		}
	},
	/**
	 * Opens the field mappings dialog for the current security.
	 */
	openFieldMappingInformation: function() {
		const parentForm = this.getParentForm();
		const entity = parentForm.getWindow().getMainForm().formValues;
		const className = 'Clifton.investment.specificity.InvestmentSpecificityEntryListWindow';

		// Guard-clause
		if (!entity.id) {
			TCG.showError('Please save changes first. The security does not yet exist.');
			return;
		}

		TCG.createComponent(className, {
			id: TCG.getComponentId(className, entity.id + ' (Security)'),
			title: entity.label,
			securityScope: entity,
			showSearchPatternFilter: false,
			showInstrumentFilter: false,
			openerCt: parentForm
		});
	}
});
Ext.reg('investment-instrument-ticker', Clifton.investment.instrument.SecurityTicker);


Clifton.investment.instrument.UnderlyingInstrumentComboBox = Ext.extend(TCG.form.ComboBox, {
	// name: SHOULD BE 'underlyingInstrument.labelShort' or 'instrument.underlyingInstrument.labelShort' depending on if it's really the instrument window (1:Many) or security window (1:1)
	// hiddenName:
	fieldLabel: 'Underlying Instrument',
	displayField: 'labelShort',
	url: 'investmentInstrumentListFind.json',
	detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
	beforequery: function(queryEvent) {
		const uh = this.getUnderlyingHierarchy();
		const group = this.getUnderlyingInvestmentGroup();

		if ((uh && uh.id) || (group && group.id)) {
			const params = {};
			if (uh && uh.id) {
				params.hierarchyId = uh.id;
			}
			if (group && group.id) {
				params.investmentGroupId = group.id;
			}
			queryEvent.combo.store.baseParams = params;
		}
	},
	reload: function(win) {
		// populate value/text from the window passed (not in store)
		let v = win.getMainFormId();
		let text = win.getMainFormPanel().getFormLabel();
		// If window was actually for the security (1:1) then need to lookup the instrument in the formpanel
		if (TCG.isEquals(win.getMainFormPanel().url, 'investmentSecurity.json')) {
			v = TCG.getValue('instrument.id', win.getMainFormPanel().getForm().formValues);
			text = TCG.getValue('instrument.labelShort', win.getMainFormPanel().getForm().formValues);
		}
		this.clearValue();
		this.store.removeAll();
		this.lastQuery = null;
		// code block from setValue
		this.lastSelectionText = text;
		if (this.hiddenField) {
			this.hiddenField.value = v;
		}
		Ext.form.ComboBox.superclass.setValue.call(this, text);
		this.value = v;

		const fp = TCG.getParentFormPanel(this);
		fp.form.formValues[this.hiddenName] = {value: v, text: text};
	},
	getUnderlyingHierarchy: function() {
		const hierarchy = this.getHierarchy();
		if (hierarchy) {
			return hierarchy.underlyingHierarchy;
		}
	},
	getUnderlyingInvestmentGroup: function() {
		const hierarchy = this.getHierarchy();
		if (hierarchy) {
			return hierarchy.underlyingInvestmentGroup;
		}
	},
	getHierarchy: function() {
		let fp = TCG.getParentFormPanel(this);
		fp = fp.getForm();
		let hId = '';
		if (TCG.isEquals(this.hiddenName, 'instrument.underlyingInstrument.id')) {
			hId = TCG.getValue('instrument.hierarchy.id', fp.formValues);
		}
		if (TCG.isBlank(hId)) {
			hId = TCG.getValue('hierarchy.id', fp.formValues);
		}
		if (TCG.isNotBlank(hId)) {
			return TCG.data.getData('investmentInstrumentHierarchy.json?id=' + hId, this, 'investment.instrument.hierarchy.' + hId);
		}
		return undefined;
	},
	getDefaultData: function(f, newInstance) { // defaults underlying hierarchy for new only
		if (newInstance) {
			const uh = this.getUnderlyingHierarchy();
			if (!uh) {
				TCG.showError('Unable to create underlying instrument because it is not know what hierarchy it belongs to.  Please create the underlying instrument from the main Investments window.');
				return false;
			}
			return {
				hierarchy: uh
			};
		}
	}
});
Ext.reg('investment-instrument-underlying-combo', Clifton.investment.instrument.UnderlyingInstrumentComboBox);


Clifton.investment.instrument.OTCSecurityFormWithDynamicFields = Ext.extend(Clifton.investment.instrument.SecurityFormWithDynamicFields, {
	initComponent: function() {
		const currentItems = [];
		if (this.itemsBefore) { // Used in OTC project for copy ability that shows copy information first
			Ext.each(this.itemsBefore, function(f) {
				currentItems.push(f);
			});
		}
		if (this.isdaItems) {
			Ext.each(this.isdaItems, function(f) {
				currentItems.push(f);
			});
		}
		Ext.each(this.securityItems, function(f) {
			currentItems.push(f);
		});

		const dd = this.getWindow().defaultData;
		const hierarchy = TCG.getValue('instrument.hierarchy', dd);
		const investmentType = TCG.getValue('investmentType.name', hierarchy);
		this.getWindow().titlePrefix = investmentType.substring(0, investmentType.length - 1);
		if (investmentType === 'Options' || investmentType === 'Swaptions') {
			currentItems.push({
				fieldLabel: 'Expiration Date', name: 'lastDeliveryDate', xtype: 'datefield', allowBlank: false,
				listeners: {
					change: function(field, newValue, oldValue) {
						TCG.getParentFormPanel(field).setFormValue('firstDeliveryDate', newValue);
					},
					select: function(field, newValue) {
						TCG.getParentFormPanel(field).setFormValue('firstDeliveryDate', newValue);
					}
				}
			});
			// option specific fields -- if we have an option type, display option-specific fields
			if (investmentType === 'Options') {
				currentItems.push({fieldLabel: 'Put or Call', name: 'optionType', hiddenName: 'optionType', valueField: 'value', displayField: 'value', tooltipField: 'tooltip', xtype: 'system-list-combo', listName: 'Investment Option Type'});
				currentItems.push({fieldLabel: 'Strike Price', name: 'optionStrikePrice', xtype: 'pricefield', qtip: 'The option exercise price.'});
				currentItems.push({fieldLabel: 'Option Style', name: 'optionStyle', hiddenName: 'optionStyle', valueField: 'value', displayField: 'value', tooltipField: 'tooltip', xtype: 'system-list-combo', listName: 'Investment Option Style'});
				currentItems.push({fieldLabel: 'Expiry Time', name: 'settlementExpiryTime', hiddenName: 'settlementExpiryTime', valueField: 'value', displayField: 'value', tooltipField: 'tooltip', xtype: 'system-list-combo', listName: 'Investment Expiry Time'});
			}
			currentItems.push({name: 'firstDeliveryDate', xtype: 'datefield', hidden: true});
		}
		currentItems.push({boxLabel: 'Illiquid (cannot be easily sold or exchanged for cash)', name: 'illiquid', xtype: 'checkbox'});
		if (TCG.getValue('oneSecurityPerInstrument', hierarchy) === true && TCG.getValue('instrumentDeliverableType', hierarchy) === 'BOTH') {
			currentItems.push({fieldLabel: 'Deliverable', name: 'instrument.deliverable', xtype: 'checkbox', boxLabel: ' (Uncheck if it is Cash Settled)'});
		}
		currentItems.push({xtype: 'label', html: '<hr/>'});

		// underlying security
		if (TCG.getValue('instrument.underlyingInstrument.hierarchy.oneSecurityPerInstrument', dd) === false) {
			const underlyingId = TCG.getValue('instrument.underlyingInstrument.id', dd);
			currentItems.push({
				fieldLabel: 'Underlying Security', name: 'underlyingSecurity.label', hiddenName: 'underlyingSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: false,
				beforequery: function(queryEvent) {
					queryEvent.combo.store.baseParams = {instrumentId: underlyingId};
				}
			});
		}
		else {
			currentItems.push({fieldLabel: 'Underlying Security', name: 'underlyingSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'underlyingSecurity.id'});
		}

		// optional reference security
		if (TCG.isTrue(TCG.getValue('referenceSecurityAllowed', hierarchy))) {
			const investmentTypeSubType2 = TCG.getValue('investmentTypeSubType2.name', hierarchy);
			currentItems.push({
				fieldLabel: TCG.getValue('referenceSecurityLabel', hierarchy), name: 'referenceSecurity.label', hiddenName: 'referenceSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: true,
				qtip: TCG.getValue('referenceSecurityTooltip', hierarchy),
				beforequery: function(queryEvent) {
					const store = queryEvent.combo.store;
					store.baseParams = {};

					const formValues = TCG.getParentFormPanel(queryEvent.combo).getForm().formValues,
						active = TCG.getValue('active', formValues);
					if (TCG.isNotBlank(active)) {
						store.baseParams['active'] = active;
					}

					if (TCG.getValue('referenceSecurityFromSameInstrument', hierarchy)) {
						store.baseParams['instrumentId'] = TCG.getValue('instrument.id', formValues);
					}
					else {
						const underlyingSecurityId = TCG.getValue('underlyingSecurity.id', formValues);
						if (TCG.isNotNull(underlyingSecurityId)) {
							store.baseParams['underlyingSecurityId'] = underlyingSecurityId;
						}
						// If the subtype2 is flex or OTC Options filter for listed options. The result of match(regex) is null if no match.
						// ^ signals beginning of input, $ signals end of input, and the three options are or'd with |
						if (investmentTypeSubType2.match(/^(FLEX|Look Alike|Custom) Options$/)) {
							queryEvent.combo.store.baseParams['investmentTypeSubType2'] = 'Listed Options';
						}
					}
				}
			});
		}

		// optional settlement currency
		if (TCG.isTrue(TCG.getValue('differentSettlementCurrencyAllowed', hierarchy))) {
			currentItems.push({
				fieldLabel: 'Settlement CCY', name: 'settlementCurrency.label', hiddenName: 'settlementCurrency.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: true,
				qtip: 'In rare cases of OTC securities, Settlement Currency for events and trades maybe different than Currency Denomination of security. Use this field to specify the default Settlement Currency. It can still be manually overridden for individual transactions.'
			});
		}

		this.items = currentItems;
		Clifton.investment.instrument.OTCSecurityFormWithDynamicFields.superclass.initComponent.apply(this, arguments);
	},
	itemsBefore: undefined,
	securityItems: [
		{fieldLabel: 'Investment Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
		{fieldLabel: 'Investment Instrument', name: 'instrument.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'instrument.id'},
		{xtype: 'investment-instrument-underlying-combo', name: 'instrument.underlyingInstrument.labelShort', hiddenName: 'instrument.underlyingInstrument.id', hidden: true},
		{xtype: 'investment-instrument-ticker'},
		{fieldLabel: 'Internal Identifier', name: 'cusip', readOnly: true, qtip: 'Unique internal identifier generated by our system for this OTC security. It follows reserved CUSIP range format.'},
		{fieldLabel: 'Security Name', name: 'name'},
		{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 40},
		{fieldLabel: 'Country of Risk', name: 'instrument.countryOfRisk.text', hiddenName: 'instrument.countryOfRisk.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries', hidden: true},
		{fieldLabel: 'Country of Incorporation', name: 'instrument.countryOfIncorporation.text', hiddenName: 'instrument.countryOfIncorporation.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries', hidden: true},
		{fieldLabel: 'CCY Denomination', name: 'instrument.tradingCurrency.name', hiddenName: 'instrument.tradingCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', allowBlank: false, hidden: true},
		{fieldLabel: 'Price Multiplier', name: 'instrument.priceMultiplier', xtype: 'floatfield', allowBlank: false, value: 1, hidden: true},
		{fieldLabel: 'First Trade', name: 'startDate', xtype: 'datefield', allowBlank: false},
		{fieldLabel: 'Last Trade', name: 'endDate', xtype: 'datefield', allowBlank: false}
	],
	prepareDefaultData: function(defaultData) {
		const f = this.getForm();
		if (TCG.getValue('instrument.hierarchy.oneSecurityPerInstrument', defaultData) === true) {
			// do not submit instrument: it's not known and setting it can clear hierarchy
			f.findField('instrument.id').submitValue = false;
			f.findField('instrument.name').setVisible(false);
			f.findField('instrument.underlyingInstrument.labelShort').setVisible(true);
			f.findField('instrument.countryOfRisk.text').setVisible(true);
			f.findField('instrument.tradingCurrency.name').setVisible(true);
		}
		return defaultData;
	},
	defaultOtcNameAndSymbolFieldValue: function() {
		//Prepopulate name field
		const fp = this;
		if (fp && fp.defaultOtcName) {
			const prefix = fp.getFormValue('instrument.identifierPrefix');
			const counterPartyAbv = fp.getFormValue('businessCompany.abbreviation', true);
			const clientAbv = fp.getFormValue('businessContract.company.abbreviation', true);
			if (TCG.isNotBlank(prefix) && TCG.isNotBlank(counterPartyAbv) && TCG.isNotBlank(clientAbv)) {
				const namePrePop = prefix + '-' + counterPartyAbv + '-' + clientAbv;
				fp.setFormValue('name', namePrePop, true);
				fp.setFormValue('symbol', 'TBD-' + namePrePop, true);
			}
		}
		const symbolField = fp.getForm().findField('symbol');
		if (symbolField) {
			const currentValue = symbolField.getValue();
			let newValue = 'TBD-';
			if (TCG.isTrue(fp.defaultOtcName)) {
				newValue += fp.getFormValue('name', true);
			}
			if (TCG.isBlank(currentValue) || currentValue !== newValue) {
				symbolField.setValue(newValue);
			}
		}
	},
	isdaItems: [
		{xtype: 'hidden', name: 'businessContract.company.abbreviation', submitValue: false},
		{
			fieldLabel: 'Client', name: 'businessContract.company.name', hiddenName: 'businessContract.company.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow', url: 'businessCompanyListFind.json?companyType=Client', submitValue: false, disableAddNewItem: true, allowBlank: false,
			requestedProps: 'abbreviation',
			listeners: {
				select: function(combo, record, index) {
					const fp = combo.getParentForm();
					fp.setFormValue('businessContract.company.abbreviation', TCG.getValue('abbreviation', record.json));
					//Prepopulate name field and symbol field
					fp.defaultOtcNameAndSymbolFieldValue();
				}
			}
		},
		{xtype: 'hidden', name: 'businessCompany.abbreviation', submitValue: false},
		{
			fieldLabel: 'Counterparty', name: 'businessCompany.name', hiddenName: 'businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourCompany=false', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true,
			allowBlank: false,
			requiredFields: ['businessContract.company.id'],
			beforequery: function(queryEvent) {
				const fp = queryEvent.combo.getParentForm();
				// Client Company
				const clientCompanyId = fp.getFormValue('businessContract.company.id', true);
				const params = {contractCompanyId: clientCompanyId};

				// ISDA Contract Type
				params.contractTypeNames = ['ISDA', 'ISDA Amendment'];

				// Counterparty Role
				params.contractPartyRoleName = 'Counterparty';

				queryEvent.combo.store.baseParams = params;
			},
			listeners: {
				select: function(combo, record, index) {
					// On Select - Attempt to Auto Load the correct ISDA
					const fp = combo.getParentForm();

					fp.setFormValue('businessCompany.abbreviation', TCG.getValue('abbreviation', record.json));

					// Client Company
					const clientCompanyId = fp.getFormValue('businessContract.company.id', true);
					const params = {companyId: clientCompanyId, active: true};

					// ISDA Contract Type
					params.contractTypeNames = ['ISDA', 'ISDA Amendment'];

					// Counterparty Company
					params.partyCompanyId = record.json.id;

					// Counterparty Role
					const role = TCG.data.getData('businessContractPartyRoleByName.json?name=Counterparty', combo, 'business.contract.party.role.Counterparty');
					params.partyRoleId = role.id;

					const loader = new TCG.data.JsonLoader({
						params: params,
						onLoad: function(record, conf) {
							const l = record.length || 0;
							if (l === 0) {
								TCG.showError('Cannot find any ISDA Contracts that match selected Client and Counterparty.', 'Invalid Setup');
							}
							else if (l === 1) {
								const isda = record[0];
								fp.setFormValue('businessContract.id', {value: isda.id, text: isda.label}, true);
							}
							else {
								const isdaContracts = record ? record.filter(i => i.contractType.name === 'ISDA') : [];
								if (isdaContracts.length === 1) {
									const isda = isdaContracts[0];
									fp.setFormValue('businessContract.id', {value: isda.id, text: isda.label}, true);
								}
							}
						}
					});
					loader.load('businessContractListFind.json', fp);

					//Prepopulate name field and symbol field
					fp.defaultOtcNameAndSymbolFieldValue();
				}
			}
		},
		{
			fieldLabel: 'ISDA', name: 'businessContract.label', hiddenName: 'businessContract.id', xtype: 'combo', url: 'businessContractListFind.json', displayField: 'label', tooltipField: 'label', detailPageClass: 'Clifton.business.contract.ContractWindow', disableAddNewItem: true,
			allowBlank: false,
			requiredFields: ['businessCompany.id'],
			beforequery: function(queryEvent) {
				const fp = queryEvent.combo.getParentForm();
				// Client Company
				const clientCompanyId = fp.getFormValue('businessContract.company.id', true);
				const params = {companyId: clientCompanyId, active: true};

				// ISDA Contract Types
				params.contractTypeNames = ['ISDA', 'ISDA Amendment'];

				// Counterparty Company
				params.partyCompanyId = fp.getFormValue('businessCompany.id', true);

				// Counterparty Role
				const role = TCG.data.getData('businessContractPartyRoleByName.json?name=Counterparty', queryEvent.combo, 'business.contract.party.role.Counterparty');
				params.partyRoleId = role.id;

				queryEvent.combo.store.baseParams = params;
			},
			listeners: {
				select: function(combo, record, index) {
					const fp = combo.getParentForm();
					fp.setFormValue('businessContract.company.abbreviation', TCG.getValue('company.abbreviation', record.json));
					//Prepopulate name field and symbol field
					fp.defaultOtcNameAndSymbolFieldValue();
				}
			}
		},
		{xtype: 'label', html: '<hr/>'}
	]
});
Ext.reg('investment-security-otc-forms-with-dynamic-fields', Clifton.investment.instrument.OTCSecurityFormWithDynamicFields);


Clifton.investment.instrument.getTradeDatePromise = function(securityId, returnString, componentScope) {
	return new Promise(function(resolveCallback) {
		if (!securityId) {
			resolveCallback(null);
		}
		else {
			TCG.data.getDataValue('investmentSecurityTradeDate.json?securityId=' + securityId, componentScope, function(result) {
				const parsedResult = TCG.parseDate(result);
				const today = new Date().clearTime();
				if (parsedResult.clearTime() > today) {
					/*
									 * The Security Trade Date service uses the Security's Exchange to lookup the Trade Date. If the result is greater
									 * than today, an exchange was found to be closed. Get the security so we have all the information we need for the message.
									 */
					TCG.data.getDataPromise('investmentSecurity.json?id=' + securityId, componentScope, 'investment.security.id.' + securityId)
						.then(function(sec) {
							const exchange = sec.instrument.exchange ? sec.instrument.exchange : sec.instrument.hierarchy.defaultExchange;
							const exchangeMessage = exchange ? 'Investment Exchange: [' + exchange.name + '] is currently closed.  ' : '';
							TCG.showInfo(exchangeMessage + 'Trade Date for Security: [' + sec.symbol + '] will be moved forward to the next available Trade Date of [' + parsedResult.format('m/d/Y') + '].', 'Trade Date');
							resolveCallback(returnString ? parsedResult.format('m/d/Y') : parsedResult);
						});
				}
				else {
					resolveCallback(returnString ? parsedResult.format('m/d/Y') : parsedResult);
				}
			});
		}
	});
};


// settlementCurrencyId is needed for physical currencies that have settlement convention
Clifton.investment.instrument.getSettlementDatePromise = function(securityId, transactionDate, returnString, settlementCurrencyId, componentScope) {
	return new Promise(function(resolveCallback) {
		if (!securityId) {
			resolveCallback(null);
		}
		else {
			let dateStr = transactionDate;
			if (typeof dateStr !== 'string') {
				dateStr = dateStr.format('m/d/Y');
			}
			TCG.data.getDataValue('investmentSecuritySettlementDate.json?securityId=' + securityId + '&transactionDate=' + dateStr + (settlementCurrencyId ? '&settlementCurrencyId=' + settlementCurrencyId : ''), componentScope, function(result) {
				const parsedResult = TCG.parseDate(result);
				resolveCallback(returnString ? parsedResult.format('m/d/Y') : parsedResult);
			});
		}
	});
};


Clifton.investment.instrument.InstrumentSecurityGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentSecurityListFind',
	xtype: 'gridpanel',
	instructions: 'The following securities are associated with this investment instrument.',
	additionalPropertiesToRequest: 'id|priceMultiplierOverride|instrument.priceMultiplier',
	columns: [
		{header: 'ID', width: 50, dataIndex: 'id', hidden: true},
		{header: 'Investment Type', width: 60, dataIndex: 'instrument.hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}, hidden: true},
		{header: 'Sub Type', width: 60, dataIndex: 'instrument.hierarchy.investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}, hidden: true},
		{header: 'Hierarchy', width: 50, dataIndex: 'instrument.hierarchy.labelExpanded', hidden: true},
		{header: 'Instrument', width: 50, dataIndex: 'instrument.name', hidden: true},
		{header: 'Symbol', width: 50, dataIndex: 'symbol'},
		{header: 'CUSIP', width: 50, dataIndex: 'cusip', hidden: true},
		{header: 'ISIN', width: 50, dataIndex: 'isin', hidden: true},
		{header: 'SEDOL', width: 50, dataIndex: 'sedol', hidden: true},
		{
			header: 'FIGI', width: 50, dataIndex: 'figi', hidden: true,
			tooltip: 'Financial Instrument Global Identifier (FIGI; also known as the Bloomberg Global Identifier (BBGID)) is an open standard, 12 character unique identifier assigned to all financial instruments and will not change once issued. For equity instruments, an identifier is issued per trading venue.'
		},
		{header: 'OCC Symbol', width: 50, dataIndex: 'occSymbol', hidden: true, tooltip: 'OCC Symbol is a unique code used to identify Options on a futures exchange.  Options Clearing Corporation\'s (OCC) Options Symbology Initiative (OSI) mandated an industry-wide change to this methodology in 2010. We use it to reconcile and confirm trades with external parties.'},
		{header: 'Security Name', width: 100, dataIndex: 'name'},
		{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
		{header: 'Currency', width: 50, dataIndex: 'currency', type: 'boolean', hidden: true},
		{header: 'Primary Exchange', width: 100, dataIndex: 'instrument.exchange.label', filter: {type: 'combo', searchFieldName: 'exchangeId', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=false'}, hidden: true},
		{header: 'Composite Exchange', width: 100, dataIndex: 'instrument.compositeExchange.label', filter: {type: 'combo', searchFieldName: 'compositeExchangeId', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=true'}, hidden: true},
		{header: 'Country of Risk', width: 100, dataIndex: 'instrument.countryOfRisk.text', filter: {type: 'combo', searchFieldName: 'countryOfRiskId', displayField: 'text', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Countries'}, hidden: true},
		{header: 'CCY Denomination', width: 60, dataIndex: 'instrument.tradingCurrency.name', filter: {type: 'combo', searchFieldName: 'tradingCurrencyId', displayField: 'name', url: 'investmentSecurityListFind.json?currency=true'}, hidden: true},
		{header: 'Counterparty or Issuer', width: 70, dataIndex: 'businessCompany.name', filter: {type: 'combo', searchFieldName: 'businessCompanyId', displayField: 'name', url: 'businessCompanyListFind.json?issuer=true'}, tooltip: 'Issuer or Counterparty for this security.'},
		{header: 'Client', width: 70, dataIndex: 'businessContract.company.name', filter: {type: 'combo', searchFieldName: 'clientId', displayField: 'name', url: 'businessCompanyListFind.json?companyType=Client'}},
		{header: 'First Trade', width: 40, dataIndex: 'startDate'},
		{header: 'Last Trade', width: 40, dataIndex: 'endDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'First Notice Date', width: 50, dataIndex: 'firstNoticeDate', hidden: true},
		{header: 'First Delivery Date', width: 50, dataIndex: 'firstDeliveryDate', hidden: true},
		{header: 'Last Delivery Date', width: 50, dataIndex: 'lastDeliveryDate', hidden: true},
		{header: 'Illiquid', width: 35, dataIndex: 'illiquid', type: 'boolean', hidden: true},
		{
			header: 'Price Multiplier', width: 50, dataIndex: 'priceMultiplier', type: 'float', hidden: true,
			renderer: function(v, metaData, r) {
				if (!TCG.isBlank(r.json.priceMultiplierOverride) && TCG.getValue('instrument.priceMultiplier', r.json) !== v) {
					metaData.css = 'amountAdjusted';
					const qtip = 'Price Multiplier Overridden from Instrument value of: ' + TCG.renderAmount(TCG.getValue('instrument.priceMultiplier', r.json), false, '0,000.0000', true);
					metaData.attr = TCG.renderQtip(qtip);
				}
				return v;
			}
		},
		{header: 'Active', width: 30, dataIndex: 'active', type: 'boolean'}
	],
	plugins: [
		{ptype: 'investment-contextmenu-plugin', recordSecurityIdPath: 'id', recordInstrumentIdPath: 'instrument.id', filterForPositions: false, openInstrument: false, openClientAccount: false, openHoldingAccount: false}
	],
	editor: {
		detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
		getDefaultData: function(gridPanel) { // defaults instrument/hierarchy
			return {
				instrument: gridPanel.getWindow().getMainForm().formValues
			};
		},
		// Add "Add from Template" Button (See CDS and TRS swaps for hierarchy specific examples - all others use the generic copy window)
		addToolbarAddButton: function(tb, gridPanel) {
			tb.add({
				text: 'Add',
				xtype: 'splitbutton',
				tooltip: 'Add a new item',
				iconCls: 'add',
				scope: this,
				handler: function() {
					this.openDetailPage(this.getDetailPageClass(), gridPanel);
				},
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Add',
							tooltip: 'Add a new item',
							iconCls: 'add',
							scope: this,
							handler: function() {
								this.openDetailPage(this.getDetailPageClass(), this.getGridPanel());
							}
						},
						{
							text: 'Add from Template',
							tooltip: 'Create a new security using the selected as a template',
							iconCls: 'copy',
							handler: function() {
								const h = TCG.getValue('hierarchy', gridPanel.getWindow().getMainForm().formValues);
								if (!h) {
									TCG.showError('Missing hierarchy information for this instrument.', 'Copy from Template');
									return false;
								}
								const sm = gridPanel.grid.getSelectionModel();
								if (sm.getCount() === 0) {
									TCG.showError('Please select a security to use as the template.', 'No Row(s) Selected');
								}
								else if (sm.getCount() !== 1) {
									TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
								}
								else {
									const addFromTemplateClass = 'Clifton.investment.instrument.copy.SecurityCopyWindow';
									const cmpId = TCG.getComponentId(addFromTemplateClass);
									const defaultData = {hierarchyId: h.id};
									defaultData.copyFromSecurityId = sm.getSelected().id;
									defaultData.copyFromSecurityIdLabel = sm.getSelected().label;
									defaultData.instrument = gridPanel.getWindow().getMainForm().formValues;
									TCG.createComponent(addFromTemplateClass, {
										id: cmpId,
										defaultData: defaultData,
										openerCt: gridPanel
									});
								}
							}
						}
					]
				})
			});
			tb.add('-');
		}

	},
	getLoadParams: function() {
		return {
			'instrumentId': this.getWindow().getMainFormId(),
			requestedMaxDepth: 4
		};
	}
});
Ext.reg('investment-instrumentSecurityGrid', Clifton.investment.instrument.InstrumentSecurityGrid);


Clifton.investment.instrument.InstrumentGroupGrid_ByInstrument = Ext.extend(TCG.grid.GridPanel, {
	// Drives how to find the instrument id on the main form panel instead of over-riding getInstrumentId method
	securityWindow: false, // By Default assumes Instrument Window, set security: true to use on 1:1 windows where instrument window is not separated.
	getInstrumentId: function() {
		if (this.securityWindow === false) {
			return TCG.getParentTabPanel(this).getWindow().getMainFormId();
		}
		return TCG.getValue('instrument.id', this.getWindow().getMainForm().formValues);
	},

	title: 'Included Instrument Groups',
	name: 'investmentGroupItemInstrumentListFind',
	instructions: 'This instrument is associated with the following Investment Groups.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'GroupItemID', width: 15, dataIndex: 'referenceOne.id', hidden: true, filter: false},
		{header: 'Investment Group', width: 150, dataIndex: 'referenceOne.group.name', defaultSortColumn: true, filter: {searchFieldName: 'groupNameContains'}},
		{header: 'Group Item', width: 200, dataIndex: 'referenceOne.labelExpanded', filter: {searchFieldName: 'searchPattern'}},
		{header: 'Currency Type', width: 50, dataIndex: 'referenceOne.currencyType', storeData: Clifton.investment.instrument.CurrencyTypes}
	],
	editor: {
		detailPageClass: 'Clifton.investment.setup.GroupItemWindow',
		drillDownOnly: true,
		getDetailPageId: function(gridPanel, row) {
			return row.json.referenceOne.id;
		}
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		const instId = this.getInstrumentId();
		if (TCG.isBlank(instId)) {
			TCG.showError('Selected Instrument is missing');
			return false;
		}
		return {instrumentId: instId};
	}
});
Ext.reg('investment-instrumentGroupGrid_ByInstrument', Clifton.investment.instrument.InstrumentGroupGrid_ByInstrument);


Clifton.investment.instrument.InstrumentGroupMissingGrid_ByInstrument = Ext.extend(TCG.grid.GridPanel, {
	// Drives how to find the instrument id on the main form panel instead of over-riding getInstrumentId method
	securityWindow: false, // By Default assumes Instrument Window, set security: true to use on 1:1 windows where instrument window is not separated.
	getInstrumentId: function() {
		if (this.securityWindow === false) {
			return TCG.getParentTabPanel(this).getWindow().getMainFormId();
		}
		return TCG.getValue('instrument.id', this.getWindow().getMainForm().formValues);
	},

	title: 'Missing Instrument Groups',
	name: 'investmentGroupListFind',
	instructions: 'This investment instrument is not included in the following Investment Groups.',
	topToolbarSearchParameter: 'searchPattern',

	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Investment Group', width: 125, dataIndex: 'name', defaultSortField: true},
		{header: 'Base CCY', width: 30, dataIndex: 'baseCurrency.symbol', filter: {searchFieldName: 'baseCurrencySymbol'}}
	],
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Investment Group Tags'},
			{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
		];
	},
	editor: {
		detailPageClass: 'Clifton.investment.setup.GroupWindow',
		drillDownOnly: true
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		const instId = this.getInstrumentId();
		if (TCG.isBlank(instId)) {
			TCG.showError('Selected Instrument is missing');
			return false;
		}
		const params = {missingInstrumentId: instId};
		const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
		if (TCG.isNotBlank(tag.getValue())) {
			params.categoryName = 'Investment Group Tags';
			params.categoryHierarchyId = tag.getValue();
		}
		return params;
	}
});
Ext.reg('investment-instrumentGroupMissingGrid_ByInstrument', Clifton.investment.instrument.InstrumentGroupMissingGrid_ByInstrument);


Clifton.investment.instrument.SecurityGroupGrid_BySecurity = Ext.extend(TCG.grid.GridPanel, {
	getInvestmentSecurityId: function() {
		return TCG.getParentTabPanel(this).getWindow().getMainFormId();
	},

	name: 'investmentSecurityGroupListFind',
	instructions: 'This security is associated with the following Security Groups.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Group Name', width: 80, dataIndex: 'name'},
		{header: 'Description', width: 100, dataIndex: 'description', hidden: true},
		{header: 'Modify Condition', width: 80, dataIndex: 'entityModifyCondition.name', filter: {searchFieldName: 'entityModifyConditionName'}},
		{header: 'Rebuild Bean', width: 90, dataIndex: 'rebuildSystemBean.name', filter: false, hidden: true},
		{header: 'Rebuild on Security Insert', width: 40, dataIndex: 'rebuildOnNewSecurityInsert', type: 'boolean', tooltip: 'Controls whether or not this group is immediately rebuilt when a new Investment Security is inserted.', hidden: true},
		{header: 'Allow Same Instrument', width: 35, dataIndex: 'securityFromSameInstrumentAllowed', type: 'boolean', hidden: true},
		{header: 'System Managed', width: 25, dataIndex: 'systemManaged', type: 'boolean', sortable: false},
		{header: 'System Defined', width: 25, dataIndex: 'systemDefined', type: 'boolean'}
	],
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Investment Security Group Tags'},
			{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
		];
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		const securityId = this.getInvestmentSecurityId();
		if (TCG.isBlank(securityId)) {
			TCG.showError('Selected Security is missing');
			return false;
		}
		const params = {securityId: securityId};
		const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
		if (TCG.isNotBlank(tag.getValue())) {
			params.categoryName = 'Investment Security Group Tags';
			params.categoryHierarchyId = tag.getValue();
		}
		return params;
	},
	editor: {
		detailPageClass: 'Clifton.investment.instrument.SecurityGroupWindow'
	}
});
Ext.reg('investment-securityGroupGrid_BySecurity', Clifton.investment.instrument.SecurityGroupGrid_BySecurity);


Clifton.investment.manager.ManagerAccountsGrid_ByClient = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentManagerAccountListFind',
	viewNames: ['Default View', 'Proxy Details', 'Cash Details', 'Linked Manager Details'],
	isIncludeRelatedClientCheckbox: function() {
		return false;
	},
	getRelatedClientCheckboxLabel: function() {
		return 'Include Parent/Child Client\'s Managers';
	},
	instructions: 'A list of money manager accounts used by this client.  Each manager account can be assigned to one or more client accounts, usually in order to overlay manager\'s assets.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Client', width: 75, dataIndex: 'client.name', filter: {searchFieldName: 'clientName'}, hidden: true},
		{header: 'Manager Company', width: 100, dataIndex: 'managerCompany.name', filter: {searchFieldName: 'managerCompanyName'}, viewNames: ['Default View']},
		{header: 'Manager Account', width: 100, dataIndex: 'labelShort', filter: {searchFieldName: 'accountNameNumber'}, allViews: true},
		{header: 'Display Name', width: 100, dataIndex: 'accountName', hidden: true},
		{header: 'Base CCY', width: 40, dataIndex: 'baseCurrency.symbol', filter: {type: 'combo', searchFieldName: 'baseCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}, viewNames: ['Default View']},
		{header: 'Benchmark', width: 100, hidden: true, dataIndex: 'benchmarkSecurity.label', filter: {searchFieldName: 'benchmarkSecurityLabel'}},
		{header: 'Custodial Account', width: 100, dataIndex: 'custodianLabel', filter: {type: 'combo', searchFieldName: 'custodianCompanyId', displayField: 'label', url: 'businessCompanyListFind.json?companyType=Custodian'}, viewNames: ['Default View']},
		{header: 'Zero Balance If No Download', width: 50, dataIndex: 'zeroBalanceIfNoDownload', type: 'boolean', hidden: true},
		{
			header: 'Manager Type', dataIndex: 'managerType', width: 60, allViews: true,
			filter: {
				type: 'combo', searchFieldName: 'managerType', displayField: 'name', valueField: 'value', mode: 'local',
				store: new Ext.data.ArrayStore({
					fields: ['name', 'value', 'description'],
					data: Clifton.investment.manager.ManagerTypes
				})
			},
			renderer: function(v, p, r) {
				if (v === 'LINKED') {
					let qtip = '<table><tr><td>' + r.data['linkedManagerTypeLabel'] + '</td></tr>';
					if (r.data['linkedManagerProcessingDependent'] === true) {
						qtip += '<tr><td>* Portfolio Processing Dependent</td></tr>';
					}
					qtip += '</table>';
					p.attr = 'qtip=\'' + qtip + '\'';
					p.css = 'amountAdjusted';
				}
				return v;
			}
		},
		{
			header: 'Linked Type', hidden: true, dataIndex: 'linkedManagerTypeLabel', width: 60, viewNames: ['Linked Manager Details'],
			filter: {
				type: 'combo', searchFieldName: 'linkedManagerType', displayField: 'name', valueField: 'value', mode: 'local',
				store: new Ext.data.ArrayStore({
					fields: ['name', 'value', 'description'],
					data: Clifton.investment.manager.LinkedManagerTypes
				})
			}
		},
		{header: 'Linked Client Account', hidden: true, dataIndex: 'linkedInvestmentAccount.label', width: 100, filter: {searchFieldName: 'linkedInvestmentAccountLabel'}, viewNames: ['Linked Manager Details']},

		{header: 'Proxy Date', dataIndex: 'proxyValueDate', hidden: true, width: 40, viewNames: ['Proxy Details']},
		{header: 'Proxy Value', dataIndex: 'proxyValue', hidden: true, width: 50, type: 'currency', useNull: true, viewNames: ['Proxy Details']},
		{header: 'Proxy Benchmark', width: 100, hidden: true, dataIndex: 'proxyBenchmarkSecurity.label', filter: {searchFieldName: 'proxyBenchmarkSecurityLabel'}, viewNames: ['Proxy Details']},
		{header: 'Proxy Quantity', dataIndex: 'proxySecurityQuantity', hidden: true, width: 50, type: 'currency', useNull: true, viewNames: ['Proxy Details']},
		{header: 'Proxy Rate', width: 100, hidden: true, dataIndex: 'proxyBenchmarkRateIndex.label', filter: {searchFieldName: 'proxyBenchmarkRateIndexLabel'}, viewNames: ['Proxy Details']},
		{header: 'Proxy Growth Inverted', width: 50, hidden: true, dataIndex: 'proxyValueGrowthInverted', type: 'boolean', viewNames: ['Proxy Details']},
		{header: 'Proxy Securities Only', width: 50, hidden: true, dataIndex: 'proxySecuritiesOnly', type: 'boolean', viewNames: ['Proxy Details']},
		{header: 'Proxy To Last Known Price', width: 50, hidden: true, dataIndex: 'proxyToLastKnownPrice', type: 'boolean', viewNames: ['Proxy Details']},

		{
			header: 'Cash Adj Type', dataIndex: 'cashAdjustmentType', width: 50, tooltip: 'Cash Adjustment Type', viewNames: ['Default View', 'Cash Details'],
			filter: {
				type: 'combo', searchFieldName: 'cashAdjustmentType', displayField: 'name', valueField: 'value', mode: 'local',
				store: new Ext.data.ArrayStore({
					fields: ['name', 'value', 'description'],
					data: Clifton.investment.manager.ManagerCashAdjustmentTypes
				})
			}
		},
		{header: 'Custom Cash Date', dataIndex: 'customCashPercentUpdateDate', type: 'date', width: 60, hidden: true, viewNames: ['Cash Details']},
		{header: 'Custom Cash Value', dataIndex: 'customCashValue', width: 50, type: 'currency', hidden: true, useNull: true, viewNames: ['Cash Details']},
		{header: 'Inactive', dataIndex: 'inactive', width: 35, type: 'boolean', allViews: true}
	],
	configureToolsMenu: function(menu) {
		const gp = this;
		menu.add('-');
		menu.add({
			text: 'Rollup Allocation Entry',
			iconCls: 'manager',
			tooltip: 'Quick Edit of rollup manager allocations',
			scope: this,
			handler: function() {
				TCG.createComponent('Clifton.investment.manager.RollupListWindow', {
					defaultData: {client: gp.getWindow().getMainForm().formValues},
					openerCt: this
				});
			}
		});
	},
	editor: {
		detailPageClass: 'Clifton.investment.manager.AccountWindow',
		getDefaultData: function(gridPanel) { // defaults clientAccount for the detail page
			return {
				client: gridPanel.getWindow().getMainForm().formValues
			};
		},
		deleteEnabled: false
	},
	getTopToolbarFilters: function(toolbar) {
		if (this.isIncludeRelatedClientCheckbox() === true) {
			return [
				{boxLabel: this.getRelatedClientCheckboxLabel(), xtype: 'toolbar-checkbox', name: 'includeRelatedClients'}
			];
		}
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			// default to active
			this.setFilterValue('inactive', false);
		}
		const cId = this.getWindow().getMainFormId();
		const cm = this.getColumnModel();
		if (this.isIncludeRelatedClientCheckbox() === true && TCG.getChildByName(this.getTopToolbar(), 'includeRelatedClients').checked) {
			cm.setHidden(cm.findColumnIndex('client.name'), false);
			return {clientIdOrRelatedClient: cId};
		}
		cm.setHidden(cm.findColumnIndex('client.name'), true);
		return {clientId: cId};
	}
});
Ext.reg('investment-managerAccountsGrid_byClient', Clifton.investment.manager.ManagerAccountsGrid_ByClient);


Clifton.investment.account.InvestmentAccountsGrid_ByClient = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentAccountListFind',
	xtype: 'gridpanel',
	isIncludeRelatedClientCheckbox: function() {
		return false;
	},
	getRelatedClientCheckboxLabel: function() {
		return 'Include Parent/Child Client\'s Accounts';
	},
	instructions: 'The following investment accounts issued by us are associated with the client.',
	additionalPropertiesToRequest: 'businessService.aumCalculatorBean.labelShort',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Client', width: 75, dataIndex: 'businessClient.name', filter: {searchFieldName: 'clientName'}, hidden: true},
		{header: 'Account Type', width: 60, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'accountTypeId', displayField: 'name', url: 'investmentAccountTypeListFind.json'}, hidden: true},
		{header: 'Account #', dataIndex: 'number', width: 50, defaultSortColumn: true},
		{header: 'Account Name', dataIndex: 'name', width: 130},
		{header: 'Pooled Vehicle Type', width: 80, dataIndex: 'clientAccountType', hidden: true},
		{header: 'Contract', width: 150, dataIndex: 'businessContract.label', filter: {type: 'combo', searchFieldName: 'businessContractId', displayField: 'name', url: 'businessContractListFind.json'}, hidden: true},
		{header: 'GIPS', width: 80, dataIndex: 'discretionType', hidden: true},
		{header: 'Service', dataIndex: 'businessService.name', width: 130, filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}},
		{header: 'Team', width: 50, dataIndex: 'teamSecurityGroup.name', filter: {searchFieldName: 'teamSecurityGroupId', type: 'combo', url: 'securityGroupTeamList.json', loadAll: true}},
		{header: 'Base Currency', width: 50, dataIndex: 'baseCurrency.symbol', filter: {searchFieldName: 'baseCurrencyId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true'}},
		{
			header: 'AUM Type', width: 80, dataIndex: 'aumCalculatorBean.labelShort', hidden: true, filter: {searchFieldName: 'aumCalculatorBeanId', type: 'combo', displayField: 'labelShort', url: 'systemBeanListFind.json?groupName=Investment Account Calculation Snapshot Calculator'},
			renderer: function(v, metaData, r) {
				const serviceCalculator = TCG.getValue('businessService.aumCalculatorBean.labelShort', r.json);
				if (TCG.isNotBlank(v) && TCG.isNotEquals(v, serviceCalculator)) {
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'Account Override.  Service AUM Calculator is [' + serviceCalculator + ']\'';
				}
				return v;
			}
		},
		{header: 'Closing Method', width: 50, dataIndex: 'accountingClosingMethod', filter: false, tooltip: 'Accounting Closing Method (FIFO, LIFO, etc.) from corresponding Business Service or Account level override.', hidden: true},
		{header: 'Closing Method Override', width: 50, dataIndex: 'accountingClosingMethodOverride', filter: {type: 'list', options: Clifton.business.service.AccountingClosingMethods}, tooltip: 'Optionally overrides Accounting Closing Method (FIFO, LIFO, etc.) specified for account\'s Business Service.', hidden: true},
		{header: 'EV Portfolio Code', width: 80, dataIndex: 'externalPortfolioCode.text', hidden: true, filter: {searchFieldName: 'externalPortfolioCodeName'}},
		{header: 'EV Product Code', width: 80, dataIndex: 'externalProductCode.text', hidden: true, filter: {searchFieldName: 'externalProductCodeName'}},
		{header: 'Channel', width: 100, dataIndex: 'clientAccountChannel.text', hidden: true, filter: {searchFieldName: 'clientAccountChannelName'}, renderer: TCG.renderValueWithTooltip},
		{header: 'Workflow State', width: 50, dataIndex: 'workflowState.label', filter: {searchFieldName: 'workflowStateName'}},
		{header: 'Workflow Status', width: 50, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=InvestmentAccount'}, hidden: true},
		{header: 'Effective Start', width: 50, dataIndex: 'workflowStateEffectiveStartDate', filter: {searchFieldName: 'workflowStateEffectiveStartDate'}, hidden: true},
		{header: 'Positions On Date', width: 60, dataIndex: 'inceptionDate'},
		{header: 'Performance Start Date', width: 70, dataIndex: 'performanceInceptionDate', hidden: true},
		{header: 'Issued By', width: 120, dataIndex: 'issuingCompany.name', hidden: true, filter: {type: 'combo', searchFieldName: 'issuingCompanyId', url: 'businessCompanyListFind.json'}},
		{
			header: 'Client Directed', width: 80, dataIndex: 'clientDirected', type: 'boolean', hidden: true,
			tooltip: 'Trading for this account is Directed by the Client (used by Holding Accounts only)'
		},
		{header: 'Platform', width: 90, dataIndex: 'companyPlatform.name', hidden: true, filter: {type: 'combo', searchFieldName: 'companyPlatformId', url: 'businessCompanyPlatformListFind.json'}, tooltip: 'Broker-Dealer or RIA program providing access to a variety of investment alternatives including third party investment managers\'s strategies.'},
		{header: 'Wrap', width: 50, dataIndex: 'wrapAccount', hidden: true, type: 'boolean', tooltip: 'Differentiates accounts where the client pays a periodic account fee rather than trade commissions.  Is tied to Platform in IMS so that we may differentiate between clients who are vs. are not utilizing the platform WRAP feature (retail clients generally do (Institutional clients generally do not) participate in the WRAP feature/account structure on a platform.'}
	],
	editor: {
		detailPageClass: {
			getItemText: function(rowItem) {
				return 'Default';
			},
			items: [
				{text: 'Client Account', iconCls: 'account', className: 'Clifton.investment.account.CliftonAccountWindow'},
				{text: 'Holding Account', iconCls: 'account', className: 'Clifton.investment.account.NonCliftonAccountWindow'}
			]
		},
		getDetailPageClassByText: function(text) {
			if (TCG.isEquals('Default', text)) {
				// Let the window selector pick it for the account
				// MUST do this for existing accounts otherwise the required/dependent fields are getting cleared for platform and wrap
				// SEE: INVESTMENT-717
				return 'Clifton.investment.account.AccountWindow';
			}
			let result = undefined;
			Ext.each(this.detailPageClass.items, function(item) {
				if (TCG.isEquals(item.text, text)) {
					result = item.className;
				}
			});
			return result;
		},
		getDefaultData: function(gridPanel, row, className, itemText) {
			const param = {
				businessClient: gridPanel.getWindow().getMainForm().formValues,
				ourAccount: false
			};
			if (itemText === 'Client Account') {
				return TCG.data.getDataPromiseUsingCaching('investmentAccountTypeByOurAccount.json', gridPanel, 'investment.getOurAccountType')
					.then(function(type) {
						param['ourAccount'] = true;
						param['type'] = type;
						return TCG.data.getDataPromiseUsingCaching('investmentSecurityBySymbol.json?symbol=USD&currency=true', gridPanel, 'investment.security.usd');
					})
					.then(function(baseCurrency) {
						param['baseCurrency'] = baseCurrency;
						return param;
					});
			}
			return param;
		},
		getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
			return undefined; // Not needed for Existing Runs
		}
	},
	getTopToolbarFilters: function(toolbar) {
		const display = {
			fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 150, minListWidth: 150, mode: 'local', value: 'CLIENT', displayField: 'name', valueField: 'value',
			store: new Ext.data.ArrayStore({
				fields: ['value', 'name', 'description'],
				data: [['ALL', 'All Account'], ['CLIENT', 'Client Accounts Only', 'Display Client accounts only'], ['HOLDING', 'Holding Accounts Only', 'Display holding accounts only']]
			})
		};
		if (this.isIncludeRelatedClientCheckbox() === true) {
			return [
				{boxLabel: this.getRelatedClientCheckboxLabel(), xtype: 'toolbar-checkbox', name: 'includeRelatedClients'},
				display
			];
		}
		return [display];
	},
	getLoadParams: function(firstLoad) {
		const params = {
			clientId: this.getWindow().getMainFormId(),
			ourAccount: true
		};
		if (!firstLoad) {
			const cm = this.getColumnModel();
			const dType = TCG.getChildByName(this.getTopToolbar(), 'displayType').getValue();
			if (dType === 'CLIENT') {
				params.ourAccount = true;
				cm.setHidden(cm.findColumnIndex('type.name'), true);
				cm.setHidden(cm.findColumnIndex('issuingCompany.name'), true);
				cm.setHidden(cm.findColumnIndex('businessService.name'), false);
				cm.setHidden(cm.findColumnIndex('inceptionDate'), false);
			}
			else if (dType === 'HOLDING') {
				params.ourAccount = false;
				cm.setHidden(cm.findColumnIndex('type.name'), false);
				cm.setHidden(cm.findColumnIndex('issuingCompany.name'), false);
				cm.setHidden(cm.findColumnIndex('businessService.name'), true);
				cm.setHidden(cm.findColumnIndex('inceptionDate'), true);
			}
			else {
				cm.setHidden(cm.findColumnIndex('type.name'), false);
				cm.setHidden(cm.findColumnIndex('issuingCompany.name'), false);
				cm.setHidden(cm.findColumnIndex('businessService.name'), false);
				cm.setHidden(cm.findColumnIndex('inceptionDate'), true);
				delete params.ourAccount;
			}
			if ((this.isIncludeRelatedClientCheckbox() === true) && TCG.getChildByName(this.getTopToolbar(), 'includeRelatedClients').checked) {
				cm.setHidden(cm.findColumnIndex('businessClient.name'), false);
				delete params.clientId;
				params.clientIdOrRelatedClient = this.getWindow().getMainFormId();
			}
			else {
				cm.setHidden(cm.findColumnIndex('businessClient.name'), true);
			}
			this.updateAllColumnWidthsFORCE();
		}
		return params;
	}
});
Ext.reg('investment-accountsGrid_byClient', Clifton.investment.account.InvestmentAccountsGrid_ByClient);

Clifton.investment.account.InvestmentAccountsGrid_ByClientRelationship = Ext.extend(Clifton.investment.account.InvestmentAccountsGrid_ByClient, {
	instructions: 'The following investment accounts issued by us are associated with the client relationship.',
	getLoadParams: function(firstLoad) {
		const params = {
			clientRelationshipId: this.getWindow().getMainFormId(),
			ourAccount: true
		};
		this.getColumnModel().setHidden(this.getColumnModel().findColumnIndex('businessClient.name'), false);
		if (!firstLoad) {
			const cm = this.getColumnModel();
			const dType = TCG.getChildByName(this.getTopToolbar(), 'displayType').getValue();
			if (dType === 'CLIENT') {
				params.ourAccount = true;
				cm.setHidden(cm.findColumnIndex('type.name'), true);
				cm.setHidden(cm.findColumnIndex('issuingCompany.name'), true);
				cm.setHidden(cm.findColumnIndex('businessService.name'), false);
				cm.setHidden(cm.findColumnIndex('inceptionDate'), false);
			}
			else if (dType === 'HOLDING') {
				params.ourAccount = false;
				cm.setHidden(cm.findColumnIndex('type.name'), false);
				cm.setHidden(cm.findColumnIndex('issuingCompany.name'), false);
				cm.setHidden(cm.findColumnIndex('businessService.name'), true);
				cm.setHidden(cm.findColumnIndex('inceptionDate'), true);
			}
			else {
				cm.setHidden(cm.findColumnIndex('type.name'), false);
				cm.setHidden(cm.findColumnIndex('issuingCompany.name'), false);
				cm.setHidden(cm.findColumnIndex('businessService.name'), false);
				cm.setHidden(cm.findColumnIndex('inceptionDate'), true);
				delete params.ourAccount;
			}
			this.updateAllColumnWidthsFORCE();
		}
		return params;
	}
});
Ext.reg('investment-accountsGrid_byClientRelationship', Clifton.investment.account.InvestmentAccountsGrid_ByClientRelationship);

Clifton.investment.account.InvestmentAccountRelationshipGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentAccountRelationshipListFind',
	instructions: 'A list of all investment account relationships defined in the system.',
	viewNames: ['Default View', 'Accounting View'],

	// Used on Account Window to Show one version of the other and condense labels
	showClient: true,
	showStartEndDates: true, // Will Hide/Show Active Column based on the reverse
	showDrillDowns: false,
	excludeMainInfo: false,
	excludeRelatedInfo: false,
	initComponent: function() {
		const currentItems = [];
		Ext.each(this.beforeColumns, function(f) {
			currentItems.push(f);
		});
		let condenseLabels;
		if (this.excludeMainInfo === false) {
			condenseLabels = (this.excludeRelatedInfo === true);
			Ext.each(TCG.clone(this.mainAccountColumns), function(f) {
				if (condenseLabels) {
					if (f.shortHeader) {
						f.header = f.shortHeader;
						if (f.header === 'Account') {
							f.defaultSortColumn = true;
						}
					}
				}
				currentItems.push(f);
			});
		}
		if (this.excludeRelatedInfo === false) {
			condenseLabels = (this.excludeMainInfo === true);
			Ext.each(TCG.clone(this.relatedAccountColumns), function(f) {
				if (condenseLabels) {
					if (f.shortHeader) {
						f.header = f.shortHeader;
						if ('Type' === f.shortHeader) {
							f.hidden = false;
						}
						if (f.header === 'Account') {
							f.defaultSortColumn = true;
						}
					}
				}
				currentItems.push(f);
			});
		}
		Ext.each(this.afterColumns, function(f) {
			currentItems.push(f);
		});

		if (this.showDrillDowns === true) {
			if (this.excludeMainInfo === false) {
				currentItems.push(this.mainAccountDrillDownColumn);
			}
			if (this.excludeRelatedInfo === false) {
				currentItems.push(this.relatedAccountDrillDownColumn);
			}
		}

		this.columns = currentItems;
		Clifton.investment.account.InvestmentAccountRelationshipGrid.superclass.initComponent.call(this);
		this.showHideColumn('referenceOne.businessClient.label', (this.showClient === false));
		this.showHideColumn('startDate', (this.showStartEndDates === false));
		this.showHideColumn('endDate', (this.showStartEndDates === false));
		this.showHideColumn('active', (this.showStartEndDates === true));
		this.showHideAdditionalColumns();
	},
	showHideAdditionalColumns: function() {
		// can be overridden for additional customization
	},
	showHideColumn: function(dataIndex, hidden) {
		const i = this.getColumnModel().findColumnIndex(dataIndex);
		this.getColumnModel().config[i].hidden = hidden;
	},
	// Common Relationship Columns - Displayed First
	beforeColumns: [
		{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
		{header: 'Client', dataIndex: 'referenceOne.businessClient.label', width: 100, filter: {type: 'combo', searchFieldName: 'mainAccountClientId', displayField: 'label', url: 'businessClientListFind.json'}, defaultSortColumn: true}
	],
	// Common Relationship Columns - Displayed Last
	afterColumns: [
		{header: 'Purpose', dataIndex: 'purpose.name', width: 60, filter: {type: 'combo', searchFieldName: 'purposeId', url: 'investmentAccountRelationshipPurposeListFind.json'}},
		{
			header: 'Security Group', dataIndex: 'securityGroup.name', width: 50, hidden: true, filter: {searchFieldName: 'securityGroupName'},
			tooltip: 'Optional group of securities that this relationship applies to.  If set, security must belong to this group. For example, can have define relationships to 2 custodian accounts: one for PUT options and one for CALL options.'
		},
		{header: 'Asset Class', dataIndex: 'accountAssetClass.label', width: 50, filter: {searchFieldName: 'investmentAccountAssetClass'}, viewNames: ['Default View']},

		{header: 'Start', tooltip: 'Relationship Start', dataIndex: 'startDate', width: 40},
		{header: 'End', tooltip: 'Relationship End', dataIndex: 'endDate', width: 40},
		{header: 'Active', tooltip: 'Active Relationship', width: 30, dataIndex: 'active', type: 'boolean'}
	],
	// Main Account Columns
	mainAccountColumns: [
		{header: 'Main Account', shortHeader: 'Account', dataIndex: 'referenceOne.label', width: 130, filter: {type: 'combo', searchFieldName: 'mainAccountId', displayField: 'label', url: 'investmentAccountListFind.json'}},
		{header: 'Main Account Type', shortHeader: 'Type', width: 60, dataIndex: 'referenceOne.type.name', filter: {type: 'combo', searchFieldName: 'mainAccountTypeId', displayField: 'name', url: 'investmentAccountTypeListFind.json'}, hidden: true},
		{header: 'Main Account Issuer', shortHeader: 'Issuer', dataIndex: 'referenceOne.issuingCompany.name', width: 75, filter: {type: 'combo', searchFieldName: 'mainAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
		{header: 'Base Currency', shortHeader: 'Base Currency', dataIndex: 'referenceOne.baseCurrency.symbol', width: 50, hidden: true},
		{header: 'Main Workflow State', shortHeader: 'Workflow State', tooltip: 'Workflow State', dataIndex: 'referenceOne.workflowState.name', width: 40, filter: {searchFieldName: 'mainAccountWorkflowStateName'}, hidden: true, viewNames: ['Accounting View']},
		{header: 'Main Workflow Start', shortHeader: 'Workflow Start', tooltip: 'Workflow State Effective Start', dataIndex: 'referenceOne.workflowStateEffectiveStartDate', width: 40, filter: {searchFieldName: 'mainAccountWorkflowStateEffectiveStart'}, hidden: true, viewNames: ['Accounting View']}
	],
	mainAccountDrillDownColumn: {
		width: 18, dataIndex: 'referenceOne.id', filter: false, sortable: false,
		renderer: function(v, args, r) {
			return TCG.renderActionColumn('account', '', 'Open Main Account', 'ShowMain');
		}
	},
	// Related Account Columns
	relatedAccountColumns: [
		{header: 'Related Account', shortHeader: 'Account', dataIndex: 'referenceTwo.label', width: 130, filter: {type: 'combo', searchFieldName: 'relatedAccountId', displayField: 'label', url: 'investmentAccountListFind.json'}},
		{header: 'Related Account Type', shortHeader: 'Type', width: 60, dataIndex: 'referenceTwo.type.name', filter: {type: 'combo', searchFieldName: 'relatedAccountTypeId', displayField: 'name', url: 'investmentAccountTypeListFind.json'}, hidden: true},
		{header: 'Related Account Issuer', shortHeader: 'Issuer', dataIndex: 'referenceTwo.issuingCompany.name', width: 75, filter: {type: 'combo', searchFieldName: 'relatedAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
		{header: 'Base Currency', shortHeader: 'Base Currency', dataIndex: 'referenceTwo.baseCurrency.symbol', width: 50, hidden: true},
		{header: 'Related Workflow State', shortHeader: 'Workflow State', tooltip: 'Workflow State', dataIndex: 'referenceTwo.workflowState.name', width: 50, filter: {searchFieldName: 'relatedAccountWorkflowStateName'}, viewNames: ['Default View', 'Accounting View']},
		{header: 'Related Workflow Start', shortHeader: 'Workflow Start', tooltip: 'Workflow State Effective Start', dataIndex: 'referenceTwo.workflowStateEffectiveStartDate', width: 40, filter: {searchFieldName: 'relatedAccountWorkflowStateEffectiveStart'}, hidden: true, viewNames: ['Accounting View']}
	],
	relatedAccountDrillDownColumn: {
		width: 25, fixed: true, dataIndex: 'referenceTwo.id', filter: false, sortable: false,
		renderer: function(v, args, r) {
			return TCG.renderActionColumn('account', '', 'Open Related Account', 'ShowRelated');
		}
	},
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.investment.account.AccountRelationshipWindow'
	},
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					const row = grid.store.data.items[rowIndex];

					// Default to the Main Account
					let actId = row.json.referenceOne.id;
					if (eventName === 'ShowRelated') {
						actId = row.json.referenceTwo.id;
					}
					const gridPanel = grid.ownerGridPanel;
					gridPanel.editor.openDetailPage('Clifton.investment.account.AccountWindow', gridPanel, actId, row);
				}
			}
		}
	}
});
Ext.reg('investment-account-relationship-grid', Clifton.investment.account.InvestmentAccountRelationshipGrid);


Clifton.investment.instrument.structure.StructureTypes = [
	['STRUCTURED_INDEX', 'Structured Index'],
	['MODEL_PORTFOLIO', 'Model Portfolio']
];

Clifton.investment.instrument.structure.InvestmentSecurityStructuredIndexWeightsGrid = Ext.extend(TCG.grid.GridPanel, {
	xtype: 'gridpanel',
	name: 'investmentSecurityStructureAllocationListForSecurityAndDate',
	instructions: 'Select a date in the above toolbar to see what the calculated weights would be for this security and the structure active on that date.',
	columns: [
		{header: 'Order', width: 10, dataIndex: 'securityStructureWeight.instrumentWeight.order', type: 'int', hidden: true},
		{header: 'Instrument', width: 100, dataIndex: 'currentSecurity.instrument.name'},
		{header: 'Weight', width: 75, dataIndex: 'securityStructureWeight.coalesceWeight', type: 'float'},
		{header: 'Current Security', width: 75, dataIndex: 'currentSecurity.symbol'},
		{
			header: 'Target Adjustment', width: 100, dataIndex: 'securityStructureWeight.currentSecurityTargetAdjustmentTypeLabel',
			tooltip: 'Used during portfolio processing, When both non-current and current security are available determines how to adjust the targets.'
		},
		{header: 'Always Include Current Security', width: 100, dataIndex: 'securityStructureWeight.alwaysIncludeCurrentSecurity', type: 'boolean', tooltip: 'This flag determines whether or not a current security is always included during portfolio run processing, even if it results in a 0 allocation (0 target, 0 exposure). If FALSE, the current security will not be included if it results in a 0 allocation.'},
		{header: 'Comments', width: 100, dataIndex: 'comments'},
		{header: 'Current Weight (%)', width: 75, dataIndex: 'allocationWeight', type: 'float', summaryType: 'sum'}
	],
	plugins: {ptype: 'gridsummary'},
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'activeOnDate', value: Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y')});
		return filters;
	},
	// Must Be Overridden
	getSecurityId: function() {
		return undefined;
	},
	// Optionally overridden to force viewing calculated weights for a specific structure even if not active on that date yet
	getStructureId: function() {
		return undefined;
	},
	getLoadParams: function(firstLoad) {
		const pm = {};
		pm.date = TCG.getChildByName(this.getTopToolbar(), 'activeOnDate').getValue().format('m/d/Y');
		pm.securityId = this.getSecurityId();
		const strId = this.getStructureId();
		if (strId) {
			pm.structureId = strId;
		}
		return pm;
	}
});
Ext.reg('investment-securityStructure-structuredIndex-weights', Clifton.investment.instrument.structure.InvestmentSecurityStructuredIndexWeightsGrid);


Clifton.investment.instrument.structure.InvestmentSecurityStructuredIndexModelPortfolioWeightsGrid = Ext.extend(TCG.grid.GridPanel, {
	xtype: 'gridpanel',
	name: 'investmentSecurityStructureAllocationListFind',
	instructions: 'Select a date in the above toolbar to see what the calculated weights would be for this security and the structure active on that date.',
	columns: [
		{header: 'Date', width: 30, dataIndex: 'measureDate', hidden: true},
		{header: 'Order', width: 10, dataIndex: 'securityStructureWeight.instrumentWeight.order', type: 'int', hidden: true, filter: {searchFieldName: 'order'}},
		{header: 'Instrument', width: 100, dataIndex: 'currentSecurity.instrument.name', filter: {searchFieldName: 'currentSecurityInstrumentLabel'}},
		{header: 'Weight', width: 60, dataIndex: 'securityStructureWeight.coalesceWeight', type: 'float', filter: {searchFieldName: 'coalesceWeight'}},
		{header: 'Current Security', width: 75, dataIndex: 'currentSecurity.symbol', filter: {searchFieldName: 'currentSecurityLabel'}},
		{header: 'Allocation Value (Local)', width: 75, dataIndex: 'allocationValueLocal', type: 'currency'},
		{header: 'FX Rate', width: 75, dataIndex: 'fxRateToBase', type: 'float'},
		{header: 'Allocation Value (Base)', width: 75, dataIndex: 'allocationValueBase', type: 'currency', summaryType: 'sum', filter: false, sortable: false},
		{header: 'Unrealized Gain/Loss (Base)', width: 75, dataIndex: 'additionalAmount1', type: 'currency'},
		{header: 'Realized Gain/Loss (Base)', width: 75, dataIndex: 'additionalAmount2', type: 'currency'},
		{header: 'Current Weight (%)', width: 75, dataIndex: 'allocationWeight', type: 'float', summaryType: 'sum'},
		{
			header: 'Target Adjustment', width: 100, dataIndex: 'securityStructureWeight.currentSecurityTargetAdjustmentTypeLabel',
			tooltip: 'Used during portfolio processing, When both non-current and current security are available determines how to adjust the targets.'
		},
		{header: 'Always Include Current Security', width: 100, dataIndex: 'securityStructureWeight.alwaysIncludeCurrentSecurity', type: 'boolean', tooltip: 'This flag determines whether or not a current security is always included during portfolio run processing, even if it results in a 0 allocation (0 target, 0 exposure). If FALSE, the current security will not be included if it results in a 0 allocation.'}
	],
	plugins: {ptype: 'gridsummary'},
	getTopToolbarFilters: function(toolbar) {
		return [{fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}];
	},
	addToolbarButtons: function(toolBar, gridPanel) {
		toolBar.add({
			text: 'Rebuild',
			tooltip: 'Rebuild structure allocations for this security structure for a date range',
			iconCls: 'run',
			scope: this,
			handler: function() {
				const dd = {};
				dd.securityId = gridPanel.getSecurityId();
				dd.structureId = gridPanel.getStructureId();
				dd.startDate = Clifton.calendar.getBusinessDayFrom(-1).format('Y-m-d 00:00:00');
				dd.endDate = dd.startDate;
				TCG.createComponent('Clifton.investment.instrument.structure.StructureAllocationRebuildWindow', {
					defaultData: dd,
					openerCt: gridPanel
				});
			}
		});
		toolBar.add('-');
	},
	// Must Be Overridden
	getSecurityId: function() {
		return undefined;
	},
	// Optionally overridden to force viewing calculated weights for a specific structure even if not active on that date yet
	getStructureId: function() {
		return undefined;
	},
	getLoadParams: function(firstLoad) {
		const pm = {};
		const dateField = TCG.getChildByName(this.getTopToolbar(), 'activeOnDate');
		if (firstLoad) {
			dateField.setValue(Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y'));
		}
		if (TCG.isNotBlank(dateField.getValue())) {
			pm.measureDate = dateField.getValue().format('m/d/Y');
		}
		pm.securityId = this.getSecurityId();
		pm.structureId = this.getStructureId();
		return pm;
	}
});
Ext.reg('investment-securityStructure-modelPortfolio-weights', Clifton.investment.instrument.structure.InvestmentSecurityStructuredIndexModelPortfolioWeightsGrid);


Clifton.investment.account.assetclass.PositionAllocationListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentAccountAssetClassPositionAllocationListFind',
	xtype: 'gridpanel',
	pageSize: 200,
	instructions: 'Asset Class Position Allocations allow explicitly defining contract splitting when processing portfolio runs.  Position allocations can be defined for up to one less than the total asset classes the security is split between.  The remaining asset class always gets the rest of the allocation, or if multiple the system will calculate the split.  Order allows defining which asset classes are filled first and are useful for more than 2 asset class splits with long and short positions where virtual contracts are used to fulfill the specified quantity.',
	wikiPage: 'IT/PIOS Contract Splitting and Virtual Contracts',
	// Overridden to return the account for default data in new window
	// Showing Columns and Filtering
	// Do not need to implement getAccount method if getAccountAssetClass method returns value
	showAccountColumn: true,
	getAccount: function() {
		if (this.showAccountColumn === true) {
			const t = this.getTopToolbar();
			const ia = TCG.getChildByName(t, 'accountId');
			if (ia) {
				return {id: ia.getValue(), label: ia.getRawValue()};
			}
		}
	},
	showAssetClassColumn: true,
	getAccountAssetClass: function() {
		return undefined;
	},
	getSecurity: function() {
		return undefined;
	},
	getDefaultActiveOnDate: function() {
		let dateV = undefined;
		const win = this.getWindow();
		if (win.params && win.params.activeOnDate) {
			dateV = win.params.activeOnDate;
		}
		if (!dateV) {
			dateV = Clifton.calendar.getBusinessDayFrom(-1);
		}
		return dateV;
	},
	initComponent: function() {
		TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);
		// Hide Columns if option set
		const cm = this.getColumnModel();
		cm.setHidden(cm.findColumnIndex('accountAssetClass.account.label'), (this.showAccountColumn === false));
		cm.setHidden(cm.findColumnIndex('accountAssetClass.label'), (this.showAssetClassColumn === false));
	},
	columns: [
		{header: 'Client Account', width: 175, dataIndex: 'accountAssetClass.account.label', filter: {searchFieldName: 'accountId', type: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'}, defaultSortColumn: true},
		{header: 'Asset Class', width: 150, dataIndex: 'accountAssetClass.label', filter: {searchFieldName: 'accountAssetClassLabel'}},
		{header: 'Replication', width: 100, dataIndex: 'replication.name', filter: {searchFieldName: 'replicationName'}},
		{header: 'Security', width: 75, dataIndex: 'security.symbol', filter: {searchFieldName: 'securityLabel'}},
		{header: 'Start Date', width: 50, dataIndex: 'startDate'},
		{header: 'End Date', width: 50, dataIndex: 'endDate'},
		{header: 'Order', width: 30, dataIndex: 'allocationOrder', type: 'int', useNull: true},
		{header: 'Quantity', width: 50, dataIndex: 'allocationQuantity', type: 'float'}
	],
	editor: {
		detailPageClass: 'Clifton.investment.account.assetclass.position.PositionAllocationWindow',
		copyURL: 'investmentAccountAssetClassPositionAllocationCopy.json',
		copyButtonName: 'Copy Allocation',
		copyButtonTooltip: 'Creates a copy of the selected allocation, ending the copied one before selected start date if not already ended.',
		copyHandler: function(gridPanel) {
			const editor = this;
			const grid = this.grid;
			const sm = grid.getSelectionModel();
			if (sm.getCount() === 0) {
				TCG.showError('Please select an existing allocation to copy.', 'No Row(s) Selected');
				return;
			}
			else if (sm.getCount() !== 1) {
				TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
				return;
			}
			Ext.Msg.confirm('Copy Selected Allocation', 'Are you sure you would like to copy the selected position allocation?  You will be able to enter a new Start Date and quantity for the new allocation.  The allocation you are copying will be updated to end the day before if not already inactive.', function(a) {
				if (a === 'yes') {
					const id = sm.getSelected().id;
					editor.openDetailPage('Clifton.investment.account.assetclass.position.PositionAllocationCopyWindow', editor.getGridPanel(), id);
				}
			});
		},
		getDefaultData: function(gridPanel) {
			const aas = gridPanel.getAccountAssetClass();
			const dd = {};
			if (TCG.isNotNull(aas)) {
				dd.accountAssetClass = aas;
			}

			const act = gridPanel.getAccount();
			if (TCG.isNotNull(act)) {
				dd.accountAssetClass = {account: act};
			}
			const sec = gridPanel.getSecurity();
			if (TCG.isNotNull(sec)) {
				dd.security = sec;
			}
			return dd;
		}
	},

	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		if (this.showAccountColumn === true) {
			filters.push({fieldLabel: 'Client Account', xtype: 'toolbar-combo', name: 'accountId', width: 250, url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'accountAssetClass.account.label', displayField: 'label'});
		}
		filters.push({fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'activeOnDate'});
		return filters;
	},
	getLoadParams: function(firstLoad) {
		const pm = {};
		if (this.showAccountColumn !== true) {
			if (TCG.isNotNull(this.getAccount())) {
				pm.accountId = this.getAccount().id;
			}
		}
		if (this.showAssetColumn !== true) {
			if (TCG.isNotNull(this.getAccountAssetClass())) {
				pm.accountAssetClassId = this.getAccountAssetClass().id;
			}
		}
		if (TCG.isNotNull(this.getSecurity())) {
			pm.securityId = this.getSecurity().id;
		}
		let dateV = TCG.getChildByName(this.getTopToolbar(), 'activeOnDate').getValue();
		if (firstLoad) {
			dateV = this.getDefaultActiveOnDate();
			TCG.getChildByName(this.getTopToolbar(), 'activeOnDate').setValue(dateV);
		}
		if (dateV) {
			pm.activeOnDate = dateV.format('m/d/Y');
		}
		return pm;
	}
});
Ext.reg('investment-accountAssetClass-PositionAllocation', Clifton.investment.account.assetclass.PositionAllocationListGrid);


Clifton.investment.calendar.InvestmentCalendarEventGrid_ByAccount = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentEventListFind',
	xtype: 'gridpanel',
	pageSize: 200,
	defaultAfterDaysBack: -35,
	defaultBeforeDaysForward: 35,
	includeSubAccounts: false,
	getInvestmentAccountId: function() {
		return this.getWindow().getMainFormId();
	},
	initComponent: function() {
		TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);

		// Hide Columns if option set
		const cm = this.getColumnModel();
		cm.setHidden(cm.findColumnIndex('investmentAccount.label'), (this.includeSubAccounts === false));
	},
	additionalPropertiesToRequest: 'id|eventType.cssStyle|seriesDefinition.id',
	instructions: 'A list all calendar events including separate rows for "deleted" events and "series definitions". "Series Date" identifies series events and can be different from "Event Date" when system calculated series date is changed by the user. Use this section to troubleshoot advanced scheduling.',
	columns: [
		{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
		{
			header: 'Event Type', width: 80, dataIndex: 'eventType.name', filter: {type: 'combo', searchFieldName: 'eventTypeId', url: 'investmentEventTypeListFind.json'},
			renderer: function(v, c, r) {
				const style = r.json.eventType.cssStyle;
				if (TCG.isNotBlank(style)) {
					c.attr = 'style="' + style + '"';
				}
				return v;
			}
		},
		{header: 'Client Account', width: 120, dataIndex: 'investmentAccount.label', hidden: true, filter: false},
		{
			header: 'Summary', width: 150, dataIndex: 'label', filter: {searchFieldName: 'summary'},
			renderer: function(v, c, r) {
				if (TCG.isNotNull(r.data.details)) {
					let text = r.data.details;
					if (text.length > 2000) {
						text = 'Double click to see details';
					}
					return '<div qtip=\'' + Ext.util.Format.htmlEncode(text) + '\'>' + v + '</div>';
				}
				return v;
			}
		},
		{header: 'Details', width: 200, dataIndex: 'details', hidden: true},
		{header: 'Amount', width: 40, dataIndex: 'amount', type: 'currency', useNull: true, positiveInGreen: true, negativeInRed: true},
		{header: 'Grouping Label', width: 50, dataIndex: 'labelForGrouping', hidden: true},
		{header: 'Date', width: 35, dataIndex: 'eventDate', filter: {orEquals: true}},
		{header: 'Series Date', width: 35, dataIndex: 'seriesDate', filter: {orEquals: true}, hidden: true},
		{header: 'MOC', width: 23, dataIndex: 'eventType.marketOnClose', filter: {searchFieldName: 'marketOnClose'}, type: 'boolean'},
		{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'File Attachment(s)', width: 12, tooltip: 'File Attachment(s)', dataIndex: 'fullDocumentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'},
		{
			header: '<div class="recurrence" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'Recurring Event', width: 12, tooltip: 'Recurring Event', dataIndex: 'seriesDefinitionPresent', type: 'boolean',
			renderer: function(v) {
				if (v === true) {
					return TCG.renderActionColumn('recurrence', '', 'Recurring Calendar Event');
				}
				return '';
			}
		},
		{
			header: '<div class="checked" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'Completed', width: 12, tooltip: 'Completed Event', dataIndex: 'completedEvent', type: 'boolean',
			renderer: function(v, c, r) {
				return TCG.renderBoolean(v, r.data.resolution);
			}
		},
		{header: 'Resolution', width: 50, dataIndex: 'resolution', hidden: true}
	],
	editor: {
		detailPageClass: 'Clifton.investment.calendar.EventWindow',
		drillDownOnly: true
	},
	getEventDateStartDefaultDate: function() {
		return new Date().add(Date.DAY, this.defaultAfterDaysBack);
	},
	getEventDateEndDefaultDate: function() {
		return new Date().add(Date.DAY, this.defaultBeforeDaysForward);
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Tag', width: 120, xtype: 'toolbar-combo', name: 'calendarTagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Investment Calendar Tags'}
		];
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('eventDate', {
				'after': this.getEventDateStartDefaultDate(),
				'before': this.getEventDateEndDefaultDate()
			});
		}
		const params = {};

		const tag = TCG.getChildByName(this.getTopToolbar(), 'calendarTagHierarchyId');
		if (TCG.isNotBlank(tag.getValue())) {
			params.categoryName = 'Investment Calendar Tags';
			params.categoryTableName = 'InvestmentEventType';
			params.categoryLinkFieldPath = 'eventType';
			params.categoryHierarchyId = tag.getValue();
		}

		if (this.includeSubAccounts === true) {
			params.investmentAccountIdOrSubAccount = this.getInvestmentAccountId();
		}
		else {
			params.investmentAccountId = this.getInvestmentAccountId();
		}
		params.deletedEvent = false;
		params.nonRecurringEvent = true;
		return params;
	}
});
Ext.reg('investment-calendar-event-grid-by-account', Clifton.investment.calendar.InvestmentCalendarEventGrid_ByAccount);


Clifton.investment.instruction.InstructionGrid = Ext.extend(TCG.grid.GridPanel, {
	showSetupAndGenerateButtons: true, // Adds shortcut to Instruction Definitions Setup (from Tools menu) and Administration tabs (Generate button)
	name: 'investmentInstructionListPopulatedFind',
	xtype: 'gridpanel',
	wikiPage: 'IT/Cash and Trade Settlement Instructions',
	defaultCategoryName: undefined, // If populated, will default category to this, i.e. M2M Counterparty
	// Getter Method is available for dynamic overriding
	getDefaultCategoryName: function() {
		return this.defaultCategoryName;
	},
	defaultTagName: undefined, // If populated, will default the tag to this, i.e. Futures
	// Getter Method is available for dynamic overriding
	getDefaultTagName: function() {
		return this.defaultTagName;
	},
	groupField: 'definition.category.name',
	groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Instructions" : "Instruction"]}',
	viewNames: ['Group by Category', 'Group by Category and Recipient Company', 'Group by Category and Recipient', 'Group by Recipient Company'],
	switchToViewBeforeReload: function(viewName) {
		let grpPropertyName = 'definition.category.name';
		if (viewName === 'Group by Category and Recipient') {
			grpPropertyName = 'categoryRecipientLabel';
		}
		else if (viewName === 'Group by Recipient Company') {
			grpPropertyName = 'recipientCompany.name';
		}
		this.grid.getStore().groupBy(grpPropertyName, false);
	},
	rowSelectionModel: 'multiple',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Category', width: 60, dataIndex: 'definition.category.name', filter: {searchFieldName: 'instructionCategoryName'}, hidden: true, viewNames: ['Group by Recipient Company', 'Group by Recipient']},
		{header: 'Category and Recipient', width: 60, hidden: true, dataIndex: 'categoryRecipientLabel', filter: false, sortable: false},
		{header: 'Cash', width: 30, dataIndex: 'definition.category.cash', type: 'boolean', filter: {searchFieldName: 'cash'}, hidden: true},
		{header: 'Client', width: 30, dataIndex: 'definition.category.client', type: 'boolean', filter: {searchFieldName: 'client'}, hidden: true},
		{header: 'Instruction Definition', width: 150, dataIndex: 'definition.name', filter: {searchFieldName: 'definitionName'}},
		{header: 'Recipient Company', width: 70, dataIndex: 'recipientCompany.name', filter: {searchFieldName: 'recipientCompanyName'}},
		{header: 'Generating', width: 25, dataIndex: 'regenerating', filter: false, sortable: false, type: 'boolean', tooltip: 'Generating indicates whether the instruction is locked as part of the Instruction Generation Process.', hidden: true},
		{header: 'Ready', width: 20, dataIndex: 'allItemsReady', filter: false, sortable: false, type: 'boolean', tooltip: 'Ready indicates that all items in the instruction are ready to be sent.  Ready usually indicates that the item has been booked - but some definition selectors allow other conditions (Booked or (Amount = 0 and Reconciled))'},
		{
			header: 'Status', width: 30, dataIndex: 'status.name',
			filter: {searchFieldName: 'instructionStatusList', type: 'list', options: Clifton.investment.instruction.StatusList},
			renderer: function(v, m, r) {
				return (Clifton.investment.instruction.renderStatus(v, m, r));
			}
		},
		{header: 'Date', width: 30, dataIndex: 'instructionDate'},
		{header: 'Create Date', width: 45, dataIndex: 'createDate'}
	],
	plugins: {ptype: 'gridsummary'},
	getLoadParams: function(firstLoad) {
		const lp = {};
		let catObj;
		if (firstLoad) {
			this.setFilterValue('status.name', ['OPEN', 'ERROR', 'SEND', 'SENDING', 'SENT']);
			this.grid.filters.getFilter('status.name').setActive(true, true);

			const catName = this.getDefaultCategoryName();
			if (catName) {
				catObj = TCG.data.getData('investmentInstructionCategoryByName.json?name=' + catName, this, 'investment.instruction.category.' + catName);
				if (catObj) {
					TCG.getChildByName(this.getTopToolbar(), 'instructionCategoryId').setValue({value: catObj.id, text: catObj.name});
				}
			}
			const tagName = this.getDefaultTagName();
			if (TCG.isNotBlank(tagName)) {
				const tagObj = TCG.data.getData('systemHierarchyByCategoryAndNameExpanded.json?categoryName=Investment Instruction Tags&name=' + tagName, this, 'investment.instruction.tag.' + tagName);
				if (tagObj) {
					TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId').setValue({value: tagObj.id, text: tagObj.name});
				}
			}
			if (this.defaultViewName) {
				this.setDefaultView(this.defaultViewName); // sets it as "checked"
				this.switchToView(this.defaultViewName, true);
			}
		}
		const cat = TCG.getChildByName(this.getTopToolbar(), 'instructionCategoryId');
		if (TCG.isNotBlank(cat.getValue())) {
			lp.instructionCategoryId = cat.getValue();
			if (!catObj) {
				catObj = TCG.data.getData('investmentInstructionCategory.json?id=' + cat.getValue(), this);
			}
		}
		const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
		if (TCG.isNotBlank(tag.getValue())) {
			lp.categoryName = 'Investment Instruction Tags';
			lp.categoryTableName = 'InvestmentInstructionDefinition';
			lp.categoryLinkFieldPath = 'definition';
			lp.categoryHierarchyId = tag.getValue();
		}

		let dateLabel = 'Date';
		if (catObj && catObj.dateLabel) {
			dateLabel = catObj.dateLabel;
		}
		const cm = this.grid.getColumnModel();
		const colIndex = cm.findColumnIndex('instructionDate');
		if (colIndex !== -1) {
			cm.setColumnHeader(colIndex, dateLabel);
		}

		return lp;
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Category', width: 150, xtype: 'toolbar-combo', name: 'instructionCategoryId', url: 'investmentInstructionCategoryListFind.json'},
			{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Investment Instruction Tags'}
		];
	},
	editor: {
		addEnabled: false,
		deleteEnabled: true,
		deleteURL: 'investmentInstructionDelete.json',
		detailPageClass: 'Clifton.investment.instruction.InstructionWindow'
	},
	openActionWindow: function(windowClass, toolBar, gridPanel) {
		const dd = {};
		if (this.defaultCategoryName) {
			const cat = this.defaultCategoryName;
			const cato = TCG.data.getData('investmentInstructionCategoryByName.json?name=' + cat, this, 'investment.instruction.category.' + cat);
			if (cato) {
				dd.categoryId = cato.id;
				dd.categoryName = cato.name;
			}
		}
		TCG.createComponent(windowClass, {
			defaultData: dd,
			openerCt: gridPanel
		});
	},
	configureToolsMenu: function(menu) {
		if (this.showSetupAndGenerateButtons === true) {
			menu.add({
				text: 'Setup',
				tooltip: 'Go to settlement instruction definition setup to configure define instruction definitions.',
				iconCls: 'fax',
				handler: function() {
					TCG.createComponent('Clifton.investment.instruction.InstructionListWindow', {defaultActiveTabName: 'Instruction Definitions'});
				}
			});
		}
	},
	uploadInstructionRevision: function(gp) {
		const sm = this.grid.getSelectionModel();
		if (sm.getCount() === 1) {
			const instruction = sm.getSelected().json;
			const recipientCompany = instruction.recipientCompany;
			TCG.createComponent('Clifton.investment.instruction.InvestmentInstructionRevisionUploadWindow', {
				defaultData: {
					instructionLabel: instruction.categoryRecipientLabel,
					instructionId: instruction.id,
					recipientCompanyName: recipientCompany.name
				},
				openerCt: this,
				defaultIconCls: this.getWindow().iconCls
			});
		}
		else {
			TCG.createComponent('Clifton.investment.instruction.InvestmentInstructionRevisionUploadWindow');
		}
	},
	sendInstructions: function(gridPanel) {
		const grid = this.grid;
		const sm = grid.getSelectionModel();
		const toolbar = gridPanel.getTopToolbar();
		const toolbarWireDate = TCG.getChildByName(toolbar, 'toolbarWireDate');
		if (sm.getCount() === 0) {
			TCG.showError('Please select an instruction to send.', 'No Row(s) Selected');
		}
		else {
			Ext.Msg.confirm('Send selected instructions?', 'Would you like to send all of the selected instruction(s)?', function(a) {
				if (a === 'yes') {
					const ids = [];
					const errorMessages = [];
					const ut = sm.getSelections();
					for (let i = 0; i < ut.length; i++) {
						const record = ut[i].json;
						if (record.status.name === 'OPEN' || record.status.name === 'ERROR') {
							if (record.allItemsReady !== true) {
								errorMessages.push('&#8226; Instruction[' + record.id + '] "' + record.definition.name + '" will only have items sent that are marked as Ready.');
							}
							ids.push(record.id);
						}
						else {
							errorMessages.push('&#8226; Instruction[' + record.id + '] "' + record.definition.name + '" will not be sent because it does not have a OPEN or ERROR status.');
						}
					}

					if (ids.length > 0) {
						const loader = new TCG.data.JsonLoader({
							waitTarget: gridPanel,
							params: {
								instructionIds: ids,
								wireDate: toolbarWireDate.getValue() ? toolbarWireDate.getValue().format('m/d/Y') : undefined
							},
							onLoad: function(record, conf) {
								gridPanel.reload();
							}
						});
						loader.load('investmentInstructionListSend.json');
					}

					if (errorMessages.length > 0) {
						TCG.showError(errorMessages.join('\<br /\>'), 'Error Occurred');
					}
				}
			});
		}
	},
	addFirstToolbarButtons: function(toolBar, gridPanel) {
		toolBar.add({
			iconCls: 'expand-all',
			tooltip: 'Expand or Collapse all accounts',
			scope: this,
			handler: function() {
				gridPanel.grid.collapsed = !gridPanel.grid.collapsed;
				gridPanel.grid.view.toggleAllGroups(!gridPanel.grid.collapsed);
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'View Report',
			tooltip: 'View report for all items in selected instruction.',
			iconCls: 'pdf',
			scope: this,
			handler: function() {
				const grid = this.grid;
				const sm = grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('Please select an instruction.', 'No Row(s) Selected');
				}
				else if (sm.getCount() === 1) {
					TCG.downloadFile('investmentInstructionReportDownload.json?instructionId=' + sm.getSelected().id, null, this);
				}
				else {
					const ids = [];
					const ut = sm.getSelections();

					for (let i = 0; i < ut.length; i++) {
						ids.push(ut[i].json.id);
					}
					TCG.downloadFile('investmentInstructionReportListDownload.json?instructionIds=' + ids, null, this);
				}
			}
		});
		toolBar.add('-');
		if (this.showSetupAndGenerateButtons === true) {
			toolBar.add({
				text: 'Generate...',
				tooltip: 'Generate missing instructions.',
				iconCls: 'run',
				scope: this,
				handler: function() {
					gridPanel.openActionWindow('Clifton.investment.instruction.InstructionGenerateWindow', toolBar, gridPanel);
				}
			});
			toolBar.add('-');
			toolBar.add({xtype: 'displayfield', value: 'Wire Date:&nbsp;&nbsp;', qtip: 'Sets Wire Date on selected instructions.'});
			toolBar.add({name: 'toolbarWireDate', xtype: 'datefield'});
			toolBar.add('-');
		}
		toolBar.add({
			text: 'Send',
			xtype: 'splitbutton',
			iconCls: 'run',
			scope: this,
			handler: function(b) {
				if (!b.hasVisibleMenu()) {
					b.ownerCt.ownerCt.sendInstructions(this);
				}
			},
			menu: {
				items: [
					{
						text: 'Send Instructions',
						iconCls: 'run',
						tooltip: 'Creates an Instruction Run and sends the selected Instructions to the Contacts specified on each Instruction',
						scope: this,
						handler: function(b) {
							b.parentMenu.ownerCt.ownerCt.ownerCt.sendInstructions(this);
						}
					},
					{
						text: 'Upload Revision',
						tooltip: 'Upload an instruction that was manually sent',
						iconCls: 'import',
						scope: this,
						handler: function(b) {
							b.parentMenu.ownerCt.ownerCt.ownerCt.uploadInstructionRevision(this);
						}
					}
				]
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Mark Complete',
			tooltip: 'Mark selected instruction item(s) as completed.',
			iconCls: 'row_checked',
			scope: this,
			handler: function() {
				const grid = this.grid;
				const toolbarWireDate = TCG.getChildByName(toolBar, 'toolbarWireDate');
				const sm = grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('Please select an instruction.', 'No Row(s) Selected');
				}
				else if (sm.getCount() === 1) {
					const instructionId = sm.getSelected().id;
					const loader = new TCG.data.JsonLoader({
						waitTarget: gridPanel,
						params: {
							id: instructionId,
							wireDate: toolbarWireDate.getValue() ? toolbarWireDate.getValue().format('m/d/Y') : undefined
						},
						onLoad: function(record, conf) {
							gridPanel.reload();
						}
					});
					loader.load('investmentInstructionCompleteSave.json');
				}
				else {
					const ids = [];
					const ut = sm.getSelections();
					for (let i = 0; i < ut.length; i++) {
						ids.push(ut[i].json.id);
					}
					const listLoader = new TCG.data.JsonLoader({
						waitTarget: gridPanel,
						params: {
							instructionIds: ids,
							wireDate: toolbarWireDate.getValue() ? toolbarWireDate.getValue().format('m/d/Y') : undefined
						},
						onLoad: function(record, conf) {
							gridPanel.reload();
						}
					});
					listLoader.load('investmentInstructionListCompleteSave.json');
				}
			}
		});
		toolBar.add('-');
	}
});
Ext.reg('investment-instruction-grid', Clifton.investment.instruction.InstructionGrid);

Clifton.investment.instruction.InvestmentInstructionRevisionUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Upload (File Revision)',
	iconCls: 'import',
	modal: true,
	enableShowInfo: false,
	hideApplyButton: true,
	height: 300,
	width: 500,
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		labelWidth: 100,
		instructions: 'Please populate all fields, and click OK to upload.',
		items: [
			{
				fieldLabel: 'External File',
				name: 'file',
				xtype: 'fileuploadfield'
			},
			{
				fieldLabel: 'Instruction',
				name: 'instructionLabel',
				hiddenName: 'instructionId',
				xtype: 'linkfield',
				detailPageClass: 'Clifton.investment.instruction.InstructionWindow',
				detailIdField: 'instructionId',
				qtip: 'The instruction that this revision will be filed under'
			},
			{
				fieldLabel: 'Recipient Company',
				name: 'recipientCompanyName',
				xtype: 'combo',
				url: 'businessCompanyListFind.json',
				displayField: 'label',
				detailPageClass: 'Clifton.business.company.CompanyWindow',
				disableAddNewItem: true,
				qtip: 'The company that received the instruction'
			},
			{
				fieldLabel: 'Custodian Account',
				name: 'custodianAccountName',
				xtype: 'combo',
				url: 'investmentAccountListFind.json?accountType=Custodian',
				displayField: 'label',
				detailPageClass: 'Clifton.investment.account.AccountWindow',
				disableAddNewItem: true,
				qtip: 'The custodian the instruction is for'
			},
			{
				fieldLabel: 'Effective Date',
				xtype: 'datefield',
				name: 'effectiveDate',
				qtip: 'The date the instruction is effective on',
				//Have to use a listener instead of 'value' of 'render' due to our version of extJs as well as this being added/defined within a container.
				listeners: {
					afterrender: function() {
						this.setValue(Clifton.calendar.getBusinessDayFrom(0).format('m/d/Y'));
					}
				}
			}
		],
		getSaveURL: function() {
			return 'integrationInvestmentInstructionRevisionUpload.json';
		}
	}]
});

// Instruction Items for a Specific Entity
Clifton.investment.instruction.InstructionItemGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentInstructionItemListForEntity',
	xtype: 'gridpanel',
	tableName: 'OVERRIDE_ME', // Table name associated with this entity
	defaultCategoryName: undefined, // If populated, will default category to this for generating
	rowSelectionModel: 'multiple',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'InstructionID', width: 15, dataIndex: 'instruction.id', hidden: true},
		{header: 'Category', width: 60, dataIndex: 'instruction.definition.category.name'},
		{header: 'Cash', width: 30, dataIndex: 'instruction.definition.category.cash', type: 'boolean', hidden: true},
		{header: 'Client', width: 30, dataIndex: 'instruction.definition.category.client', type: 'boolean', hidden: true},
		{header: 'Definition', width: 150, dataIndex: 'instruction.definition.name'},
		{header: 'Recipient Company', width: 70, dataIndex: 'instruction.recipientCompany.name'},
		{header: 'Date', width: 30, dataIndex: 'instruction.instructionDate'},
		{
			header: 'Status', width: 40, dataIndex: 'status.name', defaultSortColumn: true, defaultSortDirection: 'DESC',
			renderer: function(v, m, r) {
				return (Clifton.investment.instruction.renderStatus(v, m, r));
			}
		}
	],
	getEntityId: function() {
		return this.getWindow().getMainFormId();
	},
	getInstructionDate: function() {
		return undefined; // Override to pass the date that would default for generating - i.e. for a specific trade, it would be the trade date
	},
	getLoadParams: function(firstLoad) {
		return {
			tableName: this.tableName,
			fkFieldId: this.getEntityId()
		};
	},
	editor: {
		addEnabled: false,
		deleteEnabled: false,
		detailPageClass: 'Clifton.investment.instruction.InstructionWindow',
		getDetailPageId: function(grid, row) {
			return row.json.instruction.id;
		}
	},
	addFirstToolbarButtons: function(toolBar) {
		const gridPanel = this;
		toolBar.add({
			text: 'Generate...',
			tooltip: 'Generate missing instructions.',
			iconCls: 'run',
			scope: this,
			handler: function() {
				gridPanel.openActionWindow('Clifton.investment.instruction.InstructionGenerateWindow', toolBar, gridPanel);
			}
		});
		toolBar.add('-');
	},
	openActionWindow: function(windowClass, toolBar, gridPanel) {
		const tbName = (this.tableName === 'OVERRIDE_ME' ? '' : this.tableName);
		const dd = {};
		dd.tableName = tbName;
		if (this.defaultCategoryName) {
			const cat = this.defaultCategoryName;
			const cato = TCG.data.getData('investmentInstructionCategoryByName.json?name=' + cat, this, 'investment.instruction.category.' + cat);
			if (cato) {
				dd.categoryId = cato.id;
				dd.categoryName = cato.name;
			}
		}
		const iDate = this.getInstructionDate();
		if (iDate) {
			dd.date = TCG.parseDate(iDate).format('Y-m-d 00:00:00');
		}
		TCG.createComponent(windowClass, {
			defaultData: dd,
			openerCt: gridPanel
		});
	},
	configureToolsMenu: function(menu) {
		menu.add({
			text: 'Setup',
			tooltip: 'Go to settlement instruction definition setup to configure define instruction definitions.',
			iconCls: 'fax',
			handler: function() {
				TCG.createComponent('Clifton.investment.instruction.InstructionListWindow', {defaultActiveTabName: 'Instruction Definitions'});
			}
		});
	},
	addToolbarButtons: function(toolBar, gridPanel) {
		toolBar.add({
			text: 'View Report',
			tooltip: 'View report for all items in selected instruction.',
			iconCls: 'pdf',
			scope: this,
			handler: function() {
				const grid = this.grid;
				const sm = grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('Please select an instruction.', 'No Row(s) Selected');
				}
				else if (sm.getCount() !== 1) {
					TCG.showError('Multi-selections are not supported yet.  Please select one row.', 'NOT SUPPORTED');
				}
				else {
					const url = 'investmentInstructionItemReportDownload.json?instructionItemId=' + sm.getSelected().id;
					TCG.downloadFile(url, null, this);
				}
			}
		});
		toolBar.add('-');
	}
});
Ext.reg('investment-instruction-item-grid', Clifton.investment.instruction.InstructionItemGrid);


Clifton.investment.instruction.InstructionGenerateWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Generate',
	iconCls: 'run',
	height: 200,
	width: 500,
	modal: true,
	forceModified: true,

	hideOKButton: true,
	cancelButtonText: 'Close',
	cancelButtonTooltip: 'Close this window',
	applyButtonText: 'Generate',
	applyButtonTooltip: 'Generate Instructions for Selected Category and Date',
	doNotWarnOnCloseModified: true, // use true if don't want to warn when closing a modified window
	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.savedSinceOpen = true;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		// TODO: data binding for partial field updates
		// TODO: concurrent updates validation
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Processing...',
			success: function(form, action) {
				TCG.createComponent('Clifton.core.StatusWindow', {
					defaultData: {status: action.result.data},
					render: function() {
						TCG.Window.superclass.render.apply(this, arguments);
					}
				});
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	},
	items: [{
		xtype: 'formpanel',
		loadValidation: false, // using the form only to get background color/padding
		instructions: 'Select criteria below to filter which instructions and date to process. Processing will create/update corresponding instructions on the specified date.',
		loadDefaultDataAfterRender: true,
		prepareDefaultData: function(defaultData) {
			const dd = defaultData || {};
			if (!dd.date) {
				const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
				dd.date = prevBD.format('Y-m-d 00:00:00');
			}
			return dd;
		},
		items: [
			{name: 'synchronous', value: true, xtype: 'hidden'},
			{name: 'tableName', xtype: 'hidden'},
			{
				fieldLabel: 'Category', name: 'categoryName', hiddenName: 'categoryId', xtype: 'combo', url: 'investmentInstructionCategoryListFind.json',
				beforequery: function(queryEvent) {
					const cmb = queryEvent.combo;
					const f = TCG.getParentFormPanel(cmb).getForm();
					if (TCG.isNotBlank(f.findField('tableName').getValue())) {
						cmb.store.baseParams = {
							tableName: f.findField('tableName').getValue()
						};
					}
				}, allowBlank: false
			},
			{fieldLabel: 'Date', xtype: 'datefield', name: 'date', allowBlank: false}
		],
		getSaveURL: function() {
			return 'investmentInstructionListProcess.json';
		}
	}]
});


Clifton.investment.instruction.InstructionItemGridForEntity = Ext.extend(Clifton.investment.instruction.InstructionGrid, {
	name: 'investmentInstructionItemListForEntity',
	groupField: undefined,
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'InstructionID', width: 15, dataIndex: 'instruction.id', hidden: true},
		{header: 'Category', width: 60, dataIndex: 'instruction.definition.category.name'},
		{header: 'Recipient Company', width: 80, dataIndex: 'instruction.recipientCompany.name'},
		{header: 'Date', width: 50, dataIndex: 'instruction.instructionDate'},
		{
			header: '', width: 15,
			renderer: function(v, args, r) {
				return TCG.renderActionColumn('pdf', '', 'View Report', 'report');
			}
		}
	],
	getLoadParams: function(firstLoad) {
		return {tableName: this.tableName, fkFieldId: this.getEntityId()};
	},
	getEntityId: function() {
		return this.getWindow().getMainFormId();
	},
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					const row = grid.store.data.items[rowIndex];
					if (eventName === 'report') {
						const url = 'investmentInstructionItemReportDownload.json?instructionItemId=' + row.json.id;
						TCG.downloadFile(url, null, grid);
					}
				}
			}
		}
	},
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.investment.instruction.InstructionWindow',
		getDetailPageId: function(gridPanel, row) {
			return row.json.instruction.id;
		}
	}
});
Ext.reg('investment-instruction-item-grid-for-entity', Clifton.investment.instruction.InstructionItemGridForEntity);


Clifton.investment.instruction.renderStatus = function(v, m, r) {
	if (v === 'ERROR') {
		m.css = 'important amountNegative';
	}
	else if (v === 'COMPLETED') {
		m.css = 'amountPositive';
	}
	else {
		m.css = 'amountAdjusted';
	}
	return v;
};


Clifton.investment.instrument.allocation.SecurityAllocationsGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentSecurityAllocationListFind',
	instructions: 'Allocations...', // Dynamically overwritten with Allocation Type & Hierarchy Description
	importTableName: 'InvestmentSecurityAllocation',
	importComponentName: 'Clifton.investment.instrument.upload.SecurityAllocationUploadWindow',
	getImportComponentDefaultData: function() {
		return {
			parentInvestmentSecurity: this.getWindow().getMainForm().formValues,
			applyAsFullAllocationListOnStartDate: 'true'
		};
	},
	viewNames: ['Default View', 'Security View', 'Instrument View', 'Portfolio Details', 'Expanded View'],
	additionalPropertiesToRequest: 'id|investmentSecurity.id|investmentInstrument.id',
	rowSelectionModel: 'multiple',
	columns: [
		{header: 'Parent Security Name', width: 150, dataIndex: 'parentInvestmentSecurity.name', hidden: true, viewNames: ['Portfolio Details'], filter: false, sortable: false},
		{header: 'Parent Security Symbol', width: 150, dataIndex: 'parentInvestmentSecurity.symbol', hidden: true, viewNames: ['Portfolio Details'], filter: false, sortable: false},
		{header: 'Security/Instrument', width: 300, dataIndex: 'allocationLabelExpanded', viewNames: ['Default View'], sortable: false, filter: false},
		{header: 'Security', width: 200, dataIndex: 'investmentSecurity.label', hidden: true, viewNames: ['Security View', 'Expanded View'], filter: {searchFieldName: 'investmentSecurityLabel'}},
		{header: 'Security Name', width: 150, dataIndex: 'investmentSecurity.name', hidden: true, viewNames: ['Portfolio Details'], filter: {searchFieldName: 'investmentSecurityLabel'}},
		{header: 'Security Symbol', width: 150, dataIndex: 'investmentSecurity.symbol', hidden: true, viewNames: ['Portfolio Details'], filter: {searchFieldName: 'investmentSecurityLabel'}},
		{
			header: 'Instrument', width: 200, dataIndex: 'investmentInstrument.label', hidden: true, viewNames: ['Instrument View', 'Expanded View'], filter: {searchFieldName: 'investmentInstrumentLabel'},
			tooltip: 'For cases where a many-to-many security is needed you can optionally select the instrument instead and a current security calculator.'
		},
		// Note: Can't Sort or Filter on Hierarchy because it could be from the security or the instrument
		{header: 'Hierarchy Name', width: 100, dataIndex: 'hierarchy.name', hidden: true, viewNames: ['Portfolio Details'], filter: false, sortable: false},
		{header: 'Hierarchy (Expanded)', width: 100, dataIndex: 'hierarchy.nameExpanded', hidden: true, filter: false, sortable: false},
		{header: 'Primary Exchange', width: 100, dataIndex: 'exchange.exchangeCode', hidden: true, viewNames: ['Expanded View'], filter: false, sortable: false},
		{
			header: 'Override Exchange', width: 100, dataIndex: 'overrideExchange.exchangeCode', hidden: true, viewNames: ['Expanded View'], filter: {searchFieldName: 'overrideExchangeId', type: 'combo', url: 'investmentExchangeListFind.json?compositeExchange=false'},
			tooltip: 'An optional exchange used to override the override a security\'s exchange for use during price lookup.'
		},
		{header: 'CCY Denom', width: 85, dataIndex: 'tradingCurrency.symbol', hidden: true, viewNames: ['Expanded View', 'Portfolio Details'], filter: false, sortable: false},
		{
			header: 'Current Security Calculator', width: 150, dataIndex: 'currentSecurityCalculatorBean.name', hidden: true, viewNames: ['Instrument View', 'Expanded View'], filter: {searchFieldName: 'currencySecurityCalculatorBeanName'},
			tooltip: 'Current Security Calculator defines the behavior of how the <i>current</i> security is selected for a specific date and selected instrument so we know which security price to use.  Used with instrument selection only.  Default behavior is to first check the <b><i>Current Securities</i></b> security group, otherwise the first active security ordered by end date ascending.  <br/><br/>Note: This does not apply if a specific security is selected for the allocation.'
		},
		{header: 'Return Weight', width: 85, dataIndex: 'allocationWeight', type: 'percent', numberFormat: '0,000.0000', summaryType: 'sum', viewNames: ['Portfolio Details']},
		{header: 'Start Date', width: 50, dataIndex: 'startDate', type: 'date', viewNames: ['Default View', 'Security View', 'Instrument View', 'Expanded View']},
		{header: 'End Date', width: 50, dataIndex: 'endDate', type: 'date', viewNames: ['Default View', 'Security View', 'Instrument View', 'Expanded View']},
		{header: 'Exchange Overridden', width: 50, dataIndex: 'exchangeOverridden', type: 'boolean', filter: {searchFieldName: 'isExchangeOverridden'}, tooltip: 'Indicates that the security\'s primary exchange has been overridden with another exchange.'},
		{header: 'Note', width: 200, hidden: true, dataIndex: 'note'}
	],
	plugins: {ptype: 'gridsummary'},
	getDefaultActiveOnDate: async function() {
		return (new Date()).format('m/d/Y');
	},
	getLoadParams: async function(firstLoad) {
		const t = this.getTopToolbar();
		let dateValue;
		const df = TCG.getChildByName(t, 'activeOnDate');
		if (TCG.isNotBlank(df.getValue())) {
			dateValue = (df.getValue()).format('m/d/Y');
		}
		else if (firstLoad === true) {
			dateValue = await this.getDefaultActiveOnDate();
			df.setValue(dateValue);
		}
		const params = {};
		params.parentInvestmentSecurityId = this.getWindow().getMainFormId();
		if (TCG.isNotBlank(df.getValue())) {
			params.activeOnDate = dateValue;
		}
		params.disableNoActiveAllocationsValidationError = true;
		return params;
	},
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({fieldLabel: 'Active on Date', xtype: 'toolbar-datefield', name: 'activeOnDate'});
		return filters;
	},
	addToolbarButtons: function(toolbar, gridPanel) {
		toolbar.add({
			text: 'End Allocation',
			tooltip: 'End the selected allocation. This will set the allocations end date to the current day. This should be used instead of the \'Delete\' option if it is desired to retain a record of the allocation.',
			iconCls: 'remove',
			scope: this,
			handler: function() {
				const grid = this.grid;
				const gp = grid.ownerCt;
				const sm = grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('Please select an allocation to end.', 'No Allocation(s) Selected');
				}
				else {
					Ext.Msg.confirm('End Selected Allocation(s)', 'Would you like to end the selected allocation(s)? This will set the allocation\'s end date to ' + (new Date()).format('m/d/Y'), function(a) {
						if (a === 'yes') {
							const allocations = sm.getSelections().map(selection => ({class: 'com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation', id: selection.id}));
							TCG.data.getDataPromise('investmentSecurityAllocationEnd.json', this, {params: {beanList: JSON.stringify(allocations)}})
								.then(function() {
									gp.reload();
								});
						}
					});
				}
			}
		});
		toolbar.add('-');

		// Doesn't add an extra filter, but added the DD Icon/Message to the toolbar
		// Drag and Drop Support
		toolbar.add({
			xtype: 'drag-drop-container',
			layout: 'fit',
			allowMultiple: false,
			bindToFormLoad: false,
			cls: undefined,
			popupComponentName: 'Clifton.investment.instrument.upload.SecurityAllocationUploadWindow',
			message: '&nbsp;',
			tooltipMessage: 'Drag and drop a file to this grid to upload Security Allocations.',
			// Object to reload - i.e. gridPanel to reload after attaching new file
			getReloadObject: function() {
				return gridPanel;
			},
			getParams: function() {
				const parent = this.ownerCt.ownerCt.getWindow().getMainForm().formValues;
				return {
					parentInvestmentSecurity: parent,
					applyAsFullAllocationListOnStartDate: 'true'
				};
			}
		});
		toolbar.add('-');
	},
	editor: {
		detailPageClass: 'Clifton.investment.instrument.allocation.SecurityAllocationWindow',
		deleteURL: 'investmentSecurityAllocationDelete.json',
		allowToDeleteMultiple: true,
		getDefaultData: function(gridPanel) {
			const grid = this.grid;
			const sm = grid.getSelectionModel();
			if (sm.getCount() === 1) {
				const sa = TCG.data.getData('investmentSecurityAllocation.json?id=' + sm.getSelected().id, gridPanel);
				if (sa) {
					return {
						parentInvestmentSecurity: gridPanel.getWindow().getMainForm().formValues,
						investmentSecurity: sa.investmentSecurity,
						investmentInstrument: sa.investmentInstrument,
						currentSecurityCalculatorBean: sa.currentSecurityCalculatorBean,
						existingId: sa.id,
						existingLabel: sa.allocationLabelWithDates
					};
				}
			}
			return {
				parentInvestmentSecurity: gridPanel.getWindow().getMainForm().formValues
			};
		},
		// Used to Open the Allocation or Security/Instrument Window Directly
		openWindowFromContextMenu: function(grid, rowIndex, screen) {
			const gridPanel = this.getGridPanel();
			const row = grid.store.data.items[rowIndex];
			let clazz = this.detailPageClass;
			let id = undefined;

			if (screen === 'SECURITY_ALLOCATION') {
				id = row.json.id;
				clazz = 'Clifton.investment.instrument.allocation.SecurityAllocationWindow';
			}
			else if (row.json.investmentInstrument && row.json.investmentInstrument.id) {
				id = row.json.investmentInstrument.id;
				clazz = 'Clifton.investment.instrument.InstrumentWindow';
			}
			else if (row.json.investmentSecurity && row.json.investmentSecurity.id) {
				id = row.json.investmentSecurity.id;
				clazz = 'Clifton.investment.instrument.SecurityWindow';
			}
			if (clazz) {
				this.openDetailPage(clazz, gridPanel, id, row);
			}
		}
	},
	listeners: {
		afterrender: function(gridPanel) {
			const fp = this.getWindow().getMainFormPanel();
			const allocTypeWeightLabel = TCG.getValue('instrument.hierarchy.securityAllocationTypeWeightLabel', fp.getForm().formValues);
			const allocType = TCG.getValue('instrument.hierarchy.securityAllocationTypeLabel', fp.getForm().formValues);
			const hierarchyDesc = TCG.getValue('instrument.hierarchy.description', fp.getForm().formValues);
			this.instructions = '<b>' + allocType + ':</b>&nbsp;' + hierarchyDesc;
			const cm = this.getColumnModel();
			const l = cm.getColumnCount();
			for (let i = 0; i < l; i++) {
				const c = cm.getColumnAt(i);
				if (c.dataIndex === 'allocationWeight') {
					cm.setColumnHeader(i, allocTypeWeightLabel);
					break;
				}
			}
			// Can be overridden to show a different view - i.e. market data share price uses "current view" (not available here, but overridden in market data)
			if (this.defaultView) {
				this.setDefaultView(this.defaultView);
			}

			const el = gridPanel.getEl();
			el.on('contextmenu', function(e, target) {
				const g = gridPanel.grid;
				g.contextRowIndex = g.view.findRowIndex(target);
				e.preventDefault();
				if (!g.drillDownMenu) {
					g.drillDownMenu = new Ext.menu.Menu({
						items: [
							{
								text: 'Open Security Allocation', iconCls: 'grid',
								handler: function() {
									gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'SECURITY_ALLOCATION');
								}
							},
							{
								text: 'Open Security/Instrument', iconCls: 'coins',
								handler: function() {
									gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'SECURITY_INSTRUMENT');
								}
							}
						]
					});
				}
				TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
			}, gridPanel);
		}
	}
});
Ext.reg('investment-security-allocations-grid', Clifton.investment.instrument.allocation.SecurityAllocationsGrid);


Clifton.investment.instrument.allocation.SecurityAllocationsRebalanceDatesGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentSecurityAllocationRebalanceList',
	rebalancingSupported: false,
	instructions: 'The following dates are when this security has been rebalanced.  A rebalance is considered the start date of the security, the start date of a new allocation, or the end date + 1 of an existing allocation',
	useBufferView: false, // allow cell wrapping
	columns: [
		{header: 'Date', width: 50, dataIndex: 'rebalanceDate', filter: false, type: 'date', defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: 'Description', width: 300, dataIndex: 'description', filter: false, summaryType: 'count'}
	],
	plugins: {ptype: 'gridsummary'},
	getLoadParams: function(firstLoad) {
		const params = {};
		params.parentInvestmentSecurityId = this.getWindow().getMainFormId();
		params.populateDetails = true;
		return params;
	},
	editor: {
		addEnabled: false,
		deleteEnabled: true,
		getDeleteURL: function() {
			return 'investmentSecurityAllocationRebalanceDelete.json';
		},
		getDeleteParams: function(sm) {
			return {
				securityId: this.getWindow().getMainFormId(),
				rebalanceDate: sm.getSelected().get('rebalanceDate').format('m/d/Y')
			};
		}
	},
	getRebalanceDefaultData: function() {
		const dd = {};
		dd.securityId = this.getWindow().getMainFormId();
		dd.securityLabel = TCG.getValue('label', this.getWindow().getMainForm().formValues);
		return dd;
	},
	addToolbarButtons: function(toolBar, gridPanel) {
		if (this.rebalancingSupported === true) {
			toolBar.add({
				text: 'Add Rebalance',
				tooltip: 'Create a new rebalance',
				iconCls: 'add',
				scope: this,
				handler: function() {
					const dd = this.getRebalanceDefaultData();
					TCG.createComponent('Clifton.investment.instrument.allocation.SecurityAllocationRebalanceCreateWindow', {
						defaultData: dd,
						openerCt: gridPanel
					});
				}
			});
			toolBar.add('-');
			toolBar.add({
				text: 'Rebuild Rebalance Date',
				tooltip: 'Recalculate Base Weight or Shares on selected rebalance date.',
				iconCls: 'run',
				scope: this,
				handler: function() {
					const grid = this.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a rebalance date.', 'No Row(s) Selected');
					}
					else if (sm.getCount() !== 1) {
						TCG.showError('Multi-selections are not supported.  Please select one row.', 'NOT SUPPORTED');
					}
					else {
						const dd = this.getRebalanceDefaultData();
						dd.rebalanceDate = sm.getSelected().get('rebalanceDate').format('Y-m-d 00:00:00');
						TCG.createComponent('Clifton.investment.instrument.allocation.SecurityAllocationRebalanceRecalculateWindow', {
							defaultData: dd,
							openerCt: gridPanel
						});
					}
				}
			});
			toolBar.add('-');
		}
	}
});
Ext.reg('investment-security-allocations-rebalanceDates-grid', Clifton.investment.instrument.allocation.SecurityAllocationsRebalanceDatesGrid);


Clifton.investment.instrument.allocation.SecurityAllocationsEventsGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentSecurityEventListFind',
	xtype: 'gridpanel',
	instructions: 'The following security events apply to securities within the allocations.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Security', width: 100, dataIndex: 'security.label', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Event Type', width: 100, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}},
		{header: 'Additional Security', width: 80, dataIndex: 'additionalSecurity.symbol', filter: {type: 'combo', searchFieldName: 'additionalSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Before Value', width: 60, dataIndex: 'beforeEventValue', type: 'float'},
		{header: 'After Value', width: 60, dataIndex: 'afterEventValue', type: 'float'},
		{header: 'Declare Date', width: 50, dataIndex: 'declareDate', hidden: true},
		{header: 'Ex Date', width: 50, dataIndex: 'exDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: 'Record Date', width: 50, dataIndex: 'recordDate', hidden: true},
		{header: 'Event Date', width: 50, dataIndex: 'eventDate', hidden: true},
		{header: 'Description', width: 50, dataIndex: 'eventDescription'}
	],
	editor: {
		detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow',
		drillDownOnly: true
	},
	getLoadParams: function(firstLoad) {
		const t = this.getTopToolbar();

		if (firstLoad) {
			const startDate = new Date(this.getWindow().getMainForm().formValues.startDate);
			const endDate = (new Date());
			TCG.getChildByName(t, 'startDate').setValue(startDate);
			TCG.getChildByName(t, 'endDate').setValue(endDate);
			this.setFilterValue('exDate', {
				'after': startDate,
				'before': endDate
			});
		}
		return {'parentInvestmentSecurityId': this.getWindow().getMainFormId()};
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{text: 'View security events from ', xtype: 'tbtext'},
			{
				fieldLabel: 'Start Date', xtype: 'toolbar-datefield', name: 'startDate',
				listeners: {
					select: function(field) {
						const start = field.getValue();
						const gp = TCG.getParentByClass(field, Ext.Panel);
						const t = gp.getTopToolbar();
						const end = TCG.getChildByName(t, 'endDate').getValue();
						// these date filters are exclusive normally, but we want inclusive for the end date in this case
						const effectiveEnd = new Date(end);
						effectiveEnd.setDate(effectiveEnd.getDate() + 1);
						gp.setFilterValue('exDate', {
							'after': start,
							'before': effectiveEnd
						});
						gp.reload();
					}
				}
			},
			{text: 'to ', xtype: 'tbtext'},
			{
				fieldLabel: 'End Date', xtype: 'toolbar-datefield', name: 'endDate',
				listeners: {
					select: function(field) {
						const end = field.getValue();
						// these date filters are exclusive normally, but we want inclusive for the end date in this case
						const effectiveEnd = new Date(end);
						effectiveEnd.setDate(effectiveEnd.getDate() + 1);
						const gp = TCG.getParentByClass(field, Ext.Panel);
						const t = gp.getTopToolbar();
						const start = TCG.getChildByName(t, 'startDate').getValue();
						gp.setFilterValue('exDate', {
							'before': effectiveEnd,
							'after': start
						});
						gp.reload();
					}
				}
			}];
	}
});
Ext.reg('investment-security-allocations-events-grid', Clifton.investment.instrument.allocation.SecurityAllocationsEventsGrid);


Clifton.investment.instrument.event.SecurityEventsGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentSecurityEventListFind',
	importTableName: 'InvestmentSecurityEvent',
	xtype: 'gridpanel',
	instructions: 'A security event represents an instance of a specific event type for a specific security. For example, specific stock split, dividend payment, etc .',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'CA ID', width: 35, dataIndex: 'corporateActionIdentifier', type: 'int', numberFormat: '0', useNull: true, tooltip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.'},
		{header: 'Event Type', width: 80, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}},
		{header: 'Event Status', width: 70, dataIndex: 'status.name', renderer: Clifton.investment.instrument.event.renderEventStatus, filter: {type: 'combo', searchFieldName: 'statusId', url: 'investmentSecurityEventStatusListFind.json', showNotEquals: true}},
		{header: 'Description', width: 200, dataIndex: 'eventDescription', hidden: true},
		{header: 'Investment Type', width: 60, dataIndex: 'security.instrument.hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'securityInvestmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}, hidden: true},
		{header: 'Investment Hierarchy', width: 130, dataIndex: 'security.instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'securityInstrumentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}, hidden: true},
		{header: 'Security', width: 100, dataIndex: 'security.label', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Additional Security', width: 70, dataIndex: 'additionalSecurity.label', filter: {type: 'combo', searchFieldName: 'additionalSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Before Value', width: 50, dataIndex: 'beforeEventValue', type: 'float'},
		{header: 'After Value', width: 45, dataIndex: 'afterEventValue', type: 'float'},
		{header: 'Additional Value', width: 55, dataIndex: 'additionalEventValue', type: 'float', useNull: true},
		{header: 'Voluntary', width: 30, dataIndex: 'voluntary', type: 'boolean', tooltip: 'Specifies whether the holder of the security has an option to choose to participate or not participate in this event.'},
		{header: 'Taxable', width: 30, dataIndex: 'taxable', type: 'boolean', tooltip: 'Specifies whether this event is likely subject to additional taxes affecting net payout amounts and booking rules.'},
		{header: 'Order', width: 30, dataIndex: 'bookingOrderOverride', type: 'int', useNull: true, tooltip: 'Optionally overrides the booking order specified on accounting event journal types.'},
		{header: 'Declare Date', width: 45, dataIndex: 'declareDate'},
		{header: 'Ex Date', width: 45, dataIndex: 'exDate'},
		{header: 'Record Date', width: 45, dataIndex: 'recordDate'},
		{header: 'Event Date', width: 45, dataIndex: 'eventDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Additional Date', width: 45, dataIndex: 'additionalDate', hidden: true, tooltip: 'Some events may require additional date (Reset Date for Equity/Interest Leg Payment for Total Return Swaps)'},
		{header: 'Actual Settlement Date', width: 45, dataIndex: 'actualSettlementDate', hidden: true, tooltip: 'Actual settlement date that may differs from contractual settlement date.'},
		{header: 'Event Description', width: 100, dataIndex: 'eventDescription', hidden: true}
	],
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 200, url: 'investmentGroupListFind.json'},
			{fieldLabel: 'Event Type', xtype: 'toolbar-combo', width: 200, url: 'investmentSecurityEventTypeListFind.json', linkedFilter: 'type.name'}
		];
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('eventDate', {
				after: new Date().add(Date.DAY, -15),
				before: new Date().add(Date.DAY, 15)
			});
		}
		const v = TCG.getChildByName(this.getTopToolbar(), 'investmentGroupId').getValue();
		if (v) {
			return {investmentGroupId: v};
		}
	},
	editor: {
		detailPageClass: 'Clifton.investment.instrument.event.SingleSecurityEventWindow',
		getDefaultData: function(gridPanel) {
			return this.savedDefaultData;
		},
		addToolbarAddButton: function(toolBar) {
			const gridPanel = this.getGridPanel();
			toolBar.add(new TCG.form.ComboBox({name: 'eventType', url: 'investmentSecurityEventTypeListFind.json', emptyText: '< Select Event Type >', width: 150, listWidth: 230}));
			toolBar.add(new TCG.form.ComboBox({name: 'eventSecurity', url: 'investmentSecurityListFind.json', emptyText: '< Select Security >', displayField: 'label', width: 150, listWidth: 230, requestedProps: 'instrument.hierarchy.investmentType.name|instrument.hierarchy.investmentTypeSubType.name'}));
			toolBar.add({
				text: 'Add',
				tooltip: 'Add an event for selected event type and security',
				iconCls: 'add',
				scope: this,
				handler: function() {
					const eventType = TCG.getChildByName(toolBar, 'eventType');
					const eventSecurity = TCG.getChildByName(toolBar, 'eventSecurity');
					const eventTypeId = eventType.getValue();
					const securityObject = eventSecurity.getValueObject();
					if (TCG.isBlank(eventTypeId) || TCG.isNull(securityObject)) {
						TCG.showError('You must first select desired Event Type and Security from the lists.');
					}
					else {
						this.savedDefaultData = {
							security: securityObject,
							type: {
								id: eventTypeId,
								name: eventType.lastSelectionText
							},
							eventDescription: (eventType.lastSelectionText === 'Stock Spinoff') ? 'Cash in Lieu' : undefined
						};
						this.openDetailPage(this.getDetailPageClass(), gridPanel);
					}
				}
			});
			toolBar.add('-');
		}
	}
});
Ext.reg('investment-security-events-grid', Clifton.investment.instrument.event.SecurityEventsGrid);


Clifton.investment.instrument.event.SecurityEventPayoutsGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentSecurityEventPayoutListFind',
	xtype: 'gridpanel',
	instructions: 'A single security event may have multiple payouts or elections. All payouts with the same "Election Number" are interdependent and are booked together. Lists all available options for selected event.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'CA ID', width: 30, dataIndex: 'securityEvent.corporateActionIdentifier', filter: {searchFieldName: 'corporateActionIdentifier'}, type: 'int', numberFormat: '0', useNull: true, tooltip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.'},
		{header: 'Event Type', width: 50, dataIndex: 'securityEvent.type.name', filter: {type: 'combo', searchFieldName: 'eventTypeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}},
		{header: 'Event Status', width: 38, dataIndex: 'securityEvent.status.name', renderer: Clifton.investment.instrument.event.renderEventStatus, filter: {type: 'combo', searchFieldName: 'eventStatusId', url: 'investmentSecurityEventStatusListFind.json', showNotEquals: true}},
		{header: 'Security', width: 38, dataIndex: 'securityEvent.security.symbol', filter: {type: 'combo', searchFieldName: 'eventSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},

		{header: 'Type', width: 15, dataIndex: 'payoutType.typeCode', idDataIndex: 'payoutType.id', filter: {type: 'combo', searchFieldName: 'payoutTypeId', displayField: 'name', url: 'investmentSecurityEventPayoutTypeListFind.json'}},
		{header: 'Election #', width: 28, dataIndex: 'electionNumber', type: 'int'},
		{header: 'Payout #', width: 28, dataIndex: 'payoutNumber', type: 'int'},
		{header: 'Payout Type', width: 30, dataIndex: 'payoutType.label', idDataIndex: 'payoutType.id', filter: {type: 'combo', searchFieldName: 'payoutTypeId', url: 'investmentSecurityEventPayoutTypeListFind.json'}, hidden: true},
		{header: 'Payout Security', width: 38, dataIndex: 'payoutSecurity.symbol', filter: {type: 'combo', searchFieldName: 'payoutSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json?active=true'}},

		{header: 'Before Value', width: 35, dataIndex: 'beforeEventValue', type: 'float'},
		{header: 'After Value', width: 33, dataIndex: 'afterEventValue', type: 'float'},
		{header: 'Additional Value', width: 43, dataIndex: 'additionalPayoutValue', type: 'float'},
		{header: 'Additional Value 2', width: 43, dataIndex: 'additionalPayoutValue2', type: 'float', hidden: true},
		{header: 'Proration Rate', width: 43, dataIndex: 'prorationRate', type: 'float', hidden: true},
		{header: 'Additional Date', width: 40, dataIndex: 'additionalPayoutDate'},
		{
			header: 'Fractional Shares', width: 40, dataIndex: 'fractionalSharesMethod', idDataIndex: 'fractionalSharesMethod', filter: {searchFieldName: 'fractionalSharesMethod', type: 'combo', mode: 'local', matchTextInLocalMode: false, store: {xtype: 'arraystore', data: Clifton.investment.instrument.event.FractionalShares}},
			renderer: function(value, metaData, r) {
				const fractionalSharesMethod = TCG.getValue('fractionalSharesMethod', r.json);
				if (TCG.isBlank(fractionalSharesMethod)) {
					return fractionalSharesMethod;
				}
				const fractionalShareEntry = Clifton.investment.instrument.event.FractionalShares.find(valueArray => valueArray[0].includes(fractionalSharesMethod));
				return fractionalShareEntry ? fractionalShareEntry[1] : fractionalShareEntry;
			}
		},
		{header: 'Description', width: 100, dataIndex: 'description', hidden: true},
		{header: 'Default', width: 22, dataIndex: 'defaultElection', type: 'boolean'},
		{header: 'Deleted', width: 22, dataIndex: 'deleted', type: 'boolean'},

		{header: 'Event Date', width: 33, dataIndex: 'securityEvent.eventDate', filter: {searchFieldName: 'eventDate'}, defaultSortColumn: true, defaultSortDirection: 'DESC'}
	],
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 200, url: 'investmentGroupListFind.json'},
			{fieldLabel: 'Event Type', xtype: 'toolbar-combo', width: 200, url: 'investmentSecurityEventTypeListFind.json', linkedFilter: 'securityEvent.type.name'}
		];
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('securityEvent.eventDate', {
				after: new Date().add(Date.DAY, -30),
				before: new Date().add(Date.DAY, 30)
			});
		}
		const v = TCG.getChildByName(this.getTopToolbar(), 'investmentGroupId').getValue();
		if (v) {
			return {investmentGroupId: v};
		}
	},
	editor: {
		detailPageClass: 'Clifton.investment.instrument.event.payout.SecurityEventPayoutWindow',
		drillDownOnly: true
	}
});
Ext.reg('investment-security-event-payouts-grid', Clifton.investment.instrument.event.SecurityEventPayoutsGrid);

Clifton.investment.instrument.event.PayoutGrid = Ext.extend(Clifton.investment.instrument.event.SecurityEventPayoutsGrid, {
	name: 'investmentSecurityEventPayoutListFind',
	instructions: 'A single security event may have multiple payouts or elections. All payouts with the same "Election Number" are interdependent and are booked together. Lists all available options for selected event.',
	columnOverrides: [
		{dataIndex: 'securityEvent.corporateActionIdentifier', hidden: true},
		{dataIndex: 'securityEvent.type.name', hidden: true},
		{dataIndex: 'securityEvent.status.name', hidden: true},
		{dataIndex: 'securityEvent.security.symbol', hidden: true},
		{dataIndex: 'securityEvent.eventDate', hidden: true},
		{dataIndex: 'payoutType.typeCode', hidden: true},
		{dataIndex: 'payoutType.label', hidden: false},
		{dataIndex: 'fractionalSharesMethod', hidden: true},
		{dataIndex: 'description', hidden: false, width: 50}
	],
	getTopToolbarFilters: function(toolbar) {
		return [];
	},
	getLoadParams: function() {
		return {'securityEventId': this.getWindow().getMainFormId()};
	},
	editor: {
		detailPageClass: 'Clifton.investment.instrument.event.payout.SecurityEventPayoutWindow',
		selectedPayoutType: null,
		addToolbarAddButton: function(toolBar) {
			const gridEditor = this;
			const gridPanel = gridEditor.getGridPanel();
			toolBar.add(new TCG.form.ComboBox({
				name: 'payoutTypeCombo', hiddenName: 'referenceOne.id', displayField: 'referenceOne.label', url: 'investmentSecurityEventPayoutTypeAssignmentListFind.json', emptyText: '< Select Payout Type >', width: 150, listWidth: 230, allowBlank: false,
				requestedProps: 'referenceOne.id|referenceOne.name',
				listeners: {
					beforequery: function(qEvent) {
						const store = qEvent.combo.store;
						const eventTypeId = gridPanel.getWindow().getMainForm().formValues.type.id;
						store.setBaseParam('eventTypeId', eventTypeId);
					},
					select: function(combo, record, index) {
						if (TCG.isNotBlank(record)) {
							gridEditor.selectedPayoutType = record.json.referenceOne;
						}
						else {
							gridEditor.selectedPayoutType = null;
						}
					}
				}
			}));

			toolBar.add({
				text: 'Add',
				tooltip: 'Add a Security Payout for the event.',
				iconCls: 'add',
				scope: this,
				handler: function() {
					if (TCG.isBlank(this.selectedPayoutType)) {
						TCG.showError('Please select a payout type before proceeding.');
					}
					else {
						this.openDetailPage(this.getDetailPageClass(), gridPanel);
					}
				}
			});
			toolBar.add('-');
		},
		getDefaultData: function(gridPanel, row, className, itemText) {
			const defaultData = {
				'securityEvent': gridPanel.getWindow().getMainForm().formValues,
				'payoutType': this.selectedPayoutType
			};

			return defaultData;
		}
	}
});
Ext.reg('investment-event-payout-grid', Clifton.investment.instrument.event.PayoutGrid);

Clifton.investment.instrument.event.ClientElectionsGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentSecurityEventClientElectionListFind',
	xtype: 'gridpanel',
	instructions: 'For corporate actions that offer elections, identifies specific payout elections that each client that held corresponding position made. Election Quantity is usually equal to the total quantity held. However, it is possible to split existing position across more than one election.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Client Account', width: 200, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Election #', width: 50, dataIndex: 'electionNumber', type: 'int'},
		{header: 'Election Qty', width: 50, dataIndex: 'electionQuantity', type: 'float', useNull: true, tooltip: 'Empty Election Quantity, means total quantity held by the client (most common election).'},
		{header: 'Election Value', width: 50, dataIndex: 'electionValue', type: 'float', useNull: true, tooltip: 'Election Value will be used to allow clients to declare a bid on Dutch Auctions for consideration by the Event Offeror.'}
	],
	editor: {
		detailPageClass: 'Clifton.investment.instrument.event.payout.SecurityEventClientElectionWindow',
		getDefaultData: function(gridPanel, row, className, itemText) {
			const formValues = gridPanel.getWindow().getMainForm().formValues;
			if (TCG.isNotBlank(formValues.securityEvent)) {
				return {
					securityEvent: formValues.securityEvent,
					electionNumber: formValues.electionNumber,
					dtcOnly: formValues.dtcOnly
				};
			}
			return {
				securityEvent: formValues
			};
		}
	},
	getLoadParams: function() {
		if (TCG.isNotBlank(this.getWindow().defaultData.securityEvent)) {
			return {'securityEventId': this.getWindow().defaultData.securityEvent.id};
		}
		return {'securityEventId': this.getWindow().getMainFormId()};
	}
});
Ext.reg('investment-event-client-elections-grid', Clifton.investment.instrument.event.ClientElectionsGrid);

Clifton.investment.account.group.InvestmentAccountGroupGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentAccountGroupListFind',
	xtype: 'gridpanel',
	instructions: 'Account group is a user defined collection of investment accounts that can be used by various parts of the system. The following Investment Account Groups are defined in the system.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Group Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
		{header: 'Group Alias', width: 50, dataIndex: 'groupAlias', hidden: true},
		{header: 'Group Type', width: 50, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', displayField: 'name', url: 'investmentAccountGroupTypeListFind.json'}},
		{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
		{header: 'Account Type', width: 50, dataIndex: 'accountType.name', filter: {type: 'combo', searchFieldName: 'accountTypeId', displayField: 'name', url: 'investmentAccountTypeListFind.json'}},
		{header: 'Modify Condition', width: 110, dataIndex: 'entityModifyCondition.name', filter: {searchFieldName: 'entityModifyCondition'}},
		{header: 'System Defined', width: 40, dataIndex: 'systemDefined', type: 'boolean'},
		{header: 'System Managed', width: 40, dataIndex: 'systemManaged', type: 'boolean'}
	],
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Investment Account Group Tags'},
			{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
		];
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		const params = {};
		const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
		if (TCG.isNotBlank(tag.getValue())) {
			params.categoryName = 'Investment Account Group Tags';
			params.categoryHierarchyId = tag.getValue();
		}
		this.configureAdditionalLoadParams(params);
		return params;
	},
	configureAdditionalLoadParams: function(params) {
		if (this.groupTypeName) {
			params.typeName = this.groupTypeName;
		}
	},
	editor: {
		detailPageClass: 'Clifton.investment.account.AccountGroupWindow',
		getDefaultData: function(gridPanel) { // defaults groupItem for the detail page
			if (gridPanel.groupTypeName) {
				const groupType = TCG.data.getData('investmentAccountGroupTypeListFind.json?name=' + gridPanel.groupTypeName, gridPanel, 'investment.account.group.' + gridPanel.groupTypeName);
				if (groupType && groupType[0]) {
					return {type: groupType[0]};
				}
			}
			return null;
		}
	}
});
Ext.reg('investment-account-group-grid', Clifton.investment.account.group.InvestmentAccountGroupGrid);


Clifton.investment.manager.custodian.InvestmentManagerCustodianTransactionGrid = Ext.extend(TCG.grid.GridPanel, {
	xtype: 'gridpanel',
	name: 'investmentManagerCustodianTransactionListFind.json',
	topToolbarSearchParameter: 'searchPattern',
	pageSize: 100,
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Date', width: 25, dataIndex: 'transactionDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: 'Amount', width: 25, dataIndex: 'transactionAmount', type: 'currency', summaryType: 'sum', negativeInRed: true},
		{header: 'Description', width: 100, dataIndex: 'description'},
		{header: 'Custodian Number', width: 50, dataIndex: 'custodianAccount.number', hidden: true, filter: {searchFieldName: 'custodianAccountNumber'}},
		{header: 'Issued By', width: 50, dataIndex: 'custodianAccount.company.name', hidden: true, filter: {searchFieldName: 'custodianCompanyName'}},
		{header: 'Holding Account', width: 50, dataIndex: 'custodianAccount.holdingAccount.label', hidden: true, filter: false},
		{header: 'Source Data Identifier', width: 50, dataIndex: 'sourceDataIdentifier', hidden: true},
		{
			header: '',
			width: 5,
			renderer: function(v, args, r) {
				return TCG.renderActionColumn('grid', '', 'Open Import Run', 'OPEN_IMPORT_RUN');
			}
		}
	],
	gridConfig: {
		listeners: {
			rowclick: function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					if (eventName && TCG.startsWith(eventName, 'OPEN_IMPORT_RUN')) {
						grid.ownerGridPanel.editor.openWindowFromContextMenu(grid, rowIndex, eventName);
					}
				}
			}
		}
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		if (firstLoad) {
			// default to last 7 days of balances
			this.setFilterValue('transactionDate', {'after': new Date().add(Date.DAY, -7)});
		}
	},
	isPagingEnabled: function() {
		return true;
	},
	editor: {
		addEnabled: false,
		deleteEnabled: false,
		openWindowFromContextMenu: function(grid, rowIndex, screen) {
			const gridPanel = this.getGridPanel();
			const row = grid.store.data.items[rowIndex];
			let clazz = this.detailPageClass;
			let id = undefined;
			const defaultActiveTabName = undefined;

			if (screen === 'OPEN_IMPORT_RUN') {
				id = row.json.sourceDataIdentifier;
				clazz = 'Clifton.integration.definition.ImportRunEventWindow';
			}

			if (clazz) {
				this.openDetailPage(clazz, gridPanel, id, row, null, defaultActiveTabName);
			}
		}
	}
});
Ext.reg('investment-manager-custodian-transaction-grid', Clifton.investment.manager.custodian.InvestmentManagerCustodianTransactionGrid);

Clifton.investment.specificity.InvestmentSpecificityValuesCombo = Ext.extend(TCG.form.ComboBox, {
	url: 'investmentSpecificityEntryPropertyListForEntryFind.json',
	valueField: 'value',
	displayField: 'text',
	root: 'data',
	detailPageClass: 'REQUIRED_DETAIL_PAGE_CLASS',
	definitionName: 'REQUIRED_DEFINITION_NAME',
	fieldName: 'REQUIRED_FIELD_NAME',
	filterParams: function() {
		return {};
	},
	params: function() {
		const p = {
			'definition': this.definitionName,
			'field': this.fieldName
		};
		const q = this.filterParams();
		if (q) {
			Ext.apply(p, q);
		}
		return p;
	},
	reload: function() {
		this.resetStore();
	},
	beforequery: function(queryEvent) {
		queryEvent.combo.store.baseParams = this.params();
	}
});
Ext.reg('investment-specificity-values-combo', Clifton.investment.specificity.InvestmentSpecificityValuesCombo);

/**
 * The grid for viewing and modifying Investment Specificity Entry objects.
 */
Clifton.investment.specificity.InvestmentSpecificityEntryGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentSpecificityEntryListFind',
	loadURL: 'investmentSpecificityEntryListFind.json?includeVirtualPropertyList=true',
	xtype: 'gridpanel',
	instructions: 'Use this section to define entries for corresponding fields using the specificity scope. This section is usually used to override standard or define custom values for Investment Security attributes. For each definition, the most specific values for each field will be used.',
	additionalPropertiesToRequest: 'virtualPropertyList.text',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Specificity Definition', width: 70, dataIndex: 'field.definition.name', filter: {type: 'combo', searchFieldName: 'definitionId', url: 'investmentSpecificityDefinitionListFind.json', displayField: 'name'}, defaultSortColumn: true},
		{header: 'Definition Description', width: 100, dataIndex: 'field.definition.description', filter: {searchFieldName: 'definitionDescription'}, hidden: true},
		{header: 'Specificity Field', width: 45, dataIndex: 'field.label', filter: {searchFieldName: 'field'}},
		{header: 'Field Description', width: 100, dataIndex: 'field.description', filter: {searchFieldName: 'fieldDescription'}, hidden: true},
		{header: 'Currency Denomination', width: 30, dataIndex: 'currencyDenomination.label', filter: {searchFieldName: 'currencyDenomination'}, hidden: true},
		{header: 'Issuer Company', width: 50, dataIndex: 'issuerCompany.label', filter: {searchFieldName: 'issuerCompany'}, hidden: true},
		{header: 'Investment Type', width: 45, dataIndex: 'investmentType.name', filter: {searchFieldName: 'investmentType'}},
		{header: 'Sub Type', width: 54, dataIndex: 'investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}},
		{header: 'Sub Type 2', width: 54, dataIndex: 'investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}},
		{header: 'Investment Hierarchy', width: 140, dataIndex: 'hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'hierarchyId', url: 'investmentInstrumentHierarchyListFind.json', displayField: 'labelExpanded', queryParam: 'labelExpanded', listWidth: 600}},
		{header: 'Instrument', width: 50, dataIndex: 'instrument.label', filter: {type: 'combo', searchFieldName: 'instrumentId', url: 'investmentInstrumentListFind.json', displayField: 'label'}, hidden: true}
	],

	// Configurable properties
	numberOfValuesUnhidden: 2,
	maxNumberOfValuesToDisplay: 5,

	showSecurityFilter: true,
	securityScope: null,
	showInstrumentFilter: true,
	instrumentScope: null,
	showSearchPatternFilter: true,

	defaultDefinitionId: null,
	getDefaultDefinitionId: function() {
		return this.defaultDefinitionId;
	},

	defaultDefinitionName: null,
	getDefaultDefinitionName: function() {
		return this.defaultDefinitionName;
	},

	listeners: {
		afterrender: function(panel) {
			let colModel;
			let definitionColumn;

			// Modifications when static definition is present
			if (TCG.isNotNull(panel.getDefaultDefinitionId()) || TCG.isNotNull(panel.getDefaultDefinitionName())) {
				// Disable definition column
				colModel = panel.grid.getColumnModel();
				definitionColumn = colModel.getColumnsBy(function(col, index) {
					return col.dataIndex === 'field.definition.name';
				})[0];
				colModel.setHidden(colModel.getIndexById(definitionColumn.id), true);

				// Use definition property labels
				TCG.data.getDataPromise('investmentSpecificityEntryPropertyTypeListFind.json', panel, {
					params: {
						definitionId: panel.getDefaultDefinitionId(),
						definition: panel.getDefaultDefinitionName(),
						orderBy: 'order:ASC'
					}
				}).then(function(propertyTypes) {
					const columnModel = panel.grid.colModel;
					let columnIndex, i, type;
					// Update label or set hidden status for all property types
					for (i = 0; i < panel.maxNumberOfValuesToDisplay; i++) {
						columnIndex = columnModel.getIndexById('value' + (i + 1));
						if (columnIndex < 0) {
							break;
						}
						type = propertyTypes[i];
						if (type) {
							columnModel.setColumnHeader(columnIndex, type.label);
						}
						else {
							columnModel.setHidden(columnIndex, true);
						}
					}
				});
			}
		}
	},

	editor: {
		detailPageClass: 'Clifton.investment.specificity.SpecificityEntryWindow',
		allowToDeleteMultiple: true,
		getDefaultData: function(gridPanel) {
			let defaultData = undefined;
			if (TCG.isNotNull(gridPanel.getDefaultDefinitionId())) {
				defaultData = {field: {}};
				defaultData.field['definition'] = {
					id: gridPanel.getDefaultDefinitionId(),
					label: gridPanel.getDefaultDefinitionName()
				};
			}
			return defaultData;
		}
	},

	initComponent: function() {
		// Configure property value columns
		let i, valueCol;
		this.columns = TCG.clone(this.columns) || [];
		for (i = 0; i < this.maxNumberOfValuesToDisplay; i++) {
			valueCol = {
				header: 'Value ' + (i + 1), id: 'value' + (i + 1),
				dataIndex: 'virtualPropertyList[' + i + '].text', width: 50,
				sortable: false, filter: false, hidden: (i >= this.numberOfValuesUnhidden)
			};
			this.columns.push(valueCol);
		}
		TCG.callSuper(this, 'initComponent', arguments);
	},

	topToolbarSearchParameter: 'searchPattern',
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		let securityFilter, instrumentFilter;
		const filterShared = {
			width: 150, disableAddNewItem: true,
			mutuallyExclusiveToolbarFields: [],
			listeners: {
				select: evaluateToolbarFieldStates,
				blur: evaluateToolbarFieldStates
			}
		};

		// Create security filter if enabled
		if (this.showSecurityFilter) {
			securityFilter = Ext.applyIf({
				fieldLabel: 'Security Scope', xtype: 'toolbar-combo',
				name: 'investmentSecurity', qtip: 'Select a security to view all entries which apply to that security.',
				displayField: 'label', url: 'investmentSecurityListFind.json',
				detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
				mutuallyExclusiveToolbarFields: ['searchPattern', 'investmentInstrument']
			}, filterShared);

			// Apply pre-selected security if specified
			if (this.securityScope) {
				Ext.apply(securityFilter, {
					store: {xtype: 'jsonstore', root: 'data', data: {data: this.securityScope}, fields: Object.getOwnPropertyNames(this.securityScope)},
					mode: 'local', url: null, value: this.securityScope.id, readOnly: true
				});
			}
			filters.push(securityFilter);
		}

		// Create instrument filter if enabled
		if (this.showInstrumentFilter) {
			instrumentFilter = Ext.applyIf({
				fieldLabel: 'Instrument Scope', xtype: 'toolbar-combo',
				name: 'investmentInstrument', qtip: 'Select an instrument to view all entries which apply to that instrument.',
				displayField: 'label', url: 'investmentInstrumentListFind.json',
				detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
				mutuallyExclusiveToolbarFields: ['searchPattern', 'investmentSecurity']
			}, filterShared);

			// Apply pre-selected instrument if specified
			if (this.instrumentScope) {
				Ext.apply(instrumentFilter, {
					store: {xtype: 'jsonstore', root: 'data', data: {data: this.instrumentScope}, fields: Object.getOwnPropertyNames(this.instrumentScope)},
					mode: 'local', url: null, value: this.instrumentScope.id, readOnly: true
				});
			}
			filters.push(instrumentFilter);
		}

		// Add search pattern filter
		filters.push(Ext.applyIf({
			fieldLabel: this.showSearchPatternFilter ? 'Search' : '',
			xtype: 'toolbar-textfield',
			name: 'searchPattern',
			mutuallyExclusiveToolbarFields: ['investmentSecurity', 'investmentInstrument'],
			hidden: !this.showSearchPatternFilter
		}, filterShared));

		return filters;

		// Evaluate mutually exclusive fields
		function evaluateToolbarFieldStates() {
			let i, fieldName;
			let field;
			for (i = 0; i < this.mutuallyExclusiveToolbarFields.length; i++) {
				fieldName = this.mutuallyExclusiveToolbarFields[i];
				field = TCG.getChildByName(toolbar, fieldName);
				if (field) {
					field.setDisabled(!!this.getValue());
				}
			}
		}
	},

	getTopToolbarInitialLoadParams: function(firstLoad) {
		// Apply the entity ID applicability filters
		const params = {};
		let entity;
		if (this.showSecurityFilter) {
			entity = TCG.getChildByName(this.getTopToolbar(), 'investmentSecurity').getValueObject();
			if (entity) {
				params['appliesToSecurityId'] = entity.id;
			}
		}
		if (this.showInstrumentFilter) {
			entity = TCG.getChildByName(this.getTopToolbar(), 'investmentInstrument').getValueObject();
			if (entity) {
				params['appliesToInstrumentId'] = entity.id;
			}
		}
		return params;
	},

	getLoadParams: function(firstLoad) {
		// Apply default load parameters
		const loadParams = TCG.callSuper(this, 'getLoadParams', arguments) || {};
		const definitionId = this.getDefaultDefinitionId();
		const definitionName = this.getDefaultDefinitionName();
		Ext.apply(loadParams, {definitionId: definitionId, definition: definitionName});
		return loadParams;
	},

	addToolbarButtons: function(toolbar) {
		toolbar.add({
			text: 'Copy',
			tooltip: 'Create a new item by using a selected entry as a template',
			iconCls: 'copy',
			scope: this,
			handler: function() {
				const gridPanel = this;
				const sm = gridPanel.grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('Please select a row to be copied.', 'No Row(s) Selected');
				}
				else if (sm.getCount() !== 1) {
					TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
				}
				else {
					TCG.data.getDataPromise('investmentSpecificityEntry.json', gridPanel, {params: {id: sm.getSelected().id}})
						.then(function(entryTemplate) {
							const params = Ext.applyIf({newBean: true}, entryTemplate);
							// clear the ID of the entry and its properties
							delete params['id'];
							if (params.propertyList && params.propertyList.length > 0) {
								let i = 0;
								const len = params.propertyList.length;
								for (; i < len; i++) {
									delete params.propertyList[i].id;
								}
							}
							TCG.createComponent(gridPanel.editor.detailPageClass, {
								defaultData: params,
								openerCt: gridPanel,
								defaultIconCls: gridPanel.getWindow().iconCls
							});
						});
				}
			}
		}, '-');
	}
});
Ext.reg('investment-specificity-entry-grid', Clifton.investment.specificity.InvestmentSpecificityEntryGrid);

/**
 * The field grid to display with the definition window or the setup window.
 */
Clifton.investment.specificity.InvestmentSpecificityFieldsGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentSpecificityFieldListFind',
	xtype: 'gridpanel',
	instructions: 'The fields which are available to specificity entries for this definition. When determining the most specific entries for a given entity, up to one entry may be returned for each of these fields.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Definition', dataIndex: 'definition.label', width: 80, filter: {type: 'combo', url: 'investmentSpecificityDefinitionListFind.json', searchFieldName: 'definitionId'}},
		{header: 'System Column', width: 50, dataIndex: 'label', filter: {searchFieldName: 'field'}},
		{header: 'Field Name', width: 50, dataIndex: 'name'},
		{header: 'Field Description', width: 100, dataIndex: 'description', filter: {searchFieldName: 'fieldDescription'}},
		{header: 'Table', width: 50, dataIndex: 'systemColumn.table.label'},
		{header: 'System Defined', dataIndex: 'systemDefined', width: 45, type: 'boolean', hidden: true}
	]
});
Ext.reg('investment-specificity-fields-grid', Clifton.investment.specificity.InvestmentSpecificityFieldsGrid);

/**
 * The window to display the list of Investment Specificity Entry objects.
 */
Clifton.investment.specificity.InvestmentSpecificityEntryListWindow = Ext.extend(TCG.app.Window, {
	titlePrefix: 'Investment Specificity',
	iconCls: 'grouping',
	width: 1200,
	height: 400,
	items: [{
		xtype: 'investment-specificity-entry-grid',
		initComponent: function() {
			// Expose configurations to window instantiator
			const props = ['securityScope', 'instrumentScope', 'showSecurityFilter', 'showInstrumentFilter', 'showSearchPatternFilter',
				'defaultDefinitionId', 'defaultDefinitionName'];
			let i, prop;
			const win = this.getWindow();
			for (i = 0; i < props.length; i++) {
				prop = props[i];
				if (TCG.isNotNull(win[prop])) {
					this[prop] = win[prop];
				}
			}
			TCG.callOverridden(this, 'initComponent', arguments);
		}
	}],
	init: function() {
		// Prepend definition name to title if present
		if (this.defaultDefinitionName || this.defaultDefinitionId) {
			this.title = (this.defaultDefinitionName || this.defaultDefinitionId) + (this.title ? (' - ' + this.title) : '');
		}
		return TCG.callSuper(this, 'init', arguments);
	}
});

/**
 *  Handle applying specificity actions to the investment windows.
 */
Clifton.investment.specificity.SpecificityHandler = {
	specificityInstrument: {
		param: 'instrumentId',
		definition: 'Investment Instrument Columns',
		url: 'investmentSpecificityEntryListForInstrument.json'
	},
	specificitySecurity: {
		param: 'securityId',
		definition: 'Investment Security Columns',
		url: 'investmentSpecificityEntryListForSecurity.json'
	},
	applySpecificityOverrides: function(panel, type) {
		const specificityHandler = this;
		const mainFormId = panel.getWindow().getMainFormId();
		let url = type.url;
		const params = {
			requestedMaxDepth: 4,
			requestedPropertiesToExclude: 'field.definition',
			definitionName: type.definition
		};
		if (mainFormId) {
			params[type.param] = mainFormId;
		}
		else {
			const dd = panel.getDefaultData(panel.getWindow());
			if (dd && dd.hasOwnProperty('hierarchy') && dd.hierarchy.hasOwnProperty('id')) {
				params['hierarchyId'] = dd.hierarchy.id;
				url = 'investmentSpecificityEntryListForHierarchy.json';
			}
			else if (dd && dd.hasOwnProperty('instrument') && dd.instrument.hasOwnProperty('id')) {
				params['instrumentId'] = dd.instrument.id;
				url = 'investmentSpecificityEntryListForInstrument.json';
			}
		}
		const loader = new TCG.data.JsonLoader({
			params: params,
			onLoad: function(record, conf) {
				if (record.rows) {
					const entries = record.rows;
					for (let index = 0; index < entries.length; index++) {
						const beanPropertyNames = specificityHandler.getBeanPropertyNames(entries[index]);
						for (let b = 0; b < beanPropertyNames.length; b++) {
							if (specificityHandler.hasOwnProperty(beanPropertyNames[b])) {
								specificityHandler[beanPropertyNames[b]](panel, entries[index]);
							}
						}
					}
				}
			}
		});
		loader.load(url, panel);
	},
	fieldLabelOverride: function(formPanel, propertyEntry) {
		const specificityHandler = this;
		const fieldName = specificityHandler.getFieldName(propertyEntry);
		const property = specificityHandler.getPropertyForBeanName(propertyEntry, 'fieldLabelOverride');
		if (formPanel && fieldName && property && property.hasOwnProperty('text')) {
			specificityHandler.setFieldLabel(formPanel, fieldName, property.text);
		}
	},
	fieldValueApplicability: function(formPanel, propertyEntry) {
		const specificityHandler = this;
		const fieldName = specificityHandler.getFieldName(propertyEntry);
		const property = specificityHandler.getPropertyForBeanName(propertyEntry, 'fieldValueApplicability');
		const field = formPanel.getForm().findField(fieldName, true);
		if (field && fieldName && property && property.hasOwnProperty('value')) {
			switch (property.value) {
				case 'REQUIRED':
					field.allowBlank = false;
					break;
				case 'OPTIONAL':
					field.allowBlank = true;
					break;
				case 'NOT_APPLICABLE':
					specificityHandler.removeField(formPanel, fieldName);
					break;
				default:
					break;
			}
		}
	},
	setFieldLabel: function(formPanel, fieldName, label) {
		let field = formPanel.getForm().findField(fieldName, true);
		if (!field) {
			field = TCG.getChildByName(formPanel, fieldName);
		}
		if (field) {
			if (field.setFieldLabel) {
				field.setFieldLabel(label + ':');
			}
			else if (field.setText) {
				field.setText(label + ':');
			}
		}
	},
	removeField: function(formPanel, fieldName) {
		let field = formPanel.items.get(fieldName);
		if (!field) {
			formPanel.items.each(function(f) {
				if (!f.isFormField) {
					return;
				}
				if (f.dataIndex === fieldName || f.id === fieldName || f.getName() === fieldName || f.name === fieldName) {
					field = f;
					return false;
				}
				if (f.getName() && f.getName().indexOf(fieldName + '.') === 0) {
					field = f;
					return false;
				}
				if (f.items) {
					const items = f.items;
					for (let i = 0; i < items.length; i++) {
						const o = items.get ? items.get(i) : items[i];
						if (o.name === fieldName) {
							field = f;
							return false;
						}
					}
				}
			});
		}
		if (field) {
			if (TCG.isBlank(field.getValue())) {
				formPanel.removeField(field);
			}
			else {
				const handler = function(fld) {
					if (TCG.isNotBlank(fld.getValue())) {
						fld.markInvalid('The column is intended to be "Not Applicable" via Investement Specificity, but cannot be hidden because it has a value set.');
					}
				};
				field.mon(field, 'valid', handler);
				field.validate();
			}
		}
	},
	getFieldName: function(propertyEntry) {
		return propertyEntry.field.systemColumn.beanPropertyName;
	},
	getPropertyForBeanName: function(propertyEntry, beanPropertyName) {
		if (propertyEntry.hasOwnProperty('propertyList')) {
			for (let index = 0; index < propertyEntry.propertyList.length; index++) {
				const property = propertyEntry.propertyList[index];
				if (property && property.hasOwnProperty('type') && property.type.hasOwnProperty('entryBeanPropertyName') && property.type.entryBeanPropertyName === beanPropertyName) {
					return property;
				}
			}
		}
	},
	getBeanPropertyNames: function(propertyEntry) {
		const beanPropertyNames = [];
		if (propertyEntry.hasOwnProperty('propertyList')) {
			for (let index = 0; index < propertyEntry.propertyList.length; index++) {
				const property = propertyEntry.propertyList[index];
				beanPropertyNames.push(property.parameter.entryBeanPropertyName);
			}
		}
		return beanPropertyNames;
	}
};

Clifton.investment.account.InvestmentAccountMappingGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentAccountMappingListFind',
	xtype: 'gridpanel',
	instructions: 'Shows investment accounts with mapped values and the purpose for that mapping.',
	topToolbarSearchParameter: 'searchPattern',
	requestIdDataIndex: true,
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Account Number', width: 50, dataIndex: 'investmentAccount.number', idDataIndex: 'investmentAccount.id', filter: {type: 'combo', searchFieldName: 'investmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json', autoLoad: 'true'}, defaultSortColumn: true},
		{header: 'Our Account', width: 22, dataIndex: 'investmentAccount.type.ourAccount', type: 'boolean'},
		{header: 'Purpose Name', width: 50, dataIndex: 'investmentAccountMappingPurpose.name', filter: {searchFieldName: 'mappingPurposeName'}},
		{header: 'Purpose Description', width: 150, dataIndex: 'investmentAccountMappingPurpose.description', filter: {searchFieldName: 'mappingPurposeDescription'}},
		{header: 'Mapped Value', width: 50, dataIndex: 'fieldValue'}
	],
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPatternControl'}
		];
	},
	getLoadParams: function(firstLoad) {
		const params = {};

		const searchPatternControl = TCG.getChildByName(this.getTopToolbar(), 'searchPatternControl');
		if (TCG.isNotBlank(searchPatternControl.getValue())) {
			params[this.topToolbarSearchParameter] = searchPatternControl.getValue();
		}

		return params;
	},
	editor: {
		detailPageClass: 'Clifton.investment.account.mapping.AccountMappingWindow',
		getDefaultData: function(gridPanel) {
			return {investmentAccount: gridPanel.getWindow().getMainForm().formValues};
		}
	}
});
Ext.reg('investment-account-mapping-grid', Clifton.investment.account.InvestmentAccountMappingGrid);

Clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetGridPlugins = [];

Clifton.investment.account.InvestmentAccountSecurityTargetGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentAccountSecurityTargetListFind',
	instructions: 'The following security targets are associated with this client account.',
	additionalPropertiesToRequest: 'clientInvestmentAccount.serviceProcessingType',
	requestIdDataIndex: true,
	postWarningMsg: undefined,
	plugins: Clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetGridPlugins,
	getTopToolbarFilters: function(toolbar) {
		const gridPanel = this;

		if (this.getWindow().getMainFormId) {
			toolbar.add({
				text: 'Setup',
				tooltip: 'Account Setup',
				iconCls: 'account',
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Portfolio Setup',
							handler: function() {
								gridPanel.openCustomFieldWindow('Portfolio Setup');
							}
						},
						{
							text: 'Performance Summary Setup',
							handler: function() {
								gridPanel.openCustomFieldWindow('Performance Summary Fields');
							}
						}
					]
				})
			});
			toolbar.add('-');
		}
		return [
			{fieldLabel: 'Holding Account', xtype: 'toolbar-combo', name: 'holdingInvestmentAccount.label', hiddenName: 'holdingInvestmentAccount.id', width: 180, url: 'investmentAccountListFind.json?ourAccount=false', displayField: 'label', qtip: 'Filters the security targets by an investment account that is related to the investment account on the security target.'},
			{fieldLabel: 'Holding Company', xtype: 'toolbar-combo', name: 'issuingCompany.name', hiddenName: 'issuingCompany.id', width: 180, url: 'businessCompanyListFind.json?issuer=true&ourCompany=false', disableAddNewItem: true, qtip: 'Filters the security targets by a issuing company on the investment account that is related to the investment account on the security target.'},
			{fieldLabel: 'Active On', name: 'activeOnDate', xtype: 'toolbar-datefield'}
		];
	},
	applyTopToolbarFilterLoadParams: function(params) {
		const returnParams = params || {};
		const t = this.getTopToolbar();
		const aodFilter = TCG.getChildByName(t, 'activeOnDate');
		const haFilter = TCG.getChildByName(t, 'holdingInvestmentAccount.label');
		const hcFilter = TCG.getChildByName(t, 'issuingCompany.name');
		if (TCG.isNotBlank(aodFilter.getValue())) {
			returnParams.activeOnDate = aodFilter.getValue().format('m/d/Y');
		}
		if (TCG.isNotBlank(haFilter.getValue())) {
			returnParams.relatedAccountId = haFilter.getValue();
		}
		if (TCG.isNotBlank(hcFilter.getValue())) {
			returnParams.issuingCompanyId = hcFilter.getValue();
		}
		return returnParams;
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			TCG.getChildByName(this.getTopToolbar(), 'activeOnDate').setValue(new Date());
		}
		const defaultParams = {};
		const clientInvestmentAccountId = this.getWindow().getMainFormId ? this.getWindow().getMainFormId() : void 0;
		if (TCG.isNotBlank(clientInvestmentAccountId)) {
			defaultParams['clientInvestmentAccountId'] = clientInvestmentAccountId;
		}
		return this.applyTopToolbarFilterLoadParams(defaultParams);
	},
	openCustomFieldWindow: function(groupName) {
		const accountId = this.getWindow().getMainFormId();
		Clifton.investment.account.openCliftonAccountCustomFieldWindow(groupName, accountId);
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Client Account', width: 75, dataIndex: 'clientInvestmentAccount.label', idDataIndex: 'clientInvestmentAccount.id', filter: {searchFieldName: 'clientInvestmentAccountId', type: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Team', width: 40, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', idDataIndex: 'clientInvestmentAccount.teamSecurityGroup.id', filter: {searchFieldName: 'teamSecurityGroupId', type: 'combo', url: 'securityGroupTeamList.json', loadAll: true}},
		{header: 'Service', width: 60, dataIndex: 'clientInvestmentAccount.coalesceBusinessServiceGroupName', idDataIndex: 'clientInvestmentAccount.businessService.id', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}},
		{header: 'Target Underlying Security', width: 75, dataIndex: 'targetUnderlyingSecurity.label', idDataIndex: 'targetUnderlyingSecurity.id', renderer: TCG.renderValueWithTooltip, filter: {searchFieldName: 'targetUnderlyingSecurityId', type: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label'}},
		{
			header: 'Target Quantity', width: 40, dataIndex: 'targetUnderlyingQuantity', type: 'currency', useNull: true, hidden: true,
			tooltip: 'The target quantity of the underlying security to hold for this client account. Will be blank if notional or manager account is used. Negative amounts target short positions; positive amounts target long positions.'
		},
		{
			header: 'Target Notional', width: 40, dataIndex: 'targetUnderlyingNotional', type: 'currency', useNull: true, hidden: true,
			tooltip: 'The target notional of the underlying security to hold for this client account. Will be blank if quantity or manager account is used. Negative amounts target short positions; positive amounts target long positions.'
		},
		{
			header: 'Target Manager', width: 40, dataIndex: 'targetUnderlyingManagerAccount.label', idDataIndex: 'targetUnderlyingManagerAccount.id', useNull: true, hidden: true, filter: {type: 'combo', searchFieldName: 'targetUnderlyingManagerAccountId', displayField: 'label', url: 'investmentManagerAccountListFind.json'},
			tooltip: 'The manager account to use for the target of the underlying security to hold for this client account. Will be blank if quantity or notional is used.'
		},
		{header: 'Target Type', width: 50, hidden: true, dataIndex: 'targetType', filter: {type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: [['Quantity', 'Quantity'], ['Notional', 'Notional'], ['Manager', 'Manager']]}}, sortable: false},
		{
			header: 'Target', width: 60, dataIndex: 'target', tooltip: 'The target of the underlying security to hold for this client account', filter: false, sortable: false,
			renderer: function(value, metadata, record) {
				let returnValue = value;
				if (TCG.isBlank(returnValue)) {
					let targetValue = TCG.getValue('targetUnderlyingQuantity', record.json);
					if (TCG.isBlank(targetValue)) {
						targetValue = TCG.getValue('targetUnderlyingNotional', record.json);
					}
					if (TCG.isBlank(targetValue)) {
						targetValue = TCG.getValue('targetUnderlyingManagerAccount.label', record.json);
					}
					else {
						targetValue = TCG.numberFormat(targetValue, '0,000.00');
					}
					returnValue = TCG.getValue('targetType', record.json) + ': ' + targetValue;
					record.data['target'] = returnValue;
				}
				return TCG.renderValueWithTooltip(returnValue, metadata, record);
			}
		},
		{
			header: 'Target Multiplier', width: 40, dataIndex: 'targetUnderlyingMultiplier', type: 'float',
			tooltip: 'The target multiplier to use for the target of underlying security. The default is 1, which means 100% of the defined target should be held.'
		},
		{
			header: 'Tranches', width: 30, dataIndex: 'trancheCount', type: 'int', useNull: true,
			renderer: function(value, metaData, record, rowIndex, colIndex, store) {
				if (TCG.isBlank(value)) {
					value = Clifton.business.service.getTrancheCountFromBusinessServiceProcessingType(record.json.clientInvestmentAccount.serviceProcessingType.id);
				}
				return value;
			}
		},
		{
			header: 'Cycle Duration', width: 30, dataIndex: 'cycleDurationWeeks', type: 'int', useNull: true,
			tooltip: 'The duration (in weeks) of a service\'s trading cycle. This value is only defined if it is applicable to the trading service.',
			renderer: function(value, metaData, record, rowIndex, colIndex, store) {
				if (TCG.isBlank(value)) {
					value = Clifton.business.service.getCycleDurationWeeksFromBusinessServiceProcessingType(record.json.clientInvestmentAccount.serviceProcessingType.id);
				}
				return value;
			}
		},
		{header: 'Note', width: 100, dataIndex: 'securityTargetNote', renderer: TCG.renderValueWithTooltip},
		{header: 'Start Date', width: 30, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'End Date', width: 30, dataIndex: 'endDate'},
		{header: 'Last Buy', width: 30, dataIndex: 'lastBuyTradeDate', hidden: true, tooltip: 'Date for the last buy trade applicable to this Security Target'},
		{header: 'Last Sell', width: 30, dataIndex: 'lastSellTradeDate', hidden: true, tooltip: 'Date for the last sell trade applicable to this Security Target'}
	],
	editor: {
		detailPageClass: 'Clifton.investment.account.securitytarget.AccountSecurityTargetWindow',
		endURL: 'investmentAccountSecurityTargetEnd.json',
		deleteURL: 'investmentAccountSecurityTargetDelete.json',
		getDefaultData: function(gridPanel) { // defaults client account for the detail page
			const defaultData = this.defaultData || {};
			if (!defaultData.clientInvestmentAccount) {
				const clientInvestmentAccount = this.getWindow().getMainFormId ? gridPanel.getWindow().getMainForm().formValues : void 0;
				if (TCG.isNotNull(clientInvestmentAccount)) {
					defaultData['clientInvestmentAccount'] = clientInvestmentAccount;
				}
			}
			return defaultData;
		},
		addToolbarAddButton: function(toolBar, gridPanel) {
			const editor = this;
			toolBar.add({
				text: 'Add',
				xtype: 'splitbutton',
				tooltip: 'Add a new item',
				iconCls: 'add',
				handler: function() {
					editor.openDetailPage(editor.getDetailPageClass(), gridPanel);
				},
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Add',
							tooltip: 'Add a new item',
							iconCls: 'add',
							handler: function() {
								editor.openDetailPage(editor.getDetailPageClass(), gridPanel);
							}
						},
						{
							text: 'End and Add from Template',
							tooltip: 'End selected target and use its values as a template for a new target',
							iconCls: 'copy',
							handler: function() {
								const sm = gridPanel.grid.getSelectionModel();
								if (sm.getCount() === 0) {
									TCG.showError('Please select a row to use as the template.', 'No Row(s) Selected');
								}
								else if (sm.getCount() !== 1) {
									TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
								}
								else {
									const loader = new TCG.data.JsonLoader({
										params: {id: sm.getSelected().id},
										onLoad: function(record, conf) {
											const pageClass = editor.getDetailPageClass();
											record.existingId = record.id;
											record.existingLabel = record.id + ': ' + (record.label || '');
											record.endExisting = TCG.isNotBlank(record.id) && TCG.isNotBlank(record.label);
											delete record['id'];
											delete record['securityTargetNote'];
											TCG.createComponent(pageClass, {
												id: TCG.getComponentId(pageClass),
												defaultData: record,
												openerCt: gridPanel
											});
										}
									});
									loader.load('investmentAccountSecurityTarget.json', gridPanel);
								}
							}
						}
					]
				})
			});
			toolBar.add('-');
		},
		addToolbarDeleteButton: function(toolBar, gridPanel) {
			const editor = this;
			const deleteHandler = (action, deleteUrl) => {
				const sm = editor.grid.getSelectionModel();
				const confirmSuffix = action && typeof action === 'string' && 'Delete'.toUpperCase() === action.toUpperCase()
					? ' The preferred method of deactivating a Security Target is to End it so historic targets are available.' : '';
				if (sm.getCount() === 0) {
					TCG.showError('Please select a Security Target to ' + action + '.', 'No Security Target(s) Selected');
				}
				else if (sm.getCount() !== 1) {
					if (editor.allowToDeleteMultiple) {
						Ext.Msg.confirm(action + ' Selected Security Target(s)', 'Would you like to ' + action + ' all selected Security Target(s)?' + confirmSuffix, function(a) {
							if (a === 'yes') {
								editor.deleteSelection(sm, gridPanel, deleteUrl);
							}
						});
					}
					else {
						TCG.showError('Multi-selection is not supported yet.  Please select one Security Target.', 'NOT SUPPORTED');
					}
				}
				else {
					Ext.Msg.confirm(action + ' Selected Security Target', 'Would you like to ' + action + ' selected Security Target?' + confirmSuffix, function(a) {
						if (a === 'yes') {
							editor.deleteSelection(sm, gridPanel, deleteUrl);
						}
					});
				}
			};

			toolBar.add({
				text: 'End',
				xtype: 'splitbutton',
				tooltip: 'End selected item(s) for Current Day - 1',
				iconCls: 'remove',
				handler: function() {
					deleteHandler('End', editor.endURL);
				},
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'End',
							tooltip: 'End selected item(s) for Current Day - 1',
							iconCls: 'remove',
							handler: function() {
								deleteHandler('End', editor.endURL);
							}
						},
						{
							text: 'Delete',
							tooltip: 'Delete selected item(s)',
							iconCls: 'remove',
							handler: function() {
								deleteHandler('Delete', editor.deleteURL);
							}
						}
					]
				})
			});
			toolBar.add('-');
		}
	}
});
Ext.reg('security-target-gridpanel', Clifton.investment.account.InvestmentAccountSecurityTargetGrid);


Clifton.investment.account.InvestmentAccountSecurityTargetHistoryGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentAccountSecurityTargetHistoryListFind',
	instructions: 'The following security targets are associated with this client account.',
	additionalPropertiesToRequest: 'securityTarget.clientInvestmentAccount.serviceProcessingType',
	standardColumns: [
		{
			header: 'Created By', width: 30, dataIndex: 'securityTarget.createUserId', idDataIndex: 'securityTarget.createUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'createUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true},
			renderer: Clifton.security.renderSecurityUser,
			exportColumnValueConverter: 'securityUserColumnReversableConverter'
		},
		{header: 'Created On', width: 40, dataIndex: 'securityTarget.createDate', hidden: true},
		{
			header: 'Updated By', width: 30, dataIndex: 'securityTarget.updateUserId', idDataIndex: 'securityTarget.updateUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'updateUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true},
			renderer: Clifton.security.renderSecurityUser,
			exportColumnValueConverter: 'securityUserColumnReversableConverter'
		},
		{header: 'Updated On', width: 40, dataIndex: 'securityTarget.updateDate', hidden: true}
	],

	getTopToolbarFilters: function(toolbar) {
		const gridPanel = this;

		if (this.getWindow().getMainFormId) {
			toolbar.add({
				text: 'Setup',
				tooltip: 'Account Setup',
				iconCls: 'account',
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Portfolio Setup',
							handler: function() {
								gridPanel.openCustomFieldWindow('Portfolio Setup');
							}
						},
						{
							text: 'Performance Summary Setup',
							handler: function() {
								gridPanel.openCustomFieldWindow('Performance Summary Fields');
							}
						}
					]
				})
			});
			toolbar.add('-');
		}


		return [
			{fieldLabel: 'Active On', name: 'activeOnDate', xtype: 'toolbar-datefield'}
		];
	},
	applyTopToolbarFilterLoadParams: function(params) {
		const returnParams = params || {};
		const t = this.getTopToolbar();
		const filter = TCG.getChildByName(t, 'activeOnDate');
		if (TCG.isNotBlank(filter.getValue())) {
			returnParams.activeOnDate = filter.getValue().format('m/d/Y');
		}
		return returnParams;
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			TCG.getChildByName(this.getTopToolbar(), 'activeOnDate').setValue(new Date());
		}
		const defaultParams = {};
		const clientInvestmentAccountId = this.getWindow().getMainFormId ? this.getWindow().getMainFormId() : void 0;
		if (TCG.isNotBlank(clientInvestmentAccountId)) {
			defaultParams['clientInvestmentAccountId'] = clientInvestmentAccountId;
		}
		return this.applyTopToolbarFilterLoadParams(defaultParams);
	},
	openCustomFieldWindow: function(groupName) {
		const accountId = this.getWindow().getMainFormId();
		Clifton.investment.account.openCliftonAccountCustomFieldWindow(groupName, accountId);
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'securityTarget.id', hidden: true},
		{header: 'Client Account', width: 75, dataIndex: 'securityTarget.clientInvestmentAccount.label', idDataIndex: 'clientInvestmentAccount.id', filter: {searchFieldName: 'clientInvestmentAccountId', type: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, renderer: TCG.renderValueWithTooltip},
		{header: 'Team', width: 40, dataIndex: 'securityTarget.clientInvestmentAccount.teamSecurityGroup.name', idDataIndex: 'clientInvestmentAccount.teamSecurityGroup.id', filter: {searchFieldName: 'teamSecurityGroupId', type: 'combo', url: 'securityGroupTeamList.json', loadAll: true}},
		{header: 'Service', width: 60, dataIndex: 'securityTarget.clientInvestmentAccount.coalesceBusinessServiceGroupName', idDataIndex: 'clientInvestmentAccount.businessService.id', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}},
		{header: 'Target Underlying Security', width: 75, dataIndex: 'securityTarget.targetUnderlyingSecurity.label', renderer: TCG.renderValueWithTooltip, filter: {searchFieldName: 'targetUnderlyingSecurityId', type: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label'}},
		{
			header: 'Target Quantity', width: 40, dataIndex: 'securityTarget.targetUnderlyingQuantity', type: 'currency', useNull: true, hidden: true,
			tooltip: 'The target quantity of the underlying security to hold for this client account. Will be blank if notional or manager account is used. Negative amounts target short positions; positive amounts target long positions.'
		},
		{
			header: 'Target Notional', width: 40, dataIndex: 'securityTarget.targetUnderlyingNotional', type: 'currency', useNull: true, hidden: true,
			tooltip: 'The target notional of the underlying security to hold for this client account. Will be blank if quantity or manager account is used. Negative amounts target short positions; positive amounts target long positions.'
		},
		{
			header: 'Target Manager', width: 40, dataIndex: 'securityTarget.targetUnderlyingManagerAccount.label', idDataIndex: 'targetUnderlyingManagerAccount.id', useNull: true, hidden: true, filter: {type: 'combo', searchFieldName: 'targetUnderlyingManagerAccountId', displayField: 'label', url: 'investmentManagerAccountListFind.json'},
			tooltip: 'The manager account to use for the target of the underlying security to hold for this client account. Will be blank if quantity or notional is used.'
		},
		{header: 'Target Type', width: 50, hidden: true, dataIndex: 'securityTarget.targetType', filter: {type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: [['Quantity', 'Quantity'], ['Notional', 'Notional'], ['Manager', 'Manager']]}}, sortable: false},
		{
			header: 'Target', width: 60, dataIndex: 'securityTarget.target', tooltip: 'The target of the underlying security to hold for this client account', filter: false, sortable: false,
			renderer: function(value, metadata, record) {
				let returnValue = value;
				if (TCG.isBlank(returnValue)) {
					let targetValue = TCG.getValue('securityTarget.targetUnderlyingQuantity', record.json);
					if (TCG.isBlank(targetValue)) {
						targetValue = TCG.getValue('securityTarget.targetUnderlyingNotional', record.json);
					}
					if (TCG.isBlank(targetValue)) {
						targetValue = TCG.getValue('securityTarget.targetUnderlyingManagerAccount.label', record.json);
					}
					else {
						targetValue = TCG.numberFormat(targetValue, '0,000.00');
					}
					returnValue = TCG.getValue('securityTarget.targetType', record.json) + ': ' + targetValue;
					record.data['target'] = returnValue;
				}
				return TCG.renderValueWithTooltip(returnValue, metadata, record);
			}
		},
		{
			header: 'Difference', width: 40, dataIndex: 'quantityOrNotionalDifference', type: 'currency', useNull: true, filter: false, sortable: false,
			tooltip: 'The difference of Target Quantity or Target Notional between this security target entry and the previous historical security target entry.'
		},
		{
			header: 'Target Multiplier', width: 40, dataIndex: 'securityTarget.targetUnderlyingMultiplier', type: 'float',
			tooltip: 'The target multiplier to use for the target of underlying security. The default is 1, which means 100% of the defined target should be held.'
		},
		{
			header: 'Tranches', width: 30, dataIndex: 'securityTarget.trancheCount', type: 'int', useNull: true,
			renderer: function(value, metaData, record, rowIndex, colIndex, store) {
				if (TCG.isBlank(value)) {
					value = Clifton.business.service.getTrancheCountFromBusinessServiceProcessingType(record.json.securityTarget.clientInvestmentAccount.serviceProcessingType.id);
				}
				return value;
			}
		},
		{
			header: 'Cycle Duration', width: 30, dataIndex: 'securityTarget.cycleDurationWeeks', type: 'int', useNull: true,
			tooltip: 'The duration (in weeks) of a service\'s trading cycle. This value is only defined if it is applicable to the trading service.',
			renderer: function(value, metaData, record, rowIndex, colIndex, store) {
				if (TCG.isBlank(value)) {
					value = Clifton.business.service.getCycleDurationWeeksFromBusinessServiceProcessingType(record.json.securityTarget.clientInvestmentAccount.serviceProcessingType.id);
				}
				return value;
			}
		},
		{header: 'Note', width: 100, dataIndex: 'securityTarget.securityTargetNote', renderer: TCG.renderValueWithTooltip},
		{header: 'Start Date', width: 30, dataIndex: 'securityTarget.startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'End Date', width: 30, dataIndex: 'securityTarget.endDate'},
		{header: 'Last Buy', width: 30, dataIndex: 'securityTarget.lastBuyTradeDate', hidden: true, tooltip: 'Date for the last buy trade applicable to this Security Target'},
		{header: 'Last Sell', width: 30, dataIndex: 'securityTarget.lastSellTradeDate', hidden: true, tooltip: 'Date for the last sell trade applicable to this Security Target'}
	],
	editor: {
		detailPageClass: 'Clifton.investment.account.securitytarget.AccountSecurityTargetWindow',
		drillDownOnly: true,
		getDetailPageId: function(grid, row) {
			return TCG.getValue('securityTarget.id', row.json);
		}
	}
});
Ext.reg('security-target-history-gridpanel', Clifton.investment.account.InvestmentAccountSecurityTargetHistoryGrid);

Clifton.investment.instrument.SecurityDragAndDropCreationModalWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Investment Security Creation By Ticker',
	iconCls: 'stock-chart',
	instructions: 'Select the instrument for the security symbol.',
	modal: true,
	allowOpenFromModal: true,
	okButtonText: 'Continue',
	okButtonTooltip: 'Continue to the security window to finish creating the security',
	width: 700,
	height: 400,

	items: [{
		xtype: 'formpanel',
		instructions: 'Select the instrument for the security symbol and then click the \'Continue\' button to launch the necessary security window. Use the optional investment security properties to assist in finding the correct instrument for the new security ticker.',
		labelWidth: 140,
		getDefaultData: window => {
			const defaultData = window.defaultData || {};
			const underlyingInstrumentId = TCG.getValue('underlyingSecurity.instrument.id', defaultData);
			if (underlyingInstrumentId) {
				defaultData.underlyingInstrument = {id: underlyingInstrumentId};
			}
			return defaultData;
		},
		items: [
			{
				xtype: 'fieldset',
				title: 'Investment Properties',
				collapsible: false,
				items: [
					{
						fieldLabel: 'Investment Type', name: 'investmentType.name', hiddenName: 'investmentType.id', displayField: 'name', xtype: 'combo', loadAll: true, limitRequestedProperties: false,
						url: 'investmentTypeList.json',
						listeners: {
							change: function(combo, newValue, oldValue) {
								const form = combo.getParentForm().getForm();

								if (TCG.isBlank(newValue)) {
									form.findField('instrument.label').resetStore();
									form.findField('investmentTypeSubType.name').resetStore();
									form.findField('investmentTypeSubType2.name').resetStore();
								}
							},
							select: function(combo, record, index) {
								const form = combo.getParentForm().getForm();

								form.findField('investmentTypeSubType.name').clearAndReset();
								form.findField('investmentTypeSubType2.name').clearAndReset();
								form.findField('instrumentHierarchy.labelExpanded').clearAndReset();
								form.findField('instrument.label').clearAndReset();
							}
						}
					},
					{
						fieldLabel: 'Investment Sub Type', name: 'investmentTypeSubType.name', hiddenName: 'investmentTypeSubType.id', displayField: 'name', xtype: 'combo', loadAll: true, limitRequestedProperties: false, requiredFields: ['investmentType.name'],
						url: 'investmentTypeSubTypeListByType.json',
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const form = combo.getParentForm().getForm();

								combo.store.baseParams = {investmentTypeId: form.findField('investmentType.name').getValue()};
							},
							change: function(combo, newValue, oldValue) {
								const form = combo.getParentForm().getForm();

								if (TCG.isBlank(newValue)) {
									form.findField('instrument.label').resetStore();
								}
							},
							select: function(combo, record, index) {
								const form = combo.getParentForm().getForm();

								form.findField('instrumentHierarchy.labelExpanded').clearAndReset();
								form.findField('instrument.label').clearAndReset();
							}
						}
					},
					{
						fieldLabel: 'Investment Sub Type 2', name: 'investmentTypeSubType2.name', hiddenName: 'investmentTypeSubType2.id', displayField: 'name', xtype: 'combo', loadAll: true, limitRequestedProperties: false, requiredFields: ['investmentType.name'],
						url: 'investmentTypeSubType2ListByType.json',
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const form = combo.getParentForm().getForm();

								combo.store.baseParams = {
									investmentTypeId: form.findField('investmentType.name').getValue(),
									investmentTypeSubTypeId: form.findField('investmentTypeSubType.id').getValue()
								};
							},
							change: function(combo, newValue, oldValue) {
								const form = combo.getParentForm().getForm();

								if (TCG.isBlank(newValue)) {
									form.findField('instrument.label').resetStore();
								}
							},
							select: function(combo, record, index) {
								const form = combo.getParentForm().getForm();

								form.findField('instrumentHierarchy.labelExpanded').clearAndReset();
								form.findField('instrument.label').clearAndReset();
							}
						}
					},
					{
						fieldLabel: 'Investment Hierarchy', queryParam: 'labelExpanded', name: 'instrumentHierarchy.labelExpanded', hiddenName: 'instrumentHierarchy.id', displayField: 'labelExpanded', xtype: 'combo', limitRequestedProperties: false,
						url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true&tradingDisallowed=false', listWidth: 600, detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow',
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const form = combo.getParentForm().getForm();

								combo.store.baseParams = {
									investmentTypeId: form.findField('investmentType.id').getValue(),
									investmentTypeSubTypeId: form.findField('investmentTypeSubType.id').getValue(),
									investmentTypeSubType2Id: form.findField('investmentTypeSubType2.id').getValue()
								};
							},
							change: function(combo, newValue, oldValue) {
								const form = combo.getParentForm().getForm();

								if (TCG.isBlank(newValue)) {
									form.findField('instrument.label').clearAndReset();
								}
							},
							select: function(combo, record, index) {
								const form = combo.getParentForm().getForm();

								const investmentType = record.json.investmentType;

								form.findField('investmentType.name').setValue({value: investmentType.id, text: investmentType.name});
								form.findField('instrument.label').clearAndReset();
							}
						}
					},
					{
						fieldLabel: 'Underlying Security', name: 'underlyingSecurity.label', hiddenName: 'underlyingSecurity.id', xtype: 'combo', limitRequestedProperties: false,
						displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;

								combo.store.baseParams = {
									active: true
								};
							},
							change: function(combo, newValue, oldValue) {
								const form = combo.getParentForm().getForm();

								if (TCG.isBlank(newValue)) {
									form.findField('underlyingInstrument.id').setValue('');
									form.findField('instrument.label').clearAndReset();
								}
							},
							select: function(combo, record, index) {
								const form = combo.getParentForm().getForm();

								form.findField('underlyingInstrument.id').setValue(record.json.instrument.id);
								form.findField('instrument.label').clearAndReset();
							}
						}
					},
					{
						boxLabel: 'Active Instruments Only', name: 'activeOnly', xtype: 'checkbox', checked: true,
						qtip: 'Filter for active instruments only. Most one-to-many instruments (futures, options, etc.) are considered always active and will remain in the list even if this is checked.'
					}
				]
			},
			{name: 'underlyingInstrument.id', xtype: 'hidden'},
			{
				fieldLabel: 'Investment Instrument', xtype: 'combo', name: 'instrument.label', hiddenName: 'instrument.id', allowBlank: false, displayField: 'labelLong', limitRequestedProperties: false,
				url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const form = combo.getParentForm().getForm();
						const baseParams = {
							hierarchyId: form.findField('instrumentHierarchy.id').getValue(),
							investmentTypeId: form.findField('investmentType.id').getValue(),
							investmentTypeSubTypeId: form.findField('investmentTypeSubType.id').getValue(),
							investmentTypeSubType2Id: form.findField('investmentTypeSubType2.id').getValue(),
							underlyingInstrumentId: form.findField('underlyingInstrument.id').getValue()
						};
						const activeOnly = form.findField('activeOnly').checked;
						if (activeOnly) {
							baseParams.inactive = false;
						}
						combo.store.baseParams = baseParams;
					},
					select: function(combo, record, index) {
						const form = combo.getParentForm().getForm();

						const hierarchy = record.json.hierarchy;
						const investmentType = hierarchy.investmentType;

						form.findField('investmentType.name').setValue({value: investmentType.id, text: investmentType.name});
						form.findField('instrumentHierarchy.labelExpanded').setValue({value: hierarchy.id, text: hierarchy.labelExpanded});
					}
				}
			},
			{fieldLabel: 'Security Symbol', name: 'symbol', allowBlank: false}
		]
	}],
	saveWindow: async function() {
		const modalWindow = this;
		const form = modalWindow.getMainFormPanel().getForm();
		const defaultData = {
			symbol: form.findField('symbol').getValue().toUpperCase(),
			startDate: new Date()
		};

		const security = await TCG.data.getDataPromise('investmentSecurityBySymbolOnly.json?symbol=' + defaultData.symbol, this);
		if (security) {
			TCG.showError('Security with symbol already exists', 'Security Creation');
			return;
		}
		// launch window after stripping optional market sector from security symbol
		const dataSource = await TCG.data.getDataPromise('marketDataSourceByName.json', this, {
			params: {
				marketDatasourceName: 'Bloomberg',
				requestedPropertiesRoot: 'data',
				requestedProperties: 'id'
			}
		});
		const sectors = (dataSource)
			? await TCG.data.getDataPromise('marketDataSourceSectorListByDataSource.json', this, {
				params: {
					dataSourceId: dataSource.id,
					requestedPropertiesRoot: 'data',
					requestedProperties: 'name'
				}
			})
			: [];
		if (sectors) {
			const lastSpaceIndex = defaultData.symbol.trim().lastIndexOf(' ');
			const symbolSector = defaultData.symbol.substring(lastSpaceIndex + 1).toUpperCase();
			const length = sectors.length;
			for (let i = 0; i < length; i++) {
				const sectorName = sectors[i].name.toUpperCase();
				if (symbolSector === sectorName) {
					defaultData.symbol = defaultData.symbol.substring(0, lastSpaceIndex).trim();
					break;
				}
			}
		}
		const instrument = await TCG.data.getDataPromise('investmentInstrument.json?id=' + form.findField('instrument.id').getValue(), this);
		defaultData.instrument = instrument;
		if (instrument && TCG.getValue('hierarchy.investmentType.name', instrument) === 'Options') {
			defaultData.settlementExpiryTime = 'PM';
		}


		// attempt to validate instrument with security symbol
		let error = void 0;
		let warning = void 0;
		if (instrument && instrument.underlyingInstrument) {
			const instrumentUnderlyingSymbol = instrument.underlyingInstrument.identifierPrefix.replace(/\/\./g, '');
			const instrumentSymbolParts = instrument.identifierPrefix.split('-');
			const newTickerPrefix = defaultData.symbol.split(' ')[0] || void 0;
			if (!defaultData.symbol.includes(instrumentUnderlyingSymbol)) {
				// try instrument's identifier (e.g. Russell options have underlying RTY with options using RUT)
				if (instrumentSymbolParts.length === 1 || !defaultData.symbol.includes(instrumentSymbolParts[1])) {
					error = 'New security symbol [' + defaultData.symbol + '] is invalid for the specified instrument.';
				}
			}
			if (TCG.isBlank(error)) {
				let errorSymbol = void 0;
				if (instrumentSymbolParts.length === 1 && instrumentSymbolParts[0] !== newTickerPrefix) {
					errorSymbol = instrumentSymbolParts[0];
				}
				else if (TCG.isNotBlank(instrumentSymbolParts[1]) && TCG.isNotBlank(newTickerPrefix) && !newTickerPrefix.startsWith(instrumentSymbolParts[1])) {
					errorSymbol = instrumentSymbolParts.slice(1).join('-');
				}
				if (TCG.isNotBlank(errorSymbol)) {
					warning = 'New security symbol prefix [' + newTickerPrefix + '] does not match the specified instrument\'s prefix [' + errorSymbol + ']. Would you like to continue with security creation?';
				}
			}
		}

		if (error) {
			TCG.showError(error, 'Security Creation');
		}
		else if (!warning || ('yes' === await TCG.showConfirm(warning, 'Security Creation'))) {
			const openSecurityWindow = (securityData, disableModalClose, callback) => {
				// This config is applied on the security window. The hooks for 'afterRenderPromise' loads field mapping data.
				// The 'aftercreate' and 'beforeclose' vary based on a chained security creation such as with big/mini creation.
				const config = {
					defaultData: securityData,
					listeners: {
						afterrender: function(window) {
							// auto load security data for this ticker
							const formPanel = window.getMainFormPanel();
							formPanel.on('afterRenderPromise', panel => {
								const tickers = TCG.getChildrenByClass(panel, Clifton.investment.instrument.SecurityTicker);
								tickers.forEach(ticker => {
									const securityDataButton = TCG.getChildByName(ticker.innerCt, 'securityTickerDataButton');
									if (securityDataButton) {
										securityDataButton.handler(securityDataButton, Ext.EventObject);
									}
								});
							});
							formPanel.on('aftercreate', panel => {
								if (callback) {
									const createdSecurity = panel.getForm().formValues;
									callback(createdSecurity);
								}
							});
							formPanel.getWindow().on('beforeclose', window => {
								// close the original modal window after the new security is created if requested
								if (!TCG.isTrue(disableModalClose) && window.params && window.params.id) {
									modalWindow.close();
								}
							});
						}
					}
				};
				TCG.createComponent('Clifton.investment.instrument.SecurityWindow', config);
			};

			if (TCG.getValue('hierarchy.investmentType.name', instrument) === 'Options') {
				// default expiry time to PM
				defaultData.settlementExpiryTime = 'PM';

				if (instrument.bigInstrument && !instrument.bigInstrument.inactive) {
					// swap mini prefix with big prefix for creating big security
					if (!instrument.identifierPrefix) {
						TCG.showError(`Instrument [${instrument.symbol}] is missing an identifier prefix`, 'Security Creation');
						return;
					}
					if (!instrument.bigInstrument.identifierPrefix) {
						TCG.showError(`Instrument [${instrument.bigInstrument.symbol}] is missing an identifier prefix`, 'Security Creation');
						return;
					}
					const instrumentPrefixParts = instrument.identifierPrefix.replace(/\/\./g, '').split('-');
					const bigInstrumentPrefixParts = instrument.bigInstrument.identifierPrefix.replace(/\/\./g, '').split('-');
					if (instrumentPrefixParts.length !== bigInstrumentPrefixParts.length) {
						TCG.showError(`Mini instrument [${instrumentPrefixParts}] and big instrument [${bigInstrumentPrefixParts}] prefixes are not consistent`, 'Security Creation');
						return;
					}
					let instrumentPrefix = instrumentPrefixParts[0];
					let bigInstrumentPrefix = bigInstrumentPrefixParts[0];
					if (instrumentPrefixParts.length > 1) {
						if (instrumentPrefix !== bigInstrumentPrefix) {
							TCG.showError(`Mini instrument prefix [${instrumentPrefix}] and big instrument prefix [${bigInstrumentPrefix}] first portion is inconsistent`, 'Security Creation');
							return;
						}
						instrumentPrefix = instrumentPrefixParts[1];
						bigInstrumentPrefix = bigInstrumentPrefixParts[1];
					}
					const bigSymbol = defaultData.symbol.replace(instrumentPrefix, bigInstrumentPrefix);

					const bigSecurity = await TCG.data.getDataPromise('investmentSecurityBySymbolOnly.json?symbol=' + bigSymbol, this);
					if (bigSecurity) {
						// big security exists, set it on the default data of the mini creation with the below call to open security window
						defaultData['bigSecurity'] = {id: bigSecurity.id, label: bigSecurity.label};
					}
					else {
						const missingBigWarning = `New security with symbol [${defaultData.symbol}] is missing its Big Security. Would you like to create it now with symbol [${bigSymbol}]?`;
						if ('yes' === await TCG.showConfirm(missingBigWarning, 'Security Creation')) {
							const bigDefaultData = TCG.clone(defaultData);
							bigDefaultData['instrument'] = instrument.bigInstrument;
							bigDefaultData['symbol'] = bigSymbol;
							// create big and chain mini creation with it
							openSecurityWindow(bigDefaultData, true, createdBigSecurity => {
								defaultData['bigSecurity'] = {id: createdBigSecurity.id, label: createdBigSecurity.label};
								openSecurityWindow(defaultData);
							});
							return;
						}
					}
				}
			}

			openSecurityWindow(defaultData);
		}
	}
});

Clifton.investment.instrument.SecurityDragAndDropContainer = Ext.extend(TCG.file.DragAndDropContainer, {
	iconCls: 'import',
	message: 'Create New Security With Ticker',
	tooltipMessage: 'Drag and drop a security ticker from Bloomberg Terminal here, or click here to copy paste a symbol to launch a process for creating the new security in the system',
	enableDD: function(fp) {
		TCG.enableDragAndDrop(this, this.ondropCallback, this, this.onclickCallback);
	},
	ondropCallback: async (event, container) => {
		if (event) {
			const text = event.dataTransfer.getData('Text');
			if (TCG.isNotBlank(text)) {
				let defaultData = {symbol: text};
				defaultData = await (container.prepareSecurityDefaultData(defaultData) || defaultData);
				TCG.createComponent('Clifton.investment.instrument.SecurityDragAndDropCreationModalWindow', {defaultData: defaultData});
			}
		}
	},
	onclickCallback: async (event, container) => {
		if (event) {
			let defaultData = {};
			defaultData = await (container.prepareSecurityDefaultData(defaultData) || defaultData);
			TCG.createComponent('Clifton.investment.instrument.SecurityDragAndDropCreationModalWindow', {defaultData: defaultData});
		}
	},
	prepareSecurityDefaultData: async function(defaultData) {
		// Override to add default data for the security D&D modal for finding an instrument on security creation
		// Uses could be to default investment type, subtype, or subtype2 from some windows.
	}
});
Ext.reg('security-drag-and-drop-container', Clifton.investment.instrument.SecurityDragAndDropContainer);


Clifton.business.company.CompanyPlatformAdditionalTabs.push({
	title: 'Investment Accounts',
	instructions: 'The following investment accounts are using the selected platform.',
	name: 'investmentAccountListFind',
	xtype: 'gridpanel',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Client Relationship', width: 150, hidden: true, dataIndex: 'businessClient.clientRelationship.name', filter: {type: 'combo', searchFieldName: 'clientRelationshipId', displayField: 'name', url: 'businessClientRelationshipListFind.json'}},
		{header: 'Relationship Category', width: 70, hidden: true, dataIndex: 'businessClient.clientRelationship.relationshipCategory.name', filter: {searchFieldName: 'clientRelationshipCategoryId', type: 'combo', url: 'businessClientRelationshipCategoryListFind.json'}},
		{header: 'Retail', width: 35, dataIndex: 'businessClient.clientRelationship.relationshipCategory.retail', type: 'boolean', filter: {searchFieldName: 'clientRelationshipCategoryRetail'}},
		{header: 'Client', width: 75, dataIndex: 'businessClient.name', filter: {searchFieldName: 'clientName'}, hidden: true},
		{header: 'Account Type', width: 60, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'accountTypeId', displayField: 'name', url: 'investmentAccountTypeListFind.json'}, hidden: true},
		{header: 'Account #', dataIndex: 'number', width: 50, defaultSortColumn: true},
		{header: 'Account Name', dataIndex: 'name', width: 130},
		{header: 'Contract', width: 150, dataIndex: 'businessContract.label', filter: {type: 'combo', searchFieldName: 'businessContractId', displayField: 'name', url: 'businessContractListFind.json'}, hidden: true},
		{header: 'Base Currency', width: 50, dataIndex: 'baseCurrency.symbol', filter: {searchFieldName: 'baseCurrencyId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true'}},
		{header: 'Workflow State', width: 50, dataIndex: 'workflowState.label', filter: {searchFieldName: 'workflowStateName'}},
		{header: 'Workflow Status', width: 50, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=InvestmentAccount'}, hidden: true},
		{header: 'Effective Start', width: 50, dataIndex: 'workflowStateEffectiveStartDate', filter: {searchFieldName: 'workflowStateEffectiveStartDate'}, hidden: true},
		{header: 'Issued By', width: 120, dataIndex: 'issuingCompany.name', hidden: true, filter: {type: 'combo', searchFieldName: 'issuingCompanyId', url: 'businessCompanyListFind.json'}},
		{header: 'Wrap', width: 30, dataIndex: 'wrapAccount', type: 'boolean', tooltip: 'Differentiates accounts where the client pays a periodic account fee rather than trade commissions.  Is tied to Platform in IMS so that we may differentiate between clients who are vs. are not utilizing the platform WRAP feature (retail clients generally do (Institutional clients generally do not) participate in the WRAP feature/account structure on a platform.'}
	],
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.investment.account.AccountWindow'
	},
	getLoadParams: function(firstLoad) {
		return {companyPlatformId: this.getWindow().getMainFormId()};
	}
});


/**
 * The grid type for displaying investment account objects.
 */
Clifton.investment.account.AccountGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentAccountListFind',
	instructions: 'The following Investment Accounts are in the system.  You can search/filter and view accounts across clients from here.  Creating new accounts must be performed from within the Client Setup.',
	queryExportTagName: 'Accounts',
	additionalPropertiesToRequest: 'businessService.aumCalculatorBean.labelShort',
	viewNames: ['Default View', 'Finance View', 'Simple View'],
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Client Relationship', width: 150, hidden: true, dataIndex: 'businessClient.clientRelationship.name', filter: {type: 'combo', searchFieldName: 'clientRelationshipId', displayField: 'name', url: 'businessClientRelationshipListFind.json'}, viewNames: ['Finance View']},
		{header: 'Client Relationship Inception Date', width: 70, dataIndex: 'businessClient.clientRelationship.inceptionDate', hidden: true, filter: {searchFieldName: 'clientRelationshipInceptionDate'}},
		{header: 'Relationship Category', width: 70, hidden: true, dataIndex: 'businessClient.clientRelationship.relationshipCategory.name', filter: {searchFieldName: 'clientRelationshipCategoryId', type: 'combo', url: 'businessClientRelationshipCategoryListFind.json'}},
		{header: 'Client', width: 150, dataIndex: 'businessClient.name', filter: {type: 'combo', searchFieldName: 'clientId', displayField: 'name', url: 'businessClientListFind.json'}, defaultSortColumn: true},
		{header: 'Client Inception Date', width: 70, dataIndex: 'businessClient.inceptionDate', hidden: true, filter: {searchFieldName: 'clientInceptionDate'}},
		{header: 'Contract Category', width: 100, dataIndex: 'businessContract.contractType.categoryName', filter: {searchFieldName: 'contractCategory'}, hidden: true},
		{header: 'Contract Type', width: 100, dataIndex: 'businessContract.contractType.name', filter: {searchFieldName: 'contractType'}, hidden: true},
		{header: 'Contract', width: 150, dataIndex: 'businessContract.label', filter: {type: 'combo', searchFieldName: 'businessContractId', displayField: 'name', url: 'businessContractListFind.json'}, hidden: true},
		{header: 'Account Type', width: 75, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'accountTypeId', displayField: 'name', url: 'investmentAccountTypeListFind.json'}, viewNames: ['Default View']},
		{header: 'Pooled Vehicle Type', width: 80, dataIndex: 'clientAccountType', hidden: true},
		{header: 'GIPS', width: 80, dataIndex: 'discretionType', hidden: true},
		{header: 'Account Number', width: 80, dataIndex: 'number'},
		{header: 'Account Number2', width: 80, dataIndex: 'number2', hidden: true},
		{header: 'Account Name', width: 170, dataIndex: 'name'},
		{header: 'Short Name', width: 150, dataIndex: 'shortName', hidden: true},
		{header: 'Retail', width: 35, dataIndex: 'businessClient.clientRelationship.relationshipCategory.retail', type: 'boolean', filter: {searchFieldName: 'clientRelationshipCategoryRetail'}},
		{header: 'Service', width: 110, dataIndex: 'businessService.name', filter: {type: 'combo', queryParam: 'nameExpanded', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}},
		{header: 'Service Processing Type', width: 80, dataIndex: 'serviceProcessingType.name', filter: {searchFieldName: 'businessServiceProcessingTypeName'}, hidden: true},
		{
			header: 'AUM Calculator', width: 80, dataIndex: 'aumCalculatorBean.labelShort', filter: {searchFieldName: 'aumCalculatorBeanId', type: 'combo', displayField: 'labelShort', url: 'systemBeanListFind.json?groupName=Investment Account Calculation Snapshot Calculator'}, viewNames: ['Default View', 'Finance View'],
			renderer: function(v, metaData, r) {
				const serviceCalculator = TCG.getValue('businessService.aumCalculatorBean.labelShort', r.json);
				if (TCG.isNotBlank(v) && TCG.isNotEquals(v, serviceCalculator)) {
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'Account Override.  Service AUM Calculator is [' + serviceCalculator + ']\'';
				}
				return v;
			}
		},
		{header: 'Closing Method', width: 50, dataIndex: 'accountingClosingMethod', filter: false, tooltip: 'Accounting Closing Method (FIFO, LIFO, etc.) from corresponding Business Service or Account level override.', hidden: true},
		{header: 'Closing Method Override', width: 50, dataIndex: 'accountingClosingMethodOverride', filter: {type: 'list', options: Clifton.business.service.AccountingClosingMethods}, tooltip: 'Optionally overrides Accounting Closing Method (FIFO, LIFO, etc.) specified for account\'s Business Service.', hidden: true},
		{header: 'Team', width: 60, dataIndex: 'teamSecurityGroup.name', filter: {searchFieldName: 'teamSecurityGroupId', type: 'combo', url: 'securityGroupTeamList.json', loadAll: true}},
		{header: 'Base CCY', width: 60, dataIndex: 'baseCurrency.symbol', filter: {searchFieldName: 'baseCurrencyId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true'}},
		{
			header: 'FX Source', width: 90, dataIndex: 'defaultExchangeRateSourceCompany.name', hidden: true, filter: {searchFieldName: 'defaultExchangeRateSourceCompanyId', type: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourAccount=false'},
			tooltip: 'Used for Client Accounts only.  The default company (uses parent name if applies) to use for retrieving FX rates.  Whenever possible, we use the holding account issuer to determine the data source for the FX Rate for a particular position, however it is not possible in all cases to know the holding account.  So, when we do not know the holding account, this is used as the default source for FX rate retrieval for the client account. The largest use case of this is currently Portfolio Runs.  Also, Manager Balances (When Manager is in different CCY then Account) and Billing (When Billing CCY is different than Account). When not selected, Goldman Sachs data source will be used.'
		},
		{header: 'EV Portfolio Code', width: 80, dataIndex: 'externalPortfolioCode.text', hidden: true, filter: {searchFieldName: 'externalPortfolioCodeName'}},
		{header: 'EV Product Code', width: 80, dataIndex: 'externalProductCode.text', hidden: true, filter: {searchFieldName: 'externalProductCodeName'}},
		{header: 'Channel', width: 100, dataIndex: 'clientAccountChannel.text', hidden: true, filter: {searchFieldName: 'clientAccountChannelName'}, renderer: TCG.renderValueWithTooltip},
		{header: 'Workflow State', width: 75, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName', comparison: 'BEGINS_WITH'}},
		{header: 'Workflow Status', width: 70, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName', comparison: 'BEGINS_WITH'}, hidden: true},
		{header: 'Effective Start', width: 70, dataIndex: 'workflowStateEffectiveStartDate', filter: {searchFieldName: 'workflowStateEffectiveStartDate'}, hidden: true},
		{header: 'Positions On Date', width: 70, dataIndex: 'inceptionDate', hidden: true, viewNames: ['Finance View']},
		{header: 'Performance Start Date', width: 70, dataIndex: 'performanceInceptionDate', hidden: true},
		{header: 'Federal Tax #', width: 40, dataIndex: 'businessClient.federalTaxNumber', filter: {searchFieldName: 'federalTaxNumber'}, hidden: true},
		{header: 'State Tax #', width: 40, dataIndex: 'businessClient.stateTaxNumber', filter: {searchFieldName: 'stateTaxNumber'}, hidden: true},
		{header: 'City', width: 75, dataIndex: 'businessClient.company.city', hidden: true, filter: {searchFieldName: 'city'}},
		{header: 'State', width: 50, dataIndex: 'businessClient.company.state', hidden: true, filter: {searchFieldName: 'state'}},
		{header: 'Postal Code', width: 50, dataIndex: 'businessClient.company.postalCode', hidden: true, filter: {searchFieldName: 'postalCode'}},
		{header: 'Country', width: 75, dataIndex: 'businessClient.company.country', hidden: true, filter: {searchFieldName: 'country'}},
		{header: 'Issued By', width: 90, dataIndex: 'issuingCompany.name', filter: {type: 'combo', searchFieldName: 'issuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, viewNames: ['Default View']},
		{
			header: 'Client Directed', width: 80, dataIndex: 'clientDirected', type: 'boolean', hidden: true,
			tooltip: 'Trading for this account is Directed by the Client (used by Holding Accounts only)'
		},
		{
			header: 'Speculative Account', width: 80, dataIndex: 'usedForSpeculation', type: 'boolean', hidden: true,
			tooltip: 'This account is used for speculation rather than hedging (may require higher margin)'
		},
		{header: 'Platform', width: 90, dataIndex: 'companyPlatform.name', hidden: true, filter: {type: 'combo', searchFieldName: 'companyPlatformId', url: 'businessCompanyPlatformListFind.json'}, tooltip: 'Broker-Dealer or RIA program providing access to a variety of investment alternatives including third party investment managers\'s strategies.'},
		{header: 'Wrap', width: 50, dataIndex: 'wrapAccount', hidden: true, type: 'boolean', tooltip: 'Differentiates accounts where the client pays a periodic account fee rather than trade commissions.  Is tied to Platform in IMS so that we may differentiate between clients who are vs. are not utilizing the platform WRAP feature (retail clients generally do (Institutional clients generally do not) participate in the WRAP feature/account structure on a platform.'},
		{header: 'Initial Margin Multiplier', width: 80, dataIndex: 'initialMarginMultiplier', type: 'float', hidden: true, tooltip: 'This field applies to holding accounts and is usually equal to 1 (no additional requirements: use standard). However, a broker may deem a certain account to be more risky and impose additional requirement: 1.5, 2.5, 3.25, etc.'}
	],
	getTopToolbarFilters: function(toolbar) {
		return [
			{
				fieldLabel: 'Risk Designation', width: 125, xtype: 'system-list-combo', name: 'riskDesignation', listName: 'Risk Designations', qtip: 'Select a risk designation to find Client Accounts with the selected risk designation active as of today.  To see values for an account go to Data Fields tab on the Client Account window.',
				listeners: {
					select: function(field) {
						TCG.getParentByClass(field, Ext.Panel).reload();
					}
				}
			},
			{fieldLabel: 'Account Type', width: 150, xtype: 'combo', name: 'accountType', url: 'investmentAccountTypeListFind.json', linkedFilter: 'type.name'},
			{fieldLabel: 'Account Group', width: 150, xtype: 'toolbar-combo', name: 'accountGroup', url: 'investmentAccountGroupListFind.json'},
			{fieldLabel: 'Service Component', width: 200, minListWidth: 250, name: 'serviceComponent', xtype: 'toolbar-combo', url: 'businessServiceListFind.json?orderBy=name&serviceLevelType=SERVICE_COMPONENT', displayField: 'nameWithParent', listWidth: 350},
			{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
		];
	},
	switchToViewBeforeReload: function(viewName) {
		const at = TCG.getChildByName(this.getTopToolbar(), 'accountType');
		let record = null;
		if (TCG.isEquals(viewName, 'Finance View')) {
			const client = TCG.data.getData('investmentAccountTypeByName.json?name=Client', this, 'investment.account.type.Client');
			record = {text: client.name, value: client.id};
			at.setValue(record);
		}
		else {
			at.reload(null, true);
		}
		at.fireEvent('select', at, record);
		return true; // cancel reload: select event will reload
	},
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.investment.account.AccountWindow'
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		const params = {};
		const svc = TCG.getChildByName(this.getTopToolbar(), 'serviceComponent');
		if (TCG.isNotBlank(svc.getValue())) {
			params.businessServiceComponentOrParentId = svc.getValue();
		}
		const riskDesignation = TCG.getChildByName(this.getTopToolbar(), 'riskDesignation');
		if (TCG.isNotBlank(riskDesignation.getValue())) {
			params.systemColumnDatedValueColumnName = 'Risk Designation';
			params.systemColumnDatedValue = riskDesignation.getValue();
			params.datedValueEquals = true; // Exact value = search
			params.datedValueActive = true; // Only those active today
		}
		const accountGroup = TCG.getChildByName(this.getTopToolbar(), 'accountGroup');
		if (TCG.isNotBlank(accountGroup.getValue())) {
			params.investmentAccountGroupId = accountGroup.getValue();
		}
		return params;
	}
});
Ext.reg('investment-account-grid', Clifton.investment.account.AccountGrid);

Clifton.investment.instrument.event.PaymentEventFormPanel = Ext.extend(TCG.form.FormPanel, {
	instructions: 'Coupon payments are made by bond issuers to bond holders. Payments are represented as cash percentage of principal owned.<br />The party that holds position (based on <b>Settlement Date</b>) at the end of day prior to <b>Ex Date</b>, is entitled to the coupon payment. In most cases Ex Date is one day after Accrual End date except for British bonds which have negative accrual from Ex to Accrual End date.',
	url: 'investmentSecurityEvent.json',
	labelWidth: 125,
	initComponent: function() {
		this.items = this.formPanelItems;
		TCG.callSuper(this, 'initComponent', arguments);
	},
	formPanelItems: [
		{fieldLabel: 'Event Type', name: 'type.name', detailIdField: 'type.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow'},
		{fieldLabel: 'Security', name: 'security.label', detailIdField: 'security.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
		{name: 'beforeEventValue', xtype: 'hidden', value: '0'},
		{fieldLabel: 'Coupon %', name: 'afterEventValue', xtype: 'currencyfield', decimalPrecision: 6},
		{fieldLabel: 'Accrual Start', name: 'declareDate', xtype: 'datefield', qtip: 'Accrual is NOT included for the start date (End Of Day industry convention). Previous period Accrual End must be the same as this date.'},
		{fieldLabel: 'Accrual End', name: 'recordDate', xtype: 'datefield', qtip: 'Accrual is included for the end date. Next period Accrual Start must be one calendar day after this date.'},
		{fieldLabel: 'Ex Date', name: 'exDate', xtype: 'datefield'},
		{fieldLabel: 'Payment Date', name: 'eventDate', xtype: 'datefield'},
		{
			fieldLabel: 'Actual Settle', name: 'actualSettlementDate', xtype: 'datefield', allowBlank: true, requiredFields: ['eventDate'],
			qtip: 'The actual settlement (wire) date.',
			validateValue: function(value) {
				const fp = TCG.getParentFormPanel(this);
				const f = fp.getForm();
				const actualField = f.findField('actualSettlementDate');
				if (!actualField.el.dom.readOnly) {
					const actualDate = actualField.getValue();
					const settlementDate = fp.getFormValue('eventDate', true, true);
					if (actualDate && settlementDate && actualDate < settlementDate) {
						actualField.markInvalid('Actual Settlement Date cannot be before the Payment Date.');
						return false;
					}
				}
				return true;
			}
		},
		{fieldLabel: 'Additional Date', name: 'additionalDate', xtype: 'datefield', qtip: 'Some events may require additional date (Fixing/Reset Date for Equity/Interest Leg Payment for Total Return Swaps'},
		{fieldLabel: 'Settlement Currency', name: 'additionalSecurity.label', hiddenName: 'additionalSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true', qtip: 'This event will be paid in the currency specified here. Leave blank to use currency denomination of corresponding security.'},
		{fieldLabel: 'Event Description', name: 'eventDescription', xtype: 'textarea', height: 50},
		{boxLabel: 'Include pending leg payments with accrual amount', name: 'security.instrument.hierarchy.includeAccrualReceivables', xtype: 'checkbox', hidden: false}
	],
	listeners: {
		afterload: function(formpanel) {
			const form = formpanel.getForm();
			const actualSettlementDate = form.findField('actualSettlementDate');
			const flag = form.findField('security.instrument.hierarchy.includeAccrualReceivables');
			if (flag?.value === false) {
				formpanel.removeField(actualSettlementDate);
			}
		}
	}
});
Ext.reg('investment-security-event-form', Clifton.investment.instrument.event.PaymentEventFormPanel);

Clifton.investment.instrument.SecurityEventsBySecurityGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentSecurityEventListFind',
	instructions: 'A list of all events for selected security.',
	viewNames: ['Default', {text: 'Indirect Events', tooltip: 'Shows events that reference this Security as a Payout or as an Additional Security.'}],
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'CA ID', width: 40, dataIndex: 'corporateActionIdentifier', type: 'int', numberFormat: '0', useNull: true, hidden: true, tooltip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.'},
		{header: 'Event Type', width: 80, dataIndex: 'type.name', filter: {searchFieldName: 'typeNameSearch'}},
		{header: 'Event Status', width: 50, dataIndex: 'status.name', renderer: Clifton.investment.instrument.event.renderEventStatus, filter: {type: 'combo', searchFieldName: 'statusId', url: 'investmentSecurityEventStatusListFind.json'}},
		{header: 'Before Value', width: 45, dataIndex: 'beforeEventValue', type: 'float', useNull: true},
		{header: 'After Value', width: 45, dataIndex: 'afterEventValue', type: 'float', useNull: true},
		{header: 'Additional Value', width: 45, dataIndex: 'additionalEventValue', type: 'float', useNull: true, hidden: true},
		{header: 'Additional Date', width: 50, dataIndex: 'additionalDate', hidden: true},
		{header: 'Declare Date', width: 50, dataIndex: 'declareDate', hidden: true},
		{header: 'Ex Date', width: 45, dataIndex: 'exDate'},
		{header: 'Record Date', width: 50, dataIndex: 'recordDate', hidden: true},
		{header: 'Payment Date', width: 50, dataIndex: 'eventDate'},
		{header: 'CCY', width: 25, dataIndex: 'additionalSecurity.symbol', tooltip: 'This dividend will be paid in the currency specified here. Blank means use currency denomination of corresponding security.', filter: {searchFieldName: 'additionalSecurityId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true'}},
		{header: 'Description', width: 50, dataIndex: 'eventDescription'},
		{header: 'Voluntary', width: 30, dataIndex: 'voluntary', type: 'boolean', tooltip: 'Specifies whether the holder of the security has an option to choose to participate or not participate in this event.'},
		{header: 'Taxable', width: 30, dataIndex: 'taxable', type: 'boolean', hidden: true, tooltip: 'Specifies whether this event is likely subject to additional taxes affecting net payout amounts and booking rules.'},
		{header: 'Order', width: 30, dataIndex: 'bookingOrderOverride', type: 'int', useNull: true, hidden: true, tooltip: 'Optionally overrides the booking order specified on accounting event journal types.'}
	],
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow'
	},
	getLoadParams: function() {
		return (this.currentViewName === 'Indirect Events')
			? {indirectSecurityId: this.getWindow().getMainFormId()}
			: {securityId: this.getWindow().getMainFormId()};
	}
});
Ext.reg('investment-security-events-by-security-grid', Clifton.investment.instrument.SecurityEventsBySecurityGrid);

Clifton.investment.InvestmentContextMenuPlugin = Ext.extend(Ext.util.Observable, {
	grid: null,
	/**
	 * Set to false to avoid filtering for row currency or security positions.
	 */
	filterForPositions: true,
	/**
	 * Set to false to not include the contextmenu item to launch a security window
	 */
	openSecurity: true,
	/**
	 * Set to false to not include the contextmenu item to launch an instrument window
	 */
	openInstrument: true,
	/**
	 * Set to false to not include the contextmenu item to launch client account
	 */
	openClientAccount: true,
	/**
	 * Set to false to not include the contextmenu item to launch holding account
	 */
	openHoldingAccount: true,

	/**
	 * Security paths used for getting security data from a record
	 */
	recordSecurityIdPath: 'investmentSecurity.id',
	recordInstrumentIdPath: 'investmentSecurity.instrument.id',
	recordSecurityOnePerInstrumentPathOverride: void 0, // defaults to instrument path with 'id' replaced with 'hierarchy.oneSecurityPerInstrument'

	/**
	 * Account properties for getting account IDs for navigation
	 */
	recordClientAccountIdPath: 'clientInvestmentAccount.id',
	recordHoldingAccountIdPath: 'holdingInvestmentAccount.id',

	/**
	 * Path for general ledger accounting account name for filtering records that can have navigation
	 */
	recordAccountingAccountNamePath: 'accountingAccount.name',

	/**
	 * Flag used to enable async data lookups with caching to perform the context menu items.
	 * First retrieval will fetch data and cache so subsequent menus can build quickly
	 */
	enableAsyncLookups: false,

	constructor: function(config) {
		Object.assign(this, config);
		TCG.callSuper(this, 'constructor', arguments);
	},

	init: function(gridOrPanel) {
		// Unwrap grid panel if necessary
		this.grid = (gridOrPanel instanceof TCG.grid.GridPanel) ? gridOrPanel.grid : gridOrPanel;

		const investmentPlugin = this;
		const existingRowContextMenuItemsFunction = gridOrPanel.getGridRowContextMenuItems
			? gridOrPanel.getGridRowContextMenuItems : () => [];
		gridOrPanel.getGridRowContextMenuItems = async (...args) => {
			const menuItems = await existingRowContextMenuItemsFunction(...args) || [];
			const investmentPluginItems = await investmentPlugin.getInvestmentRowContextMenuItems(...args);
			if (investmentPluginItems && investmentPluginItems.length > 0) {
				if (menuItems.length > 0) {
					menuItems.push('-');
				}
				menuItems.push(...investmentPluginItems);
			}
			return menuItems;
		};
	},

	getInvestmentRowContextMenuItems: async function(grid, rowId, record) {
		const investmentPlugin = this;
		const menuItems = [];

		if (!TCG.isTrue(await investmentPlugin.isNavigationAllowed(record))) {
			return menuItems;
		}

		await investmentPlugin.appendSecurityNavigationToMenu(record, menuItems);
		await investmentPlugin.appendInstrumentNavigationToMenu(record, menuItems);
		investmentPlugin.appendAccountNavigationToMenu(record, menuItems);

		return menuItems;
	},

	getMainWindowForm: function() {
		const gridPanel = this.grid.ownerGridPanel;
		if (gridPanel) {
			const window = gridPanel.getWindow();
			if (window && window.getMainForm) {
				return window.getMainForm();
			}
		}
		return void 0;
	},

	isAccountWindow: function() {
		const mainForm = this.getMainWindowForm();
		return mainForm && TCG.isNotBlank(mainForm.formValues['number']);
	},

	isClientAccountWindow: function() {
		return this.isAccountWindow() && TCG.isTrue(TCG.getValue('type.ourAccount', this.getMainWindowForm().formValues));
	},

	isSecurityWindow: function() {
		const mainForm = this.getMainWindowForm();
		return mainForm && TCG.isNotBlank(mainForm.formValues['symbol']);
	},

	isNavigationAllowed: async function(record) {
		const investmentPlugin = this;
		if (TCG.isFalse(this.filterForPositions)) {
			return true;
		}
		const accountingAccountName = TCG.getValue(investmentPlugin.recordAccountingAccountNamePath, record.json);
		if (TCG.isBlank(accountingAccountName)) {
			return false;
		}
		if (TCG.isTrue(this.enableAsyncLookups)) {
			const accountingAccount = await TCG.data.getDataPromiseUsingCaching(`accountingAccountByName.json?name=${accountingAccountName}`, this.grid, `accountingAccount.${accountingAccountName}`);
			return (TCG.isTrue(TCG.getValue('position', accountingAccount)) || TCG.isTrue(TCG.getValue('currency', accountingAccount)));
		}

		// return true if accountingAccountName includes Position, Cash, or Currency
		return /Position|Cash|Currency/.test(accountingAccountName);
	},

	isRecordCurrency: async function(record) {
		const accountingAccountName = TCG.getValue(this.recordAccountingAccountNamePath, record.json);
		if (TCG.isBlank(accountingAccountName)) {
			return false;
		}
		if (TCG.isTrue(this.enableAsyncLookups)) {
			const accountingAccount = await TCG.data.getDataPromiseUsingCaching(`accountingAccountByName.json?name=${accountingAccountName}`, this.grid, `accountingAccount.${accountingAccountName}`);
			return (TCG.isTrue(TCG.getValue('position', accountingAccount)) || TCG.isTrue(TCG.getValue('currency', accountingAccount)));
		}

		// return true if accountingAccountName includes Cash, or Currency
		return /Cash|Currency/.test(accountingAccountName);
	},

	appendSecurityNavigationToMenu: async function(record, menuItems) {
		const investmentPlugin = this;
		if (TCG.isTrue(await investmentPlugin.isSecurityNavigationAllowed(record))) {
			let securityLabel = 'Security';
			let openPositionTabName = 'Open Positions';
			if (TCG.isTrue(await investmentPlugin.isRecordCurrency(record))) {
				securityLabel = 'Currency';
				openPositionTabName = 'Holdings';
			}

			menuItems.push(
				{
					text: `View ${securityLabel}`,
					iconCls: 'stock-chart',
					menu: {
						xtype: 'menu',
						items: [
							{
								text: `View ${securityLabel}`,
								tooltip: 'Launch the security window for the security of this row',
								iconCls: 'stock-chart',
								handler: function() {
									investmentPlugin.navigateToSecurityWindow(record);
								}
							},
							{
								text: `View ${securityLabel} Open Positions`,
								tooltip: 'Launch the security window for the security of this row for the Open Positions tab',
								iconCls: 'stock-chart',
								handler: function() {
									investmentPlugin.navigateToSecurityWindow(record, openPositionTabName);
								}
							}
						]
					}
				}
			);
		}
	},

	appendInstrumentNavigationToMenu: async function(record, menuItems) {
		const investmentPlugin = this;
		if (TCG.isTrue(await this.isInstrumentNavigationAllowed(record))) {
			if (menuItems.length > 0) {
				menuItems.push('-');
			}
			menuItems.push(
				{
					text: 'View Instrument',
					iconCls: 'stock-chart',
					menu: {
						xtype: 'menu',
						items: [
							{
								text: 'View Instrument',
								tooltip: 'Launch the instrument window for the security of this row',
								iconCls: 'stock-chart',
								handler: function() {
									investmentPlugin.navigateToInstrumentWindow(record);
								}
							},
							{
								text: 'View Instrument Open Positions',
								tooltip: 'Launch the instrument window for the security of this row for the Open Positions tab',
								iconCls: 'stock-chart',
								handler: function() {
									investmentPlugin.navigateToInstrumentWindow(record, 'Open Positions');
								}
							}
						]
					}
				}
			);
		}
	},

	appendAccountNavigationToMenu: function(record, menuItems) {
		const investmentPlugin = this;
		const accountMenu = [];
		const accountWindow = this.isAccountWindow();
		if (TCG.isTrue(this.openClientAccount) && (!accountWindow || !this.isClientAccountWindow())) {
			accountMenu.push(
				{
					text: 'View Client Account',
					tooltip: 'Launch the client account window of this row',
					iconCls: 'account',
					handler: function() {
						investmentPlugin.navigateToAccountWindow(record);
					}
				}
			);
		}
		if (TCG.isTrue(this.openHoldingAccount) && (!accountWindow || this.isClientAccountWindow())) {
			if (accountMenu.length > 0) {
				accountMenu.push('-');
			}
			accountMenu.push(
				{
					text: 'View Holding Account',
					tooltip: 'Launch the holding account window of this row',
					iconCls: 'account',
					handler: function() {
						investmentPlugin.navigateToAccountWindow(record, true);
					}
				}
			);
		}
		if (accountMenu.length > 0) {
			if (menuItems.length > 0) {
				menuItems.push('-');
			}
			if (accountMenu.length === 1) {
				menuItems.push(...accountMenu);
			}
			else {
				menuItems.push(
					{
						text: 'View Account',
						iconCls: 'account',
						menu: {
							xtype: 'menu',
							items: accountMenu
						}
					}
				);
			}
		}
	},

	getSecurityIdFromRecord: function(record) {
		return this.getEntityIdFromRecord(record, this.recordSecurityIdPath, 'Security');
	},

	isSecurityNavigationAllowed: async function(record) {
		if (!TCG.isTrue(this.openSecurity)) {
			return false;
		}
		const securityId = this.getSecurityIdFromRecord(record);
		return !TCG.isBlank(securityId) && !TCG.isTrue(this.isSecurityWindow());

	},

	getInstrumentIdFromRecord: function(record) {
		return this.getEntityIdFromRecord(record, this.recordInstrumentIdPath, 'Instrument');
	},

	isInstrumentNavigationAllowed: async function(record) {
		if (!TCG.isTrue(this.openInstrument) || TCG.isBlank(this.recordInstrumentIdPath)) {
			return false;
		}

		const instrumentId = this.getInstrumentIdFromRecord(record);
		if (TCG.isBlank(instrumentId)) {
			return false;
		}

		const propertyPath = this.recordSecurityOnePerInstrumentPathOverride || this.recordInstrumentIdPath.replace('id', 'hierarchy.oneSecurityPerInstrument');
		let onePerInstrument = TCG.getValue(propertyPath, record.json);
		if (TCG.isBlank(onePerInstrument)) {
			if (TCG.isTrue(this.enableAsyncLookups)) {
				const instrumentOnePerInstrument = await TCG.data.getDataPromiseUsingCaching(`investmentInstrument.json?id=${instrumentId}`, this.grid, `instrument.onePerSecurity.${instrumentId}`, {
					requestedPropertiesRoot: 'data',
					requestedProperties: 'hierarchy.oneSecurityPerInstrument'
				});
				onePerInstrument = TCG.getValue('hierarchy.oneSecurityPerInstrument', instrumentOnePerInstrument);
			}
			else {
				console.warn('Investment navigation plugin: instrument navigate enabled with no way to look up security one per instrument (e.g., recordSecurityOnePerInstrumentPathOverride or enableAsyncLookups)');
			}
		}
		return TCG.isFalse(onePerInstrument);
	},


	navigateToSecurityWindow: function(record, defaultTabName) {
		const securityId = this.getSecurityIdFromRecord(record);
		if (TCG.isNotBlank(securityId)) {
			this.navigateToWindow('Clifton.investment.instrument.SecurityWindow', securityId, defaultTabName);
		}
	},
	navigateToInstrumentWindow: function(record, defaultTabName) {
		const instrumentId = this.getInstrumentIdFromRecord(record);
		if (TCG.isNotBlank(instrumentId)) {
			this.navigateToWindow('Clifton.investment.instrument.InstrumentWindow', instrumentId, defaultTabName);
		}
	},

	navigateToAccountWindow: function(record, holdingAccount) {
		const accountId = this.getEntityIdFromRecord(record, TCG.isTrue(holdingAccount) ? this.recordHoldingAccountIdPath : this.recordClientAccountIdPath, TCG.isTrue(holdingAccount) ? 'Holding Account' : 'Client Account');
		if (TCG.isNotBlank(accountId)) {
			this.navigateToWindow('Clifton.investment.account.AccountWindow', accountId);
		}
	},

	getEntityIdFromRecord: function(record, path, pathLabel) {
		const id = TCG.getValue(path, record.json);
		if (TCG.isBlank(id)) {
			TCG.showError(`Failed to find ${pathLabel} ID for row using property: ${path}`, `${pathLabel} Navigation`);
		}
		return id;
	},

	navigateToWindow: function(windowName, id, defaultTabName) {
		TCG.createComponent(windowName, {
			id: TCG.getComponentId(windowName, id),
			params: {id: id},
			openerCt: this.grid,
			defaultActiveTabName: defaultTabName
		});
	}
});
Ext.preg('investment-contextmenu-plugin', Clifton.investment.InvestmentContextMenuPlugin);
