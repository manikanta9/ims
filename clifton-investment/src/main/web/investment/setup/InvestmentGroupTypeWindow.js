Clifton.investment.setup.InvestmentGroupTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Instrument Group Type',
	iconCls: 'grouping',
	height: 425,
	width: 700,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		requiredFormIndex: 0,
		url: 'investmentGroupType.json',
		items: [
			{fieldLabel: 'Group Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Levels Deep', name: 'levelsDeep', xtype: 'spinnerfield'},
			{
				fieldLabel: 'Currency Type', name: 'currencyType', xtype: 'combo', displayField: 'name', valueField: 'name', mode: 'local',
				store: new Ext.data.ArrayStore({
					fields: ['name'],
					data: [
						['ALL'],
						['DOMESTIC'],
						['INTERNATIONAL']
					]
				})
			},
			{boxLabel: 'Template for other groups', name: 'template', xtype: 'checkbox', mutuallyExclusiveFields: ['basedOnTemplate']},
			{boxLabel: 'Based on Template (copy group item structure from the template but use different currency)', name: 'basedOnTemplate', xtype: 'checkbox', mutuallyExclusiveFields: ['template']},
			{boxLabel: 'Groups have only one Group Item (collection of instruments)', name: 'singleGroupItem', xtype: 'checkbox'},
			{boxLabel: 'Require Base Currency selection for Instrument Groups of this type', name: 'baseCurrencyRequired', xtype: 'checkbox'},
			{boxLabel: 'Allow assignments to leaf Group Items only', name: 'leafAssignmentsOnly', xtype: 'checkbox'},
			{boxLabel: 'Allow the same security to be assigned to more than one group item', name: 'duplicationAllowed', xtype: 'checkbox'},
			{boxLabel: 'Do not allow missing instrument (map everything: 100% of security universe)', name: 'missingNotAllowed', xtype: 'checkbox'}
		]
	}]
});
