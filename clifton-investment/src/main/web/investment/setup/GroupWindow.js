Clifton.investment.setup.GroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Instrument Group',
	iconCls: 'grouping',
	height: 550,
	width: 750,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Group',
				name: 'group-tab',
				items: [{
					xtype: 'formpanel',
					instructions: 'Instrument Groups are used to classify Investment Instruments/Securities for reporting (group by items) or filtering purposes.',
					url: 'investmentGroup.json',
					loadDefaultDataAfterRender: true,
					name: 'group-formpanel',
					getWarningMessage: function(form) {
						let msg = undefined;
						if (form.formValues.templateInvestmentGroup) {
							msg = 'This group is using a template to build and maintain group items and assignments.  These can only be edited through the group that is being used as the template.';
						}
						return msg;
					},

					items: [
						{fieldLabel: 'Group Type', name: 'investmentGroupType.name', detailIdField: 'investmentGroupType.id', xtype: 'linkfield', url: 'investmentGroupTypeListFind.json', detailPageClass: 'Clifton.investment.setup.InvestmentGroupTypeWindow'},
						{
							fieldLabel: 'Template', name: 'templateInvestmentGroup.name', hiddenName: 'templateInvestmentGroup.id', xtype: 'combo', url: 'investmentGroupListFind.json?hasTemplate=false&hasBaseCurrency=true', detailPageClass: 'Clifton.investment.setup.GroupWindow',
							qtip: 'Selected group that is the template is used to build group items and assignments.',
							requestedProps: 'tradableSecurityOnly|duplicationAllowed|levelsDeep|leafAssignmentOnly|missingInstrumentNotAllowed',
							listeners: {
								// reset schedule amount label/remove field enable/disable tiers based on selection
								select: function(combo, record, index) {
									combo.ownerCt.hideTemplateUpdatedProperties(record.json);
								}
							}
						},
						{fieldLabel: 'Group Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{xtype: 'system-entity-modify-condition-combo'},
						{
							fieldLabel: 'Base CCY', name: 'baseCurrency.symbol', hiddenName: 'baseCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', displayField: 'symbol',
							qtip: 'Available Only if assignments to leaf items only checked or using a Template.  Currency Type selections on the group items are used to compare to the base currency to determine if instruments apply.'
						},
						{fieldLabel: 'Items depth', name: 'levelsDeep', xtype: 'spinnerfield', maxValue: 5, value: 1},
						{boxLabel: 'Allow assignments to leaf Group Items only', name: 'leafAssignmentOnly', xtype: 'checkbox'},
						{boxLabel: 'Allow the same security to be assigned to more than one group item', name: 'duplicationAllowed', xtype: 'checkbox'},
						{
							boxLabel: 'Do not allow missing instrument (map everything: 100% of security universe)', name: 'missingInstrumentNotAllowed', xtype: 'checkbox',
							qtip: 'If checked, notifications will be generated if this group ever has any missing instruments unmapped.'
						},
						{
							boxLabel: 'Tradable Securities Only (specified on Investment Hierarchy)', name: 'tradableSecurityOnly', xtype: 'checkbox',
							qtip: 'Checking this box allows only instruments that belong to a hierarchy that allows trading.'
						},
						{boxLabel: 'System Defined (cannot insert, delete or change the name)', name: 'systemDefined', xtype: 'checkbox', disabled: true},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'InvestmentGroup',
							hierarchyCategoryName: 'Investment Group Tags'
						}
					],
					prepareDefaultData: function(dd) {
						const groupType = TCG.getValue('investmentGroupType', dd);
						this.hideGroupTypeFields(groupType);
						return dd;
					},
					listeners: {
						afterload: function(fp) {
							this.hideTemplateUpdatedProperties();
							fp.hideGroupTypeFields.call(fp, fp.getFormValue('investmentGroupType'));
							const f = this.getForm();
							const levelsDeep = TCG.getValue('investmentGroupType.levelsDeep', f.formValues);
							const singleGroupItem = TCG.getValue('investmentGroupType.singleGroupItem', f.formValues);
							const tabs = TCG.getParentByClass(fp, Ext.TabPanel);
							if (levelsDeep === 1 || singleGroupItem) {
								tabs.hideTabStripItem(TCG.getChildByName(tabs, 'group-items-tab'));
							}
							else {
								tabs.hideTabStripItem(TCG.getChildByName(tabs, 'assignments-tab'));
							}
						}
					},
					hideTemplateUpdatedProperties: function(record) {
						const f = this.getForm();
						if (!record) {
							record = TCG.getValue('templateInvestmentGroup.id', this.getForm().formValues);
						}
						else {
							// Was just selected:
							f.findField('tradableSecurityOnly').setValue(record.tradableSecurityOnly);
							f.findField('duplicationAllowed').setValue(record.duplicationAllowed);
							f.findField('levelsDeep').setValue(record.levelsDeep);
							f.findField('leafAssignmentOnly').setValue(record.leafAssignmentOnly);
							f.findField('missingInstrumentNotAllowed').setValue(record.missingInstrumentNotAllowed);
						}

						const readonly = TCG.isNotNull(record);
						f.findField('tradableSecurityOnly').setDisabled(readonly);
						f.findField('duplicationAllowed').setDisabled(readonly);
						f.findField('levelsDeep').setDisabled(readonly);
						f.findField('leafAssignmentOnly').setDisabled(readonly);
						f.findField('missingInstrumentNotAllowed').setDisabled(readonly);
					},
					hideGroupTypeFields: function(groupType) {
						if (!groupType.basedOnTemplate) {
							this.hideField('templateInvestmentGroup.name');
						}
						if (groupType.name !== 'Other' && groupType.name !== 'Template Group') {
							// If 'Other' type, show fields for customizability - otherwise, hide group type defined fields
							this.hideField('leafAssignmentOnly');
							this.hideField('missingInstrumentNotAllowed');
						}
						if (groupType.levelsDeep) {
							this.hideField('levelsDeep');
						}
						if (groupType.singleGroupItem) {
							this.hideField('duplicationAllowed');
						}
						if (groupType.currencyType === 'BOTH') {
							this.hideField('baseCurrency.symbol');
						}
					}
				}]
			},


			{
				title: 'Group Items',
				name: 'group-items-tab',
				items: [{
					name: 'investmentGroupItemListFind',
					xtype: 'treegrid',
					instructions: 'The following items are associated with this group. Group Items are hierarchical and are used to classify investment instruments/securities for reporting purposes.',
					appendStandardColumns: false, // to avoid UI glitch
					columns: [
						{header: 'Group Item Name', width: 200, dataIndex: 'name'},
						{header: 'Order', width: 50, dataIndex: 'order', hidden: true, type: 'int', useNull: true},
						{header: 'Item Type', width: 150, dataIndex: 'itemType'},
						{header: 'Currency Type', width: 100, dataIndex: 'currencyType', storeData: Clifton.investment.instrument.CurrencyTypes},
						{header: 'Description', width: 250, dataIndex: 'description'}
					],

					editor: {
						detailPageClass: 'Clifton.investment.setup.GroupItemWindow',
						deleteURL: 'investmentGroupItemDelete.json',
						getDefaultData: function(treePanel) { // defaults group id for the detail page
							return {
								group: treePanel.getWindow().getMainForm().formValues
							};
						},

						addEditButtons: function(toolBar) {
							const tree = this.tree;

							toolBar.add({
								iconCls: 'expand-all',
								tooltip: 'Expand or Collapse all items',
								scope: this,
								handler: function() {
									tree.collapsed = !tree.collapsed;
									if (TCG.isTrue(tree.collapsed)) {
										tree.collapseAll();
									}
									else {
										tree.expandAll();
									}
								}
							});
							toolBar.add('-');

							const usesTemplate = TCG.getValue('templateInvestmentGroup.id', tree.getWindow().getMainForm().formValues);
							if (TCG.isNull(usesTemplate)) {
								TCG.tree.TreeGridEditor.prototype.addEditButtons.apply(this, arguments);
							}
							else {
								toolBar.add({
									text: 'Rebuild',
									tooltip: 'Rebuild group items/matrix from template',
									iconCls: 'run',
									handler: function() {
										const loader = new TCG.data.JsonLoader({
											waitTarget: tree,
											timeout: 40000, // If data goes back a long time
											waitMsg: 'Rebuilding...',
											params: {groupId: tree.getWindow().getMainFormId()},
											onLoad: function() {
												tree.reload();
											}
										});
										loader.load('investmentGroupItemListRebuild.json');
									}
								});
								toolBar.add('-');
							}
						}
					},
					getLoadParams: function() {
						return {'groupId': this.getWindow().getMainFormId()};
					},
					getRootParams: function() {
						return {rootNodesOnly: true};
					}
				}]
			},


			{
				title: 'Group Assignments',
				name: 'assignments-tab',
				items: [{
					name: 'investmentGroupMatrixListByGroup',
					xtype: 'gridpanel',
					instructions: 'The following assignments are tied to this group.',
					columns: [
						{header: 'Assignment', width: 220, dataIndex: 'label', sortable: false, filter: false}, // can be one of 3 fields: how do we sort/filter
						{header: 'Group Item', dataIndex: 'groupItem.name', width: 65, hidden: true},
						{header: 'Investment Type', width: 100, hidden: true, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Investment Sub Type', width: 100, hidden: true, dataIndex: 'subType.name', filter: false, sortable: false},
						{header: 'Investment Sub Type2', width: 100, hidden: true, dataIndex: 'subType2.name', filter: false, sortable: false},
						{header: 'Investment Hierarchy', width: 100, hidden: true, dataIndex: 'hierarchy.nameExpanded', filter: {type: 'combo', searchFieldName: 'hierarchyId', displayField: 'nameExpanded', url: 'investmentInstrumentHierarchyListFind.json', queryParam: 'nameExpanded', listWidth: 600}},
						{header: 'Investment Instrument', width: 100, hidden: true, dataIndex: 'instrument.label', filter: {type: 'combo', searchFieldName: 'instrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},
						{header: 'CCY Type', width: 50, dataIndex: 'groupItem.currencyType', storeData: Clifton.investment.instrument.CurrencyTypes},
						{header: 'CCY Denom', width: 50, dataIndex: 'tradingCurrency.name', filter: {type: 'combo', searchFieldName: 'tradingCurrencyId', displayField: 'name', url: 'investmentSecurityListFind.json?currency=true'}, tooltip: 'Currency Denomination'},
						{header: 'Country of Risk', width: 65, dataIndex: 'countryOfRisk.text', filter: false, sortable: false},
						{header: 'Ultimate Issuer', width: 100, hidden: true, dataIndex: 'ultimateBusinessCompany.name', filter: {type: 'combo', searchFieldName: 'ultimateBusinessCompanyId', displayField: 'name', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Recursive', width: 50, dataIndex: 'includeSubHierarchies', type: 'boolean'},
						{header: 'Excluded', width: 50, dataIndex: 'excluded', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.setup.GroupMatrixWindow',
						deleteURL: 'investmentGroupMatrixDelete.json',
						getDefaultData: function(gridPanel) { // defaults groupItem for the detail page
							const groupItems = TCG.data.getData('investmentGroupItemListByGroup.json?groupId=' + gridPanel.getWindow().getMainFormId(), gridPanel);
							const groupItem = groupItems[0];
							return {
								groupItem: groupItem
							};
						},
						addEditButtons: function(toolBar, gridPanel) {
							const usesTemplate = TCG.getValue('group.templateInvestmentGroup.id', gridPanel.getWindow().getMainForm().formValues);
							if (TCG.isNull(usesTemplate)) {
								TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
							}
						}
					},
					getLoadParams: function() {
						return {
							requestedMaxDepth: 3,
							groupId: this.getWindow().getMainFormId()
						};
					}
				}]
			},


			{
				title: 'Included Instruments',
				items: [{
					name: 'investmentGroupItemInstrumentListFind',
					xtype: 'gridpanel',
					instructions: 'The following instrument assignments are associated with this group. These assignments are built from the Group Item Matrix table.  Click Rebuild to rebuild the data.',
					additionalPropertiesToRequest: 'referenceTwo.id',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Group Item', width: 100, dataIndex: 'referenceOne.labelExpanded', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Investment Type', width: 60, dataIndex: 'referenceTwo.hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}, hidden: true},
						{header: 'Sub Type', width: 60, dataIndex: 'referenceTwo.hierarchy.investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}, hidden: true},
						{header: 'Sub Type 2', width: 60, dataIndex: 'referenceTwo.hierarchy.investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}, hidden: true},
						{header: 'Investment Hierarchy', width: 100, dataIndex: 'referenceTwo.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'instrumentHierarchyId', displayField: 'labelExpanded', queryParam: 'labelExpanded', listWidth: 600, url: 'investmentInstrumentHierarchyListFind.json'}},
						{header: 'Instrument Name', width: 100, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'instrumentName'}},
						{header: 'Description', width: 200, dataIndex: 'referenceTwo.description', hidden: true, filter: {searchFieldName: 'instrumentDescription'}},
						{header: 'Primary Exchange', width: 100, dataIndex: 'referenceTwo.exchange.label', filter: false},
						{header: 'CCY Denomination', width: 70, dataIndex: 'referenceTwo.tradingCurrency.name', filter: false},
						{header: 'Country of Risk', width: 60, dataIndex: 'referenceTwo.countryOfRisk.text', filter: false, hidden: true},
						{header: 'Inactive', width: 40, dataIndex: 'inactive', type: 'boolean', filter: false, hidden: true}
					],
					getTopToolbarInitialLoadParams: function() {
						return {
							groupId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.referenceTwo.id;
						},
						addEditButtons: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Rebuild',
								tooltip: 'Rebuild Instrument Group Item Instruments Relationships.',
								iconCls: 'run',
								handler: function() {
									const params = {groupId: gridPanel.getWindow().getMainFormId()};
									const loader = new TCG.data.JsonLoader({
										waitTarget: gridPanel,
										waitMsg: 'Rebuilding...',
										params: params
									});
									loader.load('investmentGroupItemInstrumentListByGroupRebuild.json');
								}
							});
							toolBar.add('-');
						}
					},
					listeners: {
						afterrender: function(gp) {
							const tabs = TCG.getParentByClass(gp, Ext.TabPanel);
							const formTab = TCG.getChildByName(tabs, 'group-tab');
							const f = TCG.getChildByName(formTab, 'group-formpanel').getForm();
							const levelsDeep = TCG.getValue('investmentGroupType.levelsDeep', f.formValues);
							const singleGroupItem = TCG.getValue('investmentGroupType.singleGroupItem', f.formValues);
							if (singleGroupItem || levelsDeep === 1) {
								const cm = this.getColumnModel();
								cm.setHidden(cm.findColumnIndex('referenceOne.labelExpanded'), true);
							}
						}
					}
				}]
			},


			{
				title: 'Missing Instruments',
				items: [{
					name: 'investmentInstrumentMissingByGroupListFind',
					xtype: 'gridpanel',
					instructions: 'The following instruments are missing assignments from this group. Assignments are built from the Group Item Matrix table.  Click Rebuild to rebuild the data.',
					columns: [
						{header: 'Investment Type', width: 60, dataIndex: 'hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}, hidden: true},
						{header: 'Sub Type', width: 60, dataIndex: 'hierarchy.investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}, hidden: true},
						{header: 'Sub Type 2', width: 60, dataIndex: 'hierarchy.investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}, hidden: true},
						{header: 'Investment Hierarchy', width: 200, dataIndex: 'hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'hierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Instrument Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Primary Exchange', width: 70, dataIndex: 'exchange.label', filter: {type: 'combo', searchFieldName: 'exchangeId', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=false'}},
						{header: 'CCY Denomination', width: 70, dataIndex: 'tradingCurrency.name', filter: {type: 'combo', searchFieldName: 'tradingCurrencyId', displayField: 'name', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Country of Risk', width: 60, dataIndex: 'countryOfRisk.text', filter: false, hidden: true},
						{header: 'Inactive', width: 40, dataIndex: 'inactive', type: 'boolean', hidden: true}
					],
					getLoadParams: function() {
						return {'groupId': this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.investment.instrument.InstrumentWindow'
					}
				}]
			}
		]
	}]
});

