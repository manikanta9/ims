Clifton.investment.setup.ExchangeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Exchange',
	iconCls: 'stock-chart',
	width: 800,
	height: 650,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Exchange',
				items: [{
					xtype: 'formpanel',
					labelWidth: 130,
					instructions: 'An exchange is a marketplace in which financial instruments are traded. Exchanges give companies, governments and other groups a platform to sell securities to the investing public and provides regulation to ensure fair and orderly trading, as well as efficient dissemination of price information for any securities trading on that exchange.  It may or may not be a physical location, but could also be an electronic platform.',
					url: 'investmentExchange.json',

					getWarningMessage: function(form) {
						let msg = undefined;
						if (TCG.isFalse(form.formValues.open)) {
							msg = 'This exchange is currently closed.';
						}
						return msg;
					},

					items: [
						{fieldLabel: 'Parent Exchange', name: 'parent.name', hiddenName: 'parent.id', xtype: 'combo', url: 'investmentExchangeListFind.json', detailPageClass: 'Clifton.investment.setup.ExchangeWindow'},
						{fieldLabel: 'Exchange Name', name: 'name'},
						{fieldLabel: 'Exchange Code', name: 'exchangeCode'},
						{fieldLabel: 'Market Identifier Code', name: 'marketIdentifierCode', qtip: 'ISO 10383 Market Identifier Code (MIC)'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Composite Exchange', name: 'compositeExchange', xtype: 'checkbox', boxLabel: 'Used to calculate average or standardized prices for a security across all exchanges where it trades.'},
						{fieldLabel: 'Base Currency', name: 'baseCurrency.name', hiddenName: 'baseCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Country', name: 'country.text', hiddenName: 'country.id', xtype: 'combo', displayField: 'text', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Countries'},
						{fieldLabel: 'Calendar', name: 'calendar.name', hiddenName: 'calendar.id', xtype: 'combo', url: 'calendarListFind.json', detailPageClass: 'Clifton.calendar.setup.CalendarWindow'},
						{fieldLabel: 'Time Zone', name: 'timeZone.label', hiddenName: 'timeZone.id', displayField: 'label', xtype: 'combo', url: 'calendarTimeZoneListFind.json'},

						{
							fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
							defaults: {
								xtype: 'displayfield',
								flex: 1
							},
							items: [
								{value: 'Exchange Time Zone'},
								{name: 'localTimeZoneLabel'}
							]
						},
						{
							fieldLabel: 'Open Time', xtype: 'compositefield', qtip: 'The time when this Exchange opens for a full day of work: a day that is not full or partial holiday.',
							defaults: {
								xtype: 'timefield',
								flex: 1
							},
							items: [
								{name: 'openTime'},
								{name: 'openTimeLocal', submitValue: false, disabled: true}
							]
						},
						{
							fieldLabel: 'Close Time', xtype: 'compositefield', qtip: 'The time when this Exchange closes for a full day of work: a day that is not full or partial holiday.',
							defaults: {
								xtype: 'timefield',
								flex: 1
							},
							items: [
								{name: 'closeTime'},
								{name: 'closeTimeLocal', submitValue: false, disabled: true}
							]
						},
						{
							fieldLabel: 'Partial Day Open Time', xtype: 'compositefield', qtip: 'The time when this Exchange opens on a Partial Holiday: corresponding Calendar has a holiday on this day and the Holiday is NOT marked as Full Day.',
							defaults: {
								xtype: 'timefield',
								flex: 1
							},
							items: [
								{name: 'openTimePartialDay'},
								{name: 'openTimePartialDayLocal', submitValue: false, disabled: true}
							]
						},
						{
							fieldLabel: 'Partial Day Close Time', xtype: 'compositefield', qtip: 'The time when this Exchange closes on a Partial Holiday: corresponding Calendar has a holiday on this day and the Holiday is NOT marked as Full Day.',
							defaults: {
								xtype: 'timefield',
								flex: 1
							},
							items: [
								{name: 'closeTimePartialDay'},
								{name: 'closeTimePartialDayLocal', submitValue: false, disabled: true}
							]
						}
					]
				}]
			},


			{
				title: 'Instruments',
				items: [{
					name: 'investmentInstrumentListFind',
					xtype: 'gridpanel',
					instructions: 'The following investment instruments are traded on selected exchange.',
					columns: [
						{header: 'ID', width: 50, dataIndex: 'id', hidden: true},
						{header: 'Investment Hierarchy', width: 120, dataIndex: 'hierarchy.labelExpanded', defaultSortColumn: true, filter: {searchFieldName: 'hierarchyName'}},
						{header: 'Prefix', width: 50, dataIndex: 'identifierPrefix'},
						{header: 'Instrument Name', width: 100, dataIndex: 'name'},
						{header: 'Underlying', width: 100, dataIndex: 'underlyingInstrument.name', filter: {type: 'combo', searchFieldName: 'underlyingInstrumentId', displayField: 'name', url: 'investmentInstrumentListFind.json'}},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'CCY Denom', width: 80, dataIndex: 'tradingCurrency.name', filter: {type: 'combo', searchFieldName: 'tradingCurrencyId', displayField: 'name', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Multiplier', width: 50, dataIndex: 'priceMultiplier', type: 'float'},
						{header: 'Deliverable', width: 50, dataIndex: 'deliverable', type: 'boolean', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
						drillDownOnly: true
					},
					getLoadParams: function() {
						return {
							exchangeId: this.getWindow().getMainFormId()
						};
					}
				}]
			},


			{
				title: 'Holidays',
				items: [{
					name: 'calendarHolidayDayListFind',
					xtype: 'gridpanel',
					instructions: 'The following holidays are defined for this Calendar of this Exchange.',
					importTableName: 'CalendarHolidayDay',
					getLoadParams: function(firstLoad) {
						const calendarId = this.getWindow().getMainFormPanel().getFormValue('calendar.id');
						if (TCG.isBlank(calendarId)) {
							return false;
						}
						if (firstLoad) {
							this.setFilterValue('day.startDate', {'after': new Date().add(Date.DAY, -90)});
						}
						return {'calendarId': calendarId};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Holiday', width: 150, dataIndex: 'holiday.name'},
						{header: 'Day', width: 100, dataIndex: 'day.startDate', filter: {searchFieldName: 'startDate'}, defaultSortColumn: true},
						{header: 'Weekday', width: 100, dataIndex: 'day.weekday.name'},
						{header: 'Year', width: 70, dataIndex: 'day.year.year', type: 'int', doNotFormat: true},
						{header: 'Trade', width: 70, dataIndex: 'tradeHoliday', type: 'boolean', tooltip: 'Most holidays are just holidays. However, investment management industry can differentiate between Trading and Settlement holidays: <li>Trading: the specified market is closed for trading; therefore, price settlement data are not published. Note: in cases where electronic trading schedules are supplied separately from floor trading, the latter should be used to determine business and settlement data holidays)</li><li>Settlement: relates to cash settlement and clearing of trades; settlement of trades may not be possible because banks are closed, clearing agency is closed, specified market is close, or some combination of the above.</li><p>When applicable, use this field to determine if it is a trading day holiday.'},
						{header: 'Settle', width: 70, dataIndex: 'settlementHoliday', type: 'boolean', tooltip: 'Most holidays are just holidays. However, investment management industry can differentiate between Trading and Settlement holidays: <li>Trading: the specified market is closed for trading; therefore, price settlement data are not published. Note: in cases where electronic trading schedules are supplied separately from floor trading, the latter should be used to determine business and settlement data holidays)</li><li>Settlement: relates to cash settlement and clearing of trades; settlement of trades may not be possible because banks are closed, clearing agency is closed, specified market is close, or some combination of the above.</li><p>When applicable, use this field to determine if it is a settlement day holiday.'},
						{header: 'Full Day', width: 70, dataIndex: 'fullDayHoliday', type: 'boolean', tooltip: 'Most holidays are full day holidays.  However, some investment exchanges may have partial day holidays. In case of a partial day holiday, partial day start/end times should be used in order to determine if the exchange is open.'}
					]
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'InvestmentExchange'
				}]
			}
		]
	}]
});
