Clifton.investment.setup.GroupMatrixWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Instrument Group Matrix',
	iconCls: 'grouping',
	width: 700,
	height: 450,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'An instrument group matrix assigns or excludes investment instrument(s) matched by the following selections.',
		url: 'investmentGroupMatrix.json',
		labelFieldName: 'groupItem.nameExpanded',
		labelWidth: 135,
		items: [
			{fieldLabel: 'Group Name', name: 'groupItem.group.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.GroupWindow', detailIdField: 'groupItem.group.id'},
			{fieldLabel: 'Group Item', name: 'groupItem.nameExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.GroupItemWindow', detailIdField: 'groupItem.id'},
			{
				fieldLabel: 'CCY Type', name: 'groupItem.currencyType', hiddenName: 'groupItem.currencyType', mode: 'local', xtype: 'combo', readOnly: true,
				emptyText: '',
				store: {xtype: 'arraystore', data: Clifton.investment.instrument.CurrencyTypes}
			},
			{fieldLabel: 'Investment Type', name: 'type.name', hiddenName: 'type.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'investmentTypeList.json', mutuallyExclusiveFields: ['hierarchy.id', 'instrument.id']},
			{
				fieldLabel: 'Investment Sub Type', name: 'subType.name', hiddenName: 'subType.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'investmentTypeSubTypeListByType.json', mutuallyExclusiveFields: ['hierarchy.id', 'instrument.id'], requiredFields: ['type.id'], qtip: 'Investment sub-types can be used to identify similar securities that reside in different hierarchies.',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const form = combo.getParentForm().getForm();

						combo.store.baseParams = {
							investmentTypeId: form.findField('type.id').getValue()
						};
					}
				}
			},
			{
				fieldLabel: 'Investment Sub Type 2', name: 'subType2.name', hiddenName: 'subType2.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'investmentTypeSubType2ListByType.json', mutuallyExclusiveFields: ['hierarchy.id', 'instrument.id'], requiredFields: ['type.id'], qtip: 'Investment sub-types 2 can be used to identify similar securities that reside in different hierarchies.',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const form = combo.getParentForm().getForm();

						combo.store.baseParams = {
							investmentTypeId: form.findField('type.id').getValue()
						};
					}
				}
			},
			{fieldLabel: 'Investment Hierarchy', name: 'hierarchy.labelExpanded', hiddenName: 'hierarchy.id', displayField: 'labelExpanded', xtype: 'combo', url: 'investmentInstrumentHierarchyListFind.json', queryParam: 'labelExpanded', listWidth: 600, mutuallyExclusiveFields: ['type.id', 'instrument.id'], detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow'},
			{boxLabel: 'Include all instruments that belong to selected hierarchy and all of its sub-hierarchies', name: 'includeSubHierarchies', xtype: 'checkbox', mutuallyExclusiveFields: ['type.id', 'instrument.id'], requiredFields: ['hierarchy.id']},
			{fieldLabel: 'Investment Instrument', name: 'instrument.label', hiddenName: 'instrument.id', displayField: 'label', xtype: 'combo', url: 'investmentInstrumentListFind.json', mutuallyExclusiveFields: ['type.id', 'hierarchy.id', 'ultimateBusinessCompany.id'], detailPageClass: 'Clifton.investment.instrument.InstrumentWindow'},
			{fieldLabel: 'Currency Denomination', name: 'tradingCurrency.name', hiddenName: 'tradingCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
			{fieldLabel: 'Country of Risk', name: 'countryOfRisk.label', hiddenName: 'countryOfRisk.id', xtype: 'combo', url: 'systemListItemListFind.json?listName=Countries', displayField: 'label', detailPageClass: 'Clifton.system.list.ItemWindow', qtip: 'Filter based on country of risk'},
			{fieldLabel: 'Ultimate Issuer', name: 'ultimateBusinessCompany.nameExpandedWithType', hiddenName: 'ultimateBusinessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true', displayField: 'nameExpandedWithType', detailPageClass: 'Clifton.business.company.CompanyWindow', mutuallyExclusiveFields: ['instrument.id'], qtip: 'Filter all instruments with a one to one security mapping by the ultimate issuer of its designated security.'},
			{boxLabel: 'Exclude selected instruments from this group item', name: 'excluded', xtype: 'checkbox'}
		]
	}]
});
