Clifton.investment.setup.GroupItemWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Instrument Group Item',
	iconCls: 'grouping',
	width: 750,
	height: 550,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Group Item',
				items: [{
					xtype: 'formpanel',
					instructions: 'Group Items are hierarchical and are used to classify investment instruments/securities for reporting purposes.',
					url: 'investmentGroupItem.json',

					getWarningMessage: function(form) {
						let msg = undefined;
						if (form.formValues.group.templateInvestmentGroup) {
							msg = 'This group is using a template to build and maintain group items and assignments.  These can only be edited through the group that is being used as the template.';
						}
						return msg;
					},

					items: [
						{fieldLabel: 'Instrument Group', name: 'group.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.GroupWindow', detailIdField: 'group.id'},
						{name: 'group.baseCurrency.id', xtype: 'hidden'},
						{name: 'group.templateInvestmentGroup.id', xtype: 'hidden'},
						{
							fieldLabel: 'Parent Item', name: 'parent.name', hiddenName: 'parent.id', xtype: 'combo', url: 'investmentGroupItemListFind.json', detailPageClass: 'Clifton.investment.setup.GroupItemWindow',
							beforequery: function(queryEvent) {
								queryEvent.combo.store.baseParams = {
									groupId: queryEvent.combo.getParentForm().getForm().findField('group.id').getValue()
								};
							}
						},
						{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield'},
						{fieldLabel: 'Name', name: 'name'},
						{fieldLabel: 'Item Type', name: 'itemType', qtip: 'Can be used in reporting to group related items: Bonds, etc.'},
						{
							fieldLabel: 'CCY Type', name: 'currencyType.label', hiddenName: 'currencyType', mode: 'local', xtype: 'combo',
							emptyText: '',
							store: {xtype: 'arraystore', data: Clifton.investment.instrument.CurrencyTypes}
						},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
					],

					listeners: {
						afterload: function(form, isClosing) {
							this.hideCurrencyTypeField();
							const f = this.getForm();
							const singleGroupItem = TCG.getValue('group.investmentGroupType.singleGroupItem', f.formValues);
							if (singleGroupItem) {
								this.setReadOnly(true);
							}
						}
					},

					hideCurrencyTypeField: function() {
						const f = this.getForm();

						if (TCG.isBlank(TCG.getValue('group.baseCurrency.id', f.formValues))) {
							f.findField('currencyType.label').setValue('');
							f.findField('currencyType.label').setVisible(false);
						}
						if (f.formValues.group.templateInvestmentGroup) {
							this.setReadOnly(true);
						}
					}
				}]
			},


			{
				title: 'Assignment Rules',
				items: [{
					name: 'investmentGroupMatrixListFind',
					xtype: 'gridpanel',
					instructions: 'The following assignments are tied to this group item.',
					columns: [
						{header: 'Assignment', width: 220, dataIndex: 'label', sortable: false, filter: false}, // can be one of 3 fields: how do we sort/filter
						{header: 'Investment Type', width: 100, hidden: true, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Investment Sub Type', width: 100, hidden: true, dataIndex: 'subType.name', filter: false, sortable: false},
						{header: 'Investment Sub Type2', width: 100, hidden: true, dataIndex: 'subType2.name', filter: false, sortable: false},
						{header: 'Investment Hierarchy', width: 100, hidden: true, dataIndex: 'hierarchy.nameExpanded', filter: {type: 'combo', searchFieldName: 'hierarchyId', displayField: 'nameExpanded', url: 'investmentInstrumentHierarchyListFind.json', queryParam: 'nameExpanded', listWidth: 600}},
						{header: 'Investment Instrument', width: 100, hidden: true, dataIndex: 'instrument.label', filter: {type: 'combo', searchFieldName: 'instrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},
						{header: 'CCY Denom', width: 50, dataIndex: 'tradingCurrency.name', filter: {type: 'combo', searchFieldName: 'tradingCurrencyId', displayField: 'name', url: 'investmentSecurityListFind.json?currency=true'}, tooltip: 'Currency Denomination'},
						{header: 'Country of Risk', width: 65, dataIndex: 'countryOfRisk.text', filter: false, sortable: false},
						{header: 'Ultimate Issuer', width: 100, hidden: true, dataIndex: 'ultimateBusinessCompany.name', filter: {type: 'combo', searchFieldName: 'ultimateBusinessCompanyId', displayField: 'name', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Recursive', width: 50, dataIndex: 'includeSubHierarchies', type: 'boolean'},
						{header: 'Excluded', width: 50, dataIndex: 'excluded', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.setup.GroupMatrixWindow',
						deleteURL: 'investmentGroupMatrixDelete.json',
						getDefaultData: function(gridPanel) { // defaults groupItem for the detail page
							return {
								groupItem: gridPanel.getWindow().getMainForm().formValues
							};
						},
						addEditButtons: function(toolBar, gridPanel) {
							const usesTemplate = TCG.getValue('group.templateInvestmentGroup.id', gridPanel.getWindow().getMainForm().formValues);
							if (TCG.isNull(usesTemplate)) {
								TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
							}
						}
					},
					getLoadParams: function() {
						return {
							requestedMaxDepth: 3,
							groupItemId: this.getWindow().getMainFormId()
						};
					}
				}]
			},


			{
				title: 'Included Instruments',
				items: [{
					name: 'investmentGroupItemInstrumentListFind',
					xtype: 'gridpanel',
					instructions: 'The following instruments are tied to this group item.',
					additionalPropertiesToRequest: 'referenceTwo.id',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Investment Type', width: 60, dataIndex: 'referenceTwo.hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}, hidden: true},
						{header: 'Sub Type', width: 60, dataIndex: 'referenceTwo.hierarchy.investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}, hidden: true},
						{header: 'Sub Type 2', width: 60, dataIndex: 'referenceTwo.hierarchy.investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}, hidden: true},
						{header: 'Hierarchy', width: 120, dataIndex: 'referenceTwo.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'instrumentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Instrument Name', width: 100, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'instrumentName'}},
						{header: 'Description', width: 200, dataIndex: 'referenceTwo.description', hidden: true, filter: {searchFieldName: 'instrumentDescription'}},
						{header: 'Primary Exchange', width: 100, dataIndex: 'referenceTwo.exchange.label', filter: false},
						{header: 'Country of Risk', width: 100, dataIndex: 'countryOfRisk.text', filter: {type: 'combo', searchFieldName: 'countryOfRiskId', displayField: 'text', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Countries'}, hidden: true},
						{header: 'CCY Denomination', width: 70, dataIndex: 'referenceTwo.tradingCurrency.name', filter: false},
						{header: 'Country of Risk', width: 60, dataIndex: 'referenceTwo.countryOfRisk.text', filter: false, hidden: true},
						{header: 'Inactive', width: 40, dataIndex: 'inactive', type: 'boolean', filter: false, hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.referenceTwo.id;
						},
						deleteEnabled: false,
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add({
								text: 'Rebuild',
								tooltip: 'Rebuild Instrument Group Item Instruments Relationships.',
								iconCls: 'run',
								handler: function() {
									const params = {groupId: gridPanel.getWindow().getMainForm().formValues.group.id};
									const loader = new TCG.data.JsonLoader({
										waitTarget: gridPanel,
										waitMsg: 'Rebuilding...',
										params: params
									});
									loader.load('investmentGroupItemInstrumentListByGroupRebuild.json');
								}
							});
							toolBar.add('-');
						}
					},
					getTopToolbarInitialLoadParams: function() {
						return {'groupItemId': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
