Clifton.investment.setup.InvestmentListWindow = Ext.extend(TCG.app.Window, {
	id: 'investmentListWindow',
	title: 'Investments',
	iconCls: 'stock-chart',
	width: 1700,
	height: 700,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.investment.setup.InvestmentListWindowAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			const o = arr[i];
			tabs.add((typeof o === 'function') ? o(w) : o);
		}
	},

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Instruments',
				layout: 'border',

				items: [
					{
						region: 'west',
						xtype: 'treepanel-advanced',
						id: 'investmentInstrumentTree',
						title: 'Investment Hierarchy',
						width: 275,
						rootVisible: false,
						root: {
							nodeType: 'async',
							text: 'Instruments'
						},
						detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow',
						openDetailWindowAsModal: false,
						deleteURL: 'investmentInstrumentHierarchyDelete.json',
						loader: new TCG.tree.JsonTreeLoader({
							dataUrl: 'investmentInstrumentHierarchyListFind.json',
							getRootParams: function() {
								return {rootNodesOnly: true};
							}
						}),
						onNodeSelected: function(id, node) {
							const grid = Ext.getCmp('investmentInstrumentList');
							grid.additionalLoadParams = TCG.isNull(id) ? {} : {hierarchyId: id};
							this.selectedNodeData = node.nodeData;
							grid.reload(true); // always start with first page
						}
					},


					{
						region: 'center',
						layout: 'fit',
						items: [{
							xtype: 'gridpanel-custom-json-fields',
							tableName: 'InvestmentInstrument',
							id: 'investmentInstrumentList',
							name: 'investmentInstrumentListFind',
							importTableName: 'InvestmentSecurity',
							importComponentName: 'Clifton.investment.instrument.upload.SecurityUploadWindow',
							instructions: 'Investment instrument is a grouping of securities. Each futures instrument generally has 4 securities per year, etc.',
							groupField: 'hierarchy.labelExpanded',
							groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Instruments" : "Instrument"]})',
							autoLoad: false,
							pageSize: 200,
							folderView: true,
							nonCustomColumns: [
								{header: 'ID', width: 50, dataIndex: 'id', hidden: true},
								{header: 'Investment Type', width: 60, dataIndex: 'hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}, hidden: true},
								{header: 'Sub Type', width: 60, dataIndex: 'hierarchy.investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}, hidden: true},
								{header: 'Sub Type 2', width: 60, dataIndex: 'hierarchy.investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}, hidden: true},
								{header: 'Investment Hierarchy', width: 130, dataIndex: 'hierarchy.labelExpanded', filter: {searchFieldName: 'hierarchyName'}, hidden: true},
								{header: 'Prefix', width: 70, dataIndex: 'identifierPrefix', defaultSortColumn: true},
								{header: 'Instrument Name', width: 120, dataIndex: 'name'},
								{header: 'Investment Account', width: 100, dataIndex: 'investmentAccount.label', filter: {type: 'combo', searchFieldName: 'investmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
								{header: 'Underlying', width: 120, dataIndex: 'underlyingInstrument.name', filter: {type: 'combo', searchFieldName: 'underlyingInstrumentId', displayField: 'name', url: 'investmentInstrumentListFind.json'}},
								{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
								{header: 'Group Name', width: 100, dataIndex: 'groupName', hidden: true},
								{header: 'Country of Risk', width: 100, dataIndex: 'countryOfRisk.text', filter: {type: 'combo', searchFieldName: 'countryOfRiskId', displayField: 'text', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Countries'}},
								{header: 'Country of Incorporation', width: 100, dataIndex: 'countryOfIncorporation.text', filter: {type: 'combo', searchFieldName: 'countryOfIncorporationId', displayField: 'text', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Countries'}},
								{header: 'Primary Exchange', width: 120, dataIndex: 'exchange.label', filter: {type: 'combo', searchFieldName: 'exchangeId', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=false'}},
								{header: 'Composite Exchange', width: 120, dataIndex: 'compositeExchange.label', filter: {type: 'combo', searchFieldName: 'compositeExchangeId', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=true'}, hidden: true},
								{header: 'Settlement Calendar', width: 100, dataIndex: 'settlementCalendar.name', filter: {type: 'combo', searchFieldName: 'settlementCalendarId', url: 'calendarListFind.json'}, hidden: true},
								{header: 'CCY Denomination', width: 65, dataIndex: 'tradingCurrency.name', filter: {type: 'combo', searchFieldName: 'tradingCurrencyId', displayField: 'name', url: 'investmentSecurityListFind.json?currency=true'}},
								{header: 'Days to Settle', width: 50, dataIndex: 'daysToSettle', type: 'int', useNull: true, hidden: true},
								{header: 'Price Multiplier', width: 60, dataIndex: 'priceMultiplier', type: 'float'},
								{header: 'Exposure Multiplier', width: 50, dataIndex: 'exposureMultiplier', type: 'float', hidden: true, tooltip: 'Exposure Multiplier used for a few futures (Euro dollar and Euro Yen = 4) where the exposure is different than the notional value and needs to be accounted for by this multiplier. Example: Euro Dollar has contract size of $1,000,000 (multiplied by 4 to get annualized equivalent of quarterly interest rate: 12 / 3 = 4). Exposure = Price * Quantity * Multiplier * 4 where 4 is the exposure multiplier'},
								{header: 'Fair Value Adjustment Used', width: 50, dataIndex: 'fairValueAdjustmentUsed', type: 'boolean', hidden: true},
								{header: 'Notional Calculator', width: 80, dataIndex: 'costCalculator', hidden: true},
								{header: 'M2M Calculator', width: 80, dataIndex: 'm2mCalculator', hidden: true},
								{header: 'Accrual Method', width: 80, dataIndex: 'accrualMethod', hidden: true, filter: {type: 'list', options: Clifton.investment.instrument.AccrualMethods}},
								{header: 'Accrual Date Calculator', width: 80, dataIndex: 'accrualDateCalculator', hidden: true, filter: {type: 'list', options: Clifton.investment.instrument.AccrualDateCalculators}},
								{header: 'Accrual Method 2', width: 80, dataIndex: 'accrualMethod2', hidden: true, filter: {type: 'list', options: Clifton.investment.instrument.AccrualMethods}},
								{header: 'Accrual Date Calculator 2', width: 80, dataIndex: 'accrualDateCalculator2', hidden: true, filter: {type: 'list', options: Clifton.investment.instrument.AccrualDateCalculators}},
								{header: 'Hedger Initial Margin Per Unit', width: 50, dataIndex: 'hedgerInitialMarginPerUnit', type: 'currency', hidden: true},
								{header: 'Hedger Secondary Margin Per Unit', width: 50, dataIndex: 'hedgerSecondaryMarginPerUnit', type: 'currency', hidden: true},
								{header: 'Speculator Initial Margin Per Unit', width: 50, dataIndex: 'speculatorInitialMarginPerUnit', type: 'currency', hidden: true},
								{header: 'Speculator Secondary Margin Per Unit', width: 50, dataIndex: 'speculatorSecondaryMarginPerUnit', type: 'currency', hidden: true},
								{header: 'Deliverable', width: 50, dataIndex: 'deliverable', type: 'boolean', hidden: true},
								{header: 'Discount Note', width: 50, dataIndex: 'discountNote', type: 'boolean', hidden: true},
								{header: 'Reference Allowed', width: 50, dataIndex: 'hierarchy.referenceSecurityAllowed', type: 'boolean', hidden: true, filter: {searchFieldName: 'referenceSecurityAllowed'}, tooltip: 'Securities of this instrument\'s hierarchy allow a reference security to be defined'},
								{header: 'Spot Month Calculator', width: 80, dataIndex: 'spotMonthCalculatorBean.name', hidden: true, filter: {type: 'combo', searchFieldName: 'spotMonthCalculatorBeanId', url: 'systemBeanListFind.json?groupName=Investment Security Spot Month Calculator'}},
								{header: 'Inactive', width: 40, dataIndex: 'inactive', type: 'boolean'}
							],
							isFolderView: function() {
								return this.folderView;
							},
							switchGrouping: function() { // Needs to be called after load so that grouping is enabled for the store
								if (this.folderView === true) {
									this.grid.store.clearGrouping();
								}
								else {
									this.grid.store.groupBy('hierarchy.labelExpanded');
								}
							},
							switchToView: function(viewName, doNotReload) {
								if (viewName === 'All Columns') {
									this.switchToViewColumns('All Columns');
								}
								const showFolders = (viewName !== 'Global Search');
								if (this.folderView !== showFolders) {
									this.folderView = showFolders;
									this.savedLoadParams = this.getLoadParams();
									if (showFolders === true) {
										Ext.getCmp('investmentInstrumentTree').show();
									}
									else {
										Ext.getCmp('investmentInstrumentTree').hide();
									}
									this.getTopToolbar().find('text', 'Views')[0].menu.findBy(i => i.text && i.text === 'Global Search')[0].setChecked(true);
									this.ownerCt.ownerCt.doLayout();
								}
								if (doNotReload !== true) {
									this.reload();
								}
							},
							getTopToolbarFilters: function(toolbar) {
								return [
									{
										fieldLabel: 'Instrument Group', xtype: 'combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json',
										listeners: {
											select: function(field) {
												const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
												gridPanel.switchToView('Global Search');
											},
											specialkey: function(field, e) {
												if (e.getKey() === e.ENTER) {
													const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
													gridPanel.switchToView('Global Search');
												}
											}
										}
									},
									{
										fieldLabel: 'Search', width: 130, xtype: 'textfield', name: 'searchPattern',
										listeners: {
											specialkey: function(field, e) {
												if (e.getKey() === e.ENTER) {
													const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
													if (gridPanel.additionalLoadParams && gridPanel.additionalLoadParams.hierarchyId) {
														gridPanel.reload();
													}
													else {
														gridPanel.switchToView('Global Search');
													}
												}
											}
										}
									}
								];
							},
							additionalLoadParams: {},
							getLoadURL: function() {
								return 'investmentInstrumentListFind.json';
							},
							getLoadParams: function(firstLoad) {
								const params = TCG.clone(this.additionalLoadParams);
								const grp = TCG.getChildByName(this.getTopToolbar(), 'investmentGroupId');
								if (TCG.isNotBlank(grp.getValue())) {
									params.investmentGroupId = grp.getValue();
								}
								const searchPattern = TCG.getChildByName(this.getTopToolbar(), 'searchPattern');
								if (TCG.isNotBlank(searchPattern.getValue())) {
									params.searchPattern = searchPattern.getValue();
								}
								if (firstLoad) {
									searchPattern.focus(false, 500);
								}
								return params;
							},
							// NOTE: Normally this would go in the getLoadParams if firstLoad, but since we don't auto-load this
							// screen, added it to this listener so it's set once when the screen is rendered
							listeners: {
								afterRender: function() {
									// default to active
									this.setFilterValue('inactive', false);
									const gp = this;
									gp.grid.store.addListener('load', gp.switchGrouping, this);
								}
							},
							configureToolsMenu: function(menu) {
								menu.add('-');
								menu.add({
									text: 'Investment Setup',
									iconCls: 'stock-chart',
									scope: this,
									handler: function() {
										TCG.createComponent('Clifton.investment.setup.InvestmentSetupWindow', {openerCt: this});
									}
								});
							},
							editor: new TCG.grid.GridEditor({
								detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
								getDefaultDataForExisting: function() { // EXISTING ROW
									return undefined;
								},
								getDefaultData: function(grid) { // NEW ROW
									const h = Ext.getCmp('investmentInstrumentTree').selectedNodeData;
									if (!h) {
										TCG.showError('New investment hierarchy must be selected first', 'Instrument Add');
										return false;
									}
									return {hierarchy: h};
								},
								// Add "Add from Template" Button (See CDS and TRS swaps for hierarchy specific examples - all others use the generic copy window)
								addToolbarAddButton: function(tb) {
									const gridPanel = this.getGridPanel();
									tb.add({
										text: 'Add',
										xtype: 'splitbutton',
										tooltip: 'Add a new item',
										iconCls: 'add',
										scope: this,
										handler: function() {
											this.openDetailPage(this.getDetailPageClass(), gridPanel);
										},
										menu: new Ext.menu.Menu({
											items: [
												{
													text: 'Add',
													tooltip: 'Add a new item',
													iconCls: 'add',
													scope: this,
													handler: function() {
														this.openDetailPage(this.getDetailPageClass(), gridPanel);
													}
												},
												{
													text: 'Add from Template',
													tooltip: 'Create a new security using the selected as a template',
													iconCls: 'copy',
													handler: function() {
														const h = Ext.getCmp('investmentInstrumentTree').selectedNodeData;
														if (!h) {
															TCG.showError('Investment hierarchy must be selected first', 'Copy from Template');
															return false;
														}
														if (!TCG.isTrue(h.oneSecurityPerInstrument)) {
															TCG.showError('Selected hierarchy has multiple securities per instrument.  To add a new security from a template, please drill into the Instrument and select a security and then click on the Add from Template button to use selected security as the template.');
															return false;
														}
														const sm = gridPanel.grid.getSelectionModel();
														if (sm.getCount() === 0) {
															TCG.showError('Please select a row to use as the template.', 'No Row(s) Selected');
														}
														else if (sm.getCount() !== 1) {
															TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
														}
														else {
															const addFromTemplateClass = 'Clifton.investment.instrument.copy.SecurityCopyWindow';
															const cmpId = TCG.getComponentId(addFromTemplateClass);
															const instId = sm.getSelected().id;
															const defaultData = {hierarchyId: h.id};
															const sec = TCG.data.getData('investmentSecurityByInstrument.json?instrumentId=' + instId, gridPanel);
															defaultData.copyFromSecurityId = sec.id;
															defaultData.copyFromSecurityIdLabel = sec.label;
															defaultData.instrument = sec.instrument;
															TCG.createComponent(addFromTemplateClass, {
																id: cmpId,
																defaultData: defaultData,
																openerCt: gridPanel
															});
														}
													}
												}
											]
										})
									});
									tb.add('-');
								},
								addEditButtons: function(t, gridPanel) {
									TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

									t.add({
											text: 'Move',
											tooltip: 'Move Selected Instrument to a New Hierarchy',
											iconCls: 'move',
											handler: function() {
												const sm = gridPanel.grid.getSelectionModel();
												if (sm.getCount() === 0) {
													TCG.showError('Please select a row to move.', 'No Row(s) Selected');
												}
												else if (sm.getCount() !== 1) {
													TCG.showError('Multi-selection moves are not supported.  Please select one row.', 'NOT SUPPORTED');
												}
												else {
													const clazz = 'Clifton.investment.instrument.MoveInstrumentWindow';
													const cmpId = TCG.getComponentId(clazz, id);
													TCG.createComponent(clazz, {
														id: cmpId,
														params: {id: sm.getSelected().id},
														openerCt: gridPanel,
														defaultIconCls: gridPanel.getWindow().iconCls
													});
												}

											}
										}
									);
									t.add('-');

									const menu = new Ext.menu.Menu();
									menu.add({
										text: 'Global Search',
										xtype: 'menucheckitem',
										group: 'investmentInstrumentView',
										scope: gridPanel,
										handler: function() {
											if (this.folderView) {
												this.folderView = false;
												this.savedLoadParams = this.additionalLoadParams;
												this.additionalLoadParams = {};
												Ext.getCmp('investmentInstrumentTree').hide();
												this.ownerCt.ownerCt.doLayout();
												this.reload();
											}
										}
									});
									menu.add({
										text: 'Organized in Folders',
										xtype: 'menucheckitem',
										group: 'investmentInstrumentView',
										checked: true,
										scope: gridPanel,
										handler: function() {
											if (!this.folderView) {
												this.folderView = true;
												this.additionalLoadParams = this.savedLoadParams;
												Ext.getCmp('investmentInstrumentTree').show();
												this.ownerCt.ownerCt.doLayout();
												this.reload();
											}
										}
									});

									t.add({
										text: 'Views',
										iconCls: 'views',
										menu: menu
									});
									t.add('-');
								}
							})
						}]
					}
				]
			},


			{
				title: 'Instrument Groups',
				items: [{
					name: 'investmentGroupListFind',
					xtype: 'gridpanel',
					instructions: 'Instrument Groups are used to classify Investment Instruments/Securities for reporting (group by items) purposes. They can also be used to group securities for other purposes: filtering, morning vs. afternoon pricing jobs, etc.',
					topToolbarSearchParameter: 'searchPattern',
					additionalPropertiesToRequest: 'investmentGroupType',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 110, dataIndex: 'name'},
						{header: 'Description', width: 150, dataIndex: 'description', hidden: true},
						{header: 'Group Type', width: 100, dataIndex: 'investmentGroupType.name', filter: {type: 'combo', searchFieldName: 'investmentGroupTypeId', url: 'investmentGroupTypeListFind.json'}},
						{header: 'Template', width: 50, dataIndex: 'templateInvestmentGroup.name', filter: {searchFieldName: 'templateInvestmentGroupName'}},
						{header: 'Modify Condition', width: 130, dataIndex: 'entityModifyCondition.name', filter: {searchFieldName: 'entityModifyCondition'}},
						{header: 'Base CCY', width: 30, dataIndex: 'baseCurrency.symbol', filter: {searchFieldName: 'baseCurrencySymbol'}},
						{header: 'Max Depth', width: 30, dataIndex: 'levelsDeep', type: 'int', hidden: true},
						{header: 'Leaf Only', width: 30, dataIndex: 'leafAssignmentOnly', type: 'boolean', hidden: true, tooltip: 'Allow assignments to leaf items only'},
						{header: 'Duplicates', width: 30, dataIndex: 'duplicationAllowed', type: 'boolean', hidden: true, tooltip: 'Allow the same security to be assigned to more than one group item'},
						{header: 'No Missing', width: 30, dataIndex: 'missingInstrumentNotAllowed', type: 'boolean', hidden: true, tooltip: 'Do not allow missing instrument (map everything: 100% of security universe). If checked, notifications will be generated if this group ever has any missing instruments unmapped.'},
						{header: 'Tradable Only', width: 30, dataIndex: 'tradableSecurityOnly', type: 'boolean', tooltip: 'Tradable Securities Only (specified on Investment Hierarchy). Checking this box allows only instruments that belong to a hierarchy that allows trading.'},
						{header: 'System', width: 30, dataIndex: 'systemDefined', type: 'boolean'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Group Type', width: 230, xtype: 'toolbar-combo', name: 'investmentGroupTypeId', url: 'investmentGroupTypeListFind.json', linkedFilter: 'investmentGroupType.name'},
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Investment Group Tags'},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const params = {};
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.categoryName = 'Investment Group Tags';
							params.categoryHierarchyId = tag.getValue();
						}
						return params;
					},
					editor: {
						detailPageClass: 'Clifton.investment.setup.GroupWindow',
						copyURL: 'investmentGroupCopy.json',
						getDefaultData: function(gridPanel) {
							return this.savedDefaultData;
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'investmentGroupType', url: 'investmentGroupTypeListFind.json', limitRequestedProperties: false, width: 200, listWidth: 250, emptyText: '< Select Group Type >'}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Create a new Investment Group of selected type',
								iconCls: 'add',
								scope: this,
								handler: function() {
									let groupType = TCG.getChildByName(toolBar, 'investmentGroupType');
									const groupTypeId = groupType.getValue();
									if (TCG.isBlank(groupTypeId)) {
										TCG.showError('You must first select desired group type from the list.');
									}
									else {
										groupType = groupType.store.getById(groupTypeId);
										this.savedDefaultData = {
											investmentGroupType: groupType.json
										};
										this.openDetailPage(this.getDetailPageClass(), gridPanel);
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},


			{
				title: 'Securities',
				items: [{
					name: 'investmentSecurityListFind',
					xtype: 'gridpanel',
					importTableName: 'InvestmentSecurity',
					importComponentName: 'Clifton.investment.instrument.upload.SecurityUploadWindow',
					instructions: 'This section allows browsing and filtering all securities defined in the system.',
					topToolbarSearchParameter: 'searchPattern',
					additionalPropertiesToRequest: 'priceMultiplierOverride|instrument.priceMultiplier',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Type', width: 60, dataIndex: 'instrument.hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true, showNotEquals: true}, viewNames: ['All Securities', 'Option Securities']},
						{header: 'Sub Type', width: 60, dataIndex: 'instrument.hierarchy.investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}, hidden: true},
						{header: 'Sub Type 2', width: 60, dataIndex: 'instrument.hierarchy.investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}, hidden: true},
						{header: 'Investment Hierarchy', width: 130, dataIndex: 'instrument.hierarchy.labelExpanded', defaultSortColumn: true, filter: {searchFieldName: 'hierarchyName'}, viewNames: ['All Securities', 'Option Securities']},
						{header: 'Underlying', width: 100, dataIndex: 'instrument.underlyingInstrument.name', filter: {type: 'combo', searchFieldName: 'underlyingInstrumentId', displayField: 'name', url: 'investmentInstrumentListFind.json'}, hidden: true},
						{header: 'Underlying Security', width: 60, dataIndex: 'underlyingSecurity.symbol', filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, viewNames: ['All Securities', 'Option Securities']},
						{header: 'Counterparty', width: 100, dataIndex: 'businessCompany.name', filter: {type: 'combo', searchFieldName: 'businessCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
						{header: 'Symbol', width: 75, dataIndex: 'symbol', viewNames: ['All Securities', 'Option Securities']},
						{header: 'CUSIP', width: 45, dataIndex: 'cusip', viewNames: ['All Securities', 'Option Securities']},
						{header: 'ISIN', width: 60, dataIndex: 'isin', viewNames: ['All Securities']},
						{header: 'SEDOL', width: 45, dataIndex: 'sedol', viewNames: ['All Securities']},
						{
							header: 'FIGI', width: 55, dataIndex: 'figi', viewNames: ['All Securities'],
							tooltip: 'Financial Instrument Global Identifier (FIGI; also known as the Bloomberg Global Identifier (BBGID)) is an open standard, 12 character unique identifier assigned to all financial instruments and will not change once issued. For equity instruments, an identifier is issued per trading venue.'
						},
						{header: 'OCC Symbol', width: 45, dataIndex: 'occSymbol', viewNames: ['All Securities', 'Option Securities']},
						{header: 'Security Name', width: 100, dataIndex: 'name', viewNames: ['All Securities', 'Option Securities']},
						{header: 'Option Type', width: 45, dataIndex: 'optionType', hidden: true, viewNames: ['Option Securities']},
						{header: 'Strike Price', width: 45, dataIndex: 'optionStrikePrice', hidden: true, viewNames: ['Option Securities']},
						{header: 'Option Style', width: 60, dataIndex: 'optionStyle', hidden: true, viewNames: ['Option Securities']},
						{header: 'Expiry Time', width: 45, dataIndex: 'settlementExpiryTime', hidden: true, viewNames: ['Option Securities']},
						{header: 'Country of Risk', width: 100, dataIndex: 'instrument.countryOfRisk.text', filter: {type: 'combo', searchFieldName: 'countryOfRiskId', displayField: 'text', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Countries'}, hidden: true},
						{header: 'Country of Incorporation', width: 100, dataIndex: 'instrument.countryOfIncorporation.text', filter: {type: 'combo', searchFieldName: 'countryOfIncorporationId', displayField: 'text', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Countries'}, hidden: true},
						{header: 'Primary Exchange', width: 100, dataIndex: 'instrument.exchange.label', filter: {type: 'combo', searchFieldName: 'exchangeId', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=false'}, hidden: true},
						{header: 'Composite Exchange', width: 100, dataIndex: 'instrument.compositeExchange.label', filter: {type: 'combo', searchFieldName: 'compositeExchangeId', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=true'}},
						{header: 'Settlement Calendar', width: 100, dataIndex: 'instrument.settlementCalendar.name', filter: {type: 'combo', searchFieldName: 'settlementCalendarId', url: 'calendarListFind.json'}, hidden: true},
						{header: 'CCY Denomination', width: 40, dataIndex: 'instrument.tradingCurrency.symbol', filter: {type: 'combo', searchFieldName: 'tradingCurrencyId', displayField: 'name', url: 'investmentSecurityListFind.json?currency=true'}, viewNames: ['All Securities', 'Option Securities']},
						{header: 'Settlement CCY', width: 40, dataIndex: 'settlementCurrency.symbol', filter: {type: 'combo', searchFieldName: 'settlementCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}, viewNames: ['All Securities']},
						{header: 'Big Security', width: 60, dataIndex: 'bigSecurity.symbol', filter: {type: 'combo', searchFieldName: 'bigSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, viewNames: ['All Securities', 'Option Securities']},
						{header: 'Reference Security', width: 60, dataIndex: 'referenceSecurity.symbol', filter: {type: 'combo', searchFieldName: 'referenceSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, viewNames: ['All Securities', 'Option Securities']},
						{header: 'First Trade', width: 50, dataIndex: 'startDate', hidden: true, viewNames: ['All Securities', 'Option Securities']},
						{header: 'Last Trade', width: 50, dataIndex: 'endDate', viewNames: ['All Securities']},
						{header: 'Early Termination Date', width: 50, dataIndex: 'earlyTerminationDate', hidden: true},
						{header: 'First Notice Date', width: 50, dataIndex: 'firstNoticeDate', hidden: true},
						{header: 'First Delivery Date', width: 50, dataIndex: 'firstDeliveryDate', hidden: true},
						{header: 'Last Delivery Date', width: 50, dataIndex: 'lastDeliveryDate', hidden: true, viewNames: ['Option Securities']},
						{header: 'Illiquid', width: 35, dataIndex: 'illiquid', type: 'boolean', hidden: true},
						{header: 'Reference Allowed', width: 40, dataIndex: 'instrument.hierarchy.referenceSecurityAllowed', type: 'boolean', hidden: true, filter: {searchFieldName: 'referenceSecurityAllowed'}, tooltip: 'Security allows a reference security to be defined'},
						{
							header: 'Price Multiplier', width: 50, dataIndex: 'priceMultiplier', type: 'float', hidden: true, viewNames: ['All Securities', 'Option Securities'],
							renderer: function(v, metaData, r) {
								if (!TCG.isBlank(r.json.priceMultiplierOverride) && TCG.getValue('instrument.priceMultiplier', r.json) !== v) {
									metaData.css = 'amountAdjusted';
									const qtip = 'Price Multiplier Overridden from Instrument value of: ' + TCG.renderAmount(TCG.getValue('instrument.priceMultiplier', r.json), false, '0,000.0000', true);
									metaData.attr = TCG.renderQtip(qtip);
								}
								return v;
							}
						},
						{header: 'Active', width: 30, dataIndex: 'active', type: 'boolean', viewNames: ['All Securities', 'Option Securities']}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'},
							{fieldLabel: 'Security Group', xtype: 'toolbar-combo', name: 'securityGroupId', width: 150, url: 'investmentSecurityGroupListFind.json'},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to active
							this.setFilterValue('active', true);
						}
						const params = {};
						const t = this.getTopToolbar();
						let grp = TCG.getChildByName(t, 'investmentGroupId');
						if (TCG.isNotBlank(grp.getValue())) {
							params.investmentGroupId = grp.getValue();
						}
						grp = TCG.getChildByName(t, 'securityGroupId');
						if (TCG.isNotBlank(grp.getValue())) {
							params.securityGroupId = grp.getValue();
						}
						return params;
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.investment.instrument.SecurityWindow'
					},
					viewNames: ['All Securities', 'Option Securities'],
					switchToViewBeforeReload: function(viewName) {
						switch (viewName) {
							case this.viewNames[1]:
								this.configureOptionSecuritiesView();
								break;
							case this.viewNames[0]:
							default:
								this.configureDefaultView();
								break;
						}
					},

					configureOptionSecuritiesView: function() {
						const gp = this;
						// filter grid on option types, and show option-specific columns
						TCG.data.getDataPromiseUsingCaching('investmentTypeByName.json?name=Options', this, 'investment.type.Options')
							.then(investmentType => {
								gp.setFilterValue('instrument.hierarchy.investmentType.name', {value: investmentType.id, text: investmentType.name}, true, true);
							});
					},
					configureDefaultView: function() {
						const gp = this;
						gp.clearFilter('instrument.hierarchy.investmentType.name');
					}
				}]
			},


			{
				title: 'Security Groups',
				items: [{
					name: 'investmentSecurityGroupListFind',
					xtype: 'gridpanel',
					instructions: 'An investment security group classifies securities for specific purposes. System Defined group names cannot be changed. System Managed groups have a rebuild query that can automatically pick up new or updated securities based on the specific logic in the query.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 80, dataIndex: 'name'},
						{header: 'Description', width: 100, dataIndex: 'description'},
						{header: 'Modify Condition', width: 90, dataIndex: 'entityModifyCondition.name', filter: {searchFieldName: 'entityModifyConditionName'}},
						{header: 'Rebuild Bean', width: 90, dataIndex: 'rebuildSystemBean.name', filter: false, hidden: true},
						{header: 'Rebuild on Security Insert', width: 40, dataIndex: 'rebuildOnNewSecurityInsert', type: 'boolean', tooltip: 'Controls whether or not this group is immediately rebuilt when a new Investment Security is inserted.'},
						{header: 'Allow Same Instrument', width: 35, dataIndex: 'securityFromSameInstrumentAllowed', type: 'boolean'},
						{header: 'System Managed', width: 25, dataIndex: 'systemManaged', type: 'boolean', sortable: false},
						{header: 'System Defined', width: 25, dataIndex: 'systemDefined', type: 'boolean'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Investment Security Group Tags'},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const params = {};
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.categoryName = 'Investment Security Group Tags';
							params.categoryHierarchyId = tag.getValue();
						}
						return params;
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.SecurityGroupWindow'
					}
				}]
			},


			{
				title: 'Security Events',
				items: [{
					xtype: 'investment-security-events-grid'
				}]
			},


			{
				title: 'Event Payouts',
				items: [{
					name: 'investmentSecurityEventExtendedListFind',
					importTableName: 'InvestmentSecurityEvent',
					xtype: 'gridpanel',
					remoteSort: true,
					groupField: 'eventId',
					groupTextTpl: '{[values.rs[0].json.type.name + " for " + values.rs[0].json.security.symbol + (TCG.isBlank(values.rs[0].json.eventDate) ? "" : " on " + new Date(values.rs[0].json.eventDate).format(\'m/d/Y\'))]}: ({[TCG.isTrue(values.rs[0].json.payoutPresent) ? values.rs.length + " " + (values.rs.length > 1 ? "Payouts" : "Payout") : "No Payout"]})',
					instructions: 'A security event represents an instance of a specific event type for a specific security. For example, specific stock split, dividend payment, etc .',
					additionalPropertiesToRequest: 'eventLabel|payoutPresent|type.id|security.symbol',
					viewNames: ['Default', 'Expanded'],
					listeners: {
						afterrender: function(gridPanel) {
							gridPanel.setDefaultView('Default');

							const el = gridPanel.getEl();
							el.on('contextmenu', function(e, target) {
								const g = gridPanel.grid;
								const editor = gridPanel.editor;
								g.contextRowIndex = g.view.findRowIndex(target);
								e.preventDefault();
								if (!g.drillDownMenu) {
									g.drillDownMenu = new Ext.menu.Menu({
										items: [
											{
												text: 'Open Event', iconCls: 'event',
												handler: function() {
													const row = g.getStore().getAt(g.contextRowIndex);
													editor.openDetailPage(editor.detailPageClass, gridPanel, editor.getEventId(row), row);
												}
											}, {
												text: 'Open Payout', iconCls: 'event',
												listeners: {
													afterrender: function(item) {
														item.setDisabled(!editor.isPayoutPresentOnRow(g.getStore().getAt(g.contextRowIndex)));
													}
												},
												handler: function() {
													const row = g.getStore().getAt(g.contextRowIndex);
													editor.openDetailPage(editor.getPayoutDetailClass(), gridPanel, editor.getPayoutId(row), row);
												}
											}, '-', {
												text: 'Add Payout', iconCls: 'event',
												handler: function() {
													const row = g.getStore().getAt(g.contextRowIndex);
													TCG.createComponent('Clifton.investment.instrument.event.payout.AddPayoutWindow', {
														defaultData: {securityEvent: {id: row.json.eventId, label: row.json.eventLabel, type: row.json.type}},
														openerCt: gridPanel,
														defaultIconCls: gridPanel.getWindow().iconCls
													});
												}
											}
										]
									});
								}
								TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
							}, gridPanel);
						}
					},
					columns: [
						{header: 'Event ID', width: 15, dataIndex: 'eventId', type: 'int', hidden: true},
						{header: 'CA ID', width: 20, dataIndex: 'corporateActionIdentifier', type: 'int', numberFormat: '0', useNull: true, tooltip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.'},
						{header: 'Event Type', width: 45, dataIndex: 'type.name', idDataIndex: 'type.id', filter: {type: 'combo', searchFieldName: 'typeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}},
						{header: 'Event Status', width: 35, dataIndex: 'status.name', renderer: Clifton.investment.instrument.event.renderEventStatus, filter: {type: 'combo', searchFieldName: 'statusId', url: 'investmentSecurityEventStatusListFind.json', showNotEquals: true}},
						{header: 'Investment Type', width: 60, dataIndex: 'security.instrument.hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'securityInvestmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}, hidden: true},
						{header: 'Investment Hierarchy', width: 130, dataIndex: 'security.instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'securityInstrumentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}, hidden: true},
						{header: 'Security', width: 70, dataIndex: 'security.label', renderer: TCG.renderValueWithTooltip, filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Additional Security', width: 70, dataIndex: 'additionalSecurity.label', renderer: TCG.renderValueWithTooltip, filter: {type: 'combo', searchFieldName: 'additionalSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true, viewNames: ['Expanded']},
						{header: 'Before Value', width: 25, dataIndex: 'beforeEventValue', type: 'float'},
						{header: 'After Value', width: 25, dataIndex: 'afterEventValue', type: 'float'},
						{header: 'Event Additional Value', width: 50, dataIndex: 'additionalEventValue', type: 'float', useNull: true, hidden: true, viewNames: ['Expanded']},
						{header: 'Voluntary', width: 19, dataIndex: 'voluntary', type: 'boolean', tooltip: 'Specifies whether the holder of the security has an option to choose to participate or not participate in this event.'},
						{header: 'Taxable', width: 17, dataIndex: 'taxable', type: 'boolean', tooltip: 'Specifies whether this event is likely subject to additional taxes affecting net payout amounts and booking rules.', hidden: true, viewNames: ['Expanded']},
						{header: 'Order', width: 30, dataIndex: 'bookingOrderOverride', type: 'int', useNull: true, tooltip: 'Optionally overrides the booking order specified on accounting event journal types.', hidden: true},
						{header: 'Declare Date', width: 27, dataIndex: 'declareDate', hidden: true, viewNames: ['Expanded']},
						{header: 'Ex Date', width: 23, dataIndex: 'exDate'},
						{header: 'Record Date', width: 27, dataIndex: 'recordDate', hidden: true, viewNames: ['Expanded']},
						{header: 'Event Date', width: 25, dataIndex: 'eventDate', hidden: true},
						{header: 'Payout Date', width: 27, dataIndex: 'payoutDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},

						{header: 'Payout ID', width: 15, dataIndex: 'payout.id', hidden: true},
						{header: 'Payout Type', width: 20, dataIndex: 'payoutType.typeCode', idDataIndex: 'payoutType.id', filter: {type: 'combo', searchFieldName: 'payoutTypeId', displayField: 'name', url: 'investmentSecurityEventPayoutTypeListFind.json'}},
						{header: 'Election #', width: 20, dataIndex: 'electionNumber', type: 'int', useNull: true},
						{header: 'Payout #', width: 20, dataIndex: 'payoutNumber', type: 'int', useNull: true},
						{header: 'Payout Security', width: 60, dataIndex: 'payoutSecurity.label', renderer: TCG.renderValueWithTooltip, filter: {type: 'combo', searchFieldName: 'payoutSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json?active=true'}},
						{header: 'Payout Additional Value', width: 45, dataIndex: 'additionalPayoutValue', type: 'float', useNull: true, hidden: true, viewNames: ['Expanded']},
						{header: 'Payout Additional Value 2', width: 45, dataIndex: 'additionalPayoutValue2', type: 'float', useNull: true, hidden: true, viewNames: ['Expanded']},
						{header: 'Proration Rate', width: 45, dataIndex: 'prorationRate', type: 'float', useNull: true, hidden: true, viewNames: ['Expanded']},
						{header: 'Additional Date', width: 45, dataIndex: 'additionalPayoutDate', hidden: true, viewNames: ['Expanded']},
						{
							header: 'Fractional Shares', width: 40, dataIndex: 'fractionalSharesMethod', idDataIndex: 'fractionalSharesMethod', filter: {searchFieldName: 'fractionalSharesMethod', type: 'combo', mode: 'local', matchTextInLocalMode: false, store: {xtype: 'arraystore', data: Clifton.investment.instrument.event.FractionalShares}},
							hidden: true, viewNames: ['Expanded'], renderer: function(value, metaData, r) {
								const fractionalSharesMethod = TCG.getValue('fractionalSharesMethod', r.json);
								if (TCG.isBlank(fractionalSharesMethod)) {
									return fractionalSharesMethod;
								}
								const fractionalShareEntry = Clifton.investment.instrument.event.FractionalShares.find(valueArray => valueArray[0].includes(fractionalSharesMethod));
								return fractionalShareEntry ? fractionalShareEntry[1] : fractionalShareEntry;
							}
						},
						{header: 'Description', width: 100, dataIndex: 'description', hidden: true},
						{header: 'Default', width: 18, dataIndex: 'defaultElection', type: 'boolean'},
						{header: 'Deleted', width: 18, dataIndex: 'deleted', type: 'boolean', hidden: true},
						{header: 'DTC Only', width: 18, dataIndex: 'payout.dtcOnly', type: 'boolean'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'},
							{fieldLabel: 'Event Type', xtype: 'toolbar-combo', width: 150, url: 'investmentSecurityEventTypeListFind.json', linkedFilter: 'type.name'},
							{
								fieldLabel: 'Single Payout', xtype: 'toolbar-combo', width: 50, minListWidth: 50, name: 'singlePayout', mode: 'local', valueField: 'value', value: {value: null, text: 'All'},
								store: {
									xtype: 'arraystore',
									fields: ['value', 'name', 'description'],
									data: [
										[null, 'All', 'Display all'],
										[true, 'Yes', 'Display only events with a single payout'],
										[false, 'No', 'Display only events with multiple payouts']
									]
								}
							},
							{
								fieldLabel: 'Deleted', xtype: 'toolbar-combo', width: 50, minListWidth: 50, name: 'deletedToolbar', linkedFilter: 'deleted', mode: 'local', valueField: 'value', value: {value: false, text: 'No'},
								store: {
									xtype: 'arraystore',
									fields: ['value', 'name', 'description'],
									data: [
										[null, 'All', 'Display all payouts'],
										[true, 'Yes', 'Display only deleted payouts'],
										[false, 'No', 'Display only non-deleted payouts']
									]
								},
								applyFilter: function(combo) {
									const linkedFilter = TCG.getParentByClass(combo, TCG.grid.GridPanel);
									if (TCG.isNull(combo.value)) {
										linkedFilter.clearFilter(combo.linkedFilter, true);
									}
									else {
										linkedFilter.setFilterValue(combo.linkedFilter, combo.value);
									}
								},
								listeners: {
									afterrender: function(combo) {
										combo.applyFilter(combo);
									},
									select: function(combo) {
										combo.applyFilter(combo);
									}
								}
							}
						];
					},
					getLoadParams: function(firstLoad) {
						const params = {};
						if (firstLoad) {
							this.setFilterValue('payoutDate', {
								after: new Date().add(Date.DAY, -15),
								before: new Date().add(Date.DAY, 15)
							});
							this.setFilterValue('defaultElection', true);
						}
						const toolbar = this.getTopToolbar();
						const investmentGroupId = TCG.getChildByName(toolbar, 'investmentGroupId').getValue();
						if (investmentGroupId) {
							params['investmentGroupId'] = v;
						}
						const singlePayout = TCG.getChildByName(toolbar, 'singlePayout').getValue();
						if (TCG.isNotNull(singlePayout)) {
							params['singlePayout'] = singlePayout;
						}
						return params;
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.SingleSecurityEventWindow',
						deleteButtonTooltip: 'Delete the selected row. If the row represents a payout, the payout is deleted. If the row has no payout, it represents the event and the event is deleted.',
						getDefaultData: function(gridPanel, row) {
							return this.savedDefaultData;
						},
						getDetailPageId: function(gridPanel, row) {
							return TCG.getValue('payout.id', row.json) || TCG.getValue('eventId', row.json);
						},
						getDetailPageClass: function(grid, row) {
							return this.isPayoutPresentOnRow(row) ? this.getPayoutDetailClass()
								: TCG.callOverridden(this, 'getDetailPageClass', arguments);
						},
						getDeleteURL: function(selectedItem) {
							if (TCG.isNull(selectedItem)) {
								return false;
							}
							return this.isPayoutPresentOnRow(selectedItem) ? 'investmentSecurityEventPayoutDelete.json' : 'investmentSecurityEventDelete.json';
						},
						getDeleteParams: function(selectionModel) {
							const selectedItem = selectionModel.getSelected();
							const payoutId = this.getPayoutId(selectedItem);
							return {id: TCG.isNotBlank(payoutId) ? payoutId : this.getEventId(selectedItem)};
						},
						getPayoutId: function(row) {
							return TCG.getValue('payout.id', row.json);
						},
						getEventId: function(row) {
							return TCG.getValue('eventId', row.json);
						},
						isPayoutPresentOnRow: function(row) {
							return TCG.isNotNull(row) && TCG.isNotBlank(this.getPayoutId(row));
						},
						getPayoutDetailClass: function() {
							return 'Clifton.investment.instrument.event.payout.SecurityEventPayoutWindow';
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add({
								iconCls: 'expand-all',
								tooltip: 'Expand or Collapse all groups',
								scope: this,
								handler: function() {
									gridPanel.grid.collapsed = !gridPanel.grid.collapsed;
									gridPanel.grid.view.toggleAllGroups(!gridPanel.grid.collapsed);
								}
							}, '-');
							toolBar.add(new TCG.form.ComboBox({name: 'eventType', url: 'investmentSecurityEventTypeListFind.json', emptyText: '< Select Event Type >', width: 150, listWidth: 230}));
							toolBar.add(new TCG.form.ComboBox({name: 'eventSecurity', url: 'investmentSecurityListFind.json', emptyText: '< Select Security >', displayField: 'label', width: 150, listWidth: 230, requestedProps: 'instrument.hierarchy.investmentType.name|instrument.hierarchy.investmentTypeSubType.name'}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add an event for selected event type and security',
								iconCls: 'add',
								scope: this,
								handler: function() {
									const eventType = TCG.getChildByName(toolBar, 'eventType');
									const eventSecurity = TCG.getChildByName(toolBar, 'eventSecurity');
									const eventTypeId = eventType.getValue();
									const securityObject = eventSecurity.getValueObject();
									if (TCG.isBlank(eventTypeId) || TCG.isNull(securityObject)) {
										TCG.showError('You must first select desired Event Type and Security from the lists.');
									}
									else {
										this.savedDefaultData = {
											security: securityObject,
											type: {
												id: eventTypeId,
												name: eventType.lastSelectionText
											},
											eventDescription: (eventType.lastSelectionText === 'Stock Spinoff') ? 'Cash in Lieu' : undefined
										};
										this.openDetailPage(this.getDetailPageClass(), gridPanel);
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},

			{
				title: 'Event Actions',
				items: [{
					name: 'investmentSecurityEventActionListFind',
					xtype: 'gridpanel',
					instructions: 'This window shows actions associated with an Investment Security Events.  Investment Security Event Actions track whether an action of a given type was executed for a given security event.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Action Type', width: 110, dataIndex: 'actionType.name', filter: {type: 'combo', searchFieldName: 'actionTypeId', displayField: 'name', url: 'investmentSecurityEventActionTypeListFind.json'}},
						{header: 'Event Type', width: 50, dataIndex: 'securityEvent.type.name', filter: {type: 'combo', searchFieldName: 'eventTypeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}, tooltip: 'Type of Investment Security Event to which this action pertains.'},
						{header: 'Event Status', width: 50, dataIndex: 'securityEvent.status.name', filter: {type: 'combo', searchFieldName: 'eventStatusId', displayField: 'name', url: 'investmentSecurityEventStatusListFind.json'}},
						{header: 'Security', width: 100, dataIndex: 'securityEvent.security.label', idDataIndex: 'securityEvent.security.id', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json?', showNotEquals: true}},
						{header: 'Event Date', width: 30, dataIndex: 'securityEvent.eventDate', type: 'date', filter: {searchFieldName: 'eventDate'}},
						{header: 'Processed Date', width: 40, dataIndex: 'processedDate', tooltip: 'Date that the action was executed for the Investment Security Event.'},
						{header: 'Manual', width: 50, dataIndex: 'actionType.manual', type: 'boolean', tooltip: 'If checked, indicates that this is a manual action type used where special processing is required. Manual event actions are manually added to an Investment Security Event and processed manually. Examples may be security event actions related to OTC Securities, or Price Baskets.'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.action.EventActionWindow',
						drillDownOnly: true
					}
				}]
			},

			{
				title: 'Interest Rate Indices',
				items: [{
					name: 'investmentInterestRateIndexListFind',
					xtype: 'gridpanel',
					instructions: 'Interest rate index is an index that tracks a rate that is charged or paid for the use of money. Index rates change over time. Rates of major indices are used to derive valuation of various investment securities.',
					importTableName: 'InvestmentInterestRateIndex',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Index Name', width: 150, dataIndex: 'name'},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'ISDA Name', width: 80, dataIndex: 'isdaName'},
						{header: 'Ticker Symbol', width: 60, dataIndex: 'symbol'},
						{header: 'CCY Denomination', width: 80, dataIndex: 'currencyDenomination.label', filter: {searchFieldName: 'currencyLabel'}},
						{header: 'Calendar', width: 80, dataIndex: 'calendar.name', filter: {searchFieldName: 'calendarName'}},
						{header: 'Fixing Term', width: 40, dataIndex: 'fixingTerm'},
						{header: 'Days', width: 30, dataIndex: 'numberOfDays', type: 'int'},
						{header: 'Order', width: 30, dataIndex: 'order', type: 'int'},
						{header: 'Start Date', width: 40, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 40, dataIndex: 'endDate', hidden: true},
						{header: 'Active', width: 30, dataIndex: 'active', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.rates.InterestRateIndexWindow'
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to active
							this.setFilterValue('active', true);
						}
						return {};
					}
				}]
			},


			{
				title: 'Interest Rate Groups',
				items: [{
					name: 'investmentInterestRateIndexGroupListFind',
					xtype: 'gridpanel',
					forceLocalFiltering: true,
					instructions: 'Interest rate groups are used to group interest rate indices. System Defined group names cannot be changed as they are referenced by that name throughout the system. For example, Synthetic Adjustment Factor group is used in Portfolio Run processing to determine the value of Equity Index Futures.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'System Defined', width: 50, dataIndex: 'systemDefined', type: 'boolean'},
						{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.rates.InterestRateIndexGroupWindow'
					},
					isPagingEnabled: function() {
						return false;
					}
				}]
			}
		]
	}]
});
