Clifton.investment.setup.InvestmentSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'investmentSetupWindow',
	title: 'Investment Setup',
	iconCls: 'stock-chart',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Asset Classes',
				items: [{
					name: 'investmentAssetClassListFind',
					xtype: 'gridpanel',
					instructions: 'Investment Asset Classes are used to group Investment Instruments/Securities that exhibit similar characteristics, behave similarly in the marketplace, and are subject to the same laws and regulations. Different clients may choose to name same asset class differently or may have unique names for asset classes.',
					additionalPropertiesToRequest: 'level',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Asset Class', width: 120, dataIndex: 'nameExpanded', filter: {searchFieldName: 'searchPattern'}, defaultSortColumn: true},
						{header: 'Master', width: 40, dataIndex: 'master', type: 'boolean', tooltip: 'If this is a master asset class, it is used to group asset classes together for AUM related reporting across all Clients.'},
						{header: 'Master Asset Class', width: 120, dataIndex: 'masterAssetClass.name', tooltip: 'The master asset class this asset class is assigned to.', filter: {searchFieldName: 'masterAssetClassId', url: 'investmentAssetClassListFind.json?master=true', type: 'combo'}},
						{header: 'Parent Asset Class', width: 120, dataIndex: 'parent.name', filter: {searchFieldName: 'parentName'}, hidden: true},
						{header: 'Short Label', width: 100, dataIndex: 'shortLabel'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Cash', width: 50, dataIndex: 'cash', type: 'boolean'},
						{header: 'Main Cash', width: 50, dataIndex: 'mainCash', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.setup.AssetClassWindow'
					}
				}]
			},


			{
				title: 'Exchanges',
				items: [{
					name: 'investmentExchangeListFind',
					xtype: 'gridpanel',
					instructions: 'An exchange is a marketplace in which financial instruments are traded. Exchanges give companies, governments and other groups a platform to sell securities to the investing public and provides regulation to ensure fair and orderly trading, as well as efficient dissemination of price information for any securities trading on that exchange.  It may or may not be a physical location, but could also be an electronic platform.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Exchange Name', width: 130, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Code', width: 50, dataIndex: 'exchangeCode', filter: {comparison: 'EQUALS'}},
						{header: 'MIC', width: 50, dataIndex: 'marketIdentifierCode', filter: {comparison: 'EQUALS'}, hidden: true},
						{header: 'Composite', width: 40, dataIndex: 'compositeExchange', type: 'boolean'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Parent Exchange', width: 120, dataIndex: 'parent.name'},
						{header: 'Base Currency', width: 60, dataIndex: 'baseCurrency.name'},
						{header: 'Country', width: 70, dataIndex: 'country.text'},
						{header: 'Calendar', width: 100, dataIndex: 'calendar.name'},
						{header: 'Time Zone', width: 120, dataIndex: 'timeZone.label'},
						{header: 'Open Time', width: 50, dataIndex: 'openTime', hidden: true},
						{header: 'Close Time', width: 50, dataIndex: 'closeTime', hidden: true},
						{header: 'Local Open Time', width: 50, dataIndex: 'openTimeLocal', hidden: true},
						{header: 'Local Close Time', width: 50, dataIndex: 'closeTimeLocal', hidden: true},
						{header: 'Open', width: 40, dataIndex: 'open', type: 'boolean', filter: false, sortable: false},
						{header: 'Partial Day Open Time', width: 50, dataIndex: 'openTimePartialDay', hidden: true},
						{header: 'Partial Day Close Time', width: 50, dataIndex: 'closeTimePartialDay', hidden: true},
						{header: 'Partial Day Local Open Time', width: 50, dataIndex: 'openTimePartialDayLocal', hidden: true},
						{header: 'Partial Day Local Close Time', width: 50, dataIndex: 'closeTimePartialDayLocal', hidden: true},
						{header: 'Partial Day Open', width: 40, dataIndex: 'openPartialDay', type: 'boolean', filter: false, sortable: false, hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.investment.setup.ExchangeWindow'
					}
				}]
			},


			{
				title: 'Investment Types',
				items: [{
					name: 'investmentTypeList',
					xtype: 'gridpanel',
					forceLocalFiltering: true,
					instructions: 'Investment Types define the types of investments instruments. Each security is associated with corresponding instrument, instrument with hierarchy and hierarchy with investment type. Each level may define attributes/behavior that should apply to security. More specific configuration can be used to override higher level settings.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 35, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 180, dataIndex: 'description', hidden: true},

						{header: 'Physical', width: 20, dataIndex: 'physicalSecurity', type: 'boolean', tooltip: 'A non-derivative security that is paid with cash at open/close'},

						{header: 'Unadjusted Qty Name', width: 45, dataIndex: 'unadjustedQuantityName', tooltip: 'For securities with factor changes (ABS and CDS) it is the Original Face/Original Notional (Opening Quantity; not adjusted by Current Factor). For other securities it is the same as Remaining Quantity or Quantity (current number of units). These values can be overridden at Investment Sub Type and Investment Sub Type 2.'},
						{header: 'Quantity Name', width: 45, dataIndex: 'quantityName', tooltip: 'Industry standard name for "Quantity" field (remaining quantity). These values can be overridden at Investment Sub Type and Investment Sub Type 2.'},

						{header: 'Cost Field Name', width: 45, dataIndex: 'costFieldName', tooltip: 'This name can be overridden at Sub Type or Sub Type 2'},
						{header: 'Cost Basis Field Name', width: 45, dataIndex: 'costBasisFieldName', tooltip: 'This name can be overridden at Sub Type or Sub Type 2'},
						{header: 'Notional Field Name', width: 45, dataIndex: 'notionalFieldName', tooltip: 'This name can be overridden at Sub Type or Sub Type 2'},
						{header: 'Market Value Field Name', width: 45, dataIndex: 'marketValueFieldName', tooltip: 'This name can be overridden at Sub Type or Sub Type 2'},

						{header: 'Quantity Precision', width: 30, dataIndex: 'quantityDecimalPrecision', type: 'int', useNull: true, tooltip: 'The decimal precision for the quantity on trades and positions.  This value can be overridden at Investment Sub Type or Investment Sub Type 2.'},
						{header: 'Days to Settle', width: 30, dataIndex: 'daysToSettle', type: 'int', tooltip: 'Specifies the number of days it may take to settle a security of this type. This value can be overridden at Investment Hierarchy or security level (custom fields).'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.setup.InvestmentTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Investment Hierarchies',
				items: [{
					name: 'investmentInstrumentHierarchyListFind',
					xtype: 'gridpanel',
					instructions: 'A flat list of all investment hierarchies in the system.',
					pageSize: 200,
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Hierarchy', width: 150, dataIndex: 'labelExpanded', defaultSortColumn: true},
						{header: 'Level', width: 30, dataIndex: 'level', hidden: true, filter: false, sortable: false},
						{header: 'Investment Type', width: 60, dataIndex: 'investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Sub Type', width: 60, dataIndex: 'investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}},
						{header: 'Sub Type 2', width: 60, dataIndex: 'investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}},
						{header: 'Security Allocation Type', width: 50, dataIndex: 'securityAllocationType', hidden: true},
						{header: 'Assignment Allowed', width: 50, dataIndex: 'assignmentAllowed', type: 'boolean'},
						{header: 'One Per Instrument', width: 50, dataIndex: 'oneSecurityPerInstrument', type: 'boolean'},
						{header: 'Trading Disallowed', width: 50, dataIndex: 'tradingDisallowed', type: 'boolean'},
						{header: 'Not a Security', width: 50, dataIndex: 'notASecurity', type: 'boolean'},
						{header: 'Currency', width: 50, dataIndex: 'currency', hidden: true, type: 'boolean'},
						{header: 'Allow Different Settle CCY', width: 50, dataIndex: 'differentSettlementCurrencyAllowed', hidden: true, type: 'boolean'},
						{header: 'OTC', width: 30, dataIndex: 'otc', type: 'boolean'},
						{
							header: 'Pricing Frequency', width: 30, hidden: true, dataIndex: 'pricingFrequency',
							filter: {
								type: 'combo', searchFieldName: 'pricingFrequency', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.investment.instrument.InvestmentPricingFrequencies
								})
							}
						},
						{header: 'No Payment On Open', width: 50, dataIndex: 'noPaymentOnOpen', type: 'boolean'},
						{header: 'Close On Maturity Only', width: 50, dataIndex: 'closeOnMaturityOnly', type: 'boolean'},
						{header: 'Different Quantity And Cost Basis Sign Allowed', width: 50, dataIndex: 'differentQuantityAndCostBasisSignAllowed', type: 'boolean', hidden: true},
						{header: 'Factor Change Event', width: 50, dataIndex: 'factorChangeEventType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'factorChangeEventTypeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}},

						{header: 'Accrual Event', width: 50, dataIndex: 'accrualSecurityEventType.name', filter: {searchFieldName: 'accrualSecurityEventType'}, hidden: true},
						{header: 'Accrual Method', width: 50, dataIndex: 'accrualMethod', hidden: true, filter: {type: 'list', options: Clifton.investment.instrument.AccrualMethods}},
						{header: 'Accrual Event 2', width: 50, dataIndex: 'accrualSecurityEventType2.name', filter: {searchFieldName: 'accrualSecurityEventType2'}, hidden: true},
						{header: 'Accrual Method 2', width: 50, dataIndex: 'accrualMethod2', hidden: true, filter: {type: 'list', options: Clifton.investment.instrument.AccrualMethods}},
						{header: 'Accrual Date Calculator', width: 50, dataIndex: 'accrualDateCalculator', hidden: true, filter: {type: 'list', options: Clifton.investment.instrument.AccrualDateCalculators}},
						{header: 'Accrual Date Calculator 2', width: 50, dataIndex: 'accrualDateCalculator2', hidden: true, filter: {type: 'list', options: Clifton.investment.instrument.AccrualDateCalculators}},
						{header: 'Accrual Sign Calculator', width: 50, dataIndex: 'accrualSignCalculator', hidden: true, filter: {type: 'list', options: Clifton.investment.instrument.AccrualSignCalculators}},
						{header: 'Accrual Based on Notional', width: 50, dataIndex: 'accrualBasedOnNotional', type: 'boolean', hidden: true},
						{header: 'Accrual 2 Based on Notional', width: 50, dataIndex: 'accrual2BasedOnNotional', type: 'boolean', hidden: true},
						{header: 'Trade Accrual Based on Opening Notional', width: 50, dataIndex: 'tradeAccrualBasedOnOpeningNotional', type: 'boolean', hidden: true},
						{header: 'No Accrual on Opening Trade', width: 50, dataIndex: 'tradeAccrualOnOpeningAbsent', type: 'boolean', hidden: true},
						{header: 'Accrue After Ex Date', width: 50, dataIndex: 'accrueAfterExDate', type: 'boolean', hidden: true},
						{header: 'Full Payment for First Partial Period', width: 50, dataIndex: 'fullPaymentForFirstPartialPeriod', type: 'boolean', hidden: true},

						{header: 'Include pending leg payments with accrual amount', width: 50, dataIndex: 'includeAccrualReceivables', type: 'boolean', hidden: true},
						{header: 'Notional Multiplier Retriever', width: 50, dataIndex: 'notionalMultiplierRetriever.name', hidden: true, filter: {searchFieldName: 'notionalMultiplierRetriever'}},
						{header: 'Do Not Use Notional Multiplier for Price', width: 50, dataIndex: 'notionalMultiplierForPriceNotUsed', type: 'boolean', hidden: true},
						{header: 'Do Not Use Notional Multiplier for Accrual', width: 50, dataIndex: 'notionalMultiplierForAccrualNotUsed', type: 'boolean', hidden: true},
						{header: 'Index Ratio Adjusted', width: 50, dataIndex: 'indexRatioAdjusted', type: 'boolean', hidden: true},
						{header: 'Index Ratio Calculated Using Month Start After Prev Coupon', width: 50, dataIndex: 'indexRatioCalculatedUsingMonthStartAfterPrevCoupon', type: 'boolean', hidden: true},
						{header: 'Index Ratio Precision', width: 50, dataIndex: 'indexRatioPrecision', type: 'int', hidden: true},

						{header: 'Cash Location Purpose', width: 50, dataIndex: 'cashLocationPurpose.name', hidden: true, filter: {type: 'combo', searchFieldName: 'cashLocationPurposeId', url: 'investmentAccountRelationshipPurposeListFind.json'}},
						{header: 'Cash Location Purpose 2', width: 50, dataIndex: 'cashLocationPurpose2.name', hidden: true, filter: {type: 'combo', searchFieldName: 'cashLocationPurpose2Id', url: 'investmentAccountRelationshipPurposeListFind.json'}},
						{header: 'Holding Account Type', width: 50, dataIndex: 'holdingAccountType.name', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingAccountTypeId', url: 'investmentAccountTypeListFind.json'}},
						{header: 'Issuer Type', width: 50, dataIndex: 'businessCompanyType.name', hidden: true, filter: {type: 'combo', searchFieldName: 'businessCompanyTypeId', url: 'businessCompanyTypeListFind.json'}},

						{header: 'Instrument Screen', width: 50, dataIndex: 'instrumentScreen', hidden: true},
						{header: 'Illiquid', width: 40, dataIndex: 'illiquid', type: 'boolean'},
						{header: 'Notional Calculator', width: 50, dataIndex: 'costCalculator'},
						{header: 'M2M Calculator', width: 50, dataIndex: 'm2mCalculator'},
						{header: 'Default Exchange', width: 50, dataIndex: 'defaultExchange.name', hidden: true},
						{header: 'Deliverable', width: 50, dataIndex: 'instrumentDeliverableType', hidden: true},
						{header: 'Reference Allowed', width: 50, dataIndex: 'referenceSecurityAllowed', type: 'boolean', hidden: true, tooltip: 'Securities of this hierarchy allow a reference security to be defined'},
						{header: 'Reference from Same Instrument', width: 50, dataIndex: 'referenceSecurityFromSameInstrument', type: 'boolean', hidden: true},
						{header: 'Reference Child not Instructed for Events', width: 50, dataIndex: 'referenceSecurityChildNotInstructedForEvents', type: 'boolean', hidden: true},
						{header: 'Reference Security Label', width: 50, dataIndex: 'referenceSecurityLabel', hidden: true},
						{header: 'Reference Security Tooltip', width: 100, dataIndex: 'referenceSecurityTooltip', hidden: true},
						{header: 'Days to Settle', width: 50, dataIndex: 'daysToSettle', type: 'int', useNull: true},
						{header: 'Price Multiplier', width: 50, dataIndex: 'priceMultiplier', type: 'int', useNull: true, hidden: true},
						{header: 'Security Price Multiplier Override Allowed', width: 50, dataIndex: 'securityPriceMultiplierOverrideAllowed', type: 'boolean', hidden: true},
						{header: 'Underlying Hierarchy', width: 50, dataIndex: 'underlyingHierarchy.labelExpanded', hidden: true, filter: {type: 'combo', searchFieldName: 'underlyingHierarchyId', url: 'investmentInstrumentHierarchyListFind.json', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Underlying Instrument Group', width: 50, dataIndex: 'underlyingInvestmentGroup.name', hidden: true, filter: {type: 'combo', searchFieldName: 'underlyingInvestmentGroupId', url: 'investmentGroupListFind.json'}}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('assignmentAllowed', true);
						}
					},
					editor: {
						detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Instrument Group Types',
				items: [{
					name: 'investmentGroupTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Instrument Group Types define the rules that will be applied to corresponding Instrument Groups.',
					columns: [
						{header: 'ID', dataIndex: 'id', hidden: true},
						{header: 'Group Type Name', dataIndex: 'name', width: 100},
						{header: 'Description', dataIndex: 'description', with: 250, hidden: true},
						{header: 'Levels Deep', dataIndex: 'levelsDeep', width: 40, type: 'int', useNull: true},
						{header: 'Currency Type', dataIndex: 'currencyType', width: 60},
						{header: 'Template', dataIndex: 'template', type: 'boolean', width: 40},
						{header: 'Based on Template', dataIndex: 'basedOnTemplate', type: 'boolean', width: 65},
						{header: 'Single Group Item', dataIndex: 'singleGroupItem', type: 'boolean', width: 65},
						{header: 'Base Currency Required', dataIndex: 'baseCurrencyRequired', type: 'boolean', width: 75},
						{header: 'Missing Not Allowed', dataIndex: 'missingNotAllowed', type: 'boolean', width: 70},
						{header: 'Leaf Assignments Only', dataIndex: 'leafAssignmentsOnly', type: 'boolean', width: 75}
					],
					editor: {
						detailPageClass: 'Clifton.investment.setup.InvestmentGroupTypeWindow'
					}
				}]
			},


			{
				title: 'Currency Conventions',
				items: [{
					name: 'investmentCurrencyConventionListFind',
					xtype: 'gridpanel',
					instructions: 'FX Rate between USD and GBP is quoted as 1.5 meaning 1 USD = 1 GBP / 1.5 [multiplyFromByFX = false], but FX rate between USD and JPY is quoted as 80 meaning 1 USD = 1 JPY * 80 [multiplyFromByFX = true]',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'From Currency', width: 100, dataIndex: 'fromCurrency.label', filter: {type: 'combo', searchFieldName: 'fromCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'To Currency', width: 100, dataIndex: 'toCurrency.label', filter: {type: 'combo', searchFieldName: 'toCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Multiply From By FX Rate', width: 100, dataIndex: 'multiplyFromByFX', type: 'boolean'},
						{header: 'Dominant Currency', width: 100, dataIndex: 'dominantCurrency.label'},
						{header: 'Days to Settle', width: 50, dataIndex: 'daysToSettle', type: 'int', useNull: true}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.currency.CurrencyConventionWindow'
					}
				}]
			},


			{
				title: 'Security Event Statuses',
				items: [{
					name: 'investmentSecurityEventStatusListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'Event Status is used to define a life-cycle of an event.  Event is always in a specific status that may change over time: Incomplete => Approved.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Event Status Name', width: 100, dataIndex: 'name', renderer: Clifton.investment.instrument.event.renderEventStatus, defaultSortColumn: true},
						{header: 'Description', width: 400, dataIndex: 'description'},
						{header: 'Allow Journals', width: 50, dataIndex: 'eventJournalAllowed', type: 'boolean'},
						{header: 'Auto Journals', width: 50, dataIndex: 'eventJournalAutomatic', type: 'boolean'},
						{header: 'Journals N/A', width: 50, dataIndex: 'eventJournalNotApplicable', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.EventStatusWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Security Event Types',
				items: [{
					name: 'investmentSecurityEventTypeListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'Security event types define types of events that apply to securities and need to be accounted for (stock splits, dividend payments, factor changes, etc.). Event types are assigned to Investment Hierarchy nodes in order to specify what types are allowed for specific securities.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Event Type Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'One Value', width: 40, dataIndex: 'beforeSameAsAfter', type: 'boolean'},
						{header: 'Percent Value', width: 50, dataIndex: 'valuePercent', type: 'boolean'},
						{header: 'Allow Additional', width: 50, dataIndex: 'additionalSecurityAllowed', type: 'boolean'},
						{header: 'Additional is CCY', width: 55, dataIndex: 'additionalSecurityCurrency', type: 'boolean'},
						{header: 'CCY Units', width: 37, dataIndex: 'eventValueInEventCurrencyUnits', type: 'boolean', tooltip: 'When Additional Security is Currency, specifies if the Before and After Value are in the Event currency units or security currency units. For example, "Cash Dividend Payment" event type will have this field set to true, meaning the dividend per share amount (Before/After Value) is in Additional Security units (Event currency) and the exchange rate is from Event currency to Client Account base currency.'},
						{header: 'New Position', width: 45, dataIndex: 'newPositionCreated', type: 'boolean'},
						{header: 'New Security', width: 45, dataIndex: 'newSecurityCreated', type: 'boolean'},
						{header: 'Payment Delay', width: 47, dataIndex: 'paymentDelayAllowed', type: 'boolean'},
						{header: 'One Event', width: 40, dataIndex: 'onePerEventDate', type: 'boolean', tooltip: 'Allow only one event of this type per event date for each security.'},
						{header: 'No Payout', width: 40, dataIndex: 'withoutPayout', type: 'boolean', tooltip: 'Identifies events that have not payouts (change to domicile, call for shareholder approval) but still offer valuable information to the holder.'},
						{header: 'Single Payout', width: 45, dataIndex: 'singlePayoutOnly', type: 'boolean', tooltip: 'Events with a single payout have all of event information on the event (payout table is not used).'},
						{header: 'Actions', width: 32, dataIndex: 'actionAllowed', type: 'boolean'},
						{header: 'Natural Key Description', width: 45, dataIndex: 'descriptionIncludedInNaturalKey', type: 'boolean', hidden: true},
						{header: 'Precision', width: 40, dataIndex: 'decimalPrecision', type: 'int'},
						{header: 'Min Value', width: 45, dataIndex: 'minValue', type: 'currency', hidden: true},
						{header: 'Max Value', width: 50, dataIndex: 'maxValue', type: 'currency', hidden: true},
						{
							header: 'Before Start', width: 40, dataIndex: 'eventBeforeStartType',
							tooltip: 'Number of events allowed before the security start date'
						},
						{header: 'Force Status', width: 40, dataIndex: 'forceEventStatus.name', filter: {searchFieldName: 'forceEventStatus'}},
						{header: 'Order', width: 30, dataIndex: 'eventOrder', type: 'int'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Event Payout Types',
				items: [{
					name: 'investmentSecurityEventPayoutTypeListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'Security event payout types define types of payout that apply to security events (Currency, Security, etc.). Each security event can have more than one payout and these payouts can be of different types. Payout types are assigned to corresponding event types to limit valid choices.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Payout Type Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Payout Code', width: 40, dataIndex: 'typeCode'},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'One Value', width: 40, dataIndex: 'beforeSameAsAfter', type: 'boolean'},
						{header: 'Additional is CCY', width: 55, dataIndex: 'additionalSecurityCurrency', type: 'boolean'},
						{header: 'CCY Units', width: 37, dataIndex: 'eventValueInEventCurrencyUnits', type: 'boolean', tooltip: 'When Additional Security is Currency, specifies if the Before and After Value are in the Event currency units or security currency units. For example, "Cash Dividend Payment" event type will have this field set to true, meaning the dividend per share amount (Before/After Value) is in Additional Security units (Event currency) and the exchange rate is from Event currency to Client Account base currency.'},
						{header: 'New Position', width: 45, dataIndex: 'newPositionCreated', type: 'boolean'},
						{header: 'New Security', width: 45, dataIndex: 'newSecurityCreated', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.payout.SecurityEventPayoutTypeWindow',
						drillDownOnly: true
					}
				}]
			},

			{
				title: 'Event Action Types',
				items: [{
					name: 'investmentSecurityEventActionTypeListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'This window shows a list of Investment Security Event Action Types used in Investment Security Event Action entities to specify the type of action.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Action Type Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Event Type', width: 40, dataIndex: 'eventType.name', tooltip: 'The Investment Security Event Type associated with this event action type.'},
						{header: 'Description', width: 280, dataIndex: 'description'},
						{header: 'Action Bean', width: 100, dataIndex: 'actionBean.label', hidden: true},
						{header: 'Process Before', width: 35, dataIndex: 'processBeforeEventJournal', type: 'boolean', tooltip: 'Process actions of this type before processing the corresponding Event Journal'},
						{header: 'Manual', width: 50, dataIndex: 'manual', type: 'boolean', tooltip: 'If checked, indicates that this is a manual action type used where special processing is required. Manual event actions are manually added to an Investment Security Event and processed manually. Examples may be security event actions related to OTC Securities, or Price Baskets.'},
						{header: 'Processing Order', width: 40, type: 'int', dataIndex: 'processingOrder'},
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.action.EventActionTypeWindow',
						deleteURL: 'investmentSecurityEventActionTypeDelete.json',

						addToolbarAddButton: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Add',
								tooltip: 'Add a new item',
								iconCls: 'add',
								name: 'add',
								scope: this,
								handler: function() {
									this.openDetailPage(this.getDetailPageClass(), gridPanel);
								}
							});
							toolBar.add('-');
						},
						addToolbarDeleteButton: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Remove',
								tooltip: 'Remove selected item(s)',
								iconCls: 'remove',
								name: 'remove',
								scope: this,
								handler: function() {
									const editor = this;
									const sm = editor.grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
									}
									else if (sm.getCount() !== 1) {
										TCG.showError('Multi-selection deletes are not supported yet.  Please select one row.', 'NOT SUPPORTED');
									}
									else {
										const deleteConfirmMessage = editor.deleteConfirmMessage ? editor.deleteConfirmMessage : 'Would you like to delete selected row?';
										Ext.Msg.confirm('Delete Selected Row', deleteConfirmMessage, function(a) {
											if (a === 'yes') {
												editor.deleteSelection(sm, gridPanel, editor.getDeleteURL());
											}
										});
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},

			{
				title: 'Delivery Month Codes',
				items: [{
					name: 'investmentDeliveryMonthCodesList',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					instructions: 'Applies to futures contracts and represents the delivery month of the future contract.  On the ticker, delivery month is indicated by a letter.  The code representations for each month are listed below.',
					columns: [
						{header: 'Code', width: 20, dataIndex: 'value'},
						{header: 'Month', width: 200, dataIndex: 'name'}
					],
					createStore: function(fields) {
						return new Ext.data.ArrayStore({
							fields: ['name', 'value'],
							data: Clifton.investment.setup.DeliveryMonthCodes
						});
					}
				}]
			}
		]
	}]
});
