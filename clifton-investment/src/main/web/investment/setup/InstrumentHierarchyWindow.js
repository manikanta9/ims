Clifton.investment.setup.InstrumentHierarchyWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Hierarchy',
	iconCls: 'hierarchy',
	height: 700,
	width: 800,
	allowOpenFromModal: true,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Hierarchy',
				items: [{
					xtype: 'formpanel',
					instructions: 'Investment instruments are organized into hierarchy nodes that group them as well as define their behavior.',
					url: 'investmentInstrumentHierarchy.json',
					labelWidth: 170,

					items: [
						{fieldLabel: 'Parent Hierarchy', name: 'parent.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'parent.id'},
						{fieldLabel: 'Hierarchy Name', name: 'name'},
						{fieldLabel: 'Hierarchy Label', name: 'label'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},

						{boxLabel: 'Allow assignment of investment instruments to this hierarchy node', name: 'assignmentAllowed', xtype: 'checkbox'},
						{boxLabel: 'Each instrument in this hierarchy always has one security: One-to-One', name: 'oneSecurityPerInstrument', xtype: 'checkbox', requiredFields: ['assignmentAllowed']},
						{boxLabel: 'This hierarchy is for physical currencies only', name: 'currency', xtype: 'checkbox', requiredFields: ['assignmentAllowed', 'oneSecurityPerInstrument']},
						{boxLabel: 'Allow Settlement CCY to be different than CCY Denomination for OTC security', name: 'differentSettlementCurrencyAllowed', xtype: 'checkbox', requiredFields: ['assignmentAllowed']},
						{boxLabel: 'This hierarchy is for non-tradable securities: benchmarks and indices', name: 'tradingDisallowed', xtype: 'checkbox', requiredFields: ['assignmentAllowed', 'oneSecurityPerInstrument']},
						{
							boxLabel: 'This hierarchy does not contain real securities', name: 'notASecurity', xtype: 'checkbox', requiredFields: ['tradingDisallowed'],
							qtip: 'Used for non-tradable securities to also mark them as not real securities.  This indicates that prices do not apply to this hierarchy.'
						},
						{
							fieldLabel: 'Pricing Frequency', name: 'pricingFrequency', hiddenName: 'pricingFrequency', requiredFields: ['assignmentAllowed'], displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
							store: new Ext.data.ArrayStore({
								fields: ['name', 'value', 'description'],
								data: Clifton.investment.instrument.InvestmentPricingFrequencies
							}),
							qtip: 'Some securities (ex. Agency MBS Bonds, some Benchmarks) we get prices monthly or quarterly only.'
						},
						// Security Allocations
						{boxLabel: 'Securities in this hierarchy are an allocation of securities', name: 'allocationOfSecurities', xtype: 'checkbox', requiredFields: ['assignmentAllowed']},
						{
							fieldLabel: 'Allocation Type', name: 'securityAllocationType', hiddenName: 'securityAllocationType', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', requiredFields: ['allocationOfSecurities'],
							store: new Ext.data.ArrayStore({
								fields: ['name', 'value', 'description'],
								data: Clifton.investment.instrument.SecurityAllocationTypes
							})
						},
						{
							fieldLabel: 'Dynamic Calculator', name: 'dynamicAllocationCalculatorSystemBean.name', hiddenName: 'dynamicAllocationCalculatorSystemBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Investment Security Allocation Dynamic Calculator', detailPageClass: 'Clifton.system.bean.BeanWindow',
							qtip: 'Dynamic Allocation Calculators dynamically build the allocation list and weights for a specific date based on the calculator.',
							requiredFields: 'securityAllocationType'
						},

						{boxLabel: 'Allow different sign for Quantity and Position Cost Basis (CDS)', name: 'differentQuantityAndCostBasisSignAllowed', xtype: 'checkbox', requiredFields: ['assignmentAllowed']},
						{boxLabel: 'Securities in this hierarchy close on maturity only', name: 'closeOnMaturityOnly', xtype: 'checkbox', requiredFields: ['assignmentAllowed']},
						{boxLabel: 'Securities in this hierarchy do not have a payment on open (no upfront payment)', name: 'noPaymentOnOpen', xtype: 'checkbox', requiredFields: ['assignmentAllowed']},
						{boxLabel: 'Securities in this hierarchy are Illiquid (cannot be easily sold or exchanged for cash)', name: 'illiquid', xtype: 'checkbox'},
						{boxLabel: 'Securities in this hierarchy are OTC traded', name: 'otc', xtype: 'checkbox', requiredFields: ['assignmentAllowed']},
						{boxLabel: 'Securities in this hierarchy require collateral', name: 'collateralUsed', xtype: 'checkbox', requiredFields: ['assignmentAllowed']},
						{boxLabel: 'All Securities for an instrument have the same events (copy on update/delete)', name: 'eventSameForInstrumentSecurities', xtype: 'checkbox', requiredFields: ['assignmentAllowed'], mutuallyExclusiveFields: ['oneSecurityPerInstrument']},
						{
							boxLabel: 'Allow trading securities on End Date (Maturity Date)', name: 'tradingOnEndDateAllowed', xtype: 'checkbox',
							qtip: 'Specifies whether securities in this hierarchy can be traded on Maturity Date. For example, T-Bills cannot be traded so maturity event will have Ex Date = End Date (vs one day after).'
						},

						{
							xtype: 'fieldset-checkbox',
							title: 'Referenced Security Allowed',
							checkboxName: 'referenceSecurityAllowed',
							instructions: 'Securities in this hierarchy can specify a reference security',
							anchor: '0',
							items: [
								{fieldLabel: 'Reference Security Label', name: 'referenceSecurityLabel'},
								{fieldLabel: 'Reference Security Tooltip', name: 'referenceSecurityTooltip', xtype: 'textarea', height: 50},
								{boxLabel: 'Reference security must be from the same investment instrument', name: 'referenceSecurityFromSameInstrument', xtype: 'checkbox'},
								{boxLabel: 'Do not instruct Events for child securities (instructed by the parent)', name: 'referenceSecurityChildNotInstructedForEvents', xtype: 'checkbox'}
							]
						},

						{
							xtype: 'fieldset-checkbox',
							title: 'Notional Multiplier',
							checkboxName: 'notionalMultiplierUsed',
							instructions: 'Securities in this hierarchy use additional Notional Multiplier in price/notional and/or accrual calculations: Inflation Linked, Zero Coupon Swaps, etc.',
							anchor: '0',
							items: [
								{fieldLabel: 'Notional Multiplier Retriever', name: 'notionalMultiplierRetriever.name', hiddenName: 'notionalMultiplierRetriever.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Notional Multiplier Retriever', detailPageClass: 'Clifton.system.bean.BeanWindow'},
								{boxLabel: 'Do not use Notional Multiplier in Price/Notional calculations', name: 'notionalMultiplierForPriceNotUsed', xtype: 'checkbox'},
								{boxLabel: 'Do not use Notional Multiplier in Accrual calculations', name: 'notionalMultiplierForAccrualNotUsed', xtype: 'checkbox'},
								{boxLabel: 'Index Ratio applies to Inflation Linked securities in this hierarchy', name: 'indexRatioAdjusted', xtype: 'checkbox'},
								{fieldLabel: 'Index Ratio Precision', name: 'indexRatioPrecision', xtype: 'spinnerfield'},
								{boxLabel: 'Use current Inflation Index value on the first day of the month following last coupon payment date', name: 'indexRatioCalculatedUsingMonthStartAfterPrevCoupon', xtype: 'checkbox'}
							]
						},

						{
							xtype: 'fieldset-checkbox',
							title: 'Accrual Settings',
							checkboxName: 'accrualAllowed',
							instructions: 'Define rules used to calculate Accrual value for securities with periodic payments based on interest rate.',
							labelWidth: 140,
							anchor: '0',
							items: [
								{fieldLabel: 'Accrual Event Type', name: 'accrualSecurityEventType.name', hiddenName: 'accrualSecurityEventType.id', xtype: 'combo', url: 'investmentSecurityEventTypeListFind.json', requiredFields: ['assignmentAllowed'], qtip: 'For hierarchies with securities that have periodic payments based on an interest rate (Cash Coupon Payment for bonds), identifies the InvestmentSecurityEventType that has payment information.  The same info is also used to calculate accrued interest.'},
								{
									fieldLabel: 'Accrual Method', name: 'accrualMethod', hiddenName: 'accrualMethod', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', requiredFields: ['assignmentAllowed', 'accrualSecurityEventType.name'],
									qtip: 'Uses [Coupon Frequency] and [Day Count] custom fields for methods that need them.',
									store: new Ext.data.ArrayStore({
										fields: ['value', 'name', 'description'],
										data: Clifton.investment.instrument.AccrualMethods
									})
								},
								{
									fieldLabel: 'Accrual Date Calculator', name: 'accrualDateCalculator', hiddenName: 'accrualDateCalculator', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
									qtip: 'When security valuation is performed on a given date, use this calculator for Accrual Event Type to determine the date up to which accrual is calculated.',
									requiredFields: ['accrualSecurityEventType.name'],
									store: new Ext.data.ArrayStore({
										fields: ['value', 'name', 'description'],
										data: Clifton.investment.instrument.AccrualDateCalculators
									})
								},
								{boxLabel: 'Base Accrual calculation on Notional (TRS) as opposed to Quantity (Bonds)', name: 'accrualBasedOnNotional', xtype: 'checkbox', requiredFields: ['accrualSecurityEventType.name']},

								{fieldLabel: 'Accrual Event Type 2', name: 'accrualSecurityEventType2.name', hiddenName: 'accrualSecurityEventType2.id', xtype: 'combo', url: 'investmentSecurityEventTypeListFind.json', requiredFields: ['assignmentAllowed', 'accrualSecurityEventType.name'], qtip: 'Use this in rare cases when there are 2 different accrual events (Interest Rate Swaps). See Accrual Event Type tooltip for more details.'},
								{
									fieldLabel: 'Accrual Method 2', name: 'accrualMethod2', hiddenName: 'accrualMethod2', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', requiredFields: ['assignmentAllowed', 'accrualSecurityEventType.name'],
									qtip: 'Uses [Coupon Frequency 2] and [Day Count 2] custom fields for methods that need them.',
									store: new Ext.data.ArrayStore({
										fields: ['value', 'name', 'description'],
										data: Clifton.investment.instrument.AccrualMethods
									})
								},
								{
									fieldLabel: 'Accrual Date Calculator 2', name: 'accrualDateCalculator2', hiddenName: 'accrualDateCalculator2', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
									qtip: 'When security valuation is performed on a given date, use this calculator for Accrual Event Type 2 to determine the date up to which accrual is calculated.',
									requiredFields: ['accrualSecurityEventType.name'],
									store: new Ext.data.ArrayStore({
										fields: ['value', 'name', 'description'],
										data: Clifton.investment.instrument.AccrualDateCalculators
									})
								},
								{boxLabel: 'Base Accrual 2 calculation on Notional (TRS) as opposed to Quantity (Bonds)', name: 'accrual2BasedOnNotional', xtype: 'checkbox', requiredFields: ['accrualSecurityEventType2.name']},

								{
									fieldLabel: 'Accrual Sign Calculator', name: 'accrualSignCalculator', hiddenName: 'accrualSignCalculator', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
									requiredFields: ['accrualSecurityEventType.name'],
									store: new Ext.data.ArrayStore({
										fields: ['value', 'name', 'description'],
										data: Clifton.investment.instrument.AccrualSignCalculators
									})
								},
								{boxLabel: 'Base Trade Accrual on opening Notional (TRS)', name: 'tradeAccrualBasedOnOpeningNotional', xtype: 'checkbox', requiredFields: ['accrualSecurityEventType.name'], qtip: 'Some securities (TRS) use base trade accrual on opening notional. For vanilla TRS, there is no accrual on opening and closing accrual is calculated based on the corresponding notional being closed. For iBoxx, opening trade accrual is based on notional and closing trade accrual is based on opening notional being closed.'},
								{boxLabel: 'No Accrual on Opening Trades (closing trades only) (TRS)', name: 'tradeAccrualOnOpeningAbsent', xtype: 'checkbox', requiredFields: ['accrualSecurityEventType.name']},
								{boxLabel: 'Continue accruing after Ex Date (uncheck to negate after Ex Date)', name: 'accrueAfterExDate', xtype: 'checkbox', requiredFields: ['accrualSecurityEventType.name'], qtip: 'Most of the time Ex Date is one day after Accrual End Date and therefore there\'s no accrual after Ex Date.British Linkers (Gilts) have the concept of Ex Date which is 8 days before period end date.One is entitled to full dividend for a position at close of day before Ex Date.From Ex Date to Accrual Period End Date, we\'re accruing negative interest. Total Return Swaps need to have this field set because they continue accruing until the end of the period (even after Ex Date).'},
								{boxLabel: 'Full payment for first partial accrual period', name: 'fullPaymentForFirstPartialPeriod', xtype: 'checkbox', requiredFields: ['accrualSecurityEventType.name']},
								{boxLabel: 'Include pending leg payments with accrual amount', name: 'includeAccrualReceivables', xtype: 'checkbox', requiredFields: ['accrualSecurityEventType.name']}
							]
						},

						{fieldLabel: 'Factor Change Event', name: 'factorChangeEventType.name', hiddenName: 'factorChangeEventType.id', xtype: 'combo', url: 'investmentSecurityEventTypeListFind.json', requiredFields: ['assignmentAllowed'], qtip: 'Some securities can have "Factor Change" event (asset backed bonds, credit default swaps, etc.) If set, then factor change is allowed and the following event type is used to process factor changes.'},
						{
							fieldLabel: 'Investment Type', name: 'investmentType.name', hiddenName: 'investmentType.id', xtype: 'combo', url: 'investmentTypeList.json', loadAll: true,
							detailPageClass: 'Clifton.investment.setup.InvestmentTypeWindow', disableAddNewItem: true,
							listeners: {
								beforeselect: function(combo, record) {
									const f = combo.getParentForm().getForm();
									let subType = f.findField('investmentTypeSubType.name');
									subType.clearAndReset();
									subType = f.findField('investmentTypeSubType2.name');
									subType.clearAndReset();
								}
							}
						},

						{
							fieldLabel: 'Investment Sub Type', name: 'investmentTypeSubType.name', hiddenName: 'investmentTypeSubType.id', xtype: 'combo', url: 'investmentTypeSubTypeListByType.json', loadAll: true, requiredFields: ['investmentType.name'],
							detailPageClass: 'Clifton.investment.setup.InvestmentTypeSubTypeWindow', disableAddNewItem: true,
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									combo.store.baseParams = {investmentTypeId: f.findField('investmentType.name').getValue()};
								}
							}
						},
						{
							fieldLabel: 'Investment Sub Type 2', name: 'investmentTypeSubType2.name', hiddenName: 'investmentTypeSubType2.id', xtype: 'combo', url: 'investmentTypeSubType2ListByType.json', loadAll: true, requiredFields: ['investmentType.name'],
							detailPageClass: 'Clifton.investment.setup.InvestmentTypeSubType2Window', disableAddNewItem: true,
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									combo.store.baseParams = {investmentTypeId: f.findField('investmentType.name').getValue()};
								}
							}
						},
						{
							fieldLabel: 'Instrument Screen', name: 'instrumentScreen', hiddenName: 'instrumentScreen', displayField: 'name', valueField: 'instrumentScreen', mode: 'local', xtype: 'combo',
							requiredFields: ['assignmentAllowed'],
							store: new Ext.data.ArrayStore({
								fields: ['name', 'instrumentScreen', 'description'],
								data: Clifton.investment.setup.InstrumentHierarchyScreens
							})
						},
						{
							fieldLabel: 'Notional Calculator', name: 'costCalculator', hiddenName: 'costCalculator', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
							requiredFields: ['assignmentAllowed'],
							store: new Ext.data.ArrayStore({
								fields: ['name', 'value', 'description'],
								data: Clifton.investment.instrument.NotionalCalculators
							})
						},
						{
							fieldLabel: 'M2M Calculator', name: 'm2mCalculator', hiddenName: 'm2mCalculator', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
							requiredFields: ['assignmentAllowed'],
							store: new Ext.data.ArrayStore({
								fields: ['name', 'value', 'description'],
								data: Clifton.investment.instrument.M2MCalculators
							})
						},
						{fieldLabel: 'Cash Location Purpose', name: 'cashLocationPurpose.name', hiddenName: 'cashLocationPurpose.id', xtype: 'combo', displayField: 'name', url: 'investmentAccountRelationshipPurposeListFind.json', disableAddNewItem: true, requiredFields: ['assignmentAllowed'], qtip: 'Specifies an override for what holding account to use for Cash when booking to the General Ledger.  By default will be the same holding account as the one used for Position.'},
						{fieldLabel: 'Cash Location Purpose 2', name: 'cashLocationPurpose2.name', hiddenName: 'cashLocationPurpose2.id', xtype: 'combo', displayField: 'name', url: 'investmentAccountRelationshipPurposeListFind.json', disableAddNewItem: true, requiredFields: ['assignmentAllowed', 'cashLocationPurpose.name'], qtip: 'Used to allow overrides for Cash Location Purpose field. The logic is COALESCE(Cash Location Purpose, Cash Location Purpose 2).  For example, Cash Location Purpose = Cash Location Override, Cash Location Purpose 2 = Custodian.'},
						{fieldLabel: 'Holding Account Type', name: 'holdingAccountType.name', hiddenName: 'holdingAccountType.id', xtype: 'combo', url: 'investmentAccountTypeListFind.json', detailPageClass: 'Clifton.investment.account.AccountTypeWindow', disableAddNewItem: true, qtip: 'Optionally specifies the type of Holding Account for every security in this hierarchy: OTC Account, OTC Cleared, etc.'},
						{fieldLabel: 'Issuer Type', name: 'businessCompanyType.name', hiddenName: 'businessCompanyType.id', xtype: 'combo', url: 'businessCompanyTypeListFind.json', detailPageClass: 'Clifton.business.company.CompanyTypeWindow', disableAddNewItem: true, qtip: 'Optionally specifies the type of Business Company (issuer/counterparty) for every security in this hierarchy: "Registered Investment Company" for ETF, etc.'},
						{fieldLabel: 'Default Exchange', name: 'defaultExchange.name', hiddenName: 'defaultExchange.id', xtype: 'combo', url: 'investmentExchangeListFind.json', detailPageClass: 'Clifton.investment.setup.ExchangeWindow'},
						{
							fieldLabel: 'Settlement Calendar Field', name: 'settlementCalendarColumn.name', hiddenName: 'settlementCalendarColumn.id', xtype: 'combo', url: 'systemColumnListFind.json?customOnly=true&tableName=InvestmentSecurity', detailPageClass: 'Clifton.system.schema.ColumnWindow', disableAddNewItem: true,
							qtip: 'When selected, this is the custom column on the security that holds the settlement calendar selection, which is used to determine business day logic for the security which is used for price look ups.',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									// Filter to security custom columns used for this hierarchy only
									combo.store.baseParams = {linkedValue: combo.getParentForm().getWindow().getMainFormId()};
								}
							}
						},
						{fieldLabel: 'Days to Settle', name: 'daysToSettle', xtype: 'integerfield', requiredFields: ['assignmentAllowed'], qtip: 'Specifies the number of days it may take to settle a security in this Hierarchy. This value overrides the value specified for the Investment Type and can be overridden at Investment Instrument or Security level (custom fields).'},
						{fieldLabel: 'Price Multiplier', name: 'priceMultiplier', xtype: 'floatfield', qtip: 'Optional price multiplier to be used for all securities in this hierarchy.'},
						{
							boxLabel: 'Allow price multiplier overrides at individual security level', name: 'securityPriceMultiplierOverrideAllowed', xtype: 'checkbox',
							qtip: 'For One-to-Many Securities: Used for cases where securities for the same instrument in this hierarchy might need to use different price multipliers.  Example: UK Futures where security price multipliers are dependent on the number of days in the month.<br/>'
								+ 'For One-to-One Securities: Used for cases where hierarchy level price multiplier is entered, but individual level price multiplier overrides are still allowed that are different than the hierarchy price multiplier.'
						},
						{
							fieldLabel: 'Underlying Hierarchy', name: 'underlyingHierarchy.labelExpanded', hiddenName: 'underlyingHierarchy.id', xtype: 'combo', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', displayField: 'labelExpanded', queryParam: 'labelExpanded', listWidth: 600, requiredFields: ['assignmentAllowed'],
							qtip: 'Underlying Instrument selections will be required to be associated with the selected hierarchy'
						},
						{
							fieldLabel: 'Underlying Instrument Group', name: 'underlyingInvestmentGroup.name', hiddenName: 'underlyingInvestmentGroup.id', xtype: 'combo', url: 'investmentGroupListFind.json', requiredFields: ['assignmentAllowed'], detailPageClass: 'Clifton.investment.setup.GroupWindow',
							qtip: 'Underlying Instrument selections will be required to be associated with the selected group. For cases where underlying must come from a specific hierarchy, the underlying hierarchy selection should be used.'
						},
						{
							fieldLabel: 'Deliverable', name: 'instrumentDeliverableType', hiddenName: 'instrumentDeliverableType', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', requiredFields: ['assignmentAllowed'],
							store: new Ext.data.ArrayStore({
								fields: ['name', 'value', 'description'],
								data: Clifton.investment.instrument.InstrumentDeliverableTypes
							})
						}
					]
				}]
			},


			{
				title: 'Custom Security Fields',
				items: [{
					name: 'systemColumnCustomListFind',
					xtype: 'gridpanel',
					instructions: 'The following custom columns are associated with securities or instruments in this hierarchy.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Column Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Data Type', width: 70, dataIndex: 'dataType.name'},
						{header: 'Order', width: 50, dataIndex: 'order', type: 'int'},
						{header: 'Required', width: 50, dataIndex: 'required', type: 'boolean'},
						{header: 'Global', width: 50, dataIndex: 'linkedToAllRows', type: 'boolean', tooltip: 'Global custom fields are not hierarchy specific: apply to all rows'},
						{header: 'System', width: 50, dataIndex: 'systemDefined', type: 'boolean'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [{
							fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 150, minListWidth: 150, mode: 'local', value: 'SECURITY', displayField: 'name', valueField: 'value',
							store: new Ext.data.ArrayStore({
								fields: ['value', 'name', 'description'],
								data: [['SECURITY', 'Security Fields', 'Displays custom fields applicable to securities in this hierarchy.'], ['INSTRUMENT', 'Instrument Fields', 'Displays custom fields applicable to instruments in of this hierarchy.']]
							})
						}];
					},
					getLoadParams: function() {
						const displayType = TCG.getChildByName(this.getTopToolbar(), 'displayType').getValue();
						return {
							linkedValue: this.getWindow().getMainFormId(),
							columnGroupName: (displayType === 'SECURITY') ? 'Security Custom Fields' : 'Instrument Custom Fields',
							includeNullLinkedValue: true
						};
					},
					editor: {
						detailPageClass: 'Clifton.system.schema.ColumnWindow',
						addFromTemplateURL: 'systemColumn.json',
						deleteURL: 'systemColumnDelete.json',
						getDefaultData: function(gridPanel) { // defaults group
							let displayType = TCG.getChildByName(gridPanel.getTopToolbar(), 'displayType').getValue();
							displayType = (displayType === 'SECURITY') ? 'Security' : 'Instrument';
							const grp = TCG.data.getData('systemColumnGroupByName.json?name=' + displayType + ' Custom Fields', gridPanel, 'system.schema.group.' + displayType + ' Custom Fields');
							const fVal = gridPanel.getWindow().getMainForm().formValues;
							let lbl = TCG.getValue('parent.labelExpanded', fVal);
							if (TCG.isNotBlank(lbl)) {
								lbl += ' / ';
							}
							lbl += TCG.getValue('label', fVal);
							return {
								columnGroup: grp,
								linkedValue: TCG.getValue('id', fVal),
								linkedLabel: lbl,
								table: grp.table
							};
						},
						getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
							return undefined; // Not needed for Existing
						}
					}
				}]
			},


			{
				title: 'Event Types',
				items: [{
					name: 'investmentSecurityEventTypeHierarchyListByHierarchy',
					xtype: 'gridpanel',
					instructions: 'The following security event types are allowed for instruments in this hierarchy.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Event Type Name', width: 100, dataIndex: 'referenceOne.name', defaultSortColumn: true},
						{header: 'Description', width: 250, dataIndex: 'referenceOne.description', hidden: true},
						{header: 'Report', width: 200, dataIndex: 'eventReport.name'},
						{header: 'One Value', width: 50, dataIndex: 'referenceOne.beforeSameAsAfter', type: 'boolean'},
						{header: 'Creates New', width: 50, dataIndex: 'referenceOne.newSecurityCreated', type: 'boolean'},
						{header: 'Copy from Underlying', width: 80, dataIndex: 'copiedFromUnderlying', type: 'boolean'}
					],
					getLoadParams: function() {
						return {hierarchyId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.EventTypeHierarchyWindow',
						getDefaultData: function(gridPanel) {
							const dd = {
								referenceTwo: gridPanel.getWindow().getMainForm().formValues
							};

							const t = gridPanel.getTopToolbar();
							const et = TCG.getChildByName(t, 'eventType');
							if (TCG.isNotBlank(et.getValue())) {
								dd.referenceOne = {id: et.getValue(), name: et.getRawValue()};
							}
							return dd;
						},
						addToolbarAddButton: function(toolBar, gridPanel) {
							toolBar.add(new TCG.form.ComboBox({name: 'eventType', url: 'investmentSecurityEventTypeListFind.json', width: 150, listWidth: 230}));
							toolBar.add({
								text: 'Quick Add',
								tooltip: 'Quick addition to allow selected event type to be used for instruments in this hierarchy',
								iconCls: 'add',
								handler: function() {
									const eventTypeId = TCG.getChildByName(toolBar, 'eventType').getValue();
									if (TCG.isBlank(eventTypeId)) {
										TCG.showError('You must first select desired user Event Type from the list.');
									}
									else {
										const hierarchyId = gridPanel.getWindow().getMainFormId();
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Linking...',
											params: {instrumentHierarchyId: hierarchyId, eventTypeId: eventTypeId},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'eventType').reset();
											}
										});
										loader.load('investmentSecurityEventTypeHierarchyLink.json');
									}
								}
							});
							toolBar.add('-');
							toolBar.add({
								text: 'Add',
								tooltip: 'Add event type and also select a report',
								iconCls: 'add',
								scope: this,
								handler: function() {
									this.openDetailPage(this.getDetailPageClass(), gridPanel);
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},


			{
				title: 'Instruments',
				items: [{
					xtype: 'gridpanel',
					name: 'investmentInstrumentListFind',
					instructions: 'Investment instrument is a grouping of securities. Each futures instrument generally has 4 securities per year, etc.',
					getLoadParams: function() {
						return {hierarchyId: this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'Prefix', width: 70, dataIndex: 'identifierPrefix'},
						{header: 'Instrument Name', width: 100, dataIndex: 'name'},
						{header: 'Underlying', width: 100, dataIndex: 'underlyingInstrument.name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Primary Exchange', width: 100, dataIndex: 'exchange.label'},
						{header: 'Composite Exchange', width: 100, dataIndex: 'compositeExchange.label', hidden: true},
						{header: 'CCY Denomination', width: 100, dataIndex: 'tradingCurrency.name'},
						{header: 'Multiplier', width: 70, dataIndex: 'priceMultiplier', type: 'float'},
						{header: 'Deliverable', width: 50, dataIndex: 'deliverable', type: 'boolean', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Copy',
				items: [{
					xtype: 'formpanel',
					controlWindowModified: false,
					instructions: 'Create a copy of selected Hierarchy under a different parent. Also copies custom fields associated with this hierarchy.',
					buttons: [{
						type: 'button',
						text: 'Copy Hierarchy',
						handler: function() {
							const panel = TCG.getParentFormPanel(this);
							const form = panel.getForm();
							form.submit(Ext.applyIf({
								url: 'investmentInstrumentHierarchyCopy.json',
								waitMsg: 'Copying...',
								success: function(form, action) {
									Ext.Msg.alert('Copy Investment Hierarchy', 'Selected hierarchy was successfully copied.');
								}
							}, TCG.form.submitDefaults));
						}
					}],
					getDefaultData: function(form) {
						const f = this.getWindow().getMainForm().formValues;
						return {fromHierarchyId: f.id, newDescription: f.description};
					},
					items: [
						{name: 'fromHierarchyId', hidden: true},
						{fieldLabel: 'New Parent', name: 'newParentHierarchyName', hiddenName: 'newParentHierarchyId', valueField: 'id', displayField: 'labelExpanded', xtype: 'combo', url: 'investmentInstrumentHierarchyListFind.json', queryParam: 'labelExpanded', listWidth: 600, allowBlank: false, detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow'},
						{fieldLabel: 'New Name', name: 'newName', allowBlank: false},
						{fieldLabel: 'New Label', name: 'newLabel', allowBlank: false},
						{fieldLabel: 'New Description', name: 'newDescription', xtype: 'textarea', height: 70, allowBlank: false}
					]
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'InvestmentInstrumentHierarchy'
				}]
			}
		]
	}]
});
