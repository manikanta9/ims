Clifton.investment.setup.AssetClassWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Asset Class',
	iconCls: 'grouping',
	height: 550,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Asset Class',
				items: [{
					xtype: 'formpanel',
					instructions: 'An asset class defines a group of instruments and securities that exhibit similar characteristics, behave similarly in the marketplace, and are subject to the same laws and regulations.',
					url: 'investmentAssetClass.json',

					listeners: {
						afterload: function(panel) {
							this.setResetMasterFields();
						}
					},

					getWarningMessage: function(form) {
						let msg = null;
						const values = form.formValues;
						if (values && values.master) {
							msg = 'This Asset Class is a <b>Master Asset Class</b> that is used for rollup of other Asset Classes. Master Asset Classes are used by Accounting for AUM reporting and should only be changed by Accounting Admins.';
						}
						return msg;
					},

					items: [
						{fieldLabel: 'Expanded Name', name: 'nameExpanded', disabled: true},
						{
							fieldLabel: 'Parent', name: 'parent.name', hiddenName: 'parent.id', displayField: 'nameExpanded', xtype: 'combo', loadAll: false, url: 'investmentAssetClassListFind.json?ignoreNullParent=true', detailPageClass: 'Clifton.investment.setup.AssetClassWindow',
							qtip: 'The concept of "Parent" Asset class here is not really used, however we can\'t delete because it\'s included in the unique index. At this time it\'s not possible to merge those with the same name together, because Lorillard for example uses the same "asset class name" Global Equity (as Global Equity and Global Equity / Global Equity) as two separate account asset classes, which cannot reference the same asset class.'
						},
						{fieldLabel: 'Asset Class Name', name: 'name'},
						{fieldLabel: 'Short Label', name: 'shortLabel', qtip: 'If populated, will be used in Reporting instead of Asset Class Name.'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{boxLabel: 'Cash asset class holds cash or equivalent positions', name: 'cash', xtype: 'checkbox'},
						{boxLabel: 'Main Cash is used to accumulate cash balances from non-cash asset classes', name: 'mainCash', xtype: 'checkbox', requiredFields: ['cash']},
						{xtype: 'label', html: '<hr/>'},
						{
							fieldLabel: 'Master Asset Class', name: 'masterAssetClass.name', hiddenName: 'masterAssetClass.id', displayField: 'nameExpanded', xtype: 'combo', url: 'investmentAssetClassListFind.json?ignoreNullParent=true&master=true', detailPageClass: 'Clifton.investment.setup.AssetClassWindow',
							qtip: 'When asset class is not a master asset class, it must be mapped to an existing master asset class'
						},
						{
							boxLabel: 'Master Asset Class (Designated by Accounting for AUM purposes only)', name: 'master', xtype: 'checkbox',
							listeners: {
								'check': function(f) {
									const p = TCG.getParentFormPanel(f);
									p.setResetMasterFields();
								}
							}
						}
					],

					setResetMasterFields: function() {
						// If not a master, master asset class selection is required
						const f = this.getForm();
						const macf = f.findField('masterAssetClass.name');
						if (f.findField('master').getValue() === true) {
							macf.allowBlank = true;
							macf.reset();
							macf.setDisabled(true);
						}
						else {
							macf.allowBlank = false;
							macf.setDisabled(false);
						}
					}
				}]
			},


			{
				title: 'Used By',
				items: [{
					name: 'investmentAccountListFind',
					xtype: 'gridpanel',
					instructions: 'Selected asset class is used by the following investment accounts.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Account Type', width: 60, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'accountTypeId', displayField: 'name', url: 'investmentAccountTypeListFind.json'}, hidden: true},
						{header: 'Account #', dataIndex: 'number', width: 50, defaultSortColumn: true},
						{header: 'Account Name', dataIndex: 'name', width: 130},
						{header: 'Service', dataIndex: 'businessService.name', width: 130, filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true},
						{header: 'Team', width: 50, dataIndex: 'teamSecurityGroup.name', filter: {searchFieldName: 'teamSecurityGroupId', type: 'combo', url: 'securityGroupTeamList.json', loadAll: true}},
						{header: 'Base Currency', width: 60, dataIndex: 'baseCurrency.symbol', filter: {searchFieldName: 'baseCurrencyId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Workflow State', width: 60, dataIndex: 'workflowState.label', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Workflow Status', width: 70, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=InvestmentAccount'}, hidden: true},
						{header: 'Effective Start', width: 60, dataIndex: 'workflowStateEffectiveStartDate', filter: {searchFieldName: 'workflowStateEffectiveStartDate'}, hidden: true},
						{header: 'Issued By', width: 60, dataIndex: 'issuingCompany.name', hidden: true, filter: {type: 'combo', searchFieldName: 'issuingCompanyId', url: 'businessCompanyListFind.json'}}
					],
					editor: {
						detailPageClass: 'Clifton.investment.account.AccountWindow',
						drillDownOnly: true
					},
					getLoadParams: function() {
						return {
							assetClassId: this.getWindow().getMainFormId()
						};
					}
				}]
			}
		]
	}]
});

