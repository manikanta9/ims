Clifton.investment.setup.InvestmentTypeSubType2Window = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Sub Type 2',
	iconCls: 'stock-chart',
	height: 500,

	items: [{
		xtype: 'formpanel',
		instructions: 'An investment type sub type 2 is used to help further classify an investment type into smaller categories.',
		url: 'investmentTypeSubType2.json',
		labelWidth: 140,
		readOnly: true,
		items: [
			{fieldLabel: 'Investment Type', name: 'investmentType.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InvestmentTypeWindow', detailIdField: 'investmentType.id'},
			{fieldLabel: 'Sub Type 2 Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},

			{fieldLabel: 'Unadjusted Qty Name', name: 'unadjustedQuantityName', qtip: 'For securities with factor changes (ABS and CDS) it is the Original Face/Original Notional (Opening Quantity; not adjusted by Current Factor). For other securities it is the same as Remaining Quantity or Quantity (current number of units). These values override Investment Type and Investment Sub Type settings.'},
			{fieldLabel: 'Quantity Name', name: 'quantityName', qtip: 'Industry standard name for "Quantity" field (remaining quantity). These values override Investment Type and Investment Sub Type settings.'},

			{fieldLabel: 'Cost Field Name', name: 'costFieldName', qtip: 'Optional override for Investment Type and Sub Type field name'},
			{fieldLabel: 'Cost Basis Field Name', name: 'costBasisFieldName', qtip: 'Optional override for Investment Type and Sub Type field name'},
			{fieldLabel: 'Notional Field Name', name: 'notionalFieldName', qtip: 'Optional override for Investment Type and Sub Type field name'},
			{fieldLabel: 'Market Value Field Name', name: 'marketValueFieldName', qtip: 'Optional override for Investment Type and Sub Type field name'},

			{fieldLabel: 'Quantity Precision', name: 'quantityDecimalPrecision', qtip: 'The decimal precision for the quantity on trades and positions.  This value overrides Investment Type and Investment Sub Type settings.'}
		]
	}]
});
