Clifton.investment.setup.InvestmentTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Type',
	iconCls: 'stock-chart',
	width: 800,
	height: 525,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Investment Types define the types of investments instruments. Each security is associated with corresponding instrument, instrument with hierarchy and hierarchy with investment type. Each level may define attributes/behavior that should apply to security. More specific configuration can be used to override higher level settings.',
					url: 'investmentType.json',
					labelWidth: 140,
					readOnly: true,
					items: [
						{fieldLabel: 'Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},

						{fieldLabel: '', boxLabel: 'Physical Securities', xtype: 'checkbox', name: 'physicalSecurity', qtip: 'Physical Securities are non-derivative securities that are paid with cash at open/close'},

						{fieldLabel: 'Unadjusted Qty Name', name: 'unadjustedQuantityName', qtip: 'For securities with factor changes (ABS and CDS) it is the Original Face/Original Notional (Opening Quantity; not adjusted by Current Factor). For other securities it is the same as Remaining Quantity or Quantity (current number of units). These values can be overridden at Investment Sub Type and Investment Sub Type 2.'},
						{fieldLabel: 'Quantity Name', name: 'quantityName', qtip: 'Industry standard name for "Quantity" field (remaining quantity). These values can be overridden at Investment Sub Type and Investment Sub Type 2.'},
						{fieldLabel: 'Quantity Precision', name: 'quantityDecimalPrecision', qtip: 'The decimal precision for the quantity on trades and positions.  This value can be overridden at Investment Sub Type or Investment Sub Type 2.'},

						{fieldLabel: 'Cost Field Name', name: 'costFieldName', qtip: 'This name can be overridden at Sub Type or Sub Type 2'},
						{fieldLabel: 'Cost Basis Field Name', name: 'costBasisFieldName', qtip: 'This name can be overridden at Sub Type or Sub Type 2'},
						{fieldLabel: 'Notional Field Name', name: 'notionalFieldName', qtip: 'This name can be overridden at Sub Type or Sub Type 2'},
						{fieldLabel: 'Market Value Field Name', name: 'marketValueFieldName', qtip: 'This name can be overridden at Sub Type or Sub Type 2'},

						{fieldLabel: 'Days to Settle', name: 'daysToSettle', xtype: 'integerfield', qtip: 'Specifies the number of days it may take to settle a security of this Type. This value can be overridden at Investment Hierarchy, Investment Instrument or Security level (custom fields).'}
					]
				}]
			},


			{
				title: 'Sub Types',
				items: [{
					name: 'investmentTypeSubTypeListByType',
					xtype: 'gridpanel',
					instructions: 'Investment sub-types are optional. Not every investment type has sub-types.  If investment sub-types are available for an investment type, then investment sub-type can be selected at InvestmentInstrumentHierarchy.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Sub Type Name', dataIndex: 'name', width: 60},
						{header: 'Description', dataIndex: 'description', width: 200, hidden: true},

						{header: 'Unadjusted Qty Name', width: 45, dataIndex: 'unadjustedQuantityName', tooltip: 'For securities with factor changes (ABS and CDS) it is the Original Face/Original Notional (Opening Quantity; not adjusted by Current Factor). For other securities it is the same as Remaining Quantity or Quantity (current number of units). These values can be overridden at Investment Sub Type and Investment Sub Type 2.'},
						{header: 'Quantity Name', width: 30, dataIndex: 'quantityName', tooltip: 'Industry standard name for "Quantity" field (remaining quantity). These values can be overridden at Investment Sub Type and Investment Sub Type 2.'},

						{header: 'Cost Field Name', width: 45, dataIndex: 'costFieldName', tooltip: 'Optional override for Investment Type field name'},
						{header: 'Cost Basis Field Name', width: 45, dataIndex: 'costBasisFieldName', tooltip: 'Optional override for Investment Type field name'},
						{header: 'Notional Field Name', width: 45, dataIndex: 'notionalFieldName', tooltip: 'Optional override for Investment Type field name'},
						{header: 'Market Value Field Name', width: 45, dataIndex: 'marketValueFieldName', tooltip: 'Optional override for Investment Type field name'},

						{header: 'Quantity Precision', width: 40, dataIndex: 'quantityDecimalPrecision', type: 'int', useNull: true, tooltip: 'The decimal precision for the quantity on trades and positions.  This value can be overridden at Investment Sub Type or Investment Sub Type 2.'}
					],
					getLoadParams: function() {
						return {
							investmentTypeId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.investment.setup.InvestmentTypeSubTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Sub Types 2',
				items: [{
					name: 'investmentTypeSubType2ListByType',
					xtype: 'gridpanel',
					instructions: 'Investment sub-types 2 are optional. Not every investment type has sub-types.  If investment sub-types are available for an investment type, then investment sub-type can be selected at InvestmentInstrumentHierarchy.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Sub Type 2 Name', dataIndex: 'name', width: 60},
						{header: 'Description', dataIndex: 'description', width: 200, hidden: true},

						{header: 'Unadjusted Qty Name', width: 45, dataIndex: 'unadjustedQuantityName', tooltip: 'For securities with factor changes (ABS and CDS) it is the Original Face/Original Notional (Opening Quantity; not adjusted by Current Factor). For other securities it is the same as Remaining Quantity or Quantity (current number of units). These values can be overridden at Investment Sub Type and Investment Sub Type 2.'},
						{header: 'Quantity Name', width: 30, dataIndex: 'quantityName', tooltip: 'Industry standard name for "Quantity" field (remaining quantity). These values can be overridden at Investment Sub Type and Investment Sub Type 2.'},

						{header: 'Cost Field Name', width: 45, dataIndex: 'costFieldName', tooltip: 'Optional override for Investment Type and Sub Type field name'},
						{header: 'Cost Basis Field Name', width: 45, dataIndex: 'costBasisFieldName', tooltip: 'Optional override for Investment Type and Sub Type field name'},
						{header: 'Notional Field Name', width: 45, dataIndex: 'notionalFieldName', tooltip: 'Optional override for Investment Type and Sub Type field name'},
						{header: 'Market Value Field Name', width: 45, dataIndex: 'marketValueFieldName', tooltip: 'Optional override for Investment Type and Sub Type field name'},

						{header: 'Quantity Precision', width: 40, dataIndex: 'quantityDecimalPrecision', type: 'int', useNull: true, tooltip: 'The decimal precision for the quantity on trades and positions.  This value can be overridden at Investment Sub Type or Investment Sub Type 2.'}
					],
					getLoadParams: function() {
						return {
							investmentTypeId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.investment.setup.InvestmentTypeSubType2Window',
						drillDownOnly: true
					}

				}]
			}
		]
	}]
});

