Clifton.investment.calendar.EventWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'investmentEvent.json',
	className: 'Clifton.investment.calendar.CalendarEventWindow',

	getClassName: function(config, entity) {
		// config.defaultData.eventType ...
		if (entity && entity.seriesDefinition && entity.seriesDefinition.recurrenceSchedule) {
			const eventSelector = this;
			TCG.createComponent('TCG.app.OKCancelWindow', {
				title: 'Open Recurring Event',
				iconCls: 'calendar',
				height: 200,
				width: 500,
				modal: true,

				items: [{
					xtype: 'formpanel',
					loadValidation: false,
					instructions: '"' + entity.label + '" is a recurring event.<br />Do you want to open only this occurrence or the series?',
					items: [
						{
							xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'eventType',
							items: [
								{boxLabel: 'Open this occurrence.', xtype: 'radio', name: 'eventType', inputValue: 'OCCURRENCE', checked: true},
								{boxLabel: 'Open the series.', xtype: 'radio', name: 'eventType', inputValue: 'SERIES'}
							]
						}
					]
				}],
				saveWindow: function(closeOnSuccess, forceSubmit) {
					const fp = this.getMainFormPanel();
					const et = fp.getForm().findField('eventType').getValue().inputValue;
					if (et === 'SERIES') {
						config.params.id = entity.seriesDefinition.id;
						Clifton.investment.calendar.EventWindow.superclass.constructor.apply(eventSelector, [config]);
					}
					else {
						eventSelector.doOpenEntityWindow(config, entity, eventSelector.className);
					}
					this.closeWindow();
				},
				openerCt: this.openerCt
			});
			return false;
		}
		return this.className;
	}
});


Clifton.investment.calendar.CalendarEventWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Calendar Event',
	iconCls: 'calendar',
	width: 1000,
	height: 650,
	enableRefreshWindow: true,

	windowOnShow: function(w) {
		if (TCG.getValue('parentSeriesDefinition', w.defaultData) === true) {
			const tabs = w.items.get(0);
			tabs.insert(2, {
				title: 'Series Events',
				items: [{
					xtype: 'investment-event-list-gridpanel',
					instructions: 'A list of Calendar Events in the selected Series.',
					reloadOnRender: true,
					disableInitialLoadDateFilter: true,
					useDisplayFilter: false,
					groupField: undefined,
					columnOverrides: [
						{dataIndex: 'eventType.name', hidden: true},
						{dataIndex: 'coalesceAssigneeGroupInvestmentAccountName', hidden: true},
						{dataIndex: 'eventDate', defaultSortColumn: true, defaultSortDirection: 'DESC'}
					],
					getTopToolbarFilters: function(toolbar) {
						return undefined;
					},
					getLoadParams: function() {
						return {
							seriesDefinitionId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.investment.calendar.CalendarEventWindow',
						drillDownOnly: true
					}
				}]
			});
		}
	},

	gridTabWithCount: 'Notes', // show notes count in  the tab

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'InvestmentEvent',
					finalState: 'Deleted',
					groupedInvoice: false,
					optionalWorkflow: true,
					reloadFormPanelAfterTransition: true,
					listeners: {
						afterWorkflowToolbarRefresh: function(toolbar) {
							toolbar.getFormPanel().displayButtons();
						}
					},
					addAdditionalButtons: function(t) {
						const recurrenceButton = t.items.find(i => i.name === 'recurrence');
						if (TCG.isNull(recurrenceButton)) {
							t.insert(0, {
								text: 'Recurrence',
								name: 'recurrence',
								xtype: 'button',
								iconCls: 'recurrence',
								tooltip: 'Make this calendar entry recurring.',
								handler: async function() {
									const f = this.ownerCt.ownerCt.items.get(0);
									const w = f.getWindow();
									if (w.isModified()) {
										TCG.showError('You must save the event before changing Recurrence schedule.', 'Save First');
									}
									else {
										const id = f.getFormValue('recurrenceSchedule.id');
										const customDetailClass = 'Clifton.calendar.schedule.ScheduleWithoutTimeWindow';
										const scheduleTypeName = 'Investment Calendar Event Schedules';
										const scheduleType = await TCG.data.getDataPromiseUsingCaching(`calendarScheduleTypeByName.json?name=${scheduleTypeName}`, this, `calendarScheduleType.${scheduleTypeName}`);

										const newComponentArgs = {
											id: 'InvestmentEventSchedule',
											modal: true,
											openerCt: f,

											defaultData: {
												systemDefined: true,
												name: 'InvestmentEvent-' + w.getMainFormId(),
												description: 'Auto-generated schedule for Investment Event with id = ' + w.getMainFormId(),
												startDate: f.getFormValue('eventDate'),
												calendarScheduleType: scheduleType
											},
											params: TCG.isNull(id) ? undefined : {id: id},
											fieldOverrides: [
												{name: 'calendarScheduleType.name', disabled: true}
											]
										};
										const currentState = f.getFormValue('workflowStatus.name');
										if (f.getFormValue('eventType.workflow') && (TCG.isNotBlank(currentState) && currentState !== f.getFormValue('eventType.editableWorkflowStatus.name'))) {
											newComponentArgs.listeners = {
												afterrender: function(win) {
													win.getMainFormPanel().setReadOnly(true);
												}
											};
										}
										TCG.createComponent(customDetailClass, newComponentArgs);
									}
								}
							}, '-');
						}
						else {
							recurrenceButton.setVisible(true);
						}
						const editSeriesButton = t.items.find(i => i.name === 'editseries');
						if (TCG.isNull(editSeriesButton)) {
							t.insert(0, {
								text: 'Edit Series',
								name: 'editseries',
								xtype: 'button',
								iconCls: 'recurrence',
								tooltip: 'This event is a part of a recurring series. Edit the series.',
								hidden: true,
								handler: function() {
									const f = this.ownerCt.ownerCt.items.get(0);
									const w = f.getWindow();
									if (w.isModified()) {
										TCG.showError('You must save the series before changing Recurrence schedule.', 'Save First');
									}
									else {
										const id = f.getFormValue('seriesDefinition.id');
										const className = 'Clifton.investment.calendar.EventWindow';
										const cmpId = TCG.getComponentId(className, id);
										w.closeWindow();
										TCG.createComponent(className, {
											id: cmpId,
											openerCt: w.openerCt,
											params: {id: id}
										});
									}
								}
							});
						}
					}
				},
				items: [{
					xtype: 'formpanel',
					url: 'investmentEvent.json',
					childTables: 'InvestmentEventManagerAdjustment',
					loadDefaultDataAfterRender: true, // set eventType CSS
					items: [
						{name: 'recurrenceSchedule.id', xtype: 'hidden'},
						{fieldLabel: 'Event Type', name: 'eventType.name', xtype: 'linkfield', height: 23, detailPageClass: 'Clifton.investment.calendar.EventTypeWindow', detailIdField: 'eventType.id'},
						{xtype: 'label', html: '&nbsp;'},
						{xtype: 'label', html: '&nbsp;'},
						{
							fieldLabel: 'Event Date', xtype: 'panel', layout: 'hbox',
							items: [
								{name: 'eventDate', xtype: 'datefield', width: 95},
								{xtype: 'displayfield', width: 20},
								{value: 'Amount:', xtype: 'displayfield', width: 65, name: 'amountLabel'},
								{name: 'amount', xtype: 'currencyfield', width: 110},
								{xtype: 'displayfield', flex: 1},
								{value: 'Completed:', xtype: 'displayfield', width: 80, name: 'completedLabel'},
								{
									name: 'completedEvent', xtype: 'checkbox', width: 20,
									listeners: {
										check: function(obj, checked) {
											if (!checked) {
												const fp = TCG.getParentFormPanel(obj);
												const r = fp.getForm().findField('resolution');
												r.clearValue();
											}
										}
									}
								},
								{name: 'resolutionPadding', xtype: 'displayfield', width: 20},
								{name: 'resolutionLabel', value: 'Resolution:', xtype: 'displayfield', width: 75},
								{
									name: 'resolution', xtype: 'combo', width: 140, url: 'systemListItemListFind.json', displayField: 'text', valueField: 'value', tooltipField: 'tooltip', emptyText: '',
									listeners: {
										beforequery: function(queryEvent) {
											const fp = TCG.getParentFormPanel(queryEvent.combo);
											const name = fp.getFormValue('eventType.resolutionSystemList.name');
											if (name) {
												queryEvent.combo.store.setBaseParam('listName', name);
											}
											else {
												TCG.showError('Events of selected type do not support resolution.', 'Resolution Not Supported');
												return false;
											}
										},
										beforeselect: function(combo, record) {
											const fp = TCG.getParentFormPanel(combo);
											const c = fp.getForm().findField('completedEvent');
											if (!c.getValue()) {
												c.setValue(true);
											}
										}
									}
								}
							]
						},
						{
							fieldLabel: 'Client Account', name: 'investmentAccount.label', hiddenName: 'investmentAccount.id', xtype: 'combo', limitRequestedProperties: false, url: 'investmentAccountListFind.json?ourAccount=true&workflowStateNames=Active&workflowStateNames=Open&workflowStateNames=Pending Approval', displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow', disableAddNewItem: true,
							listeners: {
								beforeselect: function(combo, record) {
									// formgrid needs deep account properties
									const f = combo.getParentForm().getForm();
									f.formValues['investmentAccount'] = record.json;
								}
							}
						},
						{
							fieldLabel: 'Team', name: 'assigneeSecurityGroup.label', hiddenName: 'assigneeSecurityGroup.id', xtype: 'combo', loadAll: true, url: 'securityGroupTeamList.json', displayField: 'label', detailPageClass: 'Clifton.investment.instrument.SecurityGroupWindow', disableAddNewItem: true,
							listeners: {
								beforeselect: function(combo, record) {
									// formgrid needs deep account properties
									const f = combo.getParentForm().getForm();
									f.formValues['assigneeSecurityGroup'] = record.json;
								}
							}
						},
						{fieldLabel: 'Summary', name: 'summary'},
						{fieldLabel: 'Details', name: 'details', xtype: 'textarea', height: 70}
					],
					prepareDefaultData: function(dd) { // also apply style for new events using default type data
						const eventType = TCG.getValue('eventType', dd);
						if (!eventType.eventAmountAllowed) {
							this.hideField('amountLabel');
							this.hideField('amount');
						}
						if (!eventType.eventCompletionAllowed) {
							this.hideField('completedLabel');
							this.hideField('completedEvent');
						}
						if (!(eventType.eventScope === 'INVESTMENT_ACCOUNT' || eventType.eventScope === 'INVESTMENT_ACCOUNT_REQUIRED')) {
							this.hideField('investmentAccount.label');
						}
						if (eventType.eventScope !== 'TEAM_REQUIRED') {
							this.hideField('assigneeSecurityGroup.label');
						}
						if (eventType.managerBalanceAdjustment) {
							const managerBalanceGrid = {
								xtype: 'formgrid',
								name: 'managerAdjustmentListGrid',
								title: 'Manager Account Balance Adjustments',
								storeRoot: 'managerAdjustmentList',
								dtoClass: 'com.clifton.investment.calendar.InvestmentEventManagerAdjustment',
								hidden: !'eventType.managerBalanceAdjustment',
								columnsConfig: [
									{header: 'ID', width: 60, dataIndex: 'id', hidden: true},
									{
										header: 'Manager Account', width: 200, dataIndex: 'managerAccount.labelCustodian', idDataIndex: 'managerAccount.id',
										editor: {
											xtype: 'combo', url: 'investmentManagerAccountListFind.json?inactive=false', displayField: 'labelCustodian', detailPageClass: 'Clifton.investment.manager.AccountWindow', disableAddNewItem: true,
											beforequery: function(queryEvent) {
												const grid = queryEvent.combo.gridEditor.containerGrid;
												const f = TCG.getParentFormPanel(grid);
												const clientId = f.getFormValue('investmentAccount.businessClient.id');
												if (clientId) {
													queryEvent.combo.store.setBaseParam('clientId', clientId);
													queryEvent.combo.store.setBaseParam('assignmentInactive', false);
													queryEvent.combo.store.setBaseParam('assignmentAccountId', f.getFormValue('investmentAccount.id'));
												}
												else {
													TCG.showError('Client Account must be selected before entering adjustments.', 'Missing Client Account');
													return false;
												}
											}
										}
									},
									{
										header: 'Adjustment Type', width: 130, dataIndex: 'adjustmentType.label', idDataIndex: 'adjustmentType.id',
										editor: {
											xtype: 'combo', url: 'investmentManagerAccountBalanceAdjustmentTypeListFind.json', displayField: 'label', allowBlank: false,
											beforequery: function(queryEvent) {
												const grid = queryEvent.combo.gridEditor.containerGrid;
												const f = TCG.getParentFormPanel(grid);
												queryEvent.combo.store.setBaseParam('marketOnClose', f.getFormValue('eventType.marketOnClose'));
												queryEvent.combo.store.setBaseParam('systemDefined', false);
											}
										}
									},
									{header: 'Note', dataIndex: 'note', width: 290, editor: {xtype: 'textfield'}},
									{header: 'Ignore', dataIndex: 'ignored', width: 50, type: 'boolean', editor: {xtype: 'checkbox'}, tooltip: 'Do NOT apply this adjustment to manager balance'},
									{header: 'Cash', dataIndex: 'cashValue', width: 110, type: 'currency', editor: {xtype: 'currencyfield'}, summaryType: 'sum'},
									{header: 'Securities', dataIndex: 'securitiesValue', width: 110, type: 'currency', editor: {xtype: 'currencyfield'}, summaryType: 'sum'}
								],
								plugins: {ptype: 'gridsummary'}
							};
							this.add(managerBalanceGrid);
						}
						if (eventType.fileAttachmentAllowed) {
							this.add({
								title: 'File Attachments',
								xtype: 'documentFileGrid-simple',
								definitionName: 'InvestmentEventAttachments',
								collapsed: false, // need this to fix layout bug that skips tbar
								isPagingEnabled: function() {
									return false;
								},
								// Show document files for this event and the series definition event
								getEntityIds: function() {
									const seriesId = TCG.getValue('seriesDefinition.id', this.getWindow().getMainForm().formValues);
									if (seriesId) {
										return [this.getEntityId(), seriesId];
									}
									return undefined;
								}
							});
						}
						this.applyEventTypeUI(eventType);
						return dd;
					},
					displayButtons: function() {
						const f = this.getForm();
						const tb = this.ownerCt.getTopToolbar();
						const recurrenceButton = TCG.getChildByName(tb, 'recurrence');
						if (recurrenceButton) {
							const currentState = this.getFormValue('workflowStatus.name');
							const disableRecurrence = this.getFormValue('eventType.workflow') && (TCG.isNotBlank(currentState) && currentState !== this.getFormValue('eventType.editableWorkflowStatus.name'));
							if (this.getFormValue('eventType.systemDefined')) { // system defined events cannot have recurrence
								recurrenceButton.setDisabled(true);
							}
							else if (this.getFormValue('seriesDefinition')) { // event from a series
								recurrenceButton.hide();
								const editSeriesButton = TCG.getChildByName(tb, 'editseries');
								if (editSeriesButton) {
									editSeriesButton.show();
									editSeriesButton.getEl().setStyle('background', '#ffcc66');
									editSeriesButton.getEl().setStyle('border', '#C0C0C0 1px solid');
								}
							}
							else if (this.getFormValue('recurrenceSchedule.id')) { // series definition
								recurrenceButton.getEl().setStyle('background', '#ffcc66');
								recurrenceButton.getEl().setStyle('border', '#C0C0C0 1px solid');
								recurrenceButton.setTooltip('Modify existing Recurrence schedule.');
								f.findField('eventDate').setDisabled(true);
								f.findField('completedEvent').setDisabled(true);
							}
							else {
								recurrenceButton.setDisabled(disableRecurrence);
							}
						}
					},
					listeners: {
						afterload: function(fp) {
							fp.displayButtons();
							fp.applyEventTypeUI.call(fp, fp.getFormValue('eventType'));
							if (this.getFormValue('workflowStatus.name') !== this.getFormValue('eventType.editableWorkflowStatus.name')) {
								fp.setReadOnlyFieldsForApprovedStatus(true);
							}
							else {
								fp.setReadOnlyFieldsForApprovedStatus(false);
							}
						}
					},
					applyEventTypeUI: function(eventType) {
						const type = eventType || {};
						const s = type.cssStyle;
						if (TCG.isNotBlank(s)) {
							const et = this.getForm().findField('eventType.name');
							const el = et.getEl();
							if (el) {
								el.applyStyles(s);
							}
						}
						// hide sections not applicable to selected event type
						if (!type.managerBalanceAdjustment) {
							this.hideField('managerAdjustmentListGrid');
						}
						if (type.eventScope !== 'INVESTMENT_ACCOUNT' && type.eventScope !== 'INVESTMENT_ACCOUNT_REQUIRED') {
							this.hideField('investmentAccount.label');
						}
						if (type.eventScope !== 'TEAM_REQUIRED') {
							this.hideField('assigneeSecurityGroup.label');
						}
						if (!type.resolutionSystemList) {
							this.hideField('resolutionPadding');
							this.hideField('resolutionLabel');
							this.hideField('resolution');
						}
						if (this.layout.layout) {
							this.doLayout();
						} // strange case when the method may not be available in some cases/browsers
					},
					getWarningMessage: function(f) {
						let message = '';
						if (TCG.getValue('completedEvent', f.formValues)) {
							const resolution = TCG.getValue('resolution', f.formValues);
							this.warningMessageCls = 'warning-msg';
							message = TCG.getValue('eventType.name', f.formValues) + ' calendar event was completed' + (resolution ? ' with the following resolution: <b>' + resolution + '</b>' : '');
						}
						else if (TCG.isTrue(TCG.getValue('deletedEvent', f.formValues))) {
							message = 'This recurring event was DELETED. NOTE: the record of it was preserved in the system so that it is not recreated again by the scheduler.';
						}
						else if (TCG.getValue('workflowState.name', f.formValues) !== null && TCG.getValue('workflowState.name', f.formValues) !== 'Approved' && !this.getFormValue('seriesDefinition') && this.getFormValue('recurrenceSchedule')) {
							message = 'Warning: Event recurrences will be regenerated on approval.';
						}

						const currentState = TCG.getValue('workflowStatus.name', f.formValues);
						if (TCG.isNotBlank(currentState) && currentState !== TCG.getValue('eventType.editableWorkflowStatus.name', f.formValues)) {
							const approvedEditableFields = TCG.getValue('eventType.approvedStateEditableFields', f.formValues);
							const approvedState = TCG.getValue('eventType.approvedWorkflowState.name');

							if (TCG.getValue('eventType.managerBalanceAdjustment', f.formValues)) {
								if (TCG.getValue('workflowState.name', f.formValues) !== approvedState ||
									TCG.isNull(approvedEditableFields) ||
									(TCG.isNotNull(approvedEditableFields) && !approvedEditableFields.includes('managerAdjustmentList'))) {

									message += ((message === '' ? '' : '<br/>') + 'Warning: Because this event is not in an editable Workflow State, modifications to Manager Balance Adjustment will not be saved.');
								}
							}
						}

						if (message.length > 0) {
							return message;
						}
						return undefined;
					},
					// Give Save Confirm if Event Date is before today
					confirmBeforeSaveMsgTitle: 'Create/Update Historical Event?',
					getConfirmBeforeSaveMsg: function() {
						const eDate = this.getForm().findField('eventDate').getValue();
						if (TCG.isNotBlank(eDate)) {
							const today = new Date((new Date()).format('m/d/Y'));
							if (eDate.getTime() < today.getTime()) {
								return 'This event occurred in the past.  Are you sure you want to create/update this event?';
							}
						}
						return undefined;
					},
					reload: async function(closedWindow) {
						const w = this.getWindow();
						if (closedWindow && closedWindow.id === 'InvestmentEventSchedule') {
							const newId = closedWindow.getMainFormId();
							// always reload on schedule change to rebuild events in the series
							this.setFormValue('recurrenceSchedule.id', newId);
							this.setFormValue('eventDate', '');
							w.saveWindow(false, true);
							return;
						}
						const fp = this;
						const event = await TCG.data.getDataPromise('investmentEvent.json?id=' + w.getMainFormId(), this, 'investment.event.' + w.getMainFormId());
						if (event) {
							fp.getForm().setValues(event, true);
							fp.fireEvent('afterload', fp);
						}
					},
					setReadOnlyFieldsForApprovedStatus: function(readOnly) {
						const approvedEditableFields = TCG.isNotNull(this.getFormValue('eventType.approvedStateEditableFields')) && this.getFormValue('workflowState.name') === this.getFormValue('eventType.approvedWorkflowState.name') ? this.getFormValue('eventType.approvedStateEditableFields').split(',') : [];
						this.setReadOnly(readOnly, approvedEditableFields);
						const managerAdjustmentGrid = TCG.getChildByName(this, 'managerAdjustmentListGrid');
						if (managerAdjustmentGrid) {
							managerAdjustmentGrid.setReadOnly(readOnly);
						}
					}
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'system-note-grid',
					tableName: 'InvestmentEvent'
				}]
			},


			{
				title: 'Event Life Cycle',
				items: [{
					xtype: 'system-lifecycle-grid',
					tableName: 'InvestmentEvent'
				}]
			},


			{
				title: 'Audit Trail',
				items: [{
					xtype: 'system-audit-grid',
					tableName: 'InvestmentEvent',
					childTables: 'InvestmentEventManagerAdjustment'
				}]
			}
		]
	}]
});
