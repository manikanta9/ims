// Break this grid panel into it's own component for reusability
Clifton.investment.calendar.EventListGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentEventListFind',
	importTableName: 'InvestmentEvent',
	pageSize: 200,
	groupField: 'eventDate',
	groupTextTpl: '<div class="x-tab-panel-header" style="BORDER-RADIUS: 5px; PADDING: 3px 3px 3px 7px; MARGIN-BOTTOM: 5px; MARGIN-TOP: 2px;{[values.rs[0].data["eventDate"] == (new Date().clearTime()+"") ? "COLOR: #EE0000; BORDER-COLOR: #EE0000" : ""]}">{[values.rs[0].data.labelForGrouping]} ({[values.rs.length]} {[values.rs.length > 1 ? "Events" : "Event"]})</div>',
	additionalPropertiesToRequest: 'id|eventType.cssStyle|seriesDefinition.id|eventScope',
	instructions: 'A list of investment Calendar Events grouped by Event Date that match selected filters.  Each event (based on its type) is associated with the corresponding Client Account, Team or is global.',
	wikiPage: 'IT/Investment+Calendar',
	workflowStatus: 'All',
	reloadOnRender: false,
	excludeSeriesDefinition: false,
	disableInitialLoadDateFilter: false,
	useDisplayFilter: true,

	columns: [
		{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
		{
			header: 'Event Type', width: 60, dataIndex: 'eventType.name', filter: {type: 'combo', searchFieldName: 'eventTypeId', url: 'investmentEventTypeListFind.json'},
			renderer: function(v, c, r) {
				const style = r.json.eventType.cssStyle;
				if (TCG.isNotBlank(style)) {
					c.attr = 'style="' + style + '"';
				}
				return v;
			}
		},
		{
			header: 'Client Account / Team', width: 100, dataIndex: 'coalesceAssigneeGroupInvestmentAccountName',
			filter: {
				searchFieldName: 'eventScope',
				convert: function(value, record) {
					return record.json.eventScope;
				}
			}
		},
		{header: 'Client Account', width: 100, dataIndex: 'investmentAccount.label', filter: {type: 'combo', searchFieldName: 'investmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
		{header: 'Team', dataIndex: 'coalesceTeam.name', hiddenName: 'coalesceTeam.id', width: 50, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', orNull: true, url: 'securityGroupTeamList.json', loadAll: true}, hidden: true},
		{
			header: 'Summary', width: 150, dataIndex: 'label', filter: {searchFieldName: 'summary'},
			renderer: function(v, c, r) {
				if (TCG.isNotNull(r.data.details)) {
					let text = r.data.details;
					if (text.length > 2000) {
						text = 'Double click to see details';
					}
					return '<div qtip=\'' + Ext.util.Format.htmlEncode(text) + '\'>' + v + '</div>';
				}
				return v;
			}
		},
		{header: 'Workflow Status', width: 50, dataIndex: 'workflowStatus.name', idDataIndex: 'workflowStatus.id', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=InvestmentEvent'}, hidden: true},
		{header: 'Workflow State', width: 35, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
		{header: 'Details', width: 200, dataIndex: 'details', hidden: true},
		{header: 'Amount', width: 30, dataIndex: 'amount', type: 'currency', useNull: true, positiveInGreen: true, negativeInRed: true},
		{header: 'Grouping Label', width: 50, dataIndex: 'labelForGrouping', hidden: true},
		{header: 'Date', width: 25, dataIndex: 'eventDate', filter: {orEquals: true}},
		{header: 'MOC', width: 15, dataIndex: 'eventType.marketOnClose', filter: {searchFieldName: 'marketOnClose'}, type: 'boolean', tooltip: 'Market On Close'},
		{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'File Attachment(s)', width: 8, tooltip: 'File Attachment(s)', dataIndex: 'fullDocumentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'},
		{
			header: '<div class="recurrence" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'Recurring Event', width: 8, tooltip: 'Recurring Event', dataIndex: 'seriesDefinitionPresent', type: 'boolean',
			renderer: function(v) {
				if (TCG.isTrue(v)) {
					return TCG.renderActionColumn('recurrence', '', 'Recurring Calendar Event');
				}
				return '';
			}
		},
		{
			header: '<div class="checked" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'Completed', width: 8, tooltip: 'Completed Event', dataIndex: 'completedEvent', type: 'boolean',
			renderer: function(v, c, r) {
				return TCG.renderBoolean(v, r.data.resolution);
			}
		},
		{header: 'Resolution', width: 50, dataIndex: 'resolution', hidden: true}
	],
	getLoadParams: async function(firstLoad) {
		const t = this.getTopToolbar();
		const tag = TCG.getChildByName(t, 'calendarTagHierarchyId');
		const tagName = this.getWindow().defaultTagName;
		if (firstLoad) {
			// try to get trader's team: only if one
			const team = await Clifton.security.user.getUserTeam(this);

			if (tagName && TCG.isNotNull(tag)) {
				const tagValue = await TCG.data.getDataPromiseUsingCaching('systemHierarchyByCategoryAndNameExpanded.json?categoryName=Investment Calendar Tags&name=' + tagName, this, 'system.hierarchy.Investment Calendar Tags.' + tagName);
				if (tagValue) {
					tag.setValue({value: tagValue.id, text: tagValue.name});
				}
			}
			if (TCG.isNotNull(team)) {
				this.setFilterValue('coalesceTeam.name', {value: team.id, text: team.name});
			}
			this.setFilterValue('eventDate', {
				'after': Clifton.calendar.getBusinessDayFrom(-1),
				'before': new Date().add(Date.DAY, 30)
			});
			if (TCG.isTrue(this.disableInitialLoadDateFilter)) {
				this.clearFilter('eventDate', true);
			}
			if (this.workflowStatus !== 'All') {
				const status = await TCG.data.getDataPromiseUsingCaching('workflowStatusByName.json?name=' + this.workflowStatus, this, 'workflow.status.' + this.workflowStatus);
				this.setFilterValue('workflowStatus.name', {text: status.name, value: status.id});
			}
		}
		const params = {
			deletedEvent: false,
			nonRecurringEvent: TCG.isTrue(this.excludeSeriesDefinition) ? true : void 0, // load all if not excluding series definition
			readUncommittedRequested: true
		};
		const n = TCG.getChildByName(t, 'investmentAccountGroupId');

		if (n) {
			const v = n.getValue();
			if (v) {
				params.investmentAccountGroupId = v;
			}
		}


		// The tag-related params must be set in either of these two cases for accurate counts to be displayed on the "Draft" and "Pending" tabs.
		//  The first condition will only be true if the gridpanel's toolbar has been rendered already (i.e. tag !== null), but the tag param needs to be applied even if the
		//  toolbar has not yet been rendered, so that the count displayed reflects the tag value.
		//  However, we only want to use the defaultTagName on first load, because otherwise it will not be accurate if the user changes the tag value, and after first load the toolbar
		//  has already been rendered.
		if ((tag && TCG.isNotBlank(tag.getValue())) || (firstLoad && this.getWindow().defaultTagName)) {
			params.categoryName = 'Investment Calendar Tags';
			params.categoryTableName = 'InvestmentEventType';
			params.categoryLinkFieldPath = 'eventType';

			if (tag) {
				params.categoryHierarchyId = tag.getValue();
			}
			else {
				const tagValue = await TCG.data.getDataPromiseUsingCaching('systemHierarchyByCategoryAndNameExpanded.json?categoryName=Investment Calendar Tags&name=' + tagName, this, 'system.hierarchy.Investment Calendar Tags.' + tagName);
				params.categoryHierarchyId = tagValue.id;
			}
		}
		return params;
	},

	getTopToolbarFilters: function(toolbar) {
		const items = [
			{fieldLabel: 'Tag', width: 100, xtype: 'toolbar-combo', name: 'calendarTagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Investment Calendar Tags'},
			{fieldLabel: 'Client Account Group', xtype: 'toolbar-combo', name: 'investmentAccountGroupId', width: 180, url: 'investmentAccountGroupListFind.json'},
			{fieldLabel: 'Team', name: 'teamSecurityGroupId', width: 120, minListWidth: 120, xtype: 'toolbar-combo', url: 'securityGroupTeamList.json', loadAll: true, linkedFilter: 'coalesceTeam.name'}
		];

		if (toolbar.ownerCt.useDisplayFilter) {
			items.push({
				fieldLabel: 'Display', xtype: 'combo', name: 'displayType', width: 100, minListWidth: 120, mode: 'local', emptyText: '-1 to +30 days', displayField: 'name', valueField: 'value',
				store: new Ext.data.ArrayStore({
					fields: ['value', 'name', 'description'],
					data: [
						['DEFAULT', '-1 to +30 days', 'From previous business day to 30 days from now'],
						['TODAY', 'Today', 'Display events for today'],
						['WEEK', 'Current Week', 'Display events for current week'],
						['MONTH', 'Current Month', 'Display events for current month']
					]
				}),
				listeners: {
					select: function(field) {
						const v = field.getValue();
						const now = new Date();
						let from, to;
						if (v === 'TODAY') {
							from = now;
							to = from;
						}
						else if (v === 'WEEK') {
							from = now.add(Date.DAY, 1 - now.getDay());
							to = from.add(Date.DAY, 6);
						}
						else if (v === 'MONTH') {
							from = TCG.parseDate(now.dateFormat('m/01/Y'), 'm/d/Y');
							to = from.add(Date.MONTH, 1).add(Date.DAY, -1);
						}
						else {
							from = Clifton.calendar.getBusinessDayFromDate(now, -1);
							to = now.add(Date.DAY, 30);
						}
						const gp = TCG.getParentByClass(field, Ext.Panel);
						gp.setFilterValue('eventDate', {
							'after': from,
							'before': to
						});
						gp.reload();
					}
				}
			});
		}

		return items;
	},

	editor: {
		detailPageClass: 'Clifton.investment.calendar.EventWindow',
		getDefaultData: function(gridPanel) {
			return this.savedDefaultData;
		},
		addToolbarAddButton: function(toolBar) {
			const gridPanel = this.getGridPanel();
			toolBar.add(new TCG.form.ComboBox({name: 'eventType', url: 'investmentEventTypeListFind.json?systemDefined=false&', limitRequestedProperties: false, width: 150, listWidth: 200, emptyText: '< Select Event Type >'}));
			toolBar.add({
				text: 'Add',
				tooltip: 'Create a new Calendar Event of selected type',
				iconCls: 'add',
				scope: this,
				handler: function() {
					let eventType = TCG.getChildByName(toolBar, 'eventType');
					const eventTypeId = eventType.getValue();
					if (TCG.isBlank(eventTypeId)) {
						TCG.showError('You must first select desired event type from the list.');
					}
					else {
						eventType = eventType.store.getById(eventTypeId);
						this.savedDefaultData = {
							eventType: eventType.json
						};
						this.openDetailPage(this.getDetailPageClass(), gridPanel);
					}
				}
			});
			toolBar.add('-');
		},
		addToolbarDeleteButton: function(toolBar) {
			toolBar.add({
				text: 'Remove',
				tooltip: 'Remove selected event',
				iconCls: 'remove',
				scope: this,
				handler: function() {
					const editor = this;
					const grid = this.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select an event to be deleted.', 'No Event Selected');
					}
					else if (sm.getCount() !== 1) {
						TCG.showError('Multi-selection deletes are not supported yet.  Please select one event.', 'NOT SUPPORTED');
					}
					else {
						const e = sm.getSelected();
						const seriesId = TCG.getValue('json.seriesDefinition.id', e);
						if (seriesId) {
							// recurring event
							TCG.createComponent('TCG.app.OKCancelWindow', {
								title: 'Delete Recurring Event',
								iconCls: 'calendar',
								height: 220,
								width: 500,
								modal: true,

								items: [{
									xtype: 'formpanel',
									loadValidation: false,
									instructions: '"' + e.json.label + '" is a recurring event.<br />Do you want to delete all occurrences of this recurring series or just this one?<br/><br />Note: when delete entire series, historic occurrences will not be deleted.',
									items: [
										{
											xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'eventType',
											items: [
												{boxLabel: 'Delete this occurrence.', xtype: 'radio', name: 'eventType', inputValue: 'OCCURRENCE', checked: true},
												{boxLabel: 'Delete the series.', xtype: 'radio', name: 'eventType', inputValue: 'SERIES'}
											]
										}
									]
								}],
								saveWindow: function(closeOnSuccess, forceSubmit) {
									const fp = this.getMainFormPanel();
									const et = fp.getForm().findField('eventType').getValue().inputValue;
									const loader = new TCG.data.JsonLoader({
										waitTarget: grid.ownerCt,
										waitMsg: 'Deleting...',
										params: {id: (et === 'SERIES') ? seriesId : sm.getSelected().id},
										conf: {rowId: sm.getSelected().id},
										onLoad: function(record, conf) {
											grid.ownerCt.reload();
										}
									});
									loader.load(editor.getDeleteURL());
									this.closeWindow();
								},
								openerCt: grid.ownerCt
							});
						}
						else { // non-recurring event
							Ext.Msg.confirm('Delete Selected Event', 'Would you like to delete selected event?', function(a) {
								if (a === 'yes') {
									const loader = new TCG.data.JsonLoader({
										waitTarget: grid.ownerCt,
										waitMsg: 'Deleting...',
										params: editor.getDeleteParams(sm),
										conf: {rowId: sm.getSelected().id},
										onLoad: function(record, conf) {
											grid.getStore().remove(sm.getSelected());
											grid.ownerCt.updateCount();
										}
									});
									loader.load(editor.getDeleteURL());
								}
							});
						}
					}
				}
			});
			toolBar.add('-');
		}
	},

	listeners: {
		afterrender: async function(gridPanel) {
			const t = this.getTopToolbar();
			const tag = TCG.getChildByName(t, 'calendarTagHierarchyId');
			// try to get trader's team: only if one
			const team = await Clifton.security.user.getUserTeam(gridPanel);
			if (TCG.isNotNull(team)) {
				TCG.getChildByName(t, 'teamSecurityGroupId').setValue({value: team.id, text: team.name});
			}
			if (this.getWindow().defaultTagName) {
				const tagName = this.getWindow().defaultTagName;
				if (tagName) {
					const tagValue = await TCG.data.getDataPromiseUsingCaching('systemHierarchyByCategoryAndNameExpanded.json?categoryName=Investment Calendar Tags&name=' + tagName, gridPanel, 'system.hierarchy.Investment Calendar Tags.' + tagName);
					if (tagValue) {
						tag.setValue(tagValue.id);
						tag.setRawValue(tagValue.name);
					}
				}
			}
			gridPanel.reload();
		}
	}

});
Ext.reg('investment-event-list-gridpanel', Clifton.investment.calendar.EventListGridPanel);

Clifton.investment.calendar.EventListWindow = Ext.extend(TCG.app.Window, {
	id: 'investmentEventListWindow',
	title: 'Investment Calendar',
	iconCls: 'calendar',
	width: 1600,
	height: 700,

	gridTabsWithCount: ['Draft Events', 'Pending Events'],
	listeners: {
		afterrender: function(window) {
			if (window.gridTabsWithCount) {
				// when specified, dynamically includes row count from corresponding grid in the title of the tab
				const tabs = window.items.get(0);
				for (let i = 0; i < window.gridTabsWithCount.length; i++) {
					let tabToUpdate;
					for (let j = 0; j < tabs.items.length; j++) {
						const tab = tabs.items.get(j);
						if (tab.title === window.gridTabsWithCount[i]) {
							tabToUpdate = tab;
							break;
						}
					}
					if (tabToUpdate) {
						const gp = tabToUpdate.items.get(0);
						gp.reloadOnRender = false;
						gp.ds.addListener('load', function(store, records, opts) {
							const count = store.getCount();
							tabToUpdate.setTitle(window.gridTabsWithCount[i] + '<b> (' + count + ')</b>');
						});
						gp.reload();
					}
				}
			}

		}
	},

	items: [{
		xtype: 'tabpanel',
		listeners: {
			beforetabchange: function(tabPanel, newTab, currentTab) {
				if (currentTab) {
					const tb = currentTab.items.get(0).getTopToolbar();
					if (tb) {
						// keep selected user team
						const team = TCG.getChildByName(tb, 'teamSecurityGroupId');
						if (team) {
							tabPanel.savedTeam = {
								value: team.getValue(),
								text: team.getRawValue()
							};
						}
						// keep selected tag
						const tag = TCG.getChildByName(tb, 'calendarTagHierarchyId');
						if (tag) {
							tabPanel.savedTag = {
								value: tag.getValue(),
								text: tag.getRawValue()
							};
						}
						// keep selected client account group
						const group = TCG.getChildByName(tb, 'investmentAccountGroupId');
						if (group) {
							tabPanel.savedGroup = {
								value: group.getValue(),
								text: group.getRawValue()
							};
						}
					}
				}
			},
			tabchange: function(tabPanel, tab) {
				const tb = tab.items.get(0).getTopToolbar();
				if (tb) {
					const team = tabPanel.savedTeam;
					if (team) {
						const o = TCG.getChildByName(tb, 'teamSecurityGroupId');
						if (o && o.getValue() !== team.value) {
							o.setValue(team);
							o.fireEvent('select', o, team);
						}
					}
					const tag = tabPanel.savedTag;
					if (tag) {
						const o = TCG.getChildByName(tb, 'calendarTagHierarchyId');
						if (o && o.getValue() !== tag.value) {
							o.setValue(tag);
							o.fireEvent('select', o, tag);
						}
					}
					const group = tabPanel.savedGroup;
					if (group) {
						const o = TCG.getChildByName(tb, 'investmentAccountGroupId');
						if (o && o.getValue() !== group.value) {
							o.setValue(group);
							o.fireEvent('select', o, group);
						}
					}
				}
			}
		},
		items: [
			{
				title: 'Calendar Events',
				reloadOnTabChange: true,
				items: [{
					xtype: 'investment-event-list-gridpanel',
					excludeSeriesDefinition: true
				}]
			},


			{
				title: 'Draft Events',
				reloadOnTabChange: true,
				items: [{
					xtype: 'investment-event-list-gridpanel',
					instructions: 'A list of Calendar Events that are in "Draft" workflow status: currently being edited before submitting for approval.',
					workflowStatus: 'Draft',
					disableInitialLoadDateFilter: true,
					useDisplayFilter: false,
					columnOverrides: [
						{header: 'Workflow Status', width: 50, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=InvestmentEvent'}, hidden: true}
					]
				}]
			},


			{
				title: 'Pending Events',
				reloadOnTabChange: true,
				items: [{
					xtype: 'investment-event-list-gridpanel',
					instructions: 'A list of Calendar Events that are in "Pending" workflow status: pending review and approval (cannot be edited).',
					workflowStatus: 'Pending',
					disableInitialLoadDateFilter: true,
					useDisplayFilter: false,
					columnOverrides: [
						{header: 'Workflow Status', width: 50, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=InvestmentEvent'}, hidden: true}
					]
				}]
			},


			{
				title: 'Advanced',
				items: [{
					name: 'investmentEventListFind',
					xtype: 'gridpanel',
					pageSize: 200,
					additionalPropertiesToRequest: 'id|eventType.cssStyle|seriesDefinition.id',
					instructions: 'A list all calendar events including separate rows for "deleted" events and "series definitions". "Series Date" identifies series events and can be different from "Event Date" when system calculated series date is changed by the user. Use this section to troubleshoot advanced scheduling.',
					wikiPage: 'IT/Investment+Calendar',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 100, xtype: 'toolbar-combo', name: 'calendarTagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Investment Calendar Tags'},
							{fieldLabel: 'Client Account Group', xtype: 'combo', name: 'investmentAccountGroupId', width: 180, url: 'investmentAccountGroupListFind.json'},
							{fieldLabel: 'Client', xtype: 'combo', name: 'businessClientId', width: 180, url: 'businessClientListFind.json'}
						];
					},
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{
							header: 'Event Type', width: 80, dataIndex: 'eventType.name', filter: {type: 'combo', searchFieldName: 'eventTypeId', url: 'investmentEventTypeListFind.json'},
							renderer: function(v, c, r) {
								const style = r.json.eventType.cssStyle;
								if (TCG.isNotBlank(style)) {
									c.attr = 'style="' + style + '"';
								}
								return v;
							}
						},
						{header: 'Client Account / Team', width: 95, dataIndex: 'coalesceAssigneeGroupInvestmentAccountName', filter: {searchFieldName: 'eventScope'}},
						{header: 'Client Account', width: 95, dataIndex: 'investmentAccount.label', filter: {type: 'combo', searchFieldName: 'investmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
						{header: 'Client Account Team', dataIndex: 'investmentAccount.teamSecurityGroup.name', width: 100, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', orNull: true, url: 'securityGroupList.json', loadAll: true}, hidden: true},
						{header: 'Team (Assignee Security Group)', dataIndex: 'assigneeSecurityGroup.name', width: 100, filter: {type: 'combo', searchFieldName: 'assigneeSecurityGroupId', orNull: true, url: 'securityGroupTeamList.json', loadAll: true}, hidden: true},
						{
							header: 'Summary', width: 150, dataIndex: 'label', filter: {searchFieldName: 'summary'},
							renderer: function(v, c, r) {
								if (TCG.isNotNull(r.data.details)) {
									let text = r.data.details;
									if (text.length > 2000) {
										text = 'Double click to see details';
									}
									return '<div qtip=\'' + Ext.util.Format.htmlEncode(text) + '\'>' + v + '</div>';
								}
								return v;
							}
						},
						{header: 'Workflow Status', width: 50, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=InvestmentEvent'}, hidden: true},
						{header: 'Workflow State', width: 40, dataIndex: 'workflowState.name', filter: {type: 'combo', searchFieldName: 'workflowStateId', url: 'workflowStateListFind.json?assignmentTableName=InvestmentEvent'}},
						{header: 'Details', width: 200, dataIndex: 'details', hidden: true},
						{header: 'Amount', width: 40, dataIndex: 'amount', type: 'currency', useNull: true, positiveInGreen: true, negativeInRed: true},
						{header: 'Grouping Label', width: 50, dataIndex: 'labelForGrouping', hidden: true},
						{header: 'Date', width: 35, dataIndex: 'eventDate', filter: {orEquals: true}},
						{header: 'Series Date', width: 35, dataIndex: 'seriesDate', filter: {orEquals: true}},
						{header: 'MOC', width: 23, dataIndex: 'eventType.marketOnClose', filter: {searchFieldName: 'marketOnClose'}, type: 'boolean', tooltip: 'Market On Close'},
						{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'File Attachment(s)', width: 12, tooltip: 'File Attachment(s)', dataIndex: 'fullDocumentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'},
						{
							header: '<div class="recurrence" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'Recurring Event', width: 12, tooltip: 'Recurring Event', dataIndex: 'seriesDefinitionPresent', type: 'boolean',
							renderer: function(v) {
								if (TCG.isTrue(v)) {
									return TCG.renderActionColumn('recurrence', '', 'Recurring Calendar Event');
								}
								return '';
							}
						},
						{
							header: '<div class="checked" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'Completed', width: 12, tooltip: 'Completed Event', dataIndex: 'completedEvent', type: 'boolean',
							renderer: function(v, c, r) {
								return TCG.renderBoolean(v, r.data.resolution);
							}
						},
						{header: 'Resolution', width: 50, dataIndex: 'resolution', hidden: true},
						{header: 'Deleted', width: 25, dataIndex: 'deletedEvent', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.calendar.EventWindow',
						drillDownOnly: true
					},
					getLoadParams: async function(firstLoad) {
						const t = this.getTopToolbar();
						const tag = TCG.getChildByName(this.getTopToolbar(), 'calendarTagHierarchyId');
						if (firstLoad) {
							this.setFilterValue('eventDate', {
								'after': Clifton.calendar.getBusinessDayFrom(-1),
								'before': new Date().add(Date.DAY, 30)
							});
							if (this.getWindow().defaultTagName) {
								const tagName = this.getWindow().defaultTagName;
								if (tagName) {
									const tagValue = await TCG.data.getDataPromiseUsingCaching('systemHierarchyByCategoryAndNameExpanded.json?categoryName=Investment Calendar Tags&name=' + tagName, this, 'system.hierarchy.Investment Calendar Tags.' + tagName);
									if (tagValue) {
										tag.setValue(tagValue.id);
										tag.setRawValue(tagValue.name);
									}
								}
							}
						}

						const params = {};
						let v = TCG.getChildByName(t, 'investmentAccountGroupId').getValue();
						if (v) {
							params.investmentAccountGroupId = v;
						}
						v = TCG.getChildByName(t, 'businessClientId').getValue();
						if (v) {
							params.businessClientId = v;
						}
						if (TCG.isNotBlank(tag.getValue())) {
							params.categoryName = 'Investment Calendar Tags';
							params.categoryTableName = 'InvestmentEventType';
							params.categoryLinkFieldPath = 'eventType';
							params.categoryHierarchyId = tag.getValue();
						}
						return params;
					}
				}]
			},


			{
				title: 'Event Types',
				items: [{
					name: 'investmentEventTypeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all calendar event types defined in the system.  Each Event Type defines the behavior, visual representation and available fields for the corresponding Events.',
					wikiPage: 'IT/Investment+Calendar',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{
							header: 'Event Type Name', width: 67, dataIndex: 'name',
							renderer: function(v, c, r) {
								const s = r.data['cssStyle'];
								if (TCG.isNotBlank(s)) {
									c.attr = 'style="' + s + '"';
								}
								return v;
							}
						},
						{header: 'Event Workflow', width: 73, dataIndex: 'workflow.name', idDataIndex: 'workflow.id'},
						{header: 'Approved State', width: 50, dataIndex: 'approvedWorkflowState.name', idDataIndex: 'approvedWorkflowState.id', hidden: true},
						{header: 'Editable Status', width: 50, dataIndex: 'editableWorkflowStatus.name', idDataIndex: 'editableWorkflowStatus.id', hidden: true},
						{header: 'Holiday Calendar', width: 85, dataIndex: 'holidayCalendar.name', idDataIndex: 'holidayCalendar.id', hidden: true},
						{header: 'Approved State Editable Fields', width: 100, dataIndex: 'approvedStateEditableFields', hidden: true},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Resolution List', width: 80, dataIndex: 'resolutionSystemList.name'},
						{header: 'CSS Style', width: 100, dataIndex: 'cssStyle', hidden: true},
						{
							header: 'Event Modify Condition', width: 85, dataIndex: 'entityModifyCondition.name', url: 'systemConditionListFind.json', hidden: true,
							tooltip: 'This is the condition that will allow modification for Investment Calendar Events of this Type.'
						},
						{
							header: 'Event Scope', dataIndex: 'eventScope', width: 75,
							tooltip: 'Indicates the scope of the event -\n - INVESTMENT_ACCOUNT: An investment account is allowed for events of this type\n - INVESTMENT_ACCOUNT_REQUIRED: An investment account is required for events of this type\n - TEAM_REQUIRED: A team (security group) is required for events of this type',
							filter: {
								type: 'combo', displayField: 'name', valueField: 'name', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['display', 'name', 'description'],
									data: Clifton.investment.calendar.EventScopes
								})
							}
						},
						{header: 'MOC', width: 17, dataIndex: 'marketOnClose', type: 'boolean', tooltip: 'Market On Close'},

						{header: 'Manager Adjustment', width: 50, dataIndex: 'managerBalanceAdjustment', type: 'boolean'},
						{
							header: 'System Defined', width: 40, dataIndex: 'systemDefined', type: 'boolean',
							tooltip: 'Events cannot be created or updated manually.  System Defined events should also be system managed.'
						},
						{
							header: 'System Managed', width: 40, dataIndex: 'systemManaged', type: 'boolean',
							tooltip: 'Events are created by the system, however once created the events can be updated, files attached, etc. to it.'
						},
						{header: 'Allow Amount', width: 35, dataIndex: 'eventAmountAllowed', type: 'boolean'},
						{header: 'Allow Complete', width: 40, dataIndex: 'eventCompletionAllowed', type: 'boolean'},
						{header: 'Require Complete', width: 42, dataIndex: 'eventCompletionRequired', type: 'boolean'},
						{
							header: 'Allow Files', width: 28, dataIndex: 'fileAttachmentAllowed', type: 'boolean',
							tooltip: 'Files are allowed to be attached to events of this type.'
						},
						{header: 'Order', width: 27, dataIndex: 'order', type: 'int', useNull: true, defaultSortColumn: true}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 100, xtype: 'toolbar-combo', name: 'calendarTagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Investment Calendar Tags'}
						];
					},
					getLoadParams: async function(firstLoad) {
						const params = {};
						const tag = TCG.getChildByName(this.getTopToolbar(), 'calendarTagHierarchyId');

						if (firstLoad) {
							if (this.getWindow().defaultTagName) {
								const tagName = this.getWindow().defaultTagName;
								if (tagName) {
									const tagValue = await TCG.data.getDataPromiseUsingCaching('systemHierarchyByCategoryAndNameExpanded.json?categoryName=Investment Calendar Tags&name=' + tagName, this, 'system.hierarchy.Investment Calendar Tags.' + tagName);
									if (tagValue) {
										tag.setValue(tagValue.id);
										tag.setRawValue(tagValue.name);
									}
								}
							}
						}

						if (TCG.isNotBlank(tag.getValue())) {
							params.categoryName = 'Investment Calendar Tags';
							params.categoryHierarchyId = tag.getValue();
						}
						return params;
					},
					editor: {
						detailPageClass: 'Clifton.investment.calendar.EventTypeWindow'
					}
				}]
			},


			{
				title: 'Administration',
				items: [{
					xtype: 'formpanel',
					loadValidation: false,
					validatedOnLoad: false,

					items: [
						{
							xtype: 'fieldset',
							title: 'Rebuild Calendar Events',
							buttonAlign: 'right',
							labelWidth: 130,
							instructions: 'Rebuild events for selected event type(s) and date range.  If date range is left blank, will use the default date range generated based on the event type setup.  Events are rebuilt nightly by a batch job.',

							items: [
								{
									xtype: 'listfield',
									allowBlank: true,
									name: 'eventTypeIds',
									fieldLabel: 'Event Types',
									controlConfig: {
										fieldLabel: 'Event Type', name: 'eventType.name', hiddenName: 'eventType.id', valueField: 'id', displayField: 'name', xtype: 'combo',
										url: 'investmentEventTypeListFind.json?systemManaged=true', detailIdField: 'eventType.id'
									}
								},
								{fieldLabel: 'Start Date', xtype: 'datefield', name: 'startDate'},
								{fieldLabel: 'End Date', xtype: 'datefield', name: 'endDate'}
							],
							buttons: [{
								text: 'Rebuild Events',
								iconCls: 'run',
								width: 150,
								handler: function() {
									const owner = this.findParentByType('formpanel');
									const form = owner.getForm();
									form.submit(Ext.applyIf({
										url: 'investmentEventListSystemManagedRebuild.json',
										waitMsg: 'Rebuilding...',
										success: function(form, action) {
											Ext.Msg.alert('Investment Calendar', action.result.result, function() {
												const grid = owner.items.get(1);
												grid.reload.defer(300, grid);
											});
										}
									}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
								}
							}]
						},

						{
							xtype: 'core-scheduled-runner-grid',
							typeName: 'INVESTMENT-CALENDAR',
							instantRunner: true,
							title: 'Current Processing',
							frame: true,
							height: 250
						}
					]
				}]
			}
		]
	}]
});
