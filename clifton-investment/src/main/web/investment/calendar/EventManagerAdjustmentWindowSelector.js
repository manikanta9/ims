// not the actual window but looks up the event for the adjustment and opens that window
TCG.use('Clifton.investment.calendar.EventWindow');

Clifton.investment.calendar.EventManagerAdjustmentWindowSelector = Ext.extend(Clifton.investment.calendar.EventWindow, {
	url: 'investmentEventManagerAdjustment.json',

	prepareEntity: function(entity) {
		if (entity && entity.investmentEvent && entity.investmentEvent.id) {
			// need to get the event with corresponding dependent list(s)
			return TCG.data.getDataPromise('investmentEvent.json', this, {params: {id: entity.investmentEvent.id}});
		}
		return entity;
	}
});
