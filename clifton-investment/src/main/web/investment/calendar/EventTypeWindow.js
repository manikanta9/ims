Clifton.investment.calendar.EventTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Calendar Event Type',
	iconCls: 'calendar',
	width: 700,
	height: 700,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		url: 'investmentEventType.json',
		instructions: 'Event Type defines the behavior, visual representation and available fields for the corresponding Events.',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Event Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50, grow: true},
			{
				fieldLabel: 'Event Modify Condition', name: 'eventEntityModifyCondition.name', hiddenName: 'eventEntityModifyCondition.id', xtype: 'combo', detailPageClass: 'Clifton.system.condition.ConditionWindow', url: 'systemConditionListFind.json',
				tooltip: 'This is the condition that will allow modification for Investment Calendar Events of this Type.'
			},
			{xtype: 'label', html: '<hr />'},

			{fieldLabel: 'Event Workflow', name: 'workflow.name', hiddenName: 'workflow.id', xtype: 'combo', detailPageClass: 'Clifton.workflow.definition.WorkflowWindow', url: 'workflowListFind.json?assignmentTableName=InvestmentEvent', qtip: 'Optional Workflow that Events of this Type will go through.'},
			{
				fieldLabel: 'Approved State', name: 'approvedWorkflowState.name', hiddenName: 'approvedWorkflowState.id', xtype: 'combo', detailPageClass: 'Clifton.workflow.definition.StateWindow', url: 'workflowStateListByWorkflow.json', loadAll: true, requiredFields: ['workflow.name'],
				qtip: 'Used to determine if an event has been Approved or not.',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const formPanel = TCG.getParentFormPanel(combo);
						const form = formPanel.getForm();
						combo.store.baseParams = {
							workflowId: form.findField('workflow.name').getValue()
						};
					}
				}
			},
			{
				fieldLabel: 'Editable Status', name: 'editableWorkflowStatus.name', hiddenName: 'editableWorkflowStatus.id', xtype: 'combo', detailPageClass: 'Clifton.workflow.definition.StatusWindow', url: 'workflowStatusListFind.json', requiredFields: ['workflow.name'],
				qtip: 'Used to determine if an event is Editable: enable changes to Editable Fields.',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const formPanel = TCG.getParentFormPanel(combo);
						const form = formPanel.getForm();
						combo.store.baseParams = {
							workflowId: form.findField('workflow.name').getValue()
						};
					}
				}
			},
			{
				fieldLabel: 'Workflow Editable Fields', name: 'approvedStateEditableFields', xtype: 'textfield',
				qtip: 'Comma-separated list of name of fields on the Investment Event which should be editable when the event is in Approved workflow state.',
				requiredFields: ['approvedWorkflowState.name']
			},

			{xtype: 'label', html: '<hr />'},
			{
				fieldLabel: 'Holiday Calendar', name: 'holidayCalendar.name', hiddenName: 'holidayCalendar.id', xtype: 'combo', detailPageClass: 'Clifton.calendar.setup.CalendarWindow', url: 'calendarListFind.json',
				qtip: 'Reference to the Calendar which will indicate Holidays for this event type.'
			},
			{
				fieldLabel: 'Event Scope', name: 'eventScope', hiddenName: 'eventScope', displayField: 'name', valueField: 'eventScope', mode: 'local', xtype: 'combo',
				store: {
					xtype: 'arraystore',
					fields: ['name', 'eventScope', 'description'],
					data: Clifton.investment.calendar.EventScopes
				}
			},
			{fieldLabel: 'Resolution List', name: 'resolutionSystemList.name', hiddenName: 'resolutionSystemList.id', xtype: 'combo', detailPageClass: 'Clifton.system.list.ListWindow', url: 'systemListListFind.json'},
			{fieldLabel: 'CSS Style', name: 'cssStyle'},
			{fieldLabel: 'Sort Order', name: 'order', xtype: 'spinnerfield'},

			{boxLabel: 'Market on Close (MOC)', name: 'marketOnClose', xtype: 'checkbox'},
			{
				boxLabel: 'Manager Balance Adjustment', name: 'managerBalanceAdjustment', xtype: 'checkbox',
				qtip: 'This will hide the Manager Balance Adjustment field on events of this type if unchecked.'
			},
			{
				boxLabel: 'Event Amount Allowed', name: 'eventAmountAllowed', xtype: 'checkbox',
				qtip: 'This will hide the Amount field on events of this type if unchecked.'
			},
			{
				boxLabel: 'Event Completion Allowed', name: 'eventCompletionAllowed', xtype: 'checkbox',
				qtip: 'This will hide the Completed field on events of this type if unchecked.'
			},
			{
				boxLabel: 'Event Completion Required', name: 'eventCompletionRequired', xtype: 'checkbox',
				qtip: 'This field exist to indicate if events of this type are required to be completed. At a later time, the system will be updated to generate a notification if events of type with eventCompletionRequired = true have not been completed.'
			},
			{
				boxLabel: 'File Attachment Allowed', name: 'fileAttachmentAllowed', xtype: 'checkbox',
				qtip: 'This will hide the File Attachments field on events of this type if unchecked.'
			},
			{boxLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox'},
			{
				fieldLabel: 'Rebuild Bean', name: 'rebuildSystemBean.name', hiddenName: 'rebuildSystemBean.id', xtype: 'combo', detailPageClass: 'Clifton.system.bean.BeanWindow', url: 'systemBeanListFind.json?groupName=Investment Calendar Event Rebuild Executor',
				getDefaultData: function() {
					return {type: {group: {name: 'Investment Calendar Event Rebuild Executor'}}};
				}
			},
			{fieldLabel: 'Start Days Back', name: 'rebuildDefaultDaysBack', xtype: 'integerfield', minValue: 0, qtip: 'Enter a positive value. By default when rebuilding events of this type go back this many days from today as the start date', requiredFields: ['rebuildSystemBean.id'], setVisibilityOnRequired: true, doNotClearIfRequiredChanges: true},
			{fieldLabel: 'Days to Rebuild', name: 'rebuildDefaultDaysToRebuild', xtype: 'integerfield', minValue: 0, qtip: 'By default when rebuilding events of this type, rebuild this many days from start date', requiredFields: ['rebuildSystemBean.id'], setVisibilityOnRequired: true, doNotClearIfRequiredChanges: true},
			{
				xtype: 'system-tags-fieldset',
				title: 'Tags',
				tableName: 'InvestmentEventType',
				hierarchyCategoryName: 'Investment Calendar Tags'
			}
		]
	}
	]
})
;
