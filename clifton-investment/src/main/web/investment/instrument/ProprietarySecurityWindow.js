TCG.use('Clifton.investment.instrument.BaseSecurityDetailWindow');

Clifton.investment.instrument.ProprietarySecurityWindow = Ext.extend(Clifton.investment.instrument.BaseSecurityDetailWindow, {
	titlePrefix: 'Investment Security - Proprietary',
	iconCls: 'company-logo',
	height: 500,
	width: 900,
	enableRefreshWindow: true,

	securityType: 'Proprietary',

	tabItems: [
		{
			title: 'Security',
			items: [{
				xtype: 'investment-security-forms-with-dynamic-fields',

				items: [
					{fieldLabel: 'Investment Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
					{fieldLabel: 'Ticker Symbol', name: 'symbol'},
					{fieldLabel: 'Security Name', name: 'name'},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
					{fieldLabel: 'Country of Risk', name: 'instrument.countryOfRisk.text', hiddenName: 'instrument.countryOfRisk.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
					{fieldLabel: 'Country of Incorporation', name: 'instrument.countryOfIncorporation.text', hiddenName: 'instrument.countryOfIncorporation.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
					{fieldLabel: 'First Trade', name: 'startDate', xtype: 'datefield'},
					{fieldLabel: 'Last Trade', name: 'endDate', xtype: 'datefield'},
					{boxLabel: 'Illiquid (cannot be easily sold or exchanged for cash)', name: 'illiquid', xtype: 'checkbox'},

					{xtype: 'label', html: '<hr/>'},
					{fieldLabel: 'Proprietary Account', name: 'instrument.investmentAccount.label', hiddenName: 'instrument.investmentAccount.id', xtype: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', disableAddNewItem: true, allowBlank: false},
					{fieldLabel: 'Currency Denomination', name: 'instrument.tradingCurrency.name', hiddenName: 'instrument.tradingCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', allowBlank: false, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
					{fieldLabel: 'Price Multiplier', name: 'instrument.priceMultiplier', xtype: 'floatfield', hidden: true, value: '1', allowBlank: false}
				]
			}]
		},


		{
			title: 'Notes',
			items: [{
				xtype: 'investment-security-system-note-grid'
			}]
		},


		{
			title: 'Instrument Groups',
			layout: 'border',
			items: [
				{
					xtype: 'investment-instrumentGroupGrid_ByInstrument',
					region: 'north',
					height: 250,
					securityWindow: true
				},
				{
					xtype: 'investment-instrumentGroupMissingGrid_ByInstrument',
					region: 'center',
					securityWindow: true
				}
			]
		},


		{
			title: 'Tasks',
			items: [{
				xtype: 'workflow-task-grid',
				tableName: 'InvestmentSecurity'
			}]
		}
	]
});
