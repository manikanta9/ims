Clifton.investment.instrument.upload.SecurityUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'investmentSecurityUploadWindow',
	title: 'Investment Securities Import',
	iconCls: 'import',
	height: 600,
	hideOKButton: true,

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File',
		handler: function() {
			const f = this.ownerCt.ownerCt.items.get(0);
			f.getWindow().getMainFormPanel().downloadSampleFile(false);
		}
	}, '-', {
		text: 'Sample File (All Data)',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File (Contains All (up to 500 rows) Existing Data)',
		handler: function() {
			const f = this.ownerCt.ownerCt.items.get(0);
			f.getWindow().getMainFormPanel().downloadSampleFile(true);
		}
	}, {
		xtype: 'tbfill'
	}, {
		text: 'Generic Import',
		iconCls: 'import',
		tooltip: 'Use generic upload window',
		handler: function() {
			TCG.createComponent('Clifton.system.upload.UploadWindow', {
				defaultData: {tableName: 'InvestmentSecurity'}
			});
		}
	}],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Investment Security Uploads allow you to import securities from files directly into the system. Simple Upload functionality notes: <br/>' +
			'1. Only securities that DO NOT exist will be inserted.  Existing securities will be skipped.  To update securities please use the generic upload.<br/>' +
			'2. File must segregate One-to-One (i.e. Stocks) from One-to-Many securities (i.e. Futures)<br/>' +
			'3. Can upload for a specific hierarchy or across hierarchies that share the same parent hierarchy<br/>' +
			'4. The system will not create new One-to-Many instruments (i.e. Futures)<br/>' +
			'5. Trading Currency must be specified for One-to-One securities within the file or on the upload screen',

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},

			{
				fieldLabel: '', boxLabel: 'Upload One-to-One Securities Only', name: 'oneSecurityPerInstrument', xtype: 'checkbox',
				qtip: 'You can either upload a set of securities that are 1 security per instrument (checked) which means each security will also create/update the corresponding instrument, or many securities per instrument (unchecked).'
			},
			{
				fieldLabel: 'Single Hierarchy', name: 'hierarchy.labelExpanded', hiddenName: 'hierarchy.id', xtype: 'combo', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600, mutuallyExclusiveFields: ['parentHierarchy.id'],
				qtip: 'Used if uploading securities for a specific hierarchy only (Note: Specific hierarchy must be able to contain securities).',

				listeners: {
					beforequery: function(queryEvent) {
						const oneSecurityPerInstrument = queryEvent.combo.getParentForm().getForm().findField('oneSecurityPerInstrument').getValue();
						queryEvent.combo.store.setBaseParam('oneSecurityPerInstrument', oneSecurityPerInstrument);
					}
				}
			},
			{
				fieldLabel: 'Parent Hierarchy', name: 'parentHierarchy.labelExpanded', hiddenName: 'parentHierarchy.id', xtype: 'combo', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=false', queryParam: 'labelExpanded', listWidth: 600, mutuallyExclusiveFields: ['hierarchy.id'],
				qtip: 'Used if uploading securities across hierarchies that share the same parent only (Note: Parent hierarchy can only be one level above child hierarchies).'
			},
			{
				fieldLabel: 'Trading CCY', name: 'tradingCurrency.name', hiddenName: 'tradingCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', requiredFields: ['oneSecurityPerInstrument'],
				qtip: 'Applies only to 1:1 securities where the instrument CCY denomination would be required.  If not selected on screen, assumed that it is included in the file.'
			},
			{fieldLabel: '', xtype: 'checkbox', boxLabel: 'Auto-Generate All Events', name: 'autoGenerateEvents', qtip: 'Allowed for Hierarchies that have event types that support auto-generation.  If checked will also auto-generate all events after creating the new security.'},
			{
				xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
				items: [
					{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
				]
			}
		],

		getSaveURL: function() {
			return 'investmentSecurityUploadFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true';
		},

		downloadSampleFile: function(allRows) {
			const hierarchyId = this.getForm().findField('hierarchy.id').value;
			const parentHierarchyId = this.getForm().findField('parentHierarchy.id').value;
			if ((!hierarchyId || hierarchyId === '') && (!parentHierarchyId || parentHierarchyId === '')) {
				TCG.showError('Single or Parent Hierarchy selection is required to download a sample file.', 'Hierarchy Required');
			}
			else {
				// FileName - usually uses tableName but we include hierarchy in the title
				let fileName = 'InvestmentSecurityUpload-';
				if (hierarchyId && hierarchyId !== '') {
					fileName = fileName + this.getForm().findField('hierarchy.id').getRawValue();
				}
				else if (parentHierarchyId && parentHierarchyId !== '') {
					fileName = fileName + this.getForm().findField('parentHierarchy.id').getRawValue();
				}
				const params = this.getFormValuesFormatted(true);
				params.enableValidatingBinding = true;
				params.fileName = fileName;
				params.allRows = allRows;
				params.outputFormat = 'xls';
				TCG.downloadFile('investmentSecurityUploadFileSampleDownload.json', params, this);
			}
		}
	}]
});
