Clifton.investment.instrument.upload.SecurityAllocationUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'investmentSecurityAllocationUploadWindow',
	title: 'Investment Security Allocation Import',
	iconCls: 'import',
	doNotWarnOnCloseModified: true,
	height: 600,

	tbar: [
		{
			text: 'Sample File',
			iconCls: 'excel',
			tooltip: 'Download Excel Sample File',
			handler: function() {
				const f = this.ownerCt.ownerCt.items.get(0);
				f.getWindow().getMainFormPanel().downloadSampleFile(false);
			}
		}, '-', {
			text: 'Sample File (All Data)',
			iconCls: 'excel',
			tooltip: 'Download Excel Sample File (Contains All (up to 500 rows) Existing Data)',
			handler: function() {
				const f = this.ownerCt.ownerCt.items.get(0);
				f.getWindow().getMainFormPanel().downloadSampleFile(true);
			}
		}, '-', {
			text: 'Sample File (Simple Upload)',
			iconCls: 'excel',
			tooltip: 'Download Excel Sample File',
			handler: function() {
				TCG.openFile('investment/instrument/upload/SimpleInvestmentSecurityAllocationUploadSample.xlsx');
			}
		}, {
			xtype: 'tbfill'
		}, {
			text: 'Generic Import',
			iconCls: 'import',
			tooltip: 'Use generic upload window',
			handler: function() {
				TCG.createComponent('Clifton.system.upload.UploadWindow', {
					defaultData: {tableName: 'InvestmentSecurityAllocation'}
				});
			}
		}
	],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Investment Security Allocation Uploads allow you to import security allocations from files directly into the system for a specific security and start date.  For securities that use allocation types that support rebalancing - rebalancing can automatically be performed on the start date. Note: Leaving start date blank will default to the parent security start date.  When the start date uses the parent security start date, this date will not be set on each allocation.',

		initComponent: function() {
			const currentItems = [];

			// If window.ddFiles (Drag and drop file) - don't show the file selector
			if (this.getWindow().ddFiles) {
				currentItems.push({fieldLabel: 'File', name: 'fileName', xtype: 'displayfield', value: this.getWindow().ddFiles[0].name, submitValue: false});
			}
			else {
				currentItems.push({fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false});
			}

			Ext.each(this.commonItems, function(f) {
				currentItems.push(f);
			});

			this.items = currentItems;

			TCG.form.FormPanel.superclass.initComponent.call(this);
		},

		listeners: {
			afterrender: function() {
				const fp = this;
				const win = fp.getWindow();
				const okButton = win.toolbars[1].items.items[0];
				okButton.handler = function() {
					win.okButtonClicked = true;
					if (TCG.isBlank(win.savedSinceOpen)) {
						win.saveWindow(false);
					}
					if (fp.getFormValue('errorCount') === 0) {
						win.closeWindow();
					}
				};
			}
		},

		saveWithDragAndDropFile: function(closeOnSuccess, forms, form, panel, params) {
			if (panel.getWindow().ddFiles) {
				const win = this;
				const files = win.ddFiles;
				TCG.file.enableDD.upload(files, 0, panel.getEl(), panel.getFormValuesFormatted(), win, panel.getSaveURL(), panel, panel.showStatusWindow);
				return false;
			}
		},

		showStatusWindow: function(data) {
			if (data.detailList && data.detailList.length > 0) {
				TCG.createComponent('Clifton.core.StatusWindow', {
					title: 'Security Allocation Upload Status',
					defaultData: {status: data}
				});
			}
			this.form.formValues['errorCount'] = data.errorCount;
			this.form.setValues(data, true);
		},

		getDefaultData: async function(win) {
			win.saveForm = win.saveForm.createInterceptor(this.saveWithDragAndDropFile);
			return win.defaultData || {};
		},

		loadDefaultDataAfterRender: true,
		prepareDefaultData: function(defaultData) {
			if (defaultData && defaultData.parentInvestmentSecurity) {
				const security = TCG.data.getData('investmentSecurity.json?id=' + defaultData.parentInvestmentSecurity.id + '&requestedPropertiesRoot=data&requestedProperties=id|label|instrument.hierarchy.securityAllocationType.autoRebalancingSupported', this);
				if (security && security.instrument && security.instrument.hierarchy && security.instrument.hierarchy.securityAllocationAutoRebalancingSupported === true) {
					this.showHideRebalancingFieldSet(true);
				}
				else {
					this.showHideRebalancingFieldSet(false);
				}
			}
			const defaultDataSource = TCG.data.getData('marketDataSourceByName.json?marketDatasourceName=Parametric Clifton', this, 'Clifton.marketdata.dataSource.Parametric Clifton');
			if (defaultDataSource) {
				defaultData.dataSourceId = defaultDataSource.id;
				defaultData.dataSourceName = defaultDataSource.name;
			}
			return defaultData;
		},

		showHideRebalancingFieldSet: function(rebalancingSupported) {
			const fp = this;

			const rebalancingFieldSet = TCG.getChildByName(fp, 'rebalancingFieldSet');
			if (rebalancingSupported === true) {
				rebalancingFieldSet.show();
			}
			else {
				rebalancingFieldSet.hide();
			}
		},
		commonItems: [
			{
				fieldLabel: 'Parent Security', name: 'parentInvestmentSecurity.label', hiddenName: 'parentInvestmentSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json?allocationOfSecurities=true', allowBlank: false,
				requestedProps: 'instrument.hierarchy.securityAllocationType.autoRebalancingSupported',
				listeners: {
					select: function(combo, record, index) {
						const fp = combo.getParentForm();
						fp.showHideRebalancingFieldSet(record.json['instrument.hierarchy.securityAllocationType.autoRebalancingSupported']);
					}
				}
			},
			{fieldLabel: 'Start Date', xtype: 'datefield', name: 'startDate'},
			{
				xtype: 'checkbox', name: 'applyAsFullAllocationListOnStartDate', fieldLabel: ''
				, boxLabel: 'Selected file contains full set of allocations on start date. Will end and start new allocations for changes only (Allocations missing from the file will be considered ended on start date - 1)'
				, qtip: 'If checked, will retrieve all allocations active on the start date and attempt to match each allocation to the upload file.  Those missing from the file (or entered with weight/shares = 0) will be ended on start date -1. Those that match with no change will not be changes.  Those that match with a change will be ended on start date - 1 and new allocation will be started on start date.'
			},
			{
				xtype: 'fieldset', title: 'Recalculate Rebalance', name: 'rebalancingFieldSet',
				items: [
					{xtype: 'checkbox', boxLabel: 'Recalculate Security Allocation Rebalance on Selected Start Date (Or Security Start Date if left blank)', name: 'recalculateRebalanceOnStartDate'},
					{
						xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'useBaseWeights', allowBlank: false, requiredFields: 'recalculateRebalanceOnStartDate',
						items: [
							{boxLabel: 'Recalculate weights based on Shares', xtype: 'radio', name: 'updateWeights', inputValue: true},
							{boxLabel: 'Recalculate shares based on Weights', xtype: 'radio', name: 'updateWeights', inputValue: false}
						],
						listeners: {
							change: function(rg, r) {
								if (r) {
									const p = TCG.getParentFormPanel(rg);
									const startingPriceField = p.getForm().findField('startingPrice');
									startingPriceField.setDisabled(r.inputValue === true);
								}
							}
						}
					},
					{fieldLabel: 'Data Source', name: 'dataSourceName', hiddenName: 'dataSourceId', xtype: 'combo', requiredFields: 'recalculateRebalanceOnStartDate', url: 'marketDataSourceListFind.json?marketDataValueSupported=true', qtip: 'Datasource to use when saving calculated values', doNotClearIfRequiredChanges: true},
					{fieldLabel: 'Starting Price', name: 'startingPrice', xtype: 'floatfield', requiredFields: 'recalculateRebalanceOnStartDate'}
				]
			},
			{
				xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
				items: [
					{name: 'messageWithErrors', xtype: 'textarea', readOnly: true, submitField: false}
				]
			}
		],

		getSaveURL: function() {
			return 'investmentSecurityAllocationUploadFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true';
		},

		downloadSampleFile: function(allRows) {
			if (this.getForm().findField('parentInvestmentSecurity.id').value === '') {
				TCG.showError('Parent Security selection is required to download a sample file.', 'Parent Security Required');
			}
			else {
				// Pass fileName here - usually uses tableName but we aren't passing that as a parameter here and want to include hierarchy
				const fileName = 'InvestmentSecurityAllocation-' + this.getForm().findField('parentInvestmentSecurity.id').getRawValue();
				const params = this.getFormValuesFormatted(true);
				params.enableValidatingBinding = true;
				params.allRows = allRows;
				params.fileName = fileName;
				params.outputFormat = 'xls';
				TCG.downloadFile('investmentSecurityAllocationUploadFileSampleDownload.json', params, this);
			}
		}
	}],

	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Saving...',
			success: function(form, action) {
				const data = action.result.data;
				form.clearInvalid();
				win.savedSinceOpen = true;
				panel.showStatusWindow(data);
				if (win.modifiedFormCount > 1) {
					win.saveWindow(win.closeOnSuccess);
				}
				else if (win.closeOnSuccess) {
					win.closeWindow();
				}
				else {
					win.setModified(false);
				}
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	}
});
