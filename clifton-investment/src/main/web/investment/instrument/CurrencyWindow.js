TCG.use('Clifton.investment.instrument.BaseSecurityDetailWindow');

Clifton.investment.instrument.CurrencyWindow = Ext.extend(Clifton.investment.instrument.BaseSecurityDetailWindow, {
	titlePrefix: 'Currency',
	iconCls: 'money',
	height: 600,
	width: 850,
	enableRefreshWindow: true,

	securityType: 'Currency',
	insertTabTitle: 'Instrument Groups',

	tabItems: [
		{
			title: 'Currency',
			items: [{
				xtype: 'formpanel',
				url: 'investmentSecurity.json',
				labelWidth: 125,
				getInfoWindowAuditTrailLoadParams: function(fp) {
					// include both security and instrument fields in audit trail
					return {
						tableName: 'InvestmentInstrument',
						childTables: 'InvestmentSecurity',
						entityId: fp.getFormValue('instrument.id')
					};
				},

				items: [
					{fieldLabel: 'Investment Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
					{fieldLabel: 'Ticker Symbol', name: 'symbol'},
					{fieldLabel: 'Currency Name', name: 'name'},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
					{fieldLabel: 'Country of Risk', name: 'instrument.countryOfRisk.text', hiddenName: 'instrument.countryOfRisk.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
					{fieldLabel: 'Country of Incorporation', name: 'instrument.countryOfIncorporation.text', hiddenName: 'instrument.countryOfIncorporation.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
					{fieldLabel: 'Settlement Calendar', name: 'instrument.settlementCalendar.name', hiddenName: 'instrument.settlementCalendar.id', xtype: 'combo', url: 'calendarListFind.json', detailPageClass: 'Clifton.calendar.setup.CalendarWindow', qtip: 'Days to Settle are defined in Currency Conventions for each currency pair'},
					{
						fieldLabel: 'Notional Calculator', name: 'instrument.costCalculator', hiddenName: 'instrument.costCalculator', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
						store: new Ext.data.ArrayStore({
							fields: ['name', 'value', 'description'],
							data: Clifton.investment.instrument.NotionalCalculators
						})
					},
					{fieldLabel: 'First Trade', name: 'startDate', xtype: 'datefield'},
					{fieldLabel: 'Last Trade', name: 'endDate', xtype: 'datefield'}
				]
			}]
		},


		{
			title: 'Instrument Groups',
			layout: 'border',
			items: [
				{
					xtype: 'investment-instrumentGroupGrid_ByInstrument',
					region: 'north',
					height: 250,
					securityWindow: true
				},
				{
					xtype: 'investment-instrumentGroupMissingGrid_ByInstrument',
					region: 'center',
					securityWindow: true
				}
			]
		},


		{
			title: 'Tasks',
			items: [{
				xtype: 'workflow-task-grid',
				tableName: 'InvestmentSecurity'
			}]
		}
	]
});
