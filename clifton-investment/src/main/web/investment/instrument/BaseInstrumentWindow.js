/**
 * The base instrument window, from which all instrument windows inherit.
 * <p>
 * This class provides functions to aid with configuring the structure of the instrument window. Shared tabs and instrument fields are included in this base
 * class. Tabs and fields which are used by only a single instrument window should be not included in this base class, but should rather be added to the
 * sub-class by overriding the configurable options, such as <tt>afterExtraFields</tt> or <tt>customTabs</tt>.
 */
Clifton.investment.instrument.BaseInstrumentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Instrument',
	iconCls: 'coins',
	height: 700,
	width: 850,
	enableRefreshWindow: true,

	// If true then will put generic security fields in a field set
	useFieldSet: false,
	fieldSetTitle: 'General Instrument Info',
	instructions: 'Please enter Instrument Info below.',

	getSaveURL: function() {
		return 'investmentInstrumentSave.json';
	},

	getConfirmBeforeSaveMsg: function() {
		return undefined;
	},

	getConfirmBeforeSaveMsgTitle: function() {
		return undefined;
	},


	/**
	 * The list of default field names which shall be excluded from the instrument form. These exclusions are only applied to the <tt>standardFields</tt> and
	 * <tt>extraFields</tt> lists, and not any of the <tt>before</tt> or <tt>after</tt> lists.
	 */
	excludedFields: [],

	// Standard instrument fields
	standardFields: [
		{fieldLabel: 'Hierarchy', name: 'hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'hierarchy.id'},
		{xtype: 'investment-instrument-underlying-combo', name: 'underlyingInstrument.labelShort', hiddenName: 'underlyingInstrument.id'},
		{fieldLabel: 'Big Instrument', name: 'bigInstrument.labelShort', hiddenName: 'bigInstrument.id', xtype: 'combo', displayField: 'labelShort', url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow'},
		{xtype: 'investment-instrument-identifier-prefix'},
		{fieldLabel: 'Instrument Name', name: 'name'},
		{fieldLabel: 'Group Name', name: 'groupName', qtip: 'Optional group name can be used in reporting in order to combine multiple instruments together under this name.'},
		{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70}
	],
	beforeStandardFields: [],
	getBeforeStandardFields: function() {
		return this.beforeStandardFields;
	},
	afterStandardFields: [],
	getAfterStandardFields: function() {
		return this.afterStandardFields;
	},

	// Extra instrument fields (separated from standard fields with an <hr/>)
	extraFields: [
		{fieldLabel: 'BICS Industry Subgroup', xtype: 'system-hierarchy-combo', tableName: 'InvestmentInstrument', hierarchyCategoryName: 'BICS Hierarchy', qtip: 'Bloomberg Industry Classification System (BICS)'},
		{fieldLabel: 'Country of Risk', name: 'countryOfRisk.text', hiddenName: 'countryOfRisk.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
		{fieldLabel: 'Country of Incorporation', name: 'countryOfIncorporation.text', hiddenName: 'countryOfIncorporation.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
		{fieldLabel: 'Primary Exchange', name: 'exchange.label', hiddenName: 'exchange.id', xtype: 'combo', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=false', detailPageClass: 'Clifton.investment.setup.ExchangeWindow', qtip: 'The main stock exchange where a publicly traded company\'s stock is bought and sold.'},
		{fieldLabel: 'Composite Exchange', name: 'compositeExchange.label', hiddenName: 'compositeExchange.id', xtype: 'combo', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=true', detailPageClass: 'Clifton.investment.setup.ExchangeWindow', qtip: 'Composite Exchange is not a real exchange. It is used to calculate average or standardized prices for a security across all exchanges where it trades.'},
		{fieldLabel: 'Settlement Calendar', name: 'settlementCalendar.name', hiddenName: 'settlementCalendar.id', xtype: 'combo', url: 'calendarListFind.json', detailPageClass: 'Clifton.calendar.setup.CalendarWindow', qtip: 'Optional Settlement Calendar that will override the calendar of the Exchange or Hierarchy. It is used to calculate the Settlement Date and to determine holidays when securities are not priced. Define only if different.'},
		{fieldLabel: 'Days to Settle', name: 'daysToSettle', xtype: 'integerfield', qtip: 'Specifies the number of days it may take to settle a security for this Instrument. This value overrides the value specified at Investment Type and Investment Hierarchy.  Some Securities can override this value via custom fields.'},
		{fieldLabel: 'Currency Denomination', name: 'tradingCurrency.name', hiddenName: 'tradingCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', allowBlank: false, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
		{fieldLabel: 'Price Multiplier', name: 'priceMultiplier', xtype: 'floatfield', qtip: 'Price Multiplier for securities for this instrument. If the hierarchy allows it, this price multiplier can be overridden for securities for this instrument.'},
		{fieldLabel: 'Exposure Multiplier', name: 'exposureMultiplier', xtype: 'floatfield', qtip: 'Exposure Multiplier used for a few futures (Euro dollar and Euro Yen = 4) where the exposure is different than the notional value and needs to be accounted for by this multiplier. Example: Euro Dollar has contract size of $1,000,000 (multiplied by 4 to get annualized equivalent of quarterly interest rate: 12 / 3 = 4). Exposure = Price * Quantity * Multiplier * 4 where 4 is the exposure multiplier'},
		{fieldLabel: 'Notional Calculator', name: 'costCalculator', hiddenName: 'costCalculator', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', store: {xtype: 'arraystore', fields: ['name', 'value', 'description'], data: Clifton.investment.instrument.NotionalCalculators}},
		{fieldLabel: 'M2M Calculator', name: 'm2mCalculator', hiddenName: 'm2mCalculator', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', store: {xtype: 'arraystore', fields: ['name', 'value', 'description'], data: Clifton.investment.instrument.M2MCalculators}},
		{fieldLabel: 'Accrual Date Calculator', name: 'accrualDateCalculator', hiddenName: 'accrualDateCalculator', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', store: {xtype: 'arraystore', fields: ['value', 'name', 'description'], data: Clifton.investment.instrument.AccrualDateCalculators}}
	],
	beforeExtraFields: [],
	getBeforeExtraFields: function() {
		return this.beforeExtraFields;
	},
	afterExtraFields: [],
	getAfterExtraFields: function() {
		return this.afterExtraFields;
	},

	/**
	 * The list of default tabs which shall be excluded from the window. These exclusions are applied to tabs listed in <tt>standardTabs</tt> only.
	 */
	excludedTabs: [],

	// The list of default tabs which will be displayed in the window.
	standardTabs: [
		{
			title: 'Instrument',
			items: [{
				xtype: 'investment-instrument-forms-with-dynamic-fields',
				labelWidth: 140,
				getSaveURL: function() {
					return this.getWindow().getSaveURL();
				},
				getConfirmBeforeSaveMsg: function() {
					this.confirmBeforeSaveMsgTitle = this.getWindow().getConfirmBeforeSaveMsgTitle();
					return this.getWindow().getConfirmBeforeSaveMsg();
				},
				initComponent: function() {
					this.items = this.getWindow().generateInstrumentFormItems();
					TCG.callSuper(this, 'initComponent', arguments);
				}
			}]
		},


		{
			title: 'Securities',
			items: [{
				xtype: 'investment-instrumentSecurityGrid'
			}]
		},


		{
			title: 'Instrument Groups',
			layout: 'border',
			items: [{
				xtype: 'investment-instrumentGroupGrid_ByInstrument',
				region: 'north',
				height: 250
			}, {
				xtype: 'investment-instrumentGroupMissingGrid_ByInstrument',
				region: 'center'
			}]
		},


		{
			title: 'Tasks',
			items: [{
				xtype: 'workflow-task-grid',
				tableName: 'InvestmentInstrument'
			}]
		}
	],

	/**
	 * The list of custom tabs to add to the dialog window at the specified indices. The structure of elements should as follows:
	 * <pre>
	 * {
	 *     index: &lt;insertAtIndex&gt;,
	 *     item: &lt;tabConfiguration&gt;
	 * }
	 * </pre>
	 * Where <tt>insertAtIndex</tt> is the index at which the tab shall be created (after <tt>excludedTabs</tt> tabs are removed), and <tt>tabConfiguration</tt>
	 * is the actual tab configuration.
	 */
	customTabs: [],
	getCustomTabs: function() {
		return this.customTabs;
	},


	/**
	 * Performs pre-initialization configuration.
	 */
	init: function() {
		this.items = this.generateWindowItems();
		return TCG.callSuper(this, 'init', arguments);
	},


	/**
	 * Generates the window items, including the configured tabs.
	 *
	 * @return {[*]} the generated window items
	 */
	generateWindowItems: function() {
		const self = this;
		const tabs = [];
		let i;
		let tab;

		// Add standard tabs, applying filters
		const standardTabOverrides = Clifton.investment.instrument.InstrumentStandardTabOverrides || [];
		const standardTabsWithAdditional = [...this.standardTabs];
		const additionalTabs = Clifton.investment.instrument.InstrumentStandardAdditionalTabs;
		if (additionalTabs && additionalTabs.length > 0) {
			// Insert additional tabs before Instrument Groups tab
			standardTabsWithAdditional.splice(standardTabsWithAdditional.length - 2, 0, ...additionalTabs);
		}
		// filter out excluded tabs and apply any overrides before creating tabs
		const filteredStandardTabs = standardTabsWithAdditional.filter(currentTab => !self.isItemInList(self.excludedTabs, currentTab, 'title'))
			.map(currentTab => {
				const override = standardTabOverrides.find(overriddenTab => overriddenTab.title === currentTab.title);
				return override ? override : currentTab;
			});

		TCG.appendToArray(tabs, ...filteredStandardTabs);

		// Add additional tabs
		const customTabs = [...this.getCustomTabs()];
		if (TCG.isNotBlank(this.securityType)) {
			const additionalCustomTabs = Clifton.investment.instrument.InstrumentCustomAdditionalTabsByType[this.securityType];
			if (additionalCustomTabs && additionalCustomTabs.length > 0) {
				customTabs.push(...additionalCustomTabs);
			}
		}
		customTabs.sort((first, second) => {
			if (TCG.isNumber(first.index)) {
				return TCG.isNumber(second.index) ? first.index - second.index : -1;
			}
			else if (TCG.isNumber(second.index)) {
				return 1;
			}
			return 0;
		});
		for (i = 0; i < customTabs.length; i++) {
			tab = customTabs[i];
			tabs.splice(tab.index, 0, tab.item);
		}

		return [{
			xtype: 'tabpanel',
			requiredFormIndex: 0,
			items: tabs
		}];
	},


	/**
	 * Generates the list of items for the instrument form panel. These items are configurable via several configurable properties in this class.
	 *
	 * @return {Array} the list of items for the instrument form panel
	 */
	generateInstrumentFormItems: function() {
		let instrumentFormItems;
		const self = this;
		const fieldList = [];

		// Aggregate filtered field list
		Array.prototype.push.apply(fieldList, Array.prototype.concat.call(
			this.getBeforeStandardFields(),
			this.standardFields.filter(function(field) {
				return !self.isItemInList(self.excludedFields, field, 'name');
			}),
			this.getAfterStandardFields(),

			{xtype: 'label', html: '<hr/>'},
			this.getBeforeExtraFields(),
			this.extraFields.filter(function(field) {
				return !self.isItemInList(self.excludedFields, field, 'name');
			}),
			this.getAfterExtraFields()
		));

		// Wrap in fieldset if enabled
		if (this.useFieldSet) {
			instrumentFormItems = {
				xtype: 'fieldset',
				collapsible: false,
				title: this.fieldSetTitle,
				items: fieldList
			};
		}
		else {
			instrumentFormItems = fieldList;
		}
		return instrumentFormItems;
	},

	/**
	 * Determines if the given source item exists in the given comparison list by complex comparison logic based on the type of each item in the comparison
	 * list.
	 * <p>
	 * The given source item is compared against each comparison item. The comparison behavior depends on the type of the comparison item as follows:
	 * <ul>
	 * <li><tt>string</tt>: <tt>true</tt> if the comparison value is equal to the value of the <tt>defaultPropertyName</tt> property of the source item
	 * <li><tt>function</tt>: <tt>true</tt> if the function returns true when evaluated with the source item as its sole argument
	 * <li><tt>object</tt>: <tt>true</tt> if each property of the comparison item exists in the source item with an equal value (via <tt>==</tt> comparison)
	 * </ul>
	 * If any of item in the comparison list matches the source item, then this function will return <tt>true</tt>.
	 *
	 * @param comparisonList the list of comparison elements
	 * @param sourceItem the source item
	 * @param defaultPropertyName the default property name to evaluate against if the comparison item is a string
	 * @return {boolean} <tt>true</tt> if a comparison item matches the source item, or <tt>false</tt> otherwise
	 */
	isItemInList: function(comparisonList, sourceItem, defaultPropertyName) {
		let itemInList = false;
		let i;
		let listItem;
		for (i = 0; i < comparisonList.length; i++) {
			listItem = comparisonList[i];
			if (typeof listItem === 'string') {
				itemInList = sourceItem[defaultPropertyName] === listItem;
			}
			else if (typeof listItem === 'function') {
				itemInList = listItem(sourceItem);
			}
			else if (typeof listItem === 'object') {
				Ext.each(Object.getOwnPropertyNames(listItem), function(propertyName) {
					itemInList = itemInList || sourceItem[propertyName] === listItem[propertyName];
				});
			}
			else {
				console.warn('Unexpected sourceItem type: [' + (typeof sourceItem) + ']. Value: [' + sourceItem + '].');
			}
			if (itemInList) {
				break;
			}
		}
		return itemInList;
	}
});
