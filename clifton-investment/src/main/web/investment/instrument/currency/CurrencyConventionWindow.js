Clifton.investment.instrument.currency.CurrencyConventionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Currency Convention',
	iconCls: 'currency',
	width: 670,
	height: 300,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Convention',
				items: [{
					xtype: 'formpanel',
					instructions: 'FX Rate between USD and GBP is quoted as 1.5 meaning 1 USD = 1 GBP / 1.5 [multiplyFromByFX = false], but FX rate between USD and JPY is quoted as 80 meaning 1 USD = 1 JPY * 80 [multiplyFromByFX = true]',
					url: 'investmentCurrencyConvention.json',
					items: [
						{fieldLabel: 'From Currency', name: 'fromCurrency.label', hiddenName: 'fromCurrency.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'},
						{fieldLabel: 'To Currency', name: 'toCurrency.label', hiddenName: 'toCurrency.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'},
						{boxLabel: 'Multiply From currency amount by FX Rate to get To currency amount', name: 'multiplyFromByFX', xtype: 'checkbox'},
						{fieldLabel: 'Days to Settle', name: 'daysToSettle', xtype: 'integerfield'}
					]
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'InvestmentCurrencyConvention'
				}]
			}
		]
	}]
});
