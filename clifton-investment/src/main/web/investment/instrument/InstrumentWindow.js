// not the actual window but a window selector based on hierarchy meta-data
Clifton.investment.instrument.InstrumentWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'investmentInstrument.json',

	getClassName: function(config, entity) {
		if (!config.params) {
			// NEW instrument or security
			const dd = config.defaultData;
			const h = dd.hierarchy;
			const screen = h.instrumentScreen;
			if (h.oneSecurityPerInstrument) {
				// for securities, hierarchy is a level deeper
				dd.instrument = {hierarchy: h, priceMultiplier: h.priceMultiplier};
				return this.getSecurityClassName(screen);
			}
			dd.priceMultiplier = h.priceMultiplier;
			dd.exposureMultiplier = 1;
			return this.getInstrumentClassName(screen);
		}

		// open existing instrument or security window based on hierarchy
		if (entity) {
			const h = entity.hierarchy;
			const screen = h.instrumentScreen;
			if (h.oneSecurityPerInstrument) {
				// need to lookup the one-to-one security for the instrument
				const selector = this;
				const sLoader = new TCG.data.JsonLoader({
					params: {instrumentId: entity.id},
					onLoad: function(record, conf) {
						if (record) {
							const className = selector.getSecurityClassName(screen);
							if (className) {
								selector.doOpenEntityWindow(config, record, className);
							}
						}
					}
				});
				sLoader.load('investmentSecurityByInstrument.json', this);
			}
			else {
				return this.getInstrumentClassName(screen);
			}
		}
		return false;
	},

	getSecurityClassName: function(screen) {
		const className = Clifton.investment.instrument.SecurityWindowClassMap[screen];
		if (!className) {
			TCG.showError('Unsupported security screen: ' + screen, 'Investment Security');
			return false;
		}
		return className;
	},

	getInstrumentClassName: function(screen) {
		const className = Clifton.investment.instrument.InstrumentWindowClassMap[screen];
		if (!className) {
			TCG.showError('Unsupported instrument screen: ' + screen, 'Investment Instrument');
			return false;
		}
		return className;
	}
});
