TCG.use('Clifton.investment.instrument.BaseSecurityDetailWindow');

Clifton.investment.instrument.GenericSecurityWindow = Ext.extend(Clifton.investment.instrument.BaseSecurityDetailWindow, {
	titlePrefix: 'Investment Security',
	iconCls: 'stock-chart',
	width: 900,
	height: 680,
	categorySpecificItems: undefined,
	enableRefreshWindow: true,

	// If true then will put generic security fields in a field set
	useFieldSet: false,
	fieldSetTitle: 'General Security Info',
	instructions: 'Please enter Security Info below.',

	getSaveURL: function() {
		// Override if category specific save url is necessary
		return 'investmentSecuritySave.json';
	},

	securityItems: [
		// General Security Fields
		{fieldLabel: 'Investment Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
		{fieldLabel: 'Investment Instrument', name: 'instrument.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'instrument.id'},

		{xtype: 'investment-instrument-ticker'},
		{fieldLabel: 'CUSIP', name: 'cusip'},
		{fieldLabel: 'ISIN', name: 'isin'},
		{fieldLabel: 'SEDOL', name: 'sedol'},
		{fieldLabel: 'FIGI', name: 'figi', qtip: 'Financial Instrument Global Identifier (FIGI; also known as the Bloomberg Global Identifier (BBGID)) is an open standard, 12 character unique identifier assigned to all financial instruments and will not change once issued. For equity instruments, an identifier is issued per trading venue.'},
		{fieldLabel: 'OCC Symbol', name: 'occSymbol', qtip: 'OCC Symbol is a unique code used to identify Options on a futures exchange.  Options Clearing Corporation\'s (OCC) Options Symbology Initiative (OSI) mandated an industry-wide change to this methodology in 2010. We use it to reconcile and confirm trades with external parties.'},
		{fieldLabel: 'Security Name', name: 'name'},
		{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 40},
		{boxLabel: 'Illiquid (cannot be easily sold or exchanged for cash)', name: 'illiquid', xtype: 'checkbox'},
		{
			fieldLabel: 'BICS Industry Subgroup', xtype: 'system-hierarchy-combo', tableName: 'InvestmentInstrument', hierarchyCategoryName: 'BICS Hierarchy',
			qtip: 'Bloomberg Industry Classification System (BICS)',
			getFkFieldId: function(fp) {
				return fp.getFormValue('instrument.id', true);
			}
		},
		{
			fieldLabel: 'GICS Sub-industry', xtype: 'system-hierarchy-combo', tableName: 'InvestmentInstrument', hierarchyCategoryName: 'GICS Hierarchy',
			qtip: 'Global Industry Classification Standard (GICS)',
			getFkFieldId: function(fp) {
				return fp.getFormValue('instrument.id', true);
			}
		},
		{
			fieldLabel: 'Issuer', name: 'businessCompany.name', hiddenName: 'businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true', detailPageClass: 'Clifton.business.company.CompanyWindow',
			listeners: {
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const companyType = combo.getParentForm().getFormValue('instrument.hierarchy.businessCompanyType.name');
					combo.store.baseParams = companyType ? {companyType: companyType} : {};
				}
			}
		},

		{xtype: 'label', html: '<hr/>'},
		{fieldLabel: 'First Trade', name: 'startDate', xtype: 'datefield'},
		{fieldLabel: 'Last Trade', name: 'endDate', xtype: 'datefield'}
	],


	getFormItems: function() {
		const currentItems = [];

		if (this.useFieldSet) {
			const fs = new TCG.form.FieldSet({
				collapsible: false,
				title: this.fieldSetTitle,
				items: TCG.clone(this.securityItems)
			});
			currentItems.push(fs);
		}
		else {
			Ext.each(this.securityItems, function(f) {
				currentItems.push(TCG.clone(f));
			});
		}

		const hierarchy = TCG.getValue('instrument.hierarchy', this.defaultData);
		const investmentType = TCG.getValue('investmentType.name', hierarchy);
		// these fields should be displayed only when instrument.deliverable == true
		if (investmentType === 'Options') {
			currentItems.push({
				fieldLabel: 'Expiration Date', name: 'lastDeliveryDate', xtype: 'datefield',
				listeners: {
					change: function(field, newValue, oldValue) {
						TCG.getParentFormPanel(field).setFormValue('firstDeliveryDate', newValue);
					},
					select: function(field, newValue) {
						TCG.getParentFormPanel(field).setFormValue('firstDeliveryDate', newValue);
					}
				}
			});
			currentItems.push({name: 'firstDeliveryDate', xtype: 'datefield', hidden: true});
		}
		else if (TCG.isTrue(TCG.getValue('instrument.deliverable', this.defaultData))) {
			if (investmentType === 'Forwards') {
				Ext.each(currentItems, function(f) {
					if (f.name === 'endDate') {
						f.fieldLabel = 'Settlement Date';
						f.qtip = 'Last Date when this forward can be traded.';
					}
				});
			}
			else {
				const deliverableItems = [
					{fieldLabel: 'First Notice Date', name: 'firstNoticeDate', xtype: 'datefield'},
					{fieldLabel: 'First Delivery Date', name: 'firstDeliveryDate', xtype: 'datefield'},
					{fieldLabel: 'Last Delivery Date', name: 'lastDeliveryDate', xtype: 'datefield'}
				];
				Ext.each(deliverableItems, function(f) {
					currentItems.push(f);
				});
			}
		}
		else if (investmentType === 'Forwards') {
			//else if (TCG.getValue('instrument.hierarchy.closeOnMaturityOnly', this.defaultData) == true) {
			Ext.each(currentItems, function(f) {
				if (f.name === 'endDate') {
					f.fieldLabel = 'Fixing Date';
					f.qtip = 'Last Date when this forward can be traded.  Also, the date when spot rate used to calculate settlement amount for NDF is recorded.';
				}
			});
			currentItems.push({
				fieldLabel: 'Settlement Date', name: 'firstDeliveryDate', xtype: 'datefield', qtip: 'Security Maturity event is processed and payment is made on this date',
				listeners: {
					change: function(field, newValue, oldValue) {
						field.updateLastDeliveryDate(newValue);
					},
					select: function(field, date) {
						field.updateLastDeliveryDate(date);
					}
				},
				updateLastDeliveryDate: function(date) {
					const fp = TCG.getParentFormPanel(this);
					fp.setFormValue('lastDeliveryDate', date);
				}
			});
			currentItems.push({name: 'lastDeliveryDate', xtype: 'datefield', hidden: true});
		}
		else if (TCG.isTrue(TCG.getValue('noPaymentOnOpen', hierarchy))) {
			currentItems.push({fieldLabel: 'Valuation Date', name: 'firstNoticeDate', xtype: 'datefield', qtip: 'Valuation Date for Cash Settled securities'});
		}
		if (TCG.isTrue(TCG.getValue('securityPriceMultiplierOverrideAllowed', hierarchy))) {
			currentItems.push({fieldLabel: 'Price Multiplier Override', name: 'priceMultiplierOverride', xtype: 'floatfield', qtip: 'Price Multiplier override for this specific security - if different than the instrument.'});
		}
		currentItems.push({xtype: 'label', html: '<hr/>'});

		// underlying security
		if (TCG.getValue('instrument.underlyingInstrument.hierarchy.oneSecurityPerInstrument', this.defaultData) === false) {
			const underlyingId = TCG.getValue('instrument.underlyingInstrument.id', this.defaultData);
			currentItems.push({
				fieldLabel: 'Underlying Security', name: 'underlyingSecurity.label', hiddenName: 'underlyingSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: false,
				beforequery: function(queryEvent) {
					queryEvent.combo.store.baseParams = {instrumentId: underlyingId};
				}
			});
		}
		else {
			currentItems.push({fieldLabel: 'Underlying Security', name: 'underlyingSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'underlyingSecurity.id'});
		}

		// option specific fields -- if we have an option type, display option-specific fields
		if (investmentType === 'Options') {
			currentItems.push({fieldLabel: 'Put or Call', name: 'optionType', hiddenName: 'optionType', valueField: 'value', displayField: 'value', tooltipField: 'tooltip', xtype: 'system-list-combo', listName: 'Investment Option Type'});
			currentItems.push({fieldLabel: 'Strike Price', name: 'optionStrikePrice', xtype: 'pricefield', qtip: 'The option exercise price.'});
			currentItems.push({fieldLabel: 'Option Style', name: 'optionStyle', hiddenName: 'optionStyle', valueField: 'value', displayField: 'value', tooltipField: 'tooltip', xtype: 'system-list-combo', listName: 'Investment Option Style'});
			currentItems.push({fieldLabel: 'Expiry Time', name: 'settlementExpiryTime', hiddenName: 'settlementExpiryTime', valueField: 'value', displayField: 'value', tooltipField: 'tooltip', xtype: 'system-list-combo', listName: 'Investment Expiry Time'});
		}

		if (TCG.getValue('investmentTypeSubType.name', hierarchy) === 'Participatory Notes') {
			Ext.each(currentItems, function(f) {
				if (f.name === 'businessCompany.name') {
					f.allowBlank = false;
					f.fieldLabel = 'Counterparty';
					f.qtip = 'The counterparty that issued this P-Note and that we are transacting with.';
				}
			});
		}

		// these fields should be displayed only when instrument.bigInstrument is set
		const bigId = TCG.getValue('instrument.bigInstrument.id', this.defaultData);
		if (TCG.isNotBlank(bigId)) {
			const bigInstrumentInactive = TCG.getValue('instrument.bigInstrument.inactive', this.defaultData);
			// Require a big security if big instrument is defined and active. Server side validation will catch cases where blank is mistakenly allowed.
			const requireBigSecurity = TCG.isNotBlank(bigInstrumentInactive) && TCG.isFalse(bigInstrumentInactive);
			currentItems.push({
				fieldLabel: 'Big Security', name: 'bigSecurity.label', hiddenName: 'bigSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
				allowBlank: !requireBigSecurity,
				beforequery: function(queryEvent) {
					queryEvent.combo.store.baseParams = {instrumentId: bigId};
				}
			});
		}

		// optional reference security
		if (TCG.isTrue(TCG.getValue('referenceSecurityAllowed', hierarchy))) {
			const investmentTypeSubType2 = TCG.getValue('investmentTypeSubType2.name', hierarchy);
			currentItems.push({
				fieldLabel: TCG.getValue('referenceSecurityLabel', hierarchy), name: 'referenceSecurity.label', hiddenName: 'referenceSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: true,
				qtip: TCG.getValue('referenceSecurityTooltip', hierarchy),
				beforequery: function(queryEvent) {
					const store = queryEvent.combo.store;
					store.baseParams = {};

					const formValues = TCG.getParentFormPanel(queryEvent.combo).getForm().formValues,
							active = TCG.getValue('active', formValues);
					if (TCG.isNotBlank(active)) {
						store.baseParams['active'] = active;
					}

					if (TCG.getValue('referenceSecurityFromSameInstrument', hierarchy)) {
						store.baseParams['instrumentId'] = TCG.getValue('instrument.id', formValues);
					}
					else {
						const underlyingSecurityId = TCG.getValue('underlyingSecurity.id', formValues);
						if (TCG.isNotNull(underlyingSecurityId)) {
							store.baseParams['underlyingSecurityId'] = underlyingSecurityId;
						}
						// If the subtype2 is flex or OTC Options filter for listed options. The result of match(regex) is null if no match.
						// ^ signals beginning of input, $ signals end of input, and the three options are or'd with |
						if (investmentTypeSubType2.match(/^(FLEX|Look Alike|Custom) Options$/)) {
							queryEvent.combo.store.baseParams['investmentTypeSubType2'] = 'Listed Options';
						}
					}
				}
			});
		}
		if (TCG.getValue('accrualSecurityEventType', hierarchy)) {
			currentItems.push(
					{fieldLabel: 'Accrual Method', name: 'accrualMethod', hiddenName: 'accrualMethod', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', store: {xtype: 'arraystore', fields: ['value', 'name', 'description'], data: Clifton.investment.instrument.AccrualMethods}},
					{fieldLabel: 'Accrual Method 2', name: 'accrualMethod2', hiddenName: 'accrualMethod2', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', store: {xtype: 'arraystore', fields: ['value', 'name', 'description'], data: Clifton.investment.instrument.AccrualMethods}}
			);
		}

		if (this.categorySpecificItems) {
			Ext.each(this.categorySpecificItems, function(f) {
				currentItems.push(f);
			});
		}
		return currentItems;
	},


	tabItems: [
		{
			title: 'Security',
			items: [{
				xtype: 'investment-security-forms-with-dynamic-fields',

				getSaveURL: function() {
					return this.getWindow().getSaveURL();
				},

				initComponent: function() {
					this.items = this.getWindow().getFormItems();
					Clifton.investment.instrument.SecurityFormWithDynamicFields.superclass.initComponent.call(this);
				},

				prepareDefaultData: function(defaultData) {
					this.configureRequiredDates(defaultData);
					return defaultData;
				},

				listeners: {
					afterload: function(form, isClosing) {
						this.configureRequiredDates(form.formValues);
					}
				},

				configureRequiredDates: function(s) {
					const type = TCG.getValue('instrument.hierarchy.investmentType.name', s);
					if (type === 'Futures' || type === 'Options') {
						const f = this.getForm();
						f.findField('startDate').allowBlank = false;
						f.findField('endDate').allowBlank = false;
					}
				}
			}]
		},


		{
			title: 'All Events',
			items: [{xtype: 'investment-security-events-by-security-grid'}]
		},


		{
			title: 'Notes',
			items: [{
				xtype: 'investment-security-system-note-grid'
			}]
		},


		{
			title: 'Tasks',
			items: [{
				xtype: 'workflow-task-grid',
				tableName: 'InvestmentSecurity'
			}]
		}
	]
});
