TCG.use('Clifton.investment.instrument.BaseInstrumentWindow');

/**
 * The instrument window for simple one-to-many instruments.
 */
Clifton.investment.instrument.OneToManySimpleWindow = Ext.extend(Clifton.investment.instrument.BaseInstrumentWindow, {
	excludedFields: ['bigInstrument.labelShort', 'm2mCalculator', 'accrualDateCalculator'],

	getAfterExtraFields: function() {
		const fields = [];
		fields.push(
				{fieldLabel: 'Deliverable', name: 'deliverable', xtype: 'checkbox', boxLabel: ' (Enables delivery specific date field(s) on Security screen)'},
				{fieldLabel: 'Inactive', name: 'inactive', xtype: 'checkbox', boxLabel: ' (Marks the instrument as inactive when checked)'}
		);
		if (TCG.getValue('hierarchy.accrualSecurityEventType', this.defaultData)) {
			fields.push(
					{fieldLabel: 'Accrual Method', name: 'accrualMethod', hiddenName: 'accrualMethod', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', store: {xtype: 'arraystore', fields: ['value', 'name', 'description'], data: Clifton.investment.instrument.AccrualMethods}},
					{fieldLabel: 'Accrual Date Calculator', name: 'accrualDateCalculator', hiddenName: 'accrualDateCalculator', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', store: {xtype: 'arraystore', fields: ['value', 'name', 'description'], data: Clifton.investment.instrument.AccrualDateCalculators}},
					{fieldLabel: 'Accrual Method 2', name: 'accrualMethod2', hiddenName: 'accrualMethod2', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', store: {xtype: 'arraystore', fields: ['value', 'name', 'description'], data: Clifton.investment.instrument.AccrualMethods}},
					{fieldLabel: 'Accrual Date Calculator 2', name: 'accrualDateCalculator2', hiddenName: 'accrualDateCalculator2', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', store: {xtype: 'arraystore', fields: ['value', 'name', 'description'], data: Clifton.investment.instrument.AccrualDateCalculators}}
			);
		}
		return fields;
	}
});
