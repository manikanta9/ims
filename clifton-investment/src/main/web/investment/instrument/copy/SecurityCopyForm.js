Clifton.investment.instrument.copy.SecurityCopyForm = Ext.extend(Clifton.investment.instrument.SecurityFormWithDynamicFields, {
	labelWidth: 140,

	// Need to be in a form fragment so reloads when there are "hrs" set up as columns
	// the screen just removes all and re-adds instead of trying to find and remove each field
	dynamicFieldFormFragment: 'securityCustomFields',

	// When Extended (OTC Swaps) and overriding copyItems - be sure to include copyFromSecurityId field
	// Can also specify columns to copy (By default, the security fields aren't copied) but all custom fields are
	copyFieldNames: [], // Override so when loading copy from these are the fields that are defaulted automatically based on the selected swap
	skipCopyCustomFieldNames: [], // Override to skip defaulting specific custom field values
	doNotCopyContract: false, // Override to skip defaulting the of the client field (businessContract)

	getSaveURL: function() {
		return 'investmentSecurityFromTemplateSave.json';
	},

	initComponent: function() {
		const currentItems = [];
		if (this.copyItems) {
			Ext.each(this.copyItems, function(f) {
				currentItems.push(f);
			});
		}
		if (this.isdaItems) {
			Ext.each(this.isdaItems, function(f) {
				currentItems.push(f);
			});
		}
		Ext.each(this.securityItems, function(f) {
			currentItems.push(f);
		});

		const dd = this.getWindow().defaultData;
		const hierarchy = TCG.getValue('instrument.hierarchy', dd);
		// optional reference security
		if (TCG.isTrue(TCG.getValue('referenceSecurityAllowed', hierarchy))) {
			const investmentTypeSubType2 = TCG.getValue('investmentTypeSubType2.name', hierarchy);
			currentItems.push({
				fieldLabel: TCG.getValue('referenceSecurityLabel', hierarchy), name: 'referenceSecurity.label', hiddenName: 'referenceSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: true,
				qtip: TCG.getValue('referenceSecurityTooltip', hierarchy),
				beforequery: function(queryEvent) {
					const store = queryEvent.combo.store;
					store.baseParams = {};

					const formValues = TCG.getParentFormPanel(queryEvent.combo).getForm().formValues,
						active = TCG.getValue('active', formValues);
					if (TCG.isNotBlank(active)) {
						store.baseParams['active'] = active;
					}

					if (TCG.getValue('referenceSecurityFromSameInstrument', hierarchy)) {
						store.baseParams['instrumentId'] = TCG.getValue('instrument.id', formValues);
					}
					else {
						const underlyingSecurityId = TCG.getValue('underlyingSecurity.id', formValues);
						if (TCG.isNotNull(underlyingSecurityId)) {
							store.baseParams['underlyingSecurityId'] = underlyingSecurityId;
						}
						// If the subtype2 is flex or OTC Options filter for listed options. The result of match(regex) is null if no match.
						// ^ signals beginning of input, $ signals end of input, and the three options are or'd with |
						if (investmentTypeSubType2.match(/^(FLEX|Look Alike|Custom) Options$/)) {
							queryEvent.combo.store.baseParams['investmentTypeSubType2'] = 'Listed Options';
						}
					}
				}
			});
		}

		// optional accrual fields
		if (TCG.isNotNull(TCG.getValue('accrualSecurityEventType', hierarchy))) {
			currentItems.push(
				{fieldLabel: 'Accrual Method', name: 'accrualMethod', hiddenName: 'accrualMethod', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', store: {xtype: 'arraystore', fields: ['value', 'name', 'description'], data: Clifton.investment.instrument.AccrualMethods}},
				{fieldLabel: 'Accrual Method 2', name: 'accrualMethod2', hiddenName: 'accrualMethod2', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', store: {xtype: 'arraystore', fields: ['value', 'name', 'description'], data: Clifton.investment.instrument.AccrualMethods}}
			);
		}

		// optional settlement currency
		if (TCG.isTrue(TCG.getValue('differentSettlementCurrencyAllowed', hierarchy))) {
			currentItems.push({
				fieldLabel: 'Settlement CCY', name: 'settlementCurrency.label', hiddenName: 'settlementCurrency.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: true,
				qtip: 'In rare cases of OTC securities, Settlement Currency for events and trades maybe different than Currency Denomination of security. Use this field to specify the default Settlement Currency. It can still be manually overridden for individual transactions.'
			});
		}

		if (this.customFieldItems) {
			Ext.each(this.customFieldItems, function(f) {
				currentItems.push(f);
			});
		}

		this.items = currentItems;

		Clifton.investment.instrument.SecurityFormWithDynamicFields.superclass.initComponent.apply(this, arguments);
	},

	listeners: {
		// After Creating - If not closing the window - switch to the real window
		// Right now this opens new window and closes this one -
		// Not sure the best way to just switch this window
		aftercreate: function(panel, closeOnSuccess) {
			// the entity was already retrieved: pass it to the window and instruct not to get it again
			const config = {
				defaultDataIsReal: true,
				defaultData: panel.getForm().formValues,
				openerCt: panel.ownerCt.openerCt
			};
			if (this.securityDefaultActiveTab) {
				config.defaultActiveTab = this.securityDefaultActiveTab;
			}
			TCG.createComponent('Clifton.investment.instrument.SecurityWindow', config);
		}
	},


	prepareDefaultData: function(dd) {
		if (dd && dd.copyFromSecurityId && dd.copyFromSecurityId !== '') {
			const sec = TCG.data.getData('investmentSecurity.json?id=' + dd.copyFromSecurityId, this);
			dd.copyFromSecurityIdLabel = sec.label;
			dd.businessCompany = sec.businessCompany;

			if (!this.doNotCopyContract) {
				dd.businessConract = sec.businessContract;
			}

			if (this.copyFieldNames && this.copyFieldNames.length > 0) {
				for (let i = 0; i < this.copyFieldNames.length; i++) {
					const fldName = this.copyFieldNames[i];
					dd[fldName] = sec[fldName];
				}
			}

			// Set so it loads custom fields
			this.setFormValue('copyFromSecurityId', sec.id);
			this.setFormValue('instrument.hierarchy.id', sec.instrument.hierarchy.id);
			this.updateCopyFromValues(sec, true);
		}
		return dd;
	},


	updateCopyFromValues: function(sec) {
		const fp = this;
		if (!sec) {
			fp.setFormValue('copyFromSecurityId', '', true);
			if (fp.getForm().findField('copySymbolOverrides')) {
				fp.setFormValue('copySymbolOverrides', false, true);
				fp.getForm().findField('copySymbolOverrides').setDisabled(true);
			}
			if (fp.getForm().findField('copyEvents')) {
				fp.setFormValue('copyEvents', false, true);
				fp.getForm().findField('copyEvents').setDisabled(true);
			}

		}
		else {
			fp.setFormValue('copyFromSecurityId', sec.id, true);
			if (fp.getForm().findField('copySymbolOverrides')) {
				fp.getForm().findField('copySymbolOverrides').setDisabled(false);
			}
			if (fp.getForm().findField('copyEvents')) {
				fp.getForm().findField('copyEvents').setDisabled(false);
			}

		}

		if (fp.getForm().findField('accrualMethod') && !this.copyFieldNames.includes('accrualMethod')) {
			this.copyFieldNames.push('accrualMethod', 'accrualMethod2');
		}

		if (this.copyFieldNames && this.copyFieldNames.length > 0) {
			for (let i = 0; i < this.copyFieldNames.length; i++) {
				const fldName = this.copyFieldNames[i];
				if (sec[fldName] && sec[fldName] !== '') {
					if (fldName.indexOf('Date') > 0) {
						fp.setFormValue(fldName, TCG.parseDate(sec[fldName]).format('m/d/Y'), true);
					}
					else {
						fp.setFormValue(fldName, sec[fldName]);
					}
				}
			}
		}

		if (this.isdaItems) {
			Ext.each(this.isdaItems, function(f) {
				// Fields
				if (f.name) {
					const fld = fp.getForm().findField(f.name);
					if (sec.instrument.hierarchy.otc === false) {
						fld.reset();
						fld.allowBlank = true;
						fld.setVisible(false);
					}
					else {
						fld.setVisible(true);
					}
				}
			});
		}
		fp.resetDynamicFields(fp.getForm().findField('instrument.hierarchy.id'), false);
	},

	// Load Dynamic Field Values from copied security
	getEntityId: function() {
		return this.getForm().findField('copyFromSecurityId').getValue();
	},

	// call-back because get is asynchronous
	// Overridden to allow skipping loading copy from values for specific fields
	getDynamicFields: function(triggerField, callback) {
		const fp = this;
		const params = {};
		params['linkedValue'] = this.getDynamicTriggerFieldValue();
		params['columnGroupName'] = fp.columnGroupName;
		params['entityId'] = fp.getEntityId();
		const loader = new TCG.data.JsonLoader({
			waitTarget: fp,
			waitMsg: 'Loading fields...',
			params: params,
			onLoad: function(record, conf) {
				if (record.columnValueList && record.columnValueList.length > 0 && fp.skipCopyCustomFieldNames && fp.skipCopyCustomFieldNames.length > 0) {
					fp.fieldValueList = [];
					for (let i = 0; i < record.columnValueList.length; i++) {
						let skip = false;
						const fieldValue = record.columnValueList[i];
						for (let j = 0; j < fp.skipCopyCustomFieldNames.length; j++) {
							if (fieldValue.column.name === fp.skipCopyCustomFieldNames[j]) {
								skip = true;
								break;
							}
						}
						if (skip === false) {
							fp.fieldValueList.push(fieldValue);
						}
					}
				}
				else {
					fp.fieldValueList = record.columnValueList;
				}
				const fieldRecords = record.columnList;
				if (fieldRecords) {
					const fields = [];
					for (let i = 0; i < fieldRecords.length; i++) {
						record = fieldRecords[i];
						const field = fp.getDynamicFieldFromRecord(fp, record, i);
						if (field) {
							fields.push(field);
						}
					}

					callback.apply(fp, [fields]);
				}
			}
		});
		loader.load(this.dynamicFieldsUrl);
	},
	defaultOtcNameAndSymbolFieldValue: function() {
		const fp = this;
		if (fp && TCG.isTrue(fp.defaultOtcName)) {
			const prefix = fp.getFormValue('instrument.identifierPrefix');
			const counterPartyAbv = fp.getFormValue('businessCompany.abbreviation', true);
			const clientAbv = fp.getFormValue('businessContract.company.abbreviation', true);
			if (TCG.isNotBlank(prefix) && TCG.isNotBlank(counterPartyAbv) && TCG.isNotBlank(clientAbv)) {
				const namePrePop = prefix + '-' + counterPartyAbv + '-' + clientAbv;
				fp.setFormValue('name', namePrePop, true);
			}
		}
		const symbolField = fp.getForm().findField('symbol');
		if (symbolField) {
			const currentValue = symbolField.getValue();
			let newValue = 'TBD-';
			if (TCG.isTrue(fp.defaultOtcName)) {
				newValue += fp.getFormValue('name', true);
			}
			if (TCG.isBlank(currentValue) || currentValue !== newValue) {
				symbolField.setValue(newValue);
			}
		}
	},
	copyItems: [
		{
			xtype: 'fieldset',
			title: 'Copy From (Template)',
			instructions: 'Defaults will be populated based on the selected instrument.  One-to-One securities will create both a new instrument/security.  One-to-Many securities will create the new security under the existing instrument.',
			labelWidth: 130,

			items: [
				// If passed used to filter the to specific instrument group (populated from trading window)
				{hidden: true, name: 'investmentGroupId', submitValue: false},
				{hidden: true, name: 'investmentTypeId', submitValue: false},

				// Populated from instrument/hierarchy
				{hidden: true, name: 'hierarchyId', submitValue: false},

				{fieldLabel: 'Investment Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
				{fieldLabel: 'Investment Instrument', name: 'instrument.labelShort', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'instrument.id'},
				{
					fieldLabel: 'Security', xtype: 'combo', name: 'copyFromSecurityIdLabel', hiddenName: 'copyFromSecurityId', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', displayField: 'label',
					allowBlank: false, // required to select one, if not need to add an instrument selection so for new ones we know which instrument/hierarchy to put it under
					requestedProps: 'startDate|endDate|instrument.hierarchy.labelExpanded|instrument.hierarchy.id|instrument.labelShort|instrument.id',
					listeners: {
						beforequery: function(queryEvent) {
							const f = queryEvent.combo.getParentForm().getForm();
							const params = {};
							// if a specific instrument was passed to this window - use it - otherwise filter on hierarchy/group/type whatever has been set.
							const dd = queryEvent.combo.getParentForm().getWindow().defaultData;
							if (dd && dd.instrument && dd.instrument.id) {
								params.instrumentId = dd.instrument.id;
							}
							params.hierarchyId = f.findField('hierarchyId').value;
							params.investmentGroupId = f.findField('investmentGroupId').value;
							params.investmentTypeId = f.findField('investmentTypeId').value;
							queryEvent.combo.store.baseParams = params;
						},
						select: function(combo, record, index) {
							// On Select - Attempt to Load all defaults
							const fp = combo.getParentForm();
							fp.setFormValue('instrument.hierarchy.labelExpanded', record.json.instrument.hierarchy.labelExpanded, true);
							fp.setFormValue('instrument.hierarchy.id', record.json.instrument.hierarchy.id, true);

							fp.setFormValue('instrument.labelShort', record.json.instrument.labelShort, true);
							fp.setFormValue('instrument.id', record.json.instrument.id, true);

							fp.updateCopyFromValues(record.json);
						}
					}
				}
			]
		}
	],
	securityItems: [
		{fieldLabel: 'Ticker Symbol', name: 'symbol'},
		{fieldLabel: 'CUSIP', name: 'cusip'},
		{fieldLabel: 'Security Name', name: 'name'},
		{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
		{boxLabel: 'Illiquid (cannot be easily sold or exchanged for cash)', name: 'illiquid', xtype: 'checkbox'},

		{xtype: 'label', html: '<hr/>'},
		{fieldLabel: 'First Trade', name: 'startDate', xtype: 'datefield'},
		{fieldLabel: 'Last Trade', name: 'endDate', xtype: 'datefield'}
	],

	customFieldItems: [
		{xtype: 'label', html: '<hr/>'},
		{
			xtype: 'formfragment',
			frame: false,
			labelWidth: 140,
			name: 'securityCustomFields',
			items: []
		}
	],

	isdaItems: [
		{xtype: 'hidden', name: 'businessContract.company.abbreviation', submitValue: false},
		{
			fieldLabel: 'Client', name: 'businessContract.company.name', hiddenName: 'businessContract.company.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow', url: 'businessCompanyListFind.json?companyType=Client', submitValue: false, disableAddNewItem: true,
			requestedProps: 'abbreviation',
			listeners: {
				select: async function(combo, record, index) {
					const fp = combo.getParentForm();

					fp.setFormValue('businessContract.company.abbreviation', TCG.getValue('abbreviation', record.json));

					const counterpartyField = fp.getForm().findField('businessCompany.name');
					const counterpartyId = counterpartyField.getValue();

					if (counterpartyId) {
						const role = await TCG.data.getDataPromiseUsingCaching('businessContractPartyRoleByName.json?name=Counterparty', combo, 'business.contract.party.role.Counterparty');

						const contractList = await TCG.data.getDataPromise('businessContractListFind.json', fp, {
							params: {
								companyId: record.json.id,
								contractTypeNames: ['ISDA', 'ISDA Amendment'],
								contractPartyRoleName: 'Counterparty',
								partyCompanyId: counterpartyId,
								partyRoleId: role.id,
								active: true
							}
						});
						const l = contractList.length || 0;
						const contractField = fp.getForm().findField('businessContract.label');
						if (l < 1) {
							counterpartyField.clearAndReset();
							contractField.clearAndReset();
						}
						else if (l === 1) {
							const isda = contractList[0];
							contractField.setValue({value: isda.id, text: isda.label});
							contractField.resetStore();
							counterpartyField.resetStore();
						}
						else {
							const isdaContracts = contractList ? contractList.filter(i => i.contractType.name === 'ISDA') : [];
							if (isdaContracts.length === 1) {
								const isda = isdaContracts[0];
								contractField.setValue({value: isda.id, text: isda.label});
								contractField.resetStore();
							}
							else {
								contractField.clearAndReset();
							}
							counterpartyField.resetStore();
						}
					}

					//Prepopulate name field
					fp.defaultOtcNameAndSymbolFieldValue();
				}
			}
		},
		{xtype: 'hidden', name: 'businessCompany.abbreviation', submitValue: false},
		{
			fieldLabel: 'Counterparty', name: 'businessCompany.name', hiddenName: 'businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourCompany=false', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true,
			requiredFields: ['businessContract.company.id'], doNotClearIfRequiredChanges: true,
			beforequery: function(queryEvent) {
				const fp = queryEvent.combo.getParentForm();
				// Client Company
				const clientCompanyId = fp.getFormValue('businessContract.company.id', true);
				const params = {contractCompanyId: clientCompanyId};

				// ISDA Contract Types
				params.contractTypeNames = ['ISDA', 'ISDA Amendment'];

				// Counterparty Role
				params.contractPartyRoleName = 'Counterparty';

				queryEvent.combo.store.baseParams = params;
			},
			listeners: {
				select: function(combo, record, index) {
					// On Select - Attempt to Auto Load the correct ISDA
					const fp = combo.getParentForm();

					fp.setFormValue('businessCompany.abbreviation', TCG.getValue('abbreviation', record.json));

					// Client Company
					const clientCompanyId = fp.getFormValue('businessContract.company.id', true);
					const params = {companyId: clientCompanyId, active: true};

					// ISDA Contract Type
					params.contractTypeNames = ['ISDA', 'ISDA Amendment'];

					// Counterparty Company
					params.partyCompanyId = record.json.id;

					// Counterparty Role
					const role = TCG.data.getData('businessContractPartyRoleByName.json?name=Counterparty', combo, 'business.contract.party.role.Counterparty');
					params.partyRoleId = role.id;

					const loader = new TCG.data.JsonLoader({
						params: params,
						onLoad: function(record, conf) {
							const l = record.length || 0;
							const contractField = fp.getForm().findField('businessContract.id');
							if (l === 0) {
								contractField.clearAndReset();
								TCG.showError('Cannot find any ISDA Contracts that match selected Client and Counterparty.', 'Invalid Setup');
							}
							else if (l === 1) {
								const isda = record[0];
								fp.setFormValue('businessContract.id', {value: isda.id, text: isda.label}, true);
								contractField.resetStore();
							}
							else {
								const isdaContracts = record ? record.filter(i => i.contractType.name === 'ISDA') : [];
								if (isdaContracts.length === 1) {
									const isda = isdaContracts[0];
									contractField.setValue({value: isda.id, text: isda.label});
									contractField.resetStore();
								}
								else {
									contractField.clearAndReset();
								}
							}
						}
					});
					loader.load('businessContractListFind.json', fp);

					//Prepopulate name field
					fp.defaultOtcNameAndSymbolFieldValue();
				}
			}
		},
		{
			fieldLabel: 'ISDA', name: 'businessContract.label', hiddenName: 'businessContract.id', xtype: 'combo', url: 'businessContractListFind.json', displayField: 'label', tooltipField: 'label', detailPageClass: 'Clifton.business.contract.ContractWindow', disableAddNewItem: true,
			allowBlank: false,
			requiredFields: ['businessCompany.id'],
			beforequery: function(queryEvent) {
				const fp = queryEvent.combo.getParentForm();
				// Client Company
				const clientCompanyId = fp.getFormValue('businessContract.company.id', true);
				const params = {companyId: clientCompanyId, active: true};

				// ISDA Contract Types
				params.contractTypeNames = ['ISDA', 'ISDA Amendment'];

				// Counterparty Company
				params.partyCompanyId = fp.getFormValue('businessCompany.id', true);

				// Counterparty Role
				const role = TCG.data.getData('businessContractPartyRoleByName.json?name=Counterparty', queryEvent.combo, 'business.contract.party.role.Counterparty');
				params.partyRoleId = role.id;

				queryEvent.combo.store.baseParams = params;
			},
			listeners: {
				select: function(combo, record, index) {
					const fp = combo.getParentForm();

					fp.setFormValue('businessContract.company.abbreviation', TCG.getValue('company.abbreviation', record.json));

					//Prepopulate name field and symbol field
					fp.defaultOtcNameAndSymbolFieldValue();
				}
			}
		},
		{xtype: 'label', html: '<hr/>'}
	]

});
Ext.reg('investment-security-copy-form', Clifton.investment.instrument.copy.SecurityCopyForm);
