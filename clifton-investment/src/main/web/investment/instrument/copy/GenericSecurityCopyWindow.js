TCG.use('Clifton.investment.instrument.copy.SecurityCopyForm');

Clifton.investment.instrument.copy.GenericSecurityCopyWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Create New Security from Template',
	iconCls: 'stock-chart',
	height: 600,

	items: [{
		xtype: 'investment-security-copy-form'
	}]
});




