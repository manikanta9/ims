// not the actual window but a window selector:
//   - if override for the hierarchy name - uses that copy window
//   - the rest get generic security copy window
Clifton.investment.instrument.copy.SecurityCopyWindow = Ext.extend(TCG.app.DetailWindowSelector, {

	getClassName: function(config, entity) {
		let h = null;
		const i = config.defaultData.instrument;
		if (i && i.hierarchy) {
			h = i.hierarchy;
		}
		let addFromTemplateClass = undefined;
		if (h && h.investmentTypeSubType) {
			addFromTemplateClass = Clifton.investment.instrument.copy.SecurityHierarchyCopyWindowClassMap[h.investmentTypeSubType.name];
		}

		const investmentTypeName = (h && h.investmentType) ? h.investmentType.name : config.defaultData.investmentTypeName;
		if (!addFromTemplateClass && TCG.isNotNull(investmentTypeName)) {
			addFromTemplateClass = Clifton.investment.instrument.copy.SecurityHierarchyCopyWindowClassMap[investmentTypeName];
		}
		if (!addFromTemplateClass) {
			addFromTemplateClass = 'Clifton.investment.instrument.copy.GenericSecurityCopyWindow';
		}
		return addFromTemplateClass;
	}
});
