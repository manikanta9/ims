Clifton.investment.instrument.copy.OptionCopyWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Create New Option From Template',
	iconCls: 'stock-chart',
	height: 375,
	allowOpenFromModal: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'Enter a strike price to create a new option under same instrument with specified strike price. All information for the selected option will be copied to the new option, with the exception of the strike price which will also be updated in the symbol, name, and description of the security. Specifying non-required fields will override the value from the copied option.',
		getSaveURL: function() {
			return 'investmentSecurityOptionWithCommandSave.json';
		},
		prepareDefaultData: async function(dd) {
			// leaving this for backward compatibility
			const securityId = dd.copyFromSecurityId || TCG.getValue('securityToCopy.id', dd);
			if (dd && !TCG.isBlank(securityId) && TCG.isBlank(TCG.getValue('securityToCopy.label', dd))) {
				dd.securityToCopy = await TCG.data.getDataPromise('investmentSecurity.json?id=' + securityId, this);
				dd.expirationDate = dd.securityToCopy.lastDeliveryDate;
			}
			else if (TCG.isNotBlank(TCG.getValue('securityToCopy.lastDeliveryDate', dd))) {
				dd.expirationDate = dd.securityToCopy.lastDeliveryDate;
			}
			return dd;
		},

		afterRenderPromise: function(formPanel) {
			const hierarchy = formPanel.getFormValue('instrument.hierarchy');
			if (TCG.isTrue(TCG.getValue('referenceSecurityAllowed', hierarchy))) {
				const investmentTypeSubType2 = TCG.getValue('investmentTypeSubType2.name', hierarchy);
				formPanel.add({
					fieldLabel: TCG.getValue('referenceSecurityLabel', hierarchy), name: 'referenceSecurity.label', hiddenName: 'referenceSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: true,
					qtip: TCG.getValue('referenceSecurityTooltip', hierarchy),
					beforequery: function(queryEvent) {
						const store = queryEvent.combo.store;
						store.baseParams = {active: true};

						const formValues = TCG.getParentFormPanel(queryEvent.combo).getForm().formValues;

						if (TCG.getValue('referenceSecurityFromSameInstrument', hierarchy)) {
							store.baseParams['instrumentId'] = TCG.getValue('instrument.id', formValues);
						}
						else {
							const underlyingSecurityId = TCG.getValue('securityToCopy.underlyingSecurity.id', formValues);
							if (TCG.isNotNull(underlyingSecurityId)) {
								store.baseParams['underlyingSecurityId'] = underlyingSecurityId;
							}
							// If the subtype2 is flex or OTC Options filter for listed options. The result of match(regex) is null if no match.
							// ^ signals beginning of input, $ signals end of input, and the three options are or'd with |
							if (investmentTypeSubType2.match(/^(FLEX|Look Alike|Custom) Options$/)) {
								queryEvent.combo.store.baseParams['investmentTypeSubType2'] = 'Listed Options';
							}
						}
					},
					getDetailPageClass: function(newInstance) {
						if (newInstance) {
							if (!this.getParentForm().getForm().findField('referenceSecurity.id').value) {
								TCG.showError('You must first select an option to copy', 'New Option');
								return false;
							}
							return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
						}
						return this.detailPageClass;
					},
					getDefaultData: function(f) { // defaults client/counterparty/isda
						let fp = TCG.getParentFormPanel(this);
						fp = fp.getForm();
						const secId = fp.findField('referenceSecurity.id').value;
						return Promise.resolve(secId)
							.then(id => TCG.data.getDataPromise('investmentSecurity.json', this, {params: {id: id}}))
							.then(securityToCopy => ({
								instrument: securityToCopy.instrument,
								securityToCopy: securityToCopy,
								optionType: fp.findField('optionType').value
							}));
					}
				});
				formPanel.doLayout();
			}
		},

		items: [
			{fieldLabel: 'Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
			{fieldLabel: 'Instrument', name: 'instrument.labelShort', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'instrument.id'},
			{
				fieldLabel: 'Security', xtype: 'combo', name: 'securityToCopy.label', hiddenName: 'securityToCopy.id', disableAddNewItem: true,
				url: 'investmentSecurityListFind.json?investmentType=Options&active=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', displayField: 'label',
				allowBlank: false, // required to select one, if not need to add an instrument selection so for new ones we know which instrument/hierarchy to put it under
				requestedProps: 'instrument.hierarchy.labelExpanded|instrument.hierarchy.id|instrument.labelShort|instrument.id',
				listeners: {
					beforeQuery: function(qEvent) {
						const f = qEvent.combo.getParentForm().getForm();
						const instId = f.findField('instrument.id').getValue();
						const optionType = f.findField('optionType').getValue();
						const params = {instrumentId: instId};
						if (optionType) {
							params.optionType = optionType;
						}
						qEvent.combo.store.baseParams = params;
					},
					select: function(combo, record, index) {
						// On Select - Change Hierarchy and Instrument Selection
						const fp = combo.getParentForm();
						fp.setFormValue('instrument.hierarchy.labelExpanded', record.json.instrument.hierarchy.labelExpanded, true);
						fp.setFormValue('instrument.hierarchy.id', record.json.instrument.hierarchy.id, true);
						fp.setFormValue('instrument.labelShort', record.json.instrument.labelShort, true);
						fp.setFormValue('instrument.id', record.json.instrument.id, true);
					}
				}
			},
			{fieldLabel: 'Strike Price', name: 'strikePrice', xtype: 'floatfield', allowBlank: false},
			{fieldLabel: 'Expiration', name: 'expirationDate', xtype: 'datefield', allowBlank: false},
			{fieldLabel: 'Option Type', name: 'optionType', xtype: 'system-list-combo', valueField: 'value', displayField: 'value', listName: 'Investment Option Type', allowBlank: true, submitValue: true},
			{fieldLabel: 'Option Style', hiddenName: 'optionStyle', xtype: 'system-list-combo', valueField: 'value', displayField: 'value', listName: 'Investment Option Style', allowBlank: true, submitValue: true},
			{fieldLabel: 'Option Expiry Time', hiddenName: 'settlementTime', xtype: 'system-list-combo', valueField: 'value', displayField: 'value', listName: 'Investment Expiry Time', allowBlank: true, submitValue: true}
		]
	}]
});



