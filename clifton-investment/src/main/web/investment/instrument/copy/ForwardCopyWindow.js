Clifton.investment.instrument.copy.ForwardCopyWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Create New Forward From Template',
	iconCls: 'stock-chart',
	height: 375,
	allowOpenFromModal: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'Use the form below to create a new forward. All information for the selected Security will be copied to the new forward, with the exception of the fields below the selected Security.',
		getSaveURL: function() {
			return 'investmentSecurityForwardWithCommandSave.json';
		},
		prepareDefaultData: async function(dd) {
			// leaving this for backward compatibility
			const securityId = dd.copyFromSecurityId || TCG.getValue('securityToCopy.id', dd);
			if (dd && !TCG.isBlank(securityId) && TCG.isBlank(TCG.getValue('securityToCopy.label', dd))) {
				dd.securityToCopy = await TCG.data.getDataPromise('investmentSecurity.json?id=' + securityId, this);
				dd.expirationDate = dd.securityToCopy.lastDeliveryDate;
			}
			else if (TCG.isNotBlank(TCG.getValue('securityToCopy.lastDeliveryDate', dd))) {
				dd.expirationDate = dd.securityToCopy.lastDeliveryDate;
			}
			if (dd && TCG.isBlank(TCG.getValue('startDate', dd))) {
				dd.startDate = new Date().add(Date.DAY, -1).format('Y-m-d 00:00:00');
			}
			return dd;
		},

		afterRenderPromise: function(formPanel) {
			formPanel.updateFixingDateField();
		},

		updateFixingDateField: function() {
			const formPanel = this;
			const securitySubType = formPanel.getForm().findField('instrument.hierarchy.investmentTypeSubType.name').value;
			const nonDeliverableForward = TCG.isNotBlank(securitySubType) && securitySubType === 'Non Deliverable Forwards';
			const fixingDateField = formPanel.getForm().findField('endDate');
			if (!nonDeliverableForward) {
				fixingDateField.setValue('');
			}
			fixingDateField.allowBlank = !nonDeliverableForward;
			formPanel.setHidden(fixingDateField, !nonDeliverableForward);
		},

		items: [
			{fieldLabel: 'Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
			{
				fieldLabel: 'Instrument', name: 'instrument.labelShort', hiddenName: 'instrument.id', xtype: 'combo', url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'instrument.id',
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const fp = TCG.getParentFormPanel(combo);
					const instrument = fp.getFormValue('instrument');
					const givenCCYId = instrument.underlyingInstrument.tradingCurrency.id;
					const settleCCYId = instrument.tradingCurrency.id;
					combo.store.baseParams = {
						hierarchyId: instrument.hierarchy.id,
						underlyingTradingCurrencyIds: [givenCCYId, settleCCYId],
						tradingCurrencyIds: [givenCCYId, settleCCYId],
						inactive: false
					};
				},
				listeners: {
					select: function(combo) {
						const fp = TCG.getParentFormPanel(combo);
						const securityCombo = TCG.getChildByName(fp, 'securityToCopy.label');
						securityCombo.clearAndReset();
					}
				}
			},
			{
				fieldLabel: 'Security', xtype: 'combo', name: 'securityToCopy.label', hiddenName: 'securityToCopy.id', disableAddNewItem: true,
				url: 'investmentSecurityListFind.json?investmentType=Forwards&active=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', displayField: 'label',
				allowBlank: false, // required to select one, if not need to add an instrument selection so for new ones we know which instrument/hierarchy to put it under
				requestedProps: 'instrument.hierarchy.labelExpanded|instrument.hierarchy.id|instrument.labelShort|instrument.id|instrument.hierarchy.investmentTypeSubType.name',
				listeners: {
					beforeQuery: function(qEvent) {
						const f = qEvent.combo.getParentForm().getForm();
						const instId = f.findField('instrument.id').getValue();
						const params = {instrumentId: instId};
						qEvent.combo.store.baseParams = params;
					},
					select: function(combo, record, index) {
						// On Select - Change Hierarchy and Instrument Selection
						const fp = combo.getParentForm();
						fp.setFormValue('instrument.hierarchy.labelExpanded', record.json.instrument.hierarchy.labelExpanded, true);
						fp.setFormValue('instrument.hierarchy.id', record.json.instrument.hierarchy.id, true);
						fp.setFormValue('instrument.labelShort', record.json.instrument.labelShort, true);
						fp.setFormValue('instrument.id', record.json.instrument.id, true);
						fp.setFormValue('instrument.hierarchy.investmentTypeSubType.name', record.json.instrument.hierarchy.investmentTypeSubType.name, true);
						fp.updateFixingDateField();
					}
				}
			},
			{fieldLabel: 'Security Sub Type', name: 'instrument.hierarchy.investmentTypeSubType.name', hidden: true},
			{fieldLabel: 'First Trade', name: 'startDate', xtype: 'datefield', allowBlank: false},
			{fieldLabel: 'Fixing Date', name: 'endDate', xtype: 'datefield', allowBlank: false},
			{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', allowBlank: false}
		]
	}]
});



