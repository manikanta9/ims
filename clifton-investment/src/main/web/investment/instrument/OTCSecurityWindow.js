TCG.use('Clifton.investment.instrument.BaseSecurityDetailWindow');

// used by OTC Options and Swaptions
Clifton.investment.instrument.OTCSecurityWindow = Ext.extend(Clifton.investment.instrument.BaseSecurityDetailWindow, {
	titlePrefix: 'OTC Security',
	iconCls: 'coins',
	height: 650,
	width: 650,
	enableRefreshWindow: true,

	securityType: 'OTC',

	tabItems: [
		{
			title: 'Security',
			items: [{
				xtype: 'investment-security-otc-forms-with-dynamic-fields'
			}]
		},


		{
			title: 'Events',
			items: [{
				name: 'investmentSecurityEventListFind',
				xtype: 'gridpanel',
				instructions: 'A security event represents an instance of a specific event type for a specific security. For example, specific stock split, dividend payment, etc .',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Type', width: 100, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}},
					{header: 'Additional Security', width: 100, dataIndex: 'additionalSecurity.label', filter: {type: 'combo', searchFieldName: 'additionalSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
					{header: 'Before Value', width: 60, dataIndex: 'beforeEventValue', type: 'float'},
					{header: 'After Value', width: 60, dataIndex: 'afterEventValue', type: 'float'},
					{header: 'Declare Date', width: 50, dataIndex: 'declareDate', hidden: true},
					{header: 'Ex Date', width: 50, dataIndex: 'exDate', hidden: true},
					{header: 'Record Date', width: 50, dataIndex: 'recordDate', hidden: true},
					{header: 'Event Date', width: 50, dataIndex: 'eventDate'}
				],
				editor: {
					detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow',
					getDefaultData: function(gridPanel) {
						return this.savedDefaultData;
					},
					addToolbarAddButton: function(toolBar) {
						const gridPanel = this.getGridPanel();
						toolBar.add(new TCG.form.ComboBox({name: 'eventType', url: 'investmentSecurityEventTypeListFind.json', width: 150, listWidth: 230}));
						toolBar.add({
							text: 'Add',
							tooltip: 'Add an event of selected type to this security',
							iconCls: 'add',
							scope: this,
							handler: function() {
								const eventType = TCG.getChildByName(toolBar, 'eventType');
								const eventTypeId = eventType.getValue();
								if (eventTypeId === '') {
									TCG.showError('You must first select desired Event Type from the list.');
								}
								else {
									this.savedDefaultData = {
										security: gridPanel.getWindow().getMainForm().formValues,
										type: {
											id: eventTypeId,
											name: eventType.lastSelectionText
										}
									};
									this.openDetailPage(this.getDetailPageClass(), gridPanel);
								}
							}
						});
						toolBar.add('-');
					}
				},
				getLoadParams: function() {
					return {'securityId': this.getWindow().getMainFormId()};
				}
			}]
		},


		{
			title: 'Symbol Overrides',
			items: [{xtype: 'marketdata-datasource-security-override-grid'}]
		},


		{
			title: 'Notes',
			items: [{
				xtype: 'investment-security-system-note-grid'
			}]
		},


		{
			title: 'Instrument Groups',
			layout: 'border',
			items: [
				{
					xtype: 'investment-instrumentGroupGrid_ByInstrument',
					region: 'north',
					height: 250,
					securityWindow: true
				},
				{
					xtype: 'investment-instrumentGroupMissingGrid_ByInstrument',
					region: 'center',
					securityWindow: true
				}
			]
		},


		{
			title: 'Tasks',
			items: [{
				xtype: 'workflow-task-grid',
				tableName: 'InvestmentSecurity'
			}]
		}
	]
});
