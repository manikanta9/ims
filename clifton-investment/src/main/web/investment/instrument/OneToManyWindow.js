TCG.use('Clifton.investment.instrument.BaseInstrumentWindow');

/**
 * The instrument window for one-to-many instruments.
 */
Clifton.investment.instrument.OneToManyWindow = Ext.extend(Clifton.investment.instrument.BaseInstrumentWindow, {
	excludedFields: ['groupName'],

	// One-to-many-specific fields
	afterExtraFields: [
		{
			fieldLabel: 'Spot Month Calculator', name: 'spotMonthCalculatorBean.name', hiddenName: 'spotMonthCalculatorBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Investment Security Spot Month Calculator',
			qtip: 'Spot Month is a time period until last delivery or expiration date of the the contract.  Used for Exchange Limits as we enter the Spot month our limits are generally reduced so we start trading out of that contract and into a new one.',
			detailPageClass: 'Clifton.system.bean.BeanWindow',
			getDefaultData: function() {
				return {type: {group: {name: 'Investment Security Spot Month Calculator', alias: 'Spot Month Calculator'}}};
			}
		},
		{fieldLabel: 'Deliverable', name: 'deliverable', xtype: 'checkbox', boxLabel: ' (Enables first notice and delivery dates on Security screen)'},
		{fieldLabel: 'Discount Note', name: 'discountNote', xtype: 'checkbox'},
		{fieldLabel: 'Inactive', name: 'inactive', xtype: 'checkbox', boxLabel: ' (Marks the instrument as inactive when checked)'},

		{xtype: 'label', html: '<hr/>'},
		{
			fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
			defaults: {
				xtype: 'displayfield',
				flex: 1
			},
			items: [
				{value: 'Speculator'},
				{value: 'Hedger'}
			]
		},
		{
			fieldLabel: 'Initial Margin', xtype: 'compositefield',
			defaults: {
				xtype: 'currencyfield',
				flex: 1
			},
			items: [
				{name: 'speculatorInitialMarginPerUnit'},
				{name: 'hedgerInitialMarginPerUnit'}
			]
		},
		{
			fieldLabel: 'Maintenance Margin', xtype: 'compositefield',
			defaults: {
				xtype: 'currencyfield',
				flex: 1
			},
			items: [
				{name: 'speculatorSecondaryMarginPerUnit'},
				{name: 'hedgerSecondaryMarginPerUnit'}
			]
		}
	]
});
