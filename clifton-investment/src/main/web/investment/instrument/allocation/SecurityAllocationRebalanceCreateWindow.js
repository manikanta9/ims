Clifton.investment.instrument.allocation.SecurityAllocationRebalanceCreateWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Rebalance Security Allocations',
	iconCls: 'coins',

	okButtonText: 'Rebalance',
	okButtonTooltip: 'Rebalance Security using selected options',

	items: [{
		xtype: 'formpanel',
		instructions: 'Rebalancing security allocations will end all active allocations on the start date - 1, and will create new allocations using the base date allocations. If left blank, the system will use the most recent rebalance available.  You can optionally keep base date current weights and have the system re-calculate shares, or keep shares and recalculate weights.  If the system is to re-calculate shares, a starting price can be supplied, but if not supplied, the system will use the pre-calculated price on the start date.  If missing, will default to 100.',
		getSaveURL: function() {
			return 'investmentSecurityAllocationRebalanceProcess.json';
		},
		items: [
			{xtype: 'hidden', name: 'securityId'},
			{fieldLabel: 'Security', name: 'securityLabel', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'securityId'},
			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', allowBlank: false},
			{
				fieldLabel: 'Base Date', name: 'baseDate', xtype: 'datefield',
				qtip: 'If left blank, will default to the base weights from the last rebalance date.  If selected, will use the <b>Current</b> weights on the selected date.'
			},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'useBaseWeights', allowBlank: false,
				items: [
					{boxLabel: 'Rebalance using weights on selected Base Date', xtype: 'radio', name: 'useBaseWeights', inputValue: true},
					{boxLabel: 'Rebalance using shares on selected Base Date', xtype: 'radio', name: 'useBaseWeights', inputValue: false}
				],
				listeners: {
					change: function(rg, r) {
						const p = TCG.getParentFormPanel(rg);
						const startingPriceField = p.getForm().findField('startingPrice');
						startingPriceField.setDisabled(r.inputValue !== true);
					}
				}
			},
			{
				fieldLabel: 'Starting Price', name: 'startingPrice', xtype: 'floatfield',
				qtip: 'If left blank, will default to the calculated security price on the start date.  If not available, will default to 100.'
			}
		]
	}]
});
