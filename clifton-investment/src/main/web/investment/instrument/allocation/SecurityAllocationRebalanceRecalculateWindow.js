Clifton.investment.instrument.allocation.SecurityAllocationRebalanceRecalculateWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Recalculate Security Allocations',
	iconCls: 'coins',

	okButtonText: 'Recalculate',
	okButtonTooltip: 'Recalculate Weights or Shares for Selected Rebalance Date',

	items: [{
		xtype: 'formpanel',
		instructions: 'Recalculating a Rebalance Date will update shares based on base weights, or update base weights based on shares.  If updating shares based on weights a starting price is necessary to calculate shares. Starting price will default to use 100 if not entered.',
		getSaveURL: function() {
			return 'investmentSecurityAllocationRebalanceRebuild.json';
		},
		items: [
			{xtype: 'hidden', name: 'securityId'},
			{fieldLabel: 'Security', name: 'securityLabel', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'securityId'},
			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'Rebalance Date', name: 'rebalanceDate', xtype: 'datefield', readOnly: true},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'useBaseWeights', allowBlank: false,
				items: [
					{boxLabel: 'Recalculate weights based on Shares', xtype: 'radio', name: 'updateWeights', inputValue: true},
					{boxLabel: 'Recalculate shares based on Weights', xtype: 'radio', name: 'updateWeights', inputValue: false}
				],
				listeners: {
					change: function(rg, r) {
						const p = TCG.getParentFormPanel(rg);
						const startingPriceField = p.getForm().findField('startingPrice');
						startingPriceField.setDisabled(r.inputValue === true);
					}
				}
			},
			{
				fieldLabel: 'Starting Price', name: 'startingPrice', xtype: 'floatfield',
				qtip: 'If left blank, will default to the calculated security price on the rebalance date.  If not available, will default to 100.'
			}
		]
	}]
});
