Clifton.investment.instrument.allocation.SecurityAllocationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Allocation',
	iconCls: 'coins',
	width: 700,
	height: 500,


	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Allocation',
			items: [{
				xtype: 'formpanel',
				url: 'investmentSecurityAllocation.json',
				labelWidth: 150,
				labelFieldName: 'allocationLabel',

				getSaveURL: function() {
					const f = this.getForm();
					if (!f.idFieldValue) {
						if (TCG.isTrue(f.findField('endExisting').checked) && TCG.isNotBlank(f.findField('existingId').getValue())) {
							return 'investmentSecurityAllocationFromExistingSave.json';
						}
					}
					return 'investmentSecurityAllocationSave.json';
				},

				prepareDefaultData: function(dd) {
					// Trigger afterload to apply field labels, etc. for new allocations
					this.fireEvent.defer(10, this, ['afterload', this]);
					return dd;
				},

				listeners: {
					afterload: function(panel) {
						const f = panel.getForm();
						const allocTypeWeightLabel = TCG.getValue('parentInvestmentSecurity.instrument.hierarchy.securityAllocationTypeWeightLabel', f.formValues);
						const allocTypeSharesLabel = TCG.getValue('parentInvestmentSecurity.instrument.hierarchy.securityAllocationTypeSharesLabel', f.formValues);
						if (TCG.isNotBlank(allocTypeWeightLabel)) {
							// Change Weight Label
							const field = f.findField('allocationWeight');
							field.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(allocTypeWeightLabel + ':');
							field.fieldLabel = allocTypeWeightLabel;
						}
						if (TCG.isNotBlank(allocTypeSharesLabel)) {
							// Change Shares Label
							const field = f.findField('allocationShares');
							field.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(allocTypeSharesLabel + ':');
							field.fieldLabel = allocTypeSharesLabel;
							field.setVisible(true);
						}
						else {
							// Hide Shares field if no label
							const field = f.findField('allocationShares');
							field.setVisible(false);
						}

						if (f.idFieldValue) {
							f.findField('existingLabel').setVisible(false);
							f.findField('endExisting').setVisible(false);
						}

					}
				},
				items: [
					{fieldLabel: 'Parent Security', name: 'parentInvestmentSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'parentInvestmentSecurity.id'},
					{xtype: 'hidden', name: 'existingId'},
					{xtype: 'displayfield', name: 'existingLabel', fieldLabel: 'Existing Allocation'},
					{xtype: 'checkbox', name: 'endExisting', fieldLabel: '', boxLabel: 'End Existing Allocation on Selected Start Date - 1', requiredFields: ['existingLabel', 'existingId']},
					{xtype: 'label', html: '<hr/>'},
					{fieldLabel: 'Security', name: 'investmentSecurity.label', hiddenName: 'investmentSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', disableAddNewItem: true, mutuallyExclusiveFields: ['investmentInstrument.id']},
					{
						fieldLabel: 'Instrument', name: 'investmentInstrument.label', xtype: 'combo', hiddenName: 'investmentInstrument.id', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', url: 'investmentInstrumentListFind.json?oneSecurityPerInstrument=false', mutuallyExclusiveFields: ['investmentSecurity.id'],
						qtip: 'For cases where a many-to-many security is needed you can optionally select the instrument instead and a current security calculator.',
						listeners: {
							// Default Current Security Calculator on select
							select: function(combo, record, index) {
								const fp = combo.getParentForm();
								fp.updateDefaultCurrentSecurityCalculator();

							}
						}
					},
					{
						fieldLabel: 'Override Exchange', name: 'overrideExchange.label', hiddenName: 'overrideExchange.id', xtype: 'combo', url: 'investmentExchangeListFind.json?compositeExchange=false', displayField: 'label', detailPageClass: 'Clifton.investment.setup.ExchangeWindow', disableAddNewItem: true,
						qtip: 'An optional exchange used to override the override a security\'s exchange for use during price lookup.'
					},
					{
						fieldLabel: 'Current Security Calculator', name: 'currentSecurityCalculatorBean.name', hiddenName: 'currentSecurityCalculatorBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Investment Current Security Calculator', detailPageClass: 'Clifton.system.bean.BeanWindow',
						qtip: 'Current Security Calculator defines the behavior of how the <i>current</i> security is selected for a specific date and selected instrument so we know which security price to use.  Used with instrument selection only.  Default behavior is to first check the <b><i>Current Securities</i></b> security group, otherwise the first active security ordered by end date ascending.  <br/><br/>Note: This does not apply if a specific security is selected for the allocation.',
						requiredFields: ['investmentInstrument.id']
					},
					{fieldLabel: 'Return Weight', name: 'allocationWeight', xtype: 'floatfield'},
					{fieldLabel: 'Shares', name: 'allocationShares', xtype: 'floatfield'},
					{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
					{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
					{fieldLabel: 'Note', name: 'note', xtype: 'textarea'}
				],

				updateDefaultCurrentSecurityCalculator: function() {
					const csc = this.getForm().findField('currentSecurityCalculatorBean.id');
					if (TCG.isBlank(csc.getValue())) {
						const defaultCalc = TCG.data.getData('systemBeanByName.json?name=Default Current Contract Calculator', this, 'investment.replication.current.security.calc.default');
						if (defaultCalc) {
							this.setFormValue('currentSecurityCalculatorBean.id', {value: defaultCalc.id, text: defaultCalc.label}, true);
						}
					}
				}

			}]

		},

			{
				title: 'History',
				items: [{
					name: 'investmentSecurityAllocationListFind',
					xtype: 'gridpanel',
					instructions: 'The following displays the fully history of allocations for the same security or instrument/current security calculator.',

					columns: [
						{header: 'Weight', width: 50, dataIndex: 'allocationWeight', type: 'percent', numberFormat: '0,000.0000', useNull: true},
						{header: 'Shares', width: 100, dataIndex: 'allocationShares', type: 'float'},
						{header: 'Start Date', width: 50, dataIndex: 'startDate', type: 'date'},
						{header: 'End Date', width: 50, dataIndex: 'endDate', type: 'date'},
						{header: 'Note', width: 100, dataIndex: 'note'}
					],
					getLoadParams: function(firstLoad) {
						const sec = TCG.getValue('investmentSecurity.id', this.getWindow().getMainForm().formValues);
						const inst = TCG.getValue('investmentInstrument.id', this.getWindow().getMainForm().formValues);
						const csc = TCG.getValue('currentSecurityCalculatorBean.id', this.getWindow().getMainForm().formValues);

						const params = {};
						params.parentInvestmentSecurityId = TCG.getValue('parentInvestmentSecurity.id', this.getWindow().getMainForm().formValues);
						if (sec) {
							params.investmentSecurityId = sec;
						}
						else if (inst) {
							params.investmentInstrumentId = inst;
							if (csc) {
								params.currentSecurityCalculatorBeanId = csc;
							}
						}
						else {
							return false;
						}
						return params;
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.allocation.SecurityAllocationWindow',
						drillDownOnly: true
					}
				}]
			}]

	}]

});
