TCG.use('Clifton.investment.instrument.BaseSecurityDetailWindow');

Clifton.investment.instrument.StockWindow = Ext.extend(Clifton.investment.instrument.BaseSecurityDetailWindow, {
	titlePrefix: 'Investment Security - Stock',
	iconCls: 'coins',
	height: 650,
	width: 900,
	enableRefreshWindow: true,

	securityType: 'Stock',

	tabItems: [
		{
			title: 'Security',
			items: [{
				xtype: 'investment-security-forms-with-dynamic-fields',
				labelWidth: 140,

				items: [
					{fieldLabel: 'Investment Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
					{xtype: 'investment-instrument-ticker'},
					{fieldLabel: 'CUSIP', name: 'cusip'},
					{fieldLabel: 'ISIN', name: 'isin'},
					{fieldLabel: 'SEDOL', name: 'sedol'},
					{fieldLabel: 'FIGI', name: 'figi', qtip: 'Financial Instrument Global Identifier (FIGI; also known as the Bloomberg Global Identifier (BBGID)) is an open standard, 12 character unique identifier assigned to all financial instruments and will not change once issued. For equity instruments, an identifier is issued per trading venue.'},
					{fieldLabel: 'Security Name', name: 'name'},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 40},
					{fieldLabel: 'First Trade', name: 'startDate', xtype: 'datefield', qtip: 'Listing date for the stock.  The first date when the stock can be traded.'},
					{fieldLabel: 'Last Trade', name: 'endDate', xtype: 'datefield', qtip: 'Delisting date for the stock. The stock cannot be traded after this date but may have a security event payment(s) up to the Last Payment Date.'},
					{fieldLabel: 'Last Payment Date', name: 'lastDeliveryDate', xtype: 'datefield', qtip: 'When a stock is delisted, it may have a payment after Last Trade date. This is the date of the last entitled security event payment.'},
					{boxLabel: 'Illiquid (cannot be easily sold or exchanged for cash)', name: 'illiquid', xtype: 'checkbox'},

					{xtype: 'label', html: '<hr/>'},
					{name: 'underlyingSecurityFromSecurityUsed', value: 'false', hidden: true},
					{
						fieldLabel: 'Underlying Security', name: 'underlyingSecurity.label', hiddenName: 'underlyingSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', hidden: true,
						beforequery: function(queryEvent) {
							const formPanel = TCG.getParentFormPanel(queryEvent.combo);
							queryEvent.combo.store.baseParams = {
								hierarchyId: formPanel.getFormValue('instrument.hierarchy.underlyingHierarchy.id'),
								investmentGroupId: formPanel.getFormValue('instrument.hierarchy.underlyingInvestmentGroup.id')
							};
						},
						listeners: {
							change: function(combo, newValue, oldValue) {
								const formPanel = TCG.getParentFormPanel(combo);
								formPanel.setFormValue('underlyingSecurityFromSecurityUsed', 'true');
							}
						}
					},
					{
						fieldLabel: 'BICS Industry Subgroup', xtype: 'system-hierarchy-combo', tableName: 'InvestmentInstrument', hierarchyCategoryName: 'BICS Hierarchy',
						qtip: 'Bloomberg Industry Classification System (BICS)',
						getFkFieldId: function(fp) {
							return fp.getFormValue('instrument.id', true);
						}
					},
					{
						fieldLabel: 'GICS Sub-industry', xtype: 'system-hierarchy-combo', tableName: 'InvestmentInstrument', hierarchyCategoryName: 'GICS Hierarchy',
						qtip: 'Global Industry Classification Standard (GICS)',
						getFkFieldId: function(fp) {
							return fp.getFormValue('instrument.id', true);
						}
					},
					{
						fieldLabel: 'Issuer', name: 'businessCompany.name', hiddenName: 'businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true', detailPageClass: 'Clifton.business.company.CompanyWindow',
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const companyType = combo.getParentForm().getFormValue('instrument.hierarchy.businessCompanyType.name');
								combo.store.baseParams = companyType ? {companyType: companyType} : {};
							}
						}
					},
					{fieldLabel: 'Primary Exchange', name: 'instrument.exchange.label', hiddenName: 'instrument.exchange.id', xtype: 'combo', displayField: 'label', url: 'investmentExchangeListFind.json', detailPageClass: 'Clifton.investment.setup.ExchangeWindow', qtip: 'The main stock exchange where a publicly traded company\'s stock is bought and sold.'},
					{fieldLabel: 'Composite Exchange', name: 'instrument.compositeExchange.label', hiddenName: 'instrument.compositeExchange.id', xtype: 'combo', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=true', detailPageClass: 'Clifton.investment.setup.ExchangeWindow', qtip: 'Composite Exchange is not a real exchange. It is used to calculate average or standardized prices for a security across all exchanges where it trades.'},
					{fieldLabel: 'Country of Risk', name: 'instrument.countryOfRisk.text', hiddenName: 'instrument.countryOfRisk.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
					{fieldLabel: 'Country of Incorporation', name: 'instrument.countryOfIncorporation.text', hiddenName: 'instrument.countryOfIncorporation.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
					{fieldLabel: 'Currency Denomination', name: 'instrument.tradingCurrency.name', hiddenName: 'instrument.tradingCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', allowBlank: false, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
					{fieldLabel: 'Price Multiplier', name: 'instrument.priceMultiplier', xtype: 'floatfield', hidden: true, value: '1', allowBlank: false}
				],
				processAfterRenderFieldVisibility: async function(formPanel) {
					Clifton.investment.instrument.SecurityFormWithDynamicFields.prototype.processAfterRenderFieldVisibility.apply(this, arguments);
					const underlyingSecurityField = formPanel.getForm().findField('underlyingSecurity.label');
					if (underlyingSecurityField) {
						let underlyingAllowed = formPanel.getFormValue('instrument.hierarchy.underlyingAllowed');
						let instrumentId = formPanel.getFormValue('instrument.id');
						if (TCG.isBlank(instrumentId)) {
							// creating a new security from default data
							const defaultData = formPanel.getWindow().defaultData;
							if (defaultData.instrument) {
								instrumentId = formPanel.getWindow().defaultData.instrument.id;
								if (defaultData.instrument.hierarchy) {
									underlyingAllowed = defaultData.instrument.hierarchy.underlyingAllowed;
								}
							}
						}
						if (TCG.isBlank(underlyingAllowed) && TCG.isNotBlank(instrumentId)) {
							const instrument = await TCG.data.getDataPromise('investmentInstrument.json', formPanel, {params: {id: instrumentId}});
							underlyingAllowed = instrument.hierarchy.underlyingAllowed;
						}

						if (TCG.isTrue(underlyingAllowed)) {
							underlyingSecurityField.setVisible(true);
						}
					}
				}
			}]
		},


		{
			title: 'All Events',
			items: [{
				xtype: 'investment-security-events-by-security-grid',
				editor: {
					allowToDeleteMultiple: true,
					detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow',
					getDefaultData: function(gridPanel) {
						return this.savedDefaultData;
					},
					addToolbarAddButton: function(toolBar, gridPanel) {
						const fp = gridPanel.getWindow().getMainFormPanel();
						toolBar.add(new TCG.form.ComboBox({name: 'eventType', url: 'investmentSecurityEventTypeListFind.json?investmentInstrumentHierarchyId=' + fp.getFormValue('instrument.hierarchy.id'), limitRequestedProperties: false, width: 170, emptyText: '< Select Event Type >'}));
						toolBar.add({
							text: 'Add',
							tooltip: 'Create a new security event of selected type',
							iconCls: 'add',
							scope: this,
							handler: function() {
								let eventType = TCG.getChildByName(toolBar, 'eventType');
								const eventTypeId = eventType.getValue();
								if (TCG.isBlank(eventTypeId)) {
									TCG.showError('You must first select desired event type from the list.');
								}
								else {
									eventType = eventType.store.getById(eventTypeId);
									this.savedDefaultData = {
										security: fp.getForm().formValues,
										type: eventType.json
									};
									this.openDetailPage(this.getDetailPageClass(), gridPanel);
								}
							}
						});
						toolBar.add('-');
					}
				}
			}]
		},


		{
			title: 'Splits',
			items: [{
				name: 'investmentSecurityEventListFind',
				xtype: 'gridpanel',
				instructions: 'Stock split is an increase of decrease in the number of shares in the public company (2 for 1, 3 for 1, 5 for 2, etc.). When split happens, quantities of existing positions held as well as historic stock prices need to be adjusted. NOTE: this grid also includes Stock Spinoff events.',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'CA ID', width: 40, dataIndex: 'corporateActionIdentifier', type: 'int', numberFormat: '0', useNull: true, hidden: true, tooltip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.'},
					{header: 'Event Type', width: 60, dataIndex: 'type.name', filter: {searchFieldName: 'typeNameSearch'}},
					{header: 'Event Status', width: 55, dataIndex: 'status.name', renderer: Clifton.investment.instrument.event.renderEventStatus, filter: {type: 'combo', searchFieldName: 'statusId', url: 'investmentSecurityEventStatusListFind.json'}},
					{header: 'Spinoff Security', width: 60, dataIndex: 'additionalSecurity.symbol', hidden: true},
					{header: 'Start Shares', width: 50, dataIndex: 'beforeEventValue', type: 'float', useNull: true},
					{header: 'End Shares', width: 50, dataIndex: 'afterEventValue', type: 'float', useNull: true},
					{header: 'Adjustment Factor', width: 60, dataIndex: 'additionalEventValue', type: 'float', useNull: true, hidden: true, tooltip: 'Optional parent security Cost Basis adjustment factor (multiplier). The cost basis of parent security will change by this adjustment factor and the cost basis for spinoff security will get the remaining cost basis. If this value is not defined, the system will calculated it using Event Date prices and shares Distribution Ratio.'},
					{header: 'Declare Date', width: 50, dataIndex: 'declareDate'},
					{header: 'Ex Date', width: 50, dataIndex: 'exDate'},
					{header: 'Record Date', width: 50, dataIndex: 'recordDate'},
					{header: 'Event Date', width: 50, dataIndex: 'eventDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
					{header: 'Description', width: 60, dataIndex: 'eventDescription', hidden: true},
					{header: 'Voluntary', width: 30, dataIndex: 'voluntary', type: 'boolean', hidden: true, tooltip: 'Specifies whether the holder of the security has an option to choose to participate or not participate in this event.'},
					{header: 'Taxable', width: 30, dataIndex: 'taxable', type: 'boolean', hidden: true, tooltip: 'Specifies whether this event is likely subject to additional taxes affecting net payout amounts and booking rules.'},
					{header: 'Order', width: 30, dataIndex: 'bookingOrderOverride', type: 'int', useNull: true, hidden: true, tooltip: 'Optionally overrides the booking order specified on accounting event journal types.'}
				],
				editor: {
					detailPageClass: {
						getItemText: function(rowItem) {
							return rowItem.get('type.name');
						},
						items: [
							{text: 'Stock Split', iconCls: 'event', className: 'Clifton.investment.instrument.event.StockSplitEventWindow'},
							{text: 'Stock Split (Reverse)', iconCls: 'event', className: 'Clifton.investment.instrument.event.StockSplitEventWindow'},
							{text: 'Stock Spinoff', iconCls: 'event', className: 'Clifton.investment.instrument.event.StockSpinoffEventWindow'}
						]
					},
					getDefaultData: function(gridPanel, row, className, itemText) {
						const type = itemText || row.json.type.name;
						return {
							security: gridPanel.getWindow().getMainForm().formValues,
							type: TCG.data.getData('investmentSecurityEventTypeByName.json?name=' + type, gridPanel, 'investment.security.event.' + type),
							eventDescription: 'Cash in Lieu'
						};
					},
					addEditButtons: function(t, gridPanel) {
						const hierarchy = gridPanel.getWindow().defaultData.instrument.hierarchy;
						if ((hierarchy.investmentType.name !== 'Stocks' && hierarchy.investmentType.name !== 'Funds') ||
							(hierarchy.investmentType.name === 'Stocks' && hierarchy.investmentTypeSubType.name === 'Preferred Stocks') ||
							(hierarchy.investmentType.name === 'Funds' && hierarchy.investmentTypeSubType.name === 'Proprietary Funds')) {
							t.add({
								text: 'Import Latest Stock Split',
								xtype: 'splitbutton',
								tooltip: 'Import latest stock split event from Bloomberg',
								iconCls: 'run',
								scope: gridPanel,
								handler: function() {
									gridPanel.importStockSplits(false);
								},
								menu: new Ext.menu.Menu({
									items: [
										{
											text: 'Import Historic Stock Split',
											tooltip: 'Import historic stock split events from Bloomberg.',
											iconCls: 'run',
											scope: this,
											handler: function() {
												TCG.showInfo('This feature was disabled.');
											}
										}
									]
								})
							});
							t.add('-');
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
						}
					}
				},
				getLoadParams: function() {
					return {
						securityId: this.getWindow().getMainFormId(),
						typeNames: ['Stock Split', 'Stock Split (Reverse)', 'Stock Spinoff']
					};
				},
				importStockSplits: function(historic) {
					const grid = this;
					const params = {'securityId': this.getWindow().getMainFormId(), 'eventTypeNameList': 'Stock Split'};
					const loader = new TCG.data.JsonLoader({
						waitTarget: grid,
						waitMsg: 'Processing...',
						params: params,
						conf: params,
						onLoad: function(record, conf) {
							grid.reload(false);
						},
						onFailure: function() {
							// Even if the Processing Fails, still need to reload so that dates are properly updated, and warnings adjusted
							grid.reload(false);
						}
					});
					loader.load(historic ? 'investmentSecurityEventHistoryLoad.json' : 'investmentSecurityEventLoad.json');
				}
			}]
		},


		{
			title: 'Dividends',
			items: [{
				name: 'investmentSecurityEventListFind',
				xtype: 'gridpanel',
				instructions: 'Dividend is payment made by a corporation to its shareholders. Payments are represented as cash value per share (Cash Dividend Payment) or stock quantity per share (Stock Dividend Payment).',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'CA ID', width: 40, dataIndex: 'corporateActionIdentifier', type: 'int', numberFormat: '0', useNull: true, hidden: true, tooltip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.'},
					{header: 'Event Type', width: 80, dataIndex: 'type.name', filter: {searchFieldName: 'typeNameSearch'}},
					{header: 'Event Status', width: 50, dataIndex: 'status.name', renderer: Clifton.investment.instrument.event.renderEventStatus, filter: {type: 'combo', searchFieldName: 'statusId', url: 'investmentSecurityEventStatusListFind.json'}},
					{header: 'Per Share', width: 45, dataIndex: 'afterEventValue', type: 'float', useNull: true, tooltip: 'Dividend per Share'},
					{header: 'Declare Date', width: 50, dataIndex: 'declareDate'},
					{header: 'Ex Date', width: 45, dataIndex: 'exDate'},
					{header: 'Record Date', width: 50, dataIndex: 'recordDate'},
					{header: 'Payment Date', width: 50, dataIndex: 'eventDate'},
					{header: 'CCY', width: 25, dataIndex: 'additionalSecurity.symbol', tooltip: 'This dividend will be paid in the currency specified here. Blank means use currency denomination of corresponding security.', filter: {searchFieldName: 'additionalSecurityId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true'}},
					{header: 'Description', width: 50, dataIndex: 'eventDescription'},
					{header: 'Voluntary', width: 30, dataIndex: 'voluntary', type: 'boolean', hidden: true, tooltip: 'Specifies whether the holder of the security has an option to choose to participate or not participate in this event.'},
					{header: 'Taxable', width: 30, dataIndex: 'taxable', type: 'boolean', hidden: true, tooltip: 'Specifies whether this event is likely subject to additional taxes affecting net payout amounts and booking rules.'},
					{header: 'Order', width: 30, dataIndex: 'bookingOrderOverride', type: 'int', useNull: true, hidden: true, tooltip: 'Optionally overrides the booking order specified on accounting event journal types.'}
				],
				editor: {
					detailPageClass: {
						getItemText: function(rowItem) {
							return rowItem.get('type.name');
						},
						items: [
							{text: 'Cash Dividend Payment', iconCls: 'event', className: 'Clifton.investment.instrument.event.DividendPaymentEventWindow'},
							{text: 'Cash Dividend (with DRIP)', iconCls: 'event', className: 'Clifton.investment.instrument.event.ScripDividendEventWindow'},
							{text: 'Scrip Dividend', iconCls: 'event', className: 'Clifton.investment.instrument.event.ScripDividendEventWindow'},
							{text: 'Stock Dividend Payment', iconCls: 'event', className: 'Clifton.investment.instrument.event.DividendPaymentEventWindow'}
						]
					},
					getDefaultData: function(gridPanel, row, className, itemText) {
						const type = itemText || row.json.type.name;
						return {
							security: gridPanel.getWindow().getMainForm().formValues,
							type: TCG.data.getData('investmentSecurityEventTypeByName.json?name=' + type, gridPanel, 'investment.security.event.' + type),
							eventDescription: (type === 'Cash Dividend Payment') ? undefined : 'Cash in Lieu'
						};
					},
					addEditButtons: function(t, gridPanel) {
						const hierarchy = gridPanel.getWindow().defaultData.instrument.hierarchy;
						if ((hierarchy.investmentType.name !== 'Stocks' && hierarchy.investmentType.name !== 'Funds') ||
							(hierarchy.investmentType.name === 'Stocks' && hierarchy.investmentTypeSubType.name === 'Preferred Stocks') ||
							(hierarchy.investmentType.name === 'Funds' && hierarchy.investmentTypeSubType.name === 'Proprietary Funds')) {
							t.add({
								text: 'Import Latest Dividend',
								tooltip: 'Import latest dividend event from Bloomberg',
								iconCls: 'run',
								scope: gridPanel,
								handler: function() {
									const grid = this;
									const params = {'securityId': this.getWindow().getMainFormId(), 'eventTypeNameList': ['Cash Dividend Payment']};
									const loader = new TCG.data.JsonLoader({
										waitTarget: gridPanel,
										waitMsg: 'Processing...',
										params: params,
										conf: params,
										onLoad: function(record, conf) {
											grid.reload();
										},
										onFailure: function() {
											// Even if the Processing Fails, still need to reload so that dates are properly updated, and warnings adjusted
											grid.reload();
										}
									});
									loader.load('investmentSecurityEventLoad.json');
								}
							});
							t.add('-');
						}
						TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
					}
				},
				getLoadParams: function() {
					return {
						securityId: this.getWindow().getMainFormId(),
						typeNames: ['Cash Dividend Payment', 'Stock Dividend Payment', 'Cash Dividend (with DRIP)', 'Scrip Dividend']
					};
				}
			}]
		},


		{
			title: 'Notes',
			items: [{
				xtype: 'investment-security-system-note-grid'
			}]
		},


		{
			title: 'Instrument Groups',
			layout: 'border',
			items: [
				{
					xtype: 'investment-instrumentGroupGrid_ByInstrument',
					region: 'north',
					height: 250,
					securityWindow: true
				},
				{
					xtype: 'investment-instrumentGroupMissingGrid_ByInstrument',
					region: 'center',
					securityWindow: true
				}
			]
		},


		{
			title: 'Security Groups',
			items: [{
				xtype: 'investment-securityGroupGrid_BySecurity'
			}]
		},


		{
			title: 'Tasks',
			items: [{
				xtype: 'workflow-task-grid',
				tableName: 'InvestmentSecurity'
			}]
		}
	]
});
