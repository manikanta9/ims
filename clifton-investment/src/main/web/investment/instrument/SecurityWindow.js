// not the actual window but a window selector:
//   - one-to-one instrument to security get their custom window
//   - the rest get generic security window

Clifton.investment.instrument.SecurityWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'investmentSecurity.json',

	getClassName: function(config, entity) {
		let screen = 'GenericSecurity';
		let h = null;
		if (!config.params) {
			// NEW instrument or security
			const i = config.defaultData.instrument;
			if (i && i.hierarchy) {
				h = i.hierarchy;
			}
		}
		if (entity) {
			h = entity.instrument.hierarchy;
		}
		if (h && (h.oneSecurityPerInstrument || Clifton.investment.instrument.SecurityWindowClassMap[h.instrumentScreen])) {
			screen = h.instrumentScreen;
		}
		const className = Clifton.investment.instrument.SecurityWindowClassMap[screen];
		if (!className) {
			TCG.showError('Unsupported security screen: ' + screen, 'Investment Security');
			return false;
		}
		return className;
	}
});
