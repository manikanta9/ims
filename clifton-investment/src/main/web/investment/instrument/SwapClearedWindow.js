TCG.use('Clifton.investment.instrument.SwapWindow');

Clifton.investment.instrument.SwapClearedWindow = Ext.extend(Clifton.investment.instrument.SwapWindow, {
	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Swap',
			items: [{
				xtype: 'investment-security-forms-with-dynamic-fields',
				labelFieldName: 'name',
				labelWidth: 140,
				items: [
					{fieldLabel: 'Investment Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
					{fieldLabel: 'Investment Instrument', name: 'instrument.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'instrument.id'},
					{fieldLabel: 'Clearing Code', xtype: 'investment-instrument-ticker'},
					{fieldLabel: 'Internal Identifier', name: 'cusip', readOnly: true, qtip: 'Unique internal identifier generated by our system for this ClearedSwap. It follows reserved CUSIP range format.'},
					{xtype: 'investment-instrument-underlying-combo', name: 'instrument.underlyingInstrument.labelShort', hiddenName: 'instrument.underlyingInstrument.id'},
					{fieldLabel: 'Security Name', name: 'name'},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 40},
					{fieldLabel: 'CCP', name: 'instrument.exchange.name', hiddenName: 'instrument.exchange.id', xtype: 'combo', url: 'investmentExchangeListFind.json', detailPageClass: 'Clifton.investment.setup.ExchangeWindow', allowBlank: false, qtip: 'Clearing Counterparty Clearing House'},
					{fieldLabel: 'Country of Risk', name: 'instrument.countryOfRisk.text', hiddenName: 'instrument.countryOfRisk.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
					{fieldLabel: 'Country of Incorporation', name: 'instrument.countryOfIncorporation.text', hiddenName: 'instrument.countryOfIncorporation.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
					{fieldLabel: 'CCY Denomination', name: 'instrument.tradingCurrency.name', hiddenName: 'instrument.tradingCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', allowBlank: false, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
					{xtype: 'label', html: '<hr/>'},
					{fieldLabel: 'Index Start Date', name: 'startDate', xtype: 'datefield'},
					{fieldLabel: 'Maturity Date', name: 'endDate', xtype: 'datefield', allowBlank: false},
					{
						fieldLabel: 'Final Payment Date', name: 'lastDeliveryDate', xtype: 'datefield', allowBlank: false,
						listeners: {
							change: function(field, newValue, oldValue) {
								TCG.getParentFormPanel(field).setFormValue('firstDeliveryDate', newValue);
							},
							select: function(field, newValue) {
								TCG.getParentFormPanel(field).setFormValue('firstDeliveryDate', newValue);
							}
						}
					},
					{name: 'firstDeliveryDate', xtype: 'datefield', hidden: true},
					{fieldLabel: 'Price Multiplier', name: 'instrument.priceMultiplier', xtype: 'floatfield', hidden: true, value: '1', allowBlank: false},
					{xtype: 'label', html: '<hr/>'},
					{
						fieldLabel: 'Settlement CCY', name: 'settlementCurrency.label', hiddenName: 'settlementCurrency.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: true, hidden: true,
						qtip: 'In rare cases of OTC securities, Settlement Currency for events and trades maybe different than Currency Denomination of security. Use this field to specify the default Settlement Currency. It can still be manually overridden for individual transactions.'
					}
				],
				prepareDefaultData: function(defaultData) {
					const f = this.getForm();

					if (TCG.getValue('instrument.hierarchy.oneSecurityPerInstrument', defaultData) === true) {
						this.hideField('instrument.name');
					}
					else {
						f.findField('instrument.underlyingInstrument.labelShort').setReadOnly(true);
						f.findField('instrument.exchange.name').setReadOnly(true);
						f.findField('instrument.exchange.name').allowBlank = true;
						f.findField('instrument.countryOfRisk.text').setReadOnly(true);
						f.findField('instrument.countryOfIncorporation.text').setReadOnly(true);
						f.findField('instrument.tradingCurrency.name').setReadOnly(true);
					}
					if (TCG.getValue('instrument.hierarchy.investmentTypeSubType.name', defaultData) === 'Interest Rate Swaps') {
						this.setFieldLabel('startDate', 'Effective Date');
						f.findField('lastDeliveryDate').allowBlank = true;
						this.hideField('instrument.underlyingInstrument.labelShort');
					}
					if (TCG.getValue('instrument.hierarchy.investmentTypeSubType.name', defaultData) === 'Inflation Swaps') {
						this.setFieldLabel('startDate', 'Effective Date');
					}
					if (TCG.isTrue(TCG.getValue('instrument.hierarchy.differentSettlementCurrencyAllowed', defaultData))) {
						this.showField('settlementCurrency.label');
					}

					return defaultData;
				}
			}]
		}]
	}]
});
