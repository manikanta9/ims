Clifton.investment.instrument.MoveInstrumentWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Move Instrument to a new Hierarchy',
	iconCls: 'move',
	height: 300,
	modal: true,
	allowOpenFromModal: true,

	okButtonText: 'Move',
	okButtonTooltip: 'Move Instrument to Selected Hierarchy',

	items: [{
		xtype: 'formpanel',
		instructions: 'Moving instruments to a new hierarchy is only allowed if both hierarchies utilize the same custom fields and event type mappings.  Invalid custom field mappings can in some cases be ignored if necessary to help with moves.  Your from/to hierarchies must also both be one to one or one to many.  After the move is complete the instrument window will open for you to review/address any missing custom field values.',
		url: 'investmentInstrument.json',
		getSaveURL: function() {
			return 'investmentInstrumentMove.json';
		},
		items: [
			{fieldLabel: 'Instrument', name: 'labelShort', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'id', submitValue: false, submitDetailField: false},
			{fieldLabel: 'Current Hierarchy', name: 'hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'hierarchy.id', submitValue: false, submitDetailField: false},
			{xtype: 'hidden', name: 'hierarchy.oneSecurityPerInstrument'},
			{xtype: 'label', html: '<hr/>'},
			{
				fieldLabel: 'New Hierarchy', xtype: 'combo', name: 'newInstrumentHierarchyLabel', hiddenName: 'newInstrumentHierarchyId', allowBlank: false, url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true',
				detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow',
				displayField: 'labelExpanded',
				queryParam: 'labelExpanded', listWidth: 600,
				disableAddNewItem: true,
				listeners: {
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						const params = {};
						// if a specific instrument was passed to this window - use it - otherwise filter on hierarchy/group/type whatever has been set.
						params.oneSecurityPerInstrument = f.findField('hierarchy.oneSecurityPerInstrument').value;
						queryEvent.combo.store.baseParams = params;
					}
				}
			}
		],

		listeners: {
			// After Saving - close this window and open the instrument window
			afterload: function(panel, closeOnSuccess) {
				if (TCG.isTrue(panel.getWindow().savedSinceOpen)) {
					const config = {
						params: {id: panel.getFormValue('id')},
						openerCt: panel.ownerCt.openerCt
					};
					TCG.createComponent('Clifton.investment.instrument.InstrumentWindow', config);
					panel.getWindow().closeWindow();
				}
			}
		}
	}]
});
