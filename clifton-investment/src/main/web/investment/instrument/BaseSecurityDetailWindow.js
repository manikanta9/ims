Clifton.investment.instrument.BaseSecurityDetailWindow = Ext.extend(TCG.app.DetailWindow, {
	// Title of tab to use as an insert location for additional tabs. If undefined, additional tabs will be appended to the existing.
	insertTabTitle: 'Notes',

	/**
	 * Performs pre-initialization configuration.
	 */
	init: function() {
		this.items = this.generateWindowItems();
		return TCG.callSuper(this, 'init', arguments);
	},


	/**
	 * Generates the window items, including the configured tabs.
	 *
	 * @return {[*]} the generated window items
	 */
	generateWindowItems: function() {
		// Add standard tabs, applying filters
		const self = this;
		const tabs = [...self.tabItems];
		const additionalTabs = Clifton.investment.instrument.SecurityAdditionalTabsByType[self.securityType] || Clifton.investment.instrument.SecurityAdditionalTabsByType['DEFAULT'];
		// Sort tabs by an optional index property of the tab item. If provided, lowest index values will be first. If no index, the order should remain in which tabs are added.
		additionalTabs.sort((first, second) => {
			if (TCG.isNumber(first.index)) {
				return TCG.isNumber(second.index) ? first.index - second.index : -1;
			}
			else if (TCG.isNumber(second.index)) {
				return 1;
			}
			return 0;
		});
		if (additionalTabs && additionalTabs.length > 0) {
			if (TCG.isBlank(self.insertTabTitle)) {
				tabs.push(...additionalTabs);
			}
			else {
				const insertTabIndex = tabs.findIndex(element => element.title === self.insertTabTitle);
				tabs.splice(insertTabIndex, 0, ...additionalTabs);
			}
		}

		return [{
			xtype: 'tabpanel',
			requiredFormIndex: 0,
			items: tabs
		}];
	}
});
