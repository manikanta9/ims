Clifton.investment.instrument.BenchmarkWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Benchmark',
	iconCls: 'coins',
	height: 575,
	width: 1020,
	enableRefreshWindow: true,

	showClientAccount: false,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Benchmark',
				items: [{
					xtype: 'investment-security-forms-with-dynamic-fields',
					instructions: 'Benchmark is an index or an allocation of indices that adds to 100%. Benchmarks and indices cannot be traded but can be replicated.',
					labelWidth: 140,

					listeners: {
						afterrender: function(panel) {
							// If allocation of securities show allocations tab
							const f = panel.getForm();
							const w = panel.getWindow();
							// Client Account (Defined by the Screen)
							this.showHideField('instrument.investmentAccount.id', (w.showClientAccount === true));

							const alloc = TCG.getValue('instrument.hierarchy.securityAllocationType', f.formValues);
							const isAllocation = TCG.isNotBlank(alloc);
							if (isAllocation === true) {

								const tabs = w.items.get(0);
								if (Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs[alloc]) {
									// ADD THEM IN
									for (let i = 0; i < Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs[alloc].length; i++) {
										tabs.insert(i + 1, Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs[alloc][i]);
									}
								}
								else if (Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs['DEFAULT']) {
									// ADD IN THE DEFAULT
									for (let i = 0; i < Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs['DEFAULT'].length; i++) {
										tabs.insert(i + 1, Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs['DEFAULT'][i]);
									}
								}
								// If Dynamic, Replace Rebalance Dates tab with Preview Weights
								const dynAlloc = TCG.getValue('instrument.hierarchy.dynamicAllocationCalculatorSystemBean', f.formValues);
								const isDynamic = TCG.isNotBlank(dynAlloc);
								if (isDynamic === true) {
									const i = this.getTabPosition(w, 'Rebalance Dates');
									tabs.remove(i);
									tabs.insert(i, this.previewWeightsTab);
								}


								const id = TCG.getValue('id', f.formValues);

								const date = new Date();
								const dateString = date.format('m/d/Y');

								TCG.data.getDataValuePromise(`investmentSecurityNotionalMultiplier.json?securityId=${id}&date=${dateString}&settlementDate=${dateString}`, this)
									.then(function(data) {
										panel.setFormValue('indexDivisor', data, true);
									});
							}

							// CUSIP, ISIN, SEDOL, FIGI - Hide if allocation of securities, else show
							this.showHideField('cusip', isAllocation !== true);
							this.showHideField('isin', isAllocation !== true);
							this.showHideField('sedol', isAllocation !== true);
							this.showHideField('figi', isAllocation !== true);

							panel.doLayout();
						}
					},

					getTabPosition: function(w, tabTitle) {
						const tabs = w.items.get(0);
						for (let i = 0; i < tabs.items.length; i++) {
							if (TCG.isEquals(tabs.items.get(i).title, tabTitle)) {
								return i;
							}
						}
						return 0;
					},

					showHideField: function(fieldName, visible, overrideFieldLabel) {
						const fld = this.getForm().findField(fieldName);
						if (fld) {
							fld.setVisible(visible === true);
							if (overrideFieldLabel) {
								fld.fieldLabel = overrideFieldLabel;
							}
						}
					},

					previewWeightsTab: {
						title: 'Preview Weights',
						xtype: 'gridpanel',
						name: 'investmentSecurityAllocationDynamicList',
						useBufferView: false, // allow cell wrapping
						instructions: 'Select a date in the above toolbar to see what the calculated allocations and weights would be for this security.',
						additionalPropertiesToRequest: 'investmentSecurity.id|investmentInstrument.id',
						columns: [
							{header: 'ID', width: 15, hidden: true, dataIndex: 'id'},
							{header: 'Security/Instrument', width: 300, dataIndex: 'allocationLabelExpanded'},
							{header: 'Security', width: 200, dataIndex: 'investmentSecurity.label', hidden: true},
							{header: 'Instrument', width: 200, dataIndex: 'investmentInstrument.label', hidden: true},
							{
								header: 'Note', width: 200, dataIndex: 'note',
								renderer: function(v, metaData, r) {
									if (TCG.isNotBlank(v) && v.startsWith('ERROR: ')) {
										metaData.css = 'amountNegative';
									}
									return v;
								}
							},
							{header: 'Return Weight', width: 100, dataIndex: 'allocationWeight', type: 'percent', numberFormat: '0,000.0000', summaryType: 'sum'}
						],
						plugins: {ptype: 'gridsummary'},
						editor: {
							drillDownOnly: true,
							detailPageClass: 'Clifton.investment.instrument.allocation.SecurityAllocationWindow',
							// Used to Open the Allocation or Security/Instrument Window Directly
							openWindowFromContextMenu: function(grid, rowIndex, screen) {
								const gridPanel = this.getGridPanel();
								const row = grid.store.data.items[rowIndex];
								let clazz = this.detailPageClass;
								let id = undefined;

								if (screen === 'SECURITY_ALLOCATION') {
									id = row.json.id;
									clazz = 'Clifton.investment.instrument.allocation.SecurityAllocationWindow';
								}
								else if (row.json.investmentInstrument && row.json.investmentInstrument.id) {
									id = row.json.investmentInstrument.id;
									clazz = 'Clifton.investment.instrument.InstrumentWindow';
								}
								else if (row.json.investmentSecurity && row.json.investmentSecurity.id) {
									id = row.json.investmentSecurity.id;
									clazz = 'Clifton.investment.instrument.SecurityWindow';
								}
								if (clazz) {
									this.openDetailPage(clazz, gridPanel, id, row);
								}
							}
						},
						getTopToolbarFilters: function(toolbar) {
							const filters = [];
							filters.push({fieldLabel: 'Date', xtype: 'toolbar-datefield', name: 'measureDate', value: Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y')});
							return filters;
						},
						getLoadParams: function(firstLoad) {
							const t = this.getTopToolbar();
							let dateValue;
							const df = TCG.getChildByName(t, 'measureDate');
							if (TCG.isNotBlank(df.getValue())) {
								dateValue = (df.getValue()).format('m/d/Y');
							}
							else {
								const prevBD = Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y');
								dateValue = prevBD.format('m/d/Y');
								df.setValue(dateValue);
							}
							const result = {date: dateValue};
							result.securityId = this.getWindow().getMainFormId();
							return result;
						},
						listeners: {
							afterrender: function(gridPanel) {
								const el = gridPanel.getEl();
								el.on('contextmenu', function(e, target) {
									const g = gridPanel.grid;
									g.contextRowIndex = g.view.findRowIndex(target);
									e.preventDefault();
									if (!g.drillDownMenu) {
										g.drillDownMenu = new Ext.menu.Menu({
											items: [
												{
													text: 'Open Security Allocation', iconCls: 'grid',
													handler: function() {
														gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'SECURITY_ALLOCATION');
													}
												},
												{
													text: 'Open Security/Instrument', iconCls: 'coins',
													handler: function() {
														gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'SECURITY_INSTRUMENT');
													}
												}
											]
										});
									}
									TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
								}, gridPanel);
							}
						}
					},

					items: [
						{fieldLabel: 'Investment Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
						{xtype: 'investment-instrument-underlying-combo', name: 'instrument.underlyingInstrument.labelShort', hiddenName: 'instrument.underlyingInstrument.id'},
						{xtype: 'investment-instrument-ticker'},
						{fieldLabel: 'CUSIP', name: 'cusip'},
						{fieldLabel: 'ISIN', name: 'isin'},
						{fieldLabel: 'SEDOL', name: 'sedol'},
						{fieldLabel: 'FIGI', name: 'figi', qtip: 'Financial Instrument Global Identifier (FIGI; also known as the Bloomberg Global Identifier (BBGID)) is an open standard, 12 character unique identifier assigned to all financial instruments and will not change once issued. For equity instruments, an identifier is issued per trading venue.'},
						{fieldLabel: 'Security Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 40},

						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Country of Risk', name: 'instrument.countryOfRisk.text', hiddenName: 'instrument.countryOfRisk.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
						{fieldLabel: 'Country of Incorporation', name: 'instrument.countryOfIncorporation.text', hiddenName: 'instrument.countryOfIncorporation.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
						{
							fieldLabel: 'Issuer', name: 'businessCompany.name', hiddenName: 'businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true', detailPageClass: 'Clifton.business.company.CompanyWindow',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const companyType = combo.getParentForm().getFormValue('instrument.hierarchy.businessCompanyType.name');
									combo.store.baseParams = companyType ? {companyType: companyType} : {};
								}
							}
						},
						{fieldLabel: 'Primary Exchange', name: 'instrument.exchange.label', hiddenName: 'instrument.exchange.id', xtype: 'combo', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=false', detailPageClass: 'Clifton.investment.setup.ExchangeWindow', qtip: 'The main stock exchange where a publicly traded company\'s stock is bought and sold.'},
						{fieldLabel: 'Composite Exchange', name: 'instrument.compositeExchange.label', hiddenName: 'instrument.compositeExchange.id', xtype: 'combo', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=true', detailPageClass: 'Clifton.investment.setup.ExchangeWindow', qtip: 'Composite Exchange is not a real exchange. It is used to calculate average or standardized prices for a security across all exchanges where it trades.'},
						{fieldLabel: 'Settlement Calendar', name: 'instrument.settlementCalendar.name', hiddenName: 'instrument.settlementCalendar.id', xtype: 'combo', url: 'calendarListFind.json', detailPageClass: 'Clifton.calendar.setup.CalendarWindow', qtip: 'Optional Settlement Calendar that will override the calendar of the Exchange or Hierarchy. It is used to calculate the Settlement Date and to determine holidays when securities are not priced. Define only if different.'},
						{fieldLabel: 'Currency Denomination', name: 'instrument.tradingCurrency.name', hiddenName: 'instrument.tradingCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', allowBlank: false, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{
							fieldLabel: 'Price Multiplier', name: 'instrument.priceMultiplier', xtype: 'floatfield', hidden: true,
							qtip: 'Price multiplier inherited from the security\'s instrument'
						},
						{
							fieldLabel: 'Dated Index Divisor', name: 'indexDivisor', xtype: 'floatfield', readOnly: 'true',
							qtip: 'Because this field is a dated value, it cannot be modified here. Go to the Data Fields tab to enter a new value.'
						},
						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'First Trade', name: 'startDate', xtype: 'datefield'},
						{fieldLabel: 'Last Trade', name: 'endDate', xtype: 'datefield'},
						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Client Account', hidden: true, name: 'instrument.investmentAccount.label', hiddenName: 'instrument.investmentAccount.id', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'}
					]
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'investment-security-system-note-grid'
				}]
			},


			{
				title: 'Events',
				items: [{
					name: 'investmentSecurityEventListFind',
					xtype: 'gridpanel',
					instructions: 'A security event represents an instance of a specific event type for a specific security. For example, specific stock split, dividend payment, etc.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type', width: 100, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}},
						{header: 'Additional Security', width: 100, dataIndex: 'additionalSecurity.label', filter: {type: 'combo', searchFieldName: 'additionalSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Before Value', width: 60, dataIndex: 'beforeEventValue', type: 'float'},
						{header: 'After Value', width: 60, dataIndex: 'afterEventValue', type: 'float'},
						{header: 'Declare Date', width: 50, dataIndex: 'declareDate', hidden: true},
						{header: 'Ex Date', width: 50, dataIndex: 'exDate', hidden: true},
						{header: 'Record Date', width: 50, dataIndex: 'recordDate', hidden: true},
						{header: 'Event Date', width: 50, dataIndex: 'eventDate'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow',
						getDefaultData: function(gridPanel) {
							return this.savedDefaultData;
						},
						addToolbarAddButton: function(toolBar, gridPanel) {
							toolBar.add(new TCG.form.ComboBox({name: 'eventType', url: 'investmentSecurityEventTypeListFind.json', width: 150, listWidth: 230}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add an event of selected type to this security',
								iconCls: 'add',
								scope: this,
								handler: function() {
									const eventType = TCG.getChildByName(toolBar, 'eventType');
									const eventTypeId = eventType.getValue();
									if (TCG.isNotBlank(eventTypeId)) {
										TCG.showError('You must first select desired Event Type from the list.');
									}
									else {
										this.savedDefaultData = {
											security: gridPanel.getWindow().getMainForm().formValues,
											type: {
												id: eventTypeId,
												name: eventType.lastSelectionText
											}
										};
										this.openDetailPage(this.getDetailPageClass(), gridPanel);
									}
								}
							});
							toolBar.add('-');
						}
					},
					getLoadParams: function() {
						return {'securityId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Symbol Overrides',
				items: [{xtype: 'marketdata-datasource-security-override-grid'}]
			},


			{
				title: 'Data Fields',
				items: [{
					xtype: 'system-column-dated-value-grid',
					tableName: 'InvestmentSecurity',
					includeTopToolbarActiveOnDateFilter: false
				}]
			},


			{
				title: 'Market Data',
				items: [{xtype: 'marketdata-security-datavalue-grid'}]
			},


			{
				title: 'Instrument Groups',
				layout: 'border',
				items: [
					{
						xtype: 'investment-instrumentGroupGrid_ByInstrument',
						region: 'north',
						height: 250,
						securityWindow: true
					},
					{
						xtype: 'investment-instrumentGroupMissingGrid_ByInstrument',
						region: 'center',
						securityWindow: true
					}
				]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'InvestmentSecurity'
				}]
			}
		]
	}]
});
