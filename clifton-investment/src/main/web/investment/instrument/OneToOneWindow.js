TCG.use('Clifton.investment.instrument.BaseSecurityDetailWindow');

Clifton.investment.instrument.OneToOneWindow = Ext.extend(Clifton.investment.instrument.BaseSecurityDetailWindow, {
	titlePrefix: 'Investment Security',
	iconCls: 'coins',
	height: 600,
	width: 650,

	tabItems: [
		{
			title: 'Security',
			items: [{
				xtype: 'investment-security-forms-with-dynamic-fields',
				labelWidth: 120,

				items: [
					{fieldLabel: 'Investment Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
					{xtype: 'investment-instrument-ticker'},
					{fieldLabel: 'CUSIP', name: 'cusip'},
					{fieldLabel: 'ISIN', name: 'isin'},
					{fieldLabel: 'SEDOL', name: 'sedol'},
					{fieldLabel: 'FIGI', name: 'figi', qtip: 'Financial Instrument Global Identifier (FIGI; also known as the Bloomberg Global Identifier (BBGID)) is an open standard, 12 character unique identifier assigned to all financial instruments and will not change once issued. For equity instruments, an identifier is issued per trading venue.'},
					{fieldLabel: 'OCC Symbol', name: 'occSymbol', qtip: 'OCC Symbol is a unique code used to identify Options on a futures exchange.  Options Clearing Corporation\'s (OCC) Options Symbology Initiative (OSI) mandated an industry-wide change to this methodology in 2010. We use it to reconcile and confirm trades with external parties.'},
					{xtype: 'investment-instrument-underlying-combo', name: 'instrument.underlyingInstrument.labelShort', hiddenName: 'instrument.underlyingInstrument.id'},
					{fieldLabel: 'Security Name', name: 'name'},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 40},
					{fieldLabel: 'First Trade', name: 'startDate', xtype: 'datefield'},
					{fieldLabel: 'Last Trade', name: 'endDate', xtype: 'datefield'},

					{xtype: 'label', html: '<hr/>'},
					{boxLabel: 'Illiquid (cannot be easily sold or exchanged for cash)', name: 'illiquid', xtype: 'checkbox'},
					{
						fieldLabel: 'BICS Industry Subgroup', xtype: 'system-hierarchy-combo', tableName: 'InvestmentInstrument', hierarchyCategoryName: 'BICS Hierarchy',
						qtip: 'Bloomberg Industry Classification System (BICS)',
						getFkFieldId: function(fp) {
							return fp.getFormValue('instrument.id', true);
						}
					},
					{
						fieldLabel: 'GICS Sub-industry', xtype: 'system-hierarchy-combo', tableName: 'InvestmentInstrument', hierarchyCategoryName: 'GICS Hierarchy',
						qtip: 'Global Industry Classification Standard (GICS)',
						getFkFieldId: function(fp) {
							return fp.getFormValue('instrument.id', true);
						}
					},
					{
						fieldLabel: 'Issuer', name: 'businessCompany.name', hiddenName: 'businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true', detailPageClass: 'Clifton.business.company.CompanyWindow',
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const companyType = combo.getParentForm().getFormValue('instrument.hierarchy.businessCompanyType.name');
								combo.store.baseParams = companyType ? {companyType: companyType} : {};
							}
						}
					},
					{fieldLabel: 'Primary Exchange', name: 'instrument.exchange.label', hiddenName: 'instrument.exchange.id', xtype: 'combo', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=false', detailPageClass: 'Clifton.investment.setup.ExchangeWindow', qtip: 'The main stock exchange where a publicly traded company\'s stock is bought and sold.'},
					{fieldLabel: 'Composite Exchange', name: 'instrument.compositeExchange.label', hiddenName: 'instrument.compositeExchange.id', xtype: 'combo', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=true', detailPageClass: 'Clifton.investment.setup.ExchangeWindow', qtip: 'Composite Exchange is not a real exchange. It is used to calculate average or standardized prices for a security across all exchanges where it trades.'},
					{fieldLabel: 'Country of Risk', name: 'instrument.countryOfRisk.text', hiddenName: 'instrument.countryOfRisk.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
					{fieldLabel: 'Country of Incorporation', name: 'instrument.countryOfIncorporation.text', hiddenName: 'instrument.countryOfIncorporation.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
					{fieldLabel: 'CCY Denomination', name: 'instrument.tradingCurrency.name', hiddenName: 'instrument.tradingCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', allowBlank: false},
					{fieldLabel: 'Price Multiplier', name: 'instrument.priceMultiplier', xtype: 'floatfield'},
					{fieldLabel: 'Exposure Multiplier', name: 'instrument.exposureMultiplier', xtype: 'floatfield', qtip: 'Exposure Multiplier used for a few futures (Euro dollar and Euro Yen = 4) where the exposure is different than the notional value and needs to be accounted for by this multiplier. Example: Euro Dollar has contract size of $1,000,000 (multiplied by 4 to get annualized equivalent of quarterly interest rate: 12 / 3 = 4). Exposure = Price * Quantity * Multiplier * 4 where 4 is the exposure multiplier'},
					{
						fieldLabel: 'Notional Calculator', name: 'costCalculator', hiddenName: 'costCalculator', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
						store: new Ext.data.ArrayStore({
							fields: ['name', 'value', 'description'],
							data: Clifton.investment.instrument.NotionalCalculators
						})
					},
					{
						fieldLabel: 'M2M Calculator', name: 'm2mCalculator', hiddenName: 'm2mCalculator', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
						store: new Ext.data.ArrayStore({
							fields: ['name', 'value', 'description'],
							data: Clifton.investment.instrument.M2MCalculators
						})
					},

					// margin fields:
					{xtype: 'label', html: '<hr/>'},
					{
						fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
						defaults: {
							xtype: 'displayfield',
							flex: 1
						},
						items: [
							{value: 'Speculator'},
							{value: 'Hedger'}
						]
					},
					{
						fieldLabel: 'Initial Margin', xtype: 'compositefield',
						defaults: {
							xtype: 'currencyfield',
							flex: 1
						},
						items: [
							{name: 'instrument.speculatorInitialMarginPerUnit'},
							{name: 'instrument.hedgerInitialMarginPerUnit'}
						]
					},
					{
						fieldLabel: 'Maintenance Margin', xtype: 'compositefield',
						defaults: {
							xtype: 'currencyfield',
							flex: 1
						},
						items: [
							{name: 'instrument.speculatorSecondaryMarginPerUnit'},
							{name: 'instrument.hedgerSecondaryMarginPerUnit'}
						]
					}
				]
			}]
		},


		{
			title: 'Events',
			items: [{
				name: 'investmentSecurityEventListFind',
				xtype: 'gridpanel',
				instructions: 'A security event represents an instance of a specific event type for a specific security. For example, specific stock split, dividend payment, etc .',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Type', width: 100, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', displayField: 'name', url: 'investmentSecurityEventTypeListFind.json'}},
					{header: 'Additional Security', width: 100, dataIndex: 'additionalSecurity.label', filter: {type: 'combo', searchFieldName: 'additionalSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
					{header: 'Before Value', width: 60, dataIndex: 'beforeEventValue', type: 'float'},
					{header: 'After Value', width: 60, dataIndex: 'afterEventValue', type: 'float'},
					{header: 'Declare Date', width: 50, dataIndex: 'declareDate', hidden: true},
					{header: 'Ex Date', width: 50, dataIndex: 'exDate', hidden: true},
					{header: 'Record Date', width: 50, dataIndex: 'recordDate', hidden: true},
					{header: 'Event Date', width: 50, dataIndex: 'eventDate'}
				],
				editor: {
					detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow',
					getDefaultData: function(gridPanel) {
						return this.savedDefaultData;
					},
					addToolbarAddButton: function(toolBar, gridPanel) {
						toolBar.add(new TCG.form.ComboBox({name: 'eventType', url: 'investmentSecurityEventTypeListFind.json', width: 150, listWidth: 230}));
						toolBar.add({
							text: 'Add',
							tooltip: 'Add an event of selected type to this security',
							iconCls: 'add',
							scope: this,
							handler: function() {
								const eventType = TCG.getChildByName(toolBar, 'eventType');
								const eventTypeId = eventType.getValue();
								if (eventTypeId === '') {
									TCG.showError('You must first select desired Event Type from the list.');
								}
								else {
									this.savedDefaultData = {
										security: gridPanel.getWindow().getMainForm().formValues,
										type: {
											id: eventTypeId,
											name: eventType.lastSelectionText
										}
									};
									this.openDetailPage(this.getDetailPageClass(), gridPanel);
								}
							}
						});
						toolBar.add('-');
					}
				},
				getLoadParams: function() {
					return {'securityId': this.getWindow().getMainFormId()};
				}
			}]
		},

		{
			title: 'Symbol Overrides',
			items: [{xtype: 'marketdata-datasource-security-override-grid'}]
		},


		{
			title: 'Notes',
			items: [{
				xtype: 'investment-security-system-note-grid'
			}]
		},


		{
			title: 'Instrument Groups',
			layout: 'border',
			items: [
				{
					xtype: 'investment-instrumentGroupGrid_ByInstrument',
					region: 'north',
					height: 250,
					securityWindow: true
				},
				{
					xtype: 'investment-instrumentGroupMissingGrid_ByInstrument',
					region: 'center',
					securityWindow: true
				}
			]
		},


		{
			title: 'Tasks',
			items: [{
				xtype: 'workflow-task-grid',
				tableName: 'InvestmentSecurity'
			}]
		}
	]
});
