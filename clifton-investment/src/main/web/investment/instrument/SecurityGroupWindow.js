Clifton.investment.instrument.SecurityGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Group',
	iconCls: 'grouping',
	width: 900,
	height: 500,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Group',
				items: [{
					xtype: 'formpanel',
					instructions: 'An investment security group classifies securities for specific purposes. System Defined group names cannot be changed.',
					url: 'investmentSecurityGroup.json',
					listeners: {
						afterload: function() {
							this.updateRebuildFields(TCG.getValue('rebuildSystemBean.id', this.getForm().formValues));
						}
					},
					updateRebuildFields: function(rebuildSystemBeanId) {
						const f = this.getForm();
						const disableRebuildFields = TCG.isBlank(rebuildSystemBeanId);
						const rebuildOnNewSecurityInsertField = f.findField('rebuildOnNewSecurityInsert');
						if (disableRebuildFields) {
							rebuildOnNewSecurityInsertField.setValue(false);
						}
						rebuildOnNewSecurityInsertField.setDisabled(disableRebuildFields);
					},
					items: [
						{fieldLabel: 'Group Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{xtype: 'system-entity-modify-condition-combo'},
						{
							fieldLabel: 'Rebuild Bean', name: 'rebuildSystemBean.name', hiddenName: 'rebuildSystemBean.id', xtype: 'combo', detailPageClass: 'Clifton.system.bean.BeanWindow', url: 'systemBeanListFind.json?groupName=Investment Security Group Rebuild Executor',
							qtip: 'Use to created a System Managed group that will use the rules defined by selected Rebuilt Bean to automatically update the list of securities.',
							getDefaultData: function() {
								return {type: {group: {name: 'Investment Security Group Rebuild Executor'}}};
							},
							listeners: {
								// reset schedule amount label/remove field enable/disable tiers based on selection
								change: function(field, newValue) {
									field.ownerCt.updateRebuildFields(newValue);
								}
							}
						},
						{boxLabel: 'Rebuild the group immediately on new security insert (use only when necessary)', name: 'rebuildOnNewSecurityInsert', xtype: 'checkbox'},
						{boxLabel: 'Allow the same instrument to have more than one securities assigned to the group', name: 'securityFromSameInstrumentAllowed', xtype: 'checkbox'},
						{boxLabel: 'System Defined groups are used by the System and cannot have their name changed', name: 'systemDefined', xtype: 'checkbox', disabled: true},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'InvestmentSecurityGroup',
							hierarchyCategoryName: 'Investment Security Group Tags'
						}
					]
				}]
			},


			{
				title: 'Included Securities',
				items: [{
					name: 'investmentSecurityListFind',
					xtype: 'gridpanel',
					instructions: 'The following securities are associated with this group.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 20, dataIndex: 'id', filter: false, hidden: true},
						{header: 'Investment Type', width: 65, dataIndex: 'instrument.hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Sub Type', width: 60, dataIndex: 'instrument.hierarchy.investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}, hidden: true},
						{header: 'Sub Type 2', width: 60, dataIndex: 'instrument.hierarchy.investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}, hidden: true},
						{header: 'Investment Hierarchy', width: 160, dataIndex: 'instrument.hierarchy.labelExpanded', defaultSortColumn: true, filter: {searchFieldName: 'hierarchyName'}},
						{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean'},
						{header: 'Symbol', width: 50, dataIndex: 'symbol'},
						{header: 'CUSIP', width: 40, dataIndex: 'cusip', hidden: true},
						{header: 'ISIN', width: 60, dataIndex: 'isin', hidden: true},
						{header: 'SEDOL', width: 45, dataIndex: 'sedol', hidden: true},
						{
							header: 'FIGI', width: 55, dataIndex: 'figi', hidden: true,
							tooltip: 'Financial Instrument Global Identifier (FIGI; also known as the Bloomberg Global Identifier (BBGID)) is an open standard, 12 character unique identifier assigned to all financial instruments and will not change once issued. For equity instruments, an identifier is issued per trading venue.'
						},
						{header: 'OCC Symbol', width: 45, dataIndex: 'occSymbol', hidden: true},
						{header: 'Security Name', width: 100, dataIndex: 'name'},
						{header: 'CCY Denomination', hidden: true, width: 60, dataIndex: 'instrument.tradingCurrency.name', filter: {type: 'combo', searchFieldName: 'tradingCurrencyId', displayField: 'name', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Last Trade', width: 50, dataIndex: 'endDate'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
						deleteURL: 'investmentSecurityGroupSecurityDelete.json',
						getDeleteParams: function(selectionModel) {
							return {
								groupId: this.getWindow().getMainFormId(),
								securityId: selectionModel.getSelected().get('id')
							};
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							const grpId = gridPanel.getWindow().getMainFormId();
							const systemManaged = TCG.getValue('systemManaged', gridPanel.getWindow().getMainForm().formValues);
							if (systemManaged === true) {
								toolBar.add({
									text: 'Rebuild',
									tooltip: 'Rebuild securities for this group',
									iconCls: 'run',
									handler: function() {
										TCG.data.getDataPromise('investmentSecurityGroupRebuild.json', gridPanel, {
											waitMsg: 'Rebuilding...',
											params: {groupId: grpId}
										}).then(result => {
											TCG.createComponent('Clifton.core.StatusWindow', {
												title: 'Security Group Rebuild Status',
												defaultData: {status: result}
											});
											gridPanel.reload();
										});
									}
								});
								toolBar.add('-');
							}
							else {
								toolBar.add(new TCG.form.ComboBox({name: 'sec', url: 'investmentSecurityListFind.json?excludeInvestmentSecurityGroupId=' + grpId, displayField: 'label', width: 150, listWidth: 230}));
								toolBar.add({
									text: 'Add',
									tooltip: 'Add selected security to this group',
									iconCls: 'add',
									handler: function() {
										const secId = TCG.getChildByName(toolBar, 'sec').getValue();
										if (TCG.isBlank(secId)) {
											TCG.showError('You must first select desired security from the list.');
										}
										else {
											const loader = new TCG.data.JsonLoader({
												waitTarget: gridPanel,
												waitMsg: 'Linking...',
												params: {securityId: secId, groupId: grpId},
												onLoad: function(record, conf) {
													gridPanel.reload();
													TCG.getChildByName(toolBar, 'sec').reset();
												}
											});
											loader.load('investmentSecurityGroupToSecurityLink.json');
										}
									}
								});
								toolBar.add('-');
							}
						}
					},
					getTopToolbarInitialLoadParams: function() {
						return {'investmentSecurityGroupId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Overlapping Securities',
				items: [{
					name: 'investmentSecurityGroupSecurityListFind',
					xtype: 'gridpanel',
					instructions: 'Includes securities from other Security Groups that also belong to (overlap with) this Security Group.',
					topToolbarSearchParameter: 'searchPattern',
					additionalPropertiesToRequest: 'referenceTwo.id',
					columns: [
						{header: 'Security Group', width: 100, dataIndex: 'referenceOne.name', filter: {searchFieldName: 'investmentSecurityGroupName'}},
						{header: 'Investment Type', width: 65, dataIndex: 'referenceTwo.instrument.hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}, hidden: true},
						{header: 'Sub Type', width: 60, dataIndex: 'referenceTwo.instrument.hierarchy.investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}, hidden: true},
						{header: 'Sub Type 2', width: 60, dataIndex: 'referenceTwo.instrument.hierarchy.investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}, hidden: true},
						{header: 'Investment Hierarchy', width: 130, dataIndex: 'referenceTwo.instrument.hierarchy.labelExpanded', filter: {searchFieldName: 'hierarchyName'}},
						{header: 'Active', width: 40, dataIndex: 'referenceTwo.active', type: 'boolean', filter: false, sortable: false},
						{header: 'Symbol', width: 50, dataIndex: 'referenceTwo.symbol', filter: {searchFieldName: 'symbol'}, defaultSortColumn: true},
						{header: 'CUSIP', width: 40, dataIndex: 'referenceTwo.cusip', filter: {searchFieldName: 'cusip'}, hidden: true},
						{header: 'ISIN', width: 60, dataIndex: 'referenceTwo.isin', filter: {searchFieldName: 'isin'}, hidden: true},
						{header: 'SEDOL', width: 45, dataIndex: 'referenceTwo.sedol', filter: {searchFieldName: 'sedol'}, hidden: true},
						{
							header: 'FIGI', width: 55, dataIndex: 'referenceTwo.figi', filter: {searchFieldName: 'figi'}, hidden: true,
							tooltip: 'Financial Instrument Global Identifier (FIGI; also known as the Bloomberg Global Identifier (BBGID)) is an open standard, 12 character unique identifier assigned to all financial instruments and will not change once issued. For equity instruments, an identifier is issued per trading venue.'
						},
						{header: 'OCC Symbol', width: 45, dataIndex: 'referenceTwo.occSymbol', filter: {searchFieldName: 'occSymbol'}, hidden: true},
						{header: 'Security Name', width: 100, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'securityName'}},
						{header: 'CCY Denomination', hidden: true, width: 60, dataIndex: 'referenceTwo.instrument.tradingCurrency.name', filter: {type: 'combo', searchFieldName: 'currencyDenominationId', displayField: 'name', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Last Trade', width: 50, dataIndex: 'referenceTwo.endDate', filter: {searchFieldName: 'securityEndDate'}}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Investment Security Group Tags'},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function() {
						const params = {'overlappingSecurityGroupId': this.getWindow().getMainFormId()};
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.category2Name = 'Investment Security Group Tags';
							params.category2TableName = 'InvestmentSecurityGroup';
							params.category2LinkFieldPath = 'referenceOne';
							params.category2HierarchyId = tag.getValue();

						}
						return params;
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.referenceTwo.id;
						}
					}
				}]
			},


			{
				title: 'Missing Instruments',
				items: [{
					name: 'investmentInstrumentListFind',
					xtype: 'gridpanel',
					instructions: 'The following instruments do not have a security assigned to this group.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 20, dataIndex: 'id', filter: false, hidden: true},
						{header: 'Investment Type', width: 50, dataIndex: 'hierarchy.investmentType.name', filter: {type: 'combo', showNotEquals: true, searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Sub Type', width: 60, dataIndex: 'hierarchy.investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}, hidden: true},
						{header: 'Sub Type 2', width: 60, dataIndex: 'hierarchy.investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}, hidden: true},
						{header: 'Investment Hierarchy', width: 200, dataIndex: 'hierarchy.labelExpanded', defaultSortColumn: true, filter: {type: 'combo', searchFieldName: 'hierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Prefix', width: 60, dataIndex: 'identifierPrefix'},
						{header: 'Instrument Name', width: 100, dataIndex: 'name'},
						{header: 'Underlying', hidden: true, width: 100, dataIndex: 'underlyingInstrument.name', filter: {type: 'combo', searchFieldName: 'underlyingInstrumentId', displayField: 'name', url: 'investmentInstrumentListFind.json'}},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Primary Exchange', hidden: true, width: 100, dataIndex: 'exchange.label', filter: {type: 'combo', searchFieldName: 'exchangeId', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=false'}},
						{header: 'CCY Denomination', hidden: true, width: 80, dataIndex: 'tradingCurrency.name', filter: {type: 'combo', searchFieldName: 'tradingCurrencyId', displayField: 'name', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Multiplier', hidden: true, width: 50, dataIndex: 'priceMultiplier', type: 'float'},
						{header: 'Deliverable', hidden: true, width: 50, dataIndex: 'deliverable', type: 'boolean'},
						{header: 'Inactive', width: 40, dataIndex: 'inactive', type: 'boolean'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.investment.instrument.InstrumentWindow'
					},
					getTopToolbarInitialLoadParams: function() {
						return {'missingInvestmentSecurityGroupId': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});

