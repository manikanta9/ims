TCG.use('Clifton.investment.instrument.event.BaseEventWindow');

Clifton.investment.instrument.event.SymbolChangeEventWindow = Ext.extend(Clifton.investment.instrument.event.BaseEventWindow, {
	titlePrefix: 'Symbol Change Event',
	height: 520,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [{
			title: 'Event',
			items: [{
				xtype: 'formpanel',
				instructions: 'Symbol change represents creation of new security that is identical to the old one but with a different symbol. The old security is de-activated and the new security is created. All existing positions are closed with no realized gains and reopen immediately with proper original transaction date.',
				url: 'investmentSecurityEvent.json',
				labelWidth: 130,
				items: Clifton.investment.instrument.event.ExchangeBasedEventColumns,
				fieldOverrides: [
					{fieldLabel: 'Ex Date', name: 'exDate', xtype: 'datefield', hidden: false}
				]
			}]
		}]
	}]
});
