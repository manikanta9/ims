TCG.use('Clifton.investment.instrument.event.BaseEventWindow');

Clifton.investment.instrument.event.TenderOfferWindow = Ext.extend(Clifton.investment.instrument.event.BaseEventWindow, {
	titlePrefix: 'Security Event',
	height: 580,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Event',
				items: [{
					xtype: 'formpanel',
					instructions: 'A security event represents an instance of a specific event type for a specific security. For example, specific stock split, dividend payment, etc .  Payments are represented as cash value per share.<br />The party that holds position (based on <b>Trade Date</b> or <b>Settlement Date</b> depending on event journal type) at the end of day prior to <b>Begin Date</b>, is entitled to this event.',
					url: 'investmentSecurityEvent.json',
					items: [
						{fieldLabel: 'CA ID', name: 'corporateActionIdentifier', xtype: 'numberfield', decimalPrecision: 0, qtip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.'},
						{fieldLabel: 'Event Type', name: 'type.name', detailIdField: 'type.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow'},
						{fieldLabel: 'Event Status', name: 'status.name', hiddenName: 'status.id', xtype: 'combo', url: 'investmentSecurityEventStatusListFind.json', detailPageClass: 'Clifton.investment.instrument.event.EventStatusWindow', addNew: false},
						{fieldLabel: 'Security', name: 'security.label', detailIdField: 'security.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{
							fieldLabel: 'Additional Security', name: 'additionalSecurity.label', hiddenName: 'additionalSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
							beforequery: function(queryEvent) {
								queryEvent.combo.store.baseParams = {
									eventTypeId: queryEvent.combo.getParentForm().getForm().findField('type.id').getValue()
								};
							}
						},
						{fieldLabel: 'Tender Rate', name: 'afterEventValue'},
						{fieldLabel: 'Declare Date', name: 'declareDate', xtype: 'datefield'},
						{fieldLabel: 'Begin Date', name: 'exDate', xtype: 'datefield'},
						{fieldLabel: 'Expiration Date', name: 'recordDate', xtype: 'datefield'},
						{
							fieldLabel: 'Estimated Payable Date', name: 'eventDate', xtype: 'datefield',
							tooltip: 'Payment for tendered shares is received “promptly” after expiration date (refer to SEC Regulation <b>17 CFR 240.14e-1(c)</b> for exact language). If a payout date is not specified by offeror, expiration date is given as best estimate.'
						},
						{fieldLabel: 'Additional Date', name: 'additionalDate', xtype: 'datefield'},
						{fieldLabel: 'Event Description', name: 'eventDescription', xtype: 'textarea', height: 50},
						{name: 'voluntary', xtype: 'checkbox', boxLabel: 'The holder of the security has an option to choose to participate or not participate in this event'},
						{name: 'taxable', xtype: 'checkbox', boxLabel: 'This event subject to additional taxes affecting net payout amounts and booking rules'}
					]
				}]
			},

			{
				title: 'Payouts',
				items: [{xtype: 'investment-event-payout-grid'}]
			},


			{
				title: 'Client Elections',
				items: [{
					xtype: 'investment-event-client-elections-grid'
				}]
			}
		]
	}]
});
