TCG.use('Clifton.investment.instrument.event.BaseEventWindow');

Clifton.investment.instrument.event.EquityLegPaymentEventWindow = Ext.extend(Clifton.investment.instrument.event.BaseEventWindow, {
	titlePrefix: 'Equity Leg Payment Event',
	height: 470,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [{
			title: 'Event',
			items: [{
				xtype: 'formpanel',
				instructions: 'Equity Leg Payment for a Total Return Swap (TRS) is based on Underlying price change from Valuation Date (reset period start) to Next Valuation Date (reset period end) and is calculated per Unit of position held.',
				url: 'investmentSecurityEvent.json',
				labelWidth: 125,
				items: [
					{fieldLabel: 'Event Type', name: 'type.name', detailIdField: 'type.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow'},
					{fieldLabel: 'Security', name: 'security.label', detailIdField: 'security.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
					{xtype: 'label', html: '<hr />'},
					{fieldLabel: 'Valuation Price', name: 'beforeEventValue', xtype: 'floatfield', qtip: 'Equity Leg Underlying Price on Valuation Date'},
					{fieldLabel: 'Next Valuation Price', name: 'afterEventValue', xtype: 'floatfield', qtip: 'Equity Leg Underlying Price on Next Valuation Date'},
					{xtype: 'label', html: '<hr />'},
					{fieldLabel: 'Valuation Date', name: 'declareDate', xtype: 'datefield', qtip: 'Valuation price for the beginning of reset period is looked up on this date'},
					{fieldLabel: 'Next Valuation Date', name: 'recordDate', xtype: 'datefield', qtip: 'Valuation price for the end of reset period is looked up on this date'},
					{fieldLabel: 'Ex Date', name: 'exDate', xtype: 'datefield'},
					{fieldLabel: 'Payment Date', name: 'eventDate', xtype: 'datefield'},
					{fieldLabel: 'Payment Currency', name: 'additionalSecurity.label', hiddenName: 'additionalSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true', qtip: 'Equity Leg Payment is calculated in Local Currency of TRS but can be paid (settled) in a different currency specified here. Leave blank to use currency denomination of corresponding security.'},
					{fieldLabel: 'Event Description', name: 'eventDescription', xtype: 'textarea', height: 50}
				]
			}]
		}]
	}]
});

