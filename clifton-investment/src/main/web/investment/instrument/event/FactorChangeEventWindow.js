TCG.use('Clifton.investment.instrument.event.BaseEventWindow');

Clifton.investment.instrument.event.FactorChangeEventWindow = Ext.extend(Clifton.investment.instrument.event.BaseEventWindow, {
	titlePrefix: 'Factor Change Event',
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [{
			title: 'Event',
			items: [{
				xtype: 'formpanel',
				instructions: 'For asset backed securities, factor change represents the change (usually reduction) of outstanding principal usually due to early mortgage prepayment. Value of 1 represents full principal and value of 0 represents no principal left.<br />The party that holds position (based on <b>Settlement Date</b>) at the end of day prior to <b>Ex Date</b>, is entitled to the Factor Change payment. In most cases Ex Date is one day after Accrual End date except for British bonds which have negative accrual from Ex to Accrual End date. Optional <b>Loss Percent</b> is based on Original Face.',
				url: 'investmentSecurityEvent.json',
				items: [
					{fieldLabel: 'Event Type', name: 'type.name', detailIdField: 'type.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow'},
					{fieldLabel: 'Security', name: 'security.label', detailIdField: 'security.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
					{fieldLabel: 'Previous Factor', name: 'beforeEventValue'},
					{fieldLabel: 'New Factor', name: 'afterEventValue'},
					{fieldLabel: 'Loss Percent', name: 'additionalEventValue'},
					{fieldLabel: 'Accrual Start', name: 'declareDate', xtype: 'datefield'},
					{fieldLabel: 'Accrual End', name: 'recordDate', xtype: 'datefield'},
					{fieldLabel: 'Ex Date', name: 'exDate', xtype: 'datefield'},
					{fieldLabel: 'Payment Date', name: 'eventDate', xtype: 'datefield'},
					{fieldLabel: 'Event Description', name: 'eventDescription', xtype: 'textarea', height: 50}
				]
			}]
		}]
	}]
});
