TCG.use('Clifton.investment.instrument.event.BaseEventWindow');

Clifton.investment.instrument.event.StockSplitEventWindow = Ext.extend(Clifton.investment.instrument.event.BaseEventWindow, {
	titlePrefix: 'Stock Split Event',
	height: 550,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Event',
				items: [{
					xtype: 'formpanel',
					instructions: 'Stock split is an increase or decrease in the number of shares in the public company (2 for 1, 3 for 1, 5 for 2, etc.). When split happens, quantities of existing positions held as well as historic stock prices need to be adjusted.',
					url: 'investmentSecurityEvent.json',
					items: [
						{fieldLabel: 'CA ID', name: 'corporateActionIdentifier', xtype: 'numberfield', decimalPrecision: 0, qtip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.'},
						{fieldLabel: 'Event Type', name: 'type.name', detailIdField: 'type.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow'},
						{fieldLabel: 'Event Status', name: 'status.name', hiddenName: 'status.id', xtype: 'combo', url: 'investmentSecurityEventStatusListFind.json', detailPageClass: 'Clifton.investment.instrument.event.EventStatusWindow', addNew: false},
						{fieldLabel: 'Security', name: 'security.label', detailIdField: 'security.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Start Shares', name: 'beforeEventValue', xtype: 'floatfield'},
						{fieldLabel: 'End Shares', name: 'afterEventValue', xtype: 'floatfield'},
						{fieldLabel: 'Declare Date', name: 'declareDate', xtype: 'datefield'},
						{fieldLabel: 'Ex Date', name: 'exDate', xtype: 'datefield'},
						{fieldLabel: 'Record Date', name: 'recordDate', xtype: 'datefield'},
						{fieldLabel: 'Event Date', name: 'eventDate', xtype: 'datefield'},
						{fieldLabel: 'Event Description', name: 'eventDescription', xtype: 'textarea', height: 70},
						{fieldLabel: 'Order Override', name: 'bookingOrderOverride', qtip: 'This order will override the default order for generating journals for events of this type.'},
						{name: 'voluntary', xtype: 'checkbox', boxLabel: 'The holder of the security has an option to choose to participate or not participate in this event'},
						{name: 'taxable', xtype: 'checkbox', boxLabel: 'This event subject to additional taxes affecting net payout amounts and booking rules'}
					]
				}]
			},

			{
				title: 'Payouts',
				items: [{xtype: 'investment-event-payout-grid'}]
			}
		]
	}]
});
