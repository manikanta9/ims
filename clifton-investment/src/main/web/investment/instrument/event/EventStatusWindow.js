Clifton.investment.instrument.event.EventStatusWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Event Status',
	iconCls: 'event',
	height: 400,
	width: 650,

	items: [{
		xtype: 'formpanel',
		url: 'investmentSecurityEventStatus.json',
		instructions: 'Event Status is used to define a life-cycle of an event.  Event is always in a specific status that may change over time: Incomplete => Approved.',
		readOnly: true,
		items: [
			{fieldLabel: 'Status Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{boxLabel: 'Allow manual generation of event journals for events in this status', name: 'eventJournalAllowed', xtype: 'checkbox'},
			{boxLabel: 'Automatically generate event journals for events in this status (Approved)', name: 'eventJournalAutomatic', xtype: 'checkbox'},
			{boxLabel: 'Ignore events in this status (Cancelled, Deleted)', name: 'eventJournalNotApplicable', xtype: 'checkbox'}
		]
	}]
});
