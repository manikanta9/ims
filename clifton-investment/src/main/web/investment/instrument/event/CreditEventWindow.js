TCG.use('Clifton.investment.instrument.event.BaseEventWindow');

Clifton.investment.instrument.event.CreditEventWindow = Ext.extend(Clifton.investment.instrument.event.BaseEventWindow, {
	titlePrefix: 'Credit Event',
	height: 540,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [{
			title: 'Event',
			items: [{
				xtype: 'formpanel',
				instructions: 'For Credit Default Swaps, credit event represents a default by one or more entities in the underlying index. It results in a factor change similar to asset backed bonds which will reduce the notional of existing position. Recovery Rate is determined at the auction that occurs after Default Date but before Payment Date.<br />On payment date, the party that is long protection will receive the payment equal to reduction of notional minus amount recovered. The same party will also pay accrued interest from current accrual period start to and including Default Date on the amount equal to notional reduction.',
				url: 'investmentSecurityEvent.json',
				items: [
					{fieldLabel: 'Event Type', name: 'type.name', detailIdField: 'type.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow'},
					{fieldLabel: 'Security', name: 'security.label', detailIdField: 'security.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
					{fieldLabel: 'Constituent', name: 'additionalSecurity.label', hiddenName: 'additionalSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', qtip: 'Underlying Reference Entity that has triggered a Credit Event. It is assigned a zero percent weighting after processing.'},
					{fieldLabel: 'Previous Factor', name: 'beforeEventValue'},
					{fieldLabel: 'New Factor', name: 'afterEventValue'},
					{fieldLabel: 'Recovery Rate (%)', name: 'additionalEventValue', allowBlank: false, qtip: 'Assets of the entity that defaulted are auctioned after Default Date but before Payment Date. This is a percentage of assets being recovered. For example 20%.'},
					{fieldLabel: 'Accrual Start', name: 'declareDate', xtype: 'datefield', qtip: 'Accrual Start and End dates should match these of the corresponding Premium Leg Payment period that the Default Date falls in'},
					{fieldLabel: 'Accrual End', name: 'recordDate', xtype: 'datefield'},
					{fieldLabel: 'Default Date', name: 'additionalDate', xtype: 'datefield', qtip: 'Accrued interest is accrued up to and including this date using the old factor.'},
					{fieldLabel: 'Ex Date', name: 'exDate', xtype: 'datefield', qtip: 'One day after auction date. New Factor (notional) starts on this day.'},
					{fieldLabel: 'Payment Date', name: 'eventDate', xtype: 'datefield', qtip: 'The date when the money from the default is received/paid.'},
					{fieldLabel: 'Event Description', name: 'eventDescription', xtype: 'textarea', height: 50}
				]
			}]
		}]
	}]
});
