TCG.use('Clifton.investment.instrument.event.BaseEventWindow');

Clifton.investment.instrument.event.ExchangeEventWindow = Ext.extend(Clifton.investment.instrument.event.BaseEventWindow, {
	titlePrefix: 'Exchange-Based Event',
	height: 520,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [{
			title: 'Event',
			items: [{
				xtype: 'formpanel',
				instructions: 'Exchange-based events represent cases where the symbol (ticker) remains the same, while other aspects of the security change (e.g. exchange, company name/description, or identifiers—SEDOL, ISIN, CUSIP, etc.) on transaction date. No new security is created and the symbol remains the same with positions unchanged.',
				url: 'investmentSecurityEvent.json',
				labelWidth: 130,
				items: Clifton.investment.instrument.event.ExchangeBasedEventColumns
			}]
		}]
	}]
});
