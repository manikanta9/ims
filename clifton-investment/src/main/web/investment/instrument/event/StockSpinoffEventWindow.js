TCG.use('Clifton.investment.instrument.event.BaseEventWindow');

Clifton.investment.instrument.event.StockSpinoffEventWindow = Ext.extend(Clifton.investment.instrument.event.BaseEventWindow, {
	titlePrefix: 'Stock Spinoff Event',
	height: 610,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Event',
				items: [{
					xtype: 'formpanel',
					instructions: 'A spinoff is the creation of an independent company through the sale or distribution of new shares of an existing business or division of a parent company by distributing 100% of its ownership interest as a stock dividend to existing shareholders or offering a discount to exchange shares in the parent company for shares of the spinoff company. The event will result in new allocation of cost basis for the parent and spinoff company determined by the relative market values measured at completion date (Event Date) closing prices.',
					url: 'investmentSecurityEvent.json',
					labelWidth: 130,
					items: [
						{fieldLabel: 'CA ID', name: 'corporateActionIdentifier', xtype: 'numberfield', decimalPrecision: 0, qtip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.'},
						{fieldLabel: 'Event Type', name: 'type.name', detailIdField: 'type.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow'},
						{fieldLabel: 'Event Status', name: 'status.name', hiddenName: 'status.id', xtype: 'combo', url: 'investmentSecurityEventStatusListFind.json', detailPageClass: 'Clifton.investment.instrument.event.EventStatusWindow', addNew: false},
						{fieldLabel: 'Parent Security', name: 'security.label', detailIdField: 'security.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Spinoff Security', name: 'additionalSecurity.label', hiddenName: 'additionalSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', qtip: '???'},
						{fieldLabel: 'Parent Shares', name: 'beforeEventValue', xtype: 'floatfield', qtip: 'Distribution Ratio: for this many Parent Shares get the following number of Spinoff Shares'},
						{fieldLabel: 'Spinoff Shares', name: 'afterEventValue', xtype: 'floatfield', qtip: 'Distribution Ratio: get this number of Spinoff Shares for each preceding number of Parent Shares'},
						{fieldLabel: 'Adjustment Factor', name: 'additionalEventValue', qtip: 'Optional parent security Cost Basis adjustment factor (multiplier). The cost basis of parent security will change by this adjustment factor and the cost basis for spinoff security will get the remaining cost basis. If this value is not defined, the system will calculated it using Event Date prices and shares Distribution Ratio.'},
						{fieldLabel: 'Declare Date', name: 'declareDate', xtype: 'datefield'},
						{fieldLabel: 'Ex Date', name: 'exDate', xtype: 'datefield'},
						{fieldLabel: 'Record Date', name: 'recordDate', xtype: 'datefield'},
						{fieldLabel: 'Event Date', name: 'eventDate', xtype: 'datefield'},
						{fieldLabel: 'Event Description', name: 'eventDescription', xtype: 'textarea', height: 70},
						{fieldLabel: 'Order Override', name: 'bookingOrderOverride', qtip: 'This order will override the default order for generating journals for events of this type.'},
						{name: 'voluntary', xtype: 'checkbox', boxLabel: 'The holder of the security has an option to choose to participate or not participate in this event'},
						{name: 'taxable', xtype: 'checkbox', boxLabel: 'This event subject to additional taxes affecting net payout amounts and booking rules'}
					]
				}]
			},


			{
				title: 'Payouts',
				items: [{xtype: 'investment-event-payout-grid'}]
			},


			{
				title: 'Client Elections',
				items: [{
					xtype: 'investment-event-client-elections-grid'
				}]
			}
		]
	}]
});
