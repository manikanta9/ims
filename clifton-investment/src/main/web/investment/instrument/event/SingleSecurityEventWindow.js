TCG.use('Clifton.investment.instrument.event.SecurityEventWindow');


// Extends SecurityEventWindow, but allows opening a single event window (based on event type)
// instead of a grouped window (for resets).  When in the reset window, want to still be able to drill into a specific event)
// We need a separate class so that the UI doesn't think the window is already open with the id of the event was
// used to open the grouped window
Clifton.investment.instrument.event.SingleSecurityEventWindow = Ext.extend(Clifton.investment.instrument.event.SecurityEventWindow, {
	openForEventTypeNameOnly: true

});
