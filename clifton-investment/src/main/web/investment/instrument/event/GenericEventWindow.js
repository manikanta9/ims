TCG.use('Clifton.investment.instrument.event.BaseEventWindow');

Clifton.investment.instrument.event.GenericEventWindow = Ext.extend(Clifton.investment.instrument.event.BaseEventWindow, {
	titlePrefix: 'Security Event',
	height: 580,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Event',
				items: [{
					xtype: 'formpanel',
					instructions: 'A security event represents an instance of a specific event type for a specific security. For example, specific stock split, dividend payment, etc .  Payments are represented as cash value per share.<br />The party that holds position (based on <b>Trade Date</b> or <b>Settlement Date</b> depending on event journal type) at the end of day prior to <b>Ex Date</b>, is entitled to this event.',
					url: 'investmentSecurityEvent.json',
					items: [
						{fieldLabel: 'CA ID', name: 'corporateActionIdentifier', xtype: 'numberfield', decimalPrecision: 0, qtip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.'},
						{fieldLabel: 'Event Type', name: 'type.name', detailIdField: 'type.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow'},
						{fieldLabel: 'Event Status', name: 'status.name', hiddenName: 'status.id', xtype: 'combo', url: 'investmentSecurityEventStatusListFind.json', detailPageClass: 'Clifton.investment.instrument.event.EventStatusWindow', addNew: false},
						{fieldLabel: 'Security', name: 'security.label', detailIdField: 'security.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{
							fieldLabel: 'Additional Security', name: 'additionalSecurity.label', hiddenName: 'additionalSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
							beforequery: function(queryEvent) {
								queryEvent.combo.store.baseParams = {
									eventTypeId: queryEvent.combo.getParentForm().getForm().findField('type.id').getValue()
								};
							}
						},
						{fieldLabel: 'Before Value', name: 'beforeEventValue'},
						{fieldLabel: 'After Value', name: 'afterEventValue'},
						{fieldLabel: 'Declare Date', name: 'declareDate', xtype: 'datefield'},
						{fieldLabel: 'Ex Date', name: 'exDate', xtype: 'datefield'},
						{fieldLabel: 'Record Date', name: 'recordDate', xtype: 'datefield'},
						{fieldLabel: 'Event Date', name: 'eventDate', xtype: 'datefield'},
						{
							fieldLabel: 'Actual Settle', name: 'actualSettlementDate', xtype: 'datefield', allowBlank: true, requiredFields: ['eventDate'],
							qtip: 'The actual settlement (wire) date.',
							validateValue: function(value) {
								const fp = TCG.getParentFormPanel(this);
								return fp.validateActualSettlementDate();
							}
						},
						{fieldLabel: 'Additional Date', name: 'additionalDate', xtype: 'datefield'},
						{fieldLabel: 'Event Description', name: 'eventDescription', xtype: 'textarea', height: 50},
						{name: 'voluntary', xtype: 'checkbox', boxLabel: 'The holder of the security has an option to choose to participate or not participate in this event'},
						{name: 'taxable', xtype: 'checkbox', boxLabel: 'This event subject to additional taxes affecting net payout amounts and booking rules'}
					],
					listeners: {
						afterload: function(formpanel) {
							const form = formpanel.getForm();
							const actualSettlementDate = form.findField('actualSettlementDate');
							const flag = form.findField('security.instrument.hierarchy.includeAccrualReceivables');
							if (flag?.value === false) {
								formpanel.removeField(actualSettlementDate);
							}
						}
					},
					validateActualSettlementDate: function() {
						const f = this.getForm();
						const actualSettlementDateField = f.findField('actualSettlementDate');
						if (!actualSettlementDateField.el.dom.readOnly) {
							const settlementDateField = f.findField('eventDate');
							const actualSettlementDateValue = actualSettlementDateField.getValue();
							const settlementDateValue = settlementDateField.getValue();
							if (actualSettlementDateValue && settlementDateValue && actualSettlementDateValue < settlementDateValue) {
								actualSettlementDateField.markInvalid('Actual Settlement Date cannot be before the Payment Date.');
								return false;
							}
						}
						return true;
					}
				}]
			},


			{
				title: 'Payouts',
				items: [{xtype: 'investment-event-payout-grid'}]
			},


			{
				title: 'Client Elections',
				items: [{
					xtype: 'investment-event-client-elections-grid'
				}]
			}
		]
	}]
});
