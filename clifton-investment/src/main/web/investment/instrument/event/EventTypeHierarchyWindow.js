Clifton.investment.instrument.event.EventTypeHierarchyWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Event Type Hierarchy Mapping',
	iconCls: 'event',
	height: 300,
	width: 650,

	items: [{
		xtype: 'formpanel',
		url: 'investmentSecurityEventTypeHierarchy.json',
		instructions: 'Specifies what Security Event Type\'s are allowed for a given investment Hierarchy.',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Event Type', name: 'referenceOne.name', hiddenName: 'referenceOne.id', xtype: 'combo', url: 'investmentSecurityEventTypeListFind.json', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow'},
			{fieldLabel: 'Investment Hierarchy', name: 'referenceTwo.labelExpanded', hiddenName: 'referenceTwo.id', xtype: 'combo', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', displayField: 'labelExpanded', queryParam: 'labelExpanded', listWidth: 600},
			{
				fieldLabel: 'Report', name: 'eventReport.name', hiddenName: 'eventReport.id', xtype: 'combo', detailPageClass: 'Clifton.report.ReportWindow'
				, qtip: 'Report selection when viewing Event Journal Details for this event type/hierarchy selection.'
				, url: 'reportListFind.json?categoryName=Report Template Tags&categoryTableName=ReportTemplate&categoryLinkFieldPath=reportTemplate&categoryHierarchyName=Event Journals'
			},
			{boxLabel: 'Copy events of selected type from the underlying', name: 'copiedFromUnderlying', xtype: 'checkbox', labelSeparator: '', qtip: 'Based on this flag, the system will copy events; such as credit events, from the underlying listed on the instrument of the given security.  Consistency of events will be maintained through the underlying listed on the instrument of the given security; including deletions or updates. If a user is unable to delete or update the event on the underlying, he or she must unbook the particular event on the dependent security.'}
		]
	}]
});
