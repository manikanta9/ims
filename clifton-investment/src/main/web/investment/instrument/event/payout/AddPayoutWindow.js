Clifton.investment.instrument.event.payout.AddPayoutWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Security Event Payout Creation',
	iconCls: 'add',
	height: 200,
	width: 500,
	modal: true,
	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'Specify values for adding a payout for a Security Event.',
			items: [
				{
					xtype: 'panel',
					layout: 'form',
					// labelWidth: 150,
					items: [
						{fieldLabel: 'Event', name: 'securityEvent.label', detailIdField: 'securityEvent.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow'},
						{fieldLabel: 'Event Type', name: 'securityEvent.type.name', detailIdField: 'securityEvent.type.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow'},
						{
							fieldLabel: 'Payout Type', name: 'payoutType.name', hiddenName: 'payoutType.id', xtype: 'combo', displayField: 'referenceOne.name', valueField: 'referenceOne.id', tooltipField: 'referenceOne.label', url: 'investmentSecurityEventPayoutTypeAssignmentListFind.json', emptyText: '< Select Payout Type >', allowBlank: false,
							requestedProps: 'referenceOne.id|referenceOne.name|referenceOne.label',
							listeners: {
								beforequery: function(qEvent) {
									const store = qEvent.combo.store;
									const eventTypeId = TCG.getParentFormPanel(qEvent.combo).getFormValue('securityEvent.type.id');
									store.setBaseParam('eventTypeId', eventTypeId);
								}
							}
						}
					]
				}
			]
		}
	],
	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const window = this;
		const payoutTypeId = form.findField('payoutType.id').getValue();
		if (TCG.isBlank(payoutTypeId)) {
			TCG.showError('Please select a payout type before selecting OK.', this.title);
			return;
		}
		TCG.data.getDataPromise('investmentSecurityEventPayoutType.json?id=' + payoutTypeId, form)
			.then(type => {
				TCG.createComponent('Clifton.investment.instrument.event.payout.SecurityEventPayoutWindow', {
					defaultData: {
						securityEvent: form.formValues.securityEvent,
						payoutType: type
					},
					openerCt: this.openerCt
				});
				if (closeOnSuccess) {
					window.closeWindow();
				}
			});
	}
});
