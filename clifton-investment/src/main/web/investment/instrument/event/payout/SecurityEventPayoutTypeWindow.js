Clifton.investment.instrument.event.payout.SecurityEventPayoutTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Event Payout Type',
	iconCls: 'event',
	height: 450,
	width: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Payout Type',
				items: [{
					xtype: 'formpanel',
					instructions: 'Security event payout types define types of payout that apply to security events (Currency, Security, etc.). Each security event can have more than one payout and these payouts can be of different types. Payout types are assigned to corresponding event types to limit valid choices.',
					url: 'investmentSecurityEventPayoutType.json',
					readOnly: true,
					labelWidth: 120,
					items: [
						{fieldLabel: 'Payout Type Name', name: 'name'},
						{fieldLabel: 'Payout Code', name: 'typeCode'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 90},
						{boxLabel: 'Use only one value (before and after values are the same)', name: 'beforeSameAsAfter', xtype: 'checkbox'},
						{boxLabel: 'Additional security is a currency (dividend currency, etc.)', name: 'additionalSecurityCurrency', xtype: 'checkbox'},
						{boxLabel: 'Additional security is used for value units (as opposed to local currency)', name: 'eventValueInEventCurrencyUnits', xtype: 'checkbox', qtip: 'When Additional Security is Currency, specifies if the Before and After Value are in the Event currency units or security currency units. For example, "Cash Dividend Payment" event type will have this field set to true, meaning the dividend per share amount (Before/After Value) is in Additional Security units (Event currency) and the exchange rate is from Event currency to Client Account base currency.'},
						{boxLabel: 'Create a new position and close existing one (stock split, spin-off, etc.)', name: 'newPositionCreated', xtype: 'checkbox'},
						{boxLabel: 'Create a new investment security (stock spin-off)', name: 'newSecurityCreated', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Event Type Assignments',
				items: [{
					name: 'investmentSecurityEventPayoutTypeAssignmentListFind',
					xtype: 'gridpanel',
					instructions: 'The following investment security event types are allowed to have this payout type.',
					additionalPropertiesToRequest: 'id|referenceTwo.id',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Security Event Type', width: 100, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'eventTypeName'}, defaultSortColumn: true},
						{header: 'Description', width: 300, dataIndex: 'referenceTwo.description'}
					],
					getLoadParams: function() {
						return {payoutTypeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.referenceTwo.id;
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'eventType', url: 'investmentSecurityEventTypeListFind.json', width: 150}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Allow this payout type for selected security event type',
								iconCls: 'add',
								scope: this,
								handler: function() {
									const eventTypeId = TCG.getChildByName(toolBar, 'eventType').getValue();
									if (TCG.isBlank(eventTypeId)) {
										TCG.showError('You must first select desired Security Event Type from the list.');
									}
									else {
										const payoutTypeId = gridPanel.getWindow().getMainFormId();
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Assigning...',
											params: {eventTypeId: eventTypeId, payoutTypeId: payoutTypeId},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'eventType').reset();
											}
										});
										loader.load('investmentSecurityEventPayoutTypeAssignmentLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			}
		]
	}]
});
