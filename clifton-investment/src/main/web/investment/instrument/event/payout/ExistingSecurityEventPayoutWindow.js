Clifton.investment.instrument.event.payout.ExistingSecurityEventPayoutWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security (Existing) Payout',
	iconCls: 'event',
	height: 550,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Payout',
				items: [{
					xtype: 'formpanel',
					instructions: 'A single security event may have multiple payouts or elections. All payouts with the same "Election Number" are interdependent and are booked together. Lists all available options for selected event.',
					url: 'investmentSecurityEventPayout.json',
					labelWidth: 140,
					items: [
						{fieldLabel: 'Security Event', name: 'securityEvent.label', detailIdField: 'securityEvent.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow'},
						{fieldLabel: 'Payout Type', name: 'payoutType.label', detailIdField: 'payoutType.id', xtype: 'linkfield', displayField: 'label', detailPageClass: 'Clifton.investment.instrument.event.payout.SecurityEventPayoutTypeWindow'},
						{fieldLabel: 'Election Number', name: 'electionNumber', xtype: 'integerfield'},
						{fieldLabel: 'Payout Number', name: 'payoutNumber', xtype: 'integerfield'},
						{fieldLabel: 'Payout Security', name: 'payoutSecurity.label', hiddenName: 'payoutSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json?active=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', disableAddNewItem: true},
						{
							fieldLabel: 'Payout per Share', name: 'beforeEventValue', xtype: 'floatfield',
							listeners: {
								change: function(field, newValue, oldValue) {
									const fp = TCG.getParentFormPanel(field);
									fp.getForm().findField('afterEventValue').setValue(newValue);
								}
							}
						},
						{fieldLabel: 'After Value', name: 'afterEventValue', xtype: 'floatfield', hidden: true},
						{fieldLabel: 'Payout Security Price', name: 'additionalPayoutValue', xtype: 'floatfield'},
						{fieldLabel: 'Payout Date', name: 'additionalPayoutDate', xtype: 'datefield'},
						{fieldLabel: 'Fractional Shares', name: 'fractionalSharesMethod.name', hiddenName: 'fractionalSharesMethod', xtype: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.investment.instrument.event.FractionalShares}, qtip: 'The following values will result in corresponding special treatment of fractional shares: "Cash in Lieu", "Round Up", "Round Half Up", "Round Down".  Any other values will result in Cash in Lieu.'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Default Election', name: 'defaultElection', xtype: 'checkbox'},
						{fieldLabel: 'DTC Only', name: 'dtcOnly', xtype: 'checkbox', qtip: 'If checked, indicates that the election requires that a position for the event\'s security to be held in a holding account whose issuer is a DTC participant.'},
						{fieldLabel: 'Deleted', name: 'deleted', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Client Elections',
				items: [{
					xtype: 'investment-event-client-elections-grid'
				}]
			}
		]
	}]
});
