// not the actual window but a window selector for the SecurityEventPayoutWindow, based on the payout type.
Clifton.investment.instrument.event.payout.SecurityEventPayoutWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'investmentSecurityEventPayout.json',

	getClassName: function(config, entity) {
		const payoutTypeName = entity ? TCG.getValue('payoutType.name', entity) : TCG.getValue('defaultData.payoutType.name', config);
		const eventTypeName = entity ? TCG.getValue('securityEvent.type.name', entity) : TCG.getValue('defaultData.securityEvent.type.name', config);

		let className;
		if (Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides[eventTypeName]) {
			className = Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides[eventTypeName][payoutTypeName] ?
				Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides[eventTypeName][payoutTypeName] :
				Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides['Default'][payoutTypeName];
		}
		else {
			className = Clifton.investment.instrument.event.SecurityEventPayoutWindowClassOverrides['Default'][payoutTypeName];
		}

		if (!className) {
			TCG.showError('Unsupported Security Event Payout screen: [Event Type:' + eventTypeName + ', Payout Type: ' + payoutTypeName + '].', 'Investment Security Event Payout');
			return false;
		}
		return className;
	}
});
