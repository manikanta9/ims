Clifton.investment.instrument.event.payout.TenderOfferCurrencyEventPayoutWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Currency Payout (Dutch Auction)',
	iconCls: 'event',
	height: 550,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Payout',
				items: [{
					xtype: 'formpanel',
					instructions: 'A single security event may have multiple payouts or elections. All payouts with the same "Election Number" are interdependent and are booked together. Lists all available options for selected event.',
					url: 'investmentSecurityEventPayout.json',
					items: [
						{fieldLabel: 'Security Event', name: 'securityEvent.label', detailIdField: 'securityEvent.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow'},
						{fieldLabel: 'Payout Type', name: 'payoutType.label', detailIdField: 'payoutType.id', xtype: 'linkfield', displayField: 'label', detailPageClass: 'Clifton.investment.instrument.event.payout.SecurityEventPayoutTypeWindow'},
						{fieldLabel: 'Election Number', name: 'electionNumber', xtype: 'integerfield'},
						{fieldLabel: 'Payout Number', name: 'payoutNumber', xtype: 'integerfield'},
						{fieldLabel: 'Payout Currency', name: 'payoutSecurity.label', hiddenName: 'payoutSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json?active=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', disableAddNewItem: true},
						{
							fieldLabel: 'Payout per Share', name: 'beforeEventValue', xtype: 'floatfield',
							listeners: {
								change: function(field, newValue, oldValue) {
									const fp = TCG.getParentFormPanel(field);
									fp.getForm().findField('afterEventValue').setValue(newValue);
								}
							}
						},
						{fieldLabel: 'After Value', name: 'afterEventValue', xtype: 'floatfield', hidden: true},
						{fieldLabel: 'Payout Date', name: 'additionalPayoutDate', xtype: 'datefield'},
						{fieldLabel: 'Additional Value', name: 'additionalValue', xtype: 'floatfield', hidden: true},
						{fieldLabel: 'Additional Value 2', name: 'additionalValue2', xtype: 'floatfield', hidden: true},
						{fieldLabel: 'Proration Rate', name: 'prorationRate', xtype: 'floatfield'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Default Election', name: 'defaultElection', xtype: 'checkbox'},
						{fieldLabel: 'Deleted', name: 'deleted', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Client Elections',
				items: [{
					xtype: 'investment-event-client-elections-grid'
				}]
			}
		]
	}]
});
