Clifton.investment.instrument.event.payout.SecurityEventClientElectionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Event Client Election',
	width: 900,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Election',
				items: [{
					xtype: 'formpanel',
					instructions: 'For corporate actions that offer elections, identifies specific payout elections that each client that held corresponding position made. Election Quantity is usually equal to the total quantity held. However, it is possible to split existing position across more than one election.',
					url: 'investmentSecurityEventClientElection.json',
					labelFieldName: 'electionNumber',
					items: [
						{fieldLabel: 'Security Event', name: 'securityEvent.label', detailIdField: 'securityEvent.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow'},
						{
							fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', xtype: 'combo', displayField: 'label',
							url: 'accountingBalanceClientAccountListFind.json', detailPageClass: 'Clifton.investment.account.AccountWindow', disableAddNewItem: true, requestedProps: 'id|label',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const win = combo.ownerCt.getWindow();
									const isDtcOnly = win.defaultData.dtcOnly;
									const investmentSecurityId = win.defaultData.securityEvent.security.id;
									const exDate = TCG.parseDate(win.defaultData.securityEvent.exDate).add(Date.DAY, -1).format('m/d/Y');

									combo.store.baseParams = {
										accountingAccountIdName: 'POSITION_ACCOUNTS_EXCLUDE_RECEIVABLE_COLLATERAL',
										includeUnderlyingPrice: false,
										includeZeroLocalBalances: false,
										investmentSecurityId: investmentSecurityId,
										transactionDate: exDate,
										useHoldingAccountBaseCurrency: false
									};

									if (TCG.isNotBlank(isDtcOnly) && TCG.isTrue(isDtcOnly)) {
										combo.store.baseParams.dtcParticipant = true;
									}
								}
							}
						},
						{fieldLabel: 'Election Number', name: 'electionNumber', xtype: 'integerfield'},
						{fieldLabel: 'Election Quantity', name: 'electionQuantity', xtype: 'floatfield', qtip: 'Empty Election Quantity, means total quantity held by the client (most common election).'},
						{fieldLabel: 'Election Value', name: 'electionValue', xtype: 'floatfield', qtip: 'Election Value will be used to allow clients to declare a bid on Dutch Auctions for consideration by the Event Offeror.'}

					]
				}]
			},


			{
				title: 'Payouts',
				items: [{
					xtype: 'investment-event-payout-grid',
					getColumnOverrides: () => [
						{dataIndex: 'electionNumber', width: 50},
						{dataIndex: 'payoutNumber', width: 46},
						{dataIndex: 'payoutType.name', width: 56},
						{dataIndex: 'payoutSecurity.symbol', width: 62},
						{dataIndex: 'beforeEventValue', width: 60},
						{dataIndex: 'afterEventValue', width: 53},
						{dataIndex: 'additionalPayoutValue', width: 70},
						{dataIndex: 'additionalPayoutDate', width: 60},
						{dataIndex: 'description', width: 65}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.payout.SecurityEventPayoutWindow',
						getDefaultData: function(gridPanel, row, className, itemText) {
							return {
								securityEvent: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function() {
						const fp = this.getWindow().getMainFormPanel();
						return {
							securityEventId: fp.getFormValue('securityEvent.id'),
							electionNumber: fp.getFormValue('electionNumber')
						};
					}
				}]
			}
		]
	}]
});
