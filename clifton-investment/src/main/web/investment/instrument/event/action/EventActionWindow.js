Clifton.investment.instrument.event.action.EventActionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Event Action',
	iconCls: 'event',
	height: 400,
	readOnly: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'An action associated with an Investment Security Event.  Investment Security Event Actions track whether an action of a given type was executed for a given security event.',
		url: 'investmentSecurityEventAction.json',
		labelWidth: 120,

		getFormLabel: function() {
			return this.getFormValue('actionType.name') + ': ' + this.getFormValue('securityEvent.label');
		},

		items: [
			{fieldLabel: 'Action Type', name: 'actionType.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.action.EventActionTypeWindow', detailIdField: 'actionType.id'},
			{fieldLabel: 'Security Event', name: 'securityEvent.type.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow', detailIdField: 'securityEvent.id'},
			{fieldLabel: 'Event Status', name: 'securityEvent.status.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventStatusWindow', detailIdField: 'securityEvent.status.id'},
			{fieldLabel: 'Security', name: 'securityEvent.security.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'securityEvent.security.id'},
			{fieldLabel: 'Event Date', name: 'securityEvent.eventDate', xtype: 'datefield', readOnly: true},
			{fieldLabel: 'Processed Date', name: 'securityEvent.eventDate', xtype: 'datefield', readOnly: true},
			{boxLabel: 'Manual Action', name: 'actionType.manual', xtype: 'checkbox', disabled: true, qtip: 'If checked, indicates that this is a manual action type used where special processing is required. Manual event actions are manually added to an Investment Security Event and processed manually. Examples may be security event actions related to OTC Securities, or Price Baskets.'}
		]
	}]
});
