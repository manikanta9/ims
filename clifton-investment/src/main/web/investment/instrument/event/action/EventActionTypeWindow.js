Clifton.investment.instrument.event.action.EventActionTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Event Action Type',
	iconCls: 'event',
	height: 400,

	items: [{
		xtype: 'formpanel',
		instructions: 'Action Types define what actions and in what order must be executed for this security event type.',
		url: 'investmentSecurityEventActionType.json',
		labelWidth: 120,
		items: [
			{fieldLabel: 'Event Type', name: 'eventType.name', hiddenName: 'eventType.id', xtype: 'combo', url: 'investmentEventTypeListFind.json', detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow', detailIdField: 'eventType.id'},
			{fieldLabel: 'Action Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{
				fieldLabel: 'Action Bean', name: 'actionBean.label', hiddenName: 'actionBean.id', displayField: 'name', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Investment Security Event Action Processor', detailPageClass: 'Clifton.system.bean.BeanWindow', allowBlank: true,
				getDefaultData: function() {
					return {type: {group: {name: 'Investment Security Event Action Processor'}}};
				}
			},
			{fieldLabel: 'Processing Order', name: 'processingOrder', xtype: 'spinnerfield', qtip: 'If more than one action is defined for an event, use this field to define the order of execution.'},
			{boxLabel: 'Process actions before Event Journal generation (uncheck to process after)', name: 'processBeforeEventJournal', xtype: 'checkbox'},
			{boxLabel: 'Manual Action', name: 'manual', xtype: 'checkbox', qtip: 'If checked, indicates that this is a manual action type used where special processing is required. Manual event actions are manually added to an Investment Security Event and processed manually. Examples may be security event actions related to OTC Securities, or Price Baskets.'}
		],
	}]
});
