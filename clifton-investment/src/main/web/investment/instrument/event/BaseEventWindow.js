Clifton.investment.instrument.event.BaseEventWindow = Ext.extend(TCG.app.DetailWindow, {
	iconCls: 'event',
	width: 1000,
	height: 470,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);

		const eventType = w.defaultData.type;
		const instrument = w.defaultData.security.instrument;
		if (eventType.name === TCG.getValue('hierarchy.accrualSecurityEventType.name', instrument)) {
			let h = TCG.getValue('accrualMethod', w.defaultData.security);
			if (!h) {
				h = TCG.getValue('accrualMethod', instrument);
				if (!h) {
					h = TCG.getValue('hierarchy.accrualMethod', instrument);
				}
			}
			if (h && h !== 'DAYCOUNT') {
				tabs.add(Clifton.investment.instrument.event.EventDetailsTab);
			}
		}
		else if (eventType.name === TCG.getValue('hierarchy.accrualSecurityEventType2.name', instrument)) {
			const h = TCG.getValue('hierarchy.accrualMethod2', instrument);
			if (h && h !== 'DAYCOUNT') {
				tabs.add(Clifton.investment.instrument.event.EventDetailsTab);
			}
		}

		if (TCG.isTrue(eventType.actionAllowed)) {
			tabs.add(Clifton.investment.instrument.event.EventActionsTab);
		}

		const arr = Clifton.investment.instrument.event.AdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	}
});


Clifton.investment.instrument.event.EventDetailsTab = {
	title: 'Event Details',
	items: [{
		name: 'investmentSecurityEventDetailListByEvent',
		xtype: 'gridpanel',
		instructions: 'Defines details for events that can have their parameters change during event. For example, daily/weekly interest rate changes.',
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{header: 'Effective Date', width: 50, dataIndex: 'effectiveDate'},
			{header: 'Reference Rate (%)', width: 50, dataIndex: 'referenceRate', type: 'float', useNull: true},
			{header: 'Spread (BPS)', width: 50, dataIndex: 'spread', type: 'float', useNull: true},
			{header: 'Effective Rate (%)', width: 50, dataIndex: 'effectiveRate', type: 'float', useNull: true}
		],
		editor: {
			detailPageClass: 'Clifton.investment.instrument.event.SecurityEventDetailWindow',
			getDefaultData: function(gridPanel) {
				return {
					event: gridPanel.getWindow().getMainForm().formValues
				};
			}
		},
		getLoadParams: function() {
			return {
				securityEventId: this.getWindow().getMainFormId()
			};
		}
	}]
};


Clifton.investment.instrument.event.EventActionsTab = {
	title: 'Event Actions',
	items: [{
		name: 'investmentSecurityEventActionListFind',
		xtype: 'gridpanel',
		instructions: 'A list of event action(s) that have been processed for this security event.',
		includeBeforeEventJournal: true,
		includeAfterEventJournal: true,
		standardColumns: [],
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{header: 'Action Type', width: 200, dataIndex: 'actionType.name', filter: {searchFieldName: 'actionTypeName'}},
			{header: 'Processing Order', width: 50, dataIndex: 'actionType.processingOrder', type: 'int', defaultSortColumn: true, filter: {searchFieldName: 'processingOrder'}},
			{header: 'Processed Date', width: 40, dataIndex: 'processedDate'}
		],
		editor: {
			addEditButtons: function(toolBar, gridPanel) {
				toolBar.add({
					text: 'Processing',
					iconCls: 'run',
					menu: new Ext.menu.Menu({
						items: [{
							text: 'Process Action(s)',
							tooltip: 'Process the action(s) for this event',
							iconCls: 'run',
							scope: gridPanel,
							handler: function() {
								gridPanel.processEventActions(false, gridPanel);
							}
						}, {
							text: 'Rollback Action(s)',
							tooltip: 'Undo the action(s) for this event',
							iconCls: 'remove',
							scope: gridPanel,
							handler: function() {
								gridPanel.processEventActions(true, gridPanel);
							}
						}, '-', {
							text: 'Include Before Event Journal Actions',
							xtype: 'menucheckitem',
							checked: true,
							handler: function(item, event) {
								gridPanel.includeBeforeEventJournal = !item.checked;
							}
						}, {
							text: 'Include After Event Journal Actions',
							xtype: 'menucheckitem',
							checked: true,
							handler: function(item, event) {
								gridPanel.includeAfterEventJournal = !item.checked;
							}
						}]
					})
				});
			}
		},
		processEventActions: function(rollBack, gridPanel) {
			const params = {securityEventId: gridPanel.getWindow().getMainFormId()};
			if (gridPanel.includeAfterEventJournal !== gridPanel.includeBeforeEventJournal) {
				params.processBeforeEventJournal = gridPanel.includeBeforeEventJournal;
			}
			const action = rollBack ? 'Rollback' : 'Processing';
			Ext.Msg.confirm('Event Action ' + action, 'Would you like to perform ' + action + ' action(s) for this security event?', function(a) {
				if (a === 'yes') {
					const loader = new TCG.data.JsonLoader({
						waitTarget: gridPanel,
						params: params,
						onLoad: function(record, conf) {
							TCG.createComponent('Clifton.core.StatusWindow', {
								defaultData: {status: record}
							});
							gridPanel.reload();
						}
					});
					loader.load(rollBack ? 'investmentSecurityEventActionsRollback.json' : 'investmentSecurityEventActionsProcess.json');
				}
			});
		},
		getLoadParams: function() {
			return {
				securityEventId: this.getWindow().getMainFormId()
			};
		}
	}]
};

Clifton.investment.instrument.event.ExchangeBasedEventColumns = [
	{fieldLabel: 'CA ID', name: 'corporateActionIdentifier', xtype: 'numberfield', decimalPrecision: 0, qtip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.'},
	{fieldLabel: 'Event Type', name: 'type.name', detailIdField: 'type.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow'},
	{fieldLabel: 'Event Status', name: 'status.name', hiddenName: 'status.id', xtype: 'combo', url: 'investmentSecurityEventStatusListFind.json', detailPageClass: 'Clifton.investment.instrument.event.EventStatusWindow', addNew: false},
	{fieldLabel: 'Security', name: 'security.label', detailIdField: 'security.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
	{fieldLabel: 'New Security', name: 'additionalSecurity.label', hiddenName: 'additionalSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
	{name: 'beforeEventValue', xtype: 'hidden', value: '1'},
	{name: 'afterEventValue', xtype: 'hidden', value: '1'},
	{fieldLabel: 'Announce Date', name: 'declareDate', xtype: 'datefield'},
	{name: 'exDate', xtype: 'datefield', hidden: true},
	{name: 'recordDate', xtype: 'datefield', hidden: true},
	{
		fieldLabel: 'Event Date', name: 'eventDate', xtype: 'datefield',
		listeners: { // Ex Date and Record Date are populated based on Event Date
			change: function(field, newValue, oldValue) {
				const form = TCG.getParentFormPanel(field).getForm();
				form.findField('recordDate').setValue(new Date(newValue).format('m/d/Y'));
				form.findField('exDate').setValue(new Date(newValue).add(Date.DAY, 1).format('m/d/Y'));
			},
			select: function(field, date) {
				const form = TCG.getParentFormPanel(field).getForm();
				form.findField('recordDate').setValue(date.format('m/d/Y'));
				form.findField('exDate').setValue(date.add(Date.DAY, 1).format('m/d/Y'));
			}
		}
	},
	{fieldLabel: 'Description', name: 'eventDescription', xtype: 'textarea', height: 30, grow: true}
];
