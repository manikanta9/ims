Clifton.investment.instrument.event.SecurityEventDetailWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Event Detail',
	iconCls: 'event',

	items: [{
		xtype: 'formpanel',
		instructions: 'Defines details for events that can have their parameters change during event. For example, daily/weekly interest rate changes.',
		url: 'investmentSecurityEventDetail.json',
		labelWidth: 125,
		labelFieldName: 'event.label',
		items: [
			{fieldLabel: 'Security Event', name: 'event.label', detailIdField: 'event.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow'},
			{fieldLabel: 'Effective Date', name: 'effectiveDate', xtype: 'datefield'},
			{fieldLabel: 'Reference Rate (%)', name: 'referenceRate', xtype: 'floatfield', allowBlank: false},
			{fieldLabel: 'Spread (BPS)', name: 'spread', xtype: 'floatfield'},
			{xtype: 'label', html: '<hr />'},
			{fieldLabel: 'Effective Rate (%)', name: 'effectiveRate', xtype: 'displayfield', type: 'float', style: 'text-align: right'}
		]
	}]
});
