// not the actual window but a window selector based on event.type.name meta-data
Clifton.investment.instrument.event.SecurityEventWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'investmentSecurityEvent.json',

	openForEventTypeNameOnly: false,

	getClassName: function(config, entity) {
		const type = entity ? entity.type.name : config.defaultData.type.name;
		const sec = entity ? entity.security : config.defaultData.security;

		let secType = undefined;
		let secSubType = undefined;

		// When in the Reset window and want to drill into a specific event, do not open the reset window, but the event specific window
		if (this.openForEventTypeNameOnly !== true) {
			if (sec && sec.instrument) {
				secType = sec.instrument.hierarchy.investmentType.name;
				secSubType = sec.instrument.hierarchy.investmentTypeSubType ? sec.instrument.hierarchy.investmentTypeSubType.name : '';
			}
		}

		let className = undefined;
		if (secSubType && secSubType !== '') {
			className = Clifton.investment.instrument.event.SecurityEventWindowClassOverrides[secType + '_' + secSubType + '_' + type];
		}
		if (!className && secType && secType !== '') {
			className = Clifton.investment.instrument.event.SecurityEventWindowClassOverrides[secType + '_' + type];
		}
		if (!className) {
			className = Clifton.investment.instrument.event.SecurityEventWindowClassOverrides[type];
		}
		if (!className) {
			className = 'Clifton.investment.instrument.event.GenericEventWindow';
		}
		return className;
	}

});
