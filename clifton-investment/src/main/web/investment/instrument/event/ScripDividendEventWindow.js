TCG.use('Clifton.investment.instrument.event.BaseEventWindow');

Clifton.investment.instrument.event.ScripDividendEventWindow = Ext.extend(Clifton.investment.instrument.event.BaseEventWindow, {
	titlePrefix: 'Scrip/DRIP Dividend Event',
	height: 590,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Event',
				items: [{
					xtype: 'formpanel',
					instructions: 'Dividend is a payment made by a corporation to its shareholders. Payments are represented as cash value or stock quantity per share.<br/>The party that holds position (based on <b>Trade Date</b>) at the end of day prior to <b>Ex Date</b>, is entitled to the dividend payment.',
					url: 'investmentSecurityEvent.json',
					labelWidth: 140,
					items: [
						{fieldLabel: 'CA ID', name: 'corporateActionIdentifier', xtype: 'numberfield', decimalPrecision: 0, qtip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.'},
						{fieldLabel: 'Event Type', name: 'type.name', detailIdField: 'type.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventTypeWindow'},
						{fieldLabel: 'Event Status', name: 'status.name', hiddenName: 'status.id', xtype: 'combo', url: 'investmentSecurityEventStatusListFind.json', detailPageClass: 'Clifton.investment.instrument.event.EventStatusWindow', addNew: false},
						{fieldLabel: 'Security', name: 'security.label', detailIdField: 'security.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{name: 'beforeEventValue', xtype: 'hidden', value: '0'},
						{fieldLabel: 'Payout per Share', name: 'afterEventValue', xtype: 'floatfield'},
						{fieldLabel: 'Declare Date', name: 'declareDate', xtype: 'datefield'},
						{fieldLabel: 'Ex Date', name: 'exDate', xtype: 'datefield'},
						{fieldLabel: 'Record Date', name: 'recordDate', xtype: 'datefield'},
						{fieldLabel: 'Event Date', name: 'eventDate', xtype: 'datefield'},
						{fieldLabel: 'Election Expiration Date', name: 'additionalDate', xtype: 'datefield'},
						{fieldLabel: 'Payout Security', name: 'additionalSecurity.label', hiddenName: 'additionalSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', qtip: '???'},
						{fieldLabel: 'Event Description', name: 'eventDescription', xtype: 'textarea', height: 70},
						{fieldLabel: 'Order Override', name: 'bookingOrderOverride', qtip: 'This order will override the default order for generating journals for events of this type.'},
						{name: 'voluntary', xtype: 'checkbox', boxLabel: 'The holder of the security has an option to choose to participate or not participate in this event'},
						{name: 'taxable', xtype: 'checkbox', boxLabel: 'This event subject to additional taxes affecting net payout amounts and booking rules'}
					]
				}]
			},


			{
				title: 'Payouts',
				items: [{xtype: 'investment-event-payout-grid'}]
			},


			{
				title: 'Client Elections',
				items: [{
					xtype: 'investment-event-client-elections-grid'
				}]
			}
		]
	}]
});
