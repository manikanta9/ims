Clifton.investment.instrument.event.EventTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Event Type',
	iconCls: 'event',
	height: 670,
	width: 750,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Event Type',
				items: [{
					xtype: 'formpanel',
					instructions: 'Security event types define types of events that apply to securities and need to be accounted for (stock splits, dividend payments, factor changes, etc.). Event types are assigned to Investment Hierarchy nodes in order to specify what types are allowed for specific securities.',
					url: 'investmentSecurityEventType.json',
					readOnly: true,
					labelWidth: 120,
					items: [
						{fieldLabel: 'Event Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 90},
						{
							xtype: 'columnpanel',
							columns: [
								{
									rows: [
										{fieldLabel: 'Processing Order', name: 'eventOrder', xtype: 'spinnerfield', minValue: 0, maxValue: 999},
										{fieldLabel: 'Min Value', name: 'minValue', xtype: 'floatfield'}
									]
								},
								{
									rows: [
										{fieldLabel: 'Decimal Precision', name: 'decimalPrecision', xtype: 'spinnerfield', minValue: 0},
										{fieldLabel: 'Max Value', name: 'maxValue', xtype: 'floatfield'}
									]
								}
							]
						},
						{
							fieldLabel: 'Events Before Start', name: 'eventBeforeStartType', hiddenName: 'eventBeforeStartType', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
							qtip: 'Number of events allowed before the security start date',
							store: new Ext.data.ArrayStore({
								fields: ['name', 'value', 'description'],
								data: [
									['None', 'NONE', 'No events before security start are allowed.'],
									['One', 'ONE', 'Maximum of one event before security start is allowed.'],
									['Many', 'MANY', 'Any amount of events before security start are allowed.']
								]
							})
						},
						{fieldLabel: 'Force Event Status', name: 'forceEventStatus.name', detailIdField: 'forceEventStatus.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.event.EventStatusWindow', qtip: 'All events of this type must be in this status and the status will be defaulted upon event create. Usually "Approved".'},
						{boxLabel: 'Use only one value (before and after values are the same)', name: 'beforeSameAsAfter', xtype: 'checkbox'},
						{boxLabel: 'Value fields are a percent of notional rather than the absolute amount', name: 'valuePercent', xtype: 'checkbox'},
						{boxLabel: 'Allow additional security for events of this type', name: 'additionalSecurityAllowed', xtype: 'checkbox'},
						{boxLabel: 'Additional security is a currency (dividend currency, etc.)', name: 'additionalSecurityCurrency', xtype: 'checkbox'},
						{boxLabel: 'Additional security is used for value units (as opposed to local currency)', name: 'eventValueInEventCurrencyUnits', xtype: 'checkbox', qtip: 'When Additional Security is Currency, specifies if the Before and After Value are in the Event currency units or security currency units. For example, "Cash Dividend Payment" event type will have this field set to true, meaning the dividend per share amount (Before/After Value) is in Additional Security units (Event currency) and the exchange rate is from Event currency to Client Account base currency.'},
						{boxLabel: 'Create a new position and close existing one (stock split, spin-off, etc.)', name: 'newPositionCreated', xtype: 'checkbox'},
						{boxLabel: 'Create a new investment security (stock spin-off)', name: 'newSecurityCreated', xtype: 'checkbox'},
						{boxLabel: 'Allow payment delay (accrual and then reversal and payment)', name: 'paymentDelayAllowed', xtype: 'checkbox'},
						{boxLabel: 'Allow only one event of this type per event date for each security', name: 'onePerEventDate', xtype: 'checkbox'},
						{boxLabel: 'Events that have no payouts (journals) but offer valuable information to the holder', name: 'withoutPayout', xtype: 'checkbox'},
						{boxLabel: 'Events with single payout (all event info is on the event: payout table is not used)', name: 'singlePayoutOnly', xtype: 'checkbox'},
						{boxLabel: 'Allow action(s) for security events of this type (adds Actions tab)', name: 'actionAllowed', xtype: 'checkbox'},
						{boxLabel: 'Description Included In Natural Key', name: 'descriptionIncludedInNaturalKey', xtype: 'checkbox', labelSeparator: '', qtip: 'Copy events by including description in the natural key lookup'}
					]
				}]
			},


			{
				title: 'Action Types',
				items: [{
					name: 'investmentSecurityEventActionTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Action Types define what actions and in what order must be executed for this security event type.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Action Bean', width: 300, dataIndex: 'actionBean.name'},
						{header: 'Process Before', width: 100, dataIndex: 'processBeforeEventJournal', type: 'boolean', tooltip: 'Process actions of this type before processing the corresponding Event Journal'},
						{header: 'Manual', width: 50, dataIndex: 'manual', type: 'boolean', tooltip: 'If checked, indicates that this is a manual action type used where special processing is required. Manual event actions are manually added to an Investment Security Event and processed manually. Examples may be security event actions related to OTC Securities, or Price Baskets.'},
						{header: 'Order', width: 70, dataIndex: 'processingOrder', type: 'int', defaultSortColumn: true}
					],
					getLoadParams: function() {
						return {eventTypeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.action.EventActionTypeWindow',
						getDefaultData: function(gridPanel) {
							return {eventType: gridPanel.getWindow().getMainForm().formValues};
						}
					}
				}]
			},


			{
				title: 'Hierarchy Assignments',
				items: [{
					name: 'investmentSecurityEventTypeHierarchyListByEventType',
					xtype: 'gridpanel',
					instructions: 'Securities in the following investment instrument hierarchies are allowed events of this type.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Hierarchy', width: 100, dataIndex: 'referenceTwo.labelExpanded', defaultSortColumn: true},
						{header: 'Report', width: 80, dataIndex: 'eventReport.name'},
						{header: 'Description', width: 150, dataIndex: 'referenceTwo.description', hidden: true}
					],
					getLoadParams: function() {
						return {eventTypeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.EventTypeHierarchyWindow',
						getDefaultData: function(gridPanel) {
							const dd = {
								referenceOne: gridPanel.getWindow().getMainForm().formValues
							};

							const t = gridPanel.getTopToolbar();
							const et = TCG.getChildByName(t, 'hierarchy');
							if (TCG.isNotBlank(et.getValue())) {
								dd.referenceTwo = {id: et.getValue(), labelExpanded: et.getRawValue()};
							}
							return dd;
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'hierarchy', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', displayField: 'labelExpanded', queryParam: 'labelExpanded', width: 150, listWidth: 600}));
							toolBar.add({
								text: 'Quick Add',
								tooltip: 'Quick addition to allow applying this event type to securities from selected hierarchy',
								iconCls: 'add',
								scope: this,
								handler: function() {
									const hierarchyId = TCG.getChildByName(toolBar, 'hierarchy').getValue();
									if (TCG.isBlank(hierarchyId)) {
										TCG.showError('You must first select desired Hierarchy from the list.');
									}
									else {
										const eventTypeId = gridPanel.getWindow().getMainFormId();
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Linking...',
											params: {instrumentHierarchyId: hierarchyId, eventTypeId: eventTypeId},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'hierarchy').reset();
											}
										});
										loader.load('investmentSecurityEventTypeHierarchyLink.json');
									}
								}
							});
							toolBar.add('-');
							toolBar.add({
								text: 'Add',
								tooltip: 'Add hierarchy type and also select a report',
								iconCls: 'add',
								scope: this,
								handler: function() {
									this.openDetailPage(this.getDetailPageClass(), gridPanel);
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},


			{
				title: 'Payout Type Assignments',
				items: [{
					name: 'investmentSecurityEventPayoutTypeAssignmentListFind',
					xtype: 'gridpanel',
					instructions: 'The following investment security event payout types are allowed for this event type.',
					additionalPropertiesToRequest: 'id|referenceOne.id',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Payout Type', width: 100, dataIndex: 'referenceOne.name', filter: {searchFieldName: 'payoutTypeName'}, defaultSortColumn: true},
						{header: 'Description', width: 300, dataIndex: 'referenceOne.description'}
					],
					getLoadParams: function() {
						return {eventTypeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.payout.SecurityEventPayoutTypeWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.referenceOne.id;
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'payoutType', url: 'investmentSecurityEventPayoutTypeListFind.json', width: 150}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Allow this security event type type to have selected payout type',
								iconCls: 'add',
								scope: this,
								handler: function() {
									const payoutTypeId = TCG.getChildByName(toolBar, 'payoutType').getValue();
									if (TCG.isBlank(payoutTypeId)) {
										TCG.showError('You must first select desired Event Payout Type from the list.');
									}
									else {
										const eventTypeId = gridPanel.getWindow().getMainFormId();
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Assigning...',
											params: {eventTypeId: eventTypeId, payoutTypeId: payoutTypeId},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'payoutType').reset();
											}
										});
										loader.load('investmentSecurityEventPayoutTypeAssignmentLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			}
		]
	}]
});
