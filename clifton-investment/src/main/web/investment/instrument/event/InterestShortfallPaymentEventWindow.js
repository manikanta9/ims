TCG.use('Clifton.investment.instrument.event.BaseEventWindow');

Clifton.investment.instrument.event.InterestShortfallPaymentEventWindow = Ext.extend(Clifton.investment.instrument.event.BaseEventWindow, {
	titlePrefix: 'Interest Shortfall Payment Event',

	// support title for both Shortfall Payment and Shortfall Reimbursement Payment
	init: function() {
		this.titlePrefix = this.defaultData.type.name;
		TCG.callSuper(this, 'init', arguments);
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [{
			title: 'Event',
			items: [{
				xtype: 'investment-security-event-form',
				instructions: 'The party that holds position (based on <b>Settlement Date</b>) at the end of day prior to <b>Ex Date</b>, is entitled to the coupon payment. In most cases Ex Date is one day after Accrual End date except for British bonds which have negative accrual from Ex to Accrual End date.',
				fieldOverrides: [
					{fieldLabel: 'Rate', name: 'afterEventValue', decimalPrecision: 10},
					{fieldLabel: 'Divisor', name: 'additionalEventValue', type: 'float', value: '1000000', useNull: true}
				]
			}]
		}]
	}]
});

