TCG.use('Clifton.investment.instrument.BaseInstrumentWindow');

/**
 * The instrument window for structured instruments.
 */
Clifton.investment.instrument.structure.StructuredInstrumentWindow = Ext.extend(Clifton.investment.instrument.BaseInstrumentWindow, {
	titlePrefix: 'Structured Index',
	instructions: 'A structured index instrument defines the structure for a specific index, i.e. Dow Jones-UBS Commodity Index.  Each security under this structured index instrument is a different version of the index (F2, F3, DCS, custom, etc).',

	excludedFields: ['underlyingInstrument.labelShort', 'bigInstrument.labelShort', {fieldLabel: 'BICS Industry Subgroup'}, 'countryOfRisk.text', 'countryOfIncorporation.text', 'compositeExchange.label', 'settlementCalendar.name', 'priceMultiplier', 'exposureMultiplier', 'costCalculator', 'm2mCalculator', 'accrualDateCalculator'],
	excludedTabs: ['Open Positions', 'Tasks'],

	getConfirmBeforeSaveMsg: function() {
		if (TCG.isNotBlank(this.getMainFormPanel().getIdFieldValue()) && TCG.isEquals('Structured Index - Model Portfolio', this.getMainFormPanel().getFormValue('hierarchy.instrumentScreen'))) {
			return 'Changing values can impact Portfolio Weight calculations. After changes are completed, please rebuild the allocations from the Portfolio Weights tab within the applicable investment securities.<br/><br/><b>Are you sure you want to continue saving changes?</b>';
		}
		return undefined;
	},

	getConfirmBeforeSaveMsgTitle: function() {
		return 'Rebuild Portfolio Weights';
	},


	customTabs: [{
		index: 2,
		item: {
			title: 'Structures',
			items: [{
				name: 'investmentInstrumentStructureListByInstrument',
				xtype: 'gridpanel',
				instructions: 'The following structures are associated with this instrument.  Only one structure can be active for a specific date.',
				columns: [
					{header: 'Start Date', width: 50, dataIndex: 'startDate'},
					{header: 'End Date', width: 50, dataIndex: 'endDate'},
					{header: 'Weight Calculator', width: 150, dataIndex: 'currentWeightCalculatorBean.name'},
					{
						header: 'Additional Multiplier', width: 80, dataIndex: 'additionalMultiplier', type: 'float',
						tooltip: 'Additional multiplier that can be applied to the weight calculation.<br><br><b>Example:</b>GSCI Commodity Indices use a GSCI Normalizing Constant. This field would = 1/GSCI Constant since it is always multiplied, not divided by it.'
					}
				],
				editor: {
					detailPageClass: 'Clifton.investment.instrument.structure.InstrumentStructureWindow',
					getDefaultData: function(gridPanel) { // defaults instrument/hierarchy
						return {
							instrument: gridPanel.getWindow().getMainForm().formValues
						};
					},
					copyURL: 'investmentInstrumentStructureCopy.json',
					copyButtonName: 'Copy Structure',
					copyButtonTooltip: 'Creates a copy of the selected structure, ending the copied one before selected start date if not already ended.',
					copyHandler: function(gridPanel) {
						const editor = this;

						const grid = this.grid;
						const sm = grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select an existing structure to copy.', 'No Row(s) Selected');
							return;
						}
						else if (sm.getCount() !== 1) {
							TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
							return;
						}
						Ext.Msg.confirm('Copy Selected Structure', 'Are you sure you would like to copy the selected structure?  You will be able to enter a new Start Date for the new version and the structure you are copying will be updated to end the day before if not already inactive.', function(a) {
							if (a === 'yes') {
								const id = sm.getSelected().id;
								editor.openDetailPage('Clifton.investment.instrument.structure.InstrumentStructureCopyWindow', editor.getGridPanel(), id);
							}
						});
					}
				},
				addToolbarButtons: function(toolBar, gridPanel) {
					toolBar.add({
						text: 'Add/Edit With Weight Upload',
						tooltip: 'Add a new structure, or edit an existing structure with ability to upload the instrument weights.',
						iconCls: 'upload',
						scope: this,
						handler: function() {
							const grid = gridPanel.grid;
							const sm = grid.getSelectionModel();
							if (sm.getCount() > 1) {
								TCG.showError('Multi-selection edits are not supported.  Please select one row.', 'NOT SUPPORTED');
								return;
							}
							let id = undefined;
							if (sm.getCount() === 1) {
								id = sm.getSelected().id;
							}
							const editor = gridPanel.editor;
							editor.openDetailPage('Clifton.investment.instrument.structure.InstrumentStructureUploadWindow', gridPanel, id);
						}
					});
				},
				getLoadParams: function() {
					return {
						'instrumentId': this.getWindow().getMainFormId(),
						requestedMaxDepth: 4
					};
				}
			}]
		}
	}]
});
