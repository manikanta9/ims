Clifton.investment.instrument.structure.InstrumentStructureUploadWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Structure',
	width: 800,
	height: 400,

	windowOnShow: function(w) {
		if (w.defaultData) {
			const screen = TCG.getValue('instrument.hierarchy.instrumentScreen', w.defaultData);
			w.getMainFormPanel().updateForInstrumentScreen(screen);
		}
	},


	items: [{
		xtype: 'formpanel',
		url: 'investmentInstrumentStructure.json',
		labelWidth: 160,
		fileUpload: true,
		instructions: 'Add/Update structure. Instead of manually entering instruments and weights, load them from a file.  Note: If editing an existing structure, the entire weight list will be overwritten with the list from the file.',

		tbar: [{
			text: 'Download Sample File',
			tooltip: 'Download sample weights file',
			iconCls: 'excel',
			handler: function() {
				const fp = TCG.getParentFormPanel(this);
				const params = {};
				if (TCG.isNotBlank(fp.getWindow().getMainFormId())) {
					params.instrumentStructureId = fp.getWindow().getMainFormId();
				}
				params.fileName = 'Investment Instrument Structure Weights Sample File';
				params.outputFormat = 'xls';
				TCG.downloadFile('investmentInstrumentStructureWeightFileDownload.json', params, this);
			}
		}],

		getSaveURL: function() {
			return 'investmentInstrumentStructureFileUpload.json?enableValidatingBinding=true';
		},

		afterload: function(panel) {
			// Screen Used Determines the Structure Type
			const f = panel.getForm();
			const screen = TCG.getValue('instrument.hierarchy.instrumentScreen', f.formValues);
			panel.updateForInstrumentScreen(screen);
		},
		updateForInstrumentScreen(instrumentScreen) {
			if (TCG.isEquals('Structured Index - Model Portfolio', instrumentScreen)) {
				this.setFormValue('instrumentStructureType', 'MODEL_PORTFOLIO', true);
				this.setFormValue('additionalMultiplier', 1, true); // Not Used for Model Portfolio
				this.hideField('additionalMultiplier');
			}
			else {
				this.setFormValue('instrumentStructureType', 'STRUCTURED_INDEX', true);
			}
		},

		items: [
			{fieldLabel: 'Instrument', name: 'instrument.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'instrument.id'},
			{name: 'instrumentStructureType', xtype: 'hidden'},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
			{fieldLabel: 'Additional Multiplier', name: 'additionalMultiplier', xtype: 'floatfield'},
			{
				fieldLabel: 'Current Weight Calculator', name: 'currentWeightCalculatorBean.name', hiddenName: 'currentWeightCalculatorBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Investment Structure Current Weight Calculator', detailPageClass: 'Clifton.system.bean.BeanWindow',
				qtip: 'Current Weight Calculators dynamically build the allocation list and weights for a specific date based on the calculator.'
			},
			{xtype: 'sectionheaderfield', header: 'Upload Weights From File'},

			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield'}
		]
	}]
});
