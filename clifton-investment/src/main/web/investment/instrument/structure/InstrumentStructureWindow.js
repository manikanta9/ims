Clifton.investment.instrument.structure.InstrumentStructureWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Structure',
	width: 800,
	height: 600,

	windowOnShow: function(w) {
		if (w.defaultData) {
			const screen = TCG.getValue('instrument.hierarchy.instrumentScreen', w.defaultData);
			w.getMainFormPanel().updateForInstrumentScreen(screen);
		}
	},


	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Structure',
			items: [{
				xtype: 'formpanel',
				url: 'investmentInstrumentStructure.json?requestedMaxDepth=5',
				labelWidth: 160,

				requestedProperties: 'id|instrument.id|instrument.label|sector|order|weight',
				requestedPropertiesRoot: 'data.instrumentStructureWeightList',

				afterload: function(panel) {
					// Screen Used Determines the Structure Type
					const f = panel.getForm();
					const screen = TCG.getValue('instrument.hierarchy.instrumentScreen', f.formValues);
					panel.updateForInstrumentScreen(screen);
				},
				updateForInstrumentScreen(instrumentScreen) {
					if (TCG.isEquals('Structured Index - Model Portfolio', instrumentScreen)) {
						this.setFormValue('instrumentStructureType', 'MODEL_PORTFOLIO', true);
						this.setFormValue('additionalMultiplier', 1, true); // Not Used for Model Portfolio
						this.hideField('additionalMultiplier');
						this.confirmBeforeSaveMessage = 'Changing values can impact Portfolio Weight calculations. After changes are completed, please rebuild the allocations from the Portfolio Weights tab within the applicable investment securities.<br/><br/><b>Are you sure you want to continue saving changes?</b>';

					}
					else {
						this.setFormValue('instrumentStructureType', 'STRUCTURED_INDEX', true);
						this.confirmBeforeSaveMessage = undefined;
					}
				},

				confirmBeforeSaveMsgTitle: 'Rebuild Portfolio Weights',
				confirmBeforeSaveMessage: undefined,
				getConfirmBeforeSaveMsg: function() {
					return this.confirmBeforeSaveMessage;
				},

				items: [
					{fieldLabel: 'Instrument', name: 'instrument.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'instrument.id'},
					{name: 'instrumentStructureType', xtype: 'hidden'},
					{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
					{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
					{fieldLabel: 'Additional Multiplier', name: 'additionalMultiplier', xtype: 'floatfield'},
					{
						fieldLabel: 'Current Weight Calculator', name: 'currentWeightCalculatorBean.name', hiddenName: 'currentWeightCalculatorBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Investment Structure Current Weight Calculator', detailPageClass: 'Clifton.system.bean.BeanWindow',
						qtip: 'Current Weight Calculators dynamically build the allocation list and weights for a specific date based on the calculator.'
					},
					{
						xtype: 'formgrid-scroll',
						heightResized: true,
						height: 325,
						forcefit: true,
						title: 'Instrument Weights',
						storeRoot: 'instrumentStructureWeightList',
						dtoClass: 'com.clifton.investment.instrument.structure.InvestmentInstrumentStructureWeight',

						columnsConfig: [
							{header: 'Instrument', width: 300, dataIndex: 'instrument.label', idDataIndex: 'instrument.id', editor: {xtype: 'combo', url: 'investmentInstrumentListFind.json', displayField: 'label', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow'}},
							{header: 'Sector', width: 150, dataIndex: 'sector', editor: {xtype: 'system-list-combo', listName: 'Investment Instrument Sectors'}},
							{header: 'Order', width: 75, dataIndex: 'order', type: 'int', editor: {xtype: 'integerfield'}},
							{
								header: 'Weight', width: 100, dataIndex: 'weight', type: 'float', editor: {xtype: 'floatfield'}, useNull: true,
								tooltip: 'Weight multiplier for this instrument and structure.  DJ-UBS calls this the Commodity Index Multiplier, GSCI calls this the Constant Product Weight'
							}
						]
					}
				]
			}]
		},


			{
				title: 'Security Structures',
				items: [{
					name: 'investmentInstrumentSecurityStructureListByStructure',
					xtype: 'gridpanel',

					instructions: 'The following security structures are associated with this instrument structure.   Double click to view/edit security structure setup for the security structure.',
					columns: [
						{header: 'StructureID', width: 10, dataIndex: 'structure.id', hidden: true},
						{header: 'SecurityID', width: 10, dataIndex: 'security.id', hidden: true},
						{header: 'Security', width: 150, dataIndex: 'security.label'},
						{header: 'Populated', width: 50, dataIndex: 'populated', type: 'boolean'}
					],
					editor: {
						addEnabled: false,
						detailPageClass: 'Clifton.investment.instrument.structure.SecurityStructureWindow',
						getDeleteParams: function(selectionModel) {
							return {
								securityId: selectionModel.getSelected().get('security.id'),
								structureId: selectionModel.getSelected().get('structure.id')
							};
						},
						openDetailPage: function(className, gridPanel, id, row) {
							const securityId = row.data['security.id'];
							const structureId = row.data['structure.id'];
							const params = {securityId: securityId, structureId: structureId};
							const cmpId = TCG.getComponentId(className, structureId + '-' + securityId);
							TCG.createComponent(className, {
								id: cmpId,
								params: params,
								openerCt: gridPanel,
								defaultIconCls: gridPanel.getWindow().iconCls
							});
						}
					},
					getLoadParams: function() {
						return {
							'structureId': this.getWindow().getMainFormId(),
							requestedMaxDepth: 4
						};
					}
				}]
			}]
	}]
});
