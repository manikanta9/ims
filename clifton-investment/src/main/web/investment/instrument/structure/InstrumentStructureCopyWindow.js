Clifton.investment.instrument.structure.InstrumentStructureCopyWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Copy Instrument Structure',
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		labelWidth: 100,
		url: 'investmentInstrumentStructure.json',
		instructions: 'Please enter a new start and/or end date and select if security structure weights should also be copied to the new structure.  When copying the existing structure, if it has no end date, or ends after this structure\'s start date it will automatically be updated to end on the previous day.',
		items: [
			{fieldLabel: 'Structure', name: 'label', xtype: 'displayfield', submitValue: false},
			{fieldLabel: 'Start Date', name: 'newStartDate', xtype: 'datefield'},
			{fieldLabel: 'End Date', name: 'newEndDate', xtype: 'datefield'},
			{
				boxLabel: 'Copy security structure weights', name: 'copySecurityStructures', xtype: 'checkbox', checked: true,
				tooltip: 'When selected, the existing security structure weights will also be copied to the new structure.'
			}
		],
		getSaveURL: function() {
			return 'investmentInstrumentStructureCopy.json';
		}
	}]
});
