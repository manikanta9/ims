Clifton.investment.instrument.structure.StructureAllocationRebuildWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Rebuild Structured Index Allocations',
	iconCls: 'run',
	height: 250,
	width: 400,

	items: [
		{
			xtype: 'formpanel',
			labelWidth: 180,
			loadValidation: false,
			height: 230,
			instructions: 'Enter an optional start/end date.  Leave dates blank to perform a full rebuild on the security structure.',
			okButtonText: 'Rebuild Allocations',
			okButtonTooltip: 'Rebuild Allocations',
			getSaveURL: function() {
				return 'investmentSecurityStructureAllocationRebuild.json';
			},

			items: [
				{xtype: 'hidden', name: 'securityId'},
				{xtype: 'hidden', name: 'structureId'},
				{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
				{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
				{fieldLabel: '', xtype: 'checkbox', boxLabel: 'Re-Process Existing', qtip: 'If data already exists for a given date, re-calculate weights.', name: 'reprocessExisting'}
			]
		}
	]
});

