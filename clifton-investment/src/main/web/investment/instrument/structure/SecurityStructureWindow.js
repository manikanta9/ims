Clifton.investment.instrument.structure.SecurityStructureWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Structure',
	width: 1100,
	height: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Structure',
				items: [{
					xtype: 'formpanel',
					url: 'investmentInstrumentSecurityStructure.json?requestedMaxDepth=5',
					loadValidation: false, // Not a Real Entity, so Can't load validation
					labelWidth: 160,

					requestedProperties: 'id|instrumentWeight.id|instrumentWeight.instrument.label|instrumentWeight.instrument.id|instrumentWeight.weight|weightOverride|currentSecurityCalculatorBean.name|currentSecurityCalculatorBean.id|currentSecurityTargetAdjustmentType|currentSecurityTargetAdjustmentTypeLabel|alwaysIncludeCurrentSecurity',
					requestedPropertiesRoot: 'data.securityStructureWeightList',

					listeners: {
						afterload: function(panel) {
							// Structured Type Determines the Weights Tab to Show
							const f = panel.getForm();
							const w = panel.getWindow();
							const tabs = w.items.get(0);

							const typeName = TCG.getValue('structure.instrumentStructureType', f.formValues);
							const previewWeights = this.getTabPosition(w, 'Preview Weights');
							const portfolioWeights = this.getTabPosition(w, 'Portfolio Weights');
							switch (typeName) {
								case 'MODEL_PORTFOLIO':
									if (TCG.isNotBlank(previewWeights)) {
										tabs.remove(previewWeights);
									}
									if (TCG.isBlank(portfolioWeights)) {
										tabs.add(panel.modelPortfolioWeightsTab);
									}
									break;
								default:
									if (TCG.isNotBlank(portfolioWeights)) {
										tabs.remove(portfolioWeights);
									}
									if (TCG.isBlank(previewWeights)) {
										tabs.add(panel.structuredIndexWeightsTab);
									}
									break;
							}
						}
					},

					confirmBeforeSaveMsgTitle: 'Rebuild Portfolio Weights',
					getConfirmBeforeSaveMsg: function() {
						if (TCG.isEquals('MODEL_PORTFOLIO', this.getFormValue('structure.instrumentStructureType'))) {
							return 'Changing values can impact Portfolio Weight calculations. After changes are completed, please rebuild the allocations from the Portfolio Weights tab.<br/><br/><b>Are you sure you want to continue saving changes?</b>';
						}
						return undefined;
					},


					getTabPosition: function(w, tabTitle) {
						const tabs = w.items.get(0);
						for (let i = 0; i < tabs.items.length; i++) {
							if (TCG.isEquals(tabs.items.get(i).title, tabTitle)) {
								return i;
							}
						}
						return undefined;
					},

					structuredIndexWeightsTab: {
						title: 'Preview Weights',
						items: [{
							xtype: 'investment-securityStructure-structuredIndex-weights',
							instructions: 'Select a date in the above toolbar to see what the calculated weights would be for this security and structure on that date.',
							getSecurityId: function() {
								return TCG.getValue('security.id', this.getWindow().getMainForm().formValues);
							},
							getStructureId: function() {
								return TCG.getValue('structure.id', this.getWindow().getMainForm().formValues);
							}
						}]
					},

					modelPortfolioWeightsTab: {
						title: 'Portfolio Weights',
						items: [{
							xtype: 'investment-securityStructure-modelPortfolio-weights',
							instructions: 'The following weights are saved calculated weights for the security structure.  Click Rebuild to rebuild the weights for a given date range.',
							getSecurityId: function() {
								return TCG.getValue('security.id', this.getWindow().getMainForm().formValues);
							},
							getStructureId: function() {
								return TCG.getValue('structure.id', this.getWindow().getMainForm().formValues);
							}
						}]
					},


					items: [
						{fieldLabel: 'Instrument', name: 'structure.instrument.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'structure.instrument.id'},
						{fieldLabel: 'Structure', name: 'structure.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.structure.InstrumentStructureWindow', detailIdField: 'structure.id'},
						{fieldLabel: 'Security', name: 'security.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'security.id'},

						{
							xtype: 'formgrid-scroll',
							heightResized: true,
							height: 550,
							forcefit: true,
							title: 'Security Structure Weights',
							storeRoot: 'securityStructureWeightList',
							dtoClass: 'com.clifton.investment.instrument.structure.InvestmentSecurityStructureWeight',
							readOnly: true,

							columnsConfig: [
								{header: 'InstrumentWeightID', width: 20, hidden: true, dataIndex: 'instrumentWeight.id'},
								{header: 'Instrument', width: 250, dataIndex: 'instrumentWeight.instrument.label', idDataIndex: 'instrumentWeight.instrument.id'},
								{
									header: 'Weight', width: 100, dataIndex: 'instrumentWeight.weight', type: 'float', useNull: true,
									tooltip: 'Weight multiplier for this instrument and structure.  DJ-UBS calls this the Commodity Index Multiplier, GSCI calls this the Constant Product Weight'
								},
								{
									header: 'Weight Override', width: 120, dataIndex: 'weightOverride', type: 'float', useNull: true, editor: {xtype: 'floatfield'},
									tooltip: 'Can be used to override the weight defined by the instrument structure.  Use 0 to exclude.'
								},

								{
									header: 'Current Security Calculator', width: 250, dataIndex: 'currentSecurityCalculatorBean.name', idDataIndex: 'currentSecurityCalculatorBean.id', editor: {xtype: 'combo', url: 'systemBeanListFind.json?groupName=Investment Current Security Calculator', detailPageClass: 'Clifton.system.bean.BeanWindow'},
									tooltip: 'Current Security Calculator defines the behavior of how the <i>current</i> security is selected when dynamically calculating weights and allocations for a given date.'
								},
								{
									header: 'Current Security Target Adjustment', width: 130, dataIndex: 'currentSecurityTargetAdjustmentTypeLabel', idDataIndex: 'currentSecurityTargetAdjustmentType',
									editor: {
										displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
										store: new Ext.data.ArrayStore({
											fields: ['value', 'name', 'description'],
											data: Clifton.investment.instrument.CurrentSecurityTargetAdjustmentTypes
										})
									},
									tooltip: 'Defines when portfolio holds additional securities that are the non-current, how the targets are adjusted for the non-current and current securities.'
								},
								{
									header: 'Always Include Current Security', width: 100, dataIndex: 'alwaysIncludeCurrentSecurity', type: 'boolean', editor: {xtype: 'checkbox'},
									tooltip: 'This flag determines whether or not a current security is always included during portfolio run processing, even if it results in a 0 allocation (0 target, 0 exposure). If FALSE, the current security will not be included if it results in a 0 allocation.'
								}
							]
						}
					]
				}]
			}


		]
	}]
});
