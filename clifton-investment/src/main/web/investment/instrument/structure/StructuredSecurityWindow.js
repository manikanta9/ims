Clifton.investment.instrument.structure.StructuredSecurityWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Structured Index',
	iconCls: 'coins',
	width: 900,
	height: 500,

	typeName: 'STRUCTURED_INDEX',

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		if (this.typeName === 'MODEL_PORTFOLIO') {
			tabs.insert(2, this.modelPortfolioWeightsTab);
		}
		else {
			tabs.insert(2, this.structuredIndexWeightsTab);
		}
	},

	structuredIndexWeightsTab: {
		title: 'Preview Weights',
		items: [{
			xtype: 'investment-securityStructure-structuredIndex-weights',
			instructions: 'Select a date in the above toolbar to see what the calculated weights would be for this security and the structure active on that date.',
			getSecurityId: function() {
				return this.getWindow().getMainFormId();
			}
		}]
	},

	modelPortfolioWeightsTab: {
		title: 'Portfolio Weights',
		items: [{
			xtype: 'investment-securityStructure-modelPortfolio-weights',
			instructions: 'The following weights are saved calculated weights for the security structure.  Click Rebuild to rebuild the weights for a given date range.',
			getSecurityId: function() {
				return this.getWindow().getMainFormId();
			}
		}]
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Structured Index',
				items: [{
					xtype: 'investment-security-forms-with-dynamic-fields',
					url: 'investmentSecurity.json',
					instructions: 'Structured index is an allocation of instruments whose weights and current securities are dynamically calculated based on the instrument structure and this security\'s specific structure.',
					labelWidth: 140,

					confirmBeforeSaveMsgTitle: 'Rebuild Portfolio Weights',
					getConfirmBeforeSaveMsg: function() {
						if (TCG.isNotBlank(this.getIdFieldValue()) && TCG.isEquals('MODEL_PORTFOLIO', this.getWindow().typeName)) {
							return 'Changing values can impact Portfolio Weight calculations. After changes are completed, please rebuild the allocations from the Portfolio Weights tab.<br/><br/><b>Are you sure you want to continue saving changes?</b>';
						}
						return undefined;
					},


					items: [
						{fieldLabel: 'Investment Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
						{fieldLabel: 'Investment Instrument', name: 'instrument.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'instrument.id'},

						{fieldLabel: 'Ticker Symbol', name: 'symbol'},
						{fieldLabel: 'Security Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},

						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
						{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
					]
				}]
			},

			{
				title: 'Structures',
				items: [{
					name: 'investmentInstrumentSecurityStructureListBySecurity',
					xtype: 'gridpanel',
					instructions: 'The following structures are associated with this instrument.  Only one structure can be active for a specific date.  Drill into a structure to view/edit security structure setup for the instrument structure.',
					columns: [
						{header: 'StructureID', width: 10, dataIndex: 'structure.id', hidden: true},
						{header: 'SecurityID', width: 10, dataIndex: 'security.id', hidden: true},
						{header: 'Start Date', width: 50, dataIndex: 'structure.startDate'},
						{header: 'End Date', width: 50, dataIndex: 'structure.endDate'},
						{header: 'Weight Calculator', width: 120, dataIndex: 'structure.currentWeightCalculatorBean.name'},
						{
							header: 'Additional Multiplier', width: 60, dataIndex: 'structure.additionalMultiplier', type: 'float',
							tooltip: 'Additional multiplier that can be applied to the weight calculation.<br><br><b>Example:</b>GSCI Commodity Indices use a GSCI Normalizing Constant. This field would = 1/GSCI Constant since it is always multiplied, not divided by it.'
						},
						{header: 'Populated', width: 50, dataIndex: 'populated', type: 'boolean'}
					],
					editor: {
						addEnabled: false,
						detailPageClass: 'Clifton.investment.instrument.structure.SecurityStructureWindow',
						getDeleteParams: function(selectionModel) {
							return {
								securityId: selectionModel.getSelected().get('security.id'),
								structureId: selectionModel.getSelected().get('structure.id')
							};
						},
						openDetailPage: function(className, gridPanel, id, row) {
							const securityId = row.data['security.id'];
							const structureId = row.data['structure.id'];
							const params = {securityId: securityId, structureId: structureId};
							const cmpId = TCG.getComponentId(className, structureId + '-' + securityId);
							TCG.createComponent(className, {
								id: cmpId,
								params: params,
								openerCt: gridPanel,
								defaultIconCls: gridPanel.getWindow().iconCls
							});
						}
					},
					getLoadParams: function() {
						return {
							'securityId': this.getWindow().getMainFormId(),
							requestedMaxDepth: 4
						};
					}
				}]
			},


			{
				title: 'Symbol Overrides',
				items: [{xtype: 'marketdata-datasource-security-override-grid'}]
			},

			{
				title: 'Market Data',
				items: [{xtype: 'marketdata-security-datavalue-grid'}]
			}

		]
	}]
});
