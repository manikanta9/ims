TCG.use('Clifton.investment.instrument.BaseSecurityDetailWindow');

Clifton.investment.instrument.BondWindow = Ext.extend(Clifton.investment.instrument.BaseSecurityDetailWindow, {
	titlePrefix: 'Investment Security - Bond',
	iconCls: 'bonds',
	height: 650,
	width: 900,
	enableRefreshWindow: true,

	securityType: 'Bond',

	tabItems: [
		{
			title: 'Security',
			items: [{
				xtype: 'investment-security-forms-with-dynamic-fields',
				labelWidth: 160,

				processAfterRenderFieldVisibility: function(formPanel) {
					Clifton.investment.instrument.SecurityFormWithDynamicFields.prototype.processAfterRenderFieldVisibility.apply(this, arguments);
					if (formPanel.getFormValue('instrument.hierarchy.factorChangeEventType.name') !== 'Factor Change') {
						const tabs = TCG.getParentByClass(formPanel, Ext.TabPanel);
						tabs.hideTabStripItem(3); // hide 'Factor Changes' tab
					}
				},

				items: [
					{fieldLabel: 'Investment Hierarchy', name: 'instrument.hierarchy.labelExpanded', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', detailIdField: 'instrument.hierarchy.id'},
					{xtype: 'investment-instrument-ticker'},
					{fieldLabel: 'CUSIP', name: 'cusip'},
					{fieldLabel: 'ISIN', name: 'isin'},
					{fieldLabel: 'SEDOL', name: 'sedol'},
					{fieldLabel: 'FIGI', name: 'figi', qtip: 'Financial Instrument Global Identifier (FIGI; also known as the Bloomberg Global Identifier (BBGID)) is an open standard, 12 character unique identifier assigned to all financial instruments and will not change once issued. For equity instruments, an identifier is issued per trading venue.'},
					{fieldLabel: 'Security Name', name: 'name'},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 40},
					{boxLabel: 'Illiquid (cannot be easily sold or exchanged for cash)', name: 'illiquid', xtype: 'checkbox'},
					{
						fieldLabel: 'BICS Industry Subgroup', xtype: 'system-hierarchy-combo', tableName: 'InvestmentInstrument', hierarchyCategoryName: 'BICS Hierarchy',
						qtip: 'Bloomberg Industry Classification System (BICS)',
						getFkFieldId: function(fp) {
							return fp.getFormValue('instrument.id', true);
						}
					},
					{
						fieldLabel: 'GICS Sub-industry', xtype: 'system-hierarchy-combo', tableName: 'InvestmentInstrument', hierarchyCategoryName: 'GICS Hierarchy',
						qtip: 'Global Industry Classification Standard (GICS)',
						getFkFieldId: function(fp) {
							return fp.getFormValue('instrument.id', true);
						}
					},
					{fieldLabel: 'Country of Risk', name: 'instrument.countryOfRisk.text', hiddenName: 'instrument.countryOfRisk.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
					{fieldLabel: 'Country of Incorporation', name: 'instrument.countryOfIncorporation.text', hiddenName: 'instrument.countryOfIncorporation.id', xtype: 'system-list-combo', valueField: 'id', listName: 'Countries'},
					{fieldLabel: 'Settlement Calendar', name: 'instrument.settlementCalendar.name', hiddenName: 'instrument.settlementCalendar.id', xtype: 'combo', url: 'calendarListFind.json', detailPageClass: 'Clifton.calendar.setup.CalendarWindow'},
					{
						fieldLabel: 'Issuer', name: 'businessCompany.name', hiddenName: 'businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true', detailPageClass: 'Clifton.business.company.CompanyWindow',
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const companyType = combo.getParentForm().getFormValue('instrument.hierarchy.businessCompanyType.name');
								combo.store.baseParams = companyType ? {companyType: companyType} : {};
							}
						}
					},
					{fieldLabel: 'Issue Date', name: 'startDate', xtype: 'datefield', allowBlank: false},
					{fieldLabel: 'Maturity Date', name: 'endDate', xtype: 'datefield', allowBlank: false},
					{
						xtype: 'fieldset',
						title: 'Delivery Dates',
						collapsed: true,
						items: [{
							xtype: 'formfragment',
							frame: false,
							instructions: 'A delayed bond may have the very last payment after maturity date. Set delivery date fields to allow posting the last payment to the General Ledger.',
							labelWidth: 145,
							items: [
								{fieldLabel: 'First Delivery Date', name: 'firstDeliveryDate', xtype: 'datefield'},
								{fieldLabel: 'Last Delivery Date', name: 'lastDeliveryDate', xtype: 'datefield'},
								{fieldLabel: 'Early Termination Date', name: 'earlyTerminationDate', xtype: 'datefield'}
							]
						}]
					},
					{fieldLabel: 'Currency', name: 'instrument.tradingCurrency.name', hiddenName: 'instrument.tradingCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', allowBlank: false},
					{fieldLabel: 'Price Multiplier', name: 'instrument.priceMultiplier', xtype: 'floatfield', hidden: true, value: '0.01', allowBlank: false},
					{name: 'instrument.exposureMultiplier', xtype: 'hidden', value: '1'}
				]
			}]
		},


		{
			title: 'All Events',
			items: [{
				xtype: 'investment-security-events-by-security-grid'
			}]
		},


		{
			title: 'Coupons',
			items: [{
				name: 'investmentSecurityEventListFind',
				xtype: 'gridpanel',
				instructions: 'Coupon payments are made by bond issuers to bond holders. Payments are represented as cash percentage of principal owned.',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Coupon %', width: 60, dataIndex: 'afterEventValue', type: 'float'},
					{header: 'Accrual Start', width: 50, dataIndex: 'declareDate'},
					{header: 'Accrual End', width: 50, dataIndex: 'recordDate'},
					{header: 'Ex Date', width: 50, dataIndex: 'exDate'},
					{
						header: 'Payment Date', width: 50, dataIndex: 'eventDate',
						renderer: function(v, metaData, r) {
							const value = TCG.renderDate(v);
							if (r.json.createDate !== r.json.updateDate) {
								return '<div class="amountAdjusted" qtip="Coupon Data was updated. See Audit Trail for details.">' + value + '</div>';
							}
							return value;
						}
					},
					{header: 'Description', width: 80, dataIndex: 'eventDescription'}
				],
				editor: {
					detailPageClass: 'Clifton.investment.instrument.event.CouponPaymentEventWindow',
					getDefaultData: function(gridPanel) {
						return {
							security: gridPanel.getWindow().getMainForm().formValues,
							type: TCG.data.getData('investmentSecurityEventTypeByName.json?name=Cash Coupon Payment', gridPanel, 'investment.security.event.cashCouponPayment')
						};
					},
					addEditButtons: function(t, gridPanel) {
						t.add({
							text: 'Import Latest Coupon',
							xtype: 'splitbutton',
							tooltip: 'Import latest coupon event from Bloomberg',
							iconCls: 'run',
							scope: gridPanel,
							handler: function() {
								gridPanel.importCoupons(false);
							},
							menu: new Ext.menu.Menu({
								items: [
									{
										text: 'Generate Fixed Coupon Schedule',
										tooltip: 'Generate Fixed Coupon events from Bloomberg.',
										iconCls: 'run',
										scope: gridPanel,
										handler: function() {
											gridPanel.importCoupons(true);
										}
									}
								]
							})
						});
						t.add('-');
						TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
					}
				},
				getLoadParams: function() {
					return {
						securityId: this.getWindow().getMainFormId(),
						typeName: 'Cash Coupon Payment',
						requestedMaxDepth: 2
					};
				},
				importCoupons: function(historic) {
					const grid = this;

					const params = {'securityId': this.getWindow().getMainFormId(), 'eventTypeNameList': ['Cash Coupon Payment']};
					const loader = new TCG.data.JsonLoader({
						waitTarget: grid,
						waitMsg: 'Processing...',
						params: params,
						conf: params,
						onLoad: function(record, conf) {
							grid.reload();
						},
						onFailure: function() {
							// Even if the Processing Fails, still need to reload so that dates are properly updated, and warnings adjusted
							grid.reload();
						}
					});
					loader.load(historic ? 'investmentSecurityEventHistoryLoad.json' : 'investmentSecurityEventLoad.json');
				}
			}]
		},


		{
			title: 'Factor Changes',
			items: [{
				name: 'investmentSecurityEventListFind',
				xtype: 'gridpanel',
				instructions: 'For asset backed securities, factor change represents the change (usually reduction) of outstanding principal usually due to early mortgage prepayment. Value of 1 represents full principal and value of 0 represents no principal left.',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Previous Factor', width: 60, dataIndex: 'beforeEventValue', type: 'float'},
					{header: 'New Factor', width: 52, dataIndex: 'afterEventValue', type: 'float'},
					{header: 'Loss %', width: 40, dataIndex: 'additionalEventValue', type: 'float', useNull: true},
					{header: 'Accrual Start', width: 48, dataIndex: 'declareDate'},
					{header: 'Accrual End', width: 48, dataIndex: 'recordDate'},
					{header: 'Ex Date', width: 50, dataIndex: 'exDate', hidden: true},
					{
						header: 'Payment Date', width: 50, dataIndex: 'eventDate',
						renderer: function(v, metaData, r) {
							const value = TCG.renderDate(v);
							if (r.json.createDate !== r.json.updateDate) {
								return '<div class="amountAdjusted" qtip="Factor Data was updated. See Audit Trail for details.">' + value + '</div>';
							}
							return value;
						}
					},
					{header: 'Description', width: 40, dataIndex: 'eventDescription'}
				],
				editor: {
					detailPageClass: 'Clifton.investment.instrument.event.FactorChangeEventWindow',
					getDefaultData: function(gridPanel) {
						return {
							security: gridPanel.getWindow().getMainForm().formValues,
							type: TCG.data.getData('investmentSecurityEventTypeByName.json?name=Factor Change', gridPanel, 'investment.security.event.factorChange')
						};
					},
					addEditButtons: function(t, gridPanel) {
						t.add({
							text: 'Import Latest Factor',
							tooltip: 'Import latest factor event from Bloomberg',
							iconCls: 'run',
							scope: gridPanel,
							handler: function() {
								gridPanel.importFactors(false);
							}
						});
						t.add('-');
						TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
					}
				},
				getLoadParams: function() {
					return {
						securityId: this.getWindow().getMainFormId(),
						typeName: 'Factor Change',
						requestedMaxDepth: 2
					};
				},
				importFactors: function(historic) {
					const grid = this;

					const params = {'securityId': this.getWindow().getMainFormId(), 'eventTypeNameList': 'Factor Change'};
					const loader = new TCG.data.JsonLoader({
						waitTarget: grid,
						waitMsg: 'Processing...',
						params: params,
						conf: params,
						onLoad: function(record, conf) {
							grid.reload();
						},
						onFailure: function() {
							// Even if the Processing Fails, still need to reload so that dates are properly updated, and warnings adjusted
							grid.reload();
						}
					});
					loader.load(historic ? 'investmentSecurityEventHistoryLoad.json' : 'investmentSecurityEventLoad.json');
				}
			}]
		},


		{
			title: 'Notes',
			items: [{
				xtype: 'investment-security-system-note-grid'
			}]
		},


		{
			title: 'Instrument Groups',
			layout: 'border',
			items: [
				{
					xtype: 'investment-instrumentGroupGrid_ByInstrument',
					region: 'north',
					height: 250,
					securityWindow: true
				},
				{
					xtype: 'investment-instrumentGroupMissingGrid_ByInstrument',
					region: 'center',
					securityWindow: true
				}
			]
		},


		{
			title: 'Security Groups',
			items: [{
				xtype: 'investment-securityGroupGrid_BySecurity'
			}]
		},


		{
			title: 'Tasks',
			items: [{
				xtype: 'workflow-task-grid',
				tableName: 'InvestmentSecurity'
			}]
		}
	]
});
