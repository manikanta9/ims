Clifton.investment.instrument.SwapScheduleCopyWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Copy Schedule',
	iconCls: 'run',
	width: 700,
	height: 300,
	// Load the default data after render for copying to work
	loadDefaultDataAfterRender: true,
	enableRefreshWindow: true,
	okButtonText: 'Copy',
	okButtonToolTip: 'Copy the event schedule of the selected Instrument or Security to the destination Security.',

	items: [{
		xtype: 'formpanel',
		instructions: 'Please select the instrument or security that\'s schedule should be copied from the original investment to the destination investment.',
		getSaveURL: function() {
			return 'investmentSecurityEventScheduleCopy.json?enableValidatingBinding=true&disableValidatingBindingValidation=true';
		},

		getDataAfterSave: function(data) {
			TCG.showInfo('A total of ' + data + ' events were copied to your security.');
			return data;
		},

		labelWidth: 150,
		labelFieldName: 'id',
		items: [
			{fieldLabel: 'Target Investment Security', name: 'destinationSecurity.name', xtype: 'linkfield', detailIdField: 'destinationSecurity.id'},
			{xtype: 'sectionheaderfield', header: 'Copy Instrument Schedule'},
			{
				fieldLabel: 'Investment Type', xtype: 'combo',
				name: 'sourceInvestmentType.name', hiddenName: 'sourceInvestmentType.id',
				displayField: 'name', url: 'investmentTypeList.json', loadAll: true,
				disableAddNewItem: true,
				qtip: 'The Investment Type for securities to which this mapping shall be applied.',
				listeners: {
					select: function(combo, record, index) {
						combo.getParentForm().getForm().findField('sourceHierarchy.id').clearAndReset();
						combo.getParentForm().getForm().findField('sourceInstrument.id').clearAndReset();
						combo.getParentForm().getForm().findField('sourceSecurity.id').clearAndReset();
					}
				}
			},
			{
				fieldLabel: 'Investment Sub Type', xtype: 'combo',
				name: 'sourceInvestmentTypeSubType.name', hiddenName: 'sourceInvestmentTypeSubType.id',
				displayField: 'name', url: 'investmentTypeSubTypeListByType.json', loadAll: true,
				disableAddNewItem: true,
				requiredFields: ['sourceInvestmentType.id'],
				qtip: 'The Investment Sub Type for securities to which this mapping shall be applied.',
				listeners: {
					select: function(combo, record, index) {
						combo.getParentForm().getForm().findField('sourceHierarchy.id').clearAndReset();
						combo.getParentForm().getForm().findField('sourceInstrument.id').clearAndReset();
						combo.getParentForm().getForm().findField('sourceSecurity.id').clearAndReset();
					},
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const form = combo.getParentForm().getForm();
						const investmentTypeId = form.findField('sourceInvestmentType.id').getValue();
						combo.store.setBaseParam('investmentTypeId', investmentTypeId);
					}
				}
			},
			{
				fieldLabel: 'Investment Sub Type 2', xtype: 'combo',
				name: 'sourceInvestmentTypeSubType2.name', hiddenName: 'sourceInvestmentTypeSubType2.id',
				displayField: 'name', url: 'investmentTypeSubType2ListByType.json', loadAll: true,
				disableAddNewItem: true,
				requiredFields: ['sourceInvestmentType.id'],
				qtip: 'The Investment Sub Type 2 for securities to which this mapping shall be applied.',
				listeners: {
					select: function(combo, record, index) {
						combo.getParentForm().getForm().findField('sourceHierarchy.id').clearAndReset();
						combo.getParentForm().getForm().findField('sourceInstrument.id').clearAndReset();
						combo.getParentForm().getForm().findField('sourceSecurity.id').clearAndReset();
					},
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const form = combo.getParentForm().getForm();
						const investmentTypeId = form.findField('sourceInvestmentType.id').getValue();
						combo.store.setBaseParam('investmentTypeId', investmentTypeId);
					}
				}
			},
			{
				fieldLabel: 'Investment Hierarchy', xtype: 'combo',
				name: 'sourceHierarchy.labelExpanded', hiddenName: 'sourceHierarchy.id',
				displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json',
				queryParam: 'labelExpanded', listWidth: 600,
				sortInfo: {orderBy: 'labelExpanded'},
				qtip: 'The Investment Hierarchy for securities to which this mapping shall be applied.',
				listeners: {
					select: function(combo, record, index) {
						combo.getParentForm().getForm().findField('sourceInstrument.id').clearAndReset();
						combo.getParentForm().getForm().findField('sourceSecurity.id').clearAndReset();
					},
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const form = combo.getParentForm().getForm();
						const investmentTypeId = form.findField('sourceInvestmentType.id').getValue();
						const investmentTypeSubTypeId = form.findField('sourceInvestmentTypeSubType.id').getValue();
						const investmentTypeSubType2Id = form.findField('sourceInvestmentTypeSubType2.id').getValue();
						combo.store.setBaseParam('investmentTypeId', investmentTypeId);
						combo.store.setBaseParam('investmentTypeSubTypeId', investmentTypeSubTypeId);
						combo.store.setBaseParam('investmentTypeSubType2Id', investmentTypeSubType2Id);
					}
				}
			},
			{
				fieldLabel: 'Investment Instrument', xtype: 'combo',
				name: 'sourceInstrument.label', hiddenName: 'sourceInstrument.id',
				displayField: 'label', url: 'investmentInstrumentListFind.json',
				disableAddNewItem: true,
				requiredFields: ['sourceInvestmentType.id', 'sourceHierarchy.id'],
				qtip: 'The Investment Instrument for securities to which this mapping shall be applied.',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const form = combo.getParentForm().getForm();
						const hierarchyId = form.findField('sourceHierarchy.id').getValue();
						const investmentTypeId = form.findField('sourceInvestmentType.id').getValue();
						const investmentTypeSubTypeId = form.findField('sourceInvestmentTypeSubType.id').getValue();
						const investmentTypeSubType2Id = form.findField('sourceInvestmentTypeSubType2.id').getValue();
						combo.store.setBaseParam('hierarchyId', hierarchyId);
						combo.store.setBaseParam('investmentTypeId', investmentTypeId);
						combo.store.setBaseParam('investmentTypeSubTypeId', investmentTypeSubTypeId);
						combo.store.setBaseParam('investmentTypeSubType2Id', investmentTypeSubType2Id);
					}
				}
			},
			{
				fieldLabel: 'Investment Security', xtype: 'combo',
				name: 'sourceSecurity.label', hiddenName: 'sourceSecurity.id',
				displayField: 'label', url: 'investmentSecurityListFind.json',
				disableAddNewItem: true,
				requiredFields: ['sourceInvestmentType.id', 'sourceHierarchy.id', 'sourceInstrument.id'],
				qtip: 'The Investment Instrument for securities to which this mapping shall be applied.',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const form = combo.getParentForm().getForm();
						const hierarchyId = form.findField('sourceHierarchy.id').getValue();
						const investmentTypeId = form.findField('sourceInvestmentType.id').getValue();
						const investmentTypeSubTypeId = form.findField('sourceInvestmentTypeSubType.id').getValue();
						const investmentTypeSubType2Id = form.findField('sourceInvestmentTypeSubType2.id').getValue();
						const instrumentId = form.findField('sourceInstrument.id').getValue();
						combo.store.setBaseParam('hierarchyId', hierarchyId);
						combo.store.setBaseParam('investmentTypeId', investmentTypeId);
						combo.store.setBaseParam('investmentTypeSubTypeId', investmentTypeSubTypeId);
						combo.store.setBaseParam('investmentTypeSubType2Id', investmentTypeSubType2Id);
						combo.store.setBaseParam('instrumentId', instrumentId);
					}
				}
			}
		]
	}]
});
