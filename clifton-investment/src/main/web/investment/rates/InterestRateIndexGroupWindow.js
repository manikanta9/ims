Clifton.investment.rates.InterestRateIndexGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Interest Rate Index Group',
	iconCls: 'numbers',
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Interest Rate Index Groups are ways of group indices for specific reasons.  For example, Synthetic Adjustment Factor group contains all indices that can be used to calculate values during Portfolio processing.',
					url: 'investmentInterestRateIndexGroup.json',
					items: [
						{fieldLabel: 'Group Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							xtype: 'columnpanel',
							columns: [
								{
									rows: [
										{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'}
									]
								},
								{
									rows: [
										{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
									]
								}
							]
						},
						{fieldLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox', disabled: true}
					]
				}]
			},


			{
				title: 'Indices',
				items: [{
					name: 'investmentInterestRateIndexGroupIndexListFind',
					xtype: 'gridpanel',
					instructions: 'This group is associated with the following indices.',
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to active indices
							this.setFilterValue('active', true);
						}
						return {groupId: this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'IndexID', width: 15, dataIndex: 'referenceTwo.id', hidden: true},
						{header: 'Index Name', width: 100, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'indexName'}},
						{header: 'Ticker Symbol', width: 60, dataIndex: 'referenceTwo.symbol', filter: {searchFieldName: 'indexSymbol'}},
						{header: 'CCY Denomination', width: 80, dataIndex: 'referenceTwo.currencyDenomination.name', filter: {type: 'combo', searchFieldName: 'currencyId', displayField: 'name', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Days', width: 40, dataIndex: 'referenceTwo.numberOfDays', type: 'int', filter: {searchFieldName: 'numberOfDays'}},
						{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.rates.InterestRateIndexGroupIndexWindow',
						deleteURL: 'investmentInterestRateIndexGroupIndexDelete.json',
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'index', url: 'investmentInterestRateIndexListFind.json?active=true', displayfield: 'label', width: 150, listWidth: 230}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add selected index to this group',
								iconCls: 'add',
								handler: function() {
									const indexId = TCG.getChildByName(toolBar, 'index').getValue();
									if (indexId === '') {
										TCG.showError('You must first select desired index from the list.');
									}
									else {
										const groupId = gridPanel.getWindow().getMainFormId();
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Linking...',
											params: {groupId: groupId, indexId: indexId},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'index').reset();
											}
										});
										loader.load('investmentInterestRateIndexGroupIndexLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			}
		]
	}]
});
