Clifton.investment.rates.InterestRateIndexGroupIndexWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Interest Rate Index Group Mapping',
	iconCls: 'numbers',
	height: 400,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		url: 'investmentInterestRateIndexGroupIndex.json',

		getWarningMessage: function(form) {
			let msg = undefined;
			if (form.formValues.active === false) {
				if (form.formValues.indexActive === false) {
					msg = 'The index is currently inactive.';
				}
				else {
					msg = 'The index is currently active, however this mapping is inactive.';
				}
			}
			return msg;
		},

		items: [
			{fieldLabel: 'Group', name: 'referenceOne.name', hiddenName: 'referenceOne.id', url: 'investmentInterestRateIndexGroupList.json', xtype: 'combo', detailPageClass: 'Clifton.investment.rates.InterestRateIndexGroupWindow'},
			{fieldLabel: 'Index', name: 'referenceTwo.label', hiddenName: 'referenceTwo.id', url: 'investmentInterestRateIndexListFind.json?active=true', xtype: 'combo', detailPageClass: 'Clifton.investment.rates.InterestRateIndexWindow'},
			{
				fieldLabel: 'Start Date', xtype: 'compositefield', labelSeparator: '',
				defaults: {
					xtype: 'datefield',
					flex: 1
				},
				items: [
					{name: 'startDate'},
					{html: 'End Date:', flex: 0.5, xtype: 'label'},
					{name: 'endDate'}
				]
			}
		]
	}]
});
