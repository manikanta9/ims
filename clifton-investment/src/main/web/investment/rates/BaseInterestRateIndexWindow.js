Clifton.investment.rates.BaseInterestRateIndexWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Interest Rate Index',
	iconCls: 'numbers',
	width: 1000,
	height: 600,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		url: 'investmentInterestRateIndex.json',
		items: [
			{fieldLabel: 'Index Name', name: 'name'},
			{fieldLabel: 'ISDA Name', name: 'isdaName', qtip: 'The International Swaps and Derivatives Association (ISDA) Definition for the interest or swap rate reference'},
			{
				xtype: 'columnpanel',
				columns: [
					{
						rows: [
							{fieldLabel: 'Ticker Symbol', name: 'symbol', xtype: 'textfield', qtip: 'Default or Internal Ticker Symbol for this index.  When mapping to market data source sectors this symbol can be overridden for data source specific symbols.'},
							{fieldLabel: 'Fixing Term', name: 'fixingTerm', xtype: 'textfield', qtip: 'The maturity of the interest or swap rate e.g. 28D, 1M, 3M, 6M, 1Y'},
							{fieldLabel: 'CCY Denomination', name: 'currencyDenomination.name', hiddenName: 'currencyDenomination.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
							{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'}
						]
					},
					{
						rows: [
							{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield', maxValue: 100000},
							{fieldLabel: 'Days', name: 'numberOfDays', xtype: 'spinnerfield', maxValue: 100000},
							{fieldLabel: 'Calendar', name: 'calendar.name', hiddenName: 'calendar.id', xtype: 'combo', url: 'calendarListFind.json', detailPageClass: 'Clifton.calendar.setup.CalendarWindow'},
							{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
						]
					}
				]
			},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70}
		]
	}]
});
