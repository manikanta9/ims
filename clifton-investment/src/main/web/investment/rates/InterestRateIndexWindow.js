// not the actual window but a window selector based on overrides
Clifton.investment.rates.InterestRateIndexWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'investmentInterestRateIndex.json',

	getClassName: function(config, entity) {
		return (Clifton.investment.rates.InterestRateIndexWindowOverride);
	}
});
