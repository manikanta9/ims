Clifton.investment.replication.ReplicationTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Replication Type',
	iconCls: 'replicate',
	width: 800,
	height: 700,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'Investment replication type defines the types of securities that are in the replication and how Portfolio processing will handle processing & calculating contract values. The financing rate is used for performance summary calculations.',
		url: 'investmentReplicationType.json',
		labelWidth: 160,

		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 100},
			{
				fieldLabel: 'Performance Financing Rate', name: 'financingRateIndex.label', hiddenName: 'financingRateIndex.id', xtype: 'combo', url: 'investmentInterestRateIndexListFind.json?active=true', displayField: 'label', detailPageClass: 'Clifton.investment.rates.InterestRateIndexWindow',
				qtip: 'Financing rate for this replication type - used for performance summaries'
			},

			{
				xtype: 'fieldset',
				title: 'Contract Value Calculator Properties',
				instructions: 'The following options may be required to be selected if the selected calculator requires them.  If not required, can optionally be selected to calculate/display the information even if not used to calculate contract value.',
				items: [
					{
						fieldLabel: 'Contract Value Calculator', name: 'calculatorBean.name', hiddenName: 'calculatorBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Investment Replication Calculator', detailPageClass: 'Clifton.system.bean.BeanWindow',
						qtip: 'Calculator used to determine the value of one contract, as well as what fields and how to populate on the run'
					},
					{
						fieldLabel: 'Interest Rate Group', name: 'interestRateIndexGroup.name', hiddenName: 'interestRateIndexGroup.id', xtype: 'combo', url: 'investmentInterestRateIndexGroupListFind.json?active=true', detailPageClass: 'Clifton.investment.rates.InterestRateIndexGroupWindow',
						qtip: 'Defines which interest rate indices apply to this replication type, which is then further filtered based on the currency denomination and days to maturity vs. number of days for the interest rate to determine the "closest" rate to use.'
					},
					{boxLabel: 'Credit Duration Supported', name: 'creditDurationSupported', xtype: 'checkbox'},
					{boxLabel: 'Duration Supported', name: 'durationSupported', xtype: 'checkbox'},
					{
						boxLabel: 'Synthetic Adjustment Factor Supported', name: 'syntheticAdjustmentFactorSupported', xtype: 'checkbox',
						qtip: 'Formula: 1 / (1+((Days to Maturity/365)*Interest Rate))'
					},
					{
						boxLabel: 'Currency', name: 'currency', xtype: 'checkbox',
						qtip: 'Currency replications have special meaning and when noted as a "currency" replication, physical and unrealized currency balances can be included in the total allocation.'
					},
					{
						boxLabel: 'Do Not Apply Currency Other Allocation', name: 'doNotApplyCurrencyOtherAllocation', xtype: 'checkbox', requiredFields: ['currency'],
						qtip: 'If selected, currency other will still be generated if needed, however this replication type excludes allocating base currency other and unrealized base currency other to it.'
					},
					{boxLabel: 'Index Ratio Supported', name: 'indexRatioSupported', xtype: 'checkbox'},
					{boxLabel: 'Dirty Price Supported', name: 'dirtyPriceSupported', xtype: 'checkbox'},
					{boxLabel: 'Delta Supported', name: 'deltaSupported', xtype: 'checkbox'},
					{
						boxLabel: 'Do Not Apply Exposure Multiplier', name: 'doNotApplyExposureMultiplier', xtype: 'checkbox',
						qtip: 'If selected, exposure multiplier will not be included in the calculation (See Euro Dollar, Euro Yen for examples that use exposure multipliers.'
					},
					{
						boxLabel: 'Do Not Apply M2M Adjustment', name: 'doNotApplyM2MAdjustment', xtype: 'checkbox',
						qtip: 'M2M Adjustment on the Trading Screen based on price change (when update with live prices). Currently doesn\'t apply to Options replication calc because change in price doesn\'t mean anything.'
					},
					{
						boxLabel: 'Apply Factor', name: 'applyCurrentFactor', xtype: 'checkbox',
						qtip: 'This flag is applicable to securities that have Factor Events. When checked, the factor is included in the replication calculations for value calculations so the trade quantities can be shown and entered as original face values. When unchecked, the factor is not applied to contract value calculations so the trade values are in adjusted quantity values to match historic processing.'
					},
					{
						boxLabel: 'Use Ultimate Underlying Security', name: 'ultimateUnderlyingUsed', xtype: 'checkbox',
						qtip: 'When checked, the underlying security used will be the ultimate underlying security.  If the contract value calculator uses the underlying security price, then the ultimate underlying security price would be used.'
					}
				]
			}
		]
	}]
});
