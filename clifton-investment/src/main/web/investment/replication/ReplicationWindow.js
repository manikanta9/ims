Clifton.investment.replication.ReplicationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Replication',
	iconCls: 'replicate',
	width: 1400,
	height: 650,
	enableRefreshWindow: true,


	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Investment replication is a weighted allocation of investment instruments or/and securities that is trying to mimic a benchmark. "<b>Replication Instruments</b>" Instrument Group limits the list of available instruments in Replication Allocations.<br/>Once a replication has history, the following fields cannot be changed: Replication Type, Reverse Exposure Sign, Dynamic Calculator, and Rebalance Trigger Type.',
					url: 'investmentReplication.json',

					childTables: 'InvestmentReplicationAllocation',
					items: [
						{
							fieldLabel: 'Replication Type', hiddenName: 'type.id', name: 'type.name', xtype: 'combo', url: 'investmentReplicationTypeList.json', loadAll: true,
							detailPageClass: 'Clifton.investment.replication.ReplicationTypeWindow',
							listeners: {
								select: function(combo, record, index) {
									const panel = combo.ownerCt;
									panel.resetGridColumns();
								}
							}
						},
						{fieldLabel: 'Replication Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},
						{boxLabel: 'Inactive', name: 'inactive', xtype: 'checkbox'},
						{boxLabel: 'Allow partially allocated replication (does not enforce total allocation equal to 100%)', name: 'partiallyAllocated', xtype: 'checkbox'},
						{boxLabel: 'Allow using as a matching replication by other replications (can create/remove matching exposure)', name: 'allowedForMatching', xtype: 'checkbox'},
						{boxLabel: 'Reverse exposure sign (Exposure will be the reverse sign of contracts held: negated)', name: 'reverseExposureSign', xtype: 'checkbox'},
						{
							fieldLabel: 'Dynamic Calculator', name: 'dynamicCalculatorBean.name', hiddenName: 'dynamicCalculatorBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Investment Replication Dynamic Calculator', detailPageClass: 'Clifton.system.bean.BeanWindow',
							qtip: 'Dynamic Replication Calculators dynamically build the allocation list and weights for a specific date based on the calculator.',
							getDefaultData: async function() {
								const formPanel = TCG.getParentFormPanel(this);
								const beanGroup = await TCG.data.getDataPromise('systemBeanGroupByName.json?name=Investment Replication Dynamic Calculator', formPanel);
								return {
									type: {group: beanGroup}
								};
							},
							listeners: {
								change: function(combo, record, index) {
									const panel = combo.ownerCt;
									panel.resetGridColumns();
								}
							}
						},
						{
							xtype: 'radiogroup', columns: 3, fieldLabel: 'Rebalance Triggers', name: 'rebalanceTriggerType-Group',
							items: [
								{boxLabel: 'Do Not Use', name: 'rebalanceTriggerType', checked: true, inputValue: ''},
								{
									boxLabel: 'Fixed (Apply Values As Absolute Bands)', name: 'rebalanceTriggerType', inputValue: 'FIXED',
									qtip: 'Process entered rebalance bands (below) for each allocation as a fixed percentage of the total replication.'
								},
								{
									boxLabel: 'Proportional (Apply Values As % of Target)', name: 'rebalanceTriggerType', inputValue: 'PROPORTIONAL',
									qtip: 'Process entered rebalance bands (below) for each allocation proportional to the target.  i.e. A 10% rebalance band for an allocation with a 20% target results in a 2% rebalance band during processing.'
								}
							],
							listeners: {
								change: function(f) {
									const panel = TCG.getParentFormPanel(f);
									panel.resetGridColumns();
								}
							}
						},
						{
							xtype: 'formgrid-scroll',
							title: 'Replication Allocations',
							storeRoot: 'allocationList',
							dtoClass: 'com.clifton.investment.replication.InvestmentReplicationAllocation',
							height: 245,
							heightResized: true,

							onAfterUpdateFieldValue: async function(editor, field) {
								const f = editor.field;
								const v = editor.value;
								if (TCG.isNotBlank(v) && (f === 'replicationInstrument.label' || f === 'replicationSecurityGroup.name')) {
									const currentCalc = editor.record.get('currentSecurityCalculatorBean.name');
									if (TCG.isBlank(currentCalc)) {
										let defaultCalc;
										if (f === 'replicationInstrument.label') {
											defaultCalc = await TCG.data.getDataPromiseUsingCaching('systemBeanByName.json?name=Default Futures Roll Schedule Current Security Calculator', this, 'investment.replication.instrument.current.security.calc.default');
										}
										else {
											defaultCalc = await TCG.data.getDataPromiseUsingCaching('systemBeanByName.json?name=Default Current Contract Calculator', this, 'investment.replication.securityGroup.current.security.calc.default');
										}
										if (defaultCalc) {
											editor.record.set('currentSecurityCalculatorBean.id', defaultCalc.id);
											editor.record.set('currentSecurityCalculatorBean.name', defaultCalc.name);
										}
									}
								}
							},


							columnsConfig: [
								{header: 'Child Replication', width: 300, dataIndex: 'childReplication.label', idDataIndex: 'childReplication.id', editor: {xtype: 'combo', searchFieldName: 'childReplicationId', url: 'investmentReplicationListFind.json', detailPageClass: 'Clifton.investment.replication.ReplicationWindow'}, hidden: true},
								{header: 'Instrument', width: 140, dataIndex: 'replicationInstrument.label', idDataIndex: 'replicationInstrument.id', editor: {xtype: 'combo', url: 'investmentInstrumentListFind.json?investmentGroupName=Replication Instruments', displayField: 'label', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow'}},
								{
									header: 'Security', width: 130, dataIndex: 'replicationSecurity.label', idDataIndex: 'replicationSecurity.id', editor: {xtype: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
									tooltip: 'Most of the time allocations should use the instrument. However, if it is necessary to use specific securities that belong to the same instrument (spread between different expiration months for a Future, etc.), then security specific allocation should be used.'
								},
								{
									header: 'Security Group', width: 130, dataIndex: 'replicationSecurityGroup.name', idDataIndex: 'replicationSecurityGroup.id', editor: {xtype: 'combo', url: 'investmentSecurityGroupListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityGroupWindow'},
									tooltip: 'Security Groups can be used to defined a specific set of securities that would apply to this allocation.  This option can be used independently or with the instrument to filter an instrument\'s securities.  For example, can select an Option instrument and Security Group Put Options to exclude Calls.'
								},

								{
									header: 'Matching', width: 110, dataIndex: 'matchingReplication.name', idDataIndex: 'matchingReplication.id', editor: {xtype: 'combo', url: 'investmentReplicationListFind.json?allowedForMatching=true', detailPageClass: 'Clifton.investment.replication.ReplicationWindow'},
									tooltip: 'Matching replication will overlay the same notional amount as this allocated to this replication allocation. It is often used to remove FX exposure by getting matching currency exposure to corresponding foreign exposure.'
								},

								{
									header: 'Duration Override', width: 70, dataIndex: 'durationOverride', type: 'float', editor: {xtype: 'floatfield'}, useNull: true,
									tooltip: 'Duration Supported Replications (i.e. Fixed Income): Optionally override to Contract Duration in Contract Value calculation'
								},

								{
									header: 'Use Benchmark Duration', width: 70, dataIndex: 'useBenchmarkDuration', type: 'boolean', editor: {xtype: 'checkbox'},
									tooltip: 'Duration Supported Replications (i.e. Fixed Income): Use Benchmark Duration (Useful if the replication is Fixed Income, however it has a commodity in it.  Sets the duration equal to the benchmark duration so the value isn\'t adjusted (see Contract Value formula).'
								},

								{header: 'Order', width: 55, dataIndex: 'order', type: 'int', editor: {xtype: 'integerfield'}},
								{header: 'Allocation', width: 70, dataIndex: 'allocationWeight', type: 'currency', numberFormat: '0,000.00000', editor: {xtype: 'floatfield', allowBlank: false}, summaryType: 'sum'},

								{header: 'Tranche Schedule', width: 80, dataIndex: 'trancheSchedule.label', idDataIndex: 'trancheSchedule.id', useNull: true, hidden: true, editor: {xtype: 'combo', url: 'calendarScheduleListFind.json?calendarScheduleTypeName=Investment Security Schedules', detailPageClass: 'Clifton.calendar.schedule.ScheduleWindow'}, tooltip: 'The tranche schedule this allocation represents for the replication. This field is used with Tranche Count by a dynamic calculator to calculate the replication allocations.'},
								{header: 'Tranches', width: 70, dataIndex: 'trancheCount', type: 'int', useNull: true, hidden: true, editor: {xtype: 'integerfield', minValue: 0}, tooltip: 'The number of tranches this allocation represents for the replication. This field is used with Tranche Schedule by a dynamic calculator to calculate the replication allocations.'},

								{
									header: 'Rebal Min %', width: 75, dataIndex: 'rebalanceAllocationMin', useNull: true, type: 'currency', numberFormat: '0,000.00000', editor: {xtype: 'currencyfield', minValue: 0, decimalPrecision: 5},
									tooltip: 'All rebalancing targets should be entered as positive values. Allowed for entry when a Rebalance Trigger type is selected for the replication.'
								},
								{
									header: 'Rebal Max %', width: 75, dataIndex: 'rebalanceAllocationMax', useNull: true, type: 'currency', numberFormat: '0,000.00000', editor: {xtype: 'currencyfield', minValue: 0, decimalPrecision: 5},
									tooltip: 'All rebalancing targets should be entered as positive values. Allowed for entry when a Rebalance Trigger type is selected for the replication.'
								},
								{
									header: 'Current Security Calculator', width: 130, dataIndex: 'currentSecurityCalculatorBean.name', idDataIndex: 'currentSecurityCalculatorBean.id', editor: {xtype: 'combo', url: 'systemBeanListFind.json?groupName=Investment Current Security Calculator', detailPageClass: 'Clifton.system.bean.BeanWindow'},
									tooltip: 'Current Security Calculator defines the behavior of how the <i>current</i> security is selected when processing daily runs.  Default behavior is to first check the <b><i>Current Securities</i></b> security group, otherwise the first active security ordered by end date ascending.  <br/><br/>Note: This does not apply if a specific security is selected for the allocation.'
								},
								{
									header: 'Current Security Target Adjustment', width: 130, dataIndex: 'currentSecurityTargetAdjustmentTypeLabel', idDataIndex: 'currentSecurityTargetAdjustmentType',
									editor: {
										displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
										store: new Ext.data.ArrayStore({
											fields: ['value', 'name', 'description'],
											data: Clifton.investment.instrument.CurrentSecurityTargetAdjustmentTypes
										})
									},
									tooltip: 'Defines when portfolio holds additional securities that are the non-current, how the targets are adjusted for the non-current and current securities.'
								},
								{
									header: 'Always Include Current Security', width: 100, dataIndex: 'alwaysIncludeCurrentSecurity', type: 'boolean', editor: {xtype: 'checkbox'},
									tooltip: 'This flag determines whether or not a current security is always included during portfolio run processing, even if it results in a 0 allocation (0 target, 0 exposure). If FALSE, the current security will not be included if it results in a 0 allocation.'
								},
								{
									header: 'Portfolio Defines Current Security', width: 100, dataIndex: 'portfolioSelectedCurrentSecurity', type: 'boolean', editor: {xtype: 'checkbox'},
									tooltip: 'Used for allocations using Security Groups to include all securities that have an open position within the portfolio. If TRUE, current security is determined by existing positions within the portfolio during portfolio run processing, overriding the initial current security determined by the replication allocation settings. If existing positions cannot be used to determine the current security, the system will default the current security to the farthest end date, start date, or highest security ID based on securities within the Security Group. If FALSE, the current security will always be the initial current security determined by the replication allocation settings.'
								},
								{
									header: 'Security Is Underlying', width: 100, dataIndex: 'securityUnderlying', type: 'boolean', editor: {xtype: 'checkbox'},
									tooltip: 'Indicates that the Security is the Underlying Security. This is useful when the goal is to include Flex, Listed and OTC securities. For example, using security group Put Options and specifying SPX as the Underlying Security to get SPX FLEX, SPXW, etc.'
								},
								{
									header: 'Type Override', width: 130, dataIndex: 'replicationTypeOverride.name', idDataIndex: 'replicationTypeOverride.id', editor: {xtype: 'combo', url: 'investmentReplicationTypeListFind.json', detailPageClass: 'Clifton.investment.replication.ReplicationTypeWindow'},
									tooltip: 'Use for rare cases where a specific allocation should be associated with a different replication type (i.e. contract value calculation).'
								}
							],
							plugins: {ptype: 'gridsummary'}
						}
					],
					getAllocationsGrid: function() {
						return this.findByType('formgrid')[0];
					},

					getReplicationTypeObject: function() {
						return TCG.data.getData('investmentReplicationType.json?id=' + this.getForm().findField('type.id').getValue(), this);
					},

					resetGridColumns: function() {
						const grid = this.getAllocationsGrid();
						const sm = grid.getColumnModel();
						const repType = this.getReplicationTypeObject();

						const rollupReplication = repType && TCG.isTrue(repType.rollupReplication);
						sm.setHidden(sm.findColumnIndex('childReplication.label'), !rollupReplication);
						sm.setHidden(sm.findColumnIndex('replicationInstrument.label'), rollupReplication);
						sm.setHidden(sm.findColumnIndex('replicationSecurity.label'), rollupReplication);
						sm.setHidden(sm.findColumnIndex('securityUnderlying'), rollupReplication);
						sm.setHidden(sm.findColumnIndex('replicationSecurityGroup.name'), rollupReplication);
						sm.setHidden(sm.findColumnIndex('matchingReplication.name'), rollupReplication);

						if (rollupReplication) {
							this.updateDynamicCalculatorBean(repType);
						}

						const durationSupported = repType && TCG.isTrue(repType.durationSupport);
						sm.setHidden(sm.findColumnIndex('durationOverride'), !durationSupported);
						sm.setHidden(sm.findColumnIndex('useBenchmarkDuration'), !durationSupported);

						const dynamic = TCG.isNotBlank(this.getForm().findField('dynamicCalculatorBean.id').getValue());
						sm.setHidden(sm.findColumnIndex('currentSecurityCalculatorBean.name'), dynamic);
						sm.setHidden(sm.findColumnIndex('currentSecurityTargetAdjustmentTypeLabel'), dynamic);
						sm.setHidden(sm.findColumnIndex('alwaysIncludeCurrentSecurity'), dynamic);
						sm.setHidden(sm.findColumnIndex('portfolioSelectedCurrentSecurity'), dynamic);
						sm.setHidden(sm.findColumnIndex('trancheSchedule.label'), !dynamic);
						sm.setHidden(sm.findColumnIndex('trancheCount'), !dynamic);

						// If any rebalancing trigger type is selected, enable min/max columns
						const rtt = this.getForm().findField('rebalanceTriggerType').getGroupValue();
						const fixedOrProportional = rtt === 'FIXED' || rtt === 'PROPORTIONAL';
						sm.setHidden(sm.findColumnIndex('rebalanceAllocationMin'), !fixedOrProportional);
						sm.setHidden(sm.findColumnIndex('rebalanceAllocationMax'), !fixedOrProportional);
					},

					updateDynamicCalculatorBean: function(repType) {
						if (repType.name === 'Blended') {
							const calculatorBeanField = this.getForm().findField('dynamicCalculatorBean.id');
							if (TCG.isBlank(calculatorBeanField.getValue())) {
								const defaultCalc = TCG.data.getData('systemBeanByName.json?name=Default Blended Replication Calculator', this, 'investment.replication.dynamic.blended.default');
								if (defaultCalc) {
									calculatorBeanField.setValue({value: defaultCalc.id, text: defaultCalc.name}, true);
								}
							}
							const replicationNameField = this.getForm().findField('name');
							if (TCG.isBlank(replicationNameField.getValue())) {
								replicationNameField.setValue('Blended: ');
							}
						}
					},

					listeners: {
						afterload: function(panel) {
							panel.resetGridColumns();
						}
					}
				}]
			},


			{
				title: 'Allocation History',
				items: [{
					name: 'investmentReplicationAllocationListFind',
					xtype: 'gridpanel',
					instructions: 'The following allocation history exists for this replication.  This allocation history shows user changes to the replication allocations on the info tab over time. See Security Allocation History tab for current securities as calculated by the system per balance date.',
					additionalPropertiesToRequest: 'childReplication.id',
					getLoadParams: function(firstLoad) {
						const dynamic = this.getWindow().getMainForm().findField('dynamicCalculatorBean.id').getValue();
						if (firstLoad) {
							const sm = this.grid.getColumnModel();
							sm.setHidden(sm.findColumnIndex('dynamicResult'), !TCG.isNotBlank(dynamic));
							const repType = this.getWindow().getMainFormPanel().getReplicationTypeObject();
							const rollupReplication = repType && TCG.isTrue(repType.rollupReplication);
							sm.setHidden(sm.findColumnIndex('childReplication.label'), !rollupReplication);
							sm.setHidden(sm.findColumnIndex('allocationLabel'), !rollupReplication);
							sm.setHidden(sm.findColumnIndex('replicationInstrument.label'), rollupReplication);
							sm.setHidden(sm.findColumnIndex('replicationSecurity.label'), rollupReplication);
							sm.setHidden(sm.findColumnIndex('replicationSecurityGroup.name'), rollupReplication);
							sm.setHidden(sm.findColumnIndex('matchingReplication.name'), rollupReplication);
						}
						const t = this.getTopToolbar();
						let dateValue = null;
						const df = TCG.getChildByName(t, 'activeOnDate');
						if (TCG.isNotBlank(df.getValue())) {
							if (dynamic) {
								dateValue = (df.getValue()).format('m/d/Y');
							}
							else {
								dateValue = (df.getValue()).format('m/d/Y H:i:s');
							}
						}
						else if (firstLoad) {
							if (dynamic) {
								dateValue = (df.getValue()).format('m/d/Y');
							}
							else {
								dateValue = (new Date()).format('m/d/Y H:i:s');
							}
							df.setValue(dateValue);
						}
						const result = {activeOnDate: dateValue};
						result.replicationId = this.getWindow().getMainFormId();
						return result;
					},
					getTopToolbarFilters: function(toolbar) {
						const filters = [];
						filters.push({fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate', format: 'm/d/Y H:i:s', value: (new Date()).format('m/d/Y H:i:s')});
						return filters;
					},
					columns: [
						{header: 'Dynamic Result', hidden: true, width: 60, dataIndex: 'dynamicResult', type: 'boolean'},
						{
							header: 'Child Replication', width: 200, dataIndex: 'childReplication.label', idDataIndex: 'childReplication.id', filter: {xtype: 'combo', searchFieldName: 'childReplicationId', url: 'investmentReplicationListFind.json'}, hidden: true,
							eventListeners: {
								dblclick: function(column, grid, rowIndex, event) {
									const config = {
										params: {id: grid.getStore().getAt(rowIndex).data['childReplication.id']},
										openerCt: grid.ownerGridPanel
									};
									TCG.createComponent('Clifton.investment.replication.ReplicationWindow', config);
								}
							}
						},
						{header: 'Allocation', width: 200, dataIndex: 'allocationLabel', sortable: false},
						{header: 'Instrument', hidden: true, width: 140, dataIndex: 'replicationInstrument.label', filter: {searchFieldName: 'replicationInstrumentId', xtype: 'combo', url: 'investmentInstrumentListFind.json'}},
						{header: 'Security', hidden: true, width: 130, dataIndex: 'replicationSecurity.label', filter: {searchFieldName: 'replicationSecurityId', xtype: 'combo', url: 'investmentSecurityListFind.json'}},
						{header: 'Security Group', hidden: true, width: 130, dataIndex: 'replicationSecurityGroup.name', filter: {searchFieldName: 'replicationSecurityGroupId', xtype: 'combo', url: 'investmentSecurityGroupListFind.json'}},
						{header: 'Matching', width: 110, dataIndex: 'matchingReplication.name', filter: {searchFieldName: 'matchingReplicationId', xtype: 'combo', url: 'investmentReplicationListFind.json?allowedForMatching=true'}},
						{header: 'Duration Override', hidden: true, width: 60, dataIndex: 'durationOverride', type: 'float', useNull: true},
						{header: 'Use Benchmark Duration', hidden: true, width: 60, dataIndex: 'useBenchmarkDuration', type: 'boolean'},
						{header: 'Order', width: 50, dataIndex: 'order', type: 'int'},
						{header: 'Weight', width: 75, dataIndex: 'allocationWeight', type: 'currency', numberFormat: '0,000.00000'},
						{header: 'Rebal Min %', hidden: true, width: 75, dataIndex: 'rebalanceAllocationMin', useNull: true, type: 'percent'},
						{header: 'Rebal Max %', hidden: true, width: 75, dataIndex: 'rebalanceAllocationMax', useNull: true, type: 'percent'},
						{header: 'Current Security Calculator', width: 150, dataIndex: 'currentSecurityCalculatorBean.name', filter: {searchFieldName: 'currentSecurityCalculatorBeanId', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Investment Current Security Calculator'}},
						{
							header: 'Current Security Target Adjustment', width: 130, dataIndex: 'currentSecurityTargetAdjustmentTypeLabel', filter: {
								searchFieldName: 'currentSecurityTargetAdjustmentType', displayField: 'name', valueField: 'value', mode: 'local', type: 'combo',
								store: new Ext.data.ArrayStore({
									fields: ['value', 'name', 'description'],
									data: Clifton.investment.instrument.CurrentSecurityTargetAdjustmentTypes
								})
							}
						},
						{header: 'Always Include Current Security', width: 100, dataIndex: 'alwaysIncludeCurrentSecurity', type: 'boolean', tooltip: 'This flag determines whether or not a current security is always included during portfolio run processing, even if it results in a 0 allocation (0 target, 0 exposure). If FALSE, the current security will not be included if it results in a 0 allocation.'},
						{header: 'Portfolio Defines Current Security', width: 70, dataIndex: 'portfolioSelectedCurrentSecurity', type: 'boolean'},
						{header: 'Security Is Underlying', width: 70, dataIndex: 'securityUnderlying', type: 'boolean', tooltip: 'Indicates that the Security is the Underlying Security. This is useful when the goal is to include Flex, Listed and OTC securities. For example, using security group Put Options and specifying SPX as the Underlying Security to get SPX FLEX, SPXW, etc.'},
						{header: 'Start Date', width: 100, dataIndex: 'startDate'},
						{header: 'End Date', width: 100, dataIndex: 'endDate'}
					]
				}]
			},


			{
				title: 'Security Allocation History',
				items: [{
					name: 'investmentReplicationAllocationHistoryListFind',
					xtype: 'gridpanel',
					instructions: 'The following security allocation exists for this replication. This history shows the current security calculated by the defined current security calculator and weight per balance date.',
					appendStandardColumns: false, // Manually added standard columns since we need to use replicationHistory
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 30 days of  history
							const dateV = new Date().add(Date.DAY, -30);
							this.setFilterValue('replicationHistory.endDate', {'after': dateV});
						}

						const t = this.getTopToolbar();
						const result = {};
						const df = TCG.getChildByName(t, 'activeOnDate');
						if (TCG.isNotBlank(df.getValue())) {
							result.activeOnDate = (df.getValue()).format('m/d/Y');
							// If active on date is set, clear the start/end filters
							this.clearFilter('replicationHistory.endDate');
							this.clearFilter('replicationHistory.startDate');
						}
						result.replicationId = this.getWindow().getMainFormId();
						return result;
					},
					getTopToolbarFilters: function(toolbar) {
						const filters = [];
						filters.push({fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'});
						return filters;
					},
					columns: [
						{header: 'Replication History ID', hidden: true, width: 20, dataIndex: 'replicationHistory.id', type: 'int', filter: {searchFieldName: 'replicationHistoryId'}},
						{header: 'Balance Start Date', width: 60, dataIndex: 'replicationHistory.startDate', filter: {searchFieldName: 'startDate'}},
						{header: 'Balance End Date', width: 60, dataIndex: 'replicationHistory.endDate', filter: {searchFieldName: 'endDate'}},
						{header: 'Allocation Label', width: 200, dataIndex: 'replicationAllocation.allocationLabel', filter: {searchFieldName: 'allocationLabel'}},
						{header: 'Order', width: 45, dataIndex: 'replicationAllocation.order', type: 'int', useNull: true, filter: {searchFieldName: 'allocationOrder'}},
						{header: 'Current Security', width: 200, dataIndex: 'currentInvestmentSecurity.label', filter: {searchFieldName: 'currentInvestmentSecurityLabel'}},
						{header: 'Allocation Weight', width: 70, dataIndex: 'replicationAllocation.allocationWeight', type: 'currency', numberFormat: '0,000.00000', filter: {searchFieldName: 'allocationWeight'}},
						{
							header: 'Target Adjustment', width: 100, dataIndex: 'replicationAllocation.currentSecurityTargetAdjustmentTypeLabel', filter: {
								searchFieldName: 'currentSecurityTargetAdjustmentType', displayField: 'name', valueField: 'value', mode: 'local', type: 'combo',
								store: new Ext.data.ArrayStore({
									fields: ['value', 'name', 'description'],
									data: Clifton.investment.instrument.CurrentSecurityTargetAdjustmentTypes
								})
							}, tooltip: 'Used during portfolio processing, When both non-current and current security are available determines how to adjust the targets.'
						},
						{header: 'Always Include Current Security', width: 100, dataIndex: 'replicationAllocation.alwaysIncludeCurrentSecurity', type: 'boolean', filter: {searchFieldName: 'alwaysIncludeCurrentSecurity'}, tooltip: 'This flag determines whether or not a current security is always included during portfolio run processing, even if it results in a 0 allocation (0 target, 0 exposure). If FALSE, the current security will not be included if it results in a 0 allocation.'},
						{header: 'Portfolio Defines Current Security', width: 100, dataIndex: 'replicationAllocation.portfolioSelectedCurrentSecurity', type: 'boolean', filter: {searchFieldName: 'portfolioSelectedCurrentSecurity'}, tooltip: 'Used during portfolio processing for security groups only.  Allows the portfolio to reset the current security based on positions held (picks first one for max end date, max start date, max security id). If no positions are held, the portfolio includes the calculated current security.'},
						{
							header: 'Created By', width: 30, dataIndex: 'replicationHistory.createUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'createUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true},
							renderer: Clifton.security.renderSecurityUser,
							exportColumnValueConverter: 'securityUserColumnReversableConverter'
						},
						{header: 'Created On', width: 40, dataIndex: 'replicationHistory.createDate', hidden: true},
						{
							header: 'Updated By', width: 30, dataIndex: 'replicationHistory.updateUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'updateUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true},
							renderer: Clifton.security.renderSecurityUser,
							exportColumnValueConverter: 'securityUserColumnReversableConverter'
						},
						{header: 'Updated On', width: 40, dataIndex: 'replicationHistory.updateDate', hidden: true}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.investment.replication.history.ReplicationHistoryWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.replicationHistory.id;
						}
					},
					addFirstToolbarButtons: function(toolBar, gridPanel) {
						toolBar.add({
							text: 'Clear History',
							tooltip: 'Clears entire history for the replication.  <b>Note:</b> Can only be performed if the replication history has NEVER been used.',
							iconCls: 'remove',
							handler: function() {
								Ext.Msg.confirm('Clear Replication History', 'Are you sure you would like to clear <b>ALL</b> replication history for this replication?  Note: No history will be cleared if any record is being used by portfolio runs.', function(a) {
									if (TCG.isEqualsStrict(a, 'yes')) {
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel.ownerCt,
											waitMsg: 'Clearing History...',
											params: {
												replicationId: gridPanel.getWindow().getMainFormId()
											},
											onLoad: function(record, conf) {
												gridPanel.reload();
											}
										});
										loader.load('investmentReplicationHistoryListClear.json');
									}
								});
							}
						});
						toolBar.add('-');
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'InvestmentReplication'
				}]
			},


			{
				title: 'Used By',
				items: [{
					name: 'investmentAccountAssetClassListFind',
					xtype: 'gridpanel',
					instructions: 'The following Investment Account asset classes use the replication as either its primary or secondary replication/',
					getLoadParams: function() {
						return {useReplicationId: this.getWindow().getMainForm().formValues.id};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Account', width: 200, dataIndex: 'account.label', filter: {searchFieldName: 'investmentAccountLabel'}, defaultSortColumn: true},
						{header: 'Asset Class', width: 100, dataIndex: 'label', filter: {searchFieldName: 'label'}},
						{header: 'Primary Replication', width: 100, dataIndex: 'primaryReplication.name', filter: {searchFieldName: 'primaryReplication'}},
						{header: 'Secondary Replication', width: 100, dataIndex: 'secondaryReplication.name', filter: {searchFieldName: 'secondaryReplication'}},
						{header: 'Rollup', dataIndex: 'rollupAssetClass', width: 60, type: 'boolean', hidden: true}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: {
							getItemText: function(rowItem) {
								return TCG.isTrue(rowItem.get('rollupAssetClass')) ? 'Rollup Asset Class' : 'Asset Class';
							},
							items: [
								{text: 'Asset Class', iconCls: 'grouping', className: 'Clifton.investment.account.assetclass.AssetClassWindow'},
								{text: 'Rollup Asset Class', iconCls: 'hierarchy', className: 'Clifton.investment.account.assetclass.RollupAssetClassWindow'}
							]
						}
					}
				}]
			}
		]
	}]
});
