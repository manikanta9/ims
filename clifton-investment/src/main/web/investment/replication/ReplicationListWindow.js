Clifton.investment.replication.ReplicationListWindow = Ext.extend(TCG.app.Window, {
	id: 'investmentReplicationListWindow',
	title: 'Investment Replications',
	iconCls: 'replicate',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Replications',
				items: [{
					xtype: 'gridpanel',
					name: 'investmentReplicationListFind',
					instructions: 'Investment replication is a weighted allocation of investment instruments or/and securities that is trying to mimic a benchmark.',
					importTableName: 'InvestmentReplicationAllocation', // Not actually supported upload table name, but used to drive if import button shows up
					importComponentName: 'Clifton.investment.replication.upload.ReplicationAllocationUploadWindow',
					wikiPage: 'IT/Investment+Replication',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Replication Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Replication Type', width: 80, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', url: 'investmentReplicationTypeListFind.json'}},
						{header: 'Contract Value Calculator', width: 80, dataIndex: 'type.calculatorBean.name', filter: {searchFieldName: 'calculatorBeanId', type: 'combo', url: 'systemBeanListFind.json?groupName=Investment Replication Calculator'}},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Dynamic Calculator', width: 50, dataIndex: 'dynamicCalculatorBean.name', filter: {searchFieldName: 'dynamicCalculatorBeanName'}},
						{header: 'Rebalance Trigger', width: 40, dataIndex: 'rebalanceTriggerType'},
						{header: 'Partial', width: 25, dataIndex: 'partiallyAllocated', type: 'boolean'},
						{header: 'Matching Allowed', width: 40, dataIndex: 'allowedForMatching', type: 'boolean'},
						{header: 'Negate Exposure', width: 40, dataIndex: 'reverseExposureSign', type: 'boolean'},
						{header: 'Inactive', width: 25, dataIndex: 'inactive', type: 'boolean'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Replication Type', width: 180, xtype: 'combo', url: 'investmentReplicationTypeList.json', loadAll: true, linkedFilter: 'type.name'},
							{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('inactive', false);
						}
					},
					editor: {
						detailPageClass: 'Clifton.investment.replication.ReplicationWindow'
					}
				}]
			},


			{
				title: 'Replication Types',
				items: [{
					name: 'investmentReplicationTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Investment replication type defines the types of securities that are in the replication and how Portfolio processing will handle processing & calculating contract values.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 75, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Order', width: 15, dataIndex: 'order', type: 'int', useNull: true, defaultSortColumn: true},
						{
							header: 'Contract Value Calculator', width: 80, dataIndex: 'calculatorBean.name', filter: {searchFieldName: 'calculatorBeanId', type: 'combo', url: 'systemBeanListFind.json?groupName=Investment Replication Calculator'},
							tooltip: 'Calculation Type used to determine the value of one contract'
						},
						{
							header: 'Performance Financing Rate', width: 80, dataIndex: 'financingRateIndex.label', filter: {searchFieldName: 'financingRateIndexName'},
							tooltip: 'Financing rate for this replication type - used for performance summaries'
						},
						{
							header: 'Interest Rate Group', width: 75, dataIndex: 'interestRateIndexGroup.name', filter: {searchFieldName: 'interestRateIndexGroupName'},
							tooltip: 'Defines which interest rate indices apply to this replication type, which is then further filtered based on the days to maturity vs. number of days for the interest rate to determine the <i>closest</i> rate to use.<br/><br/>Required to be true if the selected calculator uses the interest rate in its calculation.  If not required, can optionally be selected to calculate/display interest rate even if not used to calculate contract value.'
						},
						{header: 'Description', width: 310, dataIndex: 'description', hidden: true},
						{
							header: 'Credit Duration Supported', width: 40, dataIndex: 'creditDurationSupported', type: 'boolean', hidden: true,
							tooltip: 'Required to be true if the selected calculator uses the credit duration in its calculation.'
						},
						{
							header: 'Duration Supported', width: 40, dataIndex: 'durationSupported', type: 'boolean', hidden: true,
							tooltip: 'Required to be true if the selected calculator uses the duration in its calculation.  If not required, can optionally be selected to calculate/display duration even if not used to calculate contract value.'
						},
						{
							header: 'Syn Adj Factor Supported', width: 40, dataIndex: 'syntheticAdjustmentFactorSupported', type: 'boolean', hidden: true,
							tooltip: 'Formula: 1 / (1+((Days to Maturity/365)*Interest Rate))<br/><br/>Required to be true if the selected calculator uses the synthetic adjustment factor in its calculation.  If not required, can optionally be selected to calculate/display synthetic adjustment factor even if not used to calculate contract value.'
						},
						{
							header: 'Index Ratio Supported', width: 40, dataIndex: 'indexRatioSupported', type: 'boolean', hidden: true,
							tooltip: 'Required to be true if the selected calculator uses the index ratio in its calculation.  Note that the dirty price calculation uses index ratio.  If not required, can optionally be selected to calculate/display index ratio even if not used to calculate contract value.'
						},
						{
							header: 'Dirty Price Supported', width: 40, dataIndex: 'dirtyPriceSupported', type: 'boolean', hidden: true,
							tooltip: 'Required to be true if the selected calculator uses the dirty price in its calculation.  If not required, can optionally be selected to calculate/display index ratio even if not used to calculate contract value.'
						},
						{header: 'Delta Supported', width: 40, dataIndex: 'deltaSupported', type: 'boolean', hidden: true},
						{
							header: 'Do Not Apply Exposure Multiplier', width: 40, dataIndex: 'doNotApplyExposureMultiplier', type: 'boolean', hidden: true,
							tooltip: 'If selected, the exposure multiplier will not be applied to notional calculations (See Euro Dollar/Euro Yen)'
						},
						{
							header: 'Do Not Apply M2M Adjustment', width: 40, dataIndex: 'doNotApplyM2MAdjustment', type: 'boolean', hidden: true,
							tooltip: 'M2M Adjustment on the Trading Screen based on price change (when update with live prices). Currently doesn\'t apply to Options replication calc because change in price doesn\'t mean anything.'
						},
						{
							header: 'Use Ultimate Underlying', width: 40, dataIndex: 'ultimateUnderlyingUsed', type: 'boolean', hidden: true,
							tooltip: 'When true, the replication will display the ultimate underlying security and use that security\'s price in any calculator that uses the underlying security price.'
						},
						{
							header: 'Currency', width: 40, dataIndex: 'currency', type: 'boolean', hidden: true,
							tooltip: 'Currency replications have special meaning and when noted as a "currency" replication, physical and unrealized currency balances can be included in the total allocation.'
						},
						{
							header: 'Do Not Apply Currency Other Allocation', width: 40, dataIndex: 'doNotApplyCurrencyOtherAllocation', type: 'boolean', hidden: true,
							tooltip: 'If selected, currency other will still be generated if needed, however this replication type excludes allocating base currency other and unrealized base currency other to it.'
						}
					],
					editor: {
						detailPageClass: 'Clifton.investment.replication.ReplicationTypeWindow'
					}
				}]
			},


			{
				title: 'Contract Value Calculators',
				items: [{
					name: 'systemBeanListFind',
					xtype: 'gridpanel',
					groupField: 'type.name',
					// TODO ADD TYPE.DESCRIPTION AS TOOLTIP FOR THE GROUP HEADER???
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Calculators" : "Calculator"]}',
					instructions: 'Contract Value Calculators are used during daily runs to evaluate for a replication allocation the value of a given contract, as well as how other fields are populated.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Type Name', width: 15, dataIndex: 'type.name', hidden: true},
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Replication Calculator Name', width: 100, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Description', width: 220, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.system.bean.BeanWindow',
						getDefaultData: function(gridPanel) {
							const grp = TCG.data.getData('systemBeanGroupByName.json?name=Investment Replication Calculator', gridPanel, 'system.bean.group.' + 'Investment Replication Calculator');
							return {type: {group: grp}};
						}
					},
					getTopToolbarInitialLoadParams: function() {
						return {groupName: 'Investment Replication Calculator'};
					}
				}]
			},


			{
				title: 'Current Security Calculators',
				items: [{
					name: 'systemBeanListFind',
					xtype: 'gridpanel',
					groupField: 'type.name',
					// TODO ADD TYPE.DESCRIPTION AS TOOLTIP FOR THE GROUP HEADER???
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Calculators" : "Calculator"]}',
					instructions: 'Current Security Calculators are used during daily runs to evaluate for a replication allocation what the current security is. Current securities are used in two ways: ' +
						'1.  When a client does not hold any positions in a security that falls into the allocation, the system must then try to determine the best fitting security to use.' +
						'2.  When a client holds multiple securities for an instrument, or the instrument is rolling, if the system knows the current one it can be added to the daily run and all suggested trades to open positions will be applied to this \'current\' one.' +
						'By default, the system uses a default calculator that first checks the \'Current Securities\' security group for a matching security, otherwise will return the first active one for the instrument.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Type Name', width: 15, dataIndex: 'type.name', hidden: true},
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Current Security Calculator Name', width: 100, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Description', width: 220, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.system.bean.BeanWindow',
						getDefaultData: function(gridPanel) {
							const grp = TCG.data.getData('systemBeanGroupByName.json?name=Investment Current Security Calculator', gridPanel, 'system.bean.group.' + 'Investment Replication Current Security Calculator');
							return {type: {group: grp}};
						}
					},
					getTopToolbarInitialLoadParams: function() {
						return {groupName: 'Investment Current Security Calculator'};
					}
				}]
			},


			{
				title: 'Dynamic Replication Calculators',
				items: [{
					name: 'systemBeanListFind',
					xtype: 'gridpanel',
					groupField: 'type.name',
					// TODO ADD TYPE.DESCRIPTION AS TOOLTIP FOR THE GROUP HEADER???
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Calculators" : "Calculator"]}',
					instructions: 'Dynamic Replication Calculators are used during daily runs to evaluate for a replication the full allocation list and weights for each allocation.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Type Name', width: 15, dataIndex: 'type.name', hidden: true},
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Dynamic Replication Calculator Name', width: 100, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Description', width: 220, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.system.bean.BeanWindow',
						getDefaultData: function(gridPanel) {
							const grp = TCG.data.getData('systemBeanGroupByName.json?name=Investment Replication Dynamic Calculator', gridPanel, 'system.bean.group.' + 'Investment Replication Dynamic Calculator');
							return {type: {group: grp}};
						}
					},
					getTopToolbarInitialLoadParams: function() {
						return {groupName: 'Investment Replication Dynamic Calculator'};
					}
				}]
			},


			{
				title: 'Administration',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						labelWidth: 140,
						loadValidation: false, // using the form only to get background color/padding
						height: 230,
						buttonAlign: 'right',
						instructions: 'Use this section to (re)build replication history. Select criteria below to filter which Replications runs to (re)build.',
						items: [
							{fieldLabel: 'Replication', name: 'replicationLabel', hiddenName: 'replicationId', xtype: 'combo', url: 'investmentReplicationListFind.json?active=true', detailPageClass: 'Clifton.investment.replication.ReplicationWindow'},
							{
								xtype: 'columnpanel',
								columns: [
									{
										rows: [
											{fieldLabel: '', boxLabel: 'Rebuild Existing records (save new results only if different from existing)', name: 'rebuildExisting', xtype: 'checkbox', qtip: 'If false, will just skip existing records, else will attempt to rebuild, will only save new results if different from existing.'},
											{fieldLabel: '', boxLabel: 'Use Currently Active Allocations (not as of EOD balance date + 1 weekday)', name: 'useCurrentlyActiveAllocations', xtype: 'checkbox', qtip: 'If checked, will rebuilding the history for selected balance dates using the replication allocations as defined currently, not as of EOD balance date + 1 weekday'},
											{fieldLabel: '', boxLabel: 'Exclude Dynamic replications', name: 'excludeDynamic', xtype: 'checkbox', qtip: 'If checked, will exclude dynamic replications from the rebuild. Cannot exclude both dynamic and standard replications.', mutuallyExclusiveFields: ['excludeStandard']},
											{fieldLabel: '', boxLabel: 'Exclude Standard (non-dynamic) replications', name: 'excludeStandard', xtype: 'checkbox', qtip: 'If checked, will exclude standard (non-dynamic) replications from the rebuild.  Cannot exclude both dynamic and standard replications.', mutuallyExclusiveFields: ['excludeDynamic']}
										],
										config: {columnWidth: 0.8}
									},
									{
										rows: [
											{fieldLabel: 'Start Balance Date', xtype: 'datefield', name: 'startBalanceDate', allowBlank: false, value: TCG.getPreviousWeekday()},
											{fieldLabel: 'End Balance Date', xtype: 'datefield', name: 'endBalanceDate', allowBlank: false, value: TCG.getPreviousWeekday()}
										],
										config: {columnWidth: 0.2, labelWidth: 140}
									}
								]
							}
						],

						buttons: [{
							text: 'Rebuild Replication History',
							iconCls: 'run',
							width: 150,
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								form.submit(Ext.applyIf({
									url: 'investmentReplicationHistoryRebuild.json',
									waitMsg: 'Processing...',
									success: function(form, action) {
										Ext.Msg.alert('Processing Started', action.result.status.message, function() {
											const grid = owner.ownerCt.items.get(1);
											grid.reload.defer(300, grid);
										});
									}
								}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
							}
						}]
					},
					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'REPLICATION-HISTORY',
						instantRunner: true,
						instructions: 'The following Replications are being (re)processed right now.',
						title: 'Current Replication Processing',
						flex: 1
					}
				]
			}
		]
	}]
});

