Clifton.investment.replication.history.ReplicationHistoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Replication History',
	iconCls: 'replicate',
	width: 1100,
	height: 650,

	layout: 'border',
	items: [
		{
			xtype: 'formpanel',
			region: 'north',
			instructions: 'Investment replication history shows the actual replication for a given balance date including each allocations current security.',
			url: 'investmentReplicationHistory.json',
			labelWidth: 160,
			readOnly: true,
			height: 200,

			items: [
				{fieldLabel: 'Replication Name', name: 'replication.name', detailIdField: 'replication.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.replication.ReplicationWindow'},
				{fieldLabel: 'Balance Start Date', name: 'startDate', xtype: 'datefield'},
				{fieldLabel: 'Balance End Date', name: 'endDate', xtype: 'datefield'}
			]
		},
		{
			region: 'center',
			xtype: 'gridpanel',
			title: 'Security Allocation History',
			name: 'investmentReplicationAllocationHistoryListFind',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Allocation Label', width: 200, dataIndex: 'replicationAllocation.allocationLabel'},
				{header: 'Order', width: 50, dataIndex: 'replicationAllocation.order', type: 'int', useNull: true},
				{header: 'Current Security', width: 200, dataIndex: 'currentInvestmentSecurity.label'},
				{header: 'Allocation Weight', width: 70, dataIndex: 'replicationAllocation.allocationWeight', type: 'currency', numberFormat: '0,000.00000', summaryType: 'sum'},
				{header: 'Target Adjustment', width: 100, dataIndex: 'replicationAllocation.currentSecurityTargetAdjustmentTypeLabel', tooltip: 'Used during portfolio processing, When both non-current and current security are available determines how to adjust the targets.'},
				{header: 'Always Include Current Security', width: 100, dataIndex: 'replicationAllocation.alwaysIncludeCurrentSecurity', type: 'boolean', tooltip: 'This flag determines whether or not a current security is always included during portfolio run processing, even if it results in a 0 allocation (0 target, 0 exposure). If FALSE, the current security will not be included if it results in a 0 allocation.'},
				{header: 'Portfolio Defines Current Security', width: 100, dataIndex: 'replicationAllocation.portfolioSelectedCurrentSecurity', type: 'boolean'}
			],
			plugins: {ptype: 'gridsummary'},
			getLoadParams: function() {
				const w = this.getWindow();
				return {replicationHistoryId: w.getMainFormId()};
			}
		}
	]
});

