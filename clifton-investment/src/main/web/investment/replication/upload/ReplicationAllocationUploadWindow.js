Clifton.investment.replication.upload.ReplicationAllocationUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'investmentReplicationAllocationUploadWindow',
	title: 'Replication Allocation Updates - Upload Window',
	iconCls: 'import',
	height: 450,
	width: 700,
	hideOKButton: true,

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File',
		handler: function() {
			TCG.openFile('investment/replication/upload/SimpleReplicationAllocationUploadSampleFile.xls');
		}
	}],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Replication Allocation Updates Upload allows using a customized file format to easily upload new Weights, Order, and Current Security Calculators for existing allocations. Note: You can only <b>update</b> existing allocations (found on the replication by matching the instrument, security, security group selections).',

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{fieldLabel: 'Max % Change', name: 'maxAllocationChange', xtype: 'currencyfield', value: 5, qtip: 'Max change an allocation can change by.  Applied as +/-% for increase or decrease in allocation weights.  If blank, this is not validated.'},
			{xtype: 'sectionheaderfield', header: 'If invalid data is encountered'},
			{
				xtype: 'radiogroup', columns: 1,
				items: [
					{boxLabel: 'Fail this upload and don\'t load any data.', name: 'partialUploadAllowed', inputValue: 'false', checked: true},
					{boxLabel: 'Skip rows with invalid data but upload valid rows.', name: 'partialUploadAllowed', inputValue: 'true'}
				]
			},
			{xtype: 'sectionheaderfield', header: 'Upload Results'},
			{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
		],

		getSaveURL: function() {
			return 'investmentReplicationAllocationUploadFileUpload.json';
		}

	}]
});
