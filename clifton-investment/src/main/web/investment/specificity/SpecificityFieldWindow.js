Clifton.investment.specificity.SpecificityFieldWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Specificity Field',
	iconCls: 'grouping',
	height: 365,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'Specificity fields define the field for which a specificity entry under a given specificity definition may apply. These fields may be custom named fields for the definition, or they may be system columns. When determining the most specific investment specificity entries which apply for any entity, one entry may be returned for each specificity field under each definition.',
		url: 'investmentSpecificityField.json',
		labelWidth: 150,
		items: [
			{
				fieldLabel: 'Definition', xtype: 'combo',
				name: 'definition.label', hiddenName: 'definition.id',
				displayField: 'label', url: 'investmentSpecificityDefinitionListFind.json',
				detailPageClass: 'Clifton.investment.specificity.SpecificityDefinitionWindow',
				requestedProps: 'systemTable.id',
				listeners: {
					select: function(combo, record, index) {
						const form = combo.getParentForm();
						if (record.json.hasOwnProperty('systemTable') && record.json.systemTable.hasOwnProperty('id')) {
							form.setFormValue('definition.systemTable.id', record.json.systemTable.id);
						}
						const hasDefinition = !!form.getFormValue('definition.id');
						const hasSystemTable = !!form.getFormValue('definition.systemTable.id');
						form.applyFieldStates(hasDefinition, hasSystemTable);
					}
				}
			},
			{fieldLabel: 'Definition System Table', name: 'definition.systemTable.id', readOnly: true, hidden: true, submitField: false},

			// Only one of [System Column, Field Name] shall be displayed
			{
				fieldLabel: 'System Column', xtype: 'combo',
				name: 'systemColumn.label', hiddenName: 'systemColumn.id',
				displayField: 'label', url: 'systemColumnListFind.json',
				detailPageClass: 'Clifton.system.schema.ColumnWindow',
				mutuallyExclusiveFields: ['name'], qtip: 'The System Column for this field.',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						combo.store.setBaseParam('tableId', combo.getParentForm().getFormValue('definition.systemTable.id'));
						combo.store.setDefaultSort('label');
					}
				}
			},
			{fieldLabel: 'Field Name', name: 'name', mutuallyExclusiveFields: ['systemColumn.id'], qtip: 'The name of this field.', readOnly: true, hidden: true},

			{fieldLabel: 'Field Description', name: 'description', xtype: 'textarea', qtip: 'The description for this field.'},
			{boxLabel: 'System Defined', xtype: 'checkbox', name: 'systemDefined'}
		],

		listeners: {
			afterrender: function() {
				const form = this;
				const fieldEntity = this.getWindow().defaultData;
				let hasDefinition = !!form.getFormValue('definition.id');
				let hasSystemTable = !!form.getFormValue('definition.systemTable.id');
				if (fieldEntity) {
					hasDefinition = !!TCG.getValue('definition.id', fieldEntity);
					hasSystemTable = !!TCG.getValue('definition.systemTable.id', fieldEntity);
				}
				form.applyFieldStates(hasDefinition, hasSystemTable);
			},

			afterload: function(panel, closeOnSuccess) {
				const form = this;
				// Skip UI modifications if window is about to be closed
				if (!closeOnSuccess) {
					form.applyFieldStates();
				}
			}
		},

		applyFieldStates: function(hasDefinition, hasSystemTable) {
			const form = this;
			const editMode = !!form.getIdFieldValue();
			if (editMode) {
				const systemDefined = !!form.getFormValue('systemDefined');
				form.setReadOnly(systemDefined);
				form.setReadOnlyField('definition.label', true);
				form.setReadOnlyField('systemColumn.label', true);
				form.setReadOnlyField('name', true);
			}
			else {
				form.setReadOnlyField('definition.label', hasDefinition);
				form.setReadOnlyField('systemColumn.label', !hasDefinition);
				form.setReadOnlyField('name', !hasDefinition);
				form.setReadOnlyField('systemDefined', false);
			}
			if (hasDefinition) {
				if (hasSystemTable) {
					form.hideField('name');
					form.showField('systemColumn.label');
				}
				else {
					form.showField('name');
					form.hideField('systemColumn.label');
				}
			}
		}
	}]
});
