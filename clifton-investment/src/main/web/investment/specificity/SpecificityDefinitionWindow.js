/**
 * The dialog window for viewing, creating, and modifying Investment Specificity Definition objects.
 */
Clifton.investment.specificity.SpecificityDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Specificity Definition',
	iconCls: 'grouping',
	width: 800,
	height: 400,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Definition',
				items: [{
					xtype: 'formpanel',
					instructions: 'The Specificity Definition can be used to define values for investment security fields based on investment security specificity.',
					url: 'investmentSpecificityDefinition.json',
					items: [
						{fieldLabel: 'Definition Name', name: 'name', readOnly: true},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70, readOnly: true},
						{xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: true},
						{
							fieldLabel: 'System Table', xtype: 'combo',
							name: 'systemTable.label', hiddenName: 'systemTable.id',
							displayField: 'label', url: 'systemTableListFind.json?defaultDataSource=true',
							detailPageClass: 'Clifton.system.schema.TableWindow',
							mutuallyExclusiveFields: ['name'], qtip: 'The System Table for this definition. All fields under this definition must correspond to a column under this System Table, if provided.',
							readOnly: true
						},
						{fieldLabel: 'Entry Bean Class', name: 'entryBeanClassName', qtip: 'The backing class which will be instantiated for entries for this definition.', readOnly: true},

						{boxLabel: 'Allow Investment Instrument to be used in Field Mapping Specificity Scope', xtype: 'checkbox', name: 'instrumentAllowed', disabled: true},
						{boxLabel: 'Allow Currency Denomination to be used in Field Mapping Specificity Scope', xtype: 'checkbox', name: 'currencyDenominationAllowed', disabled: true},
						{boxLabel: 'Allow Issuer Company to be used in Field Mapping Specificity Scope', xtype: 'checkbox', name: 'issuerCompanyAllowed', disabled: true}
					]
				}]
			},


			{
				title: 'Available Fields',
				items: [{
					xtype: 'investment-specificity-fields-grid',
					columnOverrides: [
						{dataIndex: 'definition.label', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.investment.specificity.SpecificityFieldWindow',
						getDefaultData: function(gridPanel, row) {
							return {
								definition: {
									id: gridPanel.getWindow().getMainFormId(),
									label: TCG.getValue('name', gridPanel.getWindow().getMainForm().formValues),
									systemTable: TCG.getValue('systemTable', gridPanel.getWindow().getMainForm().formValues)
								}
							};
						}
					},
					getTopToolbarInitialLoadParams: function() {
						if (typeof this.getWindow().getMainFormId != 'undefined') {
							return {definitionId: this.getWindow().getMainFormId()};
						}
						return {};
					}
				}]
			},


			{
				title: 'Property Definitions',
				items: [{
					name: 'investmentSpecificityEntryPropertyTypeListFind',
					xtype: 'gridpanel',
					instructions: 'The properties which are available to specificity entries for this definition. Each entry for this definition may have a value for any of these properties.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Property Name', width: 100, dataIndex: 'name'},
						{header: 'System Name', width: 80, dataIndex: 'entryBeanPropertyName'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Data Type', width: 45, dataIndex: 'dataType.name'},
						{header: 'Value List URL', width: 80, dataIndex: 'valueListUrl', hidden: true},
						{header: 'Order', width: 30, dataIndex: 'order', type: 'int', useNull: true},
						{header: 'Allow Multiple', width: 30, dataIndex: 'multipleValuesAllowed', type: 'boolean'},
						{header: 'Required', width: 30, dataIndex: 'required', type: 'boolean'}
					],
					getLoadParams: function() {
						return {definitionId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.investment.specificity.SpecificityPropertyTypeWindow'
					}
				}]
			},


			{
				title: 'Entries',
				items: [{
					xtype: 'investment-specificity-entry-grid',
					showSecurityFilter: false,
					showInstrumentFilter: false,
					getDefaultDefinitionId: function() {
						return this.getWindow().getMainFormId();
					},
					getDefaultDefinitionName: function() {
						return TCG.getValue('name', this.getWindow().getMainForm().formValues);
					}
				}]
			}
		]
	}]
});
