/**
 * The dialog window for viewing, creating, and modifying Investment Specificity Property Type objects.
 */
Clifton.investment.specificity.SpecificityPropertyTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Specificity Entry Property Type',
	iconCls: 'grouping',
	height: 500,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'Property types define the available value fields that may be used for any specificity entry under a given specificity definition.',
		url: 'investmentSpecificityEntryPropertyType.json',
		labelWidth: 150,
		items: [
			{fieldLabel: 'Specificity Definition', xtype: 'linkfield', name: 'definition.label', detailIdField: 'definition.id', detailPageClass: 'Clifton.investment.specificity.SpecificityDefinitionWindow', qtip: 'The definition for which this property is defined.'},
			{fieldLabel: 'Property Name', name: 'name'},
			{fieldLabel: 'System Name', name: 'entryBeanPropertyName', qtip: 'The internal field name for the property.', readOnly: true},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
			{fieldLabel: 'Data Type', name: 'dataType.name', qtip: 'The data type for the values which may populate this property.', detailIdField: 'dataType.id', xtype: 'linkfield', detailPageClass: 'Clifton.system.schema.DataTypeWindow'},
			{fieldLabel: 'Value List Table', name: 'valueTable.name', hiddenName: 'valueTable.id', xtype: 'combo', qtip: 'Optional: If specified, the name of the table from which values are retrieved.', url: 'systemTableListFind.json?defaultDataSource=true', detailPageClass: 'Clifton.system.schema.TableWindow', disableAddNewItem: true},
			{fieldLabel: 'Value List URL', name: 'valueListUrl', qtip: 'Optional: If specified, the URL which shall be used to retrieve the available values for this property type.'},
			{fieldLabel: 'UI Config', name: 'userInterfaceConfig', xtype: 'script-textarea', qtip: 'Optional: If specified, the user interface configuration to override the default user interface for this field.', height: 35, grow: true},
			{fieldLabel: 'Order', name: 'order', qtip: 'The order of this property.'},
			{boxLabel: 'Allow multiple values to be selected for this property', name: 'multipleValuesAllowed', xtype: 'checkbox', qtip: 'Enable multiple values for this property.', disabled: true},
			{fieldLabel: 'Required', name: 'required', xtype: 'checkbox', qtip: 'Require a value this property.', disabled: true}
		]
	}]
});
