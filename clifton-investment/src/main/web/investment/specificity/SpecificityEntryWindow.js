/**
 * The dialog window for viewing, creating, and modifying Investment Specificity Entry objects.
 */
Clifton.investment.specificity.SpecificityEntryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Specificity Entry',
	iconCls: 'grouping',
	height: 500,
	// Load the default data after render for copying to work
	loadDefaultDataAfterRender: true,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formwithdynamicfields',
		instructions: 'Specificity entries provide the configuration for a specificity definition/field combination at a given specificity level. To override less-specific entries, select the same definition/field combination with a more specific scope.',
		url: 'investmentSpecificityEntry.json',

		// Dynamic fields form configuration
		dynamicTriggerFieldName: 'field.definition.label',
		dynamicTriggerValueFieldName: 'field.definition.id',
		dynamicFieldTypePropertyName: 'parameter',
		dynamicFieldListPropertyName: 'propertyList',
		dynamicFieldsUrl: 'investmentSpecificityEntryPropertyTypeListFind.json',
		dynamicFieldsUrlParameterName: 'definitionId',

		labelWidth: 150,
		labelFieldName: 'id',
		items: [
			// Field selection
			{xtype: 'sectionheaderfield', header: 'Affected Field'},
			{
				fieldLabel: 'Specificity Definition', xtype: 'combo',
				name: 'field.definition.label', hiddenName: 'field.definition.id',
				displayField: 'label', url: 'investmentSpecificityDefinitionListFind.json', loadAll: true, limitRequestedProperties: false,
				detailPageClass: 'Clifton.investment.specificity.SpecificityDefinitionWindow', disableAddNewItem: true,
				submitValue: false, allowBlank: false,
				qtip: 'The Definition defining the behavior and the available configuration fields for this mapping.',
				listeners: {
					// Reload dynamic fields on change
					select: function(combo, record, index) {
						combo.getParentForm().resetDynamicFields(combo, false);
					}
				}
			},
			{
				fieldLabel: 'Specificity Field', xtype: 'combo',
				name: 'field.label', hiddenName: 'field.id',
				displayField: 'label', url: 'investmentSpecificityFieldListFind.json', loadAll: true,
				detailPageClass: 'Clifton.investment.specificity.SpecificityFieldWindow', disableAddNewItem: true,
				requiredFields: ['field.definition.id'],
				qtip: 'The specificity field to which this entry will be applied. When determining the most specific entries for an entity, one entry may be returned for each specificity field under each definition.',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const form = combo.getParentForm().getForm();
						const definitionId = form.findField('field.definition.id').getValue();
						combo.store.setBaseParam('definitionId', definitionId);
					}
				}
			},

			// Specificity properties
			{xtype: 'sectionheaderfield', header: 'Specificity Scope'},
			{
				fieldLabel: 'Currency Denomination', xtype: 'combo',
				name: 'currencyDenomination.label', hiddenName: 'currencyDenomination.id',
				displayField: 'label', url: 'investmentSecurityListFind.json?currency=true', loadAll: true,
				detailPageClass: 'Clifton.investment.instrument.SecurityWindow', disableAddNewItem: true,
				qtip: 'The Currency Denomination for securities to which this mapping shall be applied.',
				hidden: true
			},
			{
				fieldLabel: 'Issuer Company', xtype: 'combo',
				name: 'issuerCompany.label', hiddenName: 'issuerCompany.id',
				displayField: 'label', url: 'businessCompanyListFind.json?issuer=true', loadAll: true,
				detailPageClass: 'Clifton.business.company.CompanyWindow',
				qtip: 'The Issuer Company for securities to which this mapping shall be applied.',
				hidden: true
			},
			{
				fieldLabel: 'Investment Type', xtype: 'combo',
				name: 'investmentType.name', hiddenName: 'investmentType.id',
				displayField: 'name', url: 'investmentTypeList.json', loadAll: true,
				detailPageClass: 'Clifton.investment.setup.InvestmentTypeWindow', disableAddNewItem: true,
				mutuallyExclusiveFields: ['hierarchy.id', 'instrument.id'],
				qtip: 'The Investment Type for securities to which this mapping shall be applied.'
			},
			{
				fieldLabel: 'Investment Sub Type', xtype: 'combo',
				name: 'investmentTypeSubType.name', hiddenName: 'investmentTypeSubType.id',
				displayField: 'name', url: 'investmentTypeSubTypeListByType.json', loadAll: true,
				detailPageClass: 'Clifton.investment.setup.InvestmentTypeSubTypeWindow', disableAddNewItem: true,
				requiredFields: ['investmentType.name'],
				qtip: 'The Investment Sub Type for securities to which this mapping shall be applied.',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const form = combo.getParentForm().getForm();
						const investmentTypeId = form.findField('investmentType.id').getValue();
						combo.store.setBaseParam('investmentTypeId', investmentTypeId);
					}
				}
			},
			{
				fieldLabel: 'Investment Sub Type 2', xtype: 'combo',
				name: 'investmentTypeSubType2.name', hiddenName: 'investmentTypeSubType2.id',
				displayField: 'name', url: 'investmentTypeSubType2ListByType.json', loadAll: true,
				detailPageClass: 'Clifton.investment.setup.InvestmentTypeSubType2Window', disableAddNewItem: true,
				requiredFields: ['investmentType.id'],
				qtip: 'The Investment Sub Type 2 for securities to which this mapping shall be applied.',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const form = combo.getParentForm().getForm();
						const investmentTypeId = form.findField('investmentType.id').getValue();
						combo.store.setBaseParam('investmentTypeId', investmentTypeId);
					}
				}
			},
			{
				fieldLabel: 'Investment Hierarchy', xtype: 'combo',
				name: 'hierarchy.labelExpanded', hiddenName: 'hierarchy.id',
				displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true',
				queryParam: 'labelExpanded', listWidth: 600,
				detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow',
				sortInfo: {orderBy: 'labelExpanded'},
				mutuallyExclusiveFields: ['investmentType.id', 'instrument.id'],
				qtip: 'The Investment Hierarchy for securities to which this mapping shall be applied.',
				listeners: {
					select: function(combo, record, index) {
						combo.getParentForm().getForm().findField('instrument.id').clearAndReset();
					}
				}
			},
			{
				fieldLabel: 'Investment Instrument', xtype: 'combo',
				name: 'instrument.label', hiddenName: 'instrument.id',
				displayField: 'label', url: 'investmentInstrumentListFind.json',
				detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', disableAddNewItem: true,
				mutuallyExclusiveFields: ['investmentType.id', 'hierarchy.id'],
				qtip: 'The Investment Instrument for securities to which this mapping shall be applied.',
				hidden: true,
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const form = combo.getParentForm().getForm();
						const hierarchyId = form.findField('hierarchy.id').getValue();
						combo.store.setBaseParam('hierarchyId', hierarchyId);
					}
				}
			},

			// Entry properties (auto-populated via dynamic fields form)
			{xtype: 'sectionheaderfield', header: 'Entry Configuration'}
		],

		listeners: {
			afterload: function(panel, closeOnSuccess) {
				// Guard-clause: Skip UI modifications if the window is about to be closed
				if (closeOnSuccess) {
					return;
				}

				// Guard-clause: Do not perform link-field replacement if this is not a loaded entity
				if (TCG.isNull(this.getIdFieldValue())) {
					return;
				}

				// Replace definition combo-field with link-field if this is a loaded entity
				const formValues = this.getForm().formValues;
				const definitionComboField = this.getForm().findField(this.dynamicTriggerFieldName);
				const definitionLinkField = Ext.apply(definitionComboField.initialConfig, {
					xtype: 'linkfield',
					detailIdField: definitionComboField.initialConfig.hiddenName,
					value: TCG.getValue('field.definition.label', formValues)
				});

				this.remove(definitionComboField, true);
				this.insert(2, definitionLinkField);
				this.doLayout();
			}
		},

		/**
		 * Applies UI overrides for dynamic fields. This overrides the standard function to include modifications to the hierarchy fields, such as instrument
		 * and currency denomination.
		 */
		resetDynamicFields: function(triggerField, cancelIfLoaded, keepCurrentFieldValues) {
			// Guard-clauses
			if ((triggerField && !triggerField.isValid()) || !this.getDynamicFields) {
				return;
			}
			if (cancelIfLoaded && this.dynamicFieldsLoaded) {
				return;
			}

			const form = this.getForm();
			let definitionRecord;
			let definition;

			// Determine definition from combo-box record or from loaded form values
			if (triggerField.xtype === 'combo') {
				definitionRecord = triggerField.findRecord('id', triggerField.getValue());
				definition = definitionRecord && definitionRecord.json;
			}
			// When copying an entry, the definition cannot be retrieved from the combo record.
			// Thus, always set the definition to the form value if it is not defined.
			if (TCG.isNull(definition)) {
				definition = TCG.getValue('field.definition', form.formValues);
			}

			// Set custom dynamic fields
			applyFieldVisibility('instrument.id', definition.instrumentAllowed);
			applyFieldVisibility('issuerCompany.id', definition.issuerCompanyAllowed);
			applyFieldVisibility('currencyDenomination.id', definition.currencyDenominationAllowed);
			TCG.callOverridden(this, 'resetDynamicFields', arguments);

			// Configures field visibility and submission
			function applyFieldVisibility(fieldName, enabled) {
				const field = form.findField(fieldName);
				field.setVisible(enabled);
				const fieldElement = field.hiddenField || TCG.getValue('el.dom', field);
				if (fieldElement) {
					fieldElement.doNotSubmit = !enabled;
				}
				else {
					field.doNotSubmitValue = !enabled;
				}
			}
		}
	}]
});
