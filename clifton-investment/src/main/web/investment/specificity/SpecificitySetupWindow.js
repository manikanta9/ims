/**
 * The setup window for viewing, creating, and modifying Investment Specificity definitions, fields, and entries.
 */
Clifton.investment.specificity.SpecificitySetupWindow = Ext.extend(TCG.app.Window, {
	id: 'specificitySetupWindow',
	title: 'Investment Specificity Setup',
	iconCls: 'grouping',
	width: 1500,
	height: 700,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Definitions',
				items: [{
					name: 'investmentSpecificityDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'The Specificity Definitions can be used to define values for investment security fields based on investment security specificity.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 15, hidden: true},
						{header: 'Definition Name', dataIndex: 'name', width: 80},
						{header: 'Description', dataIndex: 'description', width: 150, hidden: true},
						{header: 'Modify Condition', width: 60, dataIndex: 'entityModifyCondition.name', filter: {type: 'combo', url: 'systemConditionListFind.json', searchFieldName: 'entityModifyConditionId'}},
						{header: 'System Table', dataIndex: 'systemTable.name', width: 40, filter: {type: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', searchFieldName: 'systemTableId'}},
						{header: 'Entry Bean Class', dataIndex: 'entryBeanClassName', width: 100},
						{header: 'Investment Instrument Allowed', dataIndex: 'instrumentAllowed', width: 45, type: 'boolean', hidden: true},
						{header: 'Currency Denomination Allowed', dataIndex: 'currencyDenominationAllowed', width: 45, type: 'boolean', hidden: true},
						{header: 'Issuer Company Allowed', dataIndex: 'issuerCompanyAllowed', width: 45, type: 'boolean', hidden: true}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.investment.specificity.SpecificityDefinitionWindow'
					}
				}]
			},


			{
				title: 'Available Fields',
				items: [{
					xtype: 'investment-specificity-fields-grid',
					additionalPropertiesToRequest: 'definition.id|definition.systemTable.id',
					editor: {
						detailPageClass: 'Clifton.investment.specificity.SpecificityFieldWindow',
						getDefaultData: function(gridPanel, row) {
							if (row) {
								let table;
								if (row.json.definition.hasOwnProperty('systemTable') && row.json.definition.systemTable.hasOwnProperty('id')) {
									table = {
										id: row.json.definition.systemTable.id
									};
								}
								return {
									definition: {
										id: row.json.definition.id,
										label: row.json.definition.label,
										systemTable: table
									}
								};
							}
							return {};
						}
					}
				}]
			},


			{
				title: 'Entries',
				items: [{
					xtype: 'investment-specificity-entry-grid'
				}]
			}]
	}]
});
