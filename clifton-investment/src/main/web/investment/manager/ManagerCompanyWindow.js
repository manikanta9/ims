TCG.use('Clifton.business.company.BasicCompanyWindow');

// adds "Manager Accounts" tab at the end of BasicCompanyWindow
Clifton.investment.manager.ManagerCompanyWindow = Ext.extend(Clifton.business.company.BasicCompanyWindow, {
	windowOnShow: function(w) {
		Clifton.investment.manager.ManagerCompanyWindow.superclass.windowOnShow.apply(this, arguments);
		const tabs = w.items.get(0).items.get(0);
		tabs.add(this.managerAccountsTab);
	},

	managerAccountsTab: {
		title: 'Manager Accounts',
		items: [{
			xtype: 'gridpanel',
			name: 'investmentManagerAccountListFind',
			instructions: 'A list of money manager accounts for this manager.',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Client', width: 100, dataIndex: 'client.name', filter: {type: 'combo', searchFieldName: 'clientId', url: 'businessClientListFind.json'}},
				{header: 'Account', width: 70, dataIndex: 'labelShort', filter: {searchFieldName: 'accountNameNumber'}},
				{header: 'Custodial Account', width: 100, dataIndex: 'custodianLabel', filter: {type: 'combo', searchFieldName: 'custodianCompanyId', displayField: 'label', url: 'businessCompanyListFind.json?companyType=Custodian'}},
				{header: 'Type', dataIndex: 'managerType', width: 40, type: 'boolean'},
				{header: 'Proxy Date', dataIndex: 'proxyValueDate', width: 50, hidden: true},
				{header: 'Proxy Value', dataIndex: 'proxyValue', width: 60, type: 'currency', filter: false, sortable: false, hidden: true},
				{header: 'Inactive', dataIndex: 'inactive', width: 40, type: 'boolean'},
				{header: 'Base CCY', width: 50, hidden: true, dataIndex: 'baseCurrency.symbol', filter: {type: 'combo', searchFieldName: 'baseCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}}
			],
			editor: {
				detailPageClass: 'Clifton.investment.manager.AccountWindow',
				drillDownOnly: true
			},
			getLoadParams: function(firstLoad) {
				if (firstLoad) {
					// default to active
					this.setFilterValue('inactive', false);
				}
				return {managerCompanyId: this.getWindow().getMainFormId()};
			}
		}]
	}
});
