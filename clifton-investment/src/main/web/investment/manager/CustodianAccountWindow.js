Clifton.investment.manager.CustodianAccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Manager Custodian Account',
	iconCls: 'manager',
	width: 1000,
	height: 500,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Custodian Account',
				items: [{
					xtype: 'formpanel',
					url: 'investmentManagerCustodianAccount.json',
					instructions: 'Custodian accounts are unique based on Account Number and Issuing Company. They may be reused across multiple Manager Accounts. The Holding Account Number is Parametric\'s internal Custodian Account Number.',
					items: [
						{fieldLabel: 'Custodian Number', name: 'number'},
						{
							fieldLabel: 'Issued By',
							name: 'company.name',
							hiddenName: 'company.id',
							xtype: 'combo',
							url: 'businessCompanyListFind.json?companyTypeNames=Custodian,Investment Manager,Broker,Other Issuer',
							detailPageClass: 'Clifton.business.company.CompanyWindow',
							disableAddNewItem: true,
							listeners: {
								beforeselect: function(combo, record, index) {
									const fp = combo.getParentForm();
									fp.getForm().findField('holdingAccount.id').clearAndReset();
								}
							}
						},
						{
							fieldLabel: 'Base Currency',
							name: 'baseCurrency.label',
							xtype: 'combo',
							hiddenName: 'baseCurrency.id',
							displayField: 'label',
							url: 'investmentSecurityListFind.json?currency=true'
						},
						{
							fieldLabel: 'Holding Account',
							name: 'holdingAccount.label',
							hiddenName: 'holdingAccount.id',
							xtype: 'combo',
							url: 'investmentAccountListFind.json',
							displayField: 'label',
							detailPageClass: 'Clifton.investment.account.AccountWindow',
							requiredFields: ['company.name'],
							disableAddNewItem: true,
							beforequery: function(queryEvent) {
								const f = queryEvent.combo.getParentForm().getForm();
								queryEvent.combo.store.baseParams = {
									issuingCompanyIdOrRelatedIssuingCompanyId: f.findField('company.id').getValue()
								};
							}
						},
						{name: 'custodyOnlyAccount', xtype: 'checkbox', boxLabel: 'Custody Only vs Full Service Account (does not include assets from other accounts)'},
						{name: 'sweepAccount', xtype: 'checkbox', boxLabel: 'Sweep Account', qtip: 'A sweep account is a bank account that automatically transfers amounts that exceed, or fall short of, a certain level into a higher interest-earning investment option at the close of each business day. Commonly, the excess cash is swept into money market funds.'}
					]
				}]
			},


			{
				title: 'Manager Accounts',
				items: [{
					xtype: 'gridpanel',
					name: 'investmentManagerAccountListFind.json',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Manager', width: 100, dataIndex: 'managerCompany.name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Client', width: 100, dataIndex: 'client.name', filter: {type: 'combo', searchFieldName: 'clientId', url: 'businessClientListFind.json'}},
						{header: 'Account', width: 50, dataIndex: 'labelShort', filter: {searchFieldName: 'accountNameNumber'}},
						{
							header: 'Manager Type', dataIndex: 'managerType', width: 60,
							filter: {
								type: 'combo', searchFieldName: 'managerType', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.investment.manager.ManagerTypes
								})
							},
							renderer: function(v, p, r) {
								if (v === 'LINKED') {
									let qtip = '<table><tr><td>' + r.data['linkedManagerTypeLabel'] + '</td></tr>';
									if (r.data['linkedManagerProcessingDependent'] === true) {
										qtip += '<tr><td>* Portfolio Processing Dependent</td></tr>';
									}
									qtip += '</table>';
									p.attr = 'qtip=\'' + qtip + '\'';
									p.css = 'amountAdjusted';
								}
								return v;
							}
						},
						{header: 'Linked - Portfolio dependency', dataIndex: 'linkedManagerProcessingDependent', type: 'boolean', hidden: true, sortable: false, filter: false},
						{
							header: 'Linked Type', hidden: true, dataIndex: 'linkedManagerTypeLabel', width: 40,
							filter: {
								type: 'combo', searchFieldName: 'linkedManagerType', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.investment.manager.LinkedManagerTypes
								})
							}
						},
						{
							header: 'Cash Adj Type', dataIndex: 'cashAdjustmentType', width: 50, tooltip: 'Cash Adjustment Type',
							filter: {
								type: 'combo', searchFieldName: 'cashAdjustmentType', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.investment.manager.ManagerCashAdjustmentTypes
								})
							}
						},
						{header: 'Benchmark', width: 100, hidden: true, dataIndex: 'benchmarkSecurity.label', filter: {searchFieldName: 'benchmarkSecurityLabel'}},
						{header: 'Base CCY', width: 50, hidden: true, dataIndex: 'baseCurrency.symbol', filter: {type: 'combo', searchFieldName: 'baseCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Inactive', dataIndex: 'inactive', width: 35, type: 'boolean'},
						{header: 'Active Client', dataIndex: 'client.active', width: 50, type: 'boolean', filter: {searchFieldName: 'clientActive'}, sortable: false}
					],
					getLoadParams: function(firstLoad) {
						const f = this.getWindow().getMainForm();
						const custodianAccountId = f.formValues.id;
						if (custodianAccountId) {
							return {
								custodianAccountId: custodianAccountId
							};
						}
						else {
							this.grid.store.removeAll();
							return false;
						}
					},
					editor: {
						addEnabled: false,
						deleteEnabled: false,
						detailPageClass: 'Clifton.investment.manager.AccountWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Balance History',
				items: [{
					name: 'investmentManagerAccountBalanceListFind',
					xtype: 'gridpanel',
					groupField: 'managerAccount.labelShort',
					instructions: 'Securities and cash balance history for this custodian\'s Manager Accounts.' +
						'<ul class="c-list">' +
						'<li>Adjusted balances are displayed in red and include all system and user defined adjustments that are not MOC adjustments.</li>' +
						'<li>If MOC (Market On Close) column is checked there are MOC adjustments that result in a different balance than the adjusted balance.  MOC is used when clients make a significant change (i.e. get/remove a lot of cash or move cash between managers) on a specific date and they want to change exposure right before the market closes to account for this.  There will be 2 or so day delay before we see this in manager download, so we use MOC adjustments to account for these changes.</li>' +
						'</ul>',
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 30 days of balances
							this.setFilterValue('balanceDate', {'after': new Date().add(Date.DAY, -30)});
						}
						const f = this.getWindow().getMainForm();
						const custodianAccountId = f.formValues.id;
						if (custodianAccountId) {
							return {
								custodianAccountId: custodianAccountId
							};
						}
						else {
							this.grid.store.removeAll();
							return false;
						}
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Date', width: 70, dataIndex: 'balanceDate'},
						{header: 'Manager Account', width: 150, dataIndex: 'managerAccount.labelShort', filter: {type: 'combo', searchFieldName: 'searchPattern', displayField: 'label', url: 'investmentManagerAccountListFind.json'}},
						{header: 'Note', width: 180, dataIndex: 'note', hidden: true},

						// Boolean Flags for if the Balance was adjusted
						{header: 'Cash Adjusted', dataIndex: 'cashAdjustment', type: 'boolean', hidden: true, filter: false},
						{header: 'Securities Adjusted', dataIndex: 'securitiesAdjustment', type: 'boolean', hidden: true, filter: false},

						// Original Balance Info
						{header: 'Original Cash', dataIndex: 'cashValue', hidden: true, type: 'currency'},
						{header: 'Original Securities', dataIndex: 'securitiesValue', hidden: true, type: 'currency'},
						{header: 'Original Total', dataIndex: 'totalValue', hidden: true, type: 'currency'},

						// Adjusted Balance Info
						{
							header: 'Cash', width: 100, dataIndex: 'adjustedCashValue', type: 'currency',
							renderer: function(v, p, r) {
								const value = Ext.util.Format.number(v, '0,000.00');
								if (r.data['cashAdjustment'] === true) {
									return '<div style="COLOR: #ff0000;" qtip=\'Original value: ' + Ext.util.Format.number(r.data['cashValue'], '0,000.00') + '<br/>Adjustment: ' + Ext.util.Format.number(r.data['adjustedCashValue'] - r.data['cashValue'], '0,000.00') + '\'>' + value + '</div>';
								}
								return value;
							}
						},
						{
							header: 'Securities', width: 100, dataIndex: 'adjustedSecuritiesValue', type: 'currency',
							renderer: function(v, p, r) {
								const value = Ext.util.Format.number(v, '0,000.00');
								if (r.data['securitiesAdjustment'] === true) {
									return '<div style="COLOR: #ff0000;" qtip=\'Original value: ' + Ext.util.Format.number(r.data['securitiesValue'], '0,000.00') + '<br/>Adjustment: ' + Ext.util.Format.number(r.data['adjustedSecuritiesValue'] - r.data['securitiesValue'], '0,000.00') + '\'>' + value + '</div>';
								}
								return value;
							}
						},
						{
							header: 'Total', width: 100, dataIndex: 'adjustedTotalValue', type: 'currency',
							renderer: function(v, p, r) {
								const value = Ext.util.Format.number(v, '0,000.00');
								if (r.data['cashAdjustment'] === true || r.data['securitiesAdjustment'] === true) {
									return '<div style="COLOR: #ff0000;" qtip=\'Original value: ' + Ext.util.Format.number(r.data['totalValue'], '0,000.00') + '<br/>Adjustment: ' + Ext.util.Format.number(r.data['adjustedTotalValue'] - r.data['totalValue'], '0,000.00') + '\'>' + value + '</div>';
								}
								return value;
							}
						},

						// MOC Balance Info
						{header: 'MOC', width: 35, dataIndex: 'mocAdjustment', type: 'boolean'},
						{
							header: 'MOC Cash', width: 100, dataIndex: 'marketOnCloseCashValue', type: 'currency', hidden: true,
							renderer: function(v, p, r) {
								const value = Ext.util.Format.number(v, '0,000.00');
								if (r.data['mocAdjustment'] === true) {
									return '<div style="COLOR: #ff0000;" qtip=\'Adjusted value: ' + Ext.util.Format.number(r.data['adjustedCashValue'], '0,000.00') + '<br/>MOC Adjustment: ' + Ext.util.Format.number(r.data['marketOnCloseCashValue'] - r.data['adjustedCashValue'], '0,000.00') + '\'>' + value + '</div>';
								}
								return value;
							}
						},
						{
							header: 'MOC Securities', width: 100, dataIndex: 'marketOnCloseSecuritiesValue', type: 'currency', hidden: true,
							renderer: function(v, p, r) {
								const value = Ext.util.Format.number(v, '0,000.00');
								if (r.data['mocAdjustment'] === true) {
									return '<div style="COLOR: #ff0000;" qtip=\'Adjusted value: ' + Ext.util.Format.number(r.data['adjustedSecuritiesValue'], '0,000.00') + '<br/>MOC Adjustment: ' + Ext.util.Format.number(r.data['marketOnCloseSecuritiesValue'] - r.data['adjustedSecuritiesValue'], '0,000.00') + '\'>' + value + '</div>';
								}
								return value;
							}
						},
						{
							header: 'MOC Total', width: 100, dataIndex: 'marketOnCloseTotalValue', type: 'currency',
							renderer: function(v, p, r) {
								const value = Ext.util.Format.number(v, '0,000.00');
								if (r.data['mocAdjustment'] === true) {
									return '<div style="COLOR: #ff0000;" qtip=\'Adjusted value: ' + Ext.util.Format.number(r.data['adjustedTotalValue'], '0,000.00') + '<br/>MOC Adjustment: ' + Ext.util.Format.number(r.data['marketOnCloseTotalValue'] - r.data['adjustedTotalValue'], '0,000.00') + '\'>' + value + '</div>';
								}
								return value;
							}
						}
					],
					editor: {
						detailPageClass: 'Clifton.investment.manager.BalanceWindow',
						getDefaultData: function(gridPanel) { // defaults manager account for the detail page
							return {
								managerAccount: gridPanel.getWindow().getMainForm().formValues
							};
						},
						deleteEnabled: false,
						addEnabled: false
					}
				}]
			},


			{
				title: 'Transaction History',
				items: [{
					xtype: 'investment-manager-custodian-transaction-grid',
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 7 days of balances
							this.setFilterValue('transactionDate', {'after': new Date().add(Date.DAY, -7)});
						}

						const custodianAccountId = this.getWindow().getMainFormId();
						if (custodianAccountId) {
							return {
								custodianAccountId: custodianAccountId
							};
						}
						else {
							this.grid.store.removeAll();
							return false;
						}
					}
				}]
			}
		]
	}]
});
