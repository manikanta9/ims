Clifton.investment.manager.upload.AccountAllocationUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'investmentManagerAccountAllocationUploadWindow',
	title: 'Manager Allocation Updates - Upload Window',
	iconCls: 'import',
	height: 500,
	width: 700,
	hideOKButton: true,

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File',
		handler: function() {
			TCG.openFile('investment/manager/upload/CustomManagerAllocationUploadSampleFile.xls');
		}
	}],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Manager Allocation Updates Upload allows using a customized file format to easily upload new Weights and manager allocations for existing manager assignments.',

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{
				fieldLabel: '', boxLabel: 'Allow Partially Allocated Managers (Leave unchecked to enforce allocation total of 100%)', name: 'allowPartiallyAllocatedManagers', xtype: 'checkbox',
				qtip: 'If unchecked, the end result of each manager assignment\'s allocations must equal 100%.'
			},
			{
				fieldLabel: '', boxLabel: 'Allow Adding New and/or Removing Existing Allocations', name: 'allowAddingOrRemovingAllocations', xtype: 'checkbox',
				qtip: 'Leave unchecked to only allow updating existing allocations, and any existing allocations not included in the file will remain on the assignment.  If checked, the final allocations for the manager will be based on the file, and any new allocations can be added, and existing ones removed (if missing in the file) or updated (if included in the file).'
			},
			{xtype: 'sectionheaderfield', header: 'If invalid data is encountered'},
			{
				xtype: 'radiogroup', columns: 1,
				items: [
					{boxLabel: 'Fail this upload and don\'t load any data.', name: 'partialUploadAllowed', inputValue: 'false', checked: true},
					{boxLabel: 'Skip rows with invalid data but upload valid rows.', name: 'partialUploadAllowed', inputValue: 'true'}
				]
			},
			{xtype: 'sectionheaderfield', header: 'Upload Results'},
			{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false, height: 150}
		],

		getSaveURL: function() {
			return 'investmentManagerAccountAllocationUploadFileUpload.json';
		}
	}]
});
