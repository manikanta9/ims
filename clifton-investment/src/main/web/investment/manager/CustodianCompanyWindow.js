TCG.use('Clifton.business.company.BasicCompanyWindow');

// adds "Manager Accounts" tab at the end of BasicCompanyWindow
Clifton.investment.manager.CustodianCompanyWindow = Ext.extend(Clifton.business.company.BasicCompanyWindow, {
	windowOnShow: function(w) {
		Clifton.investment.manager.CustodianCompanyWindow.superclass.windowOnShow.apply(this, arguments);
		const tabs = w.items.get(0).items.get(0);
		tabs.add(this.managerAccountsTab);
		tabs.add(this.deliveryInstructionsTab);
	},

	managerAccountsTab: {
		title: 'Manager Accounts',
		items: [{
			xtype: 'gridpanel',
			name: 'investmentManagerAccountListFind',
			instructions: 'A list of money manager accounts reported by selected custodian.',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Client', width: 100, dataIndex: 'client.name', filter: {type: 'combo', searchFieldName: 'clientId', url: 'businessClientListFind.json'}},
				{header: 'Manager', width: 100, dataIndex: 'managerCompany.name', filter: {searchFieldName: 'searchPattern'}},
				{header: 'Account', width: 70, dataIndex: 'labelShort', filter: {searchFieldName: 'accountNameNumber'}},
				{header: 'Custodial Account', width: 100, dataIndex: 'custodianLabel', filter: {type: 'combo', searchFieldName: 'custodianCompanyId', displayField: 'label', url: 'businessCompanyListFind.json?companyType=Custodian'}},
				{header: 'Type', dataIndex: 'managerType', width: 40},
				{header: 'Proxy Date', dataIndex: 'proxyValueDate', width: 50, hidden: true},
				{header: 'Proxy Value', dataIndex: 'proxyValue', width: 60, type: 'currency', filter: false, sortable: false, hidden: true},
				{header: 'Inactive', dataIndex: 'inactive', width: 40, type: 'boolean'},
				{header: 'Base CCY', width: 50, hidden: true, dataIndex: 'baseCurrency.symbol', filter: {type: 'combo', searchFieldName: 'baseCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}}
			],
			editor: {
				detailPageClass: 'Clifton.investment.manager.AccountWindow',
				drillDownOnly: true
			},
			getLoadParams: function(firstLoad) {
				if (firstLoad) {
					// default to active
					this.setFilterValue('inactive', false);
				}
				return {custodianCompanyId: this.getWindow().getMainFormId()};
			}
		}]
	},


	deliveryInstructionsTab: {
		title: 'Delivery Instructions',
		items: [{
			xtype: 'gridpanel',
			appendStandardColumns: false,
			name: 'investmentInstructionDeliveryBusinessCompanyListFind',
			instructions: 'Delivery instructions for wire transfers for each bank account used by this custodian.',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'referenceOne.id', hidden: true},
				{header: 'Instruction Name', width: 100, dataIndex: 'referenceOne.name'},
				{header: 'Description', width: 100, dataIndex: 'referenceOne.description', hidden: true},
				{header: 'Bank', width: 80, dataIndex: 'referenceOne.deliveryCompany.name', filter: {type: 'combo', searchFieldName: 'deliveryCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
				{header: 'Currency', width: 30, dataIndex: 'deliveryCurrency.symbol', filter: {type: 'combo', searchFieldName: 'deliveryCurrencyId', url: 'investmentSecurityListFind.json?currency=true'}},
				{header: 'Delivery Type', width: 70, dataIndex: 'investmentInstructionDeliveryType.name', filter: {type: 'combo', searchFieldName: 'typeId', loadAll: true, url: 'investmentInstructionDeliveryTypeListFind.json'}},
				{header: 'Account Number', width: 60, dataIndex: 'referenceOne.deliveryAccountNumber'},
				{header: 'Account Name', width: 100, dataIndex: 'referenceOne.deliveryAccountName', hidden: true},
				{header: 'ABA', width: 50, dataIndex: 'referenceOne.deliveryABA'}
			],
			editor: {
				detailPageClass: 'Clifton.investment.instruction.delivery.InstructionInstructionDeliveryBusinessCompanyWindow',
				getDefaultData: function(gridPanel) {
					return {
						referenceTwo: gridPanel.getWindow().getMainForm().formValues
					};
				}
			},
			getLoadParams: function(firstLoad) {
				return {businessCompanyId: this.getWindow().getMainFormId()};
			}
		}]
	}
});
