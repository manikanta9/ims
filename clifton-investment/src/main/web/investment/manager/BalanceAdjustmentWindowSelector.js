// not the actual window but looks up the balance for the adjustment and opens that window
Clifton.investment.manager.BalanceAdjustmentWindowSelector = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'investmentManagerAccountBalanceAdjustment.json',

	openEntityWindow: function(config, entity) {
		const className = 'Clifton.investment.manager.BalanceWindow';
		if (entity) {
			config.params.id = entity.managerAccountBalance.id;
			entity = TCG.data.getData('investmentManagerAccountBalance.json?id=' + entity.managerAccountBalance.id, this);
		}
		this.doOpenEntityWindow(config, entity, className);
	}
});
