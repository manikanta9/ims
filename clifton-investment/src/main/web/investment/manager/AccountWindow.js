Clifton.investment.manager.AccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Manager Account',
	iconCls: 'manager',
	height: 600,
	width: 850,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Account Details',
				items: [{
					xtype: 'formpanel-custom-fields',
					columnGroupName: 'Linked Manager',
					dynamicFieldFormFragment: 'linkedManagerCustomFields',
					url: 'investmentManagerAccount.json',
					labelWidth: 135,

					listeners: {
						afterload: function(panel) {
							this.setResetCashAllocationFields();
						}
					},

					setResetCashAllocationFields: function() {
						// If custom is selected, enable and required allocation fields, else reset & disable
						const f = this.getForm();
						if (TCG.isNull(f.findField('cashAdjustmentType').getValue())) {
							return;
						}
						const adjustmentType = f.findField('cashAdjustmentType').getValue().getGroupValue();
						const customCash = f.findField('customCashValue');
						const updateDate = f.findField('customCashValueUpdateDate');
						const managerCash = f.findField('cashPercentManager.id');
						if (adjustmentType === 'CUSTOM_VALUE' || adjustmentType === 'CUSTOM_PERCENT') {
							customCash.allowBlank = false;
							customCash.setDisabled(false);

							if (adjustmentType === 'CUSTOM_PERCENT') {
								customCash.minValue = 0;
								customCash.maxValue = 100;
							}
							else {
								customCash.minValue = null;
								customCash.maxValue = null;
							}

							updateDate.allowBlank = false;
							updateDate.setDisabled(false);
						}
						else {
							customCash.allowBlank = true;
							if (TCG.isNotBlank(customCash.getValue())) {
								customCash.setValue('');
							}
							customCash.setDisabled(true);

							updateDate.allowBlank = true;
							if (TCG.isNotBlank(updateDate.getValue())) {
								updateDate.setValue('');
							}
							updateDate.setDisabled(true);
						}
						if (adjustmentType === 'MANAGER') {
							managerCash.allowBlank = false;
							managerCash.setDisabled(false);
						}
						else {
							if (TCG.isNotBlank(managerCash.getValue())) {
								managerCash.reset();
								managerCash.clearValue();
							}
							managerCash.allowBlank = true;
							managerCash.setDisabled(true);
						}
					},

					getDefaultData: function(win) {
						const dd = win.defaultData || {};
						if (win.defaultDataIsReal) {
							return dd;
						}
						dd.baseCurrency = TCG.data.getData('investmentSecurityBySymbol.json?symbol=USD&currency=true', this, 'investment.security.usd');
						return dd;
					},


					items: [
						{fieldLabel: 'Client', name: 'client.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailIdField: 'client.id'},
						{
							fieldLabel: 'Manager Company', name: 'managerCompany.name', hiddenName: 'managerCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow',
							beforequery: function(queryEvent) {
								queryEvent.combo.store.setBaseParam('moneyManager', true);
							},
							getDefaultData: function(f) { // defaults company type for "add new" drill down
								return {type: TCG.data.getData('businessCompanyTypeByName.json?name=Investment Manager', this, 'business.getCompanyTypeInvestmentManager')};
							}

						},
						{
							fieldLabel: 'Manager Number', xtype: 'compositefield', qtip: 'Manager account numbers must be unique.  If you chose to enter an account number it must be exactly 6 digits long.  Leave the account number blank to have the system generate a unique account number for you.',
							items: [
								{name: 'accountNumber', xtype: 'textfield', flex: 1},
								{value: 'Account Name:', xtype: 'displayfield', width: 100},
								{name: 'accountName', xtype: 'textfield', flex: 2}
							]
						},
						{
							fieldLabel: 'Benchmark Security', name: 'benchmarkSecurity.label', hiddenName: 'benchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
							qtip: 'Used for Reporting Purposes<br />'
								+ '<ul><li>Overlay Managers (Optional)</li>'
								+ '<li>Linked Managers for Duration Adjusted market value use the duration from this security.</li>'
								+ '<li>LDI & Duration Management (Required)</li></ul>'
						},
						{fieldLabel: 'Duration Field Override', name: 'durationFieldNameOverride', xtype: 'system-list-combo', listName: 'Portfolio: Duration Field', qtip: 'Optional duration field name override to use for the Benchmark Duration. If blank, \'Duration\' is used.'},
						{
							fieldLabel: 'Base CCY', name: 'baseCurrency.label', hiddenName: 'baseCurrency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', displayField: 'label',
							qtip: 'Base CCY of the Manager Account.  Usually the same as the Client account that uses the manager, but can be different and converting during account processing.'
						},
						{boxLabel: 'Inactive', xtype: 'checkbox', name: 'inactive'},

						{
							xtype: 'fieldset-checkbox',
							title: 'Custodian Info',
							checkboxName: 'custodian',
							instructions: 'Custodial bank account is used to track and report manager positions and performance.',
							labelWidth: 110,
							items: [
								{
									fieldLabel: 'Custodian Account',
									name: 'custodianAccount.label',
									xtype: 'combo',
									hiddenName: 'custodianAccount.id',
									url: 'investmentManagerCustodianAccountListFind.json',
									displayField: 'label',
									detailPageClass: 'Clifton.investment.manager.CustodianAccountWindow',
									getDefaultData: function(f) {
										return {
											baseCurrency: TCG.getValue('baseCurrency', f.formValues)
										};
									}
								},
								{boxLabel: 'Create 0 balance history if no assets are present in download', name: 'zeroBalanceIfNoDownload', xtype: 'checkbox', requiredFields: ['custodianAccount.label'], doNotClearIfRequiredChanges: true}
							]
						},

						{
							xtype: 'fieldset-checkbox',
							title: 'Proxy/Rollup/Linked Manager Setup',
							checkboxName: 'managerTypeSelected',
							instructions: 'Select one of the following manager types.  Based upon your selection, additional fields may be available to further classify the manager setup.',
							defaults: {anchor: '0'},
							items: [
								{
									xtype: 'radiogroup', columns: 4, hideLabel: true, name: 'managerType-Group',
									items: [
										{boxLabel: 'Standard', name: 'managerType', inputValue: 'STANDARD', checked: true},
										{boxLabel: 'Proxy', name: 'managerType', inputValue: 'PROXY'},
										{boxLabel: 'Rollup', name: 'managerType', inputValue: 'ROLLUP'},
										{boxLabel: 'Linked', name: 'managerType', inputValue: 'LINKED'}
									]
								},
								{
									xtype: 'fieldset-radio',
									groupName: 'managerType-Group',
									radioName: 'managerType',
									expandValue: 'PROXY',
									items: [
										{xtype: 'label', html: 'Specifies account value when accurate balance information is not available from the Custodian. Takes precedence over Custodian data. The proxy value can be adjusted based on selected benchmark\'s performance (security or interest rate).  Value or quantity (benchmark shares held) can be entered, but not both.'},
										{
											fieldLabel: 'Value', xtype: 'currencyfield', name: 'proxyValue', mutuallyExclusiveFields: ['proxySecurityQuantity'],
											qtip: 'Reported Proxy Balance on Last Updated Date'
										},
										{
											fieldLabel: 'Last Updated', xtype: 'datefield', name: 'proxyValueDate'
										},
										{
											fieldLabel: 'Benchmark Security', name: 'proxyBenchmarkSecurity.label', hiddenName: 'proxyBenchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow', mutuallyExclusiveFields: ['proxyBenchmarkRateIndex.id'],
											qtip: 'Proxy Manager Balances can be auto adjusted based on proxy value date and the benchmark\'s performance'
										},
										{
											fieldLabel: 'Display Name', name: 'proxyBenchmarkSecurityDisplayName', requiredFields: ['proxyBenchmarkSecurity.label'],
											qtip: 'Used for reporting to display this name instead of the security name or ticker.'
										},
										{
											boxLabel: 'Proxy to Last Known Price', xtype: 'checkbox', name: 'proxyToLastKnownPrice', requiredFields: ['proxyBenchmarkSecurity.label'],
											qtip: 'Used for Managers with Proxy Benchmark whose price is not available every date, i.e. 2 day lag'
										},
										{
											fieldLabel: 'Quantity', xtype: 'floatfield', name: 'proxySecurityQuantity', requiredFields: ['proxyBenchmarkSecurity.label'], mutuallyExclusiveFields: ['proxyValue'],
											qtip: 'Instead of an actual value, number of shares of proxy benchmark that are held.  Manager balance is calculated as Shares * Security Price.'
										},
										{
											fieldLabel: 'Benchmark Rate', name: 'proxyBenchmarkRateIndex.label', hiddenName: 'proxyBenchmarkRateIndex.id', xtype: 'combo', url: 'investmentInterestRateIndexListFind.json?active=true', displayField: 'label', detailPageClass: 'Clifton.investment.rates.InterestRateIndexWindow', mutuallyExclusiveFields: ['proxyBenchmarkSecurity.id'],
											qtip: 'Instead of a benchmark, proxy values can be adjusted based on the rate change since proxy value date.'
										},
										{boxLabel: 'Invert manager value growth based on benchmark', xtype: 'checkbox', name: 'proxyValueGrowthInverted', requiredFields: ['proxyBenchmarkSecurity.label']},
										{boxLabel: 'Proxy securities balance only and use custodial cash balance', xtype: 'checkbox', name: 'proxySecuritiesOnly', requiredFields: ['custodianAccount.id']}
									]
								},
								{
									xtype: 'fieldset-radio',
									groupName: 'managerType-Group',
									radioName: 'managerType',
									expandValue: 'ROLLUP',
									labelWidth: 150,
									items: [
										{
											fieldLabel: 'Rollup Aggregation Type', name: 'aggregationType', hiddenName: 'rollupAggregationType', allowBlank: 'false',
											qtip: 'Determines how child manager balances are processed. Sum: sums child account balances, Minimum: selects just the minimum balance, Maximum: selects just the maximum balance.',
											xtype: 'combo', mode: 'local', value: 'SUM', store: {xtype: 'arraystore', data: Clifton.core.util.math.AGGREGATION_OPERATIONS.filter(item => ['SUM', 'MIN', 'MAX'].includes(item[0]))}
										},
										{xtype: 'label', html: '<br/>'},
										{xtype: 'label', html: 'A rollup manager is not a real manager but a roll up of one or more manager account balances.'},
										{
											xtype: 'formgrid',
											title: 'Child Managers',
											storeRoot: 'rollupManagerList',
											dtoClass: 'com.clifton.investment.manager.InvestmentManagerAccountRollup',
											columnsConfig: [
												{
													header: 'Manager Account', width: 350, dataIndex: 'referenceTwo.label', idDataIndex: 'referenceTwo.id',
													editor: {
														xtype: 'combo', url: 'investmentManagerAccountListFind.json', displayField: 'label',
														detailPageClass: 'Clifton.investment.manager.AccountWindow',
														beforequery: function(queryEvent) {
															const grid = queryEvent.combo.gridEditor.containerGrid;
															const f = TCG.getParentFormPanel(grid).getForm();
															queryEvent.combo.store.baseParams = {clientIdOrRelatedClient: TCG.getValue('client.id', f.formValues)};
														}
													}
												},
												{header: 'Percentage', width: 150, dataIndex: 'allocationPercent', type: 'percent', numberFormat: '0,000.0000000000', editor: {xtype: 'currencyfield', decimalPrecision: 10, allowBlank: false}}
											]
										}
									]
								},
								{
									xtype: 'fieldset-radio',
									groupName: 'managerType-Group',
									radioName: 'managerType',
									expandValue: 'LINKED',
									items: [
										{xtype: 'label', html: 'Takes precedence over Custodian data. A linked manager is not a real manager but a manager whose balances are created from linked investment account values, manager assignments, or exposure. You can optionally filter balance information to a specific manager and/or asset class.'},

										{
											fieldLabel: 'Link Type', name: 'linkedManagerTypeLabel', hiddenName: 'linkedManagerType', xtype: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
											store: new Ext.data.ArrayStore({
												fields: ['name', 'value', 'description'],
												data: Clifton.investment.manager.LinkedManagerTypes
											})
										},
										{
											fieldLabel: 'Client Account', xtype: 'combo', name: 'linkedInvestmentAccount.label', hiddenName: 'linkedInvestmentAccount.id', url: 'investmentAccountListFind.json', displayField: 'label',
											beforequery: function(queryEvent) {
												const f = queryEvent.combo.getParentForm().getForm();
												queryEvent.combo.store.baseParams = {ourAccount: true};
												if (!f.findField('allowAnyClient').getValue()) {
													queryEvent.combo.store.baseParams.clientIdOrRelatedClient = TCG.getValue('client.id', f.formValues);
												}
											}
										},
										{boxLabel: 'Allow any client account to be selected', xtype: 'checkbox', name: 'allowAnyClient'},

										{
											xtype: 'formfragment',
											frame: false,
											name: 'linkedManagerCustomFields',
											items: []
										}
									]
								}
							]
						},

						{
							xtype: 'fieldset-checkbox',
							title: 'Mark to Market Cash Adjustment',
							instructions: 'Applies the M2M for the selected Clifton account to the manager cash balance. ' +
								'You may choose either Days of M2M or Start Date.  ' +
								'You may include the M2M from multiple Clifton accounts in one manager.',
							checkboxName: 'adjustmentM2MExists',
							items: [{
								xtype: 'formgrid',
								storeRoot: 'adjustmentM2MList',
								dtoClass: 'com.clifton.investment.manager.InvestmentManagerAccountAdjustmentM2M',
								columnsConfig: [
									{
										header: 'Mark to Market Account', width: 230, dataIndex: 'referenceTwo.label', idDataIndex: 'referenceTwo.id',
										editor: {
											xtype: 'combo', url: 'investmentAccountListFind.json', displayField: 'label',
											beforequery: function(queryEvent) {
												const grid = queryEvent.combo.gridEditor.containerGrid;
												const f = TCG.getParentFormPanel(grid).getForm();
												queryEvent.combo.store.baseParams = {ourAccount: true, clientIdOrRelatedClient: TCG.getValue('client.id', f.formValues)};
											}
										}
									},
									{
										header: 'Calculation Type', width: 150, dataIndex: 'adjustmentTypeLabel', idDataIndex: 'adjustmentType',
										tooltip: 'How M2M is calculated/Where it is pulled from.',
										editor: {
											displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', allowBlank: false,
											store: new Ext.data.ArrayStore({
												fields: ['name', 'value', 'description'],
												data: Clifton.investment.manager.ManagerM2MAdjustmentTypes
											})
										}
									},
									{
										header: 'Days of M2M', width: 90, dataIndex: 'daysOfM2M', type: 'int', useNull: true, editor: {xtype: 'integerfield'},
										tooltip: 'Days of M2M is the total number of previous business days of M2Ms to be added to the daily balance.'
									},
									{
										header: 'Start Date', width: 90, dataIndex: 'startDate', editor: {xtype: 'datefield', width: 60, disabledDays: [0, 6], maxValue: new Date()},
										tooltip: 'Start Date is the date from which to begin calculating M2M\'s.'
									}
								]
							}]
						},

						{
							xtype: 'fieldset-checkbox',
							title: 'Cash Balance Adjustments',
							checkboxName: 'cashAllocation',
							labelWidth: 10,
							items: [
								{
									xtype: 'radiogroup', name: 'cashAdjustmentType', columns: 1, fieldLabel: '',
									anchor: '0',
									items: [
										{boxLabel: 'Do not change cash allocation', name: 'cashAdjustmentType', inputValue: 'NONE', checked: true},
										{boxLabel: 'Allocate full manager account balance to cash (nothing to securities)', name: 'cashAdjustmentType', inputValue: 'CASH'},
										{boxLabel: 'Allocate full manager account balance to securities (nothing to cash)', name: 'cashAdjustmentType', inputValue: 'SECURITIES'},
										{boxLabel: 'Ignore securities balance but do not change cash allocation', name: 'cashAdjustmentType', inputValue: 'IGNORE_SECURITIES'},
										{boxLabel: 'Ignore cash balance but do not change securities allocation', name: 'cashAdjustmentType', inputValue: 'IGNORE_CASH'},
										{boxLabel: 'Use the following custom Cash allocation percentage (remainder to securities):', name: 'cashAdjustmentType', inputValue: 'CUSTOM_PERCENT'},
										{boxLabel: 'Use the following custom Cash allocation value (remainder to securities):', name: 'cashAdjustmentType', inputValue: 'CUSTOM_VALUE'},
										{
											xtype: 'formfragment',
											frame: false,
											bodyStyle: 'padding-left: 30px',
											items: [
												{fieldLabel: 'Cash Value', name: 'customCashValue', xtype: 'currencyfield'},
												{fieldLabel: 'Updated On', name: 'customCashValueUpdateDate', xtype: 'datefield'}
											]
										},
										{boxLabel: 'Use cash allocation percentage from the following manager account:', name: 'cashAdjustmentType', inputValue: 'MANAGER'}
									],
									listeners: {
										change: function(f) {
											const p = TCG.getParentFormPanel(f);
											p.setResetCashAllocationFields();
										}
									}
								},
								{
									xtype: 'formfragment',
									frame: false,
									bodyStyle: 'padding-left: 45px',
									anchor: '0',
									items: [
										{
											fieldLabel: 'Manager Account', name: 'cashPercentManager.label', hiddenName: 'cashPercentManager.id', xtype: 'combo', url: 'investmentManagerAccountListFind.json', displayField: 'label',
											beforequery: function(queryEvent) {
												const f = queryEvent.combo.getParentForm().getForm();
												queryEvent.combo.store.baseParams = {clientIdOrRelatedClient: TCG.getValue('client.id', f.formValues)};
											}
										}
									]
								}
							]
						},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'InvestmentManagerAccount'
						}
					]
				}]
			},


			{
				title: 'Assignments',
				items: [{
					name: 'investmentManagerAccountAssignmentListByManager',
					xtype: 'gridpanel',
					instructions: 'The following client accounts are assigned to this manager, usually in order to overlay manager\'s assets.',
					getLoadParams: function() {
						return {managerAccountId: this.getWindow().getMainForm().formValues.id};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Service Processing Type', width: 200, hidden: true, dataIndex: 'referenceTwo.serviceProcessingType.name'},
						{header: 'Processing Type', width: 100, hidden: true, dataIndex: 'referenceTwo.serviceProcessingType.processingType'},
						{header: 'Client Account', width: 200, dataIndex: 'referenceTwo.label'},
						{header: 'Display Name', width: 100, dataIndex: 'displayName'},
						{header: 'Target Allocation', width: 100, dataIndex: 'targetAllocationPercent', type: 'currency'},
						{header: 'Overlay Cash', width: 125, dataIndex: 'cashOverlayType'},
						{
							header: 'Allocate Securities', width: 75, dataIndex: 'allocateSecuritiesBalanceToReplications', type: 'boolean',
							tooltip: 'Allocate Securities Balance Allocations to Replication\'s Additional Exposure'
						},
						{header: 'Inactive', width: 50, dataIndex: 'inactive', type: 'boolean'}
					],
					editor: {
						detailPageClass: {
							processingTypeItemMappings: [
								['LDI', 'LDI'],
								['DURATION_MANAGEMENT', 'Duration Management'],
								['SECURITY_TARGET', 'Security Target'],
								['PORTFOLIO_TARGET', 'Portfolio Target'],
								['PORTFOLIO_RUN', 'Overlay']
							],
							getItemText: function(rowItem) {
								const pt = rowItem.get('referenceTwo.serviceProcessingType.processingType');
								const mapping = this.processingTypeItemMappings.find(type => type[0] === pt);
								if (mapping) {
									return mapping[1];
								}
								return 'Overlay';
							},
							items: [
								{text: 'Overlay', tooltip: 'Applies to Overlay, Structured Options, and Fully Funded Service Types', iconCls: 'contract', className: 'Clifton.investment.manager.assignment.OverlayAssignmentWindow'},
								{text: 'LDI', iconCls: 'contract', className: 'Clifton.investment.manager.assignment.LDIAssignmentWindow'},
								{text: 'Duration Management', iconCls: 'contract', className: 'Clifton.investment.manager.assignment.DurationManagementAssignmentWindow'},
								{text: 'Security Target', iconCls: 'contract', className: 'Clifton.investment.manager.assignment.SecurityTargetAssignmentWindow'},
								{text: 'Portfolio Target', iconCls: 'contract', className: 'Clifton.investment.manager.assignment.TargetAssignmentWindow'}
							]
						},
						deleteURL: 'investmentManagerAccountAssignmentDelete.json',
						getDefaultData: function(gridPanel) { // defaults manager account for the detail page
							return {
								referenceOne: gridPanel.getWindow().getMainForm().formValues,
								referenceTwo: {businessClient: TCG.getValue('client', gridPanel.getWindow().getMainForm().formValues)}
							};
						}
					}
				}]
			},


			{
				title: 'Balance History',
				items: [{
					name: 'investmentManagerAccountBalanceListFind',
					xtype: 'gridpanel',
					instructions: 'Securities and cash balance history for selected manager\'s account.' +
						'<ul class="c-list">' +
						'<li>Adjusted balances are displayed in red and include all system and user defined adjustments that are not MOC adjustments.</li>' +
						'<li>If MOC (Market On Close) column is checked there are MOC adjustments that result in a different balance than the adjusted balance.  MOC is used when clients make a significant change (i.e. get/remove a lot of cash or move cash between managers) on a specific date and they want to change exposure right before the market closes to account for this.  There will be 2 or so day delay before we see this in manager download, so we use MOC adjustments to account for these changes.</li>' +
						'</ul>',
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 30 days of balances
							this.setFilterValue('balanceDate', {'after': new Date().add(Date.DAY, -30)});
						}
						return {managerAccountId: this.getWindow().getMainForm().formValues.id};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Date', width: 70, dataIndex: 'balanceDate'},
						{header: 'Note', width: 180, dataIndex: 'note'},

						// Boolean Flags for if the Balance was adjusted
						{header: 'Cash Adjusted', dataIndex: 'cashAdjustment', type: 'boolean', hidden: true, filter: false},
						{header: 'Securities Adjusted', dataIndex: 'securitiesAdjustment', type: 'boolean', hidden: true, filter: false},

						// Original Balance Info
						{header: 'Original Cash', dataIndex: 'cashValue', hidden: true, type: 'currency'},
						{header: 'Original Securities', dataIndex: 'securitiesValue', hidden: true, type: 'currency'},
						{header: 'Original Total', dataIndex: 'totalValue', hidden: true, type: 'currency'},

						// Adjusted Balance Info
						{
							header: 'Cash', width: 100, dataIndex: 'adjustedCashValue', type: 'currency',
							renderer: function(v, p, r) {
								const value = Ext.util.Format.number(v, '0,000.00');
								if (TCG.isTrue(r.data['cashAdjustment'])) {
									return '<div style="COLOR: #ff0000;" qtip=\'Original value: ' + Ext.util.Format.number(r.data['cashValue'], '0,000.00') + '<br/>Adjustment: ' + Ext.util.Format.number(r.data['adjustedCashValue'] - r.data['cashValue'], '0,000.00') + '\'>' + value + '</div>';
								}
								return value;
							}
						},
						{
							header: 'Securities', width: 100, dataIndex: 'adjustedSecuritiesValue', type: 'currency',
							renderer: function(v, p, r) {
								const value = Ext.util.Format.number(v, '0,000.00');
								if (TCG.isTrue(r.data['securitiesAdjustment'])) {
									return '<div style="COLOR: #ff0000;" qtip=\'Original value: ' + Ext.util.Format.number(r.data['securitiesValue'], '0,000.00') + '<br/>Adjustment: ' + Ext.util.Format.number(r.data['adjustedSecuritiesValue'] - r.data['securitiesValue'], '0,000.00') + '\'>' + value + '</div>';
								}
								return value;
							}
						},
						{
							header: 'Total', width: 100, dataIndex: 'adjustedTotalValue', type: 'currency',
							renderer: function(v, p, r) {
								const value = Ext.util.Format.number(v, '0,000.00');
								if (TCG.isTrue(r.data['cashAdjustment']) || TCG.isTrue(r.data['securitiesAdjustment'])) {
									return '<div style="COLOR: #ff0000;" qtip=\'Original value: ' + Ext.util.Format.number(r.data['totalValue'], '0,000.00') + '<br/>Adjustment: ' + Ext.util.Format.number(r.data['adjustedTotalValue'] - r.data['totalValue'], '0,000.00') + '\'>' + value + '</div>';
								}
								return value;
							}
						},


						// MOC Balance Info
						{header: 'MOC', width: 35, dataIndex: 'mocAdjustment', type: 'boolean'},
						{
							header: 'MOC Cash', width: 100, dataIndex: 'marketOnCloseCashValue', type: 'currency', hidden: true,
							renderer: function(v, p, r) {
								const value = Ext.util.Format.number(v, '0,000.00');
								if (TCG.isTrue(r.data['mocAdjustment'])) {
									return '<div style="COLOR: #ff0000;" qtip=\'Adjusted value: ' + Ext.util.Format.number(r.data['adjustedCashValue'], '0,000.00') + '<br/>MOC Adjustment: ' + Ext.util.Format.number(r.data['marketOnCloseCashValue'] - r.data['adjustedCashValue'], '0,000.00') + '\'>' + value + '</div>';
								}
								return value;
							}
						},
						{
							header: 'MOC Securities', width: 100, dataIndex: 'marketOnCloseSecuritiesValue', type: 'currency', hidden: true,
							renderer: function(v, p, r) {
								const value = Ext.util.Format.number(v, '0,000.00');
								if (TCG.isTrue(r.data['mocAdjustment'])) {
									return '<div style="COLOR: #ff0000;" qtip=\'Adjusted value: ' + Ext.util.Format.number(r.data['adjustedSecuritiesValue'], '0,000.00') + '<br/>MOC Adjustment: ' + Ext.util.Format.number(r.data['marketOnCloseSecuritiesValue'] - r.data['adjustedSecuritiesValue'], '0,000.00') + '\'>' + value + '</div>';
								}
								return value;
							}
						},
						{
							header: 'MOC Total', width: 100, dataIndex: 'marketOnCloseTotalValue', type: 'currency',
							renderer: function(v, p, r) {
								const value = Ext.util.Format.number(v, '0,000.00');
								if (TCG.isTrue(r.data['mocAdjustment'])) {
									return '<div style="COLOR: #ff0000;" qtip=\'Adjusted value: ' + Ext.util.Format.number(r.data['adjustedTotalValue'], '0,000.00') + '<br/>MOC Adjustment: ' + Ext.util.Format.number(r.data['marketOnCloseTotalValue'] - r.data['adjustedTotalValue'], '0,000.00') + '\'>' + value + '</div>';
								}
								return value;
							}
						}


					],
					editor: {
						detailPageClass: 'Clifton.investment.manager.BalanceWindow',
						getDefaultData: function(gridPanel) { // defaults manager account for the detail page
							return {
								managerAccount: gridPanel.getWindow().getMainForm().formValues
							};
						},
						deleteEnabled: false
					}
				}]
			},


			{
				title: 'Transaction History',
				items: [{
					xtype: 'investment-manager-custodian-transaction-grid',
					instructions: 'A list of Transactions for selected Investment Manager\'s Custodian Account.',
					plugins: {ptype: 'gridsummary'},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 7 days of balances
							this.setFilterValue('transactionDate', {'after': new Date().add(Date.DAY, -7)});
						}
						const f = this.getWindow().getMainForm();
						const custodianAccount = f.formValues.custodianAccount;
						if (custodianAccount) {
							return {
								custodianAccountNumber: custodianAccount.number
							};
						}
						TCG.showInfo('Cannot retrieve Custodian Transactions because Investment Manager Account does not have Custodian Account populated.');
						return false;
					}
				}]
			},


			{
				title: 'Manager Rules',
				items: [{
					xtype: 'rule-assignment-grid-forAdditionalScopeEntity',
					scopeTableName: 'InvestmentAccount',
					entityTableName: 'InvestmentManagerAccount',
					defaultCategoryName: 'Portfolio Run Rules',
					instructions: 'The following Portfolio Run Rules apply to this manager account.  An account selection is required because a manager can be assigned to multiple client accounts of different types.',

					getAdditionalScopeFkFieldId: function() {
						const acctField = TCG.getChildByName(this.getTopToolbar(), 'additionalFkFieldId');
						return acctField.getValue();
					},
					getAdditionalScopeFkFieldLabel: function() {
						const acctField = TCG.getChildByName(this.getTopToolbar(), 'additionalFkFieldId');
						return acctField.getRawValue();
					},
					getTopToolbarFilters: function(toolbar) {
						const w = this.getWindow();
						return [
							{
								fieldLabel: 'Client Account', xtype: 'toolbar-combo', name: 'additionalFkFieldId', width: 200, displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true',
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const clientId = TCG.getValue('client.id', w.getMainForm().formValues);
									combo.store.baseParams = {clientId: clientId};
								}
							},
							{fieldLabel: 'Active On', xtype: 'datefield', name: 'activeOnDate', width: 90}
						];
					},
					appendAdditionalLoadParams: function(firstLoad, params) {
						if (firstLoad) {
							// See if there is only one active client account assigned
							const accounts = TCG.data.getData('investmentManagerAccountAssignmentListByManager.json?managerAccountId=' + this.getWindow().getMainFormId(), this);
							if (accounts && accounts.length === 1) {
								TCG.getChildByName(this.getTopToolbar(), 'additionalFkFieldId').setValue({value: accounts[0].referenceTwo.id, text: accounts[0].referenceTwo.label});
							}
							else {
								return false;
							}
						}

						params.entityTableNameEquals = this.entityTableName;
						params.entityFkFieldId = this.getEntityFkFieldId();
						return params;
					}
				}]
			},


			{
				title: 'Contacts',
				items: [{
					xtype: 'business-companyContactEntryGrid-byCompany',
					instructions: 'The following contacts are associated with this manager\'s company.',
					getCompany: function() {
						return TCG.getValue('managerCompany', this.getWindow().getMainForm().formValues);
					}
				}]
			},


			{
				title: 'Audit Trail',
				items: [{
					xtype: 'system-audit-grid',
					tableName: 'InvestmentManagerAccount',
					childTables: 'InvestmentManagerAccountRollup,InvestmentManagerAccountAdjustmentM2M'
				}]
			}
		]
	}]
});
