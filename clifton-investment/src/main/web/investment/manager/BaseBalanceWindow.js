Clifton.investment.manager.BaseBalanceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Manager Account Balance',
	iconCls: 'manager',
	height: 600,
	width: 800,
	enableRefreshWindow: true,

	addFormGridButtons: function(gridPanel, toolBar) {
		// Can be Overridden - Used to Add "Process Adjustments" in Product Project
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Balance',
				items: [{
					xtype: 'formpanel',
					loadValidation: false,
					url: 'investmentManagerAccountBalance.json',
					saveURL: 'investmentManagerAccountBalanceSave.json',
					getSaveURL: function() {
						return this.saveURL;
					},
					layout: 'anchor',
					defaults: {anchor: '-15'},
					getWarningMessage: function(form) {
						return Clifton.rule.violation.getRuleViolationWarningMessage('balance', form.formValues, true);
					},
					getDefaultData: function(win) { // defaults balance date to previous business day
						let dd = win.defaultData;
						if (!dd) {
							dd = {};
						}
						const previousBDay = Clifton.calendar.getBusinessDayFrom(-1).format('Y-m-d 00:00:00');
						dd = Ext.apply({
							balanceDate: previousBDay
						}, dd);
						return dd;
					},
					items: [
						{
							xtype: 'fieldset',
							title: 'Account Info',
							items: [
								{fieldLabel: 'Client', name: 'managerAccount.client.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailIdField: 'managerAccount.client.id', submitDetailField: false},
								{fieldLabel: 'Manager Account', name: 'managerAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.manager.AccountWindow', detailIdField: 'managerAccount.id'}
							]
						},
						{
							xtype: 'fieldset',
							title: 'Balance Info',
							items: [
								{fieldLabel: 'Violation Status', name: 'violationStatus.name', xtype: 'displayfield'},
								{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'datefield', allowBlank: false},
								{
									fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
									defaults: {
										xtype: 'displayfield',
										align: 'right',
										flex: 1
									},
									items: [
										{value: 'Original Balance'},
										{value: 'Adjusted Balance'},
										{value: 'MOC Balance'}
									]
								},
								{
									fieldLabel: 'Cash Value', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										flex: 1
									},
									items: [
										{name: 'cashValue', allowBlank: false},
										{name: 'adjustedCashValue', submitValue: false, disabled: true},
										{name: 'marketOnCloseCashValue', submitValue: false, disabled: true}
									]
								},
								{
									fieldLabel: 'Securities Value', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										flex: 1
									},
									items: [
										{name: 'securitiesValue', allowBlank: false},
										{name: 'adjustedSecuritiesValue', submitValue: false, disabled: true},
										{name: 'marketOnCloseSecuritiesValue', submitValue: false, disabled: true}
									]
								},
								{
									fieldLabel: 'Totals', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										flex: 1
									},
									items: [
										{name: 'totalValue', submitValue: false, disabled: true},
										{name: 'adjustedTotalValue', submitValue: false, disabled: true},
										{name: 'marketOnCloseTotalValue', submitValue: false, disabled: true}
									]
								},
								{fieldLabel: 'Note', name: 'note', xtype: 'textarea', height: 50}

							]
						},
						{
							xtype: 'formgrid',
							title: 'Balance Adjustments',
							storeRoot: 'adjustmentList',
							dtoClass: 'com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment',
							addToolbarButtons: function(toolBar, gridPanel) {
								toolBar.add({
									text: 'Calculate Adjustment',
									tooltip: 'Add new calculated adjustment by entering final balance values',
									iconCls: 'calculator',
									handler: function() {
										const panel = TCG.getParentFormPanel(gridPanel);
										const w = panel.getWindow();
										if (w.isModified() || !w.isMainFormSaved()) {
											TCG.showError('Please save your changes before entering a calculated adjustment.');
											return;
										}
										TCG.createComponent('Clifton.investment.manager.CalculateAdjustmentWindow', {
											defaultData: panel.getForm().formValues,
											openerCt: gridPanel
										});
									}
								});
								toolBar.add('-');
								toolBar.add({
									text: 'Copy Previous',
									tooltip: 'Copy manual adjustments from previous day balance (excludes adjustments created from Calendar Events).',
									iconCls: 'copy',
									handler: function() {
										Ext.Msg.confirm('Copy Previous', 'Are you sure you want to save all current changes and copy all manual adjustments from this manager\'s previous business day\'s balance?', function(a) {
											if (a === 'yes') {
												const panel = TCG.getParentFormPanel(gridPanel);
												panel.saveURL = 'investmentManagerAccountBalanceAndCopyPreviousAdjustmentsSave.json';
												gridPanel.getWindow().saveWindow(false, true);
												// Reset Save Method
												panel.saveURL = 'investmentManagerAccountBalanceSave.json';
											}
										});
									}
								});
								toolBar.add('-');
								// Add Additional Buttons (i.e. Process Adjustments Button)
								gridPanel.getWindow().addFormGridButtons(gridPanel, toolBar);
							},
							addCalculatedAdjustmentRow: function(params, saveBalance) {
								const store = this.getStore();
								const RowClass = store.recordType;
								this.stopEditing();
								store.insert(0, new RowClass(params));
								this.startEditing(0, 0);
								this.markModified();
								if (saveBalance) {
									this.getWindow().saveWindow(false);
								}
							},
							columnsConfig: [
								{
									header: 'Adjustment Type', width: 190, dataIndex: 'adjustmentType.label', idDataIndex: 'adjustmentType.id',
									editor: {xtype: 'combo', url: 'investmentManagerAccountBalanceAdjustmentTypeListFind.json?systemDefined=false', displayField: 'label'}
								},
								{header: 'Note', dataIndex: 'note', width: 270, editor: {xtype: 'textfield'}},
								{header: 'Cash', dataIndex: 'cashValue', width: 90, type: 'currency', editor: {xtype: 'currencyfield'}},
								{header: 'Securities', dataIndex: 'securitiesValue', width: 90, type: 'currency', editor: {xtype: 'currencyfield'}},
								{header: 'Source Table', dataIndex: 'sourceSystemTable.name', width: 10, hidden: true},
								{
									header: '', width: 30, dataIndex: 'sourceFkFieldId',
									renderer: function(fkId, args, r) {
										if (r.data['sourceSystemTable.name'] === 'InvestmentEventManagerAdjustment' && TCG.isNotBlank(fkId)) {
											return TCG.renderActionColumn('calendar', '', 'View Event that Created this Adjustment', 'calendar');
										}
										if (r.data['sourceSystemTable.name'] === 'InvestmentManagerAccountBalanceAdjustment' && TCG.isNotBlank(fkId)) {
											return TCG.renderActionColumn('manager', '', 'View Original Adjustment that Created this Adjustment', 'balanceAdjustment');
										}
										return '';
									}
								}
							],

							listeners: {
								// Drill Into Event
								'cellclick': function(grid, rowIndex, cellIndex, evt) {
									if (TCG.isActionColumn(evt.target)) {
										const row = grid.store.data.items[rowIndex];
										const fkId = row.data['sourceFkFieldId'];
										const eventName = TCG.getActionColumnEventName(evt);
										if (eventName === 'calendar') {
											TCG.createComponent('Clifton.investment.calendar.EventManagerAdjustmentWindowSelector', {
												id: TCG.getComponentId('Clifton.investment.calendar.EventManagerAdjustmentWindowSelector', fkId),
												params: {id: fkId},
												openerCt: grid
											});
										}
										else if (eventName === 'balanceAdjustment') {
											TCG.createComponent('Clifton.investment.manager.BalanceAdjustmentWindowSelector', {
												id: TCG.getComponentId('Clifton.investment.manager.BalanceAdjustmentWindowSelector', fkId),
												params: {id: fkId},
												openerCt: grid
											});
										}
									}
								}
							}
						}
					]
				}]
			},


			{
				title: 'Violations',
				items: [{
					xtype: 'rule-violation-grid',
					tableName: 'InvestmentManagerAccountBalance'
				}]
			},


			{
				title: 'Position Import Details',
				items: [{
					name: 'integrationManagerPositionHistoryList',
					xtype: 'gridpanel',
					instructions: 'The following manager position history is associated with this manager\'s custodial account.',
					columns: [
						{header: 'Asset Class', width: 120, dataIndex: 'managerSecurity.assetClass'},
						{header: 'Source Asset Class', width: 80, dataIndex: 'managerSecurity.sourceAssetClass', hidden: true},
						{header: 'Security', width: 80, dataIndex: 'managerSecurity.securityIdentifier', hidden: true},
						{header: 'Security Type', width: 75, dataIndex: 'managerSecurity.securityIdentifierType', hidden: true},
						{header: 'Security Description', width: 120, dataIndex: 'managerSecurity.securityDescription'},
						{header: 'Type', width: 70, dataIndex: 'type.name'},
						{header: 'Cash', width: 95, dataIndex: 'cashValue', type: 'currency', summaryType: 'sum', negativeInRed: true},
						{header: 'Securities', width: 95, dataIndex: 'securityValue', type: 'currency', summaryType: 'sum', negativeInRed: true},
						{header: 'Value', width: 95, dataIndex: 'value', type: 'currency', summaryType: 'sum', negativeInRed: true}
					],
					plugins: {ptype: 'gridsummary'},
					getLoadParams: function() {
						const f = this.getWindow().getMainForm();
						const custodianAccount = f.formValues.managerAccount.custodianAccount;
						if (custodianAccount) {
							let company = custodianAccount.company;
							if (company) {
								company = company.id;
							}
							return {
								businessCompanyId: company,
								bankCode: f.formValues.managerAccount.custodianAccount.number,
								balanceDate: TCG.parseDate(f.formValues.balanceDate).format('m/d/Y')
							};
						}

						TCG.showInfo('Cannot retrieve Position History because Investment Manager Account does not have Custodian Account populated.');
						return false;
					},
					reload: function(firstPage) {
						const params = this.getLoadParams();
						if (TCG.isBlank(params.businessCompanyId) || TCG.isBlank(params.bankCode)) {
							TCG.showError('There is no custodian and/or bank code associated with this manager account.', 'No Import Details Available');
							return;
						}
						if (this.autoLoad === false) {
							this.autoLoad = true;
							return;
						}
						if (this.ds.proxy) {
							if (!(typeof params === 'boolean' && !params)) {
								this.ds.baseParams = params;
								this.ds.proxy.setUrl(encodeURI(this.getLoadURL()));
								const p = this.grid.getBottomToolbar();
								if (p) {
									p.doLoad(firstPage ? 0 : p.cursor);
								}
								else {
									this.ds.load();
								}
							}
						}
					}
				}]
			},


			{
				title: 'Transactions',
				items: [{
					xtype: 'investment-manager-custodian-transaction-grid',
					instructions: 'A list of Transactions for selected Investment Manager\'s Custodian Account on Balance Date.',
					columnOverrides: [
						{dataIndex: 'transactionDate', hidden: true}
					],
					plugins: {ptype: 'gridsummary'},
					getTopToolbarInitialLoadParams: function() {
						const f = this.getWindow().getMainForm();
						const custodianAccount = f.formValues.managerAccount.custodianAccount;
						if (custodianAccount) {
							return {
								custodianAccountNumber: custodianAccount.number,
								transactionDate: TCG.parseDate(f.formValues.balanceDate).format('m/d/Y')
							};
						}

						TCG.showInfo('Cannot retrieve Custodian Transactions because Investment Manager Account does not have Custodian Account populated.');
						return false;
					}
				}]
			}
		]
	}]
});


Clifton.investment.manager.CalculateAdjustmentWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Calculate Manager Balance Adjustment',
	iconCls: 'calculator',
	height: 400,
	width: 625,
	modal: true,
	okButtonText: 'Calculate',
	okButtonTooltip: 'Calculate Adjustment',
	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			labelWidth: 120,
			instructions: 'Select an adjustment type, and then enter the final expected cash and/or securities balance to calculate the adjustment values.  If a final balance is not entered, the existing balance will remain (and the corresponding cash or securities adjustment will be zero.), unless you select the box to apply an offsetting adjustment which essentially moves the value between cash and securities.',

			prepareDefaultData: function(dd) {
				if (!dd) {
					dd = {};
				}
				// Default to Daily Manual Bank Override
				dd.adjustmentType = TCG.data.getData('investmentManagerAccountBalanceAdjustmentTypeByName.json?name=Daily Manual Bank Override', this, 'investment.manager.balance.adjustment.type.Daily Manual Bank Override');
				return dd;
			},
			items: [
				{xtype: 'hidden', name: 'marketOnClose'},
				{
					fieldLabel: 'Adjustment Type', name: 'adjustmentType.label', hiddenName: 'adjustmentType.id', xtype: 'combo', url: 'investmentManagerAccountBalanceAdjustmentTypeListFind.json?systemDefined=false', displayField: 'label', requestedProps: 'marketOnClose',
					listeners: {
						// show current balances based on MOC flag
						select: function(combo, record, index) {
							const moc = TCG.getValue('marketOnClose', record.json);
							TCG.getChildByName(combo.getParentForm(), 'marketOnClose').setValue(moc);

							TCG.getChildByName(combo.getParentForm(), 'adjustedBalance').setVisible(moc === false);
							TCG.getChildByName(combo.getParentForm(), 'mocBalance').setVisible(moc === true);
						}
					}
				},
				{
					fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
					defaults: {
						xtype: 'displayfield',
						align: 'right',
						flex: 1
					},
					items: [
						{value: 'Cash'},
						{value: 'Securities'}
					]
				},
				{
					fieldLabel: 'Current Balance', xtype: 'compositefield',
					name: 'adjustedBalance',
					defaults: {
						xtype: 'currencyfield',
						flex: 1
					},
					items: [
						{name: 'adjustedCashValue', readOnly: true},
						{name: 'adjustedSecuritiesValue', readOnly: true}
					]
				},
				{
					fieldLabel: 'Current MOC Balance', xtype: 'compositefield',
					hidden: true,
					name: 'mocBalance',
					defaults: {
						xtype: 'currencyfield',
						flex: 1
					},
					items: [
						{name: 'marketOnCloseCashValue', readOnly: true},
						{name: 'marketOnCloseSecuritiesValue', readOnly: true}
					]
				},
				{
					fieldLabel: 'Final Balance', xtype: 'compositefield',
					requiredFields: ['adjustmentType.label'],
					defaults: {
						xtype: 'currencyfield',
						flex: 1
					},
					items: [
						{name: 'finalCashBalance'},
						{name: 'finalSecuritiesBalance'}
					]
				},
				{
					boxLabel: 'Apply Offsetting Adjustment to Cash/Securities', name: 'applyOffSettingAdjustment', xtype: 'checkbox',
					qtip: 'When only final cash or securities balance is entered, checking this box will create a negative adjustment that will offset the adjustment, essentially moving the amount between cash and securities without changing the total balance.'
				},
				{fieldLabel: 'Note', name: 'note', xtype: 'textarea'},
				{boxLabel: 'Immediately save calculated adjustment on the balance record', name: 'saveBalance', xtype: 'checkbox', checked: true}
			]
		}
	],

	saveWindow: function() {
		const panel = this.items.get(0);
		const form = panel.getForm();
		const params = {'adjustmentType.id': form.findField('adjustmentType.label').getValue(), 'adjustmentType.label': form.findField('adjustmentType.label').getRawValue(), 'note': form.findField('note').getValue()};

		let originalCash = form.findField('adjustedCashValue').getNumericValue();
		let originalSecurities = form.findField('adjustedSecuritiesValue').getNumericValue();
		if (form.getValues().marketOnClose === 'true') {
			originalCash = form.findField('marketOnCloseCashValue').getNumericValue();
			originalSecurities = form.findField('marketOnCloseSecuritiesValue').getNumericValue();
		}

		const finalCash = form.findField('finalCashBalance').getNumericValue();
		let cashAdjustment = 0;
		if (Ext.isNumber(finalCash)) {
			cashAdjustment = finalCash - originalCash;
		}

		const finalSecurities = form.findField('finalSecuritiesBalance').getNumericValue();
		let securitiesAdjustment = 0;
		if (Ext.isNumber(finalSecurities)) {
			securitiesAdjustment = finalSecurities - originalSecurities;
		}

		if (form.findField('applyOffSettingAdjustment').checked) {
			if (cashAdjustment !== 0) {
				if (securitiesAdjustment !== 0) {
					TCG.showError('You can only apply an offsetting adjustment if either final cash or final securities balance is entered, but not both.');
					return;
				}
				securitiesAdjustment = -cashAdjustment;
			}
			else if (securitiesAdjustment !== 0) {
				cashAdjustment = -securitiesAdjustment;
			}
		}
		params.cashValue = cashAdjustment;
		params.securitiesValue = securitiesAdjustment;

		const saveBalance = form.findField('saveBalance').checked;
		if (form.isDirty()) {
			if (this.openerCt && this.openerCt.addCalculatedAdjustmentRow) {
				this.openerCt.addCalculatedAdjustmentRow.call(this.openerCt, params, saveBalance);
				this.close();
			}
			else {
				TCG.showError('Opener component must be defined and must have addCalculatedAdjustmentRow(params) method.', 'Callback Missing');
			}
		}
		else {
			this.close();
		}
	}
});

