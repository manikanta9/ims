Clifton.investment.manager.AdjustmentTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Manager Adjustment Type',
	iconCls: 'manager',
	height: 450,
	width: 750,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'Adjustment Types are used when entering adjustments to Manager account balances.  System Defined adjustments can only be made system when generating automatic adjustments based upon specific manager attributes.  System Defined Adjustments are always processed first.',
		url: 'investmentManagerAccountBalanceAdjustmentType.json',
		labelWidth: 160,
		items: [
			{fieldLabel: 'Adjustment Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{boxLabel: 'System Defined (cannot be edited)', name: 'systemDefined', xtype: 'checkbox', readOnly: true},
			{fieldLabel: 'CSS Style', name: 'cssStyle'},
			{boxLabel: 'Allow Cash balance adjustments', name: 'cashAdjustmentAllowed', xtype: 'checkbox'},
			{boxLabel: 'Allow Securities balance adjustments', name: 'securitiesAdjustmentAllowed', xtype: 'checkbox'},
			{boxLabel: 'Market On Close Adjustment (Adjustments of this type are only applied to the MOC balances)', name: 'marketOnClose', xtype: 'checkbox'},
			{boxLabel: 'Require note field for adjustments of this type', name: 'noteRequired', xtype: 'checkbox'},
			{
				fieldLabel: 'Next Day Adjustment Type', name: 'nextDayAdjustmentType.label', hiddenName: 'nextDayAdjustmentType.id', xtype: 'combo', url: 'investmentManagerAccountBalanceAdjustmentTypeListFind.json?systemDefined=false',
				qtip: 'When populated, processing the manager balance will automatically copy all previous day adjustments to the next day and use the selected type.'
			}

		]
	}]
});
