Clifton.investment.manager.ManagerSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'investmentManagerSetupWindow',
	title: 'Investment Manager Setup',
	iconCls: 'manager',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Manager Accounts',
				items: [{
					xtype: 'gridpanel',
					name: 'investmentManagerAccountListFind',
					instructions: 'A list of money manager accounts used across all clients.  Each manager account can be assigned to specific client accounts, usually in order to overlay manager\'s assets. New manager accounts must be added through the client\'s detail page.',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Exclude Accounts in Workflow State', xtype: 'toolbar-combo', name: 'excludeClientAccountWorkflowStateId', width: 180, url: 'workflowStateListByWorkflowName.json?workflowName=Investment Account Status - Clifton Accounts', root: 'data'},
							{fieldLabel: 'Tag', xtype: 'toolbar-combo', name: 'tagHierarchyId', displayField: 'nameExpandedWithLabel', width: 330, url: 'systemHierarchyListFind.json?tableName=InvestmentManagerAccount'},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Manager', width: 100, dataIndex: 'managerCompany.name', filter: {searchFieldName: 'searchPattern'}, defaultSortColumn: true},
						{header: 'Client', width: 100, dataIndex: 'client.name', filter: {type: 'combo', searchFieldName: 'clientId', url: 'businessClientListFind.json'}},
						{header: 'Account', width: 50, dataIndex: 'labelShort', filter: {searchFieldName: 'accountNameNumber'}},
						{header: 'Custodial Account', width: 100, dataIndex: 'custodianLabel', filter: {type: 'combo', searchFieldName: 'custodianAccountId', displayField: 'label', url: 'investmentManagerCustodianAccountListFind.json'}},
						{
							header: 'Manager Type', dataIndex: 'managerType', width: 60,
							filter: {
								type: 'combo', searchFieldName: 'managerType', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.investment.manager.ManagerTypes
								})
							},
							renderer: function(v, p, r) {
								if (v === 'LINKED') {
									let qtip = '<table><tr><td>' + r.data['linkedManagerTypeLabel'] + '</td></tr>';
									if (TCG.isTrue(r.data['linkedManagerProcessingDependent'])) {
										qtip += '<tr><td>* Portfolio Processing Dependent</td></tr>';
									}
									qtip += '</table>';
									p.attr = 'qtip=\'' + qtip + '\'';
									p.css = 'amountAdjusted';
								}
								return v;
							}
						},
						{header: 'Sweep Account', width: 50, hidden: true, dataIndex: 'custodianAccount.sweepAccount', type: 'boolean', tooltip: 'A sweep account is a bank account that automatically transfers amounts that exceed, or fall short of, a certain level into a higher interest-earning investment option at the close of each business day. Commonly, the excess cash is swept into money market funds.', filter: {searchFieldName: 'sweepAccount'}},
						{header: 'Linked - Portfolio dependency', dataIndex: 'linkedManagerProcessingDependent', type: 'boolean', hidden: true, sortable: false, filter: false},
						{
							header: 'Linked Type', hidden: true, dataIndex: 'linkedManagerTypeLabel', width: 40,
							filter: {
								type: 'combo', searchFieldName: 'linkedManagerType', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.investment.manager.LinkedManagerTypes
								})
							}
						},
						{
							header: 'Cash Adj Type', dataIndex: 'cashAdjustmentType', width: 50, tooltip: 'Cash Adjustment Type',
							filter: {
								type: 'combo', searchFieldName: 'cashAdjustmentType', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.investment.manager.ManagerCashAdjustmentTypes
								})
							}
						},
						{header: 'Benchmark', width: 100, hidden: true, dataIndex: 'benchmarkSecurity.label', filter: {searchFieldName: 'benchmarkSecurityLabel'}},
						{header: 'Duration Field', width: 60, hidden: true, dataIndex: 'durationFieldNameOverride', type: 'string', filter: {xtype: 'system-list-combo', listName: 'Portfolio: Duration Field', searchFieldName: 'durationFieldNameOverride'}},
						{header: 'Base CCY', width: 50, hidden: true, dataIndex: 'baseCurrency.symbol', filter: {type: 'combo', searchFieldName: 'baseCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Proxy Date', dataIndex: 'proxyValueDate', width: 40},
						{header: 'Proxy Value', dataIndex: 'proxyValue', width: 60, type: 'currency', filter: false, sortable: false},
						{header: 'Proxy Benchmark', width: 100, hidden: true, dataIndex: 'proxyBenchmarkSecurity.label', filter: {searchFieldName: 'proxyBenchmarkSecurityLabel'}},
						{header: 'Proxy Rate', width: 100, hidden: true, dataIndex: 'proxyBenchmarkRateIndex.label', filter: {searchFieldName: 'proxyBenchmarkRateIndexLabel'}},
						{header: 'Rollup Aggregation Type', width: 50, hidden: true, dataIndex: 'rollupAggregationType'},
						{header: 'Inactive', dataIndex: 'inactive', width: 35, type: 'boolean'},
						{header: 'Active Client', dataIndex: 'client.active', width: 50, type: 'boolean', filter: {searchFieldName: 'clientActive'}, sortable: false}
					],
					editor: {
						detailPageClass: 'Clifton.investment.manager.AccountWindow',
						drillDownOnly: true
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to active
							this.setFilterValue('inactive', false);
							this.setFilterValue('client.active', true);
						}
						const params = {};
						const ca = TCG.getChildByName(this.getTopToolbar(), 'excludeClientAccountWorkflowStateId');
						if (TCG.isNotBlank(ca.getValue())) {
							params.excludeClientAccountWorkflowStateId = ca.getValue();
						}
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.categoryTableName = 'InvestmentManagerAccount';
							params.categoryHierarchyId = tag.getValue();
						}
						return params;
					}
				}]
			},


			{
				title: 'Manager Companies',
				items: [{
					name: 'businessCompanyListFind',
					xtype: 'gridpanel',
					instructions: 'The following manager companies are in the system',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Company Name', width: 100, dataIndex: 'name', filter: {searchFieldName: 'name'}, defaultSortColumn: true},
						{header: 'Expanded Name', width: 200, dataIndex: 'nameExpanded', hidden: true, filter: {searchFieldName: 'searchPattern'}},
						{header: 'Company Type', width: 100, dataIndex: 'type.label', filter: {type: 'combo', searchFieldName: 'companyTypeId', url: 'businessCompanyTypeListFind.json'}},
						{header: 'Alias', width: 100, dataIndex: 'alias', filter: {searchFieldName: 'alias'}},
						{header: 'Abbreviation', width: 50, dataIndex: 'abbreviation', filter: {searchFieldName: 'abbreviation'}},
						{header: 'City/State', width: 75, dataIndex: 'cityState'},
						{header: 'Address 1', width: 75, dataIndex: 'addressLine1', hidden: true},
						{header: 'Address 2', width: 75, dataIndex: 'addressLine2', hidden: true},
						{header: 'Address 3', width: 75, dataIndex: 'addressLine3', hidden: true},
						{header: 'City', width: 75, dataIndex: 'city', hidden: true},
						{header: 'State', width: 50, dataIndex: 'state', hidden: true},
						{header: 'Postal Code', width: 50, dataIndex: 'postalCode', hidden: true},
						{header: 'Country', width: 75, dataIndex: 'country'}
					],
					editor: {
						detailPageClass: 'Clifton.business.company.CompanyWindow',
						deleteURL: 'businessCompanyDelete.json',
						getDefaultData: function(gridPanel) { // defaults MONEY MANAGER for the detail page
							return {moneyManager: true};
						}
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Company Name', width: 150, xtype: 'toolbar-textfield', name: 'companyName'},
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Business Company Tags'}
						];
					},
					getLoadParams: function(firstLoad) {
						const params = {moneyManager: true};
						const companyName = TCG.getChildByName(this.getTopToolbar(), 'companyName');
						if (TCG.isNotBlank(companyName.getValue())) {
							params.searchPattern = companyName.getValue();
						}
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.categoryName = 'Business Company Tags';
							params.categoryHierarchyId = tag.getValue();
						}
						return params;
					}
				}]
			},


			{
				title: 'Manager Account Groups',
				items: [{
					xtype: 'gridpanel',
					name: 'investmentManagerAccountGroupListFind',
					instructions: 'A list of money manager account groups.  Each group can specify allocations of manager accounts and the target percentages of each allocations. Portfolio runs will generate rebalance warnings when manager balances cross the target percentages by the min/max rebalance targets.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Client Account', width: 100, dataIndex: 'investmentAccount.label', filter: {type: 'combo', searchFieldName: 'investmentAccountId', url: 'investmentAccountListFind.json?ourAccount=true'}, defaultSortColumn: true},
						{header: 'Group Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Inactive', width: 30, dataIndex: 'inactive', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.manager.AccountGroupWindow'
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to active
							this.setFilterValue('inactive', false);
						}
					}
				}]
			},


			{
				title: 'Adjustment Types',
				items: [{
					name: 'investmentManagerAccountBalanceAdjustmentTypeListFind',
					additionalPropertiesToRequest: 'cssStyle',
					xtype: 'gridpanel',
					instructions: 'The following manager account balance Adjustment Types are defined in the system.  System Defined Adjustment Types are read only and cannot be edited and are always processed prior to any non-system defined adjustments.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{
							header: 'Adjustment Name', width: 160, dataIndex: 'name', defaultSortColumn: true,
							renderer: function(v, c, r) {
								const style = r.json.cssStyle;
								if (TCG.isNotBlank(style)) {
									c.attr = 'style="' + style + '"';
								}
								return v;
							}
						},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'System Defined', width: 90, dataIndex: 'systemDefined', type: 'boolean'},
						{header: 'Cash', width: 50, dataIndex: 'cashAdjustmentAllowed', type: 'boolean'},
						{header: 'Securities', width: 70, dataIndex: 'securitiesAdjustmentAllowed', type: 'boolean'},
						{header: 'MOC', width: 50, dataIndex: 'marketOnClose', type: 'boolean'},
						{header: 'Note Required', width: 80, dataIndex: 'noteRequired', type: 'boolean'},
						{
							header: 'Next Day Adjustment', width: 160, dataIndex: 'nextDayAdjustmentType.name', filter: {searchFieldName: 'nextDayAdjustmentTypeName'},
							tooltip: 'Manager Balance adjustments are automatically copied to the following day\'s balance and applied as selected type.'
						}
					],
					editor: {
						detailPageClass: 'Clifton.investment.manager.AdjustmentTypeWindow'
					}
				}]
			},


			{
				title: 'Manager Custodian Accounts',
				items: [{
					name: 'investmentManagerCustodianAccountListFind',
					xtype: 'gridpanel',
					instructions: 'The following are custodian accounts that can be reused across multiple manager accounts.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Custodian Account Number', width: 60, dataIndex: 'number', defaultSortColumn: true},
						{header: 'Custodian Company', width: 100, dataIndex: 'company.name', filter: {type: 'combo', searchFieldName: 'companyId', displayField: 'label', url: 'businessCompanyListFind.json?companyType=Custodian'}},
						{header: 'Holding Account', width: 100, dataIndex: 'holdingAccount.label', filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'label', url: 'investmentAccountListFind.json?accountType=Custodian'}},
						{header: 'Base CCY', width: 50, dataIndex: 'baseCurrency.symbol', filter: {type: 'combo', searchFieldName: 'baseCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Custody Only', width: 50, dataIndex: 'custodyOnlyAccount', type: 'boolean', tooltip: 'Custody Only vs Full Service Account (does not include assets from other accounts)'},
						{header: 'Sweep Account', width: 50, dataIndex: 'sweepAccount', type: 'boolean', tooltip: 'A sweep account is a bank account that automatically transfers amounts that exceed, or fall short of, a certain level into a higher interest-earning investment option at the close of each business day. Commonly, the excess cash is swept into money market funds.'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.manager.CustodianAccountWindow'
					}
				}]
			}
		]
	}]
});
