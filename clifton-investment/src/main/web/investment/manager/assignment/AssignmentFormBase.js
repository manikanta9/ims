Clifton.investment.manager.assignment.AssignmentFormBase = Ext.extend(TCG.form.FormPanel, {
	labelWidth: 120,
	url: 'investmentManagerAccountAssignment.json',
	loadDefaultDataAfterRender: true,

	serviceProcessingType: 'OVERRIDE_ME',

	getWarningMessage: function(f) {
		return f.findField('inactive').getValue() ? 'This manager assignment is currently inactive.' : undefined;
	},

	initComponent: function() {
		const currentItems = [];

		if (Clifton.investment.manager.assignment.MANAGER_ASSIGNMENT_FORM_PROCESSING_TYPE_OVERRIDES
			&& Clifton.investment.manager.assignment.MANAGER_ASSIGNMENT_FORM_PROCESSING_TYPE_OVERRIDES[this.serviceProcessingType]) {
			Ext.apply(this, Clifton.investment.manager.assignment.MANAGER_ASSIGNMENT_FORM_PROCESSING_TYPE_OVERRIDES[this.serviceProcessingType]);
		}

		Ext.each(this.commonAssignmentItems, function(f) {
			currentItems.push(f);
		});
		Ext.each(this.serviceProcessingTypeItems, function(f) {
			currentItems.push(f);
		});
		this.items = currentItems;
		Clifton.investment.manager.assignment.AssignmentFormBase.superclass.initComponent.call(this);
	},
	listeners: {
		afterload: function(panel) {
			this.disableInactiveFields();
		}
	},
	disableInactiveFields: function() {
		// Override for Specific Fields/Sections to disable
	},
	commonAssignmentItems: [
		{
			xtype: 'fieldset',
			title: 'Account Info',
			items: [
				{name: 'referenceTwo.businessClient.id', xtype: 'hidden', submitValue: false},
				{
					fieldLabel: 'Client Account', name: 'referenceTwo.label', hiddenName: 'referenceTwo.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow',
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						queryEvent.combo.store.setBaseParam('clientIdOrRelatedClient', TCG.getValue('referenceTwo.businessClient.id', f.formValues));
						queryEvent.combo.store.setBaseParam('serviceProcessingType', f.serviceProcessingType);
					},
					getDefaultData: function(f) { // defaults client
						return {
							client: TCG.getValue('referenceOne.businessClient', f.formValues)
						};
					}
				},
				{
					fieldLabel: 'Manager Account', name: 'referenceOne.label', hiddenName: 'referenceOne.id', xtype: 'combo', displayField: 'label', url: 'investmentManagerAccountListFind.json', detailPageClass: 'Clifton.investment.manager.AccountWindow',
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						queryEvent.combo.store.setBaseParam('clientIdOrRelatedClient', TCG.getValue('referenceTwo.businessClient.id', f.formValues));
					},
					getDefaultData: function(f) { // defaults client
						return {
							client: TCG.getValue('referenceTwo.businessClient', f.formValues)
						};
					}
				},
				{fieldLabel: 'Display Name', name: 'displayName'},
				{
					boxLabel: 'Inactive', xtype: 'checkbox', name: 'inactive',
					listeners: {
						check: function(f) {
							const p = TCG.getParentFormPanel(f);
							p.disableInactiveFields();
						}
					}
				}
			]
		}
	],
	serviceProcessingTypeItems: []
});
Ext.reg('manager-assignment-form', Clifton.investment.manager.assignment.AssignmentFormBase);
