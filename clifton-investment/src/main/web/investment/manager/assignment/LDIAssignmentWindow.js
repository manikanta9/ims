TCG.use('Clifton.investment.manager.assignment.AssignmentFormBase');

Clifton.investment.manager.assignment.LDIAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Manager Account Assignment',
	iconCls: 'manager',
	height: 400,
	width: 675,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Account Details',
				items: [{
					xtype: 'manager-assignment-form',
					serviceProcessingType: 'LDI',

					disableInactiveFields: function() {
						this.enableDisableBenchmarkSecurityField();
					},
					enableDisableBenchmarkSecurityField: function() {
						const f = this.getForm();
						// Don't require benchmark if disabled, and don't allow selecting if Margin (not used)
						const margin = f.findField('cashType-Group').getValue().getGroupValue();
						const fld = f.findField('benchmarkSecurity.label');
						if (TCG.isEquals('MARGIN', margin)) {
							fld.allowBlank = true;
							fld.clearValue();
							fld.setDisabled(true);
						}
						else {
							fld.setDisabled(false);
							fld.allowBlank = f.findField('inactive').getValue();
						}
					},
					serviceProcessingTypeItems: [
						{xtype: 'label', html: '&nbsp;'},
						{
							xtype: 'radiogroup', columns: 3, fieldLabel: 'Cash Type', name: 'cashType-Group',
							items: [
								{boxLabel: 'Assets', name: 'cashType', inputValue: 'ASSETS', checked: true, qtip: 'Manager Assets are included in total assets for KRD Processing.'},
								{boxLabel: 'Liabilities', name: 'cashType', inputValue: 'LIABILITIES', qtip: 'Manager Liabilities are included in Liabilities DV01 processing which is used to generate targets per KRD Point.'},
								{boxLabel: 'Margin', name: 'cashType', inputValue: 'MARGIN', qtip: 'Margin managers are not processed in the run as part of Assets or Liabilities, but used only for Our Account Value during margin analysis'}
							],
							listeners: {
								change: function(f) {
									const p = TCG.getParentFormPanel(f);
									p.enableDisableBenchmarkSecurityField();
								}
							}
						},
						{
							fieldLabel: 'KRD Benchmark', name: 'benchmarkSecurity.label', hiddenName: 'benchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: false,
							beforequery: function(queryEvent) {
								if (TCG.isTrue(queryEvent.combo.getParentForm().getForm().findField('securitySearchAccount').checked)) {
									const accountId = queryEvent.combo.getParentForm().getForm().findField('referenceTwo.id').value;
									queryEvent.combo.store.baseParams = {
										investmentAccountId: accountId
									};
								}
								else {
									queryEvent.combo.store.baseParams = {};
								}
							}
						},
						{
							boxLabel: 'Limit to client account\'s specific securities (Leave unchecked to search for any)', xtype: 'checkbox', name: 'securitySearchAccount', submitValue: 'false',
							listeners: {
								check: function(f) {
									// On changes reset security drop down
									const p = TCG.getParentFormPanel(f);
									const bc = p.getForm().findField('benchmarkSecurity.label');
									bc.clearValue();
									bc.store.removeAll();
									bc.lastQuery = null;
								}
							}
						}
					]
				}]
			},


			{
				title: 'Audit Trail',
				items: [{
					xtype: 'system-audit-grid',
					tableName: 'InvestmentManagerAccountAssignment'
				}]
			}
		]
	}]
});
