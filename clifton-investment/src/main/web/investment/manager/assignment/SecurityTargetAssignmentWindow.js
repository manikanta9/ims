TCG.use('Clifton.investment.manager.assignment.AssignmentFormBase');

Clifton.investment.manager.assignment.SecurityTargetAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Manager Account Assignment',
	iconCls: 'manager',
	height: 400,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Account Details',
				items: [{
					xtype: 'manager-assignment-form',
					serviceProcessingType: 'SECURITY_TARGET'
				}]
			},


			{
				title: 'Audit Trail',
				items: [{
					xtype: 'system-audit-grid',
					tableName: 'InvestmentManagerAccountAssignment'
				}]
			}
		]
	}]
});
