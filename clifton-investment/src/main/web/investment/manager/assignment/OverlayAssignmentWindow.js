TCG.use('Clifton.investment.manager.assignment.AssignmentFormBase');

Clifton.investment.manager.assignment.OverlayAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Manager Account Assignment',
	iconCls: 'manager',
	height: 800,
	width: 1100,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Account Details',
				items: [{
					xtype: 'manager-assignment-form',
					childTables: 'InvestmentManagerAccountAllocation',
					serviceProcessingType: 'PORTFOLIO_RUNS',
					serviceProcessingTypeItems: [
						{
							xtype: 'formgrid',
							title: 'Asset Class Allocations for Manager Balance',
							storeRoot: 'allocationList',
							name: 'allocationFormGrid',
							dtoClass: 'com.clifton.investment.manager.InvestmentManagerAccountAllocation',
							addToolbarButtons: function(toolBar, grid) {
								toolBar.add('->');
								toolBar.add({boxLabel: 'Do Not Enforce 100% Allocation', name: 'allowPartialAllocation', xtype: 'checkbox'});
							},
							columnsConfig: [
								{
									header: 'Asset Class', width: 400, dataIndex: 'assetClass.name', idDataIndex: 'assetClass.id',
									editor: {
										xtype: 'combo', allowBlank: false, url: 'investmentAssetClassListFind.json',
										beforequery: function(queryEvent) {
											const grid = queryEvent.combo.gridEditor.containerGrid;
											const f = TCG.getParentFormPanel(grid).getForm();
											const s = queryEvent.combo.store;
											s.setBaseParam('accountId', f.findField('referenceTwo.id').value);
										}
									}
								},
								{
									header: 'Percentage', width: 150, dataIndex: 'allocationPercent', type: 'percent', numberFormat: '0,000.0000000000', summaryType: 'sum', editor: {xtype: 'currencyfield', decimalPrecision: 10, allowBlank: false},
									tooltip: 'Manager Asset Class Allocation'
								},
								{
									header: 'Private', width: 60, type: 'boolean', dataIndex: 'privateAssignment', editor: {xtype: 'checkbox'},
									tooltip: 'Included in all calculations for Portfolio runs, but excluded in the client facing Portfolio report.'
								}
							],
							plugins: {ptype: 'gridsummary'}
						},
						{xtype: 'label', html: '&nbsp;'},
						{
							xtype: 'fieldset-checkbox',
							title: 'Default Manager Cash Balance Overlay Rules',
							checkboxName: 'overlayCash',
							instructions: 'Default Rules for overlaying manager cash.  All custom rules will be applied first.  If no default option selected any remaining cash after custom rules are applied will be ignored. For default rules, select the "cash bucket" that overlay manager cash should be put into and how that cash should be overlaid.',
							labelWidth: 1,
							items: [
								{
									xtype: 'radiogroup', columns: 3, fieldLabel: '', name: 'cashType-Group',
									qtip: 'When overlaying manager cash according to selections below, this is the "cash bucket" that that money is put into. Can be overridden using the Custom Cash Overlay selection below.',
									items: [
										{boxLabel: 'Manager', name: 'cashType', inputValue: 'MANAGER', checked: true},
										{boxLabel: 'Fund', name: 'cashType', inputValue: 'FUND'},
										{boxLabel: 'Transition', name: 'cashType', inputValue: 'TRANSITION'}
									]
								},
								{xtype: 'label', html: '<hr/>'},
								{
									xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'cashOverlayType-Group',
									items: [
										{boxLabel: 'Do not overlay manager cash balance', name: 'cashOverlayType', inputValue: 'NONE', checked: true},
										{boxLabel: 'Overlay manager cash balance using Fund Cash allocation weights', name: 'cashOverlayType', inputValue: 'FUND'},
										{boxLabel: 'Overlay manager cash balance using manager\'s asset class allocation', name: 'cashOverlayType', inputValue: 'MANAGER'},
										{boxLabel: 'Allocate manager cash to minimize imbalances', qtip: 'Manager cash remains in "cash bucket" selected above, however its allocated across asset classes based on the account\'s minimize imbalances calculation type. <br/><br/>See Rebalancing tab on the account to select a minimize imbalances calculator', name: 'cashOverlayType', inputValue: 'MINIMIZE_IMBALANCES'}
									]
								}
							]
						},
						{xtype: 'label', html: '&nbsp;'},
						{
							xtype: 'fieldset-checkbox',
							title: 'Custom Manager Cash Balance Overlay Rules',
							checkboxName: 'overlayCashCustom',
							instructions: 'Custom Rules for overlaying manager cash.  All custom rules will be applied first.  If no default option selected any remaining cash after custom rules are applied will be ignored.',
							labelWidth: 1,
							items: [
								{
									xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'customCashOverlayType-Group',
									items: [
										{boxLabel: 'Do not apply custom rules', name: 'customCashOverlayType', inputValue: 'NONE', checked: true},
										{boxLabel: 'Overlay manager cash balance using the following custom asset class allocation (%):', name: 'customCashOverlayType', inputValue: 'CUSTOM_PERCENT'},
										{boxLabel: 'Overlay manager cash balance using the following custom asset class allocation (Base CCY):', name: 'customCashOverlayType', inputValue: 'CUSTOM_AMOUNT'}
									]
								},
								{
									xtype: 'fieldset-radio',
									groupName: 'customCashOverlayType-Group',
									radioName: 'customCashOverlayType',
									expandValue: ['CUSTOM_PERCENT', 'CUSTOM_AMOUNT'],
									anchor: '0',
									bodyStyle: 'padding-left: 30px',
									onCheckClick: function() {
										TCG.form.FieldSetRadio.prototype.onCheckClick.call(this, arguments);

										const f = this.getParentForm().getForm();
										const radioField = f.findField(this.radioName);
										const radioValue = (this.groupName ? radioField.getGroupValue() : radioField.getValue());
										const fg = TCG.getChildByName(this, 'customCashFormGrid');
										const valueIndex = fg.getColumnModel().findColumnIndex('customAllocationValue');
										const valueColumn = fg.getColumnModel().getColumnAt(valueIndex);
										const valueField = fg.getColumnModel().getCellEditor(valueIndex).field;

										if (TCG.isEquals('CUSTOM_PERCENT', radioValue)) {
											fg.getColumnModel().setColumnHeader(valueIndex, 'Percentage');
											valueColumn.renderer = function(value, metaData, record) {
												return (value == null || value === '') ? '' : Ext.util.Format.number(value, '0,000.0000000000 %');
											};
											valueField.decimalPrecision = 10;
										}
										else {
											fg.getColumnModel().setColumnHeader(valueIndex, 'Amount');
											valueColumn.renderer = function(value, metaData, record) {
												return (value == null || value === '') ? '' : Ext.util.Format.number(value, '0,000.00');
											};
											valueField.decimalPrecision = 2;
										}
									},
									items: [
										{
											xtype: 'formgrid',
											name: 'customCashFormGrid',
											anchor: '0',
											storeRoot: 'customCashOverlayAllocationList',
											dtoClass: 'com.clifton.investment.manager.InvestmentManagerAccountAllocation',
											columnsConfig: [
												{
													header: 'Asset Class', width: 175, dataIndex: 'assetClass.name', idDataIndex: 'assetClass.id',
													editor: {
														xtype: 'combo', url: 'investmentAssetClassListFind.json',
														beforequery: function(queryEvent) {
															const grid = queryEvent.combo.gridEditor.containerGrid;
															const f = TCG.getParentFormPanel(grid).getForm();
															queryEvent.combo.store.baseParams = {accountId: TCG.getValue('referenceTwo.id', f.formValues)};
														}

													}
												},
												{
													header: 'Cash Type', width: 100, dataIndex: 'cashTypeLabel', idDataIndex: 'cashType',
													editor: {
														displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', allowBlank: false,
														store: new Ext.data.ArrayStore({
															fields: ['name', 'value'],
															data: [['Manager', 'MANAGER'], ['Fund', 'FUND'], ['Transition', 'TRANSITION']]
														})
													}
												},
												{
													header: 'Custom Amount', width: 100, dataIndex: 'customAllocationValue', type: 'currency', numberFormat: '0,000.0000000000', summaryType: 'sum', editor: {xtype: 'currencyfield', decimalPrecision: 10, allowBlank: false}

												},
												{header: 'Custom Update Date', width: 75, dataIndex: 'customAllocationValueDate', type: 'date', editor: {xtype: 'datefield', allowBlank: false}},
												{
													header: 'Private', width: 60, type: 'boolean', dataIndex: 'privateAssignment', editor: {xtype: 'checkbox'},
													tooltip: 'Included in all calculations for Portfolio runs, but excluded in the client facing Portfolio report.'
												},
												{header: 'Apply Benchmark Adjustment', width: 75, dataIndex: 'applyBenchmarkAdjustment', type: 'boolean', editor: {xtype: 'checkbox'}, tooltip: 'Adjusts the custom value based on the performance of the selected benchmark (uses asset class benchmark if not explicitly defined) as of the Custom Update Date'},
												{header: 'Benchmark', width: 120, dataIndex: 'benchmarkSecurity.label', idDataIndex: 'benchmarkSecurity.id', editor: {xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'}},
												{
													header: 'Benchmark Adjustment Offset Cash Overlay Type', width: 100, dataIndex: 'benchmarkAdjustmentOffSetCashOverlayType',
													tooltip: 'When applying a benchmark adjustment the offset from the adjustment can be applied specifically to Fund Cash or None which means to use the default rules (if defined).',
													editor: {
														displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
														store: new Ext.data.ArrayStore({
															fields: ['name', 'value', 'description'],
															// INVESTMENT-350: Only realistic option would be Fund Cash - so using Fund Cash or None (which falls back to default rules)
															data: [['NONE', 'NONE', 'No custom rules for the offset.  Will be applied using default rules (if defined).'], ['FUND', 'FUND', 'Overlay benchmark adjustment offset to Fund Cash using Fund Cash Weights.']]
														})
													}
												},
												{header: 'Note', width: 200, dataIndex: 'note', editor: {xtype: 'textfield'}}
											],
											plugins: {ptype: 'gridsummary'}
										},


										{xtype: 'sectionheaderfield', header: 'Custom Cash Allocation (Define where the manager cash is allocated)'},
										{
											xtype: 'radiogroup', columns: 1, name: 'customCashAllocationType', allowBlank: false, fieldLabel: '',
											items: [
												{boxLabel: 'Allocate Cash to Manager Asset Classes', xtype: 'radio', name: 'customCashAllocationType', inputValue: 'MANAGER_ASSET_CLASSES', checked: true},
												{boxLabel: 'Allocate Cash to Custom Cash Asset Classes Only (anything left over is ignored)', xtype: 'radio', name: 'customCashAllocationType', inputValue: 'CUSTOM_CASH_ASSET_CLASSES'},
												{boxLabel: 'Allocate Cash to Custom Asset Classes and Remainder to Manager Asset Classes', xtype: 'radio', name: 'customCashAllocationType', inputValue: 'CUSTOM_CASH_ASSET_CLASSES_APPLY_REMAINDER'}
											]
										}
									]
								}
							]
						},
						{
							xtype: 'fieldset-checkbox',
							title: 'Securities Balance Replication Allocation',
							checkboxName: 'allocateSecuritiesBalanceToReplications',
							instructions: 'When a securities balance is present and an allocator is selected, the portfolio run will take the securities balance allocated to its asset class(es) and apply that amount to the replications <b>additional</b> exposure.  How that allocation is calculated is based on the following selection.<br/><b>NOTE:</b> This amount is also added to the asset class total target for replications, but removed from individual targets when calculating target contracts.',
							labelWidth: 1,
							items: [
								{
									xtype: 'combo', name: 'securitiesBalanceReplicationAllocatorBean.name', hiddenName: 'securitiesBalanceReplicationAllocatorBean.id',
									url: 'systemBeanListFind.json?groupName=Manager Assignment - Securities Balance Replication Allocator',
									detailPageClass: 'Clifton.system.bean.BeanWindow',
									getDefaultData: function() {
										return {type: {group: {name: 'Manager Assignment - Securities Balance Replication Allocator'}}};
									}
								}
							]
						},
						{
							xtype: 'fieldset-checkbox',
							title: 'Rebalancing Configuration',
							checkboxName: 'targetAllocationExists',
							instructions: 'All rebalancing targets should be entered as positive values.  Even though we cannot rebalance at manager level, some clients want to establish targets for specific managers and do physical rebalancing (performed by client; not us) when actual manager allocation deviates from the target more than the threshold. A warning is generated when manager target is outside of a threshold.',
							items: [
								{fieldLabel: 'Target Exposure %', xtype: 'currencyfield', name: 'targetAllocationPercent'},
								{fieldLabel: 'Minimum %', name: 'rebalanceAllocationMin', minValue: 0, xtype: 'currencyfield', requiredFields: ['targetAllocationPercent']},
								{fieldLabel: 'Maximum %', name: 'rebalanceAllocationMax', minValue: 0, xtype: 'currencyfield', requiredFields: ['targetAllocationPercent']},
								{boxLabel: 'Apply Values as Absolute Bands (if left unchecked rebalance triggers will be calculated Proportionally)', name: 'rebalanceAllocationFixed', xtype: 'checkbox', requiredFields: ['targetAllocationPercent']}
							]
						}
					]
				}]
			},


			{
				title: 'Overlay Actions',
				items: [{
					name: 'investmentManagerAccountAssignmentOverlayActionListFind',
					xtype: 'gridpanel',
					instructions: 'The following overlay actions have been set up for this manager assignment.  Only one of each type can be active for a manager assignment and specified date range.',
					getLoadParams: function() {
						return {managerAccountAssignmentId: this.getWindow().getMainForm().formValues.id};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type', width: 70, dataIndex: 'overlayActionType'},
						{header: 'Amount', width: 50, dataIndex: 'overlayActionAmount', type: 'currency', useNull: true},
						{header: 'Percent', width: 50, dataIndex: 'overlayActionPercent', type: 'percent', useNull: true},
						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'},
						{
							header: 'Apply Difference to Securities', width: 75, dataIndex: 'applyDifferenceToSecurities', type: 'boolean',
							tooltip: 'If checked, the adjustment made to overlay cash from cash balance is applied to manager securities balance. '
						},
						{header: 'Start Date', width: 50, dataIndex: 'startDate'},
						{header: 'End Date', width: 50, dataIndex: 'endDate'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.manager.assignment.AssignmentOverlayActionWindow',
						getDefaultData: function(gridPanel) { // defaults client id for the detail page
							return {
								managerAccountAssignment: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]

			}
		]
	}]
});
