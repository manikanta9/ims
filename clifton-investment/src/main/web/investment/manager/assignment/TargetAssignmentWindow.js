TCG.use('Clifton.investment.manager.assignment.AssignmentFormBase');

Clifton.investment.manager.assignment.TargetAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Manager Account Assignment',
	iconCls: 'manager',
	height: 400,
	width: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Account Details',
				items: [{
					xtype: 'manager-assignment-form',
					serviceProcessingType: 'PORTFOLIO_TARGET'
				}]
			},


			{
				title: 'Audit Trail',
				items: [{
					xtype: 'system-audit-grid',
					tableName: 'InvestmentManagerAccountAssignment'
				}]
			}
		]
	}]
});
