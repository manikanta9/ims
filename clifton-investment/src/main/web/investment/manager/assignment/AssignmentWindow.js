// not the actual window but a window selector based on company type
Clifton.investment.manager.assignment.AssignmentWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'investmentManagerAccountAssignment.json',

	getClassName: function(config, entity) {
		// Use Overlay by default for now
		let className = 'Clifton.investment.manager.assignment.OverlayAssignmentWindow';
		let type;
		// Clifton Account Service Processing Type Different Windows
		if (entity && entity.referenceTwo.serviceProcessingType && entity.referenceTwo.serviceProcessingType) {
			type = entity.referenceTwo.serviceProcessingType.processingType;
		}
		else {
			type = config.defaultData.referenceTwo.serviceProcessingType.processingType;
		}
		if (type) {
			if (type === 'LDI') {
				className = 'Clifton.investment.manager.assignment.LDIAssignmentWindow';
			}
			else if (type === 'DURATION_MANAGEMENT') {
				className = 'Clifton.investment.manager.assignment.DurationManagementAssignmentWindow';
			}
			else if (type === 'SECURITY_TARGET') {
				className = 'Clifton.investment.manager.assignment.SecurityTargetAssignmentWindow';
			}
			else if (type === 'PORTFOLIO_TARGET') {
				className = 'Clifton.investment.manager.assignment.TargetAssignmentWindow';
			}
		}
		return className;
	},

	useEntityAsDefaultData: function(className, config, entity) {
		return TCG.isNotNull(entity) && className !== 'Clifton.investment.manager.assignment.TargetAssignmentWindow';
	}
});
