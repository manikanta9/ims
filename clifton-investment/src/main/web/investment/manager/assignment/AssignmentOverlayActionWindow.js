Clifton.investment.manager.assignment.AssignmentOverlayActionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Manager Assignment Overlay Action',
	iconCls: 'manager',
	width: 650,
	height: 400,

	items: [{
		xtype: 'formpanel',
		instructions: 'Manager Assignment Overlay Actions allow applying changes to the cash amount to be overlaid for specified time period.  These changes can come from different action types, such as adjustments and/or limits.',
		url: 'investmentManagerAccountAssignmentOverlayAction.json',
		labelWidth: 150,
		loadDefaultDataAfterRender: true,
		listeners: {
			afterload: function(panel) {
				this.setResetCashOverlayActionFields();
			}
		},
		setResetCashOverlayActionFields: function() {
			const f = this.getForm();
			const actionType = f.findField('overlayActionType').getGroupValue();

			const instruct = TCG.getChildByName(this, 'inputInstructions');
			const amountField = f.findField('overlayActionAmount');
			const percentField = f.findField('overlayActionPercent');
			const securitiesCheckbox = f.findField('applyDifferenceToSecurities');

			if (actionType === 'ADJUSTMENT') {
				instruct.setText(this.adjustmentInstructions, false);
				amountField.setFieldLabel('Amount:');
				amountField.qtip = 'Static adjustment amount to add/subtract from overlay cash amount';
				amountField.minValue = null;

				percentField.setFieldLabel('Percent of TMV:');
				percentField.qtip = 'Percentage of Total Market Value.  Enter -1.00 to subtract 1% of total market value from the cash that is overlaid.';
				percentField.minValue = null;
				percentField.maxValue = null;

				securitiesCheckbox.setBoxLabel('Apply difference of cash overlay adjustment value to manager securities balance');
			}
			else {
				instruct.setText(this.limitInstructions, false);

				amountField.setFieldLabel('Max Cash Amount:');
				amountField.qtip = '';
				amountField.minValue = 0;

				percentField.setFieldLabel('Max Cash Percent:');
				percentField.qtip = '';
				percentField.minValue = 0;
				percentField.maxValue = 100;

				securitiesCheckbox.setBoxLabel('Add cash in excess of the specified limit to manager securities balance');
			}
			this.doLayout();
		},

		adjustmentInstructions: 'Cash Overlay Adjustment is a value (or percent of total market value) that can be added/subtracted to the cash value to be overlaid.  This adjustment is applied prior to any maximum limits entered. The difference can optionally be applied to the securities balance.',
		limitInstructions: 'Max Cash Percent</b> is the maximum % of the manager\'s balance that will be overlaid.  Max Cash Amount is the maximum dollar amount that will be overlaid for the manager.  When both are supplied the lower limit will be applied.  Cash in excess of the limit can optionally be applied to the securities balance.',

		items: [
			{fieldLabel: 'Manager Assignment', name: 'managerAccountAssignment.label', xtype: 'linkfield', detailIdField: 'managerAccountAssignment.id', detailPageClass: 'Clifton.investment.manager.assignment.AssignmentWindow'},
			{xtype: 'label', html: '<hr/>'},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: 'Action', name: 'overlayActionType-Group',
				items: [
					{boxLabel: 'Apply Manager Cash Overlay Adjustment', name: 'overlayActionType', inputValue: 'ADJUSTMENT'},
					{boxLabel: 'Apply Manager Cash Overlay Limit', name: 'overlayActionType', inputValue: 'LIMIT'}
				],
				listeners: {
					change: function(f) {
						const p = TCG.getParentFormPanel(f);
						p.setResetCashOverlayActionFields();
					}
				}
			},
			{xtype: 'label', name: 'inputInstructions', html: ''},
			{fieldLabel: 'Amount', name: 'overlayActionAmount', xtype: 'currencyfield'},
			{fieldLabel: 'Percent', name: 'overlayActionPercent', xtype: 'floatfield'},
			{boxLabel: 'Apply difference to manager securities balance', name: 'applyDifferenceToSecurities', xtype: 'checkbox'},
			{xtype: 'label', html: '<hr/>'},
			{
				fieldLabel: 'Start Date', xtype: 'container', layout: 'hbox',
				items: [
					{name: 'startDate', xtype: 'datefield', flex: 1},
					{value: '&nbsp;&nbsp;&nbsp;&nbsp;End Date:', xtype: 'displayfield', width: 165},
					{name: 'endDate', xtype: 'datefield', flex: 1}
				]
			}
		]
	}]
});
