TCG.use('Clifton.investment.manager.assignment.AssignmentFormBase');

Clifton.investment.manager.assignment.DurationManagementAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Manager Account Assignment',
	iconCls: 'manager',
	height: 400,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Account Details',
				items: [{
					xtype: 'manager-assignment-form',
					serviceProcessingType: 'DURATION_MANAGEMENT',
					serviceProcessingTypeItems: [
						{xtype: 'label', html: '&nbsp;'},
						{fieldLabel: 'Benchmark', name: 'benchmarkSecurity.label', hiddenName: 'benchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: false}
					]
				}]
			},


			{
				title: 'Audit Trail',
				items: [{
					xtype: 'system-audit-grid',
					tableName: 'InvestmentManagerAccountAssignment'
				}]
			}
		]
	}]
});
