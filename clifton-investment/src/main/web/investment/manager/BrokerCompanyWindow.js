TCG.use('Clifton.business.company.BasicCompanyWindow');

// adds "Delivery Instructions" tab at the end of BasicCompanyWindow
Clifton.investment.manager.BrokerCompanyWindow = Ext.extend(Clifton.business.company.BasicCompanyWindow, {
	windowOnShow: function(w) {
		Clifton.investment.manager.BrokerCompanyWindow.superclass.windowOnShow.apply(this, arguments);
		const tabs = w.items.get(0).items.get(0);
		tabs.add(this.deliveryInstructionsTab);
	},

	deliveryInstructionsTab: {
		title: 'Delivery Instructions',
		items: [{
			xtype: 'gridpanel',
			appendStandardColumns: false,
			name: 'investmentInstructionDeliveryBusinessCompanyListFind',
			instructions: 'Delivery instructions for wire transfers for each bank account used by this broker.',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Instruction Name', width: 100, dataIndex: 'referenceOne.name'},
				{header: 'Description', width: 100, dataIndex: 'referenceOne.description', hidden: true},
				{header: 'Bank', width: 100, dataIndex: 'referenceOne.deliveryCompany.name', filter: {type: 'combo', searchFieldName: 'deliveryCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
				{header: 'Currency', width: 30, dataIndex: 'deliveryCurrency.symbol', filter: {type: 'combo', searchFieldName: 'deliveryCurrencyId', url: 'investmentSecurityListFind.json?currency=true'}},
				{header: 'Delivery Type', width: 50, dataIndex: 'investmentInstructionDeliveryType.name', filter: {type: 'combo', searchFieldName: 'typeId', loadAll: true, url: 'investmentInstructionDeliveryTypeListFind.json'}},
				{header: 'Account Number', width: 60, dataIndex: 'referenceOne.deliveryAccountNumber'},
				{header: 'Account Name', width: 100, dataIndex: 'referenceOne.deliveryAccountName', hidden: true},
				{header: 'ABA', width: 50, dataIndex: 'referenceOne.deliveryABA'}
			],
			editor: {
				detailPageClass: 'Clifton.investment.instruction.delivery.InstructionInstructionDeliveryBusinessCompanyWindow',
				getDefaultData: function(gridPanel) {
					return {
						referenceTwo: gridPanel.getWindow().getMainForm().formValues
					};
				}
			},
			getLoadParams: function(firstLoad) {
				return {businessCompanyId: this.getWindow().getMainFormId()};
			}
		}]
	}
});
