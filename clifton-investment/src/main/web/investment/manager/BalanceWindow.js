// not the actual window but a window selector based on overrides
Clifton.investment.manager.BalanceWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'investmentManagerAccountBalance.json',

	getClassName: function(config, entity) {
		return (Clifton.investment.manager.BalanceWindowOverride);
	}
});
