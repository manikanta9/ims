Clifton.investment.manager.StaleBalanceDetailListWindow = Ext.extend(TCG.app.Window, {
	title: 'Stale Manager Balances',
	iconCls: 'manager',
	width: 900,
	items: [{
		name: 'investmentManagerAccountBalanceStaleDetailListFind',
		xtype: 'gridpanel',
		instructions: 'List of all stale manage accounts.  If this was opened from the summary grid, then it will be filter for that custodian.',
		getTopToolbarFilters: function(toolbar) {
			return [
				{fieldLabel: 'Custodian', xtype: 'combo', name: 'custodianCompanyId', width: 180, url: 'businessCompanyListFind.json?companyType=Custodian', linkedFilter: 'managerAccount.custodianLabel'},
				{fieldLabel: 'Balance Date', xtype: 'toolbar-datefield', name: 'balanceDate'}
			];
		},

		getLoadParams: function(firstLoad) {
			// default to yesterday
			const t = this.getTopToolbar();
			const win = this.getWindow();
			const bd = TCG.getChildByName(t, 'balanceDate');
			const dd = win.defaultData;
			let dateValue = dd && dd.balanceDate ? dd.balanceDate : null;
			if (dateValue && firstLoad) {
				bd.setValue(dateValue);
			}
			else {
				if (bd.getValue() !== '') {
					dateValue = (bd.getValue()).format('m/d/Y');
				}
				else {
					const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
					dateValue = prevBD.format('m/d/Y');
					bd.setValue(dateValue);
				}
			}
			if (firstLoad) {
				// default to active
				this.setFilterValue('adjustedDifference', {'lt': 1, 'gt': -1});
				this.setFilterValue('adjustedCashValue', {'gt': 0});
			}
			return Ext.apply(dd, {
				balanceDate: dateValue,
				proxyManager: false,
				managerType: 'STANDARD',
				requestedMaxDepth: 4
			});
		},
		pageSize: 200,
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{header: 'Client', width: 150, dataIndex: 'managerAccount.client.name', filter: {type: 'combo', searchFieldName: 'clientId', url: 'businessClientListFind.json'}},
			{header: 'Manager', width: 150, dataIndex: 'managerAccount.managerCompany.name', filter: {searchFieldName: 'managerCompanyName'}},
			{header: 'Account', width: 70, dataIndex: 'managerAccount.labelShort', filter: {searchFieldName: 'accountNameNumber'}},
			{header: 'Custodial', width: 100, dataIndex: 'managerAccount.custodianLabel', filter: {type: 'combo', searchFieldName: 'custodianCompanyId', displayField: 'label', url: 'businessCompanyListFind.json?companyType=Custodian'}},
			{
				header: 'Type', dataIndex: 'managerAccount.managerType', width: 60, filter: false,
				renderer: function(v, p, r) {
					if (v === 'LINKED') {
						let qtip = '<table><tr><td>' + r.data['managerAccount.linkedManagerTypeLabel'] + '</td></tr>';
						if (r.data['managerAccount.linkedManagerProcessingDependent'] === true) {
							qtip += '<tr><td>* Portfolio Processing Dependent</td></tr>';
						}
						qtip += '</table>';
						p.attr = 'qtip=\'' + qtip + '\'';
						p.css = 'amountAdjusted';
					}
					return v;
				}
			},

			// Adjusted Balance Info
			{
				header: 'Cash', width: 90, dataIndex: 'adjustedCashValue', type: 'currency',
				renderer: function(v, p, r) {
					return TCG.renderAdjustedAmount(v, r.data['cashAdjustment'], r.data['cashValue']);
				}
			},
			{
				header: 'Securities', width: 90, dataIndex: 'adjustedSecuritiesValue', type: 'currency',
				renderer: function(v, p, r) {
					return TCG.renderAdjustedAmount(v, r.data['securitiesAdjustment'], r.data['securitiesValue']);
				}
			},
			{
				header: 'Total', width: 90, dataIndex: 'adjustedTotalValue', type: 'currency', filter: false, sortable: false,
				renderer: function(v, p, r) {
					return TCG.renderAdjustedAmount(v, r.data['cashAdjustment'] || r.data['securitiesAdjustment'], r.data['totalValue']);
				}
			},
			{
				header: 'Difference', width: 90, dataIndex: 'adjustedDifference', type: 'currency', sortable: false, filter: {orEquals: true},
				renderer: function(v, p, r) {
					return TCG.renderAdjustedAmount(v, r.data['cashAdjustment'] || r.data['securitiesAdjustment'], r.data['difference']);
				}
			}
		],
		editor: {
			detailPageClass: 'Clifton.investment.manager.BalanceWindow',
			drillDownOnly: true
		}
	}]
});
