Clifton.investment.manager.RollupListWindow = Ext.extend(TCG.app.Window, {
	title: 'Rollup Manager Allocation Quick Entry',
	iconCls: 'manager',
	width: 800,
	items: [{
		xtype: 'gridpanel',
		name: 'investmentManagerAccountRollupListFind',
		rowSelectionModel: 'checkbox',
		instructions: 'Enter a percentage in the toolbar and select the rollups to update and click Apply to update their allocation percentages.',
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{header: 'ParentID', width: 15, dataIndex: 'referenceOne.id', hidden: true},
			{header: 'Rollup Manager', width: 150, dataIndex: 'referenceOne.label', filter: {searchFieldName: 'parentManagerAccountNumberName'}},
			{header: 'Child Manager', width: 150, dataIndex: 'referenceTwo.label', filter: {searchFieldName: 'childManagerAccountNumberName'}},
			{header: 'Percentage', width: 50, dataIndex: 'allocationPercent', type: 'percent', numberFormat: '0,000.0000000000'},
			{header: 'Inactive', dataIndex: 'referenceOne.inactive', width: 30, type: 'boolean', filter: {searchFieldName: 'inactive'}}
		],
		getTopToolbarFilters: function(toolbar) {
			const gp = this;
			return [
				{fieldLabel: 'Percentage', xtype: 'currencyfield', name: 'applyAllocation', width: 100},
				{
					text: 'Apply',
					xtype: 'button',
					tooltip: 'Applies percentage to all selected rollups.',
					iconCls: 'run',
					scope: this,
					handler: function() {
						gp.executeSave();
					}
				}
			];
		},

		executeSave: function() {
			const gp = this;
			const tb = gp.getTopToolbar();
			const amt = TCG.getChildByName(tb, 'applyAllocation').getValue();

			if (!amt || amt === '') {
				TCG.showError('Please enter a percentage in the toolbar to apply to selected rollups.', 'Validation');
				return;
			}
			const sm = this.grid.getSelectionModel();
			if (sm.getCount() === 0) {
				TCG.showError('Please select at least one rollup to update.', 'No Rollups(s) Selected');
			}
			else {
				const ut = sm.getSelections();
				gp.applyRollupAllocations(ut, gp, amt);
			}
		},
		applyRollupAllocations: function(rollups, gridPanel, amt) {
			const rollupIds = [];
			for (let i = 0; i < rollups.length; i++) {
				rollupIds[i] = rollups[i].id;
			}
			const loader = new TCG.data.JsonLoader({
				waitMsg: 'Updating Allocation Percentages...',
				waitTarget: this,
				params: {
					rollupIds: rollupIds,
					allocationPercent: amt
				},
				onFailure: function() {
					// If it failed, reload to current status
					gridPanel.reload();
				},
				onLoad: function(record, conf) {
					gridPanel.reload();
				}
			});
			loader.load('investmentManagerAccountRollupPercentageListSave.json');
		},

		editor: {
			detailPageClass: 'Clifton.investment.manager.AccountWindow',
			getDetailPageId: function(gridPanel, row) {
				return row.json.referenceOne.id;
			},
			getDefaultData: function(gridPanel) { // defaults client for the detail page
				return {
					client: gridPanel.getWindow().defaultData.client
				};
			},
			deleteEnabled: false
		},
		getLoadParams: function(firstLoad) {
			const dd = this.getWindow().defaultData;

			if (firstLoad) {
				// default to active
				this.setFilterValue('referenceOne.inactive', false);
			}
			return {clientId: dd.client.id};
		}
	}]
});
