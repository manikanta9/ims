Clifton.investment.manager.AccountGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Manager Account Group',
	iconCls: 'manager',
	width: 770,
	height: 570,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		instructions: 'A money manager account group can specify allocations of manager accounts and the target percentages of each allocations. Portfolio runs will generate rebalance warnings when manager balances cross the target percentages by the min/max rebalance targets.',
		url: 'investmentManagerAccountGroup.json',

		items: [
			{xtype: 'hidden', name: 'investmentAccount.businessClient.id', submitValue: false},
			{
				fieldLabel: 'Client Account', hiddenName: 'investmentAccount.id', name: 'investmentAccount.label', xtype: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true',
				requestedProps: 'businessClient.id',
				listeners: {
					select: function(combo, record, index) {
						const client = record.json.businessClient;
						const fp = combo.getParentForm();
						const f = fp.getForm();
						f.findField('investmentAccount.businessClient.id').setValue(client.id);
					}
				}
			},
			{fieldLabel: 'Group Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},
			{fieldLabel: '', boxLabel: 'Inactive', name: 'inactive', xtype: 'checkbox'},
			{
				xtype: 'formgrid',
				title: 'Allocations',
				storeRoot: 'allocationList',
				dtoClass: 'com.clifton.investment.manager.InvestmentManagerAccountGroupAllocation',
				instructions: 'Group allocations allow you to define the target percentage a specific manager account balance should be, as well as rebalance min/max percentages.  Rebalance percentages will generate warnings during Portfolio runs if a manager balance crosses the trigger.  Rebalance percentages that are not fixed will be calculated proportionally.',
				columnsConfig: [
					{
						header: 'Manager Account', width: 225, dataIndex: 'referenceTwo.label', idDataIndex: 'referenceTwo.id',
						editor: {
							xtype: 'combo', url: 'investmentManagerAccountListFind.json', displayField: 'label',
							beforequery: function(queryEvent) {
								const grid = queryEvent.combo.gridEditor.containerGrid;
								const fp = TCG.getParentFormPanel(grid);
								queryEvent.combo.store.baseParams = {clientIdOrRelatedClient: fp.getFormValue('investmentAccount.businessClient.id', true)};
							}
						}
					},
					{header: 'Target %', width: 100, dataIndex: 'targetAllocation', type: 'currency', editor: {xtype: 'currencyfield', allowBlank: false}, summaryType: 'sum'},
					{header: 'Fixed Rebalance %', width: 110, dataIndex: 'rebalanceAllocationFixed', type: 'boolean', editor: {xtype: 'checkbox'}},
					{header: 'Min Rebalance Trigger', width: 110, dataIndex: 'rebalanceAllocationMin', type: 'currency', editor: {xtype: 'currencyfield', allowBlank: false, minValue: 0}},
					{header: 'Max Rebalance Trigger', width: 110, dataIndex: 'rebalanceAllocationMax', type: 'currency', editor: {xtype: 'currencyfield', allowBlank: false, minValue: 0}}
				],
				plugins: {ptype: 'gridsummary'}
			}
		]
	}]
});
