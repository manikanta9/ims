package com.clifton.investment.api.account;

import com.clifton.investment.shared.ClientAccount;
import com.clifton.investment.shared.HoldingAccount;


/**
 * @author vgomelsky
 */
public interface InvestmentAccountApiService {

	public ClientAccount getClientAccount(int id);


	public HoldingAccount getHoldingAccount(int id);
}
