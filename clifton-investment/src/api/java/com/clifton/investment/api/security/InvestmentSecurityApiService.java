package com.clifton.investment.api.security;

import com.clifton.investment.shared.Security;


/**
 * @author vgomelsky
 */
public interface InvestmentSecurityApiService {

	public Security getCurrencyBySymbol(String currencySymbol);
}
