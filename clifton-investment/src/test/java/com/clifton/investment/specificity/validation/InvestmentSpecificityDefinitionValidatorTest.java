package com.clifton.investment.specificity.validation;

import com.clifton.core.context.ThreadLocalContextHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.specificity.BaseInvestmentSpecificityBean;
import com.clifton.investment.specificity.InvestmentSpecificityDefinition;
import com.clifton.investment.specificity.InvestmentSpecificityField;
import com.clifton.investment.specificity.InvestmentSpecificityService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnStandard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;


/**
 * Test class for {@link InvestmentSpecificityDefinitionValidator}.
 */
public class InvestmentSpecificityDefinitionValidatorTest {

	@Mock
	private InvestmentSpecificityService investmentSpecificityService;
	@Mock
	private DaoLocator investmentSpecificityDefinitionDaoLocator;
	@Mock
	private ReadOnlyDAO<InvestmentSpecificityDefinition> investmentSpecificityDefinitionDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidateDifferentTables() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Generate sample schema data
			SystemTable definitionTable = new SystemTable();
			definitionTable.setId((short) 2);
			SystemTable fieldTable = new SystemTable();
			fieldTable.setId((short) 1);
			SystemColumn fieldColumn = new SystemColumnStandard();
			fieldColumn.setTable(fieldTable);

			// Generate sample data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setId((short) 1);
			definition.setSystemTable(definitionTable);
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setSystemColumn(fieldColumn);
			field.setDefinition(definition);

			// Run test
			InvestmentSpecificityDefinitionValidator definitionValidator = generateValidator();
			Mockito.when(this.investmentSpecificityService.getInvestmentSpecificityFieldList(ArgumentMatchers.any())).thenReturn(Collections.singletonList(field));
			definitionValidator.validate(definition, DaoEventTypes.UPDATE);
		}, "Modifications to the given fields are not allowed: " + CollectionUtils.toString(Arrays.asList("systemTable", "id"), 10));
	}


	@Test
	public void testValidateBeanClassNameQualifiers() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Generate sample data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setId((short) 1);
			definition.setEntryBeanClassName(TestInvalidSpecificityBeanClassQualifiers.class.getName());

			// Run test
			InvestmentSpecificityDefinitionValidator definitionValidator = generateValidator();
			Mockito.when(this.investmentSpecificityService.getInvestmentSpecificityFieldList(ArgumentMatchers.any())).thenReturn(Collections.emptyList());
			definitionValidator.validate(definition, DaoEventTypes.UPDATE);
		}, "Modifications to the given fields are not allowed: " + CollectionUtils.toString(Arrays.asList("id", "entryBeanClassName"), 10));
	}


	@Test
	public void testValidateBeanClassNameType() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Generate sample data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setId((short) 1);
			definition.setEntryBeanClassName(TestInvalidSpecificityBeanClassType.class.getName());

			// Run test
			InvestmentSpecificityDefinitionValidator definitionValidator = generateValidator();
			Mockito.when(this.investmentSpecificityService.getInvestmentSpecificityFieldList(ArgumentMatchers.any())).thenReturn(Collections.emptyList());
			definitionValidator.validate(definition, DaoEventTypes.UPDATE);
		}, "Modifications to the given fields are not allowed: " + CollectionUtils.toString(Arrays.asList("id", "entryBeanClassName"), 10));
	}


	@Test
	public void testSuccessfulDefinition() {
		// Field restrictions prevent modifications; if field restrictions are disabled, expected exception should be removed
		TestUtils.expectException(ValidationException.class, () -> {
			// Generate sample data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setId((short) 1);
			definition.setEntryBeanClassName(TestValidSpecificityBeanClass.class.getName());

			// Run test
			InvestmentSpecificityDefinitionValidator definitionValidator = generateValidator();
			Mockito.when(this.investmentSpecificityService.getInvestmentSpecificityFieldList(ArgumentMatchers.any())).thenReturn(Collections.emptyList());
			definitionValidator.validate(definition, DaoEventTypes.UPDATE);
		}, "Modifications to the given fields are not allowed: " + CollectionUtils.toString(Arrays.asList("id", "entryBeanClassName"), 10));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentSpecificityDefinitionValidator generateValidator() {
		InvestmentSpecificityDefinitionValidator definitionValidator = new InvestmentSpecificityDefinitionValidator();
		definitionValidator.setInvestmentSpecificityService(this.investmentSpecificityService);
		definitionValidator.setDaoLocator(this.investmentSpecificityDefinitionDaoLocator);
		definitionValidator.setContextHandler(new ThreadLocalContextHandler());

		// Setup mocked methods for getOriginalBean method
		Mockito.when(this.investmentSpecificityDefinitionDaoLocator.locate(ArgumentMatchers.any(InvestmentSpecificityDefinition.class.getClass()))).thenReturn(this.investmentSpecificityDefinitionDAO);
		Mockito.when(this.investmentSpecificityDefinitionDAO.findByPrimaryKey(ArgumentMatchers.any(Serializable.class))).thenReturn(new InvestmentSpecificityDefinition());

		return definitionValidator;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private abstract static class TestInvalidSpecificityBeanClassQualifiers extends BaseInvestmentSpecificityBean {

	}

	public static class TestInvalidSpecificityBeanClassType {

	}

	public static class TestValidSpecificityBeanClass extends BaseInvestmentSpecificityBean {

	}
}
