package com.clifton.investment.specificity.validation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ThreadLocalContextHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.investment.specificity.InvestmentSpecificityDefinition;
import com.clifton.investment.specificity.InvestmentSpecificityField;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnStandard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.Serializable;
import java.util.Arrays;


/**
 * Test class for {@link InvestmentSpecificityFieldValidator}.
 */
public class InvestmentSpecificityFieldValidatorTest {

	@Mock
	private DaoLocator investmentSpecificityFieldDaoLocator;
	@Mock
	private ReadOnlyDAO<InvestmentSpecificityField> investmentSpecificityFieldDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidateColumnAndField() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			// Create original field
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setId((short) 1);
			InvestmentSpecificityFieldValidator fieldValidator = generateValidator(BeanUtils.cloneBean(field, false, true));

			// Generate test data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			field.setDefinition(definition);
			field.setName("field");
			field.setSystemColumn(new SystemColumnStandard());

			// Run test
			fieldValidator.validate(field, DaoEventTypes.UPDATE);
		}, "Modifications to the given fields are not allowed: " + CollectionUtils.toString(Arrays.asList("systemColumn", "name", "definition"), 10));
	}


	@Test
	public void testValidateFieldWithTable() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			// Create original field
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setId((short) 1);
			InvestmentSpecificityFieldValidator fieldValidator = generateValidator(BeanUtils.cloneBean(field, false, true));

			// Generate test data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setSystemTable(new SystemTable());
			field.setDefinition(definition);
			field.setName("field");

			// Run test
			fieldValidator.validate(field, DaoEventTypes.UPDATE);
		}, "Modifications to the given fields are not allowed: " + CollectionUtils.toString(Arrays.asList("name", "definition"), 10));
	}


	@Test
	public void testValidateColumnWithNoTable() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			// Create original field
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setId((short) 1);
			InvestmentSpecificityFieldValidator fieldValidator = generateValidator(BeanUtils.cloneBean(field, false, true));

			// Generate test data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			field.setDefinition(definition);
			field.setSystemColumn(new SystemColumnStandard());

			// Run test
			fieldValidator.validate(field, DaoEventTypes.UPDATE);
		}, "Modifications to the given fields are not allowed: " +
				CollectionUtils.toString(Arrays.asList("systemColumn", "definition"), 10));
	}


	@Test
	public void testValidateIncorrectTable() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			// Generate sample schema data
			SystemTable definitionTable = new SystemTable();
			definitionTable.setId((short) 1);
			SystemTable fieldTable = new SystemTable();
			fieldTable.setId((short) 2);
			SystemColumn fieldColumn = new SystemColumnStandard();
			fieldColumn.setTable(fieldTable);

			// Create original field
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setId((short) 1);
			InvestmentSpecificityFieldValidator fieldValidator = generateValidator(BeanUtils.cloneBean(field, false, true));

			// Generate test data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setSystemTable(definitionTable);
			field.setDefinition(definition);
			field.setSystemColumn(fieldColumn);

			// Run test
			fieldValidator.validate(field, DaoEventTypes.UPDATE);
		}, "Modifications to the given fields are not allowed: " +
				CollectionUtils.toString(Arrays.asList("systemColumn", "definition"), 10));
	}


	@Test
	public void testValidateSuccessField() {
		// Field restrictions prevent modifications; if field restrictions are disabled, expected exception should be removed
		TestUtils.expectException(FieldValidationException.class, () -> {
			// Create original field
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setId((short) 1);
			InvestmentSpecificityFieldValidator fieldValidator = generateValidator(BeanUtils.cloneBean(field, false, true));

			// Generate test data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			field.setDefinition(definition);
			field.setName("field");

			// Run test
			fieldValidator.validate(field, DaoEventTypes.UPDATE);
		}, "Modifications to the given fields are not allowed: " + CollectionUtils.toString(Arrays.asList("name", "definition"), 10));
	}


	@Test
	public void testValidateSuccessColumn() {
		// Field restrictions prevent modifications; if field restrictions are disabled, expected exception should be removed
		TestUtils.expectException(FieldValidationException.class, () -> {
			// Create original field
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setId((short) 1);
			InvestmentSpecificityFieldValidator fieldValidator = generateValidator(BeanUtils.cloneBean(field, false, true));

			// Generate sample schema data
			SystemTable table = new SystemTable();
			SystemColumn column = new SystemColumnStandard();
			column.setTable(table);

			// Generate test data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setSystemTable(table);
			field.setDefinition(definition);
			field.setSystemColumn(column);

			// Run test
			fieldValidator.validate(field, DaoEventTypes.UPDATE);
		}, "Modifications to the given fields are not allowed: " +
				CollectionUtils.toString(Arrays.asList("systemColumn", "definition"), 10));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentSpecificityFieldValidator generateValidator(InvestmentSpecificityField originalField) {
		InvestmentSpecificityFieldValidator fieldValidator = new InvestmentSpecificityFieldValidator();
		fieldValidator.setDaoLocator(this.investmentSpecificityFieldDaoLocator);
		fieldValidator.setContextHandler(new ThreadLocalContextHandler());

		// Setup mocked methods for getOriginalBean method
		Mockito.when(this.investmentSpecificityFieldDaoLocator.locate(ArgumentMatchers.any(InvestmentSpecificityField.class.getClass()))).thenReturn(this.investmentSpecificityFieldDAO);
		Mockito.when(this.investmentSpecificityFieldDAO.findByPrimaryKey(ArgumentMatchers.any(Serializable.class))).thenReturn(originalField);

		return fieldValidator;
	}
}
