package com.clifton.investment.specificity.validation;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyType;
import com.clifton.core.context.ThreadLocalContextHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.specificity.BaseInvestmentSpecificityBean;
import com.clifton.investment.specificity.InvestmentSpecificityDefinition;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityField;
import com.clifton.investment.specificity.InvestmentSpecificityUtilHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.Serializable;


/**
 * Test class for {@link InvestmentSpecificityEntryValidator}.
 */
public class InvestmentSpecificityEntryValidatorTest {

	@InjectMocks
	private InvestmentSpecificityEntryValidator investmentSpecificityEntryValidator;
	@Mock
	private DaoLocator investmentSpecificityEntryDaoLocator;
	@Mock
	private ReadOnlyDAO<InvestmentSpecificityEntry> investmentSpecificityEntryDAO;
	@Mock
	private InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		// Setup validator properties and mocks
		this.investmentSpecificityEntryValidator.setContextHandler(new ThreadLocalContextHandler());
		Mockito.when(this.investmentSpecificityEntryDaoLocator.locate(ArgumentMatchers.any(InvestmentSpecificityEntry.class))).thenReturn(this.investmentSpecificityEntryDAO);
		Mockito.when(this.investmentSpecificityEntryDAO.findByPrimaryKey(ArgumentMatchers.any(Serializable.class))).thenReturn(new InvestmentSpecificityEntry());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAllowedInstrument() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Generate sample data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setInstrumentAllowed(false);
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setDefinition(definition);
			InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
			entry.setField(field);
			entry.setInstrument(new InvestmentInstrument());

			// Run test
			this.investmentSpecificityEntryValidator.validate(entry, DaoEventTypes.INSERT);
		}, "The instrument qualifier is not allowed by this specificity definition.");
	}


	@Test
	public void testAllowedCurrencyDenomination() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Generate sample data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setCurrencyDenominationAllowed(false);
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setDefinition(definition);
			InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
			entry.setField(field);
			entry.setCurrencyDenomination(new InvestmentSecurity());

			// Run test
			this.investmentSpecificityEntryValidator.validate(entry, DaoEventTypes.INSERT);
		}, "The currency denomination qualifier is not allowed by this specificity definition.");
	}


	@Test
	public void testAllowedIssuerCompany() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Generate sample data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setIssuerCompanyAllowed(false);
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setDefinition(definition);
			InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
			entry.setField(field);
			entry.setIssuerCompany(new BusinessCompany());

			// Run test
			this.investmentSpecificityEntryValidator.validate(entry, DaoEventTypes.INSERT);
		}, "The issuer company qualifier is not allowed by this specificity definition.");
	}


	@Test
	public void testHierarchyAndInstrument() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Generate sample data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setInstrumentAllowed(true);
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setDefinition(definition);
			InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
			entry.setField(field);
			entry.setHierarchy(new InvestmentInstrumentHierarchy());
			entry.setInstrument(new InvestmentInstrument());

			// Run test
			this.investmentSpecificityEntryValidator.validate(entry, DaoEventTypes.INSERT);
		}, "The hierarchy and instrument qualifiers may not be simultaneously defined.");
	}


	@Test
	public void testHierarchyAndType() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Generate sample data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setDefinition(definition);
			InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
			entry.setField(field);
			entry.setHierarchy(new InvestmentInstrumentHierarchy());
			entry.setInvestmentType(new InvestmentType());

			// Run test
			this.investmentSpecificityEntryValidator.validate(entry, DaoEventTypes.INSERT);
		}, "A hierarchy or instrument may not be defined if categorical parameters, such as type and sub type, are defined.");
	}


	@Test
	public void testInstrumentAndType() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Generate sample data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setInstrumentAllowed(true);
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setDefinition(definition);
			InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
			entry.setField(field);
			entry.setInstrument(new InvestmentInstrument());
			entry.setInvestmentType(new InvestmentType());

			// Run test
			this.investmentSpecificityEntryValidator.validate(entry, DaoEventTypes.INSERT);
		}, "A hierarchy or instrument may not be defined if categorical parameters, such as type and sub type, are defined.");
	}


	@Test
	public void testCurrencyDenominationIsCurrency() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Generate sample data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setCurrencyDenominationAllowed(true);
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setDefinition(definition);
			InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
			entry.setField(field);
			InvestmentSecurity currencyDenomination = new InvestmentSecurity();
			currencyDenomination.setCurrency(false);
			entry.setCurrencyDenomination(currencyDenomination);

			// Run test
			this.investmentSpecificityEntryValidator.validate(entry, DaoEventTypes.INSERT);
		}, "The selected security is not a currency denomination.");
	}


	@Test
	public void testIssuerCompanyIsIssuer() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Generate sample data
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setIssuerCompanyAllowed(true);
			InvestmentSpecificityField field = new InvestmentSpecificityField();
			field.setDefinition(definition);
			InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
			entry.setField(field);
			BusinessCompany issuerCompany = new BusinessCompany();
			BusinessCompanyType companyType = new BusinessCompanyType();
			companyType.setIssuer(false);
			issuerCompany.setType(companyType);
			entry.setIssuerCompany(issuerCompany);

			// Run test
			this.investmentSpecificityEntryValidator.validate(entry, DaoEventTypes.INSERT);
		}, "The selected company is not an issuer.");
	}


	@Test
	public void testCustomValidation() {
		// Generate sample data
		InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
		definition.setIssuerCompanyAllowed(true);
		definition.setEntryBeanClassName(TestInvestmentSpecificityValidationAwareBean.class.getName());
		InvestmentSpecificityField field = new InvestmentSpecificityField();
		field.setDefinition(definition);
		InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
		entry.setField(field);

		BaseInvestmentSpecificityBean specificityBean = Mockito.mock(TestInvestmentSpecificityValidationAwareBean.class);
		Mockito.when(this.investmentSpecificityUtilHandler.generatePopulatedSpecificityBean(ArgumentMatchers.any())).thenReturn(specificityBean);

		// Run test
		this.investmentSpecificityEntryValidator.validate(entry, DaoEventTypes.INSERT);
		Mockito.verify(this.investmentSpecificityUtilHandler, Mockito.times(1)).generatePopulatedSpecificityBean(ArgumentMatchers.any());
		Mockito.verify((ValidationAware) specificityBean, Mockito.times(1)).validate();
	}


	@Test
	public void testNoCustomValidation() {
		// Generate sample data
		InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
		definition.setIssuerCompanyAllowed(true);
		definition.setEntryBeanClassName(TestInvestmentSpecificityBean.class.getName());
		InvestmentSpecificityField field = new InvestmentSpecificityField();
		field.setDefinition(definition);
		InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
		entry.setField(field);

		// Run test
		this.investmentSpecificityEntryValidator.validate(entry, DaoEventTypes.INSERT);
		Mockito.verify(this.investmentSpecificityUtilHandler, Mockito.never()).generatePopulatedSpecificityBean(ArgumentMatchers.any());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private abstract static class TestInvestmentSpecificityBean extends BaseInvestmentSpecificityBean {

	}

	private abstract static class TestInvestmentSpecificityValidationAwareBean extends BaseInvestmentSpecificityBean implements ValidationAware {

	}
}
