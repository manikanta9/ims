package com.clifton.investment.specificity.validation;

import com.clifton.core.context.ThreadLocalContextHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityEntryProperty;
import com.clifton.investment.specificity.InvestmentSpecificityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Test class for {@link InvestmentSpecificityEntryValidator}.
 */
public class InvestmentSpecificityEntryObserverTest {

	@Mock
	private DaoLocator investmentSpecificityEntryDaoLocator;
	@Mock
	private InvestmentSpecificityService investmentSpecificityService;
	@Mock
	private ReadOnlyDAO<InvestmentSpecificityEntry> investmentSpecificityEntryDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCreatePropertiesOnSave() throws Exception {
		// Generate sample data
		InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
		entry.setId(1);

		// Run test
		InvestmentSpecificityEntryObserver observer = generateObserver(null);
		runOperation(observer, getInvestmentSpecificityEntryDAO(), DaoEventTypes.INSERT, entry);
		Mockito.verify(observer.getInvestmentSpecificityService(), Mockito.times(1)).saveInvestmentSpecificityEntryPropertiesForEntry(entry);
	}


	@Test
	public void testDeleteRemovedPropertiesOnSave() throws Exception {
		// Generate sample data
		InvestmentSpecificityEntry originalEntry = new InvestmentSpecificityEntry();
		originalEntry.setId(1);
		InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
		entry.setId(1);

		// Run test
		InvestmentSpecificityEntryObserver observer = generateObserver(originalEntry);
		runOperation(observer, getInvestmentSpecificityEntryDAO(), DaoEventTypes.UPDATE, entry);
		Mockito.verify(observer.getInvestmentSpecificityService(), Mockito.times(1)).saveInvestmentSpecificityEntryPropertiesForEntry(entry);
	}


	@Test
	public void testDeletePropertiesOnDelete() throws Exception {
		// Generate sample data
		InvestmentSpecificityEntry originalEntry = new InvestmentSpecificityEntry();
		originalEntry.setId(1);
		List<InvestmentSpecificityEntryProperty> originalPropertyList = new ArrayList<>();
		originalEntry.setPropertyList(originalPropertyList);
		for (int i = 0; i < 5; i++) {
			InvestmentSpecificityEntryProperty property = new InvestmentSpecificityEntryProperty();
			property.setId(i);
			property.setText("Text " + i);
			property.setValue("Value " + i);
			originalPropertyList.add(property);
		}

		// Run test
		InvestmentSpecificityEntryObserver observer = generateObserver(originalEntry);
		runOperation(observer, getInvestmentSpecificityEntryDAO(), DaoEventTypes.DELETE, originalEntry);

		// Verify number of service calls
		int deleteCalls = CollectionUtils.getSize(originalEntry.getPropertyList());
		Mockito.verify(observer.getInvestmentSpecificityService(), Mockito.times(deleteCalls)).deleteInvestmentSpecificityEntryProperty(Mockito.anyInt());
		for (InvestmentSpecificityEntryProperty property : originalEntry.getPropertyList()) {
			Mockito.verify(observer.getInvestmentSpecificityService(), Mockito.times(1)).deleteInvestmentSpecificityEntryProperty(property.getId());
		}
	}


	@Test
	public void testUpdatePropertiesOnSave() throws Exception {
		// Generate sample data
		InvestmentSpecificityEntry originalEntry = new InvestmentSpecificityEntry();
		originalEntry.setId(1);
		InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
		entry.setId(1);

		// Run test
		InvestmentSpecificityEntryObserver observer = generateObserver(originalEntry);
		runOperation(observer, getInvestmentSpecificityEntryDAO(), DaoEventTypes.UPDATE, entry);
		Mockito.verify(observer.getInvestmentSpecificityService(), Mockito.times(1)).saveInvestmentSpecificityEntryPropertiesForEntry(entry);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentSpecificityEntryObserver generateObserver(InvestmentSpecificityEntry originalBean) {
		InvestmentSpecificityEntryObserver entryObserver = new InvestmentSpecificityEntryObserver();
		entryObserver.setInvestmentSpecificityService(getInvestmentSpecificityService());
		entryObserver.setContextHandler(new ThreadLocalContextHandler());

		// Setup mocked methods
		Mockito.when(getInvestmentSpecificityService().getInvestmentSpecificityEntryPropertiesForEntry(ArgumentMatchers.any(InvestmentSpecificityEntry.class)))
				.thenAnswer(invocation -> originalBean.getPropertyList());
		Mockito.when(getInvestmentSpecificityEntryDaoLocator().locate(ArgumentMatchers.any(InvestmentSpecificityEntry.class.getClass()))).thenReturn(getInvestmentSpecificityEntryDAO());
		Mockito.when(getInvestmentSpecificityEntryDAO().findByPrimaryKey(ArgumentMatchers.any(Serializable.class))).thenReturn(originalBean);

		return entryObserver;
	}


	private void runOperation(InvestmentSpecificityEntryObserver observer, ReadOnlyDAO<InvestmentSpecificityEntry> dao, DaoEventTypes eventType, InvestmentSpecificityEntry bean) {
		observer.beforeMethodCall(dao, eventType, bean);
		observer.afterMethodCall(dao, eventType, bean, null);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getInvestmentSpecificityEntryDaoLocator() {
		return this.investmentSpecificityEntryDaoLocator;
	}


	public void setInvestmentSpecificityEntryDaoLocator(DaoLocator investmentSpecificityEntryDaoLocator) {
		this.investmentSpecificityEntryDaoLocator = investmentSpecificityEntryDaoLocator;
	}


	public InvestmentSpecificityService getInvestmentSpecificityService() {
		return this.investmentSpecificityService;
	}


	public void setInvestmentSpecificityService(InvestmentSpecificityService investmentSpecificityService) {
		this.investmentSpecificityService = investmentSpecificityService;
	}


	public ReadOnlyDAO<InvestmentSpecificityEntry> getInvestmentSpecificityEntryDAO() {
		return this.investmentSpecificityEntryDAO;
	}


	public void setInvestmentSpecificityEntryDAO(ReadOnlyDAO<InvestmentSpecificityEntry> investmentSpecificityEntryDAO) {
		this.investmentSpecificityEntryDAO = investmentSpecificityEntryDAO;
	}
}
