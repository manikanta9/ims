package com.clifton.investment.specificity.validation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ThreadLocalContextHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.investment.specificity.InvestmentSpecificityDefinition;
import com.clifton.investment.specificity.InvestmentSpecificityEntryPropertyType;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.SystemTable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;


/**
 * Test class for {@link InvestmentSpecificityEntryPropertyTypeValidator}.
 */
public class InvestmentSpecificityEntryPropertyTypeValidatorTest {

	@Mock
	private DaoLocator investmentSpecificityEntryPropertyTypeDaoLocator;
	@Mock
	private ReadOnlyDAO<InvestmentSpecificityEntryPropertyType> investmentSpecificityEntryPropertyTypeDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testModifiableFields() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			// Generate property data
			InvestmentSpecificityEntryPropertyType propertyType = new InvestmentSpecificityEntryPropertyType();
			propertyType.setId(1);
			Date propertyDate = new Date();
			InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
			definition.setId((short) 1);
			SystemDataType systemDataType = new SystemDataType();
			systemDataType.setId((short) 1);
			SystemTable valueTable = new SystemTable();
			valueTable.setId((short) 1);

			// Create original property type
			InvestmentSpecificityEntryPropertyTypeValidator propertyTypeValidator = generateValidator(BeanUtils.cloneBean(propertyType, false, true));

			// Assign updated properties
			propertyType.setCreateDate(propertyDate);
			propertyType.setCreateUserId((short) 1);
			propertyType.setDataType(systemDataType);
			propertyType.setDefinition(definition);
			propertyType.setDescription("description");
			propertyType.setEntryBeanPropertyName("property name");
			propertyType.setLabel("label");
			propertyType.setMultipleValuesAllowed(true);
			propertyType.setName("name");
			propertyType.setOrder(1);
			propertyType.setRequired(true);
			propertyType.setResetDynamicFieldsOnChange(true);
			propertyType.setRv(new byte[]{1, 2, 3, 4});
			propertyType.setTargetSystemUserInterfaceAware(propertyType);
			propertyType.setUpdateDate(propertyDate);
			propertyType.setUpdateUserId((short) 1);
			propertyType.setUserInterfaceConfig("user interface config");
			propertyType.setValueListUrl("value list URL");
			propertyType.setValueTable(valueTable);
			propertyType.setVirtualTypeId(1);

			// Run test
			propertyTypeValidator.validate(propertyType, DaoEventTypes.UPDATE);
		}, "Modifications to the given fields are not allowed: " + CollectionUtils.toString(Arrays.asList("resetDynamicFieldsOnChange", "dataType", "required", "virtualTypeId", "entryBeanPropertyName", "multipleValuesAllowed", "definition", "targetSystemUserInterfaceAware"), 10));
	}


	@Test
	public void testSuccess() {
		// Generate property data
		InvestmentSpecificityEntryPropertyType propertyType = new InvestmentSpecificityEntryPropertyType();
		propertyType.setId(1);
		Date propertyDate = new Date();
		InvestmentSpecificityDefinition definition = new InvestmentSpecificityDefinition();
		definition.setId((short) 1);
		SystemDataType systemDataType = new SystemDataType();
		systemDataType.setId((short) 1);
		SystemTable valueTable = new SystemTable();
		valueTable.setId((short) 1);

		// Create original property type
		InvestmentSpecificityEntryPropertyTypeValidator propertyTypeValidator = generateValidator(BeanUtils.cloneBean(propertyType, false, true));

		// Assign updated properties
		propertyType.setDescription("description");
		propertyType.setLabel("label");
		propertyType.setName("name");
		propertyType.setOrder(1);
		propertyType.setUserInterfaceConfig("user interface config");
		propertyType.setValueListUrl("value list URL");
		propertyType.setValueTable(valueTable);

		// Run test
		propertyTypeValidator.validate(propertyType, DaoEventTypes.UPDATE);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentSpecificityEntryPropertyTypeValidator generateValidator(InvestmentSpecificityEntryPropertyType originalPropertyType) {
		InvestmentSpecificityEntryPropertyTypeValidator propertyTypeValidator = new InvestmentSpecificityEntryPropertyTypeValidator();
		propertyTypeValidator.setDaoLocator(this.investmentSpecificityEntryPropertyTypeDaoLocator);
		propertyTypeValidator.setContextHandler(new ThreadLocalContextHandler());

		// Setup mocked methods for getOriginalBean method
		Mockito.when(this.investmentSpecificityEntryPropertyTypeDaoLocator.locate(ArgumentMatchers.any(InvestmentSpecificityEntryPropertyType.class.getClass()))).thenReturn(this.investmentSpecificityEntryPropertyTypeDAO);
		Mockito.when(this.investmentSpecificityEntryPropertyTypeDAO.findByPrimaryKey(ArgumentMatchers.any(Serializable.class))).thenReturn(originalPropertyType);

		return propertyTypeValidator;
	}
}
