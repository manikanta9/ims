package com.clifton.investment.replication.upload;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


/**
 * InvestmentReplicationUploadServiceTests tests uploading updates to existing allocations
 * Created by Mary
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class InvestmentReplicationUploadServiceTests {


	private static final int INVALID_SHEET_INDEX = 0;
	private static final int PARTIAL_INVALID_SHEET_INDEX = 1;
	private static final int VALID_SHEET_INDEX = 2;
	@Resource
	private InvestmentReplicationUploadService investmentReplicationUploadService;


	@Test
	public void testInvalidFile() {
		InvestmentReplicationAllocationUploadCommand bean = createUploadBean(false, BigDecimal.ONE, INVALID_SHEET_INDEX);
		this.investmentReplicationUploadService.uploadInvestmentReplicationAllocationUploadFile(bean);
		String expectedResult = "No Replications Updated." + StringUtils.NEW_LINE //
				+ "Replication [ACWI - 3 Contract] Errors: Allocation [Instrument: S&P 500 Mini]: Too high percent change (900.00%) from [51] to [510]." + StringUtils.NEW_LINE //
				+ "Replication [FIX BC Global Agg Currency] Errors: Allocation [Instrument: EC - Euro Currency]: Too high percent change (41.70%) from [24.7] to [35]., Allocation [Instrument: BP - British Pound]: Too high percent change (-66.14%) from [6.29] to [2.13]." + StringUtils.NEW_LINE //
				+ "Cannot find a replication with name [GuideStone: Put Options] in the system to update." + StringUtils.NEW_LINE //
				+ "Replication [Cleveland Clinic - FIX WGBI ex. US NO CURRENCY] total allocation is [100.2] but must total 100 %." + StringUtils.NEW_LINE //
				+ "Replication [GuideStone: Call Options] Errors: Cannot find existing allocation for [Instrument: S&P 500 PM Index Options]., Cannot find current security calculator with name [Missing Current Contract Calculator] used by allocation [Instrument: S&P 500 Week 4 Index Options, Security Group: Call Options]." + StringUtils.NEW_LINE //
				+ "Replication [DE Russell 3000] Errors: Cannot find existing allocation for [Instrument: EC - Euro Currency]., Allocation [Instrument: FA]: Too high percent change (189.02%) from [5.19] to [15]., Allocation [Instrument: RTA]: Too high percent change (212.78%) from [11.19] to [35].";
		TestUtils.validateUploadResultStringsIgnoringOrder(expectedResult, bean.getUploadResultsString());
	}


	@Test
	public void testPartialInvalidFile_DoNotUpload() {
		InvestmentReplicationAllocationUploadCommand bean = createUploadBean(false, BigDecimal.TEN, PARTIAL_INVALID_SHEET_INDEX);
		this.investmentReplicationUploadService.uploadInvestmentReplicationAllocationUploadFile(bean);
		String expectedResult = "No Replications Updated." + StringUtils.NEW_LINE //
				+ "Replication [ACWI - 3 Contract] Errors: Allocation [Instrument: S&P 500 Mini]: Too high percent change (900.00%) from [51] to [510]." + StringUtils.NEW_LINE //
				+ "Cannot find a replication with name [GuideStone: Put Options] in the system to update." + StringUtils.NEW_LINE //
				+ "Replication [Cleveland Clinic - FIX WGBI ex. US NO CURRENCY] total allocation is [100.2] but must total 100 %." + StringUtils.NEW_LINE //
				+ "Replication [DE Russell 3000] Errors: Cannot find existing allocation for [Instrument: EC - Euro Currency]., Allocation [Instrument: FA]: Too high percent change (189.02%) from [5.19] to [15]., Allocation [Instrument: RTA]: Too high percent change (212.78%) from [11.19] to [35].";
		TestUtils.validateUploadResultStringsIgnoringOrder(expectedResult, bean.getUploadResultsString());
	}


	@Test
	public void testPartialInvalidFile_Upload() {
		InvestmentReplicationAllocationUploadCommand bean = createUploadBean(true, BigDecimal.TEN, PARTIAL_INVALID_SHEET_INDEX);
		this.investmentReplicationUploadService.uploadInvestmentReplicationAllocationUploadFile(bean);
		String expectedResult = "2 Replications Updated." + StringUtils.NEW_LINE //
				+ "Replication [ACWI - 3 Contract] Errors: Allocation [Instrument: S&P 500 Mini]: Too high percent change (900.00%) from [51] to [510]." + StringUtils.NEW_LINE //
				+ "Cannot find a replication with name [GuideStone: Put Options] in the system to update." + StringUtils.NEW_LINE //
				+ "Replication [Cleveland Clinic - FIX WGBI ex. US NO CURRENCY] total allocation is [100.2] but must total 100 %." + StringUtils.NEW_LINE //
				+ "Replication [DE Russell 3000] Errors: Cannot find existing allocation for [Instrument: EC - Euro Currency]., Allocation [Instrument: FA]: Too high percent change (189.02%) from [5.19] to [15]., Allocation [Instrument: RTA]: Too high percent change (212.78%) from [11.19] to [35].";
		TestUtils.validateUploadResultStringsIgnoringOrder(expectedResult, bean.getUploadResultsString());
	}


	@Test
	public void testValidFile() {
		InvestmentReplicationAllocationUploadCommand bean = createUploadBean(false, BigDecimal.valueOf(6.0), VALID_SHEET_INDEX);
		this.investmentReplicationUploadService.uploadInvestmentReplicationAllocationUploadFile(bean);
		String expectedResult = "6 Replications Updated.";
		Assertions.assertEquals(expectedResult, bean.getUploadResultsString());
	}

	////////////////////////////////////////////////////////////////////////


	private InvestmentReplicationAllocationUploadCommand createUploadBean(boolean partialUploadAllowed, BigDecimal maxAllocationChange, int sheetIndex) {
		InvestmentReplicationAllocationUploadCommand bean = new InvestmentReplicationAllocationUploadCommand();
		bean.setFile(new MultipartFileImpl("src/test/java/com/clifton/investment/replication/upload/TestSimpleReplicationAllocationUploadFile.xls"));
		bean.setPartialUploadAllowed(partialUploadAllowed);
		bean.setMaxAllocationChange(maxAllocationChange);
		bean.setSheetIndex(sheetIndex);
		return bean;
	}
}
