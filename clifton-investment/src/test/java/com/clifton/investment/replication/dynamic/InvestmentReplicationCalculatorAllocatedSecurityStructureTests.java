package com.clifton.investment.replication.dynamic;

import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class InvestmentReplicationCalculatorAllocatedSecurityStructureTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentReplicationService investmentReplicationService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void test5050_DefaultThreshold_ApplySector() {
		Map<String, Double> resultMap = new HashMap<>();
		resultMap.put("W", 3.57);
		resultMap.put("KW", 1.06);
		resultMap.put("C", 6.30);
		resultMap.put("S", 4.11);
		resultMap.put("BO", 1.38);
		resultMap.put("SM", 1.29);
		resultMap.put("KC", 1.57);
		resultMap.put("SB", 2.68);

		resultMap.put("CT", 1.37);
		resultMap.put("LH", 1.83);
		resultMap.put("LC", 3.24);

		resultMap.put("CL", 16.60);
		resultMap.put("HO", 4.81);
		resultMap.put("XB", 4.49);
		resultMap.put("CO", 13.81);
		resultMap.put("QS", 4.31);
		resultMap.put("NG", 6.59);
		resultMap.put("LA", 3.51);
		resultMap.put("HG", 5.31);

		resultMap.put("LN", 1.43);
		resultMap.put("LX", 1.51);
		resultMap.put("GC", 6.97);
		resultMap.put("SI", 2.26);
		InvestmentReplication rep = setupReplication(BigDecimal.valueOf(50), BigDecimal.valueOf(50));
		InvestmentReplicationDynamicCalculator calc = setupCalculator(null, true);

		List<InvestmentReplicationAllocation> allocationList = calc.calculate(rep, new Date());
		validateResults(allocationList, resultMap);
	}


	@Test
	public void test5050_NoThreshold() {
		Map<String, Double> resultMap = new HashMap<>();
		resultMap.put("W", 3.57);
		resultMap.put("KW", 1.06);
		resultMap.put("C", 6.30);
		resultMap.put("S", 4.11);
		resultMap.put("BO", 1.38);
		resultMap.put("SM", 1.29);
		resultMap.put("KC", 1.54);
		resultMap.put("SB", 2.63);
		resultMap.put("CC", 0.11);
		resultMap.put("CT", 1.35);
		resultMap.put("LH", 1.74);
		resultMap.put("LC", 3.07);
		resultMap.put("FC", 0.26);
		resultMap.put("CL", 16.58);
		resultMap.put("HO", 4.81);
		resultMap.put("XB", 4.49);
		resultMap.put("CO", 13.81);
		resultMap.put("QS", 4.31);
		resultMap.put("NG", 6.59);
		resultMap.put("LA", 3.45);
		resultMap.put("HG", 5.20);
		resultMap.put("LL", 0.22);
		resultMap.put("LN", 1.41);
		resultMap.put("LX", 1.49);
		resultMap.put("GC", 6.97);
		resultMap.put("SI", 2.26);
		InvestmentReplication rep = setupReplication(BigDecimal.valueOf(50), BigDecimal.valueOf(50));
		InvestmentReplicationDynamicCalculator calc = setupCalculator(BigDecimal.ZERO, false);

		List<InvestmentReplicationAllocation> allocationList = calc.calculate(rep, new Date());
		validateResults(allocationList, resultMap);
	}


	@Test
	public void test5050_1Percent_ApplyGlobal() {
		Map<String, Double> resultMap = new HashMap<>();
		resultMap.put("W", 3.59);
		resultMap.put("KW", 1.07);
		resultMap.put("C", 6.33);
		resultMap.put("S", 4.13);
		resultMap.put("BO", 1.38);
		resultMap.put("SM", 1.30);
		resultMap.put("KC", 1.54);
		resultMap.put("SB", 2.65);
		resultMap.put("CT", 1.35);
		resultMap.put("LH", 1.75);
		resultMap.put("LC", 3.09);
		resultMap.put("CL", 16.72);
		resultMap.put("HO", 4.84);
		resultMap.put("XB", 4.52);
		resultMap.put("CO", 13.89);
		resultMap.put("QS", 4.34);
		resultMap.put("NG", 6.63);
		resultMap.put("LA", 3.47);
		resultMap.put("HG", 5.23);
		resultMap.put("LN", 1.41);
		resultMap.put("LX", 1.49);
		resultMap.put("GC", 7.01);
		resultMap.put("SI", 2.27);
		InvestmentReplication rep = setupReplication(BigDecimal.valueOf(50), BigDecimal.valueOf(50));
		InvestmentReplicationDynamicCalculator calc = setupCalculator(BigDecimal.ONE, false);

		List<InvestmentReplicationAllocation> allocationList = calc.calculate(rep, new Date());
		validateResults(allocationList, resultMap);
	}


	@Test
	public void testNegative5050_DefaultThreshold_ApplySector() {
		Map<String, Double> resultMap = new HashMap<>();
		resultMap.put("W", -3.57);
		resultMap.put("KW", -1.02);
		resultMap.put("C", -6.30);
		resultMap.put("S", -4.11);
		resultMap.put("BO", -1.38);
		resultMap.put("SM", -1.29);
		resultMap.put("KC", -1.57);
		resultMap.put("SB", -2.68);

		resultMap.put("CT", -1.37);
		resultMap.put("LH", -1.83);
		resultMap.put("LC", -3.24);

		resultMap.put("CL", -16.64);
		resultMap.put("HO", -4.81);
		resultMap.put("XB", -4.49);
		resultMap.put("CO", -13.81);
		resultMap.put("QS", -4.31);
		resultMap.put("NG", -6.59);
		resultMap.put("LA", -3.51);
		resultMap.put("HG", -5.30);

		resultMap.put("LN", -1.44);
		resultMap.put("LX", -1.51);
		resultMap.put("GC", -6.97);
		resultMap.put("SI", -2.26);
		InvestmentReplication rep = setupReplication(BigDecimal.valueOf(-50), BigDecimal.valueOf(-50));
		InvestmentReplicationDynamicCalculator calc = setupCalculator(null, true);

		List<InvestmentReplicationAllocation> allocationList = calc.calculate(rep, new Date());
		validateResults(allocationList, resultMap);
	}


	///////////////////////////////////////////////////
	///////////////////////////////////////////////////


	private InvestmentReplicationDynamicCalculator setupCalculator(BigDecimal threshold, boolean applySector) {

		InvestmentReplicationCalculatorAllocatedSecurityStructure calc = new InvestmentReplicationCalculatorAllocatedSecurityStructure();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(calc, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		calc.setThresholdPercentage(threshold);
		calc.setApplyUnderThresholdProportionallySameSector(applySector);
		return calc;
	}


	private InvestmentReplication setupReplication(BigDecimal djubsPercentage, BigDecimal gsciPercentage) {
		InvestmentReplication rep = new InvestmentReplication();
		rep.setType(this.investmentReplicationService.getInvestmentReplicationType((short) 1));

		List<InvestmentReplicationAllocation> allocList = new ArrayList<>();

		InvestmentReplicationAllocation alloc = new InvestmentReplicationAllocation();
		alloc.setId(1);
		alloc.setOrder(MathUtils.SHORT_ONE);
		alloc.setAllocationWeight(djubsPercentage);
		alloc.setReplicationSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbol("DJ-UBS", null));

		InvestmentReplicationAllocation alloc2 = new InvestmentReplicationAllocation();
		alloc2.setId(2);
		alloc2.setOrder(MathUtils.SHORT_TWO);
		alloc2.setAllocationWeight(gsciPercentage);
		alloc2.setReplicationSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbol("GSCI", null));

		allocList.add(alloc);
		allocList.add(alloc2);

		rep.setAllocationList(allocList);
		return rep;
	}


	private void validateResults(List<InvestmentReplicationAllocation> list, Map<String, Double> resultMap) {
		Assertions.assertEquals(resultMap.size(), list.size());
		BigDecimal sum = BigDecimal.ZERO;
		for (InvestmentReplicationAllocation allocation : list) {
			String instrumentPrefix = allocation.getReplicationInstrument().getIdentifierPrefix();
			//System.out.println(instrumentPrefix + ": " + CoreMathUtils.formatNumberDecimal(securityConfig.getReplicationAllocation().getAllocationWeight()));

			BigDecimal actual = allocation.getAllocationWeight();
			Assertions.assertNotNull(resultMap.get(instrumentPrefix), instrumentPrefix + " not expected in the results with a weight, but was returned with a weight of " + CoreMathUtils.formatNumberDecimal(actual));
			BigDecimal expected = BigDecimal.valueOf(resultMap.get(instrumentPrefix));

			sum = MathUtils.add(sum, expected);
			Assertions.assertTrue(MathUtils.isEqual(expected, actual), "Expected weight of [" + CoreMathUtils.formatNumberDecimal(expected) + "] for instrument [" + instrumentPrefix + "] but got [" + CoreMathUtils.formatNumberDecimal(actual) + "].");
		}
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
