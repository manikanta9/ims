package com.clifton.investment.replication;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityTargetAdjustmentTypes;
import com.clifton.investment.replication.history.InvestmentReplicationHistoryService;
import com.clifton.investment.replication.history.rebuild.InvestmentReplicationHistoryRebuildService;
import com.clifton.investment.replication.search.InvestmentReplicationAllocationSearchForm;
import com.clifton.system.bean.SystemBeanService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentReplicationServiceImplTests</code> ...
 *
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class InvestmentReplicationServiceImplTests {

	private static final Integer HG_SECURITY = 429;
	private static final Integer CL_SECURITY = 430;
	private static final Integer SB_SECURITY = 905;
	private static final Short TEST_REP_TYPE = 1;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private static final Short REPLICATION_REP_TYPE = 2;
	@Resource
	private InvestmentInstrumentService investmentInstrumentService;
	@Resource
	private InvestmentReplicationService investmentReplicationService;
	@Resource
	private InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService;
	@Resource
	private InvestmentReplicationHistoryService investmentReplicationHistoryService;
	@Resource
	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testChangeDefaultReplicationTypeName() {
		InvestmentReplicationType defaultType = this.investmentReplicationService.getInvestmentReplicationTypeByName(InvestmentReplicationType.DEFAULT_REPLICATION_TYPE_NAME);
		defaultType.setName(defaultType.getName() + "-Updated");
		boolean error = false;
		try {
			this.investmentReplicationService.saveInvestmentReplicationType(defaultType);
		}
		catch (Exception e) {
			error = true;
			Assertions.assertEquals("Replication Type name is used as a default selection and used by name. You cannot change the replication type name from [Replication] to [Replication-Updated].", e.getMessage());
		}
		Assertions.assertTrue(error, "Changing default replication type name should have throw validation exception.");
	}


	@Test
	public void testSaveInvestmentReplication() {
		// 1. New Replication
		InvestmentReplication rep = new InvestmentReplication();
		rep.setType(this.investmentReplicationService.getInvestmentReplicationType(TEST_REP_TYPE));

		List<InvestmentReplicationAllocation> allocationList = new ArrayList<>();
		allocationList.add(createAllocation(null, HG_SECURITY, 10, 50));
		InvestmentReplicationAllocation clAllocation = createAllocation(null, CL_SECURITY, 20, 50);
		allocationList.add(clAllocation);
		rep.setAllocationList(allocationList);

		rep = saveWaitAndReturnReplication(rep);

		// Validate New Replication Allocations
		// Active Only - 2
		List<InvestmentReplicationAllocation> list = getReplicationAllocationList(rep.getId(), true);
		Assertions.assertEquals(2, list.size());
		// Full History - 2
		list = getReplicationAllocationList(rep.getId(), false);
		Assertions.assertEquals(2, list.size());

		// 2.  Update Replication - Existing Allocation Changes Only
		rep.getAllocationList().get(0).setAllocationWeight(BigDecimal.valueOf(40));
		rep.getAllocationList().get(1).setAllocationWeight(BigDecimal.valueOf(60));

		rep = saveWaitAndReturnReplication(rep);

		// Validate Updates
		// Active Only - 2
		list = getReplicationAllocationList(rep.getId(), true);
		Assertions.assertEquals(2, list.size());
		for (InvestmentReplicationAllocation alloc : list) {
			if (HG_SECURITY.equals(alloc.getReplicationSecurity().getId())) {
				Assertions.assertEquals(40.0, alloc.getAllocationWeight().doubleValue(), 0d);
			}
			else {
				Assertions.assertEquals(60.0, alloc.getAllocationWeight().doubleValue(), 0d);
			}
		}
		// Full History - 4
		list = getReplicationAllocationList(rep.getId(), false);
		Assertions.assertEquals(4, list.size());

		// 3.  Add new Allocation Row and update 1 existing
		rep.getAllocationList().get(0).setAllocationWeight(BigDecimal.valueOf(30));
		rep.getAllocationList().add(createAllocation(null, SB_SECURITY, 50, 10.0));

		rep = saveWaitAndReturnReplication(rep);

		// Validate Updates
		// Active Only - 3
		list = getReplicationAllocationList(rep.getId(), true);
		Assertions.assertEquals(3, list.size());
		for (InvestmentReplicationAllocation alloc : list) {
			if (HG_SECURITY.equals(alloc.getReplicationSecurity().getId())) {
				Assertions.assertEquals(30.0, alloc.getAllocationWeight().doubleValue(), 0d);
			}
			else if (CL_SECURITY.equals(alloc.getReplicationSecurity().getId())) {
				Assertions.assertEquals(60.0, alloc.getAllocationWeight().doubleValue(), 0d);
			}
			else {
				Assertions.assertEquals(10.0, alloc.getAllocationWeight().doubleValue(), 0d);
			}
		}
		// Full History - 6
		list = getReplicationAllocationList(rep.getId(), false);
		Assertions.assertEquals(6, list.size());

		// 4.  Remove one Allocation Row and update 1 existing
		rep.getAllocationList().get(0).setAllocationWeight(BigDecimal.valueOf(90));
		rep.getAllocationList().remove(1);

		rep = saveWaitAndReturnReplication(rep);

		// Validate Updates
		// Active Only - 2
		list = getReplicationAllocationList(rep.getId(), true);
		Assertions.assertEquals(2, list.size());
		for (InvestmentReplicationAllocation alloc : list) {
			if (HG_SECURITY.equals(alloc.getReplicationSecurity().getId())) {
				Assertions.assertEquals(90.0, alloc.getAllocationWeight().doubleValue(), 0d);
			}
			else {
				Assertions.assertEquals(10.0, alloc.getAllocationWeight().doubleValue(), 0d);
			}
		}
		// Full History - 7
		list = getReplicationAllocationList(rep.getId(), false);
		Assertions.assertEquals(7, list.size());
	}


	@Test
	public void testInvestmentReplicationUpdate_WithHistory_Succeed() {
		InvestmentReplication rep = new InvestmentReplication();
		rep.setName("Test Update OK");
		rep.setType(this.investmentReplicationService.getInvestmentReplicationType(TEST_REP_TYPE));

		List<InvestmentReplicationAllocation> allocationList = new ArrayList<>();
		allocationList.add(createAllocation(null, HG_SECURITY, 10, 50));
		allocationList.add(createAllocation(null, CL_SECURITY, 20, 50));
		rep.setAllocationList(allocationList);

		rep = this.investmentReplicationService.saveInvestmentReplication(rep);

		this.investmentReplicationHistoryRebuildService.getOrBuildInvestmentReplicationHistory(rep.getId(), DateUtils.toDate("02/21/2020"));

		rep.setName("Test Update OK = Update Name");
		rep.setInactive(true);
		this.investmentReplicationService.saveInvestmentReplication(rep);

		// Clear history to ensure we can update rep type
		this.investmentReplicationHistoryService.clearInvestmentReplicationHistoryList(rep.getId());

		rep.setType(this.investmentReplicationService.getInvestmentReplicationType(REPLICATION_REP_TYPE));
		this.investmentReplicationService.saveInvestmentReplication(rep);
	}


	@Test
	public void testInvestmentReplicationUpdate_WithHistory_Fail() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentReplication rep = new InvestmentReplication();
			rep.setName("Test Update Failure");
			rep.setType(this.investmentReplicationService.getInvestmentReplicationType(TEST_REP_TYPE));

			List<InvestmentReplicationAllocation> allocationList = new ArrayList<>();
			allocationList.add(createAllocation(null, HG_SECURITY, 10, 50));
			allocationList.add(createAllocation(null, CL_SECURITY, 20, 50));
			rep.setAllocationList(allocationList);

			rep = this.investmentReplicationService.saveInvestmentReplication(rep);

			this.investmentReplicationHistoryRebuildService.getOrBuildInvestmentReplicationHistory(rep.getId(), DateUtils.toDate("02/21/2020"));

			rep.setType(this.investmentReplicationService.getInvestmentReplicationType(REPLICATION_REP_TYPE));
			this.investmentReplicationService.saveInvestmentReplication(rep);
		}, "Replication Test Update Failure has history associated with it.  The following fields cannot be changed: Replication Type, Reverse Exposure Sign, Dynamic Calculator Bean, Rebalance Trigger Type");
	}


	@Test
	public void testInvestmentReplicationAllocationSecurityList() {
		InvestmentSecurity spx = this.investmentInstrumentService.getInvestmentSecurityBySymbol("SPX", false);

		InvestmentSecurity spxw = this.investmentInstrumentService.getInvestmentSecurityBySymbol("SPXW US 04/16/21 C4020", false);
		InvestmentSecurity spxFlex = this.investmentInstrumentService.getInvestmentSecurityBySymbol("SPX FLEX 04/16/21 C2985.00", false);

		List<InvestmentSecurity> spxOptionList = Arrays.asList(spxw, spxFlex);

		InvestmentReplicationAllocation securityUnderlyingReplicationAllocation = new InvestmentReplicationAllocation();
		securityUnderlyingReplicationAllocation.setSecurityUnderlying(true);
		securityUnderlyingReplicationAllocation.setReplicationSecurity(spx);
		securityUnderlyingReplicationAllocation.setAllocationWeight(new BigDecimal("100"));
		securityUnderlyingReplicationAllocation.setReplicationInstrument(spx.getInstrument());
		securityUnderlyingReplicationAllocation.setCurrentSecurityCalculatorBean(this.systemBeanService.getSystemBeanByName("Default Current Security Calculator"));
		securityUnderlyingReplicationAllocation.setCurrentSecurityTargetAdjustmentType(InvestmentCurrentSecurityTargetAdjustmentTypes.DIFFERENCE_ALL);

		InvestmentReplication rep = new InvestmentReplication();
		rep.setType(this.investmentReplicationService.getInvestmentReplicationType(TEST_REP_TYPE));
		rep.setAllocationList(Arrays.asList(securityUnderlyingReplicationAllocation));
		rep = saveWaitAndReturnReplication(rep);

		this.investmentReplicationService.saveInvestmentReplication(rep);
		List<InvestmentSecurity> securityList = this.investmentReplicationService.getInvestmentReplicationAllocationSecurityList(com.clifton.core.util.CollectionUtils.getFirstElement(rep.getAllocationList()), null);
		ValidationUtils.assertTrue(CollectionUtils.getIntersection(securityList, spxOptionList).size() == 2, "Expected SPXW and SPX FLEX options to be returned for Replication Allocation.");

		InvestmentSecurity boh3 = this.investmentInstrumentService.getInvestmentSecurityBySymbol("BOH3", false);
		CollectionUtils.getFirstElement(rep.getAllocationList()).setReplicationSecurity(boh3);
		rep = saveWaitAndReturnReplication(rep);
		securityList = this.investmentReplicationService.getInvestmentReplicationAllocationSecurityList(com.clifton.core.util.CollectionUtils.getFirstElement(rep.getAllocationList()), null);
		ValidationUtils.assertEmpty(CollectionUtils.getIntersection(securityList, spxOptionList), "Expected no securities to be returned for Replication Allocation.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentReplication saveWaitAndReturnReplication(InvestmentReplication rep) {
		// Wait a second and then return
		try {
			Thread.sleep(1000);
			this.investmentReplicationService.saveInvestmentReplication(rep);
			Thread.sleep(1000);
			return this.investmentReplicationService.getInvestmentReplication(rep.getId());
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	private List<InvestmentReplicationAllocation> getReplicationAllocationList(Integer repId, boolean activeOnly) {
		InvestmentReplicationAllocationSearchForm searchForm = new InvestmentReplicationAllocationSearchForm();
		if (activeOnly) {
			searchForm.setActiveOnDate(new Date());
		}
		searchForm.setReplicationId(repId);
		return this.investmentReplicationService.getInvestmentReplicationAllocationList(searchForm);
	}


	private InvestmentReplicationAllocation createAllocation(Integer instrumentId, Integer securityId, Integer order, double weight) {
		InvestmentReplicationAllocation allocation = new InvestmentReplicationAllocation();
		allocation.setReplicationInstrument(instrumentId == null ? null : this.investmentInstrumentService.getInvestmentInstrument(instrumentId));
		allocation.setReplicationSecurity(securityId == null ? null : this.investmentInstrumentService.getInvestmentSecurity(securityId));
		allocation.setOrder(order == null ? null : order.shortValue());
		allocation.setAllocationWeight(BigDecimal.valueOf(weight));
		return allocation;
	}
}
