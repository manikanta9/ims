package com.clifton.investment.replication.dynamic;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.system.bean.SystemBeanService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author mitchellf
 */
@InvestmentInMemoryDatabaseContext
public class InvestmentReplicationCalculatorTrancheTests extends BaseInMemoryDatabaseTests {

	private static final int REPLICATION_ID = 1788;
	private static final String BEAN_NAME = "Adept Tranche Replication Calculator (Analyst)";
	private static final String BALANCE_DATE = "01/24/2022";

	@Resource
	private InvestmentReplicationService investmentReplicationService;

	@Resource
	private SystemBeanService systemBeanService;


	@Test
	public void testCreateAllocationWhenSecurityDoesNotExist() {
		InvestmentReplication replication = this.investmentReplicationService.getInvestmentReplication(REPLICATION_ID);
		InvestmentReplicationCalculatorTranche calculator = (InvestmentReplicationCalculatorTranche) this.systemBeanService.getBeanInstance(this.systemBeanService.getSystemBeanByName(BEAN_NAME));
		TestUtils.expectException(ValidationException.class,
				() -> calculator.calculateImpl(replication, DateUtils.toDate(BALANCE_DATE)),
				"Expected exactly 1 current security using Schedule Based Current Security Calculator but found 0 for {id=520353, label=Adept 19 - LGIM ACS BP Only (Short) - Dynamic 2:Instrument [SGD/GBP Currency Forward (Currency / Forwards / Deliverable Forwards)] } with Last Delivery Date 03/31/2022.");

		calculator.setCreateSecurityIfMissing(true);
		List<InvestmentReplicationAllocation> allocations = calculator.calculateImpl(replication, DateUtils.toDate(BALANCE_DATE));
		ValidationUtils.assertEquals(allocations.size(), 3, "Expected to generate 3 allocation but generated " + allocations.size());
	}
}
