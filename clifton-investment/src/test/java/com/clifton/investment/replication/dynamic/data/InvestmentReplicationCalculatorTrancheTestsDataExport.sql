SELECT 'InvestmentReplicationAllocation' AS entityTableName, InvestmentReplicationAllocationID AS entityID
FROM InvestmentReplicationAllocation
WHERE InvestmentReplicationAllocationID = 520353
UNION
SELECT 'SystemBeanProperty' AS entityTableName, SystemBeanPropertyID AS entityID
FROM SystemBeanProperty
WHERE SystemBeanID IN (7733, 6541, 8266)
UNION
SELECT 'CalendarSchedule' AS entityTableName, CalendarScheduleID AS entityID
FROM CalendarSchedule
WHERE CalendarScheduleID IN (852, 853)
UNION
SELECT 'Calendar' AS entityTableName, CalendarID AS entityID
FROM Calendar
WHERE CalendarID IN (129)
UNION
SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityID
FROM InvestmentSecurity
WHERE InvestmentInstrumentID = 17668
UNION
SELECT 'SystemColumn' AS entityTableName, SystemColumnID AS entityID
FROM SystemColumn
WHERE SystemColumnGroupID = 3
UNION
SELECT 'InvestmentSecurityEventType' AS entityTableName, InvestmentSecurityEventTypeID AS entityID
FROM InvestmentSecurityEventType;
