package com.clifton.investment.replication.history;

import com.clifton.core.dataaccess.dao.xml.XmlUpdatableDAO;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentInstrumentServiceImpl;
import com.clifton.investment.replication.InvestmentReplicationServiceImpl;
import com.clifton.investment.replication.history.rebuild.InvestmentReplicationHistoryRebuildServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;


/**
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class InvestmentReplicationHistoryServiceImplTests {

	private static final int TEST_REPLICATION_ID = 1;
	private static final int TEST_REPLICATION_SECURITY_UNDERLYING_ID = 2;
	private static final int SECURITY_BO_REPLICATION_ALLOCATION_ID = 10;
	private static final int SECURITY_C_REPLICATION_ALLOCATION_ID = 11;
	private static final int SECURITY_SPXFLEXC2985_ID = 128087;
	@Resource
	private InvestmentReplicationHistoryRebuildServiceImpl investmentReplicationHistoryRebuildService;
	@Resource
	private InvestmentReplicationHistoryServiceImpl investmentReplicationHistoryService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInvestmentReplicationHistoryBalanceDateCache() {
		Date firstDate = DateUtils.toDate("03/02/2020");
		Date previousWeekday = DateUtils.getPreviousWeekday(firstDate);
		Date secondDate = DateUtils.getNextWeekday(firstDate);
		Date thirdDate = DateUtils.getNextWeekday(secondDate);

		Assertions.assertNull(this.investmentReplicationHistoryService.getInvestmentReplicationHistoryForBalanceDate(TEST_REPLICATION_ID, previousWeekday, false));
		Assertions.assertNull(this.investmentReplicationHistoryService.getInvestmentReplicationHistoryForBalanceDate(TEST_REPLICATION_ID, firstDate, false));
		Assertions.assertNull(this.investmentReplicationHistoryService.getInvestmentReplicationHistoryForBalanceDate(TEST_REPLICATION_ID, secondDate, false));
		Assertions.assertNull(this.investmentReplicationHistoryService.getInvestmentReplicationHistoryForBalanceDate(TEST_REPLICATION_ID, thirdDate, false));

		this.investmentReplicationHistoryRebuildService.getOrBuildInvestmentReplicationHistory(TEST_REPLICATION_ID, firstDate);
		// Confirms the Cache is Reset for 3/2
		Assertions.assertNotNull(this.investmentReplicationHistoryService.getInvestmentReplicationHistoryForBalanceDate(TEST_REPLICATION_ID, firstDate, false));

		this.investmentReplicationHistoryRebuildService.getOrBuildInvestmentReplicationHistory(TEST_REPLICATION_ID, secondDate);
		// Confirms the Cache is Reset for 3/3
		Assertions.assertNotNull(this.investmentReplicationHistoryService.getInvestmentReplicationHistoryForBalanceDate(TEST_REPLICATION_ID, secondDate, false));

		this.investmentReplicationHistoryRebuildService.getOrBuildInvestmentReplicationHistory(TEST_REPLICATION_ID, thirdDate);
		// Confirms the Cache is Reset for 3/4
		Assertions.assertNotNull(this.investmentReplicationHistoryService.getInvestmentReplicationHistoryForBalanceDate(TEST_REPLICATION_ID, thirdDate, false));

		// Clear History
		this.investmentReplicationHistoryService.clearInvestmentReplicationHistoryList(TEST_REPLICATION_ID);
		Assertions.assertNull(this.investmentReplicationHistoryService.getInvestmentReplicationHistoryForBalanceDate(TEST_REPLICATION_ID, firstDate, false));
		Assertions.assertNull(this.investmentReplicationHistoryService.getInvestmentReplicationHistoryForBalanceDate(TEST_REPLICATION_ID, secondDate, false));
		Assertions.assertNull(this.investmentReplicationHistoryService.getInvestmentReplicationHistoryForBalanceDate(TEST_REPLICATION_ID, thirdDate, false));
	}


	@Test
	public void testInvestmentReplicationHistory_AllocationSecurityIsUnderlying() {
		// Set the logicalEvaluatesToTrue flag on the InvestmentSecurity dao because our XML based criteria for unit tests doesn't support activeOnDate filter criteria
		((XmlUpdatableDAO) ((InvestmentInstrumentServiceImpl) ((InvestmentReplicationServiceImpl) this.investmentReplicationHistoryRebuildService.getInvestmentReplicationService()).getInvestmentInstrumentService()).getInvestmentSecurityDAO()).setLogicalEvaluatesToTrue(true);
		InvestmentReplicationHistory replicationHistory = this.investmentReplicationHistoryRebuildService.getOrBuildInvestmentReplicationHistory(TEST_REPLICATION_SECURITY_UNDERLYING_ID, DateUtils.toDate("03/23/2021"));
		int currentSecurityId = CollectionUtils.getStream(replicationHistory.getReplicationAllocationHistoryList()).findFirst().get().getCurrentInvestmentSecurity().getId();
		AssertUtils.assertEquals(currentSecurityId, SECURITY_SPXFLEXC2985_ID, "Security Underlying flag should be used to filter instrument securities for Replication Allocation History.");
	}
}

