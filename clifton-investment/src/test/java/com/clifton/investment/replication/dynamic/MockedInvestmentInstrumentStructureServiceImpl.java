package com.clifton.investment.replication.dynamic;


import com.clifton.core.beans.BeanUtils;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureServiceImpl;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureWeight;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>MockedInvestmentInstrumentStructureServiceImpl</code> ...
 * <p/>
 * Mocked Version for simplified version without calculators so can test using the results
 *
 * @author manderson
 */
public class MockedInvestmentInstrumentStructureServiceImpl extends InvestmentInstrumentStructureServiceImpl {

	@Override
	public List<InvestmentSecurityStructureAllocation> getInvestmentSecurityStructureAllocationListForSecurityAndDate(@SuppressWarnings("unused") Integer structureId, int securityId, Date date,
	                                                                                                                  @SuppressWarnings("unused") boolean calculateCurrentForNextBusinessDay) {
		// Structure Service Uses lots of hibernate queries - custom sql, so for tests, this may be the best way to get the list
		List<InvestmentSecurityStructureWeight> securityWeightList = getInvestmentSecurityStructureWeightDAO().findByField("security.id", securityId);
		List<InvestmentSecurityStructureAllocation> securityAllocationList = new ArrayList<>();
		for (InvestmentSecurityStructureWeight secWeight : securityWeightList) {
			InvestmentSecurityStructureAllocation alloc = new InvestmentSecurityStructureAllocation(secWeight, date);
			alloc.setCurrentSecurity(getInvestmentInstrumentService().getInvestmentSecurityBySymbol(secWeight.getInstrumentWeight().getInstrument().getIdentifierPrefix() + "H3", null));
			alloc.setAllocationWeight(secWeight.getCoalesceWeight());
			alloc.getSecurityStructureWeight().getInstrumentWeight().setSector(getSector(alloc));
			securityAllocationList.add(alloc);
		}
		return BeanUtils.sortWithFunction(securityAllocationList, securityStructureAllocation -> securityStructureAllocation.getSecurityStructureWeight().getInstrumentWeight().getOrder(), true);
	}


	/**
	 * Easier to manager here than in the xml
	 *
	 * @param alloc
	 */
	private String getSector(InvestmentSecurityStructureAllocation alloc) {
		String identifierPrefix = alloc.getInstrumentAssignment().getIdentifierPrefix();

		switch (identifierPrefix) {
			case "BO":
			case "C":
			case "KW":
			case "W":
			case "SM":
			case "S":
				return "Grains";
			case "CC":
			case "CT":
			case "KC":
			case "SB":
				return "Softs";
			case "CL":
			case "CO":
			case "HO":
			case "NG":
			case "QS":
			case "XB":
				return "Energy";
			case "FC":
			case "LC":
			case "LH":
				return "Livestock";
			case "GC":
			case "SI":
				return "Precious Metals";
			case "HG":
			case "LA":
			case "LL":
			case "LN":
			case "LP":
			case "LX":
				return "Industrial Metals";
			default:
				return null;
		}
	}
}
