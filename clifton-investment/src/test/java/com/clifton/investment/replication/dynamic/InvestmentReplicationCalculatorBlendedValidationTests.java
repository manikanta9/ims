package com.clifton.investment.replication.dynamic;


import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.system.bean.SystemBean;
import org.junit.jupiter.api.Test;

import java.util.Arrays;


/**
 * Tests validation for {@link InvestmentReplicationCalculatorBlended}. Calculations are tested via integration tests.
 *
 * @author michaelm
 */
public class InvestmentReplicationCalculatorBlendedValidationTests {

	@Test
	public void testValidate_ReplicationTypeRequired() {
		InvestmentReplication replication = createInvestmentReplication();
		replication.setType(null);
		TestUtils.expectException(ValidationException.class, () -> createCalculator().validateInvestmentReplication(replication),
				"Replication Type is required for Dynamic Replication Calculators.");
	}


	@Test
	public void testValidate_AtLeastOneAllocationRequired() {
		InvestmentReplication replication = createInvestmentReplication();
		replication.setAllocationList(null);
		TestUtils.expectException(ValidationException.class, () -> createCalculator().validateInvestmentReplication(replication),
				"Selected Dynamic Replication Calculator requires a minimum of [1] allocation(s) defined.");
	}


	@Test
	public void testValidate_RollupReplicationTypeRequiredForBlendedCalculator() {
		InvestmentReplication replication = createInvestmentReplication();
		replication.getType().setRollupReplication(false);
		TestUtils.expectException(ValidationException.class, () -> createCalculator().validateInvestmentReplication(replication),
				String.format("Replication Type [%s] is not supported because Blended Replication Calculators only support Replication rollups.", replication.getType().getName()));
	}


	@Test
	public void testValidate_MissingChildReplication() {
		TestUtils.expectException(ValidationException.class, () -> createCalculator().validateInvestmentReplication(createInvestmentReplication()),
				"Blended Replications only support Replication Allocations that have a Child Replication.");
	}


	@Test
	public void testValidate_BlendedChildReplicationNotAllowed() {
		InvestmentReplication replication = createInvestmentReplication();
		replication.getAllocationList().get(0).setChildReplication(createInvestmentReplication());
		TestUtils.expectException(ValidationException.class, () -> createCalculator().validateInvestmentReplication(replication),
				"Blended Replications do not support Child Replications that are rollups.");
	}


	@Test
	public void testValidate_ChildReplicationWithDynamicCalculatorNotAllowed() {
		InvestmentReplication replication = createInvestmentReplication();
		InvestmentReplication childReplication = createInvestmentReplication();
		childReplication.setType(new InvestmentReplicationType());
		childReplication.setDynamicCalculatorBean(new SystemBean());
		replication.getAllocationList().get(0).setChildReplication(childReplication);
		TestUtils.expectException(ValidationException.class, () -> createCalculator().validateInvestmentReplication(replication),
				"Blended Replications do not support Child Replications that use a Dynamic Calculator.");
	}


	@Test
	public void testValidate_ChildReplicationWithReversedExposureSignNotAllowed() {
		InvestmentReplication replication = createInvestmentReplication();
		InvestmentReplication childReplication = createInvestmentReplication();
		childReplication.setType(new InvestmentReplicationType());
		childReplication.setReverseExposureSign(true);
		replication.getAllocationList().get(0).setChildReplication(childReplication);
		TestUtils.expectException(ValidationException.class, () -> createCalculator().validateInvestmentReplication(replication),
				"Blended Replications do not support Child Replications that have a reversed exposure sign.");
	}


	@Test
	public void testValidate_ChildReplicationWithPortfolioDefinedCurrentSecurityNotAllowed() {
		InvestmentReplication replication = createInvestmentReplication();
		InvestmentReplication childReplication = createInvestmentReplication();
		childReplication.setType(new InvestmentReplicationType());
		childReplication.getAllocationList().get(0).setPortfolioSelectedCurrentSecurity(true);
		replication.getAllocationList().get(0).setChildReplication(childReplication);
		TestUtils.expectException(ValidationException.class, () -> createCalculator().validateInvestmentReplication(replication),
				"Blended Replications do not support Child Replications that have an allocation with a portfolio defined Current Security.");
	}


	@Test
	public void testValidate_ChildReplicationWithMatchingReplicationInAllocationNotAllowed() {
		InvestmentReplication replication = createInvestmentReplication();
		InvestmentReplication childReplication = createInvestmentReplication();
		childReplication.setType(new InvestmentReplicationType());
		childReplication.getAllocationList().get(0).setMatchingReplication(createInvestmentReplication());
		replication.getAllocationList().get(0).setChildReplication(childReplication);
		TestUtils.expectException(ValidationException.class, () -> createCalculator().validateInvestmentReplication(replication),
				"Blended Replications do not support Child Replications that have an allocation with a Matching Replication.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentReplicationDynamicCalculator createCalculator() {
		return new InvestmentReplicationCalculatorBlended();
	}


	private InvestmentReplication createInvestmentReplication() {
		InvestmentReplication replication = new InvestmentReplication();
		replication.setType(createBlendedReplicationType());
		replication.setAllocationList(Arrays.asList(createReplicationAllocation()));
		return replication;
	}


	private InvestmentReplicationType createBlendedReplicationType() {
		InvestmentReplicationType replicationType = new InvestmentReplicationType();
		replicationType.setName("Blended");
		replicationType.setOrder(1000000);
		replicationType.setRollupReplication(true);
		return replicationType;
	}


	private InvestmentReplicationAllocation createReplicationAllocation() {
		return new InvestmentReplicationAllocation();
	}
}
