package com.clifton.investment.replication.dynamic;

import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.holiday.CalendarBusinessDayTypes;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class InvestmentReplicationCalculatorRollingFuturesTests {

	private InvestmentReplicationCalculatorRollingFutures investmentReplicationCalculatorVIX;
	@Mock
	private CalendarBusinessDayService calendarBusinessDayService;

	@Mock
	private CalendarSetupService calendarSetupService;

	private static InvestmentInstrument UX;
	private static InvestmentSecurity ESM3, UXZ1, UXF2, UXG2, UXH2, UXJ2, UXK2, UXM2, UXN2, UXQ2, UXS2;
	private static List<InvestmentSecurity> SECURITY_LIST = new ArrayList<>();


	static {
		UX = new InvestmentInstrument();
		UX.setId(400);
		UX.setIdentifierPrefix("UX");

		InvestmentInstrument ES = new InvestmentInstrument();
		ES.setId(410);
		ES.setIdentifierPrefix("ES");

		ESM3 = new InvestmentSecurity();
		ESM3.setCurrency(false);
		ESM3.setName("ESM3");
		ESM3.setSymbol("ESM3");
		ESM3.setCusip("ESM3");
		ESM3.setId(10);
		ESM3.setEndDate(DateUtils.toDate("12/20/2011"));
		ESM3.setInstrument(ES);

		UXZ1 = new InvestmentSecurity();
		UXZ1.setCurrency(false);
		UXZ1.setName("UXZ1");
		UXZ1.setSymbol("UXZ1");
		UXZ1.setCusip("UXZ1");
		UXZ1.setId(10);
		UXZ1.setEndDate(DateUtils.toDate("12/20/2011"));
		UXZ1.setInstrument(UX);

		UXF2 = new InvestmentSecurity();
		UXF2.setCurrency(false);
		UXF2.setName("UXF2");
		UXF2.setSymbol("UXF2");
		UXF2.setCusip("UXF2");
		UXF2.setId(20);
		UXF2.setEndDate(DateUtils.toDate("01/17/2012"));
		UXF2.setInstrument(UX);

		UXG2 = new InvestmentSecurity();
		UXG2.setCurrency(false);
		UXG2.setName("UXG2");
		UXG2.setSymbol("UXG2");
		UXG2.setCusip("UXG2");
		UXG2.setId(30);
		UXG2.setEndDate(DateUtils.toDate("02/14/2012"));
		UXG2.setInstrument(UX);

		UXH2 = new InvestmentSecurity();
		UXH2.setCurrency(false);
		UXH2.setName("UXH2");
		UXH2.setSymbol("UXH2");
		UXH2.setCusip("UXH2");
		UXH2.setId(35);
		UXH2.setEndDate(DateUtils.toDate("03/20/2012"));
		UXH2.setInstrument(UX);

		UXJ2 = new InvestmentSecurity();
		UXJ2.setCurrency(false);
		UXJ2.setName("UXJ2");
		UXJ2.setSymbol("UXJ2");
		UXJ2.setCusip("UXJ2");
		UXJ2.setId(40);
		UXJ2.setEndDate(DateUtils.toDate("04/17/2012"));
		UXJ2.setInstrument(UX);

		UXK2 = new InvestmentSecurity();
		UXK2.setCurrency(false);
		UXK2.setName("UXK2");
		UXK2.setSymbol("UXK2");
		UXK2.setCusip("UXK2");
		UXK2.setId(50);
		UXK2.setEndDate(DateUtils.toDate("05/15/2012"));
		UXK2.setInstrument(UX);

		UXM2 = new InvestmentSecurity();
		UXM2.setCurrency(false);
		UXM2.setName("UXM2");
		UXM2.setSymbol("UXM2");
		UXM2.setCusip("UXM2");
		UXM2.setId(60);
		UXM2.setEndDate(DateUtils.toDate("06/19/2012"));
		UXM2.setInstrument(UX);

		UXN2 = new InvestmentSecurity();
		UXN2.setCurrency(false);
		UXN2.setName("UXN2");
		UXN2.setSymbol("UXN2");
		UXN2.setCusip("UXN2");
		UXN2.setId(70);
		UXN2.setEndDate(DateUtils.toDate("07/17/2012"));
		UXN2.setInstrument(UX);

		UXQ2 = new InvestmentSecurity();
		UXQ2.setCurrency(false);
		UXQ2.setName("UXQ2");
		UXQ2.setSymbol("UXQ2");
		UXQ2.setCusip("UXQ2");
		UXQ2.setId(80);
		UXQ2.setEndDate(DateUtils.toDate("8/21/12"));
		UXQ2.setInstrument(UX);

		UXS2 = new InvestmentSecurity();
		UXS2.setCurrency(false);
		UXS2.setName("UXS2");
		UXS2.setSymbol("UXS2");
		UXS2.setCusip("UXS2");
		UXS2.setId(90);
		UXS2.setEndDate(DateUtils.toDate("9/21/12"));
		UXS2.setInstrument(UX);

		SECURITY_LIST.add(UXZ1);
		SECURITY_LIST.add(UXF2);
		SECURITY_LIST.add(UXG2);
		SECURITY_LIST.add(UXH2);
		SECURITY_LIST.add(UXJ2);
		SECURITY_LIST.add(UXK2);
		SECURITY_LIST.add(UXM2);
		SECURITY_LIST.add(UXN2);
		SECURITY_LIST.add(UXQ2);
		SECURITY_LIST.add(UXS2);
	}


	public void setupTest(int numberOfContacts, int numberOfMonthsToFirstContact) {
		this.investmentReplicationCalculatorVIX = Mockito.spy(new InvestmentReplicationCalculatorRollingFutures());

		// mock the business day services
		this.calendarBusinessDayService = Mockito.mock(CalendarBusinessDayService.class);
		// mock the calendar setup service
		this.calendarSetupService = Mockito.mock(CalendarSetupService.class);

		Date allocationTestDateActual1 = DateUtils.toDate("01/05/2012");
		Date allocationTestDateActual4 = DateUtils.toDate("01/06/2012");

		Date rollTestDateActual0 = DateUtils.toDate("01/11/2012");
		Date rollTestDateActual1 = DateUtils.toDate("01/12/2012");
		Date rollTestDateActual2 = DateUtils.toDate("01/13/2012");
		Date rollTestDateActual3 = DateUtils.toDate("01/17/2012");
		Date rollTestDateActual4 = DateUtils.toDate("01/18/2012");

		// mock the functions that return securities
		Mockito.doReturn(UXF2).when(this.investmentReplicationCalculatorVIX).getNextSecurity(ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.eq(DateUtils.toDate("12/20/2011")));
		Mockito.doReturn(UXG2).when(this.investmentReplicationCalculatorVIX).getNextSecurity(ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.eq(DateUtils.toDate("01/17/2012")));
		Mockito.doReturn(UXH2).when(this.investmentReplicationCalculatorVIX).getNextSecurity(ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.eq(DateUtils.toDate("02/14/2012")));
		Mockito.doReturn(UXJ2).when(this.investmentReplicationCalculatorVIX).getNextSecurity(ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.eq(DateUtils.toDate("03/20/2012")));
		Mockito.doReturn(UXK2).when(this.investmentReplicationCalculatorVIX).getNextSecurity(ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.eq(DateUtils.toDate("04/17/2012")));
		Mockito.doReturn(UXM2).when(this.investmentReplicationCalculatorVIX).getNextSecurity(ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.eq(DateUtils.toDate("05/15/2012")));
		Mockito.doReturn(UXN2).when(this.investmentReplicationCalculatorVIX).getNextSecurity(ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.eq(DateUtils.toDate("06/19/2012")));
		Mockito.doReturn(UXQ2).when(this.investmentReplicationCalculatorVIX).getNextSecurity(ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.eq(DateUtils.toDate("07/17/2012")));
		Mockito.doReturn(UXS2).when(this.investmentReplicationCalculatorVIX).getNextSecurity(ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.eq(DateUtils.toDate("08/21/2012")));


		// Previous (What we need to roll out of)
		Mockito.doReturn(null).when(this.investmentReplicationCalculatorVIX).getPreviousSecurity(UXZ1);
		Mockito.doReturn(UXZ1).when(this.investmentReplicationCalculatorVIX).getPreviousSecurity(UXF2);
		Mockito.doReturn(UXF2).when(this.investmentReplicationCalculatorVIX).getPreviousSecurity(UXG2);
		Mockito.doReturn(UXG2).when(this.investmentReplicationCalculatorVIX).getPreviousSecurity(UXH2);
		Mockito.doReturn(UXH2).when(this.investmentReplicationCalculatorVIX).getPreviousSecurity(UXJ2);
		Mockito.doReturn(UXJ2).when(this.investmentReplicationCalculatorVIX).getPreviousSecurity(UXK2);
		Mockito.doReturn(UXK2).when(this.investmentReplicationCalculatorVIX).getPreviousSecurity(UXM2);
		Mockito.doReturn(UXK2).when(this.investmentReplicationCalculatorVIX).getPreviousSecurity(UXM2);
		Mockito.doReturn(UXM2).when(this.investmentReplicationCalculatorVIX).getPreviousSecurity(UXN2);
		Mockito.doReturn(UXN2).when(this.investmentReplicationCalculatorVIX).getPreviousSecurity(UXQ2);
		Mockito.doReturn(UXQ2).when(this.investmentReplicationCalculatorVIX).getPreviousSecurity(UXS2);

		addRollDate(allocationTestDateActual1, 10, UXZ1, UXF2);
		addRollDate(allocationTestDateActual4, 11, UXZ1, UXF2);

		addRollDate(rollTestDateActual0, 14, UXZ1, UXF2);
		addRollDate(rollTestDateActual1, 15, UXZ1, UXF2);
		addRollDate(rollTestDateActual2, 16, UXZ1, UXF2);
		addRollDate(rollTestDateActual3, 0, UXF2, UXG2);
		addRollDate(rollTestDateActual4, 1, UXF2, UXG2);

		mockBusinessDayLookups();

		Mockito.when(this.calendarBusinessDayService.getBusinessDaysBetween(ArgumentMatchers.eq(UXZ1.getEndDate()), ArgumentMatchers.eq(UXF2.getEndDate()), ArgumentMatchers.nullable(CalendarBusinessDayTypes.class), ArgumentMatchers.anyShort())).thenReturn(17);
		Mockito.when(this.calendarBusinessDayService.getBusinessDaysBetween(ArgumentMatchers.eq(UXF2.getEndDate()), ArgumentMatchers.eq(UXG2.getEndDate()), ArgumentMatchers.nullable(CalendarBusinessDayTypes.class), ArgumentMatchers.anyShort())).thenReturn(20);

		this.investmentReplicationCalculatorVIX.setCalendarSetupService(this.calendarSetupService);
		this.investmentReplicationCalculatorVIX.setCalendarBusinessDayService(this.calendarBusinessDayService);

		this.investmentReplicationCalculatorVIX.setNumberOfContacts(numberOfContacts);
		this.investmentReplicationCalculatorVIX.setMonthsToFirstContact(numberOfMonthsToFirstContact);

		Mockito.doAnswer(new Answer<InvestmentSecurity>() {

			@Override
			public InvestmentSecurity answer(InvocationOnMock invocation) {
				Object[] args = invocation.getArguments();
				return getFirstSecurity((Date) args[1], (int) args[2]);
			}
		}).when(this.investmentReplicationCalculatorVIX).getFirstSecurity(ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyInt());
	}


	private void addRollDate(Date dateActual, int periodBusinessDays, InvestmentSecurity last, InvestmentSecurity next) {
		Mockito.doReturn(last).when(this.investmentReplicationCalculatorVIX).getLastMaturingSecurity(ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.eq(dateActual));
		Mockito.doReturn(next).when(this.investmentReplicationCalculatorVIX).getNextMaturingSecurity(ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.eq(dateActual));

		Mockito.when(this.calendarBusinessDayService.getBusinessDaysBetween(ArgumentMatchers.eq(last.getEndDate()), ArgumentMatchers.eq(dateActual), ArgumentMatchers.nullable(CalendarBusinessDayTypes.class), ArgumentMatchers.anyShort())).thenReturn(periodBusinessDays);
	}


	private InvestmentSecurity getFirstSecurity(Date dateActual, int numberOfMonthsToFirstContact) {
		int index = 0;
		for (InvestmentSecurity s : CollectionUtils.getIterable(SECURITY_LIST)) {
			if (DateUtils.compare(s.getEndDate(), dateActual, false) > 0) {
				return SECURITY_LIST.get(index + numberOfMonthsToFirstContact - 1);
			}
			index++;
		}
		return null;
	}


	private void mockBusinessDayLookups() {
		Calendar cal = new Calendar();
		cal.setId(MathUtils.SHORT_ONE);
		Mockito.when(this.calendarSetupService.getDefaultCalendar()).thenReturn(cal);

		Mockito.when(this.calendarBusinessDayService.getNextBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/03/2012"))))).thenReturn(DateUtils.toDate("01/04/2012"));
		Mockito.when(this.calendarBusinessDayService.getNextBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/04/2012"))))).thenReturn(DateUtils.toDate("01/05/2012"));
		Mockito.when(this.calendarBusinessDayService.getNextBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/05/2012"))))).thenReturn(DateUtils.toDate("01/06/2012"));
		Mockito.when(this.calendarBusinessDayService.getNextBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/06/2012"))))).thenReturn(DateUtils.toDate("01/09/2012"));

		Mockito.when(this.calendarBusinessDayService.getNextBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/11/2012"))))).thenReturn(DateUtils.toDate("01/12/2012"));
		Mockito.when(this.calendarBusinessDayService.getNextBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/12/2012"))))).thenReturn(DateUtils.toDate("01/13/2012"));
		Mockito.when(this.calendarBusinessDayService.getNextBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/13/2012"))))).thenReturn(DateUtils.toDate("01/17/2012"));
		Mockito.when(this.calendarBusinessDayService.getNextBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/14/2012"))))).thenReturn(DateUtils.toDate("01/17/2012"));
		Mockito.when(this.calendarBusinessDayService.getNextBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/15/2012"))))).thenReturn(DateUtils.toDate("01/17/2012"));
		Mockito.when(this.calendarBusinessDayService.getNextBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/16/2012"))))).thenReturn(DateUtils.toDate("01/17/2012"));
		Mockito.when(this.calendarBusinessDayService.getNextBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/17/2012"))))).thenReturn(DateUtils.toDate("01/18/2012"));
		Mockito.when(this.calendarBusinessDayService.getNextBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/18/2012"))))).thenReturn(DateUtils.toDate("01/19/2012"));

		Mockito.when(this.calendarBusinessDayService.getPreviousBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/04/2012"))))).thenReturn(DateUtils.toDate("01/03/2012"));
		Mockito.when(this.calendarBusinessDayService.getPreviousBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/05/2012"))))).thenReturn(DateUtils.toDate("01/05/2012"));
		Mockito.when(this.calendarBusinessDayService.getPreviousBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/06/2012"))))).thenReturn(DateUtils.toDate("01/06/2012"));
		Mockito.when(this.calendarBusinessDayService.getPreviousBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/07/2012"))))).thenReturn(DateUtils.toDate("01/09/2012"));

		Mockito.when(this.calendarBusinessDayService.getPreviousBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/12/2012"))))).thenReturn(DateUtils.toDate("01/11/2012"));
		Mockito.when(this.calendarBusinessDayService.getPreviousBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/13/2012"))))).thenReturn(DateUtils.toDate("01/12/2012"));
		Mockito.when(this.calendarBusinessDayService.getPreviousBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/14/2012"))))).thenReturn(DateUtils.toDate("01/13/2012"));
		Mockito.when(this.calendarBusinessDayService.getPreviousBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/15/2012"))))).thenReturn(DateUtils.toDate("01/13/2012"));
		Mockito.when(this.calendarBusinessDayService.getPreviousBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/16/2012"))))).thenReturn(DateUtils.toDate("01/13/2012"));
		Mockito.when(this.calendarBusinessDayService.getPreviousBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/17/2012"))))).thenReturn(DateUtils.toDate("01/13/2012"));
		Mockito.when(this.calendarBusinessDayService.getPreviousBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/18/2012"))))).thenReturn(DateUtils.toDate("01/17/2012"));
		Mockito.when(this.calendarBusinessDayService.getPreviousBusinessDay(ArgumentMatchers.argThat(arg -> arg != null && DateUtils.isEqualWithoutTime(arg.getDate(), DateUtils.toDate("01/19/2012"))))).thenReturn(DateUtils.toDate("01/18/2012"));
	}


	@Test
	public void testReplication() {
		setupTest(4, 4);

		InvestmentReplication replication = new InvestmentReplication();
		InvestmentReplicationAllocation allocation = new InvestmentReplicationAllocation();

		BigDecimal oneThird = new BigDecimal(1).divide(new BigDecimal(3), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100));

		replication.setType(new InvestmentReplicationType());
		replication.setAllocationList(new ArrayList<>());
		allocation.setReplicationInstrument(UX);
		replication.getAllocationList().add(allocation);

		// test a regular day
		List<InvestmentReplicationAllocation> replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/05/2012"));
		Assertions.assertEquals(5, replications.size());
		Assertions.assertEquals("UXJ2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXK2", replications.get(1).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXM2", replications.get(2).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXN2", replications.get(3).getReplicationSecurity().getSymbol());
		validateEmptyInstrumentAllocation(replications.get(4), UXH2);


		BigDecimal lastAllocation = new BigDecimal(11).divide(new BigDecimal(17), 4, RoundingMode.HALF_UP).divide(new BigDecimal(3), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100.0));
		BigDecimal firstAllocation = new BigDecimal(100.00).subtract(lastAllocation.add(oneThird).add(oneThird));

		Assertions.assertEquals(0, firstAllocation.compareTo(replications.get(0).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(1).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(2).getAllocationWeight()));
		Assertions.assertEquals(0, lastAllocation.compareTo(replications.get(3).getAllocationWeight()));

		// test a roll day
		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/12/2012"));
		Assertions.assertEquals(5, replications.size());
		Assertions.assertEquals("UXJ2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXK2", replications.get(1).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXM2", replications.get(2).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXN2", replications.get(3).getReplicationSecurity().getSymbol());
		validateEmptyInstrumentAllocation(replications.get(4), UXH2);

		lastAllocation = new BigDecimal(16).divide(new BigDecimal(17), 4, RoundingMode.HALF_UP).divide(new BigDecimal(3), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100.0));
		firstAllocation = new BigDecimal(100.00).subtract(lastAllocation.add(oneThird).add(oneThird));

		Assertions.assertEquals(0, firstAllocation.compareTo(replications.get(0).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(1).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(2).getAllocationWeight()));
		Assertions.assertEquals(0, lastAllocation.compareTo(replications.get(3).getAllocationWeight()));


		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/13/2012"));
		Assertions.assertEquals(5, replications.size());

		Assertions.assertEquals("UXK2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXM2", replications.get(1).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXN2", replications.get(2).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXQ2", replications.get(3).getReplicationSecurity().getSymbol());
		validateEmptyInstrumentAllocation(replications.get(4), UXJ2);

		lastAllocation = new BigDecimal(0).divide(new BigDecimal(20), 4, RoundingMode.HALF_UP).divide(new BigDecimal(3), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100.0));
		firstAllocation = new BigDecimal(100.00).subtract(lastAllocation.add(oneThird).add(oneThird));

		Assertions.assertEquals(0, firstAllocation.compareTo(replications.get(0).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(1).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(2).getAllocationWeight()));
		Assertions.assertEquals(0, lastAllocation.compareTo(replications.get(3).getAllocationWeight()));
		Assertions.assertEquals(0, BigDecimal.ZERO.compareTo(replications.get(4).getAllocationWeight()));

		// test a roll day
		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/17/2012"));
		Assertions.assertEquals(5, replications.size());

		Assertions.assertEquals("UXK2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXM2", replications.get(1).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXN2", replications.get(2).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXQ2", replications.get(3).getReplicationSecurity().getSymbol());
		validateEmptyInstrumentAllocation(replications.get(4), UXJ2);

		lastAllocation = new BigDecimal(1).divide(new BigDecimal(20), 4, RoundingMode.HALF_UP).divide(new BigDecimal(3), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100.0));
		firstAllocation = new BigDecimal(100.00).subtract(lastAllocation.add(oneThird).add(oneThird));

		Assertions.assertEquals(0, firstAllocation.compareTo(replications.get(0).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(1).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(2).getAllocationWeight()));
		Assertions.assertEquals(0, lastAllocation.compareTo(replications.get(3).getAllocationWeight()));
	}


	@Test
	public void testReplicationWithUnrolledSecurities() {
		setupTest(4, 4);

		InvestmentReplication replication = new InvestmentReplication();
		InvestmentReplicationAllocation allocation = new InvestmentReplicationAllocation();

		BigDecimal oneThird = new BigDecimal(1).divide(new BigDecimal(3), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100));

		replication.setType(new InvestmentReplicationType());
		replication.setAllocationList(new ArrayList<>());
		allocation.setReplicationInstrument(UX);
		replication.getAllocationList().add(allocation);


		// test a regular day
		List<InvestmentReplicationAllocation> replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/05/2012"));
		Assertions.assertEquals(5, replications.size());
		Assertions.assertEquals("UXJ2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXK2", replications.get(1).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXM2", replications.get(2).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXN2", replications.get(3).getReplicationSecurity().getSymbol());
		validateEmptyInstrumentAllocation(replications.get(4), UXH2);

		BigDecimal lastAllocation = new BigDecimal(11).divide(new BigDecimal(17), 4, RoundingMode.HALF_UP).divide(new BigDecimal(3), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100.0));
		BigDecimal firstAllocation = new BigDecimal(100.00).subtract(lastAllocation.add(oneThird).add(oneThird));

		Assertions.assertEquals(0, firstAllocation.compareTo(replications.get(0).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(1).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(2).getAllocationWeight()));
		Assertions.assertEquals(0, lastAllocation.compareTo(replications.get(3).getAllocationWeight()));
		Assertions.assertEquals(0, BigDecimal.ZERO.compareTo(replications.get(4).getAllocationWeight()));

		// test a roll day with currently held securities
		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/13/2012"));
		Assertions.assertEquals(5, replications.size());

		Assertions.assertEquals("UXK2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXM2", replications.get(1).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXN2", replications.get(2).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXQ2", replications.get(3).getReplicationSecurity().getSymbol());
		validateEmptyInstrumentAllocation(replications.get(4), UXJ2);

		lastAllocation = new BigDecimal(0).divide(new BigDecimal(20), 4, RoundingMode.HALF_UP).divide(new BigDecimal(3), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100.0));
		firstAllocation = new BigDecimal(100.00).subtract(lastAllocation.add(oneThird).add(oneThird));

		Assertions.assertEquals(0, firstAllocation.compareTo(replications.get(0).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(1).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(2).getAllocationWeight()));
		Assertions.assertEquals(0, lastAllocation.compareTo(replications.get(3).getAllocationWeight()));
		Assertions.assertEquals(0, BigDecimal.ZERO.compareTo(replications.get(4).getAllocationWeight()));

		// test a roll day with currently held securities
		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/13/2012"));
		Assertions.assertEquals(5, replications.size());

		Assertions.assertEquals("UXK2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXM2", replications.get(1).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXN2", replications.get(2).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXQ2", replications.get(3).getReplicationSecurity().getSymbol());
		validateEmptyInstrumentAllocation(replications.get(4), UXJ2);

		lastAllocation = new BigDecimal(0).divide(new BigDecimal(20), 4, RoundingMode.HALF_UP).divide(new BigDecimal(3), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100.0));
		firstAllocation = new BigDecimal(100.00).subtract(lastAllocation.add(oneThird).add(oneThird));

		Assertions.assertEquals(0, firstAllocation.compareTo(replications.get(0).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(1).getAllocationWeight()));
		Assertions.assertEquals(0, oneThird.compareTo(replications.get(2).getAllocationWeight()));
		Assertions.assertEquals(0, lastAllocation.compareTo(replications.get(3).getAllocationWeight()));
		Assertions.assertEquals(0, BigDecimal.ZERO.compareTo(replications.get(4).getAllocationWeight()));
	}


	@Test
	public void testReplicationOneMonthOutTwoContacts() {
		setupTest(2, 1);

		InvestmentReplication replication = new InvestmentReplication();
		InvestmentReplicationAllocation allocation = new InvestmentReplicationAllocation();

		replication.setType(new InvestmentReplicationType());
		replication.setAllocationList(new ArrayList<>());
		allocation.setReplicationInstrument(UX);
		replication.getAllocationList().add(allocation);

		// test a regular day
		List<InvestmentReplicationAllocation> replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/05/2012"));
		Assertions.assertEquals(2, replications.size());
		Assertions.assertEquals("UXF2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXG2", replications.get(1).getReplicationSecurity().getSymbol());
		// Previous UXZ1 doesn't apply because it's not active on 1/5/2012

		Assertions.assertEquals("35.29", CoreMathUtils.formatNumber(replications.get(0).getAllocationWeight(), "#,###.####"));
		Assertions.assertEquals("64.71", CoreMathUtils.formatNumber(replications.get(1).getAllocationWeight(), "#,###.####"));

		// test a roll day
		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/12/2012"));
		Assertions.assertEquals(2, replications.size());
		Assertions.assertEquals("UXF2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXG2", replications.get(1).getReplicationSecurity().getSymbol());
		// Previous UXZ1 doesn't apply because it's not active on 1/12/2012

		Assertions.assertEquals("5.88", CoreMathUtils.formatNumber(replications.get(0).getAllocationWeight(), "#,###.####"));
		Assertions.assertEquals("94.12", CoreMathUtils.formatNumber(replications.get(1).getAllocationWeight(), "#,###.####"));

		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/13/2012"));
		Assertions.assertEquals(3, replications.size());

		Assertions.assertEquals("UXG2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXH2", replications.get(1).getReplicationSecurity().getSymbol());
		validateEmptyInstrumentAllocation(replications.get(2), UXF2);

		Assertions.assertEquals("100", CoreMathUtils.formatNumber(replications.get(0).getAllocationWeight(), "#,###.####"));
		Assertions.assertEquals("0", CoreMathUtils.formatNumber(replications.get(1).getAllocationWeight(), "#,###.####"));
		Assertions.assertEquals("0", CoreMathUtils.formatNumber(replications.get(2).getAllocationWeight(), "#,###.####"));

		// test a roll day
		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/17/2012"));
		Assertions.assertEquals(3, replications.size());

		Assertions.assertEquals("UXG2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXH2", replications.get(1).getReplicationSecurity().getSymbol());
		validateEmptyInstrumentAllocation(replications.get(2), UXF2);

		Assertions.assertEquals("95", CoreMathUtils.formatNumber(replications.get(0).getAllocationWeight(), "#,###.####"));
		Assertions.assertEquals("5", CoreMathUtils.formatNumber(replications.get(1).getAllocationWeight(), "#,###.####"));
	}


	@Test
	public void testReplicationOneMonthOutTwoContactsWithUnrolledSecurities() {
		setupTest(2, 1);

		InvestmentReplication replication = new InvestmentReplication();
		InvestmentReplicationAllocation allocation = new InvestmentReplicationAllocation();

		replication.setType(new InvestmentReplicationType());
		replication.setAllocationList(new ArrayList<>());
		allocation.setReplicationInstrument(UX);
		replication.getAllocationList().add(allocation);

		// test a regular day
		List<InvestmentReplicationAllocation> replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/05/2012"));
		Assertions.assertEquals(2, replications.size());
		Assertions.assertEquals("UXF2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXG2", replications.get(1).getReplicationSecurity().getSymbol());
		// Previous UXZ1 doesn't apply because it's not active on 1/5/2012

		Assertions.assertEquals("35.29", CoreMathUtils.formatNumber(replications.get(0).getAllocationWeight(), "#,###.####"));
		Assertions.assertEquals("64.71", CoreMathUtils.formatNumber(replications.get(1).getAllocationWeight(), "#,###.####"));

		// test a roll day
		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/12/2012"));
		Assertions.assertEquals(2, replications.size());
		Assertions.assertEquals("UXF2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXG2", replications.get(1).getReplicationSecurity().getSymbol());
		// Previous UXZ1 doesn't apply because it's not active on 1/12/2012

		Assertions.assertEquals("5.88", CoreMathUtils.formatNumber(replications.get(0).getAllocationWeight(), "#,###.####"));
		Assertions.assertEquals("94.12", CoreMathUtils.formatNumber(replications.get(1).getAllocationWeight(), "#,###.####"));

		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/13/2012"));
		Assertions.assertEquals(3, replications.size());

		Assertions.assertEquals("UXG2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXH2", replications.get(1).getReplicationSecurity().getSymbol());
		validateEmptyInstrumentAllocation(replications.get(2), UXF2);

		Assertions.assertEquals("100", CoreMathUtils.formatNumber(replications.get(0).getAllocationWeight(), "#,###.####"));
		Assertions.assertEquals("0", CoreMathUtils.formatNumber(replications.get(1).getAllocationWeight(), "#,###.####"));
		Assertions.assertEquals("0", CoreMathUtils.formatNumber(replications.get(2).getAllocationWeight(), "#,###.####"));


		// test a roll day
		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/17/2012"));
		Assertions.assertEquals(3, replications.size());

		Assertions.assertEquals("UXG2", replications.get(0).getReplicationSecurity().getSymbol());
		Assertions.assertEquals("UXH2", replications.get(1).getReplicationSecurity().getSymbol());
		validateEmptyInstrumentAllocation(replications.get(2), UXF2);

		Assertions.assertEquals("95", CoreMathUtils.formatNumber(replications.get(0).getAllocationWeight(), "#,###.####"));
		Assertions.assertEquals("5", CoreMathUtils.formatNumber(replications.get(1).getAllocationWeight(), "#,###.####"));
		Assertions.assertEquals("0", CoreMathUtils.formatNumber(replications.get(2).getAllocationWeight(), "#,###.####"));
	}


	@Test
	public void testReplicationOneMonthOutOneContacts() {
		setupTest(1, 1);

		InvestmentReplication replication = new InvestmentReplication();
		InvestmentReplicationAllocation allocation = new InvestmentReplicationAllocation();

		replication.setType(new InvestmentReplicationType());
		replication.setAllocationList(new ArrayList<>());
		allocation.setReplicationInstrument(UX);
		replication.getAllocationList().add(allocation);

		// test a regular day
		List<InvestmentReplicationAllocation> replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/05/2012"));
		Assertions.assertEquals(1, replications.size());
		Assertions.assertEquals("UXF2", replications.get(0).getReplicationSecurity().getSymbol());
		// Previous UXZ1 doesn't apply because it's not active on 1/5/2012

		Assertions.assertEquals("100", CoreMathUtils.formatNumber(replications.get(0).getAllocationWeight(), "#,###.####"));

		// test a roll day
		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/12/2012"));
		Assertions.assertEquals(1, replications.size());
		Assertions.assertEquals("UXF2", replications.get(0).getReplicationSecurity().getSymbol());
		// Previous UXZ1 doesn't apply because it's not active on 1/12/2012

		Assertions.assertEquals("100", CoreMathUtils.formatNumber(replications.get(0).getAllocationWeight(), "#,###.####"));

		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/13/2012"));
		Assertions.assertEquals(2, replications.size());

		Assertions.assertEquals("UXG2", replications.get(0).getReplicationSecurity().getSymbol());
		validateEmptyInstrumentAllocation(replications.get(1), UXF2);

		Assertions.assertEquals("100", CoreMathUtils.formatNumber(replications.get(0).getAllocationWeight(), "#,###.####"));
		Assertions.assertEquals("0", CoreMathUtils.formatNumber(replications.get(1).getAllocationWeight(), "#,###.####"));

		// test a roll day
		replications = this.investmentReplicationCalculatorVIX.calculate(replication, DateUtils.toDate("01/17/2012"));
		Assertions.assertEquals(2, replications.size());

		Assertions.assertEquals("UXG2", replications.get(0).getReplicationSecurity().getSymbol());
		validateEmptyInstrumentAllocation(replications.get(1), UXF2);

		Assertions.assertEquals("100", CoreMathUtils.formatNumber(replications.get(0).getAllocationWeight(), "#,###.####"));
	}


	private void validateEmptyInstrumentAllocation(InvestmentReplicationAllocation allocation, InvestmentSecurity expectedZeroSecurity) {
		Assertions.assertEquals("UX", allocation.getReplicationInstrument().getIdentifierPrefix());
		Assertions.assertNotNull(allocation.getReplicationInstrument());
		Assertions.assertEquals(expectedZeroSecurity, allocation.getReplicationSecurity());
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, allocation.getAllocationWeight()));
	}
}
