package com.clifton.investment.expression.value;

import com.clifton.core.converter.template.FreemarkerTemplateConverter;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class InvestmentExpressionEvalTests {

	@Test
	public void evaluateExpression() {
		FreemarkerTemplateConverter ftc = new FreemarkerTemplateConverter();
		String valueExpression = "(${SW_VAL_PREMIUM} + ${SW_PAY_NOTL_AMT}) / ${SW_PAY_NOTL_AMT} / ${investmentSecurity.instrument.priceMultiplier}";

		Set<String> fields = ftc.extractAllDataFields(valueExpression);
		Map<String, Object> variables = new HashMap<>();
		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(18356, "ESU3", InvestmentType.FUTURES, new BigDecimal("50"), "USD");

		for (String field : fields) {
			if ("investmentSecurity.instrument.priceMultiplier".equals(field)) {
				variables.put("investmentSecurity", investmentSecurity);
			}
			else {
				variables.put(field, (1 + (Math.random() * ((100 - 1) + 1))));
			}
		}

		String convertedExpression = ftc.convertExpression(valueExpression, variables);

		BigDecimal value = CoreMathUtils.evaluateExpression(convertedExpression).setScale(3, RoundingMode.HALF_UP);

		BigDecimal compareToValue = new BigDecimal((Double) variables.get("SW_VAL_PREMIUM"));
		compareToValue = compareToValue.add(new BigDecimal((Double) variables.get("SW_PAY_NOTL_AMT")), MathContext.DECIMAL128);
		compareToValue = compareToValue.divide((new BigDecimal((Double) variables.get("SW_PAY_NOTL_AMT"))), MathContext.DECIMAL128);
		compareToValue = compareToValue.divide(((((InvestmentSecurity) variables.get("investmentSecurity")).getPriceMultiplier())), MathContext.DECIMAL128).setScale(3, RoundingMode.HALF_UP);
		Assertions.assertEquals(0, value.compareTo(compareToValue));
	}


	@Test
	public void freemarkerBeanTest() {
		FreemarkerTemplateConverter ftc = new FreemarkerTemplateConverter();
		InvestmentSecurity investmentSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(18356, "ESU3", InvestmentType.FUTURES, new BigDecimal("50"), "USD");

		Map<String, Object> variables = new HashMap<>();

		variables.put("investmentSecurity", investmentSecurity);
		variables.put("SW_VAL_PREMIUM", 5);
		variables.put("SW_PAY_NOTL_AMT", 5);

		TemplateConfig config = new TemplateConfig("(${SW_VAL_PREMIUM} + ${SW_PAY_NOTL_AMT}) / ${SW_PAY_NOTL_AMT} / ${investmentSecurity.instrument.priceMultiplier}");
		config.getContext().setBeanMap(variables);

		String convertedExpression = ftc.convert(config);

		Assertions.assertTrue("(5 + 5) / 5 / 50".equals(convertedExpression));
	}
}
