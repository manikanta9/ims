package com.clifton.investment.manager.balance;


import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>InvestmentManagerAccountBalanceServiceImplTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentManagerAccountBalanceServiceImplTests {

	@Resource
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;


	@Test
	public void testInsertSystemDefinedType() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			InvestmentManagerAccountBalanceAdjustmentType type = new InvestmentManagerAccountBalanceAdjustmentType();
			type.setName("Test");
			type.setSystemDefined(true);

			this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalanceAdjustmentType(type);
		}, "You cannot enter a new system defined adjustment type.");
	}


	@Test
	public void testDeleteSystemDefinedType() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentManagerAccountBalanceAdjustmentType type = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName(InvestmentManagerAccountBalanceAdjustmentType.PROXY_ADJUSTMENTS);
			this.investmentManagerAccountBalanceService.deleteInvestmentManagerAccountBalanceAdjustmentType(type.getId());
		}, "You cannot edit or delete a system defined adjustment type.");
	}


	@Test
	public void testUpdateSystemDefinedType() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentManagerAccountBalanceAdjustmentType type = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName(InvestmentManagerAccountBalanceAdjustmentType.PROXY_ADJUSTMENTS);
			type.setName("test");
			type.setCashAdjustmentAllowed(!type.isCashAdjustmentAllowed());

			this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalanceAdjustmentType(type);
		}, "You cannot edit or delete a system defined adjustment type.");
	}


	@Test
	public void testChangeSystemDefinedType() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentManagerAccountBalanceAdjustmentType type = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName(InvestmentManagerAccountBalanceAdjustmentType.PROXY_ADJUSTMENTS);
			type.setSystemDefined(false);

			this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalanceAdjustmentType(type);
		}, "You cannot edit or delete a system defined adjustment type.");
	}


	@Test
	public void testInsertType_NoCashOrSecurities() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentManagerAccountBalanceAdjustmentType type = new InvestmentManagerAccountBalanceAdjustmentType();
			type.setName("Test");

			this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalanceAdjustmentType(type);
		}, "At least one of Cash and/or Securities adjustments must be allowed.");
	}


	@Test
	public void testNextDayTypeUpdate_CashInvalid() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentManagerAccountBalanceAdjustmentType type = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName("MOC");

			InvestmentManagerAccountBalanceAdjustmentType nextDayType = type.getNextDayAdjustmentType();
			nextDayType.setCashAdjustmentAllowed(false);

			this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalanceAdjustmentType(nextDayType);
		}, "Adjustment Type [MOC] allows cash adjustments.  Next Day Adjustment Type [Previous Day MOC] must also allow cash adjustments.");
	}


	@Test
	public void testNextDayTypeUpdate_SecuritiesInvalid() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentManagerAccountBalanceAdjustmentType type = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName("MOC");

			InvestmentManagerAccountBalanceAdjustmentType nextDayType = type.getNextDayAdjustmentType();
			nextDayType.setSecuritiesAdjustmentAllowed(false);

			this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalanceAdjustmentType(nextDayType);
		}, "Adjustment Type [MOC] allows securities adjustments.  Next Day Adjustment Type [Previous Day MOC] must also allow securities adjustments.");
	}


	@Test
	public void testNextDayTypeSelection_CashInvalid() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentManagerAccountBalanceAdjustmentType type = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName("MOC");
			InvestmentManagerAccountBalanceAdjustmentType newType = new InvestmentManagerAccountBalanceAdjustmentType();
			newType.setName("test");
			newType.setSecuritiesAdjustmentAllowed(true);

			this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalanceAdjustmentType(newType);
			type.setNextDayAdjustmentType(newType);
			this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalanceAdjustmentType(type);
		}, "Adjustment Type [MOC] allows cash adjustments.  Next Day Adjustment Type [test] must also allow cash adjustments.");
	}


	@Test
	public void testNextDayTypeSelection_SecuritiesInvalid() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentManagerAccountBalanceAdjustmentType type = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName("MOC");
			InvestmentManagerAccountBalanceAdjustmentType newType = new InvestmentManagerAccountBalanceAdjustmentType();
			newType.setName("test");
			newType.setCashAdjustmentAllowed(true);

			this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalanceAdjustmentType(newType);
			type.setNextDayAdjustmentType(newType);
			this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalanceAdjustmentType(type);
		}, "Adjustment Type [MOC] allows securities adjustments.  Next Day Adjustment Type [test] must also allow securities adjustments.");
	}


	@Test
	public void testNextDayTypeSelection_SystemDefinedInvalid() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentManagerAccountBalanceAdjustmentType type = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName("MOC");
			type.setNextDayAdjustmentType(this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName(InvestmentManagerAccountBalanceAdjustmentType.M2M));
			this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalanceAdjustmentType(type);
		}, "Adjustment Type [Adding M2M] cannot be used as a next day adjustment because it is system defined.");
	}
}
