package com.clifton.investment.manager.upload;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.investment.manager.InvestmentManagerAccountAllocation;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.search.InvestmentManagerAccountAllocationSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentManagerAccountUploadServiceImplTests {

	@Resource
	private InvestmentManagerAccountUploadService investmentManagerAccountUploadService;

	@Resource
	private InvestmentManagerAccountService investmentManagerAccountService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInvestmentManagerAccountUploadFile() {

		// Make sure starting with expected allocations
		Assertions.assertEquals("[[M00001] - [Client01: ]] - [Domestic Equity] 50\n" +
				"[[M00001] - [Client01: ]] - [International Equity] 50\n" +
				"[[M00002] - [Client01: ]] - [Fixed Income] 100\n" +
				"[[M00003] - [Client01: ]] - [Cash] 100\n" +
				"[[M00002] - [Client02: ]] - [Domestic Equity] 5\n" +
				"[[M00002] - [Client02: ]] - [Fixed Income] 20\n" +
				"[[M00003] - [Client02: ]] - [Commodities] 100\n" +
				"[[M00004] - [Client02: ]] - [Commodities] 100\n" +
				"[[M00003] - [Client03: ]] - [Equity] 100\n" +
				"[[M00004] - [Client03: ]] - [Equity] 100\n" +
				"[[M00005] - [Client03: ]] - [Equity] 100\n", getInvestmentManagerAssignmentAllocationListAsString());

		InvestmentManagerAccountAllocationUploadCommand uploadCommand = new InvestmentManagerAccountAllocationUploadCommand();
		uploadCommand.setPartialUploadAllowed(false);
		uploadCommand.setFile(new MultipartFileImpl("src/test/java/com/clifton/investment/manager/upload/InvestmentManagerAccountAllocationUploadTests.xls"));

		this.investmentManagerAccountUploadService.uploadInvestmentManagerAccountAllocationUploadFile(uploadCommand);
		TestUtils.validateUploadResultStringsIgnoringOrder("No Manager Assignments Updated.\n" +
				"Manager Assignment [[M00002] - [Client01: ]] Errors: There is not an existing allocation for asset class [Equity], and the option to insert new allocations is turned off.\n" +
				"Manager Assignment [[M00004] - [Client02: ]] Errors: There is not an existing allocation for asset class [Fixed Income], and the option to insert new allocations is turned off.\n" +
				"Manager Assignment [[M00003] - [Client01: ]] Errors: There is not an existing allocation for asset class [Fixed Income], and the option to insert new allocations is turned off.\n" +
				"Manager Assignment [[M00003] - [Client02: ]] Errors: There is not an existing allocation for asset class [International Equity], and the option to insert new allocations is turned off.\n" +
				"Manager Assignment [[M00004] - [Client03: ]] Errors: There is not an existing allocation for asset class [Cash], and the option to insert new allocations is turned off.\n" +
				"Manager Assignment [[M00005] - [Client03: ]] Errors: The total allocated is 50 but you have chosen to enforce total to 100.  Please double check your file.  If you are not allowing removal of existing allocations - those asset classes not included in the file will be included at their existing percentage.\n" +
				"Manager Assignment [[M00001] - [Client01: ]] Errors: There is not an existing allocation for asset class [Commodities], and the option to insert new allocations is turned off.\n" +
				"Cannot find manager assignment for client account [Client03] and manager account [M00001]", uploadCommand.getUploadResultsString().replaceAll(StringUtils.NEW_LINE, "\n"));


		uploadCommand.setAllowAddingOrRemovingAllocations(true);
		this.investmentManagerAccountUploadService.uploadInvestmentManagerAccountAllocationUploadFile(uploadCommand);
		TestUtils.validateUploadResultStringsIgnoringOrder("No Manager Assignments Updated.\n" +
				"Manager Assignment [[M00002] - [Client01: ]] Errors: Cannot find active non-rollup asset class associated with this account with name [Equity]\n" +
				"Manager Assignment [[M00003] - [Client02: ]] Errors: Cannot find active non-rollup asset class associated with this account with name [International Equity]\n" +
				"Manager Assignment [[M00005] - [Client03: ]] Errors: The total allocated is 50 but you have chosen to enforce total to 100.  Please double check your file.  If you are not allowing removal of existing allocations - those asset classes not included in the file will be included at their existing percentage.\n" +
				"Manager Assignment [[M00001] - [Client01: ]] Errors: Cannot find active non-rollup asset class associated with this account with name [Commodities]\n" +
				"Cannot find manager assignment for client account [Client03] and manager account [M00001]", uploadCommand.getUploadResultsString().replaceAll(StringUtils.NEW_LINE, "\n"));


		uploadCommand.setAllowPartiallyAllocatedManagers(true);
		this.investmentManagerAccountUploadService.uploadInvestmentManagerAccountAllocationUploadFile(uploadCommand);
		TestUtils.validateUploadResultStringsIgnoringOrder("No Manager Assignments Updated.\n" +
				"Manager Assignment [[M00002] - [Client01: ]] Errors: Cannot find active non-rollup asset class associated with this account with name [Equity]\n" +
				"Manager Assignment [[M00003] - [Client02: ]] Errors: Cannot find active non-rollup asset class associated with this account with name [International Equity]\n" +
				"Manager Assignment [[M00001] - [Client01: ]] Errors: Cannot find active non-rollup asset class associated with this account with name [Commodities]\n" +
				"Cannot find manager assignment for client account [Client03] and manager account [M00001]", uploadCommand.getUploadResultsString().replaceAll(StringUtils.NEW_LINE, "\n"));


		uploadCommand.setPartialUploadAllowed(true);
		this.investmentManagerAccountUploadService.uploadInvestmentManagerAccountAllocationUploadFile(uploadCommand);
		TestUtils.validateUploadResultStringsIgnoringOrder("5 Manager Assignments Updated.\n" +
				"Manager Assignment [[M00002] - [Client01: ]] Errors: Cannot find active non-rollup asset class associated with this account with name [Equity]\n" +
				"Manager Assignment [[M00003] - [Client02: ]] Errors: Cannot find active non-rollup asset class associated with this account with name [International Equity]\n" +
				"Manager Assignment [[M00001] - [Client01: ]] Errors: Cannot find active non-rollup asset class associated with this account with name [Commodities]\n" +
				"Cannot find manager assignment for client account [Client03] and manager account [M00001]", uploadCommand.getUploadResultsString().replaceAll(StringUtils.NEW_LINE, "\n"));


		// Validate Final Results
		Assertions.assertEquals("[[M00001] - [Client01: ]] - [Domestic Equity] 50\n" +
				"[[M00001] - [Client01: ]] - [International Equity] 50\n" +
				"[[M00002] - [Client01: ]] - [Fixed Income] 100\n" +
				"[[M00003] - [Client01: ]] - [Cash] 50\n" +
				"[[M00003] - [Client01: ]] - [Fixed Income] 50\n" +
				"[[M00002] - [Client02: ]] - [Domestic Equity] 25\n" +
				"[[M00002] - [Client02: ]] - [Fixed Income] 75\n" +
				"[[M00003] - [Client02: ]] - [Commodities] 100\n" +
				"[[M00004] - [Client02: ]] - [Fixed Income] 100\n" +
				"[[M00003] - [Client03: ]] - [Equity] 100\n" +
				"[[M00004] - [Client03: ]] - [Cash] 50\n" +
				"[[M00004] - [Client03: ]] - [Equity] 50\n" +
				"[[M00005] - [Client03: ]] - [Equity] 50\n", getInvestmentManagerAssignmentAllocationListAsString());
	}


	private String getInvestmentManagerAssignmentAllocationListAsString() {
		List<InvestmentManagerAccountAllocation> allocationList = this.investmentManagerAccountService.getInvestmentManagerAccountAllocationList(new InvestmentManagerAccountAllocationSearchForm());
		allocationList = BeanUtils.sortWithFunctions(allocationList, CollectionUtils.createList(
				managerAccountAllocation -> managerAccountAllocation.getManagerAccountAssignment().getReferenceTwo().getNumber(),
				managerAccountAllocation -> managerAccountAllocation.getManagerAccountAssignment().getReferenceOne().getAccountNumber(),
				managerAccountAllocation -> managerAccountAllocation.getAssetClass().getName()),
				CollectionUtils.createList(true, true, true));
		StringBuilder resultString = new StringBuilder(50);
		for (InvestmentManagerAccountAllocation managerAccountAllocation : allocationList) {
			resultString.append(managerAccountAllocation.getLabel()).append(" ").append(CoreMathUtils.formatNumberDecimal(managerAccountAllocation.getAllocationPercent())).append("\n");
		}
		return resultString.toString();
	}
}
