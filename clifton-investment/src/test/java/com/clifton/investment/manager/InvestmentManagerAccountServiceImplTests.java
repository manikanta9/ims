package com.clifton.investment.manager;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientService;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.business.service.BusinessServiceProcessingTypeBuilder;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.AggregationOperations;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.manager.InvestmentManagerAccount.ManagerTypes;
import com.clifton.investment.manager.cache.InvestmentManagerAccountRollupCache;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.investment.rates.InvestmentRatesService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentManagerAccountServiceImplTests</code> ...
 *
 * @author Mary Anderson
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class InvestmentManagerAccountServiceImplTests {

	@Resource
	private InvestmentManagerAccountService investmentManagerAccountService;

	@Resource
	private BusinessClientService businessClientService;

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentManagerAccountRollupCache investmentManagerAccountRollupCache;

	@Resource
	private InvestmentRatesService investmentRatesService;

	@Resource
	private InvestmentAccountAssetClassService investmentAccountAssetClassService;


	@Test
	public void testChangeManagerCompany() {
		InvestmentManagerAccount ma = this.investmentManagerAccountService.getInvestmentManagerAccount(11);

		// Change the manager Should NOT fail because this is the only manager using this
		// custodian info
		ma.setManagerCompany(this.businessCompanyService.getBusinessCompany(211));

		this.investmentManagerAccountService.saveInvestmentManagerAccount(ma);
	}


	@Test
	public void testInactiveManagerWithActiveAssignments() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentManagerAccount manager = this.investmentManagerAccountService.getInvestmentManagerAccount(11);
			manager.setInactive(true);
			this.investmentManagerAccountService.saveInvestmentManagerAccount(manager);
		}, "You cannot de-activate a manager account that has active assignments.  Please de-activate all active assignments [Arizo1] before de-activating the manager.");
	}


	@Test
	public void testProxyManagerWithNoValue() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			InvestmentManagerAccount manager = new InvestmentManagerAccount();
			manager.setProxyManager(true);
			manager.setAccountNumber("PROXY1");
			this.investmentManagerAccountService.saveInvestmentManagerAccount(manager);
		}, "Proxy Managers must have a value or Benchmark and quantity entered. Please enter Proxy info or uncheck the Proxy Manager checkbox.");
	}


	@Test
	public void testProxyManagerWithValueAndNoDateAndNoSecurity() {
		InvestmentManagerAccount manager = new InvestmentManagerAccount();
		manager.setProxyManager(true);
		manager.setAccountNumber("PROXY1");
		manager.setProxyValue(BigDecimal.valueOf(5000));
		this.investmentManagerAccountService.saveInvestmentManagerAccount(manager);
	}


	@Test
	public void testProxyManagerWithValueAndNoDateAndSecurity() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			InvestmentManagerAccount manager = new InvestmentManagerAccount();
			manager.setProxyManager(true);
			manager.setAccountNumber("PROXY1");
			manager.setProxyValue(BigDecimal.valueOf(5000));
			manager.setProxyBenchmarkSecurity(this.investmentInstrumentService.getInvestmentSecurity(8));
			manager.setBaseCurrency(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USD", true));
			this.investmentManagerAccountService.saveInvestmentManagerAccount(manager);
		}, "This manager will apply a balance adjustment based on the selected security or index.  Proxy Last Updated Date is required in order to calculate this adjustment.");
	}


	@Test
	public void testProxyManagerWithNoDateAndSecurityAndQuantity() {
		InvestmentManagerAccount manager = new InvestmentManagerAccount();
		manager.setProxyManager(true);
		manager.setAccountNumber("PROXY1");
		manager.setProxySecurityQuantity(BigDecimal.valueOf(5000));
		manager.setProxyBenchmarkSecurity(this.investmentInstrumentService.getInvestmentSecurity(8));
		manager = this.investmentManagerAccountService.saveInvestmentManagerAccount(manager);

		// Clean up - delete the manager
		this.investmentManagerAccountService.deleteInvestmentManagerAccount(manager.getId());
	}


	@Test
	public void testProxyManagerWithValueAndNoDateAndIndex() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			InvestmentManagerAccount manager = new InvestmentManagerAccount();
			manager.setProxyManager(true);
			manager.setAccountNumber("PROXY1");
			manager.setProxyValue(BigDecimal.valueOf(5000));
			manager.setProxyBenchmarkRateIndex(this.investmentRatesService.getInvestmentInterestRateIndex(1));
			this.investmentManagerAccountService.saveInvestmentManagerAccount(manager);
		}, "This manager will apply a balance adjustment based on the selected security or index.  Proxy Last Updated Date is required in order to calculate this adjustment.");
	}


	@Test
	public void testManagerSearchFormProxyManagerFilters() {
		InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();

		InvestmentManagerAccount manager = new InvestmentManagerAccount();
		manager.setProxyManager(true);
		manager.setAccountNumber("PROXY1");
		manager.setProxyValue(BigDecimal.valueOf(100));
		this.investmentManagerAccountService.saveInvestmentManagerAccount(manager);

		InvestmentManagerAccount manager2 = new InvestmentManagerAccount();
		manager2.setProxyManager(true);
		manager2.setAccountNumber("PROXY2");
		manager2.setProxyValue(BigDecimal.valueOf(100));
		this.investmentManagerAccountService.saveInvestmentManagerAccount(manager2);

		searchForm.setProxyManager(false);
		List<InvestmentManagerAccount> list = this.investmentManagerAccountService.getInvestmentManagerAccountList(searchForm);
		Assertions.assertEquals(1, CollectionUtils.getSize(list));

		searchForm = new InvestmentManagerAccountSearchForm();
		searchForm.setProxyManager(true);
		list = this.investmentManagerAccountService.getInvestmentManagerAccountList(searchForm);

		Assertions.assertEquals(2, CollectionUtils.getSize(list));

		// Clean up the added managers
		this.investmentManagerAccountService.deleteInvestmentManagerAccount(manager.getId());
		this.investmentManagerAccountService.deleteInvestmentManagerAccount(manager2.getId());
	}


	@Test
	public void testAutoGenerateAccountNumberOnInsert() {
		InvestmentManagerAccount account = new InvestmentManagerAccount();
		account.setAccountName("Test Manager");
		this.investmentManagerAccountService.saveInvestmentManagerAccount(account);
		Assertions.assertTrue(account.getAccountNumber().length() >= 6, "System Generated Account numbers should be at least six digits long");
		Assertions.assertEquals(account.getAutoGeneratedAccountNumber(), account.getAccountNumber());
		Assertions.assertTrue(account.isAccountNumberAutoGenerated());
		// Delete to clean up test
		this.investmentManagerAccountService.deleteInvestmentManagerAccount(account.getId());
	}


	@Test
	public void testAutoGenerateAccountNumberOnUpdate() {
		InvestmentManagerAccount account = new InvestmentManagerAccount();
		account.setAccountName("Test Manager");
		account.setAccountNumber("012345");
		this.investmentManagerAccountService.saveInvestmentManagerAccount(account);
		Assertions.assertEquals("012345", account.getAccountNumber());
		Assertions.assertFalse(account.isAccountNumberAutoGenerated());
		account.setAccountNumber(null);
		this.investmentManagerAccountService.saveInvestmentManagerAccount(account);
		Assertions.assertEquals(account.getAutoGeneratedAccountNumber(), account.getAccountNumber());
		Assertions.assertTrue(account.isAccountNumberAutoGenerated());
		// Delete to clean up test
		this.investmentManagerAccountService.deleteInvestmentManagerAccount(account.getId());
	}


	@Test
	public void testValidateManagerAccountNumberOnInsert() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentManagerAccount account = new InvestmentManagerAccount();
			account.setAccountName("Test Manager");
			account.setAccountNumber("M00000001");
			this.investmentManagerAccountService.saveInvestmentManagerAccount(account);
		}, "Account number [M00000001] is invalid. User Defined Account numbers must be 6 characters long. To have the system generate a unique account number for you, please leave this field blank.");
	}


	@Test
	public void testValidateManagerAccountNumberOnUpdate() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentManagerAccount account = new InvestmentManagerAccount();
			account.setAccountName("Test Manager");
			account = this.investmentManagerAccountService.saveInvestmentManagerAccount(account);
			Assertions.assertEquals(account.getAutoGeneratedAccountNumber(), account.getAccountNumber());
			Assertions.assertTrue(account.isAccountNumberAutoGenerated());
			account.setAccountNumber("999888777");
			Assertions.assertFalse(account.isAccountNumberAutoGenerated());
			this.investmentManagerAccountService.saveInvestmentManagerAccount(account);
		}, "Account number [999888777] is invalid. User Defined Account numbers must be 6 characters long. To have the system generate a unique account number for you, please leave this field blank.");
	}


	@Test
	public void testInvestmentManagerAccountAutoGenerateNumberMethod() {
		InvestmentManagerAccount account = new InvestmentManagerAccount();
		Assertions.assertNull(account.getAutoGeneratedAccountNumber()); // Cannot generate for new beans
		Assertions.assertFalse(account.isAccountNumberAutoGenerated());
		account.setId(1);
		Assertions.assertEquals("M00001", account.getAutoGeneratedAccountNumber());
		account.setId(18);
		Assertions.assertEquals("M00018", account.getAutoGeneratedAccountNumber());
		account.setId(186);
		Assertions.assertEquals("M00186", account.getAutoGeneratedAccountNumber());
		account.setId(1867);
		Assertions.assertEquals("M01867", account.getAutoGeneratedAccountNumber());
		account.setId(18678);
		Assertions.assertEquals("M18678", account.getAutoGeneratedAccountNumber());
		account.setId(1867892);
		Assertions.assertEquals("M1867892", account.getAutoGeneratedAccountNumber());
		account.setId(18215484);
		Assertions.assertEquals("M18215484", account.getAutoGeneratedAccountNumber());
	}


	@Test
	public void testValidateInvestmentManagerAccountAllocationListBadPercent() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentManagerAccountAssignment assignment = setupTestOverlayAssignment(ManagerCustomCashOverlayTypes.CUSTOM_PERCENT, 8000000);

			this.investmentManagerAccountService.saveInvestmentManagerAccountAssignment(assignment);
		}, "Custom Cash Overlay Asset Class [AssetClass-123] percentage is not allowed to be over 9,999.99%");
	}


	@Test
	public void testValidateInvestmentManagerAccountAllocationListGoodPercent() {
		InvestmentManagerAccountAssignment assignment = setupTestOverlayAssignment(ManagerCustomCashOverlayTypes.CUSTOM_PERCENT, 8000);
		InvestmentManagerAccountAssignment result = this.investmentManagerAccountService.saveInvestmentManagerAccountAssignment(assignment);
		Assertions.assertTrue(result.getCustomCashOverlayAllocationList().size() == 1);
	}


	@Test
	public void testValidateInvestmentManagerAccountAllocationListHighAmount() {
		InvestmentManagerAccountAssignment assignment = setupTestOverlayAssignment(ManagerCustomCashOverlayTypes.CUSTOM_AMOUNT, 8000000);
		InvestmentManagerAccountAssignment result = this.investmentManagerAccountService.saveInvestmentManagerAccountAssignment(assignment);
		Assertions.assertTrue(result.getCustomCashOverlayAllocationList().size() == 1);
	}


	@Test
	public void testValidateInvestmentManagerAccountAllocationListLowAmount() {
		InvestmentManagerAccountAssignment assignment = setupTestOverlayAssignment(ManagerCustomCashOverlayTypes.CUSTOM_AMOUNT, 8000);
		InvestmentManagerAccountAssignment result = this.investmentManagerAccountService.saveInvestmentManagerAccountAssignment(assignment);
		Assertions.assertTrue(result.getCustomCashOverlayAllocationList().size() == 1);
	}


	private InvestmentManagerAccountAssignment setupTestOverlayAssignment(ManagerCustomCashOverlayTypes overlayType, int customValue) {
		InvestmentManagerAccount managerAccount = this.investmentManagerAccountService.getInvestmentManagerAccount(11);
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccount(1);
		InvestmentAccountAssetClass accountAssetClass = this.investmentAccountAssetClassService.getInvestmentAccountAssetClass(303);

		List<InvestmentManagerAccountAllocation> allocationList = new ArrayList<>();
		InvestmentManagerAccountAllocation investmentManagerAccountAllocation = new InvestmentManagerAccountAllocation();
		investmentManagerAccountAllocation.setAllocationPercent(new BigDecimal(100));
		investmentManagerAccountAllocation.setAssetClass(accountAssetClass.getAssetClass());
		allocationList.add(investmentManagerAccountAllocation);

		InvestmentManagerAccountAssignment assignment = this.investmentManagerAccountService.getInvestmentManagerAccountAssignmentByManagerAndAccount(managerAccount.getId(), account.getId());
		assignment.setCashOverlayType(ManagerCashOverlayTypes.FUND);
		assignment.setCashType(ManagerCashTypes.MANAGER);
		assignment.setCustomCashAllocationType(ManagerCustomCashAllocationTypes.MANAGER_ASSET_CLASSES);
		assignment.setCustomCashOverlayType(overlayType);
		assignment.setAllocationList(allocationList);

		InvestmentManagerAccountAllocation customCashAllocation = new InvestmentManagerAccountAllocation();
		customCashAllocation.setCustomAllocationValue(BigDecimal.valueOf(customValue));
		customCashAllocation.setAssetClass(assignment.getAllocationList().get(0).getAssetClass());
		customCashAllocation.setCashType(ManagerCashTypes.TRANSITION);
		customCashAllocation.setNote("TESTING");
		customCashAllocation.setCustomCashOverlay(true);
		assignment.setCustomCashOverlayAllocationList(CollectionUtils.createList(customCashAllocation));

		BusinessServiceProcessingType serviceProcessingType = BusinessServiceProcessingTypeBuilder.createOverlay().toBusinessServiceProcessingType();
		assignment.getReferenceTwo().setServiceProcessingType(serviceProcessingType);
		return assignment;
	}


	private InvestmentManagerAccount createManager(int number, ManagerTypes type, boolean save) {
		return createManager(number, type, save, "USD");
	}


	private InvestmentManagerAccount createManager(int number, ManagerTypes type, boolean save, String currencySymbol) {
		BusinessClient client = this.businessClientService.getBusinessClient(101);

		InvestmentManagerAccount managerAccount = new InvestmentManagerAccount();
		managerAccount.setClient(client);
		managerAccount.setAccountNumber("M0000" + number);
		managerAccount.setManagerType(type);
		managerAccount.setBaseCurrency(this.investmentInstrumentService.getInvestmentSecurityBySymbol(currencySymbol, true));

		if (type == ManagerTypes.ROLLUP) {
			managerAccount.setRollupAggregationType(AggregationOperations.SUM);
		}

		if (save) {
			this.investmentManagerAccountService.saveInvestmentManagerAccount(managerAccount);
		}
		return managerAccount;
	}


	private void addManagerRollup(InvestmentManagerAccount parent, InvestmentManagerAccount child, boolean save) {
		InvestmentManagerAccountRollup r = new InvestmentManagerAccountRollup();
		r.setReferenceOne(parent);
		r.setReferenceTwo(child);
		r.setAllocationPercent(BigDecimal.valueOf(100));

		List<InvestmentManagerAccountRollup> rList = parent.getRollupManagerList();
		if (rList == null) {
			rList = new ArrayList<>();
		}
		rList.add(r);
		parent.setRollupManagerList(rList);

		if (save) {
			this.investmentManagerAccountService.saveInvestmentManagerAccount(parent);
		}
	}


	/**
	 * Should be valid because 1 depends on 2 & 6, 6 depends on 2, but 2 only depends on 4 (no circular dependency)
	 * Mgr 1 -> Mgr 2 -> Mgr 4
	 * <p>
	 * -> Mgr 3 -> Mgr 5
	 * -> Mgr 6 -> Mgr 2
	 */
	@Test
	public void testValidRollupOfRollupManagers() {
		// Mgr 4 is a regular manager
		InvestmentManagerAccount m4 = createManager(4, ManagerTypes.STANDARD, true);

		// Mgr 2 is a rollup of Mgr 4
		InvestmentManagerAccount m2 = createManager(2, ManagerTypes.ROLLUP, false);
		addManagerRollup(m2, m4, true);

		// Mgr5 is a regular manager
		InvestmentManagerAccount m5 = createManager(5, ManagerTypes.STANDARD, true);

		// Mgr 6 is a rollup of mgr 2
		InvestmentManagerAccount m6 = createManager(6, ManagerTypes.ROLLUP, false);
		addManagerRollup(m6, m2, true);

		// Mgr 3 is a rollup of managers 5 & 6
		InvestmentManagerAccount m3 = createManager(3, ManagerTypes.ROLLUP, false);
		addManagerRollup(m3, m5, false);
		addManagerRollup(m3, m6, true);

		// Manager 1 is a rollup of 2 & 3
		InvestmentManagerAccount m1 = createManager(1, ManagerTypes.ROLLUP, false);
		addManagerRollup(m1, m2, false);
		addManagerRollup(m1, m3, true);
	}


	@Test
	public void testInValidRollupManagerDifferenceCurrency() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Mgr 4 is a regular manager
			InvestmentManagerAccount m4 = createManager(4, ManagerTypes.STANDARD, true);

			// Mgr5 is a regular manager
			InvestmentManagerAccount m5 = createManager(5, ManagerTypes.STANDARD, true);

			// Mgr 3 is a rollup of managers 4 and 5, but different currency
			InvestmentManagerAccount m3 = createManager(3, ManagerTypes.ROLLUP, false, "CAD");
			addManagerRollup(m3, m4, false);
			addManagerRollup(m3, m5, true);
		}, "Rollup managers can only contain child manager accounts for the same base currency.");
	}


	/**
	 * Should not be valid because 2 depends on 6, and 6 depends on 2
	 * Mgr 1 -> Mgr 2 -> Mgr 3
	 * -> Mgr 4 -> Mgr 5
	 * -> Mgr 6 -> Mgr 2
	 */
	@Test
	public void testCircularDependencyRollupOfRollupManagers() {
		TestUtils.expectException(ValidationException.class, () -> {
			// Start Mgr 6 as a regular manager, will change to rollup after 2 is created
			InvestmentManagerAccount m6 = createManager(6, ManagerTypes.STANDARD, true);

			// Mgr5 is a regular manager
			InvestmentManagerAccount m5 = createManager(5, ManagerTypes.STANDARD, true);

			// Mgr3 is a regular manager
			InvestmentManagerAccount m3 = createManager(3, ManagerTypes.STANDARD, true);

			// Mgr 4 is a rollup of Mgr 5 & 6
			InvestmentManagerAccount m4 = createManager(4, ManagerTypes.ROLLUP, false);
			addManagerRollup(m4, m5, false);
			addManagerRollup(m4, m6, true);

			// Mgr 2 is a rollup of Mgr 3 & 4
			InvestmentManagerAccount m2 = createManager(2, ManagerTypes.ROLLUP, false);
			addManagerRollup(m2, m3, false);
			addManagerRollup(m2, m4, true);

			// Mgr 1 is a rollup of mgr 2
			InvestmentManagerAccount m1 = createManager(1, ManagerTypes.ROLLUP, false);
			addManagerRollup(m1, m2, true);

			// Change Mgr 6 to be a rollup of 2 - should fail
			m6.setManagerType(ManagerTypes.ROLLUP);
			m6.setRollupAggregationType(AggregationOperations.SUM);
			addManagerRollup(m6, m2, true);
		}, "Rollup Manager selection will result in a circular dependency.  Manager [M00004] already depends on [M00006].");
	}


	/**
	 * Should be valid because 1 depends on 2 & 6, 6 depends on 2, but 2 only depends on 4 (no circular dependency)
	 */
	@Test
	public void testRollupManagersClearCache() {
		// Mgr 4 is a regular manager
		InvestmentManagerAccount m4 = createManager(4, ManagerTypes.STANDARD, true);
		// Mgr5 is a regular manager
		InvestmentManagerAccount m5 = createManager(5, ManagerTypes.STANDARD, true);

		// Mgr 2 is a rollup of Mgr 4 & Mgr 5
		InvestmentManagerAccount m2 = createManager(2, ManagerTypes.ROLLUP, false);
		addManagerRollup(m2, m4, true);

		// Pull the Rollup Manager
		m2 = this.investmentManagerAccountService.getInvestmentManagerAccount(m2.getId());
		// It should be in the cache now
		Integer[] rollupIds = this.investmentManagerAccountRollupCache.getInvestmentManagerAccountRollupList(m2.getId());
		Assertions.assertEquals(1, rollupIds.length);

		// Add another child
		addManagerRollup(m2, m5, true);

		// Cache should be clear now
		rollupIds = this.investmentManagerAccountRollupCache.getInvestmentManagerAccountRollupList(m2.getId());
		Assertions.assertNull(rollupIds);

		// Pull the Rollup Manager
		m2 = this.investmentManagerAccountService.getInvestmentManagerAccount(m2.getId());

		// It should be in the cache now
		rollupIds = this.investmentManagerAccountRollupCache.getInvestmentManagerAccountRollupList(m2.getId());
		Assertions.assertEquals(2, rollupIds.length);

		for (InvestmentManagerAccountRollup r : CollectionUtils.getIterable(m2.getRollupManagerList())) {
			Assertions.assertEquals(MathUtils.BIG_DECIMAL_ONE_HUNDRED, r.getAllocationPercent());
			// Change Percentage
			r.setAllocationPercent(BigDecimal.valueOf(75));
		}

		// Save Manager 2 with rollup list changes
		this.investmentManagerAccountService.saveInvestmentManagerAccount(m2);

		// Cache should be clear now
		rollupIds = this.investmentManagerAccountRollupCache.getInvestmentManagerAccountRollupList(m2.getId());
		Assertions.assertNull(rollupIds);

		// Pull the manager again
		m2 = this.investmentManagerAccountService.getInvestmentManagerAccount(m2.getId());

		// Cache should be set now
		rollupIds = this.investmentManagerAccountRollupCache.getInvestmentManagerAccountRollupList(m2.getId());
		Assertions.assertEquals(2, rollupIds.length);

		for (InvestmentManagerAccountRollup r : CollectionUtils.getIterable(m2.getRollupManagerList())) {
			Assertions.assertEquals(BigDecimal.valueOf(75), r.getAllocationPercent());
		}

		// Remove one of the children
		m2.setRollupManagerList(CollectionUtils.createList(m2.getRollupManagerList().get(0)));
		this.investmentManagerAccountService.saveInvestmentManagerAccount(m2);

		// Cache should be clear now
		rollupIds = this.investmentManagerAccountRollupCache.getInvestmentManagerAccountRollupList(m2.getId());
		Assertions.assertNull(rollupIds);

		// Pull the manager again
		m2 = this.investmentManagerAccountService.getInvestmentManagerAccount(m2.getId());
		rollupIds = this.investmentManagerAccountRollupCache.getInvestmentManagerAccountRollupList(m2.getId());

		// Cache should be set now
		Assertions.assertEquals(1, rollupIds.length);

		// Save it again with new rollup changes
		m2.setAccountName(m2.getAccountName() + "Test");
		this.investmentManagerAccountService.saveInvestmentManagerAccount(m2);

		rollupIds = this.investmentManagerAccountRollupCache.getInvestmentManagerAccountRollupList(m2.getId());

		// Cache should be set - should not have cleared
		Assertions.assertEquals(1, rollupIds.length);
	}


	//////////////////////////////////////////////////////
	// InvestmentManagerAccountAssignmentOverlayAction tests
	//////////////////////////////////////////////////////


	@Test
	public void testInvestmentManagerAccountAssignmentOverlayActionSaveAndDelete() {
		// Adjustments
		// Amount Adjustment
		InvestmentManagerAccountAssignmentOverlayAction amountAdjustment = createInvestmentManagerAccountAssignmentOverlayAction(ManagerCashOverlayActionTypes.ADJUSTMENT, 50000.00, null, null,
				DateUtils.toDate("05/01/2014"), null);
		// Percent Adjustment
		InvestmentManagerAccountAssignmentOverlayAction percentAdjustment = createInvestmentManagerAccountAssignmentOverlayAction(ManagerCashOverlayActionTypes.ADJUSTMENT, null, 50.0,
				DateUtils.toDate("05/02/2014"), null, null);

		// Attempt to create adjustment with percent and amount populated
		createInvestmentManagerAccountAssignmentOverlayAction(ManagerCashOverlayActionTypes.ADJUSTMENT, 7500000.0, 50.0, DateUtils.toDate("06/02/2014"), null,
				"Either an amount or percent value is allowed, but not both");

		// Attempt to change percent adjustment with overlapping date to amount Adjustment
		percentAdjustment.setStartDate(DateUtils.toDate("05/01/2014"));
		saveInvestmentManagerAccountAssignmentOverlayAction(percentAdjustment,
				"There already exists an overlay action of type [ADJUSTMENT] for this manager assignment and overlapping date range ( - 05/01/2014).");

		// Delete amount adjustment and re-save percent adjustment
		this.investmentManagerAccountService.deleteInvestmentManagerAccountAssignmentOverlayAction(amountAdjustment.getId());
		saveInvestmentManagerAccountAssignmentOverlayAction(percentAdjustment, null);

		// Limits
		// Amount Limit
		createInvestmentManagerAccountAssignmentOverlayAction(ManagerCashOverlayActionTypes.LIMIT, 500000.00, null, null, DateUtils.toDate("05/01/2014"), null);
		// Percent Limit
		createInvestmentManagerAccountAssignmentOverlayAction(ManagerCashOverlayActionTypes.LIMIT, null, 70.0, DateUtils.toDate("05/02/2014"), DateUtils.toDate("05/31/2014"), null);
		// Amount & Percent Limit
		createInvestmentManagerAccountAssignmentOverlayAction(ManagerCashOverlayActionTypes.LIMIT, 500000.00, 70.0, DateUtils.toDate("06/01/2014"), null, null);

		// Attempt to create New Amount Limit overlapping with Amount & Percent Limit
		createInvestmentManagerAccountAssignmentOverlayAction(ManagerCashOverlayActionTypes.LIMIT, 500000.00, null, DateUtils.toDate("05/21/2014"), DateUtils.toDate("06/15/2014"),
				"There already exists an overlay action of type [LIMIT] for this manager assignment and overlapping date range (05/02/2014 - 05/31/2014).");
	}


	private InvestmentManagerAccountAssignmentOverlayAction createInvestmentManagerAccountAssignmentOverlayAction(ManagerCashOverlayActionTypes actionType, Double amount, Double percent,
	                                                                                                              Date startDate, Date endDate, String expectedErrorMessage) {
		InvestmentManagerAccountAssignmentOverlayAction bean = new InvestmentManagerAccountAssignmentOverlayAction();
		bean.setManagerAccountAssignment(this.investmentManagerAccountService.getInvestmentManagerAccountAssignment(1));
		bean.setOverlayActionType(actionType);
		if (amount != null) {
			bean.setOverlayActionAmount(BigDecimal.valueOf(amount));
		}
		if (percent != null) {
			bean.setOverlayActionPercent(BigDecimal.valueOf(percent));
		}
		bean.setStartDate(startDate);
		bean.setEndDate(endDate);
		saveInvestmentManagerAccountAssignmentOverlayAction(bean, expectedErrorMessage);
		return bean;
	}


	private void saveInvestmentManagerAccountAssignmentOverlayAction(InvestmentManagerAccountAssignmentOverlayAction bean, String expectedErrorMessage) {
		try {
			this.investmentManagerAccountService.saveInvestmentManagerAccountAssignmentOverlayAction(bean);
		}
		catch (ValidationException e) {
			if (!StringUtils.isEmpty(expectedErrorMessage)) {
				Assertions.assertEquals(expectedErrorMessage, e.getMessage());
				return;
			}
			throw e;
		}
		if (!StringUtils.isEmpty(expectedErrorMessage)) {
			Assertions.fail("Did not get expected exception with message [" + expectedErrorMessage + "] on save.");
		}
	}
}
