package com.clifton.investment.setup;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>InvestmentSetupServiceImplTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentSetupServiceImplTests {

	@Resource
	private InvestmentSetupService investmentSetupService;


	////////////////////////////////////////////////////
	//  Instrument Hierarchy Tests
	////////////////////////////////////////////////////


	@Test
	public void testGetHierarchyListDeep() {
		// ALL BOND HIERARCHIES
		List<InvestmentInstrumentHierarchy> list = this.investmentSetupService.getInvestmentInstrumentHierarchyListDeep(MathUtils.SHORT_ONE);
		Assertions.assertEquals(3, list.size());

		InvestmentInstrumentHierarchy h = list.get(0);
		Assertions.assertEquals("Bond", h.getNameExpanded());
		h = list.get(1);
		Assertions.assertEquals("Bond / Asset Back Securities", h.getNameExpanded());
		h = list.get(2);
		Assertions.assertEquals("Bond / Taxable Bonds", h.getNameExpanded());

		// ALL FUTURES_FIXED_INCOME HIERARCHIES
		list = this.investmentSetupService.getInvestmentInstrumentHierarchyListDeep(Short.parseShort("9"));
		Assertions.assertEquals(1, list.size());
		// Has no children, so should have just returns Hierarchy with ID of 9
		Assertions.assertEquals(9, (int) list.get(0).getId());
	}


	////////////////////////////////////////////////////
	//  Asset Class Tests
	////////////////////////////////////////////////////


	@Test
	public void testSaveAssetClasses() {
		// Set Up Equity and Fixed Income as a Master Asset Class
		InvestmentAssetClass eq = setupAssetClass("Equity", true, null);
		InvestmentAssetClass fi = setupAssetClass("Fixed Income", true, null);

		// Set up DE as non master asset class - without master selected - should fail
		setupAssetClass("Domestic Equity", false, null, "Master Asset Class selection is required for non-master asset classes.");

		// Try again with master asset class selected
		InvestmentAssetClass de = setupAssetClass("Domestic Equity", false, eq);
		InvestmentAssetClass ie = setupAssetClass("International Equity", false, eq);

		// Try to change eq to non-master - should fail because it's used by DE
		eq.setMaster(false);
		saveAssetClass(eq,
				"Cannot change asset class [Equity] to a non-master asset class because it is already selected as the master asset class of [2] asset classes: Domestic Equity,International Equity");
		eq.setMaster(true);

		// Try to change fi to non-master - should fail only because it needs a master asset class selected
		fi.setMaster(false);
		saveAssetClass(fi, "Master Asset Class selection is required for non-master asset classes.");

		// Try to change fi again to non-master - this time set equity as it's master - should work OK
		fi.setMaster(false);
		fi.setMasterAssetClass(eq);
		saveAssetClass(fi, null);

		// Try to set DE as master of IE - should fail because DE is not a master asset class
		ie.setMasterAssetClass(de);
		saveAssetClass(ie, "Selected Master Asset Class [Domestic Equity] is not flagged as a master asset class.  Please select a valid master asset class.");
	}


	private InvestmentAssetClass setupAssetClass(String name, boolean master, InvestmentAssetClass masterAssetClass) {
		return setupAssetClass(name, master, masterAssetClass, null);
	}


	private InvestmentAssetClass setupAssetClass(String name, boolean master, InvestmentAssetClass masterAssetClass, String expectedException) {
		InvestmentAssetClass ac = new InvestmentAssetClass();
		ac.setName(name);
		ac.setMaster(master);
		ac.setMasterAssetClass(masterAssetClass);

		saveAssetClass(ac, expectedException);
		return ac;
	}


	private void saveAssetClass(InvestmentAssetClass ac, String expectedException) {
		try {
			this.investmentSetupService.saveInvestmentAssetClass(ac);
		}
		catch (ValidationException e) {
			if (StringUtils.isEmpty(expectedException)) {
				throw e;
			}
			Assertions.assertEquals(expectedException, e.getMessage());
			return;
		}
		if (!StringUtils.isEmpty(expectedException)) {
			Assertions.fail("Expected Exception with Message [" + expectedException + "].");
		}
	}
}
