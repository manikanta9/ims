package com.clifton.investment.setup.group;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>InvestmentGroupServiceImplTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class InvestmentGroupServiceImplTests<T extends InvestmentInstrument, S extends InvestmentSecurity> {

	@Resource
	private InvestmentGroupService investmentGroupService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private ReadOnlyDAO<InvestmentGroupItemInstrument> investmentGroupItemInstrumentDAO;

	@Resource
	private ReadOnlyDAO<InvestmentType> investmentTypeDAO;


	@Test
	public void testSaveInvestmentGroupAndItems() {
		InvestmentGroup group = new InvestmentGroup();
		group.setName("Test Group");
		group.setLevelsDeep(1);
		group.setInvestmentGroupType(this.investmentGroupService.getInvestmentGroupType((short) 2));
		// Should save new group without an exception
		saveGroup(group, false);

		InvestmentGroupItem groupItem1 = new InvestmentGroupItem();
		groupItem1.setGroup(group);
		groupItem1.setName("Test Item 1");
		// Should save first level Group Item without an exception
		saveGroupItem(groupItem1, false);

		InvestmentGroupItem groupItem2 = new InvestmentGroupItem();
		groupItem2.setGroup(group);
		groupItem2.setParent(groupItem1);
		groupItem2.setName("Test Item Level 2");
		// Try to save an item with a level of 2 (max supported is 1) so should fail
		saveGroupItem(groupItem2, true);
	}


	@Test
	public void testSaveInvestmentGroupItemInstrument() {
		// Verify the table is currently empty
		Assertions.assertTrue(CollectionUtils.isEmpty(this.investmentGroupItemInstrumentDAO.findAll()), "Group Item Instrument table should be empty.");

		// Create/rebuild the data in the InvestmentGroupItemInstrument table based upon Matrix definitions.
		this.investmentGroupService.rebuildInvestmentGroupItemInstrumentList();

		// After Rebuild, there should be 4 items in the list
		List<InvestmentGroupItemInstrument> list = this.investmentGroupItemInstrumentDAO.findAll();
		Assertions.assertEquals(6, CollectionUtils.getSize(list));
		for (InvestmentGroupItemInstrument item : list) {
			if ("Equity".equals(item.getReferenceOne().getName())) {
				Assertions.assertTrue("Regular Common Stock".equals(item.getReferenceTwo().getName()) || "Preferred Stock".equals(item.getReferenceTwo().getName()));
			}
			else if ("Other".equals(item.getReferenceOne().getName())) {
				Assertions.assertEquals("Agency Asset Back Bond", item.getReferenceTwo().getName());
			}
			else if ("Domestic".equals(item.getReferenceOne().getName())) {
				Assertions.assertEquals("Domestic Futures", item.getReferenceTwo().getName());
			}
			else if ("International".equals(item.getReferenceOne().getName())) {
				Assertions.assertTrue("Fed Funds Fixed Income Futures".equals(item.getReferenceTwo().getName()) || "International Futures".equals(item.getReferenceTwo().getName()));
			}
			else {
				Assertions.fail("Unknown Group Item in List " + item.getReferenceOne().getName());
			}
		}
	}


	@Test
	public void testSaveInvestmentGroupItemInstrumentWithDuplicates() {
		// investmentInstrument-RegularCommonStock
		InvestmentInstrument instrument = this.investmentInstrumentService.getInvestmentInstrument(2);

		// investmentGroupItem-DomesticEquity
		InvestmentGroupItem item = this.investmentGroupService.getInvestmentGroupItem(22);

		InvestmentGroupMatrix matrix = new InvestmentGroupMatrix();
		matrix.setGroupItem(item);
		matrix.setInstrument(instrument);
		this.investmentGroupService.saveInvestmentGroupMatrix(matrix);

		// Trying to tie RegularCommonStock to Domestic Equity, when Stocks
		// are already tied to Equity group item and duplicates are not allowed
		// which will fail during rebuilding.
		Status result = this.investmentGroupService.rebuildInvestmentGroupItemInstrumentList();

		if (result.getErrorCount() > 0) {
			// Should have failed.  Now change the group to allow duplications
			// and try to rebuild, should succeed.
			InvestmentGroup group = item.getGroup();
			group.setDuplicationAllowed(true);
			saveGroup(group, false);

			this.investmentGroupService.rebuildInvestmentGroupItemInstrumentList();
			Assertions.assertEquals(7, CollectionUtils.getSize(this.investmentGroupItemInstrumentDAO.findAll()));
			return;
		}

		throw new RuntimeException("Rebuilding the Investment Group Item Instrument List should have failed since duplicates aren't allowed");
	}


	@Test
	public void testInvestmentGroupItemLeafAssignmentSucceed() {
		// Group Items 31 - Not a Leaf, 32 & 33 - Leaves
		InvestmentGroupMatrix matrix = new InvestmentGroupMatrix();
		matrix.setInstrument(this.investmentInstrumentService.getInvestmentInstrument(2));
		matrix.setGroupItem(this.investmentGroupService.getInvestmentGroupItem(33));
		this.investmentGroupService.saveInvestmentGroupMatrix(matrix);
	}


	@Test
	public void testInvestmentGroupItemLeafAssignmentFail() {
		Assertions.assertThrows(Exception.class, () -> {
			// Group Items 31 - Not a Leaf, 32 & 33 - Leaves
			InvestmentGroupMatrix matrix = new InvestmentGroupMatrix();
			matrix.setInstrument(this.investmentInstrumentService.getInvestmentInstrument(2));
			matrix.setGroupItem(this.investmentGroupService.getInvestmentGroupItem(31));
			this.investmentGroupService.saveInvestmentGroupMatrix(matrix);
		});
	}


	@Test
	public void testChangingInvestmentGroupItemLeafAssignment() {
		InvestmentGroupMatrix matrix = new InvestmentGroupMatrix();
		matrix.setInstrument(this.investmentInstrumentService.getInvestmentInstrument(2));
		matrix.setGroupItem(this.investmentGroupService.getInvestmentGroupItem(33));
		this.investmentGroupService.saveInvestmentGroupMatrix(matrix);

		// Change Group to not leaf assignment only
		InvestmentGroup group = this.investmentGroupService.getInvestmentGroup(MathUtils.SHORT_TWO);
		group.setLeafAssignmentOnly(false);
		this.investmentGroupService.saveInvestmentGroup(group);

		// Assign a non leaf item
		matrix = new InvestmentGroupMatrix();
		matrix.setInstrument(this.investmentInstrumentService.getInvestmentInstrument(2));
		matrix.setGroupItem(this.investmentGroupService.getInvestmentGroupItem(31));
		this.investmentGroupService.saveInvestmentGroupMatrix(matrix);

		// Try to change leaf assignment back (Should fail, b/c an assignment exists)
		try {
			group.setLeafAssignmentOnly(true);
			this.investmentGroupService.saveInvestmentGroup(group);
		}
		catch (Exception e) {
			// delete non leaf assignment
			this.investmentGroupService.deleteInvestmentGroupMatrix(matrix.getId());

			// try again to save group with leaf assignment set
			this.investmentGroupService.saveInvestmentGroup(group);

			// Now try to add a new group item that is a child of an assigned existing leaf node
			InvestmentGroupItem groupItem = new InvestmentGroupItem();
			groupItem.setGroup(group);
			groupItem.setParent(this.investmentGroupService.getInvestmentGroupItem(33));
			try {
				this.investmentGroupService.saveInvestmentGroupItem(groupItem);
			}
			catch (Exception e2) {
				// Expected exception
				return;
			}
			throw new RuntimeException("Expected exception when trying to add a child group item to a group item that is currently assigned and the group is marked as leaf assignments only.");
		}
		throw new RuntimeException("Exception expected when setting leaf assignment to true for group with non leaf group items already assigned.");
	}


	@Test
	public void testInvestmentGroupItemInstrumentEquals() {
		// Verify equals method on InvestmentGroupItemInstrument class.
		// Two items should only be equal if BOTH of their references are equal
		InvestmentGroupItemInstrument o1 = new InvestmentGroupItemInstrument();
		Assertions.assertFalse(o1.equals(new Object()));

		InvestmentGroupItemInstrument o2 = null;
		Assertions.assertFalse(o1.equals(o2));

		o2 = new InvestmentGroupItemInstrument();

		// Both objects have null group items & null instruments
		Assertions.assertTrue(o1.equals(o2));

		o1.setReferenceOne(this.investmentGroupService.getInvestmentGroupItem(11));
		o2.setReferenceOne(this.investmentGroupService.getInvestmentGroupItem(11));

		// Both objects have same reference to group item and null instrument
		Assertions.assertTrue(o1.equals(o2));

		o1.setReferenceTwo(this.investmentInstrumentService.getInvestmentInstrument(1));
		Assertions.assertFalse(o1.equals(o2));

		o2.setReferenceTwo(this.investmentInstrumentService.getInvestmentInstrument(2));
		Assertions.assertFalse(o1.equals(o2));

		o2.setReferenceTwo(this.investmentInstrumentService.getInvestmentInstrument(1));
		Assertions.assertTrue(o1.equals(o2));
	}


	@Test
	public void testTradableSecurityGroup() {
		InvestmentGroup group = new InvestmentGroup();
		group.setLevelsDeep(1);
		group.setName("Test Tradable Only");
		group.setTradableSecurityOnly(true);
		group.setInvestmentGroupType(this.investmentGroupService.getInvestmentGroupType((short) 1));
		saveGroup(group, false);

		InvestmentGroupItem gii = new InvestmentGroupItem();
		gii.setGroup(group);
		gii.setName("Everything");
		saveGroupItem(gii, false);

		InvestmentGroupMatrix matrix = new InvestmentGroupMatrix();
		matrix.setGroupItem(gii);
		matrix.setType(this.investmentTypeDAO.findOneByField("name", "Benchmarks"));
		this.investmentGroupService.saveInvestmentGroupMatrix(matrix);

		this.investmentGroupService.rebuildInvestmentGroupItemInstrumentListByGroup(group.getId());
		// Should Have Nothing In It because Tradable Securities Only Allowed
		List<InvestmentGroupItemInstrument> list = this.investmentGroupService.getInvestmentGroupItemInstrumentListByGroup(group.getId());
		Assertions.assertTrue(CollectionUtils.isEmpty(list));

		// Take Tradable Flag Off
		group.setTradableSecurityOnly(false);
		saveGroup(group, false);

		// Rebuild
		this.investmentGroupService.rebuildInvestmentGroupItemInstrumentListByGroup(group.getId());
		// Should Have One Instrument In It
		list = this.investmentGroupService.getInvestmentGroupItemInstrumentListByGroup(group.getId());
		Assertions.assertEquals(1, CollectionUtils.getSize(list));

		// Add Another Instrument
		InvestmentGroupMatrix matrix2 = new InvestmentGroupMatrix();
		matrix2.setGroupItem(gii);
		matrix2.setInstrument(this.investmentInstrumentService.getInvestmentInstrument(1));
		this.investmentGroupService.saveInvestmentGroupMatrix(matrix2);

		// Rebuild - Should now have 2
		this.investmentGroupService.rebuildInvestmentGroupItemInstrumentListByGroup(group.getId());
		list = this.investmentGroupService.getInvestmentGroupItemInstrumentListByGroup(group.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(list));

		// Put Tradable Back On and Rebuild - Should now have one
		group.setTradableSecurityOnly(true);
		saveGroup(group, false);
		this.investmentGroupService.rebuildInvestmentGroupItemInstrumentListByGroup(group.getId());
		list = this.investmentGroupService.getInvestmentGroupItemInstrumentListByGroup(group.getId());
		Assertions.assertEquals(1, CollectionUtils.getSize(list));
	}


	@Test
	public void testDeleteInstrument() {
		// Group Matrix & Group Item Instruments should automatically be deleted as well
		// If not, then would fail
		this.investmentInstrumentService.deleteInvestmentInstrument(1);
	}


	@Test
	public void testSaveInvestmentGroupWithCCY() {
		setupUSDFuturesGroup();
	}


	@Test
	public void testRebuildInvestmentGroupWithCCY() {
		InvestmentGroup group = setupUSDFuturesGroup();
		this.investmentGroupService.rebuildInvestmentGroupItemInstrumentListByGroup(group.getId());
		List<InvestmentGroupItemInstrument> itemInstrumentList = this.investmentGroupService.getInvestmentGroupItemInstrumentListByGroup(group.getId());
		Assertions.assertEquals(3, CollectionUtils.getSize(itemInstrumentList));

		// One Domestic (USD) and Two Int'l (CAD)
		Assertions.assertEquals(1, CollectionUtils.getSize(BeanUtils.filter(itemInstrumentList, investmentGroupItemInstrument -> investmentGroupItemInstrument.getReferenceOne().getCurrencyType(), InvestmentCurrencyTypes.DOMESTIC)));
		Assertions.assertEquals(2, CollectionUtils.getSize(BeanUtils.filter(itemInstrumentList, investmentGroupItemInstrument -> investmentGroupItemInstrument.getReferenceOne().getCurrencyType(), InvestmentCurrencyTypes.INTERNATIONAL)));
	}


	@Test
	public void testInvestmentGroupUsingTemplate() {
		InvestmentGroup usdGroup = setupUSDFuturesGroup();
		InvestmentGroup cadGroup = setupGroup(this.investmentInstrumentService.getInvestmentSecurityBySymbol("CAD", true));
		cadGroup.setTemplateInvestmentGroup(usdGroup);
		cadGroup.setInvestmentGroupType(this.investmentGroupService.getInvestmentGroupType((short) 5));
		this.investmentGroupService.saveInvestmentGroup(cadGroup);

		// Ensure Items and Matrix were rebuilt
		Assertions.assertEquals(3, CollectionUtils.getSize(this.investmentGroupService.getInvestmentGroupItemListByGroup(cadGroup.getId())));
		Assertions.assertEquals(2, CollectionUtils.getSize(this.investmentGroupService.getInvestmentGroupMatrixListByGroup(cadGroup.getId())));

		this.investmentGroupService.rebuildInvestmentGroupItemInstrumentListByGroup(cadGroup.getId());
		List<InvestmentGroupItemInstrument> itemInstrumentList = this.investmentGroupService.getInvestmentGroupItemInstrumentListByGroup(cadGroup.getId());
		Assertions.assertEquals(3, CollectionUtils.getSize(itemInstrumentList));

		// Two Domestic (CAD) and One Int'l (USD)
		Assertions.assertEquals(2, CollectionUtils.getSize(BeanUtils.filter(itemInstrumentList, investmentGroupItemInstrument -> investmentGroupItemInstrument.getReferenceOne().getCurrencyType(), InvestmentCurrencyTypes.DOMESTIC)));
		Assertions.assertEquals(1, CollectionUtils.getSize(BeanUtils.filter(itemInstrumentList, investmentGroupItemInstrument -> investmentGroupItemInstrument.getReferenceOne().getCurrencyType(), InvestmentCurrencyTypes.INTERNATIONAL)));
	}


	@Test
	public void testInvestmentGroupUpdateUsingTemplate() {
		InvestmentGroup usdGroup = setupUSDFuturesGroup();
		InvestmentGroup cadGroup = setupGroup(this.investmentInstrumentService.getInvestmentSecurityBySymbol("CAD", true));
		cadGroup.setTemplateInvestmentGroup(usdGroup);
		cadGroup.setInvestmentGroupType(this.investmentGroupService.getInvestmentGroupType((short) 5));
		this.investmentGroupService.saveInvestmentGroup(cadGroup);

		// Ensure CAD was built
		Assertions.assertEquals(3, CollectionUtils.getSize(this.investmentGroupService.getInvestmentGroupItemListByGroup(cadGroup.getId())));
		Assertions.assertEquals(2, CollectionUtils.getSize(this.investmentGroupService.getInvestmentGroupMatrixListByGroup(cadGroup.getId())));

		InvestmentGroupItem newItem = setupGroupItem(usdGroup, null, "Other", null);
		this.investmentGroupService.saveInvestmentGroupItem(newItem);

		InvestmentGroupItem newItemLeaf = setupGroupItem(usdGroup, newItem, "Bonds", InvestmentCurrencyTypes.BOTH);
		this.investmentGroupService.saveInvestmentGroupItem(newItemLeaf);

		// Ensure Items and Matrix on CAD group were added
		Assertions.assertEquals(5, CollectionUtils.getSize(this.investmentGroupService.getInvestmentGroupItemListByGroup(cadGroup.getId())));
		Assertions.assertEquals(2, CollectionUtils.getSize(this.investmentGroupService.getInvestmentGroupMatrixListByGroup(cadGroup.getId())));

		newItem.setName("Other - Bonds");
		this.investmentGroupService.saveInvestmentGroupItem(newItem);

		// Ensure Items and Matrix on CAD group were updated
		Assertions.assertEquals(5, CollectionUtils.getSize(this.investmentGroupService.getInvestmentGroupItemListByGroup(cadGroup.getId())));
		Assertions.assertEquals(2, CollectionUtils.getSize(this.investmentGroupService.getInvestmentGroupMatrixListByGroup(cadGroup.getId())));

		// Delete Group Items
		this.investmentGroupService.deleteInvestmentGroupItem(newItemLeaf.getId());
		this.investmentGroupService.deleteInvestmentGroupItem(newItem.getId());

		// Ensure Items and Matrix on CAD group were updated
		Assertions.assertEquals(3, CollectionUtils.getSize(this.investmentGroupService.getInvestmentGroupItemListByGroup(cadGroup.getId())));
		Assertions.assertEquals(2, CollectionUtils.getSize(this.investmentGroupService.getInvestmentGroupMatrixListByGroup(cadGroup.getId())));
	}


	@Test
	public void testSaveInvestmentGroupTemplateNotAllowed() {
		InvestmentGroup template = setupGroup(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USD", true));
		template.setInvestmentGroupType(this.investmentGroupService.getInvestmentGroupType((short) 4));
		template.setLeafAssignmentOnly(false);
		this.investmentGroupService.saveInvestmentGroup(template);
		InvestmentGroup group = new InvestmentGroup();
		group.setName("Test Group 1");
		group.setInvestmentGroupType(this.investmentGroupService.getInvestmentGroupType((short) 1));
		group.setBaseCurrency(this.investmentInstrumentService.getInvestmentSecurityBySymbol("CAD", true));
		group.setTemplateInvestmentGroup(template);
		group.setLeafAssignmentOnly(false);
		TestUtils.expectException(ValidationException.class, () -> {
			this.investmentGroupService.saveInvestmentGroup(group);
		}, "Template is not allowed for groups of type");

		// Make sure it saves properly when it's a template based type
		group.setInvestmentGroupType(this.investmentGroupService.getInvestmentGroupType((short) 5));
		this.investmentGroupService.saveInvestmentGroup(group);
	}


	@Test
	public void testSaveInvestmentGroupTemplateRequired() {
		InvestmentGroup template = setupGroup(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USD", true));
		template.setInvestmentGroupType(this.investmentGroupService.getInvestmentGroupType((short) 4));
		this.investmentGroupService.saveInvestmentGroup(template);

		InvestmentGroup group = new InvestmentGroup();
		group.setName("Test Group 2");
		group.setInvestmentGroupType(this.investmentGroupService.getInvestmentGroupType((short) 5));
		group.setBaseCurrency(this.investmentInstrumentService.getInvestmentSecurityBySymbol("CAD", true));
		TestUtils.expectException(ValidationException.class, () -> {
			this.investmentGroupService.saveInvestmentGroup(group);
		}, "Template is required for groups of type");

		// Make sure saves with template
		group.setTemplateInvestmentGroup(template);
		this.investmentGroupService.saveInvestmentGroup(group);
	}


	@Test
	public void testChangeInvestmentGroupType() {
		InvestmentGroup group = new InvestmentGroup();
		group.setInvestmentGroupType(this.investmentGroupService.getInvestmentGroupType((short) 1));
		this.investmentGroupService.saveInvestmentGroup(group);
		group.setInvestmentGroupType(this.investmentGroupService.getInvestmentGroupType((short) 3));

		TestUtils.expectException(ValidationException.class, () -> {
			this.investmentGroupService.saveInvestmentGroup(group);
		}, "Group type cannot be changed once group is created");
	}


	@Test
	public void testSaveInvestmentGroupBaseCurrencyRequired() {
		InvestmentGroup group = new InvestmentGroup();
		group.setInvestmentGroupType(this.investmentGroupService.getInvestmentGroupType((short) 6));
		TestUtils.expectException(ValidationException.class, () -> {
			this.investmentGroupService.saveInvestmentGroup(group);
		}, "Base currency is required for groups of type");
		group.setBaseCurrency(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USD", true));
		this.investmentGroupService.saveInvestmentGroup(group);
	}


	private InvestmentGroup setupUSDFuturesGroup() {
		InvestmentGroup group = setupGroup(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USD", true));
		group.setInvestmentGroupType(this.investmentGroupService.getInvestmentGroupType((short) 4));
		this.investmentGroupService.saveInvestmentGroup(group);

		InvestmentGroupItem futures = setupGroupItem(group, null, "Futures", null);
		this.investmentGroupService.saveInvestmentGroupItem(futures);

		InvestmentGroupItem domesticFutures = setupGroupItem(group, futures, "Domestic Futures", InvestmentCurrencyTypes.DOMESTIC);
		this.investmentGroupService.saveInvestmentGroupItem(domesticFutures);

		InvestmentGroupItem internationalFutures = setupGroupItem(group, futures, "International Futures", InvestmentCurrencyTypes.INTERNATIONAL);
		this.investmentGroupService.saveInvestmentGroupItem(internationalFutures);

		InvestmentGroupMatrix domesticFuturesMatrix = new InvestmentGroupMatrix();
		domesticFuturesMatrix.setGroupItem(domesticFutures);
		domesticFuturesMatrix.setType(this.investmentTypeDAO.findOneByField("name", "Future"));
		this.investmentGroupService.saveInvestmentGroupMatrix(domesticFuturesMatrix);

		InvestmentGroupMatrix internationalFuturesMatrix = new InvestmentGroupMatrix();
		internationalFuturesMatrix.setGroupItem(internationalFutures);
		internationalFuturesMatrix.setType(this.investmentTypeDAO.findOneByField("name", "Future"));
		this.investmentGroupService.saveInvestmentGroupMatrix(internationalFuturesMatrix);
		return group;
	}


	private InvestmentGroup setupGroup(InvestmentSecurity baseCurrency) {
		InvestmentGroup group = new InvestmentGroup();
		group.setName("Test Group: " + baseCurrency.getSymbol());
		group.setLeafAssignmentOnly(true);
		group.setLevelsDeep(2);
		group.setBaseCurrency(baseCurrency);
		return group;
	}


	private InvestmentGroupItem setupGroupItem(InvestmentGroup group, InvestmentGroupItem parent, String name, InvestmentCurrencyTypes type) {
		InvestmentGroupItem item = new InvestmentGroupItem();
		item.setGroup(group);
		item.setParent(parent);
		item.setCurrencyType(type);
		item.setName(name);
		return item;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                      Helper Methods                 /////////////
	////////////////////////////////////////////////////////////////////////////


	private void saveGroupItem(InvestmentGroupItem groupItem, boolean expectException) {
		try {
			this.investmentGroupService.saveInvestmentGroupItem(groupItem);
		}
		catch (Exception e) {
			if (!expectException) {
				// If no exception expected, throw a real exception
				throw new RuntimeException("Exception NOT expected while saving Group Item, but occurred.", e);
			}
			return;
		}
		if (expectException) {
			// If exception expected, throw a real exception
			throw new RuntimeException("Exception expected while saving Group Item, but did not occur.");
		}
	}


	private void saveGroup(InvestmentGroup group, boolean expectException) {
		try {
			this.investmentGroupService.saveInvestmentGroup(group);
		}
		catch (Exception e) {
			if (!expectException) {
				// If no exception expected, throw a real exception
				throw new RuntimeException("Exception NOT expected while saving Group, but occurred.", e);
			}
			return;
		}
		if (expectException) {
			// If exception expected, throw a real exception
			throw new RuntimeException("Exception expected while saving Group, but did not occur.");
		}
	}


	private void copyProperties(InvestmentGroup group, InvestmentGroupType type) {
		group.setLeafAssignmentOnly(type.isLeafAssignmentsOnly());
		group.setMissingInstrumentNotAllowed(type.isMissingNotAllowed());
		group.setDuplicationAllowed(!type.isSingleGroupItem());
	}
}
