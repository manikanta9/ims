package com.clifton.investment.setup;


import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instruction.delivery.InvestmentDeliveryMonthCodes;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>InvestmentDeliveryMonthCodesTests</code> ...
 *
 * @author manderson
 */
public class InvestmentDeliveryMonthCodesTests {


	private static final Map<String, String> DELIVERY_CODE_MAP = new HashMap<>();


	static {
		DELIVERY_CODE_MAP.put("F", "January");
		DELIVERY_CODE_MAP.put("G", "February");
		DELIVERY_CODE_MAP.put("H", "March");
		DELIVERY_CODE_MAP.put("J", "April");
		DELIVERY_CODE_MAP.put("K", "May");
		DELIVERY_CODE_MAP.put("M", "June");
		DELIVERY_CODE_MAP.put("N", "July");
		DELIVERY_CODE_MAP.put("Q", "August");
		DELIVERY_CODE_MAP.put("U", "September");
		DELIVERY_CODE_MAP.put("V", "October");
		DELIVERY_CODE_MAP.put("X", "November");
		DELIVERY_CODE_MAP.put("Z", "December");
	}


	@Test
	public void testCodeToMonth() {
		for (Map.Entry<String, String> stringStringEntry : DELIVERY_CODE_MAP.entrySet()) {
			ValidationUtils.assertEquals(stringStringEntry.getValue(), InvestmentDeliveryMonthCodes.valueOf(stringStringEntry.getKey()).getMonthName(), "Expected Month: " + stringStringEntry.getValue() + " for code: "
					+ stringStringEntry.getKey());
			ValidationUtils.assertTrue(InvestmentDeliveryMonthCodes.isValidCode(stringStringEntry.getKey()), "Expected code: " + stringStringEntry.getKey() + " to return true for isValidCode.");
		}
	}


	@Test
	public void testMonthToCode() {
		for (Map.Entry<String, String> stringStringEntry : DELIVERY_CODE_MAP.entrySet()) {
			ValidationUtils.assertEquals(stringStringEntry.getKey(), InvestmentDeliveryMonthCodes.valueOfMonthName(stringStringEntry.getValue()).name(),
					"Expected Code: " + stringStringEntry.getKey() + " for month: " + stringStringEntry.getValue());
		}
	}


	@Test
	public void testInvalidCodes() {
		ValidationUtils.assertFalse(InvestmentDeliveryMonthCodes.isValidCode("A"), "Expected : A to return false for isValidCode.");
		ValidationUtils.assertFalse(InvestmentDeliveryMonthCodes.isValidCode(null), "Expected : null to return false for isValidCode.");
		ValidationUtils.assertFalse(InvestmentDeliveryMonthCodes.isValidCode(""), "Expected : empty string to return false for isValidCode.");
		ValidationUtils.assertFalse(InvestmentDeliveryMonthCodes.isValidCode(" "), "Expected : empty string to return false for isValidCode.");
	}


	@Test
	public void testDateToCode() {
		int count = 0;
		for (Map.Entry<String, String> stringStringEntry : DELIVERY_CODE_MAP.entrySet()) {
			Date date = DateUtils.toDate(stringStringEntry.getValue() + " " + (count + 5) + ", 20" + (count < 10 ? "0" + count : count), "MMMM d, yyyy");
			ValidationUtils.assertEquals(stringStringEntry.getKey(), InvestmentDeliveryMonthCodes.valueOfDate(date).name(), "Expected Code: " + stringStringEntry.getKey() + " for date: " + DateUtils.fromDateShort(date) + " but got code: "
					+ InvestmentDeliveryMonthCodes.valueOfDate(date));
			count++;
		}
	}


	@Test
	public void testInvalidMonth() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentDeliveryMonthCodes.valueOfMonthName("Junuary");
		}, "Missing Month Code for Month Name: Junuary. Please use valid month names only.");
	}


	@Test
	public void testNullMonth() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentDeliveryMonthCodes.valueOfMonthName(null);
		}, "Month Name is required to get delivery month code.");
	}


	@Test
	public void testEmptyMonth() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentDeliveryMonthCodes.valueOfMonthName("");
		}, "Month Name is required to get delivery month code.");
	}
}
