-- Using Terminated account 523500 so these shouldn't change
SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityId FROM InvestmentSecurity
	WHERE Symbol = 'AAPL' OR Cusip = '83165AMJ0' OR Sedol = 'BZCR8F4' OR Isin = 'US83164MBZ14'
UNION
SELECT 'SystemBeanPropertyType' AS entityTableName, SystemBeanPropertyTypeID AS entityID FROM SystemBeanPropertyType pt
	INNER JOIN SystemBeanType t ON t.SystemBeanTypeID = pt.SystemBeanTypeID
	WHERE t.SystemBeanTypeName = 'Security File Investment Security Group Rebuild Executor'
;
