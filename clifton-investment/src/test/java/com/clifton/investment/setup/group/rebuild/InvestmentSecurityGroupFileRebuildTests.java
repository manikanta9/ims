package com.clifton.investment.setup.group.rebuild;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;


/**
 * @author nickk
 */
@InvestmentInMemoryDatabaseContext
public class InvestmentSecurityGroupFileRebuildTests extends BaseInMemoryDatabaseTests {

	private static String FILE_DIRECTORY;

	private static final String FILE_PATH = "securityListSourceFilePath";
	private static final String FILE_NAME = "securityListSourceFileName";
	private static final String IDENTIFIERS = "securityIdentifierToColumnIndexList";
	private static final String IGNORE_NOT_FOUND = "ignoreNotFound";
	private static final String NO_HEADER = "noHeaderRow";

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private InvestmentSecurityGroupService investmentSecurityGroupService;


	@BeforeAll
	public static void setup() throws IOException {
		FILE_DIRECTORY = new ClassPathResource("com/clifton/investment/setup/group/rebuild/SecurityGroup.csv").getFile().getParent();
	}


	@Test
	public void testFileLoad_Csv() {
		createAndValidateSecurityGroup("CsvFileGroup",
				MapUtils.ofEntries(
						MapUtils.entry(FILE_PATH, FILE_DIRECTORY),
						MapUtils.entry(FILE_NAME, "SecurityGroup.csv"),
						MapUtils.entry(IDENTIFIERS, getSecurityIdentifierColumnIndexJson())
				));
	}


	@Test
	public void testFileLoad_CsvNoHeader() {
		createAndValidateSecurityGroup("CsvNoHeaderFileGroup",
				MapUtils.ofEntries(
						MapUtils.entry(FILE_PATH, FILE_DIRECTORY),
						MapUtils.entry(FILE_NAME, "SecurityGroup_NoHeader.csv"),
						MapUtils.entry(IDENTIFIERS, getSecurityIdentifierColumnIndexJson()),
						MapUtils.entry(NO_HEADER, "true")
				));
	}


	@Test
	public void testFileLoad_Excel() {
		createAndValidateSecurityGroup("XlsFileGroup",
				MapUtils.ofEntries(
						MapUtils.entry(FILE_PATH, FILE_DIRECTORY),
						MapUtils.entry(FILE_NAME, "SecurityGroup.xls"),
						MapUtils.entry(IDENTIFIERS, getSecurityIdentifierColumnIndexJson())
				));
	}


	@Test
	public void testFileLoad_ExcelNoHeader() {
		createAndValidateSecurityGroup("XlsNoHeaderFileGroup",
				MapUtils.ofEntries(
						MapUtils.entry(FILE_PATH, FILE_DIRECTORY),
						MapUtils.entry(FILE_NAME, "SecurityGroup_NoHeader.xls"),
						MapUtils.entry(IDENTIFIERS, getSecurityIdentifierColumnIndexJson()),
						MapUtils.entry(NO_HEADER, "true")
				));
	}


	@Test
	public void testFileLoad_MissingSecurity_Fails() {
		TestUtils.expectException(RuntimeException.class, () ->
						createAndValidateSecurityGroup("XlsNoHeaderFileGroup",
								MapUtils.ofEntries(
										MapUtils.entry(FILE_PATH, FILE_DIRECTORY),
										MapUtils.entry(FILE_NAME, "SecurityGroup_MissingSecurity.csv"),
										MapUtils.entry(IDENTIFIERS, getSecurityIdentifierColumnIndexJson())
								)),
				"Row 5 no match"
		);
	}


	@Test
	public void testFileLoad_MissingSecurity_IgnoreFailure() {
		createAndValidateSecurityGroup("XlsNoHeaderFileGroup",
				MapUtils.ofEntries(
						MapUtils.entry(FILE_PATH, FILE_DIRECTORY),
						MapUtils.entry(FILE_NAME, "SecurityGroup_MissingSecurity.csv"),
						MapUtils.entry(IDENTIFIERS, getSecurityIdentifierColumnIndexJson()),
						MapUtils.entry(IGNORE_NOT_FOUND, "true")
				));
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private String getSecurityIdentifierColumnIndexJson() {
		return "{\"entityList\":[{\"identifierType\":\"CUSIP\",\"column\":0,\"order\":3},{\"identifierType\":\"ISIN\",\"column\":3,\"order\":2},{\"identifierType\":\"SEDOL\",\"column\":1,\"order\":1},{\"identifierType\":\"SYMBOL\",\"column\":2,\"order\":4}]}";
	}


	private void createAndValidateSecurityGroup(String name, Map<String, String> rebuildProperties) {
		SystemBean rebuildBean = createGroupRebuildBean(name, rebuildProperties);

		InvestmentSecurityGroup group = new InvestmentSecurityGroup();
		group.setName(name);
		group.setRebuildSystemBean(rebuildBean);
		group = this.investmentSecurityGroupService.saveInvestmentSecurityGroup(group);

		this.investmentSecurityGroupService.rebuildInvestmentSecurityGroup(group.getId());

		List<InvestmentSecurity> securityList = this.investmentSecurityGroupService.getInvestmentSecurityListForSecurityGroup(group.getId(), null, null);
		Assertions.assertEquals(securityList.size(), 4, "Expected group security list to have 4 securities.");
		List<Predicate<InvestmentSecurity>> validationPredicateList = CollectionUtils.createList(
				security -> "APPL".equals(security.getSymbol()),
				security -> "83165AMJ0".equals(security.getCusip()),
				security -> "BZCR8F4".equals(security.getSedol()),
				security -> "US83164MBZ14".equals(security.getIsin())
		);
		securityList.stream().allMatch(security -> validationPredicateList.stream().anyMatch(p -> p.test(security)));
	}


	private SystemBean createGroupRebuildBean(String groupName, Map<String, String> propertyToSet) {
		SystemBean rebuildBean = new SystemBean();
		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName("Security File Investment Security Group Rebuild Executor");

		rebuildBean.setType(beanType);
		rebuildBean.setName(groupName + "Security Group Rebuild Bean From File");
		rebuildBean.setDescription("Test bean");

		List<SystemBeanPropertyType> propertyTypes = this.systemBeanService.getSystemBeanPropertyTypeListByBean(rebuildBean);
		List<SystemBeanProperty> properties = new ArrayList<>();
		propertyToSet.forEach((key, value) -> createSystemBeanProperty(properties, propertyTypes, key, value, rebuildBean));

		return this.systemBeanService.saveSystemBean(rebuildBean);
	}


	public List<SystemBeanProperty> createSystemBeanProperty(List<SystemBeanProperty> propertyList, List<SystemBeanPropertyType> propertyTypes, String propertyTypeName, String propertyValue, SystemBean parentBean) {
		SystemBeanPropertyType propertyType = propertyTypes.stream().filter(type -> propertyTypeName.equals(type.getSystemPropertyName())).findFirst().orElse(null);
		if (propertyType != null) {
			SystemBeanProperty property = new SystemBeanProperty();
			property.setType(propertyType);
			property.setValue(propertyValue);
			property.setBean(parentBean);
			propertyList.add(property);
			parentBean.setPropertyList(propertyList);
		}
		return propertyList;
	}
}
