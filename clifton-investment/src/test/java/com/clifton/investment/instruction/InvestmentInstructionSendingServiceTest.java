package com.clifton.investment.instruction;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.RunnerHandlerStatusCommand;
import com.clifton.core.util.runner.RunnerWrapper;
import com.clifton.core.util.runner.ThreadPoolRunnerHandler;
import com.clifton.core.util.timer.ThreadLocalTimerHandler;
import com.clifton.export.messaging.message.ExportMessagingMessage;
import com.clifton.instruction.InstructionService;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.investment.instruction.report.InvestmentInstructionReportService;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.run.InvestmentInstructionRunService;
import com.clifton.investment.instruction.run.messaging.InvestmentInstructionRunMessagingConverterService;
import com.clifton.investment.instruction.run.runner.InvestmentInstructionRunner;
import com.clifton.investment.instruction.run.runner.InvestmentInstructionRunnerFactoryImpl;
import com.clifton.investment.instruction.run.runner.InvestmentInstructionRunnerServiceImpl;
import com.clifton.investment.instruction.run.runner.InvestmentInstructionSwiftRunner;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelector;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategoryBuilder;
import com.clifton.investment.instruction.setup.InvestmentInstructionContact;
import com.clifton.investment.instruction.setup.InvestmentInstructionContactGroup;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinitionBuilder;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusBuilder;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.schema.SystemTableBuilder;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author TerryS
 */
@ExtendWith(MockitoExtension.class)
class InvestmentInstructionSendingServiceTest<M extends InstructionMessage, E extends IdentityObject> {

	private static final int instructionCount = 5;
	private static final int instructionItemCount = 10;

	private final AtomicInteger instructionSequence = new AtomicInteger(1);
	private final AtomicInteger instructionItemSequence = new AtomicInteger(1);

	@Mock(lenient = true)
	private InvestmentInstructionSetupService investmentInstructionSetupService;

	@Mock(lenient = true)
	private InvestmentInstructionStatusService investmentInstructionStatusService;

	@Mock(lenient = true)
	private InvestmentInstructionRunService investmentInstructionRunService;

	@Mock(lenient = true)
	private InvestmentInstructionReportService investmentInstructionReportService;

	@Mock(lenient = true)
	private InstructionService<M> instructionService;

	@Mock(lenient = true)
	private ApplicationContextService applicationContextService;

	@Mock(lenient = true)
	private ContextHandler contextHandler;

	@Mock(lenient = true)
	private InvestmentInstructionRunMessagingConverterService investmentInstructionRunMessagingConverterService;

	@Mock(lenient = true)
	private AsynchronousMessageHandler exportClientAsynchronousMessageHandler;

	@Spy
	@InjectMocks
	private InvestmentInstructionRunnerFactoryImpl investmentInstructionRunnerFactory;

	@Spy
	@InjectMocks
	private InvestmentInstructionRunnerServiceImpl investmentInstructionRunnerService;

	@Spy
	@InjectMocks
	private InvestmentInstructionSendingServiceImpl investmentInstructionSendingService;

	@Mock
	private AdvancedUpdatableDAO<InvestmentInstruction, Criteria> investmentInstructionDAO;

	@Mock
	private AdvancedUpdatableDAO<InvestmentInstructionItem, Criteria> investmentInstructionItemDAO;

	@Mock
	private DaoLocator daoLocator;

	@Mock
	private SystemBeanService systemBeanService;

	@Mock
	private InvestmentInstructionSelector<BaseEntity<Integer>> investmentInstructionSelector;

	@Spy
	@InjectMocks
	public InvestmentInstructionServiceImpl<M, E> investmentInstructionService;

	public Collection<InvestmentInstruction> instructionList = Stream.generate(this::buildInstruction)
			.limit(instructionCount)
			.collect(Collectors.toList());

	private final ThreadPoolRunnerHandler handler = new ThreadPoolRunnerHandler();

	private SystemBean selectorBean;


	public InvestmentInstructionContactGroup getInvestmentInstructionContactGroup() {
		InvestmentInstructionContactGroup investmentInstructionContactGroup = new InvestmentInstructionContactGroup();
		investmentInstructionContactGroup.setName("Forward Trades");
		return investmentInstructionContactGroup;
	}


	public InvestmentInstruction buildInstruction() {
		SystemBeanType selectorType = new SystemBeanType();
		selectorType.setClassName("com.clifton.trade.instruction.TradeCounterpartyInstructionSelector");

		this.selectorBean = new SystemBean();
		this.selectorBean.setName("Trade Selector");
		this.selectorBean.setType(selectorType);

		InvestmentInstruction investmentInstruction = InvestmentInstructionBuilder.createEmpty()
				.withId(this.instructionSequence.getAndIncrement())
				.withDefinition(InvestmentInstructionDefinitionBuilder.createEmpty()
						.withCategory(
								InvestmentInstructionCategoryBuilder.createEmpty()
										.withTable(SystemTableBuilder.createTrade()
												.toSystemTable()
										)
										.toInvestmentInstructionCategory()
						)
						.withContactGroup(getInvestmentInstructionContactGroup())
						.withSelectorBean(this.selectorBean)
						.toInvestmentInstructionDefinition())
				.withStatus(InvestmentInstructionStatusBuilder.createOpen().toInvestmentInstructionStatus())
				.withRecipientCompany(BusinessCompanyBuilder.createTestingCompany1().toBusinessCompany())
				.toInvestmentInstruction();
		investmentInstruction.setItemList(Stream.generate(() -> buildInstructionItem(investmentInstruction))
				.limit(instructionItemCount)
				.collect(Collectors.toList())
		);
		return investmentInstruction;
	}


	public InvestmentInstructionItem buildInstructionItem(InvestmentInstruction investmentInstruction) {
		InvestmentInstructionItem item = InvestmentInstructionItemBuilder.createEmpty()
				.withStatus(InvestmentInstructionStatusBuilder.createOpen().toInvestmentInstructionStatus())
				.withId(this.instructionItemSequence.getAndIncrement())
				.withInstruction(investmentInstruction)
				.withFkFieldId(123456)
				.toInvestmentInstructionItem();
		item.setRv("0".getBytes());
		return item;
	}


	public List<InvestmentInstructionContact> buildInvestmentInstructionContactList() {
		List<InvestmentInstructionContact> results = new ArrayList<>();

		Date yesterday = DateUtils.addDays(new Date(), -1);
		BusinessCompany testCompany = BusinessCompanyBuilder.createTestingCompany1().toBusinessCompany();

		SystemBeanType emailType = new SystemBeanType();
		emailType.setName("Instruction Email");
		emailType.setClassName("com.clifton.investment.instruction.run.destination.InvestmentInstructionDestinationMessageGenerator");
		SystemBean emailBean = new SystemBean();
		emailBean.setName("Email Bean");
		emailBean.setType(emailType);
		results.add(buildInvestmentInstructionContact(testCompany, yesterday, emailBean));

		SystemBeanType ftpType = new SystemBeanType();
		ftpType.setName("Instruction Ftp");
		ftpType.setClassName("com.clifton.investment.instruction.run.destination.InvestmentInstructionDestinationMessageGenerator");
		SystemBean ftpBean = new SystemBean();
		ftpBean.setName("Ftp Bean");
		ftpBean.setType(ftpType);
		results.add(buildInvestmentInstructionContact(testCompany, yesterday, ftpBean));

		SystemBeanType swiftType = new SystemBeanType();
		swiftType.setName("Instruction Swift");
		swiftType.setClassName("com.clifton.investment.instruction.run.destination.InvestmentInstructionDestinationMessageGenerator");
		SystemBean swiftBean = new SystemBean();
		swiftBean.setName("Swift Bean");
		swiftBean.setType(swiftType);
		results.add(buildInvestmentInstructionContact(testCompany, yesterday, swiftBean));
		return results;
	}


	public InvestmentInstructionContact buildInvestmentInstructionContact(BusinessCompany testCompany, Date date, SystemBean systemBean) {
		InvestmentInstructionContact contact = new InvestmentInstructionContact();
		contact.setCompany(testCompany);
		contact.setGroup(getInvestmentInstructionContactGroup());
		contact.setStartDate(date);
		contact.setDestinationSystemBean(systemBean);
		return contact;
	}


	@BeforeEach
	public void setup() {
		this.investmentInstructionSendingService.setInvestmentInstructionRunnerService(this.investmentInstructionRunnerService);
		this.investmentInstructionSendingService.setInvestmentInstructionService(this.investmentInstructionService);
		this.investmentInstructionRunnerService.setInvestmentInstructionRunnerFactory(this.investmentInstructionRunnerFactory);
		this.handler.setContextHandler(this.contextHandler);
		this.handler.setApplicationContextService(this.applicationContextService);
		this.investmentInstructionRunnerService.setRunnerHandler(this.handler);

		Mockito.when(this.investmentInstructionSetupService.getInvestmentInstructionContactList(ArgumentMatchers.any())).thenReturn(buildInvestmentInstructionContactList());

		Mockito.when(this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.OPEN)).thenReturn(InvestmentInstructionStatusBuilder.createOpen().toInvestmentInstructionStatus());
		Mockito.when(this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.SEND)).thenReturn(InvestmentInstructionStatusBuilder.createSend().toInvestmentInstructionStatus());
		Mockito.when(this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.SENT)).thenReturn(InvestmentInstructionStatusBuilder.createSent().toInvestmentInstructionStatus());
		Mockito.when(this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.SENDING)).thenReturn(InvestmentInstructionStatusBuilder.createSending().toInvestmentInstructionStatus());
		Mockito.when(this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.COMPLETED)).thenReturn(InvestmentInstructionStatusBuilder.createCompleted().toInvestmentInstructionStatus());

		Mockito.when(this.investmentInstructionDAO.findByPrimaryKey(ArgumentMatchers.any())).then(invocation -> {
			Integer id = invocation.getArgument(0, Integer.class);
			if (Objects.equals(id, 123456)) {
				return new IdentityObject() {
					@Override
					public Serializable getIdentity() {
						return 123456;
					}


					@Override
					public boolean isNewBean() {
						return false;
					}
				};
			}
			return this.instructionList.stream().filter(i -> Objects.equals(id, i.getId())).findFirst().orElseThrow(RuntimeException::new);
		});
		Mockito.when(this.investmentInstructionItemDAO.findByField(ArgumentMatchers.anyString(), ArgumentMatchers.anyInt())).then(invocation -> {
			String beanFieldName = invocation.getArgument(0, String.class);
			Integer id = invocation.getArgument(1, Integer.class);
			Assertions.assertEquals("instruction.id", beanFieldName);
			return this.instructionList.stream().filter(i -> Objects.equals(id, i.getId())).flatMap(i -> i.getItemList().stream()).collect(Collectors.toList());
		});
		Mockito.when(this.investmentInstructionItemDAO.save(ArgumentMatchers.any())).then(invocation -> {
			InvestmentInstructionItem item = invocation.getArgument(0);
			int rv = Integer.parseInt(new String(item.getRv()));
			rv = rv + 1;
			item.setRv(Integer.toString(rv).getBytes());
			return item;
		});

		Mockito.when(this.daoLocator.locate(ArgumentMatchers.anyString())).then(invocation -> {
			String tableName = invocation.getArgument(0, String.class);
			if ("Trade".equals(tableName)) {
				return this.investmentInstructionDAO;
			}
			return null;
		});

		Mockito.when(this.investmentInstructionRunMessagingConverterService.toExportMessagingMessage(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(new ExportMessagingMessage());

		Mockito.when(this.investmentInstructionRunService.saveInvestmentInstructionRun(ArgumentMatchers.any())).then(invocation -> {
			InvestmentInstructionRun run = invocation.getArgument(0, InvestmentInstructionRun.class);
			run.setId(this.instructionSequence.getAndIncrement());
			return run;
		});
		Mockito.doAnswer(invocation -> {
			// inject mocks where necessary.
			if (invocation.getArgument(0) instanceof RunnerWrapper) {
				RunnerWrapper wrapper = invocation.getArgument(0, RunnerWrapper.class);
				wrapper.setTimerHandler(new ThreadLocalTimerHandler());
				wrapper.setContextHandler(this.contextHandler);
			}
			else if (invocation.getArgument(0) instanceof InvestmentInstructionRunner) {
				InvestmentInstructionRunner runner = invocation.getArgument(0, InvestmentInstructionRunner.class);
				runner.setInvestmentInstructionStatusService(this.investmentInstructionStatusService);
				runner.setInvestmentInstructionService(this.investmentInstructionService);
				runner.setInvestmentInstructionReportService(this.investmentInstructionReportService);
				runner.setInvestmentInstructionRunService(this.investmentInstructionRunService);
				runner.setInvestmentInstructionRunMessagingConverterService(this.investmentInstructionRunMessagingConverterService);
				runner.setExportClientAsynchronousMessageHandler(this.exportClientAsynchronousMessageHandler);
			}
			if (invocation.getArgument(0) instanceof InvestmentInstructionSwiftRunner) {
				@SuppressWarnings("unchecked")
				InvestmentInstructionSwiftRunner<M> swiftRunner = invocation.getArgument(0, InvestmentInstructionSwiftRunner.class);
				swiftRunner.setInstructionService(this.instructionService);
			}
			return null;
		}).when(this.applicationContextService).autowireBean(ArgumentMatchers.any());
		Mockito.when(this.investmentInstructionReportService.downloadInvestmentInstructionItemReportList(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(new FileWrapper(FilePath.forPath(""), "", true));
		Mockito.when(this.systemBeanService.getBeanInstance(this.selectorBean)).thenReturn(this.investmentInstructionSelector);
		Mockito.when(this.investmentInstructionSelector.isInstructionItemReady(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(true);
	}


	private void await(Callable<Boolean> repeatedTask, int iterations, int waitSeconds) throws InterruptedException {
		ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
		RepeatedRunnable repeatedRunnable = new RepeatedRunnable(iterations, repeatedTask);
		ScheduledFuture<?> futureTradeOrder = executorService.scheduleAtFixedRate(repeatedRunnable, 0, waitSeconds, TimeUnit.SECONDS);
		repeatedRunnable.await(futureTradeOrder);
	}


	@Test
	public void sendInvestmentInstructionTest() throws InterruptedException {
		ArgumentCaptor<AsynchronousMessage> asynchronousMessageCaptor = ArgumentCaptor.forClass(AsynchronousMessage.class);
		ArgumentCaptor<InvestmentInstructionItem> investmentInstructionItemArgumentCaptor = ArgumentCaptor.forClass(InvestmentInstructionItem.class);

		int[] instructions = this.instructionList.stream().map(InvestmentInstruction::getId).mapToInt(Integer::intValue).toArray();
		this.investmentInstructionSendingService.sendInvestmentInstructionList(instructions, null);

		// wait up to 120 seconds for all executors to complete.
		RunnerHandlerStatusCommand command = new RunnerHandlerStatusCommand();
		command.setIncludeStatusOfAllThreads(true);
		await(() -> this.handler.getStatus(command).getScheduledRunnerCount() == 0, 120, 1);

		Mockito.verify(this.exportClientAsynchronousMessageHandler, Mockito.times(instructionCount)).send(asynchronousMessageCaptor.capture());
		Assertions.assertEquals(instructionCount, asynchronousMessageCaptor.getAllValues().size());

		Mockito.verify(this.investmentInstructionItemDAO, Mockito.atLeast(instructionCount * instructionItemCount)).save(investmentInstructionItemArgumentCaptor.capture());
		final Map<Integer, List<InvestmentInstructionItem>> listMap = investmentInstructionItemArgumentCaptor.getAllValues().stream()
				.collect(Collectors.groupingBy(InvestmentInstructionItem::getId));
		final List<InvestmentInstructionItem> investmentInstructionItems = listMap.values().stream()
				.map(l -> l.stream().max(Comparator.comparing(i -> Integer.parseInt(new String(i.getRv())))))
				.map(o -> o.orElse(null))
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
		Assertions.assertEquals(instructionCount * instructionItemCount, investmentInstructionItems.size());
		Assertions.assertEquals(0, investmentInstructionItems.stream().filter(i -> InvestmentInstructionStatusNames.COMPLETED != i.getStatus().getInvestmentInstructionStatusName()).count());
	}


	/**
	 * INNER CLASS to wait until all threads finish.
	 */
	private static class RepeatedRunnable implements Runnable {

		private final Callable<Boolean> repeatedTask;

		private final AtomicInteger iterationCount;
		private final Semaphore semaphore;


		RepeatedRunnable(int iterations, Callable<Boolean> repeatedTask) throws InterruptedException {
			this.repeatedTask = repeatedTask;
			this.iterationCount = new AtomicInteger(iterations);
			this.semaphore = new Semaphore(1);
			this.semaphore.acquire();
		}


		public void await(ScheduledFuture<?> future) throws InterruptedException {
			this.semaphore.acquire();
			future.cancel(false);
		}


		@Override
		public void run() {
			try {
				boolean status = this.repeatedTask.call();
				if (status) {
					this.iterationCount.set(0);
				}
				int count = this.iterationCount.decrementAndGet();
				if (count <= 0) {
					this.semaphore.release();
				}
			}
			catch (Exception e) {
				this.semaphore.release();
			}
		}
	}
}
