package com.clifton.investment.instruction.setup;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>InvestmentInstructionDefinitionServiceImplTests</code> ...
 *
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class InvestmentInstructionSetupServiceImplTests {

	private static final Short CATEGORY_M2M = 1;
	private static final Short CATEGORY_TRADE = 2;

	private static final Integer SELECTOR_BEAN_TYPE = 1;

	private static final Integer RECIPIENT_CONTACT = 101;
	private static final Integer BUSINESS_COMPANY = 202;
	private static final Integer INSTRUCTION_CONTACT_GROUP = 0;
	private static final Integer INSTRUCTION_EMAIL_RECIPIENT_BEAN_TYPE = 2;
	private static final Integer INSTRUCTION_EMAIL_RECIPIENT_BEAN_PROPERTY_TYPE = 3;

	@Resource
	private InvestmentInstructionSetupService investmentInstructionSetupService;

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private BusinessContactService businessContactService;


	@Test
	public void testCreateDuplicateContact() {
		Assertions.assertThrows(ValidationException.class, () -> {
			InvestmentInstructionContactGroup group = this.investmentInstructionSetupService.getInvestmentInstructionContactGroup(INSTRUCTION_CONTACT_GROUP);
			BusinessCompany company = this.businessCompanyService.getBusinessCompany(BUSINESS_COMPANY);
			BusinessContact businessContact = this.businessContactService.getBusinessContact(RECIPIENT_CONTACT);

			InvestmentInstructionContact instructionContact = new InvestmentInstructionContact();
			instructionContact.setGroup(group);
			instructionContact.setCompany(company);
			instructionContact.setDestinationSystemBean(setupContactSystemBean(businessContact));
			this.investmentInstructionSetupService.saveInvestmentInstructionContact(instructionContact);

			InvestmentInstructionContact duplicateContact = new InvestmentInstructionContact();
			duplicateContact.setGroup(group);
			duplicateContact.setCompany(company);
			duplicateContact.setDestinationSystemBean(setupContactSystemBean(businessContact));
			this.investmentInstructionSetupService.saveInvestmentInstructionContact(duplicateContact);
		});
	}


	private SystemBean setupContactSystemBean(BusinessContact businessContact) {
		SystemBeanProperty property = new SystemBeanProperty();
		property.setType(this.systemBeanService.getSystemBeanPropertyType(INSTRUCTION_EMAIL_RECIPIENT_BEAN_PROPERTY_TYPE));
		property.setValue(businessContact.getId().toString());
		property.setText(businessContact.getId().toString());

		List<SystemBeanProperty> propertyList = new ArrayList<>();
		propertyList.add(property);

		SystemBean bean = new SystemBean();
		bean.setType(this.systemBeanService.getSystemBeanType(INSTRUCTION_EMAIL_RECIPIENT_BEAN_TYPE));
		bean.setPropertyList(propertyList);

		this.systemBeanService.saveSystemBean(bean);
		return bean;
	}


	@Test
	public void testCreateDefinition() {
		InvestmentInstructionDefinition bean = setupNewDefinition("Test M2M Definition", CATEGORY_M2M, 1);
		this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(bean);

		ValidationUtils.assertNotNull(bean.getId(), "Bean should have been saved and id updated.");

		// Pull from DB and make sure everything is populated
		bean = this.investmentInstructionSetupService.getInvestmentInstructionDefinition(bean.getId());
		AssertUtils.assertNotNull(bean.getSelectorBean(), "Selector Bean should have been created.");
		Assertions.assertEquals(2, CollectionUtils.getSize(bean.getSelectorBean().getPropertyList()));
		Assertions.assertEquals(bean.getCalculatedSelectorBeanName(), bean.getSelectorBean().getName());
		Assertions.assertEquals(2, CollectionUtils.getSize(bean.getFieldList()));
	}


	@Test
	public void testCopyDefinition() {
		InvestmentInstructionDefinition bean = setupNewDefinition("Test Trade Definition", CATEGORY_TRADE, 2);
		this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(bean);

		ValidationUtils.assertNotNull(bean.getId(), "Bean should have been saved and id updated.");

		InvestmentInstructionDefinition copyBean = this.investmentInstructionSetupService.copyInvestmentInstructionDefinition(bean.getId(), "Copy of " + bean.getName());
		// Pull from DB again to get fully populated instruction
		copyBean = this.investmentInstructionSetupService.getInvestmentInstructionDefinition(copyBean.getId());
		Assertions.assertNotSame(bean.getId(), copyBean.getId());
		Assertions.assertEquals("Copy of " + bean.getName(), copyBean.getName());
		AssertUtils.assertNotNull(copyBean.getSelectorBean(), "Selector Bean should have been created.");
		Assertions.assertNotSame(bean.getSelectorBean().getId(), copyBean.getSelectorBean().getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(copyBean.getSelectorBean().getPropertyList()));
		Assertions.assertEquals(copyBean.getCalculatedSelectorBeanName(), copyBean.getSelectorBean().getName());
		Assertions.assertEquals(2, CollectionUtils.getSize(copyBean.getFieldList()));
	}


	private InvestmentInstructionDefinition setupNewDefinition(String name, short category, int definitionNumber) {
		InvestmentInstructionDefinition bean = new InvestmentInstructionDefinition();
		bean.setCategory(this.investmentInstructionSetupService.getInvestmentInstructionCategory(category));
		bean.setName(name);

		SystemBean selectorBean = new SystemBean();
		selectorBean.setType(this.systemBeanService.getSystemBeanType(SELECTOR_BEAN_TYPE));

		List<SystemBeanProperty> propertyList = new ArrayList<>();
		SystemBeanProperty stringProp = new SystemBeanProperty();
		stringProp.setType(this.systemBeanService.getSystemBeanPropertyType(1));
		stringProp.setValue("Test Value " + definitionNumber);
		stringProp.setText("Test Value " + definitionNumber);

		SystemBeanProperty integerProp = new SystemBeanProperty();
		integerProp.setType(this.systemBeanService.getSystemBeanPropertyType(2));
		integerProp.setValue("" + definitionNumber);
		integerProp.setText("" + definitionNumber);

		propertyList.add(stringProp);
		propertyList.add(integerProp);
		selectorBean.setPropertyList(propertyList);

		bean.setSelectorBean(selectorBean);

		List<InvestmentInstructionDefinitionField> fieldList = new ArrayList<>();
		InvestmentInstructionDefinitionField fieldOne = new InvestmentInstructionDefinitionField();
		fieldOne.setName("Bank");
		fieldOne.setValue("HSBC Bank USA");

		InvestmentInstructionDefinitionField fieldTwo = new InvestmentInstructionDefinitionField();
		fieldTwo.setName("ABA");
		fieldTwo.setValue("1234560-789");

		fieldList.add(fieldOne);
		fieldList.add(fieldTwo);
		bean.setFieldList(fieldList);
		return bean;
	}
}
