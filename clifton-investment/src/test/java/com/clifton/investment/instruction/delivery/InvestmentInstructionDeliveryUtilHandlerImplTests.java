package com.clifton.investment.instruction.delivery;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.instruction.Instruction;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionDefinitionSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


@InvestmentInMemoryDatabaseContext
public class InvestmentInstructionDeliveryUtilHandlerImplTests extends BaseInMemoryDatabaseTests {

	@Resource
	public InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler;

	@Resource
	public InvestmentInstructionDeliveryService investmentInstructionDeliveryService;

	@Resource
	public BusinessCompanyService businessCompanyService;

	@Resource
	public InvestmentInstrumentService investmentInstrumentService;

	@Resource
	public InvestmentAccountService investmentAccountService;

	@Resource
	public InvestmentInstructionSetupService investmentInstructionSetupService;


	@Test
	public void getAdditionalPartyDeliveryFieldBicListTest() {
		InvestmentInstructionDeliveryType deliveryType = this.investmentInstructionDeliveryService.getInvestmentInstructionDeliveryTypeByName("Trade Instructions – Fixed Income");

		InvestmentInstructionDefinitionSearchForm searchForm = new InvestmentInstructionDefinitionSearchForm();
		searchForm.setDeliveryTypeId(deliveryType.getId());
		List<InvestmentInstructionDefinition> definitions = this.investmentInstructionSetupService.getInvestmentInstructionDefinitionList(searchForm);
		InvestmentInstruction instruction = new InvestmentInstruction();
		instruction.setDefinition(definitions.get(0));
		final InvestmentInstructionItem investmentInstructionItem = new InvestmentInstructionItem();
		investmentInstructionItem.setInstruction(instruction);

		final BusinessCompany deliveryCompany = this.businessCompanyService.getBusinessCompanyByTypeAndName("Custodian", "Bank of New York Mellon");
		final InvestmentSecurity deliveryCurrency = this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly("USD");
		final InvestmentAccount deliveryHoldingAccount = this.investmentAccountService.getInvestmentAccountByNumber("576600");

		InstructionDeliveryFieldCommand deliveryFieldCommand = new InstructionDeliveryFieldCommand() {
			@Override
			public Instruction getInstruction() {
				return investmentInstructionItem;
			}


			@Override
			public BusinessCompany getDeliveryCompany() {
				return deliveryCompany;
			}


			@Override
			public InvestmentAccount getDeliveryAccount() {
				return deliveryHoldingAccount;
			}


			@Override
			public InvestmentSecurity getDeliveryCurrency() {
				return deliveryCurrency;
			}


			@Override
			public InvestmentAccount getDeliveryClientAccount() {
				return null;
			}
		};
		List<String> bics = this.investmentInstructionDeliveryUtilHandler.getAdditionalPartyDeliveryFieldBicList(deliveryFieldCommand);
		Assertions.assertEquals(2, bics.size());
		Assertions.assertTrue(bics.contains("BIC_01"));
		Assertions.assertTrue(bics.contains("BIC_04"));
	}
}
