SELECT 'InvestmentInstructionDeliveryType' AS entityTableName, InvestmentInstructionDeliveryTypeID AS entityId FROM InvestmentInstructionDeliveryType
UNION
SELECT 'BusinessCompany' AS entityTableName, BusinessCompanyID AS entityId FROM BusinessCompany WHERE CompanyName = 'Bank of New York Mellon'
UNION
SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityId FROM InvestmentSecurity WHERE Symbol = 'USD'
UNION
SELECT 'InvestmentInstructionDelivery' AS entityTableName, InvestmentInstructionDeliveryID AS entityId FROM InvestmentInstructionDelivery WHERE DeliveryName = 'Bank of New York Mellon FED Instructions'
UNION
SELECT 'InvestmentInstructionDelivery' AS entityTableName, InvestmentInstructionDeliveryID AS entityId FROM InvestmentInstructionDelivery WHERE DeliveryName = 'Nomura FED Instructions'
UNION
SELECT 'InvestmentAccount' AS entityTableName, InvestmentAccountID AS entityId FROM InvestmentAccount WHERE AccountName = 'Alfred P. Sloan Foundation'
UNION
SELECT 'InvestmentAccount' AS entityTableName, InvestmentAccountID AS entityId FROM InvestmentAccount WHERE AccountName = 'Bon Secours Health Assurance'
UNION
SELECT 'InvestmentInstructionDeliveryFieldType' AS entityTableName, InvestmentInstructionDeliveryFieldTypeID AS entityId FROM InvestmentInstructionDeliveryFieldType
UNION
SELECT 'InvestmentInstructionDeliveryBusinessCompany' AS entityTableName, InvestmentInstructionDeliveryBusinessCompanyID AS entityId FROM InvestmentInstructionDeliveryBusinessCompany
UNION
SELECT 'InvestmentInstructionDefinition' AS entityTableName, InvestmentInstructionDefinitionID AS entityId FROM InvestmentInstructionDefinition WHERE InvestmentInstructionDeliveryTypeID = (SELECT InvestmentInstructionDeliveryTypeID FROM InvestmentInstructionDeliveryType WHERE DeliveryTypeName = 'Trade Instructions – Fixed Income');
