package com.clifton.investment.instruction;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionUtilHandler;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTableBuilder;
import com.clifton.system.schema.util.SystemSchemaUtilHandler;
import com.clifton.system.schema.util.SystemSchemaUtilHandlerImpl;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;


public class InstructionUtilHandlerTests {

	public static final BiFunction<Field, Object, Object> GetFieldValue = (f, o) -> {
		try {
			return f.get(o);
		}
		catch (IllegalAccessException e) {
			throw new RuntimeException("Could net fetch field value " + f, e);
		}
	};

	@InjectMocks
	public InstructionUtilHandler<InvestmentInstructionItem, InstructionDeliveryFieldCommand> instructionUtilHandler = new InstructionUtilHandlerImpl();

	@InjectMocks
	public SystemSchemaUtilHandler SystemSchemaUtilHandler = new SystemSchemaUtilHandlerImpl();

	@Mock
	public SystemSchemaService systemSchemaService;

	@Mock
	public InvestmentInstructionService investmentInstructionService;

	@Mock
	public DaoLocator daoLocator;

	@Mock
	public ReadOnlyDAO<IdentityObject> instructionDao;

	@Mock
	public DAOConfiguration<IdentityObject> instructionConfig;

	@Mock
	public ReadOnlyDAO<IdentityObject> hierarchyDao;

	@Mock
	public DAOConfiguration<IdentityObject> hierarchyConfig;

	@Mock
	public ReadOnlyDAO<IdentityObject> securityDao;

	@Mock
	public DAOConfiguration<IdentityObject> securityConfig;


	@BeforeEach
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);

		Object[] mocks = Arrays.stream(this.getClass().getDeclaredFields()).filter(f -> Arrays.stream(AnnotationUtils.getAnnotations(f))
				.anyMatch(a -> Objects.equals(a.annotationType(), Mock.class))).map(f -> GetFieldValue.apply(f, this)).toArray();

		((InstructionUtilHandlerImpl) this.instructionUtilHandler).setSystemSchemaUtilHandler(this.SystemSchemaUtilHandler);

		Mockito.reset(mocks);
	}


	@Test
	public void getInstructionList() {
		InvestmentInstructionItem investmentInstructionItem = new InvestmentInstructionItem();
		investmentInstructionItem.setId(33);
		Mockito.doReturn(SystemTableBuilder.createAccountingPositionTransfer().toSystemTable()).when(this.systemSchemaService).getSystemTableByName("AccountingPositionTransfer");
		Mockito.doReturn(Collections.singletonList(
				investmentInstructionItem
		)).when(this.investmentInstructionService).getInvestmentInstructionItemList(ArgumentMatchers.argThat(
				s -> Objects.equals(55, s.getFkFieldId()) && Objects.equals((short) 175, s.getTableId())
		));

		List<InvestmentInstructionItem> instructionList = this.instructionUtilHandler.getInstructionList("AccountingPositionTransfer", 55);
		Assertions.assertFalse(instructionList.isEmpty());
		Assertions.assertEquals(1, instructionList.size());
	}


	@Test
	public void getTransactionReferenceNumber() {
		InvestmentSecurity security = InvestmentSecurityBuilder.createDiax().toInvestmentSecurity();
		Instruction instruction = new InvestmentInstructionItem();
		((InvestmentInstructionItem) instruction).setId(22);

		Mockito.doReturn(this.instructionDao).when(this.daoLocator).locate(instruction);
		Mockito.doReturn(this.instructionConfig).when(this.instructionDao).getConfiguration();
		Mockito.doReturn("InvestmentInstructionItem").when(this.instructionConfig).getTableName();
		Mockito.doReturn(SystemTableBuilder.createInvestmentInstructionItem().toSystemTable()).when(this.systemSchemaService).getSystemTableByName("InvestmentInstructionItem");

		Mockito.doReturn(this.securityDao).when(this.daoLocator).locate(security);
		Mockito.doReturn(this.securityConfig).when(this.securityDao).getConfiguration();
		Mockito.doReturn("InvestmentSecurity").when(this.securityConfig).getTableName();
		Mockito.doReturn(SystemTableBuilder.createInvestmentSecurity().withAlias("IS").toSystemTable()).when(this.systemSchemaService).getSystemTableByName("InvestmentSecurity");

		Mockito.doReturn(this.hierarchyDao).when(this.daoLocator).locate(security.getInstrument().getHierarchy());
		Mockito.doReturn(this.hierarchyConfig).when(this.hierarchyDao).getConfiguration();
		Mockito.doReturn("InvestmentInstrumentHierarchy").when(this.hierarchyConfig).getTableName();
		Mockito.doReturn(SystemTableBuilder.createInvestmentInstrumentHierarchy().withAlias("IIH").toSystemTable()).when(this.systemSchemaService).getSystemTableByName("InvestmentInstrumentHierarchy");

		String referenceNumber = this.instructionUtilHandler.getTransactionReferenceNumber(Arrays.asList(instruction, security.getInstrument().getHierarchy(), security));
		MatcherAssert.assertThat(referenceNumber, IsEqual.equalTo("IMS22II;IMS104IIH;IMS29363IS"));
	}
}
