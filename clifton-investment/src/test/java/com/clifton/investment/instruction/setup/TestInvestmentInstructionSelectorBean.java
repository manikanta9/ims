package com.clifton.investment.instruction.setup;


import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.search.InvestmentInstructionSelectionSearchForm;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelection;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelector;

import java.util.Date;
import java.util.List;


/**
 * The <code>TestInvestmentInstructionSelectorBean</code> class
 * is used to test instruction definition setup
 *
 * @author manderson
 */
public class TestInvestmentInstructionSelectorBean<T extends IdentityObject> implements InvestmentInstructionSelector<T> {

	private String testStringValue;
	private Integer testIntegerValue;


	//////////////////////////////////////////////////


	@Override
	public List<InvestmentInstructionSelection> getInstructionSelectionList(InvestmentInstructionDefinition definition, Date date) {
		return null;
	}


	@Override
	public List<InvestmentInstructionSelection> getInstructionSelectionMissingList(InvestmentInstructionCategory category, Date date, InvestmentInstructionSelectionSearchForm searchForm) {
		return null;
	}


	@Override
	public String getInstructionItemLabel(T entity) {
		return "Test Entity: " + entity.getIdentity();
	}


	@Override
	public boolean isInstructionItemBooked(InvestmentInstructionCategory category, T entity) {
		return false;
	}


	@Override
	public boolean isInstructionItemReady(InvestmentInstructionItem instructionItem, T entity) {
		return false;
	}


	@Override
	public void populateInstructionSelectionDetails(InvestmentInstructionSelection selection, T entity) {
		// Do nothing
	}

	//////////////////////////////////////////////////


	public String getTestStringValue() {
		return this.testStringValue;
	}


	public void setTestStringValue(String testStringValue) {
		this.testStringValue = testStringValue;
	}


	public Integer getTestIntegerValue() {
		return this.testIntegerValue;
	}


	public void setTestIntegerValue(Integer testIntegerValue) {
		this.testIntegerValue = testIntegerValue;
	}
}
