package com.clifton.investment;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "investment";
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("spot");
	}


	@Override
	public boolean isMethodSkipped(Method method) {
		Set<String> skipMethods = new HashSet<>();
		skipMethods.add("saveInvestmentInstrument");
		skipMethods.add("deleteInvestmentInstrument");
		skipMethods.add("saveInvestmentSecurity");
		skipMethods.add("deleteInvestmentSecurity");
		skipMethods.add("saveInvestmentSecurityEventPayout");
		skipMethods.add("getInvestmentSecurityEventExtendedList");
		skipMethods.add("saveInvestmentReplication");

		skipMethods.add("saveInvestmentManagerAccountAssignment");
		skipMethods.add("saveInvestmentManagerAccountGroup");
		skipMethods.add("saveInvestmentManagerAccountBalance");
		skipMethods.add("saveInvestmentManagerAccountBalanceAdjustment");
		skipMethods.add("deleteInvestmentAccountAssetClass");
		skipMethods.add("saveInvestmentAccountAssetClass");
		skipMethods.add("saveInvestmentAccountRebalanceHistory");
		skipMethods.add("deleteInvestmentManagerAccountBalance");
		skipMethods.add("saveInvestmentAccountRebalance");
		skipMethods.add("deleteInvestmentAccountRebalanceHistory");
		skipMethods.add("saveInvestmentInstrumentStructure");
		skipMethods.add("saveInvestmentInstrumentSecurityStructure");
		skipMethods.add("saveInvestmentInstructionDefinition");
		skipMethods.add("deleteInvestmentInstructionDefinition");
		skipMethods.add("deleteInvestmentInstruction");
		skipMethods.add("deleteInvestmentGroupItem");
		skipMethods.add("saveInvestmentGroupMatrix");
		skipMethods.add("deleteInvestmentGroupMatrix");
		skipMethods.add("getInvestmentManagerAccountBalanceStaleSummaryList");
		skipMethods.add("getInvestmentManagerAccountBalanceStaleDetailList");
		skipMethods.add("saveInvestmentAccountGroupAccount");
		skipMethods.add("getInvestmentManagerCustodianTransactionList");
		skipMethods.add("deleteInvestmentInstructionDeliveryBusinessCompany");
		skipMethods.add("deleteInvestmentAccountSecurityTarget");
		skipMethods.add("deleteInvestmentInstructionContact");
		return skipMethods.contains((method.getName()));
	}


	@Override
	protected void configureDTOSkipPropertyNames(Set<String> skipPropertyNames) {
		skipPropertyNames.add("cashPercentType");
		skipPropertyNames.add("linkedManager");
		skipPropertyNames.add("rollupManager");
		skipPropertyNames.add("proxyManager");
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.springframework.dao.EmptyResultDataAccessException");
		imports.add("org.apache.commons.csv.CSVFormat");
		imports.add("org.apache.commons.csv.CSVParser");
		imports.add("org.apache.commons.csv.CSVRecord");
		imports.add("org.apache.poi.ss.usermodel.Cell");
		imports.add("org.apache.poi.ss.usermodel.CellType");
		imports.add("org.apache.poi.ss.usermodel.Row");
		imports.add("org.apache.poi.ss.usermodel.Sheet");
		imports.add("org.apache.poi.ss.usermodel.Workbook");
		imports.add("org.springframework.transaction.interceptor.TransactionAspectSupport");
		imports.add("org.springframework.transaction.support.TransactionSynchronizationManager");
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveInvestmentInstrumentSecurityStructure");
		ignoredVoidSaveMethodSet.add("saveInvestmentManagerAccountBalanceAdjustment");
		ignoredVoidSaveMethodSet.add("saveInvestmentSpecificityEntryPropertiesForEntry");
		ignoredVoidSaveMethodSet.add("saveInvestmentInstructionListComplete");
		ignoredVoidSaveMethodSet.add("saveInvestmentInstructionItemListComplete");
		ignoredVoidSaveMethodSet.add("saveInvestmentSecurityEvents");
		ignoredVoidSaveMethodSet.add("saveInvestmentInstructionItemError");
		ignoredVoidSaveMethodSet.add("saveInvestmentInstructionItemListError");
		return ignoredVoidSaveMethodSet;
	}
}
