package com.clifton.investment.account.securitytarget.util;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;


/**
 * A set of tests to test the {@link InvestmentAccountSecurityTargetAdjustmentUtilHandler} for proper operation.
 *
 * @author davidi
 */
@InvestmentInMemoryDatabaseContext
public class InvestmentAccountSecurityTargetAdjustmentUtilHandlerTests extends BaseInMemoryDatabaseTests {

	@Resource
	InvestmentInstrumentService investmentInstrumentService;

	@Resource
	InvestmentAccountService investmentAccountService;

	@Resource
	InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;

	@Resource
	InvestmentAccountSecurityTargetAdjustmentUtilHandler investmentAccountSecurityTargetAdjustmentUtilHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAdjustInvestmentAccountSecurityTargetQuantity() {
		Date date = DateUtils.toDate("07/20/2016");
		BigDecimal quantity = BigDecimal.valueOf(105.0);
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("PG", false);
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccountByNumber("W-000340");
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(account.getId(), security.getId(), date);
		secTarget.setEndDate(null);
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity();
		BigDecimal expectedQuantity = MathUtils.add(initialQuantity, quantity);

		this.investmentAccountSecurityTargetAdjustmentUtilHandler.adjustInvestmentAccountSecurityTargetQuantity(secTarget, quantity);

		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(account, security);

		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testAdjustInvestmentAccountSecurityTargetQuantity_inactive_target() {
		Date date = DateUtils.toDate("07/20/2016");

		BigDecimal quantity = BigDecimal.valueOf(105.0);
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("PG", false);
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccountByNumber("W-000340");
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(account.getId(), security.getId(), date);
		secTarget.setEndDate(secTarget.getStartDate());
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity();

		this.investmentAccountSecurityTargetAdjustmentUtilHandler.adjustInvestmentAccountSecurityTargetQuantity(secTarget, quantity);

		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(account, security);

		//The security target is inactive, so the process should not have created a new version
		Assertions.assertNull(updatedSecurityTarget);
		secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(account.getId(), security.getId(), date);

		// The quantity of the original security target should not have been altered.
		Assertions.assertEquals(initialQuantity, secTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testAdjustInvestmentAccountSecurityTargetNotionalFromQuantity() {
		Date date = DateUtils.toDate("01/02/2019");
		BigDecimal quantity = BigDecimal.valueOf(1000);
		BigDecimal price = BigDecimal.valueOf(200.0);
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("SPX", false);
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccountByNumber("W-001453");
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(account.getId(), security.getId(), date);
		secTarget.setEndDate(null);
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal initialNotional = secTarget.getTargetUnderlyingNotional();
		BigDecimal expectedNotional = MathUtils.add(initialNotional, BigDecimal.valueOf(200000.00));

		this.investmentAccountSecurityTargetAdjustmentUtilHandler.adjustInvestmentAccountSecurityTargetNotionalFromQuantity(secTarget, quantity, price);
		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(account, security);

		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedNotional, updatedSecurityTarget.getTargetUnderlyingNotional().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testAdjustInvestmentAccountSecurityTargetNotional() {
		Date date = DateUtils.toDate("01/02/2019");
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("SPX", false);
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccountByNumber("W-001453");
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(account.getId(), security.getId(), date);
		secTarget.setEndDate(null);
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal notionalValue = BigDecimal.valueOf(600000.00);
		BigDecimal initialNotional = secTarget.getTargetUnderlyingNotional();
		BigDecimal expectedNotional = MathUtils.add(initialNotional, notionalValue);

		this.investmentAccountSecurityTargetAdjustmentUtilHandler.adjustInvestmentAccountSecurityTargetNotional(secTarget, notionalValue);

		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(account, security);

		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedNotional, updatedSecurityTarget.getTargetUnderlyingNotional().setScale(2, RoundingMode.DOWN));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentAccountSecurityTarget getActiveInvestmentAccountSecurityTarget(InvestmentAccount account, InvestmentSecurity security) {
		InvestmentAccountSecurityTargetSearchForm investmentAccountSecurityTargetSearchForm = new InvestmentAccountSecurityTargetSearchForm();

		investmentAccountSecurityTargetSearchForm.setClientInvestmentAccountId(account.getId());
		investmentAccountSecurityTargetSearchForm.setTargetUnderlyingSecurityId(security.getId());
		investmentAccountSecurityTargetSearchForm.setActive(true);
		investmentAccountSecurityTargetSearchForm.setOrderBy("startDate:desc");

		List<InvestmentAccountSecurityTarget> targetList = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetList(investmentAccountSecurityTargetSearchForm);
		if (!targetList.isEmpty()) {
			return targetList.get(0);
		}
		return null;
	}
}
