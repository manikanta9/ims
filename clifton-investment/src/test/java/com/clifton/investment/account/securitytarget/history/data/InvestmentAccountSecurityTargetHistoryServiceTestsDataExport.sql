SELECT 'InvestmentAccountSecurityTarget' AS entityTableName, InvestmentAccountSecurityTargetID AS entityID FROM InvestmentAccountSecurityTarget WHERE ClientInvestmentAccountID IN (SELECT InvestmentAccountID FROM InvestmentAccount WHERE AccountNumber ='W-001440') AND StartDate < '2019-02-07' UNION ALL
SELECT 'InvestmentAccountSecurityTarget' AS entityTableName, InvestmentAccountSecurityTargetID AS entityID FROM InvestmentAccountSecurityTarget WHERE ClientInvestmentAccountID IN (SELECT InvestmentAccountID FROM InvestmentAccount WHERE AccountNumber = 'W-000977') AND StartDate < '2019-02-20';

