-- Using Terminated account 523500 so these shouldn't change
SELECT 'InvestmentAccountAssetClass' AS entityTableName, InvestmentAccountAssetClassID AS entityId FROM InvestmentAccountAssetClass WHERE InvestmentAccountID = 2480
UNION
SELECT 'InvestmentAccountAssetClassAssignment' AS entityTableName, InvestmentAccountAssetClassAssignmentID AS entityId FROM InvestmentAccountAssetClassAssignment a INNER JOIN InvestmentAccountAssetClass ac ON a.ParentInvestmentAccountAssetClassID = ac.InvestmentAccountAssetClassID WHERE ac.InvestmentAccountID = 2480
UNION
SELECT 'SystemTable' AS EntityTableName, SystemTableID FROM SystemTable;
