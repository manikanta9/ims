SELECT 'InvestmentAccount' AS entityTableName, InvestmentAccountID AS entityId FROM InvestmentAccount WHERE AccountNumber = '051253'
UNION
SELECT 'InvestmentAccountRelationship' AS entityTableName, InvestmentAccountRelationshipID AS entityId FROM InvestmentAccountRelationship WHERE MainAccountID = (SELECT InvestmentAccountID FROM InvestmentAccount WHERE AccountNumber = '051253')
UNION
SELECT 'InvestmentAccountType' AS entityTableName, InvestmentAccountTypeID AS entityId FROM InvestmentAccountType;
