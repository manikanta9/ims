package com.clifton.investment.account.assetclass.position;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.assetclass.search.InvestmentAccountAssetClassSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


/**
 * A set of tests to ensure the proper behavior of the InvestmentAccountAssetClassPositionAllocationSearchForm.
 *
 * @author davidi
 */
@InvestmentInMemoryDatabaseContext
public class InvestmentAccountAssetClassPositionAllocationSearchFormTests extends BaseInMemoryDatabaseTests {

	@Resource
	private InvestmentAccountAssetClassPositionAllocationService investmentAccountAssetClassPositionAllocationService;

	@Resource
	private InvestmentAccountAssetClassService investmentAccountAssetClassService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetInvestmentAccountAssetClassPositionAllocationList_search_by_assetClass_no_displayName() {
		final String assetClassName = "Public Equities";
		InvestmentAccountAssetClassPositionAllocationSearchForm searchForm = new InvestmentAccountAssetClassPositionAllocationSearchForm();

		searchForm.setAssetClassName(assetClassName);
		searchForm.setStartDate(DateUtils.toDate("09/30/2016"));
		List<InvestmentAccountAssetClassPositionAllocation> investmentAccountAssetClassPositionAllocationList = this.investmentAccountAssetClassPositionAllocationService.getInvestmentAccountAssetClassPositionAllocationList(searchForm);

		Assertions.assertEquals(1, investmentAccountAssetClassPositionAllocationList.size());
		InvestmentAccountAssetClassPositionAllocation investmentAccountAssetClassPositionAllocation = investmentAccountAssetClassPositionAllocationList.get(0);

		Assertions.assertEquals(assetClassName, investmentAccountAssetClassPositionAllocation.getAccountAssetClass().getAssetClass().getName());
		Assertions.assertEquals(assetClassName, investmentAccountAssetClassPositionAllocation.getAccountAssetClass().getLabel());
		Assertions.assertNull(investmentAccountAssetClassPositionAllocation.getAccountAssetClass().getDisplayName());
	}


	@Test
	public void testGetInvestmentAccountAssetClassList_search_by_displayName_in_assetClass_property() {
		final String assetClassName = "Public Equities";
		final String displayName = "PE";
		updateDisplayNameForAssetClass(assetClassName, displayName);
		InvestmentAccountAssetClassPositionAllocationSearchForm searchForm = new InvestmentAccountAssetClassPositionAllocationSearchForm();

		searchForm.setAssetClassName(displayName);
		searchForm.setStartDate(DateUtils.toDate("09/30/2016"));
		List<InvestmentAccountAssetClassPositionAllocation> investmentAccountAssetClassPositionAllocationList = this.investmentAccountAssetClassPositionAllocationService.getInvestmentAccountAssetClassPositionAllocationList(searchForm);

		Assertions.assertEquals(1, investmentAccountAssetClassPositionAllocationList.size());
		InvestmentAccountAssetClassPositionAllocation investmentAccountAssetClassPositionAllocation = investmentAccountAssetClassPositionAllocationList.get(0);

		Assertions.assertEquals(assetClassName, investmentAccountAssetClassPositionAllocation.getAccountAssetClass().getAssetClass().getName());
		Assertions.assertEquals(displayName, investmentAccountAssetClassPositionAllocation.getAccountAssetClass().getLabel());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void updateDisplayNameForAssetClass(String assetClassName, String displayName) {
		InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();
		searchForm.setAssetClassName(assetClassName);
		List<InvestmentAccountAssetClass> investmentAccountAssetClassList = this.investmentAccountAssetClassService.getInvestmentAccountAssetClassList(searchForm);

		Assertions.assertEquals(1, investmentAccountAssetClassList.size());
		InvestmentAccountAssetClass investmentAccountAssetClass = investmentAccountAssetClassList.get(0);
		investmentAccountAssetClass.setDisplayName(displayName);

		this.investmentAccountAssetClassService.saveInvestmentAccountAssetClass(investmentAccountAssetClass);
	}
}
