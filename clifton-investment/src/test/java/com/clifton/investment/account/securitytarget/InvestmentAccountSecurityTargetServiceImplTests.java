package com.clifton.investment.account.securitytarget;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


/**
 * @author MitchellF
 */
@InvestmentInMemoryDatabaseContext
public class InvestmentAccountSecurityTargetServiceImplTests extends BaseInMemoryDatabaseTests {

	@Resource
	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;


	@Test
	public void testChangeUnderlyingSecurity() {
		InvestmentAccountSecurityTarget securityTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTarget(11691);
		// This is necessary because object reference is the same as DB object
		InvestmentAccountSecurityTarget clonedTarget = BeanUtils.cloneBean(securityTarget, false, true);
		clonedTarget.setTargetUnderlyingSecurity(this.investmentInstrumentService.getInvestmentSecurity(1618));
		TestUtils.expectException(ValidationException.class, () -> this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(clonedTarget), "Underlying security cannot be changed on a Security Target.");
	}
}
