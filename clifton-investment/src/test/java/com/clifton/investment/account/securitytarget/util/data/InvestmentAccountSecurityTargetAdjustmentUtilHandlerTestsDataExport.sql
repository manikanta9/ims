SELECT 'InvestmentAccountSecurityTarget' AS entityTableName, InvestmentAccountSecurityTargetID AS entityID FROM InvestmentAccountSecurityTarget WHERE ClientInvestmentAccountID IN (SELECT InvestmentAccountID FROM InvestmentAccount WHERE AccountNumber ='W-000340') AND StartDate = '2016-07-20' UNION ALL
SELECT 'InvestmentAccountSecurityTarget' AS entityTableName, InvestmentAccountSecurityTargetID AS entityID FROM InvestmentAccountSecurityTarget WHERE ClientInvestmentAccountID IN (SELECT InvestmentAccountID FROM InvestmentAccount WHERE AccountNumber = 'W-001453') AND StartDate = '2019-01-02';

