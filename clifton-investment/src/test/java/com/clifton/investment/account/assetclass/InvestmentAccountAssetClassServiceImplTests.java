package com.clifton.investment.account.assetclass;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.test.excel.BaseExcelTest;
import com.clifton.core.test.excel.BaseExcelTestConfig;
import com.clifton.core.test.excel.TableConfig;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.system.upload.SystemUploadCommand;
import com.clifton.system.upload.SystemUploadService;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>InvestmentAccountAssetClassServiceImplTests</code> ...
 *
 * @author Mary Anderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentAccountAssetClassServiceImplTests extends BaseExcelTest {

	@Resource
	private SystemUploadService systemUploadService;

	@Resource
	private ReadOnlyDAO<InvestmentAccountAssetClass> investmentAccountAssetClassDAO;

	@Resource
	private InvestmentAccountAssetClassService investmentAccountAssetClassService;


	private BaseExcelTestConfig setupTest(String filePath) {
		BaseExcelTestConfig config = new BaseExcelTestConfig(filePath);

		// Tabs to upload
		config.addTabTableNameUpload("InvestmentAccount", "InvestmentAccount");
		config.addTabTableNameUpload("AssetClass", "InvestmentAssetClass");
		config.addTabTableNameUpload("Account-AssetClass", "InvestmentAccountAssetClass");

		// Tabs with Results
		config.addTabTableNameResult("RESULT_Account-AssetClass", "InvestmentAccountAssetClass");

		// Result Table Configs
		TableConfig resultConfig = new TableConfig("InvestmentAccountAssetClass", new String[]{"account.number", "assetClass.name"}, new String[]{"AccountNumber", "AssetClassName"});
		resultConfig.addValidateField("assetClassPercentage", "AssetClassPercentage");
		resultConfig.addValidateField("cashPercentage", "CashPercentage");

		config.addTableConfig(resultConfig);
		return config;
	}


	@Test
	public void testRollupAccountAssetClassPercentages() {
		BaseExcelTestConfig config = setupTest("src/test/java/com/clifton/investment/account/assetclass/RollupInvestmentAccountAssetClassTests.xls");
		processFile(config);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadData(BaseExcelTestConfig config) {
		SystemUploadCommand upload = new SystemUploadCommand();
		upload.setWorkbook(config.getWorkbook());
		upload.setExistingBeans(FileUploadExistingBeanActions.INSERT);
		upload.setInsertMissingFKBeans(true);
		this.systemUploadService.uploadSystemUploadFileSheets(upload, config.getTabTableNameUploadMap());
	}


	@Override
	public void processData(@SuppressWarnings("unused") BaseExcelTestConfig config) {

		// Link Rollup Asset Classes
		InvestmentAccountAssetClass pubEq = getAccountAssetClass("1-0001", "Public Equity");
		InvestmentAccountAssetClass dom = getAccountAssetClass("1-0001", "Domestic Equity");

		this.investmentAccountAssetClassService.linkInvestmentAccountAssetClassRollup(pubEq.getId(), dom.getId());

		InvestmentAccountAssetClass intl = getAccountAssetClass("1-0001", "International Equity");
		this.investmentAccountAssetClassService.linkInvestmentAccountAssetClassRollup(pubEq.getId(), intl.getId());

		InvestmentAccountAssetClass domLarge = getAccountAssetClass("1-0001", "Domestic Equity Large Cap");
		InvestmentAccountAssetClass domMid = getAccountAssetClass("1-0001", "Domestic Equity Mid Cap");
		this.investmentAccountAssetClassService.linkInvestmentAccountAssetClassRollup(dom.getId(), domLarge.getId());
		this.investmentAccountAssetClassService.linkInvestmentAccountAssetClassRollup(dom.getId(), domMid.getId());

		InvestmentAccountAssetClass intlLarge = getAccountAssetClass("1-0001", "International Equity Large Cap");
		InvestmentAccountAssetClass intlMid = getAccountAssetClass("1-0001", "International Equity Mid Cap");
		this.investmentAccountAssetClassService.linkInvestmentAccountAssetClassRollup(intl.getId(), intlLarge.getId());
		this.investmentAccountAssetClassService.linkInvestmentAccountAssetClassRollup(intl.getId(), intlMid.getId());

		// Load all investment account asset class and add 1% to dom equity cash and 5% to both dom equity targets
		for (InvestmentAccountAssetClass iaac : CollectionUtils.getIterable(this.investmentAccountAssetClassDAO.findAll())) {
			if (!iaac.isRollupAssetClass() && iaac.getAssetClass().getName().startsWith("Domestic")) {
				iaac.setCashPercentage(iaac.getCashPercentage().add(new BigDecimal("1")));
				iaac.setAssetClassPercentage(iaac.getAssetClassPercentage().add(new BigDecimal("5")));
				this.investmentAccountAssetClassService.saveInvestmentAccountAssetClass(iaac);
			}
		}

		// Remove Domestic Equity Large Cap from Domestic Equity
		this.investmentAccountAssetClassService.deleteInvestmentAccountAssetClassRollup(domLarge.getId());
		// Add it to International Equity
		this.investmentAccountAssetClassService.linkInvestmentAccountAssetClassRollup(intl.getId(), domLarge.getId());
		// Add 10 % to cash
		domLarge = getAccountAssetClass("1-0001", "Domestic Equity Large Cap");
		domLarge.setCashPercentage(MathUtils.add(domLarge.getCashPercentage(), BigDecimal.TEN));
		this.investmentAccountAssetClassService.saveInvestmentAccountAssetClass(domLarge);
		// Remove from IE
		this.investmentAccountAssetClassService.deleteInvestmentAccountAssetClassRollup(domLarge.getId());
		// Re-Add it to DE
		this.investmentAccountAssetClassService.linkInvestmentAccountAssetClassRollup(dom.getId(), domLarge.getId());
		// Set percentage back
		domLarge = getAccountAssetClass("1-0001", "Domestic Equity Large Cap");
		domLarge.setCashPercentage(MathUtils.subtract(domLarge.getCashPercentage(), BigDecimal.TEN));
		this.investmentAccountAssetClassService.saveInvestmentAccountAssetClass(domLarge);


		// Validate Customized Replication Getter
		for (InvestmentAccountAssetClass iaac : CollectionUtils.getIterable(this.investmentAccountAssetClassDAO.findAll())) {
			String acName = iaac.getAssetClass().getName();
			List<InvestmentReplication> repList = this.investmentAccountAssetClassService.getInvestmentReplicationListForAccountAssetClass(iaac.getId());

			// Primary: Replication 1, Secondary: Replication 3
			if ("Domestic Equity Large Cap".equals(acName)) {
				validateReplicationListSize(acName, 2, repList, new String[]{"Test Replication 1", "Test Replication 3"});
			}

			// Primary: Replication 2 (uses 3 & 4 for matching)
			else if ("International Equity Large Cap".equals(acName)) {
				validateReplicationListSize(acName, 3, repList, new String[]{"Test Replication 2", "Test Replication 3", "Test Replication 4"});
			}
			// Primary: Replication 4
			else if ("International Equity Mid Cap".equals(acName)) {
				validateReplicationListSize(acName, 1, repList, new String[]{"Test Replication 4"});
			}

			// No Replications
			else {
				validateReplicationListSize(acName, 0, repList, null);
			}
		}
	}


	private void validateReplicationListSize(String assetClassName, int expectedSize, List<InvestmentReplication> list, String[] replicationNames) {
		Assertions.assertEquals(expectedSize, CollectionUtils.getSize(list), assetClassName + " replication list expected size of " + expectedSize + ", but was " + CollectionUtils.getSize(list));
		if (replicationNames != null) {
			for (String repName : replicationNames) {
				boolean found = false;

				for (InvestmentReplication rep : CollectionUtils.getIterable(list)) {
					if (repName.equals(rep.getName())) {
						found = true;
						break;
					}
				}
				AssertUtils.assertTrue(found, assetClassName + " expected Replication [" + repName + "] to be included but wasn't found.");
			}
		}
	}


	private InvestmentAccountAssetClass getAccountAssetClass(String accountNumber, String assetClassName) {
		return this.investmentAccountAssetClassDAO.findOneByFields(new String[]{"account.number", "assetClass.name"}, new Object[]{accountNumber, assetClassName});
	}


	@Override
	public List<?> getExistingRecords(String tableName) {
		if ("InvestmentAccountAssetClass".equals(tableName)) {
			return this.investmentAccountAssetClassDAO.findAll();
		}
		return null;
	}
}
