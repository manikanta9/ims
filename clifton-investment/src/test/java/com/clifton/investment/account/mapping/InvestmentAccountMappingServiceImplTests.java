package com.clifton.investment.account.mapping;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.mapping.search.InvestmentAccountMappingSearchForm;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


/**
 * Unit tests for InvestmentAccountMappingService
 *
 * @author davidi
 */
//@ContextConfiguration("InvestmentAccountMappingServiceImplTests-context.xml")
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentAccountMappingServiceImplTests {

	private InvestmentAccountMapping investmentAccountMapping_01 = null;
	private InvestmentAccountMapping investmentAccountMapping_02 = null;


	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountMappingServiceImplTests() {
		InvestmentAccount investmentAccount1 = new InvestmentAccount();
		investmentAccount1.setNumber("0000-0001");
		investmentAccount1.setId(1001);

		InvestmentAccount investmentAccount2 = new InvestmentAccount();
		investmentAccount2.setNumber("0000-0002");
		investmentAccount2.setId(1002);

		InvestmentAccountMappingPurpose mappingPurpose = new InvestmentAccountMappingPurpose();
		mappingPurpose.setFieldName("Mapping Purpose EMSX");
		mappingPurpose.setName("EMSX Mapping");
		mappingPurpose.setId((short) 1);
		mappingPurpose.setDescription("Purpose for account mapping with EMSX trading.");

		this.investmentAccountMapping_01 = new InvestmentAccountMapping();
		this.investmentAccountMapping_02 = new InvestmentAccountMapping();

		this.investmentAccountMapping_01.setId(100);
		this.investmentAccountMapping_01.setInvestmentAccount(investmentAccount1);
		this.investmentAccountMapping_01.setInvestmentAccountMappingPurpose(mappingPurpose);
		this.investmentAccountMapping_01.setFieldValue("0000-0001-mapped");

		this.investmentAccountMapping_02.setId(200);
		this.investmentAccountMapping_02.setInvestmentAccount(investmentAccount1);
		this.investmentAccountMapping_02.setInvestmentAccountMappingPurpose(mappingPurpose);
		this.investmentAccountMapping_02.setFieldValue("0000-0002-mapped");
	}


	@Mock
	AdvancedUpdatableDAO<InvestmentAccountMapping, Criteria> investmentAccountMappingDAO;

	@Mock
	private AdvancedUpdatableDAO<InvestmentAccountMappingPurpose, Criteria> investmentAccountMappingPurposeDAO;

	@Resource
	@InjectMocks
	private InvestmentAccountMappingService investmentAccountMappingService;


	@BeforeEach
	public void initialize() {
		MockitoAnnotations.initMocks(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void verifyServiceInstantiation() {
		Assertions.assertNotNull(this.investmentAccountMappingService, "InvestmentAccountMappingService not instantiated");
	}


	@Test
	public void getInvestmentAccountMappingListTest() {
		List<InvestmentAccountMapping> expectedAccountMapping = CollectionUtils.createList(this.investmentAccountMapping_01);
		when(this.investmentAccountMappingDAO.findBySearchCriteria(any())).thenReturn(expectedAccountMapping);

		Assertions.assertEquals(expectedAccountMapping, this.investmentAccountMappingService.getInvestmentAccountMappingList(new InvestmentAccountMappingSearchForm()));
	}


	@Test
	public void getInvestmentAccountMappingByAccountNumberAndPurposeNullAccountTest() {
		List<InvestmentAccountMapping> expectedAccountMapping = CollectionUtils.createList(this.investmentAccountMapping_01);
		when(this.investmentAccountMappingDAO.findBySearchCriteria(any())).thenReturn(expectedAccountMapping);
		InvestmentAccount nullAccount = null;
		InvestmentAccountMappingPurpose purpose = this.investmentAccountMapping_01.getInvestmentAccountMappingPurpose();

		Assertions.assertNull(this.investmentAccountMappingService.getInvestmentAccountMappingByAccountNumberAndPurpose(nullAccount, purpose));
	}


	@Test
	public void getInvestmentAccountMappingByAccountNumberAndPurposeNullPurposeTest() {
		List<InvestmentAccountMapping> expectedAccountMapping = CollectionUtils.createList(this.investmentAccountMapping_01);
		when(this.investmentAccountMappingDAO.findBySearchCriteria(any())).thenReturn(expectedAccountMapping);
		InvestmentAccount account = this.investmentAccountMapping_01.getInvestmentAccount();
		InvestmentAccountMappingPurpose nullPurpose = null;

		Assertions.assertNull(this.investmentAccountMappingService.getInvestmentAccountMappingByAccountNumberAndPurpose(account, nullPurpose));
	}


	@Test
	public void getInvestmentAccountMappingByAccountNumberAndPurposeTest() {
		List<InvestmentAccountMapping> expectedAccountMapping = CollectionUtils.createList(this.investmentAccountMapping_01);
		when(this.investmentAccountMappingDAO.findBySearchCriteria(any())).thenReturn(expectedAccountMapping);
		InvestmentAccount account = this.investmentAccountMapping_01.getInvestmentAccount();
		InvestmentAccountMappingPurpose purpose = this.investmentAccountMapping_01.getInvestmentAccountMappingPurpose();

		Assertions.assertEquals(expectedAccountMapping.get(0), this.investmentAccountMappingService.getInvestmentAccountMappingByAccountNumberAndPurpose(account, purpose));
	}


	@Test
	public void getInvestmentAccountMappedValueTest() {
		List<InvestmentAccountMapping> expectedAccountMapping = CollectionUtils.createList(this.investmentAccountMapping_01);
		when(this.investmentAccountMappingDAO.findBySearchCriteria(any())).thenReturn(expectedAccountMapping);
		InvestmentAccount account = this.investmentAccountMapping_01.getInvestmentAccount();
		InvestmentAccountMappingPurpose purpose = this.investmentAccountMapping_01.getInvestmentAccountMappingPurpose();

		Assertions.assertEquals(this.investmentAccountMapping_01.getFieldValue(), this.investmentAccountMappingService.getInvestmentAccountMappedValue(account, purpose));
	}


	@Test
	public void getInvestmentAccountReverseMappingTest() {
		List<InvestmentAccountMapping> expectedAccountMapping = CollectionUtils.createList(this.investmentAccountMapping_01);
		when(this.investmentAccountMappingDAO.findBySearchCriteria(any())).thenReturn(expectedAccountMapping);
		InvestmentAccountMappingPurpose purpose = this.investmentAccountMapping_01.getInvestmentAccountMappingPurpose();

		Assertions.assertEquals(this.investmentAccountMapping_01.getInvestmentAccount().getNumber(), this.investmentAccountMappingService.getInvestmentAccountReverseMapping(this.investmentAccountMapping_01.getFieldValue(), purpose));
	}


	@Test
	public void getInvestmentAccountReverseMapping_NullPurposeTest() {
		Assertions.assertThrows(RuntimeException.class, () -> {
			List<InvestmentAccountMapping> expectedAccountMapping = CollectionUtils.createList(this.investmentAccountMapping_01);
			when(this.investmentAccountMappingDAO.findBySearchCriteria(any())).thenReturn(expectedAccountMapping);
			InvestmentAccountMappingPurpose purpose = null;

			// a null purpose value should result in RuntimeException
			this.investmentAccountMappingService.getInvestmentAccountReverseMapping(this.investmentAccountMapping_01.getFieldValue(), purpose);
		});
	}


	@Test
	public void getInvestmentAccountReverseMapping_MultipleMappingsReturnedTest() {
		Assertions.assertThrows(RuntimeException.class, () -> {
			List<InvestmentAccountMapping> expectedAccountMapping = CollectionUtils.createList(this.investmentAccountMapping_01, this.investmentAccountMapping_02);
			when(this.investmentAccountMappingDAO.findBySearchCriteria(any())).thenReturn(expectedAccountMapping);
			InvestmentAccountMappingPurpose purpose = this.investmentAccountMapping_01.getInvestmentAccountMappingPurpose();

			// multiple return values should result in RuntimeException
			this.investmentAccountMappingService.getInvestmentAccountReverseMapping(this.investmentAccountMapping_01.getFieldValue(), purpose);
		});
	}
}



