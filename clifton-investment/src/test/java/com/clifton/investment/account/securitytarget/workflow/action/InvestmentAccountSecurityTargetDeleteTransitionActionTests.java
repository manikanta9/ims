package com.clifton.investment.account.securitytarget.workflow.action;

import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import com.clifton.investment.account.securitytarget.util.InvestmentAccountSecurityTargetAdjustmentUtilHandler;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.search.SystemTableSearchForm;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.transition.WorkflowTransition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;


/**
 * A set of tests to test the {@link InvestmentAccountSecurityTargetDeleteTransitionAction} for proper operation.
 *
 * @author davidi
 */
@InvestmentInMemoryDatabaseContext
public class InvestmentAccountSecurityTargetDeleteTransitionActionTests extends BaseInMemoryDatabaseTests {

	private boolean initialized;

	private InvestmentAccountSecurityTargetDeleteTransitionAction transitionAction = new InvestmentAccountSecurityTargetDeleteTransitionAction();

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;

	@Resource
	private InvestmentAccountSecurityTargetAdjustmentUtilHandler investmentAccountSecurityTargetAdjustmentUtilHandler;

	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private DaoLocator daoLocator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		if (!this.initialized) {
			this.transitionAction = new InvestmentAccountSecurityTargetDeleteTransitionAction();
			this.transitionAction.setDaoLocator(this.daoLocator);
			this.transitionAction.setInvestmentAccountSecurityTargetService(this.investmentAccountSecurityTargetService);
			this.initialized = true;
		}
	}


	@Test
	public void testProcessAction_restorePreviousTarget_true() {
		Date date = DateUtils.toDate("07/20/2016");
		BigDecimal quantity = BigDecimal.valueOf(105.0);
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("PG", false);
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccountByNumber("W-000340");
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(account.getId(), security.getId(), date);
		secTarget.setEndDate(null);
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity();
		BigDecimal expectedQuantity = MathUtils.add(initialQuantity, quantity);

		this.investmentAccountSecurityTargetAdjustmentUtilHandler.adjustInvestmentAccountSecurityTargetQuantity(secTarget, quantity);

		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(account, security);
		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));

		WorkflowTask wfTask = createWorkflowTask(updatedSecurityTarget.getId());

		// should delete new target and restore previous target to active state
		this.transitionAction.setRestorePreviousSecurityTarget(true);
		this.transitionAction.processAction(wfTask, new WorkflowTransition());

		updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(account, security);
		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(initialQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
		Assertions.assertEquals(secTarget.getId(), updatedSecurityTarget.getId());
		Assertions.assertNull(updatedSecurityTarget.getEndDate());
	}


	@Test
	public void testProcessAction_restorePreviousTarget_false() {
		Date date = DateUtils.toDate("07/20/2016");
		BigDecimal quantity = BigDecimal.valueOf(105.0);
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("PG", false);
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccountByNumber("W-000340");
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(account.getId(), security.getId(), date);
		secTarget.setEndDate(null);
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity();
		BigDecimal expectedQuantity = MathUtils.add(initialQuantity, quantity);

		this.investmentAccountSecurityTargetAdjustmentUtilHandler.adjustInvestmentAccountSecurityTargetQuantity(secTarget, quantity);

		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(account, security);
		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));

		WorkflowTask wfTask = createWorkflowTask(updatedSecurityTarget.getId());

		// should delete new target but not restore previous target to active state
		this.transitionAction.setRestorePreviousSecurityTarget(false);
		this.transitionAction.processAction(wfTask, new WorkflowTransition());

		updatedSecurityTarget = getMostRecentHistoricalInvestmentAccountSecurityTarget(account, security);
		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(initialQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
		Assertions.assertEquals(secTarget.getId(), updatedSecurityTarget.getId());
		Assertions.assertNotNull(updatedSecurityTarget.getEndDate());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentAccountSecurityTarget getActiveInvestmentAccountSecurityTarget(InvestmentAccount account, InvestmentSecurity security) {
		InvestmentAccountSecurityTargetSearchForm investmentAccountSecurityTargetSearchForm = new InvestmentAccountSecurityTargetSearchForm();

		investmentAccountSecurityTargetSearchForm.setClientInvestmentAccountId(account.getId());
		investmentAccountSecurityTargetSearchForm.setTargetUnderlyingSecurityId(security.getId());
		investmentAccountSecurityTargetSearchForm.setActive(true);
		investmentAccountSecurityTargetSearchForm.setOrderBy("startDate:desc");

		List<InvestmentAccountSecurityTarget> targetList = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetList(investmentAccountSecurityTargetSearchForm);
		if (!targetList.isEmpty()) {
			return targetList.get(0);
		}
		return null;
	}


	private InvestmentAccountSecurityTarget getMostRecentHistoricalInvestmentAccountSecurityTarget(InvestmentAccount account, InvestmentSecurity security) {
		InvestmentAccountSecurityTargetSearchForm investmentAccountSecurityTargetSearchForm = new InvestmentAccountSecurityTargetSearchForm();

		investmentAccountSecurityTargetSearchForm.setClientInvestmentAccountId(account.getId());
		investmentAccountSecurityTargetSearchForm.setTargetUnderlyingSecurityId(security.getId());
		investmentAccountSecurityTargetSearchForm.setActive(false);
		investmentAccountSecurityTargetSearchForm.setOrderBy("endDate:desc");

		List<InvestmentAccountSecurityTarget> targetList = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetList(investmentAccountSecurityTargetSearchForm);
		if (!targetList.isEmpty()) {
			return targetList.get(0);
		}
		return null;
	}


	private WorkflowTask createWorkflowTask(Integer securityTargetId) {
		WorkflowTask wfTask = new WorkflowTask();
		WorkflowTaskDefinition wfTaskDef = new WorkflowTaskDefinition();

		SystemTableSearchForm systemTableSearchForm = new SystemTableSearchForm();
		systemTableSearchForm.setTableNames(new String[]{"InvestmentAccountSecurityTarget"});

		SystemTable systemTable = this.systemSchemaService.getSystemTableByName("InvestmentAccountSecurityTarget");
		wfTaskDef.setLinkedEntityTable(systemTable);
		wfTaskDef.setLinkedEntityFkFieldId((long) securityTargetId);
		wfTask.setLinkedEntityFkFieldId((long) securityTargetId);
		wfTask.setLabel("Test workflow task");

		wfTask.setDefinition(wfTaskDef);
		return wfTask;
	}
}
