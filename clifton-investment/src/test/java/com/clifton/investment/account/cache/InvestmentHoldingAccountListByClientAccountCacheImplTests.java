package com.clifton.investment.account.cache;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * A set of tests designed to test the (@link InvestmentHoldingAccountListByClientAccountCache) for proper operation.
 *
 * @author davidi
 */
@InvestmentInMemoryDatabaseContext
public class InvestmentHoldingAccountListByClientAccountCacheImplTests extends BaseInMemoryDatabaseTests {

	@Resource
	InvestmentHoldingAccountListByClientAccountCache investmentHoldingAccountListByClientAccountCache;

	@Resource
	InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetHoldingAccountList_no_account_type() {
		Set<String> expectedHoldingAccountNumbersSet = CollectionUtils.createHashSet("AS9-BNYM", "AS9-MSCS");
		Date date = DateUtils.toDate("08/05/2019");
		InvestmentAccountType clsType = this.investmentAccountService.getInvestmentAccountTypeByName("OTC CLS");
		InvestmentAccount clsAccount = this.investmentAccountService.getInvestmentAccountByNumber("AS9-BNYM");
		clsAccount.setType(clsType);
		this.investmentAccountService.saveInvestmentAccount(clsAccount);


		InvestmentAccount clientAccount = this.investmentAccountService.getInvestmentAccountByNumber("051253");
		List<InvestmentAccount> investmentAccountList = this.investmentHoldingAccountListByClientAccountCache.getHoldingAccountList(clientAccount.getId(), "Trading: Forwards", date, null);
		Set<String> cacheResultSet = CollectionUtils.getStream(investmentAccountList).map(InvestmentAccount::getNumber).collect(Collectors.toSet());
		Assertions.assertEquals(expectedHoldingAccountNumbersSet, cacheResultSet);
	}


	@Test
	public void testGetHoldingAccountList_with_account_type() {
		Set<String> expectedHoldingAccountNumbersSet = CollectionUtils.createHashSet("AS9-BNYM");
		Date date = DateUtils.toDate("08/05/2019");
		InvestmentAccountType clsType = this.investmentAccountService.getInvestmentAccountTypeByName("OTC CLS");
		InvestmentAccount clsAccount = this.investmentAccountService.getInvestmentAccountByNumber("AS9-BNYM");
		clsAccount.setType(clsType);
		this.investmentAccountService.saveInvestmentAccount(clsAccount);


		InvestmentAccount clientAccount = this.investmentAccountService.getInvestmentAccountByNumber("051253");
		List<InvestmentAccount> investmentAccountList = this.investmentHoldingAccountListByClientAccountCache.getHoldingAccountList(clientAccount.getId(), "Trading: Forwards", date, clsType);
		Set<String> cacheResultSet = CollectionUtils.getStream(investmentAccountList).map(InvestmentAccount::getNumber).collect(Collectors.toSet());
		Assertions.assertEquals(expectedHoldingAccountNumbersSet, cacheResultSet);
	}
}
