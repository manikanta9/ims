package com.clifton.investment.account.comparison;

import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Arrays;


/**
 * The <code>InvestmentAccountInGroupComparisonTests</code> ...
 *
 * @author Mary Anderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentAccountInGroupComparisonTests {

	@Resource
	private ApplicationContextService applicationContextService;

	@Resource
	private InvestmentAccountService investmentAccountService;


	private InvestmentAccountInGroupComparison getComparisonBean() {
		InvestmentAccountInGroupComparison comparison = new InvestmentAccountInGroupComparison();
		comparison.setAccountBeanPropertyName("account");
		comparison.setAccountGroupIdList(Arrays.asList(1));
		getApplicationContextService().autowireBean(comparison);
		return comparison;
	}


	private InvestmentAccountInGroupComparison getComparisonBeanListComparingAll() {
		InvestmentAccountInGroupComparison comparison = new InvestmentAccountInGroupComparison();
		comparison.setAccountBeanPropertyName("account");
		comparison.setAccountGroupIdList(Arrays.asList(1, 2));
		getApplicationContextService().autowireBean(comparison);
		return comparison;
	}


	private InvestmentAccountInGroupComparison getComparisonBeanListComparingAny() {
		InvestmentAccountInGroupComparison comparison = new InvestmentAccountInGroupComparison();
		comparison.setAccountBeanPropertyName("account");
		comparison.setAccountGroupIdList(Arrays.asList(1, 2));
		comparison.setMatchAnyGroup(true);
		getApplicationContextService().autowireBean(comparison);
		return comparison;
	}


	@Test
	public void testNoAccount() {
		InvestmentAccountAssetClass testBean = new InvestmentAccountAssetClass();

		InvestmentAccountInGroupComparison comparison = getComparisonBean();
		Assertions.assertFalse(comparison.evaluate(testBean, null));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(testBean, context));
		Assertions.assertNull(context.getTrueMessage());
		Assertions.assertEquals("(Investment Account Property Value is not set, therefore no account to verify in group(s) [Group 1])", context.getFalseMessage());
	}


	@Test
	public void testAccountInGroup() {
		// Account 1 is in Group 1
		InvestmentAccountAssetClass testBean = new InvestmentAccountAssetClass();
		testBean.setAccount(this.investmentAccountService.getInvestmentAccount(1));

		InvestmentAccountInGroupComparison comparison = getComparisonBean();
		Assertions.assertTrue(comparison.evaluate(testBean, null));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(testBean, context));
		Assertions.assertEquals("(Account 000001 is in all groups in group list [Group 1].)", context.getTrueMessage());
		Assertions.assertNull(context.getFalseMessage());
	}


	@Test
	public void testAccountNotInGroup() {

		// Account 2 is NOT in Group 1
		InvestmentAccountAssetClass testBean = new InvestmentAccountAssetClass();
		testBean.setAccount(this.investmentAccountService.getInvestmentAccount(2));

		InvestmentAccountInGroupComparison comparison = getComparisonBean();
		Assertions.assertFalse(comparison.evaluate(testBean, null));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(testBean, context));
		Assertions.assertNull(context.getTrueMessage());
		Assertions.assertEquals("(Account 000002 is not in at least one group in group list [Group 1].)", context.getFalseMessage());
	}


	@Test
	public void testAccountInBothGroupsComparingAll() {
		// Account 1 is in Group 1 and Group 2
		InvestmentAccountAssetClass testBean = new InvestmentAccountAssetClass();
		testBean.setAccount(this.investmentAccountService.getInvestmentAccount(1));

		InvestmentAccountInGroupComparison comparison = getComparisonBeanListComparingAll();
		Assertions.assertTrue(comparison.evaluate(testBean, null));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(testBean, context));
		Assertions.assertEquals("(Account 000001 is in all groups in group list [Group 1, Group 2].)", context.getTrueMessage());
		Assertions.assertNull(context.getFalseMessage());
	}


	@Test
	public void testAccountInOneGroupNotInOneGroupComparingAll() {
		// Account 2 is in Group 2 but not Group 1
		InvestmentAccountAssetClass testBean = new InvestmentAccountAssetClass();
		testBean.setAccount(this.investmentAccountService.getInvestmentAccount(2));

		InvestmentAccountInGroupComparison comparison = getComparisonBeanListComparingAll();
		Assertions.assertFalse(comparison.evaluate(testBean, null));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(testBean, context));
		Assertions.assertNull(context.getTrueMessage());
		Assertions.assertEquals("(Account 000002 is not in at least one group in group list [Group 1, Group 2].)", context.getFalseMessage());
	}


	@Test
	public void testAccountNotInBothGroupsComparingAll() {
		// Account 2 is in Group 2 but not Group 1
		InvestmentAccountAssetClass testBean = new InvestmentAccountAssetClass();
		testBean.setAccount(this.investmentAccountService.getInvestmentAccount(2));

		InvestmentAccountInGroupComparison comparison = getComparisonBeanListComparingAll();
		Assertions.assertFalse(comparison.evaluate(testBean, null));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(testBean, context));
		Assertions.assertNull(context.getTrueMessage());
		Assertions.assertEquals("(Account 000002 is not in at least one group in group list [Group 1, Group 2].)", context.getFalseMessage());
	}


	@Test
	public void testAccountInBothGroupsComparingAny() {
		// Account 1 is in Group 1 and Group 2
		InvestmentAccountAssetClass testBean = new InvestmentAccountAssetClass();
		testBean.setAccount(this.investmentAccountService.getInvestmentAccount(1));

		InvestmentAccountInGroupComparison comparison = getComparisonBeanListComparingAny();
		Assertions.assertTrue(comparison.evaluate(testBean, null));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(testBean, context));
		Assertions.assertEquals("(Account 000001 is in at least one group in group list [Group 1, Group 2].)", context.getTrueMessage());
		Assertions.assertNull(context.getFalseMessage());
	}


	@Test
	public void testAccountInOneGroupNotInOneGroupComparingAny() {
		// Account 2 is in Group 2 but not Group 1
		InvestmentAccountAssetClass testBean = new InvestmentAccountAssetClass();
		testBean.setAccount(this.investmentAccountService.getInvestmentAccount(2));

		InvestmentAccountInGroupComparison comparison = getComparisonBeanListComparingAny();
		Assertions.assertTrue(comparison.evaluate(testBean, null));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(testBean, context));
		Assertions.assertEquals("(Account 000002 is in at least one group in group list [Group 1, Group 2].)", context.getTrueMessage());
		Assertions.assertNull(context.getFalseMessage());
	}


	@Test
	public void testAccountNotInBothGroupsComparingAny() {
		// Account 3 is not in either group
		InvestmentAccountAssetClass testBean = new InvestmentAccountAssetClass();
		testBean.setAccount(this.investmentAccountService.getInvestmentAccount(3));

		InvestmentAccountInGroupComparison comparison = getComparisonBeanListComparingAny();
		Assertions.assertFalse(comparison.evaluate(testBean, null));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(testBean, context));
		Assertions.assertNull(context.getTrueMessage());
		Assertions.assertEquals("(Account 000003 is not in any groups in group list [Group 1, Group 2].)", context.getFalseMessage());
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
