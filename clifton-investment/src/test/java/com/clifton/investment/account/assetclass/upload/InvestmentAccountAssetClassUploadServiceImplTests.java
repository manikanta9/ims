package com.clifton.investment.account.assetclass.upload;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;

@InvestmentInMemoryDatabaseContext
public class InvestmentAccountAssetClassUploadServiceImplTests extends BaseInMemoryDatabaseTests {


	@Resource
	private InvestmentAccountAssetClassService investmentAccountAssetClassService;

	@Resource
	private InvestmentAccountAssetClassUploadService investmentAccountAssetClassUploadService;


	private static final int ACCOUNT_523500 = 2480;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testUploadAccountAssetClass_UpdateTargets() {
		List<InvestmentAccountAssetClass> accountAssetClassList = this.investmentAccountAssetClassService.getInvestmentAccountAssetClassListByAccount(ACCOUNT_523500);

		InvestmentAccountAssetClassUploadCommand uploadCommand = new InvestmentAccountAssetClassUploadCommand();
		uploadCommand.setFile(new MultipartFileImpl("src/test/java/com/clifton/investment/account/assetclass/upload/InvestmentAccountAssetClass-523500 PCF-Update Targets.xls"));

		this.investmentAccountAssetClassUploadService.uploadInvestmentAccountAssetClassUploadFile(uploadCommand);

		List<InvestmentAccountAssetClass> newAccountAssetClassList = this.investmentAccountAssetClassService.getInvestmentAccountAssetClassListByAccount(ACCOUNT_523500);
		Assertions.assertEquals(accountAssetClassList.size(), newAccountAssetClassList.size());

		for (InvestmentAccountAssetClass accountAssetClass : accountAssetClassList) {
			boolean found = false;
			for (InvestmentAccountAssetClass newAccountAssetClass : newAccountAssetClassList) {
				if (accountAssetClass.equals(newAccountAssetClass)) {
					found = true;
					List<String> changedProperties = CoreCompareUtils.getNoEqualPropertiesWithSetters(accountAssetClass, newAccountAssetClass, false, "assetClassPercentage");
					Assertions.assertTrue(CollectionUtils.isEmpty(changedProperties), "Did not expect any properties except asset class target to be updated, but found " + CollectionUtils.toString(changedProperties, 10));
					break;
				}
			}
			Assertions.assertTrue(found, "Did not find asset class: " + accountAssetClass.getAssetClass().getName() + " in the updated list");
		}
	}


	@Test
	public void testUploadAccountAssetClass_UpdateInactive_NotAllowed() {
		InvestmentAccountAssetClassUploadCommand uploadCommand = new InvestmentAccountAssetClassUploadCommand();
		uploadCommand.setFile(new MultipartFileImpl("src/test/java/com/clifton/investment/account/assetclass/upload/InvestmentAccountAssetClass-523500 PCF-Update Inactive.xls"));

		TestUtils.expectException(ValidationException.class,
				() -> this.investmentAccountAssetClassUploadService.uploadInvestmentAccountAssetClassUploadFile(uploadCommand),
				"You cannot edit inactive asset class: 523500: International Equity - EAFE");
	}


	@Test
	public void testUploadAccountAssetClass_UpdateRollup_NotAllowed() {
		InvestmentAccountAssetClassUploadCommand uploadCommand = new InvestmentAccountAssetClassUploadCommand();
		uploadCommand.setFile(new MultipartFileImpl("src/test/java/com/clifton/investment/account/assetclass/upload/InvestmentAccountAssetClass-523500 PCF-Update Rollup.xls"));

		TestUtils.expectException(ValidationException.class,
				() -> this.investmentAccountAssetClassUploadService.uploadInvestmentAccountAssetClassUploadFile(uploadCommand),
				"You cannot edit rollup asset class: 523500: Total Capital Appreciation");
	}


	@Test
	public void testUploadAccountAssetClass_Insert_NotAllowed() {
		InvestmentAccountAssetClassUploadCommand uploadCommand = new InvestmentAccountAssetClassUploadCommand();
		uploadCommand.setFile(new MultipartFileImpl("src/test/java/com/clifton/investment/account/assetclass/upload/InvestmentAccountAssetClass-523500 PCF-Insert.xls"));

		TestUtils.expectException(ValidationException.class,
				() -> this.investmentAccountAssetClassUploadService.uploadInvestmentAccountAssetClassUploadFile(uploadCommand),
				"New account asset classes cannot be added, you can only edit existing asset classes: 523500: Options");
	}
}
