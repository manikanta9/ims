package com.clifton.investment.account.assetclass.search;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


/**
 * A set of tests to ensure the proper behavior of the InvestmentAccountAssetClassSearchForm.
 *
 * @author davidi
 */
@InvestmentInMemoryDatabaseContext
public class InvestmentAccountAssetClassSearchFormTests extends BaseInMemoryDatabaseTests {

	@Resource
	private InvestmentAccountAssetClassService investmentAccountAssetClassService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetInvestmentAccountAssetClassList_search_by_assetClass_no_displayName() {
		final String assetClassName = "Public Equities";
		InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();

		searchForm.setAssetClassName(assetClassName);
		List<InvestmentAccountAssetClass> investmentAccountAssetClassList = this.investmentAccountAssetClassService.getInvestmentAccountAssetClassList(searchForm);

		Assertions.assertEquals(1, investmentAccountAssetClassList.size());
		InvestmentAccountAssetClass investmentAccountAssetClass = investmentAccountAssetClassList.get(0);

		Assertions.assertEquals(assetClassName, investmentAccountAssetClassList.get(0).getAssetClass().getName());
		Assertions.assertEquals(assetClassName, investmentAccountAssetClass.getLabel());
		Assertions.assertNull(investmentAccountAssetClass.getDisplayName());
	}


	@Test
	public void testGetInvestmentAccountAssetClassList_search_by_assetClassEquals_no_displayName() {
		final String assetClassName = "Public Equities";
		InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();

		searchForm.setAssetClassNameEquals(assetClassName);
		List<InvestmentAccountAssetClass> investmentAccountAssetClassList = this.investmentAccountAssetClassService.getInvestmentAccountAssetClassList(searchForm);

		Assertions.assertEquals(1, investmentAccountAssetClassList.size());
		InvestmentAccountAssetClass investmentAccountAssetClass = investmentAccountAssetClassList.get(0);

		Assertions.assertEquals(assetClassName, investmentAccountAssetClass.getAssetClass().getName());
		Assertions.assertEquals(assetClassName, investmentAccountAssetClass.getLabel());
		Assertions.assertNull(investmentAccountAssetClass.getDisplayName());
	}


	@Test
	public void testGetInvestmentAccountAssetClassList_search_by_displayName_in_assetClass_property() {
		final String assetClassName = "Domestic Bonds";
		final String displayName = "DB";

		updateDisplayNameForAssetClass(assetClassName, displayName);
		InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();

		searchForm.setAssetClassName(displayName);
		List<InvestmentAccountAssetClass> investmentAccountAssetClassList = this.investmentAccountAssetClassService.getInvestmentAccountAssetClassList(searchForm);

		Assertions.assertEquals(1, investmentAccountAssetClassList.size());
		InvestmentAccountAssetClass investmentAccountAssetClass = investmentAccountAssetClassList.get(0);

		Assertions.assertEquals(assetClassName, investmentAccountAssetClass.getAssetClass().getName());
		Assertions.assertEquals(displayName, investmentAccountAssetClass.getDisplayName());
		Assertions.assertEquals(displayName, investmentAccountAssetClass.getLabel());
	}


	@Test
	public void testGetInvestmentAccountAssetClassList_search_by_displayName_in_assetClassEquals_property() {
		final String assetClassName = "Domestic Bonds";
		final String displayName = "DB";

		updateDisplayNameForAssetClass(assetClassName, displayName);
		InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();

		searchForm.setAssetClassNameEquals(displayName);
		List<InvestmentAccountAssetClass> investmentAccountAssetClassList = this.investmentAccountAssetClassService.getInvestmentAccountAssetClassList(searchForm);

		Assertions.assertEquals(1, investmentAccountAssetClassList.size());
		InvestmentAccountAssetClass investmentAccountAssetClass = investmentAccountAssetClassList.get(0);

		Assertions.assertEquals(assetClassName, investmentAccountAssetClass.getAssetClass().getName());
		Assertions.assertEquals(displayName, investmentAccountAssetClass.getDisplayName());
		Assertions.assertEquals(displayName, investmentAccountAssetClass.getLabel());
	}


	@Test
	public void testGetInvestmentAccountAssetClassList_search_by_displayName() {
		final String assetClassName = "Domestic Bonds";
		final String displayName = "DB";

		updateDisplayNameForAssetClass(assetClassName, displayName);
		InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();

		searchForm.setDisplayName(displayName);
		List<InvestmentAccountAssetClass> investmentAccountAssetClassList = this.investmentAccountAssetClassService.getInvestmentAccountAssetClassList(searchForm);

		Assertions.assertEquals(1, investmentAccountAssetClassList.size());
		InvestmentAccountAssetClass investmentAccountAssetClass = investmentAccountAssetClassList.get(0);

		Assertions.assertEquals(assetClassName, investmentAccountAssetClass.getAssetClass().getName());
		Assertions.assertEquals(displayName, investmentAccountAssetClass.getDisplayName());
		Assertions.assertEquals(displayName, investmentAccountAssetClass.getLabel());
	}


	@Test
	public void testGetInvestmentAccountAssetClassList_search_by_displayNameEquals() {
		final String assetClassName = "Domestic Bonds";
		final String displayName = "DB";

		updateDisplayNameForAssetClass(assetClassName, displayName);
		InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();

		searchForm.setDisplayNameEquals(displayName);
		List<InvestmentAccountAssetClass> investmentAccountAssetClassList = this.investmentAccountAssetClassService.getInvestmentAccountAssetClassList(searchForm);

		Assertions.assertEquals(1, investmentAccountAssetClassList.size());
		InvestmentAccountAssetClass investmentAccountAssetClass = investmentAccountAssetClassList.get(0);

		Assertions.assertEquals(assetClassName, investmentAccountAssetClass.getAssetClass().getName());
		Assertions.assertEquals(displayName, investmentAccountAssetClass.getDisplayName());
		Assertions.assertEquals(displayName, investmentAccountAssetClass.getLabel());
	}


	@Test
	public void testGetInvestmentAccountAssetClassList_search_by_displayName_not_exists() {
		final String assetClassName = "Domestic Bonds";
		final String displayName = "DB";

		updateDisplayNameForAssetClass(assetClassName, displayName);
		InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();

		searchForm.setAssetClassNameEquals("DOES_NOT_EXIST");
		List<InvestmentAccountAssetClass> investmentAccountAssetClassList = this.investmentAccountAssetClassService.getInvestmentAccountAssetClassList(searchForm);

		Assertions.assertEquals(0, investmentAccountAssetClassList.size());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void updateDisplayNameForAssetClass(String assetClassName, String displayName) {
		InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();
		searchForm.setAssetClassName(assetClassName);
		List<InvestmentAccountAssetClass> investmentAccountAssetClassList = this.investmentAccountAssetClassService.getInvestmentAccountAssetClassList(searchForm);

		Assertions.assertEquals(1, investmentAccountAssetClassList.size());
		InvestmentAccountAssetClass investmentAccountAssetClass = investmentAccountAssetClassList.get(0);
		investmentAccountAssetClass.setDisplayName(displayName);

		this.investmentAccountAssetClassService.saveInvestmentAccountAssetClass(investmentAccountAssetClass);
	}
}
