package com.clifton.investment.account;

import com.clifton.business.client.BusinessClientService;
import com.clifton.business.service.BusinessServiceService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>InvestmentAccountServiceImplTests</code> tests methods in InvestmentAccountService
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentAccountServiceImplTests {

	@Resource
	private BusinessClientService businessClientService;

	@Resource
	private BusinessServiceService businessServiceService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////////

	// SERVICE IDS
	private static final short FRANCHISE_CUSTOMIZED_EXPOSURE_MANAGEMENT = 1;
	private static final short SUPER_STRATEGY_CUSTOM_OVERLAY = 2;
	private static final short SERVICE_PIOS = 3;
	private static final short SERVICE_COMPONENT_PIOS_EXPOSURE_REBALANCING = 4;
	private static final short SERVICE_LDI = 5;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAddInvestmentAccountToTerminatedClient() {
		InvestmentAccount clientAccount = populateInvestmentAccount(true);
		clientAccount.setBusinessClient(this.businessClientService.getBusinessClient(102));
		saveInvestmentAccount(clientAccount, "Cannot add a new account to a terminated client.");

		InvestmentAccount holdingAccount = populateInvestmentAccount(false);
		holdingAccount.setBusinessClient(this.businessClientService.getBusinessClient(102));
		saveInvestmentAccount(holdingAccount, "Cannot add a new account to a terminated client.");
	}


	@Test
	public void testInvestmentAccount_NoBusinessServiceSelection() {
		InvestmentAccount clientAccount = populateInvestmentAccount(true);
		saveInvestmentAccount(clientAccount, null);

		InvestmentAccount holdingAccount = populateInvestmentAccount(false);
		saveInvestmentAccount(holdingAccount, null);
	}


	@Test
	public void testInvestmentAccount_BusinessServiceSelection() {
		InvestmentAccount clientAccount = populateInvestmentAccount(true);
		clientAccount.setBusinessService(this.businessServiceService.getBusinessService(FRANCHISE_CUSTOMIZED_EXPOSURE_MANAGEMENT));
		saveInvestmentAccount(clientAccount, "Selected service [Customized Exposure Management] is not assignable to accounts");

		clientAccount.setBusinessService(this.businessServiceService.getBusinessService(SERVICE_LDI));
		saveInvestmentAccount(clientAccount, null);

		InvestmentAccount holdingAccount = populateInvestmentAccount(false);
		holdingAccount.setBusinessService(this.businessServiceService.getBusinessService(SERVICE_LDI));
		saveInvestmentAccount(holdingAccount, "Business Service selection is only available for our/client accounts");
	}


	@Test
	public void testInvestmentAccount_BusinessServiceComponentSelection() {
		InvestmentAccount clientAccount = populateInvestmentAccount(true);
		InvestmentAccountBusinessService accountBusinessService = new InvestmentAccountBusinessService();
		accountBusinessService.setBusinessService(this.businessServiceService.getBusinessService(SERVICE_COMPONENT_PIOS_EXPOSURE_REBALANCING));
		clientAccount.setBusinessServiceList(CollectionUtils.createList(accountBusinessService));
		saveInvestmentAccount(clientAccount, "Service Components can not be selected if the account doesn't have a service selected");

		clientAccount.setBusinessService(this.businessServiceService.getBusinessService(SERVICE_LDI));
		saveInvestmentAccount(clientAccount, "Selected service component [Customized Exposure Management / Custom Overlay / PIOS / Exposure Rebalancing] is not applicable selection for service [Customized Exposure Management / Custom Overlay / LDI].");

		accountBusinessService.setBusinessService(this.businessServiceService.getBusinessService(SERVICE_LDI));
		saveInvestmentAccount(clientAccount, "Selected service component [Customized Exposure Management / Custom Overlay / LDI is not applicable selection (either not a service component or missing parent service)");

		accountBusinessService.setBusinessService(this.businessServiceService.getBusinessService(SERVICE_COMPONENT_PIOS_EXPOSURE_REBALANCING));
		clientAccount.setBusinessService(this.businessServiceService.getBusinessService(SERVICE_PIOS));
		saveInvestmentAccount(clientAccount, "Start Date is required for each service component.");

		accountBusinessService.setStartDate(DateUtils.toDate("10/01/2016"));
		saveInvestmentAccount(clientAccount, null);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private InvestmentAccount populateInvestmentAccount(boolean ourAccount) {
		InvestmentAccount account = new InvestmentAccount();
		if (ourAccount) {
			account.setNumber("123456");
			account.setName("Test Client Account");
			account.setType(this.investmentAccountService.getInvestmentAccountTypeByName("Client"));
		}
		else {
			account.setNumber("Test-123");
			account.setName("Test Futures Account");
			account.setType(this.investmentAccountService.getInvestmentAccountTypeByName("Futures Broker"));
		}
		account.setBusinessClient(this.businessClientService.getBusinessClient(101));
		return account;
	}


	private void saveInvestmentAccount(InvestmentAccount account, String expectedErrorMessage) {
		boolean error = false;
		try {
			this.investmentAccountService.saveInvestmentAccount(account);
		}
		catch (Exception e) {
			error = true;
			if (expectedErrorMessage == null) {
				Assertions.fail("Did not expect error on save of investment account, but did.  Message: " + e.getMessage());
			}
			Assertions.assertEquals(expectedErrorMessage, e.getMessage());
		}
		if (!error && !StringUtils.isEmpty(expectedErrorMessage)) {
			Assertions.fail("Expected error on save with message " + expectedErrorMessage + ", but no error encountered");
		}
	}
}
