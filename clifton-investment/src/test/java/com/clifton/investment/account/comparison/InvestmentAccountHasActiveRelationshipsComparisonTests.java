package com.clifton.investment.account.comparison;

import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentAccountHasActiveRelationshipsComparisonTests {

	@Resource
	private ApplicationContextService applicationContextService;

	@Resource
	private InvestmentAccountService investmentAccountService;


	private InvestmentAccountHasActiveRelationshipsComparison getComparisonBean() {
		InvestmentAccountHasActiveRelationshipsComparison comparison = new InvestmentAccountHasActiveRelationshipsComparison();
		getApplicationContextService().autowireBean(comparison);
		return comparison;
	}


	private InvestmentAccountDoesNotHaveActiveRelationshipsComparison getReverseComparisonBean() {
		InvestmentAccountDoesNotHaveActiveRelationshipsComparison comparison = new InvestmentAccountDoesNotHaveActiveRelationshipsComparison();
		getApplicationContextService().autowireBean(comparison);
		return comparison;
	}


	@Test
	public void testClientAccountWithRelationships() {
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccount(1);

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(getComparisonBean().evaluate(account, context));
		Assertions.assertEquals("(Account 100000) has [1] active account relationship(s).", context.getTrueMessage());
		Assertions.assertNull(context.getFalseMessage());

		context = new SimpleComparisonContext();
		Assertions.assertFalse(getReverseComparisonBean().evaluate(account, context));
		Assertions.assertEquals("(Account 100000) has [1] active account relationship(s).", context.getFalseMessage());
		Assertions.assertNull(context.getTrueMessage());
	}


	@Test
	public void testClientAccountWithoutRelationships() {
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccount(2);

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(getComparisonBean().evaluate(account, context));
		Assertions.assertEquals("(Account 200000) does not have any active account relationships.", context.getFalseMessage());
		Assertions.assertNull(context.getTrueMessage());

		context = new SimpleComparisonContext();
		Assertions.assertTrue(getReverseComparisonBean().evaluate(account, context));
		Assertions.assertEquals("(Account 200000) does not have any active account relationships.", context.getTrueMessage());
		Assertions.assertNull(context.getFalseMessage());
	}


	@Test
	public void testHoldingAccountWithRelationships() {
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccount(10);

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(getComparisonBean().evaluate(account, context));
		Assertions.assertEquals("(Account 100010) has [1] active account relationship(s).", context.getTrueMessage());
		Assertions.assertNull(context.getFalseMessage());

		context = new SimpleComparisonContext();
		Assertions.assertFalse(getReverseComparisonBean().evaluate(account, context));
		Assertions.assertEquals("(Account 100010) has [1] active account relationship(s).", context.getFalseMessage());
		Assertions.assertNull(context.getTrueMessage());
	}


	@Test
	public void testHoldingAccountWithoutRelationships() {
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccount(20);

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(getComparisonBean().evaluate(account, context));
		Assertions.assertEquals("(Account 200010) does not have any active account relationships.", context.getFalseMessage());
		Assertions.assertNull(context.getTrueMessage());

		context = new SimpleComparisonContext();
		Assertions.assertTrue(getReverseComparisonBean().evaluate(account, context));
		Assertions.assertEquals("(Account 200010) does not have any active account relationships.", context.getTrueMessage());
		Assertions.assertNull(context.getFalseMessage());
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
