package com.clifton.investment.account.securitytarget.history;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.securitytarget.history.search.InvestmentAccountSecurityTargetHistorySearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


/**
 * A set of tests to test the {@link InvestmentAccountSecurityTargetHistoryService} for proper operation.
 *
 * @author davidi
 */
@InvestmentInMemoryDatabaseContext
public class InvestmentAccountSecurityTargetHistoryServiceTests extends BaseInMemoryDatabaseTests {

	@Resource
	InvestmentInstrumentService investmentInstrumentService;

	@Resource
	InvestmentAccountService investmentAccountService;

	@Resource
	InvestmentAccountSecurityTargetHistoryService investmentAccountSecurityTargetHistoryService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetInvestmentAccountSecurityTargetHistoryList_notional_target() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("SPX", false);
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccountByNumber("W-001440");
		InvestmentAccountSecurityTargetHistorySearchForm searchForm = new InvestmentAccountSecurityTargetHistorySearchForm();
		searchForm.setClientInvestmentAccountId(account.getId());
		searchForm.setTargetUnderlyingSecurityId(security.getId());

		List<InvestmentAccountSecurityTargetHistory> historyList = this.investmentAccountSecurityTargetHistoryService.getInvestmentAccountSecurityTargetHistoryList(searchForm);

		testNotionalDifference(historyList);
	}


	@Test
	public void testGetInvestmentAccountSecurityTargetHistoryList_quantity_target() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("JNJ", false);
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccountByNumber("W-000977");
		InvestmentAccountSecurityTargetHistorySearchForm searchForm = new InvestmentAccountSecurityTargetHistorySearchForm();
		searchForm.setClientInvestmentAccountId(account.getId());
		searchForm.setTargetUnderlyingSecurityId(security.getId());

		List<InvestmentAccountSecurityTargetHistory> historyList = this.investmentAccountSecurityTargetHistoryService.getInvestmentAccountSecurityTargetHistoryList(searchForm);

		testQuantityDifference(historyList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void testQuantityDifference(List<InvestmentAccountSecurityTargetHistory> securityTargetHistoryList) {
		for (int index = 0; index < securityTargetHistoryList.size(); ++index) {
			InvestmentAccountSecurityTargetHistory currentEntity = securityTargetHistoryList.get(index);

			if (index == securityTargetHistoryList.size() - 1) {
				Assertions.assertEquals(currentEntity.getSecurityTarget().getTargetUnderlyingQuantity(), currentEntity.getQuantityOrNotionalDifference());
			}
			else {
				InvestmentAccountSecurityTargetHistory nextEntity = securityTargetHistoryList.get(index + 1);
				BigDecimal expectedDifference = MathUtils.subtract(currentEntity.getSecurityTarget().getTargetUnderlyingQuantity(), nextEntity.getSecurityTarget().getTargetUnderlyingQuantity());
				Assertions.assertEquals(expectedDifference, currentEntity.getQuantityOrNotionalDifference());
			}
		}
	}


	private void testNotionalDifference(List<InvestmentAccountSecurityTargetHistory> securityTargetHistoryList) {
		for (int index = 0; index < securityTargetHistoryList.size(); ++index) {
			InvestmentAccountSecurityTargetHistory currentEntity = securityTargetHistoryList.get(index);

			if (index == securityTargetHistoryList.size() - 1) {
				Assertions.assertEquals(currentEntity.getSecurityTarget().getTargetUnderlyingNotional(), currentEntity.getQuantityOrNotionalDifference());
			}
			else {
				InvestmentAccountSecurityTargetHistory nextEntity = securityTargetHistoryList.get(index + 1);
				BigDecimal expectedDifference = MathUtils.subtract(currentEntity.getSecurityTarget().getTargetUnderlyingNotional(), nextEntity.getSecurityTarget().getTargetUnderlyingNotional());
				Assertions.assertEquals(expectedDifference, currentEntity.getQuantityOrNotionalDifference());
			}
		}
	}
}
