package com.clifton.investment.account.relationship;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.relationship.cache.InvestmentAccountRelationshipConfig;
import com.clifton.investment.account.relationship.cache.InvestmentAccountRelationshipConfig.InvestmentAccountRelationshipAssetClassConfig;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Date;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentAccountRelationshipServiceImplTests {

	@Resource
	private InvestmentAccountRelationshipServiceImpl investmentAccountRelationshipService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	////////////////////////////////////////////////////////////////////////////////
	//////////              Test Account Relationship Setup               //////////
	////////////////////////////////////////////////////////////////////////////////

	private static final int TEST_CLIENT_ACCOUNT_1 = 900;
	private static final int TEST_CLIENT_ACCOUNT_2 = 901;
	private static final int TEST_FUTURES_ACCOUNT_1 = 902;
	private static final int TEST_FUTURES_ACCOUNT_2 = 903;
	private static final int TEST_SWAPS_ACCOUNT_1 = 904;
	private static final int TEST_SWAPS_ACCOUNT_2 = 905;


	private static final String TEST_FAKE_PURPOSE_NAME = "Fake Purpose";

	////////////////////////////////////////////////////////////////////////////
	////////          InvestmentAccountRelationshipMapping Tests        ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveInvestmentAccountRelationshipMapping_OverlapInDates() {
		InvestmentAccountRelationshipMapping mapping = new InvestmentAccountRelationshipMapping();
		mapping.setPurpose(this.investmentAccountRelationshipService.getInvestmentAccountRelationshipPurposeByName(TEST_FAKE_PURPOSE_NAME));
		mapping.setMainAccountType(this.investmentAccountService.getInvestmentAccountTypeByName(InvestmentAccountType.CLIENT));
		mapping.setRelatedAccountType(this.investmentAccountService.getInvestmentAccountTypeByName(InvestmentAccountType.CLIENT));

		// NO dates specified - should overlap with 3 existing
		saveInvestmentAccountRelationshipMapping(mapping, "There exists a relationship mapping for Client - Client for purpose Fake Purpose and overlapping date range.");

		// Set specific dates that overlap existing
		mapping.setStartDate(DateUtils.toDate("03/01/2011"));
		mapping.setEndDate(DateUtils.toDate("06/30/2011"));

		saveInvestmentAccountRelationshipMapping(mapping, "There exists a relationship mapping for Client - Client for purpose Fake Purpose and overlapping date range.");

		// Change Date so no more overlap and save
		mapping.setStartDate(DateUtils.toDate("05/01/2011"));
		saveInvestmentAccountRelationshipMapping(mapping, null);

		// Delete the new Mapping
		deleteInvestmentAccountRelationshipMapping(mapping, null);
	}


	@Test
	public void testSaveInvestmentAccountRelationshipMapping_DifferentAccountType() {
		// Create New Mapping, but different account type - no longer an overlap
		InvestmentAccountRelationshipMapping mapping = new InvestmentAccountRelationshipMapping();
		mapping.setPurpose(this.investmentAccountRelationshipService.getInvestmentAccountRelationshipPurposeByName(TEST_FAKE_PURPOSE_NAME));
		mapping.setMainAccountType(this.investmentAccountService.getInvestmentAccountTypeByName(InvestmentAccountType.CLIENT));
		mapping.setRelatedAccountType(this.investmentAccountService.getInvestmentAccountTypeByName(InvestmentAccountType.FUTURES_BROKER));

		saveInvestmentAccountRelationshipMapping(mapping, null);
	}


	@Test
	public void testDeleteInvestmentAccountRelationshipMapping() {
		// Create New Mapping with closed dates
		InvestmentAccountRelationshipMapping mappingClosed = new InvestmentAccountRelationshipMapping();
		mappingClosed.setPurpose(this.investmentAccountRelationshipService.getInvestmentAccountRelationshipPurposeByName(TEST_FAKE_PURPOSE_NAME));
		mappingClosed.setMainAccountType(this.investmentAccountService.getInvestmentAccountTypeByName(InvestmentAccountType.CLIENT));
		mappingClosed.setRelatedAccountType(this.investmentAccountService.getInvestmentAccountTypeByName(InvestmentAccountType.FUTURES_BROKER));
		mappingClosed.setStartDate(DateUtils.toDate("01/01/2019"));
		mappingClosed.setEndDate(DateUtils.toDate("12/31/2019"));
		saveInvestmentAccountRelationshipMapping(mappingClosed, null);

		// Create new Mapping with open dates
		InvestmentAccountRelationshipMapping mappingOpen = new InvestmentAccountRelationshipMapping();
		mappingOpen.setPurpose(this.investmentAccountRelationshipService.getInvestmentAccountRelationshipPurposeByName(TEST_FAKE_PURPOSE_NAME));
		mappingOpen.setMainAccountType(this.investmentAccountService.getInvestmentAccountTypeByName(InvestmentAccountType.CLIENT));
		mappingOpen.setRelatedAccountType(this.investmentAccountService.getInvestmentAccountTypeByName(InvestmentAccountType.FUTURES_BROKER));
		mappingOpen.setStartDate(DateUtils.toDate("01/01/2020"));
		saveInvestmentAccountRelationshipMapping(mappingOpen, null);

		// create new relationship with open dates
		InvestmentAccountRelationship relationshipOpen = new InvestmentAccountRelationship();
		relationshipOpen.setPurpose(this.investmentAccountRelationshipService.getInvestmentAccountRelationshipPurposeByName(TEST_FAKE_PURPOSE_NAME));
		relationshipOpen.setReferenceOne(this.investmentAccountService.getInvestmentAccount(TEST_CLIENT_ACCOUNT_1));
		relationshipOpen.setReferenceTwo(this.investmentAccountService.getInvestmentAccount(TEST_FUTURES_ACCOUNT_1));
		relationshipOpen.setStartDate(DateUtils.toDate("01/02/2020"));
		saveInvestmentAccountRelationship(relationshipOpen, null);

		// create new relationship with closed dates
		InvestmentAccountRelationship relationshipClosed = new InvestmentAccountRelationship();
		relationshipClosed.setPurpose(this.investmentAccountRelationshipService.getInvestmentAccountRelationshipPurposeByName(TEST_FAKE_PURPOSE_NAME));
		relationshipClosed.setReferenceOne(this.investmentAccountService.getInvestmentAccount(TEST_CLIENT_ACCOUNT_1));
		relationshipClosed.setReferenceTwo(this.investmentAccountService.getInvestmentAccount(TEST_FUTURES_ACCOUNT_1));
		relationshipClosed.setStartDate(DateUtils.toDate("01/01/2019"));
		relationshipClosed.setEndDate(DateUtils.toDate("12/01/2019"));

		// delete relationshipMapping with corresponding relationship
		deleteInvestmentAccountRelationshipMapping(mappingOpen, "There exists a relationship for Client - Futures Broker for purpose Fake Purpose and overlapping date range.");
		deleteInvestmentAccountRelationship(relationshipOpen, null);

		//assign relationship to a different date range mapping of the same type.
		saveInvestmentAccountRelationship(relationshipClosed, null);
		deleteInvestmentAccountRelationshipMapping(mappingOpen, null);

		// delete closed mapping
		deleteInvestmentAccountRelationshipMapping(mappingClosed, "There exists a relationship for Client - Futures Broker for purpose Fake Purpose and overlapping date range.");
		deleteInvestmentAccountRelationship(relationshipClosed, null);
		deleteInvestmentAccountRelationshipMapping(mappingClosed, null);
	}


	private void saveInvestmentAccountRelationshipMapping(InvestmentAccountRelationshipMapping relationshipMapping, String expectedErrorMessage) {
		saveInvestmentAccountRelationshipMappingImpl(relationshipMapping, false, expectedErrorMessage);
	}


	private void deleteInvestmentAccountRelationshipMapping(InvestmentAccountRelationshipMapping relationshipMapping, String expectedErrorMessage) {
		saveInvestmentAccountRelationshipMappingImpl(relationshipMapping, true, expectedErrorMessage);
	}


	private void saveInvestmentAccountRelationshipMappingImpl(InvestmentAccountRelationshipMapping relationshipMapping, boolean delete, String expectedErrorMessage) {
		boolean error = false;
		try {
			if (delete) {
				this.investmentAccountRelationshipService.deleteInvestmentAccountRelationshipMapping(relationshipMapping.getId());
			}
			else {
				this.investmentAccountRelationshipService.saveInvestmentAccountRelationshipMapping(relationshipMapping);
			}
		}
		catch (Exception e) {
			error = true;
			if (expectedErrorMessage == null) {
				Assertions.fail("Did not expect error on save of relationship mapping, but did.  Message: " + ExceptionUtils.getOriginalMessage(e));
			}
			Assertions.assertEquals(expectedErrorMessage, e.getMessage());
		}
		if (!error && !StringUtils.isEmpty(expectedErrorMessage)) {
			Assertions.fail("Expected error on save with message " + expectedErrorMessage + ", but no error encountered");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              InvestmentAccountRelationship Tests           ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveInvestmentAccountRelationship_ModifyCondition() {
		// Do Not Allow the Insert
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(InvestmentAccountRelationship.class))).thenReturn("You don't have access!");

		InvestmentAccountRelationship relationship = populateInvestmentAccountRelationship(TEST_CLIENT_ACCOUNT_1, TEST_CLIENT_ACCOUNT_2, InvestmentAccountRelationshipPurpose.CLIFTON_SUB_ACCOUNT_PURPOSE_NAME);
		saveInvestmentAccountRelationship(relationship, "INSERT Not Allowed. You do not have permission to modify this Account Relationship because: You don't have access!");

		// Allow the condition
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(InvestmentAccountRelationship.class))).thenReturn(null);
		saveInvestmentAccountRelationship(relationship, null);

		// Try to Update
		relationship.setStartDate(DateUtils.toDate("01/01/2016"));

		// Do Not Allow the Update
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(InvestmentAccountRelationship.class))).thenReturn("You don't have access!");
		saveInvestmentAccountRelationship(relationship, "UPDATE Not Allowed. You do not have permission to modify this Account Relationship because: You don't have access!");

		// Allow the condition
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(InvestmentAccountRelationship.class))).thenReturn(null);
		saveInvestmentAccountRelationship(relationship, null);

		// Do Not Allow the Delete
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(InvestmentAccountRelationship.class))).thenReturn("You don't have access!");
		deleteInvestmentAccountRelationship(relationship, "DELETE Not Allowed. You do not have permission to modify this Account Relationship because: You don't have access!");

		// Allow the condition
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(InvestmentAccountRelationship.class))).thenReturn(null);
		deleteInvestmentAccountRelationship(relationship, null);
	}


	@Test
	public void testSaveInvestmentAccountRelationship_MissingMapping() {
		InvestmentAccountRelationship relationship = populateInvestmentAccountRelationship(TEST_CLIENT_ACCOUNT_1, TEST_FUTURES_ACCOUNT_1, InvestmentAccountRelationshipPurpose.COLLATERAL_ACCOUNT_PURPOSE_NAME);
		saveInvestmentAccountRelationship(relationship, "Cannot find mapping between 'Client' and 'Futures Broker' account types for purpose: Collateral and date range []");
	}


	@Test
	public void testSaveInvestmentAccountRelationship_MissingMappingForDateRange() {
		InvestmentAccountRelationship relationship = populateInvestmentAccountRelationship(TEST_CLIENT_ACCOUNT_1, TEST_CLIENT_ACCOUNT_2, TEST_FAKE_PURPOSE_NAME);
		relationship.setStartDate(DateUtils.toDate("12/01/2019"));
		saveInvestmentAccountRelationship(relationship, "Cannot find mapping between 'Client' and 'Client' account types for purpose: Fake Purpose and date range [12/01/2019 - ]");
	}


	@Test
	public void testSaveInvestmentAccountRelationship_OneFromMain() {
		// Allow the Insert
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(InvestmentAccountRelationship.class))).thenReturn(null);

		InvestmentAccountRelationship relationship = populateInvestmentAccountRelationship(TEST_CLIENT_ACCOUNT_1, TEST_FUTURES_ACCOUNT_1, InvestmentAccountRelationshipPurpose.TRADING_FUTURES_PURPOSE_NAME);
		saveInvestmentAccountRelationship(relationship, null);

		InvestmentAccountRelationship relationship2 = populateInvestmentAccountRelationship(TEST_CLIENT_ACCOUNT_1, TEST_FUTURES_ACCOUNT_2, InvestmentAccountRelationshipPurpose.TRADING_FUTURES_PURPOSE_NAME);
		saveInvestmentAccountRelationship(relationship2, "Cannot have more than one overlapping relationship from main account for selected account types and purpose. Found: [999900: Test 9999 Client - Parametric Clifton] - [9999-Futures-1: Test 9999 Client - Goldman Sachs (Futures Broker)]");

		// Add Dates
		relationship.setStartDate(DateUtils.toDate("01/01/2015"));
		relationship.setEndDate(DateUtils.toDate("12/31/2015"));
		saveInvestmentAccountRelationship(relationship, null);

		// Still Overlaps
		relationship2.setStartDate(DateUtils.toDate("12/31/2015"));
		saveInvestmentAccountRelationship(relationship2, "Cannot have more than one overlapping relationship from main account for selected account types and purpose. Found: [999900: Test 9999 Client - Parametric Clifton] - [9999-Futures-1: Test 9999 Client - Goldman Sachs (Futures Broker)]");

		// No Overlap
		relationship2.setStartDate(DateUtils.toDate("01/01/2016"));
		saveInvestmentAccountRelationship(relationship, null);
	}


	@Test
	public void testSaveInvestmentAccountRelationship_OneToRelated() {
		// Allow the Insert
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(InvestmentAccountRelationship.class))).thenReturn(null);

		InvestmentAccountRelationship relationship = populateInvestmentAccountRelationship(TEST_CLIENT_ACCOUNT_1, TEST_SWAPS_ACCOUNT_1, InvestmentAccountRelationshipPurpose.TRADING_PURPOSE_NAME_PREFIX + "Swaps");
		saveInvestmentAccountRelationship(relationship, null);

		InvestmentAccountRelationship relationship2 = populateInvestmentAccountRelationship(TEST_CLIENT_ACCOUNT_2, TEST_SWAPS_ACCOUNT_1, InvestmentAccountRelationshipPurpose.TRADING_PURPOSE_NAME_PREFIX + "Swaps");
		saveInvestmentAccountRelationship(relationship2, "Cannot have more than one overlapping relationship to related account for selected account types and purpose. Found: [999900: Test 9999 Client - Parametric Clifton] - [9999-Swaps-1: Test 9999 Client - Mellon Bank, N.A. (OTC ISDA)]");

		// Add Dates
		relationship.setStartDate(DateUtils.toDate("01/01/2015"));
		relationship.setEndDate(DateUtils.toDate("12/31/2015"));
		saveInvestmentAccountRelationship(relationship, null);

		// Still Overlaps
		relationship2.setStartDate(DateUtils.toDate("12/31/2015"));
		saveInvestmentAccountRelationship(relationship2, "Cannot have more than one overlapping relationship to related account for selected account types and purpose. Found: [999900: Test 9999 Client - Parametric Clifton] - [9999-Swaps-1: Test 9999 Client - Mellon Bank, N.A. (OTC ISDA)]");

		// No Overlap
		relationship2.setStartDate(DateUtils.toDate("01/01/2016"));
		saveInvestmentAccountRelationship(relationship, null);
	}


	private InvestmentAccountRelationship populateInvestmentAccountRelationship(int mainAccountId, int relatedAccountId, String purposeName) {
		InvestmentAccountRelationship relationship = new InvestmentAccountRelationship();
		relationship.setReferenceOne(this.investmentAccountService.getInvestmentAccount(mainAccountId));
		relationship.setReferenceTwo(this.investmentAccountService.getInvestmentAccount(relatedAccountId));
		relationship.setPurpose(this.investmentAccountRelationshipService.getInvestmentAccountRelationshipPurposeByName(purposeName));
		return relationship;
	}


	private void deleteInvestmentAccountRelationship(InvestmentAccountRelationship relationship, String expectedErrorMessage) {
		saveInvestmentAccountRelationshipImpl(relationship, true, expectedErrorMessage);
	}


	private void saveInvestmentAccountRelationship(InvestmentAccountRelationship relationship, String expectedErrorMessage) {
		saveInvestmentAccountRelationshipImpl(relationship, false, expectedErrorMessage);
	}


	private void saveInvestmentAccountRelationshipImpl(InvestmentAccountRelationship relationship, boolean delete, String expectedErrorMessage) {
		boolean error = false;
		try {
			if (delete) {
				this.investmentAccountRelationshipService.deleteInvestmentAccountRelationship(relationship.getId());
			}
			else {
				this.investmentAccountRelationshipService.saveInvestmentAccountRelationship(relationship);
			}
		}
		catch (Exception e) {
			error = true;
			if (expectedErrorMessage == null) {
				Assertions.fail("Did not expect error on save of relationship, but did.  Message: " + ExceptionUtils.getOriginalMessage(e));
			}
			Assertions.assertEquals(expectedErrorMessage, e.getMessage());
		}
		if (!error && !StringUtils.isEmpty(expectedErrorMessage)) {
			Assertions.fail("Expected error on save with message " + expectedErrorMessage + ", but no error encountered");
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////            Test Account Relationship Retrievals            //////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAccountConfigRollup() {
		// ID 478 = Main Wellesly Clifton Account
		InvestmentAccountRelationshipConfig config = this.investmentAccountRelationshipService.getInvestmentAccountRelationshipConfig(478);

		Collection<Integer> cliftonAccountIds = config.getMainAccountIds();
		Assertions.assertEquals(4, CollectionUtils.getSize(cliftonAccountIds));
		Assertions.assertTrue(cliftonAccountIds.contains(478));
		Assertions.assertTrue(cliftonAccountIds.contains(479));
		Assertions.assertTrue(cliftonAccountIds.contains(480));
		Assertions.assertTrue(cliftonAccountIds.contains(481));

		// These methods use the config, but service method allows passing String purpose
		Assertions.assertEquals(3, CollectionUtils.getSize(this.investmentAccountRelationshipService.getInvestmentAccountRelationshipListForPurpose(478, "Clifton Sub-Account", new Date(), true)));
		Assertions.assertEquals(3, CollectionUtils.getSize(this.investmentAccountRelationshipService.getInvestmentAccountRelationshipListForPurpose(478, "Clifton Sub-Account", new Date(), false)));

		// Exclude SubAccount Relationships - There is no direct Trading: Futures relationship from/to 478
		Assertions.assertEquals(0, CollectionUtils.getSize(this.investmentAccountRelationshipService.getInvestmentAccountRelationshipListForPurpose(478, "Trading: Futures", new Date(), false)));

		// Include SubAccount Relationships - There are 3 nested Trading: Futures relationship from 478 through the sub-accounts
		List<InvestmentAccountRelationship> relList = this.investmentAccountRelationshipService.getInvestmentAccountRelationshipListForPurpose(478, "Trading: Futures", new Date(), true);
		Assertions.assertEquals(3, CollectionUtils.getSize(relList));

		for (InvestmentAccountRelationship rel : relList) {
			InvestmentAccountRelationshipAssetClassConfig acConfig = config.getConfig(rel.getReferenceOne().getId());
			Assertions.assertFalse(CollectionUtils.isEmpty(acConfig.getAccountAssetClassIdList()));
		}
	}


	@Test
	public void testAccountConfigRollup_SubAccountsMappedToMultipleAssetClasses() {
		// ID 2785 PBR 900100 - Main Account:
		// ID 2728 PBR 900110 Sub Account for: DE, GE, and IE
		// ID 2729 PBR 900120 Sub Account for DC, PM
		// = 5 Sub Account Relationships
		InvestmentAccountRelationshipConfig config = this.investmentAccountRelationshipService.getInvestmentAccountRelationshipConfig(2785);

		Collection<Integer> cliftonAccountIds = config.getMainAccountIds();
		Assertions.assertEquals(3, CollectionUtils.getSize(cliftonAccountIds));
		Assertions.assertTrue(cliftonAccountIds.contains(2785));
		Assertions.assertTrue(cliftonAccountIds.contains(2728));
		Assertions.assertTrue(cliftonAccountIds.contains(2729));

		// These methods use the config, but service method allows passing String purpose
		Assertions.assertEquals(5, CollectionUtils.getSize(this.investmentAccountRelationshipService.getInvestmentAccountRelationshipListForPurpose(2785, "Clifton Sub-Account", new Date(), true)));
		Assertions.assertEquals(5, CollectionUtils.getSize(this.investmentAccountRelationshipService.getInvestmentAccountRelationshipListForPurpose(2785, "Clifton Sub-Account", new Date(), false)));

		// Validate Asset Class Allocation
		InvestmentAccountRelationshipAssetClassConfig main_acConfig = config.getConfig(2785);
		Assertions.assertTrue(main_acConfig.isApplyToAll(), "Main Account Should Apply to All Asset Classes.");

		InvestmentAccountRelationshipAssetClassConfig sub_110_acConfig = config.getConfig(2728);
		Assertions.assertEquals(3, CollectionUtils.getSize(sub_110_acConfig.getAccountAssetClassIdList()), "Sub-Account 900110 should apply to 3 asset classes");

		InvestmentAccountRelationshipAssetClassConfig sub_120_acConfig = config.getConfig(2729);
		Assertions.assertEquals(2, CollectionUtils.getSize(sub_120_acConfig.getAccountAssetClassIdList()), "Sub-Account 900120 should apply to 2 asset classes");
	}


	@Test
	public void testIsAccountRelationshipValid() {
		// Main Account ID = 75 for Central Labor Client Account
		// Related Account ID 615 Valid from 6/30/05 to present
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(75, 615, new Date()));
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(75, 615, DateUtils.toDate("06/30/2005")));
		Assertions.assertFalse(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(75, 615, DateUtils.toDate("06/30/2004")));

		// Related Account ID 1376 Valid always
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(75, 1376, new Date()));
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(75, 1376, DateUtils.toDate("06/30/2005")));
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(75, 1376, DateUtils.toDate("06/30/2004")));

		// Related Account ID 1553 valid from 6/30/05 - 09/30/08
		Assertions.assertFalse(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(75, 1553, new Date()));
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(75, 1553, DateUtils.toDate("06/30/2005")));
		Assertions.assertFalse(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(75, 1553, DateUtils.toDate("06/30/2004")));
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(75, 1553, DateUtils.toDate("09/30/2008")));
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(75, 1553, DateUtils.toDate("11/26/2006")));
	}


	@Test
	public void testInvestmentAccountRelationshipConfigGetRelatedAccountList() {
		// Main Account ID = 75 for Central Labor Client Account
		// Related Account ID 615 Valid from 6/30/05 to present for purpose Trading: Bonds & valid from 10/1/08 for purpose Trading: Futures
		// Related Account ID 1376 Valid always for purpose Custodian
		// Related Account ID 1553 valid from 6/30/05 - 9/30/08 for purpose Trading: Futures

		// Check Both Ways
		Assertions.assertEquals(2, CollectionUtils.getSize(this.investmentAccountRelationshipService.getInvestmentAccountRelatedListForPurpose(75, null, "Trading: Futures", null)));
		Assertions.assertEquals((Integer) 75, this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(615, "Trading: Futures", null).getId());
		Assertions.assertEquals((Integer) 75, this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(1553, "Trading: Futures", null).getId());

		Assertions.assertEquals(1,
				CollectionUtils.getSize(this.investmentAccountRelationshipService.getInvestmentAccountRelatedListForPurpose(75, null, "Trading: Futures", DateUtils.toDate("07/01/2007"))));
		Assertions.assertNull(this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(615, "Trading: Futures", DateUtils.toDate("07/01/2007")));
		Assertions.assertEquals((Integer) 75, this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(1553, "Trading: Futures", DateUtils.toDate("07/01/2007")).getId());

		Assertions.assertEquals(1, CollectionUtils.getSize(this.investmentAccountRelationshipService.getInvestmentAccountRelatedListForPurpose(75, null, "Trading: Futures", new Date())));
		Assertions.assertNull(this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(1553, "Trading: Futures", DateUtils.toDate("07/01/2011")));
		Assertions.assertEquals((Integer) 75, this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(615, "Trading: Futures", DateUtils.toDate("07/01/2011")).getId());

		Assertions.assertNotNull(this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(75, "Trading: Bonds", new Date()));
		Assertions.assertNull(this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(75, "Trading: Bonds", DateUtils.toDate("05/01/2005")));

		// Account 615 has two relationships to main account 75 - make sure only returns one account
		List<InvestmentAccount> accountList = this.investmentAccountRelationshipService.getInvestmentAccountRelatedListForPurpose(75, null, null, new Date());
		boolean tradingFound = false;
		boolean custodianFound = false;
		for (InvestmentAccount a : accountList) {
			if (a.getId() == 615) {
				Assertions.assertFalse(tradingFound, "Trading Account with id 615 listed twice for main account id 75");
				tradingFound = true;
			}
			else if (a.getId() == 1376) {
				custodianFound = true;
			}
		}
		Assertions.assertTrue(tradingFound && custodianFound, "Expected two accounts returned - 615 for Trading: Futures & Trading: Bonds and 1376 as the custodian");
	}


	@Test
	public void testReverseIsAccountRelationshipValid() {
		// Main Account ID = 75 for Central Labor Client Account
		// Related Account ID 615 Valid from 6/30/05 to present
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(615, 75, new Date()));
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(615, 75, DateUtils.toDate("06/30/2005")));
		Assertions.assertFalse(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(615, 75, DateUtils.toDate("06/30/2004")));

		// Related Account ID 1376 Valid always
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(1376, 75, new Date()));
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(1376, 75, DateUtils.toDate("06/30/2005")));
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(1376, 75, DateUtils.toDate("06/30/2004")));

		// Related Account ID 1553 valid from 6/30/05 - 09/30/08
		Assertions.assertFalse(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(1553, 75, new Date()));
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(1553, 75, DateUtils.toDate("06/30/2005")));
		Assertions.assertFalse(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(1553, 75, DateUtils.toDate("06/30/2004")));
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(1553, 75, DateUtils.toDate("09/30/2008")));
		Assertions.assertTrue(this.investmentAccountRelationshipService.isInvestmentAccountRelationshipValid(1553, 75, DateUtils.toDate("11/26/2006")));
	}


	@Test
	public void testGetInvestmentAccountRelatedForPurposeStrict_AccountWithSubAccounts() {
		// Nestle 445000 has sub-account 445007 - make sure when looking for related account for 445000 it doesn't include 445007 and fail because it found multiple
		// Client Account = 445000, Holding Account = NEST-CIT
		// 1 Custodian is defined on 445000, 1 Custodian is Defined on 445007, No Custodians for NEST-CIT
		// Should return the 1 Custodian defined for 445000
		this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurposeStrict(275, 1722, 1, "Custodian", new Date());
	}
}
