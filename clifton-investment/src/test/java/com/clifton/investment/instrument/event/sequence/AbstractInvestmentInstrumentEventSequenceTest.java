package com.clifton.investment.instrument.event.sequence;


import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.calendar.schedule.CalendarScheduleServiceImpl;
import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.factory.CalendarScheduleTestObjectFactory;
import com.clifton.calendar.schedule.factory.CalendarScheduleTestServiceHolder;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.sequence.date.AbstractDateCalculator;
import org.junit.jupiter.api.BeforeEach;

import java.util.Date;


public abstract class AbstractInvestmentInstrumentEventSequenceTest {

	protected InvestmentInstrumentEventSequenceService investmentInstrumentEventSequenceService;

	private InvestmentEventSequenceRetrieverUtilHandler investmentEventSequenceRetrieverUtilHandler;

	private CalendarScheduleTestServiceHolder scheduleServiceHolder;

	private final Date scheduleStartDate = DateUtils.toDate("08/01/2011");
	private final Date scheduleEndDate = DateUtils.toDate("12/31/2015");


	public CalendarScheduleService getCalendarScheduleService() {
		if (this.scheduleServiceHolder != null) {
			return this.scheduleServiceHolder.getCalendarScheduleService();
		}
		return null;
	}


	public ScheduleApiService getScheduleApiService() {
		if (this.scheduleServiceHolder != null) {
			return this.scheduleServiceHolder.getScheduleApiService();
		}
		return null;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		if (this.scheduleServiceHolder != null) {
			return this.scheduleServiceHolder.getCalendarBusinessDayService();
		}
		return null;
	}


	public CalendarSetupService getCalendarSetupService() {
		if (this.scheduleServiceHolder != null) {
			return this.scheduleServiceHolder.getCalendarSetupService();
		}
		return null;
	}


	@BeforeEach
	public void setup() {
		if (this.scheduleServiceHolder == null) {
			this.scheduleServiceHolder = CalendarScheduleTestObjectFactory.newCalendarServices(getScheduleStartDate(), getScheduleEndDate());
		}

		if (this.scheduleServiceHolder.getCalendarScheduleService() != null) {
			this.investmentInstrumentEventSequenceService = new InvestmentInstrumentEventSequenceServiceImpl();
			((InvestmentInstrumentEventSequenceServiceImpl) this.investmentInstrumentEventSequenceService).setScheduleApiService(getScheduleApiService());
			((InvestmentInstrumentEventSequenceServiceImpl) this.investmentInstrumentEventSequenceService).setCalendarBusinessDayService(((CalendarScheduleServiceImpl) getCalendarScheduleService()).getCalendarBusinessDayService());
		}
	}


	protected <T extends AbstractDateCalculator> T configureDateCalculator(T calculator) {
		calculator.setScheduleApiService(getScheduleApiService());
		calculator.setCalendarBusinessDayService(getCalendarBusinessDayService());
		calculator.setInvestmentEventSequenceRetrieverUtilHandler(getInvestmentEventSequenceRetrieverUtilHandler());
		return calculator;
	}


	protected InvestmentEventSequenceRetrieverUtilHandler getInvestmentEventSequenceRetrieverUtilHandler() {
		if (this.investmentEventSequenceRetrieverUtilHandler == null) {
			InvestmentEventSequenceRetrieverUtilHandlerImpl handler = new InvestmentEventSequenceRetrieverUtilHandlerImpl();
			handler.setCalendarBusinessDayService(getCalendarBusinessDayService());
			handler.setScheduleApiService(getScheduleApiService());
			this.investmentEventSequenceRetrieverUtilHandler = handler;
		}
		return this.investmentEventSequenceRetrieverUtilHandler;
	}


	protected String formatEquityEvent(InvestmentSecurityEvent e) {
		return DateUtils.fromDate(e.getDeclareDate(), DateUtils.DATE_FORMAT_INPUT) + "\t" + DateUtils.fromDate(e.getExDate(), DateUtils.DATE_FORMAT_INPUT) + "\t"
				+ DateUtils.fromDate(e.getRecordDate(), DateUtils.DATE_FORMAT_INPUT) + "\t" + DateUtils.fromDate(e.getEventDate(), DateUtils.DATE_FORMAT_INPUT);
	}


	protected String formatInterestEvent(InvestmentSecurityEvent e) {
		return formatInterestEvent(e, false);
	}


	private String formatInterestEvent(InvestmentSecurityEvent e, boolean excludeAdditionalDate) {
		return DateUtils.fromDate(e.getAccrualStartDate(), DateUtils.DATE_FORMAT_INPUT) + "\t" + DateUtils.fromDate(e.getAccrualEndDate(), DateUtils.DATE_FORMAT_INPUT) + "\t"
				+ DateUtils.fromDate(e.getExDate(), DateUtils.DATE_FORMAT_INPUT) + "\t" + DateUtils.fromDate(e.getPaymentDate(), DateUtils.DATE_FORMAT_INPUT)
				+ (excludeAdditionalDate ? "" : "\t" + DateUtils.fromDate(e.getAdditionalDate(), DateUtils.DATE_FORMAT_INPUT));
	}


	public Date getScheduleStartDate() {
		return this.scheduleStartDate;
	}


	public Date getScheduleEndDate() {
		return this.scheduleEndDate;
	}
}
