SELECT 'CalendarSchedule' AS entityTableName, CalendarScheduleID AS entityID
FROM CalendarSchedule
WHERE CalendarID IN (2, 11, 30, 44, 156)
UNION
SELECT 'Calendar' AS entityTableName, CalendarID AS entityID
FROM Calendar
WHERE CalendarID IN (4)
UNION
SELECT 'SystemBeanPropertyType' AS entityTableName, SystemBeanPropertyTypeID AS entityID
FROM SystemBeanPropertyType
WHERE SystemBeanTypeID IN (603, 588, 591)
UNION
SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityID
FROM InvestmentSecurity
WHERE InvestmentInstrumentID IN (17642, 926)
UNION
SELECT 'SystemBeanType' AS entityTableName, SystemBeanTypeID AS entityID
FROM SystemBeanType
WHERE SystemBeanTypeID IN (590)
UNION
SELECT 'SystemColumn' AS entityTableName, SystemColumnID AS entityID
FROM SystemColumn
WHERE SystemColumnGroupID IN (3)
UNION
SELECT 'InvestmentSecurityEventType' AS entityTableName, InvestmentSecurityEventTypeID AS entityID
FROM InvestmentSecurityEventType
WHERE InvestmentSecurityEventTypeID IN (6)
