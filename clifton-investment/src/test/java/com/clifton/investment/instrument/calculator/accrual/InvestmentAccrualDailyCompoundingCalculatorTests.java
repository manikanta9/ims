package com.clifton.investment.instrument.calculator.accrual;

import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * @author vgomelsky
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentAccrualDailyCompoundingCalculatorTests {

	private static final int FIXED_LEG_EVENT_ID = 100;
	private static final int FLOATING_LEG_EVENT_ID = 200;


	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private InvestmentAccrualDailyCompoundingCalculator investmentAccrualDailyCompoundingCalculator;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;


	private SystemColumnValueHandler getSystemColumnServiceMock(InvestmentSecurity security, String frequency, String dayCountConvention) {
		SystemColumnValueHandler systemColumnValueHandler = Mockito.mock(SystemColumnValueHandler.class);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY, false))
				.thenReturn(frequency);
		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY2, false))
				.thenReturn(frequency);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION, false))
				.thenReturn(dayCountConvention);
		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION2, false))
				.thenReturn(dayCountConvention);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CALENDAR, false))
				.thenReturn(this.calendarSetupService.getCalendarByName("Test Calendar").getId());

		return systemColumnValueHandler;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalculateAccruedInterest_FloatingLeg_WithDetails() {
		InvestmentSecurityEvent floatingLeg = this.investmentSecurityEventService.getInvestmentSecurityEvent(FLOATING_LEG_EVENT_ID);
		this.investmentAccrualDailyCompoundingCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(floatingLeg.getSecurity(), "BULLET", "BUS/252"));

		Assertions.assertEquals(BigDecimal.valueOf(-16580.08), this.investmentAccrualDailyCompoundingCalculator.calculateAccruedInterest(floatingLeg, new BigDecimal("64982814.7"), BigDecimal.ONE, DateUtils.toDate("02/23/2018")));
		Assertions.assertEquals(BigDecimal.valueOf(-62345.22), this.investmentAccrualDailyCompoundingCalculator.calculateAccruedInterest(floatingLeg, new BigDecimal("64982814.7"), BigDecimal.ONE, DateUtils.toDate("02/28/2018")));
		Assertions.assertEquals(BigDecimal.valueOf(-87537.11), this.investmentAccrualDailyCompoundingCalculator.calculateAccruedInterest(floatingLeg, new BigDecimal("64982814.7"), BigDecimal.ONE, DateUtils.toDate("03/02/2018")));
		Assertions.assertEquals(BigDecimal.valueOf(-87537.11), this.investmentAccrualDailyCompoundingCalculator.calculateAccruedInterest(floatingLeg, new BigDecimal("64982814.7"), BigDecimal.ONE, DateUtils.toDate("03/03/2018"))); // weekend
		Assertions.assertEquals(BigDecimal.valueOf(-147748.74), this.investmentAccrualDailyCompoundingCalculator.calculateAccruedInterest(floatingLeg, new BigDecimal("64982814.7"), BigDecimal.ONE, DateUtils.toDate("03/08/2018")));
		Assertions.assertEquals(BigDecimal.valueOf(-147748.74), this.investmentAccrualDailyCompoundingCalculator.calculateAccruedInterest(floatingLeg, new BigDecimal("64982814.7"), BigDecimal.ONE, DateUtils.toDate("03/09/2018"))); // holiday
	}


	@Test
	public void testCalculateAccruedInterest_FixedLeg_NoDetails() {
		InvestmentSecurityEvent fixedLeg = this.investmentSecurityEventService.getInvestmentSecurityEvent(FIXED_LEG_EVENT_ID);
		this.investmentAccrualDailyCompoundingCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(fixedLeg.getSecurity(), "BULLET", "BUS/252"));

		Assertions.assertEquals(BigDecimal.valueOf(24218.37), this.investmentAccrualDailyCompoundingCalculator.calculateAccruedInterest(fixedLeg, new BigDecimal("64982814.7"), BigDecimal.ONE, DateUtils.toDate("02/23/2018")));
		Assertions.assertEquals(BigDecimal.valueOf(96927.65), this.investmentAccrualDailyCompoundingCalculator.calculateAccruedInterest(fixedLeg, new BigDecimal("64982814.7"), BigDecimal.ONE, DateUtils.toDate("02/28/2018")));
		Assertions.assertEquals(BigDecimal.valueOf(145445.68), this.investmentAccrualDailyCompoundingCalculator.calculateAccruedInterest(fixedLeg, new BigDecimal("64982814.7"), BigDecimal.ONE, DateUtils.toDate("03/02/2018")));
		Assertions.assertEquals(BigDecimal.valueOf(145445.68), this.investmentAccrualDailyCompoundingCalculator.calculateAccruedInterest(fixedLeg, new BigDecimal("64982814.7"), BigDecimal.ONE, DateUtils.toDate("03/03/2018"))); // weekend
		Assertions.assertEquals(BigDecimal.valueOf(242590.28), this.investmentAccrualDailyCompoundingCalculator.calculateAccruedInterest(fixedLeg, new BigDecimal("64982814.7"), BigDecimal.ONE, DateUtils.toDate("03/08/2018")));
		Assertions.assertEquals(BigDecimal.valueOf(242590.28), this.investmentAccrualDailyCompoundingCalculator.calculateAccruedInterest(fixedLeg, new BigDecimal("64982814.7"), BigDecimal.ONE, DateUtils.toDate("03/09/2018"))); // holiday
	}
}
