package com.clifton.investment.instrument.event.sequence;

import com.clifton.calendar.CalendarTestObjectFactory;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.calculator.CouponFrequencies;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.sequence.date.EquityLegPaymentDateCalculator;
import com.clifton.investment.instrument.event.sequence.date.InterestLegPaymentDateCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;


public class InvestmentInstrumentEventSequenceBulletSwapTests extends AbstractInvestmentInstrumentEventSequenceTest {

	@Test
	public void testSequence_36422677() {
		//Calendar calendar = CALENDAR_SETUP_SERVICE.getCalendar(CALENDAR);
		//Calendar uk_calendar = CALENDAR_SETUP_SERVICE.getCalendar(CALENDAR_UK);
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_COMBINED);

		EquityLegPaymentDateCalculator dateCalculator = configureDateCalculator(new EquityLegPaymentDateCalculator());

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(calendar);
		sequence.setSettlementCalendar(calendar);

		sequence.setSequenceStartDate(DateUtils.toDate("04/03/2014"));
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Bullet"));
		sequence.setSettlementCycle(2);
		sequence.setDateCalculator(dateCalculator);
		sequence.setResetAccrualConvention(EventSequenceResetAccrualConventions.INTEREST_ACCRUES_ON_VALUATION_DATE);
		sequence.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT));
		sequence.setFixingCycle(0);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(23934, "36422677", "Swaps"));
		sequence.getSecurity().setStartDate(DateUtils.toDate("04/03/2014"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("05/05/2014"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("05/07/2014"));

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(1, events.size());

		Assertions.assertEquals("04/03/2014	05/06/2014	05/05/2014	05/07/2014", formatEquityEvent(events.get(0)));

		sequence.setDateCalculator(configureDateCalculator(new InterestLegPaymentDateCalculator()));

		events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity().getLastDeliveryDate());
		Assertions.assertEquals(1, events.size());

		Assertions.assertEquals("04/03/2014	05/05/2014	05/06/2014	05/07/2014	04/03/2014", formatInterestEvent(events.get(0)));
	}


	@Test
	public void testSequence_Q0S6V() {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);
		//Calendar uk_calendar = CALENDAR_SETUP_SERVICE.getCalendar(CALENDAR_UK);
		//Calendar combined = CALENDAR_SETUP_SERVICE.getCalendar(CALENDAR_COMBINED);

		EquityLegPaymentDateCalculator dateCalculator = configureDateCalculator(new EquityLegPaymentDateCalculator());

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(calendar);
		sequence.setSettlementCalendar(calendar);

		sequence.setSequenceStartDate(DateUtils.toDate("07/18/2013"));
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Bullet"));
		sequence.setSettlementCycle(3);
		sequence.setDateCalculator(dateCalculator);
		//sequence.setResetAccrualConvention(EventSequenceResetAccrualConventions.INTEREST_ACCRUES_ON_VALUATION_DATE);
		sequence.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT));
		sequence.setFixingCycle(1);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(20972, "Q0S6V", "Swaps"));
		sequence.getSecurity().setStartDate(DateUtils.toDate("07/18/2013"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("08/19/2013"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("08/22/2013"));

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(1, events.size());

		Assertions.assertEquals("07/18/2013	08/20/2013	08/19/2013	08/22/2013", formatEquityEvent(events.get(0)));


		sequence.setDateCalculator(configureDateCalculator(new InterestLegPaymentDateCalculator()));

		events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity().getLastDeliveryDate());
		Assertions.assertEquals(1, events.size());

		Assertions.assertEquals("07/18/2013	08/22/2013	08/20/2013	08/22/2013	07/19/2013", formatInterestEvent(events.get(0)));
	}


	@Test
	public void testInterestLegPaymentSequence_35677751() {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);

		InterestLegPaymentDateCalculator dateCalculator = configureDateCalculator(new InterestLegPaymentDateCalculator());

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(calendar);
		sequence.setSettlementCalendar(calendar);

		sequence.setResetAccrualConvention(EventSequenceResetAccrualConventions.INTEREST_ACCRUES_ON_VALUATION_DATE);

		sequence.setSequenceStartDate(DateUtils.toDate("01/14/2014"));
		sequence.setFirstSequenceEventExist(true);
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Bullet"));
		sequence.setSettlementCycle(2);
		sequence.setDateCalculator(dateCalculator);
		sequence.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT));
		sequence.setFixingCycle(1);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(22951, "35677751", "Swaps"));
		sequence.getSecurity().setStartDate(DateUtils.toDate("01/14/2014"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("02/11/2014"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("02/13/2014"));

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(1, events.size());

		Assertions.assertEquals("01/14/2014	02/11/2014	02/12/2014	02/13/2014	01/15/2014", formatInterestEvent(events.get(0)));
	}
}
