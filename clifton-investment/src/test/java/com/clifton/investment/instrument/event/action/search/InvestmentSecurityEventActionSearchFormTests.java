package com.clifton.investment.instrument.event.action.search;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventAction;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventActionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * A set of tests used to verify the correct functionality of the {@link InvestmentSecurityEventActionSearchForm}
 *
 * @author DavidI
 */
@ContextConfiguration
public class InvestmentSecurityEventActionSearchFormTests extends BaseInMemoryDatabaseTests {

	@Resource
	InvestmentSecurityEventActionService investmentSecurityEventActionService;

	private static final Set<Integer> EMPTY_SET = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSearchPattern_pattern_security_name() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(2020, 2021);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setSearchPattern("GUD Holdings");
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchPattern_pattern_action_type_name() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(2016, 1987, 1988, 2020, 2026, 1931, 1901, 1998, 1903, 1905, 2002, 2006, 1975, 2007, 1912, 2008, 1978, 1979, 1980, 1981);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setSearchPattern("Data Value Adjustor");
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchPattern_pattern_securityEvent_event_type_name() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(1901, 1903, 1905, 1912, 1980, 1998, 2002, 2006, 2016, 2020);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setSearchPattern("Stock Spinoff");
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchPattern_pattern_securityEvent_status_name() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(1906, 1932);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setSearchPattern("Conditionally Approved");
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchPattern_pattern_no_match() {
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setSearchPattern("upsilon 9");
		testSearchForm(EMPTY_SET, searchForm);
	}


	@Test
	public void testSearchForm_security_event_id() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(1998);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setSecurityEventId(1978815);
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchForm_security_event_id_no_match() {
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setSecurityEventId(21998);
		testSearchForm(EMPTY_SET, searchForm);
	}



	@Test
	public void testSearchForm_security_id() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(1978, 1979, 1980);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setInvestmentSecurityId(129770);
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchForm_security_id_no_match() {
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setInvestmentSecurityId(9887732);
		testSearchForm(EMPTY_SET, searchForm);
	}


	@Test
	public void testSearchForm_security_name() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(1933);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setInvestmentSecurityName("Acceleron Pharma Inc");
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchForm_security_name_no_match() {
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setInvestmentSecurityName("NoExist");
		testSearchForm(EMPTY_SET, searchForm);
	}


	@Test
	public void testSearchForm_action_type_id() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(1901, 1903, 1905, 2002, 1912, 1980, 1998, 2020, 2006, 2016);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setActionTypeId((short)2);
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchForm_action_type_id_no_match() {
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setActionTypeId((short)23);
		testSearchForm(EMPTY_SET, searchForm);
	}


	@Test
	public void testSearchForm_action_type_name() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(1901, 1903, 1905, 2002, 1912, 1980, 1998, 2020, 2006, 2016);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setActionTypeName("Market Data Value Adjustor for Stock Spinoffs");
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchForm_action_type_name_no_match() {
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setActionTypeName("non existent action type");
		testSearchForm(EMPTY_SET, searchForm);
	}


	@Test
	public void testSearchForm_processing_order() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(1907, 1976);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setProcessingOrder((short)1);
		searchForm.setEventDate(DateUtils.toDate("11/10/2021"));
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchForm_processing_order_no_match() {
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setProcessingOrder((short)2);
		searchForm.setEventDate(DateUtils.toDate("11/10/2021"));
		testSearchForm(EMPTY_SET, searchForm);
	}


	@Test
	public void testSearchForm_process_before_event_journal_false() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(1907, 1976);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setProcessBeforeEventJournal(false);
		searchForm.setEventDate(DateUtils.toDate("11/10/2021"));
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchForm_process_before_event_journal_true() {
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setProcessBeforeEventJournal(true);
		searchForm.setEventDate(DateUtils.toDate("11/10/2021"));
		testSearchForm(EMPTY_SET, searchForm);
	}


	@Test
	public void testSearchForm_event_date() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(1907, 1976);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setEventDate(DateUtils.toDate("11/10/2021"));
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchForm_event_date_no_match() {
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setEventDate(DateUtils.toDate("11/10/1990"));
		testSearchForm(EMPTY_SET, searchForm);
	}


	@Test
	public void testSearchForm_processed_date() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(1907, 1909);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setProcessedDate(DateUtils.toDate("11/10/2021"));
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchForm_processed_date_no_match() {
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setProcessedDate(DateUtils.toDate("11/10/1990"));
		testSearchForm(EMPTY_SET, searchForm);
	}


	@Test
	public void testSearchForm_event_type_id() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(1981, 2007, 2026);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setEventTypeId((short)14);
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchForm_event_type_no_match() {
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setEventTypeId((short)25);
		testSearchForm(EMPTY_SET, searchForm);
	}


	@Test
	public void testSearchForm_event_status_type_id() {
		Set<Integer> expectedEventActionIdSet = CollectionUtils.createHashSet(1900, 1909, 1914, 1976, 1981, 1986, 1995, 2002,2003, 2006, 2020, 2021, 2022);
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setEventStatusId((short)8);
		testSearchForm(expectedEventActionIdSet, searchForm);
	}


	@Test
	public void testSearchForm_event_status_type_id_no_match() {
		InvestmentSecurityEventActionSearchForm searchForm = new InvestmentSecurityEventActionSearchForm();
		searchForm.setEventStatusId((short)6);
		testSearchForm(EMPTY_SET, searchForm);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * A local test facility to compare a set of InvestmentSecurityEventActionIDs returned from a DB Query to an expected set of InvestmentSecurityEventActionIDs.
	 * If the sets are equivalent, the test passes, if not it fails.
	 */
	private void testSearchForm(Set<Integer> expectedSet, InvestmentSecurityEventActionSearchForm searchForm) {
		List<InvestmentSecurityEventAction> actionList = this.investmentSecurityEventActionService.getInvestmentSecurityEventActionList(searchForm);
		Set<Integer> resultSet = CollectionUtils.getStream(actionList).map(BaseSimpleEntity::getId)
				.collect(Collectors.toSet());

		Assertions.assertEquals(expectedSet, resultSet, "Mismatch between expected InvestmentSecurityEventActionId's and returned ID set.");
	}

}
