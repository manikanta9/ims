package com.clifton.investment.instrument.event.sequence;

import com.clifton.calendar.CalendarTestObjectFactory;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.calculator.CouponFrequencies;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.sequence.date.EquityLegPaymentDateCalculator;
import com.clifton.investment.instrument.event.sequence.date.InterestLegPaymentDateCalculator;
import com.clifton.investment.instrument.event.sequence.date.InvestmentInstrumentEventSequenceDateCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;


/**
 * @author mwacker
 */
public class InvestmentInstrumentEventSequence_990288219_Tests extends AbstractInvestmentInstrumentEventSequenceTest {

	@Override
	public Date getScheduleStartDate() {
		return DateUtils.toDate("01/01/2015");
	}


	@Override
	public Date getScheduleEndDate() {
		return DateUtils.toDate("12/31/2015");
	}


	@Test
	public void testEquityLegPaymentSequence_990288219() {
		EquityLegPaymentDateCalculator dateCalculator = configureDateCalculator(new EquityLegPaymentDateCalculator());

		EventSequenceDefinition sequence = setup_990288219(dateCalculator, InvestmentSecurityEventType.EQUITY_LEG_PAYMENT);

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(6, events.size());

		// Valuation Date, ExDate, Next ValuationDate, Payment Date
		Assertions.assertEquals("02/03/2015	02/28/2015	02/27/2015	03/03/2015", formatEquityEvent(events.get(0)));
		Assertions.assertEquals("02/27/2015	04/01/2015	03/31/2015	04/02/2015", formatEquityEvent(events.get(1)));
		Assertions.assertEquals("03/31/2015	05/01/2015	04/30/2015	05/05/2015", formatEquityEvent(events.get(2)));
		Assertions.assertEquals("04/30/2015	05/30/2015	05/29/2015	06/02/2015", formatEquityEvent(events.get(3)));
		Assertions.assertEquals("05/29/2015	07/01/2015	06/30/2015	07/02/2015", formatEquityEvent(events.get(4)));
		Assertions.assertEquals("06/30/2015	08/01/2015	07/31/2015	08/04/2015", formatEquityEvent(events.get(5)));
	}


	@Test
	public void testInterestLegPaymentSequence_990288219() {
		InterestLegPaymentDateCalculator dateCalculator = configureDateCalculator(new InterestLegPaymentDateCalculator());

		EventSequenceDefinition sequence = setup_990288219(dateCalculator, InvestmentSecurityEventType.INTEREST_LEG_PAYMENT);

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(6, events.size());

		Assertions.assertEquals("02/05/2015	03/03/2015	03/01/2015	03/03/2015	02/05/2015", formatInterestEvent(events.get(0)));
		Assertions.assertEquals("03/03/2015	04/02/2015	04/01/2015	04/02/2015	03/03/2015", formatInterestEvent(events.get(1)));
		Assertions.assertEquals("04/02/2015	05/05/2015	05/01/2015	05/05/2015	04/02/2015", formatInterestEvent(events.get(2)));
		Assertions.assertEquals("05/05/2015	06/02/2015	06/01/2015	06/02/2015	05/05/2015", formatInterestEvent(events.get(3)));
		Assertions.assertEquals("06/02/2015	07/02/2015	07/01/2015	07/02/2015	06/02/2015", formatInterestEvent(events.get(4)));
		Assertions.assertEquals("07/02/2015	08/04/2015	08/01/2015	08/04/2015	07/02/2015", formatInterestEvent(events.get(5)));
	}


	private EventSequenceDefinition setup_990288219(InvestmentInstrumentEventSequenceDateCalculator dateCalculator, String typeName) {
		Calendar combined = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_COMBINED);

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(combined);
		sequence.setFixingCalendar(combined);
		sequence.setSettlementCalendar(combined);

		sequence.setSequenceStartDate(DateUtils.toDate("01/30/2015"));
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Monthly"));
		sequence.setSettlementCycle(2);
		sequence.setDateCalculator(dateCalculator);
		InvestmentSecurityEventType type = new InvestmentSecurityEventType();
		type.setName(typeName);
		sequence.setType(type);
		sequence.setFixingCycle(0);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(28821, "990288219", "Swaps"));
		sequence.getSecurity().setStartDate(DateUtils.toDate("02/03/2015"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("07/31/2015"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("08/04/2015"));
		sequence.setValuationConvention(EventSequenceRecurrenceConventions.MONTH_END_RECURRENCE);

		return sequence;
	}
}
