package com.clifton.investment.instrument.calculator.accrual;


import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentAccrualOISCompoundCalculatorTests {

	@Resource
	private InvestmentAccrualOISCompoundCalculator investmentAccrualOISCompoundCalculator;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;


	private SystemColumnValueHandler getSystemColumnServiceMock(InvestmentSecurity security, double spread, String frequency, String dayCountConvention) {
		SystemColumnValueHandler systemColumnValueHandler = Mockito.mock(SystemColumnValueHandler.class);
		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_SPREAD, false)).thenReturn(
				BigDecimal.valueOf(spread));
		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY, false))
				.thenReturn(frequency);
		Mockito.when(
						systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION, false))
				.thenReturn(dayCountConvention);

		return systemColumnValueHandler;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOISCalculatorAccrueUpToEndOfPeriod() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.investmentAccrualOISCompoundCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 5.0, "MONTHLY", "Actual/360"));

		BigDecimal result = this.investmentAccrualOISCompoundCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("52765069.0"), BigDecimal.ONE, DateUtils.toDate("10/31/2012"));

		// Actual results from spreadsheet = -9131.71.  Intermediate rounding did not have an effect on this difference.  Leaving it at -9131.72 for now.
		Assertions.assertEquals(BigDecimal.valueOf(-9131.72), result);
	}


	@Test
	public void testOISCalculatorAccrueUpToOneDayAfterPeriod() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.investmentAccrualOISCompoundCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 5.0, "MONTHLY", "Actual/360"));

		BigDecimal result = this.investmentAccrualOISCompoundCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("52765069.0"), BigDecimal.ONE, DateUtils.toDate("11/01/2012"));

		// Actual results from spreadsheet = -9468.86.  Intermediate rounding did not have an effect on this difference.  Leaving it at -9468.87 for now.
		Assertions.assertEquals(BigDecimal.valueOf(-9468.87), result);
	}


	@Test
	public void testOISCalculatorFailAccrualDateAfterPeriod() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
			this.investmentAccrualOISCompoundCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 5.0, "MONTHLY", "Actual/360"));

			this.investmentAccrualOISCompoundCalculator.calculateAccruedInterest(paymentEvent, BigDecimal.valueOf(70506339.90), BigDecimal.ONE, DateUtils.toDate("11/02/2012"));
		}, "\"accrueUpToDate\" [11/02/2012] is invalid for payment event with accrual start date of [10/01/2012], and accrual end date of [10/31/2012] (accruals are allowed to one day past the period end date).");
	}


	@Test
	public void testOISCalculatorFailAccrualDateBeforePeriod() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
			this.investmentAccrualOISCompoundCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 5.0, "MONTHLY", "Actual/360"));

			this.investmentAccrualOISCompoundCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("70506339.90"), BigDecimal.ONE, DateUtils.toDate("09/30/2012"));
		}, "\"accrueUpToDate\" [09/30/2012] is invalid for payment event with accrual start date of [10/01/2012], and accrual end date of [10/31/2012] (accruals are allowed to one day past the period end date).");
	}


	@Test
	public void testOISCalculatorAccrueForZeroDays() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.investmentAccrualOISCompoundCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 5.0, "MONTHLY", "Actual/360"));

		BigDecimal result = this.investmentAccrualOISCompoundCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("70506339.90"), BigDecimal.ONE, DateUtils.toDate("10/01/2012"));
		Assertions.assertEquals(new BigDecimal("0.00"), result);
	}


	@Test
	public void testOISCalculatorAccrueForOneDay() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.investmentAccrualOISCompoundCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 5.0, "MONTHLY", "Actual/360"));

		BigDecimal result = this.investmentAccrualOISCompoundCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal(52765069), BigDecimal.ONE, DateUtils.toDate("10/02/2012"));

		Assertions.assertEquals(BigDecimal.valueOf(-293.13), result);
	}


	@Test
	public void testOISCalculatorAccrualDateBeforeSecondDetailEffectiveDate() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.investmentAccrualOISCompoundCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 5.0, "MONTHLY", "Actual/360"));

		BigDecimal result = this.investmentAccrualOISCompoundCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal(52765069), BigDecimal.ONE, DateUtils.toDate("10/03/2012"));

		Assertions.assertEquals(BigDecimal.valueOf(-600.94), result);
	}


	@Test
	public void testOISCalculatorAccrualDateBeforeFinalDetailEffectiveDate() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.investmentAccrualOISCompoundCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 5.0, "MONTHLY", "Actual/360"));

		BigDecimal result = this.investmentAccrualOISCompoundCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal(52765069), BigDecimal.ONE, DateUtils.toDate("10/30/2012"));

		Assertions.assertEquals(BigDecimal.valueOf(-8809.24), result);
	}


	@Test
	public void testOISCalculatorAccrueToFirstDetailEffectiveDate() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.investmentAccrualOISCompoundCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 5.0, "MONTHLY", "Actual/360"));

		BigDecimal result = this.investmentAccrualOISCompoundCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal(52765069), BigDecimal.ONE, DateUtils.toDate("10/02/2012"));

		Assertions.assertEquals(BigDecimal.valueOf(-293.13), result);
	}


	@Test
	public void testOISCalculatorAccrueToSecondDetailEffectiveDate() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.investmentAccrualOISCompoundCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 5.0, "MONTHLY", "Actual/360"));

		BigDecimal result = this.investmentAccrualOISCompoundCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal(52765069), BigDecimal.ONE, DateUtils.toDate("10/04/2012"));

		Assertions.assertEquals(BigDecimal.valueOf(-908.73), result);
	}


	@Test
	public void testOISCalculatorAccrualDateBetweenLastDetailEffectiveDateAndPeriodEnd() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);

		// delete final detail
		List<InvestmentSecurityEventDetail> details = this.investmentSecurityEventService.getInvestmentSecurityEventDetailListByEvent(paymentEvent.getId());

		InvestmentSecurityEventDetail lastDetail = details.get(details.size() - 1);
		InvestmentSecurityEventDetail nextDetail = details.get(details.size() - 2);

		this.investmentSecurityEventService.deleteInvestmentSecurityEventDetail(lastDetail.getId());
		this.investmentSecurityEventService.deleteInvestmentSecurityEventDetail(nextDetail.getId());

		this.investmentAccrualOISCompoundCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 5.0, "MONTHLY", "Actual/360"));

		BigDecimal result = this.investmentAccrualOISCompoundCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal(52765069), BigDecimal.ONE, DateUtils.toDate("10/29/2012"));

		Assertions.assertEquals(BigDecimal.valueOf(-8486.76), result);
	}
}
