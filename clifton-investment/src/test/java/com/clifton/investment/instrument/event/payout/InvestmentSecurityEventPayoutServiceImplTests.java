package com.clifton.investment.instrument.event.payout;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


/**
 * @author NickK
 */
@SuppressWarnings("ThrowableNotThrown")
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentSecurityEventPayoutServiceImplTests {

	private static final int SCRIP_DIVIDEND_SECURITY_EVENT_ID = 3000;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	@Resource
	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;


	///////////////////////////////////////////////////////////////////////////
	///////////       Security Event Payout Type Tests          ///////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetInvestmentSecurityEventPayoutType() {
		Assertions.assertNotNull(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutType((short) 1), "Missing Security Event Payout Type");
		Assertions.assertNotNull(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Currency"), "Missing Security Event Payout Type");
	}

	///////////////////////////////////////////////////////////////////////////
	///////////          Security Event Payout Tests            ///////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetInvestmentSecurityEventPayout() {
		Assertions.assertNotNull(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(1), "Missing Security Event Payout");
	}


	@Test
	public void testGetInvestmentSecurityEventPayoutList() {
		InvestmentSecurityEventPayoutSearchForm payoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm.setSecurityEventId(SCRIP_DIVIDEND_SECURITY_EVENT_ID);
		List<InvestmentSecurityEventPayout> payoutList = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutList(payoutSearchForm);
		Assertions.assertEquals(3, payoutList.size());
	}


	@Test
	public void testDeleteInvestmentSecurityEventPayout_payoutInUseByElection() {
		TestUtils.expectException(ValidationException.class, () -> this.investmentSecurityEventPayoutService.deleteInvestmentSecurityEventPayout(2, false), "The Security Event Payout cannot be deleted because it is used by at least one Security Event Client Election or is a default payout with no alternative payout.");
	}


	@Test
	public void testDeleteInvestmentSecurityEventPayout_defaultPayoutInUseByElectionFails() {
		InvestmentSecurityEventPayout defaultPayout = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(1);
		InvestmentSecurityEventPayout newDefaultPayout = BeanUtils.cloneBean(defaultPayout, false, false);
		newDefaultPayout.setId(null);
		newDefaultPayout.setPayoutNumber((short) 10);
		newDefaultPayout = this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(newDefaultPayout);

		Assertions.assertNotNull(newDefaultPayout.getId());
		this.investmentSecurityEventPayoutService.deleteInvestmentSecurityEventPayout(newDefaultPayout.getId(), false);
		Assertions.assertNull(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(newDefaultPayout.getId()));

		TestUtils.expectException(ValidationException.class, () -> this.investmentSecurityEventPayoutService.deleteInvestmentSecurityEventPayout(defaultPayout.getId(), false), "The Security Event Payout cannot be deleted because it is used by at least one Security Event Client Election or is a default payout with no alternative payout.");
	}


	@Test
	public void testDeleteInvestmentSecurityEventPayout_defaultPayoutInUseByElection_ignoreValidationSucceeds() {
		InvestmentSecurityEventPayout defaultPayout = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(1);
		InvestmentSecurityEvent existingEvent = defaultPayout.getSecurityEvent();

		// Create new event and payout for testing.
		InvestmentSecurityEvent newEvent = BeanUtils.cloneBean(existingEvent, false, false);
		newEvent.setId(null);
		newEvent = this.investmentSecurityEventService.saveInvestmentSecurityEvent(newEvent);

		InvestmentSecurityEventPayout newDefaultPayout = BeanUtils.cloneBean(defaultPayout, false, false);
		newDefaultPayout.setId(null);
		newDefaultPayout.setSecurityEvent(newEvent);
		newDefaultPayout.setPayoutNumber((short) 10);
		newDefaultPayout = this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(newDefaultPayout);
		Assertions.assertNotNull(newDefaultPayout.getId());

		try {
			this.investmentSecurityEventPayoutService.deleteInvestmentSecurityEventPayout(newDefaultPayout.getId(), false);
			Assertions.fail("Expected a validation exception for deleting the only default payout.");
		}
		catch (ValidationException e) {
			Assertions.assertEquals("The Security Event Payout cannot be deleted because it is used by at least one Security Event Client Election or is a default payout with no alternative payout.", e.getMessage());
		}
		Assertions.assertNotNull(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(newDefaultPayout.getId()));
		this.investmentSecurityEventPayoutService.deleteInvestmentSecurityEventPayout(newDefaultPayout.getId(), true);
		Assertions.assertNull(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(newDefaultPayout.getId()));
	}


	@Test
	public void testUpdateInvestmentSecurityEventPayoutElectionNumber_payoutInUseByElectionFails() {
		InvestmentSecurityEventPayout payout = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(2);
		payout.setElectionNumber((short) 5);
		TestUtils.expectException(ValidationException.class, () -> this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout), "The Security Event Payout cannot be updated because it is used by at least one Security Event Client Election or is a default payout with no alternative payout.");
	}


	@Test
	public void testUpdateInvestmentSecurityEventPayoutDeleted_payoutInUseByElectionFails() {
		InvestmentSecurityEventPayout payout = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(2);
		payout.setDeleted(true);
		TestUtils.expectException(ValidationException.class, () -> this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout), "The Security Event Payout cannot be updated because it is used by at least one Security Event Client Election or is a default payout with no alternative payout.");
	}


	@Test
	public void testUpdateInvestmentSecurityEventPayoutDefault_payoutInUseByDefaultWithNoAlternativeFails() {
		InvestmentSecurityEventPayout payout = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(1);
		payout.setDefaultElection(false);
		TestUtils.expectException(ValidationException.class, () -> this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout), "The Security Event Payout cannot be updated because it is used by at least one Security Event Client Election or is a default payout with no alternative payout.");
	}


	@Test
	public void testUpdateInvestmentSecurityEventPayoutBeforeValue_payoutInUseByElectionSucceeds() {
		InvestmentSecurityEventPayout payout = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(2);
		BigDecimal originalValue = payout.getBeforeEventValue();
		try {
			payout.setBeforeAndAfterEventValue(BigDecimal.ONE);
			this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);
		}
		finally {
			payout.setBeforeAndAfterEventValue(originalValue);
			this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);
		}
	}


	@Test
	public void testSaveInvestmentSecurityEventPayout_notUniqueFails() {
		InvestmentSecurityEventPayout payout = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(2);
		InvestmentSecurityEventPayout newPayout = BeanUtils.cloneBean(payout, false, false);
		TestUtils.expectException(ValidationException.class, () -> this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(newPayout), "Investment Security Event Payout already exists for Payout Number and Election Number for Security Event.");
	}


	@Test
	public void testSaveInvestmentEventSecurityEventPayout_multipleDefaultsFail() {
		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setDefaultElection(true);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutType((short) 1));
		payout.setSecurityEvent(this.investmentSecurityEventService.getInvestmentSecurityEvent((short) 3000));
		payout.setPayoutNumber((short) 4);
		payout.setElectionNumber((short) 4);
		TestUtils.expectException(ValidationException.class, () -> {
			this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);
		}, "The Security Event Payout cannot be created because it is marked as a default election, and there are already other default elections defined for this event");
	}


	@Test
	public void testUpdateInvestmentEventSecurityEventPayout_multipleDefaultsFail() {
		// First, create another default payout under the same election number - should save with no problems
		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setDefaultElection(true);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutType((short) 1));
		payout.setSecurityEvent(this.investmentSecurityEventService.getInvestmentSecurityEvent((short) 3000));
		payout.setPayoutNumber((short) 4);
		payout.setElectionNumber((short) 1);
		try {
			this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);
			ValidationUtils.assertNotNull(payout.getId(), "Failed to save payout");

			// now, set to a different election number and it should fail
			payout.setElectionNumber((short) 2);
			TestUtils.expectException(ValidationException.class, () -> {
				this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);
			}, "The Security Event Payout cannot be updated because it is marked as a default election, and there are already other default elections defined for this event");
		}
		finally {
			if (payout.getId() != null) {
				this.investmentSecurityEventPayoutService.deleteInvestmentSecurityEventPayout(payout.getId(), true);
			}
		}
	}


	@Test
	public void testUpdateInvestmentSecurityEventPayoutElectionNumber_notUniqueFails() {
		InvestmentSecurityEventPayout payout = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(2);
		payout.setElectionNumber((short) 1);
		TestUtils.expectException(ValidationException.class, () -> this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout), "Investment Security Event Payout already exists for Payout Number and Election Number for Security Event.");
	}


	@Test
	public void testUpdateInvestmentSecurityEventPayoutPayoutNumber_uniqueSucceeds() {
		InvestmentSecurityEventPayout payout = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(1);
		try {
			payout.setPayoutNumber((short) 4);
			this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);
		}
		finally {
			payout.setPayoutNumber((short) 1);
			this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);
		}
	}


	@Test
	public void testUpdateInvestmentSecurityEventPayoutElectionNumber_uniqueSucceeds() {
		InvestmentSecurityEventPayout payout = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(3);
		try {
			payout.setElectionNumber((short) 4);
			this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);
		}
		finally {
			payout.setElectionNumber((short) 3);
			this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////       Security Event Client Election Tests          ///////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetInvestmentSecurityEventClientElection() {
		Assertions.assertNotNull(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayout(1), "Missing Security Event Client Election");
	}


	@Test
	public void testSaveInvestmentSecurityEventClientElection() {
		InvestmentSecurityEventClientElection clientElection = new InvestmentSecurityEventClientElection();
		clientElection.setClientInvestmentAccount(this.investmentAccountService.getInvestmentAccount(1));
		clientElection.setSecurityEvent(this.investmentSecurityEventService.getInvestmentSecurityEvent(SCRIP_DIVIDEND_SECURITY_EVENT_ID));
		clientElection.setElectionNumber((short) 2);
		InvestmentSecurityEventClientElection savedElection = this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventClientElection(clientElection);
		Assertions.assertFalse(savedElection.isNewBean(), "Security Event Client Election failed to save");
	}


	@Test
	public void testSaveInvestmentSecurityEventClientElection_payoutNotExist() {
		InvestmentSecurityEventClientElection clientElection = new InvestmentSecurityEventClientElection();
		clientElection.setClientInvestmentAccount(this.investmentAccountService.getInvestmentAccount(1));
		clientElection.setSecurityEvent(this.investmentSecurityEventService.getInvestmentSecurityEvent(SCRIP_DIVIDEND_SECURITY_EVENT_ID));
		clientElection.setElectionNumber((short) 4);
		TestUtils.expectException(ValidationException.class, () -> this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventClientElection(clientElection), "The Security Event Client Election must refer to at least one Security Event Payout.");
	}
}
