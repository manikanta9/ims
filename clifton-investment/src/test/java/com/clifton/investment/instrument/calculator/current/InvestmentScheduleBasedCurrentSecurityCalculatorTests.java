package com.clifton.investment.instrument.calculator.current;

import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.core.test.matcher.PropertyMatcher;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentBuilder;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Date;


/**
 * There are more comprehensive integration tests in {@link com.clifton.ims.tests.investment.instrument.calculator.current.InvestmentCurrentSecurityCalculatorTests}
 * where the service method calls don't need to be mocked.
 */
public class InvestmentScheduleBasedCurrentSecurityCalculatorTests {

	private static final Date lastDayOfFebruary = DateUtils.toDate("02/28/2020");
	private static final Date lastDayOfMarch = DateUtils.toDate("03/31/2020");
	private static final InvestmentSecurity februarySecurity = createCurrencyForward("CAD/GBP20200228", "10/26/2010", "02/28/2020");
	private static final InvestmentSecurity marchSecurity = createCurrencyForward("CAD/GBP20200331", "10/26/2010", "02/28/2020");
	@Mock
	private InvestmentInstrumentService investmentInstrumentService;
	@Mock
	private ScheduleApiService scheduleApiService;
	@Mock
	private CalendarSchedule calendarSchedule;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static InvestmentSecurity createCurrencyForward(String symbol, String startDate, String lastDeliveryDate) {
		InvestmentInstrument instrument = InvestmentInstrumentBuilder.createCADGBPCurrencyForward().toInvestmentInstrument();
		return createCurrencyForward(symbol, instrument, startDate, lastDeliveryDate);
	}


	private static InvestmentSecurity createCurrencyForward(String symbol, InvestmentInstrument instrument, String startDate, String lastDeliveryDate) {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(1, symbol, InvestmentType.FORWARDS);
		security.setInstrument(instrument);
		security.setStartDate(DateUtils.toDate(startDate));
		security.setLastDeliveryDate(DateUtils.toDate(lastDeliveryDate));
		security.setId(null);
		return security;
	}


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		Mockito.when(this.scheduleApiService.getScheduleOccurrences(Mockito.argThat(PropertyMatcher.hasProperty("occurrences", 2))))
				.thenReturn(Arrays.asList(DateUtils.toDate("02/28/2020"), DateUtils.toDate("03/31/2020")));

		Mockito.when(this.investmentInstrumentService.getInvestmentSecurityList(Mockito.argThat(PropertyMatcher.hasProperty("endDate", lastDayOfFebruary))))
				.thenReturn(Arrays.asList(februarySecurity));
		Mockito.when(this.investmentInstrumentService.getInvestmentSecurityList(Mockito.argThat(PropertyMatcher.hasProperty("endDate", lastDayOfMarch))))
				.thenReturn(Arrays.asList(marchSecurity));
		Mockito.when(this.investmentInstrumentService.getInvestmentSecurityList(Mockito.argThat(PropertyMatcher.hasProperty("lastDeliveryDate", lastDayOfFebruary))))
				.thenReturn(Arrays.asList(februarySecurity));
		Mockito.when(this.investmentInstrumentService.getInvestmentSecurityList(Mockito.argThat(PropertyMatcher.hasProperty("lastDeliveryDate", lastDayOfMarch))))
				.thenReturn(Arrays.asList(marchSecurity));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidate() {
		TestUtils.expectException(ValidationException.class, () -> createCalculator().validateImpl(null, null),
				"Instrument selection is required.");
	}


	@Test
	public void testValidate_SecurityGroup() {
		TestUtils.expectException(ValidationException.class, () -> {
			createCalculator().validateImpl(null, new InvestmentSecurityGroup());
		}, "Instrument selection is required.");
	}


	@Test
	public void testValidate_Instrument_Valid() {
		createCalculator().validateImpl(InvestmentInstrumentBuilder.createEmptyInvestmentInstrument().toInvestmentInstrument(), null);
	}


	@Test
	public void testValidate_InstrumentAndSecurityGroup_Valid() {
		createCalculator().validateImpl(InvestmentInstrumentBuilder.createEmptyInvestmentInstrument().toInvestmentInstrument(), new InvestmentSecurityGroup());
	}


	@Test
	public void testValidate_OverrideOccurrence_UnderMinValue() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentScheduleBasedCurrentSecurityCalculator calculator = createCalculator();
			calculator.setOccurrenceOverride(1);
			calculator.validateImpl(InvestmentInstrumentBuilder.createEmptyInvestmentInstrument().toInvestmentInstrument(), new InvestmentSecurityGroup());
		}, "Occurrence Override [1] cannot be less than 2 or greater than 52.");
	}


	@Test
	public void testValidate_OverrideOccurrence_OverMaxValue() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentScheduleBasedCurrentSecurityCalculator calculator = createCalculator();
			calculator.setOccurrenceOverride(53);
			calculator.validateImpl(InvestmentInstrumentBuilder.createEmptyInvestmentInstrument().toInvestmentInstrument(), new InvestmentSecurityGroup());
		}, "Occurrence Override [53] cannot be less than 2 or greater than 52.");
	}


	@Test
	public void testValidate_OverrideOccurrence_Valid() {
		InvestmentScheduleBasedCurrentSecurityCalculator calculator = createCalculator();
		calculator.setOccurrenceOverride(2);
		calculator.validateImpl(InvestmentInstrumentBuilder.createEmptyInvestmentInstrument().toInvestmentInstrument(), new InvestmentSecurityGroup());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalculate_BalanceDateOnLastDayOfFeb_EffectiveBusinessDayAdjustment() {
		// push effectiveOnDate into March using effectiveBusinessDayAdjustment so that Feb security is returned
		testCalculateSecurity(februarySecurity, lastDayOfFebruary, createCalculator(1));
		testCalculateSecurity(februarySecurity, lastDayOfFebruary, createCalculator(true, 1));
	}


	@Test
	public void testCalculate_BalanceDateInMiddleOfMarch() {
		testCalculateSecurity(marchSecurity, DateUtils.addDays(lastDayOfMarch, -15));
		testCalculateSecurity(marchSecurity, DateUtils.addDays(lastDayOfMarch, -15), createCalculator(true));
	}


	@Test
	public void testCalculate_BalanceDateOnLastDayOfMarch_NoSecurityFound() {
		Mockito.when(this.scheduleApiService.getScheduleOccurrences(Mockito.argThat(PropertyMatcher.hasProperty("occurrences", 2))))
				.thenReturn(Arrays.asList(DateUtils.toDate("03/31/2020"), DateUtils.toDate("04/30/2020")));

		TestUtils.expectException(ValidationException.class, () -> testCalculateSecurity(marchSecurity, lastDayOfMarch, createCalculator(-1)),
				"Expected exactly 1 current security using Schedule Based Current Security Calculator but found 0 for CAD/GBP Currency Forward with End Date 04/30/2020.");

		TestUtils.expectException(ValidationException.class, () -> testCalculateSecurity(marchSecurity, lastDayOfMarch, createCalculator(true, -1)),
				"Expected exactly 1 current security using Schedule Based Current Security Calculator but found 0 for CAD/GBP Currency Forward with Last Delivery Date 04/30/2020.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void testCalculateSecurity(InvestmentSecurity security, Date balanceDatePlusOneWeekday) {
		testCalculateSecurity(security, balanceDatePlusOneWeekday, createCalculator());
	}


	private void testCalculateSecurity(InvestmentSecurity security, Date balanceDatePlusOneWeekday, InvestmentScheduleBasedCurrentSecurityCalculator calculator) {
		InvestmentSecurity calculatedSecurity = calculator.calculateImpl(security.getInstrument(), new InvestmentSecurityGroup(), null, balanceDatePlusOneWeekday);
		Assertions.assertEquals(security.getSymbol(), calculatedSecurity.getSymbol(), "Incorrect security calculated by Schedule Based Current Security Calculator.");
	}


	private InvestmentScheduleBasedCurrentSecurityCalculator createCalculator() {
		return createCalculator(false, 0);
	}


	private InvestmentScheduleBasedCurrentSecurityCalculator createCalculator(boolean dateResultIsSecurityLastDeliveryDate) {
		return createCalculator(dateResultIsSecurityLastDeliveryDate, 0);
	}


	private InvestmentScheduleBasedCurrentSecurityCalculator createCalculator(int effectiveBusinessDayAdjustment) {
		return createCalculator(false, effectiveBusinessDayAdjustment);
	}


	private InvestmentScheduleBasedCurrentSecurityCalculator createCalculator(boolean dateResultIsSecurityLastDeliveryDate, int effectiveBusinessDayAdjustment) {
		InvestmentScheduleBasedCurrentSecurityCalculator calculator = new InvestmentScheduleBasedCurrentSecurityCalculator();
		calculator.setDateResultIsSecurityLastDeliveryDate(dateResultIsSecurityLastDeliveryDate);
		if (effectiveBusinessDayAdjustment != 0) {
			calculator.setEffectiveBusinessDayAdjustment(effectiveBusinessDayAdjustment);
		}
		calculator.setInvestmentInstrumentService(this.investmentInstrumentService);
		calculator.setScheduleApiService(this.scheduleApiService);
		return calculator;
	}
}
