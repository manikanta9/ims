package com.clifton.investment.instrument.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.accrual.AccrualMethods;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.specificity.InvestmentSpecificityDefinition;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityField;
import com.clifton.investment.specificity.InvestmentSpecificityUtilHandler;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Test class for {@link InvestmentInstrumentValidator}.
 *
 * @author MikeH
 */
public class InvestmentInstrumentValidatorTest {

	private static final String FIELD_NAME = "fieldName";
	private static final String CONDITION_NAME = "conditionName";
	private static final String EVALUATION_MESSAGE = "Evaluation message";


	@InjectMocks
	private InvestmentInstrumentValidator investmentInstrumentValidator;
	@Mock
	private InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler;
	@Mock
	private SystemConditionService systemConditionService;
	@Mock
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	/**
	 * Incrementing ID for generated system conditions.
	 */
	private final AtomicInteger serialConditionId = new AtomicInteger();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFailSaveInvestmentInstrumentSpecificityValidation() {
		InvestmentInstrument instrument = new InvestmentInstrument();
		configureValidatorEntry(Collections.singletonList(instrument), null);
		TestUtils.expectException(FieldValidationException.class, () -> getInvestmentInstrumentValidator().validate(instrument, DaoEventTypes.SAVE), String.format("%s validation failed on field [%s] for condition [%s]: %s", InvestmentSpecificityDefinition.DEFINITION_INSTRUMENT_VALIDATOR, FIELD_NAME, CONDITION_NAME, EVALUATION_MESSAGE));
	}


	@Test
	public void testSuccessSaveInvestmentInstrumentSpecificityValidationOtherInstrument() {
		InvestmentInstrument instrument1 = new InvestmentInstrument();
		InvestmentInstrument instrument2 = new InvestmentInstrument();
		configureValidatorEntry(Collections.singletonList(instrument1), null);
		getInvestmentInstrumentValidator().validate(instrument2, DaoEventTypes.SAVE);
	}


	@Test
	public void testSuccessSaveInvestmentInstrumentSpecificityValidation() {
		InvestmentInstrument instrument = new InvestmentInstrument();
		configureValidatorEntry(Collections.singletonList(instrument), Collections.singletonList(instrument));
		getInvestmentInstrumentValidator().validate(instrument, DaoEventTypes.SAVE);
	}


	@Test
	public void testSaveInvestmentInstrumentAccrualMethod() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(123432, "EA13901GUL", InvestmentType.SWAPS);
		InvestmentInstrument instrument = security.getInstrument();
		InvestmentSecurityEventType accrualEventType = instrument.getHierarchy().getAccrualSecurityEventType();
		instrument.getHierarchy().setAccrualSecurityEventType(null);
		instrument.setAccrualMethod(AccrualMethods.DAILY_COMPOUNDING);
		TestUtils.expectException(FieldValidationException.class, () -> getInvestmentInstrumentValidator().validate(instrument, DaoEventTypes.SAVE), "Accrual Method should not be set on an Instrument that does not have Accruals");

		instrument.getHierarchy().setAccrualSecurityEventType(accrualEventType);
		getInvestmentInstrumentValidator().validate(instrument, DaoEventTypes.SAVE);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void configureInvestmentSpecificityValidatorEntryList(ArgumentMatcher<InvestmentInstrument> instrumentMatcher, List<InvestmentSpecificityEntry> validatorEntryList) {
		Mockito.when(getInvestmentSpecificityUtilHandler().getInvestmentSpecificityEntryListForInstrument(ArgumentMatchers.eq(InvestmentSpecificityDefinition.DEFINITION_INSTRUMENT_VALIDATOR), ArgumentMatchers.argThat(instrumentMatcher))).thenReturn(validatorEntryList);
	}


	/**
	 * Configures the validator entry to produce a condition which succeeds only for the provided securities. This validator cannot be un-configured without
	 * reloading the mock objects.
	 */
	private void configureValidatorEntry(List<InvestmentInstrument> applicableInstruments, List<InvestmentInstrument> successInstruments) {
		int conditionId = this.serialConditionId.getAndIncrement();
		InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
		InvestmentSpecificityField field = new InvestmentSpecificityField();
		field.setName(FIELD_NAME);
		entry.setField(field);
		InvestmentSpecificityInvestmentValidator validator = new InvestmentSpecificityInvestmentValidator();
		validator.setValidationConditionId(conditionId);
		SystemCondition condition = new SystemCondition();
		condition.setName(CONDITION_NAME);

		configureInvestmentSpecificityValidatorEntryList(item -> CollectionUtils.getStream(applicableInstruments).anyMatch(instrument -> instrument == item), Collections.singletonList(entry));
		Mockito.when(getInvestmentSpecificityUtilHandler().generatePopulatedSpecificityBean(ArgumentMatchers.same(entry))).thenReturn(validator);
		Mockito.when(getSystemConditionService().getSystemCondition(ArgumentMatchers.eq(conditionId))).thenReturn(condition);
		Mockito.when(getSystemConditionEvaluationHandler().evaluateCondition(ArgumentMatchers.same(condition), ArgumentMatchers.any(InvestmentInstrument.class)))
				.then(invocation -> {
					InvestmentInstrument instrument = (InvestmentInstrument) invocation.getArguments()[1];
					boolean successInstrument = CollectionUtils.contains(successInstruments, instrument);
					return new EvaluationResult(successInstrument, EVALUATION_MESSAGE, condition, instrument);
				});
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentValidator getInvestmentInstrumentValidator() {
		return this.investmentInstrumentValidator;
	}


	public void setInvestmentInstrumentValidator(InvestmentInstrumentValidator investmentInstrumentValidator) {
		this.investmentInstrumentValidator = investmentInstrumentValidator;
	}


	public InvestmentSpecificityUtilHandler getInvestmentSpecificityUtilHandler() {
		return this.investmentSpecificityUtilHandler;
	}


	public void setInvestmentSpecificityUtilHandler(InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler) {
		this.investmentSpecificityUtilHandler = investmentSpecificityUtilHandler;
	}


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public AtomicInteger getSerialConditionId() {
		return this.serialConditionId;
	}
}
