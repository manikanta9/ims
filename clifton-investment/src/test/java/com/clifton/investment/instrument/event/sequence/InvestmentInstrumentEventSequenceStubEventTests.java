package com.clifton.investment.instrument.event.sequence;

import com.clifton.calendar.CalendarTestObjectFactory;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.calculator.CouponFrequencies;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.sequence.date.EquityLegPaymentDateCalculator;
import com.clifton.investment.instrument.event.sequence.date.InterestLegPaymentDateCalculator;
import com.clifton.investment.instrument.event.sequence.date.InvestmentInstrumentEventSequenceDateCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;


/**
 * The <code>InvestmentInstrumentEventSequenceStubEventTests</code> tests sequences where the first event is pre-created.
 *
 * @author mwacker
 */
public class InvestmentInstrumentEventSequenceStubEventTests extends AbstractInvestmentInstrumentEventSequenceTest {

	private EventSequenceDefinition setup_SDB4176276218(InvestmentInstrumentEventSequenceDateCalculator dateCalculator, String typeName) {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_COMBINED);
		Calendar us_calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(calendar);
		sequence.setSettlementCalendar(us_calendar);

		sequence.setSequenceStartDate(DateUtils.toDate("04/24/2014"));
		sequence.setFirstSequenceEventExist(true);
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Quarterly"));
		sequence.setSettlementCycle(3);
		sequence.setDateCalculator(dateCalculator);
		InvestmentSecurityEventType type = new InvestmentSecurityEventType();
		type.setName(typeName);
		sequence.setType(type);
		sequence.setFixingCycle(1);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(20783, "SDB4176276218", "Swaps"));
		sequence.getSecurity().setStartDate(DateUtils.toDate("03/20/2014"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("10/24/2014"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("10/29/2014"));

		return sequence;
	}


	@Test
	public void testEquityLegPaymentSequence_SDB4176276218() {
		EquityLegPaymentDateCalculator dateCalculator = configureDateCalculator(new EquityLegPaymentDateCalculator());

		EventSequenceDefinition sequence = setup_SDB4176276218(dateCalculator, InvestmentSecurityEventType.EQUITY_LEG_PAYMENT);

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(2, events.size());

		//DeclareDate,ExDate,RecordDate,EventDate
		Assertions.assertEquals("04/24/2014	07/25/2014	07/24/2014	07/29/2014", formatEquityEvent(events.get(0)));
		Assertions.assertEquals("07/24/2014	10/25/2014	10/24/2014	10/29/2014", formatEquityEvent(events.get(1)));
	}


	@Test
	public void testInterestLegPaymentSequence_SDB4176276218() {
		InterestLegPaymentDateCalculator dateCalculator = configureDateCalculator(new InterestLegPaymentDateCalculator());

		EventSequenceDefinition sequence = setup_SDB4176276218(dateCalculator, InvestmentSecurityEventType.INTEREST_LEG_PAYMENT);

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(2, events.size());

		//DeclareDate,ExDate,RecordDate,EventDate
		Assertions.assertEquals("04/29/2014	07/29/2014	07/01/2014	07/29/2014	04/30/2014", formatInterestEvent(events.get(0)));
		Assertions.assertEquals("07/29/2014	10/29/2014	10/01/2014	10/29/2014	07/30/2014", formatInterestEvent(events.get(1)));
	}


	private EventSequenceDefinition setup_BL211932(InvestmentInstrumentEventSequenceDateCalculator dateCalculator, String typeName) {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);
		//Calendar uk_calendar = CALENDAR_SETUP_SERVICE.getCalendar(CALENDAR_UK);
		Calendar combined = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_COMBINED);

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(calendar);
		sequence.setSettlementCalendar(combined);

		sequence.setSequenceStartDate(DateUtils.toDate("06/28/2013"));
		sequence.setFirstSequenceEventExist(true);
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Monthly"));
		sequence.setSettlementCycle(3);
		sequence.setDateCalculator(dateCalculator);
		InvestmentSecurityEventType type = new InvestmentSecurityEventType();
		type.setName(typeName);
		sequence.setType(type);
		sequence.setFixingCycle(1);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(20783, "BL211932", "Swaps"));
		sequence.getSecurity().setStartDate(DateUtils.toDate("06/13/2013"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("01/10/2014"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("01/15/2014"));

		return sequence;
	}


	@Test
	public void testEquityLegPaymentSequence_BL211932() {
		EquityLegPaymentDateCalculator dateCalculator = configureDateCalculator(new EquityLegPaymentDateCalculator());

		EventSequenceDefinition sequence = setup_BL211932(dateCalculator, InvestmentSecurityEventType.EQUITY_LEG_PAYMENT);

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(7, events.size());

		Assertions.assertEquals("06/28/2013	08/01/2013	07/31/2013	08/05/2013", formatEquityEvent(events.get(0)));
		Assertions.assertEquals("07/31/2013	08/31/2013	08/30/2013	09/05/2013", formatEquityEvent(events.get(1)));
		Assertions.assertEquals("08/30/2013	10/01/2013	09/30/2013	10/03/2013", formatEquityEvent(events.get(2)));
		Assertions.assertEquals("09/30/2013	11/01/2013	10/31/2013	11/05/2013", formatEquityEvent(events.get(3)));
		Assertions.assertEquals("10/31/2013	11/30/2013	11/29/2013	12/04/2013", formatEquityEvent(events.get(4)));
		Assertions.assertEquals("11/29/2013	01/01/2014	12/31/2013	01/06/2014", formatEquityEvent(events.get(5)));
		Assertions.assertEquals("12/31/2013	01/11/2014	01/10/2014	01/15/2014", formatEquityEvent(events.get(6)));
	}


	@Test
	public void testInterestLegPaymentSequence_BL211932() {
		InterestLegPaymentDateCalculator dateCalculator = configureDateCalculator(new InterestLegPaymentDateCalculator());

		EventSequenceDefinition sequence = setup_BL211932(dateCalculator, InvestmentSecurityEventType.INTEREST_LEG_PAYMENT);

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(7, events.size());

		Assertions.assertEquals("07/03/2013	08/05/2013	08/01/2013	08/05/2013	07/05/2013", formatInterestEvent(events.get(0)));
		Assertions.assertEquals("08/05/2013	09/05/2013	09/01/2013	09/05/2013	08/06/2013", formatInterestEvent(events.get(1)));
		Assertions.assertEquals("09/05/2013	10/03/2013	10/01/2013	10/03/2013	09/06/2013", formatInterestEvent(events.get(2)));
		Assertions.assertEquals("10/03/2013	11/05/2013	11/01/2013	11/05/2013	10/04/2013", formatInterestEvent(events.get(3)));
		Assertions.assertEquals("11/05/2013	12/04/2013	12/01/2013	12/04/2013	11/06/2013", formatInterestEvent(events.get(4)));
		Assertions.assertEquals("12/04/2013	01/06/2014	01/01/2014	01/06/2014	12/05/2013", formatInterestEvent(events.get(5)));
		Assertions.assertEquals("01/06/2014	01/15/2014	01/11/2014	01/15/2014	01/07/2014", formatInterestEvent(events.get(6)));
	}
}
