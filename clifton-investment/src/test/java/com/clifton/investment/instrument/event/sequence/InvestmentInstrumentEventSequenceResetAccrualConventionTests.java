package com.clifton.investment.instrument.event.sequence;

import com.clifton.calendar.CalendarTestObjectFactory;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.calculator.CouponFrequencies;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.sequence.date.InterestLegPaymentDateCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;


public class InvestmentInstrumentEventSequenceResetAccrualConventionTests extends AbstractInvestmentInstrumentEventSequenceTest {

	@Test
	public void testInterestLegPaymentSequenceValuationDate_186420142A() {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);

		InterestLegPaymentDateCalculator dateCalculator = configureDateCalculator(new InterestLegPaymentDateCalculator());

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(calendar);
		sequence.setSettlementCalendar(calendar);

		sequence.setResetAccrualConvention(EventSequenceResetAccrualConventions.INTEREST_ACCRUES_ON_VALUATION_DATE);

		sequence.setSequenceStartDate(DateUtils.toDate("11/30/2012"));
		sequence.setFirstSequenceEventExist(true);
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Monthly"));
		sequence.setSettlementCycle(2);
		sequence.setDateCalculator(dateCalculator);
		sequence.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.INTEREST_LEG_PAYMENT));
		sequence.setFixingCycle(0);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(19515, "186420142A", "Swaps"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("11/29/2013"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("12/04/2013"));

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(12, events.size());

		Assertions.assertEquals("11/30/2012	12/31/2012	01/01/2013	01/03/2013	11/30/2012", formatInterestEvent(events.get(0)));
		Assertions.assertEquals("12/31/2012	01/31/2013	02/01/2013	02/04/2013	12/31/2012", formatInterestEvent(events.get(1)));
		Assertions.assertEquals("01/31/2013	02/28/2013	03/01/2013	03/04/2013	01/31/2013", formatInterestEvent(events.get(2)));
		Assertions.assertEquals("02/28/2013	03/28/2013	03/29/2013	04/02/2013	02/28/2013", formatInterestEvent(events.get(3)));
		Assertions.assertEquals("03/28/2013	04/30/2013	05/01/2013	05/02/2013	03/28/2013", formatInterestEvent(events.get(4)));
		Assertions.assertEquals("04/30/2013	05/31/2013	06/01/2013	06/04/2013	04/30/2013", formatInterestEvent(events.get(5)));
		Assertions.assertEquals("05/31/2013	06/28/2013	06/29/2013	07/02/2013	05/31/2013", formatInterestEvent(events.get(6)));
		Assertions.assertEquals("06/28/2013	07/31/2013	08/01/2013	08/02/2013	06/28/2013", formatInterestEvent(events.get(7)));
		Assertions.assertEquals("07/31/2013	08/30/2013	08/31/2013	09/04/2013	07/31/2013", formatInterestEvent(events.get(8)));
		Assertions.assertEquals("08/30/2013	09/30/2013	10/01/2013	10/02/2013	08/30/2013", formatInterestEvent(events.get(9)));
		Assertions.assertEquals("09/30/2013	10/31/2013	11/01/2013	11/04/2013	09/30/2013", formatInterestEvent(events.get(10)));
		Assertions.assertEquals("10/31/2013	11/29/2013	11/30/2013	12/03/2013	10/31/2013", formatInterestEvent(events.get(11)));
	}


	@Test
	public void testInterestLegPaymentSequence_186420142A() {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);

		InterestLegPaymentDateCalculator dateCalculator = configureDateCalculator(new InterestLegPaymentDateCalculator());

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(calendar);
		sequence.setSettlementCalendar(calendar);

		sequence.setResetAccrualConvention(EventSequenceResetAccrualConventions.INTEREST_ACCRUES_ON_MONTH_END);

		sequence.setSequenceStartDate(DateUtils.toDate("11/30/2012"));
		sequence.setFirstSequenceEventExist(true);
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Monthly"));
		sequence.setSettlementCycle(2);
		sequence.setDateCalculator(dateCalculator);
		sequence.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.INTEREST_LEG_PAYMENT));
		sequence.setFixingCycle(0);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(19515, "186420142A", "Swaps"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("11/29/2013"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("12/04/2013"));

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(12, events.size());

		Assertions.assertEquals("11/30/2012	12/31/2012	01/01/2013	01/03/2013	11/30/2012", formatInterestEvent(events.get(0)));
		Assertions.assertEquals("12/31/2012	01/31/2013	02/01/2013	02/04/2013	12/31/2012", formatInterestEvent(events.get(1)));
		Assertions.assertEquals("01/31/2013	02/28/2013	03/01/2013	03/04/2013	01/31/2013", formatInterestEvent(events.get(2)));
		Assertions.assertEquals("02/28/2013	03/28/2013	03/29/2013	04/02/2013	02/28/2013", formatInterestEvent(events.get(3)));
		Assertions.assertEquals("03/28/2013	04/30/2013	05/01/2013	05/02/2013	03/28/2013", formatInterestEvent(events.get(4)));
		Assertions.assertEquals("04/30/2013	05/31/2013	06/01/2013	06/04/2013	04/30/2013", formatInterestEvent(events.get(5)));
		Assertions.assertEquals("05/31/2013	06/28/2013	06/29/2013	07/02/2013	05/31/2013", formatInterestEvent(events.get(6)));
		Assertions.assertEquals("06/28/2013	07/31/2013	08/01/2013	08/02/2013	06/28/2013", formatInterestEvent(events.get(7)));
		Assertions.assertEquals("07/31/2013	08/30/2013	08/31/2013	09/04/2013	07/31/2013", formatInterestEvent(events.get(8)));
		Assertions.assertEquals("08/30/2013	09/30/2013	10/01/2013	10/02/2013	08/30/2013", formatInterestEvent(events.get(9)));
		Assertions.assertEquals("09/30/2013	10/31/2013	11/01/2013	11/04/2013	09/30/2013", formatInterestEvent(events.get(10)));
		Assertions.assertEquals("10/31/2013	11/29/2013	11/30/2013	12/03/2013	10/31/2013", formatInterestEvent(events.get(11)));
	}
}
