package com.clifton.investment.instrument.calculator.current;

import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;


@ContextConfiguration("InvestmentCurrentSecurityCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class InvestmentFuturesCurrentSecurityCalculatorTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private CalendarSetupService calendarSetupService;


	private InvestmentFuturesCurrentSecurityCalculator getCalculator(String deliveryMonthSchedule, int monthYearSwitches) {
		InvestmentFuturesCurrentSecurityCalculator calc = new InvestmentFuturesCurrentSecurityCalculator();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(calc, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		if (deliveryMonthSchedule != null) {
			String[] deliveryMonths = deliveryMonthSchedule.split("-");
			calc.setJanuaryDelivery(Integer.valueOf(deliveryMonths[0]));
			calc.setFebruaryDelivery(Integer.valueOf(deliveryMonths[1]));
			calc.setMarchDelivery(Integer.valueOf(deliveryMonths[2]));
			calc.setAprilDelivery(Integer.valueOf(deliveryMonths[3]));
			calc.setMayDelivery(Integer.valueOf(deliveryMonths[4]));
			calc.setJuneDelivery(Integer.valueOf(deliveryMonths[5]));
			calc.setJulyDelivery(Integer.valueOf(deliveryMonths[6]));
			calc.setAugustDelivery(Integer.valueOf(deliveryMonths[7]));
			calc.setSeptemberDelivery(Integer.valueOf(deliveryMonths[8]));
			calc.setOctoberDelivery(Integer.valueOf(deliveryMonths[9]));
			calc.setNovemberDelivery(Integer.valueOf(deliveryMonths[10]));
			calc.setDecemberDelivery(Integer.valueOf(deliveryMonths[11]));

			calc.setMonthYearSwitches(monthYearSwitches);
		}
		return calc;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidate_InstrumentNotAFuture() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentInstrument i = new InvestmentInstrument();
			i.setName("Test i");
			InvestmentInstrumentHierarchy h = new InvestmentInstrumentHierarchy();
			InvestmentType t = new InvestmentType();
			t.setName(InvestmentType.STOCKS);
			h.setInvestmentType(t);
			i.setHierarchy(h);

			InvestmentFuturesCurrentSecurityCalculator calc = getCalculator(null, 0);
			calc.validateImpl(i, null);
		}, "Futures Delivery Month Current Security Calculators can only be used for Futures type instrument selections");
	}


	@Test
	public void testValidate_SecurityGroup() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentSecurityGroup securityGroup = new InvestmentSecurityGroup();
			InvestmentFuturesCurrentSecurityCalculator calc = getCalculator(null, 0);
			calc.validateImpl(null, securityGroup);
		}, "Security group selection is not allowed for the Futures Schedule calculator - doesn't apply.");
	}


	@Test
	public void testValidate_InstrumentAndSecurityGroup() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentSecurityGroup securityGroup = new InvestmentSecurityGroup();
			InvestmentFuturesCurrentSecurityCalculator calc = getCalculator(null, 0);

			InvestmentInstrument i = new InvestmentInstrument();
			i.setName("Test i");
			InvestmentInstrumentHierarchy h = new InvestmentInstrumentHierarchy();
			InvestmentType t = new InvestmentType();
			t.setName(InvestmentType.FUTURES);
			h.setInvestmentType(t);
			i.setHierarchy(h);

			calc.validateImpl(i, securityGroup);
		}, "Security group selection is not allowed for the Futures Schedule calculator - doesn't apply.");
	}


	@Test
	public void testValidateInstrument_Valid() {
		InvestmentInstrument i = new InvestmentInstrument();
		i.setName("Test i");
		InvestmentInstrumentHierarchy h = new InvestmentInstrumentHierarchy();
		InvestmentType t = new InvestmentType();
		t.setName(InvestmentType.FUTURES);
		h.setInvestmentType(t);
		i.setHierarchy(h);

		InvestmentFuturesCurrentSecurityCalculator calc = getCalculator(null, 0);
		calc.validateImpl(i, null);
	}


	////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////


	/**
	 * For tests, just validating we return the correct symbol
	 */
	@Test
	public void testGetCurrentContractSymbol() {
		// MARCH-MAY-MAY-JULY-JULY-SEPT-SEPT-NOV-NOV-JAN-JAN-MARCH
		// MOVE TO NEXT YEAR AFTER SEPTEMBER
		String deliverySchedule = "3-5-5-7-7-9-9-11-11-1-1-3";
		int monthWhereYearSwitches = 9;

		InvestmentInstrument i = new InvestmentInstrument();
		i.setIdentifierPrefix("NG"); // Natural Gas

		InvestmentFuturesCurrentSecurityCalculator calc = getCalculator(deliverySchedule, monthWhereYearSwitches);

		//validateExpectedSymbol("NGH3", i, DateUtils.toDate("01/01/2013"), calc);
		validateExpectedSymbol("NGK3", i, DateUtils.toDate("02/03/2013"), calc);
		validateExpectedSymbol("NGK3", i, DateUtils.toDate("03/31/2013"), calc);
		validateExpectedSymbol("NGN3", i, DateUtils.toDate("04/10/2013"), calc);
		validateExpectedSymbol("NGN3", i, DateUtils.toDate("05/10/2013"), calc);
		validateExpectedSymbol("NGU3", i, DateUtils.toDate("06/30/2013"), calc);
		validateExpectedSymbol("NGU3", i, DateUtils.toDate("07/02/2013"), calc);
		validateExpectedSymbol("NGX3", i, DateUtils.toDate("08/01/2013"), calc);
		validateExpectedSymbol("NGX3", i, DateUtils.toDate("09/23/2013"), calc);
		validateExpectedSymbol("NGF4", i, DateUtils.toDate("10/01/2013"), calc);
		validateExpectedSymbol("NGF4", i, DateUtils.toDate("11/23/2013"), calc);
		validateExpectedSymbol("NGH4", i, DateUtils.toDate("12/31/2013"), calc);

		validateExpectedSymbol("NGK9", i, DateUtils.toDate("02/03/2019"), calc);
		validateExpectedSymbol("NGK9", i, DateUtils.toDate("03/31/2019"), calc);
		validateExpectedSymbol("NGN9", i, DateUtils.toDate("04/10/2019"), calc);
		validateExpectedSymbol("NGN9", i, DateUtils.toDate("05/10/2019"), calc);
		validateExpectedSymbol("NGU9", i, DateUtils.toDate("06/30/2019"), calc);
		validateExpectedSymbol("NGU9", i, DateUtils.toDate("07/02/2019"), calc);
		validateExpectedSymbol("NGX9", i, DateUtils.toDate("08/01/2019"), calc);
		validateExpectedSymbol("NGX9", i, DateUtils.toDate("09/23/2019"), calc);
		validateExpectedSymbol("NGF0", i, DateUtils.toDate("10/01/2019"), calc);
		validateExpectedSymbol("NGF0", i, DateUtils.toDate("11/23/2019"), calc);
		validateExpectedSymbol("NGH0", i, DateUtils.toDate("12/31/2019"), calc);

		// Set To Add To Current Year + 1 Year
		calc.setAddToCurrentYear(1);
		validateExpectedSymbol("NGK4", i, DateUtils.toDate("02/03/2013"), calc);
		validateExpectedSymbol("NGK4", i, DateUtils.toDate("03/31/2013"), calc);
		validateExpectedSymbol("NGN4", i, DateUtils.toDate("04/10/2013"), calc);
		validateExpectedSymbol("NGN4", i, DateUtils.toDate("05/10/2013"), calc);
		validateExpectedSymbol("NGU4", i, DateUtils.toDate("06/30/2013"), calc);
		validateExpectedSymbol("NGU4", i, DateUtils.toDate("07/02/2013"), calc);
		validateExpectedSymbol("NGX4", i, DateUtils.toDate("08/01/2013"), calc);
		validateExpectedSymbol("NGX4", i, DateUtils.toDate("09/23/2013"), calc);
		validateExpectedSymbol("NGF5", i, DateUtils.toDate("10/01/2013"), calc);
		validateExpectedSymbol("NGF5", i, DateUtils.toDate("11/23/2013"), calc);
		validateExpectedSymbol("NGH5", i, DateUtils.toDate("12/31/2013"), calc);

		validateExpectedSymbol("NGK0", i, DateUtils.toDate("02/03/2019"), calc);
		validateExpectedSymbol("NGK0", i, DateUtils.toDate("03/31/2019"), calc);
		validateExpectedSymbol("NGN0", i, DateUtils.toDate("04/10/2019"), calc);
		validateExpectedSymbol("NGN0", i, DateUtils.toDate("05/10/2019"), calc);
		validateExpectedSymbol("NGU0", i, DateUtils.toDate("06/30/2019"), calc);
		validateExpectedSymbol("NGU0", i, DateUtils.toDate("07/02/2019"), calc);
		validateExpectedSymbol("NGX0", i, DateUtils.toDate("08/01/2019"), calc);
		validateExpectedSymbol("NGX0", i, DateUtils.toDate("09/23/2019"), calc);
		validateExpectedSymbol("NGF1", i, DateUtils.toDate("10/01/2019"), calc);
		validateExpectedSymbol("NGF1", i, DateUtils.toDate("11/23/2019"), calc);
		validateExpectedSymbol("NGH1", i, DateUtils.toDate("12/31/2019"), calc);
	}


	@Test
	public void testGetCurrentContractSymbol_BusinessDaysOfMonth_Plus10() {
		setupCalendarForYear((short) 2013);
		// MARCH-MAY-MAY-JULY-JULY-SEPT-SEPT-NOV-NOV-JAN-JAN-MARCH
		// MOVE TO NEXT YEAR AFTER SEPTEMBER
		String deliverySchedule = "3-5-5-7-7-9-9-11-11-1-1-3";
		int monthWhereYearSwitches = 9;

		InvestmentInstrument i = new InvestmentInstrument();
		i.setIdentifierPrefix("NG"); // Natural Gas

		InvestmentFuturesCurrentSecurityCalculator calc = getCalculator(deliverySchedule, monthWhereYearSwitches);
		calc.setBusinessDayOfMonthEffective(10);

		validateExpectedSymbol("NGH3", i, DateUtils.toDate("01/01/2013"), calc);
		validateExpectedSymbol("NGH3", i, DateUtils.toDate("02/03/2013"), calc);
		validateExpectedSymbol("NGK3", i, DateUtils.toDate("03/31/2013"), calc);
		validateExpectedSymbol("NGK3", i, DateUtils.toDate("04/10/2013"), calc);
		validateExpectedSymbol("NGN3", i, DateUtils.toDate("05/10/2013"), calc);
		validateExpectedSymbol("NGU3", i, DateUtils.toDate("06/30/2013"), calc);
		validateExpectedSymbol("NGU3", i, DateUtils.toDate("07/02/2013"), calc);
		validateExpectedSymbol("NGU3", i, DateUtils.toDate("08/01/2013"), calc);
		validateExpectedSymbol("NGX3", i, DateUtils.toDate("09/23/2013"), calc);
		validateExpectedSymbol("NGX4", i, DateUtils.toDate("10/01/2013"), calc);
		validateExpectedSymbol("NGF4", i, DateUtils.toDate("11/23/2013"), calc);
		validateExpectedSymbol("NGH4", i, DateUtils.toDate("12/31/2013"), calc);
	}


	@Test
	public void testGetCurrentContractSymbol_BusinessDaysOfMonth_Minus10() {
		setupCalendarForYear((short) 2013);
		// MARCH-MAY-MAY-JULY-JULY-SEPT-SEPT-NOV-NOV-JAN-JAN-MARCH
		// MOVE TO NEXT YEAR AFTER SEPTEMBER
		String deliverySchedule = "3-5-5-7-7-9-9-11-11-1-1-3";
		int monthWhereYearSwitches = 7;

		InvestmentInstrument i = new InvestmentInstrument();
		i.setIdentifierPrefix("NG"); // Natural Gas

		InvestmentFuturesCurrentSecurityCalculator calc = getCalculator(deliverySchedule, monthWhereYearSwitches);
		calc.setBusinessDayOfMonthEffective(-10);

		validateExpectedSymbol("NGH3", i, DateUtils.toDate("01/01/2013"), calc);
		validateExpectedSymbol("NGH13", i, DateUtils.toDate("01/01/2013"), calc, true);
		validateExpectedSymbol("NGH3", i, DateUtils.toDate("02/03/2013"), calc);
		validateExpectedSymbol("NGK3", i, DateUtils.toDate("03/31/2013"), calc);
		validateExpectedSymbol("NGK3", i, DateUtils.toDate("04/10/2013"), calc);
		validateExpectedSymbol("NGN3", i, DateUtils.toDate("05/10/2013"), calc);
		validateExpectedSymbol("NGU3", i, DateUtils.toDate("06/30/2013"), calc);
		validateExpectedSymbol("NGU3", i, DateUtils.toDate("07/02/2013"), calc);
		validateExpectedSymbol("NGU4", i, DateUtils.toDate("08/01/2013"), calc);
		validateExpectedSymbol("NGX4", i, DateUtils.toDate("09/23/2013"), calc);
		validateExpectedSymbol("NGX4", i, DateUtils.toDate("10/01/2013"), calc);
		validateExpectedSymbol("NGF4", i, DateUtils.toDate("11/23/2013"), calc);
		validateExpectedSymbol("NGH4", i, DateUtils.toDate("12/31/2013"), calc);
		validateExpectedSymbol("NGH14", i, DateUtils.toDate("12/31/2013"), calc, true);
	}


	@Test
	public void testGetCurrentContractSymbol_CalendarDaysOfMonth_Plus10() {
		setupCalendarForYear((short) 2013);
		// MARCH-MAY-MAY-JULY-JULY-SEPT-SEPT-NOV-NOV-JAN-JAN-MARCH
		// MOVE TO NEXT YEAR AFTER SEPTEMBER
		String deliverySchedule = "3-5-5-7-7-9-9-11-11-1-1-3";
		int monthWhereYearSwitches = 9;

		InvestmentInstrument i = new InvestmentInstrument();
		i.setIdentifierPrefix("NG"); // Natural Gas

		InvestmentFuturesCurrentSecurityCalculator calc = getCalculator(deliverySchedule, monthWhereYearSwitches);
		calc.setBusinessDayOfMonthEffective(10);
		calc.setUseCalendarDays(true);

		validateExpectedSymbol("NGH3", i, DateUtils.toDate("01/01/2013"), calc);
		validateExpectedSymbol("NGH13", i, DateUtils.toDate("01/01/2013"), calc, true);
		validateExpectedSymbol("NGH3", i, DateUtils.toDate("02/03/2013"), calc);
		validateExpectedSymbol("NGK3", i, DateUtils.toDate("03/31/2013"), calc);
		validateExpectedSymbol("NGN3", i, DateUtils.toDate("04/10/2013"), calc);
		validateExpectedSymbol("NGN3", i, DateUtils.toDate("05/10/2013"), calc);
		validateExpectedSymbol("NGU3", i, DateUtils.toDate("06/30/2013"), calc);
		validateExpectedSymbol("NGU3", i, DateUtils.toDate("07/02/2013"), calc);
		validateExpectedSymbol("NGU3", i, DateUtils.toDate("08/01/2013"), calc);
		validateExpectedSymbol("NGX3", i, DateUtils.toDate("09/23/2013"), calc);
		validateExpectedSymbol("NGX4", i, DateUtils.toDate("10/01/2013"), calc);
		validateExpectedSymbol("NGX14", i, DateUtils.toDate("10/01/2013"), calc, true);
		validateExpectedSymbol("NGF4", i, DateUtils.toDate("11/23/2013"), calc);
		validateExpectedSymbol("NGH4", i, DateUtils.toDate("12/31/2013"), calc);
		validateExpectedSymbol("NGH14", i, DateUtils.toDate("12/31/2013"), calc, true);
	}


	@Test
	public void testGetCurrentContractSymbol_CalendarDaysOfMonth_Minus10() {
		setupCalendarForYear((short) 2013);
		// MARCH-MAY-MAY-JULY-JULY-SEPT-SEPT-NOV-NOV-JAN-JAN-MARCH
		// MOVE TO NEXT YEAR AFTER SEPTEMBER
		String deliverySchedule = "3-5-5-7-7-9-9-11-11-1-1-3";
		int monthWhereYearSwitches = 7;

		InvestmentInstrument i = new InvestmentInstrument();
		i.setIdentifierPrefix("NG"); // Natural Gas

		InvestmentFuturesCurrentSecurityCalculator calc = getCalculator(deliverySchedule, monthWhereYearSwitches);
		calc.setBusinessDayOfMonthEffective(-10);
		calc.setUseCalendarDays(true);

		validateExpectedSymbol("NGH3", i, DateUtils.toDate("01/01/2013"), calc);
		validateExpectedSymbol("NGH13", i, DateUtils.toDate("01/01/2013"), calc, true);
		validateExpectedSymbol("NGH3", i, DateUtils.toDate("02/03/2013"), calc);
		validateExpectedSymbol("NGK3", i, DateUtils.toDate("03/31/2013"), calc);
		validateExpectedSymbol("NGK3", i, DateUtils.toDate("04/10/2013"), calc);
		validateExpectedSymbol("NGN3", i, DateUtils.toDate("05/10/2013"), calc);
		validateExpectedSymbol("NGU3", i, DateUtils.toDate("06/30/2013"), calc);
		validateExpectedSymbol("NGU13", i, DateUtils.toDate("06/30/2013"), calc, true);
		validateExpectedSymbol("NGU3", i, DateUtils.toDate("07/02/2013"), calc);
		validateExpectedSymbol("NGU4", i, DateUtils.toDate("08/01/2013"), calc);
		validateExpectedSymbol("NGX4", i, DateUtils.toDate("09/23/2013"), calc);
		validateExpectedSymbol("NGX4", i, DateUtils.toDate("10/01/2013"), calc);
		validateExpectedSymbol("NGF4", i, DateUtils.toDate("11/23/2013"), calc);
		validateExpectedSymbol("NGH4", i, DateUtils.toDate("12/31/2013"), calc);
		validateExpectedSymbol("NGH14", i, DateUtils.toDate("12/31/2013"), calc, true);
	}


	@Test
	public void testGetCurrentContractSymbol_BusinessDaysOfMonth_Minus3() {
		setupCalendarForYear((short) 2018);
		// MAY-MAY-JULY-JULY-OCT-OCT-OCT-MARCH-MARCH-MARCH-MARCH
		// MOVE TO NEXT YEAR ON AUG 29TH
		String deliverySchedule = "5-5-7-7-10-10-10-3-3-3-3-3";
		int monthWhereYearSwitches = 8;

		InvestmentInstrument i = new InvestmentInstrument();
		i.setIdentifierPrefix("SB"); // sugar

		InvestmentFuturesCurrentSecurityCalculator calc = getCalculator(deliverySchedule, monthWhereYearSwitches);
		calc.setBusinessDayOfMonthEffective(-3);
		calc.setUseCalendarDays(false);
		calc.setSwitchYearOnEffectiveDayOfMonth(true);

		validateExpectedSymbol("SBV8", i, DateUtils.toDate("08/28/2018"), calc);
		validateExpectedSymbol("SBH9", i, DateUtils.toDate("08/29/2018"), calc);
		validateExpectedSymbol("SBH9", i, DateUtils.toDate("08/30/2018"), calc);
		validateExpectedSymbol("SBH9", i, DateUtils.toDate("08/31/2018"), calc);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validateExpectedSymbol(String expected, InvestmentInstrument i, Date activeOnDate, InvestmentFuturesCurrentSecurityCalculator calc) {
		validateExpectedSymbol(expected, i, activeOnDate, calc, false);
	}


	private void validateExpectedSymbol(String expected, InvestmentInstrument i, Date activeOnDate, InvestmentFuturesCurrentSecurityCalculator calc, boolean useTwoYearFormat) {
		String result = calc.calculateCurrentSecuritySymbol(i, activeOnDate, useTwoYearFormat);
		Assertions.assertEquals(expected, result);
	}


	private void setupCalendarForYear(short year) {
		try {
			this.calendarSetupService.saveCalendarYearForYear(year);
		}
		catch (ValidationException e) {
			// Ignore exception if calendar was already set up
		}
	}


	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
