SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityId
FROM InvestmentSecurity
WHERE Symbol IN ('USD', 'IBM', 'IVV', 'TPZ0','LLY US 04/16/21 C230', 'FRIFX')
UNION
SELECT 'InvestmentSecurityGroupSecurity' AS entityTableName, InvestmentSecurityGroupSecurityID AS entityId
FROM InvestmentSecurityGroupSecurity
WHERE InvestmentSecurityGroupID = (SELECT InvestmentSecurityGroupID FROM InvestmentSecurityGroup WHERE SecurityGroupName = 'CLS Settlement Currency');
