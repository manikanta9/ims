package com.clifton.investment.instrument.calculator;

import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.calendar.setup.CalendarTimeZone;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.TestNotionalMultiplierRetriever;
import com.clifton.investment.instrument.calculator.accrual.AccrualBasis;
import com.clifton.investment.instrument.calculator.accrual.AccrualDates;
import com.clifton.investment.instrument.calculator.accrual.AccrualMethods;
import com.clifton.investment.instrument.calculator.accrual.InvestmentAccrualDayCountCalculator;
import com.clifton.investment.instrument.calculator.accrual.InvestmentAccrualDayCountCompoundedCalculator;
import com.clifton.investment.instrument.calculator.accrual.date.AccrualDateCalculators;
import com.clifton.investment.instrument.calculator.accrual.sign.AccrualSignCalculators;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentCalculatorImplTests {

	@Resource
	private InvestmentCalculator investmentCalculator;

	@Resource
	private InvestmentAccrualDayCountCalculator investmentAccrualDayCountCalculator;

	@Resource
	private InvestmentAccrualDayCountCompoundedCalculator investmentAccrualDayCountCompoundedCalculator;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	@Resource
	private CalendarSetupService calendarSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void resetTests() {
		// Some tests replace the event service on the calculator with a mock - make sure it's reset before each test to prevent having to use dirties context
		((InvestmentCalculatorImpl) this.investmentCalculator).setInvestmentSecurityEventService(this.investmentSecurityEventService);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// Notional Calculation Tests


	@Test
	public void testCalculateNotional2_DEFAULT() {
		InvestmentType type = new InvestmentType();
		type.setQuantityDecimalPrecision((short) 2);
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setInvestmentType(type);
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setPriceMultiplier(new BigDecimal(1));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("1.51829"), new BigDecimal("410000"), new BigDecimal("1.0000000000"));
		Assertions.assertEquals(new BigDecimal("270040.64"), result);
	}


	@Test
	public void testCalculateNotional_DEFAULT() {
		InvestmentType type = new InvestmentType();
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setInvestmentType(type);
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setPriceMultiplier(new BigDecimal(2000));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateNotional(security, new BigDecimal("108.9765625"), new BigDecimal(18), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("3923156.25"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("108.9765625"), new BigDecimal("3923156.25"), null);
		Assertions.assertEquals(new BigDecimal("18.00000000000000000000"), result);

		instrument.setPriceMultiplier(new BigDecimal(50));
		result = this.investmentCalculator.calculateNotional(security, new BigDecimal("952.0380842"), new BigDecimal(140), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("6664266.59"), result);

		hierarchy.getInvestmentType().setQuantityDecimalPrecision((short) 2);
		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("952.0380842"), new BigDecimal("6664266.59"), null);
		Assertions.assertEquals(new BigDecimal("140.00"), result);
	}


	@Test
	public void testCalculateNotional_HIERARCHY_MONEY_HALF_UP() {
		InvestmentType type = new InvestmentType();
		type.setQuantityDecimalPrecision((short) 0);
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setCostCalculator(InvestmentNotionalCalculatorTypes.MONEY_HALF_UP);
		hierarchy.setInvestmentType(type);
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setPriceMultiplier(new BigDecimal(2000));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateNotional(security, new BigDecimal("108.9765625"), new BigDecimal(18), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("3923156.25"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("108.9765625"), new BigDecimal("3923156.25"), null);
		Assertions.assertEquals(new BigDecimal("18"), result);
	}


	@Test
	public void testCalculateNotional_HIERARCHY_BPS_DISCOUNT_MONEY_HALF_UP_NEGATIVE_QTY() {
		InvestmentType type = new InvestmentType();
		type.setQuantityDecimalPrecision((short) 0);
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setCostCalculator(InvestmentNotionalCalculatorTypes.BPS_DISCOUNT_MONEY_HALF_UP_NEGATIVE_QTY);
		hierarchy.setInvestmentType(type);
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setPriceMultiplier(new BigDecimal("0.01"));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateNotional(security, new BigDecimal("100"), new BigDecimal("1000"), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("0.00"), result);

		result = this.investmentCalculator.calculateNotional(security, new BigDecimal("100"), new BigDecimal("-1000"), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("0.00"), result);

		result = this.investmentCalculator.calculateNotional(security, new BigDecimal("99.87802469"), new BigDecimal("-50000000"), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("-60987.66"), result);

		result = this.investmentCalculator.calculateNotional(security, new BigDecimal("99.87802469"), new BigDecimal("50000000"), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("-60987.66"), result);

		boolean error = false;
		try {
			this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("100"), new BigDecimal("1000"), null);
		}
		catch (IllegalStateException e) {
			error = true;
		}
		Assertions.assertTrue(error);
	}


	@Test
	public void testCalculateNotional_HIERARCHY_MONEY_UNIT_HALF_UP() {
		InvestmentType type = new InvestmentType();
		type.setQuantityDecimalPrecision((short) 2);
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setInvestmentType(type);
		hierarchy.setCostCalculator(InvestmentNotionalCalculatorTypes.MONEY_UNIT_HALF_UP);
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setPriceMultiplier(new BigDecimal(2000));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateNotional(security, new BigDecimal("108.9765625"), new BigDecimal(18), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("3923156.34"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("108.9765625"), new BigDecimal("3923156.34"), null);
		Assertions.assertEquals(new BigDecimal("18.00"), result);
	}


	@Test
	public void testCalculateNotional_INSTRUMENT_MONEY_UNIT_HALF_UP() {
		InvestmentType type = new InvestmentType();
		type.setQuantityDecimalPrecision((short) 2);
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setInvestmentType(type);
		hierarchy.setCostCalculator(InvestmentNotionalCalculatorTypes.MONEY_HALF_UP);
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setCostCalculator(InvestmentNotionalCalculatorTypes.MONEY_UNIT_HALF_UP);
		instrument.setPriceMultiplier(new BigDecimal(2000));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateNotional(security, new BigDecimal("108.9765625"), new BigDecimal(18), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("3923156.34"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("108.9765625"), new BigDecimal("3923156.34"), null);
		Assertions.assertEquals(new BigDecimal("18.00"), result);
	}


	@Test
	public void testCalculateNotional_INSTRUMENT_INTEGER_UNIT_HALF_UP() {
		InvestmentType type = new InvestmentType();
		type.setQuantityDecimalPrecision((short) 0);
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setInvestmentType(type);
		hierarchy.setCostCalculator(InvestmentNotionalCalculatorTypes.MONEY_HALF_UP);
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setCostCalculator(InvestmentNotionalCalculatorTypes.INTEGER_UNIT_HALF_UP);
		instrument.setPriceMultiplier(new BigDecimal(2000));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateNotional(security, new BigDecimal("108.9765625"), new BigDecimal(18), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("3923154"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("108.9765625"), new BigDecimal("3923154"), null);
		Assertions.assertEquals(new BigDecimal("18"), result);
	}


	@Test
	public void testCalculateNotional_INSTRUMENT_INTEGER_HALF_UP() {
		InvestmentType type = new InvestmentType();
		type.setQuantityDecimalPrecision((short) 0);
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setInvestmentType(type);
		hierarchy.setCostCalculator(InvestmentNotionalCalculatorTypes.MONEY_UNIT_HALF_UP);
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setCostCalculator(InvestmentNotionalCalculatorTypes.INTEGER_HALF_UP);
		instrument.setPriceMultiplier(new BigDecimal(2000));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateNotional(security, new BigDecimal("108.9765625"), new BigDecimal(18), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("3923156"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("108.9765625"), new BigDecimal("3923156"), null);
		Assertions.assertEquals(new BigDecimal("18"), result);
	}


	@Test
	public void testCalculateNotional_HIERARCHY_BPS_DISCOUNT_MONEY_HALF_UP() {
		InvestmentType type = new InvestmentType();
		type.setQuantityDecimalPrecision((short) 2);
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setInvestmentType(type);
		hierarchy.setCostCalculator(InvestmentNotionalCalculatorTypes.BPS_DISCOUNT_MONEY_HALF_UP);
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setPriceMultiplier(new BigDecimal("0.01"));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateNotional(security, new BigDecimal("98"), new BigDecimal(1000), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("20.00"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("98"), new BigDecimal("20.00"), null);
		Assertions.assertEquals(new BigDecimal("1000.00"), result);
	}


	@Test
	public void testCalculateNotional_INSTRUMENT_MONEY_HALF_UP_DIVIDE() {
		InvestmentType type = new InvestmentType();
		type.setQuantityDecimalPrecision((short) 2);
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setInvestmentType(type);
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setCostCalculator(InvestmentNotionalCalculatorTypes.MONEY_HALF_UP_DIVIDE);
		instrument.setPriceMultiplier(new BigDecimal(1));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateNotional(security, new BigDecimal(1_091), new BigDecimal(1_636_500_000), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("1500000.00"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal(1_091), new BigDecimal("1500000.00"), null);
		Assertions.assertEquals(new BigDecimal("1636500000.00"), result);
	}


	@Test
	public void testCalculateNotional_INSTRUMENT_INTEGER_HALF_UP_DIVIDE() {
		InvestmentType type = new InvestmentType();
		type.setQuantityDecimalPrecision((short) 0);
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setInvestmentType(type);
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setCostCalculator(InvestmentNotionalCalculatorTypes.INTEGER_HALF_UP_DIVIDE);
		instrument.setPriceMultiplier(new BigDecimal(1));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateNotional(security, new BigDecimal(1_091), new BigDecimal(1_636_500_000), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("1500000"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal(1_091), new BigDecimal("1500000"), null);
		Assertions.assertEquals(new BigDecimal("1636500000"), result);
	}


	@Test
	public void testCalculateNotional_INSTRUMENT_SFE_10YEAR_6PERCENT() {
		InvestmentType type = new InvestmentType();
		type.setQuantityDecimalPrecision((short) 2);
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setInvestmentType(type);
		hierarchy.setCostCalculator(InvestmentNotionalCalculatorTypes.MONEY_HALF_UP);
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setCostCalculator(InvestmentNotionalCalculatorTypes.SFE_10YEAR_6PERCENT);
		instrument.setPriceMultiplier(new BigDecimal(1000));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateNotional(security, new BigDecimal("94.915"), new BigDecimal(259), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("27739803.91"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("94.915"), new BigDecimal("27739803.91"), null);
		Assertions.assertEquals(new BigDecimal("259.00"), result);

		result = this.investmentCalculator.calculateNotional(security, new BigDecimal("94.875"), new BigDecimal(259), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("27656064.03"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("94.875"), new BigDecimal("27656064.03"), null);
		Assertions.assertEquals(new BigDecimal("259.00"), result);

		result = this.investmentCalculator.calculateNotional(security, new BigDecimal("95.565"), new BigDecimal(259), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("29145389.14"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("95.565"), new BigDecimal("29145389.14"), null);
		Assertions.assertEquals(new BigDecimal("259.00"), result);
	}


	@Test
	public void testCalculateNotional_TIPS() {
		InvestmentType type = new InvestmentType();
		type.setQuantityDecimalPrecision((short) 2);
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setInvestmentType(type);
		hierarchy.setIndexRatioAdjusted(true);
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setPriceMultiplier(new BigDecimal("0.01"));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateNotional(security, new BigDecimal("94.915"), new BigDecimal(100000), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("94915.00"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("94.915"), new BigDecimal("94915.00"), null);
		Assertions.assertEquals(new BigDecimal("100000.00"), result);

		result = this.investmentCalculator.calculateNotional(security, new BigDecimal("94.915"), new BigDecimal(100000), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("94915.00"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("94.915"), new BigDecimal("94915.00"), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("100000.00"), result);

		result = this.investmentCalculator.calculateNotional(security, new BigDecimal("94.915"), new BigDecimal(100000), new BigDecimal("1.1"));
		Assertions.assertEquals(new BigDecimal("104406.50"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("94.915"), new BigDecimal("104406.50"), new BigDecimal("1.1"));
		Assertions.assertEquals(new BigDecimal("100000.00"), result);
	}


	@Test
	public void testCalculateNotional_OLD_BRITISH_LINKER() {
		InvestmentType type = new InvestmentType();
		type.setQuantityDecimalPrecision((short) 2);
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setInvestmentType(type);
		hierarchy.setIndexRatioAdjusted(true);
		hierarchy.setNotionalMultiplierForPriceNotUsed(true); // make sure we don't multiply again
		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setPriceMultiplier(new BigDecimal("0.01"));
		instrument.setHierarchy(hierarchy);
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(instrument);

		BigDecimal result = this.investmentCalculator.calculateNotional(security, new BigDecimal("94.915"), new BigDecimal(100000), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("94915.00"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("94.915"), new BigDecimal("94915.00"), null);
		Assertions.assertEquals(new BigDecimal("100000.00"), result);

		result = this.investmentCalculator.calculateNotional(security, new BigDecimal("94.915"), new BigDecimal(100000), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("94915.00"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("94.915"), new BigDecimal("94915.00"), BigDecimal.ONE);
		Assertions.assertEquals(new BigDecimal("100000.00"), result);

		result = this.investmentCalculator.calculateNotional(security, new BigDecimal("94.915"), new BigDecimal(100000), new BigDecimal("1.1"));
		Assertions.assertEquals(new BigDecimal("94915.00"), result);

		result = this.investmentCalculator.calculateQuantityFromNotional(security, new BigDecimal("94.915"), new BigDecimal("94915.00"), new BigDecimal("1.1"));
		Assertions.assertEquals(new BigDecimal("100000.00"), result);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// Interest Calculation Tests
	//////////////////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalculateInterestPayment_ACT360() {
		// quarterly frequency
		BigDecimal result = this.investmentCalculator.calculateInterestPayment(new BigDecimal("800000"), new BigDecimal("0.499250"), DateUtils.toDate("07/15/2011"), DateUtils.toDate("10/17/2011"),
				null, DayCountConventions.ACTUAL_THREESIXTY, CouponFrequencies.QUARTERLY);
		Assertions.assertEquals(new BigDecimal("1042.88"), result);

		// same test but for a different method signature
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("89233P4V5", null);

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "QUARTERLY", "Actual/360"));

		InvestmentSecurityEvent couponPayment = this.investmentSecurityEventService.getInvestmentSecurityEventForAccrualEndDate(security.getId(), InvestmentSecurityEventType.CASH_COUPON_PAYMENT, DateUtils.toDate("10/16/2011"));
		result = this.investmentCalculator.calculateInterestPayment(couponPayment, new BigDecimal("800000"), BigDecimal.ONE, null);
		Assertions.assertEquals(new BigDecimal("1042.88"), result);
	}


	@Test
	public void testCalculateInterestPayment_ACT360_iBoxx() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("9O3S5", null);
		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "QUARTERLY", "Actual/360"));

		// First Coupon Event for iBoxx Security
		InvestmentSecurityEvent interestLeg = this.investmentSecurityEventService.getInvestmentSecurityEventForAccrualEndDate(security.getId(), InvestmentSecurityEventType.INTEREST_LEG_PAYMENT, DateUtils.toDate("09/19/2016"));
		BigDecimal result = this.investmentCalculator.calculateInterestPayment(interestLeg, new BigDecimal("13700000.00"), BigDecimal.ONE, null);
		Assertions.assertEquals(new BigDecimal("-22638.18"), result);

		// Second Coupon Event for iBoxx Security
		interestLeg = this.investmentSecurityEventService.getInvestmentSecurityEventForAccrualEndDate(security.getId(), InvestmentSecurityEventType.INTEREST_LEG_PAYMENT, DateUtils.toDate("12/19/2016"));
		result = this.investmentCalculator.calculateInterestPayment(interestLeg, new BigDecimal("13700000.00"), BigDecimal.ONE, null);
		Assertions.assertEquals(new BigDecimal("-29682.20"), result);
	}


	@Test
	public void testCalculateInterestPayment_ACT360_FromPeriodStartEOD() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("89233P4V5", null);

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "QUARTERLY", "Actual/360"));

		InvestmentSecurityEvent couponPayment = this.investmentSecurityEventService.getInvestmentSecurityEventForAccrualEndDate(security.getId(), InvestmentSecurityEventType.CASH_COUPON_PAYMENT, DateUtils.toDate("10/16/2011"));
		BigDecimal result = this.investmentCalculator.calculateInterestPayment(couponPayment, new BigDecimal("800000"), BigDecimal.ONE, null);
		Assertions.assertEquals(new BigDecimal("1042.88"), result);

		// changing start date convention should not affect payment
		couponPayment.getSecurity().getInstrument().getHierarchy().setAccrualDateCalculator(AccrualDateCalculators.DO_NOT_ADJUST_FROM_TRANSACTION);
		result = this.investmentCalculator.calculateInterestPayment(couponPayment, new BigDecimal("800000"), BigDecimal.ONE, null);
		Assertions.assertEquals(new BigDecimal("1042.88"), result);
	}


	@Test
	public void testCalculateInterestPayment_ACT_ACT() {
		// 280907BC9
		BigDecimal result = this.investmentCalculator.calculateInterestPayment(new BigDecimal("150000"), new BigDecimal("2.55733"), DateUtils.toDate("12/22/2011"), DateUtils.toDate("01/22/2012"),
				null, DayCountConventions.ACTUAL_ACTUAL, CouponFrequencies.MONTHLY);
		Assertions.assertEquals(new BigDecimal("319.67"), result);

		// same test but for a different method signature
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("280907BC9", null);

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "MONTHLY", "Actual/Actual"));

		InvestmentSecurityEvent couponPayment = this.investmentSecurityEventService.getInvestmentSecurityEventForAccrualEndDate(security.getId(), InvestmentSecurityEventType.CASH_COUPON_PAYMENT, DateUtils.toDate("01/22/2012"));
		result = this.investmentCalculator.calculateInterestPayment(couponPayment, new BigDecimal("150000"), BigDecimal.ONE, null);
		Assertions.assertEquals(new BigDecimal("319.67"), result);
	}


	@Test
	public void testCalculateInterestPayment_Actual365_PartialPeriod() {
		// TRS: 35677760
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("35677760", null);
		InvestmentSecurityEvent event = new InvestmentSecurityEvent();
		event.setSecurity(security);
		event.setAfterEventValue(new BigDecimal("0.21"));
		event.setAccrualStartDate(DateUtils.toDate("01/14/2014"));
		event.setAccrualEndDate(DateUtils.toDate("02/11/2014"));
		event.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.INTEREST_LEG_PAYMENT));

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "BULLET", "Actual/365"));

		BigDecimal result = this.investmentCalculator.calculateInterestPayment(event, new BigDecimal("200000"), BigDecimal.ONE, DateUtils.toDate("02/07/2014"));
		Assertions.assertEquals(new BigDecimal("-4.60"), result);

		// now test full period accrual for a different lot
		result = this.investmentCalculator.calculateInterestPayment(event, new BigDecimal("329357.72"), BigDecimal.ONE, DateUtils.toDate("01/14/2014"));
		Assertions.assertEquals(new BigDecimal("-53.06"), result);

		result = this.investmentCalculator.calculateInterestPayment(event, new BigDecimal("329357.72"), BigDecimal.ONE, null);
		Assertions.assertEquals(new BigDecimal("-53.06"), result);
	}


	@Test
	public void testCalculateAccruedInterest_ACT360() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("89233P4V5", null);

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "QUARTERLY", "Actual/360"));

		// payment is on Monday the 17th; new period starts End Of Day 10/17/2011
		BigDecimal result = calculateAccruedInterest(security, new BigDecimal("800000"), "10/14/2011");
		Assertions.assertEquals(new BigDecimal("1009.59"), result);

		result = calculateAccruedInterest(security, new BigDecimal("800000"), "10/15/2011");
		Assertions.assertEquals(new BigDecimal("1020.69"), result);

		result = calculateAccruedInterest(security, new BigDecimal("800000"), "10/16/2011");
		Assertions.assertEquals(new BigDecimal("1031.78"), result);

		// on 10/17/2011 excluding accrual on this date - new period starts on 10/17/2011
		result = calculateAccruedInterest(security, new BigDecimal("800000"), "10/17/2011");
		Assertions.assertEquals(new BigDecimal("0.00"), result);

		// on 10/17/2011 including accrual on this date - new period starts on 10/17/2011
		result = calculateAccruedInterest(security, new BigDecimal("800000"), "10/18/2011");
		Assertions.assertEquals(new BigDecimal("14.51"), result);
	}


	@Test
	public void testCalculateAccruedInterest_ACT360_iBoxx() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("9O3S5", null);
		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "QUARTERLY", "Actual/360"));

		// Valuation Date = Trade Date: Accrual is Opposite of Opening Trade Amount
		BigDecimal result = calculateAccruedInterest(security, new BigDecimal("13700000.00"), "08/27/2016");
		Assertions.assertEquals(new BigDecimal("-16732.57"), result);

		// Valuation Date = One Day Before Accrual End Date: Full Coupon Amount
		result = calculateAccruedInterest(security, new BigDecimal("13700000.00"), "09/20/2016");
		Assertions.assertEquals(new BigDecimal("-22638.18"), result);

		// Valuation Date = Accrual End Date: Accrual Using New Period Rate
		result = calculateAccruedInterest(security, new BigDecimal("13700000.00"), "09/21/2016");
		Assertions.assertEquals(new BigDecimal("-326.18"), result);

		// Valuation Date = One Day After Accrual End Date: Accrual Using New Period Rate
		result = calculateAccruedInterest(security, new BigDecimal("13700000.00"), "09/22/2016");
		Assertions.assertEquals(new BigDecimal("-652.36"), result);
	}


	@Test
	public void testCalculateAccruedInterest_ActAct_with_Partial_First_Period() {
		// first partial period payment is combined with the next period payment
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("DE0001030542", null);

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "ANNUAL", "Actual/Actual"));

		// short first coupon period starting on 03/23/2012
		BigDecimal indexRatio = new BigDecimal("0.99946");
		BigDecimal face = new BigDecimal("105000");
		BigDecimal result = calculateAccruedInterest(security, face, indexRatio, "04/10/2012");
		Assertions.assertEquals(new BigDecimal("5.16"), result);
	}


	@Test
	public void testCalculateAccruedInterest_ActAct_with_Partial_First_Period_Lumped_Into_Next_Period_Payment() {
		// first partial period payment is combined with the next period payment
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("DE0001030542-LONG", null);

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "ANNUAL", "Actual/Actual"));

		// short first coupon period starting on 03/23/2012
		BigDecimal indexRatio = new BigDecimal("0.99946");
		BigDecimal face = new BigDecimal("105000");
		BigDecimal result = calculateAccruedInterest(security, face, indexRatio, "04/10/2012");
		Assertions.assertEquals(new BigDecimal("5.16"), result);

		// short first coupon period starting on 03/23/2012 lumped into next period
		indexRatio = new BigDecimal("1.00042");
		face = new BigDecimal("399000");
		result = calculateAccruedInterest(security, face, indexRatio, "04/18/2012");
		Assertions.assertEquals(new BigDecimal("28.37"), result);
	}


	@Test
	public void testCalculateAccruedInterest_British_Linkers_Gilts() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00B0V3WQ75", null);

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "SEMIANNUAL", "Actual/Actual"));

		// on 11/10/2011 excluding accrual on this date
		BigDecimal indexRatio = new BigDecimal("1.22153");
		BigDecimal face = new BigDecimal("1000000");
		BigDecimal result = calculateAccruedInterest(security, face, indexRatio, "11/10/2011");
		Assertions.assertEquals(new BigDecimal("7136.66"), result);

		// on 11/18/2011 excluding accrual on this date
		indexRatio = new BigDecimal("1.224");
		result = calculateAccruedInterest(security, face, indexRatio, "11/18/2011");
		// NOTE: the day before Ex Date we're entitled to full dividend payment: full amount receivable
		//       need to accrue negative coupon until end of period: if position is closed after Ex but before period end, then will have to pay back overpaid amount
		Assertions.assertEquals(new BigDecimal("-166.30"), result);
	}


	@Test
	public void testCalculateAccruedInterest_British_Linkers_Gilts_with_Partial_First_Period() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00B3Y1JG82", null);

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "SEMIANNUAL", "Actual/Actual"));

		// on 12/22/2011 including accrual on this date but excluding on bond start date
		BigDecimal indexRatio = new BigDecimal("1.00232");
		BigDecimal face = new BigDecimal("1000000");
		BigDecimal result = calculateAccruedInterest(security, face, indexRatio, "12/22/2011");
		Assertions.assertEquals(new BigDecimal("99.82"), result);
	}


	@Test
	public void testCalculateAccruedInterest_Old_British_Linkers_Gilts() {
		// custom rounding to 4 decimals in the middle of calculation rounded down
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB0009036715", null);
		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "SEMIANNUAL", "Actual/Actual"));

		// on 12/28/2011 excluding accrual on this date
		BigDecimal indexRatio = new BigDecimal("2.63672634");
		BigDecimal face = new BigDecimal("1000000");
		BigDecimal result = calculateAccruedInterest(security, face, indexRatio, "12/28/2011");
		Assertions.assertEquals(new BigDecimal("24002.75"), result);

		// on 08/08/2011 excluding accrual on this date
		indexRatio = new BigDecimal("2.56049446");
		result = calculateAccruedInterest(security, face, indexRatio, "08/08/2011");
		Assertions.assertEquals(new BigDecimal("-1414.63"), result);

		// verify rounding down
		security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB0008932666", null);
		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "SEMIANNUAL", "Actual/Actual"));

		face = new BigDecimal("1000000000");
		indexRatio = new BigDecimal("1.74093264");
		result = calculateAccruedInterest(security, face, indexRatio, "12/28/2011");
		Assertions.assertEquals(new BigDecimal("31027467.39"), result);
	}


	@Test
	public void testCalculateAccruedInterest_Cleared_CDS_No_Accrual_From_Day_Before_Ex() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("XY1904D17U0500XXI", null);

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "QUARTERLY", "Actual/360"));

		BigDecimal notional = new BigDecimal("4171000");
		BigDecimal result = calculateAccruedInterest(security, notional, "06/17/2015");
		Assertions.assertEquals(new BigDecimal("52137.50"), result);

		result = calculateAccruedInterest(security, notional, "06/18/2015");
		Assertions.assertEquals(new BigDecimal("52716.81"), result);

		result = calculateAccruedInterest(security, notional, "06/19/2015");
		Assertions.assertEquals(new BigDecimal("0.00"), result);

		result = calculateAccruedInterest(security, notional, "06/20/2015");
		Assertions.assertEquals(new BigDecimal("0.00"), result);

		result = calculateAccruedInterest(security, notional, "06/21/2015");
		Assertions.assertEquals(new BigDecimal("0.00"), result);

		result = calculateAccruedInterest(security, notional, "06/22/2015");
		Assertions.assertEquals(new BigDecimal("579.31"), result);
	}


	private BigDecimal calculateAccruedInterest(InvestmentSecurity security, BigDecimal accrualBasis, String accrueUpToDate) {
		return calculateAccruedInterest(security, accrualBasis, null, accrueUpToDate);
	}


	private BigDecimal calculateAccruedInterest(InvestmentSecurity security, BigDecimal accrualBasis, BigDecimal notionalMultiplier, String accrueUpToDate) {
		return this.investmentCalculator.calculateAccruedInterest(security, AccrualBasis.of(accrualBasis), notionalMultiplier, AccrualDates.ofUpToDate(DateUtils.toDate(accrueUpToDate)));
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// Settlement Date Tests


	@Test
	public void testCalculateSettlementDate() {
		Date transactionDate = DateUtils.toDate("3/12/2012");
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(1, "IBM", InvestmentType.STOCKS);

		Date settlementDate = this.investmentCalculator.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/12/2012"), settlementDate);

		security.getInstrument().getHierarchy().getInvestmentType().setDaysToSettle(1);
		settlementDate = this.investmentCalculator.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/13/2012"), settlementDate);

		security.getInstrument().getHierarchy().setDaysToSettle(2);
		settlementDate = this.investmentCalculator.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/14/2012"), settlementDate);

		security.getInstrument().setDaysToSettle(4);
		settlementDate = this.investmentCalculator.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/16/2012"), settlementDate);

		// test Settlement Calendar Override
		security.getInstrument().setDaysToSettle(5);
		settlementDate = this.investmentCalculator.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/19/2012"), settlementDate);
		security.getInstrument().setSettlementCalendar(setupCalendar((short) 1));
		settlementDate = this.investmentCalculator.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/19/2012"), settlementDate);
		security.getInstrument().setSettlementCalendar(setupCalendar((short) 2)); // 19th is a holiday
		settlementDate = this.investmentCalculator.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/20/2012"), settlementDate);
		security.getInstrument().setSettlementCalendar(null);

		// cannot settle before Issue Date for security (common for corporate bonds)
		security.setStartDate(DateUtils.toDate("3/20/2012"));
		settlementDate = this.investmentCalculator.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/20/2012"), settlementDate);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// Trade Date Tests


	@Test
	public void testCalculateTradeDate_NoCalendar() {
		InvestmentSecurity security = setupSecurity(null);
		for (Date date : getTestDates()) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, date);
			Assertions.assertEquals(DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}

		security = setupSecurity(new InvestmentExchange());
		for (Date date : getTestDates()) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, date);
			Assertions.assertEquals(DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_NoTimeZone() {
		Calendar calendar = setupCalendar((short) 1);
		InvestmentExchange exchange = new InvestmentExchange();
		exchange.setCalendar(calendar);
		InvestmentSecurity security = setupSecurity(exchange);
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalculateTradeDate_WithoutTimeLA() {
		InvestmentSecurity security = setupSecurity(getExchange("America/Los_Angeles", false));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithoutTimeChicago() {
		InvestmentSecurity security = setupSecurity(getExchange("America/Chicago", false));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithoutTimeNewYork() {
		InvestmentSecurity security = setupSecurity(getExchange("America/New_York", false));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithoutTimeSydney() {
		InvestmentSecurity security = setupSecurity(getExchange("Australia/Sydney", false));
		String[] result = new String[]{"10/10/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithoutTimeJapan() {
		InvestmentSecurity security = setupSecurity(getExchange("Japan", false));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithoutTimeLondon() {
		InvestmentSecurity security = setupSecurity(getExchange("Europe/London", false));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}

	//////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalculateTradeDate_WithTimeLA() {
		InvestmentSecurity security = setupSecurity(getExchange("America/Los_Angeles", true));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithTimeChicago() {
		InvestmentSecurity security = setupSecurity(getExchange("America/Chicago", true));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithTimeNewYork() {
		InvestmentSecurity security = setupSecurity(getExchange("America/New_York", true));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithTimeSydney() {
		InvestmentSecurity security = setupSecurity(getExchange("Australia/Sydney", true));
		String[] result = new String[]{"10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithTimeJapan() {
		InvestmentSecurity security = setupSecurity(getExchange("Japan", true));
		String[] result = new String[]{"10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithTimeLondon() {
		InvestmentSecurity security = setupSecurity(getExchange("Europe/London", true));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.investmentCalculator.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}

	//////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalculateDirtyPrice() {
		Date date = DateUtils.toDate("12/1/2011");

		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(1, "ABC", InvestmentType.BONDS);
		SystemBean notionalMultiplierRetrieverBean = new SystemBean();
		notionalMultiplierRetrieverBean.setName("Test Notional Multiplier Retriever");
		security.getInstrument().getHierarchy().setNotionalMultiplierRetriever(notionalMultiplierRetrieverBean);

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "SEMIANNUAL", "Actual/Actual"));

		InvestmentSecurityEventService investmentSecurityEventServiceMock = Mockito.mock(InvestmentSecurityEventService.class);
		Mockito.when(investmentSecurityEventServiceMock.getInvestmentSecurityEventForAccrualEndDate(ArgumentMatchers.anyInt(), ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(
				getCouponEvent(false, security, "SEMIANNUAL"));
		((InvestmentCalculatorImpl) this.investmentCalculator).setInvestmentSecurityEventService(investmentSecurityEventServiceMock);

		SystemBeanService systemBeanServiceMock = Mockito.mock(SystemBeanService.class);
		Mockito.when(systemBeanServiceMock.getBeanInstance(ArgumentMatchers.any())).thenReturn(new TestNotionalMultiplierRetriever(new BigDecimal("1.14315")));
		((InvestmentCalculatorImpl) this.investmentCalculator).setSystemBeanService(systemBeanServiceMock);

		BigDecimal result = this.investmentCalculator.calculateDirtyPrice(security, BigDecimal.ZERO, new BigDecimal("112.31640625"), date);
		Assertions.assertEquals(new BigDecimal("129.258075076685398"), MathUtils.round(result, 15));

		date = DateUtils.toDate("01/04/2012");
		Mockito.when(systemBeanServiceMock.getBeanInstance(ArgumentMatchers.any())).thenReturn(new TestNotionalMultiplierRetriever(new BigDecimal("1.1407")));
		result = this.investmentCalculator.calculateDirtyPrice(security, BigDecimal.ZERO, new BigDecimal("112.18359375"), date);
		Assertions.assertEquals(new BigDecimal("129.040331368621857"), MathUtils.round(result, 15));

		result = this.investmentCalculator.calculateDirtyPrice(security, BigDecimal.ZERO, BigDecimal.ZERO, date);
		Assertions.assertEquals(MathUtils.round(BigDecimal.ZERO, 15), MathUtils.round(result, 15));
	}


	@Test
	public void testCalculateDirtyPrice_CDS() {
		Date date = DateUtils.toDate("12/1/2011");

		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(1, "ABC", InvestmentType.SWAPS);
		security.getInstrument().setCostCalculator(InvestmentNotionalCalculatorTypes.BPS_DISCOUNT_MONEY_HALF_UP);
		security.getInstrument().setAccrualDateCalculator(AccrualDateCalculators.DO_NOT_ADJUST);
		security.getInstrument().getHierarchy().setAccrualSignCalculator(AccrualSignCalculators.NEGATE);
		security.getInstrument().setPriceMultiplier(BigDecimal.valueOf(0.01));

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "SEMIANNUAL", "Actual/Actual"));

		InvestmentSecurityEventService investmentSecurityEventServiceMock = Mockito.mock(InvestmentSecurityEventService.class);
		Mockito.when(investmentSecurityEventServiceMock.getInvestmentSecurityEventForAccrualEndDate(ArgumentMatchers.anyInt(), ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(
				getCouponEvent(false, security, "SEMIANNUAL"));
		((InvestmentCalculatorImpl) this.investmentCalculator).setInvestmentSecurityEventService(investmentSecurityEventServiceMock);

		BigDecimal result = this.investmentCalculator.calculateDirtyPrice(security, BigDecimal.ZERO, new BigDecimal("112.31640625"), date);
		Assertions.assertEquals(new BigDecimal("86.928158967000000"), MathUtils.round(result, 15));

		// Try again, but for a Short Position - Dirty Price Should go down
		result = this.investmentCalculator.calculateDirtyPrice(security, BigDecimal.valueOf(-1000000000), new BigDecimal("112.31640625"), date);
		Assertions.assertEquals(new BigDecimal("113.071841033000000"), MathUtils.round(result, 15));
	}


	@Test
	public void testCalculateDirtyPrice_CDS_Cleared() {
		// Actual Calc From 3/10/2015 for XI2301D19U0100XXI
		Date date = DateUtils.toDate("12/10/2011");

		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(1, "ABC", InvestmentType.SWAPS);
		security.getInstrument().setCostCalculator(InvestmentNotionalCalculatorTypes.BPS_DISCOUNT_MONEY_HALF_UP);
		security.getInstrument().setAccrualDateCalculator(AccrualDateCalculators.DO_NOT_ADJUST);
		security.getInstrument().getHierarchy().setAccrualSignCalculator(AccrualSignCalculators.NEGATE);
		security.getInstrument().setPriceMultiplier(BigDecimal.valueOf(0.01));

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "QUARTERLY", "Actual/360"));

		InvestmentSecurityEventService investmentSecurityEventServiceMock = Mockito.mock(InvestmentSecurityEventService.class);
		Mockito.when(investmentSecurityEventServiceMock.getInvestmentSecurityEventForAccrualEndDate(ArgumentMatchers.anyInt(), ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(
				getCouponEvent(false, security, "QUARTERLY"));
		((InvestmentCalculatorImpl) this.investmentCalculator).setInvestmentSecurityEventService(investmentSecurityEventServiceMock);

		BigDecimal result = this.investmentCalculator.calculateDirtyPrice(security, BigDecimal.ZERO, new BigDecimal("101.5988"), date);
		Assertions.assertEquals(new BigDecimal("98.1817556"), MathUtils.round(result, 7));

		// Try again for short position
		result = this.investmentCalculator.calculateDirtyPrice(security, BigDecimal.valueOf(-1000000000), new BigDecimal("101.5988"), date);
		Assertions.assertEquals(new BigDecimal("101.8182444"), MathUtils.round(result, 7));
	}


	@Test
	public void testCalculateDirtyPrice_IRS_Cleared() {
		// Actual Test from EUR6E-20130930-20230930-2.123 on 3/10/2015
		Date date = DateUtils.toDate("03/10/2012");

		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(1, "ABC", InvestmentType.SWAPS);
		security.getInstrument().setCostCalculator(InvestmentNotionalCalculatorTypes.BPS_DISCOUNT_MONEY_HALF_UP_NEGATIVE_QTY);
		security.getInstrument().setAccrualDateCalculator(AccrualDateCalculators.DO_NOT_ADJUST);
		security.getInstrument().getHierarchy().setAccrualSignCalculator(AccrualSignCalculators.NEGATE_NON_COUPON_FREQUENCY);
		security.getInstrument().setPriceMultiplier(BigDecimal.valueOf(0.01));

		security.getInstrument().getHierarchy().setAccrualSecurityEventType2(new InvestmentSecurityEventType());
		security.getInstrument().getHierarchy().getAccrualSecurityEventType2().setName(InvestmentSecurityEventType.FLOATING_LEG_PAYMENT);
		security.getInstrument().getHierarchy().setAccrualMethod2(AccrualMethods.DAYCOUNT_COMPOUNDED);

		@SuppressWarnings("unchecked")
		SystemColumnValueHandler systemColumnValueService = Mockito.mock(SystemColumnValueHandler.class);
		Mockito.when(systemColumnValueService.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY, false))
				.thenReturn("ANNUAL");
		Mockito.when(systemColumnValueService.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY2, false))
				.thenReturn("SEMIANNUAL");
		Mockito.when(systemColumnValueService.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION, false))
				.thenReturn("30/360");
		Mockito.when(systemColumnValueService.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION2, false))
				.thenReturn("Actual/360");

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(systemColumnValueService);
		this.investmentAccrualDayCountCompoundedCalculator.setSystemColumnValueHandler(systemColumnValueService);

		InvestmentSecurityEventService investmentSecurityEventServiceMock = Mockito.mock(InvestmentSecurityEventService.class);
		Mockito.when(investmentSecurityEventServiceMock.getInvestmentSecurityEventForAccrualEndDate(ArgumentMatchers.anyInt(), ArgumentMatchers.anyString(), ArgumentMatchers.any())).
				thenReturn(getCouponEvent(false, security, "ANNUAL")
				).thenReturn(getFloatingLegEvent(security, "SEMIANNUAL")).thenReturn(
				getCouponEvent(false, security, "ANNUAL")
		).thenReturn(getFloatingLegEvent(security, "SEMIANNUAL"));
		Mockito.when(investmentSecurityEventServiceMock.getInvestmentSecurityEventDetailListByEvent(ArgumentMatchers.anyInt())).thenReturn(null);
		((InvestmentCalculatorImpl) this.investmentCalculator).setInvestmentSecurityEventService(investmentSecurityEventServiceMock);
		this.investmentAccrualDayCountCompoundedCalculator.setInvestmentSecurityEventService(investmentSecurityEventServiceMock);

		BigDecimal result = this.investmentCalculator.calculateDirtyPrice(security, BigDecimal.ZERO, new BigDecimal("113.3433"), date);
		Assertions.assertEquals(new BigDecimal("114.2216"), MathUtils.round(result, 4));

		// Try again for Short Position
		result = this.investmentCalculator.calculateDirtyPrice(security, BigDecimal.valueOf(-1000000000), new BigDecimal("113.3433"), date);
		Assertions.assertEquals(new BigDecimal("87.5350"), MathUtils.round(result, 4));
	}


	@Test
	public void testCalculateDirtyPrice_BritishLinkers() {
		Date date = DateUtils.toDate("01/04/2012");

		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(1, "ABC", InvestmentType.BONDS);
		SystemBean notionalMultiplierRetrieverBean = new SystemBean();
		notionalMultiplierRetrieverBean.setName("Test Notional Multiplier Retriever");
		security.getInstrument().getHierarchy().setNotionalMultiplierRetriever(notionalMultiplierRetrieverBean);
		security.getInstrument().getHierarchy().setNotionalMultiplierForPriceNotUsed(true);

		this.investmentAccrualDayCountCalculator.setSystemColumnValueHandler(getSystemColumnValueHandler(security, "SEMIANNUAL", "Actual/Actual"));

		InvestmentSecurityEventService investmentSecurityEventServiceMock = Mockito.mock(InvestmentSecurityEventService.class);
		Mockito.when(investmentSecurityEventServiceMock.getInvestmentSecurityEventForAccrualEndDate(ArgumentMatchers.anyInt(), ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(
				getCouponEvent(true, security, null));
		((InvestmentCalculatorImpl) this.investmentCalculator).setInvestmentSecurityEventService(investmentSecurityEventServiceMock);

		SystemBeanService systemBeanServiceMock = Mockito.mock(SystemBeanService.class);
		Mockito.when(systemBeanServiceMock.getBeanInstance(ArgumentMatchers.any())).thenReturn(new TestNotionalMultiplierRetriever(new BigDecimal("2.84575159")));
		((InvestmentCalculatorImpl) this.investmentCalculator).setSystemBeanService(systemBeanServiceMock);

		BigDecimal result = this.investmentCalculator.calculateDirtyPrice(security, BigDecimal.ZERO, new BigDecimal("362.798"), date);
		Assertions.assertEquals(BigDecimal.valueOf(364.353056), MathUtils.round(result, 6));
	}


	private InvestmentSecurityEvent getCouponEvent(boolean britishLinker, InvestmentSecurity security, String frequency) {
		InvestmentSecurityEvent event = new InvestmentSecurityEvent();
		InvestmentSecurityEventType type = new InvestmentSecurityEventType();
		type.setName(InvestmentSecurityEventType.CASH_COUPON_PAYMENT);
		event.setId(1);
		event.setType(type);
		event.setSecurity(security);
		if (!britishLinker) {
			if (StringUtils.isEqual("QUARTERLY", frequency)) {
				event.setAccrualStartDate(DateUtils.toDate("09/22/2011"));
				event.setAccrualEndDate(DateUtils.toDate("12/22/2011"));
				event.setExDate(DateUtils.toDate("12/23/2011"));
				event.setPaymentDate(DateUtils.toDate("12/22/2014"));
				event.setAfterEventValue(BigDecimal.ONE);
			}
			else if (StringUtils.isEqual("ANNUAL", frequency)) {
				event.setAccrualStartDate(DateUtils.toDate("09/30/2011"));
				event.setAccrualEndDate(DateUtils.toDate("09/30/2012"));
				event.setExDate(DateUtils.toDate("09/30/2012"));
				event.setPaymentDate(DateUtils.toDate("09/30/2012"));
				event.setAfterEventValue(BigDecimal.valueOf(2.123));
			}
			else {
				event.setAccrualStartDate(DateUtils.toDate("07/15/2011"));
				event.setAccrualEndDate(DateUtils.toDate("01/15/2012"));
				event.setExDate(DateUtils.toDate("01/16/2012"));
				event.setPaymentDate(DateUtils.toDate("01/15/2012"));
				event.setAfterEventValue(BigDecimal.valueOf(2));
			}
		}
		else {
			event.setAccrualStartDate(DateUtils.toDate("10/16/2011"));
			event.setAccrualEndDate(DateUtils.toDate("04/16/2012"));
			event.setExDate(DateUtils.toDate("04/09/2012"));
			event.setPaymentDate(DateUtils.toDate("04/16/2012"));
			event.setAfterEventValue(BigDecimal.valueOf(2.5));
		}
		return event;
	}


	private InvestmentSecurityEvent getFloatingLegEvent(InvestmentSecurity security, String frequency) {
		InvestmentSecurityEvent event = new InvestmentSecurityEvent();
		InvestmentSecurityEventType type = new InvestmentSecurityEventType();
		type.setName(InvestmentSecurityEventType.FLOATING_LEG_PAYMENT);
		event.setId(2);
		event.setType(type);
		event.setSecurity(security);

		if (StringUtils.isEqual("QUARTERLY", frequency)) {
			event.setAccrualStartDate(DateUtils.toDate("09/21/2011"));
			event.setAccrualEndDate(DateUtils.toDate("12/21/2011"));
			event.setExDate(DateUtils.toDate("12/22/2011"));
			event.setPaymentDate(DateUtils.toDate("12/22/2014"));
			event.setAfterEventValue(BigDecimal.ONE);
		}
		else if (StringUtils.isEqual("ANNUAL", frequency)) {
			event.setAccrualStartDate(DateUtils.toDate("09/30/2011"));
			event.setAccrualEndDate(DateUtils.toDate("09/30/2012"));
			event.setExDate(DateUtils.toDate("09/30/2012"));
			event.setPaymentDate(DateUtils.toDate("09/30/2012"));
			event.setAfterEventValue(BigDecimal.valueOf(0.145000));
		}
		else {
			event.setAccrualStartDate(DateUtils.toDate("09/30/2011"));
			event.setAccrualEndDate(DateUtils.toDate("03/31/2012"));
			event.setExDate(DateUtils.toDate("03/31/2012"));
			event.setPaymentDate(DateUtils.toDate("03/31/2012"));
			event.setAfterEventValue(BigDecimal.valueOf(0.145));
		}

		return event;
	}

	//////////////////////////////////////////////////////////////

	private Date[] testDates;


	private Date[] getTestDates() {
		if (this.testDates != null) {
			return this.testDates;
		}
		this.testDates = new Date[7];
		String localTimeZone = "America/Chicago";
		java.util.Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(localTimeZone));
		cal.set(java.util.Calendar.YEAR, 2011);
		cal.set(java.util.Calendar.MONTH, 9);
		cal.set(java.util.Calendar.DAY_OF_MONTH, 10);
		cal.set(java.util.Calendar.MINUTE, 0);
		cal.set(java.util.Calendar.SECOND, 0);
		cal.set(java.util.Calendar.MILLISECOND, 0);

		// 4 Hour increments from 4 am on 10/10 to 12 midnight 11/11
		cal.set(java.util.Calendar.HOUR_OF_DAY, 4);
		this.testDates[0] = cal.getTime();

		cal.set(java.util.Calendar.HOUR_OF_DAY, 8);
		this.testDates[1] = cal.getTime();

		cal.set(java.util.Calendar.HOUR_OF_DAY, 12);
		this.testDates[2] = cal.getTime();

		cal.set(java.util.Calendar.HOUR_OF_DAY, 16);
		this.testDates[3] = cal.getTime();

		cal.set(java.util.Calendar.HOUR_OF_DAY, 20);
		this.testDates[4] = cal.getTime();

		cal.set(java.util.Calendar.DAY_OF_MONTH, 11);
		cal.set(java.util.Calendar.HOUR_OF_DAY, 0);
		this.testDates[5] = cal.getTime();

		// Saturday
		cal.set(java.util.Calendar.DAY_OF_MONTH, 15);
		cal.set(java.util.Calendar.HOUR_OF_DAY, 9);
		this.testDates[6] = cal.getTime();
		return this.testDates;
	}


	public Calendar setupCalendar(short calendarId) {
		short yr = 2011;
		if (this.calendarSetupService.getCalendarYear(yr) == null) {
			this.calendarSetupService.saveCalendarYearForYear(yr);
		}
		return this.calendarSetupService.getCalendar(calendarId);
	}


	private InvestmentSecurity setupSecurity(InvestmentExchange exchange) {
		InvestmentInstrument i = new InvestmentInstrument();
		i.setExchange(exchange);
		InvestmentSecurity s = new InvestmentSecurity();
		s.setInstrument(i);
		return s;
	}


	private InvestmentExchange getExchange(String timeZone, boolean includeClosingTime) {
		Calendar calendar = setupCalendar((short) 1);
		CalendarTimeZone tz = new CalendarTimeZone();
		tz.setName(timeZone);
		InvestmentExchange exchange = new InvestmentExchange();
		exchange.setTimeZone(tz);
		exchange.setCalendar(calendar);
		if (includeClosingTime) {
			exchange.setCloseTime(Time.parse("17:00:00")); // 5 pm
		}
		return exchange;
	}


	private SystemColumnValueHandler getSystemColumnValueHandler(InvestmentSecurity security, String couponFrequency, String dayCountConvention) {
		@SuppressWarnings("unchecked")
		SystemColumnValueHandler systemColumnValueService = Mockito.mock(SystemColumnValueHandler.class);
		Mockito.when(systemColumnValueService.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY, false))
				.thenReturn(couponFrequency);
		Mockito.when(systemColumnValueService.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION, false))
				.thenReturn(dayCountConvention);
		return systemColumnValueService;
	}
}
