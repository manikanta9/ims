package com.clifton.investment.instrument.upload;

import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnGroup;
import com.clifton.system.schema.column.SystemColumnService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
@ContextConfiguration
public class InvestmentInstrumentUploadServiceImplTests<T extends SystemColumn> {

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;
	@Resource
	private InvestmentInstrumentUploadService investmentInstrumentUploadService;
	@Resource
	private InvestmentSetupService investmentSetupService;
	@Resource
	private SystemColumnService systemColumnService;
	@Resource
	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////////

	private static final int VALID_SHEET_INDEX = 0;
	private static final int INVALID_SHEET_INDEX = 1;  // Valid only if trading ccy is selected on the upload bean

	private static final short HIERARCHY_BENCHMARKS_PARENT = 1;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupTest() {
		Mockito.when(this.systemSchemaService.getSystemTableByName(ArgumentMatchers.anyString())).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			SystemTable table = new SystemTable();
			table.setId(MathUtils.SHORT_ONE);
			table.setName((String) args[0]);
			table.setUploadAllowed(StringUtils.isEqual("InvestmentSecurity", table.getName()));
			return table;
		});

		// Set mock properties on resources
		Mockito.when(this.systemColumnService.getSystemColumnGroupByName(ArgumentMatchers.anyString())).thenReturn(new SystemColumnGroup());
	}


	@Test
	public void testUploadOneToOneSecurities() {
		InvestmentSecurityUploadCommand bean = createOneToOneUploadBean(HIERARCHY_BENCHMARKS_PARENT, null, VALID_SHEET_INDEX);
		this.investmentInstrumentUploadService.uploadInvestmentSecurityUploadFile(bean);
		String expectedResult = "[InvestmentSecurity]: 6 Records Inserted." + StringUtils.NEW_LINE + StringUtils.NEW_LINE + "There were no errors with the upload file.";
		Assertions.assertEquals(expectedResult, bean.getUploadResultsString());

		// Look up Security By Symbol and ensure Instrument Exchange and Composite Exchange is populated
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly("TEST1");
		Assertions.assertNotNull(security);
		Assertions.assertNotNull(security.getInstrument());
		Assertions.assertNotNull(security.getInstrument().getExchange());
		Assertions.assertEquals("US", security.getInstrument().getExchange().getName());
		Assertions.assertNotNull(security.getInstrument().getCompositeExchange());
		Assertions.assertEquals("Composite Exchange Test", security.getInstrument().getCompositeExchange().getName());
		Assertions.assertNotNull(security.getInstrument().getCountryOfRisk());
		Assertions.assertEquals("US", security.getInstrument().getCountryOfRisk().getValue());


		// Try Upload Again - Should skip all
		bean = createOneToOneUploadBean(HIERARCHY_BENCHMARKS_PARENT, null, VALID_SHEET_INDEX);
		this.investmentInstrumentUploadService.uploadInvestmentSecurityUploadFile(bean);
		expectedResult = "[InvestmentSecurity]: 0 Records Inserted." + StringUtils.NEW_LINE + StringUtils.NEW_LINE + "There were no errors with the upload file.";
		Assertions.assertEquals(expectedResult, bean.getUploadResultsString());
	}


	@Test
	public void testUploadOneToOneSecurities_MissingTradingCCY_NoDefault() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentSecurityUploadCommand bean = createOneToOneUploadBean(HIERARCHY_BENCHMARKS_PARENT, null, INVALID_SHEET_INDEX);
			this.investmentInstrumentUploadService.uploadInvestmentSecurityUploadFile(bean);
		}, "Trading Currency selection is required on screen because there are securities in the file that do not have Instrument-TradingCurrency-Symbol column populated.");
	}


	@Test
	public void testUploadOneToOneSecurities_MissingTradingCCY_WithDefault() {
		InvestmentSecurityUploadCommand bean = createOneToOneUploadBean(HIERARCHY_BENCHMARKS_PARENT, "USD", INVALID_SHEET_INDEX);
		this.investmentInstrumentUploadService.uploadInvestmentSecurityUploadFile(bean);
		String expectedResult = "[InvestmentSecurity]: 6 Records Inserted." + StringUtils.NEW_LINE + StringUtils.NEW_LINE + "There were no errors with the upload file.";
		Assertions.assertEquals(expectedResult, bean.getUploadResultsString());
	}


	////////////////////////////////////////////////////////////////////////////////


	private InvestmentSecurityUploadCommand createOneToOneUploadBean(Short parentHierarchyId, String defaultTradingCurrencySymbol, int sheetIndex) {
		InvestmentSecurityUploadCommand bean = new InvestmentSecurityUploadCommand();
		bean.setFile(new MultipartFileImpl("src/test/java/com/clifton/investment/instrument/upload/OneToOneSecurityUploadTest.xls"));
		bean.setOneSecurityPerInstrument(true);
		bean.setTradingCurrency(StringUtils.isEmpty(defaultTradingCurrencySymbol) ? null : this.investmentInstrumentService.getInvestmentSecurityBySymbol(defaultTradingCurrencySymbol, true));
		bean.setParentHierarchy(this.investmentSetupService.getInvestmentInstrumentHierarchy(parentHierarchyId));
		bean.setSheetIndex(sheetIndex);
		return bean;
	}
}
