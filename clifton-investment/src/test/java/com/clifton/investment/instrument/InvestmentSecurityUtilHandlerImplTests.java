package com.clifton.investment.instrument;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


/**
 * A set of tests to test the functionality of the InvestmentSecurityUtilHandler.
 * Note:  current tests deal with the new CLS-related functions. Testing of other functions currently not implemented due
 * to time constraints -- these can be added at a future time.
 *
 * @author davidi
 */
@InvestmentInMemoryDatabaseContext
public class InvestmentSecurityUtilHandlerImplTests extends BaseInMemoryDatabaseTests {

	boolean initialized = false;

	InvestmentSecurity ibm;
	InvestmentSecurity usd;
	InvestmentSecurity ivv;
	InvestmentSecurity tpz0;
	InvestmentSecurity llyOpt;
	InvestmentSecurity fund;


	@Resource
	InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	@Resource
	InvestmentInstrumentService investmentInstrumentService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		if (!this.initialized) {
			this.ibm = this.investmentInstrumentService.getInvestmentSecurityBySymbol("IBM", false);
			this.usd = this.investmentInstrumentService.getInvestmentSecurityBySymbol("USD", true);
			this.ivv = this.investmentInstrumentService.getInvestmentSecurityBySymbol("IVV", false);
			this.tpz0 = this.investmentInstrumentService.getInvestmentSecurityBySymbol("TPZ0", false);
			this.llyOpt = this.investmentInstrumentService.getInvestmentSecurityBySymbol("LLY US 04/16/21 C230", false);
			this.fund = this.investmentInstrumentService.getInvestmentSecurityBySymbol("FRIFX", false);
			this.initialized = true;
		}
	}


	@Test
	public void testIsClsCurrency_true() {
		Assertions.assertTrue(this.investmentSecurityUtilHandler.isClsCurrency(this.usd));
	}


	@Test
	public void testIsClsCurrency_false() {
		Assertions.assertFalse(this.investmentSecurityUtilHandler.isClsCurrency(this.ibm));
	}


	@Test
	public void testGetCFICode_Funds_ETF() {
		Assertions.assertEquals(InvestmentSecurityUtilHandler.CFI_FUNDS, this.investmentSecurityUtilHandler.getCFICode(this.ivv));
	}


	@Test
	public void testGetCFICode_Funds() {
		Assertions.assertEquals(InvestmentSecurityUtilHandler.CFI_FUNDS, this.investmentSecurityUtilHandler.getCFICode(this.fund));
	}


	@Test
	public void testGetCFICode_Stocks() {
		Assertions.assertEquals(InvestmentSecurityUtilHandler.CFI_EQUITIES, this.investmentSecurityUtilHandler.getCFICode(this.ibm));
	}


	@Test
	public void testGetCFICode_Futures() {
		Assertions.assertEquals(InvestmentSecurityUtilHandler.CFI_FUTURES, this.investmentSecurityUtilHandler.getCFICode(this.tpz0));
	}


	@Test
	public void testGetCFICode_Options() {
		Assertions.assertEquals("OCAXPS", this.investmentSecurityUtilHandler.getCFICode(this.llyOpt));
	}
}
