package com.clifton.investment.instrument.spot.calculators;

import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.spot.InvestmentSecuritySpotMonth;
import com.clifton.investment.setup.InvestmentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@ContextConfiguration(locations = "../../calculator/InvestmentCalculatorImplTests-context.xml")
@ExtendWith(SpringExtension.class)
public class InvestmentSecuritySpotMonthDateFieldCalculatorTests {

	@Resource
	private ApplicationContextService applicationContextService;

	@Resource
	private CalendarSetupService calendarSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setup() {
		short year = 2016;
		if (this.calendarSetupService.getCalendarYear(year) == null) {
			this.calendarSetupService.saveCalendarYearForYear(year);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInvalidCalculatorSetup() {
		InvestmentSecuritySpotMonthDateFieldCalculator calculator = setupCalculator();
		validateCalculator(calculator, "Start Date Bean Property Name is Required");

		calculator.setStartDateBeanPropertyName("name");
		validateCalculator(calculator, "Start Date Bean Property Must be a Date Field.");

		calculator.setStartDateBeanPropertyName("firstNoticeDate");
		validateCalculator(calculator, "End Date Bean Property Name is Required");

		calculator.setEndDateBeanPropertyName("description");
		validateCalculator(calculator, "End Date Bean Property Must be a Date Field.");

		calculator.setEndDateBeanPropertyName("endDate");
		calculator.setStartFromFirstDayOfMonth(true);
		calculator.setStartFromPreviousDay(true);
		calculator.setStartFromLastDayOfMonth(true);
		validateCalculator(calculator, "Can only select one of start from 1. First Day of Month, 2. Previous Day, 3. Last Day of Month.");

		calculator.setStartFromFirstDayOfMonth(true);
		calculator.setStartFromPreviousDay(true);
		calculator.setStartFromLastDayOfMonth(false);
		validateCalculator(calculator, "Can only select one of start from 1. First Day of Month, 2. Previous Day, 3. Last Day of Month.");

		calculator.setStartFromFirstDayOfMonth(true);
		calculator.setStartFromPreviousDay(false);
		calculator.setStartFromLastDayOfMonth(true);
		validateCalculator(calculator, "Can only select one of start from 1. First Day of Month, 2. Previous Day, 3. Last Day of Month.");

		calculator.setStartFromFirstDayOfMonth(false);
		calculator.setStartFromPreviousDay(true);
		calculator.setStartFromLastDayOfMonth(true);
		validateCalculator(calculator, "Can only select one of start from 1. First Day of Month, 2. Previous Day, 3. Last Day of Month.");
	}


	@Test
	public void testDeliveryMonthCalculator() {
		// "close of trading x business days prior to the first trading day of the delivery month"
		InvestmentSecurity security = setupCurrencyFutureSecurity();

		InvestmentSecuritySpotMonthDateFieldCalculator calculator = setupCalculator();
		calculator.setStartDateBeanPropertyName("lastDeliveryOrEndDate");
		calculator.setEndDateBeanPropertyName("lastDeliveryOrEndDate");
		calculator.setStartFromFirstDayOfMonth(true);
		validateCalculator(calculator, null);

		InvestmentSecuritySpotMonth spotMonth = calculator.calculateInvestmentSecuritySpotMonth(security);
		Assertions.assertEquals("06/01/2016", DateUtils.fromDateShort(spotMonth.getStartDate()));
		Assertions.assertEquals("06/15/2016", DateUtils.fromDateShort(spotMonth.getEndDate()));
		Assertions.assertTrue(spotMonth.isSecurityInSpotMonth(DateUtils.toDate("06/01/2016")));
		Assertions.assertFalse(spotMonth.isSecurityInSpotMonth(DateUtils.toDate("07/01/2016")));

		calculator.setStartBusinessDays(-6);
		spotMonth = calculator.calculateInvestmentSecuritySpotMonth(security);
		Assertions.assertEquals("05/24/2016", DateUtils.fromDateShort(spotMonth.getStartDate()));
		Assertions.assertEquals("06/15/2016", DateUtils.fromDateShort(spotMonth.getEndDate()));
		Assertions.assertTrue(spotMonth.isSecurityInSpotMonth(DateUtils.toDate("05/27/2016")));
		Assertions.assertFalse(spotMonth.isSecurityInSpotMonth(DateUtils.toDate("05/23/2016")));
	}


	@Test
	public void testFirstNoticeToDeliveryCalculator() {
		// "Close of trading on x business days prior to the first notice day for any delivery month"
		InvestmentSecurity security = setupCurrencyFutureSecurity();

		InvestmentSecuritySpotMonthDateFieldCalculator calculator = setupCalculator();
		calculator.setStartDateBeanPropertyName("firstNoticeDate");
		calculator.setEndDateBeanPropertyName("lastDeliveryOrEndDate");
		calculator.setStartBusinessDays(-1);
		validateCalculator(calculator, null);

		InvestmentSecuritySpotMonth spotMonth = calculator.calculateInvestmentSecuritySpotMonth(security);
		Assertions.assertEquals("06/10/2016", DateUtils.fromDateShort(spotMonth.getStartDate()));
		Assertions.assertEquals("06/15/2016", DateUtils.fromDateShort(spotMonth.getEndDate()));
		Assertions.assertTrue(spotMonth.isSecurityInSpotMonth(DateUtils.toDate("06/15/2016")));
		Assertions.assertFalse(spotMonth.isSecurityInSpotMonth(DateUtils.toDate("06/09/2016")));
	}


	@Test
	public void testLastTradeDateCalculator() {
		// "Close of trading X business days prior to last trading day of the contract"
		InvestmentSecurity security = setupCurrencyFutureSecurity();

		InvestmentSecuritySpotMonthDateFieldCalculator calculator = setupCalculator();
		calculator.setStartDateBeanPropertyName("endDate");
		calculator.setEndDateBeanPropertyName("lastDeliveryOrEndDate");
		calculator.setStartBusinessDays(-3);
		validateCalculator(calculator, null);

		InvestmentSecuritySpotMonth spotMonth = calculator.calculateInvestmentSecuritySpotMonth(security);
		Assertions.assertEquals("06/08/2016", DateUtils.fromDateShort(spotMonth.getStartDate()));
		Assertions.assertEquals("06/15/2016", DateUtils.fromDateShort(spotMonth.getEndDate()));
	}


	@Test
	public void testLastDaysOfMonthDateCalculator() {
		// "Close of trading X business days prior to last day of the contract month"
		InvestmentSecurity security = setupCurrencyFutureSecurity();

		InvestmentSecuritySpotMonthDateFieldCalculator calculator = setupCalculator();
		calculator.setStartDateBeanPropertyName("lastDeliveryOrEndDate");
		calculator.setStartFromLastDayOfMonth(true);
		calculator.setStartBusinessDays(-10);
		calculator.setEndDateBeanPropertyName("lastDeliveryOrEndDate");
		calculator.setEndFromLastDayOfMonth(true);
		validateCalculator(calculator, null);

		InvestmentSecuritySpotMonth spotMonth = calculator.calculateInvestmentSecuritySpotMonth(security);
		Assertions.assertEquals("06/16/2016", DateUtils.fromDateShort(spotMonth.getStartDate()));
		Assertions.assertEquals("06/30/2016", DateUtils.fromDateShort(spotMonth.getEndDate()));
	}


	@Test
	public void testFridayPriorToExpiration() {
		InvestmentSecurity security = setupCurrencyFutureSecurity();

		InvestmentSecuritySpotMonthDateFieldCalculator calculator = setupCalculator();
		calculator.setStartDateBeanPropertyName("endDate");
		calculator.setStartFromPreviousDay(true);
		calculator.setStartDateWeekdayId((short) 6); // 6 = Friday
		calculator.setEndDateBeanPropertyName("lastDeliveryOrEndDate");

		validateCalculator(calculator, null);

		InvestmentSecuritySpotMonth spotMonth = calculator.calculateInvestmentSecuritySpotMonth(security);
		Assertions.assertEquals("06/10/2016", DateUtils.fromDateShort(spotMonth.getStartDate()));
		Assertions.assertEquals("06/15/2016", DateUtils.fromDateShort(spotMonth.getEndDate()));
	}


	@Test
	public void testFirstFridayOfDeliveryMonth() {
		InvestmentSecurity security = setupCurrencyFutureSecurity();

		InvestmentSecuritySpotMonthDateFieldCalculator calculator = setupCalculator();
		calculator.setStartDateBeanPropertyName("endDate");
		calculator.setStartFromFirstDayOfMonth(true);
		calculator.setStartDateWeekdayId((short) 6); // 6 = Friday
		calculator.setEndDateBeanPropertyName("lastDeliveryOrEndDate");

		validateCalculator(calculator, null);

		InvestmentSecuritySpotMonth spotMonth = calculator.calculateInvestmentSecuritySpotMonth(security);
		Assertions.assertEquals("06/03/2016", DateUtils.fromDateShort(spotMonth.getStartDate()));
		Assertions.assertEquals("06/15/2016", DateUtils.fromDateShort(spotMonth.getEndDate()));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private InvestmentSecurity setupCurrencyFutureSecurity() {
		InvestmentSecurity future = InvestmentTestObjectFactory.newInvestmentSecurity(1, "ADM6", InvestmentType.FUTURES);
		future.setStartDate(DateUtils.toDate("08/12/2013"));
		future.setEndDate(DateUtils.toDate("06/13/2016"));
		future.setFirstNoticeDate(DateUtils.toDate("06/13/2016"));
		future.setFirstDeliveryDate(DateUtils.toDate("06/15/2016"));
		future.setLastDeliveryDate(DateUtils.toDate("06/15/2016"));
		return future;
	}


	private InvestmentSecuritySpotMonthDateFieldCalculator setupCalculator() {
		InvestmentSecuritySpotMonthDateFieldCalculator calculator = new InvestmentSecuritySpotMonthDateFieldCalculator();
		this.applicationContextService.autowireBean(calculator);
		return calculator;
	}


	private void validateCalculator(InvestmentSecuritySpotMonthDateFieldCalculator calculator, String expectedErrorMessage) {
		String errorMessage = null;
		try {
			calculator.validate();
		}
		catch (Throwable e) {
			errorMessage = e.getMessage();
		}
		if (!StringUtils.isEmpty(expectedErrorMessage)) {
			Assertions.assertEquals(expectedErrorMessage, errorMessage);
		}
		else {
			Assertions.assertTrue(StringUtils.isEmpty(errorMessage), "Did not expect error during validation, but received: " + errorMessage);
		}
	}
}
