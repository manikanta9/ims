package com.clifton.investment.instrument.event;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType.EventBeforeStartTypes;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class InvestmentSecurityEventServiceImplTests {

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	@Resource
	private UpdatableDAO<InvestmentSecurityEventType> investmentSecurityEventTypeDAO;

	@Resource
	private ApplicationContextService applicationContextService;


	@BeforeEach
	public void setup() {
		InvestmentSecurityEventService eventServiceSpy = Mockito.spy(this.investmentSecurityEventService);
		Mockito.doReturn(new ArrayList<InvestmentSecurityEvent>()).when(eventServiceSpy).getInvestmentSecurityEventListSameForInstrumentUnderlying(Mockito.any());
		this.investmentSecurityEventService = eventServiceSpy;
		InvestmentSecurityEventObserver<?> eventObserver = this.applicationContextService.getContextBean("investmentSecurityEventObserver", InvestmentSecurityEventObserver.class);
		eventObserver.setInvestmentSecurityEventService(eventServiceSpy);
	}


	@Test
	public void testGetInvestmentSecurityEventPreviousForAccrualEndDate() {
		InvestmentSecurityEvent event = this.investmentSecurityEventService.getInvestmentSecurityEventPreviousForAccrualEndDate(800, "Cash Coupon Payment", DateUtils.toDate("07/16/2011"));
		Assertions.assertEquals(new BigDecimal("0.450001"), event.getAfterEventValue());

		event = this.investmentSecurityEventService.getInvestmentSecurityEventPreviousForAccrualEndDate(800, "Cash Coupon Payment", DateUtils.toDate("10/17/2011"));
		Assertions.assertEquals(new BigDecimal("0.450001"), event.getAfterEventValue());

		event = this.investmentSecurityEventService.getInvestmentSecurityEventPreviousForAccrualEndDate(800, "Cash Coupon Payment", DateUtils.toDate("07/15/2011"));
		Assertions.assertNull(event);

		event = this.investmentSecurityEventService.getInvestmentSecurityEventPreviousForAccrualEndDate(800, "Cash Coupon Payment", DateUtils.toDate("01/17/2012"));
		Assertions.assertEquals(new BigDecimal("0.499250"), event.getAfterEventValue());
	}


	@Test
	public void testAddEventBeforeSecurityStartDate_NoneAllowed() {

		InvestmentSecurity sec = this.investmentInstrumentService.getInvestmentSecurity(1001);
		sec.setStartDate(DateUtils.toDate("05/01/2013"));
		this.investmentInstrumentService.saveInvestmentSecurity(sec);

		InvestmentSecurityEventType type = this.investmentSecurityEventService.getInvestmentSecurityEventType(MathUtils.SHORT_TWO);
		type.setEventBeforeStartType(EventBeforeStartTypes.NONE);
		this.investmentSecurityEventTypeDAO.save(type);

		TestUtils.expectException(FieldValidationException.class, () -> {
			InvestmentSecurityEvent event = new InvestmentSecurityEvent();
			event.setType(type);
			event.setSecurity(this.investmentInstrumentService.getInvestmentSecurity(1001));
			event.setEventDate(DateUtils.toDate("04/28/2013"));
			event.setBeforeEventValue(BigDecimal.valueOf(2));
			event.setAfterEventValue(BigDecimal.valueOf(1));
			this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);
		}, "Event Date [04/28/2013] is invalid because it is before Security [" + sec.getSymbol() + "] start date [05/01/2013] and event type [" + type.getName() + "] does not allow events before the security start date.");
	}


	@Test
	public void testAddEventBeforeSecurityStartDate_OneAllowed() {
		InvestmentSecurity sec = this.investmentInstrumentService.getInvestmentSecurity(1001);
		sec.setStartDate(DateUtils.toDate("05/01/2013"));
		this.investmentInstrumentService.saveInvestmentSecurity(sec);

		InvestmentSecurityEventType type = this.investmentSecurityEventService.getInvestmentSecurityEventType((short) 4);
		type.setEventBeforeStartType(EventBeforeStartTypes.ONE);
		this.investmentSecurityEventTypeDAO.save(type);

		TestUtils.expectException(FieldValidationException.class, () -> {
			InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(this.investmentInstrumentService.getInvestmentSecurity(1001), type, "04/01/2013");
			event.setBeforeEventValue(BigDecimal.valueOf(2));
			event.setAfterEventValue(BigDecimal.valueOf(1));
			this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);

			event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(this.investmentInstrumentService.getInvestmentSecurity(1001), type, "03/01/2013");
			event.setBeforeEventValue(BigDecimal.valueOf(2));
			event.setAfterEventValue(BigDecimal.valueOf(1));
			this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);
		}, "There already exists a [" + type.getName() + "] event on [04/01/2013] which is before the Security [" + sec.getSymbol() + "] start date [05/01/2013]. Event type [" + type.getName() + "] allows only one event before the security start date.");
	}


	@Test
	public void testAddEventBeforeSecurityStartDate_ManyAllowed() {
		InvestmentSecurity sec = this.investmentInstrumentService.getInvestmentSecurity(1001);
		sec.setStartDate(DateUtils.toDate("05/01/2013"));
		this.investmentInstrumentService.saveInvestmentSecurity(sec);

		InvestmentSecurityEventType type = this.investmentSecurityEventService.getInvestmentSecurityEventType((short) 4);
		type.setEventBeforeStartType(EventBeforeStartTypes.MANY);
		this.investmentSecurityEventTypeDAO.save(type);

		InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(this.investmentInstrumentService.getInvestmentSecurity(1001), type, "04/01/2013");
		event.setBeforeEventValue(BigDecimal.valueOf(2));
		event.setAfterEventValue(BigDecimal.valueOf(1));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);

		event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(this.investmentInstrumentService.getInvestmentSecurity(1001), type, "03/01/2013");
		event.setBeforeEventValue(BigDecimal.valueOf(2));
		event.setAfterEventValue(BigDecimal.valueOf(1));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);
	}


	@Test
	public void testAddEventAfterSecurityEndDate() {
		InvestmentSecurity sec = this.investmentInstrumentService.getInvestmentSecurity(1001);
		sec.setEndDate(DateUtils.toDate("05/31/2013"));
		this.investmentInstrumentService.saveInvestmentSecurity(sec);

		TestUtils.expectException(FieldValidationException.class, () -> {
			InvestmentSecurityEvent event = new InvestmentSecurityEvent();
			event.setType(this.investmentSecurityEventService.getInvestmentSecurityEventType(MathUtils.SHORT_TWO));
			event.setSecurity(this.investmentInstrumentService.getInvestmentSecurity(1001));
			event.setEventDate(DateUtils.toDate("06/28/2013"));
			event.setBeforeEventValue(BigDecimal.valueOf(2));
			event.setAfterEventValue(BigDecimal.valueOf(1));
			this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);
		}, "Event Date [06/28/2013] is invalid because it is after Security [" + sec.getSymbol() + "] last delivery/maturity [05/31/2013]");
	}


	@Test
	public void testAddEventAfterSecurityEndDateAndLastDeliveryDate() {
		InvestmentSecurity sec = this.investmentInstrumentService.getInvestmentSecurity(1001);
		sec.setEndDate(DateUtils.toDate("06/27/2013"));
		sec.setLastDeliveryDate(DateUtils.toDate("06/28/2013"));
		this.investmentInstrumentService.saveInvestmentSecurity(sec);

		TestUtils.expectException(FieldValidationException.class, () -> {
			InvestmentSecurityEvent event = new InvestmentSecurityEvent();
			event.setType(this.investmentSecurityEventService.getInvestmentSecurityEventType(MathUtils.SHORT_TWO));
			event.setSecurity(this.investmentInstrumentService.getInvestmentSecurity(1001));
			event.setEventDate(DateUtils.toDate("07/01/2013"));
			event.setBeforeEventValue(BigDecimal.valueOf(2));
			event.setAfterEventValue(BigDecimal.valueOf(1));
			this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);
		}, "Event Date [07/01/2013] is invalid because it is after Security [" + sec.getSymbol() + "] last delivery/maturity [06/28/2013]");
	}


	@Test
	public void testAddEventAfterSecurityEndDateBeforeLastDeliveryDate() {
		InvestmentSecurity sec = this.investmentInstrumentService.getInvestmentSecurity(1001);
		sec.setEndDate(DateUtils.toDate("06/27/2013"));
		sec.setLastDeliveryDate(DateUtils.toDate("06/28/2013"));
		this.investmentInstrumentService.saveInvestmentSecurity(sec);

		InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(this.investmentInstrumentService.getInvestmentSecurity(1001), this.investmentSecurityEventService.getInvestmentSecurityEventType(MathUtils.SHORT_TWO), "06/28/2013");
		event.setBeforeEventValue(BigDecimal.valueOf(2));
		event.setAfterEventValue(BigDecimal.valueOf(1));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);
	}


	@Test
	public void testAddEventsToAllSecuritiesInSameHierarchy_NoneAllowedBeforeSecurityStart() {
		InvestmentSecurityEventType interestLegPayment = this.investmentSecurityEventService.getInvestmentSecurityEventType(MathUtils.SHORT_TWO);
		InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(this.investmentInstrumentService.getInvestmentSecurity(1001), interestLegPayment, "02/28/2013");
		event.setBeforeEventValue(BigDecimal.valueOf(2));
		event.setAfterEventValue(BigDecimal.valueOf(1));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);

		// Should also be created on 1002 Security

		List<InvestmentSecurityEvent> eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event);
		Assertions.assertEquals(3, CollectionUtils.getSize(eventList));

		// 1001 Security Started on 1/1, 1002 started on 2/1
		// Add another event on 3/31
		InvestmentSecurityEvent event2 = InvestmentTestObjectFactory.newInvestmentSecurityEvent(this.investmentInstrumentService.getInvestmentSecurity(1001), interestLegPayment, "03/31/2013");
		event2.setBeforeAndAfterEventValue(BigDecimal.valueOf(5));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event2);

		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event2);
		Assertions.assertEquals(3, CollectionUtils.getSize(eventList));

		// Add a New Security - starting on 3/1
		InvestmentSecurity sec_1003 = BeanUtils.cloneBean(this.investmentInstrumentService.getInvestmentSecurity(1002), false, false);
		sec_1003.setId(null);
		sec_1003.setStartDate(DateUtils.toDate("03/01/2013"));
		sec_1003.setSymbol("TEST-CDS-3");
		this.investmentInstrumentService.saveInvestmentSecurity(sec_1003);

		// First Event should still only be attached to 2 securities
		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event);
		Assertions.assertEquals(3, CollectionUtils.getSize(eventList));

		// Second Event should be attached to all 3 securities
		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event2);
		Assertions.assertEquals(4, CollectionUtils.getSize(eventList));

		// Change the Symbol on one of the securities
		sec_1003.setSymbol("TEST-CDS-3: Update");
		this.investmentInstrumentService.saveInvestmentSecurity(sec_1003);

		// Make sure the events weren't duplicated on the symbol update - there are still only 3 as there were before the symbol update
		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event2);
		Assertions.assertEquals(4, CollectionUtils.getSize(eventList));

		// More specifically - there is only 1 event still tied to the security we just updated
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(sec_1003.getId());
		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		Assertions.assertEquals(1, CollectionUtils.getSize(eventList));

		// Move the Event Date and make sure they all moved
		event2.setEventDate(DateUtils.toDate("04/15/2013"));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event2);

		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event2);
		Assertions.assertEquals(4, CollectionUtils.getSize(eventList));

		// Delete event
		this.investmentSecurityEventService.deleteInvestmentSecurityEvent(event.getId());
		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event);
		Assertions.assertTrue(CollectionUtils.isEmpty(eventList));
	}


	@Test
	public void testAddEventsToAllSecuritiesInSameHierarchy_OneAllowedBeforeSecurityStart() {
		InvestmentSecurityEventType creditEvent = this.investmentSecurityEventService.getInvestmentSecurityEventType((short) 4);
		// 1001 Security Started on 1/1, 1002 started on 2/1

		InvestmentSecurityEvent event = InvestmentTestObjectFactory.newInvestmentSecurityEvent(this.investmentInstrumentService.getInvestmentSecurity(1001), creditEvent, "12/15/2012");
		event.setBeforeEventValue(BigDecimal.valueOf(2));
		event.setAfterEventValue(BigDecimal.valueOf(1));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);

		// Should also be created on 1002 Security
		List<InvestmentSecurityEvent> eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event);
		Assertions.assertEquals(3, CollectionUtils.getSize(eventList));

		// Add another event on 1/15 - after 1001, but before 1002
		InvestmentSecurityEvent event2 = InvestmentTestObjectFactory.newInvestmentSecurityEvent(this.investmentInstrumentService.getInvestmentSecurity(1001), creditEvent, "01/15/2013");
		event2.setBeforeAndAfterEventValue(BigDecimal.valueOf(5));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event2);

		// Should be copied to 1002 security, even though before start and there is one before start - Notifications will be generated to notify users to delete the older one
		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event2);
		Assertions.assertEquals(3, CollectionUtils.getSize(eventList));

		// Add a New Security - starting on 3/1
		InvestmentSecurity sec_1003 = BeanUtils.cloneBean(this.investmentInstrumentService.getInvestmentSecurity(1002), false, false);
		sec_1003.setId(null);
		sec_1003.setStartDate(DateUtils.toDate("03/01/2013"));
		sec_1003.setSymbol("TEST-CDS-3");
		this.investmentInstrumentService.saveInvestmentSecurity(sec_1003);

		// Second Event should be attached to all 4 securities
		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event2);
		Assertions.assertEquals(4, CollectionUtils.getSize(eventList));

		// First Event should still only be attached to 3 securities - system will automatically copy the last event before start only
		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event);
		Assertions.assertEquals(3, CollectionUtils.getSize(eventList));

		// Delete event 1 from security 1002 - because it is before start date should only delete that one, but leave it with 1001 security
		InvestmentSecurityEvent deleteEvent = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(eventList, securityEvent -> securityEvent.getSecurity().getId(), 1002));
		this.investmentSecurityEventService.deleteInvestmentSecurityEvent(deleteEvent.getId());
		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event);
		Assertions.assertEquals(2, CollectionUtils.getSize(eventList));
		Assertions.assertEquals((Integer) 1001, eventList.get(0).getSecurity().getId());

		// Add another event on 5/15 - after 1001, 1002, and 1003 started
		InvestmentSecurityEvent event3 = InvestmentTestObjectFactory.newInvestmentSecurityEvent(this.investmentInstrumentService.getInvestmentSecurity(1001), creditEvent, "05/15/2013");
		event3.setBeforeAndAfterEventValue(BigDecimal.valueOf(5));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event3);

		// Third Event should be attached to all 4 securities
		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event3);
		Assertions.assertEquals(4, CollectionUtils.getSize(eventList));

		// Delete One - Should delete them all
		this.investmentSecurityEventService.deleteInvestmentSecurityEvent(event3.getId());
		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventListSameForInstrument(event3);
		Assertions.assertTrue(CollectionUtils.isEmpty(eventList));
	}
}
