package com.clifton.investment.instrument;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Date;


/**
 * @author manderson
 */
public class InvestmentPricingFrequenciesTests {

	@Mock
	private CalendarBusinessDayService calendarBusinessDayService;

	////////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			Date date = ((CalendarBusinessDayCommand) args[0]).getDate();
			return DateUtils.getNextWeekday(date);
		}).when(this.calendarBusinessDayService).getNextBusinessDay(ArgumentMatchers.any(CalendarBusinessDayCommand.class));
	}

	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testDailyPricingFrequency() {
		InvestmentPricingFrequencies pricingFrequency = InvestmentPricingFrequencies.DAILY;
		validateMinimumDate(pricingFrequency, "01/01/2016", "01/01/2016");
		validateMinimumDate(pricingFrequency, "03/31/2016", "03/31/2016");
		validateMinimumDate(pricingFrequency, "04/29/2016", "04/29/2016");
	}


	@Test
	public void testMonthlyPricingFrequency() {
		InvestmentPricingFrequencies pricingFrequency = InvestmentPricingFrequencies.MONTHLY;
		validateMinimumDate(pricingFrequency, "01/01/2016", "12/31/2015");
		validateMinimumDate(pricingFrequency, "03/30/2016", "02/29/2016");
		validateMinimumDate(pricingFrequency, "03/31/2016", "03/31/2016");
		validateMinimumDate(pricingFrequency, "04/15/2016", "03/31/2016");
		validateMinimumDate(pricingFrequency, "04/29/2016", "04/30/2016");
	}


	@Test
	public void testQuarterlyPricingFrequency() {
		InvestmentPricingFrequencies pricingFrequency = InvestmentPricingFrequencies.QUARTERLY;
		validateMinimumDate(pricingFrequency, "01/01/2016", "12/31/2015");
		validateMinimumDate(pricingFrequency, "03/30/2016", "12/31/2015");
		validateMinimumDate(pricingFrequency, "03/31/2016", "03/31/2016");
		validateMinimumDate(pricingFrequency, "04/15/2016", "03/31/2016");
		validateMinimumDate(pricingFrequency, "04/29/2016", "03/31/2016");
	}


	@Test
	public void testDailyFlexiblePricingFrequency() {
		InvestmentPricingFrequencies pricingFrequency = InvestmentPricingFrequencies.DAILY_FLEXIBLE;
		// Historically would use monthly
		validateMinimumDate(pricingFrequency, "01/01/2016", "12/31/2015");
		validateMinimumDate(pricingFrequency, "03/30/2016", "02/29/2016");
		validateMinimumDate(pricingFrequency, "03/31/2016", "03/31/2016");
		validateMinimumDate(pricingFrequency, "04/15/2016", "03/31/2016");
		validateMinimumDate(pricingFrequency, "04/29/2016", "04/30/2016");

		// But from previous business day to the future, should use DAILY
		Date testDate = DateUtils.getPreviousWeekday(new Date());
		validateMinimumDate(pricingFrequency, DateUtils.fromDateShort(testDate), DateUtils.fromDateShort(testDate));
		testDate = DateUtils.getNextWeekday(new Date());
		validateMinimumDate(pricingFrequency, DateUtils.fromDateShort(testDate), DateUtils.fromDateShort(testDate));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validateMinimumDate(InvestmentPricingFrequencies pricingFrequency, String testDate, String expectedMinimumDate) {
		short calendarId = 1;
		Date minimumDate = pricingFrequency.getMinimumPriceDateForDate(DateUtils.toDate(testDate), calendarId, this.calendarBusinessDayService);
		Assertions.assertEquals(expectedMinimumDate, DateUtils.fromDateShort(minimumDate), "Expected Minimum Price Date for [" + testDate + "] using " + pricingFrequency.name() + " to be " + expectedMinimumDate);
	}
}
