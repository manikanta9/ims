package com.clifton.investment.instrument.event.sequence;

import com.clifton.calendar.CalendarTestObjectFactory;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.calculator.CouponFrequencies;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.sequence.date.EquityLegPaymentDateCalculator;
import com.clifton.investment.instrument.event.sequence.date.InterestLegPaymentDateCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;


public class InvestmentInstrumentEventSequenceServiceImplTest extends AbstractInvestmentInstrumentEventSequenceTest {

	@Test
	public void testEquityLegPaymentSequence_LTP_JPCEQBC1() {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);
		//Calendar uk_calendar = CALENDAR_SETUP_SERVICE.getCalendar(CALENDAR_UK);
		//Calendar combined = CALENDAR_SETUP_SERVICE.getCalendar(CALENDAR_COMBINED);

		EquityLegPaymentDateCalculator dateCalculator = configureDateCalculator(new EquityLegPaymentDateCalculator());

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(calendar);
		sequence.setSettlementCalendar(calendar);

		sequence.setSequenceStartDate(DateUtils.toDate("02/03/2014"));
		//sequence.setFirstSequenceEventExist(true);
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Quarterly"));
		sequence.setSettlementCycle(3);
		sequence.setDateCalculator(dateCalculator);
		sequence.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT));
		sequence.setFixingCycle(1);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(20783, "LTP JPCEQBC1", "Swaps"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("02/03/2015"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("02/06/2015"));

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(4, events.size());

		//DeclareDate,ExDate,RecordDate,EventDate
		Assertions.assertEquals("02/03/2014	05/06/2014	05/05/2014	05/08/2014", formatEquityEvent(events.get(0)));
		Assertions.assertEquals("05/05/2014	08/05/2014	08/04/2014	08/07/2014", formatEquityEvent(events.get(1)));
		Assertions.assertEquals("08/04/2014	11/04/2014	11/03/2014	11/06/2014", formatEquityEvent(events.get(2)));
		Assertions.assertEquals("11/03/2014	02/04/2015	02/03/2015	02/06/2015", formatEquityEvent(events.get(3)));
	}


	@Test
	public void testEquityLegPaymentSequence_20153637() {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);
		Calendar uk_calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_UK);
		Calendar combined = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_COMBINED);

		EquityLegPaymentDateCalculator dateCalculator = configureDateCalculator(new EquityLegPaymentDateCalculator());

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(uk_calendar);
		sequence.setSettlementCalendar(combined);

		sequence.setSequenceStartDate(DateUtils.toDate("11/30/2012"));
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Monthly"));
		sequence.setSettlementCycle(3);
		sequence.setDateCalculator(dateCalculator);
		sequence.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT));
		sequence.setFixingCycle(1);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(18116, "20153637", "Swaps"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("11/29/2013"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("12/04/2013"));

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(12, events.size());

		Assertions.assertEquals("11/30/2012	01/01/2013	12/31/2012	01/04/2013", formatEquityEvent(events.get(0)));
		Assertions.assertEquals("12/31/2012	02/01/2013	01/31/2013	02/05/2013", formatEquityEvent(events.get(1)));
		Assertions.assertEquals("01/31/2013	03/01/2013	02/28/2013	03/05/2013", formatEquityEvent(events.get(2)));
		Assertions.assertEquals("02/28/2013	03/29/2013	03/28/2013	04/04/2013", formatEquityEvent(events.get(3)));
		Assertions.assertEquals("03/28/2013	05/01/2013	04/30/2013	05/03/2013", formatEquityEvent(events.get(4)));
		Assertions.assertEquals("04/30/2013	06/01/2013	05/31/2013	06/05/2013", formatEquityEvent(events.get(5)));
		Assertions.assertEquals("05/31/2013	06/29/2013	06/28/2013	07/03/2013", formatEquityEvent(events.get(6)));
		Assertions.assertEquals("06/28/2013	08/01/2013	07/31/2013	08/05/2013", formatEquityEvent(events.get(7)));
		Assertions.assertEquals("07/31/2013	08/31/2013	08/30/2013	09/05/2013", formatEquityEvent(events.get(8)));
		Assertions.assertEquals("08/30/2013	10/01/2013	09/30/2013	10/03/2013", formatEquityEvent(events.get(9)));
		Assertions.assertEquals("09/30/2013	11/01/2013	10/31/2013	11/05/2013", formatEquityEvent(events.get(10)));
		Assertions.assertEquals("10/31/2013	11/30/2013	11/29/2013	12/04/2013", formatEquityEvent(events.get(11)));
	}


	@Test
	public void testInterestLegPaymentSequence_20153637() {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);
		Calendar uk_calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_UK);
		Calendar combined = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_COMBINED);

		InterestLegPaymentDateCalculator dateCalculator = configureDateCalculator(new InterestLegPaymentDateCalculator());

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(uk_calendar);
		sequence.setSettlementCalendar(combined);

		sequence.setSequenceStartDate(DateUtils.toDate("11/30/2012"));
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Monthly"));
		sequence.setSettlementCycle(3);
		sequence.setDateCalculator(dateCalculator);
		sequence.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.INTEREST_LEG_PAYMENT));
		sequence.setFixingCycle(1);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(18116, "20153637", "Swaps"));
		sequence.getSecurity().setStartDate(DateUtils.toDate("11/30/2012"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("11/29/2013"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("12/04/2013"));

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(12, events.size());

		Assertions.assertEquals("12/05/2012	01/04/2013	01/01/2013	01/04/2013	12/06/2012", formatInterestEvent(events.get(0)));
		Assertions.assertEquals("01/04/2013	02/05/2013	02/01/2013	02/05/2013	01/07/2013", formatInterestEvent(events.get(1)));
		Assertions.assertEquals("02/05/2013	03/05/2013	03/01/2013	03/05/2013	02/06/2013", formatInterestEvent(events.get(2)));
		Assertions.assertEquals("03/05/2013	04/04/2013	04/01/2013	04/04/2013	03/06/2013", formatInterestEvent(events.get(3)));
		Assertions.assertEquals("04/04/2013	05/03/2013	05/01/2013	05/03/2013	04/05/2013", formatInterestEvent(events.get(4)));
		Assertions.assertEquals("05/03/2013	06/05/2013	06/01/2013	06/05/2013	05/07/2013", formatInterestEvent(events.get(5)));
		Assertions.assertEquals("06/05/2013	07/03/2013	07/01/2013	07/03/2013	06/06/2013", formatInterestEvent(events.get(6)));
		Assertions.assertEquals("07/03/2013	08/05/2013	08/01/2013	08/05/2013	07/04/2013", formatInterestEvent(events.get(7)));
		Assertions.assertEquals("08/05/2013	09/05/2013	09/01/2013	09/05/2013	08/06/2013", formatInterestEvent(events.get(8)));
		Assertions.assertEquals("09/05/2013	10/03/2013	10/01/2013	10/03/2013	09/06/2013", formatInterestEvent(events.get(9)));
		Assertions.assertEquals("10/03/2013	11/05/2013	11/01/2013	11/05/2013	10/04/2013", formatInterestEvent(events.get(10)));
		Assertions.assertEquals("11/05/2013	12/04/2013	12/01/2013	12/04/2013	11/06/2013", formatInterestEvent(events.get(11)));
	}


	@Test
	public void testEquityLegPaymentSequence_4724742B() {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);
		Calendar uk_calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_UK);
		Calendar combined = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_COMBINED);

		EquityLegPaymentDateCalculator dateCalculator = configureDateCalculator(new EquityLegPaymentDateCalculator());

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(uk_calendar);
		sequence.setSettlementCalendar(combined);

		sequence.setSequenceStartDate(DateUtils.toDate("04/30/2012"));
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Monthly"));
		sequence.setSettlementCycle(1);
		sequence.setDateCalculator(dateCalculator);
		sequence.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT));
		sequence.setFixingCycle(-1);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(18116, "4724742B", "Swaps"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("11/29/2013"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("12/04/2013"));

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, DateUtils.toDate("06/01/2012"), DateUtils.toDate("11/01/2012"));
		Assertions.assertEquals(5, events.size());

		InvestmentSecurityEvent event = events.get(0);
		Assertions.assertEquals("06/29/2012", DateUtils.fromDateShort(event.getDeclareDate()));
		Assertions.assertEquals("08/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("07/31/2012", DateUtils.fromDateShort(event.getRecordDate()));
		Assertions.assertEquals("08/01/2012", DateUtils.fromDateShort(event.getEventDate()));

		event = events.get(1);
		Assertions.assertEquals("07/31/2012", DateUtils.fromDateShort(event.getDeclareDate()));
		Assertions.assertEquals("09/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("08/31/2012", DateUtils.fromDateShort(event.getRecordDate()));
		Assertions.assertEquals("09/04/2012", DateUtils.fromDateShort(event.getEventDate()));

		event = events.get(2);
		Assertions.assertEquals("08/31/2012", DateUtils.fromDateShort(event.getDeclareDate()));
		Assertions.assertEquals("09/29/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("09/28/2012", DateUtils.fromDateShort(event.getRecordDate()));
		Assertions.assertEquals("10/01/2012", DateUtils.fromDateShort(event.getEventDate()));

		event = events.get(3);
		Assertions.assertEquals("09/28/2012", DateUtils.fromDateShort(event.getDeclareDate()));
		Assertions.assertEquals("11/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("10/31/2012", DateUtils.fromDateShort(event.getRecordDate()));
		Assertions.assertEquals("11/01/2012", DateUtils.fromDateShort(event.getEventDate()));

		event = events.get(4);
		Assertions.assertEquals("10/31/2012", DateUtils.fromDateShort(event.getDeclareDate()));
		Assertions.assertEquals("12/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("11/30/2012", DateUtils.fromDateShort(event.getRecordDate()));
		Assertions.assertEquals("12/03/2012", DateUtils.fromDateShort(event.getEventDate()));
	}


	@Test
	public void testInterestLegPaymentSequence_4724742B() {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);
		Calendar uk_calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_UK);
		Calendar combined = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_COMBINED);

		InterestLegPaymentDateCalculator dateCalculator = configureDateCalculator(new InterestLegPaymentDateCalculator());

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(uk_calendar);
		sequence.setSettlementCalendar(combined);

		sequence.setSequenceStartDate(DateUtils.toDate("04/30/2012"));
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Monthly"));
		sequence.setSettlementCycle(1);
		sequence.setDateCalculator(dateCalculator);
		sequence.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.INTEREST_LEG_PAYMENT));
		sequence.setFixingCycle(-1);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(18116, "4724742B", "Swaps"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("11/30/2012"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("12/03/2012"));

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, DateUtils.toDate("06/01/2012"), DateUtils.toDate("11/01/2012"));
		Assertions.assertEquals(5, events.size());

		InvestmentSecurityEvent event = events.get(0);
		Assertions.assertEquals("07/02/2012", DateUtils.fromDateShort(event.getAccrualStartDate()));
		Assertions.assertEquals("08/01/2012", DateUtils.fromDateShort(event.getAccrualEndDate()));
		Assertions.assertEquals("08/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("08/01/2012", DateUtils.fromDateShort(event.getPaymentDate()));
		Assertions.assertEquals("06/29/2012", DateUtils.fromDateShort(event.getAdditionalDate()));

		event = events.get(1);
		Assertions.assertEquals("08/01/2012", DateUtils.fromDateShort(event.getAccrualStartDate()));
		Assertions.assertEquals("09/04/2012", DateUtils.fromDateShort(event.getAccrualEndDate()));
		Assertions.assertEquals("09/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("09/04/2012", DateUtils.fromDateShort(event.getPaymentDate()));
		Assertions.assertEquals("07/31/2012", DateUtils.fromDateShort(event.getAdditionalDate()));

		event = events.get(2);
		Assertions.assertEquals("09/04/2012", DateUtils.fromDateShort(event.getAccrualStartDate()));
		Assertions.assertEquals("10/01/2012", DateUtils.fromDateShort(event.getAccrualEndDate()));
		Assertions.assertEquals("10/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("10/01/2012", DateUtils.fromDateShort(event.getPaymentDate()));
		Assertions.assertEquals("09/03/2012", DateUtils.fromDateShort(event.getAdditionalDate()));

		event = events.get(3);
		Assertions.assertEquals("10/01/2012", DateUtils.fromDateShort(event.getAccrualStartDate()));
		Assertions.assertEquals("11/01/2012", DateUtils.fromDateShort(event.getAccrualEndDate()));
		Assertions.assertEquals("11/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("11/01/2012", DateUtils.fromDateShort(event.getPaymentDate()));
		Assertions.assertEquals("09/28/2012", DateUtils.fromDateShort(event.getAdditionalDate()));

		event = events.get(4);
		Assertions.assertEquals("11/01/2012", DateUtils.fromDateShort(event.getAccrualStartDate()));
		Assertions.assertEquals("12/03/2012", DateUtils.fromDateShort(event.getAccrualEndDate()));
		Assertions.assertEquals("12/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("12/03/2012", DateUtils.fromDateShort(event.getPaymentDate()));
		Assertions.assertEquals("10/31/2012", DateUtils.fromDateShort(event.getAdditionalDate()));
	}


	@Test
	public void testEquityLegPaymentSequence_NET6885445() {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);
		Calendar uk_calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_UK);
		Calendar combined = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_COMBINED);

		EquityLegPaymentDateCalculator dateCalculator = configureDateCalculator(new EquityLegPaymentDateCalculator());

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(uk_calendar);
		sequence.setSettlementCalendar(combined);

		sequence.setSequenceStartDate(DateUtils.toDate("02/29/2012"));
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Quarterly"));
		sequence.setSettlementCycle(3);
		sequence.setDateCalculator(dateCalculator);
		sequence.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.EQUITY_LEG_PAYMENT));
		sequence.setFixingCycle(1);

		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(17747, "NET6885445", "Swaps"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("03/01/2013"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("03/05/2013"));

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(4, events.size());

		InvestmentSecurityEvent event = events.get(0);
		Assertions.assertEquals("02/29/2012", DateUtils.fromDateShort(event.getDeclareDate()));
		Assertions.assertEquals("06/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("05/31/2012", DateUtils.fromDateShort(event.getRecordDate()));
		Assertions.assertEquals("06/07/2012", DateUtils.fromDateShort(event.getEventDate()));

		event = events.get(1);
		Assertions.assertEquals("05/31/2012", DateUtils.fromDateShort(event.getDeclareDate()));
		Assertions.assertEquals("09/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("08/31/2012", DateUtils.fromDateShort(event.getRecordDate()));
		Assertions.assertEquals("09/06/2012", DateUtils.fromDateShort(event.getEventDate()));

		event = events.get(2);
		Assertions.assertEquals("08/31/2012", DateUtils.fromDateShort(event.getDeclareDate()));
		Assertions.assertEquals("12/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("11/30/2012", DateUtils.fromDateShort(event.getRecordDate()));
		Assertions.assertEquals("12/05/2012", DateUtils.fromDateShort(event.getEventDate()));

		event = events.get(3);
		Assertions.assertEquals("11/30/2012", DateUtils.fromDateShort(event.getDeclareDate()));
		Assertions.assertEquals("03/01/2013", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("02/28/2013", DateUtils.fromDateShort(event.getRecordDate()));
		Assertions.assertEquals("03/05/2013", DateUtils.fromDateShort(event.getEventDate()));
	}


	@Test
	public void testInterestLegPaymentSequence_NET6885445() {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);
		Calendar uk_calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_UK);
		Calendar combined = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_COMBINED);

		InterestLegPaymentDateCalculator dateCalculator = configureDateCalculator(new InterestLegPaymentDateCalculator());

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(uk_calendar);
		sequence.setSettlementCalendar(combined);

		sequence.setSequenceStartDate(DateUtils.toDate("02/29/2012"));
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Quarterly"));
		sequence.setSettlementCycle(3);
		sequence.setDateCalculator(dateCalculator);
		sequence.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.INTEREST_LEG_PAYMENT));
		sequence.setFixingCycle(1);

		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(17747, "NET6885445", "Swaps"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("03/01/2013"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("03/05/2013"));

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(4, events.size());

		InvestmentSecurityEvent event = events.get(0);
		Assertions.assertEquals("03/05/2012", DateUtils.fromDateShort(event.getAccrualStartDate()));
		Assertions.assertEquals("06/07/2012", DateUtils.fromDateShort(event.getAccrualEndDate()));
		Assertions.assertEquals("06/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("06/07/2012", DateUtils.fromDateShort(event.getPaymentDate()));
		Assertions.assertEquals("03/06/2012", DateUtils.fromDateShort(event.getAdditionalDate()));

		event = events.get(1);
		Assertions.assertEquals("06/07/2012", DateUtils.fromDateShort(event.getAccrualStartDate()));
		Assertions.assertEquals("09/06/2012", DateUtils.fromDateShort(event.getAccrualEndDate()));
		Assertions.assertEquals("09/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("09/06/2012", DateUtils.fromDateShort(event.getPaymentDate()));
		Assertions.assertEquals("06/08/2012", DateUtils.fromDateShort(event.getAdditionalDate()));

		event = events.get(2);
		Assertions.assertEquals("09/06/2012", DateUtils.fromDateShort(event.getAccrualStartDate()));
		Assertions.assertEquals("12/05/2012", DateUtils.fromDateShort(event.getAccrualEndDate()));
		Assertions.assertEquals("12/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("12/05/2012", DateUtils.fromDateShort(event.getPaymentDate()));
		Assertions.assertEquals("09/07/2012", DateUtils.fromDateShort(event.getAdditionalDate()));

		event = events.get(3);
		Assertions.assertEquals("12/05/2012", DateUtils.fromDateShort(event.getAccrualStartDate()));
		Assertions.assertEquals("03/05/2013", DateUtils.fromDateShort(event.getAccrualEndDate()));
		Assertions.assertEquals("03/01/2013", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("03/05/2013", DateUtils.fromDateShort(event.getPaymentDate()));
		Assertions.assertEquals("12/06/2012", DateUtils.fromDateShort(event.getAdditionalDate()));
	}


	@Test
	public void testInterestLegPaymentSequenceWithPreviousEvent_NET6885445() {
		Calendar calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);
		Calendar uk_calendar = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_UK);
		Calendar combined = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_COMBINED);

		InterestLegPaymentDateCalculator dateCalculator = configureDateCalculator(new InterestLegPaymentDateCalculator());

		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(calendar);
		sequence.setFixingCalendar(uk_calendar);
		sequence.setSettlementCalendar(combined);

		sequence.setSequenceStartDate(DateUtils.toDate("02/29/2012"));
		sequence.setFirstSequenceEventExist(true);
		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Quarterly"));
		sequence.setSettlementCycle(3);
		sequence.setDateCalculator(dateCalculator);
		sequence.setType(InvestmentTestObjectFactory.newInvestmentSecurityEventType(InvestmentSecurityEventType.INTEREST_LEG_PAYMENT));
		sequence.setFixingCycle(1);

		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(17747, "NET6885445", "Swaps"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("03/01/2013"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("03/05/2013"));

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(4, events.size());

		InvestmentSecurityEvent event = events.get(0);
		Assertions.assertEquals("03/05/2012", DateUtils.fromDateShort(event.getAccrualStartDate()));
		Assertions.assertEquals("06/07/2012", DateUtils.fromDateShort(event.getAccrualEndDate()));
		Assertions.assertEquals("06/01/2012", DateUtils.fromDateShort(event.getExDate()));
		Assertions.assertEquals("06/07/2012", DateUtils.fromDateShort(event.getPaymentDate()));
		Assertions.assertEquals("03/06/2012", DateUtils.fromDateShort(event.getAdditionalDate()));
	}
}
