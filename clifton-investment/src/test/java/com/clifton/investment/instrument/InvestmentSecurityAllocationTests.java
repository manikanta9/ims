package com.clifton.investment.instrument;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationService;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * A set of tests designed to test the exchange overrides on an InvestmentSecurityAllocation entity.
 *
 * @author davidi
 */
@InvestmentInMemoryDatabaseContext
public class InvestmentSecurityAllocationTests extends BaseInMemoryDatabaseTests {

	private final static String PARENT_SECURITY_SYMBOL = "CGCBDM12";

	@Resource
	private InvestmentSecurityAllocationService investmentSecurityAllocationService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOverrideExchangePropertyState_exchange_override_not_exists() {
		List<InvestmentSecurityAllocation> securityAllocationList = getSecurityAllocationList(PARENT_SECURITY_SYMBOL, "AAPL");
		Assertions.assertEquals(1, securityAllocationList.size(), "Expected one security for symbol: AAPL");
		Assertions.assertEquals("AAPL", securityAllocationList.get(0).getInvestmentSecurity().getSymbol(), "Symbol AAPL expected.");

		Assertions.assertFalse(securityAllocationList.get(0).isExchangeOverridden());
	}


	@Test
	public void testOverrideExchangePropertyState_exchange_override_exists() {
		List<InvestmentSecurityAllocation> securityAllocationList = getSecurityAllocationList(PARENT_SECURITY_SYMBOL, "CSCO");
		Assertions.assertEquals(1, securityAllocationList.size(), "Expected one security for symbol: CSCO");
		Assertions.assertEquals("CSCO", securityAllocationList.get(0).getInvestmentSecurity().getSymbol(), "Symbol CSCO expected.");

		Assertions.assertTrue(securityAllocationList.get(0).isExchangeOverridden());
	}


	@Test
	public void testOverrideExchangePropertyState_exchange_override_exists_exchange_equals_override_exchange() {
		List<InvestmentSecurityAllocation> securityAllocationList = getSecurityAllocationList(PARENT_SECURITY_SYMBOL, "CSCO");
		Assertions.assertEquals(1, securityAllocationList.size(), "Expected one security for symbol: CSCO");
		Assertions.assertEquals("CSCO", securityAllocationList.get(0).getInvestmentSecurity().getSymbol(), "Symbol CSCO expected.");

		Assertions.assertEquals(securityAllocationList.get(0).getExchange(), securityAllocationList.get(0).getOverrideExchange());
	}


	@Test
	public void testSearchForm_search_by_override_exchange_name() {
		InvestmentSecurity parentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol(PARENT_SECURITY_SYMBOL, false);
		SecurityAllocationSearchForm searchForm = new SecurityAllocationSearchForm();
		searchForm.setParentInvestmentSecurityId(parentSecurity.getId());
		searchForm.setOverrideExchangeName("NASDAQ Global Select");
		List<InvestmentSecurityAllocation> securityAllocationList = this.investmentSecurityAllocationService.getInvestmentSecurityAllocationList(searchForm);

		final String expectedSymbol = "CMCSA";
		Assertions.assertEquals(1, securityAllocationList.size(), "Expected one security for symbol: " + expectedSymbol);
		Assertions.assertEquals(expectedSymbol, securityAllocationList.get(0).getInvestmentSecurity().getSymbol(), "Investment Security Symbol should be: " + expectedSymbol + " but is: " + securityAllocationList.get(0).getInvestmentSecurity().getSymbol());
	}


	@Test
	public void testSearchForm_search_by_override_exchange_code() {
		InvestmentSecurity parentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol(PARENT_SECURITY_SYMBOL, false);
		SecurityAllocationSearchForm searchForm = new SecurityAllocationSearchForm();
		searchForm.setParentInvestmentSecurityId(parentSecurity.getId());
		searchForm.setOverrideExchangeCode("UW");
		List<InvestmentSecurityAllocation> securityAllocationList = this.investmentSecurityAllocationService.getInvestmentSecurityAllocationList(searchForm);

		final String expectedSymbol = "CMCSA";
		Assertions.assertEquals(1, securityAllocationList.size(), "Expected one security for symbol: " + expectedSymbol);
		Assertions.assertEquals(expectedSymbol, securityAllocationList.get(0).getInvestmentSecurity().getSymbol(), "Investment Security Symbol should be: " + expectedSymbol + " but is: " + securityAllocationList.get(0).getInvestmentSecurity().getSymbol());
	}


	@Test
	public void testSearchForm_search_by_override_exchange_id() {
		InvestmentSecurity parentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol(PARENT_SECURITY_SYMBOL, false);
		SecurityAllocationSearchForm searchForm = new SecurityAllocationSearchForm();
		searchForm.setParentInvestmentSecurityId(parentSecurity.getId());
		short exchangeId = 47;
		searchForm.setOverrideExchangeId(exchangeId);
		List<InvestmentSecurityAllocation> securityAllocationList = this.investmentSecurityAllocationService.getInvestmentSecurityAllocationList(searchForm);

		final String expectedSymbol = "CMCSA";
		Assertions.assertEquals(1, securityAllocationList.size(), "Expected one security for symbol: " + expectedSymbol);
		Assertions.assertEquals(expectedSymbol, securityAllocationList.get(0).getInvestmentSecurity().getSymbol(), "Investment Security Symbol should be: " + expectedSymbol + " but is: " + securityAllocationList.get(0).getInvestmentSecurity().getSymbol());
	}


	@Test
	public void testSearchForm_search_by_isExchangeOverridden_true() {
		InvestmentSecurity parentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol(PARENT_SECURITY_SYMBOL, false);
		SecurityAllocationSearchForm searchForm = new SecurityAllocationSearchForm();
		searchForm.setParentInvestmentSecurityId(parentSecurity.getId());
		boolean isExchangeOverridden = true;
		searchForm.setExchangeOverridden(isExchangeOverridden);
		List<InvestmentSecurityAllocation> securityAllocationList = this.investmentSecurityAllocationService.getInvestmentSecurityAllocationList(searchForm);

		final Set<String> expectedSymbols = CollectionUtils.createHashSet("CMCSA", "CSCO");
		Assertions.assertEquals(2, securityAllocationList.size(), "Expected two allocation entries with securities: CMCSA and CSCO.");
		final Set<String> actualSymbols = CollectionUtils.createHashSet(securityAllocationList.get(0).getInvestmentSecurity().getSymbol(), securityAllocationList.get(1).getInvestmentSecurity().getSymbol());
		Assertions.assertEquals(expectedSymbols, actualSymbols, "Investment Security Symbols should be: " + expectedSymbols + " but are: " + actualSymbols);
	}


	@Test
	public void testSearchForm_search_by_isExchangeOverridden_false() {
		InvestmentSecurity parentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol(PARENT_SECURITY_SYMBOL, false);
		SecurityAllocationSearchForm searchForm = new SecurityAllocationSearchForm();
		searchForm.setParentInvestmentSecurityId(parentSecurity.getId());
		boolean isExchangeOverridden = false;
		searchForm.setExchangeOverridden(isExchangeOverridden);
		List<InvestmentSecurityAllocation> securityAllocationList = this.investmentSecurityAllocationService.getInvestmentSecurityAllocationList(searchForm);

		final List<String> excludedSymbols = CollectionUtils.createList("CMCSA", "CSCO");
		final Set<String> actualSymbols = CollectionUtils.getStream(securityAllocationList).map(allocation -> allocation.getInvestmentSecurity().getSymbol()).collect(Collectors.toSet());

		Assertions.assertFalse(actualSymbols.contains(excludedSymbols.get(0)) || actualSymbols.contains(excludedSymbols.get(1)), "Investment security symbols CMSCA and CSCO are not expected in the result set.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<InvestmentSecurityAllocation> getSecurityAllocationList(String parentSymbol, String symbol) {
		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol(symbol, false);
		Assertions.assertNotNull(investmentSecurity, "Cannot find investment security for symbol: " + symbol);
		InvestmentSecurity parentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol(parentSymbol, false);
		Assertions.assertNotNull(parentSecurity, "Cannot find investment security for symbol: " + symbol);

		SecurityAllocationSearchForm securityAllocationSearchForm = new SecurityAllocationSearchForm();
		securityAllocationSearchForm.setParentInvestmentSecurityId(parentSecurity.getId());
		securityAllocationSearchForm.setInvestmentSecurityId(investmentSecurity.getId());

		return this.investmentSecurityAllocationService.getInvestmentSecurityAllocationList(securityAllocationSearchForm);
	}
}
