SELECT 'InvestmentSecurityEventAction' AS entityTableName, InvestmentSecurityEventActionID AS entityId
FROM InvestmentSecurityEventAction
WHERE InvestmentSecurityEventID IN (SELECT InvestmentSecurityEventID
									FROM InvestmentSecurityEvent
									WHERE EventDate BETWEEN '2021-11-02' AND '2021-12-21');

