package com.clifton.investment.instrument.calculator.accrual;

import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * @author NickK
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentAccrualDailyCompoundedAverageCalculatorTests {

	private static final int INTEREST_LEG_EVENT_100 = 100;

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private InvestmentAccrualDailyCompoundedAverageCalculator investmentAccrualDailyCompoundedAverageCalculator;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private SystemColumnValueHandler getSystemColumnServiceMock(InvestmentSecurity security, String frequency, String dayCountConvention) {
		SystemColumnValueHandler systemColumnValueHandler = Mockito.mock(SystemColumnValueHandler.class);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY, false))
				.thenReturn(frequency);
		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, "Reset Frequency", false))
				.thenReturn(frequency);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION, false))
				.thenReturn(dayCountConvention);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CALENDAR, false))
				.thenReturn(this.calendarSetupService.getCalendarByName("Test Calendar").getId());

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CYCLE, false))
				.thenReturn(3);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_FIXING_CYCLE, false))
				.thenReturn(-2);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_FIXING_CALENDAR, false))
				.thenReturn(this.calendarSetupService.getCalendarByName("Federal Reserve").getId());

		return systemColumnValueHandler;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalculateAccruedInterest_InterestLeg_WithDetails() {
		InvestmentSecurityEvent interestLeg = this.investmentSecurityEventService.getInvestmentSecurityEvent(INTEREST_LEG_EVENT_100);
		this.investmentAccrualDailyCompoundedAverageCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(interestLeg.getSecurity(), "QUARTERLY", "Actual/360"));

		int notional = 5000000;

		String[] accrualDates = {
				"12/20/2021",
				"12/21/2021",
				"12/22/2021",
				"12/23/2021",
				"12/24/2021",
				"12/25/2021",
				"12/26/2021",
				"12/27/2021",
				"12/28/2021",
				"12/29/2021",
				"12/30/2021",
				"12/31/2021",
				"1/1/2022",
				"1/2/2022",
				"1/3/2022",
				"1/4/2022",
				"1/5/2022",
				"1/6/2022",
				"1/7/2022",
				"1/8/2022",
				"1/9/2022",
				"1/10/2022",
				"1/11/2022",
				"1/12/2022",
				"1/13/2022",
				"1/14/2022",
				"1/15/2022",
				"1/16/2022",
				"1/17/2022",
				"1/18/2022",
				"1/19/2022",
				"1/20/2022",
				"1/21/2022",
				"1/22/2022",
				"1/23/2022",
				"1/24/2022",
				"1/25/2022"
		};

		double[] expectedAccruals = {
				0.0,
				6.955374442,
				20.86612333,
				40.86882086,
				67.73095664,
				100.5891049,
				140.0188827,
				186.0202903,
				238.5933274,
				299.975052,
				368.2896205,
				443.5404992,
				525.6892961,
				614.7788485,
				710.7214435,
				813.5170809,
				923.3960077,
				1040.221052,
				1163.993139,
				1294.713063,
				1432.381509,
				1576.933377,
				1728.368668,
				1886.841838,
				2052.265364,
				2224.595203,
				2403.875976,
				2590.107987,
				2783.237481,
				2983.264456,
				3190.188913,
				3404.183327,
				3625.130037,
				3853.029219,
				4086.521939,
				4326.882091,
				4574.109677
		};

		for (int i = 0; i < accrualDates.length; i++) {
			assertAccrual(interestLeg, notional, accrualDates[i], expectedAccruals[i]);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void assertAccrual(InvestmentSecurityEvent paymentEvent, int notional, String accrualDate, double expectedAccrual) {
		BigDecimal accrual = this.investmentAccrualDailyCompoundedAverageCalculator.calculateAccruedInterest(paymentEvent, BigDecimal.valueOf(notional), BigDecimal.ONE, DateUtils.toDate(accrualDate));
		BigDecimal expected = MathUtils.round(BigDecimal.valueOf(expectedAccrual), 2);
		Assertions.assertTrue(MathUtils.isEqual(expected, accrual), String.format("Accrual calculation failed on %s. Expected %s but received %s", accrualDate, expected, accrual));
	}
}
