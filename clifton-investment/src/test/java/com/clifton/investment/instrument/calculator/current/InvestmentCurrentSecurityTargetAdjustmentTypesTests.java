package com.clifton.investment.instrument.calculator.current;

import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * InvestmentCurrentSecurityTargetAdjustmentTypesTests tests the target adjustment calculations
 *
 * @author manderson
 */
public class InvestmentCurrentSecurityTargetAdjustmentTypesTests {

	////////////////////////////////////////////////////////////////////////////////
	/////    DIFFERENCE_ALL
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFiveSecurities_Difference_All() {
		// Using 382200: Loyola from 9/17/2015 Run As Test Case
		// One Replication Allocation for Security Group with T-Notes
		// 5 Securities apply, and Since total target = total actual, they all end up with TFA
		BigDecimal totalTarget = BigDecimal.valueOf(30922317);
		Map<InvestmentSecurity, BigDecimal> actualExposureMap = new HashMap<>();
		Map<InvestmentSecurity, BigDecimal> expectedTargetMap = new HashMap<>();

		setupSecurity(1, "912828ND8", 7074807.0, 7074807.0, actualExposureMap, expectedTargetMap);
		InvestmentSecurity currentSecurity = setupSecurity(2, "912828F21", 10001602.0, 10001602.0, actualExposureMap, expectedTargetMap);
		setupSecurity(3, "912828PY0", 3451336.0, 3451336.0, actualExposureMap, expectedTargetMap);
		setupSecurity(4, "912828SH4", 5086691.0, 5086691.0, actualExposureMap, expectedTargetMap);
		setupSecurity(5, "912828B58", 5307881.0, 5307881.0, actualExposureMap, expectedTargetMap);

		Map<InvestmentSecurity, BigDecimal> resultTargetMap = InvestmentCurrentSecurityTargetAdjustmentTypes.DIFFERENCE_ALL.calculateTargetAmountMap(totalTarget, currentSecurity, actualExposureMap);
		validateResults(expectedTargetMap, resultTargetMap);
	}


	@Test
	public void testTwoSecurities_Difference_All() {
		// Using 455000 Fixed Income from 9/17/2015 Run As Test Case
		BigDecimal totalTarget = BigDecimal.valueOf(134824992);
		Map<InvestmentSecurity, BigDecimal> actualExposureMap = new HashMap<>();
		Map<InvestmentSecurity, BigDecimal> expectedTargetMap = new HashMap<>();

		InvestmentSecurity currentSecurity = setupSecurity(1, "NYK0FA6327F", 38532400.0, 57760192, actualExposureMap, expectedTargetMap);
		setupSecurity(2, "LHY3322786", 77064800, 77064800, actualExposureMap, expectedTargetMap);

		Map<InvestmentSecurity, BigDecimal> resultTargetMap = InvestmentCurrentSecurityTargetAdjustmentTypes.DIFFERENCE_ALL.calculateTargetAmountMap(totalTarget, currentSecurity, actualExposureMap);
		validateResults(expectedTargetMap, resultTargetMap);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////    DIFFERENCE_OPENING
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testTwoSecurities_DifferenceOpening_OpenPositions_Shorts() {
		// Using 183050 Equity from 9/17/2015 Run Except using opening calculator
		BigDecimal totalTarget = BigDecimal.valueOf(-5663585);
		Map<InvestmentSecurity, BigDecimal> actualExposureMap = new HashMap<>();
		Map<InvestmentSecurity, BigDecimal> expectedTargetMap = new HashMap<>();

		InvestmentSecurity currentSecurity = setupSecurity(1, "2DV5C 2020", -2471500, -2697785, actualExposureMap, expectedTargetMap);
		setupSecurity(2, "2DV5C 2025", -2965800, -2965800, actualExposureMap, expectedTargetMap);

		Map<InvestmentSecurity, BigDecimal> resultTargetMap = InvestmentCurrentSecurityTargetAdjustmentTypes.DIFFERENCE_OPENING.calculateTargetAmountMap(totalTarget, currentSecurity, actualExposureMap);
		validateResults(expectedTargetMap, resultTargetMap);
	}


	@Test
	public void testTwoSecurities_DifferenceOpening_ClosePositions_Shorts() {
		BigDecimal totalTarget = BigDecimal.valueOf(-5000000);
		Map<InvestmentSecurity, BigDecimal> actualExposureMap = new HashMap<>();
		Map<InvestmentSecurity, BigDecimal> expectedTargetMap = new HashMap<>();

		InvestmentSecurity currentSecurity = setupSecurity(1, "2DV5C 2020", -2471500, -2471500, actualExposureMap, expectedTargetMap);
		setupSecurity(2, "2DV5C 2025", -2965800, -2528500, actualExposureMap, expectedTargetMap);

		Map<InvestmentSecurity, BigDecimal> resultTargetMap = InvestmentCurrentSecurityTargetAdjustmentTypes.DIFFERENCE_OPENING.calculateTargetAmountMap(totalTarget, currentSecurity, actualExposureMap);
		validateResults(expectedTargetMap, resultTargetMap);
	}


	@Test
	public void testTwoSecurities_DifferenceOpening_OpenPositions_Longs() {
		// Using 183050 Equity from 9/17/2015 Run Except using opening calculator
		BigDecimal totalTarget = BigDecimal.valueOf(5663585);
		Map<InvestmentSecurity, BigDecimal> actualExposureMap = new HashMap<>();
		Map<InvestmentSecurity, BigDecimal> expectedTargetMap = new HashMap<>();

		InvestmentSecurity currentSecurity = setupSecurity(1, "2DV5C 2020", 2471500, 2697785, actualExposureMap, expectedTargetMap);
		setupSecurity(2, "2DV5C 2025", 2965800, 2965800, actualExposureMap, expectedTargetMap);

		Map<InvestmentSecurity, BigDecimal> resultTargetMap = InvestmentCurrentSecurityTargetAdjustmentTypes.DIFFERENCE_OPENING.calculateTargetAmountMap(totalTarget, currentSecurity, actualExposureMap);
		validateResults(expectedTargetMap, resultTargetMap);
	}


	@Test
	public void testTwoSecurities_DifferenceOpening_OpenPositions_ShortToLong() {
		BigDecimal totalTarget = BigDecimal.valueOf(5397561);
		Map<InvestmentSecurity, BigDecimal> actualExposureMap = new HashMap<>();
		Map<InvestmentSecurity, BigDecimal> expectedTargetMap = new HashMap<>();

		InvestmentSecurity currentSecurity = setupSecurity(1, "LAZ17", 0, 5397561, actualExposureMap, expectedTargetMap);
		setupSecurity(2, "LAX17", -2166337, 0, actualExposureMap, expectedTargetMap);

		Map<InvestmentSecurity, BigDecimal> resultTargetMap = InvestmentCurrentSecurityTargetAdjustmentTypes.DIFFERENCE_OPENING.calculateTargetAmountMap(totalTarget, currentSecurity, actualExposureMap);
		validateResults(expectedTargetMap, resultTargetMap);
	}


	@Test
	public void testTwoSecurities_DifferenceOpening_OpenPositions_LongToShort() {
		BigDecimal totalTarget = BigDecimal.valueOf(-2159024);
		Map<InvestmentSecurity, BigDecimal> actualExposureMap = new HashMap<>();
		Map<InvestmentSecurity, BigDecimal> expectedTargetMap = new HashMap<>();

		InvestmentSecurity currentSecurity = setupSecurity(1, "COF8", 0, -2159024, actualExposureMap, expectedTargetMap);
		setupSecurity(2, "COZ7", 2374720, 0, actualExposureMap, expectedTargetMap);

		Map<InvestmentSecurity, BigDecimal> resultTargetMap = InvestmentCurrentSecurityTargetAdjustmentTypes.DIFFERENCE_OPENING.calculateTargetAmountMap(totalTarget, currentSecurity, actualExposureMap);
		validateResults(expectedTargetMap, resultTargetMap);
	}


	@Test
	public void testTwoSecurities_DifferenceOpening_ClosePositions_Longs() {
		BigDecimal totalTarget = BigDecimal.valueOf(5000000);
		Map<InvestmentSecurity, BigDecimal> actualExposureMap = new HashMap<>();
		Map<InvestmentSecurity, BigDecimal> expectedTargetMap = new HashMap<>();

		InvestmentSecurity currentSecurity = setupSecurity(1, "2DV5C 2020", 2471500, 2471500, actualExposureMap, expectedTargetMap);
		setupSecurity(2, "2DV5C 2025", 2965800, 2528500, actualExposureMap, expectedTargetMap);

		Map<InvestmentSecurity, BigDecimal> resultTargetMap = InvestmentCurrentSecurityTargetAdjustmentTypes.DIFFERENCE_OPENING.calculateTargetAmountMap(totalTarget, currentSecurity, actualExposureMap);
		validateResults(expectedTargetMap, resultTargetMap);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////    ALL
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testTwoSecurities_All_OpenPositions() {
		BigDecimal totalTarget = BigDecimal.valueOf(500000);
		Map<InvestmentSecurity, BigDecimal> actualExposureMap = new HashMap<>();
		Map<InvestmentSecurity, BigDecimal> expectedTargetMap = new HashMap<>();

		InvestmentSecurity currentSecurity = setupSecurity(1, "LHJ6", 0, 500000, actualExposureMap, expectedTargetMap);
		setupSecurity(2, "LHG6", 300000, 0, actualExposureMap, expectedTargetMap);

		Map<InvestmentSecurity, BigDecimal> resultTargetMap = InvestmentCurrentSecurityTargetAdjustmentTypes.ALL.calculateTargetAmountMap(totalTarget, currentSecurity, actualExposureMap);
		validateResults(expectedTargetMap, resultTargetMap);
	}


	@Test
	public void testTwoSecurities_All_ClosePositions() {
		BigDecimal totalTarget = BigDecimal.valueOf(300000);
		Map<InvestmentSecurity, BigDecimal> actualExposureMap = new HashMap<>();
		Map<InvestmentSecurity, BigDecimal> expectedTargetMap = new HashMap<>();

		InvestmentSecurity currentSecurity = setupSecurity(1, "LHJ6", 0, 300000, actualExposureMap, expectedTargetMap);
		setupSecurity(2, "LHG6", 500000, 0, actualExposureMap, expectedTargetMap);

		Map<InvestmentSecurity, BigDecimal> resultTargetMap = InvestmentCurrentSecurityTargetAdjustmentTypes.ALL.calculateTargetAmountMap(totalTarget, currentSecurity, actualExposureMap);
		validateResults(expectedTargetMap, resultTargetMap);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private InvestmentSecurity setupSecurity(int securityId, String symbol, double actualExposure, double expectedTarget, Map<InvestmentSecurity, BigDecimal> actualExposureMap, Map<InvestmentSecurity, BigDecimal> expectedTargetMap) {
		InvestmentSecurity security = new InvestmentSecurity();
		security.setId(securityId);
		security.setSymbol(symbol);
		actualExposureMap.put(security, BigDecimal.valueOf(actualExposure));
		expectedTargetMap.put(security, BigDecimal.valueOf(expectedTarget));
		return security;
	}


	private void validateResults(Map<InvestmentSecurity, BigDecimal> expectedTargetMap, Map<InvestmentSecurity, BigDecimal> resultTargetMap) {
		for (Map.Entry<InvestmentSecurity, BigDecimal> investmentSecurityBigDecimalEntry : expectedTargetMap.entrySet()) {
			BigDecimal expected = investmentSecurityBigDecimalEntry.getValue();
			BigDecimal actual = resultTargetMap.get(investmentSecurityBigDecimalEntry.getKey());
			Assertions.assertEquals(CoreMathUtils.formatNumberInteger(expected), CoreMathUtils.formatNumberInteger(actual));
		}
	}
}
