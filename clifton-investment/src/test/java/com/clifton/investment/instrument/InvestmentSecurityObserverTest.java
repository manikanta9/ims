package com.clifton.investment.instrument;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Date;


public class InvestmentSecurityObserverTest {

	private static final int IBM_SECURITY_ID = 11111;
	private static final int IBM_MATURITY_EVENT_ID = 22222;
	private static final Date IBM_END_DATE = DateUtils.toDate("1/1/2011");

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInsertNewSecurity_WithEndDate_CreatesMaturityEvent() {
		Date newEndDate = DateUtils.addDays(IBM_END_DATE, 100);

		InvestmentSecurityObserver securityObserver = newInvestmentSecurityObserver(false);
		securityObserver.afterMethodCallImpl(null, DaoEventTypes.INSERT, newSecurityIBM(newEndDate), null);

		verifyMaturityEventSaveForIBM(securityObserver.getInvestmentSecurityEventService(), newEndDate, 1);
	}


	@Test
	public void testInsertNewSecurity_WithEndDate_ButNoTradingOnEndDate_CreatesMaturityEvent() {
		Date endDate = new Date();
		InvestmentSecurity ibm = newSecurityIBM(endDate);
		ibm.getInstrument().getHierarchy().setTradingOnEndDateAllowed(false); // event's Ex Date will be same as Payment Date vs the Day After

		InvestmentSecurityObserver securityObserver = newInvestmentSecurityObserver(false);
		securityObserver.afterMethodCallImpl(null, DaoEventTypes.INSERT, ibm, null);

		verifyMaturityEventSaveForIBM(securityObserver.getInvestmentSecurityEventService(), endDate, 0);
	}


	@Test
	public void testUpdateSecurity_WithEndDate_UpdateEndDate_UpdatesMaturityEvent() {
		Date newEndDate = DateUtils.addDays(IBM_END_DATE, 100);

		InvestmentSecurityObserver securityObserver = newInvestmentSecurityObserver(true);
		securityObserver.afterMethodCallImpl(null, DaoEventTypes.UPDATE, newSecurityIBM(newEndDate), null);

		verifyMaturityEventSaveForIBM(securityObserver.getInvestmentSecurityEventService(), newEndDate, 1);
	}


	@Test
	public void testUpdateSecurity_WithEndDate_ButNotEndDate_DoesNotUpdateMaturityEvent() {
		InvestmentSecurityObserver securityObserver = newInvestmentSecurityObserver(true);
		securityObserver.afterMethodCallImpl(null, DaoEventTypes.UPDATE, newSecurityIBM(IBM_END_DATE), null);

		Mockito.verify(securityObserver.getInvestmentSecurityEventService()).getInvestmentSecurityEventList(ArgumentMatchers.any(InvestmentSecurityEventSearchForm.class));
		Mockito.verify(securityObserver.getInvestmentSecurityEventService()).getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.SECURITY_MATURITY);
		Mockito.verify(securityObserver.getInvestmentSecurityEventService()).getInvestmentSecurityEventTypeListByHierarchy(ArgumentMatchers.anyShort());
		Mockito.verifyNoMoreInteractions(securityObserver.getInvestmentSecurityEventService());
	}


	@Test
	public void testUpdateSecurity_ClearEndDate_DeletesMaturityEvent() {
		InvestmentSecurityObserver securityObserver = newInvestmentSecurityObserver(true);
		securityObserver.afterMethodCallImpl(null, DaoEventTypes.UPDATE, newSecurityIBM(null), null);

		ArgumentCaptor<Integer> argument = ArgumentCaptor.forClass(Integer.class);
		Mockito.verify(securityObserver.getInvestmentSecurityEventService()).deleteInvestmentSecurityEvent(argument.capture());
		Assertions.assertEquals(IBM_MATURITY_EVENT_ID, argument.getValue().intValue());
	}


	@Test
	public void testDeleteSecurity_WithEndDate_DeletesMaturityEvent() {
		InvestmentSecurityObserver securityObserver = newInvestmentSecurityObserver(true);
		securityObserver.beforeMethodCallImpl(null, DaoEventTypes.DELETE, newSecurityIBM(IBM_END_DATE));

		ArgumentCaptor<Integer> argument = ArgumentCaptor.forClass(Integer.class);
		Mockito.verify(securityObserver.getInvestmentSecurityEventService()).deleteInvestmentSecurityEvent(argument.capture());
		Assertions.assertEquals(IBM_MATURITY_EVENT_ID, argument.getValue().intValue());
	}


	@Test
	public void testDeleteSecurity_WithNoEndDate_DoesNotDeleteMaturityEvent() {
		InvestmentSecurityObserver securityObserver = newInvestmentSecurityObserver(false);
		securityObserver.beforeMethodCallImpl(null, DaoEventTypes.DELETE, newSecurityIBM(null));

		Mockito.verify(securityObserver.getInvestmentSecurityEventService()).getInvestmentSecurityEventList(ArgumentMatchers.any(InvestmentSecurityEventSearchForm.class));
		Mockito.verifyNoMoreInteractions(securityObserver.getInvestmentSecurityEventService());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentSecurityObserver newInvestmentSecurityObserver(boolean mockMaturityEvent) {
		InvestmentSecurityObserver securityObserver = new InvestmentSecurityObserver();
		securityObserver.setInvestmentSecurityEventService(Mockito.mock(InvestmentSecurityEventService.class));
		securityObserver.setInvestmentSecurityGroupService(Mockito.mock(InvestmentSecurityGroupService.class));


		InvestmentSecurityEventType maturityEventType = InvestmentSecurityEventType.ofTypeName(InvestmentSecurityEventType.SECURITY_MATURITY);
		Mockito.when(securityObserver.getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.SECURITY_MATURITY))
				.thenReturn(maturityEventType);
		Mockito.when(securityObserver.getInvestmentSecurityEventService().getInvestmentSecurityEventTypeListByHierarchy(ArgumentMatchers.anyShort()))
				.thenReturn(CollectionUtils.createList(maturityEventType));

		if (mockMaturityEvent) {
			InvestmentSecurityEvent maturityEvent = new InvestmentSecurityEvent();
			maturityEvent.setId(IBM_MATURITY_EVENT_ID);
			maturityEvent.setEventDate(IBM_END_DATE);
			maturityEvent.setBeforeAndAfterEventValue(BigDecimal.ZERO);
			maturityEvent.setType(maturityEventType);
			maturityEvent.setSecurity(newSecurityIBM(IBM_END_DATE));

			Mockito.when(securityObserver.getInvestmentSecurityEventService().getInvestmentSecurityEventList(ArgumentMatchers.any(InvestmentSecurityEventSearchForm.class)))
					.thenReturn(CollectionUtils.createList(maturityEvent));
		}

		return securityObserver;
	}


	private InvestmentSecurity newSecurityIBM(Date endDate) {
		InvestmentSecurity ibm = InvestmentTestObjectFactory.newInvestmentSecurity(IBM_SECURITY_ID, "IBM", InvestmentType.STOCKS);
		ibm.getInstrument().getHierarchy().setTradingOnEndDateAllowed(true);
		ibm.setEndDate(endDate);
		return ibm;
	}


	private void verifyMaturityEventSaveForIBM(InvestmentSecurityEventService investmentSecurityEventService, Date endDate, int exDateOffsetFromEndDate) {
		Mockito.verify(investmentSecurityEventService).getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.SECURITY_MATURITY);

		ArgumentCaptor<InvestmentSecurityEvent> argument = ArgumentCaptor.forClass(InvestmentSecurityEvent.class);
		Mockito.verify(investmentSecurityEventService).saveInvestmentSecurityEvent(argument.capture());
		InvestmentSecurityEvent maturityEvent = argument.getValue();

		Assertions.assertEquals(endDate, maturityEvent.getDeclareDate());
		Assertions.assertEquals(endDate, maturityEvent.getEventDate());
		Assertions.assertEquals(DateUtils.addDays(endDate, exDateOffsetFromEndDate), maturityEvent.getExDate());
		Assertions.assertEquals(endDate, maturityEvent.getRecordDate());
		Assertions.assertEquals(IBM_SECURITY_ID, maturityEvent.getSecurity().getId().intValue());
		Assertions.assertEquals(0, maturityEvent.getAfterEventValue().doubleValue(), 0);
		Assertions.assertEquals(0, maturityEvent.getBeforeEventValue().doubleValue(), 0);
	}
}
