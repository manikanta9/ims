package com.clifton.investment.instrument.calculator;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class DayCountConventionTests {

	@Test
	public void test30_360_Convention() {
		// NOTE that End Date is not inclusive so we always add 1 calendar day to end date before passing to calculateAccrualRate method

		// Feb 1,2010 to April 1, 2010 has 60 days for this convention: 60/360
		DayCountConventions convention = DayCountConventions.THRITY_THREESIXTY;
		Assertions.assertEquals(0.0777777777777777,
				convention.calculateAccrualRate(
						DayCountCommand.forAccrualRange(DateUtils.toDate("04/01/2011"), DateUtils.toDate("04/29/2011")).withFrequency(12)
				).doubleValue(), 0.000001);

		// 05947UR59: special case for February: need to use 30 days
		Assertions.assertEquals(0.0833333333333333,
				convention.calculateAccrualRate(
						DayCountCommand.forAccrualRange(DateUtils.toDate("02/02/2015"), DateUtils.toDate("03/02/2015")).withFrequency(12)
				).doubleValue(), 0.000001);

		// Accrued Interest: IRS Fixed Leg: USD3L-20141104-20161104-0.714
		BigDecimal rate = new BigDecimal("0.00714");
		BigDecimal notional = new BigDecimal("52000000");
		Assertions.assertEquals(56723.33, calculatedAccruedInterest30_360(notional, rate, "11/04/2014", "12/29/2014").doubleValue(), 0.000001);
		Assertions.assertEquals(57754.67, calculatedAccruedInterest30_360(notional, rate, "11/04/2014", "12/30/2014").doubleValue(), 0.000001);
		Assertions.assertEquals(58786.00, calculatedAccruedInterest30_360(notional, rate, "11/04/2014", "12/31/2014").doubleValue(), 0.000001);
		Assertions.assertEquals(116540.67, calculatedAccruedInterest30_360(notional, rate, "11/04/2014", "02/27/2015").doubleValue(), 0.000001);
	}


	private BigDecimal calculatedAccruedInterest30_360(BigDecimal notional, BigDecimal interestRate, String startDate, String endDate) {
		BigDecimal multiplier = interestRate.multiply(notional);
		return MathUtils.multiply(multiplier, DayCountConventions.THRITY_THREESIXTY.calculateAccrualRate(DayCountCommand.forAccrualRange(DateUtils.toDate(startDate), DateUtils.toDate(endDate)).withFrequency(12)), 2);
	}


	@Test
	public void test_Actual_360_Convention() {
		DayCountConventions convention = DayCountConventions.ACTUAL_THREESIXTY;
		//Feb 1,2010 to April 1, 2010 has 59 days.
		// 59/360
		Assertions.assertEquals(0.1638888888888889,
				convention.calculateAccrualRate(
						DayCountCommand.forAccrualRange(DateUtils.toDate("02/01/2010"), DateUtils.toDate("04/01/2010")).withAccrualPeriod(DateUtils.toDate("02/01/2010"), DateUtils.toDate("04/01/2010")).withFrequency(1)
				).doubleValue(), 0.0000001);
	}


	@Test
	public void test_Actual_365_Convention() {
		//Feb 1,2010 to April 1, 2010 has 59 days.
		// 59/365
		DayCountConventions convention = DayCountConventions.ACTUAL_THREESIXTYFIVE;
		Assertions.assertEquals(0.161643835,
				convention.calculateAccrualRate(
						DayCountCommand.forAccrualRange(DateUtils.toDate("02/01/2010"), DateUtils.toDate("04/01/2010")).withAccrualPeriod(DateUtils.toDate("02/01/2010"), DateUtils.toDate("04/01/2010")).withFrequency(1)
				).doubleValue(), 0.0000001);
	}


	@Test
	public void test_Actual_Actual_Convention() {
		//Feb 1,2010 to April 1, 2010 has 59 days.
		// 59/365
		DayCountConventions convention = DayCountConventions.ACTUAL_ACTUAL;
		Assertions.assertEquals(4.0 / (182 * 2),
				convention.calculateAccrualRate(
						DayCountCommand.forAccrualRange(DateUtils.toDate("04/15/2011"), DateUtils.toDate("04/19/2011")).withAccrualPeriod(DateUtils.toDate("04/15/2011"), DateUtils.toDate("10/14/2011")).withFrequency(2)
				).doubleValue(), 0.0000001);
	}


	@Test
	public void test_Actual_Actual_ISDA_Convention() {
		DayCountConventions convention = DayCountConventions.ACTUAL_ACTUAL_ISDA;
		Assertions.assertEquals(33.0 / 366.0,
				convention.calculateAccrualRate(
						DayCountCommand.forAccrualRange(DateUtils.toDate("07/29/2016"), DateUtils.toDate("08/31/2016")).withAccrualPeriod(DateUtils.toDate("07/29/2016"), DateUtils.toDate("08/31/2016")).withFrequency(2)
				).doubleValue(), 0.0000001);

		Assertions.assertEquals(30.0 / 366.0,
				convention.calculateAccrualRate(
						DayCountCommand.forAccrualRange(DateUtils.toDate("08/31/2016"), DateUtils.toDate("09/30/2016")).withAccrualPeriod(DateUtils.toDate("08/31/2016"), DateUtils.toDate("09/30/2016")).withFrequency(2)
				).doubleValue(), 0.0000001);

		Assertions.assertEquals(31.0 / 366.0,
				convention.calculateAccrualRate(
						DayCountCommand.forAccrualRange(DateUtils.toDate("9/30/2016"), DateUtils.toDate("10/31/2016")).withAccrualPeriod(DateUtils.toDate("9/30/2016"), DateUtils.toDate("10/31/2016")).withFrequency(2)
				).doubleValue(), 0.0000001);

		Assertions.assertEquals(2.0 / 366.0 + 30.0 / 365.0,
				convention.calculateAccrualRate(
						DayCountCommand.forAccrualRange(DateUtils.toDate("12/30/2016"), DateUtils.toDate("01/31/2017")).withAccrualPeriod(DateUtils.toDate("12/30/2016"), DateUtils.toDate("01/31/2017")).withFrequency(2)
				).doubleValue(), 0.0000001);
	}


	@Test
	public void test_NoLeap_365_Convention() {
		//Feb 1,2010 to April 1, 2010 has 59 days.
		// 59/365
		DayCountConventions convention = DayCountConventions.NOLEAP_THREESIXTYFIVE;
		Assertions.assertEquals(28.0 / 365.0,
				convention.calculateAccrualRate(
						DayCountCommand.forAccrualRange(DateUtils.toDate("02/15/2012"), DateUtils.toDate("03/15/2012")).withAccrualPeriod(DateUtils.toDate("02/15/2012"), DateUtils.toDate("03/19/2012")).withFrequency(12)
				).doubleValue(), 0.0000001);

		Assertions.assertEquals(28.0 / 365.0,
				convention.calculateAccrualRate(
						DayCountCommand.forAccrualRange(DateUtils.toDate("02/15/2011"), DateUtils.toDate("03/15/2011")).withAccrualPeriod(DateUtils.toDate("02/15/2011"), DateUtils.toDate("03/19/2011")).withFrequency(12)
				).doubleValue(), 0.0000001);

		Assertions.assertEquals(28.0 / 365.0,
				convention.calculateAccrualRate(
						DayCountCommand.forAccrualRange(DateUtils.toDate("02/15/2011"), DateUtils.toDate("03/15/2011")).withAccrualPeriod(DateUtils.toDate("02/15/2011"), DateUtils.toDate("03/19/2011")).withFrequency(12)
				).doubleValue(), 0.0000001);
	}
}
