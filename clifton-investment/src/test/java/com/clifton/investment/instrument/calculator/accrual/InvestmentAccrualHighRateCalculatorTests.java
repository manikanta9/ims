package com.clifton.investment.instrument.calculator.accrual;


import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class InvestmentAccrualHighRateCalculatorTests {

	@Resource
	private InvestmentAccrualHighRateCalculator InvestmentAccrualHighRateCalculator;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;


	private SystemColumnValueHandler getSystemColumnServiceMock(InvestmentSecurity security, double spread, String frequency, String dayCountConvention) {
		SystemColumnValueHandler systemColumnValueHandler = Mockito.mock(SystemColumnValueHandler.class);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_SPREAD, false)).thenReturn(
				BigDecimal.valueOf(spread));
		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY, false))
				.thenReturn(frequency);
		Mockito.when(
						systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION, false))
				.thenReturn(dayCountConvention);

		return systemColumnValueHandler;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testHighRateCalculatorAccrueUpToEndOfPeriod() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.InvestmentAccrualHighRateCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 50.0, "MONTHLY", "Actual/365"));

		BigDecimal result = this.InvestmentAccrualHighRateCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("70506339.90"), BigDecimal.ONE, DateUtils.toDate("07/30/2012"));
		Assertions.assertEquals(new BigDecimal("-35709.79"), result);
	}


	@Test
	public void testHighRateCalculatorAccrueUpToEndOfPeriodAlternate() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(200);
		this.InvestmentAccrualHighRateCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 52.0, "MONTHLY", "Actual/365"));

		BigDecimal result = this.InvestmentAccrualHighRateCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("142000000.00"), BigDecimal.ONE, DateUtils.toDate("01/31/2013"));
		Assertions.assertEquals(new BigDecimal("-71609.24"), result);
	}


	@Test
	public void testHighRateCalculatorAccrueUpToOneDayAfterPeriod() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.InvestmentAccrualHighRateCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 50.0, "MONTHLY", "Actual/365"));

		BigDecimal result = this.InvestmentAccrualHighRateCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("70506339.90"), BigDecimal.ONE, DateUtils.toDate("07/31/2012"));
		Assertions.assertEquals(new BigDecimal("-36891.11"), result);
	}


	@Test
	public void testHighRateCalculatorFailAccrualDateAfterPeriod() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
			this.InvestmentAccrualHighRateCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 50.0, "MONTHLY", "Actual/365"));

			this.InvestmentAccrualHighRateCalculator.calculateAccruedInterest(paymentEvent, BigDecimal.valueOf(70506339.90), BigDecimal.ONE, DateUtils.toDate("08/01/2012"));
		}, "\"accrueUpToDate\" [08/01/2012] is invalid for payment event with accrual start date of [06/29/2012], and accrual end date of [07/30/2012] (accruals are allowed to one day past the period end date).");
	}


	@Test
	public void testHighRateCalculatorFailAccrualDateBeforePeriod() {
		TestUtils.expectException(ValidationException.class, () -> {
			InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
			this.InvestmentAccrualHighRateCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 50.0, "MONTHLY", "Actual/365"));

			this.InvestmentAccrualHighRateCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("70506339.90"), BigDecimal.ONE, DateUtils.toDate("06/28/2012"));
		}, "\"accrueUpToDate\" [06/28/2012] is invalid for payment event with accrual start date of [06/29/2012], and accrual end date of [07/30/2012] (accruals are allowed to one day past the period end date).");
	}


	@Test
	public void testHighRateCalculatorAccrueForZeroDays() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.InvestmentAccrualHighRateCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 50.0, "MONTHLY", "Actual/365"));

		BigDecimal result = this.InvestmentAccrualHighRateCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("70506339.90"), BigDecimal.ONE, DateUtils.toDate("06/29/2012"));
		Assertions.assertEquals(new BigDecimal("0.00"), result);
	}


	@Test
	public void testHighRateCalculatorAccrueForOneDay() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.InvestmentAccrualHighRateCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 50.0, "MONTHLY", "Actual/365"));

		BigDecimal result = this.InvestmentAccrualHighRateCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("70506339.90"), BigDecimal.ONE, DateUtils.toDate("06/30/2012"));

		// spread = 965.84, rate = 186.08
		Assertions.assertEquals(new BigDecimal("-1151.92"), result);
	}


	@Test
	public void testHighRateCalculatorAccrualDateBeforeSecondDetailEffectiveDate() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.InvestmentAccrualHighRateCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 50.0, "MONTHLY", "Actual/365"));

		BigDecimal result = this.InvestmentAccrualHighRateCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("70506339.90"), BigDecimal.ONE, DateUtils.toDate("07/06/2012"));

		// spread = 6760.88, rate = 1341.76
		Assertions.assertEquals(new BigDecimal("-8102.64"), result);
	}


	@Test
	public void testHighRateCalculatorAccrualDateBeforeFinalDetailEffectiveDate() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.InvestmentAccrualHighRateCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 50.0, "MONTHLY", "Actual/365"));

		BigDecimal result = this.InvestmentAccrualHighRateCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("70506339.90"), BigDecimal.ONE, DateUtils.toDate("07/24/2012"));

		// spread = 24146.01, rate = 4652.17
		Assertions.assertEquals(new BigDecimal("-28798.18"), result);
	}


	@Test
	public void testHighRateCalculatorAccrueToFirstDetailEffectiveDate() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.InvestmentAccrualHighRateCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 50.0, "MONTHLY", "Actual/365"));

		BigDecimal result = this.InvestmentAccrualHighRateCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("70506339.90"), BigDecimal.ONE, DateUtils.toDate("07/02/2012"));

		// spread = 2897.52, rate = 558.24
		Assertions.assertEquals(new BigDecimal("-3455.76"), result);
	}


	@Test
	public void testHighRateCalculatorAccrueToSecondDetailEffectiveDate() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		this.InvestmentAccrualHighRateCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 50.0, "MONTHLY", "Actual/365"));

		BigDecimal result = this.InvestmentAccrualHighRateCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("70506339.90"), BigDecimal.ONE, DateUtils.toDate("07/09/2012"));

		// spread = 9658.4, rate = 1929.40
		Assertions.assertEquals(new BigDecimal("-11587.80"), result);
	}


	@Test
	public void testHighRateCalculatorAccrualDateBetweenLastDetailEffectiveDateAndPeriodEnd() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		// delete final detail
		List<InvestmentSecurityEventDetail> details = this.investmentSecurityEventService.getInvestmentSecurityEventDetailListByEvent(paymentEvent.getId());
		InvestmentSecurityEventDetail lastDetail = details.get(details.size() - 1);
		this.investmentSecurityEventService.deleteInvestmentSecurityEventDetail(lastDetail.getId());

		this.InvestmentAccrualHighRateCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 50.0, "MONTHLY", "Actual/365"));

		BigDecimal result = this.InvestmentAccrualHighRateCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("70506339.90"), BigDecimal.ONE, DateUtils.toDate("07/26/2012"));

		// spread = 26077.69, rate = 5024.36
		Assertions.assertEquals(new BigDecimal("-31102.05"), result);
	}


	@Test
	public void testHighRateCalculatorEventSpreadDifferentScale() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(100);
		List<InvestmentSecurityEventDetail> details = this.investmentSecurityEventService.getInvestmentSecurityEventDetailListByEvent(paymentEvent.getId());

		// Change scale on first detail
		InvestmentSecurityEventDetail detail = details.get(0);
		detail.setSpread(BigDecimal.valueOf(50.00));
		this.investmentSecurityEventService.saveInvestmentSecurityEventDetail(detail);

		this.InvestmentAccrualHighRateCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(paymentEvent.getSecurity(), 50.0, "MONTHLY", "Actual/365"));

		BigDecimal result = this.InvestmentAccrualHighRateCalculator.calculateAccruedInterest(paymentEvent, BigDecimal.valueOf(70506339.90), BigDecimal.ONE, DateUtils.toDate("07/31/2012"));
		Assertions.assertEquals(new BigDecimal("-36891.11"), result);
	}
}
