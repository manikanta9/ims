package com.clifton.investment.instrument.calculator.accrual;


import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentAccrualDayCountCompoundedCalculatorTests {

	@Resource
	private InvestmentAccrualDayCountCompoundedCalculator investmentAccrualDayCountCompoundedCalculator;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	@Resource
	private SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testMidSecondPeriodCompounding() {
		InvestmentSecurityEvent paymentEvent = this.investmentSecurityEventService.getInvestmentSecurityEvent(200);

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(paymentEvent.getSecurity(), InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME,
						InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY2, false)).thenReturn("QUARTERLY");
		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(paymentEvent.getSecurity(), InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME,
						InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION2, false)).thenReturn("Actual/360");
		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(paymentEvent.getSecurity(), InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME,
						InvestmentSecurity.CUSTOM_FIELD_STRUCTURE, false)).thenReturn("PAY_FLOAT");

		BigDecimal result = this.investmentAccrualDayCountCompoundedCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("23000000.00"), BigDecimal.ONE, DateUtils.toDate("06/17/2013"));
		Assertions.assertEquals(BigDecimal.valueOf(-16105.75), result);

		result = this.investmentAccrualDayCountCompoundedCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("23000000.00"), BigDecimal.ONE, DateUtils.toDate("06/20/2013"));
		Assertions.assertEquals(BigDecimal.valueOf(-16638.36), result);

		result = this.investmentAccrualDayCountCompoundedCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("23000000.00"), BigDecimal.ONE, DateUtils.toDate("06/21/2013"));
		Assertions.assertEquals(BigDecimal.valueOf(-16813.06), result);

		result = this.investmentAccrualDayCountCompoundedCalculator.calculateAccruedInterest(paymentEvent, new BigDecimal("23000000.00"), BigDecimal.ONE, DateUtils.toDate("06/25/2013"));
		Assertions.assertEquals(BigDecimal.valueOf(-17511.87), result); // TODO: Bloomberg has -17511.86
	}
}
