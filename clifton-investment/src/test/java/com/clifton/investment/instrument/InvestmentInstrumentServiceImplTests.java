package com.clifton.investment.instrument;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationTypes;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationValidator;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy.InstrumentDeliverableTypes;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.column.SystemColumnService;
import org.hibernate.Criteria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;


public class InvestmentInstrumentServiceImplTests {


	@InjectMocks
	private InvestmentInstrumentServiceImpl investmentInstrumentService;
	@Mock
	private InvestmentSetupService investmentSetupService;
	@Mock
	private SystemColumnService systemColumnService;
	@Mock
	private AdvancedUpdatableDAO<InvestmentInstrument, Criteria> investmentInstrumentDAO;
	@Mock
	private AdvancedUpdatableDAO<InvestmentSecurity, Criteria> investmentSecurityDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Mockito.when(this.investmentSecurityDAO.save(ArgumentMatchers.any(InvestmentSecurity.class))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return args[0];
		});
		Mockito.when(this.investmentInstrumentDAO.save(ArgumentMatchers.any(InvestmentInstrument.class))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return args[0];
		});
		configureInvestmentSetupAllocationOfSecurities(false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Tests for Correct Validation                    ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveInvestmentSecurity_Success() {
		InvestmentSecurity ibm = InvestmentSecurityBuilder.ofType("IBM", InvestmentType.STOCKS).build();

		this.investmentInstrumentService.saveInvestmentSecurity(ibm);
		Mockito.verify(this.investmentInstrumentService.getInvestmentInstrumentDAO()).save(ArgumentMatchers.any(InvestmentInstrument.class));
	}


	@Test
	public void testSaveInvestmentSecurity_FailWithNullInstrument() {
		InvestmentSecurity ibm = InvestmentSecurityBuilder.ofType("IBM", InvestmentType.STOCKS).build();
		ibm.setInstrument(null);

		TestUtils.expectException(FieldValidationException.class, () -> this.investmentInstrumentService.saveInvestmentSecurity(ibm), "Instrument hierarchy attribute is required");
	}


	@Test
	public void testSaveInvestmentSecurity_FailWithNullHierarchy() {
		InvestmentSecurity ibm = InvestmentSecurityBuilder.ofType("IBM", InvestmentType.STOCKS).build();
		ibm.getInstrument().setHierarchy(null);

		TestUtils.expectException(FieldValidationException.class, () -> this.investmentInstrumentService.saveInvestmentSecurity(ibm), "Instrument hierarchy attribute is required");
	}


	@Test
	public void testSaveInvestmentSecurity_FailWithHierarchyAsNewBean() {
		InvestmentSecurity ibm = InvestmentSecurityBuilder.ofType("IBM", InvestmentType.STOCKS).build();
		// Setting hierarchy id as null will return true for isNewBean()
		ibm.getInstrument().getHierarchy().setId(null);

		TestUtils.expectException(FieldValidationException.class, () -> this.investmentInstrumentService.saveInvestmentSecurity(ibm), "Instrument hierarchy attribute is required");
	}


	@Test
	public void testSaveInvestmentSecurity_FailNoMatchingHierarchy() {
		configureInvestmentSetupServiceHierarchy(null);
		InvestmentSecurity ibm = InvestmentSecurityBuilder.ofType("IBM", InvestmentType.STOCKS).build();

		TestUtils.expectException(ValidationException.class, () -> this.investmentInstrumentService.saveInvestmentSecurity(ibm), "Cannot find investment hierarchy for id = " + ibm.getInstrument().getHierarchy().getId());
	}


	@Test
	public void testSaveInvestmentSecurity_FailAssignmentNotAllowed() {
		// Send false for assignmentAllowed
		configureInvestmentSetupServiceHierarchy(false, true, true);
		InvestmentSecurity ibm = InvestmentSecurityBuilder.ofType("IBM", InvestmentType.STOCKS).build();

		TestUtils.expectException(FieldValidationException.class, () -> this.investmentInstrumentService.saveInvestmentSecurity(ibm), String.format("The hierarchy '%s' does not allow instrument assignments.", ibm.getInstrument().getHierarchy().getName()));
	}


	@Test
	public void testSaveInvestmentSecurity_FailAllocationsNotAllowed() {
		//Send false for allocationOfSecurityAllowed
		configureInvestmentSetupServiceHierarchy(true, false, true);
		InvestmentSecurity ibm = InvestmentSecurityBuilder.ofType("IBM", InvestmentType.STOCKS).build();
		InvestmentSecurityAllocation allocation = new InvestmentSecurityAllocation();
		allocation.setParentInvestmentSecurity(ibm);
		allocation.setAllocationWeight(BigDecimal.ONE);

		TestUtils.expectException(ValidationException.class, () -> new InvestmentSecurityAllocationValidator().validate(allocation, DaoEventTypes.INSERT), String.format("The hierarchy '%s' for security 'IBM' does not allow securities to be allocations of other securities.", ibm.getInstrument().getHierarchy().getName()));
	}


	@Test
	public void testSaveInvestmentSecurity_FailWithSymbolEqualsName() {
		InvestmentSecurity ibm = InvestmentSecurityBuilder.ofType("IBM", InvestmentType.STOCKS).build();
		// ensure CUSIP is populated
		ibm.setCusip("00508Y102");
		// set Name equal to symbol
		ibm.setName(ibm.getSymbol());

		TestUtils.expectException(ValidationException.class, () -> this.investmentInstrumentService.saveInvestmentSecurity(ibm), "\"Symbol\" and \"Name\" fields must be different");
	}


	@Test
	public void testSaveInvestmentSecurity_FailWithSymbolEqualsNameAndCusip() {
		// set symbol as a valid CUSIP
		InvestmentSecurity ibm = InvestmentSecurityBuilder.ofType("00508Y102", InvestmentType.STOCKS).build();
		// set Name equal to symbol
		ibm.setName(ibm.getSymbol());

		TestUtils.expectException(ValidationException.class, () -> this.investmentInstrumentService.saveInvestmentSecurity(ibm), "\"Name\" field must differ from \"Symbol\" field");
	}


	@Test
	public void testSaveInvestmentSecurity_FailNullCurrency() {
		InvestmentSecurity ibm = InvestmentSecurityBuilder.ofType("IBM", InvestmentType.STOCKS).build();
		ibm.getInstrument().setTradingCurrency(null);

		TestUtils.expectException(FieldValidationException.class, () -> this.investmentInstrumentService.saveInvestmentSecurity(ibm), "Currency denomination field is required.");
	}


	@Test
	public void testSaveInvestmentSecurity_Currency_FailHierarchyNotCurrency() {
		InvestmentSecurity usd = InvestmentSecurityBuilder.ofType("USD", InvestmentType.CURRENCY).build();
		usd.getInstrument().getHierarchy().setCurrency(false);

		TestUtils.expectException(FieldValidationException.class, () -> this.investmentInstrumentService.saveInvestmentSecurity(usd), "Cannot mark as currency a security that doesn't belong to currency hierarchy");
	}


	@Test
	public void testSaveInvestmentSecurity_FailHierarchyNullBigSecurity() {
		InvestmentSecurity ibm = InvestmentSecurityBuilder.ofType("IBM", InvestmentType.STOCKS).build();
		ibm.getInstrument().setBigInstrument(new InvestmentInstrument());
		ibm.setBigSecurity(null);

		TestUtils.expectException(FieldValidationException.class, () -> this.investmentInstrumentService.saveInvestmentSecurity(ibm), "Big Security field is required for IBM");
	}


	@Test
	public void testSaveInvestmentSecurity_FailNullStartDate() {
		configureInvestmentSetupAllocationOfSecurities(true);
		InvestmentSecurity ibm = InvestmentSecurityBuilder.ofType("IBM", InvestmentType.STOCKS).build();
		ibm.setStartDate(null);

		TestUtils.expectException(FieldValidationException.class, () -> this.investmentInstrumentService.saveInvestmentSecurity(ibm), "Start Date is required for securities that are an allocation of securities.");
	}


	@Test
	public void testSaveInvestmentSecurity_FailOtcWithoutCounterparty() {
		InvestmentSecurity swap = InvestmentSecurityBuilder.ofType("SWAP-ID", InvestmentType.SWAPS).build();
		swap.getInstrument().getHierarchy().setOtc(true);
		InvestmentSetupService mockInvestmentSetupService = Mockito.mock(InvestmentSetupService.class);
		Mockito.when(mockInvestmentSetupService.getInvestmentInstrumentHierarchy(ArgumentMatchers.anyShort())).thenReturn(swap.getInstrument().getHierarchy());
		this.investmentInstrumentService.setInvestmentSetupService(mockInvestmentSetupService);

		TestUtils.expectException(FieldValidationException.class, () -> this.investmentInstrumentService.saveInvestmentSecurity(swap), "Counterparty is required for OTC securities (except for Forwards).");
	}


	@Test
	public void testSaveInvestmentSecurity_FailOtcWithoutContract() {
		InvestmentSecurity swap = InvestmentSecurityBuilder.ofType("SWAP-ID", InvestmentType.SWAPS).build();
		swap.setBusinessCompany(new BusinessCompany());
		swap.getInstrument().getHierarchy().setOtc(true);
		InvestmentSetupService mockInvestmentSetupService = Mockito.mock(InvestmentSetupService.class);
		Mockito.when(mockInvestmentSetupService.getInvestmentInstrumentHierarchy(ArgumentMatchers.anyShort())).thenReturn(swap.getInstrument().getHierarchy());
		this.investmentInstrumentService.setInvestmentSetupService(mockInvestmentSetupService);

		TestUtils.expectException(FieldValidationException.class, () -> this.investmentInstrumentService.saveInvestmentSecurity(swap), "Contract is required for OTC securities (except for Forwards).");
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Instrument Deliverable Types Tests              ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveInstrument_FailNonDeliverable_HierarchyAlwaysDeliverable() {
		configureInvestmentSetupAllocationOfSecurities(true);
		InvestmentInstrumentHierarchy hierarchy = setupDeliverableHierarchy(InstrumentDeliverableTypes.ALWAYS);
		configureInvestmentSetupServiceHierarchy(hierarchy);
		InvestmentInstrument instrument = InvestmentSecurityBuilder.ofType("TST-1", InvestmentType.FORWARDS).build().getInstrument();
		instrument.setHierarchy(hierarchy);

		TestUtils.expectException(ValidationException.class, () -> this.investmentInstrumentService.saveInvestmentInstrument(instrument), "All instruments in Hierarchy [Test Hierarchy] must be deliverable.");
	}


	@Test
	public void testSaveInstrument_FailDeliverable_HierarchyNeverDeliverable() {
		configureInvestmentSetupAllocationOfSecurities(true);
		InvestmentInstrumentHierarchy hierarchy = setupDeliverableHierarchy(InstrumentDeliverableTypes.NEVER);
		configureInvestmentSetupServiceHierarchy(hierarchy);
		InvestmentInstrument instrument = InvestmentSecurityBuilder.ofType("TST-1", InvestmentType.FORWARDS).build().getInstrument();
		instrument.setDeliverable(true);
		instrument.setHierarchy(hierarchy);

		TestUtils.expectException(ValidationException.class, () -> this.investmentInstrumentService.saveInvestmentInstrument(instrument), "No instruments in Hierarchy [Test Hierarchy] can be deliverable.");
	}


	@Test
	public void testSaveInstrument_SuccessDeliverable() {
		configureInvestmentSetupAllocationOfSecurities(true);
		InvestmentInstrument instrument = InvestmentSecurityBuilder.ofType("TST-1", InvestmentType.FORWARDS).build().getInstrument();
		InvestmentInstrumentHierarchy never = setupDeliverableHierarchy(InstrumentDeliverableTypes.NEVER);
		InvestmentInstrumentHierarchy always = setupDeliverableHierarchy(InstrumentDeliverableTypes.ALWAYS);
		InvestmentInstrumentHierarchy both = setupDeliverableHierarchy(InstrumentDeliverableTypes.BOTH);

		// Never = false
		configureInvestmentSetupServiceHierarchy(never);
		instrument.setHierarchy(never);
		instrument.setDeliverable(false);
		this.investmentInstrumentService.saveInvestmentInstrument(instrument);

		// always = true
		configureInvestmentSetupServiceHierarchy(always);
		instrument.setHierarchy(always);
		instrument.setDeliverable(true);
		this.investmentInstrumentService.saveInvestmentInstrument(instrument);

		// both = true or false
		configureInvestmentSetupServiceHierarchy(both);
		instrument.setHierarchy(both);
		instrument.setDeliverable(true);
		this.investmentInstrumentService.saveInvestmentInstrument(instrument);
		instrument.setDeliverable(false);
		this.investmentInstrumentService.saveInvestmentInstrument(instrument);
	}


	private InvestmentInstrumentHierarchy setupDeliverableHierarchy(InstrumentDeliverableTypes deliverableType) {
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setId(MathUtils.SHORT_ONE);
		hierarchy.setName("Test Hierarchy");
		hierarchy.setAssignmentAllowed(true);
		hierarchy.setInstrumentDeliverableType(deliverableType);
		return hierarchy;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Helper Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	private void configureInvestmentSetupAllocationOfSecurities(boolean allocationOfSecurities) {
		// Set true for all parameters to initialize hierarchy for positive test cases
		configureInvestmentSetupServiceHierarchy(true, allocationOfSecurities, true);
	}


	/**
	 * Configures the dummy hierarchy which shall be returned by the InvestmentSetupService on request.
	 */
	private void configureInvestmentSetupServiceHierarchy(boolean hierarchyAssignmentAllowed, boolean hierarchyAllocationOfSecurityAllowed, boolean hierarchyOneSecurityPerInstrument) {
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setId(MathUtils.SHORT_ONE);
		hierarchy.setName("Hierarchy for Stocks");
		if (hierarchyAssignmentAllowed) {
			hierarchy.setAssignmentAllowed(true);
		}
		if (hierarchyOneSecurityPerInstrument) {
			hierarchy.setOneSecurityPerInstrument(true);
		}
		if (hierarchyAllocationOfSecurityAllowed) {
			hierarchy.setSecurityAllocationType(InvestmentSecurityAllocationTypes.WEIGHTED_RETURN);
		}
		configureInvestmentSetupServiceHierarchy(hierarchy);
	}


	/**
	 * Configures the dummy hierarchy which shall be returned by the InvestmentSetupService on request.
	 */
	private void configureInvestmentSetupServiceHierarchy(InvestmentInstrumentHierarchy hierarchy) {
		Mockito.when(this.investmentSetupService.getInvestmentInstrumentHierarchy(ArgumentMatchers.anyShort())).thenReturn(hierarchy);
		Mockito.when(this.systemColumnService.getSystemColumnCustomListForGroupAndLinkedValue(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyBoolean())).thenReturn(null);
	}
}
