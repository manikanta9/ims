package com.clifton.investment.instrument.calculator;

import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * @author vgomelsky
 */
public class InvestmentNotionalCalculatorTypesTests {

	@Test
	public void testCalculatePriceFromNotional() {
		BigDecimal quantity = new BigDecimal("100");
		BigDecimal price = new BigDecimal("11");
		BigDecimal notional = new BigDecimal("1100.00");
		BigDecimal priceMultiplier = BigDecimal.ONE;

		Assertions.assertEquals(price, InvestmentNotionalCalculatorTypes.MONEY_HALF_UP.calculatePriceFromNotional(quantity, notional, priceMultiplier));
		Assertions.assertEquals(notional, InvestmentNotionalCalculatorTypes.MONEY_HALF_UP.calculateNotional(price, priceMultiplier, quantity));

		Assertions.assertEquals(new BigDecimal("166.97"), InvestmentNotionalCalculatorTypes.MONEY_HALF_UP.calculatePriceFromNotional(new BigDecimal("8941"), new BigDecimal("1492878.77"), priceMultiplier));
		Assertions.assertEquals(new BigDecimal("1492878.77"), InvestmentNotionalCalculatorTypes.MONEY_HALF_UP.calculateNotional(new BigDecimal("166.97"), priceMultiplier, new BigDecimal("8941")));

		Assertions.assertEquals(price, InvestmentNotionalCalculatorTypes.MONEY_HALF_UP_DIVIDE.calculatePriceFromNotional(quantity, new BigDecimal("9.09"), priceMultiplier));
		Assertions.assertEquals(new BigDecimal("9.09"), InvestmentNotionalCalculatorTypes.MONEY_HALF_UP_DIVIDE.calculateNotional(price, priceMultiplier, quantity));

		Assertions.assertEquals(price, InvestmentNotionalCalculatorTypes.INTEGER_HALF_UP.calculatePriceFromNotional(quantity, notional.stripTrailingZeros(), priceMultiplier));
		Assertions.assertEquals(MathUtils.round(notional, 0), InvestmentNotionalCalculatorTypes.INTEGER_HALF_UP.calculateNotional(price, priceMultiplier, quantity));

		Assertions.assertEquals(price, InvestmentNotionalCalculatorTypes.INTEGER_HALF_UP_DIVIDE.calculatePriceFromNotional(quantity, new BigDecimal("9"), priceMultiplier));
		Assertions.assertEquals(new BigDecimal("9"), InvestmentNotionalCalculatorTypes.INTEGER_HALF_UP_DIVIDE.calculateNotional(price, priceMultiplier, quantity));
	}
}
