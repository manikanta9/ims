package com.clifton.investment.instrument.event.sequence;

import com.clifton.calendar.CalendarTestObjectFactory;
import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.CalendarDateGenerationHandlerImpl;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.calculator.CouponFrequencies;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.sequence.date.EquityLegPaymentDateCalculator;
import com.clifton.investment.instrument.event.sequence.date.InterestLegPaymentDateCalculator;
import com.clifton.investment.instrument.event.sequence.date.InvestmentInstrumentEventSequenceDateCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;


/**
 * @author mwacker
 */
public class InvestmentInstrumentEventSequence_N1836677M_Tests extends AbstractInvestmentInstrumentEventSequenceTest {

	@Override
	public Date getScheduleStartDate() {
		return DateUtils.toDate("01/01/2015");
	}


	@Override
	public Date getScheduleEndDate() {
		return DateUtils.toDate("02/01/2016");
	}


	public CalendarDateGenerationHandler calendarDateGenerationHandler = new CalendarDateGenerationHandlerImpl();


	@Test
	public void testEquityLegPaymentSequence_N1836677M() {
		EquityLegPaymentDateCalculator dateCalculator = configureDateCalculator(new EquityLegPaymentDateCalculator());

		EventSequenceDefinition sequence = setup_N1836677M(dateCalculator, InvestmentSecurityEventType.EQUITY_LEG_PAYMENT);

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(6, events.size());

		// Valuation Date, ExDate, Next ValuationDate, Payment Date
		Assertions.assertEquals("05/29/2015	07/01/2015	06/30/2015	07/01/2015", formatEquityEvent(events.get(0)));
		Assertions.assertEquals("06/30/2015	08/01/2015	07/31/2015	08/03/2015", formatEquityEvent(events.get(1)));
		Assertions.assertEquals("07/31/2015	09/01/2015	08/31/2015	09/01/2015", formatEquityEvent(events.get(2)));
		Assertions.assertEquals("08/31/2015	10/01/2015	09/30/2015	10/01/2015", formatEquityEvent(events.get(3)));
		Assertions.assertEquals("09/30/2015	10/31/2015	10/30/2015	11/02/2015", formatEquityEvent(events.get(4)));
		Assertions.assertEquals("10/30/2015	12/01/2015	11/30/2015	12/01/2015", formatEquityEvent(events.get(5)));
	}


	@Test
	public void testInterestLegPaymentSequence_N1836677M() {
		InterestLegPaymentDateCalculator dateCalculator = configureDateCalculator(new InterestLegPaymentDateCalculator());

		EventSequenceDefinition sequence = setup_N1836677M(dateCalculator, InvestmentSecurityEventType.INTEREST_LEG_PAYMENT);

		List<InvestmentSecurityEvent> events = this.investmentInstrumentEventSequenceService.getSecurityEventList(sequence, sequence.getSequenceStartDate(), sequence.getSecurity()
				.getLastDeliveryDate());
		Assertions.assertEquals(6, events.size());

		Assertions.assertEquals("06/01/2015	07/01/2015	07/01/2015	07/01/2015	05/29/2015", formatInterestEvent(events.get(0)));
		Assertions.assertEquals("07/01/2015	08/03/2015	08/01/2015	08/03/2015	06/30/2015", formatInterestEvent(events.get(1)));
		Assertions.assertEquals("08/03/2015	09/01/2015	09/01/2015	09/01/2015	07/31/2015", formatInterestEvent(events.get(2)));
		Assertions.assertEquals("09/01/2015	10/01/2015	10/01/2015	10/01/2015	08/28/2015", formatInterestEvent(events.get(3)));
		Assertions.assertEquals("10/01/2015	11/02/2015	11/01/2015	11/02/2015	09/30/2015", formatInterestEvent(events.get(4)));
		Assertions.assertEquals("11/02/2015	12/01/2015	12/01/2015	12/01/2015	10/30/2015", formatInterestEvent(events.get(5)));
	}


	private EventSequenceDefinition setup_N1836677M(InvestmentInstrumentEventSequenceDateCalculator dateCalculator, String typeName) {
		Calendar ny_london_japan = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_NY_LONDON_JAPAN);
		Calendar us = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_US);
		Calendar uk = getCalendarSetupService().getCalendar(CalendarTestObjectFactory.CALENDAR_UK);


		EventSequenceDefinition sequence = new EventSequenceDefinition();
		sequence.setBusinessDayConvention(BusinessDayConventions.NEXT_MODIFIED);
		sequence.setResetCalendar(us);
		sequence.setFixingCalendar(uk);
		sequence.setSettlementCalendar(ny_london_japan);

		sequence.setSequenceFrequency(CouponFrequencies.getEnum("Monthly"));
		sequence.setSettlementCycle(1);
		sequence.setDateCalculator(dateCalculator);
		InvestmentSecurityEventType type = new InvestmentSecurityEventType();
		type.setName(typeName);
		sequence.setType(type);
		sequence.setFixingCycle(-1);
		sequence.setSecurity(InvestmentTestObjectFactory.newInvestmentSecurity(30879, "N1836677M", "Swaps"));
		sequence.getSecurity().setStartDate(DateUtils.toDate("05/29/2015"));
		sequence.getSecurity().setEndDate(DateUtils.toDate("11/30/2015"));
		sequence.getSecurity().setLastDeliveryDate(DateUtils.toDate("12/01/2015"));
		sequence.setValuationConvention(EventSequenceRecurrenceConventions.MONTH_END_RECURRENCE);

		sequence.setSequenceStartDate(getSequenceStartDate(sequence, DateUtils.toDate("05/29/2015")));

		return sequence;
	}


	private Date getSequenceStartDate(EventSequenceDefinition sequence, Date date) {
		((CalendarDateGenerationHandlerImpl) this.calendarDateGenerationHandler).setCalendarBusinessDayService(getCalendarBusinessDayService());

		if (sequence.getValuationConvention() == EventSequenceRecurrenceConventions.MONTH_END_RECURRENCE) {
			Date lastBusinessDateOfThisMonth = this.calendarDateGenerationHandler.generateDate(DateGenerationOptions.LAST_BUSINESS_DAY_OF_CURRENT_MONTH, date, sequence.getCalendar());
			if (DateUtils.compare(date, lastBusinessDateOfThisMonth, false) != 0) {
				date = this.calendarDateGenerationHandler.generateDate(DateGenerationOptions.LAST_BUSINESS_DAY_OF_PREVIOUS_MONTH, date, sequence.getCalendar());
			}
		}

		return date;
	}
}
