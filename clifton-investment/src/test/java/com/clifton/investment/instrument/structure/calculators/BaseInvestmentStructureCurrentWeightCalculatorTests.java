package com.clifton.investment.instrument.structure.calculators;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructure;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureWeight;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class BaseInvestmentStructureCurrentWeightCalculatorTests {

	private class BaseInvestmentStructureCurrentWeightCalculatorTestsImpl extends BaseInvestmentStructureCurrentWeightCalculator {

		@SuppressWarnings("unused")
		@Override
		public void calculateCurrentWeightsImpl(InvestmentInstrumentStructure structure, List<InvestmentSecurityStructureAllocation> securityAllocationList, Date date, BigDecimal additionalMultiplier) {
			// DO NOTHING
		}
	}


	@Test
	public void testReapplyCurrentWeightAsAPercentage_Precision4() {
		BaseInvestmentStructureCurrentWeightCalculatorTestsImpl baseCalc = new BaseInvestmentStructureCurrentWeightCalculatorTestsImpl();
		baseCalc.setWeightPrecision(4);
		List<InvestmentSecurityStructureAllocation> list = createAllocationList(new Double[]{477.47647, 401.07632, 247.32723, 147.56589, 152.69391, 144.44372, 83.93390, 155.70077, 59.67073,
				327.14463, 243.46909, 121.54829, 114.28892, 212.16221, 313.26198, 108.16701, 99.27320, 482.29851, 177.80099, 166.90058, 77.36586, 107.77992});
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(4421.35013), CoreMathUtils.sumProperty(list, InvestmentSecurityStructureAllocation::getAllocationWeight)));
		baseCalc.reapplyCurrentWeightAsAPercentage(list);
		validatePercentages(list, new Double[]{10.7993, 9.0714, 5.5939, 3.3376, 3.4536, 3.2670, 1.8984, 3.5216, 1.3496, 7.3992, 5.5067, 2.7491, 2.5849, 4.7986, 7.0852, 2.4465, 2.2453, 10.9083,
				4.0214, 3.7749, 1.7498, 2.4377});
	}


	@Test
	public void testReapplyCurrentWeightAsAPercentage_Precision2() {
		BaseInvestmentStructureCurrentWeightCalculatorTestsImpl baseCalc = new BaseInvestmentStructureCurrentWeightCalculatorTestsImpl();
		// Default Precision is 2 so not explicitly setting it to verify using default baseCalc.setWeightPrecision(2);
		List<InvestmentSecurityStructureAllocation> list = createAllocationList(new Double[]{477.47647, 401.07632, 247.32723, 147.56589, 152.69391, 144.44372, 83.93390, 155.70077, 59.67073,
				327.14463, 243.46909, 121.54829, 114.28892, 212.16221, 313.26198, 108.16701, 99.27320, 482.29851, 177.80099, 166.90058, 77.36586, 107.77992});
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(4421.35013), CoreMathUtils.sumProperty(list, InvestmentSecurityStructureAllocation::getAllocationWeight)));
		baseCalc.reapplyCurrentWeightAsAPercentage(list);
		validatePercentages(list, new Double[]{10.80, 9.07, 5.59, 3.34, 3.45, 3.27, 1.90, 3.52, 1.35, 7.4, 5.51, 2.75, 2.58, 4.80, 7.09, 2.45, 2.25, 10.90, 4.02, 3.77, 1.75, 2.44});
	}


	private void validatePercentages(List<InvestmentSecurityStructureAllocation> list, Double[] expectedWeights) {
		Assertions.assertTrue(MathUtils.isEqual(MathUtils.BIG_DECIMAL_ONE_HUNDRED, CoreMathUtils.sumProperty(list, InvestmentSecurityStructureAllocation::getAllocationWeight)));
		Assertions.assertEquals(CollectionUtils.getSize(list), expectedWeights.length);

		for (InvestmentSecurityStructureAllocation alloc : list) {
			BigDecimal expected = BigDecimal.valueOf(expectedWeights[(alloc.getSecurityStructureWeight().getId() - 1)]);
			Assertions.assertTrue(MathUtils.isEqual(expected, alloc.getAllocationWeight()), "Expected " + CoreMathUtils.formatNumberDecimal(expected) + " for the weight for Security Weight with ID [" + alloc.getSecurityStructureWeight().getId() + "] but got [" + CoreMathUtils.formatNumberDecimal(alloc.getAllocationWeight()) + "].");
		}
	}


	private List<InvestmentSecurityStructureAllocation> createAllocationList(Double[] weights) {
		List<InvestmentSecurityStructureAllocation> list = new ArrayList<>();
		int i = 1;
		for (Double w : weights) {
			list.add(createAllocation(i, BigDecimal.valueOf(w)));
			i++;
		}
		return list;
	}


	private InvestmentSecurityStructureAllocation createAllocation(int id, BigDecimal weight) {
		InvestmentSecurityStructureWeight sw = new InvestmentSecurityStructureWeight();
		sw.setId(id);
		InvestmentSecurityStructureAllocation a = new InvestmentSecurityStructureAllocation(sw, new Date());
		a.setAllocationWeight(weight);
		return a;
	}
}
