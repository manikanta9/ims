package com.clifton.investment.instrument.calculator.accrual;

import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.CouponFrequencies;
import com.clifton.investment.instrument.calculator.DayCountConventions;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;


/**
 * Tests for InvestmentAccrualDailySimpleCalculator.
 *
 * @author davidi
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InvestmentAccrualDailyNonCompoundingCalculatorTests {

	private static final int INTEREST_LEG_EVENT_300 = 300;

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	InvestmentAccrualDailySimpleCalculator investmentAccrualDailySimpleCalculator;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemColumnValueHandler getSystemColumnServiceMock(InvestmentSecurity security, String frequency, String dayCountConvention) {
		SystemColumnValueHandler systemColumnValueHandler = Mockito.mock(SystemColumnValueHandler.class);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY, false))
				.thenReturn(frequency);
		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, "Reset Frequency", false))
				.thenReturn(frequency);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION, false))
				.thenReturn(dayCountConvention);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CALENDAR, false))
				.thenReturn(this.calendarSetupService.getCalendarByName("Test Calendar").getId());

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CYCLE, false))
				.thenReturn(3);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_FIXING_CYCLE, false))
				.thenReturn(-2);

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_SPREAD, false))
				.thenReturn(BigDecimal.valueOf(-23));

		Mockito.when(systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_FIXING_CALENDAR, false))
				.thenReturn(this.calendarSetupService.getCalendarByName("Federal Reserve").getId());

		return systemColumnValueHandler;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalculateReferenceRateAmount() {

		AccrualConfig accrualConfig = AccrualConfig.of(CouponFrequencies.QUARTERLY, DayCountConventions.ACTUAL_THREESIXTY, "Coupon Frequency");
		InvestmentAccrualDailyRateCalculator.AccrualDailyRate[] dailyRates = convertRatesToAccrualDailyRates(BigDecimal.valueOf(2.00), BigDecimal.valueOf(2.10), BigDecimal.valueOf(2.20), BigDecimal.valueOf(2.05), BigDecimal.valueOf(2.30));

		BigDecimal amount = this.investmentAccrualDailySimpleCalculator.calculateReferenceRateAmount(null, BigDecimal.valueOf(1000000.000), dailyRates, accrualConfig);
		BigDecimal expectedValue = BigDecimal.valueOf(295.8333333333).setScale(10, RoundingMode.HALF_UP);

		Assertions.assertEquals(expectedValue, amount.setScale(10, RoundingMode.HALF_UP));
	}


	@Test
	public void testCalculateReferenceRateAmount_constant_rate() {

		AccrualConfig accrualConfig = AccrualConfig.of(CouponFrequencies.QUARTERLY, DayCountConventions.ACTUAL_THREESIXTY, "Coupon Frequency");
		InvestmentAccrualDailyRateCalculator.AccrualDailyRate[] dailyRates = convertRatesToAccrualDailyRates(BigDecimal.valueOf(2.00), BigDecimal.valueOf(2.00), BigDecimal.valueOf(2.00), BigDecimal.valueOf(2.00), BigDecimal.valueOf(2.00));

		BigDecimal amount = this.investmentAccrualDailySimpleCalculator.calculateReferenceRateAmount(null, BigDecimal.valueOf(1000000.000), dailyRates, accrualConfig);
		BigDecimal expectedValue = BigDecimal.valueOf(277.7777777778).setScale(10, RoundingMode.HALF_UP);

		Assertions.assertEquals(expectedValue, amount.setScale(10, RoundingMode.HALF_UP));
	}


	@Test
	public void testCalculateAccruedInterest_nonCompounded_dailyRates() {
		InvestmentSecurityEvent interestLeg = this.investmentSecurityEventService.getInvestmentSecurityEvent(INTEREST_LEG_EVENT_300);
		this.investmentAccrualDailySimpleCalculator.setSystemColumnValueHandler(getSystemColumnServiceMock(interestLeg.getSecurity(), "QUARTERLY", "Actual/360"));

		String[] accrualDates = {
				"10/13/2021",
				"10/14/2021",
				"10/15/2021",
				"10/18/2021",
				"10/19/2021",
				"10/20/2021",
				"10/21/2021",
				"10/22/2021",
				"10/25/2021",
				"10/26/2021",
				"10/27/2021",
				"10/28/2021",
				"10/29/2021"
		};

		double[] expectedAccruals = {
				0,
				-40.70,
				-81.39,
				-203.48,
				-244.17,
				-284.86,
				-330.08,
				-375.30,
				-501.91,
				-542.60,
				-585.56,
				-626.25,
				-666.95
		};

		for (int i = 0; i < accrualDates.length; i++) {
			assertAccrual(interestLeg, 8138988.31, accrualDates[i], expectedAccruals[i]);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentAccrualDailyRateCalculator.AccrualDailyRate[] convertRatesToAccrualDailyRates(BigDecimal... rates) {
		return ArrayUtils.getStream(rates)
				.map(rate -> InvestmentAccrualDailyRateCalculator.AccrualDailyRate.forRateOnDate(rate, null))
				.toArray(InvestmentAccrualDailyRateCalculator.AccrualDailyRate[]::new);
	}


	private void assertAccrual(InvestmentSecurityEvent paymentEvent, double notional, String accrualDate, double expectedAccrual) {
		BigDecimal accrual = this.investmentAccrualDailySimpleCalculator.calculateAccruedInterest(paymentEvent, BigDecimal.valueOf(notional), BigDecimal.ONE, DateUtils.toDate(accrualDate));
		BigDecimal expected = MathUtils.round(BigDecimal.valueOf(expectedAccrual), 2);
		Assertions.assertTrue(MathUtils.isEqual(expected, accrual), String.format("Accrual calculation failed on %s. Expected %s but received %s", accrualDate, expected, accrual));
	}
}
