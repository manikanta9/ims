package com.clifton.investment.instrument;


import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.SecurityIdentifierUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class InvestmentUtilsTest {

	@Test
	public void testGetInvestmentTypeNameForSecurity() {
		InvestmentSecurity future = InvestmentTestObjectFactory.newInvestmentSecurity(1, "SPH8", InvestmentType.FUTURES);
		InvestmentSecurity option = InvestmentTestObjectFactory.newInvestmentSecurity(2, "EVZ7P 2200", InvestmentType.OPTIONS);

		Assertions.assertEquals(InvestmentType.FUTURES, InvestmentUtils.getInvestmentTypeNameForSecurity(future));
		Assertions.assertEquals(InvestmentType.OPTIONS, InvestmentUtils.getInvestmentTypeNameForSecurity(option));
		Assertions.assertNotEquals(InvestmentType.OPTIONS, InvestmentUtils.getInvestmentTypeNameForSecurity(future));
	}


	@Test
	public void testRemoveStrikeFromOptionOnFuture() {
		InvestmentSecurity future = InvestmentTestObjectFactory.newInvestmentSecurity(1, "SPH8", InvestmentType.FUTURES);
		InvestmentSecurity option = InvestmentTestObjectFactory.newInvestmentSecurity(2, "EVZ7P 2200", InvestmentType.OPTIONS);

		option.setUnderlyingSecurity(future);
		Assertions.assertEquals("EVZ7P", InvestmentUtils.getOptionOnFutureSymbolWithoutStrikePrice(option));
	}


	@Test
	public void testRemoveStrikeFromOptionOnFutureFailure() {
		InvestmentSecurity option = InvestmentTestObjectFactory.newInvestmentSecurity(2, "EVZ7P 2200", InvestmentType.OPTIONS);

		TestUtils.expectException(ValidationException.class, () -> InvestmentUtils.getOptionOnFutureSymbolWithoutStrikePrice(option), "Cannot remove strike price because [" + option + "] is not a valid Option of Futures.");
	}


	@Test
	public void testGetFutureYear() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(1, "COH15", InvestmentType.FUTURES);
		Assertions.assertEquals(new Integer(2015), InvestmentUtils.getFutureYear(security));

		security = InvestmentTestObjectFactory.newInvestmentSecurity(1, "COH4", InvestmentType.FUTURES);
		Assertions.assertEquals(new Integer(2024), InvestmentUtils.getFutureYear(security));

		security = InvestmentTestObjectFactory.newInvestmentSecurity(1, "C H16", InvestmentType.FUTURES);
		Assertions.assertEquals(new Integer(2016), InvestmentUtils.getFutureYear(security));

		security = InvestmentTestObjectFactory.newInvestmentSecurity(1, "C H11", InvestmentType.FUTURES);
		Assertions.assertEquals(new Integer(2011), InvestmentUtils.getFutureYear(security));

		Assertions.assertEquals(new Integer(2026), InvestmentUtils.getFutureYear(InvestmentTestObjectFactory.newInvestmentSecurity(1, "Z M6", InvestmentType.FUTURES)));
		// Note: This will break on 1/1/2030
		Assertions.assertEquals(new Integer(2029), InvestmentUtils.getFutureYear(InvestmentTestObjectFactory.newInvestmentSecurity(1, "Z M9", InvestmentType.FUTURES)));
	}


	@Test
	public void testGetFutureMonth() {

		Assertions.assertEquals(new Integer(1), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COF5", InvestmentType.FUTURES)));
		Assertions.assertEquals(new Integer(2), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COG5", InvestmentType.FUTURES)));
		Assertions.assertEquals(new Integer(3), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COH5", InvestmentType.FUTURES)));
		Assertions.assertEquals(new Integer(4), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COJ5", InvestmentType.FUTURES)));
		Assertions.assertEquals(new Integer(5), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COK5", InvestmentType.FUTURES)));
		Assertions.assertEquals(new Integer(6), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COM5", InvestmentType.FUTURES)));
		Assertions.assertEquals(new Integer(7), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "CON5", InvestmentType.FUTURES)));
		Assertions.assertEquals(new Integer(8), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COQ5", InvestmentType.FUTURES)));
		Assertions.assertEquals(new Integer(9), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COU5", InvestmentType.FUTURES)));
		Assertions.assertEquals(new Integer(10), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COV5", InvestmentType.FUTURES)));
		Assertions.assertEquals(new Integer(11), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COX5", InvestmentType.FUTURES)));
		Assertions.assertEquals(new Integer(12), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COZ5", InvestmentType.FUTURES)));

		Assertions.assertEquals(new Integer(1), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COF14", InvestmentType.FUTURES)));
		Assertions.assertEquals(new Integer(2), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COG11", InvestmentType.FUTURES)));
		Assertions.assertEquals(new Integer(3), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COH10", InvestmentType.FUTURES)));

		Assertions.assertEquals(new Integer(3), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurity(1, "COH10", InvestmentType.FUTURES)));
	}


	@Test
	public void testGetFutureMonthWithPrefixes() {
		Assertions.assertEquals(new Integer(6), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "Z M6", InvestmentType.FUTURES, "Z")));

		Assertions.assertEquals(new Integer(1), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COF5", InvestmentType.FUTURES, "CO")));
		Assertions.assertEquals(new Integer(2), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COG5", InvestmentType.FUTURES, "CO")));
		Assertions.assertEquals(new Integer(3), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COH5", InvestmentType.FUTURES, "CO")));
		Assertions.assertEquals(new Integer(4), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COJ5", InvestmentType.FUTURES, "CO")));
		Assertions.assertEquals(new Integer(5), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COK5", InvestmentType.FUTURES, "CO")));
		Assertions.assertEquals(new Integer(6), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COM5", InvestmentType.FUTURES, "CO")));
		Assertions.assertEquals(new Integer(7), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "CON5", InvestmentType.FUTURES, "CO")));
		Assertions.assertEquals(new Integer(8), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COQ5", InvestmentType.FUTURES, "CO")));
		Assertions.assertEquals(new Integer(9), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COU5", InvestmentType.FUTURES, "CO")));
		Assertions.assertEquals(new Integer(10), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COV5", InvestmentType.FUTURES, "CO")));
		Assertions.assertEquals(new Integer(11), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COX5", InvestmentType.FUTURES, "CO")));
		Assertions.assertEquals(new Integer(12), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COZ5", InvestmentType.FUTURES, "CO")));

		Assertions.assertEquals(new Integer(1), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COF14", InvestmentType.FUTURES, "CO")));
		Assertions.assertEquals(new Integer(2), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COG11", InvestmentType.FUTURES, "CO")));
		Assertions.assertEquals(new Integer(3), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COH10", InvestmentType.FUTURES, "CO")));

		Assertions.assertEquals(new Integer(3), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "COH10", InvestmentType.FUTURES, "CO")));

		Assertions.assertEquals(new Integer(1), InvestmentUtils.getFutureMonth(InvestmentTestObjectFactory.newInvestmentSecurityWithPrefix(1, "USF2C 146", InvestmentType.FUTURES, "OF-US")));
	}

	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidatePriceMultiplierValue_InvalidZero() {
		Assertions.assertThrows(ValidationException.class, () -> InvestmentUtils.validatePriceMultiplierValue(BigDecimal.ZERO));
	}


	@Test
	public void testValidatePriceMultiplierValue_InvalidNegative() {
		Assertions.assertThrows(ValidationException.class, () -> InvestmentUtils.validatePriceMultiplierValue(BigDecimal.valueOf(-100.0)));
	}


	@Test
	public void testValidatePriceMultiplierValue_Valid() {
		InvestmentUtils.validatePriceMultiplierValue(BigDecimal.ONE);
		InvestmentUtils.validatePriceMultiplierValue(MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		InvestmentUtils.validatePriceMultiplierValue(BigDecimal.valueOf(1000.0));
		InvestmentUtils.validatePriceMultiplierValue(BigDecimal.valueOf(856));
	}

	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetUltimateUnderlyingSecurity_CircularDependency() {
		Assertions.assertThrows(ValidationException.class, () -> {
			InvestmentSecurity optionSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(1111, "ABC-Option", InvestmentType.OPTIONS);
			InvestmentSecurity futureSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(2222, "ABC-Future", InvestmentType.FUTURES);
			optionSecurity.setUnderlyingSecurity(futureSecurity);
			futureSecurity.setUnderlyingSecurity(optionSecurity);

			InvestmentUtils.getUltimateUnderlyingSecurity(optionSecurity);
		});
	}


	@Test
	public void testGetSettlementCurrency() {
		InvestmentSecurity optionSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(1111, "ABC-Option", InvestmentType.OPTIONS);
		Assertions.assertEquals(InvestmentTestObjectFactory.getCurrencyForSymbol("USD"), InvestmentUtils.getSettlementCurrency(optionSecurity));

		optionSecurity.setSettlementCurrency(InvestmentTestObjectFactory.getCurrencyForSymbol("GBP"));
		Assertions.assertEquals(InvestmentTestObjectFactory.getCurrencyForSymbol("GBP"), InvestmentUtils.getSettlementCurrency(optionSecurity));
	}


	@Test
	public void testGetUltimateUnderlyingSecurity() {
		InvestmentSecurity optionSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(1111, "ABC-Option", InvestmentType.OPTIONS);
		InvestmentSecurity futureSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(2222, "ABC-Future", InvestmentType.FUTURES);
		InvestmentSecurity benchmarkSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(3333, "ABC-Benchmark", InvestmentType.BENCHMARKS);
		optionSecurity.setUnderlyingSecurity(futureSecurity);
		futureSecurity.setUnderlyingSecurity(benchmarkSecurity);

		// Check Option Security
		Assertions.assertEquals(benchmarkSecurity, InvestmentUtils.getUltimateUnderlyingSecurity(optionSecurity));
		// Check Future Security
		Assertions.assertEquals(benchmarkSecurity, InvestmentUtils.getUltimateUnderlyingSecurity(futureSecurity));
		// Check Benchmark Security
		Assertions.assertNull(InvestmentUtils.getUltimateUnderlyingSecurity(benchmarkSecurity));
	}

	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetSecurityOverrideKey() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(1111, "ABC", InvestmentType.BONDS);
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		hierarchy.setName("Bills");
		hierarchy.setParent(new InvestmentInstrumentHierarchy("Central Government Bonds"));
		hierarchy.getParent().setParent(new InvestmentInstrumentHierarchy("Government Bonds"));
		hierarchy.getParent().getParent().setParent(new InvestmentInstrumentHierarchy("Physicals"));
		hierarchy.getParent().getParent().getParent().setParent(new InvestmentInstrumentHierarchy("Fixed Income"));

		InvestmentTypeSubType subType = new InvestmentTypeSubType();
		subType.setName("Central Government Bonds");
		hierarchy.setInvestmentTypeSubType(subType);

		InvestmentTypeSubType2 subType2 = new InvestmentTypeSubType2();
		subType2.setName("Bills");
		hierarchy.setInvestmentTypeSubType2(subType2);

		List<String> keyList = new ArrayList<>();
		keyList.add("Security Maturity");
		keyList.add("Security Maturity: [Bonds]");
		keyList.add("Security Maturity: [Bonds:Central Government Bonds]");
		keyList.add("Security Maturity: [Bonds:Central Government Bonds:Bills]");
		keyList.add("Security Maturity: [Funds:Proprietary Funds]"); // Note: Proprietary Funds don't have event types but used for testing one hierarchy level

		// Hierarchy Specific - No Parent Hierarchy
		InvestmentSecurity fundSecurity = InvestmentTestObjectFactory.newInvestmentSecurity(2222, "DE", InvestmentType.FUNDS);
		InvestmentInstrumentHierarchy fundHierarchy = fundSecurity.getInstrument().getHierarchy();
		fundHierarchy.setName("Proprietary Funds");
		String keyOverride = InvestmentUtils.getSecurityOverrideKey("Security Maturity: [Funds:Proprietary Funds]", fundSecurity, keyList);
		String expectedKey = "Security Maturity: [Funds:Proprietary Funds]";
		Assertions.assertEquals(expectedKey, keyOverride);

		// Type/Sub Type/Sub Type 2 Specific
		expectedKey = "Security Maturity: [Bonds:Central Government Bonds:Bills]";
		keyOverride = InvestmentUtils.getSecurityOverrideKey("Security Maturity", security, keyList);
		Assertions.assertEquals(expectedKey, keyOverride);
		keyList.remove(expectedKey);

		// Type/Sub Type Specific
		expectedKey = "Security Maturity: [Bonds:Central Government Bonds]";
		keyOverride = InvestmentUtils.getSecurityOverrideKey("Security Maturity", security, keyList);
		Assertions.assertEquals(expectedKey, keyOverride);
		keyList.remove(expectedKey);

		// Type Specific
		expectedKey = "Security Maturity: [Bonds]";
		keyOverride = InvestmentUtils.getSecurityOverrideKey("Security Maturity", security, keyList);
		Assertions.assertEquals(expectedKey, keyOverride);
		keyList.remove(expectedKey);

		// No Override
		expectedKey = "Security Maturity";
		keyOverride = InvestmentUtils.getSecurityOverrideKey("Security Maturity", security, keyList);
		Assertions.assertEquals(expectedKey, keyOverride);

		// Clear Sub Type 2 from Hierarchy
		hierarchy.setInvestmentTypeSubType2(null);
		keyList.add("Security Maturity: [Bonds]");
		keyList.add("Security Maturity: [Bonds:Central Government Bonds]");
		keyList.add("Security Maturity: [Bonds:Central Government Bonds:Bills]");

		expectedKey = "Security Maturity: [Bonds:Central Government Bonds]";
		keyOverride = InvestmentUtils.getSecurityOverrideKey("Security Maturity", security, keyList);
		Assertions.assertEquals(expectedKey, keyOverride);

		// Clear Sub Type from Hierarchy
		hierarchy.setInvestmentTypeSubType(null);
		expectedKey = "Security Maturity: [Bonds]";
		keyOverride = InvestmentUtils.getSecurityOverrideKey("Security Maturity", security, keyList);
		Assertions.assertEquals(expectedKey, keyOverride);
	}

	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testIsSecurityActiveOn() {

		InvestmentSecurity security = new InvestmentSecurity();
		Assertions.assertTrue(InvestmentUtils.isSecurityActiveOn(security, DateUtils.toDate("07/11/2016")));

		security.setStartDate(DateUtils.toDate("05/01/2016"));
		Assertions.assertTrue(InvestmentUtils.isSecurityActiveOn(security, DateUtils.toDate("07/11/2016")));
		Assertions.assertFalse(InvestmentUtils.isSecurityActiveOn(security, DateUtils.toDate("04/01/2015")));

		security.setEndDate(DateUtils.toDate("05/01/2017"));
		Assertions.assertTrue(InvestmentUtils.isSecurityActiveOn(security, DateUtils.toDate("07/11/2016")));
		Assertions.assertFalse(InvestmentUtils.isSecurityActiveOn(security, DateUtils.toDate("07/11/2017")));

		security.setLastDeliveryDate(DateUtils.toDate("05/05/2017"));
		Assertions.assertTrue(InvestmentUtils.isSecurityActiveOn(security, DateUtils.toDate("05/04/2017")));
		Assertions.assertFalse(InvestmentUtils.isSecurityActiveOn(security, DateUtils.toDate("05/07/2017")));

		security.setEarlyTerminationDate(DateUtils.toDate("01/31/2017"));
		Assertions.assertTrue(InvestmentUtils.isSecurityActiveOn(security, DateUtils.toDate("07/11/2016")));
		Assertions.assertFalse(InvestmentUtils.isSecurityActiveOn(security, DateUtils.toDate("02/11/2017")));
	}


	@Test
	public void testOccStrikePriceStringConversion() {
		validateOccStrikePriceConversionForPrice("55.5");
		validateOccStrikePriceConversionForPrice("2250");
		validateOccStrikePriceConversionForPrice("2250.5");
		validateOccStrikePriceConversionForPrice("2245.65");
		validateOccStrikePriceConversionForPrice("39.5");
		validateOccStrikePriceConversionForPrice("39");
		validateOccStrikePriceConversionForPrice(new BigDecimal("55.5"));
		validateOccStrikePriceConversionForPrice(new BigDecimal("2250"));
		validateOccStrikePriceConversionForPrice(new BigDecimal("2250.5"));
		validateOccStrikePriceConversionForPrice(new BigDecimal("2245.65"));
		validateOccStrikePriceConversionForPrice(new BigDecimal("39.5"));
		validateOccStrikePriceConversionForPrice(new BigDecimal("39"));

		//Invalid
		testInvalidOccStrike("222222.555", "Strike price whole dollar string [222222] is larger than allowed. Only 5 digits are allowed.");
		testInvalidOccStrike("22222.5555", "Strike price decimal string [5555] is larger than allowed. Only 3 digits are allowed.");
		testInvalidOccStrike("222222.5555", "Strike price whole dollar string [222222] is larger than allowed. Only 5 digits are allowed.");
		testInvalidOccStrike(new BigDecimal("222222.555"), "Strike price whole dollar string [222222] is larger than allowed. Only 5 digits are allowed.");
		testInvalidOccStrike(new BigDecimal("22222.5555"), "Strike price decimal string [5555] is larger than allowed. Only 3 digits are allowed.");
		testInvalidOccStrike(new BigDecimal("222222.5555"), "Strike price whole dollar string [222222] is larger than allowed. Only 5 digits are allowed.");
	}


	private void validateOccStrikePriceConversionForPrice(Object strikePrice) {
		String strikePriceString;
		if (strikePrice instanceof BigDecimal) {
			strikePriceString = ((BigDecimal) strikePrice).stripTrailingZeros().toPlainString();
		}
		else {
			strikePriceString = strikePrice.toString();
		}
		Assertions.assertEquals(getOptionOccStrikePriceString(strikePriceString), InvestmentUtils.getOccStrikePriceString(strikePriceString));
	}


	private void testInvalidOccStrike(Object strikePrice, String exceptionMessage) {
		TestUtils.expectException(ValidationException.class, () -> validateOccStrikePriceConversionForPrice(strikePrice), exceptionMessage);
	}


	// This is copied from InvestmentUtils to validate independently of IMS
	private String getOptionOccStrikePriceString(String strikeString) {
		int decimalIndex = strikeString.indexOf(".");
		String explicitStrike, decimal;
		if (decimalIndex > -1) {
			explicitStrike = strikeString.substring(0, decimalIndex);
			decimal = strikeString.substring(decimalIndex + 1);
		}
		else {
			explicitStrike = strikeString;
			decimal = StringUtils.EMPTY_STRING;
		}
		StringBuilder occStrikeStringBuilder = new StringBuilder(explicitStrike).append(decimal);
		int prefixingZeros = 5 - explicitStrike.length();
		for (int i = 0; i < prefixingZeros; i++) {
			occStrikeStringBuilder.insert(0, '0');
		}
		int suffixingZeros = 3 - decimal.length();
		for (int i = 0; i < suffixingZeros; i++) {
			occStrikeStringBuilder.append('0');
		}
		return occStrikeStringBuilder.toString();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetCurrencySymbol() {
		Assertions.assertNull(InvestmentUtils.getCurrencySymbol(null, true));
		Assertions.assertNull(InvestmentUtils.getCurrencySymbol(null, false));
		Assertions.assertEquals("", InvestmentUtils.getCurrencySymbol("", true));
		Assertions.assertNull(InvestmentUtils.getCurrencySymbol("", false));

		Assertions.assertEquals("$", InvestmentUtils.getCurrencySymbol("USD", true));
		Assertions.assertEquals("$", InvestmentUtils.getCurrencySymbol("usd", true));
		Assertions.assertEquals("$", InvestmentUtils.getCurrencySymbol("USD", false));

		Assertions.assertEquals("£", InvestmentUtils.getCurrencySymbol("GBP", true));
		Assertions.assertEquals("€", InvestmentUtils.getCurrencySymbol("EUR", false));
		Assertions.assertEquals("¥", InvestmentUtils.getCurrencySymbol("jpy", false));

		Assertions.assertEquals("CAD", InvestmentUtils.getCurrencySymbol("CAD", true));
		Assertions.assertNull(InvestmentUtils.getCurrencySymbol("CAD", false));
		Assertions.assertEquals("cad", InvestmentUtils.getCurrencySymbol("cad", true));
	}


	@Test
	public void testGetInternalCUSIP() {
		String cusip = InvestmentUtils.getInternalCUSIP(1);
		Assertions.assertEquals("990000010", cusip);
		SecurityIdentifierUtils.isValidCUSIP(cusip, true);

		cusip = InvestmentUtils.getInternalCUSIP(2);
		Assertions.assertEquals("990000028", cusip);
		SecurityIdentifierUtils.isValidCUSIP(cusip, true);

		cusip = InvestmentUtils.getInternalCUSIP(1234);
		Assertions.assertEquals("990012346", cusip);
		SecurityIdentifierUtils.isValidCUSIP(cusip, true);
	}


	@Test
	public void testFormatMoneyWithCurrency_NullAmount() {
		TestUtils.expectException(RuntimeException.class, () -> InvestmentUtils.formatAmountWithCurrency(null, "USD", false, null), "Amount cannot be null");
	}


	@Test
	public void testFormatMoneyWithCurrency() {
		BigDecimal amount = BigDecimal.valueOf(1000.00);
		Assertions.assertEquals("$ 1,000.00", InvestmentUtils.formatAmountWithCurrency(amount, "USD", true, null));
		Assertions.assertEquals("$ 1,000.00", InvestmentUtils.formatAmountWithCurrency(amount, "usd", true, null));
		Assertions.assertEquals("$1,000.00", InvestmentUtils.formatAmountWithCurrency(amount, "USD", false, null));
		Assertions.assertEquals("$1,000.00", InvestmentUtils.formatAmountWithCurrency(amount, "usd", false, null));

		Assertions.assertEquals("1,000.00 CAD", InvestmentUtils.formatAmountWithCurrency(amount, "CAD", true, null));
		Assertions.assertEquals("1,000.00 cad", InvestmentUtils.formatAmountWithCurrency(amount, "cad", true, null));
		Assertions.assertEquals("1,000.00 CAD", InvestmentUtils.formatAmountWithCurrency(amount, "CAD", false, null));
		Assertions.assertEquals("1,000.00 cad", InvestmentUtils.formatAmountWithCurrency(amount, "cad", false, null));

		Assertions.assertEquals("1,000.00 ", InvestmentUtils.formatAmountWithCurrency(amount, null, true, null));
		Assertions.assertEquals("1,000.00 ", InvestmentUtils.formatAmountWithCurrency(amount, "", false, null));

		amount = BigDecimal.valueOf(1000);
		Assertions.assertEquals("$ 1,000", InvestmentUtils.formatAmountWithCurrency(amount, "USD", true, CoreMathUtils::formatNumberDecimal));
		Assertions.assertEquals("$1,000", InvestmentUtils.formatAmountWithCurrency(amount, "USD", false, CoreMathUtils::formatNumberDecimal));
	}


	@Test
	public void testIsFactorChangeSupported() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(1111, "XY3601J26U0500XXI", "Swaps");
		Assertions.assertNull(InvestmentUtils.getFactorChangeEventTypeName(security));
		Assertions.assertFalse(InvestmentUtils.isFactorChangeSupported(security));
		security.getInstrument().getHierarchy().setFactorChangeEventType(InvestmentTestObjectFactory.newInvestmentSecurityEventType("Credit Event"));
		Assertions.assertEquals("Credit Event", InvestmentUtils.getFactorChangeEventTypeName(security));
		Assertions.assertTrue(InvestmentUtils.isFactorChangeSupported(security));
	}
}
