package com.clifton.investment.instrument;

import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * @author manderson
 */
public class InvestmentSecurityTests {

	@Test
	public void testBelongsToPrimaryExchange() {
		InvestmentSecurity ibm = InvestmentSecurityBuilder.newStock("IBM").toInvestmentSecurity();
		Assertions.assertFalse(ibm.belongsToPrimaryExchange("UN"));

		ibm.getInstrument().setExchange(new InvestmentExchange());
		ibm.getInstrument().getExchange().setExchangeCode("UN");
		Assertions.assertFalse(ibm.belongsToPrimaryExchange("US"));
		Assertions.assertTrue(ibm.belongsToPrimaryExchange("UN"));
	}


	@Test
	public void testBelongsToPrimaryOrCompositeExchange() {
		InvestmentSecurity ibm = InvestmentSecurityBuilder.newStock("IBM").toInvestmentSecurity();
		Assertions.assertFalse(ibm.belongsToPrimaryOrCompositeExchange("UN"));

		ibm.getInstrument().setExchange(new InvestmentExchange());
		ibm.getInstrument().getExchange().setExchangeCode("UN");
		ibm.getInstrument().setCompositeExchange(new InvestmentExchange());
		ibm.getInstrument().getCompositeExchange().setExchangeCode("US");
		Assertions.assertFalse(ibm.belongsToPrimaryOrCompositeExchange("UW"));
		Assertions.assertTrue(ibm.belongsToPrimaryOrCompositeExchange("US"));
		Assertions.assertTrue(ibm.belongsToPrimaryOrCompositeExchange("UN"));
	}


	@Test
	public void testGetPriceMultiplier() {
		// Set up Security with Only Instrument Level Price Multiplier Defined
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(1, "TEST-1", InvestmentType.FUTURES, MathUtils.BIG_DECIMAL_ONE_HUNDRED, "USD");
		Assertions.assertNull(security.getPriceMultiplierOverride());
		Assertions.assertEquals(MathUtils.BIG_DECIMAL_ONE_HUNDRED, security.getPriceMultiplier());
		Assertions.assertEquals(MathUtils.BIG_DECIMAL_ONE_HUNDRED, security.getInstrument().getPriceMultiplier());
		Assertions.assertEquals(MathUtils.BIG_DECIMAL_ONE_HUNDRED, security.getPriceMultiplier());


		// Add Security Level Override
		BigDecimal override = BigDecimal.valueOf(200.00);
		security.setPriceMultiplierOverride(override);
		Assertions.assertEquals(override, security.getPriceMultiplierOverride());
		Assertions.assertEquals(override, security.getPriceMultiplier());
		Assertions.assertEquals(MathUtils.BIG_DECIMAL_ONE_HUNDRED, security.getInstrument().getPriceMultiplier());
		Assertions.assertEquals(override, security.getPriceMultiplier());
	}
}
