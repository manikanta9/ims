package com.clifton.investment.instrument.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.accrual.AccrualMethods;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.specificity.InvestmentSpecificityDefinition;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityField;
import com.clifton.investment.specificity.InvestmentSpecificityUtilHandler;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Test class for {@link InvestmentSecurityValidator}.
 *
 * @author MikeH
 */
public class InvestmentSecurityValidatorTest {

	private static final String FIELD_NAME = "fieldName";
	private static final String CONDITION_NAME = "conditionName";
	private static final String EVALUATION_MESSAGE = "Evaluation message";
	private static final int FIGI_LENGTH = 12;

	@InjectMocks
	private InvestmentSecurityValidator investmentSecurityValidator;
	@Mock
	private InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler;
	@Mock
	private SystemConditionService systemConditionService;
	@Mock
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	/**
	 * Incrementing ID for generated system conditions.
	 */
	private final AtomicInteger serialConditionId = new AtomicInteger();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFailSaveInvestmentSecuritySpecificityValidation() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(123432, "1350Z7TZ7", InvestmentType.BONDS);
		configureValidatorEntry(Collections.singletonList(security), null);
		TestUtils.expectException(FieldValidationException.class, () -> getInvestmentSecurityValidator().validate(security, DaoEventTypes.SAVE), String.format("%s validation failed on field [%s] for condition [%s]: %s", InvestmentSpecificityDefinition.DEFINITION_SECURITY_VALIDATOR, FIELD_NAME, CONDITION_NAME, EVALUATION_MESSAGE));
	}


	@Test
	public void testSuccessSaveInvestmentSecuritySpecificityValidationOtherSecurity() {
		InvestmentSecurity security1 = InvestmentTestObjectFactory.newInvestmentSecurity(123432, "1350Z7TZ7", InvestmentType.BONDS);
		InvestmentSecurity security2 = InvestmentTestObjectFactory.newInvestmentSecurity(432123, "1350Z7TZ7", InvestmentType.BONDS);
		configureValidatorEntry(Collections.singletonList(security1), null);
		getInvestmentSecurityValidator().validate(security2, DaoEventTypes.SAVE);
	}


	@Test
	public void testSuccessSaveInvestmentSecuritySpecificityValidation() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(123432, "1350Z7TZ7", InvestmentType.BONDS);
		configureValidatorEntry(Collections.singletonList(security), Collections.singletonList(security));
		getInvestmentSecurityValidator().validate(security, DaoEventTypes.SAVE);
	}


	@Test
	public void testFailSaveInvestmentSecurityFigiLengthValidation() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(123432, "1350Z7TZ7", InvestmentType.BONDS);
		security.setFigi("123");
		configureValidatorEntry(Collections.singletonList(security), Collections.singletonList(security));
		TestUtils.expectException(FieldValidationException.class, () -> getInvestmentSecurityValidator().validate(security, DaoEventTypes.SAVE), String.format("The FIGI field must be exactly %s characters long.", FIGI_LENGTH));
	}


	@Test
	public void testFailSaveInvestmentSecurityFigiOTCValidation() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(123432, "1350Z7TZ7", InvestmentType.BONDS);
		security.setFigi("123456789012");
		security.getInstrument().getHierarchy().setOtc(true);
		configureValidatorEntry(Collections.singletonList(security), Collections.singletonList(security));
		TestUtils.expectException(FieldValidationException.class, () -> getInvestmentSecurityValidator().validate(security, DaoEventTypes.SAVE), "The FIGI field is not allowed for OTC securities.");
	}


	@Test
	public void testSuccessSaveInvestmentSecurityFigiValidation() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(123432, "1350Z7TZ7", InvestmentType.BONDS);
		security.setFigi("123456789012");
		security.getInstrument().getHierarchy().setOtc(false);
		configureValidatorEntry(Collections.singletonList(security), Collections.singletonList(security));
		getInvestmentSecurityValidator().validate(security, DaoEventTypes.SAVE);
	}


	@Test
	public void testSuccessSaveInvestmentSecurityFigiNullOTCTrue() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(123432, "1350Z7TZ7", InvestmentType.BONDS);
		security.getInstrument().getHierarchy().setOtc(true);
		configureValidatorEntry(Collections.singletonList(security), Collections.singletonList(security));
		getInvestmentSecurityValidator().validate(security, DaoEventTypes.SAVE);
	}


	@Test
	public void testSaveInvestmentSecurityAccrualMethod() {
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(123432, "EA13901GUL", InvestmentType.SWAPS);
		InvestmentSecurityEventType accrualEventType = security.getInstrument().getHierarchy().getAccrualSecurityEventType();
		security.getInstrument().getHierarchy().setAccrualSecurityEventType(null);
		security.setAccrualMethod(AccrualMethods.DAILY_COMPOUNDING);
		TestUtils.expectException(FieldValidationException.class, () -> getInvestmentSecurityValidator().validate(security, DaoEventTypes.SAVE), "Accrual Method should not be set on a Security that does not have Accruals");

		security.getInstrument().getHierarchy().setAccrualSecurityEventType(accrualEventType);
		getInvestmentSecurityValidator().validate(security, DaoEventTypes.SAVE);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void configureInvestmentSpecificityValidatorEntryList(ArgumentMatcher<InvestmentSecurity> securityMatcher, List<InvestmentSpecificityEntry> validatorEntryList) {
		Mockito.when(getInvestmentSpecificityUtilHandler().getInvestmentSpecificityEntryListForSecurity(ArgumentMatchers.eq(InvestmentSpecificityDefinition.DEFINITION_SECURITY_VALIDATOR), ArgumentMatchers.argThat(securityMatcher))).thenReturn(validatorEntryList);
	}


	/**
	 * Configures the validator entry to produce a condition which succeeds only for the provided securities. This validator cannot be un-configured without
	 * reloading the mock objects.
	 */
	private void configureValidatorEntry(List<InvestmentSecurity> applicableSecurities, List<InvestmentSecurity> successSecurities) {
		int conditionId = this.serialConditionId.getAndIncrement();
		InvestmentSpecificityEntry entry = new InvestmentSpecificityEntry();
		InvestmentSpecificityField field = new InvestmentSpecificityField();
		field.setName(FIELD_NAME);
		entry.setField(field);
		InvestmentSpecificityInvestmentValidator validator = new InvestmentSpecificityInvestmentValidator();
		validator.setValidationConditionId(conditionId);
		SystemCondition condition = new SystemCondition();
		condition.setName(CONDITION_NAME);

		configureInvestmentSpecificityValidatorEntryList(item -> CollectionUtils.getStream(applicableSecurities).anyMatch(security -> security == item), Collections.singletonList(entry));
		Mockito.when(getInvestmentSpecificityUtilHandler().generatePopulatedSpecificityBean(ArgumentMatchers.same(entry))).thenReturn(validator);
		Mockito.when(getSystemConditionService().getSystemCondition(ArgumentMatchers.eq(conditionId))).thenReturn(condition);
		Mockito.when(getSystemConditionEvaluationHandler().evaluateCondition(ArgumentMatchers.same(condition), ArgumentMatchers.any(InvestmentSecurity.class)))
				.then(invocation -> {
					InvestmentSecurity security = (InvestmentSecurity) invocation.getArguments()[1];
					boolean successSecurity = CollectionUtils.contains(successSecurities, security);
					return new EvaluationResult(successSecurity, EVALUATION_MESSAGE, condition, security);
				});
	}


	public InvestmentSecurityValidator getInvestmentSecurityValidator() {
		return this.investmentSecurityValidator;
	}


	public void setInvestmentSecurityValidator(InvestmentSecurityValidator investmentSecurityValidator) {
		this.investmentSecurityValidator = investmentSecurityValidator;
	}


	public InvestmentSpecificityUtilHandler getInvestmentSpecificityUtilHandler() {
		return this.investmentSpecificityUtilHandler;
	}


	public void setInvestmentSpecificityUtilHandler(InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler) {
		this.investmentSpecificityUtilHandler = investmentSpecificityUtilHandler;
	}


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
