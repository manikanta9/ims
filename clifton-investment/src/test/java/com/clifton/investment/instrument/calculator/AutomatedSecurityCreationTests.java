package com.clifton.investment.instrument.calculator;

import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.InvestmentInMemoryDatabaseContext;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.copy.jobs.InvestmentSecurityCreationForwardJob;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

;


/**
 * @author mitchellf
 */
@InvestmentInMemoryDatabaseContext
public class AutomatedSecurityCreationTests extends BaseInMemoryDatabaseTests {

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private CalendarScheduleService calendarScheduleService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;


	@Test
	public void testCopyForward_nonDeliverable() {

		// Create manual calendar calculator bean with defined include calendar list
		SystemBean calculator = AutomatedSecurityCreationTestUtils.createCalendarListCalculatorBean(this.systemBeanService, this.calendarSetupService);

		// Create bean for evaluating maturity date
		SystemBean jobBean = new SystemBean();
		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(AutomatedSecurityCreationTestUtils.JOB_BEAN_TYPE);

		jobBean.setType(beanType);
		jobBean.setName("Simple Security Creation Bean");
		jobBean.setDescription("Test bean");

		List<SystemBeanPropertyType> propertyTypes = this.systemBeanService.getSystemBeanPropertyTypeListByBean(jobBean);

		List<SystemBeanProperty> properties = new ArrayList<>();

		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setName(AutomatedSecurityCreationTestUtils.BRZ_FWD_INSTRUMENT);
		InvestmentInstrument instrument = CollectionUtils.getOnlyElement(this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm));

		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.INSTRUMENTS, instrument.getId().toString(), jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.CALENDAR_CALCULATOR, calculator.getId().toString(), jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.EVALUATION_DATE_OVERRIDE, "11/30/2020", jobBean);

		// First, we'll try a maturity schedule where the security matures before the next occurrence - should NOT create a security
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setName("Test Schedule");
		schedule.setDescription("Test Schedule");
		schedule.setCalendar(this.calendarSetupService.getCalendarByName(AutomatedSecurityCreationTestUtils.US_BANKS));
		schedule.setCalendarScheduleType(this.calendarScheduleService.getCalendarScheduleTypeByName("Standard Schedules"));
		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);
		schedule.setStartDate(DateUtils.toDate("12/31/2021"));
		schedule.setEndDate(DateUtils.toDate("01/31/2022"));
		schedule.setRecurrence(1);
		schedule.setFrequency(ScheduleFrequencies.MONTHLY);
		schedule.setDayOfMonth(4);

		this.calendarScheduleService.saveCalendarSchedule(schedule);

		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.MATURITY_SCHEDULE, schedule.getId().toString(), jobBean);

		jobBean = this.systemBeanService.saveSystemBean(jobBean);

		// Cast to proper type and evaluate
		InvestmentSecurityCreationForwardJob job = (InvestmentSecurityCreationForwardJob) this.systemBeanService.getBeanInstance(jobBean);
		Status result = job.run(new HashMap<>());
		AutomatedSecurityCreationTestUtils.validateResults(this.investmentInstrumentService, result, 1, 0, 0, null, null);

		// then, try one where it matures after the occurrence - should create a security
		schedule.setDayOfMonth(10);
		this.calendarScheduleService.saveCalendarSchedule(schedule);
		result = job.run(new HashMap<>());
		AutomatedSecurityCreationTestUtils.validateResults(this.investmentInstrumentService, result, 0, 1, 0, DateUtils.toDate("01/06/2022"), instrument);
	}


	@Test
	public void testCopyForward_deliverable() {

		// Create manual calendar calculator bean with defined include calendar list
		SystemBean calculator = AutomatedSecurityCreationTestUtils.createCalendarListCalculatorBean(this.systemBeanService, this.calendarSetupService);

		// Create bean for evaluating maturity date
		SystemBean jobBean = new SystemBean();
		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(AutomatedSecurityCreationTestUtils.JOB_BEAN_TYPE);

		jobBean.setType(beanType);
		jobBean.setName("Simple Security Creation Bean");
		jobBean.setDescription("Test bean");

		List<SystemBeanPropertyType> propertyTypes = this.systemBeanService.getSystemBeanPropertyTypeListByBean(jobBean);

		List<SystemBeanProperty> properties = new ArrayList<>();

		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setName(AutomatedSecurityCreationTestUtils.USD_FWD_INSTRUMENT);
		InvestmentInstrument instrument = CollectionUtils.getOnlyElement(this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm));

		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.INSTRUMENTS, instrument.getId().toString(), jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.CALENDAR_CALCULATOR, calculator.getId().toString(), jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.EVALUATION_DATE_OVERRIDE, "11/30/2020", jobBean);

		// First, we'll try a maturity schedule where the security matures before the next occurrence - should NOT create a security
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setName("Test Schedule");
		schedule.setDescription("Test Schedule");
		schedule.setCalendar(this.calendarSetupService.getCalendarByName(AutomatedSecurityCreationTestUtils.US_BANKS));
		schedule.setCalendarScheduleType(this.calendarScheduleService.getCalendarScheduleTypeByName("Standard Schedules"));
		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);
		schedule.setStartDate(DateUtils.toDate("01/01/2021"));
		schedule.setEndDate(DateUtils.toDate("02/01/2021"));
		schedule.setRecurrence(1);
		schedule.setFrequency(ScheduleFrequencies.MONTHLY);
		schedule.setDayOfMonth(29);

		this.calendarScheduleService.saveCalendarSchedule(schedule);

		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.MATURITY_SCHEDULE, schedule.getId().toString(), jobBean);

		jobBean = this.systemBeanService.saveSystemBean(jobBean);

		// Cast to proper type and evaluate
		InvestmentSecurityCreationForwardJob job = (InvestmentSecurityCreationForwardJob) this.systemBeanService.getBeanInstance(jobBean);
		Status result = job.run(new HashMap<>());
		AutomatedSecurityCreationTestUtils.validateResults(this.investmentInstrumentService, result, 1, 0, 0, null, null);

		// then, try one where it matures after the occurrence - should create a security
		schedule.setDayOfMonth(25);
		this.calendarScheduleService.saveCalendarSchedule(schedule);
		result = job.run(new HashMap<>());
		AutomatedSecurityCreationTestUtils.validateResults(this.investmentInstrumentService, result, 0, 1, 0, DateUtils.toDate("01/25/2021"), instrument);
	}


	@Test
	public void testCopyForward_fieldOverrides() {

		// Create manual calendar calculator bean with defined include calendar list
		SystemBean calculator = AutomatedSecurityCreationTestUtils.createCalendarListCalculatorBean(this.systemBeanService, this.calendarSetupService);

		// Create bean for evaluating maturity date
		SystemBean jobBean = new SystemBean();
		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(AutomatedSecurityCreationTestUtils.JOB_BEAN_TYPE);

		jobBean.setType(beanType);
		jobBean.setName("Simple Security Creation Bean");
		jobBean.setDescription("Test bean");

		List<SystemBeanPropertyType> propertyTypes = this.systemBeanService.getSystemBeanPropertyTypeListByBean(jobBean);

		List<SystemBeanProperty> properties = new ArrayList<>();

		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setName(AutomatedSecurityCreationTestUtils.USD_FWD_INSTRUMENT);
		InvestmentInstrument instrument = CollectionUtils.getOnlyElement(this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm));

		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.INSTRUMENTS, instrument.getId().toString(), jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.CALENDAR_CALCULATOR, calculator.getId().toString(), jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.FILED_OVERRIDES, "startDate,11/30/2020", jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.EVALUATION_DATE_OVERRIDE, "11/30/2020", jobBean);

		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setName("Test Schedule");
		schedule.setDescription("Test Schedule");
		schedule.setCalendar(this.calendarSetupService.getCalendarByName(AutomatedSecurityCreationTestUtils.US_BANKS));
		schedule.setCalendarScheduleType(this.calendarScheduleService.getCalendarScheduleTypeByName("Standard Schedules"));
		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);
		schedule.setStartDate(DateUtils.toDate("01/01/2021"));
		schedule.setEndDate(DateUtils.toDate("02/01/2021"));
		schedule.setRecurrence(1);
		schedule.setFrequency(ScheduleFrequencies.MONTHLY);
		schedule.setDayOfMonth(25);

		this.calendarScheduleService.saveCalendarSchedule(schedule);

		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.MATURITY_SCHEDULE, schedule.getId().toString(), jobBean);

		jobBean = this.systemBeanService.saveSystemBean(jobBean);

		// Cast to proper type and evaluate
		InvestmentSecurityCreationForwardJob job = (InvestmentSecurityCreationForwardJob) this.systemBeanService.getBeanInstance(jobBean);
		Status result = job.run(new HashMap<>());
		AutomatedSecurityCreationTestUtils.validateResults(this.investmentInstrumentService, result, 0, 1, 0, DateUtils.toDate("01/25/2021"), instrument);

		SecuritySearchForm sf = new SecuritySearchForm();
		String type = instrument.getHierarchy().getInvestmentType().getName();
		sf.setEndDate(DateUtils.toDate("01/25/2021"));
		sf.setStartDate(DateUtils.toDate("11/30/2020"));

		sf.setInstrumentId(instrument.getId());

		InvestmentSecurity security = CollectionUtils.getOnlyElement(this.investmentInstrumentService.getInvestmentSecurityList(sf));

		ValidationUtils.assertNotNull(security, "Did not find expected new security");
	}


	@Test
	public void testCopyForward_nonDeliverable_withFixingDateAdjustment() {

		// Create manual calendar calculator bean with defined include calendar list
		SystemBean calculator = AutomatedSecurityCreationTestUtils.createCalendarListCalculatorBean(this.systemBeanService, this.calendarSetupService);

		// Create bean for evaluating maturity date
		SystemBean jobBean = new SystemBean();
		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(AutomatedSecurityCreationTestUtils.JOB_BEAN_TYPE);

		jobBean.setType(beanType);
		jobBean.setName("Simple Security Creation Bean");
		jobBean.setDescription("Test bean");

		List<SystemBeanPropertyType> propertyTypes = this.systemBeanService.getSystemBeanPropertyTypeListByBean(jobBean);

		List<SystemBeanProperty> properties = new ArrayList<>();

		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setName(AutomatedSecurityCreationTestUtils.BRZ_FWD_INSTRUMENT);
		InvestmentInstrument instrument = CollectionUtils.getOnlyElement(this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm));

		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.INSTRUMENTS, instrument.getId().toString(), jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.CALENDAR_CALCULATOR, calculator.getId().toString(), jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.EVALUATION_DATE_OVERRIDE, "11/30/2020", jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.FIXING_DATE, "5", jobBean);

		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setName("Test Schedule");
		schedule.setDescription("Test Schedule");
		schedule.setCalendar(this.calendarSetupService.getCalendarByName(AutomatedSecurityCreationTestUtils.US_BANKS));
		schedule.setCalendarScheduleType(this.calendarScheduleService.getCalendarScheduleTypeByName("Standard Schedules"));
		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);
		schedule.setStartDate(DateUtils.toDate("12/31/2021"));
		schedule.setEndDate(DateUtils.toDate("01/31/2022"));
		schedule.setRecurrence(1);
		schedule.setFrequency(ScheduleFrequencies.MONTHLY);
		schedule.setDayOfMonth(10);

		this.calendarScheduleService.saveCalendarSchedule(schedule);

		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.MATURITY_SCHEDULE, schedule.getId().toString(), jobBean);

		jobBean = this.systemBeanService.saveSystemBean(jobBean);

		// Cast to proper type and evaluate
		InvestmentSecurityCreationForwardJob job = (InvestmentSecurityCreationForwardJob) this.systemBeanService.getBeanInstance(jobBean);
		Status result = job.run(new HashMap<>());
		AutomatedSecurityCreationTestUtils.validateResults(this.investmentInstrumentService, result, 0, 1, 0, DateUtils.toDate("01/03/2022"), instrument);
	}


	@Test
	public void testCreateJobBean_invalidProperties() {
		SystemBean jobBean = new SystemBean();
		SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName(AutomatedSecurityCreationTestUtils.JOB_BEAN_TYPE);

		jobBean.setType(beanType);
		jobBean.setName("Simple Security Creation Bean");
		jobBean.setDescription("Test bean");

		List<SystemBeanPropertyType> propertyTypes = this.systemBeanService.getSystemBeanPropertyTypeListByBean(jobBean);

		List<SystemBeanProperty> properties = new ArrayList<>();

		TestUtils.expectException(ValidationException.class, () -> this.systemBeanService.saveSystemBean(jobBean), "Required bean property 'Instruments' is not set.");

		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setName(AutomatedSecurityCreationTestUtils.BRZ_FWD_INSTRUMENT);
		InvestmentInstrument instrument = CollectionUtils.getOnlyElement(this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm));

		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.INSTRUMENTS, instrument.getId().toString(), jobBean);

		TestUtils.expectException(ValidationException.class, () -> this.systemBeanService.saveSystemBean(jobBean), "Required bean property 'Maturity Schedule' is not set.");

		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setName("Test Schedule");
		schedule.setDescription("Test Schedule");
		schedule.setCalendar(this.calendarSetupService.getCalendarByName(AutomatedSecurityCreationTestUtils.US_BANKS));
		schedule.setCalendarScheduleType(this.calendarScheduleService.getCalendarScheduleTypeByName("Standard Schedules"));
		schedule.setBusinessDayConvention(BusinessDayConventions.NEXT);
		schedule.setStartDate(DateUtils.toDate("12/31/2021"));
		schedule.setEndDate(DateUtils.toDate("01/31/2022"));
		schedule.setRecurrence(1);
		schedule.setFrequency(ScheduleFrequencies.MONTHLY);
		schedule.setDayOfMonth(10);

		this.calendarScheduleService.saveCalendarSchedule(schedule);

		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.MATURITY_SCHEDULE, schedule.getId().toString(), jobBean);
		AutomatedSecurityCreationTestUtils.createSystemBeanProperty(properties, propertyTypes, AutomatedSecurityCreationTestUtils.FILED_OVERRIDES, "settlementDate:01/01/2020", jobBean);
		TestUtils.expectException(ValidationException.class, () -> this.systemBeanService.saveSystemBean(jobBean), "Improperly specified Security field overrides.  Overrides should be a :: delimited list of key,value pairs, eg. key1,val1::key2,val2::key3,val3");
	}
}
