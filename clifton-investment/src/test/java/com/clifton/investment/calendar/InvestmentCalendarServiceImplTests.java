package com.clifton.investment.calendar;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.list.SystemListEntity;
import com.clifton.system.list.SystemListService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class InvestmentCalendarServiceImplTests {

	@Resource
	private CalendarBusinessDayService calendarBusinessDayService;

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private InvestmentCalendarService investmentCalendarService;
	@Resource
	private InvestmentManagerAccountService investmentManagerAccountService;
	@Resource
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	@Resource
	private CalendarHolidayService calendarHolidayService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private SecurityUserService securityUserService;

	@Resource
	private SystemListService<SystemListEntity> systemListService;

	private static final short MGR_MOC_ADJ_TYPE = 10; // MOC Adjustment Type
	private static final short MGR_CONT_DIST_ADJ_TYPE = 20; // Contribution & Distribution Adj Type

	private static final short EVENT_MGR_MOC_TYPE = 10; // Calendar Event Type that Uses Mgr Balances AND is MOC
	private static final short EVENT_MGR_TYPE = 20; // Calendar Event Type that Uses Mgr Balances

	private static final short EVENT_REBALANCE_TYPE = 30; // Calendar Event Type for Rebalance (No Manager)

	private static final int TEST_MGR_1 = 1;
	private static final int TEST_MGR_2 = 2;


	@BeforeEach
	public void setupTest() {
		short year = Short.parseShort("" + DateUtils.getYear(DateUtils.addYears(new Date(), -1)));
		if (this.calendarSetupService.getCalendarYear(year) == null) {
			this.calendarSetupService.saveCalendarYearForYear(year);
		}
		year = Short.parseShort("" + DateUtils.getYear(new Date()));
		if (this.calendarSetupService.getCalendarYear(year) == null) {
			this.calendarSetupService.saveCalendarYearForYear(year);
		}
	}


	private Date getToday() {
		return DateUtils.clearTime(new Date());
	}


	private Date getYesterday() {
		return this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -1);
	}


	/////////////////////////////////////////////////////
	//  Apply Changes to Manager Balances Tests Begin
	/////////////////////////////////////////////////////


	@Test
	public void testInsertEvent_CopyToManagerBalance() {
		// Create Balance For Yesterday
		InvestmentManagerAccountBalance balance1 = setupManagerBalance(TEST_MGR_1, getYesterday());
		InvestmentManagerAccountBalance balance2 = setupManagerBalance(TEST_MGR_2, getYesterday());

		// Create Mgr Event For Today for Manager 1
		InvestmentEvent event1 = setupManagerEvent(TEST_MGR_1, false, false, getToday());

		// Pull Balance 1 from the Database Again -
		balance1 = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance1.getId());
		ValidationUtils.assertTrue(balance1.isCashAdjustment(), "Cash Value for Test Mgr 1 should be adjusted from event.");
		ValidationUtils.assertTrue(balance1.isSecuritiesAdjustment(), "Securities Value for Test Mgr 1 should be adjusted from event.");

		// Adjustments Should be the same
		InvestmentManagerAccountBalanceAdjustment mgrAdj = balance1.getAdjustmentList().get(0);
		InvestmentEventManagerAdjustment eventAdj = event1.getManagerAdjustmentList().get(0);

		ValidationUtils.assertEquals("InvestmentEventManagerAdjustment", mgrAdj.getSourceSystemTable().getName(), "Mgr Adjustment Type should be linked to event manager adj table");
		ValidationUtils.assertEquals(eventAdj.getId(), mgrAdj.getSourceFkFieldId(), "Mgr Adjustment Type should be linked to event manager adj by id");
		ValidationUtils.assertEquals(eventAdj.getAdjustmentType(), mgrAdj.getAdjustmentType(), "Mgr Adjustment Type should be the same as the one on the event");
		ValidationUtils.assertEquals(eventAdj.getCashValue(), mgrAdj.getCashValue(), "Cash should be the same as the one on the event");
		ValidationUtils.assertEquals(eventAdj.getSecuritiesValue(), mgrAdj.getSecuritiesValue(), "Securities should be the same as the one on the event");

		// Balance 2 Should Not Have anything
		balance2 = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance2.getId());
		ValidationUtils.assertFalse(balance2.isCashAdjustment(), "Cash Value for Test Mgr 2 should NOT be adjusted from event.");
		ValidationUtils.assertFalse(balance2.isSecuritiesAdjustment(), "Securities Value for Test Mgr 2 should NOT be adjusted from event.");
	}


	@Test
	public void testInsertEvent_DoNotCopyToManagerBalance() {
		// Create Balance For Yesterday
		InvestmentManagerAccountBalance balance1 = setupManagerBalance(TEST_MGR_1, getYesterday());
		InvestmentManagerAccountBalance balance2 = setupManagerBalance(TEST_MGR_2, getYesterday());

		// Create Mgr Event For Yesterday for Manager 1 (Doesn't apply because only copied automatically if event is for TODAY)
		setupManagerEvent(TEST_MGR_1, false, false, getYesterday());

		// Pull Balance 1 from the Database Again -
		balance1 = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance1.getId());
		ValidationUtils.assertFalse(balance1.isCashAdjustment(), "Cash Value for Test Mgr 1 should NOT be adjusted from event.");
		ValidationUtils.assertFalse(balance1.isSecuritiesAdjustment(), "Securities Value for Test Mgr 1 should NOT be adjusted from event.");

		// Create Mgr Event For Today for Manager 2 (Doesn't apply because adjustment is flagged as ignored)
		setupManagerEvent(TEST_MGR_2, false, true, getToday());

		// Balance 2 Should Not Have anything
		balance2 = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance2.getId());
		ValidationUtils.assertFalse(balance2.isCashAdjustment(), "Cash Value for Test Mgr 2 should NOT be adjusted from event.");
		ValidationUtils.assertFalse(balance2.isSecuritiesAdjustment(), "Securities Value for Test Mgr 2 should NOT be adjusted from event.");
	}


	@Test
	public void testUpdateEvent_CopyToManagerBalance() {
		// Create Balance For Yesterday
		InvestmentManagerAccountBalance balance1 = setupManagerBalance(TEST_MGR_1, getYesterday());

		// Create IGNORED Mgr Event For Today for Manager 1
		InvestmentEvent event1 = setupManagerEvent(TEST_MGR_1, false, true, getToday());

		// Pull Balance 1 from the Database Again -
		balance1 = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance1.getId());
		ValidationUtils.assertFalse(balance1.isCashAdjustment(), "Cash Value for Test Mgr 1 should NOT be adjusted from event.");
		ValidationUtils.assertFalse(balance1.isSecuritiesAdjustment(), "Securities Value for Test Mgr 1 should NOT be adjusted from event.");

		// Remove Ignored Flag
		event1.getManagerAdjustmentList().get(0).setIgnored(false);
		this.investmentCalendarService.saveInvestmentEvent(event1, true);

		// Pull Balance 1 from the Database Again -
		balance1 = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance1.getId());
		ValidationUtils.assertTrue(balance1.isCashAdjustment(), "Cash Value for Test Mgr 1 should be adjusted from event.");
		ValidationUtils.assertTrue(balance1.isSecuritiesAdjustment(), "Securities Value for Test Mgr 1 should be adjusted from event.");

		// Adjustments Should be the same
		InvestmentManagerAccountBalanceAdjustment mgrAdj = balance1.getAdjustmentList().get(0);
		InvestmentEventManagerAdjustment eventAdj = event1.getManagerAdjustmentList().get(0);

		ValidationUtils.assertEquals("InvestmentEventManagerAdjustment", mgrAdj.getSourceSystemTable().getName(), "Mgr Adjustment Type should be linked to event manager adj table");
		ValidationUtils.assertEquals(eventAdj.getId(), mgrAdj.getSourceFkFieldId(), "Mgr Adjustment Type should be linked to event manager adj by id");
		ValidationUtils.assertEquals(eventAdj.getAdjustmentType(), mgrAdj.getAdjustmentType(), "Mgr Adjustment Type should be the same as the one on the event");
		ValidationUtils.assertEquals(eventAdj.getCashValue(), mgrAdj.getCashValue(), "Cash should be the same as the one on the event");
		ValidationUtils.assertEquals(eventAdj.getSecuritiesValue(), mgrAdj.getSecuritiesValue(), "Securities should be the same as the one on the event");

		// Clear Cash Amount
		event1.getManagerAdjustmentList().get(0).setCashValue(BigDecimal.ZERO);
		this.investmentCalendarService.saveInvestmentEvent(event1, true);

		// Pull Balance 1 from the Database Again -
		balance1 = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance1.getId());
		ValidationUtils.assertFalse(balance1.isCashAdjustment(), "Cash Value for Test Mgr 1 should NOT be adjusted from event.");
		ValidationUtils.assertTrue(balance1.isSecuritiesAdjustment(), "Securities Value for Test Mgr 1 should be adjusted from event.");

		// Adjustments Should be the same
		mgrAdj = balance1.getAdjustmentList().get(0);
		eventAdj = event1.getManagerAdjustmentList().get(0);

		ValidationUtils.assertEquals("InvestmentEventManagerAdjustment", mgrAdj.getSourceSystemTable().getName(), "Mgr Adjustment Type should be linked to event manager adj table");
		ValidationUtils.assertEquals(eventAdj.getId(), mgrAdj.getSourceFkFieldId(), "Mgr Adjustment Type should be linked to event manager adj by id");
		ValidationUtils.assertEquals(eventAdj.getAdjustmentType(), mgrAdj.getAdjustmentType(), "Mgr Adjustment Type should be the same as the one on the event");
		ValidationUtils.assertEquals(eventAdj.getCashValue(), mgrAdj.getCashValue(), "Cash should be the same as the one on the event");
		ValidationUtils.assertEquals(eventAdj.getSecuritiesValue(), mgrAdj.getSecuritiesValue(), "Securities should be the same as the one on the event");
	}


	@Test
	public void testUpdateEventDate_DeleteFromManagerBalance() {
		// Create Balance For Yesterday
		InvestmentManagerAccountBalance balance1 = setupManagerBalance(TEST_MGR_1, getYesterday());

		// Create Mgr Event For Today for Manager 1
		InvestmentEvent event1 = setupManagerEvent(TEST_MGR_1, false, false, getToday());

		// Pull Balance 1 from the Database Again -
		balance1 = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance1.getId());
		ValidationUtils.assertTrue(balance1.isCashAdjustment(), "Cash Value for Test Mgr 1 should be adjusted from event.");
		ValidationUtils.assertTrue(balance1.isSecuritiesAdjustment(), "Securities Value for Test Mgr 1 should be adjusted from event.");

		// Change Event Date to Future
		event1.setEventDate(DateUtils.addDays(getToday(), 10));
		this.investmentCalendarService.saveInvestmentEvent(event1, true);

		// Pull Balance 1 from the Database Again - Adjustment should be deleted
		balance1 = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance1.getId());
		ValidationUtils.assertFalse(balance1.isCashAdjustment(), "Cash Value for Test Mgr 1 should NOT be adjusted from event.");
		ValidationUtils.assertFalse(balance1.isSecuritiesAdjustment(), "Securities Value for Test Mgr 1 should NOT be adjusted from event.");
	}


	@Test
	public void testDeleteEvent_DeleteFromManagerBalance() {
		// Create Balance For Yesterday
		InvestmentManagerAccountBalance balance1 = setupManagerBalance(TEST_MGR_1, getYesterday());

		// Create Mgr Event For Today for Manager 1
		InvestmentEvent event1 = setupManagerEvent(TEST_MGR_1, true, false, getToday());

		// Pull Balance 1 from the Database Again -
		balance1 = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance1.getId());
		ValidationUtils.assertTrue(balance1.isMocAdjustment(), "Moc Value for Test Mgr 1 should be adjusted from event.");

		// Delete Event
		this.investmentCalendarService.deleteInvestmentEvent(event1.getId(), true, true);

		// Pull Balance 1 from the Database Again - Adjustment should be deleted
		balance1 = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance1.getId());
		ValidationUtils.assertFalse(balance1.isMocAdjustment(), "Moc Value for Test Mgr 1 should NOT be adjusted from event.");
	}


	@Test
	public void testCreateEvent_CalendarHoliday() {

		Date eventDate = new Date();
		CalendarHolidayDay holiday = new CalendarHolidayDay();
		holiday.setCalendar(this.calendarSetupService.getDefaultCalendar());
		holiday.setDay(this.calendarSetupService.getCalendarDayByDate(eventDate));
		holiday.setTradeHoliday(true);
		holiday.setSettlementHoliday(true);
		holiday.setFullDayHoliday(true);
		this.calendarHolidayService.saveCalendarHolidayDay(holiday);

		TestUtils.expectException(UserIgnorableValidationException.class, () -> {
			InvestmentEvent event = new InvestmentEvent();
			event.setEventType(this.investmentCalendarService.getInvestmentEventType((short) 40));
			event.setEventDate(eventDate);

			this.investmentCalendarService.saveInvestmentEvent(event, false);
		}, "is a Holiday for Calendar");
	}


	@Test
	public void testCreateEvent_Duplicate() {
		TestUtils.expectException(UserIgnorableValidationException.class, () -> {
			Date date = new Date();

			InvestmentEvent event1 = new InvestmentEvent();
			event1.setEventType(this.investmentCalendarService.getInvestmentEventType((short) 30));
			event1.setInvestmentAccount(this.investmentAccountService.getInvestmentAccount(1));
			event1.setEventDate(date);

			this.investmentCalendarService.saveInvestmentEvent(event1, false);


			InvestmentEvent event2 = new InvestmentEvent();
			event2.setEventType(this.investmentCalendarService.getInvestmentEventType((short) 30));
			event2.setInvestmentAccount(this.investmentAccountService.getInvestmentAccount(1));
			event2.setEventDate(date);

			this.investmentCalendarService.saveInvestmentEvent(event2, false);
		}, "An event already exists for Date");
	}


	@Test
	public void testCreateEvent_withoutEventScope() {
		InvestmentEvent event = new InvestmentEvent();
		event.setEventDate(new Date());
		event.setEventType(this.investmentCalendarService.getInvestmentEventType((short) 50));
		// make sure it allows a save here as expected
		this.investmentCalendarService.saveInvestmentEvent(event, false);

		event.setInvestmentAccount(this.investmentAccountService.getInvestmentAccount(1));
		TestUtils.expectException(ValidationException.class, () -> this.investmentCalendarService.saveInvestmentEvent(event, false), "Investment account cannot be added to an event of type");
		event.setInvestmentAccount(null);

		event.setAssigneeSecurityGroup(this.securityUserService.getSecurityGroup((short) 100));
		TestUtils.expectException(ValidationException.class, () -> this.investmentCalendarService.saveInvestmentEvent(event, false), "Security group cannot be added to an event of type");
	}


	@Test
	public void testCreateEvent_investmentAccountAllowed() {
		InvestmentEvent event = new InvestmentEvent();
		event.setEventDate(new Date());
		event.setEventType(this.investmentCalendarService.getInvestmentEventType((short) 30));
		// make sure it allows a save here as expected
		this.investmentCalendarService.saveInvestmentEvent(event, false);

		event.setInvestmentAccount(this.investmentAccountService.getInvestmentAccount(1));
		this.investmentCalendarService.saveInvestmentEvent(event, false);

		event.setAssigneeSecurityGroup(this.securityUserService.getSecurityGroup((short) 100));
		TestUtils.expectException(ValidationException.class, () -> this.investmentCalendarService.saveInvestmentEvent(event, false), "Security group cannot be added to an event of type");
	}


	@Test
	public void testCreateEvent_investmentAccountRequired() {
		InvestmentEvent event = new InvestmentEvent();
		event.setEventDate(new Date());
		event.setEventType(this.investmentCalendarService.getInvestmentEventType((short) 60));
		TestUtils.expectException(ValidationException.class, () -> this.investmentCalendarService.saveInvestmentEvent(event, false), "Investment Account is required for an event of type");

		event.setInvestmentAccount(this.investmentAccountService.getInvestmentAccount(1));
		this.investmentCalendarService.saveInvestmentEvent(event, false);

		event.setAssigneeSecurityGroup(this.securityUserService.getSecurityGroup((short) 100));
		TestUtils.expectException(ValidationException.class, () -> this.investmentCalendarService.saveInvestmentEvent(event, false), "Security group cannot be added to an event of type");
	}


	@Test
	public void testCreateEvent_teamRequired() {
		InvestmentEvent event = new InvestmentEvent();
		event.setEventDate(new Date());
		event.setEventType(this.investmentCalendarService.getInvestmentEventType((short) 70));

		TestUtils.expectException(ValidationException.class, () -> this.investmentCalendarService.saveInvestmentEvent(event, false), "Team is required for an event of type");

		event.setInvestmentAccount(this.investmentAccountService.getInvestmentAccount(1));
		TestUtils.expectException(ValidationException.class, () -> this.investmentCalendarService.saveInvestmentEvent(event, false), "Team is required for an event of type");

		event.setInvestmentAccount(null);
		event.setAssigneeSecurityGroup(this.securityUserService.getSecurityGroup((short) 100));
		this.investmentCalendarService.saveInvestmentEvent(event, false);
	}


	@Test
	public void testCompleteEvent() {
		InvestmentEvent event = new InvestmentEvent();
		event.setEventDate(new Date());
		event.setCompletedEvent(true);

		event.setEventType(this.investmentCalendarService.getInvestmentEventType((short) 50));
		TestUtils.expectException(ValidationException.class, () -> this.investmentCalendarService.saveInvestmentEvent(event, false), "Event completion is not allowed for an event of type");

		// Make sure it saves for completion allowed event type
		event.setEventType(this.investmentCalendarService.getInvestmentEventType((short) 30));
		this.investmentCalendarService.saveInvestmentEvent(event, false);

		// Now make sure it doesn't allow a resolution
		event.setResolution(this.systemListService.getSystemListItem(1).getValue());
		TestUtils.expectException(ValidationException.class, () -> this.investmentCalendarService.saveInvestmentEvent(event, false), "cannot have a resolution");

		// Now make sure resolution is allowed for events of correct type
		event.setEventType(this.investmentCalendarService.getInvestmentEventType((short) 80));
		this.investmentCalendarService.saveInvestmentEvent(event, false);

		// Finally, make sure resolution is required for events of correc type
		event.setResolution(null);
		TestUtils.expectException(ValidationException.class, () -> this.investmentCalendarService.saveInvestmentEvent(event, false), "must have a resolution");
	}


	@Test
	public void testCreateEvent_amountAllowed() {
		InvestmentEvent event = new InvestmentEvent();
		event.setEventDate(new Date());
		event.setEventType(this.investmentCalendarService.getInvestmentEventType((short) 30));
		event.setAmount(new BigDecimal(10));

		// Make sure valid event saves as expected
		this.investmentCalendarService.saveInvestmentEvent(event, false);

		// Now make sure can't save if amount not allowed
		event.setEventType(this.investmentCalendarService.getInvestmentEventType((short) 50));
		event.setResolution(null);
		TestUtils.expectException(ValidationException.class, () -> this.investmentCalendarService.saveInvestmentEvent(event, false), "Event amount cannot be added to an event of type");
	}


	// Helper Methods
	private InvestmentManagerAccountBalance setupManagerBalance(int managerId, Date balanceDate) {
		InvestmentManagerAccountBalance bal = new InvestmentManagerAccountBalance();
		bal.setManagerAccount(this.investmentManagerAccountService.getInvestmentManagerAccount(managerId));
		bal.setBalanceDate(balanceDate);
		bal.setCashValue(BigDecimal.valueOf(10000 * managerId));
		bal.setSecuritiesValue(BigDecimal.valueOf(5000 * managerId));
		this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalance(bal);
		return bal;
	}


	private InvestmentEvent setupManagerEvent(int managerId, boolean moc, boolean ignored, Date eventDate) {
		InvestmentEvent event = new InvestmentEvent();
		event.setAmount(BigDecimal.valueOf(-50 * managerId));
		event.setEventType(this.investmentCalendarService.getInvestmentEventType(moc ? EVENT_MGR_MOC_TYPE : EVENT_MGR_TYPE));
		event.setEventDate(eventDate);

		InvestmentEventManagerAdjustment eventMgr = new InvestmentEventManagerAdjustment();
		eventMgr.setInvestmentEvent(event);
		eventMgr.setManagerAccount(this.investmentManagerAccountService.getInvestmentManagerAccount(managerId));
		eventMgr.setAdjustmentType(this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentType(moc ? MGR_MOC_ADJ_TYPE : MGR_CONT_DIST_ADJ_TYPE));
		eventMgr.setCashValue(BigDecimal.valueOf(-40 * managerId));
		eventMgr.setSecuritiesValue(BigDecimal.valueOf(-10 * managerId));
		eventMgr.setIgnored(ignored);

		event.setManagerAdjustmentList(CollectionUtils.createList(eventMgr));
		this.investmentCalendarService.saveInvestmentEvent(event, true);
		return event;
	}
	/////////////////////////////////////////////////////
	//  Apply Changes to Manager Balances Tests End
	/////////////////////////////////////////////////////


	@Test
	public void testSaveHistoricalEvent() {
		InvestmentEvent event = new InvestmentEvent();
		event.setAmount(BigDecimal.valueOf(5000));
		event.setEventType(this.investmentCalendarService.getInvestmentEventType(EVENT_REBALANCE_TYPE));
		event.setEventDate(DateUtils.toDate("02/11/2016"));

		boolean error = false;
		try {
			this.investmentCalendarService.saveInvestmentEvent(event, true);
		}
		catch (UserIgnorableValidationException e) {
			error = true;
			Assertions.assertEquals("investmentEventHistoricalSave.json", e.getIgnoreExceptionUrl());
		}
		catch (Exception e) {
			Assertions.fail("Historical event save should have thrown a UserIgnorableValidationException, but instead threw [" + e.getClass() + "]");
		}
		Assertions.assertEquals(true, error, "Historical event save should have thrown a UserIgnorableValidationException.");

		// Try again with the bypass
		this.investmentCalendarService.saveInvestmentEventHistorical(event);
	}
}
