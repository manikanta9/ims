package com.clifton.investment.shared;


import com.clifton.business.shared.Company;

import java.io.Serializable;


/**
 * Represents an investment account and contains common fields shared between {@link ClientAccount} and {@link HoldingAccount}.
 *
 * @author vgomelsky
 */
public abstract class BaseAccount implements Serializable {

	private int id;

	/**
	 * Unique issuer account number assigned to this account by the issuer.
	 */
	private String accountNumber;
	private String accountName;

	private String workflowState;

	private String baseCurrencySymbol;

	/**
	 * The default company (uses parent name if applies) to use for retrieving FX rates.
	 * Applies to both Client and Holding Accounts and can be used to override default behaviour.
	 * <p>
	 * Whenever possible, holding account FX Source is used: COALESCE(Holding Account FX Source, Holding Account Issuer, Default: Goldman Sachs)
	 * <p>
	 * When holding account is not available (manager balances, billing, etc.), client account FX Source is used: COALESCE(Client Account FX Source, Default: Goldman Sachs).
	 * <p>
	 * For client account FX Source, the largest use case of this is currently Portfolio Runs.
	 * Also, Manager Balances (When Manager is in different CCY then Account) and Billing (When Billing CCY is different than Account).
	 */
	private Company defaultExchangeRateSourceCompany;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getId() {
		return this.id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public String getAccountName() {
		return this.accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public String getWorkflowState() {
		return this.workflowState;
	}


	public void setWorkflowState(String workflowState) {
		this.workflowState = workflowState;
	}


	public String getBaseCurrencySymbol() {
		return this.baseCurrencySymbol;
	}


	public void setBaseCurrencySymbol(String baseCurrencySymbol) {
		this.baseCurrencySymbol = baseCurrencySymbol;
	}


	public Company getDefaultExchangeRateSourceCompany() {
		return this.defaultExchangeRateSourceCompany;
	}


	public void setDefaultExchangeRateSourceCompany(Company defaultExchangeRateSourceCompany) {
		this.defaultExchangeRateSourceCompany = defaultExchangeRateSourceCompany;
	}
}
