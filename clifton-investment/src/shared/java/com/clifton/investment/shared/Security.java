package com.clifton.investment.shared;

import java.io.Serializable;
import java.util.Date;


/**
 * Represents a specific investment Security: stock, bond, future, benchmark, etc.
 *
 * @author vgomelsky
 */
public class Security implements Serializable {

	private int id;
	private int instrumentId; // used for filtering by CompliancePositionExchangeLimitPositionServiceImpl

	private String symbol;
	private String cusip;
	private String isin;
	private String sedol;
	private String occSymbol;

	private String name;

	private String currencyDenominationSymbol;

	private String primaryExchangeCode;
	private String compositeExchangeCode;

	private String securityType;
	private String securityTypeSubType;
	private String securityTypeSubType2;

	private Date endDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isCurrency() {
		return getSymbol() != null && getSymbol().equals(getCurrencyDenominationSymbol());
	}


	public int getId() {
		return this.id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getInstrumentId() {
		return this.instrumentId;
	}


	public void setInstrumentId(int instrumentId) {
		this.instrumentId = instrumentId;
	}


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public String getIsin() {
		return this.isin;
	}


	public void setIsin(String isin) {
		this.isin = isin;
	}


	public String getSedol() {
		return this.sedol;
	}


	public void setSedol(String sedol) {
		this.sedol = sedol;
	}


	public String getOccSymbol() {
		return this.occSymbol;
	}


	public void setOccSymbol(String occSymbol) {
		this.occSymbol = occSymbol;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCurrencyDenominationSymbol() {
		return this.currencyDenominationSymbol;
	}


	public void setCurrencyDenominationSymbol(String currencyDenominationSymbol) {
		this.currencyDenominationSymbol = currencyDenominationSymbol;
	}


	public String getPrimaryExchangeCode() {
		return this.primaryExchangeCode;
	}


	public void setPrimaryExchangeCode(String primaryExchangeCode) {
		this.primaryExchangeCode = primaryExchangeCode;
	}


	public String getCompositeExchangeCode() {
		return this.compositeExchangeCode;
	}


	public void setCompositeExchangeCode(String compositeExchangeCode) {
		this.compositeExchangeCode = compositeExchangeCode;
	}


	public String getSecurityType() {
		return this.securityType;
	}


	public void setSecurityType(String securityType) {
		this.securityType = securityType;
	}


	public String getSecurityTypeSubType() {
		return this.securityTypeSubType;
	}


	public void setSecurityTypeSubType(String securityTypeSubType) {
		this.securityTypeSubType = securityTypeSubType;
	}


	public String getSecurityTypeSubType2() {
		return this.securityTypeSubType2;
	}


	public void setSecurityTypeSubType2(String securityTypeSubType2) {
		this.securityTypeSubType2 = securityTypeSubType2;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
