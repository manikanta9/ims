package com.clifton.investment.shared;


import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;


/**
 * The <code>InvestmentNotionalCalculatorTypes</code> enum defines standard rounding algorithms supported by the system.
 *
 * @author vgomelsky
 */
public enum InvestmentNotionalCalculatorTypes {

	MONEY_HALF_UP(2, RoundingMode.HALF_UP, false, false, false, false), //
	MONEY_HALF_UP_DIVIDE(2, RoundingMode.HALF_UP, false, false, true, false), //
	MONEY_UNIT_HALF_UP(2, RoundingMode.HALF_UP, true, false, false, false), //
	INTEGER_HALF_UP(0, RoundingMode.HALF_UP, false, false, false, false), //
	INTEGER_HALF_UP_DIVIDE(0, RoundingMode.HALF_UP, false, false, true, false), //
	INTEGER_UNIT_HALF_UP(0, RoundingMode.HALF_UP, true, false, false, false), //
	INTEGER_DOWN(0, RoundingMode.DOWN, false, false, false, false), //
	INTEGER_UNIT_DOWN(0, RoundingMode.DOWN, true, false, false, false), //
	BPS_DISCOUNT_MONEY_HALF_UP(2, RoundingMode.HALF_UP, false, true, false, false), //
	BPS_DISCOUNT_MONEY_HALF_UP_NEGATIVE_QTY(2, RoundingMode.HALF_UP, false, true, false, true), //
	BPS_DISCOUNT_INTEGER_HALF_UP(0, RoundingMode.HALF_UP, false, true, false, false), //
	BPS_DISCOUNT_INTEGER_HALF_UP_NEGATIVE_QTY(0, RoundingMode.HALF_UP, false, true, false, true), //
	SFE_10YEAR_6PERCENT(2, RoundingMode.HALF_UP, true, new BigDecimal(3), 20), //
	SFE_3YEAR_6PERCENT(2, RoundingMode.HALF_UP, true, new BigDecimal(3), 6), //
	IR_90DAY(2, RoundingMode.HALF_UP, false, 90, true);


	/**
	 * The scale used for the result of rounding: 2 decimal places, etc.
	 */
	private final int scale;
	private final RoundingMode roundingMode;
	/**
	 * If perUnit rounding is used then set the scale after (price * multiplier) but before quantity multiplication.
	 */
	private final boolean perUnit;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	/**
	 * If set, quantity used in calculation will always be negative: negate positive quantity.
	 */
	private boolean negativeQuantity;
	/**
	 * If discount convention is used then the contract value is calculated as (1 - Price * Multiplier) as opposed to Price * Multiplier.
	 */
	private boolean discountConvention;
	/**
	 * If divideByPrice = true, then instead of multiplying quantity by price, it will be divided by price (currency forward convention for some currencies).
	 */
	private boolean divideByPrice;
	/**
	 * The following 2 fields are used by SFE bond futures: 10 year and 3 year.
	 * See below for details about "unusual" calculation.
	 * Coupon Rate is per payment period (not annual).  For example 10 year SFE bond that pays 6% semi-annually will have 3 for coupon rate.
	 */
	private BigDecimal couponRate;
	private int numberOfPeriods;


	/**
	 * Pricing for IR bank bills
	 */
	private boolean irPricing;


	InvestmentNotionalCalculatorTypes(int scale, RoundingMode roundingMode, boolean perUnit, boolean discountConvention, boolean divideByPrice, boolean negativeQuantity) {
		this.scale = scale;
		this.roundingMode = roundingMode;
		this.negativeQuantity = negativeQuantity;
		this.perUnit = perUnit;
		this.discountConvention = discountConvention;
		this.divideByPrice = divideByPrice;
		if (discountConvention && divideByPrice) {
			throw new IllegalArgumentException("discountConvention and divideByPrice options cannot be combined");
		}
	}


	InvestmentNotionalCalculatorTypes(int scale, RoundingMode roundingMode, boolean perUnit, BigDecimal couponRate, int numberOfPeriods) {
		this.scale = scale;
		this.roundingMode = roundingMode;
		this.perUnit = perUnit;
		this.couponRate = couponRate;
		this.numberOfPeriods = numberOfPeriods;
	}


	InvestmentNotionalCalculatorTypes(int scale, RoundingMode roundingMode, boolean perUnit, int numberOfPeriods, boolean irPricing) {
		this.scale = scale;
		this.roundingMode = roundingMode;
		this.perUnit = perUnit;
		this.numberOfPeriods = numberOfPeriods;
		this.irPricing = irPricing;
	}


	/**
	 * Returns the default InvestmentNotionalCalculatorTypes: MONEY_HALF_UP.
	 */
	public static InvestmentNotionalCalculatorTypes getDefaultCalculatorType() {
		return MONEY_HALF_UP;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getScale() {
		return this.scale;
	}


	public RoundingMode getRoundingMode() {
		return this.roundingMode;
	}


	public boolean isPerUnit() {
		return this.perUnit;
	}


	public BigDecimal getCouponRate() {
		return this.couponRate;
	}


	public int getNumberOfPeriods() {
		return this.numberOfPeriods;
	}


	public boolean isDiscountConvention() {
		return this.discountConvention;
	}


	public boolean isDivideByPrice() {
		return this.divideByPrice;
	}


	public boolean isIrPricing() {
		return this.irPricing;
	}


	/**
	 * Calculates and returns notional for this calculator type using the specified parameters.
	 */
	public BigDecimal calculateNotional(BigDecimal price, BigDecimal priceMultiplier, BigDecimal quantity) {
		return round(calculateNotionalWithoutRounding(price, priceMultiplier, quantity));
	}


	/***
	 * Calculates and returns notional for this calculator type using the specified parameters without rounding.
	 */
	public BigDecimal calculateNotionalWithoutRounding(BigDecimal price, BigDecimal priceMultiplier, BigDecimal quantity) {
		BigDecimal result = calculateContractValue(price, priceMultiplier);

		result = result.multiply(quantity);
		if (isNegativeQuantity() && quantity.compareTo(BigDecimal.ZERO) > 0) {
			result = result.negate();
		}
		return result;
	}


	public BigDecimal calculateQuantityFromNotional(BigDecimal price, BigDecimal priceMultiplier, BigDecimal notional, Short quantityScale) {
		if (isNegativeQuantity()) {
			throw new IllegalStateException("Cannot calculate the sign of quantity for this calculator: " + this.name());
		}
		BigDecimal result = calculateContractValue(price, priceMultiplier);

		result = MathUtils.divide(notional, result);
		if (quantityScale != null) {
			return result.setScale(quantityScale.intValue(), getRoundingMode());
		}
		return result;
	}


	public BigDecimal calculateContractValue(BigDecimal price, BigDecimal priceMultiplier) {
		BigDecimal normalPrice = price;
		// SFE (Sydney Futures Exchange) securities have "unusual" calculations for 10 and 3 year Australian bond futures based on 6% bonds that pay interest semi-annually
		if (getCouponRate() != null) {
			// contractValue = multiplier*(c*(1-pow(v, n))/i + 100*pow(v, n))
			// i = (100 - price) / 200
			// v = 1 / (1 + i)
			// c = 3 (coupon rate: 6% paid semi-annually)
			// n = 20 (number of payments: 10 year bond paid semi-annually)
			// NOTE: all intermediate results must be rounded to 8 decimal places
			int n = getNumberOfPeriods();
			BigDecimal c = getCouponRate();
			BigDecimal i = MathUtils.divide(new BigDecimal(100).subtract(price), new BigDecimal(200));
			BigDecimal v = MathUtils.divide(BigDecimal.ONE, BigDecimal.ONE.add(i)).setScale(8, getRoundingMode());
			normalPrice = MathUtils.divide(c.multiply(BigDecimal.ONE.subtract(v.pow(n).setScale(8, getRoundingMode()))), i);
			normalPrice = normalPrice.add(new BigDecimal(100).multiply(v.pow(n).setScale(8, getRoundingMode()))).setScale(8, getRoundingMode());
		}

		BigDecimal result;
		if (isIrPricing()) {
			BigDecimal hundred = new BigDecimal(100);
			BigDecimal yield = hundred.subtract(normalPrice);
			BigDecimal faceValue = hundred.multiply(priceMultiplier);
			BigDecimal topVal = faceValue.multiply(new BigDecimal(365));
			BigDecimal bottomVal = MathUtils.divide(yield.multiply(new BigDecimal(getNumberOfPeriods())), 100).add(new BigDecimal(365));
			result = MathUtils.divide(topVal, bottomVal);
		}
		else {
			result = normalPrice.multiply(priceMultiplier);
		}

		if (isDiscountConvention()) {
			result = MathUtils.subtract(BigDecimal.ONE, result);
		}
		else if (isDivideByPrice()) {
			result = MathUtils.divide(BigDecimal.ONE, result);
		}

		if (isPerUnit()) {
			result = round(result);
		}
		return result;
	}


	/**
	 * Backs into the price from notional and quantity.
	 */
	public BigDecimal calculatePriceFromNotional(BigDecimal quantity, BigDecimal notional, BigDecimal priceMultiplier) {
		BigDecimal result = calculateContractValue(quantity, priceMultiplier);
		if (isDivideByPrice()) {
			result = MathUtils.divide(MathUtils.divide(BigDecimal.ONE, result), notional);
		}
		else {
			result = MathUtils.divide(notional, result);
		}
		result = MathUtils.round(result.stripTrailingZeros(), DataTypes.PRICE.getPrecision()).stripTrailingZeros();

		return MathUtils.roundToSmallestPrecision(result, roundedPrice -> MathUtils.isEqual(notional, calculateNotional(roundedPrice, priceMultiplier, quantity)));
	}


	public BigDecimal calculateDirtyPrice(BigDecimal cleanPrice, BigDecimal quantity, BigDecimal notional, BigDecimal accruedInterest, BigDecimal priceMultiplier, BigDecimal notionalMultiplier) {
		// dirty price calculation includes accrued interest so needs coupon: can use any sizable face
		// however, it does not include RECEIVABLES
		// Default Calculation (Not Discount Convention): Dirty Price = Notional Multiplier * Clean Price * (1 + Accrued Interest / Notional (If not calculated to 0 else quantity)
		if (!isDiscountConvention()) {
			BigDecimal factor = BigDecimal.ONE;
			if (BigDecimal.ZERO.compareTo(accruedInterest) != 0) {
				if (MathUtils.isNullOrZero(notional)) {
					// snapshot price results in zero notional (likely price is 0 or 100 for CDX)
					notional = quantity;
				}
				factor = BigDecimal.ONE.add(MathUtils.divide(accruedInterest, notional));
			}
			cleanPrice = MathUtils.multiply(cleanPrice, notionalMultiplier);
			return MathUtils.multiply(cleanPrice, factor);
		}
		// Discount Convention Calculation
		// Accrual Adjustment = Accrual Amount / ABS(Quantity) / Price Multiplier / Notional Multiplier
		BigDecimal accrualAdjustment = MathUtils.divide(MathUtils.divide(MathUtils.divide(accruedInterest, MathUtils.abs(quantity)), priceMultiplier), notionalMultiplier);
		// Notional Negative Quantity Flag (IRS)
		if (isNegativeQuantity()) {
			// if QTY is Negative, Dirty Price = 200 - (Clean Price + Accrual Adjustment)
			if (MathUtils.isLessThan(quantity, BigDecimal.ZERO)) {
				return MathUtils.subtract(BigDecimal.valueOf(200), MathUtils.add(cleanPrice, accrualAdjustment));
			}
			// if QTY is Positive, Dirty Price = Clean Price + Accrual Adjustment
			return MathUtils.add(cleanPrice, accrualAdjustment);
		}
		// NOT Notional Negative Quantity Flag (CDS)
		// If QTY is Negative, Dirty Price = Clean Price + Accrual Adjustment
		if (MathUtils.isLessThan(quantity, BigDecimal.ZERO)) {
			return MathUtils.add(cleanPrice, accrualAdjustment);
		}
		// If QTY is Positive, Dirty Price = (200-Clean Price) + Accrual Adjustment
		return MathUtils.add(MathUtils.subtract(BigDecimal.valueOf(200), cleanPrice), accrualAdjustment);
	}


	/**
	 * Rounds the specified notional using calculator's rounding specification.
	 * For example, Japanese Yen may round down to 0 decimal places.
	 */
	public BigDecimal round(BigDecimal notional) {
		return notional.setScale(getScale(), getRoundingMode());
	}


	public boolean isNegativeQuantity() {
		return this.negativeQuantity;
	}
}

