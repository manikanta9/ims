package com.clifton.investment.shared;

import com.clifton.business.shared.Company;


/**
 * Represents Investment Accounts where assets are actually held.  These are usually broker or custodian accounts. All reconciliation is performed for holding accounts.
 * These accounts are issued by corresponding custodians/brokers that assign them their unique account numbers.
 * Each transaction in the system must always be associated with both client and holding accounts.
 * Both Client and Holding Investment Accounts are stored in InvestmentAccount table.
 * One can tell the difference by filtering on "IsOurAccount" field on corresponding InvestmentAccountType.
 *
 * @author vgomelsky
 */
public class HoldingAccount extends BaseAccount {

	private String accountType;

	/**
	 * Optional alternate account number. Some account types may have 2 different account numbers.
	 * For example, Custodian accounts will have "Custodian Number" and "Accounting Number" (number2).
	 */
	private String accountNumber2;

	/**
	 * The company that issued this account. Examples: specific bank, broker, etc.
	 */
	private Company issuer;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return getAccountNumber() + ": " + getAccountName() + " (" + getAccountType() + ")";
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getAccountType() {
		return this.accountType;
	}


	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}


	public String getAccountNumber2() {
		return this.accountNumber2;
	}


	public void setAccountNumber2(String accountNumber2) {
		this.accountNumber2 = accountNumber2;
	}


	public Company getIssuer() {
		return this.issuer;
	}


	public void setIssuer(Company issuer) {
		this.issuer = issuer;
	}
}
