package com.clifton.investment.shared;

/**
 * Represents internal account (usually 6 digit account number) that we assign to each client account.
 * Client Investment Accounts are of "Client" account type which has "Our Account" field set to true.  Client Accounts are used to segregate client assets.
 * There is usually a separate client account created for each product or service that the client uses.
 * We can also create a separate client account for each asset class in client's portfolio and roll them up into a top level client account.
 * Client accounts are used in performance reporting and specify the type of the service and corresponding portfolio management features that should be
 * enabled by the system for accounts of that type.  We can map a single client account to multiple holding accounts or vice verse.
 * Both Client and Holding Investment Accounts are stored in InvestmentAccount table.
 * One can tell the difference by filtering on the IsOurAccount field on corresponding InvestmentAccountType.
 *
 * @author vgomelsky
 */
public class ClientAccount extends BaseAccount {

	/**
	 * Optional short name for accounts with long names. Used in label when set.
	 */
	private String shortName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		if (getShortName() != null) {
			return getAccountName() + ": " + getShortName();
		}
		return getAccountNumber() + ": " + getAccountName();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getShortName() {
		return this.shortName;
	}


	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
}
