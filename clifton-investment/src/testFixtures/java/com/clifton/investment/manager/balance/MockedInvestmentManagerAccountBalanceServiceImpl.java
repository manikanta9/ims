package com.clifton.investment.manager.balance;


import com.clifton.core.beans.BeanUtils;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceAdjustmentSearchForm;

import java.util.List;


public class MockedInvestmentManagerAccountBalanceServiceImpl extends InvestmentManagerAccountBalanceServiceImpl {

	@Override
	public List<InvestmentManagerAccountBalanceAdjustment> getInvestmentManagerAccountBalanceAdjustmentList(InvestmentManagerAccountBalanceAdjustmentSearchForm searchForm) {
		// Not much for testing and these filters aren't working with XML criteria
		List<InvestmentManagerAccountBalanceAdjustment> list = getInvestmentManagerAccountBalanceAdjustmentDAO().findAll();
		if (searchForm.getBalanceDate() != null) {
			list = BeanUtils.filter(list, balanceAdjustment -> balanceAdjustment.getManagerAccountBalance().getBalanceDate(), searchForm.getBalanceDate());
		}
		if (searchForm.getNextDayAdjustmentExists() != null) {
			list = BeanUtils.filter(list, InvestmentManagerAccountBalanceAdjustment::isNextDayAdjustmentApplied, searchForm.getNextDayAdjustmentExists());
		}
		return list;
	}
}
