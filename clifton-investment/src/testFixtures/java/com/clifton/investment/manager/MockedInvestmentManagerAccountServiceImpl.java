package com.clifton.investment.manager;


import com.clifton.investment.manager.search.InvestmentManagerAccountRollupSearchForm;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>MockedInvestmentManagerAccountServiceImpl</code>
 * <p>
 * Need to override/mock to handle dao look ups that use Logical expressions
 *
 * @author manderson
 */
public class MockedInvestmentManagerAccountServiceImpl extends InvestmentManagerAccountServiceImpl {

	@Override
	public List<InvestmentManagerAccountRollup> getInvestmentManagerAccountRollupList(InvestmentManagerAccountRollupSearchForm searchForm) {
		if (searchForm.getParentOrChildManagerAccountId() != null) {
			List<InvestmentManagerAccountRollup> parentList = getInvestmentManagerAccountRollupDAO().findByField("referenceOne.id", searchForm.getParentOrChildManagerAccountId());
			List<InvestmentManagerAccountRollup> childList = getInvestmentManagerAccountRollupDAO().findByField("referenceTwo.id", searchForm.getParentOrChildManagerAccountId());
			if (parentList == null) {
				parentList = new ArrayList<>();
			}
			if (childList != null) {
				parentList.addAll(childList);
			}
			return parentList;
		}
		return super.getInvestmentManagerAccountRollupList(searchForm);
	}
}
