package com.clifton.investment.account;

public class InvestmentAccountTypeBuilder {

	private InvestmentAccountType investmentAccountType;


	private InvestmentAccountTypeBuilder(InvestmentAccountType investmentAccountType) {
		this.investmentAccountType = investmentAccountType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentAccountTypeBuilder createBroker() {
		return new InvestmentAccountTypeBuilder(new InvestmentAccountType())
				.withId((short) 4)
				.withName("Broker");
	}


	public static InvestmentAccountTypeBuilder createCustodian() {
		InvestmentAccountType investmentAccountType = new InvestmentAccountType();

		investmentAccountType.setId((short) 6);
		investmentAccountType.setName("Custodian");
		investmentAccountType.setDescription("Provider of client's custodial services.");
		investmentAccountType.setAccountNumberLabel("Custodian Number");
		investmentAccountType.setAccountNumber2Label("Accounting Number");
		investmentAccountType.setAliasAllowed(true);

		return new InvestmentAccountTypeBuilder(investmentAccountType);
	}


	public static InvestmentAccountTypeBuilder createClient() {
		InvestmentAccountType investmentAccountType = new InvestmentAccountType();

		investmentAccountType.setId((short) 1);
		investmentAccountType.setName("Client");
		investmentAccountType.setDescription("Account assigned within Clifton's proprietary system.");
		investmentAccountType.setOurAccount(true);

		return new InvestmentAccountTypeBuilder(investmentAccountType);
	}


	public static InvestmentAccountTypeBuilder createFuturesBroker() {
		InvestmentAccountType investmentAccountType = new InvestmentAccountType();

		investmentAccountType.setId((short) 2);
		investmentAccountType.setName("Futures Broker");
		investmentAccountType.setDescription("Broker/dealer account: facilitates futures trading, positions, and collateral.");
		investmentAccountType.setAliasAllowed(true);

		return new InvestmentAccountTypeBuilder(investmentAccountType);
	}


	public static InvestmentAccountTypeBuilder createOtcCls() {
		InvestmentAccountType investmentAccountType = new InvestmentAccountType();

		investmentAccountType.setId((short) 25);
		investmentAccountType.setName("OTC CLS");
		investmentAccountType.setDescription("An account for use with all currency forward CLS trades. Holding accounts used in currency forward CLS trades must be of this type.");
		investmentAccountType.setContractRequired(true);
		investmentAccountType.setOtc(true);


		return new InvestmentAccountTypeBuilder(investmentAccountType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountTypeBuilder withId(short id) {
		getInvestmentAccountType().setId(id);
		return this;
	}


	public InvestmentAccountTypeBuilder withName(String name) {
		getInvestmentAccountType().setName(name);
		return this;
	}


	public InvestmentAccountTypeBuilder withDescription(String description) {
		getInvestmentAccountType().setDescription(description);
		return this;
	}


	public InvestmentAccountTypeBuilder withAccountNumberLabel(String accountNumberLabel) {
		getInvestmentAccountType().setAccountNumberLabel(accountNumberLabel);
		return this;
	}


	public InvestmentAccountTypeBuilder withAccountNumber2Label(String accountNumber2Label) {
		getInvestmentAccountType().setAccountNumber2Label(accountNumber2Label);
		return this;
	}


	public InvestmentAccountTypeBuilder withAliasAllowed(boolean aliasAllowed) {
		getInvestmentAccountType().setAliasAllowed(aliasAllowed);
		return this;
	}


	public InvestmentAccountType toInvestmentAccountType() {
		return this.investmentAccountType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentAccountType getInvestmentAccountType() {
		return this.investmentAccountType;
	}
}
