package com.clifton.investment.account.relationship;

import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeBuilder;


public class InvestmentAccountRelationshipPurposeBuilder {

	private InvestmentAccountRelationshipPurpose investmentAccountRelationshipPurpose;


	private InvestmentAccountRelationshipPurposeBuilder(InvestmentAccountRelationshipPurpose investmentAccountRelationshipPurpose) {
		this.investmentAccountRelationshipPurpose = investmentAccountRelationshipPurpose;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentAccountRelationshipPurposeBuilder forTradingInvestmentType(String investmentType) {
		return new InvestmentAccountRelationshipPurposeBuilder(new InvestmentAccountRelationshipPurpose())
				.withId((short) 1)
				.withName("Trading: " + investmentType)
				.withInvestmentType(InvestmentTypeBuilder.forInvestmentType(investmentType).toInvestmentType());
	}


	public static InvestmentAccountRelationshipPurposeBuilder createExecutingFunds() {
		InvestmentAccountRelationshipPurpose investmentAccountRelationshipPurpose = new InvestmentAccountRelationshipPurpose();

		investmentAccountRelationshipPurpose.setId((short) 21);
		investmentAccountRelationshipPurpose.setName("Executing: Funds");
		investmentAccountRelationshipPurpose.setDescription("Account that is used to execute trades for Funds: ETF, CEF, etc.");
		investmentAccountRelationshipPurpose.setInvestmentType(InvestmentTypeBuilder.createFunds().toInvestmentType());

		return new InvestmentAccountRelationshipPurposeBuilder(investmentAccountRelationshipPurpose);
	}


	public static InvestmentAccountRelationshipPurposeBuilder createTradingFunds() {
		InvestmentAccountRelationshipPurpose investmentAccountRelationshipPurpose = new InvestmentAccountRelationshipPurpose();

		investmentAccountRelationshipPurpose.setId((short) 17);
		investmentAccountRelationshipPurpose.setName("Trading: Funds");
		investmentAccountRelationshipPurpose.setDescription("Account that is used to trade Funds");
		investmentAccountRelationshipPurpose.setInvestmentType(InvestmentTypeBuilder.createFunds().toInvestmentType());

		return new InvestmentAccountRelationshipPurposeBuilder(investmentAccountRelationshipPurpose);
	}


	public static InvestmentAccountRelationshipPurposeBuilder createTradingStocks() {
		InvestmentAccountRelationshipPurpose investmentAccountRelationshipPurpose = new InvestmentAccountRelationshipPurpose();

		investmentAccountRelationshipPurpose.setId((short) 9);
		investmentAccountRelationshipPurpose.setName("Trading: Stocks");
		investmentAccountRelationshipPurpose.setDescription("Account that is used to trade stocks");
		investmentAccountRelationshipPurpose.setInvestmentType(InvestmentTypeBuilder.createStocks().toInvestmentType());
		investmentAccountRelationshipPurpose.setTradingExecutingPurpose(InvestmentAccountRelationshipPurposeBuilder.createExecutingStocks().toInvestmentAccountRelationshipPurpose());

		return new InvestmentAccountRelationshipPurposeBuilder(investmentAccountRelationshipPurpose);
	}


	public static InvestmentAccountRelationshipPurposeBuilder createTradingFutures() {
		InvestmentAccountRelationshipPurpose investmentAccountRelationshipPurpose = new InvestmentAccountRelationshipPurpose();

		investmentAccountRelationshipPurpose.setId((short) 7);
		investmentAccountRelationshipPurpose.setName("Trading: Futures");
		investmentAccountRelationshipPurpose.setDescription("Account that is used to trade futures. There can only be one futures broker.");
		investmentAccountRelationshipPurpose.setInvestmentType(InvestmentTypeBuilder.createFutures().toInvestmentType());

		return new InvestmentAccountRelationshipPurposeBuilder(investmentAccountRelationshipPurpose);
	}


	public static InvestmentAccountRelationshipPurposeBuilder createTradingSwaps() {
		InvestmentAccountRelationshipPurpose investmentAccountRelationshipPurpose = new InvestmentAccountRelationshipPurpose();

		investmentAccountRelationshipPurpose.setId((short) 10);
		investmentAccountRelationshipPurpose.setName("Trading: Swaps");
		investmentAccountRelationshipPurpose.setDescription("Account that is used to trade swaps");
		investmentAccountRelationshipPurpose.setInvestmentType(InvestmentTypeBuilder.createSwaps().toInvestmentType());

		return new InvestmentAccountRelationshipPurposeBuilder(investmentAccountRelationshipPurpose);
	}


	public static InvestmentAccountRelationshipPurposeBuilder createExecutingStocks() {
		InvestmentAccountRelationshipPurpose investmentAccountRelationshipPurpose = new InvestmentAccountRelationshipPurpose();

		investmentAccountRelationshipPurpose.setId((short) 14);
		investmentAccountRelationshipPurpose.setName("Executing: Stocks");
		investmentAccountRelationshipPurpose.setDescription("Account that is used to execute stock trades");
		investmentAccountRelationshipPurpose.setInvestmentType(InvestmentTypeBuilder.createStocks().toInvestmentType());

		return new InvestmentAccountRelationshipPurposeBuilder(investmentAccountRelationshipPurpose);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipPurposeBuilder withId(short id) {
		getInvestmentAccountRelationshipPurpose().setId(id);
		return this;
	}


	public InvestmentAccountRelationshipPurposeBuilder withName(String name) {
		getInvestmentAccountRelationshipPurpose().setName(name);
		return this;
	}


	public InvestmentAccountRelationshipPurposeBuilder withDescription(String description) {
		getInvestmentAccountRelationshipPurpose().setDescription(description);
		return this;
	}


	public InvestmentAccountRelationshipPurposeBuilder withInvestmentType(InvestmentType investmentType) {
		getInvestmentAccountRelationshipPurpose().setInvestmentType(investmentType);
		return this;
	}


	public InvestmentAccountRelationshipPurpose toInvestmentAccountRelationshipPurpose() {
		return this.investmentAccountRelationshipPurpose;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentAccountRelationshipPurpose getInvestmentAccountRelationshipPurpose() {
		return this.investmentAccountRelationshipPurpose;
	}
}
