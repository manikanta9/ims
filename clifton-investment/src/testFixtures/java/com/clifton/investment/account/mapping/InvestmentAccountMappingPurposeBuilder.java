package com.clifton.investment.account.mapping;

public class InvestmentAccountMappingPurposeBuilder {

	private InvestmentAccountMappingPurpose investmentAccountMappingPurpose;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentAccountMappingPurposeBuilder(InvestmentAccountMappingPurpose investmentAccountMappingPurpose) {
		this.investmentAccountMappingPurpose = investmentAccountMappingPurpose;
	}


	public static InvestmentAccountMappingPurposeBuilder createEMSXTradingAccountNumberBuilder() {
		InvestmentAccountMappingPurpose investmentAccountMappingPurpose = new InvestmentAccountMappingPurpose();

		investmentAccountMappingPurpose.setId((short) 1);
		investmentAccountMappingPurpose.setName("EMSX Trading Account Number");
		investmentAccountMappingPurpose.setDescription("Maps an alphanumeric value to FIX Tag 1 (Account) for a given account number. Used in Bloomberg EMSX Electronic Trading to provide an account number that is recognized by the broker.");
		investmentAccountMappingPurpose.setFieldName("Account Number");

		return new InvestmentAccountMappingPurposeBuilder(investmentAccountMappingPurpose);
	}


	public static InvestmentAccountMappingPurposeBuilder createBloombergFITAccountBuilder() {
		InvestmentAccountMappingPurpose investmentAccountMappingPurpose = new InvestmentAccountMappingPurpose();

		investmentAccountMappingPurpose.setId((short) 4);
		investmentAccountMappingPurpose.setName("Bloomberg FIT Account Mapping");
		investmentAccountMappingPurpose.setDescription("Account name mappings for Bloomberg FIT FIX connection.  Currently used for CDS Trading on the SEF.");
		investmentAccountMappingPurpose.setFieldName("Account Number");
		investmentAccountMappingPurpose.setOneAccountValueMappingPerPurpose(true);

		return new InvestmentAccountMappingPurposeBuilder(investmentAccountMappingPurpose);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountMappingPurposeBuilder withId(short id) {
		getInvestmentAccountMappingPurpose().setId(id);
		return this;
	}


	public InvestmentAccountMappingPurposeBuilder withName(String name) {
		getInvestmentAccountMappingPurpose().setName(name);
		return this;
	}


	public InvestmentAccountMappingPurposeBuilder withDescription(String description) {
		getInvestmentAccountMappingPurpose().setDescription(description);
		return this;
	}


	public InvestmentAccountMappingPurpose toInvestmentAccountMappingPurpose() {
		return this.investmentAccountMappingPurpose;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentAccountMappingPurpose getInvestmentAccountMappingPurpose() {
		return this.investmentAccountMappingPurpose;
	}
}
