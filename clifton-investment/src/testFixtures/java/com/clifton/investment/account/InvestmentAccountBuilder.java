package com.clifton.investment.account;

import com.clifton.business.client.BusinessClientBuilder;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.business.contract.BusinessContractBuilder;
import com.clifton.business.service.BusinessServiceBuilder;
import com.clifton.business.service.BusinessServiceProcessingTypeBuilder;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.system.list.SystemListItemBuilder;
import com.clifton.workflow.definition.WorkflowStateBuilder;
import com.clifton.workflow.definition.WorkflowStatusBuilder;


public class InvestmentAccountBuilder {

	private InvestmentAccount investmentAccount;


	private InvestmentAccountBuilder(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentAccountBuilder createEmptyInvestmentAccount() {
		return new InvestmentAccountBuilder(new InvestmentAccount());
	}


	public static InvestmentAccountBuilder createTestAccount1() {
		InvestmentAccount investmentAccount = new InvestmentAccount();

		investmentAccount.setId(4073);
		investmentAccount.setBusinessClient(BusinessClientBuilder.createTestingClient1().toBusinessClient());
		investmentAccount.setType(InvestmentAccountTypeBuilder.createCustodian().toInvestmentAccountType());
		investmentAccount.setIssuingCompany(BusinessCompanyBuilder.createCustodian1().toBusinessCompany());
		investmentAccount.setName("Testing Account");
		investmentAccount.setNumber("88-12354");
		investmentAccount.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		investmentAccount.setWorkflowState(WorkflowStateBuilder.createActiveAccount().toWorkflowState());
		investmentAccount.setWorkflowStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		investmentAccount.setWorkflowStateEffectiveStartDate(DateUtils.toDate("04/30/2015"));

		return new InvestmentAccountBuilder(investmentAccount);
	}


	public static InvestmentAccountBuilder createTestAccount2() {
		InvestmentAccount investmentAccount = new InvestmentAccount();

		investmentAccount.setId(296);
		investmentAccount.setBusinessClient(BusinessClientBuilder.createTestingClient2().toBusinessClient());
		investmentAccount.setType(InvestmentAccountTypeBuilder.createClient().toInvestmentAccountType());
		investmentAccount.setIssuingCompany(BusinessCompanyBuilder.createParametric().toBusinessCompany());
		investmentAccount.setBusinessService(BusinessServiceBuilder.createOverlay().toBusinessService());
		investmentAccount.setServiceProcessingType(BusinessServiceProcessingTypeBuilder.createOverlay().toBusinessServiceProcessingType());

		investmentAccount.setName("Testing Account 2");
		investmentAccount.setNumber("88-434343");
		investmentAccount.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		investmentAccount.setWorkflowState(WorkflowStateBuilder.createActiveClient().toWorkflowState());
		investmentAccount.setWorkflowStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		investmentAccount.setWorkflowStateEffectiveStartDate(DateUtils.toDate("04/04/2005"));
		investmentAccount.setBusinessContract(BusinessContractBuilder.createTestContract1().toBusinessContract());
		investmentAccount.setInceptionDate(DateUtils.toDate("04/04/2005"));
		investmentAccount.setPerformanceInceptionDate(DateUtils.toDate("04/04/2005"));
		investmentAccount.setDiscretionType("Discretion");
		investmentAccount.setExternalProductCode(SystemListItemBuilder.createParametric().toSystemListItem());

		return new InvestmentAccountBuilder(investmentAccount);
	}


	public static InvestmentAccountBuilder createTestHoldingAccount1() {
		InvestmentAccount investmentAccount = new InvestmentAccount();

		investmentAccount.setId(4958);
		investmentAccount.setBusinessClient(BusinessClientBuilder.createTestingClient2().toBusinessClient());
		investmentAccount.setType(InvestmentAccountTypeBuilder.createFuturesBroker().toInvestmentAccountType());
		investmentAccount.setIssuingCompany(BusinessCompanyBuilder.createMorganStanley().toBusinessCompany());
		investmentAccount.setName("Testing Holding Account 1");
		investmentAccount.setNumber("88-34343");
		investmentAccount.setBaseCurrency(InvestmentSecurityBuilder.newUSD());

		investmentAccount.setWorkflowState(WorkflowStateBuilder.createActiveAccount().toWorkflowState());
		investmentAccount.setWorkflowStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		investmentAccount.setWorkflowStateEffectiveStartDate(DateUtils.toDate("04/04/2005"));

		return new InvestmentAccountBuilder(investmentAccount);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountBuilder withId(int id) {
		getInvestmentAccount().setId(id);
		return this;
	}


	public InvestmentAccountBuilder withName(String name) {
		getInvestmentAccount().setName(name);
		return this;
	}


	public InvestmentAccountBuilder withDescription(String description) {
		getInvestmentAccount().setDescription(description);
		return this;
	}


	public InvestmentAccountBuilder withNumber(String number) {
		getInvestmentAccount().setNumber(number);
		return this;
	}


	public InvestmentAccountBuilder withType(InvestmentAccountType type) {
		getInvestmentAccount().setType(type);
		return this;
	}


	public InvestmentAccountBuilder withIssuingCompany(BusinessCompany issuingCompany) {
		getInvestmentAccount().setIssuingCompany(issuingCompany);
		return this;
	}


	public InvestmentAccountBuilder withBaseCurrency(InvestmentSecurity baseCurrency) {
		getInvestmentAccount().setBaseCurrency(baseCurrency);
		return this;
	}


	public InvestmentAccount toInvestmentAccount() {
		return this.investmentAccount;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}
}
