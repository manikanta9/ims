package com.clifton.investment.rates;

import com.clifton.calendar.setup.CalendarBuilder;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;

import java.time.LocalDate;


public class InvestmentInterestRateIndexBuilder {

	private InvestmentInterestRateIndex investmentInterestRateIndex;


	private InvestmentInterestRateIndexBuilder(InvestmentInterestRateIndex investmentInterestRateIndex) {
		this.investmentInterestRateIndex = investmentInterestRateIndex;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentInterestRateIndexBuilder create_ICE_LIBOR_USD_Overnight() {
		InvestmentInterestRateIndex investmentInterestRateIndex = new InvestmentInterestRateIndex();

		investmentInterestRateIndex.setId(1);
		investmentInterestRateIndex.setName("ICE LIBOR USD Overnight");
		investmentInterestRateIndex.setDescription("United States Overnight LIBOR     Pricing Source: ICE Benchmark Administration");
		investmentInterestRateIndex.setIsdaName("USD-LIBOR-BBA");
		investmentInterestRateIndex.setSymbol("US00O/N");
		investmentInterestRateIndex.setFixingTerm("1D");
		investmentInterestRateIndex.setCurrencyDenomination(InvestmentSecurityBuilder.newUSD());
		investmentInterestRateIndex.setCalendar(CalendarBuilder.createNYSE().toCalendar());
		investmentInterestRateIndex.setNumberOfDays(1);
		investmentInterestRateIndex.setOrder(1810);
		investmentInterestRateIndex.setStartDate(DateUtils.asUtilDate(LocalDate.of(2001, 1, 2)));

		return new InvestmentInterestRateIndexBuilder(investmentInterestRateIndex);
	}


	public InvestmentInterestRateIndex toInvestmentInterestRateIndex() {
		return this.investmentInterestRateIndex;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInterestRateIndex getInvestmentInterestRateIndex() {
		return this.investmentInterestRateIndex;
	}
}
