package com.clifton.investment.exchange;

import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarBuilder;
import com.clifton.calendar.setup.CalendarTimeZone;
import com.clifton.calendar.setup.CalendarTimeZoneBuilder;
import com.clifton.core.util.date.Time;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListItemBuilder;


public class InvestmentExchangeBuilder {

	private InvestmentExchange investmentExchange;


	private InvestmentExchangeBuilder(InvestmentExchange investmentExchange) {
		this.investmentExchange = investmentExchange;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentExchangeBuilder createNyse() {
		InvestmentExchange investmentExchange = new InvestmentExchange();

		investmentExchange.setId((short) 1);
		investmentExchange.setName("NYSE");
		investmentExchange.setDescription("A stock exchange based in New York City, which is considered the largest equities-based exchange in the world based on total market capitalization of its listed securities. Formerly run as a private organization, the NYSE became a public entity in 2005 following the acquisition of electronic trading exchange Archipelago. The parent company of the New York Stock Exchange is now called NYSE Euronext, following a merger with the European exchange in 2007. Use NY for calendar.");
		investmentExchange.setExchangeCode("UN");
		investmentExchange.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		investmentExchange.setCountry(SystemListItemBuilder.createUS().toSystemListItem());
		investmentExchange.setCalendar(CalendarBuilder.createNYSE().toCalendar());
		investmentExchange.setTimeZone(CalendarTimeZoneBuilder.createEastern().toCalendarTimeZone());
		investmentExchange.setOpenTime(new Time(34200000));
		investmentExchange.setCloseTime(new Time(57600000));

		return new InvestmentExchangeBuilder(investmentExchange);
	}


	public static InvestmentExchangeBuilder createUsComposite() {
		InvestmentExchange investmentExchange = new InvestmentExchange();

		investmentExchange.setId((short) 90);
		investmentExchange.setName("United States Composite Exchange");
		investmentExchange.setDescription("Composite exchange for United States. Includes VY, UF, UM, VJ, VK, UD, UR, UQ, UW, UT, UB, UX, UN, UP, UA, UU, PG, UV (exchange codes).");
		investmentExchange.setExchangeCode("US");
		investmentExchange.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		investmentExchange.setCompositeExchange(true);
		investmentExchange.setCountry(SystemListItemBuilder.createUS().toSystemListItem());
		investmentExchange.setCalendar(CalendarBuilder.createUSTrading().toCalendar());
		investmentExchange.setTimeZone(CalendarTimeZoneBuilder.createEastern().toCalendarTimeZone());
		investmentExchange.setOpenTime(new Time(34200000));
		investmentExchange.setCloseTime(new Time(57600000));

		return new InvestmentExchangeBuilder(investmentExchange);
	}


	public static InvestmentExchangeBuilder createChicagoMercantileExchange() {
		InvestmentExchange investmentExchange = new InvestmentExchange();

		investmentExchange.setId((short) 4);
		investmentExchange.setName("Chicago Mercantile Exchange");
		investmentExchange.setDescription("CME - Standard Exchange - www.cme.com");
		investmentExchange.setExchangeCode("CME");
		investmentExchange.setMarketIdentifierCode("XCME");
		investmentExchange.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		investmentExchange.setCountry(SystemListItemBuilder.createUS().toSystemListItem());
		investmentExchange.setCalendar(CalendarBuilder.createChicagoMercantile().toCalendar());
		investmentExchange.setTimeZone(CalendarTimeZoneBuilder.createCentral().toCalendarTimeZone());
		investmentExchange.setOpenTime(new Time(30600000));
		investmentExchange.setCloseTime(new Time(61200000));

		return new InvestmentExchangeBuilder(investmentExchange);
	}


	public static InvestmentExchangeBuilder createCustomExchange() {
		InvestmentExchange investmentExchange = new InvestmentExchange();

		investmentExchange.setId((short) 37);
		investmentExchange.setName("Custom Exchange");
		investmentExchange.setDescription("Not a real exchange.  Used to create all dates for custom benchmarks with international and domestic securities.");
		investmentExchange.setExchangeCode("CUSTEXCH");
		investmentExchange.setMarketIdentifierCode("XCME");
		investmentExchange.setCalendar(CalendarBuilder.createNoHoliday().toCalendar());
		investmentExchange.setTimeZone(CalendarTimeZoneBuilder.createCentralTime().toCalendarTimeZone());
		investmentExchange.setOpenTime(new Time(0));
		investmentExchange.setCloseTime(new Time(85500000));

		return new InvestmentExchangeBuilder(investmentExchange);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentExchangeBuilder withId(short id) {
		getInvestmentExchange().setId(id);
		return this;
	}


	public InvestmentExchangeBuilder withName(String name) {
		getInvestmentExchange().setName(name);
		return this;
	}


	public InvestmentExchangeBuilder withDescription(String description) {
		getInvestmentExchange().setDescription(description);
		return this;
	}


	public InvestmentExchangeBuilder withExchangeCode(String exchangeCode) {
		getInvestmentExchange().setExchangeCode(exchangeCode);
		return this;
	}


	public InvestmentExchangeBuilder withBaseCurrency(InvestmentSecurity baseCurrency) {
		getInvestmentExchange().setBaseCurrency(baseCurrency);
		return this;
	}


	public InvestmentExchangeBuilder withCompositeExchange(boolean compositeExchange) {
		getInvestmentExchange().setCompositeExchange(compositeExchange);
		return this;
	}


	public InvestmentExchangeBuilder withCountry(SystemListItem country) {
		getInvestmentExchange().setCountry(country);
		return this;
	}


	public InvestmentExchangeBuilder withCalendar(Calendar calendar) {
		getInvestmentExchange().setCalendar(calendar);
		return this;
	}


	public InvestmentExchangeBuilder withTimeZone(CalendarTimeZone calendarTimeZone) {
		getInvestmentExchange().setTimeZone(calendarTimeZone);
		return this;
	}


	public InvestmentExchangeBuilder withOpenTime(Time openTime) {
		getInvestmentExchange().setOpenTime(openTime);
		return this;
	}


	public InvestmentExchangeBuilder withCloseTime(Time closeTime) {
		getInvestmentExchange().setCloseTime(closeTime);
		return this;
	}


	public InvestmentExchange toInvestmentExchange() {
		return this.investmentExchange;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentExchange getInvestmentExchange() {
		return this.investmentExchange;
	}
}
