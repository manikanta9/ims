package com.clifton.investment.instrument;

import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarBuilder;
import com.clifton.investment.exchange.InvestmentExchangeBuilder;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentInstrumentHierarchyBuilder;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.system.bean.SystemBeanBuilder;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListItemBuilder;

import java.math.BigDecimal;


public class InvestmentInstrumentBuilder {

	private InvestmentInstrument investmentInstrument;


	private InvestmentInstrumentBuilder(InvestmentInstrument investmentInstrument) {
		this.investmentInstrument = investmentInstrument;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentInstrumentBuilder createEmptyInvestmentInstrument() {
		return new InvestmentInstrumentBuilder(new InvestmentInstrument());
	}


	public static InvestmentInstrumentBuilder createUSDollar() {
		InvestmentInstrument investmentInstrument = new InvestmentInstrument();

		investmentInstrument.setId(462);
		investmentInstrument.setName("US Dollar");
		// investmentInstrument.setHierarchy(InvestmentInstrumentHierarchyBuilder.createPhysicals().toInvestmentInstrumentHierarchy());
		investmentInstrument.setTradingCurrency(InvestmentSecurityBuilder.newUSD());
		investmentInstrument.setIdentifierPrefix("USD");
		investmentInstrument.setPriceMultiplier(BigDecimal.valueOf(1.0000000000));
		investmentInstrument.setExposureMultiplier(BigDecimal.valueOf(1.0000000000));
		investmentInstrument.setCostCalculator(InvestmentNotionalCalculatorTypes.MONEY_HALF_UP);
		investmentInstrument.setCountryOfRisk(SystemListItemBuilder.createUS().toSystemListItem());
		investmentInstrument.setSettlementCalendar(CalendarBuilder.createUSBanks().toCalendar());

		return new InvestmentInstrumentBuilder(investmentInstrument);
	}


	public static InvestmentInstrumentBuilder createDiax() {
		InvestmentInstrument investmentInstrument = new InvestmentInstrument();

		investmentInstrument.setId(13784);
		investmentInstrument.setHierarchy(InvestmentInstrumentHierarchyBuilder.createCef().toInvestmentInstrumentHierarchy());
		investmentInstrument.setTradingCurrency(InvestmentSecurityBuilder.newUSD());
		investmentInstrument.setExchange(InvestmentExchangeBuilder.createNyse().toInvestmentExchange());
		investmentInstrument.setCompositeExchange(InvestmentExchangeBuilder.createUsComposite().toInvestmentExchange());
		investmentInstrument.setName("Nuveen Dow 30sm Dynamic Overwrite Fund");
		investmentInstrument.setDescription("Closed-End Fund // Equity // Value");
		investmentInstrument.setIdentifierPrefix("DIAX");
		investmentInstrument.setPriceMultiplier(BigDecimal.valueOf(1.0000000000));
		investmentInstrument.setExposureMultiplier(BigDecimal.valueOf(1.0000000000));
		investmentInstrument.setCountryOfRisk(SystemListItemBuilder.createUS().toSystemListItem());

		return new InvestmentInstrumentBuilder(investmentInstrument);
	}


	public static InvestmentInstrumentBuilder createSandPBigContractFutures() {
		InvestmentInstrument investmentInstrument = new InvestmentInstrument();

		investmentInstrument.setId(482);
		investmentInstrument.setHierarchy(InvestmentInstrumentHierarchyBuilder.createFuturesContractOnStockOrIndex().toInvestmentInstrumentHierarchy());
		investmentInstrument.setTradingCurrency(InvestmentSecurityBuilder.newUSD());
		investmentInstrument.setExchange(InvestmentExchangeBuilder.createChicagoMercantileExchange().toInvestmentExchange());
		investmentInstrument.setUnderlyingInstrument(InvestmentInstrumentBuilder.createSandP500Index().toInvestmentInstrument());
		investmentInstrument.setName("S&P 500 Big Contract Futures (SP)");
		investmentInstrument.setDescription("S&P 500 Big Contract Futures (SP)");
		investmentInstrument.setIdentifierPrefix("SP");
		investmentInstrument.setPriceMultiplier(BigDecimal.valueOf(250.0000000000));
		investmentInstrument.setExposureMultiplier(BigDecimal.valueOf(1.0000000000));
		investmentInstrument.setHedgerInitialMarginPerUnit(BigDecimal.valueOf(21000.00));
		investmentInstrument.setHedgerSecondaryMarginPerUnit(BigDecimal.valueOf(21000.00));
		investmentInstrument.setSpeculatorInitialMarginPerUnit(BigDecimal.valueOf(23100.00));
		investmentInstrument.setSpeculatorSecondaryMarginPerUnit(BigDecimal.valueOf(21000.00));
		investmentInstrument.setFairValueAdjustmentUsed(true);
		investmentInstrument.setSpotMonthCalculatorBean(SystemBeanBuilder.createDeliveryMonth().toSystemBean());

		return new InvestmentInstrumentBuilder(investmentInstrument);
	}


	public static InvestmentInstrumentBuilder createSandPMiniFutures() {
		InvestmentInstrument investmentInstrument = new InvestmentInstrument();

		investmentInstrument.setId(483);
		investmentInstrument.setHierarchy(InvestmentInstrumentHierarchyBuilder.createFuturesContractOnStockOrIndex().toInvestmentInstrumentHierarchy());
		investmentInstrument.setTradingCurrency(InvestmentSecurityBuilder.newUSD());
		investmentInstrument.setExchange(InvestmentExchangeBuilder.createChicagoMercantileExchange().toInvestmentExchange());
		investmentInstrument.setUnderlyingInstrument(InvestmentInstrumentBuilder.createSandP500Index().toInvestmentInstrument());
		investmentInstrument.setBigInstrument(InvestmentInstrumentBuilder.createSandPBigContractFutures().toInvestmentInstrument());
		investmentInstrument.setName("S&P 500 Mini Futures (ES)");
		investmentInstrument.setDescription("S&P 500 Mini Futures (ES)");
		investmentInstrument.setIdentifierPrefix("ES");
		investmentInstrument.setPriceMultiplier(BigDecimal.valueOf(50.0000000000));
		investmentInstrument.setExposureMultiplier(BigDecimal.valueOf(1.0000000000));

		investmentInstrument.setHedgerInitialMarginPerUnit(BigDecimal.valueOf(4200.00));
		investmentInstrument.setHedgerSecondaryMarginPerUnit(BigDecimal.valueOf(4200.00));
		investmentInstrument.setSpeculatorInitialMarginPerUnit(BigDecimal.valueOf(4620.00));
		investmentInstrument.setSpeculatorSecondaryMarginPerUnit(BigDecimal.valueOf(4200.00));
		investmentInstrument.setFairValueAdjustmentUsed(true);
		investmentInstrument.setSpotMonthCalculatorBean(SystemBeanBuilder.createDeliveryMonth().toSystemBean());

		return new InvestmentInstrumentBuilder(investmentInstrument);
	}


	public static InvestmentInstrumentBuilder createSandP500Index() {
		InvestmentInstrument investmentInstrument = new InvestmentInstrument();

		investmentInstrument.setId(49);
		investmentInstrument.setHierarchy(InvestmentInstrumentHierarchyBuilder.createPricedDaily().toInvestmentInstrumentHierarchy());
		investmentInstrument.setTradingCurrency(InvestmentSecurityBuilder.newUSD());

		investmentInstrument.setExchange(InvestmentExchangeBuilder.createChicagoMercantileExchange().toInvestmentExchange());

		investmentInstrument.setName("S&P 500 Index");
		investmentInstrument.setDescription("Standard and Poor's 500 Index is a capitalization-weighted index of 500 stocks. The index is designed to measure performance of the broad domestic economy through changes in the aggregate market value of 500 stocks representing all major industries. The index was developed with a base level of 10 for the 1941-43 base period.");
		investmentInstrument.setIdentifierPrefix("SPX");
		investmentInstrument.setPriceMultiplier(BigDecimal.valueOf(1.0000000000));
		investmentInstrument.setExposureMultiplier(BigDecimal.valueOf(1.0000000000));
		investmentInstrument.setCountryOfRisk(SystemListItemBuilder.createUS().toSystemListItem());
		investmentInstrument.setSettlementCalendar(CalendarBuilder.createSystem().toCalendar());

		return new InvestmentInstrumentBuilder(investmentInstrument);
	}


	public static InvestmentInstrumentBuilder createCADGBPCurrencyForward() {
		InvestmentInstrument investmentInstrument = new InvestmentInstrument();

		investmentInstrument.setId(14531);
		investmentInstrument.setHierarchy(InvestmentInstrumentHierarchyBuilder.createPricedDaily().toInvestmentInstrumentHierarchy());
		investmentInstrument.setTradingCurrency(InvestmentSecurityBuilder.newCAD());

		investmentInstrument.setName("CAD/GBP Currency Forward");
		investmentInstrument.setIdentifierPrefix("CAD-GBP");
		investmentInstrument.setPriceMultiplier(BigDecimal.valueOf(1.0000000000));
		investmentInstrument.setExposureMultiplier(BigDecimal.valueOf(1.0000000000));
		investmentInstrument.setCountryOfRisk(SystemListItemBuilder.createCanada().toSystemListItem());
		investmentInstrument.setSettlementCalendar(CalendarBuilder.createSystem().toCalendar());

		return new InvestmentInstrumentBuilder(investmentInstrument);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentBuilder withId(int id) {
		getInvestmentInstrument().setId(id);
		return this;
	}


	public InvestmentInstrumentBuilder withName(String name) {
		getInvestmentInstrument().setName(name);
		return this;
	}


	public InvestmentInstrumentBuilder withDescription(String description) {
		getInvestmentInstrument().setDescription(description);
		return this;
	}


	public InvestmentInstrumentBuilder withHierarchy(InvestmentInstrumentHierarchy hierarchy) {
		getInvestmentInstrument().setHierarchy(hierarchy);
		return this;
	}


	public InvestmentInstrumentBuilder withTradingCurrency(InvestmentSecurity tradingCurrency) {
		getInvestmentInstrument().setTradingCurrency(tradingCurrency);
		return this;
	}


	public InvestmentInstrumentBuilder withIdentifierPrefix(String identifierPrefix) {
		getInvestmentInstrument().setIdentifierPrefix(identifierPrefix);
		return this;
	}


	public InvestmentInstrumentBuilder withPriceMultiplier(BigDecimal priceMultiplier) {
		getInvestmentInstrument().setPriceMultiplier(priceMultiplier);
		return this;
	}


	public InvestmentInstrumentBuilder withExposureMultiplier(BigDecimal exposureMultiplier) {
		getInvestmentInstrument().setExposureMultiplier(exposureMultiplier);
		return this;
	}


	public InvestmentInstrumentBuilder withCostCalculator(InvestmentNotionalCalculatorTypes investmentNotionalCalculatorTypes) {
		getInvestmentInstrument().setCostCalculator(investmentNotionalCalculatorTypes);
		return this;
	}


	public InvestmentInstrumentBuilder withCountryOfRisk(SystemListItem countryOfRisk) {
		getInvestmentInstrument().setCountryOfRisk(countryOfRisk);
		return this;
	}


	public InvestmentInstrumentBuilder withCountryOfIncorporation(SystemListItem countryOfIncorporation) {
		getInvestmentInstrument().setCountryOfIncorporation(countryOfIncorporation);
		return this;
	}


	public InvestmentInstrumentBuilder withSettlementCalendar(Calendar settlementCalendar) {
		getInvestmentInstrument().setSettlementCalendar(settlementCalendar);
		return this;
	}


	public InvestmentInstrument toInvestmentInstrument() {
		return this.investmentInstrument;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentInstrument getInvestmentInstrument() {
		return this.investmentInstrument;
	}
}
