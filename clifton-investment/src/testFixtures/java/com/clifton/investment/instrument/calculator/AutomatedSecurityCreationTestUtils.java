package com.clifton.investment.instrument.calculator;

import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.copy.jobs.InvestmentSecurityCreationForwardJob;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author mitchellf
 */
public class AutomatedSecurityCreationTestUtils {

	public static final String CALCULATOR_BEAN_TYPE = "Calendar Manual Calendar List Calculator";
	public static final String JOB_BEAN_TYPE = "Investment Security Creation Forward Job";
	public static final String FIELD_OVERRIDE_BEAN_TYPE = "Currency Forward Field Override Value Calculator";
	public static final String CALENDARS_TO_INCLUDE = "Calendars To Include";
	public static final String INSTRUMENTS = "Instruments";
	public static final String CALENDAR_CALCULATOR = "Calendar Override Calculator";
	public static final String EVALUATION_DATE_OVERRIDE = "Evaluation Date Override";
	public static final String FILED_OVERRIDES = "Field Overrides";
	public static final String MATURITY_SCHEDULE = "Maturity Schedule";
	public static final String FIXING_DATE = "Fixing Date Adjustment";
	public static final String BRAZIL_CALENDAR = "Brazil -FX Composite";
	public static final String US_BANKS = "US Banks Calendar";
	public static final String BRZ_FWD_INSTRUMENT = "BRL/USD Currency Forward NDF";
	public static final String USD_FWD_INSTRUMENT = "USD/EUR Currency Forward";
	public static final String NUMBER_OCCURRENCES = "Number of Occurrences";


	public static SystemBean createCalendarListCalculatorBean(SystemBeanService systemBeanService, CalendarSetupService calendarSetupService) {
		SystemBeanType beanType = systemBeanService.getSystemBeanTypeByName(CALCULATOR_BEAN_TYPE);

		SystemBean calculator = new SystemBean();
		calculator.setType(beanType);
		calculator.setName("Default Calendar Calculator Bean");
		calculator.setDescription("Test Bean");

		List<SystemBeanPropertyType> propertyTypes = systemBeanService.getSystemBeanPropertyTypeListByBean(calculator);
		List<SystemBeanProperty> properties = new ArrayList<>();

		createSystemBeanProperty(properties, propertyTypes, CALENDARS_TO_INCLUDE, calendarSetupService.getCalendarByName(US_BANKS).getId().toString(), calculator);

		calculator.setPropertyList(properties);
		return systemBeanService.saveSystemBean(calculator);
	}


	public static List<SystemBeanProperty> createSystemBeanProperty(List<SystemBeanProperty> propertyList, List<SystemBeanPropertyType> propertyTypes, String propertyTypeName, String propertyValue, SystemBean parentBean) {
		SystemBeanPropertyType propertyType = CollectionUtils.getOnlyElement(propertyTypes.stream().filter(type -> propertyTypeName.equals(type.getName())).collect(Collectors.toList()));
		SystemBeanProperty property = new SystemBeanProperty();
		property.setType(propertyType);
		property.setValue(propertyValue);
		property.setBean(parentBean);
		propertyList.add(property);
		parentBean.setPropertyList(propertyList);
		return propertyList;
	}


	public static void validateResults(InvestmentInstrumentService investmentInstrumentService, Status result, int existingSecurities, int newSecurities, int noTemplate, Date maturityDate, InvestmentInstrument instrument) {
		if (maturityDate != null && instrument != null) {
			SecuritySearchForm sf = new SecuritySearchForm();
			String type = instrument.getHierarchy().getInvestmentType().getName();
			if (InvestmentType.FORWARDS.equals(type) && !instrument.isDeliverable()) {
				sf.setFirstDeliveryDate(maturityDate);
			}
			else if (InvestmentType.OPTIONS.equals(type)) {
				sf.setLastDeliveryDate(maturityDate);
			}
			else {
				sf.setEndDate(maturityDate);
			}
			sf.setInstrumentId(instrument.getId());

			InvestmentSecurity security = CollectionUtils.getOnlyElement(investmentInstrumentService.getInvestmentSecurityList(sf));

			ValidationUtils.assertNotNull(security, "Did not find expected new security");
		}
		ValidationUtils.assertEquals(result.getStatusMap().get(InvestmentSecurityCreationForwardJob.EXISTING_SECURITIES), existingSecurities, "Expected to find " + existingSecurities + " existing securities but actually found " + result.getStatusMap().get(InvestmentSecurityCreationForwardJob.EXISTING_SECURITIES));
		ValidationUtils.assertEquals(result.getStatusMap().get(InvestmentSecurityCreationForwardJob.NEW_SECURITIES), newSecurities, "Expected to create " + newSecurities + " new securities but actually created " + result.getStatusMap().get(InvestmentSecurityCreationForwardJob.NEW_SECURITIES));
		ValidationUtils.assertEquals(result.getStatusMap().get(InvestmentSecurityCreationForwardJob.NO_TEMPLATE), noTemplate, "Expected to find " + noTemplate + " instruments with no security for template but actually found " + result.getStatusMap().get(InvestmentSecurityCreationForwardJob.NO_TEMPLATE));
	}
}
