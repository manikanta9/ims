package com.clifton.investment.instrument;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeBuilder;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;
import java.util.Set;


public class InvestmentSecurityBuilder {

	private static final Random RANDOM = new Random();

	public static final Set<String> CURRENCIES_WITHOUT_PENNIES = CollectionUtils.newUnmodifiableSet("IDR, JPY, KRW");

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final InvestmentSecurity investmentSecurity;


	private InvestmentSecurityBuilder(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	private InvestmentSecurityBuilder(String symbol, Integer id) {
		this.investmentSecurity = (id == null) ? new InvestmentSecurity(symbol) : new InvestmentSecurity(symbol, id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentSecurity newAUD() {
		return InvestmentSecurityBuilder.ofType(263, "AUD", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newBRL() {
		return InvestmentSecurityBuilder.ofType(16284, "BRL", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newCAD() {
		return InvestmentSecurityBuilder.ofType(425, "CAD", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newCHF() {
		return InvestmentSecurityBuilder.ofType(458, "CHF", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newCNH() {
		return InvestmentSecurityBuilder.ofType(22632, "CNH", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newDKK() {
		return InvestmentSecurityBuilder.ofType(12375, "DKK", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newEUR() {
		return InvestmentSecurityBuilder.ofType(2875, "EUR", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newGBP() {
		return InvestmentSecurityBuilder.ofType(764, "GBP", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newHKD() {
		return InvestmentSecurityBuilder.ofType(850, "HKD", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newIDR() {
		return InvestmentSecurityBuilder.ofType(20260, "IDR", InvestmentType.CURRENCY).withNotionalCalculator(InvestmentNotionalCalculatorTypes.INTEGER_HALF_UP).build();
	}


	public static InvestmentSecurity newILS() {
		return InvestmentSecurityBuilder.ofType(23102, "ILS", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newINR() {
		return InvestmentSecurityBuilder.ofType(12455, "INR", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newJPY() {
		return InvestmentSecurityBuilder.ofType(948, "JPY", InvestmentType.CURRENCY).withNotionalCalculator(InvestmentNotionalCalculatorTypes.INTEGER_HALF_UP).build();
	}


	public static InvestmentSecurity newKRW() {
		return InvestmentSecurityBuilder.ofType(12377, "KRW", InvestmentType.CURRENCY).withNotionalCalculator(InvestmentNotionalCalculatorTypes.INTEGER_HALF_UP).build();
	}


	public static InvestmentSecurity newMXN() {
		return InvestmentSecurityBuilder.ofType(12453, "MXN", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newNOK() {
		return InvestmentSecurityBuilder.ofType(16310, "NOK", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newRUB() {
		return InvestmentSecurityBuilder.ofType(20263, "RUB", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newSEK() {
		return InvestmentSecurityBuilder.ofType(10476, "SEK", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newTHB() {
		return InvestmentSecurityBuilder.ofType(20256, "THB", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newTRY() {
		return InvestmentSecurityBuilder.ofType(20255, "TRY", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newTWD() {
		return InvestmentSecurityBuilder.ofType(12378, "TWD", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newUSD() {
		return InvestmentSecurityBuilder.ofType(1927, "USD", InvestmentType.CURRENCY).build();
	}


	public static InvestmentSecurity newZAR() {
		return InvestmentSecurityBuilder.ofType(12379, "ZAR", InvestmentType.CURRENCY).build();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentSecurityBuilder createEmptyInvestmentSecurity() {
		return new InvestmentSecurityBuilder(new InvestmentSecurity());
	}


	public static InvestmentSecurityBuilder newBond(String symbol) {
		return ofType(symbol, InvestmentType.BONDS);
	}


	public static InvestmentSecurityBuilder newStock(String symbol) {
		return ofType(symbol, InvestmentType.STOCKS);
	}


	public static InvestmentSecurityBuilder newForward(String symbol) {
		return ofType(symbol, InvestmentType.FORWARDS);
	}


	public static InvestmentSecurityBuilder newFuture(String symbol) {
		return ofType(symbol, InvestmentType.FUTURES);
	}


	public static InvestmentSecurityBuilder newCDS(String symbol) {
		return ofSubType(symbol, InvestmentTypeSubType.CREDIT_DEFAULT_SWAPS);
	}


	public static InvestmentSecurityBuilder newIRS(String symbol) {
		return ofSubType(symbol, InvestmentTypeSubType.INTEREST_RATE_SWAPS);
	}


	public static InvestmentSecurityBuilder newTRS(String symbol) {
		return ofSubType(symbol, InvestmentTypeSubType.TOTAL_RETURN_SWAPS);
	}


	public static InvestmentSecurityBuilder ofType(String symbol, String investmentType) {
		return ofType(null, symbol, investmentType);
	}


	public static InvestmentSecurityBuilder ofType(Integer id, String symbol, String investmentType) {
		// use the InvestmentTypeBuilder to avoid circular dependency on InvestmentTestObjectFactory.
		return ofType(id, symbol, InvestmentTypeBuilder.forInvestmentType(investmentType).toInvestmentType());
	}


	/**
	 * Creates a security the the provided symbol and investment type. The instrument, hierarchy, and other
	 * important properties will be defaulted based on the provided investment type. The default trading
	 * currency will be USD, unless the security is a Currency, where it would be itself.
	 */
	public static InvestmentSecurityBuilder ofType(Integer id, String symbol, InvestmentType investmentType) {
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setId((short) (1000 + RANDOM.nextInt(Short.MAX_VALUE - 1000)));
		hierarchy.setInvestmentType(investmentType);
		hierarchy.setName("Hierarchy for " + investmentType.getName());

		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setId((5000 + RANDOM.nextInt() * 100));
		instrument.setHierarchy(hierarchy);
		instrument.setPriceMultiplier(BigDecimal.ONE);

		InvestmentSecurityBuilder securityBuilder = new InvestmentSecurityBuilder(symbol, id)
				.withInstrument(instrument);

		if (InvestmentType.CURRENCY.equals(investmentType.getName())) {
			securityBuilder.asCurrency(true);
			instrument.setTradingCurrency(securityBuilder.build());
			instrument.setCostCalculator(CURRENCIES_WITHOUT_PENNIES.contains(symbol) ? InvestmentNotionalCalculatorTypes.INTEGER_HALF_UP : InvestmentNotionalCalculatorTypes.MONEY_HALF_UP);
		}
		else {
			instrument.setTradingCurrency(newUSD());
		}

		switch (investmentType.getName()) {
			case InvestmentType.BONDS:
				hierarchy.setAccrualSecurityEventType(new InvestmentSecurityEventType());
				hierarchy.getAccrualSecurityEventType().setName(InvestmentSecurityEventType.CASH_COUPON_PAYMENT);
				instrument.setPriceMultiplier(new BigDecimal("0.01"));
				break;
			case InvestmentType.FUTURES:
				hierarchy.setNoPaymentOnOpen(true);
				break;
			case InvestmentType.SWAPS:
				hierarchy.setAssignmentAllowed(true);
				hierarchy.setAccrualSecurityEventType(new InvestmentSecurityEventType());
				// NOTE: keep same as bonds for simplicity of setup
				hierarchy.getAccrualSecurityEventType().setName(InvestmentSecurityEventType.CASH_COUPON_PAYMENT);
				break;
		}

		return securityBuilder;
	}


	public static InvestmentSecurityBuilder ofSubType(String symbol, String subType) {
		return ofSubType(null, symbol, subType);
	}


	public static InvestmentSecurityBuilder ofSubType(Integer id, String symbol, String subType) {
		InvestmentSecurityBuilder result;
		switch (subType) {
			case InvestmentTypeSubType.CREDIT_DEFAULT_SWAPS:
				result = ofType(id, symbol, InvestmentType.SWAPS);
				result.getInvestmentSecurity().getInstrument().setCostCalculator(InvestmentNotionalCalculatorTypes.BPS_DISCOUNT_MONEY_HALF_UP);
				result.getInvestmentSecurity().getInstrument().setPriceMultiplier(new BigDecimal("0.01"));
				result.getInvestmentSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
				result.getInvestmentSecurity().getInstrument().getHierarchy().setInvestmentTypeSubType(getCreditDefaultSwapsSubType());
				result.getInvestmentSecurity().getInstrument().getHierarchy().setDifferentQuantityAndCostBasisSignAllowed(true);
				break;
			case InvestmentTypeSubType.INTEREST_RATE_SWAPS:
				result = ofType(id, symbol, InvestmentType.SWAPS);
				result.getInvestmentSecurity().getInstrument().setCostCalculator(InvestmentNotionalCalculatorTypes.BPS_DISCOUNT_MONEY_HALF_UP);
				result.getInvestmentSecurity().getInstrument().setPriceMultiplier(new BigDecimal("0.01"));
				result.getInvestmentSecurity().getInstrument().getHierarchy().setInvestmentTypeSubType(getInterestRateSwapsSubType());
				result.getInvestmentSecurity().getInstrument().getHierarchy().setDifferentQuantityAndCostBasisSignAllowed(true);
				break;
			case InvestmentTypeSubType.TOTAL_RETURN_SWAPS:
				result = ofType(id, symbol, InvestmentType.SWAPS);
				break;
			default:
				throw new IllegalArgumentException("Unsupported Investment Sub Type: " + subType);
		}

		if (result.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentTypeSubType() == null) {
			result.getInvestmentSecurity().getInstrument().getHierarchy().setInvestmentTypeSubType(new InvestmentTypeSubType(subType));
		}
		return result;
	}


	private static InvestmentTypeSubType getCreditDefaultSwapsSubType() {
		InvestmentTypeSubType type = new InvestmentTypeSubType(InvestmentTypeSubType.CREDIT_DEFAULT_SWAPS);
		type.setUnadjustedQuantityName("Original Notional");
		type.setQuantityName("Current Notional");
		return type;
	}


	private static InvestmentTypeSubType getInterestRateSwapsSubType() {
		InvestmentTypeSubType type = new InvestmentTypeSubType(InvestmentTypeSubType.INTEREST_RATE_SWAPS);
		type.setUnadjustedQuantityName("Notional");
		type.setQuantityName("Notional");
		return type;
	}


	public InvestmentSecurity build() {
		return getInvestmentSecurity();
	}


	public static InvestmentSecurityBuilder createSandPIndex() {
		return ofType(1618, "SPX", InvestmentType.BENCHMARKS)
				.withName("S&P 500 Index")
				.withDescription("Standard and Poor's 500 Index is a capitalization-weighted index of 500 stocks. The index is designed to measure performance of the broad domestic economy through changes in the aggregate market value of 500 stocks representing all major industries. The index was developed with a base level of 10 for the 1941-43 base period.")
				.withInstrument(InvestmentInstrumentBuilder.createSandP500Index().toInvestmentInstrument());
	}


	public static InvestmentSecurityBuilder createDiax() {
		return ofType(29363, "DIAX", InvestmentType.FUNDS)
				.withName("Nuveen Dow 30sm Dynamic Overwrite Fund")
				.withDescription("Closed-End Fund // Equity // Value")
				.withCusip("67075F105")
				.withStartDate(DateUtils.toDate("04/29/2005"))
				.withBusinessCompany(BusinessCompanyBuilder.createNuveen().toBusinessCompany())
				.withIsin("US67075F1057")
				.withSedol("BTF3114")
				.withInstrument(InvestmentInstrumentBuilder.createDiax().toInvestmentInstrument());
	}


	public static InvestmentSecurityBuilder createEsu6() {
		return ofType(35121, "ESU6", InvestmentType.FUTURES)
				.withName("S&P500 EMINI FUT  Sep16")
				.withDescription("S&P500 EMINI FUT")
				.withStartDate(DateUtils.toDate("06/19/2015"))
				.withEndDate(DateUtils.toDate("06/19/2016"))
				.withInstrument(InvestmentInstrumentBuilder.createSandPMiniFutures().toInvestmentInstrument())
				.withFirstNoticeDate(DateUtils.toDate("06/19/2016"))
				.withBigSecurity(InvestmentSecurityBuilder.createSandPIndex().toInvestmentSecurity());
	}


	public static InvestmentSecurityBuilder createEsu6BigContract() {
		return ofType(31863, "SPU6", InvestmentType.FUTURES)
				.withName("S&P 500 FUTURE  Sep16")
				.withDescription("S&P500 FUT")
				.withStartDate(DateUtils.toDate("06/19/2015"))
				.withEndDate(DateUtils.toDate("06/19/2016"))
				.withFirstNoticeDate(DateUtils.toDate("06/19/2016"))
				.withInstrument(InvestmentInstrumentBuilder.createSandPBigContractFutures().toInvestmentInstrument());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityBuilder withId(int id) {
		getInvestmentSecurity().setId(id);
		return this;
	}


	public InvestmentSecurityBuilder withName(String name) {
		getInvestmentSecurity().setName(name);
		return this;
	}


	public InvestmentSecurityBuilder withDescription(String description) {
		getInvestmentSecurity().setDescription(description);
		return this;
	}


	public InvestmentSecurityBuilder withSymbol(String symbol) {
		getInvestmentSecurity().setSymbol(symbol);
		return this;
	}


	public InvestmentSecurityBuilder withPriceMultiplier(int priceMultiplier) {
		return withPriceMultiplier(new BigDecimal(priceMultiplier));
	}


	public InvestmentSecurityBuilder withPriceMultiplier(BigDecimal priceMultiplier) {
		getInvestmentSecurity().getInstrument().setPriceMultiplier(priceMultiplier);
		return this;
	}


	public InvestmentSecurityBuilder withTradingCurrency(InvestmentSecurity tradingCurrency) {
		getInvestmentSecurity().getInstrument().setTradingCurrency(tradingCurrency);
		return this;
	}


	public InvestmentSecurityBuilder withNotionalCalculator(InvestmentNotionalCalculatorTypes notionalCalculator) {
		getInvestmentSecurity().getInstrument().setCostCalculator(notionalCalculator);
		return this;
	}


	public InvestmentSecurityBuilder withInstrument(InvestmentInstrument instrument) {
		getInvestmentSecurity().setInstrument(instrument);
		return this;
	}


	public InvestmentSecurityBuilder withBigSecurity(InvestmentSecurity bigSecurity) {
		getInvestmentSecurity().setBigSecurity(bigSecurity);
		return this;
	}


	public InvestmentSecurityBuilder asCurrency(boolean currency) {
		getInvestmentSecurity().setCurrency(currency);
		return this;
	}


	public InvestmentSecurityBuilder withSedol(String sedol) {
		getInvestmentSecurity().setSedol(sedol);
		return this;
	}


	public InvestmentSecurityBuilder withCusip(String cusip) {
		getInvestmentSecurity().setCusip(cusip);
		return this;
	}


	public InvestmentSecurityBuilder withIsin(String isin) {
		getInvestmentSecurity().setIsin(isin);
		return this;
	}


	public InvestmentSecurityBuilder withStartDate(Date startDate) {
		getInvestmentSecurity().setStartDate(startDate);
		return this;
	}


	public InvestmentSecurityBuilder withEndDate(Date endDate) {
		getInvestmentSecurity().setEndDate(endDate);
		return this;
	}


	public InvestmentSecurityBuilder withFirstNoticeDate(Date firstNoticeDate) {
		getInvestmentSecurity().setFirstNoticeDate(firstNoticeDate);
		return this;
	}


	public InvestmentSecurityBuilder withBusinessCompany(BusinessCompany businessCompany) {
		getInvestmentSecurity().setBusinessCompany(businessCompany);
		return this;
	}


	public InvestmentSecurityBuilder withOptionType(InvestmentSecurityOptionTypes optionType) {
		getInvestmentSecurity().setOptionType(optionType);
		return this;
	}


	public InvestmentSecurityBuilder withOptionStyle(OptionStyleTypes optionStyle) {
		getInvestmentSecurity().setOptionStyle(optionStyle);
		return this;
	}


	public InvestmentSecurityBuilder withOptionStrikePrice(BigDecimal strikePrice) {
		getInvestmentSecurity().setOptionStrikePrice(strikePrice);
		return this;
	}


	public InvestmentSecurityBuilder withOptionExpiryTime(ExpiryTimeValues expiryTime) {
		getInvestmentSecurity().setSettlementExpiryTime(expiryTime);
		return this;
	}


	public InvestmentSecurity toInvestmentSecurity() {
		return getInvestmentSecurity();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}
}
