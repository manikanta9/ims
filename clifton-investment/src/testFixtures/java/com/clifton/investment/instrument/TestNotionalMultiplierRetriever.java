package com.clifton.investment.instrument;

import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;

import java.math.BigDecimal;
import java.util.Date;
import java.util.function.Supplier;


/**
 * A test class used for tests that require a customizable NotionalMultiplierRetriever, which can be set up to return
 * a specific BigDecimal value.
 *
 * @author davidi
 */
public class TestNotionalMultiplierRetriever implements NotionalMultiplierRetriever {

	BigDecimal multiplierValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TestNotionalMultiplierRetriever() {
	}


	public TestNotionalMultiplierRetriever(BigDecimal multiplierValue) {
		setMultiplierValue(multiplierValue);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal retrieveNotionalMultiplier(InvestmentSecurity security, Supplier<Date> dateSupplier, boolean exceptionIfMissing) {
		return getMultiplierValue();
	}


	@Override
	public void validate(InvestmentInstrumentHierarchy hierarchy) {
		// ignore
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getMultiplierValue() {
		return this.multiplierValue;
	}


	public void setMultiplierValue(BigDecimal multiplierValue) {
		this.multiplierValue = multiplierValue;
	}
}
