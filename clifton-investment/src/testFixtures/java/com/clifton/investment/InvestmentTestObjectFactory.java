package com.clifton.investment;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>InvestmentTestObjectFactory</code> class contains helper methods for creating various investment
 * objects used in tests.
 *
 * @author vgomelsky
 */
public class InvestmentTestObjectFactory {

	public static final int INVESTMENT_ACCOUNT_CLIENT_DEFAULT = 100;
	public static final int INVESTMENT_ACCOUNT_CLIENT_DEFAULT2 = 101;
	public static final int INVESTMENT_ACCOUNT_HOLDING_DEFAULT = 200;
	public static final int INVESTMENT_ACCOUNT_HOLDING_DEFAULT2 = 201;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentAccount newInvestmentAccount_ClientDefault() {
		InvestmentAccount result = new InvestmentAccount(INVESTMENT_ACCOUNT_CLIENT_DEFAULT, "Test Client Account");
		result.setNumber("100000");
		result.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		result.setType(new InvestmentAccountType());
		result.getType().setName("Client");
		result.getType().setOurAccount(true);
		result.setIssuingCompany(new BusinessCompany());
		result.getIssuingCompany().setName("Parametric Clifton");
		result.setWorkflowStatus(new WorkflowStatus());
		result.getWorkflowStatus().setName("Active");
		return result;
	}


	public static InvestmentAccount newInvestmentAccount_ClientDefault2() {
		InvestmentAccount result = new InvestmentAccount(INVESTMENT_ACCOUNT_CLIENT_DEFAULT2, "Test Client Account 2");
		result.setNumber("200000");
		result.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		result.setType(new InvestmentAccountType());
		result.getType().setName("Client");
		result.getType().setOurAccount(true);
		result.setIssuingCompany(new BusinessCompany());
		result.getIssuingCompany().setName("Parametric Clifton");
		result.setWorkflowStatus(new WorkflowStatus());
		result.getWorkflowStatus().setName("Active");
		return result;
	}


	public static InvestmentAccount newInvestmentAccount_HoldingDefault() {
		InvestmentAccount result = new InvestmentAccount(INVESTMENT_ACCOUNT_HOLDING_DEFAULT, "Test Holding Account");
		result.setNumber("H-200");
		result.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		result.setType(new InvestmentAccountType());
		result.getType().setName("Broker");
		result.setIssuingCompany(new BusinessCompany());
		result.getIssuingCompany().setName("Goldman Sachs");
		result.setWorkflowStatus(new WorkflowStatus());
		result.getWorkflowStatus().setName("Active");
		return result;
	}


	public static InvestmentAccount newInvestmentAccount_HoldingDefault2() {
		InvestmentAccount result = new InvestmentAccount(INVESTMENT_ACCOUNT_HOLDING_DEFAULT2, "Test Holding Account 2");
		result.setNumber("H-201");
		result.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		result.setType(new InvestmentAccountType());
		result.getType().setName("Broker");
		result.setIssuingCompany(new BusinessCompany());
		result.getIssuingCompany().setName("Goldman Sachs");
		result.setWorkflowStatus(new WorkflowStatus());
		result.getWorkflowStatus().setName("Active");
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////               InvestmentSecurity              ///////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentSecurity newInvestmentSecurity(int id, String symbol, String investmentType, BigDecimal priceMultiplier, String ccyDenominationSymbol) {
		return newInvestmentSecurity(id, symbol, investmentType, priceMultiplier, ccyDenominationSymbol, null);
	}


	public static InvestmentSecurity newInvestmentSecurity(int id, String symbol, String investmentType, BigDecimal priceMultiplier, String ccyDenominationSymbol, String identifierPrefix) {
		InvestmentSecurity security = new InvestmentSecurity(symbol, id);

		InvestmentType type = new InvestmentType();
		type.setId(MathUtils.SHORT_ONE);
		type.setName(investmentType);

		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		hierarchy.setId(Short.parseShort("100"));
		hierarchy.setInvestmentType(type);

		InvestmentInstrument instrument = new InvestmentInstrument();
		instrument.setPriceMultiplier(priceMultiplier);
		instrument.setId(1);
		instrument.setIdentifierPrefix(identifierPrefix);
		if (ccyDenominationSymbol.equals(symbol)) {
			instrument.setTradingCurrency(security);
		}
		else {
			instrument.setTradingCurrency(getCurrencyForSymbol(ccyDenominationSymbol));
		}
		instrument.setHierarchy(hierarchy);
		security.setInstrument(instrument);

		switch (investmentType) {
			case InvestmentType.BONDS:
				type.setQuantityName("Current Face");
				type.setUnadjustedQuantityName("Original Face");
				type.setDaysToSettle(3);
				hierarchy.setAccrualSecurityEventType(newInvestmentSecurityEventType(InvestmentSecurityEventType.CASH_COUPON_PAYMENT));
				break;
			case InvestmentType.CURRENCY:
				security.setCurrency(true);
				break;
			case InvestmentType.FORWARDS:
				type.setQuantityName("Underlying CCY Notional");
				hierarchy.setNoPaymentOnOpen(true);

				InvestmentSecurity underlyingCurrency = getCurrencyForSymbol(symbol.substring(0, 3));
				security.setUnderlyingSecurity(underlyingCurrency);
				instrument.setUnderlyingInstrument(underlyingCurrency.getInstrument());
				if ("BRL".equals(underlyingCurrency.getSymbol()) || "JPY".equals(underlyingCurrency.getSymbol()) || "SEK".equals(underlyingCurrency.getSymbol())) {
					instrument.setCostCalculator(InvestmentNotionalCalculatorTypes.MONEY_HALF_UP_DIVIDE);
				}
				break;
			case InvestmentType.FUTURES:
				hierarchy.setNoPaymentOnOpen(true);
				type.setDaysToSettle(1);
				break;
			case InvestmentType.STOCKS:
				type.setQuantityName("Shares");
				break;
			case InvestmentType.SWAPS:
				type.setQuantityName("Notional");
				type.setUnadjustedQuantityName("Notional");
				type.setDaysToSettle(1);
				hierarchy.setNoPaymentOnOpen(true);
				hierarchy.setAssignmentAllowed(true);
				// NOTE: keep same as bonds for simplicity of setup
				hierarchy.setAccrualSecurityEventType(newInvestmentSecurityEventType(InvestmentSecurityEventType.CASH_COUPON_PAYMENT));
				break;
		}

		return security;
	}


	public static InvestmentSecurity newInvestmentSecurity(int id, String symbol, String investmentType, String ccyDenominationSymbol) {
		return newInvestmentSecurity(id, symbol, investmentType, ccyDenominationSymbol, null);
	}


	private static InvestmentSecurity newInvestmentSecurity(int id, String symbol, String investmentType, String ccyDenominationSymbol, String identifierPrefix) {
		BigDecimal priceMultiplier = new BigDecimal(100); // default
		switch (investmentType) {
			case InvestmentType.BONDS:
				priceMultiplier = new BigDecimal("0.01");
				break;
			case InvestmentType.FORWARDS:
			case InvestmentType.STOCKS:
				priceMultiplier = BigDecimal.ONE;
				break;
		}
		return newInvestmentSecurity(id, symbol, investmentType, priceMultiplier, ccyDenominationSymbol, identifierPrefix);
	}


	public static InvestmentSecurity newInvestmentSecurity(int id, String symbol, String investmentType) {
		return newInvestmentSecurity(id, symbol, investmentType, "USD");
	}


	public static InvestmentSecurity newInvestmentSecurityWithPrefix(int id, String symbol, String investmentType, String identifierPrefix) {
		return newInvestmentSecurity(id, symbol, investmentType, "USD", identifierPrefix);
	}


	private static Map<String, InvestmentSecurity> currencyMap;


	public static InvestmentSecurity getCurrencyForSymbol(String symbol) {
		if (currencyMap == null) {
			currencyMap = new HashMap<>();
			currencyMap.put("AUD", InvestmentSecurityBuilder.newAUD());
			currencyMap.put("BRL", InvestmentSecurityBuilder.newBRL());
			currencyMap.put("CAD", InvestmentSecurityBuilder.newCAD());
			currencyMap.put("CHF", InvestmentSecurityBuilder.newCHF());
			currencyMap.put("CNH", InvestmentSecurityBuilder.newCNH());
			currencyMap.put("DKK", InvestmentSecurityBuilder.newDKK());
			currencyMap.put("EUR", InvestmentSecurityBuilder.newEUR());
			currencyMap.put("GBP", InvestmentSecurityBuilder.newGBP());
			currencyMap.put("HKD", InvestmentSecurityBuilder.newHKD());
			currencyMap.put("IDR", InvestmentSecurityBuilder.newIDR());
			currencyMap.put("ILS", InvestmentSecurityBuilder.newILS());
			currencyMap.put("INR", InvestmentSecurityBuilder.newINR());
			currencyMap.put("JPY", InvestmentSecurityBuilder.newJPY());
			currencyMap.put("KRW", InvestmentSecurityBuilder.newKRW());
			currencyMap.put("MXN", InvestmentSecurityBuilder.newMXN());
			currencyMap.put("NOK", InvestmentSecurityBuilder.newNOK());
			currencyMap.put("RUB", InvestmentSecurityBuilder.newRUB());
			currencyMap.put("SEK", InvestmentSecurityBuilder.newSEK());
			currencyMap.put("THB", InvestmentSecurityBuilder.newTHB());
			currencyMap.put("TRY", InvestmentSecurityBuilder.newTRY());
			currencyMap.put("TWD", InvestmentSecurityBuilder.newTWD());
			currencyMap.put("USD", InvestmentSecurityBuilder.newUSD());
			currencyMap.put("ZAR", InvestmentSecurityBuilder.newZAR());
		}

		InvestmentSecurity result = currencyMap.get(symbol);
		ValidationUtils.assertNotNull(result, "Unsupported currency denomination: " + symbol);
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////           InvestmentSecurityEventType          //////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentSecurityEventType newInvestmentSecurityEventType(String eventTypeName) {
		InvestmentSecurityEventType eventType = InvestmentSecurityEventType.ofTypeName(eventTypeName);

		switch (eventTypeName) {
			case InvestmentSecurityEventType.CREDIT_EVENT:
				eventType.setAdditionalSecurityAllowed(true);
				eventType.setPaymentDelayAllowed(true);
				eventType.setDecimalPrecision(15);
				break;
			case InvestmentSecurityEventType.FACTOR_CHANGE:
				eventType.setPaymentDelayAllowed(true);
				eventType.setDecimalPrecision(10);
				break;
			case InvestmentSecurityEventType.CASH_COUPON_PAYMENT:
				eventType.setBeforeSameAsAfter(true);
				eventType.setValuePercent(true);
				eventType.setPaymentDelayAllowed(true);
				eventType.setOnePerEventDate(true);
				eventType.setDecimalPrecision(8);
				break;
			case InvestmentSecurityEventType.CASH_DIVIDEND_PAYMENT:
				eventType.setBeforeSameAsAfter(true);
				eventType.setAdditionalSecurityAllowed(true);
				eventType.setAdditionalSecurityCurrency(true);
				eventType.setEventValueInEventCurrencyUnits(true);
				eventType.setPaymentDelayAllowed(true);
				eventType.setDecimalPrecision(8);
				break;
			case InvestmentSecurityEventType.STOCK_DIVIDEND_PAYMENT:
				eventType.setBeforeSameAsAfter(true);
				eventType.setNewPositionCreated(true);
				eventType.setPaymentDelayAllowed(true);
				eventType.setDecimalPrecision(8);
				break;
			case InvestmentSecurityEventType.DIVIDEND_SCRIP:
				eventType.setBeforeSameAsAfter(true);
				eventType.setAdditionalSecurityAllowed(true);
				eventType.setEventValueInEventCurrencyUnits(true);
				eventType.setPaymentDelayAllowed(true);
				eventType.setActionAllowed(true);
				eventType.setDecimalPrecision(8);
				break;
			case InvestmentSecurityEventType.INTEREST_LEG_PAYMENT:
				eventType.setBeforeSameAsAfter(true);
				eventType.setValuePercent(true);
				eventType.setAdditionalSecurityAllowed(true);
				eventType.setAdditionalSecurityCurrency(true);
				eventType.setPaymentDelayAllowed(true);
				eventType.setOnePerEventDate(true);
				eventType.setDecimalPrecision(8);
				break;
			case InvestmentSecurityEventType.EQUITY_LEG_PAYMENT:
				eventType.setAdditionalSecurityAllowed(true);
				eventType.setAdditionalSecurityCurrency(true);
				eventType.setPaymentDelayAllowed(true);
				eventType.setOnePerEventDate(true);
				eventType.setDecimalPrecision(15);
				break;
			case InvestmentSecurityEventType.STOCK_SPLIT:
				eventType.setNewPositionCreated(true);
				eventType.setOnePerEventDate(true);
				eventType.setDecimalPrecision(8);
				break;
			case InvestmentSecurityEventType.SYMBOL_CHANGE:
				eventType.setBeforeSameAsAfter(true);
				eventType.setAdditionalSecurityAllowed(true);
				eventType.setNewSecurityCreated(true);
				eventType.setOnePerEventDate(true);
				eventType.setDecimalPrecision(0);
				break;
			case InvestmentSecurityEventType.SECURITY_MATURITY:
				eventType.setOnePerEventDate(true);
				eventType.setDecimalPrecision(0);
				break;
			case InvestmentSecurityEventType.STOCK_SPINOFF:
				eventType.setAdditionalSecurityAllowed(true);
				eventType.setNewSecurityCreated(true);
				eventType.setNewPositionCreated(true);
				eventType.setActionAllowed(true);
				eventType.setDecimalPrecision(8);
				break;
			case InvestmentSecurityEventType.RETURN_OF_CAPITAL:
				eventType.setBeforeSameAsAfter(true);
				eventType.setAdditionalSecurityAllowed(true);
				eventType.setAdditionalSecurityCurrency(true);
				eventType.setEventValueInEventCurrencyUnits(true);
				eventType.setPaymentDelayAllowed(true);
				eventType.setDecimalPrecision(8);
				eventType.setNewPositionCreated(true);
				break;
			case InvestmentSecurityEventType.MERGER:
				eventType.setAdditionalSecurityAllowed(true);
				eventType.setNewSecurityCreated(true);
				eventType.setNewPositionCreated(true);
				eventType.setOnePerEventDate(true);
				eventType.setActionAllowed(true);
				eventType.setDecimalPrecision(10);
				break;
			default:
				throw new ValidationException("Unsupported InvestmentSecurityEventType name: " + eventTypeName);
		}

		return eventType;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////              InvestmentSecurityEvent           //////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentSecurityEvent newInvestmentSecurityEvent(InvestmentSecurity security, InvestmentSecurityEventType eventType, String eventDate) {
		InvestmentSecurityEvent event = new InvestmentSecurityEvent();
		event.setSecurity(security);
		event.setType(eventType);
		event.setStatus(eventType.getForceEventStatus());
		event.setEventDate(DateUtils.toDate(eventDate));
		event.setDeclareDate(event.getEventDate());
		event.setRecordDate(event.getEventDate());
		event.setExDate(event.getEventDate());
		return event;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////              InvestmentSecurityEventPayoutTYpe           ////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentSecurityEventPayoutType newInvestmentSecurityEventPayoutType(String payoutTypeName) {
		InvestmentSecurityEventPayoutType type = new InvestmentSecurityEventPayoutType();
		type.setName(payoutTypeName);

		switch (payoutTypeName) {
			case InvestmentSecurityEventPayoutType.CURRENCY:
				type.setId((short) 1);
				type.setTypeCode("CY");
				type.setBeforeSameAsAfter(true);
				type.setAdditionalSecurityCurrency(true);
				type.setEventValueInEventCurrencyUnits(true);
				break;
			case InvestmentSecurityEventPayoutType.CURRENCY_FRANKING_CREDIT:
				type.setId((short) 2);
				type.setTypeCode("CYF");
				type.setBeforeSameAsAfter(true);
				type.setAdditionalSecurityCurrency(true);
				type.setEventValueInEventCurrencyUnits(true);
				break;
			case InvestmentSecurityEventPayoutType.SECURITY_NEW:
				type.setId((short) 3);
				type.setTypeCode("SN");
				type.setNewPositionCreated(true);
				type.setNewSecurityCreated(true);
				break;
			case InvestmentSecurityEventPayoutType.SECURITY_EXISTING:
				type.setId((short) 4);
				type.setTypeCode("SE");
				type.setBeforeSameAsAfter(true);
				break;
			case InvestmentSecurityEventPayoutType.SECURITY_STOCK_DIVIDEND:
				type.setId((short) 5);
				type.setTypeCode("SD");
				type.setBeforeSameAsAfter(true);
				type.setNewPositionCreated(true);
				break;
			case InvestmentSecurityEventPayoutType.SECURITY_STOCK_SPLIT:
				type.setId((short) 6);
				type.setTypeCode("SS");
				type.setNewPositionCreated(true);
				break;
			case InvestmentSecurityEventPayoutType.SECURITY_FRANKING_CREDIT:
				type.setId((short) 7);
				type.setTypeCode("SF");
				type.setBeforeSameAsAfter(true);
				break;
			default:
				throw new ValidationException("Unsupported InvestmentSecurityEventPayoutType name: " + payoutTypeName);
		}

		return type;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////              InvestmentSecurityEventPayout            ///////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentSecurityEventPayout newInvestmentSecurityEventPayout(InvestmentSecurityEvent securityEvent, String payoutType, InvestmentSecurity payoutSecurity) {
		InvestmentSecurityEventPayout payout = newInvestmentSecurityEventPayout(securityEvent, newInvestmentSecurityEventPayoutType(payoutType));
		payout.setPayoutSecurity(payoutSecurity);
		return payout;
	}


	public static InvestmentSecurityEventPayout newInvestmentSecurityEventPayout(InvestmentSecurityEvent securityEvent, String payoutType, FractionalShares fractionalShares) {
		InvestmentSecurityEventPayout payout = newInvestmentSecurityEventPayout(securityEvent, newInvestmentSecurityEventPayoutType(payoutType));
		payout.setFractionalSharesMethod(fractionalShares);
		return payout;
	}


	public static InvestmentSecurityEventPayout newInvestmentSecurityEventPayout(InvestmentSecurityEvent securityEvent, String payoutType, InvestmentSecurity payoutSecurity, FractionalShares fractionalShares) {
		InvestmentSecurityEventPayout payout = newInvestmentSecurityEventPayout(securityEvent, payoutType, payoutSecurity);
		payout.setFractionalSharesMethod(fractionalShares);
		return payout;
	}


	public static InvestmentSecurityEventPayout newInvestmentSecurityEventPayout(InvestmentSecurityEvent securityEvent, InvestmentSecurityEventPayoutType payoutType) {
		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(securityEvent);
		payout.setPayoutSecurity(securityEvent.getSecurity());
		payout.setPayoutType(payoutType);
		payout.setElectionNumber((short) 1);
		payout.setPayoutNumber(payout.getElectionNumber());
		payout.copyBeforeAndAfterEventValues(securityEvent);
		return payout;
	}
}
