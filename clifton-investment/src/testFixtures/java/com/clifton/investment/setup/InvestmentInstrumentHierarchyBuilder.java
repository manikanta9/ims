package com.clifton.investment.setup;

import com.clifton.business.company.BusinessCompanyType;
import com.clifton.business.company.BusinessCompanyTypeBuilder;
import com.clifton.investment.exchange.InvestmentExchangeBuilder;
import com.clifton.investment.instrument.InvestmentPricingFrequencies;
import com.clifton.investment.instrument.calculator.InvestmentM2MCalculatorTypes;

import java.math.BigDecimal;


public class InvestmentInstrumentHierarchyBuilder {

	private InvestmentInstrumentHierarchy investmentInstrumentHierarchy;


	private InvestmentInstrumentHierarchyBuilder(InvestmentInstrumentHierarchy investmentInstrumentHierarchy) {
		this.investmentInstrumentHierarchy = investmentInstrumentHierarchy;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentInstrumentHierarchyBuilder createEmptyInvestmentInstrumentHierarchy() {
		return new InvestmentInstrumentHierarchyBuilder(new InvestmentInstrumentHierarchy());
	}


	public static InvestmentInstrumentHierarchyBuilder createCurrency() {
		InvestmentInstrumentHierarchy investmentInstrumentHierarchy = new InvestmentInstrumentHierarchy();

		investmentInstrumentHierarchy.setName("Currency");
		investmentInstrumentHierarchy.setId((short) 6);
		investmentInstrumentHierarchy.setLabel("Currency");
		investmentInstrumentHierarchy.setDescription("Currency");

		return new InvestmentInstrumentHierarchyBuilder(investmentInstrumentHierarchy);
	}


	public static InvestmentInstrumentHierarchyBuilder createEquities() {
		InvestmentInstrumentHierarchy investmentInstrumentHierarchy = new InvestmentInstrumentHierarchy();

		investmentInstrumentHierarchy.setName("Equities");
		investmentInstrumentHierarchy.setId((short) 12);
		investmentInstrumentHierarchy.setLabel("Equities");
		investmentInstrumentHierarchy.setDescription("Equities");

		return new InvestmentInstrumentHierarchyBuilder(investmentInstrumentHierarchy);
	}


	public static InvestmentInstrumentHierarchyBuilder createPhysicals() {
		InvestmentInstrumentHierarchy investmentInstrumentHierarchy = new InvestmentInstrumentHierarchy();

		investmentInstrumentHierarchy.setName("Physicals");
		investmentInstrumentHierarchy.setId((short) 9);
		investmentInstrumentHierarchy.setParent(InvestmentInstrumentHierarchyBuilder.createCurrency().toInvestmentInstrumentHierarchy());
		investmentInstrumentHierarchy.setInvestmentType(InvestmentTypeBuilder.createCurrency().toInvestmentType());
		investmentInstrumentHierarchy.setName("Physicals");
		investmentInstrumentHierarchy.setLabel("Physicals");
		investmentInstrumentHierarchy.setDescription("Physicals on Currency");
		investmentInstrumentHierarchy.setAssignmentAllowed(true);
		investmentInstrumentHierarchy.setOneSecurityPerInstrument(true);
		investmentInstrumentHierarchy.setInstrumentScreen("Currency");
		investmentInstrumentHierarchy.setCurrency(true);
		investmentInstrumentHierarchy.setM2mCalculator(InvestmentM2MCalculatorTypes.DAILY_OTE_CHANGE);
		investmentInstrumentHierarchy.setPriceMultiplier(BigDecimal.valueOf(1.0000000000));
		investmentInstrumentHierarchy.setInstrumentDeliverableType(InvestmentInstrumentHierarchy.InstrumentDeliverableTypes.NEVER);
		investmentInstrumentHierarchy.setTradingOnEndDateAllowed(true);
		investmentInstrumentHierarchy.setPricingFrequency(InvestmentPricingFrequencies.DAILY);

		return new InvestmentInstrumentHierarchyBuilder(investmentInstrumentHierarchy);
	}


	public static InvestmentInstrumentHierarchyBuilder createFunds() {
		InvestmentInstrumentHierarchy investmentInstrumentHierarchy = new InvestmentInstrumentHierarchy();

		investmentInstrumentHierarchy.setName("Funds");
		investmentInstrumentHierarchy.setId((short) 116);
		investmentInstrumentHierarchy.setParent(InvestmentInstrumentHierarchyBuilder.createEquities().toInvestmentInstrumentHierarchy());
		investmentInstrumentHierarchy.setLabel("Funds");
		investmentInstrumentHierarchy.setDescription("Equity Funds (CEF, ETF, Mutual Funds, etc.)");

		return new InvestmentInstrumentHierarchyBuilder(investmentInstrumentHierarchy);
	}


	public static InvestmentInstrumentHierarchyBuilder createFutures() {
		InvestmentInstrumentHierarchy investmentInstrumentHierarchy = new InvestmentInstrumentHierarchy();

		investmentInstrumentHierarchy.setName("Futures");
		investmentInstrumentHierarchy.setId((short) 13);
		investmentInstrumentHierarchy.setParent(InvestmentInstrumentHierarchyBuilder.createEquities().toInvestmentInstrumentHierarchy());
		investmentInstrumentHierarchy.setLabel("Futures");
		investmentInstrumentHierarchy.setDescription("Futures on Equities");

		return new InvestmentInstrumentHierarchyBuilder(investmentInstrumentHierarchy);
	}


	public static InvestmentInstrumentHierarchyBuilder createCef() {
		InvestmentInstrumentHierarchy investmentInstrumentHierarchy = new InvestmentInstrumentHierarchy();

		investmentInstrumentHierarchy.setName("CEF");
		investmentInstrumentHierarchy.setId((short) 104);
		investmentInstrumentHierarchy.setParent(InvestmentInstrumentHierarchyBuilder.createFunds().toInvestmentInstrumentHierarchy());
		investmentInstrumentHierarchy.setInvestmentType(InvestmentTypeBuilder.createFunds().toInvestmentType());
		investmentInstrumentHierarchy.setLabel("CEF");
		investmentInstrumentHierarchy.setDescription("Closed End Funds is a regulated investment company that issues a fixed number of shares which are listed on a stock market.");
		investmentInstrumentHierarchy.setAssignmentAllowed(true);
		investmentInstrumentHierarchy.setOneSecurityPerInstrument(true);
		investmentInstrumentHierarchy.setInstrumentScreen("Stock");
		investmentInstrumentHierarchy.setPriceMultiplier(BigDecimal.valueOf(1.0000000000));
		investmentInstrumentHierarchy.setBusinessCompanyType(BusinessCompanyTypeBuilder.createRegisteredInvestmentAccount().toBusinessCompanyType());
		investmentInstrumentHierarchy.setInstrumentDeliverableType(InvestmentInstrumentHierarchy.InstrumentDeliverableTypes.NEVER);
		investmentInstrumentHierarchy.setInvestmentTypeSubType(InvestmentTypeSubTypeBuilder.createClosedEndFunds().toInvestmentTypeSubType());
		investmentInstrumentHierarchy.setInvestmentTypeSubType2(InvestmentTypeSubType2Builder.createEquities().toInvestmentTypeSubType2());
		investmentInstrumentHierarchy.setTradingOnEndDateAllowed(true);
		investmentInstrumentHierarchy.setSecurityPriceMultiplierOverrideAllowed(true);
		investmentInstrumentHierarchy.setPricingFrequency(InvestmentPricingFrequencies.DAILY);

		return new InvestmentInstrumentHierarchyBuilder(investmentInstrumentHierarchy);
	}


	public static InvestmentInstrumentHierarchyBuilder createFuturesContractOnStockOrIndex() {
		InvestmentInstrumentHierarchy investmentInstrumentHierarchy = new InvestmentInstrumentHierarchy();

		investmentInstrumentHierarchy.setName("Index");
		investmentInstrumentHierarchy.setId((short) 14);
		investmentInstrumentHierarchy.setParent(InvestmentInstrumentHierarchyBuilder.createFutures().toInvestmentInstrumentHierarchy());
		investmentInstrumentHierarchy.setInvestmentType(InvestmentTypeBuilder.createFutures().toInvestmentType());
		investmentInstrumentHierarchy.setLabel("Index");
		investmentInstrumentHierarchy.setDescription("A futures contract on a stock or financial index. For each index there may be a different multiple for determining the price of the futures contract.");
		investmentInstrumentHierarchy.setAssignmentAllowed(true);
		investmentInstrumentHierarchy.setInstrumentScreen("Future");
		investmentInstrumentHierarchy.setNoPaymentOnOpen(true);
		investmentInstrumentHierarchy.setM2mCalculator(InvestmentM2MCalculatorTypes.DAILY_OTE_CHANGE);
		investmentInstrumentHierarchy.setInstrumentDeliverableType(InvestmentInstrumentHierarchy.InstrumentDeliverableTypes.NEVER);
		investmentInstrumentHierarchy.setInvestmentTypeSubType(InvestmentTypeSubTypeBuilder.createEquitiesFutures().toInvestmentTypeSubType());
		investmentInstrumentHierarchy.setInvestmentTypeSubType2(InvestmentTypeSubType2Builder.createIndex().toInvestmentTypeSubType2());
		investmentInstrumentHierarchy.setTradingOnEndDateAllowed(true);
		investmentInstrumentHierarchy.setPricingFrequency(InvestmentPricingFrequencies.DAILY);

		return new InvestmentInstrumentHierarchyBuilder(investmentInstrumentHierarchy);
	}


	public static InvestmentInstrumentHierarchyBuilder createBenchmarks() {
		InvestmentInstrumentHierarchy investmentInstrumentHierarchy = new InvestmentInstrumentHierarchy();

		investmentInstrumentHierarchy.setId((short) 1);
		investmentInstrumentHierarchy.setName("Benchmarks");
		investmentInstrumentHierarchy.setLabel("Benchmarks");
		investmentInstrumentHierarchy.setDescription("Holds benchmarks (allocations of indices) or indices: neither can be traded.");

		return new InvestmentInstrumentHierarchyBuilder(investmentInstrumentHierarchy);
	}


	public static InvestmentInstrumentHierarchyBuilder createIndices() {
		InvestmentInstrumentHierarchy investmentInstrumentHierarchy = new InvestmentInstrumentHierarchy();

		investmentInstrumentHierarchy.setId((short) 368);
		investmentInstrumentHierarchy.setName("Indices");
		investmentInstrumentHierarchy.setParent(InvestmentInstrumentHierarchyBuilder.createBenchmarks().toInvestmentInstrumentHierarchy());
		investmentInstrumentHierarchy.setInvestmentType(InvestmentTypeBuilder.createBenchmarks().toInvestmentType());
		investmentInstrumentHierarchy.setLabel("Indices");
		investmentInstrumentHierarchy.setDescription("Listed and unlisted indices, such as SPX, blended benchmarks, or manual indices and excludes economic indicators, credit, and dollar indices.");

		return new InvestmentInstrumentHierarchyBuilder(investmentInstrumentHierarchy);
	}


	public static InvestmentInstrumentHierarchyBuilder createListed() {
		InvestmentInstrumentHierarchy investmentInstrumentHierarchy = new InvestmentInstrumentHierarchy();

		investmentInstrumentHierarchy.setId((short) 369);
		investmentInstrumentHierarchy.setName("Listed");
		investmentInstrumentHierarchy.setParent(InvestmentInstrumentHierarchyBuilder.createIndices().toInvestmentInstrumentHierarchy());
		investmentInstrumentHierarchy.setInvestmentType(InvestmentTypeBuilder.createBenchmarks().toInvestmentType());
		investmentInstrumentHierarchy.setLabel("Listed");
		investmentInstrumentHierarchy.setDescription("Listed indices that have \"Index\" as yellow key (market data sector), such as SPX, and excludes economic indicators, credit and dollar indices.");

		return new InvestmentInstrumentHierarchyBuilder(investmentInstrumentHierarchy);
	}


	public static InvestmentInstrumentHierarchyBuilder createPricedDaily() {
		InvestmentInstrumentHierarchy investmentInstrumentHierarchy = new InvestmentInstrumentHierarchy();

		investmentInstrumentHierarchy.setId((short) 97);
		investmentInstrumentHierarchy.setName("Priced Daily");
		investmentInstrumentHierarchy.setParent(InvestmentInstrumentHierarchyBuilder.createListed().toInvestmentInstrumentHierarchy());
		investmentInstrumentHierarchy.setInvestmentType(InvestmentTypeBuilder.createBenchmarks().toInvestmentType());
		investmentInstrumentHierarchy.setLabel("Priced Daily");
		investmentInstrumentHierarchy.setDescription("Listed indices that have \"Index\" as yellow key (market data sector), such as SPX, and excludes economic indicators, credit and dollar indices; updated daily.");
		investmentInstrumentHierarchy.setAssignmentAllowed(true);
		investmentInstrumentHierarchy.setOneSecurityPerInstrument(true);
		investmentInstrumentHierarchy.setInstrumentScreen("Benchmark");
		investmentInstrumentHierarchy.setTradingDisallowed(true);
		investmentInstrumentHierarchy.setPriceMultiplier(BigDecimal.valueOf(1.0000000000));
		investmentInstrumentHierarchy.setDefaultExchange(InvestmentExchangeBuilder.createCustomExchange().toInvestmentExchange());
		investmentInstrumentHierarchy.setInstrumentDeliverableType(InvestmentInstrumentHierarchy.InstrumentDeliverableTypes.NEVER);
		investmentInstrumentHierarchy.setInvestmentTypeSubType(InvestmentTypeSubTypeBuilder.createIndices().toInvestmentTypeSubType());
		investmentInstrumentHierarchy.setInvestmentTypeSubType2(InvestmentTypeSubType2Builder.createListed().toInvestmentTypeSubType2());
		investmentInstrumentHierarchy.setPricingFrequency(InvestmentPricingFrequencies.DAILY_FLEXIBLE);

		return new InvestmentInstrumentHierarchyBuilder(investmentInstrumentHierarchy);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentHierarchyBuilder withId(short id) {
		getInvestmentInstrumentHierarchy().setId(id);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withName(String name) {
		getInvestmentInstrumentHierarchy().setName(name);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withDescription(String description) {
		getInvestmentInstrumentHierarchy().setDescription(description);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withLabel(String label) {
		getInvestmentInstrumentHierarchy().setLabel(label);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withParent(InvestmentInstrumentHierarchy parent) {
		getInvestmentInstrumentHierarchy().setParent(parent);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withInvestmentType(InvestmentType type) {
		getInvestmentInstrumentHierarchy().setInvestmentType(type);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withAssignmentAllowed(boolean assignmentAllowed) {
		getInvestmentInstrumentHierarchy().setAssignmentAllowed(assignmentAllowed);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withOneSecurityPerInstrument(boolean oneSecurityPerInstrument) {
		getInvestmentInstrumentHierarchy().setOneSecurityPerInstrument(oneSecurityPerInstrument);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withInstrumentScreen(String instrumentScreen) {
		getInvestmentInstrumentHierarchy().setInstrumentScreen(instrumentScreen);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withPriceMultiplier(BigDecimal priceMultiplier) {
		getInvestmentInstrumentHierarchy().setPriceMultiplier(priceMultiplier);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withBusinessCompanyType(BusinessCompanyType businessCompanyType) {
		getInvestmentInstrumentHierarchy().setBusinessCompanyType(businessCompanyType);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withInstrumentDeliverableType(InvestmentInstrumentHierarchy.InstrumentDeliverableTypes instrumentDeliverableTypes) {
		getInvestmentInstrumentHierarchy().setInstrumentDeliverableType(instrumentDeliverableTypes);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withInvestmentTypeSubType(InvestmentTypeSubType investmentTypeSubType) {
		getInvestmentInstrumentHierarchy().setInvestmentTypeSubType(investmentTypeSubType);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withInvestmentTypeSubType2(InvestmentTypeSubType2 investmentTypeSubType2) {
		getInvestmentInstrumentHierarchy().setInvestmentTypeSubType2(investmentTypeSubType2);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withTradingOnEndDateAllowed(boolean tradingOnEndDateAllowed) {
		getInvestmentInstrumentHierarchy().setTradingOnEndDateAllowed(tradingOnEndDateAllowed);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withSecurityPriceMultiplierOverrideAllowed(boolean securityPriceMultiplierOverrideAllowed) {
		getInvestmentInstrumentHierarchy().setSecurityPriceMultiplierOverrideAllowed(securityPriceMultiplierOverrideAllowed);
		return this;
	}


	public InvestmentInstrumentHierarchyBuilder withPricingFrequency(InvestmentPricingFrequencies investmentPricingFrequencies) {
		getInvestmentInstrumentHierarchy().setPricingFrequency(investmentPricingFrequencies);
		return this;
	}


	public InvestmentInstrumentHierarchy toInvestmentInstrumentHierarchy() {
		return this.investmentInstrumentHierarchy;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentInstrumentHierarchy getInvestmentInstrumentHierarchy() {
		return this.investmentInstrumentHierarchy;
	}
}
