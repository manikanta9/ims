package com.clifton.investment.setup;

public class InvestmentTypeSubType2Builder {

	private InvestmentTypeSubType2 investmentTypeSubType2;


	private InvestmentTypeSubType2Builder(InvestmentTypeSubType2 investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentTypeSubType2Builder createEquities() {
		InvestmentTypeSubType2 investmentTypeSubType2 = new InvestmentTypeSubType2();

		investmentTypeSubType2.setId((short) 36);
		investmentTypeSubType2.setInvestmentType(InvestmentTypeBuilder.createFunds().toInvestmentType());
		investmentTypeSubType2.setName("Equities");
		investmentTypeSubType2.setDescription("Equities Asset Class");

		return new InvestmentTypeSubType2Builder(investmentTypeSubType2);
	}


	public static InvestmentTypeSubType2Builder createIndex() {
		InvestmentTypeSubType2 investmentTypeSubType2 = new InvestmentTypeSubType2();

		investmentTypeSubType2.setId((short) 27);
		investmentTypeSubType2.setInvestmentType(InvestmentTypeBuilder.createFutures().toInvestmentType());
		investmentTypeSubType2.setName("Index");
		investmentTypeSubType2.setDescription("Index");

		return new InvestmentTypeSubType2Builder(investmentTypeSubType2);
	}


	public static InvestmentTypeSubType2Builder createListed() {
		InvestmentTypeSubType2 investmentTypeSubType2 = new InvestmentTypeSubType2();

		investmentTypeSubType2.setId((short) 40);
		investmentTypeSubType2.setInvestmentType(InvestmentTypeBuilder.createBenchmarks().toInvestmentType());
		investmentTypeSubType2.setName("Listed");
		investmentTypeSubType2.setDescription("Listed");

		return new InvestmentTypeSubType2Builder(investmentTypeSubType2);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentTypeSubType2Builder withId(short id) {
		getInvestmentTypeSubType2().setId(id);
		return this;
	}


	public InvestmentTypeSubType2Builder withName(String name) {
		getInvestmentTypeSubType2().setName(name);
		return this;
	}


	public InvestmentTypeSubType2Builder withDescription(String description) {
		getInvestmentTypeSubType2().setDescription(description);
		return this;
	}


	public InvestmentTypeSubType2Builder withInvestmentType(InvestmentType investmentType) {
		getInvestmentTypeSubType2().setInvestmentType(investmentType);
		return this;
	}


	public InvestmentTypeSubType2 toInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentTypeSubType2 getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}
}
