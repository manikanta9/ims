package com.clifton.investment.setup.group;

import com.clifton.system.condition.SystemConditionBuilder;


public class InvestmentGroupBuilder {

	private InvestmentGroup investmentGroup;


	private InvestmentGroupBuilder(InvestmentGroup investmentGroup) {
		this.investmentGroup = investmentGroup;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentGroupBuilder createFixContracts() {
		InvestmentGroup investmentGroup = new InvestmentGroup();

		investmentGroup.setId((short) 6);
		investmentGroup.setName("FIX Contracts");
		investmentGroup.setDescription("These contracts are traded through FIX.");
		investmentGroup.setEntityModifyCondition(SystemConditionBuilder.createUserIsAdminOrGroupDataQty().toSystemCondition());
		investmentGroup.setLevelsDeep(1);
		investmentGroup.setSystemDefined(true);

		return new InvestmentGroupBuilder(investmentGroup);
	}


	public static InvestmentGroupBuilder createSwapsCDSCleared() {
		InvestmentGroup investmentGroup = new InvestmentGroup();

		investmentGroup.setId((short) 73);
		investmentGroup.setName("Swaps - CDS - Cleared");
		investmentGroup.setDescription("Centrally Cleared Credit Default Swaps ONLY");
		investmentGroup.setEntityModifyCondition(SystemConditionBuilder.createUserIsAdminOrGroupDataQty().toSystemCondition());
		investmentGroup.setLevelsDeep(1);
		investmentGroup.setLeafAssignmentOnly(true);
		investmentGroup.setTradableSecurityOnly(true);
		investmentGroup.setSystemDefined(true);

		return new InvestmentGroupBuilder(investmentGroup);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentGroupBuilder withId(short id) {
		getInvestmentGroup().setId(id);
		return this;
	}


	public InvestmentGroupBuilder withName(String name) {
		getInvestmentGroup().setName(name);
		return this;
	}


	public InvestmentGroupBuilder withDescription(String description) {
		getInvestmentGroup().setDescription(description);
		return this;
	}


	public InvestmentGroup toInvestmentGroup() {
		return this.investmentGroup;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentGroup getInvestmentGroup() {
		return this.investmentGroup;
	}
}
