package com.clifton.investment.setup;

public class InvestmentTypeBuilder {

	private InvestmentType investmentType;


	private InvestmentTypeBuilder(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentTypeBuilder createEmptyInvestmentType() {
		return new InvestmentTypeBuilder(new InvestmentType());
	}


	/**
	 * Creates an {@link InvestmentType} for the provided type name. Values of the type
	 * are set depending on the type name provided.
	 */
	public static InvestmentTypeBuilder forInvestmentType(String investmentType) {
		InvestmentTypeBuilder builder = new InvestmentTypeBuilder(new InvestmentType())
				.withId((short) 1)
				.withName(investmentType)
				.withQuantityDecimalPrecision((short) 0);

		switch (investmentType) {
			case InvestmentType.BONDS:
				builder.withQuantityName("Current Face")
						.withUnadjustedQuantityName("Original Face")
						.withDaysToSettle(3)
						.withQuantityDecimalPrecision((short) 2);
				break;
			case InvestmentType.FUTURES:
			case InvestmentType.OPTIONS:
				builder.withQuantityName("Contracts")
						.withUnadjustedQuantityName("Contracts");
				break;
			case InvestmentType.STOCKS:
			case InvestmentType.INDEX:
				builder.withQuantityName("Shares")
						.withUnadjustedQuantityName("Shares");
				break;
			case InvestmentType.SWAPS:
				builder.withQuantityName("Notional")
						.withUnadjustedQuantityName("Notional")
						.withDaysToSettle(1)
						.withQuantityDecimalPrecision((short) 15);
				break;
			case InvestmentType.FORWARDS:
			case InvestmentType.CURRENCY:
			case InvestmentType.CASH:
				builder.withQuantityName("Amount")
						.withUnadjustedQuantityName("Amount")
						.withQuantityDecimalPrecision((short) 2);
				break;
			default:
				builder.withQuantityName("Quantity")
						.withUnadjustedQuantityName("Quantity");
		}

		return builder;
	}


	public static InvestmentTypeBuilder createFunds() {
		InvestmentType investmentType = new InvestmentType();

		investmentType.setId((short) 6);
		investmentType.setName("Funds");
		investmentType.setDescription("Funds: ETF, CEF, Mutual Funds, etc.");
		investmentType.setDaysToSettle(3);
		investmentType.setQuantityName("Shares");

		return new InvestmentTypeBuilder(investmentType);
	}


	public static InvestmentTypeBuilder createFutures() {
		InvestmentType investmentType = new InvestmentType();

		investmentType.setId((short) 4);
		investmentType.setName("Futures");
		investmentType.setDescription("Futures...");
		investmentType.setDaysToSettle(0);
		investmentType.setQuantityName("Contracts");

		return new InvestmentTypeBuilder(investmentType);
	}


	public static InvestmentTypeBuilder createBenchmarks() {
		InvestmentType investmentType = new InvestmentType();

		investmentType.setId((short) 5);
		investmentType.setName("Benchmarks");
		investmentType.setDescription("Securities within the system which cannot be traded, but can be utilized as an underlying on a tradeable instrument and/or a tool to calculate performance. Benchmarks within the system range from physical commodities, client liabilities, generic securities, indices, and etc.");
		investmentType.setDaysToSettle(1);
		investmentType.setQuantityName("Shares");

		return new InvestmentTypeBuilder(investmentType);
	}


	public static InvestmentTypeBuilder createCurrency() {
		InvestmentType investmentType = new InvestmentType();

		investmentType.setId((short) 2);
		investmentType.setName("Currency");
		investmentType.setDescription("Spot (Phycical) Currencies.");
		investmentType.setDaysToSettle(2);
		investmentType.setQuantityName("Amount");
		investmentType.setQuantityDecimalPrecision((short) 2);

		return new InvestmentTypeBuilder(investmentType);
	}


	public static InvestmentTypeBuilder createStocks() {
		InvestmentType investmentType = new InvestmentType();

		investmentType.setId((short) 9);
		investmentType.setName("Stocks");
		investmentType.setDescription("Stocks...");
		investmentType.setPhysicalSecurity(true);
		investmentType.setUnadjustedQuantityName("Shares");
		investmentType.setQuantityName("Shares");
		investmentType.setQuantityDecimalPrecision((short) 0);
		investmentType.setCostFieldName("Cost");
		investmentType.setCostBasisFieldName("Cost Basis");
		investmentType.setNotionalFieldName("Maket Value");
		investmentType.setMarketValueFieldName("Market Value");
		investmentType.setDaysToSettle(2);
		investmentType.setQuantityName("Contracts");

		return new InvestmentTypeBuilder(investmentType);
	}


	public static InvestmentTypeBuilder createSwaps() {
		InvestmentType investmentType = new InvestmentType();

		investmentType.setId((short) 10);
		investmentType.setName("Swaps");
		investmentType.setDescription("Securities through which two parties exchange financial instruments including Over-The-Counter and Centrally Cleared Swaps.");
		investmentType.setDaysToSettle(1);
		investmentType.setQuantityName("Units");
		investmentType.setUnadjustedQuantityName("Units");
		investmentType.setQuantityDecimalPrecision((short) 15);
		investmentType.setCostBasisFieldName("Original Notional");
		investmentType.setNotionalFieldName("Current Notional");
		investmentType.setMarketValueFieldName("Gain / Loss");

		return new InvestmentTypeBuilder(investmentType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentTypeBuilder withId(short id) {
		getInvestmentType().setId(id);
		return this;
	}


	public InvestmentTypeBuilder withName(String name) {
		getInvestmentType().setName(name);
		return this;
	}


	public InvestmentTypeBuilder withDescription(String description) {
		getInvestmentType().setDescription(description);
		return this;
	}


	public InvestmentTypeBuilder withQuantityDecimalPrecision(short decimalPrecision) {
		getInvestmentType().setQuantityDecimalPrecision(decimalPrecision);
		return this;
	}


	public InvestmentTypeBuilder withQuantityName(String quantityName) {
		getInvestmentType().setQuantityName(quantityName);
		return this;
	}


	public InvestmentTypeBuilder withUnadjustedQuantityName(String unadjustedQuantityName) {
		getInvestmentType().setUnadjustedQuantityName(unadjustedQuantityName);
		return this;
	}


	public InvestmentTypeBuilder withDaysToSettle(int daysToSettle) {
		getInvestmentType().setDaysToSettle(daysToSettle);
		return this;
	}


	public InvestmentType toInvestmentType() {
		return this.investmentType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentType getInvestmentType() {
		return this.investmentType;
	}
}
