package com.clifton.investment.setup;

public class InvestmentTypeSubTypeBuilder {

	private InvestmentTypeSubType investmentTypeSubType;


	private InvestmentTypeSubTypeBuilder(InvestmentTypeSubType investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentTypeSubTypeBuilder createClosedEndFunds() {
		InvestmentTypeSubType investmentTypeSubType = new InvestmentTypeSubType();

		investmentTypeSubType.setId((short) 10);
		investmentTypeSubType.setInvestmentType(InvestmentTypeBuilder.createFunds().toInvestmentType());
		investmentTypeSubType.setName("Closed End Funds");
		investmentTypeSubType.setDescription("Closed End Funds (CEF)");

		return new InvestmentTypeSubTypeBuilder(investmentTypeSubType);
	}


	public static InvestmentTypeSubTypeBuilder createEquitiesFutures() {
		InvestmentTypeSubType investmentTypeSubType = new InvestmentTypeSubType();

		investmentTypeSubType.setId((short) 24);
		investmentTypeSubType.setInvestmentType(InvestmentTypeBuilder.createFutures().toInvestmentType());
		investmentTypeSubType.setName("Equities");
		investmentTypeSubType.setDescription("Equity Futures");

		return new InvestmentTypeSubTypeBuilder(investmentTypeSubType);
	}


	public static InvestmentTypeSubTypeBuilder createIndices() {
		InvestmentTypeSubType investmentTypeSubType = new InvestmentTypeSubType();

		investmentTypeSubType.setId((short) 56);
		investmentTypeSubType.setInvestmentType(InvestmentTypeBuilder.createBenchmarks().toInvestmentType());
		investmentTypeSubType.setName("Indices");
		investmentTypeSubType.setDescription("Indices");

		return new InvestmentTypeSubTypeBuilder(investmentTypeSubType);
	}


	public static InvestmentTypeSubTypeBuilder createOptionsOnIndices() {
		InvestmentTypeSubType investmentTypeSubType = new InvestmentTypeSubType();

		investmentTypeSubType.setId((short) 34);
		investmentTypeSubType.setInvestmentType(InvestmentTypeBuilder.forInvestmentType(InvestmentType.OPTIONS).toInvestmentType());
		investmentTypeSubType.setName("Options On Indices");
		investmentTypeSubType.setDescription("Options that grant the holder the right, but not the obligation, to buy or sell an Index security at a specified price during a specified period of time.");

		return new InvestmentTypeSubTypeBuilder(investmentTypeSubType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentTypeSubTypeBuilder withId(short id) {
		getInvestmentTypeSubType().setId(id);
		return this;
	}


	public InvestmentTypeSubTypeBuilder withName(String name) {
		getInvestmentTypeSubType().setName(name);
		return this;
	}


	public InvestmentTypeSubTypeBuilder withDescription(String description) {
		getInvestmentTypeSubType().setDescription(description);
		return this;
	}


	public InvestmentTypeSubTypeBuilder withInvestmentType(InvestmentType investmentType) {
		getInvestmentTypeSubType().setInvestmentType(investmentType);
		return this;
	}


	public InvestmentTypeSubType toInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentTypeSubType getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}
}
