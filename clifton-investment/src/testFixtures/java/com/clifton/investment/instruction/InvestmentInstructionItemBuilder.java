package com.clifton.investment.instruction;

import com.clifton.investment.instruction.group.InvestmentInstructionItemGroup;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;


public class InvestmentInstructionItemBuilder {

	private InvestmentInstructionItem investmentInstructionItem;


	private InvestmentInstructionItemBuilder(InvestmentInstructionItem investmentInstructionItem) {
		this.investmentInstructionItem = investmentInstructionItem;
	}


	public static InvestmentInstructionItemBuilder createEmpty() {
		return new InvestmentInstructionItemBuilder(new InvestmentInstructionItem());
	}


	public InvestmentInstructionItemBuilder withId(Integer id) {
		getInvestmentInstructionItem().setId(id);
		return this;
	}


	public InvestmentInstructionItemBuilder withFkFieldLabel(String fkFieldLabel) {
		getInvestmentInstructionItem().setFkFieldLabel(fkFieldLabel);
		return this;
	}


	public InvestmentInstructionItemBuilder withInstruction(InvestmentInstruction instruction) {
		getInvestmentInstructionItem().setInstruction(instruction);
		return this;
	}


	public InvestmentInstructionItemBuilder withFkFieldId(Integer fkFieldId) {
		getInvestmentInstructionItem().setFkFieldId(fkFieldId);
		return this;
	}


	public InvestmentInstructionItemBuilder withStatus(InvestmentInstructionStatus status) {
		getInvestmentInstructionItem().setStatus(status);
		return this;
	}


	public InvestmentInstructionItemBuilder withItemGroup(InvestmentInstructionItemGroup itemGroup) {
		getInvestmentInstructionItem().setItemGroup(itemGroup);
		return this;
	}


	public InvestmentInstructionItemBuilder withReferencedInvestmentInstructionItem(InvestmentInstructionItem referencedInvestmentInstructionItem) {
		getInvestmentInstructionItem().setReferencedInvestmentInstructionItem(referencedInvestmentInstructionItem);
		return this;
	}


	public InvestmentInstructionItemBuilder asReady() {
		getInvestmentInstructionItem().setReady(true);
		return this;
	}


	private InvestmentInstructionItem getInvestmentInstructionItem() {
		return this.investmentInstructionItem;
	}


	public InvestmentInstructionItem toInvestmentInstructionItem() {
		return this.investmentInstructionItem;
	}
}
