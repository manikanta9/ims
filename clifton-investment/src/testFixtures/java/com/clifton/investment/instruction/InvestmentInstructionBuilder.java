package com.clifton.investment.instruction;

import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;

import java.util.Date;
import java.util.List;


public class InvestmentInstructionBuilder {

	private InvestmentInstruction investmentInstruction;


	private InvestmentInstructionBuilder(InvestmentInstruction investmentInstruction) {
		this.investmentInstruction = investmentInstruction;
	}


	public static InvestmentInstructionBuilder createEmpty() {
		return new InvestmentInstructionBuilder(new InvestmentInstruction());
	}


	public InvestmentInstructionBuilder withId(Integer id) {
		getInvestmentInstruction().setId(id);
		return this;
	}


	public InvestmentInstructionBuilder withDefinition(InvestmentInstructionDefinition definition) {
		getInvestmentInstruction().setDefinition(definition);
		return this;
	}


	public InvestmentInstructionBuilder withRecipientCompany(BusinessCompany recipientCompany) {
		getInvestmentInstruction().setRecipientCompany(recipientCompany);
		return this;
	}


	public InvestmentInstructionBuilder withInstructionDate(Date instructionDate) {
		getInvestmentInstruction().setInstructionDate(instructionDate);
		return this;
	}


	public InvestmentInstructionBuilder withItemList(List<InvestmentInstructionItem> itemList) {
		getInvestmentInstruction().setItemList(itemList);
		return this;
	}


	public InvestmentInstructionBuilder withStatus(InvestmentInstructionStatus status) {
		getInvestmentInstruction().setStatus(status);
		return this;
	}


	public InvestmentInstructionBuilder asAllItemsReady() {
		getInvestmentInstruction().setAllItemsReady(true);
		return this;
	}


	public InvestmentInstructionBuilder asRegenerating() {
		getInvestmentInstruction().setRegenerating(true);
		return this;
	}


	private InvestmentInstruction getInvestmentInstruction() {
		return this.investmentInstruction;
	}


	public InvestmentInstruction toInvestmentInstruction() {
		return this.investmentInstruction;
	}
}
