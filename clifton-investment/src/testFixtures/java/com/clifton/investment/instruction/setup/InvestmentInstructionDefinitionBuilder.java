package com.clifton.investment.instruction.setup;

import com.clifton.business.contact.BusinessContact;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryType;
import com.clifton.report.definition.Report;
import com.clifton.system.bean.SystemBean;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.List;


public class InvestmentInstructionDefinitionBuilder {

	private InvestmentInstructionDefinition investmentInstructionDefinition;


	private InvestmentInstructionDefinitionBuilder(InvestmentInstructionDefinition investmentInstructionDefinition) {
		this.investmentInstructionDefinition = investmentInstructionDefinition;
	}


	public static InvestmentInstructionDefinitionBuilder createEmpty() {
		return new InvestmentInstructionDefinitionBuilder(new InvestmentInstructionDefinition());
	}


	public InvestmentInstructionDefinitionBuilder withId(Integer id) {
		getInvestmentInstructionDefinition().setId(id);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withName(String name) {
		getInvestmentInstructionDefinition().setName(name);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withDescription(String description) {
		getInvestmentInstructionDefinition().setDescription(description);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withCategory(InvestmentInstructionCategory category) {
		getInvestmentInstructionDefinition().setCategory(category);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withReport(Report report) {
		getInvestmentInstructionDefinition().setReport(report);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withWorkflowState(WorkflowState workflowState) {
		getInvestmentInstructionDefinition().setWorkflowState(workflowState);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withWorkflowStatus(WorkflowStatus workflowStatus) {
		getInvestmentInstructionDefinition().setWorkflowStatus(workflowStatus);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withSelectorBean(SystemBean selectorBean) {
		getInvestmentInstructionDefinition().setSelectorBean(selectorBean);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withItemGroupPopulatorBean(SystemBean itemGroupPopulatorBean) {
		getInvestmentInstructionDefinition().setItemGroupPopulatorBean(itemGroupPopulatorBean);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withFieldList(List<InvestmentInstructionDefinitionField> fieldList) {
		getInvestmentInstructionDefinition().setFieldList(fieldList);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withMessageBodyContent(String messageBodyContent) {
		getInvestmentInstructionDefinition().setMessageBodyContent(messageBodyContent);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withMessageSubjectContent(String messageSubjectContent) {
		getInvestmentInstructionDefinition().setMessageSubjectContent(messageSubjectContent);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withContactGroup(InvestmentInstructionContactGroup contactGroup) {
		getInvestmentInstructionDefinition().setContactGroup(contactGroup);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withFromContact(BusinessContact fromContact) {
		getInvestmentInstructionDefinition().setFromContact(fromContact);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withFilenameTemplate(String filenameTemplate) {
		getInvestmentInstructionDefinition().setFilenameTemplate(filenameTemplate);
		return this;
	}


	public InvestmentInstructionDefinitionBuilder withDeliveryType(InvestmentInstructionDeliveryType deliveryType) {
		getInvestmentInstructionDefinition().setDeliveryType(deliveryType);
		return this;
	}


	private InvestmentInstructionDefinition getInvestmentInstructionDefinition() {
		return this.investmentInstructionDefinition;
	}


	public InvestmentInstructionDefinition toInvestmentInstructionDefinition() {
		return this.investmentInstructionDefinition;
	}
}
