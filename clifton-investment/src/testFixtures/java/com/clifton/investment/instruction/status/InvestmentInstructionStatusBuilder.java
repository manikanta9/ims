package com.clifton.investment.instruction.status;

public class InvestmentInstructionStatusBuilder {

	private InvestmentInstructionStatus investmentInstructionStatus;


	private InvestmentInstructionStatusBuilder(InvestmentInstructionStatus investmentInstructionStatus) {
		this.investmentInstructionStatus = investmentInstructionStatus;
	}


	public static InvestmentInstructionStatusBuilder createCompleted() {
		InvestmentInstructionStatusBuilder builder = new InvestmentInstructionStatusBuilder(new InvestmentInstructionStatus());
		builder.withId((short) 6).withName("COMPLETED").withDescription("All children are sent.").withStatusCode((short) 5).asCompleted();
		return builder;
	}


	public static InvestmentInstructionStatusBuilder createOpen() {
		InvestmentInstructionStatusBuilder builder = new InvestmentInstructionStatusBuilder(new InvestmentInstructionStatus());
		builder.withId((short) 2).withName("OPEN").withDescription("No action has been taken.").withStatusCode((short) 1).asSendable();
		return builder;
	}


	public static InvestmentInstructionStatusBuilder createSend() {
		InvestmentInstructionStatusBuilder builder = new InvestmentInstructionStatusBuilder(new InvestmentInstructionStatus());
		builder.withId((short) 3).withName("SEND").withDescription("Queued to be sent.").withStatusCode((short) 2);
		return builder;
	}

	public static InvestmentInstructionStatusBuilder createSending() {
		InvestmentInstructionStatusBuilder builder = new InvestmentInstructionStatusBuilder(new InvestmentInstructionStatus());
		builder.withId((short) 4).withName("SENDING").withDescription("In the process of being sent.").withStatusCode((short) 3);
		return builder;
	}

	public static InvestmentInstructionStatusBuilder createSent() {
		InvestmentInstructionStatusBuilder builder = new InvestmentInstructionStatusBuilder(new InvestmentInstructionStatus());
		builder.withId((short) 5).withName("SENT").withDescription("Has been sent.").withStatusCode((short) 4);
		return builder;
	}


	public InvestmentInstructionStatusBuilder withId(Short id) {
		getInvestmentInstructionStatus().setId(id);
		return this;
	}


	public InvestmentInstructionStatusBuilder withName(String name) {
		getInvestmentInstructionStatus().setName(name);
		return this;
	}


	public InvestmentInstructionStatusBuilder withDescription(String description) {
		getInvestmentInstructionStatus().setDescription(description);
		return this;
	}


	public InvestmentInstructionStatusBuilder withStatusCode(Short statusCode) {
		getInvestmentInstructionStatus().setStatusCode(statusCode);
		return this;
	}


	public InvestmentInstructionStatusBuilder asCompleted() {
		getInvestmentInstructionStatus().setCompleted(true);
		return this;
	}


	public InvestmentInstructionStatusBuilder asFail() {
		getInvestmentInstructionStatus().setFail(true);
		return this;
	}


	public InvestmentInstructionStatusBuilder asSendable() {
		getInvestmentInstructionStatus().setSendable(true);
		return this;
	}


	public InvestmentInstructionStatusBuilder asCancel() {
		getInvestmentInstructionStatus().setCancel(true);
		return this;
	}


	private InvestmentInstructionStatus getInvestmentInstructionStatus() {
		return this.investmentInstructionStatus;
	}


	public InvestmentInstructionStatus toInvestmentInstructionStatus() {
		return this.investmentInstructionStatus;
	}
}
