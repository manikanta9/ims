package com.clifton.investment.instruction.delivery;

import com.clifton.business.company.BusinessCompany;


public class InvestmentInstructionDeliveryBuilder {

	private InvestmentInstructionDelivery investmentInstructionDelivery;


	private InvestmentInstructionDeliveryBuilder(InvestmentInstructionDelivery investmentInstructionDelivery) {
		this.investmentInstructionDelivery = investmentInstructionDelivery;
	}


	public static InvestmentInstructionDeliveryBuilder createEmpty() {
		return new InvestmentInstructionDeliveryBuilder(new InvestmentInstructionDelivery());
	}


	public InvestmentInstructionDeliveryBuilder withDeliveryCompany(BusinessCompany deliveryCompany) {
		getInvestmentInstructionDelivery().setDeliveryCompany(deliveryCompany);
		return this;
	}


	public InvestmentInstructionDeliveryBuilder withDeliveryAccountNumber(String deliveryAccountNumber) {
		getInvestmentInstructionDelivery().setDeliveryAccountNumber(deliveryAccountNumber);
		return this;
	}


	public InvestmentInstructionDeliveryBuilder withDeliveryAccountName(String deliveryAccountName) {
		getInvestmentInstructionDelivery().setDeliveryAccountName(deliveryAccountName);
		return this;
	}


	public InvestmentInstructionDeliveryBuilder withDeliveryABA(String deliveryABA) {
		getInvestmentInstructionDelivery().setDeliveryABA(deliveryABA);
		return this;
	}


	public InvestmentInstructionDeliveryBuilder withDeliverySwiftCode(String deliverySwiftCode) {
		getInvestmentInstructionDelivery().setDeliverySwiftCode(deliverySwiftCode);
		return this;
	}


	public InvestmentInstructionDeliveryBuilder withDeliveryText(String deliveryText) {
		getInvestmentInstructionDelivery().setDeliveryText(deliveryText);
		return this;
	}


	public InvestmentInstructionDeliveryBuilder withDeliveryForFurtherCredit(String deliveryForFurtherCredit) {
		getInvestmentInstructionDelivery().setDeliveryForFurtherCredit(deliveryForFurtherCredit);
		return this;
	}


	public InvestmentInstructionDeliveryBuilder withSeparateWireForCollateral(boolean separateWireForCollateral) {
		getInvestmentInstructionDelivery().setSeparateWireForCollateral(separateWireForCollateral);
		return this;
	}


	private InvestmentInstructionDelivery getInvestmentInstructionDelivery() {
		return this.investmentInstructionDelivery;
	}


	public InvestmentInstructionDelivery toInvestmentInstructionDelivery() {
		return this.investmentInstructionDelivery;
	}
}
