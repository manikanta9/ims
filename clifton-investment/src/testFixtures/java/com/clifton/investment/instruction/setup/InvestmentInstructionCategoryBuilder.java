package com.clifton.investment.instruction.setup;

import com.clifton.business.contact.BusinessContact;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.schema.SystemTable;


public class InvestmentInstructionCategoryBuilder {

	private InvestmentInstructionCategory investmentInstructionCategory;


	private InvestmentInstructionCategoryBuilder(InvestmentInstructionCategory investmentInstructionCategory) {
		this.investmentInstructionCategory = investmentInstructionCategory;
	}


	public static InvestmentInstructionCategoryBuilder createEmpty() {
		return new InvestmentInstructionCategoryBuilder(new InvestmentInstructionCategory());
	}


	public InvestmentInstructionCategoryBuilder withId(Short id) {
		getInvestmentInstructionCategory().setId(id);
		return this;
	}


	public InvestmentInstructionCategoryBuilder withTable(SystemTable table) {
		getInvestmentInstructionCategory().setTable(table);
		return this;
	}


	public InvestmentInstructionCategoryBuilder withName(String name) {
		getInvestmentInstructionCategory().setName(name);
		return this;
	}


	public InvestmentInstructionCategoryBuilder withDescription(String description) {
		getInvestmentInstructionCategory().setDescription(description);
		return this;
	}


	public InvestmentInstructionCategoryBuilder asRequired() {
		getInvestmentInstructionCategory().setRequired(true);
		return this;
	}


	public InvestmentInstructionCategoryBuilder asCash() {
		getInvestmentInstructionCategory().setCash(true);
		return this;
	}


	public InvestmentInstructionCategoryBuilder asClient() {
		getInvestmentInstructionCategory().setClient(true);
		return this;
	}


	public InvestmentInstructionCategoryBuilder withSelectorBeanType(SystemBeanType selectorBeanType) {
		getInvestmentInstructionCategory().setSelectorBeanType(selectorBeanType);
		return this;
	}


	public InvestmentInstructionCategoryBuilder withDateLabel(String dateLabel) {
		getInvestmentInstructionCategory().setDateLabel(dateLabel);
		return this;
	}


	public InvestmentInstructionCategoryBuilder withFromContact(BusinessContact fromContact) {
		getInvestmentInstructionCategory().setFromContact(fromContact);
		return this;
	}


	private InvestmentInstructionCategory getInvestmentInstructionCategory() {
		return this.investmentInstructionCategory;
	}


	public InvestmentInstructionCategory toInvestmentInstructionCategory() {
		return this.investmentInstructionCategory;
	}
}
