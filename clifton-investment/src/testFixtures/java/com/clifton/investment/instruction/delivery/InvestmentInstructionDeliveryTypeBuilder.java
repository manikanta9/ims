package com.clifton.investment.instruction.delivery;

public class InvestmentInstructionDeliveryTypeBuilder {

	private InvestmentInstructionDeliveryType investmentInstructionDeliveryType;


	private InvestmentInstructionDeliveryTypeBuilder(InvestmentInstructionDeliveryType investmentInstructionDeliveryType) {
		this.investmentInstructionDeliveryType = investmentInstructionDeliveryType;
	}


	public static InvestmentInstructionDeliveryTypeBuilder createCashCollateralBilateralOTC() {
		InvestmentInstructionDeliveryTypeBuilder builder = new InvestmentInstructionDeliveryTypeBuilder(new InvestmentInstructionDeliveryType());
		builder.withId((short) 9).withName("Cash Collateral – Bilateral OTC").withDescription("Cash Collateral – Bilateral OTC");
		return builder;
	}


	public InvestmentInstructionDeliveryTypeBuilder withId(Short id) {
		getInvestmentInstructionDeliveryType().setId(id);
		return this;
	}


	public InvestmentInstructionDeliveryTypeBuilder withName(String name) {
		getInvestmentInstructionDeliveryType().setName(name);
		return this;
	}


	public InvestmentInstructionDeliveryTypeBuilder withDescription(String description) {
		getInvestmentInstructionDeliveryType().setDescription(description);
		return this;
	}


	public InvestmentInstructionDeliveryTypeBuilder asDefaultDeliveryType() {
		getInvestmentInstructionDeliveryType().setDefaultDeliveryType(true);
		return this;
	}


	private InvestmentInstructionDeliveryType getInvestmentInstructionDeliveryType() {
		return this.investmentInstructionDeliveryType;
	}


	public InvestmentInstructionDeliveryType toInvestmentInstructionDeliveryType() {
		return this.investmentInstructionDeliveryType;
	}
}
