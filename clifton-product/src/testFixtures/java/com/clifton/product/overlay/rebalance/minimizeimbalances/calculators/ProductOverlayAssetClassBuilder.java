package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.core.util.StringUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


public class ProductOverlayAssetClassBuilder {

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private ProductOverlayAssetClass productOverlayAssetClass;

	private Map<ManagerCashTypes, BigDecimal> minimizeImbalancesCash = new HashMap<>();


	private ProductOverlayAssetClassBuilder(ProductOverlayAssetClass productOverlayAssetClass) {
		this.productOverlayAssetClass = productOverlayAssetClass;
	}


	public static ProductOverlayAssetClassBuilder newAssetClass(Integer accountAssetClassId, String assetClassName) {
		return ProductOverlayAssetClassBuilder.newAssetClass(accountAssetClassId, assetClassName, false);
	}


	public static ProductOverlayAssetClassBuilder newAssetClassWithReplications(Integer accountAssetClassId, String assetClassName) {
		return ProductOverlayAssetClassBuilder.newAssetClass(accountAssetClassId, assetClassName, true);
	}


	private static ProductOverlayAssetClassBuilder newAssetClass(Integer accountAssetClassId, String assetClassName, boolean replications) {
		ProductOverlayAssetClass oac = new ProductOverlayAssetClass();
		InvestmentAccountAssetClass iac = new InvestmentAccountAssetClass();
		InvestmentAssetClass ac = new InvestmentAssetClass();
		ac.setName(assetClassName);
		if (StringUtils.isEqual("Cash", assetClassName)) {
			ac.setMainCash(true);
		}
		iac.setId(accountAssetClassId);
		iac.setAssetClass(ac);
		oac.setAccountAssetClass(iac);
		oac.setManagerCash(BigDecimal.ZERO);
		oac.setFundCash(BigDecimal.ZERO);
		oac.setClientDirectedCash(BigDecimal.ZERO);
		oac.setTransitionCash(BigDecimal.ZERO);
		oac.setRebalanceCash(BigDecimal.ZERO);
		oac.setRebalanceCashAdjusted(BigDecimal.ZERO);
		oac.setRebalanceAttributionCash(BigDecimal.ZERO);
		oac.setOverlayExposure(BigDecimal.ZERO);
		if (replications) {
			InvestmentReplication rep = new InvestmentReplication();
			oac.setPrimaryReplication(rep);
		}
		return new ProductOverlayAssetClassBuilder(oac);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<ManagerCashTypes, BigDecimal> getMinimizeImbalancesCash() {
		return this.minimizeImbalancesCash;
	}


	public ProductOverlayAssetClassBuilder addMinimizeImbalancesCash(ManagerCashTypes cashType, double amount) {
		if (!this.minimizeImbalancesCash.containsKey(cashType)) {
			this.minimizeImbalancesCash.put(cashType, BigDecimal.ZERO);
		}
		this.minimizeImbalancesCash.put(cashType, MathUtils.add(this.minimizeImbalancesCash.get(cashType), BigDecimal.valueOf(amount)));
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ProductOverlayAssetClassBuilder asRollup() {
		this.productOverlayAssetClass.getAccountAssetClass().setRollupAssetClass(true);
		this.productOverlayAssetClass.setRollupAssetClass(true);
		return this;
	}


	public ProductOverlayAssetClassBuilder asChild(ProductOverlayAssetClass parent) {
		this.productOverlayAssetClass.getAccountAssetClass().setParent(parent.getAccountAssetClass());
		this.productOverlayAssetClass.setParent(parent);
		return this;
	}


	public ProductOverlayAssetClassBuilder withAssetClassPercentage(double assetClassPercentage) {
		this.productOverlayAssetClass.getAccountAssetClass().setAssetClassPercentage(BigDecimal.valueOf(assetClassPercentage));
		return this;
	}


	public ProductOverlayAssetClassBuilder withCashPercentage(double cashPercentage) {
		this.productOverlayAssetClass.getAccountAssetClass().setCashPercentage(BigDecimal.valueOf(cashPercentage));
		return this;
	}


	public ProductOverlayAssetClassBuilder withAdjustedTarget(double adjustedTarget) {
		this.productOverlayAssetClass.setTargetAllocationAdjusted(BigDecimal.valueOf(adjustedTarget));
		return this;
	}


	public ProductOverlayAssetClassBuilder withPhysicalExposure(double physicalExposure) {
		this.productOverlayAssetClass.setActualAllocationAdjusted(BigDecimal.valueOf(physicalExposure));
		return this;
	}


	public ProductOverlayAssetClassBuilder withOverlayExposure(double overlayExposure) {
		this.productOverlayAssetClass.setOverlayExposure(BigDecimal.valueOf(overlayExposure));
		return this;
	}


	public ProductOverlayAssetClassBuilder withCashInBucket(ManagerCashTypes cashType, double amount) {
		this.productOverlayAssetClass.addCashToBucket(cashType, BigDecimal.valueOf(amount));
		if (cashType == ManagerCashTypes.REBALANCE) {
			this.productOverlayAssetClass.setRebalanceCashAdjusted(MathUtils.add(this.productOverlayAssetClass.getRebalanceCashAdjusted(), amount));
		}
		return this;
	}


	public ProductOverlayAssetClassBuilder withRebalanceTriggers(Double minRebalanceTriggerAmount, Double maxRebalanceTriggerAmount) {
		this.productOverlayAssetClass.setRebalanceExposureTarget(this.productOverlayAssetClass.getTargetAllocationAdjusted());
		if (minRebalanceTriggerAmount != null) {
			this.productOverlayAssetClass.setRebalanceTriggerMin(BigDecimal.valueOf(minRebalanceTriggerAmount));
		}
		if (maxRebalanceTriggerAmount != null) {
			this.productOverlayAssetClass.setRebalanceTriggerMax(BigDecimal.valueOf(maxRebalanceTriggerAmount));
		}
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ProductOverlayAssetClass build() {
		return this.productOverlayAssetClass;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
}
