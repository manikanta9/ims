Clifton.product.ldi.BalanceEntryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'LDI Balance Entry',
	iconCls: 'account',
	height: 500,

	items: [{
		xtype: 'formpanel',
		url: 'productLDIBalanceEntry.json',
		instructions: 'The following balances/DV01 values have been entered for the selected client account & maturities.  When entering balance information, all values are required for every maturity.',
		loadValidation: false,

		items: [
			{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'clientInvestmentAccount.id'},
			{name: 'clientInvestmentAccount.label', xtype: 'hidden'},
			{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'datefield', readOnly: true},

			{
				xtype: 'formgrid',
				storeRoot: 'balanceList',
				dtoClass: 'com.clifton.product.ldi.ProductLDIBalance',
				readOnly: true,
				columnsConfig: [
					{header: 'MaturityID', width: 100, dataIndex: 'maturity.id', hidden: true},
					{header: 'Maturity', width: 100, dataIndex: 'maturity.label'},
					{header: 'Assets Balance', width: 100, dataIndex: 'assetsBalance', type: 'currency', editor: {xtype: 'currencyfield', allowBlank: false}, useNull: true},
					{header: 'Assets DV01', width: 100, dataIndex: 'assetsDV01', type: 'currency', editor: {xtype: 'currencyfield', allowBlank: false}, useNull: true},
					{header: 'Liabilities Balance', width: 100, dataIndex: 'liabilitiesBalance', type: 'currency', editor: {xtype: 'currencyfield', allowBlank: false}, useNull: true},
					{header: 'Liabilities DV01', width: 100, dataIndex: 'liabilitiesDV01', type: 'currency', editor: {xtype: 'currencyfield', allowBlank: false}, useNull: true}
				]
			}
		]
	}]
});
