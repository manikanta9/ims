Clifton.product.ldi.MaturityWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'LDI Maturity',
	iconCls: 'account',
	width: 650,
	height: 475,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Maturity Details',
			items: [{
				xtype: 'formpanel',
				url: 'productLDIMaturity.json',
				labelWidth: 125,

				items: [
					{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
					{fieldLabel: 'Maturity Label', name: 'label'},
					{fieldLabel: 'Period Min Days', name: 'periodMinDays', xtype: 'spinnerfield', minValue: 0, maxValue: 20000, type: 'int'},
					{fieldLabel: 'Period Max Days', name: 'periodMaxDays', xtype: 'spinnerfield', minValue: 1, maxValue: 1000000, type: 'int'},
					{fieldLabel: 'Replication', name: 'replication.name', hiddenName: 'replication.id', xtype: 'combo', url: 'investmentReplicationListFind.json', detailPageClass: 'Clifton.investment.replication.ReplicationWindow'},

					{fieldLabel: 'Assets Proxy Benchmark', name: 'assetsProxyBenchmarkSecurity.label', hiddenName: 'assetsProxyBenchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow', mutuallyExclusiveFields: ['assetsProxyBenchmarkInterestRateIndex.id']},
					{fieldLabel: 'Assets Proxy Rate', name: 'assetsProxyBenchmarkInterestRateIndex.label', hiddenName: 'assetsProxyBenchmarkInterestRateIndex.id', xtype: 'combo', url: 'investmentInterestRateIndexListFind.json?active=true', displayField: 'label', detailPageClass: 'Clifton.investment.rates.InterestRateIndexWindow', mutuallyExclusiveFields: ['assetsProxyBenchmarkSecurity.id']},
					{fieldLabel: 'Liabilities Proxy Benchmark', name: 'liabilitiesProxyBenchmarkSecurity.label', hiddenName: 'liabilitiesProxyBenchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow', mutuallyExclusiveFields: ['liabilitiesProxyBenchmarkInterestRateIndex.id']},
					{fieldLabel: 'Liabilities Proxy Rate', name: 'liabilitiesProxyBenchmarkInterestRateIndex.label', hiddenName: 'liabilitiesProxyBenchmarkInterestRateIndex.id', xtype: 'combo', url: 'investmentInterestRateIndexListFind.json?active=true', displayField: 'label', detailPageClass: 'Clifton.investment.rates.InterestRateIndexWindow', mutuallyExclusiveFields: ['liabilitiesProxyBenchmarkSecurity.id']}
				]
			}]
		},


			{

				title: 'Balance History',
				items: [{
					name: 'productLDIBalanceListFind',
					xtype: 'gridpanel',
					instructions: 'This maturity have the following balance history.',
					columns: [
						{header: 'Balance Date', width: 140, dataIndex: 'balanceDate'},
						{header: 'Assets', width: 100, dataIndex: 'assetsBalance', type: 'currency'},
						{header: 'Assets DV01', width: 100, dataIndex: 'assetsDV01', type: 'currency'},
						{header: 'Liabilities', width: 100, dataIndex: 'liabilitiesBalance', type: 'currency'},
						{header: 'Liabilities DV01', width: 100, dataIndex: 'liabilitiesDV01', type: 'currency'}
					],
					getLoadParams: function() {
						const w = this.getWindow();
						return {
							maturityId: w.getMainFormId(),
							requestedMaxDepth: 5
						};
					}
				}]
			}
		]
	}]
});
