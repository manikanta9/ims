Clifton.product.overlay.RunRebalance = {
	title: 'Rebalancing',
	reloadOnTabChange: true,

	items: [
		{
			region: 'north',
			height: 275,
			xtype: 'formpanel',
			labelWidth: 150,
			controlWindowModified: false,

			listeners: {
				afterRender: function() {
					this.reload();
				}
			},
			getOverlayRunId: function() {
				return this.getWindow().getMainFormId();
			},
			reload: function() {
				this.getForm().setValues(TCG.data.getData('productOverlayRebalance.json?requestedMaxDepth=4&overlayRunId=' + this.getOverlayRunId(), this), true);
				// to load the formgrid
				this.fireEvent('afterload', this);
			},

			confirmSaveForm: function(action, confirmTitle, confirmDescr) {
				if (!action) {
					TCG.showError('Missing action to perform');
				}


				const fp = this;
				Ext.Msg.confirm(confirmTitle, 'Are you sure you would like to ' + confirmDescr + '?', function(a) {
					if (a === 'yes') {
						fp.saveForm(action);
					}
				});
			},
			saveForm: function(rebalAction) {
				const panel = this;
				const form = panel.getForm();
				const win = this.getWindow();

				// get the submit parameters
				let params = {};
				if (panel.getSubmitParams) {
					const returnParams = panel.getSubmitParams();
					if (returnParams) {
						params = returnParams;
					}
				}
				params.action = rebalAction;
				params.overlayRunId = this.getOverlayRunId();
				form.trackResetOnLoad = true;
				form.submit(Ext.applyIf({
					url: 'productOverlayRebalanceSave.json',
					params: params,
					waitMsg: 'Saving...',
					success: function(form, action) {
						// Window Modified is still getting triggered even without apply button being enabled/disabled
						win.setModified(false);
						// Don't auto re-process if just updating configuration
						if (rebalAction !== 'SAVE_CONFIG') {
							panel.processOverlayRun();
						}
					}
				}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
			},

			undoLastRebalance: function() {
				const panel = this;
				const lastRebal = TCG.getValue('lastRebalance.id', panel.getForm().formValues);
				const params = {id: lastRebal};

				Ext.Msg.confirm('Undo Last Rebalance', 'Are you sure you would like to undo last rebalancing?', function(a) {
					if (a === 'yes') {
						const loader = new TCG.data.JsonLoader({
							waitTarget: panel,
							waitMsg: 'Removing...',
							params: params,
							conf: params,
							onLoad: function(record, conf) {
								panel.processOverlayRun();
							}
						});
						loader.load('investmentAccountRebalanceHistoryDelete.json');
					}
				});
			},

			processOverlayRun: function() {
				const panel = this;
				const params = {runId: this.getOverlayRunId()};
				const loader = new TCG.data.JsonLoader({
					waitTarget: this,
					timeout: 180000,
					waitMsg: 'Re-Processing Portfolio Run...',
					params: params,
					conf: params,
					onLoad: function(record, conf) {
						panel.reload();
					},
					onFailure: function() {
						// Even if the Processing Fails, still need to reload so that dates are properly updated, and warnings adjusted
						panel.reload();
					}
				});
				loader.load('portfolioRunProcess.json');
			},


			getWarningMessage: function(form) {
				if (!form.formValues) {
					return;
				}
				if (TCG.isTrue(form.formValues.readOnly)) {
					return 'This run record has already been submitted to trading.  Rebalancing Changes cannot be made against this run.';
				}
			},

			items: [
				{
					xtype: 'fieldset',
					name: 'miniRebalFieldSet',
					title: 'Mini-Rebalancing',
					instructions: 'Benchmark return adjusted Rebalance Cash amounts are summed across asset classes during each Portfolio run. If Min/Max Rebalance Cash total is defined here, then the sum will be compared against these thresholds to check if mini-rebalancing is needed. Either a warning will be generated on the Portfolio run, or if the automatic option is selected mini rebalancing using the selected calculation type will be automatically applied.',
					items: [
						{name: 'readOnly', xtype: 'hidden'},
						{name: 'undoLastAllowed', xtype: 'hidden'},
						{fieldLabel: 'Min Rebalance Cash', xtype: 'currencyfield', name: 'rebalanceConfig.minAllowedCash'},
						{fieldLabel: 'Max Rebalance Cash', xtype: 'currencyfield', name: 'rebalanceConfig.maxAllowedCash'},
						{
							fieldLabel: 'Calculator', name: 'rebalanceConfig.calculationTypeLabel', hiddenName: 'rebalanceConfig.calculationType', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
							store: new Ext.data.ArrayStore({
								fields: ['name', 'value', 'description'],
								data: Clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes
							})
						},
						{
							boxLabel: 'Automatically perform rebalancing when thresholds are crossed', name: 'rebalanceConfig.automatic', xtype: 'checkbox',
							qtip: 'During Portfolio run processing, when Rebalance Cash Adjusted values are calculated across all asset classes and the sum crossed the defined threshold the system will automatically perform a mini-rebalance using the selected calculation type.  If not automatic, a warning will be generated and the ability to manually execute the mini rebalance will be allowed.'
						},
						{xtype: 'label', html: '<hr />'},
						{fieldLabel: 'Last Rebalance', xtype: 'linkfield', name: 'lastRebalance.label', detailIdField: 'lastRebalance.id', detailPageClass: 'Clifton.investment.account.assetclass.rebalance.HistoryWindow'}
					],
					buttons: [{
						xtype: 'button',
						text: 'Undo Last Rebalance History',
						iconCls: 'remove',
						width: 180,
						tooltip: 'Removes last rebalance history record. (Allowed only if last rebalancing was for same date as run date)',
						handler: function() {
							const fp = TCG.getParentFormPanel(this);
							fp.undoLastRebalance();
						},
						listeners: {
							afterRender: function() {
								const btn = this;
								const fp = TCG.getParentFormPanel(this);
								fp.on('afterload', function() {
									const undoAllowed = TCG.getValue('undoLastAllowed', fp.getForm().formValues);
									if (TCG.isTrue(undoAllowed)) {
										btn.enable();
									}
									else {
										btn.disable();
									}
								}, this);
							}
						}

					},
						{
							xtype: 'button',
							text: 'Save Config',
							iconCls: 'disk',
							width: 180,
							tooltip: 'Save account rebalance configuration, but do not process rebalance.',
							handler: function() {
								const fp = TCG.getParentFormPanel(this);
								fp.confirmSaveForm('SAVE_CONFIG', 'Save Config', 'update the account rebalance configuration');
							},
							listeners: {
								afterRender: function() {
									const btn = this;
									const fp = TCG.getParentFormPanel(this);
									fp.on('afterload', function() {
										const ro = TCG.getValue('readOnly', fp.getForm().formValues);
										if (TCG.isTrue(ro)) {
											btn.disable();
										}
										else {
											btn.enable();
										}
									}, this);
								}
							}

						},
						{
							xtype: 'button',
							text: 'Save & Process Mini Rebalance',
							iconCls: 'run',
							width: 180,
							tooltip: 'Process mini rebalance using selected options, and save config changes to the account rebalance configuration.',
							handler: function() {
								const fp = TCG.getParentFormPanel(this);
								fp.confirmSaveForm('SAVE_PROCESS_MINI', 'Save and Process', 'update the account rebalance configuration and process a mini rebalance calculation for the Portfolio run date');
							},
							listeners: {
								afterRender: function() {
									const btn = this;
									const fp = TCG.getParentFormPanel(this);
									fp.on('afterload', function() {
										const ro = TCG.getValue('readOnly', fp.getForm().formValues);
										if (TCG.isTrue(ro)) {
											btn.disable();
										}
										else {
											btn.enable();
										}
									}, this);
								}
							}

						},
						{
							xtype: 'button',
							text: 'Process Mini Rebalance',
							iconCls: 'run',
							width: 180,
							tooltip: 'Process mini rebalance using selected options, but do not save any config changes to the account rebalance configuration.',
							handler: function() {
								const fp = TCG.getParentFormPanel(this);
								fp.confirmSaveForm('PROCESS_MINI', 'Process Only', 'process a mini rebalance only (if account rebalance config changes are on this screen will process using values, but not update the account rebalance configuration)');
							},
							listeners: {
								afterRender: function() {
									const btn = this;
									const fp = TCG.getParentFormPanel(this);
									fp.on('afterload', function() {
										const ro = TCG.getValue('readOnly', fp.getForm().formValues);
										if (TCG.isTrue(ro)) {
											btn.disable();
										}
										else {
											btn.enable();
										}
									}, this);
								}
							}

						}
					]
				},
				{
					xtype: 'fieldset',
					title: 'Full Rebalancing',
					instructions: 'For manual rebalancing, enter new Rebalance cash values for each asset class below.  The asset classes that are currently outside of predefined rebalance triggers are highlighted. To have the system perform a full rebalance calculation for you, just click the <i>Process Full Rebalance</i> button.',
					buttons: [{
						text: 'Process Full Rebalance',
						tooltip: 'The system will process a full rebalance and set all rebalance cash values equal to the Asset Class Target less Physical Exposure less effective cash. Manually entered rebalance cash values will be ignored.',
						iconCls: 'run',
						width: 180,
						handler: function() {
							const fp = TCG.getParentFormPanel(this);
							fp.confirmSaveForm('PROCESS_FULL', 'Process Full Rebalance', 'update rebalance cash by processing the system calculated full rebalance');
						},
						listeners: {
							afterRender: function() {
								const btn = this;
								const fp = TCG.getParentFormPanel(this);
								fp.on('afterload', function() {
									const ro = TCG.getValue('readOnly', fp.getForm().formValues);
									if (TCG.isTrue(ro)) {
										btn.disable();
									}
									else {
										btn.enable();
									}
								}, this);
							}
						}
					},

						{
							text: 'Process Manual Rebalance',
							iconCls: 'run',
							tooltip: 'All new rebalance cash values must be entered manually.',
							width: 180,
							handler: function() {
								const fp = TCG.getParentFormPanel(this);
								fp.confirmSaveForm('PROCESS_MANUAL', 'Process Manual Rebalance', 'update Rebalance Cash values for all asset classes using the new rebalance values entered');
							},
							listeners: {
								afterRender: function() {
									const btn = this;
									const fp = TCG.getParentFormPanel(this);
									fp.on('afterload', function() {
										const ro = TCG.getValue('readOnly', fp.getForm().formValues);
										if (TCG.isTrue(ro)) {
											btn.disable();
										}
										else {
											btn.enable();
										}
									}, this);
								}
							}

						}],
					items: [
						{
							xtype: 'formgrid',
							height: 350,
							storeRoot: 'rebalanceAssetClassList',
							frame: false,
							dtoClass: 'com.clifton.product.overlay.ProductOverlayAssetClass',
							readOnly: true,
							controlWindowModified: false,
							detailPageClass: 'Clifton.product.overlay.assetclass.AssetClassWindowSelector',
							tbar: [
								{
									text: 'Export to Excel',
									xtype: 'excel_splitbutton',
									exportMethod: function(exportFormat) {
										this.ownerCt.ownerCt.exportGrid(exportFormat);
									}
								},
								'->',
								{
									text: 'Clear Rebalance Cash',
									tooltip: 'Set New Rebalance Cash to Zero for all asset classes that had non-zero values',
									iconCls: 'clear',
									handler: function() {
										const gp = this.ownerCt.ownerCt;
										gp.getStore().each(function(record) {
											if (record.get('rebalanceCash') !== 0) {
												record.set('newRebalanceCash', 0);
											}
										}, this);
										gp.onAfterEdit();
										gp.markModified();
									}
								},
								'-',
								{
									text: 'Apply No Trade',
									tooltip: 'Set New Rebalance Cash to Overlay Exposure for all asset classes with primary replication defined - i.e. No Trade',
									iconCls: 'arrow-down-green',
									handler: function() {
										const gp = this.ownerCt.ownerCt;
										gp.getStore().each(function(record) {
											if (record.json.primaryReplication) {
												record.set('newRebalanceCash', record.get('overlayExposure'));
											}
										}, this);
										gp.onAfterEdit();
										gp.markModified();
									}
								},
								'-',
								{xtype: 'label', html: 'Rebalance Date:&nbsp;'},
								{
									xtype: 'datefield', name: 'rebalanceDate',
									listeners: {
										afterRender: function() {
											const rd = this;
											const fp = TCG.getParentFormPanel(this);
											// For some reason this doesn't bind on form load, but when saving
											// the value does submit
											fp.on('afterload', function() {
												const val = TCG.parseDate(TCG.getValue('rebalanceDate', fp.getForm().formValues));
												rd.setValue(val.format('m/d/Y'));
											}, this);
										}
									}
								}
							],
							exportGrid: function(outputFormat) {
								// send fields/order/format info to the server and use it to generate exports
								const params = {runId: this.getWindow().getMainFormId()};
								params.columns = this.getColumnsMetaData();
								params.outputFormat = outputFormat;
								TCG.downloadFile('productOverlayAssetClassListByRun.json', params, this);
							},

							getColumnsMetaData: function() {
								if (TCG.isNotNull(this.columnsConfig)) {
									let columnString = '';
									Ext.each(this.columnsConfig, function(f) {
										// Only care about non hidden columns
										if (!TCG.isTrue(f.hidden) && TCG.isNotNull(f.header)) {
											columnString += (f.header + ':');
											columnString += (f.dataIndex + ':');
											let styleString = '';
											if (TCG.isNotNull(f.align)) {
												styleString += ('align=' + f.align + ',');
											}
											if (TCG.isNotNull(f.width)) {
												styleString += ('width=' + f.width + ',');
											}
											styleString += ':';
											columnString += styleString;
											if (TCG.isNotNull(f.exportColumnValueConverter)) {
												columnString += f.exportColumnValueConverter;
											}
											columnString += '|';
										}
									});
									return columnString;
								}
							},

							miniRebalFieldSetCollapsed: false,
							onAfterEdit: function(editor, field) {
								if (this.miniRebalFieldSetCollapsed === false) {
									this.miniRebalFieldSetCollapsed = true;
									const p = TCG.getParentFormPanel(this);
									const miniFieldSet = TCG.getChildByName(p, 'miniRebalFieldSet', false);
									if (miniFieldSet) {
										miniFieldSet.collapse(false);
									}
								}
							},
							listeners: {
								afteredit: function(grid, row, col) {
									// on checkbox clear, set new value to 0 and on check, set it to rebalance cash adjusted value
									if (col === 13) {
										grid.stopEditing();
										grid.startEditing(row, 14);
										const rebalEditor = grid.colModel.getCellEditor(14, row);
										rebalEditor.setValue(rebalEditor.record.get('copyRebalanceCashAdjusted') ? (rebalEditor.record.get('rebalanceCashAdjusted')) : 0);
										grid.stopEditing();
									}
								}
							},

							doNotSubmitFields: ['copyRebalanceCashAdjusted'],

							columnsConfig: [
								{header: 'Asset Class', width: 200, dataIndex: 'accountAssetClass.label'},
								{header: 'Rebalance Benchmark', width: 175, dataIndex: 'coalesceRebalanceBenchmarkSecurity.label'},

								{header: 'OutsideOfRebalanceTrigger', width: 50, dataIndex: 'outsideOfRebalanceTrigger', type: 'boolean', hidden: true},
								{header: 'OutsideOfAbsRebalanceTrigger', width: 50, dataIndex: 'outsideOfAbsoluteRebalanceTrigger', type: 'boolean', hidden: true},

								{header: 'Min Trigger', width: 75, dataIndex: 'rebalanceTriggerMin', type: 'currency', hidden: true},
								{header: 'Max Trigger', width: 75, dataIndex: 'rebalanceTriggerMax', type: 'currency', hidden: true},
								{header: 'Abs Min Trigger', width: 75, dataIndex: 'rebalanceTriggerAbsoluteMin', type: 'currency', hidden: true},
								{header: 'Abs Max Trigger', width: 75, dataIndex: 'rebalanceTriggerAbsoluteMax', type: 'currency', hidden: true},

								{header: 'Overlay Exposure', width: 110, dataIndex: 'overlayExposure', type: 'currency', summaryType: 'sum'},
								{
									header: 'Deviation from Target', tooltip: 'Total Exposure Deviation From Rebalance Exposure Target', width: 100, dataIndex: 'totalExposureDeviationFromRebalanceExposureTarget', type: 'currency', summaryType: 'sum',
									renderer: function(v, p, r) {
										if (v === undefined || v === '') {
											return '';
										}
										const value = Ext.util.Format.number(v, '0,000.00');
										if (TCG.isTrue(r.data['outsideOfRebalanceTrigger'])) {
											if (TCG.isTrue(r.data['outsideOfAbsoluteRebalanceTrigger'])) {
												return '<div class="ruleViolation"><b>' + value + '</b></div>';
											}
											return '<div class="ruleViolation">' + value + '</div>';
										}
										return value;
									}
								},

								{header: 'Rebalance Date', width: 80, dataIndex: 'rebalanceDate', type: 'date'},
								{header: 'Rebalance Cash (Original)', width: 110, dataIndex: 'rebalanceCash', type: 'currency', summaryType: 'sum', hidden: true},
								{
									header: 'Rebalance Cash', width: 110, dataIndex: 'rebalanceCashAdjusted', type: 'currency', summaryType: 'sum',
									renderer: function(v, p, r) {
										return TCG.renderAdjustedAmount(v, r.data['rebalanceAdjusted'], r.data['rebalanceCashAdjusted']);
									}
								},
								{
									header: 'Copy Value', width: 50, dataIndex: 'copyRebalanceCashAdjusted', align: 'center', xtype: 'checkcolumn',
									renderer: function(v, p, record) {
										p.css += ' x-grid3-check-col-td';
										return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
									}
								},
								{header: 'New Rebalance Cash', width: 120, dataIndex: 'newRebalanceCash', type: 'currency', useNull: true, editor: {xtype: 'currencyfield', allowBlank: true}, summaryType: 'sum'}
							],
							plugins: {ptype: 'gridsummary'}
						}
					]
				}
			]
		}
	]
};
