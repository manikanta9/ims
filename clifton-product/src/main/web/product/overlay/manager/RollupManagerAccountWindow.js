TCG.use('Clifton.product.overlay.manager.ManagerAccountForm');

Clifton.product.overlay.manager.RollupManagerAccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Rollup Overlay Manager Account',
	iconCls: 'manager',
	width: 700,
	height: 520,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Manager Account Details',
				items: [{xtype: 'product-overlay-manager-account-form'}]
			},


			{
				title: 'Child Asset Class Allocation',
				items: [{
					name: 'productOverlayManagerAccountListFind',
					xtype: 'gridpanel',
					instructions: 'This rollup manager account is broken down into the following asset class allocations.',
					standardColumns: [],
					getLoadParams: function() {
						const f = this.getWindow().getMainForm();
						return {
							parentId: f.formValues.id,
							runId: TCG.getValue('overlayRun.id', f.formValues),
							requestedMaxDepth: 5
						};
					},
					remoteSort: true,
					columns: [
						{header: 'Full Asset Class', width: 140, dataIndex: 'assetClassLabelExpanded', hidden: true},
						{header: 'Asset Class', width: 140, dataIndex: 'accountAssetClass.label'},
						{header: 'Manager', width: 170, dataIndex: 'managerAccountAssignment.managerLabel', hidden: true},
						{header: 'Account', width: 75, dataIndex: 'managerAccountAssignment.referenceOne.accountNumber', hidden: true},

						{header: 'Securities', width: 100, dataIndex: 'securitiesAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Cash', width: 100, dataIndex: 'cashAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Total Market Value', width: 100, dataIndex: 'totalAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},

						{header: 'Previous Day Securities', width: 100, dataIndex: 'previousDaySecuritiesAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Day Cash', width: 100, dataIndex: 'previousDayCashAllocation', type: 'currency', hidden: true, summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},

						{header: 'Previous Month End Securities', width: 100, dataIndex: 'previousMonthEndSecuritiesAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Month End Cash', width: 100, dataIndex: 'previousMonthEndCashAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},

						{header: 'One Day % Change', width: 100, dataIndex: 'oneDayPercentChange', type: 'percent', summaryType: 'percentChange', previousValueField: 'previousDayTotalAllocation', currentValueField: 'totalAllocation', negativeInRed: true},
						{header: 'MTD % Change', width: 100, dataIndex: 'monthPercentChange', type: 'percent', summaryType: 'percentChange', previousValueField: 'previousMonthEndTotalAllocation', currentValueField: 'totalAllocation', negativeInRed: true},
						{header: 'Cash %', width: 65, dataIndex: 'cashAllocationPercent', type: 'percent', summaryType: 'percent', denominatorValueField: 'totalAllocation', numeratorValueField: 'cashAllocation', negativeInRed: true}
					],
					plugins: {ptype: 'groupsummary'},
					editor: {
						detailPageClass: 'Clifton.product.overlay.manager.ManagerAccountWindowSelector',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
