// not the actual window but a window selector:
//   - rollup asset classes get RollupManagerAccountWindow
//   - the rest get ManagerAccountWindow

Clifton.product.overlay.manager.ManagerAccountWindowSelector = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'productOverlayManagerAccount.json',

	getClassName: function(config, entity) {
		if (entity && entity.rollupAssetClass) {
			return 'Clifton.product.overlay.manager.RollupManagerAccountWindow';
		}
		return 'Clifton.product.overlay.manager.ManagerAccountWindow';
	}
});
