TCG.use('Clifton.product.overlay.manager.ManagerAccountForm');

Clifton.product.overlay.manager.ManagerAccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Overlay Manager Account',
	iconCls: 'manager',
	width: 650,
	height: 550,
	items: [{xtype: 'product-overlay-manager-account-form'}]
});
