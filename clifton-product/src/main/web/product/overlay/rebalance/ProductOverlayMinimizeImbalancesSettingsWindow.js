Clifton.product.overlay.rebalance.ProductOverlayMinimizeImbalancesSettingsWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Minimize Imbalances Advanced Settings',
	iconCls: 'grouping',
	width: 920,
	height: 450,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		forceModified: true,
		labelWidth: 140,
		defaults: {anchor: '0'},
		url: 'productOverlayMinimizeImbalancesSettings.json',


		reload: function() {
			const w = this.getWindow();
			const data = this.convertCustomValueData(TCG.data.getData('productOverlayMinimizeImbalancesSettings.json?investmentAccountId=' + TCG.getValue('investmentAccount.id', this.getForm().formValues), this));
			this.getForm().setValues(data, true);
			w.setModified(w.isModified());
			// to load the formgrid
			this.fireEvent('afterload', this);
		},

		reloadAssetClassGrid: function(data) {
			const grid = TCG.getChildByName(this, 'assetClassGrid');
			if (grid) {
				grid.destroy();
			}


			const colList = data.columnList;
			const alwaysSubmitFields = [];
			const doNotSubmitFields = [];

			const cols = [
				{
					header: 'Asset Class', width: 280, dataIndex: 'label',
					renderer: function(v, p, r) {
						const depth = r.json.level - 1;
						if (TCG.isTrue(r.data['rollupAssetClass'])) {
							return '<div style="FONT-WEIGHT: bold; COLOR: #' + ((depth > 0) ? '777777' : '000000') + '; PADDING-LEFT: ' + 15 * depth + 'px">' + v + '</div>';
						}
						else if (depth > 0) {
							return '<div style="COLOR: #777777; PADDING-LEFT: ' + 15 * depth + 'px">' + v + '</div>';
						}
						return v;
					}
				},
				{header: 'Rollup', width: 15, dataIndex: 'rollupAssetClass', type: 'boolean', hidden: true},
				{
					header: 'Target Exposure', dataIndex: 'assetClassPercentage', width: 90, type: 'percent', summaryType: 'sum', viewNames: ['Default View', 'Exposure'],
					renderer: function(v, p, r) {
						let style = '';
						let vv = v;
						if (TCG.isTrue(r.data['rollupAssetClass'])) {
							style += 'FONT-WEIGHT: bold;';
						}
						if (r.data['parent.assetClass.name']) {
							style += 'COLOR: #777777;';
							vv = r.data['assetClassPercentageSAVED'];
							if (!vv) {
								r.data['assetClassPercentageSAVED'] = r.data['assetClassPercentage'];
								r.data['assetClassPercentage'] = 0;
								vv = v;
							}
						}
						vv = Ext.util.Format.number(vv, '0,000.0000 %');
						return TCG.isBlank(style) ? vv : '<div style="' + style + '">' + vv + '</div>';
					}
				}
			];

			for (let i = 0; i < colList.length; i++) {
				const col = colList[i];
				const dt = col.dataType.name;
				alwaysSubmitFields.push('column_' + col.id);
				doNotSubmitFields.push('column_' + col.id + '_id');
				const idColConfig = {header: col.name + ' ID', width: 50, hidden: true, dataIndex: 'column_' + col.id + '_id', type: 'int', useNull: true};
				let colConfig = {header: col.name, tooltip: col.description, width: 100, dataIndex: 'column_' + col.id, columnId: col.id};


				if (dt === 'BOOLEAN') {
					// SPECIAL OVERRIDE FOR CHECKBOXES...OTHER INPUT FIELDS USE START EDITING EVENT (SEE BELOW) TO CATCH THIS
					// Some of the calculators have properties that apply to rollup asset classes only.
					// Currently these properties are checkboxes, so this workaround is implemented for checkboxes only:
					// If Column Name has "Rollup" in it's name than it's editable ONLY if the asset class is a rollup, otherwise it is editable ONLY if it's not
					const rollupOnly = (col.name).indexOf('Rollup') !== -1;

					colConfig = Ext.apply(colConfig, {
						type: 'boolean',
						rollupOnly: rollupOnly,
						editor: {xtype: 'checkcolumn'},
						isCheckEnabled: function(record) {
							return this.rollupOnly ? TCG.isTrue(record.data.rollupAssetClass) : true;
						},
						overrideRenderer: true,
						renderer: function(v, p, record) {
							if (TCG.isTrue(this.isCheckEnabled(record))) {
								p.css += ' x-grid3-check-col-td';
								return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
							}
							return '&nbsp;';
						}
					});
				}
				else if (dt === 'DATE') {
					colConfig = Ext.apply(colConfig, {
						type: 'date',
						editor: {xtype: 'datefield'}
					});

				}
				else if (dt === 'INTEGER') {
					colConfig = Ext.apply(colConfig, {
						type: 'int',
						useNull: true,
						editor: {xtype: 'integerfield'}
					});

				}
				else if (dt === 'DECIMAL') {
					colConfig = Ext.apply(colConfig, {
						type: 'currency',
						useNull: true,
						editor: {xtype: 'currencyfield'}
					});

				}
				else {
					colConfig = Ext.apply(colConfig, {
						editor: {xtype: 'textfield'}
					});
				}


				cols.push(idColConfig);
				cols.push(colConfig);
			}

			this.add({
				xtype: 'formgrid',
				name: 'assetClassGrid',
				storeRoot: 'accountAssetClassList',
				readOnly: true,
				dtoClass: 'com.clifton.investment.account.assetclass.InvestmentAccountAssetClass',
				alwaysSubmitFields: alwaysSubmitFields,
				doNotSubmitFields: doNotSubmitFields,
				columnsConfig: cols,

				// Overridden to support tradeEntryDisabled to make cells un-editable
				startEditing: function(row, col) {
					const rollupOnly = (this.colModel.getColumnHeader(col)).indexOf('Rollup') !== -1;
					if (rollupOnly !== this.getStore().getAt(row).get('rollupAssetClass')) {
						return false;
					}

					TCG.grid.FormGridPanel.superclass.startEditing.apply(this, arguments);
					// need a way to get to grid from editable fields: combo, etc. to get form arguments
					const ed = this.colModel.getCellEditor(col, row);
					if (ed) {
						ed.containerGrid = this;
					}
				},

				markModified: function(initOnly) {
					const result = [];
					let newId = -10;
					this.store.each(function(record) {
						if (!Ext.isNumber(record.id)) {
							record.id = newId--;
							record.markDirty(); // new records should be fully submitted
						}
						const r = {
							id: record.id,
							'class': this.dtoClass
						};
						const columnValueList = [];

						for (let i = 0; i < this.alwaysSubmitFields.length; i++) {
							const f = this.alwaysSubmitFields[i];
							let val = TCG.getValue(f, record.data);
							if (TCG.isNotNull(val)) {
								if (typeof val === 'number' && isNaN(val)) {
									val = '';
								}
								else if (f.length > 4 && f.substring(f.length - 4) === 'Date') {
									val = TCG.renderDate(val);
								}

								const fi = this.getColumnModel().findColumnIndex(f);
								const fCol = this.getColumnModel().config[fi];
								const colId = fCol.columnId;
								if (colId) {
									let valId = TCG.getValue(f + '_id', record.data);
									if (TCG.isNull(valId)) {
										valId = newId--;
									}
									const valObj = {id: valId, 'class': 'com.clifton.system.schema.column.value.SystemColumnValue', 'column.id': colId, value: val, text: val};
									columnValueList.push(valObj);
								}
								else {
									r[f] = val;
								}
							}
						}

						r.columnValueList = columnValueList;
						result.push(r);
					}, this);
					const value = Ext.util.JSON.encode(result);
					this.submitField.setValue(value); // changed value
					if (initOnly) {
						this.submitField.originalValue = value; // initial value
					}
					else {
						this.getWindow().setModified(true);
					}
				}
			});
			this.doLayout();
		},

		convertCustomValueData: function(data) {
			const assetClassList = data.accountAssetClassList;
			for (let i = 0; i < assetClassList.length; i++) {
				const ac = assetClassList[i];
				const cvList = ac.columnValueList;
				for (let j = 0; j < cvList.length; j++) {
					const cv = cvList[j];
					ac['column_' + cv.column.id] = cv.value;
					ac['column_' + cv.column.id + '_id'] = cv.id;
				}
			}
			return data;
		},


		loadJsonResult: function(result, doNotUpdate) {
			const data = this.convertCustomValueData(result.data);
			this.reloadAssetClassGrid(data);
			const form = this.getForm();
			if (!doNotUpdate) {
				form.clearInvalid();
				form.setValues(data);
			}
			form.idFieldValue = result.data.id;
			form.loaded = true;
			this.updateTitle();
			this.fireEvent('change', this);
			this.focusOnFirstField();
		},


		items: [
			{fieldLabel: 'Client Account', name: 'investmentAccount.label', xtype: 'linkfield', detailIdField: 'investmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow', submitValue: false},
			{fieldLabel: 'Column Group', name: 'columnGroupName', xtype: 'displayfield'}


		]
	}],

	saveWindow: function(closeOnSuccess, forceSubmit) {
		const fp = this.getFormPanels(false)[0];
		const form = fp.getForm();
		this.saveForm(closeOnSuccess, form, fp);

	},

	saveForm: function(closeOnSuccess, form, panel) {
		const assetClassList = Ext.util.JSON.decode(form.findField('accountAssetClassList').getValue());
		const columnGroupName = form.findField('columnGroupName').getValue();
		const grid = TCG.getChildByName(this, 'assetClassGrid');
		const acCount = 0;
		const modifiedRecords = grid.getStore().getModifiedRecords();
		this.saveRow(assetClassList, acCount, grid, panel, closeOnSuccess, columnGroupName, modifiedRecords);
	},

	saveRow: function(assetClassList, counter, grid, panel, closeOnSuccess, columnGroupName, modifiedRecords) {
		const win = this;

		if (counter >= assetClassList.length) {
			// DONE
			if (closeOnSuccess) {
				this.closeWindow();
			}
			else {
				panel.reload();
			}
		}
		else {
			const ac = assetClassList[counter];
			// Only Save if the row was updated:
			let skip = true;
			for (let i = 0; i < modifiedRecords.length; i++) {
				if (modifiedRecords[i].id === ac.id) {
					skip = false;
					break;
				}
			}
			if (!skip) {

				const loader = new TCG.data.JsonLoader({
					waitMsg: 'Saving Values',
					waitTarget: panel,
					params: {
						tableName: 'InvestmentAccountAssetClass',
						id: ac.id,
						columnGroupName: columnGroupName,
						columnValueList: Ext.util.JSON.encode(ac.columnValueList)
					},
					onLoad: function(record, conf) {
						counter++;
						win.saveRow(assetClassList, counter, grid, panel, closeOnSuccess, columnGroupName, modifiedRecords);
					}
				});
				loader.load('systemColumnValueListSave.json');
			}
			else {
				counter++;
				win.saveRow(assetClassList, counter, grid, panel, closeOnSuccess, columnGroupName, modifiedRecords);
			}
		}

	}

});



