Clifton.product.overlay.assetclass.AddNewReplicationWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'New Overlay Asset Class Replication',
	iconCls: 'replicate',
	hideApplyButton: true,
	width: 600,
	height: 400,

	// Special Method so Replications are fully reloaded on Trade Creation Screen to get new ones retrieved and added to list
	reloadOpener: function() {
		const win = this;
		if (win.openerCt && win.openerCt.fullReload) {
			win.openerCt.fullReload();
		}
		else if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload) {
			win.openerCt.reload();
		}
	},

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		url: 'productOverlayAssetClassReplication.json',

		getSaveURL: function() {
			return 'productOverlayAssetClassReplicationCopy.json';
		},

		instructions: 'Select a security to add to the portfolio run asset class replication. Security will be validated based on the previously selected row. Note: Security will be added to the run for trading only and will not be added to the actual replication allocations.  All targets will be set to zero.',

		items: [
			{xtype: 'hidden', name: 'security.instrument.id', submitValue: false},
			{fieldLabel: 'Overlay Asset Class', xtype: 'linkfield', name: 'overlayAssetClass.label', hiddenName: 'overlayAssetClass.id', detailPageClass: 'Clifton.product.overlay.assetclass.AssetClassWindow', submitDetailField: false},
			{fieldLabel: 'Replication', xtype: 'linkfield', name: 'replication.name', hiddenName: 'replication.id', detailPageClass: 'Clifton.investment.replication.ReplicationWindow', submitDetailField: false},
			{
				fieldLabel: 'Security', name: 'newSecurityLabel', hiddenName: 'newSecurityId', xtype: 'combo', allowBlank: false, displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();

					const securityGroupId = TCG.getValue('replicationAllocation.replicationSecurityGroup.id', f.formValues);

					const comboStore = queryEvent.combo.getStore();

					comboStore.setBaseParam('active', true);

					// If there is no security group defined, just use the instrument from the existing security
					if (TCG.isBlank(securityGroupId)) {
						comboStore.setBaseParam('instrumentId', TCG.getValue('security.instrument.id', f.formValues));
					}
					// Otherwise use the security group (and instrument if defined on the allocation)
					else {
						const instrumentId = f.getFormValue('replicationAllocation.replicationInstrument.id');
						comboStore.setBaseParam('instrumentId', instrumentId);
						comboStore.setBaseParam('securityGroupId', securityGroupId);
					}
				},

				listeners: {
					'beforerender': function() {
						this.drillDownMenu = new Ext.menu.Menu({
							items: [
								{
									text: 'Open Detail Window', iconCls: 'info',
									handler: function() {
										this.createDetailClass(false);
									}, scope: this
								},
								{
									text: 'Add New Security For Instrument', iconCls: 'add',
									handler: function() {
										this.createDetailClass(true);
									}, scope: this
								},
								{
									text: 'Add New Security (Using Existing Security as Template)', iconCls: 'copy',
									handler: function() {
										this.createDetailClass(true, true);
									}, scope: this
								}
							]
						});
					}
				},

				createDetailClass: function(newInstance, useTemplate) {
					const fp = TCG.getParentFormPanel(this);
					const w = (TCG.isNull(fp)) ? {} : fp.getWindow();
					const className = this.getDetailPageClass(newInstance, useTemplate);
					let id = this.getValue();
					if (TCG.isBlank(id) || newInstance) {
						id = null;
					}
					const params = id ? {id: id} : null;
					const defaultData = this.getDefaultData(TCG.isNull(fp) ? {} : fp.getForm());
					const cmpId = TCG.getComponentId(className, id);
					TCG.createComponent(className, {
						modal: TCG.isNull(id),
						id: cmpId,
						defaultData: defaultData,
						params: params,
						openerCt: this, // for modal new item window will call reload() on close
						defaultIconCls: w.iconCls
					});
				},
				getDetailPageClass: function(newInstance, useTemplate) {
					if (newInstance && useTemplate) {
						return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
					}
					return this.detailPageClass;
				},
				getDefaultData: function(f) { // defaults client for "add new" drill down
					return {
						instrument: TCG.getValue('security.instrument', f.formValues),
						copyFromSecurityId: TCG.getValue('security.id', f.formValues),
						copyFromSecurityIdLabel: TCG.getValue('security.label', f.formValues)
					};
				}
			},
			{fieldLabel: 'Price Override', qtip: 'Populate this value to manually override the security price.  If price is missing from market data, this price will be used as the Previous Close price as well.', name: 'priceOverride', xtype: 'floatfield'},
			{
				xtype: 'fieldset',
				title: 'Create from Template',
				instructions: 'Create a new security using selected existing security as a template:',
				items: [
					{fieldLabel: 'Existing Security', xtype: 'linkfield', name: 'security.label', hiddenName: 'security.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'security.id'}
				],
				buttons: [
					{
						xtype: 'button', text: 'Create New Security', tooltip: 'Add New Security (Using Existing Security As Template)', width: 150,
						handler: function() {
							const f = TCG.getParentFormPanel(this);
							const ns = f.getForm().findField('newSecurityLabel');
							ns.createDetailClass(true, true);
						}
					}
				]
			}
		]
	}]
});
