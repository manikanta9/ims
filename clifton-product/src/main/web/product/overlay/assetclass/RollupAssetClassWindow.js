Clifton.product.overlay.assetclass.RollupAssetClassWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Rollup Overlay Asset Class',
	iconCls: 'grouping',
	width: 700,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Rollup Asset Class Details',
			items: [{
				xtype: 'formpanel',
				loadValidation: false,
				readOnly: true,
				url: 'productOverlayAssetClass.json',
				labelWidth: 188,

				items: [
					{fieldLabel: 'Client Account', name: 'overlayRun.clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'overlayRun.clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
					{fieldLabel: 'Trading Client Account', name: 'tradingClientAccount.label', xtype: 'linkfield', detailIdField: 'tradingClientAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
					{fieldLabel: 'Asset Class', name: 'labelExpanded', xtype: 'linkfield', detailIdField: 'accountAssetClass.id', detailPageClass: 'Clifton.investment.account.assetclass.AssetClassWindowSelector'},
					{fieldLabel: 'Balance Date', name: 'overlayRun.balanceDate', xtype: 'displayfield', type: 'date'},
					{
						xtype: 'fieldset',
						title: 'Allocations',
						labelWidth: 180,
						items: [
							{
								fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
								defaults: {
									xtype: 'displayfield',
									flex: 1
								},
								items: [
									{value: 'Amount'},
									{value: 'Percentage (%)'}
								]
							},
							{
								fieldLabel: 'Actual Physical Exposure (Original)', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'actualAllocation'},
									{name: 'actualAllocationPercent'}
								]
							},
							{
								fieldLabel: 'Actual Physical Exposure (Adjusted)', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'actualAllocationAdjusted'},
									{name: 'actualAllocationAdjustedPercent'}
								]
							},

							{
								fieldLabel: 'Target Allocation (Original)', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'targetAllocation'},
									{name: 'targetAllocationPercent'}
								]
							},
							{
								fieldLabel: 'Target Allocation (Adjusted)', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'targetAllocationAdjusted'},
									{name: 'targetAllocationAdjustedPercent'}
								]
							},
							{
								fieldLabel: 'Physical Deviation From Adjusted Target', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'physicalDeviationFromAdjustedTarget'},
									{name: 'physicalDeviationFromAdjustedTargetPercent'}
								]
							},
							{
								fieldLabel: 'Overlay Exposure', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'overlayExposure'},
									{name: 'overlayExposurePercent'}
								]
							},
							{
								fieldLabel: 'Total Exposure', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'totalExposure'},
									{name: 'totalExposurePercent'}
								]
							},
							{
								fieldLabel: 'Total Exposure Deviation From Adjusted Target', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'totalExposureDeviationFromAdjustedTarget'},
									{name: 'totalExposureDeviationFromAdjustedTargetPercent'}
								]
							}]
					},
					{
						xtype: 'fieldset',
						title: 'Cash',
						labelWidth: 180,
						items: [
							{fieldLabel: 'Fund Cash', name: 'fundCash', xtype: 'currencyfield'},
							{fieldLabel: 'Manager Cash', name: 'managerCash', xtype: 'currencyfield'},
							{fieldLabel: 'Transition Cash', name: 'transitionCash', xtype: 'currencyfield'},
							{fieldLabel: 'Rebalance Cash', name: 'rebalanceCash', xtype: 'currencyfield'},
							{fieldLabel: 'Rebalance Cash Adjusted', name: 'rebalanceCashAdjusted', xtype: 'currencyfield'},
							{fieldLabel: 'Rebalance Attribution Cash', width: 100, name: 'rebalanceAttributionCash', xtype: 'currencyfield'},
							{fieldLabel: 'Total Rebalance Cash', width: 100, name: 'rebalanceCashAdjustedWithAttribution', xtype: 'currencyfield'},
							{fieldLabel: 'Client Directed Cash', name: 'clientDirectedCash', xtype: 'currencyfield'}
						]
					},
					{
						xtype: 'fieldset-checkbox',
						title: 'Rebalance Triggers',
						checkboxName: 'rebalanceTriggersUsed',
						labelWidth: 180,
						items: [
							{
								fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
								defaults: {
									xtype: 'displayfield',
									flex: 1
								},
								items: [
									{value: 'Amount'},
									{value: 'Percentage (%)'}
								]
							},
							{
								fieldLabel: 'Rebalance Exposure Target', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'rebalanceExposureTarget'},
									{name: 'rebalanceExposureTargetPercent'}
								]
							},
							{
								fieldLabel: 'Total Exposure Deviation from Rebalance Exposure Target', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'totalExposureDeviationFromRebalanceExposureTarget'},
									{name: 'totalExposureDeviationFromRebalanceExposureTargetPercent'}
								]
							},
							{
								fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
								defaults: {
									xtype: 'displayfield',
									flex: 1
								},
								items: [
									{value: 'Min'},
									{value: 'Max'}
								]
							},
							{
								fieldLabel: 'Trigger Amounts', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'rebalanceTriggerMin'},
									{name: 'rebalanceTriggerMax'}
								]
							},
							{
								fieldLabel: 'Absolute Trigger Amounts', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'rebalanceTriggerAbsoluteMin'},
									{name: 'rebalanceTriggerAbsoluteMax'}
								]
							},
							{xtype: 'label', html: '<hr/>'},
							{
								fieldLabel: 'Trigger %', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'rebalanceTriggerMinPercent'},
									{name: 'rebalanceTriggerMaxPercent'}
								]
							},
							{
								fieldLabel: 'Absolute Trigger %', xtype: 'compositefield',
								defaults: {
									xtype: 'currencyfield',
									flex: 1
								},
								items: [
									{name: 'rebalanceTriggerAbsoluteMinPercent'},
									{name: 'rebalanceTriggerAbsoluteMaxPercent'}
								]
							}

						]
					}]
			}]
		},

			{
				title: 'Manager Allocation',

				items: [{
					name: 'productOverlayManagerAccountListFind',
					xtype: 'gridpanel',
					instructions: 'Selected client account asset class has the following allocation of manager account cash and security balances.',
					appendStandardColumns: false,
					getLoadParams: function() {
						return {
							accountAssetClassId: TCG.getValue('accountAssetClass.id', this.getWindow().getMainForm().formValues),
							runId: TCG.getValue('overlayRun.id', this.getWindow().getMainForm().formValues)

						};
					},
					remoteSort: true,
					isPagingEnabled: function() {
						return false;
					},
					pageSize: 1000,
					columns: [
						{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
						{header: 'Asset Class', width: 140, dataIndex: 'accountAssetClass.label', hidden: true},
						{header: 'Manager', width: 170, dataIndex: 'managerAccountAssignment.managerLabel'},
						{header: 'Account', width: 75, dataIndex: 'managerAccountAssignment.referenceOne.accountNumber', hidden: true},

						{header: 'Securities', width: 100, dataIndex: 'securitiesAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Cash', width: 100, dataIndex: 'cashAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Total Market Value', width: 100, dataIndex: 'totalAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},

						{header: 'Previous Day Securities', width: 100, dataIndex: 'previousDaySecuritiesAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Day Cash', width: 100, dataIndex: 'previousDayCashAllocation', type: 'currency', hidden: true, summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Day Total', width: 100, dataIndex: 'previousDayTotalAllocation', type: 'currency', hidden: true, summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},

						{header: 'Previous Month End Securities', width: 100, dataIndex: 'previousMonthEndSecuritiesAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Month End Cash', width: 100, dataIndex: 'previousMonthEndCashAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Month End Total', width: 100, dataIndex: 'previousMonthEndTotalAllocation', type: 'currency', hidden: true, summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},

						{header: 'One Day % Change', width: 100, dataIndex: 'oneDayPercentChange', type: 'percent', summaryType: 'percentChange', previousValueField: 'previousDayTotalAllocation', currentValueField: 'totalAllocation', negativeInRed: true},
						{header: 'MTD % Change', width: 100, dataIndex: 'monthPercentChange', type: 'percent', summaryType: 'percentChange', previousValueField: 'previousMonthEndTotalAllocation', currentValueField: 'totalAllocation', negativeInRed: true},
						{header: 'Cash %', width: 65, dataIndex: 'cashAllocationPercent', type: 'percent', summaryType: 'percent', denominatorValueField: 'totalAllocation', numeratorValueField: 'cashAllocation', negativeInRed: true}
					],
					plugins: {ptype: 'groupsummary'},
					editor: {
						detailPageClass: 'Clifton.product.overlay.manager.ManagerAccountWindowSelector',
						drillDownOnly: true
					}
				}]
			},
			{
				title: 'Child Asset Classes',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				frame: true,

				items: [{
					name: 'productOverlayAssetClassListFind',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					title: 'Exposure Percent',
					instructions: 'Selected rollup asset class has the following child asset class allocation from all managers.',
					border: 1,
					flex: 1,


					getLoadParams: function() {
						const f = this.getWindow().getMainForm();
						return {
							parentId: f.formValues.id,
							runId: TCG.getValue('overlayRun.id', f.formValues),
							requestedMaxDepth: 5
						};
					},

					columns: [
						{header: 'Asset Class', width: 140, dataIndex: 'label'},
						{header: 'Trading Account', width: 75, dataIndex: 'tradingClientAccount.number', hidden: true},
						{header: 'IsTargetAdjusted', width: 50, dataIndex: 'targetAdjusted', type: 'boolean', hidden: true},
						{header: 'TFA', width: 50, dataIndex: 'targetFollowsActual', type: 'boolean', hidden: true},
						{header: 'OutsideOfRebalanceTrigger', width: 50, dataIndex: 'outsideOfRebalanceTrigger', type: 'boolean', hidden: true},
						{header: 'OutsideOfAbsRebalanceTrigger', width: 50, dataIndex: 'outsideOfAbsoluteRebalanceTrigger', type: 'boolean', hidden: true},

						// Percentage Columns
						{header: 'Policy Target', width: 80, dataIndex: 'targetAllocationPercent', type: 'percent', summaryType: 'sum'},
						{
							header: 'Adjusted Target', width: 80, dataIndex: 'targetAllocationAdjustedPercent', type: 'percent', summaryType: 'sum',
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, r.data['targetAdjusted'], r.data['targetAllocationPercent'], '0,000.00 %');
							}
						},
						{header: 'Physical Exposure (Original)', width: 80, dataIndex: 'actualAllocationPercent', type: 'percent', summaryType: 'sum', hidden: true},
						{
							header: 'Physical Exposure', width: 80, dataIndex: 'actualAllocationAdjustedPercent', type: 'percent', summaryType: 'sum',
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, r.data['actualAllocationPercent'] !== r.data['actualAllocationAdjustedPercent'], r.data['actualAllocationPercent'], '0,000.00 %');
							}
						},
						{header: 'Physical Deviation From Adjusted Target', width: 80, dataIndex: 'physicalDeviationFromAdjustedTargetPercent', type: 'percent', summaryType: 'sum'},
						{header: 'Overlay Exposure', width: 80, dataIndex: 'overlayExposurePercent', type: 'percent', summaryType: 'sum'},
						{header: 'Total Exposure', width: 70, dataIndex: 'totalExposurePercent', type: 'percent', summaryType: 'sum'},
						{
							header: 'Total Exposure Deviation From Adjusted Target', width: 90, dataIndex: 'totalExposureDeviationFromAdjustedTargetPercent', type: 'percent', summaryType: 'sum',
							renderer: function(v, p, r) {
								if (v === undefined || v === '') {
									return '';
								}
								const value = Ext.util.Format.number(v, '0,000.00 %');
								if (r.data['outsideOfRebalanceTrigger'] === true) {
									if (r.data['outsideOfAbsoluteRebalanceTrigger'] === true) {
										return '<div class="ruleViolation"><b>' + value + '</b></div>';
									}
									return '<div class="ruleViolation">' + value + '</div>';
								}
								return value;
							}
						},
						// Percents - Rebalance Triggers
						{header: 'Min Rebalance Trigger', width: 80, dataIndex: 'rebalanceTriggerMinPercent', type: 'percent'},
						{header: 'Max Rebalance Trigger', width: 80, dataIndex: 'rebalanceTriggerMaxPercent', type: 'percent'},
						// Absolutes
						{header: 'Absolute Min Trigger', width: 80, dataIndex: 'rebalanceTriggerAbsoluteMinPercent', type: 'percent', hidden: true},
						{header: 'Absolute Max Trigger', width: 80, dataIndex: 'rebalanceTriggerAbsoluteMaxPercent', type: 'percent', hidden: true}
					],
					plugins: {ptype: 'gridsummary'},
					editor: {
						detailPageClass: 'Clifton.product.overlay.assetclass.AssetClassWindowSelector',
						drillDownOnly: true
					}
				},

					{flex: 0.05},

					{
						name: 'productOverlayAssetClassListFind',
						xtype: 'gridpanel',
						appendStandardColumns: false,
						title: 'Exposure in Base Currency',
						instructions: 'Selected rollup asset class has the following child asset class allocation from all managers.',
						border: 1,
						flex: 1,

						getLoadParams: function() {
							const f = this.getWindow().getMainForm();
							return {
								parentId: f.formValues.id,
								runId: TCG.getValue('overlayRun.id', f.formValues),
								requestedMaxDepth: 5
							};
						},

						columns: [
							{header: 'Asset Class', width: 140, dataIndex: 'label'},
							{header: 'Trading Account', width: 150, dataIndex: 'tradingClientAccount.number', hidden: true},
							{header: 'IsTargetAdjusted', width: 50, dataIndex: 'targetAdjusted', type: 'boolean', hidden: true},
							{header: 'TFA', width: 50, dataIndex: 'targetFollowsActual', type: 'boolean', hidden: true},
							{header: 'OutsideOfRebalanceTrigger', width: 50, dataIndex: 'outsideOfRebalanceTrigger', type: 'boolean', hidden: true},
							{header: 'OutsideOfAbsRebalanceTrigger', width: 50, dataIndex: 'outsideOfAbsoluteRebalanceTrigger', type: 'boolean', hidden: true},

							// Amount Columns
							{header: 'Policy Target', width: 80, dataIndex: 'targetAllocation', type: 'currency', numberFormat: '0,000', summaryType: 'sum'},
							{
								header: 'Adjusted Target', width: 80, dataIndex: 'targetAllocationAdjusted', type: 'currency', summaryType: 'sum',
								renderer: function(v, p, r) {
									return TCG.renderAdjustedAmount(v, r.data['targetAdjusted'], r.data['targetAllocation'], '0,000');
								}
							},
							{header: 'Physical Exposure (Original)', width: 80, dataIndex: 'actualAllocation', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hidden: true},
							{
								header: 'Physical Exposure', width: 80, dataIndex: 'actualAllocationAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum',
								renderer: function(v, p, r) {
									return TCG.renderAdjustedAmount(v, r.data['actualAllocation'] !== r.data['actualAllocationAdjusted'], r.data['actualAllocation'], '0,000');
								}
							},
							{header: 'Physical Deviation From Adjusted Target', width: 80, dataIndex: 'physicalDeviationFromAdjustedTarget', type: 'currency', numberFormat: '0,000', summaryType: 'sum'},
							{header: 'Overlay Exposure', width: 80, dataIndex: 'overlayExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum'},
							{header: 'Total Exposure', width: 80, dataIndex: 'totalExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum'},
							{
								header: 'Total Exposure Deviation From Adjusted Target', width: 80, dataIndex: 'totalExposureDeviationFromAdjustedTarget', type: 'currency', numberFormat: '0,000', summaryType: 'sum',
								renderer: function(v, p, r) {
									if (v === undefined || v === '') {
										return '';
									}
									const value = Ext.util.Format.number(v, '0,000');
									if (r.data['outsideOfRebalanceTrigger'] === true) {
										if (r.data['outsideOfAbsoluteRebalanceTrigger'] === true) {
											return '<div class="ruleViolation"><b>' + value + '</b></div>';
										}
										return '<div class="ruleViolation">' + value + '</div>';
									}
									return value;
								}
							},

							// Amounts - Rebalance Triggers
							{header: 'Min Rebalance Trigger', width: 80, dataIndex: 'rebalanceTriggerMin', type: 'currency', numberFormat: '0,000'},
							{header: 'Max Rebalance Trigger', width: 80, dataIndex: 'rebalanceTriggerMax', type: 'currency', numberFormat: '0,000'},
							// Absolutes
							{header: 'Absolute Min Trigger', width: 80, dataIndex: 'rebalanceTriggerAbsoluteMin', type: 'currency', numberFormat: '0,000', hidden: true},
							{header: 'Absolute Max Trigger', width: 80, dataIndex: 'rebalanceTriggerAbsoluteMax', type: 'currency', numberFormat: '0,000', hidden: true}
						],
						plugins: {ptype: 'gridsummary'},
						editor: {
							detailPageClass: 'Clifton.product.overlay.assetclass.AssetClassWindowSelector',
							drillDownOnly: true
						}
					}]
			}
		]
	}]
});
