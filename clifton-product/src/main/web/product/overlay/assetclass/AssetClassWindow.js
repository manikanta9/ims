Clifton.product.overlay.assetclass.AssetClassWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Overlay Asset Class',
	iconCls: 'grouping',
	width: 1050,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Asset Class Details',
			items: [{
				xtype: 'formpanel',
				loadValidation: false,
				readOnly: true,
				defaults: {anchor: '0'},
				url: 'productOverlayAssetClass.json',
				labelWidth: 135,

				items: [
					{
						xtype: 'panel',
						layout: 'column',
						items: [{
							columnWidth: .49,
							items: [
								{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 140,
									items: [
										{fieldLabel: 'Client Account', name: 'overlayRun.clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'overlayRun.clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
										{fieldLabel: 'Trading Client Account', name: 'tradingClientAccount.label', xtype: 'linkfield', detailIdField: 'tradingClientAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
										{fieldLabel: 'Asset Class', name: 'labelExpanded', xtype: 'linkfield', detailIdField: 'accountAssetClass.id', detailPageClass: 'Clifton.investment.account.assetclass.AssetClassWindowSelector'},
										{fieldLabel: 'Balance Date', name: 'overlayRun.balanceDate', xtype: 'displayfield', type: 'date'},
										{fieldLabel: 'Benchmark', name: 'benchmarkSecurity.label', xtype: 'linkfield', detailIdField: 'benchmarkSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
										{
											fieldLabel: 'Daily Return:', xtype: 'compositefield', labelSeparator: '',
											defaults: {
												xtype: 'label',
												flex: 1
											},
											items: [
												{name: 'benchmarkOneDayReturn', xtype: 'currencyfield', decimalPrecision: 5},
												{html: '&nbsp;MTD Return:', flex: 1},
												{name: 'benchmarkMonthToDateReturn', xtype: 'currencyfield', decimalPrecision: 5}
											]
										},
										{boxLabel: 'Private Asset Class (Exclude From Reports)', name: 'privateAssetClass', xtype: 'checkbox'}
									]
								},
								{
									xtype: 'fieldset-checkbox',
									title: 'Replications',
									checkboxName: 'replicationUsed',
									labelWidth: 160,
									items: [
										{fieldLabel: 'Primary Replication', name: 'primaryReplication.name', detailIdField: 'primaryReplication.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.replication.ReplicationWindow'},
										{fieldLabel: 'Secondary Replication', name: 'secondaryReplication.name', detailIdField: 'secondaryReplication.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.replication.ReplicationWindow', requiredFields: ['primaryReplication.id']},
										{fieldLabel: 'Secondary Amount', name: 'secondaryReplicationAmount', xtype: 'currencyfield'},
										{boxLabel: 'Do Not Adjust Contract Value (Notional = Price*Qty*Multiplier)', xtype: 'checkbox', name: 'doNotAdjustContractValue'}
									]
								},
								{
									xtype: 'fieldset',
									title: 'Cash',
									labelWidth: 160,
									items: [
										{fieldLabel: 'Fund Cash', name: 'fundCash', xtype: 'currencyfield'},
										{fieldLabel: 'Manager Cash', name: 'managerCash', xtype: 'currencyfield'},
										{fieldLabel: 'Transition Cash', name: 'transitionCash', xtype: 'currencyfield'},
										{
											fieldLabel: 'Rebalance Cash', xtype: 'compositefield', labelSeparator: '',
											defaults: {
												xtype: 'label',
												flex: 1
											},
											items: [
												{name: 'rebalanceCash', xtype: 'currencyfield'},
												{html: '&nbsp;Date:', flex: 0.5},
												{name: 'rebalanceDate', xtype: 'displayfield', type: 'date'}
											]
										},
										{fieldLabel: 'Rebalance Cash Adjusted', width: 100, name: 'rebalanceCashAdjusted', xtype: 'currencyfield'},
										{fieldLabel: 'Rebalance Attribution Cash', width: 100, name: 'rebalanceAttributionCash', xtype: 'currencyfield'},
										{fieldLabel: 'Total Rebalance Cash', width: 100, name: 'rebalanceCashAdjustedWithAttribution', xtype: 'currencyfield'},
										{fieldLabel: 'Client Directed Cash', name: 'clientDirectedCash', xtype: 'currencyfield'}

									]
								},
								{
									xtype: 'fieldset-checkbox',
									title: 'Durations',
									checkboxName: 'durationsUsed',
									labelWidth: 160,
									items: [
										{fieldLabel: 'Benchmark Duration', name: 'benchmarkDuration', xtype: 'currencyfield', decimalPrecision: 4},
										{fieldLabel: 'Credit Duration', name: 'benchmarkCreditDuration', xtype: 'currencyfield', decimalPrecision: 4},
										{fieldLabel: 'Effective Duration', name: 'effectiveDuration', xtype: 'currencyfield', decimalPrecision: 4},
										{fieldLabel: 'Effective vs. Benchmark', name: 'effectiveVsBenchmarkDuration', xtype: 'currencyfield', decimalPrecision: 4}
									]
								}
							]
						},

							{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},

							{
								columnWidth: .50,
								items: [
									{
										xtype: 'fieldset',
										title: 'Allocations',
										labelWidth: 200,
										items: [
											{
												fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
												defaults: {
													xtype: 'displayfield',
													style: 'text-align: right',
													flex: 1
												},
												items: [
													{value: 'Amount'},
													{value: 'Percentage (%)'}
												]
											},
											{
												fieldLabel: 'Actual Physical Exposure (Original)', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'actualAllocation'},
													{name: 'actualAllocationPercent'}
												]
											},
											{
												fieldLabel: 'Actual Physical Exposure (Adjusted)', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'actualAllocationAdjusted'},
													{name: 'actualAllocationAdjustedPercent'}
												]
											},

											{
												fieldLabel: 'Target Allocation (Original)', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'targetAllocation'},
													{name: 'targetAllocationPercent'}
												]
											},
											{
												fieldLabel: 'Target Allocation (Adjusted)', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'targetAllocationAdjusted'},
													{name: 'targetAllocationAdjustedPercent'}
												]
											},

											{
												fieldLabel: 'Physical Deviation from Adj Target', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'physicalDeviationFromAdjustedTarget'},
													{name: 'physicalDeviationFromAdjustedTargetPercent'}
												]
											},
											{
												fieldLabel: 'Overlay Exposure', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'overlayExposure'},
													{name: 'overlayExposurePercent'}
												]
											},
											{
												fieldLabel: 'Total Exposure', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'totalExposure'},
													{name: 'totalExposurePercent'}
												]
											},
											{
												fieldLabel: 'Total Exposure Deviation from Adjusted Target', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'totalExposureDeviationFromAdjustedTarget'},
													{name: 'totalExposureDeviationFromAdjustedTargetPercent'}
												]
											}]
									},
									{
										xtype: 'fieldset-checkbox',
										title: 'Rebalance Triggers',
										checkboxName: 'rebalanceTriggersUsed',
										labelWidth: 200,
										items: [
											{
												fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
												defaults: {
													xtype: 'displayfield',
													style: 'text-align: right',
													flex: 1
												},
												items: [
													{value: 'Amount'},
													{value: 'Percentage (%)'}
												]
											},
											{
												fieldLabel: 'Rebalance Exposure Target', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'rebalanceExposureTarget'},
													{name: 'rebalanceExposureTargetPercent'}
												]
											},
											{
												fieldLabel: 'Total Exposure Deviation from Rebalance Exposure Target', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'totalExposureDeviationFromRebalanceExposureTarget'},
													{name: 'totalExposureDeviationFromRebalanceExposureTargetPercent'}
												]
											},
											{
												fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
												defaults: {
													xtype: 'displayfield',
													style: 'text-align: right',
													flex: 1
												},
												items: [
													{value: 'Min'},
													{value: 'Max'}
												]
											},
											{
												fieldLabel: 'Trigger Amounts', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'rebalanceTriggerMin'},
													{name: 'rebalanceTriggerMax'}
												]
											},
											{
												fieldLabel: 'Absolute Trigger Amounts', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'rebalanceTriggerAbsoluteMin'},
													{name: 'rebalanceTriggerAbsoluteMax'}
												]
											},
											{xtype: 'label', html: '<hr/>'},
											{
												fieldLabel: 'Trigger %', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'rebalanceTriggerMinPercent'},
													{name: 'rebalanceTriggerMaxPercent'}
												]
											},
											{
												fieldLabel: 'Absolute Trigger %', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'rebalanceTriggerAbsoluteMinPercent'},
													{name: 'rebalanceTriggerAbsoluteMaxPercent'}
												]
											}
										]
									}]
							}]
					}]
			}]
		},


			{
				title: 'Manager Allocation',

				items: [{
					name: 'productOverlayManagerAccountListFind',
					xtype: 'gridpanel',
					instructions: 'Selected client account asset class has the following allocation of manager account cash and security balances.',
					appendStandardColumns: false,
					getLoadParams: function() {
						return {
							accountAssetClassId: TCG.getValue('accountAssetClass.id', this.getWindow().getMainForm().formValues),
							runId: TCG.getValue('overlayRun.id', this.getWindow().getMainForm().formValues)

						};
					},
					remoteSort: true,
					isPagingEnabled: function() {
						return false;
					},
					pageSize: 1000,
					columns: [
						{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
						{header: 'Asset Class', width: 140, dataIndex: 'accountAssetClass.label', hidden: true},
						{header: 'Manager', width: 170, dataIndex: 'managerAccountAssignment.managerLabel'},
						{header: 'Account', width: 75, dataIndex: 'managerAccountAssignment.referenceOne.accountNumber', hidden: true},

						{header: 'Securities', width: 100, dataIndex: 'securitiesAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Cash', width: 100, dataIndex: 'cashAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Total Market Value', width: 100, dataIndex: 'totalAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},

						{header: 'Previous Day Securities', width: 100, dataIndex: 'previousDaySecuritiesAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Day Cash', width: 100, dataIndex: 'previousDayCashAllocation', type: 'currency', hidden: true, summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Day Total', width: 100, dataIndex: 'previousDayTotalAllocation', type: 'currency', hidden: true, summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},

						{header: 'Previous Month End Securities', width: 100, dataIndex: 'previousMonthEndSecuritiesAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Month End Cash', width: 100, dataIndex: 'previousMonthEndCashAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Month End Total', width: 100, dataIndex: 'previousMonthEndTotalAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, hidden: true},

						{header: 'One Day % Change', width: 100, dataIndex: 'oneDayPercentChange', type: 'percent', summaryType: 'percentChange', previousValueField: 'previousDayTotalAllocation', currentValueField: 'totalAllocation', negativeInRed: true},
						{header: 'MTD % Change', width: 100, dataIndex: 'monthPercentChange', type: 'percent', summaryType: 'percentChange', previousValueField: 'previousMonthEndTotalAllocation', currentValueField: 'totalAllocation', negativeInRed: true},
						{header: 'Cash %', width: 65, dataIndex: 'cashAllocationPercent', type: 'percent', summaryType: 'percent', denominatorValueField: 'totalAllocation', numeratorValueField: 'cashAllocation', negativeInRed: true}
					],
					plugins: {ptype: 'groupsummary'},
					editor: {
						detailPageClass: 'Clifton.product.overlay.manager.ManagerAccountWindowSelector',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Replications',
				items: [{
					name: 'productOverlayAssetClassReplicationListByOverlayAssetClass',
					xtype: 'gridpanel',
					instructions: 'The following replications are configured for selected asset class.',
					standardColumns: [],
					getLoadParams: function() {
						return {
							overlayAssetClassId: this.getWindow().getMainForm().formValues.id,
							requestedMaxDepth: 5
						};
					},
					groupField: 'replication.name',
					groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Securities" : "Security"]})',
					remoteSort: true,
					columns: [
						{header: 'Replication', width: 140, dataIndex: 'replication.name', hidden: true},
						{header: 'Instrument', width: 170, dataIndex: 'security.instrument.name'},

						// Targets
						{header: 'IsTargetAdjusted', width: 50, dataIndex: 'targetAdjusted', type: 'boolean', hidden: true},
						{header: 'Target Allocation % (Original)', width: 80, dataIndex: 'allocationWeight', type: 'percent', summaryType: 'sum', hidden: true},
						{
							header: 'Target Allocation %', width: 80, dataIndex: 'allocationWeightAdjusted', type: 'percent', summaryType: 'sum', hideGrandTotal: true,
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, r.data['targetAdjusted'], r.data['allocationWeight'], '0,000.00 %');
							}
						},
						{header: 'Target Allocation Amount', width: 100, dataIndex: 'targetExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},


						// Contracts
						{header: 'Value', width: 85, dataIndex: 'value', type: 'currency', numberFormat: '0,000', negativeInRed: true},
						{header: 'Target Contracts', width: 85, dataIndex: 'targetContracts', type: 'currency', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
						{header: 'Actual Contracts (Original)', width: 85, dataIndex: 'actualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hidden: true},
						{
							header: 'Actual Contracts', width: 80, dataIndex: 'actualContractsAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, r.data['actualContracts'] !== v, r.data['actualContracts'], '0,000', 'Virtual Contracts');
							}
						},

						{header: 'Contracts to Trade', width: 85, dataIndex: 'tradedContracts', type: 'int', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
						{header: 'Total Contracts', width: 85, dataIndex: 'totalContracts', type: 'int', summaryType: 'sum', hidden: true, negativeInRed: true},

						{header: 'Security', width: 85, dataIndex: 'security.symbol'},
						{header: 'Underlying Security', width: 100, dataIndex: 'underlyingSecurity.symbol', hidden: true},
						{header: 'Underlying Security Price', width: 85, dataIndex: 'underlyingSecurityPrice', type: 'float', negativeInRed: true, useNull: true},
						{header: 'Security Price', width: 85, dataIndex: 'securityPrice', type: 'float', negativeInRed: true},

						{header: 'Days To Mature', width: 85, dataIndex: 'daysToMaturity', type: 'int', negativeInRed: true},
						{header: 'Interest Rate', width: 85, dataIndex: 'interestRate', type: 'currency', numberFormat: '0,000.0000', negativeInRed: true},
						{header: 'Duration', width: 85, dataIndex: 'duration', type: 'currency', numberFormat: '0,000.0000', negativeInRed: true, useNull: true},
						{header: 'Syn Adj Factor', width: 85, dataIndex: 'syntheticAdjustmentFactor', type: 'currency', numberFormat: '0,000.0000', negativeInRed: true, useNull: true}
					],
					plugins: {ptype: 'groupsummary', showGrandTotal: false},
					editor: {
						detailPageClass: 'Clifton.product.overlay.assetclass.ReplicationWindow',
						drillDownOnly: true
					}

				}]
			},


			{
				title: 'Position Allocation',
				items: [{
					xtype: 'investment-accountAssetClass-PositionAllocation',
					showAccountColumn: false,
					showAssetClassColumn: false,
					getAccountAssetClass: function() {
						return TCG.getValue('accountAssetClass', this.getWindow().getMainForm().formValues);
					},
					getDefaultActiveOnDate: function() {
						return TCG.parseDate(TCG.getValue('overlayRun.balanceDate', this.getWindow().getMainForm().formValues));
					}
				}]
			}
		]
	}]
});
