Clifton.product.overlay.assetclass.AssetClassCashAdjustmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Cash Adjustment',
	iconCls: 'transfer',
	width: 700,

	// Special Method so Replications are fully reloaded on Trade Creation Screen to get new ones retrieved and added to list
	reloadOpener: function() {
		const win = this;
		if (win.openerCt && win.openerCt.fullReload) {
			win.openerCt.fullReload();
		}
		else if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload) {
			win.openerCt.reload();
		}
	},

	items: [{
		xtype: 'formpanel',
		url: 'productOverlayAssetClassCashAdjustment.json',
		instructions: 'Select the asset class and from/to cash types to transfer an amount from one cash bucket to another for this specific Portfolio Run.  Note is required.',

		items: [
			{fieldLabel: 'Portfolio Run', name: 'overlayAssetClass.overlayRun.label', xtype: 'linkfield', detailPageClass: 'Clifton.portfolio.run.RunWindow', detailIdField: 'overlayAssetClass.overlayRun.id', submitDetailField: false},
			{
				fieldLabel: 'Asset Class', name: 'overlayAssetClass.label', hiddenName: 'overlayAssetClass.id', xtype: 'combo', url: 'productOverlayAssetClassListFind.json', displayField: 'label'
				, beforequery: function(queryEvent) {
					const runId = queryEvent.combo.getParentForm().getForm().findField('overlayAssetClass.overlayRun.id').value;
					queryEvent.combo.store.baseParams = {
						runId: runId,
						rollupAssetClass: false
					};
				}
			},
			{xtype: 'currencyfield', fieldLabel: 'Amount', name: 'amount'},

			{
				xtype: 'radiogroup', columns: 5, fieldLabel: 'From', name: 'fromCashType-Group',
				items: [
					{boxLabel: 'Fund', name: 'fromCashType', inputValue: 'FUND'},
					{boxLabel: 'Manager', name: 'fromCashType', inputValue: 'MANAGER'},
					{boxLabel: 'Transition', name: 'fromCashType', inputValue: 'TRANSITION'},
					{boxLabel: 'Rebalance', name: 'fromCashType', inputValue: 'REBALANCE'},
					{boxLabel: 'Client Directed', name: 'fromCashType', inputValue: 'CLIENT_DIRECTED'}
				]
			},
			{
				xtype: 'radiogroup', columns: 5, fieldLabel: 'To', name: 'toCashType-Group',
				items: [
					{boxLabel: 'Fund', name: 'toCashType', inputValue: 'FUND'},
					{boxLabel: 'Manager', name: 'toCashType', inputValue: 'MANAGER'},
					{boxLabel: 'Transition', name: 'toCashType', inputValue: 'TRANSITION'},
					{boxLabel: 'Rebalance', name: 'toCashType', inputValue: 'REBALANCE'},
					{boxLabel: 'Client Directed', name: 'toCashType', inputValue: 'CLIENT_DIRECTED'}
				]
			},

			{fieldLabel: 'Note', name: 'note', xtype: 'textarea'}
		]
	}]
});
