Clifton.product.overlay.assetclass.ReplicationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Overlay Asset Class Replication',
	iconCls: 'replicate',
	width: 900,
	height: 650,

	// Special Method so Replications are fully reloaded on Trade Creation Screen to get price overrides
	reloadOpener: function() {
		const win = this;
		if (win.openerCt && win.openerCt.fullReload) {
			win.openerCt.fullReload();
		}
		else if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload) {
			win.openerCt.reload();
		}
	},

	items: [
		{
			xtype: 'tabpanel',
			requiredFormIndex: 0,
			items: [
				{
					title: 'Replication Security Details',
					items: [
						{
							xtype: 'run-replication-formpanel',
							url: 'productOverlayAssetClassReplication.json',
							warningTypeLabel: 'asset classes',

							getRunProcessingTypeItems: function() {
								return [
									{name: 'overlayAssetClass.overlayRun.balanceDate', xtype: 'hidden', submitValue: false},
									{fieldLabel: 'Portfolio Run', name: 'overlayAssetClass.overlayRun.label', xtype: 'linkfield', detailIdField: 'overlayAssetClass.overlayRun.id', detailPageClass: 'Clifton.portfolio.run.RunWindow', submitValue: false, submitDetailField: false},
									{fieldLabel: 'Trading Client Account', name: 'overlayAssetClass.tradingClientAccount.label', xtype: 'linkfield', detailIdField: 'tradingClientAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow', submitValue: false, submitDetailField: false},
									{fieldLabel: 'Account Asset Class', name: 'overlayAssetClass.labelExpanded', xtype: 'linkfield', detailIdField: 'overlayAssetClass.accountAssetClass.id', detailPageClass: 'Clifton.investment.account.assetclass.AssetClassWindowSelector', submitValue: false}
								];
							}
						}]
				},

				{
					title: 'Pending Trades',
					items: [{xtype: 'run-replication-pending-trade-gridpanel'}]
				},

				{
					title: 'Pending Transfers',
					items: [{xtype: 'run-replication-pending-transfer-gridpanel'}]
				},

				{
					title: 'Position Allocation',
					items: [{
						xtype: 'investment-accountAssetClass-PositionAllocation',
						showAccountColumn: false,
						showAssetClassColumn: false,
						getAccountAssetClass: function() {
							return TCG.getValue('overlayAssetClass.accountAssetClass', this.getWindow().getMainForm().formValues);
						},
						getDefaultActiveOnDate: function() {
							return TCG.parseDate(TCG.getValue('overlayAssetClass.overlayRun.balanceDate', this.getWindow().getMainForm().formValues));
						},
						getSecurity: function() {
							return TCG.getValue('security', this.getWindow().getMainForm().formValues);
						}
					}]
				}
			]
		}]
});
