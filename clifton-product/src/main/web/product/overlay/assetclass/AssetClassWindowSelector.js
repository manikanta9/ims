// not the actual window but a window selector:
//   - rollup asset classes get RollupAssetClassWindow
//   - the rest get AssetClassWindow

Clifton.product.overlay.assetclass.AssetClassWindowSelector = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'productOverlayAssetClass.json',

	getClassName: function(config, entity) {
		if (entity && entity.rollupAssetClass) {
			return 'Clifton.product.overlay.assetclass.RollupAssetClassWindow';
		}
		return 'Clifton.product.overlay.assetclass.AssetClassWindow';
	}
});
