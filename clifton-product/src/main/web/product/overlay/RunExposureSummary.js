// NOTE: both grids use the same data but there are 2 separate calls: can make it more efficient later
Clifton.product.overlay.RunExposureSummary = {
	title: 'Exposure Summary',
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},
	frame: true,

	items: [{
		name: 'productOverlayAssetClassListFind',
		xtype: 'gridpanel',
		appendStandardColumns: false,
		title: 'Exposure Percent',
		instructions: 'Selected client account has the following percent allocation of asset classes from all managers.',
		border: 1,
		flex: 1,

		// Use global location so service type custom fields can select exactly what is available
		viewNames: Clifton.product.overlay.ExposureSummaryViewNames,

		listeners: {
			'afterRender': function() {
				const acct = TCG.getValue('clientInvestmentAccount', this.getWindow().getMainForm().formValues);
				const defaultView = Clifton.portfolio.run.getDefaultViewForAccount(acct, 'Exposure Summary View');
				if (defaultView) {
					this.setDefaultView(defaultView);
				}
			}
		},

		getLoadParams: function(firstLoad) {
			const nonRollupOnly = TCG.getChildByName(this.getTopToolbar(), 'nonRollupOnly').getValue();
			if (nonRollupOnly === true) {
				return {
					runId: this.getWindow().getMainForm().formValues.id,
					rollupAssetClass: false
				};
			}
			return {
				runId: this.getWindow().getMainForm().formValues.id,
				rootOnly: true
			};
		},
		topToolbarUpdateCountPrefix: '-',
		getTopToolbarFilters: function(toolbar) {
			return [
				{
					boxLabel: 'View Non Rollup Asset Class Level&nbsp;', xtype: 'checkbox', name: 'nonRollupOnly',

					listeners: {
						check: function(field) {
							TCG.getParentByClass(field, Ext.Panel).reload();
						}
					}
				}
			];
		},

		isPagingEnabled: function() {
			return false;
		},

		columns: [
			{
				header: 'Asset Class', width: 140, dataIndex: 'accountAssetClass.label',
				allViews: true
			},
			{header: 'Trading Account', width: 75, dataIndex: 'tradingClientAccount.number', hidden: true},
			{header: 'IsTargetAdjusted', width: 50, dataIndex: 'targetAdjusted', type: 'boolean', hidden: true},
			{header: 'TFA', width: 50, dataIndex: 'targetFollowsActual', type: 'boolean', hidden: true},
			{header: 'OutsideOfRebalanceTrigger', width: 50, dataIndex: 'outsideOfRebalanceTrigger', type: 'boolean', hidden: true},
			{header: 'OutsideOfAbsRebalanceTrigger', width: 50, dataIndex: 'outsideOfAbsoluteRebalanceTrigger', type: 'boolean', hidden: true},

			// Benchmark Information
			{header: 'Benchmark', width: 100, dataIndex: 'benchmarkSecurity.label', hidden: true, viewNames: ['Benchmark Returns']},
			{header: 'Benchmark Daily Return', width: 50, dataIndex: 'benchmarkOneDayReturn', type: 'percent', hidden: true, viewNames: ['Benchmark Returns']},
			{header: 'Benchmark MTD Return', width: 50, dataIndex: 'benchmarkMonthToDateReturn', type: 'percent', hidden: true, viewNames: ['Benchmark Returns']},

			// Percentage Columns
			{
				header: 'Policy Target', width: 80, dataIndex: 'targetAllocationPercent', type: 'percent', summaryType: 'sum', negativeInRed: true,
				viewNames: ['Overlay', 'Fully Funded']
			},
			{
				header: 'Adjusted Target', width: 80, dataIndex: 'targetAllocationAdjustedPercent', type: 'percent', summaryType: 'sum',
				allViews: true,
				renderer: function(v, p, r) {
					return TCG.renderAdjustedAmount(v, r.data['targetAdjusted'], r.data['targetAllocationPercent'], '0,000.00 %');
				}
			},
			{header: 'Rebalance Exposure Target', width: 80, dataIndex: 'rebalanceExposureTargetPercent', hidden: true, type: 'percent', summaryType: 'sum'},
			{header: 'Physical Exposure (Original)', width: 80, dataIndex: 'actualAllocationPercent', type: 'percent', summaryType: 'sum', hidden: true},
			{
				header: 'Physical Exposure', width: 80, dataIndex: 'actualAllocationAdjustedPercent', type: 'percent', summaryType: 'sum',
				viewNames: ['Overlay'],
				renderer: function(v, p, r) {
					return TCG.renderAdjustedAmount(v, r.data['actualAllocationPercent'] !== r.data['actualAllocationAdjustedPercent'], r.data['actualAllocationPercent'], '0,000.00 %');
				}
			},
			{
				header: 'Physical Deviation from Adj Target', width: 80, dataIndex: 'physicalDeviationFromAdjustedTargetPercent', type: 'percent', summaryType: 'sum', negativeInRed: true,
				viewNames: ['Overlay']
			},
			{
				header: 'Overlay Exposure', width: 80, dataIndex: 'overlayExposurePercent', type: 'percent', summaryType: 'sum', negativeInRed: true,
				viewNames: ['Overlay']
			},
			{
				header: 'Total Exposure', width: 70, dataIndex: 'totalExposurePercent', type: 'percent', summaryType: 'sum', negativeInRed: true,
				viewNames: ['Overlay', 'Fully Funded'],
				viewNameHeaders: [
					{name: 'Overlay', label: 'Total Exposure'},
					{name: 'Fully Funded', label: 'Exposure'}
				]
			},
			{
				header: 'Total Exposure Deviation from Adj Target', width: 90, dataIndex: 'totalExposureDeviationFromAdjustedTargetPercent', type: 'percent', summaryType: 'sum',
				viewNames: ['Overlay', 'Fully Funded'],
				viewNameHeaders: [
					{name: 'Overlay', label: 'Total Exposure Deviation from Adj Target'},
					{name: 'Fully Funded', label: 'Exposure Deviation from Adj Target'}
				],
				renderer: function(v, p, r) {
					if (v === undefined || v === '') {
						return '';
					}
					if (r.data['outsideOfRebalanceTrigger'] === true) {
						const value = Ext.util.Format.number(v, '0,000.00 %');
						if (r.data['outsideOfAbsoluteRebalanceTrigger'] === true) {
							return '<div class="ruleViolation"><b>' + value + '</b></div>';
						}
						return '<div class="ruleViolation">' + value + '</div>';
					}
					return TCG.renderAmount(v, false, '0,000.00 %');
				}
			},
			{
				header: 'Total Exposure Deviation From Rebalance Exposure Target', hidden: true, width: 90, dataIndex: 'totalExposureDeviationFromRebalanceExposureTargetPercent', type: 'percent', summaryType: 'sum',
				renderer: function(v, p, r) {
					if (v === undefined || v === '') {
						return '';
					}
					if (r.data['outsideOfRebalanceTrigger'] === true) {
						const value = Ext.util.Format.number(v, '0,000.00 %');
						if (r.data['outsideOfAbsoluteRebalanceTrigger'] === true) {
							return '<div class="ruleViolation"><b>' + value + '</b></div>';
						}
						return '<div class="ruleViolation">' + value + '</div>';
					}
					return TCG.renderAmount(v, false, '0,000.00 %');
				}
			},
			// Percents - Rebalance Triggers
			{
				header: 'Min Rebalance Trigger', width: 80, dataIndex: 'rebalanceTriggerMinPercent', type: 'percent',
				viewNames: ['Overlay', 'Fully Funded']
			},
			{
				header: 'Max Rebalance Trigger', width: 80, dataIndex: 'rebalanceTriggerMaxPercent', type: 'percent',
				viewNames: ['Overlay', 'Fully Funded']
			},
			// Absolutes
			{header: 'Absolute Min Trigger', width: 80, dataIndex: 'rebalanceTriggerAbsoluteMinPercent', type: 'percent', hidden: true},
			{header: 'Absolute Max Trigger', width: 80, dataIndex: 'rebalanceTriggerAbsoluteMaxPercent', type: 'percent', hidden: true}
		],
		plugins: {ptype: 'gridsummary'},
		editor: {
			detailPageClass: 'Clifton.product.overlay.assetclass.AssetClassWindowSelector',
			drillDownOnly: true
		}
	},

		{flex: 0.02},

		{
			name: 'productOverlayAssetClassListFind',
			xtype: 'gridpanel',
			appendStandardColumns: false,
			title: 'Exposure in Base Currency',
			instructions: 'Selected client account has the following allocation of asset classes from all managers.',
			border: 1,
			flex: 1,

			viewNames: Clifton.product.overlay.ExposureSummaryViewNames,

			listeners: {
				'afterRender': function() {
					const acct = TCG.getValue('clientInvestmentAccount', this.getWindow().getMainForm().formValues);
					const defaultView = Clifton.portfolio.run.getDefaultViewForAccount(acct, 'Exposure Summary View');
					if (defaultView) {
						this.setDefaultView(defaultView);
					}
				}
			},

			getLoadParams: function() {
				const nonRollupOnly = TCG.getChildByName(this.getTopToolbar(), 'nonRollupOnly').getValue();
				if (nonRollupOnly === true) {
					return {
						runId: this.getWindow().getMainForm().formValues.id,
						rollupAssetClass: false
					};
				}
				return {
					runId: this.getWindow().getMainForm().formValues.id,
					rootOnly: true
				};
			},
			topToolbarUpdateCountPrefix: '-',
			getTopToolbarFilters: function(toolbar) {
				return [
					{
						boxLabel: 'View Non Rollup Asset Class Level&nbsp;', xtype: 'checkbox', name: 'nonRollupOnly',

						listeners: {
							check: function(field) {
								TCG.getParentByClass(field, Ext.Panel).reload();
							}
						}
					}
				];
			},
			isPagingEnabled: function() {
				return false;
			},

			columns: [
				{
					header: 'Asset Class', width: 140, dataIndex: 'accountAssetClass.label',
					allViews: true
				},
				{header: 'Trading Account', width: 75, dataIndex: 'tradingClientAccount.number', hidden: true},
				{header: 'IsTargetAdjusted', width: 50, dataIndex: 'targetAdjusted', type: 'boolean', hidden: true},
				{header: 'TFA', width: 50, dataIndex: 'targetFollowsActual', type: 'boolean', hidden: true},
				{header: 'OutsideOfRebalanceTrigger', width: 50, dataIndex: 'outsideOfRebalanceTrigger', type: 'boolean', hidden: true},
				{header: 'OutsideOfAbsRebalanceTrigger', width: 50, dataIndex: 'outsideOfAbsoluteRebalanceTrigger', type: 'boolean', hidden: true},

				// Benchmark Information (Same as above - no difference between % or amount views)
				{header: 'Benchmark', width: 100, dataIndex: 'benchmarkSecurity.label', hidden: true, viewNames: ['Benchmark Returns']},
				{header: 'Benchmark Daily Return', width: 50, dataIndex: 'benchmarkOneDayReturn', type: 'percent', hidden: true, viewNames: ['Benchmark Returns']},
				{header: 'Benchmark MTD Return', width: 50, dataIndex: 'benchmarkMonthToDateReturn', type: 'percent', hidden: true, viewNames: ['Benchmark Returns']},

				// Amount Columns
				{
					header: 'Policy Target', width: 80, dataIndex: 'targetAllocation', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
					viewNames: ['Overlay', 'Fully Funded']
				},
				{
					header: 'Adjusted Target', width: 80, dataIndex: 'targetAllocationAdjusted', type: 'currency', summaryType: 'sum',
					allViews: true,
					renderer: function(v, p, r) {
						return TCG.renderAdjustedAmount(v, r.data['targetAdjusted'], r.data['targetAllocation'], '0,000');
					}
				},
				{header: 'Rebalance Exposure Target', width: 80, dataIndex: 'rebalanceExposureTarget', hidden: true, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
				{header: 'Physical Exposure (Original)', width: 80, dataIndex: 'actualAllocation', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hidden: true},
				{
					header: 'Physical Exposure', width: 80, dataIndex: 'actualAllocationAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum',
					viewNames: ['Overlay', 'Fully Funded'],
					renderer: function(v, p, r) {
						return TCG.renderAdjustedAmount(v, r.data['actualAllocation'] !== r.data['actualAllocationAdjusted'], r.data['actualAllocation'], '0,000');
					}
				},
				{
					header: 'Physical Deviation from Adj Target', width: 80, dataIndex: 'physicalDeviationFromAdjustedTarget', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
					viewNames: ['Overlay', 'Fully Funded']
				},
				{
					header: 'Overlay Exposure', width: 80, dataIndex: 'overlayExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
					viewNames: ['Overlay', 'Fully Funded']
				},
				{
					header: 'Total Exposure', width: 80, dataIndex: 'totalExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
					viewNames: ['Overlay', 'Fully Funded'],
					viewNameHeaders: [
						{name: 'Overlay', label: 'Total Exposure'},
						{name: 'Fully Funded', label: 'Exposure'}
					]
				},
				{
					header: 'Total Exposure Deviation from Adj Target', width: 80, dataIndex: 'totalExposureDeviationFromAdjustedTarget', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
					viewNames: ['Overlay', 'Fully Funded'],
					viewNameHeaders: [
						{name: 'Overlay', label: 'Total Exposure Deviation from Adj Target'},
						{name: 'Fully Funded', label: 'Exposure Deviation from Adj Target'}
					],
					renderer: function(v, p, r) {
						if (v === undefined || v === '') {
							return '';
						}
						if (r.data['outsideOfRebalanceTrigger'] === true) {
							const value = Ext.util.Format.number(v, '0,000');
							if (r.data['outsideOfAbsoluteRebalanceTrigger'] === true) {
								return '<div class="ruleViolation"><b>' + value + '</b></div>';
							}
							return '<div class="ruleViolation">' + value + '</div>';
						}
						return TCG.renderAmount(v, false, '0,000');
					}
				},
				{
					header: 'Total Exposure Deviation From Rebalance Exposure Target', hidden: true, width: 90, dataIndex: 'totalExposureDeviationFromRebalanceExposureTarget', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
					renderer: function(v, p, r) {
						if (v === undefined || v === '') {
							return '';
						}
						if (r.data['outsideOfRebalanceTrigger'] === true) {
							const value = Ext.util.Format.number(v, '0,000');
							if (r.data['outsideOfAbsoluteRebalanceTrigger'] === true) {
								return '<div class="ruleViolation"><b>' + value + '</b></div>';
							}
							return '<div class="ruleViolation">' + value + '</div>';
						}
						return TCG.renderAmount(v, false, '0,000');
					}
				},
				// Amounts - Rebalance Triggers
				{
					header: 'Min Rebalance Trigger', width: 80, dataIndex: 'rebalanceTriggerMin', type: 'currency', numberFormat: '0,000',
					viewNames: ['Overlay', 'Fully Funded']
				},
				{
					header: 'Max Rebalance Trigger', width: 80, dataIndex: 'rebalanceTriggerMax', type: 'currency', numberFormat: '0,000',
					viewNames: ['Overlay', 'Fully Funded']
				},
				// Absolutes
				{header: 'Absolute Min Trigger', width: 80, dataIndex: 'rebalanceTriggerAbsoluteMin', type: 'currency', numberFormat: '0,000', hidden: true},
				{header: 'Absolute Max Trigger', width: 80, dataIndex: 'rebalanceTriggerAbsoluteMax', type: 'currency', numberFormat: '0,000', hidden: true}
			],
			plugins: {ptype: 'gridsummary'},
			editor: {
				detailPageClass: 'Clifton.product.overlay.assetclass.AssetClassWindowSelector',
				drillDownOnly: true
			}
		}]
};
