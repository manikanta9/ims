TCG.use('Clifton.portfolio.run.trade.BaseRunTradeWindow');

TCG.use('Clifton.portfolio.run.trade.BaseRunTradeForm');

//NOTE: THIS SCREEN WAS BROKEN OUT INTO INDIVIDUAL FILES, HOWEVER THEY DO DEPEND ON EACH OTHER
//FOR EXAMPLE - ENTERING B/S TRADES IN THE REPLICATION GRID WILL UPDATE THE CASH EXPOSURE RECAP, PUSH WARNINGS THROUGH, ETC.

//TOP LEFT - CURRENCY POSITIONS FORM GRID
TCG.use('Clifton.product.overlay.trade.RunTradeCurrencyPositionGrid');

//TOP RIGHT - Cash Exposure Recap Fieldset
TCG.use('Clifton.product.overlay.trade.RunTradeCashExposureRecapFieldSet');

//BOTTOM - REPLICATION LIST FORM GRID
TCG.use('Clifton.product.overlay.trade.RunTradeReplicationGrid');

TCG.use('Clifton.product.overlay.RunManagerBalance');

Clifton.product.overlay.trade.RunTradeOverlayWindow = Ext.extend(Clifton.portfolio.run.trade.BaseRunTradeWindow, {

	dtoClassForBinding: 'com.clifton.product.overlay.trade.ProductOverlayRunTrade',

	additionalRunTabs: [
		Clifton.product.overlay.RunManagerBalance
	],

	runTradeEntryForm: {
		xtype: 'portfolio-run-trade-form',

		listRequestedProperties: 'id|overlayAssetClass.accountAssetClass.id|labelLong|tradeEntryDisabled|securityNotTradable|cashExposure|tradingClientAccount.id|overlayAssetClass.label|overlayAssetClass.benchmarkDuration|overlayAssetClass.benchmarkCreditDuration|overlayAssetClass.effectiveDuration|overlayAssetClass.rebalanceTriggerWarningsUsed|overlayAssetClass.rebalanceTriggerMin|overlayAssetClass.rebalanceTriggerMax|overlayAssetClass.rebalanceTriggerAbsoluteMin|overlayAssetClass.rebalanceTriggerAbsoluteMax|overlayAssetClass.rebalanceExposureTarget|overlayAssetClass.actualAllocationAdjusted|replication.name|replicationType.currency|replicationType.durationSupported|security.id|security.symbol|security.endDate|security.description|securityPrice|tradeSecurityPrice|tradeSecurityPriceDate|underlyingSecurityPrice|tradeUnderlyingSecurityPrice|tradeUnderlyingSecurityPriceDate|securityDirtyPrice|tradeSecurityDirtyPrice|tradeSecurityDirtyPriceDate|exchangeRate|tradeExchangeRate|tradeExchangeRateDate|value|tradeValue|additionalExposure|additionalOverlayExposure|totalAdditionalExposure|targetExposureAdjusted|actualContracts|actualContractsAdjusted|virtualContracts|currentContracts|currentVirtualContracts|currentContractsAdjusted|pendingContracts|pendingVirtualContracts|pendingContractsAdjusted|buyContracts|sellContracts|trade.tradeDestination.id|trade.tradeDestination.name|trade.tradeExecutionType.id|trade.tradeExecutionType.name|trade.executingBrokerCompany.id|trade.executingBrokerCompany.label|trade.holdingInvestmentAccount.id|trade.holdingInvestmentAccount.label|duration|delta|contractValuePriceField|contractValuePrice|security.priceMultiplier|markToMarketAdjustmentValue|targetExposureAdjustedWithMarkToMarket|currencyActualLocalAmount|currencyExchangeRate|currencyOtherBaseAmount|currencyCurrentLocalAmount|tradeCurrencyExchangeRate|currencyPendingLocalAmount|currencyCurrentOtherBaseAmount|currencyPendingOtherBaseAmount|currencyCurrentTotalBaseAmount|currencyPendingTotalBaseAmount|matchingReplication|currencyUnrealizedLocalAmount|currencyCurrentUnrealizedLocalAmount|currencyCurrentUnrealizedTradeBaseAmount|currencyDenominationBaseAmount|currencyCurrentDenominationBaseAmount|currencyPendingDenominationBaseAmount|allocationWeightAdjusted|reverseExposureSign|rebalanceTriggerMin|rebalanceTriggerMax|security.instrument.fairValueAdjustmentUsed|fairValueAdjustment|tradeFairValueAdjustment|tradeFairValueAdjustmentDate|previousFairValueAdjustment|mispricingValue|mispricingTradeValue|security.instrument.hierarchy.investmentType.name',
		requestedMaxDepth: 7,

		rebalanceWarnings: [],
		assetClassFinalExposure: [],

		getConfirmBeforeSaveMsg: function() {
			const f = this.getForm();

			let warning = f.findField('overlayLimitWarning').getValue();
			if (TCG.isNotBlank(warning)) {
				warning = warning + '<br><br>';
			}

			for (let i = 0; i < this.rebalanceWarnings.length; i++) {
				const acWarn = this.rebalanceWarnings[i];
				if (TCG.isNotBlank(acWarn.msg)) {
					warning += '<b>' + acWarn.name + '</b> ' + acWarn.msg + '<br><br>';
				}
			}
			warning += this.confirmBeforeSaveMsg;
			return warning;
		},

		executeAccountNavigation: function(tabName) {
			this.tabName = tabName;
			const clientId = TCG.getValue('clientInvestmentAccount.id', this.getForm().formValues);
			const clz = 'Clifton.investment.account.AccountWindow';
			const w = this.getWindow();
			const id = w.getMainFormId();
			const cmpId = TCG.getComponentId(clz, id);
			TCG.createComponent(clz, {
				id: cmpId,
				params: {id: clientId, processingType: this.getFormValue('serviceProcessingType')},
				openerCt: this,
				defaultActiveTabName: tabName,
				listeners: {
					activate: function() {
						const o = this.win.items.get(0);
						const tabNameClicked = this.openerCt.tabName;
						if (tabNameClicked) {
							for (let i = 0; i < o.items.length; i++) {
								if (o.items.get(i).title === tabNameClicked) {
									o.setActiveTab(i);
									break;
								}
							}
						}
					}
				}
			});
		},

		items: [
			// Need to add id as a hidden field and submitValue = false so that id is not double submitted
			{name: 'id', xtype: 'hidden', submitValue: false},
			{
				xtype: 'panel',
				layout: 'column',
				items: [{
					columnWidth: .58,
					layout: 'form',
					items: [
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {layout: 'form'},
							items: [
								{
									columnWidth: 1,
									items: [
										{fieldLabel: 'Portfolio Run', name: 'label', xtype: 'linkfield', detailIdField: 'id', detailPageClass: 'Clifton.portfolio.run.RunWindow'}
									]
								},
								{
									width: 100,
									items: [
										{
											text: 'Navigate to', xtype: 'button', style: {float: 'right'},
											menu: {
												defaults: {iconCls: 'account'},
												items: [{
													text: 'Client Account',
													handler: function() {
														TCG.getParentFormPanel(this).executeAccountNavigation('Info');
													}
												}, {
													text: 'Managers',
													handler: function() {
														TCG.getParentFormPanel(this).executeAccountNavigation('Managers');
													}
												}, {
													text: 'Asset Classes',
													handler: function() {
														TCG.getParentFormPanel(this).executeAccountNavigation('Asset Classes');
													}
												}, {
													text: 'Holdings',
													handler: function() {
														TCG.getParentFormPanel(this).executeAccountNavigation('Holdings');
													}
												}]
											}
										}
									]
								}
							]
						},

						// Currency Positions Grid with link to create trade
						Clifton.product.overlay.trade.RunTradeCurrencyPositionGrid
					]
				},
					{columnWidth: .02, items: [{html: '&nbsp;'}]},
					{
						columnWidth: .40,
						items: [
							Clifton.product.overlay.trade.RunTradeCashExposureRecapFieldSet
						]
					}
				]
			},

			{xtype: 'displayfield', name: 'overlayLimitWarning', cls: 'warning-msg', hidden: true},
			{xtype: 'hidden', name: 'excludeMispricing'},

			Clifton.product.overlay.trade.RunTradeReplicationGrid

		]

	}

});


