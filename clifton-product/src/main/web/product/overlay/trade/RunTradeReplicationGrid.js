// PORTFOLIO RUN - TRADE CREATION WINDOW - REPLICATION LIST FORM GRID
TCG.use('Clifton.portfolio.run.trade.BaseRunTradeDetailGrid');
Clifton.product.overlay.trade.RunTradeReplicationGrid = Ext.applyIf({

	dtoClass: 'com.clifton.product.overlay.ProductOverlayAssetClassReplication',
	detailPageClass: 'Clifton.product.overlay.assetclass.ReplicationWindow',
	urlPrefix: 'productOverlayRunTrade',
	height: 264,

	// Group on Asset Class
	groupField: 'labelLong',
	groupTextTpl: '{values.group}&nbsp;<span class="amountNegative" id="{groupId}-fe">&nbsp;</span>&nbsp;<span id="{groupId}-rb">&nbsp;</span>&nbsp;<span class="amountAdjusted" id="{groupId}-mw">&nbsp;</span>&nbsp;<span id="{groupId}-td">&nbsp;</span>&nbsp;',

	// These fields may not be edited and may just use default value, so we always need them submitted
	// Send pricing to the server so that if we are trading, we can update the trade prices on the replication
	alwaysSubmitFields: ['tradeSecurityPrice', 'tradeSecurityPriceDate', 'tradeUnderlyingSecurityPrice', 'tradeUnderlyingSecurityPriceDate', 'tradeSecurityDirtyPrice', 'tradeSecurityDirtyPriceDate', 'tradeExchangeRate', 'tradeExchangeRateDate', 'trade.executingBrokerCompany.id', 'trade.tradeDestination.id', 'trade.tradeExecutionType.id', 'trade.holdingInvestmentAccount.id', 'buyContracts', 'sellContracts', 'fairValueAdjustment', 'previousFairValueAdjustment'],
	doNotSubmitFields: ['Target Overlay %', 'Final Overlay %', 'Target - Final Overlay %', 'approveTrade'],

	// Stored in shared js so can re-use options for service type screen view selections
	viewNames: Clifton.product.overlay.trade.TradeViewNames,
	supportMispricingView: true,

	/***********************************************************************
	 * GRID LOADING/RE-LOADING/CLEAR MODIFIED
	 * *********************************************************************/

	// Called when new securities are added to fully retrieve replication list again, not just update the list
	fullReload: function() {
		// Default Reload - Reload Prices - also fully reloads replications
		this.reload();
		// Reset Target Dependencies so can be reloaded
		this.targetDependencyMap = undefined;
	},

	afterReloadGrid: function(grid, panel, record) {
		Clifton.product.overlay.trade.updateTargets(grid);
	},

	afterFormLoad: function(grid, panel) {
		grid.loadMispricing(true);
		grid.loadOverlayLimit();
		panel.rebalanceWarnings = []; // reset - seems to keep from previously opened runs
		panel.assetClassFinalExposure = [];
		if (TCG.isTrue(grid.getWindow().savedSinceOpen)) {
			grid.reloadTrades();
		}
		else {
			// Reset Target Dependencies based on trades entered
			Clifton.product.overlay.trade.updateTargets(grid);
		}
		// Enable Drag And Drop Feature to Attached "Client Trade Direction" notes
		TCG.file.enableDD(grid, grid.getNoteParams(), null, 'tradeNoteForTradeGroupBySourceUpload.json', false);
	},

	afterClearModified: function() {
		// Reset Target Dependencies based on trades entered
		Clifton.product.overlay.trade.updateTargets(this);
		// Reset trade execution type column required flags
		const columnModel = this.getColumnModel();
		const limitPriceColumnIndex = columnModel.findColumnIndex('trade.limitPrice');
		const limitPriceColumnEditor = columnModel.getCellEditor(limitPriceColumnIndex, 0);
		if (TCG.isNotNull(limitPriceColumnEditor.field.valueRequired)) {
			limitPriceColumnEditor.field.valueRequired = {};
		}
	},


	/***********************************************************************
	 * TOOLBAR - ADD SECURITY BUTTON
	 * *********************************************************************/

	addButton: function(toolBar, gp, buttonName) {
		toolBar.add({
			text: 'Add Security',
			iconCls: 'add',
			tooltip: 'Uses selected Overlay Replication Row as a model and allows adding a new security.',
			scope: this,
			handler: function() {
				if (TCG.isTrue(gp.validateTradingAllowedRun())) {
					const index = gp.getSelectionModel().getSelectedCell();
					if (index) {
						const store = gp.getStore();
						const rec = store.getAt(index[0]);

						if (TCG.isTrue(rec.json.tradeEntryDisabled)) {
							TCG.showError('Cannot add a new security to a replication where Trading is Disabled.  Please add the security to a replication where trading is not disabled and it will automatically be added here.', 'Read Only');
							return;
						}

						let className = 'Clifton.product.overlay.assetclass.AddNewReplicationWindow';
						if (TCG.getValue('security.instrument.hierarchy.investmentType.name', rec.json) === 'Options') {
							className = 'Clifton.product.overlay.assetclass.AddNewOptionReplicationWindow';
						}

						const id = rec.id;

						const cmpId = TCG.getComponentId(className, id);
						TCG.createComponent(className, {
							id: cmpId,
							params: {id: id},
							openerCt: gp
						});
					}
					else {
						TCG.showError('A row must be selected in order to add a new security to the run.  The selected Overlay Replication Row is used as a model and to define the asset class and replication the security belongs to as well as filtering which securities are allowed for selection.', 'No Row Selected');
					}
				}
			}

		});
		toolBar.add('-');
	},


	/***********************************************************************
	 * GRID EDITING
	 * *********************************************************************/


	onAfterGrandTotalUpdateFieldValue: function(v, cf) {
		// NOTE: USE FORM PANEL setFormValue METHOD SO WINDOW IS NOT MARKED AS MODIFIED!
		if (cf.dataIndex === 'Mispricing Difference') {
			this.setFormCalculatedValue('cashExposure', v);
		}
		if (cf.dataIndex === 'mispricingTradeValue') {
			this.setFormCalculatedValue('mispricingTotal', v);
		}
		if (cf.dataIndex === 'Final Overlay Difference') {
			this.setFormCalculatedValue('finalCashExposure', v);
		}
		const grid = this;
		const items = grid.store.data.items;

		if (cf.dataIndex === 'Overlay Target') {
			let i = 0;
			let row = items[i];
			// Set Overlay Target Total on the Row so we have it for % columns
			// NOTE Not sure of a better way to do this so that the screen updates automatically
			// from reading the field on the form?
			while (row) {
				row.data.overlayTargetTotal = v;
				// Get the next row
				i = i + 1;
				row = items[i];
			}
		}

		if (cf.dataIndex === 'Final Overlay Exposure') {
			let i = 0;
			let row = items[i];

			let currentAc = '';
			let fe = 0;
			let mfe = 0;

			while (row) {
				// The final exposure for the row
				const finalExp = Clifton.product.overlay.trade.calculateFinalExposure(row);

				// Then track the asset class totals, when get to matching, need to set info msg as
				// comparison of matching final overlay exposure to the non matching reps for that asset class
				const thisAc = row.data['overlayAssetClass.label'];
				if (TCG.isBlank(currentAc)) {
					currentAc = thisAc;
				}

				if (currentAc === thisAc) {
					if (TCG.isFalse(row.data['matchingReplication'])) {
						// Track total for the asset class
						if (TCG.isTrue(row['hasMatchingReplication'])) {
							fe = fe + finalExp;
						}
					}
					else {
						// When matching, track total for matching for the asset class
						mfe = mfe + finalExp;
					}
				}

				// Get the next row
				i = i + 1;
				const nextRow = items[i];

				// If next row doesn't exist (i.e. last row, or next row is for a different asset class)
				// and current row is matching and final exposure is not zero - set the info message
				if (!nextRow || nextRow.data['overlayAssetClass.label'] !== currentAc) {
					if (TCG.isTrue(row.data['matchingReplication']) && fe !== 0) {
						let matchPercent = mfe / fe * 100;
						if (matchPercent < 0) {
							matchPercent = matchPercent * -1;
						}
						const msg = 'Matching Replication Final Overlay Exposure is ' + Ext.util.Format.number(matchPercent, '0,000.0000') + '% of non-matching';
						const obj = Ext.get(row['_groupId'] + '-mw');
						if (TCG.isNotNull(obj)) {
							obj.update(msg);
						}
					}
					if (nextRow) {
						// Reset totals for next asset class and change current asset class
						currentAc = nextRow.data['overlayAssetClass.label'];
						fe = 0;
						mfe = 0;
					}
				}
				// Move row to nextRow
				row = nextRow;
			}


			const f = TCG.getParentFormPanel(this).getForm();
			const netOverlayField = f.findField('netOverlay');
			const pv = v - (netOverlayField ? netOverlayField.getNumericValue() : 0);
			this.setFormCalculatedValue('pendingOverlay', pv);
			this.setOverlayLimitValues(v);

		}
		if (cf.dataIndex === 'Overlay Exposure') {
			this.setFormCalculatedValue('netOverlay', v);
		}

		this.extendedOnAfterGrandTotalUpdateFieldValue(v, cf);
	},

	extendedOnAfterGrandTotalUpdateFieldValue: function(v, cf) {
		// Can be implemented by implementations extending this for additional calculations.
	},

	overlayLimitPercent: undefined,
	loadOverlayLimit: function() {
		const f = TCG.getParentFormPanel(this).getForm();
		const netOverlayPercentField = f.findField('netOverlayPercent'),
			pendingOverlayPercentField = f.findField('pendingOverlayPercent');
		if (netOverlayPercentField && pendingOverlayPercentField) {
			if (TCG.isNull(this.overlayLimitPercent)) {
				// Lookup if undefined
				const actId = TCG.getValue('clientInvestmentAccount.id', f.formValues);
				const url = 'ruleConfigMaxAmountForAdditionalScopeEntityAndDefinition.json?categoryName=Portfolio Run Rules&definitionName=Synthetically Adjusted Positions Exposure Range&additionalScopeEntityId=' + actId;
				TCG.data.getDataValue(url, this, function(value) {
					// Set to '' if no limit defined so we don't try to look it up again
					this.overlayLimitPercent = value;
					if (TCG.isNull(this.overlayLimitPercent)) {
						this.overlayLimitPercent = '';
					}
					this.setOverlayLimitValues(netOverlayPercentField.getNumericValue() + pendingOverlayPercentField.getNumericValue());
				}, this);
			}
			else {
				this.setOverlayLimitValues(netOverlayPercentField.getNumericValue() + pendingOverlayPercentField.getNumericValue());
			}
		}
	},

	setOverlayLimitValues: function(actual) {
		// No Limit or no actual, do nothing
		if (TCG.isBlank(this.overlayLimitPercent)) {
			return;
		}
		const fp = TCG.getParentFormPanel(this);
		const ptv = TCG.getValue('portfolioTotalValue', fp.getForm().formValues);
		if (TCG.isNotNull(ptv) && ptv !== 0) {
			const actualPercent = (actual * 100) / ptv;
			const overLimitPercent = this.overlayLimitPercent - actualPercent;

			// If negative result, then we are over limit
			const field = fp.getForm().findField('overlayLimitWarning');
			if (overLimitPercent < 0) {
				field.setVisible(true);
				fp.setFormValue('overlayLimitWarning', '<b>Warning:</b>&nbsp;Account has Synthetic Exposure Limit of ' + this.overlayLimitPercent + ' %. Final Exposure from Net/Pending Overlay is currently ' + Ext.util.Format.number((-1 * overLimitPercent), '0,000.00') + ' % over limit.', true);
			}
			else {
				fp.setFormValue('overlayLimitWarning', '', true);
				field.setVisible(false);
			}
			fp.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		}
	},

	setFormCalculatedValue: function(fieldName, v) {
		const f = TCG.getParentFormPanel(this);
		f.setFormValue(fieldName, Ext.util.Format.number(v, '0,000'), true);

		const ptv = TCG.getValue('portfolioTotalValue', f.getForm().formValues);
		if (TCG.isNotNull(ptv) && ptv !== 0) {
			const percentV = (v * 100) / ptv;
			f.setFormValue(fieldName + 'Percent', Ext.util.Format.number(percentV, '0,000.00'), true);
		}
	},


	onAfterEdit: function(editor, field) {
		const grid = this;
		const f = editor.field;

		if (f === 'buyContracts' || f === 'sellContracts') {
			const v = editor.value;
			const entryDisabled = editor.record.get('tradeEntryDisabled');
			const record = editor.record;
			const repId = record.get('id');
			Clifton.product.overlay.trade.copyTradesToTradeEntryDisabledRows(grid, repId, entryDisabled, f, v);
			// update record open/close type
			Clifton.portfolio.run.trade.updateRecordOpenCloseType(grid, record, (f === 'buyContracts'));
		}
	},

	switchToViewUpdateViewsToggleOptions: function(viewName, columnModel, showOptions) {
		showOptions.forEach(item => {
			if (item.text === 'Show Trade Note Column') {
				if (!TCG.isTrue(item.checked)) {
					if (item.rendered) {
						item.setChecked(true);
					}
					else {
						item.checked = true;
					}
					const columnIndex = columnModel.findColumnIndex('trade.description');
					if (columnIndex) {
						columnModel.setHidden(columnIndex, false);
					}
				}
			}
		});
	},

	/***********************************************************************
	 * DETAIL COLUMN DEFINITIONS -
	 * *********************************************************************/

	detailColumns: [
		{header: 'AccountAssetClassID', hidden: true, width: 10, dataIndex: 'overlayAssetClass.accountAssetClass.id'},
		{header: 'Trading Disabled', hidden: true, width: 15, dataIndex: 'tradeEntryDisabled', type: 'boolean'},
		{header: 'Security Not Tradable', hidden: true, width: 15, dataIndex: 'securityNotTradable', type: 'boolean'},

		{header: 'Asset Class Replication', hidden: true, width: 150, dataIndex: 'labelLong'},
		{header: 'RollupAssetClass', hidden: true, width: 150, dataIndex: 'overlayAssetClass.topLevelLabel'},
		{header: 'AssetClass', hidden: true, width: 150, dataIndex: 'overlayAssetClass.label'},
		{header: 'AssetClass Benchmark Duration', hidden: true, width: 50, dataIndex: 'overlayAssetClass.benchmarkDuration', numberFormat: '0,000.0000', useNull: true},
		{header: 'AssetClass Benchmark Credit Duration', hidden: true, width: 50, dataIndex: 'overlayAssetClass.benchmarkCreditDuration', numberFormat: '0,000.0000', useNull: true},
		{header: 'AssetClass Effective Duration', hidden: true, width: 50, dataIndex: 'overlayAssetClass.effectiveDuration', numberFormat: '0,000.0000', useNull: true},

		{header: 'Replication', hidden: true, width: 140, dataIndex: 'replication.name'},
		{header: 'Currency Replication', hidden: true, width: 40, dataIndex: 'replicationType.currency', type: 'boolean'},
		{header: 'Duration Supported', hidden: true, width: 40, dataIndex: 'replicationType.durationSupported', type: 'boolean'},
		{header: 'Matching Replication', hidden: true, width: 40, dataIndex: 'matchingReplication', type: 'boolean'},
		{header: 'Reverse Exposure', hidden: true, width: 40, dataIndex: 'reverseExposureSign', type: 'boolean'},
		{header: 'Cash Exposure', hidden: true, width: 40, dataIndex: 'cashExposure', type: 'boolean'},


		// Security Info and Price Info
		{header: 'SecurityID', hidden: true, width: 10, dataIndex: 'security.id'},
		{
			header: 'Security', width: 70, dataIndex: 'security.symbol',
			allViews: true,
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data.cashExposure)) {
					return 'Cash';
				}
				return v;
			}
		},
		{header: 'Security End Date', hidden: true, width: 70, dataIndex: 'security.endDate'},
		{header: 'Security Description', hidden: true, width: 135, dataIndex: 'security.description'},
		{header: 'Security Price (Original)', hidden: true, width: 50, dataIndex: 'securityPrice', type: 'float'},
		{header: 'Security Price', hidden: true, width: 50, dataIndex: 'tradeSecurityPrice', type: 'float'},
		{header: 'Security Price Date', hidden: true, width: 50, dataIndex: 'tradeSecurityPriceDate'},
		{header: 'Price Multiplier', hidden: true, width: 50, type: 'currency', dataIndex: 'security.priceMultiplier', numberFormat: '0,000'},

		// M2M Adjustment - Change in Security Price
		{
			header: 'M2M Adj', hidden: true, width: 75, dataIndex: 'markToMarketAdjustmentValue', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			tooltip: 'Change in Security Price * Price Multiplier * # Actual Contracts.'
		},

		// Underlying Price Info
		{header: 'Underlying Security Price (Original)', hidden: true, width: 50, dataIndex: 'underlyingSecurityPrice', type: 'float'},
		{header: 'Underlying Security Price', hidden: true, width: 50, dataIndex: 'tradeUnderlyingSecurityPrice', type: 'float'},
		{header: 'Underlying Security Price Date', hidden: true, width: 50, dataIndex: 'tradeUnderlyingSecurityPriceDate'},

		// Dirty Price Info
		{header: 'Security Dirty Price (Original)', hidden: true, width: 50, dataIndex: 'securityDirtyPrice', type: 'float'},
		{
			header: 'Security Dirty Price', hidden: true, width: 50, dataIndex: 'tradeSecurityDirtyPrice', type: 'float',
			tooltip: 'Used for Bond Replications.  <br>Dirty Price = Index Ratio * Clean Price * (1 + Accrued Interest / Notional). NOTE: Receivables are NOT included in the Dirty Price and thus not included in the Overlay Exposure.'
		},
		{header: 'Security Dirty Price Date', hidden: true, width: 50, dataIndex: 'tradeSecurityDirtyPriceDate'},

		// Contract Value Price Info
		{header: 'Price Field Name', width: 20, hidden: true, dataIndex: 'contractValuePriceField'},

		// Fair Value Adjustment
		{header: 'Apply Fair Value Adjustment', hidden: true, width: 40, dataIndex: 'security.instrument.fairValueAdjustmentUsed', type: 'boolean'},
		{header: 'Previous Fair Value Adjustment', width: 20, hidden: true, dataIndex: 'previousFairValueAdjustment', type: 'float', useNull: true},
		{header: 'Fair Value Adjustment (Original)', width: 20, hidden: true, dataIndex: 'fairValueAdjustment', type: 'float', useNull: true},
		{header: 'Fair Value Adjustment', width: 20, hidden: true, dataIndex: 'tradeFairValueAdjustment', type: 'float', useNull: true},
		{header: 'Fair Value Adjustment Date', hidden: true, width: 50, dataIndex: 'tradeFairValueAdjustmentDate'},


		{
			header: 'Price(s)', width: 50, hidden: true, type: 'float', dataIndex: 'contractValuePrice',
			viewNames: ['Structured Options'],
			tooltip: 'Latest Price info that is relevant to the contract value. i.e. If the contract value is calculated using the security price, underlying security price, security dirty price or strike price',
			renderer: function(v, metaData, r) {
				let qtip = '';
				let val;
				const fld = r.data.contractValuePriceField;
				let showAdjusted = false;

				if (TCG.isNotBlank(v)) {
					val = v;
					if (fld === 'strikePrice') {
						qtip = 'Strike Price';
					}
					else {
						qtip = fld;
					}
				}
				else if (fld === 'underlyingSecurityPrice') {
					// Underlying Security Price
					val = r.data.underlyingSecurityPrice;
					if (r.data.tradeUnderlyingSecurityPrice) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Underlying Price', r.data.underlyingSecurityPrice, r.data.tradeUnderlyingSecurityPrice, true, r.data.tradeUnderlyingSecurityPriceDate);
						val = r.data.tradeUnderlyingSecurityPrice;
						showAdjusted = !r.data.tradeUnderlyingSecurityPriceDate;
					}
					else {
						qtip = 'Underlying Price';
					}
				}
				else if (fld === 'securityDirtyPrice') {
					// Security Dirty Price
					val = r.data.securityDirtyPrice;
					if (r.data.tradeSecurityDirtyPrice) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Dirty Price', r.data.securityDirtyPrice, r.data.tradeSecurityDirtyPrice, false, r.data.tradeSecurityDirtyPriceDate);
						val = r.data.tradeSecurityDirtyPrice;
					}
					else {
						qtip = 'Dirty Price';
					}
				}
				else {
					// Security Price
					val = r.data.securityPrice;
					if (r.data.tradeSecurityPrice) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Security Price', r.data.securityPrice, r.data.tradeSecurityPrice, true, r.data.tradeSecurityPriceDate);
						val = r.data.tradeSecurityPrice;
						showAdjusted = !r.data.tradeSecurityPriceDate;
					}
					else {
						qtip = 'Security Price';
					}
				}
				if (TCG.isNotBlank(qtip)) {
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				if (showAdjusted) {
					metaData.css = 'amountAdjusted';
				}
				return val;
			}
		},

		// Exchange Rate Info
		{header: 'Exchange Rate (Original)', hidden: true, width: 50, dataIndex: 'exchangeRate', type: 'currency'},
		{header: 'Exchange Rate', hidden: true, width: 50, dataIndex: 'tradeExchangeRate', type: 'currency', useNull: true},
		{header: 'Exchange Rate Date', hidden: true, width: 50, dataIndex: 'tradeExchangeRateDate'},

		// Currency Info
		{header: 'CCY Exchange Rate (Original)', hidden: true, width: 50, dataIndex: 'currencyExchangeRate', type: 'currency', numberFormat: '0,000.0000'},
		{header: 'CCY Exchange Rate', hidden: true, width: 50, dataIndex: 'tradeCurrencyExchangeRate', type: 'currency', numberFormat: '0,000.0000', useNull: true},
		{header: 'CCY Exchange Rate Date', hidden: true, width: 50, dataIndex: 'tradeCurrencyExchangeRateDate'},

		{header: 'CCY Local (Original)', hidden: true, width: 50, dataIndex: 'currencyActualLocalAmount', numberFormat: '0,000'},
		{header: 'CCY Local', hidden: true, width: 50, dataIndex: 'currencyCurrentLocalAmount', numberFormat: '0,000'},
		{header: 'CCY Local (Pending)', hidden: true, width: 50, dataIndex: 'currencyPendingLocalAmount', numberFormat: '0,000'},

		{header: 'CCY Unrealized (Original)', hidden: true, width: 50, dataIndex: 'currencyUnrealizedLocalAmount', numberFormat: '0,000'},
		{header: 'CCY Unrealized', hidden: true, width: 50, dataIndex: 'currencyCurrentUnrealizedLocalAmount', numberFormat: '0,000'},

		{header: 'CCY Other (Original)', hidden: true, width: 50, dataIndex: 'currencyOtherBaseAmount', numberFormat: '0,000'},
		{header: 'CCY Other', hidden: true, width: 50, dataIndex: 'currencyCurrentOtherBaseAmount', numberFormat: '0,000'},
		{header: 'CCY Other (Pending)', hidden: true, width: 50, dataIndex: 'currencyPendingOtherBaseAmount', numberFormat: '0,000'},

		{header: 'Trading CCY (Original)', hidden: true, width: 50, dataIndex: 'currencyDenominationBaseAmount', type: 'currency', numberFormat: '0,000'},
		{header: 'Trading CCY', hidden: true, width: 50, dataIndex: 'currencyCurrentDenominationBaseAmount', type: 'currency', numberFormat: '0,000'},
		{header: 'Trading CCY (Pending)', hidden: true, width: 50, dataIndex: 'currencyPendingDenominationBaseAmount', type: 'currency', numberFormat: '0,000'},


		{
			header: 'CCY Total', hidden: true, width: 75, type: 'currency', numberFormat: '0,000', dataIndex: 'currencyCurrentTotalBaseAmount', negativeInRed: true,
			tooltip: '((Local Amount + Unrealized Local Amount) * Exchange Rate) + Currency Other Base Amount<br>Note: Excludes Trading CCY amount',
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.data['replicationType.currency'])) {
					let qtip = '';

					const actual = Ext.util.Format.number(r.data.currencyActualLocalAmount, '0,000');
					const current = Ext.util.Format.number(r.data.currencyCurrentLocalAmount, '0,000');

					const unActual = Ext.util.Format.number(r.data.currencyUnrealizedLocalAmount, '0,000');
					const unCurrent = Ext.util.Format.number(r.data.currencyCurrentUnrealizedLocalAmount, '0,000');

					const originalEX = Ext.util.Format.number(r.data.currencyExchangeRate, '0,000.0000');
					const currentEX = Ext.util.Format.number(r.data.tradeCurrencyExchangeRate, '0,000.0000');


					if (actual !== current) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Local', actual, current, false);
					}

					if (unActual !== unCurrent) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Unrealized Exposure (Local)', unActual, unCurrent, false);
					}

					if (currentEX !== originalEX) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Exchange Rate', originalEX, currentEX, false, r.data.tradeCurrencyExchangeRateDate);
					}

					const actualOther = Ext.util.Format.number(r.data.currencyOtherBaseAmount, '0,000');
					const currentOther = Ext.util.Format.number(r.data.currencyCurrentOtherBaseAmount, '0,000');

					if (actualOther !== currentOther) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Other (Base)', actualOther, currentOther, false);
					}

					qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td><hr/></td></tr>';
					qtip += '<tr><td>Actual Base:</td><td align="right">' + Ext.util.Format.number((r.data.currencyCurrentLocalAmount * r.data.tradeCurrencyExchangeRate), '0,000') + '</td></tr>';
					qtip += '<tr><td>Unrealized Base:</td><td align="right">' + Ext.util.Format.number((r.data.currencyCurrentUnrealizedLocalAmount * r.data.tradeCurrencyExchangeRate), '0,000') + '</td></tr>';
					qtip += '<tr><td>Other Base:</td><td align="right">' + currentOther + '</td></tr>';
					qtip += '<tr><td>Total:</td><td align="right">' + TCG.renderAmount(v, false, '0,000') + '</td></tr>';

					if (TCG.isNotBlank(qtip)) {
						qtip += '</table>';
					}
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'' + qtip + '\'';

					return TCG.renderAmount(v, false, '0,000');
				}
				return '';
			}
		},

		// Rebalance Triggers (Used for Outside of Rebalance Band Warnings)
		{header: 'AssetClass Rebalance Min', hidden: true, width: 50, dataIndex: 'overlayAssetClass.rebalanceTriggerMin', type: 'currency', numberFormat: '0,000', useNull: true},
		{header: 'AssetClass Rebalance Max', hidden: true, width: 50, dataIndex: 'overlayAssetClass.rebalanceTriggerMax', type: 'currency', numberFormat: '0,000', useNull: true},
		{header: 'AssetClass Rebalance Absolute Min', hidden: true, width: 50, dataIndex: 'overlayAssetClass.rebalanceTriggerAbsoluteMin', type: 'currency', numberFormat: '0,000', useNull: true},
		{header: 'AssetClass Rebalance Absolute Max', hidden: true, width: 50, dataIndex: 'overlayAssetClass.rebalanceTriggerAbsoluteMax', type: 'currency', numberFormat: '0,000', useNull: true},
		{header: 'Asset Class Rebalance Exposure Target', hidden: true, width: 50, dataIndex: 'overlayAssetClass.rebalanceExposureTarget', type: 'currency', numberFormat: '0,000', useNull: true},
		{header: 'Asset Class Actual Allocation Adjusted', hidden: true, width: 50, dataIndex: 'overlayAssetClass.actualAllocationAdjusted', type: 'currency', numberFormat: '0,000', useNull: true},
		{
			header: 'Rebalance Trigger Warnings Used', hidden: true, width: 50, dataIndex: 'overlayAssetClass.rebalanceTriggerWarningsUsed', type: 'boolean',
			tooltip: 'Applies when Rebalance Triggers are used the the Rebalance Action needs some action, i.e. Contact Client, Rebalance'
		},

		// Rebalance Triggers
		{header: 'Replication Rebalance Min', hidden: true, width: 50, dataIndex: 'rebalanceTriggerMin', type: 'currency', numberFormat: '0,000', useNull: true},
		{header: 'Replication Rebalance Max', hidden: true, width: 50, dataIndex: 'rebalanceTriggerMax', type: 'currency', numberFormat: '0,000', useNull: true},


		// Contract Value - Tooltips include price information
		{header: 'Contract Value (Original)', hidden: true, width: 55, dataIndex: 'value', type: 'currency', numberFormat: '0,000'},
		{
			header: 'Contract Value', width: 55, dataIndex: 'tradeValue', type: 'currency', numberFormat: '0,000', negativeInRed: true, css: 'BORDER-RIGHT: #c0c0c0 1px solid;',
			viewNames: ['Overlay', 'Fully Funded'],
			tooltip: 'Contract Value calculation is determined by the calculator selected for the Replication Type.',
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.data.cashExposure)) {
					return null;
				}
				const format = (v < 10 && v > -10) ? '0,000.00' : '0,000'; // bonds
				let qtip = '';
				let value = Ext.util.Format.number(v, format);
				if (r.data.tradeValue !== r.data.value) {
					qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Security Price', r.data.securityPrice, r.data.tradeSecurityPrice, true, r.data.tradeSecurityPriceDate);
					qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Underlying Price', r.data.underlyingSecurityPrice, r.data.tradeUnderlyingSecurityPrice, true, r.data.tradeUnderlyingSecurityPriceDate);
					qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Dirty Price', r.data.securityDirtyPrice, r.data.tradeSecurityDirtyPrice, false, r.data.tradeSecurityDirtyPriceDate);
					qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Exchange Rate', r.data.exchangeRate, r.data.tradeExchangeRate, false, r.data.tradeExchangeRateDate);
					metaData.css = 'amountAdjusted';
					if (TCG.isNotBlank(qtip)) {
						qtip += '</table>';
					}
					metaData.attr = 'qtip=\'' + qtip + '\'';
					value = TCG.renderAmountArrow(r.data.tradeValue > r.data.value) + value;
				}
				return value;
			}
		},

		// Replication Allocation % of Asset Class
		{header: 'Allocation %', allViews: true, width: 50, type: 'currency', dataIndex: 'allocationWeightAdjusted', summaryType: 'sum', hideGrandTotal: true},

		// Delta
		{
			header: 'Delta', hidden: true, width: 50, type: 'currency', dataIndex: 'delta', numberFormat: '0,000.0000', useNull: true, css: 'BORDER-RIGHT: #c0c0c0 1px solid;',
			tooltip: 'Market Data Value for <i>Delta</i> field. <br><br><b>Special Adjustment for Options:</b>&nbsp;Delta is Negated If Call Option and Short Target or Put Option and Long Target'
		},

		// Duration
		{
			header: 'Duration', width: 50, type: 'currency', dataIndex: 'duration', numberFormat: '0,000.0000', useNull: true, css: 'BORDER-RIGHT: #c0c0c0 1px solid;',
			viewNames: ['Overlay']
		},
		{header: 'Factor', width: 80, dataIndex: 'factor', type: 'currency', numberFormat: '0,000.0000', useNull: true, css: 'BORDER-RIGHT: #c0c0c0 1px solid;', tooltip: 'Current Factor Change event value for the security. If there are no events available the value will be 1.'},

		{
			header: 'Final Effective Duration', hidden: true, width: 50, type: 'currency', numberFormat: '0,000.0000', summaryType: 'average', hideGrandTotal: true,
			tooltip: 'Fixed Income Replications Only: (Actual + Pending) Contracts * Duration * Price * Multiplier / Overlay Target (Including M2M Adjustments)',
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.data['replicationType.durationSupported'])) {
					let price = r.data.securityPrice;
					if (r.data.tradeSecurityPrice) {
						price = r.data.tradeSecurityPrice;
					}
					const totalContracts = r.data.currentContractsAdjusted + r.data.pendingContractsAdjusted + r.data.buyContracts - r.data.sellContracts;
					let value = totalContracts * r.data.duration * price * r.data['security.priceMultiplier'];
					r.data['Final Effective Duration Numerator'] = value;
					r.data['Final Effective Duration Denominator'] = r.data.targetExposureAdjustedWithMarkToMarket;
					if (value === 0 || r.data.targetExposureAdjustedWithMarkToMarket === 0) {
						value = 0;
					}
					else {
						value = value / r.data.targetExposureAdjustedWithMarkToMarket;
					}
					r.data['Final Effective Duration'] = value;

					return TCG.renderAmount(value, false, '0,000.0000');
				}
			},
			summaryTotalCondition: function(data) {
				return TCG.isTrue(data.replicationType.durationSupported);
			},
			summaryCalculation: function(v, r, field, data, col) {
				if (TCG.isTrue(r.data['replicationType.durationSupported'])) {
					if (data[field + 'count']) {
						++data[field + 'count'];
					}
					else {
						data[field + 'count'] = 1;
					}
					let t = (data[field + ' Numerator-total'] = ((data[field + ' Numerator-total'] || 0) + (r.data[field + ' Numerator'] || 0)));
					const b = (data[field + ' Denominator-total'] = ((data[field + ' Denominator-total'] || 0) + (r.data[field + ' Denominator'] || 0)));
					if (t === 0 || b === 0) {
						t = 0;
					}
					else {
						t = (t / b);
					}
					const val = t / r.data['overlayAssetClass.benchmarkDuration'];
					const obj = Ext.fly(r['_groupId'] + '-fe');
					if (TCG.isNotNull(obj)) {
						obj.update('Final Effective Duration vs. Benchmark: ' + Ext.util.Format.number(val, '0,000.0000'));
					}
					return t;
				}
				return '';
			},
			summaryRenderer: function(v) {
				if (TCG.isNotBlank(v)) {
					return TCG.renderAmount(v, false, '0,000.0000');
				}
			}
		},


		// Targets
		{
			header: 'Overlay Target (Original)', hidden: true, width: 75, dataIndex: 'targetExposureAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			viewNameHeaders: [
				{name: 'Overlay', label: 'Overlay Target (Original)'},
				{name: 'Fully Funded', label: 'Target (Original)'},
				{name: 'Structured Options', label: 'Target (Original)'}
			],
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			}
		},
		// Original Target + M2M Adjustment (from update with Live Prices)
		{
			header: 'Overlay Target (Live Prices)', hidden: true, width: 75, dataIndex: 'targetExposureAdjustedWithMarkToMarket', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			viewNameHeaders: [
				{name: 'Overlay', label: 'Overlay Target (Live Prices)'},
				{name: 'Fully Funded', label: 'Target (Live Prices)'},
				{name: 'Structured Options', label: 'Target (Live Prices)'}
			],
			renderer: function(v, p, r) {
				return TCG.renderAdjustedAmount(v, (r.data.targetExposureAdjusted !== r.data.targetExposureAdjustedWithMarkToMarket), r.data.targetExposureAdjusted, '0,000', 'M2M Adjustment');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			}
		},
		// Original Target + M2M Adjustment (from update with Live Prices) + Screen Target Updates (TFA, Matching update from Non Matching, etc.)
		{
			header: 'Overlay Target', dataIndex: 'Overlay Target', width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			allViews: true,
			viewNameHeaders: [
				{name: 'Overlay', label: 'Overlay Target'},
				{name: 'Fully Funded', label: 'Target'},
				{name: 'Structured Options', label: 'Target'}
			],
			tooltip: 'Original Target plus any M2M Adjustments based on price or exchange rate changes.<br><br>Note: May also update based on Asset Class assignment to a Target Exposure Adjustment Used asset class (to offset its target change).<br><br>Matching Replication targets will update live based on trade entries in their non-matching counterparts.',
			renderer: function(v, p, r) {
				const original = r.data.targetExposureAdjusted;
				const current = r.data.targetExposureAdjustedWithMarkToMarket;
				const m2mAdj = r.data.targetExposureAdjustedWithMarkToMarket - r.data.targetExposureAdjusted;
				const tradingCcyAdj = r.data.currencyDenominationBaseAmount;

				let showAdjusted = false;
				let qtip = '<table><tr><td>Original Target:</td><td>&nbsp;</td><td align="right">' + TCG.renderAmount(original, false, '0,000') + '</td></tr>';

				if (m2mAdj !== 0) {
					showAdjusted = true;
					qtip += '<tr><td>M2M Adjustment:</td><td>&nbsp;</td><td align="right">' + TCG.renderAmount(m2mAdj, true, '0,000') + '</td></tr>';
				}

				if (tradingCcyAdj !== 0) {
					showAdjusted = true;
					qtip += '<tr><td>Trading CCY:</td><td>&nbsp;</td><td align="right">' + TCG.renderAmount(tradingCcyAdj, true, '0,000') + '</td></tr>';
				}

				let adjustments = 0;
				const ta = r.data.targetAdjustments;
				if (ta) {
					for (let i = 0; i < ta.length; i++) {
						const adj = ta[i].adjustment;
						if (adj !== 0) {
							adjustments = adjustments + adj;
							showAdjusted = true;
							qtip += '<tr><td>' + ta[i].label + '</td><td align="right">' + TCG.renderAmount(ta[i].adjustment) + '</td></tr>';
						}
					}
					if (adjustments !== 0) {
						qtip += '<tr><td>&nbsp;<td>&nbsp;</td></td><td align="right">' + TCG.renderAmount(adjustments, true, '0,000') + '</td></tr>';
					}
				}
				let finalTarget = current + adjustments;
				if (tradingCcyAdj !== 0) {
					finalTarget = finalTarget + tradingCcyAdj;
				}
				if (TCG.isTrue(showAdjusted)) {
					p.css = 'amountAdjusted';

					qtip += '<tr><td>&nbsp;</td><td>&nbsp;</td><td><hr/></td></tr>';
					qtip += '<tr><td><b>Total:</b></td><td>&nbsp;</td><td align="right"><b>' + TCG.renderAmount(finalTarget, true, '0,000') + '</b></td></tr>';
					qtip += '</table>';

					p.attr = 'qtip=\'' + qtip + '\'';
				}
				else if (finalTarget < 0) {
					p.css = 'amountNegative';
				}
				return Ext.util.Format.number(finalTarget, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryCalculation: function(v, r, field, data, col) {
				return v + Clifton.product.overlay.trade.calculateFinalTarget(r, true);
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}

		},

		{
			header: 'Mispricing', dataIndex: 'mispricingTradeValue', hidden: true, width: 70, type: 'currency', summaryType: 'sum', useNull: true,
			mispricingOnly: true,
			viewNames: ['Overlay', 'Fully Funded'],
			tooltip: 'Mispricing Calculation applies when security instrument is flagged as using fair value adjustment.<br/><br/>Calculation: (Future Price - (Index Price + Fair Value Adjustment)) * Future Price Multiplier * FX Rate * # of Contracts',
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data['security.instrument.fairValueAdjustmentUsed'])) {
					const priceMultiplier = r.data['security.priceMultiplier'];
					const contracts = r.data.actualContractsAdjusted;
					// Back out price multiplier and # of contracts to get value per contract
					const misPerContract = (contracts !== 0 ? ((v / priceMultiplier) / contracts) : 0);

					let qtip = '<table>';

					const fva = r.data.fairValueAdjustment;
					const tradeFVA = r.data.tradeFairValueAdjustment;
					let change;

					if (tradeFVA) {
						qtip += '<tr><td>Current Fair Value Adjustment:</td><td align="right">' + TCG.numberFormat(tradeFVA, '0,000.0000', true) + '</td><td>&nbsp;on ' + TCG.renderDate(r.data['tradeFairValueAdjustmentDate']) + '</td></tr>';
						qtip += '<tr><td>Previous Close:</td><td align="right">' + TCG.numberFormat(fva, '0,000.0000', true) + '</td></tr>';
						change = TCG.calculatePercentChange(fva, tradeFVA);
						qtip += '<tr><td>Change (%):</td><td align="right">' + TCG.renderAmount(change, true, '0,000.00 %') + '</td></tr>';
					}
					else {
						const previousFVA = r.data.previousFairValueAdjustment;
						change = TCG.calculatePercentChange(previousFVA, fva);

						qtip += '<tr><td>Fair Value Adjustment:</td><td align="right">' + TCG.numberFormat(fva, '0,000.0000', true) + '</td></tr>';
						qtip += '<tr><td>Previous Value (Balance Date - 1):</td><td align="right">' + TCG.numberFormat(previousFVA, '0,000.0000', true) + '</td></tr>';
						qtip += '<tr><td>Change (%):</td><td align="right">' + TCG.renderAmount(change, true, '0,000.00 %') + '</td></tr>';
					}

					if (change && (change > 10 || change < -10)) {
						p.css = 'ruleViolation';
						qtip += '<tr><td colspan="2"><div class="amountNegative"><b>Warning: Change Exceeds 10%</b></div></td></tr>';
					}
					qtip += '<tr><td colspan="2"><hr/></td></tr>';

					if (r.data.tradeUnderlyingSecurityPrice) {
						qtip += '<tr><td>Underlying Price:</td><td align="right">' + TCG.numberFormat(r.data.tradeUnderlyingSecurityPrice, '0,000.0000', true) + '</td><td>&nbsp;on ' + TCG.renderDate(r.data['tradeUnderlyingSecurityPriceDate']) + '</td></tr>';
					}
					else {
						qtip += '<tr><td>Underlying Price:</td><td align="right">' + TCG.renderAmount(r.data.underlyingSecurityPrice, true, '0,000.0000', true) + '</td></tr>';
					}
					if (r.data.tradeSecurityPrice) {
						qtip += '<tr><td>Security Price:</td><td align="right">' + TCG.numberFormat(r.data.tradeSecurityPrice, '0,000.0000', true) + '</td><td>&nbsp;on ' + TCG.renderDate(r.data['tradeSecurityPriceDate']) + '</td></tr>';
					}
					else {
						qtip += '<tr><td>Security Price:</td><td align="right">' + TCG.renderAmount(r.data.securityPrice, true, '0,000.0000', true) + '</td></tr>';
					}

					qtip += '<tr><td>Fair Value of Contract:</td><td align="right">' + (tradeFVA ? TCG.renderAmount(tradeFVA, true, '0,000.0000', true) : TCG.renderAmount(fva, true, '0,000.0000', true)) + '</td></tr>';

					if (r.data.tradeExchangeRate) {
						qtip += '<tr><td>FX Rate:</td><td align="right">' + TCG.numberFormat(r.data.tradeExchangeRate, '0,000.0000', true) + '</td><td>&nbsp;on ' + TCG.renderDate(r.data['tradeExchangeRateDate']) + '</td></tr>';
					}
					else {
						qtip += '<tr><td>FX Rate:</td><td align="right">' + TCG.renderAmount(r.data.exchangeRate, true, '0,000.0000', true) + '</td></tr>';
					}

					qtip += '<tr><td>&nbsp;</td><td><hr/></td></tr>';
					qtip += '<tr><td>Mispricing Per Contract:</td><td align="right">' + TCG.renderAmount(misPerContract, true, '0,000.00') + '</td></tr>';
					qtip += '<tr><td>&nbsp;</td><td><hr/></td></tr>';
					qtip += '<tr><td>Contract Multiplier:</td><td align="right">' + TCG.renderAmount(priceMultiplier, false, '0,000') + '</td></tr>';
					qtip += '<tr><td>Contracts:</td><td align="right">' + TCG.renderAmount(contracts, true, '0,000') + '</td></tr>';
					qtip += '<tr><td>&nbsp;</td><td><hr/></td></tr>';
					qtip += '<tr><td><b>Position Mispricing:</b></td><td align="right"><b>' + TCG.renderAmount(v, true, '0,000.00') + '</b></td></tr>';

					qtip += '</table>';
					p.attr = 'qtip=\'' + qtip + '\'';
				}
				return TCG.renderAmount(v, false, '0,000');
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},

		// Original Target + M2M Adjustment (from update with Live Prices) + Screen Target Updates (TFA, Matching update from Non Matching, etc.)
		{
			header: 'Overlay Target (Mispricing)', dataIndex: 'Overlay Target (Mispricing)', hidden: true, width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			mispricingOnly: true,
			viewNames: ['Overlay', 'Fully Funded'],
			viewNameHeaders: [
				{name: 'Overlay', label: 'Overlay Target (Mispricing)'},
				{name: 'Fully Funded', label: 'Target (Mispricing)'},
				{name: 'Structured Options', label: 'Target (Mispricing)'}
			],
			tooltip: 'Original Target plus any M2M Adjustments based on price or exchange rate changes less mispricing.<br><br>Note: May also update based on Asset Class assignment to a Target Exposure Adjustment Used asset class (to offset its target change).<br><br>Matching Replication targets will update live based on trade entries in their non-matching counterparts.',
			renderer: function(v, p, r) {
				const original = r.data.targetExposureAdjusted;
				let current = r.data.targetExposureAdjustedWithMarkToMarket;
				const m2mAdj = r.data.targetExposureAdjustedWithMarkToMarket - r.data.targetExposureAdjusted;
				const mispricing = r.data.mispricingTradeValue;
				const tradingCcyAdj = r.data.currencyDenominationBaseAmount;

				let showAdjusted = false;
				let qtip = '<table><tr><td>Original Target:</td><td>&nbsp;</td><td align="right">' + TCG.renderAmount(original, false, '0,000') + '</td></tr>';

				if (m2mAdj !== 0) {
					showAdjusted = true;
					qtip += '<tr><td>M2M Adjustment:</td><td>&nbsp;</td><td align="right">' + TCG.renderAmount(m2mAdj, true, '0,000') + '</td></tr>';
				}

				if (tradingCcyAdj !== 0) {
					showAdjusted = true;
					qtip += '<tr><td>Trading CCY:</td><td>&nbsp;</td><td align="right">' + TCG.renderAmount(tradingCcyAdj, true, '0,000') + '</td></tr>';
				}

				if (mispricing && mispricing !== 0) {
					current = current - mispricing;
					showAdjusted = true;
					qtip += '<tr><td>Mispricing:</td><td>&nbsp;</td><td align="right">' + TCG.renderAmount(mispricing, true, '0,000') + '</td></tr>';
				}

				let adjustments = 0;
				const ta = r.data.targetAdjustments;
				if (ta) {
					for (let i = 0; i < ta.length; i++) {
						const adj = ta[i].adjustment;
						if (adj !== 0) {
							adjustments = adjustments + adj;
							showAdjusted = true;
							qtip += '<tr><td>' + ta[i].label + '</td><td align="right">' + TCG.renderAmount(ta[i].adjustment) + '</td></tr>';
						}
					}
					if (adjustments !== 0) {
						qtip += '<tr><td>&nbsp;<td>&nbsp;</td></td><td align="right">' + TCG.renderAmount(adjustments, true, '0,000') + '</td></tr>';
					}
				}
				let finalTarget = current + adjustments;
				if (tradingCcyAdj !== 0) {
					finalTarget = finalTarget + tradingCcyAdj;
				}
				if (TCG.isTrue(showAdjusted)) {
					p.css = 'amountAdjusted';

					qtip += '<tr><td>&nbsp;</td><td>&nbsp;</td><td><hr/></td></tr>';
					qtip += '<tr><td><b>Total:</b></td><td>&nbsp;</td><td align="right"><b>' + TCG.renderAmount(finalTarget, true, '0,000') + '</b></td></tr>';
					qtip += '</table>';

					p.attr = 'qtip=\'' + qtip + '\'';
				}
				else if (finalTarget < 0) {
					p.css = 'amountNegative';
				}
				return Ext.util.Format.number(finalTarget, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryCalculation: function(v, r, field, data, col) {
				return v + Clifton.product.overlay.trade.calculateFinalTarget(r);
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}

		},

		// Exposure
		{header: 'Additional Exposure', dataIndex: 'additionalExposure', hidden: true, width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
		{header: 'Additional Overlay Exposure', dataIndex: 'additionalOverlayExposure', hidden: true, width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
		{header: 'Total Additional', dataIndex: 'totalAdditionalExposure', hidden: true, width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},

		{
			header: 'Overlay Exposure', dataIndex: 'Overlay Exposure', width: 75, type: 'currency', numberFormat: '0,000', useNull: true, summaryType: 'sum', negativeInRed: true,
			allViews: true,
			viewNameHeaders: [
				{name: 'Overlay', label: 'Overlay Exposure'},
				{name: 'Fully Funded', label: 'Exposure'},
				{name: 'Structured Options', label: 'Exposure'}
			],
			tooltip: '(Actual Contracts * Contract Value) + Additional Exposure + Currency Total<br /><br />Currency is added for Currency replication only.<br />Reverse Exposure option from the replication will reverse all exposure calculated values.',
			renderer: function(v, metaData, r) {
				const format = '0,000';

				const contracts = r.data.currentContractsAdjusted;
				const actual = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data.value);
				let current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data.tradeValue);

				let qtip = '';
				if (actual !== current) {
					qtip += '<table><tr><td>Previous Close:</td><td align="right">' + Ext.util.Format.number(actual, '0,000') + '</td></tr><tr><td>Change:</td><td align="right">' + TCG.renderAmount(current - actual, true) + '</td></tr>';
				}
				if (TCG.isTrue(r.data['replicationType.currency']) && r.data.currencyCurrentTotalBaseAmount) {
					qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td>&nbsp;</td></tr>';
					qtip += '<tr><td>Currency Futures Exposure:</td><td align="right">' + Ext.util.Format.number(current, '0,000') + '</td></tr>';
					qtip += '<tr><td>Physical Currency Exposure:</td><td align="right">' + Ext.util.Format.number((r.data.currencyCurrentLocalAmount * r.data.tradeCurrencyExchangeRate), '0,000') + '</td></tr>';
					qtip += '<tr><td>Physical Other Currency Exposure:</td><td align="right">' + Ext.util.Format.number(r.data.currencyCurrentOtherBaseAmount, '0,000') + '</td></tr>';
					qtip += '<tr><td>Unrealized Currency Exposure:</td><td align="right">' + Ext.util.Format.number((r.data.currencyCurrentUnrealizedLocalAmount * r.data.tradeCurrencyExchangeRate), '0,000') + '</td></tr>';
					qtip += '<tr><td>Total Physical Currency Exposure:</td><td align="right">' + TCG.renderAmount(r.data.currencyCurrentTotalBaseAmount, false, '0,000') + '</td></tr>';
					current = current + r.data.currencyCurrentTotalBaseAmount;
				}
				if (r.data['additionalExposure'] !== 0) {
					qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td>&nbsp;</td></tr>';
					qtip += '<tr><td>Additional Exposure:</td><td align="right">' + Ext.util.Format.number(r.data['additionalExposure'], '0,000') + '</td></tr>';
					current = current + r.data['additionalExposure'];
				}
				if (r.data['additionalOverlayExposure'] !== 0) {
					qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td>&nbsp;</td></tr>';
					qtip += '<tr><td>Additional Overlay Exposure:</td><td align="right">' + Ext.util.Format.number(r.data['additionalOverlayExposure'], '0,000') + '</td></tr>';
					current = current + r.data['additionalOverlayExposure'];
				}

				if (TCG.isNotBlank(qtip)) {
					qtip += '</table>';
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return (current === 0) ? '' : TCG.renderAmount(current, false, format);
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryCalculation: function(v, r, field, data, col) {
				const contracts = r.data.currentContractsAdjusted;
				const current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data.tradeValue, r.data.currencyCurrentTotalBaseAmount, r.data.totalAdditionalExposure);
				return v + current;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},


		{
			header: 'Pending Exposure', hidden: true, width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			tooltip: '(Pending Contracts * Contract Value) + Pending Currency Exposure',
			renderer: function(v, metaData, r) {
				const format = '0,000';
				const pending = Clifton.portfolio.run.trade.calculateExposure(r, r.data.pendingContractsAdjusted, r.data.tradeValue, r.data.currencyPendingTotalBaseAmount, r.data.totalAdditionalExposure);

				let qtip = '';
				if (TCG.isTrue(r.data['replicationType.currency']) && r.data.currencyPendingTotalBaseAmount) {
					qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td>&nbsp;</td></tr>';
					qtip += '<tr><td>Currency Futures Pending Exposure:</td><td align="right">' + Ext.util.Format.number(pending, '0,000') + '</td></tr>';
					qtip += '<tr><td>Physical Currency Pending Exposure:</td><td align="right">' + Ext.util.Format.number((r.data.currencyPendingLocalAmount * r.data.tradeCurrencyExchangeRate), '0,000') + '</td></tr>';
					qtip += '<tr><td>Physical Other Currency Pending Exposure:</td><td align="right">' + Ext.util.Format.number(r.data.currencyPendingOtherBaseAmount, '0,000') + '</td></tr>';
					qtip += '<tr><td>Total Physical Currency Pending Exposure:</td><td align="right">' + TCG.renderAmount(r.data.currencyPendingTotalBaseAmount, false, '0,000') + '</td></tr>';
				}
				if (TCG.isNotBlank(qtip)) {
					qtip += '</table>';
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return TCG.renderAmount(pending, false, format);

			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryCalculation: function(v, r, field, data, col) {
				const pending = Clifton.portfolio.run.trade.calculateExposure(r, r.data.pendingContractsAdjusted, r.data.tradeValue);
				return v + pending;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},

		{
			header: 'Overlay Exposure Difference', dataIndex: 'Overlay Exposure Difference', hidden: true, width: 85, type: 'currency', css: 'BORDER-RIGHT: #c0c0c0 1px solid;',
			mispricingOnly: false,
			viewNames: ['Overlay', 'Fully Funded', 'Structured Options'],
			viewNameHeaders: [
				{name: 'Overlay', label: 'Overlay Exposure Difference'},
				{name: 'Fully Funded', label: 'Exposure Difference'},
				{name: 'Structured Options', label: 'Exposure Difference'}
			],
			tooltip: 'Overlay Target - Overlay Exposure<br /><br />Does NOT include Pending Exposure',
			renderer: function(v, p, r) {
				const target = Clifton.product.overlay.trade.calculateFinalTarget(r);
				const contracts = r.data.currentContractsAdjusted;
				const current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data.tradeValue, r.data.currencyCurrentTotalBaseAmount, r.data.totalAdditionalExposure);
				const dif = target - current;
				return TCG.renderAmount(dif, false, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryCalculation: function(v, r, field, data, col) {
				const target = Clifton.product.overlay.trade.calculateFinalTarget(r);
				const contracts = r.data.currentContractsAdjusted;
				const current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data.tradeValue, r.data.currencyCurrentTotalBaseAmount, r.data.totalAdditionalExposure);

				const d = target - current;
				return v + d;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},


		{
			header: 'Mispricing Difference', dataIndex: 'Mispricing Difference', hidden: true, width: 70, type: 'currency', css: 'BORDER-RIGHT: #c0c0c0 1px solid;',
			mispricingOnly: true,
			viewNames: ['Overlay', 'Fully Funded'],
			tooltip: 'Overlay Target - Overlay Exposure - Mispricing <br /><br />Does NOT include Pending Exposure',
			renderer: function(v, p, r) {
				// Note: Calculate Final Target already subtracts mispricing value
				const target = Clifton.product.overlay.trade.calculateFinalTarget(r);
				const contracts = r.data.currentContractsAdjusted;
				const current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data.tradeValue, r.data.currencyCurrentTotalBaseAmount, r.data.totalAdditionalExposure);
				const dif = target - current;
				return TCG.renderAmount(dif, false, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryCalculation: function(v, r, field, data, col) {
				const target = Clifton.product.overlay.trade.calculateFinalTarget(r);
				const contracts = r.data.currentContractsAdjusted;
				const current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data.tradeValue, r.data.currencyCurrentTotalBaseAmount, r.data.totalAdditionalExposure);
				const d = target - current;
				return v + d;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},

		// Contracts
		{
			header: 'Target Contracts', width: 70, type: 'currency', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			allViews: true,
			tooltip: '(Overlay Target (including M2M Adjustment and Trading Currency (if Currency Replication)) - Currency Total (for Currency Replication only)) / Contract Value',
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data.cashExposure)) {
					return null;
				}
				let target = Clifton.product.overlay.trade.calculateFinalTarget(r);
				if (TCG.isTrue(r.data['replicationType.currency'])) {
					target = target - r.data.currencyCurrentTotalBaseAmount;
				}
				target = target / r.data.tradeValue;
				if (TCG.isTrue(r.data.reverseExposureSign)) {
					target = target * -1;
				}
				return TCG.renderAmount(target, false, '0,000.00');
			},
			summaryCondition: function(data) {
				return TCG.isFalse(data.cashExposure);
			}
		},

		{
			header: 'Actual Contracts (Original)', hidden: true, width: 70, dataIndex: 'actualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data.cashExposure)) {
					return null;
				}
				return TCG.renderAmount(v, false, '0,000');
			},
			summaryCondition: function(data) {
				return TCG.isFalse(data.cashExposure);
			}
		},
		{header: 'Actual Contracts (Actual)', hidden: true, width: 70, dataIndex: 'actualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
		{header: 'Actual Contracts (Virtual)', hidden: true, width: 70, dataIndex: 'virtualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
		{
			header: 'Actual Contracts', hidden: true, width: 80, dataIndex: 'actualContractsAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data.cashExposure)) {
					return null;
				}
				return TCG.renderAdjustedAmount(v, r.data['actualContracts'] !== v, r.data['actualContracts'], '0,000', 'Virtual Contracts');
			},
			summaryCondition: function(data) {
				return TCG.isFalse(data.cashExposure);
			}
		},
		{header: 'Current Contracts (Actual)', hidden: true, width: 70, dataIndex: 'currentContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
		{header: 'Current Contracts (Virtual)', hidden: true, width: 70, dataIndex: 'currentVirtualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
		{
			header: 'Contracts', width: 70, dataIndex: 'currentContractsAdjusted', type: 'int', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			allViews: true,
			tooltip: 'Total Contracts that apply to this replication that have been booked to the General Ledger as of the following business day from the run balance date.<br><b>NOTE:&nbsp;</b>If this is a currently active run for trading the current balance includes everything up to 30 days in the future from today, not as of the balance date.',
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.data.cashExposure)) {
					return null;
				}
				if (r.data.actualContractsAdjusted !== v || r.data.currentContracts !== v) {
					metaData.css = 'amountAdjusted';
					let qtip = '<table>';
					qtip += '<tr><td>&nbsp;</td><td align="right">Actual</td><td>&nbsp;</td><td align="right">Virtual</td><td>&nbsp;</td><td align="right">Total</td></tr>';
					qtip += '<tr><td>Previous Close:</td><td align="right">' + Ext.util.Format.number(r.data.actualContracts, '0,000') + '</td><td>&nbsp;</td><td align="right">' + Ext.util.Format.number(r.data.virtualContracts, '0,000') + '</td><td>&nbsp;</td><td align="right">' + Ext.util.Format.number(r.data.actualContractsAdjusted, '0,000') + '</td></tr>';
					if (r.data.actualContractsAdjusted !== v) {
						qtip += '<tr><td>Current:</td><td align="right">' + Ext.util.Format.number(r.data.currentContracts, '0,000') + '</td><td>&nbsp;</td><td align="right">' + Ext.util.Format.number(r.data.currentVirtualContracts, '0,000') + '</td><td>&nbsp;</td><td align="right">' + Ext.util.Format.number(r.data.currentContractsAdjusted, '0,000') + '</td></tr>';
						qtip += '<tr><td colspan="6"><hr/></td></tr>';
						qtip += '<tr><td>Change:</td><td align="right">' + Ext.util.Format.number(r.data.currentContracts - r.data.actualContracts, '0,000') + '</td><td>&nbsp;</td><td align="right">' + Ext.util.Format.number(r.data.currentVirtualContracts - r.data.virtualContracts, '0,000') + '</td><td>&nbsp;</td><td align="right">' + Ext.util.Format.number(r.data.currentContractsAdjusted - r.data.actualContractsAdjusted, '0,000') + '</td></tr>';
					}
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return TCG.numberFormat(v, '0,000');
			},
			summaryCondition: function(data) {
				return TCG.isFalse(data.cashExposure);
			}
		},
		{header: 'Pending Contracts (Actual)', hidden: true, width: 70, dataIndex: 'pendingContracts', useNull: true, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
		{header: 'Pending Contracts (Virtual)', hidden: true, width: 70, dataIndex: 'pendingVirtualContracts', useNull: true, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
		{
			header: 'Pending Contracts', width: 70, dataIndex: 'pendingContractsAdjusted', type: 'int', summaryType: 'sum', negativeInRed: true, useNull: true, hideGrandTotal: true,
			allViews: true,
			tooltip: 'Pending Trades (Not Closed) + Pending Transfers (Unbooked Transfers To/From this account)',
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data.cashExposure)) {
					return null;
				}
				const grid = this.gridPanel;
				if (grid) {
					grid.applyPendingTradesTooltipToColumn.call(grid, v, p, r);
				}
				return TCG.renderAdjustedAmount(v, r.data['pendingContracts'] !== v, r.data['pendingContracts'], '0,000', 'Virtual Contracts');
			}
		},

		{
			header: 'Difference', width: 65, type: 'currency', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			allViews: true,
			tooltip: 'Target Contracts - Actual Contracts - Pending Contracts<br /><br />Suggested Contracts to Buy/Sell to reach the target.',
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data.cashExposure)) {
					return null;
				}

				let target = Clifton.product.overlay.trade.calculateFinalTarget(r);
				if (TCG.isTrue(r.data['replicationType.currency'])) {
					target = target - r.data.currencyCurrentTotalBaseAmount;
				}
				target = target / r.data.tradeValue;
				if (TCG.isTrue(r.data.reverseExposureSign)) {
					target = target * -1;
				}
				const current = r.data.currentContractsAdjusted;
				const pending = r.data.pendingContractsAdjusted;
				// Set the Value So it can be used as the Suggested Trade Amount for the Trade Quantity Violation
				r.data['Difference'] = (target - current - pending);
				return TCG.renderAmount((target - current - pending), false, '0,000.00');
			},
			summaryCondition: function(data) {
				return TCG.isFalse(data.cashExposure);
			}
		},

		// Trading Fields
		{
			header: 'Buy', width: 50, dataIndex: 'buyContracts', type: 'int', useNull: true, editor: {xtype: 'spinnerfield', allowBlank: true, minValue: 1, maxValue: 1000000000}, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			allViews: true,
			renderer: function(value, metaData, r) {
				const tradingDisabled = r.data.tradeEntryDisabled;

				if (TCG.isNotBlank(value) && value !== 0) {
					if (TCG.isFalse(tradingDisabled)) {
						let target = Clifton.product.overlay.trade.calculateFinalTarget(r);
						if (TCG.isTrue(r.data['replicationType.currency'])) {
							target = target - r.data.currencyCurrentTotalBaseAmount;
						}
						const originalDifference = (target / r.data.tradeValue) - r.data.currentContractsAdjusted - r.data.pendingContractsAdjusted;
						if (originalDifference - value < -5) {
							metaData.css = 'ruleViolation';
							metaData.attr = 'qtip="Buys entered exceeds suggested trade amount by ' + Ext.util.Format.number((originalDifference - value) * -1, '0,000') + ' contracts."';
						}
						else {
							metaData.css = 'buy-dark';
						}
					}
					else {
						metaData.css = 'buy-dark';
					}

				}
				if (TCG.isTrue(r.data.securityNotTradable)) {
					metaData.attr = 'qtip="Security is non-tradable.  Trades cannot be entered."';
				}
				else if (TCG.isTrue(r.json.cashExposure)) {
					metaData.attr = 'qtip="Security is non-tradable.  Security represents cash exposure."';
				}
				else if (TCG.isTrue(tradingDisabled)) {
					metaData.attr = 'qtip="Trading against this asset class is disabled. Trades populated here are from security trades entered in other asset class replications on this screen."';
				}
				return TCG.numberFormat(value, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.tradeEntryDisabled);
			},

			summaryRenderer: function(value) {
				return TCG.numberFormat(value, '0,000');
			}
		},
		{
			header: 'Sell', width: 50, dataIndex: 'sellContracts', type: 'int', useNull: true, editor: {xtype: 'spinnerfield', allowBlank: true, minValue: 1, maxValue: 1000000000}, summaryType: 'sum', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid; BORDER-RIGHT: #c0c0c0 1px solid;',
			allViews: true,
			renderer: function(value, metaData, r) {
				const tradingDisabled = r.data.tradeEntryDisabled;

				if (TCG.isNotBlank(value) && value !== 0) {
					if (TCG.isFalse(tradingDisabled)) {
						let target = Clifton.product.overlay.trade.calculateFinalTarget(r);
						if (TCG.isTrue(r.data['replicationType.currency'])) {
							target = target - r.data.currencyCurrentTotalBaseAmount;
						}
						let originalDifference = (target / r.data.tradeValue) - r.data.currentContractsAdjusted - r.data.pendingContractsAdjusted;

						if (originalDifference > 0) {
							metaData.css = 'ruleViolation';
							metaData.attr = 'qtip="Suggested to buy ' + Ext.util.Format.number(originalDifference, '0,000') + ' trades, however sells were entered."';
						}
						else {
							originalDifference = originalDifference * -1; // Make it positive
							if (originalDifference - value < -5) {
								metaData.css = 'ruleViolation';
								metaData.attr = 'qtip="Sells entered exceeds suggested trade amount by ' + Ext.util.Format.number((originalDifference - value) * -1, '0,000') + ' contracts."';
							}
							else {
								metaData.css = 'sell-dark';
							}
						}
					}
					else {
						metaData.css = 'sell-dark';
					}
				}
				if (TCG.isTrue(r.data.securityNotTradable)) {
					metaData.attr = 'qtip="Security is non-tradable.  Trades cannot be entered."';
				}
				else if (TCG.isTrue(r.json.cashExposure)) {
					metaData.attr = 'qtip="Security is non-tradable.  Security represents cash exposure."';
				}
				else if (TCG.isTrue(tradingDisabled)) {
					metaData.attr = 'qtip="Trading against this asset class is disabled. Trades populated here are from security trades entered in other asset class replications on this screen."';
				}

				return TCG.numberFormat(value, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.tradeEntryDisabled);
			},

			summaryRenderer: function(value) {
				return TCG.numberFormat(value, '0,000');
			}
		},
		{
			header: 'Open/Close', width: 70, dataIndex: 'openCloseType.name', idDataIndex: 'openCloseType.id', type: 'combo', useNull: false,
			viewNames: ['Structured Options'],
			editor: {
				xtype: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeListFind.json',
				beforequery: function(queryEvent) {
					// Clear Query Store so Re-Queries each time
					this.store.removeAll();
					this.lastQuery = null;

					const record = queryEvent.combo.gridEditor.record;
					queryEvent.combo.store.baseParams = {};
					if (TCG.isNotBlank(record.get('buyContracts'))) {
						queryEvent.combo.store.baseParams['buy'] = true;
					}
					else if (TCG.isNotBlank(record.get('sellContracts'))) {
						queryEvent.combo.store.baseParams['buy'] = false;
					}
					if (TCG.getValue('security.instrument.hierarchy.investmentType.name', record.json) === 'Options') {
						// filter to those in format "[Buy|Sell] To [Open|Close]"
						queryEvent.combo.store.baseParams['name'] = ' To ';
					}
					else {
						// filter to Buy or Sell only for non-options
						queryEvent.combo.store.baseParams['open'] = true;
						queryEvent.combo.store.baseParams['close'] = true;
					}
				}
			},
			renderer: function(v, metaData) {
				if (TCG.isNotBlank(v)) {
					metaData.css = v.includes('Buy') ? 'buy-light' : 'sell-light';
				}
				return v;
			}
		},
		{
			header: 'Trade Impact', width: 75, type: 'currency', numberFormat: '0,000', useNull: true, summaryType: 'sum', negativeInRed: true,
			allViews: true,
			tooltip: 'Impact on Overlay Exposure from the Buy or Sell trade entered',
			renderer: function(v, p, r) {
				const exp = Clifton.portfolio.run.trade.calculateExposure(r, r.data.buyContracts - r.data.sellContracts, r.data.tradeValue);
				return (exp === 0) ? '' : TCG.renderAmount(exp, false, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryCalculation: function(v, r, field, data, col) {
				const exp = Clifton.portfolio.run.trade.calculateExposure(r, r.data.buyContracts - r.data.sellContracts, r.data.tradeValue);
				return v + exp;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},

		// Final Results - Exposure, etc. Based on All Actual, Current, Pending, and Entered Trades
		{
			header: 'Final Overlay Exposure', dataIndex: 'Final Overlay Exposure', width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			allViews: true,
			viewNameHeaders: [
				{name: 'Overlay', label: 'Final Overlay Exposure'},
				{name: 'Fully Funded', label: 'Final Exposure'},
				{name: 'Structured Options', label: 'Final Exposure'}
			],
			tooltip: 'Overlay Exposure + Pending Exposure + Trade Impact',
			renderer: function(v, p, r) {
				const finalExp = Clifton.product.overlay.trade.calculateFinalExposure(r);
				const repMin = r.data['rebalanceTriggerMin'];
				const repMax = r.data['rebalanceTriggerMax'];
				let repOutside = false;
				const repOff = finalExp - Clifton.product.overlay.trade.calculateFinalTarget(r);
				// If we have min, then we also have max
				if (TCG.isNotBlank(repMin)) {
					if (repOff < repMin || repOff > repMax) {
						repOutside = true;
					}
					if (TCG.isTrue(repOutside)) {
						p.css = 'ruleViolation';
						p.attr = 'qtip="Final Exposure for this replication allocation is currently outside of defined rebalance bands."';
					}
				}
				return TCG.renderAmount(finalExp, false, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryCalculation: function(v, r, field, data, col, grid) {
				const finalExp = Clifton.product.overlay.trade.calculateFinalExposure(r);

				const assetClass = r.data['overlayAssetClass.label'];
				const finalExpKey = 'Asset Class Final Exp_' + assetClass;

				if (TCG.isFalse(r.data.matchingReplication)) {
					data[finalExpKey] = (data[finalExpKey] || 0) + finalExp;

					if (TCG.isTrue(r.data['overlayAssetClass.rebalanceTriggerWarningsUsed'])) {
						const offTargetKey = 'Amount Off Target_' + assetClass;
						data[offTargetKey] = r.data['overlayAssetClass.actualAllocationAdjusted'] + (data[finalExpKey] || 0) - r.data['overlayAssetClass.rebalanceExposureTarget'];

						const min = r.data['overlayAssetClass.rebalanceTriggerMin'];
						const absMin = r.data['overlayAssetClass.rebalanceTriggerAbsoluteMin'];
						const max = r.data['overlayAssetClass.rebalanceTriggerMax'];
						const absMax = r.data['overlayAssetClass.rebalanceTriggerAbsoluteMax'];
						let outside = false;
						let outsideAbsolute = false;
						if ((TCG.isNotBlank(min) && data[offTargetKey] < min) || (TCG.isNotBlank(max) && data[offTargetKey] > max)) {
							outside = true;
						}
						if ((TCG.isNotBlank(absMin) && data[offTargetKey] < absMin) || (TCG.isNotBlank(absMax) && data[offTargetKey] > absMax)) {
							outsideAbsolute = true;
						}
						let msg = '';
						if (TCG.isTrue(outside) || TCG.isTrue(outsideAbsolute)) {
							msg = '<b>Rebalance Band Warning:</b> Off target by [' + Ext.util.Format.number(data[offTargetKey], '0,000') + '].';
							if (TCG.isTrue(outside)) {
								msg += '  Rebalance Band is [' + Ext.util.Format.number(min, '0,000') + ' - ' + Ext.util.Format.number(max, '0,000') + '].';
							}
							if (TCG.isTrue(outsideAbsolute)) {
								msg += '  Absolute Rebalance Band is [' + Ext.util.Format.number(absMin, '0,000') + ' - ' + Ext.util.Format.number(absMax, '0,000') + '].';
							}
						}
						const obj = Ext.get(r['_groupId'] + '-rb');
						if (TCG.isNotNull(obj)) {
							if (TCG.isNotBlank(msg)) {
								obj.addClass('warning-msg');
							}
							else {
								obj.removeClass('warning-msg');
							}
							obj.update(msg);

							const fp = TCG.getParentFormPanel(grid);
							let found = false;
							for (let i = 0; i < fp.rebalanceWarnings.length; i++) {
								if (fp.rebalanceWarnings[i].name === assetClass) {
									fp.rebalanceWarnings[i] = {name: assetClass, msg: msg};
									found = true;
									break;
								}
							}
							if (!found) {
								fp.rebalanceWarnings.push({name: assetClass, msg: msg});
							}
						}
					}
				}


				return v + finalExp;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},

		{
			header: 'Final Overlay Difference', dataIndex: 'Final Overlay Difference', width: 75, type: 'currency', css: 'BORDER-RIGHT: #c0c0c0 1px solid;',
			allViews: true,
			viewNameHeaders: [
				{name: 'Overlay', label: 'Final Overlay Difference'},
				{name: 'Fully Funded', label: 'Final Difference'},
				{name: 'Structured Options', label: 'Final Difference'}
			],
			tooltip: 'Overlay Target - (Overlay Exposure + Pending Exposure + Trade Impact) - Mispricing',
			renderer: function(v, p, r) {
				// Mispricing is included in target
				const target = Clifton.product.overlay.trade.calculateFinalTarget(r);
				const finalExp = Clifton.product.overlay.trade.calculateFinalExposure(r);
				return TCG.renderAmount((target - finalExp), false, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryCalculation: function(v, r, field, data, col) {
				const target = Clifton.product.overlay.trade.calculateFinalTarget(r);
				const finalExp = Clifton.product.overlay.trade.calculateFinalExposure(r);
				const d = target - finalExp;
				return v + d;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},


		// % of Total Target
		{
			header: 'Target Overlay %', dataIndex: 'Target Overlay %', hidden: true, width: 75, type: 'percent', css: 'BORDER-RIGHT: #c0c0c0 1px solid;', summaryType: 'sum', numberFormat: '0,000.00',
			tooltip: 'Target Overlay as a % of the total <b>Total Overlay Target</b>',
			viewNameHeaders: [
				{name: 'Overlay', label: 'Target Overlay %'},
				{name: 'Fully Funded', label: 'Target %'},
				{name: 'Structured Options', label: 'Target %'}
			],
			useNull: true,
			renderer: function(v, p, r) {
				// When not matching, set the percent of total
				let targetPercent = 0;
				if (TCG.isFalse(r.data.matchingReplication)) {
					const totalOverlay = r.data.overlayTargetTotal;
					if (totalOverlay && totalOverlay !== 0) {
						// Target As a Percent Of Total
						targetPercent = Clifton.product.overlay.trade.calculateFinalTarget(r) / totalOverlay * 100;
					}
					return TCG.renderAmount(targetPercent, false, '0,000.00 %');
				}

			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000.00 %');
			},
			summaryCalculation: function(v, r, field, data, col) {
				if (TCG.isFalse(r.data.matchingReplication)) {
					const totalOverlay = r.data.overlayTargetTotal;
					if (totalOverlay && totalOverlay !== 0) {
						return v + (Clifton.product.overlay.trade.calculateFinalTarget(r) / totalOverlay * 100);
					}
				}
				return v;
			}
		},


		{
			header: 'Final Overlay %', dataIndex: 'Final Overlay %', hidden: true, editable: false, width: 75, type: 'percent', css: 'BORDER-RIGHT: #c0c0c0 1px solid;', summaryType: 'sum', numberFormat: '0,000.00',
			viewNames: ['Structured Options'],
			viewNameHeaders: [
				{name: 'Overlay', label: 'Final Overlay %'},
				{name: 'Fully Funded', label: 'Final %'},
				{name: 'Structured Options', label: 'Final %'}
			],
			tooltip: 'Final Overlay Exposure as a % of the total <b>Total Overlay Target</b>',
			useNull: true,
			renderer: function(v, p, r) {
				// When not matching, set the percent of total
				let finalPercent = 0;
				if (TCG.isFalse(r.data.matchingReplication)) {
					const totalOverlay = r.data.overlayTargetTotal;
					if (totalOverlay && totalOverlay !== 0) {
						// Final As a Percent Of Total
						finalPercent = Clifton.product.overlay.trade.calculateFinalExposure(r) / totalOverlay * 100;
					}
					return TCG.renderAmount(finalPercent, false, '0,000.00 %');
				}
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000.00 %');
			},
			summaryCalculation: function(v, r, field, data, col) {
				if (TCG.isFalse(r.data.matchingReplication)) {
					const totalOverlay = r.data.overlayTargetTotal;
					if (totalOverlay && totalOverlay !== 0) {
						return v + (Clifton.product.overlay.trade.calculateFinalExposure(r) / totalOverlay * 100);
					}
				}
				return v;
			}
		},


		{
			header: 'Target - Final Overlay %', dataIndex: 'Target - Final Overlay %', hidden: true, width: 75, type: 'percent', css: 'BORDER-RIGHT: #c0c0c0 1px solid;', summaryType: 'sum', numberFormat: '0,000.00',
			viewNames: ['Structured Options'],
			viewNameHeaders: [
				{name: 'Overlay', label: 'Target - Final Overlay %'},
				{name: 'Fully Funded', label: 'Target - Final %'},
				{name: 'Structured Options', label: 'Target - Final %'}
			],
			tooltip: 'Target Overlay % - Final Overlay %',
			useNull: true,
			renderer: function(v, p, r) {
				// When not matching, set the percent of total
				let diffPercent = 0;
				if (TCG.isFalse(r.data.matchingReplication)) {
					const totalOverlay = r.data.overlayTargetTotal;
					if (totalOverlay && totalOverlay !== 0) {
						// Target As a Percent Of Total
						const targetPercent = Clifton.product.overlay.trade.calculateFinalTarget(r) / totalOverlay * 100;

						// Final As a Percent Of Total
						const finalPercent = Clifton.product.overlay.trade.calculateFinalExposure(r) / totalOverlay * 100;
						diffPercent = targetPercent - finalPercent;
					}
					return TCG.renderAmount(diffPercent, false, '0,000.00 %');
				}

			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000.00 %');
			},
			summaryCalculation: function(v, r, field, data, col) {
				if (TCG.isFalse(r.data.matchingReplication)) {
					const totalOverlay = r.data.overlayTargetTotal;
					if (totalOverlay && totalOverlay !== 0) {
						return v + ((Clifton.product.overlay.trade.calculateFinalTarget(r) / totalOverlay * 100) - (Clifton.product.overlay.trade.calculateFinalExposure(r) / totalOverlay * 100));
					}
				}
				return v;
			}
		}
	]

}, Clifton.portfolio.run.trade.BaseRunTradeDetailGrid);


/***********************************************************************
 * UTILITY METHODS
 * *********************************************************************/

/**
 * Here for re-use throughout Trade Creation screen to calculate Final Overlay Exposure for a given row
 */
Clifton.product.overlay.trade.calculateFinalExposure = function(row, currentContractsOnly) {
	const totalContracts = TCG.isTrue(currentContractsOnly) ? row.data.currentContractsAdjusted
		: row.data.currentContractsAdjusted + row.data.pendingContractsAdjusted + row.data.buyContracts - row.data.sellContracts;

	let curExp = 0;
	if (row.data.currencyCurrentTotalBaseAmount) {
		curExp += row.data.currencyCurrentTotalBaseAmount;
	}
	if (row.data.currencyPendingTotalBaseAmount) {
		curExp += row.data.currencyPendingTotalBaseAmount;
	}
	return Clifton.portfolio.run.trade.calculateExposure(row, totalContracts, row.data.tradeValue, curExp, row.data.totalAdditionalExposure);
};


Clifton.product.overlay.trade.calculateFinalTarget = function(row, excludeMispricing) {
	if (!excludeMispricing) {
		excludeMispricing = false;
	}
	let current = row.data.targetExposureAdjustedWithMarkToMarket;
	let curTrading = 0;
	if (row.data.currencyCurrentDenominationBaseAmount) {
		curTrading += row.data.currencyCurrentDenominationBaseAmount;
	}
	if (row.data.currencyPendingDenominationBaseAmount) {
		curTrading += row.data.currencyPendingDenominationBaseAmount;
	}
	current = current + curTrading;

	if (excludeMispricing !== true) {
		const mispricing = row.data.mispricingTradeValue;
		if (mispricing && mispricing !== 0) {
			current = current - mispricing;
		}
	}

	const ta = row.data.targetAdjustments;
	let adjustments = 0;
	if (ta) {
		for (let i = 0; i < ta.length; i++) {
			const adj = ta[i].adjustment;
			if (adj !== 0) {
				adjustments = adjustments + adj;
			}
		}
	}
	return current + adjustments;
};


Clifton.product.overlay.trade.copyTradesToTradeEntryDisabledRows = function(grid, repId, entryDisabled, f, v) {
	const thisRow = Clifton.portfolio.run.trade.findRow(grid, repId);

	// Get total across all where manual entry is allowed
	let i = 0;
	const items = grid.store.data.items;
	let row = items[i];
	let total;
	if (TCG.isFalse(entryDisabled)) {
		total = v;
	}

	while (row) {
		// skip this row since we have the latest value in v
		if (row.data['id'] !== repId && row.data['security.id'] === thisRow.data['security.id']) {
			if (TCG.isFalse(row.data['tradeEntryDisabled'])) {
				total = total + row.get(f);
			}
		}
		row = items[i++];
	}

	// Copy total across all to where manual entry is not allowed and contracts are duplicated
	i = 0;
	row = items[i];
	while (row) {
		if (row.data['security.id'] === thisRow.data['security.id']) {
			if (TCG.isTrue(row.data['tradeEntryDisabled'])) {
				row.set(f, total);
				Clifton.product.overlay.trade.updateTargetsCausedByRow(grid, row, 'ACTUAL');
				Clifton.product.overlay.trade.updateTargetsCausedByRow(grid, row, 'TRADE');
			}
			else if (row.data['id'] === repId) {
				Clifton.product.overlay.trade.updateTargetsCausedByRow(grid, row, 'ACTUAL');
				Clifton.product.overlay.trade.updateTargetsCausedByRow(grid, row, 'TRADE');
			}
		}
		row = items[i++];
	}

	// Refresh Grid So Updates Are Visible
	grid.getView().refresh(false);
	Clifton.product.overlay.trade.setTradingDisabledMessage(grid);
	grid.markModified();

};


Clifton.product.overlay.trade.setTradingDisabledMessage = function(grid) {
	let i = 0;
	const items = grid.store.data.items;
	let r = items[i];
	const acIds = [];
	while (r) {
		if (TCG.isTrue(r.data['tradeEntryDisabled'])) {
			if (acIds.indexOf(r.data['overlayAssetClass.id']) === -1) {
				const obj = Ext.fly(r['_groupId'] + '-td');
				if (TCG.isNotNull(obj)) {
					acIds.push(r.data['overlayAssetClass.id']);
					obj.update('<span ext:qtip="Trading is Disabled on this Asset Class" class="amountNegative"><span class="flag-red" style="padding-right: 10px;" >&nbsp;&nbsp;</span>&nbsp;&nbsp;[ READ ONLY ]</span>');
				}
			}
		}
		i = i + 1;
		r = items[i];
	}
};


Clifton.product.overlay.trade.updateTargets = function(grid) {
	const f = TCG.getParentFormPanel(grid).getForm();
	if (!f.formValues) {
		return;
	}

	// Don't load for closed runs
	const st = TCG.getValue('workflowStatus.name', f.formValues);
	if (st === 'Closed') {
		return;
	}

	let dependencies = grid.targetDependencyMap;
	// If not defined - needs to be loaded
	if (!dependencies) {
		const runId = TCG.getValue('id', f.formValues);
		if (runId) {
			dependencies = TCG.data.getData('productOverlayAssetClassReplicationTradeTargetDependencyList.json?runId=' + runId, grid);
			grid.targetDependencyMap = dependencies;
		}
	}

	if (!dependencies) {
		return;
	}

	// Go through each row - see if it is a cause for anything based on actuals first
	// Also call to update copyTradesToTradeEntryDisabledRows if buy or sell Contracts are populated and trade entry is not disabled on the row
	let i = 0;
	const items = grid.store.data.items;
	let r = items[i];
	while (r) {
		Clifton.product.overlay.trade.updateTargetsCausedByRow(grid, r, 'ACTUAL');
		i = i + 1;
		r = items[i];
	}

	// Then update based on any Trades/Pending Trades
	i = 0;
	r = items[i];
	while (r) {
		Clifton.product.overlay.trade.updateTargetsCausedByRow(grid, r, 'TRADE');
		i = i + 1;
		r = items[i];
	}


	// Then update targets based on matching M2M Adjustments that need to be applied to to non-matching counterparts first
	// Only called from here once since price change is reflected on load and doesn't change based on trades entered
	i = 0;
	r = items[i];
	while (r) {
		Clifton.product.overlay.trade.updateTargetsCausedByRow(grid, r, 'M2M');
		i = i + 1;
		r = items[i];
	}

	// Then check based on targets
	i = 0;
	r = items[i];
	while (r) {
		Clifton.product.overlay.trade.updateTargetsCausedByRow(grid, r, 'TARGET');
		i = i + 1;
		r = items[i];
	}

	// Refresh Grid So Updates Are Visible
	grid.getView().refresh(false);
	Clifton.product.overlay.trade.setTradingDisabledMessage(grid);
};

Clifton.product.overlay.trade.updateTargetsCausedByRow = function(grid, causeRow, triggeredBy) {
	if (causeRow) {
		const dependencies = grid.targetDependencyMap;
		if (!dependencies) {
			return;
		}

		// calc difference once
		let diff;
		if (triggeredBy === 'TARGET') {
			diff = Clifton.product.overlay.trade.calculateFinalTarget(causeRow) - causeRow.data.targetExposureAdjusted;
		}
		else if (triggeredBy === 'M2M') {
			diff = causeRow.data.markToMarketAdjustmentValue;
		}
		else if (triggeredBy === 'TRADE') {
			// Current - Actual + pending + buys - sells
			let tradeContracts = causeRow.data.currentContractsAdjusted - causeRow.data.actualContractsAdjusted;
			tradeContracts = tradeContracts + causeRow.data.pendingContractsAdjusted + causeRow.data.buyContracts - causeRow.data.sellContracts;
			let curExp = 0;
			if (causeRow.data.currencyPendingTotalBaseAmount) {
				curExp += causeRow.data.currencyPendingTotalBaseAmount;
			}
			diff = Clifton.portfolio.run.trade.calculateExposure(causeRow, tradeContracts, causeRow.data.tradeValue, curExp, causeRow.data.totalAdditionalExposure);
		}
		// Actual
		else {
			diff = Clifton.product.overlay.trade.calculateFinalExposure(causeRow) - Clifton.portfolio.run.trade.calculateExposure(causeRow, causeRow.data.actualContractsAdjusted, causeRow.data.value);
		}
		if (!diff) {
			diff = 0;
		}

		for (let i = 0; i < dependencies.length; i++) {
			const dep = dependencies[i];
			const depType = dep.dependencyType;

			if (dep.causeOverlayReplicationId === causeRow.data['id']) {

				if ((depType === 'MATCHING_TARGET' || depType === 'MATCHING_ACTUAL') && !TCG.isTrue(causeRow['hasMatchingReplication'])) {
					causeRow['hasMatchingReplication'] = true;
				}

				let processDep = false;
				if (triggeredBy === dep.triggeredBy) {
					processDep = true;
				}

				if (TCG.isTrue(processDep)) {
					const dependRow = Clifton.portfolio.run.trade.findRow(grid, dep.dependentOverlayReplicationId);
					if (dependRow) {
						// calc % of difference for each dependency
						let depApply = dep.allocationPercentage * diff;
						if (TCG.isTrue(dep.negateAllocation)) {
							depApply = depApply * -1;
						}
						depApply = depApply / 100;

						let tgs = dependRow.data.targetAdjustments;
						if (!tgs) {
							tgs = [];
						}
						let found = false;
						for (let j = 0; j < tgs.length; j++) {
							if (tgs[j].causeId === dep.causeOverlayReplicationId) {
								tgs[j].adjustment = depApply;
								found = true;
								break;
							}
						}
						if (!found) {
							tgs.push({causeId: dep.causeOverlayReplicationId, label: dep.label, adjustment: depApply});
						}

						dependRow.data.targetAdjustments = tgs;

						Clifton.product.overlay.trade.updateTargetsCausedByRow(grid, dependRow, 'TARGET');
					}
				}
			}
		}
	}
};

