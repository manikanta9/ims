// Extended Grid
TCG.use('Clifton.product.overlay.trade.RunTradeReplicationGrid');

/***********************************************************************
 * UTILITY METHODS
 * *********************************************************************/

Clifton.product.overlay.trade.group.option.calculateTotalContracts = function(r) {
	return r.data.currentContractsAdjusted + r.data.pendingContractsAdjusted + r.data.buyContracts - r.data.sellContracts;
};

/**
 * Calculates the exposure for an Option replication based on the underlying security price and number of
 * available contracts. If the currentContractsOnly is true, the underlying security's previous close price
 * and current contracts will be used for calculating the value.
 */
Clifton.product.overlay.trade.group.option.calculateOptionExposure = function(r, currentContractsOnly) {
	let price = r.data.underlyingSecurityPrice;
	if (!TCG.isTrue(currentContractsOnly) && r.data['tradeUnderlyingSecurityPrice']) {
		price = r.data['tradeUnderlyingSecurityPrice'];
	}
	const contracts = TCG.isTrue(currentContractsOnly) ? r.data.currentContractsAdjusted : Clifton.product.overlay.trade.group.option.calculateTotalContracts(r);
	let delta = r.data['tradeDelta'];
	if (TCG.isBlank(delta)) {
		delta = r.data['delta'];
	}
	return (contracts * delta * price * r.data['security.priceMultiplier'] * -1);
};

Clifton.product.overlay.trade.group.option.calculateOptionMarketValue = function(r) {
	let price = r.data.securityPrice;
	if (r.data.tradeSecurityPrice) {
		price = r.data.tradeSecurityPrice;
	}
	return Clifton.product.overlay.trade.group.option.calculateTotalContracts(r) * price * r.data['security.priceMultiplier'];
};

Clifton.product.overlay.trade.group.option.calculateOptionMarketValuePercent = function(r) {
	let targetPercent = 0;
	const totalOverlay = r.data.overlayTargetTotal;
	if (totalOverlay && totalOverlay !== 0) {
		// Target As a Percent Of Total
		targetPercent = r.data['Market Value'] / totalOverlay * 100;
	}
	return targetPercent;
};

/***********************************************************************
 * Grid
 * *********************************************************************/

Clifton.product.overlay.trade.group.option.RunTradeOptionCashFlowReplicationGrid = Ext.applyIf({
	height: 450,
	viewNames: ['Options'],
	currentViewName: 'Options',
	reloadReplications: false,

	// These fields may not be edited and may just use default value, so we always need them submitted
	// Send pricing to the server so that if we are trading, we can update the trade prices on the replication
	alwaysSubmitFields: ['tradeSecurityPrice', 'tradeSecurityPriceDate', 'tradeUnderlyingSecurityPrice', 'tradeUnderlyingSecurityPriceDate', 'tradeSecurityDirtyPrice', 'tradeSecurityDirtyPriceDate', 'tradeExchangeRate', 'tradeExchangeRateDate', 'trade.executingBrokerCompany.id', 'trade.tradeDestination.id', 'trade.holdingInvestmentAccount.id', 'buyContracts', 'sellContracts', 'replication.id', 'overlayAssetClass.id'],
	doNotSubmitFields: ['Final Overlay %', 'approveTrade', 'Difference', 'Target Contracts', 'Final Overlay Exposure', 'Pending Exposure', 'Overlay Target', 'Delta Exposure', 'Market Value', 'NAV %',
		'tradeEntryDisabled', 'overlayAssetClass.label', 'overlayAssetClass.rebalanceTriggerWarningsUsed', 'securityNotTradable', 'security.priceMultiplier', 'matchingReplication', 'pendingContractsAdjusted',
		'totalAdditionalExposure', 'labelLong', 'currentContractsAdjusted', 'actualContractsAdjusted', 'cashExposure', 'currencyCurrentTotalBaseAmount', 'replicationType.currency', 'replicationType.deltaSupported'],

	/***********************************************************************
	 * GRID LOADING/RE-LOADING
	 * *********************************************************************/

	reloadPrices: function(livePrices, reloadReplications) {
		this.reloadGrid('productOverlayRunOptionTradeGroupPopulate.json', livePrices, reloadReplications);
		if (this.getWindow().isModified()) {
			this.markModified();
		}
	},

	reloadTrades: function() {
		this.reloadGrid('productOverlayRunOptionTradeGroupTradesClear.json');
		this.getWindow().setModified(false);
		this.clearModified();
	},

	/***********************************************************************
	 * TRADE APPROVAL
	 * *********************************************************************/

	approveTrades: function() {
		const grid = this;
		let i = 0;
		const items = grid.store.data.items;
		let row = items[i];
		const details = [];
		while (row) {
			if (row.data.approveTrade) {
				details.push({
					class: 'com.clifton.product.overlay.ProductOverlayAssetClassReplication',
					id: row.data.id,
					'security.id': TCG.getValue('security.id', row.json),
					'replication.type.name': TCG.getValue('replication.type.name', row.json),
					'overlayAssetClass.accountAssetClass.id': TCG.getValue('overlayAssetClass.accountAssetClass.id', row.json)
				});
			}
			// Get the next row
			i = i + 1;
			row = items[i];
		}
		if (details.length === 0) {
			TCG.showError('No rows with pending contracts selected to approve.  Please select at least one row with pending trades to approve.');
			return;
		}
		Ext.Msg.confirm('Approve Trades', 'Are you sure you would like to approve trades for selected rows?', function(a) {
			if (a === 'yes') {
				const loader = new TCG.data.JsonLoader({
					waitMsg: 'Submitting Trades for Approval',
					waitTarget: grid,
					params: {
						'portfolioRun.id': grid.getWindow().getMainFormId(),
						beanList: Ext.util.JSON.encode(details)
					},
					onLoad: function(record, conf) {
						Ext.Msg.alert('Trades Approved', record);
						TCG.getParentFormPanel(grid).transitionRun('Completed - Trades');
					}
				});
				loader.load('productOverlayRunOptionTradeGroupApprovalProcess.json');
			}
		});
	},

	/***********************************************************************
	 * GRID EDITING
	 * *********************************************************************/

	extendedOnAfterGrandTotalUpdateFieldValue: function(v, cf) {
		const grid = this;
		const fp = TCG.getParentFormPanel(grid);
		const form = fp.getForm();

		if (cf.dataIndex === 'Overlay Target') {
			grid.store.each(function(row) {
				row.data.overlayTargetTotal = v;
			});
		}

		if (cf.dataIndex === 'Delta Exposure') {
			let totalOptionsExposure = 0,
				totalOptionCurrentExposure = 0;
			grid.store.each(function(row) {
				const deltaSupported = TCG.getValue('replicationType.deltaSupported', row.json);
				if (TCG.isTrue(deltaSupported)) {
					let rowNewExposure = row.data['Delta Exposure'];
					if (TCG.isBlank(rowNewExposure) || rowNewExposure === 0) {
						rowNewExposure = Clifton.product.overlay.trade.group.option.calculateOptionExposure(row);
					}
					totalOptionsExposure += rowNewExposure;
					totalOptionCurrentExposure += Clifton.product.overlay.trade.group.option.calculateOptionExposure(row, true);
				}
			});
			// beta summary
			this.setFormCalculatedValue('optionsExposure', totalOptionsExposure);
			this.setFormCalculatedValue('optionsCurrentExposure', totalOptionCurrentExposure);
		}

		if (cf.dataIndex === 'Market Value') {
			let totalOptionMarketValue = 0;
			grid.store.each(function(row) {
				const deltaSupported = TCG.getValue('replicationType.deltaSupported', row.json);
				if (TCG.isTrue(deltaSupported)) {
					let rowMarketValue = row.data['Market Value'];
					if (TCG.isBlank(rowMarketValue) || rowMarketValue === 0) {
						rowMarketValue = Clifton.product.overlay.trade.group.option.calculateOptionMarketValue(row);
					}
					totalOptionMarketValue += rowMarketValue;
				}
			});
			this.setFormCalculatedValue('optionsMarketValue', totalOptionMarketValue);
		}


		if (cf.dataIndex === 'Final Overlay Exposure') {
			let putExposure = 0,
				callExposure = 0,
				putMatchingExposure = 0,
				callMatchingExposure = 0;
			grid.store.each(function(row) {
				const exposure = Clifton.product.overlay.trade.calculateFinalExposure(row);
				const deltaSupported = TCG.getValue('replicationType.deltaSupported', row.json), // option replications use delta
					// Cash is a cash asset class; Put asset classes are not always marked as cash, so look for Put or Cash in labels
					// as there is not a great alternatives to matching a Put replication.
					cash = TCG.isTrue(TCG.getValue('overlayAssetClass.accountAssetClass.assetClass.cash', row.json))
						|| TCG.getValue('overlayAssetClass.label', row.json).includes('Put')
						|| TCG.getValue('overlayAssetClass.label', row.json).includes('Cash');
				if (TCG.isTrue(deltaSupported)) {
					if (TCG.isTrue(cash)) {
						putExposure += exposure;
					}
					else {
						callExposure += exposure;
					}
				}
				else {
					if (TCG.isTrue(cash)) {
						putMatchingExposure += exposure;
					}
					else {
						callMatchingExposure += exposure;
					}
				}
			});
			this.setFormCalculatedValue('putNotional', putExposure); // excess summary
			this.setFormCalculatedValue('callNotional', callExposure); // excess summary

			// Add Manager Balance to Equity
			let additionalEquityExposure = TCG.getValue('callMatchingAdditionalExposure', form.formValues);
			if (TCG.isBlank(additionalEquityExposure)) {
				additionalEquityExposure = 0;
			}
			callMatchingExposure += additionalEquityExposure;
			const callMatchingExposureCurrent = TCG.getValue('callMatchingExposureCurrent', form.formValues) + additionalEquityExposure;
			this.setFormCalculatedValue('callMatchingExposure', callMatchingExposure); // beta summary
			this.setFormCalculatedValue('callMatchingCurrentExposure', callMatchingExposureCurrent); // beta summary
			this.setFormCalculatedValue('equityCollateralExposure', callMatchingExposure); // excess summary
			this.setFormCalculatedValue('callMatchingTotalExposure', callMatchingExposure); // position summary

			// Add Manager Balance to Cash
			let putMatchingAdditionalExposure = TCG.getValue('putMatchingAdditionalExposure', form.formValues);
			if (TCG.isBlank(putMatchingAdditionalExposure)) {
				putMatchingAdditionalExposure = 0;
			}
			const totalPutMatchingExposure = putMatchingExposure + putMatchingAdditionalExposure;
			this.setFormCalculatedValue('cashCollateralExposure', totalPutMatchingExposure); // excess summary
			this.setFormCalculatedValue('putMatchingExposure', putMatchingExposure); // position summary
			this.setFormCalculatedValue('putMatchingAdditionalExposure', putMatchingAdditionalExposure); // position summary

			// excess summary calculated values
			const putExcess = totalPutMatchingExposure + putExposure,
				callExcess = callMatchingExposure + callExposure;
			this.setFormCalculatedValue('putExcess', putExcess);
			this.setFormCalculatedValue('callExcess', callExcess);
			// Excess percentage is calculated as (put/call)Excess / Collateral
			TCG.getParentFormPanel(this).setFormValue('putExcessCollateralPercent',
				Ext.util.Format.number((putExcess * 100 / totalPutMatchingExposure), '0,000.00'), true);
			TCG.getParentFormPanel(this).setFormValue('callExcessCollateralPercent',
				Ext.util.Format.number((callExcess * 100 / callMatchingExposure), '0,000.00'), true);
			this.setFormCalculatedValue('optionNotionalTotal', putExposure + callExposure);
			this.setFormCalculatedValue('collateralTotal', callMatchingExposure + totalPutMatchingExposure);
			this.setFormCalculatedValue('optionExcessTotal', putExcess + callExcess);
			// beta summary including equity updates
			const optionsExposureField = form.findField('optionsExposure');
			const optionsExposure = optionsExposureField ? optionsExposureField.getNumericValue() : 0;
			this.setFormCalculatedValue('equityOptionsOffsetExposure', callMatchingExposure + optionsExposure);
			const optionsCurrentExposureField = form.findField('optionsCurrentExposure');
			const optionsCurrentExposure = optionsCurrentExposureField ? optionsCurrentExposureField.getNumericValue() : 0;
			this.setFormCalculatedValue('equityOptionsOffsetCurrentExposure', callMatchingExposureCurrent + optionsCurrentExposure);
			// position summary calculated values
			const optionsMarketValueField = form.findField('optionsMarketValue');
			const optionsMarketValue = optionsMarketValueField ? optionsMarketValueField.getNumericValue() : 0;
			this.setFormCalculatedValue('portfolioTotal', callMatchingExposure + totalPutMatchingExposure + optionsMarketValue);
		}
	},

	/***********************************************************************
	 * GRID FIELDS
	 *
	 * Fields are taken and modified from RunTradeReplicationGrid
	 * *********************************************************************/

	detailColumns: [
		{header: 'AccountAssetClassID', hidden: true, width: 10, dataIndex: 'overlayAssetClass.accountAssetClass.id'},
		{header: 'Trading Disabled', hidden: true, width: 15, dataIndex: 'tradeEntryDisabled', type: 'boolean'},
		{header: 'Security Not Tradable', hidden: true, width: 15, dataIndex: 'securityNotTradable', type: 'boolean'},

		{header: 'Asset Class Replication', hidden: true, width: 150, dataIndex: 'labelLong'},
		{header: 'AssetClass', hidden: true, width: 150, dataIndex: 'overlayAssetClass.label'},
		{header: 'AssetClass Cash', hidden: true, width: 40, dataIndex: 'overlayAssetClass.accountAssetClass.assetClass.cash', type: 'boolean'},

		{header: 'Replication', hidden: true, width: 140, dataIndex: 'replication.name'},
		{header: 'Currency Replication', hidden: true, width: 40, dataIndex: 'replicationType.currency', type: 'boolean'},
		{header: 'Delta Supported Replication', hidden: true, width: 40, dataIndex: 'replicationType.deltaSupported', type: 'boolean'},
		{header: 'Matching Replication', hidden: true, width: 40, dataIndex: 'matchingReplication', type: 'boolean'},
		{header: 'Cash Exposure', hidden: true, width: 40, dataIndex: 'cashExposure', type: 'boolean'},

		// Security Info
		{header: 'SecurityID', hidden: true, width: 10, dataIndex: 'security.id'},

		{
			header: 'Security End Date', hidden: true, width: 50, dataIndex: 'security.endDate',
			viewNames: ['Options'],
			viewNameHeaders: [
				{name: 'Options', label: 'Expiration'}
			]
		},
		{
			header: 'Security', width: 120, dataIndex: 'security.symbol',
			allViews: true,
			viewNameHeaders: [
				{name: 'Options', label: 'Ticker'}
			],
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data.cashExposure)) {
					return 'Cash';
				}
				return v;
			}
		},

		// Contracts
		{header: 'Actual Contracts (Actual)', hidden: true, width: 70, dataIndex: 'actualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
		{header: 'Actual Contracts (Virtual)', hidden: true, width: 70, dataIndex: 'virtualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
		{
			header: 'Actual Contracts', hidden: true, width: 80, dataIndex: 'actualContractsAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data.cashExposure)) {
					return null;
				}
				return TCG.renderAdjustedAmount(v, r.data['actualContracts'] !== v, r.data['actualContracts'], '0,000', 'Virtual Contracts');
			},
			summaryCondition: function(data) {
				return TCG.isFalse(data.cashExposure);
			}
		},
		{header: 'Current Contracts (Actual)', hidden: true, width: 70, dataIndex: 'currentContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
		{header: 'Current Contracts (Virtual)', hidden: true, width: 70, dataIndex: 'currentVirtualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
		{
			header: 'Contracts', width: 60, dataIndex: 'currentContractsAdjusted', type: 'int', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			allViews: true,
			tooltip: 'Total Contracts that apply to this replication that have been booked to the General Ledger as of the following business day from the run balance date.',
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.data.cashExposure)) {
					return null;
				}
				if (r.data.actualContractsAdjusted !== v || r.data.currentContracts !== v) {
					metaData.css = 'amountAdjusted';
					let qtip = '<table>';
					qtip += '<tr><td>&nbsp;</td><td align="right">Actual</td><td>&nbsp;</td><td align="right">Virtual</td><td>&nbsp;</td><td align="right">Total</td></tr>';
					qtip += '<tr><td>Previous Close:</td><td align="right">' + Ext.util.Format.number(r.data.actualContracts, '0,000') + '</td><td>&nbsp;</td><td align="right">' + Ext.util.Format.number(r.data.virtualContracts, '0,000') + '</td><td>&nbsp;</td><td align="right">' + Ext.util.Format.number(r.data.actualContractsAdjusted, '0,000') + '</td></tr>';
					if (r.data.actualContractsAdjusted !== v) {
						qtip += '<tr><td>Current:</td><td align="right">' + Ext.util.Format.number(r.data.currentContracts, '0,000') + '</td><td>&nbsp;</td><td align="right">' + Ext.util.Format.number(r.data.currentVirtualContracts, '0,000') + '</td><td>&nbsp;</td><td align="right">' + Ext.util.Format.number(r.data.currentContractsAdjusted, '0,000') + '</td></tr>';
						qtip += '<tr><td colspan="6"><hr/></td></tr>';
						qtip += '<tr><td>Change:</td><td align="right">' + Ext.util.Format.number(r.data.currentContracts - r.data.actualContracts, '0,000') + '</td><td>&nbsp;</td><td align="right">' + Ext.util.Format.number(r.data.currentVirtualContracts - r.data.virtualContracts, '0,000') + '</td><td>&nbsp;</td><td align="right">' + Ext.util.Format.number(r.data.currentContractsAdjusted - r.data.actualContractsAdjusted, '0,000') + '</td></tr>';
					}
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return TCG.numberFormat(v, '0,000');
			},
			summaryCondition: function(data) {
				return TCG.isFalse(data.cashExposure);
			}
		},
		{header: 'Pending Contracts (Actual)', hidden: true, width: 70, dataIndex: 'pendingContracts', useNull: true, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
		{
			header: 'Pending Contracts', width: 60, dataIndex: 'pendingContractsAdjusted', type: 'int', summaryType: 'sum', negativeInRed: true, useNull: true, hideGrandTotal: true,
			allViews: true,
			tooltip: 'Pending Trades (Not Closed) + Pending Transfers (Unbooked Transfers To/From this account)',
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data.cashExposure)) {
					return null;
				}
				return TCG.renderAdjustedAmount(v, r.data['pendingContracts'] !== v, r.data['pendingContracts'], '0,000', 'Virtual Contracts');
			}
		},

		// Trading Fields
		{
			header: 'Buy', width: 50, dataIndex: 'buyContracts', type: 'int', useNull: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			allViews: true,
			editor: {
				xtype: 'spinnerfield', allowBlank: true, minValue: 1, maxValue: 1000000000,
				listeners: {
					blur: field => {
						if (TCG.isNotBlank(field.getValue() && TCG.isNumber(field.getValue())) && field.getValue() > 0) {
							Clifton.portfolio.run.trade.updateRecordOpenCloseType(field.gridEditor.containerGrid, field.gridEditor.record, true);
						}
					}
				}
			},
			renderer: function(value, metaData, r) {
				const tradingDisabled = r.data.tradeEntryDisabled;

				if (TCG.isNotBlank(value) && value > 0) {
					if (TCG.isFalse(tradingDisabled)) {
						let target = Clifton.product.overlay.trade.calculateFinalTarget(r);
						if (TCG.isTrue(r.data['replicationType.currency'])) {
							target = target - r.data.currencyCurrentTotalBaseAmount;
						}
						const originalDifference = (target / r.data.tradeValue) - r.data.currentContractsAdjusted - r.data.pendingContractsAdjusted;
						if (originalDifference - value < -5) {
							metaData.css = 'ruleViolation';
							metaData.attr = 'qtip="Buys entered exceeds suggested trade amount by ' + Ext.util.Format.number((originalDifference - value) * -1, '0,000') + ' contracts."';
						}
						else {
							metaData.css = 'buy-dark';
						}
					}
					else {
						metaData.css = 'buy-dark';
					}

				}
				if (TCG.isTrue(r.data.securityNotTradable)) {
					metaData.attr = 'qtip="Security is non-tradable.  Trades cannot be entered."';
				}
				else if (TCG.isTrue(r.json.cashExposure)) {
					metaData.attr = 'qtip="Security is non-tradable.  Security represents cash exposure."';
				}
				else if (TCG.isTrue(tradingDisabled)) {
					metaData.attr = 'qtip="Trading against this asset class is disabled. Trades populated here are from security trades entered in other asset class replications on this screen."';
				}
				return TCG.numberFormat(value, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.tradeEntryDisabled);
			},

			summaryRenderer: function(value) {
				return TCG.numberFormat(value, '0,000');
			}
		},
		{
			header: 'Sell', width: 50, dataIndex: 'sellContracts', type: 'int', useNull: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid; BORDER-RIGHT: #c0c0c0 1px solid;',
			allViews: true,
			editor: {
				xtype: 'spinnerfield', allowBlank: true, minValue: 1, maxValue: 1000000000,
				listeners: {
					blur: field => {
						if (TCG.isNotBlank(field.getValue() && TCG.isNumber(field.getValue())) && field.getValue() > 0) {
							Clifton.portfolio.run.trade.updateRecordOpenCloseType(field.gridEditor.containerGrid, field.gridEditor.record, false);
						}
					}
				}
			},
			renderer: function(value, metaData, r) {
				const tradingDisabled = r.data.tradeEntryDisabled;

				if (TCG.isNotBlank(value) && value > 0) {
					if (TCG.isFalse(tradingDisabled)) {
						let target = Clifton.product.overlay.trade.calculateFinalTarget(r);
						if (TCG.isTrue(r.data['replicationType.currency'])) {
							target = target - r.data.currencyCurrentTotalBaseAmount;
						}
						let originalDifference = (target / r.data.tradeValue) - r.data.currentContractsAdjusted - r.data.pendingContractsAdjusted;

						if (originalDifference > 0) {
							metaData.css = 'ruleViolation';
							metaData.attr = 'qtip="Suggested to buy ' + Ext.util.Format.number(originalDifference, '0,000') + ' trades, however sells were entered."';
						}
						else {
							originalDifference = originalDifference * -1; // Make it positive
							if (originalDifference - value < -5) {
								metaData.css = 'ruleViolation';
								metaData.attr = 'qtip="Sells entered exceeds suggested trade amount by ' + Ext.util.Format.number((originalDifference - value) * -1, '0,000') + ' contracts."';
							}
							else {
								metaData.css = 'sell-dark';
							}
						}
					}
					else {
						metaData.css = 'sell-dark';
					}
				}
				if (TCG.isTrue(r.data.securityNotTradable)) {
					metaData.attr = 'qtip="Security is non-tradable.  Trades cannot be entered."';
				}
				else if (TCG.isTrue(r.json.cashExposure)) {
					metaData.attr = 'qtip="Security is non-tradable.  Security represents cash exposure."';
				}
				else if (TCG.isTrue(tradingDisabled)) {
					metaData.attr = 'qtip="Trading against this asset class is disabled. Trades populated here are from security trades entered in other asset class replications on this screen."';
				}

				return TCG.numberFormat(value, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.tradeEntryDisabled);
			},

			summaryRenderer: function(value) {
				return TCG.numberFormat(value, '0,000');
			}
		},
		{
			header: 'Open/Close', width: 70, dataIndex: 'openCloseType.name', idDataIndex: 'openCloseType.id', type: 'combo', useNull: false,
			allViews: true,
			editor: {
				xtype: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeListFind.json',
				beforequery: function(queryEvent) {
					// Clear Query Store so Re-Queries each time
					this.store.removeAll();
					this.lastQuery = null;

					const record = queryEvent.combo.gridEditor.record;
					queryEvent.combo.store.baseParams = {};
					if (TCG.isNotBlank(record.get('buyContracts'))) {
						queryEvent.combo.store.baseParams['buy'] = true;
					}
					else if (TCG.isNotBlank(record.get('sellContracts'))) {
						queryEvent.combo.store.baseParams['buy'] = false;
					}
					if (TCG.getValue('security.instrument.hierarchy.investmentType.name', record.json) === 'Options') {
						// filter to those in format "[Buy|Sell] To [Open|Close]"
						queryEvent.combo.store.baseParams['name'] = ' To ';
					}
					else {
						// filter to Buy or Sell only for non-options
						queryEvent.combo.store.baseParams['open'] = true;
						queryEvent.combo.store.baseParams['close'] = true;
					}
				}
			},
			renderer: function(v, metaData) {
				if (TCG.isNotBlank(v)) {
					metaData.css = v.includes('Buy') ? 'buy-light' : 'sell-light';
				}
				return v;
			}
		},

		{
			header: 'Trade Impact', width: 75, type: 'currency', numberFormat: '0,000', useNull: true, summaryType: 'sum', negativeInRed: true, hidden: true,
			// allViews: true,
			tooltip: 'Impact on Overlay Exposure from the Buy or Sell trade entered',
			renderer: function(v, p, r) {
				const exp = Clifton.portfolio.run.trade.calculateExposure(r, r.data.buyContracts - r.data.sellContracts, r.data.tradeValue);
				return (exp === 0) ? '' : TCG.renderAmount(exp, false, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryCalculation: function(v, r, field, data, col) {
				const exp = Clifton.portfolio.run.trade.calculateExposure(r, r.data.buyContracts - r.data.sellContracts, r.data.tradeValue);
				return v + exp;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},

		// Price Info
		{header: 'Security Price (Original)', hidden: true, width: 50, dataIndex: 'securityPrice', type: 'float'},
		{
			header: 'Security Price', hidden: true, width: 50, dataIndex: 'tradeSecurityPrice', type: 'float', useNull: true,
			viewNames: ['Options'],
			viewNameHeaders: [
				{name: 'Options', label: 'Price'}
			],
			renderer: function(v, p, r) {
				return TCG.isBlank(v) ? r.data.securityPrice : v;
			}
		},
		{header: 'Security Price Date', hidden: true, width: 50, dataIndex: 'tradeSecurityPriceDate'},
		{header: 'Price Multiplier', hidden: true, width: 50, type: 'currency', dataIndex: 'security.priceMultiplier', numberFormat: '0,000'},

		// Delta
		{header: 'Delta (Original)', hidden: true, width: 50, dataIndex: 'delta', type: 'float', useNull: true},
		{
			header: 'Delta', hidden: true, width: 50, type: 'currency', dataIndex: 'tradeDelta', numberFormat: '0,000.0000', useNull: true, css: 'BORDER-RIGHT: #c0c0c0 1px solid;',
			viewNames: ['Options'],
			tooltip: 'Market Data Value for <i>Delta</i> field. <br><br><b>Special Adjustment for Options:</b>&nbsp;Delta is Negated If Call Option and Short Target or Put Option and Long Target',
			renderer: function(v, p, r) {
				return TCG.isBlank(v) ? r.data.delta : v;
			}
		},

		// Underlying Price Info
		{header: 'Underlying Security Price (Original)', hidden: true, width: 50, dataIndex: 'underlyingSecurityPrice', type: 'float'},
		{
			header: 'Underlying Security Price', hidden: true, width: 50, dataIndex: 'tradeUnderlyingSecurityPrice', type: 'float', useNull: true,
			viewNames: ['Options'],
			viewNameHeaders: [
				{name: 'Options', label: 'Underlying Price'}
			],
			renderer: function(v, p, r) {
				return TCG.isBlank(v) ? r.data.underlyingSecurityPrice : v;
			}
		},
		{header: 'Underlying Security Price Date', hidden: true, width: 50, dataIndex: 'tradeUnderlyingSecurityPriceDate'},

		// Dirty Price Info
		{header: 'Security Dirty Price (Original)', hidden: true, width: 50, dataIndex: 'securityDirtyPrice', type: 'float'},
		{
			header: 'Security Dirty Price', hidden: true, width: 50, dataIndex: 'tradeSecurityDirtyPrice', type: 'float',
			tooltip: 'Used for Bond Replications.  <br>Dirty Price = Index Ratio * Clean Price * (1 + Accrued Interest / Notional). NOTE: Receivables are NOT included in the Dirty Price and thus not included in the Overlay Exposure.'
		},
		{header: 'Security Dirty Price Date', hidden: true, width: 50, dataIndex: 'tradeSecurityDirtyPriceDate'},

		// Contract Value Price Info
		{header: 'Price Field Name', width: 20, hidden: true, dataIndex: 'contractValuePriceField'},
		{
			header: 'Price(s)', width: 50, hidden: true, type: 'float', dataIndex: 'contractValuePrice',
			viewNames: ['Options'],
			tooltip: 'Latest Price info that is relevant to the contract value. i.e. If the contract value is calculated using the security price, underlying security price, security dirty price or strike price',
			renderer: function(v, metaData, r) {
				let qtip = '';
				let val;
				const fld = r.data.contractValuePriceField;
				let showAdjusted = false;

				if (TCG.isNotBlank(v)) {
					val = v;
					if (fld === 'strikePrice') {
						qtip = 'Strike Price';
					}
					else {
						qtip = fld;
					}
				}
				else if (fld === 'underlyingSecurityPrice') {
					// Underlying Security Price
					val = r.data.underlyingSecurityPrice;
					if (r.data.tradeUnderlyingSecurityPrice) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Underlying Price', r.data.underlyingSecurityPrice, r.data.tradeUnderlyingSecurityPrice, true, r.data.tradeUnderlyingSecurityPriceDate);
						val = r.data.tradeUnderlyingSecurityPrice;
						showAdjusted = !r.data.tradeUnderlyingSecurityPriceDate;
					}
					else {
						qtip = 'Underlying Price';
					}
				}
				else if (fld === 'securityDirtyPrice') {
					// Security Dirty Price
					val = r.data.securityDirtyPrice;
					if (r.data.tradeSecurityDirtyPrice) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Dirty Price', r.data.securityDirtyPrice, r.data.tradeSecurityDirtyPrice, false, r.data.tradeSecurityDirtyPriceDate);
						val = r.data.tradeSecurityDirtyPrice;
					}
					else {
						qtip = 'Dirty Price';
					}
				}
				else {
					// Security Price
					val = r.data.securityPrice;
					if (r.data.tradeSecurityPrice) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Security Price', r.data.securityPrice, r.data.tradeSecurityPrice, true, r.data.tradeSecurityPriceDate);
						val = r.data.tradeSecurityPrice;
						showAdjusted = !r.data.tradeSecurityPriceDate;
					}
					else {
						qtip = 'Security Price';
					}
				}
				if (TCG.isNotBlank(qtip)) {
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				if (showAdjusted) {
					metaData.css = 'amountAdjusted';
				}
				return val;
			}
		},

		// Exchange Rate Info
		{header: 'Exchange Rate (Original)', hidden: true, width: 50, dataIndex: 'exchangeRate', type: 'currency'},
		{header: 'Exchange Rate', hidden: true, width: 50, dataIndex: 'tradeExchangeRate', type: 'currency', useNull: true},
		{header: 'Exchange Rate Date', hidden: true, width: 50, dataIndex: 'tradeExchangeRateDate'},

		// Currency Info
		{header: 'CCY Local (Pending)', hidden: true, width: 50, dataIndex: 'currencyPendingLocalAmount', numberFormat: '0,000'},
		{header: 'CCY Other (Pending)', hidden: true, width: 50, dataIndex: 'currencyPendingOtherBaseAmount', numberFormat: '0,000'},

		// Rebalance Triggers (Used for Outside of Rebalance Band Warnings)
		{header: 'AssetClass Rebalance Min', hidden: true, width: 50, dataIndex: 'overlayAssetClass.rebalanceTriggerMin', type: 'currency', numberFormat: '0,000', useNull: true},
		{header: 'AssetClass Rebalance Max', hidden: true, width: 50, dataIndex: 'overlayAssetClass.rebalanceTriggerMax', type: 'currency', numberFormat: '0,000', useNull: true},
		{header: 'AssetClass Rebalance Absolute Min', hidden: true, width: 50, dataIndex: 'overlayAssetClass.rebalanceTriggerAbsoluteMin', type: 'currency', numberFormat: '0,000', useNull: true},
		{header: 'AssetClass Rebalance Absolute Max', hidden: true, width: 50, dataIndex: 'overlayAssetClass.rebalanceTriggerAbsoluteMax', type: 'currency', numberFormat: '0,000', useNull: true},
		{header: 'Asset Class Rebalance Exposure Target', hidden: true, width: 50, dataIndex: 'overlayAssetClass.rebalanceExposureTarget', type: 'currency', numberFormat: '0,000', useNull: true},
		{header: 'Asset Class Actual Allocation Adjusted', hidden: true, width: 50, dataIndex: 'overlayAssetClass.actualAllocationAdjusted', type: 'currency', numberFormat: '0,000', useNull: true},
		{
			header: 'Rebalance Trigger Warnings Used', hidden: true, width: 50, dataIndex: 'overlayAssetClass.rebalanceTriggerWarningsUsed', type: 'boolean',
			tooltip: 'Applies when Rebalance Triggers are used the the Rebalance Action needs some action, i.e. Contact Client, Rebalance'
		},
		// Rebalance Triggers
		{header: 'Replication Rebalance Min', hidden: true, width: 50, dataIndex: 'rebalanceTriggerMin', type: 'currency', numberFormat: '0,000', useNull: true},
		{header: 'Replication Rebalance Max', hidden: true, width: 50, dataIndex: 'rebalanceTriggerMax', type: 'currency', numberFormat: '0,000', useNull: true},


		// Contract Value - Tooltips include price information
		{header: 'Contract Value (Original)', hidden: true, width: 55, dataIndex: 'value', type: 'currency', numberFormat: '0,000'},
		{
			header: 'Contract Value', width: 55, dataIndex: 'tradeValue', type: 'currency', numberFormat: '0,000', negativeInRed: true, css: 'BORDER-RIGHT: #c0c0c0 1px solid;', hidden: true,
			// allViews: true,
			tooltip: 'Contract Value calculation is determined by the calculator selected for the Replication Type.',
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.data.cashExposure)) {
					return null;
				}
				const format = (v < 10 && v > -10) ? '0,000.00' : '0,000'; // bonds
				let qtip = '';
				let value = Ext.util.Format.number(v, format);
				if (r.data.tradeValue !== r.data.value) {
					qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Security Price', r.data.securityPrice, r.data.tradeSecurityPrice, true, r.data.tradeSecurityPriceDate);
					qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Underlying Price', r.data.underlyingSecurityPrice, r.data.tradeUnderlyingSecurityPrice, true, r.data.tradeUnderlyingSecurityPriceDate);
					qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Dirty Price', r.data.securityDirtyPrice, r.data.tradeSecurityDirtyPrice, false, r.data.tradeSecurityDirtyPriceDate);
					qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Exchange Rate', r.data.exchangeRate, r.data.tradeExchangeRate, false, r.data.tradeExchangeRateDate);
					metaData.css = 'amountAdjusted';
					if (TCG.isNotBlank(qtip)) {
						qtip += '</table>';
					}
					metaData.attr = 'qtip=\'' + qtip + '\'';
					value = TCG.renderAmountArrow(r.data.tradeValue > r.data.value) + value;
				}
				return value;
			}
		},

		// Exposure
		{
			header: 'Pending Exposure', hidden: true, width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			tooltip: '(Pending Contracts * Contract Value) + Pending Currency Exposure',
			renderer: function(v, metaData, r) {
				const format = '0,000';
				const pending = Clifton.portfolio.run.trade.calculateExposure(r, r.data.pendingContractsAdjusted, r.data.tradeValue, r.data.currencyPendingTotalBaseAmount, r.data.totalAdditionalExposure);

				let qtip = '';
				if (TCG.isTrue(r.data['replicationType.currency']) && r.data.currencyPendingTotalBaseAmount) {
					qtip += '<table>';
					qtip += '<tr><td>Currency Futures Pending Exposure:</td><td align="right">' + Ext.util.Format.number(pending, '0,000') + '</td></tr>';
					qtip += '<tr><td>Physical Currency Pending Exposure:</td><td align="right">' + Ext.util.Format.number((r.data.currencyPendingLocalAmount * r.data.tradeCurrencyExchangeRate), '0,000') + '</td></tr>';
					qtip += '<tr><td>Physical Other Currency Pending Exposure:</td><td align="right">' + Ext.util.Format.number(r.data.currencyPendingOtherBaseAmount, '0,000') + '</td></tr>';
					qtip += '<tr><td>Total Physical Currency Pending Exposure:</td><td align="right">' + TCG.renderAmount(r.data.currencyPendingTotalBaseAmount, false, '0,000') + '</td></tr>';
				}
				if (TCG.isNotBlank(qtip)) {
					qtip += '</table>';
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return TCG.renderAmount(pending, false, format);

			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryCalculation: function(v, r, field, data, col) {
				const pending = Clifton.portfolio.run.trade.calculateExposure(r, r.data.pendingContractsAdjusted, r.data.tradeValue);
				return v + pending;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},

		// Final Results - Exposure, etc. Based on All Actual, Current, Pending, and Entered Trades
		{
			header: 'Exposure', dataIndex: 'Delta Exposure', width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			viewNames: ['Options'],
			tooltip: '((Contracts + Pending Contracts + (Buy - Sell)) * Delta) * Underlying Price * Price Multiplier) * -1 <br/><br/>Only applies to Options.',
			renderer: function(v, p, r) {
				const value = Clifton.product.overlay.trade.group.option.calculateOptionExposure(r);
				r.data['Delta Exposure'] = value;
				return TCG.renderAmount(value, false, '0,000');
			},
			summaryTotalCondition: function(data) {
				return true;
			},
			summaryCalculation: function(v, r, field, data, col) {
				return v + r.data['Delta Exposure'];
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},
		{
			header: 'Market Value', dataIndex: 'Market Value', width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			viewNames: ['Options'],
			tooltip: '((Contracts + Pending Contracts + (Buy - Sell)) * Delta) * Security Price * Price Multiplier)',
			renderer: function(v, p, r) {
				const value = Clifton.product.overlay.trade.group.option.calculateOptionMarketValue(r);
				r.data['Market Value'] = value;
				return TCG.renderAmount(value, false, '0,000');
			},
			summaryTotalCondition: function(data) {
				return true;
			},
			summaryCalculation: function(v, r, field, data, col) {
				return v + r.data['Market Value'];
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},
		{
			header: '% NAV', dataIndex: 'NAV %', hidden: true, width: 75, type: 'percent', css: 'BORDER-RIGHT: #c0c0c0 1px solid;', numberFormat: '0,000.00',
			tooltip: '(Market Value / Portfolio Total) * 100',
			viewNames: ['Options'],
			useNull: true,
			renderer: function(v, p, r) {
				const percent = Clifton.product.overlay.trade.group.option.calculateOptionMarketValuePercent(r);
				r.data['NAV %'] = percent;
				return TCG.renderAmount(percent, false, '0,000.00 %');
			},
			summaryTotalCondition: function(data) {
				return true;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000.00 %');
			},
			summaryCalculation: function(v, r, field, data, col) {
				return v + r.data['Market Value %'];
			}
		},
		{
			header: 'Final Overlay Exposure', dataIndex: 'Final Overlay Exposure', width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			allViews: true,
			viewNameHeaders: [
				{name: 'Options', label: 'Notional'}
			],
			tooltip: 'Overlay Exposure + Pending Exposure + Trade Impact',
			renderer: function(v, p, r) {
				const finalExp = Clifton.product.overlay.trade.calculateFinalExposure(r);
				const repMin = r.data['rebalanceTriggerMin'];
				const repMax = r.data['rebalanceTriggerMax'];
				let repOutside = false;
				const repOffset = finalExp - Clifton.product.overlay.trade.calculateFinalTarget(r);
				// If we have min, then we also have max
				if (TCG.isNotBlank(repMin)) {
					if (repOffset < repMin || repOffset > repMax) {
						repOutside = true;
					}
					if (TCG.isTrue(repOutside)) {
						p.css = 'ruleViolation';
						p.attr = 'qtip="Final Exposure for this replication allocation is currently outside of defined rebalance bands."';
					}
				}
				return TCG.renderAmount(finalExp, false, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data.matchingReplication);
			},
			summaryCalculation: function(v, r, field, data, col, grid) {
				const finalExp = Clifton.product.overlay.trade.calculateFinalExposure(r);

				const assetClass = r.data['overlayAssetClass.label'];
				const finalExpKey = 'Asset Class Final Exp_' + assetClass;

				if (TCG.isFalse(r.data.matchingReplication)) {
					data[finalExpKey] = (data[finalExpKey] || 0) + finalExp;

					if (TCG.isTrue(r.data['overlayAssetClass.rebalanceTriggerWarningsUsed'])) {
						const offTargetKey = 'Amount Off Target_' + assetClass;
						data[offTargetKey] = r.data['overlayAssetClass.actualAllocationAdjusted'] + (data[finalExpKey] || 0) - r.data['overlayAssetClass.rebalanceExposureTarget'];

						const min = r.data['overlayAssetClass.rebalanceTriggerMin'];
						const absMin = r.data['overlayAssetClass.rebalanceTriggerAbsoluteMin'];
						const max = r.data['overlayAssetClass.rebalanceTriggerMax'];
						const absMax = r.data['overlayAssetClass.rebalanceTriggerAbsoluteMax'];
						let outside = false;
						let outsideAbsolute = false;
						if ((TCG.isNotBlank(min) && data[offTargetKey] < min) || (TCG.isNotBlank(max) && data[offTargetKey] > max)) {
							outside = true;
						}
						if ((TCG.isNotBlank(absMin) && data[offTargetKey] < absMin) || (TCG.isNotBlank(absMax) && data[offTargetKey] > absMax)) {
							outsideAbsolute = true;
						}
						let msg = '';
						if (TCG.isTrue(outside) || TCG.isTrue(outsideAbsolute)) {
							msg = '<b>Rebalance Band Warning:</b> Off target by [' + Ext.util.Format.number(data[offTargetKey], '0,000') + '].';
							if (TCG.isTrue(outside)) {
								msg += '  Rebalance Band is [' + Ext.util.Format.number(min, '0,000') + ' - ' + Ext.util.Format.number(max, '0,000') + '].';
							}
							if (TCG.isTrue(outsideAbsolute)) {
								msg += '  Absolute Rebalance Band is [' + Ext.util.Format.number(absMin, '0,000') + ' - ' + Ext.util.Format.number(absMax, '0,000') + '].';
							}
						}
						const obj = Ext.get(r['_groupId'] + '-rb');
						if (TCG.isNotNull(obj)) {
							if (TCG.isNotBlank(msg)) {
								obj.addClass('warning-msg');
							}
							else {
								obj.removeClass('warning-msg');
							}
							obj.update(msg);

							const fp = TCG.getParentFormPanel(grid);
							let found = false;
							for (let i = 0; i < fp.rebalanceWarnings.length; i++) {
								if (fp.rebalanceWarnings[i].name === assetClass) {
									fp.rebalanceWarnings[i] = {name: assetClass, msg: msg};
									found = true;
									break;
								}
							}
							if (!found) {
								fp.rebalanceWarnings.push({name: assetClass, msg: msg});
							}
						}
					}
				}


				return v + finalExp;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},

		// % of Total Target
		{
			header: 'Final Overlay %', dataIndex: 'Final Overlay %', hidden: true, editable: false, width: 75, type: 'percent', css: 'BORDER-RIGHT: #c0c0c0 1px solid;', summaryType: 'sum', numberFormat: '0,000.00',
			tooltip: 'Final Overlay Exposure as a % of the total <b>Total Overlay Target</b>',
			useNull: true,
			renderer: function(v, p, r) {
				// When not matching, set the percent of total
				let finalPercent = 0;
				const totalOverlay = r.data.overlayTargetTotal;
				if (totalOverlay && totalOverlay !== 0) {
					// Final As a Percent Of Total
					finalPercent = Clifton.product.overlay.trade.calculateFinalExposure(r) / totalOverlay * 100;
				}
				return TCG.renderAmount(finalPercent, false, '0,000.00 %');
			},
			summaryTotalCondition: function(data) {
				return true;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000.00 %');
			},
			summaryCalculation: function(v, r, field, data, col) {
				const totalOverlay = r.data.overlayTargetTotal;
				if (totalOverlay && totalOverlay !== 0) {
					return v + (Clifton.product.overlay.trade.calculateFinalExposure(r) / totalOverlay * 100);
				}
				return v;
			}
		}
	]
}, Clifton.product.overlay.trade.RunTradeReplicationGrid);
