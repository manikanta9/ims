TCG.useAsynchronous('Clifton.product.overlay.trade.group.option.RunTradeOptionCashFlowWindow');

Clifton.product.overlay.trade.group.option.RunTradeGroupDynamicOptionRollWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Portfolio: Option Roll Trades',
	iconCls: 'run',
	width: 1300,
	height: 715,
	hideApplyButton: true,
	hideOKButton: true, // use true if only want Apply & Cancel Buttons
	hideCancelButton: true, // use true to hide Cancel button
	doNotWarnOnCloseModified: true, // use to avoid "Closing Window without Save" message. The header panel and grid are not editable in this window.

	items: [
		{xtype: 'product-run-trade-group-option-roll-form-panel'}
	]

});

Clifton.product.overlay.trade.group.option.RunTradeGroupDynamicOptionRollFormPanel = Ext.extend(TCG.form.FormPanel, {

	url: 'tradeGroupDynamic.json',
	loadValidation: false,
	dtoClassForBinding: 'com.clifton.product.overlay.trade.group.dynamic.option.ProductOverlayRunTradeGroupDynamicOptionRoll',
	// field used to pass to the server that live prices should be obtained when calculating trade cash impact
	calculateCashTradeImpact: false,

	// Use vbox layout to maximize the Grid to the remaining space of the window after the header column
	// panel is rendered. The header panel needs to have a height defined and adjusted as fields are added.
	layout: 'vbox',
	layoutConfig: {
		align: 'stretch'
	},

	appendBindingParametersToUrl: function(url) {
		url += ((url.indexOf('?') > 0) ? '&' : '?');
		url += 'requestedMaxDepth=5&enableValidatingBinding=true&dtoClassForBinding=' + this.dtoClassForBinding;
		url += '&requestedPropertiesToExcludeGlobally=tradeList|productOverlayRunOptionTradeCommand|savePropertiesAsTradeGroupNote|dynamicCalculatorBean';
		return url;
	},

	listRequestedProperties: 'clientAccount.id|clientAccount.label|clientAccount.name|holdingAccount.id|holdingAccount.number|holdingAccount.issuingCompany.name|portfolioRunId|warningMessage|doNotRemoveExpiringOptionsFromExcessCalculation|portfolioTotalValue|' + //
		'expiringPutQuantity|expiringPutExposure|expiringPutExposureActual|expiringPutExposurePending|expiringPutExposureCurrent|expiringPutTooltipString|expiringPutTradedInstrumentQuantity|' + //
		'expiringPutMarketValueActual|expiringPutMarketValueCurrent|expiringPutMarketValuePending|' + //
		'expiringCallQuantity|expiringCallExposure|expiringCallExposureActual|expiringCallExposurePending|expiringCallExposureCurrent|expiringCallTooltipString|expiringCallTradedInstrumentQuantity|' + //
		'expiringCallMarketValueActual|expiringCallMarketValueCurrent|expiringCallMarketValuePending|' + //
		'putOptionsTooltipString|putExposure|putExposureActual|putExposurePending|putExposureCurrent|' + //
		'callOptionsTooltipString|callExposure|callExposureActual|callExposurePending|callExposureCurrent|' + //
		'putMatchingReplicationLabel|putMatchingExposure|putMatchingExposureActual|putMatchingExposurePending|putMatchingExposureCurrent|putMatchingAdditionalExposureLabel|putMatchingAdditionalExposure|' + //
		'callMatchingReplicationLabel|callMatchingExposure|callMatchingExposureActual|callMatchingExposurePending|callMatchingExposureCurrent|callMatchingAdditionalExposureLabel|callMatchingAdditionalExposure|' + //
		'putTrade.id|putTradeQuantity|putTrade.workflowState.name|putTrade.commissionPerUnit|putTrade.averageUnitPrice|' + //
		'callTrade.id|callTradeQuantity|callTrade.workflowState.name|callTrade.commissionPerUnit|callTrade.averageUnitPrice|' + //
		'putTailTrade.id|putTailTrade.quantityIntended|putTailTrade.workflowState.name|putTailTrade.commissionPerUnit|putTailTrade.averageUnitPrice|' + //
		'callTailTrade.id|callTailTrade.quantityIntended|callTailTrade.workflowState.name|callTailTrade.commissionPerUnit|callTailTrade.averageUnitPrice|' + //
		'putContractValue|callContractValue|putTrancheTradeAverage|callTrancheTradeAverage|cashBalance|putPrice|putPriceMultiplier|callPrice|callPriceMultiplier',
	listRequestedPropertiesRoot: 'data.detailList',

	getLoadURL: function() {
		const panel = this;
		const strangleMethod = panel.getFormValue('strangleCalculatorMethod');
		if (TCG.isNotBlank(strangleMethod)) {
			return this.appendBindingParametersToUrl('tradeGroupDynamicEntry.json');
		}
		const w = this.getWindow();
		if (this.url && w && w.params) {
			return this.appendBindingParametersToUrl(this.url);
		}
		return false;
	},

	getLoadParams: function(win) {
		const panel = this;
		const form = panel.getForm();
		const params = Ext.apply(win.params ? win.params : {}, form.getValues());
		params.requestedProperties = this.listRequestedProperties;
		params.requestedPropertiesRoot = this.listRequestedPropertiesRoot;
		params.calculateCashBalance = true;
		params.calculateCashTradeImpact = this.calculateCashTradeImpact;
		return params;
	},

	getSaveURL: function() {
		const form = this.getForm();
		// allow strangle to have precedence over executing actions
		const strangleMethod = form.findField('strangleCalculatorMethod').getValue();
		if (TCG.isNotBlank(strangleMethod)) {
			return this.appendBindingParametersToUrl('tradeGroupDynamicEntrySave.json');
		}
		const executeAction = form.findField('tradeGroupAction').getValue();
		if (TCG.isNotBlank(executeAction)) {
			return this.appendBindingParametersToUrl('tradeGroupDynamicActionExecute.json?enableOpenSessionInView=true');
		}
		return false;
	},

	getSubmitParams: function() {
		return {
			requestedProperties: this.listRequestedProperties,
			requestedPropertiesRoot: this.listRequestedPropertiesRoot
		};
	},

	defaults: {
		anchor: '0'
	},

	listeners: {
		beforerender: function() {
			this.add(this.groupItems);
		},
		// After Submitting Trades the group is created, but the grid is being reloaded - so we add a listener to after the reload finishes to clear the trades so it doesn't double count what was submitted and new quantities
		aftercreate: function() {
			const gp = TCG.getChildByName(this, 'option-roll-trade-grid');
			const clearTradeHandler = function() {
				// Clear submitted trades and keep track of non-submitted trade quantity modifications for future submits.
				const gridStore = gp.getStore();
				const modifiedRecords = gridStore.getModifiedRecords();
				gridStore.each(function(record) {
					const recordClientAccount = TCG.getValue('clientAccount.id', record.json),
						recordHoldingAccount = TCG.getValue('holdingAccount.id', record.json);
					record.beginEdit();
					for (let i = 0; i < modifiedRecords.length; i++) {
						const modifiedRecord = modifiedRecords[i];
						const modifiedClientAccount = TCG.getValue('clientAccount.id', modifiedRecord.json),
							modifiedHoldingAccount = TCG.getValue('holdingAccount.id', modifiedRecord.json);
						if ((recordClientAccount === modifiedClientAccount) && (recordHoldingAccount === modifiedHoldingAccount)) {
							if (TCG.isNumber(modifiedRecord.id)) {
								// submitted modified record - clear quantity
								gp.clearRecordTradeQuantities(record);
							}
							else {
								// non-submitted modified record - update quantity
								if (modifiedRecord.modified['callTradeQuantity']) {
									record.data['callTradeQuantity'] = modifiedRecord.modified['callTradeQuantity'];
								}
								if (modifiedRecord.modified['putTradeQuantity']) {
									record.data['putTradeQuantity'] = modifiedRecord.modified['putTradeQuantity'];
								}
								if (modifiedRecord.modified['holdingAccount.id']) {
									record.data['holdingAccount.id'] = modifiedRecord.modified['holdingAccount.id'];
									record.data['holdingAccount.number'] = modifiedRecord.modified['holdingAccount.number'];
								}
							}
						}
					}
					record.endEdit();
				}, gp);
				gp.getView().refresh();
				// Remove this listener
				gp.un('afterReloadGrid', clearTradeHandler, gp);
			};
			gp.on('afterReloadGrid', clearTradeHandler, gp);
		},
		afterload: function() {
			// If only the put option is defined, set the 'callSecurity.lastDeliveryDate' to the lastDeliveryDate value of the put option
			const lastDeliveryDateField = this.form.findField('callSecurity.lastDeliveryDate');
			if (TCG.isBlank(lastDeliveryDateField.getValue())) {
				lastDeliveryDateField.setValue(TCG.parseDate(TCG.getValue('putSecurity.lastDeliveryDate', this.form.formValues)));
			}
		}
	},

	groupItems: [
		{name: 'tradeGroupType.id', xtype: 'hidden'},
		//Need to add id as a hidden field and submitValue = false so that id is not double submitted
		{name: 'id', xtype: 'hidden', submitValue: false},
		{name: 'tradeGroupAction', xtype: 'hidden'},
		{name: 'strangleCalculatorMethod', xtype: 'hidden'},
		// fields for applying fill price
		{name: 'putFillPrice', xtype: 'hidden'},
		{name: 'callFillPrice', xtype: 'hidden'},
		{name: 'putTailFillPrice', xtype: 'hidden'},
		{name: 'callTailFillPrice', xtype: 'hidden'},
		// fields for applying commission overrides
		{name: 'putCommissionPerUnit', xtype: 'hidden'},
		{name: 'callCommissionPerUnit', xtype: 'hidden'},
		{name: 'putTailCommissionPerUnit', xtype: 'hidden'},
		{name: 'callTailCommissionPerUnit', xtype: 'hidden'},
		{name: 'enableCommissionRemoval', xtype: 'hidden'},
		{
			xtype: 'panel',
			layout: 'column',
			// height in the parent vbox formpanel - resize as fields are added/removed
			height: 150,
			defaults: {labelWidth: 100, layout: 'form', columnWidth: .34},
			items: [
				{
					columnWidth: .30,
					items: [
						{xtype: 'sectionheaderfield', header: 'Portfolio Info'},
						{fieldLabel: 'Client Acct Group', name: 'clientAccountGroup.name', detailIdField: 'clientAccountGroup.id', xtype: 'linkfield', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
						{fieldLabel: 'Expiration Date', name: 'expiringOptionsDate', xtype: 'datefield', readOnly: true},
						{
							fieldLabel: '', boxLabel: 'Do Not Remove Expiring from % Excess', name: 'doNotRemoveExpiringOptionsFromExcessCalculation', xtype: 'checkbox',
							qtip: 'If expiration date is current, then we generally want to remove what is expiring from the calculation of % excess since those positions will be removed (i.e. Friday trades).  However, we may want to replicate or add to existing positions, in those cases we want to see what is held, but not remove it from the % Excess calculation (i.e. Monday Trades).',
							disabled: true
						},
						{fieldLabel: 'Run Balance Date', name: 'balanceDate', xtype: 'datefield', readOnly: true},
						{
							xtype: 'radiogroup', disabled: true,
							fieldLabel: 'Position Direction',
							qtip: 'Used to filter Expiring Options and Trades for New Options as Buy for Long and Sell for Short.',
							columns: [70, 70],
							allowBlank: false,
							anchor: '-82',
							items: [
								{boxLabel: 'Long', name: 'longPositions', inputValue: true},
								{boxLabel: 'Short', name: 'longPositions', inputValue: false}
							]
						}

					]
				},
				{columnWidth: .01, items: [{html: '&nbsp;'}]},
				{
					items: [
						{xtype: 'sectionheaderfield', header: 'New Options Info'},
						{fieldLabel: 'Expiration (New)', name: 'callSecurity.lastDeliveryDate', xtype: 'datefield', submitValue: false, readOnly: true},
						{
							fieldLabel: 'Instrument', xtype: 'panel', layout: 'hbox', anchor: '0',
							items: [
								{flex: 1, name: 'optionInstrument.name', detailIdField: 'optionInstrument.id', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow'},
								{html: '&nbsp;'},
								{
									iconCls: 'bloomberg', text: 'Live Prices', xtype: 'button', width: 114, tooltip: 'Load the live data values for the specified Put and Call options. The form fields are dynamically loaded as data is retrieved, so there may be a slight delay before the values are visible.',
									listeners: {
										click: function(button, e) {
											const fp = TCG.getParentFormPanel(button);
											const form = fp.getForm();
											const putId = form.findField('putSecurity.id').getValue(),
												callId = form.findField('callSecurity.id').getValue();
											if (!TCG.isBlank(putId) && !TCG.isBlank(callId)) {
												Clifton.trade.group.updateTradeGroupDynamicOptionFormMarketDataValues(fp, [putId, callId]);
											}
											else if (!TCG.isBlank(putId)) {
												Clifton.trade.group.updateTradeGroupDynamicOptionFormMarketDataValues(fp, putId, true);
											}
											else if (!TCG.isBlank(callId)) {
												Clifton.trade.group.updateTradeGroupDynamicOptionFormMarketDataValues(fp, callId, false);
											}
										}
									}
								}
							]

						},
						{
							fieldLabel: 'Put', xtype: 'panel', layout: 'hbox', anchor: '0',
							items: [
								{flex: 1, name: 'putSecurity.label', detailIdField: 'putSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
								{html: '&nbsp;'},
								{name: 'putDelta', xtype: 'floatfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg delta for the selected Put Option.'},
								{html: '&nbsp;'},
								{name: 'putPrice', xtype: 'currencyfield', readOnly: true, width: 50, submitValue: false, qtip: 'The live Bloomberg price for the selected Put Option.'}

							]
						},
						{
							fieldLabel: 'Call', xtype: 'panel', layout: 'hbox', anchor: '0',
							items: [
								{flex: 1, name: 'callSecurity.label', detailIdField: 'callSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
								{html: '&nbsp;'},
								{name: 'callDelta', xtype: 'floatfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg delta for the selected Call Option.'},
								{html: '&nbsp;'},
								{name: 'callPrice', xtype: 'currencyfield', readOnly: true, width: 50, submitValue: false, qtip: 'The live Bloomberg price for the selected Call Option.'}

							]
						}
					]
				},
				{columnWidth: .01, items: [{html: '&nbsp;'}]},
				{
					items: [
						{xtype: 'sectionheaderfield', header: 'Trade Info'},
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {columnWidth: .49, layout: 'form'},
							items: [
								{
									labelWidth: 115,
									items: [
										{name: 'workflowState.id', xtype: 'hidden'},
										{fieldLabel: 'Workflow State', name: 'workflowState.name', xtype: 'displayfield'},
										{fieldLabel: 'Strangle Group', name: 'strangleGroup.id', xtype: 'linkfield', detailIdField: 'strangleGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow', submitDetailField: false},
										{fieldLabel: 'Put Outright Group', name: 'strangleGroup.putTailGroup.id', xtype: 'linkfield', detailIdField: 'strangleGroup.putTailGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow', submitDetailField: false},
										{fieldLabel: 'Call Outright Group', name: 'strangleGroup.callTailGroup.id', xtype: 'linkfield', detailIdField: 'strangleGroup.callTailGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow', submitDetailField: false}
									]
								},
								{columnWidth: .02, items: [{html: '&nbsp;'}]},
								{
									labelWidth: 70,
									items: [
										{name: 'tradeDestination.id', xtype: 'hidden'},
										{name: 'executingBrokerCompany.label', xtype: 'hidden'},
										{name: 'executingBrokerCompany.id', xtype: 'hidden'},
										{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
										{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true}
									]
								}
							]
						}
					]
				}
			]
		},
		{
			xtype: 'product-run-trade-group-option-roll-form-grid',
			tradeEntrySupported: false,
			autoHeight: false,
			flex: 1 // consume remaining space of the parent vbox formpanel
		}
	]
});
Ext.reg('product-run-trade-group-option-roll-form-panel', Clifton.product.overlay.trade.group.option.RunTradeGroupDynamicOptionRollFormPanel);

Clifton.product.overlay.trade.group.option.RunTradeGroupDynamicOptionRollTradeEntryFormPanel = Ext.extend(Clifton.product.overlay.trade.group.option.RunTradeGroupDynamicOptionRollFormPanel, {

	portfoliosLoaded: false,

	setPortfoliosLoaded: function(newVal) {
		this.portfoliosLoaded = newVal;
	},

	updateTitle: function() {
		// DO NOTHING
	},

	getLoadURL: function() {
		return false;
	},

	getSaveURL: function() {
		return this.appendBindingParametersToUrl('tradeGroupDynamicEntrySave.json');
	},

	getSubmitParams: function() {
		return {
			requestedProperties: this.listRequestedProperties,
			requestedPropertiesRoot: this.listRequestedPropertiesRoot
		};
	},

	getDefaultData: function(win) {
		let dd = win.defaultData || {};
		if (win.defaultDataIsReal) {
			return dd;
		}
		dd = Ext.apply({
			traderUser: TCG.getCurrentUser(),
			tradeDate: new Date().format('Y-m-d 00:00:00'),
			balanceDate: Clifton.calendar.getBusinessDayFromDate(new Date(), -1).format('Y-m-d 00:00:00'),
			tradeGroupType: TCG.data.getData('tradeGroupTypeByName.json?name=Option Roll', this, 'trade.group.type.Option Roll'),
			newTradeDefaultPercentage: '50',
			newTradeDefaultPercentageType: 'EXPOSURE',
			tradeDestination: TCG.data.getData('tradeDestinationByName.json?name=Manual', this, 'trade.tradeDestination.Manual')
		}, dd);
		return dd;
	},

	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		// TODO: data binding for partial field updates
		// TODO: concurrent updates validation
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Saving...',
			submitEmptyText: panel.submitEmptyText,
			success: function(form, action) {
				win.windowSaveInProgress = false;
				// TODO: move the code to win.loadJsonResult (need to save the form and the panel)
				panel.fireEvent('afterload', panel, win.closeOnSuccess);
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	},

	applySecurityChange: function(securityId, fieldNameToClear) {
		if (securityId) {
			const form = this.getForm();
			// update new expiration date if changed
			const newExpirationDateField = form.findField('callSecurity.lastDeliveryDate');
			TCG.data.getDataPromise('investmentSecurity.json?id=' + securityId, this)
				.then(security => {
					const securityExpirationDate = new Date(security.lastDeliveryDate);
					if (securityExpirationDate.format('m/d/Y') !== newExpirationDateField.value) {
						const currentClearValue = newExpirationDateField.doNotClearIfRequiredChanges;
						newExpirationDateField.doNotClearIfRequiredChanges = true;
						newExpirationDateField.setValue(securityExpirationDate);
						// clear other security as date has changed
						if (fieldNameToClear) {
							const field = form.findField(fieldNameToClear);
							if (field) {
								field.clearAndReset();
							}
						}
						// reset the clear flag in the future to allow time for the events to fire on update
						window.setTimeout(() => newExpirationDateField.doNotClearIfRequiredChanges = currentClearValue, 1000);
					}
				});
		}
	},

	groupItems: [
		{
			xtype: 'security-drag-and-drop-container', height: 30,
			prepareSecurityDefaultData: function(defaultData) {
				this.resetSecurityCombos();

				const form = TCG.getParentFormPanel(this).getForm();
				const instrumentId = form.findField('optionInstrument.id').getValue();
				return TCG.data.getDataPromise('investmentTypeByName.json?name=Options', this)
					.then(investmentType => {
						defaultData.investmentType = investmentType;

						if (instrumentId) {
							return TCG.data.getDataPromise('investmentInstrument.json?id=' + instrumentId, this)
								.then(instrument => {
									defaultData.instrument = instrument;
									return defaultData;
								});
						}
						return defaultData;
					});
			},
			resetSecurityCombos: function() {
				const form = TCG.getParentFormPanel(this).getForm();
				form.findField('putSecurity.label').resetStore();
				form.findField('callSecurity.label').resetStore();
			}
		},
		{name: 'tradeGroupType.id', xtype: 'hidden'},
		{
			xtype: 'panel',
			layout: 'column',
			// height in the parent vbox formpanel - resize as fields are added/removed
			height: 155,
			items: [
				{
					columnWidth: .29,
					layout: 'form',
					items: [
						{xtype: 'sectionheaderfield', header: 'Portfolio Info'},
						{
							fieldLabel: 'Client Acct Group', name: 'clientAccountGroup.name', hiddenName: 'clientAccountGroup.id', xtype: 'combo', url: 'investmentAccountGroupListFind.json?categoryName=Investment Account Group Tags&categoryHierarchyName=Option Rolls', allowBlank: false, detailPageClass: 'Clifton.investment.account.AccountGroupWindow', width: 215,
							qtip: 'Account group selections are limited to those tagged as <i>Option Rolls</i>',
							listeners: {
								select: function(combo, record, index) {
									// On changes reset instrument drop down
									const form = combo.getParentForm().getForm();
									const instrumentField = form.findField('optionInstrument.label');
									instrumentField.clearValue();
									instrumentField.store.removeAll();
									instrumentField.lastQuery = null;
									combo.getParentForm().setPortfoliosLoaded(false);
									TCG.data.getDataPromise('investmentGroupByName.json?requestedPropertiesRoot=data&requestedProperties=id&name=Option Roll: ' + record.json.name, combo)
										.then(function(data) {
											form.findField('investmentGroupId').setValue(data ? data.id : '');
											form.findField('newTradeDefaultPercentage').setValue(record.json.name.includes('SP-20D') ? 50 : 100);
										});
								}
							}
						},
						{
							fieldLabel: 'Expiration Date', name: 'expiringOptionsDate', xtype: 'datefield', allowBlank: false,
							qtip: 'Expiration Date of Existing Options Positions.  Note: Expiring Positions will also be filtered on Options for the same underlying instrument as the Option instrument selected for the new positions.'
						},
						{
							fieldLabel: '', boxLabel: 'Do Not Remove Expiring from % Excess', name: 'doNotRemoveExpiringOptionsFromExcessCalculation', xtype: 'checkbox',
							listeners: {
								check: function(f, checked) {
									const p = TCG.getParentFormPanel(f);
									const gp = TCG.getChildByName(p, 'option-roll-trade-grid');
									gp.updateDoNotRemoveExpiringOptionsFromExcessCalculation(checked);
								}
							},
							qtip: 'If expiration date is current, then we generally want to remove what is expiring from the calculation of % excess since those positions will be removed (i.e. Friday trades).  However, we may want to replicate or add to existing positions, in those cases we want to see what is held, but not remove it from the % Excess calculation (i.e. Monday Trades).'
						},
						{
							fieldLabel: 'Run Balance Date', xtype: 'compositefield',
							items: [
								{name: 'balanceDate', xtype: 'datefield', allowBlank: false, qtip: 'Balance Date of the Portfolio Runs to use to look up existing portfolio information.  Generally this is the previous business day for the latest information, and currently uses Main Runs only for the selected accounts.'},
								{boxLabel: 'Validate Runs', qtip: 'If checked will verify that every account in the account group has a valid run to generate trades against', xtype: 'checkbox', name: 'validateRuns', checked: true}
							]
						},
						{
							xtype: 'radiogroup', readOnly: true,
							fieldLabel: 'Position Direction',
							qtip: 'Used to filter Expiring Options and Trades for New Options as Buy for Long and Sell for Short.',
							columns: [70, 70],
							allowBlank: false,
							anchor: '-82',
							items: [
								{boxLabel: 'Long', name: 'longPositions', inputValue: true},
								{boxLabel: 'Short', name: 'longPositions', inputValue: false, checked: true}
							]
						}
					]
				},

				{columnWidth: .02, items: [{html: '&nbsp;'}]},

				{
					columnWidth: .35,
					layout: 'form',
					labelWidth: 100,
					items: [
						{xtype: 'sectionheaderfield', header: 'New Options Info'},
						{
							fieldLabel: 'Expiration (New)', name: 'callSecurity.lastDeliveryDate', xtype: 'datefield', submitValue: false,
							qtip: 'Expiration Date of the New Options that will be traded here.'
						},
						// Populated when client account group is selected as the investment group id where group name = Option Roll: + Client Acct Group Name - If populated will be used to filter instrument selection below
						{xtype: 'hidden', name: 'investmentGroupId', submitValue: false},
						{
							fieldLabel: 'Instrument', xtype: 'panel', layout: 'hbox', anchor: '0',
							items: [
								{
									name: 'optionInstrument.label', hiddenName: 'optionInstrument.id', xtype: 'combo', displayField: 'label', url: 'investmentInstrumentListFind.json?investmentType=Options', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', minListWidth: 550, flex: 1,
									qtip: 'If instrument group exists with name = Option Roll: + Selected Client Account Group Name (Example: Option Roll: SPX-20D) then option instrument selections will be limited to the selected group.  If the group is missing, any Option can be selected.',
									listeners: {
										beforeQuery: function(qEvent) {
											const f = qEvent.combo.getParentForm().getForm();
											const instrumentGroupId = f.findField('investmentGroupId').getValue();
											// If blank, clears the filter
											qEvent.combo.store.setBaseParam('investmentGroupId', instrumentGroupId);
										}
									}
								},
								{html: '&nbsp;'},
								{
									iconCls: 'bloomberg', text: 'Live Prices', xtype: 'button', tooltip: 'Load the live data value(s) for the specified Put and/or Call options. The form fields are dynamically loaded as data is retrieved, so there may be a slight delay before the values are visible.',
									width: 114,
									listeners: {
										click: function(button, e) {
											const fp = TCG.getParentFormPanel(button);
											const form = fp.getForm();
											const putId = form.findField('putSecurity.id').getValue(),
												callId = form.findField('callSecurity.id').getValue();
											if (!TCG.isBlank(putId) && !TCG.isBlank(callId)) {
												Clifton.trade.group.updateTradeGroupDynamicOptionFormMarketDataValues(fp, [putId, callId]);
											}
											else if (!TCG.isBlank(putId)) {
												Clifton.trade.group.updateTradeGroupDynamicOptionFormMarketDataValues(fp, putId, true);
											}
											else if (!TCG.isBlank(callId)) {
												Clifton.trade.group.updateTradeGroupDynamicOptionFormMarketDataValues(fp, callId, false);
											}
										}
									}
								}
							]
						},
						// TODO ADD STRIKE PRICE OPTIONS AND BUTTON TO LOOK UP OR CREATE IF MISSING - INSTEAD OF BELOW COMBO BOXES MAKE LINK FIELDS
						{
							fieldLabel: 'Put', xtype: 'panel', layout: 'hbox', anchor: '0',
							items: [
								{
									name: 'putSecurity.label', hiddenName: 'putSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: true, flex: 1,
									qtip: 'Put Security that will be traded based on quantities entered below',
									requiredFields: ['optionInstrument.id', 'callSecurity.lastDeliveryDate'],
									listeners: {
										beforeQuery: function(qEvent) {
											const f = qEvent.combo.getParentForm().getForm();
											const instId = f.findField('optionInstrument.id').getValue();
											const store = qEvent.combo.store;
											store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
											store.setBaseParam('instrumentId', instId);
											store.setBaseParam('lastDeliveryDate', f.findField('callSecurity.lastDeliveryDate').value);
											store.setBaseParam('optionType', 'PUT');
										},
										select: function(combo, record, index) {
											const fp = combo.getParentForm();
											const form = fp.getForm();

											Clifton.investment.instrument.getSettlementDatePromise(record.id, form.findField('tradeDate').value, null, null, combo)
												.then(function(settlementDate) {
													form.findField('settlementDate').setValue(settlementDate);
												});
										},
										change: function(combo, newValue, oldValue) {
											if (TCG.isBlank(newValue)) {
												const form = combo.getParentForm().getForm();
												form.findField('putDelta').setValue('');
												form.findField('putPrice').setValue('');
											}
											else {
												// update new expiration date if changed
												combo.getParentForm().applySecurityChange(newValue, 'callSecurity.label');
											}
										}
									},
									getDetailPageClass: function(newInstance) {
										if (newInstance) {
											if (!this.getParentForm().getForm().findField('optionInstrument.id').value) {
												TCG.showError('You must first select Instrument for the option.', 'New Option');
												return false;
											}
											return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
										}
										return this.detailPageClass;
									},
									getDefaultData: function(f) { // defaults client/counterparty/isda
										let fp = TCG.getParentFormPanel(this);
										fp = fp.getForm();
										const investmentInstrumentId = fp.findField('optionInstrument.id').value;
										const secId = fp.findField('putSecurity.id').value;
										return {
											instrument: TCG.data.getData('investmentInstrument.json?id=' + investmentInstrumentId, this),
											securityToCopy: {id: secId},
											optionType: 'PUT'
										};
									}
								},
								{html: '&nbsp;'},
								{name: 'putDelta', xtype: 'floatfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg delta for the selected Put Option.'},
								{html: '&nbsp;'},
								{name: 'putPrice', xtype: 'currencyfield', readOnly: true, width: 50, submitValue: false, qtip: 'The live Bloomberg price for the selected Put Option.'}
							]
						},
						{
							fieldLabel: 'Call', xtype: 'panel', layout: 'hbox', anchor: '0',
							items: [
								{
									name: 'callSecurity.label', hiddenName: 'callSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: true, flex: 1,
									qtip: 'Call Security that will be traded based on quantities entered below',
									requiredFields: ['optionInstrument.id', 'callSecurity.lastDeliveryDate'],
									listeners: {
										beforeQuery: function(qEvent) {
											const f = qEvent.combo.getParentForm().getForm();
											const instId = f.findField('optionInstrument.id').getValue();
											qEvent.combo.store.baseParams = {activeOnDate: f.findField('tradeDate').value, instrumentId: instId, lastDeliveryDate: f.findField('callSecurity.lastDeliveryDate').value, optionType: 'CALL'};
										},
										change: function(combo, newValue, oldValue) {
											if (TCG.isBlank(newValue)) {
												const form = combo.getParentForm().getForm();
												form.findField('callDelta').setValue('');
												form.findField('callPrice').setValue('');
											}
											else {
												// update new expiration date if changed
												combo.getParentForm().applySecurityChange(newValue, 'putSecurity.label');
											}
										}
									},
									getDetailPageClass: function(newInstance) {
										if (newInstance) {
											if (!this.getParentForm().getForm().findField('optionInstrument.id').value) {
												TCG.showError('You must first select Instrument for the option.', 'New Option');
												return false;
											}
											return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
										}
										return this.detailPageClass;
									},
									getDefaultData: function(f) { // defaults client/counterparty/isda
										let fp = TCG.getParentFormPanel(this);
										fp = fp.getForm();
										const investmentInstrumentId = fp.findField('optionInstrument.id').value;
										const secId = fp.findField('callSecurity.id').value;
										return {
											instrument: TCG.data.getData('investmentInstrument.json?id=' + investmentInstrumentId, this),
											securityToCopy: {id: secId},
											optionType: 'CALL'
										};
									}
								},
								{html: '&nbsp;'},
								{name: 'callDelta', xtype: 'floatfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg delta for the selected Call Option.'},
								{html: '&nbsp;'},
								{name: 'callPrice', xtype: 'currencyfield', readOnly: true, width: 50, submitValue: false, qtip: 'The live Bloomberg price for the selected Call Option.'}
							]
						},
						{
							fieldLabel: '% of Expiring', xtype: 'compositefield', anchor: '0',
							items: [
								{
									fieldLabel: '', xtype: 'combo', name: 'newTradeDefaultPercentageTypeLabel', hiddenName: 'newTradeDefaultPercentageType', mode: 'local', displayField: 'name', valueField: 'value', flex: 1,
									store: new Ext.data.ArrayStore({
										fields: ['value', 'name', 'description'],
										data: [
											['EXPOSURE', 'Exposure', 'Calculate new trade quantities as a percentage of the expiring EXPOSURE based on positions booked as of the run date.'],
											['EXPOSURE_CURRENT', 'Exposure (Current)', 'Calculate new trade quantities as a percentage of the expiring EXPOSURE based on positions booked as of the following day of the run.'],
											['QUANTITY', 'Quantity', 'Calculate new trade quantities as a percentage of the expiring QUANTITY based on positions booked as of the run date.'],
											['QUANTITY_CURRENT', 'Quantity (Current)', 'Calculate new trade quantities as a percentage of the expiring QUANTITY based on positions booked as of the following day of the run.']
										]
									})
								},
								{
									name: 'newTradeDefaultPercentage', xtype: 'currencyfield', flex: 1, minValue: -100, maxValue: 100,
									width: 114,
									qtip: 'The default percentage of Expiring Puts or Expiring Calls to generate new trade quantities.  Can use percentage of expiring quantity or expiring exposure.  Calculation will always round down. Generally, 50% of expiring (when being removed), or 100% of previously traded (i.e. Expiration date not being removed).'
								}
							]
						}
					]
				},

				{columnWidth: .02, items: [{html: '&nbsp;'}]},

				{
					columnWidth: .32,
					layout: 'form',
					items: [
						{xtype: 'sectionheaderfield', header: 'Trade Info'},
						{
							fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: false, width: 225,
							listeners: {
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									qEvent.combo.store.baseParams = {
										activeOnDate: f.findField('tradeDate').value,
										tradeTypeNameEquals: 'Options'
									};
								},
								select: function(combo, record, index) {
									// Clear trade destination when executing broker is selected
									const f = combo.getParentForm().getForm();
									const eb = f.findField('executingBrokerCompany.label');
									eb.clearValue();
									eb.reset();
									eb.store.removeAll();
									eb.lastQuery = null;
								}
							}
						},
						{
							fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow', width: 225,
							displayField: 'label',
							requiredFields: ['tradeDestination.id'],
							url: 'tradeExecutingBrokerCompanyListFind.json',
							queryParam: 'executingBrokerCompanyName',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();

									combo.store.baseParams = {
										activeOnDate: f.findField('tradeDate').value,
										tradeTypeNameEquals: 'Options',
										tradeDestinationId: f.findField('tradeDestination.id').getValue()
									};
								}
							}
						},


						{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', allowBlank: false, width: 225},
						{
							fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
							listeners: {
								change: function(field, newValue, oldValue) {
									const form = field.getParentFormPanel().getForm();
									const settlementDateField = form.findField('settlementDate');

									if (TCG.isBlank(newValue)) {
										settlementDateField.setValue('');
									}
									else {
										Clifton.investment.instrument.getSettlementDatePromise(form.findField('putSecurity.id').value, newValue, null, null, field)
											.then(function(settlementDate) {
												form.findField('settlementDate').setValue(settlementDate);
											});
									}
								},
								select: function(field, date) {
									const form = TCG.getParentFormPanel(field).getForm();

									Clifton.investment.instrument.getSettlementDatePromise(form.findField('putSecurity.id').value, date, null, null, field)
										.then(function(settlementDate) {
											form.findField('settlementDate').setValue(settlementDate);
										});
								}
							}
						},
						{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield'}

					]
				}

			]
		},
		{
			xtype: 'product-run-trade-group-option-roll-form-grid',
			tradeEntrySupported: true,
			viewConfig: {markDirty: false}, // Trade Entry all entries are "dirty" so don't need red boxes everywhere
			allRows: false,
			autoHeight: false,
			flex: 1 // consume remaining space in the parent vbox formpanel
		}
	]
});
Ext.reg('product-run-trade-group-option-roll-trade-entry-form-panel', Clifton.product.overlay.trade.group.option.RunTradeGroupDynamicOptionRollTradeEntryFormPanel);


/**
 * Parses and Renders the options tooltip information as an html table
 * Supports passing in 2 tooltips, so can have one set on the left, and another set on the right (i.e. all Puts on the left, and all Calls on the right)
 */
Clifton.product.overlay.trade.group.renderOptionsTooltip = function(metaData, tooltip, tooltip2, showAdjusted) {
	// if showAdjusted then value will be displayed in blue if there is any pending or current
	const table1 = Clifton.product.overlay.trade.group.getOptionsTooltipAsTable(tooltip, TCG.isTrue(showAdjusted) ? metaData : undefined);
	const table2 = Clifton.product.overlay.trade.group.getOptionsTooltipAsTable(tooltip2, TCG.isTrue(showAdjusted) ? metaData : undefined);

	if (TCG.isNotBlank(table1) && TCG.isNotBlank(table2)) {
		const qtip = '<table><tr><td>' + table1 + '</td><td>' + table2 + '</td></tr></table>';
		metaData.attr = TCG.renderQtip(qtip);
	}
	else if (TCG.isNotBlank(table1)) {
		metaData.attr = TCG.renderQtip(table1);
	}
	else if (TCG.isNotBlank(table2)) {
		metaData.attr = TCG.renderQtip(table2);
	}
};

Clifton.product.overlay.trade.group.getOptionsTooltipAsTable = function(tooltip, metaData) {
	if (TCG.isNotBlank(tooltip)) {
		let qtip = '';
		const tooltipProperties = tooltip.split(';');
		let showCurrent = false;
		let showPending = false;
		let colspan = 3;
		for (let i = 0; i < tooltipProperties.length; i++) {
			const property = tooltipProperties[i].split('=');
			if (property[0] === 'showCurrent') {
				if (TCG.isTrue(property[1])) {
					showCurrent = true;
					colspan = colspan + 2;
				}
			}
			else if (property[0] === 'showPending') {
				if (TCG.isTrue(property[1])) {
					showPending = true;
					colspan = colspan + 4;
				}
			}
			else if (TCG.isNotBlank(property[0])) {
				if (property[0] === 'Total') {
					qtip += '<tr><td colspan="' + colspan + '"><hr /></td></tr>';
				}
				// 0 = Previous Close Quantity, 1 = Current Quantity, 2 = Pending Quantity, 3 = Previous Close Exposure, 4 = Current Exposure, 5 = Pending Exposure
				const amounts = property[1].split(':');
				qtip += '<tr><td nowrap="true">' + property[0] + '</td>';
				qtip += '<td align="right">' + TCG.renderAmount(amounts[0], true, '0,000') + '</td>';
				if (showCurrent === true) {
					qtip += '<td align="right">' + TCG.renderAmount(amounts[1], true, '0,000') + '</td>';
				}
				if (showPending === true) {
					qtip += '<td align="right">' + TCG.renderAmount(amounts[2], true, '0,000') + '</td>';
					// use unary + to convert string values to numbers
					qtip += '<td align="right">' + TCG.renderAmount(+amounts[1] + +amounts[2], true, '0,000') + '</td>';
				}
				qtip += '<td align="right">' + TCG.renderAmount(amounts[3], true, '0,000') + '</td>';
				if (showCurrent === true) {
					qtip += '<td align="right">' + TCG.renderAmount(amounts[4], true, '0,000') + '</td>';
				}
				if (showPending === true) {
					qtip += '<td align="right">' + TCG.renderAmount(amounts[5], true, '0,000') + '</td>';
					qtip += '<td align="right">' + TCG.renderAmount(TCG.parseFloat(amounts[4]) + TCG.parseFloat(amounts[5]), true, '0,000') + '</td>';
				}
			}
		}
		let header = '<table cellspacing="5" bgcolor="#eee"><tr><td>&nbsp;</td>';
		header += '<td colspan="' + (((colspan - 3) / 2) + 1) + '" align="right">Quantity</td>';
		header += '<td colspan="' + (((colspan - 3) / 2) + 1) + '" align="right">Exposure</td>';
		header += '</tr>';
		if (TCG.isTrue(showCurrent) || TCG.isTrue(showPending)) {
			if (metaData) {
				metaData.css = 'amountAdjusted';
			}
			header += '<tr><td>&nbsp;</td><td align="right">Previous Close</td>';
			if (TCG.isTrue(showCurrent)) {
				header += '<td align="right">Current</td>';
			}
			if (TCG.isTrue(showPending)) {
				header += '<td align="right">Pending</td>';
				header += '<td align="right">Total</td>';
			}
			header += '<td align="right">Previous Close</td>';
			if (TCG.isTrue(showCurrent)) {
				header += '<td align="right">Current</td>';
			}
			if (TCG.isTrue(showPending)) {
				header += '<td align="right">Pending</td>';
				header += '<td align="right">Total</td>';
			}
			header += '</tr>';
		}
		return (header + qtip + '</table>');
	}
	return '';
};

Clifton.product.overlay.trade.group.renderExcessExposureTooltip = function(type, doNotExcludeExpiring, tradeExposure, tailTradeExposure, record) {
	const lowerType = type.toLowerCase();

	const expiringActual = TCG.getValue('expiring' + type + 'ExposureActual', record.json);
	const expiringPending = TCG.getValue('expiring' + type + 'ExposurePending', record.json);
	const expiringCurrent = TCG.getValue('expiring' + type + 'ExposureCurrent', record.json);

	const actual = TCG.getValue(lowerType + 'ExposureActual', record.json);
	let pending = TCG.getValue(lowerType + 'ExposurePending', record.json);
	let current = TCG.getValue(lowerType + 'ExposureCurrent', record.json);

	// If Existing Trade, remove from pending or current (or clear amount if trade is cancelled)
	let tradeState = TCG.getValue(lowerType + 'Trade.workflowState.name', record.json);
	let tailTradeState = TCG.getValue(lowerType + 'TailTrade.workflowState.name', record.json);
	// For Strangle Previews the Existing Trade Could Be On the Trade or Tail Trade, so if only one workflow state is populated, but there is exposure in the other, use the other's workflow state
	if (tailTradeExposure !== 0 && TCG.isBlank(tailTradeState)) {
		tailTradeState = tradeState;
	}
	if (tradeExposure !== 0 && TCG.isBlank(tradeState)) {
		tradeState = tailTradeState;
	}
	if (TCG.isNotBlank(tradeState)) {
		if (tradeState === 'Booked') {
			current = current - tradeExposure;
		}
		else if (tradeState === 'Cancelled') {
			tradeExposure = 0;
		}
		else {
			pending = pending - tradeExposure;
		}
	}
	if (TCG.isNotBlank(tailTradeState)) {
		if (tailTradeState === 'Booked') {
			current = current - tailTradeExposure;
		}
		else if (tailTradeState === 'Cancelled') {
			tailTradeExposure = 0;
		}
		else {
			pending = pending - tailTradeExposure;
		}
	}

	const additionalLabel = TCG.getValue(lowerType + 'MatchingAdditionalExposureLabel', record.json);
	const additional = TCG.getValue(lowerType + 'MatchingAdditionalExposure', record.json);
	// No Pending or Current for Additional Amount

	const matchingLabel = TCG.getValue(lowerType + 'MatchingReplicationLabel', record.json);
	const matchingActual = TCG.getValue(lowerType + 'MatchingExposureActual', record.json);
	const matchingPending = TCG.getValue(lowerType + 'MatchingExposurePending', record.json);
	const matchingCurrent = TCG.getValue(lowerType + 'MatchingExposureCurrent', record.json);

	let qtip = '<table cellspacing="5">';
	qtip += '<tr><td colspan="5"><b>' + type + ' Exposure:</b></td>';
	qtip += '<tr><td>&nbsp;</td><td align="right">Previous Close</td><td align="right">Current</td><td align="right">Pending</td><td align="right">Total</td></tr>';
	qtip += '<tr><td>Original:</td><td align="right">' + TCG.renderAmount(actual + expiringActual, true, '0,000') + '</td>' + '<td align="right">' + TCG.renderAmount(current + expiringCurrent, true, '0,000') + '</td>' + '<td align="right">' + TCG.renderAmount(pending + expiringPending, true, '0,000') + '</td>' + '<td align="right">' + TCG.renderAmount(current + pending + expiringCurrent + expiringPending, true, '0,000') + '</td></tr>';
	if (doNotExcludeExpiring === false) {
		qtip += '<tr><td>Expiring:</td><td align="right">' + TCG.renderAmount(expiringActual, true, '0,000') + '</td>' + '<td align="right">' + TCG.renderAmount(expiringCurrent, true, '0,000') + '</td>' + '<td align="right">' + TCG.renderAmount(expiringPending, true, '0,000') + '</td>' + '<td align="right">' + TCG.renderAmount(expiringCurrent + expiringPending, true, '0,000') + '</td></tr>';
	}
	qtip += '<tr><td>Trade Impact:</td><td colspan="3">&nbsp;</td><td align="right">' + TCG.renderAmount((tradeExposure + tailTradeExposure), true, '0,000') + '</td></tr>';
	qtip += '<tr><td colspan="5"><hr></td></tr>';


	if (doNotExcludeExpiring === false) {
		qtip += '<tr><td><b>Final:</b></td><td colspan="3">&nbsp;</td><td align="right">' + TCG.renderAmount(current + pending + (tradeExposure + tailTradeExposure), true, '0,000') + '</td></tr>';
	}
	else {
		qtip += '<tr><td><b>Final:</b></td><td colspan="3">&nbsp;</td><td align="right">' + TCG.renderAmount(current + pending + (tradeExposure + tailTradeExposure) + expiringCurrent + expiringPending, true, '0,000') + '</td></tr>';
	}
	qtip += '<tr><td colspan="5">&nbsp;</td></tr>';

	if (matchingLabel) {
		qtip += '<tr><td>' + matchingLabel + ':</td><td align="right">' + TCG.renderAmount(matchingActual, true, '0,000') + '</td>' + '<td align="right">' + TCG.renderAmount(matchingCurrent, true, '0,000') + '</td>' + '<td align="right">' + TCG.renderAmount(matchingPending, true, '0,000') + '</td>' + '<td align="right">' + TCG.renderAmount(matchingCurrent + matchingPending, true, '0,000') + '</td></tr>';
	}
	if (additionalLabel && Ext.isNumber(additional)) {
		qtip += '<tr><td>' + additionalLabel + ':</td><td align="right">' + TCG.renderAmount(additional, true, '0,000') + '</td><td colspan="2">&nbsp;</td><td align="right">' + TCG.renderAmount(additional, true, '0,000') + '</td></tr>';
	}
	qtip += '<tr><td colspan="5"><hr></td></tr>';
	qtip += '<tr><td><b>Total Matching:</b></td><td colspan="3">&nbsp;</td><td align="right">' + TCG.renderAmount(matchingCurrent + matchingPending + additional, true, '0,000') + '</td></tr>';
	qtip += '</table>';
	return qtip;
};

Clifton.product.overlay.trade.group.getOptionMarketValueForType = function(type, record) {
	const lowerType = type.toLowerCase();

	const price = TCG.getValue(lowerType + 'Price', record.json) * TCG.getValue(lowerType + 'PriceMultiplier', record.json),
		quantity = TCG.getValue(lowerType + 'TradeQuantity', record.json),
		tailQuantity = TCG.getValue(lowerType + 'TailTrade.quantityIntended', record.json);
	let tradeMarketValue = Ext.isNumber(quantity) ? quantity * price : 0,
		tailMarketValue = Ext.isNumber(tailQuantity) ? tailQuantity * price : 0;

	if (TCG.isTrue(TCG.getValue('longPositions', record.json))) {
		tradeMarketValue = tradeMarketValue * -1;
		tailMarketValue = tailMarketValue * -1;
	}

	let pending = 0,
		current = 0;

	let tradeState = TCG.getValue(lowerType + 'Trade.workflowState.name', record.json),
		tailTradeState = TCG.getValue(lowerType + 'TailTrade.workflowState.name', record.json);

	// For Strangle Previews the Existing Trade Could Be On the Trade or Tail Trade, so if only one workflow state is populated, but there is exposure in the other, use the other's workflow state
	if (tailMarketValue !== 0 && TCG.isBlank(tailTradeState)) {
		tailTradeState = tradeState;
		if (TCG.isBlank(tailTradeState)) {
			// trade entry
			pending += tailMarketValue;
		}
	}
	if (tradeMarketValue !== 0 && TCG.isBlank(tradeState)) {
		tradeState = tailTradeState;
		if (TCG.isBlank(tradeState)) {
			// trade entry
			pending += tradeMarketValue;
		}
	}
	if (TCG.isNotBlank(tradeState)) {
		if (tradeState === 'Booked') {
			current += tradeMarketValue;
		}
		else if (tradeState !== 'Cancelled') {
			pending += tradeMarketValue;
		}
	}
	if (TCG.isNotBlank(tailTradeState)) {
		if (tailTradeState === 'Booked') {
			current += tailMarketValue;
		}
		else if (tailTradeState !== 'Cancelled') {
			pending += tailMarketValue;
		}
	}
	return [pending, current];
};

/**
 * Calculates the cash impact on the cash balance considering the expiring option positions and trade options. In addition
 * to calculating the updated cash balance as a percentage of the total portfolio value, the tooltip to display is also created.
 *
 * Returns an array of two values: cash as a percentage of total portfolio value (index 0) and the tooltip (index 1).
 */
Clifton.product.overlay.trade.group.calculateCashImpact = function(record) {
	const putType = 'Put',
		callType = 'Call';

	const putExpiringActual = TCG.getValue('expiring' + putType + 'MarketValueActual', record.json),
		putExpiringPending = TCG.getValue('expiring' + putType + 'MarketValuePending', record.json),
		putExpiringCurrent = TCG.getValue('expiring' + putType + 'MarketValueCurrent', record.json),
		callExpiringActual = TCG.getValue('expiring' + callType + 'MarketValueActual', record.json),
		callExpiringPending = TCG.getValue('expiring' + callType + 'MarketValuePending', record.json),
		callExpiringCurrent = TCG.getValue('expiring' + callType + 'MarketValueCurrent', record.json);

	const putExposures = Clifton.product.overlay.trade.group.getOptionMarketValueForType(putType, record),
		callExposures = Clifton.product.overlay.trade.group.getOptionMarketValueForType(callType, record);

	const cashBalance = TCG.getValue('cashBalance', record.json);
	const tradeCashImpact = (putExpiringCurrent + putExpiringPending) + (callExpiringCurrent + callExpiringPending) + (putExposures[1] + putExposures[0]) + (callExposures[1] + callExposures[0]);
	const finalCashBalance = cashBalance + tradeCashImpact;
	const portfolioTotal = TCG.getValue('portfolioTotalValue', record.json);
	const cashPercent = (finalCashBalance / (portfolioTotal + tradeCashImpact) * 100);

	let qtip = '<table cellspacing="5">';
	qtip += '<tr><td colspan="5"><b>Cash:</b></td>';
	qtip += '<tr><td>&nbsp;</td><td align="right">Previous Close</td><td align="right">Current</td><td align="right">Pending</td><td align="right">Total</td></tr>';
	qtip += '<tr><td>Cash Balance:</td><td align="right">' + TCG.renderAmount(cashBalance, true, '0,000') + '</td><td align="right">' + TCG.renderAmount(cashBalance, true, '0,000') + '</td><td align="right">' + TCG.renderAmount(0, true, '0,000') + '</td>' + '<td align="right">' + TCG.renderAmount(cashBalance, true, '0,000') + '</td></tr>';
	qtip += '<tr><td>Expiring Puts:</td><td align="right">' + TCG.renderAmount(putExpiringActual, true, '0,000') + '</td><td align="right">' + TCG.renderAmount(putExpiringCurrent, true, '0,000') + '</td><td align="right">' + TCG.renderAmount(putExpiringPending, true, '0,000') + '</td>' + '<td align="right">' + TCG.renderAmount(putExpiringCurrent + putExpiringPending, true, '0,000') + '</td></tr>';
	qtip += '<tr><td>Expiring Calls:</td><td align="right">' + TCG.renderAmount(callExpiringActual, true, '0,000') + '</td><td align="right">' + TCG.renderAmount(callExpiringCurrent, true, '0,000') + '</td><td align="right">' + TCG.renderAmount(callExpiringPending, true, '0,000') + '</td>' + '<td align="right">' + TCG.renderAmount(callExpiringCurrent + callExpiringPending, true, '0,000') + '</td></tr>';
	qtip += '<tr><td colspan="5"><hr></td></tr>';
	qtip += '<tr><td><b>Updated Cash:</b></td><td colspan="3">&nbsp;</td><td align="right">' + TCG.renderAmount(cashBalance + (putExpiringCurrent + putExpiringPending) + (callExpiringCurrent + callExpiringPending), true, '0,000') + '</td></tr>';
	qtip += '<tr><td colspan="5">&nbsp;</td></tr>';

	qtip += '<tr><td colspan="5"><b>Trade Impact:</b></td>';
	qtip += '<tr><td>New Puts:</td><td>&nbsp;</td><td align="right">' + TCG.renderAmount(putExposures[1], true, '0,000') + '</td><td align="right">' + TCG.renderAmount(putExposures[0], true, '0,000') + '</td><td align="right">' + TCG.renderAmount((putExposures[1] + putExposures[0]), true, '0,000') + '</td></tr>';
	qtip += '<tr><td>New Calls:</td><td>&nbsp;</td><td align="right">' + TCG.renderAmount(callExposures[1], true, '0,000') + '</td><td align="right">' + TCG.renderAmount(callExposures[0], true, '0,000') + '</td><td align="right">' + TCG.renderAmount((callExposures[1] + callExposures[0]), true, '0,000') + '</td></tr>';
	qtip += '<tr><td>Equities:</td><td colspan="3">&nbsp;</td><td align="right">' + TCG.renderAmount(0, true, '0,000') + '</td></tr>';
	qtip += '<tr><td>Cash Equivalents:</td><td colspan="3">&nbsp;</td><td align="right">' + TCG.renderAmount(0, true, '0,000') + '</td></tr>';
	qtip += '<tr><td colspan="5"><hr></td></tr>';
	qtip += '<tr><td><b>Final Cash:</b></td><td colspan="3">&nbsp;</td><td align="right">' + TCG.renderAmount(finalCashBalance, true, '0,000') + '</td></tr>';
	qtip += '<tr><td colspan="5">&nbsp;</td></tr>';
	qtip += '</table>';
	return [cashPercent, qtip];
};


Clifton.product.overlay.trade.group.option.RunTradeGroupDynamicOptionRollDetailFormGrid = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicOptionStrangleDetailFormGrid, {
	name: 'option-roll-trade-grid',
	dtoClass: 'com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunTradeGroupDynamicOptionAccountRoll',
	instructions: 'Option Roll screen is used to roll into new Options (Puts and Calls) based on existing Expiring Options and Portfolio runs for the selected Client Account Group. Expiring Positions are populated by the selected expiration date and for the same underlying instrument as the Options we are trading. Note: Previous Close means what was included on the Portfolio Run, and Current is what was booked as of the following business day from the Run Balance Date.',
	useQuantityOnTrade: false,
	emailFileNamePrefix: 'OptionRoll',

	addEntryToolbarButtons: function(toolBar, gp) {
		toolBar.add({
			text: 'Load Portfolios',
			tooltip: 'Load Portfolio Run information for selected options. All position details below will be re-generated based on above selections.',
			iconCls: 'table-refresh',
			scope: this,
			handler: function() {
				gp.submitField.setValue('[]');
				this.ownerCt.setPortfoliosLoaded(true);
				gp.reloadGrid();

			}
		});
		toolBar.add('-');

		toolBar.add({
			text: 'Submit Trades',
			tooltip: 'Create New Option Trades based on quantities entered below.',
			iconCls: 'shopping-cart',
			scope: this,
			handler: function() {
				if (this.submitField.getValue() === '[]') {
					TCG.showError('No Accounts selected.  Please select at least one Account from the list.');
					return;
				}
				else if (!this.ownerCt.portfoliosLoaded) {
					TCG.showError('Portfolios must be loaded to submit trades.  Make sure to reload portfolios after changing Client Account Group.');
					return;
				}

				gp.getWindow().saveWindow(false);
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Clear Trades',
			tooltip: 'Clear all put and call trade quantities entered below.',
			iconCls: 'clear',
			scope: this,
			handler: function() {
				gp.clearTrades();
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Info',
			tooltip: 'Toggle instructions for this page',
			iconCls: 'info',
			enableToggle: true,
			handler: function() {
				if (this.pressed) {
					gp.insert(0, {
						xtype: 'panel',
						frame: true,
						layout: 'fit',
						bodyStyle: 'padding: 3px 3px 3px 3px',
						html: gp.instructions
					});
				}
				else {
					gp.remove(0);
				}
				gp.doLayout();
			}
		}, '-');
		toolBar.add({
			text: 'Help',
			iconCls: 'help',
			handler: function() {
				TCG.openWIKI('IT/Defensive Equity: Option Rolls', 'Defensive Equity Option Rolls Help');
			}
		}, '-');

		toolBar.addFill();
		toolBar.add({xtype: 'label', html: '% Target Excess&nbsp;'});
		toolBar.add({xtype: 'label', html: 'Put:&nbsp;'});
		toolBar.add({xtype: 'currencyfield', minValue: 0, name: 'putTargetExcess', width: 70, submitValue: false});
		toolBar.add({xtype: 'label', html: '&nbsp;Call:&nbsp;'});
		toolBar.add({xtype: 'currencyfield', minValue: 0, name: 'callTargetExcess', width: 70, submitValue: false});
		toolBar.add({
			text: 'Apply Excess (Selected)',
			iconCls: 'arrow-down-green',
			tooltip: 'Calculate new quantities for option based on target excess percentages. Trade quantities will be updated on selected row(s) ONLY.',
			scope: toolBar,
			handler: function() {
				const doNotExcludeExpiring = TCG.getParentFormPanel(gp).getForm().findField('doNotRemoveExpiringOptionsFromExcessCalculation').checked;
				const putTargetExcessValue = TCG.getChildByName(toolBar, 'putTargetExcess').getValue();
				const callTargetExcessValue = TCG.getChildByName(toolBar, 'callTargetExcess').getValue();
				if (TCG.isBlank(putTargetExcessValue) && TCG.isBlank(callTargetExcessValue)) {
					TCG.showError('Please enter Put Target % Excess to calculate Put trades, and/or Call Target % Excess to calculate Call trades');
					return;
				}
				gp.getStore().each(function(record) {
					if (TCG.isTrue(record.get('applyTargetExcess'))) {
						const r = record.json;
						if (TCG.isNotBlank(putTargetExcessValue)) {
							let current = r.putExposure;
							if (doNotExcludeExpiring === true) {
								current += r.expiringPutExposure;
							}
							const additional = Ext.isNumber(r.putMatchingAdditionalExposure) ? r.putMatchingAdditionalExposure : 0;
							record.set('putTradeQuantity', gp.calculateContractQuantity(r.putMatchingExposure + additional, current, r.putContractValue, putTargetExcessValue));
						}
						if (TCG.isNotBlank(callTargetExcessValue)) {
							let current = r.callExposure;
							if (doNotExcludeExpiring === true) {
								current += r.expiringCallExposure;
							}
							const additional = Ext.isNumber(r.callMatchingAdditionalExposure) ? r.callMatchingAdditionalExposure : 0;
							record.set('callTradeQuantity', gp.calculateContractQuantity(r.callMatchingExposure + additional, current, r.callContractValue, callTargetExcessValue));
						}
					}
				}, this);
				gp.markModified();
			}
		});
		toolBar.add('-', {
			text: 'Live Prices (Cash Impact)',
			iconCls: 'bloomberg',
			tooltip: 'Use live prices for expiring options and pending trades when showing the impact on the cash balance.',
			scope: gp,
			handler: function() {
				const formPanel = TCG.getParentFormPanel(gp);
				formPanel.calculateCashTradeImpact = true;
				gp.reloadGrid();
				formPanel.calculateCashTradeImpact = false;
			}
		});
	},

	listeners: {
		afterRender: function() {
			const grid = this;
			if (grid.tradeEntrySupported === true) {
				const fp = TCG.getParentFormPanel(this);
				fp.on('afterload', function() {
					if (TCG.isTrue(grid.getWindow().savedSinceOpen)) {
						grid.reloadGrid();
					}
				}, grid);
			}

			grid.createPortfolioContextMenu();
		},
		// drill into specific trade or run
		'celldblclick': function(grid, rowIndex, cellIndex, evt) {
			const row = grid.store.data.items[rowIndex];
			if (this.tradeEntrySupported === false) {
				const columnName = grid.getColumnModel().getColumnById(cellIndex).dataIndex;
				if (columnName.includes('Trade')) {
					let tid = '';
					if (columnName.indexOf('Tail') > -1) {
						if (columnName && columnName.startsWith('put')) {
							tid = TCG.getValue('putTailTrade.id', row.json);
						}
						else if (columnName && columnName.startsWith('call')) {
							tid = TCG.getValue('callTailTrade.id', row.json);
						}
					}
					else {
						if (columnName && columnName.startsWith('put')) {
							tid = TCG.getValue('putTrade.id', row.json);
						}
						else if (columnName && columnName.startsWith('call')) {
							tid = TCG.getValue('callTrade.id', row.json);
						}
					}
					if (TCG.isNotBlank(tid)) {
						TCG.createComponent('Clifton.trade.TradeWindow', {
							id: TCG.getComponentId('Clifton.trade.TradeWindow', tid),
							params: {id: tid},
							openerCt: grid
						});
					}
					// return to avoid opening the run window too
					return;
				}
			}
			const pid = row.json['portfolioRunId'];
			if (TCG.isNotBlank(pid)) {
				TCG.createComponent('Clifton.portfolio.run.RunWindow', {
					id: TCG.getComponentId('Clifton.portfolio.run.RunWindow', pid),
					params: {id: pid},
					openerCt: grid
				});
			}
		},
		'headerclick': function(grid, col, e) {
			if (col === grid.getColumnModel().findColumnIndex('applyTargetExcess')) {
				grid.toggleAll('applyTargetExcess');
			}
			else if (col === grid.getColumnModel().findColumnIndex('includeTrade')) {
				grid.toggleAll('includeTrade');
			}
		},
		afterload: function(grid) {
			grid.reloadToolbarMenus();
			grid.addOtcExecutionContextMenuItem.call(grid);
			grid.markModified(true);
			const panel = TCG.getParentFormPanel(grid);
			const form = panel.getForm();
			const actionField = form.findField('tradeGroupAction');
			if (actionField) {
				// since we load the response data, make sure to clear action
				actionField.setValue('');
			}
			const strangleMethodField = form.findField('strangleCalculatorMethod');
			if (strangleMethodField) {
				// since we load the response data, make sure to clear strangle method
				strangleMethodField.setValue('');
			}

			if (grid.chainedRequestCallback !== undefined) {
				if (grid.applyChainedCallback === true) {
					grid.chainedRequestCallback.call(grid);
				}
				else {
					// clear the callback as the previous request failed and the callback was not executed
					grid.chainedRequestCallback = undefined;
				}
			}
		}
	},

	createPortfolioContextMenu: function() {
		const gridPanel = this;
		const columnModel = gridPanel.getColumnModel();
		const column = columnModel.getColumnAt(columnModel.findColumnIndex('clientAccount.label'));

		// set context menu on column once rather than on every right-click
		if (!column.drillDown) {
			column.drillDown = new Ext.menu.Menu({
				items: [
					{
						text: 'Open Portfolio Run',
						iconCls: 'run',
						handler: function() {
							const gp = this;
							// get current row selection for copying
							const currentRecord = gp.getStore().getAt(gp.contextRowIndex);
							const runId = currentRecord.json['portfolioRunId'];
							if (TCG.isBlank(runId)) {
								TCG.showError('Unable to get Portfolio Run ID for selected row.');
								return;
							}
							TCG.createComponent('Clifton.portfolio.run.RunWindow', {
								id: TCG.getComponentId('Clifton.portfolio.run.RunWindow', runId),
								params: {id: runId},
								openerCt: gp
							});
						},
						scope: gridPanel
					},
					{
						text: 'Open Portfolio Cash Flow Window',
						iconCls: 'shopping-cart',
						handler: function() {
							const gp = this;
							// get current row selection for copying
							const currentRecord = gp.getStore().getAt(gp.contextRowIndex);
							const runId = currentRecord.json['portfolioRunId'];
							if (TCG.isBlank(runId)) {
								TCG.showError('Unable to get Portfolio Run ID for selected row.');
								return;
							}
							TCG.createComponent('Clifton.product.overlay.trade.group.option.RunTradeOptionCashFlowWindow', {
								params: {id: runId},
								openerCt: gp
							});
						},
						scope: gridPanel
					}
				]
			});
		}

		// add menu to column
		column.on('contextmenu', function(column, grid, row, event) {
			event.preventDefault();
			// set grid row placeholder for the context menu to use
			grid.contextRowIndex = row;
			column.drillDown.showAt(event.getXY());
		}, column);
	},

	addOtcExecutionContextMenuItem: function() {
		const gridPanel = this;
		const form = TCG.getParentFormPanel(gridPanel).getForm();
		const investmentSubType2 = TCG.getValue('optionInstrument.hierarchy.investmentTypeSubType2.name', form.formValues);
		if (investmentSubType2 === 'Listed Options') {
			const columnModel = gridPanel.getColumnModel();
			const column = columnModel.getColumnAt(columnModel.findColumnIndex('clientAccount.label'));
			if (!TCG.isTrue(this.tradeEntrySupported) && column.drillDown) {
				if (!column.drillDown.get('convert-otc-menu-item')) {
					column.drillDown.add([
						'-',
						{
							text: 'Convert to OTC...',
							itemId: 'convert-otc-menu-item',
							iconCls: 'shopping-cart',
							handler: function() {
								const gp = this;

								// get current row selection for copying
								const currentRecord = gp.getStore().getAt(gp.contextRowIndex);
								const params = form.formValues;
								params['detailList'] = [currentRecord.json];
								const storeRecordCount = gp.getStore().getTotalCount();
								if (storeRecordCount === 1) {
									params.updateCurrentGroup = true;
								}
								const putState = TCG.getValue('putTrade.workflowState.name', currentRecord.json),
									callState = TCG.getValue('callTrade.workflowState.name', currentRecord.json);
								if ((putState && putState !== 'Rejected') || (callState && callState !== 'Rejected')) {
									Ext.Msg.confirm('Reject Trades', 'The current trades of the Option Roll must be rejected in order to update them ' +
										'for OTC conversion. During the conversion, the converted trades will be validated but the existing Option Roll\'s ' +
										'trades will need to be validated with the \'Validate Trades\' trade group action. ' +
										'Do you want to continue rejecting the trades?',
										function(a) {
											if (a === 'yes') {
												// reject only current row trades and not all within the trade group
												Promise.all([
													gp.getTradeRejectTransitionPromise(TCG.getValue('putTrade.id', currentRecord.json)),
													gp.getTradeRejectTransitionPromise(TCG.getValue('putTailTrade.id', currentRecord.json)),
													gp.getTradeRejectTransitionPromise(TCG.getValue('callTrade.id', currentRecord.json)),
													gp.getTradeRejectTransitionPromise(TCG.getValue('callTailTrade.id', currentRecord.json))
												]).then(function() {
													TCG.createComponent('Clifton.product.overlay.trade.group.option.RunTradeGroupOtcOptionWindow', {
														params: params,
														openerCt: gp
													});
												});

											}
										}
									);
								}
								else {
									TCG.createComponent('Clifton.product.overlay.trade.group.option.RunTradeGroupOtcOptionWindow', {
										params: params,
										openerCt: gp
									});
								}
							},
							scope: gridPanel
						}
					]);
				}
			}
		}
	},

	getTradeRejectTransitionPromise: function(tradeId) {
		if (TCG.isNotBlank(tradeId)) {
			const gridPanel = this;
			return TCG.data.getDataPromise('workflowTransitionNextListByEntity.json?requestedMaxDepth=3&&tableName=Trade&id=' + tradeId, gridPanel)
				.then(function(transitions) {
					if (transitions) {
						for (let j = 1; j < transitions.length; j++) {
							const t = transitions[j];
							if (t.name.includes('Reject')) {
								return t;
							}
						}
					}
				})
				.then(function(rejectTransition) {
					if (rejectTransition) {
						return TCG.data.getDataPromise('workflowTransitionExecuteByTransition.json', gridPanel, {
							params: {
								tableName: 'Trade',
								id: tradeId,
								workflowTransitionId: rejectTransition.id
							},
							waitMsg: 'Executing Transition',
							waitTarget: gridPanel,
							timeout: 40000
						});
					}
				});
		}
	},

	updateDoNotRemoveExpiringOptionsFromExcessCalculation: function(doNotRemoveExpiring) {
		const gp = this;

		gp.getStore().each(function(record) {
			record.set('doNotRemoveExpiringOptionsFromExcessCalculation', doNotRemoveExpiring);
		});
		gp.getView().refresh();
	},

	calculateContractQuantity: function(matchingExposure, currentOptionExposure, newOptionContractValue, targetExcessPercent) {
		// Need to Back into Contract Quantity from this formula (where x is the contract exposure we need)
		// 1 - ((ABS(Options + x) / Matching) = TargetExcessPercent (after divide by 100 for % value)
		targetExcessPercent = targetExcessPercent / 100;
		targetExcessPercent = -(targetExcessPercent - 1);

		let x = targetExcessPercent * matchingExposure;
		x = x - Math.abs(currentOptionExposure);
		const quantity = x / newOptionContractValue;
		// Always Round Down and use positive quantities (B/S are determined by Long/Short Filter)
		return Math.floor(Math.abs(quantity));
	},

	clearTrades: function() {
		const gp = this;
		gp.getStore().each(function(record) {
			this.clearRecordTradeQuantities(record);
		}, this);
		gp.getView().refresh();
	},

	clearRecordTradeQuantities: function(record) {
		record.beginEdit();
		record.data['callTradeQuantity'] = '';
		record.data['putTradeQuantity'] = '';
		record.endEdit();
	},

	alwaysSubmitFields: ['holdingAccount.id', 'clientAccount.id', 'callTrade.id', 'callTradeQuantity', 'putTrade.id', 'putTradeQuantity', 'callTailTrade.id', 'putTailTrade.id'],
	doNotSubmitFields: ['includeTrade', 'expiringPutQuantity', 'expiringCallQuantity', 'expiringPutTradedInstrumentQuantity', 'expiringCallTradedInstrumentQuantity', 'applyTargetExcess', 'putTrancheTradeAverage', 'callTrancheTradeAverage', 'cashBalance'],

	initComponent: function() {
		const cols = [];
		const tradeEntry = this.tradeEntrySupported;
		Ext.each(this.allColumns, function(f) {
			if (TCG.isBlank(f.tradeEntry) || TCG.isEquals(f.tradeEntry, tradeEntry)) {
				cols.push(f);
			}
		});
		this.columnsConfig = cols;
		TCG.grid.FormGridPanel.prototype.initComponent.call(this, arguments);
	},

	// Columns that have tradeEntry = true/false will be included based on if this is tradeEntry or tradeReview - anything without that property will always be included
	allColumns: [
		{
			width: 30, dataIndex: 'includeTrade', align: 'center', xtype: 'checkcolumn', sortable: false, filter: false, tradeEntry: true,
			menuDisabled: true,
			header: String.format('<div class="x-grid3-check-col{0}">&#160;</div>', ''),
			renderer: function(v, p, record) {
				p.css += ' x-grid3-check-col-td';
				if (TCG.isBlank(v)) {
					record.data['includeTrade'] = false;
					v = false;
				}
				return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
			}
		},
		{// Not used yet, so hiding this column for now
			header: '', width: 35, filter: false, sortable: false, dataIndex: 'warningMessage', hidden: true,
			renderer: function(v, metaData, r) {
				if (TCG.isNotBlank(v)) {
					const imgCls = 'flag-red';
					return '<span ext:qtip="' + v + '"><span style="width: 16px; height: 16px; float: left;" class="' + imgCls + '" ext:qtip="' + v + '">&nbsp;</span> </span>';
				}
				return '&nbsp;';
			}
		},
		{header: '', width: 25, filter: false, sortable: false, dataIndex: 'doNotRemoveExpiringOptionsFromExcessCalculation', type: 'boolean', hidden: true},
		{header: '', width: 25, filter: false, sortable: false, dataIndex: 'longPositions', type: 'boolean', hidden: true},
		{
			header: 'Client Account', width: 250, dataIndex: 'clientAccount.label',
			renderer: function(v, metaData, r) {
				metaData.attr = 'qtip=\'Right click to display the context menu with additional features.\'';
				return v;
			}
		},
		// Holding Account - Editable for Trade Entry, Read Only for Trade Review
		{
			header: 'Holding Account', width: 100, dataIndex: 'holdingAccount.number', idDataIndex: 'holdingAccount.id', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tradeEntry: true,
			tooltip: 'Holding Account to trade new Options under.',
			editor: {
				xtype: 'combo', displayField: 'number', url: 'investmentAccountListFind.json?ourAccount=false', tooltipField: 'label',
				beforequery: function(queryEvent) {
					// Reset Combo so re-queries each time (since each row has different results)
					this.clearAndReset();
					const editor = queryEvent.combo.gridEditor;
					const record = editor.record;
					const grid = editor.containerGrid;
					const fp = TCG.getParentFormPanel(grid);
					queryEvent.combo.store.baseParams = {
						mainAccountId: TCG.getValue('clientAccount.id', record.json),
						mainPurpose: 'Trading: Options',
						mainPurposeActiveOnDate: fp.getForm().findField('tradeDate').value
					};
				}
			},
			renderer: function(v, metaData, r) {
				if (r.json) {
					Clifton.product.overlay.trade.group.renderOptionsTooltip(metaData, r.json.putOptionsTooltipString, r.json.callOptionsTooltipString);
				}
				return v;
			}
		},
		{
			header: 'Holding Account', tradeEntry: false, width: 100, dataIndex: 'holdingAccount.number',
			renderer: function(v, metaData, r) {
				if (r.json) {
					Clifton.product.overlay.trade.group.renderOptionsTooltip(metaData, r.json.putOptionsTooltipString, r.json.callOptionsTooltipString);
				}
				return v;
			}
		},

		{
			header: 'Expiring Put', width: 85, dataIndex: 'expiringPutTradedInstrumentQuantity', type: 'float', summaryType: 'sum',
			tooltip: 'Quantity of put positions with Expiration Date equal to the selected expiration above (Note: The security must be included on the run), and for the same Underlying Instrument as selected instrument above.',
			renderer: function(v, metaData, r) {
				if (r.json) {
					Clifton.product.overlay.trade.group.renderOptionsTooltip(metaData, r.json.expiringPutTooltipString, '', true);
				}
				return v;
			}
		},

		{
			header: 'Expiring Call', width: 85, dataIndex: 'expiringCallTradedInstrumentQuantity', type: 'float', summaryType: 'sum',
			tooltip: 'Quantity of call positions with Expiration Date equal to the selected expiration above (Note: The security must be included on the run), and for the same Underlying Instrument as selected instrument above.',
			renderer: function(v, metaData, r) {
				if (r.json) {
					Clifton.product.overlay.trade.group.renderOptionsTooltip(metaData, r.json.expiringCallTooltipString, '', true);
				}
				return v;
			}

		},
		// Trade Entry
		{
			header: 'Put Trade', tradeEntry: true, width: 85, dataIndex: 'putTradeQuantity', type: 'int', useNull: true, editor: {xtype: 'spinnerfield', minValue: 0, allowBlank: true}, negativeInRed: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Quantity to trade for selected Put above for this account. Buy/Sell is based on Long/Short selection at the top of the screen.'
		},
		{
			header: 'Call Trade', tradeEntry: true, width: 85, dataIndex: 'callTradeQuantity', type: 'int', useNull: true, editor: {xtype: 'spinnerfield', minValue: 0, allowBlank: true}, negativeInRed: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Quantity to trade for selected Call above for this account. Buy/Sell is based on Long/Short selection at the top of the screen.'
		},
		// Trade Review
		{
			header: 'Put State', width: 70, tradeEntry: false, hidden: true, dataIndex: 'putTrade.workflowState.name', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			renderer: function(value, metaData, r) {
				if (value === 'Invalid') {
					metaData.css = 'ruleViolation';
				}
				return value;
			}
		},
		{
			header: 'Put Qty', width: 60, tradeEntry: false, dataIndex: 'putTradeQuantity', type: 'int', useNull: true, negativeInRed: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Quantity to trade for selected Put above for this account. Buy/Sell is based on Long/Short selection at the top of the screen.',
			renderer: function(value, metaData, record) {
				if (record.data['putTrade.workflowState.name'] === 'Invalid') {
					metaData.css = 'ruleViolation';
					metaData.attr = TCG.renderQtip('Trade failed compliance check(s) and is invalid');
				}
				return value;
			}
		},
		{
			header: 'Put Tail State', width: 85, tradeEntry: false, hidden: true, dataIndex: 'putTailTrade.workflowState.name', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			renderer: function(value, metaData, r) {
				if (value === 'Invalid') {
					metaData.css = 'ruleViolation';
				}
				return value;
			}
		},
		{
			header: 'Put Tail Qty', width: 80, tradeEntry: false, dataIndex: 'putTailTrade.quantityIntended', type: 'int', useNull: true, negativeInRed: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Tail quantity to trade for selected Put above for this account. Buy/Sell is based on Long/Short selection at the top of the screen.',
			renderer: function(value, metaData, record) {
				if (record.data['putTailTrade.workflowState.name'] === 'Invalid') {
					metaData.css = 'ruleViolation';
					metaData.attr = TCG.renderQtip('Trade failed compliance check(s) and is invalid');
				}
				return value;
			}
		},
		{
			header: 'Call State', width: 70, tradeEntry: false, hidden: true, dataIndex: 'callTrade.workflowState.name', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			renderer: function(value, metaData, r) {
				if (value === 'Invalid') {
					metaData.css = 'ruleViolation';
				}
				return value;
			}
		},
		{
			header: 'Call Qty', width: 60, tradeEntry: false, dataIndex: 'callTradeQuantity', type: 'int', useNull: true, negativeInRed: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Quantity to trade for selected Call above for this account. Buy/Sell is based on Long/Short selection at the top of the screen.',
			renderer: function(value, metaData, record) {
				if (record.data['callTrade.workflowState.name'] === 'Invalid') {
					metaData.css = 'ruleViolation';
					metaData.attr = TCG.renderQtip('Trade failed compliance check(s) and is invalid');
				}
				return value;
			}
		},
		{
			header: 'Call Tail State', width: 85, tradeEntry: false, hidden: true, dataIndex: 'callTailTrade.workflowState.name', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			renderer: function(value, metaData, r) {
				if (value === 'Invalid') {
					metaData.css = 'ruleViolation';
				}
				return value;
			}
		},
		{
			header: 'Call Tail Qty', width: 80, tradeEntry: false, dataIndex: 'callTailTrade.quantityIntended', type: 'int', useNull: true, negativeInRed: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Tail quantity to trade for selected Call above for this account. Buy/Sell is based on Long/Short selection at the top of the screen.',
			renderer: function(value, metaData, record) {
				if (record.data['callTailTrade.workflowState.name'] === 'Invalid') {
					metaData.css = 'ruleViolation';
					metaData.attr = TCG.renderQtip('Trade failed compliance check(s) and is invalid');
				}
				return value;
			}
		},
		// Calculated Fields Based On Trades
		{
			header: 'Put % Excess', width: 90, dataIndex: 'putExcess', type: 'currency', useNull: true, negativeInRed: true,
			tooltip: 'Calculation: 1 - (ABS(Put Options Exposure) / ABS(Matching Exposure))<br/>Note: Matching Exposure is configurable based on the account\'s calculator.  By default for Puts, this currently uses Cash and Cash Equivalents. Hover over each value to see the details that makes up the matching exposure for that account.',
			renderer: function(v, metaData, r) {
				let putExposure = Ext.isNumber(r.data['putTradeQuantity']) ? (r.data['putTradeQuantity'] * r.json.putContractValue) : 0;
				let putTailExposure = Ext.isNumber(r.data['putTailTrade.quantityIntended']) ? (r.data['putTailTrade.quantityIntended'] * r.json.putContractValue) : 0;
				if (TCG.isFalse(r.data['longPositions'])) {
					putExposure = putExposure * -1;
					putTailExposure = putTailExposure * -1;
				}
				let totalOptionExposure = r.json.putExposure;
				// If the trade already exists, then it's already included in the total option exposure, don't add it again.
				// So, if there isn't a trade workflow state name, then it doesn't exist and needs to be added to the exposure
				totalOptionExposure += TCG.isBlank(r.data['putTrade.workflowState.name']) ? putExposure : 0;
				totalOptionExposure += TCG.isBlank(r.data['putTailTrade.workflowState.name']) ? putTailExposure : 0;

				const doNotExcludeExpiring = r.data['doNotRemoveExpiringOptionsFromExcessCalculation'];
				if (doNotExcludeExpiring === true) {
					totalOptionExposure += r.json.expiringPutExposure;
				}

				metaData.attr = TCG.renderQtip(Clifton.product.overlay.trade.group.renderExcessExposureTooltip('Put', doNotExcludeExpiring, putExposure, putTailExposure, r));

				const additional = Ext.isNumber(r.json.putMatchingAdditionalExposure) ? r.json.putMatchingAdditionalExposure : 0;
				const matching = r.json.putMatchingExposure + additional;
				if (matching === 0) {
					return '&nbsp;';
				}
				const value = (1 - (Math.abs(totalOptionExposure) / Math.abs(matching))) * 100;
				return TCG.renderAmount(value, false, '0,000.00%');
			}
		},
		{
			header: 'Call % Excess', width: 95, dataIndex: 'callExcess', type: 'currency', useNull: true, negativeInRed: true,
			tooltip: 'Calculation: 1 - (ABS(Call Options Exposure) / ABS(Matching Exposure))<br/>Note: Matching Exposure is configurable based on the account\'s calculator. By default for Calls, this currently uses Equities. Hover over each value to see the details that makes up the matching exposure for that account.',
			renderer: function(v, metaData, r) {
				let callExposure = Ext.isNumber(r.data['callTradeQuantity']) ? (r.data['callTradeQuantity'] * r.json.callContractValue) : 0;
				let callTailExposure = Ext.isNumber(r.data['callTailTrade.quantityIntended']) ? (r.data['callTailTrade.quantityIntended'] * r.json.callContractValue) : 0;
				if (TCG.isFalse(r.data['longPositions'])) {
					callExposure = callExposure * -1;
					callTailExposure = callTailExposure * -1;
				}
				let totalOptionExposure = r.json.callExposure;
				// If the trade already exists, then it's already included in the total option exposure, don't add it again.
				// So, if there isn't a trade workflow state name, then it doesn't exist and needs to be added to the exposure
				totalOptionExposure += TCG.isBlank(r.data['callTrade.workflowState.name']) ? callExposure : 0;
				totalOptionExposure += TCG.isBlank(r.data['callTailTrade.workflowState.name']) ? callTailExposure : 0;

				const doNotExcludeExpiring = r.data['doNotRemoveExpiringOptionsFromExcessCalculation'];
				if (doNotExcludeExpiring === true) {
					totalOptionExposure += r.json.expiringCallExposure;
				}

				metaData.attr = TCG.renderQtip(Clifton.product.overlay.trade.group.renderExcessExposureTooltip('Call', doNotExcludeExpiring, callExposure, callTailExposure, r));

				const additional = Ext.isNumber(r.json.callMatchingAdditionalExposure) ? r.json.callMatchingAdditionalExposure : 0;
				const matching = r.json.callMatchingExposure + additional;
				if (matching === 0) {
					return '&nbsp;';
				}
				const value = (1 - (Math.abs(totalOptionExposure) / Math.abs(matching))) * 100;
				return TCG.renderAmount(value, false, '0,000.00%');
			}
		},
		{
			width: 30, tradeEntry: true, dataIndex: 'applyTargetExcess', align: 'center', xtype: 'checkcolumn', sortable: false, filter: false,
			tooltip: 'When checked and % Target Excess is entered and applied, it will apply to only the selected rows.',
			menuDisabled: true,
			header: String.format('<div class="x-grid3-check-col{0}">&#160;</div>', '-on'),
			renderer: function(v, p, record) {
				p.css += ' x-grid3-check-col-td';
				if (TCG.isBlank(v)) { // CHANGE: added to default to checked
					record.data['applyTargetExcess'] = true;
					v = true;
				}
				return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
			}
		},
		{
			header: 'Put Avg', width: 65, dataIndex: 'putTrancheTradeAverage', type: 'float', useNull: true, negativeInRed: true,
			tooltip: 'Average Put Trade quantities calculated as (Put Total Matching/Average Put Option Strike/Tranche Count/100). The number of tranches is defaulted and can be overridden in the client account\'s portfolio setup by setting the \'Option Tranche Count\' field.',
			renderer: function(v, p, record) {
				return (v) ? Math.round(v) : v;
			}
		},
		{
			header: 'Call Avg', width: 65, dataIndex: 'callTrancheTradeAverage', type: 'float', useNull: true, negativeInRed: true,
			tooltip: 'Average Call Trade quantities calculated as (Call Total Matching/Underlying Price/Tranche Count/100). The number of tranches is defaulted and can be overridden in the client account\'s portfolio setup by setting the \'Option Tranche Count\' field.',
			renderer: function(v, p, record) {
				return (v) ? Math.round(v) : v;
			}
		},
		{
			header: 'Cash %', width: 90, dataIndex: 'cashBalance', type: 'float', useNull: false, negativeInRed: true,
			tooltip: 'Estimated cash balance, including estimated payout of expiring positions and new option trades, as a percentage of updated total portfolio value.',
			renderer: function(value, metadata, record) {
				const cashPercentAndTooltip = Clifton.product.overlay.trade.group.calculateCashImpact(record);
				metadata.attr = TCG.renderQtip(cashPercentAndTooltip[1]);
				// Anything greater than 10 bps color in green
				return TCG.renderAmount(cashPercentAndTooltip[0], TCG.isTrue(cashPercentAndTooltip[0] > 0.10), '0,000.0000 %');
			}
		}
	],


	plugins: {
		ptype: 'gridsummary'
	},

	// Defaults to Checked
	allRows: true,
	toggleAll: function(columnName) {
		this.allRows = !TCG.isTrue(this.allRows);
		const grid = this;
		const columnIndex = grid.getColumnModel().findColumnIndex(columnName);
		grid.getColumnModel().setColumnHeader(columnIndex, String.format('<div class="x-grid3-check-col{0}">&#160;</div>', this.allRows ? '-on' : ''));
		grid.getStore().each(function(record) {
			record.set(columnName, this.allRows);
		}, grid);
		grid.markModified();
	},

	// Include records for dirty checking when tradeEntrySupported is false or the record is selected
	isRecordApplicableForDirtyChecking: function(record) {
		return (TCG.isFalse(this.tradeEntrySupported) || TCG.isTrue(record.get('includeTrade')));
	}
});
Ext.reg('product-run-trade-group-option-roll-form-grid', Clifton.product.overlay.trade.group.option.RunTradeGroupDynamicOptionRollDetailFormGrid);

Clifton.product.overlay.trade.group.option.RunTradeGroupOtcOptionWindow = Ext.extend(TCG.app.OKCancelWindow, {
	iconCls: 'add',
	height: 500,
	width: 800,
	modal: false, // not modal so new Options can be created from template
	okButtonTooltip: 'Creates a new Option Roll Trade Group using the existing group as a template and containing the specified Client Account\'s Trades. The Trades will be updated to include the defined OTC security details.',

	reloadOpener: function() {
		const win = this;
		if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reloadGrid) {
			win.openerCt.reloadGrid();
		}
	},

	getForms: function() {
		return this.items;
	},

	hasData: function(fieldNameArray) {
		if (fieldNameArray && fieldNameArray.length > 0) {
			const form = this.getForm();
			for (let i = 0; i < fieldNameArray.length; i++) {
				const field = form.findField(fieldNameArray[i]);
				if (field) {
					const value = field.getValue();
					if (TCG.isNotBLank(value)) {
						return true;
					}
				}
			}
		}
		return false;
	},

	saveWindow: function() {
		const forms = this.getForms();
		const panel = forms.get(0);
		const form = panel.getForm();
		if (form.isDirty()) {
			if (this.openerCt) {
				// Close of window will happen after success of the save by the panel.
				// The panel attempts to validate the trades.
				this.saveForm(false, forms, form, panel, panel.getSubmitParams());
			}
			else {
				TCG.showError('Opener component must be defined.', 'Parent Window Missing');
			}
		}
		else {
			this.close();
		}
	},

	title: 'Option Roll: Convert To OTC',
	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'This window will help specify or create OTC Put and Call Options for a Client Account. Specify the Counterparty, ISDA, Holding Account, ' +
				'and OTC Instrument in order to select existing Options or create new ones from a template. Clicking OK will create a new Option Roll trade group ' +
				'and update the Client Account\'s trades to reflect the OTC Options. If the Client Account being converted to OTC was the only row in the existing ' +
				'Option Roll trade group, the existing Option Roll will be updated instead of creating a new one.',
			labelWidth: 100,
			dtoClassForBinding: 'com.clifton.product.overlay.trade.group.dynamic.option.ProductOverlayRunTradeGroupDynamicOptionRoll',
			actionUrl: 'tradeGroupDynamicActionExecute.json',
			saveUrl: 'tradeGroupDynamicEntrySave.json',

			getSaveURL: function() {
				const formValues = this.getForm().formValues;
				const url = TCG.isTrue(formValues.updateCurrentGroup) ? this.actionUrl : this.saveUrl;
				return this.appendBindingParametersToUrl(url);
			},

			appendBindingParametersToUrl: function(url) {
				url = url + ((url.indexOf('?') > 0) ? '&' : '?');
				url = url + 'requestedMaxDepth=5&requestedPropertiesToExcludeGlobally=tradeList&enableValidatingBinding=true&dtoClassForBinding=' + this.dtoClassForBinding;
				return url;
			},

			listeners: {
				afterload: function(panel) {
					// After successful OTC execution, validate the trades.
					const form = panel.getForm();
					const formValues = form.formValues;
					const params = Ext.applyIf({
						id: formValues.id,
						tradeGroupAction: 'VALIDATE'
					}, Ext.applyIf(panel.getSubmitParams(), form.getValues()));

					TCG.data.getDataPromise(panel.appendBindingParametersToUrl(this.actionUrl), panel, {
						params: params,
						waitMsg: 'Validating Trades...',
						waitTarget: panel,
						timeout: panel.saveTimeout,
						onLoad: function() {
							panel.getWindow().closeWindow();
						},
						onFailure: function() {
							panel.getWindow().closeWindow();
						}
					});
				}
			},

			getDefaultData: async function(win) {
				const dd = Ext.apply(win.params || {}, {});
				let listedPutSecurityId,
					listedCallSecurityId;
				if (dd.putSecurity) {
					listedPutSecurityId = dd.putSecurity.id;
					if (!dd.callSecurity) {
						dd.callSecurity = {
							lastDeliveryDate: dd.putSecurity.lastDeliveryDate
						};
					}
					// Hide call OTC fields if no call trade to avoid unnecessarily creating OTC security
					if (TCG.isBlank(TCG.getValue('id', dd.detailList[0].callTrade))) {
						this.form.findField('callOtcSecurity.label').hide();
						this.find('itemId', 'createCallSecurity')[0].hide();
					}
				}
				if (dd.callSecurity) {
					listedCallSecurityId = dd.callSecurity.id;
					// Hide put OTC fields if no put trade to avoid unnecessarily creating OTC security
					if (TCG.isBlank(TCG.getValue('id', dd.detailList[0].putTrade))) {
						// hide put fields
						this.form.findField('putOtcSecurity.label').hide();
						this.find('itemId', 'createPutSecurity')[0].hide();
					}
				}
				const [clientAccount, investmentGroup, putSecurity, callSecurity] = await Promise.all([
					TCG.data.getDataPromise('investmentAccount.json?id=' + dd.detailList[0].clientAccount.id, this),
					TCG.data.getDataPromise('investmentGroupByName.json?requestedPropertiesRoot=data&requestedProperties=id&name=Option Roll: ' + dd.clientAccountGroup.name, this),
					TCG.isBlank(listedPutSecurityId) ? undefined : TCG.data.getDataPromise('investmentSecurity.json?id=' + listedPutSecurityId, this),
					TCG.isBlank(listedCallSecurityId) ? undefined : TCG.data.getDataPromise('investmentSecurity.json?id=' + listedCallSecurityId, this)
				]);
				dd.clientAccount = clientAccount;
				if (investmentGroup) {
					dd.investmentGroupId = investmentGroup.id;
				}
				if (putSecurity) {
					dd.putStrikePrice = putSecurity.optionStrikePrice;
				}
				if (callSecurity) {
					dd.callStrikePrice = callSecurity.optionStrikePrice;
				}
				return dd;
			},

			getSubmitParams: function() {
				const form = this.getForm();
				const formValues = form.formValues;

				const holdingAccountId = form.findField('holdingAccount.id').getValue();
				const detailList = {
					id: -10,
					class: this.getWindow().openerCt.dtoClass,
					'clientAccount.id': formValues.detailList[0].clientAccount.id,
					'holdingAccount.id': holdingAccountId
				};
				const putTrade = formValues.detailList[0].putTrade,
					callTrade = formValues.detailList[0].callTrade;
				if (putTrade) {
					detailList['putTrade.id'] = putTrade.id;
				}
				if (callTrade) {
					detailList['callTrade.id'] = callTrade.id;
				}
				const params = {
					'optionInstrument.id': form.findField('optionOtcInstrument.id').getValue(),
					doNotRemoveExpiringOptionsFromExcessCalculation: formValues.doNotRemoveExpiringOptionsFromExcessCalculation,
					longPositions: formValues.longPositions,
					marketOnClose: formValues.marketOnClose,
					'executingBrokerCompany.id': form.findField('businessCompany.id').getValue(),
					detailList: JSON.stringify([detailList])
				};

				if (form.findField('callOtcSecurity.id').isVisible() && callTrade) {
					params['callSecurity.id'] = form.findField('callOtcSecurity.id').getValue();
				}
				if (form.findField('putOtcSecurity.id').isVisible() && putTrade) {
					params['putSecurity.id'] = form.findField('putOtcSecurity.id').getValue();
				}

				if (TCG.isTrue(formValues.updateCurrentGroup)) {
					// Execute Trade Group Action to update the Trades
					params.id = formValues.id;
					params.tradeGroupAction = formValues.tradeGroupAction ? formValues.tradeGroupAction : 'SAVE_TRADE';
				}
				return params;
			},

			createNewOtcSecurity: function(isPut) {
				const form = this.getForm();
				let strike, style, securityToCopy, settlement;
				if (isPut) {
					strike = form.findField('putStrikePrice').getValue();
					style = form.findField('putOptionStyle').getValue();
					settlement = form.findField('putSettlementTime').getValue();
					securityToCopy = form.findField('putSecurity.id').getValue();
				}
				else {
					strike = form.findField('callStrikePrice').getValue();
					style = form.findField('callOptionStyle').getValue();
					settlement = form.findField('callSettlementTime').getValue();
					securityToCopy = form.findField('callSecurity.id').getValue();
				}
				const url = 'investmentSecurityOptionWithCommandSave.json';
				const params = {
					'securityToCopy.id': securityToCopy,
					strikePrice: strike,
					optionStyle: style,
					settlementTime: settlement,
					'clientAccount.id': form.findField('clientAccount.id').getValue(),
					'counterparty.id': form.findField('businessCompany.id').getValue(),
					'isda.id': form.findField('businessContract.id').getValue(),
					'instrument.id': form.findField('optionOtcInstrument.id').getValue(),
					returnExisting: true
				};
				TCG.data.getDataPromise(url, this, {params: params})
					.then(function(otcSecurity) {
						if (otcSecurity) {
							let otcSecurityLabelField, otcSecurityIdField;
							if (isPut) {
								otcSecurityLabelField = form.findField('putOtcSecurity.label');
								otcSecurityIdField = form.findField('putOtcSecurity.id');
							}
							else {
								otcSecurityLabelField = form.findField('callOtcSecurity.label');
								otcSecurityIdField = form.findField('callOtcSecurity.id');
							}
							otcSecurityLabelField.setValue(otcSecurity.label);
							otcSecurityIdField.setValue(otcSecurity.id);
						}
					});
			},

			items: [
				{
					xtype: 'panel',
					layout: 'form',
					defaults: {anchor: '-20'},
					items: [
						// Fields for submitting new Option Roll
						{xtype: 'hidden', name: 'clientAccountGroup.id'},
						{xtype: 'datefield', name: 'expiringOptionsDate', hidden: true},
						{xtype: 'datefield', name: 'balanceDate', hidden: true},
						{xtype: 'hidden', name: 'tradeGroupType.id'},
						{xtype: 'hidden', name: 'traderUser.id'},
						{xtype: 'hidden', name: 'tradeDestination.id'},

						{xtype: 'hidden', name: 'investmentGroupId', submitValue: false},
						{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', hidden: true},
						{fieldLabel: 'Client Account', name: 'clientAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'clientAccount.id', submitDetailField: false},
						{xtype: 'hidden', name: 'clientAccount.businessClient.company.id', submitValue: false},
						{
							fieldLabel: 'Counterparty', name: 'businessCompany.name', hiddenName: 'businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourCompany=false', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true,
							allowBlank: false, submitValue: false, requiredFields: ['clientAccount.businessClient.company.id'],
							beforequery: function(queryEvent) {
								const fp = queryEvent.combo.getParentForm();

								queryEvent.combo.store.baseParams = {
									contractCompanyId: fp.getFormValue('clientAccount.businessClient.company.id', true),
									contractTypeName: 'ISDA',
									contractPartyRoleName: 'Counterparty'
								};
							},
							listeners: {
								select: function(combo, record, index) {
									// On Select - Attempt to Auto Load the correct ISDA
									const fp = combo.getParentForm();
									const params = {
										companyId: fp.getFormValue('clientAccount.businessClient.company.id', true),
										contractTypeName: 'ISDA',
										active: true,
										partyCompanyId: record.json.id
									};

									// Counterparty Role
									const role = TCG.data.getData('businessContractPartyRoleByName.json?name=Counterparty', combo, 'business.contract.party.role.Counterparty');
									params.partyRoleId = role.id;

									TCG.data.getDataPromise('businessContractListFind.json', combo, {params: params})
										.then(function(contracts) {
											const l = contracts.length || 0;
											if (l === 0) {
												TCG.showError('Cannot find any ISDA Contracts that match selected Client and Counterparty.', 'Invalid Setup');
											}
											else if (l === 1) {
												const isda = contracts[0];
												fp.setFormValue('businessContract.id', {value: isda.id, text: isda.label}, true);
											}
										});
									TCG.data.getDataPromise('investmentAccountListFind.json?ourAccount=false', combo, {
										params: {
											mainAccountId: fp.getFormValue('clientAccount.id', true),
											mainPurpose: 'Trading: Options',
											mainPurposeActiveOnDate: fp.getForm().findField('tradeDate').value,
											accountType: 'OTC ISDA',
											issuingCompanyId: record.json.id
										}
									})
										.then(function(holdingAccounts) {
											const count = holdingAccounts.length || 0;
											if (count === 1) {
												fp.setFormValue('holdingAccount.id', {value: holdingAccounts[0].id, text: holdingAccounts[0].number}, true);
											}
										});
								}
							}
						},
						{
							fieldLabel: 'ISDA', name: 'businessContract.label', hiddenName: 'businessContract.id', xtype: 'combo', url: 'businessContractListFind.json', displayField: 'label', tooltipField: 'label', detailPageClass: 'Clifton.business.contract.ContractWindow', disableAddNewItem: true,
							allowBlank: false, submitValue: false, requiredFields: ['businessCompany.id'],
							beforequery: function(queryEvent) {
								const fp = queryEvent.combo.getParentForm();
								const params = {
									companyId: fp.getFormValue('clientAccount.businessClient.company.id', true),
									contractTypeName: 'ISDA',
									active: true,
									partyCompanyId: fp.getFormValue('businessCompany.id', true)
								};

								// Counterparty Role
								const role = TCG.data.getData('businessContractPartyRoleByName.json?name=Counterparty', queryEvent.combo, 'business.contract.party.role.Counterparty');
								params.partyRoleId = role.id;

								queryEvent.combo.store.baseParams = params;
							}
						},
						{
							fieldLabel: 'Holding Account', name: 'holdingAccount.number', hiddenName: 'holdingAccount.id', xtype: 'combo', displayField: 'number', url: 'investmentAccountListFind.json?ourAccount=false', tooltipField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow',
							allowBlank: false, submitValue: false, requiredFields: ['businessCompany.id'],
							beforequery: function(queryEvent) {
								const fp = queryEvent.combo.getParentForm();
								queryEvent.combo.store.baseParams = {
									mainAccountId: fp.getFormValue('clientAccount.id', true),
									mainPurpose: 'Trading: Options',
									mainPurposeActiveOnDate: fp.getForm().findField('tradeDate').value,
									accountType: 'OTC ISDA',
									issuingCompanyId: fp.getFormValue('businessCompany.id', true)
								};
							}
						},
						{xtype: 'label', html: '<hr/>'},
						{name: 'callSecurity.lastDeliveryDate', xtype: 'datefield', submitValue: false, hidden: true},
						{
							fieldLabel: 'Instrument', name: 'optionOtcInstrument.label', hiddenName: 'optionOtcInstrument.id', xtype: 'combo', displayField: 'label', url: 'investmentInstrumentListFind.json?investmentType=Options', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', minListWidth: 550, flex: 1,
							allowBlank: false, submitValue: false,
							qtip: 'The instrument is filtered to those using an OTC hierarchy, the same investment type and investment type sub-type as the original option instrument, and instrument group name of the format \'Option Roll: Client Account Group Name\' (Example: Option Roll: SPX-20D).',
							listeners: {
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									const instrumentGroupId = f.findField('investmentGroupId').getValue();

									// If blank, clears the filter
									qEvent.combo.store.baseParams = {
										investmentGroupId: instrumentGroupId,
										investmentTypeId: TCG.getValue('optionInstrument.hierarchy.investmentType.id', f.formValues),
										investmentTypeSubTypeId: TCG.getValue('optionInstrument.hierarchy.investmentTypeSubType.id', f.formValues),
										otc: true
									};
								}
							}
						}
					]
				},
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {
						layout: 'form',
						columnWidth: .50,
						defaults: {anchor: '-20'}
					},
					items: [
						{
							items: [
								{xtype: 'sectionheaderfield', header: 'Put OTC Option Info'},
								{
									fieldLabel: 'Put', name: 'putOtcSecurity.label', hiddenName: 'putOtcSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
									allowBlank: true, submitValue: false,
									requiredFields: ['optionOtcInstrument.id', 'callSecurity.lastDeliveryDate', 'businessCompany.id'],
									listeners: {
										beforeQuery: function(qEvent) {
											const f = qEvent.combo.getParentForm().getForm();
											const store = qEvent.combo.store;
											store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
											store.setBaseParam('instrumentId', f.findField('optionOtcInstrument.id').getValue());
											store.setBaseParam('lastDeliveryDate', f.findField('callSecurity.lastDeliveryDate').value);
											store.setBaseParam('businessCompanyId', f.findField('businessCompany.id').getValue());
											store.setBaseParam('businessContractId', f.findField('businessContract.id').getValue());
											store.setBaseParam('optionType', 'PUT');
											store.setBaseParam('otc', true);
										}
									},
									getDetailPageClass: function(newInstance) {
										if (newInstance) {
											if (!this.getParentForm().getForm().findField('optionOtcInstrument.id').value) {
												TCG.showError('You must first select Instrument for the option.', 'New Option');
												return false;
											}
											return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
										}
										return this.detailPageClass;
									},
									getDefaultData: function(f) { // defaults client/counterparty/isda
										const fp = TCG.getParentFormPanel(this);
										const form = fp.getForm();
										const investmentInstrumentId = form.findField('optionOtcInstrument.id').value;
										const secId = form.findField('putSecurity.id').value;
										return {
											instrument: TCG.data.getData('investmentInstrument.json?id=' + investmentInstrumentId, this),
											securityToCopy: {id: secId},
											optionType: 'PUT',
											strikePrice: form.findField('putStrikePrice').getValue()
										};
									}
								},
								{
									xtype: 'fieldset', title: 'Create New Put OTC Option', itemId: 'createPutSecurity', collapsed: true,
									items: [
										{fieldLabel: 'Option To Copy', name: 'putSecurity.label', detailIdField: 'putSecurity.id', submitDetailField: false, xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
										{fieldLabel: 'Option Style', name: 'putOptionStyle', xtype: 'system-list-combo', listName: 'Investment Option Style', allowBlank: false, value: {value: 'EUROPEAN', text: 'European'}, submitValue: false},
										{fieldLabel: 'Expiry Time', name: 'putSettlementTime', xtype: 'system-list-combo', listName: 'Investment Expiry Time', allowBlank: false, value: {value: 'PM', text: 'PM Settlement'}, submitValue: false},
										{fieldLabel: 'Strike Price', name: 'putStrikePrice', xtype: 'floatfield', submitValue: false},
										{
											fieldLabel: ' ', labelSeparator: '', text: 'Create', xtype: 'button', iconCls: 'add',
											tooltip: 'Creates a new OTC Option using the above Option as a template and the specified custom values. The Call field will be updated to include the newly created or existing Option.',
											handler: function(button) {
												const fp = TCG.getParentFormPanel(button);
												fp.createNewOtcSecurity(true);
											}
										}
									]
								}
							]
						},
						{
							items: [
								{xtype: 'sectionheaderfield', header: 'Call OTC Option Info'},
								{
									fieldLabel: 'Call', name: 'callOtcSecurity.label', hiddenName: 'callOtcSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
									allowBlank: true, submitValue: false,
									requiredFields: ['optionOtcInstrument.id', 'callSecurity.lastDeliveryDate', 'businessCompany.id'],
									listeners: {
										beforeQuery: function(qEvent) {
											const f = qEvent.combo.getParentForm().getForm();
											const store = qEvent.combo.store;
											store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
											store.setBaseParam('instrumentId', f.findField('optionOtcInstrument.id').getValue());
											store.setBaseParam('lastDeliveryDate', f.findField('callSecurity.lastDeliveryDate').value);
											store.setBaseParam('businessCompanyId', f.findField('businessCompany.id').getValue());
											store.setBaseParam('businessContractId', f.findField('businessContract.id').getValue());
											store.setBaseParam('optionType', 'CALL');
											store.setBaseParam('otc', true);
										}
									},
									getDetailPageClass: function(newInstance) {
										if (newInstance) {
											if (!this.getParentForm().getForm().findField('optionOtcInstrument.id').value) {
												TCG.showError('You must first select Instrument for the option.', 'New Option');
												return false;
											}
											return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
										}
										return this.detailPageClass;
									},
									getDefaultData: function(f) { // defaults client/counterparty/isda
										const fp = TCG.getParentFormPanel(this);
										const form = fp.getForm();
										const investmentInstrumentId = form.findField('optionOtcInstrument.id').value;
										const secId = form.findField('callSecurity.id').value;
										return {
											instrument: TCG.data.getData('investmentInstrument.json?id=' + investmentInstrumentId, this),
											securityToCopy: {id: secId},
											optionType: 'CALL',
											strikePrice: form.findField('callStrikePrice').getValue()
										};
									}
								},
								{
									xtype: 'fieldset', title: 'Create New Call OTC Option', itemId: 'createCallSecurity', collapsed: true,
									items: [
										{fieldLabel: 'Option To Copy', name: 'callSecurity.label', detailIdField: 'callSecurity.id', submitDetailField: false, xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
										{fieldLabel: 'Option Style', name: 'callOptionStyle', xtype: 'system-list-combo', listName: 'Investment Option Style', allowBlank: false, value: {value: 'EUROPEAN', text: 'European'}, submitValue: false},
										{fieldLabel: 'Expiry Time', name: 'callSettlementTime', xtype: 'system-list-combo', listName: 'Investment Expiry Time', allowBlank: false, value: {value: 'PM', text: 'PM Settlement'}, submitValue: false},
										{fieldLabel: 'Strike Price', name: 'callStrikePrice', xtype: 'floatfield', submitValue: false},
										{
											fieldLabel: ' ', labelSeparator: '', text: 'Create', xtype: 'button', iconCls: 'add',
											tooltip: 'Creates a new OTC Option using the above Option as a template and the specified custom values. The Call field will be updated to include the newly created or existing Option.',
											handler: function(button) {
												const fp = TCG.getParentFormPanel(button);
												fp.createNewOtcSecurity(false);
											}
										}
									]
								}
							]
						}
					]
				}
			]
		}
	]
});
