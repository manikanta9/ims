TCG.use('Clifton.portfolio.run.trade.BaseRunTradeWindow');

TCG.use('Clifton.portfolio.run.trade.BaseRunTradeForm');

//BOTTOM - REPLICATION LIST FORM GRID
TCG.use('Clifton.product.overlay.trade.group.option.RunTradeOptionCashFlowReplicationGrid');

TCG.use('Clifton.product.overlay.RunManagerBalance');

/***********************************************************************
 * FIELD SETS
 * *********************************************************************/

Clifton.product.overlay.trade.group.option.RunTradeOptionCashFlowSummaryFieldSet = {
	xtype: 'fieldset',
	title: 'Position Summary',
	listeners: {
		collapse: function(p) {
			p.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		},
		expand: function(p) {
			p.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		}
	},
	items: [{
		xtype: 'formtable',
		columns: 3,
		padding: '3',
		defaults: {
			xtype: 'currencyfield',
			decimalPrecision: 2,
			readOnly: true,
			submitValue: false,
			width: '95%'
		},
		items: [
			{cellWidth: '100px', xtype: 'label'},
			{html: 'Market Value', xtype: 'label', style: 'text-align: right'},
			{html: '% NAV', qtip: '% NAV', xtype: 'label', style: 'text-align: right'},

			{html: 'Options:', xtype: 'label', qtip: 'Total Market Value for all Options contracts (PUTs + CALLs)'},
			{name: 'optionsMarketValue', decimalPrecision: 0},
			{name: 'optionsMarketValuePercent'},

			{html: 'Equity:', xtype: 'label', qtip: 'Total Equity Exposure (Equity + Managed Equity)'},
			{name: 'callMatchingTotalExposure', decimalPrecision: 0},
			{name: 'callMatchingTotalExposurePercent'},

			{html: 'Cash Equivalents:', xtype: 'label', qtip: 'PUT Matching Exposure (e.g. Cash Equivalents)'},
			{name: 'putMatchingExposure', decimalPrecision: 0},
			{name: 'putMatchingExposurePercent'},
			{html: 'Cash:', xtype: 'label', qtip: 'PUT Matching Additional Exposure (e.g. Cash)'},
			{name: 'putMatchingAdditionalExposure', decimalPrecision: 0},
			{name: 'putMatchingAdditionalExposurePercent'},

			{html: '<hr/>', xtype: 'label', colspan: 3, width: '99%'},

			{html: 'Total:', xtype: 'label'},
			{name: 'portfolioTotal', decimalPrecision: 0},
			{name: 'portfolioTotalPercent'}
		]
	}]
};

Clifton.product.overlay.trade.group.option.RunTradeOptionPortfolioBetaSummaryFieldSet = {
	xtype: 'fieldset',
	title: 'Portfolio Beta Summary',
	listeners: {
		collapse: function(p) {
			p.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		},
		expand: function(p) {
			p.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		}
	},
	items: [{
		xtype: 'formtable',
		columns: 5,
		padding: '3',
		defaults: {
			xtype: 'currencyfield',
			decimalPrecision: 2,
			readOnly: true,
			submitValue: false,
			width: '95%'
		},
		items: [
			{cellWidth: '50px', xtype: 'label'},
			{html: 'Exposure Current', qtip: 'Exposure calculated using Current Contracts only.', xtype: 'label', style: 'text-align: right'},
			{html: '% Current', qtip: '% NAV of Current Exposure', xtype: 'label', style: 'text-align: right'},
			{html: 'Exposure New', qtip: 'Exposure calculated using (Current + Pending + (Buy - Sell)) Contracts.', xtype: 'label', style: 'text-align: right'},
			{html: '% New', qtip: '% NAV for New Exposure', xtype: 'label', style: 'text-align: right'},

			{html: 'Equity:', xtype: 'label', qtip: 'Total Equity exposure (Equity + Managed Equity)'},
			{name: 'callMatchingCurrentExposure', decimalPrecision: 0},
			{name: 'callMatchingCurrentExposurePercent'},
			{name: 'callMatchingExposure', decimalPrecision: 0},
			{name: 'callMatchingExposurePercent'},

			{html: 'Option:', xtype: 'label', qtip: 'Options Exposure calculated as (CALL Options Exposure - PUT Options Exposure)'},
			{name: 'optionsCurrentExposure', decimalPrecision: 0},
			{name: 'optionsCurrentExposurePercent'},
			{name: 'optionsExposure', decimalPrecision: 0},
			{name: 'optionsExposurePercent'},

			{html: '<hr/>', xtype: 'label', colspan: 5, width: '99%'},

			{html: 'Total:', xtype: 'label'},
			{name: 'equityOptionsOffsetCurrentExposure', decimalPrecision: 0},
			{name: 'equityOptionsOffsetCurrentExposurePercent'},
			{name: 'equityOptionsOffsetExposure', decimalPrecision: 0},
			{name: 'equityOptionsOffsetExposurePercent'}
		]
	}]
};

Clifton.product.overlay.trade.group.option.RunTradeOptionOptionExcessSummaryFieldSet = {
	xtype: 'fieldset',
	title: 'Option Excess Summary',
	listeners: {
		collapse: function(p) {
			p.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		},
		expand: function(p) {
			p.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		}
	},
	items: [{
		xtype: 'formtable',
		columns: 5,
		padding: '3',
		defaults: {
			xtype: 'currencyfield',
			decimalPrecision: 2,
			readOnly: true,
			submitValue: false,
			width: '95%'
		},
		items: [
			{cellWidth: '40px', xtype: 'label'},
			{html: 'Notional', xtype: 'label', style: 'text-align: right'},
			{html: 'Collateral', xtype: 'label', style: 'text-align: right'},
			{html: 'Excess', xtype: 'label', style: 'text-align: right', qtip: 'Difference in Notional and Collateral'},
			{html: '% Excess', qtip: '% Excess', xtype: 'label', style: 'text-align: right'},

			{html: 'Put:', xtype: 'label'},
			{name: 'putNotional', decimalPrecision: 0, qtip: 'Total Notional for all PUT Option Contracts.'},
			{name: 'cashCollateralExposure', decimalPrecision: 0, qtip: 'Total Market Value for Cash (Cash Equivalents + Cash + Managed Cash).'},
			{name: 'putExcess', decimalPrecision: 0},
			{name: 'putExcessCollateralPercent'},

			{html: 'Call:', xtype: 'label'},
			{name: 'callNotional', decimalPrecision: 0, qtip: 'Total Notional for all CALL Option Contracts.'},
			{name: 'equityCollateralExposure', decimalPrecision: 0, qtip: 'Total Market Value for Equity  (Equity + Managed Equity).'},
			{name: 'callExcess', decimalPrecision: 0},
			{name: 'callExcessCollateralPercent'},

			{html: '<hr/>', xtype: 'label', colspan: 5, width: '99%'},

			{html: 'Total:', xtype: 'label'},
			{name: 'optionNotionalTotal', decimalPrecision: 0},
			{name: 'collateralTotal', decimalPrecision: 0},
			{name: 'optionExcessTotal', decimalPrecision: 0},
			{name: 'optionExcessTotalPercent'}
		]
	}]
};

/***********************************************************************
 * Option Cash Flow Run Trade Window
 * *********************************************************************/

Clifton.product.overlay.trade.group.option.RunTradeOptionCashFlowWindow = Ext.extend(Clifton.portfolio.run.trade.BaseRunTradeWindow, {
	dtoClassForBinding: 'com.clifton.product.overlay.trade.group.dynamic.option.ProductOverlayRunOptionTradeGroup',

	additionalRunTabs: [
		Clifton.product.overlay.RunManagerBalance
	],

	runTradeEntryForm: {
		xtype: 'portfolio-run-trade-form',

		url: 'productOverlayRunOptionTradeGroup.json?requestedMaxDepth=7&requestedPropertiesToExcludeGlobally=tradeReplicationHolder',
		listRequestedPropertiesToExclude: 'data.optionReplicationList|data.matchingReplicationList',
		listRequestedPropertiesRoot: 'data.detailList',
		listRequestedProperties: 'id|overlayAssetClass.id|overlayAssetClass.accountAssetClass.id|labelLong|tradeEntryDisabled|securityNotTradable|cashExposure|tradingClientAccount.id|overlayAssetClass.accountAssetClass.assetClass.cash|overlayAssetClass.label|overlayAssetClass.effectiveDuration|overlayAssetClass.rebalanceTriggerWarningsUsed|overlayAssetClass.rebalanceTriggerMin|overlayAssetClass.rebalanceTriggerAbsoluteMin|overlayAssetClass.actualAllocationAdjusted|replication.name|replication.name|replicationType.currency|replicationType.deltaSupported|replicationType.name|security.id|security.symbol|security.endDate|security.description|securityPrice|tradeSecurityPrice|tradeSecurityPriceDate|underlyingSecurityPrice|tradeUnderlyingSecurityPrice|tradeUnderlyingSecurityPriceDate|securityDirtyPrice|tradeSecurityDirtyPrice|tradeSecurityDirtyPriceDate|exchangeRate|tradeExchangeRate|tradeExchangeRateDate|value|tradeValue|additionalExposure|additionalOverlayExposure|totalAdditionalExposure|targetExposureAdjusted|actualContracts|actualContractsAdjusted|virtualContracts|currentContracts|currentVirtualContracts|currentContractsAdjusted|pendingContracts|pendingVirtualContracts|pendingContractsAdjusted|buyContracts|sellContracts|trade.tradeDestination.id|trade.tradeDestination.name|trade.executingBrokerCompany.id|trade.executingBrokerCompany.label|trade.holdingInvestmentAccount.id|trade.holdingInvestmentAccount.label|delta|tradeDelta|contractValuePriceField|contractValuePrice|security.priceMultiplier|markToMarketAdjustmentValue|targetExposureAdjustedWithMarkToMarket|currencyExchangeRate|tradeCurrencyExchangeRate|currencyPendingLocalAmount|currencyPendingOtherBaseAmount|matchingReplication|currencyDenominationBaseAmount|currencyCurrentDenominationBaseAmount|currencyPendingDenominationBaseAmount|allocationWeightAdjusted|reverseExposureSign|rebalanceTriggerMin|rebalanceTriggerMax|security.instrument.hierarchy.investmentType.name',

		requestedMaxDepth: 7,

		rebalanceWarnings: [],
		assetClassFinalExposure: [],
		getConfirmBeforeSaveMsg: function() {
			const f = this.getForm();

			let warning = f.findField('overlayLimitWarning').getValue();
			if (TCG.isNotBlank(warning)) {
				warning = warning + '<br><br>';
			}

			for (let i = 0; i < this.rebalanceWarnings.length; i++) {
				const acWarn = this.rebalanceWarnings[i];
				if (TCG.isNotBlank(acWarn.msg)) {
					warning += '<b>' + acWarn.name + '</b> ' + acWarn.msg + '<br><br>';
				}
			}
			warning += this.confirmBeforeSaveMsg;
			return warning;
		},

		getSaveURL: function() {
			// Use parent URL, but exclude the trade command in the response. Command objects are always
			// rendered in the JSON response. The Option Cash Flow Run Trade may have transient replications
			// for the options created by an Option Roll, and they will not be processed correctly by the
			// response handler since they do not exist in the DB and not fully populated.
			return 'portfolioRunTradeCreationProcess.json?requestedPropertiesToExclude=portfolioRunTrade';
		},

		items: [
			// Need to add id as a hidden field and submitValue = false so that id is not double submitted
			{name: 'id', xtype: 'hidden', submitValue: false},
			{name: 'tradeGroupType.name', xtype: 'hidden'},
			{name: 'balanceDate', xtype: 'datefield', hidden: true},
			{
				xtype: 'panel',
				layout: 'column',
				items: [
					{
						columnWidth: .70,
						layout: 'form',
						items: [
							{fieldLabel: 'Portfolio Run', name: 'label', xtype: 'linkfield', detailIdField: 'id', detailPageClass: 'Clifton.portfolio.run.RunWindow'},
							{
								xtype: 'panel',
								layout: 'column',
								items: [
									{
										columnWidth: .50,
										items: [
											Clifton.product.overlay.trade.group.option.RunTradeOptionPortfolioBetaSummaryFieldSet
										]
									},
									{columnWidth: .02, items: [{html: '&nbsp;'}]},
									{
										columnWidth: .48,
										items: [
											Clifton.product.overlay.trade.group.option.RunTradeOptionOptionExcessSummaryFieldSet
										]
									}
								]
							}
						]
					},
					{columnWidth: .02, items: [{html: '&nbsp;'}]},
					{
						columnWidth: .28,
						items: [
							Clifton.product.overlay.trade.group.option.RunTradeOptionCashFlowSummaryFieldSet
						]
					}
				]
			},

			{xtype: 'displayfield', name: 'overlayLimitWarning', cls: 'warning-msg', hidden: true},
			{xtype: 'hidden', name: 'excludeMispricing'},

			Clifton.product.overlay.trade.group.option.RunTradeOptionCashFlowReplicationGrid
		]
	}
});

