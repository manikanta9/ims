Clifton.product.overlay.trade.RunTradeCashExposureRecapFieldSet = {
	xtype: 'fieldset',
	title: 'Cash Exposure Recap',
	labelWidth: 140,
	listeners: {
		collapse: function(p) {
			p.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		},
		expand: function(p) {
			p.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		}
	},
	items: [{
		xtype: 'formtable',
		columns: 3,
		defaults: {
			xtype: 'currencyfield',
			decimalPrecision: 2,
			readOnly: true,
			submitValue: false,
			width: '95%'
		},
		items: [
			{cellWidth: '120px', xtype: 'label'},
			{html: '$', xtype: 'label', style: 'text-align: right'},
			{html: '%', qtip: '% of Overlay', xtype: 'label', style: 'text-align: right'},

			{html: 'Cash Total:', xtype: 'label', qtip: 'Total Cash allocation across all managers'},
			{name: 'cashTotal', decimalPrecision: 0},
			{name: 'cashTotalPercent'},

			{html: 'Net Overlay:', xtype: 'label', qtip: 'Sum of Overlay Exposure: actual positions booked to General Ledger'},
			{name: 'netOverlay', decimalPrecision: 0},
			{name: 'netOverlayPercent'},

			{html: 'Mispricing Total:', xtype: 'label', qtip: 'Total Mispricing across all contracts.  Mispricing Calculation applies when security instrument is flagged as using fair value adjustment.<br/><br/>Calculation: (Future Price - (Index Price + Fair Value Adjustment)) * Future Price Multiplier * FX Rate * # of Contracts'},
			{name: 'mispricingTotal', decimalPrecision: 0},
			{name: 'mispricingTotalPercent'},

			{html: 'Cash Target:', xtype: 'label', qtip: 'Cash Target = Cash Total - Overlay Target. Percentage value is capped at +/-9,999.9999.'},
			{name: 'cashTarget', decimalPrecision: 0},
			{name: 'cashTargetPercent'},

			{html: '<hr/>', xtype: 'label', colspan: 3, width: '99%'},

			{html: 'Cash Exposure:', xtype: 'label', qtip: 'Cash Exposure = Cash Total - (Net Overlay + Cash Target) - Mispricing Total. Percentage value is capped at +/-9,999.9999.'},
			{name: 'cashExposure', decimalPrecision: 0},
			{name: 'cashExposurePercent'},

			{html: 'Pending Overlay:', xtype: 'label', qtip: 'Includes Overlay Exposure from Pending Contracts as well as Trade Impact: not in General Ledger'},
			{name: 'pendingOverlay', decimalPrecision: 0},
			{name: 'pendingOverlayPercent'},

			{html: '<hr/>', xtype: 'label', colspan: 3, width: '99%'},

			{html: 'Final Cash Exposure:', xtype: 'label', qtip: 'Final Cash Exposure = Cash Exposure - Pending Overlay = Cash Total - (Net Overlay + <b>Pending Overlay</b> + Cash Target)'},
			{name: 'finalCashExposure', decimalPrecision: 0},
			{name: 'finalCashExposurePercent'}

		]
	}]
};
