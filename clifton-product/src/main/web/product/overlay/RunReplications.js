Clifton.product.overlay.RunReplications = {
	title: 'Replications',
	items: [{
		xtype: 'panel',
		border: false,
		autoScroll: true,

		listeners: {
			afterrender: function(p) {
				p.reload.call(p);
			}
		},

		reload: function() {
			const p = this;
			const loader = new TCG.data.JsonLoader({
				waitTarget: p,
				waitMsg: 'Loading Replications...',
				params: {
					runId: p.getWindow().getMainFormId(),
					requestedMaxDepth: 4
				},
				onLoad: function(data, conf) {
					// NOTE: THIS WILL ALWAYS CALL CURRENCY OTHER LOAD ONCE FOR EACH RUN
					const repData = data;
					const curLoader = new TCG.data.JsonLoader({
						waitTarget: p,
						onLoad: function(data, conf) {
							p.refresh.call(p, repData, data);
						}
					});
					curLoader.load('portfolioCurrencyOtherListByRun.json?requestedMaxDepth=3&runId=' + p.getWindow().getMainFormId());
				}
			});
			loader.load('productOverlayAssetClassReplicationListByRun.json');

		},

		refresh: function(repData, currencyOtherData) {
			this.removeAll(true);

			let currData = [];
			let currLabel = undefined;
			const l = repData.length;
			for (let i = 0; i < l; i++) {
				const d = repData[i];
				if (d.labelLong !== currLabel) { // found new group
					if (currData.length > 0) {
						this.addGrid(currData, currencyOtherData);
					}
					currLabel = d.labelLong;
					currData = [];
				}
				currData[currData.length] = d;
				if (i === (l - 1)) {
					this.addGrid(currData, currencyOtherData);
				}
			}
			this.doLayout();
		},

		addGrid: function(data, currencyOtherData) {
			const rt = data[0].replicationType;
			let cols = [
				{header: 'Cash Exposure', width: 15, dataIndex: 'cashExposure', hidden: true, type: 'boolean'},
				{
					header: 'Instrument', width: 170, dataIndex: 'security.instrument.name',
					renderer: function(v, p, r) {
						if (TCG.isTrue(r.data.cashExposure)) {
							return 'Cash';
						}
						return v;
					}
				},

				// Targets
				{header: 'IsTargetAdjusted', width: 50, dataIndex: 'targetAdjusted', type: 'boolean', hidden: true},
				{header: 'Target Allocation % (Original)', width: 80, dataIndex: 'allocationWeight', type: 'percent', summaryType: 'sum', hidden: true},
				{
					header: 'Target Allocation %', width: 80, dataIndex: 'allocationWeightAdjusted', type: 'percent', summaryType: 'sum',
					renderer: function(v, p, r) {
						return TCG.renderAdjustedAmount(v, r.data['targetAdjusted'], r.data['allocationWeight'], '0,000.00 %');
					}
				},
				{header: 'Target Allocation Amount (Original)', width: 100, dataIndex: 'targetExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hidden: true},
				{header: 'Target Allocation Amount', width: 100, dataIndex: 'targetExposureAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},

				// Contracts
				{
					header: 'Contract Value', width: 85, dataIndex: 'value', type: 'currency', numberFormat: '0,000', negativeInRed: true,
					tooltip: 'Contract Value calculation is determined by the Replication Type.',
					renderer: function(v, p, r) {
						if (TCG.isTrue(r.data.cashExposure)) {
							return null;
						}
						return TCG.renderAmount(v, false, '0,000');
					}
				},
				{
					header: 'Target Contracts (Original)', width: 85, dataIndex: 'targetContracts', type: 'currency', summaryType: 'sum', negativeInRed: true, hidden: true,
					renderer: function(v, p, r) {
						if (TCG.isTrue(r.data.cashExposure)) {
							return null;
						}
						return TCG.renderAmount(v, false, '0,000');
					},
					summaryCondition: function(data) {
						return TCG.isFalse(data.cashExposure);
					},
					summaryTotalCondition: function(data) {
						return TCG.isFalse(data.cashExposure);
					}

				},
				{
					header: 'Target Contracts', width: 80, dataIndex: 'targetContractsAdjusted', type: 'currency', summaryType: 'sum', negativeInRed: true,
					renderer: function(v, p, r) {
						if (TCG.isTrue(r.data.cashExposure)) {
							return null;
						}
						return TCG.renderAdjustedAmount(v, r.data['targetAdjusted'], r.data['targetContracts'], '0,000.00');
					},
					summaryCondition: function(data) {
						return TCG.isFalse(data.cashExposure);
					},
					summaryTotalCondition: function(data) {
						return TCG.isFalse(data.cashExposure);
					}

				},

				{header: 'Actual Exposure (Original)', width: 85, dataIndex: 'actualExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hidden: true},
				{header: 'Additional Exposure', hidden: true, width: 100, dataIndex: 'additionalExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
				{header: 'Additional Overlay Exposure', hidden: true, width: 100, dataIndex: 'additionalOverlayExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
				{header: 'Virtual Exposure', hidden: true, width: 100, dataIndex: 'virtualExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},

				// Actual + Additional + Virtual
				{
					header: 'Total Exposure', hidden: true, width: 80, dataIndex: 'actualExposureAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
					tooltip: 'Actual Overlay Exposure + Virtual Exposure + Additional Exposure + Additional Overlay Exposure',
					renderer: function(v, metaData, r) {
						const actual = r.data['actualExposure'];
						const additional = r.data['additionalExposure'];
						const additionalOverlay = r.data['additionalOverlayExposure'];
						const virtual = r.data['virtualExposure'];
						let qtip = '';
						let showAdjusted = false;
						if (additional !== 0) {
							showAdjusted = true;
							qtip += '<tr><td>Additional Exposure:</td><td align="right">' + Ext.util.Format.number(additional, '0,000') + '</td></tr>';
						}
						if (additionalOverlay !== 0) {
							showAdjusted = true;
							qtip += '<tr><td>Additional Overlay Exposure:</td><td align="right">' + Ext.util.Format.number(additionalOverlay, '0,000') + '</td></tr>';
						}

						if (virtual !== 0) {
							showAdjusted = true;
							qtip += '<tr><td>Virtual Exposure:</td><td align="right">' + Ext.util.Format.number(virtual, '0,000') + '</td></tr>';
						}

						if (showAdjusted === true) {
							metaData.css = 'amountAdjusted';
							metaData.attr = 'qtip=\'<table><tr><td>Overlay Exposure:</td><td align="right">' + Ext.util.Format.number(actual, '0,000') + '</td></tr>' + qtip + '</table>\'';
						}
						return TCG.renderAmount(v, false, '0,000');
					}
				},

				{
					header: 'Actual Contracts (Original)', width: 85, dataIndex: 'actualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hidden: true,
					renderer: function(v, p, r) {
						if (TCG.isTrue(r.data.cashExposure)) {
							return null;
						}
						return TCG.renderAmount(v, false, '0,000');
					},
					summaryCondition: function(data) {
						return TCG.isFalse(data.cashExposure);
					},
					summaryTotalCondition: function(data) {
						return TCG.isFalse(data.cashExposure);
					}

				},
				{
					header: 'Actual Contracts', width: 80, dataIndex: 'actualContractsAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
					renderer: function(v, p, r) {
						if (TCG.isTrue(r.data.cashExposure)) {
							return null;
						}
						return TCG.renderAdjustedAmount(v, r.data['actualContracts'] !== v, r.data['actualContracts'], '0,000', 'Virtual Contracts');
					},
					summaryCondition: function(data) {
						return TCG.isFalse(data.cashExposure);
					},
					summaryTotalCondition: function(data) {
						return TCG.isFalse(data.cashExposure);
					}

				},


				{
					header: 'Difference', width: 85, dataIndex: 'actualTargetAdjustedContractDifference', type: 'currency', summaryType: 'sum', negativeInRed: true,
					renderer: function(v, p, r) {
						if (TCG.isTrue(r.data.cashExposure)) {
							return null;
						}
						return TCG.renderAmount(v, false, '0,000');
					},
					summaryCondition: function(data) {
						return TCG.isFalse(data.cashExposure);
					},
					summaryTotalCondition: function(data) {
						return TCG.isFalse(data.cashExposure);
					}

				},
				{
					header: 'Total Contracts', width: 85, dataIndex: 'totalContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hidden: true, negativeInRed: true,
					renderer: function(v, p, r) {
						if (TCG.isTrue(r.data.cashExposure)) {
							return null;
						}
						return TCG.renderAmount(v, false, '0,000');
					},
					summaryCondition: function(data) {
						return TCG.isFalse(data.cashExposure);
					},
					summaryTotalCondition: function(data) {
						return TCG.isFalse(data.cashExposure);
					}

				},

				{
					header: 'Security', width: 85, dataIndex: 'security.symbol',
					renderer: function(v, p, r) {
						if (TCG.isTrue(r.data.cashExposure)) {
							return null;
						}
						return v;
					}
				},


				{header: 'Underlying Security', width: 100, dataIndex: 'underlyingSecurity.symbol', hidden: true},
				// Default Display if calculator useUnderlying = true
				{header: 'Underlying Price', width: 85, dataIndex: 'underlyingSecurityPrice', type: 'float', negativeInRed: true, useNull: true},

				// Default Display if type.dirtyPriceSupported = false
				{
					header: 'Security Price', width: 85, dataIndex: 'securityPrice', type: 'float', negativeInRed: true,
					renderer: function(v, p, r) {
						if (TCG.isTrue(r.data.cashExposure)) {
							return null;
						}
						return TCG.renderAmount(v, false, '0,000', true);
					}
				},

				// Default Display if type.dirtyPriceSupported = true
				// Same as Security Price - just use that ???{header: 'Clean Price', width: 85, dataIndex: 'securityPrice', type: 'float', negativeInRed: true},
				{header: 'Dirty Price', width: 85, dataIndex: 'securityDirtyPrice', type: 'float', negativeInRed: true},

				// Default Display if type.indexRatioSupported = true
				{
					header: 'Index Ratio', width: 80, dataIndex: 'indexRatio', type: 'float', negativeInRed: true, useNull: true,
					tooltip: 'Index Ratio is a ratio between current and base CPI\'s and is used by inflation linked securities (TIPS, etc.).'
				},

				{header: 'Days To Mature', width: 80, dataIndex: 'daysToMaturity', type: 'int', negativeInRed: true, useNull: true},

				// Default Display if type.isInterestRateSupported
				{header: 'Interest Rate', width: 80, dataIndex: 'interestRate', type: 'currency', numberFormat: '0,000.0000', negativeInRed: true, useNull: true},

				// Default Display if type.syntheticAdjustmentFactorSupported
				{
					header: 'Syn Adj Factor', width: 80, dataIndex: 'syntheticAdjustmentFactor', type: 'currency', numberFormat: '0,000.0000', negativeInRed: true, useNull: true,
					tooltip: 'Formula: 1 / (1+((Days to Maturity/365)*Interest Rate))'
				},

				// Default Display if type.durationSupported = true
				{header: 'Duration', width: 80, dataIndex: 'duration', type: 'currency', numberFormat: '0,000.0000', negativeInRed: true, useNull: true},

				// Default Display if type.deltaSupported = true
				{
					header: 'Delta', width: 80, dataIndex: 'delta', type: 'currency', numberFormat: '0,000.0000', negativeInRed: true, useNull: true,
					tooltip: 'Market Data Value for <i>Delta</i> field. <br><br><b>Special Adjustment for Options:</b>&nbsp;Delta is Negated If Call Option and Short Target or Put Option and Long Target'
				},

				{header: 'Factor', width: 80, dataIndex: 'factor', type: 'currency', numberFormat: '0,000.0000', useNull: true, negativeInRed: true, tooltip: 'Current Factor Change event value for the security. If there are no events available the value will be 1.'},

				{header: 'Exchange Rate', width: 100, dataIndex: 'exchangeRate', type: 'float', negativeInRed: true, useNull: true}
			];

			if (TCG.isTrue(rt.currency)) {
				cols = [
					{header: 'Instrument', width: 170, dataIndex: 'security.instrument.name'},

					// Targets
					{header: 'IsTargetAdjusted', width: 50, dataIndex: 'targetAdjusted', type: 'boolean', hidden: true},
					{header: 'Target Allocation % (Original)', width: 80, dataIndex: 'allocationWeight', type: 'percent', summaryType: 'sum', hidden: true},
					{
						header: 'Target Allocation %', width: 80, dataIndex: 'allocationWeightAdjusted', type: 'percent', summaryType: 'sum',
						renderer: function(v, p, r) {
							return TCG.renderAdjustedAmount(v, r.data['targetAdjusted'], r.data['allocationWeight'], '0,000.00 %');
						}
					},
					{header: 'Target Allocation Amount (Original)', width: 100, dataIndex: 'targetExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hidden: true},
					{header: 'Target Allocation Amount', width: 100, dataIndex: 'targetExposureAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},

					// Physical Currency
					{header: 'Currency Local', width: 100, dataIndex: 'currencyActualLocalAmount', hidden: 'true', type: 'currency', numberFormat: '0,000', negativeInRed: true, useNull: true},
					{header: 'Unrealized Currency Exposure - Local', width: 100, dataIndex: 'currencyUnrealizedLocalAmount', hidden: 'true', type: 'currency', numberFormat: '0,000', negativeInRed: true, useNull: true},
					{header: 'Currency Exchange Rate', width: 50, dataIndex: 'currencyExchangeRate', hidden: 'true', type: 'currency', numberFormat: '0,000.0000', negativeInRed: true, useNull: true},

					{
						header: 'Currency Actual Base', width: 100, dataIndex: 'currencyActualBaseAmount', hidden: 'true', type: 'currency', numberFormat: '0,000', negativeInRed: true, useNull: true,
						tooltip: 'Physical Currency Balance in Account Base Currency'
					},
					{
						header: 'Unrealized Currency Exposure - Base', width: 100, dataIndex: 'currencyUnrealizedBaseAmount', hidden: 'true', type: 'currency', numberFormat: '0,000', negativeInRed: true, useNull: true,
						tooltip: 'Unrealized Currency Exposure in Account Base Currency'
					},

					{
						header: 'Currency (Actual)', width: 100, dataIndex: 'currencyTotalActualBaseAmount', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, useNull: true,
						tooltip: 'Physical Currency + Unrealized Currency Exposure in Account Base Currency',
						renderer: function(v, p, r) {
							return TCG.renderAdjustedAmount(v, r.data['currencyActualBaseAmount'] !== v, r.data['currencyActualBaseAmount'], '0,000', 'Unrealized Currency Exposure');
						}
					},

					// Default display if type.doNotApplyCurrencyOtherAllocation = false
					{
						header: 'Currency (Other)', width: 100, dataIndex: 'currencyOtherBaseAmount', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, useNull: true,
						tooltip: 'Currency Other applied to this contract in Account Base Currency'
					},
					{
						header: 'Currency (Trading)', width: 100, dataIndex: 'currencyDenominationBaseAmount', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, useNull: true,
						tooltip: 'Trading Currency applied to this contract in Account Base Currency.  Trading currency is applied differently than currency other when the underlying security equals the account base currency.  The trading currency of the security is then applied here and added to the target to get the net allocation.'
					},

					{
						header: 'Net Allocation', width: 100, dataIndex: 'netAllocation', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, useNull: true,
						tooltip: 'Target - (Currency Actual + Currency Other) + Currency Trading - Additional Exposure'
					},

					// Contracts
					{
						header: 'Contract Value', width: 85, dataIndex: 'value', type: 'currency', numberFormat: '0,000', negativeInRed: true,
						tooltip: 'Contract Value calculation is determined by the Replication Type.'
					},
					{header: 'Target Contracts (Original)', width: 85, dataIndex: 'targetContracts', type: 'currency', summaryType: 'sum', negativeInRed: true, hidden: true},
					{
						header: 'Target Contracts', width: 80, dataIndex: 'targetContractsAdjusted', type: 'currency', summaryType: 'sum', negativeInRed: true,
						renderer: function(v, p, r) {
							return TCG.renderAdjustedAmount(v, r.data['targetAdjusted'], r.data['targetContracts'], '0,000.00');
						}
					},

					{header: 'Additional Exposure', hidden: true, width: 100, dataIndex: 'additionalExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
					{header: 'Additional Overlay Exposure', hidden: true, width: 100, dataIndex: 'additionalOverlayExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
					{header: 'Virtual Exposure', hidden: true, width: 100, dataIndex: 'virtualExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
					{header: 'Actual Exposure (Original)', width: 85, dataIndex: 'actualExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hidden: true},
					// Actual + Additional + Virtual
					{
						header: 'Total Exposure', hidden: true, width: 80, dataIndex: 'actualExposureAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
						tooltip: 'Actual Overlay Exposure + Virtual Exposure + Additional Exposure',
						renderer: function(v, metaData, r) {
							const actual = r.data['actualExposure'];
							const additional = r.data['additionalExposure'];
							const additionalOverlay = r.data['additionalOverlayExposure'];
							const virtual = r.data['virtualExposure'];
							let qtip = '';
							let showAdjusted = false;
							if (additional !== 0) {
								showAdjusted = true;
								qtip += '<tr><td>Additional Exposure:</td><td align="right">' + Ext.util.Format.number(additional, '0,000') + '</td></tr>';
							}
							if (additionalOverlay !== 0) {
								showAdjusted = true;
								qtip += '<tr><td>Additional Overlay Exposure:</td><td align="right">' + Ext.util.Format.number(additionalOverlay, '0,000') + '</td></tr>';
							}

							if (virtual !== 0) {
								showAdjusted = true;
								qtip += '<tr><td>Virtual Exposure:</td><td align="right">' + Ext.util.Format.number(virtual, '0,000') + '</td></tr>';
							}

							if (showAdjusted === true) {
								metaData.css = 'amountAdjusted';
								metaData.attr = 'qtip=\'<table><tr><td>Overlay Exposure:</td><td align="right">' + Ext.util.Format.number(actual, '0,000') + '</td></tr>' + qtip + '</table>\'';
							}
							return TCG.renderAmount(v, false, '0,000');
						}
					},

					{header: 'Actual Contracts (Original)', width: 85, dataIndex: 'actualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hidden: true},
					{
						header: 'Actual Contracts', width: 80, dataIndex: 'actualContractsAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
						renderer: function(v, p, r) {
							return TCG.renderAdjustedAmount(v, r.data['actualContracts'] !== v, r.data['actualContracts'], '0,000', 'Virtual Contracts');
						}
					},
					{header: 'Difference', width: 85, dataIndex: 'actualTargetAdjustedContractDifference', type: 'currency', summaryType: 'sum', negativeInRed: true},
					{header: 'Total Contracts', width: 85, dataIndex: 'totalContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hidden: true, negativeInRed: true},


					{header: 'Security Price', width: 85, dataIndex: 'securityPrice', type: 'float', negativeInRed: true},

					{header: 'Days To Mature', width: 80, dataIndex: 'daysToMaturity', type: 'int', negativeInRed: true, useNull: true},
					{header: 'Exchange Rate', width: 85, dataIndex: 'exchangeRate', type: 'float', negativeInRed: true, useNull: true, hidden: true}
				];
			}

			TCG.grid.fillColumnMetaData(cols, [], []);

			this.add(new Ext.grid.GridPanel({
				autoHeight: true,
				collapsible: true,
				repType: rt,
				viewConfig: {forceFit: true},
				title: data[0].labelLong,
				store: new Ext.data.JsonStore({
					idProperty: 'id',
					fields: ['id', 'labelLong', 'security.instrument.name', 'targetAdjusted', 'allocationWeight', 'allocationWeightAdjusted', 'targetExposure', 'targetExposureAdjusted',
						'currencyActualLocalAmount', 'currencyUnrealizedLocalAmount', 'currencyActualBaseAmount', 'currencyUnrealizedBaseAmount', 'currencyTotalActualBaseAmount', 'currencyOtherBaseAmount', 'currencyDenominationBaseAmount', 'currencyExchangeRate', 'netAllocation',
						'value', 'targetContracts', 'targetContractsAdjusted', 'additionalExposure', 'additionalOverlayExposure', 'actualExposure', 'virtualExposure', 'actualExposureAdjusted', 'actualContracts', 'actualContractsAdjusted', 'actualTargetAdjustedContractDifference', 'totalContracts',
						'security.symbol', 'underlyingSecurity.symbol', 'underlyingSecurityPrice', 'securityPrice',
						'daysToMaturity', 'exchangeRate', 'interestRate', 'duration', 'delta', 'syntheticAdjustmentFactor', 'indexRatio', 'securityDirtyPrice', 'cashExposure'],
					data: data
				}),
				colModel: new Ext.grid.ColumnModel({columns: cols}),
				plugins: {ptype: 'gridsummary'},
				listeners: {
					rowdblclick: function(grid, rowIndex, evt) {
						const className = 'Clifton.product.overlay.assetclass.ReplicationWindow';
						const row = grid.store.data.items[rowIndex];
						const id = row.id;
						const cmpId = TCG.getComponentId(className, id);
						TCG.createComponent(className, {
							id: cmpId,
							params: {id: id},
							openerCt: grid
						});
					}
				},
				initComponent: function() {
					Ext.grid.GridPanel.prototype.initComponent.call(this, arguments);

					const cm = this.getColumnModel();
					const repType = this.repType;
					if (TCG.isFalse(repType.currency)) {
						cm.setHidden(cm.findColumnIndex('duration'), TCG.isFalse(repType.durationSupported));
						cm.setHidden(cm.findColumnIndex('syntheticAdjustmentFactor'), TCG.isFalse(repType.syntheticAdjustmentFactorSupported));
						cm.setHidden(cm.findColumnIndex('indexRatio'), TCG.isFalse(repType.indexRatioSupported));
						cm.setHidden(cm.findColumnIndex('securityDirtyPrice'), TCG.isFalse(repType.dirtyPriceSupported));
						// NOTE: Exchange Rate was only shown by default for Bond Replications (except Currency), so setting visibility based on security dirty price by default???
						cm.setHidden(cm.findColumnIndex('exchangeRate'), TCG.isFalse(repType.dirtyPriceSupported));
						cm.setHidden(cm.findColumnIndex('interestRate'), TCG.isFalse(repType.interestRateSupported));
						cm.setHidden(cm.findColumnIndex('delta'), TCG.isFalse(repType.deltaSupported));
						cm.setHidden(cm.findColumnIndex('underlyingSecurityPrice'), TCG.isFalse(repType.useUnderlying));
					}
					else {
						cm.setHidden(cm.findColumnIndex('currencyOtherBaseAmount'), TCG.isTrue(repType.doNotApplyCurrencyOtherAllocation));
						cm.setHidden(cm.findColumnIndex('currencyDenominationBaseAmount'), TCG.isTrue(repType.doNotApplyCurrencyOtherAllocation));
					}
				}
			}));
			if (TCG.isTrue(rt.currency)) {
				this.addCurrencyOtherGrid(currencyOtherData);
			}
		},

		addCurrencyOtherGrid: function(currencyOtherData) {
			if (currencyOtherData && currencyOtherData.length > 0) {
				const p = this;
				const cols = Clifton.product.overlay.CurrencyOtherColumns;
				TCG.grid.fillColumnMetaData(cols, [], []);

				p.add(new Ext.grid.GridPanel({
					autoHeight: true,
					collapsible: true,
					width: 500,
					viewConfig: {forceFit: true},
					title: 'Currency Other',
					store: new Ext.data.JsonStore({
						idProperty: 'id',
						fields: ['id', 'currencySecurity.symbol', 'localAmount', 'exchangeRate', 'baseAmount', 'totalBaseAmount', 'unrealizedLocalAmount', 'currencyDenominationAllocation'],
						data: currencyOtherData
					}),
					colModel: new Ext.grid.ColumnModel({columns: cols}),
					plugins: {ptype: 'gridsummary'}
				}));
			}
		},

		getWindow: function() {
			let result = this.findParentByType(Ext.Window);
			if (TCG.isNull(result)) {
				result = this.findParentBy(function(o) {
					return o.baseCls === 'x-window';
				});
			}
			return result;
		}
	}]
};


Clifton.product.overlay.CurrencyOtherColumns = [
	{header: 'Currency', width: 100, dataIndex: 'currencySecurity.symbol'},
	{header: 'Trading CCY Allocation', width: 50, dataIndex: 'currencyDenominationAllocation', type: 'boolean', hidden: true},
	{header: 'Local Amount (Currency Only)', width: 100, dataIndex: 'localAmount', type: 'currency', numberFormat: '0,000', negativeInRed: true, useNull: true, hidden: true},
	{header: 'Unrealized Currency Exposure (Local)', width: 100, dataIndex: 'unrealizedLocalAmount', type: 'currency', numberFormat: '0,000', negativeInRed: true, useNull: true, hidden: true},

	{
		header: 'Local Amount', width: 100, type: 'currency', numberFormat: '0,000', negativeInRed: true, useNull: true,
		renderer: function(v, p, r) {
			const total = r.data.localAmount + r.data.unrealizedLocalAmount;
			return TCG.renderAdjustedAmount(total, r.data['localAmount'] !== total, r.data['localAmount'], '0,000', 'Unrealized Currency Exposure');
		}
	},

	{header: 'Exchange Rate', width: 100, dataIndex: 'exchangeRate', type: 'float', negativeInRed: true, useNull: true},
	{
		header: 'Base Amount (Other)', width: 100, dataIndex: 'totalBaseAmount', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, useNull: true,
		renderer: function(v, p, r) {
			if (TCG.isFalse(r.data.currencyDenominationAllocation)) {
				return TCG.renderAmount(v, false, '0,000');
			}
			return null;
		},
		summaryCalculation: function(v, r, field, data, col) {
			if (TCG.isFalse(r.data.currencyDenominationAllocation)) {
				return v + r.data.totalBaseAmount;
			}
			return v;
		},
		summaryRenderer: function(v) {
			return TCG.renderAmount(v, false, '0,000');
		}
	},
	{
		header: 'Base Amount (Trading)', width: 100, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, useNull: true,
		tooltip: 'Trading currency is applied differently than currency other when a replication is found where the underlying security equals the account base currency.  The trading currency of the security is then added to the target to get the net allocation.',
		renderer: function(v, p, r) {
			if (TCG.isTrue(r.data.currencyDenominationAllocation)) {
				return TCG.renderAmount(r.data.totalBaseAmount, false, '0,000');
			}
			return null;
		},
		summaryCalculation: function(v, r, field, data, col) {
			if (TCG.isTrue(r.data.currencyDenominationAllocation)) {
				return v + r.data.totalBaseAmount;
			}
			return v;
		},
		summaryRenderer: function(v) {
			return TCG.renderAmount(v, false, '0,000');
		}
	}

];


