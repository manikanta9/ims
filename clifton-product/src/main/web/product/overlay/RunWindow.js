TCG.use('Clifton.portfolio.run.BaseRunWindow');

TCG.use('Clifton.product.overlay.RunManagerAllocation');
TCG.use('Clifton.product.overlay.RunManagerBalance');
TCG.use('Clifton.product.overlay.RunCashAllocation');
TCG.use('Clifton.product.overlay.RunExposureSummary');
TCG.use('Clifton.product.overlay.RunReplications');
TCG.use('Clifton.product.overlay.RunRebalance');
TCG.use('Clifton.product.overlay.RunManagerRebalance');
TCG.use('Clifton.product.overlay.RunSyntheticExposure');
TCG.use('Clifton.product.overlay.RunCashSummary');
TCG.use('Clifton.portfolio.run.RunMargin');

Clifton.product.overlay.RunWindow = Ext.extend(Clifton.portfolio.run.BaseRunWindow, {

	additionalRunItems: [
		{xtype: 'sectionheaderfield', header: 'Cash Exposure Recap', fieldLabel: ''},
		{
			xtype: 'formtable',
			columns: 5,
			defaults: {
				xtype: 'currencyfield',
				decimalPrecision: 0,
				readOnly: true,
				submitValue: false,
				width: '95%'
			},
			items: [
				{cellWidth: '120px', xtype: 'label'},
				{html: 'Cash Total', qtip: 'Total Cash allocation across all managers', xtype: 'label', style: 'text-align: right'},
				{html: 'Net Overlay', qtip: 'Sum of Overlay Exposure: actual positions booked to General Ledger', xtype: 'label', style: 'text-align: right'},
				{html: 'Cash Target', qtip: 'Cash Target = Cash Total - Overlay Target. Percentage value is capped at +/-9,999.9999.', xtype: 'label', style: 'text-align: right'},
				{html: 'Cash Exposure', qtip: 'Cash Exposure = Cash Total - (Overlay Exposure + Cash Target). Percentage value is capped at +/-9,999.9999.', xtype: 'label', style: 'text-align: right'},

				{html: 'Amount:', xtype: 'label'},
				{name: 'cashTotal'},
				{name: 'overlayExposureTotal'},
				{name: 'cashTarget'},
				{name: 'cashExposure'},

				{html: '% of TPV:', xtype: 'label'},
				{name: 'cashTotalPercent', decimalPrecision: 4},
				{name: 'overlayExposureTotalPercent', decimalPrecision: 4},
				{name: 'cashTargetPercent', decimalPrecision: 4},
				{name: 'cashExposurePercent', decimalPrecision: 4},

				{cellWidth: '120px', xtype: 'label', colspan: 3},
				{html: 'Portfolio (TPV):', xtype: 'label', style: 'text-align: right', qtip: 'Total Portfolio Value'},
				{name: 'portfolioTotalValue'}
			]
		}
	],

	additionalRunTabs: [
		Clifton.product.overlay.RunManagerAllocation,
		Clifton.product.overlay.RunManagerBalance,
		Clifton.product.overlay.RunCashAllocation,
		Clifton.product.overlay.RunExposureSummary,
		Clifton.product.overlay.RunSyntheticExposure,
		Clifton.portfolio.run.RunAccountNotes,
		Clifton.product.overlay.RunRebalance,
		Clifton.product.overlay.RunManagerRebalance,
		Clifton.product.overlay.RunReplications,
		Clifton.product.overlay.RunCashSummary,
		Clifton.portfolio.run.RunMargin,
		Clifton.portfolio.run.RunPositions,
		Clifton.portfolio.run.RunPortalFiles
	]
});
