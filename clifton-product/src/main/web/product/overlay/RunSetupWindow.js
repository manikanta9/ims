Clifton.product.overlay.RunSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'productOverlayRunSetupWindow',
	title: 'Portfolio Runs',
	iconCls: 'run',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{
				title: 'My Runs',
				items: [{
					xtype: 'portfolio-runs-gridpanel',
					rowSelectionModel: 'checkbox',
					instructions: 'The following Portfolio runs have been or are being processed, and have been created by you (the current user).',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'investmentAccountGroupId', width: 180, url: 'investmentAccountGroupListFind.json'},
							{fieldLabel: 'Client Acct', xtype: 'toolbar-combo', name: 'investmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientInvestmentAccount.label', displayField: 'label'}
						];
					},
					getLoadParams: function(firstLoad) {
						const t = this.getTopToolbar();
						if (firstLoad) {
							// default balance date to previous business day
							const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
							this.setFilterValue('balanceDate', {'on': prevBD});
						}
						const lp = {requestedMaxDepth: 4, readUncommittedRequested: true, createUserId: TCG.getCurrentUser().id};
						const v = TCG.getChildByName(t, 'investmentAccountGroupId').getValue();
						if (v) {
							lp.investmentAccountGroupId = v;
						}
						return lp;
					},
					editor: {
						detailPageClass: 'Clifton.portfolio.run.RunWindow',
						getDefaultData: function(gridPanel) { // defaults investment account
							const t = gridPanel.getTopToolbar();
							const ia = TCG.getChildByName(t, 'investmentAccountId');
							if (ia.getValue() === '') {
								TCG.showError('Please first select an investment account for the new Portfolio Run you are creating.');
								return false;
							}
							const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
							return {
								clientInvestmentAccount: {id: ia.getValue(), label: ia.getRawValue()},
								balanceDate: prevBD.format('Y-m-d 00:00:00'),
								mainRun: true
							};
						},
						getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
							return undefined; // Not needed for Existing Runs
						},

						ptype: 'workflowAwareEntity-grideditor',
						tableName: 'PortfolioRun',
						transitionWorkflowStateList: [
							{stateName: 'Analyst Approved', iconCls: 'shopping-cart', buttonText: 'Submit to Trading', buttonTooltip: 'Submit selected runs to PMs for Trading/Approval'},
							{stateName: 'Posted (Current)', iconCls: 'www', buttonText: 'Post & Submit to Trading', buttonTooltip: 'Post selected runs to the portal and submit to PMs for Trading/Approval.'},
							{stateName: 'Processing', iconCls: 'run', buttonText: 'Re-Process', buttonTooltip: 'Re-Process selected runs'}
						],
						addToolbarDeleteButton: function(toolBar) {
							toolBar.add({
								text: 'Remove',
								tooltip: 'Remove selected item(s)',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									const gridPanel = this.getGridPanel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
									}
									else {
										Ext.Msg.confirm('Delete Selected Row(s)', 'Would you like to delete all selected rows(s)?', function(a) {
											if (a === 'yes') {
												const runs = sm.getSelections();
												gridPanel.deleteRun(runs, 0, gridPanel);
											}
										});
									}
								}
							});
							toolBar.add('-');
						}
					},
					deleteRun: function(runs, count, gridPanel) {
						const runId = runs[count].json.id;
						const loader = new TCG.data.JsonLoader({
							waitMsg: 'Deleting Portfolio Run',
							waitTarget: this,
							params: {
								id: runId
							},
							onLoad: function(record, conf) {
								count++;
								if (count === runs.length) { // refresh after all runs were deleted
									gridPanel.reload();
								}
								else {
									gridPanel.deleteRun(runs, count, gridPanel);
								}
							}
						});
						loader.load('portfolioRunDelete.json');
					}
				}]
			},


			{
				title: 'All Runs',
				items: [{
					xtype: 'portfolio-runs-gridpanel'
				}]
			},


			{
				title: 'Trade Creation',
				items: [{
					xtype: 'product-tradeCreationGrid'
				}]
			},


			{
				title: 'Incomplete Runs',
				items: [{
					xtype: 'product-incompleteRunsGrid'
				}]
			},


			{
				title: 'Administration',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						labelWidth: 180,
						loadValidation: false, // using the form only to get background color/padding
						height: 230,
						buttonAlign: 'right',
						instructions: 'Use this section to (re)process multiple Portfolio runs at a time. Select criteria below to filter which Portfolio runs to (re)process.',
						listeners: {
							afterrender: function(fp) {
								const f = this.getForm();
								const bd = f.findField('balanceDate');
								if (bd.getValue() === '') {
									const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
									bd.setValue(prevBD.format('m/d/Y'));
								}
							}
						},
						items: [
							{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'investmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['clientInvestmentAccountId'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
							{fieldLabel: 'Client Account', name: 'accountLabel', hiddenName: 'clientInvestmentAccountId', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', mutuallyExclusiveFields: ['investmentAccountGroupId']},

							{boxLabel: 'Include Market On Close Adjustments', name: 'marketOnCloseAdjustmentsIncluded', xtype: 'checkbox', labelSeparator: ''},
							{fieldLabel: 'Balance Date', xtype: 'datefield', name: 'balanceDate', allowBlank: false},
							{boxLabel: 'If data already has been processed, reprocess and overwrite.', name: 'reload', xtype: 'checkbox', labelSeparator: ''},
							{boxLabel: 'Reload/Re-Process Proxy Manager Balances', name: 'reloadProxyManagers', xtype: 'checkbox', labelSeparator: ''}
						],

						buttons: [{
							text: 'Process Portfolio Runs',
							iconCls: 'run',
							width: 150,
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								form.submit(Ext.applyIf({
									url: 'portfolioRunListProcess.json',
									waitMsg: 'Processing...',
									success: function(form, action) {
										Ext.Msg.alert('Process Started', action.result.data, function() {
											const grid = owner.ownerCt.items.get(1);
											grid.reloadRepeated(); // uses defaults of every 3s for 30s
										});
									}
								}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
							}
						}]
					},
					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'PORTFOLIO-RUN',
						instantRunner: true,
						instructions: 'The following Portfolio runs are being (re)processed right now.',
						title: 'Current Runs',
						flex: 1
					}
				]
			}
		]
	}]
});

