Clifton.product.overlay.RunCashSummary = {
	title: 'Cash Summary',
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},
	frame: true,

	items: [{
		name: 'productOverlayAssetClassListFind',
		xtype: 'gridpanel',
		instructions: 'Implementing client account policy results in the following cash allocations to asset classes. Totals for each asset class need to be overlayed using corresponding replications. The total cash to be overlayed may not equal to total manager cash because not all cash is always overlayed and some cash amounts (rebalance cash) do not represent real cash and may get adjusted based on corresponding benchmark performance.',
		border: 1,
		flex: 0.68,

		getLoadParams: function() {
			const nonRollupOnly = TCG.getChildByName(this.getTopToolbar(), 'nonRollupOnly').getValue();
			if (nonRollupOnly === true) {
				return {
					runId: this.getWindow().getMainForm().formValues.id,
					populateCashAdjustments: true,
					rollupAssetClass: false
				};
			}
			return {
				runId: this.getWindow().getMainForm().formValues.id,
				populateCashAdjustments: true,
				rootOnly: true
			};
		},
		topToolbarUpdateCountPrefix: '-',
		getTopToolbarFilters: function(toolbar) {
			return [
				{
					boxLabel: 'View Non Rollup Asset Class Level&nbsp;', xtype: 'checkbox', name: 'nonRollupOnly',

					listeners: {
						check: function(field) {
							TCG.getParentByClass(field, Ext.Panel).reload();
						}
					}
				}
			];
		},
		isPagingEnabled: function() {
			return false;
		},
		pageSize: 500,
		columns: [
			{header: 'Asset Class', width: 140, dataIndex: 'accountAssetClass.label'},
			{header: 'IsTargetAdjusted', width: 50, dataIndex: 'targetAdjusted', type: 'boolean', hidden: true},
			{header: 'TFA', width: 50, dataIndex: 'targetFollowsActual', type: 'boolean', hidden: true},
			{header: 'Original Target', width: 80, dataIndex: 'targetAllocation', type: 'currency', summaryType: 'sum', hidden: true},
			{header: 'Original Target %', width: 80, dataIndex: 'targetAllocationPercent', type: 'percent', summaryType: 'sum', hidden: true},

			// Cash Columns
			{
				header: 'Fund Cash (Original)', width: 80, hidden: true, dataIndex: 'fundCash', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Cash held in a cash only account.  This is essentially a checking/savings account for the fund.  The fund holds a certain amount of cash to pay expenses, make pension payments, fund new managers, etc.'
			},
			{
				header: 'Fund Cash', width: 80, dataIndex: 'fundCashWithAdjustments', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Cash held in a cash only account.  This is essentially a checking/savings account for the fund.  The fund holds a certain amount of cash to pay expenses, make pension payments, fund new managers, etc.',
				renderer: function(v, metaData, r) {
					const original = r.data.fundCash;
					return TCG.renderAdjustedAmount(v, (v - original !== 0), original, '0,000', 'Manual Cash Adjustments:', false);
				}
			},

			{
				header: 'Manager Cash (Original)', width: 80, hidden: true, dataIndex: 'managerCash', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Cash held within an investment manager in an asset class other than the Cash asset class.'
			},
			{
				header: 'Manager Cash', width: 80, dataIndex: 'managerCashWithAdjustments', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Cash held within an investment manager in an asset class other than the Cash asset class.',
				renderer: function(v, metaData, r) {
					const original = r.data.managerCash;
					return TCG.renderAdjustedAmount(v, (v - original !== 0), original, '0,000', 'Manual Cash Adjustments:', false);
				}
			},


			{header: 'Transition Cash (Original)', width: 80, hidden: true, dataIndex: 'transitionCash', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
			{
				header: 'Transition Cash', width: 80, dataIndex: 'transitionCashWithAdjustments', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				renderer: function(v, metaData, r) {
					const original = r.data.transitionCash;
					return TCG.renderAdjustedAmount(v, (v - original !== 0), original, '0,000', 'Manual Cash Adjustments:', false);
				}
			},

			{
				header: 'Rebalance Cash (Original)', width: 80, dataIndex: 'rebalanceCash', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hidden: true, negativeInRed: true,
				tooltip: 'This is a cash \'proxy\'.  When rebalancing a client, we use offsetting positive and negative cash values by asset class to bring the fund back in line with their asset class targets.  This cash value is assigned to an asset class and generally has a benchmark associated with it.'
			},
			{header: 'Rebalance Cash Adjusted', width: 80, dataIndex: 'rebalanceCashAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hidden: true, negativeInRed: true},
			{
				header: 'Rebalance Attribution Cash', width: 80, dataIndex: 'rebalanceAttributionCash', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hidden: true, negativeInRed: true,
				tooltip: 'Generated during minimize imbalances processing and used to remove negative Manager, Fund, and Transition Cash from their buckets in order to properly perform attribution analysis.'
			},
			{
				header: 'Rebalance Cash', width: 80, dataIndex: 'rebalanceCashWithAdjustments', type: 'currency', summaryType: 'sum', negativeInRed: true,
				tooltip: '<b>Rebalance Cash/Adjusted:&nbsp;</b>A cash \'proxy\'.  When rebalancing a client, we use offsetting positive and negative cash values by asset class to bring the fund back in line with their asset class targets.  This cash value is assigned to an asset class and generally has a benchmark associated with it.<br/><br/>'
					+ '<b>Attribution Cash:&nbsp;</b>Generated during minimize imbalancing processing and used to remove negative Manager, Fund, and Transition Cash from their buckets in order to properly perform attribution analysis.<br/><br/>',
				renderer: function(v, metaData, r) {
					const rebalCash = r.data['rebalanceCash'];
					const rebalAdjustment = r.data['rebalanceCashAdjusted'] - rebalCash;
					const rebalAttribution = r.data['rebalanceAttributionCash'];
					const rebalManualAdj = v - rebalAttribution - r.data['rebalanceCashAdjusted'];
					if (rebalAdjustment !== 0 || rebalAttribution !== 0 || rebalManualAdj !== 0) {
						let qtip = '<table><tr><td>Original Rebalance Cash:</td><td align="right">' + TCG.renderAmount(rebalCash, true, '0,000') + '</td></tr>';
						qtip += '<tr><td>Adjustment for Benchmark:</td><td align="right">' + TCG.renderAmount(rebalAdjustment, true, '0,000') + '</td></tr>';
						qtip += '<tr><td>Attribution Rebalancing:</td><td align="right">' + TCG.renderAmount(rebalAttribution, true, '0,000') + '</td></tr>';
						qtip += '<tr><td>Manual Adjustments:</td><td align="right">' + TCG.renderAmount(rebalManualAdj, true, '0,000') + '</td></tr>';
						qtip += '<tr><td>&nbsp;</td><td><hr/></td></tr>';
						qtip += '<tr><td>Total Rebalance Cash:</td><td align="right">' + TCG.renderAmount(v, true, '0,000') + '</td></tr>';
						qtip += '</table>';
						metaData.css = 'amountAdjusted';
						metaData.attr = 'qtip=\'' + qtip + '\'';
					}
					return Ext.util.Format.number(v, '0,000');
				}
			},
			{
				header: 'Effective Total', width: 80, dataIndex: 'effectiveCashTotal', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, css: 'BORDER-LEFT: #c0c0c0 1px solid;',
				tooltip: 'Effective Cash = Manager Cash + Fund Cash + Transition Cash + Rebalance Cash Adjusted<br/><br/>NOTE: Client Directed Cash is excluded from Effective Cash Total'
			},
			{
				header: 'Client Directed Cash (Original)', width: 80, hidden: true, dataIndex: 'clientDirectedCash', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Defined on the Account Asset Class and used to generate trade recommendations only when the client instructs us to (except for when there are imbalances within the asset class)'
			},
			{
				header: 'Client Directed Cash', width: 80, dataIndex: 'clientDirectedCashWithAdjustments', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Defined on the Account Asset Class and used to generate trade recommendations only when the client instructs us to (except for when there are imbalances within the asset class)',
				renderer: function(v, metaData, r) {
					const original = r.data.clientDirectedCash;
					return TCG.renderAdjustedAmount(v, (v - original !== 0), original, '0,000', 'Manual Cash Adjustments:', false);
				}
			},
			{
				header: 'Total', width: 80, dataIndex: 'cashTotal', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, css: 'BORDER-LEFT: #c0c0c0 1px solid;',
				tooltip: 'Cash Total = Effective Cash (Manager Cash + Fund Cash + Transition Cash + Total Rebalance Cash (Adjusted + Attribution)) + Client Directed Cash'
			}
		],
		plugins: {ptype: 'gridsummary'},
		editor: {
			detailPageClass: 'Clifton.product.overlay.assetclass.AssetClassWindowSelector',
			drillDownOnly: true
		}
	},

		{flex: 0.02},

		{
			name: 'productOverlayAssetClassCashAdjustmentListByRun',
			xtype: 'gridpanel',
			title: 'Cash Bucket Adjustments',
			wikiPage: 'IT/Cash Attribution',
			instructions: 'The following cash has been moved from one bucket to another for cash attribution for the selected asset class.  These cash adjustments are not applied to run processing but used for Performance Summaries only.  When re-processing the run the cash adjustments entered will be deleted.',
			border: 1,
			flex: 0.3,

			fullReload: function() {
				this.reload();
				this.reloadTopGrid();
			},
			reloadTopGrid: function() {
				this.ownerCt.items.get(0).reload();
			},

			getLoadParams: function() {
				return {
					runId: this.getWindow().getMainForm().formValues.id
				};
			},
			isPagingEnabled: function() {
				return false;
			},

			columns: [
				{header: 'Asset Class', width: 140, dataIndex: 'overlayAssetClass.label'},
				{header: 'From', width: 75, dataIndex: 'fromCashType'},
				{header: 'To', width: 75, dataIndex: 'toCashType'},
				{header: 'Amount', width: 75, dataIndex: 'amount', type: 'currency', numberFormat: '0,000'},
				{header: 'Note', width: 200, dataIndex: 'note'}
			],
			editor: {
				detailPageClass: 'Clifton.product.overlay.assetclass.AssetClassCashAdjustmentWindow',
				getDefaultData: function(gridPanel) {
					const run = gridPanel.getWindow().getMainForm().formValues;
					return {overlayAssetClass: {overlayRun: run}};
				},
				addToolbarDeleteButton: function(toolBar) {
					toolBar.add({
						text: 'Remove',
						tooltip: 'Remove selected item(s)',
						iconCls: 'remove',
						scope: this,
						handler: function() {
							const editor = this;
							const grid = this.grid;
							const gp = grid.ownerCt;
							const sm = grid.getSelectionModel();
							if (sm.getCount() === 0) {
								TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
							}
							else if (sm.getCount() !== 1) {
								TCG.showError('Multi-selection deletes are not supported yet.  Please select one row.', 'NOT SUPPORTED');
							}
							else {
								Ext.Msg.confirm('Delete Selected Row', 'Would you like to delete selected row?', function(a) {
									if (a === 'yes') {
										const loader = new TCG.data.JsonLoader({
											waitTarget: gp,
											waitMsg: 'Deleting...',
											params: editor.getDeleteParams(sm),
											conf: {rowId: sm.getSelected().id},
											onLoad: function(record, conf) {
												grid.getStore().remove(sm.getSelected());
												grid.ownerCt.updateCount();
												gp.reloadTopGrid();
											}
										});
										loader.load(editor.getDeleteURL());
									}
								});
							}
						}
					});
					toolBar.add('-');
				}
			}
		}]
};
