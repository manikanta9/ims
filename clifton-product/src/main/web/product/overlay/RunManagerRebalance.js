Clifton.product.overlay.RunManagerRebalance = {
	title: 'Manager Rebalance',
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},
	frame: true,

	items: [{
		name: 'productOverlayManagerAccountRebalanceListByRun',
		xtype: 'gridpanel',
		instructions: 'Selected client account has the following manager account groups and distribution for each group.  Rebalance warnings are generated when the balances cross the rebalance triggers for a manager within the group.',
		appendStandardColumns: false,
		border: 1,
		flex: 1,
		getLoadParams: function() {
			return {
				runId: this.getWindow().getMainForm().formValues.id
			};
		},
		groupField: 'managerAccountGroup.name',
		groupTextTpl: '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Managers" : "Manager"]})',
		remoteSort: true,
		isPagingEnabled: function() {
			return false;
		},
		pageSize: 1000,
		columns: [
			{header: 'Group', width: 75, dataIndex: 'managerAccountGroup.name', hidden: true},
			{header: 'Outside of Rebalance Triggers', width: 75, dataIndex: 'outsideOfRebalanceTriggers', type: 'boolean', hidden: true},
			{header: 'Manager Account', width: 75, dataIndex: 'managerAccount.label'},

			{header: 'Target Allocation', width: 100, dataIndex: 'targetAllocationPercent', type: 'percent', summaryType: 'sum', hideGrandTotal: true},
			{header: 'Actual Allocation', width: 100, dataIndex: 'actualAllocationPercent', type: 'percent', summaryType: 'sum', hideGrandTotal: true},
			{
				header: 'Actual Deviation from Target', width: 100, dataIndex: 'actualDeviationFromTargetPercent', type: 'percent',
				renderer: function(v, p, r) {
					if (r.data['outsideOfRebalanceTriggers'] === true) {
						p.css = 'ruleViolation';
					}
					return TCG.renderAmount(v, false, '0,000.00 %');
				}
			},
			{header: 'Rebalance Min', width: 100, dataIndex: 'rebalanceTriggerMinPercent', type: 'percent'},
			{header: 'Rebalance Max', width: 100, dataIndex: 'rebalanceTriggerMaxPercent', type: 'percent'}
		],
		plugins: {ptype: 'groupsummary'}
	},


		{flex: 0.02},

		{
			name: 'productOverlayManagerAccountRebalanceListByRun',
			xtype: 'gridpanel',
			instructions: 'Selected client account has the following manager account groups and distribution for each group.  Rebalance warnings are generated when the balances cross the rebalance triggers for a manager within the group.',
			appendStandardColumns: false,
			border: 1,
			flex: 1,
			getLoadParams: function() {
				return {
					runId: this.getWindow().getMainForm().formValues.id
				};
			},
			groupField: 'managerAccountGroup.name',
			groupTextTpl: '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Managers" : "Manager"]})',
			remoteSort: true,
			isPagingEnabled: function() {
				return false;
			},
			pageSize: 1000,
			columns: [
				{header: 'Group', width: 75, dataIndex: 'managerAccountGroup.name', hidden: true},
				{header: 'Outside of Rebalance Triggers', width: 75, dataIndex: 'outsideOfRebalanceTriggers', type: 'boolean', hidden: true},
				{header: 'Manager Account', width: 75, dataIndex: 'managerAccount.label'},

				{header: 'Target Allocation', width: 100, dataIndex: 'targetAllocation', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hideGrandTotal: true},
				{header: 'Actual Allocation', width: 100, dataIndex: 'actualAllocation', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hideGrandTotal: true},
				{
					header: 'Actual Deviation from Target', width: 100, dataIndex: 'actualDeviationFromTarget', type: 'currency',
					renderer: function(v, p, r) {
						if (r.data['outsideOfRebalanceTriggers'] === true) {
							p.css = 'ruleViolation';
						}
						return TCG.renderAmount(v, false, '0,000');
					}
				},
				{header: 'Rebalance Min', width: 100, dataIndex: 'rebalanceTriggerMin', type: 'currency', numberFormat: '0,000'},
				{header: 'Rebalance Max', width: 100, dataIndex: 'rebalanceTriggerMax', type: 'currency', numberFormat: '0,000'}
			],
			plugins: {ptype: 'groupsummary'}
		}
	]
};
