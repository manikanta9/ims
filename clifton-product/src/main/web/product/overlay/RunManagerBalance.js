TCG.use('Clifton.portfolio.run.RunManagerBalanceGrid');

Clifton.product.overlay.RunManagerBalance = {
	title: 'Manager Adjustments',
	items: [{
		xtype: 'portfolio-run-manager-balance-grid',
		showGroupedAllocations: true
	}]
};
