Clifton.product.hedging.AccountStrategyWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Hedging Strategy',
	iconCls: 'account',
	width: 650,
	height: 500,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Strategy Details',
				items: [{
					xtype: 'formpanel',
					url: 'productHedgingAccountStrategy.json',
					labelWidth: 125,

					items: [
						{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
						{fieldLabel: 'Name', name: 'name'},
						{fieldLabel: 'Structure', name: 'strategyStructure'},
						{fieldLabel: 'Notional Multiplier', name: 'strategyNotionalMultiplier', value: 1, xtype: 'floatfield'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
						{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
						{
							xtype: 'fieldset',
							title: 'Underlying Security',
							items: [
								{fieldLabel: 'Security', name: 'underlyingSecurity.label', hiddenName: 'underlyingSecurity.id', displayField: 'label', xtype: 'combo', url: 'investmentSecurityListFind.json', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
								{fieldLabel: 'Initial Price', name: 'underlyingInitialPrice', xtype: 'floatfield'},
								{fieldLabel: 'Initial Quantity', name: 'underlyingInitialQuantity', xtype: 'currencyfield'}
							]
						}

					]
				}]
			},


			{

				title: 'Position Allocation',
				items: [{
					name: 'productHedgingAccountStrategyPositionListByStrategy',
					xtype: 'gridpanel',
					instructions: 'The following positions allocations are tied to this hedging strategy.  If quantity is not specified, the full amount is allocated.',
					columns: [
						{header: 'ID', width: 10, dataIndex: 'id', hidden: true},
						{header: 'TID', width: 10, dataIndex: 'accountingTransaction.id', hidden: true},
						{header: 'Position', width: 140, dataIndex: 'accountingTransaction.investmentSecurity.label'},
						{header: 'Total Quantity', width: 70, dataIndex: 'accountingTransaction.quantity', type: 'float'},
						{header: 'Allocation Quantity', width: 70, dataIndex: 'allocationQuantity', type: 'float', useNull: true}
					],
					getLoadParams: function() {
						const w = this.getWindow();
						return {
							strategyId: w.getMainFormId(),
							requestedMaxDepth: 5
						};
					},
					editor: {
						detailPageClass: 'Clifton.accounting.gl.TransactionWindow',
						deleteURL: 'productHedgingAccountStrategyPositionDelete.json',
						getDetailPageId: function(gridPanel, row) {
							return row.json.accountingTransaction.id;
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({
								emptyText: '< Select Position >', name: 'transactionId', url: 'accountingTransactionListFind.json', loadAll: false, width: 200, listWidth: 350,
								displayField: 'securityLabel',
								fields: ['id', 'description', 'quantity', {name: 'originalTransactionDate', type: 'date', dateFormat: 'Y-m-d H:i:s'}, {name: 'securityLabel', mapping: 'investmentSecurity.label'}],
								tpl: '<tpl for="."><div class="x-combo-list-item" ext:qtip="{description:htmlEncode} on {[values.originalTransactionDate.format(\"m/d/Y\")]}">{securityLabel}: {quantity} on {[values.originalTransactionDate.format(\"m/d/Y\")]}</div></tpl>',
								beforequery: function(queryEvent) {
									queryEvent.combo.store.baseParams = {
										clientInvestmentAccountId: TCG.getValue('clientInvestmentAccount.id', gridPanel.getWindow().getMainForm().formValues),
										positionAccountingAccount: true,
										opening: true,
										requestedPropertiesRoot: 'data',
										requestedProperties: 'id|description|investmentSecurity.label|quantity|originalTransactionDate'
									};
								},
								listeners: {
									select: function(combo, record, index) {
										combo.transactionIdValue = combo.getValue();
									}
								}
							}));
							toolBar.add(new TCG.form.CurrencyField({fieldLabel: 'Allocation Quantity', emptyText: 'Qty', name: 'quantity', width: 50}));


							toolBar.add({
								text: 'Add',
								tooltip: 'Add Selected Position',
								iconCls: 'add',
								handler: function() {
									const strategyId = gridPanel.getWindow().getMainFormId();
									const transId = TCG.getChildByName(toolBar, 'transactionId').transactionIdValue;
									if (TCG.isBlank(transId)) {
										TCG.showError('You must select an accounting transaction from the list.');
										return;
									}
									const qty = TCG.getChildByName(toolBar, 'quantity').getValue();
									if (qty !== 'Qty') {
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Adding...',
											params: {strategyId: strategyId, accountingTransactionId: transId, allocationQuantity: qty},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'transactionId').reset();
												TCG.getChildByName(toolBar, 'transactionId').transactionIdValue = '';
												TCG.getChildByName(toolBar, 'quantity').setValue('Qty');
											}
										});
										loader.load('productHedgingAccountStrategyPositionLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			}
		]
	}]
});
