Clifton.product.manager.ManagerBalanceProcessWindow = Ext.extend(TCG.app.Window, {
	id: 'investmentManagerBalanceProcessWindow',
	title: 'Investment Manager Balances',
	iconCls: 'manager',
	width: 1700,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Balance History',
				items: [{
					name: 'investmentManagerAccountBalanceListFind',
					xtype: 'gridpanel',
					reloadOnRender: false,
					rowSelectionModel: 'checkbox',
					instructions: 'Securities and cash balance history across all manager accounts that have already processed system defined adjustments.' +
						'<ul class="c-list">' +
						'<li>Adjusted balances are displayed in red and include all system and user defined adjustments that are not MOC adjustments.</li>' +
						'<li>If MOC (Market On Close) column is checked there are MOC adjustments that result in a different balance than the adjusted balance.  MOC is used when clients make a significant change (i.e. get/remove a lot of cash or move cash between managers) on a specific date and they want to change exposure right before the market closes to account for this.  There will be 2 or so day delay before we see this in manager download, so we use MOC adjustments to account for these changes.</li>' +
						'</ul>',
					importTableName: [
						{table: 'InvestmentManagerAccountBalanceAdjustment', label: 'Adjustments'},
						{table: 'InvestmentManagerAccountAllocation', label: 'Manager Allocations', importComponentName: 'Clifton.investment.manager.upload.AccountAllocationUploadWindow'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Client Acct Group', name: 'clientAccountGroupName', hiddenName: 'clientAccountGroupId', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json?accountTypeNameEqualsOrNull=Client', tooltip: 'Filters to those managers with assignments to the accounts in the selected group.'},
							{fieldLabel: 'Client', xtype: 'toolbar-combo', name: 'clientId', url: 'businessClientListFind.json', linkedFilter: 'managerAccount.client.name'},
							{fieldLabel: 'Balance Date', xtype: 'toolbar-datefield', name: 'balanceDate'}
						];
					},

					getLoadParams: function() {
						// default to yesterday
						const t = this.getTopToolbar();
						let dateValue;
						const bd = TCG.getChildByName(t, 'balanceDate');
						if (TCG.isNotBlank(bd.getValue())) {
							dateValue = (bd.getValue()).format('m/d/Y');
						}
						else {
							const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
							dateValue = prevBD.format('m/d/Y');
							bd.setValue(dateValue);
						}
						const params = {
							balanceDate: dateValue,
							readUncommittedRequested: true
						};
						const grp = TCG.getChildByName(t, 'clientAccountGroupName');
						if (TCG.isNotBlank(grp.getValue())) {
							params.clientInvestmentAccountGroupId = grp.getValue();
						}
						return params;
					},
					pageSize: 200,
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Client', width: 150, dataIndex: 'managerAccount.client.name', filter: {type: 'combo', searchFieldName: 'clientId', url: 'businessClientListFind.json'}, defaultSortColumn: true},
						{header: 'Manager', width: 150, dataIndex: 'managerAccount.managerCompany.name', filter: {searchFieldName: 'managerCompanyName'}},
						{header: 'Account', width: 100, dataIndex: 'managerAccount.labelShort', filter: {searchFieldName: 'accountNameNumber'}},
						{header: 'Custodian Account', width: 100, dataIndex: 'managerAccount.custodianLabel', filter: {type: 'combo', searchFieldName: 'custodianAccountId', displayField: 'label', url: 'investmentManagerCustodianAccountListFind.json'}},
						{header: 'CCY', width: 50, hidden: true, dataIndex: 'managerAccount.baseCurrency.symbol', filter: {type: 'combo', searchFieldName: 'baseCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
						{
							header: 'Type', dataIndex: 'managerAccount.managerType', width: 60,
							filter: {
								type: 'combo', searchFieldName: 'managerType', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.investment.manager.ManagerTypes
								})
							},
							renderer: function(v, p, r) {
								if (v === 'LINKED') {
									let qtip = '<table><tr><td>' + r.data['managerAccount.linkedManagerTypeLabel'] + '</td></tr>';
									if (TCG.isTrue(r.data['managerAccount.linkedManagerProcessingDependent'])) {
										qtip += '<tr><td>* Portfolio Processing Dependent</td></tr>';
									}
									qtip += '</table>';
									p.attr = 'qtip=\'' + qtip + '\'';
									p.css = 'amountAdjusted';
								}
								return v;
							}
						},
						{header: 'Linked - Portfolio dependency', dataIndex: 'managerAccount.linkedManagerProcessingDependent', type: 'boolean', hidden: true, sortable: false, filter: false},
						{
							header: 'Linked Type', hidden: true, dataIndex: 'managerAccount.linkedManagerTypeLabel', width: 40,
							filter: {
								type: 'combo', searchFieldName: 'linkedManagerType', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.investment.manager.LinkedManagerTypes
								})
							}
						},
						{header: 'Note', width: 100, dataIndex: 'note', hidden: true},
						{
							header: 'Violation Status', width: 110, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
							renderer: function(v, p, r) {
								return (Clifton.rule.violation.renderViolationStatus(v));
							}
						},

						// Boolean Flags for if the Balance was adjusted
						{header: 'Cash Adjusted', dataIndex: 'cashAdjustment', type: 'boolean', hidden: true, filter: false},
						{header: 'Securities Adjusted', dataIndex: 'securitiesAdjustment', type: 'boolean', hidden: true, filter: false},

						// Original Balance Info
						{header: 'Original Cash', dataIndex: 'cashValue', hidden: true, type: 'currency'},
						{header: 'Original Securities', dataIndex: 'securitiesValue', hidden: true, type: 'currency'},
						{header: 'Original Total', dataIndex: 'totalValue', hidden: true, type: 'currency'},

						// Adjusted Balance Info
						{
							header: 'Cash', width: 70, dataIndex: 'adjustedCashValue', type: 'currency',
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, r.data['cashAdjustment'], r.data['cashValue']);
							}
						},
						{
							header: 'Securities', width: 70, dataIndex: 'adjustedSecuritiesValue', type: 'currency',
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, r.data['securitiesAdjustment'], r.data['securitiesValue']);
							}
						},
						{
							header: 'Total', width: 70, dataIndex: 'adjustedTotalValue', type: 'currency', filter: false, sortable: false,
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, r.data['cashAdjustment'] || r.data['securitiesAdjustment'], r.data['totalValue']);
							}
						},

						// MOC Balance Info
						{header: 'MOC', width: 35, dataIndex: 'mocAdjustment', type: 'boolean', filter: false, sortable: false},
						{
							header: 'MOC Cash', width: 100, dataIndex: 'marketOnCloseCashValue', type: 'currency', hidden: true,
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, r.data['mocAdjustment'], r.data['adjustedCashValue']);
							}
						},
						{
							header: 'MOC Securities', width: 100, dataIndex: 'marketOnCloseSecuritiesValue', type: 'currency', hidden: true,
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, r.data['mocAdjustment'], r.data['adjustedSecuritiesValue']);
							}
						},
						{
							header: 'MOC Total', width: 100, dataIndex: 'marketOnCloseTotalValue', type: 'currency', hidden: true,
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, r.data['mocAdjustment'], r.data['adjustedTotalValue']);
							}
						}
					],
					processManagerBalances: function(balances, gridPanel, reloadBalance) {
						let url = 'productManagerAccountBalanceAdjustmentListForBalancesProcess.json';
						let msg = 'Reprocessing Manager Balance Adjustments';
						let confirmMsg = 'Are you sure you want to reprocess the selected balance(s) adjustments?';
						if (TCG.isTrue(reloadBalance)) {
							url = 'productManagerAccountBalanceListForBalancesProcess.json';
							msg = 'Reloading Manager Balances';
							confirmMsg = 'Are you sure you want to completely reload the selected balances?  Existing manual adjustments will be copied over to the new balance record.';
						}

						Ext.Msg.confirm(msg, confirmMsg, function(a) {
							if (a === 'yes') {
								const balIds = [];
								for (let i = 0; i < balances.length; i++) {
									balIds[i] = balances[i].id;
								}
								const loader = new TCG.data.JsonLoader({
									waitMsg: msg,
									waitTarget: gridPanel,
									params: {
										balanceIds: balIds
									},
									onFailure: function() {
										// If it failed, reload to current status
										gridPanel.reload();
									},
									onLoad: function(record, conf) {
										gridPanel.reload();
									}
								});
								loader.load(url);
							}
						});
					},
					editor: {
						detailPageClass: 'Clifton.investment.manager.BalanceWindow',
						drillDownOnly: true,

						addEditButtons: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Reprocess Adjustments',
								tooltip: 'Reprocess Adjustments For Selected Balances (Overwrites Existing Data)',
								iconCls: 'run',
								scope: this,
								timeout: 30000,
								handler: function() {
									const sm = this.grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select at least one balance to process.', 'No Balance(s) Selected');
									}
									else {
										const ut = sm.getSelections();
										gridPanel.processManagerBalances(ut, gridPanel, false);
									}
								}
							}, '-');

							toolBar.add({
								text: 'Reload Balance',
								tooltip: 'Reloads the Selected Balances (Overwrites Existing Data).  If the selected balance is for a Rollup Manager the Rollup Manager Balance will be reloaded.',
								iconCls: 'run',
								scope: this,
								timeout: 30000,
								handler: function() {
									const sm = this.grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select at least one balance to process.', 'No Balance(s) Selected');
									}
									else {
										const ut = sm.getSelections();
										gridPanel.processManagerBalances(ut, gridPanel, true);
									}
								}
							}, '-');
						}
					}
				}]
			},


			{
				title: 'Missing Balances',
				items: [{
					name: 'investmentManagerAccountMissingBalanceListFind',
					xtype: 'gridpanel',
					instructions: 'The following investment manager accounts have security and cash balances missing on the specified date.',
					pageSize: 200,
					limitRequestedProperties: true,
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Client', xtype: 'toolbar-combo', name: 'clientId', width: 200, url: 'businessClientListFind.json', linkedFilter: 'client.name'},
							{fieldLabel: 'Balance Date', xtype: 'toolbar-datefield', name: 'balanceDate'}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to active
							this.setFilterValue('inactive', false);
						}
						// default to yesterday
						const t = this.getTopToolbar();
						let dateValue;
						const bd = TCG.getChildByName(t, 'balanceDate');
						if (TCG.isNotBlank(bd.getValue())) {
							dateValue = (bd.getValue()).format('m/d/Y');
						}
						else {
							const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
							dateValue = prevBD.format('m/d/Y');
							bd.setValue(dateValue);
						}
						return {
							balanceDate: dateValue,
							requestedMaxDepth: 4
						};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Client', width: 100, dataIndex: 'client.name', filter: {type: 'combo', searchFieldName: 'clientId', url: 'businessClientListFind.json'}, defaultSortColumn: true},
						{header: 'Manager', width: 100, dataIndex: 'managerCompany.name', filter: {searchFieldName: 'managerCompanyName'}},
						{header: 'Account', width: 100, dataIndex: 'labelShort', filter: {searchFieldName: 'accountNameNumber'}},
						{header: 'Custodian Account', width: 100, dataIndex: 'custodianLabel', filter: {type: 'combo', searchFieldName: 'custodianCompanyId', displayField: 'label', url: 'businessCompanyListFind.json?companyType=Custodian'}},
						{
							header: 'Manager Type', dataIndex: 'managerType', width: 40,
							filter: {
								type: 'combo', searchFieldName: 'managerType', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.investment.manager.ManagerTypes
								})
							},
							renderer: function(v, p, r) {
								if (v === 'LINKED') {
									let qtip = '<table><tr><td>' + r.data['linkedManagerTypeLabel'] + '</td></tr>';
									if (TCG.isTrue(r.data['linkedManagerProcessingDependent'])) {
										qtip += '<tr><td>* Portfolio Processing Dependent</td></tr>';
									}
									qtip += '</table>';
									p.attr = 'qtip=\'' + qtip + '\'';
									p.css = 'amountAdjusted';
								}
								return v;
							}
						},
						{header: 'Linked - Portfolio dependency', dataIndex: 'linkedManagerProcessingDependent', type: 'boolean', hidden: true, sortable: false, filter: false},
						{
							header: 'Linked Type', hidden: true, dataIndex: 'linkedManagerTypeLabel', width: 40,
							filter: {
								type: 'combo', searchFieldName: 'linkedManagerType', displayField: 'name', valueField: 'value', mode: 'local',
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.investment.manager.LinkedManagerTypes
								})
							}
						},
						{header: 'Inactive', dataIndex: 'inactive', width: 30, type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.manager.AccountWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Stale Balance Summary',
				items: [{
					xtype: 'investment-manager-staleBalanceSummaryGrid'
				}]
			},


			{
				title: 'Administration',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						labelWidth: 180,
						loadValidation: false, // using the form only to get background color/padding
						height: 290,
						buttonAlign: 'right',
						instructions: 'Select criteria below to filter which manager accounts to process. Processing will create/update corresponding manager account cash and security balances on the specified date based on the rules defined for manager\'s account.',
						listeners: {
							afterrender: function(fp) {
								const f = this.getForm();
								const bd = f.findField('balanceDate');
								if (TCG.isBlank(bd.getValue())) {
									const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
									bd.setValue(prevBD.format('m/d/Y'));
								}
							}
						},
						items: [{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .68,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{
												fieldLabel: 'Client Group', name: 'groupName', hiddenName: 'investmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['clientId'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow',
												qtip: 'Client group is an Account Group selection, however it applies to managers that are assigned to the <b>client</b> of each account.  It is NOT account specific.'
											},
											{fieldLabel: 'Client', name: 'clientName', hiddenName: 'clientId', xtype: 'combo', loadAll: false, url: 'businessClientListFind.json', mutuallyExclusiveFields: ['investmentAccountGroupId']},
											{
												fieldLabel: 'Custodian', name: 'custodianCompanyName', hiddenName: 'custodianCompanyId', xtype: 'combo', displayField: 'label', url: 'businessCompanyListFind.json',
												beforequery: function(queryEvent) {
													queryEvent.combo.store.baseParams = {companyTypeNames: 'Custodian,Investment Manager,Broker,Other Issuer'};
												}
											},
											{fieldLabel: 'Balance Date', xtype: 'datefield', name: 'balanceDate', allowBlank: false},
											{boxLabel: 'If data already has been processed, reprocess and overwrite.', name: 'reload', xtype: 'checkbox', labelSeparator: ''},
											{boxLabel: 'Immediately process adjustments for loaded balances.', name: 'loadProcessAdjustments', xtype: 'checkbox', labelSeparator: '', checked: true},
											// Added because sometimes analysts don't close their browser at night and the next day they process manager balances for 2 days ago, instead of previous business day
											{boxLabel: 'Allow processing for previous business day only (uncheck to process other dates)', name: 'prevOnly', xtype: 'checkbox', checked: true},
											{boxLabel: 'Restrict processing to require Client, Account Group, or Custodian (uncheck to allow all)', name: 'restrictionRequired', xtype: 'checkbox', checked: true, qtip: '<b>Warning:</b> Should be unchecked ONLY when you want to load/process manager balances across all Clients.'},
											{xtype: 'hidden', name: 'processingType'}
										]
									}]
								},
								{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
								{
									columnWidth: .30,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 0,
										items: [
											{
												xtype: 'fieldset', title: 'Manager Types Filter', collapsible: false, layout: 'fit',
												instructions: 'Select which manager types you would like to process.',
												items: [{
													xtype: 'radiogroup', columns: 1,
													items: [
														{boxLabel: 'All', name: 'managerType', inputValue: '', checked: true, qtip: 'Proxy, Standard, and Linked)'},
														{boxLabel: 'Proxy', name: 'managerType', inputValue: 'PROXY'},
														{boxLabel: 'Standard', name: 'managerType', inputValue: 'STANDARD', qtip: 'Applies to managers that usually just load a custodian balance from an external source.'},
														{boxLabel: 'Linked', name: 'managerType', inputValue: 'LINKED', qtip: 'Applies to Linked Managers whose balance does not dependent on processing of another Portfolio run.  Portfolio dependent manager balances are automatically loaded for you during the run.'}
													]
												}]
											}
										]
									}]
								}
							]
						}],

						validatePreviousOnly: function() {
							const form = this.getForm();
							const prevOnly = form.findField('prevOnly').getValue();
							if (TCG.isTrue(prevOnly)) {
								// Look it up forcing it to skip the cache to ensure it gets current value
								const prevBD = Clifton.calendar.getBusinessDayFrom(-1, true);
								const balDate = form.findField('balanceDate').value;
								if (TCG.isNotEquals(prevBD.format('m/d/Y'), balDate)) {
									TCG.showError('You have selected to allow processing for previous business day only, however the balance date is not the previous business day.  Either uncheck the previous business day checkbox or enter balance date ' + prevBD.format('m/d/Y'));
									return false;
								}
							}
							return true;
						},

						validateLoadAll: function() {
							const form = this.getForm();
							const restrictionRequired = form.findField('restrictionRequired').getValue();
							if (TCG.isTrue(restrictionRequired)) {
								// All is Not Allowed - Must have Client, Account Group, or Custodian selected
								const cl = form.findField('clientId').value;
								const ag = form.findField('investmentAccountGroupId').value;
								const cus = form.findField('custodianCompanyName').value;
								if (TCG.isNotBlank(cl) || TCG.isNotBlank(ag) || TCG.isNotBlank(cus)) {
									return true;
								}
								TCG.showError('You have selected to restrict processing to a Client, Account Group, or Custodian.  Either uncheck the restriction checkbox or select at least one of the filters.');
								return false;
							}
							return true;

						},

						setLoadProcessingType: function() {
							const form = this.getForm();
							const isReload = form.findField('reload').getValue();
							const isAdj = form.findField('loadProcessAdjustments').getValue();
							let ptype = 'LOAD';
							if (TCG.isTrue(isReload)) {
								if (TCG.isTrue(isAdj)) {
									ptype = 'RELOAD_PROCESS';
								}
								else {
									ptype = 'RELOAD';
								}
							}
							else if (TCG.isTrue(isAdj)) {
								ptype = 'LOAD_PROCESS';
							}
							form.findField('processingType').setValue(ptype);

						},

						buttons: [
							{
								text: 'Load Balances',
								iconCls: 'run',
								width: 150,
								handler: function() {
									const owner = this.findParentByType('formpanel');
									let allowProcessing = owner.validatePreviousOnly();
									if (allowProcessing === true) {
										allowProcessing = owner.validateLoadAll();
									}
									if (allowProcessing === true) {
										owner.setLoadProcessingType();
										const form = owner.getForm();
										form.submit(Ext.applyIf({
											url: 'productManagerAccountBalanceListProcess.json',
											waitMsg: 'Loading...',
											success: function(form, action) {
												Ext.Msg.alert('Load Processing', action.result.result, function() {
													const grid = owner.ownerCt.items.get(1);
													grid.reloadRepeated(); // uses defaults of every 3s for 30s
												});
											}
										}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
									}
								}
							},
							{
								text: 'Process Adjustments',
								iconCls: 'run',
								width: 150,
								handler: function() {
									const owner = this.findParentByType('formpanel');
									let allowProcessing = owner.validatePreviousOnly();
									if (allowProcessing === true) {
										allowProcessing = owner.validateLoadAll();
									}
									if (allowProcessing === true) {
										const form = owner.getForm();
										form.submit(Ext.applyIf({
											url: 'productManagerAccountBalanceAdjustmentListProcess.json',
											waitMsg: 'Processing...',
											success: function(form, action) {
												Ext.Msg.alert('Adjustments Processing', action.result.result, function() {
													const grid = owner.ownerCt.items.get(1);
													grid.reloadRepeated(); // uses defaults of every 3s for 30s
												});
											}
										}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
									}
								}
							}]
					},
					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'PRODUCT-MANAGER',
						instantRunner: true,
						instructions: 'The following Manager Balances are being (re)loaded and/or (re)processed right now.',
						title: 'Current Manager Balance Processing',
						flex: 1
					}
				]
			},


			{
				title: 'External Loads',
				items: [{
					xtype: 'integration-importRunEventGrid',
					targetApplicationName: 'Integration',
					importEventName: 'Manager Download'
				}]
			},


			{
				title: 'Import Groups',
				items: [{
					xtype: 'integration-assetClassGroupGrid'
				}]
			},


			{
				title: 'Import Definitions',
				items: [{
					xtype: 'integration-importDefinitionGrid',
					definitionTypeName: 'Manager Download'
				}]
			}
		]
	}]
});

