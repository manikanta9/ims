TCG.use('Clifton.investment.manager.BaseBalanceWindow');

Clifton.product.manager.BalanceWindow = Ext.extend(Clifton.investment.manager.BaseBalanceWindow, {

	// Add Process Adjustments Button to Adjustments FormGrid
	addFormGridButtons: function(gp, toolBar) {
		toolBar.add({
			text: 'Process Adjustments',
			tooltip: 'Process/Reprocess Balance for System Defined Adjustments.',
			iconCls: 'run',
			handler: function() {
				const panel = TCG.getParentFormPanel(gp);
				const w = panel.getWindow();
				if (w.isModified() || !w.isMainFormSaved()) {
					TCG.showError('Please save your changes before processing adjustments.');
				}
				else {
					panel.saveURL = 'productManagerAccountBalanceAdjustmentsForBalanceProcess.json';
					gp.getWindow().saveWindow(false, true);
					// Reset Save Method
					panel.saveURL = 'investmentManagerAccountBalanceSave.json';
				}
			}
		});
		toolBar.add('-');
	}
});
