Ext.ns('Clifton.product', 'Clifton.product.manager', 'Clifton.product.overlay', 'Clifton.product.overlay.assetclass', 'Clifton.product.overlay.manager', 'Clifton.product.overlay.rebalance', 'Clifton.product.overlay.trade', 'Clifton.product.overlay.trade.group', 'Clifton.product.overlay.trade.group.option', 'Clifton.product.hedging', 'Clifton.product.ldi', 'Clifton.product.durationmanagement', 'Clifton.product.performance', 'Clifton.product.performance.upload', 'Clifton.product.performance.overlay');

// UI Overrides for other Projects
Clifton.portfolio.run.PortfolioRunWindowOverrides['PORTFOLIO_RUNS'] = 'Clifton.product.overlay.RunWindow';
Clifton.portfolio.run.PortfolioRunWindowOverrides['DEFAULT'] = 'Clifton.product.overlay.RunWindow';

Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides['PORTFOLIO_RUNS'] = {
	className: 'Clifton.product.overlay.trade.RunTradeOverlayWindow',
	getEntity: function(config, entity) {
		const requestedProperties = 'id|overlayAssetClass.accountAssetClass.id|labelLong|tradeEntryDisabled|securityNotTradable|cashExposure|tradingClientAccount.id|overlayAssetClass.label|overlayAssetClass.benchmarkDuration|overlayAssetClass.benchmarkCreditDuration|overlayAssetClass.effectiveDuration|overlayAssetClass.rebalanceTriggerWarningsUsed|overlayAssetClass.rebalanceTriggerMin|overlayAssetClass.rebalanceTriggerMax|overlayAssetClass.rebalanceTriggerAbsoluteMin|overlayAssetClass.rebalanceTriggerAbsoluteMax|overlayAssetClass.rebalanceExposureTarget|overlayAssetClass.actualAllocationAdjusted|replication.name|replicationType.currency|replicationType.durationSupported|security.id|security.symbol|security.endDate|security.description|securityPrice|tradeSecurityPrice|tradeSecurityPriceDate|underlyingSecurityPrice|tradeUnderlyingSecurityPrice|tradeUnderlyingSecurityPriceDate|securityDirtyPrice|tradeSecurityDirtyPrice|tradeSecurityDirtyPriceDate|exchangeRate|tradeExchangeRate|tradeExchangeRateDate|value|tradeValue|additionalExposure|additionalOverlayExposure|totalAdditionalExposure|targetExposureAdjusted|actualContracts|actualContractsAdjusted|virtualContracts|currentContracts|currentVirtualContracts|currentContractsAdjusted|pendingContracts|pendingVirtualContracts|pendingContractsAdjusted|buyContracts|sellContracts|trade.tradeDestination.id|trade.tradeDestination.name|trade.tradeExecutionType.id|trade.tradeExecutionType.name|trade.executingBrokerCompany.id|trade.executingBrokerCompany.label|trade.holdingInvestmentAccount.id|trade.holdingInvestmentAccount.label|duration|delta|contractValuePriceField|contractValuePrice|security.priceMultiplier|markToMarketAdjustmentValue|targetExposureAdjustedWithMarkToMarket|currencyActualLocalAmount|currencyExchangeRate|currencyOtherBaseAmount|currencyCurrentLocalAmount|tradeCurrencyExchangeRate|currencyPendingLocalAmount|currencyCurrentOtherBaseAmount|currencyPendingOtherBaseAmount|currencyCurrentTotalBaseAmount|currencyPendingTotalBaseAmount|matchingReplication|currencyUnrealizedLocalAmount|currencyCurrentUnrealizedLocalAmount|currencyCurrentUnrealizedTradeBaseAmount|currencyDenominationBaseAmount|currencyCurrentDenominationBaseAmount|currencyPendingDenominationBaseAmount|allocationWeightAdjusted|reverseExposureSign|rebalanceTriggerMin|rebalanceTriggerMax|security.instrument.fairValueAdjustmentUsed|previousFairValueAdjustment|fairValueAdjustment|tradeFairValueAdjustment|tradeFairValueAdjustmentDate|mispricingValue|mispricingTradeValue|security.instrument.hierarchy.investmentType.name';
		const requestedPropertiesRoot = 'data.detailList';
		const url = 'portfolioRunTrade.json?requestedMaxDepth=7';

		const newEntity = TCG.data.getData(url + '&id=' + entity.id + '&requestedProperties=' + requestedProperties + '&requestedPropertiesRoot=' + requestedPropertiesRoot, this);
		newEntity.defaultView = entity.defaultView;
		return newEntity;
	}
};
Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides['DEFAULT'] = Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides['PORTFOLIO_RUNS'];

Clifton.investment.account.CliftonAccountPortfolioTabs['HEDGING'].push(
		{
			title: 'Hedge Strategies',
			items: [{
				name: 'productHedgingAccountStrategyListFind',
				xtype: 'gridpanel',
				instructions: 'The following hedging strategies are associated with this client account.',
				getLoadParams: function() {
					return {
						clientInvestmentAccountId: this.getWindow().getMainFormId()
					};
				},
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Name', width: 150, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
					{header: 'Structure', width: 150, dataIndex: 'strategyStructure'},
					{header: 'Notional Multiplier', width: 70, dataIndex: 'strategyNotionalMultiplier', type: 'float', hidden: true},
					{header: 'Description', width: 150, dataIndex: 'description', hidden: true},
					{header: 'Underlying Security', width: 100, dataIndex: 'underlyingSecurity.label', filter: false},
					{header: 'Initial Price', width: 70, dataIndex: 'underlyingInitialPrice', type: 'float'},
					{header: 'Initial Qty', width: 70, dataIndex: 'underlyingInitialQuantity', type: 'currency'},
					{header: 'Start Date', width: 70, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
					{header: 'End Date', width: 70, dataIndex: 'endDate'},
					{header: 'Description', width: 200, dataIndex: 'description', hidden: true}
				],
				editor: {
					detailPageClass: 'Clifton.product.hedging.AccountStrategyWindow',
					getDefaultData: function(gridPanel) { // defaults client account for the detail page
						return {
							clientInvestmentAccount: gridPanel.getWindow().getMainForm().formValues
						};
					}
				}
			}]
		}
);

Clifton.investment.account.CliftonAccountPortfolioTabs['DURATION_MANAGEMENT'].push(
		{
			title: 'Duration Setup',
			tbar: [{
				text: 'Duration Setup',
				tooltip: 'Configure Duration Management specific fields',
				iconCls: 'account',
				handler: function() {
					const f = this.ownerCt.ownerCt.items.get(0);
					const accountId = f.getWindow().getMainFormId();
					Clifton.investment.account.openCliftonAccountCustomFieldWindow('Duration Management', accountId);
				}
			}],
			items: [{
				xtype: 'formpanel',
				instructions: 'Use the Duration Setup button to configure Duration management for this client account.'
			}]
		}
);

Clifton.investment.account.CliftonAccountPortfolioTabs['LDI'].push(
		{
			title: 'Maturities',
			items: [{
				name: 'productLDIMaturityExtendedListFind',
				xtype: 'gridpanel',
				instructions: 'The following maturities are assigned to this investment account, with the most recent balance/DV01 values entered prior to the selected balance date.  To enter balances for a specific date, enter the date at the right and click the Balance Entry button.',
				getTopToolbarFilters: function(toolBar) {
					const grid = this;
					toolBar.add({
						text: 'LDI Setup',
						tooltip: 'Configure LDI specific fields',
						iconCls: 'account',
						handler: function() {
							const accountId = grid.getWindow().getMainFormId();
							Clifton.investment.account.openCliftonAccountCustomFieldWindow('LDI', accountId);
						}
					});
					toolBar.add('-');

					return [
						{fieldLabel: 'Balance Date', name: 'balanceDate', width: 80, xtype: 'datefield'}, '-',
						{
							text: 'Balance Entry',
							xtype: 'button',
							iconCls: 'pencil',
							handler: function() {
								grid.handleBalanceEntry();
							}
						},
						{
							text: 'Remove Balance Entry',
							xtype: 'button',
							iconCls: 'remove',
							handler: function() {
								grid.handleDeleteBalanceEntry();
							}
						}
					];
				},
				handleBalanceEntry: function() {
					const clz = 'Clifton.product.ldi.BalanceEntryWindow';
					TCG.createComponent(clz, {
						params: this.getLoadParams(),
						openerCt: this
					});
				},

				handleDeleteBalanceEntry: function() {
					const grid = this;
					Ext.Msg.confirm('Delete Balance Entry', 'Are you sure you want to delete all balance entries for the selected date?', function(a) {
						if (a === 'yes') {
							const loader = new TCG.data.JsonLoader({
								waitTarget: grid,
								waitMsg: 'Removing...',
								params: grid.getLoadParams(),
								onLoad: function(record, conf) {
									grid.reload();
								}
							});
							loader.load('productLDIBalanceEntryDelete.json');
						}
					});
				},

				getLoadParams: function(firstLoad) {
					let dateValue;
					const t = this.getTopToolbar();
					const bd = TCG.getChildByName(t, 'balanceDate');
					if (bd.getValue() !== '') {
						dateValue = (bd.getValue()).format('m/d/Y');
					}
					else {
						const bD = Clifton.calendar.getBusinessDayFrom(0);
						dateValue = bD.format('m/d/Y');
						bd.setValue(dateValue);
					}
					return {
						clientInvestmentAccountId: this.getWindow().getMainFormId(),
						balanceDate: dateValue
					};
				},
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Maturity', width: 80, dataIndex: 'label'},
					{header: 'Min Days', width: 70, dataIndex: 'periodMinDays', type: 'int', hidden: true},
					{header: 'Max Days', width: 70, dataIndex: 'periodMinDays', type: 'int', hidden: true},
					{header: 'Replication', dataIndex: 'replication.name', width: 140, filter: {type: 'combo', searchFieldName: 'replicationId', url: 'investmentReplicationListFind.json'}},
					{header: 'Balance Date', dataIndex: 'balance.balanceDate', width: 100, type: 'date', filter: false, hidden: true},
					{header: 'Assets Balance', dataIndex: 'balance.assetsBalance', width: 100, type: 'currency', filter: false, useNull: true},
					{header: 'Assets DV01', dataIndex: 'balance.assetsDV01', width: 95, type: 'currency', filter: false, useNull: true},
					{header: 'Liabilities Balance', dataIndex: 'balance.liabilitiesBalance', width: 100, type: 'currency', filter: false, useNull: true},
					{header: 'Liabilities DV01', dataIndex: 'balance.liabilitiesDV01', width: 95, type: 'currency', filter: false, useNull: true}
				],
				editor: {
					detailPageClass: 'Clifton.product.ldi.MaturityWindow',
					deleteURL: 'productLDIMaturityDelete.json',
					getDefaultData: function(gridPanel) { // defaults client account for the detail page
						return {
							clientInvestmentAccount: gridPanel.getWindow().getMainForm().formValues
						};
					}
				}
			}]
		}
);

Clifton.investment.account.CliftonAccountPortfolioTabs['PORTFOLIO_RUNS'].push(
		{
			title: 'Rebalancing',
			layout: 'border',
			reloadOnRender: true,
			items: [
				{
					region: 'north',
					height: 310,
					xtype: 'formpanel',
					labelWidth: 130,
					controlWindowModified: false,
					trackResetOnLoad: true,
					buttons: [{
						xtype: 'button',
						text: 'Save',
						handler: function() {
							const owner = this.findParentByType('formpanel');
							owner.saveForm();
						}
					}],
					listeners: {
						afterRender: function() {
							this.reload();
						}
					},
					getAccountId: function() {
						const w = this.getWindow();
						return w.getMainFormId();
					},
					reload: function() {
						this.getForm().setValues(TCG.data.getData('investmentAccountRebalance.json?requestedMaxDepth=4&id=' + this.getAccountId(), this), true);
					},

					openMinimizeImbalancesAdvancedSettingsWindow: function() {
						if (this.getForm().findField('minimizeImbalancesCalculatorBean.id').isDirty()) {
							TCG.showError('Please save your changes before opening minimize imbalances advanced settings.');
						}
						else {
							const columnGroupName = TCG.data.getData('productOverlayMinimizeImbalancesSettingsColumnGroupName.json?investmentAccountId=' + this.getAccountId(), this);
							if (TCG.isBlank(columnGroupName)) {
								TCG.showError('Advanced Settings are not available for selected calculator.');
								return;
							}
							const accountId = this.getAccountId();
							const className = 'Clifton.product.overlay.rebalance.ProductOverlayMinimizeImbalancesSettingsWindow';
							const cmpId = TCG.getComponentId(className, accountId);
							TCG.createComponent(className, {
								id: cmpId,
								params: {investmentAccountId: accountId},
								openerCt: this
							});
						}
					},

					saveForm: function() {
						const panel = this;
						const form = panel.getForm();
						const win = this.getWindow();

						// get the submit parameters
						let params = {};
						if (panel.getSubmitParams) {
							const returnParams = panel.getSubmitParams();
							if (returnParams) {
								params = returnParams;
							}
						}
						params.id = this.getAccountId();
						form.trackResetOnLoad = true;
						form.submit(Ext.applyIf({
							url: 'investmentAccountRebalanceSaveConfig.json?enableValidatingBinding=true',
							params: params,
							waitMsg: 'Saving...',
							success: function(form, action) {
								// Window Modified is still getting triggered even without apply button being enabled/disabled
								win.setModified(false);
								panel.reload();
							}
						}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
					},


					items: [
						{
							xtype: 'fieldset-checkbox', title: 'Mini-Rebalancing Options',
							checkboxName: 'rebalancingUsed',
							instructions: 'Benchmark adjusted Rebalance Cash amounts are summed across asset classes during each Portfolio run. If Min/Max Rebalance Cash total is defined here, then the sum will be compared against these thresholds to check if mini-rebalancing is needed.',
							items: [
								{
									fieldLabel: 'Min Rebalance Cash', xtype: 'compositefield',
									defaults: {
										flex: 1
									},
									items: [
										{name: 'minAllowedCash', xtype: 'currencyfield'},
										{xtype: 'label'},
										{xtype: 'label', html: 'Max Rebalance Cash:'},
										{name: 'maxAllowedCash', xtype: 'currencyfield'}
									]
								},
								{
									fieldLabel: 'Calculator', name: 'calculationTypeLabel', hiddenName: 'calculationType', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
									store: new Ext.data.ArrayStore({
										fields: ['name', 'value', 'description'],
										data: Clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes
									})
								},
								{
									boxLabel: 'Automatically perform rebalancing when thresholds are crossed', name: 'automatic', xtype: 'checkbox',
									qtip: 'During Portfolio run processing, when Rebalance Cash Adjusted values are calculated across all asset classes and the sum crossed the defined threshold the system will automatically perform a mini-rebalance using the selected calculation type.  If not automatic, a warning will be generated and the ability to manually execute the mini rebalance will be allowed.'
								}
							]
						},
						{
							xtype: 'fieldset-checkbox', title: 'Trading/Minimize Imbalances Options',
							checkboxName: 'minimizeImbalancesUsed',
							items: [
								{xtype: 'label', html: 'Only Trade if (Effective Cash - Overlay Exposure) is outside of the following bands.  If within these bands, do not trade and set all Overlay Targets = Overlay Exposure. (If a minimize imbalances calculator is used, that cash will be applied to set these targets.  Otherwise Client Directed Cash bucket will be used.)'},
								{
									fieldLabel: 'Min Effective Cash', xtype: 'compositefield',
									defaults: {
										flex: 1
									},
									items: [
										{name: 'minEffectiveCashExposure', xtype: 'currencyfield'},
										{xtype: 'label'},
										{xtype: 'label', html: 'Max Effective Cash:'},
										{name: 'maxEffectiveCashExposure', xtype: 'currencyfield'}
									]
								},
								{xtype: 'label', html: '<hr/>If Trading, manager cash can be overlaid to minimize imbalances.  How this cash is overlaid is determined by the selected calculator and options. Note: Full Rebalancing may also apply cash differently based on the selected calculator. Cash Adjustment adds/removes cash applied to minimize imbalances. Use negative amounts to reduce the amount, i.e. apply a cash buffer. A (*) in the calculator name indicates Advanced Settings are available for that calculator.'},
								{
									fieldLabel: 'Calculator', xtype: 'panel', layout: 'hbox',
									defaults: {
										flex: 1
									},
									items: [
										{beanName: 'minimizeImbalancesCalculatorBean', xtype: 'system-bean-combo', flex: 1, groupName: 'Portfolio Minimize Imbalances Calculator'},
										{
											xtype: 'button', iconCls: 'config', width: 25, tooltip: 'Click to enter advanced settings applicable to selected calculator and this account\'s asset classes.',
											handler: function() {
												const form = TCG.getParentFormPanel(this.ownerCt);
												form.openMinimizeImbalancesAdvancedSettingsWindow();
											}
										}
									]
								},
								{fieldLabel: 'Cash Adjustment', name: 'minimizeImbalancesAdjustmentAmount', xtype: 'currencyfield', requiredFields: ['minimizeImbalancesCalculatorBean.id']},
								{
									boxLabel: 'Reduce overlay cash by cash asset class adjusted target', xtype: 'checkbox', name: 'minimizeImbalancesAdjustForCashTarget', requiredFields: ['minimizeImbalancesCalculatorBean.id'],
									qtip: 'When adjusted target is negative, this adjustment may not be applied.  See the minimize imbalances calculator options for the ability to turn this on or off for negative cash adjusted targets.'
								},
								{xtype: 'hidden', name: 'minimizeImbalancesAdvancedSettingsScreenClass', submitValue: false}
							]
						}
					]
				},

				{
					region: 'center',
					xtype: 'gridpanel',
					title: 'Rebalance History',
					name: 'investmentAccountRebalanceHistoryListFind',
					wikiPage: 'IT/Investment Rebalancing',
					instructions: 'Rebalancing History is recorded at the account level every time a rebalance occurs. Drill into a history record to see the asset class level historical details',
					columns: [
						{header: 'Date', dataIndex: 'rebalanceDate', width: 45},
						{header: 'Calculation Type', dataIndex: 'rebalanceCalculationTypeLabel', width: 75},
						{header: 'Before Rebalance Cash Total', dataIndex: 'beforeRebalanceCashTotal', width: 65, type: 'currency'},
						{header: 'After Rebalance Cash Total', dataIndex: 'afterRebalanceCashTotal', width: 65, type: 'currency'},
						{header: 'Description', dataIndex: 'description', width: 125},
						{header: 'Create Date', width: 70, dataIndex: 'createDate'}
					],

					editor: {
						detailPageClass: 'Clifton.investment.account.assetclass.rebalance.HistoryWindow',
						drillDownOnly: true
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('rebalanceDate', {'after': new Date().add(Date.DAY, -90)});
						}

						const w = this.getWindow();
						if (w.isMainFormSaved()) {
							return {investmentAccountId: w.getMainFormId()};
						}
						return false;
					}
				}
			]
		}
);
Clifton.investment.manager.BalanceWindowOverride = 'Clifton.product.manager.BalanceWindow';


// UI Overrides for performance windows
Clifton.performance.account.PerformanceAccountWindowOverrides['PORTFOLIO_RUNS'] = 'Clifton.product.performance.SummaryWindow';
Clifton.performance.account.PerformanceAccountWindowOverrides['LDI'] = 'Clifton.product.performance.SummaryWindow';

Clifton.performance.account.portfolio.PerformanceRunWindowOverrides['DEFAULT'] = 'Clifton.product.performance.overlay.RunWindow';
Clifton.performance.account.portfolio.PerformanceRunWindowOverrides['PORTFOLIO_RUNS'] = Clifton.performance.account.portfolio.PerformanceRunWindowOverrides['DEFAULT'];

Clifton.product.overlay.rebalance.MinimizeImbalancesDeviationDenominatorOptions = [
	['Target', 'Target', 'Deviation As Percentage of Adjusted Target'],
	['Rebalance Trigger', 'Rebalance Trigger', 'Deviation As Percentage of Rebalance Trigger (Min or Max depends on if under/over weight']
];
Clifton.product.overlay.ExposureSummaryViewNames = ['Overlay', 'Fully Funded', 'Benchmark Returns'];
Clifton.product.overlay.trade.TradeViewNames = ['Overlay', 'Fully Funded', 'Structured Options'];

Clifton.product.overlay.IncompleteRunsGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'systemQueryResult',
	queryName: 'Missing or Incomplete Portfolio Runs',
	instructions: 'A list of Portfolio Runs that are missing or have not been completed for selected Balance Date. Uses "Missing or Incomplete Portfolio Runs" system query for Active client accounts excluding "Portfolio Run Client Accounts - EXCLUDED" client account group.',
	dataTable: true,
	storeRoot: 'dataTable.rows',
	pageSize: 1000,
	columns: [
		{header: 'Account ID', dataIndex: 'accountId', type: 'int', width: 40, doNotFormat: true, hidden: true},
		{header: 'Run ID', dataIndex: 'runId', type: 'int', width: 40, doNotFormat: true, useNull: true, hidden: true},
		{header: 'Event ID', dataIndex: 'eventId', type: 'int', width: 40, doNotFormat: true, useNull: true, hidden: true},
		{header: 'Client Account', dataIndex: 'clientAccount', width: 160},
		{header: 'Team', dataIndex: 'team', width: 60},
		{header: 'Positions on Date', dataIndex: 'positionsOnDate', type: 'date', width: 70, hidden: true},
		{header: 'Main', dataIndex: 'main', type: 'boolean', width: 30},
		{header: 'MOC', dataIndex: 'moc', type: 'boolean', width: 30},
		{
			header: 'Violation Status', dataIndex: 'violationStatus', width: 80,
			renderer: function(v, p, r) {
				return (Clifton.rule.violation.renderViolationStatus(v));
			}
		},
		{header: 'Run Status', dataIndex: 'runStatus', width: 200},
		{header: 'Updated By', dataIndex: 'updatedBy', width: 50},
		{header: 'Updated At', dataIndex: 'updatedAt', width: 45},
		{header: 'Cash Exp %', dataIndex: 'cashExpPercent', type: 'currency', useNull: true, width: 55}
	],
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Team', name: 'teamSecurityGroupId', width: 120, xtype: 'toolbar-combo', url: 'securityGroupTeamList.json', loadAll: true},
			{fieldLabel: 'Client Acct Group', name: 'clientAccountGroupId', width: 180, xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow', disableAddNewItem: true},
			{fieldLabel: 'Balance Date', xtype: 'toolbar-datefield', name: 'balanceDate', value: Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y')}
		];
	},
	editor: {
		drillDownOnly: true,
		getDetailPageClass: function(grid, row) {
			return row.data.runId ? 'Clifton.portfolio.run.RunWindow' : (row.data.eventId ? 'Clifton.investment.calendar.EventWindow' : 'Clifton.investment.account.CliftonAccountWindow');
		},
		getDetailPageId: function(gridPanel, row) {
			return row.data.runId || row.data.eventId || row.data.accountId;
		}
	},
	getLoadParams: async function(firstLoad) {
		const gp = this;
		const t = gp.getTopToolbar();
		if (firstLoad) {
			const team = await Clifton.security.user.getUserTeam(gp);
			if (TCG.isNotNull(team)) {
				TCG.getChildByName(t, 'teamSecurityGroupId').setValue({value: team.id, text: team.name});
			}
		}

		let balanceDate = TCG.getChildByName(t, 'balanceDate');
		if (balanceDate.getValue() === '') {
			TCG.showError('Please select required filter: Balance Date');
			return false;
		}
		balanceDate = balanceDate.getValue().format('m/d/Y');

		const params = {};
		const loadingPromise = TCG.data.getDataPromiseUsingCaching('systemQueryByName.json?name=' + this.queryName, gp, 'system.query.' + this.queryName)
				.then(function(query) {
					params.id = query.id;
					return TCG.data.getDataPromiseUsingCaching('systemQueryParameterListByQuery.json?queryId=' + query.id, gp, 'system.query.params.' + this.queryName);
				})
				.then(function(paramList) {
					let paramIndex = 0;
					paramIndex = gp.configureParamValue(params, paramList, paramIndex, 'RunBalanceDate', balanceDate);
					paramIndex = gp.configureParamValue(params, paramList, paramIndex, 'TeamID', TCG.getChildByName(t, 'teamSecurityGroupId').getValue());
					gp.configureParamValue(params, paramList, paramIndex, 'ClientAccountGroupID', TCG.getChildByName(t, 'clientAccountGroupId').getValue());
				});

		return Promise.resolve(loadingPromise)
				.then(function() {
					return params;
				});
	},
	// configures the specified parameter if the value is not blank and match found: returns next parameter index
	configureParamValue: function(params, paramList, paramIndex, paramName, paramValue) {
		if (TCG.isNotBlank(paramValue)) {
			for (let i = 0; i < paramList.length; i++) {
				if (paramList[i].name === paramName) {
					params['parameterValueList[' + paramIndex + '].parameter.id'] = paramList[i].id;
					params['parameterValueList[' + paramIndex + '].class'] = 'com.clifton.system.query.SystemQueryParameterValue';
					params['parameterValueList[' + paramIndex + '].value'] = paramValue;
					params['parameterValueList[' + paramIndex + '].text'] = paramValue;
					return paramIndex + 1;
				}
			}
		}
		return paramIndex;
	}
});
Ext.reg('product-incompleteRunsGrid', Clifton.product.overlay.IncompleteRunsGrid);


Clifton.product.overlay.PIOSTradeCreationGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioRunListFind',
	instructions: 'The following Portfolio runs have been processed and are ready for trade creation, or trades have already been created for them.',
	wikiPage: 'IT/PIOS Trade Creation Screen',
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Team', name: 'teamSecurityGroupId', width: 120, xtype: 'toolbar-combo', url: 'securityGroupTeamList.json', loadAll: true, linkedFilter: 'clientInvestmentAccount.teamSecurityGroup.name'},
			{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'investmentAccountGroupId', width: 180, url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow', disableAddNewItem: true},
			{fieldLabel: 'Client Acct', xtype: 'toolbar-combo', name: 'investmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientInvestmentAccount.label', displayField: 'label'}
		];
	},
	openRunTradeGroupSelectionWindow: function() {
		const dd = {};

		// Client Account Group
		const t = this.getTopToolbar();
		const cag = TCG.getChildByName(t, 'investmentAccountGroupId');
		if (cag.getValue()) {
			dd.clientAccountGroup = {id: cag.getValue(), name: cag.getRawValue()};
		}

		// Balance Date
		const dateFilter = this.grid.filters.getFilter('balanceDate');
		if (dateFilter) {
			const v = dateFilter.getValue();
			if (v && v.on) {
				dd.balanceDate = (v.on).format('Y-m-d 00:00:00');
			}
		}

		// MOC
		dd.marketOnClose = 'false'; // Default to False
		const mocFilter = this.grid.filters.getFilter('marketOnCloseAdjustmentsIncluded');
		if (mocFilter) {
			const v = mocFilter.getValue();
			if (v && v === true) {
				dd.marketOnClose = 'true';
			}
		}

		const config = {
			defaultDataIsReal: true,
			defaultData: dd
		};
		TCG.createComponent('Clifton.portfolio.run.trade.group.RunTradeGroupSelectionWindow', config);
	},
	getLoadParams: async function(firstLoad) {
		const t = this.getTopToolbar();
		if (firstLoad) {
			// default balance date to previous business day
			const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
			this.setFilterValue('balanceDate', {'on': prevBD});

			// try to get trader's team: only if one
			const team = await Clifton.security.user.getUserTeam(this);
			if (TCG.isNotNull(team)) {
				const teamSecurityGroupToolbarFilterField = TCG.getChildByName(t, 'teamSecurityGroupId');
				if (teamSecurityGroupToolbarFilterField) {
					teamSecurityGroupToolbarFilterField.setValue({value: team.id, text: team.name});
				}
				this.setFilterValue('clientInvestmentAccount.teamSecurityGroup.name', {value: team.id, text: team.name});
			}
		}

		const lp = {readUncommittedRequested: true, tradeReady: true};
		const v = TCG.getChildByName(t, 'investmentAccountGroupId').getValue();
		if (v) {
			lp.investmentAccountGroupId = v;
		}
		lp.workflowStatusNames = ['Pending', 'Approved'];
		return lp;
	},

	rowSelectionModel: 'checkbox',
	columns: [
		{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
		{
			header: '', width: 12,
			sortable: false, filter: false,
			renderer: function(v, args, r) {
				return TCG.renderActionColumn('pdf', '', 'View Portfolio Report', 'report');
			}
		},
		{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', width: 12, tooltip: 'File Attachment(s)', exportHeader: 'File Attachment(s)', dataIndex: 'documentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'},
		{header: '<div class="www" style="WIDTH:16px">&nbsp;</div>', width: 12, tooltip: 'If checked, there is at least one APPROVED file associated with this run on the portal.', exportHeader: 'Posted', dataIndex: 'postedToPortal', type: 'boolean'},
		{
			header: 'Client Account', width: 250, dataIndex: 'clientInvestmentAccount.label',
			filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'},
			renderer: function(v, metaData, r) {
				const note = r.data['notes'];
				if (TCG.isNotBlank(note)) {
					const qtip = r.data['notes'];
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				return v;
			}
		},
		{header: 'Client Service Processing Type', dataIndex: 'clientInvestmentAccount.serviceProcessingType.name', width: 70, hidden: true},
		{header: 'Client Processing Type', dataIndex: 'clientInvestmentAccount.serviceProcessingType.processingType', width: 70, hidden: true},
		{header: 'Processing Type', dataIndex: 'serviceProcessingType', width: 70, hidden: true},
		{header: 'Team', dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', width: 70, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}},
		{header: 'Notes', width: 100, dataIndex: 'notes', hidden: true},

		{header: 'MOC', width: 35, dataIndex: 'marketOnCloseAdjustmentsIncluded', type: 'boolean'},
		{header: 'Workflow State', width: 110, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
		{header: 'Workflow Status', width: 85, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=PortfolioRun'}},

		{
			header: 'Violation Status', width: 125, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
			renderer: function(v, p, r) {
				return (Clifton.rule.violation.renderViolationStatus(v));
			}
		},
		{header: 'Balance Date', width: 75, dataIndex: 'balanceDate'},
		{
			header: 'Updated By', width: 70, dataIndex: 'updateUserId', filter: {type: 'combo', searchFieldName: 'updateUserId', url: 'securityUserListFind.json', displayField: 'label'},
			renderer: Clifton.security.renderSecurityUser,
			exportColumnValueConverter: 'securityUserColumnReversableConverter'
		},
		{header: 'Updated At', width: 67, dataIndex: 'updateDate', align: 'center', renderer: TCG.renderTime},
		{header: 'Portfolio (TPV)', hidden: true, width: 100, dataIndex: 'portfolioTotalValue', type: 'currency', useNull: true, negativeInRed: true},
		{header: 'Cash Total', hidden: true, width: 100, dataIndex: 'cashTotal', type: 'currency', useNull: true, negativeInRed: true},
		{header: 'Overlay Target Total', hidden: true, width: 100, dataIndex: 'overlayTargetTotal', type: 'currency', useNull: true, negativeInRed: true},
		{header: 'Overlay Exposure Total', hidden: true, width: 100, dataIndex: 'overlayExposureTotal', type: 'currency', useNull: true, negativeInRed: true},
		{header: 'Cash Target', hidden: true, width: 100, dataIndex: 'cashTarget', type: 'currency', useNull: true, negativeInRed: true},
		{header: 'Cash Exposure', hidden: true, width: 100, dataIndex: 'cashExposure', type: 'currency', useNull: true, negativeInRed: true},
		{header: 'Cash Target %', hidden: true, width: 100, dataIndex: 'cashTargetPercent', type: 'currency', useNull: true, negativeInRed: true, numberFormat: '0,000.00 %'},
		{
			header: 'Cash Exposure % - Without Mispricing', hidden: true, width: 120, dataIndex: 'cashExposurePercent', type: 'currency', useNull: true, negativeInRed: true, numberFormat: '0,000.00 %',
			tooltip: '<b>Note</b>: Does NOT include mispricing. Value is capped at +/-9,999.9999.',
			renderer: function(v, p, r) {
				// Anything greater than 10 bps color in green
				if (v > 0.10) {
					return TCG.renderAmount(v, true, '0,000.0000 %');
				}
				// otherwise only color negatives in red
				return TCG.renderAmount(v, false, '0,000.0000 %');
			}
		},
		{header: 'Mispricing Total', hidden: true, width: 100, dataIndex: 'mispricingTotal', type: 'currency', useNull: true, negativeInRed: true},
		{
			header: 'Cash Exp %', width: 110, dataIndex: 'cashExposureWithMispricingPercent', type: 'currency', useNull: true, negativeInRed: true, numberFormat: '0,000.00 %',
			tooltip: '<b>Note</b>: Cash Exposure may include mispricing if the account holds securities that apply the fair value adjustment and the account is not set up to exclude mispricing. Value is capped at +/-9,999.9999.',
			renderer: function(v, p, r) {
				const mispricingTotal = r.data.mispricingTotal;
				if (mispricingTotal && mispricingTotal !== 0) {
					let qtip = '<table>';
					qtip += '<tr><td>Cash Exposure % (Original)</td><td align="right">' + TCG.renderAmount(r.data.cashExposurePercent, true, '0,000.0000 %') + '</td></tr>';
					qtip += '<tr><td>Mispricing Total</td><td align="right">' + TCG.renderAmount(mispricingTotal, true, '0,000.00') + '</td></tr>';
					qtip += '</table>';
					p.attr = 'qtip=\'' + qtip + '\'';
				}

				// Anything greater than 10 bps color in green
				if (v > 0.10) {
					return TCG.renderAmount(v, true, '0,000.0000 %');
				}
				// otherwise only color negatives in red
				return TCG.renderAmount(v, false, '0,000.0000 %');
			}
		}

	],
	editor: {
		detailPageClass: 'Clifton.portfolio.run.trade.RunTradeWindow',
		drillDownOnly: true,
		ptype: 'workflowAwareEntity-grideditor',
		tableName: 'PortfolioRun',
		skipDifferentFromStates: true,
		transitionWorkflowStateList: [
			{stateName: 'Reject', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'If Analyst Approved, will return run to Analyst for re-processing/changes.  Otherwise will return run to Analyst Approved for trade entry/changes.'},
			{stateName: 'Approve - No Trades', iconCls: 'row_reconciled', buttonText: 'Approve (No Trades)', buttonTooltip: 'If Analyst Approved, approve the run without submitting trades.  Otherwise, must be in 1st PM Approved - No Trades state and approving no trades entered.  To Submit/Approve with Trades please drill into the Trade Creation screen for the run.'}
		],
		getCorrectToStateName: function(fromState, toState, record) {
			if (toState === 'Reject') {
				if (fromState === 'Analyst Approved') {
					return 'Pending Analyst Approval';
				}
				return 'Analyst Approved';
			}
			else if (toState === 'Approve - No Trades') {
				if (fromState === 'Analyst Approved') {
					return '1st PM Approved - No Trades';
				}
				return 'Completed - No Trades';
			}
			return toState;
		},
		getDetailPageParams: function(id) {
			const gridPanel = this.getGridPanel();
			const selection = gridPanel.grid.getSelectionModel().getSelected();
			let serviceProcessingType = TCG.getValue('serviceProcessingType', selection.json);
			const clientProcessingType = TCG.getValue('clientInvestmentAccount.serviceProcessingType', selection.json);
			const processingType = TCG.getValue('clientInvestmentAccount.serviceProcessingType.processingType', selection.json);
			if (clientProcessingType === serviceProcessingType) {
				// If the run and client account processing types are the same, use the service processing type to attempt an overridden selector view.
				// If there is not an override for the business service type, the client account's processing type will be used in the selector.
				serviceProcessingType = TCG.getValue('clientInvestmentAccount.serviceProcessingType', selection.json);
			}
			return {
				id: id,
				processingType: processingType, // processing type string e.g., clientInvestmentAccount.serviceProcessingType.processingType
				serviceProcessingType: serviceProcessingType // service processing type; could be object or name as long as the selector window can process e.g., clientInvestmentAccount.serviceProcessingType
			};
		}
	},
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					const row = grid.store.data.items[rowIndex];
					const runId = row.json.id;
					if (eventName === 'report') {
						Clifton.portfolio.run.downloadPortfolioReport(runId, grid);
					}
				}
			}
		}
	}
});
Ext.reg('product-tradeCreationGrid', Clifton.product.overlay.PIOSTradeCreationGrid);


//add event journals tab to security event windows
Clifton.trade.TradeBlotterAdditionalTabs[Clifton.trade.TradeBlotterAdditionalTabs.length] = {
	title: 'Portfolio Trade Creation',
	items: [{
		xtype: 'product-tradeCreationGrid'
	}]
};
Clifton.trade.TradeBlotterAdditionalTabs[Clifton.trade.TradeBlotterAdditionalTabs.length] = {
	title: 'Incomplete Portfolio Runs',
	items: [{
		xtype: 'product-incompleteRunsGrid'
	}]
};

// Note: Actual Implementation Can Be Found in RunTradeGroupDynamicOptionRollWindow.js - need to include it so it loads
TCG.use('Clifton.product.overlay.trade.group.option.RunTradeGroupDynamicOptionRollWindow');
Clifton.trade.option.OptionBlotterAdditionalTabs[Clifton.trade.option.OptionBlotterAdditionalTabs.length] = {
	title: 'Option Roll',
	items: [{
		xtype: 'product-run-trade-group-option-roll-trade-entry-form-panel'
	}]
};
Clifton.trade.option.OptionBlotterAdditionalTabs[Clifton.trade.option.OptionBlotterAdditionalTabs.length] = {
	title: 'Portfolio Trade Creation',
	items: [{
		xtype: 'product-tradeCreationGrid',

		getTopToolbarFilters: function(toolbar) {
			return [
				{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'investmentAccountGroupId', width: 180, url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow', disableAddNewItem: true},
				{fieldLabel: 'Client Acct', xtype: 'combo', name: 'investmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientInvestmentAccount.label', displayField: 'label'},
				{
					fieldLabel: '&nbsp', labelSeparator: '', boxLabel: 'Option Roll Account Groups Only', xtype: 'toolbar-checkbox', name: 'optionRollAccountGroups', checked: true,
					qtip: 'Filter client account groups to those tagged with "Option Rolls"',
					listeners: {
						change: function(field, newValue, oldValue) {
							TCG.getChildByName(field.ownerCt, 'investmentAccountGroupId').resetStore();
						}
					}
				}
			];
		},

		getLoadParams: function(firstLoad) {
			let accountGroupPromise;
			const panel = this;
			const toolbar = panel.getTopToolbar();
			if (firstLoad) {
				accountGroupPromise = TCG.data.getDataPromise('investmentAccountGroupListFind.json', this, {params: {name: 'SPX-20D'}})
						.then(function(accountGroup) {
							if (accountGroup && accountGroup.length === 1) {
								TCG.getChildByName(toolbar, 'investmentAccountGroupId').setValue({value: accountGroup[0].id, text: accountGroup[0].name});
							}
						});
			}
			return Promise.resolve(Promise.resolve(accountGroupPromise).then(function() {
				// Add Option Roll Account Group category filter if filter is checked
				const optionRollAccountGroups = TCG.getChildByName(toolbar, 'optionRollAccountGroups').getValue();
				const investmentAccountGroupFilterField = TCG.getChildByName(toolbar, 'investmentAccountGroupId');
				investmentAccountGroupFilterField.store.proxy.api.read.url = investmentAccountGroupFilterField.url;
				if (TCG.isTrue(optionRollAccountGroups)) {
					investmentAccountGroupFilterField.store.proxy.api.read.url += '?categoryName=Investment Account Group Tags&categoryHierarchyName=Option Rolls';
				}
				return Clifton.product.overlay.PIOSTradeCreationGrid.prototype.getLoadParams.call(panel, firstLoad);
			}));
		},

		editor: {
			detailPageClass: 'Clifton.product.overlay.trade.group.option.RunTradeOptionCashFlowWindow',
			drillDownOnly: true,
			ptype: 'workflowAwareEntity-grideditor',
			tableName: 'PortfolioRun',
			skipDifferentFromStates: true,
			transitionWorkflowStateList: [
				{stateName: 'Reject', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'If Analyst Approved, will return run to Analyst for re-processing/changes.  Otherwise will return run to Analyst Approved for trade entry/changes.'},
				{stateName: 'Approve - No Trades', iconCls: 'row_reconciled', buttonText: 'Approve (No Trades)', buttonTooltip: 'If Analyst Approved, approve the run without submitting trades.  Otherwise, must be in 1st PM Approved - No Trades state and approving no trades entered.  To Submit/Approve with Trades please drill into the Trade Creation screen for the run.'}
			],
			getCorrectToStateName: function(fromState, toState, record) {
				if (toState === 'Reject') {
					if (fromState === 'Analyst Approved') {
						return 'Pending Analyst Approval';
					}
					return 'Analyst Approved';
				}
				else if (toState === 'Approve - No Trades') {
					if (fromState === 'Analyst Approved') {
						return '1st PM Approved - No Trades';
					}
					return 'Completed - No Trades';
				}
				return toState;
			}
		}
	}]
};


Clifton.product.performance.overlay.CashAttributionGrid = Ext.extend(TCG.grid.GridPanel, {
	xtype: 'gridpanel-scroll',
	wikiPage: 'IT/Cash Attribution',
	instructions: 'Cash Balance or Attribution breakdown for each day for the performance summary.  You can switch to view asset class details, or drill into a specific date to see the asset class cash summary/attribution breakdown. Note: All attribution calculations begin no earlier than January 1, 2014',
	viewNames: ['Daily Cash Balances', 'Daily Attribution', 'Daily Asset Class Cash Balances', 'Daily Asset Class Attribution'],
	includeAllColumnsView: false,
	isPagingEnabled: function() {
		return true;
	},
	initComponent: function() {
		const gp = this;
		Clifton.product.performance.overlay.CashAttributionGrid.superclass.initComponent.call(this);
		// Note: Because the URL Changes Based on the View Need to make sure URL is updated appropriately
		// Otherwise when clicking on a column to sort it reverts back to the original default run view url
		const store = this.ds;
		store.on('beforeload', function() {
			store.url = encodeURI(gp.getLoadURL());
			store.proxy.setUrl(store.url);
		});
	},
	getLoadURL: function() {
		let url = 'productPerformanceOverlayRunCashAttributionListFind.json';
		if (this.isCurrentViewForAssetClass()) {
			url = 'productPerformanceOverlayAssetClassCashAttributionListFind.json';
		}
		return url;
	},
	isCurrentViewForAssetClass: function() {
		return this.currentViewName === 'Daily Asset Class Cash Balances' || this.currentViewName === 'Daily Asset Class Attribution';
	},
	switchToViewBeforeReload: function(viewName) {
		// Disable Filtering/sorting on asset class only columns
		const cm = this.getColumnModel();
		const l = cm.getColumnCount();
		const offset = (this.rowSelectionModel && this.rowSelectionModel.width) ? 1 : 0; // checkbox selector
		for (let i = offset; i < l; i++) {
			const c = cm.getColumnById(i);
			if (c.assetClassFilter) {
				if (viewName === 'Daily Asset Class Cash Balances' || viewName === 'Daily Asset Class Attribution') {
					c.filter = c.assetClassFilter;
					c.sortable = true;
				}
				else {
					c.filter = false;
					c.sortable = false;
					// If currently sorting on a field that no longer applies, clear the sorting and use the default measure date option
					if (this.ds.sortInfo && this.ds.sortInfo.field === c.dataIndex) {
						delete this.ds.sortInfo;
						this.ds.setDefaultSort('measureDate', 'ASC');
					}
					// Clear if currently being filtered
					if (this.isFilterValueSet(c.dataIndex)) {
						this.clearFilter(c.dataIndex, true); // Suppress Events
					}
				}
			}
		}
	},
	columns: [
		{header: 'ID', width: 30, dataIndex: 'performanceOverlayRunId', hidden: true},
		{
			header: '&nbsp;', width: 15, hidden: true, dataIndex: 'attributionCalculationNote', viewNames: ['Daily Asset Class Cash Balances', 'Daily Asset Class Attribution'], assetClassFilter: {searchFieldName: 'attributionCalculationNote'},
			renderer: function(v, metaData, r) {
				if (v !== '') {
					const imgCls = 'flag-red';
					const tooltip = v;
					return '<span ext:qtip="' + tooltip + '"><span style="width: 16px; height: 16px; float: left;" class="' + imgCls + '" ext:qtip="' + tooltip + '">&nbsp;</span> </span>';
				}
			}
		},
		{header: 'Client Account', width: 100, hidden: true, dataIndex: 'clientAccount.label', filter: false}, // Using Toolbar filter explicitly so can validate that it's required on screen
		{header: 'Measure Date', width: 75, dataIndex: 'measureDate', allViews: true, defaultSortColumn: true},
		{
			header: 'Balance Date', width: 75, hidden: true, dataIndex: 'overlayRunDate', align: 'center',
			renderer: function(v, p, r) {
				if (r.data['overlayRun.id'] === '') {
					p.css = 'ruleViolation';
					p.attr = 'qtip=\'No Overlay Run Available on this date to process\'';
				}
				return TCG.renderDate(v);
			}
		},
		{header: 'Asset Class', width: 125, hidden: true, dataIndex: 'assetClassName', viewNames: ['Daily Asset Class Cash Balances', 'Daily Asset Class Attribution'], assetClassFilter: {searchFieldName: 'assetClassName'}},

		{header: 'Fund Cash (Original)', width: 90, dataIndex: 'fundCash', type: 'currency', hidden: true},
		{
			header: 'Fund Cash', width: 90, dataIndex: 'fundCashAdjusted', type: 'currency',
			viewNames: ['Daily Cash Balances', 'Daily Asset Class Cash Balances'],
			renderer: function(v, p, r) {
				return TCG.renderAdjustedAmount(v, r.data['fundCashAdjusted'] - r.data['fundCash'] !== 0, r.data['fundCash'], '0,000.00', 'Adjustment', true);
			}
		},
		{header: 'Fund Cash Attribution', width: 90, hidden: true, dataIndex: 'fundCashAttribution', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true, viewNames: ['Daily Attribution', 'Daily Asset Class Attribution']},

		{header: 'Manager Cash (Original)', width: 90, dataIndex: 'managerCash', type: 'currency', hidden: true},
		{
			header: 'Manager Cash', width: 90, dataIndex: 'managerCashAdjusted', type: 'currency',
			viewNames: ['Daily Cash Balances', 'Daily Asset Class Cash Balances'],
			renderer: function(v, p, r) {
				return TCG.renderAdjustedAmount(v, r.data['managerCashAdjusted'] - r.data['managerCash'] !== 0, r.data['managerCash'], '0,000.00', 'Adjustment', true);
			}
		},
		{header: 'Manager Cash Attribution', width: 90, hidden: true, dataIndex: 'managerCashAttribution', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true, viewNames: ['Daily Attribution', 'Daily Asset Class Attribution']},

		{header: 'Transition Cash (Original)', width: 90, dataIndex: 'transitionCash', type: 'currency', hidden: true},
		{
			header: 'Transition Cash', width: 90, dataIndex: 'transitionCashAdjusted', type: 'currency',
			viewNames: ['Daily Cash Balances', 'Daily Asset Class Cash Balances'],
			renderer: function(v, p, r) {
				return TCG.renderAdjustedAmount(v, r.data['transitionCashAdjusted'] - r.data['transitionCash'] !== 0, r.data['transitionCash'], '0,000.00', 'Adjustment', true);
			}
		},
		{header: 'Transition Cash Attribution', width: 90, hidden: true, dataIndex: 'transitionCashAttribution', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true, viewNames: ['Daily Attribution', 'Daily Asset Class Attribution']},

		{header: 'Rebalance Cash (Original)', width: 90, dataIndex: 'rebalanceCash', type: 'currency', hidden: true},
		{
			header: 'Rebalance Cash', width: 90, dataIndex: 'rebalanceCashAdjusted', type: 'currency',
			viewNames: ['Daily Cash Balances', 'Daily Asset Class Cash Balances'],
			renderer: function(v, p, r) {
				return TCG.renderAdjustedAmount(v, r.data['rebalanceCashAdjusted'] - r.data['rebalanceCash'] !== 0, r.data['rebalanceCash'], '0,000.00', 'Adjustment', true);
			}
		},
		{header: 'Rebalance Cash Attribution', width: 90, hidden: true, dataIndex: 'rebalanceCashAttribution', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true, viewNames: ['Daily Attribution', 'Daily Asset Class Attribution']},

		{header: 'Client Directed Cash (Original)', width: 90, dataIndex: 'clientDirectedCash', type: 'currency', hidden: true},
		{
			header: 'Client Directed Cash', width: 90, dataIndex: 'clientDirectedCashAdjusted', type: 'currency',
			viewNames: ['Daily Cash Balances', 'Daily Asset Class Cash Balances'],
			renderer: function(v, p, r) {
				return TCG.renderAdjustedAmount(v, r.data['clientDirectedCashAdjusted'] - r.data['clientDirectedCash'] !== 0, r.data['rebalanceCash'], '0,000.00', 'Adjustment', true);
			}
		},
		{header: 'Client Directed Cash Attribution', width: 90, hidden: true, dataIndex: 'clientDirectedCashAttribution', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true, viewNames: ['Daily Attribution', 'Daily Asset Class Attribution']},

		{header: 'Overlay Exposure', width: 90, hidden: true, dataIndex: 'overlayExposure', type: 'currency', negativeInRed: true, positiveInGreen: true, assetClassFilter: {searchFieldName: 'overlayExposure'}},
		{header: 'Overlay Target', width: 90, hidden: true, dataIndex: 'coalesceTargetOverride', type: 'currency', negativeInRed: true, positiveInGreen: true, assetClassFilter: {searchFieldName: 'coalesceOverlayTargetOverride'}},
		{header: 'Benchmark Return', width: 90, hidden: true, dataIndex: 'benchmarkReturn', type: 'percent', numberFormat: '0,000.0000', negativeInRed: true, positiveInGreen: true, assetClassFilter: {searchFieldName: 'benchmarkReturn'}},
		{header: 'Attribution (%) (Calculated)', hidden: true, width: 90, dataIndex: 'attributionReturn', type: 'percent', numberFormat: '0,000.0000', negativeInRed: true, positiveInGreen: true, assetClassFilter: {searchFieldName: 'attributionReturn'}},
		{header: 'Attribution (%) (Override)', hidden: true, width: 90, dataIndex: 'attributionReturnOverride', type: 'percent', useNull: true, numberFormat: '0,000.0000', negativeInRed: true, positiveInGreen: true, assetClassFilter: {searchFieldName: 'attributionReturnOverride'}},
		{
			header: 'Attribution (%)', width: 90, hidden: true, dataIndex: 'coalesceAttributionReturnOverride', type: 'percent', viewNames: ['Daily Asset Class Attribution'], assetClassFilter: {searchFieldName: 'coalesceAttributionReturnOverride'},
			renderer: function(v, p, r) {
				return TCG.renderAdjustedAmount(v, TCG.isNotBlank(r.data['attributionReturnOverride']), r.data['attributionReturn'], '0,000.0000', 'Adjustment', true);
			}
		},

		{header: 'Plug Cash Type', width: 90, hidden: true, dataIndex: 'plugAttributionCashType', assetClassFilter: {searchFieldName: 'plugAttributionCashType'}},
		{
			header: 'Plug Amount', width: 90, hidden: true, dataIndex: 'plugAttribution', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true, viewNames: ['Daily Asset Class Attribution'],
			assetClassFilter: {searchFieldName: 'plugAttribution'},
			renderer: function(v, metaData, r) {
				if (r.data.plugAttributionCashType !== '') {
					metaData.attr = TCG.renderQtip('Applied to ' + r.data.plugAttributionCashType + ' cash attribution.');
				}
				return TCG.renderAmount(v, true, '0,000.00', false);
			}
		},
		{header: 'Gain Loss', width: 90, hidden: true, dataIndex: 'coalesceGainLossOverride', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true, viewNames: ['Daily Attribution', 'Daily Asset Class Attribution']},
		{header: 'Cash Total', width: 90, dataIndex: 'cashTotal', type: 'currency', negativeInRed: true, positiveInGreen: true, viewNames: ['Daily Cash Balances', 'Daily Asset Class Cash Balances']}
	],
	plugins: {ptype: 'gridsummary'},
	editor: {
		detailPageClass: 'Clifton.product.performance.overlay.RunCashAttributionWindow',
		drillDownOnly: true,
		getDetailPageId: function(gridPanel, row) {
			return row.json.performanceOverlayRunId;
		}
	}

});
Ext.reg('product-performance-cash-attribution-grid', Clifton.product.performance.overlay.CashAttributionGrid);

Clifton.product.performance.overlay.RunTrackingErrorGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'performancePortfolioRunListByPerformanceSummaryPortfolioWide',
	instructions: 'Tracking Error breakdown for each day for the performance summary.  Drill into a specific date to see the asset class tracking error breakdown.',
	appendStandardColumns: false,
	groupField: 'measureDate',
	getLoadParams: function() {
		const portfolioWideField = TCG.getChildByName(this.getTopToolbar(), 'portfolioWide');
		const startMeasureDate = TCG.getChildByName(this.getTopToolbar(), 'startMeasureDate');
		const endMeasureDate = TCG.getChildByName(this.getTopToolbar(), 'endMeasureDate');
		const portfolioWide = portfolioWideField.getValue();

		this.grid.getStore().groupBy((portfolioWide) ? 'measureDate' : '', false);

		return {
			summaryId: this.getSummaryId(), portfolioWide: portfolioWide,
			startMeasureDate: startMeasureDate.getValue().format('m/d/Y'), endMeasureDate: endMeasureDate.getValue().format('m/d/Y')
		};
	},
	getSummaryId: function() {
		return this.getWindow().getMainFormId();
	},
	topToolbarUpdateCountPrefix: '-',
	getStartMeasureDateDefault: function() {
		return TCG.parseDate(this.getWindow().getMainForm().formValues.accountingPeriod.startDate);
	},
	getEndMeasureDateDefault: function() {
		return TCG.parseDate(this.getWindow().getMainForm().formValues.accountingPeriod.endDate);
	},
	getTopToolbarFilters: function(toolbar) {
		const startMeasureDate = this.getStartMeasureDateDefault();
		const endMeasureDate = this.getEndMeasureDateDefault();
		return [
			{fieldLabel: 'Start Date', name: 'startMeasureDate', xtype: 'datefield', value: startMeasureDate},
			{fieldLabel: 'End Date', name: 'endMeasureDate', xtype: 'datefield', value: endMeasureDate},
			'-',
			{
				boxLabel: 'Portfolio Wide&nbsp;', xtype: 'checkbox', name: 'portfolioWide',
				listeners: {
					check: function(field, checked) {
						const columnModel = TCG.getParentByClass(field, Ext.Panel).getColumnModel();
						const index = columnModel.findColumnIndex('performanceSummary.clientAccount.label');

						columnModel.setHidden(index, !checked);
						//Resize columns so everything fits nicely
						columnModel.config[4].width = 80;
						columnModel.config[5].width = 80;
						columnModel.config[6].width = 80;
						TCG.getParentByClass(field, Ext.Panel).reload();
					}
				}
			}

		];
	},
	isPagingEnabled: function() {
		return false;
	},
	totalPercentChange: function(v, r, field, data, col, grid, grandTotal, i, len) {
		if (grandTotal) {
			//Initialize undefined temporary value holders
			if (TCG.isNull(data[field + 'dateList'])) {
				data[field + 'dateList'] = [];
			}

			const dateList = data[field + 'dateList'];
			let dateValueList = dateList[r.data['measureDate']];

			if (TCG.isNull(dateValueList)) {
				dateValueList = [];
				dateList[r.data['measureDate']] = dateValueList;
			}

			//Aggregate values under the given measure date
			dateValueList.push(r.data[field]);

			if ((i + 1) < len) {
				return;
			}
			else {
				//Aggregation complete, Process totalPercentChange Calculation
				let totalPercentChange = 1;
				for (const key in dateList) {
					if (dateList.hasOwnProperty(key)) {
						dateValueList = dateList[key];
						let val = 0;

						for (let k = 0; k < dateValueList.length; k++) {
							val = val + dateValueList[k];
						}

						totalPercentChange = totalPercentChange * ((val / 100) + 1);
					}
				}
				return (totalPercentChange - 1) * 100;
			}
		}
		return v + r.data[field];
	},

	columns: [
		{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
		{header: 'Investment Account', width: 100, dataIndex: 'performanceSummary.clientAccount.label', hidden: true},
		{header: 'Measure Date', width: 50, dataIndex: 'measureDate'},
		{header: 'Run ID', width: 30, dataIndex: 'portfolioRun.id', hidden: true},
		{
			header: 'Balance Date', width: 50, hidden: true, dataIndex: 'portfolioRunDate', align: 'center',
			renderer: function(v, p, r) {
				if (TCG.isBlank(r.data['portfolioRun.id'])) {
					p.css = 'ruleViolation';
					p.attr = 'qtip=\'No Overlay Run Available on this date to process\'';
				}
				return TCG.renderDate(v);
			}
		},
		{
			header: 'Portfolio Benchmark Return', width: 90, dataIndex: 'trackingErrorAdjustedTargetAllocationPercent', type: 'float', useNull: true, renderer: TCG.renderPercent, summaryType: 'sum', positiveInGreen: true, negativeInRed: true,
			tooltip: 'Aggregate of Each Asset Class\'s Portfolio Adjusted Target % x Daily Asset Class Benchmark Return',
			summaryCalculation: function(v, r, field, data, col, grid, grandTotal, i, len) {
				return grid.ownerCt.totalPercentChange(v, r, field, data, col, grid, grandTotal, i, len);
			}
		},
		{
			header: 'Portfolio Return - Physical Exposure', width: 110, dataIndex: 'trackingErrorPhysicalExposurePercent', type: 'float', useNull: true, renderer: TCG.renderPercent, summaryType: 'sum', positiveInGreen: true, negativeInRed: true,
			tooltip: 'Aggregate of Each Asset Class\'s Portfolio Physical Exposure % x Daily Asset Class Benchmark Return',
			summaryCalculation: function(v, r, field, data, col, grid, grandTotal, i, len) {
				return grid.ownerCt.totalPercentChange(v, r, field, data, col, grid, grandTotal, i, len);
			}
		},
		{
			header: 'Portfolio Return - Total Exposure', width: 110, dataIndex: 'trackingErrorTotalExposurePercent', type: 'float', useNull: true, renderer: TCG.renderPercent, summaryType: 'sum', positiveInGreen: true, negativeInRed: true,
			tooltip: 'Aggregate of Each Asset Class\'s Portfolio Total Exposure % x Daily Asset Class Benchmark Return',
			summaryCalculation: function(v, r, field, data, col, grid, grandTotal, i, len) {
				return grid.ownerCt.totalPercentChange(v, r, field, data, col, grid, grandTotal, i, len);
			}
		}
	],
	plugins: {ptype: 'gridsummary'},

	editor: {
		detailPageClass: 'Clifton.product.performance.overlay.RunTrackingErrorWindow',
		drillDownOnly: true,
		getDefaultData: function(gridPanel, row) {
			const portfolioWideField = TCG.getChildByName(gridPanel.getTopToolbar(), 'portfolioWide');
			const portfolioWide = portfolioWideField.getValue();

			return {
				portfolioWide: portfolioWide
			};
		}
	}
});
Ext.reg('product-performance-tracking-error-grid', Clifton.product.performance.overlay.RunTrackingErrorGrid);
