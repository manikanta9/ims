Clifton.product.performance.PerformanceSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'productPerformanceSetupWindow',
	title: 'Performance Summaries',
	iconCls: 'chart-bar',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [{
			title: 'Summary History',
			items: [{
				name: 'performanceInvestmentAccountListFind',
				xtype: 'gridpanel',
				importTableName: 'PerformanceInvestmentAccount',
				importComponentName: 'Clifton.product.performance.upload.ProductPerformanceSummaryUploadWindow',
				instructions: 'The following performance summaries have been or are being processed.',
				wikiPage: 'IT/Performance Summaries',
				viewNames: ['Overlay', 'Security Targets'],
				getTopToolbarFilters: function(toolbar) {
					return [
						{fieldLabel: 'Team', name: 'teamSecurityGroupId', width: 120, xtype: 'toolbar-combo', url: 'securityGroupTeamList.json', loadAll: true, linkedFilter: 'clientAccount.teamSecurityGroup.name'},
						{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'investmentAccountGroupId', width: 180, url: 'investmentAccountGroupListFind.json'},
						{fieldLabel: 'Client Acct', xtype: 'toolbar-combo', name: 'investmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientAccount.label', displayField: 'label'},
						{fieldLabel: 'FA', tooltip: 'Financial Advisor', xtype: 'toolbar-combo', hidden: true, name: 'financialAdvisorId', width: 100, url: 'businessContactListFind.json?active=true&contactTypeNames=Financial Advisor', displayField: 'label'},
						{fieldLabel: 'Month', xtype: 'datefield-monthYear', name: 'measureMonthDate', width: 110}
					];
				},
				switchToViewBeforeReload: function(viewName) {
					const toolbar = this.getTopToolbar();
					const financialAdvisorToolbarCombo = TCG.getChildByName(toolbar, 'financialAdvisorId');
					if (financialAdvisorToolbarCombo) {
						financialAdvisorToolbarCombo.setVisible(viewName === 'Security Targets');
						if (financialAdvisorToolbarCombo.previousSibling && financialAdvisorToolbarCombo.previousSibling()) {
							// adjust visibility of label of combo
							financialAdvisorToolbarCombo.previousSibling().setVisible(viewName === 'Security Targets');
						}
					}
				},
				getLoadParams: async function(firstLoad) {
					const t = this.getTopToolbar();
					if (firstLoad) {
						// Default to Last Month
						const dt = (new Date()).getFirstDateOfMonth().add(Date.MONTH, -1);
						TCG.getChildByName(t, 'measureMonthDate').setValue(dt);

						// try to get trader's team: only if one
						const team = await Clifton.security.user.getUserTeam(this);
						if (TCG.isNotNull(team)) {
							TCG.getChildByName(t, 'teamSecurityGroupId').setValue({value: team.id, text: team.name});
							this.setFilterValue('clientAccount.teamSecurityGroup.name', {value: team.id, text: team.name});
						}
					}
					const lp = {requestedMaxDepth: 5};
					const v = TCG.getChildByName(t, 'investmentAccountGroupId').getValue();
					if (v) {
						lp.clientAccountGroupId = v;
					}

					const measureMonthDate = TCG.parseDate(TCG.getChildByName(t, 'measureMonthDate').getValue(), 'm/d/Y');
					if (TCG.isNotBlank(measureMonthDate)) {
						lp.startMeasureDate = (measureMonthDate.getFirstDateOfMonth()).format('m/d/Y');
						lp.endMeasureDate = (measureMonthDate.getLastDateOfMonth()).format('m/d/Y');
						if (this.currentViewName === 'Security Targets') {
							this.setFilterValue('clientAccount.startDate', {before: measureMonthDate.getFirstDateOfMonth()});
						}
					}

					if (this.currentViewName === 'Security Targets') {
						lp.clientAccountProcessingTypePurposeAccount = true;
						lp.clientAccountProcessingTypePurposeName = 'Trading: Options';
						const financialAdvisorCombo = TCG.getChildByName(t, 'financialAdvisorId');
						if (financialAdvisorCombo) {
							lp.clientAccountBusinessCompanyContactId = financialAdvisorCombo.getValue();
							lp.clientAccountBusinessCompanyContactTypeName = 'Financial Advisor';
						}
					}
					else {
						this.clearFilter('clientAccount.startDate');
					}

					return lp;
				},
				columns: [
					{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
					{header: 'Client Account', width: 200, dataIndex: 'clientAccount.label', filter: {type: 'combo', searchFieldName: 'clientAccountId', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'}, renderer: TCG.renderValueWithTooltip},
					{header: 'Service', width: 120, dataIndex: 'clientAccount.coalesceBusinessServiceGroupName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true, renderer: TCG.renderValueWithTooltip, viewNames: ['Security Targets']},
					{header: 'Inception Date', dataIndex: 'clientAccount.startDate', width: 60, hidden: true, filter: {searchFieldName: 'coalesceClientInvestmentAccountPerformanceInceptionDate', orEquals: true}, viewNames: ['Security Targets']},
					{header: 'Client Workflow State', dataIndex: 'clientAccount.workflowState.name', width: 60, hidden: true},
					{
						header: 'Closed Date', dataIndex: 'clientAccount.workflowStateEffectiveStartDate', width: 60, hidden: true, filter: {searchFieldName: 'clientAccountClosedDate', filterNull: true}, viewNames: ['Security Targets'],
						renderer: function(value, metadata, record) {
							if (TCG.isNotBlank(value)) {
								const clientState = TCG.getValue('clientAccount.workflowState.name', record.json);
								// only show the value if the client account is terminated
								if (clientState) {
									// contains Termination or Terminated
									if (clientState.startsWith('Closed') || clientState.includes('Terminat')) {
										return TCG.renderDate(new Date(value).format('m/d/Y'));
									}
								}
							}
							// clear the value for filtering
							record.data['clientAccount.workflowStateEffectiveStartDate'] = void 0;
							return void 0;
						}
					},
					{header: 'Holding Company', dataIndex: 'clientAccount.processingTypePurposeAccount.issuingCompany.name', width: 70, idDataIndex: 'clientAccount.processingTypePurposeAccount.issuingCompany.id', hidden: true, renderer: TCG.renderValueWithTooltip, filter: {searchFieldName: 'clientAccountProcessingTypePurposeAccountIssuingCompanyId', type: 'combo', displayField: 'label', url: 'businessCompanyListFind.json'}, viewNames: ['Security Targets'], tooltip: 'The issuing company for the Holding Account responsible for the Client Account\'s processing type configured reporting account purpose '},
					{
						header: 'Financial Advisors', width: 70, dataIndex: 'clientAccount.businessClient.id', filter: false, sortable: false, hidden: true, viewNames: ['Security Targets'],
						renderer: function(value, metadata, record) {
							if (TCG.isNotBlank(value)) {
								return TCG.renderActionColumn('view', 'View', 'View Financial Advisors for this Client', 'ShowFinancialAdvisors');
							}
							return 'N/A';
						},
						eventListeners: {
							'click': function(column, grid, rowIndex, event) {
								const clientId = TCG.getValue('clientAccount.businessClient.id', grid.getStore().getAt(rowIndex).json);
								TCG.createComponent('Clifton.business.client.ClientWindow', {
									id: TCG.getComponentId('Clifton.business.client.ClientWindow', clientId),
									params: {id: clientId},
									defaultActiveTabName: 'Contacts',
									additionalComponentConfig: {contactType: {label: 'Financial Advisor'}},
									openerCt: this
								});
							}
						}
					},
					{header: 'Team', dataIndex: 'clientAccount.teamSecurityGroup.name', width: 90, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}, viewNames: ['Overlay']},
					{header: 'Date', dataIndex: 'measureDate', width: 60},
					{header: '<div class="www" style="WIDTH:16px">&nbsp;</div>', width: 12, tooltip: 'If checked, there is at least one APPROVED file associated with this summary on the portal.', exportHeader: 'Posted', dataIndex: 'postedToPortal', type: 'boolean'},
					{header: 'Workflow Status', dataIndex: 'workflowStatus.name', width: 75, filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
					{
						header: 'Workflow State', dataIndex: 'workflowState.name', width: 110, filter: {searchFieldName: 'workflowStateName'},
						renderer: function(v, metaData, r) {
							const sts = r.data['workflowStatus.name'];
							if (sts === 'Draft') {
								metaData.css = 'amountNegative';
							}
							else if (sts === 'Closed') {
								metaData.css = 'amountPositive';
							}
							else {
								metaData.css = 'amountAdjusted';
							}
							return TCG.renderValueWithTooltip(v, metaData);
						}
					},
					{
						header: 'Violation Status', width: 110, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
						renderer: function(v, p, r) {
							return (Clifton.rule.violation.renderViolationStatus(v));
						}
					},

					{header: 'Benchmark MTD Return', dataIndex: 'monthToDateBenchmarkReturn', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, allViews: true, viewNameHeaders: [{name: 'Overlay', label: 'Benchmark MTD Return'}, {name: 'Security Targets', label: 'Target MTD Return'}]},
					{header: 'MTD Return', dataIndex: 'monthToDateReturn', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, allViews: true, viewNameHeaders: [{name: 'Overlay', label: 'MTD Return'}, {name: 'Security Targets', label: 'MTD Return (Gross)'}]},
					{header: 'MTD Difference', dataIndex: 'monthToDateReturnDifference', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNames: ['Overlay']},
					{header: 'Portfolio MTD Return', hidden: true, dataIndex: 'monthToDatePortfolioReturn', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNames: ['Security Targets'], viewNameHeaders: [{name: 'Overlay', label: 'Portfolio MTD Return'}, {name: 'Security Targets', label: 'MTD Return (Net)'}]},

					{header: 'Benchmark QTD Return', hidden: true, dataIndex: 'quarterToDateBenchmarkReturn', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Benchmark QTD Return'}, {name: 'Security Targets', label: 'Target QTD Return'}]},
					{header: 'QTD Return', hidden: true, dataIndex: 'quarterToDateReturn', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'QTD Return'}, {name: 'Security Targets', label: 'QTD Return (Gross)'}]},
					{header: 'QTD Difference', hidden: true, dataIndex: 'quarterToDateReturnDifference', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent},
					{header: 'Portfolio QTD Return', hidden: true, dataIndex: 'quarterToDatePortfolioReturn', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Portfolio QTD Return'}, {name: 'Security Targets', label: 'QTD Return (Net)'}]},

					{header: 'Benchmark YTD Return', hidden: true, dataIndex: 'yearToDateBenchmarkReturn', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Benchmark YTD Return'}, {name: 'Security Targets', label: 'Target YTD Return'}]},
					{header: 'YTD Return', hidden: true, dataIndex: 'yearToDateReturn', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'YTD Return'}, {name: 'Security Targets', label: 'YTD Return (Gross)'}]},
					{header: 'YTD Difference', hidden: true, dataIndex: 'yearToDateReturnDifference', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent},
					{header: 'Portfolio YTD Return', hidden: true, dataIndex: 'yearToDatePortfolioReturn', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Portfolio YTD Return'}, {name: 'Security Targets', label: 'YTD Return (Net)'}]},

					{header: 'Benchmark ITD Return', hidden: true, dataIndex: 'inceptionToDateBenchmarkReturn', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Benchmark ITD Return'}, {name: 'Security Targets', label: 'Target ITD Return'}]},
					{header: 'ITD Return', hidden: true, dataIndex: 'inceptionToDateReturn', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'ITD Return'}, {name: 'Security Targets', label: 'ITD Return (Gross)'}]},
					{header: 'ITD Difference', hidden: true, dataIndex: 'inceptionToDateReturnDifference', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent},
					{header: 'Portfolio ITD Return', hidden: true, dataIndex: 'inceptionToDatePortfolioReturn', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Portfolio ITD Return'}, {name: 'Security Targets', label: 'ITD Return (Net)'}]},

					{header: 'Benchmark ITD Return (Cumulative)', hidden: true, dataIndex: 'inceptionToDateBenchmarkReturnCumulative', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Benchmark ITD Return (Cumulative)'}, {name: 'Security Targets', label: 'Target ITD Return (Cumulative)'}]},
					{header: 'ITD Return (Cumulative)', hidden: true, dataIndex: 'inceptionToDateReturnCumulative', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'ITD Return (Cumulative)'}, {name: 'Security Targets', label: 'ITD Return (Gross) (Cumulative)'}]},
					{header: 'ITD Difference (Cumulative)', hidden: true, dataIndex: 'inceptionToDateReturnDifferenceCumulative', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent},
					{header: 'Portfolio ITD Return (Cumulative)', hidden: true, dataIndex: 'inceptionToDatePortfolioReturnCumulative', type: 'currency', useNull: true, width: 90, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Portfolio ITD Return (Cumulative)'}, {name: 'Security Targets', label: 'ITD Return (Net) (Cumulative)'}]}

				],
				editor: {
					detailPageClass: {
						getItemText: function(rowItem) {
							return 'Default';
						},
						items: [
							{text: 'Overlay Performance', tooltip: 'Applies to Overlay accounts that use Portfolio Runs', className: 'Clifton.product.performance.SummaryWindow'},
							{text: 'Security Target Performance', tooltip: 'Applies to Option strategy accounts that use Security Targets.', className: 'Clifton.performance.account.securitytarget.PerformanceAccountSecurityTargetWindow'}
						]
					},
					getDetailPageClassByText: function(text) {
						if (TCG.isEquals('Default', text)) {
							// Let the window selector pick it for the account
							return 'Clifton.performance.account.PerformanceAccountWindow';
						}
						let result = undefined;
						Ext.each(this.detailPageClass.items, function(item) {
							if (TCG.isEquals(item.text, text)) {
								result = item.className;
							}
						});
						return result;
					},
					getDefaultData: function(gridPanel) { // defaults investment account
						const t = gridPanel.getTopToolbar();
						const lp = {};
						const ia = TCG.getChildByName(t, 'investmentAccountId');
						if (TCG.isNotBlank(ia.getValue())) {
							lp.clientAccount = {id: ia.getValue(), label: ia.getRawValue()};
						}
						const measureMonthDate = TCG.parseDate(TCG.getChildByName(t, 'measureMonthDate').getValue());
						if (TCG.isNotBlank(measureMonthDate)) {
							const ap = TCG.data.getData('accountingPeriodForDate.json?date=' + measureMonthDate.format('m/d/Y'), gridPanel, 'accounting.period.date.' + measureMonthDate.format('m/d/Y'));
							if (ap) {
								lp.accountingPeriod = {id: ap.id, label: ap.label};
							}
						}
						return lp;
					},
					getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
						return undefined; // Not needed for Existing Runs
					}
				}
			}]

		},


			{
				title: 'Draft Summaries',
				items: [{
					xtype: 'product-performanceSummaryWorkflowGrid',
					workflowStateNames: 'Draft',

					columnOverrides: [{dataIndex: 'workflowState.name', hidden: true}],

					transitionWorkflowStateList: [
						{stateName: 'Approved', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Approve Selected Summaries'}
					],
					instructions: 'The following summaries are in Draft status and have not been approved.',

					getCorrectToStateName: function(fromState, toState, record) {
						if (TCG.isTrue(this.isSecurityTargetSummary(record))) {
							return 'Pending Final Approval';
						}
						return 'Approved By Accounting';
					}
				}]
			},


			{
				title: 'Pending Review',
				items: [{
					xtype: 'product-performanceSummaryWorkflowGrid',
					workflowStateNames: 'Approved By Accounting',

					columnOverrides: [{dataIndex: 'workflowState.name', hidden: true}],

					transitionWorkflowStateList: [
						{stateName: 'Approved By Analyst', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Approved Selected Summaries'},
						{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'Reject Selected Summaries'}
					],
					instructions: 'The following summaries have been approved by Accounting and are being edited/reviewed by analysts. If you are a member of the Analyst team, you may select summaries from the list below and approve or reject them using the Approve & Reject buttons'
				}]
			},


			{
				title: 'Pending Approval',
				items: [{
					xtype: 'product-performanceSummaryWorkflowGrid',
					workflowStateNames: ['Approved By Analyst', 'Pending Final Approval'],

					transitionWorkflowStateList: [
						{stateName: 'Approve', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Approved Selected Summaries'},
						{stateName: 'Reject', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'Reject Selected Summaries'}
					],
					instructions: 'The following summaries have gone through initial review and approval and are waiting for secondary approval.',

					getCorrectToStateName: function(fromState, toState, record) {
						if (TCG.isTrue(this.isSecurityTargetSummary(record))) {
							if (TCG.isEquals('Approve', toState)) {
								return 'Approved';
							}
							else {
								return 'Draft';
							}
						}
						if (TCG.isEquals('Approve', toState)) {
							return 'Approved By Portfolio Manager';
						}
						else {
							return 'Approved By Accounting';
						}
					}
				}]
			},


			{
				title: 'Approved Summaries',
				items: [{
					xtype: 'product-performanceSummaryWorkflowGrid',
					workflowStateNames: ['Approved By Portfolio Manager', 'Approved'],

					transitionWorkflowStateList: [
						{stateName: 'Posted', iconCls: 'www', buttonText: 'Post', buttonTooltip: 'Post Selected Summaries to the Portal'},
						{stateName: 'Complete - Not Posted', iconCls: 'row_reconciled', buttonText: 'Mark Complete', buttonTooltip: 'Mark the selected Summaries as complete but do not post to the portal.'},
						{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'Reject Selected Summaries'}
					],
					instructions: 'The following summaries have been approved by all parties and are ready to be posted to the portal.'
				}]
			},


			{
				title: 'Completed Summaries',
				items: [{
					xtype: 'product-performanceSummaryWorkflowGrid',
					workflowStatusNames: 'Closed',

					transitionWorkflowStateList: [
						{stateName: 'Posted', iconCls: 'www', buttonText: 'Post', buttonTooltip: 'Post Selected Summaries to the Portal'},
						{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'Reject Selected Summaries'}
					],
					instructions: 'The following summaries have been approved by all parties and reporting is complete.',
					getCorrectToStateName: function(fromState, toState, record) {
						if (TCG.isEquals('Draft', toState)) {
							if (!TCG.isTrue(this.isSecurityTargetSummary(record))) {
								return 'Approved By Accounting';
							}
						}
						return 'Draft';
					},

					applyAdditionalLoadParams: function(loadParams, firstLoad) {
						if (firstLoad) {
							this.setFilterValue('measureDate', {'after': TCG.getLastDateOfPreviousQuarter().add(Date.DAY, -1)});
						}
						return loadParams;
					}
				}]
			},


			{
				title: 'Benchmark Setup',
				items: [{
					name: 'performanceBenchmarkListFind',
					xtype: 'gridpanel',
					wikiPage: 'IT/Performance Benchmarks',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Team', name: 'teamSecurityGroupId', width: 180, xtype: 'toolbar-combo', url: 'securityGroupTeamList.json', loadAll: true, linkedFilter: 'clientAccount.teamSecurityGroup.name'},
							{fieldLabel: 'Client Account', xtype: 'toolbar-combo', name: 'clientAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientAccount.label', displayField: 'label'}
						];
					},
					getLoadParams: async function(firstLoad) {
						if (firstLoad) {
							const t = this.getTopToolbar();

							// try to get trader's team: only if one
							const team = await Clifton.security.user.getUserTeam(this);
							if (TCG.isNotNull(team)) {
								TCG.getChildByName(t, 'teamSecurityGroupId').setValue({value: team.id, text: team.name});
								this.setFilterValue('clientAccount.teamSecurityGroup.name', {value: team.id, text: team.name});
							}
						}
					},
					columns: [
						{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
						{header: 'Client Account', width: 200, dataIndex: 'clientAccount.label', filter: {type: 'combo', searchFieldName: 'clientAccountId', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Team', dataIndex: 'clientAccount.teamSecurityGroup.name', width: 50, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}},

						{header: 'Benchmark Name', width: 90, dataIndex: 'name'},
						{header: 'Benchmark Security', width: 130, dataIndex: 'benchmarkSecurity.label', filter: {type: 'combo', searchFieldName: 'benchmarkSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Benchmark Interest Rate Index', width: 130, dataIndex: 'benchmarkInterestRateIndex.label', filter: {type: 'combo', searchFieldName: 'benchmarkInterestRateIndexId', displayField: 'label', url: 'investmentInterestRateIndexListFind.json'}},
						{header: 'Order', width: 40, dataIndex: 'benchmarkOrder', type: 'int'},
						{header: 'Hide Historical Returns', type: 'boolean', width: 50, qtip: 'Hide Historical Returns if Start Date after Period Start<br /><br />If checked, displays <i>N/A</i> for benchmark returns on the report if the benchmark start date is after the period start (QTD, YTD, ITD).<br /><br /><b>Note:</b>&nbsp;Processing and calculations are still evaluated.  This option is used for reports only.'},
						{header: 'Start Date', width: 50, dataIndex: 'startDate'},
						{header: 'End Date', width: 50, dataIndex: 'endDate'}
					],
					editor: {
						detailPageClass: 'Clifton.performance.account.benchmark.BenchmarkWindow',
						getDefaultData: function(gridPanel) { // defaults investment account
							const t = gridPanel.getTopToolbar();
							const ia = TCG.getChildByName(t, 'clientAccountId');
							if (TCG.isNotBlank(ia.getValue())) {
								return {
									clientAccount: {id: ia.getValue(), label: ia.getRawValue()}
								};
							}
						}
					}
				}]

			},


			{
				title: 'Benchmark Returns',
				items: [{
					name: 'performanceSummaryBenchmarkListFind',
					xtype: 'gridpanel',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Client Account', xtype: 'toolbar-combo', name: 'clientAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'referenceOne.clientAccount.label', displayField: 'label'},
							{fieldLabel: 'Month', xtype: 'datefield-monthYear', name: 'measureMonthDate', width: 110, format: 'F, Y'}
						];
					},
					getLoadParams: function(firstLoad) {
						const t = this.getTopToolbar();
						if (firstLoad) {
							// Default to Last Month
							const dt = (new Date()).getFirstDateOfMonth().add(Date.MONTH, -1);
							TCG.getChildByName(t, 'measureMonthDate').setValue(dt);
						}
						const measureMonthDate = TCG.parseDate(TCG.getChildByName(t, 'measureMonthDate').getValue());
						if (TCG.isNotBlank(measureMonthDate)) {
							return {
								requestedMaxDepth: 4,
								startSummaryMeasureDate: (measureMonthDate.getFirstDateOfMonth()).format('m/d/Y'),
								endSummaryMeasureDate: (measureMonthDate.getLastDateOfMonth()).format('m/d/Y')
							};

						}
						return {requestedMaxDepth: 4};

					},
					columns: [
						{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
						{header: 'Client Account', width: 200, dataIndex: 'referenceOne.clientAccount.label', filter: {type: 'combo', searchFieldName: 'clientAccountId', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Date', width: 50, dataIndex: 'referenceOne.measureDate', filter: {searchFieldName: 'summaryMeasureDate'}},
						{header: 'Benchmark Name', width: 90, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'benchmarkName'}},
						{header: 'Benchmark Security', width: 120, dataIndex: 'referenceTwo.benchmarkSecurity.label', filter: {type: 'combo', searchFieldName: 'benchmarkSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Benchmark Interest Rate Index', width: 120, dataIndex: 'referenceTwo.benchmarkInterestRateIndex.label', filter: {type: 'combo', searchFieldName: 'benchmarkInterestRateIndexId', displayField: 'label', url: 'investmentInterestRateIndexListFind.json'}},

						{header: 'MTD (Calculated)', dataIndex: 'monthToDateReturn', type: 'currency', hidden: true, useNull: true, width: 50, renderer: TCG.renderPercent},
						{header: 'MTD (Override)', dataIndex: 'monthToDateReturnOverride', type: 'currency', hidden: true, useNull: true, width: 50, renderer: TCG.renderPercent},
						{
							header: 'MTD', dataIndex: 'coalesceMonthToDateReturnOverride', useNull: true, type: 'currency', numberFormat: '0,000.0000 %', width: 50,
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, TCG.isNotBlank(r.data['monthToDateReturnOverride']), r.data['monthToDateReturn'], '0,000.0000 %', 'Adjustment', true);
							}
						},

						{header: 'QTD', dataIndex: 'quarterToDateReturn', type: 'currency', useNull: true, width: 50, renderer: TCG.renderPercent},
						{header: 'YTD', dataIndex: 'yearToDateReturn', type: 'currency', useNull: true, width: 50, renderer: TCG.renderPercent},
						{header: 'ITD', dataIndex: 'inceptionToDateReturn', type: 'currency', useNull: true, width: 50, renderer: TCG.renderPercent}
					],
					editor: {
						detailPageClass: 'Clifton.performance.account.benchmark.SummaryBenchmarkWindow',
						drillDownOnly: true
					}
				}]

			},


			{
				title: 'Cash Attribution',
				items: [{
					xtype: 'product-performance-cash-attribution-grid',
					reloadOnRender: false,
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Client Account', xtype: 'toolbar-combo', name: 'clientAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'},
							{fieldLabel: '', boxLabel: 'Include Sub-Accounts', xtype: 'toolbar-checkbox', name: 'includeSubAccounts', qtip: 'Optionally include client accounts related with purpose "Clifton Sub-Account" for the selected account.  Useful for reviewing data when the report is a rollup of sub-accounts and includes attribution for the sub-accounts.'}
						];
					},
					getLoadParams: function(firstLoad) {
						const clientAccount = TCG.getChildByName(this.getTopToolbar(), 'clientAccountId').getValue();
						if (TCG.isBlank(clientAccount)) {
							TCG.showError('A client account selection is required.');
							return false;
						}
						if (firstLoad) {
							// default to previous month
							let date = new Date();
							date = date.add(Date.MONTH, -1);
							date = new Date(date.getFullYear(), date.getMonth(), 1);
							date = date.add(Date.DAY, -1);
							this.setFilterValue('measureDate', {'after': date});
						}
						if (TCG.getChildByName(this.getTopToolbar(), 'includeSubAccounts') && TCG.isTrue(TCG.getChildByName(this.getTopToolbar(), 'includeSubAccounts').getValue())) {
							// Show the Client Account Column If Not Visible Already
							const cm = this.getColumnModel();
							cm.setHidden(cm.findColumnIndex('clientAccount.label'), false);
							return {clientAccountIdOrSubAccount: clientAccount};
						}
						return {clientAccountId: clientAccount};
					}
				}]
			},


			{
				title: 'Administration',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [{
					xtype: 'formpanel',
					labelWidth: 180,
					loadValidation: false, // using the form only to get background color/padding
					height: 220,
					buttonAlign: 'right',
					instructions: 'Use this section to (re)process multiple performance summaries at one time. Select criteria below to filter which summaries to (re)process.',
					listeners: {
						afterrender: function(formpanel) {
							const form = this.getForm();
							const measureDate = form.findField('measureDate');
							if (TCG.isBlank(measureDate.getValue())) {
								measureDate.setValue(TCG.getLastDateOfPreviousMonth());
							}
						}
					},
					items: [
						{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'investmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['investmentAccountId'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
						{fieldLabel: 'Client Account', name: 'accountLabel', hiddenName: 'investmentAccountId', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', mutuallyExclusiveFields: ['investmentAccountGroupId']},
						{fieldLabel: 'Measure Date', xtype: 'datefield', name: 'measureDate', allowBlank: false},
						{boxLabel: 'Immediately process Portfolio runs for each performance summary', name: 'processAll', xtype: 'checkbox'}
						//{xtype: 'hidden', name: 'processWarnings', value: false}
					],

					buttons: [
						{
							text: 'Process Performance Summaries',
							iconCls: 'run',
							width: 150,
							tooltip: 'Create multiple performance summaries in DRAFT state, and optionally process portfolio runs',
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								form.submit(Ext.applyIf({
									url: 'performanceInvestmentAccountListProcess.json',
									waitMsg: 'Processing...',
									success: function(form, action) {
										Ext.Msg.alert('Process Success', action.result.result, function() {
											const grid = owner.ownerCt.items.get(1);
											grid.reload.defer(300, grid);
										});
									}
								}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
							}
						},
						{
							text: 'Process Warnings',
							iconCls: 'warning',
							width: 150,
							tooltip: 'Process summaries that are in DRAFT state, and have warnings or errors',
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								form.submit(Ext.applyIf({
									url: 'performanceInvestmentAccountListProcess.json?processWarnings=true',
									waitMsg: 'Processing...',
									success: function(form, action) {
										Ext.Msg.alert('Process Success', action.result.result, function() {
											const grid = owner.ownerCt.items.get(1);
											grid.reload.defer(300, grid);
										});
									}
								}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
							}
						}]
				},

					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'PERFORMANCE-ACCOUNT',
						instantRunner: true,
						instructions: 'The following Performance Summaries are being (re)processed right now.',
						title: 'Current Performance Summaries',
						flex: 1
					}]
			}]
	}]
});


Clifton.product.performance.SummaryWorkflowGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'performanceInvestmentAccountListFind',
	xtype: 'gridpanel',
	instructions: 'OVERRIDE-ME',
	workflowStateName: 'OVERRIDE-ME',
	viewNames: ['Overlay', 'Security Targets'],
	additionalPropertiesToRequest: 'workflowState.workflow.name',

	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Team', name: 'teamSecurityGroupId', width: 120, xtype: 'toolbar-combo', url: 'securityGroupTeamList.json', loadAll: true, linkedFilter: 'clientAccount.teamSecurityGroup.name'},
			{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'investmentAccountGroupId', width: 180, url: 'investmentAccountGroupListFind.json'},
			{fieldLabel: 'Client Acct', xtype: 'toolbar-combo', name: 'investmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientAccount.label', displayField: 'label'},
			{fieldLabel: 'FA', tooltip: 'Financial Advisor', xtype: 'toolbar-combo', hidden: true, name: 'financialAdvisorId', width: 100, url: 'businessContactListFind.json?active=true&contactTypeNames=Financial Advisor', displayField: 'label'}
		];
	},

	switchToViewBeforeReload: function(viewName) {
		const toolbar = this.getTopToolbar();
		const financialAdvisorToolbarCombo = TCG.getChildByName(toolbar, 'financialAdvisorId');
		if (financialAdvisorToolbarCombo) {
			financialAdvisorToolbarCombo.setVisible(viewName === 'Security Targets');
			if (financialAdvisorToolbarCombo.previousSibling && financialAdvisorToolbarCombo.previousSibling()) {
				// adjust visibility of label of combo
				financialAdvisorToolbarCombo.previousSibling().setVisible(viewName === 'Security Targets');
			}
		}
	},

	getLoadParams: async function(firstLoad) {
		const t = this.getTopToolbar();
		if (firstLoad) {
			// try to get trader's team: only if one
			const team = await Clifton.security.user.getUserTeam(this);
			if (TCG.isNotNull(team)) {
				TCG.getChildByName(t, 'teamSecurityGroupId').setValue({value: team.id, text: team.name});
				this.setFilterValue('clientAccount.teamSecurityGroup.name', {value: team.id, text: team.name});
			}
		}
		const lp = {requestedMaxDepth: 5};
		const v = TCG.getChildByName(t, 'investmentAccountGroupId').getValue();
		if (v) {
			lp.clientAccountGroupId = v;
		}

		const workflowStateNames = this.workflowStateNames;
		if (TCG.isNotNull(workflowStateNames)) {
			if (typeof workflowStateNames == 'string') {
				lp.workflowStateNameEquals = workflowStateNames;
			}
			else {
				lp.workflowStateNames = workflowStateNames;
			}
		}
		if (TCG.isNotNull(this.workflowStatusNames)) {
			if (typeof this.workflowStatusNames == 'string') {
				lp.workflowStatusNameEquals = this.workflowStatusNames;
			}
			else {
				lp.workflowStatusNames = this.workflowStatusNames;
			}
		}
		if (this.currentViewName === 'Security Targets') {
			lp.clientAccountProcessingTypePurposeAccount = true;
			lp.clientAccountProcessingTypePurposeName = 'Trading: Options';
			const financialAdvisorCombo = TCG.getChildByName(t, 'financialAdvisorId');
			if (financialAdvisorCombo) {
				lp.clientAccountBusinessCompanyContactId = financialAdvisorCombo.getValue();
				lp.clientAccountBusinessCompanyContactTypeName = 'Financial Advisor';
			}
		}
		return this.applyAdditionalLoadParams(lp, firstLoad);
	},
	applyAdditionalLoadParams: function(loadParams, firstLoad) {
		return loadParams;
	},

	rowSelectionModel: 'checkbox',
	columns: [
		{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
		{header: 'Client Account', width: 200, dataIndex: 'clientAccount.label', filter: {type: 'combo', searchFieldName: 'clientAccountId', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'}, renderer: TCG.renderValueWithTooltip},
		{header: 'Inception Date', dataIndex: 'clientAccount.startDate', width: 60, hidden: true, filter: {searchFieldName: 'coalesceClientInvestmentAccountPerformanceInceptionDate', orEquals: true}, viewNames: ['Security Targets']},
		{header: 'Client Workflow State', dataIndex: 'clientAccount.workflowState.name', width: 60, hidden: true},
		{
			header: 'Closed Date', dataIndex: 'clientAccount.workflowStateEffectiveStartDate', width: 60, filter: {searchFieldName: 'clientAccountClosedDate', filterNull: true}, hidden: true, viewNames: ['Security Targets'],
			renderer: function(value, metadata, record) {
				if (TCG.isNotBlank(value)) {
					const clientState = TCG.getValue('clientAccount.workflowState.name', record.json);
					// only show the value if the client account is terminated
					if (clientState) {
						// contains Termination or Terminated
						if (clientState.startsWith('Closed') || clientState.includes('Terminat')) {
							return TCG.renderDate(new Date(value).format('m/d/Y'));
						}
					}
				}
				// clear the value for filtering
				record.data['clientAccount.workflowStateEffectiveStartDate'] = void 0;
				return void 0;
			}
		},
		{header: 'Holding Company', dataIndex: 'clientAccount.processingTypePurposeAccount.issuingCompany.name', width: 70, idDataIndex: 'clientAccount.processingTypePurposeAccount.issuingCompany.id', hidden: true, renderer: TCG.renderValueWithTooltip, filter: {searchFieldName: 'clientAccountProcessingTypePurposeAccountIssuingCompanyId', type: 'combo', displayField: 'label', url: 'businessCompanyListFind.json'}, viewNames: ['Security Targets'], tooltip: 'The issuing company for the Holding Account responsible for the Client Account\'s processing type configured reporting account purpose '},
		{
			header: 'Financial Advisors', dataIndex: 'clientAccount.businessClient.id', width: 50, filter: false, sortable: false, hidden: true, viewNames: ['Security Targets'],
			renderer: function(value, metadata, record) {
				if (TCG.isNotBlank(value)) {
					return TCG.renderActionColumn('view', 'View', 'View Financial Advisors for this Client', 'ShowFinancialAdvisors');
				}
				return 'N/A';
			},
			eventListeners: {
				'click': function(column, grid, rowIndex, event) {
					const clientId = TCG.getValue('clientAccount.businessClient.id', grid.getStore().getAt(rowIndex).json);
					TCG.createComponent('Clifton.business.client.ClientWindow', {
						id: TCG.getComponentId('Clifton.business.client.ClientWindow', clientId),
						params: {id: clientId},
						defaultActiveTabName: 'Contacts',
						additionalComponentConfig: {contactType: {label: 'Financial Advisor'}},
						openerCt: this
					});
				}
			}
		},
		{header: 'Team', dataIndex: 'clientAccount.teamSecurityGroup.name', width: 70, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}, viewNames: ['Overlay']},
		{header: 'Date', dataIndex: 'measureDate', width: 60},
		{header: 'Workflow Status', dataIndex: 'workflowStatus.name', width: 75, hidden: true, filter: {searchFieldName: 'workflowStatusName'}},
		{
			header: 'Workflow State', dataIndex: 'workflowState.name', width: 90, filter: {searchFieldName: 'workflowStateName'},
			renderer: function(v, metaData, r) {
				const sts = r.data['workflowStatus.name'];
				if (sts === 'Draft') {
					metaData.css = 'amountNegative';
				}
				else if (sts === 'Closed') {
					metaData.css = 'amountPositive';
				}
				else {
					metaData.css = 'amountAdjusted';
				}
				return TCG.renderValueWithTooltip(v, metaData);
			}
		},
		{
			header: 'Violation Status', width: 90, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
			renderer: function(v, p, r) {
				return (Clifton.rule.violation.renderViolationStatus(v));
			}
		},

		{header: 'Benchmark MTD Return', dataIndex: 'monthToDateBenchmarkReturn', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, allViews: true, viewNameHeaders: [{name: 'Overlay', label: 'Benchmark MTD Return'}, {name: 'Security Targets', label: 'Target MTD Return'}]},
		{header: 'MTD Return', dataIndex: 'monthToDateReturn', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, allViews: true, viewNameHeaders: [{name: 'Overlay', label: 'MTD Return'}, {name: 'Security Targets', label: 'MTD Return (Gross)'}]},
		{header: 'MTD Difference', dataIndex: 'monthToDateReturnDifference', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, viewNames: ['Overlay']},
		{header: 'Portfolio MTD Return', hidden: true, dataIndex: 'monthToDatePortfolioReturn', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, viewNames: ['Security Targets'], viewNameHeaders: [{name: 'Overlay', label: 'Portfolio MTD Return'}, {name: 'Security Targets', label: 'MTD Return (Net)'}]},

		{header: 'Benchmark QTD Return', hidden: true, dataIndex: 'quarterToDateBenchmarkReturn', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Benchmark QTD Return'}, {name: 'Security Targets', label: 'Target QTD Return'}]},
		{header: 'QTD Return', hidden: true, dataIndex: 'quarterToDateReturn', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'QTD Return'}, {name: 'Security Targets', label: 'QTD Return (Gross)'}]},
		{header: 'QTD Difference', hidden: true, dataIndex: 'quarterToDateReturnDifference', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent},
		{header: 'Portfolio QTD Return', hidden: true, dataIndex: 'quarterToDatePortfolioReturn', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Portfolio QTD Return'}, {name: 'Security Targets', label: 'QTD Return (Net)'}]},

		{header: 'Benchmark YTD Return', hidden: true, dataIndex: 'yearToDateBenchmarkReturn', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercentt, viewNameHeaders: [{name: 'Overlay', label: 'Benchmark YTD Return'}, {name: 'Security Targets', label: 'Target YTD Return'}]},
		{header: 'YTD Return', hidden: true, dataIndex: 'yearToDateReturn', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'YTD Return'}, {name: 'Security Targets', label: 'YTD Return (Gross)'}]},
		{header: 'YTD Difference', hidden: true, dataIndex: 'yearToDateReturnDifference', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent},
		{header: 'Portfolio YTD Return', hidden: true, dataIndex: 'yearToDatePortfolioReturn', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Portfolio YTD Return'}, {name: 'Security Targets', label: 'YTD Return (Net)'}]},

		{header: 'Benchmark ITD Return', hidden: true, dataIndex: 'inceptionToDateBenchmarkReturn', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Benchmark ITD Return'}, {name: 'Security Targets', label: 'Target ITD Return'}]},
		{header: 'ITD Return', hidden: true, dataIndex: 'inceptionToDateReturn', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'ITD Return'}, {name: 'Security Targets', label: 'ITD Return (Gross)'}]},
		{header: 'ITD Difference', hidden: true, dataIndex: 'inceptionToDateReturnDifference', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent},
		{header: 'Portfolio ITD Return', hidden: true, dataIndex: 'inceptionToDatePortfolioReturn', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Portfolio ITD Return'}, {name: 'Security Targets', label: 'ITD Return (Net)'}]},

		{header: 'Benchmark ITD Return (Cumulative)', hidden: true, dataIndex: 'inceptionToDateBenchmarkReturnCumulative', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Benchmark ITD Return (Cumulative)'}, {name: 'Security Targets', label: 'Target ITD Return (Cumulative)'}]},
		{header: 'ITD Return (Cumulative)', hidden: true, dataIndex: 'inceptionToDateReturnCumulative', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'ITD Return (Cumulative)'}, {name: 'Security Targets', label: 'ITD Return (Gross) (Cumulative)'}]},
		{header: 'ITD Difference (Cumulative)', hidden: true, dataIndex: 'inceptionToDateReturnDifferenceCumulative', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent},
		{header: 'Portfolio ITD Return (Cumulative)', hidden: true, dataIndex: 'inceptionToDatePortfolioReturnCumulative', type: 'currency', useNull: true, width: 80, renderer: TCG.renderPercent, viewNameHeaders: [{name: 'Overlay', label: 'Portfolio ITD Return (Cumulative)'}, {name: 'Security Targets', label: 'ITD Return (Net) (Cumulative)'}]}
	],
	editor: {
		ptype: 'workflowAwareEntity-grideditor',
		detailPageClass: 'Clifton.performance.account.PerformanceAccountWindow',
		drillDownOnly: true,
		tableName: 'PerformanceInvestmentAccount',
		excludeAutomatic: true,
		getCorrectToStateName: function(fromState, toState, record) {
			if (this.getGridPanel().getCorrectToStateName) {
				return this.getGridPanel().getCorrectToStateName(fromState, toState, record);
			}
			return toState;
		}
	},

	isSecurityTargetSummary: function(record) {
		return TCG.isEquals('Performance Summary (VRP) Status', TCG.getValue('workflowState.workflow.name', record.json));
	}
});
Ext.reg('product-performanceSummaryWorkflowGrid', Clifton.product.performance.SummaryWorkflowGrid);
