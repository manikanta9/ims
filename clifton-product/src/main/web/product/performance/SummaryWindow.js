Clifton.product.performance.SummaryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Performance Summary',
	iconCls: 'chart-bar',
	width: 900,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,
		items: [
			{
				title: 'Summary Info',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'PerformanceInvestmentAccount',
					finalState: 'Posted (Archive)',
					reloadFormPanelAfterTransition: true
				},
				items: [{
					xtype: 'formpanel',
					url: 'performanceInvestmentAccount.json',
					instructions: 'Note: Portfolio Return accounts for all client assets (managed by this account as well as other managers).',
					labelWidth: 175,
					getWarningMessage: function(form) {
						return Clifton.rule.violation.getRuleViolationWarningMessage('summary', form.formValues);
					},
					loadDefaultDataAfterRender: true,

					reload: function() {
						const w = this.getWindow();
						this.getForm().setValues(TCG.data.getData('performanceInvestmentAccount.json?id=' + w.getMainFormId(), this), true);
						this.fireEvent('afterload', this);
					},

					executeProcessingAction: async function(url, params = {}) {
						const f = this;
						const w = this.getWindow();
						if (w.isModified() || !w.isMainFormSaved()) {
							await TCG.showError('Please save your changes before processing.');
							return false;
						}

						params['performanceAccountId'] = w.getMainFormId();

						await TCG.data.getDataPromise(url, this, {
							waitTarget: this,
							timeout: 40000,
							waitMsg: 'Processing...',
							params: params,
							conf: params
						});
						f.reload();
					},

					executeReport: function(format) {
						const w = this.getWindow();
						const id = w.getMainFormId();
						// Client Name + "Performance" + Year + Month Name
						const reportFileName = this.getFormValue('clientAccount.businessClient.name') + ' Performance ' + TCG.parseDate(this.getFormValue('accountingPeriod.endDate')).format('Y F');
						const params = {
							'performanceAccountId': id,
							'exportFormat': format,
							'reportFileName': reportFileName
						};
						TCG.downloadFile('performanceInvestmentAccountReportDownload.json', params, this);
					},

					items: [
						{fieldLabel: 'Client Account', name: 'clientAccount.label', hiddenName: 'clientAccount.id', xtype: 'combo', detailPageClass: 'Clifton.investment.account.AccountWindow', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'},
						{fieldLabel: 'Accounting Period', name: 'accountingPeriod.label', hiddenName: 'accountingPeriod.id', xtype: 'combo', url: 'accountingPeriodListFind.json?orderBy=startDate:desc', displayField: 'label'},
						{name: 'accountingPeriod.startDate', xtype: 'datefield', hidden: true, submitValue: false},
						{name: 'accountingPeriod.endDate', xtype: 'datefield', hidden: true, submitValue: false},
						{name: 'postedToPortal', boxLabel: 'Posted to Portal', xtype: 'checkbox', doNotSubmitValue: true, disabled: true, qtip: 'If checked, there is at least one approved file on the Portal associated with this performance summary.  Click Portal Files button to view more information.'},
						{xtype: 'label', html: '&nbsp;'},

						{
							fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
							defaults: {
								xtype: 'displayfield',
								style: 'text-align: right',
								flex: 1
							},
							items: [
								{value: 'Account Return'},
								{value: 'Benchmark Return'},
								{value: 'Difference'},
								{xtype: 'label', html: '&nbsp;', flex: 0.2},
								{value: 'Portfolio Return'}
							]
						},
						{
							fieldLabel: 'MTD Return', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								decimalPrecision: 4,
								readOnly: true,
								submitValue: false,
								flex: 1
							},
							items: [
								{name: 'monthToDateReturn'},
								{name: 'monthToDateBenchmarkReturn'},
								{name: 'monthToDateReturnDifference'},
								{xtype: 'label', html: '&nbsp;', flex: 0.2},
								{name: 'monthToDatePortfolioReturn'}
							]
						},
						{
							fieldLabel: 'QTD Return', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								decimalPrecision: 4,
								readOnly: true,
								submitValue: false,
								flex: 1
							},
							items: [
								{name: 'quarterToDateReturn'},
								{name: 'quarterToDateBenchmarkReturn'},
								{name: 'quarterToDateReturnDifference'},
								{xtype: 'label', html: '&nbsp;', flex: 0.2},
								{name: 'quarterToDatePortfolioReturn'}
							]
						},

						{
							fieldLabel: 'YTD Return', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								decimalPrecision: 4,
								readOnly: true,
								submitValue: false,
								flex: 1
							},
							items: [
								{name: 'yearToDateReturn'},
								{name: 'yearToDateBenchmarkReturn'},
								{name: 'yearToDateReturnDifference'},
								{xtype: 'label', html: '&nbsp;', flex: 0.2},
								{name: 'yearToDatePortfolioReturn'}
							]
						},
						{
							fieldLabel: 'ITD Return (Annualized)', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								decimalPrecision: 4,
								readOnly: true,
								submitValue: false,
								flex: 1
							},
							items: [
								{name: 'inceptionToDateReturn'},
								{name: 'inceptionToDateBenchmarkReturn'},
								{name: 'inceptionToDateReturnDifference'},
								{xtype: 'label', html: '&nbsp;', flex: 0.2},
								{name: 'inceptionToDatePortfolioReturn'}
							]
						},
						{
							fieldLabel: 'ITD Return (Cumulative)', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								decimalPrecision: 4,
								readOnly: true,
								submitValue: false,
								flex: 1
							},
							items: [
								{name: 'inceptionToDateReturnCumulative'},
								{name: 'inceptionToDateBenchmarkReturnCumulative'},
								{name: 'inceptionToDateReturnDifferenceCumulative'},
								{xtype: 'label', html: '&nbsp;', flex: 0.2},
								{name: 'inceptionToDatePortfolioReturnCumulative'}
							]
						}
					],
					enableDisableButton: function(btn, enableWorkflowStatusOnly) {

						let enable = true; // By Default After Load Button Can be enabled
						if (enableWorkflowStatusOnly) {
							const dt = TCG.getValue('workflowStatus.name', this.getForm().formValues);
							if (dt !== enableWorkflowStatusOnly) {
								enable = false;
							}
						}
						if (TCG.isTrue(enable)) {
							btn.enable();
						}
						else {
							btn.disable();
						}
					},
					buttons: [
						{
							text: 'Rebuild Snapshots',
							xtype: 'button',
							iconCls: 'run',
							width: 120,
							disabled: true,
							handler: async function() {
								const fp = TCG.getParentFormPanel(this);
								const startSnapshotDate = TCG.getChildByName(fp, 'accountingPeriod.startDate').getValue().format('m/d/Y');
								const endSnapshotDate = TCG.getChildByName(fp, 'accountingPeriod.endDate').getValue().format('m/d/Y');
								const params = {
									'clientAccountId': fp.getForm().formValues.clientAccount.id,
									'startSnapshotDate': startSnapshotDate,
									'endSnapshotDate': endSnapshotDate,
									'doNotThrowExceptionForFutureSnapshots': true,
									'synchronous': true
								};
								await fp.executeProcessingAction('accountingPositionDailyRebuild.json?UI_SOURCE=PerformanceSummary', params);
								await fp.executeProcessingAction('dwAccountingSnapshotsRebuild.json?UI_SOURCE=PerformanceSummary', params);
							},
							listeners: {
								afterRender: function() {
									const btn = this;
									const fp = TCG.getParentFormPanel(this);
									fp.on('afterload', function() {
										fp.enableDisableButton(btn, 'Draft');
									}, this);
								}
							}
						},
						{
							text: 'Process Portfolio Runs',
							xtype: 'button',
							iconCls: 'run',
							disabled: true,
							width: 120,
							handler: function() {
								const f = TCG.getParentFormPanel(this);
								f.executeProcessingAction('performanceInvestmentAccountProcess.json');
							},
							listeners: {
								afterRender: function() {
									const btn = this;
									const fp = TCG.getParentFormPanel(this);
									fp.on('afterload', function() {
										fp.enableDisableButton(btn, 'Draft');
									}, this);
								}
							}
						},

						{
							text: 'Process Violations',
							xtype: 'button',
							iconCls: 'warning',
							width: 120,
							disabled: true,
							handler: function() {
								const f = TCG.getParentFormPanel(this);
								f.executeProcessingAction('performanceInvestmentAccountWarningsProcess.json');
							},
							listeners: {
								afterRender: function() {
									const btn = this;
									const fp = TCG.getParentFormPanel(this);
									fp.on('afterload', function() {
										fp.enableDisableButton(btn, 'Draft');
									}, this);
								}
							}
						},

						{
							text: 'Performance Report',
							xtype: 'splitbutton',
							iconCls: 'pdf',
							disabled: true,
							width: 120,
							handler: function() {
								const f = TCG.getParentFormPanel(this);
								f.executeReport('PDF');
							},
							listeners: {
								afterRender: function() {
									const btn = this;
									const fp = TCG.getParentFormPanel(this);
									fp.on('afterload', function() {
										fp.enableDisableButton(btn);
									}, this);
								}
							},
							menu: {
								items: [{
									text: 'Excel',
									iconCls: 'excel_open_xml',
									tooltip: 'Generate the excel version of the report.',
									handler: function() {
										const f = TCG.getParentFormPanel(this);
										f.executeReport('EXCEL_OPEN_XML');
									}
								}, {
									text: 'Excel (97-2003)',
									iconCls: 'excel',
									tooltip: 'Generate the excel version of the report.',
									handler: function() {
										const f = TCG.getParentFormPanel(this);
										f.executeReport('EXCEL');
									}
								}]
							}
						},
						{
							text: 'Portal Files',
							xtype: 'button',
							iconCls: 'www',
							tooltip: 'View Portal Files associated with this performance summary.',
							width: 120,
							handler: function() {
								const f = TCG.getParentFormPanel(this);
								const className = 'Clifton.portal.file.FileListForSourceEntityWindow';
								const id = f.getFormValue('id');
								const cmpId = TCG.getComponentId(className, id);
								TCG.createComponent(className, {
									id: cmpId,
									sourceLabel: f.getFormValue('label'),
									sourceTableName: 'PerformanceInvestmentAccount',
									sourceFkFieldId: id
									// Looks like we only post P-Sums through workflow so no post url
								});
							}
						}
					]
				}]
			},

			{
				title: 'Violations',
				items: [{
					xtype: 'rule-violation-grid',
					tableName: 'PerformanceInvestmentAccount'

				}]
			},


			{
				title: 'Portfolio Runs',
				items: [{
					name: 'productPerformanceOverlayRunListByPerformanceSummary',
					xtype: 'gridpanel',
					instructions: 'Monthly performance is calculated by compounding daily performances of individual runs. Main Run for each business day is used included by default.',
					appendStandardColumns: false,
					wikiPage: 'IT/Performance Summaries',
					getLoadParams: function() {
						return {
							summaryId: this.getWindow().getMainFormId()
						};
					},
					isPagingEnabled: function() {
						return false;
					},
					exportSummaryQuery: function(queryName, outputFormat) {
						const params = {performanceAccountId: this.getWindow().getMainFormId()};
						params.outputFormat = outputFormat;
						params.queryName = queryName;
						TCG.downloadFile('performanceInvestmentAccountSystemQueryResult.json', params, this);
					},
					pageSize: 50,
					getTopToolbarFilters: function(toolBar) {
						const gridPanel = this;
						const menu = new Ext.menu.Menu();
						menu.add({
							text: 'Performance Summary Setup',
							handler: function() {
								gridPanel.openCustomFieldWindow('Performance Summary Fields');
							}
						});

						toolBar.add({
							text: 'Setup',
							tooltip: 'Account Setup',
							iconCls: 'account',
							menu: menu
						});
						toolBar.add('-');
					},
					openCustomFieldWindow: function(groupName) {
						const accountId = this.getWindow().getMainForm().formValues.clientAccount.id;
						Clifton.investment.account.openCliftonAccountCustomFieldWindow(groupName, accountId);
					},
					configureToolsMenu: function(menu) {
						const gp = this;
						const subMenu = new Ext.menu.Menu();
						subMenu.add({
							text: 'Overlay Targets',
							xtype: 'excel_splitbutton',
							scope: this,
							exportMethod: function(exportFormat) {
								gp.exportSummaryQuery('Performance Summary Asset Class Overlay Targets', exportFormat);
							}
						});
						subMenu.add({
							text: 'Overlay Targets',
							iconCls: 'pdf',
							scope: this,
							handler: function() {
								gp.exportSummaryQuery('Performance Summary Asset Class Overlay Targets', 'pdf');
							}
						});
						subMenu.add('-');
						subMenu.add({
							text: 'Gain Loss',
							xtype: 'excel_splitbutton',
							scope: this,
							exportMethod: function(exportFormat) {
								gp.exportSummaryQuery('Performance Summary Asset Class Gain Loss', exportFormat);
							}
						});
						subMenu.add({
							text: 'Gain Loss',
							iconCls: 'pdf',
							scope: this,
							handler: function() {
								gp.exportSummaryQuery('Performance Summary Asset Class Gain Loss', 'pdf');
							}
						});
						menu.add('-');
						menu.add({
							text: 'Export Asset Class Breakdown',
							iconCls: 'excel',
							scope: this,
							menu: subMenu
						});
					},
					columns: [
						{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
						{header: 'OverlayRunId', width: 30, dataIndex: 'portfolioRun.id', hidden: true},
						{header: 'Measure Date', width: 75, dataIndex: 'measureDate'},
						{
							header: 'Balance Date', width: 75, dataIndex: 'portfolioRunDate', align: 'center',
							renderer: function(v, p, r) {
								if (TCG.isBlank(r.data['portfolioRun.id'])) {
									p.css = 'ruleViolation';
									p.attr = 'qtip=\'No Overlay Run Available on this date to process\'';
								}
								return TCG.renderDate(v);
							}
						},
						{header: 'Financing Amount', hidden: true, width: 90, dataIndex: 'financingAmountOverride', type: 'currency', useNull: true},
						{header: 'Gain Loss (Calculated)', width: 90, dataIndex: 'gainLoss', type: 'currency', hidden: true},
						{header: 'Gain Loss (Override)', width: 90, dataIndex: 'gainLossOverride', type: 'currency', hidden: true, useNull: true},
						{
							header: 'Gain Loss', width: 90, dataIndex: 'coalesceGainLossOverride', type: 'currency', summaryType: 'sum',
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, TCG.isNotBlank(r.data['gainLossOverride']), r.data['gainLoss'], '0,000.00', 'Adjustment', true);
							},
							summaryRenderer: function(v) {
								if (TCG.isNotBlank(v)) {
									return TCG.renderAmount(v, true, '0,000.00');
								}
							}
						},
						{header: 'Gain Loss (Other) (Calculated)', width: 90, dataIndex: 'gainLossOther', type: 'currency', hidden: true},
						{header: 'Gain Loss (Other) (Override)', width: 90, dataIndex: 'gainLossOtherOverride', type: 'currency', hidden: true, useNull: true},
						{
							header: 'Gain Loss (Other)', width: 90, dataIndex: 'coalesceGainLossOtherOverride', type: 'currency', summaryType: 'sum',
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, TCG.isNotBlank(r.data['gainLossOtherOverride']), r.data['gainLossOther'], '0,000.00', 'Adjustment', true);
							},
							summaryRenderer: function(v) {
								if (TCG.isNotBlank(v)) {
									return TCG.renderAmount(v, true, '0,000.00');
								}
							}
						},
						{header: 'Gain Loss (Total)', width: 90, dataIndex: 'coalesceGainLossTotalOverride', type: 'currency', summaryType: 'sum', positiveInGreen: true, negativeInRed: true},
						{header: 'Overlay Target (Calculated)', width: 90, dataIndex: 'target', type: 'currency', hidden: true, numberFormat: '0,000'},
						{header: 'Overlay Target (Override)', width: 90, dataIndex: 'targetOverride', type: 'currency', hidden: true, numberFormat: '0,000', useNull: true},
						{
							header: 'Overlay Target', width: 90, dataIndex: 'coalesceTargetOverride', type: 'currency', numberFormat: '0,000',
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, TCG.isNotBlank(r.data['targetOverride']), r.data['overlayTarget'], '0,000');
							}
						},
						{header: 'Account Return', width: 85, dataIndex: 'accountReturn', type: 'currency', hidden: true, renderer: TCG.renderPercent},
						{header: 'Benchmark Return', width: 85, dataIndex: 'benchmarkReturn', type: 'currency', hidden: true, renderer: TCG.renderPercent},
						{
							header: 'Return Difference', width: 85, dataIndex: 'returnDifference', type: 'currency', numberFormat: '0,000.0000 %',
							renderer: function(v, metaData, r) {
								const format = '0,000.0000 %';
								const value = TCG.renderAmount(v, true, format);
								let qtip = '<table><tr><td>Account Return:</td><td align="right">' + Ext.util.Format.number(r.data.accountReturn, format) + '</td></tr>';
								qtip += '<tr><td>Benchmark Return:</td><td align="right">' + Ext.util.Format.number(r.data.benchmarkReturn, format) + '</td></tr>';
								qtip += '<tr><td>Difference:</td><td align="right">' + value + '</td></tr>';
								qtip += '</table>';
								metaData.attr = 'qtip=\'' + qtip + '\'';
								return value;
							}
						}

					],
					plugins: {ptype: 'gridsummary'},

					openDrillDownWindow: function(type, sm) {
						const gridPanel = this;
						if (sm.getCount() === 0) {
							TCG.showError('Please select a day for ' + type + '.', 'No Row(s) Selected');
						}
						else {
							const clientAccountId = TCG.getValue('clientAccount.id', this.getWindow().getMainForm().formValues);
							const measureDate = sm.getSelected().get('measureDate').format('m/d/Y');
							const portfolioRunDate = sm.getSelected().get('portfolioRunDate').format('m/d/Y');

							if ('Mark to Market' === type) {
								const m2mLoader = new TCG.data.JsonLoader({
									waitTarget: gridPanel,
									params: {clientAccountId: clientAccountId, date: measureDate},
									onLoad: function(record, conf) {
										if (record) {
											const config = {
												defaultDataIsReal: true,
												defaultData: record,
												params: {id: record.id}
											};
											TCG.createComponent('Clifton.accounting.m2m.M2MDailyWindow', config);
										}
										else {
											TCG.showError('Cannot find M2M daily associated with that record.');
										}
									}
								});
								m2mLoader.load('accountingM2MDailyByClientAccountAndDate.json?enableOpenSessionInView=true&requestedMaxDepth=4');
							}
							else if ('Gain Loss Details' === type) {

								TCG.createComponent('Clifton.performance.account.portfolio.RunGainLossDetailsWindow', {
									params: {
										clientInvestmentAccountId: clientAccountId,
										measureDate: measureDate,
										portfolioRunDate: portfolioRunDate
									}
								});
							}

						}

					},
					editor: {
						detailPageClass: 'Clifton.product.performance.overlay.RunWindow',
						drillDownOnly: true,
						addEditButtons: function(t) {
							t.add({
								text: 'Mark to Market',
								tooltip: 'View mark to market details for selected day',
								iconCls: 'exchange',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									this.getGridPanel().openDrillDownWindow('Mark to Market', sm);
								}
							});
							t.add('-');
							t.add({
								text: 'Gain/Loss Details',
								tooltip: 'View details for the daily gain/loss for selected day',
								iconCls: 'chart-pie',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									this.getGridPanel().openDrillDownWindow('Gain Loss Details', sm);
								}
							});
							t.add('-');
						}
					}
				}]
			},

			{
				title: 'Cash Attribution',
				items: [{
					xtype: 'product-performance-cash-attribution-grid',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: '', boxLabel: 'Include Sub-Accounts', xtype: 'toolbar-checkbox', name: 'includeSubAccounts', qtip: 'Optionally include client accounts related with purpose "Clifton Sub-Account" for this summary\'s account. Useful for reviewing data when the report is a rollup of sub-accounts and includes attribution for the sub-accounts.'}
						];
					},
					getLoadParams: function() {
						if (TCG.getChildByName(this.getTopToolbar(), 'includeSubAccounts') && TCG.isTrue(TCG.getChildByName(this.getTopToolbar(), 'includeSubAccounts').getValue())) {
							// Show the Client Account Column If Not Visible Already
							const cm = this.getColumnModel();
							cm.setHidden(cm.findColumnIndex('clientAccount.label'), false);
							return {clientAccountIdOrSubAccount: this.getWindow().getMainFormPanel().getFormValue('clientAccount.id'), accountingPeriodId: this.getWindow().getMainFormPanel().getFormValue('accountingPeriod.id')};
						}
						return {
							performanceSummaryId: this.getWindow().getMainFormId()
						};
					}
				}]
			},

			{
				title: 'Tracking Error',
				items: [{xtype: 'product-performance-tracking-error-grid'}]
			},


			{
				title: 'Benchmarks',
				items: [{ xtype: 'performance-account-benchmark-grid'}]
			},


			{
				title: 'Service Objectives',
				items: [{
					name: 'investmentAccountServiceObjectiveListFind',
					xtype: 'gridpanel',
					instructions: 'The following Service Objectives have been identified for selected client account.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Objective Name', width: 150, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'objectiveName'}},
						{
							header: 'Description', width: 300, dataIndex: 'objectiveDescription', filter: {searchFieldName: 'coalesceObjectiveDescription'},
							renderer: function(v, metaData, r) {
								if (TCG.isTrue(r.json.objectiveDescriptionOverridden)) {
									metaData.css = 'amountAdjusted';
								}
								return v;
							}
						},
						{header: 'Overridden', width: 50, dataIndex: 'objectiveDescriptionOverridden', type: 'boolean'}
					],
					getLoadParams: function() {
						return {investmentAccountId: TCG.getValue('clientAccount.id', this.getWindow().getMainForm().formValues)};
					},
					editor: {
						detailPageClass: 'Clifton.investment.account.AccountServiceObjectiveWindow',
						deleteURL: 'investmentAccountServiceObjectiveDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								referenceOne: TCG.getValue('clientAccount', gridPanel.getWindow().getMainForm().formValues)
							};
						}
					}
				}]
			},

			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'PerformanceInvestmentAccount',
					showOrderInfo: true,

					getEntityId: function() {
						return this.getWindow().getMainFormId();
					}
				}]
			},


			{
				title: 'Portal Files',
				reloadOnTabChange: true,
				items: [
					{
						xtype: 'portal-files-grid-for-source-entity',
						sourceTableName: 'PerformanceInvestmentAccount'
					}
				]
			}
		]
	}]
});
