Clifton.product.performance.overlay.RunWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Performance Overlay Run',
	iconCls: 'chart-bar',
	width: 1100,
	height: 550,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'productPerformanceOverlayRun.json?requestedMaxDepth=4',
					loadValidation: false, // entity extends a persisted entity and there is no direct table
					labelWidth: 140,
					defaults: {anchor: '-20'},
					labelFieldName: 'portfolioRun.id',

					items: [
						{fieldLabel: 'Performance Summary', name: 'performanceSummary.label', xtype: 'linkfield', detailIdField: 'performanceSummary.id', detailPageClass: 'Clifton.performance.account.PerformanceAccountWindow'},
						{
							fieldLabel: 'Portfolio Run', name: 'portfolioRun.label', hiddenName: 'portfolioRun.id', xtype: 'combo', detailPageClass: 'Clifton.portfolio.run.RunWindow', url: 'portfolioRunListFind.json', displayField: 'label', tooltipField: 'statusLabel',
							beforequery: function(queryEvent) {
								const f = queryEvent.combo.getParentForm().getForm();
								const balanceDate = f.findField('portfolioRunDate').value;
								const clientAccountId = TCG.getValue('performanceSummary.clientAccount.id', f.formValues);
								const parentClientAccountId = TCG.getValue('performanceSummary.parentClientAccount.id', f.formValues);
								// If there is a parent account - can use one of those runs OR those ran individually for the account
								if (TCG.isNotBlank(parentClientAccountId)) {
									queryEvent.combo.store.baseParams = {
										clientInvestmentAccountIds: [parentClientAccountId, clientAccountId],
										marketOnCloseAdjustmentsIncluded: false,
										balanceDate: balanceDate
									};
								}
								else {
									queryEvent.combo.store.baseParams = {
										clientInvestmentAccountId: clientAccountId,
										marketOnCloseAdjustmentsIncluded: false,
										balanceDate: balanceDate
									};
								}
							}
						},
						{fieldLabel: 'Balance Date', name: 'portfolioRunDate', xtype: 'datefield', readOnly: true},

						{
							fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
							defaults: {
								xtype: 'displayfield',
								flex: 1
							},
							items: [
								{value: 'Calculated Value'},
								{value: 'Override Value'},
								{value: 'Change'}
							]
						},
						{
							fieldLabel: 'Gain / Loss', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								readOnly: true,
								submitValue: false,
								flex: 1
							},
							items: [
								{name: 'gainLoss'},
								{name: 'gainLossOverride'},
								{name: 'gainLossDifference'}
							]
						},
						{
							fieldLabel: 'Overlay Target', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								readOnly: true,
								submitValue: false,
								flex: 1
							},
							items: [
								{name: 'target'},
								{name: 'targetOverride'},
								{name: 'targetDifference'}
							]
						},
						{
							fieldLabel: 'Gain / Loss (Other)', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								readOnly: true,
								submitValue: false,
								flex: 1
							},
							items: [
								{name: 'gainLossOther'},
								{name: 'gainLossOtherOverride', readOnly: false, submitValue: true},
								{name: 'gainLossOtherDifference'}
							]
						},
						{fieldLabel: 'Financing Amount', xtype: 'currencyfield', name: 'financingAmountOverride'},
						{fieldLabel: 'Account Return', xtype: 'currencyfield', decimalPrecision: 4, readOnly: true, submitValue: false, name: 'accountReturn'},
						{fieldLabel: 'Benchmark Return', xtype: 'currencyfield', decimalPrecision: 4, readOnly: true, submitValue: false, name: 'benchmarkReturn'},
						{fieldLabel: 'Return Difference', xtype: 'currencyfield', decimalPrecision: 4, readOnly: true, submitValue: false, name: 'returnDifference'},
						{
							xtype: 'formgrid',
							storeRoot: 'performanceOverlayAssetClassList',
							dtoClass: 'com.clifton.product.performance.overlay.ProductPerformanceOverlayAssetClass',
							readOnly: true,
							detailPageClass: 'Clifton.product.overlay.assetclass.AssetClassWindowSelector',
							getDetailPageId: function(grid, row) {
								return row.json.overlayAssetClass.id;
							},
							doNotSubmitFields: ['coalesceGainLossOverride', 'coalesceOverlayTargetOverride'],
							columnsConfig: [
								{header: 'ID', width: 10, dataIndex: 'id', hidden: true},
								{header: 'AID', width: 10, dataIndex: 'overlayAssetClass.id', hidden: true},
								{header: 'Asset Class', width: 175, dataIndex: 'overlayAssetClass.label'},
								{header: 'Benchmark', width: 175, dataIndex: 'benchmarkSecurity.label'},
								{header: 'Asset Class Return', width: 85, dataIndex: 'assetClassReturn', type: 'currency', numberFormat: '0,000.0000 %', hidden: true},
								{header: 'Benchmark Return', width: 85, dataIndex: 'benchmarkReturn', type: 'currency', numberFormat: '0,000.0000 %', hidden: true},
								{
									header: 'Return Difference', width: 85, dataIndex: 'returnDifference', type: 'currency', numberFormat: '0,000.0000 %',
									renderer: function(v, metaData, r) {
										const format = '0,000.0000 %';
										const value = TCG.renderAmount(v, true, format);
										let qtip = '<table><tr><td>Asset Class Return:</td><td align="right">' + Ext.util.Format.number(r.data.assetClassReturn, format) + '</td></tr>';
										qtip += '<tr><td>Benchmark Return:</td><td align="right">' + Ext.util.Format.number(r.data.benchmarkReturn, format) + '</td></tr>';
										qtip += '<tr><td>Difference:</td><td align="right">' + value + '</td></tr>';
										qtip += '</table>';
										metaData.attr = 'qtip=\'' + qtip + '\'';
										return value;
									}
								},
								{header: 'Financing Rate', width: 85, dataIndex: 'financingRate', type: 'currency', numberFormat: '0,000.0000 %'},
								{header: 'Gain Loss (Calculated)', hidden: true, width: 100, dataIndex: 'gainLoss', type: 'currency', negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
								{header: 'Gain Loss (Override)', hidden: true, width: 100, dataIndex: 'gainLossOverride', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
								{
									header: 'Gain Loss', width: 100, dataIndex: 'coalesceGainLossOverride', type: 'currency', summaryType: 'sum', editor: {xtype: 'currencyfield'},
									renderer: function(v, p, r) {
										return TCG.renderAdjustedAmount(v, TCG.isNotBlank(r.data['gainLossOverride']), r.data['gainLoss'], '0,000.00', 'Adjustment', true);
									},
									summaryRenderer: function(v) {
										if (TCG.isNotBlank(v)) {
											return TCG.renderAmount(v, false, '0,000.00');
										}
									}
								},

								{header: 'Overlay Target (Calculated)', hidden: true, width: 100, dataIndex: 'overlayTarget', type: 'currency', summaryType: 'sum'},
								{header: 'Overlay Target (Override)', hidden: true, width: 100, dataIndex: 'overlayTargetOverride', type: 'currency', useNull: true, summaryType: 'sum'},
								{
									header: 'Overlay Target', width: 100, dataIndex: 'coalesceOverlayTargetOverride', type: 'currency', summaryType: 'sum', editor: {xtype: 'currencyfield'},
									renderer: function(v, p, r) {
										return TCG.renderAdjustedAmount(v, TCG.isNotBlank(r.data['overlayTargetOverride']), r.data['overlayTarget'], '0,000.00');
									},
									summaryRenderer: function(v) {
										if (TCG.isNotBlank(v)) {
											return TCG.renderAmount(v, false, '0,000.00');
										}
									}
								},
								{header: 'Override Note', width: 400, dataIndex: 'overrideNote', editor: {xtype: 'textarea'}}
							],
							plugins: {ptype: 'gridsummary'},
							// submit the override only if different from calculated
							onAfterUpdateFieldValue: function(editor, field) {
								const f = editor.field;
								if (f === 'coalesceGainLossOverride') {
									const orig = editor.record.get('gainLoss');
									let val = editor.value;
									if (!Ext.isNumber(val)) {
										val = orig;
										editor.value = orig;
										editor.record.set(f, orig);
									}
									if (orig - val !== 0) {
										editor.record.set('gainLossOverride', val);
									}
									else {
										editor.record.set('gainLossOverride', '');
									}
								}
								if (f === 'coalesceOverlayTargetOverride') {
									const orig = editor.record.get('overlayTarget');
									let val = editor.value;
									if (!Ext.isNumber(val)) {
										val = orig;
										editor.value = orig;
										editor.record.set(f, orig);
									}
									if (orig - val !== 0) {
										editor.record.set('overlayTargetOverride', val);
									}
									else {
										editor.record.set('overlayTargetOverride', '');
									}
								}
							}
						}
					]
				}]
			},
			{
				title: 'Cash Attribution',
				items: [{
					xtype: 'product-performance-cash-attribution-grid',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: '', boxLabel: 'Include Sub-Accounts', xtype: 'toolbar-checkbox', name: 'includeSubAccounts', qtip: 'Optionally include client accounts related with purpose "Clifton Sub-Account" for this summary\'s account. Useful for reviewing data when the report is a rollup of sub-accounts and includes attribution for the sub-accounts.'}
						];
					},
					getLoadParams: function() {
						const params = {};
						params[this.isCurrentViewForAssetClass() ? 'overlayRunId' : 'id'] = this.getWindow().getMainFormId();

						if (TCG.getChildByName(this.getTopToolbar(), 'includeSubAccounts') && TCG.isTrue(TCG.getChildByName(this.getTopToolbar(), 'includeSubAccounts').getValue())) {
							// Show the Client Account Column If Not Visible Already
							const cm = this.getColumnModel();
							cm.setHidden(cm.findColumnIndex('clientAccount.label'), false);
							params.clientAccountIdOrSubAccount = this.getWindow().getMainFormPanel().getFormValue('performanceSummary.clientAccount.id');
							params.accountingPeriodId = this.getWindow().getMainFormPanel().getFormValue('performanceSummary.accountingPeriod.id');
						}

						return params;
					}
				}]
			},

			{
				title: 'Tracking Error',
				items: [{
					xtype: 'product-performance-tracking-error-grid',
					getSummaryId: function() {
						return this.getWindow().getMainForm().formValues.performanceSummary.id;
					},
					getStartMeasureDateDefault: function() {
						return TCG.parseDate(this.getWindow().getMainForm().formValues.performanceSummary.accountingPeriod.startDate);
					},
					getEndMeasureDateDefault: function() {
						return TCG.parseDate(this.getWindow().getMainForm().formValues.performanceSummary.accountingPeriod.endDate);
					}
				}]
			}
		]
	}]
});
