Clifton.product.performance.overlay.RunCashAttributionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Performance Overlay Run Cash Attribution',
	iconCls: 'chart-bar',
	width: 1000,
	height: 600,
	layout: 'border',

	items: [{
		xtype: 'formpanel',
		url: 'productPerformanceOverlayRunCashAttribution.json?requestedMaxDepth=4',
		loadValidation: false,
		labelWidth: 140,
		defaults: {anchor: '-5'},
		labelFieldName: 'portfolioRun.id',
		region: 'north',
		flex: 0.49,
		height: 300,

		listeners: {
			afterload: function() {
				const gp = this.ownerCt.items.get(2);
				gp.reload();
			}
		},

		items: [
			{fieldLabel: 'Performance Summary', name: 'performanceSummary.label', xtype: 'linkfield', detailIdField: 'performanceSummary.id', detailPageClass: 'Clifton.performance.account.PerformanceAccountWindow'},
			{fieldLabel: 'Portfolio Run', name: 'portfolioRun.label', xtype: 'linkfield', detailIdField: 'portfolioRun.id', detailPageClass: 'Clifton.portfolio.run.RunWindow'},
			{fieldLabel: 'Measure Date', name: 'measureDate', xtype: 'datefield'},
			{
				xtype: 'formgrid-scroll',
				storeRoot: 'assetClassList',
				dtoClass: 'com.clifton.product.performance.overlay.cash.ProductPerformanceOverlayAssetClassCashAttribution',
				readOnly: true,
				doNotSubmitFields: ['coalesceAttributionReturnOverride'],
				columnsConfig: [
					{
						header: '&nbsp;', width: 35, dataIndex: 'attributionCalculationNote',
						renderer: function(v, metaData, r) {
							if (TCG.isNotBlank(v)) {
								const imgCls = 'flag-red';
								const tooltip = v;
								return '<span ext:qtip="' + tooltip + '"><span style="width: 16px; height: 16px; float: left;" class="' + imgCls + '" ext:qtip="' + tooltip + '">&nbsp;</span> </span>';
							}
						}
					},
					{header: 'Asset Class', width: 200, dataIndex: 'overlayAssetClass.label'},

					{header: 'Fund Cash Attribution', width: 110, dataIndex: 'fundCashAttribution', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true},
					{header: 'Manager Cash Attribution', width: 110, dataIndex: 'managerCashAttribution', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true},
					{header: 'Transition Cash Attribution', width: 110, dataIndex: 'transitionCashAttribution', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true},
					{header: 'Rebalance Cash Attribution', width: 110, dataIndex: 'rebalanceCashAttribution', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true},
					{header: 'Client Directed Cash Attribution', width: 110, dataIndex: 'clientDirectedCashAttribution', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true},
					{header: 'Gain Loss', width: 110, dataIndex: 'coalesceGainLossOverride', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true},
					{header: 'Overlay Exposure', width: 110, dataIndex: 'overlayAssetClass.overlayExposure', type: 'currency', summaryType: 'sum', negativeInRed: true, positiveInGreen: true},
					{header: 'Attribution (%) (Calculated)', hidden: true, width: 100, dataIndex: 'attributionReturn', type: 'percent', numberFormat: '0,000.0000', negativeInRed: true, positiveInGreen: true},
					{header: 'Attribution (%) (Override)', hidden: true, width: 100, dataIndex: 'attributionReturnOverride', type: 'percent', useNull: true, numberFormat: '0,000.0000', negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
					{
						header: 'Attribution (%)', width: 100, dataIndex: 'coalesceAttributionReturnOverride', type: 'percent', summaryType: 'sum', editor: {xtype: 'currencyfield', decimalPrecision: 4},
						renderer: function(v, p, r) {
							return TCG.renderAdjustedAmount(v, TCG.isNotBlank(r.data['attributionReturnOverride']), r.data['attributionReturn'], '0,000.0000', 'Adjustment', true);
						}
					},
					{header: 'Plug Attribution', width: 100, dataIndex: 'plugAttribution', type: 'currency', useNull: true, summaryType: 'sum', negativeInRed: true, positiveInGreen: true},
					{header: 'Plug Cash Type', width: 75, dataIndex: 'plugAttributionCashType'}
				],
				plugins: {ptype: 'gridsummary'},
				// submit the override only if different from calculated
				onAfterUpdateFieldValue: function(editor, field) {
					const f = editor.field;
					if (f === 'coalesceAttributionReturnOverride') {
						const orig = editor.record.get('attributionReturn');
						let val = editor.value;
						if (!Ext.isNumber(val)) {
							val = orig;
							editor.value = orig;
							editor.record.set(f, orig);
						}
						if (orig - val !== 0) {
							editor.record.set('attributionReturnOverride', val);
						}
						else {
							editor.record.set('attributionReturnOverride', '');
						}
					}
				}

			}
		]
	}, {
		flex: 0.02
	}, {

		name: 'productOverlayAssetClassListFind',
		xtype: 'gridpanel',
		title: 'Cash Summary',
		instructions: 'Implementing client account policy results in the following cash allocations to asset classes. Totals for each asset class need to be overlayed using corresponding replications. The total cash to be overlayed may not equal to total manager cash because not all cash is always overlayed and some cash amounts (rebalance cash) do not represent real cash and may get adjusted based on corresponding benchmark performance.',
		flex: 0.49,
		region: 'center',
		reloadOnRender: false,

		getLoadParams: function() {
			const w = this.getWindow();
			const f = w.getMainForm();

			const nonRollupOnly = TCG.getChildByName(this.getTopToolbar(), 'nonRollupOnly').getValue();
			if (TCG.isTrue(nonRollupOnly)) {
				return {
					runId: TCG.getValue('portfolioRun.id', f.formValues),
					populateCashAdjustments: true,
					rollupAssetClass: false
				};
			}
			return {
				runId: TCG.getValue('portfolioRun.id', f.formValues),
				populateCashAdjustments: true,
				rootOnly: true
			};
		},
		topToolbarUpdateCountPrefix: '-',
		getTopToolbarFilters: function(toolbar) {
			return [
				{boxLabel: 'View Non Rollup Asset Class Level&nbsp;', xtype: 'toolbar-checkbox', name: 'nonRollupOnly'}
			];
		},
		isPagingEnabled: function() {
			return false;
		},
		pageSize: 500,
		columns: [
			{header: 'Asset Class', width: 140, dataIndex: 'label'},
			{header: 'IsTargetAdjusted', width: 50, dataIndex: 'targetAdjusted', type: 'boolean', hidden: true},
			{header: 'TFA', width: 50, dataIndex: 'targetFollowsActual', type: 'boolean', hidden: true},
			{header: 'Original Target', width: 80, dataIndex: 'targetAllocation', type: 'currency', summaryType: 'sum', hidden: true},
			{header: 'Original Target %', width: 80, dataIndex: 'targetAllocationPercent', type: 'percent', summaryType: 'sum', hidden: true},

			// Cash Columns
			{
				header: 'Fund Cash (Original)', width: 80, hidden: true, dataIndex: 'fundCash', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Cash held in a cash only account.  This is essentially a checking/savings account for the fund.  The fund holds a certain amount of cash to pay expenses, make pension payments, fund new managers, etc.'
			},
			{
				header: 'Fund Cash', width: 80, dataIndex: 'fundCashWithAdjustments', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Cash held in a cash only account.  This is essentially a checking/savings account for the fund.  The fund holds a certain amount of cash to pay expenses, make pension payments, fund new managers, etc.',
				renderer: function(v, metaData, r) {
					const original = r.data.fundCash;
					return TCG.renderAdjustedAmount(v, (v - original !== 0), original, '0,000', 'Manual Cash Adjustments:', false);
				}
			},

			{
				header: 'Manager Cash (Original)', width: 80, hidden: true, dataIndex: 'managerCash', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Cash held within an investment manager in an asset class other than the Cash asset class.'
			},
			{
				header: 'Manager Cash', width: 80, dataIndex: 'managerCashWithAdjustments', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Cash held within an investment manager in an asset class other than the Cash asset class.',
				renderer: function(v, metaData, r) {
					const original = r.data.managerCash;
					return TCG.renderAdjustedAmount(v, (v - original !== 0), original, '0,000', 'Manual Cash Adjustments:', false);
				}
			},


			{header: 'Transition Cash (Original)', width: 80, hidden: true, dataIndex: 'transitionCash', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
			{
				header: 'Transition Cash', width: 80, dataIndex: 'transitionCashWithAdjustments', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				renderer: function(v, metaData, r) {
					const original = r.data.transitionCash;
					return TCG.renderAdjustedAmount(v, (v - original !== 0), original, '0,000', 'Manual Cash Adjustments:', false);
				}
			},

			{
				header: 'Rebalance Cash (Original)', width: 80, dataIndex: 'rebalanceCash', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hidden: true, negativeInRed: true,
				tooltip: 'This is a cash \'proxy\'.  When rebalancing a client, we use offsetting positive and negative cash values by asset class to bring the fund back in line with their asset class targets.  This cash value is assigned to an asset class and generally has a benchmark associated with it.'
			},
			{header: 'Rebalance Cash Adjusted', width: 80, dataIndex: 'rebalanceCashAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hidden: true, negativeInRed: true},
			{
				header: 'Rebalance Attribution Cash', width: 80, dataIndex: 'rebalanceAttributionCash', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hidden: true, negativeInRed: true,
				tooltip: 'Generated during minimize imbalances processing and used to remove negative Manager, Fund, and Transition Cash from their buckets in order to properly perform attribution analysis.'
			},
			{
				header: 'Rebalance Cash', width: 80, dataIndex: 'rebalanceCashWithAdjustments', type: 'currency', summaryType: 'sum', negativeInRed: true,
				tooltip: '<b>Rebalance Cash/Adjusted:&nbsp;</b>A cash \'proxy\'.  When rebalancing a client, we use offsetting positive and negative cash values by asset class to bring the fund back in line with their asset class targets.  This cash value is assigned to an asset class and generally has a benchmark associated with it.<br/><br/>'
					+ '<b>Attribution Cash:&nbsp;</b>Generated during minimize imbalancing processing and used to remove negative Manager, Fund, and Transition Cash from their buckets in order to properly perform attribution analysis.<br/><br/>',
				renderer: function(v, metaData, r) {
					const rebalCash = r.data['rebalanceCash'];
					const rebalAdjustment = r.data['rebalanceCashAdjusted'] - rebalCash;
					const rebalAttribution = r.data['rebalanceAttributionCash'];
					const rebalManualAdj = v - rebalAttribution - r.data['rebalanceCashAdjusted'];
					if (rebalAdjustment !== 0 || rebalAttribution !== 0 || rebalManualAdj !== 0) {
						let qtip = '<table><tr><td>Original Rebalance Cash:</td><td align="right">' + TCG.renderAmount(rebalCash, true, '0,000') + '</td></tr>';
						qtip += '<tr><td>Adjustment for Benchmark:</td><td align="right">' + TCG.renderAmount(rebalAdjustment, true, '0,000') + '</td></tr>';
						qtip += '<tr><td>Attribution Rebalancing:</td><td align="right">' + TCG.renderAmount(rebalAttribution, true, '0,000') + '</td></tr>';
						qtip += '<tr><td>Manual Adjustments:</td><td align="right">' + TCG.renderAmount(rebalManualAdj, true, '0,000') + '</td></tr>';
						qtip += '<tr><td>&nbsp;</td><td><hr/></td></tr>';
						qtip += '<tr><td>Total Rebalance Cash:</td><td align="right">' + TCG.renderAmount(v, true, '0,000') + '</td></tr>';
						qtip += '</table>';
						metaData.css = 'amountAdjusted';
						metaData.attr = 'qtip=\'' + qtip + '\'';
					}
					return Ext.util.Format.number(v, '0,000');
				}
			},
			{
				header: 'Effective Total', width: 80, dataIndex: 'effectiveCashTotal', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, css: 'BORDER-LEFT: #c0c0c0 1px solid;',
				tooltip: 'Effective Cash = Manager Cash + Fund Cash + Transition Cash + Rebalance Cash Adjusted<br/><br/>NOTE: Client Directed Cash is excluded from Effective Cash Total'
			},
			{
				header: 'Client Directed Cash (Original)', width: 80, hidden: true, dataIndex: 'clientDirectedCash', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Defined on the Account Asset Class and used to generate trade recommendations only when the client instructs us to (except for when there are imbalances within the asset class)'
			},
			{
				header: 'Client Directed Cash', width: 80, dataIndex: 'clientDirectedCashWithAdjustments', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Defined on the Account Asset Class and used to generate trade recommendations only when the client instructs us to (except for when there are imbalances within the asset class)',
				renderer: function(v, metaData, r) {
					const original = r.data.clientDirectedCash;
					return TCG.renderAdjustedAmount(v, (v - original !== 0), original, '0,000', 'Manual Cash Adjustments:', false);
				}
			},
			{
				header: 'Total', width: 80, dataIndex: 'cashTotal', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, css: 'BORDER-LEFT: #c0c0c0 1px solid;',
				tooltip: 'Cash Total = Effective Cash (Manager Cash + Fund Cash + Transition Cash + Total Rebalance Cash (Adjusted + Attribution)) + Client Directed Cash'
			}
		],
		plugins: {ptype: 'gridsummary'},
		editor: {
			detailPageClass: 'Clifton.product.overlay.assetclass.AssetClassWindowSelector',
			drillDownOnly: true
		}

	}]
});
