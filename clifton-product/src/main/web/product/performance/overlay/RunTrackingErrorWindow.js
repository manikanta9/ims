Clifton.product.performance.overlay.RunTrackingErrorWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Performance Overlay Run Tracking Error',
	iconCls: 'chart-bar',
	width: 1000,
	height: 600,
	layout: 'border',

	items: [{
		xtype: 'formpanel',
		url: 'productPerformanceOverlayRun.json',
		loadValidation: false,
		readOnly: true,
		labelWidth: 140,
		defaults: {anchor: '-5'},
		labelFieldName: 'portfolioRun.id',
		region: 'north',
		flex: 0.49,
		height: 125,

		listeners: {
			afterload: function() {
				const gp = this.ownerCt.items.get(1);
				gp.reload();
			}
		},

		items: [
			{fieldLabel: 'Performance Summary', name: 'performanceSummary.label', xtype: 'linkfield', detailIdField: 'performanceSummary.id', detailPageClass: 'Clifton.performance.account.PerformanceAccountWindow'},
			{fieldLabel: 'Portfolio Run', name: 'portfolioRun.label', xtype: 'linkfield', detailIdField: 'portfolioRun.id', detailPageClass: 'Clifton.portfolio.run.RunWindow'},
			{fieldLabel: 'Measure Date', name: 'measureDate', xtype: 'datefield'},
			{fieldLabel: 'Portfolio Wide', name: 'portfolioWide', xtype: 'checkbox', readOnly: true}
		]
	}, {

		name: 'productPerformanceOverlayAssetClassTrackingErrorListByPerformanceOverlayRun',
		xtype: 'gridpanel',
		flex: 0.49,
		region: 'center',
		reloadOnRender: false,

		getLoadParams: function(firstLoad) {
			const w = this.getWindow();
			const f = w.getMainForm();

			if (firstLoad) {
				const portfolioWideField = TCG.getChildByName(this.getWindow().items.items[0], 'portfolioWide');
				portfolioWideField.setValue(this.getWindow().defaultData.portfolioWide);
			}

			return {
				performanceOverlayRunId: TCG.getValue('id', f.formValues),
				portfolioWide: TCG.getValue('portfolioWide', f.formValues)
			};
		},
		isPagingEnabled: function() {
			return false;
		},
		pageSize: 500,
		columns: [
			{header: 'Asset Class', width: 200, dataIndex: 'productOverlayAssetClass.accountAssetClass.label'},
			{header: 'Benchmark', width: 150, dataIndex: 'productOverlayAssetClass.benchmarkSecurity.symbol'},
			{
				header: 'Benchmark Return', width: 110, dataIndex: 'benchmarkReturn', type: 'float', renderer: TCG.renderPercent, positiveInGreen: true, negativeInRed: true,
				tooltip: 'Daily Asset Class Benchmark Return'
			},
			{
				header: 'Portfolio Benchmark Return', width: 110, dataIndex: 'adjustedTargetAllocationPercent', type: 'float', useNull: true, renderer: TCG.renderPercent, summaryType: 'sum', positiveInGreen: true, negativeInRed: true,
				tooltip: 'Portfolio Adjusted Target % x Daily Asset Class Benchmark Return'
			},
			{
				header: 'Portfolio Return - Physical Exposure', width: 110, dataIndex: 'physicalExposurePercent', type: 'float', useNull: true, renderer: TCG.renderPercent, summaryType: 'sum', positiveInGreen: true, negativeInRed: true,
				tooltip: 'Portfolio Physical Exposure % x Daily Asset Class Benchmark Return'
			},
			{
				header: 'Portfolio Return - Total Exposure', width: 110, dataIndex: 'totalExposurePercent', type: 'float', useNull: true, renderer: TCG.renderPercent, summaryType: 'sum', positiveInGreen: true, negativeInRed: true,
				tooltip: 'Portfolio Total Exposure % x Daily Asset Class Benchmark Return'
			}
		],
		plugins: {ptype: 'gridsummary'}
	}]
});
