Clifton.product.performance.upload.ProductPerformanceSummaryUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'productPerformanceSummaryUploadWindow',
	title: 'Product Performance Summary Upload',
	iconCls: 'import',
	height: 480,
	width: 700,
	hideOKButton: true,

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File',
		handler: function() {
			TCG.openFile('product/performance/upload/ProductPerformanceSummaryUploadFileSample.xls');
		}
	}],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Easily upload historical performance summaries into IMS. These uploads must follow the format specified in the sample file.',

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', displayField: 'label', xtype: 'combo', allowBlank: false, url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'clientInvestmentAccount.id'},
			{xtype: 'sectionheaderfield', header: 'If uploaded row already exists in the system'},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: '',
				items: [
					{fieldLabel: '', boxLabel: 'Skip upload for rows that already exist.', name: 'existingBeans', inputValue: 'SKIP', checked: true},
					{fieldLabel: '', boxLabel: 'Overwrite existing data with uploaded information. (Columns not included in the file will NOT overwrite existing data in the database)', name: 'existingBeans', inputValue: 'UPDATE'},
					{fieldLabel: '', boxLabel: 'Insert all data to improve upload performance (Fails if a constraint is violated).', name: 'existingBeans', inputValue: 'INSERT'}
				]

			},
			{xtype: 'sectionheaderfield', header: 'Upload Results'},
			{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
		],

		getSaveURL: function() {
			return 'productPerformanceSummaryUploadFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true';
		}

	}]
});
