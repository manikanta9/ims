package com.clifton.product.performance.upload;


import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>ProductPerformanceSummaryUploadTemplate</code> represents daily/monthly performance summary and performance overlay run
 * values.
 *
 * @author apopp
 */
public class ProductPerformanceSummaryUploadTemplate {

	private Date measureDate; //Date on column

	//ProductPerformanceOverlayRun attributes
	private BigDecimal gainLoss; // since previous performance detail (business day) - sum of asset classes
	private BigDecimal overlayTarget; // SUM(assetClassOverlayTarget) -

	//ProductPerformanceSummary level attributes
	private BigDecimal monthToDateReturn; // (1 + Day 1 Return) * (1 + Day 2 Return) * ... * (1 + Day N Return) - 1
	private BigDecimal monthToDateBenchmarkReturn;
	private BigDecimal monthToDatePortfolioReturn; // SUM(Gain/Loss) for the month divided by the First Date's Overlay Run's Total Portfolio Value


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public BigDecimal getGainLoss() {
		return this.gainLoss;
	}


	public void setGainLoss(BigDecimal gainLoss) {
		this.gainLoss = gainLoss;
	}


	public BigDecimal getOverlayTarget() {
		return this.overlayTarget;
	}


	public void setOverlayTarget(BigDecimal overlayTarget) {
		this.overlayTarget = overlayTarget;
	}


	public BigDecimal getMonthToDateReturn() {
		return this.monthToDateReturn;
	}


	public void setMonthToDateReturn(BigDecimal monthToDateReturn) {
		this.monthToDateReturn = monthToDateReturn;
	}


	public BigDecimal getMonthToDateBenchmarkReturn() {
		return this.monthToDateBenchmarkReturn;
	}


	public void setMonthToDateBenchmarkReturn(BigDecimal monthToDateBenchmarkReturn) {
		this.monthToDateBenchmarkReturn = monthToDateBenchmarkReturn;
	}


	public BigDecimal getMonthToDatePortfolioReturn() {
		return this.monthToDatePortfolioReturn;
	}


	public void setMonthToDatePortfolioReturn(BigDecimal monthToDatePortfolioReturn) {
		this.monthToDatePortfolioReturn = monthToDatePortfolioReturn;
	}
}
