package com.clifton.product.performance.overlay;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;

import java.util.List;


/**
 * <code>ProductPerformancePortfolioRun</code> is an extension of {@link PerformancePortfolioRun}
 * to attach related collections for performance processing.
 *
 * @author nickk
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class ProductPerformancePortfolioRun extends PerformancePortfolioRun {

	private List<ProductPerformanceOverlayAssetClassTrackingError> performanceOverlayAssetClassTrackingErrorList;
	private List<ProductPerformanceOverlayAssetClass> performanceOverlayAssetClassList;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static ProductPerformancePortfolioRun fromPerformancePortfolioRun(PerformancePortfolioRun performancePortfolioRun) {
		ProductPerformancePortfolioRun productRun = new ProductPerformancePortfolioRun();
		BeanUtils.copyProperties(performancePortfolioRun, productRun);
		return productRun;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public List<ProductPerformanceOverlayAssetClassTrackingError> getPerformanceOverlayAssetClassTrackingErrorList() {
		return this.performanceOverlayAssetClassTrackingErrorList;
	}


	public void setPerformanceOverlayAssetClassTrackingErrorList(List<ProductPerformanceOverlayAssetClassTrackingError> performanceOverlayAssetClassTrackingErrorList) {
		this.performanceOverlayAssetClassTrackingErrorList = performanceOverlayAssetClassTrackingErrorList;
	}


	public List<ProductPerformanceOverlayAssetClass> getPerformanceOverlayAssetClassList() {
		return this.performanceOverlayAssetClassList;
	}


	public void setPerformanceOverlayAssetClassList(List<ProductPerformanceOverlayAssetClass> performanceOverlayAssetClassList) {
		this.performanceOverlayAssetClassList = performanceOverlayAssetClassList;
	}
}
