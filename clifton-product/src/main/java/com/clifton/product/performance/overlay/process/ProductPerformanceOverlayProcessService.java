package com.clifton.product.performance.overlay.process;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.product.performance.overlay.ProductPerformancePortfolioRun;

import java.util.Date;


/**
 * The <code>ProductPerformanceOverlayProcessService</code> ...
 *
 * @author manderson
 */
public interface ProductPerformanceOverlayProcessService {

	/**
	 * Admin only.  Intended for back-filling tracking error calculations.  The method will
	 * process all summaries where their measure date is within the month defined by
	 * the provided measureDate. It also optionally allows filtering to a specific client account id.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void processProductPerformanceInvestmentAccountListTrackingErrorByMeasureDate(Date measureDate, Integer clientAccountId);


	/**
	 * Save method called from UI - validates edits - updates overrides/main run & re-processes for that date
	 */
	@SecureMethod(dtoClass = PerformancePortfolioRun.class)
	public ProductPerformancePortfolioRun saveProductPerformanceOverlayRun(ProductPerformancePortfolioRun bean);
}
