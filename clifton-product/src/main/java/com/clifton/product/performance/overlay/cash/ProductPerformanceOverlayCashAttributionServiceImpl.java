package com.clifton.product.performance.overlay.cash;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayAssetClass;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayService;
import com.clifton.product.performance.overlay.cash.search.OverlayAssetClassCashAttributionSearchForm;
import com.clifton.product.performance.overlay.cash.search.OverlayRunCashAttributionSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The <code>ProductPerformanceOverlayRunCashAttributionServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class ProductPerformanceOverlayCashAttributionServiceImpl implements ProductPerformanceOverlayCashAttributionService {

	private AdvancedReadOnlyDAO<ProductPerformanceOverlayRunCashAttribution, Criteria> productPerformanceOverlayRunCashAttributionDAO;
	private AdvancedReadOnlyDAO<ProductPerformanceOverlayAssetClassCashAttribution, Criteria> productPerformanceOverlayAssetClassCashAttributionDAO;

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private ProductPerformanceOverlayService productPerformanceOverlayService;

	////////////////////////////////////////////////////////////////
	////  ProductPerformanceOverlayRunCashAttribution Methods   ////
	////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public ProductPerformanceOverlayRunCashAttribution getProductPerformanceOverlayRunCashAttribution(int id) {
		ProductPerformanceOverlayRunCashAttribution runAttribution = getProductPerformanceOverlayRunCashAttributionDAO().findByPrimaryKey(id);
		if (runAttribution != null) {
			runAttribution.setAssetClassList(getProductPerformanceOverlayAssetClassCashAttributionListByPerformanceOverlayRun(id));
		}
		return runAttribution;
	}


	@Override
	@Transactional
	public ProductPerformanceOverlayRunCashAttribution saveProductPerformanceOverlayRunCashAttribution(ProductPerformanceOverlayRunCashAttribution bean) {
		List<ProductPerformanceOverlayAssetClassCashAttribution> assetClassAttributionList = bean.getAssetClassList();
		// Only updates the Performance Attribution Return Override Field
		List<ProductPerformanceOverlayAssetClass> assetClassList = getProductPerformanceOverlayService().getProductPerformanceOverlayAssetClassListByPerformanceOverlayRun(bean.getPerformanceOverlayRunId());
		for (ProductPerformanceOverlayAssetClass overlayAssetClass : CollectionUtils.getIterable(assetClassList)) {
			for (ProductPerformanceOverlayAssetClassCashAttribution overlayAssetClassCashAttribution : CollectionUtils.getIterable(assetClassAttributionList)) {
				if (MathUtils.isEqual(overlayAssetClassCashAttribution.getId(), overlayAssetClass.getId())) {
					overlayAssetClass.setAttributionReturnOverride(overlayAssetClassCashAttribution.getAttributionReturnOverride());
				}
			}
		}
		getProductPerformanceOverlayService().saveProductPerformanceOverlayAssetClassListForRun(bean.getPerformanceOverlayRunId(), assetClassList);
		return getProductPerformanceOverlayRunCashAttribution(bean.getId());
	}


	@Override
	@Transactional(readOnly = true)
	public List<ProductPerformanceOverlayRunCashAttribution> getProductPerformanceOverlayRunCashAttributionList(OverlayRunCashAttributionSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (searchForm.getClientAccountIdOrSubAccount() != null) {
					appendClientAccountOrSubAccountCriteria(criteria, searchForm.getClientAccountIdOrSubAccount(), getPathAlias("performanceSummary", criteria));
				}
			}
		};
		return getProductPerformanceOverlayRunCashAttributionDAO().findBySearchCriteria(config);
	}

	///////////////////////////////////////////////////////////////////////
	////  ProductPerformanceOverlayAssetClassCashAttribution Methods   ////
	///////////////////////////////////////////////////////////////////////


	private List<ProductPerformanceOverlayAssetClassCashAttribution> getProductPerformanceOverlayAssetClassCashAttributionListByPerformanceOverlayRun(int performanceOverlayRunId) {
		return getProductPerformanceOverlayAssetClassCashAttributionDAO().findByField("performanceOverlayRun.id", performanceOverlayRunId);
	}


	@Override
	public List<ProductPerformanceOverlayAssetClassCashAttribution> getProductPerformanceOverlayAssetClassCashAttributionList(OverlayAssetClassCashAttributionSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (searchForm.getClientAccountIdOrSubAccount() != null) {
					appendClientAccountOrSubAccountCriteria(criteria, searchForm.getClientAccountIdOrSubAccount(), getPathAlias("performanceOverlayRun.performanceSummary", criteria));
				}
			}
		};
		return getProductPerformanceOverlayAssetClassCashAttributionDAO().findBySearchCriteria(config);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////                   Helper Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Appends appropriately equals or in clause for the given account to the criteria based on the account's current sub-accounts
	 * The account filter is applied at the summary level, so also pass the summary table alias so the account filter can be applied there
	 */
	private void appendClientAccountOrSubAccountCriteria(Criteria criteria, Integer clientAccountOrSubAccount, String summaryAlias) {
		List<InvestmentAccountRelationship> relList = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipListForPurposeActive(
				clientAccountOrSubAccount, InvestmentAccountRelationshipPurpose.CLIFTON_SUB_ACCOUNT_PURPOSE_NAME, true);
		if (CollectionUtils.isEmpty(relList)) {
			criteria.add(Restrictions.eq(summaryAlias + ".clientAccount.id", clientAccountOrSubAccount));
		}
		else {
			Object[] accountIds = BeanUtils.getPropertyValues(relList, "referenceTwo.id");
			accountIds = ArrayUtils.add(accountIds, clientAccountOrSubAccount);
			criteria.add(Restrictions.in(summaryAlias + ".clientAccount.id", accountIds));
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////                Getter & Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedReadOnlyDAO<ProductPerformanceOverlayRunCashAttribution, Criteria> getProductPerformanceOverlayRunCashAttributionDAO() {
		return this.productPerformanceOverlayRunCashAttributionDAO;
	}


	public void setProductPerformanceOverlayRunCashAttributionDAO(AdvancedReadOnlyDAO<ProductPerformanceOverlayRunCashAttribution, Criteria> productPerformanceOverlayRunCashAttributionDAO) {
		this.productPerformanceOverlayRunCashAttributionDAO = productPerformanceOverlayRunCashAttributionDAO;
	}


	public AdvancedReadOnlyDAO<ProductPerformanceOverlayAssetClassCashAttribution, Criteria> getProductPerformanceOverlayAssetClassCashAttributionDAO() {
		return this.productPerformanceOverlayAssetClassCashAttributionDAO;
	}


	public void setProductPerformanceOverlayAssetClassCashAttributionDAO(
			AdvancedReadOnlyDAO<ProductPerformanceOverlayAssetClassCashAttribution, Criteria> productPerformanceOverlayAssetClassCashAttributionDAO) {
		this.productPerformanceOverlayAssetClassCashAttributionDAO = productPerformanceOverlayAssetClassCashAttributionDAO;
	}


	public ProductPerformanceOverlayService getProductPerformanceOverlayService() {
		return this.productPerformanceOverlayService;
	}


	public void setProductPerformanceOverlayService(ProductPerformanceOverlayService productPerformanceOverlayService) {
		this.productPerformanceOverlayService = productPerformanceOverlayService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}
}
