package com.clifton.product.performance.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.benchmark.PerformanceSummaryBenchmark;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Rule Evaluator for PerformanceInvestmentAccounts. Creates a violation when the performance summary benchmark, related to the performance investment account, is missing.
 *
 * @author jonathanr
 */
public class ProductPerformanceSummaryBenchmarkRuleEvaluator extends BaseRuleEvaluator<PerformanceInvestmentAccount, ProductPerformanceInvestmentAccountRuleEvaluatorContext> {

	@Override
	public List<RuleViolation> evaluateRule(PerformanceInvestmentAccount performanceInvestmentAccount, RuleConfig ruleConfig, ProductPerformanceInvestmentAccountRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<PerformanceSummaryBenchmark> performanceSummaryBenchmarks = context.getPerformanceSummaryBenchmarkList(performanceInvestmentAccount);
			List<String> violationNotes = new ArrayList<>();
			for (PerformanceSummaryBenchmark performanceSummaryBenchmark : CollectionUtils.getIterable(performanceSummaryBenchmarks)) {
				if (performanceSummaryBenchmark.getCoalesceMonthToDateReturnOverride() == null) {
					violationNotes.add("Missing MTD Return for Benchmark [" + (performanceSummaryBenchmark.getReferenceTwo().getName() != null ? performanceSummaryBenchmark.getReferenceTwo().getName() : performanceSummaryBenchmark.getReferenceTwo().getLabel()) + "]");
				}
			}
			if (!violationNotes.isEmpty()) {
				Map<String, Object> contextValues = new HashMap<>();
				contextValues.put("violationNotes", violationNotes);
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, performanceInvestmentAccount.getId(), contextValues));
			}
		}
		return ruleViolationList;
	}
}
