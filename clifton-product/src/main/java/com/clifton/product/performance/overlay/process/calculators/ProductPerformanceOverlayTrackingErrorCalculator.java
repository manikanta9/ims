package com.clifton.product.performance.overlay.process.calculators;


import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayAssetClassTrackingError;
import com.clifton.product.performance.overlay.ProductPerformancePortfolioRun;
import com.clifton.product.performance.overlay.process.ProductPerformanceOverlayProcessConfig;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>ProductPerformanceOverlayTrackingErrorCalculator</code> calculates tracking error for each applicable asset class and rolls up totals
 * to the {@link PerformancePortfolioRun}
 *
 * @author manderson
 */
@Component
public class ProductPerformanceOverlayTrackingErrorCalculator extends ProductPerformanceOverlayAssetClassCalculator {

	@Override
	protected void setupProcessing(ProductPerformanceOverlayProcessConfig config) {
		// Do Nothing - No Additional Setup to Perform
	}


	@Override
	protected void processProductPerformanceOverlayRun(ProductPerformanceOverlayProcessConfig config, ProductPerformancePortfolioRun performanceRun) {
		if (performanceRun.getPortfolioRun() != null && performanceRun.getPortfolioRun().getServiceProcessingType() != ServiceProcessingTypes.PORTFOLIO_TARGET) {
			List<ProductOverlayAssetClass> assetClassList = getProductOverlayAssetClassListForRun(config, performanceRun);
			processProductPerformanceOverlayRunTrackingError(performanceRun, assetClassList);
		}
	}


	@Override
	protected void recalculateProductPerformanceOverlayRun(ProductPerformanceOverlayProcessConfig config, ProductPerformancePortfolioRun performanceRun) {
		//Calculate run level tracking error properties from the list of related asset class level tracking errors
		performanceRun.setTrackingErrorBenchmarkReturn(CoreMathUtils.sumProperty(performanceRun.getPerformanceOverlayAssetClassTrackingErrorList(), ProductPerformanceOverlayAssetClassTrackingError::getBenchmarkReturn));
		performanceRun.setTrackingErrorAdjustedTargetAllocationPercent(CoreMathUtils.sumProperty(performanceRun.getPerformanceOverlayAssetClassTrackingErrorList(), ProductPerformanceOverlayAssetClassTrackingError::getAdjustedTargetAllocationPercent));
		performanceRun.setTrackingErrorPhysicalExposurePercent(CoreMathUtils.sumProperty(performanceRun.getPerformanceOverlayAssetClassTrackingErrorList(), ProductPerformanceOverlayAssetClassTrackingError::getPhysicalExposurePercent));
		performanceRun.setTrackingErrorTotalExposurePercent(CoreMathUtils.sumProperty(performanceRun.getPerformanceOverlayAssetClassTrackingErrorList(), ProductPerformanceOverlayAssetClassTrackingError::getTotalExposurePercent));
	}


	public void processProductPerformanceOverlayRunTrackingError(ProductPerformancePortfolioRun performanceRun, List<ProductOverlayAssetClass> acList) {
		List<ProductPerformanceOverlayAssetClassTrackingError> assetClassTrackingErrorList = new ArrayList<>();
		List<ProductPerformanceOverlayAssetClassTrackingError> existingAssetClassTrackingErrorList = null;

		// If not a new run get the existing tracking error list for this run
		if (performanceRun.getId() != null) {
			existingAssetClassTrackingErrorList = getProductPerformanceOverlayService().getProductPerformanceOverlayAssetClassTrackingErrorListByPerformanceOverlayRun(performanceRun.getId());
		}

		//Process each asset class
		for (ProductOverlayAssetClass productOverlayAssetClass : CollectionUtils.getIterable(acList)) {
			//Set benchmark to the productOverlayAssetClass benchmarkSecurity otherwise if it's null set it to the productOverlayAssetClass accountAssetClass benchmarkSecurity
			InvestmentSecurity benchmark = (productOverlayAssetClass.getBenchmarkSecurity() != null) ? productOverlayAssetClass.getBenchmarkSecurity()
					: (productOverlayAssetClass.getAccountAssetClass() != null) ? productOverlayAssetClass.getAccountAssetClass().getBenchmarkSecurity() : null;

			if (benchmark == null) {
				continue;
			}

			// Find existing tracking error if exists
			ProductPerformanceOverlayAssetClassTrackingError trackingError = CollectionUtils.getOnlyElement(
					BeanUtils.filter(existingAssetClassTrackingErrorList, (ProductPerformanceOverlayAssetClassTrackingError assetClassTrackingError) -> assetClassTrackingError.getProductOverlayAssetClass().getId(), productOverlayAssetClass.getId()));

			// If existing tracking error doesn't exist, create it
			if (trackingError == null) {
				trackingError = new ProductPerformanceOverlayAssetClassTrackingError();
				trackingError.setProductPerformanceOverlayRun(performanceRun);
				trackingError.setProductOverlayAssetClass(productOverlayAssetClass);
			}
			assetClassTrackingErrorList.add(trackingError);

			// Update Calculated Values
			BigDecimal benchmarkReturn = getMarketDataRetriever().getInvestmentSecurityReturnFlexible(performanceRun.getPerformanceSummary().getClientAccount(), benchmark,
					DateUtils.getPreviousWeekday(performanceRun.getMeasureDate()), performanceRun.getMeasureDate(), "Asset Class Benchmark", true);

			//Calculate asset class tracking error properties
			trackingError.setBenchmarkReturn(benchmarkReturn);

			trackingError.setAdjustedTargetAllocationPercent(MathUtils.multiply(MathUtils.divide(productOverlayAssetClass.getTargetAllocationAdjustedPercent(), 100), benchmarkReturn));

			trackingError.setPhysicalExposurePercent(MathUtils.multiply(MathUtils.divide(productOverlayAssetClass.getActualAllocationAdjustedPercent(), 100), benchmarkReturn));

			trackingError.setTotalExposurePercent(MathUtils.multiply(
					MathUtils.add(MathUtils.divide(productOverlayAssetClass.getActualAllocationAdjustedPercent(), 100), MathUtils.divide(productOverlayAssetClass.getOverlayExposurePercent(), 100)),
					benchmarkReturn));
		}

		// Set the new list on the performance run object
		performanceRun.setPerformanceOverlayAssetClassTrackingErrorList(assetClassTrackingErrorList);
	}
}
