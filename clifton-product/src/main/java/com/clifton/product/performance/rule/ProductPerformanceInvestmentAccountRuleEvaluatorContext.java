package com.clifton.product.performance.rule;

import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.portfolio.run.rule.PerformanceInvestmentAccountPortfolioRunRuleEvaluatorContext;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayAssetClass;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayService;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
public class ProductPerformanceInvestmentAccountRuleEvaluatorContext extends PerformanceInvestmentAccountPortfolioRunRuleEvaluatorContext {

	private static final String PERFORMANCE_DAILY_OVERLAY_RUN_ASSET_CLASS_LIST = "PERFORMANCE_DAILY_OVERLAY_RUN_ASSET_CLASS_LIST";

	/////////////////////////////////////////////////////////////////////////////

	private ProductPerformanceOverlayService productPerformanceOverlayService;

	/////////////////////////////////////////////////////////////////////////////


	/**
	 * Get investment account asset class list by the client account id defined on the current performance summary.
	 * <p>
	 * This will exclude roll-up lists and attempts to retrieve a cached version from the context based on client account id
	 */
	public List<ProductPerformanceOverlayAssetClass> getProductPerformanceOverlayAssetClassListList(PerformanceInvestmentAccount performanceAccount) {
		@SuppressWarnings("unchecked")
		List<ProductPerformanceOverlayAssetClass> performanceOverlayAssetClassList = (List<ProductPerformanceOverlayAssetClass>) getContext().getBean(PERFORMANCE_DAILY_OVERLAY_RUN_ASSET_CLASS_LIST);

		if (performanceOverlayAssetClassList == null) {
			performanceOverlayAssetClassList = getProductPerformanceOverlayService().getProductPerformanceOverlayAssetClassListByPerformanceSummary(performanceAccount.getId());
			if (performanceOverlayAssetClassList == null) {
				performanceOverlayAssetClassList = new ArrayList<>();
			}
			getContext().setBean(PERFORMANCE_DAILY_OVERLAY_RUN_ASSET_CLASS_LIST, performanceOverlayAssetClassList);
		}
		return performanceOverlayAssetClassList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods            //////////////
	/////////////////////////////////////////////////////////////////////////////


	public ProductPerformanceOverlayService getProductPerformanceOverlayService() {
		return this.productPerformanceOverlayService;
	}


	public void setProductPerformanceOverlayService(ProductPerformanceOverlayService productPerformanceOverlayService) {
		this.productPerformanceOverlayService = productPerformanceOverlayService;
	}
}
