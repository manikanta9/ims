package com.clifton.product.performance.overlay.cash;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayAssetClass;
import com.clifton.product.performance.overlay.cash.search.OverlayAssetClassCashAttributionSearchForm;
import com.clifton.product.performance.overlay.cash.search.OverlayRunCashAttributionSearchForm;

import java.util.List;


/**
 * The <code>ProductPerformanceOverlayRunCashAttributionService</code> contains methods
 * for getting the cash attribution breakdown for a summary/run/assetclass.
 *
 * @author manderson
 */
public interface ProductPerformanceOverlayCashAttributionService {

	////////////////////////////////////////////////////////////////
	////  ProductPerformanceOverlayRunCashAttribution Methods   ////
	////////////////////////////////////////////////////////////////


	/**
	 * ID is the ProductPerformanceOverlayRunID
	 * <p/>
	 * NOTE: Also populates assetClassList with the asset class attribution breakdown
	 */
	@SecureMethod(dtoClass = PerformancePortfolioRun.class)
	public ProductPerformanceOverlayRunCashAttribution getProductPerformanceOverlayRunCashAttribution(int id);


	/**
	 * Saves the asset class performance attribution return overrides for each asset class in the list on the bean
	 * Returns updated bean as any changes would cause attributions to change
	 */
	@SecureMethod(dtoClass = PerformancePortfolioRun.class)
	public ProductPerformanceOverlayRunCashAttribution saveProductPerformanceOverlayRunCashAttribution(ProductPerformanceOverlayRunCashAttribution bean);


	@SecureMethod(dtoClass = PerformancePortfolioRun.class)
	public List<ProductPerformanceOverlayRunCashAttribution> getProductPerformanceOverlayRunCashAttributionList(OverlayRunCashAttributionSearchForm searchForm);

	///////////////////////////////////////////////////////////////////////
	////  ProductPerformanceOverlayAssetClassCashAttribution Methods   ////
	///////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = ProductPerformanceOverlayAssetClass.class)
	public List<ProductPerformanceOverlayAssetClassCashAttribution> getProductPerformanceOverlayAssetClassCashAttributionList(OverlayAssetClassCashAttributionSearchForm searchForm);
}
