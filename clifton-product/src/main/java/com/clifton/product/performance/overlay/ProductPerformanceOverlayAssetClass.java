package com.clifton.product.performance.overlay;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClass;

import java.math.BigDecimal;


/**
 * The <code>ProductPerformanceOverlayAssetClass</code> class represents details used to calculate ProductPerformanceSummary information.
 * It has overlay run's asset class level data and allows overrides to run's data.
 * <p/>
 * Only asset classes that are replicated (have primary replication) are included. Matching replication should not affect overlay target
 * (do not double count), however it's performance must be included into gain/loss.
 * <p/>
 * NOTE: Commodities use a different interest rate index (3 month T-bill vs 3-month LIBOR) in financingRate calculation.
 * InvestmentReplicationType is mapped to corresponding interest rate index.
 *
 * @author vgomelsky
 */
public class ProductPerformanceOverlayAssetClass extends BaseEntity<Integer> {

	private PerformancePortfolioRun performanceOverlayRun;
	private ProductOverlayAssetClass overlayAssetClass;

	private BigDecimal gainLoss; // since previous performance detail (business day); mark to market for futures; includes commission; NOT synthetically adjusted
	private BigDecimal gainLossOverride;

	private BigDecimal overlayTarget;
	private BigDecimal overlayTargetOverride;

	private String overrideNote;

	// ((1+(Previous Month End Cost of Capital (3 month LIBOR, T-BILL)))^(1/360)-1)*Days from Previous Performance Detail
	private BigDecimal financingRate;

	private InvestmentSecurity benchmarkSecurity; // Benchmark used on this date to calculate benchmark return
	private BigDecimal benchmarkReturn; // % increase since last business day (run)

	/**
	 * For cash attribution calculations.  By default, the attribution return is
	 * Gain/Loss divided by Overlay Exposure
	 * If overlay exposure is zero then Overlay Target is used
	 * If, when using overlay exposure, the return is greater than 5%, then uses the larger of the abs(overlay exposure) and abs(overlay target)
	 * The override field is defined here, but see {@link com.clifton.product.performance.overlay.cash.ProductPerformanceOverlayAssetClassCashAttribution}
	 * for additional details
	 */
	private BigDecimal attributionReturnOverride;

	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	public BigDecimal getCoalesceGainLossOverride() {
		if (getGainLossOverride() == null) {
			return getGainLoss();
		}
		return getGainLossOverride();
	}


	public BigDecimal getCoalesceOverlayTargetOverride() {
		if (getOverlayTargetOverride() == null) {
			return getOverlayTarget();
		}
		return getOverlayTargetOverride();
	}


	public BigDecimal getOverlayTargetTimesFinancingRate() {
		if (MathUtils.isNullOrZero(getFinancingRate())) {
			return BigDecimal.ZERO;
		}
		return MathUtils.getPercentageOf(getFinancingRate(), getCoalesceOverlayTargetOverride(), true);
	}


	public BigDecimal getAbsCoalesceOverlayTargetOverride() {
		return MathUtils.abs(getCoalesceOverlayTargetOverride());
	}


	public BigDecimal getAssetClassReturn() {
		BigDecimal result = BigDecimal.ZERO;
		if (!MathUtils.isNullOrZero(getCoalesceOverlayTargetOverride())) {
			// (Gain/Loss + (OverlayTarget * Financing Rate) / ABS(OverlayTarget)
			result = MathUtils.add(getCoalesceGainLossOverride(), getOverlayTargetTimesFinancingRate());
			result = MathUtils.divide(result, getAbsCoalesceOverlayTargetOverride());
			result = MathUtils.multiply(result, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		}
		return result;
	}


	public BigDecimal getReturnDifference() {
		return MathUtils.subtract(getAssetClassReturn(), getBenchmarkReturn());
	}

	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	public PerformancePortfolioRun getPerformanceOverlayRun() {
		return this.performanceOverlayRun;
	}


	public void setPerformanceOverlayRun(PerformancePortfolioRun performanceOverlayRun) {
		this.performanceOverlayRun = performanceOverlayRun;
	}


	public ProductOverlayAssetClass getOverlayAssetClass() {
		return this.overlayAssetClass;
	}


	public void setOverlayAssetClass(ProductOverlayAssetClass overlayAssetClass) {
		this.overlayAssetClass = overlayAssetClass;
	}


	public BigDecimal getGainLoss() {
		return this.gainLoss;
	}


	public void setGainLoss(BigDecimal gainLoss) {
		this.gainLoss = gainLoss;
	}


	public BigDecimal getGainLossOverride() {
		return this.gainLossOverride;
	}


	public void setGainLossOverride(BigDecimal gainLossOverride) {
		this.gainLossOverride = gainLossOverride;
	}


	public BigDecimal getOverlayTarget() {
		return this.overlayTarget;
	}


	public void setOverlayTarget(BigDecimal overlayTarget) {
		this.overlayTarget = overlayTarget;
	}


	public BigDecimal getOverlayTargetOverride() {
		return this.overlayTargetOverride;
	}


	public void setOverlayTargetOverride(BigDecimal overlayTargetOverride) {
		this.overlayTargetOverride = overlayTargetOverride;
	}


	public String getOverrideNote() {
		return this.overrideNote;
	}


	public void setOverrideNote(String overrideNote) {
		this.overrideNote = overrideNote;
	}


	public BigDecimal getBenchmarkReturn() {
		return this.benchmarkReturn;
	}


	public void setBenchmarkReturn(BigDecimal benchmarkReturn) {
		this.benchmarkReturn = benchmarkReturn;
	}


	public BigDecimal getFinancingRate() {
		return this.financingRate;
	}


	public void setFinancingRate(BigDecimal financingRate) {
		this.financingRate = financingRate;
	}


	public InvestmentSecurity getBenchmarkSecurity() {
		return this.benchmarkSecurity;
	}


	public void setBenchmarkSecurity(InvestmentSecurity benchmarkSecurity) {
		this.benchmarkSecurity = benchmarkSecurity;
	}


	public BigDecimal getAttributionReturnOverride() {
		return this.attributionReturnOverride;
	}


	public void setAttributionReturnOverride(BigDecimal attributionReturnOverride) {
		this.attributionReturnOverride = attributionReturnOverride;
	}
}
