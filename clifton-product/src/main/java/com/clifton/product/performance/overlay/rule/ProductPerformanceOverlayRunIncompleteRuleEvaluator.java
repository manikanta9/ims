package com.clifton.product.performance.overlay.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.product.performance.rule.ProductPerformanceInvestmentAccountRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.List;


/**
 * ProductPerformanceOverlayRunIncompleteRuleEvaluator Returns violations for each ProductPerformanceOverlayRun that is associated with a PortfolioRun that hasn't been submitted to trading (a.k.a. Incomplete)
 * <p>
 *
 * @author manderson
 */
public class ProductPerformanceOverlayRunIncompleteRuleEvaluator extends BaseRuleEvaluator<PerformanceInvestmentAccount, ProductPerformanceInvestmentAccountRuleEvaluatorContext> {


	@Override
	public List<RuleViolation> evaluateRule(PerformanceInvestmentAccount performanceInvestmentAccount, RuleConfig ruleConfig, ProductPerformanceInvestmentAccountRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<PerformancePortfolioRun> performanceRunList = context.getPerformancePortfolioRunList(performanceInvestmentAccount);
			for (PerformancePortfolioRun performanceOverlayRun : CollectionUtils.getIterable(performanceRunList)) {
				if (performanceOverlayRun.getPortfolioRun() != null && !performanceOverlayRun.getPortfolioRun().isSubmittedToTrading()) {
					violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceInvestmentAccount.getId(), performanceOverlayRun.getPortfolioRun().getId()));
				}
			}
		}
		return violationList;
	}
}
