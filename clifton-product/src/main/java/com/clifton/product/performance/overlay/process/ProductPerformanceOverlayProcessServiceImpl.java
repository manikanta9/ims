package com.clifton.product.performance.overlay.process;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.account.process.PerformanceInvestmentAccountProcessService;
import com.clifton.performance.account.process.type.PerformanceInvestmentAccountProcessingTypes;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchForm;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayService;
import com.clifton.product.performance.overlay.ProductPerformancePortfolioRun;
import com.clifton.product.performance.overlay.process.calculators.ProductPerformanceOverlayTrackingErrorCalculator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * The <code>ProductPerformanceOverlayProcessServiceImpl</code>
 * <p/>
 * exposes some of the partial processing specific for overlay summaries
 *
 * @author manderson
 */
@Service
public class ProductPerformanceOverlayProcessServiceImpl implements ProductPerformanceOverlayProcessService {

	private PerformanceInvestmentAccountService performanceInvestmentAccountService;
	private PerformanceInvestmentAccountProcessService performanceInvestmentAccountProcessService;

	private PortfolioRunService portfolioRunService;

	private ProductPerformanceOverlayService productPerformanceOverlayService;

	/**
	 * TODO ONCE HISTORY IS LOADED REMOVE THIS
	 */
	private ProductPerformanceOverlayTrackingErrorCalculator productPerformanceOverlayTrackingErrorCalculator;


	@Override
	public void processProductPerformanceInvestmentAccountListTrackingErrorByMeasureDate(Date measureDate, Integer clientAccountId) {
		PerformanceInvestmentAccountSearchForm searchForm = new PerformanceInvestmentAccountSearchForm();
		searchForm.setStartMeasureDate(DateUtils.getFirstDayOfMonth(measureDate));
		searchForm.setEndMeasureDate(DateUtils.getLastDayOfMonth(measureDate));
		searchForm.setClientAccountId(clientAccountId);

		List<PerformanceInvestmentAccount> performanceSummaryList = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccountList(searchForm);

		// THIS IS A SHORTCUT TO PROCESS HISTORY OF ONE SECTION BY CALLING THE CALCULATOR DIRECTLY
		// THIS SHOULD BE REMOVED ONCE NO LONGER NEEDED
		StringBuilder errors = new StringBuilder(16);
		for (PerformanceInvestmentAccount performanceSummary : CollectionUtils.getIterable(performanceSummaryList)) {
			try {
				processProductPerformanceInvestmentAccountListTrackingError(performanceSummary);
			}
			catch (Throwable e) {
				errors.append(StringUtils.NEW_LINE).append(performanceSummary.getClientAccount().getNumber()).append(": ").append(e.getMessage()).append(StringUtils.NEW_LINE);
			}
		}

		if (errors.length() > 0) {
			throw new ValidationException("Errors encountered processing tracking error for the following accounts for " + DateUtils.fromDateShort(measureDate) + ": " + errors);
		}
	}


	@Transactional
	protected void processProductPerformanceInvestmentAccountListTrackingError(PerformanceInvestmentAccount performanceSummary) {
		ProductPerformanceOverlayProcessConfig config = new ProductPerformanceOverlayProcessConfig(performanceSummary, PerformanceInvestmentAccountProcessingTypes.PROCESS);
		getProductPerformanceOverlayTrackingErrorCalculator().calculate(config, null);
	}


	/**
	 * Save method called from UI - validates edits - updates overrides/main run & re-processes for that date
	 */
	@Override
	public ProductPerformancePortfolioRun saveProductPerformanceOverlayRun(ProductPerformancePortfolioRun bean) {
		PerformanceInvestmentAccount summary = bean.getPerformanceSummary();
		// Validate Editing Is Allowed
		getPerformanceInvestmentAccountService().validatePerformanceInvestmentAccountModify(bean.getPerformanceSummary().getId());

		// If the Portfolio Run isn't the Main Run, then it was changed.  Let's set it to be the main run so used when re-processed
		PortfolioRun run = bean.getPortfolioRun();
		if (run != null && !run.isMainRun()) {
			run.setMainRun(true);
			getPortfolioRunService().savePortfolioRun(run);
		}

		// Save any overrides
		bean = getProductPerformanceOverlayService().saveProductPerformanceOverlayRun(bean);

		// Re-process this run and Re-calculate the summary
		getPerformanceInvestmentAccountProcessService().processPerformanceInvestmentAccountForDate(summary, bean.getMeasureDate());
		// return saved and reprocessed instance
		return getProductPerformanceOverlayService().getProductPerformanceOverlayRun(bean.getId());
	}

	///////////////////////////////////////////////////////////
	///////           Getter & Setter Methods          ////////
	///////////////////////////////////////////////////////////


	public ProductPerformanceOverlayService getProductPerformanceOverlayService() {
		return this.productPerformanceOverlayService;
	}


	public void setProductPerformanceOverlayService(ProductPerformanceOverlayService productPerformanceOverlayService) {
		this.productPerformanceOverlayService = productPerformanceOverlayService;
	}


	public PerformanceInvestmentAccountService getPerformanceInvestmentAccountService() {
		return this.performanceInvestmentAccountService;
	}


	public void setPerformanceInvestmentAccountService(PerformanceInvestmentAccountService performanceInvestmentAccountService) {
		this.performanceInvestmentAccountService = performanceInvestmentAccountService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public PerformanceInvestmentAccountProcessService getPerformanceInvestmentAccountProcessService() {
		return this.performanceInvestmentAccountProcessService;
	}


	public void setPerformanceInvestmentAccountProcessService(PerformanceInvestmentAccountProcessService performanceInvestmentAccountProcessService) {
		this.performanceInvestmentAccountProcessService = performanceInvestmentAccountProcessService;
	}


	public ProductPerformanceOverlayTrackingErrorCalculator getProductPerformanceOverlayTrackingErrorCalculator() {
		return this.productPerformanceOverlayTrackingErrorCalculator;
	}


	public void setProductPerformanceOverlayTrackingErrorCalculator(ProductPerformanceOverlayTrackingErrorCalculator productPerformanceOverlayTrackingErrorCalculator) {
		this.productPerformanceOverlayTrackingErrorCalculator = productPerformanceOverlayTrackingErrorCalculator;
	}
}
