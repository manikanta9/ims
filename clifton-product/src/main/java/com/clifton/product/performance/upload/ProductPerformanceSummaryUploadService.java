package com.clifton.product.performance.upload;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.performance.account.PerformanceInvestmentAccount;


/**
 * The <code>ProductPerformanceSummaryUploadService</code> allows importing performance summaries into the system.
 *
 * @author apopp
 */
public interface ProductPerformanceSummaryUploadService {

	/**
	 * Custom upload to populate {@link com.clifton.performance.account.PerformanceInvestmentAccount} beans and then
	 * process those beans to create actual full {@link com.clifton.performance.account.PerformanceInvestmentAccount} objects to save
	 */
	@SecureMethod(dtoClass = PerformanceInvestmentAccount.class)
	public void uploadProductPerformanceSummaryUploadFile(ProductPerformanceSummaryUploadCommand uploadCommand);
}
