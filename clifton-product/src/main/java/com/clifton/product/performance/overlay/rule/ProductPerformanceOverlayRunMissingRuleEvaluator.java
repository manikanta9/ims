package com.clifton.product.performance.overlay.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.product.performance.rule.ProductPerformanceInvestmentAccountRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * ProductPerformanceOverlayRunMissingRuleEvaluator Returns violations for each ProductPerformanceOverlayRun that doesn't have a PortfolioRun associated with it
 * and the balance date is on or after the performance start date for the account.
 * Also validates (optionally) that the run date is the previous weekday from the measure date (which ensures holiday runs aren't missing)
 * <p>
 *
 * @author manderson
 */
public class ProductPerformanceOverlayRunMissingRuleEvaluator extends BaseRuleEvaluator<PerformanceInvestmentAccount, ProductPerformanceInvestmentAccountRuleEvaluatorContext> {

	/**
	 * If true, will validate that there is a portfolio run associated AND the portfolio run date is the previous weekday from the measure date
	 * * This ensures that missing holiday runs are added for the performance summary - rather than at quarter end for billing
	 * If false, will only validate that there is a portfolio run associated - and it's OK if the date doesn't match (for holidays)
	 */
	private boolean validatePortfolioRunDate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PerformanceInvestmentAccount performanceInvestmentAccount, RuleConfig ruleConfig, ProductPerformanceInvestmentAccountRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<PerformancePortfolioRun> performanceRunList = context.getPerformancePortfolioRunList(performanceInvestmentAccount);
			for (PerformancePortfolioRun performanceOverlayRun : CollectionUtils.getIterable(performanceRunList)) {
				// We need the previous week day always - if it's a holiday the performance summary will have the previous business day if missing, and we need to verify the actual week day run is there.
				Date expectedPortfolioRunDate = DateUtils.getPreviousWeekday(performanceOverlayRun.getMeasureDate());
				if (DateUtils.compare(expectedPortfolioRunDate, context.getInvestmentAccountPerformanceInceptionDate(performanceInvestmentAccount), false) >= 0) {
					if (performanceOverlayRun.getPortfolioRun() == null || (isValidatePortfolioRunDate() && DateUtils.compare(expectedPortfolioRunDate, performanceOverlayRun.getPortfolioRunDate(), false) != 0)) {
						Map<String, Object> templateValues = new HashMap<>();
						templateValues.put("portfolioRunDate", expectedPortfolioRunDate);
						violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceInvestmentAccount.getId(), performanceOverlayRun.getId(), null, templateValues));
					}
				}
			}
		}
		return violationList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isValidatePortfolioRunDate() {
		return this.validatePortfolioRunDate;
	}


	public void setValidatePortfolioRunDate(boolean validatePortfolioRunDate) {
		this.validatePortfolioRunDate = validatePortfolioRunDate;
	}
}
