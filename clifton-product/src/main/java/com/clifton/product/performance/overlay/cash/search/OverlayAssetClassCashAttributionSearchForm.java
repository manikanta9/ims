package com.clifton.product.performance.overlay.cash.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.manager.ManagerCashTypes;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
public class OverlayAssetClassCashAttributionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "performanceOverlayRun.id")
	private Integer overlayRunId;

	@SearchField(searchFieldPath = "performanceOverlayRun", searchField = "performanceSummary.id")
	private Integer performanceSummaryId;

	@SearchField(searchFieldPath = "performanceOverlayRun.performanceSummary", searchField = "clientAccount.id")
	private Integer clientAccountId;

	// Custom Search Field = Includes Specific Account or any of its SubAccounts
	private Integer clientAccountIdOrSubAccount;

	@SearchField(searchFieldPath = "performanceOverlayRun.performanceSummary", searchField = "accountingPeriod.id")
	private Integer accountingPeriodId;

	@SearchField(searchFieldPath = "performanceOverlayRun", searchField = "measureDate")
	private Date measureDate;

	@SearchField(searchFieldPath = "performanceOverlayRun", searchField = "portfolioRunDate")
	private Date overlayRunDate;

	@SearchField(searchFieldPath = "overlayAssetClass.accountAssetClass.assetClass", searchField = "name")
	private String assetClassName;

	@SearchField
	private BigDecimal fundCash;
	@SearchField
	private BigDecimal fundCashAdjusted;
	@SearchField
	private BigDecimal fundCashAttribution;

	@SearchField
	private BigDecimal managerCash;
	@SearchField
	private BigDecimal managerCashAdjusted;
	@SearchField
	private BigDecimal managerCashAttribution;

	@SearchField
	private BigDecimal transitionCash;
	@SearchField
	private BigDecimal transitionCashAdjusted;
	@SearchField
	private BigDecimal transitionCashAttribution;

	@SearchField
	private BigDecimal rebalanceCash;
	@SearchField
	private BigDecimal rebalanceCashAdjusted;
	@SearchField
	private BigDecimal rebalanceCashAttribution;

	@SearchField
	private BigDecimal clientDirectedCash;
	@SearchField
	private BigDecimal clientDirectedCashAdjusted;
	@SearchField
	private BigDecimal clientDirectedCashAttribution;


	@SearchField
	private BigDecimal plugAttribution;
	@SearchField
	private ManagerCashTypes plugAttributionCashType;

	@SearchField
	private BigDecimal attributionReturn;
	@SearchField
	private BigDecimal attributionReturnOverride;
	@SearchField(searchField = "attributionReturnOverride,attributionReturn", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private BigDecimal coalesceAttributionReturnOverride;

	@SearchField
	private String attributionCalculationNote;

	@SearchField(searchField = "gainLoss,gainLossOverride", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private BigDecimal coalesceGainLossOverride;

	@SearchField(searchField = "overlayTarget,overlayTargetOverride", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private BigDecimal coalesceOverlayTargetOverride;

	@SearchField(searchFieldPath = "overlayAssetClass", searchField = "overlayExposure")
	private BigDecimal overlayExposure;

	@SearchField
	private BigDecimal benchmarkReturn;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getOverlayRunId() {
		return this.overlayRunId;
	}


	public void setOverlayRunId(Integer overlayRunId) {
		this.overlayRunId = overlayRunId;
	}


	public BigDecimal getFundCash() {
		return this.fundCash;
	}


	public void setFundCash(BigDecimal fundCash) {
		this.fundCash = fundCash;
	}


	public BigDecimal getFundCashAdjusted() {
		return this.fundCashAdjusted;
	}


	public void setFundCashAdjusted(BigDecimal fundCashAdjusted) {
		this.fundCashAdjusted = fundCashAdjusted;
	}


	public BigDecimal getFundCashAttribution() {
		return this.fundCashAttribution;
	}


	public void setFundCashAttribution(BigDecimal fundCashAttribution) {
		this.fundCashAttribution = fundCashAttribution;
	}


	public BigDecimal getManagerCash() {
		return this.managerCash;
	}


	public void setManagerCash(BigDecimal managerCash) {
		this.managerCash = managerCash;
	}


	public BigDecimal getManagerCashAdjusted() {
		return this.managerCashAdjusted;
	}


	public void setManagerCashAdjusted(BigDecimal managerCashAdjusted) {
		this.managerCashAdjusted = managerCashAdjusted;
	}


	public BigDecimal getManagerCashAttribution() {
		return this.managerCashAttribution;
	}


	public void setManagerCashAttribution(BigDecimal managerCashAttribution) {
		this.managerCashAttribution = managerCashAttribution;
	}


	public BigDecimal getTransitionCash() {
		return this.transitionCash;
	}


	public void setTransitionCash(BigDecimal transitionCash) {
		this.transitionCash = transitionCash;
	}


	public BigDecimal getTransitionCashAdjusted() {
		return this.transitionCashAdjusted;
	}


	public void setTransitionCashAdjusted(BigDecimal transitionCashAdjusted) {
		this.transitionCashAdjusted = transitionCashAdjusted;
	}


	public BigDecimal getTransitionCashAttribution() {
		return this.transitionCashAttribution;
	}


	public void setTransitionCashAttribution(BigDecimal transitionCashAttribution) {
		this.transitionCashAttribution = transitionCashAttribution;
	}


	public BigDecimal getRebalanceCash() {
		return this.rebalanceCash;
	}


	public void setRebalanceCash(BigDecimal rebalanceCash) {
		this.rebalanceCash = rebalanceCash;
	}


	public BigDecimal getRebalanceCashAdjusted() {
		return this.rebalanceCashAdjusted;
	}


	public void setRebalanceCashAdjusted(BigDecimal rebalanceCashAdjusted) {
		this.rebalanceCashAdjusted = rebalanceCashAdjusted;
	}


	public BigDecimal getRebalanceCashAttribution() {
		return this.rebalanceCashAttribution;
	}


	public void setRebalanceCashAttribution(BigDecimal rebalanceCashAttribution) {
		this.rebalanceCashAttribution = rebalanceCashAttribution;
	}


	public BigDecimal getClientDirectedCash() {
		return this.clientDirectedCash;
	}


	public void setClientDirectedCash(BigDecimal clientDirectedCash) {
		this.clientDirectedCash = clientDirectedCash;
	}


	public BigDecimal getClientDirectedCashAdjusted() {
		return this.clientDirectedCashAdjusted;
	}


	public void setClientDirectedCashAdjusted(BigDecimal clientDirectedCashAdjusted) {
		this.clientDirectedCashAdjusted = clientDirectedCashAdjusted;
	}


	public BigDecimal getClientDirectedCashAttribution() {
		return this.clientDirectedCashAttribution;
	}


	public void setClientDirectedCashAttribution(BigDecimal clientDirectedCashAttribution) {
		this.clientDirectedCashAttribution = clientDirectedCashAttribution;
	}


	public BigDecimal getPlugAttribution() {
		return this.plugAttribution;
	}


	public void setPlugAttribution(BigDecimal plugAttribution) {
		this.plugAttribution = plugAttribution;
	}


	public ManagerCashTypes getPlugAttributionCashType() {
		return this.plugAttributionCashType;
	}


	public void setPlugAttributionCashType(ManagerCashTypes plugAttributionCashType) {
		this.plugAttributionCashType = plugAttributionCashType;
	}


	public BigDecimal getAttributionReturn() {
		return this.attributionReturn;
	}


	public void setAttributionReturn(BigDecimal attributionReturn) {
		this.attributionReturn = attributionReturn;
	}


	public BigDecimal getAttributionReturnOverride() {
		return this.attributionReturnOverride;
	}


	public void setAttributionReturnOverride(BigDecimal attributionReturnOverride) {
		this.attributionReturnOverride = attributionReturnOverride;
	}


	public String getAttributionCalculationNote() {
		return this.attributionCalculationNote;
	}


	public void setAttributionCalculationNote(String attributionCalculationNote) {
		this.attributionCalculationNote = attributionCalculationNote;
	}


	public Integer getPerformanceSummaryId() {
		return this.performanceSummaryId;
	}


	public void setPerformanceSummaryId(Integer performanceSummaryId) {
		this.performanceSummaryId = performanceSummaryId;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public Date getOverlayRunDate() {
		return this.overlayRunDate;
	}


	public void setOverlayRunDate(Date overlayRunDate) {
		this.overlayRunDate = overlayRunDate;
	}


	public String getAssetClassName() {
		return this.assetClassName;
	}


	public void setAssetClassName(String assetClassName) {
		this.assetClassName = assetClassName;
	}


	public BigDecimal getCoalesceAttributionReturnOverride() {
		return this.coalesceAttributionReturnOverride;
	}


	public void setCoalesceAttributionReturnOverride(BigDecimal coalesceAttributionReturnOverride) {
		this.coalesceAttributionReturnOverride = coalesceAttributionReturnOverride;
	}


	public BigDecimal getCoalesceGainLossOverride() {
		return this.coalesceGainLossOverride;
	}


	public void setCoalesceGainLossOverride(BigDecimal coalesceGainLossOverride) {
		this.coalesceGainLossOverride = coalesceGainLossOverride;
	}


	public BigDecimal getCoalesceOverlayTargetOverride() {
		return this.coalesceOverlayTargetOverride;
	}


	public void setCoalesceOverlayTargetOverride(BigDecimal coalesceOverlayTargetOverride) {
		this.coalesceOverlayTargetOverride = coalesceOverlayTargetOverride;
	}


	public BigDecimal getOverlayExposure() {
		return this.overlayExposure;
	}


	public void setOverlayExposure(BigDecimal overlayExposure) {
		this.overlayExposure = overlayExposure;
	}


	public BigDecimal getBenchmarkReturn() {
		return this.benchmarkReturn;
	}


	public void setBenchmarkReturn(BigDecimal benchmarkReturn) {
		this.benchmarkReturn = benchmarkReturn;
	}


	public Integer getClientAccountIdOrSubAccount() {
		return this.clientAccountIdOrSubAccount;
	}


	public void setClientAccountIdOrSubAccount(Integer clientAccountIdOrSubAccount) {
		this.clientAccountIdOrSubAccount = clientAccountIdOrSubAccount;
	}


	public Integer getAccountingPeriodId() {
		return this.accountingPeriodId;
	}


	public void setAccountingPeriodId(Integer accountingPeriodId) {
		this.accountingPeriodId = accountingPeriodId;
	}
}
