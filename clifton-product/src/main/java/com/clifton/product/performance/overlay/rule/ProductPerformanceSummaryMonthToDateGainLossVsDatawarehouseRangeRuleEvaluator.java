package com.clifton.product.performance.overlay.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.performance.account.process.portfolio.run.search.PerformancePortfolioRunSearchForm;
import com.clifton.product.performance.rule.ProductPerformanceInvestmentAccountRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * ProductPerformanceSummaryHistoricalGainLossAdjustmentsRangeRuleEvaluator Returns violations for the summary if historical to date gain/loss adjustments are outside of specified range
 *
 * @author manderson
 */
public class ProductPerformanceSummaryMonthToDateGainLossVsDatawarehouseRangeRuleEvaluator extends BaseRuleEvaluator<PerformanceInvestmentAccount, ProductPerformanceInvestmentAccountRuleEvaluatorContext> {


	@Override
	public List<RuleViolation> evaluateRule(PerformanceInvestmentAccount performanceInvestmentAccount, RuleConfig ruleConfig, ProductPerformanceInvestmentAccountRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();

		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			// Note: Not kept in the context because there isn't anything else that would need to use this
			PerformancePortfolioRunSearchForm searchForm = new PerformancePortfolioRunSearchForm();
			searchForm.setClientAccountId(performanceInvestmentAccount.getClientAccount().getId());
			searchForm.setMaxMeasureDate(performanceInvestmentAccount.getMeasureDate());
			searchForm.setGainLossOrGainLossOtherOverridden(true);
			List<PerformancePortfolioRun> performanceOverlayRunList = context.getPerformancePortfolioRunService().getPerformancePortfolioRunList(searchForm);
			if (!CollectionUtils.isEmpty(performanceOverlayRunList)) {
				BigDecimal gainLossAdjustmentTotal = CoreMathUtils.sumProperty(performanceOverlayRunList, PerformancePortfolioRun::getGainLossDifference);
				BigDecimal gainLossOtherAdjustmentTotal = CoreMathUtils.sumProperty(performanceOverlayRunList, PerformancePortfolioRun::getGainLossOtherDifference);
				BigDecimal totalAdjustment = MathUtils.add(gainLossAdjustmentTotal, gainLossOtherAdjustmentTotal);
				String violationNote = RuleEvaluatorUtils.evaluateValueForRange(totalAdjustment, entityConfig.getMinAmount(), entityConfig.getMaxAmount(), MathUtils.NUMBER_FORMAT_MONEY);
				if (!StringUtils.isEmpty(violationNote)) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("violationNote", "Gain Loss Adjustments across all summaries since inception to [" + DateUtils.fromDateShort(performanceInvestmentAccount.getMeasureDate()) + "] equals: " + violationNote);
					violationList.add(getRuleViolationService().createRuleViolation(entityConfig, performanceInvestmentAccount.getId(), templateValues));
				}
			}
		}
		return violationList;
	}
}
