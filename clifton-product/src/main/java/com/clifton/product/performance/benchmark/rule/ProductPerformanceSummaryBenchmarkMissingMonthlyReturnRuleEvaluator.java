package com.clifton.product.performance.benchmark.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.benchmark.PerformanceSummaryBenchmark;
import com.clifton.product.performance.rule.ProductPerformanceInvestmentAccountRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.List;


/**
 * ProductPerformanceSummaryBenchmarkMissingMonthlyReturnRuleEvaluator returns a list of violations where the summary benchmark is missing a MTD return value.
 *
 * @author manderson
 */
public class ProductPerformanceSummaryBenchmarkMissingMonthlyReturnRuleEvaluator extends BaseRuleEvaluator<PerformanceInvestmentAccount, ProductPerformanceInvestmentAccountRuleEvaluatorContext> {

	@Override
	public List<RuleViolation> evaluateRule(PerformanceInvestmentAccount performanceInvestmentAccount, RuleConfig ruleConfig, ProductPerformanceInvestmentAccountRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<PerformanceSummaryBenchmark> summaryBenchmarkList = context.getPerformanceSummaryBenchmarkList(performanceInvestmentAccount);
			for (PerformanceSummaryBenchmark summaryBenchmark : CollectionUtils.getIterable(summaryBenchmarkList)) {
				if (summaryBenchmark.getCoalesceMonthToDateReturnOverride() == null) {
					violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceInvestmentAccount.getId(), summaryBenchmark.getId()));
				}
			}
		}
		return violationList;
	}
}
