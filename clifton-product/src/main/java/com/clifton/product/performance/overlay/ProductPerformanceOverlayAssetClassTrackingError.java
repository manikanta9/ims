package com.clifton.product.performance.overlay;


import com.clifton.core.beans.BaseEntity;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClass;

import java.math.BigDecimal;


public class ProductPerformanceOverlayAssetClassTrackingError extends BaseEntity<Integer> {

	private PerformancePortfolioRun productPerformanceOverlayRun;
	private ProductOverlayAssetClass productOverlayAssetClass;

	private BigDecimal benchmarkReturn;
	private BigDecimal adjustedTargetAllocationPercent;
	private BigDecimal physicalExposurePercent;
	private BigDecimal totalExposurePercent;


	public ProductOverlayAssetClass getProductOverlayAssetClass() {
		return this.productOverlayAssetClass;
	}


	public void setProductOverlayAssetClass(ProductOverlayAssetClass productOverlayAssetClass) {
		this.productOverlayAssetClass = productOverlayAssetClass;
	}


	public PerformancePortfolioRun getProductPerformanceOverlayRun() {
		return this.productPerformanceOverlayRun;
	}


	public void setProductPerformanceOverlayRun(PerformancePortfolioRun productPerformanceOverlayRun) {
		this.productPerformanceOverlayRun = productPerformanceOverlayRun;
	}


	public BigDecimal getBenchmarkReturn() {
		return this.benchmarkReturn;
	}


	public void setBenchmarkReturn(BigDecimal benchmarkReturn) {
		this.benchmarkReturn = benchmarkReturn;
	}


	public BigDecimal getAdjustedTargetAllocationPercent() {
		return this.adjustedTargetAllocationPercent;
	}


	public void setAdjustedTargetAllocationPercent(BigDecimal adjustedTargetAllocationPercent) {
		this.adjustedTargetAllocationPercent = adjustedTargetAllocationPercent;
	}


	public BigDecimal getPhysicalExposurePercent() {
		return this.physicalExposurePercent;
	}


	public void setPhysicalExposurePercent(BigDecimal physicalExposurePercent) {
		this.physicalExposurePercent = physicalExposurePercent;
	}


	public BigDecimal getTotalExposurePercent() {
		return this.totalExposurePercent;
	}


	public void setTotalExposurePercent(BigDecimal totalExposurePercent) {
		this.totalExposurePercent = totalExposurePercent;
	}
}
