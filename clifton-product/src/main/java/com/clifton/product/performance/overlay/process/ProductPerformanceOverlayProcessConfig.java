package com.clifton.product.performance.overlay.process;


import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassBenchmark;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRunProcessConfig;
import com.clifton.performance.account.process.type.PerformanceInvestmentAccountProcessingTypes;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.performance.overlay.ProductPerformancePortfolioRun;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductPerformanceOverlayProcessConfig</code> object is used for processing
 * {@link PerformancePortfolioRun}s and associated tables.  It pre-loads necessary data which can be
 * passed around to various processing methods so look ups can be re-used.
 *
 * @author manderson
 */
public class ProductPerformanceOverlayProcessConfig extends PerformancePortfolioRunProcessConfig {

	private List<ProductPerformancePortfolioRun> productPerformanceRunList;

	private Map<Integer, List<InvestmentAccountAssetClassBenchmark>> assetClassPerformanceBenchmarkMap = new HashMap<>();

	/**
	 * Contains a map for each run used the list of overlay asset classes
	 * for that run
	 */
	private Map<Integer, List<ProductOverlayAssetClass>> runAssetClassMap;

	// Maps each asset class to the financing rate to use
	// Populated as asset classes are encountered so next time don't have to look it up again.
	// By default, it is defined by the primary replication type for the asset class
	// however each asset class has a custom field which can override that selection.
	private Map<Integer, InvestmentInterestRateIndex> assetClassFinancingRateMap = new HashMap<>();

	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	public ProductPerformanceOverlayProcessConfig(PerformanceInvestmentAccount performanceAccount, PerformanceInvestmentAccountProcessingTypes processingType) {
		super(performanceAccount, processingType);
	}

	//////////////////////////////////////////////////////////


	public List<ProductOverlayAssetClass> getProductOverlayAssetClassListForRun(PortfolioRun run) {
		if (run != null && this.runAssetClassMap != null) {
			return this.runAssetClassMap.get(run.getId());
		}
		return null;
	}


	public void setProductOverlayAssetClassListForRun(PortfolioRun run, List<ProductOverlayAssetClass> list) {
		if (this.runAssetClassMap == null) {
			this.runAssetClassMap = new HashMap<>();
		}
		if (run != null) {
			this.runAssetClassMap.put(run.getId(), list);
		}
	}

	//////////////////////////////////////////////////////////


	public Map<Integer, List<InvestmentAccountAssetClassBenchmark>> getAssetClassPerformanceBenchmarkMap() {
		return this.assetClassPerformanceBenchmarkMap;
	}


	public void setAssetClassPerformanceBenchmarkMap(Map<Integer, List<InvestmentAccountAssetClassBenchmark>> assetClassPerformanceBenchmarkMap) {
		this.assetClassPerformanceBenchmarkMap = assetClassPerformanceBenchmarkMap;
	}


	public Map<Integer, InvestmentInterestRateIndex> getAssetClassFinancingRateMap() {
		return this.assetClassFinancingRateMap;
	}


	public void setAssetClassFinancingRateMap(Map<Integer, InvestmentInterestRateIndex> assetClassFinancingRateMap) {
		this.assetClassFinancingRateMap = assetClassFinancingRateMap;
	}


	public List<ProductPerformancePortfolioRun> getProductPerformanceRunList() {
		return this.productPerformanceRunList;
	}


	public void setProductPerformanceRunList(List<ProductPerformancePortfolioRun> productPerformanceRunList) {
		if (productPerformanceRunList != null) {
			// Set the base run list in the parent config for generic calculator access
			setPerformanceRunList(productPerformanceRunList);
		}
		this.productPerformanceRunList = productPerformanceRunList;
	}
}
