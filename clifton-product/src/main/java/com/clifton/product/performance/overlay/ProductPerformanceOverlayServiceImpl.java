package com.clifton.product.performance.overlay;


import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRunService;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.product.overlay.search.ProductPerformanceOverlayAssetClassTrackingErrorSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The <code>ProductPerformanceOverlayServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class ProductPerformanceOverlayServiceImpl implements ProductPerformanceOverlayService {

	private AdvancedUpdatableDAO<ProductPerformanceOverlayAssetClass, Criteria> productPerformanceOverlayAssetClassDAO;
	private AdvancedUpdatableDAO<ProductPerformanceOverlayAssetClassTrackingError, Criteria> productPerformanceOverlayAssetClassTrackingErrorDAO;

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private PerformancePortfolioRunService performancePortfolioRunService;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;

	////////////////////////////////////////////////////////////////////////////
	///////      Product Performance Overlay Run Business Methods        ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public ProductPerformancePortfolioRun getProductPerformanceOverlayRun(int id) {
		ProductPerformancePortfolioRun productRun = convertPerformanceRunToProductPerformanceRun(getPerformancePortfolioRunService().getPerformancePortfolioRun(id));
		if (productRun != null) {
			productRun.setPerformanceOverlayAssetClassList(getProductPerformanceOverlayAssetClassListByPerformanceOverlayRun(id));
		}
		return productRun;
	}


	@Override
	public List<ProductPerformancePortfolioRun> getProductPerformanceOverlayRunListByPerformanceSummary(int summaryId) {
		return convertPerformanceRunListToProductPerformanceRunList(getPerformancePortfolioRunService().getPerformancePortfolioRunListByPerformanceSummary(summaryId));
	}


	@Override
	@Transactional
	public ProductPerformancePortfolioRun saveProductPerformanceOverlayRun(ProductPerformancePortfolioRun bean) {
		/*
		Since ProductPerformancePortfolioRun is a non-persistent DTO, we need to ensure the saved instance is
		hydrated and any updated properties are saved.
		 */
		PerformancePortfolioRun existingRun = bean.isNewBean() ? null : getPerformancePortfolioRunService().getPerformancePortfolioRun(bean.getId());

		List<ProductPerformanceOverlayAssetClass> performanceOverlayAssetClassList = bean.getPerformanceOverlayAssetClassList();
		List<ProductPerformanceOverlayAssetClassTrackingError> performanceOverlayAssetClassTrackingErrorList = bean.getPerformanceOverlayAssetClassTrackingErrorList();
		PerformancePortfolioRun savedRun;
		if (existingRun != null) {
			existingRun.copyProperties(bean);
			savedRun = getPerformancePortfolioRunService().savePerformancePortfolioRun(existingRun);
		}
		else {
			savedRun = getPerformancePortfolioRunService().savePerformancePortfolioRun(bean.toPerformancePortfolioRun());
		}

		if (!CollectionUtils.isEmpty(performanceOverlayAssetClassList)) {
			saveProductPerformanceOverlayAssetClassListForRun(savedRun.getId(), performanceOverlayAssetClassList);
		}
		if (!CollectionUtils.isEmpty(performanceOverlayAssetClassTrackingErrorList)) {
			saveProductPerformanceOverlayAssetClassTrackingErrorListForRun(savedRun.getId(), performanceOverlayAssetClassTrackingErrorList);
		}

		ProductPerformancePortfolioRun productPerformanceRun = convertPerformanceRunToProductPerformanceRun(savedRun);
		productPerformanceRun.setPerformanceOverlayAssetClassList(performanceOverlayAssetClassList);
		productPerformanceRun.setPerformanceOverlayAssetClassTrackingErrorList(performanceOverlayAssetClassTrackingErrorList);
		return productPerformanceRun;
	}


	@Override
	@Transactional
	public void saveProductPerformanceOverlayRunListForSummary(int summaryId, List<ProductPerformancePortfolioRun> beanList) {
		List<PerformancePortfolioRun> originalList = getPerformancePortfolioRunService().getPerformancePortfolioRunListByPerformanceSummary(summaryId);
		Set<Integer> performanceRunIdSet = new HashSet<>();
		if (!CollectionUtils.isEmpty(beanList)) {
			for (int i = 0; i < beanList.size(); i++) {
				// Update the saved list in place and update the reference to fit the DAO saveList behavior.
				PerformancePortfolioRun performancePortfolioRun = beanList.get(i).toPerformancePortfolioRun();
				ProductPerformancePortfolioRun savedRun = convertPerformanceRunToProductPerformanceRun(getPerformancePortfolioRunService().savePerformancePortfolioRun(performancePortfolioRun));
				beanList.set(i, savedRun);
				performanceRunIdSet.add(savedRun.getId());
			}
		}
		for (PerformancePortfolioRun old : CollectionUtils.getIterable(originalList)) {
			if (!performanceRunIdSet.contains(old.getId())) {
				deleteProductPerformanceOverlayRun(old.getId());
			}
		}
	}


	@Override
	@Transactional
	public void deleteProductPerformanceOverlayRun(int id) {
		deleteProductPerformanceOverlayAssetClassByPerformanceOverlayRun(id);
		deleteProductPerformanceOverlayAssetClassTrackingErrorListByPerformanceOverlayRun(id);

		PerformancePortfolioRun performancePortfolioRun = getPerformancePortfolioRunService().getPerformancePortfolioRun(id);
		if (performancePortfolioRun.getPortfolioRun() == null || performancePortfolioRun.getPortfolioRun().getServiceProcessingType() != ServiceProcessingTypes.PORTFOLIO_TARGET) {
			getPerformancePortfolioRunService().deletePerformancePortfolioRun(id);
		}
	}


	@Override
	@Transactional
	public void deleteProductPerformanceOverlayRunListBySummary(int summaryId) {
		for (PerformancePortfolioRun r : CollectionUtils.getIterable(getPerformancePortfolioRunService().getPerformancePortfolioRunListByPerformanceSummary(summaryId))) {
			deleteProductPerformanceOverlayRun(r.getId());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////    Product Performance Overlay Asset Class Business Methods     //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ProductPerformanceOverlayAssetClass> getProductPerformanceOverlayAssetClassListByPerformanceSummary(final int summaryId) {
		HibernateSearchConfigurer configurer = criteria -> {
			criteria.createAlias("performanceOverlayRun", "por");
			criteria.add(Restrictions.eq("por.performanceSummary.id", summaryId));
		};
		return getProductPerformanceOverlayAssetClassDAO().findBySearchCriteria(configurer);
	}


	@Override
	public List<ProductPerformanceOverlayAssetClass> getProductPerformanceOverlayAssetClassListByPerformanceOverlayRun(int performanceOverlayRunId) {
		return getProductPerformanceOverlayAssetClassDAO().findByField("performanceOverlayRun.id", performanceOverlayRunId);
	}


	@Override
	public void saveProductPerformanceOverlayAssetClassListForRun(int performanceRunId, List<ProductPerformanceOverlayAssetClass> list) {
		getProductPerformanceOverlayAssetClassDAO().saveList(list, getProductPerformanceOverlayAssetClassListByPerformanceOverlayRun(performanceRunId));
	}


	private void deleteProductPerformanceOverlayAssetClassByPerformanceOverlayRun(int performanceOverlayRunId) {
		getProductPerformanceOverlayAssetClassDAO().deleteList(getProductPerformanceOverlayAssetClassListByPerformanceOverlayRun(performanceOverlayRunId));
	}

	///////////////////////////////////////////////////////////////////////
	////  ProductPerformanceOverlayAssetClassTrackingError Methods   ////
	///////////////////////////////////////////////////////////////////////


	@Override
	public List<ProductPerformanceOverlayAssetClassTrackingError> getProductPerformanceOverlayAssetClassTrackingErrorListByPerformanceOverlayRun(int performanceOverlayRunId) {
		return getProductPerformanceOverlayAssetClassTrackingErrorDAO().findByField("productPerformanceOverlayRun.id", performanceOverlayRunId);
	}


	@Override
	public List<ProductPerformanceOverlayAssetClassTrackingError> getProductPerformanceOverlayAssetClassTrackingErrorListByPerformanceOverlayRunAndPortfolioWide(int performanceOverlayRunId,
	                                                                                                                                                             boolean portfolioWide) {

		if (portfolioWide) {
			PerformancePortfolioRun performanceOverlayRun = getProductPerformanceOverlayRun(performanceOverlayRunId);

			Integer mainAccountId = performanceOverlayRun.getPerformanceSummary().getClientAccount().getId();
			InvestmentAccount mainAccount = getPortfolioAccountDataRetriever().getMainAccountForCliftonSubAccount(performanceOverlayRun.getPerformanceSummary().getClientAccount(), false,
					performanceOverlayRun.getMeasureDate());

			if (mainAccount != null) {
				mainAccountId = mainAccount.getId();
			}

			//Get list for all accounts
			ProductPerformanceOverlayAssetClassTrackingErrorSearchForm searchForm = new ProductPerformanceOverlayAssetClassTrackingErrorSearchForm();
			searchForm.setClientInvestmentAccountIdOrSubAccount(mainAccountId);
			searchForm.setMeasureDate(performanceOverlayRun.getMeasureDate());

			return getProductPerformanceOverlayAssetClassTrackingErrorList(searchForm);
		}

		return getProductPerformanceOverlayAssetClassTrackingErrorDAO().findByField("productPerformanceOverlayRun.id", performanceOverlayRunId);
	}


	@Override
	public List<ProductPerformanceOverlayAssetClassTrackingError> getProductPerformanceOverlayAssetClassTrackingErrorList(final ProductPerformanceOverlayAssetClassTrackingErrorSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (searchForm.getClientInvestmentAccountIdOrSubAccount() != null) {
					List<InvestmentAccountRelationship> relList = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipListForPurposeActive(
							searchForm.getClientInvestmentAccountIdOrSubAccount(), InvestmentAccountRelationshipPurpose.CLIFTON_SUB_ACCOUNT_PURPOSE_NAME, true);
					if (CollectionUtils.isEmpty(relList)) {
						criteria.add(Restrictions.eq("productPerformanceOverlayRun.performanceSummary.clientAccount.id", searchForm.getClientInvestmentAccountIdOrSubAccount()));
					}
					else {
						Object[] accountIds = BeanUtils.getPropertyValues(relList, "referenceTwo.id");
						ArrayUtils.add(accountIds, searchForm.getClientInvestmentAccountIdOrSubAccount());
						criteria.add(Restrictions.in("productPerformanceOverlayRun.performanceSummary.clientAccount.id", accountIds));
					}
				}
			}
		};
		return getProductPerformanceOverlayAssetClassTrackingErrorDAO().findBySearchCriteria(config);
	}


	private void deleteProductPerformanceOverlayAssetClassTrackingErrorListByPerformanceOverlayRun(int performanceOverlayRunId) {
		getProductPerformanceOverlayAssetClassTrackingErrorDAO().deleteList(getProductPerformanceOverlayAssetClassTrackingErrorListByPerformanceOverlayRun(performanceOverlayRunId));
	}


	@Override
	public void saveProductPerformanceOverlayAssetClassTrackingErrorListForRun(int performanceRunId, List<ProductPerformanceOverlayAssetClassTrackingError> beanList) {
		getProductPerformanceOverlayAssetClassTrackingErrorDAO().saveList(beanList, getProductPerformanceOverlayAssetClassTrackingErrorListByPerformanceOverlayRun(performanceRunId));
	}

	////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////


	private List<ProductPerformancePortfolioRun> convertPerformanceRunListToProductPerformanceRunList(List<PerformancePortfolioRun> performancePortfolioRunList) {
		return CollectionUtils.getConverted(performancePortfolioRunList, this::convertPerformanceRunToProductPerformanceRun);
	}


	private ProductPerformancePortfolioRun convertPerformanceRunToProductPerformanceRun(PerformancePortfolioRun performancePortfolioRun) {
		return performancePortfolioRun == null ? null : ProductPerformancePortfolioRun.fromPerformancePortfolioRun(performancePortfolioRun);
	}

	////////////////////////////////////////////////////////////////////
	/////////           Getter and Setter Methods             //////////
	////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ProductPerformanceOverlayAssetClass, Criteria> getProductPerformanceOverlayAssetClassDAO() {
		return this.productPerformanceOverlayAssetClassDAO;
	}


	public void setProductPerformanceOverlayAssetClassDAO(AdvancedUpdatableDAO<ProductPerformanceOverlayAssetClass, Criteria> productPerformanceOverlayAssetClassDAO) {
		this.productPerformanceOverlayAssetClassDAO = productPerformanceOverlayAssetClassDAO;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public AdvancedUpdatableDAO<ProductPerformanceOverlayAssetClassTrackingError, Criteria> getProductPerformanceOverlayAssetClassTrackingErrorDAO() {
		return this.productPerformanceOverlayAssetClassTrackingErrorDAO;
	}


	public void setProductPerformanceOverlayAssetClassTrackingErrorDAO(
			AdvancedUpdatableDAO<ProductPerformanceOverlayAssetClassTrackingError, Criteria> productPerformanceOverlayAssetClassTrackingErrorDAO) {
		this.productPerformanceOverlayAssetClassTrackingErrorDAO = productPerformanceOverlayAssetClassTrackingErrorDAO;
	}


	public PerformancePortfolioRunService getPerformancePortfolioRunService() {
		return this.performancePortfolioRunService;
	}


	public void setPerformancePortfolioRunService(PerformancePortfolioRunService performancePortfolioRunService) {
		this.performancePortfolioRunService = performancePortfolioRunService;
	}
}
