package com.clifton.product.performance.overlay.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayAssetClass;
import com.clifton.product.performance.rule.ProductPerformanceInvestmentAccountRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * ProductPerformanceOverlayAssetClassOverrideRuleEvaluator Returns violations for each ProductPerformanceOverlayRunAssetClass that has an Overlay Target and/or Gain/Loss Override entered
 * <p>
 *
 * @author manderson
 */
public class ProductPerformanceOverlayAssetClassOverrideRuleEvaluator extends BaseRuleEvaluator<PerformanceInvestmentAccount, ProductPerformanceInvestmentAccountRuleEvaluatorContext> {


	@Override
	public List<RuleViolation> evaluateRule(PerformanceInvestmentAccount performanceInvestmentAccount, RuleConfig ruleConfig, ProductPerformanceInvestmentAccountRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<ProductPerformanceOverlayAssetClass> performanceOverlayAssetClassList = context.getProductPerformanceOverlayAssetClassListList(performanceInvestmentAccount);
			for (ProductPerformanceOverlayAssetClass performanceOverlayAssetClass : CollectionUtils.getIterable(performanceOverlayAssetClassList)) {
				if (performanceOverlayAssetClass.getGainLossOverride() != null || performanceOverlayAssetClass.getOverlayTargetOverride() != null) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("lastUpdateUser", context.getSecurityUser(performanceOverlayAssetClass.getUpdateUserId()));
					violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceInvestmentAccount.getId(), performanceOverlayAssetClass.getId(), null, templateValues));
				}
			}
		}
		return violationList;
	}
}
