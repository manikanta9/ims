package com.clifton.product.performance.overlay.cash;

import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>ProductPerformanceOverlayCashAttributionInfo</code> interface defines the common fields used for cash attribution the can be
 * used at both the daily and asset class level.  Using the common interface and same getters makes it easier for UI to use the same grid and change views by pulling different levels of data.
 *
 * @author manderson
 */
public interface ProductPerformanceOverlayCashAttributionInfo {

	public Integer getPerformanceOverlayRunId();


	public InvestmentAccount getClientAccount();


	public Date getMeasureDate();


	public Date getOverlayRunDate();


	public BigDecimal getFundCash();


	public BigDecimal getFundCashAdjusted();


	public BigDecimal getFundCashAttribution();


	public BigDecimal getManagerCash();


	public BigDecimal getManagerCashAdjusted();


	public BigDecimal getManagerCashAttribution();


	public BigDecimal getTransitionCash();


	public BigDecimal getTransitionCashAdjusted();


	public BigDecimal getTransitionCashAttribution();


	public BigDecimal getRebalanceCash();


	public BigDecimal getRebalanceCashAdjusted();


	public BigDecimal getRebalanceCashAttribution();


	public BigDecimal getClientDirectedCash();


	public BigDecimal getClientDirectedCashAdjusted();


	public BigDecimal getClientDirectedCashAttribution();


	public BigDecimal getCoalesceGainLossOverride();


	public BigDecimal getCashTotal();
}
