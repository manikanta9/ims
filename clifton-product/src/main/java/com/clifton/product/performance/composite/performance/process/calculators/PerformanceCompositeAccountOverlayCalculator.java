package com.clifton.product.performance.composite.performance.process.calculators;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchForm;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.process.calculators.BasePerformanceCalculator;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayService;

import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceCompositeAccountOverlayCalculator</code> copies return values from performance summary objects
 * to the new monthly composite account return objects
 *
 * @author apopp
 */
public class PerformanceCompositeAccountOverlayCalculator extends BasePerformanceCalculator {

	private PerformanceInvestmentAccountService performanceInvestmentAccountService;

	private ProductOverlayService productOverlayService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void calculateMonthlyMetrics(PerformanceCompositeAccountRuleEvaluatorContext config) {
		// Get performance summary for this account
		PerformanceInvestmentAccountSearchForm searchForm = new PerformanceInvestmentAccountSearchForm();
		searchForm.setClientAccountId(config.getInvestmentAccount().getId());
		searchForm.setAccountingPeriodId(config.getAccountingPeriod().getId());
		PerformanceInvestmentAccount performanceInvestmentAccount = CollectionUtils.getOnlyElement(getPerformanceInvestmentAccountService().getPerformanceInvestmentAccountList(searchForm));
		ValidationUtils.assertNotNull(performanceInvestmentAccount, "No monthly performance summary found for account [" + config.getInvestmentAccount().getLabel() + "] for accounting period [" + config.getAccountingPeriod().getLabel() + "]");

		// Get or create the new monthly performance object and populate the gross return from the summary
		PerformanceCompositeInvestmentAccountPerformance performanceCompositeInvestmentAccountPerformance = config.getPerformanceCompositeInvestmentAccountPerformance();
		performanceCompositeInvestmentAccountPerformance.setPeriodGrossReturn(performanceInvestmentAccount.getMonthToDatePortfolioReturn());

		// Populate Period End Market Value to the Main Run for the Last Weekday of the Month's Overlay Target (PERFORMANC-262)
		// Cannot use the Performance Summary Directly because it is a day behind (Balance Date = Measure Date - 1)
		Date lastWeekdayOfMonth = DateUtils.getLastWeekdayOfMonth(config.getAccountingPeriod().getEndDate());
		List<ProductOverlayAssetClassReplication> replicationList = getProductOverlayService().getProductOverlayAssetClassReplicationListByAccountMainRun(config.getInvestmentAccount().getId(), lastWeekdayOfMonth, lastWeekdayOfMonth, true);
		performanceCompositeInvestmentAccountPerformance.setPeriodEndMarketValue(CoreMathUtils.sumProperty(replicationList, ProductOverlayAssetClassReplication::getOverlayTargetTotal));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceInvestmentAccountService getPerformanceInvestmentAccountService() {
		return this.performanceInvestmentAccountService;
	}


	public void setPerformanceInvestmentAccountService(PerformanceInvestmentAccountService performanceInvestmentAccountService) {
		this.performanceInvestmentAccountService = performanceInvestmentAccountService;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}
}
