package com.clifton.product.performance.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Rule Evaluator that creates violations when portfolio runs related to a PerformanceInvestmentAccount has not been submitted to Trading.
 *
 * @author jonathanr
 */
public class PerformanceInvestmentAccountPortfolioRunIncompleteRuleEvaluator extends BaseRuleEvaluator<PerformanceInvestmentAccount, ProductPerformanceInvestmentAccountRuleEvaluatorContext> {

	@Override
	public List<RuleViolation> evaluateRule(PerformanceInvestmentAccount performanceInvestmentAccount, RuleConfig ruleConfig, ProductPerformanceInvestmentAccountRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<PortfolioRun> portfolioRuns = context.getPerformancePortfolioRunPortfolioRunList(performanceInvestmentAccount);
			List<String> violationNotes = new ArrayList<>();
			for (PortfolioRun portfolioRun : CollectionUtils.getIterable(portfolioRuns)) {
				if (!portfolioRun.isSubmittedToTrading()) {
					violationNotes.add("Main Portfolio Run with balance date " + DateUtils.fromDate(portfolioRun.getBalanceDate(), "MM/dd/yyyy") + " is incomplete and has not been submitting to Trading.");
				}
			}
			if (!violationNotes.isEmpty()) {
				Map<String, Object> contextValues = new HashMap<>();
				contextValues.put("violationNotes", violationNotes);
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, performanceInvestmentAccount.getId(), contextValues));
			}
		}
		return ruleViolationList;
	}
}
