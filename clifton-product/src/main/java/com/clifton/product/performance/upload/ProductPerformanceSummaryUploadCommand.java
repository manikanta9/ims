package com.clifton.product.performance.upload;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.file.upload.FileUploadCommand;
import com.clifton.investment.account.InvestmentAccount;


/**
 * The <code>ProductPerformanceSummaryUploadCommand</code> represents a performance summary upload
 *
 * @author apopp
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class ProductPerformanceSummaryUploadCommand extends FileUploadCommand<ProductPerformanceSummaryUploadTemplate> {

	public static final String[] REQUIRED_PROPERTIES = {"measureDate"};

	private InvestmentAccount clientInvestmentAccount;


	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<ProductPerformanceSummaryUploadTemplate> getUploadBeanClass() {
		return ProductPerformanceSummaryUploadTemplate.class;
	}


	@Override
	public String[] getRequiredProperties() {
		return REQUIRED_PROPERTIES;
	}


	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}
}
