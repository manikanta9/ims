package com.clifton.product.performance.overlay.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayAssetClass;
import com.clifton.product.performance.rule.ProductPerformanceInvestmentAccountRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductPerformanceOverlayAssetClassBenchmarkMissingSyntheticRuleEvaluator</code> creates a rule violation
 * for every asset class performance benchmark that doesn't have a synthetic benchmark mapped to it, or if there is a synthetic benchmark
 * mapped, but there is no monthly return value (either by specific market data field, or calculated value from prices).
 * <p>
 *
 * @author manderson
 */
public class ProductPerformanceOverlayAssetClassBenchmarkMissingSyntheticRuleEvaluator extends BaseRuleEvaluator<PerformanceInvestmentAccount, ProductPerformanceInvestmentAccountRuleEvaluatorContext> {


	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataFieldService marketDataFieldService;
	private MarketDataRetriever marketDataRetriever;
	private SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PerformanceInvestmentAccount performanceInvestmentAccount, RuleConfig ruleConfig, ProductPerformanceInvestmentAccountRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();

		Map<InvestmentAccountAssetClass, List<InvestmentSecurity>> accountAssetClassBenchmarkListMap = getInvestmentAccountAssetClassPerformanceBenchmarkListMap(performanceInvestmentAccount, context);
		Map<Integer, BigDecimal> syntheticBenchmarkReturnMap = new HashMap<>();
		for (Map.Entry<InvestmentAccountAssetClass, List<InvestmentSecurity>> investmentAccountAssetClassListEntry : accountAssetClassBenchmarkListMap.entrySet()) {
			EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(investmentAccountAssetClassListEntry.getKey()));
			if (entityConfig != null && !entityConfig.isExcluded()) {
				for (InvestmentSecurity security : investmentAccountAssetClassListEntry.getValue()) {
					Integer syntheticBenchmarkId = (Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_SYNTHETIC_BENCHMARK, false);
					if (syntheticBenchmarkId == null) {
						Map<String, Object> templateValues = new HashMap<>();
						templateValues.put("assetClassName", (investmentAccountAssetClassListEntry.getKey()).getAssetClass().getName());
						violationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(performanceInvestmentAccount), BeanUtils.getIdentityAsLong(investmentAccountAssetClassListEntry.getKey()), BeanUtils.getIdentityAsLong(security), null, templateValues));
					}
					// If the return is missing add a violation
					else if (getSyntheticBenchmarkMonthlyReturn(syntheticBenchmarkReturnMap, syntheticBenchmarkId, performanceInvestmentAccount) == null) {
						Map<String, Object> templateValues = new HashMap<>();
						templateValues.put("assetClassName", (investmentAccountAssetClassListEntry.getKey()).getAssetClass().getName());
						templateValues.put("returnMissing", true);
						violationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(performanceInvestmentAccount), BeanUtils.getIdentityAsLong(investmentAccountAssetClassListEntry.getKey()), MathUtils.getNumberAsLong(syntheticBenchmarkId), null, templateValues));
					}
				}
			}
		}
		return violationList;
	}


	/**
	 * Returns a map of {@link InvestmentAccountAssetClass} objects to their performance benchmarks used on the performance summary
	 * Most cases there will only be one performance benchmark for the asset class for the month, but it's possible that it was changed during the month
	 */
	private Map<InvestmentAccountAssetClass, List<InvestmentSecurity>> getInvestmentAccountAssetClassPerformanceBenchmarkListMap(PerformanceInvestmentAccount performanceInvestmentAccount, ProductPerformanceInvestmentAccountRuleEvaluatorContext context) {
		List<ProductPerformanceOverlayAssetClass> performanceOverlayAssetClassList = context.getProductPerformanceOverlayAssetClassListList(performanceInvestmentAccount);
		Map<InvestmentAccountAssetClass, List<InvestmentSecurity>> accountAssetClassBenchmarkListMap = new HashMap<>();
		for (ProductPerformanceOverlayAssetClass overlayAssetClass : CollectionUtils.getIterable(performanceOverlayAssetClassList)) {
			if (overlayAssetClass.getBenchmarkSecurity() != null) {
				InvestmentAccountAssetClass accountAssetClass = overlayAssetClass.getOverlayAssetClass().getAccountAssetClass();
				List<InvestmentSecurity> benchmarkList = accountAssetClassBenchmarkListMap.get(accountAssetClass);
				if (benchmarkList == null) {
					benchmarkList = CollectionUtils.createList(overlayAssetClass.getBenchmarkSecurity());
				}
				else if (!benchmarkList.contains(overlayAssetClass.getBenchmarkSecurity())) {
					benchmarkList.add(overlayAssetClass.getBenchmarkSecurity());
				}
				accountAssetClassBenchmarkListMap.put(accountAssetClass, benchmarkList);
			}
		}
		return accountAssetClassBenchmarkListMap;
	}


	private BigDecimal getSyntheticBenchmarkMonthlyReturn(Map<Integer, BigDecimal> syntheticBenchmarkReturnMap, Integer syntheticBenchmarkId, PerformanceInvestmentAccount performanceInvestmentAccount) {
		if (!syntheticBenchmarkReturnMap.containsKey(syntheticBenchmarkId)) {
			syntheticBenchmarkReturnMap.put(syntheticBenchmarkId, getSyntheticBenchmarkMonthlyReturnImpl(syntheticBenchmarkId, performanceInvestmentAccount));
		}
		return syntheticBenchmarkReturnMap.get(syntheticBenchmarkId);
	}


	private BigDecimal getSyntheticBenchmarkMonthlyReturnImpl(Integer syntheticBenchmarkId, PerformanceInvestmentAccount performanceInvestmentAccount) {
		// Mimics how reports works.  First try for specific Monthly Return Field Value
		MarketDataValueHolder value = getMarketDataFieldService().getMarketDataValueForDate(syntheticBenchmarkId, performanceInvestmentAccount.getAccountingPeriod().getEndDate(), MarketDataField.FIELD_MONTHLY_RETURN);
		if (value != null) {
			return value.getMeasureValue();
		}
		// Then Try Calculating Return by Prices
		try {
			return getMarketDataRetriever().getInvestmentSecurityReturn(performanceInvestmentAccount.getClientAccount(), getInvestmentInstrumentService().getInvestmentSecurity(syntheticBenchmarkId), DateUtils.getLastDayOfPreviousMonth(performanceInvestmentAccount.getAccountingPeriod().getEndDate()), performanceInvestmentAccount.getAccountingPeriod().getEndDate(), null, false);
		}
		catch (ValidationException exception) {
			// Exception means it's missing - return null
			return null;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
