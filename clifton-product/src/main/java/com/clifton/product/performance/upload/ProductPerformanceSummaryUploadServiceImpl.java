package com.clifton.product.performance.upload;


import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.dataaccess.file.upload.FileUploadHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.account.process.PerformanceInvestmentAccountProcessService;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchForm;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayService;
import com.clifton.product.performance.overlay.ProductPerformancePortfolioRun;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class ProductPerformanceSummaryUploadServiceImpl implements ProductPerformanceSummaryUploadService {

	private AccountingPeriodService accountingPeriodService;

	private CalendarBusinessDayService calendarBusinessDayService;

	private PerformanceInvestmentAccountService performanceInvestmentAccountService;
	private PerformanceInvestmentAccountProcessService performanceInvestmentAccountProcessService;

	private ProductPerformanceOverlayService productPerformanceOverlayService;

	private FileUploadHandler fileUploadHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadProductPerformanceSummaryUploadFile(ProductPerformanceSummaryUploadCommand uploadCommand) {
		List<ProductPerformanceSummaryUploadTemplate> beanList = getFileUploadHandler().convertFileUploadFileToBeanList(uploadCommand);
		ValidationUtils.assertNotEmpty(beanList, "No performance summaries found for import.");

		//Sort lowest date to highest for processing order
		beanList = BeanUtils.sortWithFunction(beanList, ProductPerformanceSummaryUploadTemplate::getMeasureDate, true);
		InvestmentAccount clientAccount = uploadCommand.getClientInvestmentAccount();

		//Get the total list of summaries that exist in the system now for this account
		List<PerformanceInvestmentAccount> currentPerformanceSummaryList = getPerformanceSummaryListByAccount(clientAccount);

		if (currentPerformanceSummaryList == null) {
			currentPerformanceSummaryList = new ArrayList<>();
		}

		//Sort lowest date to highest for processing order
		currentPerformanceSummaryList = BeanUtils.sortWithFunction(currentPerformanceSummaryList, PerformanceInvestmentAccount::getMeasureDate, true);

		processImportBeanList(beanList, clientAccount, currentPerformanceSummaryList, uploadCommand.getExistingBeans() != FileUploadExistingBeanActions.SKIP);
	}


	private void processImportBeanList(List<ProductPerformanceSummaryUploadTemplate> beanList, InvestmentAccount clientInvestmentAccount,
	                                   List<PerformanceInvestmentAccount> currentPerformanceSummaryList, boolean overwriteExistingSummary) {
		//Find the index of the performance summary for the previous business day on the first summary we would like to
		//  import
		Integer startIndex = CoreCollectionUtils.getIndexOfFirstPropertyMatch(currentPerformanceSummaryList, "measureDate",
				DateUtils.getLastDayOfMonth(CollectionUtils.getFirstElementStrict(beanList).getMeasureDate()));

		//In rare cases we will re-upload from the beginning of time when there are already values existing
		// we need to allow the start index to equal 0 in these cases
		if (startIndex == null && !CollectionUtils.isEmpty(currentPerformanceSummaryList)) {
			if (DateUtils.isDateAfterOrEqual(CollectionUtils.getFirstElementStrict(currentPerformanceSummaryList).getMeasureDate(),
					DateUtils.getLastDayOfMonth(CollectionUtils.getFirstElementStrict(beanList).getMeasureDate()))) {
				startIndex = 0;
			}
		}

		//If there are summaries in the system then the index of where we should start cannot be null
		ValidationUtils.assertTrue(CollectionUtils.isEmpty(currentPerformanceSummaryList) || (!CollectionUtils.isEmpty(currentPerformanceSummaryList) && startIndex != null),
				"You must import performance in order. There can be no breaks between what is in the system and what is being imported.");

		//If no summary is found, start from beginning
		if (startIndex == null) {
			startIndex = 0;
		}

		//Process each import template (Each row of the import file)
		processPerformanceSummaryUploadTemplateList(beanList, currentPerformanceSummaryList, clientInvestmentAccount, startIndex, overwriteExistingSummary);
	}


	private void processPerformanceSummaryUploadTemplateList(List<ProductPerformanceSummaryUploadTemplate> beanList, List<PerformanceInvestmentAccount> currentPerformanceSummaryList,
	                                                         InvestmentAccount clientInvestmentAccount, Integer startIndex, boolean overwriteExistingSummary) {

		Date endMeasureDate = null; //Used to determine when this month is over
		PerformanceInvestmentAccount performanceSummary = null; //Current summary we are processing
		List<ProductPerformancePortfolioRun> overlayRunList = new ArrayList<>(); //List of associated performance overlay runs

		for (int i = 0; i < beanList.size(); i++) {
			ProductPerformanceSummaryUploadTemplate summaryUploadTemplate = beanList.get(i);
			ValidationUtils.assertNotNull(summaryUploadTemplate.getMeasureDate(), "Row missing measure date");

			//Find the old performance summary we should be reprocessing
			// if it doesn't exist then we need to create a new one
			if (performanceSummary == null) {
				//This is the first processing for this month. Determine the last day of the month
				//  so we can keep track of when we are done processing this month
				endMeasureDate = DateUtils.getLastDayOfMonth(summaryUploadTemplate.getMeasureDate());

				//If we currently have a summary in the system for this date
				performanceSummary = CollectionUtils.getObjectAtIndex(currentPerformanceSummaryList, startIndex);

				//We have an entry for this date in the system but we aren't support to override
				// so skip this iteration
				if (performanceSummary != null && !overwriteExistingSummary) {
					performanceSummary = null;
					//Only increment index if we are passing another performance summary month instead
					// of a daily calculation
					if ((i + 1) < beanList.size() && !CompareUtils.isEqual(endMeasureDate, DateUtils.getLastDayOfMonth(beanList.get(i + 1).getMeasureDate()))) {
						startIndex = startIndex + 1;
					}
					continue;
				}

				if (performanceSummary == null
						|| (DateUtils.isDateAfterOrEqual(performanceSummary.getMeasureDate(), endMeasureDate) && DateUtils.compare(performanceSummary.getMeasureDate(), endMeasureDate, false) != 0)) {
					// No entry found in the system for this date, create new one.
					performanceSummary = new PerformanceInvestmentAccount();
					performanceSummary.setAccountingPeriod(getAccountingPeriodService().getAccountingPeriodForDate(endMeasureDate));
					performanceSummary.setClientAccount(clientInvestmentAccount);
				}
			}

			//Populate performance summary, this method will only modify fields if they are present
			populatePerformanceSummary(performanceSummary, summaryUploadTemplate.getMonthToDateReturn(), summaryUploadTemplate.getMonthToDateBenchmarkReturn(),
					summaryUploadTemplate.getMonthToDatePortfolioReturn());

			//Create associated performance overlay run if this is day level attributes
			ProductPerformancePortfolioRun performanceOverlayRun = createPerformanceOverlayRunFromProperties(performanceSummary, summaryUploadTemplate.getMeasureDate(),
					summaryUploadTemplate.getGainLoss(), summaryUploadTemplate.getOverlayTarget());

			//Add to list for month
			if (performanceOverlayRun != null) {
				overlayRunList.add(performanceOverlayRun);
			}

			//We are done processing if this is the last element in the list
			// or our current iteration is the last day of the month
			// or the next iteration is next month
			if ((i + 1) == beanList.size() || DateUtils.isDateAfterOrEqual(summaryUploadTemplate.getMeasureDate(), endMeasureDate)
					|| DateUtils.isDateAfterOrEqual(beanList.get(i + 1).getMeasureDate(), DateUtils.addDays(endMeasureDate, 1))) {

				//At this point the performance summary fields should be populated and the
				// list of ProductPerformanceOverlayRun should be populated daily
				// so validate, process, and save these entries
				processPerformanceSummaryAndPerformanceOverlayRunList(performanceSummary, overlayRunList);

				//Make sure we maintain a list of all performance summaries for side field processing
				if (!currentPerformanceSummaryList.contains(performanceSummary)) {
					currentPerformanceSummaryList.add(startIndex, performanceSummary);
				}
				startIndex = startIndex + 1; //Move forward one for next processing

				//If we get here we will continue processing next month.
				performanceSummary = null;
				endMeasureDate = null;
				overlayRunList.clear();
			}
		}

		//Process any remaining summaries that were stored in the system but
		//  are affected by calculation changes
		for (int i = startIndex; i < currentPerformanceSummaryList.size(); i++) {
			PerformanceInvestmentAccount summary = currentPerformanceSummaryList.get(i);
			getPerformanceInvestmentAccountProcessService().recalculatePerformanceInvestmentAccountHistoricalReturns(summary);
		}
	}


	private void processPerformanceSummaryAndPerformanceOverlayRunList(PerformanceInvestmentAccount performanceSummary, List<ProductPerformancePortfolioRun> performanceOverlayRunList) {
		validatePerformanceSummary(performanceSummary); //Validate all properties are set

		//Save summary and performance overlay run list
		saveSummaryAndPerformanceOverlayRunList(performanceSummary, performanceOverlayRunList);

		getPerformanceInvestmentAccountProcessService().recalculatePerformanceInvestmentAccountHistoricalReturns(performanceSummary);
	}


	private void saveSummaryAndPerformanceOverlayRunList(PerformanceInvestmentAccount performanceSummary, List<ProductPerformancePortfolioRun> performanceOverlayRunList) {
		//If performance summary id is not equal to null this means we have loaded a previously saved summary
		// and recalculated it with new values. This means we need to delete the underlying ProductPerformanceOverlayRunList
		//  before we can save the new ones.
		if (performanceSummary.getId() != null && !CollectionUtils.isEmpty(performanceOverlayRunList)) {
			getProductPerformanceOverlayService().deleteProductPerformanceOverlayRunListBySummary(performanceSummary.getId());
		}

		getPerformanceInvestmentAccountService().savePerformanceInvestmentAccount(performanceSummary);
		getProductPerformanceOverlayService().saveProductPerformanceOverlayRunListForSummary(performanceSummary.getId(), performanceOverlayRunList);
	}


	private void validatePerformanceSummary(PerformanceInvestmentAccount performanceSummary) {
		ValidationUtils.assertNotNull(performanceSummary.getMeasureDate(), "Could not find measure date for month");
	}


	/**
	 * Create performance overlay run if sufficient properties are provided
	 */
	private ProductPerformancePortfolioRun createPerformanceOverlayRunFromProperties(PerformanceInvestmentAccount performanceSummary, Date measureDate, BigDecimal gainLoss, BigDecimal overlayTarget) {
		ProductPerformancePortfolioRun performanceOverlayRun = null;

		if (gainLoss != null || overlayTarget != null) {
			performanceOverlayRun = new ProductPerformancePortfolioRun();
			performanceOverlayRun.setPerformanceSummary(performanceSummary);
			performanceOverlayRun.setMeasureDate(measureDate);
			performanceOverlayRun.setPortfolioRunDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(measureDate), -1));
			performanceOverlayRun.setGainLoss(gainLoss);
			performanceOverlayRun.setTarget(overlayTarget);
		}

		return performanceOverlayRun;
	}


	private PerformanceInvestmentAccount populatePerformanceSummary(PerformanceInvestmentAccount performanceSummary, BigDecimal monthToDateReturn, BigDecimal monthToDateBenchmarkReturn,
	                                                                BigDecimal monthToDatePortfolioReturn) {

		if (monthToDateReturn != null || monthToDateBenchmarkReturn != null || monthToDatePortfolioReturn != null) {
			performanceSummary.setMonthToDateReturn(monthToDateReturn);
			performanceSummary.setMonthToDateBenchmarkReturn(monthToDateBenchmarkReturn);
			performanceSummary.setMonthToDatePortfolioReturn(monthToDatePortfolioReturn);
		}

		return performanceSummary;
	}


	private List<PerformanceInvestmentAccount> getPerformanceSummaryListByAccount(InvestmentAccount investmentAccount) {
		PerformanceInvestmentAccountSearchForm searchForm = new PerformanceInvestmentAccountSearchForm();
		searchForm.setClientAccountId(investmentAccount.getId());
		return getPerformanceInvestmentAccountService().getPerformanceInvestmentAccountList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////                Getter & Setters                  ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public FileUploadHandler getFileUploadHandler() {
		return this.fileUploadHandler;
	}


	public void setFileUploadHandler(FileUploadHandler fileUploadHandler) {
		this.fileUploadHandler = fileUploadHandler;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public ProductPerformanceOverlayService getProductPerformanceOverlayService() {
		return this.productPerformanceOverlayService;
	}


	public void setProductPerformanceOverlayService(ProductPerformanceOverlayService productPerformanceOverlayService) {
		this.productPerformanceOverlayService = productPerformanceOverlayService;
	}


	public PerformanceInvestmentAccountService getPerformanceInvestmentAccountService() {
		return this.performanceInvestmentAccountService;
	}


	public void setPerformanceInvestmentAccountService(PerformanceInvestmentAccountService performanceInvestmentAccountService) {
		this.performanceInvestmentAccountService = performanceInvestmentAccountService;
	}


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public PerformanceInvestmentAccountProcessService getPerformanceInvestmentAccountProcessService() {
		return this.performanceInvestmentAccountProcessService;
	}


	public void setPerformanceInvestmentAccountProcessService(PerformanceInvestmentAccountProcessService performanceInvestmentAccountProcessService) {
		this.performanceInvestmentAccountProcessService = performanceInvestmentAccountProcessService;
	}
}
