package com.clifton.product.performance.overlay.process.calculators;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassBenchmark;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.investment.rates.InvestmentRatesService;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetrieverImpl;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.performance.account.process.portfolio.run.calculators.PerformancePortfolioRunCalculator;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayAssetClass;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayService;
import com.clifton.product.performance.overlay.ProductPerformancePortfolioRun;
import com.clifton.product.performance.overlay.process.ProductPerformanceOverlayProcessConfig;
import com.clifton.product.util.ProductUtilService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductPerformanceOverlayAssetClassCalculator</code> calculates returns for each asset class and rolls up totals to the {@link PerformancePortfolioRun}
 *
 * @author manderson
 */
@Component
public class ProductPerformanceOverlayAssetClassCalculator extends PerformancePortfolioRunCalculator<ProductPerformanceOverlayProcessConfig> {

	private static final String ASSET_CLASS_FINANCING_RATE_OVERRIDE_CUSTOM_FIELD = "Financing Rate Override";

	private InvestmentAccountAssetClassService investmentAccountAssetClassService;
	private InvestmentRatesService investmentRatesService;

	private MarketDataRetriever marketDataRetriever;

	private ProductOverlayService productOverlayService;
	private ProductPerformanceOverlayService productPerformanceOverlayService;

	private ProductUtilService productUtilService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void calculate(ProductPerformanceOverlayProcessConfig config, Date date) {
		setupProcessing(config);
		for (ProductPerformancePortfolioRun performanceRun : CollectionUtils.getIterable(getProductPerformanceOverlayRunList(config))) {
			if (date == null || DateUtils.compare(date, performanceRun.getMeasureDate(), false) == 0) {
				processProductPerformancePortfolioRun(config, performanceRun);
			}
		}
	}


	@Override
	@Transactional
	public void saveResults(ProductPerformanceOverlayProcessConfig config, Date date) {
		List<ProductPerformancePortfolioRun> performanceRunList = getProductPerformanceOverlayRunList(config);
		if (!CollectionUtils.isEmpty(performanceRunList)) {
			for (int i = 0; i < performanceRunList.size(); i++) {
				ProductPerformancePortfolioRun run = performanceRunList.get(i);
				if (date == null || DateUtils.compare(date, run.getMeasureDate(), false) == 0) {
					performanceRunList.set(i, saveProductPerformanceOverlayRun(run));
				}
			}
		}
	}


	protected ProductPerformancePortfolioRun saveProductPerformanceOverlayRun(ProductPerformancePortfolioRun performanceRun) {
		return getProductPerformanceOverlayService().saveProductPerformanceOverlayRun(performanceRun);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Transactional
	protected void processProductPerformancePortfolioRun(ProductPerformanceOverlayProcessConfig config, ProductPerformancePortfolioRun performanceRun) {
		processProductPerformanceOverlayRun(config, performanceRun);

		// Rollup to Performance Run
		recalculateProductPerformanceOverlayRun(config, performanceRun);
	}


	protected void processProductPerformanceOverlayRun(ProductPerformanceOverlayProcessConfig config, ProductPerformancePortfolioRun performanceRun) {
		// If no run - nothing to calculate - set all gain loss under "other"
		if (performanceRun.getPortfolioRun() == null) {
			// Don't throw an exception, because accounts that start late or end early it is OK if they are missing runs
			// There are non-ignorable warnings to catch all other cases
			// Put Account Gain/Loss Total Into Other Gain/Loss so we have the amounts
			List<AccountingPositionDaily> positionList = config.getPositionListForDate(performanceRun.getMeasureDate());
			performanceRun.setGainLossOther(CoreMathUtils.sumProperty(positionList, AccountingPositionDaily::getDailyGainLossBase));
			return;
		}
		if (performanceRun.getPortfolioRun().getServiceProcessingType() != ServiceProcessingTypes.PORTFOLIO_TARGET) {
			List<ProductOverlayAssetClass> overlayAssetClassList = getProductOverlayAssetClassListForRun(config, performanceRun);
			processProductPerformanceOverlayAssetClassList(performanceRun, overlayAssetClassList, DateUtils.getPreviousWeekday(performanceRun.getMeasureDate()), config);
		}
	}


	private void processProductPerformanceOverlayAssetClassList(ProductPerformancePortfolioRun performanceRun, List<ProductOverlayAssetClass> acList, Date previousWeekDay,
	                                                            ProductPerformanceOverlayProcessConfig config) {

		// PERFORMANCE-53: Do Not Set Financing Return or Benchmark Return if Before Inception Date
		boolean beforeInception = (config.getPerformanceInceptionDate() == null || DateUtils.compare(performanceRun.getMeasureDate(), config.getPerformanceInceptionDate(), false) < 0);

		// Total Gain/Loss for the day - Once PIOS Replications are processed, anything left over is put into the Gain Loss Other field - some accounts include it in reporting
		List<AccountingPositionDaily> positionList = config.getPositionListForDate(performanceRun.getMeasureDate());
		BigDecimal totalGainLoss = CoreMathUtils.sumProperty(positionList, AccountingPositionDaily::getDailyGainLossBase);

		List<ProductPerformanceOverlayAssetClass> performanceAcList = (performanceRun.isNewBean() ? null
				: getProductPerformanceOverlayService().getProductPerformanceOverlayAssetClassListByPerformanceOverlayRun(performanceRun.getId()));
		List<ProductPerformanceOverlayAssetClass> newPerformanceAcList = new ArrayList<>();

		List<ProductOverlayAssetClassReplication> repList = getProductOverlayService().getProductOverlayAssetClassReplicationListByRun(performanceRun.getPortfolioRun().getId());

		// Filter Replications By the Asset Classes that we need
		repList = BeanUtils.filter(repList, overlayReplication -> CollectionUtils.contains(acList, overlayReplication.getOverlayAssetClass()));

		Map<Integer, List<ProductOverlayAssetClassReplication>> instrumentReplicationMap = new HashMap<>();
		Map<Integer, List<ProductOverlayAssetClassReplication>> currencyReplicationMap = new HashMap<>();
		for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(repList)) {
			// Skip "Cash Exposure" replications
			if (rep.isCashExposure()) {
				continue;
			}
			Integer instrumentId = rep.getSecurity().getInstrument().getId();
			List<ProductOverlayAssetClassReplication> instrumentRepList = instrumentReplicationMap.get(instrumentId);
			if (instrumentRepList == null) {
				instrumentRepList = CollectionUtils.createList(rep);
			}
			else {
				instrumentRepList.add(rep);
			}
			instrumentReplicationMap.put(instrumentId, instrumentRepList);

			// Currency Gain/Loss is applied to the security gain/loss at the proportion the actual currency and actual currency other amount is allocated to it
			if (rep.isCurrencyReplication()) {
				InvestmentSecurity cur = rep.getUnderlyingSecurity();
				List<ProductOverlayAssetClassReplication> curRepList = currencyReplicationMap.get(cur.getId());
				if (curRepList == null) {
					curRepList = CollectionUtils.createList(rep);
				}
				else {
					curRepList.add(rep);
				}
				currencyReplicationMap.put(cur.getId(), curRepList);
			}
		}

		List<PortfolioCurrencyOther> currencyOtherList = getPortfolioRunService().getPortfolioCurrencyOtherListByRun(performanceRun.getPortfolioRun().getId());
		BigDecimal totalBaseCurrencyOther = CoreMathUtils.sumProperty(currencyOtherList, PortfolioCurrencyOther::getTotalBaseAmount);

		BigDecimal currencyOtherTotalGainLoss = BigDecimal.ZERO;
		if (!CollectionUtils.isEmpty(currencyOtherList)) {
			// Currency Other Positions
			InvestmentSecurity[] currencySecurities = BeanUtils.getPropertyValuesUniqueExcludeNull(currencyOtherList, PortfolioCurrencyOther::getCurrencySecurity, InvestmentSecurity.class);
			List<AccountingPositionDaily> currencyOtherPositionList = BeanUtils.filter(positionList, accountingPositionDaily -> ArrayUtils.contains(currencySecurities, accountingPositionDaily.getInvestmentSecurity()));
			currencyOtherTotalGainLoss = CoreMathUtils.sumProperty(currencyOtherPositionList, AccountingPositionDaily::getDailyGainLossBase);
		}

		//If an asset class uses market value overlay calculation we must later validate that there is only one
		boolean assetClassWithMarketValueCalculationUsed = false;

		for (ProductOverlayAssetClass overlayAssetClass : CollectionUtils.getIterable(acList)) {
			// Only process for asset classes with a primary replication
			if (overlayAssetClass.getPrimaryReplication() == null) {
				continue;
			}

			BigDecimal overlayTarget = BigDecimal.ZERO;
			if (InvestmentAccountAssetClass.PerformanceOverlayTargetSources.MARKET_VALUE_INCLUDE_CASH == overlayAssetClass.getAccountAssetClass().getPerformanceOverlayTargetSource()) {
				//Used for later validation to ensure there was a single asset class
				assetClassWithMarketValueCalculationUsed = true;
				overlayTarget = calculateOverlayTargetByMarketValue(performanceRun, config);
			}

			ProductPerformanceOverlayAssetClass performanceOverlayAssetClass = CollectionUtils.getFirstElement(BeanUtils.filter(performanceAcList, ProductPerformanceOverlayAssetClass::getOverlayAssetClass, overlayAssetClass));
			if (performanceOverlayAssetClass == null) {
				performanceOverlayAssetClass = new ProductPerformanceOverlayAssetClass();
				performanceOverlayAssetClass.setOverlayAssetClass(overlayAssetClass);
				performanceOverlayAssetClass.setPerformanceOverlayRun(performanceRun);
			}

			// Gain/Loss - Contract Splitting
			BigDecimal gainLoss = BigDecimal.ZERO;

			BigDecimal currencyOtherAssetClassTotal = BigDecimal.ZERO;
			List<ProductOverlayAssetClassReplication> acRepList = getProductOverlayService().getProductOverlayAssetClassReplicationListByOverlayAssetClass(overlayAssetClass.getId());
			for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(acRepList)) {
				// Skip "Cash Exposure" replications - if it's the only one
				if (rep.isCashExposure() && acRepList.size() == 1) {
					continue;
				}

				overlayTarget = MathUtils.add(overlayTarget, calculateOverlayTargetByAssetClass(rep, overlayAssetClass.getAccountAssetClass().getPerformanceOverlayTargetSource()));

				// If we are still looking at a cash exposure rep - continue on - all we needed to add was overlay target
				if (rep.isCashExposure()) {
					continue;
				}

				List<ProductOverlayAssetClassReplication> instrumentRepList = instrumentReplicationMap.get(rep.getSecurity().getInstrument().getId());
				BigDecimal instrumentGainLoss = BigDecimal.ZERO;

				// If there is only one asset class that uses this replication - give it all
				List<AccountingPositionDaily> instrumentList = BeanUtils.filter(positionList, accountingPositionDaily -> accountingPositionDaily.getAccountingTransaction() == null ? null : accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity().getInstrument(), rep.getSecurity().getInstrument());
				if (!CollectionUtils.isEmpty(instrumentList)) {
					if (CollectionUtils.getSize(instrumentRepList) == 1) {
						instrumentGainLoss = CoreMathUtils.sumProperty(instrumentList, AccountingPositionDaily::getDailyGainLossBase);
					}
					// Otherwise - need to split it
					else if (CollectionUtils.getSize(instrumentRepList) > 1) {
						BigDecimal totalQuantity = CoreMathUtils.sumProperty(instrumentRepList, ProductOverlayAssetClassReplication::getActualContractsAdjusted);
						// If actual total is zero, use targets for proportional allocation
						// shouldn't happen very often, only when positions are first put on and split across asset classes
						// If the analyst doesn't like the split, he can re-allocate to the asset classes he wants using the overrides.
						boolean useTarget = false;
						if (MathUtils.isNullOrZero(totalQuantity)) {
							useTarget = true;
							totalQuantity = CoreMathUtils.sumProperty(instrumentRepList, ProductOverlayAssetClassReplication::getTargetContractsAdjusted);
						}
						BigDecimal percentage = CoreMathUtils.getPercentValue(useTarget ? rep.getTargetContractsAdjusted() : rep.getActualContractsAdjusted(), totalQuantity, true);
						instrumentGainLoss = MathUtils.getPercentageOf(percentage, CoreMathUtils.sumProperty(instrumentList, AccountingPositionDaily::getDailyGainLossBase), true);
					}
					gainLoss = MathUtils.add(gainLoss, instrumentGainLoss);
				}

				// Currency
				if (rep.isCurrencyReplication()) {
					InvestmentSecurity currencySecurity = rep.getUnderlyingSecurity();
					List<AccountingPositionDaily> currencyPositionList = BeanUtils.filter(positionList, accountingPositionDaily -> accountingPositionDaily.getAccountingTransaction() == null ? null : accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity(), currencySecurity);

					if (!CollectionUtils.isEmpty(currencyPositionList)) {
						List<ProductOverlayAssetClassReplication> currencyRepList = currencyReplicationMap.get(rep.getUnderlyingSecurity().getId());
						BigDecimal currencyGainLoss = BigDecimal.ZERO;

						if (CollectionUtils.getSize(currencyRepList) == 1) {
							currencyGainLoss = CoreMathUtils.sumProperty(currencyPositionList, AccountingPositionDaily::getDailyGainLossBase);
						}
						// Otherwise - need to split it
						else if (CollectionUtils.getSize(currencyRepList) > 1) {
							BigDecimal totalAmount = CoreMathUtils.sumProperty(currencyRepList, ProductOverlayAssetClassReplication::getCurrencyActualLocalAmount);
							BigDecimal percentage = CoreMathUtils.getPercentValue(rep.getCurrencyActualLocalAmount(), totalAmount, true);
							currencyGainLoss = MathUtils.getPercentageOf(percentage, CoreMathUtils.sumProperty(currencyPositionList, AccountingPositionDaily::getDailyGainLossBase), true);
						}
						gainLoss = MathUtils.add(gainLoss, currencyGainLoss);
					}
					// Currency Other - Track total currency other applied to this asset class - likely 100%
					if (!CollectionUtils.isEmpty(currencyOtherList) && !MathUtils.isNullOrZero(rep.getCurrencyOtherBaseAmount())) {
						currencyOtherAssetClassTotal = MathUtils.add(currencyOtherAssetClassTotal, rep.getCurrencyOtherBaseAmount());
					}
				}
			}

			// Apply percentage of Currency Other total gain/loss
			if (!MathUtils.isNullOrZero(currencyOtherAssetClassTotal)) {
				BigDecimal percentage = CoreMathUtils.getPercentValue(currencyOtherAssetClassTotal, totalBaseCurrencyOther, true);
				gainLoss = MathUtils.add(gainLoss, MathUtils.getPercentageOf(percentage, currencyOtherTotalGainLoss, true));
			}

			// If Overlay Target is within -1 and 1 - consider it to be zero
			if (MathUtils.isGreaterThan(overlayTarget, -1) && MathUtils.isLessThan(overlayTarget, 1)) {
				performanceOverlayAssetClass.setOverlayTarget(BigDecimal.ZERO);
			}
			else {
				performanceOverlayAssetClass.setOverlayTarget(overlayTarget);
			}

			performanceOverlayAssetClass.setGainLoss(gainLoss);

			// If the map is null, then this client has opted not to include financing rates - or if before inception
			if (config.getInterestRateDailyChangeIndexMap() != null && !beforeInception) {
				InvestmentInterestRateIndex index = null;
				if (!config.getAssetClassFinancingRateMap().containsKey(overlayAssetClass.getAccountAssetClass().getId())) {
					Integer overrideIndexId = (Integer) getProductUtilService().getInvestmentAccountAssetClassCustomFieldValue(overlayAssetClass.getAccountAssetClass(),
							ASSET_CLASS_FINANCING_RATE_OVERRIDE_CUSTOM_FIELD);
					if (overrideIndexId != null) {
						index = getInvestmentRatesService().getInvestmentInterestRateIndex(overrideIndexId);
					}

					if (index == null) {
						index = overlayAssetClass.getPrimaryReplication().getType().getFinancingRateIndex();
					}
					config.getAssetClassFinancingRateMap().put(overlayAssetClass.getAccountAssetClass().getId(), index);
					// Generate daily change if it does not exist - for reuse
					addInterestRateIndexDailyChangeToMap(config.getInterestRateDailyChangeIndexMap(), index, DateUtils.getLastWeekdayOfPreviousMonth(performanceRun.getMeasureDate()));
				}
				else {
					index = config.getAssetClassFinancingRateMap().get(overlayAssetClass.getAccountAssetClass().getId());
				}
				// ((1+(Previous Month End Cost of Capital (3 month LIBOR, T-BILL)))^(1/360)-1)*Days from Previous Performance Detail
				BigDecimal dailyRate = config.getInterestRateDailyChangeIndexMap().get(index.getId());
				// Interest Rate Performance Calculation: Multiply # of days by the deannualized rate
				performanceOverlayAssetClass.setFinancingRate(MathUtils.multiply(dailyRate, DateUtils.getDaysDifference(performanceRun.getMeasureDate(), previousWeekDay)));
			}
			else {
				performanceOverlayAssetClass.setFinancingRate(null);
			}
			// Benchmark Return
			InvestmentAccountAssetClassBenchmark acBenchmark = getAssetClassPerformanceBenchmark(performanceOverlayAssetClass.getOverlayAssetClass().getAccountAssetClass(), performanceRun.getMeasureDate(),
					config.getAssetClassPerformanceBenchmarkMap());
			performanceOverlayAssetClass.setBenchmarkSecurity(acBenchmark.getReferenceTwo());
			// Only Set Benchmark Return if COALESCE(OverlayTargetOverride, OverlayTarget) is not zero and not before inception
			if (!MathUtils.isNullOrZero(performanceOverlayAssetClass.getCoalesceOverlayTargetOverride()) && !beforeInception) {
				// Check if should use account asset class return for benchmark return or look it up
				if (acBenchmark.isUseAccountAssetClassReturn()) {
					performanceOverlayAssetClass.setBenchmarkReturn(performanceOverlayAssetClass.getAssetClassReturn());
				}
				else {
					performanceOverlayAssetClass.setBenchmarkReturn(getMarketDataRetriever().getInvestmentSecurityReturnFlexible(performanceRun.getPerformanceSummary().getClientAccount(), performanceOverlayAssetClass.getBenchmarkSecurity(),
							DateUtils.getPreviousWeekday(performanceRun.getMeasureDate()), performanceRun.getMeasureDate(), "Asset Class Performance Benchmark", true));
				}
			}
			else {
				performanceOverlayAssetClass.setBenchmarkReturn(BigDecimal.ZERO);
			}
			newPerformanceAcList.add(performanceOverlayAssetClass);
		}

		ValidationUtils.assertFalse(assetClassWithMarketValueCalculationUsed && CollectionUtils.getSize(newPerformanceAcList) > 1, "Market Value Include Cash Overlay Calculation Option Set, this is only allowed when there is a single asset class.");

		BigDecimal piosGainLoss = CoreMathUtils.sumProperty(newPerformanceAcList, ProductPerformanceOverlayAssetClass::getGainLoss);

		// Include REPO Accrued Interest In Gain/Loss (Other)
		BigDecimal otherGainLoss = MathUtils.subtract(totalGainLoss, piosGainLoss);
		if (config.getRepoAccruedInterestMap() != null && config.getRepoAccruedInterestMap().containsKey(DateUtils.fromDateShort(performanceRun.getMeasureDate()))) {
			// Negate the interest because it's accruing liability
			BigDecimal repoAccruedInterest = MathUtils.negate(config.getRepoAccruedInterestMap().get(DateUtils.fromDateShort(performanceRun.getMeasureDate())));
			otherGainLoss = MathUtils.add(otherGainLoss, repoAccruedInterest);
		}
		performanceRun.setGainLossOther(otherGainLoss);
		performanceRun.setPerformanceOverlayAssetClassList(newPerformanceAcList);
	}


	private BigDecimal calculateOverlayTargetByAssetClass(ProductOverlayAssetClassReplication rep, InvestmentAccountAssetClass.PerformanceOverlayTargetSources performanceOverlayTargetSources) {
		BigDecimal overlayTarget = BigDecimal.ZERO;
		if (InvestmentAccountAssetClass.PerformanceOverlayTargetSources.ACTUAL_EXPOSURE == performanceOverlayTargetSources) {
			overlayTarget = rep.getOverlayExposureTotal();
		}
		else if (InvestmentAccountAssetClass.PerformanceOverlayTargetSources.TARGET_EXPOSURE == performanceOverlayTargetSources) {
			overlayTarget = rep.getOverlayTargetTotal();
		}

		return overlayTarget;
	}


	private BigDecimal calculateOverlayTargetByMarketValue(PerformancePortfolioRun performanceRun, ProductPerformanceOverlayProcessConfig config) {
		if (config.getCashValueDailyMap() == null) {
			config.setCashValueDailyMap(getPerformanceInvestmentAccountDataRetriever().getCashValueDailyMapForPerformanceAccount(config.getPerformanceAccount().getClientAccount(), config.getPerformanceAccount().getAccountingPeriod(), true, false));
		}
		Date processDate = (performanceRun.getPortfolioRun() != null) ? performanceRun.getPortfolioRun().getBalanceDate() : performanceRun.getMeasureDate();
		List<AccountingPositionDaily> positionList = config.getPositionListForDate(processDate);

		if (positionList == null) {
			// This calculation uses balance date therefore we must ensure positions are looked up on that date. Usually positions are only
			// pre-populated across the month whereas balance date can be adjusted to previous months last weekday
			config.getDailyPositionMap().put(DateUtils.fromDateShort(processDate), getPerformanceInvestmentAccountDataRetriever().getAccountingPositionDailyListForPerformanceAccount(performanceRun.getPerformanceSummary().getClientAccount(), processDate, processDate, true));
			positionList = config.getPositionListForDate(processDate);
		}

		BigDecimal marketValue = CoreMathUtils.sumProperty(positionList, AccountingPositionDaily::getMarketValueBase);
		BigDecimal cashValue = config.getCashValueForDate(processDate);

		//Adjust total cash by asset class allocation percent
		return MathUtils.add(marketValue, cashValue);
	}


	/**
	 * Sets values on the run level based on calculated values/aggregates from asset class level details
	 */
	protected void recalculateProductPerformanceOverlayRun(ProductPerformanceOverlayProcessConfig config, ProductPerformancePortfolioRun performanceRun) {
		performanceRun.setGainLoss(CoreMathUtils.sumProperty(performanceRun.getPerformanceOverlayAssetClassList(), ProductPerformanceOverlayAssetClass::getGainLoss));
		performanceRun.setTarget(CoreMathUtils.sumProperty(performanceRun.getPerformanceOverlayAssetClassList(), ProductPerformanceOverlayAssetClass::getOverlayTarget));

		BigDecimal gainLossCoalesceSum = CoreMathUtils.sumProperty(performanceRun.getPerformanceOverlayAssetClassList(), ProductPerformanceOverlayAssetClass::getCoalesceGainLossOverride);
		if (!MathUtils.isEqual(performanceRun.getGainLoss(), gainLossCoalesceSum)) {
			performanceRun.setGainLossOverride(gainLossCoalesceSum);
		}
		else {
			performanceRun.setGainLossOverride(null);
		}
		BigDecimal overlayTargetCoalesceSum = CoreMathUtils.sumProperty(performanceRun.getPerformanceOverlayAssetClassList(), ProductPerformanceOverlayAssetClass::getCoalesceOverlayTargetOverride);
		if (!MathUtils.isEqual(performanceRun.getTarget(), overlayTargetCoalesceSum)) {
			performanceRun.setTargetOverride(overlayTargetCoalesceSum);
		}
		else {
			performanceRun.setTargetOverride(null);
		}

		// benchmark return for each asset class
		// asset class benchmark return * assetClassOverlayTarget / SUM(ABS(assetClassOverlayTarget))
		BigDecimal benchmarkReturn = BigDecimal.ZERO;
		BigDecimal absAssetClassOverlayTarget = CoreMathUtils.sumProperty(performanceRun.getPerformanceOverlayAssetClassList(), ProductPerformanceOverlayAssetClass::getAbsCoalesceOverlayTargetOverride);
		if (MathUtils.isNullOrZero(absAssetClassOverlayTarget)) {
			performanceRun.setBenchmarkReturn(BigDecimal.ZERO);
			performanceRun.setAccountReturn(BigDecimal.ZERO);
		}
		else {
			for (ProductPerformanceOverlayAssetClass performanceOverlayAssetClass : CollectionUtils.getIterable(performanceRun.getPerformanceOverlayAssetClassList())) {
				BigDecimal assetClassOverlayTarget = performanceOverlayAssetClass.getCoalesceOverlayTargetOverride();
				// JIRA: PERFORMANCE-175 - If the Asset Class Overlay Target is Negative, and Asset Class Account Return = Benchmark Return check
				if (MathUtils.isLessThan(assetClassOverlayTarget, BigDecimal.ZERO) && MathUtils.isEqual(performanceOverlayAssetClass.getAssetClassReturn(), performanceOverlayAssetClass.getBenchmarkReturn())) {
					// and if it is explicitly set to use account return
					InvestmentAccountAssetClassBenchmark acBenchmark = getAssetClassPerformanceBenchmark(performanceOverlayAssetClass.getOverlayAssetClass().getAccountAssetClass(), performanceRun.getMeasureDate(),
							config.getAssetClassPerformanceBenchmarkMap());
					if (acBenchmark.isUseAccountAssetClassReturn()) {
						// use abs of overlay target to prevent reversing the benchmark return
						assetClassOverlayTarget = MathUtils.abs(assetClassOverlayTarget);
					}
				}
				BigDecimal acBenchmarkReturn = MathUtils.divide(MathUtils.multiply(performanceOverlayAssetClass.getBenchmarkReturn(), assetClassOverlayTarget), absAssetClassOverlayTarget);
				benchmarkReturn = MathUtils.add(benchmarkReturn, acBenchmarkReturn);
			}

			performanceRun.setBenchmarkReturn(benchmarkReturn);

			// Daily Return = (gainLoss + overlayTarget*financingRate) / SUM(ABS(assetClassOverlayTarget))
			BigDecimal accountReturn = BigDecimal.ZERO;
			boolean useFinancingAmountOverride = (performanceRun.getFinancingAmountOverride() != null);
			if (!useFinancingAmountOverride) {
				for (ProductPerformanceOverlayAssetClass performanceOverlayAssetClass : CollectionUtils.getIterable(performanceRun.getPerformanceOverlayAssetClassList())) {
					accountReturn = MathUtils.add(accountReturn, performanceOverlayAssetClass.getOverlayTargetTimesFinancingRate());
				}
			}
			else {
				accountReturn = performanceRun.getFinancingAmountOverride();
			}
			accountReturn = MathUtils.add(accountReturn, performanceRun.getCoalesceGainLossOverride());

			Boolean includeGLOther = (Boolean) getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(
					performanceRun.getPerformanceSummary().getClientAccount(), PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_INCLUDE_GL_OTHER_IN_DAILY_ACCOUNT_RETURN_CALCULATIONS);
			if (includeGLOther == null) {
				includeGLOther = false;
			}

			if (includeGLOther) {
				accountReturn = MathUtils.add(accountReturn, performanceRun.getCoalesceGainLossOtherOverride());
			}

			// Multiply By 100 to set value as a percentage
			performanceRun.setAccountReturn(MathUtils.multiply(MathUtils.divide(accountReturn, absAssetClassOverlayTarget), MathUtils.BIG_DECIMAL_ONE_HUNDRED));
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                        Helper Methods                     //////////
	////////////////////////////////////////////////////////////////////////////////


	private List<ProductPerformancePortfolioRun> getProductPerformanceOverlayRunList(ProductPerformanceOverlayProcessConfig config) {
		List<ProductPerformancePortfolioRun> list = config.getProductPerformanceRunList();
		if (list == null) {
			list = getProductPerformanceOverlayService().getProductPerformanceOverlayRunListByPerformanceSummary(config.getPerformanceAccount().getId());
			config.setProductPerformanceRunList(list);
		}
		return list;
	}


	protected List<ProductOverlayAssetClass> getProductOverlayAssetClassListForRun(ProductPerformanceOverlayProcessConfig config, PerformancePortfolioRun performanceRun) {
		if (performanceRun.getPortfolioRun() != null) {
			List<ProductOverlayAssetClass> list = config.getProductOverlayAssetClassListForRun(performanceRun.getPortfolioRun());
			if (list == null) {
				list = getProductOverlayService().getProductOverlayAssetClassListByRun(performanceRun.getPortfolioRun().getId());
				// Filter Asset Class List By the Trading Account (COALESCE PositionInvestmentAccount, Run's Account)
				list = BeanUtils.filter(list, ProductOverlayAssetClass::getTradingClientAccount, performanceRun.getPerformanceSummary().getClientAccount());
				config.setProductOverlayAssetClassListForRun(performanceRun.getPortfolioRun(), list);
			}
			return list;
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	//////       Investment Account Asset Class Performance Benchmarks       ///////
	////////////////////////////////////////////////////////////////////////////////


	private InvestmentAccountAssetClassBenchmark getAssetClassPerformanceBenchmark(InvestmentAccountAssetClass ac, Date date,
	                                                                               Map<Integer, List<InvestmentAccountAssetClassBenchmark>> assetClassBenchmarkMap) {
		List<InvestmentAccountAssetClassBenchmark> list = assetClassBenchmarkMap.get(ac.getId());
		if (list == null) {
			list = getInvestmentAccountAssetClassService().getInvestmentAccountAssetClassBenchmarkListActiveMonthToDate(ac.getId(), DateUtils.getLastDayOfMonth(date));
			assetClassBenchmarkMap.put(ac.getId(), list);
		}
		// If only one, use it
		InvestmentAccountAssetClassBenchmark benchmark = null;
		for (InvestmentAccountAssetClassBenchmark acb : CollectionUtils.getIterable(list)) {
			if (acb.isActiveOnDate(date)) {
				benchmark = acb;
				break;
			}
		}
		if (benchmark == null) {

			InvestmentSecurity benchmarkSecurity = ac.getBenchmarkSecurity();
			if (benchmarkSecurity == null) {
				throw new ValidationExceptionWithCause("InvestmentAccountAssetClass", ac.getId(), "Asset Class [" + ac.getAssetClass().getName()
						+ "] is missing a performance benchmark that is active on [" + DateUtils.fromDateShort(date) + ", and there is not a default benchmark security defined for the asset class.");
			}

			// Create an instance of the InvestmentAccountAssetClassBenchmark to use with the default asset class benchmark
			benchmark = new InvestmentAccountAssetClassBenchmark();
			benchmark.setReferenceTwo(benchmarkSecurity);
		}
		return benchmark;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}


	public InvestmentRatesService getInvestmentRatesService() {
		return this.investmentRatesService;
	}


	public void setInvestmentRatesService(InvestmentRatesService investmentRatesService) {
		this.investmentRatesService = investmentRatesService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public ProductUtilService getProductUtilService() {
		return this.productUtilService;
	}


	public void setProductUtilService(ProductUtilService productUtilService) {
		this.productUtilService = productUtilService;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}


	public ProductPerformanceOverlayService getProductPerformanceOverlayService() {
		return this.productPerformanceOverlayService;
	}


	public void setProductPerformanceOverlayService(ProductPerformanceOverlayService productPerformanceOverlayService) {
		this.productPerformanceOverlayService = productPerformanceOverlayService;
	}
}
