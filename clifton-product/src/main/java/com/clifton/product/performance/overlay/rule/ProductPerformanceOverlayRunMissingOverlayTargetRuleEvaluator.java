package com.clifton.product.performance.overlay.rule;

import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetrieverImpl;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.product.performance.rule.ProductPerformanceInvestmentAccountRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.List;


/**
 * ProductPerformanceOverlayRunMissingOverlayTargetRuleEvaluator Returns violations for each ProductPerformanceOverlayRun that doesn't have an Overlay Target, but has gain/loss associated with it
 * to indicate that the return will be missing on that date
 * <p>
 *
 * @author manderson
 */
public class ProductPerformanceOverlayRunMissingOverlayTargetRuleEvaluator extends BaseRuleEvaluator<PerformanceInvestmentAccount, ProductPerformanceInvestmentAccountRuleEvaluatorContext> {


	@Override
	public List<RuleViolation> evaluateRule(PerformanceInvestmentAccount performanceInvestmentAccount, RuleConfig ruleConfig, ProductPerformanceInvestmentAccountRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();

		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {

			List<PerformancePortfolioRun> performanceRunList = context.getPerformancePortfolioRunList(performanceInvestmentAccount);

			Boolean includeGLOther = (Boolean) context.getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(performanceInvestmentAccount.getClientAccount(),
					PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_INCLUDE_GL_OTHER_IN_DAILY_ACCOUNT_RETURN_CALCULATIONS);
			if (includeGLOther == null) {
				includeGLOther = false;
			}

			for (PerformancePortfolioRun run : CollectionUtils.getIterable(performanceRunList)) {
				if (MathUtils.isNullOrZero(run.getCoalesceTargetOverride())) {
					if ((BooleanUtils.isTrue(includeGLOther) && !MathUtils.isNullOrZero(run.getCoalesceGainLossTotalOverride())) ||
							(BooleanUtils.isFalse(includeGLOther) && !MathUtils.isNullOrZero(run.getCoalesceGainLossOverride()))) {

						violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceInvestmentAccount.getId(), run.getId()));
					}
				}
			}
		}
		return violationList;
	}
}
