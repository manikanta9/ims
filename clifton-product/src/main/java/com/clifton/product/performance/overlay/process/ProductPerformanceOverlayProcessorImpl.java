package com.clifton.product.performance.overlay.process;


import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.portfolio.run.BasePerformanceInvestmentAccountPortfolioRunProcessor;
import com.clifton.performance.account.process.type.PerformanceInvestmentAccountProcessingTypes;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayService;
import org.springframework.transaction.annotation.Transactional;


/**
 * The <code>ProductPerformanceOverlayProcessorImpl</code> ...
 *
 * @author manderson
 */
public class ProductPerformanceOverlayProcessorImpl extends BasePerformanceInvestmentAccountPortfolioRunProcessor<ProductPerformanceOverlayProcessConfig> {

	private ProductPerformanceOverlayService productPerformanceOverlayService;

	///////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////


	@Override
	public ProductPerformanceOverlayProcessConfig createPerformanceInvestmentAccountConfig(PerformanceInvestmentAccount performanceInvestmentAccount, PerformanceInvestmentAccountProcessingTypes processingType) {
		return new ProductPerformanceOverlayProcessConfig(performanceInvestmentAccount, processingType);
	}


	@Override
	@Transactional
	public void deletePerformanceInvestmentAccountProcessing(int performanceAccountId) {
		// Delete Benchmarks
		getPerformanceBenchmarkService().deletePerformanceSummaryBenchmarkListBySummary(performanceAccountId);
		// Delete Overlay Run Processing (This deletes each performance run, which already includes deleting performance asset classes and tracking error asset classes)
		getProductPerformanceOverlayService().deleteProductPerformanceOverlayRunListBySummary(performanceAccountId);
	}

	///////////////////////////////////////////////////////////
	///////         Getter and Setter Methods         /////////
	///////////////////////////////////////////////////////////


	public ProductPerformanceOverlayService getProductPerformanceOverlayService() {
		return this.productPerformanceOverlayService;
	}


	public void setProductPerformanceOverlayService(ProductPerformanceOverlayService productPerformanceOverlayService) {
		this.productPerformanceOverlayService = productPerformanceOverlayService;
	}
}
