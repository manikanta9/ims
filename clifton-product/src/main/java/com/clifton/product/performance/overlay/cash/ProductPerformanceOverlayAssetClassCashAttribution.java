package com.clifton.product.performance.overlay.cash;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayAssetClass;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>ProductPerformanceOverlayAssetClassCashAttribution</code> contains information for an asset class including the
 * adjusted cash buckets, performance for the asset class, and the attribution (gain/loss) attributed to each cash bucket.
 */
@NonPersistentObject
public class ProductPerformanceOverlayAssetClassCashAttribution extends ProductPerformanceOverlayAssetClass implements ProductPerformanceOverlayCashAttributionInfo {


	private BigDecimal fundCash;
	private BigDecimal fundCashAdjusted;
	private BigDecimal fundCashAttribution;

	private BigDecimal managerCash;
	private BigDecimal managerCashAdjusted;
	private BigDecimal managerCashAttribution;

	private BigDecimal transitionCash;
	private BigDecimal transitionCashAdjusted;
	private BigDecimal transitionCashAttribution;

	private BigDecimal rebalanceCash;
	private BigDecimal rebalanceCashAdjusted;
	private BigDecimal rebalanceCashAttribution;

	private BigDecimal clientDirectedCash;
	private BigDecimal clientDirectedCashAdjusted;
	private BigDecimal clientDirectedCashAttribution;


	/**
	 * Since attribution is applied based on performance to each bucket
	 * there is some left over amounts after that attribution is applied
	 * This is usually applied to the rebalance bucket when there is an amount there,
	 * otherwise it is applied to the largest (abs) cash bucket.
	 */
	private BigDecimal plugAttribution;
	private ManagerCashTypes plugAttributionCashType;


	/**
	 * For cash attribution calculations.  By default, the attribution return is
	 * Gain/Loss divided by Overlay Exposure
	 * If overlay exposure is zero then Overlay Target is used
	 * If, when using overlay exposure, the return is greater than 5%, then uses the larger of the abs(overlay exposure) and abs(overlay target)
	 * The override field is defined on {@link com.clifton.product.performance.overlay.ProductPerformanceOverlayAssetClass}
	 * so it can be saved
	 */
	private BigDecimal attributionReturn;

	/**
	 * As the view determines the attribution %, it also generates a calculation note
	 * if something other than the default is used. i.e. when overlay exposure is zero, or value is overridden
	 * Kept on the view as calculation is updated can automatically update message to reflect on screens and violations
	 */
	private String attributionCalculationNote;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccount getClientAccount() {
		if (getPerformanceOverlayRun() != null) {
			return getPerformanceOverlayRun().getPerformanceSummary().getClientAccount();
		}
		return null;
	}


	@Override
	public Date getMeasureDate() {
		if (getPerformanceOverlayRun() != null) {
			return getPerformanceOverlayRun().getMeasureDate();
		}
		return null;
	}


	@Override
	public Integer getPerformanceOverlayRunId() {
		if (getPerformanceOverlayRun() != null) {
			return getPerformanceOverlayRun().getId();
		}
		return null;
	}


	@Override
	public Date getOverlayRunDate() {
		if (getPerformanceOverlayRun() != null) {
			return getPerformanceOverlayRun().getPortfolioRunDate();
		}
		return null;
	}


	@Override
	public BigDecimal getCashTotal() {
		return MathUtils.add(MathUtils.add(MathUtils.add(MathUtils.add(getFundCashAdjusted(), getManagerCashAdjusted()), getTransitionCashAdjusted()), getRebalanceCashAdjusted()), getClientDirectedCashAdjusted());
	}


	public String getAssetClassName() {
		return getOverlayAssetClass().getLabel();
	}


	public BigDecimal getCoalesceAttributionReturnOverride() {
		return ObjectUtils.coalesce(getAttributionReturnOverride(), getAttributionReturn());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getFundCash() {
		return this.fundCash;
	}


	public void setFundCash(BigDecimal fundCash) {
		this.fundCash = fundCash;
	}


	@Override
	public BigDecimal getFundCashAdjusted() {
		return this.fundCashAdjusted;
	}


	public void setFundCashAdjusted(BigDecimal fundCashAdjusted) {
		this.fundCashAdjusted = fundCashAdjusted;
	}


	@Override
	public BigDecimal getFundCashAttribution() {
		return this.fundCashAttribution;
	}


	public void setFundCashAttribution(BigDecimal fundCashAttribution) {
		this.fundCashAttribution = fundCashAttribution;
	}


	@Override
	public BigDecimal getManagerCash() {
		return this.managerCash;
	}


	public void setManagerCash(BigDecimal managerCash) {
		this.managerCash = managerCash;
	}


	@Override
	public BigDecimal getManagerCashAdjusted() {
		return this.managerCashAdjusted;
	}


	public void setManagerCashAdjusted(BigDecimal managerCashAdjusted) {
		this.managerCashAdjusted = managerCashAdjusted;
	}


	@Override
	public BigDecimal getManagerCashAttribution() {
		return this.managerCashAttribution;
	}


	public void setManagerCashAttribution(BigDecimal managerCashAttribution) {
		this.managerCashAttribution = managerCashAttribution;
	}


	@Override
	public BigDecimal getTransitionCash() {
		return this.transitionCash;
	}


	public void setTransitionCash(BigDecimal transitionCash) {
		this.transitionCash = transitionCash;
	}


	@Override
	public BigDecimal getTransitionCashAdjusted() {
		return this.transitionCashAdjusted;
	}


	public void setTransitionCashAdjusted(BigDecimal transitionCashAdjusted) {
		this.transitionCashAdjusted = transitionCashAdjusted;
	}


	@Override
	public BigDecimal getTransitionCashAttribution() {
		return this.transitionCashAttribution;
	}


	public void setTransitionCashAttribution(BigDecimal transitionCashAttribution) {
		this.transitionCashAttribution = transitionCashAttribution;
	}


	@Override
	public BigDecimal getRebalanceCash() {
		return this.rebalanceCash;
	}


	public void setRebalanceCash(BigDecimal rebalanceCash) {
		this.rebalanceCash = rebalanceCash;
	}


	@Override
	public BigDecimal getRebalanceCashAdjusted() {
		return this.rebalanceCashAdjusted;
	}


	public void setRebalanceCashAdjusted(BigDecimal rebalanceCashAdjusted) {
		this.rebalanceCashAdjusted = rebalanceCashAdjusted;
	}


	@Override
	public BigDecimal getRebalanceCashAttribution() {
		return this.rebalanceCashAttribution;
	}


	public void setRebalanceCashAttribution(BigDecimal rebalanceCashAttribution) {
		this.rebalanceCashAttribution = rebalanceCashAttribution;
	}


	@Override
	public BigDecimal getClientDirectedCash() {
		return this.clientDirectedCash;
	}


	public void setClientDirectedCash(BigDecimal clientDirectedCash) {
		this.clientDirectedCash = clientDirectedCash;
	}


	@Override
	public BigDecimal getClientDirectedCashAdjusted() {
		return this.clientDirectedCashAdjusted;
	}


	public void setClientDirectedCashAdjusted(BigDecimal clientDirectedCashAdjusted) {
		this.clientDirectedCashAdjusted = clientDirectedCashAdjusted;
	}


	@Override
	public BigDecimal getClientDirectedCashAttribution() {
		return this.clientDirectedCashAttribution;
	}


	public void setClientDirectedCashAttribution(BigDecimal clientDirectedCashAttribution) {
		this.clientDirectedCashAttribution = clientDirectedCashAttribution;
	}


	public BigDecimal getPlugAttribution() {
		return this.plugAttribution;
	}


	public void setPlugAttribution(BigDecimal plugAttribution) {
		this.plugAttribution = plugAttribution;
	}


	public ManagerCashTypes getPlugAttributionCashType() {
		return this.plugAttributionCashType;
	}


	public void setPlugAttributionCashType(ManagerCashTypes plugAttributionCashType) {
		this.plugAttributionCashType = plugAttributionCashType;
	}


	public BigDecimal getAttributionReturn() {
		return this.attributionReturn;
	}


	public void setAttributionReturn(BigDecimal attributionReturn) {
		this.attributionReturn = attributionReturn;
	}


	public String getAttributionCalculationNote() {
		return this.attributionCalculationNote;
	}


	public void setAttributionCalculationNote(String attributionCalculationNote) {
		this.attributionCalculationNote = attributionCalculationNote;
	}
}
