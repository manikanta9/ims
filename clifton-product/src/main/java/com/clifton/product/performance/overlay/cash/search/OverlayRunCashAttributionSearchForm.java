package com.clifton.product.performance.overlay.cash.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.performance.account.process.portfolio.run.search.PerformancePortfolioRunSearchForm;

import java.math.BigDecimal;


/**
 * @author manderson
 */
public class OverlayRunCashAttributionSearchForm extends PerformancePortfolioRunSearchForm {

	// Custom Search Field = Includes Specific Account or any of its SubAccounts
	private Integer clientAccountIdOrSubAccount;

	@SearchField
	private BigDecimal fundCash;
	@SearchField
	private BigDecimal fundCashAdjusted;
	@SearchField
	private BigDecimal fundCashAttribution;

	@SearchField
	private BigDecimal managerCash;
	@SearchField
	private BigDecimal managerCashAdjusted;
	@SearchField
	private BigDecimal managerCashAttribution;

	@SearchField
	private BigDecimal transitionCash;
	@SearchField
	private BigDecimal transitionCashAdjusted;
	@SearchField
	private BigDecimal transitionCashAttribution;

	@SearchField
	private BigDecimal rebalanceCash;
	@SearchField
	private BigDecimal rebalanceCashAdjusted;
	@SearchField
	private BigDecimal rebalanceCashAttribution;

	@SearchField
	private BigDecimal clientDirectedCash;
	@SearchField
	private BigDecimal clientDirectedCashAdjusted;
	@SearchField
	private BigDecimal clientDirectedCashAttribution;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getFundCash() {
		return this.fundCash;
	}


	public void setFundCash(BigDecimal fundCash) {
		this.fundCash = fundCash;
	}


	public BigDecimal getFundCashAdjusted() {
		return this.fundCashAdjusted;
	}


	public void setFundCashAdjusted(BigDecimal fundCashAdjusted) {
		this.fundCashAdjusted = fundCashAdjusted;
	}


	public BigDecimal getFundCashAttribution() {
		return this.fundCashAttribution;
	}


	public void setFundCashAttribution(BigDecimal fundCashAttribution) {
		this.fundCashAttribution = fundCashAttribution;
	}


	public BigDecimal getManagerCash() {
		return this.managerCash;
	}


	public void setManagerCash(BigDecimal managerCash) {
		this.managerCash = managerCash;
	}


	public BigDecimal getManagerCashAdjusted() {
		return this.managerCashAdjusted;
	}


	public void setManagerCashAdjusted(BigDecimal managerCashAdjusted) {
		this.managerCashAdjusted = managerCashAdjusted;
	}


	public BigDecimal getManagerCashAttribution() {
		return this.managerCashAttribution;
	}


	public void setManagerCashAttribution(BigDecimal managerCashAttribution) {
		this.managerCashAttribution = managerCashAttribution;
	}


	public BigDecimal getTransitionCash() {
		return this.transitionCash;
	}


	public void setTransitionCash(BigDecimal transitionCash) {
		this.transitionCash = transitionCash;
	}


	public BigDecimal getTransitionCashAdjusted() {
		return this.transitionCashAdjusted;
	}


	public void setTransitionCashAdjusted(BigDecimal transitionCashAdjusted) {
		this.transitionCashAdjusted = transitionCashAdjusted;
	}


	public BigDecimal getTransitionCashAttribution() {
		return this.transitionCashAttribution;
	}


	public void setTransitionCashAttribution(BigDecimal transitionCashAttribution) {
		this.transitionCashAttribution = transitionCashAttribution;
	}


	public BigDecimal getRebalanceCash() {
		return this.rebalanceCash;
	}


	public void setRebalanceCash(BigDecimal rebalanceCash) {
		this.rebalanceCash = rebalanceCash;
	}


	public BigDecimal getRebalanceCashAdjusted() {
		return this.rebalanceCashAdjusted;
	}


	public void setRebalanceCashAdjusted(BigDecimal rebalanceCashAdjusted) {
		this.rebalanceCashAdjusted = rebalanceCashAdjusted;
	}


	public BigDecimal getRebalanceCashAttribution() {
		return this.rebalanceCashAttribution;
	}


	public void setRebalanceCashAttribution(BigDecimal rebalanceCashAttribution) {
		this.rebalanceCashAttribution = rebalanceCashAttribution;
	}


	public BigDecimal getClientDirectedCash() {
		return this.clientDirectedCash;
	}


	public void setClientDirectedCash(BigDecimal clientDirectedCash) {
		this.clientDirectedCash = clientDirectedCash;
	}


	public BigDecimal getClientDirectedCashAdjusted() {
		return this.clientDirectedCashAdjusted;
	}


	public void setClientDirectedCashAdjusted(BigDecimal clientDirectedCashAdjusted) {
		this.clientDirectedCashAdjusted = clientDirectedCashAdjusted;
	}


	public BigDecimal getClientDirectedCashAttribution() {
		return this.clientDirectedCashAttribution;
	}


	public void setClientDirectedCashAttribution(BigDecimal clientDirectedCashAttribution) {
		this.clientDirectedCashAttribution = clientDirectedCashAttribution;
	}


	public Integer getClientAccountIdOrSubAccount() {
		return this.clientAccountIdOrSubAccount;
	}


	public void setClientAccountIdOrSubAccount(Integer clientAccountIdOrSubAccount) {
		this.clientAccountIdOrSubAccount = clientAccountIdOrSubAccount;
	}
}
