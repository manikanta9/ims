package com.clifton.product.performance.overlay.cash;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.product.performance.overlay.ProductPerformancePortfolioRun;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>ProductPerformanceOverlayRunCashAttribution</code> is and virtual dto class
 * that is an extension of the {@link ProductPerformancePortfolioRun} class with cash attribution
 * information populated
 *
 * @author manderson
 */
@NonPersistentObject
public class ProductPerformanceOverlayRunCashAttribution extends PerformancePortfolioRun implements ProductPerformanceOverlayCashAttributionInfo {

	private BigDecimal fundCash;
	private BigDecimal fundCashAdjusted;
	private BigDecimal fundCashAttribution;

	private BigDecimal managerCash;
	private BigDecimal managerCashAdjusted;
	private BigDecimal managerCashAttribution;

	private BigDecimal transitionCash;
	private BigDecimal transitionCashAdjusted;
	private BigDecimal transitionCashAttribution;

	private BigDecimal rebalanceCash;
	private BigDecimal rebalanceCashAdjusted;
	private BigDecimal rebalanceCashAttribution;

	private BigDecimal clientDirectedCash;
	private BigDecimal clientDirectedCashAdjusted;
	private BigDecimal clientDirectedCashAttribution;

	private List<ProductPerformanceOverlayAssetClassCashAttribution> assetClassList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccount getClientAccount() {
		if (getPerformanceSummary() != null) {
			return getPerformanceSummary().getClientAccount();
		}
		return null;
	}


	@Override
	public BigDecimal getCashTotal() {
		return MathUtils.add(MathUtils.add(MathUtils.add(MathUtils.add(getFundCashAdjusted(), getManagerCashAdjusted()), getTransitionCashAdjusted()), getRebalanceCashAdjusted()), getClientDirectedCashAdjusted());
	}


	@Override
	public Integer getPerformanceOverlayRunId() {
		return getId();
	}


	@Override
	public Date getOverlayRunDate() {
		return getPortfolioRunDate();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getFundCashAdjusted() {
		return this.fundCashAdjusted;
	}


	public void setFundCashAdjusted(BigDecimal fundCashAdjusted) {
		this.fundCashAdjusted = fundCashAdjusted;
	}


	@Override
	public BigDecimal getFundCashAttribution() {
		return this.fundCashAttribution;
	}


	public void setFundCashAttribution(BigDecimal fundCashAttribution) {
		this.fundCashAttribution = fundCashAttribution;
	}


	@Override
	public BigDecimal getManagerCashAdjusted() {
		return this.managerCashAdjusted;
	}


	public void setManagerCashAdjusted(BigDecimal managerCashAdjusted) {
		this.managerCashAdjusted = managerCashAdjusted;
	}


	@Override
	public BigDecimal getManagerCashAttribution() {
		return this.managerCashAttribution;
	}


	public void setManagerCashAttribution(BigDecimal managerCashAttribution) {
		this.managerCashAttribution = managerCashAttribution;
	}


	@Override
	public BigDecimal getTransitionCashAdjusted() {
		return this.transitionCashAdjusted;
	}


	public void setTransitionCashAdjusted(BigDecimal transitionCashAdjusted) {
		this.transitionCashAdjusted = transitionCashAdjusted;
	}


	@Override
	public BigDecimal getTransitionCashAttribution() {
		return this.transitionCashAttribution;
	}


	public void setTransitionCashAttribution(BigDecimal transitionCashAttribution) {
		this.transitionCashAttribution = transitionCashAttribution;
	}


	@Override
	public BigDecimal getRebalanceCashAdjusted() {
		return this.rebalanceCashAdjusted;
	}


	public void setRebalanceCashAdjusted(BigDecimal rebalanceCashAdjusted) {
		this.rebalanceCashAdjusted = rebalanceCashAdjusted;
	}


	@Override
	public BigDecimal getRebalanceCashAttribution() {
		return this.rebalanceCashAttribution;
	}


	public void setRebalanceCashAttribution(BigDecimal rebalanceCashAttribution) {
		this.rebalanceCashAttribution = rebalanceCashAttribution;
	}


	@Override
	public BigDecimal getClientDirectedCashAdjusted() {
		return this.clientDirectedCashAdjusted;
	}


	public void setClientDirectedCashAdjusted(BigDecimal clientDirectedCashAdjusted) {
		this.clientDirectedCashAdjusted = clientDirectedCashAdjusted;
	}


	@Override
	public BigDecimal getClientDirectedCashAttribution() {
		return this.clientDirectedCashAttribution;
	}


	public void setClientDirectedCashAttribution(BigDecimal clientDirectedCashAttribution) {
		this.clientDirectedCashAttribution = clientDirectedCashAttribution;
	}


	@Override
	public BigDecimal getFundCash() {
		return this.fundCash;
	}


	public void setFundCash(BigDecimal fundCash) {
		this.fundCash = fundCash;
	}


	@Override
	public BigDecimal getManagerCash() {
		return this.managerCash;
	}


	public void setManagerCash(BigDecimal managerCash) {
		this.managerCash = managerCash;
	}


	@Override
	public BigDecimal getTransitionCash() {
		return this.transitionCash;
	}


	public void setTransitionCash(BigDecimal transitionCash) {
		this.transitionCash = transitionCash;
	}


	@Override
	public BigDecimal getRebalanceCash() {
		return this.rebalanceCash;
	}


	public void setRebalanceCash(BigDecimal rebalanceCash) {
		this.rebalanceCash = rebalanceCash;
	}


	@Override
	public BigDecimal getClientDirectedCash() {
		return this.clientDirectedCash;
	}


	public void setClientDirectedCash(BigDecimal clientDirectedCash) {
		this.clientDirectedCash = clientDirectedCash;
	}


	public List<ProductPerformanceOverlayAssetClassCashAttribution> getAssetClassList() {
		return this.assetClassList;
	}


	public void setAssetClassList(List<ProductPerformanceOverlayAssetClassCashAttribution> assetClassList) {
		this.assetClassList = assetClassList;
	}
}
