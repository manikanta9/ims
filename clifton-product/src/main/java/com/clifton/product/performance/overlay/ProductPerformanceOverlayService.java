package com.clifton.product.performance.overlay;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.product.overlay.search.ProductPerformanceOverlayAssetClassTrackingErrorSearchForm;

import java.util.List;


public interface ProductPerformanceOverlayService {

	////////////////////////////////////////////////////////////////////////////////
	///////        Product Performance Overlay Run Business Methods          ///////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Used in overlay run performance processing. This service is an extension of
	 * {@link com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRunService#getPerformancePortfolioRun(int)} (int)}
	 * in that it populates the result with for setting the list of {@link ProductPerformanceOverlayAssetClass}.
	 */
	@SecureMethod(dtoClass = PerformancePortfolioRun.class)
	public ProductPerformancePortfolioRun getProductPerformanceOverlayRun(int id);


	/**
	 * Used in overlay run performance processing. This service is an extension of
	 * {@link com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRunService#getPerformancePortfolioRunListByPerformanceSummary(int)}
	 * in that it allows for setting the list of {@link ProductPerformanceOverlayAssetClass} and {@link ProductPerformanceOverlayAssetClassTrackingError}
	 * to each {@link com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun}.
	 */
	@SecureMethod(dtoClass = PerformancePortfolioRun.class)
	public List<ProductPerformancePortfolioRun> getProductPerformanceOverlayRunListByPerformanceSummary(int summaryId);


	/**
	 * Convenience service for saving {@link com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun} by
	 * handling the conversion from and to {@link ProductPerformancePortfolioRun}.
	 */
	@DoNotAddRequestMapping
	public ProductPerformancePortfolioRun saveProductPerformanceOverlayRun(ProductPerformancePortfolioRun bean);


	/**
	 * Save given list as the {@link ProductPerformancePortfolioRun} for the given summary.
	 * Will get the original list to properly perform inserts/updates/deletes
	 */
	@DoNotAddRequestMapping
	public void saveProductPerformanceOverlayRunListForSummary(int summaryId, List<ProductPerformancePortfolioRun> beanList);


	/**
	 * Deletes the {@link ProductPerformancePortfolioRun} and {@link ProductPerformanceOverlayAssetClass}s and {@link ProductPerformanceOverlayAssetClassTrackingError}s
	 * associated with it.
	 */
	@SecureMethod(dtoClass = PerformancePortfolioRun.class)
	public void deleteProductPerformanceOverlayRun(int id);


	/**
	 * Deletes all {@link ProductPerformancePortfolioRun}s associate with the provided {@link com.clifton.performance.account.PerformanceInvestmentAccount} ID,
	 * and {@link ProductPerformanceOverlayAssetClass}s and {@link ProductPerformanceOverlayAssetClassTrackingError}s associated with each run.
	 */
	@SecureMethod(dtoClass = PerformancePortfolioRun.class)
	public void deleteProductPerformanceOverlayRunListBySummary(int summaryId);

	////////////////////////////////////////////////////////////////////////////////
	///////    Product Performance Overlay Asset Class Business Methods     ////////
	////////////////////////////////////////////////////////////////////////////////


	public List<ProductPerformanceOverlayAssetClass> getProductPerformanceOverlayAssetClassListByPerformanceSummary(int summaryId);


	public List<ProductPerformanceOverlayAssetClass> getProductPerformanceOverlayAssetClassListByPerformanceOverlayRun(int performanceOverlayRunId);


	/**
	 * The performanceRunId is used to get the original list, then does save list with original list so properly inserts/updates/deletes
	 */
	public void saveProductPerformanceOverlayAssetClassListForRun(int performanceRunId, List<ProductPerformanceOverlayAssetClass> list);

	////////////////////////////////////////////////////////////////////////////////
	//////   Product Performance Overlay Tracking Error Business Methods      //////
	////////////////////////////////////////////////////////////////////////////////


	public List<ProductPerformanceOverlayAssetClassTrackingError> getProductPerformanceOverlayAssetClassTrackingErrorListByPerformanceOverlayRun(int performanceOverlayRunId);


	/**
	 * This method will return back all tracking error for the provided performanceOverlayRunId.  If portfolio wide is specified it will take the
	 * account from the provided performance Overlay Run and find it's related main account. A list of all sub-accounts will be created from
	 * this main account.  This whole list is considered the portfolio wide list.  We then iterate over it building a complete list of all
	 * tracking error for each account on the given measure date.
	 */
	public List<ProductPerformanceOverlayAssetClassTrackingError> getProductPerformanceOverlayAssetClassTrackingErrorListByPerformanceOverlayRunAndPortfolioWide(int performanceOverlayRunId,
	                                                                                                                                                             boolean portfolioWide);


	public List<ProductPerformanceOverlayAssetClassTrackingError> getProductPerformanceOverlayAssetClassTrackingErrorList(ProductPerformanceOverlayAssetClassTrackingErrorSearchForm searchForm);


	public void saveProductPerformanceOverlayAssetClassTrackingErrorListForRun(int performanceRunId, List<ProductPerformanceOverlayAssetClassTrackingError> beanList);
}
