package com.clifton.product.performance.overlay.process.calculators;


import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.performance.account.process.portfolio.run.calculators.PerformancePortfolioRunCalculator;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayService;
import com.clifton.product.performance.overlay.ProductPerformancePortfolioRun;
import com.clifton.product.performance.overlay.process.ProductPerformanceOverlayProcessConfig;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * The <code>ProductPerformanceOverlayRunCalculator</code> populates the {@link PerformancePortfolioRun} for each day
 * for the {@link PerformanceInvestmentAccount}
 * <p/>
 * NOTE: This calculator just populates the run information - gain/loss, overlay target, tracking error, etc
 * values are populated during asset class and tracking error processing in separate calculators
 *
 * @author manderson
 */
@Component
public class ProductPerformanceOverlayRunCalculator extends PerformancePortfolioRunCalculator<ProductPerformanceOverlayProcessConfig> {

	private ProductPerformanceOverlayService productPerformanceOverlayService;

	///////////////////////////////////////////////////////////////


	@Override
	public void calculate(ProductPerformanceOverlayProcessConfig config, Date date) {
		if (date == null) {
			config.setProductPerformanceRunList(generateProductPerformanceOverlayRunList(config));
		}
		else {
			// If only editing one date - then nothing to actually process at this level
			// pull existing list and set it - any changes will following in subsequent calculators
			config.setProductPerformanceRunList(getPerformancePortfolioRunListForPerformanceSummary(config.getPerformanceAccount()));
		}
	}


	@Override
	public void saveResults(ProductPerformanceOverlayProcessConfig config, Date date) {
		if (date == null) {
			getProductPerformanceOverlayService().saveProductPerformanceOverlayRunListForSummary(config.getPerformanceAccount().getId(), config.getProductPerformanceRunList());
		}
		// If only editing one date - then nothing was actually processed at this point so nothing to save
	}

	/////////////////////////////////////////////////////////////////////


	private List<ProductPerformancePortfolioRun> generateProductPerformanceOverlayRunList(ProductPerformanceOverlayProcessConfig config) {
		return generatePerformancePortfolioRunList(config);
	}


	@SuppressWarnings("unchecked")
	@Override
	protected <P extends PerformancePortfolioRun> List<P> getPerformancePortfolioRunListForPerformanceSummary(PerformanceInvestmentAccount performanceInvestmentAccount) {
		return (List<P>) getProductPerformanceOverlayService().getProductPerformanceOverlayRunListByPerformanceSummary(performanceInvestmentAccount.getId());
	}


	@SuppressWarnings("unchecked")
	@Override
	protected <P extends PerformancePortfolioRun> P createNewPerformancePortfolioRun() {
		return (P) new ProductPerformancePortfolioRun();
	}

	///////////////////////////////////////////////////////////
	////////         Getter and Setter Methods        /////////
	///////////////////////////////////////////////////////////


	public ProductPerformanceOverlayService getProductPerformanceOverlayService() {
		return this.productPerformanceOverlayService;
	}


	public void setProductPerformanceOverlayService(ProductPerformanceOverlayService productPerformanceOverlayService) {
		this.productPerformanceOverlayService = productPerformanceOverlayService;
	}
}
