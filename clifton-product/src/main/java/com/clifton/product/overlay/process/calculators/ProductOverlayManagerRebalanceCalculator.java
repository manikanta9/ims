package com.clifton.product.overlay.process.calculators;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.manager.InvestmentManagerAccountGroup;
import com.clifton.investment.manager.InvestmentManagerAccountGroupAllocation;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalanceService;
import com.clifton.portfolio.run.process.calculator.BasePortfolioRunProcessStepCalculatorImpl;
import com.clifton.product.overlay.ProductOverlayManagerAccountRebalance;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>ProductOverlayManagerRebalanceCalculator</code> processes {@link ProductOverlayManagerAccountRebalance}s for specific run.
 *
 * @author Mary Anderson
 */
public class ProductOverlayManagerRebalanceCalculator extends BasePortfolioRunProcessStepCalculatorImpl<ProductOverlayRunConfig> {

	private PortfolioRunManagerAccountBalanceService<?> portfolioRunManagerAccountBalanceService;

	private ProductOverlayService productOverlayService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void processStep(ProductOverlayRunConfig runConfig) {
		// Manager Rebalancing
		calculateProductOverlayManagerAccountRebalanceList(runConfig);
	}


	@Override
	protected void saveStep(ProductOverlayRunConfig runConfig) {
		// Save All Manager Related Data
		getProductOverlayService().saveProductOverlayManagerAccountRebalanceListByRun(runConfig.getRun().getId(), runConfig.getOverlayManagerAccountRebalanceList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void calculateProductOverlayManagerAccountRebalanceList(ProductOverlayRunConfig runConfig) {
		// Get the List of Groups to Process
		List<InvestmentManagerAccountGroup> groupList = getInvestmentManagerAccountService().getInvestmentManagerAccountGroupListByInvestmentAccount(runConfig.getRun().getClientInvestmentAccount().getId(), false);
		List<ProductOverlayManagerAccountRebalance> mgrRebalanceList = new ArrayList<>();
		for (InvestmentManagerAccountGroup group : CollectionUtils.getIterable(groupList)) {
			// Pull group from the DB again so we have allocation list populated
			group = getInvestmentManagerAccountService().getInvestmentManagerAccountGroup(group.getId());
			mgrRebalanceList.addAll(generateProductOverlayManagerAccountRebalanceListForGroup(runConfig, group));
		}

		runConfig.setOverlayManagerAccountRebalanceList(mgrRebalanceList);
	}


	private List<ProductOverlayManagerAccountRebalance> generateProductOverlayManagerAccountRebalanceListForGroup(ProductOverlayRunConfig runConfig, InvestmentManagerAccountGroup group) {
		List<ProductOverlayManagerAccountRebalance> mgrRebalanceList = new ArrayList<>();

		// First go through each manager account in the group, verify balance exists and set actual allocation - we'll need this total to then go through each one to calculate proper percentages and rebalance values
		for (InvestmentManagerAccountGroupAllocation allocation : CollectionUtils.getIterable(group.getAllocationList())) {
			ProductOverlayManagerAccountRebalance rebalMgr = createProductOverlayManagerAccountRebalanceActualAllocation(runConfig, allocation);
			mgrRebalanceList.add(rebalMgr);
		}

		// Now, calculate total allocation so we can use this to set targets, rebalance values, etc.
		BigDecimal totalAllocation = CoreMathUtils.sumProperty(mgrRebalanceList, ProductOverlayManagerAccountRebalance::getActualAllocation);

		for (ProductOverlayManagerAccountRebalance rebalMgr : CollectionUtils.getIterable(mgrRebalanceList)) {
			BigDecimal targetPercent = rebalMgr.getGroupAllocation().getTargetAllocation();

			rebalMgr.setActualAllocationPercent(CoreMathUtils.getPercentValue(rebalMgr.getActualAllocation(), totalAllocation, true));
			rebalMgr.setTargetAllocationPercent(targetPercent);
			rebalMgr.setTargetAllocation(MathUtils.getPercentageOf(targetPercent, totalAllocation, true));

			BigDecimal minPercent = rebalMgr.getGroupAllocation().getRebalanceAllocationMin();
			BigDecimal maxPercent = rebalMgr.getGroupAllocation().getRebalanceAllocationMax();

			if (!rebalMgr.getGroupAllocation().isRebalanceAllocationFixed()) {
				// RECALC PERCENTAGE PROPORTIONALLY - CALCULATING AGAINST TARGET
				minPercent = MathUtils.getPercentageOf(minPercent, targetPercent, true);
				maxPercent = MathUtils.getPercentageOf(maxPercent, targetPercent, true);
			}

			// Calculate actual rebalance targets based on given percentage - Negate Min Amounts
			rebalMgr.setRebalanceTriggerMinPercent(MathUtils.negate(minPercent));
			rebalMgr.setRebalanceTriggerMaxPercent(maxPercent);

			rebalMgr.setRebalanceTriggerMin(MathUtils.negate(MathUtils.getPercentageOf(minPercent, totalAllocation, true)));
			rebalMgr.setRebalanceTriggerMax(MathUtils.getPercentageOf(maxPercent, totalAllocation, true));
		}

		if (!CollectionUtils.isEmpty(mgrRebalanceList)) {
			// Clean Up Rounding
			CoreMathUtils.applySumPropertyDifference(mgrRebalanceList, ProductOverlayManagerAccountRebalance::getActualAllocationPercent, ProductOverlayManagerAccountRebalance::setActualAllocationPercent, MathUtils.BIG_DECIMAL_ONE_HUNDRED, true);
			CoreMathUtils.applySumPropertyDifference(mgrRebalanceList, ProductOverlayManagerAccountRebalance::getTargetAllocationPercent, ProductOverlayManagerAccountRebalance::setTargetAllocationPercent, MathUtils.BIG_DECIMAL_ONE_HUNDRED, true);

			CoreMathUtils.applySumPropertyDifference(mgrRebalanceList, ProductOverlayManagerAccountRebalance::getActualAllocation, ProductOverlayManagerAccountRebalance::setActualAllocation, totalAllocation, true);
			CoreMathUtils.applySumPropertyDifference(mgrRebalanceList, ProductOverlayManagerAccountRebalance::getTargetAllocation, ProductOverlayManagerAccountRebalance::setTargetAllocation, totalAllocation, true);
		}
		return mgrRebalanceList;
	}


	private ProductOverlayManagerAccountRebalance createProductOverlayManagerAccountRebalanceActualAllocation(ProductOverlayRunConfig runConfig, InvestmentManagerAccountGroupAllocation groupAllocation) {
		PortfolioRun run = runConfig.getRun();
		ProductOverlayManagerAccountRebalance rebalMgr = new ProductOverlayManagerAccountRebalance();
		rebalMgr.setManagerAccountGroup(groupAllocation.getReferenceOne());
		rebalMgr.setManagerAccount(groupAllocation.getReferenceTwo());
		rebalMgr.setOverlayRun(run);
		rebalMgr.setGroupAllocation(groupAllocation);
		InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(groupAllocation.getReferenceTwo().getId(),
				run.getBalanceDate(), false);

		if (balance == null) {
			throw new ValidationExceptionWithCause("InvestmentManagerAccount", groupAllocation.getReferenceTwo().getId(), "Balance for date: [" + run.getBalanceDate()
					+ "] is missing for manager account [" + groupAllocation.getReferenceTwo().getAccountNumber()
					+ "].  This manager balance is needed to perform manager account rebalance information for manager group [" + groupAllocation.getReferenceOne().getName() + "]");
		}
		if (run.isMarketOnCloseAdjustmentsIncluded()) {
			rebalMgr.setActualAllocation(getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, balance.getManagerAccount(), balance.getBalanceDate(), balance.getMarketOnCloseTotalValue()));
		}
		else {
			rebalMgr.setActualAllocation(getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, balance.getManagerAccount(), balance.getBalanceDate(), balance.getAdjustedTotalValue()));
		}
		return rebalMgr;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////             Getter and Setter Methods               //////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioRunManagerAccountBalanceService<?> getPortfolioRunManagerAccountBalanceService() {
		return this.portfolioRunManagerAccountBalanceService;
	}


	public void setPortfolioRunManagerAccountBalanceService(PortfolioRunManagerAccountBalanceService<?> portfolioRunManagerAccountBalanceService) {
		this.portfolioRunManagerAccountBalanceService = portfolioRunManagerAccountBalanceService;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}
}
