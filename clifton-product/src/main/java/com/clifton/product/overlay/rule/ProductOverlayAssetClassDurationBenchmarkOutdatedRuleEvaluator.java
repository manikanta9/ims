package com.clifton.product.overlay.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductOverlayAssetClassDurationBenchmarkOutdatedRuleEvaluator</code> creates violations
 * when a asset class has a benchmark security that has an outdated duration, or a duration that does not exist.
 *
 * @author stevenf
 */
public class ProductOverlayAssetClassDurationBenchmarkOutdatedRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, ProductOverlayRuleEvaluatorContext> {

	private String marketDataFieldName;
	private MarketDataFieldService marketDataFieldService;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, ProductOverlayRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		List<ProductOverlayAssetClass> list = context.getRunSummaryList(run, false, null);
		for (ProductOverlayAssetClass overlayAssetClass : CollectionUtils.getIterable(list)) {
			EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(overlayAssetClass.getAccountAssetClass()));
			if (entityConfig != null && !entityConfig.isExcluded()) {
				ruleViolationList.addAll(evaluateBenchmarkDuration(overlayAssetClass, entityConfig, run));
			}
		}
		return ruleViolationList;
	}


	private List<RuleViolation> evaluateBenchmarkDuration(ProductOverlayAssetClass overlayAssetClass, EntityConfig entityConfig, PortfolioRun run) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		InvestmentSecurity durationSecurity = overlayAssetClass.getAccountAssetClass().getBenchmarkDurationSecurity();
		if (!MathUtils.isNullOrZero(overlayAssetClass.getBenchmarkDuration()) && durationSecurity != null) {
			Map<String, Object> templateValues = new HashMap<>();
			MarketDataValueHolder duration = getMarketDataFieldService().getMarketDataValueForDateFlexible(durationSecurity.getId(), run.getBalanceDate(), getMarketDataFieldName());
			if (duration != null) {
				int dateDiff = DateUtils.getDaysDifference(run.getBalanceDate(), duration.getMeasureDate());
				int outdatedDaysAllowed = entityConfig.getRuleAmount() != null ? entityConfig.getRuleAmount().intValue() : 0;
				if (MathUtils.isGreaterThan(dateDiff, outdatedDaysAllowed)) {
					templateValues.put("dateDiff", dateDiff);
					templateValues.put("duration", duration);
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), BeanUtils.getIdentityAsLong(overlayAssetClass.getAccountAssetClass()), BeanUtils.getIdentityAsLong(overlayAssetClass), null, templateValues));
				}
			}
			else {
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), BeanUtils.getIdentityAsLong(overlayAssetClass.getAccountAssetClass()), BeanUtils.getIdentityAsLong(overlayAssetClass), null, templateValues));
			}
		}
		return ruleViolationList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public String getMarketDataFieldName() {
		return this.marketDataFieldName;
	}


	public void setMarketDataFieldName(String marketDataFieldName) {
		this.marketDataFieldName = marketDataFieldName;
	}
}
