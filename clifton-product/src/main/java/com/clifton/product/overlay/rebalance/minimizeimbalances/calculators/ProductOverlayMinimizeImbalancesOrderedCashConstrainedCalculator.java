package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator</code> applies cash to asset classes in priority order groupings.
 * If Rebalance Triggers Are Crossed:
 * For each grouping, if we have enough cash to bring deviation to 0 - cash is applied
 * If there is cash left over we move on to the next grouping -
 * For each grouping, if we don't have enough cash to bring to target, applies proportionally using most underweight calculation
 * After processing all groupings, if we have cash left over that is applied as a cash buffer - i.e. not applied
 * If Rebalance Triggers are NOT Crossed:
 * If overweight: Will attempt to keep cash amount = overlay exposure to result in no trade, even if it's overweight because it's within the rebalance band
 * If underweight: will give enough cash to bring up to target
 *
 * @author manderson
 */
public class ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator extends ProductOverlayMinimizeImbalancesUnderweightCalculator {

	public static final String PRIORITY_ORDER_OPTIONS_CUSTOM_COLUMN_GROUP_NAME = "Minimize Imbalances: Priority Order Options";
	public static final String PRIORITY_ORDER_ASSET_CLASS_COLUMN_NAME = "Priority Order";


	@Override
	public String getCustomColumnGroupName() {
		return PRIORITY_ORDER_OPTIONS_CUSTOM_COLUMN_GROUP_NAME;
	}


	@Override
	public boolean calculateMinimizeImbalances(ProductOverlayRunConfig config) {
		BigDecimal totalMinimizeImbalancesCash = getTotalMinimizeImbalanceCash(config);

		// First Filter list to those that we actually will apply cash to:
		List<ProductOverlayAssetClass> overlayAssetClassList = new ArrayList<>();
		// If no rebalance triggers are cross and we have more
		boolean rebalanceTriggersCrossed = false;
		for (ProductOverlayAssetClass oac : CollectionUtils.getIterable(config.getOverlayAssetClassListExcludeRollups())) {
			if (isProcessProductOverlayAssetClass(config, oac)) {
				overlayAssetClassList.add(oac);
				if (oac.isOutsideOfRebalanceTrigger()) {
					rebalanceTriggersCrossed = true;
				}
			}
		}

		BigDecimal totalEffectiveCash = MathUtils.subtract(totalMinimizeImbalancesCash, CoreMathUtils.sumProperty(overlayAssetClassList, ProductOverlayAssetClass::getOverlayExposure));

		// Then Put them in priority or
		Map<Integer, List<ProductOverlayAssetClass>> assetClassOrderMap = calculateOrderAssetClassMap(config, overlayAssetClassList);
		Map<InvestmentAccountAssetClass, BigDecimal> result = new HashMap<>();

		List<Integer> priorityList = new ArrayList<>(assetClassOrderMap.keySet());
		Collections.sort(priorityList);

		BigDecimal currentMinimizeImbalancesCash = totalMinimizeImbalancesCash;
		for (Integer priority : priorityList) {
			//System.out.println("Apply For Priority: " + priority + " Available Cash: " + CoreMathUtils.formatNumberMoney(currentMinimizeImbalancesCash));
			if (!MathUtils.isNullOrZero(currentMinimizeImbalancesCash)) {
				List<ProductOverlayAssetClass> acList = assetClassOrderMap.get(priority);
				// What we need to apply is Target - Physical - Any Cash Already Allocated
				// We need to do this to ensure we don't go overweight
				BigDecimal cashToApply = BigDecimal.ZERO;
				// Need to check each asset class individually in case short targets are not allowed, we don't want to include the difference
				// for asset classes that don't match what we can give
				for (ProductOverlayAssetClass ac : CollectionUtils.getIterable(acList)) {
					BigDecimal acCashToApply = MathUtils.subtract(MathUtils.subtract(ac.getTargetAllocationAdjusted(), ac.getActualAllocationAdjusted()), ac.getCashTotal());

					// If Overweight, i.e. need to sell - verify sell can happen
					if (MathUtils.isLessThan(acCashToApply, BigDecimal.ZERO)) {
						if (!isShortTargetsAllowed()) {
							acCashToApply = MathUtils.abs(ac.getOverlayExposure());
						}
						else if (!isExpandShortPositionsAllowed()) {
							acCashToApply = ac.getOverlayExposure();
						}
					}
					cashToApply = MathUtils.add(cashToApply, acCashToApply);
				}
				// Exception to allow going overweight is if none of the asset classes crossed rebalance trigger, then we can
				// keep overlay exposure that is there so we don't trade
				BigDecimal overlayExposure = CoreMathUtils.sumProperty(acList, ProductOverlayAssetClass::getOverlayExposure);
				// If no rebalance triggers crossed
				if (!rebalanceTriggersCrossed) {
					// and overlay exposure is greater than cash needed - use overlay exposure to result in no trade
					if (MathUtils.isGreaterThanOrEqual(overlayExposure, cashToApply)) {
						cashToApply = overlayExposure;
					}
					// but if overlay exposure is less than cash to apply, apply maximum available from effective cash
					else {
						BigDecimal availableCash = MathUtils.add(overlayExposure, totalEffectiveCash);
						if (MathUtils.isGreaterThanOrEqual(availableCash, cashToApply)) {
							// Remove the difference from total effective cash
							totalEffectiveCash = MathUtils.subtract(totalEffectiveCash, MathUtils.subtract(cashToApply, overlayExposure));
							// Leave Cash to Apply as is since it will bring it back to target
						}
						else {
							// Give it what is available
							cashToApply = availableCash;
							// Zero out effective cash
							totalEffectiveCash = BigDecimal.ZERO;
						}
					}
				}
				// If we have less cash than we need for this grouping - apply what we have
				if (MathUtils.isLessThan(currentMinimizeImbalancesCash, cashToApply)) {
					cashToApply = currentMinimizeImbalancesCash;
				}
				// Calculate for this priority order
				Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap = calculateMinimizeImbalancesImpl(config, acList, cashToApply);

				// Set asset class results in the final map
				for (Map.Entry<InvestmentAccountAssetClass, BigDecimal> investmentAccountAssetClassBigDecimalEntry : assetClassCashMap.entrySet()) {
					result.put(investmentAccountAssetClassBigDecimalEntry.getKey(), investmentAccountAssetClassBigDecimalEntry.getValue());
				}
				currentMinimizeImbalancesCash = MathUtils.subtract(currentMinimizeImbalancesCash, cashToApply);
			}
		}

		if (MathUtils.isGreaterThan(currentMinimizeImbalancesCash, BigDecimal.ZERO)) {
			config.getMinimizeImbalancesCashConfig().appendLineToResultMessage("Extra Cash Left Over (Applied As Cash Buffer): " + CoreMathUtils.formatNumberMoney(currentMinimizeImbalancesCash));
		}

		// Apply Cash to Asset Classes from the map based on MinimizeImbalancesCashConfig setup
		return applyCashToAssetClasses(config, result, true);
	}


	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////


	private Map<Integer, List<ProductOverlayAssetClass>> calculateOrderAssetClassMap(ProductOverlayRunConfig config, List<ProductOverlayAssetClass> overlayAssetClassList) {
		Map<Integer, List<ProductOverlayAssetClass>> assetClassOrderMap = new HashMap<>();
		for (ProductOverlayAssetClass oac : CollectionUtils.getIterable(overlayAssetClassList)) {
			Integer priority = getAssetClassOrder(config, oac.getAccountAssetClass());
			List<ProductOverlayAssetClass> list = assetClassOrderMap.get(priority);
			if (list == null) {
				list = new ArrayList<>();
			}
			list.add(oac);
			assetClassOrderMap.put(priority, list);
		}
		return assetClassOrderMap;
	}


	private Integer getAssetClassOrder(ProductOverlayRunConfig config, InvestmentAccountAssetClass iac) {
		return (Integer) getInvestmentAccountAssetClassCustomFieldValue(config, iac, PRIORITY_ORDER_ASSET_CLASS_COLUMN_NAME, Integer.MAX_VALUE);
	}
}
