package com.clifton.product.overlay.trade.group.dynamic.option;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.trade.PortfolioRunTradeCommand;
import com.clifton.portfolio.run.trade.PortfolioRunTradeService;
import com.clifton.portfolio.run.trade.group.PortfolioRunTradeGroupHandler;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionTradeCommand;
import com.clifton.product.overlay.trade.group.dynamic.option.account.calculators.ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator;
import com.clifton.product.overlay.trade.group.dynamic.option.calculators.ProductOverlayRunTradeGroupDynamicOptionRollCalculator;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;


/**
 * @author NickK
 */
@Service
public class ProductOverlayRunOptionTradeServiceImpl implements ProductOverlayRunOptionTradeService {

	private PortfolioRunService portfolioRunService;
	private PortfolioRunTradeService<ProductOverlayRunOptionTradeGroup, ProductOverlayAssetClassReplication> portfolioRunTradeService;
	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;
	private PortfolioRunTradeGroupHandler<ProductOverlayAssetClassReplication> portfolioRunTradeGroupHandler;

	private TradeGroupService tradeGroupService;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public ProductOverlayRunOptionTradeGroup getProductOverlayRunOptionTradeGroup(int id) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(id);
		if (Objects.isNull(run)) {
			throw new ValidationException("Unable to find Portfolio Run with ID: " + id);
		}
		ProductOverlayRunOptionTradeGroup group = new ProductOverlayRunOptionTradeGroup();
		BeanUtils.copyProperties(run, group);
		populateTradeGroupType(group);

		ProductOverlayRunOptionTradeCommand command = new ProductOverlayRunOptionTradeCommand();
		command.setBalanceDate(run.getBalanceDate());
		populateProductOverlayRunOptionTradeAccount(group, command);

		return group;
	}


	@Override
	@Transactional(readOnly = true)
	public ProductOverlayRunOptionTradeGroup populateProductOverlayRunOptionTradeGroup(ProductOverlayRunOptionTradeGroup bean, boolean livePrices) {
		return populateProductOverlayRunOptionTradeGroupFromTemplate(bean, livePrices, true);
	}


	@Override
	@Transactional(readOnly = true)
	public ProductOverlayRunOptionTradeGroup clearProductOverlayRunOptionTradeGroupTrades(ProductOverlayRunOptionTradeGroup bean) {
		return populateProductOverlayRunOptionTradeGroupFromTemplate(bean, false, false);
	}


	@Override
	@Transactional
	public String processProductOverlayRunOptionTradeGroupApproval(PortfolioRunTradeCommand<ProductOverlayAssetClassReplication> command) {
		PortfolioRun run = command.getPortfolioRun() == null ? null : getPortfolioRunService().getPortfolioRun(command.getPortfolioRun().getId());
		ValidationUtils.assertNotNull(run, "Unable to process Trade Approval. No Portfolio Run found [" + command.getPortfolioRun() + "]");

		command.forEach(detail -> {
			if (detail != null && detail.getPortfolioRun() == null) {
				detail.setPortfolioRun(run);
			}
		});
		return getPortfolioRunTradeService().getPortfolioRunTradeHandlerForRun(command.getPortfolioRun().getId()).processPortfolioRunTradeApproval(command.getBeanList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ProductOverlayRunOptionTradeGroup populateProductOverlayRunOptionTradeGroupFromTemplate(ProductOverlayRunOptionTradeGroup template, boolean livePrices, boolean copyTradeQuantities) {
		if (Objects.isNull(template.getId())) {
			throw new ValidationException("Unable to find Portfolio Run with ID: " + template.getId());
		}

		ProductOverlayRunOptionTradeGroup returnGroup = new ProductOverlayRunOptionTradeGroup();
		BeanUtils.copyProperties(template, returnGroup);
		populateTradeGroupType(returnGroup);

		ProductOverlayRunOptionTradeCommand command = new ProductOverlayRunOptionTradeCommand();
		command.setBalanceDate(returnGroup.getBalanceDate());
		command.setPopulateLivePrices(livePrices);
		command.setExcludeMispricing(returnGroup.isExcludeMispricing());
		populateProductOverlayRunOptionTradeAccount(returnGroup, command);

		if (copyTradeQuantities) {
			copyProductOverlayRunOptionTradeGroupTradeQuantities(template, returnGroup);
		}

		return returnGroup;
	}


	private void populateTradeGroupType(ProductOverlayRunOptionTradeGroup runTradeGroup) {
		if (runTradeGroup.getTradeGroupType() == null || runTradeGroup.getTradeGroupType().getId() == null) {
			runTradeGroup.setTradeGroupType(getTradeGroupService().getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_CASH_FLOW.getTypeName()));
		}
	}


	private void populateProductOverlayRunOptionTradeAccount(ProductOverlayRunOptionTradeGroup runTradeGroup, ProductOverlayRunOptionTradeCommand command) {
		Map<InvestmentAccount, List<ProductOverlayAssetClassReplication>> clientAccountRunDetailListMap = getPortfolioRunTradeGroupHandler().getPortfolioRunAccountTradeGroupDetailMapForRun(runTradeGroup, runTradeGroup.getId());
		Optional<Map.Entry<InvestmentAccount, List<ProductOverlayAssetClassReplication>>> entry = CollectionUtils.getStream(clientAccountRunDetailListMap.entrySet()).findFirst();
		if (!entry.isPresent()) {
			throw new ValidationException("Unable to populate Portfolio Details for Portfolio Run with ID: " + runTradeGroup.getId());
		}
		ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator calculator = getProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator(entry.get().getKey());
		runTradeGroup.setTradeReplicationHolder(calculator.getProductOverlayRunOptionTradeReplicationHolder(command, entry.get().getKey(), entry.get().getValue()));
		// null detailList so it will be repopulated from the underlying replication holder
		runTradeGroup.setDetailList(null);
		getPortfolioRunTradeService().getPortfolioRunTradeHandlerForRun(runTradeGroup.getId()).populatePortfolioRunTradeDetailTradeDefaults(runTradeGroup);
	}


	private ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator getProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator(InvestmentAccount clientAccount) {
		Integer beanId = (Integer) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(clientAccount, ProductOverlayRunTradeGroupDynamicOptionRollCalculator.ACCOUNT_CALCULATOR_BEAN_OVERRIDE_COLUMN_NAME);
		if (beanId == null) {
			beanId = getDefaultAccountCalculatorBeanId();
			ValidationUtils.assertNotNull(getDefaultAccountCalculatorBeanId(), "Missing Account Calculator for Client Account: " + clientAccount);
		}

		SystemBean calculatorBean = getSystemBeanService().getSystemBean(beanId);
		return (ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator) getSystemBeanService().getBeanInstance(calculatorBean);
	}


	// TODO Should refactor the calculation of the accounts so we do not have to get the Option Roll Calculator.
	private Integer getDefaultAccountCalculatorBeanId() {
		SystemBean optionRollCalculatorBean = getTradeGroupService().getTradeGroupTypeByName("Option Roll").getDynamicCalculatorBean();
		ProductOverlayRunTradeGroupDynamicOptionRollCalculator optionRollCalculator = (ProductOverlayRunTradeGroupDynamicOptionRollCalculator) getSystemBeanService().getBeanInstance(optionRollCalculatorBean);
		return optionRollCalculator.getDefaultAccountCalculatorBeanId();
	}


	private void copyProductOverlayRunOptionTradeGroupTradeQuantities(ProductOverlayRunOptionTradeGroup source, ProductOverlayRunOptionTradeGroup destination) {
		for (ProductOverlayAssetClassReplication sourceDetail : CollectionUtils.getIterable(source.getDetailList())) {
			if (MathUtils.isGreaterThan(sourceDetail.getBuyContracts(), BigDecimal.ZERO) || MathUtils.isGreaterThan(sourceDetail.getSellContracts(), BigDecimal.ZERO)) {
				for (ProductOverlayAssetClassReplication destinationDetail : CollectionUtils.getIterable(destination.getDetailList())) {
					if (MathUtils.isEqual(destinationDetail.getId(), sourceDetail.getId())) {
						destinationDetail.setBuyContracts(sourceDetail.getBuyContracts());
						destinationDetail.setSellContracts(sourceDetail.getSellContracts());
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public PortfolioRunTradeService<ProductOverlayRunOptionTradeGroup, ProductOverlayAssetClassReplication> getPortfolioRunTradeService() {
		return this.portfolioRunTradeService;
	}


	public void setPortfolioRunTradeService(PortfolioRunTradeService<ProductOverlayRunOptionTradeGroup, ProductOverlayAssetClassReplication> portfolioRunTradeService) {
		this.portfolioRunTradeService = portfolioRunTradeService;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public PortfolioRunTradeGroupHandler<ProductOverlayAssetClassReplication> getPortfolioRunTradeGroupHandler() {
		return this.portfolioRunTradeGroupHandler;
	}


	public void setPortfolioRunTradeGroupHandler(PortfolioRunTradeGroupHandler<ProductOverlayAssetClassReplication> portfolioRunTradeGroupHandler) {
		this.portfolioRunTradeGroupHandler = portfolioRunTradeGroupHandler;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
