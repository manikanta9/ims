package com.clifton.product.overlay.process.calculators;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass.ClientDirectedCashOptions;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassAssignment;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalance;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceAssetClassHistory;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceHistory;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.targetexposure.InvestmentAccountTargetExposureHandler;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentTypes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAllocationConfigTypes;
import com.clifton.investment.manager.ManagerCashOverlayTypes;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.portfolio.run.process.calculator.BasePortfolioRunReplicationProcessStepCalculator;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.overlay.manager.allocation.ProductOverlayManagerAllocationConfig;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import com.clifton.product.overlay.rebalance.ProductOverlayRebalanceService;
import com.clifton.product.overlay.rebalance.minimizeimbalances.calculators.MinimizeImbalancesCashConfig;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * {@link ProductOverlayAssetClassCalculator} calculator is abstract, because it's extended by the {@link ProductOverlayAssetClassReplicationCalculator} which calls methods here
 * for asset class processing.
 *
 * @author Mary Anderson
 */
public abstract class ProductOverlayAssetClassCalculator extends BasePortfolioRunReplicationProcessStepCalculator<ProductOverlayRunConfig> {

	private static final String FIXED_FUND_VALUE_FIELD = "Fixed Fund Value";
	private static final String USE_MANAGER_TMV_FOR_TPV = "Use Total Market Value for Portfolio";
	private static final String USE_ACTUAL_POLICY_TARGET_TOTAL_PERCENT = "Use Actual Policy Target Total Percent";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentAccountAssetClassService investmentAccountAssetClassService;
	private InvestmentAccountRebalanceService investmentAccountRebalanceService;

	private InvestmentAccountTargetExposureHandler<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> investmentAccountTargetExposureHandler;

	private ProductOverlayRebalanceService productOverlayRebalanceService;
	private ProductOverlayService productOverlayService;

	////////////////////////////////////////////////////////////////////////////
	////////                     Setup                                  ////////
	////////////////////////////////////////////////////////////////////////////


	protected void setupProductOverlayAssetClasses(ProductOverlayRunConfig runConfig) {
		runConfig.setAccountRebalanceConfig(getInvestmentAccountRebalanceService().getInvestmentAccountRebalance(runConfig.getClientInvestmentAccountId()));

		// Returns true if this run uses rebalance exposure targets
		// Also, if it does - validates sum = 100%
		setRebalanceExposureTargetUsed(runConfig);

		// 0.  Generate Initial Set(s) of ProductOverlayAssetClasses
		runConfig.setAssetClassRollupMap(getNewProductOverlayAssetClassMapByRun(runConfig, true));
		runConfig.setAssetClassMap(getNewProductOverlayAssetClassMapByRun(runConfig, false));
		// Setup Minimize Imbalances Cash Config object for applying cash overlay to minimize imbalances
		// need to know how much is in which buckets so can apply it to the correct asset classes proportionally in the correct buckets
		runConfig.setMinimizeImbalancesCashConfig(new MinimizeImbalancesCashConfig());
	}

	////////////////////////////////////////////////////////////////////////////
	///////   Investment Account Asset Class --> Overlay Asset Class     ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Processes InvestmentAccountAssetClass setup on the Overlay Asset Class:
	 * Client Directed Cash - When explicit amount
	 * Sets Targets, Rebalance exposure targets based on Asset Class % and Total Portfolio Value
	 * Processes changes based on {@link InvestmentTargetExposureAdjustmentTypes} selections
	 */
	protected void processProductOverlayAssetClassForInvestmentAccountAssetClasses(ProductOverlayRunConfig runConfig) {
		setupPortfolioTotalValues(runConfig);

		for (Map.Entry<InvestmentAccountAssetClass, ProductOverlayAssetClass> accountAssetClassEntry : runConfig.getAssetClassMap().entrySet()) {
			ProductOverlayAssetClass overlayAssetClass = accountAssetClassEntry.getValue();
			InvestmentAccountAssetClass accountAssetClass = accountAssetClassEntry.getKey();

			// Client Directed Cash Option of Constant and Target - Only set explicitly where Cash Date = Balance Date
			if (accountAssetClass.isClientDirectedCashUsed()) {
				if (DateUtils.compare(accountAssetClass.getClientDirectedCashDate(), overlayAssetClass.getOverlayRun().getBalanceDate(), false) == 0) {
					if (ClientDirectedCashOptions.CONSTANT == accountAssetClass.getClientDirectedCashOption()) {
						// If Constant then as entered is the actual Client Directed cash
						overlayAssetClass.setClientDirectedCash(accountAssetClass.getClientDirectedCashAmount());
					}

					else if (ClientDirectedCashOptions.TARGET == accountAssetClass.getClientDirectedCashOption()
							|| ClientDirectedCashOptions.TARGET_PREVIOUS_DURATION == accountAssetClass.getClientDirectedCashOption()) {
						// If target, the apply Client Directed cash as the difference of client's state target and effective cash.
						overlayAssetClass.setClientDirectedCash(MathUtils.subtract(accountAssetClass.getClientDirectedCashAmount(), overlayAssetClass.getEffectiveCashTotal()));
					}
				}
			}

			BigDecimal target = MathUtils.round(MathUtils.getPercentageOf(accountAssetClass.getAssetClassPercentage(), runConfig.getPortfolioTotalCalculatedValue(), true), 2);
			overlayAssetClass.setTargetAllocation(target);
			overlayAssetClass.setTargetAllocationAdjusted(MathUtils.add(overlayAssetClass.getTargetAllocationAdjusted(), target));

			if (runConfig.isRebalanceExposureTargetsUsed()) {
				BigDecimal rebTarget = MathUtils.round(MathUtils.getPercentageOf(accountAssetClass.getRebalanceExposureTarget(), runConfig.getRebalanceExposurePortfolioTotalValue(), true), 2);
				overlayAssetClass.setRebalanceExposureTarget(rebTarget);
			}

			// Set Actual Adjusted = Actual
			overlayAssetClass.setActualAllocationAdjusted(MathUtils.add(overlayAssetClass.getActualAllocationAdjusted(), overlayAssetClass.getActualAllocation()));

			// Determine Target Adjustments based upon any TFA or Custom targets
			if (accountAssetClass.isTargetExposureAdjustmentUsed()) {
				// Pull From DB again so target assignment list is populated
				accountAssetClass = getInvestmentAccountAssetClassService().getInvestmentAccountAssetClass(accountAssetClass.getId());
				BigDecimal amount = getInvestmentAccountTargetExposureHandler().calculateTargetExposureAdjustedAmount(accountAssetClass, accountAssetClass.getTargetExposureAdjustment(), target, overlayAssetClass.getActualAllocationAdjusted(), runConfig.getBalanceDate(), runConfig.getPortfolioTotalCalculatedValue(), runConfig.getRun().isMarketOnCloseAdjustmentsIncluded());
				BigDecimal totalAdjustment;
				boolean targetAdj = true;

				if (InvestmentTargetExposureAdjustmentTypes.AFT == accountAssetClass.getTargetExposureAdjustment().getTargetExposureAdjustmentType()) {
					overlayAssetClass.setActualAllocationAdjusted(amount);
					totalAdjustment = MathUtils.subtract(overlayAssetClass.getActualAllocation(), amount);
					targetAdj = false;
				}
				else {
					overlayAssetClass.setTargetAllocationAdjusted(amount);
					// The difference between target TFA allocation and actual allocation needs to be redistributed across other asset classes
					// Either proportional or weighted allocation distribution can be used for any subset of NONE (Unadjusted) asset classes
					totalAdjustment = MathUtils.subtract(target, amount);
				}

				if (!MathUtils.isNullOrZero(totalAdjustment)) {
					applyTargetExposureAdjustments(runConfig, accountAssetClass, totalAdjustment, targetAdj);
				}
			}
		}
		setProductOverlayAssetClassRebalanceTriggerValues(runConfig);
	}


	/**
	 * Sets portfolio total value and rebalance exposure portfolio total value
	 * which is eventually saved on the run, but also used to calculate percentage fields for those that are displayed as a percentage of the total target exposure value
	 */
	private void setupPortfolioTotalValues(ProductOverlayRunConfig runConfig) {
		// If Manager TMV is used, set it as override
		Boolean useManagerTotalMarketValue = (Boolean) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(runConfig.getRun().getClientInvestmentAccount(), USE_MANAGER_TMV_FOR_TPV);
		if (useManagerTotalMarketValue != null && useManagerTotalMarketValue) {
			List<ProductOverlayManagerAccount> managerList = BeanUtils.filter(runConfig.getOverlayManagerAccountList(), overlayManagerAccount -> !overlayManagerAccount.isRollupAssetClass());
			runConfig.setPortfolioTotalOverrideValue(CoreMathUtils.sumProperty(managerList, ProductOverlayManagerAccount::getTotalAllocation));
			runConfig.setDoNotUseTPVOverrideValueForAssetClassProcessing(true);
		}
		// Else, use explicit override (mutually exclusive)
		else {
			// Check if there is a TPV override for Fixed Fund Value
			runConfig.setPortfolioTotalOverrideValue((BigDecimal) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(runConfig.getRun().getClientInvestmentAccount(), FIXED_FUND_VALUE_FIELD));
		}

		// Always Set the Standard Calculated Value (Used for Rounding/Sum Fixes)
		BigDecimal total = CoreMathUtils.sumProperty(runConfig.getAssetClassMap().values(), ProductOverlayAssetClass::getActualAllocation);
		BigDecimal rebalanceTotal = total;

		if (runConfig.isRebalanceExposureTargetsUsed()) {
			List<ProductOverlayAssetClass> rebalList = CollectionUtils.getStream(runConfig.getAssetClassMap().values()).filter(ProductOverlayAssetClass::isRebalanceExposureTargetUsed).collect(Collectors.toList());
			rebalanceTotal = CoreMathUtils.sumProperty(rebalList, ProductOverlayAssetClass::getActualAllocation);
		}
		runConfig.setPortfolioTotalCalculatedValue(total);
		runConfig.setRebalanceExposurePortfolioTotalValue(rebalanceTotal);
	}


	/**
	 * Applies adjustments for {@link InvestmentTargetExposureAdjustmentTypes} for dependent overlay asset classes where needed
	 */
	private void applyTargetExposureAdjustments(ProductOverlayRunConfig runConfig, InvestmentAccountAssetClass accountAssetClass, BigDecimal totalAdjustment, boolean target) {
		Map<InvestmentAccountAssetClass, BigDecimal> adjustmentMap = getInvestmentAccountTargetExposureHandler().calculateTargetExposureAdjustmentMap(accountAssetClass, accountAssetClass.getTargetExposureAdjustment(), totalAdjustment, null);
		for (Map.Entry<InvestmentAccountAssetClass, BigDecimal> investmentAccountAssetClassAdjustmentEntry : adjustmentMap.entrySet()) {
			ProductOverlayAssetClass adjAssetClass = runConfig.getAssetClassMap().get(investmentAccountAssetClassAdjustmentEntry.getKey());
			if (adjAssetClass == null) {
				throw new ValidationExceptionWithCause("InvestmentAccountAssetClass", accountAssetClass.getId(), "Asset Class [" + accountAssetClass.getAssetClass().getName()
						+ "] references asset class [" + (investmentAccountAssetClassAdjustmentEntry.getKey()).getAssetClass().getName() + "] for target exposure adjustments, which is not a valid option. "
						+ ((investmentAccountAssetClassAdjustmentEntry.getKey()).isRollupAssetClass() ? "Rollup asset classes cannot be used for Target Exposure Adjustment assignments." : "Please make sure this asset class is active for the account."));
			}
			if (target) {
				adjAssetClass.setTargetAllocationAdjusted(MathUtils.add(adjAssetClass.getTargetAllocationAdjusted(), investmentAccountAssetClassAdjustmentEntry.getValue()));
			}
			else {
				adjAssetClass.setActualAllocationAdjusted(MathUtils.add(adjAssetClass.getActualAllocationAdjusted(), investmentAccountAssetClassAdjustmentEntry.getValue()));
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////
	/////////           Asset Class Benchmark Returns              //////////
	/////////////////////////////////////////////////////////////////////////


	private void setProductOverlayAssetClassBenchmarkReturns(ProductOverlayRunConfig runConfig) {
		for (ProductOverlayAssetClass oac : CollectionUtils.getIterable(runConfig.getOverlayAssetClassListExcludeRollups())) {
			if (oac.getBenchmarkSecurity() != null) {
				oac.setBenchmarkOneDayReturn(getMarketDataRetriever().getInvestmentSecurityReturnFlexible(runConfig.getRun().getClientInvestmentAccount(), oac.getBenchmarkSecurity(),
						runConfig.getPreviousBusinessDay(), runConfig.getBalanceDate(), "Asset Class Benchmark", true));

				oac.setBenchmarkMonthToDateReturn(getMarketDataRetriever().getInvestmentSecurityReturnFlexible(runConfig.getRun().getClientInvestmentAccount(), oac.getBenchmarkSecurity(),
						runConfig.getPreviousMonthEnd(), runConfig.getBalanceDate(), "Asset Class Benchmark", true));
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////
	/////////           Asset Class Rebalance Triggers             //////////
	/////////////////////////////////////////////////////////////////////////


	/**
	 * Sets min/max abs min/abs max Rebalance Trigger Values
	 */
	private void setProductOverlayAssetClassRebalanceTriggerValues(ProductOverlayRunConfig runConfig) {
		setProductOverlayAssetClassRebalanceTriggerValuesImpl(runConfig, false);
		setProductOverlayAssetClassRebalanceTriggerValuesImpl(runConfig, true);
	}


	private void setProductOverlayAssetClassRebalanceTriggerValuesImpl(ProductOverlayRunConfig runConfig, boolean rollup) {
		Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> map = (rollup ? runConfig.getAssetClassRollupMap() : runConfig.getAssetClassMap());
		if (map != null && !CollectionUtils.isEmpty(map)) {
			BigDecimal rebalTotal = runConfig.getRebalanceExposurePortfolioTotalValue();
			// If not using explicit rebal value, use coalesce override/calculated
			if (!runConfig.isRebalanceExposureTargetsUsed()) {
				rebalTotal = runConfig.getCoalescePortfolioTotalValue(true);
			}

			for (Map.Entry<InvestmentAccountAssetClass, ProductOverlayAssetClass> investmentAccountAssetClassProductOverlayAssetClassEntry : map.entrySet()) {
				ProductOverlayAssetClass overlayAssetClass = investmentAccountAssetClassProductOverlayAssetClassEntry.getValue();

				BigDecimal minPercent;
				BigDecimal maxPercent;
				BigDecimal absMinPercent;
				BigDecimal absMaxPercent;

				if ((investmentAccountAssetClassProductOverlayAssetClassEntry.getKey()).isRebalancingUsed()) {
					minPercent = (investmentAccountAssetClassProductOverlayAssetClassEntry.getKey()).getRebalanceAllocationMin();
					maxPercent = (investmentAccountAssetClassProductOverlayAssetClassEntry.getKey()).getRebalanceAllocationMax();
					absMinPercent = (investmentAccountAssetClassProductOverlayAssetClassEntry.getKey()).getRebalanceAllocationAbsoluteMin();
					absMaxPercent = (investmentAccountAssetClassProductOverlayAssetClassEntry.getKey()).getRebalanceAllocationAbsoluteMax();

					BigDecimal percent = overlayAssetClass.getRebalanceExposureTargetPercent();
					// In cases of Negative Targets, we need to use the absolute value, so that our min/max rebalance triggers are calculated correctly
					percent = MathUtils.abs(percent);

					if (!(investmentAccountAssetClassProductOverlayAssetClassEntry.getKey()).isRebalanceAllocationFixed()) {
						// RECALC PERCENTAGE PROPORTIONALLY - CALCULATING AGAINST ADJUSTED TARGET
						minPercent = MathUtils.getPercentageOf(minPercent, percent, true);
						maxPercent = MathUtils.getPercentageOf(maxPercent, percent, true);
						absMinPercent = MathUtils.getPercentageOf(absMinPercent, percent, true);
						absMaxPercent = MathUtils.getPercentageOf(absMaxPercent, percent, true);
					}

					// Calculate actual amount based on given percentage - Negate Min Amounts
					overlayAssetClass.setRebalanceTriggerMin(MathUtils.negate(MathUtils.getPercentageOf(minPercent, rebalTotal, true)));
					overlayAssetClass.setRebalanceTriggerAbsoluteMin(MathUtils.negate(MathUtils.getPercentageOf(absMinPercent, rebalTotal, true)));

					overlayAssetClass.setRebalanceTriggerMax(MathUtils.getPercentageOf(maxPercent, rebalTotal, true));
					overlayAssetClass.setRebalanceTriggerAbsoluteMax(MathUtils.getPercentageOf(absMaxPercent, rebalTotal, true));
				}
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////
	/////////   Overlay Manager --> Overlay Asset Class Methods    //////////
	/////////////////////////////////////////////////////////////////////////


	/**
	 * Processes each Overlay manager and applies the cash/securities balance to the overlay asset classes
	 */
	protected void processProductOverlayAssetClassForInvestmentManagerAccountAssignments(ProductOverlayRunConfig runConfig) {

		List<ProductOverlayManagerAllocationConfig> managerAllocationConfigList = BeanUtils.filter(runConfig.getManagerAllocationConfigList(), managerAllocationConfig -> managerAllocationConfig.getConfigType() != InvestmentManagerAllocationConfigTypes.OVERLAY_ALLOCATION);
		for (ProductOverlayManagerAllocationConfig managerAllocationConfig : managerAllocationConfigList) {
			if (managerAllocationConfig.getAccountAssetClass().isExcludeFromExposure()) {
				continue;
			}
			ProductOverlayAssetClass overlayAssetClass = runConfig.getAssetClassMap().get(managerAllocationConfig.getAccountAssetClass());
			processAssetClassActualAllocation(runConfig, managerAllocationConfig, overlayAssetClass);
		}

		// Cash Overlay Allocations
		List<ProductOverlayManagerAllocationConfig> cashManagerOverlayConfigList = BeanUtils.sortWithFunction(BeanUtils.filter(runConfig.getManagerAllocationConfigList(), managerAllocationConfig -> managerAllocationConfig.getConfigType() == InvestmentManagerAllocationConfigTypes.OVERLAY_ALLOCATION), ProductOverlayManagerAllocationConfig::getOrder, true);
		for (ProductOverlayManagerAllocationConfig cashManagerOverlayConfig : cashManagerOverlayConfigList) {
			if (cashManagerOverlayConfig.getAccountAssetClass().isExcludeFromExposure()) {
				continue;
			}
			processCashAssetClassOverlayAllocation(runConfig, cashManagerOverlayConfig);
		}
	}


	/**
	 * Processes Overlay Manager for Cash Overlay into Overlay Asset Classes (i.e. allocates in correct buckets)
	 * Also builds {@link MinimizeImbalancesCashConfig} for overlaying cash later using minimizing imbalances calculator
	 */
	private void processCashAssetClassOverlayAllocation(ProductOverlayRunConfig runConfig, ProductOverlayManagerAllocationConfig managerOverlayConfig) {
		// Skip Overlay if Cash but not main cash
		// UNLESS it's assigned to another "cash" asset class that isn't main cash.  Otherwise that cash will get double counted
		if (!managerOverlayConfig.getAccountAssetClass().getAssetClass().isMainCash() && managerOverlayConfig.getAccountAssetClass().getAssetClass().isCash()) {
			return;
		}

		BigDecimal overlayAmount = managerOverlayConfig.getOverlayAmount();

		ProductOverlayAssetClass overlayAssetClass = runConfig.getAssetClassMap().get(managerOverlayConfig.getAccountAssetClass());

		if (managerOverlayConfig.getManagerAccountAllocation().isCustomCashOverlay()) {
			if (managerOverlayConfig.isBenchmarkOffsetAdjustment()) {
				// Only supporting Fund Cash Right now
				if (managerOverlayConfig.getManagerAccountAllocation().getBenchmarkAdjustmentOffSetCashOverlayType() == ManagerCashOverlayTypes.FUND) {
					addCashAssetClassOverlayAllocationsForAccountAssetClasses(runConfig, overlayAssetClass, overlayAmount, ManagerCashTypes.FUND);
				}
				else {
					throw new ValidationException("Error processing manager allocation " + managerOverlayConfig.getManagerAccountAllocation().getLabel() + " benchmark adjustment offset cash overlay type = " + managerOverlayConfig.getManagerAccountAllocation().getBenchmarkAdjustmentOffSetCashOverlayType().getLabel() + " which is not supported");
				}
			}
			else {
				overlayAssetClass.addCashToBucket(managerOverlayConfig.getManagerAccountAllocation().getCashType(), overlayAmount);
			}
		}
		else {
			ManagerCashOverlayTypes cashOverlayType = managerOverlayConfig.getManagerAccountAssignment().getCashOverlayType();
			ManagerCashTypes cashType = managerOverlayConfig.getManagerAccountAssignment().getCashType();

			// Overlay manager cash balance using Fund Cash allocation weights
			if (ManagerCashOverlayTypes.FUND == cashOverlayType) {
				// Fund Cash allocation weights are defined for PIOS account asset classes and sum to 100%.
				// Selecting this option will put manager's cash balance into Fund Cash pool.
				// Fund cash is typically used for cash accounts (Clifton account, cash asset class accounts) and its balance to overlay is usually allocated across main asset classes.
				addCashAssetClassOverlayAllocationsForAccountAssetClasses(runConfig, overlayAssetClass, overlayAmount, cashType);
			}
			else if (ManagerCashOverlayTypes.MINIMIZE_IMBALANCES == cashOverlayType) {
				// Allocate manager cash to asset class(es) in order to minimize imbalances
				// We don't know how this will actually be applied, i.e. underweight, so add the amount to the config object to be allocated across asset classes later.
				// Stored in the config object so we know which buckets the cash is coming from/should be allocated to.
				// For attribution purposes we need to know which asset class the cash buck is for so that can be retained
				runConfig.getMinimizeImbalancesCashConfig().addCashToBucket(managerOverlayConfig.getAccountAssetClass(), cashType, overlayAmount);
			}
			else if (ManagerCashOverlayTypes.MANAGER == cashOverlayType) {
				overlayAssetClass.addCashToBucket(cashType, overlayAmount);
			}

			// If Used Cash Overlay Adjustment, Check if should apply adjustment to Securities
			if (managerOverlayConfig.getOverlayAdjustmentAction() != null && managerOverlayConfig.getOverlayAdjustmentAction().isApplyDifferenceToSecurities() && !MathUtils.isNullOrZero(managerOverlayConfig.getOverlayAdjustment())) {
				overlayAssetClass.setActualAllocation(MathUtils.subtract(overlayAssetClass.getActualAllocation(), managerOverlayConfig.getOverlayAdjustment()));
			}

			// If Used Max Cash Limits, Check if should apply excess to Securities
			BigDecimal adjustedOverlayAmount = MathUtils.add(managerOverlayConfig.getAmount(), managerOverlayConfig.getOverlayAdjustment());
			if (managerOverlayConfig.getOverlayLimitAction() != null && managerOverlayConfig.getOverlayLimitAction().isApplyDifferenceToSecurities() && MathUtils.isGreaterThan(adjustedOverlayAmount, managerOverlayConfig.getOverlayLimit())) {
				BigDecimal overLimit = MathUtils.subtract(adjustedOverlayAmount, managerOverlayConfig.getOverlayLimit());
				overlayAssetClass.setActualAllocation(MathUtils.add(overlayAssetClass.getActualAllocation(), overLimit));
			}
		}
	}


	private void addCashAssetClassOverlayAllocationsForAccountAssetClasses(ProductOverlayRunConfig runConfig, ProductOverlayAssetClass overlayAssetClass, BigDecimal overlayAmount, ManagerCashTypes type) {
		for (Map.Entry<InvestmentAccountAssetClass, ProductOverlayAssetClass> accountAssetClassEntry : runConfig.getAssetClassMap().entrySet()) {
			InvestmentAccountAssetClass accountAssetClass = accountAssetClassEntry.getKey();
			if (!MathUtils.isEqual(0, accountAssetClass.getCashPercentage()) || accountAssetClass.isCashAdjustmentUsed()) {
				ProductOverlayAssetClass fundOverlayAssetClass = accountAssetClassEntry.getValue();
				BigDecimal cashAmount = MathUtils.getPercentageOf(accountAssetClass.getCashPercentage(), overlayAmount, true);
				if (accountAssetClass.isCashAdjustmentUsed()) {
					BigDecimal amount = getInvestmentAccountTargetExposureHandler().calculateTargetExposureAdjustedAmount(accountAssetClass, accountAssetClass.getCashAdjustment(),
							cashAmount, overlayAssetClass.getActualAllocationAdjusted(), runConfig.getBalanceDate(), overlayAmount, runConfig.getRun().isMarketOnCloseAdjustmentsIncluded());
					fundOverlayAssetClass.addCashToBucket(type, amount);
					// Apply cash adjustments to assignments
					BigDecimal totalAdjustment = MathUtils.subtract(cashAmount, amount);
					if (!MathUtils.isNullOrZero(totalAdjustment)) {
						applyCashAdjustments(runConfig, accountAssetClass, totalAdjustment, type);
					}
				}
				else {
					fundOverlayAssetClass.addCashToBucket(type, cashAmount);
				}
			}
		}
	}


	/**
	 * Applies adjustments for Cash {@link com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustment} for dependent overlay asset classes where needed
	 */
	private void applyCashAdjustments(ProductOverlayRunConfig runConfig, InvestmentAccountAssetClass accountAssetClass, BigDecimal totalAdjustment, ManagerCashTypes type) {
		Map<InvestmentAccountAssetClass, BigDecimal> adjustmentMap = getInvestmentAccountTargetExposureHandler().calculateTargetExposureAdjustmentMap(accountAssetClass, accountAssetClass.getCashAdjustment(), totalAdjustment, null);
		for (Map.Entry<InvestmentAccountAssetClass, BigDecimal> investmentAccountAssetClassAdjustmentEntry : adjustmentMap.entrySet()) {
			ProductOverlayAssetClass adjAssetClass = runConfig.getAssetClassMap().get(investmentAccountAssetClassAdjustmentEntry.getKey());
			if (adjAssetClass == null) {
				throw new ValidationExceptionWithCause("InvestmentAccountAssetClass", accountAssetClass.getId(), "Asset Class [" + accountAssetClass.getAssetClass().getName()
						+ "] references asset class [" + (investmentAccountAssetClassAdjustmentEntry.getKey()).getAssetClass().getName() + "] for cash adjustments, which is not a valid option. "
						+ ((investmentAccountAssetClassAdjustmentEntry.getKey()).isRollupAssetClass() ? "Rollup asset classes cannot be used for Cash Adjustment assignments." : "Please make sure this asset class is active for the account."));
			}
			adjAssetClass.addCashToBucket(type, investmentAccountAssetClassAdjustmentEntry.getValue());
		}
	}


	/**
	 * If Overlay Asset Class IsMainCash = True: Applies Overlay Manager Cash Value to Asset Class Actual Allocation
	 * Else: Applies Overlay Manager Securities Value to the Overlay Asset Class Actual Allocation
	 */
	private void processAssetClassActualAllocation(ProductOverlayRunConfig runConfig, ProductOverlayManagerAllocationConfig managerAllocationConfig, ProductOverlayAssetClass overlayAssetClass) {
		if (InvestmentManagerAllocationConfigTypes.CASH_ALLOCATION == managerAllocationConfig.getConfigType()) {
			BigDecimal originalCashAllocation = managerAllocationConfig.getAmount();

			// Whether or not we are overlaying cash into different buckets, still put entire amount into Actual Allocation for Cash Asset Class
			// UNLESS it's assigned to another "cash" asset class that isn't main cash.  Otherwise that cash will get double counted
			if (!managerAllocationConfig.getAccountAssetClass().getAssetClass().isMainCash()) {
				if (managerAllocationConfig.getAccountAssetClass().getAssetClass().isCash()) {
					originalCashAllocation = BigDecimal.ZERO;
				}
			}

			if (!MathUtils.isNullOrZero(originalCashAllocation)) {
				for (InvestmentAccountAssetClass accountAssetClass : runConfig.getAssetClassMap().keySet()) {
					if (accountAssetClass.getAssetClass().isMainCash()) {
						ProductOverlayAssetClass mainCashOverlayAssetClass = runConfig.getAssetClassMap().get(accountAssetClass);
						mainCashOverlayAssetClass.setActualAllocation(MathUtils.add(originalCashAllocation, mainCashOverlayAssetClass.getActualAllocation()));
						break;
					}
				}
			}
		}

		if (!overlayAssetClass.getAccountAssetClass().getAssetClass().isMainCash() && overlayAssetClass.getAccountAssetClass().getAssetClass().isCash()) {
			if (managerAllocationConfig.getConfigType() == InvestmentManagerAllocationConfigTypes.CASH_ALLOCATION) {
				overlayAssetClass.setActualAllocation(MathUtils.add(overlayAssetClass.getActualAllocation(), managerAllocationConfig.getAmount()));
			}
		}
		else {
			if (managerAllocationConfig.getConfigType() == InvestmentManagerAllocationConfigTypes.SECURITIES_ALLOCATION) {
				overlayAssetClass.setActualAllocation(MathUtils.add(overlayAssetClass.getActualAllocation(), managerAllocationConfig.getAmount()));
				// If Securities Balance Is Applied to Replications, Add Total so it can be added to Asset Class Replication Targets
				// NOTE: May want to make this a configurable option, but without additional use cases don't know if that will be needed
				if (managerAllocationConfig.getManagerAccountAssignment().isAllocateSecuritiesBalanceToReplications()) {
					runConfig.addAdditionalExposureToAssetClass(managerAllocationConfig.getAccountAssetClass(), managerAllocationConfig.getAmount());
				}
			}
		}
	}


	/**
	 * Validates when rebalance exposure targets are set on asset classes that the sum = 100%
	 * if not throws an exception, if yes, then flags the runConfig as using rebalance exposure targets
	 */
	private void setRebalanceExposureTargetUsed(ProductOverlayRunConfig runConfig) {
		BigDecimal rebalanceExposureSum = null;

		for (InvestmentAccountAssetClass assetClass : CollectionUtils.getIterable(runConfig.getAssetClassList())) {
			// Skip Rollups or those asset classes excluded from exposure
			if (assetClass.isRollupAssetClass() || assetClass.isExcludeFromExposure()) {
				continue;
			}
			if (assetClass.getRebalanceExposureTarget() != null) {
				rebalanceExposureSum = MathUtils.add(rebalanceExposureSum, assetClass.getRebalanceExposureTarget());
			}
		}
		if (rebalanceExposureSum != null && !MathUtils.isEqual(rebalanceExposureSum, 100)) {
			throw new ValidationExceptionWithCause("Rebalance Exposure Targets currently equal [" + CoreMathUtils.formatNumberMoney(rebalanceExposureSum)
					+ "].  If rebalancing exposure targets are not to be used, please clear these values, otherwise the sum must equal 100.");
		}
		runConfig.setRebalanceExposureTargetsUsed(rebalanceExposureSum != null);
	}


	/**
	 * Creates a map of new ProductOverlayAssetClasses.  Creates and returns only for those InvestmentAccountAssetClass whose rollup flag = rollup parameter passed in
	 */
	private Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> getNewProductOverlayAssetClassMapByRun(ProductOverlayRunConfig runConfig, boolean rollup) {
		// Sub Account Relationship list (Only defined for Non Rollups, so only look it up once)
		List<InvestmentAccountRelationship> subAccountRelationshipList = (rollup ? null : getPortfolioAccountDataRetriever().getCliftonSubAccountAccountRelationshipList(runConfig.getRun().getClientInvestmentAccount().getId(), runConfig.getBalanceDate()));

		Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> map = new LinkedHashMap<>();
		// Get a list of all Active InvestmentAccountAssetClasses
		for (InvestmentAccountAssetClass accountAssetClass : CollectionUtils.getIterable(runConfig.getAssetClassList())) {
			if (rollup != accountAssetClass.isRollupAssetClass()) {
				continue;
			}
			// Asset Classes that are skipped from Exposure
			if (accountAssetClass.isExcludeFromExposure()) {
				continue;
			}
			ProductOverlayAssetClass asset = new ProductOverlayAssetClass();
			asset.setRollupAssetClass(accountAssetClass.isRollupAssetClass());
			asset.setOverlayRun(runConfig.getRun());
			asset.setAccountAssetClass(accountAssetClass);
			asset.setPrivateAssetClass(accountAssetClass.isPrivateAssetClass());
			// Set all values to 0
			asset.setActualAllocation(BigDecimal.ZERO);
			asset.setActualAllocationAdjusted(BigDecimal.ZERO);
			asset.setFundCash(BigDecimal.ZERO);
			asset.setManagerCash(BigDecimal.ZERO);
			asset.setOverlayExposure(BigDecimal.ZERO);
			asset.setRebalanceCash(BigDecimal.ZERO);
			asset.setRebalanceAttributionCash(BigDecimal.ZERO);
			asset.setRebalanceCashAdjusted(BigDecimal.ZERO);
			asset.setClientDirectedCash(BigDecimal.ZERO);
			asset.setTargetAllocation(BigDecimal.ZERO);
			asset.setTargetAllocationAdjusted(BigDecimal.ZERO);
			asset.setTransitionCash(BigDecimal.ZERO);
			asset.setPrimaryReplication(accountAssetClass.getPrimaryReplication());
			asset.setSecondaryReplication(accountAssetClass.getSecondaryReplication());
			asset.setSecondaryReplicationAmount(accountAssetClass.getSecondaryReplicationAmount());
			asset.setBenchmarkSecurity(accountAssetClass.getBenchmarkSecurity());
			asset.setDoNotAdjustContractValue(accountAssetClass.isDoNotAdjustContractValue());

			if (accountAssetClass.getBenchmarkDurationSecurity() != null) {
				// If there is a Benchmark Duration, then expect durations to be present...
				String durationFieldNameOverride = accountAssetClass.getDurationFieldNameOverride();
				BigDecimal duration = getMarketDataRetriever().getDurationFlexible(accountAssetClass.getBenchmarkDurationSecurity(), runConfig.getBalanceDate(), durationFieldNameOverride, true);
				asset.setBenchmarkDuration(duration);
				// Don't require Credit Duration unless replication calculator needs it - in that case exception will be thrown there
				asset.setBenchmarkCreditDuration(getMarketDataRetriever().getCreditDuration(accountAssetClass.getBenchmarkDurationSecurity(), runConfig.getBalanceDate(), false));
			}

			if (!CollectionUtils.isEmpty(subAccountRelationshipList)) {
				InvestmentAccountRelationship relationship = CollectionUtils.getOnlyElement(BeanUtils.filter(subAccountRelationshipList, InvestmentAccountRelationship::getAccountAssetClass, accountAssetClass));
				if (relationship != null) {
					asset.setPositionInvestmentAccount(relationship.getReferenceTwo());
				}
			}

			map.put(accountAssetClass, asset);
		}
		return map;
	}

	/////////////////////////////////////////////////////////////////////////
	/////////          Asset Class Rebalancing Methods             //////////
	/////////////////////////////////////////////////////////////////////////


	protected void processProductOverlayAssetClassRebalancing(ProductOverlayRunConfig runConfig) {
		// Set Rebalance Cash, Rebalance Cash Adjusted Values on the Asset Classes
		InvestmentAccountRebalanceHistory rebalanceHistory = getInvestmentAccountRebalanceService().getInvestmentAccountRebalanceHistoryRecent(runConfig.getClientInvestmentAccountId(), runConfig.getBalanceDate());
		// Rebal Cash and Rebal Cash Adjusted
		processRebalanceCash(runConfig, rebalanceHistory);

		// Mini Rebalance (if performed, automatically updates Rebal Cash/Rebal Cash Adjusted)
		processMiniRebalance(runConfig);
	}


	private void processRebalanceCash(ProductOverlayRunConfig runConfig, InvestmentAccountRebalanceHistory rebalanceHistory) {
		runConfig.setAccountRebalanceHistory(rebalanceHistory);

		for (Map.Entry<InvestmentAccountAssetClass, ProductOverlayAssetClass> accountAssetClassEntry : runConfig.getAssetClassMap().entrySet()) {
			InvestmentAccountAssetClass accountAssetClass = accountAssetClassEntry.getKey();
			InvestmentAccountRebalanceAssetClassHistory assetClassRebalanceHistory = (rebalanceHistory != null ? CollectionUtils.getFirstElement(BeanUtils.filter(rebalanceHistory.getAssetClassHistoryList(), InvestmentAccountRebalanceAssetClassHistory::getAccountAssetClass, accountAssetClass)) : null);
			ProductOverlayAssetClass overlayAssetClass = accountAssetClassEntry.getValue();
			overlayAssetClass.setRebalanceBenchmarkSecurity(accountAssetClass.getRebalanceBenchmarkSecurity());
			BigDecimal rebalanceCash = (assetClassRebalanceHistory != null ? assetClassRebalanceHistory.getAfterRebalanceCash() : BigDecimal.ZERO);
			if (rebalanceHistory != null && !MathUtils.isNullOrZero(rebalanceCash)) {
				overlayAssetClass.setRebalanceDate(rebalanceHistory.getRebalanceDate());
			}

			if (rebalanceHistory != null) {
				InvestmentSecurity benchmark = overlayAssetClass.getCoalesceRebalanceBenchmarkSecurity();

				String benchmarkLabel = "Asset Class [" + accountAssetClass.getAssetClass().getName() + "] " + (overlayAssetClass.getRebalanceBenchmarkSecurity() != null ? "Rebalance " : "")
						+ "Benchmark";

				// Only calculate adjusted value if there is a benchmark, rebalance cash value is set, and the rebalance date & run balance date are not equal.
				if (!accountAssetClass.isSkipRebalanceCashAdjustment() && benchmark != null && !MathUtils.isNullOrZero(rebalanceCash)
						&& DateUtils.compare(overlayAssetClass.getRebalanceDate(), overlayAssetClass.getOverlayRun().getBalanceDate(), false) != 0) {
					BigDecimal percentChange = getMarketDataRetriever().getInvestmentSecurityReturn(accountAssetClass.getAccount(), benchmark, overlayAssetClass.getRebalanceDate(),
							overlayAssetClass.getOverlayRun().getBalanceDate(), benchmarkLabel, false);
					BigDecimal currentRebalanceCash = MathUtils.add(rebalanceCash, MathUtils.getPercentageOf(percentChange, rebalanceCash));
					overlayAssetClass.setRebalanceCash(rebalanceCash);
					overlayAssetClass.setRebalanceCashAdjusted(currentRebalanceCash);
				}
				else {
					overlayAssetClass.setRebalanceCash(rebalanceCash);
					overlayAssetClass.setRebalanceCashAdjusted(rebalanceCash);
				}
			}
		}
	}


	private void processMiniRebalance(ProductOverlayRunConfig runConfig) {
		// Attempt Automatic Rebalancing - Validation Here is also done in actual rebalancing, but to be efficient - do simple checks here
		InvestmentAccountRebalance rebalanceConfig = runConfig.getAccountRebalanceConfig();

		boolean attemptAutoRebalance = true;
		if (!(rebalanceConfig == null || !rebalanceConfig.isAutomatic() || rebalanceConfig.getCalculationType() == null)) {
			InvestmentAccountRebalanceHistory history = getInvestmentAccountRebalanceService().getInvestmentAccountRebalanceHistoryRecent(runConfig.getClientInvestmentAccountId(), new Date());
			if (history != null && DateUtils.compare(history.getRebalanceDate(), runConfig.getBalanceDate(), false) >= 0) {
				attemptAutoRebalance = false;
			}
			if (attemptAutoRebalance) {
				getProductOverlayRebalanceService().processProductOverlayRebalanceAutomatic(runConfig);
				// See if we have new history available on the balance date
				InvestmentAccountRebalanceHistory currentHistory = getInvestmentAccountRebalanceService().getInvestmentAccountRebalanceHistoryRecent(runConfig.getClientInvestmentAccountId(), new Date());
				if (!CompareUtils.isEqual(history, currentHistory)) {
					// If so...reset Rebal Cash/Rebal Cash Adjusted
					processRebalanceCash(runConfig, currentHistory);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////        Product Overlay Asset Class Cleanup Methods          ///////
	////////////////////////////////////////////////////////////////////////////


	protected void cleanUpAndSaveAssetClasses(ProductOverlayRunConfig runConfig) {
		cleanUpRoundingForProductOverlayAssetClassAmounts(runConfig);
		setProductOverlayAssetClassRebalanceTriggerValues(runConfig);
		setProductOverlayAssetClassBenchmarkReturns(runConfig);
		getProductOverlayService().saveProductOverlayAssetClassListByRun(runConfig.getRun().getId(), runConfig.getCoalescePortfolioTotalValue(false), runConfig.getOverlayAssetClassList());
	}


	/**
	 * For Each Asset Class:
	 * Sets RebalanceExposureTarget/Percent
	 * Sets ActualAllocationPercent/ActualAllocationAdjustedPercent based on Actual Values and Total Portfolio Value
	 * Sets TargetAllocationPercent/TargetAllocationAdjustedPercent based on Target Values and Total Portfolio Value
	 */
	private void cleanUpRoundingForProductOverlayAssetClassAmounts(ProductOverlayRunConfig runConfig) {
		List<ProductOverlayAssetClass> list = new ArrayList<>(runConfig.getAssetClassMap().values());

		Boolean useAssetClassTotal = (Boolean) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(runConfig.getRun().getClientInvestmentAccount(),
				USE_ACTUAL_POLICY_TARGET_TOTAL_PERCENT);

		BigDecimal calculatedTotal = runConfig.getPortfolioTotalCalculatedValue();
		BigDecimal overrideTotal = runConfig.getCoalescePortfolioTotalValue(true);
		BigDecimal totalPercentage = (runConfig.getPortfolioTotalOverrideValue() == null ? (Boolean.TRUE.equals(useAssetClassTotal) ? CoreMathUtils.sumProperty(list, productOverlayAssetClass -> productOverlayAssetClass.getAccountAssetClass() == null ? null : productOverlayAssetClass.getAccountAssetClass().getAssetClassPercentage()) : MathUtils.BIG_DECIMAL_ONE_HUNDRED) : CoreMathUtils.getPercentValue(calculatedTotal, overrideTotal, true));
		BigDecimal rebalanceCalculatedTotal = runConfig.getRebalanceExposurePortfolioTotalValue();
		BigDecimal rebalanceOverrideTotal = rebalanceCalculatedTotal;
		// If Rebalance Exposure Targets are not explicitly used, make sure to use the overridden TPV value if specified
		if (!runConfig.isRebalanceExposureTargetsUsed()) {
			rebalanceOverrideTotal = overrideTotal;
		}

		// Set the Bean to Apply Differences to as the one with the largest target value that is NOT TFA
		List<ProductOverlayAssetClass> sortedList = BeanUtils.sortWithFunction(list, ProductOverlayAssetClass::getTargetAllocation, false);
		ProductOverlayAssetClass dif = null;
		ProductOverlayAssetClass rebalDif = null;
		for (ProductOverlayAssetClass ac : CollectionUtils.getIterable(sortedList)) {

			// If Rebalancing Exposure Targets are not explicitly used, use the Adjusted Target Values
			if (!runConfig.isRebalanceExposureTargetsUsed()) {
				ac.setRebalanceExposureTarget(ac.getTargetAllocationAdjusted());
				// ac.setRebalanceExposureTargetPercent(ac.getTargetAllocationAdjustedPercent());
			}

			// Set and Fix Percent Values (Used Override Total For Percentage Calculations)
			ac.setActualAllocationPercent(MathUtils.round(getPercentValueForProductOverlayAssetClass(ac, "Physical Exposure %", ac.getActualAllocation(), overrideTotal), 2));
			ac.setActualAllocationAdjustedPercent(MathUtils.round(getPercentValueForProductOverlayAssetClass(ac, "Physical Exposure Adjusted %", ac.getActualAllocationAdjusted(), overrideTotal), 2));
			ac.setTargetAllocationPercent(MathUtils.round(getPercentValueForProductOverlayAssetClass(ac, "Target %", ac.getTargetAllocation(), overrideTotal), 2));
			ac.setTargetAllocationAdjustedPercent(MathUtils.round(getPercentValueForProductOverlayAssetClass(ac, "Adjusted Target %", ac.getTargetAllocationAdjusted(), overrideTotal), 2));
			ac.setRebalanceExposureTargetPercent(MathUtils.round(
					getPercentValueForProductOverlayAssetClass(ac, "Rebalance Exposure Target %", ac.getRebalanceExposureTarget(), rebalanceOverrideTotal), 2));

			if (dif == null && !ac.isTargetFollowsActual() && !MathUtils.isNullOrZero(ac.getTargetAllocation())) {
				dif = ac;
			}
			if (rebalDif == null && !MathUtils.isNullOrZero(ac.getRebalanceExposureTarget())) {
				rebalDif = ac;
			}
		}
		if (dif != null) {
			if (Boolean.TRUE.equals(useAssetClassTotal)) {
				calculatedTotal = MathUtils.getPercentageOf(totalPercentage, calculatedTotal, true);
			}
			// Use Actual Calculated Total for Rounding Fixes
			BigDecimal calculatedDifference = CoreMathUtils.getSumPropertyDifference(list, ProductOverlayAssetClass::getTargetAllocation, calculatedTotal);
			dif.setTargetAllocation(MathUtils.add(dif.getTargetAllocation(), calculatedDifference));

			calculatedDifference = CoreMathUtils.getSumPropertyDifference(list, ProductOverlayAssetClass::getTargetAllocationAdjusted, calculatedTotal);
			dif.setTargetAllocationAdjusted(MathUtils.add(dif.getTargetAllocationAdjusted(), calculatedDifference));

			calculatedDifference = CoreMathUtils.getSumPropertyDifference(list, ProductOverlayAssetClass::getTargetAllocationPercent, totalPercentage);
			dif.setTargetAllocationPercent(MathUtils.add(dif.getTargetAllocationPercent(), calculatedDifference));

			calculatedDifference = CoreMathUtils.getSumPropertyDifference(list, ProductOverlayAssetClass::getTargetAllocationAdjustedPercent, totalPercentage);
			dif.setTargetAllocationAdjustedPercent(MathUtils.add(dif.getTargetAllocationAdjustedPercent(), calculatedDifference));
		}
		if (rebalDif != null) {
			BigDecimal calculatedDifference = CoreMathUtils.getSumPropertyDifference(list, ProductOverlayAssetClass::getRebalanceExposureTarget, rebalanceCalculatedTotal);
			rebalDif.setRebalanceExposureTarget(MathUtils.add(rebalDif.getRebalanceExposureTarget(), calculatedDifference));

			calculatedDifference = CoreMathUtils.getSumPropertyDifference(list, ProductOverlayAssetClass::getRebalanceExposureTargetPercent, (!runConfig.isRebalanceExposureTargetsUsed() ? totalPercentage : MathUtils.BIG_DECIMAL_ONE_HUNDRED));
			rebalDif.setRebalanceExposureTargetPercent(MathUtils.add(rebalDif.getRebalanceExposureTargetPercent(), calculatedDifference));
		}

		CoreMathUtils.applySumPropertyDifference(list, ProductOverlayAssetClass::getActualAllocationPercent, ProductOverlayAssetClass::setActualAllocationPercent, totalPercentage, true);
		CoreMathUtils.applySumPropertyDifference(list, ProductOverlayAssetClass::getActualAllocationAdjustedPercent, ProductOverlayAssetClass::setActualAllocationAdjustedPercent, totalPercentage, true);

		// Fix Attribution Rebalancing Rounding (Affected Cash Buckets and Expected Totals, Ensure Rebalance Attribution Cash = 0)
		if (runConfig.getCashBucketExpectedTotalMap().containsKey(ManagerCashTypes.MANAGER)) {
			CoreMathUtils.applySumPropertyDifference(list, ProductOverlayAssetClass::getManagerCash, ProductOverlayAssetClass::setManagerCash, runConfig.getCashBucketExpectedTotalMap().get(ManagerCashTypes.MANAGER), true);
		}
		if (runConfig.getCashBucketExpectedTotalMap().containsKey(ManagerCashTypes.TRANSITION)) {
			CoreMathUtils.applySumPropertyDifference(list, ProductOverlayAssetClass::getTransitionCash, ProductOverlayAssetClass::setTransitionCash, runConfig.getCashBucketExpectedTotalMap().get(ManagerCashTypes.TRANSITION), true);
		}
		if (runConfig.getCashBucketExpectedTotalMap().containsKey(ManagerCashTypes.FUND)) {
			CoreMathUtils.applySumPropertyDifference(list, ProductOverlayAssetClass::getFundCash, ProductOverlayAssetClass::setFundCash, runConfig.getCashBucketExpectedTotalMap().get(ManagerCashTypes.FUND), true);
		}
		if (runConfig.getCashBucketExpectedTotalMap().containsKey(ManagerCashTypes.REBALANCE_ATTRIBUTION)) {
			CoreMathUtils.applySumPropertyDifference(list, ProductOverlayAssetClass::getRebalanceAttributionCash, ProductOverlayAssetClass::setRebalanceAttributionCash, runConfig.getCashBucketExpectedTotalMap().get(ManagerCashTypes.REBALANCE_ATTRIBUTION), true);
		}
		processProductOverlayAssetClassRollups(runConfig);
	}


	private BigDecimal getPercentValueForProductOverlayAssetClass(ProductOverlayAssetClass oac, String fieldName, BigDecimal value, BigDecimal total) {
		BigDecimal result;
		try {
			result = CoreMathUtils.getPercentValue(value, total, true, true);
		}
		catch (ValidationException e) {
			throw new ValidationExceptionWithCause("InvestmentAccountAssetClass", oac.getAccountAssetClass().getId(), "Error calculating [" + fieldName + "] for asset class [" + oac.getLabel()
					+ "]: " + e.getMessage());
		}
		return result;
	}


	private void processProductOverlayAssetClassRollups(ProductOverlayRunConfig runConfig) {
		// If no rollups - do nothing
		if (CollectionUtils.isEmpty(runConfig.getAssetClassRollupMap().keySet())) {
			return;
		}

		// Go through each non rollup first and if there's a parent, find it in rollup map and set it and set values
		for (Map.Entry<InvestmentAccountAssetClass, ProductOverlayAssetClass> childACEntry : runConfig.getAssetClassMap().entrySet()) {
			InvestmentAccountAssetClass childAC = childACEntry.getKey();
			if (childAC.getParent() != null) {
				ProductOverlayAssetClass child = childACEntry.getValue();
				ProductOverlayAssetClass parent = runConfig.getAssetClassRollupMap().get(childAC.getParent());
				runConfig.addChildValuesToParentOverlayAssetClass(parent, child);
				runConfig.getAssetClassMap().put(childAC, child);
				runConfig.getAssetClassRollupMap().put(childAC.getParent(), parent);
			}
		}

		// Now go through each rollup, and if there's a parent (rollup of a rollup) find it in rollup map and set it and set values
		for (Map.Entry<InvestmentAccountAssetClass, ProductOverlayAssetClass> childACEntry : runConfig.getAssetClassRollupMap().entrySet()) {
			InvestmentAccountAssetClass childAC = childACEntry.getKey();
			if (childAC.getParent() != null) {
				ProductOverlayAssetClass child = childACEntry.getValue();
				ProductOverlayAssetClass parent = runConfig.getAssetClassRollupMap().get(childAC.getParent());
				runConfig.addChildValuesToParentOverlayAssetClass(parent, child);
				runConfig.getAssetClassRollupMap().put(childAC, child);
				runConfig.getAssetClassRollupMap().put(childAC.getParent(), parent);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////                Getter and Setter Methods                   ////////
	////////////////////////////////////////////////////////////////////////////


	public ProductOverlayRebalanceService getProductOverlayRebalanceService() {
		return this.productOverlayRebalanceService;
	}


	public void setProductOverlayRebalanceService(ProductOverlayRebalanceService productOverlayRebalanceService) {
		this.productOverlayRebalanceService = productOverlayRebalanceService;
	}


	public InvestmentAccountRebalanceService getInvestmentAccountRebalanceService() {
		return this.investmentAccountRebalanceService;
	}


	public void setInvestmentAccountRebalanceService(InvestmentAccountRebalanceService investmentAccountRebalanceService) {
		this.investmentAccountRebalanceService = investmentAccountRebalanceService;
	}


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}


	public InvestmentAccountTargetExposureHandler<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> getInvestmentAccountTargetExposureHandler() {
		return this.investmentAccountTargetExposureHandler;
	}


	public void setInvestmentAccountTargetExposureHandler(InvestmentAccountTargetExposureHandler<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> investmentAccountTargetExposureHandler) {
		this.investmentAccountTargetExposureHandler = investmentAccountTargetExposureHandler;
	}
}
