package com.clifton.product.overlay.rebalance.calculators;


import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;


/**
 * The <code>ProductOverlayRebalanceCalculatorLocator</code> class finds and returns ProductOverlayRebalanceCalculator
 * that corresponds to a RebalanceCalculationType
 *
 * @author manderson
 */
public interface ProductOverlayRebalanceCalculatorLocator {

	/**
	 * Returns ProductOverlayRebalanceCalculator for the specified rebalanceCalculationType
	 *
	 * @param rebalanceCalculationType
	 */
	public ProductOverlayRebalanceCalculator locate(RebalanceCalculationTypes rebalanceCalculationType);
}
