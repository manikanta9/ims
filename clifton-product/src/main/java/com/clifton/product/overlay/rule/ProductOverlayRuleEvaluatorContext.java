package com.clifton.product.overlay.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.product.overlay.ProductOverlayManagerAccountRebalance;
import com.clifton.product.overlay.ProductOverlayService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Utility class providing cached objects for product overlay rules.
 *
 * @author stevenf on 9/8/2015.
 */
public class ProductOverlayRuleEvaluatorContext extends PortfolioRunRuleEvaluatorContext<ProductOverlayAssetClass, ProductOverlayAssetClassReplication> {

	private static final String ASSET_CLASS_BENCHMARK_MAP = "AssetClassBenchmarkMap";


	private ProductOverlayService productOverlayService;


	@SuppressWarnings("unchecked")
	@Override
	public List<ProductOverlayAssetClass> getRunSummaryList(PortfolioRun run, boolean previousRun, Boolean rollupAssetClass) {
		String key = (previousRun ? PortfolioRunRuleEvaluatorContext.PREVIOUS_OVERLAY_ASSET_CLASS_LIST : PortfolioRunRuleEvaluatorContext.OVERLAY_ASSET_CLASS_LIST);
		List<ProductOverlayAssetClass> list = (List<ProductOverlayAssetClass>) getContext().getBean(key);
		if (list == null) {
			if (previousRun) {
				PortfolioRun previous = getPreviousRun(run);
				if (previous != null) {
					list = getProductOverlayService().getProductOverlayAssetClassListByRun(previous.getId());
				}
			}
			else {
				list = getProductOverlayService().getProductOverlayAssetClassListByRun(run.getId());
			}
			if (list == null) {
				list = new ArrayList<>();
			}
			getContext().setBean(key, list);
		}
		if (rollupAssetClass != null) {
			return BeanUtils.filter(list, ProductOverlayAssetClass::isRollupAssetClass, rollupAssetClass);
		}
		return list;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<ProductOverlayAssetClassReplication> getReplicationList(PortfolioRun run, boolean previousRun) {
		String key = (previousRun ? PortfolioRunRuleEvaluatorContext.PREVIOUS_OVERLAY_ASSET_CLASS_REPLICATION_LIST : PortfolioRunRuleEvaluatorContext.OVERLAY_ASSET_CLASS_REPLICATION_LIST);
		List<ProductOverlayAssetClassReplication> replicationList = (List<ProductOverlayAssetClassReplication>) getContext().getBean(key);
		if (replicationList == null) {
			if (previousRun) {
				PortfolioRun previous = getPreviousRun(run);
				if (previous != null) {
					replicationList = getProductOverlayService().getProductOverlayAssetClassReplicationListByRun(previous.getId());
				}
			}
			else {
				replicationList = getProductOverlayService().getProductOverlayAssetClassReplicationListByRun(run.getId());
			}
			if (replicationList == null) {
				replicationList = new ArrayList<>();
			}
			getContext().setBean(key, replicationList);
		}
		return replicationList;
	}


	@SuppressWarnings("unchecked")
	public List<ProductOverlayManagerAccount> getProductOverlayManagerList(PortfolioRun run, boolean previousRun, Boolean rollupAssetClass) {
		String key = (previousRun ? PortfolioRunRuleEvaluatorContext.PREVIOUS_OVERLAY_MANAGER_LIST : PortfolioRunRuleEvaluatorContext.OVERLAY_MANAGER_LIST);
		List<ProductOverlayManagerAccount> list = (List<ProductOverlayManagerAccount>) getContext().getBean(key);
		if (list == null) {
			if (previousRun) {
				PortfolioRun previous = getPreviousRun(run);
				if (previous != null) {
					list = getProductOverlayService().getProductOverlayManagerAccountListByRun(previous.getId());
				}
			}
			else {
				list = getProductOverlayService().getProductOverlayManagerAccountListByRun(run.getId());
			}
			if (list == null) {
				list = new ArrayList<>();
			}
			getContext().setBean(key, list);
		}
		if (rollupAssetClass != null) {
			return BeanUtils.filter(list, ProductOverlayManagerAccount::isRollupAssetClass, rollupAssetClass);
		}
		return list;
	}


	@Override
	public List<ProductOverlayManagerAccountRebalance> getManagerAccountRebalanceList(PortfolioRun run) {
		return getProductOverlayService().getProductOverlayManagerAccountRebalanceListByRun(run.getId());
	}


	@SuppressWarnings("unchecked")
	public Map<Integer, BigDecimal> getAccountAssetClassBenchmarkReturnMap(PortfolioRun run) {
		Map<Integer, BigDecimal> assetClassBenchmarkReturnMap = (Map<Integer, BigDecimal>) getContext().getBean(ASSET_CLASS_BENCHMARK_MAP);
		if (assetClassBenchmarkReturnMap == null) {
			assetClassBenchmarkReturnMap = new HashMap<>();
			for (ProductOverlayAssetClass summary : CollectionUtils.getIterable(getRunSummaryList(run, false, null))) {
				assetClassBenchmarkReturnMap.put(summary.getAccountAssetClass().getId(), summary.getBenchmarkOneDayReturn());
			}
			getContext().setBean(ASSET_CLASS_BENCHMARK_MAP, assetClassBenchmarkReturnMap);
		}
		return assetClassBenchmarkReturnMap;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}
}
