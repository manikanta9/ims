package com.clifton.product.overlay.trade.group.dynamic.option.account;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.trade.Trade;
import com.clifton.trade.group.dynamic.TradeGroupDynamicOptionStrangleDetail;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.function.Consumer;
import java.util.function.Function;


/**
 * ProductOverlayRunTradeGroupDynamicOptionAccountRoll displays portfolio information for a specific account/run
 * and is useful to display the impact of expiring options on the portfolio and trade entry of new options.
 * <p>
 * The runs are evaluated and the basic calculated values are stored here for easy use in the UI so users can play around with new quantities and easily see the impact on the portfolio
 *
 * @author manderson
 */
public class ProductOverlayRunTradeGroupDynamicOptionAccountRoll extends TradeGroupDynamicOptionStrangleDetail {

	private ProductOverlayRunOptionTradeReplicationHolder tradeReplicationHolder;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void setClientAccount(InvestmentAccount clientAccount) {
		super.setClientAccount(clientAccount);
		setTradeReplicationHolderValue(tradeAccount -> tradeAccount.setClientAccount(clientAccount));
	}


	public BigDecimal getPutTradeQuantity() {
		final BigDecimal quantity;
		if (getPutTrade() != null) {
			quantity = getTradeQuantity(getPutTrade());
		}
		else {
			quantity = getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutTradeQuantity);
		}
		return quantity;
	}


	public void setPutTradeQuantity(BigDecimal putTradeQuantity) {
		setTradeReplicationHolderValue(tradeAccount -> tradeAccount.setPutTradeQuantity(putTradeQuantity));
		if (getPutTrade() == null) {
			setPutTrade(new Trade());
		}
		setTradeQuantity(getPutTrade(), putTradeQuantity);
	}


	public BigDecimal getCallTradeQuantity() {
		final BigDecimal quantity;
		if (getCallTrade() != null) {
			quantity = getTradeQuantity(getCallTrade());
		}
		else {
			quantity = getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallTradeQuantity);
		}
		return quantity;
	}


	public void setCallTradeQuantity(BigDecimal callTradeQuantity) {
		setTradeReplicationHolderValue(tradeAccount -> tradeAccount.setCallTradeQuantity(callTradeQuantity));
		if (getCallTrade() == null) {
			setCallTrade(new Trade());
		}
		setTradeQuantity(getCallTrade(), callTradeQuantity);
	}


	private BigDecimal getTradeQuantity(Trade trade) {
		return (trade != null) ? trade.getQuantityIntended() : null;
	}


	/**
	 * Note: Value should already be positive - Trade Direction is based on longPositions filter on the {@see ProductOverlayRunTradeGroupDynamicOptionRoll} bean
	 */
	private void setTradeQuantity(Trade trade, BigDecimal quantity) {
		if (trade != null) {
			trade.setQuantityIntended(MathUtils.abs(quantity));
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ProductOverlayRunOptionTradeReplicationHolder getTradeReplicationHolder() {
		return this.tradeReplicationHolder;
	}


	public void setTradeReplicationHolder(ProductOverlayRunOptionTradeReplicationHolder productOverlayRunOptionTradeReplicationHolder) {
		this.tradeReplicationHolder = productOverlayRunOptionTradeReplicationHolder;
	}


	// Convenience method for getting values from the underlying replication holder
	private <R> R getTradeReplicationHolderValue(Function<ProductOverlayRunOptionTradeReplicationHolder, R> valueGetter) {
		return getTradeReplicationHolder() != null ? valueGetter.apply(getTradeReplicationHolder()) : null;
	}


	// Convenience method for setting values on the underlying replication holder
	private void setTradeReplicationHolderValue(Consumer<ProductOverlayRunOptionTradeReplicationHolder> valueSetter) {
		if (getTradeReplicationHolder() == null) {
			setTradeReplicationHolder(new ProductOverlayRunOptionTradeReplicationHolder());
		}
		valueSetter.accept(getTradeReplicationHolder());
	}

	////////////////////////////////////////////////////////////////////////////////
	// Convenience methods to interact with the replication holder directly to
	// avoid having to update the UI to reference the member holder object for all
	// attributes of the Grid.
	////////////////////////////////////////////////////////////////////////////////


	public String getExpiringPutTooltipString() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringPutReplicationHolder().getTooltipString());
	}


	public BigDecimal getExpiringPutTradedInstrumentQuantity() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringPutTradedInstrumentReplicationHolder().getQuantity());
	}


	public BigDecimal getExpiringPutExposure() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringPutReplicationHolder().getExposure());
	}


	public BigDecimal getExpiringPutExposureActual() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringPutReplicationHolder().getExposureActual());
	}


	public BigDecimal getExpiringPutExposurePending() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringPutReplicationHolder().getExposurePending());
	}


	public BigDecimal getExpiringPutExposureCurrent() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringPutReplicationHolder().getExposureCurrent());
	}


	public BigDecimal getExpiringPutMarketValueActual() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringPutReplicationHolder().getMarketValueActual());
	}


	public BigDecimal getExpiringPutMarketValuePending() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringPutReplicationHolder().getMarketValuePending());
	}


	public BigDecimal getExpiringPutMarketValueCurrent() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringPutReplicationHolder().getMarketValueCurrent());
	}


	////////////////////////////////////////////////////////////////////////////////


	public String getExpiringCallTooltipString() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringCallReplicationHolder().getTooltipString());
	}


	public BigDecimal getExpiringCallTradedInstrumentQuantity() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringCallTradedInstrumentReplicationHolder().getQuantity());
	}


	public BigDecimal getExpiringCallExposure() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringCallReplicationHolder().getExposure());
	}


	public BigDecimal getExpiringCallExposureActual() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringCallReplicationHolder().getExposureActual());
	}


	public BigDecimal getExpiringCallExposurePending() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringCallReplicationHolder().getExposurePending());
	}


	public BigDecimal getExpiringCallExposureCurrent() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringCallReplicationHolder().getExposureCurrent());
	}


	public BigDecimal getExpiringCallMarketValueActual() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringCallReplicationHolder().getMarketValueActual());
	}


	public BigDecimal getExpiringCallMarketValuePending() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringCallReplicationHolder().getMarketValuePending());
	}


	public BigDecimal getExpiringCallMarketValueCurrent() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getExpiringCallReplicationHolder().getMarketValueCurrent());
	}
	////////////////////////////////////////////////////////////////////////////////


	public String getPutOptionsTooltipString() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutOptionsTooltipString);
	}


	public BigDecimal getPutExposure() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getPutReplicationHolder().getExposure());
	}


	public BigDecimal getPutExposureActual() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getPutReplicationHolder().getExposureActual());
	}


	public BigDecimal getPutExposurePending() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getPutReplicationHolder().getExposurePending());
	}


	public BigDecimal getPutExposureCurrent() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getPutReplicationHolder().getExposureCurrent());
	}

	////////////////////////////////////////////////////////////////////////////////


	public String getCallOptionsTooltipString() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallOptionsTooltipString);
	}


	public BigDecimal getCallExposure() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getCallReplicationHolder().getExposure());
	}


	public BigDecimal getCallExposureActual() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getCallReplicationHolder().getExposureActual());
	}


	public BigDecimal getCallExposurePending() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getCallReplicationHolder().getExposurePending());
	}


	public BigDecimal getCallExposureCurrent() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getCallReplicationHolder().getExposureCurrent());
	}

	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getPutMatchingExposure() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getPutMatchingReplicationHolder().getExposure());
	}


	public BigDecimal getPutMatchingExposureActual() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getPutMatchingReplicationHolder().getExposureActual());
	}


	public BigDecimal getPutMatchingExposurePending() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getPutMatchingReplicationHolder().getExposurePending());
	}


	public BigDecimal getPutMatchingExposureCurrent() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getPutMatchingReplicationHolder().getExposureCurrent());
	}


	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getCallMatchingExposure() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getCallMatchingReplicationHolder().getExposure());
	}


	public BigDecimal getCallMatchingExposureActual() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getCallMatchingReplicationHolder().getExposureActual());
	}


	public BigDecimal getCallMatchingExposurePending() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getCallMatchingReplicationHolder().getExposurePending());
	}


	public BigDecimal getCallMatchingExposureCurrent() {
		return getTradeReplicationHolderValue(replicationHolder -> replicationHolder.getCallMatchingReplicationHolder().getExposureCurrent());
	}


	////////////////////////////////////////////////////////////////////////////////


	public String getPutMatchingReplicationLabel() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutMatchingReplicationLabel);
	}


	public String getPutMatchingAdditionalExposureLabel() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutMatchingAdditionalExposureLabel);
	}


	public BigDecimal getPutMatchingAdditionalExposure() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutMatchingAdditionalExposure);
	}


	public String getCallMatchingReplicationLabel() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallMatchingReplicationLabel);
	}


	public String getCallMatchingAdditionalExposureLabel() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallMatchingAdditionalExposureLabel);
	}


	public BigDecimal getCallMatchingAdditionalExposure() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallMatchingAdditionalExposure);
	}


	public BigDecimal getPutContractValue() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutContractValue);
	}


	public BigDecimal getCallContractValue() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallContractValue);
	}


	public BigDecimal getPutPrice() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutPrice);
	}


	public BigDecimal getPutPriceMultiplier() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutPriceMultiplier);
	}


	public BigDecimal getCallPrice() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallPrice);
	}


	public BigDecimal getCallPriceMultiplier() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallPriceMultiplier);
	}


	public BigDecimal getPutTrancheTradeAverage() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutTrancheTradeAverage);
	}


	public BigDecimal getCallTrancheTradeAverage() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallTrancheTradeAverage);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getPortfolioRunId() {
		return getTradeReplicationHolderValue(account -> account.getPortfolioRun() != null ? account.getPortfolioRun().getId() : null);
	}


	public void setPortfolioRunId(Integer portfolioRunId) {
		setTradeReplicationHolderValue(replicationHolder -> {
			replicationHolder.setPortfolioRun(new PortfolioRun());
			replicationHolder.getPortfolioRun().setId(portfolioRunId);
		});
	}


	public String getWarningMessage() {
		return getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getWarningMessage);
	}


	public void setWarningMessage(String warningMessage) {
		setTradeReplicationHolderValue(replicationHolder -> replicationHolder.setWarningMessage(warningMessage));
	}


	public boolean isDoNotRemoveExpiringOptionsFromExcessCalculation() {
		Boolean holderValue = getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getDoNotRemoveExpiringOptionsFromExcessCalculation);
		return holderValue == null || holderValue;
	}


	public void setDoNotRemoveExpiringOptionsFromExcessCalculation(boolean doNotRemoveExpiringOptionsFromExcessCalculation) {
		setTradeReplicationHolderValue(replicationHolder -> replicationHolder.setDoNotRemoveExpiringOptionsFromExcessCalculation(doNotRemoveExpiringOptionsFromExcessCalculation));
	}


	public boolean isLongPositions() {
		Boolean holderValue = getTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getLongPositions);
		return holderValue != null && holderValue;
	}


	public void setLongPositions(boolean longPositions) {
		setTradeReplicationHolderValue(replicationHolder -> replicationHolder.setLongPositions(longPositions));
	}


	public BigDecimal getCashBalance() {
		return getTradeReplicationHolderValue(replicationHolder -> (replicationHolder.getPortfolioRun() != null) ? replicationHolder.getCashBalance() : null);
	}


	public BigDecimal getPortfolioTotalValue() {
		return getTradeReplicationHolderValue(replicationHolder -> (replicationHolder.getPortfolioRun() != null) ? replicationHolder.getPortfolioRun().getPortfolioTotalValue() : null);
	}
}
