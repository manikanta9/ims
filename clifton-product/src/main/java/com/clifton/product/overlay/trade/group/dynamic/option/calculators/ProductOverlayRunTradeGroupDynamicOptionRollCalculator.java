package com.clifton.product.overlay.trade.group.dynamic.option.calculators;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.trade.group.PortfolioRunTradeGroupHandler;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.trade.group.dynamic.option.ProductOverlayRunTradeGroupDynamicOptionRoll;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionTradeReplicationHolder;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunTradeGroupDynamicOptionAccountRoll;
import com.clifton.product.overlay.trade.group.dynamic.option.account.calculators.ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.group.dynamic.calculators.BaseTradeGroupDynamicOptionStrangleCalculator;
import com.clifton.workflow.definition.WorkflowState;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * ProductOverlayRunTradeGroupDynamicOptionRollCalculator is used for Defensive Equity accounts to enter trades for new Call and Put options while
 * reviewing all accounts at once and the impact of the new trade on the entire Portfolio.
 *
 * @author manderson
 */
public class ProductOverlayRunTradeGroupDynamicOptionRollCalculator extends BaseTradeGroupDynamicOptionStrangleCalculator<ProductOverlayRunTradeGroupDynamicOptionRoll, ProductOverlayRunTradeGroupDynamicOptionAccountRoll> {

	public static final String ACCOUNT_CALCULATOR_BEAN_OVERRIDE_COLUMN_NAME = "Trade Group Option Roll Account Calculator";

	////////////////////////////////////////////////////////////////////////////////

	private AccountingPositionService accountingPositionService;

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentAccountGroupService investmentAccountGroupService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;
	private PortfolioRunTradeGroupHandler<ProductOverlayAssetClassReplication> portfolioRunTradeGroupHandler;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Most accounts use the same default calculation of Calls = Equities and Puts = Cash and Cash Equivalents
	 * so when an account override is not defined (via Custom Column on the account), this calculator is used
	 */
	private Integer defaultAccountCalculatorBeanId;

	////////////////////////////////////////////////////////////////////////////////
	/////////////         TradeGroupDynamicCalculator Methods         //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public ProductOverlayRunTradeGroupDynamicOptionRoll populateTradeGroupDynamicEntry(ProductOverlayRunTradeGroupDynamicOptionRoll rollCommand) {
		Date balanceDate = rollCommand.getBalanceDate();
		if (balanceDate == null) {
			rollCommand.setBalanceDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -1));
		}
		// Always calculate tranche trade average for Option Rolls. Unable to pass from the UI in all cases because
		// on initial load or refresh, TradeGroup is used rather than the virtual TradeGroupDynamic necessary.
		rollCommand.setCalculateTrancheTradeAverage(true);

		Map<Integer, ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator> calculatorMap = new HashMap<>();
		if (rollCommand.isNewBean()) {
			Map<InvestmentAccount, List<ProductOverlayAssetClassReplication>> clientAccountRunDetailListMap = getPortfolioRunTradeGroupHandler().getPortfolioRunAccountTradeGroupDetailMap(rollCommand);

			List<ProductOverlayRunTradeGroupDynamicOptionAccountRoll> groupDetailList = new ArrayList<>();
			for (Map.Entry<InvestmentAccount, List<ProductOverlayAssetClassReplication>> clientAssetClassReplicationEntry : CollectionUtils.getIterable(clientAccountRunDetailListMap.entrySet())) {
				InvestmentAccount clientAccount = clientAssetClassReplicationEntry.getKey();
				ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator accountRollCalculator = getProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator(clientAccount, calculatorMap);
				ProductOverlayRunTradeGroupDynamicOptionAccountRoll accountRoll = new ProductOverlayRunTradeGroupDynamicOptionAccountRoll();
				accountRoll.setClientAccount(clientAccount);
				accountRoll.setTradeReplicationHolder(accountRollCalculator.getProductOverlayRunOptionTradeReplicationHolder(rollCommand.getProductOverlayRunOptionTradeCommand(), clientAccount, clientAssetClassReplicationEntry.getValue()));
				// Only set if new entry - otherwise will just pull holding accounts when applying trades
				if (rollCommand.isNewBean() && accountRoll.getHoldingAccount() == null) {
					populateHoldingAccountForAccountRoll(rollCommand, accountRoll);
				}
				groupDetailList.add(accountRoll);
			}
			rollCommand.setDetailList(groupDetailList);
		}
		else {
			populateTradeGroupDynamicDetailListWithTrades(rollCommand);
			Integer[] clientAccountIds = BeanUtils.getPropertyValues(rollCommand.getDetailList(), detail -> detail.getClientAccount().getId(), Integer.class);
			Map<InvestmentAccount, List<ProductOverlayAssetClassReplication>> clientAccountRunDetailListMap = getPortfolioRunTradeGroupHandler().getPortfolioRunAccountTradeGroupDetailMap(rollCommand, clientAccountIds);

			for (ProductOverlayRunTradeGroupDynamicOptionAccountRoll accountRoll : CollectionUtils.getIterable(rollCommand.getDetailList())) {
				ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator accountRollCalculator = getProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator(accountRoll.getClientAccount(), calculatorMap);
				if (Objects.isNull(accountRoll.getTradeReplicationHolder())) {
					accountRoll.setTradeReplicationHolder(new ProductOverlayRunOptionTradeReplicationHolder());
				}
				accountRollCalculator.populateProductOverlayRunOptionTradeReplicationHolder(rollCommand.getProductOverlayRunOptionTradeCommand(), accountRoll.getTradeReplicationHolder(), clientAccountRunDetailListMap.get(accountRoll.getClientAccount()));
			}
		}

		return rollCommand;
	}

	////////////////////////////////////////////////////////////////////////////////
	////      Base Trade Group Dynamic Option Strangle Calculator Methods       ////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected ProductOverlayRunTradeGroupDynamicOptionRoll createTradeGroupDynamicOptionStrangle() {
		return new ProductOverlayRunTradeGroupDynamicOptionRoll();
	}


	@Override
	protected void populateTradeGroupDynamicDefaults(ProductOverlayRunTradeGroupDynamicOptionRoll rollCommand) {
		if (rollCommand.getClientAccountGroup() != null && rollCommand.getClientAccountGroup().getId() != null) {
			rollCommand.setClientAccountGroup(getInvestmentAccountGroupService().getInvestmentAccountGroup(rollCommand.getClientAccountGroup().getId()));
		}
		// Could Use Put Or Call - Both would refer to the same instrument, it's separate on the bean so we can evaluate
		// on entry existing expiring positions without new securities actually selected
		if (rollCommand.getPutSecurity() != null) {
			rollCommand.setOptionInstrument(rollCommand.getPutSecurity().getInstrument());
		}
		else {
			rollCommand.setOptionInstrument(rollCommand.getCallSecurity().getInstrument());
		}

		// Unable to pass values from the UI when loading an existing TradeGroupDynamic because the group is loaded
		// as a TradeGroup. Thus, some values need to be defaulted.
		if (rollCommand.getCalculateCashBalance() == null) {
			rollCommand.setCalculateCashBalance(Boolean.TRUE);
		}
		WorkflowState rollState = rollCommand.getWorkflowState();
		if (rollCommand.getCalculateCashTradeImpact() == null && (rollState != null && rollState.getStatus() != null && !TradeService.TRADES_CLOSED_STATUS_NAME.equals(rollState.getStatus().getName()))) {
			rollCommand.setCalculateCashTradeImpact(Boolean.TRUE);
		}
	}


	@Override
	protected ProductOverlayRunTradeGroupDynamicOptionAccountRoll createTradeGroupDynamicOptionStrangleDetail() {
		return new ProductOverlayRunTradeGroupDynamicOptionAccountRoll();
	}


	@Override
	protected void updateTradeGroupDynamicTradeSecurity(ProductOverlayRunTradeGroupDynamicOptionRoll rollCommand, Trade trade) {
		// Update the trade's security to the correct put or call Option Roll security if its current security is out of sync
		if (!isOptionRollTradeSecurityValid(rollCommand, trade)) {
			trade.setInvestmentSecurity(trade.getInvestmentSecurity().isPutOption() ? rollCommand.getPutSecurity() : rollCommand.getCallSecurity());
			// update the settlement date - if security was updated to be OTC, the settlement date may have changed
			trade.setSettlementDate(getInvestmentCalculator().calculateSettlementDate(trade.getInvestmentSecurity(), null, trade.getTradeDate()));
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////            Option Roll Calculator Methods            //////////////
	////////////////////////////////////////////////////////////////////////////////


	private boolean isOptionRollTradeSecurityValid(ProductOverlayRunTradeGroupDynamicOptionRoll rollCommand, Trade trade) {
		return (trade.getInvestmentSecurity() == rollCommand.getPutSecurity()) || (trade.getInvestmentSecurity() == rollCommand.getCallSecurity());
	}


	private ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator getProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator(InvestmentAccount clientAccount, Map<Integer, ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator> calculatorMap) {
		Integer beanId = (Integer) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(clientAccount, ACCOUNT_CALCULATOR_BEAN_OVERRIDE_COLUMN_NAME);
		if (beanId == null) {
			ValidationUtils.assertNotNull(getDefaultAccountCalculatorBeanId(), "Default Account Calculator is Required.");
			beanId = getDefaultAccountCalculatorBeanId();
		}
		if (calculatorMap.containsKey(beanId)) {
			return calculatorMap.get(beanId);
		}

		SystemBean calculatorBean = getSystemBeanService().getSystemBean(beanId);
		ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator calculator = (ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator) getSystemBeanService().getBeanInstance(calculatorBean);
		calculatorMap.put(beanId, calculator);
		return calculator;
	}


	private void populateHoldingAccountForAccountRoll(ProductOverlayRunTradeGroupDynamicOptionRoll rollCommand, ProductOverlayRunTradeGroupDynamicOptionAccountRoll accountRoll) {
		// If Only 1, Set It
		List<InvestmentAccount> list = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedListForPurpose(accountRoll.getClientAccount().getId(), null, getTradeType().getHoldingAccountPurpose().getName(), rollCommand.getTradeDate());
		if (CollectionUtils.getSize(list) == 1) {
			accountRoll.setHoldingAccount(list.get(0));
		}

		// If there are multiple, and an instrument was selected, see if there is only one of the holding accounts that actually holds positions in that instrument
		else if (CollectionUtils.getSize(list) > 1 && rollCommand.getOptionInstrument() != null) {
			AccountingPositionCommand accountingPositionCommand = AccountingPositionCommand.onPositionTransactionDate(rollCommand.getTradeDate() != null ? rollCommand.getTradeDate() : new Date());
			accountingPositionCommand.setClientInvestmentAccountId(accountRoll.getClientAccount().getId());
			accountingPositionCommand.setInvestmentInstrumentId(rollCommand.getOptionInstrument().getId());
			List<AccountingPosition> accountingPositionList = getAccountingPositionService().getAccountingPositionListUsingCommand(accountingPositionCommand);
			Map<Integer, List<AccountingPosition>> holdingAccountPositionMap = BeanUtils.getBeansMap(accountingPositionList, accountingPosition -> accountingPosition.getHoldingInvestmentAccount().getId());
			if (CollectionUtils.getSize(holdingAccountPositionMap.keySet()) == 1) {
				for (InvestmentAccount account : list) {
					if (holdingAccountPositionMap.containsKey(account.getId())) {
						accountRoll.setHoldingAccount(account);
						break;
					}
				}
			}
		}
		// Note: It's possible users will be able to submit the trade without a holding account to select later.
		// The one DE account that runs into this issue 800025 will have one for SPX options (will add support for that one in filtering later), but multiple allowed for OTC Options
		// which we won't know until later.
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getDefaultAccountCalculatorBeanId() {
		return this.defaultAccountCalculatorBeanId;
	}


	public void setDefaultAccountCalculatorBeanId(Integer defaultAccountCalculatorBeanId) {
		this.defaultAccountCalculatorBeanId = defaultAccountCalculatorBeanId;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public PortfolioRunTradeGroupHandler<ProductOverlayAssetClassReplication> getPortfolioRunTradeGroupHandler() {
		return this.portfolioRunTradeGroupHandler;
	}


	public void setPortfolioRunTradeGroupHandler(PortfolioRunTradeGroupHandler<ProductOverlayAssetClassReplication> portfolioRunTradeGroupHandler) {
		this.portfolioRunTradeGroupHandler = portfolioRunTradeGroupHandler;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
