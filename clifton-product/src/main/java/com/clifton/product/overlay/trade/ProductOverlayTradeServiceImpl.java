package com.clifton.product.overlay.trade;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassAssignment;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentTypes;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.overlay.trade.ProductOverlayAssetClassReplicationTradeTargetDependency.ReplicationTargetDependencyTypes;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>ProductOverlayTradeServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class ProductOverlayTradeServiceImpl implements ProductOverlayTradeService {

	private InvestmentAccountAssetClassService investmentAccountAssetClassService;
	private PortfolioRunService portfolioRunService;
	private ProductOverlayService productOverlayService;


	@Override
	public List<ProductOverlayAssetClassReplicationTradeTargetDependency> getProductOverlayAssetClassReplicationTradeTargetDependencyList(int runId) {
		//ProductOverlayRunTrade run = getPortfolioRunForTrading(runId);
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);

		// Only load if Trading isn't completed yet
		// Also checked on the screen before server call, but just in case
		if (run.isClosed()) {
			return null;
		}

		List<ProductOverlayAssetClassReplication> replicationList = getProductOverlayService().getProductOverlayAssetClassReplicationListByRun(runId);

		// Process Asset Class Target = Actual
		List<ProductOverlayAssetClassReplicationTradeTargetDependency> dependencyList = createDependencyListForAssetClassTargetEqualsActual(replicationList);

		// Process Matching - Non-Matching
		dependencyList.addAll(createDependencyListForMatchingReplications(replicationList));

		// Process Reverse Matching - Non-Matching for M2M Adjustments
		dependencyList.addAll(createDependencyListForM2MFromMatchingReplications(replicationList));

		return dependencyList;
	}


	private List<ProductOverlayAssetClassReplicationTradeTargetDependency> createDependencyListForAssetClassTargetEqualsActual(List<ProductOverlayAssetClassReplication> replicationList) {

		List<ProductOverlayAssetClassReplicationTradeTargetDependency> dependencyList = new ArrayList<>();

		// Do we have any Asset Class Target Equals Actual?
		List<ProductOverlayAssetClassReplication> targetFollowsActualList = BeanUtils.filter(replicationList, overlayAssetClassReplication -> overlayAssetClassReplication.isTargetAdjusted() && overlayAssetClassReplication.getAccountAssetClass().isTargetExposureAdjustmentUsed() && InvestmentTargetExposureAdjustmentTypes.TFA == overlayAssetClassReplication.getAccountAssetClass().getTargetExposureAdjustment().getTargetExposureAdjustmentType(), true);

		if (!CollectionUtils.isEmpty(targetFollowsActualList)) {
			// When updating targets on other asset classes - only affects NON matching replications
			List<ProductOverlayAssetClassReplication> nonMatchingRepList = BeanUtils.filter(replicationList, ProductOverlayAssetClassReplication::isMatchingReplication, false);

			// Track Asset Classes Processed
			Set<Integer> assetClassesProcessed = new HashSet<>();

			// Iterate through the TFA list and setup target dependencies
			for (ProductOverlayAssetClassReplication overlayAssetClassReplication : targetFollowsActualList) {
				Integer accountAssetClassId = overlayAssetClassReplication.getAccountAssetClass().getId();
				if (assetClassesProcessed.add(accountAssetClassId)) {
					List<ProductOverlayAssetClassReplication> assetClassTargetFollowsActualList = BeanUtils.filter(targetFollowsActualList, replication -> replication.getAccountAssetClass().getId(), accountAssetClassId);
					dependencyList.addAll(createDependencyListForAssetClassTargetEqualsActualForAssetClass(accountAssetClassId, assetClassTargetFollowsActualList, nonMatchingRepList));
				}
			}
		}
		return dependencyList;
	}


	private List<ProductOverlayAssetClassReplicationTradeTargetDependency> createDependencyListForAssetClassTargetEqualsActualForAssetClass(Integer accountAssetClassId, List<ProductOverlayAssetClassReplication> assetClassReplicationList, List<ProductOverlayAssetClassReplication> nonMatchingReplicationList) {
		List<InvestmentAccountAssetClassAssignment> assignList = getInvestmentAccountAssetClassService().getInvestmentAccountAssetClassAssignmentListByParent(accountAssetClassId);
		InvestmentAccountAssetClass[] accountAssetClasses = BeanUtils.getPropertyValues(assignList, InvestmentAccountAssetClassAssignment::getReferenceTwo, InvestmentAccountAssetClass.class);
		List<ProductOverlayAssetClassReplication> assignRepList = BeanUtils.filter(nonMatchingReplicationList, overlayReplication -> ArrayUtils.contains(accountAssetClasses, overlayReplication.getOverlayAssetClass().getAccountAssetClass()));
		List<ProductOverlayAssetClassReplicationTradeTargetDependency> dependencyList = new ArrayList<>();

		// First: Asset Class Target = Actual - add it as a reference to itself to update targets to match it's own Final Overlay Exposure
		for (ProductOverlayAssetClassReplication assetClassReplication : CollectionUtils.getIterable(assetClassReplicationList)) {
			dependencyList.add(new ProductOverlayAssetClassReplicationTradeTargetDependency(ReplicationTargetDependencyTypes.TARGET_EQUALS_ACTUAL, assetClassReplication,
					assetClassReplication, MathUtils.BIG_DECIMAL_ONE_HUNDRED, false));
		}

		// Now find the assignments and set them to update targets in order to offset the TFA target adjustment made
		if (!CollectionUtils.isEmpty(assignList) && !CollectionUtils.isEmpty(assignRepList)) {
			// Separate Proportional with Explicitly defined - reduce proportional total by what's been used already, then will use proportional asset class percentages of total available to get the proportional percentage
			Map<Boolean, List<InvestmentAccountAssetClassAssignment>> proportionalListMap = BeanUtils.getBeansMap(assignList, InvestmentAccountAssetClassAssignment::isProportionalAllocation);
			BigDecimal proportionalAdjustment = MathUtils.BIG_DECIMAL_ONE_HUNDRED;

			// Explicitly Defined
			for (InvestmentAccountAssetClassAssignment assign : CollectionUtils.getIterable(proportionalListMap.get(false))) {
				proportionalAdjustment = MathUtils.subtract(proportionalAdjustment, assign.getAllocationPercent());
				List<ProductOverlayAssetClassReplication> depRepList = BeanUtils.filter(assignRepList, overlayReplication -> overlayReplication.getOverlayAssetClass().getAccountAssetClass(), assign.getReferenceTwo());

				for (ProductOverlayAssetClassReplication causeReplication : CollectionUtils.getIterable(assetClassReplicationList)) {
					for (ProductOverlayAssetClassReplication dep : CollectionUtils.getIterable(depRepList)) {
						dependencyList.add(new ProductOverlayAssetClassReplicationTradeTargetDependency(ReplicationTargetDependencyTypes.TARGET_EQUALS_ACTUAL_OFFSET, causeReplication,
								dep, assign.getAllocationPercent(), true));
					}
				}
			}

			// Proportional Assignment
			BigDecimal proportionalTotal = CoreMathUtils.sumProperty(proportionalListMap.get(true), investmentAccountAssetClassAssignment -> investmentAccountAssetClassAssignment.getReferenceTwo() == null ? null : investmentAccountAssetClassAssignment.getReferenceTwo().getAssetClassPercentage());
			for (InvestmentAccountAssetClassAssignment propAssign : CollectionUtils.getIterable(proportionalListMap.get(true))) {
				BigDecimal percentage = CoreMathUtils.getPercentValue(propAssign.getReferenceTwo().getAssetClassPercentage(), proportionalTotal, true);
				percentage = CoreMathUtils.getPercentValue(percentage, proportionalAdjustment, true);
				List<ProductOverlayAssetClassReplication> depRepList = BeanUtils.filter(nonMatchingReplicationList, overlayReplication -> overlayReplication.getOverlayAssetClass().getAccountAssetClass(), propAssign.getReferenceTwo());

				for (ProductOverlayAssetClassReplication causeReplication : CollectionUtils.getIterable(assetClassReplicationList)) {
					for (ProductOverlayAssetClassReplication dep : CollectionUtils.getIterable(depRepList)) {
						dependencyList.add(new ProductOverlayAssetClassReplicationTradeTargetDependency(ReplicationTargetDependencyTypes.TARGET_EQUALS_ACTUAL_OFFSET, causeReplication,
								dep, MathUtils.getPercentageOf(dep.getAllocationWeightAdjusted(), percentage, true), true));
					}
				}
			}
		}
		return dependencyList;
	}


	private List<ProductOverlayAssetClassReplicationTradeTargetDependency> createDependencyListForMatchingReplications(List<ProductOverlayAssetClassReplication> replicationList) {

		List<ProductOverlayAssetClassReplicationTradeTargetDependency> dependencyList = new ArrayList<>();

		// Pull Out Matching Replications
		List<ProductOverlayAssetClassReplication> matchingRepList = BeanUtils.filter(replicationList, ProductOverlayAssetClassReplication::isMatchingReplication);

		for (ProductOverlayAssetClassReplication overlayReplication : CollectionUtils.getIterable(replicationList)) {
			if (overlayReplication.getReplicationAllocation().getMatchingReplication() != null) {
				// Not likely to have the same replication used as a matching rep in two different asset classes, but it is possible, so also check asset class
				List<ProductOverlayAssetClassReplication> usedReplicationList = CollectionUtils.getStream(matchingRepList)
						.filter(r -> CompareUtils.isEqual(overlayReplication.getOverlayAssetClass(), r.getOverlayAssetClass()))
						.filter(r -> CompareUtils.isEqual(overlayReplication.getReplicationAllocation().getMatchingReplication(), r.getReplication()))
						.collect(Collectors.toList());

				BigDecimal total = CoreMathUtils.sumProperty(usedReplicationList, ProductOverlayAssetClassReplication::getAllocationWeightAdjusted);

				ReplicationTargetDependencyTypes type = ReplicationTargetDependencyTypes.MATCHING_TARGET;
				if (overlayReplication.getOverlayAssetClass().getAccountAssetClass().isUseActualForMatchingReplication()) {
					type = ReplicationTargetDependencyTypes.MATCHING_ACTUAL;
				}
				boolean negateAllocation = false;
				for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(usedReplicationList)) {
					// If original allocation weight was negative and adjusted is positive, then negate allocation
					// adjusted was changed to positive to force to = 100%
					if (MathUtils.isLessThan(rep.getAllocationWeight(), BigDecimal.ZERO) && MathUtils.isGreaterThanOrEqual(rep.getAllocationWeightAdjusted(), BigDecimal.ZERO)) {
						negateAllocation = true;
					}
					dependencyList.add(new ProductOverlayAssetClassReplicationTradeTargetDependency(type, overlayReplication, rep, MathUtils.getPercentageOf(rep.getAllocationWeightAdjusted(), total, true), negateAllocation));
				}
			}
		}

		return dependencyList;
	}


	private List<ProductOverlayAssetClassReplicationTradeTargetDependency> createDependencyListForM2MFromMatchingReplications(List<ProductOverlayAssetClassReplication> replicationList) {

		List<ProductOverlayAssetClassReplicationTradeTargetDependency> dependencyList = new ArrayList<>();

		// Pull Out Matching Replications
		List<ProductOverlayAssetClassReplication> matchingRepList = BeanUtils.filter(replicationList, ProductOverlayAssetClassReplication::isMatchingReplication);

		for (ProductOverlayAssetClassReplication matchRep : CollectionUtils.getIterable(matchingRepList)) {
			List<ProductOverlayAssetClassReplication> nonMatchReplicationList = BeanUtils.filter(replicationList, overlayReplication -> overlayReplication.getReplicationAllocation().getMatchingReplication(), matchRep.getReplication());
			if (!CollectionUtils.isEmpty(nonMatchReplicationList)) {
				BigDecimal total = CoreMathUtils.sumProperty(nonMatchReplicationList, replication -> replication == null ? null : replication.getAllocationWeightAdjusted());

				for (ProductOverlayAssetClassReplication nonMatchReplication : nonMatchReplicationList) {
					dependencyList.add(new ProductOverlayAssetClassReplicationTradeTargetDependency(ReplicationTargetDependencyTypes.M2M, matchRep, nonMatchReplication, CoreMathUtils.getPercentValue(nonMatchReplication.getAllocationWeightAdjusted(), total, true), false));
				}
			}
		}
		return dependencyList;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}
}
