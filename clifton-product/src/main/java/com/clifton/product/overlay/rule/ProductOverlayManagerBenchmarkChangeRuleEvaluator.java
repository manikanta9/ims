package com.clifton.product.overlay.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.manager.BasePortfolioRunManagerAccountRuleEvaluator;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * ProductOverlayManagerBenchmarkChangeRuleEvaluator class compares manager asset class allocation changes to the asset class benchmark change
 * and create violations if the difference is outside of specified bands
 *
 * @author manderson
 */
public class ProductOverlayManagerBenchmarkChangeRuleEvaluator extends BasePortfolioRunManagerAccountRuleEvaluator<PortfolioRun, ProductOverlayRuleEvaluatorContext> {


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, ProductOverlayRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		List<ProductOverlayManagerAccount> managerAccountList = context.getProductOverlayManagerList(run, false, null);
		Map<Integer, BigDecimal> assetClassReturnMap = context.getAccountAssetClassBenchmarkReturnMap(run);
		for (ProductOverlayManagerAccount overlayManagerAccount : CollectionUtils.getIterable(managerAccountList)) {
			if (!overlayManagerAccount.isRollupAssetClass()) {
				InvestmentManagerAccount managerAccount = overlayManagerAccount.getManagerAccountAssignment().getReferenceOne();
				long entityFkFieldId = BeanUtils.getIdentityAsLong(managerAccount);
				EntityConfig entityConfig = getEntityConfig(managerAccount, ruleConfig);
				if (entityConfig != null && !entityConfig.isExcluded()) {
					BigDecimal assetClassReturn = assetClassReturnMap.get(overlayManagerAccount.getAccountAssetClass().getId());
					if (assetClassReturn != null) {
						BigDecimal managerChange = (overlayManagerAccount.getOneDayPercentChange() == null ? BigDecimal.ZERO : overlayManagerAccount.getOneDayPercentChange());
						BigDecimal change = MathUtils.subtract(managerChange, assetClassReturn);
						Map<String, Object> templateValues = new HashMap<>();
						// Do not generate a warning if the manager has a manual adjustment - we already have warning for these so it is redundant
						if (RuleEvaluatorUtils.isEntityConfigRangeViolated(change, entityConfig, templateValues)
								&& !context.isManagerBalanceManuallyAdjusted(overlayManagerAccount)) {
							templateValues.put("managerChange", managerChange);
							ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), entityFkFieldId, BeanUtils.getIdentityAsLong(overlayManagerAccount), null, templateValues));
						}
					}
				}
			}
		}
		return ruleViolationList;
	}
}
