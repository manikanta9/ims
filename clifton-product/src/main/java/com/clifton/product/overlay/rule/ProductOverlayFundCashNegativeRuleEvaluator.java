package com.clifton.product.overlay.rule;

import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunFundCashNegativeRuleEvaluator;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.overlay.search.ProductOverlayAssetClassSearchForm;

import java.math.BigDecimal;
import java.util.List;


/**
 * Concrete implementation of {@link PortfolioRunFundCashNegativeRuleEvaluator}. Uses the {@link ProductOverlayService} to retrieve the exposure summary for the provided run from the database.
 * Includes any adjustments in the calculation of Fund Cash.
 *
 * @author mitchellf
 */
public class ProductOverlayFundCashNegativeRuleEvaluator extends PortfolioRunFundCashNegativeRuleEvaluator<ProductOverlayAssetClass> {

	private ProductOverlayService productOverlayService;


	@Override
	protected BigDecimal getFundCash(ProductOverlayAssetClass assetClass) {
		return assetClass.getFundCashWithAdjustments();
	}


	@Override
	protected List<ProductOverlayAssetClass> getSummaryList(PortfolioRun run) {
		ProductOverlayAssetClassSearchForm sf = new ProductOverlayAssetClassSearchForm();
		sf.setRunId(run.getId());
		sf.setRootOnly(true);
		return getProductOverlayService().getProductOverlayAssetClassList(sf, true);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}
}
