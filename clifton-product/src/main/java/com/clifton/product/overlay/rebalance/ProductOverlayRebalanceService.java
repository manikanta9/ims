package com.clifton.product.overlay.rebalance;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalance;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import com.clifton.product.overlay.rebalance.minimizeimbalances.ProductOverlayMinimizeImbalancesSettings;
import org.springframework.web.bind.annotation.ModelAttribute;


/**
 * The <code>ProductOverlayRebalanceService</code> ...
 *
 * @author manderson
 */
public interface ProductOverlayRebalanceService {

	///////////////////////////////////////////////////////////
	////////     Product Overlay Rebalance Methods    /////////
	///////////////////////////////////////////////////////////


	/**
	 * Returns special bean that contains rebalance specific configuration, fields for specified run
	 * so that users can manually enter rebalancing values and save
	 *
	 * @param overlayRunId
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public ProductOverlayRebalance getProductOverlayRebalance(int overlayRunId);


	/**
	 * Saves/Processes Rebalancing based on bean.action field set
	 *
	 * @param bean
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public void saveProductOverlayRebalance(ProductOverlayRebalance bean);


	/**
	 * Called directly from PIOS processing.  Asset Classes haven't been saved in the database yet,
	 * so need to pull the list from the runConfig object
	 *
	 * @param runConfig
	 */
	@DoNotAddRequestMapping
	public void processProductOverlayRebalanceAutomatic(ProductOverlayRunConfig runConfig);


	///////////////////////////////////////////////////////////
	/////   Product Overlay Minimize Imbalances Methods   /////
	///////////////////////////////////////////////////////////


	/**
	 * Returns the column group name the minimize imbalances calculator uses (if any)
	 * If no calculator, or no column group name used on the calculator - returns null;
	 *
	 * @param investmentAccountId
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = InvestmentAccountRebalance.class)
	public String getProductOverlayMinimizeImbalancesSettingsColumnGroupName(int investmentAccountId);


	@SecureMethod(dtoClass = InvestmentAccountRebalance.class)
	public ProductOverlayMinimizeImbalancesSettings getProductOverlayMinimizeImbalancesSettings(int investmentAccountId);


	/**
	 * Returns true if minimize imbalances processing was performed and ProductOverlayAssetClasses need to be saved
	 *
	 * @param runConfig
	 */
	@DoNotAddRequestMapping
	public boolean processProductOverlayMinimizeImbalances(ProductOverlayRunConfig runConfig);
}
