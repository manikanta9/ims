package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceAssetClassHistory;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductOverlayMinimizeImbalancesUnderweightCalculator</code> minimizes imbalances using the traditional method
 * where manager cash is first allocated to most underweight (where overlaid to minimize imbalances defined in buckets in MinimizeImbalancesCashConfig)
 * Then re-allocated to remove from overweight and give to underweight
 *
 * @author manderson
 */
public class ProductOverlayMinimizeImbalancesUnderweightCalculator extends BaseProductOverlayMinimizeImbalancesCalculatorImpl {

	private boolean shortTargetsAllowed;

	/**
	 * short positions cannot be expanded other than in a rebalance event (full rebalance). This means that each of these accounts can have net short positions
	 * (as a result of a rebalance), however those short positions cannot be increased until another full rebalance is performed.
	 */
	private boolean expandShortPositionsAllowed;


	@Override
	public String getCustomColumnGroupName() {
		return null;
	}


	@Override
	public boolean calculateMinimizeImbalances(ProductOverlayRunConfig config) {
		BigDecimal totalMinimizeImbalancesCash = getTotalMinimizeImbalanceCash(config);

		// Apply Manager Cash Overlaid to Minimize Imbalances First
		// And Reallocate Cash Across Asset Classes - Take From Overweight and Give to Underweight
		Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap = calculateMinimizeImbalancesImpl(config, config.getOverlayAssetClassListExcludeRollups(), totalMinimizeImbalancesCash);

		// Apply Cash to Asset Classes from the map based on MinimizeImbalancesCashConfig setup
		return applyCashToAssetClasses(config, assetClassCashMap, true);
	}


	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////


	protected Map<InvestmentAccountAssetClass, BigDecimal> calculateMinimizeImbalancesImpl(ProductOverlayRunConfig config, List<ProductOverlayAssetClass> list, BigDecimal totalMinimizeImbalancesCash) {
		StringBuilder debugOutput = new StringBuilder(StringUtils.NEW_LINE + config.getRun().getLabel() + " Minimizing Imbalances" + StringUtils.NEW_LINE);

		// First - Overlay Manager Minimize Imbalances to fill Overlay Exposure
		Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap = new HashMap<>();
		// If a full rebalance was performed on the same date as the balance date, use the rebalance cash values that were there before the full rebalance in minimize imbalances calculation
		// We build a map that will also define values at the rollup level if needed for calculating
		Map<InvestmentAccountAssetClass, BigDecimal> rebalanceCashOverrideMap = getRebalanceCashOverrideMap(config);
		BigDecimal remainingCash = applyMinimizeImbalancesCashToFillOverlayExposure(config, list, assetClassCashMap, totalMinimizeImbalancesCash, rebalanceCashOverrideMap, debugOutput);

		// If ZERO, then everything matched up so Target = Exposure nothing over or under to apply to minimize imbalances;
		// Otherwise we either have extra and need to add exposure to most underweight
		// or not enough and need to remove exposure from most overweight
		// Create a Deviation Map to track over/under during processing
		if (!MathUtils.isEqual(remainingCash, BigDecimal.ZERO)) {
			Map<InvestmentAccountAssetClass, BigDecimal> deviationMap = new HashMap<>();

			List<ProductOverlayAssetClass> underweightList = new ArrayList<>();
			List<ProductOverlayAssetClass> overweightList = new ArrayList<>();

			for (ProductOverlayAssetClass oac : list) {
				if (!isProcessProductOverlayAssetClass(config, oac)) {
					continue;
				}

				// If total exposure deviation from Adjusted target is negative, then we are underweight
				// How much are we over/underweight
				// This is the percent of total deviation from adjusted target as a percentage of the adjusted target
				BigDecimal deviation = getDeviationPercentage(oac.getTotalExposureDeviationFromAdjustedTarget(), oac);
				deviationMap.put(oac.getAccountAssetClass(), deviation);

				debugOutput.append(StringUtils.NEW_LINE).append(oac.getLabel()).append(" is ").append(MathUtils.isLessThan(deviation, BigDecimal.ZERO) ? "under" : "over").append("weight by: ").append(CoreMathUtils.formatNumberMoney(oac.getTotalExposureDeviationFromAdjustedTarget())).append(" = ").append(CoreMathUtils.formatNumberDecimal(deviationMap.get(oac.getAccountAssetClass()))).append(" %").append(StringUtils.NEW_LINE);
				if (MathUtils.isLessThan(deviation, BigDecimal.ZERO)) {
					underweightList.add(oac);
				}
				else {
					overweightList.add(oac);
				}

				// Start At Zero if not already populated
				if (!assetClassCashMap.containsKey(oac.getAccountAssetClass())) {
					assetClassCashMap.put(oac.getAccountAssetClass(), BigDecimal.ZERO);
				}
			}

			// If Cash Available is Positive - Adding to Underweight, then Underweight + Overweight
			// Applies proportionally to most underweight to bring the most underweight as a percentage of the adjusted target to the next underweight percentage
			// If No Underweight, or fully applied and cash left over (i.e. all now are overweight) - Applies to least overweight relative to it's target again proportionally bringing it back up to the next overweight
			if (MathUtils.isGreaterThan(remainingCash, BigDecimal.ZERO)) {
				// Since adding - sort ascending
				underweightList = getProductOverlayAssetClassListSortedByDeviationPercentage(underweightList, true);
				overweightList = getProductOverlayAssetClassListSortedByDeviationPercentage(overweightList, true);

				// Then Add to Underweight
				if (!CollectionUtils.isEmpty(underweightList)) {
					debugOutput.append(StringUtils.NEW_LINE + "Add remaining cash to underweight" + StringUtils.NEW_LINE);
					remainingCash = applyManagerCashOverlayToMinimizeImbalances(assetClassCashMap, remainingCash, underweightList, deviationMap, CollectionUtils.isEmpty(overweightList), debugOutput);
					// IF TOTAL MINIMIZE IMBALANCE CASH IS NOT ZERO - APPEND OVERWEIGHT LIST TO UNDERWEIGHT LIST AND APPLY THE REST OF THE CASH
					if (!MathUtils.isEqual(remainingCash, BigDecimal.ZERO)) {
						debugOutput.append(StringUtils.NEW_LINE + "Underweight filled, now all are on target or overweight, add remaining cash proportionally to all." + StringUtils.NEW_LINE);
						underweightList.addAll(overweightList);
						applyManagerCashOverlayToMinimizeImbalances(assetClassCashMap, remainingCash, underweightList, deviationMap, true, debugOutput);
					}
				}
				// If we didn't have any underweight to add to add to overweight - least overweight to most overweight
				else {
					debugOutput.append(StringUtils.NEW_LINE + "No Underweight, add remaining cash to overweight" + StringUtils.NEW_LINE);
					applyManagerCashOverlayToMinimizeImbalances(assetClassCashMap, remainingCash, overweightList, deviationMap, true, debugOutput);
				}
			}
			// If Cash Available is Negative - Remove from Overweight...If none, then underweight
			// Applies to most overweight to least overweight - proportionally bring the most overweight as a percentage of the adjusted target to the next Overweight percentage
			// If No Overweight - Applies to least underweight relative to it's target again proportionally bringing it to the next underweight
			else {
				// Since removing - sort descending
				underweightList = getProductOverlayAssetClassListSortedByDeviationPercentage(underweightList, false);
				overweightList = getProductOverlayAssetClassListSortedByDeviationPercentage(overweightList, false);

				// Then Take Away From Overweight
				if (!CollectionUtils.isEmpty(overweightList)) {
					debugOutput.append(StringUtils.NEW_LINE + "Remove remaining cash from overweight" + StringUtils.NEW_LINE);
					remainingCash = applyManagerCashOverlayToMinimizeImbalances(assetClassCashMap, remainingCash, overweightList, deviationMap, CollectionUtils.isEmpty(underweightList), debugOutput);
					// IF TOTAL MINIMIZE IMBALANCE CASH IS NOT ZERO - APPEND UNDERWEIGHT LIST TO OVERWEIGHT LIST AND APPLY THE REST OF THE CASH
					if (!MathUtils.isEqual(remainingCash, BigDecimal.ZERO)) {
						debugOutput.append(StringUtils.NEW_LINE + "Overweight removed, now all are on target or underweight...remove remaining cash proportionally to all." + StringUtils.NEW_LINE);
						overweightList.addAll(underweightList);
						applyManagerCashOverlayToMinimizeImbalances(assetClassCashMap, remainingCash, overweightList, deviationMap, true, debugOutput);
					}
				}
				// If we have no overweight to take from, take from underweight least underweight to most underweight
				else {
					debugOutput.append(StringUtils.NEW_LINE + "No Overweight, remove remaining cash from underweight" + StringUtils.NEW_LINE);
					applyManagerCashOverlayToMinimizeImbalances(assetClassCashMap, remainingCash, underweightList, deviationMap, true, debugOutput);
				}
			}
		}
		LogUtils.info(getClass(), debugOutput.toString());
		return assetClassCashMap;
	}


	private BigDecimal applyManagerCashOverlayToMinimizeImbalances(Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap, BigDecimal cashAvailable, List<ProductOverlayAssetClass> list,
	                                                               Map<InvestmentAccountAssetClass, BigDecimal> deviationMap, boolean applyAllAvailableCash, StringBuilder debugOutput) {

		if (MathUtils.isEqual(cashAvailable, BigDecimal.ZERO)) {
			return cashAvailable;
		}

		list = filterProductOverlayAssetClassListForNoChanges(list, cashAvailable, assetClassCashMap);

		int currentIndex = 0;
		int totalSize = CollectionUtils.getSize(list);

		while (currentIndex < totalSize) {
			// NOTE: Changed to use current instead of always comparing against the first one
			// because for no shorts allowed, when removing cash, we take what we can but that's it
			// Going forward want to get the next one, not always compare it to the first if the first one will always remain as it is.
			ProductOverlayAssetClass current = list.get(currentIndex);
			BigDecimal thisPercentage = deviationMap.get(current.getAccountAssetClass());
			BigDecimal nextPercentage = BigDecimal.ZERO;

			if (currentIndex < totalSize - 1) {
				ProductOverlayAssetClass nextAc = list.get(currentIndex + 1);
				nextPercentage = deviationMap.get(nextAc.getAccountAssetClass());
			}
			BigDecimal diffPercentage = MathUtils.round(MathUtils.subtract(nextPercentage, thisPercentage), 10);

			// If difference in percentage, OR Last one and we need to apply all available cash
			if (!MathUtils.isNullOrZero(diffPercentage) || ((currentIndex == totalSize - 1) && applyAllAvailableCash)) {
				Map<InvestmentAccountAssetClass, BigDecimal> needAmountMap = calculateNeedAmountMap(currentIndex, list, cashAvailable, diffPercentage, assetClassCashMap, applyAllAvailableCash,
						debugOutput, 0);

				for (int i = 0; i <= currentIndex; i++) {

					ProductOverlayAssetClass ac = list.get(i);
					debugOutput.append(StringUtils.NEW_LINE + StringUtils.NEW_LINE).append(ac.getLabel());
					debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "Before Cash Total: ").append(CoreMathUtils.formatNumberMoney(assetClassCashMap.get(ac.getAccountAssetClass())));
					debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "Before Deviation: ").append(CoreMathUtils.formatNumberMoney(deviationMap.get(ac.getAccountAssetClass()))).append(" %");

					if (!needAmountMap.containsKey(ac.getAccountAssetClass())) {
						// Would be skipped if no short targets allowed and cash total is already zero
						debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "NO CHANGE");
						continue;
					}

					BigDecimal thisApplyAmount = needAmountMap.get(ac.getAccountAssetClass());
					BigDecimal thisDeviationAmount = getDeviationPercentage(thisApplyAmount, ac);

					deviationMap.put(ac.getAccountAssetClass(), MathUtils.add(deviationMap.get(ac.getAccountAssetClass()), thisDeviationAmount));
					assetClassCashMap.put(ac.getAccountAssetClass(), MathUtils.add(assetClassCashMap.get(ac.getAccountAssetClass()), thisApplyAmount));
					//totalApplied = MathUtils.add(totalApplied, thisApplyAmount);
					cashAvailable = MathUtils.subtract(cashAvailable, thisApplyAmount);

					debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "Cash Adjustment: ").append(CoreMathUtils.formatNumberMoney(thisApplyAmount));
					debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "Deviation Adjustment: ").append(CoreMathUtils.formatNumberMoney(thisDeviationAmount)).append(" %");

					debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "After Cash Total: ").append(CoreMathUtils.formatNumberMoney(assetClassCashMap.get(ac.getAccountAssetClass())));
					debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "After Deviation: ").append(CoreMathUtils.formatNumberMoney(deviationMap.get(ac.getAccountAssetClass()))).append(" %");
				}
				debugOutput.append(StringUtils.NEW_LINE + StringUtils.NEW_LINE + "*** Remaining Cash: ").append(CoreMathUtils.formatNumberMoney(cashAvailable)).append(StringUtils.NEW_LINE);
			}
			if (MathUtils.isEqual(cashAvailable, BigDecimal.ZERO)) {
				break;
			}
			currentIndex++;
		}
		return cashAvailable;
	}


	/**
	 * If we have negative cash to apply and cannot go short, or expand shorts
	 * filter those asset classes out of the list
	 * so we don't include their deviation in the list.  They'll never change
	 */
	private List<ProductOverlayAssetClass> filterProductOverlayAssetClassListForNoChanges(List<ProductOverlayAssetClass> list, BigDecimal cashAvailable,
	                                                                                      Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap) {
		List<ProductOverlayAssetClass> filteredList = new ArrayList<>();
		if (MathUtils.isLessThan(cashAvailable, BigDecimal.ZERO) && (!isShortTargetsAllowed() || !isExpandShortPositionsAllowed())) {
			for (ProductOverlayAssetClass oac : CollectionUtils.getIterable(list)) {
				BigDecimal acCash = MathUtils.add(oac.getCashTotal(), assetClassCashMap.get(oac.getAccountAssetClass()));
				if (MathUtils.isGreaterThan(acCash, BigDecimal.ZERO)) {
					filteredList.add(oac);
				}
			}
		}
		else {
			filteredList = list;
		}
		return filteredList;
	}


	private Map<InvestmentAccountAssetClass, BigDecimal> calculateNeedAmountMap(int currentIndex, List<ProductOverlayAssetClass> list, BigDecimal cashAvailable, BigDecimal diffPercentage,
	                                                                            Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap, boolean applyAllAvailableCash, StringBuilder debugOutput, int count) {

		Map<InvestmentAccountAssetClass, BigDecimal> needAmountMap = new HashMap<>();
		Map<InvestmentAccountAssetClass, BigDecimal> staticNeedAmountMap = new HashMap<>();

		// Calculate Needed Amounts for Each Asset Class
		for (int i = 0; i <= currentIndex; i++) {
			ProductOverlayAssetClass ac = list.get(i);
			BigDecimal needAmount = MathUtils.getPercentageOf(diffPercentage, getDeviationPercentageDenominatorValue(ac), true);
			adjustCalculatedNeedAmount(ac, assetClassCashMap, cashAvailable, needAmount, needAmountMap, staticNeedAmountMap, debugOutput);
		}

		BigDecimal totalNeedAmount = CoreMathUtils.sum(needAmountMap.values());
		BigDecimal staticNeedAmount = CoreMathUtils.sum(staticNeedAmountMap.values());

		// Don't "need" anything, but still have cash to apply - and applyAllAvailableCash = true
		if (MathUtils.isNullOrZero(totalNeedAmount) && applyAllAvailableCash) {
			// and NOT the last one in the list return
			if (currentIndex != list.size() - 1) {
				return needAmountMap;
			}
			// Otherwise apply proportionally
			else {
				// Need to check asset classes from need amount map so we skip those that can't be adjusted anymore:
				List<ProductOverlayAssetClass> filteredList = BeanUtils.filter(list, ProductOverlayAssetClass::getAccountAssetClass, needAmountMap.keySet().toArray());
				BigDecimal totalTarget = getDeviationTotal(filteredList);
				// Reset diffPercentage for debug output purposes - not actually used anymore
				diffPercentage = CoreMathUtils.getPercentValue(cashAvailable, totalTarget, true);
				int size = list.size();

				for (int i = 0; i <= currentIndex; i++) {
					ProductOverlayAssetClass ac = list.get(i);
					if (needAmountMap.containsKey(ac.getAccountAssetClass())) {
						BigDecimal needAmount = MathUtils.isNullOrZero(totalTarget) ? MathUtils.divide(cashAvailable, size) : MathUtils.getPercentageOf(
								CoreMathUtils.getPercentValue(getDeviationPercentageDenominatorValue(ac), totalTarget, true), cashAvailable, true);
						adjustCalculatedNeedAmount(ac, assetClassCashMap, cashAvailable, needAmount, needAmountMap, staticNeedAmountMap, debugOutput);
					}
				}
			}
		}
		else {
			BigDecimal applyAmount;

			// Do we have enough cash
			if (MathUtils.isSameSignum(cashAvailable, totalNeedAmount) && MathUtils.isGreaterThanOrEqual(MathUtils.abs(cashAvailable), MathUtils.abs(totalNeedAmount))) {
				// If we  have enough cash, plus extra and it's the last round - apply all that we have
				if (applyAllAvailableCash && currentIndex == CollectionUtils.getSize(list) - 1) {
					applyAmount = cashAvailable;
				}
				else {
					applyAmount = totalNeedAmount;
				}
			}
			else {
				applyAmount = cashAvailable;
			}

			if (count <= 10) {
				BigDecimal prorateNeedAmountPercentage;
				if (MathUtils.isEqual(totalNeedAmount, staticNeedAmount)) {
					prorateNeedAmountPercentage = MathUtils.getPercentChange(totalNeedAmount, applyAmount, true);
				}
				else {
					prorateNeedAmountPercentage = MathUtils.getPercentChange(MathUtils.subtract(totalNeedAmount, staticNeedAmount), MathUtils.subtract(applyAmount, staticNeedAmount), true);
				}
				// If prorating - recalc need amounts based on new percentages...also verifies that we don't go short if not allowed
				if (!MathUtils.isEqual(prorateNeedAmountPercentage, BigDecimal.ZERO)) {
					debugOutput.append(StringUtils.NEW_LINE + StringUtils.NEW_LINE + " Proportionally adjust by ").append(CoreMathUtils.formatNumberDecimal(diffPercentage)).append("% ");

					// Over/Under apply amount needed based on prorating percentage
					diffPercentage = MathUtils.add(diffPercentage, MathUtils.getPercentageOf(prorateNeedAmountPercentage, diffPercentage, true));
					debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + " Prorated by ").append(CoreMathUtils.formatNumberDecimal(prorateNeedAmountPercentage)).append("% ");
					debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + " New Adjustment ").append(CoreMathUtils.formatNumberDecimal(diffPercentage)).append("% ");
					// Called again with count incremented - recursive call max 10 times to avoid infinite loop, likely only needs a few recursive calls to finalize any penny adjustments
					return calculateNeedAmountMap(currentIndex, list, cashAvailable, diffPercentage, assetClassCashMap, applyAllAvailableCash, debugOutput, ++count);
				}
			}
		}

		debugOutput.append(StringUtils.NEW_LINE + StringUtils.NEW_LINE + " Proportionally adjust by ").append(CoreMathUtils.formatNumberDecimal(diffPercentage)).append("% ");
		return needAmountMap;
	}


	/**
	 * Applies for some cases - adjustments to the "need" amount based on minimize imbalance rules.
	 * i.e. long only would change negative need amounts so that we don't have any short targets
	 * For all cases, will round to 2 decimals to follow money precision
	 */
	private void adjustCalculatedNeedAmount(ProductOverlayAssetClass ac, Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap, BigDecimal cashAvailable, BigDecimal needAmount,
	                                        Map<InvestmentAccountAssetClass, BigDecimal> needAmountMap, Map<InvestmentAccountAssetClass, BigDecimal> staticNeedAmountMap, StringBuilder debugOutput) {

		BigDecimal currentCashTotal = MathUtils.add(ac.getCashTotal(), assetClassCashMap.get(ac.getAccountAssetClass()));
		debugOutput.append(StringUtils.NEW_LINE).append(ac.getLabel());
		debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "Current Cash Total: ").append(CoreMathUtils.formatNumberDecimal(currentCashTotal));
		debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "Need Amount (Orig): ").append(CoreMathUtils.formatNumberDecimal(needAmount));
		// Static Amount is used to prevent from violating long only or do not expand short rules
		boolean staticAmount = false;

		// If current cash is zero and cash available is negative and no shorts allowed - skip it
		if (!isShortTargetsAllowed() && MathUtils.isNullOrZero(currentCashTotal) && MathUtils.isLessThan(cashAvailable, BigDecimal.ZERO)) {
			needAmount = BigDecimal.ZERO;
			staticAmount = true;
		}

		// If subtracting from cash, i.e. subtracting from Overlay Target
		else if (MathUtils.isLessThan(needAmount, BigDecimal.ZERO)) {
			// and no shorts allowed, most that can be taken away is current cash - i.e. bring target to zero
			if (!isShortTargetsAllowed()) {

				if (MathUtils.isLessThan(MathUtils.add(currentCashTotal, needAmount), BigDecimal.ZERO)) {
					needAmount = MathUtils.negate(currentCashTotal);
					staticAmount = true;
				}
			}
			// Otherwise, if you are not allowed to expand short positions...
			else if (!isExpandShortPositionsAllowed()) {
				// And current <= 0 and Need is going to subtract (i.e. expand short)
				// Set need = 0 so it stays where it is.
				// If already short or going to make it short - leave it where it is
				if (MathUtils.isLessThanOrEqual(currentCashTotal, BigDecimal.ZERO)) {
					debugOutput.append(StringUtils.NEW_LINE).append(ac.getLabel()).append(" Do Not Expand Shorts - Change Need Amount From ").append(CoreMathUtils.formatNumberDecimal(needAmount)).append(" to Zero.");
					needAmount = BigDecimal.ZERO;
					staticAmount = true;
				}
				else if (MathUtils.isLessThan(MathUtils.add(currentCashTotal, needAmount), BigDecimal.ZERO)) {
					// Or current > 0 and current + need amount < 0, set need amount to MathUtils.negate(currentCashTotal)
					debugOutput.append(StringUtils.NEW_LINE).append(ac.getLabel()).append(" Do Not Expand Shorts - Change Need Amount From ").append(CoreMathUtils.formatNumberDecimal(needAmount)).append(" to ").append(CoreMathUtils.formatNumberDecimal(MathUtils.negate(currentCashTotal)));
					needAmount = MathUtils.negate(currentCashTotal);
					staticAmount = true;
				}
			}
		}

		// If opposite sign and need amount is less than a dollar set it to zero as it's minor rounding issues
		if (!MathUtils.isSameSignum(cashAvailable, needAmount) && MathUtils.isLessThan(MathUtils.abs(needAmount), BigDecimal.ONE)) {
			needAmount = BigDecimal.ZERO;
		}
		// Round amount to 2 decimals
		needAmount = MathUtils.round(needAmount, 2);

		// Put adjusted Values in the Map(s) - as long as not a zero static amount - in those cases we "skip" it
		debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "Need Amount (Adjusted): ").append(CoreMathUtils.formatNumberDecimal(needAmount));

		if (!(staticAmount && MathUtils.isNullOrZero(needAmount))) {
			needAmountMap.put(ac.getAccountAssetClass(), needAmount);

			if (staticAmount) {
				staticNeedAmountMap.put(ac.getAccountAssetClass(), needAmount);
			}
		}
		else {
			debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "Skip From Need Amount Map");
		}
	}


	///////////////////////////////////////////////////////////


	private Map<InvestmentAccountAssetClass, BigDecimal> getRebalanceCashOverrideMap(ProductOverlayRunConfig config) {
		boolean full = config.isFullRebalancePerformedOnBalanceDate();
		boolean manual = config.isManualRebalancePerformedOnBalanceDate();
		Map<InvestmentAccountAssetClass, BigDecimal> rebalanceCashOverrideMap = new HashMap<>();
		if (full || manual) {
			for (InvestmentAccountRebalanceAssetClassHistory acHistory : CollectionUtils.getIterable(config.getAccountRebalanceHistory().getAssetClassHistoryList())) {
				addRebalanceCashToMap(acHistory.getAccountAssetClass(), (full ? acHistory.getBeforeRebalanceCash() : BigDecimal.ZERO), rebalanceCashOverrideMap);
			}
		}
		return rebalanceCashOverrideMap;
	}


	/**
	 * Recursively adds Rebalance Cash Amount to the asset class and it's parent(s)
	 */
	private void addRebalanceCashToMap(InvestmentAccountAssetClass ac, BigDecimal amount, Map<InvestmentAccountAssetClass, BigDecimal> rebalanceCashOverrideMap) {
		if (ac != null) {
			rebalanceCashOverrideMap.put(ac, MathUtils.add(rebalanceCashOverrideMap.get(ac), amount));
			addRebalanceCashToMap(ac.getParent(), amount, rebalanceCashOverrideMap);
		}
	}


	///////////////////////////////////////////////////////////


	@Override
	protected boolean isShortTargetsAllowed() {
		return this.shortTargetsAllowed;
	}


	public void setShortTargetsAllowed(boolean shortTargetsAllowed) {
		this.shortTargetsAllowed = shortTargetsAllowed;
	}


	public boolean isExpandShortPositionsAllowed() {
		return this.expandShortPositionsAllowed;
	}


	public void setExpandShortPositionsAllowed(boolean expandShortPositionsAllowed) {
		this.expandShortPositionsAllowed = expandShortPositionsAllowed;
	}
}
