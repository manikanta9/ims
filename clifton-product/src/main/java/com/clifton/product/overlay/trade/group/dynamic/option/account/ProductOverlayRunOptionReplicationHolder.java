package com.clifton.product.overlay.trade.group.dynamic.option.account;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;


/**
 * <code>ProductOverlayRunOptionReplicationHolder</code> is an object that holds a collection of
 * {@link ProductOverlayAssetClassReplication}s for calculating total values.
 * <p>
 * The quantities, exposures, and market values returned are a calculated total for all contained replications.
 * The calculated values are calculated once and cached for retrieval until the collection of replications is modified.
 *
 * @author NickK
 */
public class ProductOverlayRunOptionReplicationHolder implements Iterable<ProductOverlayAssetClassReplication> {

	private static final String QUANTITY_ACTUAL = "quantityActual";
	private static final String QUANTITY_CURRENT = "quantityCurrent";
	private static final String QUANTITY_PENDING = "quantityPending";
	private static final String EXPOSURE_ACTUAL = "exposureActual";
	private static final String EXPOSURE_CURRENT = "exposureCurrent";
	private static final String EXPOSURE_PENDING = "exposurePending";
	private static final String MARKET_VALUE_ACTUAL = "marketValueActual";
	private static final String MARKET_VALUE_CURRENT = "marketValueCurrent";
	private static final String MARKET_VALUE_PENDING = "marketValuePending";

	/**
	 * Cache used for storing calculated values with the above keys to avoid unnecessary calculations.
	 */
	private final Map<String, BigDecimal> calculatedValueCache = new HashMap<>();

	private final List<ProductOverlayAssetClassReplication> replicationList = new ArrayList<>();


	public static ProductOverlayRunOptionReplicationHolder of(List<ProductOverlayAssetClassReplication> replicationList) {
		ProductOverlayRunOptionReplicationHolder holder = new ProductOverlayRunOptionReplicationHolder();
		holder.addAll(replicationList);
		return holder;
	}


	public static ProductOverlayRunOptionReplicationHolder of(ProductOverlayRunOptionReplicationHolder replicationHolder) {
		ProductOverlayRunOptionReplicationHolder holder = new ProductOverlayRunOptionReplicationHolder();
		holder.addAll(replicationHolder);
		return holder;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public Iterator<ProductOverlayAssetClassReplication> iterator() {
		return getReplicationList().iterator();
	}


	@Override
	public void forEach(Consumer<? super ProductOverlayAssetClassReplication> action) {
		getReplicationList().forEach(action);
	}


	public Stream<ProductOverlayAssetClassReplication> stream() {
		return getReplicationList().stream();
	}


	public void add(ProductOverlayAssetClassReplication replication) {
		if (replication != null) {
			getReplicationList().add(replication);
			this.calculatedValueCache.clear(); // clear cache upon changes
		}
	}


	public void addAll(List<ProductOverlayAssetClassReplication> replicationList) {
		CollectionUtils.getStream(replicationList)
				.filter(Objects::nonNull)
				.forEach(this::add);
	}


	public void addAll(ProductOverlayRunOptionReplicationHolder replicationHolder) {
		CollectionUtils.getStream(replicationHolder.getReplicationList())
				.filter(Objects::nonNull)
				.forEach(this::add);
	}


	public void clear() {
		getReplicationList().clear();
		this.calculatedValueCache.clear(); // clear cache upon changes
	}


	public void sort(Comparator<ProductOverlayAssetClassReplication> comparator) {
		getReplicationList().sort(comparator);
	}


	public boolean isEmpty() {
		return getReplicationList().isEmpty();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BigDecimal getQuantityActual() {
		return getOrComputeValue(QUANTITY_ACTUAL, repList -> CoreMathUtils.sumProperty(repList, ProductOverlayAssetClassReplication::getActualContractsAdjusted));
	}


	/**
	 * Returns the sum of {@link #getQuantityCurrent()} and {@link #getQuantityPending()}
	 */
	public BigDecimal getQuantity() {
		return MathUtils.add(getQuantityCurrent(), getQuantityPending());
	}


	public BigDecimal getQuantityPending() {
		return getOrComputeValue(QUANTITY_PENDING, repList -> CoreMathUtils.sumProperty(repList, ProductOverlayAssetClassReplication::getPendingContractsAdjusted));
	}


	public BigDecimal getQuantityCurrent() {
		return getOrComputeValue(QUANTITY_CURRENT, repList -> CoreMathUtils.sumProperty(repList, ProductOverlayAssetClassReplication::getCurrentContractsAdjusted));
	}


	public BigDecimal getExposureActual() {
		return getOrComputeValue(EXPOSURE_ACTUAL, repList -> CoreMathUtils.sumProperty(repList, ProductOverlayAssetClassReplication::getActualExposureAdjusted));
	}


	/**
	 * Returns the sum of {@link #getExposureCurrent()} and {@link #getExposurePending()}
	 */
	public BigDecimal getExposure() {
		return MathUtils.add(getExposureCurrent(), getExposurePending());
	}


	public BigDecimal getExposurePending() {
		return getOrComputeValue(EXPOSURE_PENDING, repList -> CoreMathUtils.sumProperty(repList, ProductOverlayAssetClassReplication::getPendingExposureAdjusted));
	}


	public BigDecimal getExposureCurrent() {
		return getOrComputeValue(EXPOSURE_CURRENT, repList -> CoreMathUtils.sumProperty(repList, ProductOverlayAssetClassReplication::getCurrentExposureAdjusted));
	}


	public BigDecimal getMarketValueActual() {
		return getMarketValue(MARKET_VALUE_ACTUAL, ProductOverlayAssetClassReplication::getSecurityPrice, ProductOverlayAssetClassReplication::getActualContractsAdjusted);
	}


	public BigDecimal getMarketValuePending() {
		return getMarketValue(MARKET_VALUE_PENDING, replication -> ObjectUtils.coalesce(replication.getTradeSecurityPrice(), replication.getSecurityPrice()), ProductOverlayAssetClassReplication::getPendingContractsAdjusted);
	}


	public BigDecimal getMarketValueCurrent() {
		return getMarketValue(MARKET_VALUE_CURRENT, replication -> ObjectUtils.coalesce(replication.getTradeSecurityPrice(), replication.getSecurityPrice()), ProductOverlayAssetClassReplication::getCurrentContractsAdjusted);
	}


	/**
	 * Used to display the expiring options information for each security in UI returns formatted string that js parses to create nicely formatted html table
	 * Example: showCurrent=true;showPending=true;symbol=1:2:3:4:...;symbol=1:2:3:4
	 * where the data after the symbol separated by : is Actual Contracts : Current Contracts : Pending Contracts : Actual Exposure : Current Exposure : Pending Exposure
	 * Note: Numbers are formatted as integers without commas - JS handles formatting so can show negative in red
	 * See RunTradeGroupDynamicOptionRollWindow.js : Clifton.product.overlay.trade.group.renderExpiringTooltip function for JS parsing
	 */
	public String getTooltipString() {
		if (CollectionUtils.isEmpty(getReplicationList())) {
			return null;
		}
		boolean showCurrent = false;
		boolean showPending = false;

		StringBuilder tooltip = new StringBuilder(100);
		for (ProductOverlayAssetClassReplication replication : getReplicationList()) {
			if (!MathUtils.isEqual(replication.getActualContractsAdjusted(), replication.getCurrentContractsAdjusted())) {
				showCurrent = true;
			}
			if (!MathUtils.isNullOrZero(replication.getPendingContractsAdjusted())) {
				showPending = true;
			}
			tooltip.append(replication.getSecurity().getSymbol());
			tooltip.append("=");
			appendAmount(replication.getActualContractsAdjusted(), tooltip);
			appendAmount(replication.getCurrentContractsAdjusted(), tooltip);
			appendAmount(replication.getPendingContractsAdjusted(), tooltip);
			appendAmount(replication.getActualExposureAdjusted(), tooltip);
			appendAmount(replication.getCurrentExposureAdjusted(), tooltip);
			appendAmount(replication.getPendingExposureAdjusted(), tooltip);
			tooltip.append(";");
		}

		tooltip.append("Total=");
		appendAmount(getQuantityActual(), tooltip);
		appendAmount(getQuantityCurrent(), tooltip);
		appendAmount(getQuantityPending(), tooltip);
		appendAmount(getExposureActual(), tooltip);
		appendAmount(getExposureCurrent(), tooltip);
		appendAmount(getExposurePending(), tooltip);
		tooltip.append(";");
		return "showCurrent=" + showCurrent + ";showPending=" + showPending + ";" + tooltip.toString();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private BigDecimal getOrComputeValue(String key, Function<List<ProductOverlayAssetClassReplication>, BigDecimal> valueCalculator) {
		return CollectionUtils.getValue(this.calculatedValueCache, key, () -> valueCalculator.apply(getReplicationList()));
	}


	/**
	 * Returns the total market value for the {@link ProductOverlayAssetClassReplication}s provided.
	 * Market value is calculated as (security price * security price multiplier * trade quantity).
	 */
	private BigDecimal getMarketValue(String key, Function<ProductOverlayAssetClassReplication, BigDecimal> replicationPriceFunction, Function<ProductOverlayAssetClassReplication, BigDecimal> replicationContractsFunction) {
		return getOrComputeValue(key, repList -> CoreMathUtils.sumProperty(repList, replication -> MathUtils.multiply(replicationContractsFunction.apply(replication), MathUtils.multiply(replication.getSecurity().getPriceMultiplier(), replicationPriceFunction.apply(replication)))));
	}


	private void appendAmount(BigDecimal amount, StringBuilder sb) {
		sb.append(CoreMathUtils.formatNumber(amount, "####"));
		sb.append(":");
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public List<ProductOverlayAssetClassReplication> getReplicationList() {
		return this.replicationList;
	}
}
