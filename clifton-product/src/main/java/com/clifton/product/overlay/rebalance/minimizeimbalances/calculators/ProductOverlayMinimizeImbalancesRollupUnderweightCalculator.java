package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import com.clifton.product.overlay.rebalance.ProductOverlayRebalance;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductOverlayMinimizeImbalancesRollupUnderweightCalculator</code> is an extension
 * of the {@link ProductOverlayMinimizeImbalancesCalculator} with support for traditional underweight
 * and underweight long only, however first processes at the rollup level for minimizing imbalances first
 * between "groups" of asset classes, then within those rollup asset classes
 *
 * @author manderson
 */
public class ProductOverlayMinimizeImbalancesRollupUnderweightCalculator extends ProductOverlayMinimizeImbalancesUnderweightCalculator {

	public static final String ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME = "Minimize Imbalances: Rollup Options";

	public static final String EXCLUDE_ROLLUP_ASSET_CLASS_COLUMN_NAME = "Exclude Rollup";
	public static final String APPLY_ROLLUP_PROPORTIONAL_ASSET_CLASS_COLUMN_NAME = "Apply Rollup To Children Proportionally";


	@Override
	public String getCustomColumnGroupName() {
		return ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME;
	}


	@Override
	public boolean calculateMinimizeImbalances(ProductOverlayRunConfig config) {
		BigDecimal totalMinimizeImbalancesCash = getTotalMinimizeImbalanceCash(config);

		// Processes the rollup map, recursive to support if there are rollups of rollups
		Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> clonedAssetClassMap = setupRollupAssetClasses(config, config.getOverlayAssetClassRollupList(),
				config.getOverlayAssetClassListExcludeRollups());
		Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap = processRollupAssetClass(config, clonedAssetClassMap, null, totalMinimizeImbalancesCash, false);

		// Apply Cash to Asset Classes from the map based on MinimizeImbalancesCashConfig setup
		return applyCashToAssetClasses(config, assetClassCashMap, true);
	}


	@Override
	public void calculateNewRebalanceCash(ProductOverlayRebalance bean) {
		List<ProductOverlayAssetClass> assetClassList = bean.getRebalanceAssetClassList();

		// Processes the rollup map, recursive to support if there are rollups of rollups
		Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> clonedAssetClassMap = setupRollupAssetClasses(null, bean.getRollupAssetClassList(), assetClassList);
		Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap = processRollupAssetClass(null, clonedAssetClassMap, null, null, true);

		for (ProductOverlayAssetClass portfolioAssetClass : assetClassList) {
			if (assetClassCashMap.containsKey(portfolioAssetClass.getAccountAssetClass())) {
				portfolioAssetClass.setNewRebalanceCash(assetClassCashMap.get(portfolioAssetClass.getAccountAssetClass()));
			}
			else {
				portfolioAssetClass.setNewRebalanceCash(BigDecimal.ZERO);
			}
		}
	}


	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////


	/**
	 * Do we include this asset class in our minimizing imbalances processing?
	 * By default, skip main cash asset class and any NON ROLLUP asset class without replications are skipped
	 */
	@Override
	protected boolean isProcessProductOverlayAssetClass(ProductOverlayRunConfig config, ProductOverlayAssetClass oac) {
		if (oac.getAccountAssetClass().getAssetClass().isMainCash()) {
			return false;
		}
		// Rollup - Since rollups cash will be reallocated to children
		// Unless it's flagged as skipped
		if (oac.isRollupAssetClass()) {
			return !isExcludeRollupAssetClass(config, oac.getAccountAssetClass());
		}
		// OR Replications Used - Since they are the only ones with an Overlay Target
		return oac.isReplicationUsed();
	}


	/**
	 * We need to clone the {@link ProductOverlayAssetClass} objects because we don't want to actually set rollup values
	 * But need what we have now in order to process minimize imbalances
	 *
	 * @param runConfig
	 */
	private Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> setupRollupAssetClasses(ProductOverlayRunConfig runConfig, List<ProductOverlayAssetClass> rollupAssetClassClassList,
	                                                                                           List<ProductOverlayAssetClass> childAssetClassList) {
		Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> clonedMap = new HashMap<>();

		for (ProductOverlayAssetClass oac : CollectionUtils.getIterable(rollupAssetClassClassList)) {
			clonedMap.put(oac.getAccountAssetClass(), BeanUtils.cloneBean(oac, false, false));
		}
		for (ProductOverlayAssetClass oac : CollectionUtils.getIterable(childAssetClassList)) {
			clonedMap.put(oac.getAccountAssetClass(), BeanUtils.cloneBean(oac, false, false));
		}

		// Process Rollup Calculations

		// Go through each non rollup first and if there's a parent, find it in rollup map and set it and set values
		for (ProductOverlayAssetClass childOAC : CollectionUtils.getIterable(childAssetClassList)) {
			InvestmentAccountAssetClass childAC = childOAC.getAccountAssetClass();
			if (childAC.getParent() != null) {
				// As long as not excluding the parent, then set it in the map
				if (!isExcludeRollupAssetClass(runConfig, childAC.getParent())) {
					ProductOverlayAssetClass child = clonedMap.get(childAC);
					ProductOverlayAssetClass parent = clonedMap.get(childAC.getParent());
					if (runConfig != null) {
						runConfig.addChildValuesToParentOverlayAssetClass(parent, child);
					}
					clonedMap.put(childAC, child);
					clonedMap.put(childAC.getParent(), parent);
				}
			}
		}

		// Now go through each rollup, and if there's a parent (rollup of a rollup) find it in rollup map and set it and set values
		for (ProductOverlayAssetClass childOAC : CollectionUtils.getIterable(rollupAssetClassClassList)) {
			InvestmentAccountAssetClass childAC = childOAC.getAccountAssetClass();
			if (childAC.getParent() != null) {
				// As long as not excluding the parent, then set it in the map
				if (!isExcludeRollupAssetClass(runConfig, childAC.getParent())) {
					ProductOverlayAssetClass child = clonedMap.get(childAC);
					ProductOverlayAssetClass parent = clonedMap.get(childAC.getParent());
					if (runConfig != null) {
						runConfig.addChildValuesToParentOverlayAssetClass(parent, child);
					}
					clonedMap.put(childAC, child);
					clonedMap.put(childAC.getParent(), parent);
				}
			}
		}

		return clonedMap;
	}


	private Map<InvestmentAccountAssetClass, BigDecimal> processRollupAssetClass(ProductOverlayRunConfig config, Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> clonedAssetClassMap,
	                                                                             InvestmentAccountAssetClass parent, BigDecimal cashToApply, boolean fullRebalance) {
		List<ProductOverlayAssetClass> list = new ArrayList<>();

		for (ProductOverlayAssetClass oac : CollectionUtils.getIterable(clonedAssetClassMap.values())) {
			// Only put in the list if going to use it - this is checked during calculation of minimize imbalances, but for proportional allocation makes it easier to determine % if list is already filtered
			if (!isProcessProductOverlayAssetClass(config, oac)) {
				continue;
			}
			// IF parent is null - starting at top level
			if (parent == null) {
				if ((oac.getAccountAssetClass().getParent() == null) || (isExcludeRollupAssetClass(config, oac.getAccountAssetClass().getParent()))) {
					list.add(oac);
				}
			}
			else {
				if (parent.equals(oac.getAccountAssetClass().getParent())) {
					list.add(oac);
				}
			}
		}

		Map<InvestmentAccountAssetClass, BigDecimal> result = new HashMap<>();
		Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap;
		if (parent != null && isApplyToChildrenProportionally(config, parent)) {
			//System.out.println("Apply to children for " + parent.getAssetClass().getName() + ": " + CoreMathUtils.formatNumberMoney(cashToApply));
			assetClassCashMap = allocateCashProportionally(list, cashToApply);
		}
		else {
			if (fullRebalance) {
				assetClassCashMap = calculateFullRebalanceImpl(parent, list, cashToApply);
			}
			else {
				assetClassCashMap = calculateMinimizeImbalancesImpl(config, list, cashToApply);
			}
		}

		// Now we have cash to apply at the rollup level, now we need to minimize imbalances within the rollups
		for (Map.Entry<InvestmentAccountAssetClass, BigDecimal> investmentAccountAssetClassBigDecimalEntry1 : assetClassCashMap.entrySet()) {
			// If not a rollup, just add cash allocated to map
			if (!(investmentAccountAssetClassBigDecimalEntry1.getKey()).isRollupAssetClass()) {
				result.put(investmentAccountAssetClassBigDecimalEntry1.getKey(), investmentAccountAssetClassBigDecimalEntry1.getValue());
			}
			else {
				Map<InvestmentAccountAssetClass, BigDecimal> assetClassResult = processRollupAssetClass(config, clonedAssetClassMap, investmentAccountAssetClassBigDecimalEntry1.getKey(), investmentAccountAssetClassBigDecimalEntry1.getValue(), fullRebalance);
				for (Map.Entry<InvestmentAccountAssetClass, BigDecimal> investmentAccountAssetClassBigDecimalEntry : assetClassResult.entrySet()) {
					result.put(investmentAccountAssetClassBigDecimalEntry.getKey(), investmentAccountAssetClassBigDecimalEntry.getValue());
				}
			}
		}
		return result;
	}


	private Map<InvestmentAccountAssetClass, BigDecimal> allocateCashProportionally(List<ProductOverlayAssetClass> list, BigDecimal totalMinimizeImbalancesCash) {

		Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap = new HashMap<>();

		BigDecimal targetTotal = CoreMathUtils.sumProperty(list, productOverlayAssetClass -> productOverlayAssetClass.getAccountAssetClass() == null ? null : productOverlayAssetClass.getAccountAssetClass().getAssetClassPercentage());
		BigDecimal remainingCash = totalMinimizeImbalancesCash;

		// Sort in asc order so last one/biggest one gets rounding different
		List<ProductOverlayAssetClass> sortedList = BeanUtils.sortWithFunction(list, productOverlayAssetClass -> productOverlayAssetClass.getAccountAssetClass().getAssetClassPercentage(), true);
		for (int i = 0; i < CollectionUtils.getSize(sortedList); i++) {
			ProductOverlayAssetClass oac = sortedList.get(i);
			if (i == sortedList.size() - 1) {
				assetClassCashMap.put(oac.getAccountAssetClass(), remainingCash);
				//System.out.println("Allocate to [" + oac.getLabel() + "] " + CoreMathUtils.formatNumberMoney(remainingCash) + " (Remaining Amount)");
			}
			else {
				BigDecimal percentage = CoreMathUtils.getPercentValue(oac.getAccountAssetClass().getAssetClassPercentage(), targetTotal, true);
				BigDecimal amount = MathUtils.round(MathUtils.getPercentageOf(percentage, totalMinimizeImbalancesCash, true), 2);
				//System.out.println(oac.getAccountAssetClass().getLabel() + " gets " + CoreMathUtils.formatNumberMoney(percentage) + " of "
				//       + CoreMathUtils.formatNumberMoney(totalMinimizeImbalancesCash) + " = " + CoreMathUtils.formatNumberMoney(amount));
				//System.out.println("Allocate to [" + oac.getLabel() + "] " + CoreMathUtils.formatNumberMoney(percentage) + "% = " + CoreMathUtils.formatNumberMoney(amount));
				assetClassCashMap.put(oac.getAccountAssetClass(), amount);
				remainingCash = MathUtils.subtract(remainingCash, amount);
			}
		}
		return assetClassCashMap;
	}


	private Map<InvestmentAccountAssetClass, BigDecimal> calculateFullRebalanceImpl(InvestmentAccountAssetClass parent, List<ProductOverlayAssetClass> assetClassList, BigDecimal cashToApply) {
		Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap = new HashMap<>();
		if (!CollectionUtils.isEmpty(assetClassList)) {
			if (parent == null) {
				for (ProductOverlayAssetClass oac : assetClassList) {
					assetClassCashMap.put(oac.getAccountAssetClass(), calculateNewRebalanceCashForAssetClass(oac));
				}
			}
			else {
				if (assetClassList.size() == 1) {
					assetClassCashMap.put(assetClassList.get(0).getAccountAssetClass(), cashToApply);
				}
				else {
					BigDecimal childrenTotal = BigDecimal.ZERO;
					for (ProductOverlayAssetClass oac : assetClassList) {
						childrenTotal = MathUtils.add(childrenTotal, calculateNewRebalanceCashForAssetClass(oac));
					}
					// Apply Proportionally Based on Children Weighted
					// This should probably always be the same, but for cases where one of the children has no replication, their amount needs to be split into the others
					InvestmentAccountAssetClass largest = null; // track the largest one for rounding
					BigDecimal largestAmount = null;

					for (ProductOverlayAssetClass oac : assetClassList) {
						BigDecimal percentage = CoreMathUtils.getPercentValue(calculateNewRebalanceCashForAssetClass(oac), childrenTotal);
						BigDecimal amount = MathUtils.round(MathUtils.getPercentageOf(percentage, cashToApply), 2);
						//System.out.println(oac.getAccountAssetClass().getLabel() + " gets " + CoreMathUtils.formatNumberMoney(percentage) + " of "
						//        + CoreMathUtils.formatNumberMoney(cashToApply) + " = " + CoreMathUtils.formatNumberMoney(amount));
						if (largestAmount == null || MathUtils.isGreaterThan(MathUtils.abs(amount), largestAmount)) {
							largest = oac.getAccountAssetClass();
							largestAmount = MathUtils.abs(amount);
						}
						assetClassCashMap.put(oac.getAccountAssetClass(), amount);
					}
					BigDecimal sum = CoreMathUtils.sum(assetClassCashMap.values());
					assetClassCashMap.put(largest, MathUtils.add(assetClassCashMap.get(largest), MathUtils.subtract(cashToApply, sum)));
				}
			}
		}
		return assetClassCashMap;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                   Helper Methods                       //////////
	////////////////////////////////////////////////////////////////////////////


	private boolean isExcludeRollupAssetClass(ProductOverlayRunConfig config, InvestmentAccountAssetClass iac) {
		return (Boolean) getInvestmentAccountAssetClassCustomFieldValue(config, iac, EXCLUDE_ROLLUP_ASSET_CLASS_COLUMN_NAME, false);
	}


	private boolean isApplyToChildrenProportionally(ProductOverlayRunConfig config, InvestmentAccountAssetClass iac) {
		return (Boolean) getInvestmentAccountAssetClassCustomFieldValue(config, iac, APPLY_ROLLUP_PROPORTIONAL_ASSET_CLASS_COLUMN_NAME, false);
	}
}
