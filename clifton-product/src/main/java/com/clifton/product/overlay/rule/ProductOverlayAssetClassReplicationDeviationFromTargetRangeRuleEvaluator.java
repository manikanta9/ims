package com.clifton.product.overlay.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.rule.evaluator.BaseRangeRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is used by several system beans to validate currency balances in the clients base currency to ensure
 * individual unrealized gain/loss as well as total unrealized gain/loss does not fall outside of the specified threshold for an account
 *
 * @author stevenf on 8/21/2015.
 */
public class ProductOverlayAssetClassReplicationDeviationFromTargetRangeRuleEvaluator extends BaseRangeRuleEvaluator<PortfolioRun, ProductOverlayRuleEvaluatorContext> {

	//Balance Range Types
	private static final String EXPOSURE_DEVIATION_AMOUNT = "ExposureDeviationAmount";
	private static final String EXPOSURE_DEVIATION_PERCENT = "ExposureDeviationPercent";

	private ProductOverlayService productOverlayService;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, ProductOverlayRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		Map<Integer, BigDecimal> assetClassOverlayTargetMap = new HashMap<>();
		Map<Integer, ProductOverlayAssetClass> overlayAssetClassMap = new HashMap<>();
		String balanceRangeType = getRangeType();
		for (ProductOverlayAssetClassReplication assetClassReplication : CollectionUtils.getIterable(context.getReplicationList(run))) {
			Integer assetClassId = assetClassReplication.getOverlayAssetClass().getAccountAssetClass().getId();
			EntityConfig entityConfig = ruleConfig.getEntityConfig(MathUtils.getNumberAsLong(assetClassId));
			if (entityConfig != null && !entityConfig.isExcluded()) {
				if (!assetClassReplication.isMatchingReplication()) {
					BigDecimal targetExposure = assetClassOverlayTargetMap.get(assetClassId);
					if (targetExposure == null) {
						targetExposure = BigDecimal.ZERO;
					}
					assetClassOverlayTargetMap.put(assetClassId, MathUtils.add(targetExposure, assetClassReplication.getTargetExposure()));
					overlayAssetClassMap.put(assetClassId, assetClassReplication.getOverlayAssetClass());
				}
			}
		}
		for (Map.Entry<Integer, BigDecimal> assetClassOverlayTargetEntry : assetClassOverlayTargetMap.entrySet()) {
			EntityConfig entityConfig = ruleConfig.getEntityConfig(MathUtils.getNumberAsLong(assetClassOverlayTargetEntry.getKey()));

			ProductOverlayAssetClass overlayAssetClass = overlayAssetClassMap.get(assetClassOverlayTargetEntry.getKey());
			BigDecimal overlayExposureTarget = assetClassOverlayTargetEntry.getValue();
			BigDecimal overlayExposureDifference = overlayExposureTarget.subtract(overlayAssetClass.getOverlayExposure());

			BigDecimal rangeValue = null;
			if (balanceRangeType.equals(EXPOSURE_DEVIATION_AMOUNT)) {
				rangeValue = overlayExposureDifference;
			}
			else if (balanceRangeType.equals(EXPOSURE_DEVIATION_PERCENT)) {
				rangeValue = CoreMathUtils.getPercentValue(overlayExposureDifference, overlayExposureTarget, true);
			}
			Map<String, Object> templateValues = new HashMap<>();
			if (RuleEvaluatorUtils.isEntityConfigRangeViolated(rangeValue, entityConfig, templateValues)) {
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), MathUtils.getNumberAsLong(assetClassOverlayTargetEntry.getKey()), BeanUtils.getIdentityAsLong(overlayAssetClass.getAccountAssetClass()), null, templateValues));
			}
		}
		return ruleViolationList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}
}
