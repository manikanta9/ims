package com.clifton.product.overlay;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.product.overlay.search.ProductOverlayAssetClassSearchForm;
import com.clifton.product.overlay.search.ProductOverlayManagerAccountSearchForm;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>ProductOverlayService</code> defines the basic get, save, delete methods for
 * ProductOverlay beans.
 *
 * @author Mary Anderson
 */
public interface ProductOverlayService {

	////////////////////////////////////////////////////////////////////////////
	///////         Product Overlay Run Business Methods               /////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Deletes all processing related info that was created for this run.
	 */
	@DoNotAddRequestMapping
	public void deleteProductOverlayRunProcessing(int id);


	/**
	 * Checks if there exists at least 1 (because of sub-accounts there could be 2) main run for an account for the given balance date
	 * uses {@link ProductOverlayAssetClass} to check coalesce (asset class account, run account)
	 */
	@DoNotAddRequestMapping
	public boolean isProductOverlayMainRunExistsForAccount(int clientInvestmentAccountId, Date balanceDate);

	////////////////////////////////////////////////////////////////////////////
	/////  Product Overlay Manager Account Rebalance Business Methods      /////
	////////////////////////////////////////////////////////////////////////////


	public ProductOverlayManagerAccountRebalance getProductOverlayManagerAccountRebalance(int id);


	public List<ProductOverlayManagerAccountRebalance> getProductOverlayManagerAccountRebalanceListByRun(int runId);


	public void saveProductOverlayManagerAccountRebalanceListByRun(int runId, List<ProductOverlayManagerAccountRebalance> saveList);

	////////////////////////////////////////////////////////////////////////////
	/////    Product Overlay Manager Account Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	public ProductOverlayManagerAccount getProductOverlayManagerAccount(int id);


	public List<ProductOverlayManagerAccount> getProductOverlayManagerAccountListByRun(int runId);


	public List<ProductOverlayManagerAccount> getProductOverlayManagerAccountList(ProductOverlayManagerAccountSearchForm searchForm);


	public void saveProductOverlayManagerAccountListByRun(int runId, BigDecimal totalCash, List<ProductOverlayManagerAccount> saveList);

	////////////////////////////////////////////////////////////////////////////
	////////     Product Overlay Asset Class Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	public ProductOverlayAssetClass getProductOverlayAssetClass(int id);


	public List<ProductOverlayAssetClass> getProductOverlayAssetClassListByRun(int runId);


	/**
	 * Note: runId on the Search Form is REQUIRED
	 *
	 * @param populateCashAdjustments - Also populates cash adjustment lists for each asset class so can see "adjusted" cash bucket values
	 */
	public List<ProductOverlayAssetClass> getProductOverlayAssetClassList(ProductOverlayAssetClassSearchForm searchForm, boolean populateCashAdjustments);


	public void saveProductOverlayAssetClassListByRun(int runId, BigDecimal totalPortfolioValue, List<ProductOverlayAssetClass> saveList);

	////////////////////////////////////////////////////////////////////////////
	////  Product Overlay Asset Class Cash Adjustment Business Methods     /////
	////////////////////////////////////////////////////////////////////////////


	public ProductOverlayAssetClassCashAdjustment getProductOverlayAssetClassCashAdjustment(int id);


	public List<ProductOverlayAssetClassCashAdjustment> getProductOverlayAssetClassCashAdjustmentListByRun(int runId);


	public ProductOverlayAssetClassCashAdjustment saveProductOverlayAssetClassCashAdjustment(ProductOverlayAssetClassCashAdjustment bean);


	/**
	 * Bypasses validation that ensures end result after cash adjustment doesn't move Manager, Fund, or Transition Cash to negative
	 */
	public ProductOverlayAssetClassCashAdjustment saveProductOverlayAssetClassCashAdjustmentAllowNegative(ProductOverlayAssetClassCashAdjustment bean);


	public void deleteProductOverlayAssetClassCashAdjustment(int id);


	/**
	 * Bypasses validation that ensures end result after cash adjustment doesn't move Manager, Fund, or Transition Cash to negative
	 */
	@SecureMethod(dtoClass = ProductOverlayAssetClassCashAdjustment.class)
	public void deleteProductOverlayAssetClassCashAdjustmentAllowNegative(int id);

	////////////////////////////////////////////////////////////////////////////
	//////   Product Overlay Asset Class Replication Business Methods     //////
	////////////////////////////////////////////////////////////////////////////


	public ProductOverlayAssetClassReplication getProductOverlayAssetClassReplication(int id);


	public List<ProductOverlayAssetClassReplication> getProductOverlayAssetClassReplicationListByRun(int runId);


	public List<ProductOverlayAssetClassReplication> getProductOverlayAssetClassReplicationListByOverlayAssetClass(int overlayAssetClassId);


	public List<ProductOverlayAssetClassReplication> getProductOverlayAssetClassReplicationList(final Integer runId, final Integer overlayAssetClassId);


	/**
	 * NOTE FOR ACCOUNTS WITH SUB-ACCOUNTS - IF THERE ARE RUNS AT BOTH LEVELS, IT WILL THE ONE FOUND FOR A SPECIFIC DATE AT THE MAIN LEVEL OTHERWISE THE SUB-ACCOUNT LEVEL.
	 * <p>
	 * This method actually performs two SQL look ups to prevent OR clause across tables which causes major performance issues.
	 */
	public List<ProductOverlayAssetClassReplication> getProductOverlayAssetClassReplicationListByAccountMainRun(final int investmentAccountId, final Date startDate, final Date endDate,
	                                                                                                            final boolean excludeMatchingReplications);


	public ProductOverlayAssetClassReplication saveProductOverlayAssetClassReplication(ProductOverlayAssetClassReplication bean);


	public void saveProductOverlayAssetClassReplicationList(List<ProductOverlayAssetClassReplication> saveList);


	/**
	 * Gets original list for run and does insert/update/delete where necessary
	 */
	public void saveProductOverlayAssetClassReplicationListByRun(int runId, BigDecimal overlayTargetTotal, BigDecimal overlayExposureTotal, BigDecimal mispricingTotal,
	                                                             List<ProductOverlayAssetClassReplication> saveList);
}
