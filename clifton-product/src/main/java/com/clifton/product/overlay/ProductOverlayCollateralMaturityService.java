package com.clifton.product.overlay;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.portfolio.run.PortfolioRun;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * @author terrys
 */
public interface ProductOverlayCollateralMaturityService {

	/**
	 * Loads collateral positions applicable for the {@link PortfolioRun} of the provided command.
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public List<ProductOverlayCollateralMaturity> getProductOverlayCollateralMaturityList(ProductOverlayCollateralMaturityListCommand collateralMaturityListCommand);


	/**
	 * Creates trades from provided {@link ProductOverlayCollateralMaturity}s of the command.
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	@ModelAttribute("data")
	public Status generateProductOverlayCollateralMaturityTradeList(ProductOverlayCollateralMaturityListCommand collateralMaturityListCommand);
}
