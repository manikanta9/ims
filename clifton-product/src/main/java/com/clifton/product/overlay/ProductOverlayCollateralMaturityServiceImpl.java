package com.clifton.product.overlay;

import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.AccountingBalanceUtils;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeListActionCommand;
import com.clifton.trade.TradeManagementService;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.workflow.definition.WorkflowStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author terrys
 */
@Service
public class ProductOverlayCollateralMaturityServiceImpl implements ProductOverlayCollateralMaturityService {

	private AccountingBalanceService accountingBalanceService;

	private CollateralBalanceService collateralBalanceService;

	private InvestmentAccountService investmentAccountService;

	private InvestmentCalculator investmentCalculator;

	private MarketDataRetriever marketDataRetriever;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private PortfolioRunService portfolioRunService;
	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;

	private SecurityUserService securityUserService;

	private TradeDestinationService tradeDestinationService;
	private TradeManagementService tradeManagementService;
	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public List<ProductOverlayCollateralMaturity> getProductOverlayCollateralMaturityList(ProductOverlayCollateralMaturityListCommand collateralMaturityListCommand) {
		PortfolioRun portfolioRun = hydratePortfolioRunForCommand(collateralMaturityListCommand);
		Set<Integer> clientInvestmentAccountIdSet = CollectionUtils.getStream(getClientSubAccountList(collateralMaturityListCommand.getPortfolioRun()))
				.map(InvestmentAccount::getId).collect(Collectors.toSet());
		clientInvestmentAccountIdSet.add(collateralMaturityListCommand.getPortfolioRun().getClientInvestmentAccount().getId());
		Integer[] clientInvestmentAccountIds = clientInvestmentAccountIdSet.toArray(new Integer[0]);

		Map<InvestmentAccount, List<AccountingBalance>> accountingBalanceMap = getCollateralAccountingBalanceListMap(collateralMaturityListCommand, clientInvestmentAccountIds);

		if (CollectionUtils.isEmpty(accountingBalanceMap)) {
			return Collections.emptyList();
		}

		// A map to look up the holdingAccountToTargetCollateralMap for a given client account / subAccount
		Map<InvestmentAccount, Map<InvestmentAccount, BigDecimal>> holdingAccountToTargetCollateralMapByClientAccountMap = getHoldingAccountTargetCollateralMapByClientAccountMap(collateralMaturityListCommand, clientInvestmentAccountIds);
		List<ProductOverlayCollateralMaturity> productOverlayCollateralMaturityList = new ArrayList<>();

		for (Map.Entry<InvestmentAccount, List<AccountingBalance>> accountingBalanceListEntry : CollectionUtils.getIterable(accountingBalanceMap.entrySet())) {
			Map<InvestmentAccount, BigDecimal> holdingAccountToTargetCollateralMap = holdingAccountToTargetCollateralMapByClientAccountMap.computeIfAbsent(accountingBalanceListEntry.getKey(), key -> Collections.emptyMap());
			List<ProductOverlayCollateralMaturity> productOverlayCollateralMaturitySubList = accountingBalanceListEntry.getValue().stream()
					.map(ab -> createProductOverlayCollateralMaturity(portfolioRun, ab, holdingAccountToTargetCollateralMap))
					.sorted((item1, item2) -> DateUtils.compare(item1.getSecurity().getEndDate(), item2.getSecurity().getEndDate(), false))
					.collect(Collectors.toList());
			productOverlayCollateralMaturityList.addAll(productOverlayCollateralMaturitySubList);
		}

		return productOverlayCollateralMaturityList;
	}


	@Override
	@Transactional
	public Status generateProductOverlayCollateralMaturityTradeList(ProductOverlayCollateralMaturityListCommand collateralMaturityListCommand) {
		Status status = new Status();
		if (!CollectionUtils.isEmpty(collateralMaturityListCommand.getCollateralMaturityList())) {
			// Initial trades are created for the current user for auditing
			List<Trade> tradeList = createTradeListForCollateralMaturityCommand(collateralMaturityListCommand);

			if (!CollectionUtils.isEmpty(tradeList) && collateralMaturityListCommand.getTradeAssignee() != null) {
				// reassign trades to new assignee
				return reassignTradeListToTrader(tradeList, collateralMaturityListCommand.getTradeAssignee());
			}
		}
		return status;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private PortfolioRun hydratePortfolioRunForCommand(ProductOverlayCollateralMaturityListCommand collateralMaturityListCommand) {
		Integer portfolioRunId = collateralMaturityListCommand.getPortfolioRun() == null ? null : collateralMaturityListCommand.getPortfolioRun().getId();
		ValidationUtils.assertNotNull(portfolioRunId, "The Portfolio run identifier is required.");
		PortfolioRun portfolioRun = getPortfolioRunService().getPortfolioRun(portfolioRunId);
		ValidationUtils.assertNotNull(portfolioRun, "The portfolio run cannot be found " + portfolioRunId);
		collateralMaturityListCommand.setPortfolioRun(portfolioRun);
		return portfolioRun;
	}


	private Map<InvestmentAccount, List<AccountingBalance>> getCollateralAccountingBalanceListMap(ProductOverlayCollateralMaturityListCommand collateralMaturityListCommand, Integer[] clientInvestmentAccountIds) {
		AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onTransactionDate(collateralMaturityListCommand.getPortfolioRun().getBalanceDate());
		searchForm.setClientInvestmentAccountIds(clientInvestmentAccountIds);
		searchForm.setAccountingAccountTypes(new String[]{AccountingAccountType.ASSET, AccountingAccountType.LIABILITY});
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS_EXCLUDE_RECEIVABLE_COLLATERAL);
		List<AccountingBalance> currentBalanceList = getAccountingBalanceService().getAccountingBalanceList(searchForm);

		List<AccountingBalance> pendingBalanceList = getPendingCollateralBalanceList(clientInvestmentAccountIds);
		if (!CollectionUtils.isEmpty(pendingBalanceList)) {
			currentBalanceList.addAll(pendingBalanceList);
			currentBalanceList = AccountingBalanceUtils.mergeAccountingBalanceList(currentBalanceList, false);
		}

		// filter to physical securities
		currentBalanceList = CollectionUtils.getFiltered(currentBalanceList, balance -> InvestmentUtils.isPhysicalSecurity(balance.getInvestmentSecurity()));

		return BeanUtils.getBeansMap(currentBalanceList, AccountingBalance::getClientInvestmentAccount);
	}


	private List<InvestmentAccount> getClientSubAccountList(PortfolioRun run) {
		if (run != null) {
			InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
			searchForm.setMainAccountId(run.getClientInvestmentAccount().getId());
			searchForm.setMainPurpose(InvestmentAccountRelationshipPurpose.CLIFTON_SUB_ACCOUNT_PURPOSE_NAME);
			searchForm.setMainPurposeActiveOnDate(run.getBalanceDate());
			searchForm.setWorkflowStatusName(WorkflowStatus.STATUS_ACTIVE);
			return getInvestmentAccountService().getInvestmentAccountList(searchForm);
		}
		return null;
	}


	private List<AccountingBalance> getPendingCollateralBalanceList(Integer[] clientInvestmentAccountIds) {
		// Get pending activity for transaction date of tomorrow for pending activity; tomorrow will pick up trades entered after market close today.
		AccountingBalanceSearchForm pendingSearchForm = AccountingBalanceSearchForm.onTransactionDate(DateUtils.addDays(new Date(), 1));
		pendingSearchForm.setClientInvestmentAccountIds(clientInvestmentAccountIds);
		pendingSearchForm.setAccountingAccountTypes(new String[]{AccountingAccountType.ASSET, AccountingAccountType.LIABILITY});
		pendingSearchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS_EXCLUDE_RECEIVABLE_COLLATERAL);
		pendingSearchForm.setPendingActivityRequest(PendingActivityRequests.POSITIONS_WITH_PREVIEW);
		pendingSearchForm.setPendingOnly(true);
		return getAccountingBalanceService().getAccountingBalanceList(pendingSearchForm);
	}


	private ProductOverlayCollateralMaturity createProductOverlayCollateralMaturity(PortfolioRun portfolioRun, AccountingBalance accountingBalance, Map<InvestmentAccount, BigDecimal> holdingAccountToTargetCollateralMap) {
		ProductOverlayCollateralMaturity result = ProductOverlayCollateralMaturity.of(accountingBalance);
		Date processDate = portfolioRun.getBalanceDate();

		if (accountingBalance.getAccountingAccount().isPosition()) {
			result.setSecurityPrice(getMarketDataRetriever().getPriceFlexible(result.getSecurity(), processDate, false));
			if (InvestmentUtils.isSecurityOfType(accountingBalance.getInvestmentSecurity(), InvestmentType.BONDS)) {
				result.setDuration(getMarketDataRetriever().getDurationFlexible(result.getSecurity(), processDate, false));

				//TODO milestone 4
				result.setYield(null);
			}
		}
		result.setTargetExposure(holdingAccountToTargetCollateralMap.get(result.getHoldingAccount()));

		populateCollateralMaturityWithTradeDefaults(portfolioRun, result, accountingBalance.getAccountingAccount().isCollateral());

		return result;
	}


	private ProductOverlayCollateralMaturity populateCollateralMaturityWithTradeDefaults(PortfolioRun portfolioRun, ProductOverlayCollateralMaturity collateralMaturity, boolean collateralTrade) {
		Trade trade = new Trade();
		trade.setCollateralTrade(collateralTrade);
		collateralMaturity.setTrade(trade);

		if (portfolioRun.isTradingAllowed()) {
			// default destination and broker for grid
			trade.setClientInvestmentAccount(collateralMaturity.getClientAccount());
			trade.setHoldingInvestmentAccount(collateralMaturity.getHoldingAccount());
			trade.setInvestmentSecurity(collateralMaturity.getSecurity());
			trade.setTradeDate(portfolioRun.getBalanceDate());
			populateTradeBrokerAndDestination(trade, null);
		}

		return collateralMaturity;
	}


	//Creates and returns a Map for HoldingAccountTargetCollateral for a given keyed on a client investment account or sub-account.
	private Map<InvestmentAccount, Map<InvestmentAccount, BigDecimal>> getHoldingAccountTargetCollateralMapByClientAccountMap(ProductOverlayCollateralMaturityListCommand collateralMaturityListCommand, Integer[] clientInvestmentAccountIds) {
		PortfolioRun portfolioRun = collateralMaturityListCommand.getPortfolioRun();
		Map<InvestmentAccount, Map<Boolean, BigDecimal>> holdingAccountCollateralBalanceTargetMap = new HashMap<>();

		CollateralBalanceSearchForm searchForm = new CollateralBalanceSearchForm();
		searchForm.setClientInvestmentAccountIds(clientInvestmentAccountIds);
		searchForm.setBalanceDate(portfolioRun.getBalanceDate());
		List<CollateralBalance> collateralBalanceList = getCollateralBalanceService().getCollateralBalanceList(searchForm);
		Map<InvestmentAccount, List<CollateralBalance>> collateralBalanceListMap = BeanUtils.getBeansMap(collateralBalanceList, CollateralBalance::getClientInvestmentAccount);

		Map<InvestmentAccount, Map<InvestmentAccount, BigDecimal>> holdingAccountTargetCollateralMapByClientAccountMap = new HashMap<>();

		// Iterate over the collateral balance lists for each subAccount and portfolioRun client account, and create a holdingAccountTargetCollateralMap for each client account,
		// then add this dictionary to a map so it can be retrieved by client account.
		for (Map.Entry<InvestmentAccount, List<CollateralBalance>> collateralBalanceListMapEntry : CollectionUtils.getIterable(collateralBalanceListMap.entrySet())) {
			for (CollateralBalance collateralBalance : CollectionUtils.getIterable(collateralBalanceListMapEntry.getValue())) {
				holdingAccountCollateralBalanceTargetMap.computeIfAbsent(collateralBalance.getHoldingInvestmentAccount(), holdingAccount -> new HashMap<>())
						.compute(collateralBalance.getParent() == null, (parentNull, current) -> {
							BigDecimal exchangeRate = BigDecimal.ONE;
							if (collateralBalance.getCollateralCurrency() != null) {
								exchangeRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forHoldingAccount(collateralBalance.getHoldingInvestmentAccount().toHoldingAccount(),
										collateralBalance.getCollateralCurrency().getSymbol(), portfolioRun.getClientInvestmentAccount().getBaseCurrency().getSymbol(), portfolioRun.getBalanceDate()));
							}
							BigDecimal collateralBalanceBalance = MathUtils.multiply(collateralBalance.getCoalesceExternalCounterpartyCollateralRequirement(), exchangeRate, 2);
							return current == null ? collateralBalanceBalance : MathUtils.add(current, collateralBalanceBalance);
						});
			}

			BigDecimal excessCollateralPercent = (BigDecimal) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(collateralBalanceListMapEntry.getKey(), InvestmentAccount.CLIENT_ACCOUNT_COLLATERAL_EXCESS_TARGET_PERCENT_CUSTOM_COLUMN);

			Map<InvestmentAccount, BigDecimal> collateralBalanceTargetMap = holdingAccountCollateralBalanceTargetMap.entrySet().stream()
					.collect(Collectors.toMap(Map.Entry::getKey, entry -> {
						BigDecimal targetCollateral = entry.getValue().get(Boolean.TRUE);
						if (MathUtils.isNullOrZero(targetCollateral)) {
							targetCollateral = entry.getValue().get(Boolean.FALSE);
							if (MathUtils.isNullOrZero(targetCollateral)) {
								targetCollateral = BigDecimal.ZERO;
							}
						}

						return MathUtils.add(targetCollateral, MathUtils.getPercentageOf(excessCollateralPercent, targetCollateral, true));
					}));
			holdingAccountTargetCollateralMapByClientAccountMap.put(collateralBalanceListMapEntry.getKey(), collateralBalanceTargetMap);
		}

		return holdingAccountTargetCollateralMapByClientAccountMap;
	}


	private List<Trade> createTradeListForCollateralMaturityCommand(ProductOverlayCollateralMaturityListCommand collateralMaturityListCommand) {
		PortfolioRun portfolioRun = hydratePortfolioRunForCommand(collateralMaturityListCommand);

		SecurityUser trader = getSecurityUserService().getSecurityUserCurrent();

		List<Trade> tradeList = collateralMaturityListCommand.getCollateralMaturityList().stream()
				.filter(collateralMaturity -> !MathUtils.isNullOrZero(collateralMaturity.getBuy()) || !MathUtils.isNullOrZero(collateralMaturity.getSell()))
				.map(collateralMaturity -> populateTradeForCollateralMaturity(portfolioRun, collateralMaturity, trader, collateralMaturityListCommand.getTradeDate()))
				.filter(Objects::nonNull)
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(tradeList)) {
			return Collections.emptyList();
		}

		return tradeList.stream()
				.map(getTradeService()::saveTrade)
				.collect(Collectors.toList());
	}


	private Status reassignTradeListToTrader(List<Trade> tradeList, SecurityUser trader) {
		Status status = new Status();
		// Update trader user on trade if different. All trades in the list will have the same trader since they were just created.
		Trade firstTrade = CollectionUtils.getFirstElement(tradeList);
		if (firstTrade != null && !CompareUtils.isEqual(firstTrade.getTraderUser(), trader)) {
			TradeListActionCommand reassignmentCommand = TradeListActionCommand.forReassignWithAssigneeAndTradeList(trader, tradeList);
			status = getTradeManagementService().executeTradeListActionCommand(reassignmentCommand);
		}

		return status;
	}


	private Trade populateTradeForCollateralMaturity(PortfolioRun portfolioRun, ProductOverlayCollateralMaturity collateralMaturity, SecurityUser trader, Date tradeDate) {
		ValidationUtils.assertNotNull(collateralMaturity.getSecurity(), "A Security is required to create a trade.");
		String prefix = "Trade Entry for Security [" + collateralMaturity.getSecurity().getSymbol() + "]"; // i.e. "Trade entry for Security [xyz]"

		// Perform Validation - BUY/SELL MUTUALLY EXCLUSIVE
		// If BUY Present
		if (!MathUtils.isNullOrZero(collateralMaturity.getBuy())) {
			// Make sure no SELL
			ValidationUtils.assertTrue(MathUtils.isNullOrZero(collateralMaturity.getSell()), prefix + " has buy and sell trades entered.  Please enter one or the other, but not both.");
		}
		// ELSE if no SELL, then NO Trades
		else if (MathUtils.isNullOrZero(collateralMaturity.getSell())) {
			return null;
		}

		// If trade date is explicitly set on the screen, otherwise get trade date based on when exchange is open
		Date securityTradeDate = tradeDate != null ? tradeDate : getInvestmentCalculator().calculateTradeDate(collateralMaturity.getSecurity(), null);
		ValidationUtils.assertTrue(
				DateUtils.isDateBetween(securityTradeDate, collateralMaturity.getSecurity().getStartDate(), collateralMaturity.getSecurity().getEndDate(), false),
				prefix + " This security is not active on trade date [" + DateUtils.fromDateShort(securityTradeDate) + "] and cannot be traded on at this time.  Security End Date: "
						+ DateUtils.fromDateShort(collateralMaturity.getSecurity().getEndDate()));

		Trade trade = collateralMaturity.getTrade() == null ? new Trade() : collateralMaturity.getTrade();
		// Trade Properties
		InvestmentAccount clientInvestmentAccount = ObjectUtils.coalesce(collateralMaturity.getClientAccount(), portfolioRun.getClientInvestmentAccount());
		trade.setClientInvestmentAccount(clientInvestmentAccount);
		trade.setHoldingInvestmentAccount(collateralMaturity.getHoldingAccount());
		trade.setTradeDate(securityTradeDate);
		trade.setTraderUser(trader);
		trade.setExpectedUnitPrice(collateralMaturity.getSecurityPrice());

		trade.setInvestmentSecurity(collateralMaturity.getSecurity());
		trade.setPayingSecurity(InvestmentUtils.getSettlementCurrency(collateralMaturity.getSecurity()));
		trade.setSettlementDate(getInvestmentCalculator().calculateSettlementDate(trade.getInvestmentSecurity(), trade.getSettlementCurrency(), trade.getTradeDate()));

		if (MathUtils.isNullOrZero(collateralMaturity.getBuy())) {
			trade.setBuy(false);
			trade.setQuantityIntended(collateralMaturity.getSell());
		}
		else {
			trade.setBuy(true);
			trade.setQuantityIntended(collateralMaturity.getBuy());
		}

		populateTradeBrokerAndDestination(trade, prefix);

		return trade;
	}


	private void populateTradeBrokerAndDestination(Trade trade, String validationMessagePrefix) {
		if (trade.getInvestmentSecurity() != null) {
			// Set Trade Type based on the Security (based on investment type and optionally group if more than one, i.e. swaps and 3 trade types)
			TradeType type = getTradeService().getTradeTypeForSecurity(trade.getInvestmentSecurity().getId());
			// Removed NULL validation since done within the getTradeTypeForSecurity method
			trade.setTradeType(type);
		}

		if (trade.getTradeDestination() == null) {
			TradeDestinationMapping td = getTradeDestinationService().getTradeDestinationMappingByCommand(new TradeDestinationMappingSearchCommand(trade.getInvestmentSecurity().getId(), null, trade.getTradeDate()));
			if (td != null) {
				trade.setTradeDestination(td.getTradeDestination());

				// If it's a manual trade - Use the Holding Account's executing broker
				if (TradeDestination.MANUAL.equals(td.getTradeDestination().getName()) && trade.getHoldingInvestmentAccount() != null) {
					trade.setExecutingBrokerCompany(trade.getHoldingInvestmentAccount().getIssuingCompany());
				}
				else {
					trade.setExecutingBrokerCompany(td.getExecutingBrokerCompany());
				}
			}
		}

		String messagePrefix = StringUtils.isEmpty(validationMessagePrefix) ? trade.toString() : validationMessagePrefix;
		if (trade.isFixTrade()) {
			ValidationUtils.assertTrue(trade.getTradeType().isFixSupported(), messagePrefix + ". Fix Trades are not supported for type ["
					+ trade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName() + "]");
		}
		if (trade.getTradeType() != null && !trade.getTradeType().isBrokerSelectionAtExecutionAllowed()) {
			ValidationUtils.assertNotNull(trade.getExecutingBrokerCompany(), messagePrefix + " is missing a selection for the the executing broker.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public TradeManagementService getTradeManagementService() {
		return this.tradeManagementService;
	}


	public void setTradeManagementService(TradeManagementService tradeManagementService) {
		this.tradeManagementService = tradeManagementService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
