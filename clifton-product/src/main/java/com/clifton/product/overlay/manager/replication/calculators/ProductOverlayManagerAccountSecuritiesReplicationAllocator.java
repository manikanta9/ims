package com.clifton.product.overlay.manager.replication.calculators;


import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayManagerAccount;

import java.util.List;


/**
 * The <code>ProductOverlayManagerAccountSecuritiesReplicationAllocator</code> interface defines the method that
 * allocates the securitiesAllocation amount from the {@link ProductOverlayManagerAccount} to the list of {@link ProductOverlayAssetClassReplication}
 *
 * @author manderson
 */
public interface ProductOverlayManagerAccountSecuritiesReplicationAllocator {

	public void allocate(ProductOverlayManagerAccount managerAccount, List<ProductOverlayAssetClassReplication> replicationList);
}
