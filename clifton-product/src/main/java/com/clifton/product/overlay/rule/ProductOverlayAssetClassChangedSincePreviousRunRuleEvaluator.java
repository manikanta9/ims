package com.clifton.product.overlay.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.core.util.MathUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * ProductOverlayAssetClassUpdatedRuleEvaluator class handles triggering a violation when an asset class has been modified since the last
 * portfolio run (based on create date, not balance date).  It ensures portfolio managers are aware of any changes made to their accounts and is meant as a temporary solution
 * until a workflow process can be implemented.
 *
 * @author stevenf
 */
public class ProductOverlayAssetClassChangedSincePreviousRunRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, ProductOverlayRuleEvaluatorContext> {

	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, ProductOverlayRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			PortfolioRun previousRun = context.getPreviousRunByCreateDate(run);
			if (previousRun != null) {
				Date previousRunCreateDate = previousRun.getCreateDate();
				for (ProductOverlayAssetClass oac : CollectionUtils.getIterable(context.getRunSummaryList(run, false, null))) {
					InvestmentAccountAssetClass assetClass = oac.getAccountAssetClass();
					if (DateUtils.isDateAfter(assetClass.getUpdateDate(), previousRunCreateDate) && !(isViolationIgnoredOnPreviousRun(ruleConfig, previousRun, assetClass.getId()))) {
						Map<String, Object> templateValues = new HashMap<>();
						templateValues.put("updateUser", context.getSecurityUser(assetClass.getUpdateUserId()));
						ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), assetClass.getId(), null, templateValues));
					}
				}
			}
		}
		return ruleViolationList;
	}


	private boolean isViolationIgnoredOnPreviousRun(RuleConfig ruleConfig, PortfolioRun previousRun, int assetClassId) {
		RuleViolationSearchForm searchForm = new RuleViolationSearchForm();
		searchForm.setRuleDefinitionId(ruleConfig.getRuleDefinitionId());
		searchForm.setLinkedFkFieldId(BeanUtils.getIdentityAsLong(previousRun));
		searchForm.setCauseFkFieldId(MathUtils.getNumberAsLong(assetClassId));
		searchForm.setIgnored(true);
		return !CollectionUtils.isEmpty(getRuleViolationService().getRuleViolationList(searchForm));
	}
}



