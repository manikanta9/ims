package com.clifton.product.overlay.trade.group.dynamic.option.account.calculators;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionTradeCommand;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionTradeReplicationHolder;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>ProductOverlayRunTradeGroupDynamicOptionAccountRollUsingPortfolioValueCalculator</code> is used for accounts that calculate percent excess for puts
 * and calls as a percentage of the total portfolio value.
 *
 * @author manderson
 */

public class ProductOverlayRunTradeGroupDynamicOptionAccountRollUsingPortfolioValueCalculator extends BaseProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator {

	private AccountingPositionDailyService accountingPositionDailyService;

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * The percentage of the total portfolio value to set for matching exposure for puts/calls
	 */
	private BigDecimal putPercentValue;
	private BigDecimal callPercentValue;


	/**
	 * Can be made available as system bean properties if we have other use cases, but for now
	 * will default them to total portfolio value.
	 */
	private String beanPropertyName = "portfolioTotalValue";

	/**
	 * If true, will get the portfolio value, then reduce that value by the market value of options held in the account.
	 */
	private boolean removeOptionMarketValue;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyReplicationToMatchingExposure(ProductOverlayRunOptionTradeCommand tradeCommand, ProductOverlayRunOptionTradeReplicationHolder accountRoll, ProductOverlayAssetClassReplication replication) {
		// DO NOTHING - CALCULATOR DOESN'T INCLUDE ANY MATCHING EXPOSURE FROM REPLICATIONS
	}


	@Override
	public void applyAdditionalMatchingExposure(ProductOverlayRunOptionTradeReplicationHolder replicationHolder, PortfolioRun portfolioRun) {
		BigDecimal portfolioRunValue = getPortfolioRunValue(replicationHolder, portfolioRun);
		if (!MathUtils.isNullOrZero(getPutPercentValue())) {
			replicationHolder.setPutMatchingAdditionalExposure(MathUtils.getPercentageOf(getPutPercentValue(), portfolioRunValue, true));
		}
		if (!MathUtils.isNullOrZero(getCallPercentValue())) {
			replicationHolder.setCallMatchingAdditionalExposure(MathUtils.getPercentageOf(getCallPercentValue(), portfolioRunValue, true));
		}
	}

	////////////////////////////////////////////////////////////////////////////////


	private BigDecimal getPortfolioRunValue(ProductOverlayRunOptionTradeReplicationHolder replicationHolder, PortfolioRun portfolioRun) {
		BigDecimal portfolioRunValue = (BigDecimal) BeanUtils.getPropertyValue(portfolioRun, getBeanPropertyName());
		if (isRemoveOptionMarketValue()) {
			portfolioRunValue = MathUtils.subtract(portfolioRunValue, getOptionsMarketValue(replicationHolder.getClientAccount().getId(), portfolioRun.getBalanceDate()));
		}
		return portfolioRunValue;
	}


	private BigDecimal getOptionsMarketValue(Integer clientAccountId, Date date) {
		AccountingPositionDailyLiveSearchForm searchForm = new AccountingPositionDailyLiveSearchForm();
		searchForm.setClientAccountId(clientAccountId);
		searchForm.setSnapshotDate(date);
		List<AccountingPositionDaily> positionList = getAccountingPositionDailyService().getAccountingPositionDailyLiveList(searchForm);
		positionList = BeanUtils.filter(positionList, position -> position.getAccountingTransaction().getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName(), InvestmentType.OPTIONS);
		return CoreMathUtils.sumProperty(positionList, AccountingPositionDaily::getMarketValueBase);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public BigDecimal getPutPercentValue() {
		return this.putPercentValue;
	}


	public void setPutPercentValue(BigDecimal putPercentValue) {
		this.putPercentValue = putPercentValue;
	}


	public BigDecimal getCallPercentValue() {
		return this.callPercentValue;
	}


	public void setCallPercentValue(BigDecimal callPercentValue) {
		this.callPercentValue = callPercentValue;
	}


	public String getBeanPropertyName() {
		return this.beanPropertyName;
	}


	public void setBeanPropertyName(String beanPropertyName) {
		this.beanPropertyName = beanPropertyName;
	}


	public boolean isRemoveOptionMarketValue() {
		return this.removeOptionMarketValue;
	}


	public void setRemoveOptionMarketValue(boolean removeOptionMarketValue) {
		this.removeOptionMarketValue = removeOptionMarketValue;
	}
}
