package com.clifton.product.overlay;

import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.portfolio.run.margin.PortfolioRunMarginDetail;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Optional;


/**
 * <code>ProductOverlayCollateralMaturity</code> Show live collateral positions information necessary for creating trades for collateral.
 * Simplified version of {@link ProductOverlayAssetClassReplication}.
 *
 * @author terrys
 */
@NonPersistentObject
public class ProductOverlayCollateralMaturity {

	private PortfolioRunMarginDetail portfolioRunMarginDetail;

	private InvestmentAccount clientAccount;
	private InvestmentAccount holdingAccount;

	private InvestmentSecurity security;
	private String securityTypeName;
	private BigDecimal securityPrice;
	/**
	 * Duration until security matures.
	 */
	private BigDecimal duration;
	private BigDecimal yield;

	private BigDecimal targetExposure;

	/**
	 * Current quantity as reflected in general ledger
	 */
	private BigDecimal currentQuantity;
	/**
	 * Current exposure. For cash collateral, this will be set from a position.
	 * For security positions, it will be calculated from the security and price when unset.
	 */
	private BigDecimal currentExposure;
	/**
	 * Pending quantity from transfers and/or trades not yet booked to general ledger
	 */
	private BigDecimal pendingQuantity;
	/**
	 * Pending exposure. For cash collateral, this will be set from a position.
	 * For security positions, it will be calculated from the security and price when unset.
	 */
	private BigDecimal pendingExposure;

	/**
	 * Quantity fields applicable to generating trades.
	 */
	private BigDecimal buy;
	private BigDecimal sell;

	/**
	 * Placeholder reference for trade details such as collateral, executing broker, destination, etc.
	 */
	private Trade trade;

	/**
	 * Flag to define if this maturity is disabled from trading. Cash Collateral positions cannot be traded.
	 */
	boolean tradingDisabled;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a {@link ProductOverlayCollateralMaturity} with the client account, holding account, security, and quantity of the provided balance.
	 */
	public static ProductOverlayCollateralMaturity of(AccountingBalance accountingBalance) {
		ProductOverlayCollateralMaturity collateralMaturity = new ProductOverlayCollateralMaturity();
		collateralMaturity.setClientAccount(accountingBalance.getClientInvestmentAccount());
		collateralMaturity.setHoldingAccount(accountingBalance.getHoldingInvestmentAccount());
		collateralMaturity.setSecurity(accountingBalance.getInvestmentSecurity());
		if (accountingBalance.getAccountingAccount().isPosition()) {
			collateralMaturity.setPendingQuantity(accountingBalance.getPendingQuantity());
			collateralMaturity.setCurrentQuantity(MathUtils.subtract(accountingBalance.getQuantity(), collateralMaturity.getPendingQuantity()));
		}
		else {
			collateralMaturity.setPendingExposure(accountingBalance.getPendingLocalAmount());
			collateralMaturity.setCurrentExposure(MathUtils.subtract(accountingBalance.getLocalAmount(), collateralMaturity.getPendingExposure()));
			collateralMaturity.setTradingDisabled(true);
		}
		return collateralMaturity;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getPendingExposure() {
		if (this.pendingExposure != null) {
			return this.pendingExposure;
		}

		BigDecimal contractValue = InvestmentCalculatorUtils.calculateContractValue(getSecurity(), getSecurityPrice(), false);
		return MathUtils.multiply(getPendingQuantity(), contractValue);
	}


	public BigDecimal getCurrentExposure() {
		if (this.currentExposure != null) {
			return this.currentExposure;
		}
		BigDecimal contractValue = InvestmentCalculatorUtils.calculateContractValue(getSecurity(), getSecurityPrice(), false);
		return MathUtils.multiply(getCurrentQuantity(), contractValue);
	}


	public BigDecimal getQuantity() {
		return MathUtils.add(getCurrentQuantity(), getPendingQuantity());
	}


	public BigDecimal getExposure() {
		if (getSecurity().isCurrency()) {
			return MathUtils.add(getCurrentExposure(), getPendingExposure());
		}
		else {
			BigDecimal contractValue = InvestmentCalculatorUtils.calculateContractValue(getSecurity(), getSecurityPrice(), false);
			return MathUtils.multiply(getQuantity(), contractValue);
		}
	}


	public BigDecimal getAllocationPercent() {
		return CoreMathUtils.getPercentValue(getExposure(), getTargetExposure(), true);
	}


	public BigDecimal getTargetQuantity() {
		if (getSecurity().isCurrency()) {
			return BigDecimal.ZERO;
		}
		else {
			BigDecimal contractValue = InvestmentCalculatorUtils.calculateContractValue(getSecurity(), getSecurityPrice(), false);
			return MathUtils.divide(getTargetExposure(), contractValue);
		}
	}


	public boolean isSecurityNotTradable() {
		if (getSecurity() != null && getSecurity().getInstrument() != null && getSecurity().getInstrument().getHierarchy() != null) {
			return getSecurity().getInstrument().getHierarchy().isTradingDisallowed();
		}
		return false;
	}


	public InvestmentType getInvestmentType() {
		return Optional.ofNullable(getSecurity())
				.map(InvestmentSecurity::getInstrument)
				.map(InvestmentInstrument::getHierarchy)
				.map(InvestmentInstrumentHierarchy::getInvestmentType)
				.orElse(null);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunMarginDetail getPortfolioRunMarginDetail() {
		return this.portfolioRunMarginDetail;
	}


	public void setPortfolioRunMarginDetail(PortfolioRunMarginDetail portfolioRunMarginDetail) {
		this.portfolioRunMarginDetail = portfolioRunMarginDetail;
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public String getSecurityTypeName() {
		return this.securityTypeName;
	}


	public void setSecurityTypeName(String securityTypeName) {
		this.securityTypeName = securityTypeName;
	}


	public BigDecimal getSecurityPrice() {
		return this.securityPrice;
	}


	public void setSecurityPrice(BigDecimal securityPrice) {
		this.securityPrice = securityPrice;
	}


	public BigDecimal getDuration() {
		return this.duration;
	}


	public void setDuration(BigDecimal duration) {
		this.duration = duration;
	}


	public BigDecimal getYield() {
		return this.yield;
	}


	public void setYield(BigDecimal yield) {
		this.yield = yield;
	}


	public BigDecimal getTargetExposure() {
		return this.targetExposure;
	}


	public void setTargetExposure(BigDecimal targetExposure) {
		this.targetExposure = targetExposure;
	}


	public BigDecimal getCurrentQuantity() {
		return this.currentQuantity;
	}


	public void setCurrentQuantity(BigDecimal currentQuantity) {
		this.currentQuantity = currentQuantity;
	}


	public void setCurrentExposure(BigDecimal currentExposure) {
		this.currentExposure = currentExposure;
	}


	public BigDecimal getPendingQuantity() {
		return this.pendingQuantity;
	}


	public void setPendingQuantity(BigDecimal pendingQuantity) {
		this.pendingQuantity = pendingQuantity;
	}


	public void setPendingExposure(BigDecimal pendingExposure) {
		this.pendingExposure = pendingExposure;
	}


	public BigDecimal getBuy() {
		return this.buy;
	}


	public void setBuy(BigDecimal buy) {
		this.buy = buy;
	}


	public BigDecimal getSell() {
		return this.sell;
	}


	public void setSell(BigDecimal sell) {
		this.sell = sell;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public Trade getTrade() {
		return this.trade;
	}


	public void setTrade(Trade trade) {
		this.trade = trade;
	}


	public boolean isTradingDisabled() {
		return this.tradingDisabled;
	}


	public void setTradingDisabled(boolean tradingDisabled) {
		this.tradingDisabled = tradingDisabled;
	}
}
