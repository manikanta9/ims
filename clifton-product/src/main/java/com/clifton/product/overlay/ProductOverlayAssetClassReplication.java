package com.clifton.product.overlay;


import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InvestmentOverlayAssetClassReplication</code> class represents a specific security position that is a part of a replication for the specified asset class.
 * <p>
 * It contains existing position as well as recommended (target) position information.
 *
 * @author vgomelsky
 */
public class ProductOverlayAssetClassReplication extends BasePortfolioRunReplication {

	protected static final String TABLE_NAME = "ProductOverlayAssetClassReplication";

	////////////////////////////////////////////////////////////////////////////////

	private ProductOverlayAssetClass overlayAssetClass;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getTableName() {
		return TABLE_NAME;
	}


	@Override
	protected String getDurationLabel() {
		if (getOverlayAssetClass().getBenchmarkDuration() != null) {
			return " (Benchmark Dur: " + CoreMathUtils.formatNumber(getOverlayAssetClass().getBenchmarkDuration(), "#,###.0000")
					+ (getOverlayAssetClass().getBenchmarkCreditDuration() != null ? ", Credit Dur: " + CoreMathUtils.formatNumber(getOverlayAssetClass().getBenchmarkCreditDuration(), "#,###.0000") : "")
					+ ", Effective vs. Benchmark: " + CoreMathUtils.formatNumber(getOverlayAssetClass().getEffectiveVsBenchmarkDuration(), "#,###.0000") + ")";
		}
		return "";
	}


	@Override
	public PortfolioRun getPortfolioRun() {
		if (getOverlayAssetClass() != null) {
			return getOverlayAssetClass().getOverlayRun();
		}
		return null;
	}


	@Override
	public void setPortfolioRun(PortfolioRun portfolioRun) {
		if (getOverlayAssetClass() == null) {
			setOverlayAssetClass(new ProductOverlayAssetClass());
		}
		getOverlayAssetClass().setOverlayRun(portfolioRun);
	}


	@Override
	public InvestmentAccount getTradingClientAccount() {
		if (getOverlayAssetClass() != null) {
			return getOverlayAssetClass().getTradingClientAccount();
		}
		return null;
	}


	@Override
	public Short getOrder() {
		if (getOverlayAssetClass() != null && getOverlayAssetClass().getAccountAssetClass() != null) {
			return getOverlayAssetClass().getAccountAssetClass().getOrder();
		}
		return super.getOrder();
	}


	@Override
	public InvestmentAccountAssetClass getAccountAssetClass() {
		// Doesn't apply to LDI Runs
		return getOverlayAssetClass().getAccountAssetClass();
	}


	@Override
	protected String getDependentEntityTopLevelLabel() {
		return getOverlayAssetClass() == null ? "" : getOverlayAssetClass().getTopLevelLabel();
	}


	@Override
	protected String getDependentEntityLabel() {
		return getOverlayAssetClass() == null ? "" : getOverlayAssetClass().getLabel();
	}


	// (Contract Duration / Benchmark Duration)
	public BigDecimal getDurationAdjustment() {
		if (isDurationAdjusted()) {
			return MathUtils.divide(getDuration(), getOverlayAssetClass().getBenchmarkDuration());
		}
		return BigDecimal.ONE;
	}


	public boolean isDurationAdjusted() {
		// Note: NOT NECESSARILY FULLY ACCURATE BECAUSE WE'D NEED THE CALCULATOR TO KNOW THIS...BUT 99% OF CASES THIS SHOULD WORK
		if (getReplicationType().isDurationSupported() && !getOverlayAssetClass().getAccountAssetClass().isDoNotAdjustContractValue()) {
			return (!MathUtils.isNullOrZero(getDuration()) && !MathUtils.isNullOrZero(getOverlayAssetClass().getBenchmarkDuration()));
		}
		return false;
	}


	/**
	 * Trade Entry is disabled for asset classes that are flagged as positions being excluded or hold a Non Tradable Securities These contracts should be held in other asset
	 * classes where trading is enabled there, i.e. "duplicate" contract and these buys/sells are updated to reflect those trades to see the trade impact
	 */
	@Override
	public boolean isTradeEntryDisabled() {
		if (isSecurityNotTradable()) {
			return true;
		}
		if (isCashExposure()) {
			return true;
		}
		if (getOverlayAssetClass() != null) {
			if (getOverlayAssetClass().getAccountAssetClass() != null) {
				return getOverlayAssetClass().getAccountAssetClass().isReplicationPositionExcludedFromCount();
			}
		}

		return false;
	}


	/**
	 * If the security is the same as the Client Account base CCY, then this is considered to be
	 * cash exposure that is handled differently (See Jira: PRODUCT-243) which disables trade entry,
	 * is excluded from billing basis and aum calculations, and will back into overlay exposure based on other allocations (if any)
	 * else based on asset class target (if the only allocation)
	 */
	@Override
	public boolean isCashExposure() {
		if (getSecurity() != null && getOverlayAssetClass() != null && getOverlayAssetClass().getTradingClientAccount() != null && getOverlayAssetClass().getTradingClientAccount().getBaseCurrency() != null) {
			return getOverlayAssetClass().getTradingClientAccount().getBaseCurrency().equals(getSecurity());
		}
		return false;
	}


	public boolean isPrimaryReplication() {
		if (getReplication() != null) {
			if (getOverlayAssetClass() != null) {
				return getReplication().equals(getOverlayAssetClass().getPrimaryReplication());
			}
		}
		return false;
	}


	public boolean isSecondaryReplication() {
		if (getReplication() != null) {
			if (getOverlayAssetClass() != null) {
				return getReplication().equals(getOverlayAssetClass().getSecondaryReplication());
			}
		}
		return false;
	}


	@Override
	public boolean isMatchingReplication() {
		return !isPrimaryReplication() && !isSecondaryReplication();
	}


	@Override
	public boolean isRollupReplication() {
		return getOverlayAssetClass() != null && getOverlayAssetClass().isRollupAssetClass();
	}


	@Override
	public boolean isTargetFollowsActual() {
		return getOverlayAssetClass() != null && getOverlayAssetClass().isTargetFollowsActual();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ProductOverlayAssetClass getOverlayAssetClass() {
		return this.overlayAssetClass;
	}


	public void setOverlayAssetClass(ProductOverlayAssetClass overlayAssetClass) {
		this.overlayAssetClass = overlayAssetClass;
	}


	@Override
	public InvestmentAccount getClientAccount() {
		if (getPortfolioRun() != null) {
			return getPortfolioRun().getClientInvestmentAccount();
		}
		return null;
	}


	@Override
	public Date getBalanceDate() {
		if (getPortfolioRun() != null) {
			return getPortfolioRun().getBalanceDate();
		}
		return null;
	}


	@Override
	public InvestmentSecurity getBenchmarkDurationSecurity() {
		if (getAccountAssetClass() != null) {
			return getAccountAssetClass().getBenchmarkDurationSecurity();
		}
		return null;
	}


	@Override
	public BigDecimal getBenchmarkDuration() {
		if (getOverlayAssetClass() != null) {
			return getOverlayAssetClass().getBenchmarkDuration();
		}
		return null;
	}


	@Override
	public BigDecimal getBenchmarkCreditDuration() {
		if (getOverlayAssetClass() != null) {
			return getOverlayAssetClass().getBenchmarkCreditDuration();
		}
		return null;
	}


	@Override
	public String getDurationFieldNameOverride() {
		return getOverlayAssetClass().getAccountAssetClass().getDurationFieldNameOverride();
	}
}
