package com.clifton.product.overlay.trade;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;

import java.math.BigDecimal;


/**
 * The <code>ProductOverlayAssetClassReplicationTradeTargetDependency</code> is a helper object
 * that stores target update information for a dependent asset class or specific replication.
 * <p/>
 * For example, target exposure adjustment used asset classes apply changes to their dependent asset class assignments
 * Non Matching Replications targets or actual updates would affect their matching counterpart targets.
 * <p/>
 * Doesn't reference actual objects, just ids for small memory use in UI
 * <p/>
 * Populated from parsing {@link com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory}
 *
 * @author manderson
 */
@NonPersistentObject
public class ProductOverlayAssetClassReplicationTradeTargetDependency {

	private static final String TRIGGERED_BY_ACTUAL = "ACTUAL";
	private static final String TRIGGERED_BY_TARGET = "TARGET";
	private static final String TRIGGERED_BY_TRADE = "TRADE";
	private static final String TRIGGERED_BY_M2M = "M2M";

	public enum ReplicationTargetDependencyTypes {

		TARGET_EQUALS_ACTUAL("Target = Actual Adjustment", TRIGGERED_BY_ACTUAL), // Self TFA update
		TARGET_EQUALS_ACTUAL_OFFSET("Target Adjustment Offset", TRIGGERED_BY_TRADE), // Non Matching --> Non Matching TFA updates from one asset class to apply offset to the other asset class, offsets only apply to trade(s) entered, not change due to price changes
		MATCHING_TARGET("Matching Target Adjustment", TRIGGERED_BY_TARGET), //  Non Matching --> Matching: Matching Replication Target matched to non-matching targets
		MATCHING_ACTUAL("Matching Actual Adjustment", TRIGGERED_BY_ACTUAL), // Non Matching --> Matching:  Matching Replication Target matched to non-matching actuals
		M2M("M2M Adjustment From Matching", TRIGGERED_BY_M2M); // Matching --> Non Matching: Applies for Matching Replications to apply M2M adjustment to it's NonMatching Counterparts


		ReplicationTargetDependencyTypes(String typeLabel, String triggeredBy) {
			this.typeLabel = typeLabel;
			this.triggeredBy = triggeredBy;
		}


		private final String typeLabel;
		private final String triggeredBy;


		public String getTypeLabel() {
			return this.typeLabel;
		}


		public String getTriggeredBy() {
			return this.triggeredBy;
		}
	}

	private String label;

	private Integer causeOverlayReplicationId;

	private Integer dependentOverlayReplicationId;

	private final ReplicationTargetDependencyTypes dependencyType;

	private BigDecimal allocationPercentage;

	private boolean negateAllocation;


	public ProductOverlayAssetClassReplicationTradeTargetDependency(ReplicationTargetDependencyTypes dependencyType, ProductOverlayAssetClassReplication causeRep,
	                                                                ProductOverlayAssetClassReplication dependencyRep, BigDecimal allocationPercentage, boolean negateAllocation) {
		this.dependencyType = dependencyType;
		this.allocationPercentage = allocationPercentage;
		this.causeOverlayReplicationId = causeRep.getId();
		this.dependentOverlayReplicationId = dependencyRep.getId();
		this.label = this.dependencyType.getTypeLabel() + " (" + causeRep.getSecurity().getSymbol() + "): ";
		this.negateAllocation = negateAllocation;
	}


	public String getTriggeredBy() {
		return getDependencyType().getTriggeredBy();
	}


	public Integer getCauseOverlayReplicationId() {
		return this.causeOverlayReplicationId;
	}


	public void setCauseOverlayReplicationId(Integer causeOverlayReplicationId) {
		this.causeOverlayReplicationId = causeOverlayReplicationId;
	}


	public Integer getDependentOverlayReplicationId() {
		return this.dependentOverlayReplicationId;
	}


	public void setDependentOverlayReplicationId(Integer dependentOverlayReplicationId) {
		this.dependentOverlayReplicationId = dependentOverlayReplicationId;
	}


	public BigDecimal getAllocationPercentage() {
		return this.allocationPercentage;
	}


	public void setAllocationPercentage(BigDecimal allocationPercentage) {
		this.allocationPercentage = allocationPercentage;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public boolean isNegateAllocation() {
		return this.negateAllocation;
	}


	public void setNegateAllocation(boolean negateAllocation) {
		this.negateAllocation = negateAllocation;
	}


	public ReplicationTargetDependencyTypes getDependencyType() {
		return this.dependencyType;
	}
}
