package com.clifton.product.overlay.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunReplicationDurationChangeRuleEvaluator;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
public class ProductOverlayAssetClassReplicationDurationChangeRuleEvaluator extends PortfolioRunReplicationDurationChangeRuleEvaluator {

	@Override
	protected List<RuleViolation> evaluateRuleForAssetClassBenchmarkDurationValueChange(PortfolioRun run, EntityConfig entityConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		// We need ProductOverlayContext for some of the processing - can't cast, but we should be able to copy properties, as the context that is passed in should be the product type

		ProductOverlayRuleEvaluatorContext productContext = new ProductOverlayRuleEvaluatorContext();
		BeanUtils.copyProperties(context, productContext);

		List<RuleViolation> ruleViolationList = new ArrayList<>();
		List<ProductOverlayAssetClass> list = productContext.getRunSummaryList(run, false, null);
		List<ProductOverlayAssetClass> previousList = productContext.getRunSummaryList(run, true, null);
		for (ProductOverlayAssetClass overlayAssetClass : CollectionUtils.getIterable(list)) {
			if (!MathUtils.isNullOrZero(overlayAssetClass.getBenchmarkDuration())) {
				for (ProductOverlayAssetClass previousOverlayAssetClass : CollectionUtils.getIterable(previousList)) {
					if (overlayAssetClass.getAccountAssetClass().equals(previousOverlayAssetClass.getAccountAssetClass())) {
						BigDecimal currentValue = overlayAssetClass.getBenchmarkDuration();
						BigDecimal previousValue = previousOverlayAssetClass.getBenchmarkDuration();
						if (currentValue != null && previousValue != null) {
							BigDecimal percentChange = MathUtils.getPercentChange(previousValue, currentValue, true);
							Map<String, Object> templateValues = new HashMap<>();
							if (RuleEvaluatorUtils.isEntityConfigRangeViolated(percentChange, entityConfig, templateValues)) {
								templateValues.put("currentValue", currentValue);
								templateValues.put("previousValue", previousValue);
								ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), overlayAssetClass.getId(), null, templateValues));
							}
						}
					}
				}
			}
		}
		return ruleViolationList;
	}
}
