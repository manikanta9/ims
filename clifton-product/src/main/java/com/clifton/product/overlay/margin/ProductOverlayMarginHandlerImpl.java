package com.clifton.product.overlay.margin;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.margin.BasePortfolioRunMarginHandler;
import com.clifton.portfolio.run.margin.PortfolioRunMargin;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>ProductOverlayMarginHandlerImpl</code> processes PortfolioRunMargin for Portfolio Runs/Accounts that use Overlay Service Types (Overlay, Fully Funded)
 * It is the default calculator and utilizes asset classes and replication positions, where overlay target is used as the margin basis
 *
 * @author manderson
 */
@Component("portfolioRunMarginOverlayHandler")
public class ProductOverlayMarginHandlerImpl extends BasePortfolioRunMarginHandler<ProductOverlayRunConfig> {


	@Override
	public List<PortfolioRunMargin> generatePortfolioRunMarginList(ProductOverlayRunConfig runConfig) {
		List<PortfolioRunMargin> marginList = new ArrayList<>();

		List<InvestmentAccountRelationship> tradingFuturesAccountRelationshipList = getTradingFuturesAccountRelationshipList(runConfig);
		if (!CollectionUtils.isEmpty(tradingFuturesAccountRelationshipList)) {
			PortfolioRun run = runConfig.getRun();

			Map<InvestmentAccount, List<ProductOverlayAssetClass>> brokerAssetClassMap = createBrokerAccountAssetClassMap(runConfig, tradingFuturesAccountRelationshipList);
			List<ProductOverlayAssetClassReplication> replicationList = runConfig.getOverlayAssetClassReplicationList();

			List<ProductOverlayManagerAccount> ourManagerAccountList = CollectionUtils.getStream(runConfig.getOverlayManagerAccountList())
					.filter(m -> !m.isRollupAssetClass())
					.filter(m -> m.getManagerAccountAssignment().getReferenceOne().getManagerCompany().getType().isOurCompany())
					.collect(Collectors.toList());

			for (Map.Entry<InvestmentAccount, List<ProductOverlayAssetClass>> investmentAccountListEntry : brokerAssetClassMap.entrySet()) {
				// Asset Class List
				List<ProductOverlayAssetClass> assetClassList = investmentAccountListEntry.getValue();
				List<ProductOverlayAssetClass> assetClassListWithReplications = new ArrayList<>();
				for (ProductOverlayAssetClass ac : CollectionUtils.getIterable(assetClassList)) {
					if (ac.getPrimaryReplication() != null) {
						assetClassListWithReplications.add(ac);
					}
				}
				// Skip this broker account
				if (CollectionUtils.isEmpty(assetClassListWithReplications)) {
					continue;
				}

				PortfolioRunMargin brokerMargin = populateMarginForBroker(runConfig, investmentAccountListEntry.getKey());
				// If there is only one asset class - set it
				if (CollectionUtils.getSize(assetClassList) == 1) {
					brokerMargin.setAccountAssetClass(assetClassList.get(0).getAccountAssetClass());
				}

				// Asset Class Replications - Necessary to calculate Overlay Target and to recalculate Required Initial Margin
				List<ProductOverlayAssetClassReplication> acRepList = BeanUtils.filter(replicationList, overlayReplication -> assetClassListWithReplications.contains(overlayReplication.getOverlayAssetClass()));

				// Overlay Target
				BigDecimal overlayTarget = BigDecimal.ZERO;
				// Store in a map by asset class because we need to sum the absolute value of each asset class
				Map<Integer, BigDecimal> assetClassOverlayTargetMap = new HashMap<>();
				for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(acRepList)) {
					// Skip Cash Exposure from Margin Calculation
					if (!rep.isExcludedFromOverlayTotal() && !rep.isCashExposure()) {
						Integer acId = rep.getOverlayAssetClass().getAccountAssetClass().getId();
						if (!assetClassOverlayTargetMap.containsKey(acId)) {
							assetClassOverlayTargetMap.put(acId, rep.getTargetExposureAdjusted());
						}
						else {
							assetClassOverlayTargetMap.put(acId, MathUtils.add(assetClassOverlayTargetMap.get(acId), rep.getTargetExposureAdjusted()));
						}
					}
				}

				for (Map.Entry<Integer, BigDecimal> integerBigDecimalEntry : assetClassOverlayTargetMap.entrySet()) {
					overlayTarget = MathUtils.add(overlayTarget, MathUtils.abs(integerBigDecimalEntry.getValue()));
				}
				brokerMargin.setMarginBasis(overlayTarget);

				// If not set - Recalc
				if (MathUtils.isNullOrZero(brokerMargin.getMarginInitialValue())) {
					BigDecimal requiredInitial = BigDecimal.ZERO;
					boolean groupedMarginAccount = BooleanUtils.isTrue((Boolean) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(brokerMargin.getPortfolioRun().getClientInvestmentAccount(), InvestmentAccount.GROUPED_MARGIN_ACCOUNT));

					if (!groupedMarginAccount) {
						for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(acRepList)) {
							// Only applies to replications with Actual Contracts != 0 AND Futures - Calculator checks these
							// Note: Do Not include Virtual Contracts because instrumentMargin returned is always absolute value and include virtual contracts would inflate the value
							BigDecimal instrumentMargin = InvestmentCalculatorUtils.calculateRequiredCollateral(rep.getSecurity(), investmentAccountListEntry.getKey(), rep.getActualContracts(), rep.getExchangeRate(), true);
							requiredInitial = MathUtils.add(requiredInitial, instrumentMargin);
						}
					}
					brokerMargin.setMarginInitialValue(requiredInitial);
				}
				brokerMargin.setMarginRecommendedValue(MathUtils.getPercentageOf(runConfig.getMarginRecommendedPercent(), brokerMargin.getMarginBasis(), true));

				// Clifton Manager Account Value for Asset Class(es)
				// Invalid only if there is no manager for "Clifton" to pull a balance from, if the balance
				// happens to be zero - an ignorable warning will be created.  In some cases, this is OK, but to get around it analysts were adding 0.01 adjustments just to get past this error.
				// Note - Name is hardcoded to Parametric Minneapolis even though any Our Companies would apply, but makes the warning clearer when users run into the error
				InvestmentAccountAssetClass[] accountAssetClasses = BeanUtils.getPropertyValuesUniqueExcludeNull(assetClassList, ProductOverlayAssetClass::getAccountAssetClass, InvestmentAccountAssetClass.class);
				List<ProductOverlayManagerAccount> ourManagerAccountAssetClassList = BeanUtils.filter(ourManagerAccountList, overlayManagerAccount -> ArrayUtils.contains(accountAssetClasses, overlayManagerAccount.getAccountAssetClass()));
				if (CollectionUtils.isEmpty(ourManagerAccountAssetClassList)) {
					throw new ValidationExceptionWithCause(
							"InvestmentAccount",
							run.getClientInvestmentAccount().getId(),
							"Unable to perform margin analysis for broker account ["
									+ (investmentAccountListEntry.getKey()).getLabel()
									+ "].  There are no managers tied to Parametric Minneapolis (Type: Our Companies) company to find our account value for any of the asset classes associated with this broker account.  Parametric Manager Account must be assigned to one of these asset classes: "
									+ BeanUtils.getPropertyValues(assetClassList, "label", ",") + ".");
				}
				brokerMargin.setOurAccountValue(CoreMathUtils.sumProperty(ourManagerAccountAssetClassList, ProductOverlayManagerAccount::getCashAllocation));
				marginList.add(brokerMargin);
			}
			return marginList;
		}
		return null;
	}


	private Map<InvestmentAccount, List<ProductOverlayAssetClass>> createBrokerAccountAssetClassMap(ProductOverlayRunConfig runConfig,
	                                                                                                List<InvestmentAccountRelationship> tradingFuturesBrokerRelationshipList) {
		InvestmentAccount defaultBroker = getDefaultBroker(runConfig, tradingFuturesBrokerRelationshipList);

		List<InvestmentAccountRelationship> subAccountRelationshipList = getPortfolioAccountDataRetriever().getCliftonSubAccountAccountRelationshipList(runConfig.getClientInvestmentAccountId(),
				runConfig.getBalanceDate());

		Map<InvestmentAccount, List<ProductOverlayAssetClass>> brokerAssetClassMap = new HashMap<>();
		List<ProductOverlayAssetClass> assetClassList = runConfig.getOverlayAssetClassList();

		for (ProductOverlayAssetClass ac : CollectionUtils.getIterable(assetClassList)) {
			if (ac.isRollupAssetClass()) {
				continue;
			}

			boolean found = false;
			Set<Integer> checkedAccounts = new HashSet<>();
			for (InvestmentAccountRelationship relationship : CollectionUtils.getIterable(tradingFuturesBrokerRelationshipList)) {
				if (checkedAccounts.add(relationship.getReferenceTwo().getId())) {
					// If a sub-account's broker, look up the asset class on the sub account relationship
					if (!runConfig.getRun().getClientInvestmentAccount().equals(relationship.getReferenceOne())) {
						// Can have one sub-account related to multiple asset classes
						List<InvestmentAccountRelationship> subRelList = BeanUtils.filter(subAccountRelationshipList, investmentAccountRelationship -> investmentAccountRelationship.getReferenceTwo().getId(), relationship.getReferenceOne().getId());
						for (InvestmentAccountRelationship subRel : CollectionUtils.getIterable(subRelList)) {
							InvestmentAccountAssetClass relAccountAssetClass = subRel.getAccountAssetClass();
							if (relAccountAssetClass != null) {
								if (relAccountAssetClass.equals(ac.getAccountAssetClass())) {
									found = true;
									if (brokerAssetClassMap.containsKey(relationship.getReferenceTwo())) {
										brokerAssetClassMap.get(relationship.getReferenceTwo()).add(ac);
									}
									else {
										brokerAssetClassMap.put(relationship.getReferenceTwo(), CollectionUtils.createList(ac));
									}
									break;
								}
							}
						}
					}
				}
			}
			if (!found) {
				// JUST SKIP THE ASSET CLASS IF THERE IS NOT A SPECIFIC OR DEFAULT BROKER SET UP
				if (defaultBroker != null) {
					if (brokerAssetClassMap.containsKey(defaultBroker)) {
						brokerAssetClassMap.get(defaultBroker).add(ac);
					}
					else {
						brokerAssetClassMap.put(defaultBroker, CollectionUtils.createList(ac));
					}
				}
			}
		}
		return brokerAssetClassMap;
	}
}
