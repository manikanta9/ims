package com.clifton.product.overlay.manager.replication.calculators;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureService;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;
import com.clifton.marketdata.instrument.allocation.MarketDataInvestmentSecurityAllocation;
import com.clifton.marketdata.instrument.allocation.MarketDataInvestmentSecurityAllocationService;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.system.bean.SystemBeanService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductOverlayManagerAccountSecuritiesReplicationByInvestmentSecurityAllocator</code> class
 * is an implementation of the {@link ProductOverlayManagerAccountSecuritiesReplicationAllocator} and applies the
 * manager's security allocation to the Replication's additionalExposure or additionalOverlayExposure field
 * based on the selected security.
 * <p/>
 * 1.  A security that is a structured index will match based on the calculated weights for the given balance date (pull from the manager account)
 * 2.  Security that is made up of security allocations will match based on the calculated weights for the given balance date (pull from the manager account)
 * 3.  All other securities match to that security and are given 100%
 * <p/>
 * Matching logic will attempt:
 * Equals:
 * Most Underlying = Most Underlying
 *
 * @author manderson
 */
public class ProductOverlayManagerAccountSecuritiesReplicationByInvestmentSecurityAllocator implements ProductOverlayManagerAccountSecuritiesReplicationAllocator {

	private InvestmentInstrumentService investmentInstrumentService;

	private InvestmentInstrumentStructureService investmentInstrumentStructureService;

	private MarketDataInvestmentSecurityAllocationService marketDataInvestmentSecurityAllocationService;

	private SystemBeanService systemBeanService;

	//////////////////////////////////////

	private Integer investmentSecurityId;

	private boolean additionalOverlayExposure;


	//////////////////////////////////////


	@Override
	public void allocate(ProductOverlayManagerAccount managerAccount, List<ProductOverlayAssetClassReplication> replicationList) {
		ValidationUtils.assertNotNull(getInvestmentSecurityId(), "Investment Security ID is required");

		BigDecimal amount = managerAccount.getSecuritiesAllocation();
		if (!MathUtils.isNullOrZero(amount)) {
			Map<InvestmentSecurity, BigDecimal> securityAllocationMap = generateInvestmentSecurityAllocationAmountMap(managerAccount, amount);
			for (Map.Entry<InvestmentSecurity, BigDecimal> investmentSecurityBigDecimalEntry : securityAllocationMap.entrySet()) {
				List<ProductOverlayAssetClassReplication> filteredReps = BeanUtils.filter(replicationList, replication -> isApplySecurityToReplication(investmentSecurityBigDecimalEntry.getKey(), replication));
				applyAdditionalExposureToFilteredReplications(investmentSecurityBigDecimalEntry.getKey(), investmentSecurityBigDecimalEntry.getValue(), filteredReps, generateExceptionMessage(managerAccount, ""));
			}
		}
	}


	private void applyAdditionalExposureToFilteredReplications(InvestmentSecurity security, BigDecimal amount, List<ProductOverlayAssetClassReplication> filteredReps, String messagePrefix) {
		// If None Found - Throw An Exception
		int size = CollectionUtils.getSize(filteredReps);
		if (size == 0) {
			throw new ValidationException(messagePrefix + "Unable to find a replication contract that matches security [" + security.getLabel() + "] with most underlying [" + getMostUnderlying(security).getLabel() + "]");
		}
		// If One - Give it All
		if (size == 1) {
			addExposureToReplication(filteredReps.get(0), amount);
		}
		else {
			BigDecimal totalWeight = CoreMathUtils.sumProperty(filteredReps, ProductOverlayAssetClassReplication::getAllocationWeightAdjusted);
			BigDecimal previousTotal = CoreMathUtils.sumProperty(filteredReps, (isAdditionalOverlayExposure() ? ProductOverlayAssetClassReplication::getAdditionalOverlayExposure : ProductOverlayAssetClassReplication::getAdditionalExposure));
			for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(filteredReps)) {
				BigDecimal amountToAllocate = MathUtils.round(MathUtils.getPercentageOf(CoreMathUtils.getPercentValue(rep.getAllocationWeightAdjusted(), totalWeight, true), amount, true), 2);
				addExposureToReplication(rep, amountToAllocate);
			}
			// Apply small rounding differences
			CoreMathUtils.applySumPropertyDifference(filteredReps, (isAdditionalOverlayExposure() ? ProductOverlayAssetClassReplication::getAdditionalOverlayExposure : ProductOverlayAssetClassReplication::getAdditionalExposure), (isAdditionalOverlayExposure() ? ProductOverlayAssetClassReplication::setAdditionalOverlayExposure : ProductOverlayAssetClassReplication::setAdditionalExposure), MathUtils.add(previousTotal, amount), true);
		}
	}


	private void addExposureToReplication(ProductOverlayAssetClassReplication rep, BigDecimal amount) {
		if (isAdditionalOverlayExposure()) {
			rep.setAdditionalOverlayExposure(MathUtils.add(rep.getAdditionalOverlayExposure(), amount));
		}
		else {
			rep.setAdditionalExposure(MathUtils.add(rep.getAdditionalExposure(), amount));
		}
	}


	private String generateExceptionMessage(ProductOverlayManagerAccount managerAccount, String message) {
		return "Manager Assignment [" + managerAccount.getLabel() + "] Securities Balance Allocation: " + message;
	}


	private Map<InvestmentSecurity, BigDecimal> generateInvestmentSecurityAllocationAmountMap(ProductOverlayManagerAccount managerAccount, BigDecimal amount) {
		Map<InvestmentSecurity, BigDecimal> securityAllocationMap = new HashMap<>();

		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(getInvestmentSecurityId());

		Date date = managerAccount.getOverlayRun().getBalanceDate();
		if (security.isAllocationOfSecurities()) {
			SecurityAllocationSearchForm searchForm = new SecurityAllocationSearchForm();
			searchForm.setParentInvestmentSecurityId(security.getId());
			searchForm.setActiveOnDate(date);
			List<MarketDataInvestmentSecurityAllocation> allocationList = getMarketDataInvestmentSecurityAllocationService().getMarketDataInvestmentSecurityAllocationList(searchForm);
			if (CollectionUtils.isEmpty(allocationList)) {
				throw new ValidationException(generateExceptionMessage(managerAccount, "Security [" + security.getLabel()
						+ "] is an allocated security, however current securities and weights did not evaluate.  Verify custom allocation is set up correctly for this security and active on ["
						+ DateUtils.fromDateShort(date) + "]"));
			}
			// Sort the list from smallest to largest so last one is the largest and gets applied small amounts for rounding
			allocationList = BeanUtils.sortWithFunction(allocationList, security.getInstrument().getHierarchy().getSecurityAllocationType().isAutoRebalancingSupported() ? MarketDataInvestmentSecurityAllocation::getCurrentAllocationWeight : MarketDataInvestmentSecurityAllocation::getAllocationWeight, true);
			BigDecimal totalAllocated = BigDecimal.ZERO;
			for (int i = 0; i < allocationList.size(); i++) {
				MarketDataInvestmentSecurityAllocation allocation = allocationList.get(i);

				BigDecimal allocateAmount = MathUtils.round(MathUtils.getPercentageOf(security.getInstrument().getHierarchy().getSecurityAllocationType().isAutoRebalancingSupported() ? allocation.getCurrentAllocationWeight() : allocation.getAllocationWeight(), amount, true), 2);
				totalAllocated = MathUtils.add(totalAllocated, allocateAmount);
				// If last - apply difference caused by rounding
				if (i == (allocationList.size() - 1)) {
					allocateAmount = MathUtils.add(allocateAmount, MathUtils.subtract(amount, totalAllocated));
				}
				if (securityAllocationMap.containsKey(allocation.getCurrentSecurity())) {
					allocateAmount = MathUtils.add(allocateAmount, securityAllocationMap.get(allocation.getCurrentSecurity()));
				}
				securityAllocationMap.put(allocation.getCurrentSecurity(), allocateAmount);
			}
		}
		// IS THERE A BETTER WAY TO DETERMINE IF IT'S A SECURITY STRUCTURE?
		else if (!CollectionUtils.isEmpty(getInvestmentInstrumentStructureService().getInvestmentInstrumentStructureListByInstrument(security.getInstrument().getId()))) {
			List<InvestmentSecurityStructureAllocation> allocationList = getInvestmentInstrumentStructureService().getInvestmentSecurityStructureAllocationListForSecurityAndDate(null,
					security.getId(), date, false);
			if (CollectionUtils.isEmpty(allocationList)) {
				throw new ValidationException(generateExceptionMessage(managerAccount,
						"Security [" + security.getLabel()
								+ "] is a structured index, however current securities and weights did not evaluate.  Verify structure is set up correctly for this security and active on ["
								+ DateUtils.fromDateShort(date) + "]"));
			}
			// Sort the list from smallest to largest so last one is the largest and gets applied small amounts for rounding
			allocationList = BeanUtils.sortWithFunction(allocationList, InvestmentSecurityStructureAllocation::getAllocationWeight, true);
			BigDecimal totalAllocated = BigDecimal.ZERO;
			for (int i = 0; i < allocationList.size(); i++) {
				InvestmentSecurityStructureAllocation allocation = allocationList.get(i);

				BigDecimal allocateAmount = MathUtils.round(MathUtils.getPercentageOf(allocation.getAllocationWeight(), amount, true), 2);
				totalAllocated = MathUtils.add(totalAllocated, allocateAmount);
				// If last - apply difference caused by rounding
				if (i == (allocationList.size() - 1)) {
					allocateAmount = MathUtils.add(allocateAmount, MathUtils.subtract(amount, totalAllocated));
				}
				if (securityAllocationMap.containsKey(allocation.getCurrentSecurity())) {
					allocateAmount = MathUtils.add(allocateAmount, securityAllocationMap.get(allocation.getCurrentSecurity()));
				}
				securityAllocationMap.put(allocation.getCurrentSecurity(), allocateAmount);
			}
		}
		else {
			securityAllocationMap.put(security, amount);
		}

		return securityAllocationMap;
	}


	private boolean isApplySecurityToReplication(InvestmentSecurity security, ProductOverlayAssetClassReplication replication) {
		return getMostUnderlying(security).equals(getMostUnderlying(replication.getSecurity()));
	}


	private InvestmentInstrument getMostUnderlying(InvestmentSecurity security) {
		InvestmentInstrument instrument = security.getInstrument();
		while (instrument.getUnderlyingInstrument() != null) {
			instrument = instrument.getUnderlyingInstrument();
		}
		return instrument;
	}


	////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public InvestmentInstrumentStructureService getInvestmentInstrumentStructureService() {
		return this.investmentInstrumentStructureService;
	}


	public void setInvestmentInstrumentStructureService(InvestmentInstrumentStructureService investmentInstrumentStructureService) {
		this.investmentInstrumentStructureService = investmentInstrumentStructureService;
	}


	public boolean isAdditionalOverlayExposure() {
		return this.additionalOverlayExposure;
	}


	public void setAdditionalOverlayExposure(boolean additionalOverlayExposure) {
		this.additionalOverlayExposure = additionalOverlayExposure;
	}


	public MarketDataInvestmentSecurityAllocationService getMarketDataInvestmentSecurityAllocationService() {
		return this.marketDataInvestmentSecurityAllocationService;
	}


	public void setMarketDataInvestmentSecurityAllocationService(MarketDataInvestmentSecurityAllocationService marketDataInvestmentSecurityAllocationService) {
		this.marketDataInvestmentSecurityAllocationService = marketDataInvestmentSecurityAllocationService;
	}
}
