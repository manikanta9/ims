package com.clifton.product.overlay.trade.group.dynamic.option.account;

import com.clifton.core.util.compare.CompareUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * <code>ProductOverlayRunOptionTradeReplicationHolder</code> is an object that holds {@link ProductOverlayAssetClassReplication}
 * objects for a client {@link InvestmentAccount} with the replications separated out into different Lists according to
 * Option types (Put/Call) and their matching exposures.
 *
 * @author NickK
 */
public class ProductOverlayRunOptionTradeReplicationHolder {

	private PortfolioRun portfolioRun;

	private InvestmentAccount clientAccount;

	// Can be used to set information - i.e. reviewing the submitted trades and now the account is no longer part of the group
	private String warningMessage;

	//  Expiring Options Information
	private ProductOverlayRunOptionReplicationHolder expiringPutReplicationHolder = new ProductOverlayRunOptionReplicationHolder();
	private ProductOverlayRunOptionReplicationHolder expiringCallReplicationHolder = new ProductOverlayRunOptionReplicationHolder();
	private ProductOverlayRunOptionReplicationHolder expiringPutTradedInstrumentReplicationHolder = new ProductOverlayRunOptionReplicationHolder();
	private ProductOverlayRunOptionReplicationHolder expiringCallTradedInstrumentReplicationHolder = new ProductOverlayRunOptionReplicationHolder();

	/**
	 * Includes all puts or calls exposure on the run, EXCEPT expiring exposure and new contract exposure (that we are currently creating trades for)
	 * Useful for % excess calculation where we remove what is expiring and add in what is new
	 */
	private ProductOverlayRunOptionReplicationHolder putReplicationHolder = new ProductOverlayRunOptionReplicationHolder();
	private ProductOverlayRunOptionReplicationHolder callReplicationHolder = new ProductOverlayRunOptionReplicationHolder();

	/**
	 * Exposure from the replications as defined by the account calculator if it applies.
	 * For Puts, this is generally Cash Equivalents (i.e. T-Bills), and for Calls this is generally Equities
	 * Label is available so the account calculators can specify what is is for those accounts that deviate from the default rules
	 */
	private String putMatchingReplicationLabel;
	private ProductOverlayRunOptionReplicationHolder putMatchingReplicationHolder = new ProductOverlayRunOptionReplicationHolder();

	/**
	 * Label/Value for additional exposure outside of replications, i.e. Cash, 50% of Total Portfolio Value, etc.
	 */
	private String putMatchingAdditionalExposureLabel;
	private BigDecimal putMatchingAdditionalExposure;

	private String callMatchingReplicationLabel;
	private ProductOverlayRunOptionReplicationHolder callMatchingReplicationHolder = new ProductOverlayRunOptionReplicationHolder();

	private String callMatchingAdditionalExposureLabel;
	private BigDecimal callMatchingAdditionalExposure;


	/**
	 * Contract Value of One Contract of the New Put/Call
	 * Calculated on screen to determine the impact of the quantities on the put and call trades
	 */
	private BigDecimal putContractValue;
	private BigDecimal callContractValue;

	// price fields for calculating market value based on the traded quantities
	private BigDecimal putPrice;
	private BigDecimal callPrice;
	private BigDecimal putPriceMultiplier;
	private BigDecimal callPriceMultiplier;

	private BigDecimal putTradeQuantity;
	private BigDecimal callTradeQuantity;

	private BigDecimal putTrancheTradeAverage;
	private BigDecimal callTrancheTradeAverage;

	/**
	 * The following are copied from ProductOverlayRunTradeGroupDynamicOptionRoll object to make it
	 * easier for the grid to determine how to calculate - not different based on each account
	 */
	private Boolean doNotRemoveExpiringOptionsFromExcessCalculation;
	private Boolean longPositions;


	// Not using the cash balance or cash total from the portfolio run because it is not
	// accurate for all DE/VRP clients. Using the Cash asset class total exposure, which
	// sums the cash allocations (e.g. managed accounts, IMS managed, etc.).
	private BigDecimal cashBalance;

	///////////////////////////////////////////////////////////////////////////


	public String getPutOptionsTooltipString() {
		ProductOverlayRunOptionReplicationHolder replicationHolder = ProductOverlayRunOptionReplicationHolder.of(getExpiringPutReplicationHolder());
		replicationHolder.addAll(getPutReplicationHolder());
		// Sort by Expiring First
		replicationHolder.sort((o1, o2) -> CompareUtils.compare(o1.getSecurity().getLastDeliveryDate(), o2.getSecurity().getLastDeliveryDate()));
		return replicationHolder.getTooltipString();
	}


	public ProductOverlayRunOptionReplicationHolder getExpiringPutReplicationHolder() {
		return this.expiringPutReplicationHolder;
	}


	public ProductOverlayRunOptionReplicationHolder getExpiringPutTradedInstrumentReplicationHolder() {
		return this.expiringPutTradedInstrumentReplicationHolder;
	}


	public ProductOverlayRunOptionReplicationHolder getPutReplicationHolder() {
		return this.putReplicationHolder;
	}


	public ProductOverlayRunOptionReplicationHolder getPutMatchingReplicationHolder() {
		return this.putMatchingReplicationHolder;
	}

	////////////////////////////////////////////////////////////////////////////////


	public String getCallOptionsTooltipString() {
		ProductOverlayRunOptionReplicationHolder replicationHolder = ProductOverlayRunOptionReplicationHolder.of(getExpiringCallReplicationHolder());
		replicationHolder.addAll(getCallReplicationHolder());
		// Sort by Expiring First
		replicationHolder.sort((o1, o2) -> CompareUtils.compare(o1.getSecurity().getLastDeliveryDate(), o2.getSecurity().getLastDeliveryDate()));
		return replicationHolder.getTooltipString();
	}


	public ProductOverlayRunOptionReplicationHolder getExpiringCallReplicationHolder() {
		return this.expiringCallReplicationHolder;
	}


	public ProductOverlayRunOptionReplicationHolder getExpiringCallTradedInstrumentReplicationHolder() {
		return this.expiringCallTradedInstrumentReplicationHolder;
	}


	public ProductOverlayRunOptionReplicationHolder getCallReplicationHolder() {
		return this.callReplicationHolder;
	}


	public ProductOverlayRunOptionReplicationHolder getCallMatchingReplicationHolder() {
		return this.callMatchingReplicationHolder;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	public void setPortfolioRun(PortfolioRun portfolioRun) {
		this.portfolioRun = portfolioRun;
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public String getPutMatchingReplicationLabel() {
		return this.putMatchingReplicationLabel;
	}


	public void setPutMatchingReplicationLabel(String putMatchingReplicationLabel) {
		this.putMatchingReplicationLabel = putMatchingReplicationLabel;
	}


	public String getPutMatchingAdditionalExposureLabel() {
		return this.putMatchingAdditionalExposureLabel;
	}


	public void setPutMatchingAdditionalExposureLabel(String putMatchingAdditionalExposureLabel) {
		this.putMatchingAdditionalExposureLabel = putMatchingAdditionalExposureLabel;
	}


	public BigDecimal getPutMatchingAdditionalExposure() {
		return this.putMatchingAdditionalExposure;
	}


	public void setPutMatchingAdditionalExposure(BigDecimal putMatchingAdditionalExposure) {
		this.putMatchingAdditionalExposure = putMatchingAdditionalExposure;
	}


	public String getCallMatchingReplicationLabel() {
		return this.callMatchingReplicationLabel;
	}


	public void setCallMatchingReplicationLabel(String callMatchingReplicationLabel) {
		this.callMatchingReplicationLabel = callMatchingReplicationLabel;
	}


	public String getCallMatchingAdditionalExposureLabel() {
		return this.callMatchingAdditionalExposureLabel;
	}


	public void setCallMatchingAdditionalExposureLabel(String callMatchingAdditionalExposureLabel) {
		this.callMatchingAdditionalExposureLabel = callMatchingAdditionalExposureLabel;
	}


	public BigDecimal getCallMatchingAdditionalExposure() {
		return this.callMatchingAdditionalExposure;
	}


	public void setCallMatchingAdditionalExposure(BigDecimal callMatchingAdditionalExposure) {
		this.callMatchingAdditionalExposure = callMatchingAdditionalExposure;
	}


	public BigDecimal getPutContractValue() {
		return this.putContractValue;
	}


	public void setPutContractValue(BigDecimal putContractValue) {
		this.putContractValue = putContractValue;
	}


	public BigDecimal getPutPrice() {
		return this.putPrice;
	}


	public void setPutPrice(BigDecimal putPrice) {
		this.putPrice = putPrice;
	}


	public BigDecimal getPutPriceMultiplier() {
		return this.putPriceMultiplier;
	}


	public void setPutPriceMultiplier(BigDecimal putPriceMultiplier) {
		this.putPriceMultiplier = putPriceMultiplier;
	}


	public BigDecimal getCallContractValue() {
		return this.callContractValue;
	}


	public void setCallContractValue(BigDecimal callContractValue) {
		this.callContractValue = callContractValue;
	}


	public BigDecimal getCallPrice() {
		return this.callPrice;
	}


	public void setCallPrice(BigDecimal callPrice) {
		this.callPrice = callPrice;
	}


	public BigDecimal getCallPriceMultiplier() {
		return this.callPriceMultiplier;
	}


	public void setCallPriceMultiplier(BigDecimal callPriceMultiplier) {
		this.callPriceMultiplier = callPriceMultiplier;
	}


	public String getWarningMessage() {
		return this.warningMessage;
	}


	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}


	public BigDecimal getPutTradeQuantity() {
		return this.putTradeQuantity;
	}


	public void setPutTradeQuantity(BigDecimal putTradeQuantity) {
		this.putTradeQuantity = MathUtils.abs(putTradeQuantity);
	}


	public BigDecimal getCallTradeQuantity() {
		return this.callTradeQuantity;
	}


	public void setCallTradeQuantity(BigDecimal callTradeQuantity) {
		this.callTradeQuantity = MathUtils.abs(callTradeQuantity);
	}


	public BigDecimal getPutTrancheTradeAverage() {
		return this.putTrancheTradeAverage;
	}


	public void setPutTrancheTradeAverage(BigDecimal putTrancheTradeAverage) {
		this.putTrancheTradeAverage = putTrancheTradeAverage;
	}


	public BigDecimal getCallTrancheTradeAverage() {
		return this.callTrancheTradeAverage;
	}


	public void setCallTrancheTradeAverage(BigDecimal callTrancheTradeAverage) {
		this.callTrancheTradeAverage = callTrancheTradeAverage;
	}


	public Boolean getDoNotRemoveExpiringOptionsFromExcessCalculation() {
		return this.doNotRemoveExpiringOptionsFromExcessCalculation;
	}


	public void setDoNotRemoveExpiringOptionsFromExcessCalculation(Boolean doNotRemoveExpiringOptionsFromExcessCalculation) {
		this.doNotRemoveExpiringOptionsFromExcessCalculation = doNotRemoveExpiringOptionsFromExcessCalculation;
	}


	public Boolean getLongPositions() {
		return this.longPositions;
	}


	public void setLongPositions(Boolean longPositions) {
		this.longPositions = longPositions;
	}


	public BigDecimal getCashBalance() {
		return this.cashBalance;
	}


	public void setCashBalance(BigDecimal cashBalance) {
		this.cashBalance = cashBalance;
	}
}
