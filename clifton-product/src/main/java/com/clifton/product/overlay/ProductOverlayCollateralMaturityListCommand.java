package com.clifton.product.overlay;

import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.security.user.SecurityUser;

import java.util.Date;
import java.util.List;


/**
 * <code>ProductOverlayCollateralMaturityListCommand</code> is a command used for collateral management.
 * This command can be used for viewing collateral based positions and creating trades.
 *
 * @author nickk
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class ProductOverlayCollateralMaturityListCommand extends BeanListCommand<ProductOverlayCollateralMaturity> {

	private PortfolioRun portfolioRun;

	/**
	 * Date optionally provided to specify the trade date for generated trades.
	 */
	private Date tradeDate;

	/**
	 * Optional trader to assign created trades to after initially created.
	 */
	private SecurityUser tradeAssignee;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public List<ProductOverlayCollateralMaturity> getCollateralMaturityList() {
		return getBeanList();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	public void setPortfolioRun(PortfolioRun portfolioRun) {
		this.portfolioRun = portfolioRun;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public SecurityUser getTradeAssignee() {
		return this.tradeAssignee;
	}


	public void setTradeAssignee(SecurityUser tradeAssignee) {
		this.tradeAssignee = tradeAssignee;
	}
}
