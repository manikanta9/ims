package com.clifton.product.overlay.rebalance.calculators;


import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;


/**
 * The <code>BaseProductOverlayRebalanceCalculatorImpl</code> ...
 *
 * @author manderson
 */
public abstract class BaseProductOverlayRebalanceCalculatorImpl implements ProductOverlayRebalanceCalculator {

	private RebalanceCalculationTypes rebalanceCalculationType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public RebalanceCalculationTypes getRebalanceCalculationType() {
		return this.rebalanceCalculationType;
	}


	public void setRebalanceCalculationType(RebalanceCalculationTypes rebalanceCalculationType) {
		this.rebalanceCalculationType = rebalanceCalculationType;
	}
}
