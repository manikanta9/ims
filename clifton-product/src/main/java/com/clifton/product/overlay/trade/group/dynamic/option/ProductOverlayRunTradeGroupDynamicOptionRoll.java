package com.clifton.product.overlay.trade.group.dynamic.option;

import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.portfolio.run.trade.group.PortfolioRunTradeGroup;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionTradeCommand;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunTradeGroupDynamicOptionAccountRoll;
import com.clifton.trade.group.dynamic.BaseTradeGroupDynamicOptionStrangle;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;


/**
 * ProductOverlayRunTradeGroupDynamicOptionRoll object is a type of TradeGroupDynamic that is used for Defensive Equity when buying new
 * Puts/Class to replace expiring ones.
 * <p>
 * Customized properties are saved on the TradeGroup as the Note field as
 * propertyName:propertyValue
 * propertyName2:propertyValue2, etc.
 *
 * @author manderson
 */
public class ProductOverlayRunTradeGroupDynamicOptionRoll extends BaseTradeGroupDynamicOptionStrangle<ProductOverlayRunTradeGroupDynamicOptionAccountRoll> implements PortfolioRunTradeGroup {

	/**
	 * Portfolio Run Trade Group Properties
	 */
	private InvestmentAccountGroup clientAccountGroup;
	private Date balanceDate;
	private boolean marketOnClose; // Don't think these options rolls would use MOC runs...
	private boolean validateRuns; // Option to ensure each account has a run available for trading on the balance date
	private String warningMessage;

	/*
	 * Property holder for populating details for the Option Roll. This command
	 * is used by calculators to populate details from the PortfolioRun for each
	 * client InvestmentAccount determined by the provided values.
	 */
	private ProductOverlayRunOptionTradeCommand runOptionTradeCommand;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<String> getSavePropertiesAsTradeGroupNote() {
		return CollectionUtils.createList("clientAccountGroup.id", "balanceDate", "marketOnClose", "expiringOptionsDate", "longPositions", "doNotRemoveExpiringOptionsFromExcessCalculation");
	}


	@Override
	public ServiceProcessingTypes getProcessingType() {
		return ServiceProcessingTypes.PORTFOLIO_RUNS;
	}


	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder(16);
		if (getClientAccountGroup() != null) {
			sb.append(getClientAccountGroup().getName());
			sb.append(" ");
		}
		if (isMarketOnClose()) {
			sb.append("MOC Runs ");
		}
		else {
			sb.append("Main Runs ");
		}
		if (getBalanceDate() != null) {
			sb.append("on ");
			sb.append(DateUtils.fromDateShort(getBalanceDate()));
		}
		return sb.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	// Convenience methods to use the member trade command


	public ProductOverlayRunOptionTradeCommand getProductOverlayRunOptionTradeCommand() {
		return this.runOptionTradeCommand;
	}


	@ValueChangingSetter
	public void setProductOverlayRunOptionTradeCommand(ProductOverlayRunOptionTradeCommand runOptionTradeCommand) {
		this.runOptionTradeCommand = runOptionTradeCommand;
		// Set the flag to populate new Trade quantities for Put and Call rolling
		this.runOptionTradeCommand.setCalculateNewTradeQuantities(isNewBean());
	}


	private <R> R getProductOverlayRunOptionTradeCommandValue(Function<ProductOverlayRunOptionTradeCommand, R> valueGetter) {
		return ObjectUtils.map(getProductOverlayRunOptionTradeCommand(), valueGetter).get();
	}


	private void setProductOverlayRunOptionTradeCommandValue(Consumer<ProductOverlayRunOptionTradeCommand> valueSetter) {
		ObjectUtils.doIfPresent(getProductOverlayRunOptionTradeCommand(), valueSetter)
				.orElse(() -> {
					setProductOverlayRunOptionTradeCommand(new ProductOverlayRunOptionTradeCommand());
					valueSetter.accept(getProductOverlayRunOptionTradeCommand());
				});
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrument getOptionInstrument() {
		return getProductOverlayRunOptionTradeCommandValue(ProductOverlayRunOptionTradeCommand::getOptionInstrument);
	}


	public void setOptionInstrument(InvestmentInstrument optionInstrument) {
		setProductOverlayRunOptionTradeCommandValue(command -> command.setOptionInstrument(optionInstrument));
	}


	public Date getExpiringOptionsDate() {
		return getProductOverlayRunOptionTradeCommandValue(ProductOverlayRunOptionTradeCommand::getExpiringOptionsDate);
	}


	public void setExpiringOptionsDate(Date expiringOptionsDate) {
		setProductOverlayRunOptionTradeCommandValue(command -> command.setExpiringOptionsDate(expiringOptionsDate));
	}


	public boolean isDoNotRemoveExpiringOptionsFromExcessCalculation() {
		return getProductOverlayRunOptionTradeCommandValue(command -> {
			Boolean doNotRemoveExpiringOptionsFromExcessCalculation = command.getDoNotRemoveExpiringOptionsFromExcessCalculation();
			return doNotRemoveExpiringOptionsFromExcessCalculation != null && doNotRemoveExpiringOptionsFromExcessCalculation;
		});
	}


	public void setDoNotRemoveExpiringOptionsFromExcessCalculation(boolean doNotRemoveExpiringOptionsFromExcessCalculation) {
		setProductOverlayRunOptionTradeCommandValue(command -> command.setDoNotRemoveExpiringOptionsFromExcessCalculation(doNotRemoveExpiringOptionsFromExcessCalculation));
	}


	public BigDecimal getNewTradeDefaultPercentage() {
		return getProductOverlayRunOptionTradeCommandValue(ProductOverlayRunOptionTradeCommand::getNewTradeDefaultPercentage);
	}


	public void setNewTradeDefaultPercentage(BigDecimal newTradeDefaultPercentage) {
		setProductOverlayRunOptionTradeCommandValue(command -> command.setNewTradeDefaultPercentage(newTradeDefaultPercentage));
	}


	public ProductOverlayRunOptionTradeCommand.ProductOverlayRunTradeGroupDynamicOptionRollPercentageType getNewTradeDefaultPercentageType() {
		return getProductOverlayRunOptionTradeCommandValue(ProductOverlayRunOptionTradeCommand::getNewTradeDefaultPercentageType);
	}


	public void setNewTradeDefaultPercentageType(ProductOverlayRunOptionTradeCommand.ProductOverlayRunTradeGroupDynamicOptionRollPercentageType newTradeDefaultPercentageType) {
		setProductOverlayRunOptionTradeCommandValue(command -> command.setNewTradeDefaultPercentageType(newTradeDefaultPercentageType));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Use the Trade Group Security and Second Security, but Use Specific Getter/Setters to make it clear which is which


	@Override
	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		setPutSecurity(investmentSecurity);
	}


	@Override
	public InvestmentSecurity getPutSecurity() {
		return getInvestmentSecurity();
	}


	@Override
	public void setPutSecurity(InvestmentSecurity putSecurity) {
		super.setInvestmentSecurity(putSecurity);
		setProductOverlayRunOptionTradeCommandValue(command -> command.setPutSecurity(putSecurity));
	}


	@Override
	public void setSecondaryInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		setCallSecurity(investmentSecurity);
	}


	@Override
	public InvestmentSecurity getCallSecurity() {
		return getSecondaryInvestmentSecurity();
	}


	@Override
	public void setCallSecurity(InvestmentSecurity callSecurity) {
		super.setSecondaryInvestmentSecurity(callSecurity);
		setProductOverlayRunOptionTradeCommandValue(command -> command.setCallSecurity(callSecurity));
	}


	@Override
	public void setLongPositions(boolean longPositions) {
		super.setLongPositions(longPositions);
		setProductOverlayRunOptionTradeCommandValue(command -> command.setLongPositions(longPositions));
	}


	public boolean isCalculateTrancheTradeAverage() {
		return getProductOverlayRunOptionTradeCommandValue(ProductOverlayRunOptionTradeCommand::isCalculateTrancheTradeAverage);
	}


	public void setCalculateTrancheTradeAverage(boolean calculateTrancheTradeAverage) {
		setProductOverlayRunOptionTradeCommandValue(command -> command.setCalculateTrancheTradeAverage(calculateTrancheTradeAverage));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountGroup getClientAccountGroup() {
		return this.clientAccountGroup;
	}


	public void setClientAccountGroup(InvestmentAccountGroup clientAccountGroup) {
		this.clientAccountGroup = clientAccountGroup;
	}


	@Override
	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
		setProductOverlayRunOptionTradeCommandValue(command -> command.setBalanceDate(balanceDate));
	}


	@Override
	public boolean isMarketOnClose() {
		return this.marketOnClose;
	}


	public void setMarketOnClose(boolean marketOnClose) {
		this.marketOnClose = marketOnClose;
	}


	@Override
	public boolean isValidateRuns() {
		return this.validateRuns;
	}


	public void setValidateRuns(boolean validateRuns) {
		this.validateRuns = validateRuns;
	}


	@Override
	public String getWarningMessage() {
		return this.warningMessage;
	}


	@Override
	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}


	public Boolean getCalculateCashBalance() {
		return getProductOverlayRunOptionTradeCommandValue(ProductOverlayRunOptionTradeCommand::getCalculateCashBalance);
	}


	public void setCalculateCashBalance(Boolean calculateCashBalance) {
		setProductOverlayRunOptionTradeCommandValue(command -> command.setCalculateCashBalance(BooleanUtils.isTrue(calculateCashBalance)));
	}


	public Boolean getCalculateCashTradeImpact() {
		return getProductOverlayRunOptionTradeCommandValue(ProductOverlayRunOptionTradeCommand::getCalculateCashTradeImpact);
	}


	public void setCalculateCashTradeImpact(Boolean calculateCashTradeImpact) {
		setProductOverlayRunOptionTradeCommandValue(command -> command.setCalculateCashTradeImpact(BooleanUtils.isTrue(calculateCashTradeImpact)));
	}
}
