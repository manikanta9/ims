package com.clifton.product.overlay.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAllocationConfigTypes;
import com.clifton.investment.manager.ManagerCustomCashOverlayTypes;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.manager.BasePortfolioRunManagerAccountRuleEvaluator;
import com.clifton.product.overlay.manager.allocation.ProductOverlayManagerAllocationCalculator;
import com.clifton.product.overlay.manager.allocation.ProductOverlayManagerAllocationConfig;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductOverlayManagerCustomOverlayRuleEvaluator</code>class is used to generate rule violations when non-standard cash overlay allocations are used.
 * Can be used for Custom Allocations and/or Overlay Actions.
 *
 * @author manderson
 */
public class ProductOverlayManagerCustomOverlayRuleEvaluator extends BasePortfolioRunManagerAccountRuleEvaluator<PortfolioRun, ProductOverlayRuleEvaluatorContext> {


	private ProductOverlayManagerAllocationCalculator productOverlayManagerAllocationCalculator;

	/**
	 * Optional ability to include in violations manager assignments that use overlay actions, but don't use custom overlay allocations
	 */
	private boolean includeOverlayActionWithoutCustomOverlay;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, ProductOverlayRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();

		Map<InvestmentManagerAccountAssignment, List<ProductOverlayManagerAllocationConfig>> managerAssignmentCustomOverlayListMap = getInvestmentManagerAssignmentCustomOverlayMap(run, context);

		for (Map.Entry<InvestmentManagerAccountAssignment, List<ProductOverlayManagerAllocationConfig>> managerAssignmentEntry : managerAssignmentCustomOverlayListMap.entrySet()) {
			InvestmentManagerAccount managerAccount = managerAssignmentEntry.getKey().getReferenceOne();
			EntityConfig entityConfig = getEntityConfig(managerAccount, ruleConfig);
			if (entityConfig != null && !entityConfig.isExcluded()) {
				Map<String, Object> templateValues = new HashMap<>();
				templateValues.put("overlayAllocationConfigList", BeanUtils.sortWithFunction(managerAssignmentEntry.getValue(), ProductOverlayManagerAllocationConfig::getOrder, true));
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), BeanUtils.getIdentityAsLong(managerAccount), BeanUtils.getIdentityAsLong(managerAssignmentEntry.getKey()), null, templateValues));
			}
		}
		return ruleViolationList;
	}


	private Map<InvestmentManagerAccountAssignment, List<ProductOverlayManagerAllocationConfig>> getInvestmentManagerAssignmentCustomOverlayMap(PortfolioRun run, ProductOverlayRuleEvaluatorContext context) {
		// Create a Run Config Object to use for the calculator
		ProductOverlayRunConfig runConfig = new ProductOverlayRunConfig(run, false, 0);
		runConfig.setAssetClassList(context.getInvestmentAccountAssetClassListForRun(run));
		getProductOverlayManagerAllocationCalculator().processProductOverlayManagerAllocationConfigList(runConfig, true);
		List<ProductOverlayManagerAllocationConfig> customOverlayManagerConfigList = BeanUtils.filter(runConfig.getProductOverlayManagerAllocationConfigListForConfigType(InvestmentManagerAllocationConfigTypes.OVERLAY_ALLOCATION), this::isIncludeProductOverlayManagerAllocationConfigInViolation);
		return BeanUtils.getBeansMap(customOverlayManagerConfigList, ProductOverlayManagerAllocationConfig::getManagerAccountAssignment);
	}


	private boolean isIncludeProductOverlayManagerAllocationConfigInViolation(ProductOverlayManagerAllocationConfig productOverlayManagerAllocationConfig) {
		// Custom Allocation
		if (productOverlayManagerAllocationConfig.getManagerAccountAssignment().getCustomCashOverlayType() != ManagerCustomCashOverlayTypes.NONE) {
			return true;
		}
		// Otherwise if we are including overlay actions without custom allocations
		else if (isIncludeOverlayActionWithoutCustomOverlay() && (productOverlayManagerAllocationConfig.getOverlayLimitAction() != null && productOverlayManagerAllocationConfig.getOverlayAdjustmentAction() != null)) {
			return true;
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public ProductOverlayManagerAllocationCalculator getProductOverlayManagerAllocationCalculator() {
		return this.productOverlayManagerAllocationCalculator;
	}


	public void setProductOverlayManagerAllocationCalculator(ProductOverlayManagerAllocationCalculator productOverlayManagerAllocationCalculator) {
		this.productOverlayManagerAllocationCalculator = productOverlayManagerAllocationCalculator;
	}


	public boolean isIncludeOverlayActionWithoutCustomOverlay() {
		return this.includeOverlayActionWithoutCustomOverlay;
	}


	public void setIncludeOverlayActionWithoutCustomOverlay(boolean includeOverlayActionWithoutCustomOverlay) {
		this.includeOverlayActionWithoutCustomOverlay = includeOverlayActionWithoutCustomOverlay;
	}
}
