package com.clifton.product.overlay.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.portfolio.run.rule.BaseManagerAccountVsGLAssetsRuleEvaluator;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.overlay.search.ProductOverlayManagerAccountSearchForm;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author stevenf
 */
public class ProductOverlayPIOSNavVsGLAssetsRuleEvaluator extends BaseManagerAccountVsGLAssetsRuleEvaluator {


	private ProductOverlayService productOverlayService;


	@Override
	protected BigDecimal getManagerAccountAssets(Integer runId) {
		BigDecimal piosNavAssets = BigDecimal.ZERO;
		ProductOverlayManagerAccountSearchForm productOverlayManagerAccountSearchForm = new ProductOverlayManagerAccountSearchForm();
		productOverlayManagerAccountSearchForm.setRunId(runId);
		productOverlayManagerAccountSearchForm.setRootOnly(true);
		List<ProductOverlayManagerAccount> productOverlayManagerAccountList =
				getProductOverlayService().getProductOverlayManagerAccountList(productOverlayManagerAccountSearchForm);
		for (ProductOverlayManagerAccount productOverlayManagerAccount : CollectionUtils.getIterable(productOverlayManagerAccountList)) {
			piosNavAssets = MathUtils.add(piosNavAssets, MathUtils.add(productOverlayManagerAccount.getCashAllocation(),
					productOverlayManagerAccount.getSecuritiesAllocation()));
		}
		return piosNavAssets;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}
}
