package com.clifton.product.overlay.manager.allocation;

import com.clifton.product.overlay.process.ProductOverlayRunConfig;


/**
 * The <code>ProductOverlayManagerAllocationCalculator</code> interface defines methods for processing manager assignments and their cash and securities allocations as well as overlay allocations
 * This is used during run processing, but separated so can easily be re-used for rule violations or testing these features independently
 *
 * @author manderson
 */
public interface ProductOverlayManagerAllocationCalculator {


	/**
	 * Processes {@link com.clifton.investment.manager.InvestmentManagerAccountAssignment} for the given run and creates a list of {@link ProductOverlayManagerAllocationConfig} objects on the runConfig
	 * Note: preview feature will skip missing balances and should only be used for checking rule violations
	 */
	public void processProductOverlayManagerAllocationConfigList(ProductOverlayRunConfig runConfig, boolean preview);
}
