package com.clifton.product.overlay.trade.group.dynamic.option.account.calculators;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionTradeCommand;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionTradeReplicationHolder;

import java.util.List;


/**
 * The <code>ProductOverlayRunTradeGroupDynamicOptionAccountRollUsingAssetClassCalculator</code> is used for accounts that use generic logic of Calls being matched to Equities and Puts to Cash and Cash Equivalents (where cash can easily be turned off for some account overrides)
 *
 * @author manderson
 */
public class ProductOverlayRunTradeGroupDynamicOptionAccountRollUsingAssetClassCalculator extends BaseProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator {

	// Options are defined in System List "Option Roll Exposure Replication Options"
	private static final String ALL_NON_OPTIONS = "All Non-Options";
	private static final String OPTIONS_ONLY = "Options Only";
	private static final String OPTIONS_ONLY_NOT_EXPIRING = "Options Only (Not Expiring)";

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Usually is included with the put matching exposure, but this allows it to be configurable - or not used at all
	 */
	private InvestmentSecurityOptionTypes cashOptionTypeExposure;

	/**
	 * If specified, limits which replications in the asset classes to include - All Non-Options, Options Only, Options Only (Not Expiring)
	 */
	private String callMatchingExposureReplicationOption;

	/**
	 * i.e. Specify Equities for Call Matching Calculations
	 */
	private List<Short> callMatchingExposureAssetClassIds;

	/**
	 * If specified, limits which replications in the asset classes to include - All Non-Options, Options Only, Options Only (Not Expiring)
	 */
	private String putMatchingExposureReplicationOption;

	/**
	 * i.e. Specify Cash Equivalents/T-Bills for Put Matching Calculations
	 */
	private List<Short> putMatchingExposureAssetClassIds;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyReplicationToMatchingExposure(ProductOverlayRunOptionTradeCommand tradeCommand, ProductOverlayRunOptionTradeReplicationHolder replicationHolder, ProductOverlayAssetClassReplication replication) {
		if (!CollectionUtils.isEmpty(getCallMatchingExposureAssetClassIds()) && getCallMatchingExposureAssetClassIds().contains(replication.getOverlayAssetClass().getAccountAssetClass().getAssetClass().getId())) {
			if (isReplicationIncludedInMatchingExposure(tradeCommand, replication, getCallMatchingExposureReplicationOption())) {
				replicationHolder.getCallMatchingReplicationHolder().add(replication);
			}
		}
		if (!CollectionUtils.isEmpty(getPutMatchingExposureAssetClassIds()) && getPutMatchingExposureAssetClassIds().contains(replication.getOverlayAssetClass().getAccountAssetClass().getAssetClass().getId())) {
			if (isReplicationIncludedInMatchingExposure(tradeCommand, replication, getPutMatchingExposureReplicationOption())) {
				replicationHolder.getPutMatchingReplicationHolder().add(replication);
			}
		}
	}


	/**
	 * Optionally sets Cash Exposure as call or put matching additional exposure (or neither)
	 */
	@Override
	public void applyAdditionalMatchingExposure(ProductOverlayRunOptionTradeReplicationHolder replicationHolder, PortfolioRun portfolioRun) {
		if (getCashOptionTypeExposure() == InvestmentSecurityOptionTypes.CALL) {
			replicationHolder.setCallMatchingAdditionalExposure(portfolioRun.getCashExposure());
		}
		else if (getCashOptionTypeExposure() == InvestmentSecurityOptionTypes.PUT) {
			replicationHolder.setPutMatchingAdditionalExposure(portfolioRun.getCashExposure());
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private boolean isReplicationIncludedInMatchingExposure(ProductOverlayRunOptionTradeCommand tradeCommand, ProductOverlayAssetClassReplication replication, String matchingExposureReplicationOption) {
		// If not set, includes everything
		if (StringUtils.isEmpty(matchingExposureReplicationOption)) {
			return true;
		}
		InvestmentSecurity security = replication.getSecurity();
		if (InvestmentUtils.isSecurityOfType(security, InvestmentType.OPTIONS)) {
			if (OPTIONS_ONLY.equals(matchingExposureReplicationOption)) {
				return true;
			}
			if (OPTIONS_ONLY_NOT_EXPIRING.equals(matchingExposureReplicationOption) && !(tradeCommand.isSecurityExpiring(security, replication.getActualContractsAdjusted()) && tradeCommand.isSecurityApplicableToInstrument(security))) {
				return true;
			}
		}
		else {
			if (ALL_NON_OPTIONS.equals(matchingExposureReplicationOption)) {
				return true;
			}
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityOptionTypes getCashOptionTypeExposure() {
		return this.cashOptionTypeExposure;
	}


	public void setCashOptionTypeExposure(InvestmentSecurityOptionTypes cashOptionTypeExposure) {
		this.cashOptionTypeExposure = cashOptionTypeExposure;
	}


	public List<Short> getCallMatchingExposureAssetClassIds() {
		return this.callMatchingExposureAssetClassIds;
	}


	public void setCallMatchingExposureAssetClassIds(List<Short> callMatchingExposureAssetClassIds) {
		this.callMatchingExposureAssetClassIds = callMatchingExposureAssetClassIds;
	}


	public List<Short> getPutMatchingExposureAssetClassIds() {
		return this.putMatchingExposureAssetClassIds;
	}


	public void setPutMatchingExposureAssetClassIds(List<Short> putMatchingExposureAssetClassIds) {
		this.putMatchingExposureAssetClassIds = putMatchingExposureAssetClassIds;
	}


	public String getCallMatchingExposureReplicationOption() {
		return this.callMatchingExposureReplicationOption;
	}


	public void setCallMatchingExposureReplicationOption(String callMatchingExposureReplicationOption) {
		this.callMatchingExposureReplicationOption = callMatchingExposureReplicationOption;
	}


	public String getPutMatchingExposureReplicationOption() {
		return this.putMatchingExposureReplicationOption;
	}


	public void setPutMatchingExposureReplicationOption(String putMatchingExposureReplicationOption) {
		this.putMatchingExposureReplicationOption = putMatchingExposureReplicationOption;
	}
}
