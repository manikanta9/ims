package com.clifton.product.overlay;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.replication.ProductReplicationService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>ProductOverlayAssetClassReplicationObserver</code>
 * <p/>
 * Validates for Update Only, if the security for the replication is being changed.
 * Validation: There cannot be ANY actual contracts, Security must be for the same Instrument that it was
 * <p/>
 * Populates all Security Related Info for the new Security
 * <p/>
 * Also, if there are other replications for that run that use "replicationPositionsExcludedFromCount" that had the original security and no contracts, it will also be updated to reflect
 * the new security
 *
 * @author Mary Anderson
 */
@Component
public class ProductOverlayAssetClassReplicationObserver extends SelfRegisteringDaoObserver<ProductOverlayAssetClassReplication> {

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private ProductOverlayService productOverlayService;
	private ProductReplicationService productReplicationService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<ProductOverlayAssetClassReplication> dao, @SuppressWarnings("unused") DaoEventTypes event, ProductOverlayAssetClassReplication bean) {
		ProductOverlayAssetClassReplication originalBean = getOriginalBean(dao, bean);
		if (originalBean != null) {
			if (originalBean.getOverlayAssetClass().getOverlayRun().isClosed()) {
				throw new ValidationException("This run record is Closed. Changes are not allowed.");
			}
			if (originalBean.isCashExposure()) {
				throw new ValidationException("You cannot update Cash Exposure properties because it is not a tradable replication.");
			}
			List<String> updatedFields = CoreCompareUtils.getNoEqualProperties(originalBean, bean, false);
			if (!bean.isTradeEntryDisabled() && !CollectionUtils.isEmpty(updatedFields)) {
				PortfolioRun run = bean.getOverlayAssetClass().getOverlayRun();
				List<ProductOverlayAssetClassReplication> fullRunRepList = getProductOverlayService().getProductOverlayAssetClassReplicationListByRun(run.getId());

				// If a security change - special logic to update security information (prices, etc), validate security, and also change security for trading disabled if the securities match up
				if (updatedFields.contains("security")) {

					validateSecurityChange(originalBean, bean, fullRunRepList);

					// Use the Original Bean, so that we don't accidentally overwrite any data
					InvestmentSecurity originalSecurity = originalBean.getSecurity();
					InvestmentSecurity security = bean.getSecurity();
					bean.setSecurityPrice(null); // Clear Price - If set doesn't recalculate it

					// If we are OK to change, update and populate Security Related Info
					String dsName = getMarketDataExchangeRatesApiService().getExchangeRateDataSourceForClientAccount(run.getClientInvestmentAccount().toClientAccount());
					getProductReplicationService().setupProductOverlayAssetClassReplicationSecurityInfo(bean, security, dsName, false);

					// If not position excluded replication, check if there is one with the same security and update that security
					if (!bean.getOverlayAssetClass().getAccountAssetClass().isReplicationPositionExcludedFromCount()) {
						List<ProductOverlayAssetClassReplication> affectedList = CollectionUtils.getStream(fullRunRepList)
								.filter(r -> CompareUtils.isEqual(r.getSecurity(), originalSecurity))
								.filter(ProductOverlayAssetClassReplication::isTradeEntryDisabled)
								.collect(Collectors.toList());

						for (ProductOverlayAssetClassReplication affectedBean : CollectionUtils.getIterable(affectedList)) {
							if (MathUtils.isEqual(affectedBean.getActualContracts(), 0)) {
								if (getProductReplicationService().validateProductOverlayAssetClassReplicationSecurityChange(affectedBean, security, false, fullRunRepList)) {
									affectedBean.setSecurity(security);
									affectedBean.setSecurityPrice(null); // Clear Price so resets it
									affectedBean.setTradeSecurityPrice(bean.getTradeSecurityPrice());
									affectedBean.setTradeUnderlyingSecurityPrice(bean.getTradeUnderlyingSecurityPrice());
									// update and populate Security Related Info
									getProductReplicationService().setupProductOverlayAssetClassReplicationSecurityInfo(affectedBean, security, dsName, false);
									getProductOverlayService().saveProductOverlayAssetClassReplication(affectedBean);
								}
							}
						}
					}
				}
				// Otherwise, trade pricing changes and then just need to check if security for trading disabled then updates it's properties as well.
				// If not position excluded replication, check if there is one with the same security and update that replication
				else {
					List<ProductOverlayAssetClassReplication> affectedList = CollectionUtils.getStream(fullRunRepList)
							.filter(r -> CompareUtils.isEqual(r.getSecurity(), bean.getSecurity()))
							.filter(ProductOverlayAssetClassReplication::isTradeEntryDisabled)
							.collect(Collectors.toList());

					for (ProductOverlayAssetClassReplication affectedBean : CollectionUtils.getIterable(affectedList)) {
						for (String field : updatedFields) {
							BeanUtils.setPropertyValue(affectedBean, field, BeanUtils.getPropertyValue(bean, field), true);
						}
						getProductOverlayService().saveProductOverlayAssetClassReplication(affectedBean);
					}
				}
			}
		}
	}


	private void validateSecurityChange(ProductOverlayAssetClassReplication originalBean, ProductOverlayAssetClassReplication bean, List<ProductOverlayAssetClassReplication> fullRunRepList) {
		ValidationUtils.assertTrue(MathUtils.isEqual(originalBean.getActualContracts(), 0), "You cannot change the security for this replication, because there are existing contracts.");
		getProductReplicationService().validateProductOverlayAssetClassReplicationSecurityChange(originalBean, bean.getSecurity(), true, fullRunRepList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public ProductReplicationService getProductReplicationService() {
		return this.productReplicationService;
	}


	public void setProductReplicationService(ProductReplicationService productReplicationService) {
		this.productReplicationService = productReplicationService;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}
}
