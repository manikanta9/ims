package com.clifton.product.overlay;


import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portfolio.run.manager.BasePortfolioRunManagerAccount;

import java.util.List;


/**
 * The {@link ProductOverlayManagerAccount} class represents detailed manager account allocations to specific
 * asset classes of our investment accounts.  It represents Phase III of PIOS processing.
 * <p>
 * Note: ProductOverlayManagerAccount could reference ProductOverlayAssetClass instead of the run.  However, it needs to
 * stay this way because this table is populated first and then used to create ProductOverlayAssetClass during processing.
 *
 * @author vgomelsky
 */
public class ProductOverlayManagerAccount extends BasePortfolioRunManagerAccount {

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		String label = "";
		if (getManagerAccountAssignment() != null) {
			label = getManagerAccountAssignment().getReferenceOne().getLabel();
			if (!StringUtils.isEmpty(getManagerAccountAssignment().getDisplayName())) {
				label += " (" + getManagerAccountAssignment().getDisplayName() + ")";
			}
			if (getAccountAssetClass() != null) {
				label += ": " + getAccountAssetClass().getLabel();
			}
			return label;
		}

		return label;
	}


	public String getAssetClassLabel() {
		if (getAccountAssetClass() != null) {
			return getAccountAssetClass().getLabel();
		}
		return null;
	}


	public String getAssetClassLabelExpanded() {
		List<ProductOverlayManagerAccount> beanHierarchy = CollectionUtils.getChain(this, b -> (ProductOverlayManagerAccount) b.getParent());
		CollectionUtils.reverse(beanHierarchy);
		return StringUtils.join(beanHierarchy, ProductOverlayManagerAccount::getAssetClassLabel, NamedHierarchicalEntity.HIERARCHY_DELIMITER);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
}
