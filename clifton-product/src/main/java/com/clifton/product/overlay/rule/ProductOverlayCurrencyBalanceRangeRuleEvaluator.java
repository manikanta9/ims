package com.clifton.product.overlay.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunCurrencyBalanceRangeRuleEvaluator;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is used by several system beans to validate currency balances in the clients base currency to ensure
 * individual unrealized gain/loss as well as total unrealized gain/loss does not fall outside of the specified threshold for an account
 *
 * @author stevenf  on 8/21/2015.
 */
public class ProductOverlayCurrencyBalanceRangeRuleEvaluator extends PortfolioRunCurrencyBalanceRangeRuleEvaluator<ProductOverlayAssetClass, ProductOverlayAssetClassReplication, ProductOverlayRuleEvaluatorContext> {

	private static final String TOTAL_BASE_EXPOSURE_ASSET_CLASS_OVERLAY_TARGET_PERCENT = "TotalBaseExposureAssetClassOverlayTargetPercent";


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, ProductOverlayRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		BigDecimal totalBaseUnrealizedCurrencyExposure = processTotalBaseUnrealizedCurrencyExposure(ruleViolationList, context, run, ruleConfig);
		//Handle this here since it has entity scope.
		if (TOTAL_BASE_EXPOSURE_ASSET_CLASS_OVERLAY_TARGET_PERCENT.equals(getRangeType())) {
			for (ProductOverlayAssetClass assetClass : CollectionUtils.getIterable(context.getRunSummaryList(run, false, false))) {
				Long assetClassId = BeanUtils.getIdentityAsLong(assetClass.getAccountAssetClass());
				EntityConfig entityConfig = ruleConfig.getEntityConfig(assetClassId);
				if (entityConfig != null && !entityConfig.isExcluded()) {
					BigDecimal rangeValue = CoreMathUtils.getPercentValue(totalBaseUnrealizedCurrencyExposure, assetClass.getCashTotal(), true);
					if (rangeValue != null) {
						Map<String, Object> templateValues = new HashMap<>();
						templateValues.put("totalBaseUnrealizedCurrencyExposure", totalBaseUnrealizedCurrencyExposure);
						if (RuleEvaluatorUtils.isEntityConfigRangeViolated(rangeValue, entityConfig, templateValues)) {
							ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), assetClassId, BeanUtils.getIdentityAsLong(assetClass), null, templateValues));
						}
					}
				}
			}
		}
		ruleViolationList.addAll(evaluateTotalCurrencyRules(run, ruleConfig, totalBaseUnrealizedCurrencyExposure));
		return ruleViolationList;
	}
}
