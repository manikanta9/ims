package com.clifton.product.overlay.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


public class ProductPerformanceOverlayAssetClassTrackingErrorSearchForm extends BaseEntitySearchForm {

	@SearchField(searchFieldPath = "productPerformanceOverlayRun.performanceSummary.clientAccount", searchField = "name,number")
	private String searchPattern;

	@SearchField(searchFieldPath = "productPerformanceOverlayRun.performanceSummary", searchField = "clientAccount.id", sortField = "clientAccount.number")
	private Integer clientAccountId;

	@SearchField(searchField = "productPerformanceOverlayRun.measureDate")
	private Date measureDate;

	// Custom Search Field = Find tracking error for a Specific Account or any of its SubAccounts
	private Integer clientInvestmentAccountIdOrSubAccount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getClientInvestmentAccountIdOrSubAccount() {
		return this.clientInvestmentAccountIdOrSubAccount;
	}


	public void setClientInvestmentAccountIdOrSubAccount(Integer clientInvestmentAccountIdOrSubAccount) {
		this.clientInvestmentAccountIdOrSubAccount = clientInvestmentAccountIdOrSubAccount;
	}
}
