package com.clifton.product.overlay.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.manager.BasePortfolioRunManagerAccountRangeRuleEvaluator;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * ProductOverlayManagerBalanceChangeComparisonRuleEvaluator class compares manager asset class total market value to the total market value of the previous run
 * when no benchmark return exists that can be compared to.  It creates a violation if the difference is outside of specified bands
 *
 * @author stevenf
 */
public class ProductOverlayManagerBalanceChangeComparisonRuleEvaluator extends BasePortfolioRunManagerAccountRangeRuleEvaluator<PortfolioRun, ProductOverlayRuleEvaluatorContext> {


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, ProductOverlayRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		List<ProductOverlayManagerAccount> managerAccountList = context.getProductOverlayManagerList(run, false, null);
		Map<Integer, BigDecimal> assetClassBenchmarkReturnMap = context.getAccountAssetClassBenchmarkReturnMap(run);
		for (ProductOverlayManagerAccount overlayManagerAccount : CollectionUtils.getIterable(managerAccountList)) {
			if (!overlayManagerAccount.isRollupAssetClass()) {
				InvestmentManagerAccount managerAccount = overlayManagerAccount.getManagerAccountAssignment().getReferenceOne();
				EntityConfig entityConfig = getEntityConfig(managerAccount, ruleConfig);
				if (entityConfig != null && !entityConfig.isExcluded()) {
					BigDecimal assetClassReturn = assetClassBenchmarkReturnMap.get(overlayManagerAccount.getAccountAssetClass().getId());
					//Only run if there is no benchmark return.
					if (assetClassReturn == null) {
						BigDecimal totalAllocation = overlayManagerAccount.getTotalAllocation();
						BigDecimal previousRunTotalAllocation = overlayManagerAccount.getPreviousDayTotalAllocation();
						BigDecimal percentChange = MathUtils.round(MathUtils.getPercentChange(previousRunTotalAllocation, totalAllocation, true), 2);
						String violationNote = RuleEvaluatorUtils.evaluateValueForRange(percentChange, entityConfig.getMinAmount(), entityConfig.getMaxAmount(), MathUtils.NUMBER_FORMAT_MONEY, true);
						// Do not generate a warning if the manager has a manual adjustment - we already have warning for these so it is redundant
						if (!StringUtils.isEmpty(violationNote) && !context.isManagerBalanceManuallyAdjusted(overlayManagerAccount)) {
							Map<String, Object> templateValues = new HashMap<>();
							templateValues.put("totalAllocation", totalAllocation);
							templateValues.put("previousRunTotalAllocation", previousRunTotalAllocation);
							templateValues.put("percentChange", percentChange);
							ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), BeanUtils.getIdentityAsLong(managerAccount), BeanUtils.getIdentityAsLong(overlayManagerAccount), null, templateValues));
						}
					}
				}
			}
		}
		return ruleViolationList;
	}
}
