package com.clifton.product.overlay.trade.group.dynamic.option;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.trade.PortfolioRunTradeCommand;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.trade.Trade;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * <code>ProductOverlayRunOptionTradeService</code> provides services for providing {@link com.clifton.trade.Trade}
 * functionality based on a {@link com.clifton.portfolio.run.PortfolioRun} to rebalance cash flows for
 * Client {@link com.clifton.investment.account.InvestmentAccount}s that trades options.
 *
 * @author NickK
 * @see com.clifton.portfolio.run.trade.PortfolioRunTradeService
 */
public interface ProductOverlayRunOptionTradeService {

	/**
	 * Populates a {@link ProductOverlayRunOptionTradeGroup} with details based on the provide {@link com.clifton.portfolio.run.PortfolioRun} ID.
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public ProductOverlayRunOptionTradeGroup getProductOverlayRunOptionTradeGroup(int id);


	/**
	 * Updates and/or populates a {@link ProductOverlayRunOptionTradeGroup} with details with prices. Prices will be previous day closing price
	 * unless live prices are requested.
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	@RequestMapping("productOverlayRunOptionTradeGroupPopulate")
	public ProductOverlayRunOptionTradeGroup populateProductOverlayRunOptionTradeGroup(ProductOverlayRunOptionTradeGroup bean, boolean livePrices);


	/**
	 * Populates the requested {@link ProductOverlayRunOptionTradeGroup} while clearing any buy or sell amounts defined on it.
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public ProductOverlayRunOptionTradeGroup clearProductOverlayRunOptionTradeGroupTrades(ProductOverlayRunOptionTradeGroup bean);


	/**
	 * Processes Trade Approval for the provided {@link PortfolioRunTradeCommand} for a {@link com.clifton.portfolio.run.PortfolioRun}
	 * and List of {@link ProductOverlayAssetClassReplication} details.
	 *
	 * @see com.clifton.portfolio.run.trade.PortfolioRunTradeService#processPortfolioRunTradeApproval(int, Integer[])
	 * @see com.clifton.portfolio.run.trade.PortfolioRunTradeHandler#processPortfolioRunTradeApproval(List)
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = Trade.class, permissions = SecurityPermission.PERMISSION_READ)
	public String processProductOverlayRunOptionTradeGroupApproval(PortfolioRunTradeCommand<ProductOverlayAssetClassReplication> command);
}
