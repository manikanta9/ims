package com.clifton.product.overlay.rebalance;


import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;


/**
 * The <code>ProductOverlayRebalanceAction</code> ...
 *
 * @author manderson
 */
public enum ProductOverlayRebalanceActions {

	SAVE_CONFIG(true, false), // Just save account level options
	SAVE_PROCESS_MINI(true, true), // Save account level options and process mini rebalance
	PROCESS_MINI(false, true), // Don't save account level options, but process mini rebalance using selected options
	PROCESS_MANUAL(false, true, RebalanceCalculationTypes.MANUAL), // Process manual rebalance
	PROCESS_FULL(false, true, RebalanceCalculationTypes.FULL); // Process full rebalance


	ProductOverlayRebalanceActions(boolean saveConfig, boolean process) {
		this(saveConfig, process, null);
	}


	ProductOverlayRebalanceActions(boolean saveConfig, boolean process, RebalanceCalculationTypes useCalculationType) {
		this.saveConfig = saveConfig;
		this.process = process;
		this.fullCalculationType = useCalculationType;
	}


	private final boolean saveConfig;
	private final boolean process;
	private final RebalanceCalculationTypes fullCalculationType;


	public boolean isMiniRebalance() {
		return this.fullCalculationType == null;
	}


	public boolean isSaveConfig() {
		return this.saveConfig;
	}


	public boolean isProcess() {
		return this.process;
	}


	public RebalanceCalculationTypes getUseCalculationType() {
		return this.fullCalculationType;
	}
}
