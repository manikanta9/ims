package com.clifton.product.overlay.rebalance.minimizeimbalances;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.system.schema.column.SystemColumnCustom;

import java.util.List;


/**
 * The <code>ProductOverlayMinimizeImbalancesSettings</code> ...
 *
 * @author manderson
 */
@NonPersistentObject
public class ProductOverlayMinimizeImbalancesSettings {

	private InvestmentAccount investmentAccount;

	private String columnGroupName;

	private List<InvestmentAccountAssetClass> accountAssetClassList;

	private List<SystemColumnCustom> columnList;


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public List<InvestmentAccountAssetClass> getAccountAssetClassList() {
		return this.accountAssetClassList;
	}


	public void setAccountAssetClassList(List<InvestmentAccountAssetClass> accountAssetClassList) {
		this.accountAssetClassList = accountAssetClassList;
	}


	public List<SystemColumnCustom> getColumnList() {
		return this.columnList;
	}


	public void setColumnList(List<SystemColumnCustom> columnList) {
		this.columnList = columnList;
	}


	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}
}
