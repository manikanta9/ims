package com.clifton.product.overlay.rule;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass.RebalanceActions;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalance;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductOverlayAssetClassRebalanceRuleEvaluator</code> ...
 *
 * @author Mary Anderson
 */
public class ProductOverlayAssetClassRebalanceRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, ProductOverlayRuleEvaluatorContext> {

	private InvestmentAccountRebalanceService investmentAccountRebalanceService;


	private boolean miniRebalanceCheck;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, ProductOverlayRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			// Specific Asset Class Rebalance Warning
			if (!isMiniRebalanceCheck()) {
				List<ProductOverlayAssetClass> assetClassList = context.getRunSummaryList(run, false, null);
				for (ProductOverlayAssetClass oac : CollectionUtils.getIterable(assetClassList)) {
					if (oac.isRebalanceTriggersUsed()) {
						RebalanceActions action = oac.getAccountAssetClass().getRebalanceTriggerAction();
						// Only Applies if Action is Required
						if (action != null && action.isGenerateWarning()) {
							Map<String, Object> templateValues = new HashMap<>();
							if (RuleEvaluatorUtils.isEntityConfigRangeViolated(oac.getAmountOffTarget(), oac.getRebalanceTriggerMin(), oac.getRebalanceTriggerMax(), templateValues)) {
								ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), oac.getId(), null, templateValues));
							}
						}
					}
				}
			}
			else {
				InvestmentAccountRebalance accountRebal = getInvestmentAccountRebalanceService().getInvestmentAccountRebalance(context.getMainClientAccount(run).getId());
				if (accountRebal != null) {
					List<ProductOverlayAssetClass> assetClassList = context.getRunSummaryList(run, false, false);
					BigDecimal minAmount = accountRebal.getMinAllowedCash();
					BigDecimal maxAmount = accountRebal.getMaxAllowedCash();
					BigDecimal sumAmount = CoreMathUtils.sumProperty(assetClassList, ProductOverlayAssetClass::getRebalanceCashAdjusted);
					Map<String, Object> templateValues = new HashMap<>();
					if (RuleEvaluatorUtils.isEntityConfigRangeViolated(sumAmount, minAmount, maxAmount, templateValues)) {
						ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, run.getId(), templateValues));
					}
				}
			}
		}
		return ruleViolationList;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public boolean isMiniRebalanceCheck() {
		return this.miniRebalanceCheck;
	}


	public void setMiniRebalanceCheck(boolean miniRebalanceCheck) {
		this.miniRebalanceCheck = miniRebalanceCheck;
	}


	public InvestmentAccountRebalanceService getInvestmentAccountRebalanceService() {
		return this.investmentAccountRebalanceService;
	}


	public void setInvestmentAccountRebalanceService(InvestmentAccountRebalanceService investmentAccountRebalanceService) {
		this.investmentAccountRebalanceService = investmentAccountRebalanceService;
	}
}
