package com.clifton.product.overlay;


import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * {@link ProductOverlayAssetClass} class represents various overlay metrics
 * for a specified run (account + date) grouped by asset class.
 * <p>
 * It represents Phase IV of PIOS processing and facilitates in making trading decisions for proper overlay.
 *
 * @author vgomelsky
 */
public class ProductOverlayAssetClass extends BasePortfolioRunExposureSummary {

	private boolean rollupAssetClass;

	/**
	 * Copied from the InvestmentAccountAssetClass table - used to flag an asset class
	 * as hidden on reports
	 */
	private boolean privateAssetClass;

	/**
	 * Client Account that positions are held/traded in.  This is populated automatically by the system during processing
	 * and determined by any sub-account relationships to the main account the PIOS run is for.
	 * <p>
	 * If the positionInvestmentAccount = the account the PIOS run is for, this field is not set.
	 * <p>
	 * Calculated getter getTradingClientAccount returns the actual client account used to lookup positions and trade in
	 */
	private InvestmentAccount positionInvestmentAccount;

	private InvestmentReplication secondaryReplication;
	private BigDecimal secondaryReplicationAmount;

	/**
	 * Do not apply synthetic exposure or duration adjustments when calculating contract value
	 */
	private boolean doNotAdjustContractValue;

	// Used ONLY during Rebalancing Processing
	@NonPersistentField
	private BigDecimal newRebalanceCash;


	// Cash Adjustments for Attribution
	private List<ProductOverlayAssetClassCashAdjustment> cashAdjustmentList;

	private InvestmentAccountAssetClass accountAssetClass;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getSummaryLabel() {
		if (getAccountAssetClass() != null) {
			return getAccountAssetClass().getLabel();
		}
		return null;
	}


	public String getLabelExpanded() {
		List<ProductOverlayAssetClass> beanHierarchy = CollectionUtils.getChain(this, b -> (ProductOverlayAssetClass) b.getParent());
		CollectionUtils.reverse(beanHierarchy);
		return StringUtils.join(beanHierarchy, ProductOverlayAssetClass::getLabel, NamedHierarchicalEntity.HIERARCHY_DELIMITER);
	}


	/**
	 * Includes Client Directed Cash
	 */
	@Override
	public BigDecimal getCashTotal() {
		return MathUtils.add(getEffectiveCashTotal(), getClientDirectedCashWithAdjustments());
	}


	@Override
	public BigDecimal getTotalExposurePercent() {
		return MathUtils.round(MathUtils.add(getOverlayExposurePercent(), getActualAllocationAdjustedPercent()), 2);
	}


	public boolean isReplicationUsed() {
		return getPrimaryReplication() != null;
	}


	public boolean isRebalanceTriggerWarningsUsed() {
		if (getAccountAssetClass() != null && getAccountAssetClass().getRebalanceTriggerAction() != null) {
			return isRebalanceTriggersUsed() && getAccountAssetClass().getRebalanceTriggerAction().isGenerateWarning();
		}
		return false;
	}


	public boolean isTargetFollowsActual() {
		if (getAccountAssetClass() == null) {
			return false;
		}
		return getAccountAssetClass().isTargetExposureAdjustmentUsed();
	}

	//////////////////////////////////////////////////////
	// Calculated Cash Getters/Helpers
	//////////////////////////////////////////////////////


	/**
	 * Excludes Client Directed Cash
	 */
	public BigDecimal getEffectiveCashTotal() {
		BigDecimal total = getManagerCashWithAdjustments();
		total = MathUtils.add(total, getFundCashWithAdjustments());
		total = MathUtils.add(total, getTransitionCashWithAdjustments());
		total = MathUtils.add(total, getRebalanceCashWithAdjustments());
		return total;
	}


	public void addCashAdjustmentToList(ProductOverlayAssetClassCashAdjustment adj) {
		if (this.cashAdjustmentList == null) {
			this.cashAdjustmentList = new ArrayList<>();
		}
		this.cashAdjustmentList.add(adj);
	}


	public BigDecimal getFundCashWithAdjustments() {
		return MathUtils.add(getFundCash(), getFundCashAdjustments());
	}


	public BigDecimal getFundCashAdjustments() {
		return getCashBucketAdjustments(ManagerCashTypes.FUND);
	}


	public BigDecimal getManagerCashWithAdjustments() {
		return MathUtils.add(getManagerCash(), getManagerCashAdjustments());
	}


	public BigDecimal getManagerCashAdjustments() {
		return getCashBucketAdjustments(ManagerCashTypes.MANAGER);
	}


	public BigDecimal getTransitionCashWithAdjustments() {
		return MathUtils.add(getTransitionCash(), getTransitionCashAdjustments());
	}


	public BigDecimal getTransitionCashAdjustments() {
		return getCashBucketAdjustments(ManagerCashTypes.TRANSITION);
	}


	public BigDecimal getRebalanceCashWithAdjustments() {
		return MathUtils.add(getRebalanceCashAdjustedWithAttribution(), getRebalanceCashAdjustments());
	}


	public BigDecimal getRebalanceCashAdjustments() {
		return getCashBucketAdjustments(ManagerCashTypes.REBALANCE);
	}


	public BigDecimal getClientDirectedCashWithAdjustments() {
		return MathUtils.add(getClientDirectedCash(), getClientDirectedCashAdjustments());
	}


	public BigDecimal getClientDirectedCashAdjustments() {
		return getCashBucketAdjustments(ManagerCashTypes.CLIENT_DIRECTED);
	}


	private BigDecimal getCashBucketAdjustments(ManagerCashTypes cashType) {
		BigDecimal totalAdj = BigDecimal.ZERO;

		for (ProductOverlayAssetClassCashAdjustment adj : CollectionUtils.getIterable(getCashAdjustmentList())) {
			if (adj.getFromCashType() == cashType) {
				totalAdj = MathUtils.subtract(totalAdj, adj.getAmount());
			}
			if (adj.getToCashType() == cashType) {
				totalAdj = MathUtils.add(totalAdj, adj.getAmount());
			}
		}
		return totalAdj;
	}


	public boolean isRebalanceAdjusted() {
		return !MathUtils.isEqual(getRebalanceCash(), getRebalanceCashAdjusted());
	}


	public BigDecimal getRebalanceCashAdjustedAbs() {
		return MathUtils.abs(getRebalanceCashAdjusted());
	}


	public BigDecimal getRebalanceCashAdjustedWithAttribution() {
		return MathUtils.add(getRebalanceCashAdjusted(), getRebalanceAttributionCash());
	}


	public void addCashToBucket(ManagerCashTypes cashType, BigDecimal amount) {
		if (ManagerCashTypes.MANAGER == cashType) {
			setManagerCash(MathUtils.add(getManagerCash(), amount));
		}
		else if (ManagerCashTypes.FUND == cashType) {
			setFundCash(MathUtils.add(getFundCash(), amount));
		}
		else if (ManagerCashTypes.CLIENT_DIRECTED == cashType) {
			setClientDirectedCash(MathUtils.add(getClientDirectedCash(), amount));
		}
		else if (ManagerCashTypes.TRANSITION == cashType) {
			setTransitionCash(MathUtils.add(getTransitionCash(), amount));
		}
		else if (ManagerCashTypes.REBALANCE == cashType) {
			setRebalanceCash(MathUtils.add(getRebalanceCash(), amount));
		}
		else if (ManagerCashTypes.REBALANCE_ATTRIBUTION == cashType) {
			setRebalanceAttributionCash(MathUtils.add(getRebalanceAttributionCash(), amount));
		}
		else {
			throw new ValidationException("Unable to allocate cash - Unsupported Cash Type [" + cashType + "].");
		}
	}


	public BigDecimal getCashFromBucket(ManagerCashTypes cashType) {
		if (ManagerCashTypes.MANAGER == cashType) {
			return getManagerCash();
		}
		else if (ManagerCashTypes.FUND == cashType) {
			return getFundCash();
		}
		else if (ManagerCashTypes.CLIENT_DIRECTED == cashType) {
			return getClientDirectedCash();
		}
		else if (ManagerCashTypes.TRANSITION == cashType) {
			return getTransitionCash();
		}
		else if (ManagerCashTypes.REBALANCE == cashType) {
			return getRebalanceCash();
		}
		else if (ManagerCashTypes.REBALANCE_ATTRIBUTION == cashType) {
			return getRebalanceAttributionCash();
		}
		else {
			throw new ValidationException("No getter defined for Cash Type [" + cashType + "].");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns "positive" value for amount off target so we can sort by which is
	 * MOST off target and recommend that as the anchor.
	 */
	public BigDecimal getAmountOffTargetAbs() {
		BigDecimal amount = getAmountOffTarget();
		if (amount != null && MathUtils.isLessThan(amount, 0)) {
			return MathUtils.negate(amount);
		}
		return amount;
	}


	public InvestmentSecurity getCoalesceRebalanceBenchmarkSecurity() {
		if (getRebalanceBenchmarkSecurity() != null) {
			return getRebalanceBenchmarkSecurity();
		}
		return getBenchmarkSecurity();
	}


	public boolean isDurationsUsed() {
		return !MathUtils.isNullOrZero(getBenchmarkDuration());
	}


	public BigDecimal getEffectiveVsBenchmarkDuration() {
		if (!MathUtils.isNullOrZero(getBenchmarkDuration())) {
			return MathUtils.divide(getEffectiveDuration(), getBenchmarkDuration());
		}
		return null;
	}


	public InvestmentAccount getTradingClientAccount() {
		if (getPositionInvestmentAccount() != null) {
			return getPositionInvestmentAccount();
		}
		return getOverlayRun().getClientInvestmentAccount();
	}


	public boolean isRebalanceExposureTargetUsed() {
		return getAccountAssetClass() != null && getAccountAssetClass().getRebalanceExposureTarget() != null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	// Converted to Percentages Getters


	public BigDecimal getOverlayExposurePercent() {
		return MathUtils.round(CoreMathUtils.getPercentValue(getOverlayExposure(), getPortfolioTotalValue(), true), 2);
	}


	public BigDecimal getTotalExposureDeviationFromRebalanceExposureTargetPercent() {
		return MathUtils.round(MathUtils.subtract(getTotalExposurePercent(), getRebalanceExposureTargetPercent()), 2);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isOutsideOfRebalanceTrigger() {
		return !MathUtils.isInRange(getAmountOffTarget(), getRebalanceTriggerMin(), getRebalanceTriggerMax());
	}


	public boolean isOutsideOfAbsoluteRebalanceTrigger() {
		return !MathUtils.isInRange(getAmountOffTarget(), getRebalanceTriggerAbsoluteMin(), getRebalanceTriggerAbsoluteMax());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getNewRebalanceCash() {
		return this.newRebalanceCash;
	}


	public void setNewRebalanceCash(BigDecimal newRebalanceCash) {
		this.newRebalanceCash = newRebalanceCash;
	}


	public InvestmentReplication getSecondaryReplication() {
		return this.secondaryReplication;
	}


	public void setSecondaryReplication(InvestmentReplication secondaryReplication) {
		this.secondaryReplication = secondaryReplication;
	}


	public BigDecimal getSecondaryReplicationAmount() {
		return this.secondaryReplicationAmount;
	}


	public void setSecondaryReplicationAmount(BigDecimal secondaryReplicationAmount) {
		this.secondaryReplicationAmount = secondaryReplicationAmount;
	}


	public boolean isRollupAssetClass() {
		return this.rollupAssetClass;
	}


	public void setRollupAssetClass(boolean rollupAssetClass) {
		this.rollupAssetClass = rollupAssetClass;
	}


	public InvestmentAccount getPositionInvestmentAccount() {
		return this.positionInvestmentAccount;
	}


	public void setPositionInvestmentAccount(InvestmentAccount positionInvestmentAccount) {
		this.positionInvestmentAccount = positionInvestmentAccount;
	}


	public boolean isDoNotAdjustContractValue() {
		return this.doNotAdjustContractValue;
	}


	public void setDoNotAdjustContractValue(boolean doNotAdjustContractValue) {
		this.doNotAdjustContractValue = doNotAdjustContractValue;
	}


	public boolean isPrivateAssetClass() {
		return this.privateAssetClass;
	}


	public void setPrivateAssetClass(boolean privateAssetClass) {
		this.privateAssetClass = privateAssetClass;
	}


	public List<ProductOverlayAssetClassCashAdjustment> getCashAdjustmentList() {
		return this.cashAdjustmentList;
	}


	public void setCashAdjustmentList(List<ProductOverlayAssetClassCashAdjustment> cashAdjustmentList) {
		this.cashAdjustmentList = cashAdjustmentList;
	}


	public InvestmentAccountAssetClass getAccountAssetClass() {
		return this.accountAssetClass;
	}


	public void setAccountAssetClass(InvestmentAccountAssetClass accountAssetClass) {
		this.accountAssetClass = accountAssetClass;
	}
}
