package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.rule.PortfolioRunRuleHandler;
import com.clifton.portfolio.run.rule.PortfolioRunRuleHandlerImpl;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import com.clifton.product.overlay.rebalance.calculators.ProductOverlayRebalanceFullCalculator;
import com.clifton.product.util.ProductUtilService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BaseProductOverlayMinimizeImbalancesCalculatorImpl</code> ...
 *
 * @author manderson
 */
public abstract class BaseProductOverlayMinimizeImbalancesCalculatorImpl extends ProductOverlayRebalanceFullCalculator implements ProductOverlayMinimizeImbalancesCalculator {

	/////////////////////////////////////////////////////////////

	public static final String DEVIATION_PERCENTAGE_DENOMINATOR_TARGET = "Target";
	public static final String DEVIATION_PERCENTAGE_DENOMINATOR_REBAL_TRIGGER = "Rebalance Trigger";

	/////////////////////////////////////////////////////////////

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;

	private PortfolioRunRuleHandler portfolioRunRuleHandler;

	private ProductUtilService productUtilService;

	/////////////////////////////////////////////////////////////

	private boolean attributionRebalancing;

	private boolean enforceSyntheticExposureLimit;

	private boolean doNotAdjustForNegativeCashTarget;

	/**
	 * Default is to calculate deviation as a percentage of the adjusted target.
	 * New Feature: PRODUCT-305 will support calculating deviation as a percentage of the rebalance trigger
	 */
	private String deviationPercentageDenominator = DEVIATION_PERCENTAGE_DENOMINATOR_TARGET;


	/////////////////////////////////////////////////////////////


	@Override
	public final boolean calculate(ProductOverlayRunConfig runConfig) {
		Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap = new HashMap<>();
		// Within bands, populate cash to fulfill target = overlay exposure
		boolean result;
		boolean noTradingBands = runConfig.isEffectiveCashLessExposureInsideNoTradingBands(assetClassCashMap);
		if (noTradingBands) {
			result = applyCashToAssetClasses(runConfig, assetClassCashMap, false);
		}
		else {
			// Otherwise, we are trading, return false to continue with Minimize Imbalances processing
			result = calculateMinimizeImbalances(runConfig);
		}
		boolean attributionResult = false;
		if (isAttributionRebalancing()) {
			attributionResult = calculateAttributionRebalancing(runConfig, noTradingBands);
		}

		return result || attributionResult;
	}


	protected abstract boolean calculateMinimizeImbalances(ProductOverlayRunConfig runConfig);


	//////////////////////////////////////////////////////////////////
	///////////               Helper Methods              ////////////
	//////////////////////////////////////////////////////////////////


	/**
	 * Applies adjustment if necessary and returns total cash available for minimize imbalances
	 */
	protected BigDecimal getTotalMinimizeImbalanceCash(ProductOverlayRunConfig config) {
		BigDecimal totalAdjustment = config.getAccountRebalanceConfig().getMinimizeImbalancesAdjustmentAmount();

		// If minimize imbalance cash total should be adjusted by Cash asset class Adjusted Target
		if (config.getAccountRebalanceConfig().isMinimizeImbalancesAdjustForCashTarget()) {
			List<ProductOverlayAssetClass> cashAC = BeanUtils.filter(config.getOverlayAssetClassListExcludeRollups(), overlayAssetClass -> overlayAssetClass.getAccountAssetClass().getAssetClass().isMainCash());
			BigDecimal cashTargetAdjustment = CoreMathUtils.sumProperty(cashAC, ProductOverlayAssetClass::getTargetAllocationAdjusted);
			if (MathUtils.isLessThan(cashTargetAdjustment, BigDecimal.ZERO)) {
				if (isDoNotAdjustForNegativeCashTarget()) {
					cashTargetAdjustment = BigDecimal.ZERO;
				}
			}
			// Reduce target by cash target adjustment
			totalAdjustment = MathUtils.subtract(totalAdjustment, cashTargetAdjustment);
		}

		// If minimize imbalance cash total should be adjusted by specified static amount
		config.getMinimizeImbalancesCashConfig().applyAdjustment(totalAdjustment);

		if (isEnforceSyntheticExposureLimit()) {
			BigDecimal synLimit = getPortfolioRunRuleHandler().getPortfolioRunRuleMaxAmount(PortfolioRunRuleHandlerImpl.RULE_DEFINITION_SYNTHETIC_EXPOSURE_RANGE, config.getRun().getClientInvestmentAccount());
			if (synLimit != null) {
				BigDecimal cashAvailable = config.getMinimizeImbalancesCashConfig().getCashTotal();
				BigDecimal maxCash = MathUtils.getPercentageOf(synLimit, config.getPortfolioTotalCalculatedValue(), true);
				if (MathUtils.isGreaterThan(cashAvailable, maxCash)) {
					config.getMinimizeImbalancesCashConfig().applyAdjustment(MathUtils.subtract(maxCash, cashAvailable));
				}
			}
		}

		// Prior to Calculating - if any cash bucket total is negative apply as an adjustment to clear and move cash through each bucket totals
		// After calculating - any negative cash left will just be moved to rebalance cash
		config.getMinimizeImbalancesCashConfig().applyAttributionAdjustment();

		return config.getMinimizeImbalancesCashConfig().getCashTotal();
	}


	/**
	 * Do we include this asset class in our minimizing imbalances processing?
	 * By default, main cash asset class and any asset class without replications are skipped
	 * Separate method to allow overrides by subclasses, i.e. rollup support allows asset classes without replications if it's a rollup asset class
	 */
	protected boolean isProcessProductOverlayAssetClass(ProductOverlayRunConfig config, ProductOverlayAssetClass oac) {
		if (oac.getAccountAssetClass().getAssetClass().isMainCash()) {
			return false;
		}
		// Replications Used Only - Since they are the only ones with an Overlay Target
		return oac.isReplicationUsed();
	}


	/**
	 * By default, we'll allow this, but some calculators have properties that allow overriding this option
	 */
	protected boolean isShortTargetsAllowed() {
		return true;
	}


	protected BigDecimal applyMinimizeImbalancesCashToFillOverlayExposure(ProductOverlayRunConfig config, List<ProductOverlayAssetClass> list,
	                                                                      Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap, BigDecimal totalMinimizeImbalancesCash, Map<InvestmentAccountAssetClass, BigDecimal> rebalanceCashOverrideMap,
	                                                                      StringBuilder debugOutput) {
		// First - Overlay Manager Minimize Imbalances to fill Overlay Exposure
		debugOutput.append(StringUtils.NEW_LINE + "Total Minimize Imbalances Cash: ").append(CoreMathUtils.formatNumberMoney(totalMinimizeImbalancesCash)).append(StringUtils.NEW_LINE);

		list = BeanUtils.sortWithFunction(list, ProductOverlayAssetClass::getOverlayExposure, true);

		for (ProductOverlayAssetClass oac : CollectionUtils.getIterable(list)) {
			if (!isProcessProductOverlayAssetClass(config, oac)) {
				continue;
			}
			BigDecimal cashAllocated = oac.getCashTotal();
			debugOutput.append(StringUtils.NEW_LINE).append(oac.getLabel());
			debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "Current Cash Allocated: ").append(CoreMathUtils.formatNumberMoney(oac.getCashTotal()));
			// If the Rebalance Cash Override Map is Not Empty - Back out current rebalance cash and add in previous from the map
			// We do this from a map because we don't record the history on the rollup, but we may need it for the calculation so we build a map
			if (!CollectionUtils.isEmpty(rebalanceCashOverrideMap)) {
				debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "Backing Out Current Rebalance Cash: ").append(CoreMathUtils.formatNumberMoney(oac.getRebalanceCashAdjusted()));
				cashAllocated = MathUtils.subtract(cashAllocated, oac.getRebalanceCashAdjusted());
				// Add in what is was previously there
				BigDecimal previousRebal = rebalanceCashOverrideMap.get(oac.getAccountAssetClass());
				cashAllocated = MathUtils.add(cashAllocated, previousRebal);
				debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "Including Previous Rebalance Cash: ").append(CoreMathUtils.formatNumberMoney(previousRebal));
			}
			debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "Overlay Exposure: ").append(CoreMathUtils.formatNumberMoney(oac.getOverlayExposure()));

			BigDecimal cashNeeded = MathUtils.round(MathUtils.subtract(oac.getOverlayExposure(), cashAllocated), 2);
			debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "Apply From Minimize Imbalance Cash: ").append(CoreMathUtils.formatNumberMoney(cashNeeded));
			if (!isShortTargetsAllowed() && MathUtils.isLessThan(oac.getOverlayExposure(), BigDecimal.ZERO)) {
				// Need to bring target back to zero
				cashNeeded = MathUtils.abs(cashNeeded);
				debugOutput.append(StringUtils.NEW_LINE + StringUtils.TAB + "No Short Targets allowed - reverse amount to apply From Minimize Imbalance Cash: ").append(CoreMathUtils.formatNumberMoney(cashNeeded));
			}
			assetClassCashMap.put(oac.getAccountAssetClass(), cashNeeded);
			totalMinimizeImbalancesCash = MathUtils.subtract(totalMinimizeImbalancesCash, cashNeeded);
		}

		debugOutput.append(StringUtils.NEW_LINE + "After applying Cash to fill Overlay Exposure - Cash To Allocate: ").append(CoreMathUtils.formatNumberMoney(totalMinimizeImbalancesCash)).append(StringUtils.NEW_LINE);
		return totalMinimizeImbalancesCash;
	}


	protected boolean applyCashToAssetClasses(ProductOverlayRunConfig runConfig, Map<InvestmentAccountAssetClass, BigDecimal> assetClassCashMap, boolean populateResult) {
		boolean appliedCash = false;

		MinimizeImbalancesCashConfig cashConfig = runConfig.getMinimizeImbalancesCashConfig();

		boolean useCashPercentage = true;
		BigDecimal totalCashPercentage = CoreMathUtils.sumProperty(assetClassCashMap.keySet(), InvestmentAccountAssetClass::getCashPercentage);
		if (MathUtils.isNullOrZero(totalCashPercentage)) {
			useCashPercentage = false;
			totalCashPercentage = CoreMathUtils.sumProperty(assetClassCashMap.keySet(), InvestmentAccountAssetClass::getAssetClassPercentage);
		}
		BigDecimal totalUnallocatedManagerCash = BigDecimal.ZERO;
		BigDecimal totalUnallocatedTransitionCash = BigDecimal.ZERO;

		for (Map.Entry<InvestmentAccountAssetClass, BigDecimal> accountAssetClassEntry : cashConfig.getManagerCashMap().entrySet()) {
			if (!assetClassCashMap.containsKey(accountAssetClassEntry.getKey())) {
				totalUnallocatedManagerCash = MathUtils.add(totalUnallocatedManagerCash, accountAssetClassEntry.getValue());
			}
		}
		for (Map.Entry<InvestmentAccountAssetClass, BigDecimal> accountAssetClassEntry : cashConfig.getTransitionCashMap().entrySet()) {
			if (!assetClassCashMap.containsKey(accountAssetClassEntry.getKey())) {
				totalUnallocatedTransitionCash = MathUtils.add(totalUnallocatedTransitionCash, accountAssetClassEntry.getValue());
			}
		}

		for (Map.Entry<InvestmentAccountAssetClass, BigDecimal> investmentAccountAssetClassBigDecimalEntry : assetClassCashMap.entrySet()) {
			// Skip Main Cash
			if ((investmentAccountAssetClassBigDecimalEntry.getKey()).getAssetClass().isMainCash()) {
				continue;
			}
			ProductOverlayAssetClass oac = ((investmentAccountAssetClassBigDecimalEntry.getKey()).isRollupAssetClass() ? runConfig.getAssetClassRollupMap().get(investmentAccountAssetClassBigDecimalEntry.getKey()) : runConfig.getAssetClassMap().get(investmentAccountAssetClassBigDecimalEntry.getKey()));

			BigDecimal cashToApply = investmentAccountAssetClassBigDecimalEntry.getValue();
			if (populateResult) {
				runConfig.getMinimizeImbalancesCashConfig().appendLineToResultMessage(oac.getLabel() + ": " + CoreMathUtils.formatNumberMoney(cashToApply));
			}
			BigDecimal cashApplied = BigDecimal.ZERO;
			BigDecimal cashPercentage = CoreMathUtils.getPercentValue((useCashPercentage ? (investmentAccountAssetClassBigDecimalEntry.getKey()).getCashPercentage() : (investmentAccountAssetClassBigDecimalEntry.getKey()).getAssetClassPercentage()), totalCashPercentage, true);

			BigDecimal fundCash = MathUtils.round(MathUtils.getPercentageOf(cashPercentage, cashConfig.getFundCash(), true), 2);
			cashApplied = MathUtils.add(cashApplied, fundCash);

			BigDecimal managerCash = MathUtils.add(cashConfig.getManagerCashMap().get(investmentAccountAssetClassBigDecimalEntry.getKey()), MathUtils.round(MathUtils.getPercentageOf(cashPercentage, totalUnallocatedManagerCash, true), 2));
			cashApplied = MathUtils.add(cashApplied, managerCash);

			BigDecimal transitionCash = MathUtils.add(cashConfig.getTransitionCashMap().get(investmentAccountAssetClassBigDecimalEntry.getKey()), MathUtils.round(MathUtils.getPercentageOf(cashPercentage, totalUnallocatedTransitionCash, true), 2));
			cashApplied = MathUtils.add(cashApplied, transitionCash);

			BigDecimal difference = MathUtils.subtract(cashToApply, cashApplied);
			oac.addCashToBucket(ManagerCashTypes.FUND, fundCash);
			oac.addCashToBucket(ManagerCashTypes.MANAGER, managerCash);
			oac.addCashToBucket(ManagerCashTypes.TRANSITION, transitionCash);
			oac.addCashToBucket(ManagerCashTypes.REBALANCE_ATTRIBUTION, difference);
		}
		return appliedCash;
	}


	///////////////////////////////////////////////////////////
	/////////       Attribution Rebalancing          //////////
	///////////////////////////////////////////////////////////


	private boolean calculateAttributionRebalancing(ProductOverlayRunConfig runConfig, boolean noTradingBands) {
		// If inside No Trading Bands we don't want to force cash total - should generate a dynamic cash target
		if (!noTradingBands) {
			// Usually Rebalance Attribution cash will be forced to sum to 0, however, in cases where we need to move everything (i.e. total Fund Cash is negative), the total will be adjusted to be that be sum
			runConfig.getCashBucketExpectedTotalMap().put(ManagerCashTypes.REBALANCE_ATTRIBUTION, BigDecimal.ZERO);
		}
		boolean mgr = calculateAttributionRebalancing(runConfig, ManagerCashTypes.MANAGER);
		boolean trans = calculateAttributionRebalancing(runConfig, ManagerCashTypes.TRANSITION);
		boolean fund = calculateAttributionRebalancing(runConfig, ManagerCashTypes.FUND);
		return mgr || trans || fund;
	}


	private boolean calculateAttributionRebalancing(ProductOverlayRunConfig runConfig, ManagerCashTypes cashType) {
		BigDecimal totalMoved = BigDecimal.ZERO;
		BigDecimal totalLeftInBucket = BigDecimal.ZERO;
		// Get Cash to Move and Totals so can disperse later to offset cash move
		for (ProductOverlayAssetClass oac : runConfig.getAssetClassMap().values()) {
			BigDecimal cashAmount = oac.getCashFromBucket(cashType);
			if (MathUtils.isLessThan(cashAmount, BigDecimal.ZERO)) {
				// Set cash bucket to zero
				oac.addCashToBucket(cashType, MathUtils.negate(cashAmount));
				// Put In Rebal Attribution
				oac.addCashToBucket(ManagerCashTypes.REBALANCE_ATTRIBUTION, cashAmount);
				// Track total
				totalMoved = MathUtils.add(totalMoved, cashAmount);
			}
			else {
				totalLeftInBucket = MathUtils.add(totalLeftInBucket, cashAmount);
			}
		}

		if (!MathUtils.isNullOrZero(totalMoved)) {
			// If abs of what was moved over is greater than what is left, then need to move everything, or else we will end up with negative amounts again
			boolean moveEverything = MathUtils.isGreaterThan(MathUtils.abs(totalMoved), totalLeftInBucket);
			runConfig.getCashBucketExpectedTotalMap().put(cashType, (moveEverything ? BigDecimal.ZERO : MathUtils.add(totalLeftInBucket, totalMoved)));
			if (moveEverything) {
				// Usually Rebalance Attribution cash will be forced to sum to 0, however, in cases where we need to move everything (i.e. total Fund Cash is negative), the total will now that be sum
				runConfig.getCashBucketExpectedTotalMap().put(ManagerCashTypes.REBALANCE_ATTRIBUTION,
						MathUtils.add(MathUtils.add(totalLeftInBucket, totalMoved), runConfig.getCashBucketExpectedTotalMap().get(ManagerCashTypes.REBALANCE_ATTRIBUTION)));
			}
			for (ProductOverlayAssetClass oac : runConfig.getAssetClassMap().values()) {
				BigDecimal allocateAmount = (moveEverything ? MathUtils.negate(oac.getCashFromBucket(cashType)) : MathUtils.getPercentageOf(
						CoreMathUtils.getPercentValue(oac.getCashFromBucket(cashType), totalLeftInBucket, true), totalMoved, true));
				if (!MathUtils.isNullOrZero(allocateAmount)) {
					oac.addCashToBucket(cashType, allocateAmount);
					oac.addCashToBucket(ManagerCashTypes.REBALANCE_ATTRIBUTION, MathUtils.negate(allocateAmount));
				}
			}
			return true;
		}
		return false;
	}


	////////////////////////////////////////////////////////
	/////  Helper Methods
	////////////////////////////////////////////////////////


	/**
	 * Keeps track of custom field look ups in a map on the run config object to reduce look ups
	 * If config is null (happens rarely when full rebalance is called for a calculator that has specific logic that needs custom field values - i.e. apply to children proportionally)
	 * then will just perform the lookup
	 */
	protected Object getInvestmentAccountAssetClassCustomFieldValue(ProductOverlayRunConfig config, InvestmentAccountAssetClass iac, String columnName, Object nullValue) {
		Object value = config == null ? null : config.getAssetClassCustomFieldValue(iac, columnName);
		if (value == null) {
			value = getProductUtilService().getInvestmentAccountAssetClassCustomFieldValue(iac, getCustomColumnGroupName(), columnName);
			if (value == null) {
				value = nullValue;
			}
			if (config != null) {
				config.setAssetClassCustomFieldValue(iac, columnName, value);
			}
		}
		return value;
	}


	protected BigDecimal getDeviationTotal(List<ProductOverlayAssetClass> list) {
		BigDecimal total = BigDecimal.ZERO;
		for (ProductOverlayAssetClass ac : CollectionUtils.getIterable(list)) {
			total = MathUtils.add(total, getDeviationPercentageDenominatorValue(ac));
		}
		return total;
	}


	protected List<ProductOverlayAssetClass> getProductOverlayAssetClassListSortedByDeviationPercentage(List<ProductOverlayAssetClass> list, final boolean sortAscending) {
		Collections.sort(list, (o1, o2) -> {
			BigDecimal v1 = getDeviationPercentage(o1.getTotalExposureDeviationFromAdjustedTarget(), o1);
			BigDecimal v2 = getDeviationPercentage(o2.getTotalExposureDeviationFromAdjustedTarget(), o2);
			int result = MathUtils.compare(v1, v2);
			if (!sortAscending) {
				result = result * -1;
			}
			return result;
		});
		return list;
	}


	protected BigDecimal getDeviationPercentage(BigDecimal deviationAmount, ProductOverlayAssetClass overlayAssetClass) {
		if (MathUtils.isNullOrZero(deviationAmount)) {
			return BigDecimal.ZERO;
		}
		BigDecimal denominator = getDeviationPercentageDenominatorValue(overlayAssetClass);

		if (!MathUtils.isNullOrZero(denominator)) {
			return CoreMathUtils.getPercentValue(deviationAmount, denominator, true);
		}
		else if (MathUtils.isLessThan(deviationAmount, BigDecimal.ZERO)) {
			return MathUtils.negate(MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		}
		return MathUtils.BIG_DECIMAL_ONE_HUNDRED;
	}


	protected BigDecimal getDeviationPercentageDenominatorValue(ProductOverlayAssetClass overlayAssetClass) {
		BigDecimal denominator;
		if (DEVIATION_PERCENTAGE_DENOMINATOR_REBAL_TRIGGER.equals(getDeviationPercentageDenominator())) {
			if (MathUtils.isLessThan(overlayAssetClass.getTotalExposureDeviationFromAdjustedTarget(), BigDecimal.ZERO)) {
				denominator = MathUtils.abs(overlayAssetClass.getRebalanceTriggerMin());
			}
			else {
				denominator = MathUtils.abs(overlayAssetClass.getRebalanceTriggerMax());
			}
		}
		else if (DEVIATION_PERCENTAGE_DENOMINATOR_TARGET.equals(getDeviationPercentageDenominator())) {
			denominator = MathUtils.abs(overlayAssetClass.getTargetAllocationAdjusted());
		}
		else {
			throw new RuntimeException("Selected Minimize Imbalances Deviation Percentage Denominator [" + getDeviationPercentageDenominator() + "] is not supported.");
		}
		return denominator;
	}


	/////////////////////////////////////////////////////////////
	//////////         Getter & Setter Methods         //////////
	/////////////////////////////////////////////////////////////


	public boolean isAttributionRebalancing() {
		return this.attributionRebalancing;
	}


	public void setAttributionRebalancing(boolean attributionRebalancing) {
		this.attributionRebalancing = attributionRebalancing;
	}


	public ProductUtilService getProductUtilService() {
		return this.productUtilService;
	}


	public void setProductUtilService(ProductUtilService productUtilService) {
		this.productUtilService = productUtilService;
	}


	public boolean isEnforceSyntheticExposureLimit() {
		return this.enforceSyntheticExposureLimit;
	}


	public void setEnforceSyntheticExposureLimit(boolean enforceSyntheticExposureLimit) {
		this.enforceSyntheticExposureLimit = enforceSyntheticExposureLimit;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public PortfolioRunRuleHandler getPortfolioRunRuleHandler() {
		return this.portfolioRunRuleHandler;
	}


	public void setPortfolioRunRuleHandler(PortfolioRunRuleHandler portfolioRunRuleHandler) {
		this.portfolioRunRuleHandler = portfolioRunRuleHandler;
	}


	public boolean isDoNotAdjustForNegativeCashTarget() {
		return this.doNotAdjustForNegativeCashTarget;
	}


	public void setDoNotAdjustForNegativeCashTarget(boolean doNotAdjustForNegativeCashTarget) {
		this.doNotAdjustForNegativeCashTarget = doNotAdjustForNegativeCashTarget;
	}


	public String getDeviationPercentageDenominator() {
		return this.deviationPercentageDenominator;
	}


	public void setDeviationPercentageDenominator(String deviationPercentageDenominator) {
		this.deviationPercentageDenominator = deviationPercentageDenominator;
	}
}
