package com.clifton.product.overlay.process.calculators;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.account.PortfolioAccountContractStoreTypes;
import com.clifton.portfolio.replication.PortfolioReplicationAllocationSecurityConfig;
import com.clifton.portfolio.replication.PortfolioReplicationHandler;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioReplicationCurrencyOtherConfig;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleHandlerImpl;
import com.clifton.portfolio.run.util.PortfolioUtils;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.product.overlay.manager.replication.calculators.ProductOverlayManagerAccountSecuritiesReplicationByInvestmentSecurityAllocator;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import com.clifton.product.replication.ProductReplicationService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.bean.SystemBeanService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>ProductOverlayRunAssetClassReplicationProcessor</code> ...
 * <p>
 * STEP 3
 *
 * @author Mary Anderson
 */
public class ProductOverlayAssetClassReplicationCalculator extends ProductOverlayAssetClassCalculator {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentReplicationService investmentReplicationService;

	private PortfolioReplicationHandler portfolioReplicationHandler;

	private ProductReplicationService productReplicationService;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void processStep(ProductOverlayRunConfig runConfig) {
		// Asset Class Processing
		// 1.  Setup
		setupProductOverlayAssetClasses(runConfig);

		// 2.  Process OverlayManager/InvestmentManagerAccountAssignments For Overlay Asset Classes
		processProductOverlayAssetClassForInvestmentManagerAccountAssignments(runConfig);

		// 3.  Process InvestmentAccountAssetClasses for Overlay Asset Classes
		processProductOverlayAssetClassForInvestmentAccountAssetClasses(runConfig);

		// 4.  Set Rebalance Cash/Rebalance Cash Adjusted and perform mini rebalance (if enabled and applies)
		processProductOverlayAssetClassRebalancing(runConfig);

		// 5. Process/Setup Replications
		// Pass true to estimate client directed targets in case of contract splitting
		generateProductOverlayAssetClassReplications(runConfig, true);

		// 6. Client Directed Cash
		setClientDirectedCashFromOverlayExposure(runConfig, false);

		// 9. Evaluate Trading Bands (if applies) and Minimize Imbalances
		processOverlayExposureForTradingOrMinimizeImbalances(runConfig);

		// 10. Reprocess Reps if changes to cash buckets, i.e. Overlay Targets
		// Pass false to estimate targets since we now have the value
		// Need to Reprocess Replications based on new Cash Buckets
		generateProductOverlayAssetClassReplications(runConfig, false);

		// 11. Set Replication Targets/Adjustments based on Old-New Securities/Secondary to Primary, Matching, etc.
		calculateProductOverlayAssetClassReplicationTargets(runConfig);

		// 12. Apply Additional Exposure Amount (Process Manager Securities Balance Replication Allocations)
		// This is done last so that all adjusted targets are updated appropriately for rolling contracts and additional exposure is split using the adjusted targets
		allocateAdditionalExposureToReplications(runConfig);
	}


	@Override
	protected void saveStep(ProductOverlayRunConfig runConfig) {
		// Trading or Minimize Imbalance Warnings
		RuleViolation tradingBandsViolation = null;
		if (!StringUtils.isEmpty(runConfig.getTradingBandsMessage())) {
			tradingBandsViolation = new RuleViolation();
			tradingBandsViolation.setViolationNote(runConfig.getTradingBandsMessage());
		}
		getPortfolioRunRuleHandler().savePortfolioRunRuleViolationSystemDefined(PortfolioRunRuleHandlerImpl.RULE_DEFINITION_TRADING_BANDS, runConfig.getRun().getId(), tradingBandsViolation);

		RuleViolation minimizeViolation = null;
		if (runConfig.getMinimizeImbalancesCashConfig() != null && !StringUtils.isEmpty(runConfig.getMinimizeImbalancesCashConfig().getResultMessage())) {
			minimizeViolation = new RuleViolation();
			minimizeViolation.setViolationNote(runConfig.getMinimizeImbalancesCashConfig().getResultMessage());
		}
		getPortfolioRunRuleHandler().savePortfolioRunRuleViolationSystemDefined(PortfolioRunRuleHandlerImpl.RULE_DEFINITION_MINIMIZE_IMBALANCES, runConfig.getRun().getId(), minimizeViolation);

		// Asset Classes - Clean Up and Save the Data
		cleanUpAndSaveAssetClasses(runConfig);

		// Replications/CurrencyOther - Clean Up and Save the Data
		// Also populated ReplicationDetail objects where applicable
		cleanUpAndSaveReplications(runConfig);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates ProductOverlayAssetClassReplications
	 * Applies Security data to them
	 * Processes Contract Splitting
	 * Processes Currency and generates CurrencyOther
	 * <p>
	 * Returns true if used any estimated targets (meaning it will need to be recalculated)
	 */
	private boolean generateProductOverlayAssetClassReplications(ProductOverlayRunConfig runConfig, boolean estimateTargets) {
		// 1. Setup
		setupReplications(runConfig);

		// 2. Apply Securities to the Replications - will create additional rows for special cases, i.e. instrument assignment where we currently hold 2 contracts for that instrument, etc.
		// First time through, use estimates for Client Directed Cash Targets and Minimize Imbalances in case of contract splitting
		estimateTargets = applySecuritiesToReplications(runConfig, estimateTargets);

		// 3. Process Contract Splitting & Generate Virtual Contracts
		getProductReplicationService().processContractSplitting(runConfig.getContractStore(), runConfig.getOverlayAssetClassReplicationList());

		// 5. Processes Currency Contracts and Creates/Sets List<PortfolioCurrencyOther> list on the runConfig if there are any
		processCurrencyContracts(runConfig);

		// 6. Apply Overlay Exposure Values for Cash Exposure Replication Allocations
		applyCashExposureAmounts(runConfig);

		// 7. Rolls up Replications to Asset Class Overlay Exposure, Sets Effective Duration on the Asset Class
		// Process Client Directed Cash For Overlay Target = Overlay Exposure
		applyReplicationsToAssetClasses(runConfig);

		return estimateTargets;
	}


	private void calculateProductOverlayAssetClassReplicationTargets(ProductOverlayRunConfig runConfig) {
		List<ProductOverlayAssetClassReplication> repList = runConfig.getOverlayAssetClassReplicationList();

		// Fix Targets based on Secondary -> Primary Replication Map AND Secondary --> Matching Map
		// Original secondary was set with 0 targets for these.  Whatever we add to secondary target, need to subtract from primary target
		// List of Security Ids that should be added to currency unrealized
		for (Map.Entry<Integer, Integer> secondaryRepIdEntry : runConfig.getSecondaryToPrimaryReplicationMap().entrySet()) {
			List<ProductOverlayAssetClassReplication> secondaryRepList = BeanUtils.filter(repList, overlayReplication -> overlayReplication.getReplication().getId(), secondaryRepIdEntry.getKey());
			List<ProductOverlayAssetClassReplication> primaryRepList = BeanUtils.filter(repList, overlayReplication -> overlayReplication.getReplication().getId(), secondaryRepIdEntry.getValue());

			// Go through secondary and get the amount actually allocated.
			BigDecimal secondaryAllocated = CoreMathUtils.sumProperty(secondaryRepList, ProductOverlayAssetClassReplication::getActualExposureAdjusted);
			for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(secondaryRepList)) {
				BigDecimal amount = MathUtils.getPercentageOf(rep.getAllocationWeight(), secondaryAllocated, true);
				rep.setTargetExposureAdjusted(amount);

				BigDecimal percent = getPercentValueForProductOverlayAssetClassReplication(rep, "Adjusted Target Allocation %", rep.getTargetExposureAdjusted(), runConfig.getAssetClassReplicationTotalTarget(rep.getOverlayAssetClass()));
				rep.setAllocationWeightAdjusted(percent);
			}

			// Go through primary and subtract secondary allocation from each (based on original target %)
			for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(primaryRepList)) {
				BigDecimal subtractFromTarget = MathUtils.getPercentageOf(rep.getAllocationWeight(), secondaryAllocated, true);
				rep.setTargetExposureAdjusted(MathUtils.subtract(rep.getTargetExposureAdjusted(), subtractFromTarget));

				BigDecimal percent = getPercentValueForProductOverlayAssetClassReplication(rep, "Adjusted Target Allocation %", rep.getTargetExposureAdjusted(), runConfig.getAssetClassReplicationTotalTarget(rep.getOverlayAssetClass()));
				rep.setAllocationWeightAdjusted(percent);
			}
		}

		// Set Matching Targets based on Actuals where applies
		for (ProductOverlayAssetClassReplication overlayRep : runConfig.getOverlayAssetClassReplicationList()) {
			boolean useActualForMatching = overlayRep.getOverlayAssetClass().getAccountAssetClass().isUseActualForMatchingReplication();
			// Don't want to double count for contract splitting where targets are changed later.
			// Skip "Old" securities, values we need are in the "current" security.
			// ONLY SKIP IF USING TARGETS - IF USING ACTUALS, NEED TO INCLUDE ALL
			if (!useActualForMatching) {
				InvestmentSecurity current = overlayRep.getAllocationConfig().getCurrentSecurity();
				if (current != null && !MathUtils.isEqual(current.getId(), overlayRep.getSecurity().getId())) {
					continue;
				}
			}
			cleanUpMatchingReplicationTargets(runConfig, overlayRep, useActualForMatching);
		}

		// Fix For Allocations with more than one security mapped to it
		// Note: applies TFA logic for primary to New Security when a primary instrument is selected
		// on the replication allocation and we hold positions for the primary (already added to the replication list)
		Set<String> allocationCheckedList = new HashSet<>();
		for (ProductOverlayAssetClassReplication replication : CollectionUtils.getIterable(repList)) {
			PortfolioReplicationAllocationSecurityConfig allocationConfig = replication.getAllocationConfig();
			// Need to Include the InvestmentAccountAssetClassID in the Unique Key for the Allocation at this point
			// because there can be cases where an account uses the same replication for different asset classes
			// Applied here, because only matters for allocations with more than one security mapped to it
			// So, originally when getting the replication - don't have to do that part twice
			String allocId = replication.getOverlayAssetClass().getAccountAssetClass().getId() + "_" + allocationConfig.getUniqueKey();
			if (!allocationCheckedList.add(allocId)) {
				continue;
			}
			if (allocationConfig.isTargetAdjustmentsNeeded()) {
				// When finding matching - first filter on the allocation unique key - then filter on the asset class
				List<ProductOverlayAssetClassReplication> replicationList = CollectionUtils.getStream(repList)
						.filter(r -> r.getAllocationConfig() != null && StringUtils.isEqual(r.getAllocationConfig().getUniqueKey(), allocationConfig.getUniqueKey()))
						.filter(r -> CompareUtils.isEqual(r.getOverlayAssetClass().getAccountAssetClass(), replication.getOverlayAssetClass().getAccountAssetClass()))
						.collect(Collectors.toList());

				BigDecimal totalTarget = BigDecimal.ZERO;
				BigDecimal totalTargetPercent = BigDecimal.ZERO;
				Map<InvestmentSecurity, BigDecimal> actualExposureMap = new HashMap<>();
				for (ProductOverlayAssetClassReplication overlayAssetClassReplication : CollectionUtils.getIterable(replicationList)) {
					// Pull final target from "current" since that is where all final adjustments had been applied to.
					if (overlayAssetClassReplication.getSecurity().equals(allocationConfig.getCurrentSecurity())) {
						totalTarget = overlayAssetClassReplication.getTargetExposureAdjusted();
						totalTargetPercent = overlayAssetClassReplication.getAllocationWeightAdjusted();
					}
					actualExposureMap.put(overlayAssetClassReplication.getSecurity(), overlayAssetClassReplication.getActualExposureAdjusted());
				}
				// Using Actual Exposure and Current Security Target Adjustment Type Calculator - Calculate New Targets for Each Security
				Map<InvestmentSecurity, BigDecimal> newTargetMap = allocationConfig.getCurrentSecurityTargetAdjustmentType().calculateTargetAmountMap(totalTarget, allocationConfig.getCurrentSecurity(), actualExposureMap);
				for (ProductOverlayAssetClassReplication overlayAssetClassReplication : replicationList) {
					BigDecimal newTarget = newTargetMap.get(overlayAssetClassReplication.getSecurity());
					overlayAssetClassReplication.setTargetExposureAdjusted(newTarget);
					overlayAssetClassReplication.setAllocationWeightAdjusted(MathUtils.getPercentageOf(totalTargetPercent, getPercentValueForProductOverlayAssetClassReplication(overlayAssetClassReplication, "Adjusted Target Allocation %", newTarget, totalTarget), true));
				}
			}
		}
		// Recalc Contract Value where needed after targets are updated (Currently applies to Options Notional Only where Delta value is negated based on Adjusted Target)
		for (ProductOverlayAssetClassReplication overlayAssetClassReplication : runConfig.getOverlayAssetClassReplicationList()) {
			getProductReplicationService().recalculateProductOverlayAssetClassReplicationSecurityInfo(overlayAssetClassReplication);
		}
	}


	private BigDecimal getPercentValueForProductOverlayAssetClassReplication(ProductOverlayAssetClassReplication rep, String fieldName, BigDecimal value, BigDecimal total) {
		BigDecimal result;
		try {
			result = CoreMathUtils.getPercentValue(value, total, true, true);
		}
		catch (ValidationException e) {
			throw new ValidationExceptionWithCause("InvestmentReplication", rep.getReplication().getId(), "Error calculating [" + fieldName + "] for Asset Class: "
					+ rep.getOverlayAssetClass().getLabel() + ", Replication: " + rep.getReplication().getName() + ", Security: " + rep.getSecurity().getLabel() + ": " + e.getMessage());
		}
		return result;
	}


	private void cleanUpMatchingReplicationTargets(ProductOverlayRunConfig runConfig, ProductOverlayAssetClassReplication rep, boolean useActualForMatching) {
		List<ProductOverlayAssetClassReplication> matchingRepList = runConfig.getMatchingReplicationList(rep);
		if (!CollectionUtils.isEmpty(matchingRepList)) {
			for (ProductOverlayAssetClassReplication matchRep : CollectionUtils.getIterable(matchingRepList)) {
				BigDecimal addToTarget = MathUtils.getPercentageOf(matchRep.getAllocationWeightAdjusted(), (useActualForMatching ? rep.getActualExposureAdjusted() : rep.getTargetExposureAdjusted()),
						true);
				BigDecimal newTarget = MathUtils.add(matchRep.getTargetExposureAdjusted(), addToTarget);

				if (useActualForMatching) { // Set Original Target to Original Actual
					matchRep.setTargetExposure(MathUtils.add(matchRep.getTargetExposure(),
							MathUtils.getPercentageOf(matchRep.getAllocationConfig().getReplicationAllocation().getAllocationWeight(), rep.getActualExposure(), true)));
				}
				matchRep.setTargetExposureAdjusted(newTarget);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////                      Setup                                   /////////
	////////////////////////////////////////////////////////////////////////////////


	private void setupReplications(ProductOverlayRunConfig runConfig) {
		for (Map.Entry<InvestmentAccountAssetClass, ProductOverlayAssetClass> iacEntry : runConfig.getAssetClassMap().entrySet()) {
			ProductOverlayAssetClass ac = iacEntry.getValue();

			if (ac.getPrimaryReplication() != null) {
				BigDecimal secondaryTotalToAllocate = (ac.getSecondaryReplication() != null ? ac.getSecondaryReplicationAmount() : null);
				BigDecimal primaryTotalToAllocate = (secondaryTotalToAllocate == null ? runConfig.getAssetClassReplicationTotalTarget(ac) : MathUtils.subtract(
						runConfig.getAssetClassReplicationTotalTarget(ac), secondaryTotalToAllocate));

				List<ProductOverlayAssetClassReplication> replications = createProductOverlayAssetClassReplicationList(runConfig, ac, ac.getPrimaryReplication().getId(), primaryTotalToAllocate, false);

				// Secondary Replication
				if (ac.getSecondaryReplication() != null) {
					// If secondary amount to allocate is null, it should actually be actual allocation.  We keep a map of what to update
					// after positions are loaded, when we know what the actual is, and then can adjust primary & secondary targets accordingly
					if (secondaryTotalToAllocate == null) {
						runConfig.getSecondaryToPrimaryReplicationMap().put(ac.getSecondaryReplication().getId(), ac.getPrimaryReplication().getId());
					}
					replications.addAll(createProductOverlayAssetClassReplicationList(runConfig, ac, ac.getSecondaryReplication().getId(), secondaryTotalToAllocate == null ? BigDecimal.ZERO
							: secondaryTotalToAllocate, false));
				}

				runConfig.getAssetClassReplicationListMap().put(iacEntry.getKey(), replications);
			}
		}
	}


	private List<PortfolioReplicationAllocationSecurityConfig> getAllocationSecurityConfigListForReplication(ProductOverlayRunConfig runConfig, ProductOverlayAssetClass ac, Integer replicationId) {
		// If we already looked it up/calculated it - return what was stored
		String key = ac.getTradingClientAccount().getId() + "_" + replicationId;
		if (runConfig.getReplicationAllocationConfigMap().containsKey(key)) {
			return runConfig.getReplicationAllocationConfigMap().get(key);
		}
		// otherwise look it up/calculate it
		// Handles dynamic replications and also setting current security
		List<PortfolioReplicationAllocationSecurityConfig> allocConfigList = getPortfolioReplicationHandler().getPortfolioReplicationAllocationSecurityConfigListForBalanceDate(replicationId, runConfig.getBalanceDate(), getInvestmentSecurityContractsForAssetClass(runConfig, ac));

		runConfig.getReplicationAllocationConfigMap().put(key, allocConfigList);
		return allocConfigList;
	}


	private List<ProductOverlayAssetClassReplication> createProductOverlayAssetClassReplicationList(ProductOverlayRunConfig runConfig, ProductOverlayAssetClass overlayAssetClass,
	                                                                                                Integer replicationId, BigDecimal totalToAllocate, boolean matching) {

		List<ProductOverlayAssetClassReplication> replications = new ArrayList<>();
		Map<Integer, BigDecimal> matchingReplicationMap = new LinkedHashMap<>();
		if (replicationId != null) {
			List<PortfolioReplicationAllocationSecurityConfig> allocationConfigList = getAllocationSecurityConfigListForReplication(runConfig, overlayAssetClass, replicationId);
			for (PortfolioReplicationAllocationSecurityConfig allocConfig : CollectionUtils.getIterable(allocationConfigList)) {
				InvestmentReplicationAllocation alloc = allocConfig.getReplicationAllocation();
				BigDecimal weight = alloc.getAllocationWeight();
				// If allocation weight = 0, include it if for now, if no actuals will remove it,
				// otherwise used to indicate to traders to sell existing positions
				if (weight == null) {
					continue;
				}
				// Check if We Need to Adjust the weight - ONLY done if it is NOT a matching replication
				if (!matching && !MathUtils.isEqual(totalToAllocate, runConfig.getAssetClassReplicationTotalTarget(overlayAssetClass))) {
					BigDecimal percentageChange = CoreMathUtils.getPercentValue(totalToAllocate, runConfig.getAssetClassReplicationTotalTarget(overlayAssetClass));
					weight = MathUtils.multiply(weight, percentageChange);
				}

				BigDecimal allocate = MathUtils.getPercentageOf(alloc.getAllocationWeight(), totalToAllocate, true);

				replications.add(createProductOverlayAssetClassReplication(overlayAssetClass, allocConfig, weight, allocate, matching));

				if (alloc.getMatchingReplication() != null) {
					runConfig.addReplicationAllocationMatchingReplicationToMap(overlayAssetClass, alloc);
					if (!matchingReplicationMap.containsKey(alloc.getMatchingReplication().getId())) {
						matchingReplicationMap.put(alloc.getMatchingReplication().getId(), allocate);
					}
					else {
						matchingReplicationMap.put(alloc.getMatchingReplication().getId(), MathUtils.add(matchingReplicationMap.get(alloc.getMatchingReplication().getId()), allocate));
					}
				}
			}
		}

		for (Map.Entry<Integer, BigDecimal> integerBigDecimalEntry : matchingReplicationMap.entrySet()) {
			boolean useActual = overlayAssetClass.getAccountAssetClass().isUseActualForMatchingReplication();
			// If Use Actual = set Total to allocate = 0 - will be set after actual values are populated
			replications.addAll(createProductOverlayAssetClassReplicationList(runConfig, overlayAssetClass, integerBigDecimalEntry.getKey(), (useActual ? BigDecimal.ZERO : integerBigDecimalEntry.getValue()), true));
		}
		return replications;
	}


	private ProductOverlayAssetClassReplication createProductOverlayAssetClassReplication(ProductOverlayAssetClass overlayAssetClass, PortfolioReplicationAllocationSecurityConfig replicationAllocationConfig, BigDecimal adjustedAllocationWeight, BigDecimal totalToAllocate, boolean matching) {
		ProductOverlayAssetClassReplication rep = new ProductOverlayAssetClassReplication();
		rep.setOverlayAssetClass(overlayAssetClass);
		rep.setReplication(replicationAllocationConfig.getReplicationAllocation().getReplication());
		rep.setReplicationAllocationHistory(replicationAllocationConfig.getReplicationAllocationHistory());
		rep.setAllocationConfig(replicationAllocationConfig);
		rep.setMatchingAllocation(matching);
		rep.setTargetExposure(totalToAllocate);
		rep.setTargetExposureAdjusted(matching ? BigDecimal.ZERO : totalToAllocate);
		rep.setAllocationWeightAdjusted(adjustedAllocationWeight);
		return rep;
	}


	/**
	 * APPLY MANAGER ASSIGNMENT SECURITIES BALANCE REPLICATION ALLOCATIONS TO ADDITIONAL EXPOSURE FIELD
	 */
	private void allocateAdditionalExposureToReplications(ProductOverlayRunConfig runConfig) {
		List<ProductOverlayManagerAccount> mgrList = runConfig.getOverlayManagerAccountAllocateSecuritiesToReplicationsList();
		for (ProductOverlayManagerAccount overlayManager : CollectionUtils.getIterable(mgrList)) {
			// This is verified in the allocator bean, but to avoid extra step of getting the bean if there is no securities balance...
			if (!MathUtils.isNullOrZero(overlayManager.getSecuritiesAllocation())) {
				ProductOverlayManagerAccountSecuritiesReplicationByInvestmentSecurityAllocator allocator = (ProductOverlayManagerAccountSecuritiesReplicationByInvestmentSecurityAllocator) getSystemBeanService().getBeanInstance(
						overlayManager.getManagerAccountAssignment().getSecuritiesBalanceReplicationAllocatorBean());
				allocator.allocate(overlayManager, runConfig.getAssetClassReplicationListMap().get(overlayManager.getAccountAssetClass()));
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////            Securities/Positions --> Replications             //////////
	////////////////////////////////////////////////////////////////////////////////


	private boolean applySecuritiesToReplications(ProductOverlayRunConfig runConfig, boolean estimateTargets) {
		// If any targets were applied estimates returns true so know to recalc
		boolean appliedEstimates = false;

		for (Map.Entry<InvestmentAccountAssetClass, List<ProductOverlayAssetClassReplication>> iacEntry : runConfig.getAssetClassReplicationListMap().entrySet()) {
			ProductOverlayAssetClass ac = runConfig.getAssetClassMap().get(iacEntry.getKey());

			// Used Client Directed Cash Targets when after the target was set and want to follow Overlay Target = Overlay Exposure logic
			// Will need an accurate target in case the contract is split
			boolean applyEstimates = estimateTargets && isEstimateTargetsForAssetClass(runConfig, ac);
			List<ProductOverlayAssetClassReplication> assetClassTargetReps = null;
			if (applyEstimates) {
				appliedEstimates = true;
				assetClassTargetReps = getProductOverlayAssetClassEstimateTargetReplications(runConfig, ac);
			}

			List<ProductOverlayAssetClassReplication> repList = iacEntry.getValue();
			for (int i = 0; i < CollectionUtils.getSize(repList); i++) {
				ProductOverlayAssetClassReplication assetClassReplication = repList.get(i);

				// Already Processed
				if (assetClassReplication.getSecurity() != null) {
					continue;
				}

				BigDecimal clientDirectedTargetContracts = BigDecimal.ZERO;

				PortfolioReplicationAllocationSecurityConfig allocationConfig = assetClassReplication.getAllocationConfig();
				List<InvestmentSecurity> allocationSecurityList = allocationConfig.getSecurityList();
				InvestmentSecurity current = allocationConfig.getCurrentSecurity();

				if (applyEstimates) {
					clientDirectedTargetContracts = CoreMathUtils.sumProperty(BeanUtils.filter(assetClassTargetReps, overlayReplication -> CollectionUtils.contains(allocationConfig.getSecurityList(), overlayReplication.getSecurity())),
							ProductOverlayAssetClassReplication::getTargetContractsAdjusted);
				}

				// No existing securities, use the current one
				if (CollectionUtils.isEmpty(allocationSecurityList)) {
					if (allocationConfig.getCurrentSecurity() != null) {
						setupReplicationSecurityInfo(assetClassReplication, allocationConfig.getCurrentSecurity(), runConfig, clientDirectedTargetContracts);
					}
					else {
						throw new ValidationExceptionWithCause("InvestmentReplication", allocationConfig.getReplicationAllocation().getReplication().getId(),
								"Missing a current security while processing asset class [" + iacEntry.getKey().getAssetClass().getName() + "] for Investment Replication Allocation ["
										+ allocationConfig.getReplicationAllocation().getLabel() + "], and there are no existing positions for the account that match this replication.");
					}
				}

				else {
					// If there is a current security, and it's not the same as what's held - add it to the list
					if (current != null && !allocationSecurityList.contains(current)) {
						allocationSecurityList.add(current);
					}
				}

				int numSecurityAllocations = CollectionUtils.getSize(allocationSecurityList);
				// Only one - set it up
				if (numSecurityAllocations == 1) {
					setupReplicationSecurityInfo(assetClassReplication, CollectionUtils.getFirstElementStrict(allocationSecurityList), runConfig, clientDirectedTargetContracts);
				}
				// More than one - if no current specified - last in the list after sorting takes on the role of the "current" one
				else if (numSecurityAllocations > 1) {
					// Order them in order from earliest end date to newest end date
					allocationSecurityList = BeanUtils.sortWithFunction(allocationSecurityList, InvestmentSecurity::getEndDate, true);

					if (current == null) {
						// Set the last one in the list as the current one if none specified
						current = allocationSecurityList.get(numSecurityAllocations - 1);
					}

					// Create a new ProductOverlayAssetClassReplication object for each security allocated to this replication allocation.
					// Clone the replication first in case current is processed before others are cloned from it
					ProductOverlayAssetClassReplication clonedRep = BeanUtils.cloneBean(assetClassReplication, false, false);
					Iterator<InvestmentSecurity> securityIterator = CollectionUtils.getIterable(allocationSecurityList).iterator();

					while (securityIterator.hasNext()) {
						InvestmentSecurity allocationSecurity = securityIterator.next();
						// The Last one is the rep, the "older" ones create new replications
						if (current.equals(allocationSecurity)) {
							setupReplicationSecurityInfo(assetClassReplication, allocationSecurity, runConfig, clientDirectedTargetContracts);
						}
						else {
							// Use the cloned rep so don't get any current security processed values
							ProductOverlayAssetClassReplication newRep = BeanUtils.cloneBean(clonedRep, false, false);
							setupReplicationSecurityInfo(newRep, allocationSecurity, runConfig, clientDirectedTargetContracts);
							List<ProductOverlayAssetClassReplication> matchingReps = CollectionUtils.getStream(repList)
									.filter(r -> r.getAllocationConfig().getCurrentSecurity() != null)
									.filter(r -> r.getAllocationConfig().getCurrentSecurity().equals(newRep.getSecurity()))
									.filter(r -> r.getReplication().equals(newRep.getReplication()))
									.collect(Collectors.toList());
							if (CollectionUtils.isEmpty(matchingReps)) {
								repList.add(newRep);
							}
							else {
								// If we are excluding it, then make sure the current security calculator also knows we are excluding so it doesn't try to adjust to TFA
								securityIterator.remove();
							}
						}
					}
				}
			}
			// In case we added items, need to reset the list in the map
			runConfig.getAssetClassReplicationListMap().put(iacEntry.getKey(), repList);
		}

		return appliedEstimates;
	}


	private void setupReplicationSecurityInfo(ProductOverlayAssetClassReplication rep, InvestmentSecurity security, ProductOverlayRunConfig runConfig,
	                                          BigDecimal clientDirectedTargetContractsEstimate) {

		String exchangeRateDataSourceName = runConfig.getExchangeRateDataSourceName();
		Map<String, ProductOverlayAssetClassReplication> replicationSetupMap = runConfig.getReplicationSetupMap();
		Map<String, BigDecimal> priceOverrideMap = runConfig.getBeforeStartDateSecurityIdPriceMap();

		ProductOverlayAssetClassReplication copyFrom = null;
		String key = rep.getOverlayAssetClass().getAccountAssetClass().getId() + "_" + rep.getReplication().getId() + "_" + security.getId();
		if (replicationSetupMap != null && replicationSetupMap.containsKey(key)) {
			copyFrom = replicationSetupMap.get(key);
		}
		if (copyFrom != null) {
			rep.copySecurityInfo(copyFrom);
		}
		else {
			if (priceOverrideMap.containsKey(rep.getOverlayAssetClass().getAccountAssetClass().getId() + "_" + security.getId())) {
				rep.setSecurityPrice(priceOverrideMap.get(rep.getOverlayAssetClass().getAccountAssetClass().getId() + "_" + security.getId()));
			}
			getProductReplicationService().setupProductOverlayAssetClassReplicationSecurityInfo(rep, security, exchangeRateDataSourceName, runConfig.isExcludeMispricing());

			if (replicationSetupMap != null) {
				replicationSetupMap.put(key, rep);
			}
		}

		// Validate Cash Exposure Rep - Cannot use Client Directed Trading or Be a Secondary Replication
		if (rep.isCashExposure()) {
			ValidationUtils.assertFalse(rep.getOverlayAssetClass().getAccountAssetClass().isClientDirectedCashUsed(), "Client Directed Trading Cannot be used for Asset Class ["
					+ rep.getOverlayAssetClass().getLabel() + "] because it uses a cash exposure replication allocation.");
			ValidationUtils.assertTrue(rep.isPrimaryReplication(), "Asset Class [" + rep.getOverlayAssetClass().getLabel() + "] replication [" + rep.getReplication().getName()
					+ "] is invalid.  This replication contains a cash exposure allocation which can be used ONLY for primary replications at this time.");

			runConfig.addAssetClassToCashExposureReplicationList(rep.getOverlayAssetClass().getAccountAssetClass());
		}

		// Updates Target Exposure Adjusted so it resembles the target contracts that we should have - will only be used for contract splitting since after
		// everything is applied, targets are overridden again to match overlay exposure.
		if (!MathUtils.isNullOrZero(clientDirectedTargetContractsEstimate)) {
			rep.setTargetExposureAdjusted(MathUtils.multiply(clientDirectedTargetContractsEstimate, rep.getValue()));
		}
	}


	/**
	 * Returns true if we'll need to estimate targets that could be used for contract splitting.  Estimated targets are used when:
	 * 1.  Minimize Imbalances Calculation Type is used
	 * 2.  Account is configured with Effective Cash - Overlay Exposure bands to determine trading
	 * 3.  Asset Class uses Client Directed Trading
	 */
	private boolean isEstimateTargetsForAssetClass(ProductOverlayRunConfig runConfig, ProductOverlayAssetClass ac) {
		if (runConfig.isMinimizeImbalancesUsed()) {
			return true;
		}
		if (runConfig.getAccountRebalanceConfig() != null
				&& (runConfig.getAccountRebalanceConfig().getMinEffectiveCashExposure() != null || runConfig.getAccountRebalanceConfig().getMaxEffectiveCashExposure() != null)) {
			return true;
		}
		return isClientDirectedCashUsedForNoTrades(ac.getAccountAssetClass(), ac.getOverlayRun().getBalanceDate());
	}


	/**
	 * Pulls the securities from the store that apply to this asset class (or all asset classes).
	 * If we hold a net quantity of 0, and the security's hierarchy is close on maturity only, we skip the security, otherwise we still include it.
	 * <p>
	 * Also validates the security is active on balance date + 1
	 */
	private List<InvestmentSecurity> getInvestmentSecurityContractsForAssetClass(ProductOverlayRunConfig runConfig, ProductOverlayAssetClass ac) {
		if (runConfig.getAssetClassSecurityMap().containsKey(ac.getAccountAssetClass())) {
			return runConfig.getAssetClassSecurityMap().get(ac.getAccountAssetClass());
		}

		Map<Integer, PortfolioAccountContractStore.SecurityPositionQuantity> securityQuantityMap = runConfig.getContractStore().getSecurityIdQuantityMapForAccountAssetClass(ac.getAccountAssetClass());
		List<Integer> securityIds = new ArrayList<>();

		for (Map.Entry<Integer, PortfolioAccountContractStore.SecurityPositionQuantity> securityIdToQuantityEntry : securityQuantityMap.entrySet()) {
			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityIdToQuantityEntry.getKey());
			PortfolioAccountContractStore.SecurityPositionQuantity securityPositionQuantity = securityIdToQuantityEntry.getValue();
			if (MathUtils.isNullOrZero(securityPositionQuantity.getQuantity())) {
				if (!InvestmentUtils.isCloseOnMaturityOnly(security)) {
					securityIds.add(securityIdToQuantityEntry.getKey());
				}
			}
			else {

				if (!InvestmentUtils.isSecurityActiveOn(security, runConfig.getNextDay())) {
					boolean allowSecurity = false;
					boolean invalidPrices = false;
					// if Date is before Issue Date, then get positions and validate using settlement date
					// can buy a bond before its IssueDate as long as it settles on or after the issue date
					if (security.getStartDate() != null && DateUtils.compare(runConfig.getNextDay(), security.getStartDate(), false) < 0) {
						allowSecurity = true;
						BigDecimal price = null;
						for (AccountingPosition pos : CollectionUtils.getIterable(runConfig.getContractStore().getSecurityAccountingPositionListForAccountAssetClass(ac.getAccountAssetClass(),
								security.getId()))) {
							// validate using the settlement date
							if (!InvestmentUtils.isSecurityActiveOn(security, runConfig.getNextDay(), pos.getSettlementDate())) {
								allowSecurity = false;
							}
							if (!invalidPrices && pos.getPrice() != null) {
								if (price == null) {
									price = pos.getPrice();
								}
								else if (!MathUtils.isEqual(price, pos.getPrice())) {
									invalidPrices = true;
									price = null;
								}
							}
							// Don't throw an exception here - only need price if it is actually used on a replication
							// Need to verify/map to each asset class so cases for sub-accounts we check transactions that would apply to that asset class
							if (allowSecurity && !invalidPrices && price != null) {
								runConfig.getBeforeStartDateSecurityIdPriceMap().put(ac.getAccountAssetClass().getId() + "_" + security.getId(), price);
							}
						}
					}
					if (!allowSecurity) {
						throw new ValidationExceptionWithCause("InvestmentSecurity", security.getId(), "Security [" + security.getLabel() + "] has position quantity of ["
								+ CoreMathUtils.formatNumberInteger(securityIdToQuantityEntry.getValue().getUnadjustedQuantity()) + "] however it is not an active security as of the end of business on the balance date ["
								+ DateUtils.fromDateShort(runConfig.getBalanceDate()) + "].");
					}
				}
				securityIds.add(securityIdToQuantityEntry.getKey());
			}
		}
		return getInvestmentInstrumentService().getInvestmentSecurityListByIds(securityIds);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////            Client Directed Trading Methods                   //////////
	////////////////////////////////////////////////////////////////////////////////


	private boolean isClientDirectedCashUsedForNoTrades(InvestmentAccountAssetClass ac, Date balanceDate) {
		if (ac.getClientDirectedCashOption() != null && ac.getClientDirectedCashOption().isUseTargetFollowsActualAfterDate()) {
			if (DateUtils.compare(ac.getClientDirectedCashDate(), balanceDate, false) < 0) {
				return true;
			}
		}
		return false;
	}


	private List<ProductOverlayAssetClassReplication> getProductOverlayAssetClassEstimateTargetReplications(ProductOverlayRunConfig runConfig, ProductOverlayAssetClass ac) {

		boolean clientDirected = (ac.getAccountAssetClass().getClientDirectedCashDate() != null);
		Date targetRunDate = clientDirected ? ac.getAccountAssetClass().getClientDirectedCashDate() : runConfig.getPreviousBusinessDay();
		String dateKey = DateUtils.fromDateShort(targetRunDate);
		List<ProductOverlayAssetClassReplication> targetReps;
		if (runConfig.getEstimateTargetRunMap().containsKey(dateKey)) {
			targetReps = runConfig.getEstimateTargetRunMap().get(dateKey);
		}
		else {

			PortfolioRun targetRun = getPortfolioRunService().getPortfolioRunByMainRun(runConfig.getClientInvestmentAccountId(), targetRunDate);
			if (targetRun == null || targetRun.isIncomplete()) {
				// If Client Directed Throw An Exception
				if (clientDirected) {
					throw new ValidationExceptionWithCause(
							"InvestmentAccountAssetClass",
							ac.getAccountAssetClass().getId(),
							"Asset Class ["
									+ ac.getLabel()
									+ "] has a Client Directed Cash Target Date of ["
									+ dateKey
									+ "] however the main run for this account on that date is either missing, or didn't process asset classes.  Main run's replications will be needed to estimate targets before applying the Overlay Target = Overlay Exposure logic.");
				}
				// Otherwise just assume a zero target
				targetReps = new ArrayList<>();
			}
			else {
				targetReps = getProductOverlayService().getProductOverlayAssetClassReplicationListByRun(targetRun.getId());
			}
			runConfig.getEstimateTargetRunMap().put(dateKey, targetReps);
		}

		// Filter By Asset Class
		return BeanUtils.filter(targetReps, overlayReplication -> overlayReplication.getOverlayAssetClass().getAccountAssetClass(), ac.getAccountAssetClass());
	}


	/**
	 * Applies Client Directed Cash bucket based on overlay exposure
	 */
	private boolean setClientDirectedCashFromOverlayExposure(ProductOverlayRunConfig runConfig, boolean applyToAllAssetClasses) {
		boolean clientDirectedCashUpdate = false;
		for (Map.Entry<InvestmentAccountAssetClass, ProductOverlayAssetClass> iacEntry : runConfig.getAssetClassMap().entrySet()) {
			// Skip Main Cash
			if (iacEntry.getKey().getAssetClass().isMainCash()) {
				continue;
			}

			ProductOverlayAssetClass ac = iacEntry.getValue();
			// Client Directed Cash Targets - Fix so that Overlay Target = Overlay Exposure by setting cash needed in Client Directed Cash Bucket
			// Target as a whole is updated and then re-allocated to each rep using it's percentages.
			if (applyToAllAssetClasses || isClientDirectedCashUsedForNoTrades(iacEntry.getKey(), runConfig.getBalanceDate())) {
				// FIXED LATER WHEN REPLICATIONS ARE CLEANED UP
				//if (!applyToAllAssetClasses && iac.getClientDirectedCashOption() != null && iac.getClientDirectedCashOption().isUsePreviousDurationForTargetFollowsActual()) {
				// RECALC OVERLAY EXPOSURE USING PREVIOUS DURATION
				//}
				BigDecimal clientDirectedCash = MathUtils.subtract(ac.getOverlayExposure(), ac.getEffectiveCashTotal());
				if (!MathUtils.isNullOrZero(clientDirectedCash)) {
					clientDirectedCashUpdate = true;
					ac.setClientDirectedCash(MathUtils.subtract(ac.getOverlayExposure(), ac.getEffectiveCashTotal()));
				}
			}
		}
		return clientDirectedCashUpdate;
	}

	/////////////////////////////////////////////////////////////////////////
	/////            Process Trading Bands/Minimize Imbalances        ///////
	/////////////////////////////////////////////////////////////////////////


	/**
	 * Applies Client Directed Cash bucket based on overlay exposure
	 */
	private boolean processOverlayExposureForTradingOrMinimizeImbalances(ProductOverlayRunConfig runConfig) {
		// If Minimize Imbalances Used - Trading Bands are Validated on the Calculator
		if (runConfig.isMinimizeImbalancesUsed()) {
			return getProductOverlayRebalanceService().processProductOverlayMinimizeImbalances(runConfig);
		}

		if (runConfig.isEffectiveCashLessExposureInsideNoTradingBands(null)) {
			setClientDirectedCashFromOverlayExposure(runConfig, true);
			return true;
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////            Currency Contract Processing                      /////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Processes Currency and created {@link PortfolioCurrencyOther} where applies
	 * Applies unrealized currency exposure
	 */
	private void processCurrencyContracts(ProductOverlayRunConfig runConfig) {
		// Currency Other
		List<PortfolioCurrencyOther> currencyOtherList = new ArrayList<>();

		// Currency Replications
		List<ProductOverlayAssetClassReplication> repList = runConfig.getOverlayAssetClassReplicationList();
		List<ProductOverlayAssetClassReplication> fullCurrencyRepList = BeanUtils.filter(repList, ProductOverlayAssetClassReplication::isCurrencyReplication);
		getProductReplicationService().processCurrencyContracts(runConfig.getContractStore(), runConfig.getRun(), fullCurrencyRepList, currencyOtherList, new ArrayList<>(), repList);

		runConfig.setOverlayCurrencyOtherList(currencyOtherList);
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////         Apply Cash Exposure (Overlay Exposure) Values       /////////
	////////////////////////////////////////////////////////////////////////////////


	private void applyCashExposureAmounts(ProductOverlayRunConfig runConfig) {
		// If applies - would be present on Primary Replication Only
		for (InvestmentAccountAssetClass iac : CollectionUtils.getIterable(runConfig.getAccountAssetClassWithCashExposureReplicationList())) {
			List<ProductOverlayAssetClassReplication> assetClassReplicationList = runConfig.getAssetClassReplicationListMap().get(iac);
			assetClassReplicationList = BeanUtils.filter(assetClassReplicationList, ProductOverlayAssetClassReplication::isPrimaryReplication);

			if (CollectionUtils.getSize(assetClassReplicationList) == 1) {
				ProductOverlayAssetClassReplication rep = assetClassReplicationList.get(0);
				// if it's the only one - it should be cash exposure one, but double check
				if (rep.isCashExposure()) {
					// If so - give it actual = target
					// NOTE: Contracts for Cash Exposure = the Exposure because Contract Value is always one
					rep.setActualContracts(rep.getTargetExposureAdjusted());
				}
			}
			else if (CollectionUtils.getSize(assetClassReplicationList) > 1) {
				BigDecimal totalOverlayExposure = BigDecimal.ZERO;
				BigDecimal totalPercentage = BigDecimal.ZERO;

				List<ProductOverlayAssetClassReplication> cashExposureRepList = new ArrayList<>();

				for (ProductOverlayAssetClassReplication rep : assetClassReplicationList) {
					if (rep.isCashExposure()) {
						cashExposureRepList.add(rep);
					}
					else {
						// Uses the original weight of the "current" security only so
						// we don't double could percentages.  Can't use adjusted weight because we need to compare original weight
						// to the original weight of the cash we need to fill, and adjustments to those percentages could be applied
						// from rolling contracts and when including a secondary replication
						totalOverlayExposure = MathUtils.add(totalOverlayExposure, rep.getActualExposureAdjusted());
						if (rep.getAllocationConfig().getCurrentSecurity().equals(rep.getSecurity())) {
							totalPercentage = MathUtils.add(totalPercentage, rep.getAllocationWeight());
						}
					}
				}

				// Calculate exposure total based off of existing amounts/percentage
				BigDecimal expectedTotalExposure = MathUtils.divide(MathUtils.multiply(totalOverlayExposure, MathUtils.BIG_DECIMAL_ONE_HUNDRED), totalPercentage);
				for (ProductOverlayAssetClassReplication cashRep : CollectionUtils.getIterable(cashExposureRepList)) {
					// IF NO Other exposure, set cash exposure to 0 as well.
					if (MathUtils.isNullOrZero(expectedTotalExposure)) {
						cashRep.setActualContracts(BigDecimal.ZERO);
					}
					else {
						cashRep.setActualContracts(MathUtils.getPercentageOf(cashRep.getAllocationWeight(), expectedTotalExposure, true));
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////            Apply Replications --> Asset Classes             /////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Sets {@link ProductOverlayAssetClass#overlayExposure} based on {@link com.clifton.investment.replication.InvestmentReplication}s.
	 * Applies Client Directed Cash bucket based on OverlayExposure and also calculates effective duration based on Replications.
	 */
	private void applyReplicationsToAssetClasses(ProductOverlayRunConfig runConfig) {
		// Keep Track of the Cash Asset Class - Overlay Exposure for Cash will be the opposite of the sum of the other asset class overlay Exposure
		ProductOverlayAssetClass cashAc = CollectionUtils.getFirstElement(BeanUtils.filter(runConfig.getOverlayAssetClassList(), overlayAssetClass -> overlayAssetClass.getAccountAssetClass().getAssetClass().isMainCash()));
		BigDecimal totalOverlayExposure = BigDecimal.ZERO;
		for (Map.Entry<InvestmentAccountAssetClass, List<ProductOverlayAssetClassReplication>> iacEntry : runConfig.getAssetClassReplicationListMap().entrySet()) {
			InvestmentAccountAssetClass iac = iacEntry.getKey();
			ProductOverlayAssetClass ac = runConfig.getAssetClassMap().get(iacEntry.getKey());
			List<ProductOverlayAssetClassReplication> assetClassReplicationList = iacEntry.getValue();

			// Set Effective Duration on the Asset Class for Duration Adjusted Replications
			ac.setEffectiveDuration(getCalculatedEffectiveDuration(assetClassReplicationList));

			// Split the replications matching & not matching and fix rounding and add allocations for matching
			List<ProductOverlayAssetClassReplication> notMatchingRepList = BeanUtils.filter(assetClassReplicationList, overlayReplication -> !overlayReplication.isMatchingAllocation());

			// Do not include matching replication in the overlay Exposure for the asset class.
			if (!ac.getAccountAssetClass().isExcludeFromOverlayExposure()) {
				ac.setOverlayExposure(CoreMathUtils.sumProperty(notMatchingRepList, ProductOverlayAssetClassReplication::getActualExposureAdjusted));
				totalOverlayExposure = MathUtils.add(totalOverlayExposure, ac.getOverlayExposure());

				// Client Directed Cash Targets - Fix so that Overlay Target = Overlay Exposure by setting cash needed in Client Directed Cash Bucket
				// Target as a whole is updated and then re-allocated to each rep using it's percentages.
				if (isClientDirectedCashUsedForNoTrades(iac, runConfig.getBalanceDate())) {
					BigDecimal totalTarget = ac.getOverlayExposure();

					// Recalc Total Target
					if (iac.getClientDirectedCashOption().isUsePreviousDurationForTargetFollowsActual()) {
						totalTarget = BigDecimal.ZERO;
						// Populated Only when need to lookup previous duration adjustment and populated only once for the run and reused for asset classes that need it
						List<ProductOverlayAssetClassReplication> previousNotMatchingReplicationList = runConfig.getPreviousReplicationListForDurationAdjustmentComparison();
						if (CollectionUtils.isEmpty(previousNotMatchingReplicationList)) {
							// Some accounts will have an MOC run the previous day that already has the Duration Adjusted target.  In order to
							// properly compare to the previous day's run:
							// 1. If there are any MOC runs on the previous day - Compare this run's duration to the last MOC run submitted to trading on the previous day.
							PortfolioRun previousRun = getPortfolioRunService().getPortfolioRunLastByMOC(runConfig.getClientInvestmentAccountId(), runConfig.getPreviousBusinessDay());
							// 2. If there aren't any MOCs runs on the previous day - Will compare to the previous day's Main run.
							if (previousRun == null) {
								previousRun = getPortfolioRunService().getPortfolioRunByMainRun(runConfig.getClientInvestmentAccountId(), runConfig.getPreviousBusinessDay());
							}
							if (previousRun != null) {
								previousNotMatchingReplicationList = getProductOverlayService().getProductOverlayAssetClassReplicationListByRun(previousRun.getId());
								previousNotMatchingReplicationList = BeanUtils.filter(previousNotMatchingReplicationList, overlayReplication -> !overlayReplication.isMatchingReplication());
								runConfig.setPreviousReplicationListForDurationAdjustmentComparison(previousNotMatchingReplicationList);
							}
						}
						List<ProductOverlayAssetClassReplication> previousAcRepList = BeanUtils.filter(previousNotMatchingReplicationList, overlayReplication -> overlayReplication.getOverlayAssetClass().getAccountAssetClass(),
								ac.getAccountAssetClass());
						for (ProductOverlayAssetClassReplication r : CollectionUtils.getIterable(notMatchingRepList)) {
							BigDecimal previousDurationAdjustment = null;
							if (r.isDurationAdjusted()) {
								for (ProductOverlayAssetClassReplication pr : CollectionUtils.getIterable(previousAcRepList)) {
									if (pr.getSecurity().equals(r.getSecurity())) {
										previousDurationAdjustment = pr.getDurationAdjustment();
										break;
									}
								}
							}
							// Remove Current Duration Adjustment - Apply Previous and set as the target
							// If None Found set to be the same;
							if (previousDurationAdjustment != null && !MathUtils.isEqual(previousDurationAdjustment, r.getDurationAdjustment())) {
								BigDecimal targetExp = MathUtils.multiply(MathUtils.divide(r.getActualExposure(), r.getDurationAdjustment()), previousDurationAdjustment);
								totalTarget = MathUtils.add(totalTarget, targetExp);
							}
							else {
								totalTarget = MathUtils.add(totalTarget, r.getActualExposure());
							}
						}
					}

					// Apply Total - Reallocated by %ages and set Client Directed Cash
					for (ProductOverlayAssetClassReplication r : CollectionUtils.getIterable(notMatchingRepList)) {
						r.setTargetExposureAdjusted(MathUtils.getPercentageOf(r.getAllocationWeightAdjusted(), totalTarget, true));
					}
					ac.setClientDirectedCash(MathUtils.subtract(totalTarget, ac.getEffectiveCashTotal()));
				}
			}
		}

		if (cashAc != null) {
			if (!MathUtils.isNullOrZero(totalOverlayExposure)) {
				totalOverlayExposure = MathUtils.negate(totalOverlayExposure);
				cashAc.setOverlayExposure(totalOverlayExposure);
			}
		}

		// Apply OverlayExposure to Rollups
		setRollupOverlayAssetClassValues(runConfig);
	}


	private BigDecimal getCalculatedEffectiveDuration(List<ProductOverlayAssetClassReplication> replicationList) {
		boolean found = false;
		for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(replicationList)) {
			if (getProductReplicationService().isReplicationCalculatorDurationRequired(rep)) {
				found = true;
				break;
			}
		}
		List<ProductOverlayAssetClassReplication> durationReplications = null;
		// Only applies if there are Duration Required Replications
		if (found) {
			// If there are Duration Required Replications, Include all where SUPPORTED (not necessarily required: PORTFOLIO-19)
			// Anything that requires duration supports it, but there could be more where it is supported and not required.
			durationReplications = BeanUtils.filter(replicationList, overlayReplication -> overlayReplication.getReplicationType().isDurationSupported());
		}
		if (CollectionUtils.isEmpty(durationReplications)) {
			return null;
		}

		BigDecimal total = BigDecimal.ZERO;
		BigDecimal totalTarget = BigDecimal.ZERO;
		// For each replication, calculate (# Actual Contracts *  Duration * Price * Price Multiplier)
		for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(durationReplications)) {
			BigDecimal securityValue = MathUtils.multiply(rep.getDuration(), rep.getActualContractsAdjusted());
			securityValue = MathUtils.multiply(securityValue, rep.getSecurityPrice());
			securityValue = MathUtils.multiply(securityValue, rep.getSecurity().getPriceMultiplier());
			// SUM these values and add the Target to the Total Target
			total = MathUtils.add(total, securityValue);
			totalTarget = MathUtils.add(totalTarget, rep.getTargetExposureAdjusted());
		}
		// Effective Duration = SUM/Target
		if (!MathUtils.isNullOrZero(total) && !MathUtils.isNullOrZero(totalTarget)) {
			return MathUtils.divide(total, totalTarget);
		}
		return BigDecimal.ZERO;
	}


	private void setRollupOverlayAssetClassValues(ProductOverlayRunConfig runConfig) {
		List<ProductOverlayAssetClass> acList = runConfig.getOverlayAssetClassList();
		List<ProductOverlayAssetClass> rollupList = BeanUtils.filter(acList, ProductOverlayAssetClass::isRollupAssetClass);
		// Only bother running if there are rollups
		if (!CollectionUtils.isEmpty(rollupList)) {
			for (ProductOverlayAssetClass ac : CollectionUtils.getIterable(acList)) {
				setRollupOverlayAssetClassValuesFromChild(ac, acList);
			}
		}
	}


	private void setRollupOverlayAssetClassValuesFromChild(ProductOverlayAssetClass child, List<ProductOverlayAssetClass> acList) {
		while (child.getParent() != null) {
			for (ProductOverlayAssetClass ac : CollectionUtils.getIterable(acList)) {
				if (ac.equals(child.getParent())) {
					ac.setOverlayExposure(MathUtils.add(ac.getOverlayExposure(), child.getOverlayExposure()));
					child = ac;
					break;
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////     Product Overlay Asset Class Replication Clean Up Methods    ////////
	////////////////////////////////////////////////////////////////////////////////


	private void cleanUpAndSaveReplications(ProductOverlayRunConfig runConfig) {
		cleanUpRoundingForProductOverlayAssetClassReplicationAmounts(runConfig);
		List<ProductOverlayAssetClassReplication> replicationList = removeZeroTargetAndActualReplications(runConfig);

		BigDecimal overlayTargetTotal = CoreMathUtils.sumProperty(replicationList, ProductOverlayAssetClassReplication::getOverlayTargetTotal);
		BigDecimal overlayExposureTotal = CoreMathUtils.sumProperty(replicationList, ProductOverlayAssetClassReplication::getOverlayExposureTotal);
		BigDecimal mispricingTotal = (runConfig.isExcludeMispricing() ? null : CoreMathUtils.sumProperty(replicationList, ProductOverlayAssetClassReplication::getMispricingValue));
		getProductOverlayService().saveProductOverlayAssetClassReplicationListByRun(runConfig.getRun().getId(), overlayTargetTotal, overlayExposureTotal, mispricingTotal, replicationList);
		getPortfolioRunService().savePortfolioCurrencyOtherListByRun(runConfig.getRun().getId(), runConfig.getOverlayCurrencyOtherList());
	}


	/**
	 * Primary/Secondary Replications (i.e. Not Matching) Applies Sum Property Difference for:
	 * targetExposure
	 * targetExposureAdjusted
	 * allocationWeightAdjusted
	 * Matching Replications Applies Sum Property Difference for:
	 * allocationWeightAdjusted (Also readjusts to ensure 100% total)
	 */
	private void cleanUpRoundingForProductOverlayAssetClassReplicationAmounts(ProductOverlayRunConfig runConfig) {
		if (!runConfig.isDoNotAdjustReplicationAllocationPercentages()) {
			// Go through Recently created replications and fix rounding, get current replication allocation values, etc.
			for (Map.Entry<InvestmentAccountAssetClass, List<ProductOverlayAssetClassReplication>> iacEntry : runConfig.getAssetClassReplicationListMap().entrySet()) {
				InvestmentAccountAssetClass iac = iacEntry.getKey();
				ProductOverlayAssetClass ac = runConfig.getAssetClassMap().get(iac);
				List<ProductOverlayAssetClassReplication> assetClassReplicationList = iacEntry.getValue();

				// Split the replications matching & not matching and fix rounding and add allocations for matching
				List<ProductOverlayAssetClassReplication> notMatchingRepList = BeanUtils.filter(assetClassReplicationList, overlayReplication -> !overlayReplication.isMatchingAllocation());
				List<ProductOverlayAssetClassReplication> matchingRepList = BeanUtils.filter(assetClassReplicationList, ProductOverlayAssetClassReplication::isMatchingAllocation);

				if (!CollectionUtils.isEmpty(notMatchingRepList)) {
					// Support for not matching where sum != 100%
					BigDecimal notMatchingTotal = CoreMathUtils.sumProperty(notMatchingRepList, ProductOverlayAssetClassReplication::getAllocationWeightAdjusted);
					BigDecimal notMatchingTargetTotal = CoreMathUtils.sumProperty(notMatchingRepList, ProductOverlayAssetClassReplication::getTargetExposureAdjusted);
					BigDecimal assetClassReplicationTargetTotal = runConfig.getAssetClassReplicationTotalTarget(ac);
					// If (when rounded to two decimals) equals 100% then considered 100% to avoid rounding issues
					if (MathUtils.isEqual(MathUtils.BIG_DECIMAL_ONE_HUNDRED, MathUtils.round(notMatchingTotal, 2))) {
						CoreMathUtils.applySumPropertyDifference(notMatchingRepList, ProductOverlayAssetClassReplication::getTargetExposure, ProductOverlayAssetClassReplication::setTargetExposure, assetClassReplicationTargetTotal, true);
						CoreMathUtils.applySumPropertyDifference(notMatchingRepList, ProductOverlayAssetClassReplication::getTargetExposureAdjusted, ProductOverlayAssetClassReplication::setTargetExposureAdjusted, assetClassReplicationTargetTotal, true);
						notMatchingTargetTotal = assetClassReplicationTargetTotal;
					}
					else {
						CoreMathUtils.applySumPropertyDifference(notMatchingRepList, ProductOverlayAssetClassReplication::getTargetExposure, ProductOverlayAssetClassReplication::setTargetExposure, notMatchingTargetTotal, true);
						CoreMathUtils.applySumPropertyDifference(notMatchingRepList, ProductOverlayAssetClassReplication::getTargetExposureAdjusted, ProductOverlayAssetClassReplication::setTargetExposureAdjusted, notMatchingTargetTotal, true);
					}
					// Clean up adjusted % based on adjusted value as a percent of total
					// only if total is not equal to 0 otherwise recalc based on adjusted percentages
					boolean recalcFromPercent = MathUtils.isEqual(BigDecimal.ZERO, MathUtils.round(notMatchingTargetTotal, 0));

					for (ProductOverlayAssetClassReplication nmr : CollectionUtils.getIterable(notMatchingRepList)) {
						if (recalcFromPercent) {
							nmr.setAllocationWeightAdjusted(getPercentValueForProductOverlayAssetClassReplication(nmr, "Adjusted Target Allocation %", nmr.getAllocationWeightAdjusted(), notMatchingTotal));
						}
						else {
							nmr.setAllocationWeightAdjusted(getPercentValueForProductOverlayAssetClassReplication(nmr, "Adjusted Target Allocation %", nmr.getTargetExposureAdjusted(),
									notMatchingTargetTotal));
						}
					}
					// Fix Rounding
					applyAllocationWeightAdjustedSumToOneHundred(notMatchingRepList);
					setPortfolioRunReplicationRebalanceTriggerValues(notMatchingRepList, notMatchingTargetTotal);
				}

				// GET ACTUAL MATCHING ALLOCATION & OTHER MATCHING ALLOCATION
				if (!CollectionUtils.isEmpty(matchingRepList)) {
					BigDecimal matchingTotal = CoreMathUtils.sumProperty(matchingRepList, ProductOverlayAssetClassReplication::getAllocationWeightAdjusted);
					BigDecimal matchingTargetTotal = CoreMathUtils.sumProperty(matchingRepList, ProductOverlayAssetClassReplication::getTargetExposureAdjusted);

					// For Matching Replications let's recalc and set adjusted targets based on the actual target amounts over the total.
					// Need to do this because matching replications can span multiple replications and each would be set to 100%
					// this way we get the actual target % as a percentage across all currencies

					// Clean up adjusted % based on adjusted value as a percent of total
					// only if total is not equal to 0 otherwise recalc based on adjusted percentages
					boolean recalcFromPercent = MathUtils.isEqual(BigDecimal.ZERO, MathUtils.round(matchingTargetTotal, 0));
					for (ProductOverlayAssetClassReplication mr : CollectionUtils.getIterable(matchingRepList)) {
						if (recalcFromPercent) {
							mr.setAllocationWeightAdjusted(getPercentValueForProductOverlayAssetClassReplication(mr, "Adjusted Target Allocation %", mr.getAllocationWeightAdjusted(), matchingTotal));
						}
						else {
							mr.setAllocationWeightAdjusted(getPercentValueForProductOverlayAssetClassReplication(mr, "Adjusted Target Allocation %", mr.getTargetExposureAdjusted(), matchingTargetTotal));
						}
					}

					// Fix any rounding
					applyAllocationWeightAdjustedSumToOneHundred(matchingRepList);
					setPortfolioRunReplicationRebalanceTriggerValues(matchingRepList, matchingTargetTotal);
				}
			}
		}

		// Re-Allocate Currency Other Based on New Targets - Only attempt this if we have CCY Other
		if (!CollectionUtils.isEmpty(runConfig.getOverlayCurrencyOtherList())) {
			PortfolioReplicationCurrencyOtherConfig<ProductOverlayAssetClassReplication> ccyOtherConfig = new PortfolioReplicationCurrencyOtherConfig<>(PortfolioAccountContractStoreTypes.ACTUAL, runConfig.getRun().getClientInvestmentAccount().getBaseCurrency(),
					BeanUtils.filter(runConfig.getOverlayAssetClassReplicationList(), ProductOverlayAssetClassReplication::isCurrencyReplication));
			ccyOtherConfig.applyCurrencyOtherToReplications(runConfig.getOverlayCurrencyOtherList());
		}
	}


	private void applyAllocationWeightAdjustedSumToOneHundred(List<ProductOverlayAssetClassReplication> replicationList) {
		if (!CollectionUtils.isEmpty(replicationList)) {
			BigDecimal total = CoreMathUtils.sumProperty(replicationList, ProductOverlayAssetClassReplication::getAllocationWeightAdjusted);
			if (MathUtils.isNullOrZero(total)) {
				boolean nonZero = false;
				for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(replicationList)) {
					if (!MathUtils.isNullOrZero(rep.getAllocationWeight())) {
						nonZero = true;
						break;
					}
				}
				// if all ORIGINAL ARE zero - apply allocationWeight as 100/count
				if (!nonZero) {
					BigDecimal weight = MathUtils.divide(MathUtils.BIG_DECIMAL_ONE_HUNDRED, CollectionUtils.getSize(replicationList));
					for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(replicationList)) {
						rep.setAllocationWeightAdjusted(weight);
					}
				}
			}
			total = CoreMathUtils.sumProperty(replicationList, ProductOverlayAssetClassReplication::getAllocationWeightAdjusted);
			// If Total Is Still 0 and there is more than one replication in the list (Leave Percentages as they are - don't adjust to 100)
			if (!MathUtils.isNullOrZero(total) || replicationList.size() == 1) {
				CoreMathUtils.applySumPropertyDifference(replicationList, ProductOverlayAssetClassReplication::getAllocationWeightAdjusted, ProductOverlayAssetClassReplication::setAllocationWeightAdjusted, MathUtils.BIG_DECIMAL_ONE_HUNDRED, true);
			}
		}
	}


	/**
	 * Replications with a Zero Target - Remove them if no actual either and option not selected on allocation config to keep it
	 * <p>
	 * Dynamic replications can sometimes be built to have a zero target.
	 * If we hold actual positions, need to leave it in the
	 * list so traders know to sell.  If no target and no actual - remove it from the list.
	 * <p>
	 * Or if multiple securities for the allocation, but selected option to sell from first expiring, not current, current may end up with zero target/zero actual
	 */
	private List<ProductOverlayAssetClassReplication> removeZeroTargetAndActualReplications(ProductOverlayRunConfig runConfig) {
		// Duplicated Securities - replicationPositionExcludedFromCount
		List<ProductOverlayAssetClassReplication> positionExcludedRepList = BeanUtils.filter(runConfig.getOverlayAssetClassReplicationList(), overlayReplication -> overlayReplication.getOverlayAssetClass().getAccountAssetClass().isReplicationPositionExcludedFromCount(), true);
		List<ProductOverlayAssetClassReplication> replicationList = BeanUtils.filter(runConfig.getOverlayAssetClassReplicationList(), overlayReplication -> overlayReplication.getOverlayAssetClass().getAccountAssetClass().isReplicationPositionExcludedFromCount(), false);

		Set<String> checkedList = new HashSet<>();

		List<ProductOverlayAssetClassReplication> zeroTargetReplications = BeanUtils.filter(replicationList, replication -> MathUtils.isNullOrZero(replication.getTargetExposureAdjusted()));
		if (!CollectionUtils.isEmpty(zeroTargetReplications) || !CollectionUtils.isEmpty(positionExcludedRepList)) {
			List<ProductOverlayAssetClassReplication> filteredList = new ArrayList<>();
			for (ProductOverlayAssetClassReplication replication : CollectionUtils.getIterable(replicationList)) {
				// If their target is zero, but they do have an actual weight (%), contracts, ccy, exposure, etc. assigned to them, then they need to always be included
				if (PortfolioUtils.isPortfolioReplicationEmpty(replication)) {
					PortfolioReplicationAllocationSecurityConfig allocationConfig = replication.getAllocationConfig();
					if (allocationConfig == null || !allocationConfig.isAlwaysIncludeCurrentSecurity()) {
						continue;
					}
				}
				filteredList.add(replication);
				// See if it's already in the list - if not add it if it fits
				if (!CollectionUtils.isEmpty(positionExcludedRepList)) {
					List<ProductOverlayAssetClassReplication> positionExcludedFilteredList = BeanUtils.filter(positionExcludedRepList, overlayReplication -> overlayReplication.getSecurity().getId(), replication.getSecurity().getId());

					if (CollectionUtils.isEmpty(positionExcludedFilteredList)) {
						for (ProductOverlayAssetClassReplication positionExcludedReplication : positionExcludedRepList) {
							String key = positionExcludedReplication.getOverlayAssetClass().getAccountAssetClass().getId() + "_" + positionExcludedReplication.getAllocationConfig().getReplicationAllocation().getId() + "_" + replication.getSecurity().getId();
							if (checkedList.contains(key)) {
								continue;
							}
							// If valid for allocation - add it
							if (getInvestmentReplicationService().isInvestmentReplicationAllocationSecurityValid(positionExcludedReplication.getAllocationConfig().getReplicationAllocation(), runConfig.getBalanceDate(), replication.getSecurity())) {
								ProductOverlayAssetClassReplication newRep = createProductOverlayAssetClassReplication(positionExcludedReplication.getOverlayAssetClass(), positionExcludedReplication.getAllocationConfig(), BigDecimal.ZERO,
										BigDecimal.ZERO, positionExcludedReplication.isMatchingAllocation());
								getProductReplicationService().setupProductOverlayAssetClassReplicationSecurityInfo(newRep, replication.getSecurity(), runConfig.getExchangeRateDataSourceName(),
										runConfig.isExcludeMispricing());
								checkedList.add(key);
								filteredList.add(newRep);
							}
						}
					}
					else {
						// Otherwise - add the matching ones found back into the final list
						for (ProductOverlayAssetClassReplication positionExcludedReplication : positionExcludedFilteredList) {
							String key = positionExcludedReplication.getOverlayAssetClass().getAccountAssetClass().getId() + "_" + positionExcludedReplication.getAllocationConfig().getReplicationAllocation().getId() + "_" + positionExcludedReplication.getSecurity().getId();
							if (checkedList.add(key)) {
								filteredList.add(positionExcludedReplication);
							}
						}
					}
				}
			}
			return filteredList;
		}
		return replicationList;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods                ///////////
	////////////////////////////////////////////////////////////////////////////////


	public ProductReplicationService getProductReplicationService() {
		return this.productReplicationService;
	}


	public void setProductReplicationService(ProductReplicationService productReplicationService) {
		this.productReplicationService = productReplicationService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public PortfolioReplicationHandler getPortfolioReplicationHandler() {
		return this.portfolioReplicationHandler;
	}


	public void setPortfolioReplicationHandler(PortfolioReplicationHandler portfolioReplicationHandler) {
		this.portfolioReplicationHandler = portfolioReplicationHandler;
	}
}
