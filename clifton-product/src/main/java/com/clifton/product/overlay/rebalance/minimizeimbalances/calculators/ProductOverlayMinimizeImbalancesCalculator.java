package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import com.clifton.product.overlay.rebalance.calculators.ProductOverlayRebalanceCalculator;


/**
 * The <code>ProductOverlayMinimizeImbalancesCalculator</code> ...
 *
 * @author manderson
 */
public interface ProductOverlayMinimizeImbalancesCalculator extends ProductOverlayRebalanceCalculator {

	/**
	 * If returns a column group name - this is the group where analysts
	 * can apply additional custom column values for each asset class that is used
	 * by this calculator
	 */
	public String getCustomColumnGroupName();


	public boolean calculate(ProductOverlayRunConfig config);
}
