package com.clifton.product.overlay.rebalance;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalance;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceAssetClassHistory;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceHistory;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceService;
import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import com.clifton.product.overlay.rebalance.calculators.ProductOverlayRebalanceCalculator;
import com.clifton.product.overlay.rebalance.calculators.ProductOverlayRebalanceCalculatorLocator;
import com.clifton.product.overlay.rebalance.minimizeimbalances.ProductOverlayMinimizeImbalancesSettings;
import com.clifton.product.overlay.rebalance.minimizeimbalances.calculators.ProductOverlayMinimizeImbalancesCalculator;
import com.clifton.product.overlay.search.ProductOverlayAssetClassSearchForm;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>ProductOverlayRebalanceServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class ProductOverlayRebalanceServiceImpl implements ProductOverlayRebalanceService {

	private InvestmentAccountAssetClassService investmentAccountAssetClassService;
	private InvestmentAccountRebalanceService investmentAccountRebalanceService;
	private InvestmentAccountService investmentAccountService;

	private PortfolioRunService portfolioRunService;
	private ProductOverlayService productOverlayService;
	private ProductOverlayRebalanceCalculatorLocator productOverlayRebalanceCalculatorLocator;

	private SystemBeanService systemBeanService;
	private SystemColumnService systemColumnService;
	private SystemColumnValueHandler systemColumnValueHandler;


	///////////////////////////////////////////////////////////
	////////     Product Overlay Rebalance Methods    /////////
	///////////////////////////////////////////////////////////


	@Override
	public ProductOverlayRebalance getProductOverlayRebalance(int overlayRunId) {
		ProductOverlayRebalance bean = new ProductOverlayRebalance();
		bean.setOverlayRunId(overlayRunId);
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(overlayRunId);
		if (run == null) {
			throw new ValidationException("Cannot find specified PIOS run with ID [" + overlayRunId + "]");
		}
		bean.setOverlayRun(getPortfolioRunService().getPortfolioRun(overlayRunId));
		bean.setRebalanceDate(run.getBalanceDate());
		ProductOverlayAssetClassSearchForm searchForm = new ProductOverlayAssetClassSearchForm();
		searchForm.setRunId(overlayRunId);
		searchForm.setRollupAssetClass(false);
		bean.setRebalanceAssetClassList(getProductOverlayService().getProductOverlayAssetClassList(searchForm, false));
		bean.setRebalanceConfig(getInvestmentAccountRebalanceService().getInvestmentAccountRebalance(run.getClientInvestmentAccount().getId()));
		bean.setLastRebalance(getInvestmentAccountRebalanceService().getInvestmentAccountRebalanceHistoryRecent(run.getClientInvestmentAccount().getId(), new Date()));
		return bean;
	}


	private ProductOverlayRebalance bindProductOverlayRebalanceBean(ProductOverlayRebalance bean) {
		// Get Full Populated bean again
		ProductOverlayRebalance original = getProductOverlayRebalance(bean.getOverlayRunId());
		bean.setOverlayRun(original.getOverlayRun());
		bean.setLastRebalance(original.getLastRebalance());

		ProductOverlayAssetClassSearchForm searchForm = new ProductOverlayAssetClassSearchForm();
		searchForm.setRunId(bean.getOverlayRunId());
		searchForm.setRollupAssetClass(true);
		bean.setRollupAssetClassList(getProductOverlayService().getProductOverlayAssetClassList(searchForm, false));

		// Setup Rebalance object in case we need to update it
		InvestmentAccountRebalance originalConfig = original.getRebalanceConfig();
		bean.getRebalanceConfig().setId(bean.getOverlayRun().getClientInvestmentAccount().getId());
		if (originalConfig != null) {
			// Copy new properties to original bean and set that bean on the rebalance bean
			BeanUtils.copyProperties(bean.getRebalanceConfig(), originalConfig, new String[]{"minEffectiveCashExposure", "maxEffectiveCashExposure", "minimizeImbalancesCalculatorBean",
					"minimizeImbalancesAdjustmentAmount", "minimizeImbalancesAdjustForCashTarget"});
			bean.setRebalanceConfig(originalConfig);
		}

		return bean;
	}


	@Override
	public void saveProductOverlayRebalance(ProductOverlayRebalance bean) {
		// Manual Binding
		bean = bindProductOverlayRebalanceBean(bean);
		ProductOverlayRebalanceActions action = bean.getAction();
		if (action == null) {
			throw new ValidationException("Missing required rebalance action");
		}

		if (action.isSaveConfig()) {
			getInvestmentAccountRebalanceService().saveInvestmentAccountRebalance(bean.getRebalanceConfig());
		}
		if (action.isProcess()) {
			if (action.isMiniRebalance()) {
				processProductOverlayRebalanceCalculation(bean, false);
			}
			else if (action.getUseCalculationType() != null) {
				if (RebalanceCalculationTypes.MANUAL == action.getUseCalculationType()) {
					saveProductOverlayRebalanceImpl(bean, action.getUseCalculationType(), false);
				}
				else {
					processProductOverlayRebalanceCalculationImpl(bean, action.getUseCalculationType(), false, "Unable to perform full rebalance on run [" + bean.getOverlayRun().getLabel()
							+ "] because ");
				}
			}
		}
	}


	private void saveProductOverlayRebalanceImpl(ProductOverlayRebalance bean, RebalanceCalculationTypes calculationType, boolean automatic) {
		// Create New History Record
		InvestmentAccountRebalanceHistory history = new InvestmentAccountRebalanceHistory();
		if (automatic) {
			history.setDescription("Automatic Rebalancing");
		}
		history.setInvestmentAccount(bean.getOverlayRun().getClientInvestmentAccount());
		history.setRebalanceCalculationType(calculationType);
		history.setRebalanceDate(bean.getRebalanceDate());

		List<InvestmentAccountRebalanceAssetClassHistory> acList = new ArrayList<>();
		for (ProductOverlayAssetClass portfolioAssetClass : CollectionUtils.getIterable(bean.getRebalanceAssetClassList())) {
			InvestmentAccountRebalanceAssetClassHistory acBean = new InvestmentAccountRebalanceAssetClassHistory();
			acBean.setAccountAssetClass(portfolioAssetClass.getAccountAssetClass());
			acBean.setBeforeRebalanceCash(portfolioAssetClass.getRebalanceCashAdjusted() != null ? MathUtils.round(portfolioAssetClass.getRebalanceCashAdjusted(), 2) : BigDecimal.ZERO);
			acBean.setAfterRebalanceCash(portfolioAssetClass.getNewRebalanceCash() != null ? MathUtils.round(portfolioAssetClass.getNewRebalanceCash(), 2) : BigDecimal.ZERO);
			acList.add(acBean);
		}
		history.setAssetClassHistoryList(acList);
		getInvestmentAccountRebalanceService().saveInvestmentAccountRebalanceHistory(history);
	}


	private void processProductOverlayRebalanceCalculation(ProductOverlayRebalance bean, boolean automatic) {
		String msgPrefix = "Unable to perform mini rebalance on run [" + bean.getOverlayRun().getLabel() + "] because ";

		InvestmentAccountRebalance config = bean.getRebalanceConfig();
		InvestmentAccountRebalanceHistory lastRebalance = bean.getLastRebalance();

		if (lastRebalance != null && DateUtils.compare(lastRebalance.getRebalanceDate(), bean.getOverlayRun().getBalanceDate(), false) > 0) {
			if (automatic) {
				return;
			}
			throw new ValidationException(msgPrefix + " there is rebalance history (Latest: " + lastRebalance.getLabel() + ") after the run's balance date.");
		}

		// Automatic option not selected on the account, or no thresholds defined
		if (automatic && (config == null || !config.isAutomatic() || (config.getMinAllowedCash() == null && config.getMaxAllowedCash() == null))) {
			return;
		}

		List<ProductOverlayAssetClass> assetClassList = bean.getRebalanceAssetClassList();
		BigDecimal totalRebalCashAdj = CoreMathUtils.sumProperty(assetClassList, ProductOverlayAssetClass::getRebalanceCashAdjusted);
		if (MathUtils.isNullOrZero(totalRebalCashAdj)) {
			if (automatic) {
				return;
			}
			throw new ValidationException(msgPrefix + " the total rebalance cash adjusted values is currently zero - nothing to rebalance.");
		}

		// Only perform automatic if thresholds are crossed
		if (automatic) {
			if (!MathUtils.isLessThan(totalRebalCashAdj, config.getMinAllowedCash()) && !MathUtils.isGreaterThan(totalRebalCashAdj, config.getMaxAllowedCash())) {
				return;
			}
		}
		processProductOverlayRebalanceCalculationImpl(bean, config.getCalculationType(), automatic, msgPrefix);
	}


	private void processProductOverlayRebalanceCalculationImpl(ProductOverlayRebalance bean, RebalanceCalculationTypes calculationType, boolean automatic, String msgPrefix) {
		if (calculationType == null) {
			throw new ValidationException(msgPrefix + " there is no calculation type selected for the account.");
		}

		ProductOverlayRebalanceCalculator calculator = null;

		// If calculation type can be overridden by Minimize Imbalances calculator and there is a Minimize Imbalances calculator - use that
		if (calculationType.isOverriddenByMinimizeImbalances()) {
			calculator = getProductOverlayMinimizeImbalancesCalculator(bean.getRebalanceConfig());
		}

		if (calculator == null) {
			// If no override - use default
			calculator = getProductOverlayRebalanceCalculatorLocator().locate(calculationType);
		}

		// Calculate new values
		calculator.calculateNewRebalanceCash(bean);
		saveProductOverlayRebalanceImpl(bean, calculationType, automatic);
	}


	@Override
	public void processProductOverlayRebalanceAutomatic(ProductOverlayRunConfig runConfig) {
		ProductOverlayRebalance bean = new ProductOverlayRebalance();
		bean.setOverlayRunId(runConfig.getRun().getId());
		bean.setOverlayRun(runConfig.getRun());
		bean.setRebalanceDate(runConfig.getBalanceDate());

		bean.setRebalanceAssetClassList(runConfig.getOverlayAssetClassListExcludeRollups());
		bean.setRollupAssetClassList(runConfig.getOverlayAssetClassRollupList());
		bean.setRebalanceConfig(runConfig.getAccountRebalanceConfig());
		bean.setLastRebalance(getInvestmentAccountRebalanceService().getInvestmentAccountRebalanceHistoryRecent(runConfig.getClientInvestmentAccountId(), new Date()));

		processProductOverlayRebalanceCalculation(bean, true);
	}


	///////////////////////////////////////////////////////////
	/////   Product Overlay Minimize Imbalances Methods   /////
	///////////////////////////////////////////////////////////


	@Override
	public String getProductOverlayMinimizeImbalancesSettingsColumnGroupName(int investmentAccountId) {
		InvestmentAccountRebalance accountRebal = getInvestmentAccountRebalanceService().getInvestmentAccountRebalance(investmentAccountId);
		ProductOverlayMinimizeImbalancesCalculator calculator = getProductOverlayMinimizeImbalancesCalculator(accountRebal);
		if (calculator != null) {
			return calculator.getCustomColumnGroupName();
		}
		return null;
	}


	@Override
	public ProductOverlayMinimizeImbalancesSettings getProductOverlayMinimizeImbalancesSettings(int investmentAccountId) {
		InvestmentAccountRebalance accountRebal = getInvestmentAccountRebalanceService().getInvestmentAccountRebalance(investmentAccountId);
		ProductOverlayMinimizeImbalancesCalculator calculator = getProductOverlayMinimizeImbalancesCalculator(accountRebal);
		if (calculator == null) {
			throw new ValidationException("Minimize Imbalances Calculator Bean is not selected. Unable to enter advanced settings without a calculator selected.");
		}

		String columnGroupName = calculator.getCustomColumnGroupName();
		if (StringUtils.isEmpty(columnGroupName)) {
			throw new ValidationException("Advanced settings are not supported for Minimize Imbalances Calculator Bean [" + accountRebal.getMinimizeImbalancesCalculatorBean().getName() + "]");
		}

		ProductOverlayMinimizeImbalancesSettings settings = new ProductOverlayMinimizeImbalancesSettings();
		settings.setInvestmentAccount(getInvestmentAccountService().getInvestmentAccount(investmentAccountId));
		settings.setColumnGroupName(columnGroupName);

		List<InvestmentAccountAssetClass> accountAssetClassList = getInvestmentAccountAssetClassService().getInvestmentAccountAssetClassListByAccount(investmentAccountId);
		if (CollectionUtils.isEmpty(accountAssetClassList)) {
			throw new ValidationException("Account [" + settings.getInvestmentAccount().getLabel() + "] does not have any asset classes set up to apply settings to.");
		}

		for (InvestmentAccountAssetClass iac : accountAssetClassList) {
			iac.setColumnGroupName(columnGroupName);
			iac.setColumnValueList(getSystemColumnValueHandler().getSystemColumnValueListForEntity(iac));
			if (settings.getColumnList() == null) {
				settings.setColumnList(getSystemColumnService().getSystemColumnCustomListForEntity(iac));
			}
		}
		settings.setAccountAssetClassList(accountAssetClassList);
		return settings;
	}


	@Override
	public boolean processProductOverlayMinimizeImbalances(ProductOverlayRunConfig runConfig) {
		InvestmentAccountRebalance accountRebal = getInvestmentAccountRebalanceService().getInvestmentAccountRebalance(runConfig.getRun().getClientInvestmentAccount().getId());
		ProductOverlayMinimizeImbalancesCalculator calculator = getProductOverlayMinimizeImbalancesCalculator(accountRebal);
		if (calculator != null) {
			return calculator.calculate(runConfig);
		}
		return false;
	}


	private ProductOverlayMinimizeImbalancesCalculator getProductOverlayMinimizeImbalancesCalculator(InvestmentAccountRebalance accountRebalance) {
		if (accountRebalance != null && accountRebalance.getMinimizeImbalancesCalculatorBean() != null) {
			return (ProductOverlayMinimizeImbalancesCalculator) getSystemBeanService().getBeanInstance(accountRebalance.getMinimizeImbalancesCalculatorBean());
		}
		return null;
	}


	///////////////////////////////////////////////////////////
	///////////         Getters & Setters          ////////////
	///////////////////////////////////////////////////////////


	public InvestmentAccountRebalanceService getInvestmentAccountRebalanceService() {
		return this.investmentAccountRebalanceService;
	}


	public void setInvestmentAccountRebalanceService(InvestmentAccountRebalanceService investmentAccountRebalanceService) {
		this.investmentAccountRebalanceService = investmentAccountRebalanceService;
	}


	public ProductOverlayRebalanceCalculatorLocator getProductOverlayRebalanceCalculatorLocator() {
		return this.productOverlayRebalanceCalculatorLocator;
	}


	public void setProductOverlayRebalanceCalculatorLocator(ProductOverlayRebalanceCalculatorLocator productOverlayRebalanceCalculatorLocator) {
		this.productOverlayRebalanceCalculatorLocator = productOverlayRebalanceCalculatorLocator;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}
}
