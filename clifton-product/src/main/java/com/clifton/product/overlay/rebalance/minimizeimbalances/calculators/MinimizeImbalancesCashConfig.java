package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>MinimizeImbalancesCashConfig</code> class is populated when processing managers whose cash should be applied
 * to minimize imbalances.  Stores the amount of cash available in each bucket so we can easily get the total
 * and then when processing minimize imbalances allocate the cash back in the correct asset class and the correct percentage in each cash bucket
 *
 * @author manderson
 */
public class MinimizeImbalancesCashConfig {

	private BigDecimal managerCash = BigDecimal.ZERO;
	private BigDecimal fundCash = BigDecimal.ZERO;
	private BigDecimal transitionCash = BigDecimal.ZERO;

	// For attribution, each asset class retains it's own manager cash + fund cash percentage of manager cash in un-investable asset classes
	private Map<InvestmentAccountAssetClass, BigDecimal> managerCashMap = new HashMap<>();
	// For attribution, each asset class retains it's own transition cash + fund cash percentage of transition cash in un-investable asset classes
	private Map<InvestmentAccountAssetClass, BigDecimal> transitionCashMap = new HashMap<>();

	private StringBuilder resultMessage = null;


	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	public void appendLineToResultMessage(String msg) {
		if (this.resultMessage == null) {
			this.resultMessage = new StringBuilder(msg);
		}
		else {
			this.resultMessage.append(msg);
		}
		this.resultMessage.append(StringUtils.NEW_LINE);
	}


	public String getResultMessage() {
		if (this.resultMessage == null) {
			return null;
		}
		return this.resultMessage.toString();
	}


	public BigDecimal getCashTotal() {
		BigDecimal total = getManagerCash();
		total = MathUtils.add(total, getFundCash());
		total = MathUtils.add(total, getTransitionCash());
		return MathUtils.round(total, 2);
	}


	/**
	 * If total Fund Cash is Negative will zero out Fund Cash and add as an overall adjustment
	 * If then total Manager Cash is Negative will zero out Manager Cash and add as an overall adjustment
	 * If then Transition Cash is Negative will zero out Transition Cash and add as an overall adjustment
	 * Finally any negative cash will in the end be moved to rebalance attribution cash (Not done here, but during attribution rebalancing after the calculation is completed)
	 */
	public void applyAttributionAdjustment() {
		if (MathUtils.isNegative(getFundCash())) {
			BigDecimal adjustment = getFundCash();
			setFundCash(BigDecimal.ZERO);
			applyAdjustment(adjustment);
		}
		if (MathUtils.isNegative(getManagerCash())) {
			BigDecimal adjustment = getManagerCash();
			this.managerCashMap = new HashMap<>();
			setManagerCash(BigDecimal.ZERO);
			applyAdjustment(adjustment);
		}
		if (MathUtils.isNegative(getTransitionCash())) {
			BigDecimal adjustment = getTransitionCash();
			this.transitionCashMap = new HashMap<>();
			setTransitionCash(BigDecimal.ZERO);
			applyAdjustment(adjustment);
		}
	}


	/**
	 * Applies a cash adjustment: Applies to Fund Cash First, then Manager Cash, then Transition Cash, then Rebalance Cash
	 */
	public void applyAdjustment(BigDecimal adjustment) {
		appendLineToResultMessage("Cash Available for Minimize Imbalances: " + CoreMathUtils.formatNumberMoney(getCashTotal()));
		if (!MathUtils.isNullOrZero(adjustment)) {
			appendLineToResultMessage("Cash Adjustment: " + CoreMathUtils.formatNumberMoney(adjustment));
			// Don't think we can have a positive adjustment, but if so, just add to Fund Cash
			if (!MathUtils.isNegative(adjustment)) {
				setFundCash(MathUtils.add(getFundCash(), adjustment));
			}
			else {
				if (MathUtils.isLessThanOrEqual(MathUtils.abs(adjustment), getFundCash())) {
					setFundCash(MathUtils.add(getFundCash(), adjustment));
				}
				else {
					adjustment = MathUtils.add(getFundCash(), adjustment);
					setFundCash(BigDecimal.ZERO);

					if (MathUtils.isLessThan(MathUtils.abs(adjustment), getManagerCash())) {
						BigDecimal percentage = CoreMathUtils.getPercentValue(adjustment, getManagerCash());
						applyAdjustmentProportionally(percentage, this.managerCashMap);
						setManagerCash(MathUtils.add(getManagerCash(), adjustment));
					}
					else {
						adjustment = MathUtils.add(getManagerCash(), adjustment);
						this.managerCashMap = new HashMap<>();
						setManagerCash(BigDecimal.ZERO);

						if (MathUtils.isLessThan(MathUtils.abs(adjustment), getTransitionCash())) {
							BigDecimal percentage = CoreMathUtils.getPercentValue(adjustment, getTransitionCash());
							applyAdjustmentProportionally(percentage, this.transitionCashMap);
							setTransitionCash(MathUtils.add(getTransitionCash(), adjustment));
						}
						else {
							adjustment = MathUtils.add(getTransitionCash(), adjustment);
							this.transitionCashMap = new HashMap<>();
							setTransitionCash(BigDecimal.ZERO);

							// If we still have negative cash left over, put it into fund cash - attribution will move it to rebalance cash using fund cash weights because we can't have negative Fund Cash
							setFundCash(adjustment);
						}
					}
				}
			}
			appendLineToResultMessage("Cash To Apply: " + CoreMathUtils.formatNumberMoney(getCashTotal()));
		}
	}


	private void applyAdjustmentProportionally(BigDecimal percentage, Map<InvestmentAccountAssetClass, BigDecimal> cashMap) {
		for (Map.Entry<InvestmentAccountAssetClass, BigDecimal> entry : cashMap.entrySet()) {
			BigDecimal assetClassCash = entry.getValue();
			cashMap.put(entry.getKey(), MathUtils.add(assetClassCash, MathUtils.round(MathUtils.getPercentageOf(percentage, assetClassCash), 2)));
		}
	}


	public void addCashToBucket(InvestmentAccountAssetClass investmentAccountAssetClass, ManagerCashTypes cashType, BigDecimal amount) {
		if (ManagerCashTypes.MANAGER == cashType) {
			setManagerCash(MathUtils.add(getManagerCash(), amount));
			if (investmentAccountAssetClass != null) {
				BigDecimal assetClassManagerCash = MathUtils.add(this.managerCashMap.get(investmentAccountAssetClass), amount);
				this.managerCashMap.put(investmentAccountAssetClass, assetClassManagerCash);
			}
		}
		else if (ManagerCashTypes.FUND == cashType) {
			setFundCash(MathUtils.add(getFundCash(), amount));
		}
		else if (ManagerCashTypes.TRANSITION == cashType) {
			setTransitionCash(MathUtils.add(getTransitionCash(), amount));
			if (investmentAccountAssetClass != null) {
				BigDecimal assetClassTransitionCash = MathUtils.add(this.transitionCashMap.get(investmentAccountAssetClass), amount);
				this.transitionCashMap.put(investmentAccountAssetClass, assetClassTransitionCash);
			}
		}
		else {
			throw new ValidationException("Unable to allocate cash for minimize imbalances - Unsupported Cash Type [" + cashType + "].");
		}
	}


	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	public BigDecimal getManagerCash() {
		return this.managerCash;
	}


	private void setManagerCash(BigDecimal managerCash) {
		this.managerCash = managerCash;
	}


	public BigDecimal getFundCash() {
		return this.fundCash;
	}


	private void setFundCash(BigDecimal fundCash) {
		this.fundCash = fundCash;
	}


	public BigDecimal getTransitionCash() {
		return this.transitionCash;
	}


	private void setTransitionCash(BigDecimal transitionCash) {
		this.transitionCash = transitionCash;
	}


	public Map<InvestmentAccountAssetClass, BigDecimal> getManagerCashMap() {
		return this.managerCashMap;
	}


	public Map<InvestmentAccountAssetClass, BigDecimal> getTransitionCashMap() {
		return this.transitionCashMap;
	}
}
