package com.clifton.product.overlay.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.portfolio.run.manager.search.BasePortfolioRunManagerAccountSearchForm;


/**
 * The <code>ProductOverlayManagerAccountSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class ProductOverlayManagerAccountSearchForm extends BasePortfolioRunManagerAccountSearchForm {

	@SearchField(searchField = "accountAssetClass.id")
	private Integer accountAssetClassId;

	@SearchField(searchFieldPath = "accountAssetClass", searchField = "displayName,assetClass.name")
	private String assetClassName;

	@SearchField
	private Boolean rollupAssetClass;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getAccountAssetClassId() {
		return this.accountAssetClassId;
	}


	public void setAccountAssetClassId(Integer accountAssetClassId) {
		this.accountAssetClassId = accountAssetClassId;
	}


	public String getAssetClassName() {
		return this.assetClassName;
	}


	public void setAssetClassName(String assetClassName) {
		this.assetClassName = assetClassName;
	}


	public Boolean getRollupAssetClass() {
		return this.rollupAssetClass;
	}


	public void setRollupAssetClass(Boolean rollupAssetClass) {
		this.rollupAssetClass = rollupAssetClass;
	}
}
