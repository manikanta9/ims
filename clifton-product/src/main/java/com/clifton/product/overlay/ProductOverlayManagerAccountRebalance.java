package com.clifton.product.overlay;


import com.clifton.investment.manager.InvestmentManagerAccountRebalance;
import com.clifton.portfolio.run.PortfolioRun;


/**
 * The <code>ProductOverlayManagerAccountRebalance</code> ...
 *
 * @author Mary Anderson
 */
public class ProductOverlayManagerAccountRebalance extends InvestmentManagerAccountRebalance {

	private PortfolioRun overlayRun;


	public PortfolioRun getOverlayRun() {
		return this.overlayRun;
	}


	public void setOverlayRun(PortfolioRun overlayRun) {
		this.overlayRun = overlayRun;
	}
}
