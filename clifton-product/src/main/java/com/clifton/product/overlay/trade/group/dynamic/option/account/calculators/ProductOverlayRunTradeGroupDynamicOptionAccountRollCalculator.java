package com.clifton.product.overlay.trade.group.dynamic.option.account.calculators;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionTradeCommand;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionTradeReplicationHolder;

import java.util.List;


/**
 * @author manderson
 */
public interface ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator {

	/**
	 * Creates and populates a {@link ProductOverlayRunOptionTradeReplicationHolder} for the provided
	 * client investment account and list of details.
	 */
	public ProductOverlayRunOptionTradeReplicationHolder getProductOverlayRunOptionTradeReplicationHolder(ProductOverlayRunOptionTradeCommand rollCommand, InvestmentAccount clientAccount, List<ProductOverlayAssetClassReplication> detailList);


	/**
	 * Populates the {@link ProductOverlayRunOptionTradeReplicationHolder} for the provided list of details.
	 * The client investment account is taken from the provided account roll.
	 */
	public ProductOverlayRunOptionTradeReplicationHolder populateProductOverlayRunOptionTradeReplicationHolder(ProductOverlayRunOptionTradeCommand rollCommand, ProductOverlayRunOptionTradeReplicationHolder accountRoll, List<ProductOverlayAssetClassReplication> detailList);
}
