package com.clifton.product.overlay.trade.group.dynamic.option.account.calculators;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.replication.calculators.InvestmentReplicationCurrencyTypes;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.updater.MarketDataFieldValueUpdaterCommand;
import com.clifton.marketdata.updater.MarketDataUpdaterService;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.overlay.search.ProductOverlayAssetClassSearchForm;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionReplicationHolder;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionTradeCommand;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionTradeReplicationHolder;
import com.clifton.product.replication.ProductReplicationService;
import com.clifton.product.util.ProductUtilService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
public abstract class BaseProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator implements ProductOverlayRunTradeGroupDynamicOptionAccountRollCalculator {

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	private MarketDataRetriever marketDataRetriever;
	private MarketDataUpdaterService marketDataUpdaterService;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;

	private ProductReplicationService productReplicationService;
	private ProductOverlayService productOverlayService;
	private ProductUtilService productUtilService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Label overrides can be customized for each calculator to display what is included
	 * i.e. additional exposure could be cash, or Equities Physical Exposure, or x% of Total Portfolio Value
	 */
	private String putMatchingReplicationLabel;
	private String putMatchingAdditionalExposureLabel;
	private String callMatchingReplicationLabel;
	private String callMatchingAdditionalExposureLabel;

	/**
	 * Can be used to pull the manager balance for the selected manager
	 * add add their balance to the call or put matching exposure values.
	 * Example: Guide stone (297150) has equities managed outside of IMS, so a manager balance is used to report on that balance
	 * Business Client id is used to filter the available manager accounts to make the selection easier in UI
	 */
	private Integer businessClientId;
	private Integer putMatchingManagerAccountId;
	private Integer callMatchingManagerAccountId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public ProductOverlayRunOptionTradeReplicationHolder getProductOverlayRunOptionTradeReplicationHolder(ProductOverlayRunOptionTradeCommand tradeCommand, InvestmentAccount clientAccount, List<ProductOverlayAssetClassReplication> detailList) {
		ProductOverlayRunOptionTradeReplicationHolder replicationHolder = new ProductOverlayRunOptionTradeReplicationHolder();
		replicationHolder.setClientAccount(clientAccount);
		replicationHolder.setDoNotRemoveExpiringOptionsFromExcessCalculation(tradeCommand.getDoNotRemoveExpiringOptionsFromExcessCalculation());
		replicationHolder.setLongPositions(tradeCommand.getLongPositions());

		populateProductOverlayRunTradeGroupDynamicOptionAccountRollWithDetailList(tradeCommand, replicationHolder, detailList);
		return replicationHolder;
	}


	@Override
	public ProductOverlayRunOptionTradeReplicationHolder populateProductOverlayRunOptionTradeReplicationHolder(ProductOverlayRunOptionTradeCommand tradeCommand, ProductOverlayRunOptionTradeReplicationHolder replicationHolder, List<ProductOverlayAssetClassReplication> detailList) {
		ValidationUtils.assertNotNull(replicationHolder.getClientAccount(), "Client Investment Account is not present.");
		replicationHolder.setDoNotRemoveExpiringOptionsFromExcessCalculation(tradeCommand.getDoNotRemoveExpiringOptionsFromExcessCalculation());
		replicationHolder.setLongPositions(tradeCommand.getLongPositions());

		populateProductOverlayRunTradeGroupDynamicOptionAccountRollWithDetailList(tradeCommand, replicationHolder, detailList);
		return replicationHolder;
	}


	/**
	 * Can be used when we need to include exposure from replications to call or put matching
	 */
	public abstract void applyReplicationToMatchingExposure(ProductOverlayRunOptionTradeCommand tradeCommand, ProductOverlayRunOptionTradeReplicationHolder replicationHolder, ProductOverlayAssetClassReplication replication);


	/**
	 * Can be used to set matching exposure to something additional than the replications, i.e. add cash exposure or a different value altogether (i.e. 50 of Total Portfolio Value)
	 */
	public abstract void applyAdditionalMatchingExposure(ProductOverlayRunOptionTradeReplicationHolder replicationHolder, PortfolioRun portfolioRun);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void populateProductOverlayRunTradeGroupDynamicOptionAccountRollWithDetailList(ProductOverlayRunOptionTradeCommand tradeCommand, ProductOverlayRunOptionTradeReplicationHolder replicationHolder, List<ProductOverlayAssetClassReplication> detailList) {
		PortfolioRun portfolioRun = null;
		for (ProductOverlayAssetClassReplication replication : CollectionUtils.getIterable(detailList)) {
			// Don't want to double count options - we show Call/Put Notional separate from one asset class that has all Options with their Market Value
			if (!replication.getOverlayAssetClass().getAccountAssetClass().isReplicationPositionExcludedFromCount()) {
				if (portfolioRun == null) {
					portfolioRun = replication.getOverlayAssetClass().getOverlayRun();
					replicationHolder.setPortfolioRun(portfolioRun);
				}

				// refresh price to be live price if requested.
				refreshProductOverlayReplicationPrice(tradeCommand, replication);

				InvestmentSecurity security = replication.getSecurity();
				if (InvestmentUtils.isInstrumentOfType(security.getInstrument(), InvestmentType.OPTIONS)) {
					InvestmentSecurityOptionTypes optionType = security.getOptionType();
					// Always use original quantity so if we close out the position we include it as short/long based on what was held previously
					addReplicationToReplicationHolder(tradeCommand, replicationHolder, replication, optionType, replication.getActualContractsAdjusted());
				}
				applyReplicationToMatchingExposure(tradeCommand, replicationHolder, replication);
			}
		}

		applyAdditionalMatchingExposure(replicationHolder, portfolioRun);
		applyMatchingAdditionalExposureFromManagerBalances(replicationHolder, portfolioRun);
		applyMatchingExposureLabels(replicationHolder);

		// Note: Need to Add In Any Additional Options Positions Traded that Are Not Included On the Run
		PortfolioAccountContractStore current = getPortfolioAccountDataRetriever().getPortfolioAccountContractStoreActual(replicationHolder.getClientAccount(), getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(tradeCommand.getBalanceDate()), 1), true);
		PortfolioAccountContractStore pending = getPortfolioAccountDataRetriever().getPortfolioAccountContractStorePending(current, replicationHolder.getClientAccount(), null);

		// Apply missing Call replications from a template
		if (!replicationHolder.getCallReplicationHolder().isEmpty()) {
			// All Calls should be calculated the same way - so use the first replication as a template
			ProductOverlayAssetClassReplication templateReplication = CollectionUtils.getFirstElement(replicationHolder.getCallReplicationHolder().getReplicationList());
			// If We have a new Security - Clone one of the existing replications, and calculate for the new contract value
			if (tradeCommand.getCallSecurity() != null) {
				ProductOverlayAssetClassReplication newCallRep = calculateNewReplication(templateReplication, tradeCommand.getCallSecurity());
				replicationHolder.setCallContractValue(newCallRep.getValue());
				replicationHolder.setCallPrice(newCallRep.getSecurityPrice());
				replicationHolder.setCallPriceMultiplier(newCallRep.getSecurity().getPriceMultiplier());
				if (tradeCommand.isCalculateNewTradeQuantities()) {
					replicationHolder.setCallTradeQuantity(calculateNewTradeDefaultQuantity(tradeCommand, replicationHolder, InvestmentSecurityOptionTypes.CALL));
				}
			}
			applyMissingOptionsPositions(InvestmentSecurityOptionTypes.CALL, templateReplication, tradeCommand, replicationHolder, current, pending);
		}

		// Apply missing Put replications from a template
		if (!replicationHolder.getPutReplicationHolder().isEmpty()) {
			// All Puts should be calculated the same way - so use the first replication as a template
			ProductOverlayAssetClassReplication templateReplication = CollectionUtils.getFirstElement(replicationHolder.getPutReplicationHolder().getReplicationList());
			// If We have a new Security - Clone one of the existing replications, and calculate for the new contract value
			if (tradeCommand.getPutSecurity() != null) {
				ProductOverlayAssetClassReplication newPutRep = calculateNewReplication(templateReplication, tradeCommand.getPutSecurity());
				replicationHolder.setPutContractValue(newPutRep.getValue());
				replicationHolder.setPutPrice(newPutRep.getSecurityPrice());
				replicationHolder.setPutPriceMultiplier(newPutRep.getSecurity().getPriceMultiplier());
				if (tradeCommand.isCalculateNewTradeQuantities()) {
					replicationHolder.setPutTradeQuantity(calculateNewTradeDefaultQuantity(tradeCommand, replicationHolder, InvestmentSecurityOptionTypes.PUT));
				}
			}
			applyMissingOptionsPositions(InvestmentSecurityOptionTypes.PUT, templateReplication, tradeCommand, replicationHolder, current, pending);
		}

		if (tradeCommand.isCalculateTrancheTradeAverage()) {
			replicationHolder.setPutTrancheTradeAverage(calculateTrancheAverage(tradeCommand, replicationHolder, true));
			replicationHolder.setCallTrancheTradeAverage(calculateTrancheAverage(tradeCommand, replicationHolder, false));
		}

		if (BooleanUtils.isTrue(tradeCommand.getCalculateCashBalance())) {
			Optional.ofNullable(portfolioRun).ifPresent(pRun -> replicationHolder.setCashBalance(getCashBalance(pRun)));
			if (BooleanUtils.isTrue(tradeCommand.getCalculateCashTradeImpact())) {
				populateLivePricesForExpiringAndPendingOptions(tradeCommand, replicationHolder);
			}
		}
	}


	/**
	 * Adds the {@link ProductOverlayAssetClassReplication} to the correct {@link ProductOverlayRunOptionReplicationHolder}
	 * of the {@link ProductOverlayRunOptionTradeReplicationHolder} depending on the replication's security, option type, and quantity
	 *
	 * @param tradeCommand      the {@link ProductOverlayRunOptionTradeCommand} containing trade criteria
	 * @param replicationHolder the {@link ProductOverlayRunOptionTradeReplicationHolder} to add the replication to
	 * @param replication       the {@link ProductOverlayAssetClassReplication} to add
	 * @param optionType        the option type of the replication's Option security
	 * @param quantity          the quantity to use for this replication
	 */
	private void addReplicationToReplicationHolder(ProductOverlayRunOptionTradeCommand tradeCommand, ProductOverlayRunOptionTradeReplicationHolder replicationHolder, ProductOverlayAssetClassReplication replication, InvestmentSecurityOptionTypes optionType, BigDecimal quantity) {
		InvestmentSecurity security = replication.getSecurity();
		if (optionType == InvestmentSecurityOptionTypes.CALL) {
			if (tradeCommand.isSecurityExpiring(security, quantity)) {
				if (tradeCommand.isSecurityApplicableToInstrument(security)) {
					// add replication applicable to the instrument being traded for roll quantity calculations
					replicationHolder.getExpiringCallTradedInstrumentReplicationHolder().add(replication);
				}
				// track all expiring options for portfolios across instruments
				replicationHolder.getExpiringCallReplicationHolder().add(replication);
			}
			else {
				replicationHolder.getCallReplicationHolder().add(replication);
			}
		}
		else {
			if (tradeCommand.isSecurityExpiring(security, quantity)) {
				if (tradeCommand.isSecurityApplicableToInstrument(security)) {
					// add replication applicable to the instrument being traded for roll quantity calculations
					replicationHolder.getExpiringPutTradedInstrumentReplicationHolder().add(replication);
				}
				// track all expiring options for portfolios across instruments
				replicationHolder.getExpiringPutReplicationHolder().add(replication);
			}
			else {
				replicationHolder.getPutReplicationHolder().add(replication);
			}
		}
	}


	private ProductOverlayAssetClassReplication calculateNewReplication(ProductOverlayAssetClassReplication templateReplication, InvestmentSecurity security) {
		// Not sure if this will work correctly or is the most efficient way - may need to enhance ProductReplicationService methods, but essentially
		// for the new Option we want to copy one of the existing option rows, set the new security, and recalculate contract value
		// so we can determine the impact of trading that security on the portfolio
		ProductOverlayAssetClassReplication newRep = BeanUtils.cloneBean(templateReplication, false, false);
		newRep.setId(null);
		newRep.setActualContracts(BigDecimal.ZERO);
		newRep.setVirtualContracts(null);
		newRep.setCurrentContracts(null);
		newRep.setCurrentVirtualContracts(null);
		newRep.setPendingContracts(null);
		newRep.setPendingVirtualContracts(null);
		newRep.setSecurityPrice(
				ObjectUtils.coalesce(
						getSecurityMarketDataPriceForDate(security, templateReplication.getPortfolioRun().getBalanceDate()),
						newRep.getSecurityPrice()));
		getProductReplicationService().setupProductOverlayAssetClassReplicationSecurityInfo(newRep, security, null, false);
		return newRep;
	}


	/**
	 * Calculates Quantity for New Put/Call Trade
	 * Note: Always returns quantity as a positive value.  The longPositions option on the ProductOverlayRunTradeGroupDynamicOptionRoll object determines Buy Long/Sell Short
	 */
	private BigDecimal calculateNewTradeDefaultQuantity(ProductOverlayRunOptionTradeCommand tradeCommand, ProductOverlayRunOptionTradeReplicationHolder replicationHolder, InvestmentSecurityOptionTypes optionType) {
		boolean putOption = optionType == InvestmentSecurityOptionTypes.PUT;
		ProductOverlayRunOptionReplicationHolder repHolder = putOption ? replicationHolder.getExpiringPutTradedInstrumentReplicationHolder() : replicationHolder.getExpiringCallTradedInstrumentReplicationHolder();
		if (tradeCommand.getNewTradeDefaultPercentageType() != null && !MathUtils.isNullOrZero(tradeCommand.getNewTradeDefaultPercentage())) {
			if (tradeCommand.getNewTradeDefaultPercentageType().isExposure()) {
				BigDecimal newContractValue = (putOption ? replicationHolder.getPutContractValue() : replicationHolder.getCallContractValue());
				if (!MathUtils.isNullOrZero(newContractValue)) {
					BigDecimal exposure = tradeCommand.getNewTradeDefaultPercentageType().isCurrent() ? repHolder.getExposure() : repHolder.getExposureActual();
					exposure = MathUtils.getPercentageOf(tradeCommand.getNewTradeDefaultPercentage(), exposure, true);
					return MathUtils.abs((MathUtils.round(MathUtils.divide(exposure, newContractValue), 0, BigDecimal.ROUND_DOWN)));
				}
			}
			else {
				BigDecimal quantity = tradeCommand.getNewTradeDefaultPercentageType().isCurrent() ? repHolder.getQuantity() : repHolder.getQuantityActual();
				return MathUtils.abs((MathUtils.round(MathUtils.getPercentageOf(tradeCommand.getNewTradeDefaultPercentage(), quantity, true), 0, BigDecimal.ROUND_DOWN)));
			}
		}
		return null;
	}


	private void applyMissingOptionsPositions(InvestmentSecurityOptionTypes optionType, ProductOverlayAssetClassReplication templateReplication, ProductOverlayRunOptionTradeCommand tradeCommand, ProductOverlayRunOptionTradeReplicationHolder replicationHolder, PortfolioAccountContractStore current, PortfolioAccountContractStore pending) {
		List<ProductOverlayAssetClassReplication> missingReplicationList = new ArrayList<>();

		List<ProductOverlayAssetClassReplication> replicationList = new ArrayList<>();
		if (optionType == InvestmentSecurityOptionTypes.CALL) {
			replicationHolder.getExpiringCallReplicationHolder().forEach(replicationList::add);
			replicationHolder.getCallReplicationHolder().forEach(replicationList::add);
		}
		else {
			replicationHolder.getExpiringPutReplicationHolder().forEach(replicationList::add);
			replicationHolder.getPutReplicationHolder().forEach(replicationList::add);
		}

		addMissingOptionsReplicationsToList(optionType, templateReplication, current, replicationList, missingReplicationList);
		addMissingOptionsReplicationsToList(optionType, templateReplication, pending, replicationList, missingReplicationList);

		for (ProductOverlayAssetClassReplication missingRep : CollectionUtils.getIterable(missingReplicationList)) {
			// Refresh prices on transient replications created for Option Roll
			refreshProductOverlayReplicationPrice(tradeCommand, missingRep);

			// In this case we add Current + Pending, since these weren't held on the run
			addReplicationToReplicationHolder(tradeCommand, replicationHolder, missingRep, optionType, MathUtils.add(missingRep.getPendingContractsAdjusted(), missingRep.getCurrentContractsAdjusted()));
		}
	}


	private void addMissingOptionsReplicationsToList(InvestmentSecurityOptionTypes optionType, ProductOverlayAssetClassReplication templateReplication, PortfolioAccountContractStore contractStore, List<ProductOverlayAssetClassReplication> replicationList, List<ProductOverlayAssetClassReplication> missingReplicationList) {
		// Assume all calls for the account will be included in the same asset class - technically this filter will only apply any sub-account filtering since all positions for the account can apply across asset classes
		Map<Integer, PortfolioAccountContractStore.SecurityPositionQuantity> quantityMap = contractStore.getSecurityIdQuantityMapForAccountAssetClass(templateReplication.getOverlayAssetClass().getAccountAssetClass());
		if (quantityMap != null && !quantityMap.isEmpty()) {
			for (Map.Entry<Integer, PortfolioAccountContractStore.SecurityPositionQuantity> securityQuantityEntry : quantityMap.entrySet()) {
				InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityQuantityEntry.getKey());
				if (InvestmentUtils.isSecurityOfType(security, InvestmentType.OPTIONS)) {
					if (optionType == security.getOptionType()) {
						boolean found = false;
						for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(replicationList)) {
							if (MathUtils.isEqual(securityQuantityEntry.getKey(), rep.getSecurity().getId())) {
								found = true;
								break;
							}
						}
						if (!found) {
							ProductOverlayAssetClassReplication newCallRep = calculateNewReplication(templateReplication, security);
							if (contractStore.getType().isPending()) {
								newCallRep.setPendingContracts(securityQuantityEntry.getValue().getQuantity());
							}
							else {
								newCallRep.setCurrentContracts(securityQuantityEntry.getValue().getQuantity());
							}
							missingReplicationList.add(newCallRep);
						}
					}
				}
			}
		}
	}


	private void applyMatchingAdditionalExposureFromManagerBalances(ProductOverlayRunOptionTradeReplicationHolder replicationHolder, PortfolioRun portfolioRun) {
		if (getPutMatchingManagerAccountId() != null) {
			InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(getPutMatchingManagerAccountId(), portfolioRun.getBalanceDate(), false);
			if (balance != null) {
				if (!balance.getManagerAccount().getBaseCurrency().equals(replicationHolder.getClientAccount().getBaseCurrency())) {
					throw new ValidationException("Put Matching Manager Balance Selected [" + balance.getManagerAccount().getLabel() + "] cannot be used because it is in a difference base CCY than the client account.");
				}
				replicationHolder.setPutMatchingAdditionalExposure(MathUtils.add(replicationHolder.getPutMatchingAdditionalExposure(), portfolioRun.isMarketOnCloseAdjustmentsIncluded() ? balance.getMarketOnCloseTotalValue() : balance.getAdjustedTotalValue()));
			}
		}
		if (getCallMatchingManagerAccountId() != null) {
			InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(getCallMatchingManagerAccountId(), portfolioRun.getBalanceDate(), false);
			if (balance != null) {
				if (!balance.getManagerAccount().getBaseCurrency().equals(replicationHolder.getClientAccount().getBaseCurrency())) {
					throw new ValidationException("Call Matching Manager Balance Selected [" + balance.getManagerAccount().getLabel() + "] cannot be used because it is in a difference base CCY than the client account.");
				}
				replicationHolder.setCallMatchingAdditionalExposure(MathUtils.add(replicationHolder.getCallMatchingAdditionalExposure(), portfolioRun.isMarketOnCloseAdjustmentsIncluded() ? balance.getMarketOnCloseTotalValue() : balance.getAdjustedTotalValue()));
			}
		}
	}


	private void applyMatchingExposureLabels(ProductOverlayRunOptionTradeReplicationHolder replicationHolder) {
		if (!StringUtils.isEmpty(getPutMatchingReplicationLabel()) && !replicationHolder.getPutMatchingReplicationHolder().isEmpty()) {
			replicationHolder.setPutMatchingReplicationLabel(getPutMatchingReplicationLabel());
		}
		if (!StringUtils.isEmpty(getPutMatchingAdditionalExposureLabel()) && replicationHolder.getPutMatchingAdditionalExposure() != null) {
			replicationHolder.setPutMatchingAdditionalExposureLabel(getPutMatchingAdditionalExposureLabel());
		}
		if (!StringUtils.isEmpty(getCallMatchingReplicationLabel()) && !replicationHolder.getCallMatchingReplicationHolder().isEmpty()) {
			replicationHolder.setCallMatchingReplicationLabel(getCallMatchingReplicationLabel());
		}
		if (!StringUtils.isEmpty(getCallMatchingAdditionalExposureLabel()) && replicationHolder.getCallMatchingAdditionalExposure() != null) {
			replicationHolder.setCallMatchingAdditionalExposureLabel(getCallMatchingAdditionalExposureLabel());
		}
	}


	private void refreshProductOverlayReplicationPrice(ProductOverlayRunOptionTradeCommand command, ProductOverlayAssetClassReplication replication) {
		if (command.getPopulateLivePrices() != null) {
			getProductReplicationService().refreshProductOverlayAssetClassReplicationSecurityLivePrices(replication, InvestmentReplicationCurrencyTypes.getDefaultCurrencyType(), command.getPopulateLivePrices(), command.isExcludeMispricing());
		}
	}


	/**
	 * Returns the market data price for the provide {@link InvestmentSecurity} and date.
	 * If the security is not active on the date specified, null is returned.
	 * If the security price for the date does not exist in our market data, the prices is populated and returned.
	 * <p>
	 * Populating the new Option price(s) is a convenience for the traders to avoid manually importing prices for newly created
	 * Option securities. Trades created for the new options without a price fail compliance rules.
	 */
	private BigDecimal getSecurityMarketDataPriceForDate(InvestmentSecurity security, Date balanceDate) {
		if (InvestmentUtils.isSecurityActiveOn(security, balanceDate)) {
			BigDecimal balanceDatePrice = getMarketDataRetriever().getPrice(security, balanceDate, false, null);
			if (balanceDatePrice == null) {
				// only populate if the security is active on the balance date to avoid unnecessary data fetches
				MarketDataFieldValueUpdaterCommand updaterCommand = new MarketDataFieldValueUpdaterCommand();
				updaterCommand.setInvestmentSecurityId(security.getId());
				updaterCommand.setStartDate(balanceDate);
				updaterCommand.setEndDate(balanceDate);
				updaterCommand.setSkipDataRequestIfAlreadyExists(true);
				updaterCommand.setSynchronous(true);
				Status status = getMarketDataUpdaterService().updateMarketDataFieldValues(updaterCommand);
				// Check if status details contains '[N|n]o values returned' from MarketData import
				if (status != null && !CollectionUtils.isEmpty(CollectionUtils.getFiltered(status.getDetailList(), detail -> detail.getNote().contains("o values returned")))) {
					LogUtils.warn(getClass(), String.format("Failed to load data for security %s on %s", security, balanceDate));
				}

				balanceDatePrice = getMarketDataRetriever().getPrice(security, balanceDate, true, "Automatic price import failed for " + security + ", please add it manually. ");
			}
			return balanceDatePrice;
		}
		return null;
	}


	/**
	 * Calculates the average contracts held in replications of the portfolio applicable to the traded instrument determined using
	 * {@link ProductOverlayRunOptionTradeCommand#isSecurityApplicableToInstrument(InvestmentSecurity)}.
	 * If put is true, uses the put replications otherwise uses call replications.
	 */
	private BigDecimal calculateTrancheAverage(ProductOverlayRunOptionTradeCommand tradeCommand, ProductOverlayRunOptionTradeReplicationHolder replicationHolder, boolean put) {
		List<ProductOverlayAssetClassReplication> tradedInstrumentApplicableReplicationList;
		if (put) {
			tradedInstrumentApplicableReplicationList = new ArrayList<>(replicationHolder.getExpiringPutTradedInstrumentReplicationHolder().getReplicationList());
			replicationHolder.getPutReplicationHolder().stream()
					.filter(replication -> tradeCommand.isSecurityApplicableToInstrument(replication.getSecurity()))
					.forEach(tradedInstrumentApplicableReplicationList::add);
		}
		else {
			tradedInstrumentApplicableReplicationList = new ArrayList<>(replicationHolder.getExpiringCallTradedInstrumentReplicationHolder().getReplicationList());
			replicationHolder.getCallReplicationHolder().stream()
					.filter(replication -> tradeCommand.isSecurityApplicableToInstrument(replication.getSecurity()))
					.forEach(tradedInstrumentApplicableReplicationList::add);
		}

		// Group Option replications by expiration date to group tranches
		Map<Date, List<ProductOverlayAssetClassReplication>> trancheMap = tradedInstrumentApplicableReplicationList.stream()
				.filter(replication -> MathUtils.isNotEqual(replication.getActualContractsAdjusted(), 0))
				.collect(Collectors.groupingBy(replication -> replication.getSecurity().getLastDeliveryDate()));
		// Calculate the contracts held in each tranche
		List<BigDecimal> trancheHeldContractsList = CollectionUtils.getConverted(trancheMap.values(), replicationList -> CoreMathUtils.sumProperty(replicationList, ProductOverlayAssetClassReplication::getActualContractsAdjusted));
		// Return the average contracts held in a tranche
		return trancheHeldContractsList.isEmpty() ? null : CoreMathUtils.average(trancheHeldContractsList).abs();
	}


	/**
	 * Returns the current cash balance for the provided {@link PortfolioRun}. Looks up the cash balance by using the
	 * Cash asset class since the cash balances can come from different sources (e.g. managed accounts, IMS, etc.).
	 */
	private BigDecimal getCashBalance(PortfolioRun portfolioRun) {
		ProductOverlayAssetClassSearchForm assetClassSearchForm = new ProductOverlayAssetClassSearchForm();
		assetClassSearchForm.setRunId(portfolioRun.getId());
		assetClassSearchForm.setRollupAssetClass(Boolean.FALSE);
		assetClassSearchForm.setMainCash(Boolean.TRUE);
		List<ProductOverlayAssetClass> productOverlayAssetClassList = getProductOverlayService().getProductOverlayAssetClassList(assetClassSearchForm, false);
		return CoreMathUtils.sumProperty(productOverlayAssetClassList, ProductOverlayAssetClass::getTotalExposure);
	}


	/**
	 * Looks up live prices for all expiring options in the portfolio and the options being traded to calculate the cash impact.
	 */
	private void populateLivePricesForExpiringAndPendingOptions(ProductOverlayRunOptionTradeCommand tradeCommand, ProductOverlayRunOptionTradeReplicationHolder replicationHolder) {
		if (tradeCommand.getCallSecurity() != null) {
			MarketDataLive livePrice = getMarketDataRetriever().getLivePrice(tradeCommand.getCallSecurity());
			setLiveSecurityPrice(livePrice, replicationHolder::setCallPrice);
		}
		if (tradeCommand.getPutSecurity() != null) {
			MarketDataLive livePrice = getMarketDataRetriever().getLivePrice(tradeCommand.getPutSecurity());
			setLiveSecurityPrice(livePrice, replicationHolder::setPutPrice);
		}
		Consumer<ProductOverlayAssetClassReplication> replicationTradeSecurityPricePopulator = replication -> {
			MarketDataLive livePrice = getMarketDataRetriever().getLivePrice(replication.getSecurity());
			setLiveSecurityPrice(livePrice, replication::setTradeSecurityPrice);
		};
		replicationHolder.getExpiringCallReplicationHolder().forEach(replicationTradeSecurityPricePopulator);
		replicationHolder.getExpiringPutReplicationHolder().forEach(replicationTradeSecurityPricePopulator);
	}


	private void setLiveSecurityPrice(MarketDataLive value, Consumer<BigDecimal> priceSetter) {
		if (value != null) {
			priceSetter.accept(value.asNumericValue());
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataUpdaterService getMarketDataUpdaterService() {
		return this.marketDataUpdaterService;
	}


	public void setMarketDataUpdaterService(MarketDataUpdaterService marketDataUpdaterService) {
		this.marketDataUpdaterService = marketDataUpdaterService;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}


	public ProductReplicationService getProductReplicationService() {
		return this.productReplicationService;
	}


	public void setProductReplicationService(ProductReplicationService productReplicationService) {
		this.productReplicationService = productReplicationService;
	}


	public ProductUtilService getProductUtilService() {
		return this.productUtilService;
	}


	public void setProductUtilService(ProductUtilService productUtilService) {
		this.productUtilService = productUtilService;
	}


	public String getPutMatchingReplicationLabel() {
		return this.putMatchingReplicationLabel;
	}


	public void setPutMatchingReplicationLabel(String putMatchingReplicationLabel) {
		this.putMatchingReplicationLabel = putMatchingReplicationLabel;
	}


	public String getPutMatchingAdditionalExposureLabel() {
		return this.putMatchingAdditionalExposureLabel;
	}


	public void setPutMatchingAdditionalExposureLabel(String putMatchingAdditionalExposureLabel) {
		this.putMatchingAdditionalExposureLabel = putMatchingAdditionalExposureLabel;
	}


	public String getCallMatchingReplicationLabel() {
		return this.callMatchingReplicationLabel;
	}


	public void setCallMatchingReplicationLabel(String callMatchingReplicationLabel) {
		this.callMatchingReplicationLabel = callMatchingReplicationLabel;
	}


	public String getCallMatchingAdditionalExposureLabel() {
		return this.callMatchingAdditionalExposureLabel;
	}


	public void setCallMatchingAdditionalExposureLabel(String callMatchingAdditionalExposureLabel) {
		this.callMatchingAdditionalExposureLabel = callMatchingAdditionalExposureLabel;
	}


	public Integer getBusinessClientId() {
		return this.businessClientId;
	}


	public void setBusinessClientId(Integer businessClientId) {
		this.businessClientId = businessClientId;
	}


	public Integer getPutMatchingManagerAccountId() {
		return this.putMatchingManagerAccountId;
	}


	public void setPutMatchingManagerAccountId(Integer putMatchingManagerAccountId) {
		this.putMatchingManagerAccountId = putMatchingManagerAccountId;
	}


	public Integer getCallMatchingManagerAccountId() {
		return this.callMatchingManagerAccountId;
	}


	public void setCallMatchingManagerAccountId(Integer callMatchingManagerAccountId) {
		this.callMatchingManagerAccountId = callMatchingManagerAccountId;
	}
}
