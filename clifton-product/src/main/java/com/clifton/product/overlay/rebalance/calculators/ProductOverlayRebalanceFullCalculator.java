package com.clifton.product.overlay.rebalance.calculators;


import com.clifton.core.util.CollectionUtils;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.rebalance.ProductOverlayRebalance;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>ProductOverlayRebalanceFullCalculator</code> performs a full rebalance
 * <p/>
 * Calculation: Asset Class Target - Actual Allocation - Effective Cash = New Rebalance Cash
 * Exclude IsMainCash Asset Class
 *
 * @author manderson
 */
public class ProductOverlayRebalanceFullCalculator extends BaseProductOverlayRebalanceCalculatorImpl {

	@Override
	public void calculateNewRebalanceCash(ProductOverlayRebalance bean) {
		for (ProductOverlayAssetClass portfolioAssetClass : CollectionUtils.getIterable(bean.getRebalanceAssetClassList())) {
			if (portfolioAssetClass.getAccountAssetClass().getAssetClass().isMainCash() || !portfolioAssetClass.isReplicationUsed()) {
				portfolioAssetClass.setNewRebalanceCash(BigDecimal.ZERO);
			}
			else {
				portfolioAssetClass.setNewRebalanceCash(calculateNewRebalanceCashForAssetClass(portfolioAssetClass));
			}
		}
	}


	/**
	 * Note: This will calculate for any asset class - it is up to the calling method to check for primary replication
	 * if it should actually be set.  This is because for cases with rollups, we want to know that amount in order to
	 * distribute to it's children, but we don't actually set it when it's saved
	 */
	protected BigDecimal calculateNewRebalanceCashForAssetClass(ProductOverlayAssetClass portfolioAssetClass) {
		// Calculation: Asset Class Target - Actual Allocation - (Effective Cash - Rebalance Cash Adjusted (note DOES include attribution rebalance cash))
		// Can't include original/adjusted rebalance cash in calculation since we are overwriting it
		BigDecimal effectiveCash = portfolioAssetClass.getEffectiveCashTotal();
		effectiveCash = MathUtils.subtract(effectiveCash, portfolioAssetClass.getRebalanceCashAdjusted());
		return MathUtils.subtract(MathUtils.subtract(portfolioAssetClass.getTargetAllocationAdjusted(), portfolioAssetClass.getActualAllocationAdjusted()), effectiveCash);
	}
}
