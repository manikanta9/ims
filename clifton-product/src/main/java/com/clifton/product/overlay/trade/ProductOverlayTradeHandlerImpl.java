package com.clifton.product.overlay.trade;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.marketdata.live.MarketDataLiveExchangeRate;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.trade.BasePortfolioRunTradeHandler;
import com.clifton.portfolio.run.trade.PortfolioCurrencyTrade;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.replication.ProductReplicationService;
import com.clifton.product.util.ProductUtilServiceImpl;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductOverlayTradeHandlerImpl</code> ...
 *
 * @author manderson
 */
@Component("portfolioRunTradeOverlayHandler")
public class ProductOverlayTradeHandlerImpl extends BasePortfolioRunTradeHandler<ProductOverlayRunTrade, ProductOverlayAssetClassReplication> {

	private ProductOverlayService productOverlayService;

	private ProductReplicationService productReplicationService;


	/**
	 * When viewing trade creation for an actively traded run, the balance uses the given days in the future as the date to find current contracts
	 * This way any trades entered in the future (usually foreign exchanges that are already closed) are always picked up on the screen.
	 */
	private static final int TRADING_CURRENT_BALANCE_DAYS_IN_FUTURE = 30;


	@Override
	public ProductOverlayRunTrade getPortfolioRunTradeImpl(PortfolioRun run) {
		ProductOverlayRunTrade tradeRun = getPortfolioRunForTrading(run, ProductOverlayRunTrade.class);
		tradeRun.setExcludeMispricing(BooleanUtils.isTrue(getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(run.getClientInvestmentAccount(),
				ProductUtilServiceImpl.INVESTMENT_ACCOUNT_EXCLUDE_MISPRICING_FIELD_NAME)));
		return tradeRun;
	}


	@Override
	public ProductOverlayRunTrade getPortfolioRunTradePrices(ProductOverlayRunTrade run, boolean livePrices, boolean reloadDetails) {
		ProductOverlayRunTrade returnRun = getPortfolioRunForTrading(run.getId(), ProductOverlayRunTrade.class);
		returnRun.setExcludeMispricing(run.isExcludeMispricing());

		Map<String, List<InvestmentAccountRelationship>> runHoldingAccountRelationshipMap = new HashMap<>();
		if (reloadDetails) {
			// Don't want to lose data already entered on screen so just need to add what is missing
			List<ProductOverlayAssetClassReplication> detailList = new ArrayList<>();
			for (ProductOverlayAssetClassReplication dbDetail : CollectionUtils.getIterable(getPortfolioRunTradeDetailListForRun(run.getId()))) {
				boolean found = false;
				for (ProductOverlayAssetClassReplication uiDetail : CollectionUtils.getIterable(run.getDetailList())) {
					if (MathUtils.isEqual(dbDetail.getId(), uiDetail.getId())) {
						found = true;
						// Copy changes in overrides if necessary
						copySecurityTradeInfo(uiDetail, dbDetail);
						detailList.add(uiDetail);
						break;
					}
				}
				if (!found) {
					// Set up Trading Defaults for new rows
					populatePortfolioRunTradeDetailDefaults(dbDetail, runHoldingAccountRelationshipMap);
					detailList.add(dbDetail);
				}
			}
			returnRun.setDetailList(detailList);
		}
		else {
			returnRun.setDetailList(run.getDetailList());
		}
		returnRun.setCurrencyTradeList(run.getCurrencyTradeList());

		// Reset Prices
		refreshPortfolioRunTradeDetailListPrices(returnRun, livePrices);

		// Reset Current/Pending in case price changes change splitting
		populatePortfolioRunTradeDetailPendingAndCurrentContracts(returnRun);

		return returnRun;
	}


	@Override
	public void refreshPortfolioRunTradeDetailListPrices(ProductOverlayRunTrade run, boolean livePrices) {
		for (ProductOverlayAssetClassReplication detail : CollectionUtils.getIterable(run.getDetailList())) {
			// sets/clears live prices and also calculates "trade" contract value (if applies)
			getProductReplicationService().refreshProductOverlayAssetClassReplicationSecurityLivePrices(detail, run.getTradingCurrencyType(), livePrices, run.isExcludeMispricing());
		}

		for (PortfolioCurrencyTrade<ProductOverlayAssetClassReplication> cur : CollectionUtils.getIterable(run.getCurrencyTradeList())) {
			// Only update for Currency Others, the ones tied to replications pull updated values directly from the replication, so not necessary to dupe look ups
			if (cur.getReplication() == null) {
				if (livePrices) {
					MarketDataLiveExchangeRate liveRate = getMarketDataRatesRetriever().getLiveExchangeRate(cur.getCurrencySecurity(), run.getClientInvestmentAccount().getBaseCurrency());
					if (liveRate != null) {
						cur.setCurrentExchangeRate(liveRate.asNumericValue());
						cur.setCurrentExchangeRateDate(liveRate.getMeasureDate());
					}
				}
				else {
					cur.setCurrentExchangeRate(null);
					cur.setCurrentExchangeRateDate(null);
				}
			}
		}
	}


	@Override
	protected List<ProductOverlayAssetClassReplication> getPortfolioRunTradeDetailListForRunImpl(int runId) {
		return getProductOverlayService().getProductOverlayAssetClassReplicationListByRun(runId);
	}


	@Override
	public ProductOverlayRunTrade getPortfolioRunTradeAmounts(ProductOverlayRunTrade run) {
		ProductOverlayRunTrade returnRun = getPortfolioRunForTrading(run.getId(), ProductOverlayRunTrade.class);
		returnRun.setExcludeMispricing(run.isExcludeMispricing());

		// Get a clean list and then copy of price info that was on screen
		populatePortfolioRunTradeDetailListWithTrades(returnRun, false);
		for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(returnRun.getDetailList())) {
			for (ProductOverlayAssetClassReplication rep2 : CollectionUtils.getIterable(run.getDetailList())) {
				if (rep.equals(rep2)) {
					copySecurityTradeInfo(rep, rep2);
				}
			}
		}

		// Set Current/Pending & Process Splitting
		populatePortfolioRunTradeDetailPendingAndCurrentContracts(returnRun);

		return returnRun;
	}


	@Override
	public void populatePortfolioRunTradeDetailListWithTrades(ProductOverlayRunTrade run, boolean doNotPopulateDefaults) {
		List<ProductOverlayAssetClassReplication> list = getPortfolioRunTradeDetailListForRun(run.getId());
		List<PortfolioCurrencyTrade<ProductOverlayAssetClassReplication>> currencyTradeList = new ArrayList<>();


		if (!CollectionUtils.isEmpty(list)) {
			// Only Setup Defaults if will be generating trades, if just viewing, only need to get Pending Trades
			boolean populateDefaults = (!doNotPopulateDefaults && run.isTradingAllowed());
			// For the same look ups - re-use previously found holding accounts
			// Used for setDefaults = true
			Map<String, List<InvestmentAccountRelationship>> runHoldingAccountRelationshipMap = new HashMap<>();


			for (ProductOverlayAssetClassReplication rep : list) {
				if (populateDefaults) {
					populatePortfolioRunTradeDetailDefaults(rep, runHoldingAccountRelationshipMap);
				}
				if (rep.getReplicationType().isCurrency()) {
					PortfolioCurrencyTrade<ProductOverlayAssetClassReplication> curTrade = new PortfolioCurrencyTrade<>();
					curTrade.setReplication(rep);
					currencyTradeList.add(curTrade);
				}
			}
		}

		for (PortfolioCurrencyOther cur : CollectionUtils.getIterable(getPortfolioRunService().getPortfolioCurrencyOtherListByRun(run.getId()))) {
			PortfolioCurrencyTrade<ProductOverlayAssetClassReplication> curTrade = new PortfolioCurrencyTrade<>();
			BeanUtils.copyProperties(cur, curTrade);
			currencyTradeList.add(curTrade);
		}
		run.setCurrencyTradeList(currencyTradeList);
		run.setDetailList(list);
	}


	@Override
	@Transactional(readOnly = true)
	public void populatePortfolioRunTradeDetailPendingAndCurrentContracts(ProductOverlayRunTrade run) {
		List<ProductOverlayAssetClassReplication> replicationList = run.getDetailList();

		// If it's a historical run or trading is disabled, leave the position look up date as of the next day
		Date positionLookupDate = getNextBusinessDayFromRun(run);
		// Otherwise go specified days in the future so we get any future trades that have already been booked
		if (run.isTradingAllowed()) {
			positionLookupDate = DateUtils.addDays(new Date(), TRADING_CURRENT_BALANCE_DAYS_IN_FUTURE);
		}

		PortfolioAccountContractStore current = getPortfolioAccountDataRetriever().getPortfolioAccountContractStoreActual(run.getClientInvestmentAccount(), positionLookupDate, true);
		PortfolioAccountContractStore pending = getPortfolioAccountDataRetriever().getPortfolioAccountContractStorePending(current, run.getClientInvestmentAccount(), null);

		getProductReplicationService().processContractSplitting(current, replicationList);
		getProductReplicationService().processContractSplitting(pending, replicationList);

		List<ProductOverlayAssetClassReplication> currencyRepList = BeanUtils.filter(replicationList, ProductOverlayAssetClassReplication::isCurrencyReplication);

		List<PortfolioCurrencyTrade<ProductOverlayAssetClassReplication>> currencyTradeList = new ArrayList<>();
		List<PortfolioCurrencyOther> currencyOtherList = getPortfolioRunService().getPortfolioCurrencyOtherListByRun(run.getId());
		getProductReplicationService().processCurrencyContracts(current, run, currencyRepList, currencyOtherList, currencyTradeList, replicationList);
		getProductReplicationService().processCurrencyContracts(pending, run, currencyRepList, currencyOtherList, currencyTradeList, replicationList);
		run.setCurrencyTradeList(currencyTradeList);
	}


	//////////////////////////////////////////////////
	//////////       Helper Methods         //////////
	//////////////////////////////////////////////////


	@Override
	public ProductOverlayAssetClassReplication getPortfolioRunTradeDetail(int detailId) {
		return getProductOverlayService().getProductOverlayAssetClassReplication(detailId);
	}


	@Override
	protected boolean isTradeEntryDisabled(ProductOverlayAssetClassReplication detail) {
		return detail.isTradeEntryDisabled();
	}


	@Override
	protected InvestmentAccountAssetClass getAccountAssetClass(ProductOverlayAssetClassReplication detail) {
		return detail.getOverlayAssetClass().getAccountAssetClass();
	}


	@Override
	protected String getTradeEntryErrorMessagePrefix(ProductOverlayAssetClassReplication detail) {
		return "Trade entry [" + detail.getTopLevelLabelShort() + "] for security [" + detail.getSecurity().getSymbol() + "] ";
	}


	@Override
	protected void copySecurityTradeInfo(ProductOverlayAssetClassReplication detail, ProductOverlayAssetClassReplication fromDetail) {
		super.copySecurityTradeInfo(detail, fromDetail);

		detail.setTradeSecurityDirtyPrice(fromDetail.getTradeSecurityDirtyPrice());
		detail.setTradeSecurityDirtyPriceDate(fromDetail.getTradeSecurityPriceDate());

		detail.setTradeUnderlyingSecurityPrice(fromDetail.getTradeUnderlyingSecurityPrice());
		detail.setTradeUnderlyingSecurityPriceDate(fromDetail.getTradeUnderlyingSecurityPriceDate());

		detail.setTradeCurrencyExchangeRate(fromDetail.getTradeCurrencyExchangeRate());
		detail.setTradeCurrencyExchangeRateDate(fromDetail.getTradeCurrencyExchangeRateDate());

		// NOTE: Set Fair Value Adjustments, not just "live" data because these aren't real fields saved in the db
		detail.setFairValueAdjustment(fromDetail.getFairValueAdjustment());
		detail.setPreviousFairValueAdjustment(fromDetail.getPreviousFairValueAdjustment());
		detail.setTradeFairValueAdjustment(fromDetail.getTradeFairValueAdjustment());
		detail.setTradeFairValueAdjustmentDate(fromDetail.getTradeFairValueAdjustmentDate());
	}


	//////////////////////////////////////////////////
	///////      Getter & Setter Methods       ///////
	//////////////////////////////////////////////////


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}


	public ProductReplicationService getProductReplicationService() {
		return this.productReplicationService;
	}


	public void setProductReplicationService(ProductReplicationService productReplicationService) {
		this.productReplicationService = productReplicationService;
	}
}
