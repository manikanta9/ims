package com.clifton.product.overlay.rebalance.calculators;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassAssignment;
import com.clifton.investment.account.targetexposure.InvestmentAccountTargetExposureHandler;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.rebalance.ProductOverlayRebalance;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Map;


/**
 * The <code>ProductOverlayRebalanceFundWeightCalculator</code> ...
 *
 * @author manderson
 */
public class ProductOverlayRebalanceFundWeightCalculator extends BaseProductOverlayRebalanceCalculatorImpl {

	private InvestmentAccountTargetExposureHandler<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> investmentAccountTargetExposureHandler;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void calculateNewRebalanceCash(ProductOverlayRebalance bean) {
		BigDecimal total = CoreMathUtils.sumProperty(bean.getRebalanceAssetClassList(), ProductOverlayAssetClass::getRebalanceCashAdjusted);

		// Offset total as the actual adjustment we need to apply to bring back to zero.
		BigDecimal totalAdjustment = MathUtils.negate(total);
		Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> accountAssetClassProductOverlayAssetClassMap = BeanUtils.getBeanMap(bean.getRebalanceAssetClassList(), ProductOverlayAssetClass::getAccountAssetClass);

		for (ProductOverlayAssetClass portfolioAssetClass : CollectionUtils.getIterable(bean.getRebalanceAssetClassList())) {
			InvestmentAccountAssetClass accountAssetClass = portfolioAssetClass.getAccountAssetClass();
			BigDecimal cashAmount = MathUtils.getPercentageOf(accountAssetClass.getCashPercentage(), totalAdjustment, true);
			if (accountAssetClass.isCashAdjustmentUsed()) {
				PortfolioRun run = portfolioAssetClass.getOverlayRun();
				BigDecimal amount = getInvestmentAccountTargetExposureHandler().calculateTargetExposureAdjustedAmount(accountAssetClass, accountAssetClass.getCashAdjustment(), cashAmount,
						portfolioAssetClass.getActualAllocationAdjusted(), run.getBalanceDate(), totalAdjustment, run.isMarketOnCloseAdjustmentsIncluded());
				portfolioAssetClass.setNewRebalanceCash(MathUtils.add(portfolioAssetClass.getRebalanceCashAdjusted(), amount));
				// Apply cash adjustments to assignments
				BigDecimal adjustment = MathUtils.subtract(cashAmount, amount);
				if (!MathUtils.isNullOrZero(adjustment)) {
					applyCashAdjustments(accountAssetClassProductOverlayAssetClassMap, accountAssetClass, adjustment);
				}
			}
			else {
				portfolioAssetClass.setNewRebalanceCash(MathUtils.add(portfolioAssetClass.getRebalanceCashAdjusted(), cashAmount));
			}
		}
		// Fix Rounding
		CoreMathUtils.applySumPropertyDifference(bean.getRebalanceAssetClassList(), ProductOverlayAssetClass::getNewRebalanceCash, ProductOverlayAssetClass::setNewRebalanceCash, BigDecimal.ZERO, true);
	}


	/**
	 * Applies adjustments for Cash {@link com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustment} for dependent overlay asset classes where needed
	 */
	private void applyCashAdjustments(Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> rebalanceAssetClassMap, InvestmentAccountAssetClass accountAssetClass, BigDecimal totalAdjustment) {
		Map<InvestmentAccountAssetClass, BigDecimal> adjustmentMap = getInvestmentAccountTargetExposureHandler().calculateTargetExposureAdjustmentMap(accountAssetClass, accountAssetClass.getCashAdjustment(), totalAdjustment, null);
		for (Map.Entry<InvestmentAccountAssetClass, BigDecimal> investmentAccountAssetClassAdjustmentEntry : adjustmentMap.entrySet()) {
			ProductOverlayAssetClass adjAssetClass = rebalanceAssetClassMap.get(investmentAccountAssetClassAdjustmentEntry.getKey());
			if (adjAssetClass != null) {
				// apply adjustment to overlay asset class included in rebalancing
				adjAssetClass.setNewRebalanceCash(MathUtils.add(adjAssetClass.getRebalanceCashAdjusted(), investmentAccountAssetClassAdjustmentEntry.getValue()));
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccountTargetExposureHandler<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> getInvestmentAccountTargetExposureHandler() {
		return this.investmentAccountTargetExposureHandler;
	}


	public void setInvestmentAccountTargetExposureHandler(InvestmentAccountTargetExposureHandler<InvestmentAccountAssetClass, InvestmentAccountAssetClassAssignment> investmentAccountTargetExposureHandler) {
		this.investmentAccountTargetExposureHandler = investmentAccountTargetExposureHandler;
	}
}
