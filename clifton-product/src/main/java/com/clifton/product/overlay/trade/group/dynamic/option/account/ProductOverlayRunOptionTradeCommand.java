package com.clifton.product.overlay.trade.group.dynamic.option.account;

import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * <code>ProductOverlayRunOptionTradeCommand</code> is a command that is used for
 * providing calculation arguments for populating an {@link ProductOverlayRunOptionTradeReplicationHolder}
 * for a client {@link com.clifton.investment.account.InvestmentAccount}.
 *
 * @author NickK
 */
public class ProductOverlayRunOptionTradeCommand {

	private InvestmentInstrument optionInstrument;
	private InvestmentSecurity putSecurity;
	private InvestmentSecurity callSecurity;

	private Date balanceDate;
	private Date expiringOptionsDate;

	private Boolean longPositions;

	/**
	 * On Friday, we enter the expiration date and want to calculate % excess when those expiring options
	 * are removed.  However, on Monday, we want to enter the expiration date of the options that were bought
	 * as we generally will buy the second round based on what was traded previously.
	 * <p>
	 * Note: This is copied to each ProductOverlayRunTradeGroupDynamicOptionAccountRoll object to make it
	 * easier for the grid to determine how to calculate
	 */
	private Boolean doNotRemoveExpiringOptionsFromExcessCalculation;

	/**
	 * Usually 50% of expiring, or 100% of not expiring - Only used to default the new trade quantities on screen
	 * TODO: SHOULD MOVE THIS TO THE ACCOUNT CALCULATOR
	 */
	private BigDecimal newTradeDefaultPercentage;

	/**
	 * By default we use Actual exposure of expiring to determine new trade calculations
	 * This way if expiration is processed first, we can still calculate based on quantity/exposure that was there.
	 * But can use QUANTITY, QUANTITY_CURRENT, EXPOSURE, EXPOSURE_CURRENT
	 */
	private ProductOverlayRunTradeGroupDynamicOptionRollPercentageType newTradeDefaultPercentageType;

	// Flag used to have new Call and/or Put trade quantities for Option Roll.
	private boolean calculateNewTradeQuantities;

	// Booleans used for populating prices on the populated replications.
	private Boolean populateLivePrices;
	private boolean excludeMispricing;

	// Flag used to signal to the calculator to calculate the average trade quantity for each option Put/Call tranche
	private boolean calculateTrancheTradeAverage;

	// Booleans used to trigger lookups for cash balance and live prices for showing estimated impact to cash
	private Boolean calculateCashBalance;
	private Boolean calculateCashTradeImpact;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public enum ProductOverlayRunTradeGroupDynamicOptionRollPercentageType {
		QUANTITY(false, false),
		QUANTITY_CURRENT(false, true),
		EXPOSURE(true, false),
		EXPOSURE_CURRENT(true, true);

		private final boolean exposure;
		private final boolean current;


		ProductOverlayRunTradeGroupDynamicOptionRollPercentageType(boolean exposure, boolean current) {
			this.exposure = exposure;
			this.current = current;
		}


		public boolean isExposure() {
			return this.exposure;
		}


		public boolean isCurrent() {
			return this.current;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if the expiration date on the passed security matches the {@link #getExpiringOptionsDate()} and
	 * the passed quantity is in the same direction as {@link #getLongPositions()}
	 */
	public boolean isSecurityExpiring(InvestmentSecurity security, BigDecimal quantity) {
		if (getLongPositions() != null && MathUtils.isGreaterThan(quantity, BigDecimal.ZERO) == getLongPositions()) {
			return DateUtils.compare(getExpiringOptionsDate(), security.getLastDeliveryDate(), false) == 0;
		}
		return false;
	}


	/**
	 * Returns true if the underlying instrument of the passed security matches that of {@link #getOptionInstrument()}
	 * <p>
	 * i.e. SPX Week 2 and Week 4 are considered the "same", but EEM Options would be different
	 */
	public boolean isSecurityApplicableToInstrument(InvestmentSecurity security) {
		if (getOptionInstrument() != null && getOptionInstrument().getUnderlyingInstrument() != null && security.getInstrument().getUnderlyingInstrument() != null) {
			return getOptionInstrument().getUnderlyingInstrument().equals(security.getInstrument().getUnderlyingInstrument());
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrument getOptionInstrument() {
		return this.optionInstrument;
	}


	public InvestmentSecurity getPutSecurity() {
		return this.putSecurity;
	}


	public void setPutSecurity(InvestmentSecurity putSecurity) {
		this.putSecurity = putSecurity;
	}


	public InvestmentSecurity getCallSecurity() {
		return this.callSecurity;
	}


	public void setCallSecurity(InvestmentSecurity callSecurity) {
		this.callSecurity = callSecurity;
	}


	public void setOptionInstrument(InvestmentInstrument optionInstrument) {
		this.optionInstrument = optionInstrument;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public Date getExpiringOptionsDate() {
		return this.expiringOptionsDate;
	}


	public void setExpiringOptionsDate(Date expiringOptionsDate) {
		this.expiringOptionsDate = expiringOptionsDate;
	}


	public Boolean getLongPositions() {
		return this.longPositions;
	}


	public void setLongPositions(Boolean longPositions) {
		this.longPositions = longPositions;
	}


	public Boolean getDoNotRemoveExpiringOptionsFromExcessCalculation() {
		return this.doNotRemoveExpiringOptionsFromExcessCalculation;
	}


	public void setDoNotRemoveExpiringOptionsFromExcessCalculation(Boolean doNotRemoveExpiringOptionsFromExcessCalculation) {
		this.doNotRemoveExpiringOptionsFromExcessCalculation = doNotRemoveExpiringOptionsFromExcessCalculation;
	}


	public BigDecimal getNewTradeDefaultPercentage() {
		return this.newTradeDefaultPercentage;
	}


	public void setNewTradeDefaultPercentage(BigDecimal newTradeDefaultPercentage) {
		this.newTradeDefaultPercentage = newTradeDefaultPercentage;
	}


	public ProductOverlayRunTradeGroupDynamicOptionRollPercentageType getNewTradeDefaultPercentageType() {
		return this.newTradeDefaultPercentageType;
	}


	public void setNewTradeDefaultPercentageType(ProductOverlayRunTradeGroupDynamicOptionRollPercentageType newTradeDefaultPercentageType) {
		this.newTradeDefaultPercentageType = newTradeDefaultPercentageType;
	}


	public boolean isCalculateNewTradeQuantities() {
		return this.calculateNewTradeQuantities;
	}


	public void setCalculateNewTradeQuantities(boolean calculateNewTradeQuantities) {
		this.calculateNewTradeQuantities = calculateNewTradeQuantities;
	}


	public Boolean getPopulateLivePrices() {
		return this.populateLivePrices;
	}


	public void setPopulateLivePrices(Boolean populateLivePrices) {
		this.populateLivePrices = populateLivePrices;
	}


	public boolean isExcludeMispricing() {
		return this.excludeMispricing;
	}


	public void setExcludeMispricing(boolean excludeMispricing) {
		this.excludeMispricing = excludeMispricing;
	}


	public boolean isCalculateTrancheTradeAverage() {
		return this.calculateTrancheTradeAverage;
	}


	public void setCalculateTrancheTradeAverage(boolean calculateTrancheTradeAverage) {
		this.calculateTrancheTradeAverage = calculateTrancheTradeAverage;
	}


	public Boolean getCalculateCashBalance() {
		return this.calculateCashBalance;
	}


	public void setCalculateCashBalance(Boolean calculateCashBalance) {
		this.calculateCashBalance = calculateCashBalance;
	}


	public Boolean getCalculateCashTradeImpact() {
		return this.calculateCashTradeImpact;
	}


	public void setCalculateCashTradeImpact(Boolean calculateCashTradeImpact) {
		this.calculateCashTradeImpact = calculateCashTradeImpact;
	}
}
