package com.clifton.product.overlay.rebalance.calculators;


import com.clifton.core.util.AssertUtils;
import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>ProductOverlayRebalanceCalculatorLocatorInSpringContext</code> class locates ProductOverlayRebalanceCalculator implementations for corresponding RebalanceCalculationType.
 * It auto-discovers all application context beans that implement ProductOverlayRebalanceCalculator interface and registers them so that they can be located.
 *
 * @author manderson
 */
public class ProductOverlayRebalanceCalculatorLocatorInSpringContext implements ProductOverlayRebalanceCalculatorLocator, InitializingBean, ApplicationContextAware {

	private final Map<RebalanceCalculationTypes, ProductOverlayRebalanceCalculator> calculatorMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		Map<String, ProductOverlayRebalanceCalculator> beanMap = getApplicationContext().getBeansOfType(ProductOverlayRebalanceCalculator.class);

		// need a map with ProductManagerAccountBalanceLinkedCalculator names as keys instead of bean names
		for (Map.Entry<String, ProductOverlayRebalanceCalculator> stringProductOverlayRebalanceCalculatorEntry : beanMap.entrySet()) {
			ProductOverlayRebalanceCalculator calculator = stringProductOverlayRebalanceCalculatorEntry.getValue();
			if (getCalculatorMap().containsKey(calculator.getRebalanceCalculationType())) {
				throw new RuntimeException("Cannot register '" + stringProductOverlayRebalanceCalculatorEntry.getKey() + "' as a calculator for rebalance calculation type '" + calculator.getRebalanceCalculationType()
						+ "' because this rebalance calculation type already has a registered calculator.");
			}
			getCalculatorMap().put(calculator.getRebalanceCalculationType(), calculator);
		}
	}


	@Override
	public ProductOverlayRebalanceCalculator locate(RebalanceCalculationTypes rebalanceCalculationType) {
		AssertUtils.assertNotNull(rebalanceCalculationType, "RebalanceCalculationType cannot be null.");
		ProductOverlayRebalanceCalculator result = getCalculatorMap().get(rebalanceCalculationType);
		AssertUtils.assertNotNull(result, "Cannot locate ProductOverlayRebalanceCalculator for '%1s' rebalance calculation type.", rebalanceCalculationType);
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Map<RebalanceCalculationTypes, ProductOverlayRebalanceCalculator> getCalculatorMap() {
		return this.calculatorMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
