package com.clifton.product.overlay.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


/**
 * {@link com.clifton.core.dataaccess.search.form.SearchForm} for {@link com.clifton.product.overlay.ProductOverlayAssetClass}s.
 *
 * @author Mary Anderson
 */
public class ProductOverlayAssetClassSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "overlayRun.id", required = true)
	private Integer runId;

	@SearchField(searchField = "rebalanceTriggerMin", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean withRebalanceTriggersOnly;

	@SearchField(searchField = "primaryReplication.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean withReplicationsOnly;

	@SearchField(searchField = "parent.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean rootOnly;

	@SearchField(searchField = "parent.id")
	private Integer parentId;

	@SearchField(searchField = "accountAssetClass.id")
	private Integer accountAssetClassId;

	@SearchField(searchFieldPath = "accountAssetClass", searchField = "assetClass.mainCash")
	private Boolean mainCash;

	@SearchField
	private Boolean rollupAssetClass;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getRootOnly() {
		return this.rootOnly;
	}


	public void setRootOnly(Boolean rootOnly) {
		this.rootOnly = rootOnly;
	}


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public Boolean getWithRebalanceTriggersOnly() {
		return this.withRebalanceTriggersOnly;
	}


	public void setWithRebalanceTriggersOnly(Boolean withRebalanceTriggersOnly) {
		this.withRebalanceTriggersOnly = withRebalanceTriggersOnly;
	}


	public Boolean getWithReplicationsOnly() {
		return this.withReplicationsOnly;
	}


	public void setWithReplicationsOnly(Boolean withReplicationsOnly) {
		this.withReplicationsOnly = withReplicationsOnly;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public Integer getAccountAssetClassId() {
		return this.accountAssetClassId;
	}


	public void setAccountAssetClassId(Integer accountAssetClassId) {
		this.accountAssetClassId = accountAssetClassId;
	}


	public Boolean getMainCash() {
		return this.mainCash;
	}


	public void setMainCash(Boolean mainCash) {
		this.mainCash = mainCash;
	}


	public Boolean getRollupAssetClass() {
		return this.rollupAssetClass;
	}


	public void setRollupAssetClass(Boolean rollupAssetClass) {
		this.rollupAssetClass = rollupAssetClass;
	}
}
