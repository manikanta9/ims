package com.clifton.product.overlay.process.calculators;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAllocationConfigTypes;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalanceService;
import com.clifton.portfolio.run.process.calculator.BasePortfolioRunProcessStepCalculatorImpl;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.overlay.manager.allocation.ProductOverlayManagerAllocationCalculator;
import com.clifton.product.overlay.manager.allocation.ProductOverlayManagerAllocationConfig;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The {@link ProductOverlayManagerCalculator} processes {@link ProductOverlayManagerAccount}s for a
 * specific run.
 * <p>
 * STEP 1
 *
 * @author Mary Anderson
 */
public class ProductOverlayManagerCalculator extends BasePortfolioRunProcessStepCalculatorImpl<ProductOverlayRunConfig> {


	private PortfolioRunManagerAccountBalanceService<?> portfolioRunManagerAccountBalanceService;

	private ProductOverlayManagerAllocationCalculator productOverlayManagerAllocationCalculator;

	private ProductOverlayService productOverlayService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void processStep(ProductOverlayRunConfig runConfig) {
		// Manager Assignments
		calculateProductOverlayManagerAccountList(runConfig);
	}


	@Override
	protected void saveStep(ProductOverlayRunConfig runConfig) {
		// Save All Manager Related Data
		getProductOverlayService().saveProductOverlayManagerAccountListByRun(runConfig.getRun().getId(), runConfig.getRun().getCashTotal(), runConfig.getOverlayManagerAccountList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void calculateProductOverlayManagerAccountList(ProductOverlayRunConfig runConfig) {
		List<ProductOverlayManagerAccount> overlayManagerAccountList = new ArrayList<>();
		List<ProductOverlayManagerAccount> rollupOverlayManagerAccountList = new ArrayList<>();

		getProductOverlayManagerAllocationCalculator().processProductOverlayManagerAllocationConfigList(runConfig, false);
		Map<InvestmentManagerAccountAssignment, List<ProductOverlayManagerAllocationConfig>> assignmentAllocationConfigListMap = BeanUtils.getBeansMap(runConfig.getManagerAllocationConfigList(), ProductOverlayManagerAllocationConfig::getManagerAccountAssignment);

		for (Map.Entry<InvestmentManagerAccountAssignment, List<ProductOverlayManagerAllocationConfig>> assignmentAllocationConfigEntry : assignmentAllocationConfigListMap.entrySet()) {
			InvestmentManagerAccountAssignment assignment = assignmentAllocationConfigEntry.getKey();
			// Previous Date's Value
			InvestmentManagerAccountBalance prevBal = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(assignment.getReferenceOne().getId(),
					runConfig.getPreviousBusinessDay(), false);
			// Previous Month End Date's Value
			InvestmentManagerAccountBalance prevMEBal = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(assignment.getReferenceOne().getId(),
					runConfig.getPreviousMonthEnd(), false);

			BigDecimal previousDaySecuritiesBalance = BigDecimal.ZERO;
			BigDecimal previousDayCashBalance = BigDecimal.ZERO;
			BigDecimal previousMonthEndSecuritiesBalance = BigDecimal.ZERO;
			BigDecimal previousMonthEndCashBalance = BigDecimal.ZERO;
			if (prevBal != null) {
				previousDayCashBalance = getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, prevBal.getManagerAccount(), prevBal.getBalanceDate(), prevBal.getAdjustedCashValue());
				previousDaySecuritiesBalance = getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, prevBal.getManagerAccount(), prevBal.getBalanceDate(), prevBal.getAdjustedSecuritiesValue());
			}
			if (prevMEBal != null) {
				previousMonthEndCashBalance = getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, prevMEBal.getManagerAccount(), prevMEBal.getBalanceDate(), prevMEBal.getAdjustedCashValue());
				previousMonthEndSecuritiesBalance = getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, prevMEBal.getManagerAccount(), prevMEBal.getBalanceDate(), prevMEBal.getAdjustedSecuritiesValue());
			}

			// Setup Managers
			Map<InvestmentAccountAssetClass, ProductOverlayManagerAccount> overlayManagerMap = new HashMap<>();
			Map<InvestmentAccountAssetClass, ProductOverlayManagerAccount> rollupOverlayManagerMap = new HashMap<>();
			for (ProductOverlayManagerAllocationConfig managerAllocationConfig : CollectionUtils.getIterable(assignmentAllocationConfigEntry.getValue())) {
				if (managerAllocationConfig.getConfigType() != InvestmentManagerAllocationConfigTypes.OVERLAY_ALLOCATION) {
					ProductOverlayManagerAccount overlayManagerAccount = overlayManagerMap.get(managerAllocationConfig.getAccountAssetClass());
					if (overlayManagerAccount == null) {
						overlayManagerAccount = createProductOverlayManagerAccount(runConfig.getRun(), managerAllocationConfig);
					}
					if (managerAllocationConfig.getConfigType() == InvestmentManagerAllocationConfigTypes.CASH_ALLOCATION) {
						overlayManagerAccount.setCashAllocation(MathUtils.add(overlayManagerAccount.getCashAllocation(), managerAllocationConfig.getAmount()));
						// Reset Previous Day and Previous Month End
						//BigDecimal percentage = CoreMathUtils.getPercentValue(overlayManagerAccount.getCashAllocation(), cashBalance);
						overlayManagerAccount.setPreviousDayCashAllocation(MathUtils.getPercentageOf(managerAllocationConfig.getAllocationPercent(), previousDayCashBalance, true));
						overlayManagerAccount.setPreviousMonthEndCashAllocation(MathUtils.getPercentageOf(managerAllocationConfig.getAllocationPercent(), previousMonthEndCashBalance, true));
					}
					else {
						overlayManagerAccount.setSecuritiesAllocation(MathUtils.add(overlayManagerAccount.getSecuritiesAllocation(), managerAllocationConfig.getAmount()));

						//BigDecimal percentage = CoreMathUtils.getPercentValue(overlayManagerAccount.getSecuritiesAllocation(), securitiesBalance);

						// Proxy Value allocation
						if (assignment.getReferenceOne().isProxyManager() && !MathUtils.isNullOrZero(assignment.getReferenceOne().getProxyValue())) {
							overlayManagerAccount.setProxyValueAllocation(MathUtils.getPercentageOf(managerAllocationConfig.getAllocationPercent(), getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, assignment.getReferenceOne(), runConfig.getBalanceDate(), assignment.getReferenceOne().getProxyValue()), true));
						}

						// Reset Previous Day and Previous Month End
						overlayManagerAccount.setPreviousDaySecuritiesAllocation(MathUtils.getPercentageOf(managerAllocationConfig.getAllocationPercent(), previousDaySecuritiesBalance, true));
						overlayManagerAccount.setPreviousMonthEndSecuritiesAllocation(MathUtils.getPercentageOf(managerAllocationConfig.getAllocationPercent(), previousMonthEndSecuritiesBalance, true));
					}
					overlayManagerMap.put(managerAllocationConfig.getAccountAssetClass(), overlayManagerAccount);
					if (managerAllocationConfig.getAccountAssetClass().getParent() != null) {
						populateRollupProductOverlayManagerAccount(overlayManagerAccount, managerAllocationConfig.getConfigType(), managerAllocationConfig.getAccountAssetClass().getParent(), rollupOverlayManagerMap);
					}
				}
			}
			overlayManagerAccountList.addAll(overlayManagerMap.values());
			rollupOverlayManagerAccountList.addAll(rollupOverlayManagerMap.values());
		}
		BigDecimal cashTotal = CoreMathUtils.sumProperty(overlayManagerAccountList, ProductOverlayManagerAccount::getCashAllocation);
		runConfig.getRun().setCashTotal(cashTotal);
		runConfig.setOverlayManagerAccountList(CollectionUtils.combineCollections(overlayManagerAccountList, rollupOverlayManagerAccountList));
	}


	private ProductOverlayManagerAccount createProductOverlayManagerAccount(PortfolioRun run, ProductOverlayManagerAllocationConfig allocationConfig) {
		ProductOverlayManagerAccount overlayManager = new ProductOverlayManagerAccount();
		overlayManager.setOverlayRun(run);
		overlayManager.setManagerAccountAssignment(allocationConfig.getManagerAccountAssignment());
		overlayManager.setAccountAssetClass(allocationConfig.getAccountAssetClass());
		overlayManager.setRollupAssetClass(false);
		overlayManager.setPrivateManager(allocationConfig.getManagerAccountAllocation().isPrivateAssignment());
		// Prevent NULLs
		overlayManager.setCashAllocation(BigDecimal.ZERO);
		overlayManager.setSecuritiesAllocation(BigDecimal.ZERO);
		overlayManager.setPreviousDayCashAllocation(BigDecimal.ZERO);
		overlayManager.setPreviousDaySecuritiesAllocation(BigDecimal.ZERO);
		overlayManager.setPreviousMonthEndCashAllocation(BigDecimal.ZERO);
		overlayManager.setPreviousMonthEndSecuritiesAllocation(BigDecimal.ZERO);
		return overlayManager;
	}


	private void populateRollupProductOverlayManagerAccount(ProductOverlayManagerAccount managerAccount, InvestmentManagerAllocationConfigTypes configType, InvestmentAccountAssetClass accountAssetClass, Map<InvestmentAccountAssetClass, ProductOverlayManagerAccount> rollupOverlayManagerMap) {
		ProductOverlayManagerAccount rollupManagerAccount = rollupOverlayManagerMap.get(accountAssetClass);
		if (rollupManagerAccount == null) {
			rollupManagerAccount = BeanUtils.cloneBean(managerAccount, false, false);
			rollupManagerAccount.setAccountAssetClass(accountAssetClass);
			rollupManagerAccount.setRollupAssetClass(true);
			rollupManagerAccount.setCashAllocation(BigDecimal.ZERO);
			rollupManagerAccount.setSecuritiesAllocation(BigDecimal.ZERO);
			rollupManagerAccount.setPreviousDayCashAllocation(BigDecimal.ZERO);
			rollupManagerAccount.setPreviousDaySecuritiesAllocation(BigDecimal.ZERO);
			rollupManagerAccount.setPreviousMonthEndCashAllocation(BigDecimal.ZERO);
			rollupManagerAccount.setPreviousMonthEndSecuritiesAllocation(BigDecimal.ZERO);
			rollupManagerAccount.setProxyValueAllocation(managerAccount.getProxyValueAllocation() != null ? BigDecimal.ZERO : null);
		}

		if (InvestmentManagerAllocationConfigTypes.SECURITIES_ALLOCATION == configType) {
			rollupManagerAccount.setSecuritiesAllocation(MathUtils.add(rollupManagerAccount.getSecuritiesAllocation(), managerAccount.getSecuritiesAllocation()));
			rollupManagerAccount.setPreviousDaySecuritiesAllocation(MathUtils.add(rollupManagerAccount.getPreviousDaySecuritiesAllocation(), managerAccount.getPreviousDaySecuritiesAllocation()));
			rollupManagerAccount.setPreviousMonthEndSecuritiesAllocation(MathUtils.add(rollupManagerAccount.getPreviousMonthEndSecuritiesAllocation(), managerAccount.getPreviousMonthEndSecuritiesAllocation()));
			rollupManagerAccount.setProxyValueAllocation(MathUtils.add(rollupManagerAccount.getProxyValueAllocation(), managerAccount.getProxyValueAllocation()));
		}
		else {
			rollupManagerAccount.setCashAllocation(MathUtils.add(rollupManagerAccount.getCashAllocation(), managerAccount.getCashAllocation()));
			rollupManagerAccount.setPreviousDayCashAllocation(MathUtils.add(rollupManagerAccount.getPreviousDayCashAllocation(), managerAccount.getPreviousDayCashAllocation()));
			rollupManagerAccount.setPreviousMonthEndCashAllocation(MathUtils.add(rollupManagerAccount.getPreviousMonthEndCashAllocation(), managerAccount.getPreviousMonthEndCashAllocation()));
		}
		managerAccount.setParent(rollupManagerAccount);
		rollupOverlayManagerMap.put(accountAssetClass, rollupManagerAccount);

		if (accountAssetClass.getParent() != null) {
			populateRollupProductOverlayManagerAccount(rollupManagerAccount, configType, accountAssetClass.getParent(), rollupOverlayManagerMap);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////             Getter and Setter Methods               //////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioRunManagerAccountBalanceService<?> getPortfolioRunManagerAccountBalanceService() {
		return this.portfolioRunManagerAccountBalanceService;
	}


	public void setPortfolioRunManagerAccountBalanceService(PortfolioRunManagerAccountBalanceService<?> portfolioRunManagerAccountBalanceService) {
		this.portfolioRunManagerAccountBalanceService = portfolioRunManagerAccountBalanceService;
	}


	public ProductOverlayManagerAllocationCalculator getProductOverlayManagerAllocationCalculator() {
		return this.productOverlayManagerAllocationCalculator;
	}


	public void setProductOverlayManagerAllocationCalculator(ProductOverlayManagerAllocationCalculator productOverlayManagerAllocationCalculator) {
		this.productOverlayManagerAllocationCalculator = productOverlayManagerAllocationCalculator;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}
}
