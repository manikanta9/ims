package com.clifton.product.overlay.trade.group.dynamic.option;

import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.portfolio.run.trade.group.PortfolioRunTradeGroup;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.trade.ProductOverlayRunTrade;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionReplicationHolder;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunOptionTradeReplicationHolder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;


/**
 * <code>ProductOverlayRunOptionTradeGroup</code> is a {@link ProductOverlayRunTrade} that is specific
 * to Options processing for rebalancing cash flows.
 *
 * @author NickK
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class ProductOverlayRunOptionTradeGroup extends ProductOverlayRunTrade implements PortfolioRunTradeGroup {

	private static final String ASSET_CLASS_LABEL_OPTION_SUBSTRING = "Option";
	private static final String ASSET_CLASS_LABEL_EQUITY_SUBSTRING = "Equity";

	/**
	 * Portfolio Run Trade Group Properties
	 */
	private InvestmentAccountGroup clientAccountGroup;
	private boolean marketOnClose; // Don't think these options rolls would use MOC runs...
	private boolean validateRuns; // Option to ensure each account has a run available for trading on the balance date
	private String warningMessage;

	private ProductOverlayRunOptionTradeReplicationHolder tradeReplicationHolder;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ProductOverlayAssetClassReplication> getDetailList() {
		if (super.getDetailList() == null) {
			List<ProductOverlayAssetClassReplication> replicationList = new ArrayList<>(getOptionReplicationList());
			replicationList.addAll(getMatchingReplicationList());
			super.setDetailList(replicationList);
		}
		return super.getDetailList();
	}


	@Override
	public void setDetailList(List<ProductOverlayAssetClassReplication> detailList) {
		super.setDetailList(detailList);
		if (!CollectionUtils.isEmpty(detailList)) {
			List<ProductOverlayAssetClassReplication> optionReplicationList = new ArrayList<>(),
					matchingReplicationList = new ArrayList<>();
			detailList.forEach(replication -> {
				if (replication.getOverlayAssetClass().getLabel().contains(ASSET_CLASS_LABEL_OPTION_SUBSTRING)) {
					optionReplicationList.add(replication);
				}
				else {
					matchingReplicationList.add(replication);
				}
			});
			setOptionReplicationList(optionReplicationList);
			setMatchingReplicationList(matchingReplicationList);
		}
	}


	public List<ProductOverlayAssetClassReplication> getOptionReplicationList() {
		if (getTradeReplicationHolder() == null) {
			return Collections.emptyList();
		}
		ProductOverlayRunOptionReplicationHolder replicationHolder = ProductOverlayRunOptionReplicationHolder.of(getRunOptionTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutReplicationHolder));
		replicationHolder.addAll(getRunOptionTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallReplicationHolder));
		BeanUtils.sortWithFunctions(replicationHolder.getReplicationList(),
				CollectionUtils.createList(ProductOverlayAssetClassReplication::getOverlayAssetClass, replication -> replication.getSecurity().getLastDeliveryDate()),
				CollectionUtils.createList(false, true));
		return replicationHolder.getReplicationList();
	}


	public void setOptionReplicationList(List<ProductOverlayAssetClassReplication> optionReplicationList) {
		ProductOverlayRunOptionReplicationHolder putHolder = getRunOptionTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutReplicationHolder);
		putHolder.clear();
		ProductOverlayRunOptionReplicationHolder callHolder = getRunOptionTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallReplicationHolder);
		callHolder.clear();

		CollectionUtils.asNonNullList(optionReplicationList).forEach(replication -> {
			if (replication.getOverlayAssetClass() != null && replication.getOverlayAssetClass().getLabel().contains(ASSET_CLASS_LABEL_EQUITY_SUBSTRING)) {
				callHolder.add(replication);
			}
			else {
				putHolder.add(replication);
			}
		});
	}


	public List<ProductOverlayAssetClassReplication> getMatchingReplicationList() {
		if (getTradeReplicationHolder() == null) {
			return Collections.emptyList();
		}
		ProductOverlayRunOptionReplicationHolder replicationHolder = ProductOverlayRunOptionReplicationHolder.of(getRunOptionTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutMatchingReplicationHolder));
		replicationHolder.addAll(getRunOptionTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallMatchingReplicationHolder));
		return replicationHolder.getReplicationList();
	}


	public void setMatchingReplicationList(List<ProductOverlayAssetClassReplication> matchingReplicationList) {
		ProductOverlayRunOptionReplicationHolder putMatchingHolder = getRunOptionTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutMatchingReplicationHolder);
		putMatchingHolder.clear();
		ProductOverlayRunOptionReplicationHolder callMatchingHolder = getRunOptionTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallMatchingReplicationHolder);
		callMatchingHolder.clear();

		CollectionUtils.asNonNullList(matchingReplicationList).forEach(replication -> {
			if (replication.getOverlayAssetClass() != null && replication.getOverlayAssetClass().getLabel().contains(ASSET_CLASS_LABEL_EQUITY_SUBSTRING)) {
				callMatchingHolder.add(replication);
			}
			else {
				putMatchingHolder.add(replication);
			}
		});
	}


	@Override
	public ServiceProcessingTypes getProcessingType() {
		return ServiceProcessingTypes.PORTFOLIO_RUNS;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ProductOverlayRunOptionTradeReplicationHolder getTradeReplicationHolder() {
		return this.tradeReplicationHolder;
	}


	@ValueChangingSetter
	public void setTradeReplicationHolder(ProductOverlayRunOptionTradeReplicationHolder tradeReplicationHolder) {
		this.tradeReplicationHolder = tradeReplicationHolder;
		if (tradeReplicationHolder != null && tradeReplicationHolder.getPortfolioRun() != null) {
			BeanUtils.copyProperties(tradeReplicationHolder.getPortfolioRun(), this);
		}
	}


	private <R> R getRunOptionTradeReplicationHolderValue(Function<ProductOverlayRunOptionTradeReplicationHolder, R> valueGetter) {
		return ObjectUtils.map(getTradeReplicationHolder(), valueGetter)
				// If not initialized, initialize the replication holder and try again
				.orElseMap(new ProductOverlayRunOptionTradeReplicationHolder(), replicationHolder -> {
					setTradeReplicationHolder(replicationHolder);
					return valueGetter.apply(getTradeReplicationHolder());
				}).get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccountGroup getClientAccountGroup() {
		return this.clientAccountGroup;
	}


	public void setClientAccountGroup(InvestmentAccountGroup clientAccountGroup) {
		this.clientAccountGroup = clientAccountGroup;
	}


	@Override
	public boolean isMarketOnClose() {
		return this.marketOnClose;
	}


	public void setMarketOnClose(boolean marketOnClose) {
		this.marketOnClose = marketOnClose;
	}


	@Override
	public boolean isValidateRuns() {
		return this.validateRuns;
	}


	public void setValidateRuns(boolean validateRuns) {
		this.validateRuns = validateRuns;
	}


	@Override
	public String getWarningMessage() {
		return this.warningMessage;
	}


	@Override
	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}


	public BigDecimal getPutMatchingAdditionalExposure() {
		return getRunOptionTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getPutMatchingAdditionalExposure);
	}


	public BigDecimal getCallMatchingExposureCurrent() {
		return getRunOptionTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallMatchingReplicationHolder).getExposureCurrent();
	}


	public BigDecimal getCallMatchingAdditionalExposure() {
		return getRunOptionTradeReplicationHolderValue(ProductOverlayRunOptionTradeReplicationHolder::getCallMatchingAdditionalExposure);
	}
}
