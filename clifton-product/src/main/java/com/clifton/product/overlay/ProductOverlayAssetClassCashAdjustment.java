package com.clifton.product.overlay;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.manager.ManagerCashTypes;

import java.math.BigDecimal;


/**
 * The <code>ProductOverlayAssetClassCashAdjustment</code> holds cash bucket transfers from
 * one cash bucket to another - Used for Cash Attribution when either current configuration doesn't
 * allow cash to be put in the correct bucket, or for cleaning up history
 *
 * @author manderson
 */
public class ProductOverlayAssetClassCashAdjustment extends BaseEntity<Integer> {

	private ProductOverlayAssetClass overlayAssetClass;
	/**
	 * Cash bucket cash is removed from
	 */
	private ManagerCashTypes fromCashType;

	/**
	 * Cash bucket cash is added to
	 */
	private ManagerCashTypes toCashType;

	/**
	 * Amount to transfer
	 */
	private BigDecimal amount;

	/**
	 * Comment on why cash is being moved
	 */
	private String note;


	public ProductOverlayAssetClass getOverlayAssetClass() {
		return this.overlayAssetClass;
	}


	public void setOverlayAssetClass(ProductOverlayAssetClass overlayAssetClass) {
		this.overlayAssetClass = overlayAssetClass;
	}


	public ManagerCashTypes getFromCashType() {
		return this.fromCashType;
	}


	public void setFromCashType(ManagerCashTypes fromCashType) {
		this.fromCashType = fromCashType;
	}


	public ManagerCashTypes getToCashType() {
		return this.toCashType;
	}


	public void setToCashType(ManagerCashTypes toCashType) {
		this.toCashType = toCashType;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}
}
