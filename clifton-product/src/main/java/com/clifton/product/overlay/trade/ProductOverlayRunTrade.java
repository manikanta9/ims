package com.clifton.product.overlay.trade;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.portfolio.run.trade.PortfolioCurrencyTrade;
import com.clifton.portfolio.run.trade.PortfolioRunTrade;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;

import java.util.List;


/**
 * The <code>ProductOverlayRunTrade</code> ...
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class ProductOverlayRunTrade extends PortfolioRunTrade<ProductOverlayAssetClassReplication> {

	//////////////////////////////////////////////////////////////////////////
	// Not Persisted in the DB - Used for Trade Creation/View Screens ONLY
	//////////////////////////////////////////////////////////////////////////

	private boolean excludeMispricing;

	// Currency List for the Run.
	private List<PortfolioCurrencyTrade<ProductOverlayAssetClassReplication>> currencyTradeList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<PortfolioCurrencyTrade<ProductOverlayAssetClassReplication>> getCurrencyTradeList() {
		return this.currencyTradeList;
	}


	public void setCurrencyTradeList(List<PortfolioCurrencyTrade<ProductOverlayAssetClassReplication>> currencyTradeList) {
		this.currencyTradeList = currencyTradeList;
	}


	public boolean isExcludeMispricing() {
		return this.excludeMispricing;
	}


	public void setExcludeMispricing(boolean excludeMispricing) {
		this.excludeMispricing = excludeMispricing;
	}
}
