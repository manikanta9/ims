package com.clifton.product.overlay.manager;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalance;


/**
 * {@link ProductOverlayManagerAccountBalance} object is a virtual object that contains a combination of information about the run
 * as well as manager balance information and manual adjustments.
 *
 * @author michaelm
 */
@NonPersistentObject
public class ProductOverlayManagerAccountBalance extends PortfolioRunManagerAccountBalance {

	public static final String BALANCE_PRINT_HEADER = "MOC\tManager #\tAsset Class\tAllocation\tBase Securities\tBase Cash\tBase Total\tAdjustment Count\tSecurities Adjustment\tCash Adjustment\tAdjusted Securities\tAdjusted Cash\tAdjusted Total\r";
	public static final String BALANCE_PRINT_FORMAT = "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\r";

	/**
	 * Used for PIOS where each manager is also allocated to asset classes
	 */
	private InvestmentAccountAssetClass accountAssetClass;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toStringFormatted(boolean includeHeader) {
		String result = "";
		if (includeHeader) {
			result = BALANCE_PRINT_HEADER;
		}

		return result
				+ String.format(BALANCE_PRINT_FORMAT,
				getPortfolioRun().isMarketOnCloseAdjustmentsIncluded(),
				getManagerAccountBalance().getManagerAccount().getAccountNumber(),
				(getAccountAssetClass() == null ? "" : getAccountAssetClass().getLabel()),
				CoreMathUtils.formatNumberMoney(getAllocationPercent()),
				CoreMathUtils.formatNumberInteger(getSecuritiesBalanceBaseAllocationAmount()),
				CoreMathUtils.formatNumberInteger(getCashBalanceBaseAllocationAmount()),
				CoreMathUtils.formatNumberInteger(getTotalBalanceBaseAllocationAmount()),
				CollectionUtils.getSize(getManualAdjustmentList()),
				CoreMathUtils.formatNumberInteger(getSecuritiesManualAdjustmentTotalAllocationAmount()),
				CoreMathUtils.formatNumberInteger(getCashManualAdjustmentTotalAllocationAmount()),
				CoreMathUtils.formatNumberInteger(getSecuritiesAllocationAmount()),
				CoreMathUtils.formatNumberInteger(getCashAllocationAmount()),
				CoreMathUtils.formatNumberInteger(getTotalAllocationAmount()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountAssetClass getAccountAssetClass() {
		return this.accountAssetClass;
	}


	public void setAccountAssetClass(InvestmentAccountAssetClass accountAssetClass) {
		this.accountAssetClass = accountAssetClass;
	}
}
