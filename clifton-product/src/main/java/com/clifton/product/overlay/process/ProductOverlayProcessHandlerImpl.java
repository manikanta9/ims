package com.clifton.product.overlay.process;


import com.clifton.core.util.BooleanUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.process.BasePortfolioRunProcessHandlerImpl;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.util.ProductUtilServiceImpl;


/**
 * The <code>ProductOverlayProcessHandlerImpl</code> ...
 *
 * @author Mary Anderson
 */
public class ProductOverlayProcessHandlerImpl extends BasePortfolioRunProcessHandlerImpl<ProductOverlayRunConfig> {

	private InvestmentAccountAssetClassService investmentAccountAssetClassService;

	private ProductOverlayService productOverlayService;


	////////////////////////////////////////////////////////////////////////////
	////////                 Run Setup Methods                          ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected ProductOverlayRunConfig setupPortfolioRunConfig(PortfolioRun run, boolean reloadProxyManagers, int currentDepth) {
		ProductOverlayRunConfig runConfig = new ProductOverlayRunConfig(run, reloadProxyManagers, currentDepth);
		populatePortfolioRunConfig(runConfig);

		// Account Custom Field Options
		runConfig.setExcludeMispricing(BooleanUtils.isTrue(getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(runConfig.getRun().getClientInvestmentAccount(), ProductUtilServiceImpl.INVESTMENT_ACCOUNT_EXCLUDE_MISPRICING_FIELD_NAME)));
		runConfig.setDoNotAdjustReplicationAllocationPercentages(BooleanUtils.isTrue(getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(runConfig.getRun().getClientInvestmentAccount(), ProductUtilServiceImpl.INVESTMENT_ACCOUNT_DO_NOT_ADJUST_REPLICATION_ALLOCATION_PERCENTAGES)));

		// Get a list of all ACTIVE InvestmentAccountAssetClasses
		runConfig.setAssetClassList(getInvestmentAccountAssetClassService().getInvestmentAccountAssetClassListByAccount(runConfig.getClientInvestmentAccountId()));
		return runConfig;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Run Clear/Delete Methods                        ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void deletePortfolioRunProcessing(int runId) {
		getProductOverlayService().deleteProductOverlayRunProcessing(runId);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}
}
