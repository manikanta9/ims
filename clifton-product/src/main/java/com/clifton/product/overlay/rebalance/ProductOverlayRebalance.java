package com.clifton.product.overlay.rebalance;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalance;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceHistory;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClass;

import java.util.Date;
import java.util.List;


/**
 * The <code>ProductOverlayRebalance</code> is a virtual DTO that contains
 * PIOS run and Rebalance specific fields for supporting manual rebalancing
 *
 * @author manderson
 */
@NonPersistentObject
public class ProductOverlayRebalance {

	private int overlayRunId;

	private PortfolioRun overlayRun;
	private InvestmentAccountRebalance rebalanceConfig;

	/**
	 * Most recent re-balance performed (compared to today, not balance date)
	 */
	private InvestmentAccountRebalanceHistory lastRebalance;

	private Date rebalanceDate;

	private List<ProductOverlayAssetClass> rollupAssetClassList;
	private List<ProductOverlayAssetClass> rebalanceAssetClassList;

	/**
	 * Action to perform for re-balance when saving from UI
	 */
	private ProductOverlayRebalanceActions action;


	public boolean isReadOnly() {
		if (getOverlayRun() != null) {
			return getOverlayRun().isReadOnly();
		}
		return false;
	}


	public boolean isUndoLastAllowed() {
		if (!isReadOnly() && getLastRebalance() != null && DateUtils.compare(getLastRebalance().getRebalanceDate(), getRebalanceDate(), false) == 0) {
			return true;
		}
		return false;
	}


	public PortfolioRun getOverlayRun() {
		return this.overlayRun;
	}


	public void setOverlayRun(PortfolioRun overlayRun) {
		this.overlayRun = overlayRun;
	}


	public InvestmentAccountRebalance getRebalanceConfig() {
		return this.rebalanceConfig;
	}


	public void setRebalanceConfig(InvestmentAccountRebalance rebalanceConfig) {
		this.rebalanceConfig = rebalanceConfig;
	}


	public List<ProductOverlayAssetClass> getRebalanceAssetClassList() {
		return this.rebalanceAssetClassList;
	}


	public void setRebalanceAssetClassList(List<ProductOverlayAssetClass> rebalanceAssetClassList) {
		this.rebalanceAssetClassList = rebalanceAssetClassList;
	}


	public InvestmentAccountRebalanceHistory getLastRebalance() {
		return this.lastRebalance;
	}


	public void setLastRebalance(InvestmentAccountRebalanceHistory lastRebalance) {
		this.lastRebalance = lastRebalance;
	}


	public Date getRebalanceDate() {
		return this.rebalanceDate;
	}


	public void setRebalanceDate(Date rebalanceDate) {
		this.rebalanceDate = rebalanceDate;
	}


	public ProductOverlayRebalanceActions getAction() {
		return this.action;
	}


	public void setAction(ProductOverlayRebalanceActions action) {
		this.action = action;
	}


	public int getOverlayRunId() {
		return this.overlayRunId;
	}


	public void setOverlayRunId(int overlayRunId) {
		this.overlayRunId = overlayRunId;
	}


	public List<ProductOverlayAssetClass> getRollupAssetClassList() {
		return this.rollupAssetClassList;
	}


	public void setRollupAssetClassList(List<ProductOverlayAssetClass> rollupAssetClassList) {
		this.rollupAssetClassList = rollupAssetClassList;
	}
}
