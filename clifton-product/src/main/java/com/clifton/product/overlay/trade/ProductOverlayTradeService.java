package com.clifton.product.overlay.trade;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.trade.PortfolioRunTradeService;

import java.util.List;


/**
 * The <code>ProductOverlayTradeService</code> defines Overlay Specific Trade method(s).  All general lookup/save methods are defined in {@link PortfolioRunTradeService} and implemented in
 * {@link ProductOverlayTradeHandlerImpl}
 *
 * @author Mary Anderson
 */
public interface ProductOverlayTradeService {

	////////////////////////////////////////////////////////////////////////////
	/////    Product Overlay Run Trade Creation Business Methods          //////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * When Trading enabled (i.e. Run isn't completed) will generate the map for the replication dependencies
	 * that tells the Run Trade Screen how to update Targets based on trades entered/m2m adjustments, etc.
	 *
	 * @param runId
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public List<ProductOverlayAssetClassReplicationTradeTargetDependency> getProductOverlayAssetClassReplicationTradeTargetDependencyList(int runId);
}
