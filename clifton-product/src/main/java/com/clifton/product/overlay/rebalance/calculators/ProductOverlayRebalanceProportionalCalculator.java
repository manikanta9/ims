package com.clifton.product.overlay.rebalance.calculators;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.rebalance.ProductOverlayRebalance;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>ProductOverlayRebalanceProportionalCalculator</code> ...
 *
 * @author manderson
 */
public class ProductOverlayRebalanceProportionalCalculator extends BaseProductOverlayRebalanceCalculatorImpl {

	@Override
	public void calculateNewRebalanceCash(ProductOverlayRebalance bean) {
		BigDecimal total = CoreMathUtils.sumProperty(bean.getRebalanceAssetClassList(), ProductOverlayAssetClass::getRebalanceCashAdjusted);

		BigDecimal absTotal = CoreMathUtils.sumProperty(bean.getRebalanceAssetClassList(), ProductOverlayAssetClass::getRebalanceCashAdjustedAbs);

		// Offset total as the actual adjustment we need to apply to bring back to zero.
		BigDecimal totalAdjustment = MathUtils.negate(total);

		for (ProductOverlayAssetClass assetClass : CollectionUtils.getIterable(bean.getRebalanceAssetClassList())) {
			assetClass.setNewRebalanceCash(MathUtils.add(assetClass.getRebalanceCashAdjusted(),
					MathUtils.getPercentageOf(CoreMathUtils.getPercentValue(assetClass.getRebalanceCashAdjustedAbs(), absTotal, true), totalAdjustment, true)));
		}
		// Fix Rounding
		CoreMathUtils.applySumPropertyDifference(bean.getRebalanceAssetClassList(), ProductOverlayAssetClass::getNewRebalanceCash, ProductOverlayAssetClass::setNewRebalanceCash, BigDecimal.ZERO, true);
	}
}
