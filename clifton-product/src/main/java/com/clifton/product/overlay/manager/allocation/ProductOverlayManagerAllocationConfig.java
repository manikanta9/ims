package com.clifton.product.overlay.manager.allocation;

import com.clifton.core.util.AssertUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.manager.InvestmentManagerAccountAllocation;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAccountAssignmentOverlayAction;
import com.clifton.investment.manager.InvestmentManagerAllocationConfigTypes;
import com.clifton.product.overlay.process.calculators.ProductOverlayManagerCalculator;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>{@link ProductOverlayManagerAllocationConfig}</code> class is built during {@link ProductOverlayManagerCalculator} processing to define where for each
 * {@link com.clifton.investment.manager.InvestmentManagerAccountAssignment} and {@link com.clifton.investment.manager.InvestmentManagerAccountAllocation}s where
 * cash is allocated, securities are allocations, and cash is overlaid
 *
 * @author manderson
 */
public class ProductOverlayManagerAllocationConfig {


	private final InvestmentManagerAllocationConfigTypes configType;

	private final InvestmentManagerAccountAllocation managerAccountAllocation;

	private final InvestmentAccountAssetClass accountAssetClass;

	private BigDecimal allocationPercent;

	private BigDecimal amount;

	private InvestmentManagerAccountAssignmentOverlayAction overlayAdjustmentAction;

	private BigDecimal overlayAdjustment;

	private InvestmentManagerAccountAssignmentOverlayAction overlayLimitAction;

	private BigDecimal overlayLimit;

	/**
	 * Special case for custom allocations that allow allocating the adjustment offset to
	 * Fund Cash bucket and weights
	 */
	private boolean benchmarkOffsetAdjustment;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountAssignment getManagerAccountAssignment() {
		return getManagerAccountAllocation().getManagerAccountAssignment();
	}


	public int getOrder() {
		if (getManagerAccountAllocation().isCustomCashOverlay()) {
			return 1;
		}
		return 2;
	}


	/**
	 * Returns the overlay amount after any adjustments and/or limits are applied
	 */
	public BigDecimal getOverlayAmount() {
		BigDecimal adjustedOverlayAmount = MathUtils.add(getAmount(), getOverlayAdjustment());
		return getOverlayLimit() == null ? adjustedOverlayAmount : MathUtils.min(adjustedOverlayAmount, getOverlayLimit());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static ProductOverlayManagerAllocationConfig ofSecuritiesAllocation(InvestmentManagerAccountAllocation managerAccountAllocation, InvestmentAccountAssetClass accountAssetClass) {
		return new ProductOverlayManagerAllocationConfig(managerAccountAllocation, accountAssetClass, InvestmentManagerAllocationConfigTypes.SECURITIES_ALLOCATION);
	}


	public static ProductOverlayManagerAllocationConfig ofCashAllocation(InvestmentManagerAccountAllocation managerAccountAllocation, InvestmentAccountAssetClass accountAssetClass) {
		return new ProductOverlayManagerAllocationConfig(managerAccountAllocation, accountAssetClass, InvestmentManagerAllocationConfigTypes.CASH_ALLOCATION);
	}


	public static ProductOverlayManagerAllocationConfig ofOverlayAllocation(InvestmentManagerAccountAllocation managerAccountAllocation, InvestmentAccountAssetClass accountAssetClass) {
		return new ProductOverlayManagerAllocationConfig(managerAccountAllocation, accountAssetClass, InvestmentManagerAllocationConfigTypes.OVERLAY_ALLOCATION);
	}


	private ProductOverlayManagerAllocationConfig(InvestmentManagerAccountAllocation managerAccountAllocation, InvestmentAccountAssetClass accountAssetClass, InvestmentManagerAllocationConfigTypes configType) {
		super();
		this.managerAccountAllocation = managerAccountAllocation;
		this.accountAssetClass = accountAssetClass;
		this.configType = configType;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Populates the overlay actions and their calculated adjustments and/or limits where applies
	 *
	 * @param overlayAdjustmentAction
	 * @param overlayLimitAction
	 * @param managerTotalAllocation  - Total Cash Allocation + Securities Allocation for THIS manager assignment
	 * @param totalMarketValue        - Total Cash Allocation + Securities Allocation for ALL Manager Assignments
	 */
	public void populateOverlayAdjustmentsAndLimits(InvestmentManagerAccountAssignmentOverlayAction overlayAdjustmentAction, InvestmentManagerAccountAssignmentOverlayAction overlayLimitAction, BigDecimal managerTotalAllocation, BigDecimal totalMarketValue) {
		AssertUtils.assertEquals(InvestmentManagerAllocationConfigTypes.OVERLAY_ALLOCATION, this.getConfigType(), "Overlay Actions Apply only to Overlay Allocations.");
		this.overlayAdjustmentAction = overlayAdjustmentAction;
		this.overlayLimitAction = overlayLimitAction;
		// If Custom Cash Overlay Specific Amounts, don't apply any actions
		if (getManagerAccountAllocation().isCustomCashOverlay() && !getManagerAccountAssignment().getCustomCashOverlayType().isApplyValuesAsPercentage()) {
			return;
		}
		if (overlayAdjustmentAction != null) {
			// Create adjustment amount as a percentage of total market value
			if (overlayAdjustmentAction.getOverlayActionPercent() != null) {
				this.overlayAdjustment = MathUtils.getPercentageOf(overlayAdjustmentAction.getOverlayActionPercent(), totalMarketValue, true);
			}
			// Otherwise - static amount
			else {
				this.overlayAdjustment = getOverlayAdjustmentAction().getOverlayActionAmount();
			}
			// Allocate it
			this.overlayAdjustment = MathUtils.getPercentageOf(getAllocationPercent(), this.overlayAdjustment, true);
		}
		if (overlayLimitAction != null) {
			BigDecimal max = overlayLimitAction.getOverlayActionAmount();
			if (overlayLimitAction.getOverlayActionPercent() != null) {
				BigDecimal percentMax = MathUtils.getPercentageOf(overlayLimitAction.getOverlayActionPercent(), managerTotalAllocation, true);
				// Set max value to the minimum of the max values
				max = MathUtils.min(max, percentMax);
			}
			// Allocate it
			this.overlayLimit = MathUtils.getPercentageOf(getAllocationPercent(), max, true);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAllocationConfigTypes getConfigType() {
		return this.configType;
	}


	public InvestmentManagerAccountAllocation getManagerAccountAllocation() {
		return this.managerAccountAllocation;
	}


	public InvestmentAccountAssetClass getAccountAssetClass() {
		return this.accountAssetClass;
	}


	public BigDecimal getAllocationPercent() {
		return this.allocationPercent;
	}


	public void setAllocationPercent(BigDecimal allocationPercent) {
		this.allocationPercent = allocationPercent;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public InvestmentManagerAccountAssignmentOverlayAction getOverlayAdjustmentAction() {
		return this.overlayAdjustmentAction;
	}


	public InvestmentManagerAccountAssignmentOverlayAction getOverlayLimitAction() {
		return this.overlayLimitAction;
	}


	public BigDecimal getOverlayAdjustment() {
		return this.overlayAdjustment;
	}


	public BigDecimal getOverlayLimit() {
		return this.overlayLimit;
	}


	public boolean isBenchmarkOffsetAdjustment() {
		return this.benchmarkOffsetAdjustment;
	}


	public void setBenchmarkOffsetAdjustment(boolean benchmarkOffsetAdjustment) {
		this.benchmarkOffsetAdjustment = benchmarkOffsetAdjustment;
	}
}
