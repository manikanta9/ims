package com.clifton.product.overlay;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.hierarchy.HierarchicalSimpleEntity;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.product.overlay.search.ProductOverlayAssetClassSearchForm;
import com.clifton.product.overlay.search.ProductOverlayManagerAccountSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductOverlayServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class ProductOverlayServiceImpl implements ProductOverlayService {

	private AdvancedUpdatableDAO<ProductOverlayManagerAccount, Criteria> productOverlayManagerAccountDAO;
	private AdvancedUpdatableDAO<ProductOverlayManagerAccountRebalance, Criteria> productOverlayManagerAccountRebalanceDAO;
	private AdvancedUpdatableDAO<ProductOverlayAssetClass, Criteria> productOverlayAssetClassDAO;
	private AdvancedUpdatableDAO<ProductOverlayAssetClassCashAdjustment, Criteria> productOverlayAssetClassCashAdjustmentDAO;
	private AdvancedUpdatableDAO<ProductOverlayAssetClassReplication, Criteria> productOverlayAssetClassReplicationDAO;

	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	private PortfolioRunService portfolioRunService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void deleteProductOverlayRunProcessing(int id) {
		// NOTE: MARGIN DELETING IS IN MAIN COMMON DELETE METHOD IN PORTFOLIO RUN PROCESS SERVICE
		getPortfolioRunService().deletePortfolioCurrencyOtherListByRun(id);
		getProductOverlayAssetClassReplicationDAO().deleteList(getProductOverlayAssetClassReplicationListByRun(id));
		getProductOverlayAssetClassCashAdjustmentDAO().deleteList(getProductOverlayAssetClassCashAdjustmentListByRun(id));
		getProductOverlayAssetClassDAO().deleteList(getProductOverlayAssetClassListByRun(id));
		getProductOverlayManagerAccountDAO().deleteList(getProductOverlayManagerAccountListByRun(id));
		getProductOverlayManagerAccountRebalanceDAO().deleteList(getProductOverlayManagerAccountRebalanceListByRun(id));
	}


	/**
	 * Checks if there exists at least 1 (because of sub-accounts there could be 2) main run for an account for the given balance date
	 * uses {@link ProductOverlayAssetClass} to check coalesce (asset class account, run account)
	 */
	@Override
	public boolean isProductOverlayMainRunExistsForAccount(final int clientInvestmentAccountId, final Date balanceDate) {
		HibernateSearchConfigurer searchConfig = criteria -> {
			criteria.createAlias("overlayRun", "run");

			Criterion lhs = Restrictions.and(Restrictions.eq("run.clientInvestmentAccount.id", clientInvestmentAccountId), Restrictions.isNull("positionInvestmentAccount.id"));
			Criterion rhs = Restrictions.eq("positionInvestmentAccount.id", clientInvestmentAccountId);
			criteria.add(Restrictions.or(lhs, rhs));

			criteria.add(Restrictions.eq("run.mainRun", true));
			criteria.add(Restrictions.eq("run.balanceDate", balanceDate));
		};
		return !CollectionUtils.isEmpty(getProductOverlayAssetClassDAO().findBySearchCriteria(searchConfig));
	}


	////////////////////////////////////////////////////////////////////////////
	/////         Product Overlay Manager Account Business Methods       ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ProductOverlayManagerAccount getProductOverlayManagerAccount(int id) {
		ProductOverlayManagerAccount bean = getProductOverlayManagerAccountDAO().findByPrimaryKey(id);
		if (bean != null) {
			bean.setBalance(getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(bean.getManagerAccountAssignment().getReferenceOne().getId(),
					bean.getOverlayRun().getBalanceDate(), true));
		}
		return bean;
	}


	@Override
	public List<ProductOverlayManagerAccount> getProductOverlayManagerAccountListByRun(final int runId) {
		ProductOverlayManagerAccountSearchForm searchForm = new ProductOverlayManagerAccountSearchForm();
		searchForm.setRunId(runId);
		return getProductOverlayManagerAccountList(searchForm);
	}


	@Override
	public List<ProductOverlayManagerAccount> getProductOverlayManagerAccountList(final ProductOverlayManagerAccountSearchForm searchForm) {
		// withCashValuesOnly is a custom search restriction that has special restrictions
		// remove it because there is no definition attached to it.
		if (searchForm.containsSearchRestriction("withCashValuesOnly")) {
			searchForm.setWithCashValuesOnly((Boolean) searchForm.getSearchRestriction("withCashValuesOnly").getValue());
			searchForm.removeSearchRestriction("withCashValuesOnly");
		}

		if (searchForm.containsSearchRestriction("ourCompanyAssignment")) {
			searchForm.setOurCompanyAssignment((Boolean) searchForm.getSearchRestriction("ourCompanyAssignment").getValue());
			searchForm.removeSearchRestriction("ourCompanyAssignment");
		}

		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (BooleanUtils.isTrue(searchForm.getWithCashValuesOnly())) {
					criteria.add(Restrictions.or(Restrictions.ne("cashAllocation", BigDecimal.ZERO), Restrictions.ne("previousDayCashAllocation", BigDecimal.ZERO)));
				}

				if (BooleanUtils.isTrue(searchForm.getOurCompanyAssignment())) {
					String alias = getPathAlias("managerAccountAssignment.referenceOne.managerCompany.type", criteria);
					criteria.add(Restrictions.eq(alias + ".ourCompany", true));
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				// If no sorting, or sorting by asset class - use custom sort
				List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(getSortableSearchForm().getOrderBy());
				if (CollectionUtils.isEmpty(orderByList) || (CollectionUtils.getSize(orderByList) == 1 && StringUtils.isEqual("assetClassName", orderByList.get(0).getName()))) {
					criteria.addOrder(Order.asc(getPathAlias("accountAssetClass", criteria) + ".order"));
					criteria.addOrder(Order.asc(getPathAlias("accountAssetClass.assetClass", criteria) + ".name"));
					criteria.addOrder(Order.asc(getPathAlias("managerAccountAssignment.referenceOne.managerCompany", criteria) + ".name"));
					return true;
				}
				// Otherwise do field sorting
				return super.configureOrderBy(criteria);
			}
		};
		return getProductOverlayManagerAccountDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	@Transactional
	public void saveProductOverlayManagerAccountListByRun(int runId, BigDecimal totalCash, List<ProductOverlayManagerAccount> saveList) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		List<ProductOverlayManagerAccount> originalList = getProductOverlayManagerAccountListByRun(runId);
		// Sort in Level Order so All parents are saved first
		saveList.sort(Comparator.comparingInt(HierarchicalSimpleEntity::getLevel));
		getProductOverlayManagerAccountDAO().saveList(saveList, originalList);
		if (CollectionUtils.isEmpty(saveList)) {
			run.setCashTotal(null);
		}
		else {
			run.setCashTotal(totalCash);
		}
		getPortfolioRunService().savePortfolioRun(run);
	}


	////////////////////////////////////////////////////////////////////////////
	/////  Product Overlay Manager Account Rebalance Business Methods      /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ProductOverlayManagerAccountRebalance getProductOverlayManagerAccountRebalance(int id) {
		return getProductOverlayManagerAccountRebalanceDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ProductOverlayManagerAccountRebalance> getProductOverlayManagerAccountRebalanceListByRun(int runId) {
		return getProductOverlayManagerAccountRebalanceDAO().findByField("overlayRun.id", runId);
	}


	@Override
	public void saveProductOverlayManagerAccountRebalanceListByRun(int runId, List<ProductOverlayManagerAccountRebalance> saveList) {
		List<ProductOverlayManagerAccountRebalance> originalList = getProductOverlayManagerAccountRebalanceListByRun(runId);
		getProductOverlayManagerAccountRebalanceDAO().saveList(saveList, originalList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////     Product Overlay Asset Class Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ProductOverlayAssetClass getProductOverlayAssetClass(int id) {
		return getProductOverlayAssetClassDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ProductOverlayAssetClass> getProductOverlayAssetClassListByRun(int runId) {
		ProductOverlayAssetClassSearchForm searchForm = new ProductOverlayAssetClassSearchForm();
		searchForm.setRunId(runId);
		return getProductOverlayAssetClassList(searchForm, false);
	}


	@Override
	public List<ProductOverlayAssetClass> getProductOverlayAssetClassList(ProductOverlayAssetClassSearchForm searchForm, boolean populateCashAdjustments) {
		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public boolean configureOrderBy(Criteria criteria) {
				criteria.addOrder(Order.asc(getPathAlias("accountAssetClass", criteria) + ".order"));
				criteria.addOrder(Order.asc(getPathAlias("accountAssetClass.assetClass", criteria) + ".name"));
				return true;
			}
		};
		List<ProductOverlayAssetClass> list = getProductOverlayAssetClassDAO().findBySearchCriteria(searchConfig);
		if (populateCashAdjustments) {
			populateCashAdjustments(list, searchForm.getRunId());
		}
		return list;
	}


	private void populateCashAdjustments(List<ProductOverlayAssetClass> list, int runId) {
		List<ProductOverlayAssetClassCashAdjustment> adjList = getProductOverlayAssetClassCashAdjustmentListByRun(runId);

		// Need to go through all, because if an asset class has a rollup, or rollup of a rollup the adjustment needs to be applied to all
		// Adjustments are applied to the child only
		for (ProductOverlayAssetClassCashAdjustment adj : CollectionUtils.getIterable(adjList)) {
			for (ProductOverlayAssetClass ac : CollectionUtils.getIterable(list)) {
				if (adj.getOverlayAssetClass().equals(ac)) {
					ac.addCashAdjustmentToList(adj);
				}
				else {
					ProductOverlayAssetClass parent = (ProductOverlayAssetClass) adj.getOverlayAssetClass().getParent();
					while (parent != null) {
						if (parent.equals(ac)) {
							ac.addCashAdjustmentToList(adj);
						}
						parent = (ProductOverlayAssetClass) parent.getParent();
					}
				}
			}
		}
	}


	@Override
	@Transactional
	public void saveProductOverlayAssetClassListByRun(int runId, BigDecimal totalPortfolioValue, List<ProductOverlayAssetClass> saveList) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		List<ProductOverlayAssetClass> originalList = getProductOverlayAssetClassListByRun(runId);
		// Sort in Level Order so All parents are saved first
		saveList.sort(Comparator.comparingInt(HierarchicalSimpleEntity::getLevel));
		getProductOverlayAssetClassDAO().saveList(saveList, originalList);
		if (CollectionUtils.isEmpty(saveList)) {
			run.setPortfolioTotalValue(null);
		}
		else {
			run.setPortfolioTotalValue(totalPortfolioValue);
		}
		getPortfolioRunService().savePortfolioRun(run);
	}


	////////////////////////////////////////////////////////////////////////////
	////  Product Overlay Asset Class Cash Adjustment Business Methods     /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ProductOverlayAssetClassCashAdjustment getProductOverlayAssetClassCashAdjustment(int id) {
		return getProductOverlayAssetClassCashAdjustmentDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ProductOverlayAssetClassCashAdjustment> getProductOverlayAssetClassCashAdjustmentListByRun(final int runId) {
		HibernateSearchConfigurer searchConfig = criteria -> {
			criteria.createAlias("overlayAssetClass", "oa");
			criteria.add(Restrictions.eq("oa.overlayRun.id", runId));
		};

		return getProductOverlayAssetClassCashAdjustmentDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public ProductOverlayAssetClassCashAdjustment saveProductOverlayAssetClassCashAdjustment(ProductOverlayAssetClassCashAdjustment bean) {
		return saveProductOverlayAssetClassAdjustmentImpl(bean, true);
	}


	@Override
	public ProductOverlayAssetClassCashAdjustment saveProductOverlayAssetClassCashAdjustmentAllowNegative(ProductOverlayAssetClassCashAdjustment bean) {
		return saveProductOverlayAssetClassAdjustmentImpl(bean, false);
	}


	private ProductOverlayAssetClassCashAdjustment saveProductOverlayAssetClassAdjustmentImpl(ProductOverlayAssetClassCashAdjustment bean, boolean validateNegativeResult) {
		validateProductOverlayAssetClassAdjustment(bean, false, validateNegativeResult);
		return getProductOverlayAssetClassCashAdjustmentDAO().save(bean);
	}


	@Override
	public void deleteProductOverlayAssetClassCashAdjustment(int id) {
		deleteProductOverlayAssetClassAdjustmentImpl(id, true);
	}


	@Override
	public void deleteProductOverlayAssetClassCashAdjustmentAllowNegative(int id) {
		deleteProductOverlayAssetClassAdjustmentImpl(id, false);
	}


	private void deleteProductOverlayAssetClassAdjustmentImpl(int id, boolean validateNegativeResult) {
		ProductOverlayAssetClassCashAdjustment bean = getProductOverlayAssetClassCashAdjustment(id);
		if (bean != null) {
			validateProductOverlayAssetClassAdjustment(bean, true, validateNegativeResult);
			getProductOverlayAssetClassCashAdjustmentDAO().delete(bean.getId());
		}
	}


	private void validateProductOverlayAssetClassAdjustment(ProductOverlayAssetClassCashAdjustment bean, boolean delete, boolean validateNegativeResult) {
		if (!delete) {
			ValidationUtils.assertNotNull(bean.getOverlayAssetClass(), "Asset Class Selection is required.");
			ValidationUtils.assertFalse(bean.getOverlayAssetClass().isRollupAssetClass(), "Asset Class Selected cannot be a rollup asset class");
			ValidationUtils.assertNotNull(bean.getFromCashType(), "From Cash Type is required.");
			ValidationUtils.assertNotNull(bean.getToCashType(), "To Cash Type is required.");
			ValidationUtils.assertFalse(bean.getFromCashType() == bean.getToCashType(), "From and To Cash Types must be different");
		}
		if (validateNegativeResult) {
			// Fund, Manager & Transition Cash are the only buckets that we care about negatives
			EnumMap<ManagerCashTypes, BigDecimal> cashTypeAmounts = new EnumMap<>(ManagerCashTypes.class);
			cashTypeAmounts.put(ManagerCashTypes.FUND, bean.getOverlayAssetClass().getFundCash());
			cashTypeAmounts.put(ManagerCashTypes.MANAGER, bean.getOverlayAssetClass().getManagerCash());
			cashTypeAmounts.put(ManagerCashTypes.TRANSITION, bean.getOverlayAssetClass().getTransitionCash());

			List<ProductOverlayAssetClassCashAdjustment> adjustmentList = getProductOverlayAssetClassCashAdjustmentDAO().findByField("overlayAssetClass.id", bean.getOverlayAssetClass().getId());
			for (ProductOverlayAssetClassCashAdjustment adj : CollectionUtils.getIterable(adjustmentList)) {
				if (adj.equals(bean)) {
					// Skip this one
					continue;
				}
				applyAdjustmentToCashTypeMap(adj, cashTypeAmounts);
			}

			if (!delete) {
				applyAdjustmentToCashTypeMap(bean, cashTypeAmounts);
			}

			for (Map.Entry<ManagerCashTypes, BigDecimal> managerCashTypesBigDecimalEntry : cashTypeAmounts.entrySet()) {
				if (MathUtils.isLessThan(managerCashTypesBigDecimalEntry.getValue(), BigDecimal.ZERO)) {
					throw new UserIgnorableValidationException(ProductOverlayServiceImpl.class, (delete ? "deleteProductOverlayAssetClassCashAdjustmentAllowNegative" : "saveProductOverlayAssetClassCashAdjustmentAllowNegative"), "Unable to [" + (delete ? "delete" : "enter") + "] adjustment because it will make " + (managerCashTypesBigDecimalEntry.getKey()).getLabel()
							+ " Cash negative which is not allowed for attribution purposes.");
				}
			}
		}
	}


	private void applyAdjustmentToCashTypeMap(ProductOverlayAssetClassCashAdjustment adj, EnumMap<ManagerCashTypes, BigDecimal> cashTypeAmounts) {
		if (cashTypeAmounts.containsKey(adj.getFromCashType())) {
			cashTypeAmounts.put(adj.getFromCashType(), MathUtils.subtract(cashTypeAmounts.get(adj.getFromCashType()), adj.getAmount()));
		}
		if (cashTypeAmounts.containsKey(adj.getToCashType())) {
			cashTypeAmounts.put(adj.getToCashType(), MathUtils.add(cashTypeAmounts.get(adj.getToCashType()), adj.getAmount()));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	/////    Product Overlay Asset Class Replication Business Methods     //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ProductOverlayAssetClassReplication getProductOverlayAssetClassReplication(int id) {
		return getProductOverlayAssetClassReplicationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ProductOverlayAssetClassReplication> getProductOverlayAssetClassReplicationListByOverlayAssetClass(int overlayAssetClassId) {
		return getProductOverlayAssetClassReplicationList(null, overlayAssetClassId);
	}


	@Override
	public List<ProductOverlayAssetClassReplication> getProductOverlayAssetClassReplicationListByRun(int runId) {
		return getProductOverlayAssetClassReplicationList(runId, null);
	}


	@Override
	public List<ProductOverlayAssetClassReplication> getProductOverlayAssetClassReplicationListByAccountMainRun(final int investmentAccountId, final Date startDate, final Date endDate, final boolean excludeMatchingReplications) {

		List<ProductOverlayAssetClassReplication> subAccountReplicationList = getProductOverlayAssetClassReplicationListByAccountMainRunImpl(investmentAccountId, true, startDate, endDate, excludeMatchingReplications);
		List<ProductOverlayAssetClassReplication> mainAccountReplicationList = getProductOverlayAssetClassReplicationListByAccountMainRunImpl(investmentAccountId, false, startDate, endDate, excludeMatchingReplications);

		/// If just one or the other, then use the populated one
		if (CollectionUtils.isEmpty(mainAccountReplicationList)) {
			return subAccountReplicationList;
		}
		if (CollectionUtils.isEmpty(subAccountReplicationList)) {
			return mainAccountReplicationList;
		}

		/* Otherwise - if we have both, need to check each date - if main for that date is missing we use the sub-account
		  So, Alliant where duplicated, only the main account runs will be used
		  Another case where this is needed - Pepperdine switched over from an account that was the main account, to creating a new main account, and the original main account is a subaccount
		  Since billing is often for the quarter, we need to pick up these changes where possible, instead of having to go back in time and recreate runs (which is not always accurate or possible if you go too far back)
		  then analysts would need to verify each day's report matched original.  This way we pick up the shift when it happens
		 */
		Map<Date, List<ProductOverlayAssetClassReplication>> mainMap = BeanUtils.getBeansMap(mainAccountReplicationList, "overlayAssetClass.overlayRun.balanceDate");
		Map<Date, List<ProductOverlayAssetClassReplication>> positionMap = BeanUtils.getBeansMap(subAccountReplicationList, "overlayAssetClass.overlayRun.balanceDate");

		for (Map.Entry<Date, List<ProductOverlayAssetClassReplication>> dateListEntry : positionMap.entrySet()) {
			if (mainMap.containsKey(dateListEntry.getKey())) {
				continue;
			}
			// Add all Position Accounting Account where Main Account Run Doesn't exist on the date.
			mainAccountReplicationList.addAll(dateListEntry.getValue());
		}

		return mainAccountReplicationList;
	}


	private List<ProductOverlayAssetClassReplication> getProductOverlayAssetClassReplicationListByAccountMainRunImpl(final int investmentAccountId, final boolean subAccount, final Date startDate, final Date endDate,
	                                                                                                                 final boolean excludeMatchingReplications) {

		HibernateSearchConfigurer searchConfig = criteria -> {
			criteria.createAlias("overlayAssetClass", "oac");
			criteria.createAlias("oac.overlayRun", "run");

			if (subAccount) {
				criteria.add(Restrictions.eq("oac.positionInvestmentAccount.id", investmentAccountId));
			}
			else {
				criteria.add(Restrictions.isNull("oac.positionInvestmentAccount.id"));
				criteria.add(Restrictions.eq("run.clientInvestmentAccount.id", investmentAccountId));
			}
			criteria.add(Restrictions.eq("run.mainRun", true));
			if (startDate != null) {
				criteria.add(Restrictions.ge("run.balanceDate", startDate));
			}
			if (endDate != null) {
				criteria.add(Restrictions.le("run.balanceDate", endDate));
			}
			if (excludeMatchingReplications) {
				criteria.add(Restrictions.or(Restrictions.eqProperty("replication.id", "oac.primaryReplication.id"), Restrictions.and(Restrictions.isNotNull("oac.secondaryReplication.id"), Restrictions.eqProperty("replication.id", "oac.secondaryReplication.id"))));
			}
		};
		return getProductOverlayAssetClassReplicationDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public List<ProductOverlayAssetClassReplication> getProductOverlayAssetClassReplicationList(final Integer runId, final Integer overlayAssetClassId) {
		ValidationUtils.assertTrue((runId == null) != (overlayAssetClassId == null), "At least one of runId or overlayAssetClass is required, but not both");
		HibernateSearchConfigurer searchConfig = criteria -> {
			criteria.createAlias("overlayAssetClass", "oac");
			if (overlayAssetClassId != null) {
				criteria.add(Restrictions.eq("oac.id", overlayAssetClassId));
			}
			else {
				criteria.add(Restrictions.eq("oac.overlayRun.id", runId));
			}
			criteria.createAlias("security", "sec");
			criteria.createAlias("oac.accountAssetClass", "aac");
			criteria.createAlias("aac.assetClass", "ac");
			criteria.createAlias("replication", "rp");
			criteria.createAlias("rp.type", "rpt");
			criteria.createAlias("replicationAllocationHistory", "rah");
			criteria.createAlias("rah.replicationAllocation", "ra");
			criteria.addOrder(Order.asc("aac.order"));
			criteria.addOrder(Order.asc("ac.name"));
			criteria.addOrder(Order.asc("rpt.order"));
			criteria.addOrder(Order.asc("replication.id"));
			criteria.addOrder(Order.asc("ra.order"));
			criteria.addOrder(Order.asc("sec.endDate"));
		};
		return getProductOverlayAssetClassReplicationDAO().findBySearchCriteria(searchConfig);
	}


	/**
	 * NOTE: SPECIAL COPY METHOD IN {@ProductReplicationService} to add new securities to the replication
	 * METHOD IS THERE BECAUSE OF SECURITY SETUP/VALIDATION METHODS THAT ARE MORE PROCESSING DEPENDENT
	 */
	@Override
	public ProductOverlayAssetClassReplication saveProductOverlayAssetClassReplication(ProductOverlayAssetClassReplication bean) {
		return getProductOverlayAssetClassReplicationDAO().save(bean);
	}


	@Override
	public void saveProductOverlayAssetClassReplicationList(List<ProductOverlayAssetClassReplication> saveList) {
		getProductOverlayAssetClassReplicationDAO().saveList(saveList);
	}


	@Override
	@Transactional
	public void saveProductOverlayAssetClassReplicationListByRun(int runId, BigDecimal overlayTargetTotal, BigDecimal overlayExposureTotal, BigDecimal mispricingTotal,
	                                                             List<ProductOverlayAssetClassReplication> saveList) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		List<ProductOverlayAssetClassReplication> originalList = getProductOverlayAssetClassReplicationListByRun(runId);
		getProductOverlayAssetClassReplicationDAO().saveList(saveList, originalList);
		if (CollectionUtils.isEmpty(saveList)) {
			run.setOverlayTargetTotal(null);
			run.setOverlayExposureTotal(null);
			run.setMispricingTotal(null);
		}
		else {
			run.setOverlayTargetTotal(overlayTargetTotal);
			run.setOverlayExposureTotal(overlayExposureTotal);
			run.setMispricingTotal(mispricingTotal);
		}
		getPortfolioRunService().savePortfolioRun(run);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ProductOverlayManagerAccount, Criteria> getProductOverlayManagerAccountDAO() {
		return this.productOverlayManagerAccountDAO;
	}


	public void setProductOverlayManagerAccountDAO(AdvancedUpdatableDAO<ProductOverlayManagerAccount, Criteria> productOverlayManagerAccountDAO) {
		this.productOverlayManagerAccountDAO = productOverlayManagerAccountDAO;
	}


	public AdvancedUpdatableDAO<ProductOverlayAssetClass, Criteria> getProductOverlayAssetClassDAO() {
		return this.productOverlayAssetClassDAO;
	}


	public void setProductOverlayAssetClassDAO(AdvancedUpdatableDAO<ProductOverlayAssetClass, Criteria> productOverlayAssetClassDAO) {
		this.productOverlayAssetClassDAO = productOverlayAssetClassDAO;
	}


	public AdvancedUpdatableDAO<ProductOverlayAssetClassReplication, Criteria> getProductOverlayAssetClassReplicationDAO() {
		return this.productOverlayAssetClassReplicationDAO;
	}


	public void setProductOverlayAssetClassReplicationDAO(AdvancedUpdatableDAO<ProductOverlayAssetClassReplication, Criteria> productOverlayAssetClassReplicationDAO) {
		this.productOverlayAssetClassReplicationDAO = productOverlayAssetClassReplicationDAO;
	}


	public AdvancedUpdatableDAO<ProductOverlayManagerAccountRebalance, Criteria> getProductOverlayManagerAccountRebalanceDAO() {
		return this.productOverlayManagerAccountRebalanceDAO;
	}


	public void setProductOverlayManagerAccountRebalanceDAO(AdvancedUpdatableDAO<ProductOverlayManagerAccountRebalance, Criteria> productOverlayManagerAccountRebalanceDAO) {
		this.productOverlayManagerAccountRebalanceDAO = productOverlayManagerAccountRebalanceDAO;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public AdvancedUpdatableDAO<ProductOverlayAssetClassCashAdjustment, Criteria> getProductOverlayAssetClassCashAdjustmentDAO() {
		return this.productOverlayAssetClassCashAdjustmentDAO;
	}


	public void setProductOverlayAssetClassCashAdjustmentDAO(AdvancedUpdatableDAO<ProductOverlayAssetClassCashAdjustment, Criteria> productOverlayAssetClassCashAdjustmentDAO) {
		this.productOverlayAssetClassCashAdjustmentDAO = productOverlayAssetClassCashAdjustmentDAO;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}
}
