package com.clifton.product.overlay.manager.allocation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAccountAllocation;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAccountAssignmentOverlayAction;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.InvestmentManagerAllocationConfigTypes;
import com.clifton.investment.manager.ManagerCashOverlayActionTypes;
import com.clifton.investment.manager.ManagerCashOverlayTypes;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.search.InvestmentManagerAccountAssignmentOverlayActionSearchForm;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalanceService;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Component
public class ProductOverlayManagerAllocationCalculatorImpl implements ProductOverlayManagerAllocationCalculator {


	private InvestmentManagerAccountService investmentManagerAccountService;

	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	private MarketDataRetriever marketDataRetriever;

	private PortfolioRunManagerAccountBalanceService<?> portfolioRunManagerAccountBalanceService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void processProductOverlayManagerAllocationConfigList(ProductOverlayRunConfig runConfig, boolean preview) {
		// Get Active Assignments Only
		List<InvestmentManagerAccountAssignment> assignmentList = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentListByAccount(runConfig.getClientInvestmentAccountId(), true);
		if (CollectionUtils.isEmpty(assignmentList)) {
			// If it's a preview (rules) - do nothing
			if (preview) {
				return;
			}
			throw new ValidationExceptionWithCause("InvestmentAccount", runConfig.getClientInvestmentAccountId(), "There are no active Manager Account Assignments assigned to account ["
					+ runConfig.getRun().getClientInvestmentAccount().getLabel() + "].  Unable to process Portfolio run for this account.");
		}

		for (InvestmentManagerAccountAssignment assign : CollectionUtils.getIterable(assignmentList)) {
			BigDecimal cashBalance;
			BigDecimal securitiesBalance;

			// Balance Date's Value
			InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(assign.getReferenceOne().getId(),
					runConfig.getBalanceDate(), false);
			// Warnings should catch this, but just in case to avoid NullPointerException
			if (balance == null) {
				if (preview) {
					cashBalance = BigDecimal.ZERO;
					securitiesBalance = BigDecimal.ZERO;
				}
				else {
					throw new ValidationExceptionWithCause("InvestmentManagerAccount", assign.getReferenceOne().getId(), "Balance for date: [" + runConfig.getBalanceDate()
							+ "] is missing for manager account [" + assign.getReferenceOne().getAccountNumber() + "]");
				}
			}
			else {
				if (runConfig.getRun().isMarketOnCloseAdjustmentsIncluded()) {
					cashBalance = getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, balance.getManagerAccount(), balance.getBalanceDate(), balance.getMarketOnCloseCashValue());
					securitiesBalance = getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, balance.getManagerAccount(), balance.getBalanceDate(), balance.getMarketOnCloseSecuritiesValue());
				}
				else {
					cashBalance = getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, balance.getManagerAccount(), balance.getBalanceDate(), balance.getAdjustedCashValue());
					securitiesBalance = getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, balance.getManagerAccount(), balance.getBalanceDate(), balance.getAdjustedSecuritiesValue());
				}
			}

			// Determines the the full allocation list for the assignment (including custom cash allocations)
			List<ProductOverlayManagerAllocationConfig> managerAllocationConfigList = processProductOverlayManagerAccountAllocations(runConfig, assign, runConfig.getAssetClassList(), cashBalance, securitiesBalance);
			runConfig.getManagerAllocationConfigList().addAll(managerAllocationConfigList);
		}
		// Look up all Manager Assignment Overlay Actions for the Account Once - and put them in the map for easy look ups
		processManagerAssignmentOverlayActions(runConfig);
	}


	private List<ProductOverlayManagerAllocationConfig> processProductOverlayManagerAccountAllocations(ProductOverlayRunConfig runConfig, InvestmentManagerAccountAssignment
			assignment, List<InvestmentAccountAssetClass> investmentAccountAssetClassList, BigDecimal cashBalance, BigDecimal securitiesBalance) {
		// Get assignment by id so allocation list is populated
		assignment = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignment(assignment.getId());

		List<ProductOverlayManagerAllocationConfig> managerAllocationConfigList = getProductOverlayManagerAllocationConfigListForManagerAssignment(runConfig, assignment, investmentAccountAssetClassList);

		// Set Security Allocation Values First
		List<ProductOverlayManagerAllocationConfig> securitiesManagerAllocationConfigList = BeanUtils.filter(managerAllocationConfigList, ProductOverlayManagerAllocationConfig::getConfigType, InvestmentManagerAllocationConfigTypes.SECURITIES_ALLOCATION);
		for (ProductOverlayManagerAllocationConfig securitiesManagerAllocationConfig : CollectionUtils.getIterable(securitiesManagerAllocationConfigList)) {
			securitiesManagerAllocationConfig.setAmount(MathUtils.getPercentageOf(securitiesManagerAllocationConfig.getManagerAccountAllocation().getAllocationPercent(), securitiesBalance, true));
		}

		// Then Cash Allocation Next - Ordered so Custom Ones are Processed First
		List<ProductOverlayManagerAllocationConfig> cashManagerAllocationConfigList = BeanUtils.sortWithFunction(BeanUtils.filter(managerAllocationConfigList, ProductOverlayManagerAllocationConfig::getConfigType, InvestmentManagerAllocationConfigTypes.CASH_ALLOCATION), ProductOverlayManagerAllocationConfig::getOrder, true);
		BigDecimal cashAllocationTotal = cashBalance;
		for (ProductOverlayManagerAllocationConfig cashManagerAllocationConfig : CollectionUtils.getIterable(cashManagerAllocationConfigList)) {
			if (cashManagerAllocationConfig.getManagerAccountAllocation().isCustomCashOverlay()) {
				if (MathUtils.isNullOrZero(cashManagerAllocationConfig.getAmount())) {
					cashManagerAllocationConfig.setAmount(MathUtils.getPercentageOf(cashManagerAllocationConfig.getAllocationPercent(), cashBalance, true));
				}
				cashAllocationTotal = MathUtils.subtract(cashAllocationTotal, cashManagerAllocationConfig.getAmount());
			}
			else {
				cashManagerAllocationConfig.setAmount(MathUtils.getPercentageOf(cashManagerAllocationConfig.getAllocationPercent(), cashAllocationTotal, true));
			}
		}

		// Then Cash Overlay Next - Ordered so Custom Ones are Processed First
		List<ProductOverlayManagerAllocationConfig> cashOverlayManagerAllocationConfigList = BeanUtils.sortWithFunction(BeanUtils.filter(managerAllocationConfigList, ProductOverlayManagerAllocationConfig::getConfigType, InvestmentManagerAllocationConfigTypes.OVERLAY_ALLOCATION), ProductOverlayManagerAllocationConfig::getOrder, true);
		BigDecimal cashOverlayTotal = cashBalance;
		for (ProductOverlayManagerAllocationConfig cashManagerAllocationConfig : CollectionUtils.getIterable(cashOverlayManagerAllocationConfigList)) {
			if (cashManagerAllocationConfig.getManagerAccountAllocation().isCustomCashOverlay()) {
				if (MathUtils.isNullOrZero(cashManagerAllocationConfig.getAmount())) {
					cashManagerAllocationConfig.setAmount(MathUtils.getPercentageOf(cashManagerAllocationConfig.getAllocationPercent(), cashBalance, true));
				}
				cashOverlayTotal = MathUtils.subtract(cashOverlayTotal, cashManagerAllocationConfig.getAmount());
			}
			else {
				cashManagerAllocationConfig.setAmount(MathUtils.getPercentageOf(cashManagerAllocationConfig.getAllocationPercent(), cashOverlayTotal, true));
			}
		}
		return managerAllocationConfigList;
	}


	private List<ProductOverlayManagerAllocationConfig> getProductOverlayManagerAllocationConfigListForManagerAssignment(ProductOverlayRunConfig runConfig, InvestmentManagerAccountAssignment managerAccountAssignment, List<InvestmentAccountAssetClass> investmentAccountAssetClassList) {
		List<ProductOverlayManagerAllocationConfig> managerAllocationConfigList = new ArrayList<>();

		List<InvestmentManagerAccountAllocation> customCashAllocationList = managerAccountAssignment.getCustomCashOverlayAllocationList();
		List<InvestmentManagerAccountAllocation> allocationList = managerAccountAssignment.getAllocationList();

		BigDecimal overlayCashPercent = MathUtils.BIG_DECIMAL_ONE_HUNDRED;
		for (InvestmentManagerAccountAllocation allocation : CollectionUtils.getIterable(customCashAllocationList)) {
			InvestmentAccountAssetClass accountAssetClass = getInvestmentAccountAssetClassForAllocation(allocation, investmentAccountAssetClassList);
			ProductOverlayManagerAllocationConfig managerCashOverlayAllocationConfig = ProductOverlayManagerAllocationConfig.ofOverlayAllocation(allocation, accountAssetClass);
			BigDecimal customAllocationValueAdjustment = getCustomAllocationValueAdjustment(runConfig, accountAssetClass, allocation);
			BigDecimal adjustedCustomAllocationValue = MathUtils.add(allocation.getCustomAllocationValue(), customAllocationValueAdjustment);
			if (managerAccountAssignment.getCustomCashOverlayType().isApplyValuesAsPercentage()) {
				managerCashOverlayAllocationConfig.setAllocationPercent(adjustedCustomAllocationValue);
				// Remaining percentage for default rules if applies
				overlayCashPercent = MathUtils.subtract(overlayCashPercent, adjustedCustomAllocationValue);
			}
			else {
				managerCashOverlayAllocationConfig.setAmount(adjustedCustomAllocationValue);
			}
			managerAllocationConfigList.add(managerCashOverlayAllocationConfig);

			// Only other option is Fund - otherwise goes to Default Rules
			if (!MathUtils.isNullOrZero(customAllocationValueAdjustment) && allocation.getBenchmarkAdjustmentOffSetCashOverlayType() == ManagerCashOverlayTypes.FUND) {
				ProductOverlayManagerAllocationConfig offSetManagerCashOverlayAllocationConfig = ProductOverlayManagerAllocationConfig.ofOverlayAllocation(allocation, accountAssetClass);
				offSetManagerCashOverlayAllocationConfig.setBenchmarkOffsetAdjustment(true);
				if (managerAccountAssignment.getCustomCashOverlayType().isApplyValuesAsPercentage()) {
					offSetManagerCashOverlayAllocationConfig.setAllocationPercent(MathUtils.negate(customAllocationValueAdjustment));
					// Remaining percentage for default rules if applies
					overlayCashPercent = MathUtils.subtract(overlayCashPercent, MathUtils.negate(customAllocationValueAdjustment));
				}
				else {
					offSetManagerCashOverlayAllocationConfig.setAmount(MathUtils.negate(customAllocationValueAdjustment));
				}
				managerAllocationConfigList.add(offSetManagerCashOverlayAllocationConfig);
			}

			if (managerAccountAssignment.getCustomCashAllocationType().isAllocateUsingCustomCashAssetClasses()) {
				ProductOverlayManagerAllocationConfig managerCashAllocationConfig = ProductOverlayManagerAllocationConfig.ofCashAllocation(allocation, accountAssetClass);
				if (managerAccountAssignment.getCustomCashOverlayType().isApplyValuesAsPercentage()) {
					managerCashAllocationConfig.setAllocationPercent(adjustedCustomAllocationValue);
				}
				else {
					managerCashAllocationConfig.setAmount(adjustedCustomAllocationValue);
				}
				managerAllocationConfigList.add(managerCashAllocationConfig);
			}
		}
		for (InvestmentManagerAccountAllocation allocation : CollectionUtils.getIterable(allocationList)) {
			InvestmentAccountAssetClass accountAssetClass = getInvestmentAccountAssetClassForAllocation(allocation, investmentAccountAssetClassList);
			// Add Securities Allocation:
			ProductOverlayManagerAllocationConfig securitiesManagerAllocationConfig = ProductOverlayManagerAllocationConfig.ofSecuritiesAllocation(allocation, accountAssetClass);
			securitiesManagerAllocationConfig.setAllocationPercent(allocation.getAllocationPercent());
			managerAllocationConfigList.add(securitiesManagerAllocationConfig);

			// Cash Allocation (If Applies)
			if (managerAccountAssignment.getCustomCashAllocationType() == null || managerAccountAssignment.getCustomCashAllocationType().isAllocateUsingManagerAssetClasses() || managerAccountAssignment.getCashOverlayType() != ManagerCashOverlayTypes.NONE) {
				ProductOverlayManagerAllocationConfig cashManagerAllocationConfig = ProductOverlayManagerAllocationConfig.ofCashAllocation(allocation, accountAssetClass);
				cashManagerAllocationConfig.setAllocationPercent(allocation.getAllocationPercent());
				managerAllocationConfigList.add(cashManagerAllocationConfig);
			}
			// Cash Overlay (If Applies)
			if (managerAccountAssignment.getCashOverlayType() != ManagerCashOverlayTypes.NONE) {
				ProductOverlayManagerAllocationConfig managerCashOverlayAllocationConfig = ProductOverlayManagerAllocationConfig.ofOverlayAllocation(allocation, accountAssetClass);
				managerCashOverlayAllocationConfig.setAllocationPercent(MathUtils.getPercentageOf(allocation.getAllocationPercent(), overlayCashPercent, true));
				managerAllocationConfigList.add(managerCashOverlayAllocationConfig);
			}
		}
		return managerAllocationConfigList;
	}


	private BigDecimal getCustomAllocationValueAdjustment(ProductOverlayRunConfig runConfig, InvestmentAccountAssetClass accountAssetClass, InvestmentManagerAccountAllocation
			allocation) {
		if (!allocation.isApplyBenchmarkAdjustment()) {
			return BigDecimal.ZERO;
		}
		InvestmentSecurity benchmarkSecurity = (allocation.getBenchmarkSecurity() != null) ? allocation.getBenchmarkSecurity() : accountAssetClass.getBenchmarkSecurity();
		if (benchmarkSecurity == null) {
			throw new ValidationException("Cannot calculate benchmark adjusted custom cash manager allocation for manager [" + allocation.getManagerAccountAssignment().getReferenceOne().getLabel() + "] because benchmark security selection is missing on the allocation and the asset class [" + accountAssetClass.getLabel() + "]");
		}
		BigDecimal adjustmentPercent = getMarketDataRetriever().getInvestmentSecurityReturnFlexible(allocation.getManagerAccountAssignment().getReferenceTwo(), benchmarkSecurity, allocation.getCustomAllocationValueDate(), runConfig.getBalanceDate(), "Cannot calculate benchmark adjusted custom cash manager allocation for manager [" + allocation.getManagerAccountAssignment().getReferenceOne().getLabel() + "]", false);
		return MathUtils.getPercentageOf(adjustmentPercent, allocation.getCustomAllocationValue());
	}


	private InvestmentAccountAssetClass getInvestmentAccountAssetClassForAllocation(InvestmentManagerAccountAllocation
			                                                                                allocation, List<InvestmentAccountAssetClass> investmentAccountAssetClassList) {
		InvestmentAssetClass ac = allocation.getAssetClass();
		InvestmentAccountAssetClass iac = CollectionUtils.getOnlyElement(BeanUtils.filter(investmentAccountAssetClassList, InvestmentAccountAssetClass::getAssetClass, ac));
		if (iac == null) {
			throw new ValidationExceptionWithCause("InvestmentAccount", allocation.getManagerAccountAssignment().getReferenceTwo().getId(), "Investment Account [" + allocation.getManagerAccountAssignment().getReferenceTwo().getLabel()
					+ "] is missing asset class [" + ac.getName() + "] which is used by manager [" + allocation.getManagerAccountAssignment().getReferenceOne().getLabel()
					+ "].  Please either add this asset class to the account, or change the manager allocation to use a different asset class.");
		}
		if (iac.isRollupAssetClass()) {
			throw new ValidationExceptionWithCause("InvestmentManagerAccount", allocation.getManagerAccountAssignment().getReferenceOne().getId(), "Manager Account [" + allocation.getManagerAccountAssignment().getReferenceOne().getLabel()
					+ "] is incorrectly assigned to a rollup asset class [" + allocation.getAssetClass().getName() + "]. Manager Assignments cannot be directly tied to a rollup asset class.");
		}
		return iac;
	}


	/**
	 * For the account, populates the maps of InvestmentManagerAccountAssignmentID -> InvestmentManagerAccountAssignmentOverlayAction
	 * for adjustments and limits.  At most there can be only one of each active on a date for a manager assignment (additional validation, although already validated during saves)
	 * <p>
	 * Look up once - all for the account, create a map to find each manager's options as each assignment is processed
	 */
	private void processManagerAssignmentOverlayActions(ProductOverlayRunConfig runConfig) {
		// Look up all Manager Assignment Overlay Actions for the Account Once
		InvestmentManagerAccountAssignmentOverlayActionSearchForm managerAssignmentOverlayActionSearchForm = new InvestmentManagerAccountAssignmentOverlayActionSearchForm();
		managerAssignmentOverlayActionSearchForm.setInvestmentAccountId(runConfig.getClientInvestmentAccountId());
		managerAssignmentOverlayActionSearchForm.setActiveOnDate(runConfig.getBalanceDate());
		List<InvestmentManagerAccountAssignmentOverlayAction> managerAssignmentOverlayActionList = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentOverlayActionList(
				managerAssignmentOverlayActionSearchForm);

		if (!CollectionUtils.isEmpty(managerAssignmentOverlayActionList)) {
			BigDecimal totalMarketValue = CoreMathUtils.sumProperty(runConfig.getProductOverlayManagerAllocationConfigListExcludeConfigType(InvestmentManagerAllocationConfigTypes.OVERLAY_ALLOCATION), ProductOverlayManagerAllocationConfig::getAmount);

			// Populate all of the maps and validate the overlay actions
			Map<InvestmentManagerAccountAssignment, InvestmentManagerAccountAssignmentOverlayAction> managerAssignmentOverlayAdjustmentMap = new HashMap<>();
			Map<InvestmentManagerAccountAssignment, InvestmentManagerAccountAssignmentOverlayAction> managerAssignmentOverlayLimitMap = new HashMap<>();
			Map<InvestmentManagerAccountAssignment, List<ProductOverlayManagerAllocationConfig>> managerAssignmentOverlayAllocationConfigMap = new HashMap<>();
			for (InvestmentManagerAccountAssignmentOverlayAction action : CollectionUtils.getIterable(managerAssignmentOverlayActionList)) {
				InvestmentManagerAccountAssignment assignment = action.getManagerAccountAssignment();
				if (!managerAssignmentOverlayAllocationConfigMap.containsKey(assignment)) {
					managerAssignmentOverlayAllocationConfigMap.put(assignment, BeanUtils.filter(runConfig.getProductOverlayManagerAllocationConfigListForAssignment(assignment), ProductOverlayManagerAllocationConfig::getConfigType, InvestmentManagerAllocationConfigTypes.OVERLAY_ALLOCATION));
				}

				if (ManagerCashOverlayActionTypes.ADJUSTMENT == action.getOverlayActionType()) {
					if (managerAssignmentOverlayAdjustmentMap.get(assignment) != null) {
						throw new ValidationExceptionWithCause("InvestmentManagerAccountAssignment", assignment.getId(), "Manager Assignment [" + action.getManagerAccountAssignment().getLabel()
								+ "] has multiple cash overlay adjustment actions active on [" + DateUtils.fromDateShort(runConfig.getBalanceDate())
								+ "].  There can only be one active adjustment action for a manager assignment at a time.");
					}
					managerAssignmentOverlayAdjustmentMap.put(assignment, action);
				}
				else {
					if (managerAssignmentOverlayLimitMap.get(assignment) != null) {
						throw new ValidationExceptionWithCause("InvestmentManagerAccountAssignment", assignment.getId(), "Manager Assignment [" + action.getManagerAccountAssignment().getLabel()
								+ "] has multiple cash overlay limit actions active on [" + DateUtils.fromDateShort(runConfig.getBalanceDate())
								+ "].  There can only be one active limit action for a manager assignment at a time.");
					}
					managerAssignmentOverlayLimitMap.put(assignment, action);
				}
			}

			// Now go through the assignments with actions and adjust the overlay amounts
			for (Map.Entry<InvestmentManagerAccountAssignment, List<ProductOverlayManagerAllocationConfig>> assignmentListEntry : CollectionUtils.getIterable(managerAssignmentOverlayAllocationConfigMap.entrySet())) {
				List<ProductOverlayManagerAllocationConfig> managerAllocationConfigList = BeanUtils.sortWithFunction(assignmentListEntry.getValue(), ProductOverlayManagerAllocationConfig::getOrder, true);
				InvestmentManagerAccountAssignmentOverlayAction adjustmentAction = managerAssignmentOverlayAdjustmentMap.get(assignmentListEntry.getKey());
				InvestmentManagerAccountAssignmentOverlayAction limitAction = managerAssignmentOverlayLimitMap.get(assignmentListEntry.getKey());
				BigDecimal managerTotalAllocation = CoreMathUtils.sumProperty(BeanUtils.filter(runConfig.getProductOverlayManagerAllocationConfigListForAssignment(assignmentListEntry.getKey()), managerAllocationConfig -> managerAllocationConfig.getConfigType() != InvestmentManagerAllocationConfigTypes.OVERLAY_ALLOCATION), ProductOverlayManagerAllocationConfig::getAmount);
				for (ProductOverlayManagerAllocationConfig managerAllocationConfig : managerAllocationConfigList) {
					managerAllocationConfig.populateOverlayAdjustmentsAndLimits(adjustmentAction, limitAction, managerTotalAllocation, totalMarketValue);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods              ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public PortfolioRunManagerAccountBalanceService<?> getPortfolioRunManagerAccountBalanceService() {
		return this.portfolioRunManagerAccountBalanceService;
	}


	public void setPortfolioRunManagerAccountBalanceService(PortfolioRunManagerAccountBalanceService<?> portfolioRunManagerAccountBalanceService) {
		this.portfolioRunManagerAccountBalanceService = portfolioRunManagerAccountBalanceService;
	}
}
