package com.clifton.product.overlay.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceHistory;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceService;
import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;
import com.clifton.investment.account.assetclass.rebalance.search.InvestmentAccountRebalanceHistorySearchForm;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductOverlayManualRebalanceRuleEvaluator</code> generates a violation when a manual rebalance was performance on
 * the run's balance date and if so, who performed the manual rebalance (if there are multiple, will report who performed the last MANUAL one)
 *
 * @author manderson
 */
public class ProductOverlayManualRebalanceRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, ProductOverlayRuleEvaluatorContext> {

	private InvestmentAccountRebalanceService investmentAccountRebalanceService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, ProductOverlayRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(run.getClientInvestmentAccount()));
		if (entityConfig != null && !entityConfig.isExcluded()) {
			InvestmentAccountRebalanceHistorySearchForm searchForm = new InvestmentAccountRebalanceHistorySearchForm();
			searchForm.setInvestmentAccountId(run.getClientInvestmentAccount().getId());
			searchForm.setRebalanceDate(run.getBalanceDate());
			searchForm.setRebalanceCalculationType(RebalanceCalculationTypes.MANUAL);
			searchForm.setOrderBy("rebalanceDate:DESC");
			searchForm.setLimit(1);
			InvestmentAccountRebalanceHistory history = CollectionUtils.getFirstElement(getInvestmentAccountRebalanceService().getInvestmentAccountRebalanceHistoryList(searchForm));
			if (history != null) {
				Map<String, Object> templateValues = new HashMap<>();
				templateValues.put("createUser", context.getSecurityUser(history.getCreateUserId()));
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), history.getId(), null, templateValues));
			}
		}
		return ruleViolationList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRebalanceService getInvestmentAccountRebalanceService() {
		return this.investmentAccountRebalanceService;
	}


	public void setInvestmentAccountRebalanceService(InvestmentAccountRebalanceService investmentAccountRebalanceService) {
		this.investmentAccountRebalanceService = investmentAccountRebalanceService;
	}
}
