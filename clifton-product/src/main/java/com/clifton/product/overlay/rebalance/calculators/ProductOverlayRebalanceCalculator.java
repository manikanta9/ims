package com.clifton.product.overlay.rebalance.calculators;


import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;
import com.clifton.product.overlay.rebalance.ProductOverlayRebalance;


/**
 * The <code>ProductOverlayRebalanceCalculator</code> ...
 *
 * @author manderson
 */
public interface ProductOverlayRebalanceCalculator {

	public RebalanceCalculationTypes getRebalanceCalculationType();


	/**
	 * Takes the passed ProductOverlayRebalance bean and sets New Rebalance Cash values based on calculator processing
	 *
	 * @param bean
	 */
	public void calculateNewRebalanceCash(ProductOverlayRebalance bean);
}
