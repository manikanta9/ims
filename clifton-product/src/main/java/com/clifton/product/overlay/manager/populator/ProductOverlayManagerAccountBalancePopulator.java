package com.clifton.product.overlay.manager.populator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.run.manager.populator.BasePortfolioRunManagerAccountBalancePopulator;
import com.clifton.portfolio.run.manager.populator.PortfolioRunManagerAccountBalancePopulatorConfig;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.overlay.manager.ProductOverlayManagerAccountBalance;
import com.clifton.product.overlay.search.ProductOverlayManagerAccountSearchForm;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Component("portfolioRunOverlayManagerAccountBalancePopulator")
public class ProductOverlayManagerAccountBalancePopulator extends BasePortfolioRunManagerAccountBalancePopulator<ProductOverlayManagerAccountBalance> {

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private ProductOverlayService productOverlayService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ProductOverlayManagerAccountBalance> populatePortfolioRunManagerAccountBalanceListImpl(PortfolioRunManagerAccountBalancePopulatorConfig config) {
		List<ProductOverlayManagerAccountBalance> list = new ArrayList<>();

		List<ProductOverlayManagerAccount> overlayManagerList = getOverlayManagerList(config);
		for (ProductOverlayManagerAccount overlayManager : CollectionUtils.getIterable(overlayManagerList)) {
			ProductOverlayManagerAccountBalance runManagerBalance = getPopulatedPortfolioRunManagerAccountBalance(config, overlayManager.getManagerAccountAssignment().getReferenceOne());
			// PIOS Specific Attributes
			runManagerBalance.setAccountAssetClass(overlayManager.getAccountAssetClass());
			// convert run amounts back into manager base currency
			runManagerBalance.setCashAllocationAmount(getRunValueInManagerAccountBaseCurrency(config, overlayManager.getManagerAccountAssignment().getReferenceOne(), overlayManager.getCashAllocation()));
			runManagerBalance.setSecuritiesAllocationAmount(getRunValueInManagerAccountBaseCurrency(config, overlayManager.getManagerAccountAssignment().getReferenceOne(), overlayManager.getSecuritiesAllocation()));

			list.add(runManagerBalance);
		}

		return list;
	}


	@Override
	protected Class<?> getAccountBalanceClass() {
		return ProductOverlayManagerAccountBalance.class;
	}


	private List<ProductOverlayManagerAccount> getOverlayManagerList(PortfolioRunManagerAccountBalancePopulatorConfig config) {
		ProductOverlayManagerAccountSearchForm overlaySearchForm = new ProductOverlayManagerAccountSearchForm();
		overlaySearchForm.setRunId(config.getPortfolioRun().getId());
		if (Boolean.TRUE.equals(config.getSearchForm().getRootOnly())) {
			overlaySearchForm.setRootOnly(true);
		}
		else {
			overlaySearchForm.setRollupAssetClass(false);
		}
		return getProductOverlayService().getProductOverlayManagerAccountList(overlaySearchForm);
	}


	private BigDecimal getRunValueInManagerAccountBaseCurrency(PortfolioRunManagerAccountBalancePopulatorConfig config, InvestmentManagerAccount managerAccount, BigDecimal value) {
		if (config.getPortfolioRun().getClientInvestmentAccount().getBaseCurrency().equals(managerAccount.getBaseCurrency())) {
			return value;
		}
		return MathUtils.multiply(value, config.getExchangeRateForManagerAccount(managerAccount, getMarketDataExchangeRatesApiService()));
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods                ///////////
	////////////////////////////////////////////////////////////////////////////////


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}
}
