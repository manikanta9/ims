package com.clifton.product.overlay.process;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalance;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceHistory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAllocationConfigTypes;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.portfolio.replication.PortfolioReplicationAllocationSecurityConfig;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.process.PortfolioRunConfig;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.product.overlay.ProductOverlayManagerAccountRebalance;
import com.clifton.product.overlay.manager.allocation.ProductOverlayManagerAllocationConfig;
import com.clifton.product.overlay.rebalance.minimizeimbalances.calculators.MinimizeImbalancesCashConfig;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>ProductOverlayRunConfig</code> configuration object used
 * to store configuration, data that is necessary to run PIOS runs
 *
 * @author manderson
 */
public class ProductOverlayRunConfig extends PortfolioRunConfig {

	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////

	/**
	 * Product Overlay Run Resulting Tables/Values
	 */
	private List<ProductOverlayManagerAccount> overlayManagerAccountList;
	private List<ProductOverlayManagerAccountRebalance> overlayManagerAccountRebalanceList;
	private List<PortfolioCurrencyOther> overlayCurrencyOtherList;

	private InvestmentAccountRebalance accountRebalanceConfig;
	private InvestmentAccountRebalanceHistory accountRebalanceHistory;

	private boolean excludeMispricing = false;

	private boolean doNotAdjustReplicationAllocationPercentages = false;

	private List<InvestmentAccountAssetClass> assetClassList;

	private Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> assetClassMap;
	private Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> assetClassRollupMap;

	private Map<InvestmentAccountAssetClass, List<ProductOverlayAssetClassReplication>> assetClassReplicationListMap = new LinkedHashMap<>();

	private List<ProductOverlayManagerAllocationConfig> managerAllocationConfigList = new ArrayList<>();

	/**
	 * After Asset Classes/Replications are process this value is saved
	 * directly on the {@link PortfolioRun}, however during processing store here
	 * for quick retrieval/updates until final result is ready to be saved on the run.
	 * <p>
	 * Sum Actual Allocation on the Asset Classes
	 * <p>
	 * Even if overridden, need the calculated value to fix rounding issues to sum to total
	 */
	private BigDecimal portfolioTotalCalculatedValue;

	/**
	 * Some accounts that use AEGIS we are given the Portfolio Total Value (Fixed Fund Value)
	 * and this value is saved as the total portfolio value and to calculate percentages
	 * <p>
	 * Another case is we use Manager Total Market Value for TPV - For this case - all exposure summary rounding for sums
	 * still uses originally calculated value. (Sets: doNotUseTPVOverrideValueForAssetClassProcessing = true)
	 */
	private BigDecimal portfolioTotalOverrideValue;

	private boolean doNotUseTPVOverrideValueForAssetClassProcessing;

	private boolean rebalanceExposureTargetsUsed;
	private BigDecimal rebalanceExposurePortfolioTotalValue;

	/**
	 * Populated when evaluating trading bands, result is then added to the run as an ignorable warning
	 */
	private String tradingBandsMessage;

	/**
	 * Configuration Objects Used for Processing
	 */
	private MinimizeImbalancesCashConfig minimizeImbalancesCashConfig;
	private final Map<String, Object> assetClassCustomFieldValueMap = new HashMap<>();

	/**
	 * When performing minimize imbalances, cash is moved in and out to/from RebalanceAttribution Cash which
	 * is forced to sum to zero.  This will account for rounding and ensure the cash bucket has the correct total
	 */
	private Map<ManagerCashTypes, BigDecimal> cashBucketExpectedTotalMap = new EnumMap<>(ManagerCashTypes.class);

	// The following properties are used for replication processing and the maps contain how targets/actuals may need to be updated as their dependencies targets/actuals are updated

	// Stores at key InvestmentAccountAssetClassID_InvestmentReplicationID_SecurityID the original rep with security info populated
	// For cases with minimize imbalances it's easier to reprocess replications, but don't want to have to look up static data again
	Map<String, ProductOverlayAssetClassReplication> replicationSetupMap = new LinkedHashMap<>();

	// Used to Estimate Targets for Client Directed Asset Classes or when Minimizing Imbalances - Necessary for Calculating Splitting Contracts
	Map<String, List<ProductOverlayAssetClassReplication>> estimateTargetRunMap = new HashMap<>();

	Map<InvestmentAccountAssetClass, List<InvestmentSecurity>> assetClassSecurityMap = new LinkedHashMap<>();

	/**
	 * When securities are validated for being "active" or not, some are OK if before start date if settles before start date.
	 * i.e. can buy a bond before its IssueDate as long as it settles on or after the issue date
	 * These likely won't have prices in MarketData, so before throwing missing price exception, check price here first
	 * <p>
	 * String key is InvestmentAccountAssetClassID_SecurityID - need asset class because for cases of sub-accounts positions may only apply to a specific asset class
	 */
	private final Map<String, BigDecimal> beforeStartDateSecurityIdPriceMap = new HashMap<>();

	// Primary Replication targets depend on how much is already allocated to the secondary
	private Map<Integer, Integer> secondaryToPrimaryReplicationMap = new HashMap<>();

	// Matching Replication targets depend on the target or actual (depending on configuration) of it's source replication allocation
	// Key is AccountAssetClassID_ReplicationAllocationID -> ReplicationID
	private final Map<String, Integer> replicationAllocationToMatchingReplicationMap = new LinkedHashMap<>();

	// Key is AccountID_ReplicationID - we need to include the account in the key because for accounts with sub-accounts
	// if two sub-accounts use the same replication their result may be different if they hold different securities.
	private final Map<String, List<PortfolioReplicationAllocationSecurityConfig>> replicationAllocationConfigMap = new LinkedHashMap<>();

	// When client directed trading option for: isUsePreviousDurationForTargetFollowsActual is used
	// The previous run to use is determined once, and then if used again for another asset class we already have the
	// replication list stored here.
	private List<ProductOverlayAssetClassReplication> previousReplicationListForDurationAdjustmentComparison;

	/**
	 * When manager assignment is set to apply securities balance to replications additional Exposure, we need the total amount that
	 * will be allocated by asset class so we can add it to the replication targets.
	 */
	private final Map<InvestmentAccountAssetClass, BigDecimal> assetClassAdditionalExposureAmountMap = new HashMap<>();

	/**
	 * Tracks a list if investment account asset class id's that use cash exposure replication allocation
	 * and will need overlay exposure recalculated based on targets and/or other allocation's exposure
	 */
	private final Set<InvestmentAccountAssetClass> accountAssetClassWithCashExposureReplicationList = new HashSet<>();


	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	public ProductOverlayRunConfig(PortfolioRun run, boolean reloadProxyManagers, int currentDepth) {
		super(run, reloadProxyManagers, currentDepth);
	}


	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	public BigDecimal getCoalescePortfolioTotalValue(boolean assetClassProcessing) {
		if (getPortfolioTotalOverrideValue() != null) {
			if (!assetClassProcessing || !isDoNotUseTPVOverrideValueForAssetClassProcessing()) {
				return getPortfolioTotalOverrideValue();
			}
		}
		return getPortfolioTotalCalculatedValue();
	}


	public boolean isMinimizeImbalancesUsed() {
		if (getAccountRebalanceConfig() != null) {
			return getAccountRebalanceConfig().getMinimizeImbalancesCalculatorBean() != null;
		}
		return false;
	}


	/**
	 * If a full rebalance was performed on the same date as the balance date
	 * <p>
	 * used by minimize imbalances - if yes, exclude the rebalance cash values in minimize imbalances calculation
	 */
	public boolean isFullRebalancePerformedOnBalanceDate() {
		if (getAccountRebalanceHistory() != null && DateUtils.compare(getBalanceDate(), getAccountRebalanceHistory().getRebalanceDate(), false) == 0
				&& getAccountRebalanceHistory().getRebalanceCalculationType().isFullRebalance()) {
			return true;
		}
		return false;
	}


	public boolean isManualRebalancePerformedOnBalanceDate() {
		if (getAccountRebalanceHistory() != null && DateUtils.compare(getBalanceDate(), getAccountRebalanceHistory().getRebalanceDate(), false) == 0
				&& getAccountRebalanceHistory().getRebalanceCalculationType().isManualRebalance()) {
			return true;
		}
		return false;
	}


	/**
	 * If MinEffectiveCashExposure or MaxEffectiveCashExposure is used
	 * checks if Total Effective Cash - Total Overlay Exposure is within the band
	 * which results in No Trades
	 *
	 * @param differenceMap is populated if not null with each asset class's difference
	 */
	public boolean isEffectiveCashLessExposureInsideNoTradingBands(Map<InvestmentAccountAssetClass, BigDecimal> differenceMap) {
		// Make sure message starts as cleared
		this.tradingBandsMessage = null;
		boolean result = false;
		if (getAccountRebalanceConfig() != null) {
			BigDecimal minCash = getAccountRebalanceConfig().getMinEffectiveCashExposure();
			BigDecimal maxCash = getAccountRebalanceConfig().getMaxEffectiveCashExposure();
			if (minCash != null || maxCash != null) {

				BigDecimal totalOverlay = BigDecimal.ZERO;
				BigDecimal totalEffectiveCash = BigDecimal.ZERO;
				for (Map.Entry<InvestmentAccountAssetClass, ProductOverlayAssetClass> iacEntry : getAssetClassMap().entrySet()) {
					// Skip Main Cash
					if (iacEntry.getKey().getAssetClass().isMainCash()) {
						continue;
					}
					ProductOverlayAssetClass oac = iacEntry.getValue();
					// Only Include if Replications Are Used "Investable Asset Class"
					if (oac.isReplicationUsed()) {
						totalOverlay = MathUtils.add(totalOverlay, oac.getOverlayExposure());
						totalEffectiveCash = MathUtils.add(totalEffectiveCash, oac.getEffectiveCashTotal());
						if (differenceMap != null) {
							differenceMap.put(iacEntry.getKey(), MathUtils.subtract(oac.getOverlayExposure(), oac.getCashTotal()));
						}
					}
				}

				// Within the band - No Trades
				// If Minimize Imbalances - Add in that Cash
				if (getMinimizeImbalancesCashConfig() != null) {
					totalEffectiveCash = MathUtils.add(totalEffectiveCash, getMinimizeImbalancesCashConfig().getCashTotal());
				}
				BigDecimal difference = MathUtils.subtract(totalEffectiveCash, totalOverlay);
				StringBuilder msg = new StringBuilder("Total Effective Cash: " + CoreMathUtils.formatNumberMoney(totalEffectiveCash) + StringUtils.NEW_LINE);
				msg.append("Total Overlay Exposure: ").append(CoreMathUtils.formatNumberMoney(totalOverlay)).append(StringUtils.NEW_LINE);
				msg.append("Difference: ").append(CoreMathUtils.formatNumberMoney(difference)).append(StringUtils.NEW_LINE);
				msg.append("No Trading Range: [").append(minCash != null ? CoreMathUtils.formatNumberMoney(minCash) : "N/A").append("] - [").append(maxCash != null ? CoreMathUtils.formatNumberMoney(maxCash) : "N/A").append("]").append(StringUtils.NEW_LINE);

				// Less then minimum band - then we need to trade or More than max band - then we need to trade
				if ((minCash != null && MathUtils.isLessThan(difference, minCash)) || (maxCash != null && MathUtils.isGreaterThan(difference, maxCash))) {
					msg.append("Difference is outside of no trading bands.  Asset Class Overlay Targets will be generated.");
				}
				else {
					result = true;
					msg.append("Difference is inside of no trading bands.  Asset Class Overlay Targets will be set to Overlay Exposure.");
				}
				this.tradingBandsMessage = msg.toString();
			}
		}
		return result;
	}


	public List<ProductOverlayAssetClass> getOverlayAssetClassList() {
		List<ProductOverlayAssetClass> list = new ArrayList<>();
		if (!CollectionUtils.isEmpty(getAssetClassMap())) {
			list.addAll(getAssetClassMap().values());
		}
		if (!CollectionUtils.isEmpty(getAssetClassRollupMap())) {
			list.addAll(getAssetClassRollupMap().values());
		}
		return list;
	}


	public List<ProductOverlayAssetClass> getOverlayAssetClassListExcludeRollups() {
		List<ProductOverlayAssetClass> list = new ArrayList<>();
		if (!CollectionUtils.isEmpty(getAssetClassMap())) {
			list.addAll(getAssetClassMap().values());
		}
		return list;
	}


	public List<ProductOverlayAssetClass> getOverlayAssetClassRollupList() {
		List<ProductOverlayAssetClass> list = new ArrayList<>();
		if (!CollectionUtils.isEmpty(getAssetClassRollupMap())) {
			list.addAll(getAssetClassRollupMap().values());
		}
		return list;
	}


	public List<ProductOverlayAssetClassReplication> getOverlayAssetClassReplicationList() {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		if (!CollectionUtils.isEmpty(getAssetClassReplicationListMap())) {
			for (List<ProductOverlayAssetClassReplication> thisList : CollectionUtils.getIterable(getAssetClassReplicationListMap().values())) {
				list.addAll(thisList);
			}
		}
		return list;
	}


	public void addChildValuesToParentOverlayAssetClass(ProductOverlayAssetClass parent, ProductOverlayAssetClass child) {
		child.setParent(parent);
		parent.setActualAllocation(MathUtils.add(parent.getActualAllocation(), child.getActualAllocation()));
		parent.setActualAllocationPercent(MathUtils.add(parent.getActualAllocationPercent(), child.getActualAllocationPercent()));

		parent.setActualAllocationAdjusted(MathUtils.add(parent.getActualAllocationAdjusted(), child.getActualAllocationAdjusted()));
		parent.setActualAllocationAdjustedPercent(MathUtils.add(parent.getActualAllocationAdjustedPercent(), child.getActualAllocationAdjustedPercent()));

		parent.setTargetAllocation(MathUtils.add(parent.getTargetAllocation(), child.getTargetAllocation()));
		parent.setTargetAllocationPercent(MathUtils.add(parent.getTargetAllocationPercent(), child.getTargetAllocationPercent()));

		if (child.getRebalanceExposureTarget() != null) {
			parent.setRebalanceExposureTarget(MathUtils.add(parent.getRebalanceExposureTarget(), child.getRebalanceExposureTarget()));
			parent.setRebalanceExposureTargetPercent(MathUtils.add(parent.getRebalanceExposureTargetPercent(), child.getRebalanceExposureTargetPercent()));
		}

		parent.setTargetAllocationAdjusted(MathUtils.add(parent.getTargetAllocationAdjusted(), child.getTargetAllocationAdjusted()));
		parent.setTargetAllocationAdjustedPercent(MathUtils.add(parent.getTargetAllocationAdjustedPercent(), child.getTargetAllocationAdjustedPercent()));

		parent.setOverlayExposure(MathUtils.add(parent.getOverlayExposure(), child.getOverlayExposure()));

		parent.setFundCash(MathUtils.add(parent.getFundCash(), child.getFundCash()));
		parent.setManagerCash(MathUtils.add(parent.getManagerCash(), child.getManagerCash()));
		parent.setTransitionCash(MathUtils.add(parent.getTransitionCash(), child.getTransitionCash()));
		parent.setClientDirectedCash(MathUtils.add(parent.getClientDirectedCash(), child.getClientDirectedCash()));
		parent.setRebalanceCash(MathUtils.add(parent.getRebalanceCash(), child.getRebalanceCash()));
		parent.setRebalanceCashAdjusted(MathUtils.add(parent.getRebalanceCashAdjusted(), child.getRebalanceCashAdjusted()));
		if (parent.getRebalanceDate() == null) {
			parent.setRebalanceDate(child.getRebalanceDate());
		}
		parent.setRebalanceAttributionCash(MathUtils.add(parent.getRebalanceAttributionCash(), child.getRebalanceAttributionCash()));
	}


	public void addReplicationAllocationMatchingReplicationToMap(ProductOverlayAssetClass overlayAssetClass, InvestmentReplicationAllocation allocation) {
		if (allocation.getMatchingReplication() != null) {
			this.replicationAllocationToMatchingReplicationMap.put(overlayAssetClass.getAccountAssetClass().getId() + "_" + allocation.getId(), allocation.getMatchingReplication().getId());
		}
	}


	public List<ProductOverlayAssetClassReplication> getMatchingReplicationList(ProductOverlayAssetClassReplication overlayReplication) {
		Integer repId = this.replicationAllocationToMatchingReplicationMap.get(overlayReplication.getOverlayAssetClass().getAccountAssetClass().getId() + "_"
				+ overlayReplication.getAllocationConfig().getReplicationAllocation().getId());
		if (repId != null) {
			return CollectionUtils.getStream(getOverlayAssetClassReplicationList())
					.filter(r -> CompareUtils.isEqual(r.getOverlayAssetClass().getAccountAssetClass(), overlayReplication.getOverlayAssetClass().getAccountAssetClass()))
					.filter(r -> CompareUtils.isEqual(repId, r.getReplication().getId()))
					.collect(Collectors.toList());
		}
		return null;
	}


	public List<ProductOverlayManagerAccount> getOverlayManagerAccountAllocateSecuritiesToReplicationsList() {
		return BeanUtils.filter(getOverlayManagerAccountList(), ProductOverlayManagerAccount::isAllocateSecuritiesBalanceToReplications);
	}


	public void addAdditionalExposureToAssetClass(InvestmentAccountAssetClass iac, BigDecimal amount) {
		BigDecimal originalAmount = this.assetClassAdditionalExposureAmountMap.get(iac);
		this.assetClassAdditionalExposureAmountMap.put(iac, MathUtils.add(originalAmount, amount));
	}


	public BigDecimal getAdditionalExposureTotalForAssetClass(InvestmentAccountAssetClass iac) {
		return this.assetClassAdditionalExposureAmountMap.get(iac);
	}


	public BigDecimal getAssetClassReplicationTotalTarget(ProductOverlayAssetClass oac) {
		return MathUtils.add(oac.getCashTotal(), getAdditionalExposureTotalForAssetClass(oac.getAccountAssetClass()));
	}


	public Object getAssetClassCustomFieldValue(InvestmentAccountAssetClass iac, String columnName) {
		return this.assetClassCustomFieldValueMap.get(columnName + "_" + iac.getId());
	}


	public void setAssetClassCustomFieldValue(InvestmentAccountAssetClass iac, String columnName, Object value) {
		this.assetClassCustomFieldValueMap.put(columnName + "_" + iac.getId(), value);
	}


	public void addAssetClassToCashExposureReplicationList(InvestmentAccountAssetClass iac) {
		this.accountAssetClassWithCashExposureReplicationList.add(iac);
	}


	public List<ProductOverlayManagerAllocationConfig> getProductOverlayManagerAllocationConfigListForConfigType(InvestmentManagerAllocationConfigTypes configType) {
		return BeanUtils.filter(getManagerAllocationConfigList(), ProductOverlayManagerAllocationConfig::getConfigType, configType);
	}


	public List<ProductOverlayManagerAllocationConfig> getProductOverlayManagerAllocationConfigListExcludeConfigType(InvestmentManagerAllocationConfigTypes configType) {
		return BeanUtils.filter(getManagerAllocationConfigList(), productOverlayManagerAllocationConfig -> productOverlayManagerAllocationConfig.getConfigType() != configType);
	}


	public List<ProductOverlayManagerAllocationConfig> getProductOverlayManagerAllocationConfigListForAssignment(InvestmentManagerAccountAssignment investmentManagerAccountAssignment) {
		return BeanUtils.filter(getManagerAllocationConfigList(), ProductOverlayManagerAllocationConfig::getManagerAccountAssignment, investmentManagerAccountAssignment);
	}


	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	public MinimizeImbalancesCashConfig getMinimizeImbalancesCashConfig() {
		return this.minimizeImbalancesCashConfig;
	}


	public void setMinimizeImbalancesCashConfig(MinimizeImbalancesCashConfig minimizeImbalancesCashConfig) {
		this.minimizeImbalancesCashConfig = minimizeImbalancesCashConfig;
	}


	public List<ProductOverlayManagerAccount> getOverlayManagerAccountList() {
		return this.overlayManagerAccountList;
	}


	public void setOverlayManagerAccountList(List<ProductOverlayManagerAccount> overlayManagerAccountList) {
		this.overlayManagerAccountList = overlayManagerAccountList;
	}


	public List<ProductOverlayManagerAccountRebalance> getOverlayManagerAccountRebalanceList() {
		return this.overlayManagerAccountRebalanceList;
	}


	public void setOverlayManagerAccountRebalanceList(List<ProductOverlayManagerAccountRebalance> overlayManagerAccountRebalanceList) {
		this.overlayManagerAccountRebalanceList = overlayManagerAccountRebalanceList;
	}


	public List<PortfolioCurrencyOther> getOverlayCurrencyOtherList() {
		return this.overlayCurrencyOtherList;
	}


	public void setOverlayCurrencyOtherList(List<PortfolioCurrencyOther> overlayCurrencyOtherList) {
		this.overlayCurrencyOtherList = overlayCurrencyOtherList;
	}


	public List<InvestmentAccountAssetClass> getAssetClassList() {
		return this.assetClassList;
	}


	public void setAssetClassList(List<InvestmentAccountAssetClass> assetClassList) {
		this.assetClassList = assetClassList;
	}


	public Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> getAssetClassMap() {
		return this.assetClassMap;
	}


	public void setAssetClassMap(Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> assetClassMap) {
		this.assetClassMap = assetClassMap;
	}


	public Map<InvestmentAccountAssetClass, List<ProductOverlayAssetClassReplication>> getAssetClassReplicationListMap() {
		return this.assetClassReplicationListMap;
	}


	public void setAssetClassReplicationListMap(Map<InvestmentAccountAssetClass, List<ProductOverlayAssetClassReplication>> assetClassReplicationListMap) {
		this.assetClassReplicationListMap = assetClassReplicationListMap;
	}


	public Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> getAssetClassRollupMap() {
		return this.assetClassRollupMap;
	}


	public void setAssetClassRollupMap(Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> assetClassRollupMap) {
		this.assetClassRollupMap = assetClassRollupMap;
	}


	public boolean isRebalanceExposureTargetsUsed() {
		return this.rebalanceExposureTargetsUsed;
	}


	public void setRebalanceExposureTargetsUsed(boolean rebalanceExposureTargetsUsed) {
		this.rebalanceExposureTargetsUsed = rebalanceExposureTargetsUsed;
	}


	public BigDecimal getRebalanceExposurePortfolioTotalValue() {
		return this.rebalanceExposurePortfolioTotalValue;
	}


	public void setRebalanceExposurePortfolioTotalValue(BigDecimal rebalanceExposurePortfolioTotalValue) {
		this.rebalanceExposurePortfolioTotalValue = rebalanceExposurePortfolioTotalValue;
	}


	public Map<Integer, Integer> getSecondaryToPrimaryReplicationMap() {
		return this.secondaryToPrimaryReplicationMap;
	}


	public void setSecondaryToPrimaryReplicationMap(Map<Integer, Integer> secondaryToPrimaryReplicationMap) {
		this.secondaryToPrimaryReplicationMap = secondaryToPrimaryReplicationMap;
	}


	public Map<InvestmentAccountAssetClass, List<InvestmentSecurity>> getAssetClassSecurityMap() {
		return this.assetClassSecurityMap;
	}


	public void setAssetClassSecurityMap(Map<InvestmentAccountAssetClass, List<InvestmentSecurity>> assetClassSecurityMap) {
		this.assetClassSecurityMap = assetClassSecurityMap;
	}


	public Map<String, ProductOverlayAssetClassReplication> getReplicationSetupMap() {
		return this.replicationSetupMap;
	}


	public void setReplicationSetupMap(Map<String, ProductOverlayAssetClassReplication> replicationSetupMap) {
		this.replicationSetupMap = replicationSetupMap;
	}


	public Map<ManagerCashTypes, BigDecimal> getCashBucketExpectedTotalMap() {
		return this.cashBucketExpectedTotalMap;
	}


	public void setCashBucketExpectedTotalMap(Map<ManagerCashTypes, BigDecimal> cashBucketExpectedTotalMap) {
		this.cashBucketExpectedTotalMap = cashBucketExpectedTotalMap;
	}


	public Map<String, List<ProductOverlayAssetClassReplication>> getEstimateTargetRunMap() {
		return this.estimateTargetRunMap;
	}


	public void setEstimateTargetRunMap(Map<String, List<ProductOverlayAssetClassReplication>> estimateTargetRunMap) {
		this.estimateTargetRunMap = estimateTargetRunMap;
	}


	public InvestmentAccountRebalance getAccountRebalanceConfig() {
		return this.accountRebalanceConfig;
	}


	public void setAccountRebalanceConfig(InvestmentAccountRebalance accountRebalanceConfig) {
		this.accountRebalanceConfig = accountRebalanceConfig;
	}


	public String getTradingBandsMessage() {
		return this.tradingBandsMessage;
	}


	public InvestmentAccountRebalanceHistory getAccountRebalanceHistory() {
		return this.accountRebalanceHistory;
	}


	public void setAccountRebalanceHistory(InvestmentAccountRebalanceHistory accountRebalanceHistory) {
		this.accountRebalanceHistory = accountRebalanceHistory;
	}


	public Map<String, BigDecimal> getBeforeStartDateSecurityIdPriceMap() {
		return this.beforeStartDateSecurityIdPriceMap;
	}


	public BigDecimal getPortfolioTotalCalculatedValue() {
		return this.portfolioTotalCalculatedValue;
	}


	public void setPortfolioTotalCalculatedValue(BigDecimal portfolioTotalCalculatedValue) {
		this.portfolioTotalCalculatedValue = portfolioTotalCalculatedValue;
	}


	public BigDecimal getPortfolioTotalOverrideValue() {
		return this.portfolioTotalOverrideValue;
	}


	public void setPortfolioTotalOverrideValue(BigDecimal portfolioTotalOverrideValue) {
		this.portfolioTotalOverrideValue = portfolioTotalOverrideValue;
	}


	public Map<String, List<PortfolioReplicationAllocationSecurityConfig>> getReplicationAllocationConfigMap() {
		return this.replicationAllocationConfigMap;
	}


	public List<ProductOverlayAssetClassReplication> getPreviousReplicationListForDurationAdjustmentComparison() {
		return this.previousReplicationListForDurationAdjustmentComparison;
	}


	public void setPreviousReplicationListForDurationAdjustmentComparison(List<ProductOverlayAssetClassReplication> previousReplicationListForDurationAdjustmentComparison) {
		this.previousReplicationListForDurationAdjustmentComparison = previousReplicationListForDurationAdjustmentComparison;
	}


	public Map<String, Integer> getReplicationAllocationToMatchingReplicationMap() {
		return this.replicationAllocationToMatchingReplicationMap;
	}


	public boolean isExcludeMispricing() {
		return this.excludeMispricing;
	}


	public void setExcludeMispricing(boolean excludeMispricing) {
		this.excludeMispricing = excludeMispricing;
	}


	public Set<InvestmentAccountAssetClass> getAccountAssetClassWithCashExposureReplicationList() {
		return this.accountAssetClassWithCashExposureReplicationList;
	}


	public boolean isDoNotUseTPVOverrideValueForAssetClassProcessing() {
		return this.doNotUseTPVOverrideValueForAssetClassProcessing;
	}


	public void setDoNotUseTPVOverrideValueForAssetClassProcessing(boolean doNotUseTPVOverrideValueForAssetClassProcessing) {
		this.doNotUseTPVOverrideValueForAssetClassProcessing = doNotUseTPVOverrideValueForAssetClassProcessing;
	}


	public boolean isDoNotAdjustReplicationAllocationPercentages() {
		return this.doNotAdjustReplicationAllocationPercentages;
	}


	public void setDoNotAdjustReplicationAllocationPercentages(boolean doNotAdjustReplicationAllocationPercentages) {
		this.doNotAdjustReplicationAllocationPercentages = doNotAdjustReplicationAllocationPercentages;
	}


	public List<ProductOverlayManagerAllocationConfig> getManagerAllocationConfigList() {
		return this.managerAllocationConfigList;
	}


	public void setManagerAllocationConfigList(List<ProductOverlayManagerAllocationConfig> managerAllocationConfigList) {
		this.managerAllocationConfigList = managerAllocationConfigList;
	}
}
