package com.clifton.product.replication;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.calculators.InvestmentReplicationCurrencyTypes;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.trade.PortfolioCurrencyTrade;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ProductReplicationService</code> ...
 *
 * @author manderson
 */
public interface ProductReplicationService {

	///////////////////////////////////////////////////////////
	///////         Product Replication Methods         ///////
	///////////////////////////////////////////////////////////


	/**
	 * Sets the Security and all Replication Allocation & Security Information on the Replication
	 */
	@DoNotAddRequestMapping
	public void setupProductOverlayAssetClassReplicationSecurityInfo(ProductOverlayAssetClassReplication replication, InvestmentSecurity security, String exchangeRateDataSourceName,
	                                                                 boolean excludeMispricing);


	/**
	 * There are some fields (Delta) whose value is dependent on the target. These values need to be reset after all targets are adjusted
	 */
	@DoNotAddRequestMapping
	public void recalculateProductOverlayAssetClassReplicationSecurityInfo(ProductOverlayAssetClassReplication replication);


	/**
	 * If setLivePrices - Sets security live prices, live exchange rates, etc. and
	 * Else clearsLivePrices except where manually overridden
	 * <p>
	 * re-calculates contract value as the "tradeValue"
	 */
	@DoNotAddRequestMapping
	public void refreshProductOverlayAssetClassReplicationSecurityLivePrices(ProductOverlayAssetClassReplication replication, InvestmentReplicationCurrencyTypes currencyType, boolean setLivePrices, boolean excludeMispricing);


	/**
	 * Creates a copy of the given replication using it for asset class, replication, etc. and setting the new security id to the new row
	 *
	 * @param id            - replicationId
	 * @param newSecurityId
	 * @param priceOverride - If set, and missing from market data will use this value as previous close price.  Will also set tradeSecurityPrice for price override on trade creation screen
	 */
	public void copyProductOverlayAssetClassReplication(int id, int newSecurityId, BigDecimal priceOverride);


	/**
	 * Specialized method for creating new replication for new Option as a copy of existing with new strike price
	 */
	@SecureMethod(dtoClass = ProductOverlayAssetClassReplication.class)
	public void copyProductOverlayAssetClassReplicationOption(int id, BigDecimal strikePrice, BigDecimal priceOverride);


	/**
	 * Validates given security for the {@link ProductOverlayAssetClassReplication}
	 */
	@DoNotAddRequestMapping
	public boolean validateProductOverlayAssetClassReplicationSecurityChange(ProductOverlayAssetClassReplication replication, InvestmentSecurity security, boolean throwExceptionIfNotValid,
	                                                                         List<ProductOverlayAssetClassReplication> fullRunRepList);


	/**
	 * Returns true if the replication's contract value calculator bean requires duration
	 * i.e. adjustment type =  Duration Adjustment or Credit Duration calculator
	 * This DOES NOT evaluate for the asset class if adjustments are made OR if the adjustment applies to the specific security
	 * - This keeps backwards compatibility from when we used the flag on the ENUM calculator type
	 */
	@DoNotAddRequestMapping
	public boolean isReplicationCalculatorDurationRequired(ProductOverlayAssetClassReplication replication);


	////////////////////////////////////////////////////////////////////////////
	//////////            Replication Helper Methods                ////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Takes a ContractStore and applies the contract values to the list of replications based upon config data,
	 * i.e. matches client/holding/security
	 * The  total amount of contracts to apply for that security and a list of replications and appropriately splits the total across
	 * the applicable replications.
	 *
	 * @param contracts       Contract Store - This has a "type" enum on it that holds what we are currently processing: actual, current, pending
	 * @param replicationList full replication list for the run
	 */
	@DoNotAddRequestMapping
	public void processContractSplitting(PortfolioAccountContractStore contracts, List<ProductOverlayAssetClassReplication> replicationList);


	/**
	 * Populates Currency Exposure on the Currency Replications and also applies the "other" currency to each replication.
	 * <p>
	 * Currency Trade List is a list of all Physical Currency Exposure (Those tied to replications & other)
	 */
	@DoNotAddRequestMapping
	public void processCurrencyContracts(PortfolioAccountContractStore contracts, PortfolioRun run, List<ProductOverlayAssetClassReplication> currencyReplicationList,
	                                     List<PortfolioCurrencyOther> currencyOtherList, List<PortfolioCurrencyTrade<ProductOverlayAssetClassReplication>> currencyTradeList, List<ProductOverlayAssetClassReplication> fullReplicationList);
}
