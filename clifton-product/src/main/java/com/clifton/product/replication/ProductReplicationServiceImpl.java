package com.clifton.product.replication;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.copy.InvestmentInstrumentCopyService;
import com.clifton.investment.instrument.copy.InvestmentSecurityCopyCommand;
import com.clifton.investment.replication.calculators.InvestmentReplicationCurrencyTypes;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.account.PortfolioAccountContractStore.PortfolioAccountContractSecurityStore;
import com.clifton.portfolio.account.PortfolioAccountContractStoreTypes;
import com.clifton.portfolio.replication.PortfolioReplicationHandler;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.trade.PortfolioCurrencyTrade;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;


/**
 * The <code>ProductReplicationServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class ProductReplicationServiceImpl implements ProductReplicationService {

	private InvestmentInstrumentCopyService investmentInstrumentCopyService;
	private InvestmentInstrumentService investmentInstrumentService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private PortfolioReplicationHandler portfolioReplicationHandler;

	private ProductOverlayService productOverlayService;

	////////////////////////////////////////////////////////////////////////////////
	////////////            Replication Helper Methods                //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void setupProductOverlayAssetClassReplicationSecurityInfo(ProductOverlayAssetClassReplication replication, InvestmentSecurity security, String exchangeRateDataSourceName, boolean excludeMispricing) {
		getPortfolioReplicationHandler().setupInvestmentReplicationSecurityAllocation(replication, security, exchangeRateDataSourceName, excludeMispricing);
	}


	@Override
	public void recalculateProductOverlayAssetClassReplicationSecurityInfo(ProductOverlayAssetClassReplication replication) {
		getPortfolioReplicationHandler().recalculateInvestmentReplicationSecurityAllocation(replication);
	}


	@Override
	public void refreshProductOverlayAssetClassReplicationSecurityLivePrices(ProductOverlayAssetClassReplication replication, InvestmentReplicationCurrencyTypes currencyType, boolean setLivePrices, boolean excludeMispricing) {
		getPortfolioReplicationHandler().refreshInvestmentReplicationSecurityAllocationLivePrices(replication, currencyType, setLivePrices, excludeMispricing);
	}


	@Override
	@Transactional
	public void copyProductOverlayAssetClassReplicationOption(int id, BigDecimal strikePrice, BigDecimal priceOverride) {
		ProductOverlayAssetClassReplication bean = getProductOverlayService().getProductOverlayAssetClassReplication(id);
		InvestmentSecurityCopyCommand command = new InvestmentSecurityCopyCommand();
		command.setSecurityToCopy(bean.getSecurity());
		command.setStrikePrice(strikePrice);
		command.setReturnExisting(true);
		InvestmentSecurity newOption = getInvestmentInstrumentCopyService().saveInvestmentSecurityOptionWithCommand(command);
		copyProductOverlayAssetClassReplication(id, newOption.getId(), priceOverride);
	}


	@Override
	@Transactional
	public void copyProductOverlayAssetClassReplication(int id, int newSecurityId, BigDecimal priceOverride) {
		ProductOverlayAssetClassReplication bean = getProductOverlayService().getProductOverlayAssetClassReplication(id);
		InvestmentSecurity newSecurity = getInvestmentInstrumentService().getInvestmentSecurity(newSecurityId);
		List<ProductOverlayAssetClassReplication> fullRunRepList = getProductOverlayService().getProductOverlayAssetClassReplicationListByRun(bean.getOverlayAssetClass().getOverlayRun().getId());
		// Assume at this point not to include mispricing - the run has already been processed and we are now on the Trade Creation Screen
		copyProductOverlayAssetClassReplicationImpl(bean, newSecurity, fullRunRepList, priceOverride, false);
	}


	private void copyProductOverlayAssetClassReplicationImpl(ProductOverlayAssetClassReplication bean, InvestmentSecurity newSecurity, List<ProductOverlayAssetClassReplication> fullRunRepList, BigDecimal priceOverride, boolean excludeMispricing) {
		// Validate Selected Security Fits the Replication we are copying
		// Only throws an exception if doesn't fit tradable replication, otherwise we are just trying to add to trade entry disabled
		// and if doesn't fit then we just don't add it
		if (validateProductOverlayAssetClassReplicationSecurityChange(bean, newSecurity, !bean.isTradeEntryDisabled(), fullRunRepList)) {
			ProductOverlayAssetClassReplication newRep = new ProductOverlayAssetClassReplication();
			newRep.setOverlayAssetClass(bean.getOverlayAssetClass());
			newRep.setReplication(bean.getReplication());
			newRep.setReplicationAllocationHistory(bean.getReplicationAllocationHistory());
			newRep.setMatchingAllocation(bean.isMatchingAllocation());
			newRep.setTargetExposure(BigDecimal.ZERO);
			newRep.setTargetExposureAdjusted(BigDecimal.ZERO);
			newRep.setAllocationWeightAdjusted(BigDecimal.ZERO);

			String exchangeRateDataSourceName = getMarketDataExchangeRatesApiService().getExchangeRateDataSourceForClientAccount(bean.getOverlayAssetClass().getOverlayRun().getClientInvestmentAccount().toClientAccount());
			// If Overridden will be set - set both so will be flagged manually overridden
			if (priceOverride != null) {
				newRep.setTradeSecurityPrice(priceOverride);
			}
			setupProductOverlayAssetClassReplicationSecurityInfo(newRep, newSecurity, exchangeRateDataSourceName, excludeMispricing);
			getProductOverlayService().saveProductOverlayAssetClassReplication(newRep);

			// Add it to the full list so it's included in future checks for duplicates
			fullRunRepList.add(newRep);

			// If trading not disabled, see if any matching with trading disabled and do we need to copy there too?
			// If not position excluded replication, check if there is one with the same security and update that security
			if (!bean.isTradeEntryDisabled()) {
				List<ProductOverlayAssetClassReplication> affectedList = BeanUtils.filter(fullRunRepList, ProductOverlayAssetClassReplication::getSecurity, bean.getSecurity());
				affectedList = BeanUtils.filter(affectedList, ProductOverlayAssetClassReplication::isTradeEntryDisabled);
				for (ProductOverlayAssetClassReplication affectedBean : CollectionUtils.getIterable(affectedList)) {
					copyProductOverlayAssetClassReplicationImpl(affectedBean, newSecurity, fullRunRepList, priceOverride, excludeMispricing);
				}
			}
		}
	}


	@Override
	public boolean validateProductOverlayAssetClassReplicationSecurityChange(ProductOverlayAssetClassReplication replication, InvestmentSecurity security, boolean throwExceptionIfNotValid, List<ProductOverlayAssetClassReplication> fullRunRepList) {
		return getPortfolioReplicationHandler().validateInvestmentReplicationSecurityAllocationSecurityChange(replication, security, ProductOverlayAssetClassReplication::getOverlayAssetClass, fullRunRepList, throwExceptionIfNotValid);
	}


	@Override
	public boolean isReplicationCalculatorDurationRequired(ProductOverlayAssetClassReplication replication) {
		return getPortfolioReplicationHandler().isReplicationCalculatorDurationRequired(replication);
	}


	/**
	 * Sets and splits contracts to the appropriate replications based on the configuration data for the account.
	 * Fills smallest to largest.
	 */
	@Override
	public void processContractSplitting(PortfolioAccountContractStore contracts, List<ProductOverlayAssetClassReplication> replicationList) {

		// Group Replications Into two groups - positions included vs. excluded in count
		List<ProductOverlayAssetClassReplication> includeReplicationList = BeanUtils.filter(replicationList, overlayReplication -> !overlayReplication.getOverlayAssetClass().getAccountAssetClass().isReplicationPositionExcludedFromCount());

		// Filter out replications that do not follow contract splitting
		List<ProductOverlayAssetClassReplication> excludeReplicationList = BeanUtils.filter(replicationList, overlayReplication -> overlayReplication.getOverlayAssetClass().getAccountAssetClass().isReplicationPositionExcludedFromCount());

		processContractSplittingImpl(contracts, includeReplicationList);
		if (!CollectionUtils.isEmpty(excludeReplicationList)) {
			processContractSplittingImpl(contracts, excludeReplicationList);
		}
	}


	private void processContractSplittingImpl(PortfolioAccountContractStore contracts, List<ProductOverlayAssetClassReplication> replicationList) {
		Set<Integer> securityIdSet = new HashSet<>();
		for (PortfolioAccountContractSecurityStore store : CollectionUtils.getIterable(contracts.getSecurityStoreCollection())) {
			// Filter By Security
			securityIdSet.add(store.getSecurityId());

			List<ProductOverlayAssetClassReplication> affectedReplicationList = BeanUtils.filter(replicationList, overlayReplication -> overlayReplication.getSecurity().getId(), store.getSecurityId());

			// Filter By Account Asset Class
			if (!store.isApplyToAll()) {
				affectedReplicationList = BeanUtils.filter(affectedReplicationList, overlayReplication -> CollectionUtils.contains(store.getAssetClassIdSet(), overlayReplication.getOverlayAssetClass().getAccountAssetClass().getId()));
			}
			processContractSplittingForSecurity(affectedReplicationList, contracts.getType(), store.getPositionQuantity(), contracts.getPositionAllocationListForSecurity(store.getSecurityId()),
					contracts.getTradePositionAllocationListForSecurity(store.getSecurityId()));
		}

		// Check for any securities that were skipped because we don't hold but there is an explicit split defined for it
		// Processes for Virtual Only (dependent allocations) since we don't hold any
		for (InvestmentAccountAssetClassPositionAllocation alloc : CollectionUtils.getIterable(contracts.getAssetClassPositionAllocationList())) {
			if (securityIdSet.add(alloc.getSecurity().getId())) {
				List<ProductOverlayAssetClassReplication> affectedReplicationList = BeanUtils.filter(replicationList, ProductOverlayAssetClassReplication::getSecurity, alloc.getSecurity());
				if (!CollectionUtils.isEmpty(affectedReplicationList)) {
					processContractSplittingForSecurity(affectedReplicationList, contracts.getType(), null, contracts.getPositionAllocationListForSecurity(alloc.getSecurity().getId()),
							contracts.getTradePositionAllocationListForSecurity(alloc.getSecurity().getId()));
				}
			}
		}
	}


	/**
	 * Returns the config object if used for contract splitting, so we know to go back and process Virtual if necessary
	 */
	private void processContractSplittingForSecurity(List<ProductOverlayAssetClassReplication> affectedReplicationList, PortfolioAccountContractStoreTypes type, PortfolioAccountContractStore.SecurityPositionQuantity positionTotalQuantity, List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList, List<TradeAssetClassPositionAllocation> tradePositionAllocationList) {
		// NO REPLICATIONS FOR SECURITY, CONTINUE ON
		if (!CollectionUtils.isEmpty(affectedReplicationList)) {
			BigDecimal totalQuantity = positionTotalQuantity == null ? null : positionTotalQuantity.getQuantityForReplicationList(affectedReplicationList);
			// ONLY ONE REPLICATION FOR THE SECURITY - GIVE IT ALL THE CONTRACTS
			if (CollectionUtils.getSize(affectedReplicationList) == 1) {
				ProductOverlayAssetClassReplication rep = affectedReplicationList.get(0);
				BeanUtils.setPropertyValue(rep, type.getFieldName(), MathUtils.add((BigDecimal) BeanUtils.getPropertyValue(rep, type.getFieldName()), totalQuantity));
			}
			else {
				// OTHERWISE, NEED TO SPLIT IT UP ACROSS REPLICATIONS
				ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, affectedReplicationList, positionAllocationList, tradePositionAllocationList, totalQuantity);
				config.process();
			}
		}
	}


	@Override
	public void processCurrencyContracts(PortfolioAccountContractStore contracts, PortfolioRun run, List<ProductOverlayAssetClassReplication> currencyReplicationList, List<PortfolioCurrencyOther> currencyOtherList, List<PortfolioCurrencyTrade<ProductOverlayAssetClassReplication>> currencyTradeList, List<ProductOverlayAssetClassReplication> fullReplicationList) {
		BiFunction<PortfolioAccountContractStore, ProductOverlayAssetClassReplication, List<AccountingPosition>> replicationPositionFunction = (c, rep) ->
				c.getSecurityAccountingPositionListForAccountAssetClass(rep.getAccountAssetClass(), rep.getSecurity().getId());
		Function<ProductOverlayAssetClassReplication, BaseSimpleEntity<Integer>> matchingFilterFunction = ProductOverlayAssetClassReplication::getAccountAssetClass;
		getPortfolioReplicationHandler().processPortfolioReplicationListForCurrencyContracts(contracts, replicationPositionFunction, run, currencyReplicationList, currencyOtherList, currencyTradeList, fullReplicationList, matchingFilterFunction);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////                  Getters & Setters                  //////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentCopyService getInvestmentInstrumentCopyService() {
		return this.investmentInstrumentCopyService;
	}


	public void setInvestmentInstrumentCopyService(InvestmentInstrumentCopyService investmentInstrumentCopyService) {
		this.investmentInstrumentCopyService = investmentInstrumentCopyService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public PortfolioReplicationHandler getPortfolioReplicationHandler() {
		return this.portfolioReplicationHandler;
	}


	public void setPortfolioReplicationHandler(PortfolioReplicationHandler portfolioReplicationHandler) {
		this.portfolioReplicationHandler = portfolioReplicationHandler;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}
}
