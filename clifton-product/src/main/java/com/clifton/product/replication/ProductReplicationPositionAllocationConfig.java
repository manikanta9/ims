package com.clifton.product.replication;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.portfolio.account.PortfolioAccountContractStoreTypes;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductReplicationPositionAllocationConfig</code> holds a list of replications
 * for a security that need to process actual, current, pending allocation splitting
 *
 * @author manderson
 */
public class ProductReplicationPositionAllocationConfig {

	private final BigDecimal totalQuantity;
	private final List<ProductOverlayAssetClassReplication> replicationList;
	private final List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList;
	private final List<TradeAssetClassPositionAllocation> tradePositionAllocationList;
	private PortfolioAccountContractStoreTypes type;
	private Map<String, ProductOverlayAssetClassReplication> replicationMap;
	private Map<String, BigDecimal> replicationTargetMap;

	private Map<String, BigDecimal> positionAllocationQuantityMap;
	private Map<String, BigDecimal> tradePositionAllocationQuantityMap;
	private Map<String, Short> positionAllocationOrderMap;

	/////////////////////////////////////////////////////////


	public ProductReplicationPositionAllocationConfig(PortfolioAccountContractStoreTypes type, List<ProductOverlayAssetClassReplication> replicationList,
	                                                  List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList, BigDecimal totalQuantity) {
		this(type, replicationList, positionAllocationList, null, totalQuantity);
	}


	public ProductReplicationPositionAllocationConfig(PortfolioAccountContractStoreTypes type, List<ProductOverlayAssetClassReplication> replicationList,
	                                                  List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList, List<TradeAssetClassPositionAllocation> tradePositionAllocationList, BigDecimal totalQuantity) {
		this.type = type;
		this.replicationList = replicationList;
		this.positionAllocationList = positionAllocationList;
		this.tradePositionAllocationList = tradePositionAllocationList;
		this.totalQuantity = totalQuantity;
	}


	private void setupConfig() {
		// Use Explicit Targets Only If Not Pending Contract Allocations
		this.positionAllocationQuantityMap = new HashMap<>();
		this.positionAllocationOrderMap = new HashMap<>();

		this.replicationMap = new LinkedHashMap<>();
		for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(this.replicationList)) {
			this.replicationMap.put(getKeyForReplication(rep), rep);
		}

		// If Actual Only - Set up Explicit
		if (!this.type.isUseTradingValue()) {
			setupPositionAllocationMaps();
		}

		this.tradePositionAllocationQuantityMap = new HashMap<>();
		setupTradePositionAllocationMaps();

		this.replicationTargetMap = new HashMap<>();
		setupReplicationTargetMap();
	}

	/////////////////////////////////////////////////////////
	///////             Processing Methods           ////////
	/////////////////////////////////////////////////////////


	public void process() {
		setupConfig();
		processAllocations(this.totalQuantity);
		processDependentAllocations();
	}


	/**
	 * Processes contract splitting across replications based on totalQuantity and short/long replication lists
	 */
	private void processAllocations(BigDecimal quantity) {
		if (this.type.isVirtual()) {
			processVirtualAllocations();
		}
		else {
			processAllocationsImpl(quantity, false, true);
		}
	}


	private void processDependentAllocations() {
		PortfolioAccountContractStoreTypes dependentType = this.type.getDependentType();
		if (dependentType != null) {
			this.type = dependentType;
			processAllocations(null);
		}
	}


	/**
	 * Cases where replications for the same security have both long and short targets - applies
	 * "virtual" contracts to them to help them hit their targets.
	 * <p>
	 * i.e. Short Target of -77 and positive target of 20 and hold -60 contracts.
	 * Will add -17 virtual contracts to the short and 17 to the long - Net virtual contracts is always 0
	 */
	private void processVirtualAllocations() {
		// Change type to Virtual So sets correct fieldName
		if (PortfolioAccountContractStoreTypes.ACTUAL == this.type) {
			this.type = PortfolioAccountContractStoreTypes.VIRTUAL;
		}

		// GENERATE VIRTUAL CONTRACTS - Applied if processing ACTUAL contract split and holding long and short
		BigDecimal sTarget = getCurrentShortTargetTotal();
		BigDecimal lTarget = getCurrentLongTargetTotal();
		BigDecimal forceTarget = getExplicitTargetTotal();

		// Virtual Contract Quantity ALWAYS returned as a positive number
		BigDecimal virtualContractQuantity = generateVirtualContractQuantity(sTarget, lTarget, forceTarget);

		// Only if we still have long and short
		if (!MathUtils.isNullOrZero(virtualContractQuantity)) {
			processAllocationsImpl(MathUtils.negate(virtualContractQuantity), false, true);
			processAllocationsImpl(virtualContractQuantity, false, true);
		}
	}


	/**
	 * If short - apply contracts to the short list, else long.  Only apply to the other list if nothing in the first list
	 */
	private void processAllocationsImpl(BigDecimal quantity, boolean holdWrongDirection, boolean explicitTargets) {
		boolean shortContract = MathUtils.isLessThan(quantity, 0);
		List<ProductOverlayAssetClassReplication> list = holdWrongDirection ? null : (shortContract ? getShortReplicationListInOrder(!explicitTargets) : getLongReplicationListInOrder(!explicitTargets));

		if (CollectionUtils.isEmpty(list)) {
			holdWrongDirection = true;
			list = (shortContract ? getLongReplicationListInOrder(explicitTargets) : getShortReplicationListInOrder(explicitTargets));
			if (CollectionUtils.isEmpty(list)) {
				if (explicitTargets) {
					processAllocationsImpl(quantity, false, false);
					return;
				}
				else {
					List<ProductOverlayAssetClassReplication> originalList = new ArrayList<>();
					for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(this.replicationList)) {
						BigDecimal target = (rep.isMatchingReplication() ? rep.getTargetContracts() : rep.getTargetContractsAdjusted());
						if (MathUtils.isLessThan(target, BigDecimal.ZERO)) {
							if (shortContract) {
								originalList.add(rep);
							}
						}
						else if (!shortContract) {
							originalList.add(rep);
						}
					}
					if (!CollectionUtils.isEmpty(originalList)) {
						list = originalList;
						holdWrongDirection = false;
					}
					else {
						list = this.replicationList;
						holdWrongDirection = true;
					}
				}
			}
		}

		Iterator<ProductOverlayAssetClassReplication> iterator = list.iterator();
		while (iterator.hasNext() && !MathUtils.isNullOrZero(quantity)) {
			ProductOverlayAssetClassReplication rep = iterator.next();
			BigDecimal target = getTargetForReplication(rep, true);

			// If already fulfilled and there is another one after it - skip it
			if (MathUtils.isEqual(target, BigDecimal.ZERO) && iterator.hasNext()) {
				addContractQuantity(rep, BigDecimal.ZERO);
				continue;
			}

			// If hold in the wrong direction only - give the full amount to the first one
			if (holdWrongDirection) {
				addContractQuantity(rep, quantity);
				quantity = BigDecimal.ZERO;
				break;
			}

			// If the last one - just give it the entire balance
			// AND it's not explicit
			// If it's explicit we'll fill to target and try to use remainder to fill other reps that are in opposite direction
			if (!iterator.hasNext() && (!this.positionAllocationQuantityMap.containsKey(getKeyForReplication(rep)))) {
				addContractQuantity(rep, quantity);
				quantity = BigDecimal.ZERO;
			}
			else {
				BigDecimal difference = MathUtils.subtract(quantity, target);
				if (MathUtils.isEqual(0, difference) || (MathUtils.isLessThan(difference, 0) && shortContract) || (MathUtils.isGreaterThan(difference, 0) && !shortContract)) {
					// We have enough to fill it, set actual to target and update total to the difference
					quantity = difference;
					addContractQuantity(rep, target);
				}
				else {
					// We didn't have enough to fill it, so just give it what's left and update total to 0 so the rest are set to 0
					addContractQuantity(rep, quantity);
					quantity = BigDecimal.ZERO;
				}
			}

			// Everything allocated - break out of loop
			if (MathUtils.isNullOrZero(quantity)) {
				return;
			}
		}

		// Didn't allocate everything try again - this time will go in "wrong" direction
		if (!MathUtils.isNullOrZero(quantity)) {
			processAllocationsImpl(quantity, true, explicitTargets);
		}
	}

	/////////////////////////////////////////////////////////
	////////           Setup/Helper Methods          ////////
	/////////////////////////////////////////////////////////


	private void setupPositionAllocationMaps() {
		validatePositionAllocationList();
		for (InvestmentAccountAssetClassPositionAllocation allocation : CollectionUtils.getIterable(this.positionAllocationList)) {
			// Look it up - Make sure only one replication in the asset class uses it
			InvestmentReplication rep = getInvestmentReplicationForPositionAllocation(allocation);
			if (rep == null) {
				// Unused - Ignore it - Warnings will pick up unused explicit allocations
				continue;
			}
			String key = getKeyForPositionAllocation(allocation, rep);
			if (this.positionAllocationQuantityMap.containsKey(key)) {
				throw new ValidationExceptionWithCause("InvestmentAccountAssetClassPositionAllocation", allocation.getId(),
						"Invalid Position Allocation Defined. There is more than one active position allocation defined for security [" + allocation.getSecurity().getLabel() + "] and asset class ["
								+ allocation.getAccountAssetClass().getAssetClass().getLabel() + "].");
			}
			this.positionAllocationQuantityMap.put(key, allocation.getAllocationQuantity());
			if (allocation.getAllocationOrder() != null) {
				this.positionAllocationOrderMap.put(key, allocation.getAllocationOrder());
			}
		}
	}


	/**
	 * Returns the InvestmentReplication to use for the position allocation.  Most position allocations define the asset class only.  If splitting within the asset class the replication is required.
	 * This method will return the InvestmentReplication for the position allocation as long as it also exists on the run - otherwise considered "unused"
	 */
	private InvestmentReplication getInvestmentReplicationForPositionAllocation(InvestmentAccountAssetClassPositionAllocation allocation) {
		List<ProductOverlayAssetClassReplication> allocRepList = BeanUtils.filter(this.replicationList, productOverlayAssetClassReplication -> productOverlayAssetClassReplication.getOverlayAssetClass().getAccountAssetClass().getId(), allocation.getAccountAssetClass().getId());
		if (allocation.getReplication() != null) {
			allocRepList = BeanUtils.filter(allocRepList, allocRep -> allocRep.getReplication().getId(), allocation.getReplication().getId());
		}
		if (CollectionUtils.getSize(allocRepList) == 1) {
			return allocRepList.get(0).getReplication();
		}
		if (CollectionUtils.getSize(allocRepList) > 1) {
			// More than one found
			throw new ValidationExceptionWithCause("InvestmentAccountAssetClassPositionAllocation", allocation.getId(), "Security [" + allocation.getSecurity().getLabel()
					+ "] needs to be split within the following asset class across the following replications: " + BeanUtils.getPropertyValues(allocRepList, "replication.name", ",")
					+ ".  Explicit contract splits are defined for asset classes, but a current specific replication must be selected.");
		}
		return null;
	}


	private void setupTradePositionAllocationMaps() {
		// In order to allocate trade "balance" from rolls that weren't explicit when other trades are present, need to go through each trade totals
		// and amounts allocated explicitly, so we can allocate the balance "proportionally"
		List<Integer> tradeIdsChecked = new ArrayList<>();
		for (TradeAssetClassPositionAllocation allocation : CollectionUtils.getIterable(this.tradePositionAllocationList)) {
			// Note: Trade ID Can Be Null for Virtual Trades (Entered equal Buys and Sells across asset classes)
			Integer tradeId = (allocation.getTrade() != null ? allocation.getTrade().getId() : null);
			if (tradeId != null) {
				if (tradeIdsChecked.contains(tradeId)) {
					continue;
				}
				tradeIdsChecked.add(tradeId);
				if ((this.type.isPending() && allocation.isPending()) || (!this.type.isPending() && allocation.isActual())) {
					allocateTradePositionAllocations(BeanUtils.filter(this.tradePositionAllocationList, tradeAssetClassPositionAllocation -> (tradeAssetClassPositionAllocation.getTrade() == null ? null : tradeAssetClassPositionAllocation.getTrade().getId()), tradeId), (allocation.getTrade().isBuy() ? allocation.getTrade().getQuantity() : MathUtils.negate(allocation.getTrade().getQuantity())));
				}
			}
			else {
				if ((this.type.isPending() && allocation.isPending()) || (!this.type.isPending() && allocation.isActual())) {
					allocateTradePositionAllocations(CollectionUtils.createList(allocation), (this.type.isPending() ? allocation.getPendingQuantity() : allocation.getActualQuantity()));
				}
			}
		}
	}


	private void allocateTradePositionAllocations(List<TradeAssetClassPositionAllocation> tradePositionAllocationList, BigDecimal tradeTotal) {
		BigDecimal allocatedTotal = BigDecimal.ZERO;
		Map<String, BigDecimal> splitMap = new HashMap<>();

		for (TradeAssetClassPositionAllocation allocation : CollectionUtils.getIterable(tradePositionAllocationList)) {
			// If Pending, we include both as we want the targets to include all that have been allocated...
			BigDecimal quantity = (this.type.isPending() ? allocation.getPendingQuantity() : allocation.getActualQuantity());
			if (!MathUtils.isNullOrZero(quantity)) {
				String key = getKeyForTradePositionAllocation(allocation);
				if (key != null) {
					allocatedTotal = MathUtils.add(allocatedTotal, quantity);
					splitMap.put(key, quantity);
				}
			}
		}

		// Allocate the Difference
		if (!MathUtils.isEqual(tradeTotal, allocatedTotal)) {
			BigDecimal difference = MathUtils.subtract(tradeTotal, allocatedTotal);
			BigDecimal totalNotExplicitTarget = BigDecimal.ZERO;
			List<ProductOverlayAssetClassReplication> notExplicitList = new ArrayList<>();
			for (ProductOverlayAssetClassReplication rep : this.replicationList) {
				if (!splitMap.containsKey(getKeyForReplication(rep))) {
					totalNotExplicitTarget = MathUtils.add(totalNotExplicitTarget, MathUtils.subtract(rep.getTargetContracts(), rep.getActualContractsAdjusted()));
					notExplicitList.add(rep);
				}
			}
			if (notExplicitList.size() == 1) {
				splitMap.put(getKeyForReplication(notExplicitList.get(0)), difference);
			}
			else if (notExplicitList.size() > 1) {
				BigDecimal remaining = difference;
				for (int i = 0; i < notExplicitList.size(); i++) {
					ProductOverlayAssetClassReplication rep = notExplicitList.get(i);
					if (i == notExplicitList.size() - 1) {
						splitMap.put(getKeyForReplication(rep), remaining);
					}
					else {
						BigDecimal allocateAmount = MathUtils.round(MathUtils.getPercentageOf(CoreMathUtils.getPercentValue(MathUtils.subtract(rep.getTargetContracts(), rep.getActualContractsAdjusted()), totalNotExplicitTarget), difference), 0);
						splitMap.put(getKeyForReplication(rep), allocateAmount);
						remaining = MathUtils.subtract(remaining, allocateAmount);
					}
				}
			}
			else { // No Replications Unallocated (maybe removed from run?)
				// Reallocate Explicits Based
				for (Map.Entry<String, BigDecimal> stringBigDecimalEntry : splitMap.entrySet()) {
					splitMap.put(stringBigDecimalEntry.getKey(), MathUtils.add(stringBigDecimalEntry.getValue(), MathUtils.round(MathUtils.getPercentageOf(CoreMathUtils.getPercentValue(stringBigDecimalEntry.getValue(), allocatedTotal), difference), 0)));
				}
			}
		}

		for (Map.Entry<String, BigDecimal> stringBigDecimalEntry : splitMap.entrySet()) {
			if (this.tradePositionAllocationQuantityMap.containsKey(stringBigDecimalEntry.getKey())) {
				this.tradePositionAllocationQuantityMap.put(stringBigDecimalEntry.getKey(), MathUtils.add(this.tradePositionAllocationQuantityMap.get(stringBigDecimalEntry.getKey()), stringBigDecimalEntry.getValue()));
			}
			else {
				this.tradePositionAllocationQuantityMap.put(stringBigDecimalEntry.getKey(), stringBigDecimalEntry.getValue());
			}
		}
	}


	private String getKeyForPositionAllocation(InvestmentAccountAssetClassPositionAllocation alloc, InvestmentReplication replication) {
		return alloc.getAccountAssetClass().getId() + "_" + (alloc.getReplication() == null ? replication.getId() : alloc.getReplication().getId());
	}


	private String getKeyForTradePositionAllocation(TradeAssetClassPositionAllocation allocation) {
		// Look it up - Make sure only one replication in the asset class uses it
		// If more than one or none just skip it - don't want to have a validation exception here.  This is auto-managed so if we can't figure it out - ignore it
		InvestmentReplication rep = allocation.getReplication();
		// If no replication known, or the one assigned doesn't apply anymore, try to figure it out on the current run
		// This can happen if there are pending trades from a run, then the replications are updated and a new run is created
		if (rep == null || !this.replicationMap.containsKey(allocation.getAccountAssetClass().getId() + "_" + rep.getId())) {
			rep = null; // Clear the replication if we are trying to re-match up the allocation
			List<ProductOverlayAssetClassReplication> allocRepList = BeanUtils.filter(this.replicationList, productOverlayAssetClassReplication -> productOverlayAssetClassReplication.getOverlayAssetClass().getAccountAssetClass().getId(), allocation.getAccountAssetClass().getId());
			if (!CollectionUtils.isEmpty(allocRepList)) {
				if (allocRepList.size() == 1) {
					rep = allocRepList.get(0).getReplication();
				}
				// If more than one, try to find the one with the security
				allocRepList = BeanUtils.filter(allocRepList, ProductOverlayAssetClassReplication::getSecurity, allocation.getSecurity());
				if (CollectionUtils.getSize(allocRepList) == 1) {
					rep = allocRepList.get(0).getReplication();
				}
			}
		}
		return (rep == null ? null : allocation.getAccountAssetClass().getId() + "_" + rep.getId());
	}


	private void validatePositionAllocationList() {
		if (!CollectionUtils.isEmpty(this.positionAllocationList)) {
			if (CollectionUtils.getSize(this.replicationList) <= CollectionUtils.getSize(this.positionAllocationList)) {
				// Use the First One for the Warning
				InvestmentAccountAssetClassPositionAllocation allocation = this.positionAllocationList.get(0);
				throw new ValidationExceptionWithCause(
						"InvestmentAccountAssetClassPositionAllocation",
						allocation.getId(),
						"Security ["
								+ allocation.getSecurity().getLabel()
								+ "] needs to be split across the following asset classes: "
								+ BeanUtils.getPropertyValues(this.replicationList, "overlayAssetClass.label", ",")
								+ " with explicit contract splits defined for asset classes: "
								+ BeanUtils.getPropertyValues(this.positionAllocationList, "accountAssetClass.label", ",")
								+ ". Explicit contract splits can be defined for up to one less than the total asset classes it is used for.  The remaining asset classes unallocated will be applied the rest of the contracts.");
			}
		}
	}


	private void setupReplicationTargetMap() {
		// Actual - Use Explicit Targets or Targets from the Replication Only
		if (!this.type.isUseTradingValue()) {
			BigDecimal totalExplicit = BigDecimal.ZERO;
			for (Map.Entry<String, BigDecimal> stringBigDecimalEntry : this.positionAllocationQuantityMap.entrySet()) {
				this.replicationTargetMap.put(stringBigDecimalEntry.getKey(), stringBigDecimalEntry.getValue());
				totalExplicit = MathUtils.add(totalExplicit, stringBigDecimalEntry.getValue());
			}

			for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(this.replicationList)) {
				// If difference = 0, just give it zero, otherwise add to list for proportional allocation
				if (!this.replicationTargetMap.containsKey(getKeyForReplication(rep))) {
					// If actual, set targets as the real targets to use
					if (rep.isMatchingReplication()) {
						this.replicationTargetMap.put(getKeyForReplication(rep), rep.getTargetContracts());
					}
					else {
						this.replicationTargetMap.put(getKeyForReplication(rep), rep.getTargetContractsAdjusted());
					}
				}
			}
		}

		// Pending - Explicit Trade Allocations, Current = Actual Adjusted + Explicit Trade Allocations
		else {
			BigDecimal totalExplicit = BigDecimal.ZERO;
			for (Map.Entry<String, BigDecimal> stringBigDecimalEntry : this.tradePositionAllocationQuantityMap.entrySet()) {
				BigDecimal quantity = stringBigDecimalEntry.getValue();
				totalExplicit = MathUtils.add(totalExplicit, quantity);
				// If current
				if (!this.type.isPending()) {
					quantity = MathUtils.add(quantity, this.replicationMap.get(stringBigDecimalEntry.getKey()).getActualContractsAdjusted());
				}
				this.replicationTargetMap.put(stringBigDecimalEntry.getKey(), quantity);
			}

			// Add in actual as "explicit" so we only distribute the difference
			if (!this.type.isPending()) {
				totalExplicit = MathUtils.add(totalExplicit, CoreMathUtils.sumProperty(this.replicationList, ProductOverlayAssetClassReplication::getActualContractsAdjusted));
			}

			// Now need to allocate the difference proportionally to those not explicit
			BigDecimal difference = MathUtils.subtract(this.totalQuantity, totalExplicit);
			List<ProductOverlayAssetClassReplication> notExplicitList = new ArrayList<>();
			BigDecimal totalNotExplicitTarget = BigDecimal.ZERO;
			for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(this.replicationList)) {
				if (!this.replicationTargetMap.containsKey(getKeyForReplication(rep))) {
					// No difference - Or nothing explicit from trading - pending targets are zero, current = actual adjusted
					if (MathUtils.isNullOrZero(difference) || CollectionUtils.isEmpty(this.tradePositionAllocationQuantityMap)) {
						// AND Pending, don't bother setting target, should always be allocated zero
						if (this.type.isPending()) {
							this.replicationTargetMap.put(getKeyForReplication(rep), BigDecimal.ZERO);
						}
						// Current - Set it to Actual Adjusted
						else {
							this.replicationTargetMap.put(getKeyForReplication(rep), rep.getActualContractsAdjusted());
						}
					}
					// Otherwise, try to distribute proportionally to remaining replications where not explicit
					else {
						notExplicitList.add(rep);
						totalNotExplicitTarget = MathUtils.add(totalNotExplicitTarget, MathUtils.subtract(rep.getTargetContracts(), rep.getActualContractsAdjusted()));
					}
				}
			}

			// Only one not explicit = give it the difference
			if (notExplicitList.size() == 1) {
				if (this.type.isPending()) {
					this.replicationTargetMap.put(getKeyForReplication(notExplicitList.get(0)), difference);
				}
				else {
					this.replicationTargetMap.put(getKeyForReplication(notExplicitList.get(0)), MathUtils.add(notExplicitList.get(0).getActualContractsAdjusted(), difference));
				}
			}

			// Multiple not explicit - allocate proportionally
			else if (notExplicitList.size() > 1) {
				for (ProductOverlayAssetClassReplication rep : notExplicitList) {
					BigDecimal quantity = (this.type.isPending() ? BigDecimal.ZERO : rep.getActualContractsAdjusted());
					this.replicationTargetMap.put(getKeyForReplication(rep), MathUtils.add(quantity, MathUtils.round(MathUtils.getPercentageOf(CoreMathUtils.getPercentValue(MathUtils.subtract(rep.getTargetContracts(), rep.getActualContractsAdjusted()), totalNotExplicitTarget), difference), 0)));
				}
			}
		}
	}


	protected String getKeyForReplication(ProductOverlayAssetClassReplication rep) {
		return rep.getOverlayAssetClass().getAccountAssetClass().getId() + "_" + rep.getReplication().getId();
	}


	private BigDecimal getContractQuantity(ProductOverlayAssetClassReplication rep) {
		BigDecimal value = (BigDecimal) BeanUtils.getPropertyValue(rep, this.type.getFieldName());
		if (value == null) {
			return BigDecimal.ZERO;
		}
		return value;
	}


	private BigDecimal getTargetForReplication(ProductOverlayAssetClassReplication rep, boolean adjustedForWhatIsAllocated) {
		BigDecimal target = this.replicationTargetMap.get(getKeyForReplication(rep));
		if (adjustedForWhatIsAllocated) {
			// Reduce Virtual By Actual
			if (PortfolioAccountContractStoreTypes.VIRTUAL == this.type) {
				// Adjust target based on what is already posted
				target = MathUtils.subtract(target, rep.getActualContracts());
			}

			// Reduce Current Virtual By Current
			if (PortfolioAccountContractStoreTypes.VIRTUAL_CURRENT == this.type) {
				// Adjust target based on what is already posted
				target = MathUtils.subtract(target, rep.getCurrentContracts());
			}

			// Also Reduce Pending Virtual by Pending
			if (PortfolioAccountContractStoreTypes.VIRTUAL_PENDING == this.type) {
				// Adjust target based on what is already posted
				target = MathUtils.subtract(target, rep.getPendingContracts());
			}

			// Adjust target based upon the value we already have in that field
			BigDecimal originalValue = getContractQuantity(rep);

			// Change Target to be the lowest "int" value
			// NOTE: Removed ROUNDING - if we need to know short vs. long that partial could mean the difference between an actual value and 0
			target = MathUtils.subtract(target, originalValue);
		}
		return MathUtils.round(target, 0, BigDecimal.ROUND_DOWN);
	}


	private void addContractQuantity(ProductOverlayAssetClassReplication rep, BigDecimal value) {
		BigDecimal currentValue = getContractQuantity(rep);
		BigDecimal newValue = MathUtils.add(value, currentValue);
		BeanUtils.setPropertyValue(rep, this.type.getFieldName(), newValue);
	}


	public BigDecimal getCurrentShortTargetTotal() {
		return getCurrentTargetTotal(true);
	}


	public BigDecimal getCurrentLongTargetTotal() {
		return getCurrentTargetTotal(false);
	}


	private BigDecimal getCurrentTargetTotal(boolean shortTarget) {
		BigDecimal total = BigDecimal.ZERO;
		for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(this.replicationList)) {
			BigDecimal target = getTargetForReplication(rep, true); //this.replicationTargetMap.get(key);
			if (MathUtils.isLessThan(target, BigDecimal.ZERO) == shortTarget) {
				total = MathUtils.add(total, target);
			}
		}
		return total;
	}


	private BigDecimal getExplicitTargetTotal() {
		if (CollectionUtils.isEmpty(this.positionAllocationQuantityMap) && CollectionUtils.isEmpty(this.tradePositionAllocationQuantityMap)) {
			return null;
		}
		BigDecimal shortTotal = BigDecimal.ZERO;
		BigDecimal longTotal = BigDecimal.ZERO;
		for (String key : this.replicationTargetMap.keySet()) {
			if (this.positionAllocationQuantityMap.containsKey(key)) {
				BigDecimal target = getTargetForReplication(this.replicationMap.get(key), true);
				if (MathUtils.isLessThan(target, BigDecimal.ZERO)) {
					shortTotal = MathUtils.add(shortTotal, target);
				}
				else {
					longTotal = MathUtils.add(longTotal, target);
				}
			}
		}

		shortTotal = MathUtils.abs(shortTotal);
		if (MathUtils.isGreaterThan(longTotal, shortTotal)) {
			return longTotal;
		}
		return shortTotal;
	}


	private List<ProductOverlayAssetClassReplication> getShortReplicationListInOrder(boolean skipExplicit) {
		return getReplicationListInOrder(true, skipExplicit);
	}


	private List<ProductOverlayAssetClassReplication> getLongReplicationListInOrder(boolean skipExplicit) {
		return getReplicationListInOrder(false, skipExplicit);
	}


	private List<ProductOverlayAssetClassReplication> getReplicationListInOrder(boolean shortList, boolean skipExplicit) {
		// Otherwise need to sort by smallest target first or specified order
		List<ProductOverlayAssetClassReplication> sortedList = new ArrayList<>();
		List<String> keysChecked = new ArrayList<>();
		// First By Explicit Order
		for (Map.Entry<String, BigDecimal> stringBigDecimalEntry : this.replicationTargetMap.entrySet()) {
			// When retrieving list for those held in wrong direct to apply contracts to
			// we want to skip explicit and only apply to the rest of the asset classes - explicit should always be exact
			// and we can't add to shorts, or remove longs
			if (skipExplicit && (this.positionAllocationQuantityMap.containsKey(stringBigDecimalEntry.getKey()) || this.tradePositionAllocationQuantityMap.containsKey(stringBigDecimalEntry.getKey()))) {
				keysChecked.add(stringBigDecimalEntry.getKey());
				continue;
			}

			// If Pending - we want the target based on what's already been allocated.
			// i.e. Targets may all be long, but we are selling a portion of the contract
			BigDecimal target = (this.type.isPending() ? getTargetForReplication(this.replicationMap.get(stringBigDecimalEntry.getKey()), true) : stringBigDecimalEntry.getValue());
			if (shortList == MathUtils.isLessThan(target, BigDecimal.ZERO)) {
				if (this.positionAllocationOrderMap.containsKey(stringBigDecimalEntry.getKey())) {
					keysChecked.add(stringBigDecimalEntry.getKey());

					// If explicit and fulfilled - skip it
					if (MathUtils.isEqual(target, BigDecimal.ZERO)) {
						continue;
					}
					Short order = this.positionAllocationOrderMap.get(stringBigDecimalEntry.getKey());
					// Nothing there yet - put in the list
					if (CollectionUtils.isEmpty(sortedList)) {
						sortedList.add(this.replicationMap.get(stringBigDecimalEntry.getKey()));
					}
					else {
						for (int i = 0; i < sortedList.size(); i++) {
							ProductOverlayAssetClassReplication sortedRep = sortedList.get(i);
							Short sortedOrder = this.positionAllocationOrderMap.get(getKeyForReplication(sortedRep));
							if (MathUtils.isLessThan(order, sortedOrder)) {
								sortedList.add(i, this.replicationMap.get(stringBigDecimalEntry.getKey()));
								break;
							}
							else if (i == (sortedList.size() - 1)) {
								sortedList.add(this.replicationMap.get(stringBigDecimalEntry.getKey()));
								break;
							}
						}
					}
				}
			}
			else {
				// If held in opposite direction - i.e. looking for shorts, but this is a long
				// skip it
				keysChecked.add(stringBigDecimalEntry.getKey());
			}
		}
		int currentIndex = CollectionUtils.getSize(sortedList);

		// Then By Explicit Split Exists Sorted By Target Asc
		for (Map.Entry<String, BigDecimal> stringBigDecimalEntry : this.replicationTargetMap.entrySet()) {
			// Already explicitly sorted
			if (keysChecked.contains(stringBigDecimalEntry.getKey())) {
				continue;
			}
			if (this.positionAllocationQuantityMap.containsKey(stringBigDecimalEntry.getKey()) || this.tradePositionAllocationQuantityMap.containsKey(stringBigDecimalEntry.getKey())) {
				keysChecked.add(stringBigDecimalEntry.getKey());
				BigDecimal target = stringBigDecimalEntry.getValue();
				// If explicit and fulfilled - skip it
				if (MathUtils.isEqual(target, BigDecimal.ZERO)) {
					continue;
				}
				// Nothing there yet - put in the list
				if (CollectionUtils.isEmpty(sortedList) || (currentIndex == CollectionUtils.getSize(sortedList))) {
					sortedList.add(this.replicationMap.get(stringBigDecimalEntry.getKey()));
				}
				else {
					for (int i = currentIndex; i < sortedList.size(); i++) {
						ProductOverlayAssetClassReplication sortedRep = sortedList.get(i);
						BigDecimal sortedTarget = getTargetForReplication(sortedRep, false);

						if (MathUtils.isLessThan(MathUtils.abs(target), MathUtils.abs(sortedTarget))) {
							sortedList.add(i, this.replicationMap.get(stringBigDecimalEntry.getKey()));
							break;
						}
						else if (i == (sortedList.size() - 1)) {
							sortedList.add(this.replicationMap.get(stringBigDecimalEntry.getKey()));
							break;
						}
					}
				}
			}
		}
		currentIndex = CollectionUtils.getSize(sortedList);

		// Finally - By Target Asc
		for (Map.Entry<String, BigDecimal> stringBigDecimalEntry : this.replicationTargetMap.entrySet()) {
			if (keysChecked.contains(stringBigDecimalEntry.getKey())) {
				continue;
			}
			BigDecimal target = stringBigDecimalEntry.getValue();
			// Nothing there yet - put in the list
			if (CollectionUtils.isEmpty(sortedList) || (currentIndex == CollectionUtils.getSize(sortedList))) {
				sortedList.add(this.replicationMap.get(stringBigDecimalEntry.getKey()));
			}
			else {
				for (int i = currentIndex; i < sortedList.size(); i++) {
					ProductOverlayAssetClassReplication sortedRep = sortedList.get(i);
					BigDecimal sortedTarget = getTargetForReplication(sortedRep, false);

					if (MathUtils.isLessThanOrEqual(MathUtils.abs(target), MathUtils.abs(sortedTarget))) {
						sortedList.add(i, this.replicationMap.get(stringBigDecimalEntry.getKey()));
						break;
					}
					else if (i == (sortedList.size() - 1)) {
						sortedList.add(this.replicationMap.get(stringBigDecimalEntry.getKey()));
						break;
					}
				}
			}
		}
		return sortedList;
	}


	/**
	 * Generates the # of virtual contracts based on short and long targets which is the value of the smaller of the abs value of the two targets
	 * <p>
	 * If either is 0, returns zero.
	 * If they are the same sign - returns zero;
	 * <p>
	 * Value returns is rounded down to whole value - no partial allocations allowed
	 */
	protected BigDecimal generateVirtualContractQuantity(BigDecimal shortTarget, BigDecimal longTarget, BigDecimal forceTarget) {
		if (!MathUtils.isNullOrZero(forceTarget)) {
			return MathUtils.abs(forceTarget);
		}
		if (!MathUtils.isNullOrZero(shortTarget) && !MathUtils.isNullOrZero(longTarget)) {
			if (!MathUtils.isSameSignum(shortTarget, longTarget)) {
				BigDecimal result = longTarget;
				if (MathUtils.isLessThanOrEqual(MathUtils.abs(shortTarget), longTarget)) {
					result = MathUtils.abs(shortTarget);
				}
				return MathUtils.round(result, 0, BigDecimal.ROUND_DOWN);
			}
		}
		return BigDecimal.ZERO;
	}
}
