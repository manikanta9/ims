package com.clifton.product.ldi;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;
import java.util.List;


/**
 * The <code>ProductLDIBalanceEntry</code> is used to allow users to easily enter balance info for a list of maturities
 * for a specific clientInvestmentAccountId
 *
 * @author Mary Anderson
 */
@NonPersistentObject
public class ProductLDIBalanceEntry {

	private InvestmentAccount clientInvestmentAccount;
	private Date balanceDate;

	private List<ProductLDIBalance> balanceList;


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public List<ProductLDIBalance> getBalanceList() {
		return this.balanceList;
	}


	public void setBalanceList(List<ProductLDIBalance> balanceList) {
		this.balanceList = balanceList;
	}
}
