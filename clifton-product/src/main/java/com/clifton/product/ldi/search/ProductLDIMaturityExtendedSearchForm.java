package com.clifton.product.ldi.search;


import java.util.Date;


/**
 * The <code>ProductLDIMaturityExtendedSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class ProductLDIMaturityExtendedSearchForm extends ProductLDIMaturitySearchForm {

	// Custom Search Field
	private Date balanceDate;


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}
}
