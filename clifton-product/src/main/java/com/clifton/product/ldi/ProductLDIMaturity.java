package com.clifton.product.ldi;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.investment.replication.InvestmentReplication;


/**
 * The <code>ProductLDIMaturity</code> class defines a single maturity period.
 * Examples: 0-3 years; 3-5 years; 5-10 years; 10+ years.
 * <p/>
 * Each position falls into one of these maturity periods based on replication allocation
 * (usually used for futures) or days to maturity [minDays, maxDays] (usually for bonds).
 *
 * @author Mary Anderson
 */
public class ProductLDIMaturity extends BaseEntity<Integer> implements LabeledObject {

	private String label;

	/**
	 * Securities maturing within the following days range fall into this category.
	 * For example:
	 * 0-3 years: periodMinDays = 0, periodMaxDays = 3*365
	 * 3-5 years: periodMinDays = 3*365+1, periodMaxDays = 5*365
	 */
	private int periodMinDays;
	private int periodMaxDays;

	private InvestmentAccount clientInvestmentAccount;
	private InvestmentReplication replication;

	private InvestmentSecurity assetsProxyBenchmarkSecurity;
	private InvestmentInterestRateIndex assetsProxyBenchmarkInterestRateIndex;

	private InvestmentSecurity liabilitiesProxyBenchmarkSecurity;
	private InvestmentInterestRateIndex liabilitiesProxyBenchmarkInterestRateIndex;


	//////////////////////////////////////////


	public String getLabelLong() {
		if (getClientInvestmentAccount() != null) {
			return getClientInvestmentAccount().getLabel() + " - " + getLabel();
		}
		return getLabel();
	}


	//////////////////////////////////////////


	@Override
	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public InvestmentReplication getReplication() {
		return this.replication;
	}


	public void setReplication(InvestmentReplication replication) {
		this.replication = replication;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentSecurity getAssetsProxyBenchmarkSecurity() {
		return this.assetsProxyBenchmarkSecurity;
	}


	public void setAssetsProxyBenchmarkSecurity(InvestmentSecurity assetsProxyBenchmarkSecurity) {
		this.assetsProxyBenchmarkSecurity = assetsProxyBenchmarkSecurity;
	}


	public InvestmentSecurity getLiabilitiesProxyBenchmarkSecurity() {
		return this.liabilitiesProxyBenchmarkSecurity;
	}


	public void setLiabilitiesProxyBenchmarkSecurity(InvestmentSecurity liabilitiesProxyBenchmarkSecurity) {
		this.liabilitiesProxyBenchmarkSecurity = liabilitiesProxyBenchmarkSecurity;
	}


	public InvestmentInterestRateIndex getAssetsProxyBenchmarkInterestRateIndex() {
		return this.assetsProxyBenchmarkInterestRateIndex;
	}


	public void setAssetsProxyBenchmarkInterestRateIndex(InvestmentInterestRateIndex assetsProxyBenchmarkInterestRateIndex) {
		this.assetsProxyBenchmarkInterestRateIndex = assetsProxyBenchmarkInterestRateIndex;
	}


	public InvestmentInterestRateIndex getLiabilitiesProxyBenchmarkInterestRateIndex() {
		return this.liabilitiesProxyBenchmarkInterestRateIndex;
	}


	public void setLiabilitiesProxyBenchmarkInterestRateIndex(InvestmentInterestRateIndex liabilitiesProxyBenchmarkInterestRateIndex) {
		this.liabilitiesProxyBenchmarkInterestRateIndex = liabilitiesProxyBenchmarkInterestRateIndex;
	}


	public int getPeriodMinDays() {
		return this.periodMinDays;
	}


	public void setPeriodMinDays(int periodMinDays) {
		this.periodMinDays = periodMinDays;
	}


	public int getPeriodMaxDays() {
		return this.periodMaxDays;
	}


	public void setPeriodMaxDays(int periodMaxDays) {
		this.periodMaxDays = periodMaxDays;
	}
}
