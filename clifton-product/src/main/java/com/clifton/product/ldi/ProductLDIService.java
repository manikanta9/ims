package com.clifton.product.ldi;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.product.ldi.search.ProductLDIBalanceSearchForm;
import com.clifton.product.ldi.search.ProductLDIMaturityExtendedSearchForm;
import com.clifton.product.ldi.search.ProductLDIMaturitySearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>ProductLDIService</code> ...
 *
 * @author Mary Anderson
 */
public interface ProductLDIService {

	////////////////////////////////////////////////////////////////////////////
	///////            Product LDI Maturity Business Methods            ////////
	////////////////////////////////////////////////////////////////////////////


	public ProductLDIMaturity getProductLDIMaturity(int id);


	public List<ProductLDIMaturity> getProductLDIMaturityList(ProductLDIMaturitySearchForm searchForm);


	public ProductLDIMaturity saveProductLDIMaturity(ProductLDIMaturity bean);


	public void deleteProductLDIMaturity(int id);


	////////////////////////////////////////////////////////////////////////////
	///////       Product LDI Maturity Extended Business Methods        ////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = ProductLDIMaturity.class)
	public List<ProductLDIMaturityExtended> getProductLDIMaturityExtendedList(ProductLDIMaturityExtendedSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	///////            Product LDI Balance Business Methods            ////////
	////////////////////////////////////////////////////////////////////////////


	public List<ProductLDIBalance> getProductLDIBalanceList(ProductLDIBalanceSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	///////         Product LDI Balance Entry Business Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = ProductLDIBalance.class)
	public ProductLDIBalanceEntry getProductLDIBalanceEntry(int clientInvestmentAccountId, Date balanceDate);


	@SecureMethod(dtoClass = ProductLDIBalance.class)
	public void saveProductLDIBalanceEntry(ProductLDIBalanceEntry bean);


	@SecureMethod(dtoClass = ProductLDIBalance.class)
	public void deleteProductLDIBalanceEntry(int clientInvestmentAccountId, Date balanceDate);
}
