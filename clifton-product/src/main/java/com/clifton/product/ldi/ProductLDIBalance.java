package com.clifton.product.ldi;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>ProductLDIBalance</code> ...
 *
 * @author Mary Anderson
 */
public class ProductLDIBalance extends BaseEntity<Integer> implements LabeledObject {

	private ProductLDIMaturity maturity;
	private Date balanceDate;
	private BigDecimal assetsBalance;
	private BigDecimal assetsDV01;
	private BigDecimal liabilitiesBalance;
	private BigDecimal liabilitiesDV01;


	////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		String label = "";
		if (getMaturity() != null) {
			label = getMaturity().getLabel();
		}
		return label + " on " + DateUtils.fromDateShort(getBalanceDate());
	}


	////////////////////////////////////////////////////


	public ProductLDIMaturity getMaturity() {
		return this.maturity;
	}


	public void setMaturity(ProductLDIMaturity maturity) {
		this.maturity = maturity;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public BigDecimal getAssetsBalance() {
		return this.assetsBalance;
	}


	public void setAssetsBalance(BigDecimal assetsBalance) {
		this.assetsBalance = assetsBalance;
	}


	public BigDecimal getAssetsDV01() {
		return this.assetsDV01;
	}


	public void setAssetsDV01(BigDecimal assetsDV01) {
		this.assetsDV01 = assetsDV01;
	}


	public BigDecimal getLiabilitiesBalance() {
		return this.liabilitiesBalance;
	}


	public void setLiabilitiesBalance(BigDecimal liabilitiesBalance) {
		this.liabilitiesBalance = liabilitiesBalance;
	}


	public BigDecimal getLiabilitiesDV01() {
		return this.liabilitiesDV01;
	}


	public void setLiabilitiesDV01(BigDecimal liabilitiesDV01) {
		this.liabilitiesDV01 = liabilitiesDV01;
	}
}
