package com.clifton.product.ldi;

import com.clifton.core.dataaccess.dao.NonPersistentObject;


/**
 * The <code>ProductLDIMaturityExtended</code> ...
 *
 * @author Mary Anderson
 */
@NonPersistentObject
public class ProductLDIMaturityExtended extends ProductLDIMaturity {

	private ProductLDIBalance balance;


	public ProductLDIBalance getBalance() {
		return this.balance;
	}


	public void setBalance(ProductLDIBalance balance) {
		this.balance = balance;
	}
}
