package com.clifton.product.ldi.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>ProductLDIBalanceSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class ProductLDIBalanceSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "maturity.id")
	private Integer maturityId;

	@SearchField(searchFieldPath = "maturity", searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField
	private Date balanceDate;

	@SearchField
	private BigDecimal assetsBalance;

	@SearchField
	private BigDecimal assetsDV01;

	@SearchField
	private BigDecimal liabilitiesBalance;

	@SearchField
	private BigDecimal liabilitiesDV01;


	public Integer getMaturityId() {
		return this.maturityId;
	}


	public void setMaturityId(Integer maturityId) {
		this.maturityId = maturityId;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public BigDecimal getAssetsBalance() {
		return this.assetsBalance;
	}


	public void setAssetsBalance(BigDecimal assetsBalance) {
		this.assetsBalance = assetsBalance;
	}


	public BigDecimal getAssetsDV01() {
		return this.assetsDV01;
	}


	public void setAssetsDV01(BigDecimal assetsDV01) {
		this.assetsDV01 = assetsDV01;
	}


	public BigDecimal getLiabilitiesBalance() {
		return this.liabilitiesBalance;
	}


	public void setLiabilitiesBalance(BigDecimal liabilitiesBalance) {
		this.liabilitiesBalance = liabilitiesBalance;
	}


	public BigDecimal getLiabilitiesDV01() {
		return this.liabilitiesDV01;
	}


	public void setLiabilitiesDV01(BigDecimal liabilitiesDV01) {
		this.liabilitiesDV01 = liabilitiesDV01;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}
}
