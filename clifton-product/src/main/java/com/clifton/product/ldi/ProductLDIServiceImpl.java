package com.clifton.product.ldi;


import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.product.ldi.search.ProductLDIBalanceSearchForm;
import com.clifton.product.ldi.search.ProductLDIMaturityExtendedSearchForm;
import com.clifton.product.ldi.search.ProductLDIMaturitySearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>ProductLDIServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class ProductLDIServiceImpl implements ProductLDIService {

	private AdvancedUpdatableDAO<ProductLDIMaturity, Criteria> productLDIMaturityDAO;
	private AdvancedUpdatableDAO<ProductLDIBalance, Criteria> productLDIBalanceDAO;

	private AdvancedReadOnlyDAO<ProductLDIMaturityExtended, Criteria> productLDIMaturityExtendedDAO;

	private InvestmentAccountService investmentAccountService;


	////////////////////////////////////////////////////////////////////////////
	///////            Product LDI Maturity Business Methods            ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ProductLDIMaturity getProductLDIMaturity(int id) {
		return getProductLDIMaturityDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ProductLDIMaturity> getProductLDIMaturityList(ProductLDIMaturitySearchForm searchForm) {
		return getProductLDIMaturityDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ProductLDIMaturity saveProductLDIMaturity(final ProductLDIMaturity bean) {
		ValidationUtils.assertFalse(bean.getAssetsProxyBenchmarkSecurity() != null && bean.getAssetsProxyBenchmarkInterestRateIndex() != null,
				"Please select either an assets benchmark security or interest rate index, but not both.");
		ValidationUtils.assertFalse(bean.getLiabilitiesProxyBenchmarkSecurity() != null && bean.getLiabilitiesProxyBenchmarkInterestRateIndex() != null,
				"Please select either a liabilities benchmark security or interest rate index, but not both.");
		ValidationUtils.assertTrue(bean.getPeriodMaxDays() > bean.getPeriodMinDays(), "Period Max Days for maturity period must be greater than Period Min Days.");
		// check for overlaps
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			criteria.add(Restrictions.eq("clientInvestmentAccount.id", bean.getClientInvestmentAccount().getId()));
			if (!bean.isNewBean()) {
				criteria.add(Restrictions.ne("id", bean.getId()));
			}
			Junction c1 = Restrictions.conjunction().add(Restrictions.le("periodMinDays", bean.getPeriodMinDays())).add(Restrictions.ge("periodMaxDays", bean.getPeriodMinDays()));
			Junction c2 = Restrictions.conjunction().add(Restrictions.ge("periodMinDays", bean.getPeriodMinDays())).add(Restrictions.le("periodMaxDays", bean.getPeriodMaxDays()));
			criteria.add(Restrictions.disjunction().add(c1).add(c2));
		};
		ProductLDIMaturity firstViolation = CollectionUtils.getFirstElement(getProductLDIMaturityDAO().findBySearchCriteria(searchConfigurer));
		if (firstViolation != null) {
			throw new ValidationException("Period min/max days cannot overlap with another maturity: " + firstViolation.getLabel());
		}

		return getProductLDIMaturityDAO().save(bean);
	}


	@Override
	public void deleteProductLDIMaturity(int id) {
		getProductLDIMaturityDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////       Product LDI Maturity Extended Business Methods        ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ProductLDIMaturityExtended> getProductLDIMaturityExtendedList(ProductLDIMaturityExtendedSearchForm searchForm) {
		ValidationUtils.assertNotNull(searchForm.getBalanceDate(), "Balance Date is required for search.");
		final Date balanceDate = searchForm.getBalanceDate();
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				criteria.add(Restrictions.sqlRestriction("COALESCE(" + criteria.getAlias()
								+ "_.BalanceDate, '') = (SELECT COALESCE(MAX(b2.BalanceDate),'') FROM ProductLDIBalance b2 WHERE b2.ProductLDIMaturityID = " + criteria.getAlias()
								+ "_.ProductLDIMaturityID AND b2.BalanceDate <= ?)",
						DateUtils.fromDateShort(balanceDate),
						StringType.INSTANCE));
			}
		};
		return getProductLDIMaturityExtendedDAO().findBySearchCriteria(config);
	}


	////////////////////////////////////////////////////////////////////////////
	///////            Product LDI Balance Business Methods            /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ProductLDIBalance> getProductLDIBalanceList(ProductLDIBalanceSearchForm searchForm) {
		return getProductLDIBalanceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	///////         Product LDI Balance Entry Business Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ProductLDIBalanceEntry getProductLDIBalanceEntry(int clientInvestmentAccountId, Date balanceDate) {
		ValidationUtils.assertNotNull(balanceDate, "Balance Date is required for Maturity Balance entry.");
		ProductLDIBalanceEntry bean = new ProductLDIBalanceEntry();
		bean.setClientInvestmentAccount(getInvestmentAccountService().getInvestmentAccount(clientInvestmentAccountId));
		bean.setBalanceDate(balanceDate);

		List<ProductLDIMaturity> maturityList = getProductLDIMaturityDAO().findByField("clientInvestmentAccount.id", clientInvestmentAccountId);

		ProductLDIBalanceSearchForm searchForm = new ProductLDIBalanceSearchForm();
		searchForm.setClientInvestmentAccountId(clientInvestmentAccountId);
		searchForm.setBalanceDate(balanceDate);
		List<ProductLDIBalance> balanceList = getProductLDIBalanceList(searchForm);
		if (balanceList == null) {
			balanceList = new ArrayList<>();
		}
		for (ProductLDIMaturity m : CollectionUtils.getIterable(maturityList)) {

			boolean found = false;
			for (ProductLDIBalance b : CollectionUtils.getIterable(balanceList)) {
				if (b.getMaturity().equals(m)) {

					found = true;
					break;
				}
			}
			if (!found) {

				ProductLDIBalance b = new ProductLDIBalance();
				b.setMaturity(m);
				balanceList.add(b);
			}
		}

		bean.setBalanceList(balanceList);
		return bean;
	}


	@Override
	public void saveProductLDIBalanceEntry(ProductLDIBalanceEntry bean) {
		List<ProductLDIBalance> balanceList = new ArrayList<>();
		for (ProductLDIBalance b : CollectionUtils.getIterable(bean.getBalanceList())) {
			ValidationUtils.assertTrue(b.getAssetsBalance() != null && b.getAssetsDV01() != null && b.getLiabilitiesBalance() != null && b.getLiabilitiesDV01() != null,
					"Balance and DV01 information is required for all maturities.  Please enter all balance/DV01 values that are missing for maturity [" + b.getMaturity().getLabel() + "]");

			b.setBalanceDate(bean.getBalanceDate());
			balanceList.add(b);
		}
		getProductLDIBalanceDAO().saveList(balanceList);
	}


	@Override
	public void deleteProductLDIBalanceEntry(int clientInvestmentAccountId, Date balanceDate) {
		ValidationUtils.assertNotNull(balanceDate, "Balance Date is required to delete Maturity Balance entry.");
		ProductLDIBalanceSearchForm searchForm = new ProductLDIBalanceSearchForm();
		searchForm.setClientInvestmentAccountId(clientInvestmentAccountId);
		searchForm.setBalanceDate(balanceDate);
		List<ProductLDIBalance> balanceList = getProductLDIBalanceList(searchForm);
		getProductLDIBalanceDAO().deleteList(balanceList);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                Getter & Setter Methods                ///////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ProductLDIMaturity, Criteria> getProductLDIMaturityDAO() {
		return this.productLDIMaturityDAO;
	}


	public void setProductLDIMaturityDAO(AdvancedUpdatableDAO<ProductLDIMaturity, Criteria> productLDIMaturityDAO) {
		this.productLDIMaturityDAO = productLDIMaturityDAO;
	}


	public AdvancedUpdatableDAO<ProductLDIBalance, Criteria> getProductLDIBalanceDAO() {
		return this.productLDIBalanceDAO;
	}


	public void setProductLDIBalanceDAO(AdvancedUpdatableDAO<ProductLDIBalance, Criteria> productLDIBalanceDAO) {
		this.productLDIBalanceDAO = productLDIBalanceDAO;
	}


	public AdvancedReadOnlyDAO<ProductLDIMaturityExtended, Criteria> getProductLDIMaturityExtendedDAO() {
		return this.productLDIMaturityExtendedDAO;
	}


	public void setProductLDIMaturityExtendedDAO(AdvancedReadOnlyDAO<ProductLDIMaturityExtended, Criteria> productLDIMaturityExtendedDAO) {
		this.productLDIMaturityExtendedDAO = productLDIMaturityExtendedDAO;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
