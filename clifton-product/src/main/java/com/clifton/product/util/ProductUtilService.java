package com.clifton.product.util;


import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAccount;


/**
 * The <code>ProductUtilService</code> contains common lookup & processing methods used throughout
 * different parts of the clifton-product project.
 *
 * @author Mary Anderson
 */
public interface ProductUtilService {

	////////////////////////////////////////////////////////////////////////////
	/////////            PIOS Custom Field Value Methods               /////////
	////////////////////////////////////////////////////////////////////////////


	public Object getInvestmentAccountAssetClassCustomFieldValue(InvestmentAccountAssetClass accountAssetClass, String fieldName);


	public Object getInvestmentAccountAssetClassCustomFieldValue(InvestmentAccountAssetClass accountAssetClass, String columnGroupName, String fieldName);


	public Object getInvestmentManagerAccountLinkedManagerCustomFieldValue(InvestmentManagerAccount managerAccount, String fieldName);


	public Object getInvestmentSecurityCustomFieldValue(InvestmentSecurity security, String fieldName);
}
