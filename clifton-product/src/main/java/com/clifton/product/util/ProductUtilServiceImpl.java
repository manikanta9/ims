package com.clifton.product.util;


import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.system.schema.column.SystemColumnCustomAware;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.springframework.stereotype.Component;


/**
 * The <code>ProductUtilServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Component
public class ProductUtilServiceImpl implements ProductUtilService {

	private SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////

	public static final String INVESTMENT_ACCOUNT_ASSET_CLASS_CUSTOM_FIELD_GROUP = "Account Asset Class Custom Fields";
	public static final String INVESTMENT_MANAGER_ACCOUNT_LINKED_MANAGER_CUSTOM_FIELD_GROUP = "Linked Manager";

	public static final String INVESTMENT_ACCOUNT_EXCLUDE_MISPRICING_FIELD_NAME = "Do Not Perform Mispricing";
	public static final String INVESTMENT_ACCOUNT_DO_NOT_ADJUST_REPLICATION_ALLOCATION_PERCENTAGES = "Do Not Adjust Replication Allocation Target Percentages";


	////////////////////////////////////////////////////////////////////////////
	/////////            PIOS Custom Field Value Methods               /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Object getInvestmentAccountAssetClassCustomFieldValue(InvestmentAccountAssetClass accountAssetClass, String fieldName) {
		return getCustomFieldValue(accountAssetClass, INVESTMENT_ACCOUNT_ASSET_CLASS_CUSTOM_FIELD_GROUP, fieldName);
	}


	@Override
	public Object getInvestmentAccountAssetClassCustomFieldValue(InvestmentAccountAssetClass accountAssetClass, String columnGroupName, String fieldName) {
		return getCustomFieldValue(accountAssetClass, columnGroupName, fieldName);
	}


	@Override
	public Object getInvestmentManagerAccountLinkedManagerCustomFieldValue(InvestmentManagerAccount managerAccount, String fieldName) {
		return getCustomFieldValue(managerAccount, INVESTMENT_MANAGER_ACCOUNT_LINKED_MANAGER_CUSTOM_FIELD_GROUP, fieldName);
	}


	@Override
	public Object getInvestmentSecurityCustomFieldValue(InvestmentSecurity security, String fieldName) {
		return getCustomFieldValue(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, fieldName);
	}


	private Object getCustomFieldValue(SystemColumnCustomAware bean, String groupName, String fieldName) {
		return getSystemColumnValueHandler().getSystemColumnValueForEntity(bean, groupName, fieldName, true);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
