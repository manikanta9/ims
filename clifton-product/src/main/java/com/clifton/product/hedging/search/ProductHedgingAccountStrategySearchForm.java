package com.clifton.product.hedging.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.math.BigDecimal;


/**
 * The <code>ProductHedgingAccountStrategySearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class ProductHedgingAccountStrategySearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "underlyingSecurity.id")
	private Integer underlyingSecurityId;

	@SearchField
	private BigDecimal underlyingInitialPrice;

	@SearchField
	private BigDecimal underlyingInitialQuantity;

	@SearchField
	private String strategyStructure;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getUnderlyingSecurityId() {
		return this.underlyingSecurityId;
	}


	public void setUnderlyingSecurityId(Integer underlyingSecurityId) {
		this.underlyingSecurityId = underlyingSecurityId;
	}


	public String getStrategyStructure() {
		return this.strategyStructure;
	}


	public void setStrategyStructure(String strategyStructure) {
		this.strategyStructure = strategyStructure;
	}


	public BigDecimal getUnderlyingInitialPrice() {
		return this.underlyingInitialPrice;
	}


	public void setUnderlyingInitialPrice(BigDecimal underlyingInitialPrice) {
		this.underlyingInitialPrice = underlyingInitialPrice;
	}


	public BigDecimal getUnderlyingInitialQuantity() {
		return this.underlyingInitialQuantity;
	}


	public void setUnderlyingInitialQuantity(BigDecimal underlyingInitialQuantity) {
		this.underlyingInitialQuantity = underlyingInitialQuantity;
	}
}
