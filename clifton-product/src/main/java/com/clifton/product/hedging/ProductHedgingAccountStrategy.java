package com.clifton.product.hedging;


import com.clifton.core.beans.NamedEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>ProductHedgingAccountStrategy</code> ...
 *
 * @author Mary Anderson
 */
public class ProductHedgingAccountStrategy extends NamedEntity<Integer> {

	private InvestmentAccount clientInvestmentAccount;

	private InvestmentSecurity underlyingSecurity;
	private BigDecimal underlyingInitialPrice;
	private BigDecimal underlyingInitialQuantity;
	/**
	 * Depending on the type of a strategy we may need to adjust strategy notional by this value (usually 1).
	 * Strategy notional is calculated as the notional of the largest quantity (by absolute value) option position.
	 * Defensive Equity may use more than one option and we need to count both however other Protection Plus strategies
	 * may use multiple options for the same notional.
	 */
	private BigDecimal strategyNotionalMultiplier = BigDecimal.ONE;

	private String strategyStructure;

	private Date startDate;
	private Date endDate;

	private List<ProductHedgingAccountStrategyPosition> strategyPositionList;


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public String getStrategyStructure() {
		return this.strategyStructure;
	}


	public void setStrategyStructure(String strategyStructure) {
		this.strategyStructure = strategyStructure;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public List<ProductHedgingAccountStrategyPosition> getStrategyPositionList() {
		return this.strategyPositionList;
	}


	public void setStrategyPositionList(List<ProductHedgingAccountStrategyPosition> strategyPositionList) {
		this.strategyPositionList = strategyPositionList;
	}


	public InvestmentSecurity getUnderlyingSecurity() {
		return this.underlyingSecurity;
	}


	public void setUnderlyingSecurity(InvestmentSecurity underlyingSecurity) {
		this.underlyingSecurity = underlyingSecurity;
	}


	public BigDecimal getUnderlyingInitialPrice() {
		return this.underlyingInitialPrice;
	}


	public void setUnderlyingInitialPrice(BigDecimal underlyingInitialPrice) {
		this.underlyingInitialPrice = underlyingInitialPrice;
	}


	public BigDecimal getUnderlyingInitialQuantity() {
		return this.underlyingInitialQuantity;
	}


	public void setUnderlyingInitialQuantity(BigDecimal underlyingInitialQuantity) {
		this.underlyingInitialQuantity = underlyingInitialQuantity;
	}


	public BigDecimal getStrategyNotionalMultiplier() {
		return this.strategyNotionalMultiplier;
	}


	public void setStrategyNotionalMultiplier(BigDecimal strategyNotionalMultiplier) {
		this.strategyNotionalMultiplier = strategyNotionalMultiplier;
	}
}
