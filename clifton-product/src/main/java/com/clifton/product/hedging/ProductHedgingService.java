package com.clifton.product.hedging;


import com.clifton.product.hedging.search.ProductHedgingAccountStrategySearchForm;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ProductHedgingService</code> ...
 *
 * @author Mary Anderson
 */
public interface ProductHedgingService {

	////////////////////////////////////////////////////////////////////////////
	///////    Product Hedging Account Strategy Business Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	public ProductHedgingAccountStrategy getProductHedgingAccountStrategy(int id);


	public List<ProductHedgingAccountStrategy> getProductHedgingAccountStrategyList(ProductHedgingAccountStrategySearchForm searchForm);


	public ProductHedgingAccountStrategy saveProductHedgingAccountStrategy(ProductHedgingAccountStrategy bean);


	public void deleteProductHedgingAccountStrategy(int id);


	////////////////////////////////////////////////////////////////////////////
	/////  Product Hedging Account Strategy Position Business Methods     //////
	////////////////////////////////////////////////////////////////////////////


	public ProductHedgingAccountStrategyPosition getProductHedgingAccountStrategyPosition(int id);


	public List<ProductHedgingAccountStrategyPosition> getProductHedgingAccountStrategyPositionListByStrategy(int strategyId);


	public void linkProductHedgingAccountStrategyPosition(int strategyId, long accountingTransactionId, BigDecimal allocationQuantity);


	public void deleteProductHedgingAccountStrategyPosition(int id);
}
