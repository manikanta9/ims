package com.clifton.product.hedging;


import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.product.hedging.search.ProductHedgingAccountStrategySearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ProductHedgingServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class ProductHedgingServiceImpl implements ProductHedgingService {

	private AdvancedUpdatableDAO<ProductHedgingAccountStrategy, Criteria> productHedgingAccountStrategyDAO;
	private AdvancedUpdatableDAO<ProductHedgingAccountStrategyPosition, Criteria> productHedgingAccountStrategyPositionDAO;

	private AccountingTransactionService accountingTransactionService;


	////////////////////////////////////////////////////////////////////////////
	///////    Product Hedging Account Strategy Business Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ProductHedgingAccountStrategy getProductHedgingAccountStrategy(int id) {
		return getProductHedgingAccountStrategyDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ProductHedgingAccountStrategy> getProductHedgingAccountStrategyList(ProductHedgingAccountStrategySearchForm searchForm) {
		return getProductHedgingAccountStrategyDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ProductHedgingAccountStrategy saveProductHedgingAccountStrategy(ProductHedgingAccountStrategy bean) {
		return getProductHedgingAccountStrategyDAO().save(bean);
	}


	@Override
	public void deleteProductHedgingAccountStrategy(int id) {
		getProductHedgingAccountStrategyPositionDAO().deleteList(getProductHedgingAccountStrategyPositionListByStrategy(id));
		getProductHedgingAccountStrategyDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	/////  Product Hedging Account Strategy Position Business Methods     //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ProductHedgingAccountStrategyPosition getProductHedgingAccountStrategyPosition(int id) {
		return getProductHedgingAccountStrategyPositionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ProductHedgingAccountStrategyPosition> getProductHedgingAccountStrategyPositionListByStrategy(int strategyId) {
		return getProductHedgingAccountStrategyPositionDAO().findByField("strategy.id", strategyId);
	}


	@Override
	public void linkProductHedgingAccountStrategyPosition(int strategyId, long accountingTransactionId, BigDecimal allocationQuantity) {
		ProductHedgingAccountStrategyPosition bean = new ProductHedgingAccountStrategyPosition();
		bean.setStrategy(getProductHedgingAccountStrategy(strategyId));
		bean.setAccountingTransaction(getAccountingTransactionService().getAccountingTransaction(accountingTransactionId));
		bean.setAllocationQuantity(allocationQuantity);
		getProductHedgingAccountStrategyPositionDAO().save(bean);
	}


	@Override
	public void deleteProductHedgingAccountStrategyPosition(int id) {
		getProductHedgingAccountStrategyPositionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ProductHedgingAccountStrategy, Criteria> getProductHedgingAccountStrategyDAO() {
		return this.productHedgingAccountStrategyDAO;
	}


	public void setProductHedgingAccountStrategyDAO(AdvancedUpdatableDAO<ProductHedgingAccountStrategy, Criteria> productHedgingAccountStrategyDAO) {
		this.productHedgingAccountStrategyDAO = productHedgingAccountStrategyDAO;
	}


	public AdvancedUpdatableDAO<ProductHedgingAccountStrategyPosition, Criteria> getProductHedgingAccountStrategyPositionDAO() {
		return this.productHedgingAccountStrategyPositionDAO;
	}


	public void setProductHedgingAccountStrategyPositionDAO(AdvancedUpdatableDAO<ProductHedgingAccountStrategyPosition, Criteria> productHedgingAccountStrategyPositionDAO) {
		this.productHedgingAccountStrategyPositionDAO = productHedgingAccountStrategyPositionDAO;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}
}
