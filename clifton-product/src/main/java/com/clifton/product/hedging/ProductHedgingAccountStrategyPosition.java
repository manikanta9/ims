package com.clifton.product.hedging;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.core.beans.BaseEntity;

import java.math.BigDecimal;


/**
 * The <code>ProductHedgingAccountStrategyPosition</code> ...
 *
 * @author Mary Anderson
 */
public class ProductHedgingAccountStrategyPosition extends BaseEntity<Integer> {

	private ProductHedgingAccountStrategy strategy;

	private AccountingTransaction accountingTransaction;

	// Note: the same position maybe allocated to more than one strategy. Set to null to allocate full amount.
	private BigDecimal allocationQuantity;


	public ProductHedgingAccountStrategy getStrategy() {
		return this.strategy;
	}


	public void setStrategy(ProductHedgingAccountStrategy strategy) {
		this.strategy = strategy;
	}


	public AccountingTransaction getAccountingTransaction() {
		return this.accountingTransaction;
	}


	public void setAccountingTransaction(AccountingTransaction accountingTransaction) {
		this.accountingTransaction = accountingTransaction;
	}


	public BigDecimal getAllocationQuantity() {
		return this.allocationQuantity;
	}


	public void setAllocationQuantity(BigDecimal allocationQuantity) {
		this.allocationQuantity = allocationQuantity;
	}
}
