package com.clifton.product.manager.job;

import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.product.manager.ProductManagerBalanceProcessingTypes;

import java.util.Date;


/**
 * <code>BaseAccountBalanceCommand</code> parameters necessary to run:
 * {@link com.clifton.product.manager.ProductManagerService#processProductManagerAccountBalanceAdjustmentList(AccountBalanceAdjustmentsCommand)}
 * {@link com.clifton.product.manager.ProductManagerService#processProductManagerAccountBalanceList(AccountBalanceCommand)}
 * Relies on setters not direct property population, search fields are populated with setters but use a single search field.
 *
 * @author TerryS
 */
public class BaseAccountBalanceCommand {

	// additional criteria for processing balances.
	private Date balanceDate;
	private ProductManagerBalanceProcessingTypes processingType;

	// search criteria for getting active investment manager accounts
	private InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();

	// If data already has been processed, reprocess and overwrite.
	private boolean reload;
	// Immediately process adjustments for loaded balances.
	private boolean loadProcessAdjustments = true;

	// runner specific properties.
	private boolean synchronous;
	private Status status;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getRunId() {
		StringBuilder runId = new StringBuilder();
		if (getClientId() != null) {
			runId.append("C_").append(getClientId());
		}
		else if (getInvestmentAccountGroupId() != null) {
			runId.append("AG_").append(getInvestmentAccountGroupId());
		}
		else {
			runId.append("ALL_CLIENTS");
		}
		if (getCustodianCompanyId() != null) {
			runId.append("_CUS_").append(getCustodianCompanyId());
		}
		if (getManagerType() != null) {
			runId.append("_").append(getManagerType());
		}
		else {
			runId.append("_ALL");
		}
		runId.append("_").append(DateUtils.fromDateShort(getBalanceDate()));

		if (isReload()) {
			runId.append("_RELOAD");
		}
		return runId.toString();
	}


	@Override
	public String toString() {
		return "BaseAccountBalanceCommand{" +
				"balanceDate=" + this.balanceDate +
				", processingType=" + this.processingType +
				", InvestmentAccountGroupId=" + getInvestmentAccountGroupId() +
				", ClientId=" + getClientId() +
				", CustodianCompanyId=" + getCustodianCompanyId() +
				", ManagerType=" + getManagerType() +
				", reload=" + this.reload +
				", loadProcessAdjustments=" + this.loadProcessAdjustments +
				", synchronous=" + this.synchronous +
				'}';
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	@ValueChangingSetter
	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = DateUtils.clearTime(balanceDate);
	}


	public ProductManagerBalanceProcessingTypes getProcessingType() {
		return this.processingType;
	}


	public void setProcessingType(ProductManagerBalanceProcessingTypes processingType) {
		this.processingType = processingType;
	}


	public InvestmentManagerAccountSearchForm getSearchForm() {
		return this.searchForm;
	}


	public void setSearchForm(InvestmentManagerAccountSearchForm searchForm) {
		this.searchForm = searchForm;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.searchForm.getInvestmentAccountGroupId();
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.searchForm.setInvestmentAccountGroupId(investmentAccountGroupId);
	}


	public Integer getClientId() {
		return this.searchForm.getClientId();
	}


	public void setClientId(Integer clientId) {
		this.searchForm.setClientId(clientId);
	}


	public Integer getCustodianCompanyId() {
		return this.searchForm.getCustodianCompanyId();
	}


	public void setCustodianCompanyId(Integer custodianCompanyId) {
		this.searchForm.setCustodianCompanyId(custodianCompanyId);
	}


	public InvestmentManagerAccount.ManagerTypes getManagerType() {
		return this.searchForm.getManagerType();
	}


	public void setManagerType(InvestmentManagerAccount.ManagerTypes managerType) {
		this.searchForm.setManagerType(managerType);
	}


	public boolean isReload() {
		return this.reload;
	}


	public void setReload(boolean reload) {
		this.reload = reload;
	}


	public boolean isLoadProcessAdjustments() {
		return this.loadProcessAdjustments;
	}


	public void setLoadProcessAdjustments(boolean loadProcessAdjustments) {
		this.loadProcessAdjustments = loadProcessAdjustments;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
