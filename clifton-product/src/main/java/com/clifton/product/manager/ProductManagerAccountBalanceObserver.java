package com.clifton.product.manager;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountRollup;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.rule.violation.RuleViolationUtil;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * The <code>ProductManageAccountBalanceObserver</code> is an observer for {@link InvestmentManagerAccountBalance} objects.
 * During saves, checks if the manager is a child of a rollup manager.
 * <p>
 * If it is, and the adjusted balance changes will reload and re-process the rollup manager account balance.
 * <p>
 * Rollup manager balances use their children's adjusted balance as their original balance.
 *
 * @author Mary Anderson
 */
@Component
public class ProductManagerAccountBalanceObserver extends SelfRegisteringDaoObserver<InvestmentManagerAccountBalance> {

	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	private InvestmentManagerAccountService investmentManagerAccountService;

	private ProductManagerService productManagerService;

	private RunnerHandler runnerHandler;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE_OR_DELETE;
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<InvestmentManagerAccountBalance> dao, DaoEventTypes event, InvestmentManagerAccountBalance bean, Throwable e) {
		if (e != null) {
			return;
		}

		// If deleting or adjusted balance is changed check if it's used by a rollup
		// otherwise if just an moc adjustment was entered, need to reload the moc rollup adjustments only
		boolean delete = false;
		boolean reload = false;
		boolean loadIfMissing = false;
		boolean mocChangeOnly = false;
		// If deleting, then need to delete parent balance - don't reload, because child is now deleted, or the bean is not ready for use
		if (event.isDelete() || !RuleViolationUtil.isRuleViolationAwareReadyForUse(bean)) {
			delete = true;
		}
		else if (event.isUpdate()) {
			InvestmentManagerAccountBalance original = getOriginalBean(dao, bean);
			if (original != null) {
				// If adjusted value changed, need to reload means the entire balance will need to be reloaded & processed (i.e. delete & load)
				if (!MathUtils.isEqual(original.getAdjustedCashValue(), bean.getAdjustedCashValue()) || !MathUtils.isEqual(original.getAdjustedSecuritiesValue(), bean.getAdjustedSecuritiesValue())) {
					reload = true;
				}
				// Processing was completed (may not result in change to adjusted balance)
				else if (!RuleViolationUtil.isRuleViolationAwareReadyForUse(original) && RuleViolationUtil.isRuleViolationAwareReadyForUse(bean)) {
					reload = true;
				}
				// If Only MOC Value has changed, only need to apply that adjustment up to the parent
				else if (!MathUtils.isEqual(original.getMarketOnCloseCashValue(), bean.getMarketOnCloseCashValue())
						|| !MathUtils.isEqual(original.getMarketOnCloseSecuritiesValue(), bean.getMarketOnCloseSecuritiesValue())) {

					mocChangeOnly = true;
				}
				// Otherwise, if the manager itself is a rollup manager, trigger any parents to reload if they are missing...
				else if (bean.getManagerAccount().isRollupManager()) {
					loadIfMissing = true;
				}
			}
		}

		// Only if a change warranted a change for any rollups, then look up if there are any rollups
		if (delete || reload || mocChangeOnly || loadIfMissing) {
			List<InvestmentManagerAccountRollup> rollupList = getInvestmentManagerAccountService().getInvestmentManagerAccountRollupListByChild(bean.getManagerAccount().getId());
			if (!CollectionUtils.isEmpty(rollupList)) {
				for (InvestmentManagerAccountRollup rollup : rollupList) {
					// Pull the manager account so all lists are populated
					InvestmentManagerAccount rollupManager = getInvestmentManagerAccountService().getInvestmentManagerAccount(rollup.getReferenceOne().getId());
					InvestmentManagerAccountBalance rollupBalance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(rollupManager.getId(),
							bean.getBalanceDate(), true);
					if (rollupBalance != null) {
						if (loadIfMissing) {
							// Only load the parent if it's missing
							continue;
						}
						if (mocChangeOnly) {
							rollupBalance.setManagerAccount(rollupManager);
							getProductManagerService().processProductManagerRollupMOCAdjustments(rollupBalance, bean);
							getInvestmentManagerAccountBalanceService().saveInvestmentManagerAccountBalance(rollupBalance);
						}
					}
					// If the child was actually deleted - no reason to try to reload since the child is missing the parent can't be loaded anyway
					if (!event.isDelete() && (delete || reload || loadIfMissing)) {
						// asynchronous support - Need to schedule it to run separately, because a reload would cause a delete/insert which would flush the session
						// and appears to mess up adjustment saving on this (child) balance
						// Scheduled to run in 300 milliseconds, so almost instantaneous, but if multiple children are updated at the same time, is smart enough to keep rescheduling until the last one is done (within that time frame of each other)
						final String runId = "M-" + rollupManager.getId();
						final Date scheduleDate = DateUtils.addMilliseconds(new Date(), 300);
						Runner runner = new AbstractStatusAwareRunner("INVESTMENT-ROLLUP-MANAGER-BALANCE", runId, scheduleDate) {

							@Override
							public void run() {
								getProductManagerService().processProductManagerAccountRollupBalance(rollupManager, bean.getBalanceDate());
								getStatus().setMessage("Investment Manager Account Rollup " + rollupManager.getLabel() + " Balance Reload Completed.");
							}
						};
						getRunnerHandler().rescheduleRunner(runner);
					}
				}
			}
		}
	}


	/////////////////////////////////////////////////////////////////////
	//////////            Getter & Setter Methods           /////////////
	/////////////////////////////////////////////////////////////////////


	public ProductManagerService getProductManagerService() {
		return this.productManagerService;
	}


	public void setProductManagerService(ProductManagerService productManagerService) {
		this.productManagerService = productManagerService;
	}


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
