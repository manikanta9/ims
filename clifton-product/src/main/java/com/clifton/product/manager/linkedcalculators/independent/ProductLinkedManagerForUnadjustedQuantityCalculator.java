package com.clifton.product.manager.linkedcalculators.independent;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.manager.linkedcalculators.BaseProductLinkedManagerCalculatorImpl;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ProductLinkedManagerForUnadjustedQuantityCalculator</code> handles loading
 * Securities balances from Positions Unadjusted Quantity.  Should be used for cases where quantity has a special meaning
 * Example CCY Forwards Quantity = Notional Amount denominated in the Underlying Security of the Forward
 * <p>
 *
 * @author manderson
 */
public class ProductLinkedManagerForUnadjustedQuantityCalculator extends BaseProductLinkedManagerCalculatorImpl implements ProductLinkedManagerIndependentCalculator {

	/**
	 * Custom fields specifically for Linked Manager - Positions Quantity selection
	 */
	private static final String LINKED_MANAGER_QUANTITY_CCY_DENOMINATION = "Quantity CCY Denomination";

	////////////////////////////////////////////////////////////////////////////////

	private InvestmentInstrumentService investmentInstrumentService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void calculate(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		Integer quantityCCYDenominationId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_QUANTITY_CCY_DENOMINATION);
		if (quantityCCYDenominationId == null) {
			throw generateLinkedManagerLoadException(balance, "InvestmentManagerAccount", balance.getManagerAccount().getId(), "Quantity CCY Denomination selection is required.");
		}

		BigDecimal securitiesValue = BigDecimal.ZERO;
		List<AccountingPositionDaily> accountingPositionDailyList = getAccountingPositionDailyList(balance, managerBalanceProcessingConfig);
		for (AccountingPositionDaily accountingPositionDaily : CollectionUtils.getIterable(accountingPositionDailyList)) {
			securitiesValue = MathUtils.add(securitiesValue, accountingPositionDaily.getUnadjustedQuantity());
		}

		// Convert Value to Manager Base CCY (If Required)
		securitiesValue = getQuantityValueInManagerBaseCurrency(balance.getManagerAccount(), quantityCCYDenominationId, securitiesValue, managerBalanceProcessingConfig);
		balance.setSecuritiesValue(securitiesValue);
	}


	private BigDecimal getQuantityValueInManagerBaseCurrency(InvestmentManagerAccount managerAccount, Integer quantityCCYDenominationId, BigDecimal value, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		if (!MathUtils.isEqual(quantityCCYDenominationId, managerAccount.getBaseCurrency().getId())) {
			value = MathUtils.multiply(value, managerBalanceProcessingConfig.getFxRate(managerAccount.getLinkedInvestmentAccount(), getInvestmentInstrumentService().getInvestmentSecurity(quantityCCYDenominationId), managerAccount.getBaseCurrency(), getMarketDataExchangeRatesApiService()));
		}
		return value;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////////         Getter and Setter Methods            ////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
