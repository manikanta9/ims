package com.clifton.product.manager.linkedcalculators.independent;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailySearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.manager.linkedcalculators.BaseProductLinkedManagerCalculatorImpl;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ProductManagerAccountBalanceManagerProprietaryFundPortionCalculator</code> ...
 * <p>
 * MANAGER_BALANCE_PROPRIETARY_FUND_PORTION(100), // Cash and/or Securities: Selected Manager's Balance * Market Value of Fund Holdings in Client Account / Total Market Value of the Fund
 *
 * @author manderson
 */
public class ProductLinkedManagerForManagerProprietaryFundPortionCalculator extends BaseProductLinkedManagerCalculatorImpl implements ProductLinkedManagerIndependentCalculator {

	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	private InvestmentManagerAccountService investmentManagerAccountService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void calculate(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		// Selected Manager Balance (Cash/Securities * Market Value of Fund Holdings In Client Account / Total Market Value of the Fund
		InvestmentManagerAccountBalance linkedBalance = getInvestmentManagerAccountBalanceDependency(balance);

		Integer fundAccountId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_FUND_ACCOUNT);
		if (fundAccountId == null) {
			throw generateLinkedManagerLoadException(balance, "InvestmentManagerAccount", balance.getManagerAccount().getId(), "Missing a fund account selection.");
		}

		// 1. get all positions for the fund
		AccountingPositionDailySearchForm searchForm = new AccountingPositionDailySearchForm();
		searchForm.setFundAccountId(fundAccountId);
		searchForm.setPositionDate(balance.getBalanceDate());
		List<AccountingPositionDaily> positionDailyList = getAccountingPositionDailyService().getAccountingPositionDailyList(searchForm);

		BigDecimal totalFund = CoreMathUtils.sumProperty(positionDailyList, AccountingPositionDaily::getNotionalValueBase);

		// 2. Filter out those held by selected client account
		positionDailyList = BeanUtils.filter(positionDailyList, position -> position.getAccountingTransaction().getClientInvestmentAccount().getId(), balance.getManagerAccount().getLinkedInvestmentAccount().getId());
		BigDecimal clientFund = CoreMathUtils.sumProperty(positionDailyList, AccountingPositionDaily::getNotionalValueBase);

		// 3.  Determine Client Percentage
		BigDecimal clientPercentage = CoreMathUtils.getPercentValue(clientFund, totalFund, true);

		// RESULT = Manager Balance * Market Value of Fund Holdings in Client Account / Total Market Value of the Fund
		balance.setCashValue(MathUtils.getPercentageOf(clientPercentage, linkedBalance.getAdjustedCashValue(), true));
		balance.setSecuritiesValue(MathUtils.getPercentageOf(clientPercentage, linkedBalance.getAdjustedSecuritiesValue(), true));
		balance.setNote("Manager Balance calculated as [" + CoreMathUtils.formatNumberDecimal(clientPercentage) + "%] of Manager [" + linkedBalance.getManagerAccount().getLabel() + "]'s balance.");
	}


	private InvestmentManagerAccountBalance getInvestmentManagerAccountBalanceDependency(InvestmentManagerAccountBalance balance) {
		InvestmentManagerAccount linkedManager = getInvestmentManagerAccountDependency(balance.getManagerAccount());

		// Dependent Balance Should already have been attempted to load before calling this calculate method, so should be there, but just in case
		InvestmentManagerAccountBalance linkedBalance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(linkedManager.getId(), balance.getBalanceDate(),
				false);
		if (linkedBalance == null) {
			throw generateLinkedManagerLoadException(balance, "InvestmentManagerAccount", balance.getManagerAccount().getId(), "Missing dependent manager account [" + linkedManager.getLabel()
					+ "]'s balance on [" + DateUtils.fromDateShort(balance.getBalanceDate()) + "].  Please make sure at least all dependent Standard or Proxy manager balances are loaded.");
		}
		return linkedBalance;
	}


	/**
	 * Returns the dependent manager this manager uses to calculate it's balance
	 */
	@Override
	public InvestmentManagerAccount getInvestmentManagerAccountDependency(InvestmentManagerAccount manager) {
		Integer linkedManagerAssignmentId = (Integer) getProductUtilService().getInvestmentManagerAccountLinkedManagerCustomFieldValue(manager, LINKED_MANAGER_MANAGER_ACCOUNT_FIELD);

		InvestmentManagerAccount linkedManager = null;
		if (linkedManagerAssignmentId != null) {
			linkedManager = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignment(linkedManagerAssignmentId).getReferenceOne();
		}
		if (linkedManager == null) {
			throw new ValidationExceptionWithCause("InvestmentManagerAccount", manager.getId(), "Unable to determine manager dependencies for manager [" + manager.getLabel()
					+ "] of linked type [" + manager.getLinkedManagerType() + "] because it is missing a valid manager account selection.");
		}
		return linkedManager;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}
}
