package com.clifton.product.manager;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.manager.job.AccountBalanceAdjustmentsCommand;
import com.clifton.product.manager.job.AccountBalanceCommand;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Date;
import java.util.Map;
import java.util.Optional;


/**
 * The <code>ProductManagerService</code> class defines the methods to load {@link InvestmentManagerAccountBalance}
 * and process {@link InvestmentManagerAccountBalanceAdjustment}
 * <p>
 * NOTE: These methods are in product project and NOT investment because they depend on marketdata project.
 *
 * @author Mary Anderson
 */
public interface ProductManagerService {

	/////////////////////////////////////////////////////////////////////
	///////        Manager - Balance Processing Methods          ////////
	/////////////////////////////////////////////////////////////////////


	/**
	 * Load Investment Manager Account Balances
	 * <p>
	 * For most accounts we get this information from custodial banks every day but some balances are updated manually less frequently and may be proxied based on performance of specified benchmark.
	 * Custodians may provide detailed positions and transactions but we sum them in order to obtain total balance (separate cash vs securities).
	 * Proxy Managers generally update their values once a month, and we may need to enter adjustments later to account for benchmark performance
	 * Rollup managers can be defined to group one or more real managers using specific balance allocations.
	 *
	 * @return - Summary of what was done
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = InvestmentManagerAccountBalance.class)
	public String processProductManagerAccountBalanceList(final AccountBalanceCommand command);


	/**
	 * Attempts to reload each balance in the list.  Captures any failures and continues.  When done, if any failed then
	 * returns full list of failures.
	 * <p>
	 * Note: If the balance is for a RollupManager then has a special call so that the rollup manager balance WILL be reloaded.
	 */
	@SecureMethod(dtoClass = InvestmentManagerAccountBalance.class)
	public void processProductManagerAccountBalanceListForBalances(Integer[] balanceIds);


	@DoNotAddRequestMapping
	public Optional<Boolean> processProductManagerAccountBalance(InvestmentManagerAccount manager, ProductManagerBalanceProcessingTypes processingType,
	                                                             ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig);


	/////////////////////////////////////////////////////////////////////
	/////     Manager - Balance Adjustment Processing Methods      //////
	/////////////////////////////////////////////////////////////////////


	/**
	 * Calculate and Enter System Defined Manager Account Balance Adjustments
	 * <p>
	 * Examples: M2M Adjustments, Proxy Benchmark Performance Adjustments, Cash % adjustments, etc (All determined by each manager's specific configuration)
	 *
	 * @return - Summary of what was done
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = InvestmentManagerAccountBalance.class)
	public String processProductManagerAccountBalanceAdjustmentList(final AccountBalanceAdjustmentsCommand command);


	/**
	 * Attempts to process each balance adjustments in the list.  Captures any failures and continues.  When done, if any failed then
	 * returns full list of failures.
	 */
	@SecureMethod(dtoClass = InvestmentManagerAccountBalance.class)
	public void processProductManagerAccountBalanceAdjustmentListForBalances(Integer[] balanceIds);


	/**
	 * Calculate and Enter System Defined Manager Account Balance Adjustments
	 * <p>
	 * Processes for a specific balance record
	 * Called from Balance window to process the adjustments for the balance
	 */
	public InvestmentManagerAccountBalance processProductManagerAccountBalanceAdjustmentsForBalance(InvestmentManagerAccountBalance bean);


	/////////////////////////////////////////////////////////////////////
	////////        Rollup Manager - Processing Methods          ////////
	/////////////////////////////////////////////////////////////////////


	/**
	 * Loads a rollup manager balance for given rollup manager and date.
	 */
	@DoNotAddRequestMapping
	public void processProductManagerAccountRollupBalance(InvestmentManagerAccount rollupManager, Date balanceDate);


	/**
	 * MOC Adjustments are rolled up to the parent manager from children.  The other adjustments are applied into the rollup's original balance
	 * Can be called separately so we don't have to completely reload/re-process rollup managers when only MOC adjustments are entered/updated
	 *
	 * @param rollupBalance
	 * @param childBalance  - If set, then this is the balance record that was edited so db value may not have saved value yet.
	 */
	@DoNotAddRequestMapping
	public void processProductManagerRollupMOCAdjustments(InvestmentManagerAccountBalance rollupBalance, InvestmentManagerAccountBalance childBalance);


	/////////////////////////////////////////////////////////////////////
	////////        Linked Manager - Processing Methods          ////////
	/////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public void processProductLinkedManagerAccountListForInvestmentAccount(int investmentAccountId, Date balanceDate, Map<Integer, PortfolioRun> clientAccountLinkedRunMap);
}
