package com.clifton.product.manager;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailySearchForm;
import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.accounting.m2m.search.AccountingM2MDailySearchForm;
import com.clifton.business.company.BusinessCompany;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.Event;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.event.EventObject;
import com.clifton.core.util.math.AggregationOperations;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEventManagerAdjustment;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccount.CashAdjustmentType;
import com.clifton.investment.manager.InvestmentManagerAccountAdjustmentM2M;
import com.clifton.investment.manager.InvestmentManagerAccountAdjustmentM2M.ManagerM2MAdjustmentTypes;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAccountRollup;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustmentType;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceServiceImpl;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.rates.MarketDataRatesRetriever;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.manager.job.AccountBalanceAdjustmentsCommand;
import com.clifton.product.manager.job.AccountBalanceCommand;
import com.clifton.product.manager.linkedcalculators.ProductLinkedManagerCalculator;
import com.clifton.product.manager.linkedcalculators.ProductLinkedManagerCalculatorLocator;
import com.clifton.product.manager.linkedcalculators.dependent.ProductLinkedManagerDependentCalculator;
import com.clifton.product.manager.linkedcalculators.independent.ProductLinkedManagerIndependentCalculator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.rule.violation.RuleViolationUtil;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.rule.violation.status.RuleViolationStatusService;
import com.clifton.system.schema.SystemSchemaService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * The <code>ProductManagerServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class ProductManagerServiceImpl implements ProductManagerService {

	public static final String RULE_DEFINITION_PROXY = "Proxy Adjustments";
	public static final String RULE_DEFINITION_M2M = "M2M Adjustments";
	public static final String RULE_DEFINITION_CASH = "Cash Percentage Adjustments";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private AccountingM2MService accountingM2MService;
	private AccountingPositionDailyService accountingPositionDailyService;

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentCalendarService investmentCalendarService;
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	private InvestmentManagerAccountService investmentManagerAccountService;

	private EventHandler eventHandler;


	private MarketDataRetriever marketDataRetriever;
	private MarketDataRatesRetriever marketDataRatesRetriever;

	private ProductLinkedManagerCalculatorLocator productLinkedManagerCalculatorLocator;

	private RuleViolationService ruleViolationService;
	private RuleViolationStatusService ruleViolationStatusService;
	private RunnerHandler runnerHandler;

	private SystemSchemaService systemSchemaService;

	/////////////////////////////////////////////////////////////////////
	///////        Manager - Balance Processing Methods          ////////
	/////////////////////////////////////////////////////////////////////


	@Override
	public String processProductManagerAccountBalanceList(final AccountBalanceCommand command) {
		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : Status.ofMessage("Synchronously running for " + command);
			return doProcessProductManagerAccountBalanceList(command.getSearchForm(), command.getBalanceDate(), command.getProcessingType(), status);
		}

		// asynchronous run support
		String runId = command.getRunId();
		final Date now = new Date();
		final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + DateUtils.fromDate(now));
		Runner runner = new AbstractStatusAwareRunner("PRODUCT-MANAGER", runId, now, statusHolder) {

			@Override
			public void run() {
				try {
					String message = doProcessProductManagerAccountBalanceList(command.getSearchForm(), command.getBalanceDate(), command.getProcessingType(), statusHolder.getStatus());
					statusHolder.getStatus().addMessage(message);
				}
				catch (Throwable e) {
					getStatus().setMessage("Error updating product manager balances " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error updating product manager balances for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);
		return "Started loading requested manager balances. Processing will be completed shortly.";
	}


	private String doProcessProductManagerAccountBalanceList(InvestmentManagerAccountSearchForm searchForm, Date balanceDate, ProductManagerBalanceProcessingTypes processingType, Status status) {
		List<InvestmentManagerAccount> managerList = getInvestmentManagerAccountActiveList(searchForm);
		if (CollectionUtils.isEmpty(managerList)) {
			status.setActionPerformed(false);
			return "No Active Manager Accounts match search criteria.  No Balances we loaded for " + DateUtils.fromDateShort(balanceDate);
		}

		if (balanceDate == null) {
			balanceDate = DateUtils.clearTime(new Date());
		}
		int successCount = 0;
		int failCount = 0;
		int skipCount = 0;

		ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig = new ProductManagerBalanceProcessingConfig(balanceDate, getCalendarBusinessDayService());

		StringBuilder errors = new StringBuilder();
		for (InvestmentManagerAccount manager : managerList) {
			try {
				Optional<Boolean> processed = processProductManagerAccountBalance(manager, processingType, managerBalanceProcessingConfig);
				if (processed.isPresent()) {
					if (BooleanUtils.isTrue(processed.get())) {
						successCount++;
					}
					else {
						failCount++;
					}
				}
				else {
					skipCount++;
				}
			}
			catch (Exception e) {
				String msg = "Client [" + manager.getClient().getName() + "], Manager [" + manager.getLabel() + "]: ";
				if (status != null) {
					status.addError(msg + ExceptionUtils.getDetailedMessage(e));
				}
				Throwable orig = ExceptionUtils.getOriginalException(e);
				LogUtils.errorOrInfo(getClass(), msg + Optional.ofNullable(orig).map(Throwable::getMessage).orElse(""), orig);
				failCount++;
				errors.append("<br>").append(msg).append(ExceptionUtils.getDetailedMessage(e));
			}
			if (status != null) {
				status.setMessage("Total: " + CollectionUtils.getSize(managerList) + "; Success: " + successCount + "; Skipped: " + skipCount + "; Failed: " + failCount + " "
						+ (failCount > 0 ? ": " + errors : ""));
			}
		}
		return "Processing Complete. Manager Account Balances Loaded " + (processingType.isProcessAdjustments() ? "and Processed " : "") + "for " + DateUtils.fromDateShort(balanceDate) + ": "
				+ CollectionUtils.getSize(managerList) + " total, " + successCount + " successful, " + skipCount + " skipped, " + failCount + " failed"
				+ (failCount > 0 ? ": " + errors : "");
	}


	@Override
	public void processProductManagerAccountBalanceListForBalances(Integer[] balanceIds) {
		if (balanceIds == null || balanceIds.length == 0) {
			throw new ValidationException("No Balance(s) selected to process.");
		}
		StringBuilder errors = new StringBuilder();
		int errorCount = 0;

		// Set on the first balance needed for - note that the only way to call this method from UI would only allow the same balance date to be used across the selected balances
		ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig = null;

		for (Integer balanceId : balanceIds) {
			try {
				InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalance(balanceId);
				if (balance != null) {
					if (balance.getManagerAccount().isRollupManager()) {
						InvestmentManagerAccount rollupManager = getInvestmentManagerAccountService().getInvestmentManagerAccount(balance.getManagerAccount().getId());
						processProductManagerAccountRollupBalance(rollupManager, balance.getBalanceDate());
					}
					else {
						if (managerBalanceProcessingConfig == null) {
							managerBalanceProcessingConfig = new ProductManagerBalanceProcessingConfig(balance.getBalanceDate(), getCalendarBusinessDayService());
						}
						processProductManagerAccountBalance(balance.getManagerAccount(), ProductManagerBalanceProcessingTypes.RELOAD_PROCESS, managerBalanceProcessingConfig);
					}
				}
			}
			catch (ValidationException e) {
				errorCount++;
				errors.append(StringUtils.NEW_LINE).append(ExceptionUtils.getDetailedMessage(e));
			}
		}

		if (errorCount > 0) {
			throw new ValidationException(errorCount + " balance adjustment processing failed: " + errors);
		}
	}


	/**
	 * Returns true if successfully loaded balance, null if skipped, false if failed processing
	 */
	@Override
	public Optional<Boolean> processProductManagerAccountBalance(InvestmentManagerAccount manager, ProductManagerBalanceProcessingTypes processingType,
	                                                             ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {

		ValidationUtils.assertNotNull(manager, "Investment Manager Account required in order to load balance information.");
		ValidationUtils.assertNotNull(managerBalanceProcessingConfig.getBalanceDate(), "Balance Date required for manager [" + manager.getLabel() + "] in order to load balance information.");

		// Only Load Balances if Manager is Active and is a Proxy Manager or Custodian Account information is filled in.  Otherwise balance must be manually uploaded/entered
		if (!isInvestmentManagerAccountAutoLoaded(manager)) {
			return Optional.empty();
		}

		if (manager.isLinkedManager()) {
			return processProductLinkedManagerAccountBalanceImpl(manager, processingType, managerBalanceProcessingConfig);
		}

		InvestmentManagerAccountBalance existing = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(manager.getId(), managerBalanceProcessingConfig.getBalanceDate(), false);
		// If not reloading, skip it if it exists
		if (existing != null && processingType.isLoadMissingOnly()) {
			if (processingType.isProcessAdjustments() && !RuleViolationUtil.isRuleViolationAwareProcessed(existing)) {
				processProductManagerAccountBalanceAdjustmentsForBalanceImpl(existing, managerBalanceProcessingConfig);
				return Optional.of(true);
			}
			return Optional.empty(); // skipped
		}

		// Pull Manager Again By ID, so all the lists are populated
		manager = getInvestmentManagerAccountService().getInvestmentManagerAccount(manager.getId());

		// Create new Balance Record
		InvestmentManagerAccountBalance balance = new InvestmentManagerAccountBalance();
		balance.setManagerAccount(manager);
		balance.setBalanceDate(managerBalanceProcessingConfig.getBalanceDate());
		BigDecimal cashValue = BigDecimal.ZERO;
		BigDecimal securitiesValue = BigDecimal.ZERO;

		// Proxy is handled first if populated
		if (manager.isProxyManager()) {
			// If explicit value - copy it
			if (manager.getProxyValue() != null) {
				securitiesValue = manager.getProxyValue();
			}
			// If shares entered - calculate value
			else if (manager.getProxySecurityQuantity() != null && manager.getProxyBenchmarkSecurity() != null) {
				BigDecimal shareValue = (manager.isProxyToLastKnownPrice() ? getMarketDataRetriever().getPriceAdjustedFlexible(manager.getProxyBenchmarkSecurity(), managerBalanceProcessingConfig.getBalanceDate(), false)
						: getMarketDataRetriever().getPriceAdjusted(manager.getProxyBenchmarkSecurity(), managerBalanceProcessingConfig.getBalanceDate(), false, ""));
				if (shareValue != null) {
					securitiesValue = MathUtils.multiply(shareValue, manager.getProxySecurityQuantity());
					balance.setNote("Original Securities Value calculated as [" + manager.getProxySecurityQuantity() + "] shares of [" + manager.getProxyBenchmarkSecurity().getLabel() + "] using ["
							+ shareValue.doubleValue() + "] as price per share.");
				}
				else {
					// Cannot create balance because missing price info
					return Optional.of(false);
				}
			}
			else {
				// Cannot create balance because missing proxy value and benchmark/shares to determine original securities value.
				return Optional.of(false);
			}
		}

		// Use Custodial Account Info if Not Proxy, or if Proxy, but using Custodial info for cash values
		if ((manager.getCustodianAccount() != null) && (!manager.isProxyManager() || manager.isProxySecuritiesOnly())) {
			ManagerPositionHistorySummary custodianBalance = getCustodianBalance(manager.getCustodianAccount().getCompany(), manager.getCustodianAccount().getNumber(), managerBalanceProcessingConfig);
			if (custodianBalance != null) {
				cashValue = custodianBalance.getCashValue();
				// Only set securities value if not a proxy manager
				if (!manager.isProxyManager()) {
					securitiesValue = custodianBalance.getSecurityValue();
				}
			}
			else if (!manager.isZeroBalanceIfNoDownload()) {
				// Cannot create balance because custodian balance is missing
				throw new ValidationException("There are no Bank Position Summary Records for Bank [" + manager.getCustodianAccount().getCompany().getName() + "] and Bank Code ["
						+ manager.getCustodianAccount().getNumber() + "] loaded for Date [" + DateUtils.fromDateShort(managerBalanceProcessingConfig.getBalanceDate()) + "].");
			}
		}

		balance.setCashValue(cashValue);
		balance.setSecuritiesValue(securitiesValue);

		saveManagerBalance(existing, balance, processingType, managerBalanceProcessingConfig);
		return Optional.of(true);
	}


	/**
	 * InvestmentManagerBalance save method that will also optionally immediately process adjustments on the balance
	 */
	private InvestmentManagerAccountBalance saveManagerBalance(InvestmentManagerAccountBalance existingBalance, InvestmentManagerAccountBalance newBalance, ProductManagerBalanceProcessingTypes processingType, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		newBalance = getInvestmentManagerAccountBalanceService().saveInvestmentManagerAccountBalanceReload(existingBalance, newBalance, processingType.isForceReload());

		// Process Adjustments Immediately - If Selected
		if (processingType.isProcessAdjustments() && !RuleViolationUtil.isRuleViolationAwareProcessed(newBalance)) {
			if (managerBalanceProcessingConfig != null) {
				return processProductManagerAccountBalanceAdjustmentsForBalanceImpl(newBalance, managerBalanceProcessingConfig);
			}
			else {
				return processProductManagerAccountBalanceAdjustmentsForBalance(newBalance);
			}
		}
		return newBalance;
	}

	/////////////////////////////////////////////////////////////////////
	////////        Rollup Manager - Processing Methods          ////////
	/////////////////////////////////////////////////////////////////////


	@Override
	public void processProductManagerAccountRollupBalance(InvestmentManagerAccount rollupManager, Date balanceDate) {
		// Skip Inactive Rollups
		if (rollupManager.isInactive()) {
			return;
		}

		List<InvestmentManagerAccountRollup> children = rollupManager.getRollupManagerList();

		BigDecimal cashValue = BigDecimal.ZERO;
		BigDecimal securitiesValue = BigDecimal.ZERO;
		BigDecimal minMaxTotalValue = null;

		StringBuilder desc = new StringBuilder("Calculation: " + StringUtils.NEW_LINE);

		AggregationOperations rollupAggregationType = rollupManager.getRollupAggregationType();
		AssertUtils.assertNotNull(rollupAggregationType, "A RollupManager requires a value for property RollupAggregationType (AggregationOperation).");

		// Get Children Balance Values
		for (InvestmentManagerAccountRollup child : CollectionUtils.getIterable(children)) {
			// Skip Inactive Children
			if (child.getReferenceTwo().isInactive()) {
				continue;
			}

			InvestmentManagerAccountBalance childBalance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(child.getReferenceTwo().getId(), balanceDate, false);
			// If child balance doesn't exist yet, cannot create rollup manager's balance yet
			if (childBalance == null) {
				return;
			}
			// If the child balance hasn't been process yet, don't bother creating rollup manager, because
			// after processing will just need to recreate
			if (!RuleViolationUtil.isRuleViolationAwareReadyForUse(childBalance)) {
				return;
			}

			desc.append(CoreMathUtils.formatNumberMoney(child.getAllocationPercent()));
			desc.append(" % of ");
			desc.append(childBalance.getManagerAccount().getAccountNumber());
			desc.append("(Cash: ");
			desc.append(CoreMathUtils.formatNumberMoney(childBalance.getAdjustedCashValue()));
			desc.append(", Securities: ");
			desc.append(CoreMathUtils.formatNumberMoney(childBalance.getAdjustedSecuritiesValue()));
			desc.append(")" + StringUtils.NEW_LINE);

			BigDecimal childCashValue = MathUtils.getPercentageOf(child.getAllocationPercent(), childBalance.getAdjustedCashValue(), true);
			BigDecimal childSecuritiesValue = MathUtils.getPercentageOf(child.getAllocationPercent(), childBalance.getAdjustedSecuritiesValue(), true);
			BigDecimal childTotalValue = MathUtils.getPercentageOf(child.getAllocationPercent(), childBalance.getAdjustedTotalValue(), true);

			switch (rollupAggregationType) {
				case MIN:
					if (minMaxTotalValue == null || MathUtils.isLessThanOrEqual(childTotalValue, minMaxTotalValue)) {
						minMaxTotalValue = childTotalValue;
						cashValue = childCashValue;
						securitiesValue = childSecuritiesValue;
					}
					break;

				case MAX:
					if (minMaxTotalValue == null || MathUtils.isGreaterThan(childTotalValue, minMaxTotalValue)) {
						minMaxTotalValue = childTotalValue;
						cashValue = childCashValue;
						securitiesValue = childSecuritiesValue;
					}
					break;

				case SUM:
				default:
					cashValue = MathUtils.add(cashValue, childCashValue);
					securitiesValue = MathUtils.add(securitiesValue, childSecuritiesValue);
					break;
			}
		}

		InvestmentManagerAccountBalance rollupBalance = new InvestmentManagerAccountBalance();
		rollupBalance.setManagerAccount(rollupManager);
		rollupBalance.setBalanceDate(balanceDate);
		rollupBalance.setNote(StringUtils.formatStringUpToNCharsWithDots(desc.toString(), DataTypes.DESCRIPTION_LONG.getLength(), true));
		rollupBalance.setCashValue(cashValue);
		rollupBalance.setSecuritiesValue(securitiesValue);

		// Save the Rollup Balance
		InvestmentManagerAccountBalance existing = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(rollupManager.getId(), balanceDate, false);
		saveManagerBalance(existing, rollupBalance, ProductManagerBalanceProcessingTypes.RELOAD_PROCESS, null);
	}


	/**
	 * MOC Adjustments are rolled up to the parent manager from children.  The other adjustments are applied into the rollup's original balance
	 * Can be called separately so we don't have to completely reload/re-process rollup managers when only MOC adjustments are entered/updated
	 */
	@Override
	public void processProductManagerRollupMOCAdjustments(InvestmentManagerAccountBalance rollupBalance, InvestmentManagerAccountBalance childBalance) {
		InvestmentManagerAccountBalanceAdjustmentType rollupMOCAdjustmentType = getInvestmentManagerAccountBalanceAdjustmentTypeSystemDefined(InvestmentManagerAccountBalanceAdjustmentType.ROLLUP_MOC);
		if (rollupMOCAdjustmentType != null) {
			// Filter out Existing Rollup MOC adjustments, will reprocess and add them.
			rollupBalance.setAdjustmentList(BeanUtils.filter(rollupBalance.getAdjustmentList(), balanceAdjustment -> !CompareUtils.isEqual(rollupMOCAdjustmentType, balanceAdjustment.getAdjustmentType())));

			InvestmentManagerAccount manager = rollupBalance.getManagerAccount();
			if (manager.isRollupManager()) {
				List<InvestmentManagerAccountRollup> children = manager.getRollupManagerList();

				// properties for applying MIN/MAX MOC adjustments
				InvestmentManagerAccountBalance newMinMaxManagerBalance = null;
				BigDecimal newMinMaxChildMocTotal = null;
				BigDecimal newMinMaxChildAllocationPercent = null;

				// Get Children Balance Values
				for (InvestmentManagerAccountRollup child : CollectionUtils.getIterable(children)) {
					// Skip Inactive Children
					if (child.getReferenceTwo().isInactive()) {
						continue;
					}
					InvestmentManagerAccountBalance thisChildBalance;
					if (childBalance != null && child.getReferenceTwo().equals(childBalance.getManagerAccount())) {
						thisChildBalance = childBalance;
					}
					else {
						thisChildBalance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(child.getReferenceTwo().getId(), rollupBalance.getBalanceDate(),
								true);
					}
					if (thisChildBalance != null) {
						List<InvestmentManagerAccountBalanceAdjustment> mocAdjustmentList = BeanUtils.filter(thisChildBalance.getAdjustmentList(), adjustment -> adjustment.getAdjustmentType().isMarketOnClose(), true);

						switch (manager.getRollupAggregationType()) {
							case MIN: {
								BigDecimal childMocTotal = MathUtils.getPercentageOf(thisChildBalance.getMarketOnCloseTotalValue(), child.getAllocationPercent(), true);
								if (newMinMaxManagerBalance == null || MathUtils.isLessThan(childMocTotal, newMinMaxChildMocTotal)) {
									newMinMaxChildMocTotal = childMocTotal;
									newMinMaxManagerBalance = thisChildBalance;
									newMinMaxChildAllocationPercent = child.getAllocationPercent();
								}
								break;
							}
							case MAX: {
								BigDecimal childMocTotal = MathUtils.getPercentageOf(thisChildBalance.getMarketOnCloseTotalValue(), child.getAllocationPercent(), true);
								if (newMinMaxManagerBalance == null || MathUtils.isGreaterThan(childMocTotal, newMinMaxChildMocTotal)) {
									newMinMaxChildMocTotal = childMocTotal;
									newMinMaxManagerBalance = thisChildBalance;
									newMinMaxChildAllocationPercent = child.getAllocationPercent();
								}
								break;
							}
							case SUM:
							default: {
								for (InvestmentManagerAccountBalanceAdjustment mocAdjustment : CollectionUtils.getIterable(mocAdjustmentList)) {
									InvestmentManagerAccountBalanceAdjustment rollupAdjustment = calculateRollupMOCAdjustment(rollupBalance, mocAdjustment, child.getAllocationPercent());
									if (rollupAdjustment != null) {
										rollupAdjustment.setAdjustmentType(rollupMOCAdjustmentType);
										rollupBalance.addAdjustment(rollupAdjustment);
									}
								}
							}
						}
					}
				}

				if (newMinMaxManagerBalance != null && !MathUtils.isEqual(newMinMaxChildMocTotal, rollupBalance.getMarketOnCloseTotalValue())) {
					AggregationOperations rollupAggregation = rollupBalance.getManagerAccount().getRollupAggregationType();
					BigDecimal childMocCash = MathUtils.getPercentageOf(newMinMaxManagerBalance.getMarketOnCloseCashValue(), newMinMaxChildAllocationPercent, true);
					BigDecimal newMinMaxChildMOCSecurities = MathUtils.getPercentageOf(newMinMaxManagerBalance.getMarketOnCloseSecuritiesValue(), newMinMaxChildAllocationPercent, true);
					BigDecimal cashAdjustment = MathUtils.subtract(childMocCash, rollupBalance.getMarketOnCloseCashValue());
					BigDecimal securitiesAdjustment = MathUtils.subtract(newMinMaxChildMOCSecurities, rollupBalance.getMarketOnCloseSecuritiesValue());

					if (!MathUtils.isNullOrZero(cashAdjustment) || !MathUtils.isNullOrZero(securitiesAdjustment)) {
						StringBuilder note = new StringBuilder(CoreMathUtils.formatNumberMoney(newMinMaxChildAllocationPercent))
								.append("% of ").append(newMinMaxManagerBalance.getManagerAccount().getAccountNumber())
								.append(" adjusting ").append(rollupAggregation)
								.append(" (Cash: ").append(CoreMathUtils.formatNumberMoney(newMinMaxManagerBalance.getMarketOnCloseCashValue()))
								.append(", Securities: ").append(CoreMathUtils.formatNumberMoney(newMinMaxManagerBalance.getMarketOnCloseSecuritiesValue()))
								.append(")" + StringUtils.NEW_LINE);

						InvestmentManagerAccountBalanceAdjustment adjustment = new InvestmentManagerAccountBalanceAdjustment();
						adjustment.setCashValue(cashAdjustment);
						adjustment.setSecuritiesValue(securitiesAdjustment);
						adjustment.setNote(note.toString());
						adjustment.setAdjustmentType(rollupMOCAdjustmentType);
						rollupBalance.addAdjustment(adjustment);
					}
				}
			}
		}
		else {
			AssertUtils.fail("Could not retrieve InvestmentManagerAccountBalanceAdjustmentType with name %s", InvestmentManagerAccountBalanceAdjustmentType.ROLLUP_MOC);
		}
	}


	/**
	 * Calculates the Rollup MOC Adjustment value for Cash and Securities based on the Allocation Percent of the Rollup and the Rollup Manager's
	 * CashAdjustmentType Note: If calculation results in 0 adjustment for Cash and Securities will return null
	 */
	private InvestmentManagerAccountBalanceAdjustment calculateRollupMOCAdjustment(InvestmentManagerAccountBalance rollupBalance, InvestmentManagerAccountBalanceAdjustment childMOCAdjustment, BigDecimal childAllocationPercent) {
		BigDecimal cashAdjustment = MathUtils.getPercentageOf(childAllocationPercent, childMOCAdjustment.getCashValue(), true);
		BigDecimal securitiesAdjustment = MathUtils.getPercentageOf(childAllocationPercent, childMOCAdjustment.getSecuritiesValue(), true);
		BigDecimal totalAdjustment = MathUtils.add(cashAdjustment, securitiesAdjustment);

		StringBuilder note = new StringBuilder(50);
		note.append(CoreMathUtils.formatNumberMoney(childAllocationPercent));
		note.append(" % of ");
		note.append(childMOCAdjustment.getManagerAccountBalance().getManagerAccount().getAccountNumber());
		note.append(" (Cash: ");
		note.append(CoreMathUtils.formatNumberMoney(cashAdjustment));
		note.append(", Securities: ");
		note.append(CoreMathUtils.formatNumberMoney(securitiesAdjustment));
		note.append(")" + StringUtils.NEW_LINE);

		CashAdjustmentType cashAdjustmentType = rollupBalance.getManagerAccount().getCashAdjustmentType();
		switch (cashAdjustmentType) {
			case NONE:
				// Do Nothing
				break;
			case CASH:
				cashAdjustment = totalAdjustment;
				securitiesAdjustment = BigDecimal.ZERO;
				note.append(" Applied all to Cash");
				break;
			case SECURITIES:
				securitiesAdjustment = totalAdjustment;
				cashAdjustment = BigDecimal.ZERO;
				note.append(" Applied all to Securities");
				break;
			case IGNORE_CASH:
				cashAdjustment = BigDecimal.ZERO;
				note.append(" Ignoring Cash");
				break;
			case IGNORE_SECURITIES:
				securitiesAdjustment = BigDecimal.ZERO;
				note.append(" Ignoring Securities");
				break;
			case CUSTOM_PERCENT:
				cashAdjustment = MathUtils.getPercentageOf(rollupBalance.getManagerAccount().getCustomCashValue(), totalAdjustment, true);
				securitiesAdjustment = MathUtils.subtract(totalAdjustment, cashAdjustment);
				note.append(" Applied ").append(CoreMathUtils.formatNumberMoney(rollupBalance.getManagerAccount().getCustomCashValue())).append(" % to Cash, Remainder to Securities.");
				break;
			case CUSTOM_VALUE:
				BigDecimal customCashValue = rollupBalance.getManagerAccount().getCustomCashValue();
				rollupBalance.recalculateAdjustedValues();
				if (MathUtils.isLessThan(rollupBalance.getMarketOnCloseCashValue(), customCashValue)) {
					BigDecimal difference = MathUtils.subtract(customCashValue, rollupBalance.getMarketOnCloseCashValue());
					if (MathUtils.isGreaterThan(difference, totalAdjustment)) {
						difference = totalAdjustment;
					}
					cashAdjustment = difference;
					securitiesAdjustment = MathUtils.subtract(totalAdjustment, cashAdjustment);
					note.append(" Applied Cash to Custom Cash Value of ").append(CoreMathUtils.formatNumberMoney(customCashValue)).append(", Remainder to Securities.");
				}
				else {
					cashAdjustment = BigDecimal.ZERO;
					securitiesAdjustment = totalAdjustment;
					note.append(" Applied All to Securities because Custom Cash Value of ").append(CoreMathUtils.formatNumberMoney(customCashValue)).append(" was fulfilled.");
				}
				break;
			case MANAGER:
				// If Cash Percent Manager is Used, then the Adjusted Balance of the Rollup would already have that Ratio - No Need to Lookup the Manager Balance Again
				rollupBalance.recalculateAdjustedValues();
				BigDecimal cashPercent = CoreMathUtils.getPercentValue(rollupBalance.getAdjustedCashValue(), rollupBalance.getAdjustedTotalValue(), true);
				cashAdjustment = MathUtils.getPercentageOf(cashPercent, totalAdjustment, true);
				securitiesAdjustment = MathUtils.subtract(totalAdjustment, cashAdjustment);
				note.append(" Applied ").append(CoreMathUtils.formatNumberMoney(cashPercent)).append(" % to Cash, Remainder to Securities.");
				break;
		}

		if (MathUtils.isNullOrZero(cashAdjustment) && MathUtils.isNullOrZero(securitiesAdjustment)) {
			return null;
		}
		InvestmentManagerAccountBalanceAdjustment adjustment = new InvestmentManagerAccountBalanceAdjustment();
		adjustment.setCashValue(cashAdjustment);
		adjustment.setSecuritiesValue(securitiesAdjustment);
		adjustment.setNote(note.toString());
		return adjustment;
	}

	/////////////////////////////////////////////////////////////////////
	/////     Manager - Balance Adjustment Processing Methods      //////
	/////////////////////////////////////////////////////////////////////


	@Override
	public String processProductManagerAccountBalanceAdjustmentList(final AccountBalanceAdjustmentsCommand command) {
		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : Status.ofMessage("Synchronously running for " + command);
			return doProcessProductManagerAccountBalanceAdjustmentList(command.getSearchForm(), command.getBalanceDate(), command.isReload(), status);
		}

		// asynchronous run support
		String runId = command.getRunId();
		final Date now = new Date();
		final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + DateUtils.fromDate(now));
		// Pass false for concurrent, so runs are performed sequentially to prevent deadlocks
		Runner runner = new AbstractStatusAwareRunner("PRODUCT-MANAGER", runId, now, false, statusHolder) {

			@Override
			public void run() {
				try {
					String message = doProcessProductManagerAccountBalanceAdjustmentList(command.getSearchForm(), command.getBalanceDate(), command.isReload(), statusHolder.getStatus());
					statusHolder.getStatus().addMessage(message);
				}
				catch (Throwable e) {
					getStatus().setMessage("Error updating product manager adjustment balances " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error updating product manager adjustment balances for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);
		return "Started processing requested manager balance adjustments. Processing will be completed shortly.";
	}


	private String doProcessProductManagerAccountBalanceAdjustmentList(InvestmentManagerAccountSearchForm searchForm, Date balanceDate, boolean reload, Status status) {
		List<InvestmentManagerAccount> managerList = getInvestmentManagerAccountActiveList(searchForm);
		if (balanceDate == null) {
			balanceDate = DateUtils.clearTime(new Date());
		}

		if (CollectionUtils.isEmpty(managerList)) {
			status.setActionPerformed(false);
			return "No Active Manager Accounts match search criteria.  No Adjustments were calculated for " + DateUtils.fromDateShort(balanceDate);
		}

		ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig = new ProductManagerBalanceProcessingConfig(balanceDate, getCalendarBusinessDayService());

		int successCount = 0;
		int failCount = 0;
		int skipCount = 0;
		StringBuilder errors = new StringBuilder();

		for (InvestmentManagerAccount manager : managerList) {
			try {
				Optional<Boolean> processed = processProductManagerAccountBalanceAdjustment(manager, balanceDate, reload, managerBalanceProcessingConfig);
				if (processed.isPresent()) {
					if (BooleanUtils.isTrue(processed.get())) {
						successCount++;
					}
					else {
						failCount++;
					}
				}
				else {
					skipCount++;
				}
			}
			catch (Exception e) {
				String msg = "Client [" + manager.getClient().getName() + "], Manager [" + manager.getLabel() + "]: ";
				if (status != null) {
					status.addError(msg + ExceptionUtils.getDetailedMessage(e));
				}
				Throwable orig = ExceptionUtils.getOriginalException(e);
				LogUtils.errorOrInfo(getClass(), msg + Optional.ofNullable(orig).map(Throwable::getMessage).orElse(""), orig);
				failCount++;
				errors.append("<br>").append(msg).append(ExceptionUtils.getDetailedMessage(e));
			}
			if (status != null) {
				status.setMessage("Total: " + CollectionUtils.getSize(managerList) + "; Success: " + successCount + "; Skipped: " + skipCount + "; Failed: " + failCount + " "
						+ (failCount > 0 ? ": " + errors : ""));
			}
		}
		return "Processing Complete. Manager Account Balance Adjustments Processed for " + DateUtils.fromDateShort(balanceDate) + ": " + CollectionUtils.getSize(managerList) + " total, "
				+ successCount + " successful, " + skipCount + " skipped, " + failCount + " failed" + (failCount > 0 ? ": " + errors : "");
	}


	@Override
	public void processProductManagerAccountBalanceAdjustmentListForBalances(Integer[] balanceIds) {
		if (balanceIds == null || balanceIds.length == 0) {
			throw new ValidationException("No Balance(s) selected to process.");
		}
		StringBuilder errors = new StringBuilder();
		int errorCount = 0;

		// Set on the first balance needed for - note that the only way to call this method from UI would only allow the same balance date to be used across the selected balances
		ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig = null;

		for (Integer balanceId : balanceIds) {
			try {
				InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalance(balanceId);
				if (balance != null) {
					// Need fully populated manager account on balance in order to process
					balance.setManagerAccount(getInvestmentManagerAccountService().getInvestmentManagerAccount(balance.getManagerAccount().getId()));
					if (managerBalanceProcessingConfig == null) {
						managerBalanceProcessingConfig = new ProductManagerBalanceProcessingConfig(balance.getBalanceDate(), getCalendarBusinessDayService());
					}
					processProductManagerAccountBalanceAdjustmentsForBalanceImpl(balance, managerBalanceProcessingConfig);
				}
			}
			catch (ValidationException e) {
				errorCount++;
				errors.append(StringUtils.NEW_LINE).append(ExceptionUtils.getDetailedMessage(e));
			}
		}

		if (errorCount > 0) {
			throw new ValidationException(errorCount + " balance adjustment processing failed: " + errors);
		}
	}


	private Optional<Boolean> processProductManagerAccountBalanceAdjustment(InvestmentManagerAccount manager, Date balanceDate, boolean reload,
	                                                                        ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		ValidationUtils.assertNotNull(manager, "Investment Manager Account required in order to calculate balance adjustment information.");
		ValidationUtils.assertNotNull(balanceDate, "Balance Date required for manager [" + manager.getLabel() + "] in order to calculate balance adjustment information.");

		// Verify Manager is Active
		if (manager.isInactive()) {
			// If inactive, do nothing - should never hit this point, because they are already filtered out, but just in case...
			return Optional.empty();
		}

		InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(manager.getId(), balanceDate, true);
		if (balance == null) {
			return Optional.empty();
		}

		// Pull Manager Again By ID, so all the lists are populated, and add it to balance
		manager = getInvestmentManagerAccountService().getInvestmentManagerAccount(manager.getId());
		balance.setManagerAccount(manager);

		// If adjustments successfully exist, and not recalculating, return Skipped
		if (RuleViolationUtil.isRuleViolationAwareReadyForUse(balance) && !reload) {
			return Optional.empty();
		}

		balance = processProductManagerAccountBalanceAdjustmentsForBalanceImpl(balance, managerBalanceProcessingConfig);
		return Optional.of(RuleViolationUtil.isRuleViolationAwareReadyForUse(balance));
	}


	@Override
	public InvestmentManagerAccountBalance processProductManagerAccountBalanceAdjustmentsForBalance(InvestmentManagerAccountBalance bean) {
		if (bean != null) {
			ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig = new ProductManagerBalanceProcessingConfig(bean.getBalanceDate(), getCalendarBusinessDayService());
			return processProductManagerAccountBalanceAdjustmentsForBalanceImpl(bean, managerBalanceProcessingConfig);
		}
		return bean;
	}


	private InvestmentManagerAccountBalance processProductManagerAccountBalanceAdjustmentsForBalanceImpl(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		ValidationUtils.assertNotNull(balance, "Cannot process system defined adjustments for a null balance record.");

		// Get fully populated Manager Account for the balance so all details are know for processing
		balance.setManagerAccount(getInvestmentManagerAccountService().getInvestmentManagerAccount(balance.getManagerAccount().getId()));

		// 1. Recalculate Balances - First Remove ALL existing auto loaded adjustments
		// and keep manual adjustments separate for adding back in after system defined adjustments are added.
		List<InvestmentManagerAccountBalanceAdjustment> manualAdjustmentList = BeanUtils.filter(balance.getAdjustmentList(), InvestmentManagerAccountBalanceAdjustment::isAutoLoaded, false);
		balance.setAdjustmentList(null);
		balance.recalculateAdjustedValues();
		balance.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.UNPROCESSED));

		// 2. Calculate System Defined Adjustments
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		// Ignore Cash or Securities Adjustments
		appendIgnoreBalanceAdjustments(balance);

		// Enter Proxy Adjustments
		appendProxyAdjustments(balance, ruleViolationList);
		// Adjust for M2M - "Adding M2M"
		appendM2MAdjustments(balance, ruleViolationList);
		// Adjust for Cash %ages "Cash As % of Total", "Use Securities As Cash Value", "Use Cash As Securities Value"
		appendCashPercentageAdjustments(balance, ruleViolationList);
		// Adjust for Custom Cash Value
		appendCustomCashValueAdjustments(balance);

		// 3. Not System Defined - but Auto loaded adjustments:
		// Lookup Adjustments created from Events
		appendInvestmentEventManagerAdjustments(balance, managerBalanceProcessingConfig);

		// Copy adjustments from previous day
		appendNextDayAdjustments(balance, managerBalanceProcessingConfig);

		// 4. Add back in the manual adjustments
		for (InvestmentManagerAccountBalanceAdjustment manualAdj : CollectionUtils.getIterable(manualAdjustmentList)) {
			balance.addAdjustment(manualAdj);
		}
		balance.recalculateAdjustedValues();

		// Update the List - Add/Update/Delete
		saveInvestmentManagerAccountBalanceRuleViolationList(balance, ruleViolationList);

		// Save Method will reset Processed Type based on Existing Warnings
		balance.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS));

		// Apply Rollup MOC Adjustments
		processProductManagerRollupMOCAdjustments(balance, null);

		return getInvestmentManagerAccountBalanceService().saveInvestmentManagerAccountBalance(balance);
	}


	private void appendInvestmentEventManagerAdjustments(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		for (InvestmentEventManagerAdjustment eventAdjustment : CollectionUtils.getIterable(managerBalanceProcessingConfig.getInvestmentEventManagerAdjustmentList(balance.getManagerAccount(), getInvestmentCalendarService()))) {
			InvestmentManagerAccountBalanceAdjustment adjustment = getInvestmentManagerAccountBalanceService().createInvestmentManagerAccountBalanceAdjustmentFromEvent(eventAdjustment);
			balance.addAdjustment(adjustment);
			balance.recalculateAdjustedValues();
		}
	}


	private void appendNextDayAdjustments(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		for (InvestmentManagerAccountBalanceAdjustment previousAdjustment : CollectionUtils.getIterable(managerBalanceProcessingConfig.getInvestmentManagerPreviousDayAdjustmentList(balance.getManagerAccount(), getInvestmentManagerAccountBalanceService()))) {
			// Extra validation - but should have already been filtered out by search form
			if (previousAdjustment.getAdjustmentType().getNextDayAdjustmentType() != null) {
				InvestmentManagerAccountBalanceAdjustment adjustment = BeanUtils.cloneBean(previousAdjustment, false, false);
				// If original created from Calendar - At this point we want to point back to the previous day's adjustment
				adjustment.setSourceSystemTable(getSystemSchemaService().getSystemTableByName(InvestmentManagerAccountBalanceServiceImpl.INVESTMENT_MANAGER_BALANCE_ADJUSTMENT_TABLE));
				adjustment.setSourceFkFieldId(previousAdjustment.getId());
				adjustment.setNote("Adjustment Automatically applied from previous day [" + previousAdjustment.getAdjustmentType().getName() + "] adjustment");
				adjustment.setManagerAccountBalance(balance);
				adjustment.setAdjustmentType(previousAdjustment.getAdjustmentType().getNextDayAdjustmentType());
				balance.addAdjustment(adjustment);
				balance.recalculateAdjustedValues();
			}
		}
	}


	private void appendIgnoreBalanceAdjustments(InvestmentManagerAccountBalance balance) {
		InvestmentManagerAccount manager = balance.getManagerAccount();
		if (CashAdjustmentType.IGNORE_CASH == manager.getCashAdjustmentType()) {
			if (!MathUtils.isEqual(0, balance.getCashValue())) {
				InvestmentManagerAccountBalanceAdjustment adjustment = new InvestmentManagerAccountBalanceAdjustment();
				adjustment.setAdjustmentType(getInvestmentManagerAccountBalanceAdjustmentTypeSystemDefined(InvestmentManagerAccountBalanceAdjustmentType.IGNORE_CASH));
				adjustment.setCashValue(MathUtils.negate(balance.getCashValue()));
				balance.addAdjustment(adjustment);
				balance.recalculateAdjustedValues();
			}
		}
		if (CashAdjustmentType.IGNORE_SECURITIES == manager.getCashAdjustmentType()) {
			if (!MathUtils.isEqual(0, balance.getSecuritiesValue())) {
				InvestmentManagerAccountBalanceAdjustment adjustment = new InvestmentManagerAccountBalanceAdjustment();
				adjustment.setAdjustmentType(getInvestmentManagerAccountBalanceAdjustmentTypeSystemDefined(InvestmentManagerAccountBalanceAdjustmentType.IGNORE_SECURITIES));
				adjustment.setSecuritiesValue(MathUtils.negate(balance.getSecuritiesValue()));
				balance.addAdjustment(adjustment);
				balance.recalculateAdjustedValues();
			}
		}
	}


	private void appendProxyAdjustments(InvestmentManagerAccountBalance balance, List<RuleViolation> ruleViolationList) {
		InvestmentManagerAccount manager = balance.getManagerAccount();
		if (manager.isProxyAdjustmentAppliedToManager()) {
			// Determine Proxy % Change
			BigDecimal adj = BigDecimal.ZERO;
			try {
				// Validation has been added to save method, but just in case...validate date is populated
				if (manager.getProxyValueDate() == null) {
					throw new ValidationException("Proxy Last Updated Date is required in order to apply a Proxy Performance Adjustment");
				}
				if (manager.getProxyBenchmarkSecurity() != null) {
					if (manager.isProxyToLastKnownPrice()) {
						adj = getMarketDataRetriever().getInvestmentSecurityReturnFlexible(null, manager.getProxyBenchmarkSecurity(), manager.getProxyValueDate(), balance.getBalanceDate(),
								"Proxy Benchmark", true);
					}
					else {
						adj = getMarketDataRetriever().getInvestmentSecurityReturn(null, manager.getProxyBenchmarkSecurity(), manager.getProxyValueDate(), balance.getBalanceDate(), "Proxy Benchmark",
								true);
					}
				}
				else {
					adj = getMarketDataRatesRetriever().getInvestmentInterestRateIndexReturn(manager.getProxyBenchmarkRateIndex(), manager.getProxyValueDate(), balance.getBalanceDate(), true);
				}
			}
			catch (ValidationException e) {
				ruleViolationList.add(getRuleViolationService().createRuleViolationSystemDefined(ExceptionUtils.getDetailedMessage(e), RULE_DEFINITION_PROXY, balance.getId()));
			}
			BigDecimal proxyAdj = MathUtils.getPercentageOf(adj, balance.getSecuritiesValue(), true);
			// If proxy value performance should be inverted - then negate it.
			if (manager.isProxyValueGrowthInverted()) {
				proxyAdj = MathUtils.negate(proxyAdj);
			}
			if (!MathUtils.isNullOrZero(proxyAdj)) {
				InvestmentManagerAccountBalanceAdjustment adjustment = new InvestmentManagerAccountBalanceAdjustment();
				adjustment.setAdjustmentType(getInvestmentManagerAccountBalanceAdjustmentTypeSystemDefined(InvestmentManagerAccountBalanceAdjustmentType.PROXY_ADJUSTMENTS));
				adjustment.setSecuritiesValue(proxyAdj);
				balance.addAdjustment(adjustment);
				balance.recalculateAdjustedValues();
			}
		}
	}


	private void appendM2MAdjustments(InvestmentManagerAccountBalance balance, List<RuleViolation> ruleViolationList) {
		InvestmentManagerAccount manager = balance.getManagerAccount();
		if (manager.isAdjustmentM2MExists()) {

			List<InvestmentManagerAccountAdjustmentM2M> m2mAccounts = manager.getAdjustmentM2MList();
			BigDecimal m2mAmount = BigDecimal.ZERO;
			StringBuilder m2mMissingBuilder = new StringBuilder();
			StringBuilder note = new StringBuilder();
			for (InvestmentManagerAccountAdjustmentM2M m2m : CollectionUtils.getIterable(m2mAccounts)) {
				Date startDate = null;
				Date endDate = balance.getBalanceDate();

				boolean singleDayLookup = false;
				if (m2m.getStartDate() != null) {
					//Only set start date if the dates are different, otherwise use it will be a one day lookup
					if (DateUtils.compare(m2m.getStartDate(), endDate, false) != 0) {
						startDate = m2m.getStartDate();
					}
					else {
						singleDayLookup = true;
					}
				}
				else if (m2m.getDaysOfM2M() != 1) {
					startDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(endDate), (m2m.getDaysOfM2M() - 1) * -1);
				}
				else {
					singleDayLookup = true;
				}

				BigDecimal value;
				if (m2m.getAdjustmentType().isGainLoss()) {
					value = getAccountingPositionDailyM2MForClientAccount(m2m.getReferenceTwo(), m2m.getAdjustmentType(), startDate, endDate);
				}
				else {
					value = getAccountingM2MTransferAmountForClientAccount(m2m.getReferenceTwo(), m2m.getAdjustmentType(), startDate, endDate);
				}

				if (value != null) {
					note.append("[").append(m2m.getAdjustmentTypeLabel()).append("] M2M Included for Client Account [").append(m2m.getReferenceTwo().getNumber()).append("] ");
					if (singleDayLookup) {
						note.append("on [").append(DateUtils.fromDateShort(endDate)).append("]");
					}
					else {
						note.append("from [").append(DateUtils.fromDateShort(startDate)).append("] to [").append(DateUtils.fromDateShort(endDate)).append("] ").append(StringUtils.NEW_LINE);
					}

					m2mAmount = MathUtils.add(m2mAmount, value);
				}
				else {
					// Add M2M Missing warning
					m2mMissingBuilder.append("[").append(m2m.getLabel()).append(" - ").append(m2m.getAdjustmentTypeLabel()).append("]");
				}
			}
			if (!MathUtils.isEqual(0, m2mAmount.doubleValue())) {
				InvestmentManagerAccountBalanceAdjustment adjustment = new InvestmentManagerAccountBalanceAdjustment();
				adjustment.setAdjustmentType(getInvestmentManagerAccountBalanceAdjustmentTypeSystemDefined(InvestmentManagerAccountBalanceAdjustmentType.M2M));
				adjustment.setCashValue(m2mAmount);
				adjustment.setNote(note.toString());
				balance.addAdjustment(adjustment);
				balance.recalculateAdjustedValues();
			}
			if (!StringUtils.isEmpty(m2mMissingBuilder.toString())) {
				String violationNote = "M2M Balance History Missing for Accounts " + m2mMissingBuilder;
				ruleViolationList.add(getRuleViolationService().createRuleViolationSystemDefined(violationNote, RULE_DEFINITION_M2M, balance.getId()));
			}
		}
	}


	private BigDecimal getAccountingM2MTransferAmountForClientAccount(InvestmentAccount clientAccount, ManagerM2MAdjustmentTypes type, Date startDate, Date endDate) {
		AccountingM2MDailySearchForm searchForm = new AccountingM2MDailySearchForm();
		searchForm.setClientInvestmentAccountId(clientAccount.getId());
		// IF NO START DATE, THEN CONSIDERED TO BE ONE DAY M2M Lookup
		if (startDate != null) {
			searchForm.setStartMarkDate(startDate);
			searchForm.setEndMarkDate(endDate);
		}
		else {
			// Check if previous business day is previous week day - if it is - one day lookup, otherwise need to include the holiday mark
			// Same logic as APD and M2M is built on all days except for weekends.  Since securities have different holidays, the APD is now built for every day, but
			// we need to include the holiday mark if the previous weekday was a holiday for us.
			Date previousBusinessDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(endDate), -1);
			if (type.isExcludeHoliday() || DateUtils.compare(DateUtils.getPreviousWeekday(endDate), previousBusinessDay, false) == 0) {
				searchForm.setMarkDate(endDate);
			}
			else {
				searchForm.setStartMarkDate(DateUtils.addDays(previousBusinessDay, 1)); // Does a >= search, so start the day after the previous business day
				searchForm.setEndMarkDate(endDate);
			}
		}
		List<AccountingM2MDaily> dailyList = getAccountingM2MService().getAccountingM2MDailyList(searchForm);
		if (CollectionUtils.isEmpty(dailyList)) {
			return null;
		}
		if (ManagerM2MAdjustmentTypes.TRANSFERRED_M2M == type || ManagerM2MAdjustmentTypes.TRANSFERRED_M2M_EXCLUDE_HOLIDAYS == type) {
			return CoreMathUtils.sumProperty(dailyList, AccountingM2MDaily::getCoalesceTransferOurMarkAmount);
		}
		return CoreMathUtils.sumProperty(dailyList, AccountingM2MDaily::getOurMarkAmount);
	}


	private BigDecimal getAccountingPositionDailyM2MForClientAccount(InvestmentAccount clientAccount, ManagerM2MAdjustmentTypes type, Date startDate, Date endDate) {

		AccountingPositionDailySearchForm searchForm = new AccountingPositionDailySearchForm();
		searchForm.setClientInvestmentAccountId(clientAccount.getId());
		// IF NO START DATE, THEN CONSIDERED TO BE ONE DAY M2M Lookup
		if (startDate != null) {
			searchForm.setStartPositionDate(startDate);
			searchForm.setEndPositionDate(endDate);
		}
		else {

			// Check if previous business day is previous week day - if it is - one day lookup, otherwise need to include the holiday mark
			// APD is built on all days except for weekends.  Since securities have different holidays, the APD is now built for every day, but
			// we need to include the holiday mark if the previous weekday was a holiday for us.
			Date previousBusinessDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(endDate), -1);
			if (type.isExcludeHoliday() || DateUtils.compare(DateUtils.getPreviousWeekday(endDate), previousBusinessDay, false) == 0) {
				searchForm.setPositionDate(endDate);
			}
			else {
				searchForm.setStartPositionDate(DateUtils.addDays(previousBusinessDay, 1)); // Does a >= search, so start the day after the previous business day
				searchForm.setEndPositionDate(endDate);
			}
		}
		List<AccountingPositionDaily> dailyList = getAccountingPositionDailyService().getAccountingPositionDailyList(searchForm);
		if (CollectionUtils.isEmpty(dailyList)) {
			return null;
		}

		BigDecimal value = BigDecimal.ZERO;

		for (AccountingPositionDaily apd : CollectionUtils.getIterable(dailyList)) {
			// Futures Only
			if (apd.getAccountingTransaction().getInvestmentSecurity() != null && InvestmentUtils.isSecurityOfType(apd.getAccountingTransaction().getInvestmentSecurity(), InvestmentType.FUTURES)) {
				// BECAUSE FOREIGN FUTURES INCLUDE CURRENCY M2M WITHIN IT - NEED TO CALCULATE OFF OF LOCAL AND USE FX RATE TO GET FOREIGN FUTURES WITHOUT THE CURRENCY M2M
				// NO AFFECT ON THE DOMESTIC FUTURES, BECAUSE THE FX RATE WILL BE 1
				// ((OTE Local + Realized Local - Previous OTE Local - Commission Local) * FX Rate)
				BigDecimal positionValue = MathUtils.add(apd.getOpenTradeEquityLocal(), apd.getTodayRealizedGainLossLocal());
				positionValue = MathUtils.subtract(positionValue, apd.getPriorOpenTradeEquityLocal());
				positionValue = MathUtils.subtract(positionValue, apd.getTodayCommissionLocal());
				positionValue = MathUtils.multiply(positionValue, apd.getMarketFxRate());
				value = MathUtils.add(value, positionValue);
			}
		}
		return value;
	}


	private void appendCashPercentageAdjustments(InvestmentManagerAccountBalance balance, List<RuleViolation> ruleViolationList) {
		InvestmentManagerAccount manager = balance.getManagerAccount();

		BigDecimal cashPercent = manager.getCashPercent();

		if (manager.getCashPercentManager() != null) {
			// Get last balance (from previous day) and determine cash %age (Use adjusted values) (Cash * 100)/Total = Cash Percent (as a whole number)
			InvestmentManagerAccountBalance cashPercentManagerBalance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(
					manager.getCashPercentManager().getId(), balance.getBalanceDate(), false);
			if (cashPercentManagerBalance == null) {
				String violationNote = "Cash Percent Manager " + manager.getCashPercentManager() + " balance missing - cannot determine current cash percentage.";
				ruleViolationList.add(getRuleViolationService().createRuleViolationSystemDefined(violationNote, RULE_DEFINITION_CASH, balance.getId()));
				return;
			}
			cashPercent = CoreMathUtils.getPercentValue(cashPercentManagerBalance.getAdjustedCashValue(), cashPercentManagerBalance.getAdjustedTotalValue(), true);
		}
		if (cashPercent != null) {
			InvestmentManagerAccountBalanceAdjustmentType adjType;
			BigDecimal cashBalance = balance.getAdjustedCashValue();
			BigDecimal securitiesBalance = balance.getAdjustedSecuritiesValue();

			BigDecimal cashAdj = BigDecimal.ZERO;
			BigDecimal secAdj = BigDecimal.ZERO;

			// Apply Cash Balance to Securities Balance
			if (MathUtils.isEqual(0, cashPercent)) {
				adjType = getInvestmentManagerAccountBalanceAdjustmentTypeSystemDefined(InvestmentManagerAccountBalanceAdjustmentType.CASH_AS_SECURITIES);
				secAdj = MathUtils.add(secAdj, cashBalance);
				cashAdj = MathUtils.subtract(cashAdj, cashBalance);
			}
			// Apply Securities Balance to Cash Balance
			else if (MathUtils.isEqual(100, cashPercent)) {
				adjType = getInvestmentManagerAccountBalanceAdjustmentTypeSystemDefined(InvestmentManagerAccountBalanceAdjustmentType.SECURITIES_AS_CASH);
				cashAdj = MathUtils.add(cashAdj, securitiesBalance);
				secAdj = MathUtils.subtract(secAdj, securitiesBalance);
			}
			// Apply Cash as Custom Percentage
			else {
				adjType = getInvestmentManagerAccountBalanceAdjustmentTypeSystemDefined(InvestmentManagerAccountBalanceAdjustmentType.CASH_PERCENT);
				BigDecimal total = MathUtils.add(cashBalance, securitiesBalance);
				BigDecimal cashBalanceExpected = MathUtils.getPercentageOf(cashPercent, total, true);
				BigDecimal securitiesBalanceExpected = MathUtils.subtract(total, cashBalanceExpected);

				cashAdj = MathUtils.add(cashAdj, MathUtils.subtract(cashBalanceExpected, cashBalance));
				secAdj = MathUtils.add(secAdj, MathUtils.subtract(securitiesBalanceExpected, securitiesBalance));
			}
			if (!MathUtils.isEqual(0, cashAdj) || !MathUtils.isEqual(0, secAdj)) {
				InvestmentManagerAccountBalanceAdjustment adjustment = new InvestmentManagerAccountBalanceAdjustment();
				adjustment.setAdjustmentType(adjType);
				adjustment.setCashValue(cashAdj);
				adjustment.setSecuritiesValue(secAdj);
				balance.addAdjustment(adjustment);
			}
		}
	}


	private void appendCustomCashValueAdjustments(InvestmentManagerAccountBalance balance) {
		InvestmentManagerAccount manager = balance.getManagerAccount();

		if (CashAdjustmentType.CUSTOM_VALUE == manager.getCashAdjustmentType() && !MathUtils.isNullOrZero(manager.getCustomCashValue())) {
			BigDecimal cashAdj = MathUtils.subtract(manager.getCustomCashValue(), balance.getAdjustedCashValue());
			if (!MathUtils.isNullOrZero(cashAdj)) {
				BigDecimal secAdj = MathUtils.negate(cashAdj);

				InvestmentManagerAccountBalanceAdjustmentType adjType = getInvestmentManagerAccountBalanceAdjustmentTypeSystemDefined(InvestmentManagerAccountBalanceAdjustmentType.CUSTOM_CASH_VALUE);
				InvestmentManagerAccountBalanceAdjustment adjustment = new InvestmentManagerAccountBalanceAdjustment();
				adjustment.setAdjustmentType(adjType);
				adjustment.setCashValue(cashAdj);
				adjustment.setSecuritiesValue(secAdj);
				balance.addAdjustment(adjustment);
			}
		}
	}

	/////////////////////////////////////////////////////////////////////
	//////////                 Helper Methods               /////////////
	/////////////////////////////////////////////////////////////////////


	private List<InvestmentManagerAccount> getInvestmentManagerAccountActiveList(InvestmentManagerAccountSearchForm searchForm) {
		if (searchForm == null) {
			searchForm = new InvestmentManagerAccountSearchForm();
		}
		// Only Applies to Active Managers
		searchForm.setInactive(false);
		return getInvestmentManagerAccountService().getInvestmentManagerAccountList(searchForm);
	}


	private InvestmentManagerAccountBalanceAdjustmentType getInvestmentManagerAccountBalanceAdjustmentTypeSystemDefined(String typeName) {
		InvestmentManagerAccountBalanceAdjustmentType type = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceAdjustmentTypeByName(typeName);
		if (type.isSystemDefined()) {
			return type;
		}
		return null;
	}


	private ManagerPositionHistorySummary getCustodianBalance(BusinessCompany custodianCompany, String custodianAccountNumber, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		ValidationUtils.assertNotNull(custodianCompany, "Custodian is required in order to lookup custodial balance information.");
		Integer custodianId = custodianCompany.getId();
		ValidationUtils.assertNotNull(custodianAccountNumber, "Custodian Account Number is required in order to lookup balance information.");
		ValidationUtils.assertNotNull(managerBalanceProcessingConfig.getBalanceDate(), "Balance Date is required in order to lookup balance information.");

		if (managerBalanceProcessingConfig.getCustodianBalanceList(custodianId) == null) {
			Event<Object, List<ManagerPositionHistorySummary>> event = new EventObject<>("Integration Manager Position History Summary");
			event.addContextValue("custodianId", custodianId);
			event.addContextValue("balanceDate", managerBalanceProcessingConfig.getBalanceDate());
			getEventHandler().raiseEvent(event);
			managerBalanceProcessingConfig.setCustodianBalanceList(custodianId, event.getResult());
		}

		try {
			//Note, cannot use BeanUtils.filter because need equalsIgnoreCase
			List<ManagerPositionHistorySummary> balances = new ArrayList<>();
			for (ManagerPositionHistorySummary summary : CollectionUtils.getIterable(managerBalanceProcessingConfig.getCustodianBalanceList(custodianId))) {
				if (summary.getBankCode().trim().equalsIgnoreCase(custodianAccountNumber.trim())) {
					balances.add(summary);
				}
			}
			return CollectionUtils.getOnlyElement(balances);
		}
		catch (Exception e) {
			throw new ValidationException("Error locating Bank Position Summary Records for Bank [" + custodianId + "] and Bank Code [" + custodianAccountNumber + "] for Date ["
					+ DateUtils.fromDateShort(managerBalanceProcessingConfig.getBalanceDate()) + "]: " + ExceptionUtils.getDetailedMessage(e), e);
		}
	}


	/**
	 * Manager Account Balances are only auto loaded by the system if they are ACTIVE and Proxy Managers, or have Custodian Account information available, or
	 * independent Linked managers (not PIOS dependent) Otherwise, for other active managers the original balances must be user entered, uploaded separately, or
	 * handled in another part of the system (Linked PIOS dependent or Rollup)
	 */
	private boolean isInvestmentManagerAccountAutoLoaded(InvestmentManagerAccount manager) {
		// Verify Manager is Active First
		if (manager.isInactive()) {
			return false;
		}
		// Flag ALL Linked Managers as Auto Loaded - Processing will skip if the Run is Missing from the Processing Config Map
		if (manager.isLinkedManager()) {
			return true;
		}
		// Handled separately in {@link ProductManagerAccountBalanceObserver}
		if (manager.isRollupManager()) {
			return false;
		}
		if (manager.isProxyManager()) {
			return true;
		}
		return (manager.getCustodianAccount() != null);
	}


	private void saveInvestmentManagerAccountBalanceRuleViolationList(InvestmentManagerAccountBalance balance, List<RuleViolation> ruleViolationList) {
		getRuleViolationService().updateRuleViolationListForEntityAndDefinition(ruleViolationList, "InvestmentManagerAccountBalance", balance.getId(), null);
	}

	/////////////////////////////////////////////////////////////////////
	////////        Linked Manager - Processing Methods          ////////
	/////////////////////////////////////////////////////////////////////


	@Override
	// Called From PIOS Process where linked runs have already been set up and mapped to the managers
	public void processProductLinkedManagerAccountListForInvestmentAccount(int investmentAccountId, Date balanceDate, Map<Integer, PortfolioRun> clientAccountLinkedRunMap) {
		List<InvestmentManagerAccountAssignment> assignments = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentListByAccount(investmentAccountId, true);
		List<InvestmentManagerAccountAssignment> linkedAssignments = BeanUtils.filter(assignments, managerAssignment -> managerAssignment.getReferenceOne().isLinkedManager(), true);

		// Put Linked Manager Assignments in type sorted order so more likely that any dependencies for linked manager -> linked manager will be done first
		linkedAssignments = BeanUtils.sortWithFunction(linkedAssignments, linkedAssignment -> linkedAssignment.getReferenceOne().getLinkedManagerType().getOrder(), true);

		ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig = new ProductManagerBalanceProcessingConfig(balanceDate, getCalendarBusinessDayService(), clientAccountLinkedRunMap, false);

		for (InvestmentManagerAccountAssignment assign : CollectionUtils.getIterable(linkedAssignments)) {
			InvestmentManagerAccount manager = assign.getReferenceOne();
			processProductLinkedManagerAccountBalanceImpl(manager, ProductManagerBalanceProcessingTypes.LOAD_PROCESS, managerBalanceProcessingConfig);
		}

		// Rollup manager may be assigned, but the specific linked manager may not be - this way we can be sure we create all linked manager balances
		List<InvestmentManagerAccountAssignment> rollupAssignments = BeanUtils.filter(assignments, managerAssignment -> managerAssignment.getReferenceOne().isRollupManager(), true);
		for (InvestmentManagerAccountAssignment assign : CollectionUtils.getIterable(rollupAssignments)) {
			processProductLinkedManagerAccountBalanceFromRollupManager(assign.getReferenceOne(), ProductManagerBalanceProcessingTypes.LOAD_PROCESS, managerBalanceProcessingConfig);
		}
	}


	/**
	 * For cases of rollup managers - the linked manager itself may not be directly assigned to the account, but the parent rollup manager is - this will check
	 * rollups and their children and load all child linked managers for the account indirectly related through the rollup assignment
	 */
	private void processProductLinkedManagerAccountBalanceFromRollupManager(InvestmentManagerAccount rollupManager, ProductManagerBalanceProcessingTypes processingType,
	                                                                        ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		// Pull Rollups from the DB so we get children populated
		rollupManager = getInvestmentManagerAccountService().getInvestmentManagerAccount(rollupManager.getId());
		for (InvestmentManagerAccountRollup rollup : CollectionUtils.getIterable(rollupManager.getRollupManagerList())) {
			if (rollup.getReferenceTwo().isLinkedManager()) {
				processProductLinkedManagerAccountBalanceImpl(rollup.getReferenceTwo(), processingType, managerBalanceProcessingConfig);
			}
			// Check children of rollups that are rollups themselves, etc. etc.
			else if (rollup.getReferenceTwo().isRollupManager()) {
				processProductLinkedManagerAccountBalanceFromRollupManager(rollup.getReferenceTwo(), processingType, managerBalanceProcessingConfig);
			}
		}
	}


	/**
	 * Process Linked manager balance - passes off to dependent or independent processing method based on the LinkedManagerType processingDependent flag
	 * <p>
	 * NOTE: All properties are passed through all of the methods on the service because a linked manager could dependent on another linked manager which will
	 * have a different load type, so we need everything available even if the calculator itself doesn't use all the properties.
	 */
	private Optional<Boolean> processProductLinkedManagerAccountBalanceImpl(InvestmentManagerAccount manager, ProductManagerBalanceProcessingTypes processingType,
	                                                                        ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {

		if (manager.isLinkedManagerProcessingDependent()) {
			return processProductLinkedManagerAccountBalanceDependentImpl(manager, processingType, managerBalanceProcessingConfig);
		}
		return processProductLinkedManagerAccountBalanceIndependentImpl(manager, processingType, managerBalanceProcessingConfig);
	}


	/**
	 * PIOS processing dependent LinkedManagerType balance processing
	 */
	private Optional<Boolean> processProductLinkedManagerAccountBalanceDependentImpl(InvestmentManagerAccount manager, ProductManagerBalanceProcessingTypes processingType,
	                                                                                 ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {

		// If this method was called without a run - return it as skipped
		PortfolioRun linkedRun = managerBalanceProcessingConfig.getLinkedRunForManagerAccount(manager);
		if (linkedRun == null) {
			return Optional.empty();
		}

		// Get the Existing Balance Value (in case it needs to be updated)
		InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(manager.getId(), managerBalanceProcessingConfig.getBalanceDate(), true);

		// OVERRIDE FOR RELOADING: For linked run dependent managers, will attempt to reload if the linked run has been updated since the manager balance was created
		// OR if the Manager Account has been updated since the manager balance was created
		if (balance != null && !processingType.isForceReload() && DateUtils.compare(balance.getCreateDate(), linkedRun.getUpdateDate(), true) > 0
				&& DateUtils.compare(balance.getCreateDate(), manager.getUpdateDate(), true) > 0) {
			return Optional.of(false);
		}

		// Calculate New Balance - Pass empty manager balance - expect cash and/or securities and optional note to be populated
		InvestmentManagerAccountBalance newBalance = new InvestmentManagerAccountBalance(manager, managerBalanceProcessingConfig.getBalanceDate());

		ProductLinkedManagerDependentCalculator calculator = getProductLinkedManagerCalculatorLocator().locateDependent(manager.getLinkedManagerType());
		processLinkedManagerBalanceDependency(calculator, manager, processingType, managerBalanceProcessingConfig);
		calculator.calculate(newBalance, linkedRun, managerBalanceProcessingConfig.isDoNotValidatePortfolioRunStatus());

		saveManagerBalance(balance, newBalance, processingType, managerBalanceProcessingConfig);
		return Optional.of(true);
	}


	/**
	 * Independent LinkedManagerType balance processing
	 */
	private Optional<Boolean> processProductLinkedManagerAccountBalanceIndependentImpl(InvestmentManagerAccount manager, ProductManagerBalanceProcessingTypes processingType,
	                                                                                   ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {

		// Get the Existing Balance Value (in case it needs to be updated)
		InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(manager.getId(), managerBalanceProcessingConfig.getBalanceDate(), true);

		// Balance Exists and not reloading...
		// Note: For linked run dependent managers, will attempt to reload anyway if at least the managers were allocated after the last time the balance was created
		if (balance != null && processingType.isLoadMissingOnly()) {
			if (processingType.isProcessAdjustments() && !RuleViolationUtil.isRuleViolationAwareProcessed(balance)) {
				processProductManagerAccountBalanceAdjustmentsForBalance(balance);
				return Optional.of(true);
			}
			return Optional.empty(); // skipped
		}

		ProductLinkedManagerIndependentCalculator calculator = getProductLinkedManagerCalculatorLocator().locateIndependent(manager.getLinkedManagerType());
		processLinkedManagerBalanceDependency(calculator, manager, processingType, managerBalanceProcessingConfig);

		// Calculate New Balance - Pass empty manager balance - expect cash and/or securities and optional note to be populated
		InvestmentManagerAccountBalance newBalance = new InvestmentManagerAccountBalance(manager, managerBalanceProcessingConfig.getBalanceDate());
		calculator.calculate(newBalance, managerBalanceProcessingConfig);

		saveManagerBalance(balance, newBalance, processingType, managerBalanceProcessingConfig);
		return Optional.of(true);
	}


	/**
	 * Calls the getInvestmentManagerAccountDependency on the calculator to determine what, if applies, manager this linked manager needs loaded in order to
	 * calculate it's balance. If the balance for the given date does not exist - will attempt to load it if possible.
	 */
	private void processLinkedManagerBalanceDependency(ProductLinkedManagerCalculator calculator, InvestmentManagerAccount manager, ProductManagerBalanceProcessingTypes processingType, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {

		// Check If there is a required manager dependency and if so, that balance has been loaded
		InvestmentManagerAccount dependentManager = calculator.getInvestmentManagerAccountDependency(manager);
		if (dependentManager != null) {
			InvestmentManagerAccountBalance dependentBalance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(dependentManager.getId(), managerBalanceProcessingConfig.getBalanceDate(), false);
			// NOTE: Changed order of linked manager type loads so hopefully doesn't come to this case, but just in case...
			if (dependentBalance == null) {
				// Validate For Circular dependency
				validateManagerCircularDependency(CollectionUtils.createList(manager), dependentManager);

				// If OK - but balance is missing - try to load dependent balance(s) if possible
				processProductLinkedManagerAccountBalanceImpl(dependentManager, processingType, managerBalanceProcessingConfig);
			}
		}
	}


	private void validateManagerCircularDependency(List<InvestmentManagerAccount> parentList, InvestmentManagerAccount manager) {
		// As soon as we find a manager that is already in the parent list - again as a child - throw an exception - circular dependency found
		if (!CollectionUtils.isEmpty(BeanUtils.filter(parentList, InvestmentManagerAccount::getId, manager.getId()))) {
			throw new ValidationExceptionWithCause("InvestmentManagerAccount", manager.getId(),
					"Invalid manager account setup.  There is a circular dependency for manager balance loading for manager account [" + manager.getLabel() + "]");
		}

		if (manager.isLinkedManager()) {
			ProductLinkedManagerCalculator calculator;
			if (manager.isLinkedManagerProcessingDependent()) {
				calculator = getProductLinkedManagerCalculatorLocator().locateDependent(manager.getLinkedManagerType());
			}
			else {
				calculator = getProductLinkedManagerCalculatorLocator().locateIndependent(manager.getLinkedManagerType());
			}
			InvestmentManagerAccount linkedManager = calculator.getInvestmentManagerAccountDependency(manager);
			if (linkedManager != null && (linkedManager.isLinkedManager() || linkedManager.isRollupManager())) {
				parentList.add(manager);
				validateManagerCircularDependency(parentList, linkedManager);
			}
		}

		if (manager.isRollupManager()) {
			parentList.add(manager);
			for (InvestmentManagerAccountRollup rollup : CollectionUtils.getIterable(getInvestmentManagerAccountService().getInvestmentManagerAccountRollupListByParent(manager.getId()))) {
				if (rollup.getReferenceTwo().isLinkedManager() || rollup.getReferenceTwo().isRollupManager()) {
					validateManagerCircularDependency(parentList, rollup.getReferenceTwo());
				}
			}
		}
	}

	/////////////////////////////////////////////////////////////////////
	//////////            Getter & Setter Methods           /////////////
	/////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public InvestmentCalendarService getInvestmentCalendarService() {
		return this.investmentCalendarService;
	}


	public void setInvestmentCalendarService(InvestmentCalendarService investmentCalendarService) {
		this.investmentCalendarService = investmentCalendarService;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}


	public ProductLinkedManagerCalculatorLocator getProductLinkedManagerCalculatorLocator() {
		return this.productLinkedManagerCalculatorLocator;
	}


	public void setProductLinkedManagerCalculatorLocator(ProductLinkedManagerCalculatorLocator productLinkedManagerCalculatorLocator) {
		this.productLinkedManagerCalculatorLocator = productLinkedManagerCalculatorLocator;
	}


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataRatesRetriever getMarketDataRatesRetriever() {
		return this.marketDataRatesRetriever;
	}


	public void setMarketDataRatesRetriever(MarketDataRatesRetriever marketDataRatesRetriever) {
		this.marketDataRatesRetriever = marketDataRatesRetriever;
	}


	public RuleViolationStatusService getRuleViolationStatusService() {
		return this.ruleViolationStatusService;
	}


	public void setRuleViolationStatusService(RuleViolationStatusService ruleViolationStatusService) {
		this.ruleViolationStatusService = ruleViolationStatusService;
	}
}
