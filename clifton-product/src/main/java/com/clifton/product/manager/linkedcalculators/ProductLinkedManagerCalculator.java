package com.clifton.product.manager.linkedcalculators;


import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.LinkedManagerTypes;


/**
 * The <code>ProductManagerAccountBalanceLinkedCalculator</code> interface defines
 * the methods that linked manager balance calculators must implement
 * <p>
 * Actual "calculate" methods are on extending interfaces
 *
 * @author manderson
 */
public interface ProductLinkedManagerCalculator {

	/**
	 * Returns the {@link LinkedManagerTypes} that the calculator applies to
	 */
	public LinkedManagerTypes getLinkedManagerType();


	/**
	 * Returns the manager account this manager account depends on.  Currently only used for {@link LinkedManagerTypes} MANAGER_BALANCE_PROPRIETARY_FUND_PORTION
	 * Before attempting to calculate the balance, the dependent manager balance is loaded first if missing and also is used to verify no circular dependencies.
	 */
	public InvestmentManagerAccount getInvestmentManagerAccountDependency(InvestmentManagerAccount manager);
}
