package com.clifton.product.manager.linkedcalculators.independent;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.AccountingBalanceCommand;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingAccountBalanceSearchForm;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.manager.linkedcalculators.BaseProductLinkedManagerCalculatorImpl;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ProductManagerAccountBalanceMarketValueCalculator</code> handles loading
 * Cash/Securities balances from Positions Market Value and/or GL Account Balances (Cash)
 * <p>
 * <p>
 * Used by:
 * <p>
 * POSITIONS_MARKET_VALUE, // Securities Only: comes from the market value of our positions held in the specified account.
 * POSITIONS_MARKET_VALUE_DURATION_ADJUSTED, // Securities Only: market value * security duration / benchmark duration.
 * TOTAL_MARKET_VALUE, // Cash (Cash Market Value) & Securities:  Positions Market Value.
 * CASH_MARKET_VALUE, // Cash Only: comes from the General Ledger cash balance held in the specified client account and broker account (optional)
 *
 * @author manderson
 */
public class ProductLinkedManagerForMarketValueCalculator extends BaseProductLinkedManagerCalculatorImpl implements ProductLinkedManagerIndependentCalculator {

	private AccountingAccountService accountingAccountService;
	private AccountingBalanceService accountingBalanceService;

	////////////////////////////////////////////////////////////////////////////////


	// Defined by Calculator definition in application context - difference between Cash, Positions, Total Market Value, etc.
	private boolean cash; // CASH_MARKET_VALUE, TOTAL_MARKET_VALUE
	private boolean securities; // POSITIONS_MARKET_VALUE, POSITIONS_MARKET_VALUE_DURATION_ADJUSTED, TOTAL_MARKET_VALUE
	private boolean durationAdjusted; // POSITIONS_MARKET_VALUE_DURATION_ADJUSTED


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void calculate(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		// Set Cash Value
		if (isCash()) {
			BigDecimal cashValue = BigDecimal.ZERO;

			Integer holdingAccountId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_HOLDING_ACCOUNT_FIELD);

			String collateralOptionValue = (String) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_CASH_COLLATERAL_OPTION);
			// Including all Collateral for the Client Account - set the Cash Value, and then change the option to Exclude, so not double counted in specific client/holding cash lookup

			AccountingBalanceCommand command = AccountingBalanceCommand.forClientAccountOnTransactionDate(balance.getManagerAccount().getLinkedInvestmentAccount().getId(), balance.getBalanceDate())
					.withHoldingAccountId(holdingAccountId)
					.withNetOnly(true);

			if (LINKED_MANAGER_CASH_COLLATERAL_OPTION_INCLUDE_CLIENT_ACCOUNT.equals(collateralOptionValue)) {
				command.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_COLLATERAL_ACCOUNTS);
				cashValue = getAccountingBalanceService().getAccountingBalance(command);
				collateralOptionValue = LINKED_MANAGER_CASH_COLLATERAL_OPTION_EXCLUDE;
			}

			command = AccountingBalanceCommand.forClientAccountOnTransactionDate(balance.getManagerAccount().getLinkedInvestmentAccount().getId(), balance.getBalanceDate())
					.withHoldingAccountId(holdingAccountId)
					.withNetOnly(true);

			command.setExcludeCashCollateral(LINKED_MANAGER_CASH_COLLATERAL_OPTION_EXCLUDE.equals(collateralOptionValue));

			cashValue = MathUtils.add(
					cashValue,
					getAccountingBalanceService().getAccountingBalance(command));

			// NOTE: Will be refactored to be included in Cash Lookup
			// Add Receivables Balance
			AccountingAccountBalanceSearchForm searchForm = new AccountingAccountBalanceSearchForm();
			searchForm.setClientAccountId(balance.getManagerAccount().getLinkedInvestmentAccount().getId());
			searchForm.setHoldingAccountId(holdingAccountId);
			searchForm.setTransactionDate(balance.getBalanceDate());
			searchForm.setAccountingAccountId(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_DIVIDEND_RECEIVABLE).getId());
			cashValue = MathUtils.add(cashValue, getAccountingBalanceService().getAccountingAccountBalanceBase(searchForm));
			balance.setCashValue(cashValue);
		}

		// Set Securities Value
		if (isSecurities()) {
			BigDecimal securitiesValue = BigDecimal.ZERO;
			List<AccountingPositionDaily> accountingPositionDailyList = getAccountingPositionDailyList(balance, managerBalanceProcessingConfig);
			if (!CollectionUtils.isEmpty(accountingPositionDailyList)) {
				if (isDurationAdjusted()) {
					BigDecimal benchmarkDuration;
					if (balance.getManagerAccount().getBenchmarkSecurity() != null) {
						benchmarkDuration = managerBalanceProcessingConfig.getSecurityDuration(balance.getManagerAccount().getBenchmarkSecurity(), getMarketDataRetriever(), balance.getManagerAccount().getDurationFieldNameOverride());
					}
					else {
						benchmarkDuration = BigDecimal.ONE;
					}

					for (AccountingPositionDaily accountingPositionDaily : CollectionUtils.getIterable(accountingPositionDailyList)) {
						BigDecimal marketValue = getMarketValueInManagerBaseCurrency(balance.getManagerAccount(), accountingPositionDaily, managerBalanceProcessingConfig);
						if (!MathUtils.isNullOrZero(marketValue)) {
							BigDecimal securityDuration = managerBalanceProcessingConfig.getSecurityDuration(accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity(), getMarketDataRetriever(), balance.getManagerAccount().getDurationFieldNameOverride());
							marketValue = MathUtils.divide(MathUtils.multiply(marketValue, securityDuration), benchmarkDuration);
							securitiesValue = MathUtils.add(securitiesValue, marketValue);
						}
					}
				}
				else {
					for (AccountingPositionDaily accountingPositionDaily : CollectionUtils.getIterable(accountingPositionDailyList)) {
						securitiesValue = MathUtils.add(securitiesValue, getMarketValueInManagerBaseCurrency(balance.getManagerAccount(), accountingPositionDaily, managerBalanceProcessingConfig));
					}
				}
			}

			// Include Currency - Use separate lookup because CCY is now part of cash - but leave it in position market value?
			// Note - Just like position look ups - Implementing methods cache balance value list for all look ups per client/holding/investment group
			// and then use that cached lookup for instrument, hierarchy, type filtering
			List<AccountingBalanceValue> valueList = getAccountingBalanceValueCurrencyList(balance, managerBalanceProcessingConfig);
			for (AccountingBalanceValue accountingBalanceValue : CollectionUtils.getIterable(valueList)) {
				securitiesValue = MathUtils.add(securitiesValue, getMarketValueInManagerBaseCurrency(balance.getManagerAccount(), accountingBalanceValue, managerBalanceProcessingConfig));
			}
			balance.setSecuritiesValue(securitiesValue);
		}
	}


	private BigDecimal getMarketValueInManagerBaseCurrency(InvestmentManagerAccount managerAccount, AccountingPositionDaily accountingPositionDaily, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		return managerBalanceProcessingConfig.getAccountingValueInManagerBaseCurrency(managerAccount, accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity(), accountingPositionDaily.getMarketValueLocal(), accountingPositionDaily.getMarketValueBase(), getMarketDataExchangeRatesApiService());
	}


	private BigDecimal getMarketValueInManagerBaseCurrency(InvestmentManagerAccount managerAccount, AccountingBalanceValue accountingBalanceValue, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		return managerBalanceProcessingConfig.getAccountingValueInManagerBaseCurrency(managerAccount, accountingBalanceValue.getInvestmentSecurity(), accountingBalanceValue.getLocalMarketValue(), accountingBalanceValue.getBaseMarketValue(), getMarketDataExchangeRatesApiService());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isDurationAdjusted() {
		return this.durationAdjusted;
	}


	public void setDurationAdjusted(boolean durationAdjusted) {
		this.durationAdjusted = durationAdjusted;
	}


	public boolean isCash() {
		return this.cash;
	}


	public void setCash(boolean cash) {
		this.cash = cash;
	}


	public boolean isSecurities() {
		return this.securities;
	}


	public void setSecurities(boolean securities) {
		this.securities = securities;
	}


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}
}
