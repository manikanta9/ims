package com.clifton.product.manager.linkedcalculators.independent;


import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalDetailSearchForm;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.manager.linkedcalculators.BaseProductLinkedManagerCalculatorImpl;

import java.util.List;


/**
 * The <code>ProductLinkedManagerForAccountingEventJournalCalculator</code> ...
 * <p>
 * PENDING_EVENT_JOURNAL_BALANCE("Pending Event Journal Balance", false); // This manager\'s balance (Cash Only) comes from unbooked accounting event journals for the selected account/type/investment group options.
 *
 * @author manderson
 */
public class ProductLinkedManagerForAccountingEventJournalCalculator extends BaseProductLinkedManagerCalculatorImpl implements ProductLinkedManagerIndependentCalculator {

	private AccountingEventJournalService accountingEventJournalService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void calculate(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		AccountingEventJournalDetailSearchForm searchForm = new AccountingEventJournalDetailSearchForm();

		// Common Filters
		searchForm.setClientInvestmentAccountId(balance.getManagerAccount().getLinkedInvestmentAccount().getId());
		searchForm.setHoldingInvestmentAccountId((Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_HOLDING_ACCOUNT_FIELD));

		Integer eventJournalTypeId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_ACCOUNTING_EVENT_JOURNAL_TYPE);
		searchForm.setJournalTypeId(eventJournalTypeId == null ? null : eventJournalTypeId.shortValue());

		Integer investmentGroupId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_GROUP_FIELD);
		searchForm.setInvestmentGroupId(investmentGroupId == null ? null : investmentGroupId.shortValue());

		// UNBOOKED ONLY - This is what logic the screen uses:
		searchForm.setAccrualReversalNotBooked(true);
		//searchForm.setJournalNotBooked(true);

		List<AccountingEventJournalDetail> detailList = getAccountingEventJournalService().getAccountingEventJournalDetailList(searchForm);

		// Put Everything into cash
		balance.setCashValue(CoreMathUtils.sumProperty(detailList, AccountingEventJournalDetail::getTransactionAmount));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}
}
