package com.clifton.product.manager.job;

import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;

import java.util.Date;


/**
 * @author TerryS
 */
public class AccountBalanceAdjustmentsCommand extends BaseAccountBalanceCommand {

	public static AccountBalanceAdjustmentsCommand ofSynchronous(InvestmentManagerAccountSearchForm searchForm, Date balanceDate, boolean reload) {
		AccountBalanceAdjustmentsCommand accountBalanceCommand = new AccountBalanceAdjustmentsCommand();
		accountBalanceCommand.setSearchForm(searchForm);
		accountBalanceCommand.setBalanceDate(balanceDate);
		accountBalanceCommand.setReload(reload);
		accountBalanceCommand.setSynchronous(true);
		return accountBalanceCommand;
	}


	@Override
	public String getRunId() {
		return "ADJ_" + super.getRunId();
	}
}
