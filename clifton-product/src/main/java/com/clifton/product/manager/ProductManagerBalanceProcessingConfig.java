package com.clifton.product.manager;

import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEventManagerAdjustment;
import com.clifton.investment.calendar.search.InvestmentEventManagerAdjustmentSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductManagerBalanceProcessingConfig</code> class is used to hold common data used across balance loading and processing that can be easily retrieved
 *
 * @author manderson
 */
public class ProductManagerBalanceProcessingConfig {

	private final Date balanceDate;
	private final Date previousBusinessDay; // Used for Finding Previous Day Adjustments
	private final Date nextBusinessDay; // Used for Finding Events with Manager Adjustments

	// Map of Custodian Balances to easily pull values for a particular account
	Map<Integer, List<ManagerPositionHistorySummary>> custodianBalanceMap = new HashMap<>();

	// When loading Linked Managers that Depend on a run, the runs are stored here, by client account, so if the run exists in this map, the manager balance can be loaded
	// This is set when loading manager balance comes from portfolio run processing - that is the only time the dependent balances are loaded
	Map<Integer, PortfolioRun> clientAccountPortfolioRunMap;
	// When called during processing of a run, that run is technically not completed, but we know all necessary data has been processed
	// so in that case we don't need the run to be in a completed state
	private final boolean doNotValidatePortfolioRunStatus;

	// Map of Client Account Positions (Key is Client Account ID - Holding Account ID - Investment Group ID)
	Map<String, List<AccountingPositionDaily>> accountingPositionDailyMap = new HashMap<>();
	// Map of Client Account CCY Balances (Key is Client Account ID - Holding Account ID - Investment Group ID)
	Map<String, List<AccountingBalanceValue>> accountingBalanceValueMap = new HashMap<>();

	// Map of Security ID and optional duration field name to Duration Value
	Map<String, BigDecimal> securityDurationMap = new HashMap<>();

	// Map of Client Account ID to the Exchange Rate Data Source Name it Uses
	Map<Integer, String> clientAccountFxRateDataSourceNameMap = new HashMap<>();
	// Map of Exchange Rate Data Source Name_From CCY_To CCY Fx Rates
	Map<String, BigDecimal> dataSourceFxRateMap = new HashMap<>();

	// A list of all event manager adjustments on the balance date that aren't ignore or flagged as deleted - Not very many, so we get them all once and filter that list as needed
	List<InvestmentEventManagerAdjustment> eventManagerAdjustmentList;
	// A list of adjustments across all managers for a balance date that should be copied forward to the next day automatically - Not very many, so we get them all once and filter that list as needed
	List<InvestmentManagerAccountBalanceAdjustment> applyFromPreviousDayAdjustmentList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ProductManagerBalanceProcessingConfig(Date balanceDate, CalendarBusinessDayService calendarBusinessDayService) {
		this(balanceDate, calendarBusinessDayService, null, false);
	}


	public ProductManagerBalanceProcessingConfig(Date balanceDate, CalendarBusinessDayService calendarBusinessDayService, Map<Integer, PortfolioRun> clientAccountPortfolioRunMap, boolean doNotValidatePortfolioRunStatus) {
		this.balanceDate = balanceDate;
		this.previousBusinessDay = calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(balanceDate), -1);
		this.nextBusinessDay = calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(balanceDate), 1);
		this.clientAccountPortfolioRunMap = clientAccountPortfolioRunMap;
		this.doNotValidatePortfolioRunStatus = doNotValidatePortfolioRunStatus;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<ManagerPositionHistorySummary> getCustodianBalanceList(Integer custodianId) {
		return this.custodianBalanceMap.get(custodianId);
	}


	public void setCustodianBalanceList(Integer custodianId, List<ManagerPositionHistorySummary> managerPositionHistorySummaryList) {
		this.custodianBalanceMap.put(custodianId, managerPositionHistorySummaryList == null ? new ArrayList<>() : managerPositionHistorySummaryList);
	}

	////////////////////////////////////////////////////////////////////////////////


	public PortfolioRun getLinkedRunForManagerAccount(InvestmentManagerAccount managerAccount) {
		if (this.clientAccountPortfolioRunMap != null) {
			if (managerAccount.getLinkedInvestmentAccount() != null) {
				return this.clientAccountPortfolioRunMap.get(managerAccount.getLinkedInvestmentAccount().getId());
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////


	public List<AccountingPositionDaily> getAccountingPositionDailyList(Integer clientAccountId, Integer holdingAccountId, Short investmentGroupId, AccountingPositionDailyService accountingPositionDailyService) {
		String key = (clientAccountId == null ? "NULL" : clientAccountId) + "_" + (holdingAccountId == null ? "NULL" : holdingAccountId) + "_" + (investmentGroupId == null ? "NULL" : investmentGroupId);
		List<AccountingPositionDaily> positionList = this.accountingPositionDailyMap.get(key);
		if (positionList == null) {
			AccountingPositionDailyLiveSearchForm searchForm = new AccountingPositionDailyLiveSearchForm();
			searchForm.setClientAccountId(clientAccountId);
			searchForm.setHoldingAccountId(holdingAccountId);
			searchForm.setInvestmentGroupId(investmentGroupId);
			searchForm.setSnapshotDate(this.balanceDate);
			positionList = accountingPositionDailyService.getAccountingPositionDailyLiveList(searchForm);
			this.accountingPositionDailyMap.put(key, positionList);
		}
		return positionList;
	}

	////////////////////////////////////////////////////////////////////////////////


	public List<AccountingBalanceValue> getAccountingBalanceValueCurrencyList(Integer clientAccountId, Integer holdingAccountId, Short investmentGroupId, AccountingValuationService accountingValuationService) {
		String key = "CCY_" + (clientAccountId == null ? "NULL" : clientAccountId) + "_" + (holdingAccountId == null ? "NULL" : holdingAccountId) + "_" + (investmentGroupId == null ? "NULL" : investmentGroupId);
		List<AccountingBalanceValue> valueList = this.accountingBalanceValueMap.get(key);
		if (valueList == null) {
			AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onTransactionDate(this.balanceDate);
			searchForm.setClientInvestmentAccountId(clientAccountId);
			searchForm.setHoldingInvestmentAccountId(holdingAccountId);
			searchForm.setInvestmentGroupId(investmentGroupId);
			searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CURRENCY_NON_CASH_ACCOUNTS);
			valueList = accountingValuationService.getAccountingBalanceValueList(searchForm);
			this.accountingBalanceValueMap.put(key, valueList);
		}
		return valueList;
	}

	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getSecurityDuration(InvestmentSecurity security, MarketDataRetriever marketDataRetriever, String durationFieldNameOverride) {
		String key = String.valueOf(security.getId());
		if (!StringUtils.isEmpty(durationFieldNameOverride)) {
			key = key + durationFieldNameOverride;
		}
		BigDecimal duration = this.securityDurationMap.get(key);
		if (duration == null) {
			duration = marketDataRetriever.getDurationFlexible(security, this.balanceDate, durationFieldNameOverride, false);
			if (duration == null) {
				duration = BigDecimal.ONE;
			}
			this.securityDurationMap.put(key, duration);
		}
		return duration;
	}

	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getAccountingValueInManagerBaseCurrency(InvestmentManagerAccount managerAccount, InvestmentSecurity positionSecurity, BigDecimal localValue, BigDecimal baseValue, MarketDataExchangeRatesApiService apiService) {
		// If Client Account Base Currency Equals Manager Base Currency - Use Base Value
		InvestmentAccount clientAccount = managerAccount.getLinkedInvestmentAccount();
		if (managerAccount.getBaseCurrency().equals(clientAccount.getBaseCurrency())) {
			return baseValue;
		}

		// If Position is Denominated in Manager Base Currency - Use Local Value
		InvestmentSecurity positionDenomination = positionSecurity.getInstrument().getTradingCurrency();
		if (managerAccount.getBaseCurrency().equals(positionDenomination)) {
			return localValue;
		}

		// Otherwise - Need to Convert Value to Manager Base CCY - Convert Directly From Local
		return MathUtils.multiply(localValue, getFxRate(clientAccount, positionDenomination, managerAccount.getBaseCurrency(), apiService));
	}


	////////////////////////////////////////////////////////////////////////////////


	public String getFxRateDataSourceName(InvestmentAccount clientAccount, MarketDataExchangeRatesApiService apiService) {
		return this.clientAccountFxRateDataSourceNameMap.computeIfAbsent(clientAccount.getId(), k -> apiService.getExchangeRateDataSourceForClientAccount(clientAccount.toClientAccount()));
	}


	public BigDecimal getFxRate(InvestmentAccount clientAccount, InvestmentSecurity fromCCY, InvestmentSecurity toCCY, MarketDataExchangeRatesApiService apiService) {
		String dataSourceName = getFxRateDataSourceName(clientAccount, apiService);
		return this.dataSourceFxRateMap.computeIfAbsent(dataSourceName + "_" + fromCCY.getId() + "_" + toCCY.getId(), k ->
				apiService.getExchangeRate(FxRateLookupCommand.forDataSource(dataSourceName, fromCCY.getSymbol(), toCCY.getSymbol(), this.balanceDate)));
	}


	////////////////////////////////////////////////////////////////////////////////


	public List<InvestmentEventManagerAdjustment> getInvestmentEventManagerAdjustmentList(InvestmentManagerAccount managerAccount, InvestmentCalendarService investmentCalendarService) {
		if (this.eventManagerAdjustmentList == null) {
			InvestmentEventManagerAdjustmentSearchForm searchForm = new InvestmentEventManagerAdjustmentSearchForm();
			// Look up events that are on the next day after the balance date since today we are processing balances for previous business day
			searchForm.setEventDate(this.nextBusinessDay);
			searchForm.setIgnored(false);
			searchForm.setDeletedEvent(false);
			this.eventManagerAdjustmentList = investmentCalendarService.getInvestmentEventManagerAdjustmentList(searchForm);
		}
		return BeanUtils.filter(this.eventManagerAdjustmentList, InvestmentEventManagerAdjustment::getManagerAccount, managerAccount);
	}


	////////////////////////////////////////////////////////////////////////////////


	public List<InvestmentManagerAccountBalanceAdjustment> getInvestmentManagerPreviousDayAdjustmentList(InvestmentManagerAccount managerAccount, InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		if (this.applyFromPreviousDayAdjustmentList == null) {
			this.applyFromPreviousDayAdjustmentList = investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentCopyNextDayList(this.previousBusinessDay);
		}
		return BeanUtils.filter(this.applyFromPreviousDayAdjustmentList, adjustment -> adjustment.getManagerAccountBalance().getManagerAccount(), managerAccount);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////               Getter and Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public boolean isDoNotValidatePortfolioRunStatus() {
		return this.doNotValidatePortfolioRunStatus;
	}
}
