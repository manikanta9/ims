package com.clifton.product.manager.linkedcalculators.dependent;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.manager.linkedcalculators.BaseProductLinkedManagerCalculatorImpl;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayService;

import java.util.List;


/**
 * The <code>ProductManagerAccountBalanceSyntheticExposure</code> ...
 * <p>
 * ACCOUNT_SYNTHETIC_EXPOSURE, // Securities Only: comes from Overlay Exposure (our overlay positions)
 *
 * @author manderson
 */
public class ProductLinkedManagerForReplicationExposureCalculator extends BaseProductLinkedManagerCalculatorImpl implements ProductLinkedManagerDependentCalculator {

	private ProductOverlayService productOverlayService;

	/**
	 * i.e. Synthetic Exposure option would sum: actualExposureAdjusted
	 * Overlay Target would sum: targetExposureAdjusted
	 */
	private String exposureFieldName;


	@Override
	public void calculate(InvestmentManagerAccountBalance balance, PortfolioRun linkedRun, boolean doNotValidateLinkedRunStatus) {
		if (linkedRun == null) {
			return; // No Linked Run
		}
		validateLinkedRunStatus(balance, linkedRun, doNotValidateLinkedRunStatus);

		List<ProductOverlayAssetClassReplication> replicationList = getProductOverlayService().getProductOverlayAssetClassReplicationListByRun(linkedRun.getId());

		// If just one asset class, then just pull replications tied directly to that asset class
		Integer assetClassId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_ASSET_CLASS_FIELD);
		if (assetClassId != null) {
			replicationList = BeanUtils.filter(replicationList, replication -> replication.getOverlayAssetClass().getAccountAssetClass().getId(), assetClassId);
		}
		// Otherwise filter out rollups to avoid double counts
		else {
			replicationList = BeanUtils.filter(replicationList, replication -> replication.getOverlayAssetClass().isRollupAssetClass(), false);
		}

		// Security Level Filters - (note: not available on all types, but can easily be added via UI and will be supported automatically)
		Integer securityId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_SECURITY_FIELD);
		if (securityId != null) {
			replicationList = BeanUtils.filter(replicationList, replication -> replication.getSecurity().getId(), securityId);
		}
		else {
			Integer instrumentId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_INSTRUMENT_FIELD);
			if (instrumentId != null) {
				replicationList = BeanUtils.filter(replicationList, replication -> replication.getSecurity().getInstrument().getId(), instrumentId);
			}
			else {
				Integer investmentGroupId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_GROUP_FIELD);
				if (investmentGroupId != null) {
					// Use Cache Method
					InvestmentGroup group = getInvestmentGroupService().getInvestmentGroup(investmentGroupId.shortValue());
					replicationList = BeanUtils.filter(replicationList, replication -> getInvestmentGroupService().isInvestmentSecurityInGroup(group.getName(), replication.getSecurity().getId()), true);
				}
			}
			Integer securityGroupId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_SECURITY_GROUP_FIELD);
			if (securityGroupId != null) {
				replicationList = BeanUtils.filter(replicationList, replication -> getInvestmentSecurityGroupService().isInvestmentSecurityInSecurityGroup(replication.getSecurity().getId(), securityGroupId.shortValue()), true);
			}
		}
		balance.setSecuritiesValue(CoreMathUtils.sumPropertyName(replicationList, getExposureFieldName())); //"actualExposureAdjusted"
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}


	public String getExposureFieldName() {
		return this.exposureFieldName;
	}


	public void setExposureFieldName(String exposureFieldName) {
		this.exposureFieldName = exposureFieldName;
	}
}
