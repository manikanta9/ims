package com.clifton.product.manager.linkedcalculators;


import com.clifton.investment.manager.LinkedManagerTypes;
import com.clifton.product.manager.linkedcalculators.dependent.ProductLinkedManagerDependentCalculator;
import com.clifton.product.manager.linkedcalculators.independent.ProductLinkedManagerIndependentCalculator;


/**
 * The <code>ProductManagerAccountBalanceLinkedCalculatorLocator</code> class finds and returns ProductManagerAccountBalanceLinkedCalculator
 * that corresponds to a LinkedManagerType.
 *
 * @author manderson
 */
public interface ProductLinkedManagerCalculatorLocator {

	/**
	 * Returns ProductLinkedManagerDependentCalculator for the specified linked manager type
	 * Validates passed linkedManagerType.isProcessingDependent = true to ensure casting
	 */
	public ProductLinkedManagerDependentCalculator locateDependent(LinkedManagerTypes linkedManagerType);


	/**
	 * Returns ProductLinkedManagerDependentCalculator for the specified linked manager type
	 * Validates passed linkedManagerType.isProcessingDependent = true to ensure casting
	 */
	public ProductLinkedManagerIndependentCalculator locateIndependent(LinkedManagerTypes linkedManagerType);
}
