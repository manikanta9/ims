package com.clifton.product.manager.linkedcalculators;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.LinkedManagerTypes;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.hedging.ProductHedgingAccountStrategyPosition;
import com.clifton.product.hedging.ProductHedgingService;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.util.ProductUtilService;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>BaseProductManagerAccountBalanceLinkedCalculatorImpl</code> is an abstract
 * class that can be extended by implementing classes of {@link ProductLinkedManagerCalculator}
 * that provide reusable methods so all common code can re-use the same logic
 *
 * @author manderson
 */
public abstract class BaseProductLinkedManagerCalculatorImpl implements ProductLinkedManagerCalculator {

	////////////////////////////////////////////////////////////////////////////////
	////////////         Custom Fields Used For Linked Managers         ////////////
	////////////////////////////////////////////////////////////////////////////////

	public static final String LINKED_MANAGER_ASSET_CLASS_FIELD = "Linked Account Asset Class";
	public static final String LINKED_MANAGER_MANAGER_ACCOUNT_FIELD = "Linked Manager Account Assignment";
	public static final String LINKED_MANAGER_HEDGING_STRATEGY_FIELD = "Hedging Strategy";
	public static final String LINKED_MANAGER_HOLDING_ACCOUNT_FIELD = "Linked Holding Account";
	public static final String LINKED_MANAGER_REPO_ACCOUNT_FIELD = "Linked REPO Account";
	public static final String LINKED_MANAGER_OPTIONS_EXCLUDE_DELTA = "Exclude Delta";
	public static final String LINKED_MANAGER_CASH_COLLATERAL_OPTION = "Cash Collateral Options";
	public static final String LINKED_MANAGER_CASH_COLLATERAL_OPTION_EXCLUDE = "EXCLUDE";
	public static final String LINKED_MANAGER_CASH_COLLATERAL_OPTION_INCLUDE = "INCLUDE";
	public static final String LINKED_MANAGER_CASH_COLLATERAL_OPTION_INCLUDE_CLIENT_ACCOUNT = "INCLUDE_CLIENT_ACCOUNT";
	public static final String LINKED_MANAGER_START_DATE_FIELD = "Start Date";
	public static final String LINKED_MANAGER_CURRENCY_DENOMINATION_FIELD = "CCY Denomination";
	public static final String LINKED_MANAGER_INVESTMENT_TYPE_FIELD = "Investment Type";
	public static final String LINKED_MANAGER_INVESTMENT_SUB_TYPE_FIELD = "Investment Sub Type";
	public static final String LINKED_MANAGER_INVESTMENT_SUB_TYPE_2_FIELD = "Investment Sub Type 2";
	public static final String LINKED_MANAGER_INVESTMENT_HIERARCHY_FIELD = "Investment Hierarchy";
	public static final String LINKED_MANAGER_INVESTMENT_GROUP_FIELD = "Investment Group";
	public static final String LINKED_MANAGER_INVESTMENT_SECURITY_GROUP_FIELD = "Security Group";
	public static final String LINKED_MANAGER_INVESTMENT_INSTRUMENT_FIELD = "Investment Instrument";
	public static final String LINKED_MANAGER_INVESTMENT_SECURITY_FIELD = "Investment Security";
	public static final String LINKED_MANAGER_UNDERLYING_INSTRUMENT_FIELD = "Underlying Instrument";
	public static final String LINKED_MANAGER_REPO_TYPE = "Lending Repo Type";
	public static final String LINKED_MANAGER_FUND_ACCOUNT = "Linked Fund Account";
	public static final String LINKED_MANAGER_ACCOUNTING_ACCOUNT = "Linked Accounting Account";
	public static final String LINKED_MANAGER_ACCOUNTING_ACCOUNT_TYPE = "Linked Accounting Account Type";
	public static final String LINKED_MANAGER_ACCOUNTING_ACCOUNT_GROUP = "Linked Accounting Account Group";
	public static final String LINKED_MANAGER_ACCOUNTING_EVENT_JOURNAL_TYPE = "Linked Accounting Event Journal Type";
	public static final String LINKED_MANAGER_ACCOUNTING_SIMPLE_JOURNAL_TYPE = "Linked Accounting Simple Journal Type";


	////////////////////////////////////////////////////////////////////////////////

	private LinkedManagerTypes linkedManagerType;

	private AccountingValuationService accountingValuationService;
	private AccountingPositionDailyService accountingPositionDailyService;

	private InvestmentGroupService investmentGroupService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private MarketDataRetriever marketDataRetriever;

	private ProductHedgingService productHedgingService;
	private ProductUtilService productUtilService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentManagerAccount getInvestmentManagerAccountDependency(@SuppressWarnings("unused") InvestmentManagerAccount manager) {
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////               Accounting Position Daily Methods              /////////
	////////////////////////////////////////////////////////////////////////////////


	protected List<AccountingPositionDaily> getAccountingPositionDailyList(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		return getAccountingPositionDailyList(balance, null, managerBalanceProcessingConfig);
	}


	protected List<AccountingPositionDaily> getAccountingPositionDailyList(InvestmentManagerAccountBalance balance, String investmentTypeOverride, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		Integer clientAccountId = balance.getManagerAccount().getLinkedInvestmentAccount().getId();
		Integer holdingAccountId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_HOLDING_ACCOUNT_FIELD);

		// Filter on Group ID within SQL - much faster
		Integer groupId = (Integer) getProductUtilService().getInvestmentManagerAccountLinkedManagerCustomFieldValue(balance.getManagerAccount(), LINKED_MANAGER_INVESTMENT_GROUP_FIELD);
		return getAccountingPositionDailyListImpl(balance, clientAccountId, holdingAccountId, (groupId == null ? null : groupId.shortValue()), investmentTypeOverride, managerBalanceProcessingConfig);
	}


	private List<AccountingPositionDaily> getAccountingPositionDailyListImpl(InvestmentManagerAccountBalance balance, Integer clientAccountId, Integer holdingAccountId, Short groupId,
	                                                                         String investmentTypeOverride, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {

		List<AccountingPositionDaily> positionList = managerBalanceProcessingConfig.getAccountingPositionDailyList(clientAccountId, holdingAccountId, groupId, getAccountingPositionDailyService());

		// Now filter on what this manager specifically needs
		Integer hedgingStrategy = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_HEDGING_STRATEGY_FIELD);
		if (hedgingStrategy != null) {
			List<ProductHedgingAccountStrategyPosition> strategyPositionList = getProductHedgingService().getProductHedgingAccountStrategyPositionListByStrategy(hedgingStrategy);
			AccountingTransaction[] strategyPositionTransactions = BeanUtils.getPropertyValuesUniqueExcludeNull(strategyPositionList, ProductHedgingAccountStrategyPosition::getAccountingTransaction, AccountingTransaction.class);
			positionList = BeanUtils.filter(positionList, accountingPositionDaily -> ArrayUtils.contains(strategyPositionTransactions, accountingPositionDaily.getAccountingTransaction()));
		}

		if (!StringUtils.isEmpty(investmentTypeOverride)) {
			positionList = BeanUtils.filter(positionList, position -> position.getAccountingTransaction().getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName(), investmentTypeOverride);
		}

		Integer instrumentId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_INSTRUMENT_FIELD);
		if (instrumentId != null) {
			positionList = BeanUtils.filter(positionList, position -> position.getAccountingTransaction().getInvestmentSecurity().getInstrument().getId(), instrumentId);
		}
		// Only Filter on  hierarchy if no instrument, instrument is more explicit
		else {
			Integer hierarchyId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_HIERARCHY_FIELD);
			if (hierarchyId != null) {
				positionList = BeanUtils.filter(positionList, position -> position.getAccountingTransaction().getInvestmentSecurity().getInstrument().getHierarchy().getId(), hierarchyId.shortValue());
			}
			// Only Filter on type if no hierarchy, hierarchy is more explicit
			else {
				// If sub type or subtype 2 is selected - check those, else type
				boolean subTypeSelected = false;
				Integer subTypeId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_SUB_TYPE_FIELD);
				if (subTypeId != null) {
					subTypeSelected = true;
					positionList = BeanUtils.filter(positionList, position -> position.getAccountingTransaction().getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentTypeSubType().getId(), subTypeId.shortValue());
				}
				Integer subType2Id = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_SUB_TYPE_2_FIELD);
				if (subType2Id != null) {
					subTypeSelected = true;
					positionList = BeanUtils.filter(positionList, position -> position.getAccountingTransaction().getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentTypeSubType2().getId(), subType2Id.shortValue());
				}
				if (!subTypeSelected) {
					Integer typeId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_TYPE_FIELD);
					if (typeId != null) {
						positionList = BeanUtils.filter(positionList, position -> position.getAccountingTransaction().getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getId(), typeId.shortValue());
					}
				}
			}
			Integer underlyingInstrumentId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_UNDERLYING_INSTRUMENT_FIELD);
			if (underlyingInstrumentId != null) {
				positionList = BeanUtils.filter(positionList, position -> position.getAccountingTransaction().getInvestmentSecurity().getInstrument().getUnderlyingInstrument().getId(), underlyingInstrumentId);
			}
		}

		Integer ccyDenomination = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_CURRENCY_DENOMINATION_FIELD);
		if (ccyDenomination != null) {
			positionList = BeanUtils.filter(positionList, position -> position.getAccountingTransaction().getInvestmentSecurity().getInstrument().getTradingCurrency().getId(), ccyDenomination);
		}

		Integer securityGroupId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_SECURITY_GROUP_FIELD);
		if (securityGroupId != null) {
			List<AccountingPositionDaily> filteredList = new ArrayList<>();
			for (AccountingPositionDaily position : CollectionUtils.getIterable(positionList)) {
				if (getInvestmentSecurityGroupService().isInvestmentSecurityInSecurityGroup(position.getAccountingTransaction().getInvestmentSecurity().getId(), securityGroupId.shortValue())) {
					filteredList.add(position);
				}
			}
			positionList = filteredList;
		}

		return positionList;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////              Accounting Balance Value CCY  Methods           /////////
	////////////////////////////////////////////////////////////////////////////////


	protected List<AccountingBalanceValue> getAccountingBalanceValueCurrencyList(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		return getAccountingBalanceValueCurrencyList(balance, null, managerBalanceProcessingConfig);
	}


	private List<AccountingBalanceValue> getAccountingBalanceValueCurrencyList(InvestmentManagerAccountBalance balance, String investmentTypeOverride,
	                                                                           ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		Integer clientAccountId = balance.getManagerAccount().getLinkedInvestmentAccount().getId();
		Integer holdingAccountId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_HOLDING_ACCOUNT_FIELD);

		// Filter on Group ID within SQL - much faster
		Integer groupId = (Integer) getProductUtilService().getInvestmentManagerAccountLinkedManagerCustomFieldValue(balance.getManagerAccount(), LINKED_MANAGER_INVESTMENT_GROUP_FIELD);
		return getAccountingBalanceValueCurrencyListImpl(balance, clientAccountId, holdingAccountId, (groupId == null ? null : groupId.shortValue()), investmentTypeOverride, managerBalanceProcessingConfig);
	}


	private List<AccountingBalanceValue> getAccountingBalanceValueCurrencyListImpl(InvestmentManagerAccountBalance balance, Integer clientAccountId, Integer holdingAccountId, Short groupId,
	                                                                               String investmentTypeOverride, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {

		List<AccountingBalanceValue> valueList = managerBalanceProcessingConfig.getAccountingBalanceValueCurrencyList(clientAccountId, holdingAccountId, groupId, getAccountingValuationService());

		if (!StringUtils.isEmpty(investmentTypeOverride)) {
			valueList = BeanUtils.filter(valueList, accountingBalanceValue -> accountingBalanceValue.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName(), investmentTypeOverride);
		}
		else {
			Integer instrumentId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_INSTRUMENT_FIELD);
			if (instrumentId != null) {
				valueList = BeanUtils.filter(valueList, accountingBalanceValue -> accountingBalanceValue.getInvestmentSecurity().getInstrument().getId(), instrumentId);
			}
			// Only Filter on  hierarchy if no instrument, instrument is more explicit
			else {
				Integer hierarchyId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_HIERARCHY_FIELD);
				if (hierarchyId != null) {
					valueList = BeanUtils.filter(valueList, accountingBalanceValue -> accountingBalanceValue.getInvestmentSecurity().getInstrument().getHierarchy().getId(), hierarchyId.shortValue());
				}
				// Only Filter on type if no hierarchy, hierarchy is more explicit
				else {
					// If sub type or subtype 2 is selected - check those, else type
					boolean subTypeSelected = false;
					Integer subTypeId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_SUB_TYPE_FIELD);
					if (subTypeId != null) {
						subTypeSelected = true;
						valueList = BeanUtils.filter(valueList, accountingBalanceValue -> InvestmentUtils.isSecurityOfTypeSubType(accountingBalanceValue.getInvestmentSecurity(), subTypeId.shortValue()));
					}
					Integer subType2Id = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_SUB_TYPE_2_FIELD);
					if (subType2Id != null) {
						subTypeSelected = true;
						valueList = BeanUtils.filter(valueList, accountingBalanceValue -> InvestmentUtils.isSecurityOfTypeSubType2(accountingBalanceValue.getInvestmentSecurity(), subType2Id.shortValue()));
					}
					if (!subTypeSelected) {
						Integer typeId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_TYPE_FIELD);
						if (typeId != null) {
							valueList = BeanUtils.filter(valueList, accountingBalanceValue -> accountingBalanceValue.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getId(), typeId.shortValue());
						}
					}
				}
			}
		}

		return valueList;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////                       Helper Methods                         /////////
	////////////////////////////////////////////////////////////////////////////////


	protected Object getLinkedManagerCustomFieldValue(InvestmentManagerAccountBalance balance, String fieldName) {
		return getProductUtilService().getInvestmentManagerAccountLinkedManagerCustomFieldValue(balance.getManagerAccount(), fieldName);
	}


	/**
	 * Validates that the linked run is in a processed state and values can be pulled from it
	 */
	protected void validateLinkedRunStatus(InvestmentManagerAccountBalance balance, PortfolioRun linkedPortfolioRun, boolean doNotValidateLinkedRunStatus) {
		if (!doNotValidateLinkedRunStatus && linkedPortfolioRun.isIncomplete()) {
			throw generateLinkedManagerLoadException(balance, "InvestmentAccount", linkedPortfolioRun.getClientInvestmentAccount().getId(), "Dependent run [" + linkedPortfolioRun.getLabel()
					+ "] is in Workflow State [" + linkedPortfolioRun.getWorkflowState().getName() + "] and was unable to complete processing.");
		}
	}


	protected ValidationExceptionWithCause generateLinkedManagerLoadException(InvestmentManagerAccountBalance balance, String causeTableName, Integer causeFkFieldId, String message) {
		return new ValidationExceptionWithCause(causeTableName, causeFkFieldId, "Unable to load linked manager balance for [" + balance.getManagerAccount().getLabel() + "] of linked type ["
				+ getLinkedManagerType() + "]: " + message);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////                  Getter and Setter Methods                   /////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public LinkedManagerTypes getLinkedManagerType() {
		return this.linkedManagerType;
	}


	public void setLinkedManagerType(LinkedManagerTypes linkedManagerType) {
		this.linkedManagerType = linkedManagerType;
	}


	public ProductUtilService getProductUtilService() {
		return this.productUtilService;
	}


	public void setProductUtilService(ProductUtilService productUtilService) {
		this.productUtilService = productUtilService;
	}


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public ProductHedgingService getProductHedgingService() {
		return this.productHedgingService;
	}


	public void setProductHedgingService(ProductHedgingService productHedgingService) {
		this.productHedgingService = productHedgingService;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}
}
