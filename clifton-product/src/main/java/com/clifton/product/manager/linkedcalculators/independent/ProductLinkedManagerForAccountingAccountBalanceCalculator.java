package com.clifton.product.manager.linkedcalculators.independent;


import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingAccountBalanceSearchForm;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.manager.linkedcalculators.BaseProductLinkedManagerCalculatorImpl;


/**
 * The <code>ProductManagerAccountBalanceAccountingAccountBalanceCalculator</code> ...
 * <p>
 * GENERAL_LEDGER_BALANCE; // 'This manager\'s balance (Cash Only) comes from the General Ledger account balance held in the specified client account and broker account (optional) for a specific GL account/account type/account group.'
 *
 * @author manderson
 */
public class ProductLinkedManagerForAccountingAccountBalanceCalculator extends BaseProductLinkedManagerCalculatorImpl implements ProductLinkedManagerIndependentCalculator {

	private AccountingBalanceService accountingBalanceService;

	/**
	 * Option to apply different filters based on Account Balance or Pending Account Balance
	 */
	private boolean pending;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void calculate(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		AccountingAccountBalanceSearchForm searchForm = new AccountingAccountBalanceSearchForm();

		// Common Filters
		searchForm.setClientAccountId(balance.getManagerAccount().getLinkedInvestmentAccount().getId());
		searchForm.setHoldingAccountId((Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_HOLDING_ACCOUNT_FIELD));
		Integer accountingAccountId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_ACCOUNTING_ACCOUNT);
		searchForm.setAccountingAccountId(accountingAccountId == null ? null : accountingAccountId.shortValue());
		Integer currencyId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_SECURITY_FIELD);
		searchForm.setSecurityId(currencyId);

		// Pending Balance Filters
		if (isPending()) {
			searchForm.setTransactionDate(balance.getBalanceDate());
			// Filter is Greater than or equals, so move to next day
			searchForm.setStartSettlementDate(DateUtils.addDays(balance.getBalanceDate(), 1));

			Integer simpleJournalTypeId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_ACCOUNTING_SIMPLE_JOURNAL_TYPE);
			searchForm.setSimpleJournalTypeId(simpleJournalTypeId == null ? null : simpleJournalTypeId.shortValue());
		}
		else {
			searchForm.setTransactionDate(balance.getBalanceDate());

			Integer accountingAccountTypeId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_ACCOUNTING_ACCOUNT_TYPE);
			searchForm.setAccountingAccountTypeId(accountingAccountTypeId == null ? null : accountingAccountTypeId.shortValue());
			Integer accountingAccountGroupId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_ACCOUNTING_ACCOUNT_GROUP);
			searchForm.setAccountingAccountGroupId(accountingAccountGroupId == null ? null : accountingAccountGroupId.shortValue());
		}

		// Put Everything into cash
		balance.setCashValue(getAccountingBalanceService().getAccountingAccountBalanceBase(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public boolean isPending() {
		return this.pending;
	}


	public void setPending(boolean pending) {
		this.pending = pending;
	}
}
