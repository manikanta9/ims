package com.clifton.product.manager.run.calculators;


import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.process.PortfolioRunConfig;
import com.clifton.portfolio.run.process.PortfolioRunProcessService;
import com.clifton.portfolio.run.process.calculator.BasePortfolioRunProcessStepCalculatorImpl;
import com.clifton.portfolio.run.search.PortfolioRunSearchForm;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.manager.ProductManagerBalanceProcessingTypes;
import com.clifton.product.manager.ProductManagerService;
import com.clifton.product.manager.job.AccountBalanceAdjustmentsCommand;
import com.clifton.product.manager.job.AccountBalanceCommand;
import com.clifton.rule.violation.RuleViolationUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>PortfolioRunManagerBalanceCalculator</code> is a generic calculator that can
 * be used by any portfolio run to cause proxy managers associated with the client of the run's client account
 * to be reloaded.
 * <p>
 * For this class, preProcess indicates if this step loads manager balances BEFORE processing the run or AFTER (After should still be done before post process rules are done).
 * * Before will attempt to load/re-process linked manager balances, optionally re-process proxy managers, and attempt to load any missing balances the run may need.
 * * After will load linked manager balances that depend on THIS run but aren't actually assigned anywhere
 *
 * @author manderson
 */
public class PortfolioRunManagerBalanceCalculator extends BasePortfolioRunProcessStepCalculatorImpl<PortfolioRunConfig> {

	private CalendarBusinessDayService calendarBusinessDayService;

	private PortfolioRunProcessService portfolioRunProcessService;

	private ProductManagerService productManagerService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	@Override
	protected void processStep(PortfolioRunConfig runConfig) {
		// Loads/Re-Processes necessary linked manager account balances (handled different for pre/post processing)
		loadLinkedManagerAccountBalanceList(runConfig);
		if (runConfig.isPreProcess()) {
			// PRODUCT-279 Reload Proxy Manager Balances If Changes
			if (runConfig.isReloadProxyManagers()) {
				reloadProxyManagerAccountBalanceList(runConfig.getRun());
			}
			// ATTEMPT TO LOAD ANY MISSING MANAGER BALANCES AND PROCESS BALANCES THAT ARE UNPROCESSED
			processManagerAccountBalanceList(runConfig);
		}
	}


	@SuppressWarnings("unused")
	@Override
	protected void saveStep(PortfolioRunConfig runConfig) {
		// NOTHING TO SAVE ON THE RUN ITSELF - DONE IN MANAGER RE-PROCESSING
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////            Proxy Manager Loading Business Methods            /////////
	////////////////////////////////////////////////////////////////////////////////


	private void reloadProxyManagerAccountBalanceList(PortfolioRun run) {
		// First Get a List of All Proxy Managers for the Client
		InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();
		searchForm.setClientIdOrRelatedClient(run.getClientInvestmentAccount().getBusinessClient().getId());
		searchForm.setProxyManager(true);
		searchForm.setInactive(false);
		// Only reload if the proxy value date is on or before the run balance date
		// This way if re-processing a historical run, we don't want to reload manager balance using current proxy value vs. what was there on that date
		searchForm.addSearchRestriction(new SearchRestriction("proxyValueDate", ComparisonConditions.LESS_THAN_OR_EQUALS, run.getBalanceDate()));
		getProductManagerService().processProductManagerAccountBalanceList(AccountBalanceCommand.ofSynchronous(searchForm, run.getBalanceDate(), ProductManagerBalanceProcessingTypes.RELOAD_PROCESS));
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////          Linked Manager Loading Business Methods            //////////
	////////////////////////////////////////////////////////////////////////////////


	private void loadLinkedManagerAccountBalanceList(PortfolioRunConfig runConfig) {
		PortfolioRun run = runConfig.getRun();

		List<InvestmentManagerAccount> linkedManagerList = runConfig.getClientLinkedManagerList(getInvestmentManagerAccountService());

		if (runConfig.isPreProcess()) {
			// First Map out the Linked Client Account and Assigned Accounts to confirm no circular dependency
			Set<String> assignedToLinkedAccounts = new HashSet<>();
			for (InvestmentManagerAccount manager : CollectionUtils.getIterable(linkedManagerList)) {
				if (manager.isLinkedManagerProcessingDependent()) {
					List<InvestmentManagerAccountAssignment> assignmentList = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentListByManager(manager.getId(), true);
					for (InvestmentManagerAccountAssignment assign : CollectionUtils.getIterable(assignmentList)) {
						if (!assign.isInactive() && (assign.getReferenceTwo().equals(run.getClientInvestmentAccount()) || manager.getLinkedInvestmentAccount().equals(run.getClientInvestmentAccount()))) {
							String key = assign.getReferenceTwo().getId() + "_" + manager.getLinkedInvestmentAccount().getId();
							String reverseKey = manager.getLinkedInvestmentAccount().getId() + "_" + assign.getReferenceTwo().getId();
							assignedToLinkedAccounts.add(key);
							if (assignedToLinkedAccounts.contains(reverseKey)) {
								throw new ValidationException("Cannot process run.  There appears to be a circular dependency between Linked Managers and the manager assignments for the following 2 client accounts: " + assign.getReferenceTwo().getLabel() + ", " + manager.getLinkedInvestmentAccount().getLabel());
							}
						}
					}
				}
			}


			// Then Get a List of All Linked Managers for the Client that Depend on a Linked Run for an account other than this account...
			Map<Integer, PortfolioRun> clientAccountLinkedRunMap = new HashMap<>();
			for (InvestmentManagerAccount manager : CollectionUtils.getIterable(linkedManagerList)) {
				boolean load = false;
				if (manager.isLinkedManagerProcessingDependent() && !run.getClientInvestmentAccount().equals(manager.getLinkedInvestmentAccount())) {
					List<InvestmentManagerAccountAssignment> assignmentList = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentListByManager(manager.getId(), true);
					if (CollectionUtils.isEmpty(assignmentList)) {
						load = true;
					}
					for (InvestmentManagerAccountAssignment assign : CollectionUtils.getIterable(assignmentList)) {
						if (!assign.isInactive() && assign.getReferenceTwo().equals(run.getClientInvestmentAccount())) {
							load = true;
							break;
						}
					}
					if (load) {
						if (!clientAccountLinkedRunMap.containsKey(manager.getLinkedInvestmentAccount().getId())) {
							clientAccountLinkedRunMap.put(manager.getLinkedInvestmentAccount().getId(), getLinkedManagerProductOverlayRun(manager, run, runConfig));
						}
					}
				}
			}
			getProductManagerService().processProductLinkedManagerAccountListForInvestmentAccount(run.getClientInvestmentAccount().getId(), run.getBalanceDate(), clientAccountLinkedRunMap);
		}
		else {
			List<InvestmentManagerAccount> processManagerAccountList = new ArrayList<>();
			for (InvestmentManagerAccount manager : CollectionUtils.getIterable(linkedManagerList)) {
				// If Manager Account Balance Depends on THIS run
				if (manager.isLinkedManagerProcessingDependent() && run.getClientInvestmentAccount().equals(manager.getLinkedInvestmentAccount())) {
					List<InvestmentManagerAccountAssignment> assignmentList = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentListByManager(manager.getId(), true);
					// And has no assignments
					if (CollectionUtils.isEmpty(assignmentList)) {
						processManagerAccountList.add(manager);
					}
				}
			}
			if (!CollectionUtils.isEmpty(processManagerAccountList)) {
				Map<Integer, PortfolioRun> clientAccountLinkedRunMap = new HashMap<>();
				clientAccountLinkedRunMap.put(runConfig.getClientInvestmentAccountId(), run);
				ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig = new ProductManagerBalanceProcessingConfig(runConfig.getBalanceDate(), getCalendarBusinessDayService(), clientAccountLinkedRunMap, true);
				for (InvestmentManagerAccount managerAccount : CollectionUtils.getIterable(processManagerAccountList)) {
					getProductManagerService().processProductManagerAccountBalance(managerAccount, ProductManagerBalanceProcessingTypes.RELOAD_CHANGES_PROCESS, managerBalanceProcessingConfig);
				}
			}
		}
	}


	private PortfolioRun getLinkedManagerProductOverlayRun(InvestmentManagerAccount manager, PortfolioRun run, PortfolioRunConfig runConfig) {
		if (!manager.isLinkedManagerProcessingDependent()) {
			return null;
		}

		// First make sure there isn't a circular dependency between the run and the manager
		if (run.getClientInvestmentAccount().equals((manager.getLinkedInvestmentAccount()))) {
			throw new ValidationExceptionWithCause(
					"InvestmentManagerAccount",
					manager.getId(),
					"Unable to process run for account ["
							+ run.getClientInvestmentAccount().getLabel()
							+ "] because linked manager ["
							+ manager.getLabel()
							+ "] is linked to account ["
							+ run.getClientInvestmentAccount().getLabel()
							+ "] however the manager itself is also assigned to this account. Managers cannot be assigned to the same account they use for Linked Manager Accounts when a dependent PIOS run is required.");
		}

		// Look up the run we need
		PortfolioRunSearchForm searchForm = new PortfolioRunSearchForm();
		searchForm.setClientInvestmentAccountId(manager.getLinkedInvestmentAccount().getId());
		searchForm.setBalanceDate(run.getBalanceDate());
		if (run.isMarketOnCloseAdjustmentsIncluded()) {
			searchForm.setMarketOnCloseAdjustmentsIncluded(true);
		}
		else {
			searchForm.setMainRun(true);
		}

		PortfolioRun linkedRun = CollectionUtils.getFirstElement(getPortfolioRunService().getPortfolioRunList(searchForm));

		if (linkedRun == null) {
			linkedRun = new PortfolioRun();
			linkedRun.setClientInvestmentAccount(manager.getLinkedInvestmentAccount());
			linkedRun.setBalanceDate(run.getBalanceDate());
			linkedRun.setMarketOnCloseAdjustmentsIncluded(run.isMarketOnCloseAdjustmentsIncluded());
			linkedRun.setMainRun(run.isMainRun());
			getPortfolioRunService().savePortfolioRun(linkedRun);
		}
		try {
			if (!RuleViolationUtil.isRuleViolationAwareProcessed(linkedRun)) {
				getPortfolioRunProcessService().processPortfolioRun(linkedRun.getId(), false, runConfig.getCurrentDepth());
			}
		}
		catch (ValidationException e) {
			// Don't do anything exception was created as warning for the run
		}
		return getPortfolioRunService().getPortfolioRun(linkedRun.getId());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////          Manager Balance Adjustment Processing Methods         ////////
	////////////////////////////////////////////////////////////////////////////////


	private void processManagerAccountBalanceList(PortfolioRunConfig runConfig) {
		InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();
		searchForm.setClientId(runConfig.getRun().getClientInvestmentAccount().getBusinessClient().getId());
		getProductManagerService().processProductManagerAccountBalanceAdjustmentList(AccountBalanceAdjustmentsCommand.ofSynchronous(searchForm, runConfig.getRun().getBalanceDate(), false));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public ProductManagerService getProductManagerService() {
		return this.productManagerService;
	}


	public void setProductManagerService(ProductManagerService productManagerService) {
		this.productManagerService = productManagerService;
	}


	public PortfolioRunProcessService getPortfolioRunProcessService() {
		return this.portfolioRunProcessService;
	}


	public void setPortfolioRunProcessService(PortfolioRunProcessService portfolioRunProcessService) {
		this.portfolioRunProcessService = portfolioRunProcessService;
	}
}
