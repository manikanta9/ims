package com.clifton.product.manager.linkedcalculators;


import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.manager.LinkedManagerTypes;
import com.clifton.product.manager.linkedcalculators.dependent.ProductLinkedManagerDependentCalculator;
import com.clifton.product.manager.linkedcalculators.independent.ProductLinkedManagerIndependentCalculator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>ProductManagerAccountBalanceLinkedCalculatorLocatorInSpringContext</code> class locates ProductManagerAccountBalanceLinkedCalculator implementations for corresponding LinkedManagerTypes.
 * It auto-discovers all application context beans that implement ProductManagerAccountBalanceLinkedCalculator interface and registers them so that they can be located.
 *
 * @author manderson
 */
public class ProductLinkedManagerCalculatorLocatorInSpringContext implements ProductLinkedManagerCalculatorLocator, InitializingBean, ApplicationContextAware {

	private final Map<LinkedManagerTypes, ProductLinkedManagerCalculator> calculatorMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		Map<String, ProductLinkedManagerCalculator> beanMap = getApplicationContext().getBeansOfType(ProductLinkedManagerCalculator.class);

		// need a map with ProductManagerAccountBalanceLinkedCalculator names as keys instead of bean names
		for (Map.Entry<String, ProductLinkedManagerCalculator> stringProductLinkedManagerCalculatorEntry : beanMap.entrySet()) {
			ProductLinkedManagerCalculator calculator = stringProductLinkedManagerCalculatorEntry.getValue();
			if (getCalculatorMap().containsKey(calculator.getLinkedManagerType())) {
				throw new RuntimeException("Cannot register '" + stringProductLinkedManagerCalculatorEntry.getKey() + "' as a calculator for of linked manager type type '" + calculator.getLinkedManagerType()
						+ "' because this linked manager type type already has a registered calculator.");
			}
			if (calculator.getLinkedManagerType().isProcessingDependent()) {
				ValidationUtils.assertTrue((calculator instanceof ProductLinkedManagerDependentCalculator),
						"Linked manager types that are processing dependent must be of type ProductLinkedManagerDependentCalculator");
			}
			else {
				ValidationUtils.assertTrue((calculator instanceof ProductLinkedManagerIndependentCalculator),
						"Linked manager types that are NOT processing dependent must be of type ProductLinkedManagerIndependentCalculator");
			}

			getCalculatorMap().put(calculator.getLinkedManagerType(), calculator);
		}
	}


	////////////////////////////////////////////////////////////////////////////////


	@Override
	public ProductLinkedManagerDependentCalculator locateDependent(LinkedManagerTypes linkedManagerType) {
		return (ProductLinkedManagerDependentCalculator) locate(linkedManagerType, true);
	}


	@Override
	public ProductLinkedManagerIndependentCalculator locateIndependent(LinkedManagerTypes linkedManagerType) {
		return (ProductLinkedManagerIndependentCalculator) locate(linkedManagerType, false);
	}

	////////////////////////////////////////////////////////////////////////////////


	private ProductLinkedManagerCalculator locate(LinkedManagerTypes linkedManagerType, boolean processingDependent) {
		AssertUtils.assertNotNull(linkedManagerType, "Required linked manager type type cannot be null.");
		if (linkedManagerType.isInactive()) {
			throw new ValidationException("Linked Manager Type: " + linkedManagerType.getLabel() + " is inactive and is no longer supported.");
		}
		ValidationUtils.assertEquals(processingDependent, linkedManagerType.isProcessingDependent(), "Can only locate ProductLinkedManager" + (processingDependent ? "Dependent" : "Independent")
				+ "Calculator for Linked manager types that are processing " + (processingDependent ? "dependent" : "independent") + ". Linked Manager Type [" + linkedManagerType.getLabel() + "] is "
				+ (linkedManagerType.isProcessingDependent() ? "" : "not ") + "set as processing dependent.");
		ProductLinkedManagerCalculator result = getCalculatorMap().get(linkedManagerType);
		AssertUtils.assertNotNull(result, "Cannot locate ProductManagerAccountBalanceLinkedCalculator for '%1s' linked manager type.", linkedManagerType);
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Map<LinkedManagerTypes, ProductLinkedManagerCalculator> getCalculatorMap() {
		return this.calculatorMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
