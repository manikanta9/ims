package com.clifton.product.manager;


/**
 * The <code>ProductManagerProcessingTypes</code> enum defines the types of processing for manager balances that can be done. i.e. Balance Load, Balance Reload, Process Adjustments, etc.
 *
 * @author manderson
 */
public enum ProductManagerBalanceProcessingTypes {

	LOAD(false, false, false), // Loads Missing Balances Only - Does not process Adjustments
	LOAD_PROCESS(false, false, true), // Loads and Processes Missing Balances
	RELOAD(true, false, false), // Reloads Balances
	RELOAD_PROCESS(true, false, true), // Reloads Processes Balances
	RELOAD_CHANGES(false, true, false), // Reloads Balances that have changes only
	RELOAD_CHANGES_PROCESS(false, true, true); // Reloads Balances that have changed only and immediately process their adjustments

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private final boolean forceReload; // reload manager/sets processing to unprocessed
	private final boolean reloadIfChangedOnly; // reload manager - set processing to unprocessed ONLY if the original securities or cash value changed, else will leave balance as is
	private final boolean processAdjustments;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	ProductManagerBalanceProcessingTypes(boolean forceReload, boolean reloadIfChangedOnly, boolean processAdjustments) {
		this.forceReload = forceReload;
		this.reloadIfChangedOnly = reloadIfChangedOnly;
		this.processAdjustments = processAdjustments;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isLoadMissingOnly() {
		return !isReloadIfChangedOnly() && !isForceReload();
	}


	public boolean isForceReload() {
		return this.forceReload;
	}


	public boolean isReloadIfChangedOnly() {
		return this.reloadIfChangedOnly;
	}


	public boolean isProcessAdjustments() {
		return this.processAdjustments;
	}
}
