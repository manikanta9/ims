package com.clifton.product.manager.linkedcalculators.independent;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.manager.linkedcalculators.BaseProductLinkedManagerCalculatorImpl;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ProductLinkedManagerForNotionalCalculator</code> handles loading
 * Securities balances from Positions Notional value
 * <p>
 * NOTIONAL, // Securities Only: comes from the notional of our positions held in the specified account.
 *
 * @author manderson
 */
public class ProductLinkedManagerForNotionalCalculator extends BaseProductLinkedManagerCalculatorImpl implements ProductLinkedManagerIndependentCalculator {


	@Override
	public void calculate(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		// Set Securities Value
		BigDecimal securitiesValue = BigDecimal.ZERO;
		List<AccountingPositionDaily> accountingPositionDailyList = getAccountingPositionDailyList(balance, managerBalanceProcessingConfig);
		for (AccountingPositionDaily accountingPositionDaily : CollectionUtils.getIterable(accountingPositionDailyList)) {
			// Uses helper method so receivable positions are filtered out - handled by AccountingPositionService.getAccountingPositionMarketValue
			securitiesValue = MathUtils.add(securitiesValue, getNotionalValueInManagerBaseCurrency(balance.getManagerAccount(), accountingPositionDaily, managerBalanceProcessingConfig));
		}
		balance.setSecuritiesValue(securitiesValue);
	}


	private BigDecimal getNotionalValueInManagerBaseCurrency(InvestmentManagerAccount managerAccount, AccountingPositionDaily accountingPositionDaily, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		return managerBalanceProcessingConfig.getAccountingValueInManagerBaseCurrency(managerAccount, accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity(), accountingPositionDaily.getNotionalValueLocal(), accountingPositionDaily.getNotionalValueBase(), getMarketDataExchangeRatesApiService());
	}
}
