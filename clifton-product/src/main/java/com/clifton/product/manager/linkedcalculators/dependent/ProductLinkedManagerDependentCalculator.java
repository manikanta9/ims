package com.clifton.product.manager.linkedcalculators.dependent;


import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.manager.linkedcalculators.ProductLinkedManagerCalculator;


/**
 * The <code>ProductLinkedManagerDependentCalculator</code> interface defines the calculation method
 * that needs to be implemented by LinkedManagerType.processingDependent = true
 *
 * @author manderson
 */
public interface ProductLinkedManagerDependentCalculator extends ProductLinkedManagerCalculator {

	/**
	 * Calculates cash and/or securities and sets the values on the give balance
	 *
	 * @param balance                      - The manager balance to calculate cash and/or securities for (manager and balance date are expected to already be populated on this bean)
	 * @param linkedRun                    - Linked PIOS run that this manager needs to determine calculations
	 * @param doNotValidateLinkedRunStatus - If true, will NOT validate the linked run is not Incomplete - when called during processing of the linked run, the run is still technically in a processing state, but we know all necessary data is available to load the balance
	 */
	public void calculate(InvestmentManagerAccountBalance balance, PortfolioRun linkedRun, boolean doNotValidateLinkedRunStatus);
}
