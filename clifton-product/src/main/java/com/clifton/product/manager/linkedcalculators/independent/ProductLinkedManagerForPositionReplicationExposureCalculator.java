package com.clifton.product.manager.linkedcalculators.independent;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocationService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.portfolio.replication.PortfolioReplicationAllocationSecurityConfig;
import com.clifton.portfolio.replication.PortfolioReplicationHandler;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.manager.linkedcalculators.BaseProductLinkedManagerCalculatorImpl;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.replication.ProductReplicationService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * ProductLinkedManagerForPositionReplicationExposureCalculator allows selecting subset of positions and selecting a replication type to use to calculate
 * the contract exposure for each position.  Useful for Linked Managers that should be the exposure of positions in the same account, but we
 * can't utilize the run dependency because it will create a circular reference
 *
 * @author manderson
 */
public class ProductLinkedManagerForPositionReplicationExposureCalculator extends BaseProductLinkedManagerCalculatorImpl implements ProductLinkedManagerIndependentCalculator {

	private InvestmentAccountAssetClassService investmentAccountAssetClassService;
	private InvestmentAccountAssetClassPositionAllocationService investmentAccountAssetClassPositionAllocationService;

	private PortfolioReplicationHandler portfolioReplicationHandler;
	private ProductReplicationService productReplicationService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void calculate(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		Integer assetClassId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_ASSET_CLASS_FIELD);
		if (assetClassId == null) {
			throw new ValidationException("Unable to calculate manager balance: " + balance.getLabel() + ". Account Asset Class Selection is required for Position Replication Exposure linked manager type.");
		}
		InvestmentAccountAssetClass accountAssetClass = getInvestmentAccountAssetClassService().getInvestmentAccountAssetClass(assetClassId);
		if (accountAssetClass == null) {
			throw new ValidationException("Unable to calculate manager balance: " + balance.getLabel() + ". Account Asset Class Selection is invalid.  Cannot find account asset class with id [" + assetClassId + "].");
		}
		else if (accountAssetClass.getPrimaryReplication() == null) {
			throw new ValidationException("Unable to calculate manager balance: " + balance.getLabel() + ". Account Asset Class Selection is invalid.  There is not a replication on the asset class, so there is no way to calculate exposure.");
		}

		// Track Position Quantities (Less Processing, so we Calculate Exposure for the Total, not for each lot)
		List<InvestmentSecurity> securityList = new ArrayList<>();
		Map<Integer, BigDecimal> securityQuantityMap = new HashMap<>();
		List<AccountingPositionDaily> accountingPositionList = getAccountingPositionDailyList(balance, managerBalanceProcessingConfig);
		if (!CollectionUtils.isEmpty(accountingPositionList)) {
			for (AccountingPositionDaily pos : CollectionUtils.getIterable(accountingPositionList)) {
				securityList.add(pos.getAccountingTransaction().getInvestmentSecurity());
				Integer securityId = pos.getAccountingTransaction().getInvestmentSecurity().getId();
				if (securityQuantityMap.containsKey(securityId)) {
					securityQuantityMap.put(securityId, MathUtils.add(securityQuantityMap.get(securityId), pos.getRemainingQuantity()));
				}
				else {
					securityQuantityMap.put(securityId, pos.getRemainingQuantity());
				}
			}
		}
		adjustSecurityQuantitiesForExplicitPositionAllocation(accountAssetClass, balance.getBalanceDate(), securityQuantityMap);

		BigDecimal securitiesValue = BigDecimal.ZERO;
		if (!CollectionUtils.isEmpty(securityList)) {
			// Set Up A Fake Run/Asset Class to Pass to Calculator
			PortfolioRun run = new PortfolioRun();
			run.setClientInvestmentAccount(balance.getManagerAccount().getLinkedInvestmentAccount());
			run.setBalanceDate(balance.getBalanceDate());
			ProductOverlayAssetClass overlayAssetClass = new ProductOverlayAssetClass();
			overlayAssetClass.setAccountAssetClass(accountAssetClass);
			overlayAssetClass.setOverlayRun(run);

			// Apply Security/Asset Class Replication Filters
			List<PortfolioReplicationAllocationSecurityConfig> replicationAllocationList = getPortfolioReplicationHandler().getPortfolioReplicationAllocationSecurityConfigListForBalanceDate(accountAssetClass.getPrimaryReplication().getId(), balance.getBalanceDate(), securityList);
			if (accountAssetClass.getSecondaryReplication() != null) {
				replicationAllocationList.addAll(getPortfolioReplicationHandler().getPortfolioReplicationAllocationSecurityConfigListForBalanceDate(accountAssetClass.getSecondaryReplication().getId(), balance.getBalanceDate(), securityList));
			}

			for (PortfolioReplicationAllocationSecurityConfig allocationSecurityConfig : CollectionUtils.getIterable(replicationAllocationList)) {
				for (InvestmentSecurity security : CollectionUtils.getIterable(allocationSecurityConfig.getSecurityList())) {
					BigDecimal quantity = securityQuantityMap.get(security.getId());
					// Don't calculate for anything not held (no quantity means no exposure)
					if (!MathUtils.isNullOrZero(quantity)) {
						BigDecimal exposure = getSecurityExposure(overlayAssetClass, allocationSecurityConfig, managerBalanceProcessingConfig, security, quantity);
						securitiesValue = MathUtils.add(securitiesValue, exposure);
						// Set Quantity to Zero So we don't double count (We don't support contract splitting and it's possible same security is held in primary and secondary, so just calculate it once
						securityQuantityMap.put(security.getId(), null);
					}
				}
			}
		}
		balance.setSecuritiesValue(securitiesValue);
	}


	private void adjustSecurityQuantitiesForExplicitPositionAllocation(InvestmentAccountAssetClass accountAssetClass, Date balanceDate, Map<Integer, BigDecimal> securityQuantityMap) {
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = getInvestmentAccountAssetClassPositionAllocationService().getInvestmentAccountAssetClassPositionAllocationList(accountAssetClass.getAccount().getId(), balanceDate);
		if (!CollectionUtils.isEmpty(positionAllocationList)) {
			Map<Integer, List<InvestmentAccountAssetClassPositionAllocation>> explicitMap = BeanUtils.getBeansMap(positionAllocationList, "security.id");
			for (Map.Entry<Integer, BigDecimal> integerBigDecimalEntry : securityQuantityMap.entrySet()) {
				if (explicitMap.containsKey(integerBigDecimalEntry.getKey())) {
					BigDecimal explicitQuantity = null;
					BigDecimal otherQuantity = null;
					for (InvestmentAccountAssetClassPositionAllocation securityAllocation : explicitMap.get(integerBigDecimalEntry.getKey())) {
						if (accountAssetClass.equals(securityAllocation.getAccountAssetClass())) {
							explicitQuantity = MathUtils.add(explicitQuantity, securityAllocation.getAllocationQuantity());
						}
						else {
							otherQuantity = MathUtils.add(otherQuantity, securityAllocation.getAllocationQuantity());
						}
					}

					if (explicitQuantity != null) {
						securityQuantityMap.put(integerBigDecimalEntry.getKey(), explicitQuantity);
					}
					else {
						securityQuantityMap.put(integerBigDecimalEntry.getKey(), MathUtils.subtract(integerBigDecimalEntry.getValue(), otherQuantity));
					}
				}
			}
		}
	}


	private BigDecimal getSecurityExposure(ProductOverlayAssetClass overlayAssetClass, PortfolioReplicationAllocationSecurityConfig allocationSecurityConfig, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig, InvestmentSecurity security, BigDecimal quantity) {
		// Populate Asset Class Level Information if Required to Calculate Contract Value and Missing
		if (overlayAssetClass.getAccountAssetClass().getBenchmarkDurationSecurity() != null) {
			if (allocationSecurityConfig.getReplicationAllocation().getCoalesceInvestmentReplicationTypeOverride().isDurationSupported() && overlayAssetClass.getBenchmarkDuration() == null) {
				overlayAssetClass.setBenchmarkDuration(managerBalanceProcessingConfig.getSecurityDuration(overlayAssetClass.getAccountAssetClass().getBenchmarkDurationSecurity(), getMarketDataRetriever(), overlayAssetClass.getAccountAssetClass().getDurationFieldNameOverride()));
			}
			if (allocationSecurityConfig.getReplicationAllocation().getCoalesceInvestmentReplicationTypeOverride().isCreditDurationSupported() && overlayAssetClass.getBenchmarkCreditDuration() == null) {
				overlayAssetClass.setBenchmarkCreditDuration(getMarketDataRetriever().getCreditDuration(overlayAssetClass.getAccountAssetClass().getBenchmarkDurationSecurity(), overlayAssetClass.getOverlayRun().getBalanceDate(), true));
			}
		}

		ProductOverlayAssetClassReplication rep = new ProductOverlayAssetClassReplication();
		rep.setReplication(allocationSecurityConfig.getReplicationAllocation().getReplication());
		rep.setAllocationConfig(allocationSecurityConfig);
		rep.setOverlayAssetClass(overlayAssetClass);
		rep.setActualContracts(quantity);
		getProductReplicationService().setupProductOverlayAssetClassReplicationSecurityInfo(rep, security, managerBalanceProcessingConfig.getFxRateDataSourceName(overlayAssetClass.getAccountAssetClass().getClientInvestmentAccount(), getMarketDataExchangeRatesApiService()), true);
		return rep.getActualExposure();
	}


	////////////////////////////////////////////////////////////////////////////
	//////////////         Getter and Setter Methods            ////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}


	public InvestmentAccountAssetClassPositionAllocationService getInvestmentAccountAssetClassPositionAllocationService() {
		return this.investmentAccountAssetClassPositionAllocationService;
	}


	public void setInvestmentAccountAssetClassPositionAllocationService(InvestmentAccountAssetClassPositionAllocationService investmentAccountAssetClassPositionAllocationService) {
		this.investmentAccountAssetClassPositionAllocationService = investmentAccountAssetClassPositionAllocationService;
	}


	public PortfolioReplicationHandler getPortfolioReplicationHandler() {
		return this.portfolioReplicationHandler;
	}


	public void setPortfolioReplicationHandler(PortfolioReplicationHandler portfolioReplicationHandler) {
		this.portfolioReplicationHandler = portfolioReplicationHandler;
	}


	public ProductReplicationService getProductReplicationService() {
		return this.productReplicationService;
	}


	public void setProductReplicationService(ProductReplicationService productReplicationService) {
		this.productReplicationService = productReplicationService;
	}
}
