package com.clifton.product.manager.job;

import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.product.manager.ProductManagerBalanceProcessingTypes;
import com.clifton.product.manager.ProductManagerService;

import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * @author TerryS
 */
public class ProductManagerAccountBalanceJob implements Task, StatusHolderObjectAware<Status>, ValidationAware {

	private ProductManagerAccountBalanceJobTypes jobType;

	private Integer daysFromToday = 1;
	private DateGenerationOptions dateGenerationOption;

	private Integer investmentAccountGroupId;
	private Integer clientId;
	private Integer custodianCompanyId;
	private InvestmentManagerAccount.ManagerTypes managerType;

	private boolean reload;
	private boolean loadProcessAdjustments;

	private Date balanceDate;

	private StatusHolderObject<Status> statusHolderObject;
	private ProductManagerService productManagerService;
	private CalendarSetupService calendarSetupService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarDateGenerationHandler calendarDateGenerationHandler;


	@Override
	public Status run(Map<String, Object> context) {
		switch (getJobType()) {
			case PROCESS_MANAGER_BALANCES:
				getStatusHolderObject().getStatus().addMessage("Process Investment Manager Balances");
				return doProductManagerAccountBalance();
			case PROCESS_MANAGER_BALANCES_ADJUSTMENTS:
				getStatusHolderObject().getStatus().addMessage("Process Investment Manager Adjustment Balances");
				return doProductManagerAccountBalanceAdjustments();
			default:
				throw new RuntimeException("Product Manager Account Balance Job type not supported " + getJobType());
		}
	}


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getJobType(), "The Job Type is required.");
		ValidationUtils.assertNotNull(getDaysFromToday(), "The Days from Today required.");
		ValidationUtils.assertNotNull(getDateGenerationOption(), "The Date Generator if required.");
		ValidationUtils.assertFalse(Objects.nonNull(getClientId()) && Objects.nonNull(getInvestmentAccountGroupId()), "Cannot select both [Client Group] and [Client].");
	}


	private Status doProductManagerAccountBalanceAdjustments() {
		AccountBalanceAdjustmentsCommand command = AccountBalanceAdjustmentsCommand.ofSynchronous(createSearchForm(), calculateBalanceDate(), isReload());
		command.setStatus(getStatusHolderObject().getStatus());
		command.getStatus().addMessage(getProductManagerService().processProductManagerAccountBalanceAdjustmentList(command));
		command.getStatus().setMessage(command.getStatus().getDetailList().stream().map(d -> d.getCategory() + ": " + d.getNote()).collect(Collectors.joining("\n")));
		return command.getStatus();
	}


	private Status doProductManagerAccountBalance() {
		AccountBalanceCommand command = AccountBalanceCommand.ofSynchronous(createSearchForm(), calculateBalanceDate(), determineProcessingType());
		command.setStatus(getStatusHolderObject().getStatus());
		command.getStatus().addMessage(getProductManagerService().processProductManagerAccountBalanceList(command));
		command.getStatus().setMessage(command.getStatus().getDetailList().stream().map(d -> d.getCategory() + ": " + d.getNote()).collect(Collectors.joining("\n")));
		return command.getStatus();
	}


	public Date calculateBalanceDate() {
		if (this.balanceDate == null) {
			if (getDateGenerationOption() != null) {
				this.balanceDate = getCalendarDateGenerationHandler().generateDate(getDateGenerationOption(), getDaysFromToday());
			}
			else {
				this.balanceDate = getCalendarDateGenerationHandler().generateDate(DateGenerationOptions.PREVIOUS_BUSINESS_DAY);
			}
		}
		return this.balanceDate;
	}


	private InvestmentManagerAccountSearchForm createSearchForm() {
		InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();
		searchForm.setInvestmentAccountGroupId(getInvestmentAccountGroupId());
		searchForm.setClientId(getClientId());
		searchForm.setCustodianCompanyId(getCustodianCompanyId());
		searchForm.setManagerType(getManagerType());

		return searchForm;
	}


	private ProductManagerBalanceProcessingTypes determineProcessingType() {
		ProductManagerBalanceProcessingTypes result = ProductManagerBalanceProcessingTypes.LOAD;
		if (isReload()) {
			if (isLoadProcessAdjustments()) {
				result = ProductManagerBalanceProcessingTypes.RELOAD_PROCESS;
			}
			else {
				result = ProductManagerBalanceProcessingTypes.RELOAD;
			}
		}
		else if (isLoadProcessAdjustments()) {
			result = ProductManagerBalanceProcessingTypes.LOAD_PROCESS;
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ProductManagerAccountBalanceJobTypes getJobType() {
		return this.jobType;
	}


	public void setJobType(ProductManagerAccountBalanceJobTypes jobType) {
		this.jobType = jobType;
	}


	public Integer getDaysFromToday() {
		return this.daysFromToday;
	}


	public void setDaysFromToday(Integer daysFromToday) {
		this.daysFromToday = daysFromToday;
	}


	public DateGenerationOptions getDateGenerationOption() {
		return this.dateGenerationOption;
	}


	public void setDateGenerationOption(DateGenerationOptions dateGenerationOption) {
		this.dateGenerationOption = dateGenerationOption;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Integer getClientId() {
		return this.clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public Integer getCustodianCompanyId() {
		return this.custodianCompanyId;
	}


	public void setCustodianCompanyId(Integer custodianCompanyId) {
		this.custodianCompanyId = custodianCompanyId;
	}


	public InvestmentManagerAccount.ManagerTypes getManagerType() {
		return this.managerType;
	}


	public void setManagerType(InvestmentManagerAccount.ManagerTypes managerType) {
		this.managerType = managerType;
	}


	public boolean isReload() {
		return this.reload;
	}


	public void setReload(boolean reload) {
		this.reload = reload;
	}


	public boolean isLoadProcessAdjustments() {
		return this.loadProcessAdjustments;
	}


	public void setLoadProcessAdjustments(boolean loadProcessAdjustments) {
		this.loadProcessAdjustments = loadProcessAdjustments;
	}


	public StatusHolderObject<Status> getStatusHolderObject() {
		return this.statusHolderObject;
	}


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	public ProductManagerService getProductManagerService() {
		return this.productManagerService;
	}


	public void setProductManagerService(ProductManagerService productManagerService) {
		this.productManagerService = productManagerService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public enum ProductManagerAccountBalanceJobTypes {PROCESS_MANAGER_BALANCES, PROCESS_MANAGER_BALANCES_ADJUSTMENTS}
}
