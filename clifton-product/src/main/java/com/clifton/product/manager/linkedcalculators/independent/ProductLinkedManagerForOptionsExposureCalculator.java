package com.clifton.product.manager.linkedcalculators.independent;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.manager.linkedcalculators.BaseProductLinkedManagerCalculatorImpl;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductManagerAccountBalanceExposureCalculator</code> ...
 * <p>
 * OPTION_POSITIONS_EXPOSURE, // Securities Only: comes from the exposure of our options positions held in the specified account.  Calculated as Contracts * Underlying Index Price * Delta * Option Price Multiplier.
 *
 * @author manderson
 */
public class ProductLinkedManagerForOptionsExposureCalculator extends BaseProductLinkedManagerCalculatorImpl implements ProductLinkedManagerIndependentCalculator {

	private MarketDataFieldService marketDataFieldService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void calculate(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		BigDecimal securities = BigDecimal.ZERO;

		Map<InvestmentSecurity, BigDecimal> securityPositionsMap = new HashMap<>();
		List<AccountingPositionDaily> accountingPositionList = getAccountingPositionDailyList(balance, InvestmentType.OPTIONS, managerBalanceProcessingConfig);
		for (AccountingPositionDaily accountingPositionDaily : CollectionUtils.getIterable(accountingPositionList)) {
			InvestmentSecurity security = accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity();
			if (securityPositionsMap.containsKey(security)) {
				securityPositionsMap.put(security, MathUtils.add(securityPositionsMap.get(security), accountingPositionDaily.getRemainingQuantity()));
			}
			else {
				securityPositionsMap.put(security, accountingPositionDaily.getRemainingQuantity());
			}
		}
		for (Map.Entry<InvestmentSecurity, BigDecimal> investmentSecurityBigDecimalEntry : securityPositionsMap.entrySet()) {
			BigDecimal contracts = investmentSecurityBigDecimalEntry.getValue();
			final InvestmentSecurity investmentSecurity = investmentSecurityBigDecimalEntry.getKey();

			if (investmentSecurity.getUnderlyingSecurity() == null) {
				throw generateLinkedManagerLoadException(balance, "InvestmentSecurity", (investmentSecurity).getId(), "There is no underlying security selected for [" + (investmentSecurityBigDecimalEntry.getKey()).getLabel() + "].");
			}
			BigDecimal underlyingPrice = getMarketDataRetriever().getPrice(investmentSecurity.getUnderlyingSecurity(), balance.getBalanceDate(), true,
					"Linked Manager [" + balance.getManagerAccount().getLabel() + "] option position ");

			BigDecimal deltaValue = BigDecimal.ONE;
			Boolean excludeDelta = (Boolean) getProductUtilService().getInvestmentManagerAccountLinkedManagerCustomFieldValue(balance.getManagerAccount(),
					BaseProductLinkedManagerCalculatorImpl.LINKED_MANAGER_OPTIONS_EXCLUDE_DELTA);
			if (excludeDelta == null || !excludeDelta) {
				MarketDataValueHolder delta = getMarketDataFieldService().getMarketDataValueForDateFlexible(investmentSecurity.getId(), balance.getBalanceDate(), MarketDataField.FIELD_DELTA);
				if (delta == null) {
					throw generateLinkedManagerLoadException(balance, "InvestmentSecurity", investmentSecurity.getId(), "Unable to find a Market Data Delta value for security [" + investmentSecurity.getLabel() + "].");
				}
				deltaValue = delta.getMeasureValue();

				// If CALL and Long Position or PUT and Short Position, then Delta is positive (as entered), otherwise it's negative
				if (investmentSecurity.isCallOption() && MathUtils.isLessThan(contracts, BigDecimal.ZERO)
						|| (investmentSecurity.isPutOption() && MathUtils.isGreaterThan(contracts, BigDecimal.ZERO))) {
					deltaValue = MathUtils.negate(deltaValue);
				}
			}

			// Options Exposure = Contracts * Underlying Index Price * Delta * Option price Multiplier
			BigDecimal securityValue = MathUtils.multiply(MathUtils.abs(contracts), underlyingPrice);
			securityValue = MathUtils.multiply(securityValue, deltaValue);
			securityValue = MathUtils.multiply(securityValue, (investmentSecurityBigDecimalEntry.getKey()).getPriceMultiplier());
			securities = MathUtils.add(securities, securityValue);
		}
		balance.setSecuritiesValue(securities);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}
}
