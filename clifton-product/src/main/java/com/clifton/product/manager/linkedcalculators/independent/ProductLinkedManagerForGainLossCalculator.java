package com.clifton.product.manager.linkedcalculators.independent;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailySearchForm;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.manager.linkedcalculators.BaseProductLinkedManagerCalculatorImpl;

import java.util.Date;
import java.util.List;


/**
 * The <code>ProductManagerAccountBalanceGainLossCalculator</code> ...
 * <p>
 * SECURITY_GAIN_LOSS, // Securities: comes from the cumulative Gain/Loss from matching positions for the selected account since the given start date.  Balances on that start date are always zero.
 *
 * @author manderson
 */
public class ProductLinkedManagerForGainLossCalculator extends BaseProductLinkedManagerCalculatorImpl implements ProductLinkedManagerIndependentCalculator {


	@Override
	public void calculate(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		Date startDate = (Date) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_START_DATE_FIELD);
		// Start Date BEFORE Balance Date - Then get a value, otherwise value is 0
		if (DateUtils.compare(startDate, balance.getBalanceDate(), false) < 0) {

			AccountingPositionDailySearchForm searchForm = new AccountingPositionDailySearchForm();
			searchForm.setClientInvestmentAccountId(balance.getManagerAccount().getLinkedInvestmentAccount().getId());
			searchForm.setStartPositionDate(DateUtils.addDays(startDate, 1)); // Move start to next day so we don't include gain loss on the start date
			searchForm.setEndPositionDate(balance.getBalanceDate());

			Integer investmentGroupId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_GROUP_FIELD);
			if (investmentGroupId != null) {
				searchForm.setInvestmentGroupId(investmentGroupId.shortValue());
			}
			Integer hierarchyId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_HIERARCHY_FIELD);
			if (hierarchyId != null) {
				searchForm.setInvestmentHierarchyId(hierarchyId.shortValue());
			}

			Integer securityGroupId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_INVESTMENT_SECURITY_GROUP_FIELD);
			if (securityGroupId != null) {
				searchForm.setInvestmentSecurityGroupId(securityGroupId.shortValue());
			}
			List<AccountingPositionDaily> positionDailyList = getAccountingPositionDailyService().getAccountingPositionDailyList(searchForm);
			balance.setSecuritiesValue(CoreMathUtils.sumProperty(positionDailyList, AccountingPositionDaily::getDailyGainLossBase));
		}
	}
}
