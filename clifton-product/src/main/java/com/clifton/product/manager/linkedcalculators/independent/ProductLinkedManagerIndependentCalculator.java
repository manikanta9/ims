package com.clifton.product.manager.linkedcalculators.independent;


import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.manager.linkedcalculators.ProductLinkedManagerCalculator;


/**
 * The <code>ProductLinkedManagerIndependentCalculator</code> ...
 *
 * @author manderson
 */
public interface ProductLinkedManagerIndependentCalculator extends ProductLinkedManagerCalculator {

	/**
	 * Calculates cash and/or securities and sets the values on the give balance
	 *
	 * @param balance                        - The manager balance to calculate cash and/or securities for (manager and balance date are expected to already be populated on this bean)
	 * @param managerBalanceProcessingConfig - Used to cache data for re-usability for positions, ccy, etc. look ups
	 */
	public void calculate(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig);
}
