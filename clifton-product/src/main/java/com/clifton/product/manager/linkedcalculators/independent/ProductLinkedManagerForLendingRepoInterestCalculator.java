package com.clifton.product.manager.linkedcalculators.independent;


import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.lending.api.repo.LendingRepoApiService;
import com.clifton.lending.api.repo.RepoSearchCommand;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.manager.linkedcalculators.BaseProductLinkedManagerCalculatorImpl;

import java.math.BigDecimal;


/**
 * The <code>ProductManagerAccountBalanceLendingRepoInterestCalculator</code> ...
 * <p>
 * ACCRUED_INTEREST_REPOS, // Cash Only: comes from the interest from REPOs for the selected Client Account and optionally holding account. Open vs. Term interest is calculated based on its type.
 *
 * @author manderson
 */
public class ProductLinkedManagerForLendingRepoInterestCalculator extends BaseProductLinkedManagerCalculatorImpl implements ProductLinkedManagerIndependentCalculator {

	private LendingRepoApiService lendingRepoApiService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void calculate(InvestmentManagerAccountBalance balance, ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig) {
		RepoSearchCommand searchCommand = new RepoSearchCommand();

		Integer holdingAccountId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_HOLDING_ACCOUNT_FIELD);
		searchCommand.setHoldingInvestmentAccountId(holdingAccountId);

		Integer repoAccountId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_REPO_ACCOUNT_FIELD);
		searchCommand.setRepoInvestmentAccountId(repoAccountId);

		Integer repoTypeId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_REPO_TYPE);
		searchCommand.setTypeId(repoTypeId);

		BigDecimal cash = getLendingRepoApiService().getRepoAccruedInterestForCommand(searchCommand, balance.getBalanceDate());

		balance.setCashValue(cash);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public LendingRepoApiService getLendingRepoApiService() {
		return this.lendingRepoApiService;
	}


	public void setLendingRepoApiService(LendingRepoApiService lendingRepoApiService) {
		this.lendingRepoApiService = lendingRepoApiService;
	}
}
