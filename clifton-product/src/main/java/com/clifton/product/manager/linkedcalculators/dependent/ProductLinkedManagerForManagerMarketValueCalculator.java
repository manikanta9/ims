package com.clifton.product.manager.linkedcalculators.dependent;


import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.manager.linkedcalculators.BaseProductLinkedManagerCalculatorImpl;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.overlay.search.ProductOverlayManagerAccountSearchForm;

import java.util.List;


/**
 * The <code>ProductManagerAccountBalanceManagerMarketValueCalculator</code> ...
 * <p>
 * Handles Loading Manager Cash and/or Securities from another PIOS run for a different account:
 * Used by MANAGER_MARKET_VALUE, MANAGER_CASH_MARKET_VALUE, MANAGER_SECURITIES_MARKET_VALUE
 *
 * @author manderson
 */
public class ProductLinkedManagerForManagerMarketValueCalculator extends BaseProductLinkedManagerCalculatorImpl implements ProductLinkedManagerDependentCalculator {

	private ProductOverlayService productOverlayService;

	////////////////////////////////////////////////////////////////////////////////

	// Defined by Calculator definition in application context - difference between Cash, Positions, Total Market Value, etc.
	private boolean cash; // MANAGER_CASH_MARKET_VALUE, MANAGER_MARKET_VALUE
	private boolean securities; // MANAGER_SECURITIES_MARKET_VALUE, MANAGER_MARKET_VALUE

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void calculate(InvestmentManagerAccountBalance balance, PortfolioRun linkedRun, boolean doNotValidateLinkedRunStatus) {
		if (linkedRun == null) {
			return; // No Linked Run
		}
		validateLinkedRunStatus(balance, linkedRun, doNotValidateLinkedRunStatus);

		ProductOverlayManagerAccountSearchForm overlayManagerSearchForm = new ProductOverlayManagerAccountSearchForm();
		overlayManagerSearchForm.setRunId(linkedRun.getId());
		Integer accountAssetClassId = (Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_ASSET_CLASS_FIELD);
		// If Not looking for a specific asset class - get non rollup asset classes only (prevent double counting)
		if (accountAssetClassId != null) {
			overlayManagerSearchForm.setAccountAssetClassId(accountAssetClassId);
		}
		else {
			overlayManagerSearchForm.setRollupAssetClass(false);
		}
		overlayManagerSearchForm.setManagerAccountAssignmentId((Integer) getLinkedManagerCustomFieldValue(balance, LINKED_MANAGER_MANAGER_ACCOUNT_FIELD));

		List<ProductOverlayManagerAccount> overlayManagerList = getProductOverlayService().getProductOverlayManagerAccountList(overlayManagerSearchForm);
		if (isCash()) {
			balance.setCashValue(CoreMathUtils.sumProperty(overlayManagerList, ProductOverlayManagerAccount::getCashAllocation));
		}
		if (isSecurities()) {
			balance.setSecuritiesValue(CoreMathUtils.sumProperty(overlayManagerList, ProductOverlayManagerAccount::getSecuritiesAllocation));
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isCash() {
		return this.cash;
	}


	public void setCash(boolean cash) {
		this.cash = cash;
	}


	public boolean isSecurities() {
		return this.securities;
	}


	public void setSecurities(boolean securities) {
		this.securities = securities;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}
}
