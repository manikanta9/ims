package com.clifton.product.manager.job;

import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.product.manager.ProductManagerBalanceProcessingTypes;

import java.util.Date;


/**
 * @author TerryS
 */
public class AccountBalanceCommand extends BaseAccountBalanceCommand {

	public static AccountBalanceCommand ofSynchronous(InvestmentManagerAccountSearchForm searchForm, Date balanceDate, ProductManagerBalanceProcessingTypes processingType) {
		AccountBalanceCommand accountBalanceCommand = new AccountBalanceCommand();
		accountBalanceCommand.setSearchForm(searchForm);
		accountBalanceCommand.setBalanceDate(balanceDate);
		accountBalanceCommand.setProcessingType(processingType);
		accountBalanceCommand.setSynchronous(true);
		return accountBalanceCommand;
	}


	@Override
	public String getRunId() {
		return (getProcessingType().isProcessAdjustments() ? "BAL_ADJ_" : "BAL_") + super.getRunId();
	}
}
