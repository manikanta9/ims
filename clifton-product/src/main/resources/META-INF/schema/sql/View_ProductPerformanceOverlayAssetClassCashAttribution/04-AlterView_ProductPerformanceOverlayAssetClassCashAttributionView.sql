/****** Object:  View [dbo].[ProductPerformanceOverlayAssetClassCashAttributionView]    Script Date: 6/7/2019 8:30:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[ProductPerformanceOverlayAssetClassCashAttributionView] AS
	SELECT x.ProductPerformanceOverlayAssetClassID
		 , x.ClientInvestmentAccountID
		 , x.MeasureDate
		 , x.AssetClassName
		 , x.GainLoss
		 , x.OverlayTarget
		 , x.PerformanceAttributionReturn
		 , x.PerformanceAttributionReturnOverride
		 , x.FundCash
		 , x.FundCashAdjusted
		 , x.FundCashAttribution + CASE WHEN x.PlugAttributionCashType = 'FUND' THEN COALESCE(x.PlugAttribution, 0) ELSE 0 END                      "FundCashAttribution"
		 , x.ManagerCash
		 , x.ManagerCashAdjusted
		 , x.ManagerCashAttribution + CASE WHEN x.PlugAttributionCashType = 'MANAGER' THEN COALESCE(x.PlugAttribution, 0) ELSE 0 END                "ManagerCashAttribution"
		 , x.TransitionCash
		 , x.TransitionCashAdjusted
		 , x.TransitionCashAttribution + CASE WHEN x.PlugAttributionCashType = 'TRANSITION' THEN COALESCE(x.PlugAttribution, 0) ELSE 0 END          "TransitionCashAttribution"
		 , x.RebalanceCash
		 , x.RebalanceCashAdjusted
		 , x.RebalanceCashAttribution + CASE WHEN x.PlugAttributionCashType = 'REBALANCE' THEN COALESCE(x.PlugAttribution, 0) ELSE 0 END            "RebalanceCashAttribution"
		 , x.ClientDirectedCash
		 , x.ClientDirectedCashAdjusted
		 , x.ClientDirectedCashAttribution + CASE WHEN x.PlugAttributionCashType = 'CLIENT_DIRECTED' THEN COALESCE(x.PlugAttribution, 0) ELSE 0 END "ClientDirectedCashAttribution"
		 , x.PlugAttribution                                                                                                                        "PlugAttribution"
		 , x.PlugAttributionCashType                                                                                                                "PlugAttributionCashType"
		 , x.AttributionCalculationNote
	FROM (
			 SELECT x.ProductPerformanceOverlayAssetClassID
				  , x.ClientInvestmentAccountID
				  , x.MeasureDate
				  , x.AssetClassName
				  , x.GainLoss                                                                                                                    "GainLoss"
				  , x.OverlayTarget
				  , x.OverlayExposure
				  , x.PerformanceAttributionReturn
				  , x.PerformanceAttributionReturnOverride
				  , x.FundCash
				  , x.FundCashAdjusted
				  , ROUND(x.FundCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100), 2)       "FundCashAttribution"
				  , x.ManagerCash
				  , x.ManagerCashAdjusted
				  , ROUND(x.ManagerCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100), 2)    "ManagerCashAttribution"
				  , x.TransitionCash
				  , x.TransitionCashAdjusted
				  , ROUND(x.TransitionCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100), 2) "TransitionCashAttribution"
				  , x.RebalanceCash
				  , x.RebalanceCashAdjusted
				  , ROUND(x.RebalanceCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100), 2)  "RebalanceCashAttribution"
				  , x.ClientDirectedCash
				  , x.ClientDirectedCashAdjusted
				  , ROUND(x.ClientDirectedCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100),
						  2)                                                                                                                      "ClientDirectedCashAttribution"
				  , x.GainLoss - (ROUND(x.FundCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100), 2) +
								  ROUND(x.ManagerCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100), 2) +
								  ROUND(x.TransitionCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100), 2) +
								  ROUND(x.RebalanceCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100), 2) +
								  ROUND(x.ClientDirectedCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100),
										2))                                                                                                       "PlugAttribution"
				  , CASE
						WHEN x.GainLoss - (ROUND(x.FundCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100), 2) +
										   ROUND(x.ManagerCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100), 2) +
										   ROUND(x.TransitionCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100), 2) +
										   ROUND(x.RebalanceCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100), 2) +
										   ROUND(x.ClientDirectedCashAdjusted * (COALESCE(x.PerformanceAttributionReturnOverride, x.PerformanceAttributionReturn) / 100), 2)) = 0
							THEN NULL
						WHEN x.RebalanceCashAdjusted != 0 THEN 'REBALANCE'
						WHEN ABS(x.FundCashAdjusted) > ABS(x.ManagerCashAdjusted) AND ABS(x.FundCashAdjusted) > ABS(x.TransitionCashAdjusted) AND
							 ABS(x.FundCashAdjusted) > ABS(x.ClientDirectedCashAdjusted) THEN 'FUND'
						WHEN ABS(x.ManagerCashAdjusted) > ABS(x.FundCashAdjusted) AND ABS(x.ManagerCashAdjusted) > ABS(x.TransitionCashAdjusted) AND
							 ABS(x.ManagerCashAdjusted) > ABS(x.ClientDirectedCashAdjusted) THEN 'MANAGER'
						WHEN ABS(x.TransitionCashAdjusted) > ABS(x.FundCashAdjusted) AND ABS(x.TransitionCashAdjusted) > ABS(x.ManagerCashAdjusted) AND
							 ABS(x.TransitionCashAdjusted) > ABS(x.ClientDirectedCashAdjusted) THEN 'TRANSITION'
						WHEN ABS(x.ClientDirectedCashAdjusted) > ABS(x.FundCashAdjusted) AND ABS(x.ClientDirectedCashAdjusted) > ABS(x.ManagerCashAdjusted) AND
							 ABS(x.ClientDirectedCashAdjusted) > ABS(x.TransitionCashAdjusted) THEN 'CLIENT_DIRECTED'
						ELSE 'FUND'
					END                                                                                                                           'PlugAttributionCashType'
				  , x.AttributionCalculationNote
			 FROM (
					  SELECT poac.ProductPerformanceOverlayAssetClassID
						   , pa.ClientInvestmentAccountID
						   , por.MeasureDate
						   , COALESCE(iac.DisplayName, ac.AssetClassName)             "AssetClassName"
						   , COALESCE(poac.GainLossOverride, poac.GainLoss)           "GainLoss"
						   , COALESCE(poac.OverlayTargetOverride, poac.OverlayTarget) "OverlayTarget"
						   , oa.OverlayExposure                                       "OverlayExposure"
						   , ca.FundCash
						   , ca.FundCashAdjusted
						   , ca.ManagerCash
						   , ca.ManagerCashAdjusted
						   , ca.TransitionCash
						   , ca.TransitionCashAdjusted
						   , ca.RebalanceCash
						   , ca.RebalanceCashAdjusted
						   , ca.ClientDirectedCash
						   , ca.ClientDirectedCashAdjusted
						   , poac.PerformanceAttributionReturnOverride
						   , CASE
								 WHEN attributionCalc.OverlayExposureCalculation != 0 AND ABS(attributionCalc.OverlayExposureCalculation) < 5
									 THEN attributionCalc.OverlayExposureCalculation
								 WHEN attributionCalc.OverlayExposureCalculation != 0 AND ABS(attributionCalc.OverlayExposureCalculation) >= 5
									 THEN attributionCalc.AbsOverlayCalculation
								 WHEN attributionCalc.OverlayTargetCalculation != 0 THEN attributionCalc.OverlayTargetCalculation
								 WHEN attributionCalc.BenchmarkReturn IS NOT NULL THEN attributionCalc.BenchmarkReturn
								 ELSE 0
							 END                                                      "PerformanceAttributionReturn"
						   , CASE
								 WHEN poac.PerformanceAttributionReturnOverride IS NOT NULL THEN 'Using Manually Overridden Attribution Return Value'
								 WHEN attributionCalc.OverlayExposureCalculation != 0 AND ABS(attributionCalc.OverlayExposureCalculation) < 5 THEN NULL
								 WHEN attributionCalc.OverlayExposureCalculation != 0 AND ABS(attributionCalc.OverlayExposureCalculation) >= 5 THEN CASE
																																						WHEN ABS(oa.OverlayExposure) < ABS(COALESCE(poac.OverlayTargetOverride, poac.OverlayTarget))
																																							THEN 'Using Overlay Target because Using Overlay Exposure calculates a value greater than 5%'
																																						ELSE NULL
																																					END
								 WHEN attributionCalc.OverlayTargetCalculation != 0 THEN 'Using Overlay Target because Overlay Exposure is Zero'
								 WHEN attributionCalc.BenchmarkReturn IS NOT NULL THEN 'Using Asset Class Benchmark Return'
								 ELSE NULL
							 END                                                      "AttributionCalculationNote"
					  FROM ProductPerformanceOverlayAssetClass poac
						   INNER JOIN PerformancePortfolioRun por ON poac.ProductPerformanceOverlayRunID = por.PerformancePortfolioRunID
						   INNER JOIN PerformanceInvestmentAccount pa ON por.PerformanceInvestmentAccountID = pa.PerformanceInvestmentAccountID
						   INNER JOIN InvestmentAccount a ON pa.ClientInvestmentAccountID = a.InvestmentAccountID
						   INNER JOIN AccountingPeriod ap ON pa.AccountingPeriodID = ap.AccountingPeriodID AND ap.EndDate >= '01/01/2014' -- Hard Start Date of January 2014
						   LEFT JOIN ProductOverlayAssetClass oa ON poac.ProductOverlayAssetClassID = oa.ProductOverlayAssetClassID
						   LEFT JOIN InvestmentAccountAssetClass iac ON oa.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
						   LEFT JOIN InvestmentAssetClass ac ON iac.InvestmentAssetClassID = ac.InvestmentAssetClassID
						   OUTER APPLY (
						  SELECT oac.OverlayExposure
							   , oac.FundCash
							   , oac.FundCash + acCash.FundCashAdjustments                                                    "FundCashAdjusted"
							   , oac.ManagerCash
							   , oac.ManagerCash + acCash.ManagerCashAdjustments                                              "ManagerCashAdjusted"
							   , oac.TransitionCash
							   , oac.TransitionCash + acCash.TransitionCashAdjustments                                        "TransitionCashAdjusted"
							   , (oac.RebalanceCashAdjusted + oac.RebalanceAttributionCash)                                   "RebalanceCash"
							   , (oac.RebalanceCashAdjusted + oac.RebalanceAttributionCash) + acCash.RebalanceCashAdjustments "RebalanceCashAdjusted"
							   , oac.ClientDirectedCash
							   , oac.ClientDirectedCash + acCash.ClientDirectedCashAdjustments                                "ClientDirectedCashAdjusted"
							   , (oac.FundCash + oac.ManagerCash + oac.TransitionCash + oac.RebalanceAttributionCash + oac.RebalanceCashAdjusted +
								  oac.ClientDirectedCash)                                                                     "TotalCash"
							   , COALESCE(poac.GainLossOverride, poac.GainLoss)                                               "AttributionGainLoss"
						  FROM ProductOverlayAssetClass oac
							   OUTER APPLY (
							  SELECT SUM(COALESCE(CASE WHEN acAdj.FromCashType = 'FUND' THEN (-1 * acAdj.Amount) WHEN acAdj.ToCashType = 'FUND' THEN acAdj.Amount ELSE 0 END,
												  0))      "FundCashAdjustments"
								   , SUM(COALESCE(CASE WHEN acAdj.FromCashType = 'MANAGER' THEN (-1 * acAdj.Amount) WHEN acAdj.ToCashType = 'MANAGER' THEN acAdj.Amount ELSE 0 END,
												  0))      "ManagerCashAdjustments"
								   , SUM(COALESCE(
									  CASE WHEN acAdj.FromCashType = 'TRANSITION' THEN (-1 * acAdj.Amount) WHEN acAdj.ToCashType = 'TRANSITION' THEN acAdj.Amount ELSE 0 END,
									  0))                  "TransitionCashAdjustments"
								   , SUM(
									  COALESCE(CASE WHEN acAdj.FromCashType = 'REBALANCE' THEN (-1 * acAdj.Amount) WHEN acAdj.ToCashType = 'REBALANCE' THEN acAdj.Amount ELSE 0 END,
											   0))         "RebalanceCashAdjustments"
								   , SUM(COALESCE(CASE
													  WHEN acAdj.FromCashType = 'CLIENT_DIRECTED' THEN (-1 * acAdj.Amount)
													  WHEN acAdj.ToCashType = 'CLIENT_DIRECTED' THEN acAdj.Amount
													  ELSE 0
												  END, 0)) "ClientDirectedCashAdjustments"
							  FROM ProductOverlayAssetClass ac
								   LEFT JOIN ProductOverlayAssetClassCashAdjustment acAdj ON ac.ProductOverlayAssetClassID = acAdj.ProductOverlayAssetClassID
							  WHERE ac.ProductOverlayAssetClassID = poac.ProductOverlayAssetClassID
						  ) acCash
						  WHERE oac.ProductOverlayAssetClassID = poac.ProductOverlayAssetClassID
					  ) ca
						   CROSS APPLY (
						  SELECT MathUtil.GetPercentValue(COALESCE(poac.GainLossOverride, poac.GainLoss), oa.OverlayExposure) "OverlayExposureCalculation"
							   , MathUtil.GetPercentValue(COALESCE(poac.GainLossOverride, poac.GainLoss),
														  COALESCE(poac.OverlayTargetOverride, poac.OverlayTarget))           "OverlayTargetCalculation"
							   , CASE
									 WHEN ABS(oa.OverlayExposure) > ABS(COALESCE(poac.OverlayTargetOverride, poac.OverlayTarget))
										 THEN MathUtil.GetPercentValue(COALESCE(poac.GainLossOverride, poac.GainLoss), oa.OverlayExposure)
									 ELSE MathUtil.GetPercentValue(COALESCE(poac.GainLossOverride, poac.GainLoss), COALESCE(poac.OverlayTargetOverride, poac.OverlayTarget))
								 END                                                                                          "AbsOverlayCalculation"
							   , CASE
									 WHEN oa.OverlayExposure = 0 AND ca.RebalanceCashAdjusted != 0 AND por.MeasureDate >= a.InceptionDate THEN poac.BenchmarkReturn
									 ELSE NULL
								 END                                                                                          "BenchmarkReturn"
					  ) attributionCalc
				  ) x
		 ) x


GO


