package com.clifton.product.replication;


import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.portfolio.account.PortfolioAccountContractStoreTypes;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>ProductReplicationPositionAllocationConfigTests</code> tests methods in the {@link ProductReplicationPositionAllocationConfig}
 * for splitting Pending contracts/Pending Virtual Contracts
 * <p>
 * These splits can be allocated via {@link TradeAssetClassPositionAllocation} class which
 * are created when trades are entered via Trade Creation or Rolling Contracts
 *
 * @author manderson
 */
public class ProductReplicationPositionAllocationPendingConfigTests {

	@Test
	public void test3LongSplit_Buy() {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		// 1. Target = 0.29, Actual = 1
		list.add(setupReplication(1, 0.29, 1.0, null, 78432.0));
		// 2. Target = 0, Actual = 0
		list.add(setupReplication(2, 0.0, 0.0, null, 148191.0));
		// 3. Target = 0.40, Actual = 0
		list.add(setupReplication(3, 0.40, 0.0, null, 62866.0));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.PENDING;

		// 3. Was Explicit as 0 Before
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(3, 0, null));

		// BUY 9: Split 2, 3, 4
		List<TradeAssetClassPositionAllocation> tradeAllocationList = new ArrayList<>();
		tradeAllocationList.add(setupTradePositionAllocation(1, 2));
		tradeAllocationList.add(setupTradePositionAllocation(2, 3));
		tradeAllocationList.add(setupTradePositionAllocation(3, 4));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, tradeAllocationList, BigDecimal.valueOf(9));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(2, rep.getPendingContracts(), type.name());
			}
			else if ("2_2".equals(config.getKeyForReplication(rep))) {
				validateExpected(3, rep.getPendingContracts(), type.name());
			}
			else {
				validateExpected(4, rep.getPendingContracts(), type.name());
			}
		}
	}


	@Test
	public void test2LongSplit_Buy() {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		// 1. Target = 53.79, Actual = 52
		list.add(setupReplication(1, 53.79, 52.0, null, 112305.0));
		// 2. Target = 103.85, Actual = 99
		list.add(setupReplication(2, 103.85, 99.0, null, 226356.0));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.PENDING;

		// 1. Was Explicit as 52 Before
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 52, null));

		// BUY 5: Split 0, 5
		List<TradeAssetClassPositionAllocation> tradeAllocationList = new ArrayList<>();
		tradeAllocationList.add(setupTradePositionAllocation(2, 5));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, tradeAllocationList, BigDecimal.valueOf(5));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getPendingContracts(), type.name());
			}
			else {
				validateExpected(5, rep.getPendingContracts(), type.name());
			}
		}
	}


	@Test
	public void test2LongSplit_Sell() {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		// 1. Target = 15.17, Actual = 17
		list.add(setupReplication(1, 15.17, 17.0, null, 226356.0));
		// 2. Target = 5.24, Actual = 6
		list.add(setupReplication(2, 5.24, 6.0, null, 112305.0));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.PENDING;

		// 1. Was Explicit as 17 Before
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 17, null));

		// SELL 1: Split -1, 0
		List<TradeAssetClassPositionAllocation> tradeAllocationList = new ArrayList<>();
		tradeAllocationList.add(setupTradePositionAllocation(1, -1));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, tradeAllocationList, BigDecimal.valueOf(-1));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-1, rep.getPendingContracts(), type.name());
			}
			else {
				validateExpected(0, rep.getPendingContracts(), type.name());
			}
		}
	}


	@Test
	public void test2LongSplit_Sell_2_Trades_CurrentAndPending() {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		// 1. Target = 32.85, Actual = 45 (Explicit), Current S/B: 43, Pending: -10
		list.add(setupReplication(1, 1, 32.85, 32.85, 45.0, false, null, 111326.0));

		// 2. Target = 66.06, Actual = 90, Current S/B: 87, Pending: -21
		list.add(setupReplication(2, 2, 66.06, 66.06, 90.0, false, null, 2229647.0));

		// 1. Was Explicit as 45 Before
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 45, null));

		// SELL 5: Split -2, -3 Booked
		List<TradeAssetClassPositionAllocation> tradeAllocationList = new ArrayList<>();
		tradeAllocationList.add(setupTradePositionAllocation(1, null, -2, 5, -5, false));
		tradeAllocationList.add(setupTradePositionAllocation(2, null, -3, 5, -5, false));

		// SELL 31: Split -10, -21 Pending
		tradeAllocationList.add(setupTradePositionAllocation(1, null, -10, 6, -31, true));
		tradeAllocationList.add(setupTradePositionAllocation(2, null, -21, 6, -31, true));


		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.CURRENT;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, tradeAllocationList, BigDecimal.valueOf(130));
		config.process();
		// Verify Current Split
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(43, rep.getCurrentContracts(), type.name());
			}
			else {
				validateExpected(87, rep.getCurrentContracts(), type.name());
			}
		}

		// Then Process Second Batch of Pending
		type = PortfolioAccountContractStoreTypes.PENDING;
		config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, tradeAllocationList, BigDecimal.valueOf(-31));
		config.process();
		// Verify Current AND Pending Split
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(43, rep.getCurrentContracts(), type.name());
				validateExpected(-10, rep.getPendingContracts(), type.name());
			}
			else {
				validateExpected(87, rep.getCurrentContracts(), type.name());
				validateExpected(-21, rep.getPendingContracts(), type.name());
			}
		}
	}


	@Test
	public void test3LongSplit_Sell_Trades() {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		// int count, Double targetContracts, Double actualContracts, Double virtualContracts, Double contractValue)
		list.add(setupReplication(1, 33.6, 37.0, 0.0, 90548.0));
		list.add(setupReplication(2, 57.43, 64.0, 0.0, 129289.0));
		list.add(setupReplication(3, 15.23, 17.0, 0.0, 191989.0));

		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 37, null));
		positionAllocationList.add(setupPositionAllocation(3, 17, null));

		// SELL 13: Split -4, -7, -2
		List<TradeAssetClassPositionAllocation> tradeAllocationList = new ArrayList<>();
		// int acId, Integer repId, int allocationQuantity, Integer tradeId, Integer totalTradeQuantity, Boolean pending) {
		tradeAllocationList.add(setupTradePositionAllocation(1, null, -4, 5, -13, true));
		tradeAllocationList.add(setupTradePositionAllocation(2, null, -7, 5, -13, true));
		tradeAllocationList.add(setupTradePositionAllocation(3, null, -2, 5, -13, true));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.PENDING;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, tradeAllocationList, BigDecimal.valueOf(-13));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				//validateExpected(37, rep.getCurrentContracts(), type.name());
				validateExpected(-4, rep.getPendingContracts(), type.name());
			}
			else if ("2_2".equals(config.getKeyForReplication(rep))) {
				//validateExpected(64, rep.getCurrentContracts(), type.name());
				validateExpected(-7, rep.getPendingContracts(), type.name());
			}
			else {
				//validateExpected(17, rep.getCurrentContracts(), type.name());
				validateExpected(-2, rep.getPendingContracts(), type.name());
			}
		}
	}


	/**
	 * For rolls, logic should always try to allocate the same proportion in the Sells and the Buys to the same asset classes.
	 */
	@Test
	public void test2LongSplit_Roll_NotExplicit() {
		List<ProductOverlayAssetClassReplication> rollFromList = new ArrayList<>();
		rollFromList.add(setupReplication(1, 1, 45.29, 46.11, 46.0, true, null, 48733.0));
		rollFromList.add(setupReplication(2, 2, 17.3, 17.04, 7.0, true, null, 48733.0));

		List<ProductOverlayAssetClassReplication> rollToList = new ArrayList<>();
		rollToList.add(setupReplication(1, 1, 45.29, -0.82, 0.0, true, null, 48733.0));
		rollToList.add(setupReplication(2, 2, 17.3, 0.26, 0.0, true, null, 48733.0));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.PENDING;

		// Roll 10: Split 10, 0
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, rollFromList, null, null, BigDecimal.valueOf(-10));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : rollFromList) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-10, rep.getPendingContracts(), type.name());
			}
			else {
				validateExpected(0, rep.getPendingContracts(), type.name());
			}
		}

		config = new ProductReplicationPositionAllocationConfig(type, rollToList, null, null, BigDecimal.valueOf(10));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : rollToList) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(10, rep.getPendingContracts(), type.name());
			}
			else {
				validateExpected(0, rep.getPendingContracts(), type.name());
			}
		}

		// Try again - but this time process as Current - Pending Trades have been booked
		type = PortfolioAccountContractStoreTypes.CURRENT;
		// Reset the lists
		rollFromList = new ArrayList<>();
		rollFromList.add(setupReplication(1, 1, 45.29, 46.11, 46.0, false, null, 48733.0));
		rollFromList.add(setupReplication(2, 2, 17.3, 17.04, 7.0, false, null, 48733.0));

		rollToList = new ArrayList<>();
		rollToList.add(setupReplication(1, 1, 45.29, -0.82, 0.0, false, null, 48733.0));
		rollToList.add(setupReplication(2, 2, 17.3, 0.26, 0.0, false, null, 48733.0));

		config = new ProductReplicationPositionAllocationConfig(type, rollFromList, null, null, BigDecimal.valueOf(43));
		config.process();
		// Verify Current Split
		for (ProductOverlayAssetClassReplication rep : rollFromList) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				// Reduced by 10
				validateExpected(36, rep.getCurrentContracts(), type.name());
			}
			else {
				// Not changed
				validateExpected(7, rep.getCurrentContracts(), type.name());
			}
		}

		config = new ProductReplicationPositionAllocationConfig(type, rollToList, null, null, BigDecimal.valueOf(10));
		config.process();
		// Verify Current Split
		for (ProductOverlayAssetClassReplication rep : rollToList) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				// Set to 10
				validateExpected(10, rep.getCurrentContracts(), type.name());
			}
			else {
				// Not changed
				validateExpected(0, rep.getCurrentContracts(), type.name());
			}
		}
	}


	@Test
	public void test2LongSplit_Roll_From_Explicit() {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		// 1. Target = 46.6, Actual = 46
		list.add(setupReplication(1, 46.6, 46.0, null, 48566.0));
		// 2. Target = 7.09, Actual = 7
		list.add(setupReplication(2, 7.09, 7.0, null, 48566.0));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.PENDING;

		// 1. Was Explicit as 46 Before
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 46, null));

		// Roll (i.e. Sell 16): Split -14, -2
		List<TradeAssetClassPositionAllocation> tradeAllocationList = new ArrayList<>();
		tradeAllocationList.add(setupTradePositionAllocation(1, null, -14, 5, -16, true));


		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, tradeAllocationList, BigDecimal.valueOf(-16));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-14, rep.getPendingContracts(), type.name());
			}
			else {
				validateExpected(-2, rep.getPendingContracts(), type.name());
			}
		}

		// Try again - but this time process as Current - Pending Trades have been booked
		type = PortfolioAccountContractStoreTypes.CURRENT;
		// Mark as processed just so it pulls through as "current" vs "pending" allocation
		tradeAllocationList = new ArrayList<>();
		tradeAllocationList.add(setupTradePositionAllocation(1, null, -14, 5, -16, false));

		// Reset the list
		list = new ArrayList<>();
		list.add(setupReplication(1, 1, 46.6, 46.6, 46.0, false, null, 48566.0));
		list.add(setupReplication(2, 2, 7.09, 7.09, 7.0, false, null, 48566.0));

		config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, tradeAllocationList, BigDecimal.valueOf(37));
		config.process();
		// Verify Current Split
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(32, rep.getCurrentContracts(), type.name());
				//System.out.println("1 - " + CoreMathUtils.formatNumberMoney(rep.getCurrentContracts()) + ", " + CoreMathUtils.formatNumberMoney(rep.getCurrentVirtualContracts()));
			}
			else {
				//System.out.println("2 - " + CoreMathUtils.formatNumberMoney(rep.getCurrentContracts()) + ", " + CoreMathUtils.formatNumberMoney(rep.getCurrentVirtualContracts()));
				validateExpected(5, rep.getCurrentContracts(), type.name());
			}
		}
	}


	@Test
	public void test2LongSplit_Roll_Explicit() {
		List<ProductOverlayAssetClassReplication> rollFromList = new ArrayList<>();
		rollFromList.add(setupReplication(1, 1, 45.52, 46.6, 46.0, true, null, 48566.0));
		rollFromList.add(setupReplication(2, 2, 17.3, 7.09, 7.0, true, null, 48566.0));

		List<ProductOverlayAssetClassReplication> rollToList = new ArrayList<>();
		rollToList.add(setupReplication(1, 1, 45.52, -1.08, 0.0, true, null, 48600.0));
		rollToList.add(setupReplication(2, 2, 17.3, 10.21, 10.0, true, null, 48600.0));

		// 1. Was Explicit as 46 Before
		List<InvestmentAccountAssetClassPositionAllocation> fromPositionAllocationList = new ArrayList<>();
		fromPositionAllocationList.add(setupPositionAllocation(1, 46, null));

		// Roll 16: Split 14, 2
		List<TradeAssetClassPositionAllocation> fromTradeAllocationList = new ArrayList<>();
		fromTradeAllocationList.add(setupTradePositionAllocation(1, null, -14, 5, -16, true));
		List<TradeAssetClassPositionAllocation> toTradeAllocationList = new ArrayList<>();
		toTradeAllocationList.add(setupTradePositionAllocation(1, null, 14, 6, 16, true));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.PENDING;

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, rollFromList, fromPositionAllocationList, fromTradeAllocationList, BigDecimal.valueOf(-16));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : rollFromList) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-14, rep.getPendingContracts(), type.name());
			}
			else {
				validateExpected(-2, rep.getPendingContracts(), type.name());
			}
		}

		config = new ProductReplicationPositionAllocationConfig(type, rollToList, null, toTradeAllocationList, BigDecimal.valueOf(16));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : rollToList) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(14, rep.getPendingContracts(), type.name());
			}
			else {
				validateExpected(2, rep.getPendingContracts(), type.name());
			}
		}

		// Try again - but this time process as Current - Pending Trades have been booked
		type = PortfolioAccountContractStoreTypes.CURRENT;
		fromTradeAllocationList = new ArrayList<>();
		fromTradeAllocationList.add(setupTradePositionAllocation(1, null, -14, 5, -16, false));
		toTradeAllocationList = new ArrayList<>();
		toTradeAllocationList.add(setupTradePositionAllocation(1, null, 14, 6, 16, false));


		// Reset the lists
		rollFromList = new ArrayList<>();
		rollFromList.add(setupReplication(1, 1, 45.52, 46.6, 46.0, false, null, 48566.0));
		rollFromList.add(setupReplication(2, 2, 17.3, 7.09, 7.0, false, null, 48566.0));

		rollToList = new ArrayList<>();
		rollToList.add(setupReplication(1, 1, 45.52, -1.08, 0.0, false, null, 48600.0));
		rollToList.add(setupReplication(2, 2, 17.3, 10.21, 10.0, false, null, 48600.0));

		config = new ProductReplicationPositionAllocationConfig(type, rollFromList, fromPositionAllocationList, fromTradeAllocationList, BigDecimal.valueOf(37));
		config.process();
		// Verify Current Split
		for (ProductOverlayAssetClassReplication rep : rollFromList) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				// Reduced by 14
				validateExpected(32, rep.getCurrentContracts(), type.name());
			}
			else {
				// Reduced by 2
				validateExpected(5, rep.getCurrentContracts(), type.name());
			}
		}

		config = new ProductReplicationPositionAllocationConfig(type, rollToList, null, toTradeAllocationList, BigDecimal.valueOf(26));
		config.process();
		// Verify Current Split
		for (ProductOverlayAssetClassReplication rep : rollToList) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				// Set to 14
				validateExpected(14, rep.getCurrentContracts(), type.name());
			}
			else {
				// Original 10 + 2 new
				validateExpected(12, rep.getCurrentContracts(), type.name());
			}
		}
	}


	@Test
	public void test2ShortSplit_Buy() {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		// 1. Target = -2128.98, Actual = -2,138
		list.add(setupReplication(1, -2128.98, -2138.0, null, 101011.0));
		// 2. Target = 103.85, Actual = 99
		list.add(setupReplication(2, -24.37, -24.0, null, 101011.0));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.PENDING;

		// BUY 170: Split 170, 0
		List<TradeAssetClassPositionAllocation> tradeAllocationList = new ArrayList<>();
		tradeAllocationList.add(setupTradePositionAllocation(1, 170));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, tradeAllocationList, BigDecimal.valueOf(170));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(170, rep.getPendingContracts(), type.name());
			}
			else {
				validateExpected(0, rep.getPendingContracts(), type.name());
			}
		}
	}


	@Test
	public void test2LongAndShortSplit_Buy() {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		// 1. Target = 223.11, Actual = 192, Virtual = 9
		list.add(setupReplication(1, 223.11, 192.0, 9.0, 47892.0));
		// 2. Target = -9.87, Actual = 0, Virtual = -9
		list.add(setupReplication(2, -9.87, 0.0, -9.0, 47892.0));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.PENDING;

		// BUY 15: Split 15, 0
		List<TradeAssetClassPositionAllocation> tradeAllocationList = new ArrayList<>();
		tradeAllocationList.add(setupTradePositionAllocation(1, 15));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, tradeAllocationList, BigDecimal.valueOf(15));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(15, rep.getPendingContracts(), type.name());
			}
			else {
				validateExpected(0, rep.getPendingContracts(), type.name());
			}
		}
	}


	@Test
	public void test_LongAndShortSplit_RollToSell_AndBuyMore() {
		// 126570 Covenant 6/12 After Trading Roll and 2 Buys allocated in different asset classes
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		// 1. Domestic Equity Large Cap Target = -3.75, Actual = 0
		list.add(setupReplication(1, -3.75, 0.0, null, 105527.0));
		// 2. Global Equity Target = 4.26, Actual = 0
		list.add(setupReplication(2, 4.26, 0.0, null, 105527.0));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.PENDING;

		// 1. New Splits Because of Rolling
		// BUY 25: Domestic Equity Large Cap
		List<TradeAssetClassPositionAllocation> tradeAllocationList = new ArrayList<>();
		tradeAllocationList.add(setupTradePositionAllocation(1, null, 25, 5, 25, true)); // Marked as Trade Id #5, with total quantity of 25
		// Another BUY of 25 Domestic Equity Large Cap
		tradeAllocationList.add(setupTradePositionAllocation(1, 25));
		// BUY 7 Global Equity (To Roll Trade Actual Roll was -145)
		tradeAllocationList.add(setupTradePositionAllocation(2, null, 7, 6, -145, true)); // Marked as Trade Id #6 with full trade quantity of -145

		// Total Quantity for Pending is -95 (-145 Roll, Buy 25, Buy 25)
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, tradeAllocationList, BigDecimal.valueOf(-95));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-95, rep.getPendingContracts(), type.name());
				validateExpected(-7, rep.getPendingVirtualContracts(), PortfolioAccountContractStoreTypes.VIRTUAL_PENDING.name());
			}
			else {
				validateExpected(0, rep.getPendingContracts(), type.name());
				validateExpected(7, rep.getPendingVirtualContracts(), PortfolioAccountContractStoreTypes.VIRTUAL_PENDING.name());
			}
		}
	}


	@Test
	public void test2LongSplit_SameAssetClass_Sell() {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		// 1. Primary Rep: Target = 103.64, Actual = 104
		list.add(setupReplication(1, 1, 103.64, 103.64, 104.0, true, null, 47878.0));
		// 2. Secondary Rep Target = 567.41, Actual = 588
		list.add(setupReplication(1, 2, 567.41, 567.41, 588.0, true, null, 47878.0));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.PENDING;

		// Secondary Rep - Was Explicit as 588 Before
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 2, 588, null));

		// SELL 15: Split 3, -18
		List<TradeAssetClassPositionAllocation> tradeAllocationList = new ArrayList<>();
		tradeAllocationList.add(setupTradePositionAllocation(1, 1, 3));
		tradeAllocationList.add(setupTradePositionAllocation(1, 2, -18));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, tradeAllocationList, BigDecimal.valueOf(-15));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getPendingContracts(), type.name());
				validateExpected(3, rep.getPendingVirtualContracts(), type.name());
			}
			else {
				validateExpected(-15, rep.getPendingContracts(), type.name());
				validateExpected(-3, rep.getPendingVirtualContracts(), type.name());
			}
		}
	}


	@Test
	public void test3VirtualTradeSplit() {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		// 1. Target = 0.35, Actual = 1
		list.add(setupReplication(1, 0.35, 1.0, null, 148977.0));
		// 2. Target = 0, Actual = 0
		list.add(setupReplication(2, 0.0, 0.0, null, 281479.0));
		// 3. Target = 1.55, Actual = 1
		list.add(setupReplication(3, 1.55, 1.0, null, 119411.0));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.PENDING;

		// 1. Was Explicit as 1 Before, 2. Was Explicit as 0 Before
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 1, null));
		positionAllocationList.add(setupPositionAllocation(2, 0, null));

		// Virtual Trade: -1, 1, 0
		List<TradeAssetClassPositionAllocation> tradeAllocationList = new ArrayList<>();
		tradeAllocationList.add(setupTradePositionAllocation(1, -1));
		tradeAllocationList.add(setupTradePositionAllocation(2, 1));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, tradeAllocationList, BigDecimal.valueOf(0));
		config.process();
		// Verify Pending Split
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getPendingContracts(), type.name());
				validateExpected(-1, rep.getPendingVirtualContracts(), type.getDependentType().name());
			}
			else if ("2_2".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getPendingContracts(), type.name());
				validateExpected(1, rep.getPendingVirtualContracts(), type.getDependentType().name());
			}
			else {
				validateExpected(0, rep.getPendingContracts(), type.name());
				validateExpected(0, rep.getPendingVirtualContracts(), type.getDependentType().name());
			}
		}
	}


	///////////////////////////////////////////////////////
	// Helper Methods
	///////////////////////////////////////////////////////


	private void validateExpected(Integer expected, BigDecimal actual, String fieldName) {
		if (expected == 0) {
			Assertions.assertTrue(MathUtils.isNullOrZero(actual),
					"Expected [" + expected + " " + fieldName + "] contracts, but instead got " + (actual == null ? "NULL" : CoreMathUtils.formatNumberInteger(actual)));
		}
		else {
			Assertions.assertTrue(MathUtils.isEqual(actual, BigDecimal.valueOf(expected)),
					"Expected [" + expected + " " + fieldName + "] contracts, but instead got " + (actual == null ? "NULL" : CoreMathUtils.formatNumberInteger(actual)));
		}
	}


	private ProductOverlayAssetClassReplication setupReplication(int count, Double targetContracts, Double actualContracts, Double virtualContracts, Double contractValue) {
		return setupReplication(count, count, targetContracts, targetContracts, actualContracts, true, virtualContracts, contractValue);
	}


	private ProductOverlayAssetClassReplication setupReplication(int acCount, int repCount, Double targetContracts, Double targetContractsAdjusted, Double actualContracts, boolean setCurrent, Double virtualContracts, Double contractValue) {
		ProductOverlayAssetClassReplication rep = new ProductOverlayAssetClassReplication();
		rep.setTargetExposure(BigDecimal.valueOf((targetContracts * contractValue)));
		rep.setTargetExposureAdjusted(BigDecimal.valueOf((targetContractsAdjusted * contractValue)));
		rep.setValue(BigDecimal.valueOf(contractValue));
		rep.setActualContracts(BigDecimal.valueOf(actualContracts));
		if (setCurrent) {
			rep.setCurrentContracts(rep.getActualContracts());
		}
		if (virtualContracts != null) {
			rep.setVirtualContracts(BigDecimal.valueOf(virtualContracts));
			if (setCurrent) {
				rep.setCurrentVirtualContracts(rep.getVirtualContracts());
			}
		}

		InvestmentReplication ir = new InvestmentReplication();
		ir.setId(repCount);
		InvestmentReplicationType rt = new InvestmentReplicationType();
		ir.setType(rt);
		rep.setReplication(ir);

		ProductOverlayAssetClass oac = new ProductOverlayAssetClass();
		oac.setAccountAssetClass(setupAccountAssetClass(acCount));
		rep.setOverlayAssetClass(oac);

		return rep;
	}


	private InvestmentAccountAssetClass setupAccountAssetClass(int id) {
		InvestmentAccountAssetClass iac = new InvestmentAccountAssetClass();
		InvestmentAssetClass c = new InvestmentAssetClass();
		c.setName("Asset Class: " + id);
		iac.setId(id);
		iac.setAssetClass(c);
		return iac;
	}


	private InvestmentAccountAssetClassPositionAllocation setupPositionAllocation(int id, int allocationQuantity, Integer order) {
		return setupPositionAllocation(id, null, allocationQuantity, order);
	}


	private InvestmentAccountAssetClassPositionAllocation setupPositionAllocation(int acId, Integer repId, int allocationQuantity, Integer order) {
		InvestmentAccountAssetClassPositionAllocation alloc = new InvestmentAccountAssetClassPositionAllocation();
		alloc.setAccountAssetClass(setupAccountAssetClass(acId));
		if (repId != null) {
			InvestmentReplication rep = new InvestmentReplication();
			rep.setId(repId);
			alloc.setReplication(rep);
		}
		alloc.setAllocationQuantity(BigDecimal.valueOf(allocationQuantity));
		alloc.setAllocationOrder(order == null ? null : order.shortValue());
		return alloc;
	}


	private TradeAssetClassPositionAllocation setupTradePositionAllocation(int id, int allocationQuantity) {
		return setupTradePositionAllocation(id, null, allocationQuantity);
	}


	private TradeAssetClassPositionAllocation setupTradePositionAllocation(int acId, Integer repId, int allocationQuantity) {
		return setupTradePositionAllocation(acId, repId, allocationQuantity, null, null, null);
	}


	private TradeAssetClassPositionAllocation setupTradePositionAllocation(int acId, Integer repId, int allocationQuantity, Integer tradeId, Integer totalTradeQuantity, Boolean pending) {
		TradeAssetClassPositionAllocation alloc = new TradeAssetClassPositionAllocation();
		alloc.setAccountAssetClass(setupAccountAssetClass(acId));
		if (repId != null) {
			InvestmentReplication rep = new InvestmentReplication();
			rep.setId(repId);
			alloc.setReplication(rep);
		}

		alloc.setAllocationQuantity(BigDecimal.valueOf(allocationQuantity));
		if (tradeId != null) {
			alloc.setTrade(new Trade());
			alloc.getTrade().setId(tradeId);
			alloc.getTrade().setQuantityIntended(MathUtils.abs(BigDecimal.valueOf(totalTradeQuantity)));
			alloc.getTrade().setBuy(MathUtils.isGreaterThan(BigDecimal.valueOf(totalTradeQuantity), BigDecimal.ZERO));
			alloc.getTrade().setWorkflowState(new WorkflowState());
			alloc.getTrade().setWorkflowStatus(new WorkflowStatus());
			if (pending == null) {
				alloc.getTrade().getWorkflowState().setName(TradeService.TRADES_CANCELLED_STATE_NAME);
				alloc.getTrade().getWorkflowStatus().setName(TradeService.TRADES_CLOSED_STATUS_NAME);
			}
			else if (pending) {
				alloc.getTrade().getWorkflowState().setName("Approved");
				alloc.getTrade().getWorkflowStatus().setName(WorkflowStatus.STATUS_APPROVED);
			}
			else {
				alloc.getTrade().getWorkflowState().setName(TradeService.TRADE_BOOKED_STATE_NAME);
				alloc.getTrade().getWorkflowStatus().setName(TradeService.TRADES_CLOSED_STATUS_NAME);
			}
		}
		else if (pending != null) {
			alloc.setProcessed(!pending);
		}
		return alloc;
	}
}
