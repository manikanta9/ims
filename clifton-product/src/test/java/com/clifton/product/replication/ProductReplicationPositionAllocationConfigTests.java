package com.clifton.product.replication;


import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.portfolio.account.PortfolioAccountContractStoreTypes;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>ProductReplicationPositionAllocationConfigTests</code> tests methods in the {@link ProductReplicationPositionAllocationConfig}
 *
 * @author manderson
 */
public class ProductReplicationPositionAllocationConfigTests {

	////////////////////////////////////////////////////////////
	// Virtual Contract Quantity Generator Tests
	////////////////////////////////////////////////////////////


	@Test
	public void testGenerateVirtualContractQuantity() {
		validateVirtualContractQuantity(0, null, null, null);
		validateVirtualContractQuantity(0, -6.74, null, null);
		validateVirtualContractQuantity(0, null, 136.74, null);
		validateVirtualContractQuantity(16, -16.74, 19.34, null);
		validateVirtualContractQuantity(0, 0.0, 96.34, null);
		validateVirtualContractQuantity(5, -76.74, 5.34, null);
		validateVirtualContractQuantity(0, -76.74, -4.66, null);
		validateVirtualContractQuantity(4, -76.74, 4.66, null);
		validateVirtualContractQuantity(18, -22.0, 21.0, -18.0);
		validateVirtualContractQuantity(2, -2.0, 2.0, 0.0);
	}


	private void validateVirtualContractQuantity(Integer expected, Double shortTarget, Double longTarget, Double forceTarget) {
		BigDecimal exp = (expected == null ? null : BigDecimal.valueOf(expected));
		BigDecimal st = (shortTarget == null ? null : BigDecimal.valueOf(shortTarget));
		BigDecimal lt = (longTarget == null ? null : BigDecimal.valueOf(longTarget));
		BigDecimal ft = (forceTarget == null ? null : BigDecimal.valueOf(forceTarget));
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(PortfolioAccountContractStoreTypes.VIRTUAL, null, null, null);
		config.process();
		BigDecimal virtual = config.generateVirtualContractQuantity(st, lt, ft);
		Assertions.assertTrue(MathUtils.isEqual(exp, MathUtils.round(virtual, 0)), "Expected Virtual Contract Quantity of [" + expected + "] for Short/Long/Forced Quantity values of [" + shortTarget + ", " + longTarget + ", " + forceTarget
				+ " but instead returned value [" + virtual + "].");
	}


	////////////////////////////////////////////////////////////
	// Process Allocation Tests
	////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////
	// LONG SPLITS
	////////////////////////////////////////////////////////////


	@Test
	public void test2LongSplit_SameDirection_Extra() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(true, true, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(100));
		config.process();
		// Only Need 60, so split should be 20/80
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(20, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(80, rep.getActualContracts(), type.name());
			}
		}


		// Now Process CURRENT with less - but still enough
		PortfolioAccountContractStoreTypes currentType = PortfolioAccountContractStoreTypes.CURRENT;
		config = new ProductReplicationPositionAllocationConfig(currentType, list, null, BigDecimal.valueOf(80));
		config.process();
		// Only Need 60, so actual split is still 20/80, but current split is 20/60
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(20, rep.getActualContracts(), type.name());
				validateExpected(20, rep.getCurrentContracts(), currentType.name());
			}
			else {
				validateExpected(80, rep.getActualContracts(), type.name());
				validateExpected(60, rep.getCurrentContracts(), currentType.name());
			}
		}
	}


	@Test
	public void test2LongSplit_SameDirection_Rounding() {

		// Case where smaller target rounds up to next integer
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(540.14, 20.7, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(566));
		config.process();

		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(546, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(20, rep.getActualContracts(), type.name());
			}
		}

		// Case where smaller target rounds up to next integer
		list = setupTestListByContracts(437.15, 8.74, null);
		config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(427));
		config.process();

		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(419, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(8, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test3LongSplit_SameDirection_Extra() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(true, true, true);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(100));
		config.process();
		// Only Need 87, so split should be 20/30/50
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(20, rep.getActualContracts(), type.name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(30, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(50, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test2LongSplit_SameDirection_NotEnough() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(true, true, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(50));
		config.process();
		// Need 60 - Have 50, so split should be 20/30
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(20, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(30, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test3LongSplit_SameDirection_NotEnough() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(true, true, true);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(40));
		config.process();
		// Need 87, have 40 so split should be 20/20/0
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(20, rep.getActualContracts(), type.name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(20, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(0, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test3LongSplit_SameDirection_Extra_WithExplicit() {
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(45.08, 71.0, 51.10);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 45, null));
		positionAllocationList.add(setupPositionAllocation(2, 71, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(328));
		config.process();
		// split should be 45/71/212
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(45, rep.getActualContracts(), type.name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(212, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(71, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test2LongSplit_SameDirection_Extra_WithExplicit() {
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(23.46, 30.27, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 45, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(170));
		config.process();
		// split should be 45/125
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(45, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(125, rep.getActualContracts(), type.name());
			}
		}
	}


	////////////////////////////////////////////////////////////
	// SHORT SPLITS
	////////////////////////////////////////////////////////////


	@Test
	public void test2ShortSplit_SameDirection_Extra() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(false, false, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(-100));
		config.process();
		// Only Need -60, so split should be -20/-80
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-20, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-80, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test2ShortSplit_SameDirection_Rounding() {

		// Case where smaller target rounds up to next integer
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(-540.14, -20.7, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(-566));
		config.process();

		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-546, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-20, rep.getActualContracts(), type.name());
			}
		}

		// Case where smaller target rounds up to next integer
		list = setupTestListByContracts(-437.15, -8.74, null);
		config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(-427));
		config.process();

		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-419, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-8, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test2ShortSplit_SameDirection_Extra_WithExplicit() {
		// account 495000 on 3/6
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(-40.13, -90.76, null);

		// Explicit Split of -90 to asset Class #2
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(2, -90, null));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-133));
		config.process();

		// Split should be -90/-43
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-43, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-90, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test2ShortSplit_SameDirection_Extra_WithExplicit_2() {
		// CDM4 Matching Currency Rep Splits for 667600 on 4/25
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(-41.68, -0.77, null);

		// Explicit Split of -42 to asset Class #1
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, -42, null));

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-43));
		config.process();

		// Split should be -42/-1
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-42, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-1, rep.getActualContracts(), type.name());
			}
		}

		// Try again with explicit on the other asset class
		list = setupTestListByContracts(-41.68, -0.77, null);

		// Explicit Split of -42 to asset Class #1
		positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, -42, null));

		config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-43));
		config.process();

		// Split should be -42/-1
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-42, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-1, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test3ShortSplit_SameDirection_Extra() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(false, false, false);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(-100));
		config.process();
		// Only Need -87, so split should be -20/-30/-50
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-20, rep.getActualContracts(), type.name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(-30, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-50, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test2ShortSplit_SameDirection_NotEnough() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(false, false, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(-50));
		config.process();
		// Need -60 - Have -50, so split should be -20/-30
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-20, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-30, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test3ShortSplit_SameDirection_NotEnough() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(false, false, false);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(-40));
		config.process();
		// Need -87, have -40 so split should be -20/-20/0
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-20, rep.getActualContracts(), type.name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(-20, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(0, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test3ShortSplit_SameDirection_OverrideTargetAndOrder() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(false, false, false);
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(3, -40, 1));
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-40));
		config.process();
		// Need -87, have -40, but #3 is allocated first with -40 so it gets it all
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(-40, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(0, rep.getActualContracts(), type.name());
			}
		}
	}


	/**
	 * Should result in the same as above test
	 * - asset class assigned get's result and the others get the rest
	 */
	@Test
	public void test3ShortSplit_SameDirection_OverrideTargetNotOrder() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(false, false, false);
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(3, -40, null));
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-40));
		config.process();
		// Need -87, have -40, but #3 is allocated first with -40 so it gets it all
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(-40, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(0, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test3ShortSplit_SameDirection_OverrideTargetAndOneOrder() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(false, false, false);
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(3, -40, 1));
		positionAllocationList.add(setupPositionAllocation(2, -40, 2));
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-90));
		config.process();
		// Need -100, have -90, but #3 is allocated first with -40, #2 second with -40, and #1 gets rest -10
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-10, rep.getActualContracts(), type.name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(-40, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-40, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test3ShortSplit_SameDirection_Extra_WithExplicit() {
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(-45.08, -71.0, -51.10);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, -45, null));
		positionAllocationList.add(setupPositionAllocation(2, -71, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-328));
		config.process();
		// split should be -45/-71/-212
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-45, rep.getActualContracts(), type.name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(-212, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-71, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test2ShortSplit_SameDirection_Extra_WithExplicit_3() {
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(-23.46, -30.27, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, -45, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-170));
		config.process();
		// split should be -45/-125
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-45, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-125, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void test2ShortSplit_OneLongTargetOneShortTarget_Explicit() {
		// Ventura 5/20 MOC: Example 1, CDM4: Rep 1 Target: -43.78, Explicit Actual: -42, Rep 2 Target: 5.12, Remainder Actual: -43
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(-43.78, 5.12, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, -42, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-43));
		config.process();
		// split should be -42/-1
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-42, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-1, rep.getActualContracts(), type.name());
			}
		}

		// Switch Explicit and Try again
		list = setupTestListByContracts(-43.78, 5.12, null);
		positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(2, -1, null));

		config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-43));
		config.process();
		// split should be -42/-1
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-42, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-1, rep.getActualContracts(), type.name());
			}
		}

		// Ventura 5/20 MOC: Example 2, PTM4: Rep 1 Target: -19.32, Explicit Actual: -18, Rep 2 Target: 3.07, Remainder Actual: -1
		list = setupTestListByContracts(-19.32, 3.07, null);
		positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, -18, null));

		config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-19));
		config.process();
		// split should be -18/-1
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-18, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(-1, rep.getActualContracts(), type.name());
			}
		}
	}


	///////////////////////////////////////////////////////
	// Hold None, but Target Long & Short (Virtual)
	// NOTE: CALLS METHOD FOR VIRTUAL SPLIT ONLY _ SAME AS CODE DOES
	///////////////////////////////////////////////////////


	@Test
	public void test2Split_DifferentDirection_HoldNone() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(true, false, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, null);
		config.process();
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(20, rep.getVirtualContracts(), "VIRTUAL");
			}
			else {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-20, rep.getVirtualContracts(), "VIRTUAL");
			}
		}
	}


	@Test
	public void test2Split_DifferentDirection_HoldNone_Explicit() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(true, false, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(2, -18, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, null);
		config.process();
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(18, rep.getVirtualContracts(), "VIRTUAL");
			}
			else {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-18, rep.getVirtualContracts(), "VIRTUAL");
			}
		}
	}


	@Test
	public void test4Split_DifferentDirection_HoldLong_Explicit() {
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(0.0, 49.63, -2.75, 5.51);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(2, 52, null));
		positionAllocationList.add(setupPositionAllocation(4, 4, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(53.0));
		config.process();
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(0, rep.getVirtualContracts(), "VIRTUAL");
			}
			else if ("2_2".equals(config.getKeyForReplication(rep))) {
				validateExpected(49, rep.getActualContracts(), type.name());
				validateExpected(3, rep.getVirtualContracts(), "VIRTUAL");
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-3, rep.getVirtualContracts(), "VIRTUAL");
			}
			else {
				validateExpected(4, rep.getActualContracts(), type.name());
			}
		}
	}


	///////////////////////////////////////////////////////
	// Hold Long, but Target Long & Short (Virtual)
	///////////////////////////////////////////////////////


	@Test
	public void test3Split_DifferentDirection_Multiple() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(true, true, false);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(27));
		config.process();

		// Need 27, have 27 - all targets fulfilled using virtual
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(20, rep.getActualContracts(), type.name());
				validateExpected(0, rep.getVirtualContracts(), "VIRTUAL");
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-30, rep.getVirtualContracts(), "VIRTUAL");
			}
			else {
				validateExpected(7, rep.getActualContracts(), type.name());
				validateExpected(30, rep.getVirtualContracts(), "VIRTUAL");
			}
		}
	}


	@Test
	public void test3Split_DifferentDirection_Explicit() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(true, true, false);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 25, 2));
		positionAllocationList.add(setupPositionAllocation(2, 35, 1));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(30));
		config.process();
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(25, rep.getVirtualContracts(), "VIRTUAL");
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-30, rep.getVirtualContracts(), "VIRTUAL");
			}
			else {
				validateExpected(30, rep.getActualContracts(), type.name());
				validateExpected(5, rep.getVirtualContracts(), "VIRTUAL");
			}
		}
	}


	@Test
	public void test3Split_DifferentDirection_ExplicitNotEnough() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(true, true, false);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 25, 2));
		positionAllocationList.add(setupPositionAllocation(2, 35, 1));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(18));
		config.process();
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(25, rep.getVirtualContracts(), "VIRTUAL");
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-42, rep.getVirtualContracts(), "VIRTUAL");
			}
			else {
				validateExpected(18, rep.getActualContracts(), type.name());
				validateExpected(17, rep.getVirtualContracts(), "VIRTUAL");
			}
		}
	}


	@Test
	public void test3Split_DifferentDirection_ExplicitNotEnoughInVirtualDirection() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(true, true, false);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(3, -30, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(18));
		config.process();
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(18, rep.getActualContracts(), type.name());
				validateExpected(2, rep.getVirtualContracts(), "VIRTUAL");
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-30, rep.getVirtualContracts(), "VIRTUAL");
			}
			else {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(28, rep.getVirtualContracts(), "VIRTUAL");
			}
		}
	}


	@Test
	public void test3Split_DifferentDirection_NotEnough() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(true, true, false);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(18));
		config.process();
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(18, rep.getActualContracts(), type.name());
				validateExpected(2, rep.getVirtualContracts(), "VIRTUAL");
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-30, rep.getVirtualContracts(), "VIRTUAL");
			}
			else {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(28, rep.getVirtualContracts(), "VIRTUAL");
			}
		}
	}


	///////////////////////////////////////////////////////
	// Hold Long, but Target Short
	///////////////////////////////////////////////////////


	@Test
	public void test3ShortSplit_HoldLong() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(false, false, false);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(100));
		config.process();
		// Need -87, have 100 so split should be 100/0/0
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(100, rep.getActualContracts(), type.name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(0, rep.getActualContracts(), type.name());
			}
		}
	}


	///////////////////////////////////////////////////////
	// Hold Short, but Target Long
	///////////////////////////////////////////////////////


	@Test
	public void test3LongSplit_HoldShort() {
		List<ProductOverlayAssetClassReplication> list = setupTestList(true, true, true);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;
		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(-100));
		config.process();
		// Need 87, have -100 so split should be -100/0/0
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(-100, rep.getActualContracts(), type.name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(0, rep.getActualContracts(), type.name());
			}
		}
	}


	///////////////////////////////////////////////////////
	// Hold Short, but Target Long/Short Splits - Explicit
	///////////////////////////////////////////////////////


	@Test
	public void test2DifferentDirection_HoldShort_ExplicitLong() {
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(21.38, -95.65, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 10, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-103));
		config.process();
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(10, rep.getVirtualContracts(), "VIRTUAL");
			}
			else {
				validateExpected(-103, rep.getActualContracts(), type.name());
				validateExpected(-10, rep.getVirtualContracts(), "VIRTUAL");
			}
		}
	}


	@Test
	public void test2DifferentDirection_HoldShort_ExplicitShort() {
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(21.38, -95.65, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(2, -113, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-103));
		config.process();
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(10, rep.getVirtualContracts(), "VIRTUAL");
			}
			else {
				validateExpected(-103, rep.getActualContracts(), type.name());
				validateExpected(-10, rep.getVirtualContracts(), "VIRTUAL");
			}
		}
	}


	@Test
	public void test2Long_SameAssetClass() {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		ProductOverlayAssetClassReplication rep1 = setupReplication(1, 155 * 5.0, 5.0);
		ProductOverlayAssetClassReplication rep2 = setupReplication(2, 22 * 5.0, 5.0);
		// Give second replication the same asset class as the first one
		rep2.setOverlayAssetClass(rep1.getOverlayAssetClass());
		list.add(rep1);
		list.add(rep2);

		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		InvestmentAccountAssetClassPositionAllocation alloc = setupPositionAllocation(1, 153, null);
		alloc.setReplication(rep1.getReplication());
		positionAllocationList.add(alloc);

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(174));
		config.process();
		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(153, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(21, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void testSplit3_WithExplicitZero() {
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(1.0, 0.0, 0.0);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 1, null));
		positionAllocationList.add(setupPositionAllocation(3, 0, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(5));
		config.process();

		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(1, rep.getActualContracts(), type.name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
			}

			else {
				validateExpected(4, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void testSplit4_WithVirtual_ExplicitWithWrongTargets() {
		// 576700 Sonoma County 5/29 RTAM4
		// Test case for PRODUCT-273: Issue with Contract Splitting and Minimize Imbalances
		// Issue is the one that isn't explicit originally had a bad target (was long, but we are actually giving it short contracts)
		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(49.0, 1.0, 1.31, 1.0);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 49, null));
		positionAllocationList.add(setupPositionAllocation(2, 1, null));
		positionAllocationList.add(setupPositionAllocation(4, 1, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(50));
		config.process();

		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(48, rep.getActualContracts(), type.name());
				validateExpected(1, rep.getVirtualContracts(), type.getDependentType().name());
			}
			else if ("2_2".equals(config.getKeyForReplication(rep))) {
				validateExpected(1, rep.getActualContracts(), type.name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-1, rep.getVirtualContracts(), type.getDependentType().name());
			}
			else {
				validateExpected(1, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void testSplit4_WithVirtual_ExplicitWithVirtualForceTargetZero() {
		// 576700 Sonoma County 6/09 ESM4
		// ESM4 - actual applied, but Virtual gets Short Target = -2, Long Target = 2, Force Target = 0 (is is the net total)
		// that matches so it doesn't apply any virtual even though it should to properly disperse contracts

		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(47.8, -1.89, 5.51, 0.0);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, 50, null));
		positionAllocationList.add(setupPositionAllocation(2, -2, null));
		positionAllocationList.add(setupPositionAllocation(3, 5, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(53));
		config.process();

		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(48, rep.getActualContracts(), type.name());
				validateExpected(2, rep.getVirtualContracts(), type.getDependentType().name());
			}
			else if ("2_2".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-2, rep.getVirtualContracts(), type.getDependentType().name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(5, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(0, rep.getActualContracts(), type.name());
			}
		}
	}


	@Test
	public void testSplit4_WithVirtual_Explicit() {
		// 576700 Sonoma County 11/05 ESZ4

		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(-74.0, -3.0, 56.0, 27.0);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(2, -3, null));
		positionAllocationList.add(setupPositionAllocation(3, 56, null));
		positionAllocationList.add(setupPositionAllocation(4, 27, null));

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(6));
		config.process();

		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-74, rep.getVirtualContracts(), type.getDependentType().name());
			}
			else if ("2_2".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-3, rep.getVirtualContracts(), type.getDependentType().name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(56, rep.getVirtualContracts(), type.name());
			}
			else {
				validateExpected(6, rep.getActualContracts(), type.name());
				validateExpected(21, rep.getVirtualContracts(), type.name());
			}
		}

		// Try setting explicit for 2 short and one long
		list = setupTestListByContracts(-74.0, -3.0, 56.0, 27.0);
		positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(1, -74, null));
		positionAllocationList.add(setupPositionAllocation(2, -3, null));
		positionAllocationList.add(setupPositionAllocation(4, 27, null));

		config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(6));
		config.process();

		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-74, rep.getVirtualContracts(), type.getDependentType().name());
			}
			else if ("2_2".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-3, rep.getVirtualContracts(), type.getDependentType().name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				//validateExpected(56, rep.getVirtualContracts(), type.name());
			}
			else {
				validateExpected(6, rep.getActualContracts(), type.name());
				validateExpected(21, rep.getVirtualContracts(), type.name());
			}
		}

		// Try different split after trading
		list = setupTestListByContracts(-74.0, -3.0, 56.0, 27.0);
		positionAllocationList = new ArrayList<>();
		positionAllocationList.add(setupPositionAllocation(2, -13, null));
		positionAllocationList.add(setupPositionAllocation(3, 50, null));
		positionAllocationList.add(setupPositionAllocation(4, 27, null));

		config = new ProductReplicationPositionAllocationConfig(type, list, positionAllocationList, BigDecimal.valueOf(-10));
		config.process();

		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(-74, rep.getVirtualContracts(), type.getDependentType().name());
			}
			else if ("2_2".equals(config.getKeyForReplication(rep))) {
				validateExpected(-10, rep.getActualContracts(), type.name());
				validateExpected(-3, rep.getVirtualContracts(), type.getDependentType().name());
			}
			else if ("3_3".equals(config.getKeyForReplication(rep))) {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(50, rep.getVirtualContracts(), type.getDependentType().name());
			}
			else {
				validateExpected(0, rep.getActualContracts(), type.name());
				validateExpected(27, rep.getVirtualContracts(), type.getDependentType().name());
			}
		}
	}


	@Test
	public void testSplit2_NotExplicitWithOneTargetOfZero() {
		// 371400 Laborers' Chicago
		// TYU4 is split across two asset classes, but one of them has a target of 0%.  On Production this replication
		// was already updated to remove the 0 target, but for testing, the system should have not allocated it any contracts

		List<ProductOverlayAssetClassReplication> list = setupTestListByContracts(33.85, 0.00, null);
		PortfolioAccountContractStoreTypes type = PortfolioAccountContractStoreTypes.ACTUAL;

		ProductReplicationPositionAllocationConfig config = new ProductReplicationPositionAllocationConfig(type, list, null, BigDecimal.valueOf(38));
		config.process();

		for (ProductOverlayAssetClassReplication rep : list) {
			if ("1_1".equals(config.getKeyForReplication(rep))) {
				validateExpected(38, rep.getActualContracts(), type.name());
			}
			else {
				validateExpected(0, rep.getActualContracts(), type.name());
			}
		}
	}


	///////////////////////////////////////////////////////
	// Helper Methods
	///////////////////////////////////////////////////////


	private void validateExpected(Integer expected, BigDecimal actual, String fieldName) {
		if (expected == 0) {
			Assertions.assertTrue(MathUtils.isNullOrZero(actual),
					"Expected [" + expected + " " + fieldName + "] contracts, but instead got " + (actual == null ? "NULL" : CoreMathUtils.formatNumberInteger(actual)));
		}
		else {
			Assertions.assertTrue(MathUtils.isEqual(actual, BigDecimal.valueOf(expected)),
					"Expected [" + expected + " " + fieldName + "] contracts, but instead got " + (actual == null ? "NULL" : CoreMathUtils.formatNumberInteger(actual)));
		}
	}


	private List<ProductOverlayAssetClassReplication> setupTestList(Boolean rep1Long, Boolean rep2Long, Boolean rep3Long) {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		if (rep1Long != null) {
			// = Target of 20.2/-20.2
			list.add(setupReplication(1, (rep1Long ? 101.0 : -101.0), 5.0));
		}
		if (rep2Long != null) {
			// = Target of 37.74/-37.74
			list.add(setupReplication(2, (rep2Long ? 200.0 : -200.0), 5.3));
		}
		if (rep3Long != null) {
			// = Target of 30/-30
			list.add(setupReplication(3, (rep3Long ? 300.0 : -300.0), 10.0));
		}
		return list;
	}


	private List<ProductOverlayAssetClassReplication> setupTestListByContracts(Double rep1TargetContracts, Double rep2TargetContracts, Double rep3TargetContracts) {
		return setupTestListByContracts(rep1TargetContracts, rep2TargetContracts, rep3TargetContracts, null);
	}


	private List<ProductOverlayAssetClassReplication> setupTestListByContracts(Double rep1TargetContracts, Double rep2TargetContracts, Double rep3TargetContracts, Double rep4TargetContracts) {
		List<ProductOverlayAssetClassReplication> list = new ArrayList<>();
		if (rep1TargetContracts != null) {
			list.add(setupReplication(1, rep1TargetContracts * 5.0, 5.0));
		}
		if (rep2TargetContracts != null) {
			list.add(setupReplication(2, rep2TargetContracts * 5.3, 5.3));
		}
		if (rep3TargetContracts != null) {
			list.add(setupReplication(3, rep3TargetContracts * 10.0, 10.0));
		}
		if (rep4TargetContracts != null) {
			list.add(setupReplication(4, rep4TargetContracts * 10.0, 10.0));
		}
		return list;
	}


	private ProductOverlayAssetClassReplication setupReplication(int count, Double target, Double contractValue) {

		ProductOverlayAssetClassReplication rep = new ProductOverlayAssetClassReplication();
		rep.setTargetExposure(BigDecimal.valueOf((target)));
		rep.setTargetExposureAdjusted(BigDecimal.valueOf((target)));
		rep.setValue(BigDecimal.valueOf(contractValue));

		InvestmentReplication ir = new InvestmentReplication();
		ir.setId(count);
		InvestmentReplicationType rt = new InvestmentReplicationType();
		ir.setType(rt);
		rep.setReplication(ir);

		ProductOverlayAssetClass oac = new ProductOverlayAssetClass();
		oac.setAccountAssetClass(setupAccountAssetClass(count));
		rep.setOverlayAssetClass(oac);

		return rep;
	}


	private InvestmentAccountAssetClass setupAccountAssetClass(int id) {
		InvestmentAccountAssetClass iac = new InvestmentAccountAssetClass();
		InvestmentAssetClass c = new InvestmentAssetClass();
		c.setName("Asset Class: " + id);
		iac.setId(id);
		iac.setAssetClass(c);
		return iac;
	}


	private InvestmentAccountAssetClassPositionAllocation setupPositionAllocation(int id, int allocationQuantity, Integer order) {
		InvestmentAccountAssetClassPositionAllocation alloc = new InvestmentAccountAssetClassPositionAllocation();
		alloc.setAccountAssetClass(setupAccountAssetClass(id));
		alloc.setAllocationQuantity(BigDecimal.valueOf(allocationQuantity));
		alloc.setAllocationOrder(order == null ? null : order.shortValue());
		return alloc;
	}
}
