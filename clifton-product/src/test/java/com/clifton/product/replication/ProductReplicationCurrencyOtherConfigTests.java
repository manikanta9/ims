package com.clifton.product.replication;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.portfolio.account.PortfolioAccountContractStoreTypes;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioReplicationCurrencyOtherConfig;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductReplicationCurrencyOtherConfigTests</code> tests the processing of CCY other and allocating amounts
 * back into currency replications {@link PortfolioReplicationCurrencyOtherConfig}
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ProductReplicationCurrencyOtherConfigTests {

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	//////////////////////////////////////////////////////////////////////
	///// US Based Account Tests
	//////////////////////////////////////////////////////////////////////


	@Test
	public void testStandardUSBasedAccount() {
		// With CCY Other Applied
		testStandardUSBasedAccountImpl(PortfolioAccountContractStoreTypes.ACTUAL, false, true);
		testStandardUSBasedAccountImpl(PortfolioAccountContractStoreTypes.PENDING, false, true);
		testStandardUSBasedAccountImpl(PortfolioAccountContractStoreTypes.CURRENT, false, true);

		// With CCY Other But NOT Applied
		testStandardUSBasedAccountImpl(PortfolioAccountContractStoreTypes.ACTUAL, true, true);
		testStandardUSBasedAccountImpl(PortfolioAccountContractStoreTypes.PENDING, true, true);
		testStandardUSBasedAccountImpl(PortfolioAccountContractStoreTypes.CURRENT, true, true);

		// NO CCY Other Applied
		testStandardUSBasedAccountImpl(PortfolioAccountContractStoreTypes.ACTUAL, false, false);
		testStandardUSBasedAccountImpl(PortfolioAccountContractStoreTypes.PENDING, false, false);
		testStandardUSBasedAccountImpl(PortfolioAccountContractStoreTypes.CURRENT, false, false);

		// NO CCY Other NOT Applied
		testStandardUSBasedAccountImpl(PortfolioAccountContractStoreTypes.ACTUAL, true, false);
		testStandardUSBasedAccountImpl(PortfolioAccountContractStoreTypes.PENDING, true, false);
		testStandardUSBasedAccountImpl(PortfolioAccountContractStoreTypes.CURRENT, true, false);
	}


	private void testStandardUSBasedAccountImpl(PortfolioAccountContractStoreTypes type, boolean doNotApplyCurrencyOther, boolean includeCurrencyOtherList) {
		List<ProductOverlayAssetClassReplication> replicationList = setupReplicationList(type, doNotApplyCurrencyOther);

		List<PortfolioCurrencyOther> ccyOtherList = new ArrayList<>();
		if (includeCurrencyOtherList) {
			ccyOtherList.add(setupCurrencyOther(type, "HKD", -5000));
			ccyOtherList.add(setupCurrencyOther(type, "SEK", 150000));
		}

		processCurrencyOther(type, "USD", replicationList, ccyOtherList);

		// Validate Results
		Map<String, Double> ccyOtherResults = new HashMap<>();
		if (includeCurrencyOtherList && !doNotApplyCurrencyOther) {
			ccyOtherResults.put("AD-USD", 58232.93);
			ccyOtherResults.put("CD-USD", 46586.35);
			ccyOtherResults.put("SF-USD", 34939.76);
			ccyOtherResults.put("BP-USD", 2911.65);
			ccyOtherResults.put("JY-USD", 2329.32);
		}
		validateReplicationResults(type, replicationList, ccyOtherResults, null);
		validateCurrencyOtherResults(ccyOtherList, null);
	}

	////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Ensures for cases where targets = 0, but we still have allocation weights to split amounts
	 * it is still split
	 */
	@Test
	public void testStandardUSBasedAccount_ZeroTargets() {
		// With CCY Other Applied
		testStandardUSBasedAccountImpl_ZeroTargets(PortfolioAccountContractStoreTypes.ACTUAL, false, true);
		testStandardUSBasedAccountImpl_ZeroTargets(PortfolioAccountContractStoreTypes.PENDING, false, true);
		testStandardUSBasedAccountImpl_ZeroTargets(PortfolioAccountContractStoreTypes.CURRENT, false, true);

		// With CCY Other But NOT Applied
		testStandardUSBasedAccountImpl_ZeroTargets(PortfolioAccountContractStoreTypes.ACTUAL, true, true);
		testStandardUSBasedAccountImpl_ZeroTargets(PortfolioAccountContractStoreTypes.PENDING, true, true);
		testStandardUSBasedAccountImpl_ZeroTargets(PortfolioAccountContractStoreTypes.CURRENT, true, true);

		// NO CCY Other Applied
		testStandardUSBasedAccountImpl_ZeroTargets(PortfolioAccountContractStoreTypes.ACTUAL, false, false);
		testStandardUSBasedAccountImpl_ZeroTargets(PortfolioAccountContractStoreTypes.PENDING, false, false);
		testStandardUSBasedAccountImpl_ZeroTargets(PortfolioAccountContractStoreTypes.CURRENT, false, false);

		// NO CCY Other NOT Applied
		testStandardUSBasedAccountImpl_ZeroTargets(PortfolioAccountContractStoreTypes.ACTUAL, true, false);
		testStandardUSBasedAccountImpl_ZeroTargets(PortfolioAccountContractStoreTypes.PENDING, true, false);
		testStandardUSBasedAccountImpl_ZeroTargets(PortfolioAccountContractStoreTypes.CURRENT, true, false);
	}


	private void testStandardUSBasedAccountImpl_ZeroTargets(PortfolioAccountContractStoreTypes type, boolean doNotApplyCurrencyOther, boolean includeCurrencyOtherList) {
		List<ProductOverlayAssetClassReplication> replicationList = new ArrayList<>();
		replicationList.add(setupReplication(type, "AD-USD", 25.20, 0, 2500.0, doNotApplyCurrencyOther));
		replicationList.add(setupReplication(type, "CD-USD", 25.60, 0, 4000.0, doNotApplyCurrencyOther));
		replicationList.add(setupReplication(type, "SF-USD", 10.75, 0, 0, doNotApplyCurrencyOther));
		replicationList.add(setupReplication(type, "BP-USD", 25, 0, 0, doNotApplyCurrencyOther));
		replicationList.add(setupReplication(type, "JY-USD", 13.45, 0, 0, doNotApplyCurrencyOther));

		List<PortfolioCurrencyOther> ccyOtherList = new ArrayList<>();
		if (includeCurrencyOtherList) {
			ccyOtherList.add(setupCurrencyOther(type, "HKD", -5000));
			ccyOtherList.add(setupCurrencyOther(type, "SEK", 150000));
		}

		processCurrencyOther(type, "USD", replicationList, ccyOtherList);

		// Validate Results
		Map<String, Double> ccyOtherResults = new HashMap<>();
		if (includeCurrencyOtherList && !doNotApplyCurrencyOther) {
			ccyOtherResults.put("AD-USD", 36540.00);
			ccyOtherResults.put("CD-USD", 37120.00);
			ccyOtherResults.put("SF-USD", 15587.50);
			ccyOtherResults.put("BP-USD", 36250.00);
			ccyOtherResults.put("JY-USD", 19502.50);
		}
		validateReplicationResults(type, replicationList, ccyOtherResults, null);
		validateCurrencyOtherResults(ccyOtherList, null);
	}

	//////////////////////////////////////////////////////////////////////
	///// CAD Based Account Tests
	//////////////////////////////////////////////////////////////////////


	@Test
	public void testCADBasedAccount_NoCurrencyDenomination() {
		// With CCY Other Applied - No CCY Denomination So works same as USD
		testCADBasedAccount_NoCurrencyDenominationImpl(PortfolioAccountContractStoreTypes.ACTUAL, false, true);
		testCADBasedAccount_NoCurrencyDenominationImpl(PortfolioAccountContractStoreTypes.PENDING, false, true);
		testCADBasedAccount_NoCurrencyDenominationImpl(PortfolioAccountContractStoreTypes.CURRENT, false, true);
	}


	private void testCADBasedAccount_NoCurrencyDenominationImpl(PortfolioAccountContractStoreTypes type, boolean doNotApplyCurrencyOther, boolean includeCurrencyOtherList) {
		List<ProductOverlayAssetClassReplication> replicationList = setupReplicationList(type, doNotApplyCurrencyOther);
		List<PortfolioCurrencyOther> ccyOtherList = new ArrayList<>();
		if (includeCurrencyOtherList) {
			ccyOtherList.add(setupCurrencyOther(type, "HKD", -5000));
			ccyOtherList.add(setupCurrencyOther(type, "SEK", 150000));
		}

		processCurrencyOther(type, "CAD", replicationList, ccyOtherList);

		// Validate Results
		Map<String, Double> ccyOtherResults = new HashMap<>();
		if (includeCurrencyOtherList && !doNotApplyCurrencyOther) {
			ccyOtherResults.put("AD-USD", 58232.93);
			ccyOtherResults.put("CD-USD", 46586.35);
			ccyOtherResults.put("SF-USD", 34939.76);
			ccyOtherResults.put("BP-USD", 2911.65);
			ccyOtherResults.put("JY-USD", 2329.32);
		}
		validateReplicationResults(type, replicationList, ccyOtherResults, null);
		validateCurrencyOtherResults(ccyOtherList, null);
	}


	@Test
	public void testCADBasedAccount_OneCurrencyDenomination() {
		// With CCY Other Applied
		testCADBasedAccount_OneCurrencyDenominationImpl(PortfolioAccountContractStoreTypes.ACTUAL, false, true);
		testCADBasedAccount_OneCurrencyDenominationImpl(PortfolioAccountContractStoreTypes.PENDING, false, true);
		testCADBasedAccount_OneCurrencyDenominationImpl(PortfolioAccountContractStoreTypes.CURRENT, false, true);
	}


	private void testCADBasedAccount_OneCurrencyDenominationImpl(PortfolioAccountContractStoreTypes type, boolean doNotApplyCurrencyOther, boolean includeCurrencyOtherList) {
		List<ProductOverlayAssetClassReplication> replicationList = setupReplicationList(type, doNotApplyCurrencyOther);
		List<PortfolioCurrencyOther> ccyOtherList = new ArrayList<>();
		if (includeCurrencyOtherList) {
			ccyOtherList.add(setupCurrencyOther(type, "HKD", -5000));
			ccyOtherList.add(setupCurrencyOther(type, "USD", 150000));
		}

		processCurrencyOther(type, "CAD", replicationList, ccyOtherList);

		// Validate Results
		Map<String, Double> ccyOtherResults = new HashMap<>();
		Map<String, Double> ccyDenomResults = new HashMap<>();
		String[] ccyDenom = null;
		if (includeCurrencyOtherList && !doNotApplyCurrencyOther) {
			ccyOtherResults.put("AD-USD", -2008.03);
			ccyOtherResults.put("CD-USD", -1606.43);
			ccyOtherResults.put("SF-USD", -1204.82);
			ccyOtherResults.put("BP-USD", -100.40);
			ccyOtherResults.put("JY-USD", -80.32);

			ccyDenomResults.put("CD-USD", 150000.00);
			ccyDenom = new String[]{"USD"};
		}

		validateReplicationResults(type, replicationList, ccyOtherResults, ccyDenomResults);
		validateCurrencyOtherResults(ccyOtherList, ccyDenom);
	}


	@Test
	public void testCADBasedAccount_TwoCurrencyDenomination() {
		// With CCY Other Applied
		testCADBasedAccount_TwoCurrencyDenominationImpl(PortfolioAccountContractStoreTypes.ACTUAL, false, true);
		testCADBasedAccount_TwoCurrencyDenominationImpl(PortfolioAccountContractStoreTypes.PENDING, false, true);
		testCADBasedAccount_TwoCurrencyDenominationImpl(PortfolioAccountContractStoreTypes.CURRENT, false, true);
	}


	private void testCADBasedAccount_TwoCurrencyDenominationImpl(PortfolioAccountContractStoreTypes type, boolean doNotApplyCurrencyOther, boolean includeCurrencyOtherList) {
		List<ProductOverlayAssetClassReplication> replicationList = setupReplicationList(type, doNotApplyCurrencyOther);
		replicationList.add(setupReplication(type, "CD-EUR", 0, 25000.00, 0, false));
		recalculateReplicationAllocationWeightFromTargets(replicationList);

		List<PortfolioCurrencyOther> ccyOtherList = new ArrayList<>();
		if (includeCurrencyOtherList) {
			ccyOtherList.add(setupCurrencyOther(type, "HKD", -5000));
			ccyOtherList.add(setupCurrencyOther(type, "USD", 150000));
			ccyOtherList.add(setupCurrencyOther(type, "EUR", 45000));
		}

		processCurrencyOther(type, "CAD", replicationList, ccyOtherList);

		// Validate Results
		Map<String, Double> ccyOtherResults = new HashMap<>();
		Map<String, Double> ccyDenomResults = new HashMap<>();
		String[] ccyDenom = null;
		if (includeCurrencyOtherList && !doNotApplyCurrencyOther) {
			ccyOtherResults.put("AD-USD", -1824.82);
			ccyOtherResults.put("CD-USD", -1459.85);
			ccyOtherResults.put("CD-EUR", -456.20);
			ccyOtherResults.put("SF-USD", -1094.89);
			ccyOtherResults.put("BP-USD", -91.24);
			ccyOtherResults.put("JY-USD", -72.99);

			ccyDenomResults.put("CD-USD", 150000.00);
			ccyDenomResults.put("CD-EUR", 45000.00);

			ccyDenom = new String[]{"USD", "EUR"};
		}

		validateReplicationResults(type, replicationList, ccyOtherResults, ccyDenomResults);
		validateCurrencyOtherResults(ccyOtherList, ccyDenom);
	}

	///////////////////////////////////////////////////////
	//////////         Helper Methods            //////////
	///////////////////////////////////////////////////////


	private void processCurrencyOther(PortfolioAccountContractStoreTypes type, String ccy, List<ProductOverlayAssetClassReplication> repList, List<PortfolioCurrencyOther> ccyOtherList) {
		PortfolioReplicationCurrencyOtherConfig<ProductOverlayAssetClassReplication> config = new PortfolioReplicationCurrencyOtherConfig<>(type, getSecurity(ccy), repList);
		config.applyCurrencyOtherToReplications(ccyOtherList);
	}


	private void validateReplicationResults(PortfolioAccountContractStoreTypes type, List<ProductOverlayAssetClassReplication> repList, Map<String, Double> currencyOtherAmountMap,
	                                        Map<String, Double> currencyDenomAmountMap) {
		for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(repList)) {
			String sec = rep.getSecurity().getSymbol();

			Double expected = null;
			if (currencyOtherAmountMap != null && currencyOtherAmountMap.containsKey(sec)) {
				expected = currencyOtherAmountMap.get(sec);
			}
			validateReplicationFieldValue(rep, getReplicationCurrencyAmountFieldName(type, false), expected);

			expected = null;
			if (currencyDenomAmountMap != null && currencyDenomAmountMap.containsKey(sec)) {
				expected = currencyDenomAmountMap.get(sec);
			}
			validateReplicationFieldValue(rep, getReplicationCurrencyAmountFieldName(type, true), expected);
		}
	}


	private void validateReplicationFieldValue(ProductOverlayAssetClassReplication rep, String fldName, Double expected) {
		String sec = rep.getSecurity().getSymbol();
		BigDecimal value = MathUtils.round((BigDecimal) BeanUtils.getPropertyValue(rep, fldName), 2);
		if (expected == null) {
			Assertions.assertTrue(MathUtils.isNullOrZero(value), sec + " " + fldName + " expected to be null or 0");
		}
		else {
			Assertions.assertEquals(MathUtils.round(BigDecimal.valueOf(expected), 2), value, sec + " " + fldName);
		}
	}


	private void validateCurrencyOtherResults(List<PortfolioCurrencyOther> ccyOtherList, String[] ccyDenom) {
		for (PortfolioCurrencyOther co : CollectionUtils.getIterable(ccyOtherList)) {
			if (ArrayUtils.contains(ccyDenom, co.getCurrencySecurity().getSymbol())) {
				Assertions.assertTrue(co.isCurrencyDenominationAllocation(), "Expected " + co.getCurrencySecurity().getSymbol() + " to be IsCurrencyDenominationAllocation = TRUE");
			}
			else {
				Assertions.assertFalse(co.isCurrencyDenominationAllocation(), "Expected " + co.getCurrencySecurity().getSymbol() + " to be IsCurrencyDenominationAllocation = FALSE");
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////


	private List<ProductOverlayAssetClassReplication> setupReplicationList(PortfolioAccountContractStoreTypes type, boolean doNotApplyCurrencyOther) {
		List<ProductOverlayAssetClassReplication> replicationList = new ArrayList<>();
		replicationList.add(setupReplication(type, "AD-USD", 0, 100000, 2500.0, doNotApplyCurrencyOther));
		replicationList.add(setupReplication(type, "CD-USD", 0, 80000, 4000.0, doNotApplyCurrencyOther));
		replicationList.add(setupReplication(type, "SF-USD", 0, 60000, 0, doNotApplyCurrencyOther));
		replicationList.add(setupReplication(type, "BP-USD", 0, 5000, 0, doNotApplyCurrencyOther));
		replicationList.add(setupReplication(type, "JY-USD", 0, 4000, 0, doNotApplyCurrencyOther));
		recalculateReplicationAllocationWeightFromTargets(replicationList);
		return replicationList;
	}


	private void recalculateReplicationAllocationWeightFromTargets(List<ProductOverlayAssetClassReplication> replicationList) {
		BigDecimal sum = CoreMathUtils.sumProperty(replicationList, ProductOverlayAssetClassReplication::getTargetExposureAdjusted);
		for (ProductOverlayAssetClassReplication rep : replicationList) {
			rep.setAllocationWeightAdjusted(CoreMathUtils.getPercentValue(rep.getTargetExposureAdjusted(), sum, true));
		}
		CoreMathUtils.applySumPropertyDifference(replicationList, ProductOverlayAssetClassReplication::getAllocationWeightAdjusted, ProductOverlayAssetClassReplication::setAllocationWeightAdjusted, MathUtils.BIG_DECIMAL_ONE_HUNDRED, true);
	}


	private ProductOverlayAssetClassReplication setupReplication(PortfolioAccountContractStoreTypes type, String ccyFutureSymbol, double allocationWeight, double target, double ccyActualBase,
	                                                             boolean doNotApplyCurrencyOther) {
		InvestmentSecurity future = getSecurity(ccyFutureSymbol);
		ProductOverlayAssetClassReplication rep = new ProductOverlayAssetClassReplication();
		InvestmentReplication ir = new InvestmentReplication();
		InvestmentReplicationType rt = new InvestmentReplicationType();
		rt.setDoNotApplyCurrencyOtherAllocation(doNotApplyCurrencyOther);
		ir.setType(rt);
		rep.setReplication(ir);
		future.setUnderlyingSecurity(getSecurityByInstrument(future.getInstrument().getUnderlyingInstrument().getId()));
		rep.setSecurity(future);
		rep.setAllocationWeightAdjusted(BigDecimal.valueOf(allocationWeight));
		rep.setTargetExposureAdjusted(BigDecimal.valueOf(target));
		rep.setCurrencyExchangeRate(BigDecimal.ONE);
		if (PortfolioAccountContractStoreTypes.ACTUAL == type) {
			rep.setCurrencyActualLocalAmount(BigDecimal.valueOf(ccyActualBase));
		}
		else if (PortfolioAccountContractStoreTypes.PENDING == type) {
			rep.setCurrencyPendingLocalAmount(BigDecimal.valueOf(ccyActualBase));
		}
		else {
			rep.setCurrencyCurrentLocalAmount(BigDecimal.valueOf(ccyActualBase));
		}
		return rep;
	}


	private PortfolioCurrencyOther setupCurrencyOther(PortfolioAccountContractStoreTypes type, String ccy, double ccyBase) {
		PortfolioCurrencyOther co = new PortfolioCurrencyOther();
		co.setCurrencySecurity(getSecurity(ccy));
		// Using FX Rate of 1 always because for testing only setting/checking base amounts
		co.setExchangeRate(BigDecimal.ONE);

		if (PortfolioAccountContractStoreTypes.ACTUAL == type) {
			co.setLocalAmount(BigDecimal.valueOf(ccyBase));
		}
		else if (PortfolioAccountContractStoreTypes.PENDING == type) {
			co.setPendingLocalAmount(BigDecimal.valueOf(ccyBase));
		}
		else {
			co.setCurrentLocalAmount(BigDecimal.valueOf(ccyBase));
		}
		return co;
	}


	private InvestmentSecurity getSecurity(String symbol) {
		return this.investmentInstrumentService.getInvestmentSecurityBySymbol(symbol, null);
	}


	private InvestmentSecurity getSecurityByInstrument(int instrumentId) {
		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setInstrumentId(instrumentId);
		return CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentSecurityList(searchForm));
	}


	/**
	 * Based on Actual, Pending, Current - Set Correct Field Name
	 */
	private String getReplicationCurrencyAmountFieldName(PortfolioAccountContractStoreTypes type, boolean currencyDenomination) {
		if (PortfolioAccountContractStoreTypes.ACTUAL == type) {
			return (currencyDenomination) ? "currencyDenominationBaseAmount" : "currencyOtherBaseAmount";
		}
		else if (PortfolioAccountContractStoreTypes.PENDING == type) {
			return (currencyDenomination) ? "currencyPendingDenominationBaseAmount" : "currencyPendingOtherBaseAmount";
		}
		return (currencyDenomination) ? "currencyCurrentDenominationBaseAmount" : "currencyCurrentOtherBaseAmount";
	}
}
