package com.clifton.product.manager;

import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.AggregationOperations;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountRollup;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;


/**
 * The <code>ProductManagerRollupAggregationTypesTests</code> tests ProductManagerService.processProductManagerAccountRollupBalance() function to ensure it
 * can process AccountRollupManagers with a specified AggregationOperations (SUM, MIN, NAX).
 *
 * @author davidi
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ProductManagerRollupAggregationTypesTests {

	private static final int MANAGER1_ID = 101; // CASH BALANCE: 10,000 SECURITIES BALANCE: 12,000
	private static final int MANAGER2_ID = 102; // CASH BALANCE: 25,000 SECURITIES BALANCE: 10,000
	private static final int MANAGER3_ID = 103; // CASH BALANCE: 0 SECURITIES BALANCE: 1,000

	private static final int ROLLUP_MANAGER_ID = 201;
	private static final Date TEST_BALANCE_DATE = DateUtils.toDate("02/03/2016");

	////////////////////////////////////////////////////////////////////////////////

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private InvestmentManagerAccountService investmentManagerAccountService;

	@Resource
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	@Resource
	private ProductManagerService productManagerService;

	@Resource
	private XmlReadOnlyDAO<InvestmentManagerAccountRollup> investmentManagerAccountRollupDAO;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupTest() {
		this.investmentManagerAccountRollupDAO.setLogicalEvaluatesToTrue(true);

		short year = 2016;
		if (this.calendarSetupService.getCalendarYear(year) == null) {
			this.calendarSetupService.saveCalendarYearForYear(year);
		}
	}

	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testProcessProductManagerAccountRollupBalance_RollupAggregationType_SUM() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.NONE;
		InvestmentManagerAccount rollupManager = setupRollupManager(100.0, 100.0, 100.0, cashAdjustmentType, AggregationOperations.SUM);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		InvestmentManagerAccountBalance managerAccountBalance = getManagerAccountBalance();
		Assertions.assertEquals(new BigDecimal("35000.00"), managerAccountBalance.getCashValue());
		Assertions.assertEquals(new BigDecimal("23000.00"), managerAccountBalance.getSecuritiesValue());
	}


	@Test
	public void testProcessProductManagerAccountRollupBalance_RollupAggregationType_SUM_80_Percent() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.NONE;
		InvestmentManagerAccount rollupManager = setupRollupManager(80.0, 80.0, 80.0, cashAdjustmentType, AggregationOperations.SUM);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		InvestmentManagerAccountBalance managerAccountBalance = getManagerAccountBalance();
		Assertions.assertEquals(new BigDecimal("28000.00"), managerAccountBalance.getCashValue());
		Assertions.assertEquals(new BigDecimal("18400.00"), managerAccountBalance.getSecuritiesValue());
	}


	@Test
	public void testProcessProductManagerAccountRollupBalance__RollupAggregationType_MIN() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.NONE;
		InvestmentManagerAccount rollupManager = setupRollupManager(100.0, 100.0, 100.0, cashAdjustmentType, AggregationOperations.MIN);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		InvestmentManagerAccountBalance managerAccountBalance = getManagerAccountBalance();

		Assertions.assertEquals(new BigDecimal("0.00"), managerAccountBalance.getCashValue());
		Assertions.assertEquals(new BigDecimal("1000.00"), managerAccountBalance.getSecuritiesValue());
	}


	@Test
	public void testProcessProductManagerAccountRollupBalance__RollupAggregationType_MIN_50_Percent() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.NONE;
		InvestmentManagerAccount rollupManager = setupRollupManager(50.0, 50.0, 50.0, cashAdjustmentType, AggregationOperations.MIN);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		InvestmentManagerAccountBalance managerAccountBalance = getManagerAccountBalance();

		Assertions.assertEquals(new BigDecimal("0.00"), managerAccountBalance.getCashValue());
		Assertions.assertEquals(new BigDecimal("500.00"), managerAccountBalance.getSecuritiesValue());
	}


	@Test
	public void testProcessProductManagerAccountRollupBalance_RollupAggregationType_MAX() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.NONE;
		InvestmentManagerAccount rollupManager = setupRollupManager(100.0, 100.0, 100.0, cashAdjustmentType, AggregationOperations.MAX);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		InvestmentManagerAccountBalance managerAccountBalance = getManagerAccountBalance();

		Assertions.assertEquals(new BigDecimal("25000.00"), managerAccountBalance.getCashValue());
		Assertions.assertEquals(new BigDecimal("10000.00"), managerAccountBalance.getSecuritiesValue());
	}


	@Test
	public void testProcessProductManagerAccountRollupBalance_RollupAggregationType_MAX_10_Percent() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.NONE;
		InvestmentManagerAccount rollupManager = setupRollupManager(10.0, 10.0, 10.0, cashAdjustmentType, AggregationOperations.MAX);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		InvestmentManagerAccountBalance managerAccountBalance = getManagerAccountBalance();

		Assertions.assertEquals(new BigDecimal("2500.00"), managerAccountBalance.getCashValue());
		Assertions.assertEquals(new BigDecimal("1000.00"), managerAccountBalance.getSecuritiesValue());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private InvestmentManagerAccount setupRollupManager(Double child1Percentage, Double child2Percentage, Double child3Percentage, InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType, AggregationOperations rollupAggregationType) {
		return setupRollupManager(child1Percentage, child2Percentage, child3Percentage, cashAdjustmentType, null, null, rollupAggregationType);
	}


	private InvestmentManagerAccount setupRollupManager(Double child1Percentage, Double child2Percentage, Double child3Percentage, InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType, Double customCashValue, Integer cashPercentManagerId, AggregationOperations rollupAggregationType) {
		InvestmentManagerAccount rollupManager = this.investmentManagerAccountService.getInvestmentManagerAccount(ROLLUP_MANAGER_ID);
		rollupManager.setRollupManagerList(new ArrayList<>());
		rollupManager.setRollupAggregationType(rollupAggregationType);
		if (child1Percentage != null) {
			InvestmentManagerAccountRollup rollup = new InvestmentManagerAccountRollup();
			rollup.setReferenceOne(rollupManager);
			rollup.setReferenceTwo(this.investmentManagerAccountService.getInvestmentManagerAccount(MANAGER1_ID));
			rollup.setAllocationPercent(BigDecimal.valueOf(child1Percentage));
			rollupManager.getRollupManagerList().add(rollup);
		}
		if (child2Percentage != null) {
			InvestmentManagerAccountRollup rollup = new InvestmentManagerAccountRollup();
			rollup.setReferenceOne(rollupManager);
			rollup.setReferenceTwo(this.investmentManagerAccountService.getInvestmentManagerAccount(MANAGER2_ID));
			rollup.setAllocationPercent(BigDecimal.valueOf(child2Percentage));
			rollupManager.getRollupManagerList().add(rollup);
		}
		if (child3Percentage != null) {
			InvestmentManagerAccountRollup rollup = new InvestmentManagerAccountRollup();
			rollup.setReferenceOne(rollupManager);
			rollup.setReferenceTwo(this.investmentManagerAccountService.getInvestmentManagerAccount(MANAGER3_ID));
			rollup.setAllocationPercent(BigDecimal.valueOf(child3Percentage));
			rollupManager.getRollupManagerList().add(rollup);
		}
		rollupManager.setCashAdjustmentType(cashAdjustmentType);
		if (cashAdjustmentType == InvestmentManagerAccount.CashAdjustmentType.CUSTOM_PERCENT || cashAdjustmentType == InvestmentManagerAccount.CashAdjustmentType.CUSTOM_VALUE) {
			Assertions.assertNotNull(customCashValue, "Custom Cash Value is Required for Cash Adjustment Type " + cashAdjustmentType);
			rollupManager.setCustomCashValue(BigDecimal.valueOf(customCashValue));
			rollupManager.setCustomCashValueUpdateDate(TEST_BALANCE_DATE);
		}
		else {
			rollupManager.setCustomCashValue(null);
			rollupManager.setCustomCashValueUpdateDate(null);
		}
		if (cashAdjustmentType == InvestmentManagerAccount.CashAdjustmentType.MANAGER) {
			Assertions.assertNotNull(cashPercentManagerId, "Custom Manager is required for Cash Adjustment Type " + cashAdjustmentType);
			rollupManager.setCashPercentManager(this.investmentManagerAccountService.getInvestmentManagerAccount(cashPercentManagerId));
		}
		this.investmentManagerAccountService.saveInvestmentManagerAccount(rollupManager);
		return rollupManager;
	}


	private InvestmentManagerAccountBalance getManagerAccountBalance() {
		return this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceByManagerAndDate(ROLLUP_MANAGER_ID, TEST_BALANCE_DATE, true);
	}
}
