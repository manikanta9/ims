package com.clifton.product.manager;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.AggregationOperations;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.stream.Collectors;


/**
 * @author MitchellF
 */
@ProductManagerRollupMemoryDbContext
public class ProductManagerRollupMemoryDbTests extends BaseInMemoryDatabaseTests {

	@Resource
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	@Resource
	private InvestmentManagerAccountService investmentManagerAccountService;

	@Resource
	private ProductManagerService productManagerService;

	public static final Date BALANCE_DATE = DateUtils.toDate("06/16/2020");
	public static final Integer ROLLUP_MANGER_ID = 26930;
	public static final Integer CASH_CHILD_ID = 25523;
	public static final Integer EQUITY_CHILD_ID = 27437;

	public static final String CONTRIBUTION_ADJUSTMENT_TYPE = "Contribution/Distribution";


	@Test
	public void testAddRollupManagerBalanceAdjustment_max() {
		// First, just verify that account balance is as expected
		InvestmentManagerAccount rollupManagerAccount = this.investmentManagerAccountService.getInvestmentManagerAccount(ROLLUP_MANGER_ID);

		validateManagerAccountBalance(rollupManagerAccount, BALANCE_DATE, 0, 100000000, 100000000, 0, 100000000, 0);

		// Add an adjustment that should cause the selection to flip
		applyAdjustment(CASH_CHILD_ID, BALANCE_DATE, BigDecimal.valueOf(50000000), "Contribution/Distribution");
		validateManagerAccountBalance(rollupManagerAccount, BALANCE_DATE, 137499322.94, 0, 137499322.94, 0, 137499322.94, 0);

		// Now, add MOC adjustment to the other child, causing MOC value ONLY to flip back, and calculated adjustment to be created
		applyAdjustment(EQUITY_CHILD_ID, BALANCE_DATE, BigDecimal.valueOf(50000000), "MOC");
		InvestmentManagerAccountBalance rollupBalance = validateManagerAccountBalance(rollupManagerAccount, BALANCE_DATE, 137499322.94, 0, 137499322.94, 0, 150000000, 0);

		// Verify properly calculated Rollup MOC adjustment
		InvestmentManagerAccountBalanceAdjustment rollupAdjustment = CollectionUtils.getOnlyElement(rollupBalance.getAdjustmentList().stream().filter(adjustment -> adjustment.getAdjustmentType().getName().equals("Rollup MOC")).collect(Collectors.toList()));
		ValidationUtils.assertTrue(MathUtils.isEqual(rollupAdjustment.getCashValue(), BigDecimal.valueOf(12500677.06)), "Rollup MOC adjustment was not calculated to correct value");

		// Now, verify that if Rollup MOC adjustment is removed, it will come back when balance is reprocessed
		rollupBalance.setAdjustmentList(null);
		rollupBalance = this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalance(rollupBalance);
		// Ensure balance was actually removed in the save
		ValidationUtils.assertNull(rollupBalance.getAdjustmentList(), "Adjustment list was not removed from balance");

		// This replicates clicking the "Process Adjustments" button in the UI for the balance
		rollupBalance = this.productManagerService.processProductManagerAccountBalanceAdjustmentsForBalance(rollupBalance);
		rollupAdjustment = CollectionUtils.getOnlyElement(rollupBalance.getAdjustmentList().stream().filter(adjustment -> adjustment.getAdjustmentType().getName().equals("Rollup MOC")).collect(Collectors.toList()));
		ValidationUtils.assertNotNull(rollupAdjustment, "Rollup MOC adjustment was not restored in processing.");
		ValidationUtils.assertTrue(MathUtils.isEqual(rollupAdjustment.getCashValue(), BigDecimal.valueOf(12500677.06)), "Rollup MOC adjustment was not calculated to correct value");
	}


	@Test
	public void testAddRollupManagerBalanceAdjustment_min() {
		// First, just verify that account balance is as expected
		InvestmentManagerAccount rollupManagerAccount = this.investmentManagerAccountService.getInvestmentManagerAccount(ROLLUP_MANGER_ID);
		validateManagerAccountBalance(rollupManagerAccount, BALANCE_DATE, 0, 100000000, 100000000, 0, 100000000, 0);

		//switch to min
		rollupManagerAccount.setRollupAggregationType(AggregationOperations.MIN);
		this.investmentManagerAccountService.saveInvestmentManagerAccount(rollupManagerAccount);

		// Adding an adjustment will cause balance to be re-evaluated and flip
		applyAdjustment(EQUITY_CHILD_ID, BALANCE_DATE, BigDecimal.valueOf(5000000), "Contribution/Distribution");
		validateManagerAccountBalance(rollupManagerAccount, BALANCE_DATE, 87499322.94, 0, 87499322.94, 0, 87499322.94, 0);

		// Now, add MOC adjustment that will cause balance to flip back, ONLY for MOC value
		applyAdjustment(CASH_CHILD_ID, BALANCE_DATE, BigDecimal.valueOf(50000000), "MOC");
		validateManagerAccountBalance(rollupManagerAccount, BALANCE_DATE, 87499322.94, 0, 87499322.94, 0, 105000000, 0);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void sleep() {
		try {
			Thread.sleep(1000);
		}
		catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}


	private void applyAdjustment(Integer managerAccountId, Date balanceDate, BigDecimal cashValue, String adjustmentTypeName) {
		InvestmentManagerAccountBalanceAdjustment adj = new InvestmentManagerAccountBalanceAdjustment();
		adj.setAdjustmentType(this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName(adjustmentTypeName));
		adj.setNote("Required");
		adj.setManagerAccountBalance(this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceByManagerAndDate(managerAccountId, balanceDate, false));
		adj.setCashValue(cashValue);
		adj.setSecuritiesValue(BigDecimal.ZERO);
		this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalanceAdjustment(adj);
		// Pause for asynchronous observer processing
		sleep();
	}


	/**
	 * Validates the balance exists with expected cash/securities values
	 *
	 * @return the populated balance record, so if there are adjustments they can be validated as well, but this will validate if balance is adjusted and at least one adjustment exists
	 */
	private InvestmentManagerAccountBalance validateManagerAccountBalance(InvestmentManagerAccount managerAccount, Date balanceDate, double originalCash, double originalSecurities, double adjustedCash, double adjustedSecurities, double mocCash, double mocSecurities) {
		InvestmentManagerAccountBalance balance = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceByManagerAndDate(managerAccount.getId(), balanceDate, true);
		String msgPrefix = "Balance for Manager [" + managerAccount.getLabel() + "] on [" + DateUtils.fromDateShort(balanceDate) + "]: ";
		AssertUtils.assertNotNull(balance, msgPrefix + "Missing");
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(originalCash), balance.getCashValue()), msgPrefix + "Expected Original Cash to Be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(originalCash)) + " but was " + CoreMathUtils.formatNumberMoney(balance.getCashValue()));
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(originalSecurities), balance.getSecuritiesValue()), msgPrefix + "Expected Original Securities to Be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(originalSecurities)) + " but was " + CoreMathUtils.formatNumberMoney(balance.getSecuritiesValue()));
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(adjustedCash), balance.getAdjustedCashValue()), msgPrefix + "Expected Adjusted Cash to Be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(adjustedCash)) + " but was " + CoreMathUtils.formatNumberMoney(balance.getAdjustedCashValue()));
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(adjustedSecurities), balance.getAdjustedSecuritiesValue()), msgPrefix + "Expected Adjusted Securities to Be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(adjustedSecurities)) + " but was " + CoreMathUtils.formatNumberMoney(balance.getAdjustedSecuritiesValue()));
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(mocCash), balance.getMarketOnCloseCashValue()), msgPrefix + "Expected MOC Cash to Be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(mocCash)) + " but was " + CoreMathUtils.formatNumberMoney(balance.getMarketOnCloseCashValue()));
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(mocSecurities), balance.getMarketOnCloseSecuritiesValue()), msgPrefix + "Expected MOC Securities to Be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(mocSecurities)) + " but was " + CoreMathUtils.formatNumberMoney(balance.getMarketOnCloseSecuritiesValue()));
		if (balance.isAdjusted()) {
			ValidationUtils.assertNotEmpty(balance.getAdjustmentList(), msgPrefix + " is adjusted but doesn't have any adjustments.");
		}
		return balance;
	}
}
