SELECT 'InvestmentManagerAccountBalance' AS entityTableName, InvestmentManagerAccountBalanceID AS entityId
FROM InvestmentManagerAccountBalance
WHERE InvestmentManagerAccountBalanceID IN (28022456, 28022570, 28022382)
UNION
SELECT 'InvestmentManagerAccountRollup' AS entityTableName, InvestmentManagerAccountRollupID AS entityId
FROM InvestmentManagerAccountRollup
WHERE RollupInvestmentManagerAccountId = 26930
UNION
SELECT 'InvestmentManagerAccountBalanceAdjustment' AS entityTableName, InvestmentManagerAccountBalanceAdjustmentID AS entityId
FROM InvestmentManagerAccountBalanceAdjustment
WHERE InvestmentManagerAccountBalanceID IN (28022456, 28022570, 28022382)
UNION
SELECT 'InvestmentManagerAccountBalanceAdjustmentType' AS entityTableName, InvestmentManagerAccountBalanceAdjustmentTypeID AS entityId
FROM InvestmentManagerAccountBalanceAdjustmentType
UNION
SELECT 'SystemTable' AS entityTableName, SystemTableID AS entityId
FROM SystemTable
WHERE
TableName IN ('InvestmentManagerAccountBalance', 'InvestmentManagerAccountRollup', 'InvestmentManagerAccountBalanceAdjustment', 'InvestmentManagerAccountBalanceAdjustmentType', 'InvestmentManagerAccount')
UNION
SELECT 'RuleViolationStatus' AS entityTableName, RuleViolationStatusID AS entityId
FROM RuleViolationStatus
UNION
SELECT 'Calendar' AS entityTableName, CalendarID AS entityId
FROM Calendar
WHERE IsDefaultSystemCalendar = 1;
