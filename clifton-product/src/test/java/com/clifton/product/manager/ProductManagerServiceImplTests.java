package com.clifton.product.manager;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.ExcelFileUtils;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.test.excel.BaseExcelTest;
import com.clifton.core.test.excel.BaseExcelTestConfig;
import com.clifton.core.test.excel.TableConfig;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.Event;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.event.EventListener;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.product.manager.job.AccountBalanceCommand;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.upload.SystemUploadCommand;
import com.clifton.system.upload.SystemUploadService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * <code>ProductManagerServiceImplTests</code> ...
 *
 * @author Mary Anderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ProductManagerServiceImplTests extends BaseExcelTest {

	private static final Date RUN_DATE = DateUtils.toDate("08/12/2013");
	@Resource
	private CalendarBusinessDayService calendarBusinessDayService;
	@Resource
	private CalendarSetupService calendarSetupService;
	@Resource
	private ProductManagerServiceImpl productManagerService;
	@Resource
	private SystemUploadService systemUploadService;
	@Resource
	private SystemSchemaService systemSchemaService;
	@Resource
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	@Resource
	private InvestmentManagerAccountService investmentManagerAccountService;

	@Resource
	private MarketDataRetriever marketDataRetriever;
	@Resource
	private EventHandler eventHandler;
	@Resource
	private ReadOnlyDAO<InvestmentManagerAccountBalance> investmentManagerAccountBalanceDAO;
	@Resource
	private ReadOnlyDAO<InvestmentManagerAccountBalanceAdjustment> investmentManagerAccountBalanceAdjustmentDAO;
	@Resource
	private ReadOnlyDAO<InvestmentManagerAccount> investmentManagerAccountDAO;
	private boolean reload;
	private boolean rollupManagers;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupTest() {
		// Setup Once
		for (short year = 2011; year < 2014; year++) {
			if (this.calendarSetupService.getCalendarYear(year) == null) {
				this.calendarSetupService.saveCalendarYearForYear(year);
			}
		}

		// Remove all existing manager balances and managers - do this instead of using dirties context
		for (InvestmentManagerAccountBalance balance : CollectionUtils.getIterable(this.investmentManagerAccountBalanceDAO.findAll())) {
			this.investmentManagerAccountBalanceService.deleteInvestmentManagerAccountBalance(balance.getId());
		}

		for (InvestmentManagerAccount managerAccount : CollectionUtils.getIterable(this.investmentManagerAccountDAO.findAll())) {
			this.investmentManagerAccountService.deleteInvestmentManagerAccount(managerAccount.getId());
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BaseExcelTestConfig setupTest(String filePath) {
		BaseExcelTestConfig config = new BaseExcelTestConfig(filePath);

		// SPECIAL TEST OVERRIDE - FOR MANAGER UPLOADS CAN PASS EMPTY ACCOUNT NUMBER SO THE SYSTEM AUTO ASSIGNS IT
		// BUT WITH THAT CHANGE, CAN NO LONGER SUPPORT INCLUDING CASH PERCENT MANAGER IN THE UPLOAD.  NEED THIS IN THE TEST
		// AND SINCE WE EXPLICITLY DEFINE ACCOUNT NUMBERS FOR TESTS, THIS IS OK
		List<Column> columnList = this.investmentManagerAccountDAO.getConfiguration().getColumnList();
		Column accountNumber = CollectionUtils.getFirstElementStrict(BeanUtils.filter(columnList, Column::getBeanPropertyName, "accountNumber"));
		accountNumber.setUploadValueConverterName(null);
		Column cashPercentManager = CollectionUtils.getFirstElementStrict(BeanUtils.filter(columnList, Column::getBeanPropertyName, "cashPercentManager"));
		cashPercentManager.setIgnoreUpload(false);

		// Tabs to upload
		config.addTabTableNameUpload("ManagerAccount", "InvestmentManagerAccount");
		config.addTabTableNameUpload("ManagerAccountRollup", "InvestmentManagerAccountRollup");
		config.addTabTableNameUpload("BalanceAdjustment", "InvestmentManagerAccountBalanceAdjustment");

		// Tabs with Results
		if (isReload() && isRollupManagers()) {
			config.addTabTableNameResult("RESULT_Balance_AfterReload", "InvestmentManagerAccountBalance");
			config.addTabTableNameResult("RESULT_Adjustment_AfterReload", "InvestmentManagerAccountBalanceAdjustment");
		}
		else {
			config.addTabTableNameResult("RESULT_Balance", "InvestmentManagerAccountBalance");
			config.addTabTableNameResult("RESULT_BalanceAdjustment", "InvestmentManagerAccountBalanceAdjustment");
		}
		// Result Table Configs
		TableConfig balanceConfig = new TableConfig("InvestmentManagerAccountBalance", new String[]{"managerAccount.accountNumber"}, new String[]{"ManagerAccountNumber",});
		// Original Balances
		balanceConfig.addValidateField("cashValue", "OriginalCashValue");
		balanceConfig.addValidateField("securitiesValue", "OriginalSecuritiesValue");
		balanceConfig.addValidateField("totalValue", "TotalValue");
		// Adjusted Balances
		balanceConfig.addValidateField("adjustedCashValue", "AdjustedCashValue");
		balanceConfig.addValidateField("adjustedSecuritiesValue", "AdjustedSecuritiesValue");
		balanceConfig.addValidateField("adjustedTotalValue", "AdjustedTotalValue");
		// MOC Balances
		balanceConfig.addValidateField("marketOnCloseCashValue", "MarketOnCloseCashValue");
		balanceConfig.addValidateField("marketOnCloseSecuritiesValue", "MarketOnCloseSecuritiesValue");
		balanceConfig.addValidateField("marketOnCloseTotalValue", "MarketOnCloseTotalValue");

		// Adjustment Flags
		balanceConfig.addValidateField("cashAdjustment", "IsCashAdjustment");
		balanceConfig.addValidateField("securitiesAdjustment", "IsSecuritiesAdjustment");
		balanceConfig.addValidateField("mocAdjustment", "IsMOCAdjustment");

		TableConfig adjustmentConfig = new TableConfig("InvestmentManagerAccountBalanceAdjustment", new String[]{"managerAccountBalance.managerAccount.accountNumber", "adjustmentType.name"},
				new String[]{"ManagerAccountNumber", "Balance-AdjustmentTypeName"});

		adjustmentConfig.addValidateField("cashValue", "AdjustmentCashValue");
		adjustmentConfig.addValidateField("securitiesValue", "AdjustmentSecuritiesValue");

		config.addTableConfig(balanceConfig);
		config.addTableConfig(adjustmentConfig);
		return config;
	}


	@Test
	public void testCustodianManagerLoadAndAdjust() {
		setReload(false);
		setRollupManagers(false);
		BaseExcelTestConfig config = setupTest("src/test/java/com/clifton/product/manager/CustodianManagersLoadAndAdjustTest.xls");
		processFile(config);

		// Add an MOC adjustment and then load following day to see if it is auto applied
		InvestmentManagerAccountBalance balance = CollectionUtils.getFirstElementStrict(
				BeanUtils.filter(this.investmentManagerAccountBalanceDAO.findAll(), managerBalance -> managerBalance.getManagerAccount().getAccountNumber(), "454502"));
		balance = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance.getId());
		InvestmentManagerAccountBalanceAdjustment adj = new InvestmentManagerAccountBalanceAdjustment();
		adj.setAdjustmentType(this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName("MOC"));
		adj.setCashValue(BigDecimal.valueOf(100.00));
		adj.setManagerAccountBalance(balance);
		balance.addAdjustment(adj);
		this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalance(balance);

		balance = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance.getId());
		ValidationUtils.assertTrue(balance.isMocAdjustment(), "Expected balance to have MOC Adjustment");

		Date nextDay = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(balance.getBalanceDate()), 1);
		InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();
		searchForm.setManagerAccountId(balance.getManagerAccount().getId());
		AccountBalanceCommand command = AccountBalanceCommand.ofSynchronous(searchForm, nextDay, ProductManagerBalanceProcessingTypes.LOAD_PROCESS);
		this.productManagerService.processProductManagerAccountBalanceList(command);

		InvestmentManagerAccountBalance nextBalance = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceByManagerAndDate(balance.getManagerAccount().getId(), nextDay, true);

		boolean found = false;
		for (InvestmentManagerAccountBalanceAdjustment adjustment : CollectionUtils.getIterable(nextBalance.getAdjustmentList())) {
			if ("Previous Day MOC".equals(adjustment.getAdjustmentType().getName()) && "Adjustment Automatically applied from previous day [MOC] adjustment".equals(adjustment.getNote())) {
				found = true;
				break;
			}
		}
		AssertUtils.assertTrue(found, "Expected to find Previous Day MOC adjustment automatically applied from previous balance.");
	}


	@Test
	public void testCustodianManagerLoadAndAdjust_Reload() {
		setReload(true);
		setRollupManagers(false);
		BaseExcelTestConfig config = setupTest("src/test/java/com/clifton/product/manager/CustodianManagersLoadAndAdjustTest.xls");
		processFile(config);
	}


	@Test
	public void testProxyManagerLoadAndAdjust() {
		setReload(false);
		setRollupManagers(false);
		BaseExcelTestConfig config = setupTest("src/test/java/com/clifton/product/manager/ProxyManagersLoadAndAdjustTest.xls");
		processFile(config);
	}


	@Test
	public void testProxyManagerLoadAndAdjust_Reload() {
		setReload(true);
		setRollupManagers(false);
		BaseExcelTestConfig config = setupTest("src/test/java/com/clifton/product/manager/ProxyManagersLoadAndAdjustTest.xls");
		processFile(config);
	}


	@Test
	public void testRollupManagerLoadAndAdjust() {
		setReload(false);
		setRollupManagers(true);
		BaseExcelTestConfig config = setupTest("src/test/java/com/clifton/product/manager/RollupManagersLoadAndAdjustTest.xls");
		processFile(config);
	}


	@Test
	public void testRollupManagerLoadAndAdjust_Reload() {
		setReload(false);
		setRollupManagers(true);
		BaseExcelTestConfig config = setupTest("src/test/java/com/clifton/product/manager/RollupManagersLoadAndAdjustTest.xls");
		processFile(config);

		setReload(true);
		BaseExcelTestConfig reloadConfig = setupTest("src/test/java/com/clifton/product/manager/RollupManagersLoadAndAdjustTest.xls");
		reloadConfig.setWorkbook(config.getWorkbook());

		Map<String, DataTable> expectedResults = ExcelFileUtils.convertWorkbookToDataTableMap(config.getWorkbook(), reloadConfig.getTabTableNameResultMap());
		config.setExpectedResults(expectedResults);
		validateResultsPopulated(config);

		//System.out.println("Reloading Now...");
		processData(config);

		validateResults(config);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public void uploadData(BaseExcelTestConfig config) {
		Mockito.when(this.systemSchemaService.getSystemTableByName(ArgumentMatchers.anyString())).thenAnswer(invocation -> {
			String tableName = invocation.getArgument(0);
			SystemTable table = new SystemTable();
			// Looks up columns by table id for column descriptions, but since we don't have any
			// columns set up in our xml db, id doesn't matter - will just always return empty list
			table.setId(MathUtils.SHORT_ONE);
			table.setName(tableName);
			table.setUploadAllowed(true);
			return table;
		});

		SystemUploadCommand upload = new SystemUploadCommand();
		upload.setWorkbook(config.getWorkbook());
		upload.setExistingBeans(FileUploadExistingBeanActions.INSERT);
		upload.setInsertMissingFKBeans(true);
		this.systemUploadService.uploadSystemUploadFileSheets(upload, config.getTabTableNameUploadMap());
	}


	@Override
	public void processData(BaseExcelTestConfig config) {

		Mockito.when(this.marketDataRetriever.getPrice(ArgumentMatchers.nullable(InvestmentSecurity.class), ArgumentMatchers.argThat(new MeasureDateMatcher(RUN_DATE)), ArgumentMatchers.anyBoolean(), ArgumentMatchers.anyString()))
				.thenReturn(getMarketDataPriceMocked(true));
		Mockito.when(this.marketDataRetriever.getPrice(ArgumentMatchers.nullable(InvestmentSecurity.class), ArgumentMatchers.argThat(new MeasureDateNotMatcher(RUN_DATE)), ArgumentMatchers.anyBoolean(), ArgumentMatchers.anyString()))
				.thenReturn(getMarketDataPriceMocked(false));

		Mockito.when(this.marketDataRetriever.getPriceAdjustedFlexible(ArgumentMatchers.nullable(InvestmentSecurity.class), ArgumentMatchers.argThat(new MeasureDateMatcher(RUN_DATE)), ArgumentMatchers.anyBoolean())).thenReturn(
				getMarketDataPriceAdjustedFlexibleMocked(true));
		Mockito.when(this.marketDataRetriever.getPriceAdjustedFlexible(ArgumentMatchers.nullable(InvestmentSecurity.class), ArgumentMatchers.argThat(new MeasureDateNotMatcher(RUN_DATE)), ArgumentMatchers.anyBoolean()))
				.thenReturn(getMarketDataPriceAdjustedFlexibleMocked(false));

		Mockito.when(this.marketDataRetriever.getPriceAdjusted(ArgumentMatchers.nullable(InvestmentSecurity.class), ArgumentMatchers.argThat(new MeasureDateMatcher(RUN_DATE)), ArgumentMatchers.anyBoolean(), ArgumentMatchers.anyString()))
				.thenReturn(getMarketDataPriceAdjustedMocked(true));
		Mockito.when(
				this.marketDataRetriever.getPriceAdjusted(ArgumentMatchers.nullable(InvestmentSecurity.class), ArgumentMatchers.argThat(new MeasureDateNotMatcher(RUN_DATE)), ArgumentMatchers.anyBoolean(), ArgumentMatchers.anyString()))
				.thenReturn(getMarketDataPriceAdjustedMocked(false));

		Mockito.when(
				this.marketDataRetriever.getInvestmentSecurityReturn(ArgumentMatchers.nullable(InvestmentAccount.class), ArgumentMatchers.nullable(InvestmentSecurity.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class),
						ArgumentMatchers.anyString(), ArgumentMatchers.eq(true))).thenReturn(BigDecimal.valueOf(5.0));

		Mockito.when(
				this.marketDataRetriever.getInvestmentSecurityReturnFlexible(ArgumentMatchers.nullable(InvestmentAccount.class), ArgumentMatchers.nullable(InvestmentSecurity.class), ArgumentMatchers.any(Date.class),
						ArgumentMatchers.any(Date.class), ArgumentMatchers.anyString(), ArgumentMatchers.eq(true))).thenReturn(BigDecimal.valueOf(1.0));

		EventListener<Event<Object, List<ManagerPositionHistorySummary>>> listener = new EventListener<Event<Object, List<ManagerPositionHistorySummary>>>() {

			@Override
			public int getOrder() {
				return 0;
			}


			@Override
			public void onEvent(Event<Object, List<ManagerPositionHistorySummary>> event) {
				if (Objects.equals(203, event.getContextValue("custodianId"))) {
					event.setResult(getManagerPositionSummaryMockedList(RUN_DATE));
				}
			}


			@Override
			public String getEventName() {
				return "Integration Manager Position History Summary";
			}
		};
		this.eventHandler.registerEventListener(listener);
		//this.productManagerService.setEventHandler(this.eventHandler);

		InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();
		AccountBalanceCommand command = AccountBalanceCommand.ofSynchronous(searchForm, RUN_DATE, isReload() ? (isRollupManagers() ? ProductManagerBalanceProcessingTypes.RELOAD_CHANGES_PROCESS
				: ProductManagerBalanceProcessingTypes.RELOAD_PROCESS) : ProductManagerBalanceProcessingTypes.LOAD_PROCESS);
		this.productManagerService.processProductManagerAccountBalanceList(command);

		// Process User Defined Adjustments - If Any Explicitly Set to Upload AFTER processing
		if (config.getWorkbook().getSheet("AFTER_BalanceAdjustment") != null) {
			SystemUploadCommand upload = new SystemUploadCommand();
			upload.setWorkbook(config.getWorkbook());
			upload.setExistingBeans(FileUploadExistingBeanActions.INSERT);
			upload.setInsertMissingFKBeans(true);

			// Get and re save so that the adjustments are recalculated
			for (InvestmentManagerAccountBalance balance : CollectionUtils.getIterable(this.investmentManagerAccountBalanceDAO.findAll())) {
				balance = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance.getId());
				this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalance(balance);
			}
		}
	}


	@Override
	public void validateResults(BaseExcelTestConfig config) {
		super.validateResults(config);
		//System.out.println("Original Load Validated Successfully...");

		//System.out.println("Reloading...");
		// Reload again - should keep all manual adjustments and reprocessing should bring back all automatic adjustments so end result should be the same
		InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();
		AccountBalanceCommand command = AccountBalanceCommand.ofSynchronous(searchForm, RUN_DATE, ProductManagerBalanceProcessingTypes.RELOAD_PROCESS);
		this.productManagerService.processProductManagerAccountBalanceList(command);
		super.validateResults(config);
		//System.out.println("Reload Validated Successfully...");
	}


	private BigDecimal getMarketDataPriceAdjustedFlexibleMocked(boolean currentValue) {
		return new BigDecimal(currentValue ? 101 : 100);
	}


	private BigDecimal getMarketDataPriceMocked(boolean currentValue) {
		return new BigDecimal(currentValue ? 52.5 : 100);
	}


	private BigDecimal getMarketDataPriceAdjustedMocked(boolean currentValue) {
		return new BigDecimal(currentValue ? 105 : 100);
	}


	private List<ManagerPositionHistorySummary> getManagerPositionSummaryMockedList(Date balanceDate) {
		List<ManagerPositionHistorySummary> list = new ArrayList<>();
		String[] bankCodes = {"295307", "295094", "P 51974B", "2282209", "12345", "295095", "B00001", "B00002"};

		for (int i = 1; i <= bankCodes.length; i++) {
			ManagerPositionHistorySummary summary = new ManagerPositionHistorySummary();
			if ("B00001".equals(bankCodes[i - 1])) {
				summary.setCashValue(BigDecimal.valueOf(7394223.96));
				summary.setSecurityValue(BigDecimal.valueOf(67847395.19));
			}
			else if ("B00002".equals(bankCodes[i - 1])) {
				summary.setCashValue(BigDecimal.ZERO);
				summary.setSecurityValue(BigDecimal.valueOf(68865165.61));
			}
			else {
				if (isReload() && isRollupManagers() && "295094".equals(bankCodes[i - 1])) {
					summary.setCashValue(new BigDecimal(i * 1100));
					summary.setSecurityValue(new BigDecimal(i * 700));
				}
				else {
					summary.setCashValue(new BigDecimal(i * 1000));
					summary.setSecurityValue(new BigDecimal(i * 500));
				}
			}
			summary.setPositionDate(balanceDate);
			summary.setBankCode(bankCodes[i - 1]);
			list.add(summary);
		}
		return list;
	}


	@Override
	public List<?> getExistingRecords(String tableName) {
		if ("InvestmentManagerAccountBalance".equals(tableName)) {
			return this.investmentManagerAccountBalanceDAO.findAll();
		}
		if ("InvestmentManagerAccountBalanceAdjustment".equals(tableName)) {
			return this.investmentManagerAccountBalanceAdjustmentDAO.findAll();
		}
		return null;
	}


	public boolean isReload() {
		return this.reload;
	}


	public void setReload(boolean reload) {
		this.reload = reload;
	}


	public boolean isRollupManagers() {
		return this.rollupManagers;
	}


	public void setRollupManagers(boolean rollupManagers) {
		this.rollupManagers = rollupManagers;
	}


	/**
	 * The matcher that matches measure date on the form to a given date
	 */
	static class MeasureDateMatcher implements ArgumentMatcher<Date> {

		private final Date match;


		MeasureDateMatcher(Date match) {
			this.match = match;
		}


		@Override
		public boolean matches(Date argument) {
			if (argument == null || this.match == null) {
				return false;
			}
			return this.match.equals(argument);
		}
	}

	/**
	 * The matcher that matches true if measure date doesn't match a given date
	 */
	static class MeasureDateNotMatcher extends MeasureDateMatcher {

		MeasureDateNotMatcher(Date match) {
			super(match);
		}


		@Override
		public boolean matches(Date argument) {
			return !super.matches(argument);
		}
	}
}
