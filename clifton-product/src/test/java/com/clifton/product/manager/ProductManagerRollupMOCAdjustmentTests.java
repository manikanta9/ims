package com.clifton.product.manager;

import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.AggregationOperations;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountRollup;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;


/**
 * The <code>ProductManagerRollupMOCAdjustmentTests</code> tests applying MOC adjustments from children to the rollup based on the rollup's options
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ProductManagerRollupMOCAdjustmentTests {

	private static final int MANAGER1_ID = 101; // ADJUSTED CASH BALANCE: 35,000 ADJUSTED SECURITIES BALANCE: 10,000, MOC CASH ADJUSTMENT: 50,000, MOC SECURITIES ADJUSTMENT: 50,000
	private static final int MANAGER2_ID = 102; // ADJUSTED CASH BALANCE: 25,000 ADJUSTED SECURITIES BALANCE: 50,000, MOC CASH ADJUSTMENT: 10,000, MOC SECURITIES ADJUSTMENT: 0,000

	private static final int ROLLUP_MANAGER_ID = 201;
	private static final Date TEST_BALANCE_DATE = DateUtils.toDate("02/03/2016");

	////////////////////////////////////////////////////////////////////////////////

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private InvestmentManagerAccountService investmentManagerAccountService;

	@Resource
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	@Resource
	private ProductManagerService productManagerService;

	@Resource
	private XmlReadOnlyDAO<InvestmentManagerAccountRollup> investmentManagerAccountRollupDAO;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupTest() {
		this.investmentManagerAccountRollupDAO.setLogicalEvaluatesToTrue(true);

		short year = 2016;
		if (this.calendarSetupService.getCalendarYear(year) == null) {
			this.calendarSetupService.saveCalendarYearForYear(year);
		}
	}

	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testRollupMOC_DoNotChangeCashAllocation() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.NONE;
		InvestmentManagerAccount rollupManager = setupRollupManager(100.0, 100.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(60000, 60000, 120000, 110000);

		rollupManager = setupRollupManager(50.0, 50.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(30000, 30000, 60000, 55000);

		rollupManager = setupRollupManager(-50.0, 100.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(7500, 45000, -7500, 20000);
	}


	@Test
	public void testRollupMOC_AllocateAllCash() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.CASH;
		InvestmentManagerAccount rollupManager = setupRollupManager(100.0, 100.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(60000, 60000, 120000, 0, 230000, 0);

		rollupManager = setupRollupManager(50.0, 50.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(30000, 30000, 60000, 0, 115000, 0);

		rollupManager = setupRollupManager(-50.0, 100.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(7500, 45000, 52500, 0, 12500, 0);
	}


	@Test
	public void testRollupMOC_AllocateAllSecurities() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.SECURITIES;
		InvestmentManagerAccount rollupManager = setupRollupManager(100.0, 100.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(60000, 60000, 0, 120000, 0, 230000);

		rollupManager = setupRollupManager(50.0, 50.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(30000, 30000, 0, 60000, 0, 115000);

		rollupManager = setupRollupManager(-50.0, 100.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(7500, 45000, 0, 52500, 0, 12500);
	}


	@Test
	public void testRollupMOC_IgnoreCash() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.IGNORE_CASH;
		InvestmentManagerAccount rollupManager = setupRollupManager(100.0, 100.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(60000, 60000, 0, 60000, 0, 110000);

		rollupManager = setupRollupManager(50.0, 50.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(30000, 30000, 0, 30000, 0, 55000);

		rollupManager = setupRollupManager(-50.0, 100.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(7500, 45000, 0, 45000, 0, 20000);
	}


	@Test
	public void testRollupMOC_IgnoreSecurities() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.IGNORE_SECURITIES;
		InvestmentManagerAccount rollupManager = setupRollupManager(100.0, 100.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(60000, 60000, 60000, 0, 120000, 0);

		rollupManager = setupRollupManager(50.0, 50.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(30000, 30000, 30000, 0, 60000, 0);

		rollupManager = setupRollupManager(-50.0, 100.0, cashAdjustmentType);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(7500, 45000, 7500, 0, -7500, 0);
	}


	@Test
	public void testRollupMOC_CustomCashPercent() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.CUSTOM_PERCENT;

		InvestmentManagerAccount rollupManager = setupRollupManager(100.0, 100.0, cashAdjustmentType, 75.0, null);
		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(60000, 60000, 90000, 30000, 172500, 57500);

		rollupManager = setupRollupManager(50.0, 50.0, cashAdjustmentType, 75.0, null);
		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(30000, 30000, 45000, 15000, 86250, 28750);

		rollupManager = setupRollupManager(-50.0, 100.0, cashAdjustmentType, 75.0, null);
		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(7500, 45000, 39375, 13125, 9375, 3125);
	}


	@Test
	public void testRollupMOC_CustomCashValue_V1() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.CUSTOM_VALUE;
		InvestmentManagerAccount rollupManager = setupRollupManager(100.0, 100.0, cashAdjustmentType, 30000.0, null);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(60000, 60000, 30000, 90000, 30000, 200000);

		rollupManager = setupRollupManager(50.0, 50.0, cashAdjustmentType, 30000.0, null);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(30000, 30000, 30000, 85000);

		rollupManager = setupRollupManager(-50.0, 100.0, cashAdjustmentType, 30000.0, null);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(7500, 45000, 30000, 22500, 30000, -17500);
	}


	@Test
	public void testRollupMOC_CustomCashValue_V2() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.CUSTOM_VALUE;
		InvestmentManagerAccount rollupManager = setupRollupManager(100.0, 100.0, cashAdjustmentType, 200000.0, null);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(60000, 60000, 200000, -80000, 200000, 30000);

		rollupManager = setupRollupManager(50.0, 50.0, cashAdjustmentType, 200000.0, null);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(30000, 30000, 200000, -140000, 200000, -85000);

		rollupManager = setupRollupManager(-50.0, 100.0, cashAdjustmentType, 200000.0, null);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(7500, 45000, 200000, -147500, 200000, -187500);
	}


	@Test
	public void testRollupMOC_CashPercentManager() {
		InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType = InvestmentManagerAccount.CashAdjustmentType.MANAGER;

		// MANAGER 2 HAS A CASH % OF: 25,000 / 75,000 =  33.333...%
		InvestmentManagerAccount rollupManager = setupRollupManager(100.0, 100.0, cashAdjustmentType, null, MANAGER2_ID);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(60000, 60000, 40000, 80000, 76666.67, 153333.33);

		rollupManager = setupRollupManager(50.0, 50.0, cashAdjustmentType, null, MANAGER2_ID);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(30000, 30000, 20000, 40000, 38333.33, 76666.67);

		rollupManager = setupRollupManager(-50.0, 100.0, cashAdjustmentType, null, MANAGER2_ID);

		this.productManagerService.processProductManagerAccountRollupBalance(rollupManager, TEST_BALANCE_DATE);
		validateRollupBalance(7500, 45000, 17500, 35000, 4166.67, 8333.33);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private InvestmentManagerAccount setupRollupManager(Double child1Percentage, Double child2Percentage, InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType) {
		return setupRollupManager(child1Percentage, child2Percentage, cashAdjustmentType, null, null);
	}


	private InvestmentManagerAccount setupRollupManager(Double child1Percentage, Double child2Percentage, InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType, Double customCashValue, Integer cashPercentManagerId) {
		InvestmentManagerAccount rollupManager = this.investmentManagerAccountService.getInvestmentManagerAccount(ROLLUP_MANAGER_ID);
		rollupManager.setRollupManagerList(new ArrayList<>());
		rollupManager.setRollupAggregationType(AggregationOperations.SUM);
		if (child1Percentage != null) {
			InvestmentManagerAccountRollup rollup = new InvestmentManagerAccountRollup();
			rollup.setReferenceOne(rollupManager);
			rollup.setReferenceTwo(this.investmentManagerAccountService.getInvestmentManagerAccount(MANAGER1_ID));
			rollup.setAllocationPercent(BigDecimal.valueOf(child1Percentage));
			rollupManager.getRollupManagerList().add(rollup);
		}
		if (child2Percentage != null) {
			InvestmentManagerAccountRollup rollup = new InvestmentManagerAccountRollup();
			rollup.setReferenceOne(rollupManager);
			rollup.setReferenceTwo(this.investmentManagerAccountService.getInvestmentManagerAccount(MANAGER2_ID));
			rollup.setAllocationPercent(BigDecimal.valueOf(child2Percentage));
			rollupManager.getRollupManagerList().add(rollup);
		}
		rollupManager.setCashAdjustmentType(cashAdjustmentType);
		if (cashAdjustmentType == InvestmentManagerAccount.CashAdjustmentType.CUSTOM_PERCENT || cashAdjustmentType == InvestmentManagerAccount.CashAdjustmentType.CUSTOM_VALUE) {
			Assertions.assertNotNull(customCashValue, "Custom Cash Value is Required for Cash Adjustment Type " + cashAdjustmentType);
			rollupManager.setCustomCashValue(BigDecimal.valueOf(customCashValue));
			rollupManager.setCustomCashValueUpdateDate(TEST_BALANCE_DATE);
		}
		else {
			rollupManager.setCustomCashValue(null);
			rollupManager.setCustomCashValueUpdateDate(null);
		}
		if (cashAdjustmentType == InvestmentManagerAccount.CashAdjustmentType.MANAGER) {
			Assertions.assertNotNull(cashPercentManagerId, "Custom Manager is required for Cash Adjustment Type " + cashAdjustmentType);
			rollupManager.setCashPercentManager(this.investmentManagerAccountService.getInvestmentManagerAccount(cashPercentManagerId));
		}
		else {
			rollupManager.setCashPercentManager(null);
		}
		this.investmentManagerAccountService.saveInvestmentManagerAccount(rollupManager);
		return rollupManager;
	}


	private void validateRollupBalance(double cashValue, double securitiesValue, double mocCashValue, double mocSecuritiesValue) {
		validateRollupBalance(cashValue, securitiesValue, cashValue, securitiesValue, mocCashValue, mocSecuritiesValue);
	}


	private void validateRollupBalance(double cashValue, double securitiesValue, double adjustedCashValue, double adjustedSecuritiesValue, double mocCashValue, double mocSecuritiesValue) {
		InvestmentManagerAccountBalance balance = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceByManagerAndDate(ROLLUP_MANAGER_ID, TEST_BALANCE_DATE, true);
		/* UNCOMMENT IF DEBUGGING - USEFUL FOR REVIEW SPECIFIC MANAGER BALANCE DETAILS
		 System.out.println(balance.getNote());
		 for (InvestmentManagerAccountBalanceAdjustment adjustment : CollectionUtils.getIterable(balance.getAdjustmentList())) {
		 System.out.println(adjustment.getAdjustmentType().getName() + ": " + adjustment.getNote());
		 System.out.println(StringUtils.TAB + "Final Cash Adjustment: " + CoreMathUtils.formatNumberMoney(adjustment.getCashValue()) + " Final Securities Adjustment: " + CoreMathUtils.formatNumberMoney(adjustment.getSecuritiesValue()));
		 }
		 */
		validateAmount("Original Cash", cashValue, balance.getCashValue());
		validateAmount("Original Securities", securitiesValue, balance.getSecuritiesValue());
		validateAmount("Adjusted Cash", adjustedCashValue, balance.getAdjustedCashValue());
		validateAmount("Adjusted Securities", adjustedSecuritiesValue, balance.getAdjustedSecuritiesValue());
		validateAmount("MOC Cash", mocCashValue, balance.getMarketOnCloseCashValue());
		validateAmount("MOC Securities", mocSecuritiesValue, balance.getMarketOnCloseSecuritiesValue());
	}


	private void validateAmount(String label, double expectedAmount, BigDecimal actualAmount) {
		Assertions.assertEquals(MathUtils.round(BigDecimal.valueOf(expectedAmount), 2), MathUtils.round(actualAmount, 2), "Incorrect " + label);
	}
}
