package com.clifton.product.overlay.mock;


import com.clifton.core.beans.BeanUtils;
import com.clifton.marketdata.rates.MarketDataExchangeRate;
import com.clifton.marketdata.rates.MarketDataRatesServiceImpl;
import com.clifton.marketdata.rates.search.ExchangeRateSearchForm;

import java.util.List;


public class MarketDataRatesServiceImplMock extends MarketDataRatesServiceImpl {

	@Override
	public List<MarketDataExchangeRate> getMarketDataExchangeRateList(ExchangeRateSearchForm searchForm) {
		List<MarketDataExchangeRate> list = getMarketDataExchangeRateDAO().findAll();
		if (searchForm.getDataSourceId() != null) {
			list = BeanUtils.filter(list, exchangeRate -> exchangeRate.getDataSource().getId(), searchForm.getDataSourceId());
		}
		else if (searchForm.getDataSourceName() != null) {
			list = BeanUtils.filter(list, exchangeRate -> exchangeRate.getDataSource().getName(), searchForm.getDataSourceName());
		}
		if (searchForm.getFromCurrencyId() != null) {
			list = BeanUtils.filter(list, exchangeRate -> exchangeRate.getFromCurrency().getId(), searchForm.getFromCurrencyId());
		}
		if (searchForm.getToCurrencyId() != null) {
			list = BeanUtils.filter(list, exchangeRate -> exchangeRate.getToCurrency().getId(), searchForm.getToCurrencyId());
		}
		if (searchForm.getRateDate() != null) {
			list = BeanUtils.filter(list, MarketDataExchangeRate::getRateDate, searchForm.getRateDate());
		}
		return list;
	}
}
