package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration("ProductOverlayMinimizeImbalancesCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculatorTests extends BaseProductOverlayMinimizeImbalancesCalculatorTests {

	@Resource
	private SystemColumnValueHandler systemColumnValueHandler;


	@Override
	public ProductOverlayMinimizeImbalancesCalculator getCalculator() {
		return new ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator();
	}


	@Test
	public void test_NotEnoughCashForPriority1() {
		// 079800: BVU Fund, LP on	04/10/2014	- Changed cash total to 5 million which is less than needed for Deflation Hedging
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InvestmentAccountAssetClass ac = (InvestmentAccountAssetClass) args[0];
			if ("Deflation Hedging".equals(ac.getAssetClass().getName())) {
				return 1;
			}
			if (ac.getAssetClass().getName().contains("Equity")) {
				return 2;
			}
			return null;
		});

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2633, "Deflation Hedging").withAssetClassPercentage(13.5).withAdjustedTarget(17711310.54).withPhysicalExposure(12128964.26).withOverlayExposure(5650224.32));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2634, "Inflation Hedging").withAssetClassPercentage(8.5).withAdjustedTarget(11151565.90).withPhysicalExposure(10897826.39));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2635, "Cash").withAssetClassPercentage(0).withAdjustedTarget(0).withPhysicalExposure(10625305.02).withOverlayExposure(-10601520.84).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 5000000));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2936, "U.S. Equity").withAssetClassPercentage(29.2).withAdjustedTarget(38308908.72).withPhysicalExposure(31300461.37).withOverlayExposure(4951296.52));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2937, "World ex U.S. Equity").withAssetClassPercentage(24.57).withAdjustedTarget(32234585.18).withPhysicalExposure(32218024.66));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2938, "Emerging Markets Equity").withAssetClassPercentage(6.23).withAdjustedTarget(8173441.83).withPhysicalExposure(8384213.17));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2939, "Non-Beta Cash").withAssetClassPercentage(18).withAdjustedTarget(23615080.72).withPhysicalExposure(25640098.02));

		processCalculation(config);

		validateAssetClassAllocation(config, "Deflation Hedging", 0, 5000000.0, 0.00, 0.0, 0.00, 0.00);
		validateZeroAssetClassAllocation(config, "Inflation Hedging");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "U.S. Equity");
		validateZeroAssetClassAllocation(config, "World ex U.S. Equity");
		validateZeroAssetClassAllocation(config, "Emerging Markets Equity");
		validateZeroAssetClassAllocation(config, "Non-Beta Cash");

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(config, "Cash Available for Minimize Imbalances: 5,000,000.00");
	}


	@Test
	public void test_NotEnoughCashForPriority2_RebalanceBandsTriggered() {
		// 079800: BVU Fund, LP on	04/10/2014	
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InvestmentAccountAssetClass ac = (InvestmentAccountAssetClass) args[0];
			if ("Deflation Hedging".equals(ac.getAssetClass().getName())) {
				return 1;
			}
			if (ac.getAssetClass().getName().contains("Equity")) {
				return 2;
			}
			return null;
		});

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2633, "Deflation Hedging").withAssetClassPercentage(13.5).withAdjustedTarget(17711310.54).withPhysicalExposure(12128964.26).withOverlayExposure(5650224.32).withRebalanceTriggers(-1311948.93, 1311948.93));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2634, "Inflation Hedging").withAssetClassPercentage(8.5).withAdjustedTarget(11151565.90).withPhysicalExposure(10897826.39));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2635, "Cash").withAssetClassPercentage(0).withAdjustedTarget(0).withPhysicalExposure(10625305.02).withOverlayExposure(-10601520.84).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 10625305.02));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2936, "U.S. Equity").withAssetClassPercentage(29.2).withAdjustedTarget(38308908.72).withPhysicalExposure(31300461.37).withOverlayExposure(4951296.52).withRebalanceTriggers(-1311948.93, 1311948.93));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2937, "World ex U.S. Equity").withAssetClassPercentage(24.57).withAdjustedTarget(32234585.18).withPhysicalExposure(32218024.66).withRebalanceTriggers(-1311948.93, 1311948.93));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2938, "Emerging Markets Equity").withAssetClassPercentage(6.23).withAdjustedTarget(8173441.83).withPhysicalExposure(8384213.17).withRebalanceTriggers(-1311948.93, 1311948.93));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2939, "Non-Beta Cash").withAssetClassPercentage(18).withAdjustedTarget(23615080.72).withPhysicalExposure(25640098.02));

		processCalculation(config);

		validateAssetClassAllocation(config, "Deflation Hedging", 0.00, 1951586.64, 0.00, 0.00, 0.00, 3630759.64);
		validateZeroAssetClassAllocation(config, "Inflation Hedging");
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "U.S. Equity", 0.00, 4221209.61, 0.00, 0.00, 0.00, 821749.13);
		validateAssetClassAllocation(config, "World ex U.S. Equity", 0.00, 3551887.68, 0.00, 0.00, 0.00, -3551887.68);
		validateAssetClassAllocation(config, "Emerging Markets Equity", 0.00, 900621.09, 0.00, 0.00, 0.00, -900621.09);
		validateZeroAssetClassAllocation(config, "Non-Beta Cash");

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(config,
				"Cash Available for Minimize Imbalances: 10,625,305.02");
	}


	@Test
	public void test_NotEnoughCashForPriority2_RebalanceBandsNotTriggered_OverweightPriority1() {
		// 730100: Woodtiger, LP	04/10/2014	
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InvestmentAccountAssetClass ac = (InvestmentAccountAssetClass) args[0];
			if ("Deflation Hedging".equals(ac.getAssetClass().getName())) {
				return 1;
			}
			if (ac.getAssetClass().getName().contains("Equity")) {
				return 2;
			}
			return null;
		});


		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2637, "Inflation Hedging").withAssetClassPercentage(5).withAdjustedTarget(3329214.90).withPhysicalExposure(3470240.86));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2638, "Deflation Hedging").withAssetClassPercentage(10).withAdjustedTarget(6658429.79).withPhysicalExposure(4801748.19).withOverlayExposure(1883408.11).withRebalanceTriggers(-665842.98, 665842.98));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2639, "Cash").withAssetClassPercentage(0).withAdjustedTarget(0).withPhysicalExposure(3763163.74).withOverlayExposure(-3697686.82).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 3763163.74));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2962, "US Equity").withAssetClassPercentage(28.96).withAdjustedTarget(19282812.69).withPhysicalExposure(17290716.95).withOverlayExposure(1558741.50).withRebalanceTriggers(-665842.98, 665842.98));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2963, "World ex U.S. Equity").withAssetClassPercentage(24.36).withAdjustedTarget(16219934.98).withPhysicalExposure(16263935.38).withRebalanceTriggers(-665842.98, 665842.98));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2964, "Emerging Markets Equity").withAssetClassPercentage(6.18).withAdjustedTarget(4114909.61).withPhysicalExposure(3898598.21).withOverlayExposure(255537.21).withRebalanceTriggers(-665842.98, 665842.98));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2965, "Non-Beta Cash").withAssetClassPercentage(25.5).withAdjustedTarget(16978995.97).withPhysicalExposure(17095894.62));


		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Inflation Hedging");
		validateAssetClassAllocation(config, "Deflation Hedging", 0.00, 541462.41, 0.00, 0.00, 0.00, 1341945.70);
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "US Equity", 0.00, 1568075.14, 0.00, 0.00, 0.00, 56143.28);
		validateAssetClassAllocation(config, "World ex U.S. Equity", 0.00, 1319002.43, 0.00, 0.00, 0.00, -1319002.43);
		validateAssetClassAllocation(config, "Emerging Markets Equity", 0.00, 334623.77, 0.00, 0.00, 0.00, -79086.56);
		validateZeroAssetClassAllocation(config, "Non-Beta Cash");

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(config,
				"Cash Available for Minimize Imbalances: 3,763,163.74");
	}


	@Test
	public void test_NotEnoughCashForPriority2_RebalanceBandsNotTriggered_UnderweightPriority1() {
		// 730100: Woodtiger, LP	04/10/2014	
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InvestmentAccountAssetClass ac = (InvestmentAccountAssetClass) args[0];
			if ("Deflation Hedging".equals(ac.getAssetClass().getName())) {
				return 1;
			}
			if (ac.getAssetClass().getName().contains("Equity")) {
				return 2;
			}
			return null;
		});

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2637, "Inflation Hedging").withAssetClassPercentage(5).withAdjustedTarget(3329214.90).withPhysicalExposure(3470240.86));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2638, "Deflation Hedging").withAssetClassPercentage(10).withAdjustedTarget(6658429.79).withPhysicalExposure(4801748.19).withOverlayExposure(1256681.6).withRebalanceTriggers(-665842.98, 665842.98));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2639, "Cash").withAssetClassPercentage(0).withAdjustedTarget(0).withPhysicalExposure(3763163.74).withOverlayExposure(-3697686.82).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 3763163.74));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2962, "US Equity").withAssetClassPercentage(28.96).withAdjustedTarget(19282812.69).withPhysicalExposure(17290716.95).withOverlayExposure(1558741.50).withRebalanceTriggers(-665842.98, 665842.98));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2963, "World ex U.S. Equity").withAssetClassPercentage(24.36).withAdjustedTarget(16219934.98).withPhysicalExposure(16263935.38).withRebalanceTriggers(-665842.98, 665842.98));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2964, "Emerging Markets Equity").withAssetClassPercentage(6.18).withAdjustedTarget(4114909.61).withPhysicalExposure(3898598.21).withOverlayExposure(255537.21).withRebalanceTriggers(-665842.98, 665842.98));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2965, "Non-Beta Cash").withAssetClassPercentage(25.5).withAdjustedTarget(16978995.97).withPhysicalExposure(17095894.62));


		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Inflation Hedging");
		validateAssetClassAllocation(config, "Deflation Hedging", 0.00, 541462.41, 0.00, 0.00, 0.00, 1315219.19);
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "US Equity", 0.00, 1568075.14, 0.00, 0.00, 0.00, 82869.79);
		validateAssetClassAllocation(config, "World ex U.S. Equity", 0.00, 1319002.43, 0.00, 0.00, 0.00, -1319002.43);
		validateAssetClassAllocation(config, "Emerging Markets Equity", 0.00, 334623.77, 0.00, 0.00, 0.00, -79086.56);
		validateZeroAssetClassAllocation(config, "Non-Beta Cash");

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(config,
				"Cash Available for Minimize Imbalances: 3,763,163.74");
	}


	@Test
	public void test_EnoughCashForPriority2_RebalanceBandsNotTriggered_UnderweightPriority1() {
		// 730100: Woodtiger, LP	Test from QA	
		// Test that Priority 1 is given extra effective cash without causing a sell in priority 2
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InvestmentAccountAssetClass ac = (InvestmentAccountAssetClass) args[0];
			if ("Deflation Hedging".equals(ac.getAssetClass().getName())) {
				return 1;
			}
			if (ac.getAssetClass().getName().contains("Equity")) {
				return 2;
			}
			return null;
		});

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2637, "Inflation Hedging").withAssetClassPercentage(5).withAdjustedTarget(3343233.72).withPhysicalExposure(3510831.58));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2638, "Deflation Hedging").withAssetClassPercentage(10).withAdjustedTarget(6686467.45).withPhysicalExposure(4842903.87).withOverlayExposure(1589200.95).withRebalanceTriggers(-668646.74, 668646.74));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2639, "Cash").withAssetClassPercentage(0).withAdjustedTarget(0).withPhysicalExposure(3473793.06).withOverlayExposure(-3441784.97).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 3473793.06));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2962, "US Equity").withAssetClassPercentage(28.96).withAdjustedTarget(19364009.74).withPhysicalExposure(17572340.55).withOverlayExposure(1602281.46).withRebalanceTriggers(-668646.74, 668646.74));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2963, "World ex U.S. Equity").withAssetClassPercentage(24.36).withAdjustedTarget(16288234.70).withPhysicalExposure(16477766.67).withRebalanceTriggers(-668646.74, 668646.74));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2964, "Emerging Markets Equity").withAssetClassPercentage(6.18).withAdjustedTarget(4132236.88).withPhysicalExposure(3900834.54).withOverlayExposure(250302.56).withRebalanceTriggers(-668646.74, 668646.74));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2965, "Non-Beta Cash").withAssetClassPercentage(25.5).withAdjustedTarget(17050491.99).withPhysicalExposure(17086204.20));


		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Inflation Hedging");
		validateAssetClassAllocation(config, "Deflation Hedging", 0.00, 499826.34, 0.00, 0.00, 0.00, 1121382.70);
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "US Equity", 0.00, 1447497.08, 0.00, 0.00, 0.00, 154784.38);
		validateAssetClassAllocation(config, "World ex U.S. Equity", 0.00, 1217576.96, 0.00, 0.00, 0.00, -1217576.96);
		validateAssetClassAllocation(config, "Emerging Markets Equity", 0.00, 308892.68, 0.00, 0.00, 0.00, -58590.12);
		validateZeroAssetClassAllocation(config, "Non-Beta Cash");

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(config,
				"Cash Available for Minimize Imbalances: 3,473,793.06");
	}


	@Test
	public void test_EnoughCashForPriority2_ApplyCashBuffer_RebalanceBandsTriggered_UnderweightPriority1() {
		// 730100: Woodtiger, LP	Test from QA	
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InvestmentAccountAssetClass ac = (InvestmentAccountAssetClass) args[0];
			if ("Deflation Hedging".equals(ac.getAssetClass().getName())) {
				return 1;
			}
			if (ac.getAssetClass().getName().contains("Equity")) {
				return 2;
			}
			return null;
		});

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2637, "Inflation Hedging").withAssetClassPercentage(5).withAdjustedTarget(3368233.72).withPhysicalExposure(3510831.58));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2638, "Deflation Hedging").withAssetClassPercentage(10).withAdjustedTarget(6736467.45).withPhysicalExposure(5042903.87).withOverlayExposure(1589200.95).withRebalanceTriggers(-673646.74, 673646.74));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2639, "Cash").withAssetClassPercentage(0).withAdjustedTarget(0).withPhysicalExposure(3473793.06).withOverlayExposure(-3441784.97).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 3873793.06));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2962, "US Equity").withAssetClassPercentage(28.96).withAdjustedTarget(19508809.74).withPhysicalExposure(17212340.55).withOverlayExposure(1602281.46).withRebalanceTriggers(-673646.74, 673646.74));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2963, "World ex U.S. Equity").withAssetClassPercentage(24.36).withAdjustedTarget(16410034.70).withPhysicalExposure(16477766.67).withRebalanceTriggers(-673646.74, 673646.74));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2964, "Emerging Markets Equity").withAssetClassPercentage(6.18).withAdjustedTarget(4163136.88).withPhysicalExposure(4350834.54).withOverlayExposure(250302.56).withRebalanceTriggers(-673646.74, 673646.74));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2965, "Non-Beta Cash").withAssetClassPercentage(25.5).withAdjustedTarget(17177991.99).withPhysicalExposure(16896204.20));


		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Inflation Hedging");
		validateAssetClassAllocation(config, "Deflation Hedging", 0.00, 557380.30, 0.00, 0.00, 0.00, 1136183.28);
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "US Equity", 0.00, 1614173.34, 0.00, 0.00, 0.00, 315753.58);
		validateAssetClassAllocation(config, "World ex U.S. Equity", 0.00, 1357778.40, 0.00, 0.00, 0.00, -1357778.40);
		validateAssetClassAllocation(config, "Emerging Markets Equity", 0.00, 344461.02, 0.00, 0.00, 0.00, -94158.46);
		validateZeroAssetClassAllocation(config, "Non-Beta Cash");

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(config,
				"Cash Available for Minimize Imbalances: 3,873,793.06");
	}


	@Test
	public void test_ExtraCash_ApplyAsCashBuffer() {
		// 079800: BVU Fund, LP on	04/10/2014	- Changed cash available to more than needed to cause cash buffer to be applied
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesOrderedCashConstrainedCalculator.PRIORITY_ORDER_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InvestmentAccountAssetClass ac = (InvestmentAccountAssetClass) args[0];
			if ("Deflation Hedging".equals(ac.getAssetClass().getName())) {
				return 1;
			}
			if (ac.getAssetClass().getName().contains("Equity")) {
				return 2;
			}
			return null;
		});


		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2633, "Deflation Hedging").withAssetClassPercentage(13.5).withAdjustedTarget(17711310.54).withPhysicalExposure(12128964.26).withOverlayExposure(5650224.32));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2634, "Inflation Hedging").withAssetClassPercentage(8.5).withAdjustedTarget(11151565.90).withPhysicalExposure(10897826.39));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2635, "Cash").withAssetClassPercentage(0).withAdjustedTarget(0).withPhysicalExposure(10625305.02).withOverlayExposure(-10601520.84).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 20000000.0));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2936, "U.S. Equity").withAssetClassPercentage(29.2).withAdjustedTarget(38308908.72).withPhysicalExposure(31300461.37).withOverlayExposure(4951296.52));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2937, "World ex U.S. Equity").withAssetClassPercentage(24.57).withAdjustedTarget(32234585.18).withPhysicalExposure(32218024.66));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2938, "Emerging Markets Equity").withAssetClassPercentage(6.23).withAdjustedTarget(8173441.83).withPhysicalExposure(8384213.17));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2939, "Non-Beta Cash").withAssetClassPercentage(18).withAdjustedTarget(23615080.72).withPhysicalExposure(25640098.02));

		processCalculation(config);

		validateAssetClassAllocation(config, "Deflation Hedging", 0, 3673469.39, 0.00, 0.0, 0.00, 1976754.93);
		validateZeroAssetClassAllocation(config, "Inflation Hedging");
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "U.S. Equity", 0, 7945578.23, 0.00, 0.0, 0.00, -937130.88);
		validateAssetClassAllocation(config, "World ex U.S. Equity", 0, 6685714.29, 0.00, 0.00, 0.00, -6669153.77);
		validateAssetClassAllocation(config, "Emerging Markets Equity", 0, 1695238.10, 0, 0, 0, -1695238.10);
		validateZeroAssetClassAllocation(config, "Non-Beta Cash");

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 20,000,000.00 Extra Cash Left Over (Applied As Cash Buffer): 7,324,767.81");
	}
}
