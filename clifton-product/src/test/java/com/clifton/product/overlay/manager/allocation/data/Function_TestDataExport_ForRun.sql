DROP FUNCTION IF EXISTS TestDataExport

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION TestDataExport(
	-- Add the parameters for the function here
	@TableName NVARCHAR(50), @ClassName NVARCHAR(100), @ID INT)
	RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE
		@ColumnName         NVARCHAR(50),
		@BeanPropertyPrefix NVARCHAR(100),
		@DataTypeName       NVARCHAR(50)

	DECLARE
		@SqlString NVARCHAR(MAX) = 'SELECT ''<bean id="' + @TableName + '-' + CAST(@ID AS VARCHAR) + '" class="' + @ClassName + '" '

	DECLARE ColumnCursor CURSOR FORWARD_ONLY FOR
		SELECT DISTINCT c.ColumnName,
						dt.DataTypeName,
						'p:' + c.BeanPropertyName + CASE WHEN r.SystemRelationshipID IS NOT NULL THEN '-ref="' + pct.TableName + '-''' ELSE '="''' END
		FROM SystemColumn c
			 INNER JOIN SystemTable t ON c.SystemTableID = t.SystemTableID
			 INNER JOIN SystemDataType dt ON c.SystemDataTypeID = dt.SystemDataTypeID
			 LEFT JOIN SystemRelationship r ON c.SystemColumnID = r.SystemColumnID
			 LEFT JOIN SystemColumn pc ON r.ParentSystemColumnID = pc.SystemColumnID
			 LEFT JOIN SystemTable pct ON pc.SystemTableID = pct.SystemTableID
		WHERE t.TableName = @TableName
		  AND c.BeanPropertyName NOT IN ('rv', 'createUserId', 'createDate', 'updateUserId', 'updateDate', 'workflowstate', 'workflowstatus', 'violationstatus')
		  AND NOT (t.TableName = 'PortfolioRun' AND c.ColumnName IN
													('PortfolioTotalValue', 'Notes', 'CashTotalValue', 'OverlayTargetTotalValue', 'RuleViolationStatusID', 'WorkflowStateID',
													 'WorkflowStatusID', 'OverlayExposureTotalValue', 'CashTarget', 'CashExposure', 'CashTargetPercent', 'MispricingTotalValue',
													 'CashExposurePercent', 'CashExposureWithMispricingPercent', 'DocumentFileCount', 'IsPostedToPortal'))
		  AND NOT (t.TableName = 'InvestmentAccount' AND c.ColumnName IN
														 ('BusinessClientID', 'InvestmentAccountTypeID', 'IssuingCompanyID', 'BusinessServiceID', 'TeamSecurityGroupID',
														  'AccountDescription', 'WorkflowStateID', 'WorkflowStatusID', 'IsUsedForSpeculation', 'BusinessContractID',
														  'WorkflowStateEffectiveStartDate', 'PerformanceInceptionDate', 'AccountDiscretionType', 'ClientAccountType',
														  'ExternalPortfolioCodeID', 'ExternalProductCodeID', 'AccountNumber2', 'IsClientDirected', 'InitialMarginMultiplier',
														  'AUMCalculatorOverrideBeanID', 'BusinessCompanyPlatformID', 'IsWrapAccount', 'ClientAccountChannelID',
														  'BusinessServiceProcessingTypeID', 'DefaultExchangeRateSourceCompanyID'))
		  AND NOT (t.TableName = 'InvestmentManagerAccount' AND c.ColumnName IN
																('BusinessClientID', 'ManagerBusinessCompanyID', 'CustomCashValue', 'CashPercentManagerAccountID',
																 'CustomCashValueUpdateDate', 'IsProxyValueGrowthInverted', 'ProxyValue', 'ProxyValueDate',
																 'ProxyBenchmarkSecurityID', 'ProxySecurityQuantity', 'IsProxySecuritiesOnly', 'IsZeroBalanceIfNoDownload',
																 'LinkedInvestmentAccountID', 'ProxyBenchmarkInterestRateIndexID', 'LinkedManagerType', 'CashAdjustmentType',
																 'IsProxyToLastKnownPrice', 'BenchmarkSecurityID', 'ProxyBenchmarkSecurityDisplayName', 'CustodianAccountID',
																 'RollupAggregationType', 'DurationFieldNameOverride'))
		  AND NOT (t.TableName = 'InvestmentManagerAccountAssignment' AND
				   c.ColumnName IN ('SecuritiesBalanceReplicationAllocatorBeanID', 'IsRebalanceAllocationFixed', 'RebalanceAllocationMin', 'RebalanceAllocationMax'))
		  AND NOT (t.TableName = 'InvestmentSecurity' AND c.ColumnName IN
														  ('InvestmentInstrumentID', 'BusinessContractID', 'InvestmentSecurityDescription', 'Cusip', 'FirstNoticeDate',
														   'FirstDeliveryDate', 'LastDeliveryDate', 'BigInvestmentSecurityID', 'BusinessCompanyID', 'Isin', 'Sedol',
														   'IsIlliquid', 'UnderlyingSecurityID', 'EarlyTerminationDate', 'OccSymbol', 'PriceMultiplierOverride',
														   'ReferenceInvestmentSecurityID', 'SettlementCurrencyID'))
		  AND NOT (t.TableName = 'InvestmentAccountAssetClass' AND c.ColumnName IN ('PrimaryInvestmentReplicationID', 'SecondaryInvestmentReplicationID',
																					'SecondaryInvestmentReplicationAmount', 'IsRebalanceAllocationFixed',
																					'RebalanceAllocationMin', 'RebalanceAllocationMax', 'RebalanceAllocationAbsoluteMin',
																					'RebalanceAllocationAbsoluteMax', 'RebalanceTriggerAction',
																					'RebalanceBenchmarkInvestmentSecurityID', 'BenchmarkDurationInvestmentSecurityID',
																					'IsDoNotAdjustContractValue', 'IsUseActualForMatchingReplication',
																					'PerformanceOverlayTargetSource', 'DurationFieldNameOverride'))

	OPEN ColumnCursor
	FETCH NEXT FROM ColumnCursor INTO @ColumnName, @DataTypeName, @BeanPropertyPrefix
	WHILE @@Fetch_Status = 0
	BEGIN
		DECLARE
			@ColumnNameSql VARCHAR(100) = CASE
											  WHEN @DataTypeName = 'DATE' THEN ' +  CONVERT(VARCHAR,' + @ColumnName + ', 101)'
											  WHEN @DataTypeName = 'STRING' THEN ' +  REPLACE(' + @ColumnName + ',''&'',''&amp;'')'
											  ELSE ' +  CAST(' + @ColumnName + ' AS VARCHAR)'
										  END + ' + '''


		SET @SqlString = @SqlString + ''' +  CASE WHEN ' + @ColumnName + ' IS NULL THEN '''' ELSE ''' + @BeanPropertyPrefix + '' + @ColumnNameSql + '" '' END + '''

		FETCH NEXT FROM ColumnCursor INTO @ColumnName, @DataTypeName, @BeanPropertyPrefix
	END

	CLOSE ColumnCursor
	DEALLOCATE ColumnCursor

	SET @SqlString = @SqlString + '/>''' + ' FROM ' + @TableName + ' WHERE ' + @TableName + 'ID =' + CAST(@ID AS VARCHAR)

	RETURN @SqlString

END
GO

