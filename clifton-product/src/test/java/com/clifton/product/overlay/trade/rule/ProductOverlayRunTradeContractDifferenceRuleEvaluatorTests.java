package com.clifton.product.overlay.trade.rule;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.trade.PortfolioRunTrade;
import com.clifton.portfolio.run.trade.PortfolioRunTradeService;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@Transactional
public class ProductOverlayRunTradeContractDifferenceRuleEvaluatorTests extends BaseInMemoryDatabaseTests {


	private static final int CONCORDIA_10172019_RUN_ID = 1100330;

	private static final short PORTFOLIO_RUN_STATUS_WORKFLOW_ID = 21;

	private static final Date TRADE_DATE = DateUtils.toDate("10/18/2019");


	////////////////////////////////////////////////////////////////////////////////

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private PortfolioRunService portfolioRunService;

	@Resource
	private PortfolioRunTradeService<PortfolioRunTrade<ProductOverlayAssetClassReplication>, ProductOverlayAssetClassReplication> portfolioRunTradeService;

	@Resource
	private RuleViolationService ruleViolationService;

	@Resource
	private SecurityUserService securityUserService;

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected IdentityObject getUser() {
		SecurityUser user = new SecurityUser();
		user.setId((short) 1);
		user.setUserName("TestUser");
		return user;
	}


	@Override
	public void beforeTest() {
		// save user for foreign key constraint on Trade and TradeGroup
		SecurityUser userTemplate = (SecurityUser) getUser();
		SecurityUser user = this.securityUserService.getSecurityUserByName(userTemplate.getUserName());
		if (user == null) {
			user = this.securityUserService.saveSecurityUser(userTemplate);
			Assertions.assertNotNull(user);
		}
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);


		/*
		 // Note: This is used for the SQL data so we know which beans we need properties for - Uncomment and run when needed
		 List<SystemBean> beanList = this.systemBeanService.getSystemBeanList(new SystemBeanSearchForm());
		 StringBuilder sb = new StringBuilder(20);
		 for (SystemBean bean : CollectionUtils.getIterable(beanList)) {
		 sb.append(bean.getId());
		 sb.append(",");
		 }
		 System.out.println(sb.toString());
		 */
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<org.springframework.core.io.Resource> getFunctionsData() {
		return CollectionUtils.createList(new ClassPathResource("com/clifton/product/ProductInMemoryDatabaseTestsDataFunctions.xml"));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Tests the calculated suggested trade quantity for a replication Must be loaded from the productOverlayTradeHandler so required current positions are populated
	 */
	@Test
	public void testMultipleTradesFromRunRuleEvaluation_NoViolation() {
		validatePortfolioRunSupportsTrading(CONCORDIA_10172019_RUN_ID);

		PortfolioRunTrade<ProductOverlayAssetClassReplication> portfolioRunTrade = this.portfolioRunTradeService.getPortfolioRunTrade(CONCORDIA_10172019_RUN_ID);
		Assertions.assertNotNull(portfolioRunTrade);

		portfolioRunTrade.setTradeDate(TRADE_DATE);

		for (ProductOverlayAssetClassReplication replication : CollectionUtils.getIterable(portfolioRunTrade.getDetailList())) {
			if ("RTYZ19".equals(replication.getSecurity().getSymbol())) {
				replication.setBuyContracts(new BigDecimal(7));
			}
			else if ("PTZ19".equals(replication.getSecurity().getSymbol())) {
				replication.setBuyContracts(new BigDecimal(6));
			}
			else if ("USZ19".equals(replication.getSecurity().getSymbol())) {
				replication.setSellContracts(BigDecimal.ONE);
			}
		}
		TradeGroup tradeGroup = this.portfolioRunTradeService.processPortfolioRunTradeCreation(portfolioRunTrade);
		Assertions.assertEquals(3, tradeGroup.getTradeList().size());
		for (Trade trade : tradeGroup.getTradeList()) {
			AssertUtils.assertEmpty(this.ruleViolationService.getRuleViolationListByLinkedEntity("Trade", trade.getId()), "Did not expect any violations for Trade");
		}
	}


	/**
	 * Tests the calculated suggested trade quantity for a replication Must be loaded from the productOverlayTradeHandler so required current positions are populated
	 */
	@Test
	public void testMultipleTradesFromRunRuleEvaluation_Violations() {
		validatePortfolioRunSupportsTrading(CONCORDIA_10172019_RUN_ID);

		PortfolioRunTrade<ProductOverlayAssetClassReplication> portfolioRunTrade = this.portfolioRunTradeService.getPortfolioRunTrade(CONCORDIA_10172019_RUN_ID);
		Assertions.assertNotNull(portfolioRunTrade);

		portfolioRunTrade.setTradeDate(TRADE_DATE);

		for (ProductOverlayAssetClassReplication replication : CollectionUtils.getIterable(portfolioRunTrade.getDetailList())) {
			if ("ESZ19".equals(replication.getSecurity().getSymbol())) {
				replication.setBuyContracts(new BigDecimal(8));
			}
			else if ("MFSZ19".equals(replication.getSecurity().getSymbol())) {
				replication.setBuyContracts(new BigDecimal(5));
			}
			else if ("USZ19".equals(replication.getSecurity().getSymbol())) {
				replication.setSellContracts(BigDecimal.ONE);
			}
		}
		TradeGroup tradeGroup = this.portfolioRunTradeService.processPortfolioRunTradeCreation(portfolioRunTrade);
		Assertions.assertEquals(3, tradeGroup.getTradeList().size());
		for (Trade trade : tradeGroup.getTradeList()) {
			List<RuleViolation> violationList = this.ruleViolationService.getRuleViolationListByLinkedEntity("Trade", trade.getId());
			if ("ESZ19".equals(trade.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals(1, CollectionUtils.getSize(violationList), "Expected One Violation for ESZ9 Trade");
				Assertions.assertEquals("The trade entered <span class='amountPositive'>8.00</span> is in the same direction of the system suggested trade <span class='amountPositive'>0.36</span> and is outside the suggested band of <span class='amountPositive'>5.00</span> for U.S. Equity - Equity Replication - S&P 500 Mini Futures (ES).", violationList.get(0).getViolationNote());
			}
			else if ("MFSZ19".equals(trade.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals(1, CollectionUtils.getSize(violationList), "Expected One Violation for MFSZ9 Trade");
				Assertions.assertEquals("The trade entered <span class='amountPositive'>5.00</span> is in the opposite direction of the system suggested trade <span class='amountNegative'>-0.24</span> and is outside the suggested band of <span class='amountPositive'>1.00</span> for Non-U.S. Equity - Equity Replication - MSCI EAFE Mini Futures (MFS).", violationList.get(0).getViolationNote());
			}
			else if ("USZ19".equals(trade.getInvestmentSecurity().getSymbol())) {
				AssertUtils.assertEmpty(violationList, "Did not expect any violations for USZ9 trade");
			}
			else {
				Assertions.fail("Unexpected trade for security " + trade.getInvestmentSecurity().getSymbol() + " found");
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validatePortfolioRunSupportsTrading(int runId) {
		PortfolioRun run = this.portfolioRunService.getPortfolioRun(runId);
		Assertions.assertNotNull(run);
		if (!"Pending".equals(run.getWorkflowState().getStatus().getName())) {
			WorkflowState pendingState = this.workflowDefinitionService.getWorkflowStateByName(PORTFOLIO_RUN_STATUS_WORKFLOW_ID, "Analyst Approved");
			run.setWorkflowState(pendingState);
			run.setWorkflowStatus(pendingState.getStatus());
			DaoUtils.executeWithAllObserversDisabled(() -> this.portfolioRunService.savePortfolioRun(run));
		}
	}
}
