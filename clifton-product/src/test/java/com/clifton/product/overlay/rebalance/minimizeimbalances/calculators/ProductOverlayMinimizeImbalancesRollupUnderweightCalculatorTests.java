package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration("ProductOverlayMinimizeImbalancesCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ProductOverlayMinimizeImbalancesRollupUnderweightCalculatorTests extends BaseProductOverlayMinimizeImbalancesCalculatorTests {

	@Resource
	private SystemColumnValueHandler systemColumnValueHandler;


	@Override
	public ProductOverlayMinimizeImbalancesCalculator getCalculator() {
		ProductOverlayMinimizeImbalancesRollupUnderweightCalculator calculator = new ProductOverlayMinimizeImbalancesRollupUnderweightCalculator();
		calculator.setShortTargetsAllowed(true);
		calculator.setExpandShortPositionsAllowed(true);
		calculator.setAttributionRebalancing(true);
		return calculator;
	}


	@Test
	public void test_ApplyToChildrenForMinimizeImbalances() {
		// 423000 M&M on 10/11
		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.APPLY_ROLLUP_PROPORTIONAL_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(false);

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.EXCLUDE_ROLLUP_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(false);

		ProductOverlayRunConfig config = setupProductOverlayRunConfig();
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2136, "Equity").asRollup().withAssetClassPercentage(58.41));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2137, "Fixed").asRollup().withAssetClassPercentage(31.50));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1672, "Tactical").withAssetClassPercentage(10.07).withAdjustedTarget(190332260.20).withPhysicalExposure(190332260.20));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(548, "Cash").withAssetClassPercentage(0).withAdjustedTarget(0).withPhysicalExposure(13423965.13).withOverlayExposure(-13643473.86).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 10875913.25));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(556, "Private Equity/Debt").withAssetClassPercentage(0.02).withAdjustedTarget(1199637.90).withPhysicalExposure(1199637.90));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(549, "Domestic Equity").withAssetClassPercentage(21.67).withAdjustedTarget(419975567.32).withPhysicalExposure(424685531.89).withOverlayExposure(-2293690.85).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 2200888.29));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(550, "Fixed Income").withAssetClassPercentage(19.62).withAdjustedTarget(362556933.97).withPhysicalExposure(354031183.85).withOverlayExposure(4593271.56));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(551, "Fixed Income-Short Duration").withAssetClassPercentage(5.27).withAdjustedTarget(97437176.01).withPhysicalExposure(90735975.51).withOverlayExposure(4054029.71));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(552, "International Equity").withAssetClassPercentage(17.73).withAdjustedTarget(343668271.24).withPhysicalExposure(344185007.79).withOverlayExposure(3104780.89).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 347163.59));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(554, "Intl Equity-Emerging").withAssetClassPercentage(6.75).withAdjustedTarget(130921246.18).withPhysicalExposure(127497530.55).withOverlayExposure(4185082.54));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClass(555, "Long/Short Equity").withAssetClassPercentage(12.26).withAdjustedTarget(211353379.67).withPhysicalExposure(211353379.67));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClass(557, "Inflation-Related").withAssetClassPercentage(6.61).withAdjustedTarget(136403204.18).withPhysicalExposure(136403204.18));

		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Equity");
		validateRollupAssetClassAllocation(config, "Fixed");
		validateZeroAssetClassAllocation(config, "Tactical");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Private Equity/Debt");
		validateAssetClassAllocation(config, "Domestic Equity", 2200888.29, 3317582.21, 0.00, 0.00, 0.00, -7812161.35);
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 3003736.18, 0.00, 0.00, 0.00, 1589535.38);
		validateAssetClassAllocation(config, "Fixed Income-Short Duration", 0.00, 806813.95, 0.00, 0.00, 0.00, 3247215.76);
		validateAssetClassAllocation(config, "International Equity", 347163.59, 2714385.44, 0.00, 0.00, 0.00, -176276.86);
		validateAssetClassAllocation(config, "Intl Equity-Emerging", 0.00, 1033395.47, 0.00, 0.00, 0.00, 3151687.07);
		validateZeroAssetClassAllocation(config, "Long/Short Equity");
		validateZeroAssetClassAllocation(config, "Inflation-Related");
	}


	@Test
	public void test_ApplyToChildrenForMinimizeImbalances_AttributionCheck() {
		// Test case added for PERFORMANC-278 FROM Attachment to confirm minimize imbalances attribution
		// 423000 M&M on 10/13/2017
		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.APPLY_ROLLUP_PROPORTIONAL_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(false);

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.EXCLUDE_ROLLUP_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(false);

		ProductOverlayRunConfig config = setupProductOverlayRunConfig();
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2133, "Equity").asRollup().withAssetClassPercentage(63.00000).withCashPercentage(0.00000));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2134, "Fixed").asRollup().withAssetClassPercentage(27.00000).withCashPercentage(0.00000));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClass(4345, "Special Fixed Income").withAssetClassPercentage(4.50000).withCashPercentage(0.00000).withAdjustedTarget(46359419.86).withPhysicalExposure(46359419.86).withOverlayExposure(0.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(548, "Cash").withAssetClassPercentage(0.00000).withCashPercentage(0.00000).withAdjustedTarget(0.00).withPhysicalExposure(19831392.92).withOverlayExposure(-19592066.81).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 17748113.50));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(549, "Domestic Equity").withAssetClassPercentage(28.50000).withCashPercentage(0.00000).withAdjustedTarget(346645405.98).withPhysicalExposure(342813075.69).withOverlayExposure(6724253.06).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1756738.53));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(550, "Fixed Income").withAssetClassPercentage(18.00000).withCashPercentage(0.00000).withAdjustedTarget(226063011.69).withPhysicalExposure(210855697.96).withOverlayExposure(10546092.57));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(551, "Fixed Income-Short Term").withAssetClassPercentage(4.50000).withCashPercentage(0.00000).withAdjustedTarget(56515752.93).withPhysicalExposure(50954228.92).withOverlayExposure(3752480.86));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(552, "International Equity").withAssetClassPercentage(13.50000).withCashPercentage(0.00000).withAdjustedTarget(164200455.47).withPhysicalExposure(166955612.84).withOverlayExposure(-1205081.18).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 326540.89));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(554, "Intl Equity Emerging").withAssetClassPercentage(13.50000).withCashPercentage(0.00000).withAdjustedTarget(164200455.47).withPhysicalExposure(166215073.21).withOverlayExposure(-225678.49));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClass(555, "Long/Short Equity").withAssetClassPercentage(7.50000).withCashPercentage(0.00000).withAdjustedTarget(92524356.52).withPhysicalExposure(92524356.52).withOverlayExposure(0.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(556, "Private Equity/Debt").withAssetClassPercentage(0.00000).withCashPercentage(0.00000).withAdjustedTarget(742163.96).withPhysicalExposure(742163.96).withOverlayExposure(0.00));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClass(557, "Inflation-Related").withAssetClassPercentage(0.00000).withCashPercentage(0.00000).withAdjustedTarget(0.00).withPhysicalExposure(0.00).withOverlayExposure(0.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1672, "Tactical").withAssetClassPercentage(10.00000).withCashPercentage(0.00000).withAdjustedTarget(119698467.14).withPhysicalExposure(119698467.14).withOverlayExposure(0.00));

		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Equity");
		validateRollupAssetClassAllocation(config, "Fixed");
		validateAssetClassAllocation(config, "Tactical", 0.00, 0.00, 0.00, 0.00, 0.00, 0.000);
		validateAssetClassAllocation(config, "Cash", 0.00, 0.00, 0.00, 0.00, 0.00, 0.000);
		validateAssetClassAllocation(config, "Private Equity/Debt", 0.00, 0.00, 0.00, 0.00, 0.00, 0.000);
		validateAssetClassAllocation(config, "Domestic Equity", 1756738.53, 6484887.63, 0.00, 0.00, 0.00, -1517373.10);
		validateAssetClassAllocation(config, "Fixed Income", 0, 4095718.5, 0.00, 0.00, 0.00, 6450374.07);
		validateAssetClassAllocation(config, "Fixed Income-Short Term", 0, 1023929.63, 0.00, 0.00, 0.00, 2967877.33);
		validateAssetClassAllocation(config, "International Equity", 326540.89, 3071788.87, 0.00, 0.00, 0.00, -4603410.94);
		validateAssetClassAllocation(config, "Intl Equity Emerging", 0, 3071788.87, 0.00, 0.00, 0.00, -3297467.36);
		validateAssetClassAllocation(config, "Long/Short Equity", 0.00, 0.00, 0.00, 0.00, 0.00, 0.000);
		validateAssetClassAllocation(config, "Inflation-Related", 0.00, 0.00, 0.00, 0.00, 0.00, 0.000);
	}


	@Test
	public void test_ApplyToChildrenForMinimizeImbalances_AttributionCheck_CashTargetAdjustment() {
		// Test case added for PERFORMANC-278 from test run on QA to confirm cash target adjustment
		// Not a real run 423000 M&M on 1/10/2018
		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.APPLY_ROLLUP_PROPORTIONAL_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(false);

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.EXCLUDE_ROLLUP_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(false);

		// Version 1 - Use the Cash Target Option
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();
		config.getAccountRebalanceConfig().setMinimizeImbalancesAdjustForCashTarget(true);
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(548, "Cash").withAssetClassPercentage(0.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(15000000.00).withPhysicalExposure(16443075.29).withOverlayExposure(-20312285.48).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 14640477.91));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2133, "Equity").asRollup().withAssetClassPercentage(63.00000).withCashPercentage(0.000000000000000).withRebalanceTriggers(-35640838.38, 35640838.38));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2134, "Fixed").asRollup().withAssetClassPercentage(27.00000).withCashPercentage(0.000000000000000).withRebalanceTriggers(-35640838.38, 35640838.38));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(549, "Domestic Equity").withAssetClassPercentage(28.50000).withCashPercentage(0.000000000000000).withAdjustedTarget(335224150.26).withPhysicalExposure(359573453.64).withOverlayExposure(-5114118.09).withRebalanceTriggers(-35640838.38, 35640838.38).withCashInBucket(ManagerCashTypes.MANAGER, 695743.97).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 766471.26));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(550, "Fixed Income").withAssetClassPercentage(18.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(217389944.28).withPhysicalExposure(203194734.97).withOverlayExposure(21949821.05).withRebalanceTriggers(-35640838.38, 35640838.38));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(551, "Fixed Income-Short Term").withAssetClassPercentage(4.50000).withCashPercentage(0.000000000000000).withAdjustedTarget(54347486.06).withPhysicalExposure(48889624.32).withOverlayExposure(7718513.12).withRebalanceTriggers(-35640838.38, 35640838.38));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClass(557, "Inflation-Related").withAssetClassPercentage(0.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(0.00).withPhysicalExposure(0.00).withOverlayExposure(0.00));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(552, "International Equity").withAssetClassPercentage(13.50000).withCashPercentage(0.000000000000000).withAdjustedTarget(158790386.95).withPhysicalExposure(174379003.38).withOverlayExposure(-6884159.06).withRebalanceTriggers(-35640838.38, 35640838.38).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 340382.15));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(554, "Intl Equity Emerging").withAssetClassPercentage(13.50000).withCashPercentage(0.000000000000000).withAdjustedTarget(158790386.95).withPhysicalExposure(122062462.90).withOverlayExposure(2642228.46).withRebalanceTriggers(-35640838.38, 35640838.38));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClass(555, "Long/Short Equity").withAssetClassPercentage(7.50000).withCashPercentage(0.000000000000000).withAdjustedTarget(92978958.66).withPhysicalExposure(92978958.66).withOverlayExposure(0.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(556, "Private Equity/Debt").withAssetClassPercentage(0.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(872998.59).withPhysicalExposure(872998.59).withOverlayExposure(0.00));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClass(4345, "Special Fixed Income").withAssetClassPercentage(4.50000).withCashPercentage(0.000000000000000).withAdjustedTarget(47946173.26).withPhysicalExposure(47946173.26).withOverlayExposure(0.00).withRebalanceTriggers(-1439889.87, 1439889.87));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1672, "Tactical").withAssetClassPercentage(10.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(121687461.09).withPhysicalExposure(121687461.09).withOverlayExposure(0.00));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity", 1213254.21, 0.00, 0.00, 0.00, 0.00, -9557564.24);
		validateRollupAssetClassAllocation(config, "Equity");
		validateRollupAssetClassAllocation(config, "Fixed");
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 0.00, 0.00, 0.00, 0.00, 10594720.99);
		validateAssetClassAllocation(config, "Fixed Income-Short Term", 0.00, 0.00, 0.00, 0.00, 0.00, 4557739.66);
		validateZeroAssetClassAllocation(config, "Inflation-Related");
		validateAssetClassAllocation(config, "International Equity", 229821.08, 0.00, 0.00, 0.00, 0.00, -8237124.87);
		validateAssetClassAllocation(config, "Intl Equity Emerging", 0.00, 0.00, 0.00, 0.00, 0.00, 2642228.46);
		validateZeroAssetClassAllocation(config, "Long/Short Equity");
		validateZeroAssetClassAllocation(config, "Private Equity/Debt");
		validateZeroAssetClassAllocation(config, "Special Fixed Income");
		validateZeroAssetClassAllocation(config, "Tactical");


		// Version 2 - This time instead of cash target adjustment, change Fund Cash Overlay Amount which causes Cash Target of 15,000,000
		config = setupProductOverlayRunConfig();
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(548, "Cash").withAssetClassPercentage(0.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(0.00).withPhysicalExposure(16443075.29).withOverlayExposure(-20312285.48).addMinimizeImbalancesCash(ManagerCashTypes.FUND, -359522.09));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2133, "Equity").asRollup().withAssetClassPercentage(63.00000).withCashPercentage(0.000000000000000).withRebalanceTriggers(-35640838.38, 35640838.38));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2134, "Fixed").asRollup().withAssetClassPercentage(27.00000).withCashPercentage(0.000000000000000).withRebalanceTriggers(-35640838.38, 35640838.38));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(549, "Domestic Equity").withAssetClassPercentage(28.50000).withCashPercentage(0.000000000000000).withAdjustedTarget(335224150.26).withPhysicalExposure(359573453.64).withOverlayExposure(-5114118.09).withRebalanceTriggers(-35640838.38, 35640838.38).withCashInBucket(ManagerCashTypes.MANAGER, 695743.97).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 766471.26));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(550, "Fixed Income").withAssetClassPercentage(18.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(217389944.28).withPhysicalExposure(203194734.97).withOverlayExposure(21949821.05).withRebalanceTriggers(-35640838.38, 35640838.38));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(551, "Fixed Income-Short Term").withAssetClassPercentage(4.50000).withCashPercentage(0.000000000000000).withAdjustedTarget(54347486.06).withPhysicalExposure(48889624.32).withOverlayExposure(7718513.12).withRebalanceTriggers(-35640838.38, 35640838.38));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClass(557, "Inflation-Related").withAssetClassPercentage(0.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(0.00).withPhysicalExposure(0.00).withOverlayExposure(0.00));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(552, "International Equity").withAssetClassPercentage(13.50000).withCashPercentage(0.000000000000000).withAdjustedTarget(158790386.95).withPhysicalExposure(174379003.38).withOverlayExposure(-6884159.06).withRebalanceTriggers(-35640838.38, 35640838.38).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 340382.15));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(554, "Intl Equity Emerging").withAssetClassPercentage(13.50000).withCashPercentage(0.000000000000000).withAdjustedTarget(158790386.95).withPhysicalExposure(122062462.90).withOverlayExposure(2642228.46).withRebalanceTriggers(-35640838.38, 35640838.38));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClass(555, "Long/Short Equity").withAssetClassPercentage(7.50000).withCashPercentage(0.000000000000000).withAdjustedTarget(92978958.66).withPhysicalExposure(92978958.66).withOverlayExposure(0.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(556, "Private Equity/Debt").withAssetClassPercentage(0.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(872998.59).withPhysicalExposure(872998.59).withOverlayExposure(0.00));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClass(4345, "Special Fixed Income").withAssetClassPercentage(4.50000).withCashPercentage(0.000000000000000).withAdjustedTarget(47946173.26).withPhysicalExposure(47946173.26).withOverlayExposure(0.00).withRebalanceTriggers(-1439889.87, 1439889.87));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1672, "Tactical").withAssetClassPercentage(10.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(121687461.09).withPhysicalExposure(121687461.09).withOverlayExposure(0.00));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity", 1213254.21, 0.00, 0.00, 0.00, 0.00, -9557564.24);
		validateRollupAssetClassAllocation(config, "Equity");
		validateRollupAssetClassAllocation(config, "Fixed");
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 0.00, 0.00, 0.00, 0.00, 10594720.99);
		validateAssetClassAllocation(config, "Fixed Income-Short Term", 0.00, 0.00, 0.00, 0.00, 0.00, 4557739.66);
		validateZeroAssetClassAllocation(config, "Inflation-Related");
		validateAssetClassAllocation(config, "International Equity", 229821.08, 0.00, 0.00, 0.00, 0.00, -8237124.87);
		validateAssetClassAllocation(config, "Intl Equity Emerging", 0.00, 0.00, 0.00, 0.00, 0.00, 2642228.46);
		validateZeroAssetClassAllocation(config, "Long/Short Equity");
		validateZeroAssetClassAllocation(config, "Private Equity/Debt");
		validateZeroAssetClassAllocation(config, "Special Fixed Income");
		validateZeroAssetClassAllocation(config, "Tactical");
	}


	@Test
	public void test_ApplyToChildrenProportionally() {
		// 423000 M&M on 10/11
		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.APPLY_ROLLUP_PROPORTIONAL_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(true);

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.EXCLUDE_ROLLUP_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(false);

		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2136, "Equity").asRollup().withAssetClassPercentage(58.41));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2137, "Fixed").asRollup().withAssetClassPercentage(31.50));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1672, "Tactical").withAssetClassPercentage(10.07).withAdjustedTarget(190332260.20).withPhysicalExposure(190332260.20));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(548, "Cash").withAssetClassPercentage(0).withAdjustedTarget(0).withPhysicalExposure(13423965.13).withOverlayExposure(-13643473.86).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 10875913.25));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(556, "Private Equity/Debt").withAssetClassPercentage(0.02).withAdjustedTarget(1199637.90).withPhysicalExposure(1199637.90));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(549, "Domestic Equity").withAssetClassPercentage(21.67).withAdjustedTarget(419975567.32).withPhysicalExposure(424685531.89).withOverlayExposure(-2293690.85).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 2200888.29));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(550, "Fixed Income").withAssetClassPercentage(19.62).withAdjustedTarget(362556933.97).withPhysicalExposure(354031183.85).withOverlayExposure(4593271.56));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(551, "Fixed Income-Short Duration").withAssetClassPercentage(5.27).withAdjustedTarget(97437176.01).withPhysicalExposure(90735975.51).withOverlayExposure(4054029.71));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(552, "International Equity").withAssetClassPercentage(17.73).withAdjustedTarget(343668271.24).withPhysicalExposure(344185007.79).withOverlayExposure(3104780.89).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 347163.59));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(554, "Intl Equity-Emerging").withAssetClassPercentage(6.75).withAdjustedTarget(130921246.18).withPhysicalExposure(127497530.55).withOverlayExposure(4185082.54));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClass(555, "Long/Short Equity").withAssetClassPercentage(12.26).withAdjustedTarget(211353379.67).withPhysicalExposure(211353379.67));
		addAssetClassToMap(config, "Fixed", ProductOverlayAssetClassBuilder.newAssetClass(557, "Inflation-Related").withAssetClassPercentage(6.61).withAdjustedTarget(136403204.18).withPhysicalExposure(136403204.18));


		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Equity");
		validateRollupAssetClassAllocation(config, "Fixed");
		validateZeroAssetClassAllocation(config, "Tactical");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Private Equity/Debt");
		validateAssetClassAllocation(config, "Domestic Equity", 2200888.29, 3317582.21, 0.00, 0.00, 0.00, -3275560.30);
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 3003736.18, 0.00, 0.00, 0.00, 3812657.99);
		validateAssetClassAllocation(config, "Fixed Income-Short Duration", 0.00, 806813.95, 0.00, 0.00, 0.00, 1024093.15);
		validateAssetClassAllocation(config, "International Equity", 347163.59, 2714385.44, 0.00, 0.00, 0.00, -1226440.68);
		validateAssetClassAllocation(config, "Intl Equity-Emerging", 0.00, 1033395.47, 0.00, 0.00, 0.00, -334750.16);
		validateZeroAssetClassAllocation(config, "Long/Short Equity");
		validateZeroAssetClassAllocation(config, "Inflation-Related");
	}


	@Test
	public void test_ApplyToChildrenProportionally_Exclude_AndRegularUnderweight() {
		// 576700 Sonoma County on 04/08/2014
		// Equity: Exclude, Fixed: Regular, GAA: Proportional
		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.APPLY_ROLLUP_PROPORTIONAL_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenAnswer(
				invocation -> {
					Object[] args = invocation.getArguments();
					InvestmentAccountAssetClass ac = (InvestmentAccountAssetClass) args[0];
					if ("Global Asset Allocation".equals(ac.getAssetClass().getName())) {
						return true;
					}
					return false;
				});

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.EXCLUDE_ROLLUP_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InvestmentAccountAssetClass ac = (InvestmentAccountAssetClass) args[0];
			if ("Total Equity".equals(ac.getAssetClass().getName())) {
				return true;
			}
			return false;
		});
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(0.00, 0.00, 0.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2674, "Real Estate").asRollup().withAssetClassPercentage(15));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2675, "Global Asset Allocation").asRollup().withAssetClassPercentage(8));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2692, "Fixed Income").asRollup().withAssetClassPercentage(20));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2961, "Total Equity").asRollup().withAssetClassPercentage(57));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2676, "Cash").withAssetClassPercentage(0).withPhysicalExposure(58660802.72).withOverlayExposure(-59497474.38).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 43905684.03));

		addAssetClassToMap(config, "Total Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2668, "Domestic Equity Large Cap").withAssetClassPercentage(17).withAdjustedTarget(382850188.02).withPhysicalExposure(372109952.31).withOverlayExposure(10465770.89).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 5834490.49));
		addAssetClassToMap(config, "Total Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2669, "Domestic Equity - Broad Mandate").withAssetClassPercentage(8).withAdjustedTarget(180164794.36).withPhysicalExposure(175578773.76).withOverlayExposure(2496690.35).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 943632.10));
		addAssetClassToMap(config, "Total Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2670, "Domestic Equity Small Cap").withAssetClassPercentage(5).withAdjustedTarget(112602996.48).withPhysicalExposure(110067784.54).withOverlayExposure(-345715.86).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 932361.87));
		addAssetClassToMap(config, "Total Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2671, "Non-U.S. Equity").withAssetClassPercentage(17).withAdjustedTarget(382850188.02).withPhysicalExposure(372801668.09).withOverlayExposure(12411687.52).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4545776.35));
		addAssetClassToMap(config, "Total Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2672, "Global Equity").withAssetClassPercentage(10).withAdjustedTarget(225205992.95).withPhysicalExposure(222332792.64).withOverlayExposure(4419657.50).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 2498857.88));

		addAssetClassToMap(config, "Fixed Income", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2673, "Core Fixed Income").withAssetClassPercentage(14).withAdjustedTarget(315288390.13).withPhysicalExposure(299623776.96).withOverlayExposure(24252572.87));
		addAssetClassToMap(config, "Fixed Income", ProductOverlayAssetClassBuilder.newAssetClass(2691, "Fixed Income Alternative").withAssetClassPercentage(6).withAdjustedTarget(135123595.77).withPhysicalExposure(127699483.34));

		addAssetClassToMap(config, "Real Estate", ProductOverlayAssetClassBuilder.newAssetClass(2693, "Real Estate - Core").withAssetClassPercentage(10).withAdjustedTarget(246531406.80).withPhysicalExposure(246531406.80));
		addAssetClassToMap(config, "Real Estate", ProductOverlayAssetClassBuilder.newAssetClass(2694, "Farmland Real Estate").withAssetClassPercentage(5).withAdjustedTarget(109842691.68).withPhysicalExposure(109842691.68));

		addAssetClassToMap(config, "Global Asset Allocation", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2959, "Global Asset Equity").withAssetClassPercentage(5.2).withAdjustedTarget(117107116.34).withPhysicalExposure(113994338.72).withOverlayExposure(3796653.91));
		addAssetClassToMap(config, "Global Asset Allocation", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2960, "Global Asset Fixed Income").withAssetClassPercentage(2.8).withAdjustedTarget(63057678.02).withPhysicalExposure(61381567.01).withOverlayExposure(2000157.21));

		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Real Estate");
		validateRollupAssetClassAllocation(config, "Global Asset Allocation");
		validateRollupAssetClassAllocation(config, "Fixed Income");
		validateRollupAssetClassAllocation(config, "Total Equity");
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 5834490.49, 9448058.59, 0.00, 0.00, 0.00, -4816778.19);
		validateAssetClassAllocation(config, "Domestic Equity - Broad Mandate", 943632.10, 4446145.22, 0.00, 0.00, 0.00, -2893086.97);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 932361.87, 2778840.76, 0.00, 0.00, 0.00, -4056918.49);
		validateAssetClassAllocation(config, "Non-U.S. Equity", 4545776.35, 9448058.59, 0.00, 0.00, 0.00, -1963175.43);
		validateAssetClassAllocation(config, "Global Equity", 2498857.88, 5557681.52, 0.00, 0.00, 0.00, -4017374.63);
		validateAssetClassAllocation(config, "Core Fixed Income", 0.00, 7780754.13, 0.00, 0.00, 0.00, 16471818.74);
		validateZeroAssetClassAllocation(config, "Fixed Income Alternative");
		validateZeroAssetClassAllocation(config, "Real Estate - Core");
		validateZeroAssetClassAllocation(config, "Farmland Real Estate");
		validateAssetClassAllocation(config, "Global Asset Equity", 0.00, 2889994.39, 0.00, 0.00, 0.00, 829084.74);
		validateAssetClassAllocation(config, "Global Asset Fixed Income", 0.00, 1556150.83, 0.00, 0.00, 0.00, 446430.24);
	}


	@Test
	public void test_ApplyToChildrenProportionally_Exclude_AndRegularUnderweight_AfterFullRebalance() {
		// Sonoma County on 10/31/2014 With a FULL Rebalance
		// Equity: Exclude, Fixed: Regular, GAA: Proportional
		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.APPLY_ROLLUP_PROPORTIONAL_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenAnswer(
				invocation -> {
					Object[] args = invocation.getArguments();
					InvestmentAccountAssetClass ac = (InvestmentAccountAssetClass) args[0];
					if ("Global Asset Allocation".equals(ac.getAssetClass().getName())) {
						return true;
					}
					return false;
				});

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.EXCLUDE_ROLLUP_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InvestmentAccountAssetClass ac = (InvestmentAccountAssetClass) args[0];
			if ("Total Equity".equals(ac.getAssetClass().getName())) {
				return true;
			}
			return false;
		});
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();
		// Cash Available for Minimize Imbalances: Manager Cash: 15853986.54, Fund Cash: 62973597.33, Total: 78,827,583.87

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2674, "Real Estate").asRollup().withAssetClassPercentage(15));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2675, "Global Asset Allocation").asRollup().withAssetClassPercentage(8));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2692, "Fixed Income").asRollup().withAssetClassPercentage(20));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2961, "Total Equity").asRollup().withAssetClassPercentage(57));

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2676, "Cash").withAssetClassPercentage(0).withPhysicalExposure(78827583.87).withOverlayExposure(-76269148.02).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 62973597.33));

		addAssetClassToMap(config, "Total Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2669, "Domestic Equity - Broad Mandate").withAssetClassPercentage(8).withAdjustedTarget(186609045.05).withPhysicalExposure(195934152.09).withOverlayExposure(-2087292.69).withCashInBucket(ManagerCashTypes.REBALANCE, -2248687.44).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1778695.51));
		addAssetClassToMap(config, "Total Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2668, "Domestic Equity Large Cap").withAssetClassPercentage(17).withAdjustedTarget(396544220.74).withPhysicalExposure(396827516.47).withOverlayExposure(5248504.55).withCashInBucket(ManagerCashTypes.REBALANCE, 3615933.37).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 5141873.46));
		addAssetClassToMap(config, "Fixed Income", ProductOverlayAssetClassBuilder.newAssetClass(2691, "Fixed Income Alternative").withAssetClassPercentage(6).withAdjustedTarget(139956783.79).withPhysicalExposure(130799235.77));
		addAssetClassToMap(config, "Real Estate", ProductOverlayAssetClassBuilder.newAssetClass(2693, "Real Estate - Core").withAssetClassPercentage(10).withAdjustedTarget(253878810.49).withPhysicalExposure(253878810.49));
		addAssetClassToMap(config, "Real Estate", ProductOverlayAssetClassBuilder.newAssetClass(2694, "Farmland Real Estate").withAssetClassPercentage(5).withAdjustedTarget(113567767.70).withPhysicalExposure(113567767.70));
		addAssetClassToMap(config, "Global Asset Allocation", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2960, "Global Asset Fixed Income").withAssetClassPercentage(2.8).withAdjustedTarget(65313165.77).withPhysicalExposure(61859473.40).withOverlayExposure(3370222.68).withCashInBucket(ManagerCashTypes.REBALANCE, -394330.86));
		addAssetClassToMap(config, "Global Asset Allocation", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2959, "Global Asset Equity").withAssetClassPercentage(5.2).withAdjustedTarget(121295879.28).withPhysicalExposure(114881879.18).withOverlayExposure(5908246.14).withCashInBucket(ManagerCashTypes.REBALANCE, -732328.74));
		addAssetClassToMap(config, "Total Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2670, "Domestic Equity Small Cap").withAssetClassPercentage(5).withAdjustedTarget(116630653.15).withPhysicalExposure(111458100.12).withOverlayExposure(10447373.21).withCashInBucket(ManagerCashTypes.REBALANCE, -4098164.97).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 2383047.52));
		addAssetClassToMap(config, "Fixed Income", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2673, "Core Fixed Income").withAssetClassPercentage(14).withAdjustedTarget(326565828.84).withPhysicalExposure(301036401.48).withOverlayExposure(13377148.65).withCashInBucket(ManagerCashTypes.REBALANCE, -10413057.99));
		addAssetClassToMap(config, "Total Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2672, "Global Equity").withAssetClassPercentage(10).withAdjustedTarget(233261306.32).withPhysicalExposure(223602564.60).withOverlayExposure(12463232.39).withCashInBucket(ManagerCashTypes.REBALANCE, 3153832.16).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 2626642.22));
		addAssetClassToMap(config, "Total Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2671, "Non-U.S. Equity").withAssetClassPercentage(17).withAdjustedTarget(396544220.73).withPhysicalExposure(367494196.69).withOverlayExposure(27541714.34).withCashInBucket(ManagerCashTypes.REBALANCE, 11116804.47).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 3923727.83));

		// Set up for Full Rebalance with History
		setupInvestmentAccountFullRebalance(config, true);
		addRebalanceHistory(config, "Domestic Equity - Broad Mandate", 4989126.91, -2248687.44);
		addRebalanceHistory(config, "Domestic Equity Large Cap", 9147733.65, 3615933.37);
		addRebalanceHistory(config, "Domestic Equity Small Cap", 1176655.21, -4098164.97);
		addRebalanceHistory(config, "Non-U.S. Equity", 9608494.77, 11116804.47);
		addRebalanceHistory(config, "Global Equity", 5958322.83, 3153832.16);
		addRebalanceHistory(config, "Core Fixed Income", -29164448.86, -10413057.99);
		addRebalanceHistory(config, "Fixed Income Alternative", 0.00, 0.00);
		addRebalanceHistory(config, "Real Estate - Core", 0.00, 0.00);
		addRebalanceHistory(config, "Farmland Real Estate", 0.00, 0.00);
		addRebalanceHistory(config, "Global Asset Equity", -1143440.35, -732328.74);
		addRebalanceHistory(config, "Global Asset Fixed Income", -572444.15, -394330.86);
		addRebalanceHistory(config, "Cash", 0.00, 0.00);

		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Real Estate");
		validateRollupAssetClassAllocation(config, "Global Asset Allocation");
		validateRollupAssetClassAllocation(config, "Fixed Income");
		validateRollupAssetClassAllocation(config, "Total Equity");
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity - Broad Mandate", 1778695.51, 6377073.15, 0.00, -2248687.44, -2248687.44, -15232188.26);
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 5141873.46, 13551280.44, 0.00, 3615933.37, 3615933.37, -22592383.00);
		validateZeroAssetClassAllocation(config, "Fixed Income Alternative");
		validateZeroAssetClassAllocation(config, "Real Estate - Core");
		validateZeroAssetClassAllocation(config, "Farmland Real Estate");
		validateAssetClassAllocation(config, "Global Asset Fixed Income", 0.00, 2231975.60, 0.00, -394330.86, -394330.86, 1616048.06);
		validateAssetClassAllocation(config, "Global Asset Equity", 0.00, 4145097.55, 0.00, -732328.74, -732328.74, 3001232.11);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 2383047.52, 3985670.72, 0.00, -4098164.97, -4098164.97, 2901999.76);
		validateAssetClassAllocation(config, "Core Fixed Income", 0.00, 11159878.01, 0.00, -10413057.99, -10413057.99, 33940154.11);
		validateAssetClassAllocation(config, "Global Equity", 2626642.22, 7971341.43, 0.00, 3153832.16, 3153832.16, -4093074.09);
		validateAssetClassAllocation(config, "Non-U.S. Equity", 3923727.83, 13551280.44, 0.00, 11116804.47, 11116804.47, 458211.30);
	}
}
