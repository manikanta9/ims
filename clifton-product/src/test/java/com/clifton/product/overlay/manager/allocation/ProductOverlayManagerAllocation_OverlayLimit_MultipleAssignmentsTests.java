package com.clifton.product.overlay.manager.allocation;

import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class ProductOverlayManagerAllocation_OverlayLimit_MultipleAssignmentsTests extends BaseProductOverlayManagerAllocationCalculatorTests {


	private static final int RUN_ACCOUNT_567000_06_17_2019 = 1036067;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testManagerAllocation_OverlayLimitPercent_MultipleAssignments() {
		String result = generateResultForRun(RUN_ACCOUNT_567000_06_17_2019);
		String expectedResult = "M00506: Barrow Hanley Large Cap Value\tDomestic Equity Large Cap\tCASH_ALLOCATION\t70,619.27\t100\t,\n" +
				"M00506: Barrow Hanley Large Cap Value\tDomestic Equity Large Cap\tOVERLAY_ALLOCATION\t70,619.27\t100\tNo Adjustment Action\t\tLimit Action\t2,466,887.38\t,\n" +
				"M00506: Barrow Hanley Large Cap Value\tDomestic Equity Large Cap\tSECURITIES_ALLOCATION\t49,267,128.37\t100\t,\n" +
				"M11103: State Fund Common Stock Index (pm)\tDomestic Equity Large Cap\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M11103: State Fund Common Stock Index (pm)\tDomestic Equity Large Cap\tSECURITIES_ALLOCATION\t246,367,276.87\t100\t,\n" +
				"M13222: BlackRock S&P 500 (pm)\tDomestic Equity Large Cap\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M13222: BlackRock S&P 500 (pm)\tDomestic Equity Large Cap\tSECURITIES_ALLOCATION\t10,974,183.72\t100\t,\n" +
				"M00761: Boston Company Small Cap Value\tDomestic Equity Small Cap\tCASH_ALLOCATION\t270,719.27\t100\t,\n" +
				"M00761: Boston Company Small Cap Value\tDomestic Equity Small Cap\tOVERLAY_ALLOCATION\t270,719.27\t100\tNo Adjustment Action\t\tLimit Action\t1,880,672.24\t,\n" +
				"M00761: Boston Company Small Cap Value\tDomestic Equity Small Cap\tSECURITIES_ALLOCATION\t37,342,725.43\t100\t,\n" +
				"M01506: Dimensional Fund Advisors (pm)\tDomestic Equity Small Cap\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M01506: Dimensional Fund Advisors (pm)\tDomestic Equity Small Cap\tOVERLAY_ALLOCATION\t0.00\t100\tNo Adjustment Action\t\tNo Limit Action\t\t,\n" +
				"M01506: Dimensional Fund Advisors (pm)\tDomestic Equity Small Cap\tSECURITIES_ALLOCATION\t39,628,461.34\t100\t,\n" +
				"M04740: Wellington Small/Mid Growth\tDomestic Equity Small Cap\tCASH_ALLOCATION\t622,199.35\t100\t,\n" +
				"M04740: Wellington Small/Mid Growth\tDomestic Equity Small Cap\tOVERLAY_ALLOCATION\t622,199.35\t100\tNo Adjustment Action\t\tLimit Action\t2,595,499.47\t,\n" +
				"M04740: Wellington Small/Mid Growth\tDomestic Equity Small Cap\tSECURITIES_ALLOCATION\t51,287,790.08\t100\t,\n" +
				"M08015: Morgan Stanley Global (pm)\tInternational Equity\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M08015: Morgan Stanley Global (pm)\tInternational Equity\tOVERLAY_ALLOCATION\t0.00\t100\tNo Adjustment Action\t\tNo Limit Action\t\t,\n" +
				"M08015: Morgan Stanley Global (pm)\tInternational Equity\tSECURITIES_ALLOCATION\t73,702,344.16\t100\t,\n" +
				"M11104: State Fund International Share Account (pm)\tInternational Equity\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M11104: State Fund International Share Account (pm)\tInternational Equity\tSECURITIES_ALLOCATION\t70,269,444.82\t100\t,\n" +
				"M20899: BlackRock iShares Core MSCI Emerging Markets\tInternational Equity\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M20899: BlackRock iShares Core MSCI Emerging Markets\tInternational Equity\tSECURITIES_ALLOCATION\t6,467,632.58\t100\t,\n" +
				"M00937: Cash Flow Account\tCash\tCASH_ALLOCATION\t58,850,703.21\t100\t,\n" +
				"M00937: Cash Flow Account\tCash\tOVERLAY_ALLOCATION\t58,850,703.21\t100\tNo Adjustment Action\t\tNo Limit Action\t\t,\n" +
				"M00937: Cash Flow Account\tCash\tSECURITIES_ALLOCATION\t0.00\t100\t,\n" +
				"M04222: null\tCash\tCASH_ALLOCATION\t8,370,203.93\t100\t,\n" +
				"M04222: null\tCash\tOVERLAY_ALLOCATION\t8,370,203.93\t100\tNo Adjustment Action\t\tNo Limit Action\t\t,\n" +
				"M04222: null\tCash\tSECURITIES_ALLOCATION\t0.00\t100\t,\n" +
				"M04523: US Bank Cash Account\tCash\tCASH_ALLOCATION\t287,430.50\t100\t,\n" +
				"M04523: US Bank Cash Account\tCash\tOVERLAY_ALLOCATION\t287,430.50\t100\tNo Adjustment Action\t\tNo Limit Action\t\t,\n" +
				"M04523: US Bank Cash Account\tCash\tSECURITIES_ALLOCATION\t0.00\t100\t,\n" +
				"M13260: Pending Cash\tCash\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M13260: Pending Cash\tCash\tOVERLAY_ALLOCATION\t0.00\t100\tNo Adjustment Action\t\tNo Limit Action\t\t,\n" +
				"M13260: Pending Cash\tCash\tSECURITIES_ALLOCATION\t0.00\t100\t,\n" +
				"M08936: Brandywine (pm)\tFixed Income\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M08936: Brandywine (pm)\tFixed Income\tSECURITIES_ALLOCATION\t36,740,372.66\t100\t,\n" +
				"M09014: Guggenheim\tFixed Income\tCASH_ALLOCATION\t12,497,402.31\t100\t,\n" +
				"M09014: Guggenheim\tFixed Income\tOVERLAY_ALLOCATION\t12,497,402.31\t100\tNo Adjustment Action\t\tLimit Action\t4,924,639.87\t,\n" +
				"M09014: Guggenheim\tFixed Income\tSECURITIES_ALLOCATION\t85,995,395.04\t100\t,\n" +
				"M09087: Allianz GI (pm)\tFixed Income\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M09087: Allianz GI (pm)\tFixed Income\tOVERLAY_ALLOCATION\t0.00\t100\tNo Adjustment Action\t\tNo Limit Action\t\t,\n" +
				"M09087: Allianz GI (pm)\tFixed Income\tSECURITIES_ALLOCATION\t32,921,674.76\t100\t,\n" +
				"M00103: Securian\tReal Estate\tCASH_ALLOCATION\t457,386.77\t100\t,\n" +
				"M00103: Securian\tReal Estate\tOVERLAY_ALLOCATION\t457,386.77\t100\tNo Adjustment Action\t\tLimit Action\t1,411,960.48\t,\n" +
				"M00103: Securian\tReal Estate\tSECURITIES_ALLOCATION\t27,781,822.90\t100\t,\n" +
				"M04494: UBS Trumbull Property Fund (pm)\tReal Estate\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M04494: UBS Trumbull Property Fund (pm)\tReal Estate\tOVERLAY_ALLOCATION\t0.00\t100\tNo Adjustment Action\t\tLimit Action\t1,924,300.00\t,\n" +
				"M04494: UBS Trumbull Property Fund (pm)\tReal Estate\tSECURITIES_ALLOCATION\t38,486,000.00\t100\t,\n" +
				"M08230: Parametric - TIPS\tReal Estate\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M08230: Parametric - TIPS\tReal Estate\tSECURITIES_ALLOCATION\t9,177,011.08\t100\t,\n" +
				"M09103: Tortoise Capital Funds (pm)\tReal Estate\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M09103: Tortoise Capital Funds (pm)\tReal Estate\tOVERLAY_ALLOCATION\t0.00\t100\tNo Adjustment Action\t\tNo Limit Action\t\t,\n" +
				"M09103: Tortoise Capital Funds (pm)\tReal Estate\tSECURITIES_ALLOCATION\t17,527,604.63\t100\t,\n" +
				"M11296: UBS Trumbull Growth and Income Fund (pm)\tReal Estate\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M11296: UBS Trumbull Growth and Income Fund (pm)\tReal Estate\tSECURITIES_ALLOCATION\t18,434,000.00\t100\t,\n" +
				"M12478: Private Real Estate (pm)\tReal Estate\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M12478: Private Real Estate (pm)\tReal Estate\tSECURITIES_ALLOCATION\t5,469,000.00\t100\t,\n" +
				"M08577: Private Equity (pm)\tAlternatives\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M08577: Private Equity (pm)\tAlternatives\tOVERLAY_ALLOCATION\t0.00\t100\tNo Adjustment Action\t\tNo Limit Action\t\t,\n" +
				"M08577: Private Equity (pm)\tAlternatives\tSECURITIES_ALLOCATION\t69,373,277.30\t100\t,\n" +
				"M11096: Defensive Equity Funding (pm)\tAlternatives\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M11096: Defensive Equity Funding (pm)\tAlternatives\tOVERLAY_ALLOCATION\t0.00\t100\tNo Adjustment Action\t\tNo Limit Action\t\t,\n" +
				"M11096: Defensive Equity Funding (pm)\tAlternatives\tSECURITIES_ALLOCATION\t30,425,306.61\t100\t,\n" +
				"M13595: Entrust Special Opportunities Fund III, Ltd (pm)\tAlternatives\tCASH_ALLOCATION\t0.00\t100\t,\n" +
				"M13595: Entrust Special Opportunities Fund III, Ltd (pm)\tAlternatives\tSECURITIES_ALLOCATION\t15,078,856.00\t100";
		Assertions.assertEquals(StringUtils.trimAll(expectedResult), StringUtils.trimAll(result));
	}
}
