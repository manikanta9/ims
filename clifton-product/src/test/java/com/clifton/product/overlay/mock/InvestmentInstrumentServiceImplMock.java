package com.clifton.product.overlay.mock;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentInstrumentServiceImpl;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class InvestmentInstrumentServiceImplMock extends InvestmentInstrumentServiceImpl {

	/**
	 * Since we don't support <= < >= > searches in XML DAOs, need to override method
	 */
	@Override
	public List<InvestmentSecurity> getInvestmentSecurityList(SecuritySearchForm searchForm) {
		Date activeOnDate = searchForm.getActiveOnDate();
		searchForm.setActiveOnDate(null);

		List<InvestmentSecurity> secList = super.getInvestmentSecurityList(searchForm);

		if (activeOnDate != null) {
			List<InvestmentSecurity> newList = new ArrayList<>();
			for (InvestmentSecurity security : CollectionUtils.getIterable(secList)) {
				if (DateUtils.isDateBetween(activeOnDate, security.getStartDate(), security.getEndDate(), false)) {
					newList.add(security);
				}
			}
			return newList;
		}

		return secList;
	}
}
