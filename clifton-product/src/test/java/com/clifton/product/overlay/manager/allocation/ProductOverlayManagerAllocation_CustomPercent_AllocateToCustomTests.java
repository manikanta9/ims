package com.clifton.product.overlay.manager.allocation;

import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class ProductOverlayManagerAllocation_CustomPercent_AllocateToCustomTests extends BaseProductOverlayManagerAllocationCalculatorTests {


	private static final int RUN_ACCOUNT_310200_06_17_2019 = 1035977;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCustomCashOverlayPercent_AllocateToCustom() {
		String result = generateResultForRun(RUN_ACCOUNT_310200_06_17_2019);
		String expectedResult = "M04271: The Clifton Group - DE\tDomestic Equity\tCASH_ALLOCATION\t498,890,520.46\t100\t," +
				"M04271: The Clifton Group - DE\tDomestic Equity\tSECURITIES_ALLOCATION\t0.00\t100\t," +
				"M09391: DE - Additional Target\tDomestic Equity\tCASH_ALLOCATION\t0.00\t100\t," +
				"M09391: DE - Additional Target\tDomestic Equity\tSECURITIES_ALLOCATION\t0.00\t100\t," +
				"M14493: Parametric - No Overlay\tDomestic Equity\tCASH_ALLOCATION\t-818,803,636.21\t100\t," +
				"M14493: Parametric - No Overlay\tDomestic Equity\tSECURITIES_ALLOCATION\t0.00\t100\t," +
				"M14496: Domestic Equity Cash\tDomestic Equity\tCASH_ALLOCATION\t214,919,871.40\t100\t," +
				"M14496: Domestic Equity Cash\tDomestic Equity\tSECURITIES_ALLOCATION\t283,970,649.06\t100\t," +
				"M14714: ETF - Current Positions Value\tDomestic Equity\tCASH_ALLOCATION\t283,970,649.06\t100\t," +
				"M14714: ETF - Current Positions Value\tDomestic Equity\tOVERLAY_ALLOCATION\t283,970,649.06\t100\tNo Adjustment Action\t\tNo Limit Action\t\t," +
				"M14714: ETF - Current Positions Value\tDomestic Equity\tSECURITIES_ALLOCATION\t0.00\t100\t," +
				"M14716: Linked Cash account for levered overlay\tDomestic Equity\tCASH_ALLOCATION\t476,670,542.47\t149\t," +
				"M14716: Linked Cash account for levered overlay\tDomestic Equity\tOVERLAY_ALLOCATION\t476,670,542.47\t149\tNo Adjustment Action\t\tNo Limit Action\t\t," +
				"M14716: Linked Cash account for levered overlay\tDomestic Equity\tSECURITIES_ALLOCATION\t0.00\t100\t," +
				"M15449: STIF - Domestic Equity Margin\tDomestic Equity\tCASH_ALLOCATION\t104,993,244.35\t100\t," +
				"M15449: STIF - Domestic Equity Margin\tDomestic Equity\tSECURITIES_ALLOCATION\t0.00\t100\t";
		Assertions.assertEquals(StringUtils.trimAll(expectedResult), StringUtils.trimAll(result));
	}
}
