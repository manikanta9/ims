package com.clifton.product.overlay.mock;


import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.product.overlay.ProductOverlayServiceImpl;
import com.clifton.product.overlay.search.ProductOverlayManagerAccountSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ProductOverlayServiceImplMock</code> ...
 *
 * @author Mary Anderson
 */
public class ProductOverlayServiceImplMock extends ProductOverlayServiceImpl {

	// Original Methods with all of the fields in the order by doesn't work with
	// XML DAO.  For testing, don't care about order
	@Override
	public List<ProductOverlayManagerAccount> getProductOverlayManagerAccountListByRun(int runId) {
		return getProductOverlayManagerAccountDAO().findByField("overlayRun.id", runId);
	}


	@Override
	public List<ProductOverlayManagerAccount> getProductOverlayManagerAccountList(final ProductOverlayManagerAccountSearchForm searchForm) {
		// withCashValuesOnly is a custom search restriction that has special restrictions
		// remove it because there is no definition attached to it.
		if (searchForm.containsSearchRestriction("withCashValuesOnly")) {
			searchForm.setWithCashValuesOnly((Boolean) searchForm.getSearchRestriction("withCashValuesOnly").getValue());
			searchForm.removeSearchRestriction("withCashValuesOnly");
		}

		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (BooleanUtils.isTrue(searchForm.getWithCashValuesOnly())) {
					criteria.add(Restrictions.or(Restrictions.ne("cashAllocation", BigDecimal.ZERO), Restrictions.ne("previousDayCashAllocation", BigDecimal.ZERO)));
				}
			}
		};
		return getProductOverlayManagerAccountDAO().findBySearchCriteria(searchConfig);
	}


	// Original Methods with all of the fields in the order by doesn't work with
	// XML DAO For testing, don't care about order
	@Override
	public List<ProductOverlayAssetClass> getProductOverlayAssetClassListByRun(int runId) {
		return getProductOverlayAssetClassDAO().findByField("overlayRun.id", runId);
	}


	@Override
	public List<ProductOverlayAssetClassReplication> getProductOverlayAssetClassReplicationListByRun(int runId) {
		return getProductOverlayAssetClassReplicationDAO().findByField("overlayAssetClass.overlayRun.id", runId);
	}
}
