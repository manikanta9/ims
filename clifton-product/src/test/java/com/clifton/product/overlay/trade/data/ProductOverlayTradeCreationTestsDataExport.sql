-- Common items for Portfolio Run Trade Creation
SELECT 'CalendarDay' AS entityTableName, CalendarDayID AS entityId FROM CalendarDay WHERE CalendarYearID = 2020
UNION
SELECT 'AccountingAccount' AS entityTableName, AccountingAccountID AS entityId FROM AccountingAccount
UNION
SELECT 'WorkflowState' AS enttityTableName, WorkflowStateID AS entityId FROM WorkflowState WHERE WorkflowID IN (SELECT WorkflowID FROM Workflow WHERE WorkflowID IN (21, 6)) -- portfolio and trade
UNION
SELECT 'TradeType' AS entityTableName, TradeTypeID AS entityId FROM TradeType
UNION
SELECT 'TradeOpenCloseType' AS entityTableName, TradeOpenCloseTypeID AS entityId FROM TradeOpenCloseType
UNION
SELECT 'TradeGroupType' AS entityTableName, TradeGroupTypeID AS entityId FROM TradeGroupType
UNION
SELECT 'InvestmentType' AS entityTableName, InvestmentTypeID AS entityId from InvestmentType
UNION
SELECT 'InvestmentTypeSubType' AS entityTableName, InvestmentTypeSubTypeID AS entityId from InvestmentTypeSubType
UNION
SELECT 'InvestmentTypeSubType2' AS entityTableName, InvestmentTypeSubType2ID AS entityId from InvestmentTypeSubType2
UNION
SELECT 'SystemColumn' AS entityTableName, SystemColumnID AS entityId FROM SystemColumn sc
	INNER JOIN SystemColumnGroup scg ON scg.SystemColumnGroupID = sc.SystemColumnGroupID
	INNER JOIN SystemTable st ON st.SystemTableID = sc.SystemTableID
	WHERE st.TableName = 'InvestmentAccount' AND (scg.ColumnGroupName = 'Portfolio Setup' OR scg.ColumnGroupName = 'PIOS Processing')
UNION
SELECT 'SystemColumn' AS entityTableName, SystemColumnID AS entityId FROM SystemColumn sc
	INNER JOIN SystemColumnGroup scg ON scg.SystemColumnGroupID = sc.SystemColumnGroupID
	INNER JOIN SystemTable st ON st.SystemTableID = sc.SystemTableID
	WHERE st.TableName = 'InvestmentSecurity' AND scg.ColumnGroupName = 'Security Custom Fields'
UNION
-- Items specific to Client Account and Run
SELECT 'ProductOverlayAssetClassReplication' AS entityTableName, ProductOverlayAssetClassReplicationID as entityId FROM ProductOverlayAssetClassReplication
	WHERE ProductOverlayAssetClassID IN (SELECT ProductOverlayAssetClassID FROM ProductOverlayAssetClass WHERE ProductOverlayRunID IN (1255497, 1255056))--(703025, 703428))
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue scv
	INNER JOIN SystemColumn sc ON scv.SystemColumnID = sc.SystemColumnID
	INNER JOIN SystemColumnGroup scg ON scg.SystemColumnGroupID = sc.SystemColumnGroupID
	INNER JOIN SystemTable st ON st.SystemTableID = sc.SystemTableID
	WHERE st.TableName = 'InvestmentAccount' AND (scg.ColumnGroupName = 'Portfolio Setup' OR scg.ColumnGroupName = 'PIOS Processing')
		AND scv.FKFieldID IN (SELECT ClientInvestmentAccountID FROM PortfolioRun WHERE PortfolioRunID IN (1255497, 1255056))
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue scv
	INNER JOIN SystemColumn sc ON scv.SystemColumnID = sc.SystemColumnID
	INNER JOIN SystemColumnGroup scg ON scg.SystemColumnGroupID = sc.SystemColumnGroupID
	INNER JOIN SystemTable st ON st.SystemTableID = sc.SystemTableID
	WHERE st.TableName = 'InvestmentSecurity' AND scg.ColumnGroupName = 'Security Custom Fields'
		AND scv.FKFieldID IN (SELECT InvestmentSecurityID FROM ProductOverlayAssetClassReplication WHERE ProductOverlayAssetClassID IN (SELECT ProductOverlayAssetClassID FROM ProductOverlayAssetClass WHERE ProductOverlayRunID IN (1255497, 1255056)))
UNION
SELECT 'MarketDataSource' AS entityTableName, MarketDataSourceID AS entityId FROM MarketDataSource
	WHERE DataSourceName IN ('Morgan Stanley & Co. Inc.', 'Goldman Sachs')
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID AS entityId FROM MarketDataValue
	WHERE InvestmentSecurityID IN (SELECT InvestmentSecurityID FROM ProductOverlayAssetClassReplication WHERE ProductOverlayAssetClassID IN (SELECT ProductOverlayAssetClassID FROM ProductOverlayAssetClass WHERE ProductOverlayRunID IN (1255497, 1255056)))
	 	AND MeasureDate > '07/25/2020' AND MeasureDate < '07/28/2020'
UNION
SELECT 'InvestmentAccountRelationship' AS entityTableName, InvestmentAccountRelationshipID AS entityId FROM InvestmentAccountRelationship WHERE MainAccountId IN (4492, 5571)
UNION
SELECT 'Calendar' AS entityTableName, CalendarID as entityId FROM Calendar WHERE IsDefaultSystemCalendar = 1;
