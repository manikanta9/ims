SELECT 'CalendarDay' AS entityTableName, CalendarDayID AS entityId FROM CalendarDay WHERE CalendarYearID = (Select CalendarYearID = 2017)
UNION
SELECT 'SystemColumn' AS entityTableName, SystemColumnID AS entityId FROM SystemColumn sc
	INNER JOIN SystemColumnGroup scg ON scg.SystemColumnGroupID = sc.SystemColumnGroupID
	INNER JOIN SystemTable st ON st.SystemTableID = sc.SystemTableID
WHERE st.TableName = 'InvestmentAccount' AND (scg.ColumnGroupName = 'Portfolio Setup' OR scg.ColumnGroupName = 'PIOS Processing')
UNION
SELECT 'MarketDataField', MarketDataFieldID FROM MarketDataField
UNION
SELECT 'AccountingAccount', AccountingAccountID FROM AccountingAccount
UNION
SELECT 'WorkflowState', WorkflowStateID FROM WorkflowState s INNER JOIN Workflow w ON s.WorkflowID = w.WorkflowID WHERE w.WorkflowName = 'Trade Workflow'
UNION
-- AKZO NOBEL 9/29 MAIN RUN
SELECT 'ProductOverlayAssetClassReplication' AS entityTableName, ProductOverlayAssetClassReplicationID AS entityId FROM ProductOverlayAssetClassReplication r INNER JOIN ProductOverlayAssetClass a ON r.ProductOverlayAssetClassID = a.ProductOverlayAssetClassID WHERE a.ProductOverlayRunID = 734785
UNION
-- AKZO NOBEL TRANSACTIONS UNTIL 10/2
SELECT 'AccountingTransaction', AccountingTransactionID FROM AccountingTransaction t WHERE t.ClientInvestmentAccountID = 1083 AND t.TransactionDate <= '10/02/2017'
UNION
-- Future Fair Value Market Data for Securities included in the run
SELECT 'MarketDataValue', MarketDataValueID FROM MarketDataValue v INNER JOIN ProductOverlayAssetClassReplication r ON v.InvestmentSecurityID = r.InvestmentSecurityID INNER JOIN ProductOverlayAssetClass a ON r.ProductOverlayAssetClassID = a.ProductOverlayAssetClassID WHERE a.ProductOverlayRunID = 734785 AND MeasureDate = '09/29/2017'
UNION
-- Exchange rates to use during accounting balance retrieval
SELECT 'MarketDataExchangeRate', mder.MarketDataExchangeRateID
FROM ProductOverlayAssetClassReplication poacr
	 JOIN ProductOverlayAssetClass poac ON poacr.ProductOverlayAssetClassID = poac.ProductOverlayAssetClassID
	 JOIN InvestmentSecurity s ON poacr.InvestmentSecurityID = s.InvestmentSecurityID
	 JOIN InvestmentSecurity s2 ON s.UnderlyingSecurityID = s2.InvestmentSecurityID
	 JOIN MarketDataExchangeRate mder ON (s2.InvestmentSecurityID = mder.FromCurrencySecurityID OR s2.InvestmentSecurityID = mder.ToCurrencySecurityID)
WHERE poac.ProductOverlayRunID = 734785
  AND s2.IsCurrency = 1
  AND mder.RateDate = '09/29/2017';
