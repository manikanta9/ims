DECLARE
	@RunID INT = 1036067

DECLARE
	@EntityClassName TABLE (
		TableName NVARCHAR(100),
		ClassName NVARCHAR(500)
	)
INSERT INTO @EntityClassName
SELECT 'PortfolioRun', 'com.clifton.portfolio.run.PortfolioRun'
UNION
SELECT 'InvestmentManagerAccount', 'com.clifton.investment.manager.InvestmentManagerAccount'
UNION
SELECT 'InvestmentManagerAccountAssignment', 'com.clifton.investment.manager.InvestmentManagerAccountAssignment'
UNION
SELECT 'InvestmentManagerAccountAllocation', 'com.clifton.investment.manager.InvestmentManagerAccountAllocation'
UNION
SELECT 'InvestmentManagerAccountAssignmentOverlayAction', 'com.clifton.investment.manager.InvestmentManagerAccountAssignmentOverlayAction'
UNION
SELECT 'InvestmentManagerAccountBalance', 'com.clifton.investment.manager.balance.InvestmentManagerAccountBalance'
UNION
SELECT 'InvestmentAccountAssetClass', 'com.clifton.investment.account.assetclass.InvestmentAccountAssetClass'
UNION
SELECT 'InvestmentAssetClass', 'com.clifton.investment.setup.InvestmentAssetClass'
UNION
SELECT 'InvestmentSecurity', 'com.clifton.investment.instrument.InvestmentSecurity'
UNION
SELECT 'InvestmentInstrument', 'com.clifton.investment.instrument.InvestmentInstrument'
UNION
SELECT 'InvestmentAccount', 'com.clifton.investment.account.InvestmentAccount'

DECLARE
	@EntityTable TABLE (
		TableName NVARCHAR(100),
		EntityID  INT
	)
INSERT INTO @EntityTable
SELECT 'PortfolioRun', PortfolioRunID
FROM PortfolioRun
WHERE PortfolioRunID = @RunID
UNION
SELECT 'InvestmentAccount', ClientInvestmentAccountID
FROM PortfolioRun
WHERE PortfolioRunID = @RunID

INSERT INTO @EntityTable
SELECT 'InvestmentAccountAssetClass', InvestmentAccountAssetClassID
FROM InvestmentAccountAssetClass
WHERE InvestmentAccountID IN (SELECT EntityID FROM @EntityTable WHERE TableName = 'InvestmentAccount')
  AND IsInactive = 0

INSERT INTO @EntityTable
SELECT 'InvestmentAssetClass', InvestmentAssetClassID
FROM InvestmentAccountAssetClass
WHERE InvestmentAccountID IN (SELECT EntityID FROM @EntityTable WHERE TableName = 'InvestmentAccount')
  AND IsInactive = 0

INSERT INTO @EntityTable
SELECT DISTINCT 'InvestmentAssetClass', MasterInvestmentAssetClassID
FROM InvestmentAssetClass a
	 INNER JOIN @EntityTable e ON e.TableName = 'InvestmentAssetClass' AND e.EntityID = a.InvestmentAssetClassID
	AND NOT EXISTS(SELECT * FROM @EntityTable e2 WHERE e2.TableName = 'InvestmentAssetClass' AND e2.EntityID = a.MasterInvestmentAssetClassID)

INSERT INTO @EntityTable
SELECT 'InvestmentManagerAccountAssignment', InvestmentManagerAccountAssignmentID
FROM InvestmentManagerAccountAssignment
WHERE InvestmentAccountID IN (SELECT EntityID FROM @EntityTable WHERE TableName = 'InvestmentAccount')
  AND IsInactive = 0
UNION
SELECT DISTINCT 'InvestmentManagerAccount', InvestmentManagerAccountID
FROM InvestmentManagerAccountAssignment
WHERE InvestmentAccountID IN (SELECT EntityID FROM @EntityTable WHERE TableName = 'InvestmentAccount')
  AND IsInactive = 0

INSERT INTO @EntityTable
SELECT 'InvestmentManagerAccountAssignmentOverlayAction', InvestmentManagerAccountAssignmentOverlayActionID
FROM InvestmentManagerAccountAssignmentOverlayAction
WHERE InvestmentManagerAccountAssignmentID IN (SELECT EntityID FROM @EntityTable WHERE TableName = 'InvestmentManagerAccountAssignment')

INSERT INTO @EntityTable
SELECT 'InvestmentManagerAccountAllocation', InvestmentManagerAccountAllocationID
FROM InvestmentManagerAccountAllocation
WHERE InvestmentManagerAccountAssignmentID IN (SELECT EntityID FROM @EntityTable WHERE TableName = 'InvestmentManagerAccountAssignment')

INSERT INTO @EntityTable
SELECT 'InvestmentManagerAccountBalance', InvestmentManagerAccountBalanceID
FROM InvestmentManagerAccountBalance b
	 INNER JOIN @EntityTable e ON e.TableName = 'InvestmentManagerAccount' AND E.EntityID = b.InvestmentManagerAccountID
	AND b.BalanceDate = (SELECT BalanceDate FROM PortfolioRun WHERE PortfolioRunID = @RunID)

INSERT INTO @EntityTable
SELECT DISTINCT 'InvestmentSecurity', InvestmentSecurityID
FROM InvestmentSecurity s
WHERE s.InvestmentSecurityID IN (
	SELECT BaseCurrencyID
	FROM InvestmentAccount a
		 INNER JOIN @EntityTable t ON t.TableName = 'InvestmentAccount' AND t.EntityID = a.InvestmentAccountID
	UNION
	SELECT BaseCurrencyID
	FROM InvestmentManagerAccount a
		 INNER JOIN @EntityTable t ON t.TableName = 'InvestmentManagerAccount' AND t.EntityID = a.InvestmentManagerAccountID
	UNION
	SELECT BenchmarkSecurityID
	FROM InvestmentManagerAccountAllocation a
		 INNER JOIN @EntityTable t ON t.TableName = 'InvestmentManagerAccountAllocation' AND a.InvestmentManagerAccountAllocationID = t.EntityID
	UNION
	SELECT BenchmarkInvestmentSecurityID
	FROM InvestmentAccountAssetClass a
		 INNER JOIN @EntityTable t ON t.TableName = 'InvestmentAccountAssetClass' AND a.InvestmentAccountAssetClassID = t.EntityID
)


SELECT "Data"
FROM (
		 SELECT 1 "Order", TableName "Table", '<util:list id="' + LOWER(SUBSTRING(c.TableName, 1, 1)) + SUBSTRING(c.TableName, 2, len(c.TableName)) + 'DAO_data">' "Data"
		 FROM @EntityClassName c

		 UNION

		 SELECT DISTINCT 2, t.TableName, '<ref bean="' + t.TableName + '-' + CAST(t.EntityID AS VARCHAR) + '" />'
		 FROM @EntityTable t
			  LEFT JOIN @EntityClassName c ON t.TableName = c.TableName
		 WHERE t.EntityID IS NOT NULL

		 UNION

		 SELECT 3, TableName, '</util:list>'
		 FROM @EntityClassName c
	 ) x
ORDER BY x.[Table], x.[Order]


SELECT DISTINCT t.TableName, 'SELECT ''' + t.TableName + ''', (' + dbo.TestDataExport(t.TableName, c.ClassName, t.EntityID) + ') UNION '
FROM @EntityTable t
	 LEFT JOIN @EntityClassName c ON t.TableName = c.TableName
WHERE t.EntityID IS NOT NULL
ORDER BY t.TableName
