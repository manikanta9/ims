package com.clifton.product.overlay.trade;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.trade.PortfolioRunTrade;
import com.clifton.portfolio.run.trade.PortfolioRunTradeService;
import com.clifton.product.ProductInMemoryDatabaseContext;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.TradeOpenCloseTypes;
import com.clifton.trade.TradeService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author NickK
 */
@ProductInMemoryDatabaseContext
public class ProductOverlayTradeCreationTests extends BaseInMemoryDatabaseTests {

	private static final int ITG_BRANDS_07272020_RUN_ID = 1255497;
	private static final int SONOMA_DE_07272020_RUN_ID = 1255056;

	private static final int MORGAN_STANLEY_COMPANY_ID = 4;

	private static final short PORTFOLIO_RUN_STATUS_WORKFLOW_ID = 21;
	private static final short TRADE_WORKFLOW_ID = 6;

	private static final String ESU20_SYMBOL = "ESU0";

	private static final Date TRADE_DATE = DateUtils.toDate("07/27/2020");

	////////////////////////////////////////////////////////////////////////////////

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private PortfolioRunService portfolioRunService;

	@Resource
	private PortfolioRunTradeService<PortfolioRunTrade<ProductOverlayAssetClassReplication>, ProductOverlayAssetClassReplication> portfolioRunTradeService;

	@Resource
	private SecurityUserService securityUserService;

	@Resource
	private TradeService tradeService;

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<org.springframework.core.io.Resource> getFunctionsData() {
		return CollectionUtils.createList(new ClassPathResource("com/clifton/product/ProductInMemoryDatabaseTestsDataFunctions.xml"));
	}


	@Override
	protected IdentityObject getUser() {
		SecurityUser user = new SecurityUser();
		user.setId((short) 1);
		user.setUserName("TestUser");
		return user;
	}


	@Override
	public void beforeTest() {
		// save user for foreign key constraint on Trade and TradeGroup
		SecurityUser userTemplate = (SecurityUser) getUser();
		SecurityUser user = this.securityUserService.getSecurityUserByName(userTemplate.getUserName());
		if (user == null) {
			user = this.securityUserService.saveSecurityUser(userTemplate);
			Assertions.assertNotNull(user);
		}
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
		if (this.calendarSetupService.getCalendarYear((short) 2019) == null) {
			// Note: 2017 and 2018 are loaded via SQL migration
			this.calendarSetupService.saveCalendarYearForYear((short) 2019);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testTradeCreationContractSplittingCorrectlyUpdatesOpenCloseType() {
		validatePortfolioRunSupportsTrading(ITG_BRANDS_07272020_RUN_ID);

		PortfolioRunTrade<ProductOverlayAssetClassReplication> portfolioRunTrade = this.portfolioRunTradeService.getPortfolioRunTrade(ITG_BRANDS_07272020_RUN_ID);
		Assertions.assertNotNull(portfolioRunTrade);

		portfolioRunTrade.setTradeDate(TRADE_DATE);
		BusinessCompany morganStanley = this.businessCompanyService.getBusinessCompany(MORGAN_STANLEY_COMPANY_ID);
		List<ProductOverlayAssetClassReplication> esu7Replications = portfolioRunTrade.getDetailList().stream()
				.filter(replication -> {
					boolean matches = ESU20_SYMBOL.equals(replication.getSecurity().getSymbol());
					if (matches) {
						replication.getTrade().setExecutingBrokerCompany(morganStanley);
					}
					return matches;
				})
				.collect(Collectors.toList());
		Assertions.assertEquals(2, esu7Replications.size());

		esu7Replications.get(0).setBuyContracts(new BigDecimal(2));
		esu7Replications.get(0).setOpenCloseType(this.tradeService.getTradeOpenCloseTypeByName(TradeOpenCloseTypes.BUY.getName()));
		esu7Replications.get(1).setSellContracts(new BigDecimal(5));
		esu7Replications.get(1).setOpenCloseType(this.tradeService.getTradeOpenCloseTypeByName(TradeOpenCloseTypes.SELL.getName()));

		TradeGroup tradeGroup = this.portfolioRunTradeService.processPortfolioRunTradeCreation(portfolioRunTrade);
		try {
			Assertions.assertEquals(1, tradeGroup.getTradeList().size());
			Trade trade = tradeGroup.getTradeList().get(0);
			Assertions.assertEquals(new BigDecimal(3), trade.getQuantityIntended());
			Assertions.assertFalse(trade.isBuy());
			Assertions.assertEquals(TradeOpenCloseTypes.SELL.getName(), trade.getOpenCloseType().getName());
		}
		finally {
			cancelTradeList(tradeGroup.getTradeList());
		}
	}


	@Test
	public void testTradeCreationContractSplittingCorrectlyUpdatesOpenCloseTypeReverseOrder() {
		validatePortfolioRunSupportsTrading(ITG_BRANDS_07272020_RUN_ID);

		PortfolioRunTrade<ProductOverlayAssetClassReplication> portfolioRunTrade = this.portfolioRunTradeService.getPortfolioRunTrade(ITG_BRANDS_07272020_RUN_ID);
		Assertions.assertNotNull(portfolioRunTrade);

		portfolioRunTrade.setTradeDate(TRADE_DATE);
		BusinessCompany morganStanley = this.businessCompanyService.getBusinessCompany(MORGAN_STANLEY_COMPANY_ID);
		List<ProductOverlayAssetClassReplication> esu7Replications = portfolioRunTrade.getDetailList().stream()
				.filter(replication -> {
					boolean matches = ESU20_SYMBOL.equals(replication.getSecurity().getSymbol());
					if (matches) {
						replication.getTrade().setExecutingBrokerCompany(morganStanley);
					}
					return matches;
				})
				.collect(Collectors.toList());
		Assertions.assertEquals(2, esu7Replications.size());

		esu7Replications.get(0).setBuyContracts(new BigDecimal(5));
		esu7Replications.get(0).setOpenCloseType(this.tradeService.getTradeOpenCloseTypeByName(TradeOpenCloseTypes.BUY.getName()));
		esu7Replications.get(1).setSellContracts(new BigDecimal(2));
		esu7Replications.get(1).setOpenCloseType(this.tradeService.getTradeOpenCloseTypeByName(TradeOpenCloseTypes.SELL.getName()));

		TradeGroup tradeGroup = this.portfolioRunTradeService.processPortfolioRunTradeCreation(portfolioRunTrade);
		try {
			Assertions.assertEquals(1, tradeGroup.getTradeList().size());
			Trade trade = tradeGroup.getTradeList().get(0);
			Assertions.assertEquals(new BigDecimal(3), trade.getQuantityIntended());
			Assertions.assertTrue(trade.isBuy());
			Assertions.assertEquals(TradeOpenCloseTypes.BUY.getName(), trade.getOpenCloseType().getName());
		}
		finally {
			cancelTradeList(tradeGroup.getTradeList());
		}
	}


	@Test
	public void testTradeCreationOptionCorrectlyUpdatesOpenCloseType() {
		validatePortfolioRunSupportsTrading(SONOMA_DE_07272020_RUN_ID);

		PortfolioRunTrade<ProductOverlayAssetClassReplication> portfolioRunTrade = this.portfolioRunTradeService.getPortfolioRunTrade(SONOMA_DE_07272020_RUN_ID);
		Assertions.assertNotNull(portfolioRunTrade);

		portfolioRunTrade.setTradeDate(TRADE_DATE);
		BusinessCompany morganStanley = this.businessCompanyService.getBusinessCompany(MORGAN_STANLEY_COMPANY_ID);
		TradeOpenCloseType buyClose = this.tradeService.getTradeOpenCloseTypeByName(TradeOpenCloseTypes.BUY_CLOSE.getName());
		TradeOpenCloseType buyOpen = this.tradeService.getTradeOpenCloseTypeByName(TradeOpenCloseTypes.BUY_OPEN.getName());
		TradeOpenCloseType sellOpen = this.tradeService.getTradeOpenCloseTypeByName(TradeOpenCloseTypes.SELL_OPEN.getName());
		TradeOpenCloseType sellClose = this.tradeService.getTradeOpenCloseTypeByName(TradeOpenCloseTypes.SELL_CLOSE.getName());
		portfolioRunTrade.getDetailList().stream().filter(replication -> !replication.isTradeEntryDisabled()).forEach(replication -> {
			boolean traded = false;
			switch (replication.getSecurity().getSymbol()) {
				case "SPXW US 07/31/20 C3260":
				case "SPXW US 07/31/20 P3080": {
					replication.setBuyContracts(BigDecimal.ONE);
					replication.setOpenCloseType(MathUtils.isLessThan(replication.getCurrentContractsAdjusted(), BigDecimal.ZERO) ? buyClose : buyOpen);
					traded = true;
					break;
				}
				case "SPXW US 08/03/20 C3300":
				case "SPXW US 08/03/20 P3140": {
					replication.setSellContracts(BigDecimal.ONE);
					replication.setOpenCloseType(MathUtils.isGreaterThan(replication.getCurrentContractsAdjusted(), BigDecimal.ZERO) ? sellClose : sellOpen);
					traded = true;
					break;
				}
			}
			if (traded) {
				replication.getTrade().setExecutingBrokerCompany(morganStanley);
			}
		});

		TradeGroup tradeGroup = this.portfolioRunTradeService.processPortfolioRunTradeCreation(portfolioRunTrade);
		try {
			Assertions.assertEquals(4, tradeGroup.getTradeList().size());
			tradeGroup.getTradeList().forEach(trade -> {
				Assertions.assertEquals(BigDecimal.ONE, trade.getQuantityIntended());
				boolean buy = false;
				TradeOpenCloseType openCloseType = null;
				switch (trade.getInvestmentSecurity().getSymbol()) {
					case "SPXW US 07/31/20 C3260":
					case "SPXW US 07/31/20 P3080": {
						buy = true;
						openCloseType = buyClose;
						break;
					}
					case "SPXW US 08/03/20 C3300":
					case "SPXW US 08/03/20 P3140": {
						buy = false;
						openCloseType = sellOpen;
						break;
					}
				}
				Assertions.assertEquals(buy, trade.isBuy());
				Assertions.assertEquals(openCloseType, trade.getOpenCloseType());
			});
		}
		finally {
			cancelTradeList(tradeGroup.getTradeList());
		}
	}


	private void validatePortfolioRunSupportsTrading(int runId) {
		PortfolioRun run = this.portfolioRunService.getPortfolioRun(runId);
		Assertions.assertNotNull(run);
		if (!"Pending".equals(run.getWorkflowState().getStatus().getName())) {
			WorkflowState pendingState = this.workflowDefinitionService.getWorkflowStateByName(PORTFOLIO_RUN_STATUS_WORKFLOW_ID, "Analyst Approved");
			run.setWorkflowState(pendingState);
			run.setWorkflowStatus(pendingState.getStatus());
			DaoUtils.executeWithAllObserversDisabled(() -> this.portfolioRunService.savePortfolioRun(run));
		}
	}


	private void cancelTradeList(List<Trade> tradeList) {
		WorkflowState cancelledState = this.workflowDefinitionService.getWorkflowStateByName(TRADE_WORKFLOW_ID, "Cancelled");
		CollectionUtils.getStream(tradeList).forEach(trade -> {
			try {
				trade.setWorkflowState(cancelledState);
				trade.setWorkflowStatus(cancelledState.getStatus());
				this.tradeService.saveTrade(trade);
			}
			catch (Exception e) {
				// ignore to try all trades
			}
		});
	}
}
