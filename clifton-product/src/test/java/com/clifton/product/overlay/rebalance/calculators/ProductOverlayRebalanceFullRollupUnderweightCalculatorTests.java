package com.clifton.product.overlay.rebalance.calculators;


import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;
import com.clifton.product.overlay.rebalance.ProductOverlayRebalance;
import com.clifton.product.overlay.rebalance.minimizeimbalances.calculators.ProductOverlayMinimizeImbalancesRollupUnderweightCalculator;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>ProductOverlayRebalanceFullRollupUnderweightCalculatorTests</code> ...
 * <p>
 * RollupUnderweight minimize Imbalances calculator has special full rebalance override
 * when apply proportionally to children is used.
 *
 * @author manderson
 */
@ContextConfiguration(locations = "ProductOverlayRebalanceCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ProductOverlayRebalanceFullRollupUnderweightCalculatorTests extends BaseProductOverlayRebalanceCalculatorTests {

	@Resource
	private SystemColumnValueHandler systemColumnValueHandler;


	@Override
	protected ProductOverlayRebalanceCalculator getCalculator() {
		ProductOverlayMinimizeImbalancesRollupUnderweightCalculator calculator = new ProductOverlayMinimizeImbalancesRollupUnderweightCalculator();
		calculator.setShortTargetsAllowed(true);
		calculator.setExpandShortPositionsAllowed(true);
		calculator.setAttributionRebalancing(true);
		return calculator;
	}


	@Override
	public RebalanceCalculationTypes getRebalanceCalculationType() {
		return RebalanceCalculationTypes.FULL;
	}


	@Test
	public void test_RollupUnderweight_OneLevelDeep() {
		// 423000 M&M on 10/11
		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.APPLY_ROLLUP_PROPORTIONAL_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(false);

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.EXCLUDE_ROLLUP_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(false);

		Map<String, BigDecimal> resultMap = new HashMap<>();
		ProductOverlayRebalance rebalance = createProductOverlayRebalance();

		addRollupAssetClassToList(rebalance, null, "Equity", 58.41, 1201109921, 1256092335, 3358, 15939, 0, 0, -36541586, 0);
		addRollupAssetClassToList(rebalance, null, "Fixed", 31.50, 647738688, 584247806, 1477206, 7011964, 0, 0, 36541586, 0);

		addAssetClassToList(rebalance, resultMap, "Tactical", false, 207034325, 207034325, 0, 0, 0, 0, 0, 0, 0);
		addAssetClassToList(rebalance, resultMap, "Cash", false, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		addAssetClassToList(rebalance, resultMap, "Private Equity/Debt", false, 1523430, 1523430, 0, 0, 0, 0, 0, 0, 0);

		addChildAssetClassToList(rebalance, resultMap, "Equity", "Domestic Equity", true, 22.07, 450671945, 486708815, 0, 0, 0, 0, -25104084, 0, -10932786);
		addChildAssetClassToList(rebalance, resultMap, "Fixed", "Fixed Income", true, 19.20, 403974546, 357605305, 1043816, 4954756, 0, 0, 25820817, 0, 14549853.58);
		addChildAssetClassToList(rebalance, resultMap, "Fixed", "Fixed Income - Short Duration", true, 5.16, 108568159, 91446519, 433391, 2057208, 0, 0, 10720769, 0, 3910272.42);
		addChildAssetClassToList(rebalance, resultMap, "Equity", "International Equity", true, 18.06, 368787283, 387801694, 0, 0, 0, 0, -11520567, 0, -7493844);
		addChildAssetClassToList(rebalance, resultMap, "Equity", "Intl Equity-Emerging", true, 6.88, 140490393, 140421526, 3358, 15939, 0, 0, 83065, 0, -33495);
		addChildAssetClassToList(rebalance, resultMap, "Equity", "Long/Short Equity", true, 11.46, 241160299, 241160299, 0, 0, 0, 0, 0, 0, 0);
		addChildAssetClassToList(rebalance, resultMap, "Fixed", "Inflated-Related", true, 7.17, 135195983, 135195983, 0, 0, 0, 0, 0, 0, 0);

		calculateNewRebalanceCash(rebalance);
		validateResults(rebalance, resultMap);
	}


	@Test
	public void test_RollupUnderweight_TwoLevelsDeep() {
		// 423000 M&M on 10/11
		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.APPLY_ROLLUP_PROPORTIONAL_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(false);

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.EXCLUDE_ROLLUP_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(false);

		Map<String, BigDecimal> resultMap = new HashMap<>();
		ProductOverlayRebalance rebalance = createProductOverlayRebalance();

		addRollupAssetClassToList(rebalance, null, "Equity", 58.41, 1201109921, 1256092335, 3358, 15939, 0, 0, -36541586, 0);
		addRollupAssetClassToList(rebalance, "Equity", "International", 24.94, 509277676, 528223220, 3358, 15939, 0, 0, -11437502, 0);
		addRollupAssetClassToList(rebalance, null, "Fixed", 31.50, 647738688, 584247806, 1477206, 7011964, 0, 0, 36541586, 0);

		addAssetClassToList(rebalance, resultMap, "Tactical", false, 207034325, 207034325, 0, 0, 0, 0, 0, 0, 0);
		addAssetClassToList(rebalance, resultMap, "Cash", false, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		addAssetClassToList(rebalance, resultMap, "Private Equity/Debt", false, 1523430, 1523430, 0, 0, 0, 0, 0, 0, 0);

		addChildAssetClassToList(rebalance, resultMap, "Equity", "Domestic Equity", true, 22.07, 450671945, 486708815, 0, 0, 0, 0, -25104084, 0, -10932786);
		addChildAssetClassToList(rebalance, resultMap, "Fixed", "Fixed Income", true, 19.20, 403974546, 357605305, 1043816, 4954756, 0, 0, 25820817, 0, 14549853.58);
		addChildAssetClassToList(rebalance, resultMap, "Fixed", "Fixed Income - Short Duration", true, 5.16, 108568159, 91446519, 433391, 2057208, 0, 0, 10720769, 0, 3910272.42);
		addChildAssetClassToList(rebalance, resultMap, "International", "International Equity", true, 18.06, 368787283, 387801694, 0, 0, 0, 0, -11520567, 0, -7493844);
		addChildAssetClassToList(rebalance, resultMap, "International", "Intl Equity-Emerging", true, 6.88, 140490393, 140421526, 3358, 15939, 0, 0, 83065, 0, -33495);
		addChildAssetClassToList(rebalance, resultMap, "Equity", "Long/Short Equity", true, 11.46, 241160299, 241160299, 0, 0, 0, 0, 0, 0, 0);
		addChildAssetClassToList(rebalance, resultMap, "Fixed", "Inflated-Related", true, 7.17, 135195983, 135195983, 0, 0, 0, 0, 0, 0, 0);

		calculateNewRebalanceCash(rebalance);
		validateResults(rebalance, resultMap);
	}


	@Test
	public void test_RollupUnderweight_ApplytoChildrenProportionally_OneLevelDeep() {

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.APPLY_ROLLUP_PROPORTIONAL_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(true);

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.EXCLUDE_ROLLUP_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(false);

		Map<String, BigDecimal> resultMap = new HashMap<>();
		ProductOverlayRebalance rebalance = createProductOverlayRebalance();

		addRollupAssetClassToList(rebalance, null, "Equity", 58.41, 1201109921, 1256092335, 3358, 15939, 0, 0, -36541586, 0);
		addRollupAssetClassToList(rebalance, null, "Fixed", 31.50, 647738688, 584247806, 1477206, 7011964, 0, 0, 36541586, 0);

		addAssetClassToList(rebalance, resultMap, "Tactical", false, 207034325, 207034325, 0, 0, 0, 0, 0, 0, 0);
		addAssetClassToList(rebalance, resultMap, "Cash", false, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		addAssetClassToList(rebalance, resultMap, "Private Equity/Debt", false, 1523430, 1523430, 0, 0, 0, 0, 0, 0, 0);

		addChildAssetClassToList(rebalance, resultMap, "Equity", "Domestic Equity", true, 22.07, 450671945, 486708815, 0, 0, 0, 0, -25104084, 0, -6967931.57);
		addChildAssetClassToList(rebalance, resultMap, "Fixed", "Fixed Income", true, 19.20, 403974546, 357605305, 1043816, 4954756, 0, 0, 25820817, 0, 11241180.44);
		addChildAssetClassToList(rebalance, resultMap, "Fixed", "Fixed Income - Short Duration", true, 5.16, 108568159, 91446519, 433391, 2057208, 0, 0, 10720769, 0, 3021067.24);
		addChildAssetClassToList(rebalance, resultMap, "Equity", "International Equity", true, 18.06, 368787283, 387801694, 0, 0, 0, 0, -11520567, 0, -5701895.97);
		addChildAssetClassToList(rebalance, resultMap, "Equity", "Intl Equity-Emerging", true, 6.88, 140490393, 140421526, 3358, 15939, 0, 0, 83065, 0, -2172150.85);
		addChildAssetClassToList(rebalance, resultMap, "Equity", "Long/Short Equity", true, 11.46, 241160299, 241160299, 0, 0, 0, 0, 0, 0, -3618146.61);
		addChildAssetClassToList(rebalance, resultMap, "Fixed", "Inflated-Related", true, 7.17, 135195983, 135195983, 0, 0, 0, 0, 0, 0, 4197878.32);

		calculateNewRebalanceCash(rebalance);
		validateResults(rebalance, resultMap);
	}


	@Test
	public void test_RollupUnderweight_ApplytoChildrenProportionally_TwoLevelsDeep() {
		// 423000 M&M on 10/11
		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.APPLY_ROLLUP_PROPORTIONAL_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(true);

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.EXCLUDE_ROLLUP_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenReturn(false);

		Map<String, BigDecimal> resultMap = new HashMap<>();
		ProductOverlayRebalance rebalance = createProductOverlayRebalance();

		addRollupAssetClassToList(rebalance, null, "Equity", 58.41, 1201109921, 1256092335, 3358, 15939, 0, 0, -36541586, 0);
		addRollupAssetClassToList(rebalance, "Equity", "International", 24.94, 509277676, 528223220, 3358, 15939, 0, 0, -11437502, 0);
		addRollupAssetClassToList(rebalance, null, "Fixed", 31.50, 647738688, 584247806, 1477206, 7011964, 0, 0, 36541586, 0);

		addAssetClassToList(rebalance, resultMap, "Tactical", false, 207034325, 207034325, 0, 0, 0, 0, 0, 0, 0);
		addAssetClassToList(rebalance, resultMap, "Cash", false, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		addAssetClassToList(rebalance, resultMap, "Private Equity/Debt", false, 1523430, 1523430, 0, 0, 0, 0, 0, 0, 0);

		addChildAssetClassToList(rebalance, resultMap, "Equity", "Domestic Equity", true, 22.07, 450671945, 486708815, 0, 0, 0, 0, -25104084, 0, -6967931.57);
		addChildAssetClassToList(rebalance, resultMap, "Fixed", "Fixed Income", true, 19.20, 403974546, 357605305, 1043816, 4954756, 0, 0, 25820817, 0, 11241180.44);
		addChildAssetClassToList(rebalance, resultMap, "Fixed", "Fixed Income - Short Duration", true, 5.16, 108568159, 91446519, 433391, 2057208, 0, 0, 10720769, 0, 3021067.24);
		addChildAssetClassToList(rebalance, resultMap, "International", "International Equity", true, 18.06, 368787283, 387801694, 0, 0, 0, 0, -11520567, 0, -5701895.97);
		addChildAssetClassToList(rebalance, resultMap, "International", "Intl Equity-Emerging", true, 6.88, 140490393, 140421526, 3358, 15939, 0, 0, 83065, 0, -2172150.85);
		addChildAssetClassToList(rebalance, resultMap, "Equity", "Long/Short Equity", true, 11.46, 241160299, 241160299, 0, 0, 0, 0, 0, 0, -3618146.61);
		addChildAssetClassToList(rebalance, resultMap, "Fixed", "Inflated-Related", true, 7.17, 135195983, 135195983, 0, 0, 0, 0, 0, 0, 4197878.32);

		calculateNewRebalanceCash(rebalance);
		validateResults(rebalance, resultMap);
	}


	@Test
	public void test_ApplyToChildrenProportionally_Exclude_AndRegularUnderweight_FullRebalance() {
		// Sonoma County on 10/31/2014 With a FULL Rebalance
		// Equity: Exclude, Fixed: Regular, GAA: Proportional
		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.APPLY_ROLLUP_PROPORTIONAL_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenAnswer(
				invocation -> {
					Object[] args = invocation.getArguments();
					InvestmentAccountAssetClass ac = (InvestmentAccountAssetClass) args[0];
					if ("Global Asset Allocation".equals(ac.getAssetClass().getName())) {
						return true;
					}
					return false;
				});

		Mockito.when(
				this.systemColumnValueHandler.getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentAccountAssetClass.class),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.ROLLUP_OPTIONS_CUSTOM_COLUMN_GROUP_NAME),
						ArgumentMatchers.eq(ProductOverlayMinimizeImbalancesRollupUnderweightCalculator.EXCLUDE_ROLLUP_ASSET_CLASS_COLUMN_NAME), ArgumentMatchers.eq(true))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InvestmentAccountAssetClass ac = (InvestmentAccountAssetClass) args[0];
			if ("Total Equity".equals(ac.getAssetClass().getName())) {
				return true;
			}
			return false;
		});

		Map<String, BigDecimal> resultMap = new HashMap<>();
		ProductOverlayRebalance rebalance = createProductOverlayRebalance();

		addRollupAssetClassToList(rebalance, null, "Real Estate", 15.00000, 367446578.19, 367446578.19, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addRollupAssetClassToList(rebalance, null, "Global Asset Allocation", 8.00000, 186609045.05, 176741352.58, 1940958.08, 7709676.81, 0.00, -1126659.60, 1343717.18, 0.00);
		addRollupAssetClassToList(rebalance, null, "Fixed Income", 20.00000, 466522612.63, 431835637.25, 7962022.10, 31625936.61, 0.00, -10413057.99, 5512074.66, 0.00);
		addRollupAssetClassToList(rebalance, null, "Total Equity", 57.00000, 1329589445.99, 1295316529.97, 5951006.36, 23637983.91, 0.00, 11539717.59, -6855791.84, 0.00);

		addAssetClassToList(rebalance, resultMap, "Cash", false, 0.00, 78827583.87, 0.00, 0.00, 0.00, 0.0, 0.00, 0.00, 0.00);

		addChildAssetClassToList(rebalance, resultMap, "Fixed Income", "Core Fixed Income", true, 14.00000, 326565828.84, 301036401.48, 7962022.10, 31625936.61, 0.00, -29164448.86, 5512074.66, 0.00,
				-10413057.99);
		addChildAssetClassToList(rebalance, resultMap, "Fixed Income", "Fixed Income Alternative", false, 6.00000, 139956783.79, 130799235.77, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addChildAssetClassToList(rebalance, resultMap, "Global Asset Allocation", "Global Asset Equity", true, 5.20000, 121295879.28, 114881879.18, 1261622.75, 5011289.93, 0.00, -1143440.35,
				873416.16, 0.00, -732328.74);
		addChildAssetClassToList(rebalance, resultMap, "Global Asset Allocation", "Global Asset Fixed Income", true, 2.80000, 65313165.77, 61859473.40, 679335.33, 2698386.88, 0.00, -572444.15,
				470301.01, 0.00, -394330.86);
		addChildAssetClassToList(rebalance, resultMap, "Real Estate", "Farmland Real Estate", false, 5.00000, 113567767.70, 113567767.70, 0.00, 0.00, 0.00, 0.0, 0.00, 0.00, 0.00);
		addChildAssetClassToList(rebalance, resultMap, "Real Estate", "Real Estate - Core", false, 10.00000, 253878810.49, 253878810.49, 0.00, 0.00, 0.00, 0.0, 0.00, 0.00, 0.00);
		addChildAssetClassToList(rebalance, resultMap, "Total Equity", "Domestic Equity - Broad Mandate", true, 8.00000, 186609045.05, 195934152.09, 0.00, 0.00, 0.00, 4989126.91, -7076419.60, 0.00,
				-2248687.44);
		addChildAssetClassToList(rebalance, resultMap, "Total Equity", "Domestic Equity Large Cap", true, 17.00000, 396544220.74, 396827516.47, 0.00, 0.00, 0.00, 9147733.65, -3899229.10, 0.00,
				3615933.37);
		addChildAssetClassToList(rebalance, resultMap, "Total Equity", "Domestic Equity Small Cap", true, 5.00000, 116630653.15, 111458100.12, 1636665.34, 6500996.07, 0.00, 1176655.21, 1133056.58,
				0.00, -4098164.96);
		addChildAssetClassToList(rebalance, resultMap, "Total Equity", "Global Equity", true, 10.00000, 233261306.32, 223602564.60, 1148385.70, 4561501.23, 0.00, 5958322.83, 795022.63, 0.00,
				3153832.16);
		addChildAssetClassToList(rebalance, resultMap, "Total Equity", "Non-U.S. Equity", true, 17.00000, 396544220.73, 367494196.69, 3165955.31, 12575486.61, 0.00, 9608494.77, 2191777.65, 0.00,
				11116804.47);

		calculateNewRebalanceCash(rebalance);
		validateResults(rebalance, resultMap);
	}
}
