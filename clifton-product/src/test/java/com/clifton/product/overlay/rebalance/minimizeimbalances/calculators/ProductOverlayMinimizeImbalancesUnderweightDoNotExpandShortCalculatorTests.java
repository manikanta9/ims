package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ContextConfiguration("ProductOverlayMinimizeImbalancesCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ProductOverlayMinimizeImbalancesUnderweightDoNotExpandShortCalculatorTests extends BaseProductOverlayMinimizeImbalancesCalculatorTests {

	@Override
	public ProductOverlayMinimizeImbalancesCalculator getCalculator() {
		ProductOverlayMinimizeImbalancesUnderweightCalculator calculator = new ProductOverlayMinimizeImbalancesUnderweightCalculator();
		calculator.setShortTargetsAllowed(true);
		calculator.setExpandShortPositionsAllowed(false);
		calculator.setAttributionRebalancing(true);
		return calculator;
	}


	@Test
	public void test_ShortsUnderweight_Buy() {
		// 523500: Presb. on 10/23/2013 - No reason to increase shorts - so results would be the same as with the traditional Underwieght calculator
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(null, null, -37305685.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2331, "Total Capital Appreciation").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2332, "Total Bonds").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2294, "Diversifying Hedge Funds").withAssetClassPercentage(15).withAdjustedTarget(95059136.47).withPhysicalExposure(95059136.47));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2295, "Public Inflation Sensitive - Commodities").withAssetClassPercentage(3).withAdjustedTarget(20810417.64).withPhysicalExposure(20135371.96).withOverlayExposure(624500.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2298, "Private Inflation Sensitive (PIS)").withAssetClassPercentage(0).withAdjustedTarget(429626.00).withPhysicalExposure(429626.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2301, "Cash").withAssetClassPercentage(4).withAdjustedTarget(37305685.06).withPhysicalExposure(33005965.21).withOverlayExposure(4780827.22).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 33005965.21));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2440, "Public Inflation Sensitive - TIPS").withAssetClassPercentage(2).withAdjustedTarget(13020976.15).withPhysicalExposure(13020976.15));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2291, "Public Equities").withAssetClassPercentage(53).withAdjustedTarget(342459617.52).withPhysicalExposure(349775653.40).withOverlayExposure(-8167084.63));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2292, "Private Equity").withAssetClassPercentage(0).withAdjustedTarget(33450330.00).withPhysicalExposure(33450330.00));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2293, "Market Sensitive Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(67195899.67).withPhysicalExposure(67195899.67));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2299, "Domestic Bonds").withAssetClassPercentage(10).withAdjustedTarget(73417467.45).withPhysicalExposure(71076197.10).withOverlayExposure(2761757.41));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClass(2300, "Global Bonds").withAssetClassPercentage(3).withAdjustedTarget(17691865.32).withPhysicalExposure(17691865.32));

		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Total Capital Appreciation");
		validateRollupAssetClassAllocation(config, "Total Bonds");
		validateZeroAssetClassAllocation(config, "Diversifying Hedge Funds");
		validateAssetClassAllocation(config, "Public Inflation Sensitive - Commodities", 0.00, 0.00, 0.00, 0.00, 0.00, 650957.51);
		validateZeroAssetClassAllocation(config, "Private Inflation Sensitive (PIS)");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Public Inflation Sensitive - TIPS");
		validateAssetClassAllocation(config, "Public Equities", 0.00, 0.00, 0.00, 0.00, 0.00, -7712434.71);
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Market Sensitive Hedge Funds");
		validateAssetClassAllocation(config, "Domestic Bonds", 0.00, 0.00, 0.00, 0.00, 0.00, 2761757.41);
		validateZeroAssetClassAllocation(config, "Global Bonds");
		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 33,005,965.21 Cash Adjustment: -37,305,685.00 Cash To Apply: -4,299,719.79");
	}


	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////


	@Test
	public void test_ShortsOverweight_RemainSame() {
		// 523500: Presb. on 10/23/2013 - No reason to increase shorts - so results would be the same as with the traditional Underwieght calculator
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(null, null, -39305685.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2331, "Total Capital Appreciation").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2332, "Total Bonds").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2294, "Diversifying Hedge Funds").withAssetClassPercentage(15).withAdjustedTarget(95059136.47).withPhysicalExposure(95059136.47));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2295, "Public Inflation Sensitive - Commodities").withAssetClassPercentage(3).withAdjustedTarget(20810417.64).withPhysicalExposure(20135371.96).withOverlayExposure(624500.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2298, "Private Inflation Sensitive (PIS)").withAssetClassPercentage(0).withAdjustedTarget(429626.00).withPhysicalExposure(429626.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2301, "Cash").withAssetClassPercentage(4).withAdjustedTarget(37305685.06).withPhysicalExposure(33005965.21).withOverlayExposure(4780827.22).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 33005965.21));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2440, "Public Inflation Sensitive - TIPS").withAssetClassPercentage(2).withAdjustedTarget(13020976.15).withPhysicalExposure(13020976.15));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2291, "Public Equities").withAssetClassPercentage(53).withAdjustedTarget(342459617.52).withPhysicalExposure(349775653.40).withOverlayExposure(-8167084.63));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2292, "Private Equity").withAssetClassPercentage(0).withAdjustedTarget(33450330.00).withPhysicalExposure(33450330.00));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2293, "Market Sensitive Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(67195899.67).withPhysicalExposure(67195899.67));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2299, "Domestic Bonds").withAssetClassPercentage(10).withAdjustedTarget(73417467.45).withPhysicalExposure(71076197.10).withOverlayExposure(2761757.41));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClass(2300, "Global Bonds").withAssetClassPercentage(3).withAdjustedTarget(17691865.32).withPhysicalExposure(17691865.32));

		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Total Capital Appreciation");
		validateRollupAssetClassAllocation(config, "Total Bonds");
		validateZeroAssetClassAllocation(config, "Diversifying Hedge Funds");
		validateAssetClassAllocation(config, "Public Inflation Sensitive - Commodities", 0.00, 0.00, 0.00, 0.00, 0.00, 421297.50);
		validateZeroAssetClassAllocation(config, "Private Inflation Sensitive (PIS)");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Public Inflation Sensitive - TIPS");
		validateAssetClassAllocation(config, "Public Equities", 0.00, 0.00, 0.00, 0.00, 0.00, -8167084.63);
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Market Sensitive Hedge Funds");
		validateAssetClassAllocation(config, "Domestic Bonds", 0.00, 0.00, 0.00, 0.00, 0.00, 1446067.34);
		validateZeroAssetClassAllocation(config, "Global Bonds");

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 33,005,965.21 Cash Adjustment: -39,305,685.00 Cash To Apply: -6,299,719.79");
	}


	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////


	@Test
	public void test_OverweightPositiveOverlayExposureDoNotGoShort() {
		// 523500: Presb. on 11/05/2013 - Should have same result as underweight long only
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(null, null, -35477805.14);
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2331, "Total Capital Appreciation").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2332, "Total Bonds").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2294, "Diversifying Hedge Funds").withAssetClassPercentage(15).withAdjustedTarget(94978294.28).withPhysicalExposure(94978294.28));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2295, "Public Inflation Sensitive - Commodities").withAssetClassPercentage(3).withAdjustedTarget(20537145.56).withPhysicalExposure(19450870.65).withOverlayExposure(759375.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2298, "Private Inflation Sensitive (PIS)").withAssetClassPercentage(0).withAdjustedTarget(429626.00).withPhysicalExposure(429626.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2301, "Cash").withAssetClassPercentage(4).withAdjustedTarget(35477804.73).withPhysicalExposure(35969732.14).withOverlayExposure(-3452636.91).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 35969732.14));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2440, "Public Inflation Sensitive - TIPS").withAssetClassPercentage(2).withAdjustedTarget(12994755.90).withPhysicalExposure(12994755.90));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2291, "Public Equities").withAssetClassPercentage(53).withAdjustedTarget(336477457.67).withPhysicalExposure(338414808.71));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2292, "Private Equity").withAssetClassPercentage(0).withAdjustedTarget(33811334.00).withPhysicalExposure(33811334.00));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2293, "Market Sensitive Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(67100380.11).withPhysicalExposure(67100380.11));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2299, "Domestic Bonds").withAssetClassPercentage(10).withAdjustedTarget(72275075.92).withPhysicalExposure(70932072.38).withOverlayExposure(2693261.91));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClass(2300, "Global Bonds").withAssetClassPercentage(3).withAdjustedTarget(17650077.84).withPhysicalExposure(17650077.84));


		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Total Capital Appreciation");
		validateRollupAssetClassAllocation(config, "Total Bonds");
		validateZeroAssetClassAllocation(config, "Diversifying Hedge Funds");
		validateAssetClassAllocation(config, "Public Inflation Sensitive - Commodities", 0.00, 22360.32, 0.00, 0.00, 0.00, 469566.68);
		validateZeroAssetClassAllocation(config, "Private Inflation Sensitive (PIS)");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Public Inflation Sensitive - TIPS");
		validateAssetClassAllocation(config, "Public Equities", 0.00, 395032.29, 0.00, 0.00, 0.00, -395032.29);
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Market Sensitive Hedge Funds");
		validateAssetClassAllocation(config, "Domestic Bonds", 0.00, 74534.39, 0.00, 0.00, 0.00, -74534.39);
		validateZeroAssetClassAllocation(config, "Global Bonds");

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 35,969,732.14 Cash Adjustment: -35,477,805.14 Cash To Apply: 491,927.00");
	}


	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////


	@Test
	public void test_SellEquallyMinimizeImbalances() {
		// 523500: Presb. on 10/23/2013 - Should have same result as underweight 
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(null, null, -36660685.00);
		// Cash Available for Minimize Imbalances: Fund Cash 30005965.21

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2331, "Total Capital Appreciation").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2332, "Total Bonds").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2294, "Diversifying Hedge Funds").withAssetClassPercentage(15).withAdjustedTarget(95059136.47).withPhysicalExposure(95059136.47));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2295, "Public Inflation Sensitive - Commodities").withAssetClassPercentage(3).withAdjustedTarget(20720417.64).withPhysicalExposure(20135371.96).withOverlayExposure(624500.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2298, "Private Inflation Sensitive (PIS)").withAssetClassPercentage(0).withAdjustedTarget(429626.00).withPhysicalExposure(429626.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2301, "Cash").withAssetClassPercentage(4).withAdjustedTarget(36660685.06).withPhysicalExposure(30005965.21).withOverlayExposure(4780827.22).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 30005965.21));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2440, "Public Inflation Sensitive - TIPS").withAssetClassPercentage(2).withAdjustedTarget(13020976.15).withPhysicalExposure(13020976.15));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2291, "Public Equities").withAssetClassPercentage(53).withAdjustedTarget(340584617.52).withPhysicalExposure(349775653.40).withOverlayExposure(-8167084.63));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2292, "Private Equity").withAssetClassPercentage(0).withAdjustedTarget(33450330.00).withPhysicalExposure(33450330.00));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2293, "Market Sensitive Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(67195899.67).withPhysicalExposure(67195899.67));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2299, "Domestic Bonds").withAssetClassPercentage(10).withAdjustedTarget(73027467.45).withPhysicalExposure(71076197.10).withOverlayExposure(2761757.41));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClass(2300, "Global Bonds").withAssetClassPercentage(3).withAdjustedTarget(17691865.32).withPhysicalExposure(17691865.32));


		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Total Capital Appreciation");
		validateRollupAssetClassAllocation(config, "Total Bonds");
		validateZeroAssetClassAllocation(config, "Diversifying Hedge Funds");
		validateAssetClassAllocation(config, "Public Inflation Sensitive - Commodities", 0.00, 0.00, 0.00, 0.00, 0.00, 358729.15);
		validateZeroAssetClassAllocation(config, "Private Inflation Sensitive (PIS)");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Public Inflation Sensitive - TIPS");
		validateAssetClassAllocation(config, "Public Equities", 0.00, 0.00, 0.00, 0.00, 0.00, -8167084.63);
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Market Sensitive Hedge Funds");
		validateAssetClassAllocation(config, "Domestic Bonds", 0.00, 0.00, 0.00, 0.00, 0.00, 1153635.69);
		validateZeroAssetClassAllocation(config, "Global Bonds");

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 30,005,965.21 Cash Adjustment: -36,660,685.00 Cash To Apply: -6,654,719.79");
	}
}
