package com.clifton.product.overlay.assetclass;

import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustment;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentTypes;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * The <code>ProductOverlayAssetClassTests</code> tests the calculated getters on the
 * {@link ProductOverlayAssetClass} bean
 *
 * @author Mary Anderson
 */
public class ProductOverlayAssetClassTests {

	private ProductOverlayAssetClass bean1;
	private ProductOverlayAssetClass bean2;
	private ProductOverlayAssetClass bean3;

	private static BigDecimal BIG_DECIMAL_ZERO_SCALED = MathUtils.round(BigDecimal.ZERO, 2);


	@BeforeEach
	public void setupTests() {
		PortfolioRun run = new PortfolioRun();
		run.setPortfolioTotalValue(BigDecimal.valueOf(36032472.81));

		ProductOverlayAssetClass bean = new ProductOverlayAssetClass();
		InvestmentAccountAssetClass ac = new InvestmentAccountAssetClass();
		bean.setAccountAssetClass(ac);
		bean.setOverlayRun(run);
		bean.setOverlayExposure(BigDecimal.ZERO);
		bean.setActualAllocation(BigDecimal.ZERO);
		bean.setActualAllocationAdjusted(BigDecimal.ZERO);
		bean.setTargetAllocation(BigDecimal.ZERO);
		bean.setTargetAllocationAdjusted(BigDecimal.ZERO);
		this.bean1 = bean;

		bean = new ProductOverlayAssetClass();
		ac = new InvestmentAccountAssetClass();
		bean.setAccountAssetClass(ac);
		bean.setOverlayRun(run);
		bean.setOverlayExposure(BigDecimal.valueOf(100000.00));
		bean.setActualAllocation(BigDecimal.valueOf(35480800.00));
		bean.setActualAllocationAdjusted(BigDecimal.valueOf(35480850.00));
		bean.setTargetAllocation(BigDecimal.valueOf(13429302.62));
		bean.setTargetAllocationAdjusted(BigDecimal.valueOf(14298446.82));
		this.bean2 = bean;

		bean = new ProductOverlayAssetClass();
		ac = new InvestmentAccountAssetClass();
		InvestmentTargetExposureAdjustment adjustment = new InvestmentTargetExposureAdjustment();
		adjustment.setTargetExposureAdjustmentType(InvestmentTargetExposureAdjustmentTypes.TFA);
		ac.setTargetExposureAdjustment(adjustment);
		bean.setAccountAssetClass(ac);
		bean.setOverlayRun(run);
		bean.setOverlayExposure(BigDecimal.ZERO);
		bean.setActualAllocation(BigDecimal.valueOf(126670.45));
		bean.setActualAllocationAdjusted(BigDecimal.valueOf(126670.45));
		bean.setTargetAllocation(BigDecimal.valueOf(1801623.64));
		bean.setTargetAllocationAdjusted(BigDecimal.valueOf(126670.45));
		this.bean3 = bean;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetPhysicalDeviationFromAdjustedTarget() {
		Assertions.assertEquals(BigDecimal.ZERO, this.bean1.getPhysicalDeviationFromAdjustedTarget());
		Assertions.assertEquals(BigDecimal.valueOf(21182403.18), this.bean2.getPhysicalDeviationFromAdjustedTarget());
		Assertions.assertEquals(BIG_DECIMAL_ZERO_SCALED, this.bean3.getPhysicalDeviationFromAdjustedTarget());
	}


	@Test
	public void testIsTargetFollowsActual() {
		Assertions.assertFalse(this.bean1.isTargetFollowsActual());
		Assertions.assertFalse(this.bean2.isTargetFollowsActual());
		Assertions.assertTrue(this.bean3.isTargetFollowsActual());
	}


	@Test
	public void testIsTargetAdjusted() {
		Assertions.assertFalse(this.bean1.isTargetAdjusted());
		Assertions.assertTrue(this.bean2.isTargetAdjusted());
		Assertions.assertTrue(this.bean3.isTargetAdjusted());
	}


	@Test
	public void testGetTotalExposure() {
		Assertions.assertEquals(BIG_DECIMAL_ZERO_SCALED, this.bean1.getTotalExposure());
		Assertions.assertEquals(MathUtils.round(BigDecimal.valueOf(35580850.00), 2), this.bean2.getTotalExposure());
		Assertions.assertEquals(BigDecimal.valueOf(126670.45), this.bean3.getTotalExposure());
	}


	@Test
	public void testGetTotalExposureDeviationFromAdjustedTarget() {
		Assertions.assertEquals(BIG_DECIMAL_ZERO_SCALED, this.bean1.getTotalExposureDeviationFromAdjustedTarget());
		Assertions.assertEquals(BigDecimal.valueOf(21282403.18), this.bean2.getTotalExposureDeviationFromAdjustedTarget());
		Assertions.assertEquals(BIG_DECIMAL_ZERO_SCALED, this.bean3.getTotalExposureDeviationFromAdjustedTarget());
	}


	@Test
	public void testGetPortfolioTotalValue() {
		Assertions.assertEquals(BigDecimal.valueOf(36032472.81), this.bean1.getPortfolioTotalValue());
		Assertions.assertEquals(BigDecimal.valueOf(36032472.81), this.bean2.getPortfolioTotalValue());
		Assertions.assertEquals(BigDecimal.valueOf(36032472.81), this.bean3.getPortfolioTotalValue());
	}


	@Test
	public void testGetOverlayExposurePercent() {
		Assertions.assertEquals(BIG_DECIMAL_ZERO_SCALED, this.bean1.getOverlayExposurePercent());
		Assertions.assertEquals(BigDecimal.valueOf(0.28), this.bean2.getOverlayExposurePercent());
		Assertions.assertEquals(BIG_DECIMAL_ZERO_SCALED, this.bean3.getOverlayExposurePercent());
	}
}
