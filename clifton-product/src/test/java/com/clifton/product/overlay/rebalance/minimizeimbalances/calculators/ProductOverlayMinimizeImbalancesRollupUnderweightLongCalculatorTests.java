package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * The <code>ProductOverlayMinimizeImbalancesRollupUnderweightCalculatorTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration("ProductOverlayMinimizeImbalancesCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ProductOverlayMinimizeImbalancesRollupUnderweightLongCalculatorTests extends BaseProductOverlayMinimizeImbalancesCalculatorTests {

	@Override
	public ProductOverlayMinimizeImbalancesCalculator getCalculator() {
		ProductOverlayMinimizeImbalancesRollupUnderweightCalculator calculator = new ProductOverlayMinimizeImbalancesRollupUnderweightCalculator();
		calculator.setShortTargetsAllowed(false);
		calculator.setExpandShortPositionsAllowed(false);
		calculator.setAttributionRebalancing(true);
		return calculator;
	}


	@Test
	public void test_1() {
		// 078300: Boeing 10/16/2012
		// NOTE: THIS IS GOING TO REBALANCE BECAUSE THE 2 ASSET CLASSES WITH REPLICATIONS HAVE A ZERO TARGET.  IT'S POSSIBLE MANAGER CASH IS THE CORRECT BUCKET.  NEED TO REVIEW/FIND A NEWER EXAMPLE
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1463, "Cash").withAssetClassPercentage(3).withAdjustedTarget(34687811).withPhysicalExposure(40880788).withOverlayExposure(-39220148).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 40880787.64));

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1464, "Health").asRollup());
		addAssetClassToMap(config, "Health", ProductOverlayAssetClassBuilder.newAssetClass(1500, "Wellington Global Health").withAssetClassPercentage(19.4).withAdjustedTarget(224314511).withPhysicalExposure(233924391));
		addAssetClassToMap(config, "Health", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1501, "Clifton Healthcare Overlay").withAssetClassPercentage(0.0).withOverlayExposure(3639120));

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1465, "Technology").asRollup());
		addAssetClassToMap(config, "Technology", ProductOverlayAssetClassBuilder.newAssetClass(1600, "RCM Technology").withAssetClassPercentage(48.5).withAdjustedTarget(560786277).withPhysicalExposure(553637812));
		addAssetClassToMap(config, "Technology", ProductOverlayAssetClassBuilder.newAssetClass(1601, "Wellington Global Tech").withAssetClassPercentage(29.1).withAdjustedTarget(336471766.00).withPhysicalExposure(336471766.00));
		addAssetClassToMap(config, "Technology", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1602, "Clifton Technology Overlay").withAssetClassPercentage(0.0).withOverlayExposure(35581029.00));

		processCalculation(config);


		// Rollup Values are added up later, so should be zero
		validateRollupAssetClassAllocation(config, "Health");
		validateRollupAssetClassAllocation(config, "Technology");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Wellington Global Health");
		validateAssetClassAllocation(config, "Clifton Healthcare Overlay", 0.00, 0.00, 0.00, 0.00, 0.00, 3639120.00);
		validateZeroAssetClassAllocation(config, "RCM Technology");
		validateZeroAssetClassAllocation(config, "Wellington Global Tech");
		validateAssetClassAllocation(config, "Clifton Technology Overlay", 0.00, 0.00, 0.00, 0.00, 0.00, 37241667.64);
	}


	@Test
	public void test_2() {
		// 108000: Cargill 10/15 Cash Buffer of 500,000
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(null, null, -500000.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2135, "Equity").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(169, "Alternative Assets").withAssetClassPercentage(20).withAdjustedTarget(439347919.00).withPhysicalExposure(439347919.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(170, "Cash").withAssetClassPercentage(0).withPhysicalExposure(84537669.53).withOverlayExposure(-82636613.11).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 74322001.87));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(172, "Fixed Income").withAssetClassPercentage(20).withAdjustedTarget(439347919.00).withPhysicalExposure(459948778.42));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(173, "Hedge Funds").withAssetClassPercentage(20).withAdjustedTarget(439347919.00).withPhysicalExposure(439347919.00));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(171, "Domestic Equity").withAssetClassPercentage(21).withAdjustedTarget(461315314.94).withPhysicalExposure(451797568.47).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 10215667.66));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(174, "International Equity").withAssetClassPercentage(19).withAdjustedTarget(417380523.05).withPhysicalExposure(321759740.57).withOverlayExposure(82636613.11));


		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Equity");
		validateZeroAssetClassAllocation(config, "Alternative Assets");
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 24607333.96, 0.00, 0.00, 0.00, -24607333.96);
		validateZeroAssetClassAllocation(config, "Hedge Funds");
		validateAssetClassAllocation(config, "Domestic Equity", 10215667.66, 25837700.65, 0.00, 0.00, 0.00, -36053368.31);
		validateAssetClassAllocation(config, "International Equity", 0.00, 23376967.26, 0.00, 0.00, 0.00, 60660702.27);
		// 108000: Cargill 9/26 CashBuffer of 500,000
		config = setupProductOverlayRunConfig(null, null, -500000.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2135, "Equity").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(169, "Alternative Assets").withAssetClassPercentage(20).withAdjustedTarget(436988877.24).withPhysicalExposure(436988877.24));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(170, "Cash").withAssetClassPercentage(0).withPhysicalExposure(87191022.59).withOverlayExposure(-86386035.22).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 77080148.68));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(172, "Fixed Income").withAssetClassPercentage(20).withAdjustedTarget(436988877.24).withPhysicalExposure(453410393.11));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(173, "Hedge Funds").withAssetClassPercentage(20).withAdjustedTarget(436988877.24).withPhysicalExposure(436988877.24));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(171, "Domestic Equity").withAssetClassPercentage(21).withAdjustedTarget(458838321.09).withPhysicalExposure(449161733.95).withOverlayExposure(1434611.15).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 10110873.91));
		addAssetClassToMap(config, "Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(174, "International Equity").withAssetClassPercentage(19).withAdjustedTarget(415139433.37).withPhysicalExposure(321203482.04).withOverlayExposure(84951424.07));

		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Equity");
		validateZeroAssetClassAllocation(config, "Alternative Assets");
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 25526716.23, 0.00, 0.00, 0.00, -25526716.23);
		validateZeroAssetClassAllocation(config, "Hedge Funds");
		validateAssetClassAllocation(config, "Domestic Equity", 10110873.91, 26803052.04, 0.00, 0.00, 0.00, -35479314.80);
		validateAssetClassAllocation(config, "International Equity", 0.00, 24250380.42, 0.00, 0.00, 0.00, 61006031.02);
	}


	@Test
	public void test_TradingBandsAndCashAdjustment_NoTrades() {
		// 108000: Cargill 01/24/2018 Cash Adjustment -500,000 and Min Effective Cash: 0, Max Effective Cash: 500,000
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(0.0, 500000.00, -500000.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2568, "Total Equity").asRollup().withAssetClassPercentage(32.00000).withCashPercentage(0.000000000000000).withRebalanceTriggers(-85800268.55, 85800268.55));

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(169, "Alternative Assets").withAssetClassPercentage(12.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(343201074.20).withPhysicalExposure(343201074.20).withOverlayExposure(0.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(170, "Cash").withAssetClassPercentage(0.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(0.00).withPhysicalExposure(108891817.48).withOverlayExposure(-108615818.49).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 106401226.54));
		addAssetClassToMap(config, "Total Equity", ProductOverlayAssetClassBuilder.newAssetClass(171, "Domestic Equity").withAssetClassPercentage(12.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(343201074.20).withPhysicalExposure(614597990.85).withOverlayExposure(0.00).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 2490590.94));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(172, "Fixed Income").withAssetClassPercentage(44.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(1258403938.73).withPhysicalExposure(1013159826.52).withOverlayExposure(108615818.49).withRebalanceTriggers(-85800268.55, 85800268.55));
		addAssetClassToMap(config, "Total Equity", ProductOverlayAssetClassBuilder.newAssetClass(2491, "Global Equity").withAssetClassPercentage(9.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(257400805.65).withPhysicalExposure(0.00).withOverlayExposure(0.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(173, "Hedge Funds").withAssetClassPercentage(12.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(343201074.20).withPhysicalExposure(343201074.20).withOverlayExposure(0.00));
		addAssetClassToMap(config, "Total Equity", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(174, "International Equity").withAssetClassPercentage(11.00000).withCashPercentage(0.000000000000000).withAdjustedTarget(314600984.68).withPhysicalExposure(436957168.41).withOverlayExposure(0.00));


		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Alternative Assets");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Domestic Equity");
		validateAssetClassAllocation(config, "Fixed Income", 1992472.75, 85120981.23, 0.00, 0.00, 0.00, 21502364.51);
		validateZeroAssetClassAllocation(config, "Global Equity");
		validateZeroAssetClassAllocation(config, "Hedge Funds");
		validateAssetClassAllocation(config, "International Equity", 498118.19, 21280245.31, 0.00, 0.00, 0.00, -21778363.50);
		validateRollupAssetClassAllocation(config, "Total Equity");
	}
}
