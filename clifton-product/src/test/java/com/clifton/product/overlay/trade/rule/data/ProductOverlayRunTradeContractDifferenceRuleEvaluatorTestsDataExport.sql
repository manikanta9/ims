-- Common Table Expressions
-- Select Trade Workflow system condition entries nested deeply enough to satisfy all existing cases
WITH ResultSystemConditionEntry AS (SELECT DISTINCT SystemConditionEntryID
									FROM (SELECT sce1.SystemConditionEntryID SystemConditionEntryID1,
												 sce2.SystemConditionEntryID SystemConditionEntryID2,
												 sce3.SystemConditionEntryID SystemConditionEntryID3,
												 sce4.SystemConditionEntryID SystemConditionEntryID4
										  FROM SystemCondition sc1
											   INNER JOIN WorkflowTransition t ON sc1.SystemConditionID = t.SystemConditionID
											   INNER JOIN WorkflowState s ON t.EndWorkflowStateID = s.WorkflowStateID
											   INNER JOIN Workflow w ON s.WorkflowID = w.WorkflowID
											   INNER JOIN SystemConditionEntry sce1 ON sc1.SystemConditionEntryID = sce1.SystemConditionEntryID
											   LEFT JOIN SystemConditionEntry sce2 ON sce2.ParentSystemConditionEntryID = sce1.SystemConditionEntryID
											   LEFT JOIN SystemConditionEntry sce3 ON sce3.ParentSystemConditionEntryID = sce2.SystemConditionEntryID
											   LEFT JOIN SystemConditionEntry sce4 ON sce4.ParentSystemConditionEntryID = sce3.SystemConditionEntryID
										  WHERE w.WorkflowName = 'Trade Workflow') AllIdTable UNPIVOT (SystemConditionEntryID FOR IdColumn IN (SystemConditionEntryID1, SystemConditionEntryID2, SystemConditionEntryID3, SystemConditionEntryID4)) ResultSystemConditionEntry)
 -- Exported data queries
SELECT 'CalendarDay' AS entityTableName, CalendarDayID AS entityId FROM CalendarDay WHERE CalendarYearID = 2019
UNION
SELECT 'SystemColumn' AS entityTableName, SystemColumnID AS entityId FROM SystemColumn sc
	INNER JOIN SystemColumnGroup scg ON scg.SystemColumnGroupID = sc.SystemColumnGroupID
	INNER JOIN SystemTable st ON st.SystemTableID = sc.SystemTableID
WHERE st.TableName = 'InvestmentAccount' AND (scg.ColumnGroupName = 'Portfolio Setup' OR scg.ColumnGroupName = 'PIOS Processing')
UNION
SELECT 'TradeType', TradeTypeID FROM TradeType
UNION
SELECT 'TradeOpenCloseType' AS entityTableName, TradeOpenCloseTypeID AS entityId FROM TradeOpenCloseType
UNION
SELECT 'TradeDestinationMapping', TradeDestinationMappingID FROM TradeDestinationMapping
UNION
SELECT 'TradeGroupType', TradeGroupTypeID FROM TradeGroupType WHERE GroupTypeName = 'Portfolio Run'
UNION
SELECT 'RuleAssignment', RuleAssignmentID FROM RuleAssignment ra INNER JOIN RuleDefinition rd ON ra.RuleDefinitionID = rd.RuleDefinitionID WHERE rd.DefinitionName IN ('Portfolio Run Trade Contract Difference (Same Direction)','Portfolio Run Trade Contract Difference (Opposite Direction)') AND ra.AdditionalScopeFKFieldID IS NULL AND ra.EntityFKFieldID IS NULL
UNION
SELECT 'SystemTable', SystemTableID FROM SystemTable WHERE TableName = 'ProductOverlayAssetClassReplication'
UNION
SELECT 'InvestmentAccountRelationshipPurpose', InvestmentAccountRelationshipPurposeID FROM InvestmentAccountRelationshipPurpose
UNION
SELECT 'WorkflowState' AS enttityTableName, WorkflowStateID AS entityId FROM WorkflowState WHERE WorkflowID IN (21, 6) -- portfolio and trade
UNION
SELECT 'WorkflowTransition', WorkflowTransitionID FROM WorkflowTransition t INNER JOIN WorkflowState s ON t.EndWorkflowStateID = s.WorkflowStateID INNER JOIN Workflow w ON s.WorkflowID = w.WorkflowID WHERE w.WorkflowName = 'Trade Workflow'
UNION
SELECT 'WorkflowTransitionAction', WorkflowTransitionActionID FROM WorkflowTransitionAction a INNER JOIN WorkflowTransition t ON a.WorkflowTransitionID = t.WorkflowTransitionID INNER JOIN WorkflowState s ON t.EndWorkflowStateID = s.WorkflowStateID INNER JOIN Workflow w ON s.WorkflowID = w.WorkflowID WHERE w.WorkflowName = 'Trade Workflow'
UNION
SELECT 'WorkflowAssignment', WorkflowAssignmentID FROM WorkflowAssignment a INNER JOIN SystemTable t ON a.SystemTableID = t.SystemTableID WHERE t.TableName = 'Trade'
UNION
-- CONCORDIA 10/17 MAIN RUN
SELECT 'ProductOverlayAssetClassReplication' AS entityTableName, ProductOverlayAssetClassReplicationID AS entityId FROM ProductOverlayAssetClassReplication r INNER JOIN ProductOverlayAssetClass a ON r.ProductOverlayAssetClassID = a.ProductOverlayAssetClassID WHERE a.ProductOverlayRunID = 1100330
UNION
-- CONCORDIA ACCOUNT RELATIONSHIPS
SELECT 'InvestmentAccountRelationship', InvestmentAccountRelationshipID FROM InvestmentAccountRelationship WHERE MainAccountID = 3696 OR RelatedAccountID = 3696
UNION
-- CONCORDIA TRANSACTIONS UNTIL 10/17
SELECT 'AccountingTransaction', AccountingTransactionID FROM AccountingTransaction t WHERE t.ClientInvestmentAccountID = 3696 AND t.TransactionDate <= '10/18/2019' AND t.TransactionDate >= '01/01/2019'
UNION
-- Future Fair Value Market Data for Securities included in the run
SELECT 'MarketDataValue', MarketDataValueID FROM MarketDataValue v INNER JOIN ProductOverlayAssetClassReplication r ON v.InvestmentSecurityID = r.InvestmentSecurityID INNER JOIN ProductOverlayAssetClass a ON r.ProductOverlayAssetClassID = a.ProductOverlayAssetClassID WHERE a.ProductOverlayRunID = 1100330 AND MeasureDate = '10/16/2019'
UNION
-- SEE BEFORE TEST METHOD THAT WILL PRINT OUT THE LIST OF BEANS THAT WE WILL NEED PROPERTIES FOR
SELECT 'SystemBeanProperty', SystemBeanPropertyID FROM SystemBeanProperty WHERE SystemBeanID IN (365, 264, 69, 1716, 118, 3333, 3320, 1218, 1236, 1239, 1238, 1481, 61, 62, 53, 54, 3319, 3334, 18, 56, 236, 1960, 57, 411, 336, 126, 1247, 1248, 1041, 1219, 112, 72, 462, 3322, 3321)
UNION
-- Detached SystemConditionEntry generations
SELECT 'SystemConditionEntry', rsce.SystemConditionEntryID
FROM ResultSystemConditionEntry rsce
-- System Bean Properties for SystemConditionEntry entities
UNION
SELECT 'SystemBeanProperty', sbp.SystemBeanPropertyID
FROM ResultSystemConditionEntry rsce
	 INNER JOIN SystemConditionEntry sce ON rsce.SystemConditionEntryID = sce.SystemConditionEntryID
	 INNER JOIN SystemBean sb ON sce.SystemBeanID = sb.SystemBeanID
	 INNER JOIN SystemBeanProperty sbp ON sb.SystemBeanID = sbp.SystemBeanID
UNION
-- Exchange rates to use during accounting balance retrieval
SELECT 'MarketDataExchangeRate', mder.MarketDataExchangeRateID
FROM ProductOverlayAssetClassReplication poacr
	 JOIN ProductOverlayAssetClass poac ON poacr.ProductOverlayAssetClassID = poac.ProductOverlayAssetClassID
	 JOIN InvestmentSecurity s ON poacr.InvestmentSecurityID = s.InvestmentSecurityID
	 JOIN InvestmentSecurity s2 ON s.UnderlyingSecurityID = s2.InvestmentSecurityID
	 JOIN MarketDataExchangeRate mder ON (s2.InvestmentSecurityID = mder.FromCurrencySecurityID OR s2.InvestmentSecurityID = mder.ToCurrencySecurityID)
WHERE poac.ProductOverlayRunID = 1100330
  AND s2.IsCurrency = 1
  AND mder.RateDate = '10/16/2019'
UNION
SELECT 'InvestmentAccountGroup', InvestmentaccountGroupID FROM InvestmentAccountGroup WHERE InvestmentAccountGroupID IN (406);

