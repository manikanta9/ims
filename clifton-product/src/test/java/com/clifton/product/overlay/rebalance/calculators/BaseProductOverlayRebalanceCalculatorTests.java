package com.clifton.product.overlay.rebalance.calculators;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.rebalance.ProductOverlayRebalance;
import com.clifton.product.util.ProductUtilService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;


public abstract class BaseProductOverlayRebalanceCalculatorTests {

	@Resource
	private ProductOverlayRebalanceCalculatorLocator productOverlayRebalanceCalculatorLocator;

	@Resource
	private ProductUtilService productUtilService;

	@Resource
	private UpdatableDAO<InvestmentAssetClass> investmentAssetClassDAO;


	@Resource
	private SystemColumnValueHandler systemColumnValueHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract RebalanceCalculationTypes getRebalanceCalculationType();


	protected ProductOverlayRebalanceCalculator getCalculator() {
		return this.productOverlayRebalanceCalculatorLocator.locate(getRebalanceCalculationType());
	}


	protected void calculateNewRebalanceCash(ProductOverlayRebalance rebalance) {
		ProductOverlayRebalanceCalculator calculator = getCalculator();
		BeanUtils.setPropertyValue(calculator, "productUtilService", this.productUtilService, true);
		calculator.calculateNewRebalanceCash(rebalance);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@BeforeEach
	protected void cleanUpMockitoCalls() {
		// Instead of using dirties context for each test which slows all these tests down, just reset the mock
		// this way mocked calls from one test don't interfere with mocks from other tests
		Mockito.reset(this.systemColumnValueHandler);
	}


	///////////////////////////////////////////////////////////////
	/////////    		Helper Methods            /////////////////
	///////////////////////////////////////////////////////////////


	protected ProductOverlayRebalance createProductOverlayRebalance() {
		ProductOverlayRebalance rebalance = new ProductOverlayRebalance();
		rebalance.setRebalanceAssetClassList(new ArrayList<>());
		rebalance.setRollupAssetClassList(new ArrayList<>());
		return rebalance;
	}


	protected void addAssetClassToList(ProductOverlayRebalance rebalance, Map<String, BigDecimal> resultMap, String assetClassName, double cashPercentage, double beforeCashValue, double afterCashValue) {
		InvestmentAssetClass c = new InvestmentAssetClass();
		c.setName(assetClassName);
		InvestmentAccountAssetClass ac = new InvestmentAccountAssetClass();
		ac.setAssetClass(c);
		ac.setCashPercentage(BigDecimal.valueOf(cashPercentage));

		ProductOverlayAssetClass oac = new ProductOverlayAssetClass();
		oac.setAccountAssetClass(ac);
		oac.setRebalanceCashAdjusted(BigDecimal.valueOf(beforeCashValue));
		// Set original rebalance cash to a different number to ensure using adjusted values in tests
		if (!MathUtils.isNullOrZero(oac.getRebalanceCashAdjusted())) {
			oac.setRebalanceCash(MathUtils.subtract(oac.getRebalanceAttributionCash(), BigDecimal.valueOf(2500)));
		}
		rebalance.getRebalanceAssetClassList().add(oac);

		resultMap.put(assetClassName, BigDecimal.valueOf(afterCashValue));
	}


	protected void addRollupAssetClassToList(ProductOverlayRebalance rebalance, String parentAssetClassName, String assetClassName, double assetClassTargetPercent, double target, double actual,
	                                         double managerCash, double fundCash, double transitionCash, double rebalanceCash, double rebalanceAttributionCash, double clientDirectedCash) {
		ProductOverlayAssetClass oac = setupAssetClass(assetClassName, false, target, actual, managerCash, fundCash, transitionCash, rebalanceCash, rebalanceAttributionCash, clientDirectedCash);
		oac.getAccountAssetClass().setAssetClassPercentage(BigDecimal.valueOf(assetClassTargetPercent));
		oac.setRollupAssetClass(true);
		oac.getAccountAssetClass().setRollupAssetClass(true);

		if (!StringUtils.isEmpty(parentAssetClassName)) {
			ProductOverlayAssetClass parentOverlayAssetClass = CollectionUtils.getOnlyElementStrict(
					BeanUtils.filter(rebalance.getRollupAssetClassList(), overlayAssetClass -> overlayAssetClass.getAccountAssetClass().getAssetClass().getName(), parentAssetClassName));
			ValidationUtils.assertNotNull(parentOverlayAssetClass, "Missing Parent Asset Class in Rollup List: " + parentAssetClassName);
			oac.setParent(parentOverlayAssetClass);
			oac.getAccountAssetClass().setParent(parentOverlayAssetClass.getAccountAssetClass());
		}
		rebalance.getRollupAssetClassList().add(oac);
	}


	protected void addChildAssetClassToList(ProductOverlayRebalance rebalance, Map<String, BigDecimal> resultMap, String parentAssetClassName, String assetClassName, boolean hasReplication,
	                                        double assetClassTargetPercent, double target, double actual, double managerCash, double fundCash, double transitionCash, double rebalanceCash, double rebalanceAttributionCash,
	                                        double clientDirectedCash, double expectedRebalanceCashValue) {
		ProductOverlayAssetClass oac = setupAssetClass(assetClassName, hasReplication, target, actual, managerCash, fundCash, transitionCash, rebalanceCash, rebalanceAttributionCash,
				clientDirectedCash);
		oac.getAccountAssetClass().setAssetClassPercentage(BigDecimal.valueOf(assetClassTargetPercent));
		oac.setRollupAssetClass(false);
		if (!StringUtils.isEmpty(parentAssetClassName)) {
			ProductOverlayAssetClass parentOverlayAssetClass = CollectionUtils.getOnlyElementStrict(
					BeanUtils.filter(rebalance.getRollupAssetClassList(), overlayAssetClass -> overlayAssetClass.getAccountAssetClass().getAssetClass().getName(), parentAssetClassName));
			ValidationUtils.assertNotNull(parentOverlayAssetClass, "Missing Parent Asset Class in Rollup List: " + parentAssetClassName);
			oac.setParent(parentOverlayAssetClass);
			oac.getAccountAssetClass().setParent(parentOverlayAssetClass.getAccountAssetClass());
		}
		rebalance.getRebalanceAssetClassList().add(oac);
		resultMap.put(assetClassName, BigDecimal.valueOf(expectedRebalanceCashValue));
	}


	protected void addAssetClassToList(ProductOverlayRebalance rebalance, Map<String, BigDecimal> resultMap, String assetClassName, boolean hasReplication, double target, double actual,
	                                   double managerCash, double fundCash, double transitionCash, double rebalanceCash, double rebalanceAttributionCash, double clientDirectedCash, double expectedRebalanceCashValue) {
		ProductOverlayAssetClass oac = setupAssetClass(assetClassName, hasReplication, target, actual, managerCash, fundCash, transitionCash, rebalanceCash, rebalanceAttributionCash,
				clientDirectedCash);

		rebalance.getRebalanceAssetClassList().add(oac);
		resultMap.put(assetClassName, BigDecimal.valueOf(expectedRebalanceCashValue));
	}


	private ProductOverlayAssetClass setupAssetClass(String assetClassName, boolean hasReplication, double target, double actual, double managerCash, double fundCash, double transitionCash,
	                                                 double rebalanceCash, double rebalanceAttributionCash, double clientDirectedCash) {
		InvestmentAssetClass c = getInvestmentAssetClass(assetClassName);
		InvestmentAccountAssetClass ac = new InvestmentAccountAssetClass();
		ac.setId(c.getId().intValue());
		ac.setAssetClass(c);

		ProductOverlayAssetClass oac = new ProductOverlayAssetClass();
		oac.setAccountAssetClass(ac);

		oac.setTargetAllocationAdjusted(BigDecimal.valueOf(target));
		oac.setActualAllocationAdjusted(BigDecimal.valueOf(actual));

		oac.setManagerCash(BigDecimal.valueOf(managerCash));
		oac.setFundCash(BigDecimal.valueOf(fundCash));
		oac.setTransitionCash(BigDecimal.valueOf(transitionCash));
		oac.setClientDirectedCash(BigDecimal.valueOf(clientDirectedCash));
		oac.setRebalanceAttributionCash(BigDecimal.valueOf(rebalanceAttributionCash));

		oac.setRebalanceCash(BigDecimal.valueOf(rebalanceCash));
		oac.setRebalanceCashAdjusted(BigDecimal.valueOf(rebalanceCash));

		// Set original rebalance cash to a different number to ensure using adjusted values in tests
		if (!MathUtils.isNullOrZero(oac.getRebalanceCashAdjusted())) {
			oac.setRebalanceCash(MathUtils.subtract(oac.getRebalanceAttributionCash(), BigDecimal.valueOf(2500)));
		}

		if (hasReplication) {
			oac.setPrimaryReplication(new InvestmentReplication());
		}
		return oac;
	}


	/**
	 * We'll actually save real asset classes so we can use the id at the account asset class level
	 * to track for processing (so equals checks works)
	 */
	private InvestmentAssetClass getInvestmentAssetClass(String name) {
		InvestmentAssetClass ac = this.investmentAssetClassDAO.findOneByField("name", name);
		if (ac == null) {
			ac = new InvestmentAssetClass();
			ac.setName(name);
			if ("Cash".equals(name)) {
				ac.setMainCash(true);
			}
			ac.setMaster(true);
			this.investmentAssetClassDAO.save(ac);
		}
		return ac;
	}


	protected void validateResults(ProductOverlayRebalance rebalance, Map<String, BigDecimal> resultMap) {
		BigDecimal sum = CoreMathUtils.sumProperty(rebalance.getRebalanceAssetClassList(), ProductOverlayAssetClass::getNewRebalanceCash);
		ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(MathUtils.abs(sum), BigDecimal.ONE),
				"New Rebalance Cash Total should be nearly zero (threshold of 1), but is [" + CoreMathUtils.formatNumberMoney(sum) + "].");
		for (ProductOverlayAssetClass ac : rebalance.getRebalanceAssetClassList()) {
			ValidationUtils.assertTrue(
					MathUtils.isEqual(MathUtils.round(ac.getNewRebalanceCash(), 2), MathUtils.round(resultMap.get(ac.getAccountAssetClass().getAssetClass().getName()), 2)),
					ac.getAccountAssetClass().getAssetClass().getName() + ": Expected New Rebalance Cash to be "
							+ CoreMathUtils.formatNumberMoney(resultMap.get(ac.getAccountAssetClass().getAssetClass().getName())) + ", but was " + CoreMathUtils.formatNumberMoney(ac.getNewRebalanceCash()));
		}
	}
}
