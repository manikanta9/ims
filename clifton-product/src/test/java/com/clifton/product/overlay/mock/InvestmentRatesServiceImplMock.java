package com.clifton.product.overlay.mock;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.rates.InvestmentInterestRateIndexGroupIndex;
import com.clifton.investment.rates.InvestmentRatesServiceImpl;
import com.clifton.investment.rates.search.InvestmentInterestRateIndexGroupIndexSearchForm;
import com.clifton.core.util.MathUtils;

import java.util.ArrayList;
import java.util.List;


public class InvestmentRatesServiceImplMock extends InvestmentRatesServiceImpl {

	@Override
	public List<InvestmentInterestRateIndexGroupIndex> getInvestmentInterestRateIndexGroupIndexList(InvestmentInterestRateIndexGroupIndexSearchForm searchForm) {

		List<InvestmentInterestRateIndexGroupIndex> list = getInvestmentInterestRateIndexGroupIndexDAO().findAll();
		if (searchForm.getGroupId() != null) {
			list = BeanUtils.filter(list, investmentInterestRateIndexGroupIndex -> investmentInterestRateIndexGroupIndex.getReferenceOne().getId(), searchForm.getGroupId());
		}
		else if (searchForm.getGroupName() != null) {
			list = BeanUtils.filter(list, investmentInterestRateIndexGroupIndex -> investmentInterestRateIndexGroupIndex.getReferenceOne().getName(), searchForm.getGroupName());
		}

		if (searchForm.getCurrencyId() != null) {
			list = BeanUtils.filter(list, investmentInterestRateIndexGroupIndex -> investmentInterestRateIndexGroupIndex.getReferenceTwo().getCurrencyDenomination() == null ? null : investmentInterestRateIndexGroupIndex.getReferenceTwo().getCurrencyDenomination().getId(), searchForm.getCurrencyId());
		}
		list = BeanUtils.sortWithFunction(list, interestRateIndexGroupIndex -> interestRateIndexGroupIndex.getReferenceTwo().getNumberOfDays(), (searchForm.getMaxNumberOfDays() == null));

		if (searchForm.getMaxNumberOfDays() != null || searchForm.getMinNumberOfDays() != null) {
			List<InvestmentInterestRateIndexGroupIndex> filteredList = new ArrayList<>();
			for (InvestmentInterestRateIndexGroupIndex bean : CollectionUtils.getIterable(list)) {
				if (searchForm.getMaxNumberOfDays() != null) {
					if (MathUtils.compare(bean.getReferenceTwo().getNumberOfDays(), searchForm.getMaxNumberOfDays()) > 0) {
						continue;
					}
				}
				if (searchForm.getMinNumberOfDays() != null) {
					if (MathUtils.compare(bean.getReferenceTwo().getNumberOfDays(), searchForm.getMinNumberOfDays()) < 0) {
						continue;
					}
				}
				filteredList.add(bean);
			}
			return filteredList;
		}
		return list;
	}
}
