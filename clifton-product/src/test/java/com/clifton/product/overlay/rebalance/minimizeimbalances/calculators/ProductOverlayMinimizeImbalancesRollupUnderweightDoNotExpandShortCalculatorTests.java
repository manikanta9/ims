package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ContextConfiguration("ProductOverlayMinimizeImbalancesCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ProductOverlayMinimizeImbalancesRollupUnderweightDoNotExpandShortCalculatorTests extends BaseProductOverlayMinimizeImbalancesCalculatorTests {

	@Override
	public ProductOverlayMinimizeImbalancesCalculator getCalculator() {
		ProductOverlayMinimizeImbalancesRollupUnderweightCalculator calculator = new ProductOverlayMinimizeImbalancesRollupUnderweightCalculator();
		calculator.setShortTargetsAllowed(true);
		calculator.setExpandShortPositionsAllowed(false);
		calculator.setAttributionRebalancing(true);
		return calculator;
	}


	@Test
	public void test_ShortsUnderweight_Buy() {
		// 523500: Presb. on 10/23/2013 - No reason to increase shorts - so results would be the same as with the traditional Underwieght calculator
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(null, null, -37305685.00);
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2331, "Total Capital Appreciation").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2332, "Total Bonds").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2294, "Diversifying Hedge Funds").withAssetClassPercentage(15).withAdjustedTarget(95059136.47).withPhysicalExposure(95059136.47));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2295, "Public Inflation Sensitive - Commodities").withAssetClassPercentage(3).withAdjustedTarget(20810417.64).withPhysicalExposure(20135371.96).withOverlayExposure(624500.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2298, "Private Inflation Sensitive (PIS)").withAssetClassPercentage(0).withAdjustedTarget(429626.00).withPhysicalExposure(429626.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2301, "Cash").withAssetClassPercentage(4).withAdjustedTarget(37305685.06).withPhysicalExposure(33005965.21).withOverlayExposure(4780827.22).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 33005965.21));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2440, "Public Inflation Sensitive - TIPS").withAssetClassPercentage(2).withAdjustedTarget(13020976.15).withPhysicalExposure(13020976.15));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2291, "Public Equities").withAssetClassPercentage(53).withAdjustedTarget(342459617.52).withPhysicalExposure(349775653.40).withOverlayExposure(-8167084.63));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2292, "Private Equity").withAssetClassPercentage(0).withAdjustedTarget(33450330.00).withPhysicalExposure(33450330.00));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2293, "Market Sensitive Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(67195899.67).withPhysicalExposure(67195899.67));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2299, "Domestic Bonds").withAssetClassPercentage(10).withAdjustedTarget(73417467.45).withPhysicalExposure(71076197.10).withOverlayExposure(2761757.41));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClass(2300, "Global Bonds").withAssetClassPercentage(3).withAdjustedTarget(17691865.32).withPhysicalExposure(17691865.32));

		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Total Capital Appreciation");
		validateRollupAssetClassAllocation(config, "Total Bonds");
		validateZeroAssetClassAllocation(config, "Diversifying Hedge Funds");
		validateAssetClassAllocation(config, "Public Inflation Sensitive - Commodities", 0.00, 0.00, 0.00, 0.00, 0.00, 656183.42);
		validateZeroAssetClassAllocation(config, "Private Inflation Sensitive (PIS)");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Public Inflation Sensitive - TIPS");
		validateAssetClassAllocation(config, "Public Equities", 0.00, 0.00, 0.00, 0.00, 0.00, -7717660.62);
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Market Sensitive Hedge Funds");
		validateAssetClassAllocation(config, "Domestic Bonds", 0.00, 0.00, 0.00, 0.00, 0.00, 2761757.41);
		validateZeroAssetClassAllocation(config, "Global Bonds");
		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 33,005,965.21 Cash Adjustment: -37,305,685.00 Cash To Apply: -4,299,719.79");
	}


	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////


	@Test
	public void test_ShortsOverweight_RemainSame() {
		// 523500: Presb. on 10/23/2013 - No reason to increase shorts - so results would be the same as with the traditional Underwieght calculator
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(null, null, -39305685.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2331, "Total Capital Appreciation").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2332, "Total Bonds").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2294, "Diversifying Hedge Funds").withAssetClassPercentage(15).withAdjustedTarget(95059136.47).withPhysicalExposure(95059136.47));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2295, "Public Inflation Sensitive - Commodities").withAssetClassPercentage(3).withAdjustedTarget(20810417.64).withPhysicalExposure(20135371.96).withOverlayExposure(624500.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2298, "Private Inflation Sensitive (PIS)").withAssetClassPercentage(0).withAdjustedTarget(429626.00).withPhysicalExposure(429626.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2301, "Cash").withAssetClassPercentage(4).withAdjustedTarget(37305685.06).withPhysicalExposure(33005965.21).withOverlayExposure(4780827.22).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 33005965.21));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2440, "Public Inflation Sensitive - TIPS").withAssetClassPercentage(2).withAdjustedTarget(13020976.15).withPhysicalExposure(13020976.15));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2291, "Public Equities").withAssetClassPercentage(53).withAdjustedTarget(342459617.52).withPhysicalExposure(349775653.40).withOverlayExposure(-8167084.63));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2292, "Private Equity").withAssetClassPercentage(0).withAdjustedTarget(33450330.00).withPhysicalExposure(33450330.00));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2293, "Market Sensitive Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(67195899.67).withPhysicalExposure(67195899.67));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2299, "Domestic Bonds").withAssetClassPercentage(10).withAdjustedTarget(73417467.45).withPhysicalExposure(71076197.10).withOverlayExposure(2761757.41));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClass(2300, "Global Bonds").withAssetClassPercentage(3).withAdjustedTarget(17691865.32).withPhysicalExposure(17691865.32));


		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Total Capital Appreciation");
		validateRollupAssetClassAllocation(config, "Total Bonds");
		validateZeroAssetClassAllocation(config, "Diversifying Hedge Funds");
		validateAssetClassAllocation(config, "Public Inflation Sensitive - Commodities", 0.00, 0.00, 0.00, 0.00, 0.00, 461409.09);
		validateZeroAssetClassAllocation(config, "Private Inflation Sensitive (PIS)");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Public Inflation Sensitive - TIPS");
		validateAssetClassAllocation(config, "Public Equities", 0.00, 0.00, 0.00, 0.00, 0.00, -8167084.63);
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Market Sensitive Hedge Funds");
		validateAssetClassAllocation(config, "Domestic Bonds", 0.00, 0.00, 0.00, 0.00, 0.00, 1405955.75);
		validateZeroAssetClassAllocation(config, "Global Bonds");

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 33,005,965.21 Cash Adjustment: -39,305,685.00 Cash To Apply: -6,299,719.79");
	}


	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////


	@Test
	public void test_SellEquallyMinimizeImbalances() {
		// 523500: Presb. on 10/23/2013  
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(null, null, -36660685.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2331, "Total Capital Appreciation").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2332, "Total Bonds").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2294, "Diversifying Hedge Funds").withAssetClassPercentage(15).withAdjustedTarget(95059136.47).withPhysicalExposure(95059136.47));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2295, "Public Inflation Sensitive - Commodities").withAssetClassPercentage(3).withAdjustedTarget(20720417.64).withPhysicalExposure(20135371.96).withOverlayExposure(624500.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2298, "Private Inflation Sensitive (PIS)").withAssetClassPercentage(0).withAdjustedTarget(429626.00).withPhysicalExposure(429626.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2301, "Cash").withAssetClassPercentage(4).withAdjustedTarget(36660685.06).withPhysicalExposure(30005965.21).withOverlayExposure(4780827.22).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 30005965.21));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2440, "Public Inflation Sensitive - TIPS").withAssetClassPercentage(2).withAdjustedTarget(13020976.15).withPhysicalExposure(13020976.15));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2291, "Public Equities").withAssetClassPercentage(53).withAdjustedTarget(340584617.52).withPhysicalExposure(349775653.40).withOverlayExposure(-8167084.63));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2292, "Private Equity").withAssetClassPercentage(0).withAdjustedTarget(33450330.00).withPhysicalExposure(33450330.00));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2293, "Market Sensitive Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(67195899.67).withPhysicalExposure(67195899.67));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2299, "Domestic Bonds").withAssetClassPercentage(10).withAdjustedTarget(73027467.45).withPhysicalExposure(71076197.10).withOverlayExposure(2761757.41));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClass(2300, "Global Bonds").withAssetClassPercentage(3).withAdjustedTarget(17691865.32).withPhysicalExposure(17691865.32));


		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Total Capital Appreciation");
		validateRollupAssetClassAllocation(config, "Total Bonds");
		validateZeroAssetClassAllocation(config, "Diversifying Hedge Funds");
		validateAssetClassAllocation(config, "Public Inflation Sensitive - Commodities", 0.00, 0.00, 0.00, 0.00, 0.00, 394658.53);
		validateZeroAssetClassAllocation(config, "Private Inflation Sensitive (PIS)");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Public Inflation Sensitive - TIPS");
		validateAssetClassAllocation(config, "Public Equities", 0.00, 0.00, 0.00, 0.00, 0.00, -8167084.63);
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Market Sensitive Hedge Funds");
		validateAssetClassAllocation(config, "Domestic Bonds", 0.00, 0.00, 0.00, 0.00, 0.00, 1117706.31);
		validateZeroAssetClassAllocation(config, "Global Bonds");
		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 30,005,965.21 Cash Adjustment: -36,660,685.00 Cash To Apply: -6,654,719.79");
	}


	@Test
	public void test_SellWithRebalanceCash() {
		// 656100 on 6/18/2015
		// Incorrectly showing a small sell out of public equities even though domestic bonds is currently overweight
		// Issue with Do Not Expand Shorts Option: Has to do with Rebalance Cash originally at 1,337,749.82, and the minimize imbalances calculator has to remove cash, so it thinks it's already short and skips removing cash to prevent expanding shorts.
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();
		config.getAccountRebalanceConfig().setMinimizeImbalancesAdjustForCashTarget(true);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2489, "Total Capital Appreciation").asRollup().withAssetClassPercentage(63));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2488, "Cash").withAssetClassPercentage(3).withAdjustedTarget(8657610.82).withPhysicalExposure(14197523.09).withOverlayExposure(-5552064.47).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 14197523.09));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2483, "Diversifying Hedge Funds").withAssetClassPercentage(15).withAdjustedTarget(36511406.61).withPhysicalExposure(36511406.61));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2484, "Public Inflation Sensitive - Commodities").withAssetClassPercentage(3).withAdjustedTarget(7507691.31).withPhysicalExposure(7495999.03).withCashInBucket(ManagerCashTypes.REBALANCE, 193463.39).withRebalanceTriggers(-1126153.70, 1126153.70));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2485, "Public Inflation Sensitive - TIPS").withAssetClassPercentage(2).withAdjustedTarget(4297170.11).withPhysicalExposure(4297170.11));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2486, "Private Inflation Sensitive (PIS)").withAssetClassPercentage(0));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2487, "Domestic Bonds").withAssetClassPercentage(14).withAdjustedTarget(35035892.78).withPhysicalExposure(34391848.18).withOverlayExposure(669851.92).withCashInBucket(ManagerCashTypes.REBALANCE, 1337749.82).withRebalanceTriggers(-3503589.28, 3503589.28));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2481, "Private Equity").withAssetClassPercentage(0).withAdjustedTarget(28185940.60).withPhysicalExposure(28185940.60));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2482, "Market Sensitive Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(25579583.45).withPhysicalExposure(25579583.45));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2480, "Public Equities").withAssetClassPercentage(53).withAdjustedTarget(104481081.34).withPhysicalExposure(99596905.95).withOverlayExposure(4882212.55).withCashInBucket(ManagerCashTypes.REBALANCE, -1531213.21));

		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Total Capital Appreciation");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Diversifying Hedge Funds");
		validateAssetClassAllocation(config, "Public Inflation Sensitive - Commodities", 0.00, 237424.81, 0.00, 193463.39, 193463.39, -430888.20);
		validateZeroAssetClassAllocation(config, "Public Inflation Sensitive - TIPS");
		validateZeroAssetClassAllocation(config, "Private Inflation Sensitive (PIS)");
		validateAssetClassAllocation(config, "Domestic Bonds", 0.00, 1107982.45, 0.00, 1337749.82, 1337749.82, -1788032.55);
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Market Sensitive Hedge Funds");
		validateAssetClassAllocation(config, "Public Equities", 0.00, 4194505.00, 0.00, -1531213.21, -1531213.21, 2218920.76);
		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 14,197,523.09 Cash Adjustment: -8,657,610.82 Cash To Apply: 5,539,912.27");
	}
}
