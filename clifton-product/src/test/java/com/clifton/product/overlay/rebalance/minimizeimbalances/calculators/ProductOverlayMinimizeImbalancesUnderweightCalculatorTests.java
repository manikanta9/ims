package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ContextConfiguration("ProductOverlayMinimizeImbalancesCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ProductOverlayMinimizeImbalancesUnderweightCalculatorTests extends BaseProductOverlayMinimizeImbalancesCalculatorTests {

	@Override
	public ProductOverlayMinimizeImbalancesCalculator getCalculator() {
		ProductOverlayMinimizeImbalancesUnderweightCalculator calculator = new ProductOverlayMinimizeImbalancesUnderweightCalculator();
		calculator.setShortTargetsAllowed(true);
		calculator.setExpandShortPositionsAllowed(true);
		calculator.setAttributionRebalancing(true);
		return calculator;
	}


	@Test
	public void test_1() {
		// 530000: Retail Clerks Pension Trust on 10/01/2012
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1413, "Cash").withAssetClassPercentage(0).withPhysicalExposure(39552215.38).withOverlayExposure(-42215817.68).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 37574002.87));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1405, "Domestic Equity-SMID").withAssetClassPercentage(3).withAdjustedTarget(61003124.86).withPhysicalExposure(102540448.06).withOverlayExposure(-40880225.18));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1407, "Fixed Income High Yield").withAssetClassPercentage(5).withAdjustedTarget(107162166.22).withPhysicalExposure(107162166.22));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2044, "Fixed Income Alternative").withAssetClassPercentage(8).withAdjustedTarget(67991138.22).withPhysicalExposure(67991138.22));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2045, "Risk Parity").withAssetClassPercentage(6).withAdjustedTarget(136732248.4).withPhysicalExposure(136732248.4));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1411, "Real Estate").withAssetClassPercentage(6).withAdjustedTarget(129638176.75).withPhysicalExposure(129638176.75));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1410, "Private Equity").withAssetClassPercentage(10).withAdjustedTarget(89243948.66).withPhysicalExposure(89243948.66));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1409, "Infrastructure").withAssetClassPercentage(5).withAdjustedTarget(73232769.29).withPhysicalExposure(73232769.29));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1412, "Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(178579709.74).withPhysicalExposure(178579709.74));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1408, "Commodities").withAssetClassPercentage(6).withAdjustedTarget(122006249.73).withPhysicalExposure(120051150.5).withOverlayExposure(3333750));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1852, "International Equity").withAssetClassPercentage(6).withAdjustedTarget(122006249.73).withPhysicalExposure(104301267.88).withOverlayExposure(16962073.16));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1404, "Domestic Equity Large Cap").withAssetClassPercentage(13).withAdjustedTarget(264346874.41).withPhysicalExposure(242838125.31).withOverlayExposure(23202626.6).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1978212.51));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1406, "Fixed Income Core").withAssetClassPercentage(22).withAdjustedTarget(447356248.99).withPhysicalExposure(407435540.57).withOverlayExposure(39597593.1));

		processCalculation(config);

		validateAssetClassAllocation(config, "Domestic Equity-SMID", 0.00, 2254440.17, 0.00, 0.00, 0.00, -43646396.46);
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 1978212.51, 9769240.75, 0.00, 0.00, 0.00, 10391219.10);
		validateAssetClassAllocation(config, "International Equity", 0.00, 4508880.34, 0.00, 0.00, 0.00, 12453192.82);
		validateZeroAssetClassAllocation(config, "Fixed Income High Yield");
		validateAssetClassAllocation(config, "Fixed Income Core", 0.00, 16532561.26, 0.00, 0.00, 0.00, 23065031.84);
		validateZeroAssetClassAllocation(config, "Fixed Income Alternative");
		validateZeroAssetClassAllocation(config, "Risk Parity");
		validateZeroAssetClassAllocation(config, "Real Estate");
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Infrastructure");
		validateAssetClassAllocation(config, "Commodities", 0.00, 4508880.34, 0.00, 0.00, 0.00, -2263047.29);
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Hedge Funds");

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 39,552,215.38");
	}


	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testWithTradingBandsAndCashBuffer_NoTrades() {
		// 186000: Duke Energy 10/12
		// Trading Bands 0 - 6mm, cash buffer of 3mm
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(0.00, 6000000.00, -3000000.00);
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(281, "Cash").withAssetClassPercentage(0).withPhysicalExposure(50067561.30).withOverlayExposure(-49827958.85).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 30229304.07));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(282, "Domestic Equity").withAssetClassPercentage(29.5).withAdjustedTarget(1559710444.46).withPhysicalExposure(1575505422.26).withOverlayExposure(0.00).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 8511340.02));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(283, "Fixed Income").withAssetClassPercentage(32).withAdjustedTarget(1691889295.70).withPhysicalExposure(1791905410.49));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(284, "Global Equity").withAssetClassPercentage(10).withAdjustedTarget(528715404.90).withPhysicalExposure(508344592.83).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4904115.35));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(285, "Hedge Funds").withAssetClassPercentage(4).withAdjustedTarget(211486161.96).withPhysicalExposure(173851426.02));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(286, "International Equity").withAssetClassPercentage(16.5).withAdjustedTarget(872380418.09).withPhysicalExposure(805045395.39).withOverlayExposure(31706708.85).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 6422801.86));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(287, "Real Estate").withAssetClassPercentage(4).withAdjustedTarget(211486161.96).withPhysicalExposure(196207628.09));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1238, "Commodities").withAssetClassPercentage(2).withAdjustedTarget(105743080.98).withPhysicalExposure(84721069.57).withOverlayExposure(18121250.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1239, "Infrastructure").withAssetClassPercentage(2).withAdjustedTarget(105743080.98).withPhysicalExposure(101505543.08));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity", 21466788.43, 0.00, 0.00, 0.00, 0.00, -21466788.43);
		validateAssetClassAllocation(config, "Fixed Income", 14053367.77, 0.00, 0.00, 0.00, 0.00, -14053367.77);
		validateZeroAssetClassAllocation(config, "Global Equity");
		validateZeroAssetClassAllocation(config, "Hedge Funds");
		validateAssetClassAllocation(config, "International Equity", 13669069.62, 0.00, 0.00, 0.00, 0.00, 18037639.23);
		validateZeroAssetClassAllocation(config, "Real Estate");
		validateAssetClassAllocation(config, "Commodities", 878335.49, 0.00, 0.00, 0.00, 0.00, 17242914.51);
		validateZeroAssetClassAllocation(config, "Infrastructure");

		validateTradingBandsMessage(
				config,
				"Total Effective Cash: 50,067,561.30 Total Overlay Exposure: 49,827,958.85 Difference: 239,602.45 No Trading Range: [0.00] - [6,000,000.00] Difference is inside of no trading bands.  Asset Class Overlay Targets will be set to Overlay Exposure.");
		validateMinimizeImbalancesMessage(config, null);
	}


	@Test
	public void testWithTradingBandsAndCashBuffer_WithTrades() {
		// 186000: Duke Energy 10/10
		// Trading Bands 0 - 6mm, cash buffer of 3mm
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(0.00, 6000000.00, -3000000.00);
		// Cash Available for Minimize Imbalances: Manager Cash: 52792382.70
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(281, "Cash").withAssetClassPercentage(0).withPhysicalExposure(52792382.70).withOverlayExposure(-44761486.51).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 31728710.32));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(282, "Domestic Equity").withAssetClassPercentage(29.5).withAdjustedTarget(1552889788.49).withPhysicalExposure(1579510404.57).withOverlayExposure(0.00).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 7919719.86));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(283, "Fixed Income").withAssetClassPercentage(32).withAdjustedTarget(1684490618.03).withPhysicalExposure(1773396920.44));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(284, "Global Equity").withAssetClassPercentage(10).withAdjustedTarget(526403318.13).withPhysicalExposure(504580103.35).withOverlayExposure(225890.03).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 7378951.85));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(285, "Hedge Funds").withAssetClassPercentage(4).withAdjustedTarget(210561327.25).withPhysicalExposure(170961432.99));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(286, "International Equity").withAssetClassPercentage(16.5).withAdjustedTarget(868565474.92).withPhysicalExposure(802807528.62).withOverlayExposure(26429133.98).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 5765000.67));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(287, "Real Estate").withAssetClassPercentage(4).withAdjustedTarget(210561327.25).withPhysicalExposure(194031008.23));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1238, "Commodities").withAssetClassPercentage(2).withAdjustedTarget(105280663.63).withPhysicalExposure(84190563.75).withOverlayExposure(18106462.50));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1239, "Infrastructure").withAssetClassPercentage(2).withAdjustedTarget(105280663.63).withPhysicalExposure(101762836.68));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity", 17278644.58, 0.00, 0.00, 0.00, 0.00, -17278644.58);
		validateAssetClassAllocation(config, "Fixed Income", 10640242.69, 0.00, 0.00, 0.00, 0.00, -10640242.69);
		validateAssetClassAllocation(config, "Global Equity", 10284708.57, 0.00, 0.00, 0.00, 0.00, -9554037.52);
		validateZeroAssetClassAllocation(config, "Hedge Funds");
		validateAssetClassAllocation(config, "International Equity", 10923771.69, 0.00, 0.00, 0.00, 0.00, 20031477.46);
		validateZeroAssetClassAllocation(config, "Real Estate");
		validateAssetClassAllocation(config, "Commodities", 665015.17, 0.00, 0.00, 0.00, 0.00, 17441447.33);
		validateZeroAssetClassAllocation(config, "Infrastructure");

		validateTradingBandsMessage(
				config,
				"Total Effective Cash: 52,792,382.70 Total Overlay Exposure: 44,761,486.51 Difference: 8,030,896.19 No Trading Range: [0.00] - [6,000,000.00] Difference is outside of no trading bands.  Asset Class Overlay Targets will be generated.");
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 52,792,382.70 Cash Adjustment: -3,000,000.00 Cash To Apply: 49,792,382.70");
	}


	@Test
	public void testWithTradingBandsAndNoCashBuffer_NoTrades() {
		// 052000: Advocate 10/12
		// Trading Bands -20mm - 20mm
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(-20000000.00, 20000000.00, null);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1721, "US Large/Mid Cap").withAssetClassPercentage(10).withAdjustedTarget(421407377.42).withPhysicalExposure(402945375.59).withOverlayExposure(18675060.53));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1722, "US Non-Large Cap").withAssetClassPercentage(5).withAdjustedTarget(210703688.71).withPhysicalExposure(204002443.76).withOverlayExposure(5517461.73));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1723, "International Equity").withAssetClassPercentage(10).withAdjustedTarget(421407377.42).withPhysicalExposure(392735637.81).withOverlayExposure(26939351.68));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1724, "Emerging Markets").withAssetClassPercentage(5).withAdjustedTarget(210703688.71).withPhysicalExposure(193721122.68).withOverlayExposure(17002236.12));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1728, "Private Equity").withAssetClassPercentage(10).withAdjustedTarget(253271914.31).withPhysicalExposure(253271914.31));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1729, "Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(532550704.96).withPhysicalExposure(532550704.96));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1730, "Real Assets").withAssetClassPercentage(5).withAdjustedTarget(166342995.31).withPhysicalExposure(166342995.31));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1731, "Cash").withAssetClassPercentage(10).withAdjustedTarget(410145951.98).withPhysicalExposure(596583239.50).withOverlayExposure(-187172118.75).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 187172120));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1747, "Fixed Income").withAssetClassPercentage(35).withAdjustedTarget(1474925820.98).withPhysicalExposure(1359306085.88).withOverlayExposure(119038008.69));

		processCalculation(config);

		validateAssetClassAllocation(config, "US Large/Mid Cap", 0.00, 28795710.77, 0.00, 0.00, 0.00, -10120650.24);
		validateAssetClassAllocation(config, "US Non-Large Cap", 0.00, 14397855.38, 0.00, 0.00, 0.00, -8880393.65);
		validateAssetClassAllocation(config, "International Equity", 0.00, 28795710.77, 0.00, 0.00, 0.00, -1856359.09);
		validateAssetClassAllocation(config, "Emerging Markets", 0.00, 14397855.38, 0.00, 0.00, 0.00, 2604380.74);
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Hedge Funds");
		validateZeroAssetClassAllocation(config, "Real Assets");
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 100784987.69, 0.00, 0.00, 0.00, 18253021.00);
	}


	@Test
	public void testUnderweightWithTradingBandsAndNoCashBuffer_WithTrades() {
		// 052000: Advocate 10/11
		// Trading Bands -20mm - 20mm
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(-20000000.00, 20000000.00, null);
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1721, "US Large/Mid Cap").withAssetClassPercentage(10).withAdjustedTarget(421960225.22).withPhysicalExposure(403171079.03).withOverlayExposure(20531163.58));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1722, "US Non-Large Cap").withAssetClassPercentage(5).withAdjustedTarget(210980112.61).withPhysicalExposure(205472976.34).withOverlayExposure(7471755.88));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1723, "International Equity").withAssetClassPercentage(10).withAdjustedTarget(421960225.22).withPhysicalExposure(394991223.74).withOverlayExposure(31729346.17));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1724, "Emerging Markets").withAssetClassPercentage(5).withAdjustedTarget(210980112.61).withPhysicalExposure(193950480.95).withOverlayExposure(19978786.89));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1728, "Private Equity").withAssetClassPercentage(10).withAdjustedTarget(252629571.13).withPhysicalExposure(252629571.13));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1729, "Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(530504153.34).withPhysicalExposure(530504153.34));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1730, "Real Assets").withAssetClassPercentage(5).withAdjustedTarget(166342995.31).withPhysicalExposure(166342995.31));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1731, "Cash").withAssetClassPercentage(10).withAdjustedTarget(410246464.85).withPhysicalExposure(597505332.88).withOverlayExposure(-216028699.04).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 187258867.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1747, "Fixed Income").withAssetClassPercentage(35).withAdjustedTarget(1476860788.24).withPhysicalExposure(1357896835.81).withOverlayExposure(136317646.52));

		processCalculation(config);

		validateAssetClassAllocation(config, "US Large/Mid Cap", 0.00, 28809056.46, 0.00, 0.00, 0.00, -10019910.43);
		validateAssetClassAllocation(config, "US Non-Large Cap", 0.00, 14404528.23, 0.00, 0.00, 0.00, -8897392.04);
		validateAssetClassAllocation(config, "International Equity", 0.00, 28809056.46, 0.00, 0.00, 0.00, -1840055.14);
		validateAssetClassAllocation(config, "Emerging Markets", 0.00, 14404528.23, 0.00, 0.00, 0.00, 2625103.36);
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Hedge Funds");
		validateZeroAssetClassAllocation(config, "Real Assets");
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 100831697.62, 0.00, 0.00, 0.00, 18132254.25);
	}


	@Test
	public void test_UnderweightWithTradingBandsAndCashBufferLongAndShortTargets_MultipleAttributionRebalancing_NoTrades() {
		// Akzo Nobel 057000 10/15
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(0.00, 6000000.00, -3000000.00);
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1240, "Domestic Equity Large Cap").withAssetClassPercentage(11).withAdjustedTarget(107765372.76).withPhysicalExposure(90242675.93).withOverlayExposure(16425694.85).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 2364.69));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1241, "Domestic Equity Small Cap").withAssetClassPercentage(3).withAdjustedTarget(29390556.20).withPhysicalExposure(30157121.25).withOverlayExposure(-1740258.13));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1242, "International Equity").withAssetClassPercentage(16).withAdjustedTarget(156749633.11).withPhysicalExposure(132166635.56).withOverlayExposure(23164081.99).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 2210901.18));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1243, "Fixed Income").withAssetClassPercentage(48).withAdjustedTarget(469539021.34).withPhysicalExposure(524765716.99).withOverlayExposure(-51820378.38));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1245, "Cash").withAssetClassPercentage(6).withAdjustedTarget(58692377.67).withPhysicalExposure(44804811.35).withOverlayExposure(13970859.67).addMinimizeImbalancesCash(ManagerCashTypes.FUND, -16100832.19));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1914, "Alternative Assets").withAssetClassPercentage(16).withAdjustedTarget(156069333.39).withPhysicalExposure(156069333.39));

		processCalculation(config);

		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 2364.69, 0.00, 0.00, 0.00, 0.00, 16423330.16);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 0.00, 0.00, 0.00, 0.00, 0.00, -1740258.13);
		validateAssetClassAllocation(config, "International Equity", 2210901.18, 0.00, 0.00, 0.00, 0.00, 20953180.81);
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 0.00, 0.00, 0.00, 0.00, -51820378.38);
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Alternative Assets");
	}


	@Test
	public void test_UnderweightWithManualRebalanceBeforeBalanceDate() {
		// 530000 11/2
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();
		// Cash Available for Minimize Imbalances: Manager Cash 1321141.85, Fund Cash: 30863635.23 Total: 32,184,777.08

		setupInvestmentAccountManualRebalance(config, false);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1404, "Domestic Equity Large Cap").withAssetClassPercentage(13).withAdjustedTarget(258746430.79).withPhysicalExposure(238945109.60).withOverlayExposure(21926678.00).withCashInBucket(ManagerCashTypes.REBALANCE, 2125356.81).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1321141.85));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1405, "Domestic Equity-SMID").withAssetClassPercentage(3).withAdjustedTarget(59710714.80).withPhysicalExposure(99550803.63).withOverlayExposure(-40591919.58).withCashInBucket(ManagerCashTypes.REBALANCE, -751830.75));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1406, "Fixed Income Core").withAssetClassPercentage(22).withAdjustedTarget(437878575.20).withPhysicalExposure(407036123.67).withOverlayExposure(31967407.50).withCashInBucket(ManagerCashTypes.REBALANCE, 1124955.97));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1407, "Fixed Income High Yield").withAssetClassPercentage(5).withAdjustedTarget(108484099.46).withPhysicalExposure(108484099.46));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1408, "Commodities").withAssetClassPercentage(6).withAdjustedTarget(119421429.58).withPhysicalExposure(112888055.88).withOverlayExposure(4382000.00).withCashInBucket(ManagerCashTypes.REBALANCE, -2151373.70));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1409, "Infrastructure").withAssetClassPercentage(5).withAdjustedTarget(74686456.19).withPhysicalExposure(74686456.19));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1410, "Private Equity").withAssetClassPercentage(10).withAdjustedTarget(93109649.53).withPhysicalExposure(93109649.53));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1411, "Real Estate").withAssetClassPercentage(6).withAdjustedTarget(132422096.21).withPhysicalExposure(132422096.21));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1412, "Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(178714393.13).withPhysicalExposure(178714393.13));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1413, "Cash").withAssetClassPercentage(0).withAdjustedTarget(0).withPhysicalExposure(32184777.08).withOverlayExposure(-34532193.84).withCashInBucket(ManagerCashTypes.REBALANCE, -2347416.76).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 30863635.23));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1852, "International Equity").withAssetClassPercentage(6).withAdjustedTarget(119421429.58).withPhysicalExposure(104573710.09).withOverlayExposure(16848027.91).withCashInBucket(ManagerCashTypes.REBALANCE, 2000308.42));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2044, "Fixed Income Alternative").withAssetClassPercentage(8).withAdjustedTarget(67665049.01).withPhysicalExposure(67665049.01));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2045, "Risk Parity").withAssetClassPercentage(6).withAdjustedTarget(138380299.70).withPhysicalExposure(138380299.70));

		processCalculation(config);

		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 1321141.85, 8024545.16, 0.00, 2125356.81, 2125356.81, 10455634.18);
		validateAssetClassAllocation(config, "Domestic Equity-SMID", 0.00, 1851818.11, 0.00, -751830.75, -751830.75, -41691906.94);
		validateAssetClassAllocation(config, "Fixed Income Core", 0.00, 13579999.50, 0.00, 1124955.97, 1124955.97, 17262452.03);
		validateZeroAssetClassAllocation(config, "Fixed Income High Yield");
		validateAssetClassAllocation(config, "Commodities", 0.00, 3703636.23, 0.00, -2151373.70, -2151373.70, 2829737.47);
		validateZeroAssetClassAllocation(config, "Infrastructure");
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Real Estate");
		validateZeroAssetClassAllocation(config, "Hedge Funds");
		validateAssetClassAllocation(config, "Cash", 0.00, 0.00, 0.00, -2347416.76, -2347416.76, 0.00);
		validateAssetClassAllocation(config, "International Equity", 0.00, 3703636.23, 0.00, 2000308.42, 2000308.42, 11144083.26);
		validateZeroAssetClassAllocation(config, "Fixed Income Alternative");
		validateZeroAssetClassAllocation(config, "Risk Parity");
	}


	@Test
	public void test_UnderweightWithManualRebalanceOnBalanceDate() {
		// 530000 11/2
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();
		// Cash Available for Minimize Imbalances: Manager Cash: 1321141.85, Fund Cash: 30863635.23, Total: 32,184,777.08

		setupInvestmentAccountManualRebalance(config, true);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1404, "Domestic Equity Large Cap").withAssetClassPercentage(13).withAdjustedTarget(258746430.79).withPhysicalExposure(238945109.60).withOverlayExposure(21926678.00).withCashInBucket(ManagerCashTypes.REBALANCE, 2125356.81).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1321141.85));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1405, "Domestic Equity-SMID").withAssetClassPercentage(3).withAdjustedTarget(59710714.80).withPhysicalExposure(99550803.63).withOverlayExposure(-40591919.58).withCashInBucket(ManagerCashTypes.REBALANCE, -751830.75));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1406, "Fixed Income Core").withAssetClassPercentage(22).withAdjustedTarget(437878575.20).withPhysicalExposure(407036123.67).withOverlayExposure(31967407.50).withCashInBucket(ManagerCashTypes.REBALANCE, 1124955.97));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1407, "Fixed Income High Yield").withAssetClassPercentage(5).withAdjustedTarget(108484099.46).withPhysicalExposure(108484099.46));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1408, "Commodities").withAssetClassPercentage(6).withAdjustedTarget(119421429.58).withPhysicalExposure(112888055.88).withOverlayExposure(4382000.00).withCashInBucket(ManagerCashTypes.REBALANCE, -2151373.70));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1409, "Infrastructure").withAssetClassPercentage(5).withAdjustedTarget(74686456.19).withPhysicalExposure(74686456.19));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1410, "Private Equity").withAssetClassPercentage(10).withAdjustedTarget(93109649.53).withPhysicalExposure(93109649.53));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1411, "Real Estate").withAssetClassPercentage(6).withAdjustedTarget(132422096.21).withPhysicalExposure(132422096.21));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1412, "Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(178714393.13).withPhysicalExposure(178714393.13));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1413, "Cash").withAssetClassPercentage(0).withAdjustedTarget(0).withPhysicalExposure(32184777.08).withOverlayExposure(-34532193.84).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 30863635.23));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1852, "International Equity").withAssetClassPercentage(6).withAdjustedTarget(119421429.58).withPhysicalExposure(104573710.09).withOverlayExposure(16848027.91).withCashInBucket(ManagerCashTypes.REBALANCE, 2000308.42));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2044, "Fixed Income Alternative").withAssetClassPercentage(8).withAdjustedTarget(67665049.01).withPhysicalExposure(67665049.01));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2045, "Risk Parity").withAssetClassPercentage(6).withAdjustedTarget(138380299.70).withPhysicalExposure(138380299.70));

		// Consider Previous to Be Anything - Should not matter because it is a manual rebalance, not full
		addRebalanceHistory(config, "Domestic Equity Large Cap", 2000000.00, 2125356.81);
		addRebalanceHistory(config, "Domestic Equity-SMID", -70000.00, -751830.75);
		addRebalanceHistory(config, "Fixed Income Core", 1020044.00, 1124955.97);
		addRebalanceHistory(config, "Commodities", -210000.70, -2151373.70);
		addRebalanceHistory(config, "International Equity", 2000000.00, 2000308.42);

		processCalculation(config);

		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 1321141.85, 8024545.16, 0.00, 2125356.81, 2125356.81, 11672330.51);
		validateAssetClassAllocation(config, "Domestic Equity-SMID", 0.00, 1851818.11, 0.00, -751830.75, -751830.75, -42443737.69);
		validateAssetClassAllocation(config, "Fixed Income Core", 0.00, 13579999.50, 0.00, 1124955.97, 1124955.97, 18387408.00);
		validateZeroAssetClassAllocation(config, "Fixed Income High Yield");
		validateAssetClassAllocation(config, "Commodities", 0.00, 3703636.23, 0.00, -2151373.70, -2151373.70, 678363.77);
		validateZeroAssetClassAllocation(config, "Infrastructure");
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Real Estate");
		validateZeroAssetClassAllocation(config, "Hedge Funds");
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "International Equity", 0.00, 3703636.23, 0.00, 2000308.42, 2000308.42, 11705635.41);
		validateZeroAssetClassAllocation(config, "Fixed Income Alternative");
		validateZeroAssetClassAllocation(config, "Risk Parity");
	}


	@Test
	public void test_NegativeAdjustedTarget() {
		// 125300 Continental 10/1/2015
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1610, "Cash").withAssetClassPercentage(0).withAdjustedTarget(0).withPhysicalExposure(81605824.20).withOverlayExposure(-83642821.94).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 74774065.31));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3489, "Short Term Fixed Income").withAssetClassPercentage(0).withAdjustedTarget(-23515307.72).withPhysicalExposure(0).withOverlayExposure(-23863931.64));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1658, "Emerging Markets").withAssetClassPercentage(5).withAdjustedTarget(142682379.09).withPhysicalExposure(158368114.46).withOverlayExposure(-13648685.08).withRebalanceTriggers(-11414590.33, 11414590.33).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4981755.09));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1609, "Alternative Assets").withAssetClassPercentage(18).withAdjustedTarget(560687180.14).withPhysicalExposure(560687180.14));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(3952, "Other").withAssetClassPercentage(20).withAdjustedTarget(430041112.10).withPhysicalExposure(430041112.10));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3487, "Developed Markets Equity").withAssetClassPercentage(27).withAdjustedTarget(887657943.58).withPhysicalExposure(831205146.75).withOverlayExposure(43731017.49).withRebalanceTriggers(-71021581.01, 71021581.01).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1850003.8));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3488, "Long Duration Fixed Income").withAssetClassPercentage(30).withAdjustedTarget(856094274.51).withPhysicalExposure(791740204.05).withOverlayExposure(77424421.16).withRebalanceTriggers(-68464712.78, 68464712.78));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Short Term Fixed Income", 0.00, 0.00, 0.00, 0.00, 0.00, -23863931.64);
		validateAssetClassAllocation(config, "Emerging Markets", 4981755.09, 6030166.56, 0.00, 0.00, 0.00, -24830456.55);
		validateZeroAssetClassAllocation(config, "Alternative Assets");
		validateZeroAssetClassAllocation(config, "Other");
		validateAssetClassAllocation(config, "Developed Markets Equity", 1850003.80, 32562899.41, 0.00, 0.00, 0.00, 9318114.28);
		validateAssetClassAllocation(config, "Long Duration Fixed Income", 0.00, 36180999.34, 0.00, 0.00, 0.00, 39376273.91);
	}
}
