package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ContextConfiguration("ProductOverlayMinimizeImbalancesCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ProductOverlayMinimizeImbalancesUnderweightLongCalculatorTests extends BaseProductOverlayMinimizeImbalancesCalculatorTests {

	@Override
	public ProductOverlayMinimizeImbalancesCalculator getCalculator() {
		ProductOverlayMinimizeImbalancesUnderweightCalculator calculator = new ProductOverlayMinimizeImbalancesUnderweightCalculator();
		calculator.setShortTargetsAllowed(false);
		calculator.setExpandShortPositionsAllowed(false);
		calculator.setAttributionRebalancing(true);
		return calculator;
	}


	@Test
	public void test_1() {
		// 050920: Acument Global Technologies, Inc. Pension Plan	09/28/2012
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1463, "Cash").withAssetClassPercentage(0).withPhysicalExposure(28496852.12).withOverlayExposure(-29593309.96).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 27506423.64));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1464, "Domestic Equity Large Cap").withAssetClassPercentage(16.2).withAdjustedTarget(35732739.67).withPhysicalExposure(30615534.00).withOverlayExposure(5046382.11).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 642749.40));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1465, "Domestic Equity Small Cap").withAssetClassPercentage(7.8).withAdjustedTarget(17204652.43).withPhysicalExposure(7086186.02).withOverlayExposure(9470761.61));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1466, "International Equity").withAssetClassPercentage(3.7).withAdjustedTarget(8161181.28).withPhysicalExposure(6557730.61).withOverlayExposure(1511969.58).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 347679.08));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1467, "Emerging Markets").withAssetClassPercentage(3.3).withAdjustedTarget(7278891.42).withPhysicalExposure(0).withOverlayExposure(7275105.08));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1763, "Alternative Assets").withAssetClassPercentage(8).withAdjustedTarget(18111834.12).withPhysicalExposure(18111834.12));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1764, "Fixed Income").withAssetClassPercentage(61).withAdjustedTarget(134549204.92).withPhysicalExposure(130170366.97).withOverlayExposure(6289091.59));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 642749.40, 4843522.42, 0.00, 0.00, 0.00, -439889.71);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 0.00, 2332066.35, 0.00, 0.00, 0.00, 7138695.26);
		validateAssetClassAllocation(config, "International Equity", 347679.08, 1106236.60, 0.00, 0.00, 0.00, 58053.90);
		validateAssetClassAllocation(config, "Emerging Markets", 0.00, 986643.46, 0.00, 0.00, 0.00, 6288461.62);
		validateZeroAssetClassAllocation(config, "Alternative Assets");
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 18237954.80, 0.00, 0.00, 0.00, -13045321.06);

		// 050920	2012-10-01
		config = setupProductOverlayRunConfig();

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1463, "Cash").withAssetClassPercentage(0).withPhysicalExposure(28574491.72).withOverlayExposure(-29677447.57).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 27584063.24));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1464, "Domestic Equity Large Cap").withAssetClassPercentage(16.2).withAdjustedTarget(35804868.16).withPhysicalExposure(30685680.97).withOverlayExposure(5059762.81).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 642749.40));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1465, "Domestic Equity Small Cap").withAssetClassPercentage(7.8).withAdjustedTarget(17239380.96).withPhysicalExposure(7081446.09).withOverlayExposure(9503105.48));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1466, "International Equity").withAssetClassPercentage(3.7).withAdjustedTarget(8177655.07).withPhysicalExposure(6598849.44).withOverlayExposure(1526050.84).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 347679.08));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1467, "Emerging Markets").withAssetClassPercentage(3.3).withAdjustedTarget(7293584.26).withPhysicalExposure(0).withOverlayExposure(7292664.13));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1763, "Alternative Assets").withAssetClassPercentage(8).withAdjustedTarget(18066590.95).withPhysicalExposure(18066590.95));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1764, "Fixed Income").withAssetClassPercentage(61).withAdjustedTarget(134820799.88).withPhysicalExposure(130395820.11).withOverlayExposure(6295864.30));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 642749.40, 4857193.74, 0.00, 0.00, 0.00, -440180.33);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 0.00, 2338648.84, 0.00, 0.00, 0.00, 7164456.64);
		validateAssetClassAllocation(config, "International Equity", 347679.08, 1109359.07, 0.00, 0.00, 0.00, 69012.69);
		validateAssetClassAllocation(config, "Emerging Markets", 0.00, 989428.36, 0.00, 0.00, 0.00, 6303235.77);
		validateZeroAssetClassAllocation(config, "Alternative Assets");
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 18289433.24, 0.00, 0.00, 0.00, -13096524.78);

		// 050920	2012-10-02 
		config = setupProductOverlayRunConfig();

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1463, "Cash").withAssetClassPercentage(0).withPhysicalExposure(28609283.89).withOverlayExposure(-29698609.40).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 27618855.41));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1464, "Domestic Equity Large Cap").withAssetClassPercentage(16.2).withAdjustedTarget(35821047.05).withPhysicalExposure(30590453.52).withOverlayExposure(5064176.34).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 642749.40));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1465, "Domestic Equity Small Cap").withAssetClassPercentage(7.8).withAdjustedTarget(17247170.80).withPhysicalExposure(7095665.87).withOverlayExposure(9505367.29));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1466, "International Equity").withAssetClassPercentage(3.7).withAdjustedTarget(8181350.25).withPhysicalExposure(6595123.59).withOverlayExposure(1526120.90).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 347679.08));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1467, "Emerging Markets").withAssetClassPercentage(3.3).withAdjustedTarget(7296879.96).withPhysicalExposure(0).withOverlayExposure(7301008.31));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1763, "Alternative Assets").withAssetClassPercentage(8).withAdjustedTarget(18088998.99).withPhysicalExposure(18088998.99));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1764, "Fixed Income").withAssetClassPercentage(61).withAdjustedTarget(134881720.38).withPhysicalExposure(130537641.57).withOverlayExposure(6301936.57));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 642749.40, 4863320.19, 0.00, 0.00, 0.00, -441893.25);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 0.00, 2341598.61, 0.00, 0.00, 0.00, 7163768.68);
		validateAssetClassAllocation(config, "International Equity", 347679.08, 1110758.32, 0.00, 0.00, 0.00, 67683.50);
		validateAssetClassAllocation(config, "Emerging Markets", 0.00, 990676.34, 0.00, 0.00, 0.00, 6310331.97);
		validateZeroAssetClassAllocation(config, "Alternative Assets");
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 18312501.96, 0.00, 0.00, 0.00, -13099890.91);

		// 050920	2012-10-15 
		config = setupProductOverlayRunConfig();

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1463, "Cash").withAssetClassPercentage(0).withPhysicalExposure(13470398.86).withOverlayExposure(-28386122.56).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 12330509.96));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1464, "Domestic Equity Large Cap").withAssetClassPercentage(16.2).withAdjustedTarget(33221767.06).withPhysicalExposure(30248528.66).withOverlayExposure(5042976.49).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 821021.54));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1465, "Domestic Equity Small Cap").withAssetClassPercentage(7.8).withAdjustedTarget(15995665.62).withPhysicalExposure(7043526.70).withOverlayExposure(9364246.12));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1466, "International Equity").withAssetClassPercentage(3.7).withAdjustedTarget(7587687.53).withPhysicalExposure(6658279.15).withOverlayExposure(1516628.31).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 318867.36));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1467, "Emerging Markets").withAssetClassPercentage(3.3).withAdjustedTarget(6767396.99).withPhysicalExposure(0).withOverlayExposure(7218954.48));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1763, "Alternative Assets").withAssetClassPercentage(8).withAdjustedTarget(18041129.98).withPhysicalExposure(18041129.98));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1764, "Fixed Income").withAssetClassPercentage(61).withAdjustedTarget(125094308.06).withPhysicalExposure(131246091.89).withOverlayExposure(5243317.16));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 821021.54, 2171241.97, 0.00, 0.00, 0.00, -2992263.51);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 0.00, 1045412.80, 0.00, 0.00, 0.00, 6231546.77);
		validateAssetClassAllocation(config, "International Equity", 318867.36, 495900.94, 0.00, 0.00, 0.00, -679996.28);
		validateAssetClassAllocation(config, "Emerging Markets", 0.00, 442290.03, 0.00, 0.00, 0.00, 5616377.23);
		validateZeroAssetClassAllocation(config, "Alternative Assets");
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 8175664.21, 0.00, 0.00, 0.00, -8175664.20);
	}


	@Test
	public void test_2() {
		// From 182300 RH Donnelly Run on 10/1/2012
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(763, "Cash").withAssetClassPercentage(0).withPhysicalExposure(6271840.32).withOverlayExposure(-5907387.91).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 5063932.17));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(766, "Fixed Income").withAssetClassPercentage(35).withAdjustedTarget(62960643.99).withPhysicalExposure(65260945.7).withOverlayExposure(0));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(765, "Domestic Equity Small Cap").withAssetClassPercentage(10).withAdjustedTarget(17988755.43).withPhysicalExposure(17011458.2).withOverlayExposure(504589.67).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1207908.15));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(764, "Domestic Equity Large Cap").withAssetClassPercentage(35).withAdjustedTarget(62960643.99).withPhysicalExposure(59131024.61).withOverlayExposure(2385316.75));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(767, "International Equity").withAssetClassPercentage(20).withAdjustedTarget(35977510.85).withPhysicalExposure(32212285.42).withOverlayExposure(3017481.49));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 1772376.26, 0.00, 0.00, 0.00, -1772376.26);
		validateAssetClassAllocation(config, "International Equity", 0.00, 1012786.43, 0.00, 0.00, 0.00, 2044653.86);
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 0.00, 1772376.26, 0.00, 0.00, 0.00, 818619.11);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 1207908.15, 506393.22, 0.00, 0.00, 0.00, -1090896.71);

		// From 182300 RH Donnelly Run on 10/2
		config = setupProductOverlayRunConfig();

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(763, "Cash").withAssetClassPercentage(0).withPhysicalExposure(6248910.05).withOverlayExposure(-5910610.99).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 5063962.50));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(764, "Domestic Equity Large Cap").withAssetClassPercentage(35).withAdjustedTarget(63016187.56).withPhysicalExposure(59209069.06).withOverlayExposure(2387397.42));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(765, "Domestic Equity Small Cap").withAssetClassPercentage(10).withAdjustedTarget(18004625.02).withPhysicalExposure(17076648.90).withOverlayExposure(504709.77).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1184947.55));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(766, "Fixed Income").withAssetClassPercentage(35).withAdjustedTarget(63016187.56).withPhysicalExposure(65292684.59).withOverlayExposure(0));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(767, "International Equity").withAssetClassPercentage(20).withAdjustedTarget(36009250.04).withPhysicalExposure(32218937.58).withOverlayExposure(3018503.80));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 0.00, 1772386.88, 0.00, 0.00, 0.00, 808925.53);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 1184947.55, 506396.25, 0.00, 0.00, 0.00, -1113597.99);
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 1772386.88, 0.00, 0.00, 0.00, -1772386.88);
		validateAssetClassAllocation(config, "International Equity", 0.00, 1012792.50, 0.00, 0.00, 0.00, 2077059.33);
	}


	@Test
	public void test_3() {
		// From   386000: Marin County Employees' Retirement Association on 10/01/2012
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(526, "Cash").withAssetClassPercentage(0).withPhysicalExposure(42140243.44).withOverlayExposure(-40202659.69).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 42140243.44));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(533, "Real Estate").withAssetClassPercentage(10.5).withAdjustedTarget(164060149.06).withPhysicalExposure(135218430));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(532, "Private Equity").withAssetClassPercentage(0.5).withAdjustedTarget(7812388.05).withPhysicalExposure(53760610));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(528, "Domestic Equity Small Cap").withAssetClassPercentage(8.4).withCashPercentage(16.5).withAdjustedTarget(131248119.25).withPhysicalExposure(149679899.76));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(530, "International Equity").withAssetClassPercentage(21.5).withCashPercentage(22).withAdjustedTarget(335932686.16).withPhysicalExposure(320408340.84));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(527, "Domestic Equity Large Cap").withAssetClassPercentage(33.1).withCashPercentage(38.5).withAdjustedTarget(517180088.93).withPhysicalExposure(515780223.12));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(529, "Fixed Income").withAssetClassPercentage(26).withCashPercentage(23).withAdjustedTarget(406244178.62).withPhysicalExposure(345489862.9).withOverlayExposure(40202659.69));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Real Estate");
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 9692255.99, 0.00, 0.00, 0.00, 32375767.99);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 0.00, 6953140.17, 0.00, 0.00, 0.00, -6953140.17);
		validateAssetClassAllocation(config, "International Equity", 0.00, 9270853.56, 0.00, 0.00, 0.00, -9198634.10);
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 0.00, 16223993.72, 0.00, 0.00, 0.00, -16223993.72);

		// 386000: Marin County Employees' Retirement Association	10/02/2012
		config = setupProductOverlayRunConfig(11329839.06, 31574896.15, 0.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(526, "Cash").withAssetClassPercentage(0).withPhysicalExposure(42904735.21).withOverlayExposure(-40241277.93).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 31574896.15));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(527, "Domestic Equity Large Cap").withAssetClassPercentage(33.1).withCashPercentage(38.5).withAdjustedTarget(517988848.56).withPhysicalExposure(516367992.22).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 9471880.71));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(528, "Domestic Equity Small Cap").withAssetClassPercentage(8.4).withCashPercentage(16.5).withAdjustedTarget(131453363.38).withPhysicalExposure(149171776.79).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1857958.35));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(529, "Fixed Income").withAssetClassPercentage(26).withCashPercentage(23).withAdjustedTarget(406879458.08).withPhysicalExposure(345678009.06).withOverlayExposure(40241277.93));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(530, "International Equity").withAssetClassPercentage(21.5).withCashPercentage(22).withAdjustedTarget(336458013.42).withPhysicalExposure(321819439.35));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(532, "Private Equity").withAssetClassPercentage(0.5).withAdjustedTarget(7824604.96).withPhysicalExposure(53760610.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(533, "Real Estate").withAssetClassPercentage(10.5).withAdjustedTarget(164316704.23).withPhysicalExposure(135218430.00));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 9471880.71, 12156335.02, 0.00, 0.00, 0.00, -21628215.73);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 1857958.35, 5209857.86, 0.00, 0.00, 0.00, -7067816.21);
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 7262226.11, 0.00, 0.00, 0.00, 35642509.10);
		validateAssetClassAllocation(config, "International Equity", 0.00, 6946477.15, 0.00, 0.00, 0.00, -6946477.15);
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Real Estate");

		// 386000: Marin County Employees' Retirement Association	10/26/2012
		config = setupProductOverlayRunConfig();

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(526, "Cash").withAssetClassPercentage(0).withPhysicalExposure(36131602.60).withOverlayExposure(-41772672.75).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 36131602.60));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(527, "Domestic Equity Large Cap").withAssetClassPercentage(33.1).withCashPercentage(38.5).withAdjustedTarget(510592781.75).withPhysicalExposure(503983995.07));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(528, "Domestic Equity Small Cap").withAssetClassPercentage(8.4).withCashPercentage(16.5).withAdjustedTarget(129576415.91).withPhysicalExposure(142932808.08));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(529, "Fixed Income").withAssetClassPercentage(26).withCashPercentage(23).withAdjustedTarget(401069858.77).withPhysicalExposure(345172805.47).withOverlayExposure(41011858.43));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(530, "International Equity").withAssetClassPercentage(21.5).withCashPercentage(22).withAdjustedTarget(331653921.68).withPhysicalExposure(319600147.11).withOverlayExposure(760814.33));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(532, "Private Equity").withAssetClassPercentage(0.5).withAdjustedTarget(7712881.90).withPhysicalExposure(59536591.57));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(533, "Real Estate").withAssetClassPercentage(10.5).withAdjustedTarget(161970519.89).withPhysicalExposure(135218430.00));


		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 0.00, 13910667.00, 0.00, 0.00, 0.00, -13910667.00);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 0.00, 5961714.43, 0.00, 0.00, 0.00, -5961714.43);
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 8310268.60, 0.00, 0.00, 0.00, 27821334.00);
		validateAssetClassAllocation(config, "International Equity", 0.00, 7948952.57, 0.00, 0.00, 0.00, -7948952.57);
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Real Estate");
	}


	@Test
	public void test_4() {
		// From   186000: Duke Energy 12/5/2012
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(0.00, 6000000.00, -3000000.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(1238, "Commodities").withAssetClassPercentage(2).withAdjustedTarget(105140240.50).withPhysicalExposure(81174399.35).withOverlayExposure(17723750));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1239, "Infrastructure").withAssetClassPercentage(2).withAdjustedTarget(105140240.50).withPhysicalExposure(101014068.11));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(281, "Cash").withAssetClassPercentage(0).withPhysicalExposure(21952375.74).withOverlayExposure(25633143.94).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 21952375.74));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(282, "Domestic Equity").withAssetClassPercentage(29.5).withAdjustedTarget(1550818547.42).withPhysicalExposure(1562505704.14));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(283, "Fixed Income").withAssetClassPercentage(32).withAdjustedTarget(1682243848.04).withPhysicalExposure(1760538748.10));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(284, "Global Equity").withAssetClassPercentage(10).withAdjustedTarget(525701202.51).withPhysicalExposure(517459390.55));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(285, "Hedge Funds").withAssetClassPercentage(4).withAdjustedTarget(210280481.01).withPhysicalExposure(174229077.48));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(286, "International Equity").withAssetClassPercentage(16.5).withAdjustedTarget(867406984.15).withPhysicalExposure(840492175.58).withOverlayExposure(7909393.94));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(287, "Real Estate").withAssetClassPercentage(4).withAdjustedTarget(210280481.01).withPhysicalExposure(197646086.09));


		processCalculation(config);

		validateAssetClassAllocation(config, "Commodities", 0.00, 421163.91, 0.00, 0.00, 0.00, 17302586.09);
		validateZeroAssetClassAllocation(config, "Infrastructure");
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity", 0.00, 6212167.60, 0.00, 0.00, 0.00, -6212167.60);
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 6738622.49, 0.00, 0.00, 0.00, -6738622.49);
		validateAssetClassAllocation(config, "Global Equity", 0.00, 2105819.53, 0.00, 0.00, 0.00, -2105819.53);
		validateZeroAssetClassAllocation(config, "Hedge Funds");
		validateAssetClassAllocation(config, "International Equity", 0.00, 3474602.22, 0.00, 0.00, 0.00, -2245976.48);
		validateZeroAssetClassAllocation(config, "Real Estate");
	}


	@Test
	public void test_5() {
		// 523500: Presb. on 11/05/2013 - Should have same result as underweight long only
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(null, null, -35477805.14);
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2331, "Total Capital Appreciation").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2332, "Total Bonds").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2294, "Diversifying Hedge Funds").withAssetClassPercentage(15).withAdjustedTarget(94978294.28).withPhysicalExposure(94978294.28));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2295, "Public Inflation Sensitive - Commodities").withAssetClassPercentage(3).withAdjustedTarget(20537145.56).withPhysicalExposure(19450870.65).withOverlayExposure(759375.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2298, "Private Inflation Sensitive (PIS)").withAssetClassPercentage(0).withAdjustedTarget(429626.00).withPhysicalExposure(429626.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2301, "Cash").withAssetClassPercentage(4).withAdjustedTarget(35477804.73).withPhysicalExposure(35969732.14).withOverlayExposure(-3452636.91).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 35969732.14));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2440, "Public Inflation Sensitive - TIPS").withAssetClassPercentage(2).withAdjustedTarget(12994755.90).withPhysicalExposure(12994755.90));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2291, "Public Equities").withAssetClassPercentage(53).withAdjustedTarget(336477457.67).withPhysicalExposure(338414808.71));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2292, "Private Equity").withAssetClassPercentage(0).withAdjustedTarget(33811334.00).withPhysicalExposure(33811334.00));
		addAssetClassToMap(config, "Total Capital Appreciation", ProductOverlayAssetClassBuilder.newAssetClass(2293, "Market Sensitive Hedge Funds").withAssetClassPercentage(10).withAdjustedTarget(67100380.11).withPhysicalExposure(67100380.11));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2299, "Domestic Bonds").withAssetClassPercentage(10).withAdjustedTarget(72275075.92).withPhysicalExposure(70932072.38).withOverlayExposure(2693261.91));
		addAssetClassToMap(config, "Total Bonds", ProductOverlayAssetClassBuilder.newAssetClass(2300, "Global Bonds").withAssetClassPercentage(3).withAdjustedTarget(17650077.84).withPhysicalExposure(17650077.84));


		processCalculation(config);

		validateRollupAssetClassAllocation(config, "Total Capital Appreciation");
		validateRollupAssetClassAllocation(config, "Total Bonds");
		validateZeroAssetClassAllocation(config, "Diversifying Hedge Funds");
		validateAssetClassAllocation(config, "Public Inflation Sensitive - Commodities", 0.00, 22360.32, 0.00, 0.00, 0.00, 469566.68);
		validateZeroAssetClassAllocation(config, "Private Inflation Sensitive (PIS)");
		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Public Inflation Sensitive - TIPS");
		validateAssetClassAllocation(config, "Public Equities", 0.00, 395032.29, 0.00, 0.00, 0.00, -395032.29);
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateZeroAssetClassAllocation(config, "Market Sensitive Hedge Funds");
		validateAssetClassAllocation(config, "Domestic Bonds", 0.00, 74534.39, 0.00, 0.00, 0.00, -74534.39);
		validateZeroAssetClassAllocation(config, "Global Bonds");

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 35,969,732.14 Cash Adjustment: -35,477,805.14 Cash To Apply: 491,927.00");
	}


	@Test
	public void test_6() {
		// 719061: Alliant Energy MOC run on 2/27/2014 - 10 million decrease for MOC adjustment
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(null, null, 0.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2139, "Global Asset Allocation").withAssetClassPercentage(10).withAdjustedTarget(101393865.25).withPhysicalExposure(101393865.25));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2372, "Risk Parity").withAssetClassPercentage(10).withAdjustedTarget(99725883.00).withPhysicalExposure(99725883.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(43, "Cash").withAssetClassPercentage(0).withPhysicalExposure(13277874.73).withOverlayExposure(-22677265.69).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 9982202.48));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(44, "Domestic Equity Large Cap").withAssetClassPercentage(28).withAdjustedTarget(297158886.03).withPhysicalExposure(297158886.03).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 321013.31));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(45, "Domestic Equity Small Cap").withAssetClassPercentage(4).withAdjustedTarget(43368949.54).withPhysicalExposure(43368949.54));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(48, "International Equity").withAssetClassPercentage(13).withAdjustedTarget(136464443.21).withPhysicalExposure(136464443.21).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 2616268.20));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(49, "Intl Equity-Emerging").withAssetClassPercentage(5).withAdjustedTarget(46798285.40).withPhysicalExposure(46798285.40).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 358390.74));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2373, "Fixed Income - Long").withAssetClassPercentage(20).withAdjustedTarget(209350707.16).withPhysicalExposure(217839493.90).withOverlayExposure(522566.49));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(47, "Fixed Income - Intermediate").withAssetClassPercentage(10).withAdjustedTarget(104675353.58).withPhysicalExposure(82908692.11).withOverlayExposure(22154699.20));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Global Asset Allocation");
		validateZeroAssetClassAllocation(config, "Risk Parity");
		validateAssetClassAllocation(config, "Fixed Income - Long", 0.00, 2495550.62, 0.00, 0.00, 0.00, -2495550.62);
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 321013.31, 3493770.87, 0.00, 0.00, 0.00, -3814784.18);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 0.00, 499110.12, 0.00, 0.00, 0.00, -499110.12);
		validateAssetClassAllocation(config, "Fixed Income - Intermediate", 0.00, 1247775.31, 0.00, 0.00, 0.00, 12030099.42);
		validateAssetClassAllocation(config, "International Equity", 2616268.20, 1622107.90, 0.00, 0.00, 0.00, -4238376.10);
		validateAssetClassAllocation(config, "Intl Equity-Emerging", 358390.74, 623887.66, 0.00, 0.00, 0.00, -982278.40);

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 13,277,874.73");
	}


	@Test
	public void test_7() {
		// 564500: St. Olaf run on 12/1/2014 
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2567, "International Equity Combined").asRollup());
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(875, "Cash").withAssetClassPercentage(0).withPhysicalExposure(3849463.96).withOverlayExposure(-3674502.46).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 1309380.52));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(876, "Domestic Equity Large Cap").withAssetClassPercentage(15.25).withAdjustedTarget(72282849.07).withPhysicalExposure(74560629.47).withCashInBucket(ManagerCashTypes.REBALANCE, 922520.37).withRebalanceTriggers(-3614804.75, 3614804.75).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 622672.69));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(879, "Hedge Funds").withAssetClassPercentage(20).withAdjustedTarget(87378000.00).withPhysicalExposure(87378000.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(881, "Private Equity").withAssetClassPercentage(15).withAdjustedTarget(53232767.00).withPhysicalExposure(53232767.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(882, "Real Assets").withAssetClassPercentage(15).withAdjustedTarget(65843438.06).withPhysicalExposure(62755774.17).withOverlayExposure(123550.00).withCashInBucket(ManagerCashTypes.REBALANCE, 554889.91).withRebalanceTriggers(-3292171.90, 3292171.90).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 3219.40));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(878, "Fixed Income").withAssetClassPercentage(14).withAdjustedTarget(61867126.26).withPhysicalExposure(59171253.00).withOverlayExposure(2986982.07).withCashInBucket(ManagerCashTypes.REBALANCE, 10739.30).withRebalanceTriggers(-3092446.81, 3092446.81));
		addAssetClassToMap(config, "International Equity Combined", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2253, "Emerging Markets").withAssetClassPercentage(5.5).withAdjustedTarget(26069224.25).withPhysicalExposure(25413717.99).withOverlayExposure(197035.70).withCashInBucket(ManagerCashTypes.REBALANCE, -482693.05).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 525839.88));
		addAssetClassToMap(config, "International Equity Combined", ProductOverlayAssetClassBuilder.newAssetClassWithReplications(880, "International Equity").withAssetClassPercentage(15.25).withAdjustedTarget(72282849.06).withPhysicalExposure(72594648.11).withOverlayExposure(366934.69).withCashInBucket(ManagerCashTypes.REBALANCE, -1005456.52).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1388351.47));

		processCalculation(config);

		validateRollupAssetClassAllocation(config, "International Equity Combined");
		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 622672.69, 307200.81, 0.00, 922520.37, 922520.37, -1852393.87);
		validateZeroAssetClassAllocation(config, "Hedge Funds");
		validateZeroAssetClassAllocation(config, "Private Equity");
		validateAssetClassAllocation(config, "Real Assets", 3219.40, 302164.74, 0.00, 554889.91, 554889.91, -561762.54);
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 282020.42, 0.00, 10739.30, 10739.30, 2694222.35);
		validateAssetClassAllocation(config, "Emerging Markets", 525839.88, 110793.74, 0.00, -482693.05, -482693.05, 43095.13);
		validateAssetClassAllocation(config, "International Equity", 1388351.47, 307200.81, 0.00, -1005456.52, -1005456.52, -323161.07);

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 3,849,463.96");
	}


	@Test
	public void test_8() {
		// 719061: Alliant Energy MOC run on 1/29/2015 - 5.71 million decrease for MOC adjustment
		// Tests for No Shorts, where that (FI Long) asset class is brought down to 0 and still overweight 
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(43, "Cash").withAssetClassPercentage(0).withPhysicalExposure(38353957.67).withOverlayExposure(-43416953.26).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 36132426.12));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2139, "Global Asset Allocation").withAssetClassPercentage(10).withAdjustedTarget(102512296.63).withPhysicalExposure(102512296.63).withRebalanceTriggers(-20510021.85, 20510021.85));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(2372, "Risk Parity").withAssetClassPercentage(10).withAdjustedTarget(104676420.00).withPhysicalExposure(104676420.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3339, "Long Treasury").withAssetClassPercentage(0));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(47, "Multi-Sector Fixed Income").withAssetClassPercentage(10).withAdjustedTarget(102999135.30).withPhysicalExposure(102999135.30));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(49, "Intl Equity-Emerging").withAssetClassPercentage(5).withAdjustedTarget(51845003.79).withPhysicalExposure(49491760.72).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 565648.73));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2373, "Fixed Income - Long").withAssetClassPercentage(20).withAdjustedTarget(208267905.76).withPhysicalExposure(215012209.25).withOverlayExposure(2585363.28).withRebalanceTriggers(-41662926.08, 41662926.08));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(44, "Domestic Equity Large Cap").withAssetClassPercentage(28).withAdjustedTarget(290332835.73).withPhysicalExposure(285262332.87).withOverlayExposure(6065569.67).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 311071.72));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(45, "Domestic Equity Small Cap").withAssetClassPercentage(4).withAdjustedTarget(41476282.81).withPhysicalExposure(31421093.40).withOverlayExposure(10476727.02).withRebalanceTriggers(-8295256.56, 8295256.56));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(48, "International Equity").withAssetClassPercentage(13).withAdjustedTarget(134797190.13).withPhysicalExposure(107177864.31).withOverlayExposure(24289293.29).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1344811.10));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Global Asset Allocation");
		validateZeroAssetClassAllocation(config, "Risk Parity");
		validateZeroAssetClassAllocation(config, "Long Treasury");
		validateZeroAssetClassAllocation(config, "Multi-Sector Fixed Income");
		validateAssetClassAllocation(config, "Intl Equity-Emerging", 565648.73, 2580887.58, 0.00, 0.00, 0.00, -3146536.31);
		validateAssetClassAllocation(config, "Fixed Income - Long", 0.00, 10323550.32, 0.00, 0.00, 0.00, -10323550.32);
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 311071.72, 14452970.45, 0.00, 0.00, 0.00, -10621938.26);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 0.00, 2064710.06, 0.00, 0.00, 0.00, 7857850.41);
		validateAssetClassAllocation(config, "International Equity", 1344811.10, 6710307.71, 0.00, 0.00, 0.00, 16234174.48);

		validateTradingBandsMessage(config, null);
		validateMinimizeImbalancesMessage(
				config,
				"Cash Available for Minimize Imbalances: 38,353,957.67");
	}
}
