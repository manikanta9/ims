package com.clifton.product.overlay.manager.populator;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalance;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalanceSearchForm;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalanceService;
import com.clifton.portfolio.run.manager.populator.BasePortfolioRunManagerAccountBalancePopulatorTests;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.overlay.search.ProductOverlayManagerAccountSearchForm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ProductOverlayManagerAccountBalancePopulatorTests extends BasePortfolioRunManagerAccountBalancePopulatorTests {

	@Resource
	private PortfolioRunManagerAccountBalanceService portfolioRunManagerAccountBalanceService;

	@Resource
	private ProductOverlayService productOverlayService;

	@Resource
	private ReadOnlyDAO<ProductOverlayManagerAccount> productOverlayManagerAccountDAO;

	private static final int RUN_ZERO_BALANCE = 1;
	private static final int RUN_BALANCE_ADJUSTMENTS = 2;
	private static final int RUN_BALANCE_ADJUSTMENTS_MOC = 3;


	@BeforeEach
	public void setupTest() {
		Mockito.when(this.productOverlayService.getProductOverlayManagerAccountList(ArgumentMatchers.any(ProductOverlayManagerAccountSearchForm.class))).thenAnswer((Answer<List<ProductOverlayManagerAccount>>) invocation -> {
			Object[] args = invocation.getArguments();
			ProductOverlayManagerAccountSearchForm searchForm = (ProductOverlayManagerAccountSearchForm) args[0];
			return getProductOverlayManagerAccountDAO().findByField("overlayRun.id", searchForm.getRunId());
		});
	}


	/**
	 * Regular tests - all zero balances to verify managers pull through correctly
	 */
	@Test
	public void testProductOverlayManagerAccountBalancePopulator_ZeroBalance() {
		List<PortfolioRunManagerAccountBalance> managerAccountBalanceList = (List<PortfolioRunManagerAccountBalance>) this.portfolioRunManagerAccountBalanceService.getPortfolioRunManagerAccountBalanceList(RUN_ZERO_BALANCE, new PortfolioRunManagerAccountBalanceSearchForm());
		String expected = "MOC\tManager #\tAsset Class\tAllocation\tBase Securities\tBase Cash\tBase Total\tAdjustment Count\tSecurities Adjustment\tCash Adjustment\tAdjusted Securities\tAdjusted Cash\tAdjusted Total\n" +
				"false\tM00001\tEquity\t100.00\t0\t0\t0\t0\t\t\t0\t0\t0\n" +
				"false\tM00002\tFixed Income\t100.00\t0\t0\t0\t0\t\t\t0\t0\t0\n" +
				"false\tM00030\tEquity\t100.00\t0\t0\t0\t0\t\t\t0\t0\t0\n" +
				"false\tM00040\tCash\t100.00\t0\t0\t0\t0\t\t\t0\t0\t0\n";
		validatePortfolioRunManagerAccountBalanceList(expected, managerAccountBalanceList);
	}


	/**
	 * Test Regular run with balances and adjustments
	 */
	@Test
	public void testProductOverlayManagerAccountBalancePopulator_BalanceAdjustments() {
		List<PortfolioRunManagerAccountBalance> managerAccountBalanceList = (List<PortfolioRunManagerAccountBalance>) this.portfolioRunManagerAccountBalanceService.getPortfolioRunManagerAccountBalanceList(RUN_BALANCE_ADJUSTMENTS, new PortfolioRunManagerAccountBalanceSearchForm());
		String expected = "MOC\tManager #\tAsset Class\tAllocation\tBase Securities\tBase Cash\tBase Total\tAdjustment Count\tSecurities Adjustment\tCash Adjustment\tAdjusted Securities\tAdjusted Cash\tAdjusted Total\n" +
				"false\tM00001\tEquity\t100.00\t25,000\t0\t25,000\t1\t5,000\t\t30,000\t0\t30,000\n" +
				"false\tM00002\tEquity\t50.00\t0\t100,000\t100,000\t0\t\t\t0\t100,000\t100,000\n" +
				"false\tM00002\tFixed Income\t50.00\t0\t100,000\t100,000\t0\t\t\t0\t100,000\t100,000\n" +
				"false\tM00030\tEquity\t100.00\t5,000\t120,000\t125,000\t1\t-5,000\t\t0\t120,000\t120,000\n" +
				"false\tM00040\tCash\t75.00\t-15,000\t45,000\t30,000\t2\t-7,500\t\t-22,500\t45,000\t22,500\n" +
				"false\tM00040\tFixed Income\t25.00\t-5,000\t15,000\t10,000\t2\t-2,500\t\t-7,500\t15,000\t7,500";
		validatePortfolioRunManagerAccountBalanceList(expected, managerAccountBalanceList);
	}


	@Test
	public void testProductOverlayManagerAccountBalancePopulator_BalanceAdjustments_ShowAdjustedOnly() {
		PortfolioRunManagerAccountBalanceSearchForm searchForm = new PortfolioRunManagerAccountBalanceSearchForm();
		searchForm.setShowAdjustedManagersOnly(true);
		List<PortfolioRunManagerAccountBalance> managerAccountBalanceList = (List<PortfolioRunManagerAccountBalance>) this.portfolioRunManagerAccountBalanceService.getPortfolioRunManagerAccountBalanceList(RUN_BALANCE_ADJUSTMENTS, searchForm);
		String expected = "MOC\tManager #\tAsset Class\tAllocation\tBase Securities\tBase Cash\tBase Total\tAdjustment Count\tSecurities Adjustment\tCash Adjustment\tAdjusted Securities\tAdjusted Cash\tAdjusted Total\n" +
				"false\tM00001\tEquity\t100.00\t25,000\t0\t25,000\t1\t5,000\t\t30,000\t0\t30,000\n" +
				"false\tM00030\tEquity\t100.00\t5,000\t120,000\t125,000\t1\t-5,000\t\t0\t120,000\t120,000\n" +
				"false\tM00040\tCash\t75.00\t-15,000\t45,000\t30,000\t2\t-7,500\t\t-22,500\t45,000\t22,500\n" +
				"false\tM00040\tFixed Income\t25.00\t-5,000\t15,000\t10,000\t2\t-2,500\t\t-7,500\t15,000\t7,500";
		validatePortfolioRunManagerAccountBalanceList(expected, managerAccountBalanceList);
	}


	/**
	 * Test MOC run with balances and adjustments
	 */
	@Test
	public void testProductOverlayManagerAccountBalancePopulator_BalanceAdjustments_MOC() {
		List<PortfolioRunManagerAccountBalance> managerAccountBalanceList = (List<PortfolioRunManagerAccountBalance>) this.portfolioRunManagerAccountBalanceService.getPortfolioRunManagerAccountBalanceList(RUN_BALANCE_ADJUSTMENTS_MOC, new PortfolioRunManagerAccountBalanceSearchForm());
		String expected = "MOC\tManager #\tAsset Class\tAllocation\tBase Securities\tBase Cash\tBase Total\tAdjustment Count\tSecurities Adjustment\tCash Adjustment\tAdjusted Securities\tAdjusted Cash\tAdjusted Total\n" +
				"true\tM00001\tEquity\t100.00\t25,000\t0\t25,000\t1\t5,000\t\t30,000\t0\t30,000\n" +
				"true\tM00002\tEquity\t50.00\t0\t100,000\t100,000\t1\t\t10,000\t0\t110,000\t110,000\n" +
				"true\tM00002\tFixed Income\t50.00\t0\t100,000\t100,000\t1\t\t10,000\t0\t110,000\t110,000\n" +
				"true\tM00030\tEquity\t100.00\t-45,000\t170,000\t125,000\t3\t45,000\t15,000\t0\t185,000\t185,000\n" +
				"true\tM00040\tCash\t75.00\t-52,500\t45,000\t-7,500\t5\t67,500\t11,250\t15,000\t56,250\t71,250\n" +
				"true\tM00040\tFixed Income\t25.00\t-17,500\t15,000\t-2,500\t5\t22,500\t3,750\t5,000\t18,750\t23,750";
		validatePortfolioRunManagerAccountBalanceList(expected, managerAccountBalanceList);
	}


	public ReadOnlyDAO<ProductOverlayManagerAccount> getProductOverlayManagerAccountDAO() {
		return this.productOverlayManagerAccountDAO;
	}
}
