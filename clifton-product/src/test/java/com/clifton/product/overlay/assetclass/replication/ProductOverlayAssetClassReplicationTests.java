package com.clifton.product.overlay.assetclass.replication;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.product.ProductInMemoryDatabaseContext;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.trade.ProductOverlayRunTrade;
import com.clifton.product.overlay.trade.ProductOverlayTradeHandlerImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@ProductInMemoryDatabaseContext
public class ProductOverlayAssetClassReplicationTests extends BaseInMemoryDatabaseTests {

	@Resource
	private PortfolioRunService portfolioRunService;


	@Resource
	private ProductOverlayTradeHandlerImpl productOverlayTradeHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<org.springframework.core.io.Resource> getFunctionsData() {
		return CollectionUtils.createList(new ClassPathResource("com/clifton/product/ProductInMemoryDatabaseTestsDataFunctions.xml"));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Tests the calculated suggested trade quantity for a replication Must be loaded from the productOverlayTradeHandler so required current positions are populated
	 */
	@Test
	public void testCalculateSuggestedTradeQuantity() {
		PortfolioRun run = this.portfolioRunService.getPortfolioRun(734785);
		ProductOverlayRunTrade tradeRun = this.productOverlayTradeHandler.getPortfolioRunTrade(run, true);
		Map<Integer, String> replicationIdExpectedResult = new HashMap<>();
		replicationIdExpectedResult.put(11353707, "0.96");
		replicationIdExpectedResult.put(11353708, "-1.22");
		replicationIdExpectedResult.put(11353709, "1.16");
		replicationIdExpectedResult.put(11353710, "1.58");
		replicationIdExpectedResult.put(11353711, "-0.69");
		replicationIdExpectedResult.put(11353712, "0.32");
		replicationIdExpectedResult.put(11353713, "-0.26");
		replicationIdExpectedResult.put(11353714, "-0.24");
		replicationIdExpectedResult.put(11353715, "-0.81");
		replicationIdExpectedResult.put(11353716, "-0.26");
		replicationIdExpectedResult.put(11353717, "0.21");
		replicationIdExpectedResult.put(11353718, "0.63");
		for (ProductOverlayAssetClassReplication replication : CollectionUtils.getIterable(tradeRun.getDetailList())) {
			String expected = replicationIdExpectedResult.get(replication.getId());
			Assertions.assertEquals(expected, CoreMathUtils.formatNumberMoney(replication.calculateSuggestedTradeQuantity()), "Did not get expected suggested trade quantity for Replication " + replication.getId() + " " + replication.getLabel());
		}
	}
}
