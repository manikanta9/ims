package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ContextConfiguration("ProductOverlayMinimizeImbalancesCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ProductOverlayMinimizeImbalancesUnderweightDoNotExpandShortsPercentageOfRebalanceTriggerCalculatorTests extends BaseProductOverlayMinimizeImbalancesCalculatorTests {

	@Override
	public ProductOverlayMinimizeImbalancesCalculator getCalculator() {
		ProductOverlayMinimizeImbalancesUnderweightCalculator calculator = new ProductOverlayMinimizeImbalancesUnderweightCalculator();
		calculator.setShortTargetsAllowed(true);
		calculator.setExpandShortPositionsAllowed(false);
		calculator.setAttributionRebalancing(true);
		calculator.setDeviationPercentageDenominator(BaseProductOverlayMinimizeImbalancesCalculatorImpl.DEVIATION_PERCENTAGE_DENOMINATOR_REBAL_TRIGGER);
		return calculator;
	}


	@Test
	public void testWithTradingBandsAndNoCashBuffer_NoTrades() {
		// 221800: 1199 SEIU HEPF PIOS 5/22/2015 - No Trades
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(-1000000.00, 1000000.00, 0.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(16, "Cash").withAssetClassPercentage(0).withPhysicalExposure(294536725.80).withOverlayExposure(-293578292.81).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 143526493.34));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(18, "Domestic Equity Mid Cap").withAssetClassPercentage(3.85).withAdjustedTarget(356745331.91).withPhysicalExposure(453545597.58).withOverlayExposure(-48549283.93).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 6802520.15));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1237, "Special Opportunities").withAssetClassPercentage(5).withAdjustedTarget(503996579.31).withPhysicalExposure(503996579.31));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(17, "Domestic Equity Large Cap").withAssetClassPercentage(13.48).withAdjustedTarget(1249071967.35).withPhysicalExposure(1257713609.88).withOverlayExposure(0).withRebalanceTriggers(-301391031.73, 301391031.73).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 10709517.62));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(20, "Emerging Markets").withAssetClassPercentage(3).withAdjustedTarget(292758558.05).withPhysicalExposure(330773032.23).withOverlayExposure(0).withRebalanceTriggers(-101111830.00, 101111830.00).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4566366.49));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(23, "Mrkt. Neut. Hedge FOF").withAssetClassPercentage(12).withAdjustedTarget(1077347021.53).withPhysicalExposure(1077347021.53).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 19118927.20));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(24, "Private Equity-Domestic").withAssetClassPercentage(8).withAdjustedTarget(872639979.53).withPhysicalExposure(872639979.53).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 18092683.83));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(25, "Real Estate - Core").withAssetClassPercentage(7.5).withAdjustedTarget(699710750.58).withPhysicalExposure(699710750.58).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4350348.28));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(26, "Real Estate - Non Core").withAssetClassPercentage(2.5).withAdjustedTarget(320351595.82).withPhysicalExposure(320351595.82));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(19, "Domestic Equity Small Cap").withAssetClassPercentage(1.93).withAdjustedTarget(178835971.59).withPhysicalExposure(171617541.95).withOverlayExposure(7639305.93).withRebalanceTriggers(-59305977.21, 59305977.21).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 750860.30));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2014, "Global Equity").withAssetClassPercentage(1.5).withAdjustedTarget(146379279.02).withPhysicalExposure(118103604.79).withOverlayExposure(28461960.35).withRebalanceTriggers(-32083561.44, 32083561.44).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1912704.13));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3410, "Fixed Income - EM").withAssetClassPercentage(5).withAdjustedTarget(487930930.08).withPhysicalExposure(418247704.23).withOverlayExposure(33584390.93).withRebalanceTriggers(-89445080.38, 89445080.38).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 17087441.38));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(22, "International Equity").withAssetClassPercentage(16.24).withAdjustedTarget(1584799660.92).withPhysicalExposure(1573628525.98).withOverlayExposure(33773489.85).withRebalanceTriggers(-403475090.86, 403475090.86).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 50244958.41));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2856, "Fixed Income - High Yield").withAssetClassPercentage(10).withAdjustedTarget(975861860.18).withPhysicalExposure(873864296.49).withOverlayExposure(53871370.02).withRebalanceTriggers(-119584183.56, 119584183.56).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 17373904.67));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(21, "Fixed Income").withAssetClassPercentage(10).withAdjustedTarget(975861860.18).withPhysicalExposure(756214780.35).withOverlayExposure(184797059.67).withRebalanceTriggers(-79722789.04, 79722789.04));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Mid Cap", 9264266.97, 8501184.61, 0.00, 0.00, 0.00, -66314735.51);
		validateZeroAssetClassAllocation(config, "Special Opportunities");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 19328828.57, 29765186.62, 0.00, 0.00, 0.00, -49094015.19);
		validateAssetClassAllocation(config, "Emerging Markets", 6484610.77, 6624299.69, 0.00, 0.00, 0.00, -13108910.46);
		validateZeroAssetClassAllocation(config, "Mrkt. Neut. Hedge FOF");
		validateZeroAssetClassAllocation(config, "Private Equity-Domestic");
		validateZeroAssetClassAllocation(config, "Real Estate - Core");
		validateZeroAssetClassAllocation(config, "Real Estate - Non Core");
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 1984930.78, 4261632.80, 0.00, 0.00, 0.00, 1392742.35);
		validateAssetClassAllocation(config, "Global Equity", 2871826.27, 3312149.85, 0.00, 0.00, 0.00, 22277984.23);
		validateAssetClassAllocation(config, "Fixed Income - EM", 20284515.17, 11040499.49, 0.00, 0.00, 0.00, 2259376.27);
		validateAssetClassAllocation(config, "International Equity", 60629054.09, 35859542.34, 0.00, 0.00, 0.00, -62715106.58);
		validateAssetClassAllocation(config, "Fixed Income - High Yield", 23768052.26, 22080998.98, 0.00, 0.00, 0.00, 8022318.78);
		validateAssetClassAllocation(config, "Fixed Income", 6394147.59, 22080998.98, 0.00, 0.00, 0.00, 156321913.10);

		validateTradingBandsMessage(config, "Total Effective Cash: 294,536,725.80 Total Overlay Exposure: 293,578,292.82 Difference: 958,432.98 No Trading Range: [-1,000,000.00] - [1,000,000.00] Difference is inside of no trading bands.  Asset Class Overlay Targets will be set to Overlay Exposure.");
		validateMinimizeImbalancesMessage(config, null);
	}


	@Test
	public void testUnderweightWithTradingBandsAndNoCashBuffer() {
		// 221800: 1199 SEIU HEPF PIOS 5/21/2015 - Add to Underweight
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(-1000000.00, 1000000.00, 0.00);
		// Cash Available for Minimize Imbalances: Manager cash: 298599858.17

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(16, "Cash").withAssetClassPercentage(0).withPhysicalExposure(298599858.17).withOverlayExposure(-285690574.00).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 143416191.27));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(18, "Domestic Equity Mid Cap").withAssetClassPercentage(3.85).withAdjustedTarget(357374081.89).withPhysicalExposure(453286161.28).withOverlayExposure(-48623464.35).withRebalanceTriggers(-94380725.86, 94380725.86).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 7330934.84));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1237, "Special Opportunities").withAssetClassPercentage(5).withAdjustedTarget(504220897.87).withPhysicalExposure(504220897.87));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(17, "Domestic Equity Large Cap").withAssetClassPercentage(13.48).withAdjustedTarget(1251273408.83).withPhysicalExposure(1256069190.64).withOverlayExposure(0).withRebalanceTriggers(-301629123.87, 301629123.87).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 13193298.79));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(20, "Emerging Markets").withAssetClassPercentage(3).withAdjustedTarget(293153689.34).withPhysicalExposure(331438134.13).withOverlayExposure(0).withRebalanceTriggers(-101191706.07, 101191706.07).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4282124.17));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(23, "Mrkt. Neut. Hedge FOF").withAssetClassPercentage(12).withAdjustedTarget(1077349368.74).withPhysicalExposure(1077349368.74).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 19120902.61));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(24, "Private Equity-Domestic").withAssetClassPercentage(8).withAdjustedTarget(872645770.74).withPhysicalExposure(872645770.74).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 18142678.87));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(25, "Real Estate - Core").withAssetClassPercentage(7.5).withAdjustedTarget(699751037.88).withPhysicalExposure(699751037.88).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 5275363.45));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(26, "Real Estate - Non Core").withAssetClassPercentage(2.5).withAdjustedTarget(318589425.35).withPhysicalExposure(318589425.35));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(19, "Domestic Equity Small Cap").withAssetClassPercentage(1.93).withAdjustedTarget(179151163.13).withPhysicalExposure(172197383.63).withOverlayExposure(7666880.69).withRebalanceTriggers(-59352827.60, 59352827.60).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 799439.59));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2014, "Global Equity").withAssetClassPercentage(1.5).withAdjustedTarget(146576844.67).withPhysicalExposure(118282479.09).withOverlayExposure(28579199.88).withRebalanceTriggers(-32108906.74, 32108906.74).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1981487.67));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3410, "Fixed Income - EM").withAssetClassPercentage(5).withAdjustedTarget(488589482.24).withPhysicalExposure(417426768.55).withOverlayExposure(33926655.98).withRebalanceTriggers(-89515739.99, 89515739.99).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 17583379.36));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(22, "International Equity").withAssetClassPercentage(16.24).withAdjustedTarget(1586938638.28).withPhysicalExposure(1579104549.56).withOverlayExposure(34003154.82).withRebalanceTriggers(-403793827.12, 403793827.12).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 50025443.69));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2856, "Fixed Income - High Yield").withAssetClassPercentage(10).withAdjustedTarget(977178964.47).withPhysicalExposure(873321949.64).withOverlayExposure(54298049.60).withRebalanceTriggers(-119678652.38, 119678652.38).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 17448613.86));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(21, "Fixed Income").withAssetClassPercentage(10).withAdjustedTarget(977178964.47).withPhysicalExposure(757688762.63).withOverlayExposure(175840097.38).withRebalanceTriggers(-79785768.25, 79785768.25));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Mid Cap", 9850549.27, 8494651.33, 0.00, 0.00, 0.00, -66968664.95);
		validateZeroAssetClassAllocation(config, "Special Opportunities");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 22015221.52, 29742311.67, 0.00, 0.00, 0.00, -51757533.19);
		validateAssetClassAllocation(config, "Emerging Markets", 6245460.09, 6619208.83, 0.00, 0.00, 0.00, -12864668.92);
		validateZeroAssetClassAllocation(config, "Mrkt. Neut. Hedge FOF");
		validateZeroAssetClassAllocation(config, "Private Equity-Domestic");
		validateZeroAssetClassAllocation(config, "Real Estate - Core");
		validateZeroAssetClassAllocation(config, "Real Estate - Non Core");
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 2062519.03, 4258357.68, 0.00, 0.00, 0.00, 1346003.98);
		validateAssetClassAllocation(config, "Global Equity", 2963155.63, 3309604.41, 0.00, 0.00, 0.00, 22306439.84);
		validateAssetClassAllocation(config, "Fixed Income - EM", 20855605.89, 11032014.71, 0.00, 0.00, 0.00, 2866683.74);
		validateAssetClassAllocation(config, "International Equity", 60653635.47, 35831983.79, 0.00, 0.00, 0.00, -62482464.44);
		validateAssetClassAllocation(config, "Fixed Income - High Yield", 23993066.93, 22064029.43, 0.00, 0.00, 0.00, 9123458.14);
		validateAssetClassAllocation(config, "Fixed Income", 6544453.07, 22064029.43, 0.00, 0.00, 0.00, 158430745.79);

		validateTradingBandsMessage(config, "Total Effective Cash: 298,599,858.17 Total Overlay Exposure: 285,690,574.00 Difference: 12,909,284.17 No Trading Range: [-1,000,000.00] - [1,000,000.00] Difference is outside of no trading bands.  Asset Class Overlay Targets will be generated.");
		validateMinimizeImbalancesMessage(config, "Cash Available for Minimize Imbalances: 298,599,858.17");
	}


	@Test
	public void testOverweightWithTradingBandsAndNoCashBuffer() {
		// 221800: 1199 SEIU HEPF PIOS 5/11/2015 - Remove from Overweight
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(-1000000.00, 1000000.00, 0.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(16, "Cash").withAssetClassPercentage(0).withPhysicalExposure(280595376.77).withOverlayExposure(-281899298.33).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 137286365.31));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(18, "Domestic Equity Mid Cap").withAssetClassPercentage(3.85).withAdjustedTarget(352717623.76).withPhysicalExposure(446844181.07).withOverlayExposure(-47917659.03).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 7050342.19));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1237, "Special Opportunities").withAssetClassPercentage(5).withAdjustedTarget(503507780.00).withPhysicalExposure(503507780.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(17, "Domestic Equity Large Cap").withAssetClassPercentage(13.48).withAdjustedTarget(1234969758.02).withPhysicalExposure(1241784695.65).withOverlayExposure(0).withRebalanceTriggers(-298466357.62, 298466357.62).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 12628396.48));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(20, "Emerging Markets").withAssetClassPercentage(3).withAdjustedTarget(290416331.07).withPhysicalExposure(330999150.68).withOverlayExposure(0).withRebalanceTriggers(-100130649.01, 100130649.01).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 2625805.95));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(23, "Mrkt. Neut. Hedge FOF").withAssetClassPercentage(12).withAdjustedTarget(1059170610.52).withPhysicalExposure(1059170610.52).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 18743943.75));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(24, "Private Equity-Domestic").withAssetClassPercentage(8).withAdjustedTarget(870204338.53).withPhysicalExposure(870204338.53).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 15959582.57));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(25, "Real Estate - Core").withAssetClassPercentage(7.5).withAdjustedTarget(687541048.93).withPhysicalExposure(687541048.93).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4695220.08));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(26, "Real Estate - Non Core").withAssetClassPercentage(2.5).withAdjustedTarget(315137979.15).withPhysicalExposure(315137979.15));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(19, "Domestic Equity Small Cap").withAssetClassPercentage(1.93).withAdjustedTarget(176816886.73).withPhysicalExposure(169320193.39).withOverlayExposure(7540071.01).withRebalanceTriggers(-58730476.82, 58730476.82).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1187558.53));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2014, "Global Equity").withAssetClassPercentage(1.5).withAdjustedTarget(145208165.54).withPhysicalExposure(117878404.31).withOverlayExposure(29897001.02).withRebalanceTriggers(-31772225.17, 31772225.17).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 862253.21));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3410, "Fixed Income - EM").withAssetClassPercentage(5).withAdjustedTarget(484027218.45).withPhysicalExposure(413864247.89).withOverlayExposure(32589508.50).withRebalanceTriggers(-88577112.58, 88577112.58).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 20863186.70));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(22, "International Equity").withAssetClassPercentage(16.24).withAdjustedTarget(1572120405.45).withPhysicalExposure(1557259717.86).withOverlayExposure(34343664.23).withRebalanceTriggers(-399559801.33, 399559801.33).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 45526876.55));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2856, "Fixed Income - High Yield").withAssetClassPercentage(10).withAdjustedTarget(968054436.90).withPhysicalExposure(877175082.06).withOverlayExposure(52391931.44).withRebalanceTriggers(-118423748.35, 118423748.35).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 13165845.45));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(21, "Fixed Income").withAssetClassPercentage(10).withAdjustedTarget(968054436.90).withPhysicalExposure(756664213.14).withOverlayExposure(173054781.15).withRebalanceTriggers(-78949165.56, 78949165.56));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Mid Cap", 9383960.25, 8131577.02, 0.00, 0.00, 0.00, -65433196.30);
		validateZeroAssetClassAllocation(config, "Special Opportunities");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 20799090.35, 28471080.07, 0.00, 0.00, 0.00, -49270170.42);
		validateAssetClassAllocation(config, "Emerging Markets", 4444209.63, 6336293.78, 0.00, 0.00, 0.00, -10780503.41);
		validateZeroAssetClassAllocation(config, "Mrkt. Neut. Hedge FOF");
		validateZeroAssetClassAllocation(config, "Private Equity-Domestic");
		validateZeroAssetClassAllocation(config, "Real Estate - Core");
		validateZeroAssetClassAllocation(config, "Real Estate - Non Core");
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 2357398.23, 4076349.00, 0.00, 0.00, 0.00, 1106323.78);
		validateAssetClassAllocation(config, "Global Equity", 1771455.05, 3168146.89, 0.00, 0.00, 0.00, 23918346.19);
		validateAssetClassAllocation(config, "Fixed Income - EM", 23893859.50, 10560489.64, 0.00, 0.00, 0.00, -1864840.64);
		validateAssetClassAllocation(config, "International Equity", 55370501.80, 34300470.35, 0.00, 0.00, 0.00, -55592176.58);
		validateAssetClassAllocation(config, "Fixed Income - High Yield", 19227191.05, 21120979.28, 0.00, 0.00, 0.00, 12043761.11);
		validateAssetClassAllocation(config, "Fixed Income", 6061345.60, 21120979.28, 0.00, 0.00, 0.00, 145872456.27);

		validateTradingBandsMessage(config, "Total Effective Cash: 280,595,376.77 Total Overlay Exposure: 281,899,298.32 Difference: -1,303,921.55 No Trading Range: [-1,000,000.00] - [1,000,000.00] Difference is outside of no trading bands.  Asset Class Overlay Targets will be generated.");
		validateMinimizeImbalancesMessage(config, "Cash Available for Minimize Imbalances: 280,595,376.77");
	}


	@Test
	public void testOverweightWithNoTradingBandsAndNoCashBuffer() {
		// note: testing both main and moc - main worked correctly, but moc was incorrect using both as test cases to validate results
		// 221800: 1199 SEIU HEPF PIOS 11/29/2016 MAIN - Remove from Overweight
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();
		// Cash Available for Minimize Imbalances: Manager Cash: 162854949.35, Fund Cash: 232002755.67, Total: 394,857,705.02

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(16, "Cash").withAssetClassPercentage(0).withPhysicalExposure(394857705.01).withOverlayExposure(-436725986.59).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 232002755.67));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1237, "Special Opportunities").withAssetClassPercentage(5).withAdjustedTarget(528197283.48).withPhysicalExposure(528197283.48));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(20, "Emerging Markets").withAssetClassPercentage(5.5).withAdjustedTarget(546822099.33).withPhysicalExposure(544881308).withOverlayExposure(0).withRebalanceTriggers(-178658966.72, 178658966.72).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 8183551.61));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(23, "Mrkt. Neut. Hedge FOF").withAssetClassPercentage(12.5).withAdjustedTarget(921177691.44).withPhysicalExposure(921177691.44).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 49152349.32));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(24, "Private Equity-Domestic").withAssetClassPercentage(10).withAdjustedTarget(1002412522.86).withPhysicalExposure(1002412522.86).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4493122.73));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(25, "Real Estate - Core").withAssetClassPercentage(9.37).withAdjustedTarget(812827184.20).withPhysicalExposure(812827184.20).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 26847347.15));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(26, "Real Estate - Non Core").withAssetClassPercentage(3.13).withAdjustedTarget(325288722.95).withPhysicalExposure(325288722.95));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2856, "Fixed Income - High Yield").withAssetClassPercentage(5).withAdjustedTarget(497110999.39).withPhysicalExposure(534065658.00).withRebalanceTriggers(-57969132.82, 57969132.82).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 13265116.73));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(18, "Domestic Equity Mid Cap").withAssetClassPercentage(4).withAdjustedTarget(387269025.50).withPhysicalExposure(382688623.58).withOverlayExposure(26096900.76).withRebalanceTriggers(-95031365.27, 95031365.27).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4125684.48));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(19, "Domestic Equity Small Cap").withAssetClassPercentage(2).withAdjustedTarget(193634512.74).withPhysicalExposure(177421067.62).withOverlayExposure(31744434.10).withRebalanceTriggers(-55118191.86, 55118191.86).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1014822.55));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3410, "Fixed Income - EM").withAssetClassPercentage(5).withAdjustedTarget(497110999.39).withPhysicalExposure(431469590.18).withOverlayExposure(49299467.88).withRebalanceTriggers(-85528228.75, 85528228.75).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 22891659.57));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(17, "Domestic Equity Large Cap").withAssetClassPercentage(14).withAdjustedTarget(1355441589.21).withPhysicalExposure(1361216054.91).withOverlayExposure(49394262.85).withRebalanceTriggers(-304100368.88, 304100368.88).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 7521137.84));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(21, "Fixed Income").withAssetClassPercentage(10).withAdjustedTarget(994221998.77).withPhysicalExposure(889175078.27).withOverlayExposure(83427223.73).withRebalanceTriggers(-76975405.87, 76975405.87));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(22, "International Equity").withAssetClassPercentage(14.5).withAdjustedTarget(1441621898.23).withPhysicalExposure(1197458036.09).withOverlayExposure(196763697.28).withRebalanceTriggers(-337361346.73, 337361346.73).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 25360157.37));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Special Opportunities");
		validateAssetClassAllocation(config, "Emerging Markets", 15562060.04, 21266919.27, 0.00, 0.00, 0.00, -36828979.31);
		validateZeroAssetClassAllocation(config, "Mrkt. Neut. Hedge FOF");
		validateZeroAssetClassAllocation(config, "Private Equity-Domestic");
		validateZeroAssetClassAllocation(config, "Real Estate - Core");
		validateZeroAssetClassAllocation(config, "Real Estate - Non Core");
		validateAssetClassAllocation(config, "Fixed Income - High Yield", 19972851.66, 19333562.97, 0.00, 0.00, 0.00, -39306414.63);
		validateAssetClassAllocation(config, "Domestic Equity Mid Cap", 9491872.43, 15466850.38, 0.00, 0.00, 0.00, -9845280.14);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 3697916.52, 7733425.19, 0.00, 0.00, 0.00, 10891267.05);
		validateAssetClassAllocation(config, "Fixed Income - EM", 29599394.50, 19333562.97, 0.00, 0.00, 0.00, 366510.41);
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 26302795.65, 54133976.32, 0.00, 0.00, 0.00, -52505507.27);
		validateAssetClassAllocation(config, "Fixed Income", 13415469.87, 38667125.95, 0.00, 0.00, 0.00, 31344627.91);
		validateAssetClassAllocation(config, "International Equity", 44812588.68, 56067332.62, 0.00, 0.00, 0.00, 95883775.98);

		// 221800: 1199 SEIU HEPF PIOS 11/29/2016 MOC - Remove from Overweight
		config = setupProductOverlayRunConfig(null, null, null);
		// Cash Available for Minimize Imbalances: Manager Cash: 156854949.35, Fund Cash: 232002755.67, Total: 388,857,705.02

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(16, "Cash").withAssetClassPercentage(0).withPhysicalExposure(388857705.01).withOverlayExposure(-436725986.59).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 232002755.67));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1237, "Special Opportunities").withAssetClassPercentage(5).withAdjustedTarget(528197283.48).withPhysicalExposure(528197283.48));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(20, "Emerging Markets").withAssetClassPercentage(5.5).withAdjustedTarget(546272099.33).withPhysicalExposure(544881308.91).withOverlayExposure(0).withRebalanceTriggers(-178658966.72, 178658966.72).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 8183551.61));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(23, "Mrkt. Neut. Hedge FOF").withAssetClassPercentage(12.5).withAdjustedTarget(927177691.44).withPhysicalExposure(921177691.44).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 43152349.32));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(24, "Private Equity-Domestic").withAssetClassPercentage(10).withAdjustedTarget(1002412522.86).withPhysicalExposure(1002412522.86).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4493122.73));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(25, "Real Estate - Core").withAssetClassPercentage(9.37).withAdjustedTarget(812827184.20).withPhysicalExposure(812827184.20).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 26847347.15));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(26, "Real Estate - Non Core").withAssetClassPercentage(3.13).withAdjustedTarget(325288722.95).withPhysicalExposure(325288722.95));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2856, "Fixed Income - High Yield").withAssetClassPercentage(5).withAdjustedTarget(496610999.39).withPhysicalExposure(534065658.00).withOverlayExposure(0).withRebalanceTriggers(-57969132.82, 57969132.82).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 13265116.73));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(18, "Domestic Equity Mid Cap").withAssetClassPercentage(4).withAdjustedTarget(386869025.50).withPhysicalExposure(382688623.58).withOverlayExposure(26096900.76).withRebalanceTriggers(-95031365.27, 95031365.27).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4125684.48));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(19, "Domestic Equity Small Cap").withAssetClassPercentage(2).withAdjustedTarget(193434512.74).withPhysicalExposure(177421067.62).withOverlayExposure(31744434.10).withRebalanceTriggers(-55118191.86, 55118191.86).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1014822.55));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3410, "Fixed Income - EM").withAssetClassPercentage(5).withAdjustedTarget(496610999.39).withPhysicalExposure(431469590.18).withOverlayExposure(49299467.88).withRebalanceTriggers(-85528228.75, 85528228.75).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 22891659.57));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(17, "Domestic Equity Large Cap").withAssetClassPercentage(14).withAdjustedTarget(1354041589.21).withPhysicalExposure(1361216054.91).withOverlayExposure(49394262.85).withRebalanceTriggers(-304100368.88, 304100368.88).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 7521137.84));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(21, "Fixed Income").withAssetClassPercentage(10).withAdjustedTarget(993221998.77).withPhysicalExposure(889175078.27).withOverlayExposure(83427223.73).withRebalanceTriggers(-76975405.87, 76975405.87));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(22, "International Equity").withAssetClassPercentage(14.5).withAdjustedTarget(1440171898.23).withPhysicalExposure(1197458036.09).withOverlayExposure(196763697.28).withRebalanceTriggers(-337361346.73, 337361346.73).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 25360157.37));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Special Opportunities");
		validateAssetClassAllocation(config, "Emerging Markets", 15012060.04, 21266919.27, 0.00, 0.00, 0.00, -36278979.31);
		validateZeroAssetClassAllocation(config, "Mrkt. Neut. Hedge FOF");
		validateZeroAssetClassAllocation(config, "Private Equity-Domestic");
		validateZeroAssetClassAllocation(config, "Real Estate - Core");
		validateZeroAssetClassAllocation(config, "Real Estate - Non Core");
		validateAssetClassAllocation(config, "Fixed Income - High Yield", 19472851.66, 19333562.97, 0.00, 0.00, 0.00, -38806414.63);
		validateAssetClassAllocation(config, "Domestic Equity Mid Cap", 9091872.43, 15466850.38, 0.00, 0.00, 0.00, -10682100.22);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 3497916.52, 7733425.19, 0.00, 0.00, 0.00, 10405911.40);
		validateAssetClassAllocation(config, "Fixed Income - EM", 29099394.50, 19333562.97, 0.00, 0.00, 0.00, 866510.41);
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 24902795.65, 54133976.32, 0.00, 0.00, 0.00, -55183331.54);
		validateAssetClassAllocation(config, "Fixed Income", 12415469.87, 38667125.95, 0.00, 0.00, 0.00, 32344627.91);
		validateAssetClassAllocation(config, "International Equity", 43362588.68, 56067332.62, 0.00, 0.00, 0.00, 97333775.98);
	}


	@Test
	public void testUnderweightWithNoTradingBandsAndNoCashBuffer_SellMoreThanTotalDeviationFromTarget() {
		// 221800: 1199 SEIU HEPF PIOS 11/29/2017 MOC
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(-0.01, 0.01, null);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(16, "Cash").withAssetClassPercentage(0.00000).withCashPercentage(0.00000).withAdjustedTarget(0.00).withPhysicalExposure(421964034.27).withOverlayExposure(-487154833.62).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 265278692.77));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(17, "Domestic Equity Large Cap").withAssetClassPercentage(14.00000).withCashPercentage(0.00000).withAdjustedTarget(1598583565.63).withPhysicalExposure(1578181804.91).withOverlayExposure(40724447.22).withRebalanceTriggers(-344805512.97, 344805512.97).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 11855604.18));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(18, "Domestic Equity Mid Cap").withAssetClassPercentage(4.00000).withCashPercentage(0.00000).withAdjustedTarget(456738161.60).withPhysicalExposure(440229863.49).withOverlayExposure(23805264.13).withRebalanceTriggers(-108429406.60, 108429406.60).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 3854366.36));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(19, "Domestic Equity Small Cap").withAssetClassPercentage(2.00000).withCashPercentage(0.00000).withAdjustedTarget(228369080.81).withPhysicalExposure(201447748.67).withOverlayExposure(32250132.37).withRebalanceTriggers(-62889055.83, 62889055.83).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1543876.55));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(20, "Emerging Markets").withAssetClassPercentage(5.50000).withCashPercentage(0.00000).withAdjustedTarget(638654047.71).withPhysicalExposure(652811140.93).withOverlayExposure(0.00).withRebalanceTriggers(-201678696.27, 201678696.27).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 5617004.63));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(21, "Fixed Income").withAssetClassPercentage(10.00000).withCashPercentage(0.00000).withAdjustedTarget(1161189177.67).withPhysicalExposure(1040827402.74).withOverlayExposure(122337423.75).withRebalanceTriggers(-87827819.34, 87827819.34));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3410, "Fixed Income - EM").withAssetClassPercentage(5.00000).withCashPercentage(0.00000).withAdjustedTarget(580594588.84).withPhysicalExposure(494749987.08).withOverlayExposure(85390489.02).withRebalanceTriggers(-96502171.87, 96502171.87).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 24355090.80));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2856, "Fixed Income - High Yield").withAssetClassPercentage(5.00000).withCashPercentage(0.00000).withAdjustedTarget(580594588.84).withPhysicalExposure(518531139.15).withOverlayExposure(64446077.99).withRebalanceTriggers(-66141938.02, 66141938.02).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 18098036.87));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(22, "International Equity").withAssetClassPercentage(14.50000).withCashPercentage(0.00000).withAdjustedTarget(1683724307.59).withPhysicalExposure(1579704397.46).withOverlayExposure(118200999.15).withRebalanceTriggers(-381671511.22, 381671511.22).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 20498814.84));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(23, "Mrkt. Neut. Hedge FOF").withAssetClassPercentage(12.50000).withCashPercentage(0.00000).withAdjustedTarget(954091644.62).withPhysicalExposure(954091644.62).withOverlayExposure(0.00).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 37508301.01));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(24, "Private Equity-Domestic").withAssetClassPercentage(10.00000).withCashPercentage(0.00000).withAdjustedTarget(1122981613.25).withPhysicalExposure(1122981613.25).withOverlayExposure(0.00).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 16523784.17));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(25, "Real Estate - Core").withAssetClassPercentage(9.37000).withCashPercentage(0.00000).withAdjustedTarget(898252229.94).withPhysicalExposure(898252229.94).withOverlayExposure(0.00).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 16830462.09));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(26, "Real Estate - Non Core").withAssetClassPercentage(3.13000).withCashPercentage(0.00000).withAdjustedTarget(351403932.18).withPhysicalExposure(351403932.18).withOverlayExposure(0.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1237, "Special Opportunities").withAssetClassPercentage(5.00000).withCashPercentage(0.00000).withAdjustedTarget(587763720.89).withPhysicalExposure(587763720.89).withOverlayExposure(0.00));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 28390198.54, 61898361.65, 0.00, 0, 0, -74137937.94);
		validateAssetClassAllocation(config, "Domestic Equity Mid Cap", 8578536.18, 17685246.18, 0.00, 0, 0, -11092320.25);
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 3905961.46, 8842623.09, 0.00, 0, 0, 13397382.70);
		validateAssetClassAllocation(config, "Emerging Markets", 12112738.13, 24317213.50, 0, 0, 0, -36429951.63);
		validateAssetClassAllocation(config, "Fixed Income", 11810424.55, 44213115.46, 0.00, 0, 0, 63255397.76);
		validateAssetClassAllocation(config, "Fixed Income - EM", 30260303.07, 22106557.73, 0.00, 0, 0, 32287956.92);
		validateAssetClassAllocation(config, "Fixed Income - High Yield", 24003249.14, 22106557.73, 0.00, 0, 0, 15138172.86);
		validateAssetClassAllocation(config, "International Equity", 37623930.43, 64109017.42, 0.00, 0, 0, -2418700.42);
		validateZeroAssetClassAllocation(config, "Mrkt. Neut. Hedge FOF");
		validateZeroAssetClassAllocation(config, "Private Equity-Domestic");
		validateZeroAssetClassAllocation(config, "Real Estate - Core");
		validateZeroAssetClassAllocation(config, "Real Estate - Non Core");
		validateZeroAssetClassAllocation(config, "Special Opportunities");
	}


	@Test
	public void testUnderweight_NoCashBuffer() {
		// 221700 6/11/2015
		ProductOverlayRunConfig config = setupProductOverlayRunConfig();
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1, "Cash").withAssetClassPercentage(0).withPhysicalExposure(12778346.63).withOverlayExposure(-11820365.48).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 11346532.75));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1252, "Special Opportunities").withAssetClassPercentage(5).withAdjustedTarget(32664675.03).withPhysicalExposure(32664675.03));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(20, "Emerging Markets").withAssetClassPercentage(5).withAdjustedTarget(26743456.64).withPhysicalExposure(28801242.05).withOverlayExposure(0).withRebalanceTriggers(-9782205.76, 9782205.76).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 751478.87));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2, "Domestic Equity Large Cap").withAssetClassPercentage(17.6).withAdjustedTarget(91116545.44).withPhysicalExposure(90970270.49).withOverlayExposure(0).withRebalanceTriggers(-22900539.50, 22900539.50).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 214427.57));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2855, "Fixed Income - High Yield").withAssetClassPercentage(10).withAdjustedTarget(53486913.28).withPhysicalExposure(53912756.12).withOverlayExposure(0).withRebalanceTriggers(-6954978.66, 6954978.66));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(6, "Mrkt. Neut. Hedge FOF").withAssetClassPercentage(13).withAdjustedTarget(69603491.00).withPhysicalExposure(69603491.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(7, "Private Equity-Domestic").withAssetClassPercentage(8).withAdjustedTarget(49011161.00).withPhysicalExposure(49011161.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(8, "Real Estate").withAssetClassPercentage(5).withAdjustedTarget(48881918.42).withPhysicalExposure(48881918.42));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3, "Domestic Equity Small Cap").withAssetClassPercentage(4.4).withAdjustedTarget(22779136.36).withPhysicalExposure(21620387.71).withOverlayExposure(888244.00).withRebalanceTriggers(-7803146.79, 7803146.79).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 465907.44));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3409, "Fixed Income - EM").withAssetClassPercentage(5).withAdjustedTarget(26743456.64).withPhysicalExposure(24657713.36).withOverlayExposure(1345593.62).withRebalanceTriggers(-5202097.86, 5202097.86));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(4, "Fixed Income").withAssetClassPercentage(10).withAdjustedTarget(53486913.28).withPhysicalExposure(50497875.29).withOverlayExposure(2011342.16).withRebalanceTriggers(-4636652.44, 4636652.44));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(5, "International Equity").withAssetClassPercentage(17).withAdjustedTarget(90927752.57).withPhysicalExposure(82045582.56).withOverlayExposure(7575185.70).withRebalanceTriggers(-23465984.92, 23465984.92));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateZeroAssetClassAllocation(config, "Special Opportunities");
		validateAssetClassAllocation(config, "Emerging Markets", 751478.87, 822212.52, 0.00, 0.00, 0.00, -1573691.39);
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 214427.57, 2894188.06, 0.00, 0.00, 0.00, -3108615.63);
		validateAssetClassAllocation(config, "Fixed Income - High Yield", 0.00, 1644425.04, 0.00, 0.00, 0.00, -1644425.04);
		validateZeroAssetClassAllocation(config, "Mrkt. Neut. Hedge FOF");
		validateZeroAssetClassAllocation(config, "Private Equity-Domestic");
		validateZeroAssetClassAllocation(config, "Real Estate");
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 465907.44, 723547.02, 0.00, 0.00, 0.00, -301210.46);
		validateAssetClassAllocation(config, "Fixed Income - EM", 0.00, 822212.52, 0.00, 0.00, 0.00, 861763.41);
		validateAssetClassAllocation(config, "Fixed Income", 0.00, 1644425.04, 0.00, 0.00, 0.00, 986515.96);
		validateAssetClassAllocation(config, "International Equity", 0.00, 2795522.56, 0.00, 0.00, 0.00, 4779663.14);
	}
}
