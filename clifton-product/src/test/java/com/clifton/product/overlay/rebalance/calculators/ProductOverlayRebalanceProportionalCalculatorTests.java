package com.clifton.product.overlay.rebalance.calculators;


import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;
import com.clifton.product.overlay.rebalance.ProductOverlayRebalance;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


@ContextConfiguration(locations = "ProductOverlayRebalanceCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ProductOverlayRebalanceProportionalCalculatorTests extends BaseProductOverlayRebalanceCalculatorTests {

	@Override
	public RebalanceCalculationTypes getRebalanceCalculationType() {
		return RebalanceCalculationTypes.PROPORTIONAL_REBALANCE_CASH;
	}


	@Test
	public void testMiniRebalanceProportional_1() {
		Map<String, BigDecimal> resultMap = new HashMap<>();
		ProductOverlayRebalance rebalance = createProductOverlayRebalance();

		addAssetClassToList(rebalance, resultMap, "Alternative Assets", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Cash", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Domestic Equity Large Cap", 19.0700, 19326900.36, 19446932.35);
		addAssetClassToList(rebalance, resultMap, "Domestic Equity Small Cap", 5.2000, -581837.70, -578224.13);
		addAssetClassToList(rebalance, resultMap, "International Equity", 27.7300, 24870923.10, 25025386.90);
		addAssetClassToList(rebalance, resultMap, "Fixed Income", 48.0000, -44168408.23, -43894095.12);

		calculateNewRebalanceCash(rebalance);
		validateResults(rebalance, resultMap);
	}


	@Test
	public void testMiniRebalanceProportional_2() {
		Map<String, BigDecimal> resultMap = new HashMap<>();
		ProductOverlayRebalance rebalance = createProductOverlayRebalance();

		addAssetClassToList(rebalance, resultMap, "Fixed Income", 60.5000, 34906960.12, 34842244.42);
		addAssetClassToList(rebalance, resultMap, "International Equity", 9.5000, 6187981.49, 6176509.29);
		addAssetClassToList(rebalance, resultMap, "Commodities", 4.0000, 3297857.00, 3291742.94);
		addAssetClassToList(rebalance, resultMap, "Private Equity", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Hedge Funds", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Diversified Beta", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Cash", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Real Estate", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Domestic Equity Mid Cap", 4.0000, -3842601.02, -3849725.00);
		addAssetClassToList(rebalance, resultMap, "Domestic Equity Small Cap", 4.0000, -18852899.18, -18887851.48);
		addAssetClassToList(rebalance, resultMap, "Domestic Equity Large Cap", 18.0000, -21532999.11, -21572920.18);

		calculateNewRebalanceCash(rebalance);
		validateResults(rebalance, resultMap);
	}


	@Test
	public void testMiniRebalanceProportional_3() {
		Map<String, BigDecimal> resultMap = new HashMap<>();
		ProductOverlayRebalance rebalance = createProductOverlayRebalance();

		addAssetClassToList(rebalance, resultMap, "Fixed Income", 20.0000, -16266116.02, -16209324.31);
		addAssetClassToList(rebalance, resultMap, "Emerging Market Currency", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Emerging Markets", 13.7900, 3011529.04, 3022043.53);
		addAssetClassToList(rebalance, resultMap, "Cash", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Alternative Beta", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Alternative Credit", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Hedge Funds", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Illiquid Assets", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Emerging Market Debt", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Equity - Developed", 66.2100, 13141398.75, 13187280.78);

		calculateNewRebalanceCash(rebalance);
		validateResults(rebalance, resultMap);
	}
}
