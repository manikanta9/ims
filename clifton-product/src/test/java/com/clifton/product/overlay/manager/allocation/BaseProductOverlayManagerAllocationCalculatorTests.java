package com.clifton.product.overlay.manager.allocation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.manager.InvestmentManagerAllocationConfigTypes;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>BaseProductOverlayManagerAllocationCalculatorTests</code> is a base class that can be used for each test class
 * that needs to test the {@link ProductOverlayManagerAllocationCalculator}
 * <p>
 * These tests are broken out into separate classes as each class should contain the data for a single run / account setup
 * The tests themselves can manipulate the data for different test cases if needed, but because of the context setup each run should be
 * loaded into a separate test context
 *
 * @author manderson
 */
public abstract class BaseProductOverlayManagerAllocationCalculatorTests {

	@Resource
	private InvestmentAccountAssetClassService investmentAccountAssetClassService;

	@Resource
	private PortfolioRunService portfolioRunService;

	@Resource
	private ProductOverlayManagerAllocationCalculator productOverlayManagerAllocationCalculator;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected String generateResultForRun(int runId) {
		PortfolioRun portfolioRun = this.portfolioRunService.getPortfolioRun(runId);
		ProductOverlayRunConfig productOverlayRunConfig = new ProductOverlayRunConfig(portfolioRun, false, 0);
		productOverlayRunConfig.setAssetClassList(this.investmentAccountAssetClassService.getInvestmentAccountAssetClassListByAccount(portfolioRun.getClientInvestmentAccount().getId()));
		this.productOverlayManagerAllocationCalculator.processProductOverlayManagerAllocationConfigList(productOverlayRunConfig, false);
		List<ProductOverlayManagerAllocationConfig> configList = BeanUtils.sortWithFunction(productOverlayRunConfig.getManagerAllocationConfigList(), this::getSortString, true);
		return StringUtils.collectionToCommaDelimitedString(configList, this::generateStringForProductOverlayManagerAllocationConfig);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private String generateStringForProductOverlayManagerAllocationConfig(ProductOverlayManagerAllocationConfig config) {
		StringBuilder str = new StringBuilder(config.getManagerAccountAssignment().getManagerAccountLabel());
		str.append(StringUtils.TAB);
		str.append(config.getAccountAssetClass().getLabel());
		str.append(StringUtils.TAB);
		str.append(config.getConfigType().name());
		str.append(StringUtils.TAB);
		str.append(CoreMathUtils.formatNumberMoney(config.getAmount()));
		str.append(StringUtils.TAB);
		str.append(CoreMathUtils.formatNumberDecimal(config.getAllocationPercent()));
		str.append(StringUtils.TAB);
		if (config.getConfigType() == InvestmentManagerAllocationConfigTypes.OVERLAY_ALLOCATION) {
			str.append(config.getOverlayAdjustmentAction() != null ? "Adjustment Action" : "No Adjustment Action");
			str.append(StringUtils.TAB);
			str.append(CoreMathUtils.formatNumberMoney(config.getOverlayAdjustment()));
			str.append(StringUtils.TAB);


			str.append(config.getOverlayLimitAction() != null ? "Limit Action" : "No Limit Action");
			str.append(StringUtils.TAB);
			str.append(CoreMathUtils.formatNumberMoney(config.getOverlayLimit()));
			str.append(StringUtils.TAB);
		}
		//System.out.println(str.toString() + ",");
		return str.toString();
	}


	private String getSortString(ProductOverlayManagerAllocationConfig config) {
		return config.getAccountAssetClass().getOrder() + "_" + config.getManagerAccountAssignment().getReferenceOne().getAccountNumber() + "_" + config.getConfigType() + "_" + config.getOrder();
	}
}
