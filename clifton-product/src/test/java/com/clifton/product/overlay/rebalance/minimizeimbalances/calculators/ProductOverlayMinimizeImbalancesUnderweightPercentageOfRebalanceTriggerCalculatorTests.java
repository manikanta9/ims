package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;


import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * WARNING: THESE WERE ORIGINAL TESTS FOR 221800 ACCOUNT, HOWEVER AFTER TESTING REALIZED THERE WAS EXTRA RESTRICTION TO NOT EXPAND SHORTS.
 * ADDED NEW TEST CLASS FOR DO NOT EXPAND SHORT OPTION, BUT STILL LEAVING THIS TEST CLASS EVEN THOUGH DOESN'T FULLY MATCH ACTUAL RESULTS EXPECTED FOR THE ACCOUNT.
 */
@ContextConfiguration("ProductOverlayMinimizeImbalancesCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ProductOverlayMinimizeImbalancesUnderweightPercentageOfRebalanceTriggerCalculatorTests extends BaseProductOverlayMinimizeImbalancesCalculatorTests {

	@Override
	public ProductOverlayMinimizeImbalancesCalculator getCalculator() {
		ProductOverlayMinimizeImbalancesUnderweightCalculator calculator = new ProductOverlayMinimizeImbalancesUnderweightCalculator();
		calculator.setShortTargetsAllowed(true);
		calculator.setExpandShortPositionsAllowed(true);
		calculator.setAttributionRebalancing(true);
		calculator.setDeviationPercentageDenominator(BaseProductOverlayMinimizeImbalancesCalculatorImpl.DEVIATION_PERCENTAGE_DENOMINATOR_REBAL_TRIGGER);
		return calculator;
	}


	@Test
	public void testWithTradingBandsAndNoCashBuffer_NoTrades() {
		// 221800: 1199 SEIU HEPF PIOS 5/22/2015 - No Trades
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(-1000000.00, 1000000.00, 0.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(16, "Cash").withAssetClassPercentage(0).withPhysicalExposure(294536725.80).withOverlayExposure(-293578292.81).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 143526493.34));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(18, "Domestic Equity Mid Cap").withAssetClassPercentage(3.85).withAdjustedTarget(356745331.91).withPhysicalExposure(453545597.58).withOverlayExposure(-48549283.93).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 6802520.15));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1237, "Special Opportunities").withAssetClassPercentage(5).withAdjustedTarget(503996579.31).withPhysicalExposure(503996579.31));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(17, "Domestic Equity Large Cap").withAssetClassPercentage(13.48).withAdjustedTarget(1249071967.35).withPhysicalExposure(1257713609.88).withOverlayExposure(0).withRebalanceTriggers(-301391031.73, 301391031.73).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 10709517.62));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(20, "Emerging Markets").withAssetClassPercentage(3).withAdjustedTarget(292758558.05).withPhysicalExposure(330773032.23).withOverlayExposure(0).withRebalanceTriggers(-101111830.00, 101111830.00).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4566366.49));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(23, "Mrkt. Neut. Hedge FOF").withAssetClassPercentage(12).withAdjustedTarget(1077347021.53).withPhysicalExposure(1077347021.53).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 19118927.20));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(24, "Private Equity-Domestic").withAssetClassPercentage(8).withAdjustedTarget(872639979.53).withPhysicalExposure(872639979.53).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 18092683.83));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(25, "Real Estate - Core").withAssetClassPercentage(7.5).withAdjustedTarget(699710750.58).withPhysicalExposure(699710750.58).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4350348.28));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(26, "Real Estate - Non Core").withAssetClassPercentage(2.5).withAdjustedTarget(320351595.82).withPhysicalExposure(320351595.82));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(19, "Domestic Equity Small Cap").withAssetClassPercentage(1.93).withAdjustedTarget(178835971.59).withPhysicalExposure(171617541.95).withOverlayExposure(7639305.93).withRebalanceTriggers(-59305977.21, 59305977.21).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 750860.30));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2014, "Global Equity").withAssetClassPercentage(1.5).withAdjustedTarget(146379279.02).withPhysicalExposure(118103604.79).withOverlayExposure(28461960.35).withRebalanceTriggers(-32083561.44, 32083561.44).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1912704.13));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3410, "Fixed Income - EM").withAssetClassPercentage(5).withAdjustedTarget(487930930.08).withPhysicalExposure(418247704.23).withOverlayExposure(33584390.93).withRebalanceTriggers(-89445080.38, 89445080.38).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 17087441.38));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(22, "International Equity").withAssetClassPercentage(16.24).withAdjustedTarget(1584799660.92).withPhysicalExposure(1573628525.98).withOverlayExposure(33773489.85).withRebalanceTriggers(-403475090.86, 403475090.86).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 50244958.41));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2856, "Fixed Income - High Yield").withAssetClassPercentage(10).withAdjustedTarget(975861860.18).withPhysicalExposure(873864296.49).withOverlayExposure(53871370.02).withRebalanceTriggers(-119584183.56, 119584183.56).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 17373904.67));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(21, "Fixed Income").withAssetClassPercentage(10).withAdjustedTarget(975861860.18).withPhysicalExposure(756214780.35).withOverlayExposure(184797059.67).withRebalanceTriggers(-79722789.04, 79722789.04));


		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Mid Cap", 9264266.97, 8501184.61, 0.00, 0.00, 0.00, -66314735.51);
		validateZeroAssetClassAllocation(config, "Special Opportunities");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 19328828.57, 29765186.62, 0.00, 0.00, 0.00, -49094015.19);
		validateAssetClassAllocation(config, "Emerging Markets", 6484610.77, 6624299.69, 0.00, 0.00, 0.00, -13108910.46);
		validateZeroAssetClassAllocation(config, "Mrkt. Neut. Hedge FOF");
		validateZeroAssetClassAllocation(config, "Private Equity-Domestic");
		validateZeroAssetClassAllocation(config, "Real Estate - Core");
		validateZeroAssetClassAllocation(config, "Real Estate - Non Core");
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 1984930.78, 4261632.80, 0.00, 0.00, 0.00, 1392742.35);
		validateAssetClassAllocation(config, "Global Equity", 2871826.27, 3312149.85, 0.00, 0.00, 0.00, 22277984.23);
		validateAssetClassAllocation(config, "Fixed Income - EM", 20284515.17, 11040499.49, 0.00, 0.00, 0.00, 2259376.27);
		validateAssetClassAllocation(config, "International Equity", 60629054.09, 35859542.34, 0.00, 0.00, 0.00, -62715106.58);
		validateAssetClassAllocation(config, "Fixed Income - High Yield", 23768052.26, 22080998.98, 0.00, 0.00, 0.00, 8022318.78);
		validateAssetClassAllocation(config, "Fixed Income", 6394147.59, 22080998.98, 0.00, 0.00, 0.00, 156321913.10);

		validateTradingBandsMessage(config, "Total Effective Cash: 294,536,725.80 Total Overlay Exposure: 293,578,292.82 Difference: 958,432.98 No Trading Range: [-1,000,000.00] - [1,000,000.00] Difference is inside of no trading bands.  Asset Class Overlay Targets will be set to Overlay Exposure.");
		validateMinimizeImbalancesMessage(config, null);
	}


	@Test
	public void testUnderweightWithTradingBandsAndNoCashBuffer() {
		// 221800: 1199 SEIU HEPF PIOS 5/21/2015 - Add to Underweight
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(-1000000.00, 1000000.00, 0.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(16, "Cash").withAssetClassPercentage(0).withPhysicalExposure(298599858.17).withOverlayExposure(-285690574.00).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 143416191.27));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(18, "Domestic Equity Mid Cap").withAssetClassPercentage(3.85).withAdjustedTarget(357374081.89).withPhysicalExposure(453286161.28).withOverlayExposure(-48623464.35).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 7330934.84).withRebalanceTriggers(-94380725.86, 94380725.86));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1237, "Special Opportunities").withAssetClassPercentage(5).withAdjustedTarget(504220897.87).withPhysicalExposure(504220897.87));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(17, "Domestic Equity Large Cap").withAssetClassPercentage(13.48).withAdjustedTarget(1251273408.83).withPhysicalExposure(1256069190.64).withOverlayExposure(0).withRebalanceTriggers(-301629123.87, 301629123.87).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 13193298.79));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(20, "Emerging Markets").withAssetClassPercentage(3).withAdjustedTarget(293153689.34).withPhysicalExposure(331438134.13).withOverlayExposure(0).withRebalanceTriggers(-101191706.07, 101191706.07).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4282124.17));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(23, "Mrkt. Neut. Hedge FOF").withAssetClassPercentage(12).withAdjustedTarget(1077349368.74).withPhysicalExposure(1077349368.74).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 19120902.61));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(24, "Private Equity-Domestic").withAssetClassPercentage(8).withAdjustedTarget(872645770.74).withPhysicalExposure(872645770.74).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 18142678.87));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(25, "Real Estate - Core").withAssetClassPercentage(7.5).withAdjustedTarget(699751037.88).withPhysicalExposure(699751037.88).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 5275363.45));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(26, "Real Estate - Non Core").withAssetClassPercentage(2.5).withAdjustedTarget(318589425.35).withPhysicalExposure(318589425.35));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(19, "Domestic Equity Small Cap").withAssetClassPercentage(1.93).withAdjustedTarget(179151163.13).withPhysicalExposure(172197383.63).withOverlayExposure(7666880.69).withRebalanceTriggers(-59352827.60, 59352827.60).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 799439.59));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2014, "Global Equity").withAssetClassPercentage(1.5).withAdjustedTarget(146576844.67).withPhysicalExposure(118282479.09).withOverlayExposure(28579199.88).withRebalanceTriggers(-32108906.74, 32108906.74).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1981487.67));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3410, "Fixed Income - EM").withAssetClassPercentage(5).withAdjustedTarget(488589482.24).withPhysicalExposure(417426768.55).withOverlayExposure(33926655.98).withRebalanceTriggers(-89515739.99, 89515739.99).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 17583379.36));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(22, "International Equity").withAssetClassPercentage(16.24).withAdjustedTarget(1586938638.28).withPhysicalExposure(1579104549.56).withOverlayExposure(34003154.82).withRebalanceTriggers(-403793827.12, 403793827.12).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 50025443.69));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2856, "Fixed Income - High Yield").withAssetClassPercentage(10).withAdjustedTarget(977178964.47).withPhysicalExposure(873321949.64).withOverlayExposure(54298049.60).withRebalanceTriggers(-119678652.38, 119678652.38).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 17448613.86));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(21, "Fixed Income").withAssetClassPercentage(10).withAdjustedTarget(977178964.47).withPhysicalExposure(757688762.63).withOverlayExposure(175840097.38).withRebalanceTriggers(-79785768.25, 79785768.25));


		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Mid Cap", 9850549.27, 8494651.33, 0.00, 0.00, 0.00, -66968664.95);
		validateZeroAssetClassAllocation(config, "Special Opportunities");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 22015221.52, 29742311.67, 0.00, 0.00, 0.00, -51757533.19);
		validateAssetClassAllocation(config, "Emerging Markets", 6245460.09, 6619208.83, 0.00, 0.00, 0.00, -12864668.92);
		validateZeroAssetClassAllocation(config, "Mrkt. Neut. Hedge FOF");
		validateZeroAssetClassAllocation(config, "Private Equity-Domestic");
		validateZeroAssetClassAllocation(config, "Real Estate - Core");
		validateZeroAssetClassAllocation(config, "Real Estate - Non Core");
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 2062519.03, 4258357.68, 0.00, 0.00, 0.00, 1346003.98);
		validateAssetClassAllocation(config, "Global Equity", 2963155.63, 3309604.41, 0.00, 0.00, 0.00, 22306439.84);
		validateAssetClassAllocation(config, "Fixed Income - EM", 20855605.89, 11032014.71, 0.00, 0.00, 0.00, 2866683.74);
		validateAssetClassAllocation(config, "International Equity", 60653635.47, 35831983.79, 0.00, 0.00, 0.00, -62482464.44);
		validateAssetClassAllocation(config, "Fixed Income - High Yield", 23993066.93, 22064029.43, 0.00, 0.00, 0.00, 9123458.14);
		validateAssetClassAllocation(config, "Fixed Income", 6544453.07, 22064029.43, 0.00, 0.00, 0.00, 158430745.79);

		validateTradingBandsMessage(config, "Total Effective Cash: 298,599,858.17 Total Overlay Exposure: 285,690,574.00 Difference: 12,909,284.17 No Trading Range: [-1,000,000.00] - [1,000,000.00] Difference is outside of no trading bands.  Asset Class Overlay Targets will be generated.");
		validateMinimizeImbalancesMessage(config, "Cash Available for Minimize Imbalances: 298,599,858.17");
	}


	@Test
	public void testOverweightWithTradingBandsAndNoCashBuffer() {
		// 221800: 1199 SEIU HEPF PIOS 5/11/2015 - Remove from Overweight
		ProductOverlayRunConfig config = setupProductOverlayRunConfig(-1000000.00, 1000000.00, 0.00);

		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(16, "Cash").withAssetClassPercentage(0).withPhysicalExposure(280595376.77).withOverlayExposure(-281899298.33).addMinimizeImbalancesCash(ManagerCashTypes.FUND, 137286365.31));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(18, "Domestic Equity Mid Cap").withAssetClassPercentage(3.85).withAdjustedTarget(352717623.76).withPhysicalExposure(446844181.07).withOverlayExposure(-47917659.03).withRebalanceTriggers(-93391086.09, 93391086.09).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 7050342.19));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(1237, "Special Opportunities").withAssetClassPercentage(5).withAdjustedTarget(503507780.00).withPhysicalExposure(503507780.00));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(17, "Domestic Equity Large Cap").withAssetClassPercentage(13.48).withAdjustedTarget(1234969758.02).withPhysicalExposure(1241784695.65).withOverlayExposure(0).withRebalanceTriggers(-298466357.62, 298466357.62).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 12628396.48));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(20, "Emerging Markets").withAssetClassPercentage(3).withAdjustedTarget(290416331.07).withPhysicalExposure(330999150.68).withOverlayExposure(0).withRebalanceTriggers(-100130649.01, 100130649.01).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 2625805.95));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(23, "Mrkt. Neut. Hedge FOF").withAssetClassPercentage(12).withAdjustedTarget(1059170610.52).withPhysicalExposure(1059170610.52).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 18743943.75));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(24, "Private Equity-Domestic").withAssetClassPercentage(8).withAdjustedTarget(870204338.53).withPhysicalExposure(870204338.53).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 15959582.57));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(25, "Real Estate - Core").withAssetClassPercentage(7.5).withAdjustedTarget(687541048.93).withPhysicalExposure(687541048.93).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 4695220.08));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClass(26, "Real Estate - Non Core").withAssetClassPercentage(2.5).withAdjustedTarget(315137979.15).withPhysicalExposure(315137979.15));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(19, "Domestic Equity Small Cap").withAssetClassPercentage(1.93).withAdjustedTarget(176816886.73).withPhysicalExposure(169320193.39).withOverlayExposure(7540071.01).withRebalanceTriggers(-58730476.82, 58730476.82).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 1187558.53));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2014, "Global Equity").withAssetClassPercentage(1.5).withAdjustedTarget(145208165.54).withPhysicalExposure(117878404.31).withOverlayExposure(29897001.02).withRebalanceTriggers(-31772225.17, 31772225.17).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 862253.21));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(3410, "Fixed Income - EM").withAssetClassPercentage(5).withAdjustedTarget(484027218.45).withPhysicalExposure(413864247.89).withOverlayExposure(32589508.50).withRebalanceTriggers(-88577112.58, 88577112.58).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 20863186.70));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(22, "International Equity").withAssetClassPercentage(16.24).withAdjustedTarget(1572120405.45).withPhysicalExposure(1557259717.86).withOverlayExposure(34343664.23).withRebalanceTriggers(-399559801.33, 399559801.33).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 45526876.55));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(2856, "Fixed Income - High Yield").withAssetClassPercentage(10).withAdjustedTarget(968054436.90).withPhysicalExposure(877175082.06).withOverlayExposure(52391931.44).withRebalanceTriggers(-118423748.35, 118423748.35).addMinimizeImbalancesCash(ManagerCashTypes.MANAGER, 13165845.45));
		addAssetClassToMap(config, ProductOverlayAssetClassBuilder.newAssetClassWithReplications(21, "Fixed Income").withAssetClassPercentage(10).withAdjustedTarget(968054436.90).withPhysicalExposure(756664213.14).withOverlayExposure(173054781.15).withRebalanceTriggers(-78949165.56, 78949165.56));

		processCalculation(config);

		validateZeroAssetClassAllocation(config, "Cash");
		validateAssetClassAllocation(config, "Domestic Equity Mid Cap", 9383960.25, 8131577.02, 0.00, 0.00, 0.00, -66737117.85);
		validateZeroAssetClassAllocation(config, "Special Opportunities");
		validateAssetClassAllocation(config, "Domestic Equity Large Cap", 20799090.35, 28471080.07, 0.00, 0.00, 0.00, -49270170.42);
		validateAssetClassAllocation(config, "Emerging Markets", 4444209.63, 6336293.78, 0.00, 0.00, 0.00, -10780503.41);
		validateZeroAssetClassAllocation(config, "Mrkt. Neut. Hedge FOF");
		validateZeroAssetClassAllocation(config, "Private Equity-Domestic");
		validateZeroAssetClassAllocation(config, "Real Estate - Core");
		validateZeroAssetClassAllocation(config, "Real Estate - Non Core");
		validateAssetClassAllocation(config, "Domestic Equity Small Cap", 2357398.23, 4076349.00, 0.00, 0.00, 0.00, 1106323.78);
		validateAssetClassAllocation(config, "Global Equity", 1771455.05, 3168146.89, 0.00, 0.00, 0.00, 24957399.08);
		validateAssetClassAllocation(config, "Fixed Income - EM", 23893859.50, 10560489.64, 0.00, 0.00, 0.00, -1864840.64);
		validateAssetClassAllocation(config, "International Equity", 55370501.80, 34300470.35, 0.00, 0.00, 0.00, -55327307.92);
		validateAssetClassAllocation(config, "Fixed Income - High Yield", 19227191.05, 21120979.28, 0.00, 0.00, 0.00, 12043761.11);
		validateAssetClassAllocation(config, "Fixed Income", 6061345.60, 21120979.28, 0.00, 0.00, 0.00, 145872456.27);

		validateTradingBandsMessage(config, "Total Effective Cash: 280,595,376.77 Total Overlay Exposure: 281,899,298.32 Difference: -1,303,921.55 No Trading Range: [-1,000,000.00] - [1,000,000.00] Difference is outside of no trading bands.  Asset Class Overlay Targets will be generated.");
		validateMinimizeImbalancesMessage(config, "Cash Available for Minimize Imbalances: 280,595,376.77");
	}
}
