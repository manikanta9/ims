package com.clifton.product.overlay.rebalance.minimizeimbalances.calculators;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalance;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceAssetClassHistory;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceHistory;
import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.process.ProductOverlayRunConfig;
import com.clifton.product.util.ProductUtilService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>BaseProductOverlayMinimizeImbalancesCalculatorTests</code>
 * used by Minimize Imbalances Tests to setup/calculate/validate test cases
 * <p/>
 * Doesn't have any real tests, extended by test classes for the different calculators
 *
 * @author manderson
 */
public abstract class BaseProductOverlayMinimizeImbalancesCalculatorTests {

	public abstract ProductOverlayMinimizeImbalancesCalculator getCalculator();


	@Resource
	private ProductUtilService productUtilService;

	@Resource
	private SystemColumnValueHandler systemColumnValueHandler;


	@SuppressWarnings("unchecked")
	@BeforeEach
	protected void cleanUpMockitoCalls() {
		// Instead of using dirties context for each test which slows all these tests down, just reset the mock
		// this way mocked calls from one test don't interfere with mocks from other tests
		Mockito.reset(this.systemColumnValueHandler);
	}


	////////////////////////////////////////////////////////
	////////           Test Setup Methods            ///////
	////////////////////////////////////////////////////////


	protected ProductOverlayRunConfig setupProductOverlayRunConfig() {
		return setupProductOverlayRunConfig(null, null, null);
	}


	protected ProductOverlayRunConfig setupProductOverlayRunConfig(Double tradingMinEffectiveCash, Double tradingMaxEffectiveCash,
	                                                               Double minimizeCashAdjustment) {
		PortfolioRun run = new PortfolioRun();
		run.setBalanceDate(new Date());

		InvestmentAccount account = new InvestmentAccount(1, "Test Account");
		account.setNumber("000001");
		run.setClientInvestmentAccount(account);

		ProductOverlayRunConfig config = new ProductOverlayRunConfig(run, false, 0);
		config.setAssetClassMap(new HashMap<>());
		config.setAssetClassRollupMap(new HashMap<>());

		config.setMinimizeImbalancesCashConfig(new MinimizeImbalancesCashConfig());
		config.setAccountRebalanceConfig(createDefaultRebalanceConfig(tradingMinEffectiveCash, tradingMaxEffectiveCash, minimizeCashAdjustment));
		return config;
	}


	protected void setupInvestmentAccountFullRebalance(ProductOverlayRunConfig config, boolean useBalanceDate) {
		// Setup Rebalance History to flag as full rebalance on balance date
		InvestmentAccountRebalanceHistory history = new InvestmentAccountRebalanceHistory();
		history.setRebalanceDate((useBalanceDate) ? config.getBalanceDate() : DateUtils.addDays(config.getBalanceDate(), -3));
		history.setRebalanceCalculationType(RebalanceCalculationTypes.FULL);
		config.setAccountRebalanceHistory(history);

		ValidationUtils.assertEquals(useBalanceDate, config.isFullRebalancePerformedOnBalanceDate(), "Rebalance History should be setup so that full rebalance was performed on balance date.");
	}


	protected void setupInvestmentAccountManualRebalance(ProductOverlayRunConfig config, boolean useBalanceDate) {
		// Setup Rebalance History to flag as full rebalance on balance date
		InvestmentAccountRebalanceHistory history = new InvestmentAccountRebalanceHistory();
		history.setRebalanceDate((useBalanceDate) ? config.getBalanceDate() : DateUtils.addDays(config.getBalanceDate(), -3));
		history.setRebalanceCalculationType(RebalanceCalculationTypes.MANUAL);
		config.setAccountRebalanceHistory(history);

		ValidationUtils.assertEquals(useBalanceDate, config.isManualRebalancePerformedOnBalanceDate(), "Rebalance History should be setup so that full rebalance was performed on balance date.");
	}


	protected void addRebalanceHistory(ProductOverlayRunConfig config, String assetClassName, double beforeAmount, double afterAmount) {
		InvestmentAccountRebalanceAssetClassHistory ahr = new InvestmentAccountRebalanceAssetClassHistory();
		for (InvestmentAccountAssetClass c : config.getAssetClassMap().keySet()) {
			if (c.getAssetClass().getName().equals(assetClassName)) {
				ahr.setAccountAssetClass(c);
				break;
			}
		}
		if (ahr.getAccountAssetClass() == null) {
			throw new ValidationException("Can't set up rebalance history for asset class " + assetClassName + " because it hasn't been defined.");
		}
		ahr.setBeforeRebalanceCash(BigDecimal.valueOf(beforeAmount));
		ahr.setAfterRebalanceCash(BigDecimal.valueOf(afterAmount));
		if (config.getAccountRebalanceHistory().getAssetClassHistoryList() == null) {
			config.getAccountRebalanceHistory().setAssetClassHistoryList(new ArrayList<>());
		}
		config.getAccountRebalanceHistory().getAssetClassHistoryList().add(ahr);
	}


	private InvestmentAccountRebalance createDefaultRebalanceConfig(Double minEffectiveCash, Double maxEffectiveCash, Double cashAdjustment) {
		InvestmentAccountRebalance conf = new InvestmentAccountRebalance();
		conf.setId(1);
		if (minEffectiveCash != null) {
			conf.setMinEffectiveCashExposure(BigDecimal.valueOf(minEffectiveCash));
		}
		if (maxEffectiveCash != null) {
			conf.setMaxEffectiveCashExposure(BigDecimal.valueOf(maxEffectiveCash));
		}
		if (cashAdjustment != null) {
			conf.setMinimizeImbalancesAdjustmentAmount(BigDecimal.valueOf(cashAdjustment));
		}
		return conf;
	}


	protected void addAssetClassToMap(ProductOverlayRunConfig config, ProductOverlayAssetClassBuilder productOverlayAssetClassBuilder) {
		addAssetClassToMap(config, null, productOverlayAssetClassBuilder);
	}


	protected void addAssetClassToMap(ProductOverlayRunConfig config, String parentAssetClassName, ProductOverlayAssetClassBuilder productOverlayAssetClassBuilder) {
		InvestmentAccountAssetClass parentIac = null;
		if (!StringUtils.isEmpty(parentAssetClassName)) {
			for (InvestmentAccountAssetClass c : config.getAssetClassRollupMap().keySet()) {
				if (c.getAssetClass().getName().equals(parentAssetClassName)) {
					parentIac = c;
				}
			}
			if (parentIac == null) {
				throw new RuntimeException("Missing parent asset class [" + parentAssetClassName + "] from the asset class rollup map.  Parents should be added before their children.");
			}
			productOverlayAssetClassBuilder.asChild(config.getAssetClassRollupMap().get(parentIac));
		}

		ProductOverlayAssetClass overlayAssetClass = productOverlayAssetClassBuilder.build();
		if (overlayAssetClass.isRollupAssetClass()) {
			config.getAssetClassRollupMap().put(overlayAssetClass.getAccountAssetClass(), overlayAssetClass);
		}
		else {
			config.getAssetClassMap().put(overlayAssetClass.getAccountAssetClass(), overlayAssetClass);
		}
		for (Map.Entry<ManagerCashTypes, BigDecimal> cashTypeEntry : productOverlayAssetClassBuilder.getMinimizeImbalancesCash().entrySet()) {
			config.getMinimizeImbalancesCashConfig().addCashToBucket(overlayAssetClass.getAccountAssetClass(), cashTypeEntry.getKey(), cashTypeEntry.getValue());
		}
	}


	////////////////////////////////////////////////////////
	//////           Test Calculate Methods          ///////
	////////////////////////////////////////////////////////


	protected void processCalculation(ProductOverlayRunConfig config) {
		ProductOverlayMinimizeImbalancesCalculator calculator = getCalculator();
		((BaseProductOverlayMinimizeImbalancesCalculatorImpl) calculator).setProductUtilService(this.productUtilService);
		calculator.calculate(config);
	}


	////////////////////////////////////////////////////////
	///////           Test Validate Methods          ///////
	////////////////////////////////////////////////////////


	/**
	 * Rollups are summed up later after all processing is done
	 */
	protected void validateRollupAssetClassAllocation(ProductOverlayRunConfig config, String assetClassName) {
		validateAssetClassAllocationImpl(config, true, assetClassName, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	}


	protected void validateZeroAssetClassAllocation(ProductOverlayRunConfig config, String assetClassName) {
		validateAssetClassAllocationImpl(config, false, assetClassName, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	}


	protected void validateAssetClassAllocation(ProductOverlayRunConfig config, String assetClassName, double managerCash, double fundCash, double transitionCash, double rebalanceCash,
	                                            double rebalanceCashAdjusted, double rebalanceAttributionCash) {
		validateAssetClassAllocationImpl(config, false, assetClassName, managerCash, fundCash, transitionCash, rebalanceCash, rebalanceCashAdjusted, rebalanceAttributionCash);
	}


	private void validateAssetClassAllocationImpl(ProductOverlayRunConfig config, boolean rollup, String assetClassName, double managerCash, double fundCash, double transitionCash,
	                                              double rebalanceCash, double rebalanceCashAdjusted, double rebalanceAttributionCash) {
		boolean found = false;
		Map<InvestmentAccountAssetClass, ProductOverlayAssetClass> map = (rollup ? config.getAssetClassRollupMap() : config.getAssetClassMap());
		for (Map.Entry<InvestmentAccountAssetClass, ProductOverlayAssetClass> investmentAccountAssetClassProductOverlayAssetClassEntry : map.entrySet()) {
			String acName = (investmentAccountAssetClassProductOverlayAssetClassEntry.getKey()).getAssetClass().getName();
			if (assetClassName.equals(acName)) {

				found = true;
				ProductOverlayAssetClass oac = investmentAccountAssetClassProductOverlayAssetClassEntry.getValue();

				/*
				 System.out.println(assetClassName + ": " + CoreMathUtils.formatNumberMoney(oac.getCashTotal()));
				 System.out.println(StringUtils.TAB + "Manager: " + CoreMathUtils.formatNumberMoney(oac.getManagerCash()));
				 System.out.println(StringUtils.TAB + "Fund: " + CoreMathUtils.formatNumberMoney(oac.getFundCash()));
				 System.out.println(StringUtils.TAB + "Transition: " + CoreMathUtils.formatNumberMoney(oac.getTransitionCash()));
				 System.out.println(StringUtils.TAB + "Rebalance: " + CoreMathUtils.formatNumberMoney(oac.getRebalanceCash()));
				 System.out.println(StringUtils.TAB + "Rebal Adj: " + CoreMathUtils.formatNumberMoney(oac.getRebalanceCashAdjusted()));
				 System.out.println(StringUtils.TAB + "Attribution: " + CoreMathUtils.formatNumberMoney(oac.getRebalanceAttributionCash()));

				 double expectedTotalCash = managerCash + fundCash + rebalanceCash + rebalanceAttributionCash;

				 ValidationUtils.assertTrue(MathUtils.isEqual(MathUtils.round(BigDecimal.valueOf(expectedTotalCash), 0), MathUtils.round(oac.getEffectiveCashTotal(), 0)), assetClassName
				 + ": Expected Total Cash to be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(expectedTotalCash)) + ", but was " + CoreMathUtils.formatNumberMoney(oac.getEffectiveCashTotal()));

				 */
				ValidationUtils.assertTrue(MathUtils.isEqual(MathUtils.round(BigDecimal.valueOf(managerCash), 2), MathUtils.round(oac.getManagerCash(), 2)), assetClassName
						+ ": Expected Manager Cash to be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(managerCash)) + ", but was " + CoreMathUtils.formatNumberMoney(oac.getManagerCash()));

				ValidationUtils.assertTrue(MathUtils.isEqual(MathUtils.round(BigDecimal.valueOf(fundCash), 2), MathUtils.round(oac.getFundCash(), 2)), assetClassName + ": Expected Fund Cash to be "
						+ CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(fundCash)) + ", but was " + CoreMathUtils.formatNumberMoney(oac.getFundCash()));

				ValidationUtils.assertTrue(MathUtils.isEqual(MathUtils.round(BigDecimal.valueOf(transitionCash), 2), MathUtils.round(oac.getTransitionCash(), 2)), assetClassName
						+ ": Expected Transition Cash to be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(transitionCash)) + ", but was " + CoreMathUtils.formatNumberMoney(oac.getTransitionCash()));

				ValidationUtils.assertTrue(MathUtils.isEqual(MathUtils.round(BigDecimal.valueOf(rebalanceCash), 2), MathUtils.round(oac.getRebalanceCash(), 2)), assetClassName
						+ ": Expected Rebalance Cash to be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(rebalanceCash)) + ", but was " + CoreMathUtils.formatNumberMoney(oac.getRebalanceCash()));

				ValidationUtils.assertTrue(
						MathUtils.isEqual(MathUtils.round(BigDecimal.valueOf(rebalanceCashAdjusted), 2), MathUtils.round(oac.getRebalanceCashAdjusted(), 2)),
						assetClassName + ": Expected Rebal Cash Adjusted to be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(rebalanceCashAdjusted)) + ", but was "
								+ CoreMathUtils.formatNumberMoney(oac.getRebalanceCashAdjusted()));

				ValidationUtils.assertTrue(
						MathUtils.isEqual(MathUtils.round(BigDecimal.valueOf(rebalanceAttributionCash), 2), MathUtils.round(oac.getRebalanceAttributionCash(), 2)),
						assetClassName + ": Expected Rebalance Attribution Cash to be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(rebalanceAttributionCash)) + ", but was "
								+ CoreMathUtils.formatNumberMoney(oac.getRebalanceAttributionCash()));

				break;
			}
		}
		Assertions.assertTrue(found, "Did not find Asset Class: " + assetClassName + " in " + (rollup ? "rollup " : "") + " asset class map to validate");
	}


	protected void validateTradingBandsMessage(ProductOverlayRunConfig config, String expectedMessage) {
		if (expectedMessage == null) {
			Assertions.assertTrue(StringUtils.isEmpty(config.getTradingBandsMessage()),
					"Trading Bands message expected to be empty because no bands defined, instead it was: [" + config.getTradingBandsMessage() + "]");
		}
		else {
			Assertions.assertEquals(expectedMessage, config.getTradingBandsMessage().replaceAll(StringUtils.NEW_LINE, " "));
		}
	}


	protected void validateMinimizeImbalancesMessage(ProductOverlayRunConfig config, String expectedMessage) {
		if (expectedMessage == null) {
			Assertions.assertTrue(StringUtils.isEmpty(config.getMinimizeImbalancesCashConfig().getResultMessage()), "Minimize Imbalances Message expected to be empty because not calculated.");
		}
		else {
			Assertions.assertTrue(config.getMinimizeImbalancesCashConfig().getResultMessage().replaceAll(StringUtils.NEW_LINE, " ").startsWith(expectedMessage));
		}
	}
}
