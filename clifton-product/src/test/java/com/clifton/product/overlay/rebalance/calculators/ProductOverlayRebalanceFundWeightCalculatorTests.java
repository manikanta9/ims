package com.clifton.product.overlay.rebalance.calculators;


import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;
import com.clifton.product.overlay.rebalance.ProductOverlayRebalance;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


@ContextConfiguration(locations = "ProductOverlayRebalanceCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ProductOverlayRebalanceFundWeightCalculatorTests extends BaseProductOverlayRebalanceCalculatorTests {

	@Override
	public RebalanceCalculationTypes getRebalanceCalculationType() {
		return RebalanceCalculationTypes.PROPORTIONAL_FUND_WEIGHT;
	}


	@Test
	public void testMiniRebalanceFundWeight_1() {
		Map<String, BigDecimal> resultMap = new HashMap<>();
		ProductOverlayRebalance rebalance = createProductOverlayRebalance();

		addAssetClassToList(rebalance, resultMap, "Domestic Equity Large Cap", 20.0000, 13627153.08, 13618849.49);
		addAssetClassToList(rebalance, resultMap, "Domestic Equity Small Cap", 6.1500, -19756091.88, -19758645.24);
		addAssetClassToList(rebalance, resultMap, "Intl Equity-Developed", 20.0000, 5344438.53, 5336134.94);
		addAssetClassToList(rebalance, resultMap, "Intl Equity-Emerging", 6.1500, 18204991.77, 18202438.41);
		addAssetClassToList(rebalance, resultMap, "Fixed Income Core", 36.9300, -2723159.99, -2738492.58);
		addAssetClassToList(rebalance, resultMap, "Fixed  Absolute Return", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Fixed Income Comm. Mortgage", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Commodities", 3.8500, -2830183.84, -2831782.28);
		addAssetClassToList(rebalance, resultMap, "Real Estate", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Global Inflation Fixed Income", 6.9200, -11825629.70, -11828502.74);
		addAssetClassToList(rebalance, resultMap, "Global Asset Allocation", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Risk Parity", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "LTGB Fixed Income", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Operating Cash", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Cash", 0.0000, 0.00, 0.00);

		calculateNewRebalanceCash(rebalance);
		validateResults(rebalance, resultMap);
	}


	@Test
	public void testMiniRebalanceFundWeight_2() {
		Map<String, BigDecimal> resultMap = new HashMap<>();
		ProductOverlayRebalance rebalance = createProductOverlayRebalance();

		addAssetClassToList(rebalance, resultMap, "Real Estate", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Intl Equity-Emerging", 6.1500, 17964867.98, 17974626.32);
		addAssetClassToList(rebalance, resultMap, "Intl Equity-Developed", 20.0000, 5243118.15, 5274852.58);
		addAssetClassToList(rebalance, resultMap, "Operating Cash", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "LTGB Fixed Income", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Risk Parity", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Cash", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Fixed Income Comm. Mortgage", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Fixed Income Core", 36.9300, -2783126.87, -2724529.25);
		addAssetClassToList(rebalance, resultMap, "Global Asset Allocation", 0.0000, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Global Inflation Fixed Income", 6.9200, -11831786.60, -11820806.49);
		addAssetClassToList(rebalance, resultMap, "Commodities", 3.8500, -2795241.84, -2789132.96);
		addAssetClassToList(rebalance, resultMap, "Domestic Equity Large Cap", 20.0000, 13495370.69, 13527105.12);
		addAssetClassToList(rebalance, resultMap, "Domestic Equity Small Cap", 6.1500, -19451873.66, -19442115.32);
		addAssetClassToList(rebalance, resultMap, "Fixed  Absolute Return", 0.0000, 0.00, 0.00);

		calculateNewRebalanceCash(rebalance);
		validateResults(rebalance, resultMap);
	}
}
