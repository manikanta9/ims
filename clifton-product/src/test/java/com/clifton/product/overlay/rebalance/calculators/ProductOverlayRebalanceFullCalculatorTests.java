package com.clifton.product.overlay.rebalance.calculators;


import com.clifton.investment.account.assetclass.rebalance.RebalanceCalculationTypes;
import com.clifton.product.overlay.rebalance.ProductOverlayRebalance;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


@ContextConfiguration(locations = "ProductOverlayRebalanceCalculatorTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ProductOverlayRebalanceFullCalculatorTests extends BaseProductOverlayRebalanceCalculatorTests {

	@Override
	public RebalanceCalculationTypes getRebalanceCalculationType() {
		return RebalanceCalculationTypes.FULL;
	}


	///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////


	@Test
	public void testFullRebalance_1() {
		Map<String, BigDecimal> resultMap = new HashMap<>();
		ProductOverlayRebalance rebalance = createProductOverlayRebalance();

		addAssetClassToList(rebalance, resultMap, "Cash", false, 0.00, 35414786.38, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Commodities", true, 118225239.29, 113369107.94, 229819.80, 1894418.80, 0.00, 0.00, 2301861.40, 0.00, 430031.35);
		addAssetClassToList(rebalance, resultMap, "Domestic Equity Large Cap", true, 256154685.13, 230006303.53, 1135201.70, 9357537.81, 0.00, 0.00, 11370112.59, 0.00, 4285529.50);
		addAssetClassToList(rebalance, resultMap, "Domestic Equity-SMID", true, 59112619.65, 96499721.57, 0.00, 0.00, 0.00, 0.00, -38376070.26, 0.00, 988968.34);
		addAssetClassToList(rebalance, resultMap, "Fixed Income Alternative", false, 67665049.01, 67665049.01, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Fixed Income Core", true, 433492544.08, 407693272.62, 1684078.92, 13881966.76, 0.00, 0.00, 16867634.23, 0.00, -6634408.45);
		addAssetClassToList(rebalance, resultMap, "Fixed Income High Yield", false, 108009581.44, 108009581.44, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Hedge Funds", false, 178714393.13, 178714393.13, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Infrastructure", false, 74686456.19, 74686456.19, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "International Equity", true, 118225239.29, 102227135.40, 782399.02, 6449363.57, 0.00, 0.00, 7836462.05, 0.00, 929879.25);
		addAssetClassToList(rebalance, resultMap, "Private Equity", false, 93109649.53, 93109649.53, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Real Estate", false, 132422096.21, 132422096.21, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Risk Parity", false, 138380299.70, 138380299.70, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);

		calculateNewRebalanceCash(rebalance);
		validateResults(rebalance, resultMap);
	}


	@Test
	public void testFullRebalance_2() {

		Map<String, BigDecimal> resultMap = new HashMap<>();
		ProductOverlayRebalance rebalance = createProductOverlayRebalance();

		addAssetClassToList(rebalance, resultMap, "Alternative Assets", false, 17745476.10, 17745476.10, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Cash", false, 0.00, 26532727.74, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Domestic Equity Large Cap", true, 35389498.18, 28713514.82, 283161.87, 5491018.97, 0.00, 0.00, 0.00, 0.00, 901802.52);
		addAssetClassToList(rebalance, resultMap, "Domestic Equity Small Cap", true, 17039388.01, 6943988.72, 454067.98, 8805196.46, 0.00, 0.00, 0.00, 0.00, 836134.85);
		addAssetClassToList(rebalance, resultMap, "Emerging Markets", true, 7208971.85, 0.00, 349265.69, 6772891.24, 0.00, 0.00, 0.00, 0.00, 86814.92);
		addAssetClassToList(rebalance, resultMap, "Fixed Income", true, 133256752.41, 132213236.10, 141316.42, 2740380.02, 0.00, 0.00, 0.00, 0.00, -1838180.13);
		addAssetClassToList(rebalance, resultMap, "International Equity", true, 8082786.62, 6573929.69, 73334.82, 1422094.27, 0.00, 0.00, 0.00, 0.00, 13427.84);

		calculateNewRebalanceCash(rebalance);
		validateResults(rebalance, resultMap);
	}


	@Test
	public void testFullRebalance_withRebalanceCash() {
		Map<String, BigDecimal> resultMap = new HashMap<>();
		ProductOverlayRebalance rebalance = createProductOverlayRebalance();

		addAssetClassToList(rebalance, resultMap, "Domestic Equity", true, 167620338.38, 165235000.54, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2385337.84);
		addAssetClassToList(rebalance, resultMap, "International Equity	", true, 34181219.00, 32589124.63, 9532873.63, 0.00, 0.00, -7940779.63, 0.00, 0.00, -7940779.26);
		addAssetClassToList(rebalance, resultMap, "Emerging Markets", true, 53241116.84, 38796947.71, 14850561.00, 0.00, 0.00, 0.00, 0.00, 0.00, -406391.87);
		addAssetClassToList(rebalance, resultMap, "Fixed Income", true, 71316370.52, 45460806.22, 19893731.00, 0.00, 0.00, 0.00, 0.00, 0.00, 5961833.30);
		addAssetClassToList(rebalance, resultMap, "Real Assets", false, 67622394.62, 67622394.62, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Hedge Funds", false, 211631886.03, 211631886.03, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Private Equity", false, 116370034.77, 116370034.77, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Opportunistic", false, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Leverage Adjustment", false, -50597842.68, -50597842.68, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);
		addAssetClassToList(rebalance, resultMap, "Cash", false, 0.00, -2471684.31, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);

		calculateNewRebalanceCash(rebalance);
		validateResults(rebalance, resultMap);
	}
}
