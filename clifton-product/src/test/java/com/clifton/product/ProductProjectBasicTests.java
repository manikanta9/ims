package com.clifton.product;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ProductProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "product";
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("minimizeimbalances");
		approvedList.add("option");
	}


	@Override
	protected void configureDTOSkipPropertyNames(Set<String> skipPropertyNames) {
		skipPropertyNames.add("value");
		skipPropertyNames.add("customColumns");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		HashSet<String> skipMethods = new HashSet<>();
		skipMethods.add("getProductOverlayManagerAccountList");
		skipMethods.add("getProductLDIMaturityExtendedList");
		skipMethods.add("saveProductLDIMaturity");
		skipMethods.add("getProductPerformanceOverlayRunCashAttribution");
		skipMethods.add("getProductPerformanceOverlayRunCashAttributionList");
		skipMethods.add("getProductPerformanceOverlayAssetClassCashAttributionList");
		skipMethods.add("saveProductPerformanceOverlayRunCashAttribution");
		skipMethods.add("saveProductOverlayAssetClassCashAdjustment");
		skipMethods.add("deleteProductOverlayAssetClassCashAdjustment");
		return skipMethods.contains(method.getName());
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveProductLDIBalanceEntry");
		ignoredVoidSaveMethodSet.add("saveProductOverlayManagerAccountRebalanceListByRun");
		ignoredVoidSaveMethodSet.add("saveProductOverlayManagerAccountListByRun");
		ignoredVoidSaveMethodSet.add("saveProductOverlayAssetClassListByRun");
		ignoredVoidSaveMethodSet.add("saveProductOverlayAssetClassReplicationListByRun");
		ignoredVoidSaveMethodSet.add("saveProductOverlayAssetClassReplicationDetailListByRun");
		ignoredVoidSaveMethodSet.add("saveProductOverlayRebalance");
		ignoredVoidSaveMethodSet.add("saveProductPerformanceOverlayAssetClassListForRun");
		ignoredVoidSaveMethodSet.add("saveProductPerformanceOverlayAssetClassTrackingErrorListForRun");
		return ignoredVoidSaveMethodSet;
	}
}
