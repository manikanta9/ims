package com.clifton.report.export;


import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.report.definition.Report;
import com.clifton.report.definition.ReportDefinitionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;


public class ReportExportServiceTests<T> {

	@Mock
	ReportDefinitionService reportDefinitionService;

	private ReportExportServiceImpl<T> reportExportService;

	private static final String TEST_REPORT_URL = "http://tcgdevweb/IMSReportServer/ReportViewer.aspx?/PIOS%20Components/PIOS%20Report&reportId=";


	@BeforeEach
	public void setupTest() {
		MockitoAnnotations.initMocks(this);
		Mockito.when(this.reportDefinitionService.getReportUrl(ArgumentMatchers.anyInt())).thenReturn(TEST_REPORT_URL);

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			return TEST_REPORT_URL + args[0];
		}).when(this.reportDefinitionService).getReportUrl(ArgumentMatchers.anyInt());

		this.reportExportService = new ReportExportServiceImpl<>();
		this.reportExportService.setReportDefinitionService(this.reportDefinitionService);
	}


	@Test
	@Disabled
	public void testReportExport() {
		LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
		parameters.put("ClientAccountID", 485);
		parameters.put("BalanceDate", "7/27/11");
		parameters.put("RunID", 1772);
		parameters.put("ExcludePrivate", true);

		Report report = new Report();
		report.setId(48);
		report.setName("WARF");

		FileWrapper file = this.reportExportService.getReportPDFFile(new ReportExportConfiguration(report.getId(), parameters, false, false));

		try {
			//Assert.assertEquals("PIOS_Report___Short", file.getName().substring(0, 19));
			FileUtils.copyFile(file.getFile(), FilePath.forPath("c:\\bin\\test_report\\" + FileUtils.getFileName(file.getFile().getPath())));
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
		Assertions.assertTrue((new File("\\\\tcgutil01\\data\\PIOS\\PDFComponents\\WARF PIOS PLUS Summary.pdf")).exists());
	}
}
