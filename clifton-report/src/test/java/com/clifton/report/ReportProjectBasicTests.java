package com.clifton.report;


import com.clifton.core.test.BasicProjectTests;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;


@ContextConfiguration
public class ReportProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "report";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("com.itextpdf.");
		imports.add("java.net.URLEncoder");
		imports.add("org.apache.http.");
	}
}
