package com.clifton.report.definition;


import com.clifton.core.beans.NamedEntity;
import com.clifton.report.definition.template.ReportTemplate;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;

import java.util.List;


/**
 * The <code>Report</code> class represents a single report.
 * Each report uses a specific templates and consists of an ordered List of
 * reporting components.
 *
 * @author vgomelsky
 */
public class Report extends NamedEntity<Integer> implements SystemEntityModifyConditionAware {

	private ReportTemplate reportTemplate;

	private List<ReportComponent> componentList;

	/**
	 * Optional URL parameters to be added to template's URL.
	 * See ReportTemplate.additionalUrlParameterOnly for more details.
	 */
	private String additionalUrlParameter;

	/**
	 * Ability to restrict who can edit reports, report components, and report component parameters.
	 */
	private SystemCondition entityModifyCondition;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public ReportTemplate getReportTemplate() {
		return this.reportTemplate;
	}


	public void setReportTemplate(ReportTemplate reportTemplate) {
		this.reportTemplate = reportTemplate;
	}


	public List<ReportComponent> getComponentList() {
		return this.componentList;
	}


	public void setComponentList(List<ReportComponent> componentList) {
		this.componentList = componentList;
	}


	public String getAdditionalUrlParameter() {
		return this.additionalUrlParameter;
	}


	public void setAdditionalUrlParameter(String additionalUrlParameter) {
		this.additionalUrlParameter = additionalUrlParameter;
	}
}
