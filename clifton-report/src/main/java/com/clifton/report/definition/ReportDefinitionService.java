package com.clifton.report.definition;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.report.definition.search.ReportComponentParameterSearchForm;
import com.clifton.report.definition.search.ReportSearchForm;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * The <code>ReportDefinitionService</code> interface defines methods wor working with reports.
 *
 * @author vgomelsky
 */
public interface ReportDefinitionService {

	////////////////////////////////////////////////////////////////////////////
	////////              Report Business Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns fully qualified report url for given report
	 */
	@ResponseBody
	@SecureMethod(dtoClass = Report.class)
	public String getReportUrl(int id);


	/**
	 * Returns Report with the specified id along with its ReportComponent(s).
	 */
	public Report getReport(int id);


	/**
	 * Returns Report for the specified arguments. Report name + template must be unique.
	 */
	public Report getReportByNameAndTemplate(String reportName, String templateName);


	public List<Report> getReportList();


	public List<Report> getReportList(ReportSearchForm searchForm);


	public Report saveReport(Report bean);


	public void deleteReport(int id);


	/**
	 * Create and return a copy of the specified report with a new name.
	 * New copy includes report's components and parameters.
	 */
	public Report copyReport(int reportId, String name);


	////////////////////////////////////////////////////////////////////////////
	////////             Report Component Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReportComponent getReportComponent(int id);


	public List<ReportComponent> getReportComponentListNotOfType(int reportId, String typeName);


	public ReportComponent saveReportComponent(ReportComponent bean);


	public void deleteReportComponent(int id);


	////////////////////////////////////////////////////////////////////////////
	////////         Report Component Parameter Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	public List<ReportComponentParameter> getReportComponentParameterList(ReportComponentParameterSearchForm searchForm);


	public ReportComponentParameter getReportComponentParameterByComponentAndName(int componentId, String parameterName);


	public ReportComponentParameter getReportComponentParameterByComponentAndTemplate(int componentId, int reportTemplateComponentParameterId);
}
