package com.clifton.report.definition.template;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.report.definition.template.search.ReportTemplateComponentSearchForm;
import com.clifton.report.definition.template.search.ReportTemplateComponentTypeSearchForm;
import com.clifton.report.definition.template.search.ReportTemplateSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author vgomelsky
 */
@Service
public class ReportTemplateServiceImpl implements ReportTemplateService {

	private AdvancedUpdatableDAO<ReportTemplate, Criteria> reportTemplateDAO;
	private AdvancedUpdatableDAO<ReportTemplateParameter, Criteria> reportTemplateParameterDAO;
	private AdvancedUpdatableDAO<ReportTemplateComponent, Criteria> reportTemplateComponentDAO;
	private AdvancedUpdatableDAO<ReportTemplateComponentParameter, Criteria> reportTemplateComponentParameterDAO;
	private AdvancedUpdatableDAO<ReportTemplateComponentType, Criteria> reportTemplateComponentTypeDAO;

	private DaoNamedEntityCache<ReportTemplate> reportTemplateCache;


	////////////////////////////////////////////////////////////////////////////
	//////                   Report Template Methods                     ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReportTemplate getReportTemplate(short id) {
		return getReportTemplateDAO().findByPrimaryKey(id);
	}


	@Override
	public ReportTemplate getReportTemplateByName(String name) {
		return getReportTemplateCache().getBeanForKeyValueStrict(getReportTemplateDAO(), name);
	}


	@Override
	public List<ReportTemplate> getReportTemplateList(ReportTemplateSearchForm searchForm) {
		return getReportTemplateDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReportTemplate saveReportTemplate(ReportTemplate bean) {
		return getReportTemplateDAO().save(bean);
	}


	@Override
	public void deleteReportTemplate(short id) {
		getReportTemplateDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////              Report Template Parameter Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReportTemplateParameter getReportTemplateParameter(int id) {
		return getReportTemplateParameterDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReportTemplateParameter> getReportTemplateParameterListByTemplate(short templateId) {
		return getReportTemplateParameterDAO().findByField("template.id", templateId);
	}


	@Override
	public ReportTemplateParameter saveReportTemplateParameter(ReportTemplateParameter bean) {
		return getReportTemplateParameterDAO().save(bean);
	}


	@Override
	public void deleteReportTemplateParameter(int id) {
		getReportTemplateParameterDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////             Report Template Component Methods                 ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReportTemplateComponent getReportTemplateComponent(int id) {
		return getReportTemplateComponentDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReportTemplateComponent> getReportTemplateComponentList(ReportTemplateComponentSearchForm searchForm) {
		return getReportTemplateComponentDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<ReportTemplateComponent> getReportTemplateComponentListByTemplate(short templateId) {
		return getReportTemplateComponentDAO().findByField("reportTemplate.id", templateId);
	}


	@Override
	public ReportTemplateComponent saveReportTemplateComponent(ReportTemplateComponent bean) {
		return getReportTemplateComponentDAO().save(bean);
	}


	@Override
	public void deleteReportTemplateComponent(int id) {
		getReportTemplateComponentDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////              Report Template Component Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReportTemplateComponentParameter getReportTemplateComponentParameter(int id) {
		return getReportTemplateComponentParameterDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReportTemplateComponentParameter> getReportTemplateComponentParameterListByTemplateComponent(int templateComponentId, Boolean staticValue) {
		String[] beanFieldNames;
		Object[] values;
		if (staticValue != null) {
			beanFieldNames = new String[]{"reportTemplateComponent.id", "staticValue"};
			values = new Object[]{templateComponentId, staticValue};
		}
		else {
			beanFieldNames = new String[]{"reportTemplateComponent.id"};
			values = new Object[]{templateComponentId};
		}
		return getReportTemplateComponentParameterDAO().findByFields(beanFieldNames, values);
	}


	@Override
	public ReportTemplateComponentParameter saveReportTemplateComponentParameter(ReportTemplateComponentParameter bean) {
		return getReportTemplateComponentParameterDAO().save(bean);
	}


	@Override
	public void deleteReportTemplateComponentParameter(int id) {
		getReportTemplateComponentParameterDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////           Report Template Component Type Methods              ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ReportTemplateComponentType> getReportTemplateComponentTypeList(ReportTemplateComponentTypeSearchForm searchForm) {
		return getReportTemplateComponentTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ReportTemplate, Criteria> getReportTemplateDAO() {
		return this.reportTemplateDAO;
	}


	public void setReportTemplateDAO(AdvancedUpdatableDAO<ReportTemplate, Criteria> reportTemplateDAO) {
		this.reportTemplateDAO = reportTemplateDAO;
	}


	public AdvancedUpdatableDAO<ReportTemplateParameter, Criteria> getReportTemplateParameterDAO() {
		return this.reportTemplateParameterDAO;
	}


	public void setReportTemplateParameterDAO(AdvancedUpdatableDAO<ReportTemplateParameter, Criteria> reportTemplateParameterDAO) {
		this.reportTemplateParameterDAO = reportTemplateParameterDAO;
	}


	public AdvancedUpdatableDAO<ReportTemplateComponent, Criteria> getReportTemplateComponentDAO() {
		return this.reportTemplateComponentDAO;
	}


	public void setReportTemplateComponentDAO(AdvancedUpdatableDAO<ReportTemplateComponent, Criteria> reportTemplateComponentDAO) {
		this.reportTemplateComponentDAO = reportTemplateComponentDAO;
	}


	public AdvancedUpdatableDAO<ReportTemplateComponentParameter, Criteria> getReportTemplateComponentParameterDAO() {
		return this.reportTemplateComponentParameterDAO;
	}


	public void setReportTemplateComponentParameterDAO(AdvancedUpdatableDAO<ReportTemplateComponentParameter, Criteria> reportTemplateComponentParameterDAO) {
		this.reportTemplateComponentParameterDAO = reportTemplateComponentParameterDAO;
	}


	public AdvancedUpdatableDAO<ReportTemplateComponentType, Criteria> getReportTemplateComponentTypeDAO() {
		return this.reportTemplateComponentTypeDAO;
	}


	public void setReportTemplateComponentTypeDAO(AdvancedUpdatableDAO<ReportTemplateComponentType, Criteria> reportTemplateComponentTypeDAO) {
		this.reportTemplateComponentTypeDAO = reportTemplateComponentTypeDAO;
	}


	public DaoNamedEntityCache<ReportTemplate> getReportTemplateCache() {
		return this.reportTemplateCache;
	}


	public void setReportTemplateCache(DaoNamedEntityCache<ReportTemplate> reportTemplateCache) {
		this.reportTemplateCache = reportTemplateCache;
	}
}
