package com.clifton.report.definition.template;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;

import java.math.BigDecimal;


/**
 * The <code>ReportTemplate</code> class defines a reporting template (report layout, headers, footers, etc.)
 * and a list of available ReportTemplateComponent(s) that can be used in reports utilizing this template.
 *
 * @author vgomelsky
 */
@CacheByName
public class ReportTemplate extends NamedEntity<Short> {

	/**
	 * URL of the report (used with external reporting tools).
	 */
	private String url;

	/**
	 * If true, append Report.additionalUrlParameters to the template URL (no extra stuff).
	 * Otherwise, appends "?reportId=???&" before appending additional parameters.
	 */
	private boolean additionalUrlParameterOnly;

	/**
	 * The width of all sub report components on the report
	 */
	private BigDecimal subReportWidth;

	/**
	 * Indicates that report for this template will be cached for faster loading.
	 */
	private boolean reportCachingUsed;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUrl() {
		return this.url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public BigDecimal getSubReportWidth() {
		return this.subReportWidth;
	}


	public void setSubReportWidth(BigDecimal subReportWidth) {
		this.subReportWidth = subReportWidth;
	}


	public boolean isAdditionalUrlParameterOnly() {
		return this.additionalUrlParameterOnly;
	}


	public void setAdditionalUrlParameterOnly(boolean additionalUrlParameterOnly) {
		this.additionalUrlParameterOnly = additionalUrlParameterOnly;
	}


	public boolean isReportCachingUsed() {
		return this.reportCachingUsed;
	}


	public void setReportCachingUsed(boolean reportCachingUsed) {
		this.reportCachingUsed = reportCachingUsed;
	}
}
