package com.clifton.report.definition;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.report.definition.search.ReportComponentParameterSearchForm;
import com.clifton.report.definition.search.ReportSearchForm;
import com.clifton.report.definition.template.ReportTemplate;
import com.clifton.report.definition.template.ReportTemplateService;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>ReportDefinitionServiceImpl</code> class provides basic implementation of ReportDefinitionService interface.
 *
 * @author vgomelsky
 */
@Service
public class ReportDefinitionServiceImpl implements ReportDefinitionService {

	private AdvancedUpdatableDAO<Report, Criteria> reportDAO;
	private AdvancedUpdatableDAO<ReportComponent, Criteria> reportComponentDAO;
	private AdvancedUpdatableDAO<ReportComponentParameter, Criteria> reportComponentParameterDAO;

	private DaoCompositeKeyCache<Report, String, Short> reportByNameAndTemplateCache;
	private DaoSingleKeyListCache<ReportComponent, Integer> reportComponentByReportCache;

	private ReportTemplateService reportTemplateService;
	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;

	private String reportUrlRoot = "http://tcgdevweb/IMSReportServer/ReportViewer.aspx?";

	////////////////////////////////////////////////////////////////////////////
	////////              Report Business Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getReportUrl(int id) {
		Report report = getReport(id);
		String reportUrl = null;
		if (report != null && !StringUtils.isEmpty(report.getReportTemplate().getUrl())) {
			reportUrl = report.getReportTemplate().getUrl();

			if (!reportUrl.startsWith("http")) {
				try {
					reportUrl = getReportUrlRoot() + URLEncoder.encode(reportUrl, "UTF-8");
				}
				catch (UnsupportedEncodingException e) {
					throw new RuntimeException("Cannot encode url = '" + reportUrl + "' for reportId = " + id, e);
				}
			}

			if (report.getReportTemplate().isAdditionalUrlParameterOnly()) {
				if (!StringUtils.isEmpty(report.getAdditionalUrlParameter())) {
					reportUrl += report.getAdditionalUrlParameter();
				}
			}
			else {
				reportUrl += (reportUrl.indexOf('?') == -1) ? "?" : "&";
				reportUrl += "reportId=" + id;
				if (!StringUtils.isEmpty(report.getAdditionalUrlParameter())) {
					reportUrl += "&" + report.getAdditionalUrlParameter();
				}
			}
		}
		return reportUrl;
	}


	@Override
	public Report getReport(int id) {
		Report report = getReportDAO().findByPrimaryKey(id);
		if (report != null) {
			report.setComponentList(getReportComponentListForReport(id));
		}
		return report;
	}


	@Override
	public Report getReportByNameAndTemplate(String reportName, String templateName) {
		ReportTemplate template = getReportTemplateService().getReportTemplateByName(templateName);
		Report result = getReportByNameAndTemplateCache().getBeanForKeyValues(getReportDAO(), reportName, template.getId());
		if (result != null) {
			result.setComponentList(getReportComponentListForReport(result.getId()));
		}
		return result;
	}


	@Override
	public List<Report> getReportList() {
		return getReportDAO().findAll();
	}


	@Override
	public List<Report> getReportList(ReportSearchForm searchForm) {
		return getReportDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public Report saveReport(Report bean) {
		// save report along with its components
		List<ReportComponent> newComponents = bean.getComponentList();
		List<ReportComponent> oldComponents = null;
		if (!bean.isNewBean()) {
			oldComponents = getReportComponentListForReport(bean.getId());
		}

		for (ReportComponent component : CollectionUtils.getIterable(newComponents)) {
			component.setReport(bean);
		}

		bean = getReportDAO().save(bean);

		getReportComponentDAO().saveList(newComponents);
		// separate delete in order to delete related elements too
		for (ReportComponent old : CollectionUtils.getIterable(oldComponents)) {
			if (newComponents == null || !newComponents.contains(old)) {
				deleteReportComponent(old.getId());
			}
		}
		bean.setComponentList(newComponents);
		return bean;
	}


	@Override
	@Transactional
	public void deleteReport(int id) {
		Report report = getReport(id);

		for (ReportComponent reportComponent : CollectionUtils.getIterable(report.getComponentList())) {
			deleteReportComponent(reportComponent.getId());
		}
		getReportDAO().delete(report);
	}


	@Override
	@Transactional
	public Report copyReport(int reportId, String name) {
		// 1. save a copy of Report with new name
		Report from = getReportDAO().findByPrimaryKey(reportId);
		Report report = BeanUtils.cloneBean(from, false, false);
		report.setName(name);
		getReportDAO().save(report);

		// 2. save a copy of report components with parameters
		List<ReportComponent> fromComponents = getReportComponentListForReport(reportId);
		if (CollectionUtils.getSize(fromComponents) > 0) {
			List<ReportComponent> toComponents = new ArrayList<>();
			report.setComponentList(toComponents);
			for (ReportComponent fromComponent : fromComponents) {
				ReportComponent toComponent = BeanUtils.cloneBean(fromComponent, false, false);
				toComponent.setReport(report);
				List<ReportComponentParameter> fromParameters = getReportComponentParameterDAO().findByField("reportComponent.id", fromComponent.getId());
				if (!CollectionUtils.isEmpty(fromParameters)) {
					List<ReportComponentParameter> toParameters = new ArrayList<>();
					toComponent.setComponentParameterList(toParameters);
					for (ReportComponentParameter fromParam : fromParameters) {
						ReportComponentParameter toParam = BeanUtils.cloneBean(fromParam, false, false);
						toParam.setReportComponent(toComponent);
						toParameters.add(toParam);
					}
				}
				saveReportComponent(toComponent);
			}
		}

		// 3. put newly created report in the same folder
		SystemHierarchyLink link = CollectionUtils.getOnlyElement(getSystemHierarchyAssignmentService().getSystemHierarchyLinkList("Report", reportId, null));
		if (link != null) {
			getSystemHierarchyAssignmentService().saveSystemHierarchyLink("Report", report.getId(), link.getHierarchy().getId());
		}

		return report;
	}

	////////////////////////////////////////////////////////////////////////////
	////////             Report Component Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReportComponent getReportComponent(int id) {
		ReportComponent reportComponent = getReportComponentDAO().findByPrimaryKey(id);
		if (reportComponent != null) {
			reportComponent.setComponentParameterList(getReportComponentParameterDAO().findByField("reportComponent.id", id));
		}
		return reportComponent;
	}


	private List<ReportComponent> getReportComponentListForReport(int reportId) {
		return getReportComponentByReportCache().getBeanListForKeyValue(getReportComponentDAO(), reportId);
	}


	@Override
	public List<ReportComponent> getReportComponentListNotOfType(int reportId, final String typeName) {
		List<ReportComponent> list = getReportComponentListForReport(reportId);
		// Use Bean Utils for Filter Out Anything that Does NOT match given type
		return BeanUtils.filter(list, reportComponent -> !StringUtils.isEqual(typeName, reportComponent.getTemplateComponent().getReportTemplateComponentType().getName()));
	}


	@Override
	@Transactional
	public ReportComponent saveReportComponent(ReportComponent bean) {
		// save report component along with its parameters
		List<ReportComponentParameter> newParameters = bean.getComponentParameterList();
		List<ReportComponentParameter> oldParameters = null;
		if (!bean.isNewBean()) {
			oldParameters = getReportComponentParameterDAO().findByField("reportComponent.id", bean.getId());
		}

		for (ReportComponentParameter parameter : CollectionUtils.getIterable(newParameters)) {
			parameter.setReportComponent(bean);
		}

		bean = getReportComponentDAO().save(bean);
		getReportComponentParameterDAO().saveList(newParameters, oldParameters);
		bean.setComponentParameterList(newParameters);
		return bean;
	}


	@Override
	@Transactional
	public void deleteReportComponent(int id) {
		ReportComponent reportComponent = getReportComponent(id);
		getReportComponentParameterDAO().deleteList(reportComponent.getComponentParameterList());
		getReportComponentDAO().delete(reportComponent);
	}

	////////////////////////////////////////////////////////////////////////////
	////////         Report Component Parameter Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReportComponentParameter getReportComponentParameterByComponentAndName(final int componentId, final String parameterName) {
		ReportComponentParameterSearchForm searchForm = new ReportComponentParameterSearchForm();
		searchForm.setReportComponentId(componentId);
		searchForm.setTemplateComponentParameterName(parameterName);
		return CollectionUtils.getOnlyElement(getReportComponentParameterList(searchForm));
	}


	@Override
	public ReportComponentParameter getReportComponentParameterByComponentAndTemplate(final int componentId, final int reportTemplateComponentParameterId) {
		ReportComponentParameterSearchForm searchForm = new ReportComponentParameterSearchForm();
		searchForm.setReportComponentId(componentId);
		searchForm.setTemplateComponentParameterId(reportTemplateComponentParameterId);
		return CollectionUtils.getOnlyElement(getReportComponentParameterList(searchForm));
	}


	@Override
	public List<ReportComponentParameter> getReportComponentParameterList(ReportComponentParameterSearchForm searchForm) {
		return getReportComponentParameterDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<Report, Criteria> getReportDAO() {
		return this.reportDAO;
	}


	public void setReportDAO(AdvancedUpdatableDAO<Report, Criteria> reportDAO) {
		this.reportDAO = reportDAO;
	}


	public String getReportUrlRoot() {
		return this.reportUrlRoot;
	}


	public void setReportUrlRoot(String reportUrlRoot) {
		this.reportUrlRoot = reportUrlRoot;
	}


	public AdvancedUpdatableDAO<ReportComponent, Criteria> getReportComponentDAO() {
		return this.reportComponentDAO;
	}


	public void setReportComponentDAO(AdvancedUpdatableDAO<ReportComponent, Criteria> reportComponentDAO) {
		this.reportComponentDAO = reportComponentDAO;
	}


	public AdvancedUpdatableDAO<ReportComponentParameter, Criteria> getReportComponentParameterDAO() {
		return this.reportComponentParameterDAO;
	}


	public void setReportComponentParameterDAO(AdvancedUpdatableDAO<ReportComponentParameter, Criteria> reportComponentParameterDAO) {
		this.reportComponentParameterDAO = reportComponentParameterDAO;
	}


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}


	public ReportTemplateService getReportTemplateService() {
		return this.reportTemplateService;
	}


	public void setReportTemplateService(ReportTemplateService reportTemplateService) {
		this.reportTemplateService = reportTemplateService;
	}


	public DaoSingleKeyListCache<ReportComponent, Integer> getReportComponentByReportCache() {
		return this.reportComponentByReportCache;
	}


	public void setReportComponentByReportCache(DaoSingleKeyListCache<ReportComponent, Integer> reportComponentByReportCache) {
		this.reportComponentByReportCache = reportComponentByReportCache;
	}


	public DaoCompositeKeyCache<Report, String, Short> getReportByNameAndTemplateCache() {
		return this.reportByNameAndTemplateCache;
	}


	public void setReportByNameAndTemplateCache(DaoCompositeKeyCache<Report, String, Short> reportByNameAndTemplateCache) {
		this.reportByNameAndTemplateCache = reportByNameAndTemplateCache;
	}
}
