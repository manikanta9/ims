package com.clifton.report.definition.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.report.definition.Report;
import org.springframework.stereotype.Component;


/**
 * The <code>ReportByNameAndTemplateCache</code> caches Report objects by Unique properties of Name and Template
 *
 * @author manderson
 */
@Component
public class ReportByNameAndTemplateCache extends SelfRegisteringCompositeKeyDaoCache<Report, String, Short> {

	@Override
	protected String getBeanKey1Property() {
		return "name";
	}


	@Override
	protected String getBeanKey2Property() {
		return "reportTemplate.id";
	}


	@Override
	protected String getBeanKey1Value(Report bean) {
		return bean.getName();
	}


	@Override
	protected Short getBeanKey2Value(Report bean) {
		if (bean.getReportTemplate() != null) {
			return bean.getReportTemplate().getId();
		}
		return null;
	}
}
