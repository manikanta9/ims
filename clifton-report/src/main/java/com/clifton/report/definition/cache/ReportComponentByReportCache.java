package com.clifton.report.definition.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.report.definition.ReportComponent;
import org.springframework.stereotype.Component;


/**
 * The <code>ReportComponentByReportCache</code> stores the list of {@link ReportComponent} objects
 * by report id.
 *
 * @author manderson
 */
@Component
public class ReportComponentByReportCache extends SelfRegisteringSingleKeyDaoListCache<ReportComponent, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "report.id";
	}


	@Override
	protected Integer getBeanKeyValue(ReportComponent bean) {
		if (bean.getReport() != null) {
			return bean.getReport().getId();
		}
		return null;
	}
}
