package com.clifton.report.definition.template.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author vgomelsky
 */
public class ReportTemplateComponentSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;
	@SearchField
	private String description;

	@SearchField(searchField = "reportTemplate.id")
	private Short templateId;

	@SearchField
	private String externalReportName;

	@SearchField(searchField = "reportTemplateComponentType.id")
	private Short reportTemplateComponentTypeId;

	@SearchField
	private String excludeComponentLookupSql;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getTemplateId() {
		return this.templateId;
	}


	public void setTemplateId(Short templateId) {
		this.templateId = templateId;
	}


	public String getExternalReportName() {
		return this.externalReportName;
	}


	public void setExternalReportName(String externalReportName) {
		this.externalReportName = externalReportName;
	}


	public Short getReportTemplateComponentTypeId() {
		return this.reportTemplateComponentTypeId;
	}


	public void setReportTemplateComponentTypeId(Short reportTemplateComponentTypeId) {
		this.reportTemplateComponentTypeId = reportTemplateComponentTypeId;
	}


	public String getExcludeComponentLookupSql() {
		return this.excludeComponentLookupSql;
	}


	public void setExcludeComponentLookupSql(String excludeComponentLookupSql) {
		this.excludeComponentLookupSql = excludeComponentLookupSql;
	}
}
