package com.clifton.report.definition.template;


import com.clifton.system.userinterface.BaseNamedSystemUserInterfaceAwareEntity;
import com.clifton.system.userinterface.SystemUserInterfaceUsedByAware;
import com.clifton.system.userinterface.SystemUserInterfaceValueUsedByConfig;


/**
 * The <code>ReportTemplateComponentParameter</code> class represents a report section parameter that
 * will be passed from the host report to the report component.
 *
 * @author mwacker
 * @author vgomelsky
 */
public class ReportTemplateComponentParameter extends BaseNamedSystemUserInterfaceAwareEntity<Integer> implements SystemUserInterfaceUsedByAware {

	private ReportTemplateComponent reportTemplateComponent;

	/**
	 * Usually the name of this parameter is the same as this of the template.
	 * If it's different, specify template parameter name here which will be mapped to this component parameter.
	 */
	private String templateParameterName;

	/**
	 * Specifies whether this parameter value is static and can be specified for each ReportComponent
	 * vs dynamically passed parameter from the main report.
	 */
	private boolean staticValue;

	/**
	 * Default value for this parameters if staticValue == true.  Used only when corresponding ReportComponentParameter is not specified.
	 */
	private String defaultValue;
	private String defaultText;
	/**
	 * SQL statement used to look up value for the parameter.  The statement uses replacement strings for parameters.  For example: 'SELECT * FROM x WHERE x.ID = ${RunID} AND x.Date = ${BalanceDate}'
	 * would become 'SELECT * FROM x WHERE x.ID = ? AND x.Date = ?' and the values for ID and Date will be sent to the query in the order they were found.
	 */
	private String valueLookupSql;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemUserInterfaceValueUsedByConfig getSystemUserInterfaceValueConfig() {
		return new SystemUserInterfaceValueUsedByConfig("ReportComponentParameter", "parameterValue", "templateComponentParameter", "reportComponent", "ReportComponent", null);
	}


	@Override
	public String getUsedByLabel() {
		if (getReportTemplateComponent() != null) {
			return "Report Template Component: " + getReportTemplateComponent().getLabel() + "." + getLabel();
		}
		return "Report Template Component: [UNKNOWN Template Component] " + getLabel();
	}


	public ReportTemplateComponent getReportTemplateComponent() {
		return this.reportTemplateComponent;
	}


	public void setReportTemplateComponent(ReportTemplateComponent reportTemplateComponent) {
		this.reportTemplateComponent = reportTemplateComponent;
	}


	public boolean isStaticValue() {
		return this.staticValue;
	}


	public void setStaticValue(boolean staticValue) {
		this.staticValue = staticValue;
	}


	public String getDefaultValue() {
		return this.defaultValue;
	}


	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}


	public String getDefaultText() {
		return this.defaultText;
	}


	public void setDefaultText(String defaultText) {
		this.defaultText = defaultText;
	}


	public String getTemplateParameterName() {
		return this.templateParameterName;
	}


	public void setTemplateParameterName(String templateParameterName) {
		this.templateParameterName = templateParameterName;
	}


	public String getValueLookupSql() {
		return this.valueLookupSql;
	}


	public void setValueLookupSql(String valueLookupSql) {
		this.valueLookupSql = valueLookupSql;
	}
}
