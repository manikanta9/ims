package com.clifton.report.definition;


import com.clifton.report.definition.template.ReportTemplateParameter;
import com.clifton.report.definition.template.ReportTemplateParameterValue;
import com.clifton.report.definition.template.ReportTemplateService;
import com.clifton.system.userinterface.BaseSystemUserInterfaceBeanPropertyProvider;

import java.util.List;


/**
 * The <code>BaseReportBeanPropertyProvider</code> implements a base bean property provider for Reports.
 *
 * @author mwacker
 */
public class BaseReportBeanPropertyProvider extends BaseSystemUserInterfaceBeanPropertyProvider<ReportTemplateParameter, ReportTemplateParameterValue> {

	private Integer reportId;

	////////////////////////////////////////////////////////////////////////////

	private Report report;

	private ReportDefinitionService reportDefinitionService;
	private ReportTemplateService reportTemplateService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ReportTemplateParameter> doGetPropertyTypeList() {
		return getReportTemplateService().getReportTemplateParameterListByTemplate(getReport().getReportTemplate().getId());
	}


	@Override
	public Class<ReportTemplateParameterValue> getValueClass() {
		return ReportTemplateParameterValue.class;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Report getReport() {
		if ((this.report == null) && (getReportId() != null)) {
			this.report = getReportDefinitionService().getReport(getReportId());
		}
		return this.report;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getReportId() {
		return this.reportId;
	}


	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}


	public ReportDefinitionService getReportDefinitionService() {
		return this.reportDefinitionService;
	}


	public void setReportDefinitionService(ReportDefinitionService reportDefinitionService) {
		this.reportDefinitionService = reportDefinitionService;
	}


	public ReportTemplateService getReportTemplateService() {
		return this.reportTemplateService;
	}


	public void setReportTemplateService(ReportTemplateService reportTemplateService) {
		this.reportTemplateService = reportTemplateService;
	}
}
