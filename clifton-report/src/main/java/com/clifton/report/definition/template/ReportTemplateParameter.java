package com.clifton.report.definition.template;


import com.clifton.system.userinterface.BaseNamedSystemUserInterfaceAwareVirtualEntity;
import com.clifton.system.userinterface.SystemUserInterfaceAwareVirtual;


public class ReportTemplateParameter extends BaseNamedSystemUserInterfaceAwareVirtualEntity<SystemUserInterfaceAwareVirtual> {

	private ReportTemplate template;

	/**
	 * Usually the name of this parameter is the same as this of the template.
	 * If it's different, specify template parameter name here which will be mapped to this component parameter.
	 */
	private String templateParameterName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ReportTemplate getTemplate() {
		return this.template;
	}


	public void setTemplate(ReportTemplate template) {
		this.template = template;
	}


	public String getTemplateParameterName() {
		return this.templateParameterName;
	}


	public void setTemplateParameterName(String templateParameterName) {
		this.templateParameterName = templateParameterName;
	}
}
