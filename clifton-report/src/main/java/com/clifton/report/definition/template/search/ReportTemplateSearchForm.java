package com.clifton.report.definition.template.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;

import java.math.BigDecimal;


public class ReportTemplateSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String url;

	@SearchField
	private BigDecimal subReportWidth;

	@SearchField(searchField = "reportList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer reportId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "ReportTemplate";
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getUrl() {
		return this.url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public BigDecimal getSubReportWidth() {
		return this.subReportWidth;
	}


	public void setSubReportWidth(BigDecimal subReportWidth) {
		this.subReportWidth = subReportWidth;
	}


	public Integer getReportId() {
		return this.reportId;
	}


	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}
}
