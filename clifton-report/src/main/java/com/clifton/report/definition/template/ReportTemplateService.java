package com.clifton.report.definition.template;

import com.clifton.report.definition.template.search.ReportTemplateComponentSearchForm;
import com.clifton.report.definition.template.search.ReportTemplateComponentTypeSearchForm;
import com.clifton.report.definition.template.search.ReportTemplateSearchForm;

import java.util.List;


/**
 * @author vgomelsky
 */
public interface ReportTemplateService {

	////////////////////////////////////////////////////////////////////////////
	//////                   Report Template Methods                     ///////
	////////////////////////////////////////////////////////////////////////////


	public ReportTemplate getReportTemplate(short id);


	public ReportTemplate getReportTemplateByName(String name);


	public List<ReportTemplate> getReportTemplateList(ReportTemplateSearchForm searchForm);


	public ReportTemplate saveReportTemplate(ReportTemplate bean);


	public void deleteReportTemplate(short id);


	////////////////////////////////////////////////////////////////////////////
	//////              Report Template Parameter Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	public ReportTemplateParameter getReportTemplateParameter(int id);


	public List<ReportTemplateParameter> getReportTemplateParameterListByTemplate(short templateId);


	public ReportTemplateParameter saveReportTemplateParameter(ReportTemplateParameter bean);


	public void deleteReportTemplateParameter(int id);


	////////////////////////////////////////////////////////////////////////////
	//////              Report Template Component Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	public ReportTemplateComponent getReportTemplateComponent(int id);


	public List<ReportTemplateComponent> getReportTemplateComponentList(ReportTemplateComponentSearchForm searchForm);


	public List<ReportTemplateComponent> getReportTemplateComponentListByTemplate(short templateId);


	public ReportTemplateComponent saveReportTemplateComponent(ReportTemplateComponent bean);


	public void deleteReportTemplateComponent(int id);


	////////////////////////////////////////////////////////////////////////////
	//////              Report Template Component Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	public ReportTemplateComponentParameter getReportTemplateComponentParameter(int id);


	public List<ReportTemplateComponentParameter> getReportTemplateComponentParameterListByTemplateComponent(int templateComponentId, Boolean staticValue);


	public ReportTemplateComponentParameter saveReportTemplateComponentParameter(ReportTemplateComponentParameter bean);


	public void deleteReportTemplateComponentParameter(int id);


	////////////////////////////////////////////////////////////////////////////
	//////           Report Template Component Type Methods              ///////
	////////////////////////////////////////////////////////////////////////////


	public List<ReportTemplateComponentType> getReportTemplateComponentTypeList(ReportTemplateComponentTypeSearchForm searchForm);
}
