package com.clifton.report.definition.template;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>ReportTemplateComponent</code> class represents a report section that can be used with the
 * corresponding template.
 *
 * @author vgomelsky
 */
public class ReportTemplateComponent extends NamedEntity<Integer> {

	private ReportTemplate reportTemplate;
	private String externalReportName;
	private ReportTemplateComponentType reportTemplateComponentType;

	/**
	 * SQL statement used to look up a bit value to determine weather or not to include the component.  Result of
	 * true will exclude the component from the report.
	 */
	private String excludeComponentLookupSql;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ReportTemplate getReportTemplate() {
		return this.reportTemplate;
	}


	public void setReportTemplate(ReportTemplate reportTemplate) {
		this.reportTemplate = reportTemplate;
	}


	public String getExternalReportName() {
		return this.externalReportName;
	}


	public void setExternalReportName(String externalReportName) {
		this.externalReportName = externalReportName;
	}


	public ReportTemplateComponentType getReportTemplateComponentType() {
		return this.reportTemplateComponentType;
	}


	public void setReportTemplateComponentType(ReportTemplateComponentType reportTemplateComponentType) {
		this.reportTemplateComponentType = reportTemplateComponentType;
	}


	public String getExcludeComponentLookupSql() {
		return this.excludeComponentLookupSql;
	}


	public void setExcludeComponentLookupSql(String excludeComponentLookupSql) {
		this.excludeComponentLookupSql = excludeComponentLookupSql;
	}
}
