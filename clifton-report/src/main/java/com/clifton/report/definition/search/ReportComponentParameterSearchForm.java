package com.clifton.report.definition.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class ReportComponentParameterSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "templateComponentParameter.name,parameterValue,parameterText")
	private String searchPattern;


	@SearchField(searchField = "reportTemplate.id", searchFieldPath = "templateComponentParameter.reportTemplateComponent")
	private Short templateId;

	@SearchField(searchField = "name", searchFieldPath = "reportComponent.report")
	private String reportName;

	@SearchField(searchField = "templateComponent.id", searchFieldPath = "reportComponent")
	private Integer templateComponentId;

	@SearchField(searchField = "name", searchFieldPath = "reportComponent.templateComponent")
	private String templateComponentName;

	@SearchField(searchField = "reportComponent.id")
	private Integer reportComponentId;

	@SearchField(searchField = "name", searchFieldPath = "templateComponentParameter")
	private String templateComponentParameterName;

	@SearchField(searchField = "templateComponentParameter.id")
	private Integer templateComponentParameterId;

	@SearchField
	private String parameterValue;


	@SearchField
	private String parameterText;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getReportComponentId() {
		return this.reportComponentId;
	}


	public void setReportComponentId(Integer reportComponentId) {
		this.reportComponentId = reportComponentId;
	}


	public String getTemplateComponentParameterName() {
		return this.templateComponentParameterName;
	}


	public void setTemplateComponentParameterName(String templateComponentParameterName) {
		this.templateComponentParameterName = templateComponentParameterName;
	}


	public Integer getTemplateComponentParameterId() {
		return this.templateComponentParameterId;
	}


	public void setTemplateComponentParameterId(Integer templateComponentParameterId) {
		this.templateComponentParameterId = templateComponentParameterId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getTemplateId() {
		return this.templateId;
	}


	public void setTemplateId(Short templateId) {
		this.templateId = templateId;
	}


	public String getReportName() {
		return this.reportName;
	}


	public void setReportName(String reportName) {
		this.reportName = reportName;
	}


	public String getTemplateComponentName() {
		return this.templateComponentName;
	}


	public void setTemplateComponentName(String templateComponentName) {
		this.templateComponentName = templateComponentName;
	}


	public String getParameterValue() {
		return this.parameterValue;
	}


	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}


	public String getParameterText() {
		return this.parameterText;
	}


	public void setParameterText(String parameterText) {
		this.parameterText = parameterText;
	}


	public Integer getTemplateComponentId() {
		return this.templateComponentId;
	}


	public void setTemplateComponentId(Integer templateComponentId) {
		this.templateComponentId = templateComponentId;
	}
}
