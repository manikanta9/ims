package com.clifton.report.definition.template;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>ReportTemplateComponent</code> class represents a report section type.
 *
 * @author mwacker
 */
public class ReportTemplateComponentType extends NamedEntity<Short> {
	//
}
