package com.clifton.report.definition.template;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.system.userinterface.SystemUserInterfaceValueAware;


/**
 * The <code>ReportTemplateParameterValue</code> stores report template parameter values.
 *
 * @author mwacker
 */
@NonPersistentObject
public class ReportTemplateParameterValue implements SystemUserInterfaceValueAware<ReportTemplateParameter> {

	private ReportTemplateParameter parameter;

	private String value;
	private String text;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReportTemplateParameter getParameter() {
		return this.parameter;
	}


	public void setParameter(ReportTemplateParameter parameter) {
		this.parameter = parameter;
	}


	@Override
	public String getValue() {
		return this.value;
	}


	@Override
	public void setValue(String value) {
		this.value = value;
	}


	@Override
	public String getText() {
		return this.text;
	}


	@Override
	public void setText(String text) {
		this.text = text;
	}
}
