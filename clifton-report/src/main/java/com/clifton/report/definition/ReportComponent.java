package com.clifton.report.definition;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.util.StringUtils;
import com.clifton.report.definition.template.ReportTemplateComponent;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ReportComponent</code> class represents a reporting component.
 *
 * @author vgomelsky
 */
public class ReportComponent extends NamedEntity<Integer> implements SystemEntityModifyConditionAware {

	private Report report;
	private ReportTemplateComponent templateComponent;

	private int order;
	/**
	 * Specifies whether this component is "private": can easily be excluded from a report when
	 * distributing to clients (set ExcludePrivate parameter to true).
	 */
	private boolean privateComponent;
	/**
	 * Insert page break before this section.
	 */
	private boolean pageBreakBefore;
	/**
	 * Optionally add the following top margin to this section (used to add spacing before).
	 */
	private BigDecimal topMargin;


	private List<ReportComponentParameter> componentParameterList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (StringUtils.isEmpty(getName())) {
			if (getTemplateComponent() != null) {
				return getTemplateComponent().getName();
			}
		}
		return getName();
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return getReport().getEntityModifyCondition();
	}


	public Report getReport() {
		return this.report;
	}


	public void setReport(Report report) {
		this.report = report;
	}


	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}


	public boolean isPageBreakBefore() {
		return this.pageBreakBefore;
	}


	public void setPageBreakBefore(boolean pageBreakBefore) {
		this.pageBreakBefore = pageBreakBefore;
	}


	public ReportTemplateComponent getTemplateComponent() {
		return this.templateComponent;
	}


	public void setTemplateComponent(ReportTemplateComponent templateComponent) {
		this.templateComponent = templateComponent;
	}


	public BigDecimal getTopMargin() {
		return this.topMargin;
	}


	public void setTopMargin(BigDecimal topMargin) {
		this.topMargin = topMargin;
	}


	public List<ReportComponentParameter> getComponentParameterList() {
		return this.componentParameterList;
	}


	public void setComponentParameterList(List<ReportComponentParameter> componentParameterList) {
		this.componentParameterList = componentParameterList;
	}


	public boolean isPrivateComponent() {
		return this.privateComponent;
	}


	public void setPrivateComponent(boolean privateComponent) {
		this.privateComponent = privateComponent;
	}
}
