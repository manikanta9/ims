package com.clifton.report.definition.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;


/**
 * The <code>ReportSearchForm</code> class defines search configuration for Report objects.
 *
 * @author vgomelsky
 */
public class ReportSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "reportTemplate.id")
	private Short templateId;

	@SearchField(searchField = "name", searchFieldPath = "reportTemplate", comparisonConditions = {ComparisonConditions.EQUALS})
	private String templateName;

	@SearchField(searchField = "componentList.templateComponent.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer templateComponentId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "Report";
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getTemplateName() {
		return this.templateName;
	}


	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}


	public Short getTemplateId() {
		return this.templateId;
	}


	public void setTemplateId(Short templateId) {
		this.templateId = templateId;
	}


	public Integer getTemplateComponentId() {
		return this.templateComponentId;
	}


	public void setTemplateComponentId(Integer templateComponentId) {
		this.templateComponentId = templateComponentId;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
