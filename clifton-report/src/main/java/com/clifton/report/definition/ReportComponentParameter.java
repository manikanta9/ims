package com.clifton.report.definition;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.report.definition.template.ReportTemplateComponentParameter;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>ReportComponentParameter</code> class represents a single value of a report component parameter.
 * Some ReportComponent objects allow customization by static parameters (show/hide certain section, etc.)
 *
 * @author vgomelsky
 */
public class ReportComponentParameter extends BaseEntity<Integer> implements LabeledObject, SystemEntityModifyConditionAware {

	private ReportComponent reportComponent;
	private ReportTemplateComponentParameter templateComponentParameter;

	private String parameterValue;
	private String parameterText;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (this.templateComponentParameter != null) {
			return this.templateComponentParameter.getLabel() + " = " + this.parameterText;
		}
		return this.parameterText;
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return getReportComponent().getReport().getEntityModifyCondition();
	}


	public ReportComponent getReportComponent() {
		return this.reportComponent;
	}


	public void setReportComponent(ReportComponent reportComponent) {
		this.reportComponent = reportComponent;
	}


	public ReportTemplateComponentParameter getTemplateComponentParameter() {
		return this.templateComponentParameter;
	}


	public void setTemplateComponentParameter(ReportTemplateComponentParameter templateComponentParameter) {
		this.templateComponentParameter = templateComponentParameter;
	}


	public String getParameterValue() {
		return this.parameterValue;
	}


	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}


	public String getParameterText() {
		return this.parameterText;
	}


	public void setParameterText(String parameterText) {
		this.parameterText = parameterText;
	}
}
