package com.clifton.report.export;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.report.definition.Report;

import java.util.Map;


public interface ReportExportService {

	/**
	 * Runs a report and creates a temporary PDF file with the result.
	 *
	 * @return Returns the file path of the temporary PDF file.
	 */
	@DoNotAddRequestMapping
	public FileWrapper getReportPDFFile(ReportExportConfiguration config);


	/**
	 * Clear any cached report file(s) to a report.
	 */
	@SecureMethod(dtoClass = Report.class)
	public void clearReportPDFFileCache(Integer reportId, Map<String, Object> parameters);


	/**
	 * Allows exporting a single report or multiple reports for
	 *
	 * @param config
	 */
	@SecureMethod(dtoClass = Report.class)
	public FileWrapper downloadReportFile(ReportExportConfiguration config);


	@DoNotAddRequestMapping
	public String buildUrl(int reportId, Map<String, Object> parameters, String exportFormat);
}
