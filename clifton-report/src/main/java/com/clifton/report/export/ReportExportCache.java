package com.clifton.report.export;

import java.io.File;
import java.util.Map;


/**
 * The <code>ReportExportCacheImpl</code> maintains a list of cached report temporary files, manages the deletion when the cache is cleared or
 * a file is removed.
 *
 * @author mwacker
 */
public interface ReportExportCache {


	public String getCacheKey(Integer reportId, Map<String, Object> inputParameters);


	public void removeCacheKey(String key);


	public void setFileForCacheKey(String key, File file);


	public File getFileForCacheKey(String key);
}
