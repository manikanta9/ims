package com.clifton.report.export;


import com.clifton.core.cache.CacheEventTypes;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ReportExportCacheImpl</code> maintains a list of cached report temporary files, manages the deletion when the cache is cleared or
 * a file is removed.
 *
 * @author mwacker
 */
@Component
public class ReportExportCacheImpl implements CurrentContextApplicationListener<ContextRefreshedEvent>, CustomCache<String, String>, ReportExportCache {

	public static final String CACHED_REPORT_FILE_PREFIX = "ReportCache";

	/**
	 * Cache handler to store ReportCacheFile objects by ReportID.
	 */
	private CacheHandler<String, String> cacheHandler;

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		final ReportExportCacheImpl reportCache = this;
		getCacheHandler().registerEventListener(getCacheName(), CacheEventTypes.BEFORE_CLEAR, (cacheName, key, value) -> reportCache.clearFiles());
		getCacheHandler().registerEventListener(getCacheName(), CacheEventTypes.BEFORE_REMOVE, (cacheName, key, value) -> reportCache.removeFile(key));
	}


	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void setFileForCacheKey(String key, File file) {
		File existingFile = getFileForCacheKey(key);
		if ((existingFile != null) && (existingFile.exists())) {
			removeFile(key);
		}
		getCacheHandler().put(getCacheName(), key, file.getAbsolutePath());
	}


	@Override
	public File getFileForCacheKey(String key) {
		String filePath = getCacheHandler().get(getCacheName(), key);
		return !StringUtils.isEmpty(filePath) ? new File(filePath) : null;
	}


	@Override
	public void removeCacheKey(String key) {
		getCacheHandler().remove(getCacheName(), key);
	}


	@Override
	public String getCacheKey(Integer reportId, Map<String, Object> inputParameters) {
		ValidationUtils.assertTrue(inputParameters instanceof LinkedHashMap, "Input parameter must be of type [" + LinkedHashMap.class + "].");
		List<String> keyValues = new ArrayList<>();
		keyValues.add("reportId=" + reportId);
		for (Map.Entry<String, Object> stringObjectEntry : inputParameters.entrySet()) {
			String parameterValue = stringObjectEntry.getValue().toString();
			keyValues.add(stringObjectEntry.getKey() + "=" + parameterValue);
		}
		Collections.sort(keyValues);
		return FileUtils.replaceInvalidCharacters(StringUtils.join(keyValues, "_"), null);
	}


	/**
	 * Used by the 'Clear reporting cache' batch job to clear the cache and files.
	 */
	public void clear() {
		getCacheHandler().clear(getCacheName());
	}

	////////////////////////////////////////////////////////////////////////////////


	private void removeFile(String key) {
		String fileName = getCacheHandler().get(getCacheName(), key);
		if (!StringUtils.isEmpty(fileName)) {
			File file = new File(fileName);
			if (file.exists() && file.canWrite()) {
				try {
					FileUtils.delete(file);
				}
				catch (Throwable e) {
					LogUtils.error(getClass(), "Failed to delete cached report file [" + file + "].", e);
				}
			}
		}
	}


	private void clearFiles() {
		File file = null;
		try {
			file = File.createTempFile("location", "test");
			String directory = FileUtils.getFilePath(file.getAbsolutePath());
			File folder = new File(directory);
			File[] listOfFiles = folder.listFiles((dir, name) -> name.contains(CACHED_REPORT_FILE_PREFIX));
			AssertUtils.assertNotNull(listOfFiles, "List of cached report files was null.");
			for (File fileOrDirectory : listOfFiles) {
				if (fileOrDirectory.isFile()) {
					tryFileDelete(fileOrDirectory);
				}
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Error deleting cached report files.", e);
		}
		finally {
			// delete the temp file
			if (file != null && file.exists()) {
				file.delete();
			}
		}
	}


	private void tryFileDelete(File file) {
		try {
			FileUtils.delete(file);
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Failed to delete cached report file [" + file + "].", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<String, String> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, String> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
