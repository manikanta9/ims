package com.clifton.report.export;


import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlParameterValue;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.logging.LoggableException;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.ZipUtils;
import com.clifton.core.util.http.HttpUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.report.definition.Report;
import com.clifton.report.definition.ReportComponent;
import com.clifton.report.definition.ReportComponentParameter;
import com.clifton.report.definition.ReportDefinitionService;
import com.clifton.report.definition.template.ReportTemplate;
import com.clifton.report.definition.template.ReportTemplateComponentParameter;
import com.clifton.report.definition.template.ReportTemplateService;
import com.clifton.report.params.ReportConfigParameter;
import com.clifton.report.params.ReportParameterConfigurer;
import com.clifton.report.params.ReportParameterConfigurerLocator;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class ReportExportServiceImpl<T> implements ReportExportService {

	private static final String DEFAULT_FILENAME = "Report";

	private ReportDefinitionService reportDefinitionService;
	private ReportTemplateService reportTemplateService;
	private ReportParameterConfigurerLocator<T> reportParameterConfigurerLocator;

	private SqlHandler sqlHandler;

	private ReportExportCache reportExportCache;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public FileWrapper downloadReportFile(ReportExportConfiguration config) {
		Report report = null;
		if (config.getReportId() != null) {
			report = getReportDefinitionService().getReport(config.getReportId());
		}
		File file = doDownloadReportFile(config);
		return createFileWrapper(report, file, config);
	}


	@Override
	public String buildUrl(int reportId, Map<String, Object> parameters, String exportFormat) {
		ValidationUtils.assertTrue(parameters instanceof LinkedHashMap, "Input parameter must be of type [" + LinkedHashMap.class + "].");
		Report report = getReportDefinitionService().getReport(reportId);
		return buildUrl(report, parameters, null, exportFormat);
	}


	@Override
	public void clearReportPDFFileCache(Integer reportId, Map<String, Object> parameters) {
		parameters.put("HidePageNumbers", true);
		String cacheKey = getReportExportCache().getCacheKey(reportId, parameters);
		getReportExportCache().removeCacheKey(cacheKey);
		parameters.put("HidePageNumbers", false);
		cacheKey = getReportExportCache().getCacheKey(reportId, parameters);
		getReportExportCache().removeCacheKey(cacheKey);
	}


	@Override
	public FileWrapper getReportPDFFile(ReportExportConfiguration config) {
		ValidationUtils.assertNotNull(config.getReportId(), "Report selection is required");
		// Always want a PDF format here
		config.setExportFormat(FileFormats.PDF);
		return downloadReportFile(config);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private File doDownloadReportFile(ReportExportConfiguration config) {
		// only call multiple times if the exporting pdf files
		ReportConfigParameter additionalParam = config.getAdditionalReportParameter();

		if (additionalParam != null && !StringUtils.isEmpty(additionalParam.getValue()) && !StringUtils.isEmpty(additionalParam.getName())) {

			ReportParameterConfigurer<T> configurer = getReportParameterConfigurerLocator().locate(additionalParam.getName());
			List<T> values = configurer.getReportParameterValueList(additionalParam, config.getReportingDate());

			Map<Integer, Report> reportMap = new HashMap<>();
			ReportTemplate template = null;

			if (!CollectionUtils.isEmpty(values)) {
				// If Only One - Don't consider it a concatenation or zip file
				if (values.size() == 1) {
					T value = values.get(0);
					Report report = getReport(configurer.getReportIdForValue(config, value), reportMap);
					template = report.getReportTemplate();
					boolean useCache = (template != null && !config.isCacheIgnored() && template.isReportCachingUsed());
					configurer.populateReportParameterMap(config.getParameterMap(), value);
					try {
						return doGetReportFileIncludeAttachments(report, config, configurer, value, false, useCache);
					}
					catch (Exception ex) {
						throw new LoggableException("Failed to export report [" + report.getName() + "].", ex);
					}
				}
				else {
					// Otherwise for multiple files they can be:
					// (1) zipped in any format (attachments are concatenated to each report if the attachment is in PDF format)
					// (2) concatenation is ONLY allowed for PDF rendering
					if (config.getExportFormat() == FileFormats.PDF || config.isZipFiles()) {
						StringBuilder message = new StringBuilder();

						// maps a ReportParameterConfigurer value to the actual File
						// we'll need the actual value later to generate the individual file names when zipping
						Map<T, File> valueToFileMap = new LinkedHashMap<>();

						// set the page numbering parameters
						boolean hideAllPageNumbers = !config.isKeepSubReportPageNumbers() && values.size() > 1;

						// (1) retrieve the actual report File for the selected rows
						for (T value : values) {
							Report report = getReport(configurer.getReportIdForValue(config, value), reportMap);
							if (template == null) {
								template = report.getReportTemplate();
							}
							try {
								configurer.populateReportParameterMap(config.getParameterMap(), value);
								boolean useCache = (template != null && !config.isCacheIgnored() && template.isReportCachingUsed());
								File f = doGetReportFileIncludeAttachments(report, config, configurer, value, hideAllPageNumbers, useCache);

								if (f != null) {
									valueToFileMap.put(value, f);
								}
							}
							catch (Exception ex) {
								String exMsg = "Failed to export report [" + (report == null ? "" : report.getName()) + "] for [" + configurer.getLabelForValue(value) + "]";
								LogUtils.error(getClass(), exMsg, ex);
								message.append(new RuntimeException(exMsg + "\n", ex));
							}
						}
						if ((message.length() > 0) && config.isThrowErrorOnBadReport()) {
							throw new RuntimeException(message.toString());
						}

						// (2) zip the files or concatenate PDFs
						boolean useCache = (template != null && !config.isCacheIgnored() && template.isReportCachingUsed());
						File file;
						if (config.isZipFiles()) {
							// zip the files and find the original file names
							File firstFile = valueToFileMap.values().stream().findFirst().orElse(null);
							ValidationUtils.assertNotNull(firstFile, "Could not find a file to zip");
							String zippedFilePath = FileUtils.getFilePath(firstFile.getAbsolutePath()).concat(".zip");

							// generate the appropriate file name for each report and create a FileWrapper so we can zip them
							List<FileWrapper> fileWrapperList = valueToFileMap.entrySet().stream().map(entry -> {
								// prepare the file name and extension
								String fileName = configurer.generateFileNameWithoutExtension(entry.getKey(), config.getReportingDate());
								String extension = ".".concat(FileFormats.PDF.getFormatString());
								return new FileWrapper(entry.getValue(), fileName.concat(extension), true);
							}).collect(Collectors.toList());
							file = new File(ZipUtils.zipFiles(fileWrapperList, zippedFilePath).getAbsolutePath());
						}
						else {
							// concatenate PDFs
							file = FileUtils.concatenatePDFs(DEFAULT_FILENAME, FileFormats.PDF.getExtension(), new ArrayList<>(valueToFileMap.values()), hideAllPageNumbers, !useCache);
						}
						file.deleteOnExit();
						return file;
					}
					throw new ValidationException("Multiple reports can only be concatenated when rendering to pdf or zipped.");
				}
			}
		}
		else if (config.getReportId() != null) {
			// no additional values - just export the report
			Report report = getReportDefinitionService().getReport(config.getReportId());
			try {
				return doGetReportFile(report, config, false);
			}
			catch (Exception ex) {
				throw new LoggableException("Failed to export report [" + (report == null ? "" : report.getName()) + "].", ex);
			}
		}
		throw new ValidationException("No reports available for report parameters [" + config + "].");
	}


	private Report getReport(Integer reportId, Map<Integer, Report> reportMap) {
		if (reportMap != null) {
			if (reportMap.containsKey(reportId)) {
				return reportMap.get(reportId);
			}
		}
		Report report = getReportDefinitionService().getReport(reportId);
		if (reportMap != null) {
			reportMap.put(reportId, report);
		}
		return report;
	}


	private File doGetReportFileIncludeAttachments(Report report, ReportExportConfiguration config, ReportParameterConfigurer<T> configurer, T value, boolean hideAllPageNumbers, boolean useCache) {
		File reportFile = doGetReportFile(report, config, hideAllPageNumbers);
		// only retrieve attachments if the original is a PDF file
		if (reportFile != null && FileUtils.getFileExtension(reportFile.getName()).equalsIgnoreCase(FileFormats.PDF.getFormatString())) {
			FileWrapper attachments = configurer.getAttachments(value, config);
			if (attachments != null) {
				List<File> fileListWithAttachments = CollectionUtils.createList(reportFile);
				fileListWithAttachments.add(new File(attachments.getFile().getPath()));
				return FileUtils.concatenatePDFs(reportFile.getName(), FileFormats.PDF.getExtension(), fileListWithAttachments, hideAllPageNumbers, !useCache);
			}
		}
		return reportFile;
	}


	private File doGetReportFile(Report report, ReportExportConfiguration config, boolean hideAllPageNumbers) {
		Map<String, Object> inputParameters = config.getParameterMap();
		boolean useCache = !config.isCacheIgnored() && report.getReportTemplate().isReportCachingUsed();

		File file = null;
		// add the page number parameter to get the correct cache name
		inputParameters.put("HidePageNumbers", hideAllPageNumbers);
		String cacheKey = getReportExportCache().getCacheKey(report.getId(), inputParameters);
		if (useCache) {
			if (config.isRegenerateCache()) {
				getReportExportCache().removeCacheKey(cacheKey);
			}
			else {
				file = getReportExportCache().getFileForCacheKey(cacheKey);
			}
		}
		if ((file == null) || !file.exists()) {
			file = executeGetReportFile(report, inputParameters, config.isThrowErrorOnBadReport(), false, hideAllPageNumbers, config.getExportFormat(), useCache);
			if (useCache && file != null) {
				getReportExportCache().setFileForCacheKey(cacheKey, file);
			}
		}
		return file;
	}


	/**
	 * @param hideAllPageNumbers - overrides page number configuration for sub reports
	 */
	private File executeGetReportFile(Report report, Map<String, Object> inputParameters, boolean throwErrorOnBadReport, boolean recursiveCall, boolean hideAllPageNumbers,
	                                  FileFormats exportFormat, boolean useCache) {
		try {
			boolean excludePrivateItems = false;
			if (inputParameters.containsKey("ExcludePrivate")) {
				excludePrivateItems = (Boolean) inputParameters.get("ExcludePrivate");
			}
			// set hide page numbers, will be overridden for top level reports with hideAllPageNumbers argument
			inputParameters.put("HidePageNumbers", true);

			List<ReportComponent> nonStandardComponentList = getReportDefinitionService().getReportComponentListNotOfType(report.getId(), "Standard Component");
			File file;
			if (CollectionUtils.isEmpty(nonStandardComponentList) || (FileFormats.PDF != exportFormat)) {
				// if this is not part of larger report, keep the page numbers unless hide all is set
				if (!recursiveCall) {
					inputParameters.put("HidePageNumbers", hideAllPageNumbers);
				}
				String url = buildUrl(report, inputParameters, exportFormat.getFormatString());
				file = getReportFile(url, getCacheFileName(report.getId(), inputParameters, useCache), throwErrorOnBadReport, FileUtils.getExtensionForFormat(exportFormat.getFormatString()));
			}
			else {
				// get the fully populated object
				report = getReportDefinitionService().getReport(report.getId());
				List<File> files = new ArrayList<>();
				List<String> orderList = new ArrayList<>();
				int count = 0;
				for (ReportComponent component : CollectionUtils.getIterable(report.getComponentList())) {
					if ("Standard Component".equals(component.getTemplateComponent().getReportTemplateComponentType().getName())) {
						if (!component.isPrivateComponent() || !excludePrivateItems) {
							orderList.add(((Integer) component.getOrder()).toString());
						}
					}
					if (!"Standard Component".equals(component.getTemplateComponent().getReportTemplateComponentType().getName()) || (count >= report.getComponentList().size() - 1)) {
						// add the previous components
						if (!orderList.isEmpty()) {
							String url = buildUrl(report, inputParameters, orderList, null);
							File f = getReportFile(url, getCacheFileName(report.getId(), inputParameters, useCache), throwErrorOnBadReport, FileUtils.getExtensionForFormat(exportFormat.getFormatString()));
							if (f != null) {
								files.add(f);
							}
							orderList = new ArrayList<>();
						}
						// exclude the private components
						if (!component.isPrivateComponent() || !(component.isPrivateComponent() && excludePrivateItems)) {
							if ("Custom PDF".equals(component.getTemplateComponent().getReportTemplateComponentType().getName())) {
								ReportComponentParameter pathParam = getReportDefinitionService().getReportComponentParameterByComponentAndName(component.getId(), "PDFPath");
								ValidationUtils.assertNotNull(pathParam, "Cannot attach a pdf because the ReportComponent does not supply a PDF location.");
								ValidationUtils.assertTrue(pathParam.getParameterValue() != null && !pathParam.getParameterValue().isEmpty(), "Cannot attach a pdf because the PDF location is empty.");
								File sourceFile = new File(pathParam.getParameterValue());
								ValidationUtils.assertTrue(FileUtils.fileExists(pathParam.getParameterValue()), "Cannot attach the pdf because the file [" + pathParam.getParameterValue() + "] does not exist.");
								File f;
								try {
									f = File.createTempFile(FileUtils.getFileNameWithoutExtension(sourceFile.getName()), "." + FileUtils.getFileExtension(sourceFile.getName()));
									FileUtils.copyFileOverwrite(sourceFile, f);
								}
								catch (IOException e) {
									throw new RuntimeException("Failed to copy [" + sourceFile.getAbsolutePath() + "] for report component [" + component.getTemplateComponent().getLabel() + "]/", e);
								}
								f.deleteOnExit();
								files.add(f);
							}
							else if ("Custom Report".equals(component.getTemplateComponent().getReportTemplateComponentType().getName())) {
								LinkedHashMap<String, Object> reportParameters = new LinkedHashMap<>();
								if (excludePrivateItems) {
									reportParameters.put("ExcludePrivate", true);
								}
								List<ReportTemplateComponentParameter> templateParameters = getReportTemplateService().getReportTemplateComponentParameterListByTemplateComponent(
										component.getTemplateComponent().getId(), null);
								Report subReport = null;
								for (ReportTemplateComponentParameter parameterTemplate : CollectionUtils.getIterable(templateParameters)) {
									ReportComponentParameter parameter = getReportDefinitionService().getReportComponentParameterByComponentAndTemplate(component.getId(), parameterTemplate.getId());
									if (parameter != null) {
										if ("ReportID".equals(parameter.getTemplateComponentParameter().getName())) {
											subReport = getReportDefinitionService().getReport(Integer.parseInt(parameter.getParameterValue()));
										}
										else if (parameter.getTemplateComponentParameter().isStaticValue() || ((parameter.getParameterValue() != null) && !parameter.getParameterValue().isEmpty())) {
											reportParameters.put(parameter.getTemplateComponentParameter().getName(), parameter.getParameterValue());
										}
										else if (inputParameters.containsKey(parameter.getTemplateComponentParameter().getTemplateParameterName())) {
											String parameterName = parameter.getTemplateComponentParameter().getName();
											reportParameters.put(parameterName, inputParameters.get(parameter.getTemplateComponentParameter().getTemplateParameterName()));
										}
										else if ((parameter.getParameterValue() == null) || parameter.getParameterValue().isEmpty()) {
											reportParameters.put(parameter.getTemplateComponentParameter().getName(),
													getParameterValueFromSql(parameter.getTemplateComponentParameter(), inputParameters));
										}
									}
									else {
										if (inputParameters.containsKey(parameterTemplate.getTemplateParameterName())) {
											String parameterName = parameterTemplate.getName();
											reportParameters.put(parameterName, inputParameters.get(parameterTemplate.getTemplateParameterName()));
										}
										else if ((parameterTemplate.getValueLookupSql() != null) && !parameterTemplate.getValueLookupSql().isEmpty()) {
											reportParameters.put(parameterTemplate.getName(), getParameterValueFromSql(parameterTemplate, inputParameters));
										}
									}
								}
								if (subReport != null) {
									File f = executeGetReportFile(subReport, reportParameters, throwErrorOnBadReport, true, hideAllPageNumbers, exportFormat, false);
									if (f != null) {
										files.add(f);
									}
								}
							}
						}
					}
					count++;
				}
				// set the correct HidePageNumbers parameter for the current report level, needed to get the correct file name
				inputParameters.put("HidePageNumbers", recursiveCall || hideAllPageNumbers);
				file = FileUtils.concatenatePDFs(getCacheFileName(report.getId(), inputParameters, useCache), ".pdf", files, !recursiveCall && !hideAllPageNumbers, true);
			}
			return file;
		}
		finally {
			inputParameters.put("HidePageNumbers", true);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                    Helper Methods                      /////////////
	////////////////////////////////////////////////////////////////////////////////


	private String getCacheFileName(Integer reportId, Map<String, Object> inputParameters, boolean useCache) {
		String fileName = "Temp_Report";
		if (useCache) {
			fileName = ReportExportCacheImpl.CACHED_REPORT_FILE_PREFIX + "_" + getReportExportCache().getCacheKey(reportId, inputParameters) + "_";
		}
		return fileName;
	}


	private Object getParameterValueFromSql(ReportTemplateComponentParameter parameterTemplate, Map<String, Object> parameters) {
		return executeLookupSqlQuery(parameterTemplate.getValueLookupSql(), parameters, parameterTemplate);
	}


	private List<String> getExcludedComponents(Report report, Map<String, Object> parameters) {
		List<String> result = new ArrayList<>();
		for (ReportComponent component : CollectionUtils.getIterable(report.getComponentList())) {
			String excludeLookupSql = component.getTemplateComponent().getExcludeComponentLookupSql();
			if ((excludeLookupSql != null) && !excludeLookupSql.isEmpty()) {
				try {
					Boolean exclude = (Boolean) executeLookupSqlQuery(excludeLookupSql, parameters, null);
					if (exclude) {
						result.add(component.getId().toString());
					}
				}
				catch (Throwable e) {
					LogUtils.warn(getClass(), "Error executing sql to determine if report component [" + component.getName() + "] is included.", e);
				}
			}
		}
		return result;
	}


	private Object executeLookupSqlQuery(String sql, Map<String, Object> parameters, ReportTemplateComponentParameter parameterTemplate) {
		if ((sql != null) && !sql.isEmpty()) {
			List<SqlParameterValue> arguments = new LinkedList<>();
			List<String> replacements = new ArrayList<>();
			int index = sql.indexOf("${");
			while (index > -1) {
				int endIndex = sql.indexOf("}", index + 2);
				String parameterName = sql.substring(index + 2, endIndex);

				// check if we need to re-map with the template parameter name
				String componentParameterName = null;
				if ((parameterTemplate != null) && parameterName.equals(parameterTemplate.getTemplateParameterName())) {
					componentParameterName = parameterTemplate.getName();
				}

				ValidationUtils.assertTrue(parameters.containsKey(parameterName) || parameters.containsKey(componentParameterName), "Required parameter [" + parameterName + "] is not defined.");

				arguments.add(SqlParameterValue.ofUnknownType(parameters.containsKey(componentParameterName) ? parameters.get(componentParameterName) : parameters.get(parameterName)));

				if (!replacements.contains("${" + parameterName + "}")) {
					replacements.add("${" + parameterName + "}");
				}

				index = sql.indexOf("${", endIndex + 1);
			}
			for (String replace : replacements) {
				sql = sql.replace(replace, "?");
			}
			return getSqlHandler().queryForObject(new SqlSelectCommand(sql, CollectionUtils.toArray(arguments, SqlParameterValue.class)));
		}

		return null;
	}


	private File getReportFile(String url, String fileName, boolean throwErrorOnBadReport, String extension) {
		File file = null;
		try {
			file = HttpUtils.downloadToFile(url, fileName, extension, throwErrorOnBadReport);
			if (file != null) {
				file.deleteOnExit();
			}
		}
		catch (Throwable e) {
			if (file != null) {
				file.delete();
			}
			throw new LoggableException("Failed to download file from [" + url + "].", e);
		}
		return file;
	}


	private String buildUrl(Report report, Map<String, Object> parameters, String exportFormat) {
		return buildUrl(report, parameters, null, exportFormat);
	}


	private String buildUrl(Report report, Map<String, Object> parameters, List<String> orderList, String exportFormat) {
		try {
			List<String> excludedComponentList = getExcludedComponents(report, parameters);

			String url = getReportDefinitionService().getReportUrl(report.getId());
			List<String> params = new ArrayList<>();//[parameters.size()];// + (excludePrivateItems ? 1 : 0)];
			for (Map.Entry<String, Object> stringObjectEntry : parameters.entrySet()) {
				String parameterValue = stringObjectEntry.getValue().toString();
				if (!parameterValue.isEmpty()) {
					params.add(stringObjectEntry.getKey() + "=" + URLEncoder.encode(stringObjectEntry.getValue().toString(), "UTF-8"));
				}
			}
			String[] paramsArray = new String[params.size()];
			params.toArray(paramsArray);
			url += "&" + StringUtils.join(paramsArray, "&");
			url += "&exportFormat=" + (exportFormat != null ? exportFormat : "pdf");
			if ((orderList != null) && (!orderList.isEmpty())) {
				String[] orderStrings = new String[orderList.size()];
				orderList.toArray(orderStrings);
				url += "&orderList=" + StringUtils.join(orderStrings, ",");
			}
			if (!CollectionUtils.isEmpty(excludedComponentList)) {
				String[] excludeStrings = new String[excludedComponentList.size()];
				excludedComponentList.toArray(excludeStrings);
				url += "&excludedComponentList=" + StringUtils.join(excludeStrings, ",");
			}
			return url;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to build url for report [" + report.getName() + "].", e);
		}
	}


	private String getFileName(ReportExportConfiguration config, File file) {
		return String.format("%s.%s", getFileNameWithoutExtension(config), FileUtils.getFileExtension(file.getName()));
	}


	// Generates the file name for the report
	// (1) Checks the ReportParameterConfigurer - if defined and there's only one value then it uses that to generate the file name
	// (2) Checks the file name on the ReportExportConfiguration - if defined then it uses that file name
	// (3) Otherwise it uses "Report" as default
	private String getFileNameWithoutExtension(ReportExportConfiguration config) {
		String fileName = null;
		ReportConfigParameter additionalParam = config.getAdditionalReportParameter();
		if (additionalParam != null && !StringUtils.isEmpty(additionalParam.getValue()) && !StringUtils.isEmpty(additionalParam.getName())) {
			ReportParameterConfigurer<T> configurer = getReportParameterConfigurerLocator().locate(additionalParam.getName());
			List<T> values = configurer.getReportParameterValueList(additionalParam, config.getReportingDate());
			if (!CollectionUtils.isEmpty(values) && values.size() == 1) {
				fileName = configurer.generateFileNameWithoutExtension(values.get(0), config.getReportingDate());
			}
		}
		if (fileName == null && !StringUtils.isEmpty(config.getReportFileName())) {
			fileName = config.getReportFileName();
		}
		return fileName == null ? DEFAULT_FILENAME : fileName;
	}


	private FileWrapper createFileWrapper(Report report, File tempFile, ReportExportConfiguration config) {
		boolean useCache = (report != null && !config.isCacheIgnored() && report.getReportTemplate().isReportCachingUsed());
		return new FileWrapper(tempFile, getFileName(config, tempFile), !useCache);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReportDefinitionService getReportDefinitionService() {
		return this.reportDefinitionService;
	}


	public void setReportDefinitionService(ReportDefinitionService reportDefinitionService) {
		this.reportDefinitionService = reportDefinitionService;
	}


	public ReportTemplateService getReportTemplateService() {
		return this.reportTemplateService;
	}


	public void setReportTemplateService(ReportTemplateService reportTemplateService) {
		this.reportTemplateService = reportTemplateService;
	}


	public ReportExportCache getReportExportCache() {
		return this.reportExportCache;
	}


	public void setReportExportCache(ReportExportCache reportExportCache) {
		this.reportExportCache = reportExportCache;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}


	public ReportParameterConfigurerLocator<T> getReportParameterConfigurerLocator() {
		return this.reportParameterConfigurerLocator;
	}


	public void setReportParameterConfigurerLocator(ReportParameterConfigurerLocator<T> reportParameterConfigurerLocator) {
		this.reportParameterConfigurerLocator = reportParameterConfigurerLocator;
	}
}
