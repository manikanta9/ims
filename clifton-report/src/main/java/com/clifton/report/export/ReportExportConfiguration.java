package com.clifton.report.export;


import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.report.params.ReportConfigParameter;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;


@NonPersistentObject
public class ReportExportConfiguration {

	private Integer reportId;
	private Map<String, Object> parameterMap = new LinkedHashMap<>();

	private boolean keepSubReportPageNumbers;
	private boolean throwErrorOnBadReport = true;
	private FileFormats exportFormat = FileFormats.PDF;
	//	private String exportFormat = "pdf";
	private String reportConfigurerName;
	private Date reportingDate;

	/**
	 * The generated file will have this name
	 */
	private String reportFileName;

	/**
	 * Additional parameter that is dynamically located to a {@ReportParamConfigurer} based on the parameter name
	 * to set actual report parameters.
	 * <p>
	 * The configurer returns a list of parameter
	 * <p>
	 * Example: investmentAccountGroupId - investment project has a configurer that returns a list of report parameters for "ClientAccountID"
	 */
	private ReportConfigParameter additionalReportParameter;

	/**
	 * Indicates that the cached file needs to be regenerated.
	 */
	private boolean regenerateCache;
	/**
	 * Indicates that the cache should be ignored.
	 */
	private boolean cacheIgnored;

	/**
	 * Indicates whether or not to zip files
	 */
	private boolean zipFiles;


	public ReportExportConfiguration() {
		//
	}


	public ReportExportConfiguration(Integer reportId, Map<String, Object> parameters, boolean throwErrorOnBadReport, boolean regenerateCache) {
		this.reportId = reportId;
		this.parameterMap = parameters;
		this.throwErrorOnBadReport = throwErrorOnBadReport;
		this.regenerateCache = regenerateCache;
	}


	public Integer getReportId() {
		return this.reportId;
	}


	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}


	public Map<String, Object> getParameterMap() {
		return this.parameterMap;
	}


	@ValueChangingSetter
	public void setParameterMap(Map<String, Object> parameterMap) {
		ValidationUtils.assertTrue(parameterMap instanceof LinkedHashMap, "Only LinkedHashMaps are supported.");
		this.parameterMap = parameterMap;
	}


	public boolean isThrowErrorOnBadReport() {
		return this.throwErrorOnBadReport;
	}


	public void setThrowErrorOnBadReport(boolean throwErrorOnBadReport) {
		this.throwErrorOnBadReport = throwErrorOnBadReport;
	}


	public String getReportConfigurerName() {
		return this.reportConfigurerName;
	}


	public void setReportConfigurerName(String reportConfigurerName) {
		this.reportConfigurerName = reportConfigurerName;
	}


	public Date getReportingDate() {
		return this.reportingDate;
	}


	public void setReportingDate(Date reportingDate) {
		this.reportingDate = reportingDate;
	}


	public boolean isRegenerateCache() {
		return this.regenerateCache;
	}


	public void setRegenerateCache(boolean regenerateCache) {
		this.regenerateCache = regenerateCache;
	}


	public boolean isCacheIgnored() {
		return this.cacheIgnored;
	}


	public void setCacheIgnored(boolean cacheIgnored) {
		this.cacheIgnored = cacheIgnored;
	}


	public ReportConfigParameter getAdditionalReportParameter() {
		return this.additionalReportParameter;
	}


	public void setAdditionalReportParameter(ReportConfigParameter additionalReportParameter) {
		this.additionalReportParameter = additionalReportParameter;
	}


	public boolean isKeepSubReportPageNumbers() {
		return this.keepSubReportPageNumbers;
	}


	public void setKeepSubReportPageNumbers(boolean keepSubReportPageNumbers) {
		this.keepSubReportPageNumbers = keepSubReportPageNumbers;
	}


	public FileFormats getExportFormat() {
		return this.exportFormat;
	}


	public void setExportFormat(FileFormats exportFormat) {
		this.exportFormat = exportFormat;
	}


	public String getReportFileName() {
		return this.reportFileName;
	}


	public void setReportFileName(String reportFileName) {
		this.reportFileName = reportFileName;
	}


	public boolean isZipFiles() {
		return this.zipFiles;
	}


	public void setZipFiles(boolean zipFiles) {
		this.zipFiles = zipFiles;
	}
}
