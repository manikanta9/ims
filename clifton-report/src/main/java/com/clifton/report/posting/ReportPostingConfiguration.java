package com.clifton.report.posting;


import com.clifton.report.export.ReportExportConfiguration;
import org.springframework.web.multipart.MultipartFile;


public class ReportPostingConfiguration extends ReportExportConfiguration {

	private ReportPostingFileConfiguration reportPostingFileConfiguration;

	private MultipartFile file;

	/**
	 * If defined, will pull and set the file from Document Management System
	 * (Converted to PDF if not already a PDF)
	 */
	private String documentId;
	private boolean forceDocumentAsPDF;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReportPostingFileConfiguration getReportPostingFileConfiguration() {
		return this.reportPostingFileConfiguration;
	}


	public void setReportPostingFileConfiguration(ReportPostingFileConfiguration reportPostingFileConfiguration) {
		this.reportPostingFileConfiguration = reportPostingFileConfiguration;
	}


	public MultipartFile getFile() {
		return this.file;
	}


	public void setFile(MultipartFile file) {
		this.file = file;
	}


	public String getDocumentId() {
		return this.documentId;
	}


	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}


	public boolean isForceDocumentAsPDF() {
		return this.forceDocumentAsPDF;
	}


	public void setForceDocumentAsPDF(boolean forceDocumentAsPDF) {
		this.forceDocumentAsPDF = forceDocumentAsPDF;
	}
}
