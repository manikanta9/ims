package com.clifton.report.posting;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;


public interface ReportPostingService<T extends IdentityObject> {

	@SecureMethod(securityResource = "ReportPosting")
	public void uploadReportPostingClient(ReportPostingConfiguration config);


	@SecureMethod(securityResource = "ReportPosting")
	public void postReportPostingClient(ReportPostingConfiguration config);


	@DoNotAddRequestMapping
	public T postReportPostingClientWithSource(ReportPostingConfiguration config, T sourceEntity);
}
