package com.clifton.report.posting;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.document.DocumentManagementService;
import com.clifton.document.DocumentRecord;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.export.ReportExportService;
import com.clifton.report.params.ReportConfigParameter;
import com.clifton.report.params.ReportParameterConfigurer;
import com.clifton.report.params.ReportParameterConfigurerLocator;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;


@Service
public class ReportPostingServiceImpl<T extends IdentityObject> implements ReportPostingService<T> {

	private DocumentManagementService documentManagementService;

	private ReportExportService reportExportService;
	private ReportParameterConfigurerLocator<T> reportParameterConfigurerLocator;
	private ReportPostingController<T> reportPostingController;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadReportPostingClient(ReportPostingConfiguration config) {
		try {
			FileWrapper wrapper;
			boolean customFile = false;
			if (config.getDocumentId() != null) {
				DocumentRecord documentRecord = getDocumentManagementService().getDocumentRecordByDocumentId(config.getDocumentId());
				if (!config.isForceDocumentAsPDF() || "pdf".equalsIgnoreCase(FileUtils.getFileExtension(documentRecord.getName()))) {
					wrapper = getDocumentManagementService().downloadDocumentRecord(config.getDocumentId());
				}
				else {
					wrapper = getDocumentManagementService().downloadDocumentRecordConversion(config.getDocumentId(), "pdf");
				}
				if (wrapper == null) {
					throw new ValidationException("Cannot find requested Document.");
				}
			}
			else {
				customFile = true;
				MultipartFile thisFile = config.getFile();
				File newFile = FileUtils.convertInputStreamToFile(FileUtils.getFileNameWithoutExtension(thisFile.getOriginalFilename()), FileUtils.getFileExtension(thisFile.getOriginalFilename()),
						thisFile.getInputStream());
				wrapper = new FileWrapper(newFile, newFile.getName(), true);
			}
			ReportPostingFileConfiguration postingConfig = new ReportPostingFileConfiguration();
			BeanUtils.copyProperties(config.getReportPostingFileConfiguration(), postingConfig);
			postingConfig.setCustomFile(customFile);
			postingConfig.setFileWrapper(wrapper);
			getReportPostingController().postClientFile(postingConfig, null);
		}
		catch (Throwable e) {
			if (config.getFile() != null) {
				throw new RuntimeException("Failed to post file [" + config.getFile().getOriginalFilename() + "].", e);
			}
			throw new RuntimeException("Failed to post requested document.", e);
		}
	}


	@Override
	public void postReportPostingClient(ReportPostingConfiguration config) {
		postReportPostingClientWithSource(config, null);
	}


	@Override
	public T postReportPostingClientWithSource(ReportPostingConfiguration config, T sourceEntity) {
		if (config.getFile() == null) {
			if (config.getParameterMap() == null) {
				config.setParameterMap(new LinkedHashMap<>());
			}

			ReportConfigParameter additionalParam = config.getAdditionalReportParameter();
			if (additionalParam != null && !StringUtils.isEmpty(additionalParam.getName())) {
				ReportParameterConfigurer<T> configurer = getReportParameterConfigurerLocator().locate(additionalParam.getName());
				List<T> values = configurer.getReportParameterValueList(additionalParam, config.getReportingDate());

				if (!CollectionUtils.isEmpty(values)) {
					StringBuilder messageSb = new StringBuilder();
					for (T value : CollectionUtils.getIterable(values)) {
						try {
							ReportPostingFileConfiguration postingConfig = new ReportPostingFileConfiguration();
							BeanUtils.copyProperties(config.getReportPostingFileConfiguration(), postingConfig);
							configurer.populateReportParameterMap(config.getParameterMap(), value);
							configurer.populateReportPostingFileConfiguration(postingConfig, value);
							config.getParameterMap().put("ReportingDate", DateUtils.fromDateShort(postingConfig.getReportDate()));

							doPostClientReport(config, postingConfig, sourceEntity);
						}
						catch (Exception ex) {
							String exMsg = "Failed to post report for [" + configurer.getLabelForValue(value) + "].";
							LogUtils.error(getClass(), exMsg, ex);
							messageSb.append(new RuntimeException(exMsg + "\n", ex));
						}
					}
					if (messageSb.length() != 0) {
						throw new RuntimeException(messageSb.toString());
					}
					return sourceEntity;
				}
			}
			// If not a group it is assumed the that postEntitySourceTableName and postEntitySourceFkFieldId are populated - if not the post will return
			// an error that it doesn't know where to post to.
			return doPostClientReport(config, config.getReportPostingFileConfiguration(), sourceEntity);
		}
		return sourceEntity;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////                   Helper Methods                      ////////////
	////////////////////////////////////////////////////////////////////////////


	private T doPostClientReport(ReportPostingConfiguration config, ReportPostingFileConfiguration fileConfig, T sourceEntity) {
		// copy the export config so changes to the parameter map don't affect subsequent calls
		ReportExportConfiguration cfg = new ReportExportConfiguration();
		BeanUtils.copyProperties(config, cfg);
		config.setThrowErrorOnBadReport(true);
		// In cases where we are pulling the report in a format other than PDF call the download method, otherwise use the PDF specific method
		FileWrapper reportFile = (cfg.getExportFormat() != null && cfg.getExportFormat() != FileFormats.PDF) ? getReportExportService().downloadReportFile(cfg) : getReportExportService().getReportPDFFile(cfg);
		fileConfig.setFileWrapper(reportFile);
		return getReportPostingController().postClientFile(fileConfig, sourceEntity);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ReportExportService getReportExportService() {
		return this.reportExportService;
	}


	public void setReportExportService(ReportExportService reportExportService) {
		this.reportExportService = reportExportService;
	}


	public ReportPostingController<T> getReportPostingController() {
		return this.reportPostingController;
	}


	public void setReportPostingController(ReportPostingController<T> reportPostingController) {
		this.reportPostingController = reportPostingController;
	}


	public DocumentManagementService getDocumentManagementService() {
		return this.documentManagementService;
	}


	public void setDocumentManagementService(DocumentManagementService documentManagementService) {
		this.documentManagementService = documentManagementService;
	}


	public ReportParameterConfigurerLocator<T> getReportParameterConfigurerLocator() {
		return this.reportParameterConfigurerLocator;
	}


	public void setReportParameterConfigurerLocator(ReportParameterConfigurerLocator<T> reportParameterConfigurerLocator) {
		this.reportParameterConfigurerLocator = reportParameterConfigurerLocator;
	}
}
