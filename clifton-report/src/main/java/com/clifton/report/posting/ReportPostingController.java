package com.clifton.report.posting;


import com.clifton.core.beans.IdentityObject;


/**
 * The <code>ReportPostingService</code> stub for client file management service.  Currently implemented in the app-clifton-ims.
 *
 * @author mwacker
 */
public interface ReportPostingController<T extends IdentityObject> {


	/**
	 * Post file to the portal using given configuration.
	 * <p>
	 * Note, if sourceEntity is passed in, it will use that (if it extends PortalFileAware) to update those properties
	 * and returned the saved object.  If it's not passed in, but source table and fk field id properties are passed in it will use that.
	 */
	public T postClientFile(ReportPostingFileConfiguration config, T sourceEntity);


	/**
	 * Unapprove and optionally delete portal file(s) associated to the specified config.
	 * <p/>
	 * The config will be copied to a portal file search form as best as possible. Based on property name. The search form is used to resolve a list of files.
	 * <p>
	 * Note, if sourceEntity is passed in, it will use that (if it extends PortalFileAware) to update those properties
	 * and returned the saved object.  If it's not passed in, but source table and fk field id properties are passed in it will use that.
	 *
	 * @param delete - by default the portal will just unapprove the file so it can be replaced.  However, can optionally force delete
	 */
	public T unpostClientFile(ReportPostingFileConfiguration config, T sourceEntity, boolean delete);
}
