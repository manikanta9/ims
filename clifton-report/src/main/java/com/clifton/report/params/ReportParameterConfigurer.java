package com.clifton.report.params;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.posting.ReportPostingFileConfiguration;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>ReportParameterConfigurer</code> interface defines the method to configure a report parameter to
 * pull a list of reports concatenated when the report parameter name is set for each report with a value from the returned parameter value list
 * <p/>
 * example - see Investment project: com.clifton.investment.report.param.InvestmentAccountGroupReportParameterConfigurer
 *
 * @author manderson
 */
public interface ReportParameterConfigurer<T> {

	/**
	 * Returns the parameter name that the configurer applies to
	 * <p/>
	 * Example: investmentAccountGroupId
	 */
	public String getParameterName();


	public List<T> getReportParameterValueList(ReportConfigParameter configParameter, Date reportDate);


	/**
	 * For report failures - returns user friendly label for the value.
	 * Example: Instead of Account ID - returns Account Label
	 *
	 * @param value
	 */
	public String getLabelForValue(T value);


	/**
	 * Sets the report parameter in the map
	 * Example - Sets parameter "ClientAccountID" with the account ID
	 *
	 * @param value
	 */
	public void populateReportParameterMap(Map<String, Object> parameterMap, T value);


	/**
	 * Ability to set report to a specific one for each value, if not supplied in the config
	 * Ex. PIOS reports - can be different for each account, so for each account, we look up which report it uses
	 *
	 * @param exportConfig
	 * @param value
	 */
	public Integer getReportIdForValue(ReportExportConfiguration exportConfig, T value);


	public void populateReportPostingFileConfiguration(ReportPostingFileConfiguration postConfig, T value);


	public String generateFileNameWithoutExtension(T value, Date reportDate);


	public FileWrapper getAttachments(T value, ReportExportConfiguration configuration);
}
