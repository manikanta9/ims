package com.clifton.report.params;


import com.clifton.core.util.AssertUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>ReportParameterConfigurerLocatorInSpringContext</code> class locates ReportParameterConfigurerLocator implementations for corresponding parameter names
 * It auto-discovers all application context beans that implement ReportParameterConfigurer interface and registers them so that they can be located.
 *
 * @author manderson
 */
@Component
public class ReportParameterConfigurerLocatorInSpringContext<T> implements ReportParameterConfigurerLocator<T>, InitializingBean, ApplicationContextAware {

	private final Map<String, ReportParameterConfigurer<T>> configurerMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		@SuppressWarnings("unchecked")
		Map<String, ReportParameterConfigurer<T>> beanMap = (Map<String, ReportParameterConfigurer<T>>) (Map<String, ?>) getApplicationContext().getBeansOfType(ReportParameterConfigurer.class);

		// need a map with Parameter names as keys instead of bean names
		for (Map.Entry<String, ReportParameterConfigurer<T>> stringReportParameterConfigurerEntry : beanMap.entrySet()) {
			ReportParameterConfigurer<T> configurer = stringReportParameterConfigurerEntry.getValue();
			if (getConfigurerMap().containsKey(configurer.getParameterName())) {
				throw new RuntimeException("Cannot register '" + stringReportParameterConfigurerEntry.getKey() + "' as a configurer for report parameter '" + configurer.getParameterName()
						+ "' because this report parameter already has a registered configurer.");
			}
			getConfigurerMap().put(configurer.getParameterName(), configurer);
		}
	}


	@Override
	public ReportParameterConfigurer<T> locate(String parameterName) {
		AssertUtils.assertNotNull(parameterName, "Required parameter name cannot be null.");
		ReportParameterConfigurer<T> result = getConfigurerMap().get(parameterName);
		AssertUtils.assertNotNull(result, "Cannot locate ReportParameterConfigurer for parameter name '%1s'.", parameterName);
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, ReportParameterConfigurer<T>> getConfigurerMap() {
		return this.configurerMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
