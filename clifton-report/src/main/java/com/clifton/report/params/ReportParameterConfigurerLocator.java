package com.clifton.report.params;


/**
 * The <code>ReportParameterConfigurerLocator</code> interface defines method that returns ReportParameterConfigurer
 * that corresponds to a config parameter name
 *
 * @author manderson
 */
public interface ReportParameterConfigurerLocator<T> {

	/**
	 * Returns ReportParameterConfigurer for the specified config parameter name
	 *
	 * @param configParameterName
	 */
	public ReportParameterConfigurer<T> locate(String configParameterName);
}
