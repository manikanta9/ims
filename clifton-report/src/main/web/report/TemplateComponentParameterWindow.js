Clifton.report.TemplateComponentParameterWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Report Template Component Parameter',
	iconCls: 'report',
	width: 650,
	height: 580,

	items: [{
		xtype: 'formpanel',
		url: 'reportTemplateComponentParameter.json',
		instructions: 'A template component parameter defines a report parameter that will be passed from the main report to the sub report.',
		labelWidth: 160,
		items: [
			{fieldLabel: 'Template', name: 'reportTemplateComponent.name', xtype: 'linkfield', detailIdField: 'reportTemplateComponent.id', detailPageClass: 'Clifton.report.TemplateComponentWindow'},
			{fieldLabel: 'Data Type', name: 'dataType.name', hiddenName: 'dataType.id', xtype: 'combo', url: 'systemDataTypeList.json', detailPageClass: 'Clifton.system.schema.DataTypeWindow', loadAll: true},
			{fieldLabel: 'Parameter Name', name: 'name'},
			{fieldLabel: 'Parameter Label', name: 'label'},
			{fieldLabel: 'Template Parameter Name', name: 'templateParameterName'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{boxLabel: 'The value of this parameter can be customized per report component', name: 'staticValue', xtype: 'checkbox'},
			{fieldLabel: 'Value List Table', name: 'valueTable.name', hiddenName: 'valueTable.id', xtype: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded', requiredFields: ['staticValue']},
			{fieldLabel: 'Value List URL', name: 'valueListUrl', requiredFields: ['staticValue']},
			{fieldLabel: 'UI Config', name: 'userInterfaceConfig', requiredFields: ['staticValue']},
			{fieldLabel: 'Default Value', name: 'defaultValue', requiredFields: ['staticValue']},
			{fieldLabel: 'Default Text', name: 'defaultText', requiredFields: ['staticValue']},
			{fieldLabel: 'Value Lookup SQL', name: 'valueLookupSql', xtype: 'textarea'}
		]
	}]
});
