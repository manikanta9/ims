Clifton.report.TemplateWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Report Template',
	iconCls: 'report',
	height: 600,
	width: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Template',
				items: [{
					xtype: 'formpanel',
					instructions: 'Report template defines report layout, headers, footers, etc. and a list of available template components that can be used in reports utilizing this template.',
					url: 'reportTemplate.json',
					items: [
						{fieldLabel: 'Template Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'URL', name: 'url'},
						{boxLabel: 'Append additional URL Parameters only (do not append: "?reportId=???&")', name: 'additionalUrlParameterOnly', xtype: 'checkbox', qtip: 'If checked, append Report.additionalUrlParameters to the template URL (no extra stuff). Otherwise, appends "?reportId=???&" before appending additional parameters.'},
						{boxLabel: 'Cache reports for quick access during the day', name: 'reportCachingUsed', xtype: 'checkbox'},
						{fieldLabel: 'Sub Report Width', name: 'subReportWidth', xtype: 'floatfield'},

						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'ReportTemplate',
							hierarchyCategoryName: 'Report Template Tags'
						}
					]
				}]
			},


			{
				title: 'Components',
				items: [{
					name: 'reportTemplateComponentListFind',
					xtype: 'gridpanel',
					instructions: 'A list of available Components for this Template. A Template Component defines a report section that can be used with the corresponding Template.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Component Name', width: 100, dataIndex: 'name'},
						{header: 'Component Type', width: 50, dataIndex: 'reportTemplateComponentType.name'},
						{header: 'External Name', width: 100, dataIndex: 'externalReportName'},
						{header: 'Description', width: 150, dataIndex: 'description', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.report.TemplateComponentWindow',
						deleteURL: 'reportTemplateComponentDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								reportTemplate: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getTopToolbarInitialLoadParams: function() {
						return {'templateId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Parameters',
				items: [{
					name: 'reportTemplateParameterListByTemplate',
					xtype: 'gridpanel',
					instructions: 'The following parameters are associated with this report template.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Parameter Name', width: 100, dataIndex: 'name'},
						{header: 'Parameter Label', width: 100, dataIndex: 'label'},
						{header: 'Data Type', width: 50, dataIndex: 'dataType.name'},
						{header: 'Description', width: 100, dataIndex: 'description', hidden: true},
						{header: 'Display Order', width: 50, dataIndex: 'order', type: 'int', defaultSortColumn: true},
						{header: 'Required', width: 30, dataIndex: 'required', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.report.TemplateParameterWindow',
						deleteURL: 'reportTemplateParameter.json',
						getDefaultData: function(gridPanel) { // defaults queryId for the detail page
							return {
								template: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function() {
						return {'templateId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Parameter Values',
				items: [{
					name: 'reportComponentParameterListFind',
					xtype: 'gridpanel',
					instructions: 'The following static component parameter values are associated with reports using this template.',
					topToolbarSearchParameter: 'searchPattern',
					additionalPropertiesToRequest: 'reportComponent.id',
					columns: [
						{header: 'Report', width: 100, dataIndex: 'reportComponent.report.name', filter: {searchFieldName: 'reportName'}},
						{header: 'Component', width: 100, dataIndex: 'reportComponent.templateComponent.name', filter: {searchFieldName: 'templateComponentName'}},
						{header: 'Parameter Name', width: 75, dataIndex: 'templateComponentParameter.name', filter: {searchFieldName: 'templateComponentParameterName'}},
						{header: 'Parameter Value', width: 75, dataIndex: 'parameterText', filter: 'parameterText'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.report.ReportComponentWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.reportComponent.id;
						}
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {'templateId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Used By',
				items: [{
					name: 'reportListFind',
					xtype: 'gridpanel',
					instructions: 'The following reports have been created using this template.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Report Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 200, dataIndex: 'description'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.report.ReportWindow'
					},
					getTopToolbarInitialLoadParams: function() {
						return {'templateId': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
