Clifton.report.ReportSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'reportSetupWindow',
	title: 'Reports',
	iconCls: 'report',
	width: 1400,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Reports',
				layout: 'border',
				items: [
					{
						region: 'west',
						id: 'reportTree',
						xtype: 'system-hierarchy-folders',
						tableName: 'Report',
						rootNodeText: 'Reports',
						ddGroup: 'reportToFolder',
						itemComponentId: 'reportListGrid',
						// Override to use correct param name for search form
						onNodeSelected: function(id) {
							const grid = this.getItemComponent();
							grid.getFolderHierarchyId = function() {
								return id;
							};
							grid.reload();
						}
					},

					{
						region: 'center',
						layout: 'fit',
						items: [{
							id: 'reportListGrid',
							name: 'reportListFind',
							xtype: 'gridpanel',
							instructions: 'Reports that are located in selected folder or all reports in "Global Search" view.',
							folderView: true,
							viewNames: ['Organized in Folders', 'Global Search'], // Customized switch to view to remove/add folder tree/searches
							includeAllColumnsView: false, // Note, will still be available under "tools"
							groupField: 'reportTemplate.name',
							groupTextTpl: '"{values.group}" Template ({[values.rs.length]} {[values.rs.length > 1 ? "Reports" : "Report"]})',
							columns: [
								{header: 'ID', width: 15, dataIndex: 'id', hidden: true, filter: false, sortable: false},
								{header: 'Template', width: 50, dataIndex: 'reportTemplate.name', hidden: true, filter: {type: 'combo', searchFieldName: 'templateId', displayField: 'label', url: 'reportTemplateListFind.json'}},
								{header: 'Report Name', width: 100, dataIndex: 'name'},
								{header: 'Description', width: 200, dataIndex: 'description'},
								{header: 'Modify Condition', width: 150, dataIndex: 'entityModifyCondition.name', hidden: true},
								{
									header: 'Run', width: 20, dataIndex: 'url', filter: false, sortable: false,
									renderer: function(url, args) {
										return TCG.renderActionColumn('run', 'Run', 'Execute this report');
									}
								}
							],
							listeners: {
								afterRender: function() {
									const gp = this;
									gp.grid.store.addListener('load', gp.switchGrouping, this);
								}
							},
							isFolderView: function() {
								return this.folderView;
							},
							switchGrouping: function() { // Needs to be called after load so that grouping is enabled for the store
								if (this.folderView === true) {
									this.grid.store.clearGrouping();
								}
								else {
									this.grid.store.groupBy('reportTemplate.name');
								}
							},
							switchToView: function(viewName, doNotReload) {
								if (viewName === 'All Columns') {
									this.switchToViewColumns('All Columns');
								}
								const showFolders = (viewName !== 'Global Search');
								if (this.folderView !== showFolders) {
									this.folderView = showFolders;
									this.savedLoadParams = this.getLoadParams();
									if (showFolders === true) {
										Ext.getCmp('reportTree').show();
									}
									else {
										Ext.getCmp('reportTree').hide();
									}
									this.getTopToolbar().find('text', 'Views')[0].menu.findBy(i => i.text && i.text === 'Global Search')[0].setChecked(true);
									this.ownerCt.ownerCt.doLayout();
								}
								if (doNotReload !== true) {
									this.reload();
								}
							},
							getTopToolbarFilters: function(toolbar) {
								return [
									'-',
									{
										fieldLabel: 'Tag', width: 150, xtype: 'combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Report Template Tags',
										listeners: {
											select: function(field) {
												const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
												gridPanel.switchToView('Global Search');
											},
											specialkey: function(field, e) {
												if (e.getKey() === e.ENTER) {
													const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
													gridPanel.switchToView('Global Search');
												}
											}
										}
									},
									{
										fieldLabel: 'Search', width: 130, xtype: 'textfield', name: 'searchPattern',
										listeners: {
											specialkey: function(field, e) {
												if (e.getKey() === e.ENTER) {
													const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
													if (TCG.isNotNull(gridPanel.getFolderHierarchyId())) {
														gridPanel.reload();
													}
													else {
														gridPanel.switchToView('Global Search');
													}
												}
											}
										}
									}
								];
							},
							// Overridden by tree on left when folder is selected so don't override load params
							getFolderHierarchyId: function() {
								return null;
							},
							getLoadURL: function() {
								return 'reportListFind.json';
							},
							getLoadParams: function(firstLoad) {
								const params = {};
								params.categoryName = 'Report Folders';

								if (this.folderView === false) {
									params.categoryIncludeAllIfNull = true;
								}
								else {
									params.categoryHierarchyId = this.getFolderHierarchyId();
									params.categoryIncludeAllIfNull = false;
								}

								const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
								if (TCG.isNotBlank(tag.getValue())) {
									params.category2Name = 'Report Template Tags';
									params.category2TableName = 'ReportTemplate';
									params.category2LinkFieldPath = 'reportTemplate';
									params.category2HierarchyId = tag.getValue();
								}

								const searchPattern = TCG.getChildByName(this.getTopToolbar(), 'searchPattern');
								if (TCG.isNotBlank(searchPattern.getValue())) {
									params.searchPattern = searchPattern.getValue();
								}
								if (firstLoad) {
									searchPattern.focus(false, 500);
								}
								return params;
							},
							editor: {
								detailPageClass: 'Clifton.report.ReportWindow',
								copyURL: 'reportCopy.json',
								copyIdParameter: 'reportId',
								getDefaultData: function(grid) {
									const hierarchy = Ext.getCmp('reportTree').getSelectionModel().getSelectedNode();
									if (this.savedDefaultData) {
										this.savedDefaultData.hierarchy = (hierarchy) ? hierarchy.nodeData : undefined;
									}
									return this.savedDefaultData;
								},
								addToolbarAddButton: function(toolBar) {
									const gridPanel = this.getGridPanel();
									toolBar.add({
										text: 'Add',
										tooltip: 'Add a report for selected template',
										iconCls: 'add',
										menu: new Ext.menu.Menu({
											items: [{
												text: 'Select Template', iconCls: 'report',
												menu: new Ext.menu.Menu({
													layout: 'fit',
													style: {overflow: 'visible'},
													width: 270,
													items: [{
														xtype: 'combo', name: 'reportTemplate', url: 'reportTemplateListFind.json', listWidth: 270,
														getListParent: function() {
															return this.el.up('.x-menu');
														},
														listeners: {
															select: function(combo) {
																const editor = gridPanel.editor;
																editor.savedDefaultData = {
																	reportTemplate: {
																		id: combo.getValue(),
																		name: combo.lastSelectionText
																	}
																};
																combo.ownerCt.ownerCt.ownerCt.hide(true); // hide menus
																editor.openDetailPage(editor.getDetailPageClass(), gridPanel);
															}
														}
													}]
												})
											}]
										})
									});
									toolBar.add('-');
								}
							},
							gridConfig: {
								//localFilteringEnabled: true,
								// allow drag and drop of multiple items at a time
								sm: new Ext.grid.RowSelectionModel({singleSelect: false}),
								enableDragDrop: true,
								ddGroup: 'reportToFolder',
								listeners: {
									'rowclick': function(grid, rowIndex, evt) {
										if (TCG.isActionColumn(evt.target)) {
											const row = grid.store.data.items[rowIndex];
											const url = TCG.getResponseText('reportUrl.json?id=' + row.id, grid);
											if (url !== undefined) {
												new TCG.app.URLWindow({
													id: 'report-' + row.id,
													title: 'Report - ' + row.data.name,
													width: 1500,
													height: 700,
													iconCls: 'chart-pie',
													url: url,
													openerCt: grid
												});
											}
										}
									}
								}
							}
						}]
					}
				]
			},


			{
				title: 'Templates',
				items: [{
					name: 'reportTemplateListFind',
					xtype: 'gridpanel',
					instructions: 'Report template defines report layout, headers, footers, etc. and a list of available template components that can be used in reports utilizing this template.',
					wikiPage: 'IT/Reporting+Documentation',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Template Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'URL', width: 120, dataIndex: 'url'},
						{header: 'Params Only', width: 25, dataIndex: 'additionalUrlParameterOnly', type: 'boolean', tooltip: 'If checked, append Report.additionalUrlParameters to the template URL (no extra stuff). Otherwise, appends "?reportId=???&" before appending additional parameters.'},
						{header: 'Cached', width: 20, dataIndex: 'reportCachingUsed', type: 'boolean'},
						{header: 'Sub Report Width', width: 25, dataIndex: 'subReportWidth', type: 'float'}
					],
					editor: {
						detailPageClass: 'Clifton.report.TemplateWindow'
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Report Template Tags'},
							{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						const params = {};
						if (TCG.isNotBlank(tag.getValue())) {
							params.categoryName = 'Report Template Tags';
							params.categoryHierarchyId = tag.getValue();
						}
						return params;
					}
				}]
			},


			{
				title: 'Component Types',
				items: [{
					name: 'reportTemplateComponentTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Each report component is of one of the following types. Different types may apply different rendering rules.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Component Type', width: 50, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 200, dataIndex: 'description'}
					]
				}]
			},


			{
				title: 'Data Warehouse',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						loadValidation: false, // using the form only to get background color/padding
						height: 200,
						labelWidth: 180,
						instructions: 'Many reports use precalculated data from the Data Warehouse which can be updated using this section. Rebuild for a specific date or for multiple dates based on filters below. Dimensions (investment securities, investment accounts, accounting accounts, etc.) change infrequently and are rebuilt automatically every night.',
						listeners: {
							afterrender: function() {
								this.setFormValue('startSnapshotDate', Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y'), true);
								this.setFormValue('endSnapshotDate', Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y'), true);
							}
						},
						items: [{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .60,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Client Account', name: 'accountLabel', hiddenName: 'clientAccountId', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', mutuallyExclusiveFields: ['groupName']},
											{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'investmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['accountLabel'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
											{boxLabel: 'Rebuild each month end from client\'s inception date', name: 'buildEachMonthEnd', xtype: 'checkbox'}
										]
									}]
								},
								{columnWidth: .15, items: [{xtype: 'label', html: '&nbsp;'}]},
								{
									columnWidth: .25,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 100,
										items: [
											{fieldLabel: 'Start Date', name: 'startSnapshotDate', xtype: 'datefield', allowBlank: false},
											{fieldLabel: 'End Date', name: 'endSnapshotDate', xtype: 'datefield', allowBlank: false},
											{fieldLabel: '', boxLabel: 'Allow Today Snapshots', name: 'allowTodaySnapshots', xtype: 'checkbox', qtip: 'By default the last snapshot allowed is for the previous day.  Checking this box will allow you to rebuild snapshots for today.'},
											{fieldLabel: '', boxLabel: 'Allow Future Snapshots', name: 'allowFutureSnapshots', xtype: 'checkbox', qtip: 'By default the last snapshot allowed is for the previous day.  Checking this box will allow you to rebuild snapshots for the future (limit of no more than 60 days).'}
										]
									}]
								}
							]
						}],
						buttons: [
							{
								text: 'Rebuild Dimensions',
								tooltip: 'Update investment security, investment account and accounting account information.',
								iconCls: 'run',
								width: 170,
								handler: function() {
									const formPanel = TCG.getParentFormPanel(this);
									const loader = new TCG.data.JsonLoader({
										waitTarget: formPanel,
										onLoad: function(record, conf) {
											TCG.showInfo('Data Warehouse Dimension tables (updates to investment securities, investment accounts, accounting accounts, etc.) have been rebuilt.', 'Data Warehouse');
										}
									});
									loader.load('dwDimensionsRebuild.json');
								}
							},
							{
								text: 'Rebuild Snapshots',
								tooltip: 'Update position and balance snapshot information',
								iconCls: 'run',
								width: 170,
								handler: function() {
									const owner = this.findParentByType('formpanel');
									const form = owner.getForm();
									const url = 'dwAccountingSnapshotsRebuild.json?UI_SOURCE=ReportSetup';
									form.submit(Ext.applyIf({
										url: encodeURI(url),
										waitMsg: 'Rebuilding...',
										success: function(form, action) {
											Ext.Msg.alert('Processing Started', action.result.result.message, function() {
												const grid = owner.ownerCt.items.get(1);
												grid.reload.defer(300, grid);
											});
										}
									}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
								}
							}
						]
					},
					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'DW-SNAPSHOTS',
						instantRunner: true,
						title: 'Current Processing',
						flex: 1
					}
				]
			}
		]
	}]
});

