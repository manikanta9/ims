Clifton.report.ReportWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Report',
	iconCls: 'report',
	height: 600,
	width: 700,

	items: [{
		xtype: 'formpanel',
		url: 'report.json',
		items: [
			{fieldLabel: 'Template', name: 'reportTemplate.name', xtype: 'linkfield', detailIdField: 'reportTemplate.id', detailPageClass: 'Clifton.report.TemplateWindow'},
			{fieldLabel: 'Report Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Modify Condition', name: 'entityModifyCondition.label', hiddenName: 'entityModifyCondition.id', displayField: 'name', xtype: 'system-condition-combo'},
			{fieldLabel: 'URL Parameters', name: 'additionalUrlParameter'},
			{fieldLabel: 'Folder', name: 'hierarchy.nameExpanded', hiddenName: 'hierarchy.id', xtype: 'system-hierarchy-combo', tableName: 'Report', hierarchyCategoryName: 'Report Folders'},
			{
				xtype: 'formgrid',
				title: 'Report Components',
				storeRoot: 'componentList',
				dtoClass: 'com.clifton.report.definition.ReportComponent',
				columnsConfig: [
					{
						header: 'Component', width: 280, dataIndex: 'label', idDataIndex: 'templateComponent.id',
						editor: {
							xtype: 'combo', url: 'reportTemplateComponentListFind.json', allowBlank: false,
							beforequery: function(queryEvent) {
								const grid = queryEvent.combo.gridEditor.containerGrid;
								const f = TCG.getParentFormPanel(grid).getForm();
								queryEvent.combo.store.baseParams = {templateId: TCG.getValue('reportTemplate.id', f.formValues)};
							}
						}
					},
					{header: 'Private', width: 60, dataIndex: 'privateComponent', type: 'boolean', editor: {xtype: 'checkbox'}},
					{header: 'Page Break Before', width: 110, dataIndex: 'pageBreakBefore', type: 'boolean', editor: {xtype: 'checkbox'}},
					{header: 'Top Margin', width: 80, type: 'float', useNull: true, dataIndex: 'topMargin', editor: {xtype: 'numberfield'}},
					{header: 'Order', width: 55, dataIndex: 'order', type: 'int', editor: {xtype: 'integerfield', allowBlank: false}}
				],
				addToolbarButtons: function(toolBar, grid) {
					toolBar.add({
						text: 'Edit',
						tooltip: 'Edit selected component parameters',
						iconCls: 'pencil',
						handler: function() {
							const index = grid.getSelectionModel().getSelectedCell();
							if (index) {
								const rec = grid.getStore().getAt(index[0]);
								const id = rec.id;
								const className = 'Clifton.report.ReportComponentWindow';
								TCG.createComponent(className, {
									id: TCG.getComponentId(className, id),
									params: {id: id},
									openerCt: grid
								});
							}
						}
					});
					toolBar.add('-');
				}
			}
		]
	}]
});
