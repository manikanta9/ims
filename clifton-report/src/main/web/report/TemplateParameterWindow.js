Clifton.report.TemplateParameterWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Report Template Parameter',
	iconCls: 'list',
	height: 500,

	items: [{
		xtype: 'formpanel',
		instructions: 'Report Template Parameter describes a single parameter that is passed to the report during execution. If the parameter is not required and not defined, the parameter will be excluded.',
		url: 'reportTemplateParameter.json',
		labelWidth: 160,
		items: [
			{fieldLabel: 'Report Template', name: 'template.name', detailIdField: 'template.id', xtype: 'linkfield', detailPageClass: 'Clifton.report.ReportWindow'},
			{fieldLabel: 'Data Type', name: 'dataType.name', hiddenName: 'dataType.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'systemDataTypeList.json', detailPageClass: 'Clifton.system.schema.DataTypeWindow'},
			{fieldLabel: 'Parameter Name', name: 'name'},
			{fieldLabel: 'Parameter Label', name: 'label'},
			{fieldLabel: 'Template Parameter Name', name: 'templateParameterName'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Value List Table', name: 'valueTable.name', hiddenName: 'valueTable.id', xtype: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded'},
			{fieldLabel: 'Value List URL', name: 'valueListUrl'},
			{fieldLabel: 'UI Config', name: 'userInterfaceConfig'},
			{fieldLabel: 'Display Order', name: 'order', qtip: 'The order in which parameter is displayed on Query Export window'},
			{fieldLabel: 'Required', name: 'required', xtype: 'checkbox'}
		]
	}]
});

