Clifton.report.TemplateComponentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Report Template Component',
	iconCls: 'report',
	height: 600,
	width: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'reportTemplateComponent.json',
					instructions: 'A template component defines a report section that can be used with the corresponding template.',
					items: [
						{fieldLabel: 'Template', name: 'reportTemplate.name', xtype: 'linkfield', detailIdField: 'reportTemplate.id', detailPageClass: 'Clifton.report.TemplateWindow'},
						{fieldLabel: 'Component Type', name: 'reportTemplateComponentType.name', hiddenName: 'reportTemplateComponentType.id', xtype: 'combo', url: 'reportTemplateComponentTypeListFind.json'},
						{fieldLabel: 'Component Name', name: 'name'},
						{fieldLabel: 'External Name', name: 'externalReportName'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 150},
						{fieldLabel: 'Exclude Component Lookup SQL', name: 'excludeComponentLookupSql', xtype: 'textarea'}
					]
				}]
			},


			{
				title: 'Parameters',
				items: [{
					name: 'reportTemplateComponentParameterListByTemplateComponent',
					xtype: 'gridpanel',
					instructions: 'A template component parameter defines a report parameter that will be passed from the main report to the sub report.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Parameter Name', width: 100, dataIndex: 'name'},
						{header: 'Static', width: 25, dataIndex: 'staticValue', type: 'boolean'},
						{header: 'Default Value', width: 50, dataIndex: 'defaultValue'},
						{header: 'Description', width: 150, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.report.TemplateComponentParameterWindow',
						deleteURL: 'reportTemplateComponentParameterDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								reportTemplateComponent: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function() {
						return {'templateComponentId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Parameter Values',
				items: [{
					name: 'reportComponentParameterListFind',
					xtype: 'gridpanel',
					instructions: 'The following static component parameter values are associated with reports using this template component.',
					topToolbarSearchParameter: 'searchPattern',
					additionalPropertiesToRequest: 'reportComponent.id',
					columns: [
						{header: 'Report', width: 100, dataIndex: 'reportComponent.report.name', filter: {searchFieldName: 'reportName'}},
						{header: 'Parameter', width: 75, dataIndex: 'templateComponentParameter.name', filter: {searchFieldName: 'templateComponentParameterName'}},
						{header: 'Value', width: 75, dataIndex: 'parameterText', filter: 'parameterText'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.report.ReportComponentWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.reportComponent.id;
						}
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {'templateComponentId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Used By',
				items: [{
					name: 'reportListFind',
					xtype: 'gridpanel',
					instructions: 'The following reports include this template component.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Report Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 200, dataIndex: 'description'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.report.ReportWindow'
					},
					getTopToolbarInitialLoadParams: function() {
						return {'templateComponentId': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
