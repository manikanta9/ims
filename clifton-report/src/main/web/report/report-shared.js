Ext.ns('Clifton.report', 'Clifton.report.posting', 'Clifton.report.exports');

Clifton.report.posting.CliftonReportPostingWindowOverrides = {
	'InvestmentReport': 'Clifton.report.posting.InvestmentReportPostingWindow',
	'Contract': 'Clifton.report.posting.ContractReportPostingWindow'
};


Clifton.report.exports.CliftonReportExportWindowOverrides = {
	'ClientAccountReport': 'Clifton.report.exports.ClientAccountReportExportWindow',
	'InvestmentReport': 'Clifton.report.exports.InvestmentReportExportWindow',
	'PIOS': 'Clifton.report.exports.PIOSReportExportWindow',
	'DailyGainLossBySecurity': 'Clifton.report.exports.DailyGainLossBySecurityReportExportWindow'
};


Clifton.report.exports.DefaultReportExportWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Export Investment Reports',
	//modal: true,
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true,
	height: 300,
	templateName: 'REQUIRED-TEMPLATE_NAME',
	additionalReportFields: null,
	removeReportFields: null,
	exportFormat: 'PDF',

	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			buttonAlign: 'right',
			labelWidth: 200,
			listeners: {
				beforerender: function() {
					const win = this.getWindow();
					if (win.removeReportFields) {
						for (let i = 0; i < win.removeReportFields.length; i++) {
							const field = this.getForm().findField(win.removeReportFields[i]);
							this.remove(field);
						}
					}
					if (win.additionalReportFields) {
						this.add(win.additionalReportFields);
					}
				}
			},
			buttons: [{
				text: 'Export Report',
				handler: function() {
					const f = this.findParentByType('formpanel');
					const win = f.getWindow();
					const data = f.getForm().getValues();
					data['exportFormat'] = win.exportFormat;
					data['throwErrorOnBadReport'] = false;
					TCG.downloadFile('reportFileDownload.json', data, this);
				}
			}],
			items: [
				{
					fieldLabel: 'Report', name: 'label', hiddenName: 'reportId', xtype: 'combo', url: 'reportListFind.json', displayField: 'label',
					listeners: {
						beforequery: function(queryEvent) {
							const combo = queryEvent.combo;
							const win = combo.getParentForm().getWindow();
							combo.store.baseParams = {templateName: win.templateName};
						}
					}
				},
				{fieldLabel: 'Client Account', name: 'accountLabel', hiddenName: 'parameterMap[ClientAccountID]', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', mutuallyExclusiveFields: ['groupName']},
				{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'investmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['accountLabel'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
				{boxLabel: 'Keep sub report page numbers?', name: 'keepSubReportPageNumbers', checked: true, xtype: 'checkbox', labelSeparator: ''}
			]
		}
	]
});
