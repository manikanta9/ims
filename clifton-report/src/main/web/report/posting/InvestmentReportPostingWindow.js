TCG.use('Clifton.report.posting.BaseReportPostingForm');

Clifton.report.posting.InvestmentReportPostingWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Post Investment Reports to Portal',
	id: 'investmentReportPostWindow',
	iconCls: 'pdf',
	//modal: true,
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true,
	height: 400,

	items: [{
		xtype: 'report-posting-form',


		buttons: [{
			text: 'Post Report',
			handler: function() {
				const f = this.findParentByType('formpanel');
				const win = f.getWindow();
				const reportingDate = f.getForm().findField('reportPostingFileConfiguration.reportDate').getValue().format('m/d/y');
				const accountId = f.getForm().findField('parameterMap[ClientAccountID]').getValue();
				const account = accountId ? TCG.data.getData('investmentAccount.json?id=' + accountId, this) : null;

				const params = {
					'parameterMap[ReportingDate]': reportingDate,
					'reportPostingFileConfiguration.postEntitySourceTableName': 'InvestmentAccount',
					'reportPostingFileConfiguration.postEntitySourceFkFieldId': account ? account.id : null
				};
				f.getForm().submit(Ext.applyIf({
					url: 'reportPostingClientPost.json',
					waitMsg: 'Posting...',
					params: params,
					success: function(form, action) {
						Ext.Msg.alert('Report Posted Successfully', action.result.data.reportPostingFileConfiguration.fileWrapper.fileName + ' posted successfully');
						win.closeWindow();
					},
					error: function(form, action) {
						win.closeWindow();
					}
				}, Ext.applyIf({timeout: 16000}, TCG.form.submitDefaults)));
			}
		}],

		commonItems: [
			{fieldLabel: 'Report', name: 'label', hiddenName: 'reportId', xtype: 'combo', url: 'reportListFind.json?templateName=Investment Report', displayField: 'label', allowBlank: false},
			{fieldLabel: 'Report Date', name: 'reportPostingFileConfiguration.reportDate', xtype: 'datefield', value: Clifton.calendar.getBusinessDayFrom(-1), allowBlank: false},
			{fieldLabel: 'Client Account', name: 'accountLabel', hiddenName: 'parameterMap[ClientAccountID]', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', mutuallyExclusiveFields: ['groupName']},
			{xtype: 'hidden', name: 'additionalReportParameter.name', value: 'investmentAccountGroupId'},
			{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'additionalReportParameter.value', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['accountLabel'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'}
		],
		portalItems: [
			{
				fieldLabel: 'File Group', name: 'reportPostingFileConfiguration.portalFileCategory.name', hiddenName: 'reportPostingFileConfiguration.portalFileCategoryId', xtype: 'combo', url: 'portalFileCategoryForUploadListFind.json?categoryName=Reporting', allowBlank: false,
				requestedProps: 'portalFileFrequency',
				listeners: {
					select: function(combo, record, index) {
						const fp = combo.getParentForm();
						if (record.json) {
							if (!TCG.isBlank(record.json.portalFileFrequency)) {
								fp.setFormValue('reportPostingFileConfiguration.portalFileFrequency', record.json.portalFileFrequency);
							}
						}
					}
				}
			},
			{
				fieldLabel: 'File Frequency', name: 'reportPostingFileConfiguration.portalFileFrequency', hiddenName: 'reportPostingFileConfiguration.portalFileFrequency', xtype: 'combo', mode: 'local',
				store: {
					xtype: 'arraystore',
					data: Clifton.portal.file.PortalFileFrequencies
				}
			},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: 'If File Exists:',
				items: [
					{boxLabel: 'Error', name: 'reportPostingFileConfiguration.fileDuplicateAction', inputValue: 'ERROR', checked: true, qtip: 'Returns an error if file appears to be a duplicate on the new portal'},
					{boxLabel: 'Replace (If Existing file is Unapproved Only)', name: 'reportPostingFileConfiguration.fileDuplicateAction', inputValue: 'REPLACE', qtip: 'Replacing the file is only allowed if the file that is being replaced is not currently approved.'},
					{boxLabel: 'Skip', name: 'reportPostingFileConfiguration.fileDuplicateAction', inputValue: 'SKIP', qtip: 'Do Nothing - Leave the existing file as is and do not post this one.'}
				]
			}
		]
	}]
});
