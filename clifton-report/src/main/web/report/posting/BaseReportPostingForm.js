Clifton.report.posting.BaseReportPostingForm = Ext.extend(TCG.form.FormPanel, {

	loadValidation: false,
	loadDefaultDataAfterRender: true,
	buttonAlign: 'right',
	labelWidth: 200,


	initComponent: function() {
		const currentItems = [];
		Ext.each(this.commonItems, function(f) {
			currentItems.push(f);
		});

		Ext.each(this.portalItems, function(f) {
			currentItems.push(f);
		});
		this.items = currentItems;
		Clifton.report.posting.BaseReportPostingForm.superclass.initComponent.call(this);
	},


	getDefaultData: function(win) {
		const dd = win.defaultData || {};
		if (!dd.reportPostingFileConfiguration) {
			dd.reportPostingFileConfiguration = {};
		}
		return dd;
	},

	commonItems: [],
	portalItems: []
});
Ext.reg('report-posting-form', Clifton.report.posting.BaseReportPostingForm);
