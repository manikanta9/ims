TCG.use('Clifton.report.posting.BaseReportPostingForm');

// Could be refactored to be more generic support for documents, but contracts are the only ones that need this right now
Clifton.report.posting.ContractReportPostingWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Post Business Contract to Client Website',
	iconCls: 'pdf',
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true,
	height: 650,
	width: 700,

	defaultDataIsReal: true,

	items: [{
		xtype: 'report-posting-form',

		buttons: [{
			text: 'Post Document',
			handler: function() {
				const f = this.findParentByType('formpanel');
				const win = f.getWindow();

				f.getForm().submit(Ext.applyIf({
					url: 'reportPostingClientUpload.json',
					waitMsg: 'Posting...',
					success: function(form, action) {
						Ext.Msg.alert('Report Posted Successfully', 'Posted Successfully.');
						win.closeWindow();
					},
					error: function(form, action) {
						Ext.Msg.alert('Failed to post document', action.result.data.reportPostingFileConfiguration.fileWrapper.fileName + ' posted failure');
					}
				}, Ext.applyIf({timeout: 16000}, TCG.form.submitDefaults)));
			}
		}],


		commonItems: [
			{fieldLabel: 'DocumentID', name: 'documentId', xtype: 'hidden'},
			{fieldLabel: 'ForceAsPDF', name: 'forceDocumentAsPDF', xtype: 'hidden', value: 'true'},
			{fieldLabel: 'Document', name: 'documentName', xtype: 'displayfield', submitValue: false},
			{
				fieldLabel: 'Report Date', name: 'reportPostingFileConfiguration.reportDate', xtype: 'datefield', value: Clifton.calendar.getBusinessDayFrom(-1),
				listeners: {
					select: function(field) {
						const f = TCG.getParentFormPanel(this);
						f.updateDateFields(field.getValue());
					},
					change: function(field) {
						const f = TCG.getParentFormPanel(this);
						f.updateDateFields(field.getValue());
					},
					specialkey: function(field, e) {
						if (e.getKey() === e.ENTER) {
							const f = TCG.getParentFormPanel(this);
							f.updateDateFields(field.getValue());
						}
					}
				}
			},
			{fieldLabel: 'Publish Date', name: 'reportPostingFileConfiguration.publishDate', xtype: 'datefield', value: (new Date())}
		],
		portalItems: [
			{xtype: 'hidden', name: 'reportPostingFileConfiguration.postEntitySourceTableName'},
			{xtype: 'hidden', name: 'reportPostingFileConfiguration.postEntitySourceFkFieldId'},
			{xtype: 'hidden', name: 'reportPostingFileConfiguration.sourceEntitySectionName'},
			{xtype: 'hidden', name: 'reportPostingFileConfiguration.sourceEntityFkFieldId'},
			{
				fieldLabel: 'File Group', name: 'reportPostingFileConfiguration.portalFileCategory.name', hiddenName: 'reportPostingFileConfiguration.portalFileCategoryId', xtype: 'combo', url: 'portalFileCategoryForUploadListFind.json?categoryName=Documentation', allowBlank: false,
				requestedProps: 'portalFileFrequency',
				listeners: {
					select: function(combo, record, index) {
						const fp = combo.getParentForm();
						if (record.json) {
							if (!TCG.isBlank(record.json.portalFileFrequency)) {
								fp.setFormValue('reportPostingFileConfiguration.portalFileFrequency', record.json.portalFileFrequency);
							}
							fp.setFormValue('reportPostingFileConfiguration.displayName', record.json.name);
						}
					}
				}
			},
			{fieldLabel: 'Display Name', name: 'reportPostingFileConfiguration.displayName', allowBlank: false},
			{
				fieldLabel: 'File Frequency', name: 'reportPostingFileConfiguration.portalFileFrequency', hiddenName: 'reportPostingFileConfiguration.portalFileFrequency', xtype: 'combo', mode: 'local', requiredFields: ['reportPostingFileConfiguration.portalFileCategory.name'],
				store: {
					xtype: 'arraystore',
					data: Clifton.portal.file.PortalFileFrequencies
				}
			},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: 'If File Exists:',
				items: [
					{boxLabel: 'Error', name: 'reportPostingFileConfiguration.fileDuplicateAction', inputValue: 'ERROR', checked: true, qtip: 'Returns an error if file appears to be a duplicate on the new portal'},
					{boxLabel: 'Replace (If Existing file is Unapproved Only)', name: 'reportPostingFileConfiguration.fileDuplicateAction', inputValue: 'REPLACE', qtip: 'Replacing the file is only allowed if the file that is being replaced is not currently approved.'},
					{boxLabel: 'Skip', name: 'reportPostingFileConfiguration.fileDuplicateAction', inputValue: 'SKIP', qtip: 'Do Nothing - Leave the existing file as is and do not post this one.'}
				]
			}
		]
	}]
});
