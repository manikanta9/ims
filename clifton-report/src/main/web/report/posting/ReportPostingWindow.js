Clifton.report.posting.ReportPostingWindow = Ext.extend(TCG.app.DetailWindowSelector, {

	getClassName: function(config, entity) {
		if (Clifton.report.posting.CliftonReportPostingWindowOverrides && Clifton.report.posting.CliftonReportPostingWindowOverrides[config.postingType]) {
			return Clifton.report.posting.CliftonReportPostingWindowOverrides[config.postingType];
		}
		return '';
	}

});
