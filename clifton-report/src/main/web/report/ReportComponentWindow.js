Clifton.report.ReportComponentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Report Component',
	iconCls: 'report',

	items: [{
		xtype: 'formwithdynamicfields',
		dynamicTriggerFieldName: 'templateComponent.name',
		dynamicTriggerValueFieldName: 'templateComponent.id',
		dynamicFieldTypePropertyName: 'templateComponentParameter',
		dynamicFieldListPropertyName: 'componentParameterList',
		dynamicFieldsUrl: 'reportTemplateComponentParameterListByTemplateComponent.json?staticValue=true',
		dynamicFieldsUrlParameterName: 'templateComponentId',
		dynamicFieldValuePropertyName: 'parameterValue',
		dynamicFieldTextPropertyName: 'parameterText',

		url: 'reportComponent.json',
		labelWidth: 200,
		labelFieldName: 'id',
		items: [
			{fieldLabel: 'Report', name: 'report.name', xtype: 'linkfield', detailIdField: 'report.id', detailPageClass: 'Clifton.report.ReportWindow'},
			{fieldLabel: 'Template Component', name: 'templateComponent.name', xtype: 'linkfield', detailIdField: 'templateComponent.id', detailPageClass: 'Clifton.report.TemplateComponentWindow'},
			{fieldLabel: 'Component Type', name: 'templateComponent.reportTemplateComponentType.name', xtype: 'displayfield'},
			{fieldLabel: 'Component Name', name: 'name'},
			{xtype: 'label', html: '<hr/>'}
		]
	}]
});
