Clifton.report.exports.DailyGainLossBySecurityReportExportWindow = Ext.extend(Clifton.report.exports.DefaultReportExportWindow, {
	title: 'Export Daily Gain Loss Reports',
	templateName: 'Daily Gain Loss by Security',
	iconCls: 'excel',
	exportFormat: 'EXCEL',
	removeReportFields: ['groupName', 'keepSubReportPageNumbers'],
	additionalReportFields: [
		{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'parameterMap[InvestmentAccountGroupID]', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['accountLabel'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
		{fieldLabel: 'Date From', name: 'parameterMap[DateFrom]', xtype: 'datefield', value: new Date((new Date()).getFullYear(), (new Date()).getMonth(), 1)},
		{fieldLabel: 'Date To', name: 'parameterMap[DateTo]', xtype: 'datefield', value: Clifton.calendar.getBusinessDayFrom(-1)}
	]
});
