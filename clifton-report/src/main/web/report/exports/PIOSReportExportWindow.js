Clifton.report.exports.PIOSReportExportWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Export Portfolio Reports',
	iconCls: 'pdf',
	//modal: true,
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true,
	height: 300,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		buttonAlign: 'right',
		labelWidth: 200,
		instructions: 'Exports a Portfolio report for multiple clients.  If no report is selected, then the default Portfolio report for the client will be used.',
		buttons: [{
			text: 'Export Report',
			handler: function() {
				const f = this.findParentByType('formpanel');
				const data = f.getForm().getValues();
				const reportingDate = f.getForm().findField('reportingDate').getValue().format('m/d/Y');
				data['exportFormat'] = 'PDF';
				data['throwErrorOnBadReport'] = false;
				data['parameterMap[BalanceDate]'] = reportingDate;
				data.cacheUsed = true;
				TCG.downloadFile('reportFileDownload.json', data, this);
			}
		}],
		items: [
			{fieldLabel: 'Report', name: 'label', hiddenName: 'reportId', xtype: 'combo', url: 'reportListFind.json?categoryName=Report Template Tags&categoryTableName=ReportTemplate&categoryLinkFieldPath=reportTemplate&categoryHierarchyName=Portfolio Runs', displayField: 'label'},
			{xtype: 'hidden', name: 'additionalReportParameter.name', value: 'portfolioRunInvestmentAccountGroupId'},
			{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'additionalReportParameter.value', xtype: 'combo', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
			{fieldLabel: 'Balance Date', name: 'reportingDate', xtype: 'datefield', value: Clifton.calendar.getBusinessDayFrom(-1)},
			{boxLabel: 'Keep sub report page numbers?', name: 'keepSubReportPageNumbers', checked: true, xtype: 'checkbox', labelSeparator: ''},
			{boxLabel: 'Regenerate all reports?', name: 'regenerateCache', checked: false, xtype: 'checkbox', labelSeparator: ''}
		]
	}]
});
