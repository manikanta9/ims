Clifton.report.exports.InvestmentReportExportWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Export Investment Reports',
	iconCls: 'pdf',
	//modal: true,
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true,
	height: 300,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		buttonAlign: 'right',
		labelWidth: 200,
		buttons: [{
			text: 'Export Report',
			handler: function() {
				const f = this.findParentByType('formpanel');
				const data = f.getForm().getValues();
				data['exportFormat'] = 'PDF';
				data['throwErrorOnBadReport'] = false;
				TCG.downloadFile('reportFileDownload.json', data, this);
			}
		}],
		items: [
			{fieldLabel: 'Report', name: 'label', hiddenName: 'reportId', xtype: 'combo', url: 'reportListFind.json?templateName=Investment Report', displayField: 'label'},
			{fieldLabel: 'Client Account', name: 'accountLabel', hiddenName: 'parameterMap[ClientAccountID]', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', mutuallyExclusiveFields: ['groupName']},
			{xtype: 'hidden', name: 'additionalReportParameter.name', value: 'investmentAccountGroupId'},
			{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'additionalReportParameter.value', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['accountLabel'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
			{fieldLabel: 'Reporting Date', name: 'parameterMap[ReportingDate]', xtype: 'datefield', value: Clifton.calendar.getBusinessDayFrom(-1)},
			{boxLabel: 'Keep sub report page numbers?', name: 'keepSubReportPageNumbers', checked: true, xtype: 'checkbox', labelSeparator: ''}
		]
	}]
});
