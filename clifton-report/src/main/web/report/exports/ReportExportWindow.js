Clifton.report.exports.ReportExportWindow = Ext.extend(TCG.app.DetailWindowSelector, {

	getClassName: function(config, entity) {
		if (Clifton.report.exports.CliftonReportExportWindowOverrides && Clifton.report.exports.CliftonReportExportWindowOverrides[config.exportType]) {
			return Clifton.report.exports.CliftonReportExportWindowOverrides[config.exportType];
		}
		return '';
	}

});
