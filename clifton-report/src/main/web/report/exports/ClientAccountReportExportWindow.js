Clifton.report.exports.ClientAccountReportExportWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Export Client Account Report',
	iconCls: 'report',
	//modal: true,
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true,
	height: 250,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		buttonAlign: 'right',
		labelWidth: 200,
		instructions: 'Export Selected Report for selected Client Account. Available reports are only those that accept Client Account and Report Date as parameters. (Template tag = Client Account Report)',
		buttons: [
			{
				text: 'Export Report',
				iconCls: 'pdf',
				xtype: 'splitbutton',
				handler: function() {
					TCG.getParentFormPanel(this).executeReport('PDF');
				},
				menu: {
					items: [
						{
							text: 'Excel',
							iconCls: 'excel_open_xml',
							tooltip: 'Generate the excel version of the report.',
							handler: function() {
								TCG.getParentFormPanel(this).executeReport('EXCEL_OPEN_XML');
							}
						},
						{
							text: 'Excel (97-2003)',
							iconCls: 'excel',
							tooltip: 'Generate the excel version of the report.',
							handler: function() {
								TCG.getParentFormPanel(this).executeReport('EXCEL');
							}
						},
						{
							text: 'Word',
							iconCls: 'word',
							tooltip: 'Generate the word version of the report.',
							handler: function() {
								TCG.getParentFormPanel(this).executeReport('WORD');
							}
						}
					]
				}
			}
		],
		items: [
			{xtype: 'hidden', name: 'clientAccountId'},
			{fieldLabel: 'Client Account', name: 'clientAccountLabel', xtype: 'displayfield'},
			{fieldLabel: 'Report', name: 'label', hiddenName: 'reportId', xtype: 'combo', url: 'reportListFind.json?categoryName=Report Template Tags&categoryTableName=ReportTemplate&categoryLinkFieldPath=reportTemplate&categoryHierarchyName=Client Account Report', displayField: 'label', allowBlank: false},
			{fieldLabel: 'Reporting Date', name: 'parameterMap[ReportingDate]', xtype: 'datefield', value: Clifton.calendar.getBusinessDayFrom(-1)}
		],

		executeReport: function(format) {
			const data = this.getForm().getValues();
			data['parameterMap[ClientAccountID]'] = data['clientAccountId'];
			data['exportFormat'] = format;
			data['throwErrorOnBadReport'] = false;
			data['reportFileName'] = this.getForm().findField('clientAccountLabel').getValue() + ' - ' + this.getForm().findField('reportId').getRawValue() + ' - ' + data['parameterMap[ReportingDate]'];
			TCG.downloadFile('reportFileDownload.json', data, this);
		}
	}]
});
