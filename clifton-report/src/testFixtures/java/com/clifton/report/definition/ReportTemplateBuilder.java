package com.clifton.report.definition;

import com.clifton.report.definition.template.ReportTemplate;

import java.math.BigDecimal;


public class ReportTemplateBuilder {

	private ReportTemplate reportTemplate;


	private ReportTemplateBuilder(ReportTemplate reportTemplate) {
		this.reportTemplate = reportTemplate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static ReportTemplateBuilder createTradeReport() {
		ReportTemplate reportTemplate = new ReportTemplate();

		reportTemplate.setId((short) 28);
		reportTemplate.setName("Trade Report (Landscape)");
		reportTemplate.setDescription("Trade Report (Landscape)");
		reportTemplate.setUrl("/Trade Components/Trade Landscape Report");
		reportTemplate.setSubReportWidth(BigDecimal.valueOf(9.5000000000));

		return new ReportTemplateBuilder(reportTemplate);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ReportTemplateBuilder withId(short id) {
		getReportTemplate().setId(id);
		return this;
	}


	public ReportTemplateBuilder withName(String name) {
		getReportTemplate().setName(name);
		return this;
	}


	public ReportTemplateBuilder withDescription(String description) {
		getReportTemplate().setDescription(description);
		return this;
	}


	public ReportTemplateBuilder withUrl(String url) {
		getReportTemplate().setUrl(url);
		return this;
	}


	public ReportTemplateBuilder withSubReportWidth(BigDecimal subReportWidth) {
		getReportTemplate().setSubReportWidth(subReportWidth);
		return this;
	}


	public ReportTemplate toReportTemplate() {
		return this.reportTemplate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ReportTemplate getReportTemplate() {
		return this.reportTemplate;
	}
}
