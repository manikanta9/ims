package com.clifton.report.definition;

import com.clifton.report.definition.template.ReportTemplate;


public class ReportBuilder {

	private Report report;


	private ReportBuilder(Report report) {
		this.report = report;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static ReportBuilder createGenericTrade() {
		Report report = new Report();

		report.setId(369);
		report.setReportTemplate(ReportTemplateBuilder.createTradeReport().toReportTemplate());
		report.setName("Generic Trade Report");
		report.setDescription("Generic Trade Report - Displays trade information for selected trade, and includes all trades for the same trade date, trade type, client account, holding account, collateral, and executing broker.  For collateral trades will also include collateral balance information.");

		return new ReportBuilder(report);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ReportBuilder withId(int id) {
		getReport().setId(id);
		return this;
	}


	public ReportBuilder withName(String name) {
		getReport().setName(name);
		return this;
	}


	public Report toReport() {
		return this.report;
	}


	public ReportBuilder withReportTemplate(ReportTemplate reportTemplate) {
		getReport().setReportTemplate(reportTemplate);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Report getReport() {
		return this.report;
	}
}
