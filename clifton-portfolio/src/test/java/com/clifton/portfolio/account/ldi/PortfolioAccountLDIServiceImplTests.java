package com.clifton.portfolio.account.ldi;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.search.MarketDataFieldSearchForm;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PortfolioAccountLDIServiceImplTests {

	@Resource
	private MarketDataFieldService marketDataFieldService;

	@Resource
	private PortfolioAccountLDIServiceImpl portfolioAccountLDIService;

	public static final String KRD_1_MONTH = "1 Month KRD";
	public static final String KRD_3_MONTH = "3 Month KRD";
	public static final String KRD_6_MONTH = "6 Month KRD";
	public static final String KRD_1_YEAR = "1 Year KRD";
	public static final String KRD_2_YEAR = "2 Year KRD";
	public static final String KRD_3_YEAR = "3 Year KRD";
	public static final String KRD_4_YEAR = "4 Year KRD";
	public static final String KRD_5_YEAR = "5 Year KRD";
	public static final String KRD_6_YEAR = "6 Year KRD";
	public static final String KRD_7_YEAR = "7 Year KRD";
	public static final String KRD_8_YEAR = "8 Year KRD";
	public static final String KRD_9_YEAR = "9 Year KRD";
	public static final String KRD_10_YEAR = "10 Year KRD";
	public static final String KRD_15_YEAR = "15 Year KRD";
	public static final String KRD_20_YEAR = "20 Year KRD";
	public static final String KRD_25_YEAR = "25 Year KRD";
	public static final String KRD_30_YEAR = "30 Year KRD";
	public static final String KRD_50_YEAR = "50 Year KRD";


	@Test
	public void testGetKeyRateDurationPointModifiedValueMap_1() {
		List<PortfolioAccountKeyRateDurationPoint> accountPointList = setupPortfolioAccountKeyRateDurationPointList(new String[]{KRD_2_YEAR, KRD_4_YEAR, KRD_7_YEAR, KRD_10_YEAR, KRD_15_YEAR,
				KRD_20_YEAR, KRD_25_YEAR, KRD_30_YEAR, KRD_50_YEAR});
		Map<MarketDataField, BigDecimal> krdMap = setupMarketDataKrdValueMap(new double[]{0.0, 0.0, 0.01, 0.02, 0.05, 0.08, 0.11, 0.16, 0.20, 0.24, 0.29, 0.34, 1.28, 2.63, 2.91, 2.70, 5.30, 0.0});

		Map<PortfolioAccountKeyRateDurationPoint, BigDecimal> modifiedKrdMap = this.portfolioAccountLDIService.getKeyRateDurationPointModifiedValueMap(accountPointList, krdMap);
		validateModifiedKRDMap(modifiedKrdMap, new double[]{0.12, 0.323, 0.733, 1.603, 2.63, 2.91, 2.70, 5.30, 0});
	}


	@Test
	public void testGetKeyRateDurationPointModifiedValueMap_2() {
		List<PortfolioAccountKeyRateDurationPoint> accountPointList = setupPortfolioAccountKeyRateDurationPointList(new String[]{KRD_3_MONTH, KRD_1_YEAR, KRD_3_YEAR});
		Map<MarketDataField, BigDecimal> krdMap = setupMarketDataKrdValueMap(new double[]{0.2, 0.5, 0.7, 1.2, 1.3, 0.5, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0});

		Map<PortfolioAccountKeyRateDurationPoint, BigDecimal> modifiedKrdMap = this.portfolioAccountLDIService.getKeyRateDurationPointModifiedValueMap(accountPointList, krdMap);
		validateModifiedKRDMap(modifiedKrdMap, new double[]{1.167, 2.083, 1.250});
	}


	////////////////////////////////////////////////////////////
	////////            Helper Methods                  ////////
	////////////////////////////////////////////////////////////


	private List<PortfolioAccountKeyRateDurationPoint> setupPortfolioAccountKeyRateDurationPointList(String[] dataFieldNames) {
		List<PortfolioAccountKeyRateDurationPoint> list = new ArrayList<>();

		int counter = 0;
		for (String dataFieldName : dataFieldNames) {
			counter++;
			PortfolioAccountKeyRateDurationPoint point = new PortfolioAccountKeyRateDurationPoint();
			point.setId(counter);
			point.setMarketDataField(this.marketDataFieldService.getMarketDataFieldByName(dataFieldName));
			list.add(point);
		}

		return list;
	}


	private Map<MarketDataField, BigDecimal> setupMarketDataKrdValueMap(double[] krdValues) {
		Map<MarketDataField, BigDecimal> krdMap = new LinkedHashMap<>();
		List<MarketDataField> fieldList = this.marketDataFieldService.getMarketDataFieldList(new MarketDataFieldSearchForm());
		fieldList = BeanUtils.sortWithFunction(fieldList, MarketDataField::getFieldOrder, true);
		Assertions.assertEquals(fieldList.size(), krdValues.length, "Supplied KRD values must match the size of KRD Fields.");
		for (int i = 0; i < fieldList.size(); i++) {
			krdMap.put(fieldList.get(i), BigDecimal.valueOf(krdValues[i]));
		}
		return krdMap;
	}


	private void validateModifiedKRDMap(Map<PortfolioAccountKeyRateDurationPoint, BigDecimal> modifiedKrdMap, double[] expectedValues) {
		Assertions.assertEquals(modifiedKrdMap.size(), expectedValues.length, "Supplied KRD modified values must match the size of KRD Points used.");

		int i = 0;
		for (Map.Entry<PortfolioAccountKeyRateDurationPoint, BigDecimal> portfolioAccountKeyRateDurationPointBigDecimalEntry : modifiedKrdMap.entrySet()) {
			BigDecimal value = portfolioAccountKeyRateDurationPointBigDecimalEntry.getValue();
			BigDecimal expectedValue = BigDecimal.valueOf(expectedValues[i]);
			Assertions.assertTrue(MathUtils.isEqual(expectedValue, MathUtils.round(value, 3)), "Expected KRD Point " + (portfolioAccountKeyRateDurationPointBigDecimalEntry.getKey()).getMarketDataField().getName() + " modified KRD Value to be [" + CoreMathUtils.formatNumberDecimal(expectedValue) + "] but was ["
					+ CoreMathUtils.formatNumberDecimal(value) + "].");
			i++;
		}
	}
}
