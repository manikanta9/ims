package com.clifton.portfolio;


import com.clifton.core.test.BasicProjectTests;
import com.clifton.core.util.CollectionUtils;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The <code>PortfolioProjectBasicTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PortfolioProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "portfolio";
	}


	@Override
	protected void configureDTOSkipPropertyNames(Set<String> skipPropertyNames) {
		skipPropertyNames.add("customColumns");
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.apache.commons.math3.linear.MatrixUtils");
		imports.add("org.apache.commons.math3.linear.RealMatrix");
		imports.add("org.apache.commons.math3.linear.RealVector");
		imports.add("org.apache.commons.math3.linear.LUDecomposition");
	}


	@Override
	protected boolean isMethodSkipped(@SuppressWarnings("unused") Method method) {
		Set<String> skipMethods = new HashSet<>();
		skipMethods.add("savePortfolioCashFlow");
		return skipMethods.contains(method.getName());
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		return CollectionUtils.createHashSet(
				"savePortfolioRunMarginListByRun",
				"savePortfolioCurrencyOtherListByRun"
		);
	}
}
