package com.clifton.portfolio.run.ldi.manager.populator;

import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalance;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalanceSearchForm;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalanceService;
import com.clifton.portfolio.run.manager.populator.BasePortfolioRunManagerAccountBalancePopulatorTests;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PortfolioRunLDIManagerAccountBalancePopulatorTests extends BasePortfolioRunManagerAccountBalancePopulatorTests {

	@Resource
	private PortfolioRunManagerAccountBalanceService portfolioRunManagerAccountBalanceService;

	private static final int RUN_ZERO_BALANCE = 1;
	private static final int RUN_BALANCE_ADJUSTMENTS = 2;
	private static final int RUN_BALANCE_ADJUSTMENTS_MOC = 3;


	/**
	 * Regular tests - all zero balances to verify managers pull through correctly
	 */
	@Test
	public void testPortfolioRunLIDManagerAccountBalancePopulator_ZeroBalance() {
		List<PortfolioRunManagerAccountBalance> managerAccountBalanceList = this.portfolioRunManagerAccountBalanceService.getPortfolioRunManagerAccountBalanceList(RUN_ZERO_BALANCE, new PortfolioRunManagerAccountBalanceSearchForm());
		String expected = "MOC\tManager #\tAllocation\tBase Securities\tBase Cash\tBase Total\tAdjustment Count\tSecurities Adjustment\tCash Adjustment\tAdjusted Securities\tAdjusted Cash\tAdjusted Total\n" +
				"false\tM00001\t100.00\t0\t0\t0\t0\t\t0\t0\t0\n" +
				"false\tM00002\t100.00\t0\t0\t0\t0\t\t0\t0\t0\n" +
				"false\tM00030\t100.00\t0\t0\t0\t0\t\t0\t0\t0\n" +
				"false\tM00040\t100.00\t0\t0\t0\t0\t\t0\t0\t0";
		validatePortfolioRunManagerAccountBalanceList(expected, managerAccountBalanceList);
	}


	/**
	 * Test Regular run with balances and adjustments
	 */
	@Test
	public void testPortfolioRunLIDManagerAccountBalancePopulator_BalanceAdjustments() {
		List<PortfolioRunManagerAccountBalance> managerAccountBalanceList = this.portfolioRunManagerAccountBalanceService.getPortfolioRunManagerAccountBalanceList(RUN_BALANCE_ADJUSTMENTS, new PortfolioRunManagerAccountBalanceSearchForm());
		String expected = "MOC\tManager #\tAllocation\tBase Securities\tBase Cash\tBase Total\tAdjustment Count\tSecurities Adjustment\tCash Adjustment\tAdjusted Securities\tAdjusted Cash\tAdjusted Total\n" +
				"false\tM00001\t100.00\t25,000\t0\t25,000\t1\t5,000\t30,000\t0\t30,000\n" +
				"false\tM00002\t100.00\t0\t200,000\t200,000\t0\t\t0\t200,000\t200,000\n" +
				"false\tM00030\t100.00\t5,000\t120,000\t125,000\t1\t-5,000\t0\t120,000\t120,000\n" +
				"false\tM00040\t100.00\t-20,000\t60,000\t40,000\t2\t-10,000\t-30,000\t60,000\t30,000\n";
		validatePortfolioRunManagerAccountBalanceList(expected, managerAccountBalanceList);
	}


	/**
	 * Test MOC run with balances and adjustments
	 */
	@Test
	public void testPortfolioRunLIDManagerAccountBalancePopulator_BalanceAdjustments_MOC() {
		List<PortfolioRunManagerAccountBalance> managerAccountBalanceList = this.portfolioRunManagerAccountBalanceService.getPortfolioRunManagerAccountBalanceList(RUN_BALANCE_ADJUSTMENTS_MOC, new PortfolioRunManagerAccountBalanceSearchForm());
		String expected = "MOC\tManager #\tAllocation\tBase Securities\tBase Cash\tBase Total\tAdjustment Count\tSecurities Adjustment\tCash Adjustment\tAdjusted Securities\tAdjusted Cash\tAdjusted Total\n" +
				"true\tM00001\t100.00\t25,000\t0\t25,000\t1\t5,000\t30,000\t0\t30,000\n" +
				"true\tM00002\t100.00\t0\t200,000\t200,000\t1\t20,000\t0\t220,000\t220,000\n" +
				"true\tM00030\t100.00\t-45,000\t170,000\t125,000\t3\t45,000\t15,000\t0\t185,000\t185,000\n" +
				"true\tM00040\t100.00\t-70,000\t60,000\t-10,000\t5\t90,000\t15,000\t20,000\t75,000\t95,000";
		validatePortfolioRunManagerAccountBalanceList(expected, managerAccountBalanceList);
	}
}
