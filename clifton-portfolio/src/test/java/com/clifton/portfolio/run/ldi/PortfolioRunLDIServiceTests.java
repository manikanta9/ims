package com.clifton.portfolio.run.ldi;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author mitchellf
 */
@ContextConfiguration("PortfolioRunLDIServiceTests-context.xml")
@Transactional
public class PortfolioRunLDIServiceTests extends BaseInMemoryDatabaseTests {

	@Resource
	private PortfolioRunLDIService portfolioRunLDIService;

	private static final Integer PORTFOLIO_RUN_ID = 1095306;
	private static final Integer NEW_SECURITY_ID = 120261;
	private static final Integer EXISTING_SECURITY_ID = 115962;
	private static final Integer INVALID_SECURITY_ID = 236;
	private static final Integer CURRENT_EXPOSURE_ID = 244361;
	private static HashMap<String, BigDecimal> KRD_VALUES;
	private static final Date DATE_10_10_2019 = DateUtils.clearTime(DateUtils.toDate("10/10/2019"));


	@Test
	public void testCopyPortfolioRunLDIExposure() {
		initKRDValues();

		PortfolioRunLDIExposure currentExposure = this.portfolioRunLDIService.getPortfolioRunLDIExposure(CURRENT_EXPOSURE_ID);

		List<PortfolioRunLDIExposureKeyRateDurationPoint> oldKrdPoints = currentExposure.getKeyRateDurationPointList();

		for (PortfolioRunLDIExposureKeyRateDurationPoint point : oldKrdPoints) {
			Assertions.assertEquals(point.getKrdValue(), KRD_VALUES.get(point.getKeyRateDurationPoint().getMarketDataField().getName()).setScale(10), "Could not complete test - unexpected initial KRD value.");
		}

		updateKRDValues();

		PortfolioRunLDIExposure newExposure = this.portfolioRunLDIService.copyPortfolioRunLDIExposure(CURRENT_EXPOSURE_ID, NEW_SECURITY_ID, DATE_10_10_2019);

		List<PortfolioRunLDIExposure> exposures = this.portfolioRunLDIService.getPortfolioRunLDIExposureListByRun(PORTFOLIO_RUN_ID, true);
		List<Integer> securityIds = exposures.stream().map(PortfolioRunLDIExposure::getSecurity).map(InvestmentSecurity::getId).collect(Collectors.toList());
		Assertions.assertTrue(CollectionUtils.contains(securityIds, NEW_SECURITY_ID));

		List<PortfolioRunLDIExposureKeyRateDurationPoint> newKrdPoints = newExposure.getKeyRateDurationPointList();

		for (PortfolioRunLDIExposureKeyRateDurationPoint point : newKrdPoints) {
			Assertions.assertEquals(point.getKrdValue(), KRD_VALUES.get(point.getKeyRateDurationPoint().getMarketDataField().getName()).setScale(20));
		}
	}


	@Test
	public void testCopyPortfolioRunLDIExposure_SecurityAlreadyExists() {
		TestUtils.expectException(ValidationException.class,
				() -> this.portfolioRunLDIService.copyPortfolioRunLDIExposure(this.CURRENT_EXPOSURE_ID, this.EXISTING_SECURITY_ID, this.DATE_10_10_2019),
				"The security selected already exists for client account");
	}


	@Test
	public void testCopyPortfolioRunLDIExposure_InvalidSecurityForReplication() {
		TestUtils.expectException(ValidationException.class,
				() -> this.portfolioRunLDIService.copyPortfolioRunLDIExposure(this.CURRENT_EXPOSURE_ID, this.INVALID_SECURITY_ID, this.DATE_10_10_2019),
				"The selected security is not valid for Replication");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void initKRDValues() {
		KRD_VALUES = new HashMap<>();
		KRD_VALUES.put("7 Year KRD (Spot)", BigDecimal.ZERO);
		KRD_VALUES.put("20 Year KRD (Spot)", BigDecimal.ZERO);
		KRD_VALUES.put("25 Year KRD (Spot)", BigDecimal.ZERO);
		KRD_VALUES.put("10 Year KRD (Spot)", BigDecimal.ZERO);
		KRD_VALUES.put("2 Year KRD (Spot)", BigDecimal.valueOf(2.17));
		KRD_VALUES.put("4 Year KRD (Spot)", BigDecimal.ZERO);
	}


	private void updateKRDValues() {
		KRD_VALUES = new HashMap<>();
		KRD_VALUES.put("7 Year KRD (Spot)", BigDecimal.ZERO);
		KRD_VALUES.put("20 Year KRD (Spot)", BigDecimal.ZERO);
		KRD_VALUES.put("25 Year KRD (Spot)", BigDecimal.ZERO);
		KRD_VALUES.put("10 Year KRD (Spot)", BigDecimal.ZERO);
		KRD_VALUES.put("2 Year KRD (Spot)", BigDecimal.valueOf(1.9477605));
		KRD_VALUES.put("4 Year KRD (Spot)", BigDecimal.valueOf(0.2110855));
	}
}
