SELECT 'PortfolioRunLDIExposure' AS entityTableName, PortfolioRunLDIExposureID AS entityId
FROM PortfolioRunLDIExposure
WHERE PortfolioRunID = 1163804
UNION
SELECT 'PortfolioRunLDIExposureKeyRateDurationPoint' AS entityTableName, PortfolioRunLDIExposureKeyRateDurationPointID AS entityID
FROM PortfolioRunLDIExposureKeyRateDurationPoint
WHERE PortfolioRunLDIExposureID IN (SELECT PortfolioRunLDIExposureID FROM PortfolioRunLDIExposure WHERE PortfolioRunID = 1163804)
UNION
SELECT 'PortfolioRunLDIKeyRateDurationPoint' AS entityTableName, PortfolioRunLDIKeyRateDurationPointID AS entityID
FROM PortfolioRunLDIKeyRateDurationPoint
WHERE PortfolioRunID = 1163804
UNION
SELECT 'SystemColumn' AS entityTableName, SystemColumnID AS entityID
FROM SystemColumn
WHERE ColumnName = 'Portfolio Run LDI Target Contract Calculator'
UNION
SELECT 'SystemBean' AS entityTableName, SystemBeanID AS entityID
FROM SystemBean
WHERE SystemBeanName = 'Default Futures Only LDI Target Contract Calculator';
