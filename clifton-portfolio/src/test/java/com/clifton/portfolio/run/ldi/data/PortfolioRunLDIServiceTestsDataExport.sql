SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityID
FROM InvestmentSecurity
WHERE InvestmentSecurityID = 236
UNION
SELECT 'PortfolioRun' AS entityTableName, PortfolioRunID AS entityID
FROM PortfolioRun
WHERE PortfolioRunId = 1095306
UNION
SELECT 'MarketDataSource' AS entityTableName, MarketDataSourceID AS entityID
FROM MarketDataSource
WHERE DataSourceName = 'Goldman Sachs'
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID AS entityID
FROM MarketDataValue
WHERE InvestmentSecurityID = 120261
  AND MeasureDate < '10/10/2019'
UNION
SELECT 'MarketDataFieldGroup' AS entityTableName, MarketDataFieldGroupID AS entityID
FROM MarketDataFieldGroup
UNION
SELECT 'MarketDataFieldFieldGroup' AS entityTableName, MarketDataFieldFieldGroupID AS entityID
FROM MarketDataFieldFieldGroup
UNION
SELECT 'MarketDataPriceFieldMapping' AS entityTableName, MarketDataPriceFieldMappingID AS entityID
FROM MarketDataPriceFieldMapping
UNION
SELECT 'PortfolioRunLDIKeyRateDurationPoint' AS entityTableName, PortfolioRunLDIKeyRateDurationPointID AS entityID
FROM PortfolioRunLDIKeyRateDurationPoint
WHERE PortfolioRunID = 1095306
UNION
SELECT 'PortfolioRunLDIExposureKeyRateDurationPoint' AS entityTableName, PortfolioRunLDIExposureKeyRateDurationPointID AS entityID
FROM PortfolioRunLDIExposureKeyRateDurationPoint
WHERE PortfolioRunLDIExposureID IN
	  (SELECT PortfolioRunLDIExposureID
	   FROM PortfolioRunLDIExposure
	   WHERE PortfolioRunID = 1095306)
UNION
SELECT 'CalendarDay' AS entityTableName, CalendarDayID AS entityId
FROM CalendarDay
WHERE CalendarYearID = 2019
;
