package com.clifton.portfolio.run;

import com.clifton.business.service.BusinessService;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.account.InvestmentAccount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@ContextConfiguration(locations = "../PortfolioProjectBasicTests-context.xml")
@ExtendWith(SpringExtension.class)
public class PortfolioRunServiceTests {

	@Resource
	private PortfolioRunService portfolioRunService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidatePortfolioRunSupported() {
		PortfolioRun run = null;
		validatePortfolioRunSupportedImpl(run, "Portfolio Run is missing");

		run = new PortfolioRun();
		validatePortfolioRunSupportedImpl(run, "Client Account is required.");

		InvestmentAccount account = new InvestmentAccount();
		account.setNumber("123456");
		account.setName("Test Account");
		run.setClientInvestmentAccount(account);
		validatePortfolioRunSupportedImpl(run, "Client Account [123456: Test Account] is missing a service processing type selection.  Unable to create or process the run that doesn't have a processing type defined.");

		BusinessService service = new BusinessService();
		service.setName("Test");
		account.setBusinessService(service);
		validatePortfolioRunSupportedImpl(run, "Client Account [123456: Test Account] is missing a service processing type selection.  Unable to create or process the run that doesn't have a processing type defined.");

		BusinessServiceProcessingType serviceProcessingType = new BusinessServiceProcessingType();
		serviceProcessingType.setName("Test Service Processing Type");
		serviceProcessingType.setProcessingType(ServiceProcessingTypes.DURATION_MANAGEMENT);
		account.setServiceProcessingType(serviceProcessingType);
		validatePortfolioRunSupportedImpl(run, "Service Processing Type [Test Service Processing Type] does not support portfolio runs. Unable to create or process the run for 123456: Test Account.");

		serviceProcessingType.setProcessingType(ServiceProcessingTypes.LDI);
		validatePortfolioRunSupportedImpl(run, null);

		serviceProcessingType.setProcessingType(ServiceProcessingTypes.PORTFOLIO_RUNS);
		validatePortfolioRunSupportedImpl(run, null);
	}


	private void validatePortfolioRunSupportedImpl(PortfolioRun run, String expectedErrorMessage) {
		String errorMessage = null;
		try {
			this.portfolioRunService.validatePortfolioRunIsSupportedForClientAccount(run);
		}
		catch (Exception e) {
			errorMessage = ExceptionUtils.getOriginalMessage(e);
		}
		if (StringUtils.isEmpty(expectedErrorMessage)) {
			Assertions.assertTrue(StringUtils.isEmpty(errorMessage), "Did not expected an error, but received: " + errorMessage);
		}
		else {
			Assertions.assertEquals(expectedErrorMessage, errorMessage);
		}
	}
}
