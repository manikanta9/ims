package com.clifton.portfolio.run.manager;

import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * The <code>BasePortfolioRunManagerAccountTests</code> tests the calculated getters on the
 * {@link BasePortfolioRunManagerAccount} bean
 *
 * @author Mary Anderson
 */
public class BasePortfolioRunManagerAccountTests {

	//////////////////////////////////////////////////////////
	// Product Overlay Manager Account Calculated Getters
	//////////////////////////////////////////////////////////


	@Test
	public void testGetCashAllocationPercent() {
		BasePortfolioRunManagerAccount bean = new BasePortfolioRunManagerAccount();
		Assertions.assertNull(bean.getCashAllocationPercent());

		bean.setCashAllocation(BigDecimal.ZERO);
		bean.setSecuritiesAllocation(BigDecimal.ZERO);
		Assertions.assertEquals(BigDecimal.ZERO, bean.getCashAllocationPercent());

		bean.setCashAllocation(BigDecimal.valueOf(50));
		Assertions.assertEquals(BigDecimal.valueOf(100), MathUtils.round(bean.getCashAllocationPercent(), 0));

		bean.setSecuritiesAllocation(BigDecimal.valueOf(100));
		Assertions.assertEquals(BigDecimal.valueOf(33.33), MathUtils.round(bean.getCashAllocationPercent(), 2));

		bean.setCashAllocation(BigDecimal.valueOf(12345.12));
		bean.setSecuritiesAllocation(BigDecimal.valueOf(56789.45));
		Assertions.assertEquals(BigDecimal.valueOf(17.86), MathUtils.round(bean.getCashAllocationPercent(), 2));
	}


	@Test
	public void testGetTotalAllocation() {
		BasePortfolioRunManagerAccount bean = new BasePortfolioRunManagerAccount();
		Assertions.assertNull(bean.getTotalAllocation());

		bean.setCashAllocation(BigDecimal.ZERO);
		bean.setSecuritiesAllocation(BigDecimal.ZERO);
		Assertions.assertEquals(BigDecimal.ZERO, bean.getTotalAllocation());

		bean.setCashAllocation(BigDecimal.valueOf(50));
		Assertions.assertEquals(BigDecimal.valueOf(50), bean.getTotalAllocation());

		bean.setSecuritiesAllocation(MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		Assertions.assertEquals(BigDecimal.valueOf(150), bean.getTotalAllocation());

		bean.setCashAllocation(BigDecimal.valueOf(12345.10));
		bean.setSecuritiesAllocation(BigDecimal.valueOf(56789.45));
		Assertions.assertEquals(BigDecimal.valueOf(69134.55), bean.getTotalAllocation());
	}


	@Test
	public void testGetPreviousDayTotalAllocation() {
		BasePortfolioRunManagerAccount bean = new BasePortfolioRunManagerAccount();
		Assertions.assertNull(bean.getPreviousDayTotalAllocation());

		bean.setPreviousDayCashAllocation(BigDecimal.ZERO);
		bean.setPreviousDaySecuritiesAllocation(BigDecimal.ZERO);
		Assertions.assertEquals(BigDecimal.ZERO, bean.getPreviousDayTotalAllocation());

		bean.setPreviousDayCashAllocation(BigDecimal.valueOf(50));
		Assertions.assertEquals(BigDecimal.valueOf(50), bean.getPreviousDayTotalAllocation());

		bean.setPreviousDaySecuritiesAllocation(MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		Assertions.assertEquals(BigDecimal.valueOf(150), bean.getPreviousDayTotalAllocation());

		bean.setPreviousDayCashAllocation(BigDecimal.valueOf(12345.10));
		bean.setPreviousDaySecuritiesAllocation(BigDecimal.valueOf(56789.45));
		Assertions.assertEquals(BigDecimal.valueOf(69134.55), bean.getPreviousDayTotalAllocation());
	}


	@Test
	public void testGetPreviousMonthEndTotalAllocation() {
		BasePortfolioRunManagerAccount bean = new BasePortfolioRunManagerAccount();
		Assertions.assertNull(bean.getPreviousMonthEndTotalAllocation());

		bean.setPreviousMonthEndCashAllocation(BigDecimal.ZERO);
		bean.setPreviousMonthEndSecuritiesAllocation(BigDecimal.ZERO);
		Assertions.assertEquals(BigDecimal.ZERO, bean.getPreviousMonthEndTotalAllocation());

		bean.setPreviousMonthEndCashAllocation(BigDecimal.valueOf(50));
		Assertions.assertEquals(BigDecimal.valueOf(50), bean.getPreviousMonthEndTotalAllocation());

		bean.setPreviousMonthEndSecuritiesAllocation(MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		Assertions.assertEquals(BigDecimal.valueOf(150), bean.getPreviousMonthEndTotalAllocation());

		bean.setPreviousMonthEndCashAllocation(BigDecimal.valueOf(12345.10));
		bean.setPreviousMonthEndSecuritiesAllocation(BigDecimal.valueOf(56789.45));
		Assertions.assertEquals(BigDecimal.valueOf(69134.55), bean.getPreviousMonthEndTotalAllocation());
	}


	@Test
	public void testGetOneDayPercentChange() {
		BasePortfolioRunManagerAccount bean = new BasePortfolioRunManagerAccount();
		Assertions.assertNull(bean.getOneDayPercentChange());

		bean.setPreviousDayCashAllocation(BigDecimal.ZERO);
		bean.setPreviousDaySecuritiesAllocation(BigDecimal.ZERO);
		bean.setCashAllocation(BigDecimal.ZERO);
		bean.setSecuritiesAllocation(BigDecimal.ZERO);
		Assertions.assertEquals(BigDecimal.ZERO, bean.getOneDayPercentChange());

		bean.setCashAllocation(BigDecimal.valueOf(50));
		bean.setSecuritiesAllocation(BigDecimal.valueOf(50));
		Assertions.assertEquals(MathUtils.BIG_DECIMAL_ONE_HUNDRED, bean.getOneDayPercentChange());

		bean.setPreviousDayCashAllocation(BigDecimal.valueOf(50));
		bean.setPreviousDaySecuritiesAllocation(BigDecimal.ZERO);
		Assertions.assertEquals(MathUtils.BIG_DECIMAL_ONE_HUNDRED, MathUtils.round(bean.getOneDayPercentChange(), 0));

		bean.setCashAllocation(BigDecimal.valueOf(12345.10));
		bean.setSecuritiesAllocation(BigDecimal.valueOf(56789.45));
		bean.setPreviousDayCashAllocation(BigDecimal.valueOf(12340.00));
		bean.setPreviousDaySecuritiesAllocation(BigDecimal.valueOf(56780.45));
		Assertions.assertEquals(BigDecimal.valueOf(0.02), MathUtils.round(bean.getOneDayPercentChange(), 2));
	}


	@Test
	public void testGetMonthPercentChange() {
		BasePortfolioRunManagerAccount bean = new BasePortfolioRunManagerAccount();
		Assertions.assertNull(bean.getMonthPercentChange());

		bean.setPreviousMonthEndCashAllocation(BigDecimal.ZERO);
		bean.setPreviousMonthEndSecuritiesAllocation(BigDecimal.ZERO);
		bean.setCashAllocation(BigDecimal.ZERO);
		bean.setSecuritiesAllocation(BigDecimal.ZERO);
		Assertions.assertEquals(BigDecimal.ZERO, bean.getMonthPercentChange());

		bean.setCashAllocation(BigDecimal.valueOf(50));
		bean.setSecuritiesAllocation(BigDecimal.valueOf(50));
		Assertions.assertEquals(MathUtils.BIG_DECIMAL_ONE_HUNDRED, bean.getMonthPercentChange());

		bean.setPreviousMonthEndCashAllocation(BigDecimal.valueOf(50));
		bean.setPreviousMonthEndSecuritiesAllocation(BigDecimal.ZERO);
		Assertions.assertEquals(MathUtils.BIG_DECIMAL_ONE_HUNDRED, MathUtils.round(bean.getMonthPercentChange(), 0));

		bean.setCashAllocation(BigDecimal.valueOf(12345.12));
		bean.setSecuritiesAllocation(BigDecimal.valueOf(56789.45));
		bean.setPreviousMonthEndCashAllocation(BigDecimal.valueOf(12340.12));
		bean.setPreviousMonthEndSecuritiesAllocation(BigDecimal.valueOf(56780.45));
		Assertions.assertEquals(BigDecimal.valueOf(0.02), MathUtils.round(bean.getMonthPercentChange(), 2));
	}
}
