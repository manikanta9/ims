package com.clifton.portfolio.run.ldi.calculators;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIService;
import com.clifton.portfolio.run.ldi.trade.PortfolioRunTradeLDI;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


/**
 * A set of tests to verify the proper target contract calculations for a set of Future Securities for LDI Portfolio Runs.
 *
 * @author davidi
 */
@ContextConfiguration("../PortfolioRunLDIServiceTests-context.xml")
public class PortfolioRunLDITargetContractCalculatorTests extends BaseInMemoryDatabaseTests {

	private PortfolioRunLDITargetContractCalculator portfolioRunLDITargetContractCalculator;

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private PortfolioRunLDIService portfolioRunLDIService;

	@Resource
	private PortfolioRunService portfolioRunService;


	private PortfolioRunTradeLDI portfolioRunTradeLDI;

	////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	//@BeforeAll
	//public void Setup() {
	//	this.portfolioRunTradeLDI = getPortfolioRunTradeLDI(1163804);
	//	getCalculator().calculateLDIFutureTargetContracts(this.portfolioRunTradeLDI);
	//}

	// Test data
	// Portfolio Run ID: 1163804

	// Security ID's available for testing:
	// InvestmentSecurityID	InvestmentSecurityDescription	Symbol
	// 120262	US 5YR NOTE (CBT) Mar20	FVH0
	// 120261	US 2YR NOTE (CBT) Mar20	TUH0
	// 120263	US 10YR NOTE (CBT)Mar20	TYH0
	// 120265	US LONG BOND(CBT) Mar20	USH0
	// 120264	US 10yr Ultra Fut Mar20	UXYH0
	// 120266	US ULTRA BOND CBT Mar20	WNH0


	@Test
	public void testCalculateLDIFutureTargetContracts_FVH0() {
		this.portfolioRunTradeLDI = getPortfolioRunTradeLDI(1163804);
		getCalculator().calculateTargetContracts(this.portfolioRunTradeLDI);
		final BigDecimal expectedTargetContracts = BigDecimal.valueOf(666);
		Assertions.assertTrue(MathUtils.isEqual(expectedTargetContracts, getLDIExposureTargetContractsForSecurityId(this.portfolioRunTradeLDI, 120262)));
	}


	@Test
	public void testCalculateLDIFutureTargetContracts_TUH0() {
		this.portfolioRunTradeLDI = getPortfolioRunTradeLDI(1163804);
		getCalculator().calculateTargetContracts(this.portfolioRunTradeLDI);
		final BigDecimal expectedTargetContracts = BigDecimal.valueOf(295);
		Assertions.assertTrue(MathUtils.isEqual(expectedTargetContracts, getLDIExposureTargetContractsForSecurityId(this.portfolioRunTradeLDI, 120261)));
	}


	@Test
	public void testCalculateLDIFutureTargetContracts_TYH0() {
		this.portfolioRunTradeLDI = getPortfolioRunTradeLDI(1163804);
		getCalculator().calculateTargetContracts(this.portfolioRunTradeLDI);
		final BigDecimal expectedTargetContracts = BigDecimal.valueOf(549);
		Assertions.assertTrue(MathUtils.isEqual(expectedTargetContracts, getLDIExposureTargetContractsForSecurityId(this.portfolioRunTradeLDI, 120263)));
	}


	@Test
	public void testCalculateLDIFutureTargetContracts_USH0() {
		this.portfolioRunTradeLDI = getPortfolioRunTradeLDI(1163804);
		getCalculator().calculateTargetContracts(this.portfolioRunTradeLDI);
		final BigDecimal expectedTargetContracts = BigDecimal.valueOf(664);
		Assertions.assertTrue(MathUtils.isEqual(expectedTargetContracts, getLDIExposureTargetContractsForSecurityId(this.portfolioRunTradeLDI, 120265)));
	}


	@Test
	public void testCalculateLDIFutureTargetContracts_UXYH0() {
		this.portfolioRunTradeLDI = getPortfolioRunTradeLDI(1163804);
		getCalculator().calculateTargetContracts(this.portfolioRunTradeLDI);
		final BigDecimal expectedTargetContracts = BigDecimal.valueOf(877);
		Assertions.assertTrue(MathUtils.isEqual(expectedTargetContracts, getLDIExposureTargetContractsForSecurityId(this.portfolioRunTradeLDI, 120264)));
	}


	@Test
	public void testCalculateLDIFutureTargetContracts_WNH0() {
		this.portfolioRunTradeLDI = getPortfolioRunTradeLDI(1163804);
		getCalculator().calculateTargetContracts(this.portfolioRunTradeLDI);
		final BigDecimal expectedTargetContracts = BigDecimal.valueOf(-1107);
		Assertions.assertTrue(MathUtils.isEqual(expectedTargetContracts, getLDIExposureTargetContractsForSecurityId(this.portfolioRunTradeLDI, 120266)));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private PortfolioRunLDITargetContractCalculator getCalculator() {
		if (this.portfolioRunLDITargetContractCalculator == null) {
			SystemBean calculatorBean = this.systemBeanService.getSystemBeanByName("Default Futures Only LDI Target Contract Calculator");
			this.portfolioRunLDITargetContractCalculator = (PortfolioRunLDITargetContractCalculator) this.systemBeanService.getBeanInstance(calculatorBean);
		}

		return this.portfolioRunLDITargetContractCalculator;
	}


	private PortfolioRunTradeLDI getPortfolioRunTradeLDI(int runId) {
		PortfolioRun run = this.portfolioRunService.getPortfolioRun(runId);
		PortfolioRunTradeLDI portfolioRunTradeLDI = new PortfolioRunTradeLDI();
		BeanUtils.copyProperties(run, portfolioRunTradeLDI);
		List<PortfolioRunLDIExposure> exposureList = this.portfolioRunLDIService.getPortfolioRunLDIExposureListByRun(runId, true);
		portfolioRunTradeLDI.setDetailList(exposureList);
		return portfolioRunTradeLDI;
	}


	private BigDecimal getLDIExposureTargetContractsForSecurityId(PortfolioRunTradeLDI prun, int securityId) {
		List<PortfolioRunLDIExposure> exposureList = CollectionUtils.getFiltered(prun.getDetailList(), entry -> entry.getSecurity().getId() == securityId);
		AssertUtils.assertEquals(1, exposureList.size(), "Exposure entry count for security ID: " + securityId + "  must be 1, but is: " + exposureList.size());
		return exposureList.get(0).getTargetContracts();
	}
}

