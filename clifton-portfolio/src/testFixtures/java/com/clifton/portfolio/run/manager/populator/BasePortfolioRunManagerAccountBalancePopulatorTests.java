package com.clifton.portfolio.run.manager.populator;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.manager.InvestmentManagerAccountRollup;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceSearchForm;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalance;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author manderson
 */
public abstract class BasePortfolioRunManagerAccountBalancePopulatorTests {

	@Resource
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	@Resource
	private InvestmentManagerAccountService investmentManagerAccountService;
	@Resource
	private ReadOnlyDAO<InvestmentManagerAccountRollup> investmentManagerAccountRollupDAO;
	@Resource
	private UpdatableDAO<InvestmentManagerAccountBalance> investmentManagerAccountBalanceDAO;


	@BeforeEach
	public void setupTests() {
		Mockito.when(this.investmentManagerAccountService.getInvestmentManagerAccountRollupListByParent(ArgumentMatchers.anyInt())).thenAnswer(new Answer<List<InvestmentManagerAccountRollup>>() {
			@Override
			public List<InvestmentManagerAccountRollup> answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				int parentId = (Integer) args[0];
				return getInvestmentManagerAccountRollupDAO().findByField("referenceOne.id", parentId);
			}
		});

		// Initial Setup
		// Get all manager balances and re-save them so that adjusted values and moc values are recalculated
		InvestmentManagerAccountBalanceSearchForm searchForm = new InvestmentManagerAccountBalanceSearchForm();
		searchForm.setBalanceDate(DateUtils.toDate("05/01/2015"));
		List<InvestmentManagerAccountBalance> balanceList = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceList(searchForm);
		for (InvestmentManagerAccountBalance balance : CollectionUtils.getIterable(balanceList)) {
			balance = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(balance.getId());
			balance.recalculateAdjustedValues();
			this.investmentManagerAccountBalanceDAO.save(balance);
		}
	}


	protected void validatePortfolioRunManagerAccountBalanceList(String expected, List<PortfolioRunManagerAccountBalance> list) {
		StringBuilder sb = new StringBuilder(16);
		boolean header = true;
		for (PortfolioRunManagerAccountBalance balance : CollectionUtils.getIterable(list)) {
			sb.append(balance.toStringFormatted(header));
			header = false;
		}
		//System.out.println(sb.toString());
		Assertions.assertEquals(expected.replaceAll("\\s+", " ").trim(), sb.toString().replaceAll("\\s+", " ").trim());
	}


	public ReadOnlyDAO<InvestmentManagerAccountRollup> getInvestmentManagerAccountRollupDAO() {
		return this.investmentManagerAccountRollupDAO;
	}
}

