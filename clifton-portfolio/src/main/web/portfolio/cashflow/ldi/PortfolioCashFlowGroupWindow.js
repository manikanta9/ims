Clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Portfolio Cash Flow Group',
	iconCls: 'account',
	width: 1000,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'portfolioCashFlowGroup.json',
					instructions: 'The group used to represent the communication start and end date of a given cash flow stream.',
					labelWidth: 180,
					items: [
						{fieldLabel: 'Client Account', name: 'investmentAccount.label', hiddenName: 'investmentAccount.id', xtype: 'combo', url: 'investmentAccountListFind.json?businessServiceProcessingTypeName=LDI', displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow'},
						{
							fieldLabel: 'Currency', name: 'currencyInvestmentSecurity.name', hiddenName: 'currencyInvestmentSecurity.id', displayField: 'label', allowBlank: true,
							xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
							qtip: 'The currency to use for this flow. If left blank, IMS will use account\'s base currency security for this value when the group is saved.'
						},
						{
							fieldLabel: 'Group Type', name: 'cashFlowGroupType.text', hiddenName: 'cashFlowGroupType.id', displayField: 'text', tooltipField: 'tooltip', xtype: 'combo',
							url: 'systemListItemListFind.json?listName=Portfolio Cash Flow Group Type',
							qtip: 'The group type indicating whether or not this group gets rolled. The default value is defined in the account\'s portfolio setup.'
						},
						{fieldLabel: 'As Of Date', name: 'asOfDate', xtype: 'datefield', qtip: 'Used in conjunction with Balance Date when calculating rolled amounts.'},
						{fieldLabel: 'Effective Start Date', name: 'effectiveStartDate', xtype: 'datefield'},
						{fieldLabel: 'Effective End Date', name: 'effectiveEndDate', xtype: 'datefield'}
					]
				}]
			},
			{
				title: 'Group History',
				items: [{
					xtype: 'portfolio-cash-flow-group-history-gridpanel'
				}]
			},
			{
				title: 'Cash Flow History',
				items: [{
					xtype: 'portfolio-cash-flow-history-gridpanel',
					getCashFlowGroupId: function() {
						return this.getWindow().getMainFormId();
					},
					getCashFlowGroupHistoryId: function() {
						return '';
					}
				}]
			},
			{
				title: 'Calculated Cash Flow Liabilities',
				xtype: 'portfolio-run-ldi-manager-account-gridpanel',
				instructions: 'Manager Account key rate duration point allocations related to this Cash Flow Group, grouped by balance date of the corresponding Portfolio Run. If the related Portfolio Run has not been processed, the values can still be viewed using the Preview Modified KRD and DV01 tool.',
				wikiPage: '/IT/LDI+-+Liability+Driven+Investing',
				groupField: 'portfolioRun.balanceDate',
				groupTextTpl: '{values.text}',
				viewConfig: {forceFit: true, emptyText: 'This Cash Flow Group is not used by any Portfolio Runs. Manager Allocation records can be previewed using the Preview Modified KRD and DV01 tool.', deferEmptyText: false},
				columnOverrides: [
					{header: 'Run Balance Date', width: 18, type: 'date', dataIndex: 'portfolioRun.balanceDate', filter: {searchFieldName: 'runBalanceDate'}, defaultSortColumn: true, defaultSortDirection: 'DESC', hidden: true},
					{dataIndex: 'cashFlowGroupHistory.label', hidden: false},
					{dataIndex: 'benchmarkSecurity.name', hidden: true, viewNames: []},
					{dataIndex: 'krdValueDate', hidden: true, viewNames: []}
				],
				getLoadParams: function() {
					return {
						cashFlowGroupId: this.getWindow().getMainFormId(),
						populatePoints: true
					};
				},
				getTopToolbarFilters: function(toolBar) {
					const grid = this;
					toolBar.add({
						text: 'Preview Modified KRD and DV01',
						tooltip: 'Preview Cash Flow Liability Modified KRD Values for a selected Investment Account and Balance Date using the account\'s Managed KRD Point Subset',
						iconCls: 'view',
						handler: function() {
							const clientInvestmentAccountId = grid.getWindow().getMainForm().findField('investmentAccount.id').getValue();
							const clientInvestmentAccountLabel = grid.getWindow().getMainForm().findField('investmentAccount.label').getValue();
							const balanceDate = TCG.getValue('effectiveEndDate', grid.getWindow().getMainForm().formValues) || TCG.getValue('effectiveStartDate', grid.getWindow().getMainForm().formValues);
							const clazz = 'Clifton.portfolio.account.ldi.KeyRateDurationPreviewCashFlowLiabilitiesWindow';
							const cmpId = TCG.getComponentId(clazz, clientInvestmentAccountId);
							TCG.createComponent(clazz, {
								id: cmpId,
								defaultData: {clientInvestmentAccountId: clientInvestmentAccountId, clientInvestmentAccountLabel: clientInvestmentAccountLabel, balanceDate: TCG.parseDate(balanceDate).format('m/d/Y')}
							});
						}
					});
					toolBar.add('-');
				}
			}
		]
	}]
});
