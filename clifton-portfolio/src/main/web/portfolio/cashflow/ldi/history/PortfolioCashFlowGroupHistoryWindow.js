Clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Portfolio Cash Flow Group History',
	iconCls: 'account',
	width: 1000,
	height: 625,

	layout: 'border',
	items: [
		{
			xtype: 'formpanel',
			region: 'north',
			url: 'portfolioCashFlowGroupHistory.json',
			instructions: 'Shows the changes for the given Portfolio Cash Flow Group over time.',
			labelWidth: 180,
			readOnly: true,
			height: 175,
			items: [
				{fieldLabel: 'Cash Flow Group History Label', name: 'label', xtype: 'textfield', hidden: true},
				{fieldLabel: 'Cash Flow Group', name: 'cashFlowGroup.label', xtype: 'linkfield', detailIdField: 'cashFlowGroup.id', detailPageClass: 'Clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroupWindow'},
				{fieldLabel: 'Client Account', name: 'cashFlowGroup.investmentAccount.label', xtype: 'linkfield', detailIdField: 'cashFlowGroup.investmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow', hidden: true},
				{fieldLabel: 'Balance Start Date', name: 'startDate', xtype: 'datefield'},
				{fieldLabel: 'Balance End Date', name: 'endDate', xtype: 'datefield'},
				{fieldLabel: 'Discount Curve Security Security', name: 'discountCurveSecurity.label', detailIdField: 'discountCurveSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
				{fieldLabel: 'Discount Curve Value Date', name: 'discountCurveValueDate', xtype: 'datefield'}
			]
		},
		{
			xtype: 'tabpanel',
			region: 'center',
			items: [
				{
					title: 'Cash Flow History',
					xtype: 'portfolio-cash-flow-history-gridpanel',
					columnOverrides: [
						{dataIndex: 'cashFlowGroupHistory.label', hidden: true},
						{dataIndex: 'cashFlowGroupHistory.startDate', hidden: true},
						{dataIndex: 'cashFlowGroupHistory.endDate', hidden: true}
					],
					limitToLastMonthOfData: false
				},
				{
					title: 'History Summary',
					xtype: 'portfolio-cash-flow-group-history-summary-gridpanel',
					columnOverrides: [
						{dataIndex: 'cashFlowGroupHistory.label', hidden: true},
						{dataIndex: 'cashFlowGroupHistory.startDate', hidden: true},
						{dataIndex: 'cashFlowGroupHistory.endDate', hidden: true}
					],
					limitToLastMonthOfData: false
				},
				{
					title: 'Calculated Cash Flow Liabilities',
					xtype: 'portfolio-run-ldi-manager-account-gridpanel',
					instructions: 'Manager Account key rate duration point allocations related to this Cash Flow Group History, grouped by balance date of the corresponding Portfolio Run. If the related Portfolio Run has not been processed, the values can still be viewed using the Preview Modified KRD and DV01 tool.',
					wikiPage: '/IT/LDI+-+Liability+Driven+Investing',
					groupField: 'portfolioRun.balanceDate',
					groupTextTpl: '{values.text}',
					viewConfig: {forceFit: true, emptyText: 'This Cash Flow Group History is not used by any Portfolio Runs. Manager Allocation records can be previewed using the Preview Modified KRD and DV01 tool.', deferEmptyText: false},
					columnOverrides: [
						{header: 'Run Balance Date', width: 18, type: 'date', dataIndex: 'portfolioRun.balanceDate', filter: {searchFieldName: 'runBalanceDate'}, defaultSortColumn: true, defaultSortDirection: 'DESC', hidden: true},
						{dataIndex: 'benchmarkSecurity.name', hidden: true, viewNames: []},
						{dataIndex: 'krdValueDate', hidden: true, viewNames: []}
					],
					getLoadParams: function() {
						return {
							cashFlowGroupHistoryId: this.getWindow().getMainFormId(),
							populatePoints: true
						};
					},
					getTopToolbarFilters: function(toolBar) {
						const grid = this;
						toolBar.add({
							text: 'Preview Modified KRD and DV01',
							tooltip: 'Preview Cash Flow Liability Modified KRD Values for a selected Investment Account and Balance Date using the account\'s Managed KRD Point Subset.',
							iconCls: 'view',
							handler: function() {
								const clientInvestmentAccountId = grid.getWindow().getMainForm().findField('cashFlowGroup.investmentAccount.id').getValue();
								const clientInvestmentAccountLabel = grid.getWindow().getMainForm().findField('cashFlowGroup.investmentAccount.label').getValue();
								const balanceDate = TCG.getValue('endDate', grid.getWindow().getMainForm().formValues);
								const clazz = 'Clifton.portfolio.account.ldi.KeyRateDurationPreviewCashFlowLiabilitiesWindow';
								const cmpId = TCG.getComponentId(clazz, clientInvestmentAccountId);
								TCG.createComponent(clazz, {
									id: cmpId,
									defaultData: {clientInvestmentAccountId: clientInvestmentAccountId, clientInvestmentAccountLabel: clientInvestmentAccountLabel, balanceDate: TCG.parseDate(balanceDate).format('m/d/Y')}
								});
							}
						});
						toolBar.add('-');
					}
				}
			]
		}
	]
});
