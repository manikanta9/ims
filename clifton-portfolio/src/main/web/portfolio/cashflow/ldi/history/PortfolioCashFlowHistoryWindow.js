Clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Portfolio Cash Flow History',
	iconCls: 'account',
	width: 600,
	height: 450,

	items: [
		{
			xtype: 'formpanel',
			url: 'portfolioCashFlowHistory.json',
			instructions: 'Cash Flow History records hold present and future value data calculated using the Cash Flows of the associated Cash Flow Group and the Discount Curve specified on the client account.',
			readOnly: true,
			labelWidth: 180,
			items: [
				{fieldLabel: 'Cash Flow Group History', name: 'cashFlowGroupHistory.label', xtype: 'linkfield', detailPageClass: 'Clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistoryWindow', detailIdField: 'cashFlowGroupHistory.id'},
				{fieldLabel: 'Cash Flow Type', name: 'cashFlowType.label', xtype: 'linkfield', detailPageClass: 'Clifton.portfolio.cashflow.ldi.PortfolioCashFlowTypeWindow', detailIdField: 'cashFlowType.id'},
				{fieldLabel: 'Adjusted Year', name: 'adjustedYear', xtype: 'integerfield', disabled: true, qtip: 'Determines which Portfolio Cash Flows should be used to calculate the History Amount. See Starting Month Number for details.'},
				{fieldLabel: 'Starting Month Number', name: 'startingMonthNumber', xtype: 'integerfield', doNotSubmitValue: true, disabled: true, qtip: 'Calculated as the difference in months between Balance Date and Cash Flow Group As of Date plus one month when Adjusted Year = 1. Determines which Portfolio Cash Flows should be used to calculate the History Amount.'},
				{fieldLabel: 'Discount Month', name: 'discountMonth', xtype: 'integerfield', doNotSubmitValue: true, disabled: true, qtip: 'The discount month for the portfolio cash flow, which is automatically calculated by IMS as (AdjustedYear * 12) - 6.'},
				{fieldLabel: 'History Amount', name: 'cashFlowHistoryAmount', xtype: 'currencyfield', qtip: 'The rolled future value flow.'},
				{fieldLabel: 'Liability Discount Rate', xtype: 'floatfield', name: 'liabilityDiscountRate', qtip: 'The original market data value retrieved from the Discount Curve security on the client account.'},
				{fieldLabel: 'Liability History Amount', xtype: 'currencyfield', name: 'liabilityCashFlowHistoryAmount', qtip: 'The present value of the History Amount using the Liability Discount Rate. The sum of this column is the target for trading purposes.'},
				{fieldLabel: 'DV01 Discount Rate', xtype: 'floatfield', name: 'dv01DiscountRate', qtip: 'The Liability Discount Rate adjusted to the Periodicity defined on the client account. It is only used to derive DV01 values.'},
				{fieldLabel: 'DV01 History Amount', xtype: 'currencyfield', name: 'dv01CashFlowHistoryAmount', qtip: 'This value is used when deriving DV01.'},
				{fieldLabel: 'DV01', xtype: 'floatfield', name: 'dv01Value', qtip: 'DV01 is calculated by shocking the Discount Rate up and down by the Curve Shock Amount (bps) specified on the client account and measuring the sensitivity of the present value.'},
				{fieldLabel: 'KRD', xtype: 'floatfield', name: 'krdValue', qtip: 'DV01 / SUM(DV01 History Amount) * 10000'}
			]
		}]
});
