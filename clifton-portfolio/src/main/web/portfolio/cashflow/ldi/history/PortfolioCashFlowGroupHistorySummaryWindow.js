Clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistorySummaryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Portfolio Cash Flow Group History Summary',
	iconCls: 'account',
	width: 600,
	height: 450,

	items: [
		{
			xtype: 'formpanel',
			url: 'portfolioCashFlowGroupHistorySummary.json',
			instructions: 'The Cash Flow Group History Summary is a data summary of present value cash flow information. Data is consolidated using the data fields within the Key Rate Duration Points field group (the full curve) and is always populated if cash flows are present.',
			readOnly: true,
			labelWidth: 180,
			items: [
				{fieldLabel: 'Cash Flow Group History', name: 'cashFlowGroupHistory.label', xtype: 'linkfield', detailPageClass: 'Clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistoryWindow', detailIdField: 'cashFlowGroupHistory.id'},
				{fieldLabel: 'Investment Account', name: 'investmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'investmentAccount.id'},
				{fieldLabel: 'Market Data Field', name: 'marketDataField.label', xtype: 'linkfield', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow', detailIdField: 'marketDataField.id'},
				{fieldLabel: 'Total Liability Value', xtype: 'currencyfield', name: 'totalLiabilityValue'},
				{fieldLabel: 'Total DV01 Value', xtype: 'currencyfield', name: 'totalDv01Value'},
				{fieldLabel: 'Total KRD Value', xtype: 'floatfield', name: 'totalKrdValue'}
			]
		}]
});
