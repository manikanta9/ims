Clifton.portfolio.cashflow.ldi.PortfolioCashFlowTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Portfolio Cash Flow Type',
	iconCls: 'account',
	height: 400,
	width: 540,

	items: [{
		xtype: 'formpanel',
		url: 'portfolioCashFlowType.json',
		instructions: 'The type of Portfolio Cash Flow that may occur within a given cash flow stream.',
		labelWidth: 180,
		topToolbarSearchParameter: 'searchPattern',
		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Portfolio Cash Flow History Calculator', beanName: 'cashFlowHistoryCalculatorBean', xtype: 'system-bean-combo', groupName: 'Portfolio Cash Flow History Calculator'},
			{boxLabel: 'Cumulative', name: 'cumulative', xtype: 'checkbox'},
			{boxLabel: 'Projected Flow', name: 'projectedFlow', xtype: 'checkbox', mutuallyExclusiveFields: ['actualFlow', 'serviceCost'], qtip: 'Affects the calculation of Monthly Cash Flow Amounts.'},
			{boxLabel: 'Actual Flow', name: 'actualFlow', xtype: 'checkbox', mutuallyExclusiveFields: ['projectedFlow', 'serviceCost'], qtip: 'Affects the calculation of Monthly Cash Flow Amounts.'},
			{boxLabel: 'Service Cost', name: 'serviceCost', xtype: 'checkbox', mutuallyExclusiveFields: ['projectedFlow', 'actualFlow'], qtip: 'Affects the calculation of Monthly Cash Flow Amounts.'}
		]
	}]
});
