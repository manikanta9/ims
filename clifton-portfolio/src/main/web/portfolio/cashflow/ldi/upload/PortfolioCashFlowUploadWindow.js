Clifton.portfolio.cashflow.ldi.upload.PortfolioCashFlowUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'portfolioCashFlowUploadWindow',
	title: 'Portfolio Cash Flow Data Import',
	iconCls: 'import',
	height: 400,
	hideOKButton: true,

	tbar: [{
		text: 'Simple Sample File',
		iconCls: 'excel',
		tooltip: 'Download Simple Excel Sample File',
		handler: function() {
			TCG.openFile('portfolio/cashflow/ldi/upload/SimplePortfolioCashFlowUploadFileSample.xls');
		}
	},
		{
			text: 'Simple Multiple Sample File',
			iconCls: 'excel',
			tooltip: 'Download Excel Sample File with multiple Portfolio Cash Flow Type columns and a cashflow amount values for each column.',
			handler: function() {
				TCG.openFile('portfolio/cashflow/ldi/upload/SimplePortfolioCashFlowMultipleUploadFileSample.xls');
			}
		}],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Portfolio Cash Flow Uploads allow you to import portfolio cash flow data from files directly into the system.  Use the options below to customize how to upload your data.',
		labelWidth: 40,

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{fieldLabel: '', name: 'endActiveGroup', xtype: 'hidden'},
			{fieldLabel: '', name: 'overwriteExistingGroupData', xtype: 'hidden'},

			{
				xtype: 'radiogroup', fieldLabel: '', name: 'importMode', allowBlank: false, columns: [200, 300], submitValue: false, items: [
					{
						boxLabel: 'End Currently Active Group', name: 'importMode', xtype: 'radio', inputValue: true, checked: true,
						qtip: 'Close the currently active cash flow group for the account, then create a new cash flow group to hold the imported data.'
					},
					{
						boxLabel: 'Overwrite Existing Data', name: 'importMode', xtype: 'radio', inputValue: false,
						qtip: 'Remove all data within an existing cash flow group before adding the imported data to the group.'
					}
				],
				listeners: {
					change: function(control, selectedControl) {
						const fp = TCG.getParentFormPanel(control);
						fp.setFormValue('endActiveGroup', selectedControl.inputValue, false);
						fp.setFormValue('overwriteExistingGroupData', !selectedControl.inputValue, false);
					}
				}
			},
			{
				fieldLabel: '', boxLabel: 'Multiple: Apply Unmapped Columns as Separate Portfolio Cash Flow Types for Each Row', name: 'applyUnmappedColumnNamesAsSeparateCashFlowTypes', xtype: 'checkbox',
				qtip: 'Use this option to upload, for each row, multiple Cash Flow Amounts, with the column labels representing a cash flow type (see Simple Multiple Sample File).'
			},
			{
				xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
				items: [
					{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
				]
			}
		],

		getSaveURL: function() {
			return 'portfolioCashFlowUploadFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true';
		},

		getDefaultData: function() {
			return {
				endActiveGroup: true,
				overwriteExistingGroupData: false
			};
		}
	}]
});
