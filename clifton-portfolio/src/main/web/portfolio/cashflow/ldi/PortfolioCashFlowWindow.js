Clifton.portfolio.cashflow.ldi.PortfolioCashFlowWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Portfolio Cash Flow',
	iconCls: 'account',
	height: 350,
	width: 600,

	items: [
		{
			xtype: 'formpanel',
			url: 'portfolioCashFlow.json',
			instructions: 'The data representing a cash flow or liability for a Cash Flow Group and Cash Flow Type.',
			labelWidth: 180,
			items: [
				{
					fieldLabel: 'Cash Flow Type', name: 'cashFlowType.name', hiddenName: 'cashFlowType.id',
					xtype: 'combo', url: 'portfolioCashFlowTypeListFind.json', detailPageClass: 'Clifton.portfolio.cashflow.ldi.PortfolioCashFlowTypeWindow'
				},
				{
					fieldLabel: 'Cash Flow Group', name: 'cashFlowGroup.label', hiddenName: 'cashFlowGroup.id', displayField: 'label',
					xtype: 'combo', url: 'portfolioCashFlowGroupListFind.json?orderBy=effectiveStartDate:DESC', detailPageClass: 'Clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroupWindow',
					requestedProps: 'label',

					listeners: {
						beforeQuery: function(queryEvent) {
							const win = this.getWindow();
							queryEvent.combo.store.baseParams = {
								investmentAccountId: win.defaultData.investmentAccountId
							};
						}
					}
				},

				{fieldLabel: 'Cash Flow Year', name: 'cashFlowYear', xtype: 'numberfield'},
				{fieldLabel: 'Cash Flow Amount', name: 'cashFlowAmount', xtype: 'currencyfield'},
				{fieldLabel: 'Starting Month Number', name: 'startingMonthNumber', xtype: 'numberfield', readonly: true, doNotSubmitValue: true, disabled: true, qtip: 'The starting month number for the portfolio cash flow, which is automatically calculated by the IMS Service.'},
				{fieldLabel: 'Ending Month Number', name: 'endingMonthNumber', xtype: 'numberfield', readOnly: true, allowBlank: true, doNotSubmitValue: true, qtip: 'The starting ending number for the portfolio cash flow, which is automatically calculated by the IMS Service.'}
			]
		}]
});
