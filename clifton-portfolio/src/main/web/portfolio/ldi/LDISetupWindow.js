Clifton.portfolio.ldi.LDISetupWindow = Ext.extend(TCG.app.Window, {
	title: 'Portfolio LDI Setup',
	id: 'portfolioLDISetupWindow',
	iconCls: 'account',
	height: 800,
	width: 1500,

	items: [{
		xtype: 'tabpanel',

		items: [
			{
				title: 'Cash Flows',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				items: [
					{
						title: 'LDI Accounts',
						xtype: 'investment-account-grid',
						instructions: 'The following Investment Accounts are processed using LDI.',
						viewNames: undefined, // No Views
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('workflowState.name', 'Active', false, true);
							}

							const accountGroupCombo = TCG.getChildByName(this.getTopToolbar(), 'clientAccountGroup');
							const investmentAccountGroupId = accountGroupCombo.getValue();

							return {
								...TCG.grid.GridPanel.prototype.getLoadParams.apply(this, arguments),
								businessServiceProcessingTypeName: 'LDI',
								investmentAccountGroupId: investmentAccountGroupId
							};
						},
						getTopToolbarFilters: function(toolbar) {
							toolbar.add({
								text: 'Discount Curves',
								tooltip: 'Open the Benchmarks / Discount Curves hierarchy in the Investments window.',
								iconCls: 'stock-chart',
								handler: function() {
									TCG.createComponent('Clifton.investment.setup.InvestmentListWindow', {
										id: TCG.getComponentId('Clifton.investment.setup.InvestmentListWindow', id),
										defaultData: {defaultNodePath: 'Instruments/Benchmarks/Discount Curves'}
									});
								}
							});

							return [
								{fieldLabel: 'Client Account Group', name: 'clientAccountGroup', valueField: 'id', displayField: 'label', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
								{fieldLabel: 'Client', xtype: 'combo', name: 'businessClientId', displayField: 'label', linkedFilter: 'businessClient.name', width: 150, url: 'businessClientListFind.json?workflowStateName=Active'},
								{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
							];
						},
						getTopToolbarInitialLoadParams: () => ({}),
						getColumnOverrides: () => ([{dataIndex: 'businessService.name', hidden: true}]),
						switchToViewBeforeReload: () => true,
						listeners: {afterrender: panel => panel.setDefaultView('Simple View')},
						border: 1,
						flex: 1
					},

					{flex: 0.02},

					{
						title: 'Portfolio Cash Flow Groups',
						xtype: 'portfolio-cash-flow-group-gridpanel',
						border: 1,
						flex: 1,
						instructions: 'Portfolio cash flow groups for all LDI accounts. Editing an existing group will affect all cash flows within the group (e.g. changing the account reassigns all cash flows in the group to the selected account).',

						// Overrides for grid use in a non-account-specific context.
						editor: {
							detailPageClass: 'Clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroupWindow',
							deleteURL: 'portfolioCashFlowGroupDelete.json'
						},
						getLoadParams: function(firstLoad) {
							const params = {};
							const searchPattern = TCG.getChildByName(this.getTopToolbar(), this.topToolbarSearchParameter);

							if (TCG.isNotBlank(searchPattern.getValue())) {
								params[this.topToolbarSearchParameter] = searchPattern.getValue();
							}

							const accountGroupCombo = TCG.getChildByName(this.getTopToolbar(), 'clientAccountGroup');
							const investmentAccountGroupId = accountGroupCombo.getValue();
							params.investmentAccountGroupId = investmentAccountGroupId;

							return params;
						},
						getTopToolbarFilters() {
							return [
								{fieldLabel: 'Client Account Group', name: 'clientAccountGroup', valueField: 'id', displayField: 'label', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
								{fieldLabel: 'Client', xtype: 'combo', name: 'clientId', displayField: 'label', linkedFilter: 'investmentAccount.businessClient.name', width: 150, url: 'businessClientListFind.json'},
								{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
							];
						}
					}
				]
			},
			{
				title: 'Cash Flow Types',
				items: [{
					xtype: 'portfolio-cash-flow-type-gridpanel'
				}]
			},
			{
				title: 'Administration',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						labelWidth: 140,
						loadValidation: false, // using the form only to get background color/padding
						height: 230,
						buttonAlign: 'right',
						instructions: 'Use this section to (re)build Cash Flow History. Select criteria below to filter which Cash Flow Groups to (re)build.',
						items: [
							{fieldLabel: 'Investment Account', name: 'investmentAccountLabel', hiddenName: 'investmentAccountId', xtype: 'combo', url: 'investmentAccountListFind.json?businessServiceProcessingTypeName=LDI', displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow'},
							{
								xtype: 'columnpanel',
								columns: [
									{
										rows: [
											{fieldLabel: '', boxLabel: 'Rebuild Existing records (save new results only if different from existing)', name: 'rebuildExisting', xtype: 'checkbox', qtip: 'If false, will just skip existing records, else will attempt to rebuild, will only save new results if different from existing.'}
										],
										config: {columnWidth: 0.8}
									},
									{
										rows: [
											{fieldLabel: 'Start Balance Date', xtype: 'datefield', name: 'startBalanceDate', allowBlank: false, value: TCG.getPreviousWeekday()},
											{fieldLabel: 'End Balance Date', xtype: 'datefield', name: 'endBalanceDate', allowBlank: false, value: TCG.getPreviousWeekday()}
										],
										config: {columnWidth: 0.2, labelWidth: 140}
									}
								]
							}
						],

						buttons: [{
							text: 'Rebuild Cash Flow History',
							iconCls: 'run',
							width: 150,
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								form.submit(Ext.applyIf({
									url: 'portfolioCashFlowHistoryRebuild.json',
									waitMsg: 'Processing...',
									success: function(form, action) {
										Ext.Msg.alert('Processing Started', action.result.status.message, function() {
											const grid = owner.ownerCt.items.get(1);
											grid.reload.defer(300, grid);
										});
									}
								}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
							}
						}]
					},
					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'CASH-FLOW-HISTORY',
						instantRunner: true,
						instructions: 'The following Cash Flow Groups are being (re)processed right now.',
						title: 'Current Cash Flow Group Processing',
						flex: 1
					}
				]
			}
		]
	}]
});
