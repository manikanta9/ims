Ext.ns(
	'Clifton.portfolio',
	'Clifton.portfolio.account',
	'Clifton.portfolio.account.ldi',
	'Clifton.portfolio.cashflow.ldi',
	'Clifton.portfolio.cashflow.ldi.upload',
	'Clifton.portfolio.cashflow.ldi.history',
	'Clifton.portfolio.ldi',
	'Clifton.portfolio.run',
	'Clifton.portfolio.run.ldi',
	'Clifton.portfolio.run.ldi.trade',
	'Clifton.portfolio.run.trade',
	'Clifton.portfolio.run.trade.group',
	'Clifton.portfolio.run.trade.collateral',
	'Clifton.portfolio.run.replication'
);

Clifton.system.schema.SimpleUploadWindows['PortfolioCashFlow'] = 'Clifton.portfolio.cashflow.ldi.upload.PortfolioCashFlowUploadWindow';

Clifton.portfolio.run.getDefaultViewForAccount = function(account, columnName) {
	if (account && account.serviceProcessingType && account.serviceProcessingType.id) {
		const stId = account.serviceProcessingType.id;
		return Clifton.system.GetCustomColumnValue(stId, 'BusinessServiceProcessingType', 'Business Service Processing Type Custom Fields', columnName, true);
	}
	return undefined;
};

Clifton.portfolio.run.trade.calculateExposure = function(row, contractQuantity, contractValue, currency, additionalExposure) {
	let exp = contractQuantity * contractValue;
	if (TCG.isTrue(row.data.reverseExposureSign)) {
		exp = exp * -1;
	}

	if (TCG.isTrue(row.data['replicationType.currency']) && currency) {
		exp = exp + currency;
	}
	if (additionalExposure) {
		exp = exp + additionalExposure;
	}
	return exp;
};

Clifton.portfolio.run.PIOSRunsGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioRunListFind',
	xtype: 'gridpanel',
	instructions: 'The following Portfolio runs have been or are being processed.',
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Team', name: 'teamSecurityGroupId', width: 120, xtype: 'toolbar-combo', url: 'securityGroupTeamList.json', loadAll: true, linkedFilter: 'clientInvestmentAccount.teamSecurityGroup.name'},
			{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'investmentAccountGroupId', width: 180, url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow', disableAddNewItem: true},
			{fieldLabel: 'Client Acct', xtype: 'toolbar-combo', name: 'investmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientInvestmentAccount.label', displayField: 'label'}
		];
	},
	getLoadParams: async function(firstLoad) {
		const t = this.getTopToolbar();
		if (firstLoad) {
			// default balance date to previous business day
			const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
			this.setFilterValue('balanceDate', {'on': prevBD});

			// try to get trader's team: only if one
			const team = await Clifton.security.user.getUserTeam(this);
			if (TCG.isNotNull(team)) {
				TCG.getChildByName(t, 'teamSecurityGroupId').setValue({value: team.id, text: team.name});
				this.setFilterValue('clientInvestmentAccount.teamSecurityGroup.name', {value: team.id, text: team.name});
			}
		}
		const lp = {readUncommittedRequested: true};
		const v = TCG.getChildByName(t, 'investmentAccountGroupId').getValue();
		if (v) {
			lp.investmentAccountGroupId = v;
		}
		return lp;
	},
	columns: [
		{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
		{
			header: '', width: 12,
			sortable: false, filter: false,
			renderer: function(v, args, r) {
				return TCG.renderActionColumn('pdf', '', 'View Portfolio Report', 'report');
			}
		},
		{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', width: 12, tooltip: 'File Attachment(s)', exportHeader: 'File Attachment(s)', dataIndex: 'documentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'},
		{header: '<div class="www" style="WIDTH:16px">&nbsp;</div>', width: 12, tooltip: 'If checked, there is at least one APPROVED file associated with this run on the portal.', exportHeader: 'Posted', dataIndex: 'postedToPortal', type: 'boolean'},
		{header: 'Client Relationship', width: 250, dataIndex: 'clientInvestmentAccount.businessClient.clientRelationship.legalLabel', hidden: true, filter: {searchFieldName: 'clientRelationshipName'}},
		{header: 'Client', width: 250, dataIndex: 'clientInvestmentAccount.businessClient.name', hidden: true, filter: {searchFieldName: 'clientName'}},
		{
			header: 'Client Account', width: 250, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'},
			renderer: function(v, metaData, r) {
				const note = r.data['notes'];
				if (TCG.isNotBlank(note)) {
					const qtip = r.data['notes'];
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				return v;
			}
		},
		{header: 'Client Account Workflow State', width: 100, hidden: true, dataIndex: 'clientInvestmentAccount.workflowState.name', filter: {searchFieldName: 'investmentAccountWorkflowStateName'}},
		{
			header: 'Processing Type', dataIndex: 'serviceProcessingType', width: 70, hidden: true,
			filter: {
				type: 'combo', displayField: 'name', valueField: 'value', mode: 'local', searchFieldName: 'serviceProcessingType',
				store: new Ext.data.ArrayStore({
					fields: ['name', 'value', 'description'],
					data: Clifton.business.service.ServiceProcessingTypes
				})
			}
		},
		{header: 'Team', dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', width: 85, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}},
		{
			header: 'Franchise', width: 150, dataIndex: 'clientInvestmentAccount.businessService.franchiseName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?serviceLevelType=FRANCHISE', listWidth: 500}, hidden: true,
			tooltip: 'A franchise is the highest level of bucketing investment strategies (services).  The franchise is consistent with how the public website Our Approach Page is set up in bucketing all services into two categories.'
		},
		{
			header: 'Super Strategy', width: 150, dataIndex: 'clientInvestmentAccount.businessService.superStrategyName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?serviceLevelType=SUPER_STRATEGY', listWidth: 500}, hidden: true,
			tooltip: 'A super strategy consists of a roll up of similar services bucketed to be consistent with the public website Our Approach Page.'
		},
		{header: 'Service', width: 150, dataIndex: 'clientInvestmentAccount.coalesceBusinessServiceGroupName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true},
		{header: 'Notes', width: 100, dataIndex: 'notes', hidden: true},

		{header: 'Main', width: 35, dataIndex: 'mainRun', type: 'boolean'},
		{header: 'MOC', width: 35, dataIndex: 'marketOnCloseAdjustmentsIncluded', type: 'boolean'},
		{header: 'Workflow State', width: 100, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
		{header: 'Workflow Status', width: 95, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=PortfolioRun'}},
		{
			header: 'Violation Status', width: 125, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'},
			renderer: function(v, p, r) {
				return (Clifton.rule.violation.renderViolationStatus(v));
			}
		},
		{header: 'Balance Date', width: 82, dataIndex: 'balanceDate'},
		{
			header: 'Updated By', width: 70, dataIndex: 'updateUserId', filter: {type: 'combo', searchFieldName: 'updateUserId', url: 'securityUserListFind.json', displayField: 'label'},
			renderer: Clifton.security.renderSecurityUser,
			exportColumnValueConverter: 'securityUserColumnReversableConverter'
		},
		{header: 'Updated At', width: 65, dataIndex: 'updateDate', align: 'center', renderer: TCG.renderTime},
		{header: 'Portfolio (TPV)', hidden: true, width: 100, dataIndex: 'portfolioTotalValue', type: 'currency', useNull: true, negativeInRed: true},
		{header: 'Cash Total', hidden: true, width: 100, dataIndex: 'cashTotal', type: 'currency', useNull: true, negativeInRed: true},
		{header: 'Overlay Target Total', hidden: true, width: 100, dataIndex: 'overlayTargetTotal', type: 'currency', useNull: true, negativeInRed: true},
		{header: 'Overlay Exposure Total', hidden: true, width: 100, dataIndex: 'overlayExposureTotal', type: 'currency', useNull: true, negativeInRed: true},
		{header: 'Cash Target', hidden: true, width: 100, dataIndex: 'cashTarget', type: 'currency', useNull: true, negativeInRed: true},
		{header: 'Cash Exposure', hidden: true, width: 100, dataIndex: 'cashExposure', type: 'currency', useNull: true, negativeInRed: true},
		{header: 'Cash Target %', hidden: true, width: 100, dataIndex: 'cashTargetPercent', type: 'currency', useNull: true, negativeInRed: true, numberFormat: '0,000.00 %'},
		{
			header: 'Cash Exposure % - Without Mispricing', hidden: true, width: 120, dataIndex: 'cashExposurePercent', type: 'currency', useNull: true, negativeInRed: true, numberFormat: '0,000.00 %',
			tooltip: '<b>Note</b>: Does NOT include mispricing. Value is capped at +/-9,999.9999.',
			renderer: function(v, p, r) {
				// Anything greater than 10 bps color in green
				if (v > 0.10) {
					return TCG.renderAmount(v, true, '0,000.0000 %');
				}
				// otherwise only color negatives in red
				return TCG.renderAmount(v, false, '0,000.0000 %');
			}
		},
		{header: 'Mispricing Total', hidden: true, width: 100, dataIndex: 'mispricingTotal', type: 'currency', useNull: true, negativeInRed: true},
		{
			header: 'Cash Exp %', width: 110, dataIndex: 'cashExposureWithMispricingPercent', type: 'currency', useNull: true, negativeInRed: true, numberFormat: '0,000.00 %',
			tooltip: '<b>Note</b>: Cash Exposure may include mispricing if the account holds securities that apply the fair value adjustment and the account is not set up to exclude mispricing. Value is capped at +/-9,999.9999.',
			renderer: function(v, p, r) {
				const mispricingTotal = r.data.mispricingTotal;
				if (mispricingTotal && mispricingTotal !== 0) {
					let qtip = '<table>';
					qtip += '<tr><td>Cash Exposure % (Original)</td><td align="right">' + TCG.renderAmount(r.data.cashExposurePercent, true, '0,000.0000 %') + '</td></tr>';
					qtip += '<tr><td>Mispricing Total</td><td align="right">' + TCG.renderAmount(mispricingTotal, true, '0,000.00') + '</td></tr>';
					qtip += '</table>';
					p.attr = 'qtip=\'' + qtip + '\'';
				}

				// Anything greater than 10 bps color in green
				if (v > 0.10) {
					return TCG.renderAmount(v, true, '0,000.0000 %');
				}
				// otherwise only color negatives in red
				return TCG.renderAmount(v, false, '0,000.0000 %');
			}
		}
	],
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					const row = grid.store.data.items[rowIndex];
					const runId = row.json.id;
					if (eventName === 'report') {
						Clifton.portfolio.run.downloadPortfolioReport(runId, grid);
					}
				}
			}
		}
	},

	editor: {
		detailPageClass: 'Clifton.portfolio.run.RunWindow',
		getDefaultData: function(gridPanel) { // defaults investment account
			const t = gridPanel.getTopToolbar();
			const ia = TCG.getChildByName(t, 'investmentAccountId');
			if (ia.getValue() === '') {
				TCG.showError('Please first select an investment account for the new Portfolio Run you are creating.');
				return false;
			}
			const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
			return {
				clientInvestmentAccount: {id: ia.getValue(), label: ia.getRawValue()},
				balanceDate: prevBD.format('Y-m-d 00:00:00'),
				mainRun: true
			};
		},
		getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
			return undefined; // Not needed for Existing Runs
		},
		addEditButtons: function(t, gridPanel) {
			TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

			if (gridPanel.addAdditionalEditButtons) {
				gridPanel.addAdditionalEditButtons(t);
			}
		}
	}
});
Ext.reg('portfolio-runs-gridpanel', Clifton.portfolio.run.PIOSRunsGrid);

Clifton.investment.account.CliftonAccountPortfolioTabs['LDI'].push({
		title: 'KRD Points',
		layout: 'vbox',
		layoutConfig: {align: 'stretch'},

		items: [
			{
				title: 'Managed Points',
				xtype: 'portfolio-account-key-rate-duration-point-gridpanel',
				border: 1,
				flex: 1
			},

			{flex: 0.02},

			{
				title: 'Reportable Points',
				xtype: 'portfolio-account-key-rate-duration-point-gridpanel',
				instructions: 'The following Reportable Only key rate duration points have been assigned to this Investment Account. Reportable Only points allow the user to maintain a separate set of points than what is actually managed by Trading. These points are processed separately during run processing and will be used for reporting purposes if they exist.',
				border: 1,
				flex: 1,
				reportableOnly: true,
				getTopToolbarFilters: function(toolBar) {
					const grid = this;
					toolBar.add({
						text: 'Copy Managed Points',
						tooltip: 'Create Reportable Points for each of the Managed Points from this account (if they do not already exist as Reportable Only points for this account).',
						iconCls: 'account',
						handler: async () => {
							const accountId = grid.getWindow().getMainFormId();
							if ('yes' === await TCG.showConfirm('Copying the Managed Points for this account will create new Reportable Only points with matching properties (e.g. Target Hedge, Target Adjustment Type, etc.) unless the account already has a Reportable Only point with the corresponding Market Data Field Order. Would you like to proceed?', 'Copy Managed Points')) {
								// post request to unsubscribe
								TCG.data.getDataPromise('portfolioManagedAccountKeyRateDurationPointsCopy.json?clientInvestmentAccountId=' + accountId, grid, {
									waitTarget: grid,
									waitMsg: 'Copying Manged Points...'
								}).then(function() {
									grid.reload();
								});
							}
						}
					});
					toolBar.add('-');
				},
				editor: {
					detailPageClass: 'Clifton.portfolio.account.ldi.KeyRateDurationPointWindow',
					getDefaultData: function(gridPanel, row) { // defaults client for the detail page
						return {
							reportableOnly: true,
							clientInvestmentAccount: gridPanel.getWindow().getMainForm().formValues
						};
					}
				}
			}
		]
	}
);

Clifton.portfolio.PortfolioAccountKeyRateDurationPointGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioAccountKeyRateDurationPointListFind',
	instructions: 'The following key rate duration points have been assigned to this investment account.',
	wikiPage: 'IT/LDI+-+Liability+Driven+Investing',

	reportableOnly: false,
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('inactive', false);
		}
		return {
			reportableOnly: this.reportableOnly,
			requestedMaxDepth: 4,
			clientInvestmentAccountId: this.getWindow().getMainFormId()
		};
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'KRD Point', width: 150, dataIndex: 'marketDataField.name', filter: {searchFieldName: 'marketDataFieldName'}},
		{
			header: 'Months', width: 50, dataIndex: 'marketDataField.fieldOrder', hidden: true, filter: {searchFieldName: 'fieldOrder'}, defaultSortColumn: true, type: 'int',
			qtip: 'The number of months KRD Point represents.  Used for calculations when truncating KRD Points to client account selected subset.  This value is defined by the KRD Point Market Data Field Order value.'
		},
		{header: 'Target Hedge', width: 70, dataIndex: 'targetHedgePercent', type: 'percent'},
		{header: 'Target Adjustment Type', dataIndex: 'targetExposureAdjustmentType', width: 75},
		{header: 'Target Adjustment', dataIndex: 'targetExposureAmount', width: 75, type: 'currency', useNull: true, hidden: true},

		{header: 'Rebalance Min', width: 70, dataIndex: 'rebalanceMinPercent', type: 'percent', useNull: true},
		{header: 'Rebalance Max', width: 70, dataIndex: 'rebalanceMaxPercent', type: 'percent', useNull: true},
		{header: 'Inactive', dataIndex: 'inactive', width: 50, type: 'boolean'},
		{header: 'Reportable Only', dataIndex: 'reportableOnly', width: 50, type: 'boolean', disabled: true, filter: false, sortable: false, menuDisabled: true}
	],
	editor: {
		detailPageClass: 'Clifton.portfolio.account.ldi.KeyRateDurationPointWindow',
		getDefaultData: function(gridPanel) { // defaults client account for the detail page
			return {
				clientInvestmentAccount: gridPanel.getWindow().getMainForm().formValues
			};
		}
	},
	getTopToolbarFilters: function(toolBar) {
		const grid = this;
		toolBar.add({
			text: 'Portfolio Setup',
			tooltip: 'Enter Portfolio Setup Options',
			iconCls: 'account',
			handler: function() {
				const accountId = grid.getWindow().getMainFormId();
				Clifton.investment.account.openCliftonAccountCustomFieldWindow('Portfolio Setup', accountId, 'LDI');
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Preview Modified KRD',
			tooltip: 'Preview Modified KRD Values for a selected security and date using this account\'s Managed KRD Point Subset',
			iconCls: 'view',
			handler: function() {
				const accountId = grid.getWindow().getMainFormId();
				const clazz = 'Clifton.portfolio.account.ldi.KeyRateDurationPreviewSecurityWindow';
				const cmpId = TCG.getComponentId(clazz, accountId);
				TCG.createComponent(clazz, {
					id: cmpId,
					defaultData: {clientInvestmentAccountId: accountId}
				});
			}
		});
		toolBar.add('-');
	}
});
Ext.reg('portfolio-account-key-rate-duration-point-gridpanel', Clifton.portfolio.PortfolioAccountKeyRateDurationPointGridPanel);

Clifton.portfolio.PortfolioRunLDIKeyRateDurationPointGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioRunLDIKeyRateDurationPointListByRun',
	instructions: 'The following details the Key Rate Duration Point summary for the run.',
	appendStandardColumns: false,
	reportableOnly: false,
	getLoadParams: function() {
		return {
			runId: this.getWindow().getMainForm().formValues.id,
			reportableOnly: this.reportableOnly
		};
	},


	remoteSort: true,
	isPagingEnabled: function() {
		return false;
	},
	pageSize: 1000,
	wikiPage: 'IT/LDI+-+Liability+Driven+Investing',
	columns: [
		{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
		{header: 'KRD Point', width: 60, dataIndex: 'keyRateDurationPoint.marketDataField.name'},
		{header: 'Target % (Original)', width: 50, dataIndex: 'targetHedgePercent', hidden: true, type: 'percent', summaryType: 'percent', denominatorValueField: 'liabilitiesDv01Value', numeratorValueField: 'targetHedgeDv01Value', negativeInRed: true},
		{
			header: 'Target %', width: 50, dataIndex: 'targetHedgePercentAdjusted', type: 'percent', summaryType: 'percent', denominatorValueField: 'liabilitiesDv01Value', numeratorValueField: 'targetHedgeDv01ValueAdjusted', negativeInRed: true,
			renderer: function(v, metaData, r) {
				return TCG.renderAdjustedAmount(v, r.data['targetHedgePercent'] !== v, r.data['targetHedgePercent'], '0,000.00 %', '', false);
			}
		},
		{
			header: 'Liabilities DV01', width: 60, dataIndex: 'liabilitiesDv01Value', type: 'currency', numberFormat: '0,000', summaryType: 'sum',
			tooltip: 'Total Liabilities Manager DV01 Values'
		},
		{
			header: 'Target DV01 (Original)', width: 60, dataIndex: 'targetHedgeDv01Value', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hidden: true,
			tooltip: 'Target % of the Liabilities DV01 Value.'
		},
		{
			header: 'Target DV01', width: 60, dataIndex: 'targetHedgeDv01ValueAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum',
			tooltip: 'Target % of the Liabilities DV01 Value.',
			renderer: function(v, metaData, r) {
				return TCG.renderAdjustedAmount(v, r.data['targetHedgeDv01Value'] !== v, r.data['targetHedgeDv01Value'], '0,000', '', false);
			}
		},

		{
			header: 'Assets DV01', width: 60, dataIndex: 'assetsDv01Value', type: 'currency', numberFormat: '0,000', summaryType: 'sum',
			tooltip: 'Total Assets Manager DV01 Values'
		},
		{
			header: 'Exposure DV01', width: 60, dataIndex: 'exposureDv01Value', type: 'currency', numberFormat: '0,000', summaryType: 'sum',
			tooltip: 'Total Position Exposure DV01 Values'
		},
		{
			header: 'Total Assets DV01', width: 60, dataIndex: 'totalAssetsDv01Value', type: 'currency', numberFormat: '0,000', summaryType: 'sum',
			tooltip: 'Assets + Exposure DV01'
		},
		{
			header: 'Exposure Target DV01', width: 60, dataIndex: 'exposureTargetDv01Value', type: 'currency', numberFormat: '0,000', summaryType: 'sum',
			tooltip: 'Target DV01 - Assets DV01'
		},
		{
			header: 'Deviation From Target DV01', width: 70, dataIndex: 'totalAssetsDeviationFromTargetDv01Value', type: 'currency', numberFormat: '0,000', summaryType: 'sum',
			tooltip: 'Total Assets - Target DV01'
		},
		{
			header: 'Effective %', width: 50, dataIndex: 'effectiveHedgePercent', type: 'percent', summaryType: 'percent', denominatorValueField: 'liabilitiesDv01Value', numeratorValueField: 'totalAssetsDv01Value', negativeInRed: true,
			tooltip: 'Total Assets % of Liabilities'
		},
		{
			header: 'Deviation From Target %', width: 70, dataIndex: 'totalAssetsDeviationFromTargetPercent', type: 'percent', summaryType: 'percent', denominatorValueField: 'liabilitiesDv01Value', numeratorValueField: 'totalAssetsDeviationFromTargetDv01Value', negativeInRed: true,
			tooltip: 'Effective % - Target %'
		},
		{
			header: 'Rebalance Band', width: 60, dataIndex: 'rebalanceTriggerPercent', type: 'percent', useNull: true,
			tooltip: 'If overweight, displays Max Rebalance Band.  If underweight, displays Min Rebalance Band'
		}
	],
	plugins: {ptype: 'groupsummary'},
	editor: {
		detailPageClass: 'Clifton.portfolio.run.ldi.KeyRateDurationPointWindow',
		drillDownOnly: true
	}
});
Ext.reg('portfolio-run-ldi-key-rate-duration-point-gridpanel', Clifton.portfolio.PortfolioRunLDIKeyRateDurationPointGridPanel);

Clifton.investment.account.CliftonAccountPortfolioTabs['LDI'].push(
	{
		title: 'Cash Flows',
		layout: 'vbox',
		layoutConfig: {align: 'stretch'},

		items: [
			{
				title: 'Portfolio Cash Flows',
				xtype: 'portfolio-cash-flow-gridpanel',
				border: 1,
				flex: 1,
				instructions: 'Portfolio cash flows for this account.'
			},

			{flex: 0.02},

			{
				title: 'Portfolio Cash Flow Groups',
				xtype: 'portfolio-cash-flow-group-gridpanel',
				border: 1,
				flex: 1
			}
		]
	}
);

Clifton.portfolio.PortfolioCashFlowGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioCashFlowListFind',
	instructions: 'A list of portfolio cash flow entries for this account.',
	wikiPage: 'IT/LDI+-+Liability+Driven+Investing',
	topToolbarSearchParameter: 'searchPattern',
	importComponentName: 'Clifton.portfolio.cashflow.ldi.upload.PortfolioCashFlowUploadWindow',
	importTableName: 'PortfolioCashFlow',
	columns: [
		{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
		{header: 'Client Account', width: 40, dataIndex: 'cashFlowGroup.investmentAccount.label', filter: {searchFieldName: 'accountLabel'}, hidden: true},
		{header: 'Client Account Number', width: 18, dataIndex: 'cashFlowGroup.investmentAccount.number', filter: {searchFieldName: 'accountNumber'}, hidden: true},
		{header: 'Cash Flow Group', width: 40, dataIndex: 'cashFlowGroup.label', filter: {searchFieldName: 'groupLabel'}},
		{header: 'Cash Flow Type', width: 10, dataIndex: 'cashFlowType.name', filter: {searchFieldName: 'flowTypeName'}},
		{header: 'Currency Security', width: 8, dataIndex: 'cashFlowGroup.currencyInvestmentSecurity.symbol', filter: {searchFieldName: 'currencySymbol'}, hidden: true},
		{header: 'Cash Flow Amount', width: 12, type: 'currency', dataIndex: 'cashFlowAmount'},
		{header: 'Cash Flow Year', width: 8, type: 'int', dataIndex: 'cashFlowYear', doNotFormat: true},
		{header: 'Starting Month Number', width: 10, type: 'int', dataIndex: 'startingMonthNumber'},
		{header: 'Ending Month Number', width: 10, type: 'int', dataIndex: 'endingMonthNumber', filter: false, sortable: false}
	],
	editor: {
		detailPageClass: 'Clifton.portfolio.cashflow.ldi.PortfolioCashFlowWindow',
		deleteURL: 'portfolioCashFlowDelete.json',
		getDefaultData: function() {
			return {
				investmentAccountId: this.getWindow().getMainFormId()
			};
		}
	},
	getLoadParams: function(firstLoad) {
		const params = {};
		const searchPattern = TCG.getChildByName(this.getTopToolbar(), this.topToolbarSearchParameter);

		params['investmentAccountId'] = this.getWindow().getMainFormId();
		if (TCG.isNotBlank(searchPattern.getValue())) {
			params[this.topToolbarSearchParameter] = searchPattern.getValue();
		}
		return params;
	},
	listeners: {
		afterrender: function(gridPanel) {
			// when a row is deleted, we should refresh the grid to display updated calculated field values
			gridPanel.grid.store.addListener('remove', function(store, record) {
				gridPanel.reload();
			});
		}
	}
});
Ext.reg('portfolio-cash-flow-gridpanel', Clifton.portfolio.PortfolioCashFlowGridPanel);


Clifton.portfolio.PortfolioCashFlowHistoryGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioCashFlowHistoryListFind',
	instructions: 'Cash Flow History records hold present and future value data calculated using the Cash Flows of the associated Cash Flow Group and the Discount Curve specified on the client account.',
	wikiPage: '/IT/LDI+-+Liability+Driven+Investing',
	topToolbarSearchParameter: 'searchPattern',
	limitToLastMonthOfData: true,
	columns: [
		{header: 'ID', width: 10, dataIndex: 'id', hidden: true},
		{header: 'Balance Start Date', width: 20, dataIndex: 'cashFlowGroupHistory.startDate', filter: {searchFieldName: 'startDate'}},
		{header: 'Balance End Date', width: 20, dataIndex: 'cashFlowGroupHistory.endDate', filter: {searchFieldName: 'endDate'}},
		{header: 'Group History', width: 40, dataIndex: 'cashFlowGroupHistory.label', filter: {searchFieldName: 'cashFlowGroupHistoryId'}, hidden: true},
		{header: 'Adjusted Year', width: 20, dataIndex: 'adjustedYear', type: 'int', tooltip: 'Determines which Portfolio Cash Flows should be used to calculate the History Amount. See Starting Month Number for details.'},
		{header: 'Cash Flow Type', width: 20, dataIndex: 'cashFlowType.name', filter: {searchFieldName: 'cashFlowTypeName'}},
		{header: 'Starting Month Number', width: 30, dataIndex: 'startingMonthNumber', type: 'int', hidden: true, tooltip: 'Calculated as the difference in months between Balance Date and Cash Flow Group As of Date plus one month when Adjusted Year = 1. Determines which Portfolio Cash Flows should be used to calculate the History Amount.'},
		{header: 'Discount Month', width: 30, dataIndex: 'discountMonth', type: 'int', hidden: true, tooltip: 'The discount month for the portfolio cash flow, which is automatically calculated by IMS as (AdjustedYear * 12) - 6.'},
		{header: 'Liability Discount Rate', width: 20, type: 'float', dataIndex: 'liabilityDiscountRate', tooltip: 'The original market data value retrieved from the Discount Curve security on the client account.'},
		{header: 'DV01 Discount Rate', width: 20, type: 'float', dataIndex: 'dv01DiscountRate', numberFormat: '0,000.00000', tooltip: 'The Liability Discount Rate adjusted to the Periodicity defined on the client account. It is only used to derive DV01 values.'},
		{header: 'History Amount', width: 20, type: 'currency', dataIndex: 'cashFlowHistoryAmount', tooltip: 'The rolled future value flow.'},
		{header: 'Liability History Amount', width: 20, type: 'currency', dataIndex: 'liabilityCashFlowHistoryAmount', tooltip: 'The present value of the History Amount using the Liability Discount Rate. The sum of this column is the target for trading purposes.'},
		{header: 'DV01 History Amount', width: 20, type: 'currency', dataIndex: 'dv01CashFlowHistoryAmount', tooltip: 'The present value of the History Amount using the DV01 Discount Rate. This value is used when deriving DV01.'},
		{header: 'DV01', width: 20, type: 'float', dataIndex: 'dv01Value', tooltip: 'DV01 is calculated by shocking the Discount Rate up and down by the Curve Shock Amount (bps) specified on the client account and measuring the sensitivity of the present value.'},
		{header: 'KRD', width: 20, type: 'float', dataIndex: 'krdValue', tooltip: 'DV01 / SUM(DV01 History Amount) * 10000'}
	],
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistoryWindow',
		deleteURL: 'portfolioCashFlowHistoryDelete.json'
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.ds.sortInfo = null;

			// default to last 30 days of history
			if (this.limitToLastMonthOfData) {
				const dateV = new Date().add(Date.DAY, -30);
				this.setFilterValue('cashFlowGroupHistory.endDate', {'after': dateV});
			}
		}

		const params = {};

		if (!this.ds.sortInfo) {
			params.orderBy = 'endDate:desc#adjustedYear:asc';
		}

		params['cashFlowGroupId'] = this.getCashFlowGroupId();
		params['cashFlowGroupHistoryId'] = this.getCashFlowGroupHistoryId();

		const t = this.getTopToolbar();
		const df = TCG.getChildByName(t, 'activeOnDate');
		if (TCG.isNotBlank(df.getValue())) {
			params.activeOnDate = (df.getValue()).format('m/d/Y');
			// If active on date is set, clear the start/end filters
			this.clearFilter('cashFlowGroupHistory.endDate');
			this.clearFilter('cashFlowGroupHistory.startDate');
		}
		return params;
	},
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate', tooltip: 'Active On Date filtering includes Effective Start and End of the related Cash Flow Group. For example, record with Balance Start and End Date on 10/1 will not appear using Active On Date 10/1 if the Group was retroactively ended on 9/30.'});
		return filters;
	},
	getCashFlowGroupId: function() {
		return '';
	},
	getCashFlowGroupHistoryId: function() {
		return this.getWindow().getMainFormId();
	},
	addFirstToolbarButtons: function(toolBar, gridPanel) {
		toolBar.add({
			text: 'Clear History',
			tooltip: 'Clears entire history for the Cash Flow Group.  <b>Note:</b> Can only be performed if the Cash Flow History has NEVER been used.',
			iconCls: 'remove',
			handler: function() {
				Ext.Msg.confirm('Clear Cash Flow History', 'Are you sure you would like to clear <b>ALL</b> Cash Flow History for this Cash Flow Group?  Note: No history will be cleared if any record is being used by portfolio runs.', function(a) {
					if (TCG.isEqualsStrict(a, 'yes')) {
						const loader = new TCG.data.JsonLoader({
							waitTarget: gridPanel.ownerCt,
							waitMsg: 'Clearing History...',
							params: {
								cashFlowGroupId: gridPanel.getWindow().getMainFormId()
							},
							onLoad: function(record, conf) {
								gridPanel.reload();
							}
						});
						loader.load('portfolioCashFlowHistoryListClear.json');
					}
				});
			}
		});
		toolBar.add('-');
	}
});
Ext.reg('portfolio-cash-flow-history-gridpanel', Clifton.portfolio.PortfolioCashFlowHistoryGridPanel);


Clifton.portfolio.PortfolioCashFlowGroupGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioCashFlowGroupListFind',
	instructions: 'A list of portfolio cash flow entries for this account.',
	wikiPage: 'IT/LDI+-+Liability+Driven+Investing',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
		{header: 'Client', width: 150, dataIndex: 'investmentAccount.businessClient.name', filter: {type: 'combo', searchFieldName: 'clientId', displayField: 'name', url: 'businessClientListFind.json'}, hidden: true},
		{header: 'Client Account', width: 40, dataIndex: 'investmentAccount.name', filter: {searchFieldName: 'investmentAccountName'}},
		{header: 'Client Account Number', width: 18, dataIndex: 'investmentAccount.number', filter: {searchFieldName: 'investmentAccountNumber'}},
		{header: 'Currency Security', width: 18, dataIndex: 'currencyInvestmentSecurity.symbol', filter: {searchFieldName: 'currencySymbol'}},
		{header: 'Group Type', width: 20, dataIndex: 'cashFlowGroupType.value', filter: {searchFieldName: 'groupType'}},
		{header: 'As Of Date', width: 18, type: 'date', dataIndex: 'asOfDate', tooltip: 'Used in conjunction with Balance Date when calculating rolled amounts.'},
		{header: 'Effective Start Date', width: 18, type: 'date', dataIndex: 'effectiveStartDate'},
		{header: 'Effective End Date', width: 18, type: 'date', dataIndex: 'effectiveEndDate'}
	],
	editor: {
		detailPageClass: 'Clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroupWindow',
		deleteURL: 'portfolioCashFlowGroupDelete.json',
		getDefaultData: function(gridPanel, row) {
			if (row) {
				return {cashFlowGroupId: row.json.id};
			}

			const investmentAccount = this.getWindow().defaultData;
			const defaultCashFlowGroupTypeId = Clifton.system.GetCustomColumnValue(investmentAccount.id, 'InvestmentAccount', 'Portfolio Setup', 'Portfolio Cash Flow Group Type', false);
			const defaultCashFlowGroupType = gridPanel.cashFlowGroupListItemDictionary[defaultCashFlowGroupTypeId];
			return {
				investmentAccount: investmentAccount,
				currencyInvestmentSecurity: investmentAccount.baseCurrency,
				cashFlowGroupType: defaultCashFlowGroupType
			};
		}
	},
	getLoadParams: function(firstLoad) {
		const params = {};
		const searchPattern = TCG.getChildByName(this.getTopToolbar(), this.topToolbarSearchParameter);

		params['investmentAccountId'] = this.getWindow().getMainFormId();
		if (TCG.isNotBlank(searchPattern.getValue())) {
			params[this.topToolbarSearchParameter] = searchPattern.getValue();
		}
		return params;
	},
	cashFlowGroupListItemDictionary: {},
	initPortfolioCashFlowGroupTypeDict: async function() {
		const cacheKey = 'cashFlowGroupListItemCacheKey';
		const listItems = await TCG.data.getDataPromiseUsingCaching('systemListItemListFind.json?listName=Portfolio Cash Flow Group Type', this, cacheKey);
		listItems.forEach(entry => this.cashFlowGroupListItemDictionary[entry.id] = entry);
	},
	listeners: {
		beforerender: function() {
			this.initPortfolioCashFlowGroupTypeDict();
		}
	}
});
Ext.reg('portfolio-cash-flow-group-gridpanel', Clifton.portfolio.PortfolioCashFlowGroupGridPanel);


Clifton.portfolio.PortfolioCashFlowGroupHistoryGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioCashFlowGroupHistoryListFind',
	instructions: 'A list of the Cash Flow Group Histories for this group.',
	wikiPage: '/IT/LDI+-+Liability+Driven+Investing',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
		{header: 'Balance Start Date', width: 18, type: 'date', dataIndex: 'startDate'},
		{header: 'Balance End Date', width: 18, type: 'date', dataIndex: 'endDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Cash Flow Group', width: 45, dataIndex: 'cashFlowGroup.label', filter: {searchFieldName: 'searchPattern'}},
		{header: 'Discount Curve Security Security', width: 25, dataIndex: 'discountCurveSecurity.label', filter: {type: 'combo', searchFieldName: 'discountCurveSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json?type=Benchmarks&investmentTypeSubType=Discount Curves'}},
		{header: 'Discount Curve Value Date', width: 20, dataIndex: 'discountCurveValueDate', type: 'date'}
	],
	editor: {
		detailPageClass: 'Clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistoryWindow',
		drillDownOnly: true
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.ds.sortInfo = null;
		}

		const params = {};

		if (!this.ds.sortInfo) {
			params.orderBy = 'endDate:desc#updateDate:desc';
		}

		params['cashFlowGroupId'] = this.getCashFlowGroupId();

		const t = this.getTopToolbar();
		const df = TCG.getChildByName(t, 'activeOnDate');
		if (TCG.isNotBlank(df.getValue())) {
			params.activeOnDate = (df.getValue()).format('m/d/Y');
			// If active on date is set, clear the start/end filters
			this.clearFilter('endDate');
			this.clearFilter('startDate');
		}
		return params;
	},
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate', tooltip: 'Active On Date filtering includes Effective Start and End of the related Cash Flow Group. For example, record with Balance Start and End Date on 10/1 will not appear using Active On Date 10/1 if the Group was retroactively ended on 9/30.'});
		return filters;
	},
	getCashFlowGroupId: function() {
		return this.getWindow().getMainFormId();
	}
});
Ext.reg('portfolio-cash-flow-group-history-gridpanel', Clifton.portfolio.PortfolioCashFlowGroupHistoryGridPanel);

Clifton.portfolio.PortfolioCashFlowGroupHistorySummaryGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioCashFlowGroupHistorySummaryListFind',
	instructions: 'Cash Flow Group History Summaries are a data summary of present value cash flow information. Data is consolidated using the data fields within the Key Rate Duration Points field group (the full curve) and is always populated if cash flows are present.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
		{header: 'Balance Start Date', width: 18, type: 'date', dataIndex: 'cashFlowGroupHistory.startDate', filter: {searchFieldName: 'startDate'}},
		{header: 'Balance End Date', width: 18, type: 'date', dataIndex: 'cashFlowGroupHistory.endDate', filter: {searchFieldName: 'endDate'}},
		{header: 'Market Data Field', dataIndex: 'marketDataField.name'},
		{header: 'Field Order', dataIndex: 'marketDataField.fieldOrder', hidden: true},
		{header: 'Total Liability Value', type: 'currency', dataIndex: 'totalLiabilityValue'},
		{header: 'Total DV01 Value', type: 'currency', dataIndex: 'totalDv01Value'},
		{header: 'Total KRD Value', type: 'float', dataIndex: 'totalKrdValue'}
	],
	editor: {
		detailPageClass: 'Clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistorySummaryWindow',
		drillDownOnly: true
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.ds.sortInfo = null;
		}

		const params = {};

		if (!this.ds.sortInfo) {
			params.orderBy = 'endDate:desc#fieldOrder:asc';
		}

		params['cashFlowGroupHistoryId'] = this.getCashFlowGroupHistoryId();

		const t = this.getTopToolbar();
		const df = TCG.getChildByName(t, 'activeOnDate');
		if (TCG.isNotBlank(df.getValue())) {
			params.activeOnDate = (df.getValue()).format('m/d/Y');
			// If active on date is set, clear the start/end filters
			this.clearFilter('endDate');
			this.clearFilter('startDate');
		}
		return params;
	},
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate', tooltip: 'Active On Date filtering includes Effective Start and End of the related Cash Flow Group. For example, record with Balance Start and End Date on 10/1 will not appear using Active On Date 10/1 if the Group was retroactively ended on 9/30.'});
		return filters;
	},
	getCashFlowGroupHistoryId: function() {
		return this.getWindow().getMainFormId();
	}
});
Ext.reg('portfolio-cash-flow-group-history-summary-gridpanel', Clifton.portfolio.PortfolioCashFlowGroupHistorySummaryGridPanel);

Clifton.portfolio.PortfolioCashFlowTypeGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioCashFlowTypeListFind',
	instructions: 'A list of portfolio cash flow types used to identify the type of a cash flows.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 10, dataIndex: 'id', hidden: true},
		{header: 'Type Name', width: 20, dataIndex: 'name'},
		{header: 'Type Description', width: 40, dataIndex: 'description'},
		{header: 'Cumulative', width: 15, type: 'boolean', dataIndex: 'cumulative'},
		{header: 'Projected Flow', width: 15, type: 'boolean', dataIndex: 'projectedFlow'},
		{header: 'Actual Flow', width: 15, type: 'boolean', dataIndex: 'actualFlow'},
		{header: 'Service Cost', width: 15, type: 'boolean', dataIndex: 'serviceCost'}
	],
	editor: {
		detailPageClass: 'Clifton.portfolio.cashflow.ldi.PortfolioCashFlowTypeWindow',
		deleteURL: 'portfolioCashFlowTypeDelete.json'
	}
});
Ext.reg('portfolio-cash-flow-type-gridpanel', Clifton.portfolio.PortfolioCashFlowTypeGridPanel);


Clifton.portfolio.run.PortfolioRunWindowOverrides = {};
Clifton.portfolio.run.PortfolioRunWindowOverrides['LDI'] = 'Clifton.portfolio.run.ldi.RunLDIWindow';

Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides = {};
Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides['LDI'] = {
	className: 'Clifton.portfolio.run.ldi.trade.RunTradeLDIWindow',
	getEntity: function(config, entity) {
		// Entity Request Properties are dynamic based on the run (which KRD Points they use)
		// Do not retrieve the entity here and allow the window to handle it
		if (entity.defaultView) {
			config.defaultView = entity.defaultView;
		}
		return undefined;
	}
};


Clifton.portfolio.run.getDefaultViewForAccount = function(account, columnName) {
	if (account && account.serviceProcessingType) {
		const stId = account.serviceProcessingType.id;
		return Clifton.system.GetCustomColumnValue(stId, 'BusinessServiceProcessingType', 'Business Service Processing Type Custom Fields', columnName, true);
	}
	return undefined;
};

Clifton.portfolio.run.downloadPortfolioReport = function(runId, componentScope, regenerate, format, excludePrivate) {
	let url = 'portfolioRunReportDownload.json?runId=' + runId;
	if (format) {
		url += '&format=' + format;
	}
	if (excludePrivate === true) {
		url += '&excludePrivate=' + excludePrivate;
	}
	if (regenerate) {
		url += '&regenerateCache=' + regenerate;
	}
	TCG.downloadFile(url, null, componentScope);
};

Clifton.portfolio.PortfolioRunLDIManagerAccountGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioRunLDIManagerAccountListFind',
	additionalPropertiesToRequest: 'keyRateDurationPointList|keyRateDurationPointMap',
	instructions: 'Selected client account has the following manager account key rate duration point allocations.',
	appendStandardColumns: false,
	viewConfig: {forceFit: true, emptyText: '', deferEmptyText: false},

	getTopToolbarFilters: function(toolbar) {
		return [
			'-',
			{boxLabel: 'Reportable Only', xtype: 'toolbar-checkbox', name: 'reportableOnlyToolbarCheckbox'}
		];
	},

	getLoadParams: function() {
		const reportableOnly = TCG.isTrue(TCG.getChildByName(this.getTopToolbar(), 'reportableOnlyToolbarCheckbox').getValue());
		this.grid.getView().emptyText = reportableOnly ? 'This Portfolio Run does not have Manager Allocation records with Reportable points different than the Managed points.' : '';
		this.grid.getView().applyEmptyText();

		return {
			runId: this.getWindow().getMainForm().formValues.id,
			populatePoints: true,
			reportableOnly: TCG.isTrue(TCG.getChildByName(this.getTopToolbar(), 'reportableOnlyToolbarCheckbox').getValue())
		};
	},

	groupField: 'cashType',
	groupTextTpl: '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Managers" : "Manager"]})',
	remoteSort: true,
	isPagingEnabled: function() {
		return false;
	},
	pageSize: 1000,

	viewNames: ['DV01 Values', 'KRD Values'],

	columns: [
		{header: 'Run Manager ID', width: 30, dataIndex: 'id', hidden: true},
		{header: 'Manager ID', width: 30, dataIndex: 'managerAccountAssignment.referenceOne.id', hidden: true},
		{header: 'Type', width: 75, hidden: true, dataIndex: 'cashType'},
		{header: 'Manager', width: 200, dataIndex: 'label', allViews: true, filter: false, sortable: false},
		{header: 'Cash Flow Group History', width: 150, dataIndex: 'cashFlowGroupHistory.label', filter: {searchFieldName: 'cashFlowGroupHistoryId'}, hidden: true},
		{header: 'Account', width: 75, dataIndex: 'managerAccountAssignment.referenceOne.accountNumber', hidden: true},
		{header: 'Benchmark', width: 150, dataIndex: 'benchmarkSecurity.name', allViews: true},
		{header: 'Securities', width: 100, hidden: true, dataIndex: 'securitiesTotal', type: 'currency', numberFormat: '0,000', negativeInRed: true},
		{header: 'Cash', width: 100, hidden: true, dataIndex: 'cashTotal', type: 'currency', numberFormat: '0,000', negativeInRed: true},
		{header: 'Total Market Value', width: 100, dataIndex: 'totalValue', type: 'currency', numberFormat: '0,000', negativeInRed: true, allViews: true, filter: false, sortable: false},
		{header: 'KRD Date', width: 100, dataIndex: 'krdValueDate', allViews: true}
	],
	plugins: {ptype: 'groupsummary'},
	editor: {
		detailPageClass: 'Clifton.portfolio.run.ldi.ManagerAccountWindow',
		drillDownOnly: true,

		// Used to Open the Manager Balance Window Directly, or the Manager Account - Balance History Tab
		openWindowFromContextMenu: function(grid, rowIndex, screen) {
			const gridPanel = this.getGridPanel();
			const row = grid.store.data.items[rowIndex];
			let clazz;
			let id;
			let defaultActiveTabName = undefined;

			// Manager Balance Window
			const mgrId = row.json.managerAccountAssignment.referenceOne.id;

			if (screen === 'MANAGER_BALANCE') {
				const dateV = TCG.parseDate(TCG.getValue('balanceDate', this.getWindow().getMainForm().formValues));
				id = TCG.data.getData('investmentManagerAccountBalanceByManagerAndDate.json?managerAccountId=' + mgrId + '&balanceDate=' + dateV.dateFormat('m/d/Y'), gridPanel).id;
				clazz = 'Clifton.investment.manager.BalanceWindow';
			}
			else {
				id = mgrId;
				clazz = 'Clifton.investment.manager.AccountWindow';
				defaultActiveTabName = 'Balance History';
			}
			if (clazz) {
				this.openDetailPage(clazz, gridPanel, id, row, null, defaultActiveTabName);
			}
		}
	},

	pointIdSet: null,
	updateDynamicColumns: function() {
		const krdPointCols = [];
		const dv01PointCols = [];
		let pointIds = this.pointIdSet;
		const currentLoadPointIdSet = new Set();

		const viewName = this.currentViewName || this.defaultViewName || this.viewNames[0];
		const grid = this.grid;
		grid.store.each(ldiManagerAccount => {
			try {
				ldiManagerAccount.beginEdit();
				Ext.each(ldiManagerAccount.json.keyRateDurationPointList, point => {
					currentLoadPointIdSet.add(point.keyRateDurationPoint.id);
					if (!pointIds.has(point.keyRateDurationPoint.id)) {
						pointIds.add(point.keyRateDurationPoint.id);
						const fieldName = point.keyRateDurationPoint.marketDataField.name;
						const dv01Name = point.keyRateDurationPoint.keyRateDurationPointLabel + ' DV01';
						krdPointCols.push({header: fieldName, width: 100, dataIndex: 'keyRateDurationPointMap.KRD_' + point.keyRateDurationPoint.id, type: 'currency', numberFormat: '0,000.000000', viewNames: ['KRD Values'], hidden: viewName !== 'KRD Values', align: 'right'});
						dv01PointCols.push({
							header: dv01Name, width: 100, dataIndex: 'keyRateDurationPointMap.DV01_' + point.keyRateDurationPoint.id, type: 'currency', summaryType: 'sum', viewNames: ['DV01 Values'], hidden: viewName !== 'DV01 Values', align: 'right',
							renderer: (value, metadata, record) => Ext.util.Format.number(value, '0,000')
						});
					}
					ldiManagerAccount.set('keyRateDurationPointMap.KRD_' + point.keyRateDurationPoint.id, point.krdValue || 0);
					ldiManagerAccount.set('keyRateDurationPointMap.DV01_' + point.keyRateDurationPoint.id, point.dv01Value || 0);
				});
			}
			catch (e) {
				console.warn(e);
			}
			finally {
				ldiManagerAccount.commit();
			}
		});

		if (currentLoadPointIdSet.size < 1) {
			grid.getStore().removeAll(); // show emptyText message
		}

		pointIds.forEach(pointId => {
			if (!currentLoadPointIdSet.has(pointId)) {
				grid.removeColumn('keyRateDurationPointMap.KRD_' + pointId);
				grid.removeColumn('keyRateDurationPointMap.DV01_' + pointId);
			}
		});
		grid.removeColumn('krdTotal');
		grid.removeColumn('dv01Total');
		pointIds = currentLoadPointIdSet;
		this.pointIdSet = currentLoadPointIdSet;

		krdPointCols.push({
			header: 'KRD Total', dataIndex: 'krdTotal', width: 100, type: 'currency', numberFormat: '0,000.000000', viewNames: ['DV01 Values', 'KRD Values'], align: 'right',
			renderer: function(v, metaData, r) {
				let total = 0;
				pointIds.forEach(pointId => {
					total += r.data['keyRateDurationPointMap.KRD_' + pointId];
				});
				return TCG.renderAmount(total, false, '0,000.000000');
			}
		});
		dv01PointCols.push({
			header: 'DV01 Total', dataIndex: 'dv01Total', width: 100, type: 'currency', viewNames: ['DV01 Values'], summaryType: 'sum', numberFormat: '0,000', hidden: viewName !== 'DV01 Values', align: 'right',
			renderer: function(v, metaData, r) {
				let total = 0;
				pointIds.forEach(pointId => {
					total += r.data['keyRateDurationPointMap.DV01_' + pointId];
				});
				return TCG.renderAmount(total, false, '0,000');
			}
		});

		Ext.each(krdPointCols, function(krdPointColumn) {
			grid.addColumn(krdPointColumn);
		});
		Ext.each(dv01PointCols, function(dv01PointColumn) {
			grid.addColumn(dv01PointColumn);
		});
	},

	getGridRowContextMenuItems: function(grid, rowIndex, record) {
		return record.json && record.json.managerAccountAssignment ? [
			{
				text: 'Open Manager Balance', iconCls: 'manager', handler: function() {
					grid.ownerGridPanel.editor.openWindowFromContextMenu(grid, rowIndex, 'MANAGER_BALANCE');
				}
			},
			{
				text: 'Open Manager Balance History', iconCls: 'grid', handler: function() {
					grid.ownerGridPanel.editor.openWindowFromContextMenu(grid, rowIndex, 'MANAGER_BALANCE_HISTORY');
				}
			}
		] : [];
	},

	listeners: {
		afterrender: function(gridPanel) {
			this.pointIdSet = new Set();
			this.grid.store.on('load', () => this.updateDynamicColumns());
		}
	}
});
Ext.reg('portfolio-run-ldi-manager-account-gridpanel', Clifton.portfolio.PortfolioRunLDIManagerAccountGridPanel);

Clifton.portfolio.run.replication.ReplicationFormPanel = Ext.extend(TCG.form.FormPanel, {
	loadValidation: false,
	labelWidth: 140,
	defaults: {anchor: '0'},
	url: 'OVERRIDE_ME',
	warningTypeLabel: 'replication',

	initComponent: function() {
		if (TCG.isNull(this.tbar) && this.addToolbarButtons) {
			Ext.apply(this, {tbar: new Ext.Toolbar()});
		}
		this.items = [];
		const runProcessingItems = this.getRunProcessingTypeItems();
		if (runProcessingItems && runProcessingItems.length > 0) {
			this.items.push(...runProcessingItems);
		}
		if (this.commonItems && this.commonItems.length > 0) {
			this.items.push(...this.commonItems);
		}
		Clifton.portfolio.run.replication.ReplicationFormPanel.superclass.initComponent.call(this);
	},

	getWarningMessage: function(form) {
		let msg = undefined;
		if (!form.formValues) {
			return;
		}
		if (TCG.isTrue(form.formValues.tradeEntryDisabled)) {
			msg = `Trade Entry is Disabled for this ${this.warningTypeLabel}.  All security changes or price entries must be made on a replication where trade entry is not disabled and it will be automatically updated here.`;
		}
		return msg;
	},

	listeners: {
		afterload: function(panel) {
			// If trading is disabled - disable security, tradeSecurityPrice, and tradeUnderlyingSecurityPrice
			const f = panel.getForm();
			const ro = TCG.getValue('tradeEntryDisabled', f.formValues);
			if (TCG.isTrue(ro)) {
				// Get security.label field, tradeSecurityPrice field, & tradeUnderlyingSecurityPrice field & disable them
				let field = f.findField('security.label');
				field.setDisabled(true);
				field = f.findField('tradeSecurityPrice');
				field.setDisabled(true);
				field = f.findField('tradeUnderlyingSecurityPrice');
				field.setDisabled(true);
			}
			// Otherwise, if no underlying security, then no underlying security price
			else {
				const us = TCG.getValue('underlyingSecurity.label', f.formValues);
				if (TCG.isBlank(us)) {
					const underlying = TCG.getChildByName(this.ownerCt, 'underlyingSecurityFieldset');
					underlying.setVisible(false);
				}
			}

			if (TCG.getValue('replicationType.currency', f.formValues) !== true) {
				const ccy = TCG.getChildByName(this.ownerCt, 'currencyFieldset');
				ccy.setVisible(false);
			}

			// Replication Specific Fields - Remove if No Value
			panel.showHideField(f, 'interestRate');
			panel.showHideField(f, 'duration');
			panel.showHideField(f, 'syntheticAdjustmentFactor');
			panel.showHideField(f, 'indexRatio');
			panel.showHideField(f, 'factor');
			panel.showHideField(f, 'delta');
			panel.showHideField(f, 'daysToMaturity');
			panel.showHideField(f, 'securityDirtyPrice');

			panel.processFormInfoMessage(f);
		}
	},

	showHideField: function(form, fieldName) {
		const field = form.findField(fieldName);

		if (field) {
			const val = TCG.getValue(fieldName, form.formValues);
			if (TCG.isBlank(val)) {
				field.setVisible(false);
			}
			else {
				field.setVisible(true);
			}
		}
	},

	processFormInfoMessage: function(form) {
		let message = void 0;

		const factorField = form.findField('factor');
		if (factorField && factorField.isVisible()) {
			if (TCG.isFalse(TCG.getValue('replicationType.applyCurrentFactor', form.formValues))) {
				message = 'The Replication has a Factor that is not included in the Contract Value since the Replication Type is not configured to Apply Factor';
			}
		}

		if (TCG.isNotBlank(message)) {
			this.updateInfoMessage(message);
		}
	},

	updateInfoMessage: function(msg) {
		this.infoMsg = msg;
		const infoId = this.id + '-info';
		const info = Ext.get(infoId);
		if (TCG.isNotNull(info)) {
			this.remove(infoId);
			Ext.destroy(info);
			this.doLayout();
		}
		if (TCG.isNotNull(msg)) {
			this.insert(0, {id: infoId, xtype: 'container', layout: 'hbox', autoEl: 'div', cls: 'info-msg', items: {xtype: 'label', html: msg}});
			this.doLayout();
		}
	},

	getRunProcessingTypeItems: function() {
		// Override for run specific form fields
		return [];
	},

	commonItems: [
		{fieldLabel: 'Replication', name: 'replication.name', xtype: 'linkfield', detailIdField: 'replication.id', detailPageClass: 'Clifton.investment.replication.ReplicationWindow', submitValue: false},

		{
			xtype: 'panel',
			layout: 'column',
			items: [{
				columnWidth: .48,
				items: [{
					xtype: 'fieldset',
					title: 'Security',
					items: [
						{name: 'tradeEntryDisabled', xtype: 'hidden'},
						{fieldLabel: 'Instrument', name: 'security.instrument.id', xtype: 'hidden'},
						{
							fieldLabel: 'Security', name: 'security.label', hiddenName: 'security.id', xtype: 'combo', disabled: false, allowBlank: false, displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
							beforequery: function(queryEvent) {
								const f = queryEvent.combo.getParentForm().getForm();

								if (TCG.isTrue(TCG.getValue('tradeEntryDisabled', f.formValues))) {
									TCG.showError('Cannot change security if trade entry is disabled.  Please update this security on a replication where trade entry is not disabled and it will be automatically updated here.', 'Security Change');
									return false;
								}

								const actualContracts = TCG.getValue('actualContracts', f.formValues);
								if (actualContracts !== 0) {
									TCG.showError('Cannot change security if actual contracts are present.', 'Security Change');
									return false;
								}

								const securityGroupId = f.getFormValue('replicationAllocation.replicationSecurityGroup.id');

								queryEvent.combo.setBaseParam('active', true);

								// If there is no security group defined, just use the instrument from the existing security
								if (TCG.isBlank(securityGroupId)) {
									queryEvent.combo.setBaseParam('instrumentId', TCG.getValue('security.instrument.id', f.formValues));
								}
								// Otherwise use the security group (and instrument if defined on the allocation)
								else {
									const instrumentId = f.getFormValue('replicationAllocation.replicationInstrument.id');
									queryEvent.combo.setBaseParam('instrumentId', instrumentId);
									queryEvent.combo.setBaseParam('securityGroupId', securityGroupId);
								}
							},

							listeners: {
								'beforerender': function() {
									this.drillDownMenu = new Ext.menu.Menu({
										items: [
											{
												text: 'Open Detail Window', iconCls: 'info', handler: function() {
													this.createDetailClass(false);
												}, scope: this
											},
											{
												text: 'Add New Security For Instrument', iconCls: 'add', handler: function() {
													this.createDetailClass(true);
												}, scope: this
											},
											{
												text: 'Add New Security (Using Existing Security as Template)', iconCls: 'copy', handler: function() {
													this.createDetailClass(true, true);
												}, scope: this
											}
										]
									});
								}
							},

							createDetailClass: function(newInstance, useTemplate) {
								const fp = TCG.getParentFormPanel(this);
								const w = TCG.isNull(fp) ? {} : fp.getWindow();
								const className = this.getDetailPageClass(newInstance, useTemplate);
								let id = this.getValue();
								if (TCG.isBlank(id) || newInstance) {
									id = undefined;
								}
								const params = id ? {id: id} : undefined;
								const defaultData = this.getDefaultData(TCG.isNull(fp) ? {} : fp.getForm(), newInstance);
								const cmpId = TCG.getComponentId(className, id);
								TCG.createComponent(className, {
									modal: (TCG.isNull(id)),
									id: cmpId,
									defaultData: defaultData,
									params: params,
									openerCt: this, // for modal new item window will call reload() on close
									defaultIconCls: w.iconCls
								});
							},
							getDetailPageClass: function(newInstance, useTemplate) {
								if (newInstance && useTemplate) {
									return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
								}
								return this.detailPageClass;
							},
							getDefaultData: function(f) { // defaults client for "add new" drill down
								return {
									instrument: TCG.getValue('security.instrument', f.formValues),
									copyFromSecurityId: TCG.getValue('security.id', f.formValues),
									copyFromSecurityIdLabel: TCG.getValue('security.label', f.formValues)
								};
							}
						},
						{fieldLabel: 'Contract Value', name: 'value', xtype: 'floatfield', readOnly: true, submitValue: false},
						{fieldLabel: 'Price', name: 'securityPrice', xtype: 'floatfield', readOnly: true, submitValue: false},
						{fieldLabel: 'Trade Price', qtip: 'Price Override Used on Trade Creation Screen Only.  When using Live Prices feature, this price is used instead.', name: 'tradeSecurityPrice', xtype: 'floatfield'},
						{fieldLabel: 'Dirty Price', name: 'securityDirtyPrice', xtype: 'floatfield', readOnly: true, submitValue: false},
						{fieldLabel: 'Days To Maturity', name: 'daysToMaturity', xtype: 'numberfield', readOnly: true, submitValue: false},
						{fieldLabel: 'Interest Rate', name: 'interestRate', xtype: 'floatfield', readOnly: true, submitValue: false},
						{fieldLabel: 'Duration', name: 'duration', xtype: 'floatfield', readOnly: true, submitValue: false},
						{
							fieldLabel: 'Delta', name: 'delta', xtype: 'floatfield', readOnly: true, submitValue: false,
							qtip: 'Market Data Value for <i>Delta</i> field. <br><br><b>Special Adjustment for Options:</b>&nbsp;Delta is Negated If Call Option and Short Target or Put Option and Long Target'
						},
						{fieldLabel: 'Factor', name: 'factor', xtype: 'floatfield', readOnly: true, submitValue: false, qtip: 'Current Factor Change event value for the security. If there are no events available the value will be 1.'},
						{
							fieldLabel: 'Syn Adj Factor', name: 'syntheticAdjustmentFactor', xtype: 'floatfield', readOnly: true, submitValue: false,
							qtip: 'Formula: 1 / (1+((Days to Maturity/365)*Interest Rate))'
						},
						{
							fieldLabel: 'Index Ratio', name: 'indexRatio', xtype: 'floatfield', readOnly: true, submitValue: false,
							qtip: 'Index Ratio is a ratio between current and base CPI\'s and is used by inflation linked securities (TIPS, etc.).'
						},
						{fieldLabel: 'Exchange Rate', name: 'exchangeRate', xtype: 'floatfield', readOnly: true, submitValue: false}
					]
				},
					{
						xtype: 'fieldset',
						title: 'Underlying Security',
						name: 'underlyingSecurityFieldset',
						items: [
							{fieldLabel: 'Security', name: 'underlyingSecurity.label', xtype: 'linkfield', detailIdField: 'underlyingSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitValue: false, submitDetailField: false},
							{fieldLabel: 'Price', name: 'underlyingSecurityPrice', xtype: 'floatfield', readOnly: true, submitValue: false},
							{fieldLabel: 'Trade Price', qtip: 'Price Override Used on Trade Creation Screen Only.  When using Live Prices feature, this price is used instead.', name: 'tradeUnderlyingSecurityPrice', xtype: 'floatfield'}
						]
					},
					{
						xtype: 'fieldset',
						title: 'Currency',
						name: 'currencyFieldset',
						items: [
							// Physical Currency
							{fieldLabel: 'CCY (Local)', name: 'currencyActualLocalAmount', xtype: 'currencyfield', decimalPrecision: 0},
							{fieldLabel: 'Unrealized (Local)', name: 'currencyUnrealizedLocalAmount', xtype: 'currencyfield', decimalPrecision: 0, qtip: 'Unrealized Exposure is added when the CCY replication is a matching replication and ONLY includes exposure from positions in the same asset class.'},
							{fieldLabel: 'Total (Local)', name: 'currencyTotalActualLocalAmount', xtype: 'currencyfield', decimalPrecision: 0},

							{fieldLabel: 'FX Rate', name: 'currencyExchangeRate', xtype: 'floatfield'},

							{fieldLabel: 'Total (Base)', qtip: 'Physical Currency + Unrealized Currency Exposure in Account Base Currency', name: 'currencyTotalActualBaseAmount', xtype: 'currencyfield', decimalPrecision: 0},
							{fieldLabel: 'CCY Other', name: 'currencyOtherBaseAmount', xtype: 'currencyfield', decimalPrecision: 0, qtip: 'Currency Other applied to this contract in Account Base Currency'},
							{fieldLabel: 'CCY (Trading)', name: 'currencyDenominationBaseAmount', xtype: 'currencyfield', decimalPrecision: 0, qtip: 'Trading Currency applied to this contract in Account Base Currency.  Trading currency is applied differently than currency other when the underlying security equals the account base currency.  The trading currency of the security is then applied here and added to the target to get the net allocation.'}
						]
					}

				]
			},

				{columnWidth: .02, items: [{xtype: 'label', html: '&nbsp;'}]},

				{
					columnWidth: .50,
					items: [
						{
							xtype: 'fieldset',
							title: 'Target Allocations',
							items: [
								{
									fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
									defaults: {
										xtype: 'displayfield',
										style: 'text-align: right',
										flex: 1
									},
									items: [
										{value: 'Original', submitValue: false},
										{value: 'Adjusted', submitValue: false}
									]
								},
								{
									fieldLabel: 'Allocation %', xtype: 'compositefield',
									defaults: {
										xtype: 'floatfield',
										flex: 1
									},
									items: [
										{name: 'allocationWeight', readOnly: true, submitValue: false},
										{name: 'allocationWeightAdjusted', readOnly: true, submitValue: false}
									]
								},
								{
									fieldLabel: 'Allocation Amount', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										flex: 1
									},
									items: [
										{name: 'targetExposure', readOnly: true, submitValue: false},
										{name: 'targetExposureAdjusted', readOnly: true, submitValue: false}
									]
								},
								{
									fieldLabel: 'Target Contracts', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										flex: 1
									},
									items: [
										{name: 'targetContracts', readOnly: true, submitValue: false},
										{name: 'targetContractsAdjusted', readOnly: true, submitValue: false}
									]
								}
							]
						},

						{
							xtype: 'fieldset',
							title: 'Current Positions',
							items: [
								{
									fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
									defaults: {
										xtype: 'displayfield',
										style: 'text-align: right',
										flex: 1
									},
									items: [
										{value: 'Contracts', submitValue: false},
										{value: 'Exposure', submitValue: false}
									]
								},
								{
									fieldLabel: 'Actual', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										flex: 1
									},
									items: [
										{name: 'actualContracts', readOnly: true, submitValue: false},
										{name: 'actualExposure', readOnly: true, submitValue: false}
									]
								},
								{
									fieldLabel: 'Virtual', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										flex: 1
									},
									items: [
										{name: 'virtualContracts', readOnly: true, submitValue: false},
										{name: 'virtualExposure', readOnly: true, submitValue: false}
									]
								},

								{
									fieldLabel: 'Difference', xtype: 'compositefield',
									defaults: {
										xtype: 'currencyfield',
										flex: 1
									},
									items: [
										{name: 'actualTargetAdjustedContractDifference', readOnly: true, submitValue: false},
										{name: 'targetAdjustedLessActualExposure', readOnly: true, submitValue: false}
									]
								}

							]
						}]
				}]
		}]
});
Ext.reg('run-replication-formpanel', Clifton.portfolio.run.replication.ReplicationFormPanel);

Clifton.portfolio.run.replication.PendingTradesGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioRunTradeDetailPendingTradeList',
	instructions: 'The following is a list of pending trades tied to the client account and replication security.  Note: This does not take into account any contract splitting.',
	getLoadParams: function() {
		const f = this.getWindow().getMainForm();
		return {
			runId: TCG.getValue('portfolioRun.id', f.formValues),
			detailId: f.formValues.id
		};
	},
	remoteSort: true,
	requestIdDataIndex: true,
	columns: [
		{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
		{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name'},
		{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name'},
		{header: 'Trade Type', width: 11, dataIndex: 'tradeType.name', hidden: true},
		{header: 'Client Account', width: 40, dataIndex: 'clientInvestmentAccount.label', hidden: true},
		{header: 'Holding Account', width: 40, dataIndex: 'holdingInvestmentAccount.label', hidden: true},
		{header: 'Description', width: 50, dataIndex: 'description', hidden: true},
		{
			header: 'Buy/Sell', width: 8, dataIndex: 'buy', type: 'boolean',
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{header: 'Quantity', width: 10, dataIndex: 'quantityIntended', type: 'float', useNull: true},
		{header: 'Security', width: 15, dataIndex: 'investmentSecurity.symbol', idDataIndex: 'investmentSecurity.id', hidden: true},
		{header: 'Security Name', width: 40, dataIndex: 'investmentSecurity.name', hidden: true},
		{header: 'Exchange Rate', width: 12, dataIndex: 'exchangeRate', type: 'float', useNull: true, hidden: true},
		{header: 'Average Price', width: 14, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
		{header: 'Commission Per Unit', width: 10, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
		{header: 'Fee', width: 10, dataIndex: 'feeAmount', type: 'float', hidden: true},
		{header: 'Accounting Notional', width: 18, dataIndex: 'accountingNotional', type: 'currency', useNull: true},
		{header: 'Trader', width: 12, dataIndex: 'traderUser.label'},
		{header: 'Team', width: 12, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', hidden: true},
		{header: 'Traded On', width: 13, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: 'Settled On', width: 13, dataIndex: 'settlementDate', hidden: true}
	],
	editor: {
		detailPageClass: 'Clifton.trade.TradeWindow',
		drillDownOnly: true
	}
});
Ext.reg('run-replication-pending-trade-gridpanel', Clifton.portfolio.run.replication.PendingTradesGridPanel);

Clifton.portfolio.run.replication.PendingTransferGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioRunTradeDetailPendingTransferList',
	instructions: 'The following is a list of pending transfers tied to the client account and replication security.  Note: This does not take into account any contract splitting.',
	getLoadParams: function() {
		const f = this.getWindow().getMainForm();
		return {
			runId: TCG.getValue('portfolioRun.id', f.formValues),
			detailId: f.formValues.id
		};
	},
	remoteSort: true,
	columns: [
		{header: 'Transfer ID', width: 65, dataIndex: 'id'},
		{header: 'Transfer Type', width: 70, dataIndex: 'transferType', filter: {type: 'list', options: [['FROM', 'From Outside'], ['TO', 'To Outside'], ['BETWEEN', 'Between']]}},
		{header: 'From (Client Account)', width: 200, dataIndex: 'fromClientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'fromClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'From (Holding Account)', width: 200, dataIndex: 'fromHoldingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'fromHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
		{header: 'To (Client Account)', width: 200, dataIndex: 'toClientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'toClientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'To (Holding Account)', width: 200, dataIndex: 'toHoldingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'toHoldingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
		{header: 'Collateral', width: 55, dataIndex: 'collateralTransfer', type: 'boolean'},
		{header: 'Note', width: 200, dataIndex: 'note', hidden: true},
		{header: 'Price Date', width: 60, dataIndex: 'exposureDate', hidden: true},
		{header: 'Date', width: 60, dataIndex: 'transactionDate'}
	],
	editor: {
		detailPageClass: 'Clifton.accounting.positiontransfer.TransferWindow',
		drillDownOnly: true
	}
});
Ext.reg('run-replication-pending-transfer-gridpanel', Clifton.portfolio.run.replication.PendingTransferGridPanel);
