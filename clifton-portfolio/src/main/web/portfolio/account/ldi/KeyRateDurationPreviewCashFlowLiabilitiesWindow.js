Clifton.portfolio.account.ldi.KeyRateDurationPreviewCashFlowLiabilitiesWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Cash Flow Group KRD and DV01 Preview',
	iconCls: 'account',
	width: 750,
	height: 450,

	items: [{
		xtype: 'formpanel',
		instructions: 'Select an LDI Account and Balance Date and click Preview to see the calculated modified KRD and DV01 values for each Cash Flow Group History.',
		items: [
			{fieldLabel: 'LDI Account', name: 'clientInvestmentAccountLabel', hiddenName: 'clientInvestmentAccountId', xtype: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true&serviceTypeProcessingType=LDI.json', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'datefield'},
			{xtype: 'sectionheaderfield', header: 'Result'},
			{fieldLabel: '', name: 'result', xtype: 'textarea', height: 200, readOnly: true}
		],
		buttons: [
			{
				text: 'Preview',
				iconCls: 'preview',
				handler: function() {
					const f = TCG.getParentFormPanel(this);
					f.loadPreview();
				}
			}
		],
		getDefaultData: function(win) {
			this.setFormValue('balanceDate', win.defaultData.balanceDate);
			return win.defaultData;
		},
		loadPreview: function() {
			const clientInvestmentAccountId = this.getFormValue('clientInvestmentAccountId');
			const balanceDate = this.getForm().findField('balanceDate').getValue().format('m/d/Y');

			if (clientInvestmentAccountId !== '' && balanceDate !== '') {
				const result = TCG.getResponseText('portfolioAccountKeyRateDurationCashFlowLiabilitiesResultForClientAccount.json?clientInvestmentAccountId=' + clientInvestmentAccountId + '&balanceDate=' + balanceDate, this);
				this.getForm().findField('result').setValue(Ext.decode(result).data);
			}
		}
	}]
});
