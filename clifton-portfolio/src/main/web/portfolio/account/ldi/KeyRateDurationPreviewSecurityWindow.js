Clifton.portfolio.account.ldi.KeyRateDurationPreviewSecurityWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Client Account KRD Preview',
	iconCls: 'account',
	width: 600,
	height: 450,

	items: [{
		xtype: 'formpanel',
		instructions: 'Select a Security and Date and Click Preview to see the calculated modified KRD values.',
		items: [
			{name: 'clientInvestmentAccountId', xtype: 'hidden'},
			{fieldLabel: 'Security', name: 'securityLabel', hiddenName: 'securityId', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json?active=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
			{fieldLabel: 'Date', name: 'date', xtype: 'datefield', value: new Date()},
			{xtype: 'sectionheaderfield', header: 'Result'},
			{fieldLabel: '', name: 'result', xtype: 'textarea', height: 200, readOnly: true}
		],
		buttons: [
			{
				text: 'Preview',
				iconCls: 'preview',
				handler: function() {
					const f = TCG.getParentFormPanel(this);
					f.loadPreview();
				}
			}
		],
		loadPreview: function() {
			const caId = this.getForm().findField('clientInvestmentAccountId').getValue();
			const securityId = this.getForm().findField('securityId').getValue();
			const date = this.getForm().findField('date').getValue().format('m/d/Y');

			if (securityId !== '' && date !== '') {
				const result = TCG.getResponseText('portfolioAccountKeyRateDurationResultForSecurity.json?clientInvestmentAccountId=' + caId + '&securityId=' + securityId + '&date=' + date, this);
				this.getForm().findField('result').setValue(Ext.decode(result).data);
			}
		}
	}]
});
