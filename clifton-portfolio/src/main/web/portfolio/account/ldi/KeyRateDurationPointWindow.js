TCG.use('Clifton.investment.account.targetexposure.TargetExposureAdjustmentFieldSet');

Clifton.portfolio.account.ldi.KeyRateDurationPointWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Client Account KRD Point',
	iconCls: 'account',
	width: 700,
	height: 600,
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		url: 'portfolioAccountKeyRateDurationPoint.json?requestedPropertiesToExcludeGlobally=targetExposureAssignmentCombinedList|jsonValue',

		items: [
			{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{fieldLabel: '', boxLabel: 'Inactive', name: 'inactive', xtype: 'checkbox'},
			{fieldLabel: '', boxLabel: 'Reportable Only', name: 'reportableOnly', xtype: 'checkbox'},
			{
				fieldLabel: 'KRD Point', name: 'marketDataField.name', hiddenName: 'marketDataField.id', xtype: 'combo', url: 'marketDataFieldListByGroup.json', loadAll: true, detailPageClass: 'Clifton.marketdata.field.DataFieldWindow',
				maxHeight: 550,
				beforequery: function(queryEvent) {
					const grpId = TCG.data.getData('marketDataFieldGroupByName.json?name=Key Rate Duration Points', queryEvent.combo, 'market.data.field.group.Key Rate Duration Points');
					const cmb = queryEvent.combo;
					cmb.store.baseParams = {
						dataFieldGroupId: grpId.id
					};
				}
			},
			{fieldLabel: 'Target Hedge %', name: 'targetHedgePercent', xtype: 'currencyfield', decimalPrecision: 5},

			// Rebalance Triggers
			{
				fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
				defaults: {
					xtype: 'displayfield',
					flex: 1
				},
				items: [
					{value: 'Minimum %'},
					{value: 'Maximum %'}
				]
			},
			{
				fieldLabel: 'Rebalance Triggers', xtype: 'compositefield',
				qtip: 'Triggers are used to maintain a band where KRD Point can fluctuate before rebalancing is recommended.',
				defaults: {
					xtype: 'currencyfield',
					decimalPrecision: 5,
					minValue: 0,
					flex: 1
				},
				items: [
					{name: 'rebalanceMinPercent'},
					{name: 'rebalanceMaxPercent'}
				]
			},

			{
				xtype: 'investment-account-targetexposure-adjustment-fieldset',
				fieldSetInstructions: 'Use the following selection to change Target DV01 for this KRD Point and redistribute the difference across the following KRD Points either proportionally or using specified percentage allocation that adds up to 100%.  Proportional allocation uses the KRD Point Target Hedge % by default to determine its share of the adjustment, but can be changed by selecting Proportional type below. Actual DV01 value for a point is the Total Assets (i.e. Effective) DV01 = (Assets + Exposure).',
				allowedOptions: ['NONE', 'TFA', 'CONSTANT', 'MANAGER_MARKET_VALUE'],
				additionalItems: [
					{
						xtype: 'formfragment',
						frame: false,
						items: [
							{
								fieldLabel: 'Proportional Type', name: 'targetExposureProportionalType', xtype: 'system-list-combo', flex: 1, listName: 'Portfolio: KRD Target Exposure Proportional Types',
								defaultValue: 'Target Hedge',
								qtip: 'Target Hedge will be used by default for proportional allocations.  You can select a specific type if you prefer to determine proportional allocations differently.'
							}
						]
					}
				],
				assignmentGrid: {
					xtype: 'formgrid',
					storeRoot: 'targetAdjustmentAssignmentList',
					dtoClass: 'com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationPointAssignment',
					plugins: {ptype: 'groupsummary'},
					columnsConfig: [
						{
							header: 'KRD Point Assignment', width: 280, dataIndex: 'referenceTwo.marketDataField.name', idDataIndex: 'referenceTwo.id',
							editor: {
								xtype: 'combo', url: 'portfolioAccountKeyRateDurationPointListFind.json', displayField: 'marketDataField.name',
								beforequery: function(queryEvent) {
									const grid = queryEvent.combo.gridEditor.containerGrid;
									const f = TCG.getParentFormPanel(grid).getForm();
									queryEvent.combo.store.baseParams = {targetExposureUsed: 'false', inactive: false, clientInvestmentAccountId: TCG.getValue('clientInvestmentAccount.id', f.formValues)};
								}
							}
						},
						{header: 'Proportional', width: 90, dataIndex: 'proportionalAllocation', type: 'boolean', editor: {xtype: 'checkbox'}},
						{
							header: 'Percentage', width: 90, dataIndex: 'allocationPercent', type: 'percent', useNull: true, numberFormat: '0,000.0000',
							editor: {
								xtype: 'currencyfield', decimalPrecision: 4
							}
						}
					]
				}
			}
		]
	}]
})
;
