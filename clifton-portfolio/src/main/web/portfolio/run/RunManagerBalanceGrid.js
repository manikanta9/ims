Clifton.portfolio.run.RunManagerBalanceGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioRunManagerAccountBalanceListFind',
	//instructions: 'Selected client account has the following asset class allocation of manager account cash and security balances and their details.',
	appendStandardColumns: false,
	additionalPropertiesToRequest: 'manualAdjustmentList.cashValue|manualAdjustmentList.securitiesValue|manualAdjustmentList.adjustmentType.name|manualAdjustmentList.adjustmentType.cssStyle|manualAdjustmentList.note|managerAccountBalance.managerAccount.id|managerAccountBalance.id',
	showGroupedAllocations: false, // Enabled for PIOS to include Groupings and Allocation Display
	groupedAllocationBoxLabel: 'View Non Rollup Asset Class Level&nbsp;',

	initComponent: function() {
		if (this.showGroupedAllocations === true) {
			this.groupField = this.groupField || 'accountAssetClass.label';
			this.groupTextTpl = this.groupTextTpl || '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Managers" : "Manager"]})';
		}

		Clifton.portfolio.run.RunManagerBalanceGrid.superclass.initComponent.call(this, arguments);

		// Default presentation
		const cm = this.getColumnModel();
		cm.setHidden(cm.findColumnIndex('allocationPercent'), TCG.isFalse(this.showGroupedAllocations));
	},


	getLoadParams: function() {
		const lps = {runId: this.getWindow().getMainFormId()};
		if (this.showGroupedAllocations === true) {
			const nonRollupOnly = TCG.getChildByName(this.getTopToolbar(), 'nonRollupOnly').getValue();
			lps.rootOnly = !nonRollupOnly;
		}
		return lps;
	},
	topToolbarUpdateCountPrefix: '-',
	getTopToolbarFilters: function(toolbar) {
		const gp = this;
		const filters = [];
		filters.push(
			{
				boxLabel: 'View Manually Adjusted Only&nbsp;', xtype: 'checkbox', name: 'manuallyAdjusted',
				listeners: {
					check: function(f, checked) {
						if (checked === true) {
							gp.setFilterValue('manuallyAdjusted', checked, true, true);
						}
						else {
							gp.clearFilter('manuallyAdjusted', false);
						}
					}
				}
			}
		);
		if (TCG.isTrue(this.showGroupedAllocations)) {
			filters.push(
				{
					boxLabel: this.groupedAllocationBoxLabel, xtype: 'checkbox', name: 'nonRollupOnly',
					listeners: {
						check: function(field) {
							TCG.getParentByClass(field, Ext.Panel).reload();
						}
					}
				}
			);
		}
		return filters;
	},

	remoteSort: true,
	isPagingEnabled: function() {
		return false;
	},
	pageSize: 1000,
	columns: [
		{header: 'Asset Class', width: 140, dataIndex: 'accountAssetClass.label', hidden: true},
		{header: 'Manager Account', width: 170, dataIndex: 'managerAccountBalance.managerAccount.label'},
		{header: 'Manager Type', width: 100, dataIndex: 'managerAccountBalance.managerAccount.managerType', hidden: true},
		{header: 'Account', width: 75, dataIndex: 'managerAccountBalance.managerAccount.accountNumber', hidden: true},
		{header: 'Custodian Account', width: 170, dataIndex: 'managerAccountBalance.managerAccount.custodianAccount.number', hidden: true},
		{header: 'CCY', width: 50, dataIndex: 'managerAccountBalance.managerAccount.baseCurrency.symbol', hidden: true},
		//{header: 'Private', width: 75, dataIndex: 'privateManager', hidden: true, type: 'boolean'},
		{header: 'Manually Adjusted', width: 75, dataIndex: 'manuallyAdjusted', type: 'boolean', hidden: true},
		// Asset Class Allocations
		{header: 'Allocation', width: 70, hidden: true, tooltip: 'Some managers are allocated a portion of the balance across asset classes.', dataIndex: 'allocationPercent', type: 'percent', numberFormat: '0,000.0000000000'},

		{
			header: 'Base Securities', width: 100, dataIndex: 'securitiesBalanceBaseAllocationAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, css: 'BACKGROUND-COLOR: #F6F6F6; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Base Balances include all system/automatic adjustments, and adjusted for allocation percentages.'
		},
		{
			header: 'Base Cash', width: 100, dataIndex: 'cashBalanceBaseAllocationAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, css: 'BACKGROUND-COLOR: #F6F6F6;',
			tooltip: 'Base Balances include all system/automatic adjustments, and adjusted for allocation percentages.'
		},
		{
			header: 'Base Total', width: 100, dataIndex: 'totalBalanceBaseAllocationAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, css: 'BACKGROUND-COLOR: #F6F6F6; BORDER-RIGHT: #c0c0c0 1px solid;',
			tooltip: 'Base Balances include all system/automatic adjustments, and adjusted for allocation percentages.'
		},
		{
			header: 'Securities Adjustment', width: 100, dataIndex: 'securitiesManualAdjustmentTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', useNull: true, css: 'BACKGROUND-COLOR: #FAFAFA; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Manual Security adjustments applied to the manager balance, adjusted for allocation percentages.',
			renderer: function(v, metaData, r) {
				return Clifton.portfolio.run.RenderManagerBalanceAdjustmentList(v, metaData, r, false);
			},
			summaryRenderer: function(v) {
				return TCG.numberFormat(v, '0,000');
			}
		},
		{
			header: 'Cash Adjustment', width: 100, dataIndex: 'cashManualAdjustmentTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', useNull: true, css: 'BACKGROUND-COLOR: #FAFAFA; BORDER-RIGHT: #c0c0c0 1px solid;',
			tooltip: 'Manual Cash adjustments applied to the manager balance, adjusted for allocation percentages.',
			renderer: function(v, metaData, r) {
				return Clifton.portfolio.run.RenderManagerBalanceAdjustmentList(v, metaData, r, true);
			},
			summaryRenderer: function(v) {
				return TCG.numberFormat(v, '0,000');
			}
		},
		{
			header: 'Adjusted Securities', width: 100, dataIndex: 'securitiesAllocationAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, css: 'BACKGROUND-COLOR: #EEEEEE; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Adjusted Balance = Base Balance + Manual Adjustments.'
		},
		{
			header: 'Adjusted Cash', width: 100, dataIndex: 'cashAllocationAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, css: 'BACKGROUND-COLOR: #EEEEEE;',
			tooltip: 'Adjusted Balance = Base Balance + Manual Adjustments.'
		},
		{
			header: 'Adjusted Total', width: 100, dataIndex: 'totalAllocationAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, css: 'BACKGROUND-COLOR: #EEEEEE; BORDER-RIGHT: #c0c0c0 1px solid;',
			tooltip: 'Adjusted Balance = Base Balance + Manual Adjustments.'
		}

	],
	plugins: {
		ptype: 'groupsummary'
	}
	,
	editor: {
		detailPageClass: 'Clifton.investment.manager.AccountWindow',
		drillDownOnly: true,
		getDetailPageId: function(gridPanel, row) {
			return row.json.managerAccountBalance.managerAccount.id;
		},

		// Used to Open the Manager Balance Window Directly, or the Manager Account - Balance History Tab
		openWindowFromContextMenu: function(grid, rowIndex, screen) {
			const gridPanel = this.getGridPanel();
			const row = grid.store.data.items[rowIndex];
			let clazz;
			let id;
			let defaultActiveTabName = undefined;

			if (screen === 'MANAGER_BALANCE') {
				id = row.json.managerAccountBalance.id;
				clazz = 'Clifton.investment.manager.BalanceWindow';
			}
			else {
				id = row.json.managerAccountBalance.managerAccount.id;
				clazz = 'Clifton.investment.manager.AccountWindow';
				defaultActiveTabName = 'Balance History';
			}
			if (clazz) {
				this.openDetailPage(clazz, gridPanel, id, row, null, defaultActiveTabName);
			}
		}
	}
	,
	listeners: {
		afterrender: function(gridPanel) {
			const el = gridPanel.getEl();
			el.on('contextmenu', function(e, target) {
				const g = gridPanel.grid;
				g.contextRowIndex = g.view.findRowIndex(target);
				e.preventDefault();
				if (!g.drillDownMenu) {
					g.drillDownMenu = new Ext.menu.Menu({
						items: [
							{
								text: 'Open Manager Balance', iconCls: 'manager', handler: function() {
									gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'MANAGER_BALANCE');
								}
							},
							{
								text: 'Open Manager Balance History', iconCls: 'grid', handler: function() {
									gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'MANAGER_BALANCE_HISTORY');
								}
							}
						]
					});
				}
				TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
			}, gridPanel);
		}
	}

});
Ext.reg('portfolio-run-manager-balance-grid', Clifton.portfolio.run.RunManagerBalanceGrid);

Clifton.portfolio.run.RenderManagerBalanceAdjustmentList = function(v, metaData, r, cash) {
	let result = '';
	if (r.json && r.json.manualAdjustmentList) {
		const allocationPercent = r.json.allocationPercent / 100;
		for (let i = 0; i < r.json.manualAdjustmentList.length; i++) {
			const adjustment = r.json.manualAdjustmentList[i];
			let adjValue = (TCG.isTrue(cash) ? adjustment.cashValue : adjustment.securitiesValue);
			if (TCG.isNotBlank(adjValue) && adjValue !== 0) {
				adjValue = TCG.numberFormat(adjValue * allocationPercent, '0,000', false);
				const adjType = adjustment.adjustmentType.name;
				const style = adjustment.adjustmentType.cssStyle;
				result += '<div ';
				if (TCG.isNotBlank(style)) {
					result += 'style="' + style + '"';
				}
				let note = '';
				if (adjustment.note && TCG.isNotBlank(adjustment.note)) {
					note = ': ' + adjustment.note;
				}
				result += TCG.renderQtip('<b>' + adjType + '</b>' + note) + '>' + adjValue + '</div>';
			}
		}
	}
	return result;
};
