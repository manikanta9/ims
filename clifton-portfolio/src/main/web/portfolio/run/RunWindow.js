// Window Selector
Clifton.portfolio.run.RunWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'portfolioRun.json',

	getClassName: function(config, entity) {
		let processingType = undefined;
		if (entity) {
			processingType = TCG.getValue('serviceProcessingType', entity);
		}
		else if (config && config.defaultData && config.defaultData.clientInvestmentAccount) {
			const act = TCG.data.getData('investmentAccount.json?id=' + config.defaultData.clientInvestmentAccount.id, this);
			config.defaultData['clientInvestmentAccount'] = act;
			processingType = TCG.getValue('serviceProcessingType.processingType', act);
			config.defaultData['serviceProcessingType'] = processingType;
		}
		let className = undefined;
		if (processingType) {
			className = Clifton.portfolio.run.PortfolioRunWindowOverrides[processingType];
		}
		if (!className) {
			className = Clifton.portfolio.run.PortfolioRunWindowOverrides['DEFAULT'];
		}
		return className;
	}
});
