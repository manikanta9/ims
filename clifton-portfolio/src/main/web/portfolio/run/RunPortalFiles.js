Clifton.portfolio.run.RunPortalFiles = {
	title: 'Portal Files',
	reloadOnTabChange: true,
	items: [
		{
			xtype: 'portal-files-grid-for-source-entity',
			sourceTableName: 'PortfolioRun'
		}
	]
};
