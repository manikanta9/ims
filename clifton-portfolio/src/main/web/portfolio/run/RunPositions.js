Clifton.portfolio.run.RunPositions = {
	title: 'Positions',
	items: [{
		name: 'accountingPositionListUsingCommand',
		xtype: 'gridpanel',
		instructions: 'Selected client account had the following positions on the balance date of this run.  If this account has sub accounts you can view those positions by selecting the account from the filter.  Clear the value to view at the main account level.',
		appendStandardColumns: false,
		getLoadParams: function() {
			const formValues = this.getWindow().getMainForm().formValues;
			const transactionDate = TCG.parseDate(TCG.getValue('balanceDate', formValues));
			const clientInvestmentAccountId = TCG.getChildByName(this.getTopToolbar(), 'investmentAccountId').getValue()
				|| TCG.getValue('clientInvestmentAccount.id', formValues);
			const pendingActivityRequest = TCG.getChildByName(this.getTopToolbar(), 'pendingActivityRequest').getValue();
			return {
				clientInvestmentAccountId: clientInvestmentAccountId,
				transactionDate: transactionDate.dateFormat('m/d/Y'),
				pendingActivityRequest: pendingActivityRequest
			};
		},

		getTopToolbarFilters: function(toolbar) {
			const grid = this;
			return [
				'-',
				{
					xtype: 'toolbar-checkbox',
					name: 'pendingActivityRequest',
					boxLabel: 'Include Pending Activity',
					tooltip: 'Include impact of pending trades, transfers, etc.',
					checkedValue: 'POSITIONS_WITH_PREVIEW'
				},
				'-',
				{
					fieldLabel: 'Sub Account', xtype: 'combo', name: 'investmentAccountId', width: 180, url: 'investmentAccountRelationshipListFind.json', displayField: 'referenceTwo.label', valueField: 'referenceTwo.id',
					beforequery: function(queryEvent) {
						const w = grid.getWindow();
						queryEvent.combo.store.baseParams = {
							mainAccountId: TCG.getValue('clientInvestmentAccount.id', w.getMainForm().formValues),
							purposeName: 'Clifton Sub-Account'
						};
					}
				}
			];
		},
		groupField: 'investmentSecurity.label',
		groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Lots" : "Lot"]}',

		columns: [
			{header: 'Security', width: 150, dataIndex: 'investmentSecurity.label', hidden: true},
			{header: 'Opening Date', width: 75, dataIndex: 'originalTransactionDate'},
			{header: 'Opening FX Rate', width: 75, dataIndex: 'exchangeRateToBase', type: 'currency', numberFormat: '0,000.0000000000'},
			{header: 'Opening Price', width: 70, dataIndex: 'openingPrice', type: 'currency', useNull: true},
			{header: 'Opening Quantity', width: 80, dataIndex: 'openingQuantityClean', type: 'int', summaryType: 'sum', useNull: true, negativeInRed: true},
			{header: 'Remaining Quantity', width: 85, dataIndex: 'remainingQuantityClean', type: 'int', summaryType: 'sum', useNull: true, negativeInRed: true},

			{header: 'Opening Cost Basis', width: 85, dataIndex: 'openingCostBasis', type: 'currency', summaryType: 'sum'},
			{header: 'Remaining Cost Basis', width: 90, dataIndex: 'remainingCostBasis', type: 'currency', summaryType: 'sum'}
		],
		plugins: {ptype: 'groupsummary'},
		editor: {
			detailPageClass: 'Clifton.accounting.gl.TransactionWindow',
			drillDownOnly: true
		}
	}]
};
