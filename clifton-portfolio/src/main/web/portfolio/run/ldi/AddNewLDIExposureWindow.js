Clifton.portfolio.run.ldi.AddNewLDIExposureWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Add Security',
	iconCls: 'replicate',
	hideApplyButton: true,
	width: 600,
	height: 400,

	// Special Method so Replications are fully reloaded on Trade Creation Screen to get new ones retrieved and added to list
	reloadOpener: function() {
		const win = this;
		if (win.openerCt && win.openerCt.fullReload) {
			win.openerCt.fullReload();
		}
		else if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload) {
			win.openerCt.reload();
		}
	},

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		url: 'portfolioRunLDIExposure.json',

		getSaveURL: function() {
			return 'portfolioRunLDIExposureCopy.json';
		},

		instructions: 'Select a security to add to the portfolio run. Security will be validated based on the previously selected row. Note: Security will be added to the run for trading only and will not be added to the actual replication allocations.  All targets will be set to zero.',

		items: [
			{xtype: 'hidden', name: 'security.instrument.id', submitValue: false},
			{fieldLabel: 'Client Account', name: 'portfolioRun.clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'portfolioRun.clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow', submitDetailField: false},
			{
				fieldLabel: 'Trading Account',
				qtip: 'Trading Account or Position Account.  This client account these specific contracts come from/are traded in.',
				name: 'tradingClientAccount.label', xtype: 'linkfield', detailIdField: 'tradingClientAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow', submitDetailField: false
			},
			{fieldLabel: 'Replication', name: 'replicationAllocation.replication.label', xtype: 'linkfield', detailIdField: 'replicationAllocation.replication.id', detailPageClass: 'Clifton.investment.replication.ReplicationWindow', submitDetailField: false},
			{fieldLabel: 'Existing Security', xtype: 'linkfield', name: 'security.label', hiddenName: 'security.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'security.id'},
			{
				fieldLabel: 'New Security', name: 'newSecurityLabel', hiddenName: 'newSecurityId', xtype: 'combo', allowBlank: false, displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',

				listeners: {
					'beforerender': function() {
						this.drillDownMenu = new Ext.menu.Menu({
							items: [
								{
									text: 'Open Detail Window', iconCls: 'info',
									handler: function() {
										this.createDetailClass(false);
									}, scope: this
								},
								{
									text: 'Add New Security For Instrument', iconCls: 'add',
									handler: function() {
										this.createDetailClass(true);
									}, scope: this
								},
								{
									text: 'Add New Security (Using Existing Security as Template)', iconCls: 'copy',
									handler: function() {
										this.createDetailClass(true, true);
									}, scope: this
								}
							]
						});
					}
				},

				createDetailClass: function(newInstance, useTemplate) {
					const fp = TCG.getParentFormPanel(this);
					const w = (TCG.isNull(fp)) ? {} : fp.getWindow();
					const className = this.getDetailPageClass(newInstance, useTemplate);
					let id = this.getValue();
					if (TCG.isBlank(id) || newInstance) {
						id = null;
					}
					const params = id ? {id: id} : null;
					const defaultData = this.getDefaultData(TCG.isNull(fp) ? {} : fp.getForm());
					const cmpId = TCG.getComponentId(className, id);
					TCG.createComponent(className, {
						modal: TCG.isNull(id),
						id: cmpId,
						defaultData: defaultData,
						params: params,
						openerCt: this, // for modal new item window will call reload() on close
						defaultIconCls: w.iconCls
					});
				},
				getDetailPageClass: function(newInstance, useTemplate) {
					if (newInstance && useTemplate) {
						return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
					}
					return this.detailPageClass;
				},
				getDefaultData: function(f) { // defaults client for "add new" drill down
					return {
						instrument: TCG.getValue('security.instrument', f.formValues),
						copyFromSecurityId: TCG.getValue('security.id', f.formValues),
						copyFromSecurityIdLabel: TCG.getValue('security.label', f.formValues)
					};
				}
			},
			{fieldLabel: 'Date Override', qtip: 'Specify this date to override the date used to calculate the values of Key Rate Duration Points. If left blank, today\'s date with be used.', name: 'valueDateOverride', xtype: 'datefield'}
		]
	}]
});
