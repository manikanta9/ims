Clifton.portfolio.run.ldi.KeyRateDurationPointWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Key Rate Duration Point',
	iconCls: 'account',
	width: 700,
	height: 500,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		readOnly: true,
		url: 'portfolioRunLDIKeyRateDurationPoint.json',
		labelWidth: 125,

		items: [
			{fieldLabel: 'Client Account', name: 'portfolioRun.clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'portfolioAccount.clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{fieldLabel: 'KRD Point', name: 'keyRateDurationPoint.label', xtype: 'linkfield', detailIdField: 'keyRateDurationPoint.id', detailPageClass: 'Clifton.portfolio.account.ldi.KeyRateDurationPointWindow'},
			{fieldLabel: 'Balance Date', name: 'portfolioRun.balanceDate', xtype: 'datefield'},
			{
				xtype: 'panel',
				layout: 'column',
				items: [{
					columnWidth: .49,
					items: [
						{
							xtype: 'fieldset',
							title: 'DV01 Values',
							labelWidth: 160,
							items: [
								{fieldLabel: 'Liabilities', name: 'liabilitiesDv01Value', xtype: 'currencyfield'},
								{fieldLabel: 'Target', name: 'targetHedgeDv01Value', xtype: 'currencyfield'},
								{fieldLabel: 'Target (Adjusted)', name: 'targetHedgeDv01ValueAdjusted', xtype: 'currencyfield'},
								{fieldLabel: 'Assets', name: 'assetsDv01Value', xtype: 'currencyfield'},
								{fieldLabel: 'Exposure', name: 'exposureDv01Value', xtype: 'currencyfield'},
								{xtype: 'label', html: '<hr/>'},
								{fieldLabel: 'Total Assets', name: 'totalAssetsDv01Value', xtype: 'currencyfield'},
								{xtype: 'label', html: '<hr/>'},
								{fieldLabel: 'Deviation From Target', name: 'totalAssetsDeviationFromTargetDv01Value', xtype: 'currencyfield'}
							]
						}
					]
				},

					{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},

					{
						columnWidth: .50,
						items: [
							{
								xtype: 'fieldset',
								title: 'Percentages',
								labelWidth: 160,
								items: [
									{fieldLabel: 'Target Hedge', name: 'targetHedgePercent', xtype: 'currencyfield'},
									{fieldLabel: 'Target Hedge (Adjusted)', name: 'targetHedgePercentAdjusted', xtype: 'currencyfield'},
									{fieldLabel: 'Effective Hedge', name: 'effectiveHedgePercent', xtype: 'currencyfield'},
									{xtype: 'label', html: '<hr/>'},
									{fieldLabel: 'Deviation From Target', name: 'totalAssetsDeviationFromTargetPercent', xtype: 'currencyfield'},
									{xtype: 'label', html: '<hr/>'},
									{fieldLabel: 'Min Rebalance Trigger', name: 'rebalanceMinPercent', xtype: 'currencyfield'},
									{fieldLabel: 'Max Rebalance Trigger', name: 'rebalanceMinPercent', xtype: 'currencyfield'}
								]
							}]
					}
				]
			}
		]
	}]
});
