Clifton.portfolio.run.ldi.RunManagerAccount = {
	title: 'Manager Allocation',

	listeners: {
		beforerender: function() {
			this.add(this.maItem);
		}
	},

	items: [],
	maItem: {
		xtype: 'portfolio-run-ldi-manager-account-gridpanel'
	}
};
