Clifton.portfolio.run.ldi.trade.RunTradeDv01RecapFieldSet = {
	xtype: 'fieldset',
	title: 'DV01 Recap',
	labelWidth: 150,
	listeners: {
		collapse: function(p) {
			p.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		},
		expand: function(p) {
			p.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		}
	},
	items: [{
		xtype: 'formtable',
		initComponent: function() {
			const panel = TCG.getParentFormPanel(this);
			const id = panel.getWindow().params.id;

			const url = 'portfolioRunLDIKeyRateDurationPointListByRun.json?runId=' + id + '&reportableOnly=false&requestedMaxDepth=4';

			const pointList = TCG.data.getData(url, this);
			const size = pointList.length;
			this.columns = size + 2;

			const thisItems = [];
			thisItems.push({cellWidth: '150px', xtype: 'label'});

			// Column Headers
			Ext.each(pointList, function(f) {
				const dv01Name = f.keyRateDurationPoint.keyRateDurationPointLabel;
				thisItems.push({html: dv01Name, xtype: 'label', style: 'text-align: right'});
			});
			thisItems.push({html: 'Total', xtype: 'label', style: 'text-align: right'});

			// Liabilities DV01
			thisItems.push({html: 'Liabilities:', xtype: 'label'});
			Ext.each(pointList, function(f) {
				thisItems.push({name: 'liabilitiesDv01Value_' + f.keyRateDurationPoint.id, value: f.liabilitiesDv01Value, submitValue: false});
			});
			thisItems.push({name: 'portfolioTotalValue'});

			// Target Hedge Ratio
			thisItems.push({html: 'Target Hedge Ratio:', xtype: 'label'});
			Ext.each(pointList, function(f) {
				thisItems.push({name: 'targetHedgePercent_' + f.keyRateDurationPoint.id, value: f.targetHedgePercent, decimalPrecision: 2, submitValue: false});
			});
			thisItems.push({name: 'cashTotalPercent', decimalPrecision: 2});

			// Target Hedge Ratio
			thisItems.push({html: 'Target Hedge (Adj):', xtype: 'label'});
			Ext.each(pointList, function(f) {
				thisItems.push({name: 'targetHedgePercentAdjusted_' + f.keyRateDurationPoint.id, value: f.targetHedgePercentAdjusted, decimalPrecision: 2, submitValue: false});
			});
			thisItems.push({name: 'cashTotalPercent', decimalPrecision: 2});

			// Target DV01
			thisItems.push({html: 'Target DV01:', xtype: 'label'});
			Ext.each(pointList, function(f) {
				thisItems.push({name: 'targetDv01Value_' + f.keyRateDurationPoint.id, value: f.targetHedgeDv01Value, submitValue: false});
			});
			thisItems.push({name: 'cashTotal'});
			// Target DV01
			thisItems.push({html: 'Target DV01 (Adj):', xtype: 'label'});
			Ext.each(pointList, function(f) {
				thisItems.push({name: 'targetDv01ValueAdjsuted_' + f.keyRateDurationPoint.id, value: f.targetHedgeDv01ValueAdjusted, submitValue: false});
			});
			thisItems.push({name: 'cashTotal'});

			// Manager Assets DV01
			thisItems.push({html: 'Manager Assets:', xtype: 'label'});
			Ext.each(pointList, function(f) {
				thisItems.push({name: 'assetsDv01Value_' + f.keyRateDurationPoint.id, value: f.assetsDv01Value, submitValue: false});
			});
			thisItems.push({name: 'cashTarget'});

			// Target Exposure DV01
			thisItems.push({html: 'Target Exposure:', xtype: 'label'});
			Ext.each(pointList, function(f) {
				thisItems.push({name: 'exposureTargetDv01Value_' + f.keyRateDurationPoint.id, value: f.exposureTargetDv01Value, submitValue: false});
			});
			thisItems.push({name: 'overlayTargetTotal'});

			// Exposure DV01
			thisItems.push({html: 'Actual Exposure:', xtype: 'label'});
			Ext.each(pointList, function(f) {
				thisItems.push({name: 'exposureDv01Value_' + f.keyRateDurationPoint.id, value: f.exposureDv01Value, submitValue: false});
			});
			thisItems.push({name: 'overlayExposureTotal'});

			// Pending DV01
			thisItems.push({html: 'Pending Exposure:', xtype: 'label'});
			Ext.each(pointList, function(f) {
				thisItems.push({name: 'pendingExposureDv01Value_' + f.keyRateDurationPoint.id, submitValue: false});
			});
			thisItems.push({name: 'pendingOverlayExposureTotal'});

			thisItems.push({html: '<hr/>', xtype: 'label', colspan: this.columns, width: '99%'});

			thisItems.push({html: 'Difference:', xtype: 'label'});
			Ext.each(pointList, function(f) {
				thisItems.push({name: 'difference_' + f.keyRateDurationPoint.id, submitValue: false});
			});
			thisItems.push({name: 'cashExposure'});

			thisItems.push({html: 'Hedge Ratio Deviation from Total (%):', xtype: 'label'});
			Ext.each(pointList, function(f) {
				thisItems.push({name: 'percentDifference_' + f.keyRateDurationPoint.id, decimalPrecision: 2, submitValue: false});
			});
			thisItems.push({name: 'cashExposurePercent', decimalPrecision: 2});

			this.items = thisItems;
			TCG.form.FormPanelTable.prototype.initComponent.call(this, arguments);
		},

		defaults: {
			xtype: 'currencyfield',
			decimalPrecision: 0,
			readOnly: true,
			submitValue: false,
			width: '95%'
		}
	}]
};
