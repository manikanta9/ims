TCG.use('Clifton.portfolio.run.trade.BaseRunTradeWindow');

TCG.use('Clifton.portfolio.run.trade.BaseRunTradeForm');

TCG.use('Clifton.portfolio.run.ldi.trade.RunTradeDv01RecapFieldSet');

TCG.use('Clifton.portfolio.run.ldi.trade.RunTradeLDIExposureGrid');

TCG.use('Clifton.portfolio.run.ldi.RunManagerBalance');

Clifton.portfolio.run.ldi.trade.RunTradeLDIWindow = Ext.extend(Clifton.portfolio.run.trade.BaseRunTradeWindow, {

	dtoClassForBinding: 'com.clifton.portfolio.run.ldi.trade.PortfolioRunTradeLDI',

	additionalRunTabs: [
		Clifton.portfolio.run.ldi.RunManagerBalance
	],

	runTradeEntryForm: {
		xtype: 'portfolio-run-trade-form',

		standardListRequestedProperties: 'id|tradingClientAccount.number|tradingClientAccount.id|security.id|security.label|securityPrice|securityDirtyPrice|tradeSecurityPrice|tradeSecurityPriceDate|security.priceMultiplier|exchangeRate|tradeExchangeRate|tradeExchangeRateDate|actualContracts|currentContracts|pendingContracts|targetContracts|buyContracts|sellContracts|trade.tradeDestination.id|trade.tradeDestination.name|trade.executingBrokerCompany.id|trade.executingBrokerCompany.label|trade.holdingInvestmentAccount.id|trade.holdingInvestmentAccount.label|trade.tradeExecutionType.id|trade.tradeExecutionType.name',
		additionalListRequestedProperties: '',

		getListRequestedProperties: function() {
			return this.standardListRequestedProperties + this.additionalListRequestedProperties;
		},

		executeAccountNavigation: function(tabName) {
			this.tabName = tabName;
			const clientId = TCG.getValue('clientInvestmentAccount.id', this.getForm().formValues);
			const clz = 'Clifton.investment.account.AccountWindow';
			const w = this.getWindow();
			const id = w.getMainFormId();
			const cmpId = TCG.getComponentId(clz, id);
			TCG.createComponent(clz, {
				id: cmpId,
				params: {id: clientId, processingType: this.getFormValue('serviceProcessingType')},
				openerCt: this,
				defaultActiveTabName: tabName,
				listeners: {
					activate: function() {
						const o = this.win.items.get(0);
						const tabNameClicked = this.openerCt.tabName;
						if (tabNameClicked) {
							for (let i = 0; i < o.items.length; i++) {
								if (o.items.get(i).title === tabNameClicked) {
									o.setActiveTab(i);
									break;
								}
							}
						}
					}
				}
			});
		},

		items: [
			// Need to add id as a hidden field and submitValue = false so that id is not double submitted
			{name: 'id', xtype: 'hidden', submitValue: false},
			{
				xtype: 'panel',
				layout: 'column',
				items: [{
					columnWidth: .3,
					layout: 'form',
					defaults: {layout: 'form'},
					items: [
						{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: 1,
									items: [{fieldLabel: 'Portfolio Run', name: 'label', xtype: 'linkfield', detailIdField: 'id', detailPageClass: 'Clifton.portfolio.run.RunWindow'}]
								},
								{
									width: 100,
									items: [
										{
											text: 'Navigate to', xtype: 'button', style: {float: 'right'},
											menu: {
												defaults: {iconCls: 'account'},
												items: [{
													text: 'Client Account',
													handler: function() {
														TCG.getParentFormPanel(this).executeAccountNavigation('Info');
													}
												}, {
													text: 'Manager Accounts',
													handler: function() {
														TCG.getParentFormPanel(this).executeAccountNavigation('Manager Accounts');
													}
												}, {
													text: 'KRD Points',
													handler: function() {
														TCG.getParentFormPanel(this).executeAccountNavigation('KRD Points');
													}
												}, {
													text: 'Holdings',
													handler: function() {
														TCG.getParentFormPanel(this).executeAccountNavigation('Holdings');
													}
												}]
											}
										}
									]
								}
							]
						}
					]
				},
					{columnWidth: .02, items: [{html: '&nbsp;'}]},
					{columnWidth: .67, items: [Clifton.portfolio.run.ldi.trade.RunTradeDv01RecapFieldSet]}
				]
			},
			Clifton.portfolio.run.ldi.trade.RunTradeLDIExposureGrid
		]

	}

});


