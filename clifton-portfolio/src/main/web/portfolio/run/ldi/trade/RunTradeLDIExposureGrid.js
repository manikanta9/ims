// PORTFOLIO RUN (LDI) - TRADE CREATION WINDOW - EXPOSURE LIST FORM GRID
TCG.use('Clifton.portfolio.run.trade.BaseRunTradeDetailGrid');
Clifton.portfolio.run.ldi.trade.RunTradeLDIExposureGrid = Ext.applyIf({

	dtoClass: 'com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure',
	detailPageClass: 'Clifton.portfolio.run.ldi.ExposureWindow',
	urlPrefix: 'portfolioRunTradeLDI',
	dtoClassForBinding: 'com.clifton.portfolio.run.ldi.trade.PortfolioRunTradeLDI',

	height: 170,

	// These fields may not be edited and may just use default value, so we always need them submitted
	// Send pricing to the server so that if we are trading, we can update the trade prices on the replication
	alwaysSubmitFields: ['tradeSecurityPrice', 'tradeSecurityPriceDate', 'tradeExchangeRate', 'tradeExchangeRateDate', 'trade.executingBrokerCompany.id', 'trade.tradeDestination.id', 'trade.holdingInvestmentAccount.id', 'buyContracts', 'sellContracts', 'trade.tradeExecutionType.id'],
	doNotSubmitFields: ['approveTrade'],

	initComponent: function() {
		const panel = TCG.getParentFormPanel(this);
		const id = panel.getWindow().params.id;

		let url = 'portfolioRunLDIKeyRateDurationPointListByRun.json?runId=' + id + '&reportableOnly=false';
		url += '&requestedPropertiesRoot=data&requestedProperties=|keyRateDurationPoint.id|keyRateDurationPoint.marketDataField.name|keyRateDurationPoint.keyRateDurationPointLabel';

		const pointList = TCG.data.getData(url, this);
		const pointCols = [];
		let count = 0;

		const pointIds = [];

		let additionalRequestProperties = '';

		Ext.each(pointList, function(f) {
			pointIds.push(f.keyRateDurationPoint.id);
			const fieldName = f.keyRateDurationPoint.marketDataField.name;
			const dv01Name = f.keyRateDurationPoint.keyRateDurationPointLabel + ' DV01';

			additionalRequestProperties += '|keyRateDurationPointMap.KRD_' + f.keyRateDurationPoint.id;
			additionalRequestProperties += '|keyRateDurationPointMap.DV01Contract_' + f.keyRateDurationPoint.id;
			additionalRequestProperties += '|keyRateDurationPointMap.TradeDV01Contract_' + f.keyRateDurationPoint.id;

			pointCols.push({header: fieldName, width: 80, hidden: true, dataIndex: 'keyRateDurationPointMap.KRD_' + f.keyRateDurationPoint.id, type: 'currency', numberFormat: '0,000.000000'});
			pointCols.push({header: dv01Name + 'Contract Value (Previous Close)', hidden: true, width: 80, dataIndex: 'keyRateDurationPointMap.DV01Contract_' + f.keyRateDurationPoint.id, type: 'currency'});
			pointCols.push({header: dv01Name + 'Contract Value', hidden: true, width: 80, dataIndex: 'keyRateDurationPointMap.TradeDV01Contract_' + f.keyRateDurationPoint.id, type: 'currency'});
			pointCols.push({
				header: dv01Name, width: 80, type: 'currency', summaryType: 'sum', dataIndex: 'keyRateDurationPointMap.DV01_' + f.keyRateDurationPoint.id,
				calculateValue: function(r) {
					const current = r.data.currentContracts;
					const pending = r.data.pendingContracts;
					const buy = r.data.buyContracts;
					const sell = r.data.sellContracts;
					let total = current + pending + buy - sell;

					total = total * r.data['keyRateDurationPointMap.TradeDV01Contract_' + f.keyRateDurationPoint.id];
					return total;
				},
				renderer: function(v, metaData, r) {
					let qtip = '';
					const dv01Val = TCG.numberFormat(r.data['keyRateDurationPointMap.DV01Contract_' + f.keyRateDurationPoint.id], '0,000.0000');
					const tradeDv01Val = TCG.numberFormat(r.data['keyRateDurationPointMap.TradeDV01Contract_' + f.keyRateDurationPoint.id], '0,000.0000');

					if (dv01Val !== tradeDv01Val) {
						qtip += '<tr><td colspan="3" align="center"><b>DV01 Value of 1 Contract</b></td></tr>';
						qtip += '<tr><td colspan="2">Previous Close:</td><td align="right">' + TCG.renderAmount(dv01Val, false, '0,000.0000') + '</td></tr>';
						qtip += '<tr><td colspan="2">Current:</td><td align="right">' + TCG.renderAmount(tradeDv01Val, false, '0,000.0000') + '</td></tr>';
						qtip += '<tr><td colspan="3"><hr/></td></tr>';
						qtip += '<tr><td colspan="2">Difference:</td><td align="right">' + TCG.renderAmountArrow(tradeDv01Val > dv01Val) + TCG.renderAmount(tradeDv01Val - dv01Val, false, '0,000.00') + '</td></tr>';
						qtip += '<tr><td colspan="3">&nbsp;</td></tr>';
					}

					if (r.data.currentContracts !== r.data.actualContracts || r.data.pendingContracts || r.data.buyContracts || r.data.sellContracts) {
						qtip += '<tr><td colspan="3" align="center"><b>DV01 Breakdown</b></td></tr>';
						qtip += '<tr><td>&nbsp;</td><td align="right">Contracts</td><td align="right">DV01 Exposure</td></tr>';
						qtip += '<tr><td>Previous Close:</td><td align="right">' + TCG.renderAmount(r.data.actualContracts, false, '0,000') + '</td><td align="right">' + TCG.renderAmount(r.data.actualContracts * tradeDv01Val, false, '0,000.00') + '</td></tr>';

						if (r.data.currentContracts !== r.data.actualContracts) {
							qtip += '<tr><td>Booked Trades:</td><td align="right">' + TCG.renderAmount((r.data.currentContracts - r.data.actualContracts), false, '0,000') + '</td><td align="right">' + TCG.renderAmount((r.data.currentContracts - r.data.actualContracts) * tradeDv01Val, false, '0,000.00') + '</td></tr>';
						}
						if (r.data.pendingContracts) {
							qtip += '<tr><td>Pending Trades:</td><td align="right">' + TCG.renderAmount(r.data.pendingContracts, false, '0,000') + '</td><td align="right">' + TCG.renderAmount(r.data.pendingContracts * tradeDv01Val, false, '0,000.00') + '</td></tr>';
						}
						if (r.data.buyContracts) {
							qtip += '<tr><td>Trade Entry (Buy):</td><td align="right">' + TCG.renderAmount(r.data.buyContracts, false, '0,000') + '</td><td align="right">' + TCG.renderAmount(r.data.buyContracts * tradeDv01Val, false, '0,000.00') + '</td></tr>';
						}
						if (r.data.sellContracts) {
							qtip += '<tr><td>Trade Entry (Sell):</td><td align="right">' + TCG.renderAmount(r.data.sellContracts, false, '0,000') + '</td><td align="right">' + TCG.renderAmount(r.data.sellContracts * -1 * tradeDv01Val, false, '0,000.00') + '</td></tr>';
						}
					}

					if (TCG.isNotBlank(qtip)) {
						qtip = '<table>' + qtip + '</table>';
						metaData.css = 'amountAdjusted';
						metaData.attr = 'qtip=\'' + qtip + '\'';
					}
					return TCG.renderAmount(this.calculateValue(r), false, '0,000');
				},
				summaryCalculation: function(v, r, field, data, col) {
					return v + col.calculateValue(r);
				},
				summaryRenderer: function(v) {
					return TCG.renderAmount(v, false, '0,000');
				}
			});
			count++;
		});

		pointCols.push({
			header: 'KRD Total', width: 80, hidden: true, type: 'currency', numberFormat: '0,000.000000',
			renderer: function(v, metaData, r) {
				let total = 0;
				Ext.each(pointIds, function(f) {
					total += r.data['keyRateDurationPointMap.KRD_' + f];
				});
				return TCG.renderAmount(total, false, '0,000.000000');
			}
		});
		pointCols.push({
			header: 'DV01 Total', width: 80, type: 'currency', summaryType: 'sum',
			calculateValue: function(r) {
				const actual = r.data.currentContracts;
				const pending = r.data.pendingContracts;
				const buy = r.data.buyContracts;
				const sell = r.data.sellContracts;
				const totalContracts = actual + pending + buy - sell;

				let total = 0;
				Ext.each(pointIds, function(f) {
					total += (totalContracts * r.data['keyRateDurationPointMap.TradeDV01Contract_' + f]);
				});
				return total;
			},
			renderer: function(v, metaData, r) {
				return TCG.renderAmount(this.calculateValue(r), false, '0,000');
			},
			summaryCalculation: function(v, r, field, data, col) {
				return v + col.calculateValue(r);
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		});

		panel.additionalListRequestedProperties = additionalRequestProperties;

		const cols = [];

		this.firstColumns = TCG.clone(this.firstColumns);
		this.lastColumns = TCG.clone(this.lastColumns);

		Ext.each(this.firstColumns, function(f) {
			f.width = 30;
			cols.push(f);
		});
		Ext.each(this.detailColumns, function(f) {
			cols.push(f);
		});
		Ext.each(pointCols, function(f) {
			cols.push(f);
		});
		Ext.each(this.lastColumns, function(f) {
			f.width = 175;
			cols.push(f);
		});
		this.columnsConfig = cols;
		TCG.grid.FormGridPanel.prototype.initComponent.call(this, arguments);
	},

	updateMaximumHedgeRatioDeviationLimit: async function(formPanel) {
		const formValues = formPanel.getForm().formValues;
		const formGrid = this;
		await Clifton.system.GetCustomColumnValuePromise(formValues.clientInvestmentAccount.id, 'InvestmentAccount', 'Portfolio Setup',
			'Maximum Allowable Hedge Ratio Deviation %', true)
			.then(function(limitValue) {
				formGrid.maximumHedgeRatioDeviationLimit = parseFloat(limitValue);
			});
	},

	hedgeRatioDeviationExceedsLimit: function() {
		const formPanel = TCG.getParentFormPanel(this);
		const percentHedgeRatioDeviation = formPanel.getFormValue('cashExposurePercent', true, true);
		const limitViolated = Math.abs(percentHedgeRatioDeviation) > this.maximumHedgeRatioDeviationLimit;
		const errorMsg = limitViolated ? 'The calculated Hedge Ratio Deviation of ' + percentHedgeRatioDeviation + '% exceeds the maximum deviation limit of +/- ' + this.maximumHedgeRatioDeviationLimit + '%.' : '';
		return {isLimitViolation: limitViolated, errorMessage: errorMsg};
	},

	afterFormLoad: function(grid, panel) {
		grid.loadMispricing(true);
		if (TCG.isTrue(grid.getWindow().savedSinceOpen)) {
			grid.reloadTrades();
		}
		// Enable Drag And Drop Feature to Attached "Client Trade Direction" notes
		TCG.file.enableDD(grid, grid.getNoteParams(), null, 'tradeNoteForTradeGroupBySourceUpload.json', false);

		this.updateMaximumHedgeRatioDeviationLimit(panel);
	},

	/***********************************************************************
	 * GRID EDITING
	 * *********************************************************************/


	onAfterGrandTotalUpdateFieldValue: function(v, cf) {
		// NOTE: USE FormPanel setFormValue METHOD SO WINDOW IS NOT MARKED AS MODIFIED!
		if (cf.dataIndex === 'DV01 Total') {
			this.updateDv01Recap('Total', v);
		}
		if (cf.dataIndex.indexOf('keyRateDurationPointMap.DV01_') === 0) {
			let pointId = cf.dataIndex;
			pointId = pointId.replace('keyRateDurationPointMap.DV01_', '');
			this.updateDv01Recap(pointId, v);
		}
	},

	updateDv01Recap: function(pointId, total) {
		const f = TCG.getParentFormPanel(this);

		const totalLiabilitiesFieldName = 'portfolioTotalValue';
		let actualFieldName = 'overlayExposureTotal';
		let pendingFieldName = 'pendingOverlayExposureTotal';
		let targetExposureFieldName = 'overlayTargetTotal';
		let differenceFieldName = 'cashExposure';
		let percentDifferenceFieldName = 'cashExposurePercent';

		if (pointId !== 'Total') {
			actualFieldName = 'exposureDv01Value_' + pointId;
			pendingFieldName = 'pendingExposureDv01Value_' + pointId;
			targetExposureFieldName = 'exposureTargetDv01Value_' + pointId;
			differenceFieldName = 'difference_' + pointId;
			percentDifferenceFieldName = 'percentDifference_' + pointId;
		}

		const actual = f.getForm().findField(actualFieldName).getNumericValue();
		const pending = total - actual;

		const target = f.getForm().findField(targetExposureFieldName).getNumericValue();
		const difference = target - total;

		// percentDifference (Hedge Ratio Deviation from Total) calculation now updated to be ratio of difference to totalLiabilities
		const totalLiabilities = f.getForm().findField(totalLiabilitiesFieldName).getNumericValue();
		const percentDifference = difference / totalLiabilities * 100;

		f.setFormValue(pendingFieldName, Ext.util.Format.number(pending, '0,000'), true);
		f.setFormValue(differenceFieldName, Ext.util.Format.number(difference, '0,000'), true);
		f.setFormValue(percentDifferenceFieldName, Ext.util.Format.number(percentDifference, '0,000.00'), true);
	},

	/***********************************************************************
	 * TOOLBAR - ADD SECURITY BUTTON
	 * *********************************************************************/

	addButton: function(toolBar, gp, buttonName) {
		toolBar.add({
			text: 'Add Security',
			iconCls: 'add',
			tooltip: 'Uses selected LDI Exposure as a model and allows adding a new security.',
			scope: this,
			handler: async function() {
				if (TCG.isTrue(await gp.validateTradingAllowedRun())) {
					const index = gp.getSelectionModel().getSelectedCell();
					if (index) {
						const store = gp.getStore();
						const rec = store.getAt(index[0]);

						if (TCG.isTrue(rec.json.tradeEntryDisabled)) {
							TCG.showError('Cannot add a new security to a replication where Trading is Disabled.  Please add the security to a replication where trading is not disabled and it will automatically be added here.', 'Read Only');
							return;
						}

						const className = 'Clifton.portfolio.run.ldi.AddNewLDIExposureWindow';

						const id = rec.id;

						const cmpId = TCG.getComponentId(className, id);
						TCG.createComponent(className, {
							id: cmpId,
							params: {id: id},
							openerCt: gp
						});
					}
					else {
						TCG.showError('A row must be selected in order to add a new security to the run.  The selected LDI Exposure Row is used as a model and to define the replication the security belongs to.', 'No Row Selected');
					}
				}
			}

		});
		toolBar.add('-');
	},


	// Overridden validation to include the Hedge Ratio Deviation Limit validation
	validateTradingAllowedRun: async function() {
		// get the submit parameters
		const panel = TCG.getParentFormPanel(this);
		const st = TCG.getValue('tradingAllowed', panel.getForm().formValues);
		if (st !== true) {
			TCG.showError('This run record is not available for submitting trades.', 'Trading Prohibited');
			return false;
		}
		const hedgeRatioLimitViolationCheck = this.hedgeRatioDeviationExceedsLimit();
		if (hedgeRatioLimitViolationCheck.isLimitViolation) {
			const message = hedgeRatioLimitViolationCheck.errorMessage + ' Are you sure you want to proceed?';
			const answer = await TCG.showConfirm(message, 'Trade Rule Violation');
			if (answer === 'no') {
				return false;
			}
		}
		return true;
	},

	/***********************************************************************
	 * DETAIL COLUMN DEFINITIONS -
	 * *********************************************************************/

	groupField: 'tradingClientAccount.number',
	groupTextTpl: '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Positions" : "Position"]})',


	detailColumns: [
		{header: 'Trading Account', width: 100, dataIndex: 'tradingClientAccount.number', hidden: true},
		{header: 'SecurityID', width: 15, dataIndex: 'security.id', hidden: true},
		{header: 'Security', width: 225, dataIndex: 'security.label'},

		{header: 'Price (Previous Close)', hidden: true, width: 100, dataIndex: 'securityPrice', type: 'float'},
		{header: 'Dirty Price', hidden: true, width: 100, dataIndex: 'securityDirtyPrice', type: 'float', useNull: true},
		{
			header: 'Price', width: 100, dataIndex: 'tradeSecurityPrice', type: 'float',
			renderer: function(v, metaData, r) {
				if (r.data.tradeSecurityPrice) {
					const value = TCG.numberFormat(v, '0,000', true);
					let qtip = '';
					qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Security Price', r.data.securityPrice, r.data.tradeSecurityPrice, true, r.json.tradeSecurityPriceDate);
					if (TCG.isNotBlank(qtip)) {
						metaData.css = 'amountAdjusted';
						qtip += '</table>';
					}
					metaData.attr = 'qtip=\'' + qtip + '\'';
					return TCG.renderAmountArrow(r.data.tradeSecurityPrice > r.data.securityPrice) + value;
				}
				return TCG.numberFormat(r.data.securityPrice, '0,000', true);
			}
		},
		{header: 'Price Multiplier', width: 80, dataIndex: 'security.priceMultiplier', type: 'float'},
		{header: 'FX Rate (Previous Close)', hidden: true, width: 80, dataIndex: 'exchangeRate', type: 'float'},
		{
			header: 'FX Rate', width: 80, dataIndex: 'tradeExchangeRate', type: 'float',
			renderer: function(v, metaData, r) {
				if (r.data.tradeExchangeRate) {
					const value = TCG.numberFormat(v, '0,000', true);
					let qtip = '';
					qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'FX Rate', r.data.exchangeRate, r.data.tradeExchangeRate, true, r.json.tradeExchangeRateDate);
					if (TCG.isNotBlank(qtip)) {
						metaData.css = 'amountAdjusted';
						qtip += '</table>';
					}
					metaData.attr = 'qtip=\'' + qtip + '\'';
					return TCG.renderAmountArrow(r.data.tradeExchangeRate > r.data.exchangeRate) + value;
				}
				return TCG.numberFormat(r.data.exchangeRate, '0,000', true);
			}
		},

		{header: 'Actual Contracts (Previous Close)', hidden: true, width: 80, dataIndex: 'actualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
		{
			header: 'Actual Contracts', width: 80, dataIndex: 'currentContracts', type: 'int', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			tooltip: 'Total Contracts that have been booked to the General Ledger.',
			renderer: function(v, metaData, r) {
				const current = r.data.currentContracts;
				if (r.data.actualContracts !== current) {
					metaData.css = 'amountAdjusted';
					let qtip = '<table>';
					if (r.data.actualContracts !== r.data.currentContracts) {
						qtip += '<tr><td>Current (Actual):</td><td align="right">' + Ext.util.Format.number(r.data.currentContracts, '0,000') + '</td></tr>';
						qtip += '<tr><td>Previous Close (Actual):</td><td align="right">' + Ext.util.Format.number(r.data.actualContracts, '0,000') + '</td></tr>';
						qtip += '<tr><td>Change:</td><td align="right">' + TCG.renderAmount(r.data.currentContracts - r.data.actualContracts, true) + '</td></tr>';
					}
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return TCG.numberFormat(current, '0,000');
			}
		},
		{
			header: 'Pending Contracts', width: 80, dataIndex: 'pendingContracts', type: 'int', summaryType: 'sum', negativeInRed: true, useNull: true, hideGrandTotal: true,
			tooltip: 'Pending Trades (Not Closed) + Pending Transfers (Unbooked Transfers To/From this account)',
			renderer: function(value, metadata, record) {
				const grid = this.gridPanel;
				if (grid) {
					grid.applyPendingTradesTooltipToColumn.call(grid, value, metadata, record);
				}
				return TCG.renderAmount(value, false, '0,000', true);
			}
		},
		{
			header: 'Target Contracts', width: 80, dataIndex: 'targetContracts', type: 'int', summaryType: 'sum', negativeInRed: true, useNull: true, hideGrandTotal: true,
			tooltip: 'Target contracts for this security in order to reach the Exposure Target.'
		},
		{
			header: 'Difference', width: 85, dataIndex: 'targetDifference', type: 'int', summaryType: 'sum', negativeInRed: true, useNull: true, hideGrandTotal: true,
			tooltip: 'The difference between target contracts and the sum of actual contracts and pending contracts. It is an indicator as to how many contracts to buy or sell (if negative) to reach the target contract value.',
			renderer: function(v, metaData, r) {
				if (TCG.isNotBlank(r.data.targetContracts)) {
					const current = TCG.isNotBlank(r.data.currentContracts) ? r.data.currentContracts : 0;
					const pending = TCG.isNotBlank(r.data.pendingContracts) ? r.data.pendingContracts : 0;
					const difference = r.data.targetContracts - (current + pending);
					return TCG.numberFormat(difference, '0,000');
				}
				return '';
			}
		},
		// Trading Fields
		{
			header: 'Buy', width: 70, dataIndex: 'buyContracts', type: 'int', useNull: true, editor: {xtype: 'spinnerfield', allowBlank: true, minValue: 1, maxValue: 1000000000}, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			allViews: true,
			renderer: function(value, metaData, r) {
				if (TCG.isNotNull(value) && value !== 0) {
					metaData.css = 'buy-dark';
				}
				return TCG.numberFormat(value, '0,000');
			},

			summaryRenderer: function(value) {
				return TCG.numberFormat(value, '0,000');
			}
		},
		{
			header: 'Sell', width: 70, dataIndex: 'sellContracts', type: 'int', useNull: true, editor: {xtype: 'spinnerfield', allowBlank: true, minValue: 1, maxValue: 1000000000}, summaryType: 'sum', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid; BORDER-RIGHT: #c0c0c0 1px solid;',
			renderer: function(value, metaData, r) {
				if (TCG.isNotNull(value) && value !== 0) {
					metaData.css = 'sell-dark';
				}
				return TCG.numberFormat(value, '0,000');
			},
			summaryRenderer: function(value) {
				return TCG.numberFormat(value, '0,000');
			}
		}

	]

}, Clifton.portfolio.run.trade.BaseRunTradeDetailGrid);


