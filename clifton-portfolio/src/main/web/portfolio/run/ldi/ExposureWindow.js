Clifton.portfolio.run.ldi.ExposureWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'LDI Security Exposure',
	iconCls: 'manager',
	width: 650,
	height: 650,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Details',
				items: [
					{
						xtype: 'formpanel',
						loadValidation: false,
						readOnly: true,
						url: 'portfolioRunLDIExposure.json',
						labelWidth: 125,

						items: [
							{fieldLabel: 'Client Account', name: 'portfolioRun.clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'portfolioRun.clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
							{
								fieldLabel: 'Trading Account',
								qtip: 'Trading Account or Position Account.  This client account these specific contracts come from/are traded in.',
								name: 'tradingClientAccount.label', xtype: 'linkfield', detailIdField: 'tradingClientAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'
							},
							{fieldLabel: 'Balance Date', name: 'portfolioRun.balanceDate', xtype: 'datefield'},
							{fieldLabel: 'Replication', name: 'replicationAllocation.replication.label', xtype: 'linkfield', detailIdField: 'replicationAllocation.replication.id', detailPageClass: 'Clifton.investment.replication.ReplicationWindow'},
							{fieldLabel: 'Security', name: 'security.label', xtype: 'linkfield', detailIdField: 'security.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
							{fieldLabel: 'Actual Contracts', xtype: 'currencyfield', name: 'actualContracts'},
							{fieldLabel: 'Price', xtype: 'floatfield', name: 'securityPrice'},
							{fieldLabel: 'Dirty Price', xtype: 'floatfield', name: 'securityDirtyPrice'},
							{fieldLabel: 'Price Multiplier', xtype: 'floatfield', name: 'security.priceMultiplier'},
							{fieldLabel: 'FX Rate', xtype: 'floatfield', name: 'exchangeRate'},
							{
								title: 'Managed Points',
								xtype: 'formgrid',
								storeRoot: 'keyRateDurationPointList',
								dtoClass: 'com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccountKeyRateDurationPoint',
								viewConfig: {forceFit: true},
								readOnly: true,
								useFilters: true,
								columnsConfig: [
									{header: 'KRD Point', width: 200, dataIndex: 'keyRateDurationPoint.marketDataField.name'},
									{header: 'KRD Value', width: 100, dataIndex: 'krdValue', type: 'currency', numberFormat: '0,000.000000', summaryType: 'sum'},
									{header: 'DV01 Value', width: 100, dataIndex: 'dv01ActualExposureValue', type: 'currency', summaryType: 'sum'},
									{header: 'Reportable Only', dataIndex: 'keyRateDurationPoint.reportableOnly', width: 100, type: 'boolean', hidden: true, sortable: false, filter: {active: true, value: false, disabled: true}}
								],
								plugins: {ptype: 'groupsummary'}
							},
							{
								title: 'Reportable Points',
								xtype: 'formgrid',
								storeRoot: 'keyRateDurationPointList',
								dtoClass: 'com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccountKeyRateDurationPoint',
								viewConfig: {forceFit: true, emptyText: 'This LDI Security Exposure record does not have Reportable points different than the Managed points.', deferEmptyText: false},
								readOnly: true,
								useFilters: true,
								collapsed: true,
								columnsConfig: [
									{header: 'KRD Point', width: 200, dataIndex: 'keyRateDurationPoint.marketDataField.name'},
									{header: 'KRD Value', width: 100, dataIndex: 'krdValue', type: 'currency', numberFormat: '0,000.000000', summaryType: 'sum'},
									{header: 'DV01 Value', width: 100, dataIndex: 'dv01ActualExposureValue', type: 'currency', summaryType: 'sum'},
									{header: 'Reportable Only', dataIndex: 'keyRateDurationPoint.reportableOnly', width: 100, type: 'boolean', hidden: true, sortable: false, filter: {active: true, value: true, disabled: true}}
								],
								plugins: {ptype: 'groupsummary'}
							}
						]
					}
				]
			},

			{
				title: 'Pending Trades',
				items: [{xtype: 'run-replication-pending-trade-gridpanel'}]
			},

			{
				title: 'Pending Transfers',
				items: [{xtype: 'run-replication-pending-transfer-gridpanel'}]
			}
		]
	}]
});
