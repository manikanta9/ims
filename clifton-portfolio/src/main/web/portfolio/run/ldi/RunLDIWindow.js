TCG.use('Clifton.portfolio.run.BaseRunWindow');

TCG.use('Clifton.portfolio.run.ldi.RunManagerAccount');
TCG.use('Clifton.portfolio.run.ldi.RunKeyRateDurationPoint');
TCG.use('Clifton.portfolio.run.ldi.RunExposure');
TCG.use('Clifton.portfolio.run.ldi.RunManagerBalance');
TCG.use('Clifton.portfolio.run.RunMargin');

Clifton.portfolio.run.ldi.RunLDIWindow = Ext.extend(Clifton.portfolio.run.BaseRunWindow, {

	additionalRunItems: [
		{xtype: 'sectionheaderfield', header: 'DV01 Recap', fieldLabel: ''},
		{
			xtype: 'formtable',
			columns: 5,
			defaults: {
				xtype: 'currencyfield',
				decimalPrecision: 0,
				readOnly: true,
				submitValue: false,
				width: '95%'
			},
			items: [
				{cellWidth: '140px', xtype: 'label'},
				{html: 'Target DV01', qtip: 'Target DV01 across all points', xtype: 'label', style: 'text-align: right'},
				{html: 'Manager Assets DV01', qtip: 'Sum of Manager Assets DV01 across all points', xtype: 'label', style: 'text-align: right'},
				{html: 'Exposure DV01', qtip: 'Sum of Exposure DV01 across all positions and KRD Points (actual positions booked to General Ledger)', xtype: 'label', style: 'text-align: right'},
				{html: 'Net DV01', qtip: 'Target DV01 - Manager Assets DV01 - Exposure DV01. Percentage value is capped at +/-9,999.9999.', xtype: 'label', style: 'text-align: right'},

				{html: 'Amount:', xtype: 'label'},
				{name: 'cashTotal'},
				{name: 'cashTarget'},
				{name: 'overlayExposureTotal'},
				{name: 'cashExposure'},

				{html: '% of Liabilities DV01:', xtype: 'label'},
				{name: 'cashTotalPercent', decimalPrecision: 4},
				{name: 'cashTargetPercent', decimalPrecision: 4},
				{name: 'overlayExposureTotalPercent', decimalPrecision: 4},
				{name: 'cashExposurePercent', decimalPrecision: 4},

				{cellWidth: '120px', xtype: 'label', colspan: 3},
				{html: 'Liabilities DV01:', xtype: 'label', style: 'text-align: right', qtip: 'Total Liabilities DV01 across all points'},
				{name: 'portfolioTotalValue'}
			]
		}
	],

	additionalRunTabs: [
		Clifton.portfolio.run.ldi.RunManagerAccount,
		Clifton.portfolio.run.ldi.RunKeyRateDurationPoint,
		Clifton.portfolio.run.ldi.RunExposure,
		Clifton.portfolio.run.RunAccountNotes,
		Clifton.portfolio.run.RunMargin,
		Clifton.portfolio.run.ldi.RunManagerBalance,
		Clifton.portfolio.run.RunPositions,
		Clifton.portfolio.run.RunPortalFiles
	]
});
