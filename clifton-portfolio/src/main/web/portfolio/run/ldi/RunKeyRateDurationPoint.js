Clifton.portfolio.run.ldi.RunKeyRateDurationPoint = {
	title: 'KRD Point Summary',
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},

	items: [
		{
			title: 'Managed Points',
			xtype: 'portfolio-run-ldi-key-rate-duration-point-gridpanel',
			border: 1,
			flex: 1,
			reportableOnly: false
		},

		{flex: 0.02},

		{
			title: 'Reportable Points',
			xtype: 'portfolio-run-ldi-key-rate-duration-point-gridpanel',
			viewConfig: {forceFit: true, emptyText: 'This Portfolio Run record does not have Reportable points different than the Managed points.', deferEmptyText: false},
			border: 1,
			flex: 1,
			reportableOnly: true
		}
	]
};
