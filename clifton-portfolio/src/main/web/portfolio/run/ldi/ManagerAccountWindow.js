Clifton.portfolio.run.ldi.ManagerAccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'LDI Manager Account',
	iconCls: 'manager',
	width: 650,
	height: 650,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		readOnly: true,
		url: 'portfolioRunLDIManagerAccount.json',
		labelWidth: 135,

		items: [
			{fieldLabel: 'Portfolio Run', name: 'portfolioRun.label', xtype: 'linkfield', detailIdField: 'portfolioRun.id', detailPageClass: 'Clifton.portfolio.run.RunWindow'},
			{fieldLabel: 'Client Account', name: 'portfolioRun.clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'portfolioRun.clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{fieldLabel: 'Manager Account', name: 'managerAccountAssignment.managerAccountLabel', xtype: 'linkfield', detailIdField: 'managerAccountAssignment.referenceOne.id', detailPageClass: 'Clifton.investment.manager.AccountWindow'},
			{fieldLabel: 'Balance Date', name: 'portfolioRun.balanceDate', xtype: 'datefield'},
			{fieldLabel: 'Benchmark', name: 'benchmarkSecurity.label', xtype: 'linkfield', detailIdField: 'benchmarkSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
			{fieldLabel: 'Cash Flow Group History', name: 'cashFlowGroupHistory.label', xtype: 'linkfield', detailIdField: 'cashFlowGroupHistory.id', detailPageClass: 'Clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistoryWindow'},
			{fieldLabel: 'Duration Field Override', name: 'durationFieldNameOverride', xtype: 'system-list-combo', listName: 'Portfolio: Duration Field', qtip: 'Optional duration field name override to use for the Benchmark Duration. If blank, \'Duration\' is used.'},
			{xtype: 'label', html: '<hr />'},
			{fieldLabel: 'Securities', xtype: 'currencyfield', name: 'securitiesTotal'},
			{fieldLabel: 'Cash', xtype: 'currencyfield', name: 'cashTotal'},
			{fieldLabel: 'Total', xtype: 'currencyfield', name: 'totalValue'},
			{
				title: 'Managed Points',
				xtype: 'formgrid',
				storeRoot: 'keyRateDurationPointList',
				dtoClass: 'com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccountKeyRateDurationPoint',
				viewConfig: {forceFit: true},
				readOnly: true,
				useFilters: true,
				columnsConfig: [
					{header: 'KRD Point', width: 200, dataIndex: 'keyRateDurationPoint.marketDataField.name'},
					{header: 'KRD Value', width: 100, dataIndex: 'krdValue', type: 'currency', numberFormat: '0,000.000000', summaryType: 'sum'},
					{header: 'DV01 Value', width: 100, dataIndex: 'dv01Value', type: 'currency', summaryType: 'sum'},
					{header: 'Reportable Only', dataIndex: 'keyRateDurationPoint.reportableOnly', width: 100, type: 'boolean', hidden: true, sortable: false, filter: {active: true, value: false, disabled: true}}
				],
				plugins: {ptype: 'groupsummary'}
			},
			{
				title: 'Reportable Points',
				xtype: 'formgrid',
				storeRoot: 'keyRateDurationPointList',
				dtoClass: 'com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccountKeyRateDurationPoint',
				viewConfig: {forceFit: true, emptyText: 'This Manager Account record does not have Reportable points different than the Managed points.', deferEmptyText: false},
				readOnly: true,
				useFilters: true,
				collapsed: true,
				columnsConfig: [
					{header: 'KRD Point', width: 200, dataIndex: 'keyRateDurationPoint.marketDataField.name'},
					{header: 'KRD Value', width: 100, dataIndex: 'krdValue', type: 'currency', numberFormat: '0,000.000000', summaryType: 'sum'},
					{header: 'DV01 Value', width: 100, dataIndex: 'dv01Value', type: 'currency', summaryType: 'sum'},
					{header: 'Reportable Only', dataIndex: 'keyRateDurationPoint.reportableOnly', width: 100, type: 'boolean', hidden: true, sortable: false, filter: {active: true, value: true, disabled: true}}
				],
				plugins: {ptype: 'groupsummary'}
			}
		]
	}]
});
