Clifton.portfolio.run.ldi.RunExposure = {
	title: 'Position Exposure',

	listeners: {
		beforerender: function() {
			this.add(this.expItem);
		}
	},

	items: [],
	expItem: {
		name: 'portfolioRunLDIExposureListFind',
		additionalPropertiesToRequest: 'keyRateDurationPointList|keyRateDurationPointMap',
		xtype: 'gridpanel',
		instructions: 'Selected client account has the following position key rate duration point allocations for each security.',
		appendStandardColumns: false,
		viewConfig: {forceFit: true, emptyText: '', deferEmptyText: true},

		getTopToolbarFilters: function(toolbar) {
			return [
				'-',
				{boxLabel: 'Reportable Only', xtype: 'toolbar-checkbox', name: 'reportableOnlyToolbarCheckbox'}
			];
		},

		getLoadParams: function() {
			const reportableOnly = TCG.isTrue(TCG.getChildByName(this.getTopToolbar(), 'reportableOnlyToolbarCheckbox').getValue());
			this.grid.getView().emptyText = reportableOnly ? 'This Portfolio Run does not have Position Exposure records with Reportable points different than the Managed points.' : '';
			this.grid.getView().applyEmptyText();

			return {
				runId: this.getWindow().getMainForm().formValues.id,
				populatePoints: true,
				reportableOnly: reportableOnly
			};
		},


		remoteSort: true,
		isPagingEnabled: function() {
			return false;
		},
		pageSize: 1000,

		groupField: 'tradingClientAccount.number',
		groupTextTpl: '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Positions" : "Position"]})',

		listeners: {
			afterrender: function(gridPanel) {
				this.pointIdSet = new Set();
				this.grid.store.on('load', () => this.updateDynamicColumns());
			}
		},

		pointIdSet: null,
		updateDynamicColumns: function() {
			const krdPointCols = [];
			const dv01PointCols = [];
			let pointIds = this.pointIdSet;
			const currentLoadPointIdSet = new Set();

			const viewName = this.currentViewName || this.defaultViewName || this.viewNames[0];
			const grid = this.grid;
			grid.store.each(runLDIExposure => {
				try {
					runLDIExposure.beginEdit();
					Ext.each(runLDIExposure.json.keyRateDurationPointList, point => {
						currentLoadPointIdSet.add(point.keyRateDurationPoint.id);
						if (!pointIds.has(point.keyRateDurationPoint.id)) {
							pointIds.add(point.keyRateDurationPoint.id);
							const fieldName = point.keyRateDurationPoint.marketDataField.name;
							const dv01Name = point.keyRateDurationPoint.keyRateDurationPointLabel + ' DV01';
							krdPointCols.push({header: fieldName, width: 100, dataIndex: 'keyRateDurationPointMap.KRD_' + point.keyRateDurationPoint.id, type: 'currency', numberFormat: '0,000.000000', viewNames: ['KRD Values'], hidden: viewName !== 'KRD Values', align: 'right'});
							dv01PointCols.push({
								header: dv01Name, width: 100, dataIndex: 'keyRateDurationPointMap.DV01_' + point.keyRateDurationPoint.id, type: 'currency', summaryType: 'sum', viewNames: ['DV01 Values'], hidden: viewName !== 'DV01 Values', align: 'right',
								renderer: (value, metadata, record) => Ext.util.Format.number(value, '0,000')
							});
						}
						runLDIExposure.set('keyRateDurationPointMap.KRD_' + point.keyRateDurationPoint.id, point.krdValue || 0);
						runLDIExposure.set('keyRateDurationPointMap.DV01_' + point.keyRateDurationPoint.id, point.dv01ActualExposureValue || 0);
					});
				}
				catch (e) {
					console.warn(e);
				}
				finally {
					runLDIExposure.commit();
				}
			});

			if (currentLoadPointIdSet.size < 1) {
				grid.getStore().removeAll(); // show emptyText message
			}

			pointIds.forEach(pointId => {
				if (!currentLoadPointIdSet.has(pointId)) {
					grid.removeColumn('keyRateDurationPointMap.KRD_' + pointId);
					grid.removeColumn('keyRateDurationPointMap.DV01_' + pointId);
				}
			});
			grid.removeColumn('krdTotal');
			grid.removeColumn('dv01Total');
			pointIds = currentLoadPointIdSet;
			this.pointIdSet = currentLoadPointIdSet;

			krdPointCols.push({
				header: 'KRD Total', dataIndex: 'krdTotal', width: 100, type: 'currency', numberFormat: '0,000.000000', viewNames: ['DV01 Values', 'KRD Values'], align: 'right',
				renderer: function(v, metaData, r) {
					let total = 0;
					pointIds.forEach(pointId => {
						total += r.data['keyRateDurationPointMap.KRD_' + pointId];
					});
					return TCG.renderAmount(total, false, '0,000.000000');
				}
			});
			dv01PointCols.push({
				header: 'DV01 Total', dataIndex: 'dv01Total', width: 100, type: 'currency', viewNames: ['DV01 Values'], summaryType: 'sum', numberFormat: '0,000', hidden: viewName !== 'DV01 Values', align: 'right',
				renderer: function(v, metaData, r) {
					let total = 0;
					pointIds.forEach(pointId => {
						total += r.data['keyRateDurationPointMap.DV01_' + pointId];
					});
					return TCG.renderAmount(total, false, '0,000');
				}
			});

			Ext.each(krdPointCols, function(krdPointColumn) {
				grid.addColumn(krdPointColumn);
			});
			Ext.each(dv01PointCols, function(dv01PointColumn) {
				grid.addColumn(dv01PointColumn);
			});
		},


		viewNames: ['DV01 Values', 'KRD Values'],

		columns: [
			{header: 'Trading Account', width: 200, dataIndex: 'tradingClientAccount.number', hidden: true},
			{header: 'Run Exposure ID', width: 30, dataIndex: 'id', hidden: true},
			{header: 'Security', width: 200, dataIndex: 'security.label'},
			{header: 'Price', width: 100, dataIndex: 'securityPrice', type: 'float'},
			{header: 'Dirty Price', width: 100, dataIndex: 'securityDirtyPrice', type: 'float', useNull: true},
			{header: 'FX Rate', width: 100, dataIndex: 'exchangeRate', type: 'float', hidden: true},
			{header: 'Price Multiplier', width: 100, dataIndex: 'security.priceMultiplier', type: 'float'},
			{header: 'Actual Contracts', width: 100, dataIndex: 'actualContracts', type: 'currency', numberFormat: '0,000', negativeInRed: true},
			{header: 'KRD Date', width: 100, dataIndex: 'krdValueDate', allViews: true}
		],
		plugins: {ptype: 'groupsummary'},
		editor: {
			detailPageClass: 'Clifton.portfolio.run.ldi.ExposureWindow',
			drillDownOnly: true
		}

	}
};
