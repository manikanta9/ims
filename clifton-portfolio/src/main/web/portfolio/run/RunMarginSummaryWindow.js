Clifton.portfolio.run.RunMarginSummaryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Run Margin Summary',
	iconCls: 'list',
	width: 750,
	height: 450,

	items: [{
		xtype: 'formpanel',
		readOnly: true,
		labelWidth: 200,
		instructions: 'Display summary of run margin values.',
		url: 'portfolioRunMargin.json?requestedMaxDepth=4',
		items: [
			{
				fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
				defaults: {
					xtype: 'displayfield',
					flex: 1
				},
				items: [
					{value: 'Amount'},
					{name: 'percentageLabel'}
				]
			},
			{
				fieldLabel: 'Parametric Clifton Account Value', xtype: 'compositefield',
				defaults: {
					xtype: 'currencyfield',
					flex: 1
				},
				items: [
					{name: 'ourAccountValue'},
					{name: 'ourAccountValuePercent'}
				]
			},
			{
				fieldLabel: 'Required Initial Margin', xtype: 'compositefield',
				defaults: {
					xtype: 'currencyfield',
					flex: 1
				},
				items: [
					{name: 'marginInitialValue'},
					{name: 'marginInitialValuePercent'}
				]
			},
			{xtype: 'label', html: '<hr>'},
			{
				fieldLabel: 'Variation Margin', xtype: 'compositefield',
				defaults: {
					xtype: 'currencyfield',
					flex: 1
				},
				items: [
					{name: 'marginVariationValue'},
					{name: 'marginVariationValuePercent'}
				]
			},
			{
				fieldLabel: 'Recommended Variation Margin', xtype: 'compositefield',
				defaults: {
					xtype: 'currencyfield',
					flex: 1
				},
				items: [
					{name: 'marginRecommendedValue'},
					{name: 'marginRecommendedValuePercent'}
				]
			},
			{xtype: 'label', html: '<hr>'},
			{
				fieldLabel: 'Surplus Variation Margin', xtype: 'compositefield',
				defaults: {
					xtype: 'currencyfield',
					flex: 1
				},
				items: [
					{name: 'marginVariationValueLessRecommended'},
					{name: 'marginVariationValueLessRecommendedPercent'}
				]
			},
			{xtype: 'label', html: '&nbsp;'},

			{
				fieldLabel: 'Posted Broker Collateral', xtype: 'compositefield',
				defaults: {
					xtype: 'currencyfield',
					flex: 1
				},
				items: [
					{name: 'brokerCollateralValue'},
					{xtype: 'label', html: '&nbsp;'}
				]
			},
			{
				fieldLabel: 'Required Initial Margin', xtype: 'compositefield',
				defaults: {
					xtype: 'currencyfield',
					flex: 1
				},
				items: [
					{name: 'marginInitialValue'},
					{xtype: 'label', html: '&nbsp;'}
				]
			},
			{xtype: 'label', html: '<hr>'},
			{
				fieldLabel: 'Excess Broker Collateral', xtype: 'compositefield',
				defaults: {
					xtype: 'currencyfield',
					flex: 1
				},
				items: [
					{name: 'excessBrokerCollateralValue'},
					{xtype: 'label', html: '&nbsp;'}
				]
			}
		]
	}]
});
