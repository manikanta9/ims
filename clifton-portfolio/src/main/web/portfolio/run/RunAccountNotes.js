Clifton.portfolio.run.RunAccountNotes = {
	title: 'Account Notes',
	items: [{
		xtype: 'document-system-note-grid',
		tableName: 'InvestmentAccount',
		showOrderInfo: true,

		getEntityId: function() {
			return TCG.getValue('clientInvestmentAccount.id', this.getWindow().getMainForm().formValues);
		}
	}]
};
