TCG.use('Clifton.portfolio.run.RunAccountNotes');
TCG.use('Clifton.portfolio.run.RunPositions');
TCG.use('Clifton.portfolio.run.RunPortalFiles');

Clifton.portfolio.run.BaseRunWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Portfolio Run',
	iconCls: 'run',
	width: 1200,
	height: 700,
	enableRefreshWindow: true,

	// Additional Form items to the first run tab - i.e. for Overlay shows Cash Exposure Recap Section
	additionalRunItems: undefined,
	// Tabs to add to the Run Window - by default it is just Run & Warnings because although some tabs are re-used the order is different
	additionalRunTabs: undefined,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		if (w.additionalRunTabs) {
			Ext.each(this.additionalRunTabs, function(f) {
				tabs.add(f);
			});
		}
	},


	getFormPanels: function(modified) {
		let items = this.items;
		if (items.length === 1 && items.get(0).xtype === 'tabpanel') {
			items = items.get(0).items;
		}
		const result = [];
		items.each(function(i) {
			if (i.getForm && i.controlWindowModified === true && (!modified || i.getForm().isDirty())) {
				result[result.length] = i;
			}
			else if (i.items) {
				i.items.each(function(o) {
					if (o.getForm && o.controlWindowModified === true && (!modified || o.getForm().isDirty())) {
						result[result.length] = o;
					}
				});
			}
		});
		return result;
	},


	overrideRunItems: function(formPanel, runItems) {
		// hook to be used by extended instances to override run items of the window's formpanel during initialization
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,
		items: [
			{
				title: 'Run',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'PortfolioRun',
					finalState: 'Completed',
					reloadFormPanelAfterTransition: true,
					hideSystemDefinedTransitions: true
				},
				items: [{
					xtype: 'formpanel-custom-json-fields',
					loadDefaultDataAfterRender: true,
					url: 'portfolioRun.json',
					columnGroupName: 'Portfolio Run Custom Fields',
					defaults: {anchor: '-15'},
					getWarningMessage: function(form) {
						return Clifton.rule.violation.getRuleViolationWarningMessage('run', form.formValues);
					},

					listeners: {
						aftercreate: function(panel, closeOnSuccess) {
							if (closeOnSuccess === false) {
								const tb = panel.ownerCt.getTopToolbar();
								for (let i = 2; i < tb.items.length; i++) {
									const tran = tb.items.get(i).transition;
									if (tran && TCG.isEquals('Processing', tran.endWorkflowState.name)) {
										tb.doTransition(tran);
									}
								}
							}
						}
					},

					executeTradeAction: function(className) {
						const f = this;
						const w = this.getWindow();
						const incomplete = TCG.getValue('incomplete', f.getForm().formValues);
						if (TCG.isFalse(incomplete)) {
							const clz = 'Clifton.portfolio.run.trade.RunTradeWindow';
							const id = w.getMainFormId();
							const cmpId = TCG.getComponentId(clz, id);
							TCG.createComponent(clz, {
								id: cmpId,
								params: {id: id, processingType: this.getFormValue('serviceProcessingType')},
								className: className,
								openerCt: this,
							});
						}
						else {
							TCG.showError('This Portfolio run is not ready for trade creation/viewing.  Please complete processing and finalize run.', 'Not Available');
						}
					},


					executeReport: function(regenerate, format, excludePrivate) {
						const w = this.getWindow();
						const id = w.getMainFormId();
						Clifton.portfolio.run.downloadPortfolioReport(id, this, regenerate ? regenerate : false, format, excludePrivate);
					},

					reload: function() {
						const w = this.getWindow();
						this.getForm().setValues(TCG.data.getData('portfolioRun.json?id=' + w.getMainFormId(), this), true);
						this.fireEvent('afterload', this);
					},

					initComponent: function() {
						const currentItems = [];
						const instanceRunItems = TCG.clone(this.runItems);
						if (this.getWindow().overrideRunItems) {
							this.getWindow().overrideRunItems(this, instanceRunItems);
						}
						Ext.each(instanceRunItems, function(f) {
							currentItems.push(f);
						});
						if (this.getWindow().additionalRunItems) {
							Ext.each(this.getWindow().additionalRunItems, function(f) {
								currentItems.push(f);
							});
						}
						currentItems.push(this.fileAttachmentsGrid);
						this.items = currentItems;
						TCG.form.FormPanel.superclass.initComponent.call(this);

					},

					runItems: [
						{
							xtype: 'formtable',
							columns: 5,
							items: [
								{html: 'Client Account:', cellWidth: '115px'},
								{
									colspan: 2, name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.id',
									detailPageClass: 'Clifton.investment.account.AccountWindow',
									createDetailClass: function() {
										const p = this.getParentForm();
										const w = p.getWindow();
										const className = this.getDetailPageClass(p);
										const id = this.getDetailIdFieldValue(p);
										const cmpId = className + '-' + id + '-RunId-' + w.getMainFormId();

										const dd = {
											clientInvestmentAccount: this.getParentForm().getFormValue('clientInvestmentAccount'),
											processingType: this.getParentForm().getFormValue('serviceProcessingType'),
											defaultDataIsReal: true,
											id: this.detailPageClass + this.ownerCt.ownerCt.getWindow().getMainFormId()
										};
										const args = {
											id: cmpId,
											params: {id: id},
											defaultData: dd,
											defaultIconCls: w.iconCls,
											openerCt: p // for modal new item window will call reload() on close
										};

										TCG.createComponent(className, args);
									}
								},
								{html: 'Service:', cellWidth: '100px'},
								{name: 'clientInvestmentAccount.coalesceBusinessServiceGroupName', cellWidth: '160px', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.businessService.id', detailPageClass: 'Clifton.business.service.ServiceWindow'},

								{html: 'Balance Date:'},
								{name: 'balanceDate', xtype: 'datefield', skipValidation: true}, // skip validation (server-side will catch it) to avoid scroll bar
								{html: ''},
								{html: 'Processing Type:'},
								{fieldLabel: '', name: 'serviceProcessingType', xtype: 'displayfield', tooltip: 'Processing type used at the time of the run. This drives portfolio setup, processing components and UI components.'},

								{html: '', colspan: 3},
								{html: 'Team:'},
								{name: 'clientInvestmentAccount.teamSecurityGroup.name', xtype: 'linkfield', detailIdField: 'clientInvestmentAccount.teamSecurityGroup.id', detailPageClass: 'Clifton.investment.instrument.SecurityGroupWindow'},

								{html: '', colspan: 3},
								{html: 'Roles:'},
								{
									xtype: 'linkfield', value: 'Account Contacts', detailIdField: 'clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow',
									getDefaultData: function() {
										return {defaultActiveTabName: 'Contacts'};
									}
								}
							]
						},
						{
							name: 'mainRun',
							boxLabel: 'Main Run for the Balance Date (Used for Daily Comparisons & Performance Summaries)',
							qtip: 'Main Runs are also used for Billing and AUM if the account is configured to use run data in its calculations.',
							xtype: 'checkbox',
							mutuallyExclusiveFields: ['marketOnCloseAdjustmentsIncluded']
						},
						{name: 'marketOnCloseAdjustmentsIncluded', boxLabel: 'Include MOC (Market On Close) adjustments in this Run', xtype: 'checkbox', mutuallyExclusiveFields: ['mainRun']},
						{name: 'postedToPortal', boxLabel: 'Posted to Portal', xtype: 'checkbox', doNotSubmitValue: true, disabled: true, qtip: 'If checked, there is at least one approved file on the Portal associated with this run.  Click Portal Files button to view more information.'},
						{
							xtype: 'fieldset',
							title: 'Run Processing',
							buttons: [
								{
									text: 'Trade Creation',
									xtype: 'button',
									iconCls: 'shopping-cart',
									width: 120,
									handler: function() {
										const f = TCG.getParentFormPanel(this);
										f.executeTradeAction();
									}
								},
								{
									text: 'Mark to Market',
									xtype: 'button',
									iconCls: 'exchange',
									width: 120,
									handler: function() {
										const f = TCG.getParentFormPanel(this);
										const m2mLoader = new TCG.data.JsonLoader({
											params: {
												clientAccountId: f.getFormValue('clientInvestmentAccount.id', true),
												date: f.getFormValue('balanceDate', true).format('m/d/Y')
											},
											onLoad: function(record, conf) {
												if (record) {
													const config = {
														defaultDataIsReal: true,
														defaultData: record,
														params: {id: record.id}
													};
													TCG.createComponent('Clifton.accounting.m2m.M2MDailyWindow', config);
												}
												else {
													TCG.showError('Cannot find M2M daily associated for this run.');
												}
											}
										});
										m2mLoader.load('accountingM2MDailyByClientAccountAndDate.json?enableOpenSessionInView=true&requestedMaxDepth=4', f);
									}
								},
								{
									text: 'Portfolio Report',
									xtype: 'splitbutton',
									iconCls: 'pdf',
									width: 120,
									handler: function(b) {
										if (!b.hasVisibleMenu()) {
											const f = TCG.getParentFormPanel(this);
											f.executeReport();
										}
									},
									menu: {
										items: [{
											text: 'Regenerate PDF',
											iconCls: 'pdf',
											tooltip: 'Clear the report cache and rerun the report on the report server.  This is useful if the report file doesn\'t match the portfolio run.',
											handler: function() {
												const f = TCG.getParentFormPanel(this);
												f.executeReport(true);
											}
										}, {
											text: 'Client Facing PDF',
											iconCls: 'pdf',
											tooltip: 'Generate the PDF version that the client will see (excludes Report Components marked as Private).',
											handler: function() {
												const f = TCG.getParentFormPanel(this);
												f.executeReport(true, 'PDF', true);
											}
										}, {
											text: 'Generate Excel (97-2003)',
											iconCls: 'excel',
											tooltip: 'Generate the excel version of the report.  NOTE: This will hide any private sections.',
											handler: function() {
												const f = TCG.getParentFormPanel(this);
												f.executeReport(true, 'EXCEL', true);
											}
										}, {
											text: 'Generate Excel',
											hidden: true,
											iconCls: 'excel_open_xml',
											tooltip: 'Generate the excel version of the report.  NOTE: This will hide any private sections.',
											handler: function() {
												const f = TCG.getParentFormPanel(this);
												f.executeReport(true, 'EXCEL_OPEN_XML', true);
											}
										}]
									}
								},
								{
									text: 'Portal Files',
									xtype: 'button',
									iconCls: 'www',
									tooltip: 'Post/View Portal Files associated with this run.',
									width: 120,
									handler: function() {
										const f = TCG.getParentFormPanel(this);
										const className = 'Clifton.portal.file.FileListForSourceEntityWindow';
										const id = f.getFormValue('id');
										const cmpId = TCG.getComponentId(className, id);
										TCG.createComponent(className, {
											id: cmpId,
											sourceLabel: f.getFormValue('label'),
											sourceTableName: 'PortfolioRun',
											sourceFkFieldId: id,
											postUrl: 'portfolioRunReportPost.json',
											postParams: {runId: id}
										});
									}
								}
							],
							labelWidth: 100,
							items: [
								{fieldLabel: 'Violation Status', name: 'violationStatus.name', xtype: 'displayfield'},
								{fieldLabel: 'Notes', name: 'notes', xtype: 'textarea', height: 50}
							]
						}
					],

					fileAttachmentsGrid: {
						title: 'File Attachments',
						xtype: 'documentFileGrid-simple',
						definitionName: 'PortfolioRunAttachments'
					}
				}]
			},


			{
				title: 'Violations',
				items: [{
					xtype: 'rule-violation-grid',
					tableName: 'PortfolioRun',
					manualAllowed: true,
					manualRuleDefinitionName: 'Portfolio Run Violations',
					requireIgnoreNote: true,
					getStartDate: function() {
						return new Date(this.getWindow().getMainFormPanel().getFormValue('balanceDate')).format('m/d/Y');
					}
				}]
			}
		]
	}]
});
