// Window Selector
Clifton.portfolio.run.trade.RunTradeWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'portfolioRun.json',

	getClassName: function(config, entity) {
		let processingType = config && config.params ? config.params.processingType : undefined;
		let serviceProcessingType = config && config.params && config.params.serviceProcessingType ? config.params.serviceProcessingType.name : undefined;
		let className = config.className;
		if (TCG.isBlank(className)) {
			// if either are provided in params, use them, with service type first
			if (serviceProcessingType) {
				className = Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides[serviceProcessingType];
			}
			if (!className && processingType) {
				className = Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides[processingType];
			}
		}
		let screenDefaultView = undefined;
		if (entity) {
			if (TCG.isBlank(className)) {
				serviceProcessingType = TCG.getValue('clientInvestmentAccount.serviceProcessingType.name', entity);
				if (serviceProcessingType) {
					className = Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides[serviceProcessingType];
				}
			}
			if (TCG.isBlank(className)) {
				processingType = TCG.getValue('clientInvestmentAccount.serviceProcessingType.processingType', entity);
				className = Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides[processingType];
			}
			screenDefaultView = Clifton.portfolio.run.getDefaultViewForAccount(entity.clientInvestmentAccount, 'Trade Creation View');
		}

		if (!className) {
			className = Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides['DEFAULT'];
		}

		if (screenDefaultView) {
			entity.defaultView = screenDefaultView;
		}
		return className;
	}
});
