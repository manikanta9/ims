// // Trades tab - View trade history for account or account & all sub-accounts
TCG.use('Clifton.portfolio.run.trade.RunTradeHistoryGrid');
TCG.use('Clifton.product.overlay.trade.RunTradeReplicationGrid');
TCG.use('Clifton.portfolio.run.trade.collateral.RunTradeCollateralGrid');

Clifton.portfolio.run.trade.BaseRunTradeWindow = Ext.extend(TCG.app.DetailWindow, {

	dtoClassForBinding: 'OVERRIDE_ME', // com.clifton.portfolio.run.ldi.trade.PortfolioRunTradeLDI',

	appendBindingParametersToUrl: function(url) {
		const parameterSeparator = (url.indexOf('?') > 0) ? '&' : '?';
		return url + parameterSeparator + 'enableValidatingBinding=true&dtoClassForBinding=' + this.dtoClassForBinding;
	},

	// Properties to Override in Extending Classes:
	runTradeEntryForm: 'OVERRIDE_ME',


	init: function() {
		// replace the first tab with the override
		const win = this;
		const tabs = win.items[0].items;
		tabs[0] = {
			title: 'Run',
			tbar: {
				xtype: 'workflow-toolbar',
				tableName: 'PortfolioRun',
				finalState: 'Completed',
				reloadFormPanelAfterTransition: true,
				hideSystemDefinedTransitions: true
			},
			items: [win.runTradeEntryForm]
		};
		Clifton.portfolio.run.trade.BaseRunTradeWindow.superclass.init.apply(this, arguments);
	},


	additionalRunTabs: undefined,
	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		if (w.additionalRunTabs) {
			Ext.each(this.additionalRunTabs, function(f) {
				tabs.add(f);
			});
		}

		// Add File Attachments to the End
		tabs.add({
			title: 'File Attachments',
			items: [
				{
					xtype: 'documentFileGrid-simple',
					definitionName: 'PortfolioRunAttachments',
					layout: 'vbox',
					frame: false,
					border: false,
					collapsible: false,
					collapsed: false,
					gridConfig: {
						listeners: {
							'rowclick': function(grid, rowIndex, evt) {
								if (TCG.isActionColumn(evt.target)) {
									const eventName = TCG.getActionColumnEventName(evt);
									const row = grid.store.data.items[rowIndex];
									if (eventName === 'ViewFile') {
										Clifton.document.DownloadDocument('DocumentFile', grid, row.json.id);
									}
								}
							}
						}
					}
				}
			]
		});
		tabs.add({
			title: 'Collateral Management',
			items: [
				{
					name: 'portfolioCollateralManagement',
					loadValidation: false,
					xtype: 'formpanel',
					url: 'portfolioCollateralManagement.json?requestedMaxDepth=4',
					layout: {type: 'vbox', align: 'stretch'},
					warningMessageHeight: 25,
					getWarningMessage: function(form) {
						const mainFormPanel = TCG.getParentByClass(form.ownerCt, Ext.Window).getMainFormPanel();
						if (mainFormPanel) {
							return mainFormPanel.getWarningMessage(mainFormPanel.getForm());
						}
						return void 0;
					},
					getDefaultData: function(window) {
						const mainForm = window.getMainForm();
						if (mainForm) {
							return {portfolioRun: mainForm.formValues};
						}
						return {};
					},
					items: [
						{
							xtype: 'panel',
							layout: 'form',
							items: [
								{fieldLabel: 'Portfolio Run', name: 'portfolioRun.label', xtype: 'linkfield', detailIdField: 'portfolioRun.id', detailPageClass: 'Clifton.portfolio.run.RunWindow', height: 25}
							]
						},
						{
							xtype: 'panel',
							layout: {type: 'hbox', align: 'stretch'},
							flex: 1,
							defaults: {flex: 1, frame: true, xtype: 'gridpanel', plugins: {ptype: 'gridsummary'}, appendStandardColumns: false, gridConfig: {minColumnWidth: 5}},
							items: [
								{
									title: 'Margin Summary',
									name: 'portfolioRunMarginDetailByRun',
									storeRoot: 'data.runMarginList',
									instructions: 'Margin Summary of holding accounts.',
									additionalPropertiesToRequest: 'runMarginList|brokerInvestmentAccount.label',
									editor: {
										drillDownOnly: true,
										detailPageClass: 'Clifton.portfolio.run.RunMarginSummaryWindow'
									},
									getLoadParams: function(firstLoad) {
										if (TCG.isNotBlank(this.getWindow().getMainFormId())) {
											return {
												runId: this.getWindow().getMainFormId(),
												requestedMaxDepth: 4
											};
										}
										return false;
									},
									columns: [
										{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
										{header: 'Run ID', width: 15, dataIndex: 'portfolioRun.id', hidden: true},
										{
											header: 'Holding Account', width: 60, dataIndex: 'brokerInvestmentAccount.number', defaultSortColumn: true,
											renderer: function(value, metaData, record) {
												metaData.attr = `qtip='${record.json.brokerInvestmentAccount.label}'`;
												return value;
											}
										},
										{
											header: 'Account Value', width: 60, dataIndex: 'ourAccountValue', type: 'currency', summaryType: 'sum',
											renderer: function(value, metaData, record) {
												if (record && record.json && record.json.ourAccountValuePercent) {
													metaData.attr = `qtip='${TCG.renderPercent(record.json.ourAccountValuePercent)}'`;
												}
												return TCG.renderAmount(value);
											}
										},
										{header: 'Account Value %', width: 40, dataIndex: 'ourAccountValuePercent', type: 'currency', hidden: true},
										{
											header: 'Initial Margin', width: 60, dataIndex: 'marginInitialValue', type: 'currency', summaryType: 'sum',
											renderer: function(value, metaData, record) {
												if (record && record.json && record.json.marginInitialValuePercent) {
													metaData.attr = `qtip='${TCG.renderPercent(record.json.marginInitialValuePercent)}'`;
												}
												return TCG.renderAmount(value);
											}
										},
										{header: 'Initial Margin %', width: 40, dataIndex: 'marginInitialValuePercent', type: 'currency', hidden: true},
										{
											header: 'Collateral Target', width: 60, dataIndex: 'collateralTarget', type: 'currency', summaryType: 'sum',
											renderer: function(value, metaData, record) {
												if (record && record.json && record.json.collateralTargetPercent) {
													metaData.attr = `qtip='${TCG.renderPercent(record.json.collateralTargetPercent)}'`;
												}
												return TCG.renderAmount(value);
											}
										},
										{header: 'Collateral Target %', width: 40, dataIndex: 'collateralTargetPercent', type: 'currency', hidden: true},
										{
											header: 'Broker Collateral', width: 60, dataIndex: 'brokerCollateralValue', type: 'currency', summaryType: 'sum',
											renderer: function(value, metaData, record) {
												if (record && record.json && record.json.brokerCollateralValuePercent) {
													metaData.attr = `qtip='${TCG.renderPercent(record.json.brokerCollateralValuePercent)}'`;
												}
												return TCG.renderAmount(value);
											}
										},
										{header: 'Broker Collateral %', width: 40, dataIndex: 'brokerCollateralValuePercent', type: 'currency', hidden: true},
										{
											header: 'Pending Collateral', width: 60, dataIndex: 'pendingCollateral', type: 'currency', summaryType: 'sum',
											tooltip: 'Pending transaction market value in account base currency. The pending activity is previewed to include position and cash impact, so a value of 0 could indicate position offset with cash.',
											renderer: function(value, metaData, record) {
												if (record && record.json && record.json.pendingCollateralPercent) {
													metaData.attr = `qtip='${TCG.renderPercent(record.json.pendingCollateralPercent)}'`;
												}
												return TCG.renderAmount(value);
											}
										},
										{header: 'Pending Collateral %', width: 40, dataIndex: 'pendingCollateralPercent', type: 'currency', hidden: true},
										{
											header: 'Collateral Difference', width: 60, dataIndex: 'collateralDifference', type: 'currency', summaryType: 'sum',
											renderer: function(value, metaData, record) {
												if (record && record.json && record.json.collateralDifferencePercent) {
													metaData.attr = `qtip='${TCG.renderPercent(record.json.collateralDifferencePercent)}'`;
												}
												return TCG.renderAmount(value);
											}
										},
										{header: 'Collateral Difference %', width: 40, dataIndex: 'collateralDifferencePercent', type: 'currency', hidden: true}
									]
								},
								{
									title: 'Margin Breakdown',
									name: 'portfolioRunMarginAccountDetailListByRun',
									instructions: 'Margin Breakdown for Broker and Custodian accounts based on information available in the general ledger.',
									requestIdDataIndex: true,
									getLoadParams: function(firstLoad) {
										if (TCG.isNotBlank(this.getWindow().getMainFormId())) {
											return {
												runId: this.getWindow().getMainFormId(),
												requestedMaxDepth: 4
											};
										}
										return false;
									},
									columns: [
										{header: 'Client', dataIndex: 'clientAccount.number', width: 60, defaultSortColumn: true},
										{header: 'Account #', dataIndex: 'investmentAccount.number', idDataIndex: 'investmentAccount.id', width: 60},
										{header: 'Account Name', dataIndex: 'investmentAccount.name', width: 175},
										{header: 'Securities', width: 75, dataIndex: 'securitiesValue', summaryType: 'sum', type: 'currency', negativeInRed: true},
										{header: 'Cash', width: 75, dataIndex: 'cashValue', summaryType: 'sum', type: 'currency', negativeInRed: true},
										{header: 'Total', width: 75, dataIndex: 'totalValue', summaryType: 'sum', type: 'currency', negativeInRed: true},
										{header: 'Collateral', width: 40, dataIndex: 'collateralOnly', type: 'boolean', hidden: true, tooltip: 'Checked if this account shows cash for collateral only (e.g. Custodian will often use Cash; FCM will use Cash Collateral)'}
									],
									editor: {
										drillDownOnly: true,
										detailPageClass: 'Clifton.investment.account.AccountWindow',
										getDetailPageId: function(gridPanel, row) {
											return TCG.getValue('investmentAccount.id', row.json);
										}
									}
								}
							]
						},
						{
							xtype: 'run-trade-collateral-grid',
							flex: 2
						}
					]
				}
			]
		});

		if (this.postProcessTabs) {
			this.postProcessTabs(tabs);
		}
	},

	titlePrefix: 'Portfolio Run Trade Creation',
	iconCls: 'run',
	width: 1150,
	height: 645,
	maximized: true,
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,

	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		// TODO: data binding for partial field updates
		// TODO: concurrent updates validation
		form.trackResetOnLoad = true;

		// Trade List return for Trade Group - Set properties
		params.requestedProperties = 'id|workflowState.name|buy|quantityIntended|accountingNotional|investmentSecurity.symbol|fixTrade|tradeDate|settlementDate|security.instrument.hierarchy.investmentType.name';
		params.requestedPropertiesRoot = 'tradeGroup.tradeList';

		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Saving...',
			success: function(form, action) {
				panel.transitionRun('1st PM Approved - Trades');
				win.savedSinceOpen = true;
				if (win.closeOnSuccess) {
					win.closeWindow();
				}
				panel.fireEvent('afterload', panel, win.closeOnSuccess);
				if (action.result.data.tradeList) {
					const config = {
						defaultDataIsReal: true,
						defaultData: action.result.data,
						params: {id: action.result.data.id},
						openerCt: win
					};
					TCG.createComponent('Clifton.portfolio.run.trade.RunTradeGroupConfirmWindow', config);
				}
				else {
					Ext.Msg.show({
						title: 'Virtual Trades Submitted',
						icon: Ext.MessageBox.INFO,
						msg: 'All Buys/Sells entered resulted in a net trade of 0. No trades were created. Virtual trades have been entered for re-allocation of contract splitting.',
						buttons: {ok: 'Close Trade Creation Window', cancel: 'Return to Trade Creation Window'},
						fn: function(buttonId) {
							if (buttonId === 'ok') {
								win.closeWindow();
							}
						}
					});
				}

			}
		}, Ext.applyIf({timeout: 200, timeoutErrorMsg: 'Trade submission is taking longer than expected.  Please wait a minute for processing to finish creating your trades and then reload your screen to verify trades were created.  Clicking Submit Trades again will create duplicates.'}, TCG.form.submitDefaults)));
	},

	items: [{
		xtype: 'tabpanel',
		items: [

			{}, // first tab to be overridden


			{
				title: 'Violations',
				items: [{
					xtype: 'rule-violation-grid',
					tableName: 'PortfolioRun',
					manualAllowed: true,
					manualRuleDefinitionName: 'Portfolio Run Violations'
				}]

			},


			{
				title: 'Trades',
				items: [
					Clifton.portfolio.run.trade.RunTradeHistoryGrid
				]

			},

			{
				title: 'Trade Allocation',
				items: [{
					xtype: 'trade-assetclass-position-allocation-grid',
					hiddenColumns: ['accountAssetClass.account.label'],
					showAccountColumn: false,
					getLoadParams: function(firstLoad) {
						const f = this.getWindow().getMainForm();
						if (firstLoad) {
							const dateV = TCG.parseDate(TCG.getValue('balanceDate', f.formValues));
							const workflowStatus = TCG.getValue('workflowStatus.name', f.formValues);
							if (workflowStatus === 'Closed') {
								this.setFilterValue('tradeDate', {'after': dateV, 'before': dateV.add(Date.DAY, 7)});
							}
							else {
								this.setFilterValue('tradeDate', {'after': dateV});
							}
						}
						const acctId = TCG.getValue('clientInvestmentAccount.id', f.formValues);
						const params = {'accountId': acctId};
						return this.appendToolbarFilterParams(params);
					}
				}]

			},

			{
				title: 'Events',
				items: [{
					xtype: 'investment-calendar-event-grid-by-account',
					defaultAfterDaysBack: -2,
					defaultBeforeDaysForward: 14,
					includeSubAccounts: true,

					getEventDateStartDefaultDate: function() {
						const f = this.getWindow().getMainForm();
						const date = TCG.parseDate(TCG.getValue('balanceDate', f.formValues));
						return Clifton.calendar.getBusinessDayFromDate(date, this.defaultAfterDaysBack);
					},

					getEventDateEndDefaultDate: function() {
						const f = this.getWindow().getMainForm();
						const date = TCG.parseDate(TCG.getValue('balanceDate', f.formValues));
						return date.add(Date.DAY, this.defaultBeforeDaysForward);
					},

					getInvestmentAccountId: function() {
						const f = this.getWindow().getMainForm();
						return TCG.getValue('clientInvestmentAccount.id', f.formValues);
					}

				}]
			},


			{
				title: 'Historical Runs',
				items: [{
					xtype: 'portfolio-runs-gridpanel',

					getTopToolbarFilters: function(toolbar) {
						return undefined;
					},

					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default balance date to >= 5 business days back
							const f = this.getWindow().getMainForm();
							const date = TCG.parseDate(TCG.getValue('balanceDate', f.formValues));
							const afterDate = Clifton.calendar.getBusinessDayFromDate(date, -5);
							this.setFilterValue('balanceDate', {'after': afterDate});
						}
						return {requestedMaxDepth: 4, readUncommittedRequested: true, clientInvestmentAccountId: this.getInvestmentAccountId()};
					},

					getInvestmentAccountId: function() {
						const f = this.getWindow().getMainForm();
						return TCG.getValue('clientInvestmentAccount.id', f.formValues);
					}

				}]
			}
		]
	}
	]
});
