// PORTFOLIO RUN - TRADE CREATION WINDOW - BASE CLASS FOR DETAIL FORM GRID WITH TOOLBAR
Clifton.portfolio.run.trade.BaseRunTradeDetailGrid = {

	xtype: 'formgrid-scroll',

	dtoClass: 'OVERRIDE_ME',// 'com.clifton.product.overlay.ProductOverlayAssetClassReplication',
	detailPageClass: 'OVERRIDE_ME', // 'Clifton.product.overlay.assetclass.ReplicationWindow',
	urlPrefix: 'portfolioRunTrade', // Overridden and used for specific cases where separate URLs are required

	height: 300,
	heightResized: true,
	readOnly: true,
	name: 'details',
	storeRoot: 'detailList',

	remoteSort: true,
	forceFit: true,
	limitRequestedProperties: true,
	expanded: true,

	// These fields may not be edited and may just use default value, so we always need them submitted
	// Send pricing to the server so that if we are trading, we can update the trade prices on the replication
	alwaysSubmitFields: ['tradeSecurityPrice', 'tradeSecurityPriceDate', 'tradeUnderlyingSecurityPrice', 'tradeUnderlyingSecurityPriceDate', 'tradeSecurityDirtyPrice', 'tradeSecurityDirtyPriceDate', 'tradeExchangeRate', 'tradeExchangeRateDate', 'trade.executingBrokerCompany.id', 'trade.tradeDestination.id', 'trade.holdingInvestmentAccount.id', 'buyContracts', 'sellContracts', 'fairValueAdjustment', 'previousFairValueAdjustment', 'closePosition'],

	// ADDS MISPRICING OPTION TO THE TOOLBAR VIEWS
	supportMispricingView: false,
	// Adds the right-most column checkbox
	enableClosePositions: false,

	initComponent: function() {
		const cols = [];
		if (this.firstColumns && this.firstColumns.length > 0) {
			Ext.each(this.firstColumns, function(f) {
				cols.push(f);
			});
		}
		if (this.detailColumns && this.detailColumns.length > 0) {
			Ext.each(this.detailColumns, function(f) {
				cols.push(f);
			});
		}
		if (this.lastColumns && this.lastColumns.length > 0) {
			Ext.each(this.lastColumns, function(f) {
				cols.push(f);
			});
		}
		this.columnsConfig = cols;
		TCG.grid.FormGridPanel.prototype.initComponent.call(this, arguments);
	},

	/***********************************************************************
	 * TOOLBAR
	 * *********************************************************************/

	addButton: function(toolbar, gp) {
		// By Default - Do nothing
		// Used to support specific add row functionality
		// Used for "Add Security" button for Overlay to add replication row
	},

	addToolbarFilterButton: function(toolbar, gp) {
		// By Default - Do nothing
		// Used to support filters on the right side of the toolbar
	},

	addToolbarButtons: function(toolBar) {
		const gp = this;

		// If we use grouping - add expand/collapse all button
		if (this.groupField) {
			toolBar.add({
				iconCls: 'expand-all',
				tooltip: 'Expand or Collapse all asset class replications',
				scope: this,
				handler: function() {
					gp.expanded = !gp.expanded;
					gp.view.toggleAllGroups(gp.expanded);
				}
			});
			toolBar.add('-');
		}

		toolBar.add({
			text: 'Submit Trades',
			tooltip: 'Create a trade for each Buy and Sell quantity entered' +
				(TCG.isTrue(gp.enableClosePositions) ? ', and/or selected positions to close' : ''),
			iconCls: 'shopping-cart',
			scope: this,
			handler: async function() {
				if (TCG.isTrue(await gp.validateTradingAllowedRun())) {
					gp.getWindow().saveWindow(false);
				}
				else {
					return false;
				}
			},
			listeners: {
				afterRender: function() {
					const btn = this;
					const w = gp.getWindow();
					w.on('modified', function(win, mod) {
						if (mod) {
							btn.enable();
						}
						else {
							btn.disable();
						}
					}, this);
				}
			}
		});
		toolBar.add('-');

		toolBar.add({
			text: 'Clear Trades',
			tooltip: 'Clear All Buys/Sells and reload table with latest trade amounts from the database. Note: Prices shown on screen will not be changed.',
			iconCls: 'clear',
			scope: this,
			handler: function() {
				gp.reloadTrades();
			},
			listeners: {
				afterRender: function() {
					const btn = this;
					const w = gp.getWindow();
					w.on('modified', function(win, mod) {
						if (mod) {
							btn.enable();
						}
						else {
							btn.disable();
						}
					}, this);
				}
			}
		});
		toolBar.add('-');

		toolBar.add({
			text: 'Approve Trades',
			tooltip: 'Approve all Pending Trades for securities in selected rows.',
			iconCls: 'verify',
			scope: this,
			handler: function() {
				gp.approveTrades();
			}
		});
		toolBar.add('-');

		// Add Row Button - If supported
		this.addButton(toolBar, gp);

		toolBar.add({
			text: 'Live Pricing',
			tooltip: 'Update prices and related fields to reflect latest prices from Bloomberg',
			iconCls: 'bloomberg',
			scope: this,
			handler: function() {
				gp.reloadPrices(true);
			}
		});
		toolBar.add('-');

		toolBar.add({
			text: 'Previous Close',
			tooltip: 'Prices and related fields will be updated to reflect the prices stored on the run (Previous night\'s closing price)',
			iconCls: 'table-refresh',
			scope: this,
			handler: function() {
				gp.reloadPrices(false);
			}
		});
		toolBar.add('-');

		const viewMenu = new Ext.menu.Menu();
		if (this.viewNames) {
			const menuViewItemHandlerFunction = function(menuItem) {
				gp.switchToView(menuItem.text);
				// update Open/Close Type menu item check status
				viewMenu.findBy(function(component) {
					if (component.text === 'Show Open/Close Type Column') {
						const columnModel = gp.getColumnModel();
						const openCloseTypeColumnIndex = columnModel.findColumnIndex('openCloseType.name');
						component.setChecked(!TCG.isTrue(columnModel.isHidden(openCloseTypeColumnIndex)));
					}
				}, viewMenu);

				// update Holding Account menu item check status
				viewMenu.findBy(function(component) {
					if (component.text === 'Show Holding Account Column') {
						const columnModel = gp.getColumnModel();
						const holdingAccountColumnIndex = columnModel.findColumnIndex('trade.holdingInvestmentAccount.label');
						component.setChecked(!TCG.isTrue(columnModel.isHidden(holdingAccountColumnIndex)));
					}
				}, viewMenu);

				// update Execution Type menu item check status
				viewMenu.findBy(function(component) {
					if (component.text === 'Show Trade Execution Type Column') {
						const columnModel = gp.getColumnModel();
						const executionTypeColumnIndex = columnModel.findColumnIndex('trade.tradeExecutionType.name');
						component.setChecked(!TCG.isTrue(columnModel.isHidden(executionTypeColumnIndex)));
					}
				}, viewMenu);
			};
			for (let i = 0; i < this.viewNames.length; i++) {
				viewMenu.add({
					text: this.viewNames[i],
					xtype: 'menucheckitem',
					checked: (i === 0),
					group: 'view' + gp.id,
					handler: menuViewItemHandlerFunction
				});
			}
		}

		if (this.supportMispricingView === true) {
			viewMenu.add('-');
			viewMenu.add({
				text: 'Exclude Mispricing',
				xtype: 'menucheckitem',
				checkHandler: function(item, checked) {
					gp.switchMispricing(checked, true);
				}
			});
		}

		viewMenu.add('-', {
			text: 'Show Block Column',
			xtype: 'menucheckitem',
			tooltip: 'Show the Block column to create block trades',
			checkHandler: function(item, checked) {
				const columnModel = gp.getColumnModel();
				const blockColumnIndex = columnModel.findColumnIndex('trade.blockTrade');
				columnModel.setHidden(blockColumnIndex, !TCG.isTrue(checked));
			}

		});
		const columnModel = gp.getColumnModel();
		const openCloseTypeColumnIndex = columnModel.findColumnIndex('openCloseType.name');
		if (openCloseTypeColumnIndex > -1) {
			const hidden = columnModel.isHidden(openCloseTypeColumnIndex);
			viewMenu.add('-', {
				text: 'Show Open/Close Type Column',
				xtype: 'menucheckitem',
				tooltip: 'Show the Open/Close Type column to choose the desired position impact the trade will have for a replication. The Open/Close Type is required for Options trades.',
				checked: !TCG.isTrue(hidden),
				checkHandler: function(item, checked) {
					columnModel.setHidden(openCloseTypeColumnIndex, !TCG.isTrue(checked));
				}
			});
		}

		const holdingAccountColumnIndex = columnModel.findColumnIndex('trade.holdingInvestmentAccount.label');
		if (holdingAccountColumnIndex > -1) {
			const hidden = columnModel.isHidden(holdingAccountColumnIndex);
			viewMenu.add('-', {
				text: 'Show Holding Account Column',
				xtype: 'menucheckitem',
				tooltip: 'Show the Holding Account column to choose the desired holding account for the trade. Necessary only when multiple holding accounts can trade specified security for the replication.',
				checked: !TCG.isTrue(hidden),
				checkHandler: function(item, checked) {
					columnModel.setHidden(holdingAccountColumnIndex, !TCG.isTrue(checked));
				}
			});
		}

		const tradeExecutionTypeColumnIndex = columnModel.findColumnIndex('trade.tradeExecutionType.name');
		const limitPriceColumnIndex = columnModel.findColumnIndex('trade.limitPrice');
		if (tradeExecutionTypeColumnIndex > -1) {
			const hidden = columnModel.isHidden(tradeExecutionTypeColumnIndex);
			viewMenu.add('-', {
				text: 'Show Trade Execution Type Column',
				xtype: 'menucheckitem',
				tooltip: 'Show the Trade Execution type column and the associated Limit Price column for executing limit trades.',
				checked: !TCG.isTrue(hidden),
				checkHandler: function(item, checked) {
					columnModel.setHidden(tradeExecutionTypeColumnIndex, !TCG.isTrue(checked));
					if (limitPriceColumnIndex > -1) {
						columnModel.setHidden(limitPriceColumnIndex, !TCG.isTrue(checked));
					}
				}
			});
		}

		const tradeNoteColumn = columnModel.findColumnIndex('trade.description');
		if (tradeNoteColumn > -1) {
			const hidden = columnModel.isHidden(tradeNoteColumn);
			viewMenu.add('-', {
				text: 'Show Trade Note Column',
				xtype: 'menucheckitem',
				tooltip: 'Show the Trade Note column to choose an order processing algorithm.',
				checked: !TCG.isTrue(hidden),
				checkHandler: function(item, checked) {
					columnModel.setHidden(tradeNoteColumn, !TCG.isTrue(checked));
				}
			});
		}

		if (this.appendViewMenuItems) {
			this.appendViewMenuItems(viewMenu, gp);
		}

		if (viewMenu.items && viewMenu.items.length > 0) {
			toolBar.add({
				text: 'Views',
				iconCls: 'views',
				menu: viewMenu
			});
			toolBar.add('-');
		}

		toolBar.add({
			text: 'Report',
			tooltip: 'View Portfolio Report',
			iconCls: 'pdf',
			handler: function() {
				gp.executeReport();
			}
		});
		toolBar.add('-');

		toolBar.add({
			text: 'Print',
			iconCls: 'printer',
			handler: () => Ext.ux.Printer.print(gp)
		});
		toolBar.add('-');

		toolBar.add({
			text: 'Close Window',
			tooltip: 'Close Window',
			iconCls: 'cancel',
			handler: function() {
				gp.getWindow().closeWindow();
			}
		});
		toolBar.add('-');

		// Displays the icon, but actual drag and drop is added after load so form values are loaded
		const ddMsg = 'Drag and drop file here to create <b>Client Trade Direction</b> note for all trades associated with this run and attach this file to it.';
		toolBar.add({xtype: 'label', html: '<span style="width: 16px; height: 16px; float: left;" class="attach" ext:qtip="' + ddMsg + '">&nbsp;</span>'});

		toolBar.add('-');
		toolBar.add({
			text: 'Mark to Market',
			iconCls: 'exchange',
			handler: function() {
				const panel = TCG.getParentFormPanel(gp);
				const m2mLoader = new TCG.data.JsonLoader({
					params: {
						clientAccountId: TCG.getValue('clientInvestmentAccount.id', panel.getForm().formValues),
						date: TCG.parseDate(TCG.getValue('balanceDate', panel.getForm().formValues)).format('m/d/Y')
					},
					onLoad: function(record, conf) {
						if (record) {
							const config = {
								defaultDataIsReal: true,
								defaultData: record,
								params: {id: record.id}
							};
							TCG.createComponent('Clifton.accounting.m2m.M2MDailyWindow', config);
						}
						else {
							TCG.showError('Cannot find M2M daily associated for this run.');
						}
					}
				});
				m2mLoader.load('accountingM2MDailyByClientAccountAndDate.json?enableOpenSessionInView=true&requestedMaxDepth=4', gp);
			}
		});
		toolBar.add('-');

		toolBar.add({xtype: 'tbfill'});

		// Add toolbar filter button - If supported
		this.addToolbarFilterButton(toolBar, gp);

		toolBar.add({xtype: 'label', html: 'Trade Date:&nbsp;'});
		toolBar.add({
			xtype: 'datefield',
			name: 'tradeDate'
		});
	},


	/***********************************************************************
	 * GRID LOADING/RE-LOADING
	 * *********************************************************************/

	beforeLoadData: async function(formPanel, formGrid, formValues) {
		const mainFormValues = formPanel.getForm().formValues;
		const tradingAllowed = TCG.getValue('tradingAllowed', mainFormValues);

		if (TCG.isTrue(tradingAllowed)) {
			// look up non-cancelled trades to cache on the grid and use to show in the pending trades tooltip
			const tradeDate = new Date(mainFormValues.balanceDate);
			const clientAccountIdSet = new Set();
			clientAccountIdSet.add(TCG.getValue('clientInvestmentAccount.id', mainFormValues));
			const replications = TCG.getValue(formGrid.storeRoot, mainFormValues) || [];
			// Add sub accounts used for trading
			replications.map(rep => TCG.getValue('tradingClientAccount.id', rep))
				.filter(clientId => TCG.isNotBlank(clientId))
				.forEach(clientId => clientAccountIdSet.add(clientId));
			const clientAccountIds = [...clientAccountIdSet];

			const params = {
				start: 0,
				limit: 100,
				restrictionList: JSON.stringify([{comparison: 'GREATER_THAN', value: tradeDate.format('m/d/Y'), dataTypeName: 'DATE', field: 'tradeDate'}]),
				excludeWorkflowStateName: 'Cancelled',
				requestedPropertiesRoot: 'data.rows',
				requestedProperties: 'investmentSecurity.id|workflowState.name|executingBrokerCompany.label|buy|quantityIntended|originalFace|tradeExecutionType.name|tradeDestination.label|description|blockTrade'
			};
			if (clientAccountIds.length === 1) {
				params.clientInvestmentAccountId = clientAccountIds[0];
			}
			else {
				params.clientInvestmentAccountIds = clientAccountIds;
			}

			const tradeListResult = await TCG.data.getDataPromise('tradeListFind.json', formGrid, {params: params});
			formGrid.pendingTradeList = (tradeListResult && tradeListResult.rows) ? tradeListResult.rows : [];
		}
	},

	applyPendingTradesTooltipToColumn: function(value, metadata, record) {
		const grid = this;
		if (TCG.isNotBlank(value) && (value !== 0) && metadata && record && grid.pendingTradeList) {
			const rowSecurityId = TCG.getValue('security.id', record.json);
			const filteredTrades = (grid.pendingTradeList || []).filter(trade => TCG.getValue('investmentSecurity.id', trade) === rowSecurityId);
			if (filteredTrades && filteredTrades.length > 0) {
				const commonStyle = 'background-color: #eee; padding: 3px; white-space: nowrap; border: 1px solid black; border-collapse: collapse;';
				const headerRow = function(headerNames) {
					const headerColumns = headerNames.map(header => `<th style="text-align: center; ${commonStyle}">${header}</th>`);
					return `<tr>${headerColumns.join('')}</tr>`;
				};
				const tradeRow = function(tradeValues) {
					const tradeColumns = tradeValues.map(tradeValue => {
						const value = TCG.isBlank(tradeValue.value) ? '' : tradeValue.value;
						let style = commonStyle;
						if (TCG.isNotBlank(tradeValue.style)) {
							style += tradeValue.style;
						}
						return `<td style="${style}">${value}</td>`;
					});
					return `<tr>${tradeColumns.join('')}</tr>`;
				};
				let qtip = '<table style="border: 1px solid black; border-collapse: collapse;">';
				qtip += headerRow([
					'Workflow State',
					'Executing Broker',
					'Buy/Sell',
					'Quantity',
					'Execution Type',
					'Destination',
					'Block',
					'Trade Note'
				]);
				filteredTrades.forEach(trade => {
					qtip += tradeRow([
						{value: TCG.getValue('workflowState.name', trade)},
						{value: TCG.getValue('executingBrokerCompany.label', trade)},
						{value: trade.buy ? 'Buy' : 'Sell'},
						{value: TCG.isBlank(trade.quantityIntended) ? trade.originalFace : trade.quantityIntended, style: 'text-align: right;'},
						{value: TCG.getValue('tradeExecutionType.name', trade)},
						{value: TCG.getValue('tradeDestination.label', trade)},
						{value: TCG.isTrue(trade.blockTrade)},
						{value: trade.description}
					]);
				});
				qtip += '</table>';
				metadata.attr = TCG.renderQtip(qtip);
			}
		}
	},

	reload: function() {
		// Default Reload - Reload Prices
		this.reloadPrices(TCG.isNotNull(this.lastLivePrices) ? this.lastLivePrices : false);
	},

	// Called when new securities are added
	// TODO REFACTOR CALLING METHOD TO JUST CALL RELOAD? Overlay has an additional method that it does
	fullReload: function() {
		this.reload();
	},

	getReloadPricesUrl: function() {
		return 'portfolioRunTradePrices.json';
	},

	reloadPrices: function(livePrices, reloadReplications) {
		this.reloadGrid(this.getReloadPricesUrl(), livePrices, reloadReplications);
		if (this.getWindow().isModified()) {
			this.markModified();
		}
	},

	getReloadTradesUrl: function() {
		return 'portfolioRunTradeAmounts.json';
	},

	reloadTrades: function() {
		this.reloadGrid(this.getReloadTradesUrl());
		this.getWindow().setModified(false);
		this.clearModified();
	},

	reloadGrid: function(url, livePrices) {
		let preparedUrl = url;
		preparedUrl = this.getWindow().appendBindingParametersToUrl(preparedUrl);

		const grid = this;
		// get the submit parameters
		const panel = TCG.getParentFormPanel(this);
		const params = Ext.apply({}, panel.getFormValuesFormatted());
		if (TCG.isNotNull(livePrices)) {
			params['livePrices'] = livePrices;
			this.lastLivePrices = livePrices;
		}
		params['reloadDetails'] = true;
		params['requestedMaxDepth'] = panel.requestedMaxDepth;
		params['requestedProperties'] = panel.getListRequestedProperties();
		params['requestedPropertiesRoot'] = panel.listRequestedPropertiesRoot;
		params['requestedPropertiesToExclude'] = panel.listRequestedPropertiesToExclude;

		this.prepareReloadParameters(grid, params);

		const loader = new TCG.data.JsonLoader({
			waitTarget: TCG.getParentFormPanel(grid).getEl(),
			waitMsg: 'Reloading...',
			params: params,
			scope: grid,
			onLoad: function(record, conf) {
				grid.store.loadData(record);
				panel.fireEvent('detailRefresh', record);
				grid.loadMispricing();
				grid.afterReloadGrid(grid);
			}
		});
		loader.load(preparedUrl, grid);
	},

	prepareReloadParameters: function(grid, params) {
		// Nothing by default. Hook to append parameters on reload.
		return params;
	},

	afterReloadGrid: function(grid, panel, record) {
		// Override for additional functionality
	},

	afterLoadGrid: function(grid, fp) {
		// overridden
	},

	afterFormLoad: function(grid, panel) {
		grid.loadMispricing(true);
		if (TCG.isTrue(grid.getWindow().savedSinceOpen)) {
			grid.reloadTrades();
		}
		// Enable Drag And Drop Feature to Attached "Client Trade Direction" notes
		TCG.file.enableDD(grid, grid.getNoteParams(), null, 'tradeNoteForTradeGroupBySourceUpload.json', false);
	},

	clearModified: function() {
		const result = [];
		this.store.each(function(record) {
			const r = {
				id: record.id,
				'class': this.dtoClass
			};
			for (let i = 0; i < this.alwaysSubmitFields.length; i++) {
				const f = this.alwaysSubmitFields[i];
				const val = TCG.getValue(f, record.json);
				if (TCG.isNotNull(val)) {
					let v = TCG.getValue(f, record.json);
					if (f.length > 4 && f.substring(f.length - 4) === 'Date') {
						v = TCG.renderDate(v);
					}
					r[f] = v;
				}
			}
			result.push(r);
		}, this);
		const value = Ext.util.JSON.encode(result);
		this.submitField.setValue(value); // changed value
		this.submitField.originalValue = value; // initial value
		this.afterClearModified();
		this.getWindow().setModified(false);
	},

	afterClearModified: function() {
		// overridden
	},

	/***********************************************************************
	 * GRID LISTENERS/EDITING
	 * *********************************************************************/

	listeners: {
		afterRender: function() {
			const grid = this;

			const fp = TCG.getParentFormPanel(this);
			let defaultView = (this.getWindow().defaultData && this.getWindow().defaultData.defaultView) ? this.getWindow().defaultData.defaultView : void 0;
			if (!defaultView && this.getDefaultView) {
				defaultView = this.getDefaultView();
			}
			if (defaultView) {
				this.setDefaultView(defaultView);
			}
			fp.on('afterload', function() {
				grid.afterFormLoad(grid, fp);

				const formPanel = TCG.getParentFormPanel(grid);
				if (formPanel) {
					const resizeFunction = function(warningContainer, remove) {
						// adjust size of this grid parent panel according to the warning container height
						const adjustment = (TCG.isTrue(remove) ? -1 : 1) * warningContainer.getHeight();
						const parentPanel = TCG.getParentByClass(grid, Ext.Panel);
						if (parentPanel) {
							parentPanel.setHeight(grid.getHeight() + adjustment);
							if (parentPanel.savedOffset) {
								// clear offset so registered resizing is reset.
								parentPanel.savedOffset = void 0;
							}
						}
					};

					const warnId = formPanel.id + '-warn';
					const warn = Ext.get(warnId);
					if (TCG.isNotNull(warn) && TCG.isNull(grid.warningContainer)) {
						grid.warningContainer = warn;
						resizeFunction(grid.warningContainer, true);
					}
					else if (TCG.isNull(warn) && TCG.isNotNull(grid.warningContainer)) {
						resizeFunction(grid.warningContainer);
						grid.warningContainer = void 0;
					}
				}
			}, this);

			if (grid.enableClosePositions) {
				const columnModel = grid.getColumnModel();
				const closePositionColumnIndex = columnModel.findColumnIndex('closePosition');
				if (closePositionColumnIndex > -1) {
					columnModel.setHidden(closePositionColumnIndex, false);
				}
			}
		},

		'headerclick': function(grid, col, e) {
			if (col === 0) {
				grid.toggleAll();
			}
		},

		grandtotalupdated: function(v, cf) {
			const grid = this;
			grid.onAfterGrandTotalUpdateFieldValue(v, cf);
		}
	},

	// Overridden to support tradeEntryDisabled to make cells un-editable
	startEditing: function(row, col) {
		if (TCG.isTrue(this.getStore().getAt(row).get('tradeEntryDisabled'))) {
			return false;
		}

		TCG.grid.FormGridPanel.superclass.startEditing.apply(this, arguments);
		// need a way to get to grid from editable fields: combo, etc. to get form arguments
		const ed = this.colModel.getCellEditor(col, row);
		if (ed) {
			ed.containerGrid = this;
		}
	},


	onAfterGrandTotalUpdateFieldValue: function(v, cf) {
		// OVERRIDDEN
	},

	onAfterUpdateFieldValue: function(editor, field) {
		const grid = this;
		const f = editor.field;


		if (f === 'buyContracts' || f === 'sellContracts') {
			const v = editor.value;
			if (TCG.isNotBlank(v) && v !== 0) {
				const secId = editor.record.get('security.id');

				// Validation Trade Quantity Violation - Then Validate Trade Date
				// Quantity Violation currently only applies to Overlay Runs, not LDI because LDI doesn't calculate a suggested trade, so only the trade date check is done in that case
				Clifton.portfolio.run.trade.validateTradeQuantityForTradeViolation(editor.record, (f === 'buyContracts') ? v : -v, function() {
					// As long as there is no explicit trade date entered that would be an override
					const tradeDateOverride = TCG.getChildByName(grid.getTopToolbar(), 'tradeDate').getValue();
					if (TCG.isBlank(tradeDateOverride)) {
						// Don't care about the actual result, just want the message if this security will be pushed forward to a future trade date
						Clifton.investment.instrument.getTradeDatePromise(secId, null, grid);
					}
					grid.getView().refresh(false);
				});
			}
		}
		else if (f === 'trade.tradeDestination.name') {
			const tradeDestinationId = editor.record.get('trade.tradeDestination.id');
			const tradeDate = TCG.getChildByName(grid.getTopToolbar(), 'tradeDate').getValue();
			if (tradeDestinationId) {
				TCG.data.getDataPromise('tradeDestinationMappingListByCommandFind.json', grid, {
					params: {
						securityIdForTradeType: editor.record.get('security.id'),
						tradeDestinationId: tradeDestinationId,
						activeOnDate: TCG.isNotBlank(tradeDate) ? tradeDate.format('m/d/Y') : new Date().format('m/d/Y')
					}
				}).then(destinationMappingList => {
					if (destinationMappingList && destinationMappingList.length === 1) {
						const destinationMapping = destinationMappingList[0];
						if (destinationMapping && destinationMapping.executingBrokerCompany) {
							grid.stopEditing();
							const colIndex = grid.colModel.findColumnIndex('trade.executingBrokerCompany.label');
							grid.startEditing(editor.row, colIndex);
							editor.record.set('trade.executingBrokerCompany.id', destinationMapping.executingBrokerCompany.id);
							editor.record.set('trade.executingBrokerCompany.label', destinationMapping.executingBrokerCompany.label);
							const exBrokerEditor = grid.colModel.getCellEditor(colIndex, editor.row);
							exBrokerEditor.setValue({value: destinationMapping.executingBrokerCompany.id, text: destinationMapping.executingBrokerCompany.label});
							grid.stopEditing();
							// Need to mark modified so the submit values are updated (otherwise value isn't updated unless something else is updated)
							grid.markModified();
						}
					}
				});
			}
		}
		else if (f === 'trade.tradeExecutionType.name') {
			const record = editor.record;
			if (record && TCG.isTrue(record.isBticTrade)) {
				record.set('trade.blockTrade', true);
			}
		}

	},


	/***********************************************************************
	 * GRID COLUMN DEFINITIONS -
	 * SPLIT INTO 3 PIECES SO APPROVE/ID AND COMMON TRADE ENTRY CAN BE RE-USED
	 * *********************************************************************/

	// Approve Checkbox and ID Column
	firstColumns: [
		{
			width: 5, dataIndex: 'approveTrade', align: 'center', xtype: 'checkcolumn', sortable: false, filter: false,
			menuDisabled: true,
			tooltip: 'Select rows with pending trades and click Approve Trades to submit them for approval.',
			header: String.format('<div class="x-grid3-check-col{0}">&#160;</div>', ''),
			// Only allows checkbox for those with pending contracts populated
			isCheckEnabled: function(record) {
				return TCG.isNotNull(record.data.pendingContracts);
			},
			fireEditEvent: false,
			renderer: function(v, p, record) {
				if (TCG.isNotNull(record.data.pendingContracts)) {
					p.css += ' x-grid3-check-col-td';
					return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
				}
				return '&nbsp;';
			}
		},
		{header: 'ID', hidden: true, width: 10, dataIndex: 'id'}
	],

	// COLUMNS TO BE OVERRIDDEN
	detailColumns: [],


	// Common Trade Input (Broker, Holding Account, Trade Destination, etc.)
	lastColumns: [
		{
			header: 'Executing Broker', width: 90, dataIndex: 'trade.executingBrokerCompany.label', idDataIndex: 'trade.executingBrokerCompany.id',
			allViews: true,
			editor: {
				xtype: 'combo',
				displayField: 'label',
				queryParam: 'executingBrokerCompanyName',
				detailPageClass: 'Clifton.business.company.CompanyWindow',
				url: 'tradeExecutingBrokerCompanyListFind.json',
				beforequery: function(queryEvent) {
					// Clear Query Store so Re-Queries each time
					this.store.removeAll();
					this.lastQuery = null;

					const record = queryEvent.combo.gridEditor.record;
					if (TCG.isTrue(record.get('tradeEntryDisabled'))) {
						TCG.showError('Trade entry for this row is disabled.');
						return false;
					}

					const tradeDestinationId = record.get('trade.tradeDestination.id');

					queryEvent.combo.store.baseParams = {
						securityIdForTradeType: record.get('security.id'),
						tradeDestinationId: TCG.isNotBlank(tradeDestinationId) && tradeDestinationId > 0 ? tradeDestinationId : null,
						active: true
					};
				}
			}
		},
		{
			header: 'Execution Type', width: 80, dataIndex: 'trade.tradeExecutionType.name', idDataIndex: 'trade.tradeExecutionType.id', useNull: true, hidden: true,
			editor: {
				displayField: 'name', xtype: 'combo', url: 'tradeExecutionTypeListFind.json', requestedProps: 'bticTrade',
				selectHandler: function(combo, record, index) {
					combo.gridEditor.record.isBticTrade = false;
					if (record && TCG.isNotBlank(record.json.bticTrade) && TCG.isTrue(record.json.bticTrade)) {
						const columnModel = combo.gridEditor.containerGrid.getColumnModel();
						const blockTradeColumnIndex = columnModel.findColumnIndex('trade.blockTrade');
						columnModel.setHidden(blockTradeColumnIndex, false);
						combo.gridEditor.record.isBticTrade = true;
					}
				}
			}
		},
		{
			header: 'Limit Price', width: 60, dataIndex: 'trade.limitPrice', useNull: true, type: 'float', hidden: true,
			editor: {xtype: 'pricefield', minValue: 0.01, allowBlank: true},
			renderer: function(value, metaData, record, row) {
				if (TCG.isBlank(value) && (this.editor.valueRequired && TCG.isTrue(this.editor.valueRequired[row]))) {
					metaData.css = 'ruleViolation';
					metaData.attr = TCG.renderQtip('Value is required for defined Trade Execution Type');
				}
				return TCG.renderAmount(value, false, '0,000.0000');
			}
		},
		// See Above onAfterUpdateFieldValue For Trade Destination Selection Change can auto change Executing Broker
		{
			header: 'Trade Destination', dataIndex: 'trade.tradeDestination.name', width: 90, idDataIndex: 'trade.tradeDestination.id',
			allViews: true,
			editor: {
				xtype: 'combo', url: 'tradeDestinationListFind.json', //loadAll: true,
				beforequery: function(queryEvent) {
					// Clear Query Store so Re-Queries each time
					this.store.removeAll();
					this.lastQuery = null;

					const record = queryEvent.combo.gridEditor.record;
					if (TCG.isTrue(record.get('tradeEntryDisabled'))) {
						TCG.showError('Trade entry for this row is disabled.');
						return false;
					}
					const executingBrokerCompanyId = record.get('trade.executingBrokerCompany.id');
					const tradeDate = TCG.getChildByName(this.gridEditor.containerGrid.getTopToolbar(), 'tradeDate').getValue();
					queryEvent.combo.store.baseParams = {
						securityIdForTradeType: record.get('security.id'),
						executingBrokerCompanyId: executingBrokerCompanyId && executingBrokerCompanyId > 0 ? executingBrokerCompanyId : null,
						activeOnDate: TCG.isNotBlank(tradeDate) ? tradeDate.format('m/d/Y') : new Date().format('m/d/Y')
					};
				},

				// Special Copy Handler to only copy where ex broker matches
				copyMenuItemName: 'Copy To All Rows (With Same Executing Broker)',
				copyHandler: function() {
					const editor = this.gridEditor;
					// Get Executing Broker Company ID so we know which rows to copy to
					const exBrokerId = editor.record.get('trade.executingBrokerCompany.id');
					if (TCG.isBlank(exBrokerId) || exBrokerId === -1) {
						TCG.showError('Unable to copy destination to other rows because there is no executing broker selected.  Copy feature for Trade Destinations copies only where the executing broker matches this row.');
						return;
					}
					const grid = editor.containerGrid;
					let i = 0;
					const items = grid.store.data.items;
					let row = items[i];

					while (row) {
						if (row.data['trade.executingBrokerCompany.id'] === exBrokerId) {
							if (row.data['trade.tradeDestination.id'] !== this.getValue()) {
								row.set('trade.tradeDestination.id', this.getValue());
								row.set('trade.tradeDestination.name', this.getRawValue());
							}
						}
						row = items[i++];
					}

					// Refresh Grid So Updates Are Visible
					grid.getView().refresh(false);
					grid.markModified();
				}
			}
		},

		// Hidden by default, for special clients, users can show and edit this field as necessary, although defaults should work for them as they are
		{
			header: 'Holding Account', hidden: true, width: 90, dataIndex: 'trade.holdingInvestmentAccount.label', idDataIndex: 'trade.holdingInvestmentAccount.id',
			editor: {
				xtype: 'combo', displayField: 'label', url: 'portfolioRunTradeDetailHoldingInvestmentAccountList.json',
				root: 'data',
				beforequery: function(queryEvent) {
					const record = queryEvent.combo.gridEditor.record;
					if (TCG.isTrue(record.get('tradeEntryDisabled'))) {
						TCG.showError('Trade entry for this row is disabled.');
						return false;
					}

					// limit holding accounts to client account and security investment type
					const grid = queryEvent.combo.gridEditor.containerGrid;
					const fp = TCG.getParentFormPanel(grid);
					queryEvent.combo.store.baseParams = {runId: fp.getWindow().getMainFormId(), detailId: record.get('id')};
				}
			}
		},

		// Hidden by default, for special use to define algorithms to use for order processing
		{
			header: 'Trade Note', hidden: true, width: 90, dataIndex: 'trade.description',
			editor: {xtype: 'combo', name: 'trade.description', displayField: 'text', tooltipField: 'tooltip', valueField: 'value', url: 'systemListItemListFind.json?listName=Bloomberg Rule Builder Types'}
		},

		{
			header: 'Block', hidden: true, width: 50, dataIndex: 'trade.blockTrade', xtype: 'checkcolumn',
			tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'
		},

		{
			header: 'Close', width: 5, hidden: true, dataIndex: 'closePosition', xtype: 'checkcolumn', sortable: false, filter: false,
			menuDisabled: true,
			tooltip: 'Select row(s) to close current position/exposure. The trade impact will be visible in the grid, and trades will be generated with the Submit Trades action.' +
				' Values in the Buy/Sell column may differ from the trade value for some securities, such as Forwards where the closing trades must be entered in the underlying currency.',
			// Only allows checkbox for those with current contracts populated
			isCheckEnabled: function(record) {
				const currentContracts = record.data.currentContracts;
				if (!TCG.isTrue(record.data.tradeEntryDisabled) && TCG.isNotNull(currentContracts)) {
					const pendingContracts = record.data.pendingContracts;
					return TCG.isNull(pendingContracts) || ((currentContracts + pendingContracts) !== 0);
				}
				return false;
			},
			fireEditEvent: true, // fire markModified
			onClick: function(grid, record, rowIndex, colIndex, value) {
				// edit events do not broadcast for checkcolumns, so manually do it
				if (grid.onAfterEdit) {
					grid.onAfterEdit({grid: grid, record: record, field: 'closePosition', value: value});
				}
			},
			renderer: function(v, p, record) {
				const currentContracts = record.data.currentContracts;
				if (!TCG.isTrue(record.data.tradeEntryDisabled) && TCG.isNotNull(currentContracts)) {
					const pendingContracts = record.data.pendingContracts;
					if (TCG.isNull(pendingContracts) || ((currentContracts + pendingContracts) !== 0)) {
						p.css += ' x-grid3-check-col-td';
						return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
					}
				}
				return '&nbsp;';
			}
		}
	],

	plugins: {ptype: 'gridsummary'},


	validateTradingAllowedRun: function() {
		// get the submit parameters
		const panel = TCG.getParentFormPanel(this);
		const st = TCG.getValue('tradingAllowed', panel.getForm().formValues);
		if (st !== true) {
			TCG.showError('This run record is not available for submitting trades.', 'Trading Prohibited');
			return false;
		}
		return true;
	},

	executeReport: function() {
		const panel = TCG.getParentFormPanel(this);
		const id = TCG.getValue('id', panel.getForm().formValues);
		Clifton.portfolio.run.downloadPortfolioReport(id, this);
	},

	getNoteParams: function() {
		const panel = TCG.getParentFormPanel(this);
		const id = TCG.getValue('id', panel.getForm().formValues);
		return {
			tradeGroupTypeName: 'Portfolio Run',
			sourceFkFieldId: id,
			noteTypeName: 'Client Trade Direction'
		};
	},


	/***********************************************************************
	 * APPROVE TRADES - CHECK/UNCHECK ALL AND HANDLER
	 * *********************************************************************/

	allRows: false,
	toggleAll: function() {
		this.allRows = !TCG.isTrue(this.allRows);
		const grid = this;
		grid.getColumnModel().setColumnHeader(0, String.format('<div class="x-grid3-check-col{0}">&#160;</div>', this.allRows ? '-on' : ''));
		let i = 0;
		let row = grid.store.data.items[i];
		while (row) {
			// Only check it if there are pending contracts
			if (row.data.pendingContracts) {
				row.set('approveTrade', this.allRows);
			}
			row = grid.store.data.items[i++];
		}
	},

	getApproveTradesUrl: function() {
		return 'portfolioRunTradeApprovalProcess.json';
	},

	approveTrades: function() {
		const grid = this;
		let i = 0;
		const items = grid.store.data.items;
		let row = items[i];
		const detailIds = [];
		while (row) {
			if (row.data.approveTrade) {
				detailIds.push(row.data.id);
			}
			// Get the next row
			i = i + 1;
			row = items[i];
		}
		if (detailIds.length === 0) {
			TCG.showError('No rows with pending contracts selected to approve.  Please select at least one row with pending trades to approve.');
			return;
		}
		Ext.Msg.confirm('Approve Trades', 'Are you sure you would like to approve trades for selected rows?', function(a) {
			if (a === 'yes') {
				TCG.data.getDataPromise(grid.getApproveTradesUrl(), grid, {
					waitMsg: 'Submitting Trades for Approval',
					waitTarget: grid,
					params: {
						runId: grid.getWindow().getMainFormId(),
						detailIds: detailIds
					}
				}).then(record => {
					if (record) {
						Ext.Msg.alert('Trades Approved', record);
						TCG.getParentFormPanel(grid).transitionRun('Completed - Trades');
					}
				});
			}
		});
	},

	/***********************************************************************
	 * MISPRICING VIEWS
	 * *********************************************************************/

	loadMispricing: function(initial) {
		if (this.supportMispricingView === false) {
			return;
		}
		const grid = this;
		const fp = TCG.getParentFormPanel(grid);
		const exclude = TCG.isTrue(fp.getForm().findField('excludeMispricing').getValue());
		this.switchToViewColumns(this.currentViewName);

		const cm = this.getColumnModel();
		const l = cm.getColumnCount();
		for (let i = 0; i < l; i++) {
			const c = cm.getColumnById(i);
			// If not hidden - check mispricing column info
			if (TCG.isNotNull(c.mispricingOnly) && !cm.isHidden(i)) {
				if (c.mispricingOnly === exclude) {
					cm.setHidden(i, true);
				}
			}
		}

		// Refresh Grid So Updates Are Visible
		grid.getView().refresh(false);

		if (initial) {
			const tb = this.getTopToolbar();
			// Increment By two because of the | separators
			for (let i = 0; i < tb.items.length; i = i + 2) {
				const button = tb.items.items[i];
				// Last two, if they are there - are the filler and the document format icon
				if (button.text === 'Views') {
					for (let j = 0; j < button.menu.items.length; j++) {
						const mi = button.menu.items.items[j];
						if (mi.text === 'Exclude Mispricing') {
							// true to suppress checked event so it doesn't try to switch it again
							mi.setChecked(exclude === true, true);
						}
						if (mi.text === 'Show Block Column') {
							const columnModel = grid.getColumnModel();
							const blockColumnIndex = columnModel.findColumnIndex('trade.blockTrade');
							mi.setChecked(!TCG.isTrue(columnModel.isHidden(blockColumnIndex)), true);
						}
					}
				}
			}
		}
	},


	switchMispricing: function(checked) {
		const fp = TCG.getParentFormPanel(this);
		fp.setFormValue('excludeMispricing', checked === true, true);
		this.reload();
	},


	/***********************************************************************
	 * NOTE: THIS IS SIMILAR TO GRIDPANEL VIEW SWITCHING,
	 * BUT IS NOT CURRENTLY SUPPORTED ON THE FORM GRID -
	 * SHOULD MOVE TO CORE FUNCTIONALITY
	 * *********************************************************************/
	// each column may define views that it's included in: viewNames: ['SHORT', 'EXPANDED']
	switchToView: function(viewName) {
		this.switchToViewColumns(viewName);
		this.reload();
	},

	currentViewName: undefined,
	switchToViewColumns: function(viewName) {
		this.currentViewName = viewName;
		const cm = this.getColumnModel();
		const l = cm.getColumnCount();
		for (let i = 0; i < l; i++) {
			const c = cm.getColumnById(i);
			if (c.viewNames) {
				cm.setHidden(i, (c.viewNames.indexOf(viewName) === -1));
			}
			else if (c.allViews) {
				// Use allViews for columns that apply to everything and don't change column names
				cm.setHidden(i, c.allViews !== true);
			}
			if (c.viewNameHeaders) {
				for (let j = 0; j < c.viewNameHeaders.length; j++) {
					if (c.viewNameHeaders[j].name === viewName) {
						if (c.viewNameHeaders[j].label) {
							cm.setColumnHeader(i, c.viewNameHeaders[j].label);
						}
					}
				}
			}
		}

		const viewsMenu = this.getTopToolbar().find('text', 'Views');
		if (viewsMenu && viewsMenu.length === 1) {
			const showOptions = viewsMenu[0].menu.findBy(i => i.text && i.text.startsWith('Show'));
			this.switchToViewUpdateViewsToggleOptions(viewName, cm, showOptions);
		}
	},

	switchToViewUpdateViewsToggleOptions: function(viewName, columnModel, showOptions) {
		// can be overridden to update the optional configuration in views for shown fields
	},

	// Call be called after render to default the view to something else before loading the first time
	// Will also update the menu item so it's set as checked
	// But doesn't call "reload" since grid hasn't been loaded yet.
	setDefaultView: function(viewName) {
		this.switchToViewColumns(viewName);
		const tb = this.getTopToolbar();
		// Increment By two because of the | separators
		for (let i = 0; i < tb.items.length; i = i + 2) {
			const button = tb.items.items[i];
			// Last two, if they are there - are the filler and the document format icon
			if (button.text === 'Views') {
				for (let j = 0; j < button.menu.items.length; j++) {
					const mi = button.menu.items.items[j];
					if (mi.text === viewName) {
						// true to suppress checked event so it doesn't try to switch it again
						mi.setChecked(true, true);
					}
				}
			}
		}
	}
};


Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow = function(qtip, fieldLabel, originalValue, currentValue, manualOverrideIfNoDate, currentValueDate) {
	let preparedQtip = qtip;
	if (currentValue) {
		if (TCG.isNotBlank(preparedQtip)) {
			preparedQtip += '<tr><td colspan="3"><hr/></td></tr>';
		}
		else {
			preparedQtip += '<table>';
		}
		if (currentValueDate) {
			preparedQtip += '<tr><td>Current ' + fieldLabel + ':</td><td align="right">' + TCG.numberFormat(currentValue, '0,000', true) + '</td><td>&nbsp;on ' + TCG.renderDate(currentValueDate, 'm/d/Y H:i:s') + '</td></tr>';
		}
		else {
			preparedQtip += '<tr><td>Current ' + fieldLabel + ':</td><td align="right">' + TCG.numberFormat(currentValue, '0,000', true) + '</td>';
			if (TCG.isTrue(manualOverrideIfNoDate)) {
				preparedQtip += '<td>&nbsp;<b>(Manually Overridden)</b></td>';
			}
			preparedQtip += '</tr>';
		}
		preparedQtip += '<tr><td>Previous Close:</td><td align="right">' + TCG.numberFormat(originalValue, '0,000', true) + '</td></tr>';
		preparedQtip += '<tr><td>Change:</td><td align="right">' + TCG.renderAmount(currentValue - originalValue, true) + '</td></tr>';
	}
	return preparedQtip;
};

// nextAlertFunction allows doing second check with alert after this one is accepted.  Message Boxes are singletons so without this the second alert would just overwrite the first
Clifton.portfolio.run.trade.validateTradeQuantityForTradeViolation = function(record, tradeAmount, nextAlertFunction) {
	const suggestedTradeAmount = record.get('Difference');
	if (TCG.isNotBlank(suggestedTradeAmount)) {
		// Separate Bands if Trade is in the Same or Difference Direction as Suggested Trade (Suggested Trade of 0 is considered same direction)
		const sameDirection = (TCG.isEquals(suggestedTradeAmount, 0) || (tradeAmount > 0 === suggestedTradeAmount > 0));
		// Same Direction Rule Applies to Futures Only - so if doesn't apply - move on to next alert
		if (TCG.isTrue(sameDirection) && TCG.isNotEquals('Futures', record.json.security.instrument.hierarchy.investmentType.name)) {
			if (nextAlertFunction) {
				nextAlertFunction();
			}
		}
		else {
			const ruleDefinitionName = 'Portfolio Run Trade Contract Difference (' + (sameDirection ? 'Same Direction' : 'Opposite Direction') + ')';

			const clientAccountId = TCG.getValue('tradingClientAccount.id', record.json);
			const categoryName = 'Trade Rules';
			const cacheKey = categoryName + '_' + ruleDefinitionName + '_' + clientAccountId;
			const url = 'ruleConfigMaxAmountForAdditionalScopeEntityAndDefinition.json?categoryName=' + categoryName + '&definitionName=' + ruleDefinitionName + '&additionalScopeEntityId=' + clientAccountId;
			TCG.data.getDataValuePromiseUsingCaching(url, null, cacheKey).then(
				function(value) {
					const difference = suggestedTradeAmount - tradeAmount;
					if (!TCG.isBlank(value) && (Math.abs(difference) > value)) {
						const msg = 'Trade amount for security ' + record.get('security.symbol') + ' [' + TCG.numberFormat(tradeAmount, '0,000') + '] is in the ' + (sameDirection ? 'same' : 'opposite') + ' direction of the system suggested trade amount of [' + TCG.numberFormat(suggestedTradeAmount, '0,000.00') + '] and is outside the suggested band of [' + TCG.numberFormat(value, '0,000') + '].  Please confirm trade amount.';
						Ext.Msg.alert('Trade Quantity Violation', msg, function() {
							if (nextAlertFunction) {
								nextAlertFunction();
							}
						});
					}
					else {
						if (nextAlertFunction) {
							nextAlertFunction();
						}
					}
				}
			);
		}
	}
	else {
		if (nextAlertFunction) {
			nextAlertFunction();
		}
	}
};

Clifton.portfolio.run.trade.findRow = function(grid, id) {
	let i = 0;
	const items = grid.store.data.items;
	let row = items[i];
	while (row) {
		if (row.data['id'] === id) {
			return row;
		}
		i = i + 1;
		row = items[i];
	}
	return undefined;
};

Clifton.portfolio.run.trade.updateRecordOpenCloseType = function(grid, record, buy, silent) {
	const currentContracts = record.get('currentContractsAdjusted') + record.get('pendingContracts'),
		optionSecurity = (TCG.getValue('security.instrument.hierarchy.investmentType.name', record.json) === 'Options'),
		tradeContracts = record.get((buy ? 'buyContracts' : 'sellContracts'));
	const openCloseTypeName = TCG.isTrue(buy)
		? 'Buy' + (optionSecurity ? ((currentContracts < 0) ? ' to Close' : ' to Open') : '')
		: 'Sell' + (optionSecurity ? ((currentContracts > 0) ? ' to Close' : ' to Open') : '');
	if (TCG.isBlank(tradeContracts) || tradeContracts === 0) {
		// clear out the openCloseType value, -1 will result in null upon binding on the server
		record.set('openCloseType.id', -1);
		record.set('openCloseType.name', '');
		if (!TCG.isTrue(silent)) {
			grid.markModified();
		}
	}
	else {
		TCG.data.getDataPromiseUsingCaching(`tradeOpenCloseTypeByName.json?name=${openCloseTypeName}`, grid, `openCloseType.${openCloseTypeName}`)
			.then(function(openCloseType) {
				if (openCloseType && openCloseType.id !== record.get('openCloseType.id')) {
					record.set('openCloseType.id', openCloseType.id);
					record.set('openCloseType.name', openCloseType.name);
					if (!TCG.isTrue(silent)) {
						grid.markModified();
					}
				}
			});
	}
};
