Clifton.portfolio.run.trade.RunTradeGroupConfirmWindow = Ext.extend(TCG.app.Window, {
	title: 'Portfolio Run Trade Creation Review',
	iconCls: 'run',
	height: 430,
	width: 750,
	modal: true,
	centerAlign: true,
	buttonAlign: 'center',

	buttons: [{
		text: 'Close Trade Creation Window',
		tooltip: 'Closes Portfolio Run Trade Creation Screen',
		width: 180,
		handler: function() {
			this.ownerCt.ownerCt.handleAction('CLOSE');
		}
	}, {
		text: 'Return to Trade Creation Window',
		tooltip: 'Returns to Portfolio Run Trade Creation Screen',
		width: 180,
		handler: function() {
			this.ownerCt.ownerCt.handleAction('RETURN');
		}
	}, {
		text: 'Open Trade Group Window',
		tooltip: 'Opens Trade Group window for these Trades',
		width: 180,
		handler: function() {
			this.ownerCt.ownerCt.handleAction('TRADE');
		}
	}
	],

	handleAction: function(action) {
		const win = this;
		if (action === 'CLOSE') {
			if (win.openerCt) {
				win.openerCt.closeWindow();
			}
		}
		if (action === 'TRADE') {
			const grpId = win.params.id;
			const detailPageClass = 'Clifton.trade.group.DefaultTradeGroupWindow';
			TCG.createComponent(detailPageClass, {
				id: TCG.getComponentId(detailPageClass, grpId),
				params: {id: grpId}
			});
		}
		// action == 'RETURN' - always close this window
		this.closeWindow();

	},

	closeWindow: function(closeOpener) {
		const win = this;
		win.closing = true;

		if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload) {
			win.openerCt.reload(win);
		}
		win.close();
	},

	items: [{
		xtype: 'formpanel',
		items: [
			{
				xtype: 'drag-drop-container',
				message: 'Drag and drop file here to create <b>Client Trade Direction</b> note for the following trades and attach this file to it.',
				allowMultiple: false,
				saveUrl: 'tradeNoteForTradeGroupUpload.json',
				getParams: function() {
					const panel = TCG.getParentFormPanel(this);
					const id = TCG.getValue('id', panel.getForm().formValues);
					return {
						tradeGroupId: id,
						noteTypeName: 'Client Trade Direction'
					};
				}
			},
			{xtype: 'hidden', name: 'id'},
			{
				xtype: 'formgrid-scroll',
				title: 'The following trades have been submitted:',
				storeRoot: 'tradeList',
				dtoClass: 'com.clifton.trade.Trade',
				readOnly: true,
				height: 300,
				heightResized: true,
				collapsible: false,
				detailPageClass: 'Clifton.trade.TradeWindow',
				anchor: '-20',

				columnsConfig: [
					{
						header: 'Workflow State', width: 125, dataIndex: 'workflowState.name',
						renderer: function(value, metaData, r) {
							if (value === 'Invalid') {
								metaData.css = 'ruleViolation';
							}
							return value;
						}
					},
					{
						header: 'Buy/Sell', width: 80, dataIndex: 'buy', type: 'boolean',
						renderer: function(v, metaData) {
							metaData.css = v ? 'buy-light' : 'sell-light';
							return v ? 'BUY' : 'SELL';
						}
					},
					{header: 'Quantity', width: 80, dataIndex: 'quantityIntended', type: 'float', useNull: true},
					{header: 'Security', width: 100, dataIndex: 'investmentSecurity.symbol'},
					{header: 'Accounting Notional', width: 125, dataIndex: 'accountingNotional', type: 'currency', useNull: true},
					{header: 'Traded On', width: 80, dataIndex: 'tradeDate'},
					{header: 'Settled On', width: 80, dataIndex: 'settlementDate'}
				]
			}]
	}]
});
