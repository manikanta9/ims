// Trades tab - View trade history for account or account & all sub-accounts
Clifton.portfolio.run.trade.RunTradeHistoryGrid = {
	name: 'tradeListFind.json',
	xtype: 'gridpanel',
	instructions: 'The following is a list of trades tied to the client account (or sub-account) entered after the Portfolio Run balance date.  Note for Closed runs, the list of trades will show only those entered within 7 days after the balance date.',
	requestIdDataIndex: true,
	rowSelectionModel: 'checkbox',

	getTopToolbarFilters: function(toolbar) {
		return [
			{
				boxLabel: 'Include Sub-Account(s)', xtype: 'checkbox', name: 'includeSubAccounts',
				listeners: {
					check: function(field) {
						TCG.getParentByClass(field, Ext.Panel).reload();
					}
				}
			}
		];
	},

	getLoadParams: function(firstLoad) {
		const f = this.getWindow().getMainForm();
		if (firstLoad) {
			const dateV = TCG.parseDate(TCG.getValue('balanceDate', f.formValues));
			const workflowStatus = TCG.getValue('workflowStatus.name', f.formValues);
			if (workflowStatus === 'Closed') {
				this.setFilterValue('tradeDate', {'after': dateV, 'before': dateV.add(Date.DAY, 7)});
			}
			else {
				this.setFilterValue('tradeDate', {'after': dateV});
			}
		}
		// Show/Hide Client Account Column based on including sub-accounts
		const includeSub = TCG.getChildByName(this.getTopToolbar(), 'includeSubAccounts').getValue();
		const i = this.getColumnModel().findColumnIndex('clientInvestmentAccount.label');
		this.getColumnModel().config[i].hidden = (includeSub === false);
		this.updateAllColumnWidthsFORCE();

		const acctId = TCG.getValue('clientInvestmentAccount.id', f.formValues);
		const params = {readUncommittedRequested: true};
		if (includeSub === true) {
			params.clientInvestmentAccountIdOrSubAccount = acctId;
		}
		else {
			params.clientInvestmentAccountId = acctId;
		}
		return params;
	},
	remoteSort: true,
	columns: [
		{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
		{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade', showNotEquals: true}},
		{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
		{header: 'Trade Type', width: 11, dataIndex: 'tradeType.name', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json'}},
		{header: 'Client Account', width: 40, dataIndex: 'clientInvestmentAccount.label', hidden: true, filter: false},
		{header: 'Holding Account', width: 40, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
		{header: 'Executing Broker', width: 18, dataIndex: 'executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerId', url: 'businessCompanyListFind.json?issuer=true'}},
		{
			header: 'Buy/Sell', width: 8, dataIndex: 'buy', type: 'boolean',
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{header: 'Quantity', width: 10, dataIndex: 'quantityIntended', type: 'float', useNull: true},
		{header: 'Security', width: 15, dataIndex: 'investmentSecurity.symbol', idDataIndex: 'investmentSecurity.id', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Security Name', width: 40, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
		{header: 'Exchange Rate', width: 12, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true},
		{header: 'Average Price', width: 14, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
		{header: 'Commission Per Unit', width: 10, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
		{header: 'Fee', width: 10, dataIndex: 'feeAmount', type: 'float', hidden: true},
		{header: 'Accounting Notional', width: 18, dataIndex: 'accountingNotional', type: 'currency', useNull: true},
		{header: 'Novation', width: 10, dataIndex: 'novation', type: 'boolean', hidden: true},
		{header: 'Assigned Counterparty', width: 25, dataIndex: 'assignmentCompany.name', hidden: true, useNull: true, filter: {type: 'combo', searchFieldName: 'assignmentCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
		{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
		{header: 'Team', width: 12, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', hidden: true, filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}},
		{header: 'Destination', width: 12, dataIndex: 'tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}},
		{header: 'Execution Type', width: 12, dataIndex: 'tradeExecutionType.name', filter: {searchFieldName: 'tradeExecutionTypeName'}},
		{header: 'Trade Note', width: 15, dataIndex: 'description', renderer: TCG.renderTextNowrapWithTooltip},
		{header: 'Limit Price', width: 10, dataIndex: 'limitPrice', type: 'float', useNull: true},
		{header: 'Traded On', width: 13, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: 'Settled On', width: 13, dataIndex: 'settlementDate', hidden: true}
	],
	editor: {
		detailPageClass: 'Clifton.trade.TradeWindow',
		drillDownOnly: true,
		ptype: 'workflowAwareEntity-grideditor',
		executeMultipleInBatch: true,
		// ignore all response data, which is ignored to speed up each row's transition request
		requestedPropertiesToExclude: 'workflowTransitionCommand',
		tableName: 'Trade',
		transitionWorkflowStateList: [
			{stateName: 'Approved', iconCls: 'run', buttonText: 'Approve Selected', buttonTooltip: 'Approved Selected Trades'},
			{stateName: 'Cancelled', iconCls: 'run', buttonText: 'Cancel Selected', buttonTooltip: 'Cancel Selected Trades', requireConfirm: true}
		],
		validateRowsToBeTransitioned: Clifton.trade.validateWorkflowTransition
	}
};
