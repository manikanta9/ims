Clifton.portfolio.run.trade.RunTradeCurrencyPositionGrid = {
	xtype: 'formgrid-scroll',
	height: 205,
	title: 'Physical Currency Positions',
	collapsible: true,
	viewConfig: {forceFit: true},
	readOnly: true,
	storeRoot: 'currencyTradeList',
	dtoClass: 'com.clifton.product.overlay.trade.ProductOverlayCurrencyTrade',// 'com.clifton.portfolio.run.trade.PortfolioCurrencyOther', // TODO update
	// Don't send Currency Trade List to server at all
	markModified: function(initOnly) {
		this.submitField.setValue(''); // changed value
		if (initOnly) {
			this.submitField.originalValue = ''; // initial value
		}
	},
	listeners: {
		afterRender: function() {
			const grid = this;
			const fp = TCG.getParentFormPanel(this);
			fp.on('detailRefresh', function(record) {
				grid.store.loadData(record);
			}, this);
		},
		rowclick: function(grid, rowIndex, evt) {
			if (TCG.isActionColumn(evt.target)) {
				const clz = 'Clifton.trade.CurrencyTradeWindow';
				const row = grid.store.data.items[rowIndex];
				let clientAccount = TCG.getValue('replication.tradingClientAccount', row.json);
				if (TCG.isNull(clientAccount)) {
					clientAccount = TCG.getValue('clientInvestmentAccount', TCG.getParentFormPanel(this).getForm().formValues);
				}
				const defaultData = {
					clientInvestmentAccount: clientAccount,
					investmentSecurity: TCG.getValue('currencySecurity', row.json),
					payingSecurity: clientAccount.baseCurrency
				};
				TCG.createComponent(clz, {
					tradeType: 'Currency',
					defaultData: defaultData,
					openerCt: this
				});
			}
		}
	},
	columnsConfig: [
		{header: 'Currency', width: 70, dataIndex: 'currencySecurity.symbol', idDataIndex: 'currencySecurity.id'},
		{header: 'Local Amount (Original)', width: 75, dataIndex: 'localAmount', type: 'currency', numberFormat: '0,000', useNull: true, hidden: true},
		{
			header: 'Local Amount', width: 75, dataIndex: 'currentLocalAmount', type: 'currency', numberFormat: '0,000', useNull: true,
			renderer: function(v, metaData, r) {
				const act = r.data.localAmount;
				const cur = r.data.currentLocalAmount;
				if (TCG.isNotNull(act) && act !== cur) {
					let qtip = '<table><tr><td>Previous Close Amount:</td><td align="right">' + Ext.util.Format.number(act, '0,000') + '</td></tr>';
					qtip += '<tr><td>Change:</td><td>' + TCG.renderAmount(cur - act, true) + '</td></tr>';
					qtip += '</table>';
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return Ext.util.Format.number(v, '0,000');
			}
		},
		{header: 'Local Unrealized CCY Exposure (Original)', width: 75, dataIndex: 'unrealizedLocalAmount', type: 'currency', numberFormat: '0,000', hidden: true, useNull: true},
		{
			header: 'Local Unrealized CCY Exposure', width: 75, dataIndex: 'currentUnrealizedLocalAmount', type: 'currency', numberFormat: '0,000', useNull: true,
			tooltip: 'Unrealized Exposure is added when: CCY replication is a matching replication and ONLY includes exposure from positions in the same asset class, or there is no CCY Replication and full CCY unrealized is added here.',
			renderer: function(v, metaData, r) {
				const act = r.data.unrealizedLocalAmount;
				const cur = r.data.currentUnrealizedLocalAmount;
				if (TCG.isNotNull(act) && act !== cur) {
					let qtip = '<table><tr><td>Previous Close Amount:</td><td align="right">' + Ext.util.Format.number(act, '0,000') + '</td></tr>';
					qtip += '<tr><td>Change:</td><td>' + TCG.renderAmount(cur - act, true) + '</td></tr>';
					qtip += '</table>';
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return Ext.util.Format.number(v, '0,000');
			}
		},

		{header: 'Local Amount (Pending)', width: 80, dataIndex: 'pendingLocalAmount', type: 'currency', numberFormat: '0,000'},
		{
			header: 'Total', width: 75, type: 'currency', numberFormat: '0,000',
			renderer: function(v, metaData, r) {
				const act = r.data.localAmount + r.data.unrealizedLocalAmount;
				const cur = r.data.currentLocalAmount + r.data.pendingLocalAmount + r.data.currentUnrealizedLocalAmount;
				if (TCG.isNotNull(act) && act !== cur) {
					let qtip = '<table><tr><td>Previous Close Amount:</td><td align="right">' + Ext.util.Format.number(act, '0,000') + '</td></tr>';
					qtip += '<tr><td>Change:</td><td>' + TCG.renderAmount(cur - act, true) + '</td></tr>';
					qtip += '</table>';
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return Ext.util.Format.number(cur, '0,000');
			}
		},
		{header: 'Exchange Rate (Original)', width: 75, dataIndex: 'exchangeRate', type: 'currency', numberFormat: '0,000.0000', hidden: true},
		{header: 'Current Exchange Rate Date', width: 75, dataIndex: 'currentExchangeRateDate', type: 'date', hidden: true},
		{
			header: 'Exchange Rate', width: 70, dataIndex: 'currentExchangeRate', type: 'currency', numberFormat: '0,000.00000000',
			renderer: function(v, metaData, r) {
				const current = Ext.util.Format.number(r.data.currentExchangeRate, '0,000.00000000');
				const original = Ext.util.Format.number(r.data.exchangeRate, '0,000.00000000');
				if (current !== original) {
					let qtip = '<table><tr><td>&nbsp;</td></tr>';
					qtip += '<tr><td>Current Exchange Rate:</td><td align="right">' + current + '</td><td>on ' + TCG.renderDate(r.data.currentExchangeRateDate, 'm/d/Y H:i:s') + '</td></tr>';
					qtip += '<tr><td>Previous Exchange Rate:</td><td align="right">' + original + '</td></tr>';
					qtip += '<tr><td>Change:</td><td align="right">' + TCG.renderAmount(current - original, true, '0,000.00000000') + '</td></tr>';
					qtip += '</table>';
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return current;
			}
		},
		{
			header: 'Total Base Amount', width: 75, type: 'currency', summaryType: 'sum',
			renderer: function(v, metaData, r) {
				return Ext.util.Format.number((r.data.currentLocalAmount + r.data.pendingLocalAmount + r.data.currentUnrealizedLocalAmount) * r.data.currentExchangeRate, '0,000');
			}
		},
		{
			header: '', width: 20,
			renderer: function(url, args) {
				return TCG.renderActionColumn('currency', '', 'Create Currency Trade');
			}
		}
	],
	plugins: [
		{
			ptype: 'trade-contextmenu-plugin',
			recordSecurityIdPath: 'currencySecurity.id',
			recordClientAccountIdPath: 'clientInvestmentAccount.id',
			recordLocalNotionalPath: 'currentLocalAmount',

			getRecordInvestmentAccount: async function(record, accountPath) {
				const accountId = TCG.getValue(accountPath, TCG.getParentFormPanel(this.grid).getForm().formValues);
				if (TCG.isBlank(accountId)) {
					return;
				}
				const account = await TCG.data.getDataPromise(`investmentAccount.json?id=${accountId}`, this.grid);
				return account;
			}
		}
	]
};
