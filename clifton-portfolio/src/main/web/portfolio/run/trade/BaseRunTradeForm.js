Clifton.portfolio.run.trade.BaseRunTradeForm = Ext.extend(TCG.form.FormPanel, {
	url: 'portfolioRunTrade.json',

	listRequestedPropertiesToExclude: '',

	listRequestedPropertiesRoot: 'data.detailList',
	requestedMaxDepth: 7,

	// NOTE: ALL CHANGES MUST ALSO BE APPLIED TO THE WINDOW SELECTOR CLASS DEFINITION
	// Find class definition under Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides
	listRequestedProperties: undefined,

	getListRequestedProperties: function() {
		// Use a function instead because LDI returns standard properties and dynamic dv01 map properties to request for each point
		return this.listRequestedProperties;
	},


	getSaveURL: function() {
		return 'portfolioRunTradeCreationProcess.json?requestedPropertiesToExclude=portfolioRunTrade';
	},

	loadValidation: false,
	loadDefaultDataAfterRender: true,

	table: 'PortfolioRun',
	transitionRun: function(stateName) {
		const tb = this.ownerCt.getTopToolbar();
		for (let i = 2; i < tb.items.length; i++) {
			const tran = tb.items.get(i).transition;
			if (tran && tran.endWorkflowState.name === stateName) {
				tb.doTransition(tran);
			}
		}
	},

	getLoadParams: function(win) {
		const params = win.params;
		params.requestedProperties = this.getListRequestedProperties();
		params.requestedPropertiesRoot = this.listRequestedPropertiesRoot;
		params.requestedPropertiesToExclude = this.listRequestedPropertiesToExclude;
		params.requestedMaxDepth = this.requestedMaxDepth;
		return params;
	},

	confirmBeforeSaveMsg: 'Are you sure you want to submit trades for all buys/sells entered?',
	confirmBeforeSaveMsgTitle: 'Submit Trades',


	getWarningMessage: function(form) {
		let msg = undefined;
		if (!form.formValues) {
			return;
		}
		if (form.formValues.closed === true) {
			msg = 'This run record is closed. Trading is now available on a newer run for this account.';
		}
		else if (form.formValues.tradingAllowed === false) {
			msg = 'This run record is in workflow state [' + form.formValues.workflowState.name + ' / ' + form.formValues.workflowStatus.name + '].  Submitting Trades is not available from this state.';
		}
		return msg;
	},
	defaults: {anchor: '0'}
});
Ext.reg('portfolio-run-trade-form', Clifton.portfolio.run.trade.BaseRunTradeForm);
