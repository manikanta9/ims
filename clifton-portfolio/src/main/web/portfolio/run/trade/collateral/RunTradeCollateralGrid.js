Clifton.portfolio.run.trade.collateral.RunTradeCollateralGrid = Ext.extend(TCG.grid.FormGridPanel, {
	name: 'productOverlayCollateralMaturityList',
	dtoClass: 'com.clifton.product.overlay.ProductOverlayCollateralMaturity',
	storeRoot: 'data',
	autoHeight: false,
	forceFit: true,
	groupField: 'holdingAccount.number',
	groupTextTpl: '{values.text} with target ${[TCG.numberFormat(values.rs[0].data["targetExposure"], "0,000.00")]} ({[values.rs.length]} {[values.rs.length > 1 ? "Positions" : "Position"]})',
	instructions: 'This grid displays current collateral positions and facilitates trading to manage collateral. Positions are shown by holding account in the order in which they mature.',
	plugins: {ptype: 'gridsummary'},
	totalUpdatedRecords: [], // array to track refreshing of records during group summary differences in a way that does not create infinite recursion
	listeners: {
		afterrender: function(gridPanel) {
			gridPanel.reloadGrid();
		},
		grandtotalupdated: function(value, column) {
			const grid = column.gridPanel;
			if (grid.totalUpdatedRecords.length > 0 && !TCG.isTrue(grid.totalUpdatedRecordActive)) {
				grid.totalUpdatedRecordActive = true;
				const store = grid.getStore();
				grid.totalUpdatedRecords.forEach(record => store.fireEvent('update', store, record, Ext.data.Record.EDIT));
				grid.totalUpdatedRecords = [];
				grid.totalUpdatedRecordActive = false;
			}
		}
	},
	doNotSubmitFields: 'investmentType.name|allocationPercent|security.endDate|security.priceMultiplier|duration|yield|targetExposure|currentExposure|pendingExposure|exposure|exposureDifference|targetQuantity|quantity|quantityDifference|pendingQuantity|tradeImpact|finalExposure|finalAllocationPercent|finalDifference|tradingDisabled',
	launchEditorWindow: function(componentName, id, defaultData) {
		if (TCG.isNotBlank(componentName) && TCG.isNotBlank(id)) {
			const config = {openerCt: this};
			if (TCG.isNotBlank(id)) {
				config.id = TCG.getComponentId(componentName, id);
				config.params = {id: id};
			}
			if (TCG.isNotNull(defaultData)) {
				config.defaultData = defaultData;
			}
			TCG.createComponent(componentName, config);
		}
	},
	columnsConfig: [
		{
			header: 'Client Account', width: 70, dataIndex: 'clientAccount.number', idDataIndex: 'clientAccount.id',
			eventListeners: {
				dblclick: function(column, gridPanel, rowIndex, event) {
					const accountId = TCG.getValue('clientAccount.id', gridPanel.getStore().getAt(rowIndex).json);
					gridPanel.launchEditorWindow.call(gridPanel, 'Clifton.investment.account.CliftonAccountWindow', accountId);
				}
			}
		},
		{
			header: 'Holding Account', width: 70, dataIndex: 'holdingAccount.number', idDataIndex: 'holdingAccount.id',
			// include in summary for filtering only
			hideGroupTotal: true, hideGrandTotal: true,
			summaryCalculation: function(v, r, field, data, col) {
				return (r && r.data) ? r.data['holdingAccount.number'] : 0;
			},
			eventListeners: {
				dblclick: function(column, gridPanel, rowIndex, event) {
					const accountId = TCG.getValue('holdingAccount.id', gridPanel.getStore().getAt(rowIndex).json);
					gridPanel.launchEditorWindow.call(gridPanel, 'Clifton.investment.account.CliftonAccountWindow', accountId);
				}
			}
		},
		{
			header: 'Security', width: 75, dataIndex: 'security.symbol', idDataIndex: 'security.id',
			eventListeners: {
				dblclick: function(column, gridPanel, rowIndex, event) {
					const securityId = TCG.getValue('security.id', gridPanel.getStore().getAt(rowIndex).json);
					gridPanel.launchEditorWindow.call(gridPanel, 'Clifton.investment.instrument.SecurityWindow', securityId);
				}
			}
		},
		{header: 'Security Type', width: 60, dataIndex: 'investmentType.name', hidden: true},
		{header: 'Security Price', width: 65, dataIndex: 'securityPrice', type: 'float'},
		{header: 'Price Multiplier', width: 65, dataIndex: 'security.priceMultiplier', type: 'float', hidden: true},

		{header: 'Allocation %', width: 50, type: 'currency', dataIndex: 'allocationPercent', summaryType: 'sum'},

		{header: 'Duration', width: 50, type: 'currency', dataIndex: 'duration', numberFormat: '0,000.0000', useNull: true, hidden: true},
		{header: 'Yield', width: 50, type: 'currency', dataIndex: 'yield', useNull: true, hidden: true},
		{header: 'Maturity', width: 55, dataIndex: 'security.endDate', tooltip: 'The date the security matures', css: 'BORDER-RIGHT: #c0c0c0 1px solid;'},

		{header: 'Target', width: 60, dataIndex: 'targetExposure', type: 'currency', numberFormat: '0,000', negativeInRed: true, hidden: true},
		{
			header: 'Current Exposure', width: 60, dataIndex: 'currentExposure', type: 'currency', numberFormat: '0,000', negativeInRed: true, summaryType: 'sum', tooltip: 'Current exposure based on current positions booked to the general ledger',
			summaryRenderer: function(value, args, record, column) {
				if (column && column.gridPanel) {
					column.gridPanel.getStore().each(gridRecord => {
						if (record.data['holdingAccount.number'] === gridRecord.data['holdingAccount.number']) {
							const exposureTarget = gridRecord.get('targetExposure') || 0;
							if (exposureTarget !== 0) {
								const currentExposureDifference = exposureTarget - value;
								if (gridRecord.data['exposureDifference'] !== currentExposureDifference) {
									gridRecord.data['exposureDifference'] = currentExposureDifference;

									if (column.gridPanel.totalUpdatedRecords.indexOf(gridRecord) === -1) {
										column.gridPanel.totalUpdatedRecords.push(gridRecord);
									}
								}
							}
						}
					});
				}
				return TCG.renderAmount(value, false, '0,000');
			}
		},
		{header: 'Pending Exposure', width: 60, dataIndex: 'pendingExposure', type: 'currency', numberFormat: '0,000', negativeInRed: true, summaryType: 'sum', hidden: true, tooltip: 'Pending exposure based on pending activity not yet booked to the general ledger'},
		{
			header: 'Exposure', width: 60, dataIndex: 'exposure', type: 'currency', numberFormat: '0,000', negativeInRed: true, summaryType: 'sum', hidden: true, tooltip: 'Live exposure based on current positions booked to the general ledger and pending activity not yet booked to the general ledger',
			summaryRenderer: function(value, args, record, column) {
				if (column && column.gridPanel) {
					column.gridPanel.getStore().each(gridRecord => {
						if (record.data['holdingAccount.number'] === gridRecord.data['holdingAccount.number']) {
							const exposureTarget = gridRecord.get('targetExposure') || 0;
							if (exposureTarget !== 0) {
								const exposureDifference = exposureTarget - value;
								const contractValue = (gridRecord.data.securityPrice * gridRecord.data['security.priceMultiplier']);
								if (contractValue && contractValue !== 0) {
									const newValue = (exposureDifference / contractValue);
									if (gridRecord.data['quantityDifference'] !== newValue) {
										gridRecord.data['quantityDifference'] = newValue;

										if (column.gridPanel.totalUpdatedRecords.indexOf(gridRecord) === -1) {
											column.gridPanel.totalUpdatedRecords.push(gridRecord);
										}
									}
								}
							}
						}
					});
				}
				return TCG.renderAmount(value, false, '0,000');
			}
		},
		{header: 'Difference', width: 60, dataIndex: 'exposureDifference', type: 'currency', numberFormat: '0,000', negativeInRed: true, css: 'BORDER-RIGHT: #c0c0c0 1px solid;'},

		{header: 'Target Contracts', width: 60, dataIndex: 'targetQuantity', type: 'currency', numberFormat: '0,000', negativeInRed: true, hidden: true},
		{header: 'Current Contracts', width: 60, dataIndex: 'currentQuantity', type: 'currency', numberFormat: '0,000', negativeInRed: true, tooltip: 'Current quantity booked to the general ledger'},
		{
			header: 'Pending Contracts', width: 60, dataIndex: 'pendingQuantity', type: 'currency', numberFormat: '0,000', negativeInRed: true, tooltip: 'Pending quantity from created trades or transfers that are not yet booked to the general ledger',
			eventListeners: {
				dblclick: async function(column, grid, rowIndex, event) {
					const record = grid.getStore().getAt(rowIndex);
					const pendingQuantity = TCG.getValue('pendingQuantity', record.json);
					if (TCG.isNull(pendingQuantity) || pendingQuantity === 0) {
						// new record with no pending
						return false;
					}

					const mainFormPanel = grid.getWindow().getMainFormPanel();
					const params = {
						clientInvestmentAccountId: mainFormPanel.getFormValue('clientInvestmentAccount.id'),
						holdingInvestmentAccountId: TCG.getValue('holdingAccount.id', record.json),
						securityId: TCG.getValue('security.id', record.json),
						excludeWorkflowStatusName: 'Closed',
						requestedPropertiesRoot: 'data',
						requestedProperties: 'id'
					};
					TCG.data.getDataPromise('tradeListFind.json', grid, {params: params})
						.then(pendingTradeList => {
							if (pendingTradeList && pendingTradeList.length > 0) {
								// open the first pending trade
								const tradeId = pendingTradeList[0].id;
								TCG.createComponent('Clifton.trade.TradeWindow', {
									id: TCG.getComponentId('Clifton.trade.TradeWindow', tradeId),
									params: {id: tradeId},
									openerCt: grid
								});
							}
						});
				}
			}
		},
		{header: 'Difference', width: 60, dataIndex: 'quantityDifference', type: 'currency', numberFormat: '0,000', negativeInRed: true, tooltip: 'The difference this position is from the target amount'},

		// Trading Fields
		{
			header: 'Buy', width: 60, dataIndex: 'buy', type: 'int', useNull: true, summaryType: 'sum', numberFormat: '0,000', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			editor: {
				xtype: 'spinnerfield', allowBlank: true, minValue: 1, maxValue: 1000000000,
				listeners: {
					focus: function(field) {
						const record = this.gridEditor.record;
						if (TCG.isTrue(TCG.getValue('tradingDisabled', record.json))) {
							// avoid changing for non-tradable
							field.gridEditor.disable();
							field.spinner.disabled = true;
							return false;
						}
						else {
							field.gridEditor.enable();
							field.spinner.disabled = false;
						}
					}
				}
			}
		},
		{
			header: 'Sell', width: 60, dataIndex: 'sell', type: 'int', useNull: true, summaryType: 'sum', numberFormat: '0,000', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid; BORDER-RIGHT: #c0c0c0 1px solid;',
			editor: {
				xtype: 'spinnerfield', allowBlank: true, minValue: 1, maxValue: 1000000000,
				listeners: {
					focus: function(field) {
						const record = this.gridEditor.record;
						if (TCG.isTrue(TCG.getValue('tradingDisabled', record.json))) {
							// avoid changing for non-tradable
							field.gridEditor.disable();
							field.spinner.disabled = true;
							return false;
						}
						else {
							field.gridEditor.enable();
							field.spinner.disabled = false;
						}
					}
				}
			}
		},
		{
			header: 'Trade Impact', dataIndex: 'tradeImpact', width: 60, type: 'currency', numberFormat: '0,000', useNull: true, negativeInRed: true, summaryType: 'sum',
			tooltip: 'Impact from the Buy or Sell trade entered',
			renderer: function(v, p, r) {
				const priceMultiplier = r.data['security.priceMultiplier'] || .01;
				let netTradeQuantity = TCG.isNumber(r.data.buy) ? r.data.buy : 0;
				if (TCG.isNumber(r.data.sell)) {
					netTradeQuantity -= r.data.sell;
				}
				const exp = Clifton.portfolio.run.trade.calculateExposure(r, netTradeQuantity, (r.data.securityPrice * priceMultiplier));
				r.data.tradeImpact = exp;
				return (exp === 0) ? '' : TCG.renderAmount(exp, false, '0,000');
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},
		// Final Results - Exposure, etc. Based on All Actual, Current, Pending, and Entered Trades
		{
			header: 'Final Exposure', dataIndex: 'finalExposure', width: 60, type: 'currency', numberFormat: '0,000', negativeInRed: true, summaryType: 'sum',
			tooltip: 'Exposure + Trade Impact',
			renderer: function(v, p, r) {
				const finalExp = (TCG.isNumber(r.data.exposure) ? r.data.exposure : 0) + r.data.tradeImpact;
				r.data.finalExposure = finalExp;
				return TCG.renderAmount(finalExp, false, '0,000');
			},
			summaryRenderer: function(value, args, record, column) {
				if (column && column.gridPanel) {
					column.gridPanel.getStore().each(gridRecord => {
						if (record.data['holdingAccount.number'] === gridRecord.data['holdingAccount.number']) {
							const exposureTarget = gridRecord.get('targetExposure') || 0;
							const newValue = (exposureTarget - value);
							if (gridRecord.data['finalDifference'] !== newValue) {
								gridRecord.data['finalDifference'] = newValue;

								if (column.gridPanel.totalUpdatedRecords.indexOf(gridRecord) === -1) {
									column.gridPanel.totalUpdatedRecords.push(gridRecord);
								}
							}
						}
					});
				}
				return TCG.renderAmount(value, false, '0,000');
			}
		},
		{
			header: 'Final Alloc. %', dataIndex: 'finalAllocationPercent', width: 50, type: 'currency', summaryType: 'sum',
			renderer: function(v, p, r) {
				const finalAllocationPercent = (r.data.finalExposure / r.data.targetExposure) * 100;
				r.data.finalAllocationPercent = finalAllocationPercent;
				return TCG.renderAmount(finalAllocationPercent, false, '0,000.00 %');
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000.00 %');
			}
		},
		{header: 'Final Difference', dataIndex: 'finalDifference', width: 60, type: 'currency', numberFormat: '0,000', css: 'BORDER-RIGHT: #c0c0c0 1px solid;', tooltip: 'Target - Final Exposure'},
		{
			header: 'Executing Broker', width: 80, dataIndex: 'trade.executingBrokerCompany.label', idDataIndex: 'trade.executingBrokerCompany.id',
			editor: {
				xtype: 'combo', displayField: 'label', queryParam: 'executingBrokerCompanyName', detailPageClass: 'Clifton.business.company.CompanyWindow', url: 'tradeExecutingBrokerCompanyListFind.json',
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					// Clear Query Store so Re-Queries each time
					combo.store.removeAll();
					combo.lastQuery = null;

					const record = combo.gridEditor.record;
					const destinationId = record.get('trade.tradeDestination.id');
					combo.store.baseParams = {
						securityIdForTradeType: record.get('security.id'),
						tradeDestinationId: destinationId && destinationId > 0 ? destinationId : void 0,
						active: true
					};
				}
			}
		},
		{header: 'Execution Type', width: 80, dataIndex: 'trade.tradeExecutionType.name', idDataIndex: 'trade.tradeExecutionType.id', useNull: true, hidden: true, editor: {displayField: 'name', xtype: 'combo', url: 'tradeExecutionTypeListFind.json'}},
		{
			header: 'Trade Destination', dataIndex: 'trade.tradeDestination.name', width: 80, idDataIndex: 'trade.tradeDestination.id',
			editor: {
				xtype: 'combo', url: 'tradeDestinationListFind.json',
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					// Clear Query Store so Re-Queries each time
					combo.store.removeAll();
					combo.lastQuery = null;

					const record = combo.gridEditor.record;
					const executingBrokerCompanyId = record.get('trade.executingBrokerCompany.id');
					combo.store.baseParams = {
						securityIdForTradeType: record.get('security.id'),
						executingBrokerCompanyId: executingBrokerCompanyId && executingBrokerCompanyId > 0 ? executingBrokerCompanyId : void 0,
						active: true
					};
				},

				// Special Copy Handler to only copy where ex broker matches
				copyMenuItemName: 'Copy To All Rows (With Same Executing Broker)',
				copyHandler: function() {
					const editor = this.gridEditor;
					// Get Executing Broker Company ID so we know which rows to copy to
					const exBrokerId = editor.record.get('trade.executingBrokerCompany.id');
					if (TCG.isBlank(exBrokerId) || exBrokerId === -1) {
						TCG.showError('Unable to copy destination to other rows because there is no executing broker selected.  Copy feature for Trade Destinations copies only where the executing broker matches this row.');
						return;
					}
					editor.containerGrid.getStore().each(record => {
						if (record.data['trade.executingBrokerCompany.id'] === exBrokerId) {
							if (record.data['trade.tradeDestination.id'] !== this.getValue()) {
								record.beginEdit();
								record.set('trade.tradeDestination.id', this.getValue());
								record.set('trade.tradeDestination.name', this.getRawValue());
								record.endEdit();
							}
						}
					});
				}
			}
		},
		{header: 'Collateral', width: 45, dataIndex: 'trade.collateralTrade', xtype: 'checkcolumn', useNull: true, tooltip: 'Defines a row as a collateral trade when checked. This flag can also be used for Holding Account selection to determine if a collateral based account should be used (e.g. "Trading: Bonds Collateral" vs "Trading: Bonds"). If the row is based on a collateral position (e.g. Cash Collateral, Position Collateral), this will be automatically checked.'}
	],
	defaultTradeDestinationAndExecutingBroker: async function(grid, record, securityId, holdingAccount) {
		const destinationMapping = await grid.getTradeDestinationMapping(grid, securityId, holdingAccount);
		if (destinationMapping) {
			const controlEditing = TCG.isFalse(record.editing);
			if (controlEditing) {
				record.beginEdit();
			}

			try {
				record.set('trade.tradeDestination.id', destinationMapping.tradeDestination.id);
				record.set('trade.tradeDestination.name', destinationMapping.tradeDestination.name);

				// If it's a manual trade - Use the Holding Account's executing broker
				let executingBroker = destinationMapping.executingBrokerCompany;
				if ('MANUAL' === destinationMapping.tradeDestination.name) {
					let holdingAccountId;
					if (holdingAccount) {
						holdingAccountId = holdingAccount.id;
						if (holdingAccount.issuingCompany && holdingAccount.issuingCompany.id) {
							executingBroker = holdingAccount.issuingCompany;
						}
					}
					else {
						holdingAccountId = record.get('holdingAccount.id');
					}
					if (TCG.isNull(executingBroker) && TCG.isNotNull(holdingAccountId)) {
						const holdingAccount = await TCG.data.getDataPromise('investmentAccount.json?id=' + holdingAccountId, grid);
						executingBroker = holdingAccount.issuingCompany;
					}
				}
				if (TCG.isNotNull(executingBroker)) {
					record.set('trade.executingBrokerCompany.id', executingBroker.id);
					record.set('trade.executingBrokerCompany.label', executingBroker.label);
				}
			}
			finally {
				if (controlEditing) {
					record.endEdit();
				}
			}
		}
	},
	getTradeDestinationMapping: async function(grid, securityId, holdingAccount) {
		let destinationMapping = void 0;
		if (securityId) {
			const mainFormPanel = grid.getWindow().getMainFormPanel();
			destinationMapping = await TCG.data.getDataPromise('tradeDestinationMappingByCommand.json', grid, {
				params: {
					securityId: securityId,
					activeOnDate: new Date(mainFormPanel.getFormValue('balanceDate')).format('m/d/Y')
				}
			});
		}
		return destinationMapping;
	},
	onAfterUpdateFieldValue: async function(editor, field) {
		const grid = this;
		const f = editor.field;
		const record = editor.record;

		if (f === 'trade.tradeDestination.name') {
			const tradeDestinationId = record.get('trade.tradeDestination.id');
			if (tradeDestinationId) {
				const params = {
					securityIdForTradeType: record.get('security.id'),
					tradeDestinationId: tradeDestinationId,
					active: true
				};
				const mappings = await TCG.data.getDataPromise('tradeDestinationMappingListByCommandFind.json', this, {params: params});
				if (mappings && mappings.length === 1) {
					const rec = mappings[0];
					if (rec && rec.executingBrokerCompany) {
						grid.stopEditing();
						const colIndex = grid.colModel.findColumnIndex('trade.executingBrokerCompany.label');
						grid.startEditing(editor.row, colIndex);
						editor.record.set('trade.executingBrokerCompany.id', rec.executingBrokerCompany.id);
						editor.record.set('trade.executingBrokerCompany.label', rec.executingBrokerCompany.label);
						const exBrokerEditor = grid.colModel.getCellEditor(colIndex, editor.row);
						exBrokerEditor.setValue({value: rec.executingBrokerCompany.id, text: rec.executingBrokerCompany.label});
						grid.stopEditing();
						// Need to mark modified so the submit values are updated (otherwise value isn't updated unless something else is updated)
						grid.markModified();
					}
				}
			}
		}
	},
	addToolbarAddButton: toolbar => false,
	addToolbarRemoveButton: toolbar => false,
	addToolbarButtons: (toolbar, grid) => {
		toolbar.add({
				text: 'Reload',
				tooltip: 'Reload collateral positions for the current client account',
				iconCls: 'table-refresh',
				scope: this,
				handler: function() {
					let hasModifiedRows = false;
					grid.getStore().each(record => {
						if (TCG.isNull(record.json) || record.get('buy') > 0 || record.get('sell') > 0) {
							hasModifiedRows = true;
						}
					}, this);
					if (hasModifiedRows) {
						Ext.Msg.confirm('Modified rows', 'There are modified rows. Would you like to reload and lose these changes?', function(a) {
							if (a === 'yes') {
								grid.reloadGrid();
							}
						});
					}
					else {
						grid.reloadGrid();
					}
				}
			}, '-', {
				text: 'Submit Trades',
				tooltip: 'Create Trades based on the Buy/Sell quantities below.',
				iconCls: 'shopping-cart',
				scope: this,
				handler: async function() {
					// get the submit parameters
					if (TCG.isFalse(grid.getWindow().getMainFormPanel().getFormValue('tradingAllowed'))) {
						TCG.showError('This run record is not available for submitting trades.', 'Trading Prohibited');
						return;
					}

					const submitValue = grid.submitField.getValue();
					if (submitValue === '[]') {
						TCG.showError('No Trade Quantities.  Please define a quantity on at least one row from the list to create trades.');
						return;
					}

					if ('yes' === await TCG.showConfirm('Are you sure you want to submit trades for all buys/sells entered?', 'Submit Trades')) {
						// remove IDs from records
						const submitRecords = JSON.parse(submitValue);
						submitRecords.forEach(record => delete record['id']);

						const params = {
							'portfolioRun.id': grid.getWindow().getMainFormId(),
							tradeDate: TCG.getChildByName(toolbar, 'tradeDate').getValue(),
							'tradeAssignee.id': TCG.getChildByName(toolbar, 'tradeAssignee.label').getValue(),
							beanList: JSON.stringify(submitRecords)
						};
						const preparedUrl = 'productOverlayCollateralMaturityTradeListGenerate.json?enableValidatingBinding=true&disableValidatingBindingValidation=true';
						// Note result is a status which is being ignored for now.  The status will only be populated with errors if trade reassignment fails.
						TCG.data.getDataPromise(preparedUrl, this, {
							waitTarget: TCG.getParentFormPanel(grid).getEl(),
							waitMsg: 'Saving Trades...',
							params: params,
							scope: grid
						}).then(result => grid.reloadGrid());
					}
				},
				listeners: {
					afterRender: function() {
						const btn = this;
						const w = grid.getWindow();
						w.on('modified', function(win, mod) {
							if (mod) {
								btn.enable();
							}
							else {
								btn.disable();
							}
						}, this);
					}
				}
			}, '-', {
				text: 'Calculate Trade(s)',
				tooltip: 'Calculate trade quantities from the exposure difference',
				iconCls: 'run',
				scope: this,
				handler: function() {
					if (TCG.isFalse(grid.getWindow().getMainFormPanel().getFormValue('tradingAllowed'))) {
						TCG.showError('This run record is not available for submitting trades.', 'Trading Prohibited');
						return;
					}

					const selection = grid.getSelectionModel().selection;
					const record = selection ? selection.record : void 0;
					if (TCG.isNull(record)) {
						TCG.showError('Please select a row to calculate the quantities. This prevents calculating quantities for too many rows causing over trading for the same collateral target.', 'No Row(s) Selected');
						return;
					}

					let quantity = Math.floor(record.get('quantityDifference'));
					if (record.get('investmentType.name') === 'Bonds') {
						// round bond quantity to 1000
						quantity = Math.floor(quantity / 1000) * 1000;
					}
					if (quantity !== 0) {
						if (quantity > 0) {
							record.set('buy', quantity);
						}
						else {
							record.set('sell', quantity);
						}
					}
					grid.markModified();
				}
			}, '-', {
				text: 'Clear Trades',
				tooltip: 'Clear Buy/Sell column values',
				iconCls: 'clear',
				scope: this,
				handler: function() {
					grid.getStore().each(record => {
						record.beginEdit();
						record.set('buy', '');
						record.set('sell', '');
						record.endEdit();
					}, this);
					grid.markModified();
				},
				listeners: {
					afterRender: function() {
						const btn = this;
						const w = grid.getWindow();
						w.on('modified', function(win, mod) {
							if (mod) {
								btn.enable();
							}
							else {
								btn.disable();
							}
						}, this);
					}
				}
			}, '-', {
				text: 'Add Security',
				tooltip: 'Add a new item',
				iconCls: 'add',
				scope: this,
				handler: async function() {
					const mainFormPanel = grid.getWindow().getMainFormPanel();
					if (TCG.isFalse(mainFormPanel.getFormValue('tradingAllowed'))) {
						TCG.showError('This run record is not available for submitting trades.', 'Trading Prohibited');
						return;
					}

					const investmentType = await TCG.data.getDataPromiseUsingCaching('investmentTypeByName.json?name=Bonds', this, 'investmentType.Bonds');
					await TCG.createComponent('Clifton.portfolio.run.trade.collateral.AddNewRunTradeCollateralSecurityWindow', {
						defaultData: {
							portfolioRun: mainFormPanel.getForm().formValues,
							investmentType: investmentType
						},
						openerCt: grid
					});
				}
			}, '-', {
				text: 'Remove Security',
				tooltip: 'Remove a row that was added, but no longer needed. Rows for existing positions cannot be removed.',
				iconCls: 'remove',
				scope: this,
				handler: function() {
					const selection = grid.getSelectionModel().selection;
					const record = selection ? selection.record : void 0;
					if (TCG.isNull(record)) {
						TCG.showError('Please select a row to be removed.', 'No Row(s) Selected');
						return;
					}

					if (TCG.isNotNull(record.json)) {
						TCG.showError('Cannot delete an existing position from this grid.', 'Row Deletion');
						return;
					}

					Ext.Msg.confirm('Delete Selected Row(s)', 'Would you like to delete the selected rows?', function(a) {
						if (a === 'yes') {
							const store = grid.getStore();
							store.remove(record);
						}
					});
				}
			}, '->',
			{xtype: 'label', html: '&nbsp;Trade Assignee:&nbsp;', qtip: 'Trader to assign created trades to following initial creation. Trades will initially be created by the current user for tracking trade history details.'},
			{width: 130, xtype: 'combo', name: 'tradeAssignee.label', hiddenName: 'tradeAssignee.id', displayField: 'label', url: 'securityUserListFind.json?disabled=false'},
			{xtype: 'label', html: '&nbsp;Trade Date:&nbsp;'},
			{width: 100, xtype: 'datefield', name: 'tradeDate'});
	},
	reloadGrid: function(livePrices) {
		const grid = this;
		const preparedUrl = grid.name + '.json';

		// get the submit parameters
		const params = {
			'portfolioRun.id': this.getWindow().getMainFormId(),
			enableValidatingBinding: true,
			disableValidatingBindingValidation: true,
			requestedMaxDepth: 4
		};

		if (TCG.isNotNull(livePrices)) {
			params['livePrices'] = livePrices;
		}

		TCG.data.getDataPromise(preparedUrl, this, {
			waitTarget: TCG.getParentFormPanel(grid).getEl(),
			waitMsg: 'Reloading...',
			params: params,
			scope: grid
		}).then(result => {
			grid.getStore().loadData({data: result});
			grid.markModified(true);
		});
	},
	isRecordApplicableForDirtyChecking: function(record) {
		return (TCG.isNotBlank(record.data.buy) && record.data.buy !== 0) || (TCG.isNotBlank(record.data.sell) && record.data.sell !== 0);
	},

	addCollateralTradeDetail: async function(params) {
		if (params) {
			const grid = this;
			const store = grid.getStore();
			const rowClass = store.recordType;
			const record = new rowClass(grid.getNewRowDefaults());

			record.beginEdit();
			try {
				record.set('investmentType.name', TCG.getValue('investmentType.name', params));
				record.set('securityPrice', params.securityPrice);
				record.set('targetExposure', params.collateralTarget);
				record.set('exposure', 0);
				record.set('exposureDifference', params.collateralTarget);
				record.set('trade.collateralTrade', TCG.isTrue(params.collateral));

				const holdingAccount = params.holdingAccount;
				if (holdingAccount) {
					record.set('holdingAccount.number', holdingAccount.number);
					record.set('holdingAccount.id', holdingAccount.id);
				}

				const clientSubAccount = params.clientInvestmentAccount;
				if (clientSubAccount) {
					record.set('clientAccount.number', clientSubAccount.number);
					record.set('clientAccount.id', clientSubAccount.id);
				}

				const security = params.security;
				const securityId = TCG.getValue('id', security);
				if (securityId) {
					record.set('security.symbol', security.symbol);
					record.set('security.id', security.id);
					record.set('security.priceMultiplier', security.priceMultiplier);
					record.set('security.endDate', security.endDate);
					await grid.defaultTradeDestinationAndExecutingBroker(grid, record, securityId, holdingAccount);
				}
			}
			finally {
				record.endEdit();
			}

			//find section for the (if it exists), and insert record at the end of that section.  If no section is found, just insert record at the 0 index.
			let index = 0;
			let nextIndex = store.find('holdingAccount.number', record.data['holdingAccount.number'], 0);
			const sectionFound = nextIndex >= 0;
			while (nextIndex !== -1) {
				index = nextIndex;
				nextIndex = store.find('holdingAccount.number', record.data['holdingAccount.number'], index + 1);
			}

			if (TCG.isTrue(sectionFound)) {
				index += 1;  // adjust index up by 1 to insert into last position of the section
			}

			grid.stopEditing();
			store.insert(index, record);

			grid.startEditing(index, grid.getColumnModel().findColumnIndex('buy'));
			grid.markModified();
		}
	}
});
Ext.reg('run-trade-collateral-grid', Clifton.portfolio.run.trade.collateral.RunTradeCollateralGrid);
