Clifton.portfolio.run.trade.collateral.AddNewRunTradeCollateralSecurityWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'New Collateral Security Trade',
	iconCls: 'replicate',
	hideApplyButton: true,
	width: 800,
	height: 300,

	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'Select a security and holding account for adding a trade to the Collateral management grid. If no holding account shows up for the selected security, that means there is no relationship mapping for trading that security. If a holding account relationship is defined for collateral only, check the checkbox for collateral.',
			items: [
				{fieldLabel: 'Portfolio Run', name: 'portfolioRun.label', xtype: 'linkfield', detailIdField: 'portfolioRun.id', detailPageClass: 'Clifton.portfolio.run.RunWindow', height: 25},
				{
					fieldLabel: 'Investment Type', name: 'investmentType.name', hiddenName: 'investmentType.id', displayField: 'name', xtype: 'combo', loadAll: true, url: 'investmentTypeList.json',
					listeners: {
						afterrender: function(combo) {
							combo.clearAndReset();
							combo.doQuery('');
						},
						select: function(combo, record, index) {
							const formPanel = TCG.getParentFormPanel(combo);
							const securityCombo = TCG.getChildByName(formPanel, 'security.symbol');
							securityCombo.clearAndReset();
						}
					}
				},
				{
					fieldLabel: 'Security', name: 'security.symbol', hiddenName: 'security.id', xtype: 'combo', allowBlank: false, displayField: 'label', requestedProps: 'symbol', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json?tradingDisallowed=false',
					beforequery: function(queryEvent) {
						const formPanel = TCG.getParentFormPanel(queryEvent.combo);
						const investmentTypeId = TCG.getChildByName(formPanel, 'investmentType.name').getValue();
						queryEvent.combo.store.baseParams = {
							investmentTypeId: investmentTypeId,
							activeOnDate: new Date(formPanel.getFormValue('portfolioRun.balanceDate')).format('m/d/Y')
						};
					},
					selectHandler: async function(combo, selection, index) {
						const formPanel = TCG.getParentFormPanel(combo);
						formPanel.setFormValue('security.priceMultiplier', selection.json.priceMultiplier);
						formPanel.setFormValue('security.endDate', selection.json.endDate);
					}
				},
				{xtype: 'hidden', name: 'security.price'},
				{xtype: 'hidden', name: 'security.priceMultiplier'},
				{xtype: 'hidden', name: 'security.endDate'},
				{
					fieldLabel: 'Sub-Account', name: 'clientSubAccount.label', hiddenName: 'clientSubAccount.id', xtype: 'combo', allowBlank: true, displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow', url: 'investmentAccountListFind.json?ourAccount=true', requiredFields: ['security.symbol'],
					qtip: 'The Sub-Account to use for the collateral trade. If not selected, the portfolio run\'s main account will be used.',
					beforequery: function(queryEvent) {
						const formPanel = TCG.getParentFormPanel(queryEvent.combo);
						queryEvent.combo.store.baseParams = {
							mainAccountId: formPanel.getFormValue('portfolioRun.clientInvestmentAccount.id'),
							mainPurpose: 'Clifton Sub-Account',
							workflowStatusNameEquals: 'Active',
							mainPurposeActiveOnDate: new Date(formPanel.getFormValue('portfolioRun.balanceDate')).format('m/d/Y')
						};
					},
					listeners: {
						select: function(combo, record, index) {
							const formPanel = TCG.getParentFormPanel(combo);
							const holdingAccountCombo = TCG.getChildByName(formPanel, 'holdingAccount.label');
							holdingAccountCombo.clearAndReset();
						}
					}
				},
				{
					fieldLabel: 'Holding Account', name: 'holdingAccount.label', hiddenName: 'holdingAccount.id', xtype: 'combo', allowBlank: false, displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow', url: 'investmentAccountListFind.json?ourAccount=false', requiredFields: ['security.symbol'],
					qtip: 'Holding account used for an existing, or new position. If no holding account is available for a new position, it may be there are none available for the selected security type. You may have to check the Collateral value to look up holding accounts for a collateral purpose.',
					beforequery: function(queryEvent) {
						const formPanel = TCG.getParentFormPanel(queryEvent.combo);
						const portfolioRunMainAccountId = formPanel.getFormValue('portfolioRun.clientInvestmentAccount.id');
						const subAccountId = formPanel.getFormValue('clientSubAccount.id');
						const investmentType = TCG.getChildByName(formPanel, 'investmentType.name').getValueObject();
						const mainAccountId = TCG.isNotBlank(subAccountId) ? subAccountId : portfolioRunMainAccountId;

						queryEvent.combo.store.baseParams = {
							mainAccountId: mainAccountId,
							workflowStatusNameEquals: 'Active',
							mainPurposeActiveOnDate: new Date(formPanel.getFormValue('portfolioRun.balanceDate')).format('m/d/Y')
						};
						if (TCG.isNotBlank(investmentType)) {
							const purposeSuffix = TCG.isTrue(formPanel.getFormValue('collateral')) ? ' Collateral' : '';
							queryEvent.combo.store.baseParams['mainPurpose'] = 'Trading: ' + investmentType.name + purposeSuffix;
						}
					}
				},
				{
					boxLabel: 'Use for Collateral', xtype: 'checkbox', name: 'collateral', qtip: 'Checking this box will filter Holding Accounts to those with a purpose of trading collateral and mark the generated trade entry as a collateral trade',
					listeners: {
						check: function(field) {
							const formPanel = TCG.getParentFormPanel(field);
							const holdingAccountField = formPanel.getForm().findField('holdingAccount.label');
							holdingAccountField.clearAndReset();
						}
					}
				}
			]
		}
	],

	saveForm: async function(closeOnSuccess, forms, form, panel) {
		if (form.isDirty()) {
			if (this.openerCt && this.openerCt.addCollateralTradeDetail) {
				const params = form.formValues;
				params.holdingAccount = form.findField('holdingAccount.label').getValueObject();
				params.security = form.findField('security.symbol').getValueObject();
				params.collateral = form.findField('collateral').checked;

				const securityId = TCG.getValue('security.id', params);
				const balanceDate = new Date(TCG.getValue('portfolioRun.balanceDate', params)).format('m/d/Y');
				if (securityId) {
					const securityPrice = await TCG.data.getDataValuePromise('marketDataPriceFlexibleForSecurity.json', form, this, {
						securityId: securityId,
						date: balanceDate,
						exceptionIfMissing: false
					});
					if (securityPrice) {
						params.securityPrice = securityPrice;
					}
				}

				// To support sub accounts, we will be populating the record with a client account, which will be the sub-account (if defined), or the portfolio run's client account.
				const clientSubAccount = form.findField('clientSubAccount.id').getValueObject();
				const portfolioRunClientAccount = TCG.getValue('portfolioRun.clientInvestmentAccount', params);
				const clientAccount = TCG.isNotBlank(clientSubAccount) ? clientSubAccount : portfolioRunClientAccount;
				params.clientInvestmentAccount = clientAccount;

				const holdingAccountId = TCG.getValue('holdingAccount.id', params);
				if (clientAccount.id && holdingAccountId) {
					const collateralBalanceList = await TCG.data.getDataPromise('collateralBalanceListFind.json', form, {
						params: {
							clientInvestmentAccountId: clientAccount.id,
							holdingInvestmentAccountId: holdingAccountId,
							balanceDate: balanceDate
						}
					});

					if (TCG.isNotBlank(collateralBalanceList) && collateralBalanceList.length > 0) {
						let collateralTarget = 0;
						const balanceListByParentNullMap = collateralBalanceList.reduce((balanceByParentMap, b) => {
							const key = TCG.isNull(b.parent) ? 'true' : 'false';
							(balanceByParentMap[key] = balanceByParentMap[key] || []).push(b);
							return balanceByParentMap;
						}, {});

						if (TCG.isNotNull(balanceListByParentNullMap['true'])) {
							balanceListByParentNullMap['true'].forEach(b => collateralTarget += b.coalesceExternalCounterpartyCollateralRequirement);
						}
						else {
							balanceListByParentNullMap['false'].forEach(b => collateralTarget += b.coalesceExternalCounterpartyCollateralRequirement);
						}

						const clientAccountProcessingType = TCG.getValue('portfolioRun.clientInvestmentAccount.serviceProcessingType', params);
						let clientAccountColumnGroup = 'Portfolio Setup';
						if (clientAccountProcessingType && clientAccountProcessingType.processingType === 'PORTFOLIO_RUNS') {
							clientAccountColumnGroup = 'PIOS Processing';
						}

						const excessPercent = await Clifton.system.GetCustomColumnValuePromise(clientAccount.id, 'InvestmentAccount', clientAccountColumnGroup, 'Collateral Excess Target Percent', true);
						if (excessPercent !== 0) {
							collateralTarget = collateralTarget * (1 + (excessPercent / 100));
						}
						params.collateralTarget = collateralTarget;
					}
				}

				await this.openerCt.addCollateralTradeDetail.call(this.openerCt, params);
				this.close();
			}
			else {
				TCG.showError('Opener component must be defined and must have addCollateralTradeDetail(params) method.', 'Callback Missing');
			}
		}
		else {
			this.close();
		}
	}
});
