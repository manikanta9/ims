Clifton.portfolio.run.RunMargin = {
	title: 'Margin Summary',
	items: [{
		xtype: 'panel',
		border: false,
		autoScroll: true,

		listeners: {
			afterrender: function(p) {
				p.reload.call(p);
			}
		},

		reload: function() {
			const p = this;
			const loader = new TCG.data.JsonLoader({
				waitTarget: p,
				waitMsg: 'Loading Margin Summary...',
				params: {
					runId: p.getWindow().getMainFormId(),
					requestedMaxDepth: 4
				},
				onLoad: function(data, conf) {
					p.refresh.call(p, data);
				}
			});
			loader.load('portfolioRunMarginListByRun.json');
		},

		refresh: function(data) {
			this.removeAll(true);
			const l = data.length;
			for (let i = 0; i < l; i++) {
				const d = data[i];
				this.addForm(d);
			}
			this.doLayout();
		},

		addForm: function(dataRow) {
			const title = dataRow.brokerInvestmentAccount.label;
			const fp = new TCG.form.FormPanel({
				title: title,
				readOnly: true,
				labelWidth: 300,
				items: Clifton.portfolio.run.MarginItems
			});
			fp.getForm().setValues(dataRow);
			this.add(fp);
		},


		getWindow: function() {
			let result = this.findParentByType(Ext.Window);
			if (TCG.isNull(result)) {
				result = this.findParentBy(function(o) {
					return o.baseCls === 'x-window';
				});
			}
			return result;
		}
	}]
};


Clifton.portfolio.run.MarginItems = [
	{
		fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
		defaults: {
			xtype: 'displayfield',
			flex: 1
		},
		items: [
			{value: 'Amount'},
			{name: 'percentageLabel'}
		]
	},
	{
		fieldLabel: 'Parametric Clifton Account Value', xtype: 'compositefield',
		defaults: {
			xtype: 'currencyfield',
			flex: 1
		},
		items: [
			{name: 'ourAccountValue'},
			{name: 'ourAccountValuePercent'}
		]
	},
	{
		fieldLabel: 'Required Initial Margin', xtype: 'compositefield',
		defaults: {
			xtype: 'currencyfield',
			flex: 1
		},
		items: [
			{name: 'marginInitialValue'},
			{name: 'marginInitialValuePercent'}
		]
	},
	{xtype: 'label', html: '<hr>'},
	{
		fieldLabel: 'Variation Margin', xtype: 'compositefield',
		defaults: {
			xtype: 'currencyfield',
			flex: 1
		},
		items: [
			{name: 'marginVariationValue'},
			{name: 'marginVariationValuePercent'}
		]
	},
	{
		fieldLabel: 'Recommended Variation Margin', xtype: 'compositefield',
		defaults: {
			xtype: 'currencyfield',
			flex: 1
		},
		items: [
			{name: 'marginRecommendedValue'},
			{name: 'marginRecommendedValuePercent'}
		]
	},
	{xtype: 'label', html: '<hr>'},
	{
		fieldLabel: 'Surplus Variation Margin', xtype: 'compositefield',
		defaults: {
			xtype: 'currencyfield',
			flex: 1
		},
		items: [
			{name: 'marginVariationValueLessRecommended'},
			{name: 'marginVariationValueLessRecommendedPercent'}
		]
	},
	{xtype: 'label', html: '&nbsp;'},

	{
		fieldLabel: 'Posted Broker Collateral', xtype: 'compositefield',
		defaults: {
			xtype: 'currencyfield',
			flex: 1
		},
		items: [
			{name: 'brokerCollateralValue'},
			{xtype: 'label', html: '&nbsp;'}
		]
	},
	{
		fieldLabel: 'Required Initial Margin', xtype: 'compositefield',
		defaults: {
			xtype: 'currencyfield',
			flex: 1
		},
		items: [
			{name: 'marginInitialValue'},
			{xtype: 'label', html: '&nbsp;'}
		]
	},
	{xtype: 'label', html: '<hr>'},
	{
		fieldLabel: 'Excess Broker Collateral', xtype: 'compositefield',
		defaults: {
			xtype: 'currencyfield',
			flex: 1
		},
		items: [
			{name: 'excessBrokerCollateralValue'},
			{xtype: 'label', html: '&nbsp;'}
		]
	}
];


