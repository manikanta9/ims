package com.clifton.portfolio.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class PortfolioRunRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private RuleViolationService ruleViolationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof PortfolioRun;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();
		PortfolioRun portfolioRun = (PortfolioRun) entity;

		// Add in Rule Violations - should be a more generic way to do this (check tables for rule categories?)
		List<RuleViolation> ruleViolations = getRuleViolationService().getRuleViolationListByLinkedEntity(PortfolioRun.TABLE_PORTFOLIO_RUN, portfolioRun.getId());
		if (!CollectionUtils.isEmpty(ruleViolations)) {
			relatedEntityList.addAll(ruleViolations);
		}

		return relatedEntityList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}
}
