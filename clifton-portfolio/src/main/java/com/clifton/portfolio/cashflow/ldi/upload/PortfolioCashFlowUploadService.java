package com.clifton.portfolio.cashflow.ldi.upload;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlow;


/**
 * A service for uploading PortfolioCashFlow data thereby populating the PortfolioCashFlow table. When a new set of cash flow data is
 * uploaded, the service creates a new PortfolioCashFlowGroup based on the client account, start date, end date, and currency symbol.
 * If the upload command's overwriteExistingGroupData flag is set to True, the service will search for a matching PortfolioCashFlowGroup
 * and overwrite its data with the uploaded data.
 *
 * @author davidi
 */
public interface PortfolioCashFlowUploadService {


	/**
	 * Uploads PortfolioCashFlow entries automatically generating the required CashFlowGroups.
	 */
	@SecureMethod(dtoClass = PortfolioCashFlow.class)
	public void uploadPortfolioCashFlowUploadFile(PortfolioCashFlowUploadCommand uploadCommand);
}
