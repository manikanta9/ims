package com.clifton.portfolio.cashflow.ldi.history;

import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.marketdata.field.MarketDataField;

import java.math.BigDecimal;


/**
 * <code>PortfolioCashFlowGroupHistorySummary</code> is a data summary of the present value cash flow information within {@link PortfolioCashFlowHistory}s allocated to
 * the data fields within {@link com.clifton.marketdata.field.MarketDataFieldGroup#GROUP_KEY_RATE_DURATION} (the full curve).
 * Summaries are always populated if cash flows are present.
 *
 * @author michaelm
 */
public class PortfolioCashFlowGroupHistorySummary extends BaseEntity<Integer> {

	private PortfolioCashFlowGroupHistory cashFlowGroupHistory;

	private InvestmentAccount investmentAccount;

	private MarketDataField marketDataField;

	private BigDecimal totalLiabilityValue;

	private BigDecimal totalDv01Value;

	private BigDecimal totalKrdValue;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowGroupHistorySummary(PortfolioCashFlowGroupHistory cashFlowGroupHistory, MarketDataField marketDataField) {
		this.cashFlowGroupHistory = cashFlowGroupHistory;
		this.investmentAccount = cashFlowGroupHistory.getCashFlowGroup().getInvestmentAccount();
		this.marketDataField = marketDataField;
	}


	public PortfolioCashFlowGroupHistorySummary() {
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		StringBuilder stringBuilder = new StringBuilder();
		if (getCashFlowGroupHistory() != null) {
			stringBuilder.append(getCashFlowGroupHistory().getLabel()).append(" - ");
		}
		if (getTotalLiabilityValue() != null) {
			stringBuilder.append("Total Liability: ").append(getTotalLiabilityValue());
		}
		if (getTotalDv01Value() != null) {
			stringBuilder.append("Total DV01: ").append(getTotalDv01Value());
		}
		if (getTotalKrdValue() != null) {
			stringBuilder.append("Total KRD: ").append(getTotalKrdValue());
		}
		return stringBuilder.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowGroupHistory getCashFlowGroupHistory() {
		return this.cashFlowGroupHistory;
	}


	public void setCashFlowGroupHistory(PortfolioCashFlowGroupHistory cashFlowGroupHistory) {
		this.cashFlowGroupHistory = cashFlowGroupHistory;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public MarketDataField getMarketDataField() {
		return this.marketDataField;
	}


	public void setMarketDataField(MarketDataField marketDataField) {
		this.marketDataField = marketDataField;
	}


	public BigDecimal getTotalLiabilityValue() {
		return this.totalLiabilityValue;
	}


	public void setTotalLiabilityValue(BigDecimal totalLiabilityValue) {
		this.totalLiabilityValue = totalLiabilityValue;
	}


	public BigDecimal getTotalDv01Value() {
		return this.totalDv01Value;
	}


	public void setTotalDv01Value(BigDecimal totalDv01Value) {
		this.totalDv01Value = totalDv01Value;
	}


	public BigDecimal getTotalKrdValue() {
		return this.totalKrdValue;
	}


	public void setTotalKrdValue(BigDecimal totalKrdValue) {
		this.totalKrdValue = totalKrdValue;
	}
}
