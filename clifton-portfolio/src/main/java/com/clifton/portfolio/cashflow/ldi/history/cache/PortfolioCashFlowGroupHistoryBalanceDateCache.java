package com.clifton.portfolio.cashflow.ldi.history.cache;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory;

import java.util.Date;


/**
 * The <code>PortfolioCashFlowGroupHistoryBalanceDateCache</code> caches key PortfolioCashFlowGroupID_BalanceDate to the history record id that is the last available history for
 * the given BalanceDate.
 *
 * @author michaelm
 */
public interface PortfolioCashFlowGroupHistoryBalanceDateCache {

	public PortfolioCashFlowGroupHistory getPortfolioCashFlowGroupHistoryLastForCashFlowGroupAndBalanceDate(ReadOnlyDAO<PortfolioCashFlowGroupHistory> dao, Integer cashFlowGroupId, Date balanceDate);
}
