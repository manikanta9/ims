package com.clifton.portfolio.cashflow.ldi.history;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portfolio.cashflow.ldi.history.search.PortfolioCashFlowGroupHistorySearchForm;
import com.clifton.portfolio.cashflow.ldi.history.search.PortfolioCashFlowGroupHistorySummarySearchForm;
import com.clifton.portfolio.cashflow.ldi.history.search.PortfolioCashFlowHistorySearchForm;

import java.util.Date;
import java.util.List;


/**
 * Interface for service methods that perform business logic related to {@link PortfolioCashFlowHistory}.
 *
 * @author michaelm
 */
public interface PortfolioCashFlowHistoryService {

	////////////////////////////////////////////////////////////////////////////
	//////         Portfolio Cash Flow Group History Methods             ///////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowGroupHistory getPortfolioCashFlowGroupHistory(int id, boolean populateCashFlowHistory);


	/**
	 * Returns the history record for the given cash flow group and balance date.  If multiple apply for a balance date, returns the one with the last create date
	 * If <code>populateCashFlows</code> is true, will also populate the cash flow history list
	 */
	public PortfolioCashFlowGroupHistory getPortfolioCashFlowGroupHistoryForBalanceDate(int cashFlowGroupId, Date balanceDate, boolean populateCashFlowHistory);


	public List<PortfolioCashFlowGroupHistory> getPortfolioCashFlowGroupHistoryList(PortfolioCashFlowGroupHistorySearchForm searchForm);


	@DoNotAddRequestMapping
	public PortfolioCashFlowGroupHistory savePortfolioCashFlowGroupHistory(PortfolioCashFlowGroupHistory portfolioCashFlowGroupHistory, Date balanceDate);


	////////////////////////////////////////////////////////////////////////////
	//////     Portfolio Cash Flow Group History Summary Methods         ///////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowGroupHistorySummary getPortfolioCashFlowGroupHistorySummary(int id);


	public List<PortfolioCashFlowGroupHistorySummary> getPortfolioCashFlowGroupHistorySummaryList(PortfolioCashFlowGroupHistorySummarySearchForm searchForm);


	public List<PortfolioCashFlowGroupHistorySummary> getPortfolioCashFlowGroupHistorySummaryByGroupHistoryList(int cashFlowGroupHistoryId);


	@DoNotAddRequestMapping
	public PortfolioCashFlowGroupHistorySummary savePortfolioCashFlowGroupHistorySummary(PortfolioCashFlowGroupHistorySummary portfolioCashFlowGroupHistorySummary);


	@DoNotAddRequestMapping
	public void savePortfolioCashFlowGroupHistorySummaryList(List<PortfolioCashFlowGroupHistorySummary> portfolioCashFlowGroupHistorySummaryList);


	////////////////////////////////////////////////////////////////////////////
	//////            Portfolio Cash FLow History Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowHistory getPortfolioCashFlowHistory(int id);


	public List<PortfolioCashFlowHistory> getPortfolioCashFlowHistoryList(PortfolioCashFlowHistorySearchForm searchForm);


	public PortfolioCashFlowHistory savePortfolioCashFlowHistory(PortfolioCashFlowHistory portfolioCashFlowHistory);


	public void deletePortfolioCashFlowHistory(int id);


	void clearPortfolioCashFlowHistoryList(int cashFlowGroupId);
}
