package com.clifton.portfolio.cashflow.ldi.history;

import com.clifton.core.beans.BaseEntity;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowType;

import java.math.BigDecimal;
import java.util.Objects;


/**
 * Holds present and future value data calculated using the {@link com.clifton.portfolio.cashflow.ldi.PortfolioCashFlow}s associated with the {@link #cashFlowGroupHistory} and
 * {@link com.clifton.investment.account.InvestmentAccount#LDI_CASH_FLOW_DISCOUNT_CURVE} data associated with the client {@link com.clifton.investment.account.InvestmentAccount}.
 * .
 *
 * @author michaelm
 */
public class PortfolioCashFlowHistory extends BaseEntity<Integer> {

	private PortfolioCashFlowGroupHistory cashFlowGroupHistory;

	private PortfolioCashFlowType cashFlowType;

	/**
	 * Determines which Portfolio Cash Flows should be used to calculate the {@link #cashFlowHistoryAmount}.
	 *
	 * @see #startingMonthNumber for details
	 */
	private int adjustedYear;
	/**
	 * Calculated as the difference in months between Balance Date and {@link com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroup#asOfDate} plus one month when
	 * Adjusted Year = 1. Determines which Portfolio Cash Flows should be used to calculate the {@link #cashFlowHistoryAmount}.
	 */
	private short startingMonthNumber;

	/**
	 * The rolled future value flow.
	 */
	private BigDecimal cashFlowHistoryAmount;

	/**
	 * This is the original market data value retrieved from the ‘Discount Curve’ security on the client account where:
	 * ‘DiscountMonth’ equals market data field ‘Order’ on the fields within the Data Field Group called ‘Discount Curve Year Rates’.
	 * Market Data ‘MeasureDate’ <= Balance Date (Date you are calculating for).
	 * If there is no Market Data value with Order matching DiscountMonth and DiscountMonth > MAX(RateFieldOrder) grab the MAX(RateFieldOrder).
	 * <p>
	 * This field is used to discount the ‘CashFlowHistoryAmount’ (rolled future value flow) into a present value liability amount via the ‘Periodicity’ defined on the
	 * ‘Discount Curve’ security.
	 */
	private BigDecimal liabilityDiscountRate;
	/**
	 * The present value of the ‘CashFlowHistoryAmount’ using the ‘LiabilityDiscountRate’. The sum of this column is the target for trading purposes.
	 */
	private BigDecimal liabilityCashFlowHistoryAmount;

	/**
	 * The Liability Discount Rate adjusted to the Periodicity defined on the client account. It is only used to derive DV01 values.
	 */
	private BigDecimal dv01DiscountRate;
	/**
	 * The present value of the ‘CashFlowHistoryAmount’ using the ‘Dv01DiscountRate’. This value is used when deriving {@link #dv01Value}.
	 */
	private BigDecimal dv01CashFlowHistoryAmount;

	/**
	 * DV01 is calculated by shocking the Discount Rate up and down by the {@link com.clifton.investment.account.InvestmentAccount#LDI_CASH_FLOW_SHOCK_AMOUNT_BPS} and measuring
	 * the sensitivity of the present value.
	 */
	private BigDecimal dv01Value;
	/**
	 * {@link #dv01Value} / SUM({@link #dv01CashFlowHistoryAmount}) * 10000
	 */
	private BigDecimal krdValue;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		if (super.equals(o)) {
			return true;
		}
		PortfolioCashFlowHistory that = (PortfolioCashFlowHistory) o;
		return getCashFlowGroupHistory().equals(that.getCashFlowGroupHistory()) &&
				getCashFlowType().equals(that.getCashFlowType()) &&
				getAdjustedYear() == that.getAdjustedYear() &&
				getStartingMonthNumber() == that.getStartingMonthNumber() &&
				getCashFlowHistoryAmount().equals(that.getCashFlowHistoryAmount());
	}


	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getCashFlowGroupHistory(), getCashFlowType(), getAdjustedYear(), getStartingMonthNumber(), getCashFlowHistoryAmount());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		StringBuilder stringBuilder = new StringBuilder();
		if (getCashFlowGroupHistory() != null) {
			stringBuilder.append(getCashFlowGroupHistory().getLabel()).append(" - ");
		}
		stringBuilder.append("Adjusted Year: ").append(adjustedYear).append(" - ");
		stringBuilder.append("Starting Month: ").append(getStartingMonthNumber()).append(" - ");
		if (getCashFlowHistoryAmount() != null) {
			stringBuilder.append("Amount: ").append(getCashFlowHistoryAmount());
		}
		return stringBuilder.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This calculated getter represents the ‘Order’ on the KRD or Year Rate Market Data fields in IMS.
	 */
	public short getDiscountMonth() {
		return (short) ((getAdjustedYear() * 12) - 6);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowGroupHistory getCashFlowGroupHistory() {
		return this.cashFlowGroupHistory;
	}


	public void setCashFlowGroupHistory(PortfolioCashFlowGroupHistory cashFlowGroupHistory) {
		this.cashFlowGroupHistory = cashFlowGroupHistory;
	}


	public PortfolioCashFlowType getCashFlowType() {
		return this.cashFlowType;
	}


	public void setCashFlowType(PortfolioCashFlowType cashFlowType) {
		this.cashFlowType = cashFlowType;
	}


	public int getAdjustedYear() {
		return this.adjustedYear;
	}


	public void setAdjustedYear(int adjustedYear) {
		this.adjustedYear = adjustedYear;
	}


	public short getStartingMonthNumber() {
		return this.startingMonthNumber;
	}


	public void setStartingMonthNumber(short startingMonthNumber) {
		this.startingMonthNumber = startingMonthNumber;
	}


	public BigDecimal getCashFlowHistoryAmount() {
		return this.cashFlowHistoryAmount;
	}


	public void setCashFlowHistoryAmount(BigDecimal cashFlowHistoryAmount) {
		this.cashFlowHistoryAmount = cashFlowHistoryAmount;
	}


	public BigDecimal getLiabilityDiscountRate() {
		return this.liabilityDiscountRate;
	}


	public void setLiabilityDiscountRate(BigDecimal liabilityDiscountRate) {
		this.liabilityDiscountRate = liabilityDiscountRate;
	}


	public BigDecimal getLiabilityCashFlowHistoryAmount() {
		return this.liabilityCashFlowHistoryAmount;
	}


	public void setLiabilityCashFlowHistoryAmount(BigDecimal liabilityCashFlowHistoryAmount) {
		this.liabilityCashFlowHistoryAmount = liabilityCashFlowHistoryAmount;
	}


	public BigDecimal getDv01DiscountRate() {
		return this.dv01DiscountRate;
	}


	public void setDv01DiscountRate(BigDecimal dv01DiscountRate) {
		this.dv01DiscountRate = dv01DiscountRate;
	}


	public BigDecimal getDv01CashFlowHistoryAmount() {
		return this.dv01CashFlowHistoryAmount;
	}


	public void setDv01CashFlowHistoryAmount(BigDecimal dv01CashFlowHistoryAmount) {
		this.dv01CashFlowHistoryAmount = dv01CashFlowHistoryAmount;
	}


	public BigDecimal getDv01Value() {
		return this.dv01Value;
	}


	public void setDv01Value(BigDecimal dv01Value) {
		this.dv01Value = dv01Value;
	}


	public BigDecimal getKrdValue() {
		return this.krdValue;
	}


	public void setKrdValue(BigDecimal krdValue) {
		this.krdValue = krdValue;
	}
}
