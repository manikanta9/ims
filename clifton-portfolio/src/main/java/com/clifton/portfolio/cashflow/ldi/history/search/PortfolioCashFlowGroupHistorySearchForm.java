package com.clifton.portfolio.cashflow.ldi.history.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntityDateRangeSearchForm;

import java.util.Date;


/**
 * Search form for {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory}s
 *
 * @author michaelm
 */
public class PortfolioCashFlowGroupHistorySearchForm extends BaseEntityDateRangeSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "cashFlowGroup.currencyInvestmentSecurity.symbol,cashFlowGroup.investmentAccount.name,cashFlowGroup.investmentAccount.number", sortField = "id")
	private String searchPattern;

	@SearchField(searchField = "cashFlowGroup.id")
	private Integer cashFlowGroupId;

	@SearchField(searchField = "cashFlowGroup.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] cashFlowGroupIds;

	@SearchField(searchField = "startDate")
	private Date startDate;

	@SearchField(searchField = "endDate")
	private Date endDate;

	@SearchField(searchField = "investmentAccount.id", searchFieldPath = "cashFlowGroup")
	private Integer investmentAccountId;

	@SearchField(searchField = "investmentAccount.name,investmentAccount.number", searchFieldPath = "cashFlowGroup", sortField = "investmentAccount.number")
	private String accountLabel;

	@SearchField(searchField = "investmentAccount.number", searchFieldPath = "cashFlowGroup")
	private String accountNumber;

	@SearchField(searchField = "currencyInvestmentSecurity.symbol", searchFieldPath = "cashFlowGroup", comparisonConditions = ComparisonConditions.EQUALS)
	private String currencySymbol;

	@SearchField(searchField = "cashFlowGroup.effectiveStartDate")
	private Date flowEffectiveStartDate;

	@SearchField(searchField = "cashFlowGroup.effectiveEndDate")
	private Date flowEffectiveEndDate;

	@SearchField(searchField = "investmentAccount.name,investmentAccount.number,currencyInvestmentSecurity.symbol,cashFlowGroupType.value", searchFieldPath = "cashFlowGroup", sortField = "investmentAccount.name")
	private String groupLabel;

	@SearchField(searchField = "discountCurveSecurity.id")
	private Integer discountCurveSecurityId;

	@SearchField
	private Date discountCurveValueDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isIncludeTime() {
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getCashFlowGroupId() {
		return this.cashFlowGroupId;
	}


	public void setCashFlowGroupId(Integer cashFlowGroupId) {
		this.cashFlowGroupId = cashFlowGroupId;
	}


	public Integer[] getCashFlowGroupIds() {
		return this.cashFlowGroupIds;
	}


	public void setCashFlowGroupIds(Integer[] cashFlowGroupIds) {
		this.cashFlowGroupIds = cashFlowGroupIds;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public String getAccountLabel() {
		return this.accountLabel;
	}


	public void setAccountLabel(String accountLabel) {
		this.accountLabel = accountLabel;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public String getCurrencySymbol() {
		return this.currencySymbol;
	}


	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}


	public Date getFlowEffectiveStartDate() {
		return this.flowEffectiveStartDate;
	}


	public void setFlowEffectiveStartDate(Date flowEffectiveStartDate) {
		this.flowEffectiveStartDate = flowEffectiveStartDate;
	}


	public Date getFlowEffectiveEndDate() {
		return this.flowEffectiveEndDate;
	}


	public void setFlowEffectiveEndDate(Date flowEffectiveEndDate) {
		this.flowEffectiveEndDate = flowEffectiveEndDate;
	}


	public String getGroupLabel() {
		return this.groupLabel;
	}


	public void setGroupLabel(String groupLabel) {
		this.groupLabel = groupLabel;
	}


	public Integer getDiscountCurveSecurityId() {
		return this.discountCurveSecurityId;
	}


	public void setDiscountCurveSecurityId(Integer discountCurveSecurityId) {
		this.discountCurveSecurityId = discountCurveSecurityId;
	}


	public Date getDiscountCurveValueDate() {
		return this.discountCurveValueDate;
	}


	public void setDiscountCurveValueDate(Date discountCurveValueDate) {
		this.discountCurveValueDate = discountCurveValueDate;
	}
}
