package com.clifton.portfolio.cashflow.ldi.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The SearchForm to be used to search for PortfolioCashFlowType entities via the PortfolioCashFlowService.
 *
 * @author davidi
 */
public class PortfolioCashFlowSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Integer excludePortfolioCashFlowId;

	@SearchField(searchField = "cashFlowType.name,cashFlowGroup.currencyInvestmentSecurity.symbol,cashFlowGroup.investmentAccount.name,cashFlowGroup.investmentAccount.number", sortField = "id")
	private String searchPattern;

	@SearchField(searchField = "cashFlowType.id")
	private Short cashFlowTypeId;

	@SearchField(searchField = "cashFlowGroup.id")
	private Integer cashFlowGroupId;

	@SearchField(searchField = "cashFlowGroup.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] cashFlowGroupIds;

	@SearchField(searchField = "investmentAccount.id", searchFieldPath = "cashFlowGroup")
	private Integer investmentAccountId;

	@SearchField(searchField = "investmentAccount.name,investmentAccount.number", searchFieldPath = "cashFlowGroup", sortField = "investmentAccount.number")
	private String accountLabel;

	@SearchField(searchField = "investmentAccount.number", searchFieldPath = "cashFlowGroup")
	private String accountNumber;

	@SearchField(searchField = "currencyInvestmentSecurity.symbol", searchFieldPath = "cashFlowGroup", comparisonConditions = ComparisonConditions.EQUALS)
	private String currencySymbol;

	@SearchField
	private Short cashFlowYear;

	@SearchField
	private Short startingMonthNumber;

	@SearchField(searchField = "cashFlowAmount")
	private BigDecimal cashFlowAmount;

	@SearchField(searchField = "cashFlowType.name")
	private String flowTypeName;

	@SearchField(searchField = "cashFlowType.cumulative")
	private Boolean cumulative;

	@SearchField(searchField = "cashFlowType.projectedFlow")
	private Boolean projectedFlow;

	@SearchField(searchField = "cashFlowType.actualFlow")
	private Boolean actualFlow;

	@SearchField(searchField = "cashFlowType.serviceCost")
	private Boolean serviceCost;

	@SearchField(searchField = "cashFlowGroup.effectiveStartDate")
	private Date flowEffectiveStartDate;

	@SearchField(searchField = "cashFlowGroup.effectiveEndDate")
	private Date flowEffectiveEndDate;

	@SearchField(searchField = "investmentAccount.name,investmentAccount.number,currencyInvestmentSecurity.symbol,cashFlowGroupType.value", searchFieldPath = "cashFlowGroup", sortField = "investmentAccount.name")
	private String groupLabel;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getExcludePortfolioCashFlowId() {
		return this.excludePortfolioCashFlowId;
	}


	public void setExcludePortfolioCashFlowId(Integer excludePortfolioCashFlowId) {
		this.excludePortfolioCashFlowId = excludePortfolioCashFlowId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getCashFlowTypeId() {
		return this.cashFlowTypeId;
	}


	public void setCashFlowTypeId(Short cashFlowTypeId) {
		this.cashFlowTypeId = cashFlowTypeId;
	}


	public Integer getCashFlowGroupId() {
		return this.cashFlowGroupId;
	}


	public void setCashFlowGroupId(Integer cashFlowGroupId) {
		this.cashFlowGroupId = cashFlowGroupId;
	}


	public Integer[] getCashFlowGroupIds() {
		return this.cashFlowGroupIds;
	}


	public void setCashFlowGroupIds(Integer[] cashFlowGroupIds) {
		this.cashFlowGroupIds = cashFlowGroupIds;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public String getAccountLabel() {
		return this.accountLabel;
	}


	public void setAccountLabel(String accountLabel) {
		this.accountLabel = accountLabel;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public String getCurrencySymbol() {
		return this.currencySymbol;
	}


	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}


	public Short getCashFlowYear() {
		return this.cashFlowYear;
	}


	public void setCashFlowYear(Short cashFlowYear) {
		this.cashFlowYear = cashFlowYear;
	}


	public Short getStartingMonthNumber() {
		return this.startingMonthNumber;
	}


	public void setStartingMonthNumber(Short startingMonthNumber) {
		this.startingMonthNumber = startingMonthNumber;
	}


	public BigDecimal getCashFlowAmount() {
		return this.cashFlowAmount;
	}


	public void setCashFlowAmount(BigDecimal cashFlowAmount) {
		this.cashFlowAmount = cashFlowAmount;
	}


	public String getFlowTypeName() {
		return this.flowTypeName;
	}


	public void setFlowTypeName(String flowTypeName) {
		this.flowTypeName = flowTypeName;
	}


	public Boolean getCumulative() {
		return this.cumulative;
	}


	public void setCumulative(Boolean cumulative) {
		this.cumulative = cumulative;
	}


	public Boolean getProjectedFlow() {
		return this.projectedFlow;
	}


	public void setProjectedFlow(Boolean projectedFlow) {
		this.projectedFlow = projectedFlow;
	}


	public Boolean getActualFlow() {
		return this.actualFlow;
	}


	public void setActualFlow(Boolean actualFlow) {
		this.actualFlow = actualFlow;
	}


	public Boolean getServiceCost() {
		return this.serviceCost;
	}


	public void setServiceCost(Boolean serviceCost) {
		this.serviceCost = serviceCost;
	}


	public Date getFlowEffectiveStartDate() {
		return this.flowEffectiveStartDate;
	}


	public void setFlowEffectiveStartDate(Date flowEffectiveStartDate) {
		this.flowEffectiveStartDate = flowEffectiveStartDate;
	}


	public Date getFlowEffectiveEndDate() {
		return this.flowEffectiveEndDate;
	}


	public void setFlowEffectiveEndDate(Date flowEffectiveEndDate) {
		this.flowEffectiveEndDate = flowEffectiveEndDate;
	}


	public String getGroupLabel() {
		return this.groupLabel;
	}


	public void setGroupLabel(String groupLabel) {
		this.groupLabel = groupLabel;
	}
}
