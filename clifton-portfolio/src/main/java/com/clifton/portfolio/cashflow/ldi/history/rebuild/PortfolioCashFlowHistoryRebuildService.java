package com.clifton.portfolio.cashflow.ldi.history.rebuild;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.util.status.Status;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroup;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistorySummary;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Service that rebuilds Portfolio Cash Flow History.
 *
 * @author michaelm
 */
public interface PortfolioCashFlowHistoryRebuildService {


	public Status rebuildPortfolioCashFlowHistory(PortfolioCashFlowHistoryRebuildCommand command);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the {@link PortfolioCashFlowGroupHistory} result if it exists, otherwise attempts to build it.
	 */
	@DoNotAddRequestMapping
	public PortfolioCashFlowGroupHistory getOrBuildPortfolioCashFlowHistory(int cashFlowGroupId, Date balanceDate);


	/**
	 * Gets the {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistorySummary}s if they exist, otherwise attempts to build them.
	 */
	@DoNotAddRequestMapping
	public List<PortfolioCashFlowGroupHistorySummary> getOrBuildPortfolioCashFlowGroupHistorySummaryList(int cashFlowGroupHistoryId);


	/**
	 * Gets a map of {@link PortfolioCashFlowGroup} to list of {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistorySummary}s for the client account
	 * if they exist, otherwise attempts to build them.
	 */
	@DoNotAddRequestMapping
	public Map<PortfolioCashFlowGroup, List<PortfolioCashFlowGroupHistorySummary>> getOrBuildPortfolioCashFlowGroupHistorySummaryListMapForClientAccount(int clientInvestmentAccountId, Date balanceDate);
}
