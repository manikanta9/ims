package com.clifton.portfolio.cashflow.ldi.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowType;
import org.springframework.stereotype.Component;


/**
 * <code>PortfolioCashFlowTypeValidator</code> performs validation on new or existing PortfolioCashFlowType entities being saved.
 *
 * @author davidi
 */
@Component
public class PortfolioCashFlowTypeValidator extends SelfRegisteringDaoValidator<PortfolioCashFlowType> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(PortfolioCashFlowType cashFlowType, DaoEventTypes config) {
		// validate mutual exclusivity of selected boolean cash flow type properties
		ValidationUtils.assertOneValueMatching("One and only one type identifier must be selected.", true, cashFlowType.isProjectedFlow(), cashFlowType.isActualFlow(), cashFlowType.isServiceCost());
	}
}
