package com.clifton.portfolio.cashflow.ldi;


import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowGroupSearchForm;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowSearchForm;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowTypeSearchForm;

import java.util.List;


/**
 * The <code>PortfolioCashFlowService</code> used to retrieve, save, and delete PortfolioCashFlow,
 * PortfolioCashFlowType, and PortfolioCashFlowGroup entities.
 *
 * @author davidi
 */
public interface PortfolioCashFlowService {

	////////////////////////////////////////////////////////////////////////////
	//////        Portfolio Cash Flow Type Business Methods              ///////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowType getPortfolioCashFlowType(short id);


	public PortfolioCashFlowType getPortfolioCashFlowTypeByName(String name);


	public List<PortfolioCashFlowType> getPortfolioCashFlowTypeList(PortfolioCashFlowTypeSearchForm searchForm);


	public PortfolioCashFlowType savePortfolioCashFlowType(PortfolioCashFlowType portfolioCashFlowType);


	public void deletePortfolioCashFlowType(short id);


	////////////////////////////////////////////////////////////////////////////
	//////       Portfolio Cash Flow Group Business Methods              ///////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowGroup getPortfolioCashFlowGroup(int id);


	public List<PortfolioCashFlowGroup> getPortfolioCashFlowGroupList(PortfolioCashFlowGroupSearchForm searchForm);


	public PortfolioCashFlowGroup savePortfolioCashFlowGroup(PortfolioCashFlowGroup portfolioCashFlowGroup);


	public void deletePortfolioCashFlowGroup(int id);


	////////////////////////////////////////////////////////////////////////////
	/////////        Portfolio Cash Flow Business Methods              /////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlow getPortfolioCashFlow(int id);


	public List<PortfolioCashFlow> getPortfolioCashFlowList(PortfolioCashFlowSearchForm searchForm);


	public PortfolioCashFlow savePortfolioCashFlow(PortfolioCashFlow portfolioCashFlow);


	public void deletePortfolioCashFlow(int id);


	/**
	 * This method looks up and deletes all items in a PortfolioCashFlowGroup.  This is primarily used
	 * by the PortfolioCashFlowUploadService to clear a group of all entries prior to the
	 * an upload to implement the overwrite option.
	 */
	public void deletePortfolioCashFlowsInCashFlowGroup(PortfolioCashFlowGroup portfolioCashFlowGroup);
}
