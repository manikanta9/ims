package com.clifton.portfolio.cashflow.ldi.history.calculator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlow;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowType;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistory;
import com.clifton.portfolio.cashflow.ldi.history.rebuild.PortfolioCashFlowHistoryRebuildConfig;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;


/**
 * {@link BasePortfolioCashFlowHistoryCalculator} that copies the {@link PortfolioCashFlow#cashFlowAmount} to the {@link PortfolioCashFlowHistory#cashFlowHistoryAmount} and then
 * populates DV01 and KRD values.
 *
 * @author michaelm
 */
@Component("portfolioCashFlowHistoryCalculator")
public class PortfolioCashFlowHistoryCopyCalculator extends BasePortfolioCashFlowHistoryCalculator {

	@Override
	public PortfolioCashFlowGroupHistory calculatePortfolioCashFlowHistory(PortfolioCashFlowHistoryRebuildConfig rebuildConfig, PortfolioCashFlowGroupHistory cashFlowGroupHistory, List<PortfolioCashFlow> cashFlowList) {
		Date balanceDate = rebuildConfig.getBalanceDate();
		int adjustedYear = 1;
		short startingMonthNumber = (short) (DateUtils.getMonthsDifference(balanceDate, cashFlowGroupHistory.getCashFlowGroup().getAsOfDate()) + 1);
		PortfolioCashFlowType cashFlowType = CollectionUtils.getFirstElementStrict(cashFlowList).getCashFlowType();
		PortfolioCashFlowHistoryPresentValueCalculationData presentValueCalculationData = rebuildConfig.getRebuildCommand().getPresentValueData();
		List<PortfolioCashFlowHistory> cashFlowHistoryList = new ArrayList<>();

		Iterator<PortfolioCashFlow> cashFlowIterator = cashFlowList.iterator();
		while (adjustedYear <= 100) {
			if (CollectionUtils.isEmpty(cashFlowList)) {
				break;
			}
			PortfolioCashFlowHistory cashFlowHistory = new PortfolioCashFlowHistory();
			cashFlowHistory.setCashFlowGroupHistory(cashFlowGroupHistory);
			cashFlowHistory.setAdjustedYear(adjustedYear);
			cashFlowHistory.setStartingMonthNumber(startingMonthNumber);
			cashFlowHistory.setCashFlowType(cashFlowType);
			cashFlowHistory.setCashFlowHistoryAmount(BigDecimal.ZERO);
			boolean found = false;
			while (!found && cashFlowIterator.hasNext()) {
				PortfolioCashFlow cashFlow = cashFlowIterator.next();
				if (cashFlow.getStartingMonthNumber() <= startingMonthNumber && startingMonthNumber <= cashFlow.getEndingMonthNumber()) {
					cashFlowHistory.setCashFlowHistoryAmount(Optional.of(cashFlow.getCashFlowAmount()).orElse(BigDecimal.ZERO));
					found = true;
				}
			}
			cashFlowHistory = calculatePresentValues(cashFlowHistory, presentValueCalculationData, balanceDate);
			cashFlowHistoryList.add(cashFlowHistory);
			adjustedYear++;
			startingMonthNumber += 12;
			cashFlowGroupHistory.setTotalDv01Amount(MathUtils.add(cashFlowGroupHistory.getTotalDv01Amount(), cashFlowHistory.getDv01CashFlowHistoryAmount()));
		}
		cashFlowGroupHistory.getCashFlowHistoryList().addAll(cashFlowHistoryList);
		cashFlowGroupHistory.setDiscountCurveSecurity(presentValueCalculationData.getClientDiscountCurveSecurity());
		cashFlowGroupHistory.setDiscountCurveValueDate(presentValueCalculationData.getCurveRateMeasureDate());
		return cashFlowGroupHistory;
	}
}
