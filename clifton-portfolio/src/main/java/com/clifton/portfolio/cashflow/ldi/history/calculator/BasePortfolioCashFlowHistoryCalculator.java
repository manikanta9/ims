package com.clifton.portfolio.cashflow.ldi.history.calculator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.marketdata.field.MarketDataFieldGroup;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlow;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistory;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;


/**
 * Abstract class that contains common logic for implementations of {@link PortfolioCashFlowHistoryCalculator}.
 *
 * @author michaelm
 */
public abstract class BasePortfolioCashFlowHistoryCalculator implements PortfolioCashFlowHistoryCalculator {

	private MarketDataFieldService marketDataFieldService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Calculates the 'Monthly Amount' of the cash flow based on the flags defined on the {@link com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowType} (e.g. Projected Flow).
	 * This value needs to be calculated during {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistory} processing because
	 * balance date is required to generate the number of <code>reportMonths</code>.
	 */
	public BigDecimal calculateAdjustedCashFlowAmount(PortfolioCashFlow cashFlow, short reportMonths) {
		if (cashFlow.getCashFlowType().isProjectedFlow()) {
			return MathUtils.divide(cashFlow.getCashFlowAmount(), 12);
		}
		else {
			final BigDecimal divisor = BigDecimal.valueOf(144);
			return MathUtils.multiply(cashFlow.getCashFlowAmount(), MathUtils.divide(BigDecimal.valueOf(reportMonths, 0), divisor));
		}
	}


	/**
	 * Populate each {@link PortfolioCashFlowHistory#krdValue} using the {@link PortfolioCashFlowGroupHistory#totalDv01Amount}.
	 * <p>
	 * KRDValue = (DV01Value / totalDV01CashFlowHistoryAmount) * 10000
	 */
	public PortfolioCashFlowGroupHistory calculateKrdValues(PortfolioCashFlowGroupHistory cashFlowGroupHistory) {
		for (PortfolioCashFlowHistory cashFlowHistory : CollectionUtils.getIterable(cashFlowGroupHistory.getCashFlowHistoryList())) {
			BigDecimal krdValue = MathUtils.multiply(MathUtils.divide(cashFlowHistory.getDv01Value(), cashFlowGroupHistory.getTotalDv01Amount()), BigDecimal.valueOf(10000));
			cashFlowHistory.setKrdValue(Optional.ofNullable(krdValue).orElse(BigDecimal.ZERO));
		}
		return cashFlowGroupHistory;
	}


	/**
	 * Calculates the present values for this {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistory} object (e.g. LiabilityDiscountRate, DV01DiscountRate, etc.).
	 */
	public PortfolioCashFlowHistory calculatePresentValues(PortfolioCashFlowHistory cashFlowHistory, PortfolioCashFlowHistoryPresentValueCalculationData presentValueData, Date balanceDate) {
		cashFlowHistory.setLiabilityDiscountRate(findLiabilityDiscountRate(cashFlowHistory, presentValueData, balanceDate));
		cashFlowHistory.setLiabilityCashFlowHistoryAmount(calculateLiabilityCashFlowHistoryAmount(cashFlowHistory, presentValueData));
		cashFlowHistory.setDv01DiscountRate(calculateDv01DiscountRate(cashFlowHistory, presentValueData));
		cashFlowHistory.setDv01CashFlowHistoryAmount(calculateDv01CashFlowHistoryAmount(cashFlowHistory, presentValueData));
		cashFlowHistory.setDv01Value(calculateDv01Value(cashFlowHistory, presentValueData));
		// KRD values are calculated after ALL cash flow history is populated for the group history because totalDV01CashFlowHistoryAmount for the group is needed.
		return cashFlowHistory;
	}


	/**
	 * @see PortfolioCashFlowHistory#liabilityDiscountRate
	 */
	private BigDecimal findLiabilityDiscountRate(PortfolioCashFlowHistory cashFlowHistory, PortfolioCashFlowHistoryPresentValueCalculationData presentValueData, Date balanceDate) {
		MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm();
		searchForm.setInvestmentSecurityId(presentValueData.getClientDiscountCurveSecurity().getId());
		searchForm.setMaxMeasureDate(DateUtils.addDays(balanceDate, 1)); // add a day because search field is not inclusive
		Date minMeasureDate = DateUtils.addDays(balanceDate, -presentValueData.getClientDiscountCurveDataDaysToExpiration() - 1); // subtract a day because search field is not inclusive
		searchForm.setMinMeasureDate(minMeasureDate);
		searchForm.setDataFieldOrder(cashFlowHistory.getDiscountMonth());
		searchForm.setDataFieldGroupName(MarketDataFieldGroup.GROUP_DISCOUNT_CURVE_YEAR_RATES);
		MarketDataValue liabilityDiscountRateValue = CollectionUtils.getFirstElement(getMarketDataFieldService().getMarketDataValueList(searchForm));
		if (liabilityDiscountRateValue == null) {
			searchForm = new MarketDataValueSearchForm();
			searchForm.setInvestmentSecurityId(presentValueData.getClientDiscountCurveSecurity().getId());
			searchForm.setMaxMeasureDate(DateUtils.addDays(balanceDate, 1)); // add a day because search field is not inclusive
			searchForm.setMinMeasureDate(minMeasureDate);
			searchForm.setDataFieldGroupName(MarketDataFieldGroup.GROUP_DISCOUNT_CURVE_YEAR_RATES);
			searchForm.setOrderBy("dataFieldOrder:desc");
			searchForm.setLimit(1);
			liabilityDiscountRateValue = CollectionUtils.getFirstElement(getMarketDataFieldService().getMarketDataValueList(searchForm));
			ValidationUtils.assertTrue(liabilityDiscountRateValue != null && cashFlowHistory.getDiscountMonth() > liabilityDiscountRateValue.getDataField().getFieldOrder(),
					String.format("Unable to find Liability Discount Rate for Client Account [%s] using search criteria Security [%s], Data Field Order [%d], Data Field Group Name [%s] and Measure Date within previous %d days of %s.",
							cashFlowHistory.getCashFlowGroupHistory().getCashFlowGroup().getInvestmentAccount().getLabelShort(), presentValueData.getClientDiscountCurveSecurity().getLabel(), cashFlowHistory.getDiscountMonth(), MarketDataFieldGroup.GROUP_DISCOUNT_CURVE_YEAR_RATES, presentValueData.getClientDiscountCurveDataDaysToExpiration(), DateUtils.fromDateShort(balanceDate)));
		}

		ValidationUtils.assertTrue(presentValueData.getCurveRateMeasureDate() == null || DateUtils.isEqual(liabilityDiscountRateValue.getMeasureDate(), presentValueData.getCurveRateMeasureDate()), String.format("Invalid %s data for Security [%s] on Client Account [%s]. Cannot have data from multiple Measure Dates [%s, %s].",
				MarketDataFieldGroup.GROUP_DISCOUNT_CURVE_YEAR_RATES, presentValueData.getClientDiscountCurveSecurity().getLabel(), cashFlowHistory.getCashFlowGroupHistory().getCashFlowGroup().getInvestmentAccount().getLabelShort(), DateUtils.fromDateShort(presentValueData.getCurveRateMeasureDate()), DateUtils.fromDateShort(liabilityDiscountRateValue.getMeasureDate())));
		presentValueData.setCurveRateMeasureDate(liabilityDiscountRateValue.getMeasureDate());
		return liabilityDiscountRateValue.getMeasureValue().movePointLeft(2);
	}


	/**
	 * LiabilityCashFlowHistoryAmount = CashFlowHistoryAmount / ((1+(LiabilityDiscountRate/discountCurvePeriodicityValue))^(DiscountMonth / 12 X discountCurvePeriodicityValue))
	 */
	private BigDecimal calculateLiabilityCashFlowHistoryAmount(PortfolioCashFlowHistory cashFlowHistory, PortfolioCashFlowHistoryPresentValueCalculationData presentValueData) {
		int discountCurvePeriodicityValue = presentValueData.getDiscountCurveSecurityDv01Periodicity();
		BigDecimal onePlusRatePeriodicity = MathUtils.add(BigDecimal.ONE, MathUtils.divide(cashFlowHistory.getLiabilityDiscountRate(), discountCurvePeriodicityValue));
		return MathUtils.divide(cashFlowHistory.getCashFlowHistoryAmount(), MathUtils.pow(onePlusRatePeriodicity, MathUtils.multiply(MathUtils.divide(BigDecimal.valueOf(cashFlowHistory.getDiscountMonth()), (BigDecimal.valueOf(12))), BigDecimal.valueOf(discountCurvePeriodicityValue))));
	}


	/**
	 * If clientAccountPeriodicityValue = discountCurvePeriodicityValue, then DV01DiscountRate = LiabilityDiscountRate
	 * If clientAccountPeriodicityValue = 1 & discountCurvePeriodicityValue = 2, then DV01DiscountRate = (1 + LiabilityDiscountRate/2)^2-1
	 * If clientAccountPeriodicityValue = 2 & discountCurvePeriodicityValue = 1, then DV01DiscountRate = ((1 + LiabilityDiscountRate)^(1/2)-1)*2
	 */
	private BigDecimal calculateDv01DiscountRate(PortfolioCashFlowHistory cashFlowHistory, PortfolioCashFlowHistoryPresentValueCalculationData presentValueData) {
		int clientAccountPeriodicityValue = presentValueData.getClientDv01Periodicity();
		int discountCurvePeriodicityValue = presentValueData.getDiscountCurveSecurityDv01Periodicity();
		if (clientAccountPeriodicityValue == discountCurvePeriodicityValue) {
			return cashFlowHistory.getLiabilityDiscountRate();
		}
		if (clientAccountPeriodicityValue == 1 && discountCurvePeriodicityValue == 2) {
			return MathUtils.subtract(MathUtils.pow(MathUtils.add(BigDecimal.ONE, MathUtils.divide(cashFlowHistory.getLiabilityDiscountRate(), BigDecimal.valueOf(2))), BigDecimal.valueOf(2)), BigDecimal.ONE);
		}
		if (clientAccountPeriodicityValue == 2 && discountCurvePeriodicityValue == 1) {
			return MathUtils.multiply(MathUtils.subtract(MathUtils.pow(MathUtils.add(BigDecimal.ONE, cashFlowHistory.getLiabilityDiscountRate()), BigDecimal.valueOf(0.5D)), BigDecimal.ONE), 2);
		}
		throw new ValidationException(String.format("Unsupported Periodicity Value for Cash Flow History [%s], Discount Curve Periodicity Value [%d], and Client Account Periodicity Value [%d]",
				cashFlowHistory.getLabel(), discountCurvePeriodicityValue, clientAccountPeriodicityValue));
	}


	/**
	 * Dv01CashFlowHistoryAmount = CashFlowHistoryAmount / ((1+(DV01DiscountRate/clientAccountPeriodicityValue))^(DiscountMonth / 12 X clientAccountPeriodicityValue))
	 */
	private BigDecimal calculateDv01CashFlowHistoryAmount(PortfolioCashFlowHistory cashFlowHistory, PortfolioCashFlowHistoryPresentValueCalculationData presentValueData) {
		int clientAccountPeriodicityValue = presentValueData.getClientDv01Periodicity();
		BigDecimal onePlusRatePeriodicity = MathUtils.add(BigDecimal.ONE, MathUtils.divide(cashFlowHistory.getDv01DiscountRate(), clientAccountPeriodicityValue));
		return MathUtils.divide(cashFlowHistory.getCashFlowHistoryAmount(), MathUtils.pow(onePlusRatePeriodicity, MathUtils.multiply(MathUtils.divide(BigDecimal.valueOf(cashFlowHistory.getDiscountMonth()), BigDecimal.valueOf(12)), clientAccountPeriodicityValue)));
	}


	/**
	 * DV01Value = (((PVMinusBump – DV01CashFlowHistoryAmount) + (DV01CashFlowHistoryAmount – PVPlusBump))/2) / 10000 / Bump
	 */
	private BigDecimal calculateDv01Value(PortfolioCashFlowHistory cashFlowHistory, PortfolioCashFlowHistoryPresentValueCalculationData presentValueData) {
		BigDecimal pvMinusBump = calculatePvMinusBump(cashFlowHistory, presentValueData);
		BigDecimal pvPlusBump = calculatePvPlusBump(cashFlowHistory, presentValueData);
		BigDecimal minusBumpMinusDv01HistoryAmount = MathUtils.subtract(pvMinusBump, cashFlowHistory.getDv01CashFlowHistoryAmount());
		BigDecimal dv01HistoryAmountMinusPlusBump = MathUtils.subtract(cashFlowHistory.getDv01CashFlowHistoryAmount(), pvPlusBump);
		return MathUtils.divide(MathUtils.divide(MathUtils.add(minusBumpMinusDv01HistoryAmount, dv01HistoryAmountMinusPlusBump), 2), presentValueData.getClientCurveShockAmountBps());
	}


	/**
	 * pvMinusBump = (CashFlowHistoryAmount / ((1+((DV01DiscountRate-Bump)/clientAccountPeriodicityValue))^(DiscountMonth / 12 X clientAccountPeriodicityValue))
	 */
	private BigDecimal calculatePvMinusBump(PortfolioCashFlowHistory cashFlowHistory, PortfolioCashFlowHistoryPresentValueCalculationData presentValueData) {
		BigDecimal rateAdjustedByShockAmount = MathUtils.subtract(cashFlowHistory.getDv01DiscountRate(), presentValueData.getClientCurveShockAmountConvertedFromBps());
		return calculatePvBump(cashFlowHistory, presentValueData, rateAdjustedByShockAmount);
	}


	/**
	 * pvPlusBump = (CashFlowHistoryAmount / ((1+((DV01DiscountRate+Bump)/clientAccountPeriodicityValue))^(DiscountMonth / 12 X clientAccountPeriodicityValue))
	 */
	private BigDecimal calculatePvPlusBump(PortfolioCashFlowHistory cashFlowHistory, PortfolioCashFlowHistoryPresentValueCalculationData presentValueData) {
		BigDecimal rateAdjustedByShockAmount = MathUtils.add(cashFlowHistory.getDv01DiscountRate(), presentValueData.getClientCurveShockAmountConvertedFromBps());
		return calculatePvBump(cashFlowHistory, presentValueData, rateAdjustedByShockAmount);
	}


	private BigDecimal calculatePvBump(PortfolioCashFlowHistory cashFlowHistory, PortfolioCashFlowHistoryPresentValueCalculationData presentValueData, BigDecimal rateAdjustedByShockAmount) {
		int clientAccountPeriodicityValue = presentValueData.getClientDv01Periodicity();
		BigDecimal adjustedDiscountMonth = MathUtils.multiply(MathUtils.divide(BigDecimal.valueOf(cashFlowHistory.getDiscountMonth()), BigDecimal.valueOf(12)), clientAccountPeriodicityValue);
		BigDecimal onePlusRatePeriodicity = MathUtils.add(BigDecimal.ONE, MathUtils.divide(rateAdjustedByShockAmount, clientAccountPeriodicityValue));
		return MathUtils.divide(cashFlowHistory.getCashFlowHistoryAmount(), MathUtils.pow(onePlusRatePeriodicity, adjustedDiscountMonth));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}
}
