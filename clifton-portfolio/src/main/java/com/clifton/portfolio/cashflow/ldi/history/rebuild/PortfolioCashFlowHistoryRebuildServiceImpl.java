package com.clifton.portfolio.cashflow.ldi.history.rebuild;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldFieldGroup;
import com.clifton.marketdata.field.MarketDataFieldGroup;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlow;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroup;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowService;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowType;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistorySummary;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistory;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistoryService;
import com.clifton.portfolio.cashflow.ldi.history.calculator.BasePortfolioCashFlowHistoryCalculator;
import com.clifton.portfolio.cashflow.ldi.history.calculator.PortfolioCashFlowHistoryCalculator;
import com.clifton.portfolio.cashflow.ldi.history.calculator.PortfolioCashFlowHistoryPresentValueCalculationData;
import com.clifton.portfolio.cashflow.ldi.history.search.PortfolioCashFlowGroupHistorySearchForm;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowGroupSearchForm;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowSearchForm;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.stream.Collectors;

import static com.clifton.core.util.date.DateUtils.fromDate;
import static com.clifton.core.util.status.Status.ofMessage;


/**
 * @author michaelm
 */
@Service
public class PortfolioCashFlowHistoryRebuildServiceImpl implements PortfolioCashFlowHistoryRebuildService {

	private PortfolioCashFlowService portfolioCashFlowService;

	private PortfolioCashFlowHistoryService portfolioCashFlowHistoryService;

	private PortfolioCashFlowHistoryCalculator portfolioCashFlowHistoryCalculator;

	private RunnerHandler runnerHandler;

	private SystemBeanService systemBeanService;

	private InvestmentAccountService investmentAccountService;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;

	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	private InvestmentInstrumentService investmentInstrumentService;

	private MarketDataFieldService marketDataFieldService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status rebuildPortfolioCashFlowHistory(final PortfolioCashFlowHistoryRebuildCommand command) {
		// Command Validation
		ValidationUtils.assertNotNull(command.getStartBalanceDate(), "Start Balance Date is required.");
		ValidationUtils.assertNotNull(command.getEndBalanceDate(), "End Balance Date is required.");
		ValidationUtils.assertTrue(DateUtils.isDateBeforeOrEqual(command.getStartBalanceDate(), command.getEndBalanceDate(), false), "Start Date must be on or before end date.");
		ValidationUtils.assertTrue(DateUtils.isDateBeforeOrEqual(command.getEndBalanceDate(), new Date(), false), "Balance Date cannot be in the future.");

		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : ofMessage("Synchronously running for " + command);
			doRebuildPortfolioCashFlowGroupHistory(command, status);
			return status;
		}

		// asynchronous run support
		final String runId = command.getRunId();
		final Date now = (command.getRunSecondsDelay() == null ? new Date() : DateUtils.addSeconds(new Date(), command.getRunSecondsDelay()));

		final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + fromDate(now));
		Runner runner = new AbstractStatusAwareRunner("CASH-FLOW-HISTORY", runId, now, statusHolder) {

			@Override
			public void run() {
				try {
					doRebuildPortfolioCashFlowGroupHistory(command, statusHolder.getStatus());
				}
				catch (Throwable e) {
					getStatus().setMessage("Error rebuilding Cash Flow History for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error rebuilding Cash Flow History for " + runId, e);
				}
			}
		};
		getRunnerHandler().rescheduleRunner(runner);

		return Status.ofMessage("Started Cash Flow History rebuild: " + runId + ". Processing will be completed shortly.");
	}


	private PortfolioCashFlowHistoryPresentValueCalculationData getPresentValueDataForRebuildCommand(PortfolioCashFlowHistoryRebuildCommand command) {
		InvestmentAccount investmentAccount;
		if (command.getInvestmentAccountId() == null) {
			PortfolioCashFlowGroup cashFlowGroup = getPortfolioCashFlowService().getPortfolioCashFlowGroup(command.getCashFlowGroupId());
			ValidationUtils.assertNotNull(cashFlowGroup, "Client Account or Cash Flow Group is required to rebuild Cash Flow History.");
			command.setInvestmentAccountId(cashFlowGroup.getInvestmentAccount().getId());
			investmentAccount = cashFlowGroup.getInvestmentAccount();
		}
		else {
			investmentAccount = getInvestmentAccountService().getInvestmentAccount(command.getInvestmentAccountId());
		}
		String errorMessageFormat = "Unable to find %s for Client Account [%s].";

		Integer clientDiscountCurveSecurityId = (Integer) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(investmentAccount, InvestmentAccount.LDI_CASH_FLOW_DISCOUNT_CURVE);
		ValidationUtils.assertNotNull(clientDiscountCurveSecurityId, String.format(errorMessageFormat, InvestmentAccount.LDI_CASH_FLOW_DISCOUNT_CURVE, investmentAccount.getLabel()));

		Integer clientDiscountCurveDataDaysToExpiration = (Integer) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(investmentAccount, InvestmentAccount.LDI_CASH_FLOW_DISCOUNT_CURVE_DAYS_TILL_OUTDATED_DATA);
		ValidationUtils.assertNotNull(clientDiscountCurveDataDaysToExpiration, String.format(errorMessageFormat, InvestmentAccount.LDI_CASH_FLOW_DISCOUNT_CURVE_DAYS_TILL_OUTDATED_DATA, investmentAccount.getLabel()));

		InvestmentSecurity clientDiscountCurveSecurity = getInvestmentInstrumentService().getInvestmentSecurity(clientDiscountCurveSecurityId);
		ValidationUtils.assertNotNull(clientDiscountCurveSecurity, String.format(errorMessageFormat, InvestmentAccount.LDI_CASH_FLOW_DISCOUNT_CURVE, investmentAccount.getLabel()));

		Integer clientDv01Periodicity = (Integer) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(investmentAccount, InvestmentAccount.LDI_CASH_FLOW_DV01_PERIODICITY);
		ValidationUtils.assertNotNull(clientDv01Periodicity, String.format(errorMessageFormat, InvestmentAccount.LDI_CASH_FLOW_DV01_PERIODICITY, investmentAccount.getLabel()));

		Integer clientCurveShockAmount = (Integer) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(investmentAccount, InvestmentAccount.LDI_CASH_FLOW_SHOCK_AMOUNT_BPS); // aka Bump
		ValidationUtils.assertNotNull(clientCurveShockAmount, String.format(errorMessageFormat, InvestmentAccount.LDI_CASH_FLOW_SHOCK_AMOUNT_BPS, investmentAccount.getLabel()));

		Integer discountCurveSecurityDv01Periodicity = (Integer) getInvestmentSecurityUtilHandler().getCustomColumnValue(clientDiscountCurveSecurity, InvestmentSecurity.PERIODICITY);
		ValidationUtils.assertNotNull(discountCurveSecurityDv01Periodicity, String.format("Unable to find %s value for %s [%s] associated with Client Account [%s].", InvestmentSecurity.PERIODICITY, InvestmentAccount.LDI_CASH_FLOW_DISCOUNT_CURVE, clientDiscountCurveSecurity.getLabel(), investmentAccount.getLabel()));
		return new PortfolioCashFlowHistoryPresentValueCalculationData(clientDiscountCurveSecurity, clientDiscountCurveDataDaysToExpiration, discountCurveSecurityDv01Periodicity, clientDv01Periodicity, clientCurveShockAmount);
	}


	protected void doRebuildPortfolioCashFlowGroupHistory(PortfolioCashFlowHistoryRebuildCommand command, Status status) {
		List<PortfolioCashFlowGroup> cashFlowGroupList = getPortfolioCashFlowGroupListForCommand(command);
		if (CollectionUtils.isEmpty(cashFlowGroupList)) {
			return; // no cash flow groups to process
		}
		command.setPresentValueData(getPresentValueDataForRebuildCommand(command));

		PortfolioCashFlowHistoryRebuildConfig rebuildConfig = new PortfolioCashFlowHistoryRebuildConfig(command);

		Date date = command.getStartBalanceDate();
		int processCount = 0;
		int skipCount = 0;
		while (DateUtils.isDateBeforeOrEqual(date, command.getEndBalanceDate(), false)) {
			rebuildConfig.resetForBalanceDate(date);
			status.setMessage("Processing " + DateUtils.fromDateShort(date));
			for (PortfolioCashFlowGroup cashFlowGroup : CollectionUtils.getIterable(cashFlowGroupList)) {
				if (!command.isRebuildExisting()) {
					PortfolioCashFlowGroupHistory cashFlowGroupHistory = getPortfolioCashFlowHistoryService().getPortfolioCashFlowGroupHistoryForBalanceDate(cashFlowGroup.getId(), date, false);
					if (cashFlowGroupHistory != null) {
						status.addSkipped(cashFlowGroup.getLabel() + " - " + DateUtils.fromDateShort(date));
						skipCount++;
						continue;
					}
				}
				try {
					rebuildPortfolioCashFlowGroupHistoryImpl(cashFlowGroup, rebuildConfig);
					status.setActionPerformed(true);
					processCount++;
				}
				catch (Throwable e) {
					String message = ExceptionUtils.getOriginalMessage(e);
					status.addError(cashFlowGroup.getLabel() + " - " + DateUtils.fromDateShort(date) + ": " + message);
					LogUtils.errorOrInfo(LogCommand.ofThrowable(getClass(), e));
				}
			}
			date = DateUtils.getNextWeekday(date);
		}
		status.setMessage("Processing Complete. " + CollectionUtils.getSize(cashFlowGroupList) + " Total Cash Flow Groups.  " + DateUtils.fromDateShort(command.getStartBalanceDate()) + " - " + DateUtils.fromDateShort(command.getEndBalanceDate()) + ": Processed: " + processCount + ", Skipped: " + skipCount + ", Failed: " + status.getErrorCount());
	}


	protected PortfolioCashFlowGroupHistory rebuildPortfolioCashFlowGroupHistoryImpl(PortfolioCashFlowGroup cashFlowGroup, PortfolioCashFlowHistoryRebuildConfig rebuildConfig) {
		Map<PortfolioCashFlowType, List<PortfolioCashFlow>> cashFlowTypeToCashFlowListMap = getCashFlowTypeToCashFlowListMap(cashFlowGroup);
		PortfolioCashFlowGroupHistory cashFlowGroupHistory = getPortfolioCashFlowHistoryService().savePortfolioCashFlowGroupHistory(calculateCashFlowGroupHistory(rebuildConfig, cashFlowGroup, cashFlowTypeToCashFlowListMap), rebuildConfig.getBalanceDate());
		getPortfolioCashFlowHistoryService().savePortfolioCashFlowGroupHistorySummaryList(getOrBuildPortfolioCashFlowGroupHistorySummaryListImpl(cashFlowGroupHistory));
		return cashFlowGroupHistory;
	}


	private List<PortfolioCashFlowGroup> getPortfolioCashFlowGroupListForCommand(PortfolioCashFlowHistoryRebuildCommand command) {
		PortfolioCashFlowGroupSearchForm searchForm = new PortfolioCashFlowGroupSearchForm();
		searchForm.setActiveOnStartDate(command.getStartBalanceDate());
		searchForm.setActiveOnEndDate(command.getEndBalanceDate());
		if (command.getInvestmentAccountId() != null) {
			searchForm.setInvestmentAccountId(command.getInvestmentAccountId());
			return getPortfolioCashFlowService().getPortfolioCashFlowGroupList(searchForm);
		}
		else if (command.getCashFlowGroupId() != null) {
			return CollectionUtils.createList(getPortfolioCashFlowService().getPortfolioCashFlowGroup(command.getCashFlowGroupId()));
		}
		return getPortfolioCashFlowService().getPortfolioCashFlowGroupList(searchForm);
	}


	private Map<PortfolioCashFlowType, List<PortfolioCashFlow>> getCashFlowTypeToCashFlowListMap(PortfolioCashFlowGroup cashFlowGroup) {
		PortfolioCashFlowSearchForm searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setCashFlowGroupId(cashFlowGroup.getId());
		searchForm.setOrderBy("startingMonthNumber:ASC");
		List<PortfolioCashFlow> cashFlowList = getPortfolioCashFlowService().getPortfolioCashFlowList(searchForm);
		ValidationUtils.assertNotEmpty(cashFlowList, String.format("No Cash Flows exist for Cash Flow Group [%s]", cashFlowGroup.getLabel()));
		Map<PortfolioCashFlowType, List<PortfolioCashFlow>> cashFlowTypeToCashFlowListMap = new HashMap<>();
		for (PortfolioCashFlow cashFlow : cashFlowList) {
			cashFlowTypeToCashFlowListMap.computeIfAbsent(cashFlow.getCashFlowType(), flow -> new ArrayList<>()).add(cashFlow);
		}
		return cashFlowTypeToCashFlowListMap;
	}


	private PortfolioCashFlowGroupHistory calculateCashFlowGroupHistory(PortfolioCashFlowHistoryRebuildConfig rebuildConfig, PortfolioCashFlowGroup cashFlowGroup, Map<PortfolioCashFlowType, List<PortfolioCashFlow>> cashFlowTypeToCashFlowListMap) {
		PortfolioCashFlowGroupHistory cashFlowGroupHistory = new PortfolioCashFlowGroupHistory();
		cashFlowGroupHistory.setCashFlowGroup(cashFlowGroup);
		cashFlowGroupHistory.setCashFlowHistoryList(new ArrayList<>());
		if (cashFlowGroup.getCashFlowGroupType().getValue().equals(PortfolioCashFlowGroup.CASH_FLOW_GROUP_TYPE_LIST_ITEM_NON_ROLLING)) {
			for (Map.Entry<PortfolioCashFlowType, List<PortfolioCashFlow>> cashFlowTypeListEntry : cashFlowTypeToCashFlowListMap.entrySet()) {
				cashFlowGroupHistory = getPortfolioCashFlowHistoryCalculator().calculatePortfolioCashFlowHistory(rebuildConfig, cashFlowGroupHistory, cashFlowTypeListEntry.getValue());
			}
		}
		else {
			for (Map.Entry<PortfolioCashFlowType, List<PortfolioCashFlow>> cashFlowTypeListEntry : cashFlowTypeToCashFlowListMap.entrySet()) {
				if (cashFlowTypeListEntry.getKey().getCashFlowHistoryCalculatorBean() == null) {
					continue;
				}
				PortfolioCashFlowHistoryCalculator cashFlowHistoryCalculator = rebuildConfig.getCashFlowHistoryCalculator(cashFlowTypeListEntry.getKey().getCashFlowHistoryCalculatorBean(), getSystemBeanService());
				cashFlowGroupHistory = cashFlowHistoryCalculator.calculatePortfolioCashFlowHistory(rebuildConfig, cashFlowGroupHistory, cashFlowTypeListEntry.getValue());
			}
		}
		return ((BasePortfolioCashFlowHistoryCalculator) getPortfolioCashFlowHistoryCalculator()).calculateKrdValues(cashFlowGroupHistory);
	}


	@Override
	public PortfolioCashFlowGroupHistory getOrBuildPortfolioCashFlowHistory(int cashFlowGroupId, Date balanceDate) {
		PortfolioCashFlowGroupHistory cashFlowGroupHistory = getPortfolioCashFlowHistoryService().getPortfolioCashFlowGroupHistoryForBalanceDate(cashFlowGroupId, balanceDate, true);
		if (cashFlowGroupHistory == null) {
			PortfolioCashFlowHistoryRebuildCommand rebuildCommand = new PortfolioCashFlowHistoryRebuildCommand();
			rebuildCommand.setCashFlowGroupId(cashFlowGroupId);
			rebuildCommand.setStartBalanceDate(balanceDate);
			rebuildCommand.setEndBalanceDate(balanceDate);
			rebuildCommand.setSynchronous(true);
			rebuildPortfolioCashFlowHistory(rebuildCommand);
			cashFlowGroupHistory = getPortfolioCashFlowHistoryService().getPortfolioCashFlowGroupHistoryForBalanceDate(cashFlowGroupId, balanceDate, true);
		}
		return cashFlowGroupHistory;
	}


	private List<PortfolioCashFlowGroupHistory> getOrBuildPortfolioCashFlowHistoryListForAccount(int clientInvestmentAccountId, Date balanceDate) {
		List<PortfolioCashFlowGroupHistory> cashFlowGroupHistoryList = getPortfolioCashFlowHistoryListForAccount(clientInvestmentAccountId, balanceDate);
		if (CollectionUtils.isEmpty(cashFlowGroupHistoryList)) {
			PortfolioCashFlowHistoryRebuildCommand rebuildCommand = new PortfolioCashFlowHistoryRebuildCommand();
			rebuildCommand.setInvestmentAccountId(clientInvestmentAccountId);
			rebuildCommand.setStartBalanceDate(balanceDate);
			rebuildCommand.setEndBalanceDate(balanceDate);
			rebuildCommand.setSynchronous(true);
			rebuildPortfolioCashFlowHistory(rebuildCommand);
			cashFlowGroupHistoryList = getPortfolioCashFlowHistoryListForAccount(clientInvestmentAccountId, balanceDate);
		}
		return cashFlowGroupHistoryList;
	}


	private List<PortfolioCashFlowGroupHistory> getPortfolioCashFlowHistoryListForAccount(int clientInvestmentAccountId, Date balanceDate) {
		PortfolioCashFlowGroupHistorySearchForm searchForm = new PortfolioCashFlowGroupHistorySearchForm();
		searchForm.setActiveOnDate(balanceDate);
		searchForm.setInvestmentAccountId(clientInvestmentAccountId);
		searchForm.setOrderBy("createDate:desc");
		return getPortfolioCashFlowHistoryService().getPortfolioCashFlowGroupHistoryList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////
	////////          Cash Flow Group History Summary Methods           ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortfolioCashFlowGroupHistorySummary> getOrBuildPortfolioCashFlowGroupHistorySummaryList(int cashFlowGroupHistoryId) {
		return getOrBuildPortfolioCashFlowGroupHistorySummaryListImpl(getPortfolioCashFlowHistoryService().getPortfolioCashFlowGroupHistory(cashFlowGroupHistoryId, true));
	}


	@Override
	public Map<PortfolioCashFlowGroup, List<PortfolioCashFlowGroupHistorySummary>> getOrBuildPortfolioCashFlowGroupHistorySummaryListMapForClientAccount(int clientInvestmentAccountId, Date balanceDate) {
		Map<PortfolioCashFlowGroup, List<PortfolioCashFlowGroupHistorySummary>> cashFlowGroupSummaryListMap = new HashMap<>();
		for (PortfolioCashFlowGroupHistory groupHistory : CollectionUtils.getIterable(getOrBuildPortfolioCashFlowHistoryListForAccount(clientInvestmentAccountId, balanceDate))) {
			// only add the most recent group history for each active group on the client account, don't use putIfAbsent because null/empty value is valid
			if (!cashFlowGroupSummaryListMap.containsKey(groupHistory.getCashFlowGroup())) {
				cashFlowGroupSummaryListMap.put(groupHistory.getCashFlowGroup(), getOrBuildPortfolioCashFlowGroupHistorySummaryList(groupHistory.getId()));
			}
		}
		return cashFlowGroupSummaryListMap;
	}


	private List<PortfolioCashFlowGroupHistorySummary> getOrBuildPortfolioCashFlowGroupHistorySummaryListImpl(PortfolioCashFlowGroupHistory cashFlowGroupHistory) {
		List<PortfolioCashFlowGroupHistorySummary> cashFlowGroupHistorySummaryList = getPortfolioCashFlowHistoryService().getPortfolioCashFlowGroupHistorySummaryByGroupHistoryList(cashFlowGroupHistory.getId());
		if (CollectionUtils.isEmpty(cashFlowGroupHistorySummaryList)) {
			cashFlowGroupHistorySummaryList = createGroupHistorySummaryList(cashFlowGroupHistory);
		}
		return cashFlowGroupHistorySummaryList;
	}


	/**
	 * Summarizes the present value data from the {@link PortfolioCashFlowHistory}s by allocating them to the fields from {@link MarketDataFieldGroup#GROUP_KEY_RATE_DURATION}.
	 */
	private List<PortfolioCashFlowGroupHistorySummary> createGroupHistorySummaryList(PortfolioCashFlowGroupHistory cashFlowGroupHistory) {
		Map<MarketDataField, PortfolioCashFlowGroupHistorySummary> groupHistorySummaryMap = new HashMap<>();
		Map<PortfolioCashFlowHistory, Map<MarketDataField, BigDecimal>> cashFlowHistoryAllocationMap = new HashMap<>(); // used for validating history allocation percentages

		List<MarketDataField> krdFieldList = BeanUtils.sortWithFunction(CollectionUtils.getConverted(getMarketDataFieldService().getMarketDataFieldFieldGroupListByGroup(MarketDataFieldGroup.GROUP_KEY_RATE_DURATION), MarketDataFieldFieldGroup::getReferenceOne), MarketDataField::getFieldOrder, true);
		ListIterator<MarketDataField> krdFieldListIterator = krdFieldList.listIterator();
		while (krdFieldListIterator.hasNext()) {
			if (!krdFieldListIterator.hasPrevious()) {
				MarketDataField krdField = krdFieldListIterator.next();
				// processing field with smallest order
				MarketDataField secondKrdField = krdFieldList.get(krdFieldListIterator.nextIndex());
				for (PortfolioCashFlowHistory cashFlowHistory : CollectionUtils.getIterable(cashFlowGroupHistory.getCashFlowHistoryList())) {
					BigDecimal allocationAmount = MathUtils.max(BigDecimal.ZERO, cashFlowHistory.getDiscountMonth() <= krdField.getFieldOrder() ? BigDecimal.ONE
							: MathUtils.divide(BigDecimal.valueOf(secondKrdField.getFieldOrder() - cashFlowHistory.getDiscountMonth()), BigDecimal.valueOf(secondKrdField.getFieldOrder() - krdField.getFieldOrder())));
					updateSummaryCalculationMaps(groupHistorySummaryMap, cashFlowHistoryAllocationMap, krdField, cashFlowHistory, allocationAmount);
				}
			}
			else {
				MarketDataField krdField = krdFieldListIterator.next();
				if (!krdFieldListIterator.hasNext()) {
					// processing field with largest order
					MarketDataField penultimateKrdField = krdFieldList.get(krdFieldListIterator.previousIndex() - 1);
					for (PortfolioCashFlowHistory cashFlowHistory : CollectionUtils.getIterable(cashFlowGroupHistory.getCashFlowHistoryList())) {
						BigDecimal allocationAmount = MathUtils.max(BigDecimal.ZERO, cashFlowHistory.getDiscountMonth() >= krdField.getFieldOrder() ? BigDecimal.ONE
								: MathUtils.divide(BigDecimal.valueOf(cashFlowHistory.getDiscountMonth() - penultimateKrdField.getFieldOrder()), BigDecimal.valueOf(krdField.getFieldOrder() - penultimateKrdField.getFieldOrder())));
						updateSummaryCalculationMaps(groupHistorySummaryMap, cashFlowHistoryAllocationMap, krdField, cashFlowHistory, allocationAmount);
					}
				}
				else {
					// not processing field with smallest or largest order
					for (PortfolioCashFlowHistory cashFlowHistory : CollectionUtils.getIterable(cashFlowGroupHistory.getCashFlowHistoryList())) {
						BigDecimal allocationAmount = BigDecimal.ZERO;
						if (cashFlowHistory.getDiscountMonth() == krdField.getFieldOrder()) {
							allocationAmount = BigDecimal.ONE;
						}
						else if (cashFlowHistory.getDiscountMonth() > krdField.getFieldOrder()) {
							MarketDataField nextKrdField = krdFieldList.get(krdFieldListIterator.nextIndex());
							allocationAmount = MathUtils.max(allocationAmount, MathUtils.divide(BigDecimal.valueOf(nextKrdField.getFieldOrder() - cashFlowHistory.getDiscountMonth()), BigDecimal.valueOf(nextKrdField.getFieldOrder() - krdField.getFieldOrder())));
						}
						else {
							MarketDataField previousKrdField = krdFieldList.get(krdFieldListIterator.previousIndex() - 1);
							allocationAmount = MathUtils.max(allocationAmount, MathUtils.divide(BigDecimal.valueOf(cashFlowHistory.getDiscountMonth() - previousKrdField.getFieldOrder()), BigDecimal.valueOf(krdField.getFieldOrder() - previousKrdField.getFieldOrder())));
						}
						updateSummaryCalculationMaps(groupHistorySummaryMap, cashFlowHistoryAllocationMap, krdField, cashFlowHistory, allocationAmount);
					}
				}
			}
		}
		List<PortfolioCashFlowGroupHistorySummary> cashFlowGroupHistorySummaryList = new ArrayList<>(groupHistorySummaryMap.values());
		getPortfolioCashFlowHistoryService().savePortfolioCashFlowGroupHistorySummaryList(cashFlowGroupHistorySummaryList);
		return cashFlowGroupHistorySummaryList;
	}


	private void updateSummaryCalculationMaps(Map<MarketDataField, PortfolioCashFlowGroupHistorySummary> groupHistorySummaryMap, Map<PortfolioCashFlowHistory, Map<MarketDataField, BigDecimal>> cashFlowHistoryAllocationMap, MarketDataField krdField, PortfolioCashFlowHistory cashFlowHistory, BigDecimal allocationAmount) {
		Map<MarketDataField, BigDecimal> allocationMapValue = cashFlowHistoryAllocationMap.computeIfAbsent(cashFlowHistory, history -> new HashMap<>());
		allocationMapValue.put(krdField, allocationAmount);
		ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(CoreMathUtils.sum(allocationMapValue.values()), BigDecimal.ONE), String.format("Cash Flow History [%s] is over allocated to the following Market Data Fields [%s].",
				cashFlowHistory.getLabel(), StringUtils.join(allocationMapValue.entrySet().stream().filter(mapEntry -> MathUtils.isNotEqual(mapEntry.getValue(), BigDecimal.ZERO)).collect(Collectors.toList()), mapEntry -> mapEntry.getKey().getName() + " " + mapEntry.getValue().movePointRight(2) + "%", ", ")));
		PortfolioCashFlowGroupHistorySummary groupHistorySummary = groupHistorySummaryMap.computeIfAbsent(krdField, field -> new PortfolioCashFlowGroupHistorySummary(cashFlowHistory.getCashFlowGroupHistory(), field));
		groupHistorySummary.setTotalDv01Value(MathUtils.add(groupHistorySummary.getTotalDv01Value(), MathUtils.multiply(cashFlowHistory.getDv01Value(), allocationAmount)));
		groupHistorySummary.setTotalKrdValue(MathUtils.add(groupHistorySummary.getTotalKrdValue(), MathUtils.multiply(cashFlowHistory.getKrdValue(), allocationAmount)));
		groupHistorySummary.setTotalLiabilityValue(MathUtils.add(groupHistorySummary.getTotalLiabilityValue(), MathUtils.multiply(cashFlowHistory.getLiabilityCashFlowHistoryAmount(), allocationAmount)));
		groupHistorySummaryMap.put(krdField, groupHistorySummary);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowService getPortfolioCashFlowService() {
		return this.portfolioCashFlowService;
	}


	public void setPortfolioCashFlowService(PortfolioCashFlowService portfolioCashFlowService) {
		this.portfolioCashFlowService = portfolioCashFlowService;
	}


	public PortfolioCashFlowHistoryService getPortfolioCashFlowHistoryService() {
		return this.portfolioCashFlowHistoryService;
	}


	public void setPortfolioCashFlowHistoryService(PortfolioCashFlowHistoryService portfolioCashFlowHistoryService) {
		this.portfolioCashFlowHistoryService = portfolioCashFlowHistoryService;
	}


	public PortfolioCashFlowHistoryCalculator getPortfolioCashFlowHistoryCalculator() {
		return this.portfolioCashFlowHistoryCalculator;
	}


	public void setPortfolioCashFlowHistoryCalculator(PortfolioCashFlowHistoryCalculator portfolioCashFlowHistoryCalculator) {
		this.portfolioCashFlowHistoryCalculator = portfolioCashFlowHistoryCalculator;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}
}
