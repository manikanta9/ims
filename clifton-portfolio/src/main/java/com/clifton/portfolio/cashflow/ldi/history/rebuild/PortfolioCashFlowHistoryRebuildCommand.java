package com.clifton.portfolio.cashflow.ldi.history.rebuild;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.portfolio.cashflow.ldi.history.calculator.PortfolioCashFlowHistoryPresentValueCalculationData;

import java.util.Date;


/**
 * Command used to configure a rebuild of Portfolio Cash Flow History.
 *
 * @author michaelm
 */
public class PortfolioCashFlowHistoryRebuildCommand {

	private Integer investmentAccountId;

	/**
	 * Data object that holds fields necessary for rebuild. These are current configured on InvestmentAccount so they should only be looked up once per rebuild command.
	 */
	private PortfolioCashFlowHistoryPresentValueCalculationData presentValueData;

	private Integer cashFlowGroupId;

	private Date startBalanceDate;

	private Date endBalanceDate;

	private boolean synchronous;

	/**
	 * If asynchronous can delay the rebuild for x seconds (i.e. saving cash flows allows all changes to save before running rebuild)
	 */
	private Integer runSecondsDelay;

	private Status status;

	/**
	 * If true, will rebuild existing, if different will save results
	 * If false, will only rebuild if missing on balance date otherwise will skip
	 */
	private boolean rebuildExisting;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getRunId() {
		String dateString = DateUtils.fromDateShort(getStartBalanceDate()) + "_" + DateUtils.fromDateShort(getEndBalanceDate());
		if (getCashFlowGroupId() != null) {
			return getCashFlowGroupId() + "_" + dateString;
		}
		return dateString;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public PortfolioCashFlowHistoryPresentValueCalculationData getPresentValueData() {
		return this.presentValueData;
	}


	public void setPresentValueData(PortfolioCashFlowHistoryPresentValueCalculationData presentValueData) {
		this.presentValueData = presentValueData;
	}


	public Integer getCashFlowGroupId() {
		return this.cashFlowGroupId;
	}


	public void setCashFlowGroupId(Integer cashFlowGroupId) {
		this.cashFlowGroupId = cashFlowGroupId;
	}


	public Date getStartBalanceDate() {
		return this.startBalanceDate;
	}


	public void setStartBalanceDate(Date startBalanceDate) {
		this.startBalanceDate = startBalanceDate;
	}


	public Date getEndBalanceDate() {
		return this.endBalanceDate;
	}


	public void setEndBalanceDate(Date endBalanceDate) {
		this.endBalanceDate = endBalanceDate;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public Integer getRunSecondsDelay() {
		return this.runSecondsDelay;
	}


	public void setRunSecondsDelay(Integer runSecondsDelay) {
		this.runSecondsDelay = runSecondsDelay;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public boolean isRebuildExisting() {
		return this.rebuildExisting;
	}


	public void setRebuildExisting(boolean rebuildExisting) {
		this.rebuildExisting = rebuildExisting;
	}
}
