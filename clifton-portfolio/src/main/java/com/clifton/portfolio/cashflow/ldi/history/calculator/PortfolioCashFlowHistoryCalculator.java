
package com.clifton.portfolio.cashflow.ldi.history.calculator;

import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlow;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory;
import com.clifton.portfolio.cashflow.ldi.history.rebuild.PortfolioCashFlowHistoryRebuildConfig;

import java.util.List;


/**
 * Interface for calculating present and future values for {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistory} records.
 *
 * @author michaelm
 */
public interface PortfolioCashFlowHistoryCalculator {

	/**
	 * Calculates the {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistory}.
	 */
	public PortfolioCashFlowGroupHistory calculatePortfolioCashFlowHistory(PortfolioCashFlowHistoryRebuildConfig rebuildConfig, PortfolioCashFlowGroupHistory cashFlowGroupHistory, List<PortfolioCashFlow> cashFlowList);
}
