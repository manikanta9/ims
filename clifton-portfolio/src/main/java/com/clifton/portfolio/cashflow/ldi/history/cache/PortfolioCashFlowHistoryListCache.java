package com.clifton.portfolio.cashflow.ldi.history.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistory;
import org.springframework.stereotype.Component;


/**
 * An entity cache for retrieving lists of {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistory} entities by
 * {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory} ID.
 *
 * @author michaelm
 */
@Component
public class PortfolioCashFlowHistoryListCache extends SelfRegisteringSingleKeyDaoListCache<PortfolioCashFlowHistory, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "cashFlowGroupHistory.id";
	}


	@Override
	protected Integer getBeanKeyValue(PortfolioCashFlowHistory bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getCashFlowGroupHistory());
		}
		return null;
	}
}
