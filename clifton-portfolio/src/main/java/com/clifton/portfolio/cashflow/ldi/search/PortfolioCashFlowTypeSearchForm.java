package com.clifton.portfolio.cashflow.ldi.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The SearchForm to be used to search for PortfolioCashFlowType entities via the PortfolioCashFlowService.
 *
 * @author davidi
 */
public class PortfolioCashFlowTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "name,description", sortField = "name")
	private String searchPattern;

	@SearchField(searchField = "name")
	private String flowTypeName;

	@SearchField
	private Boolean cumulative;

	@SearchField
	private Boolean projectedFlow;

	@SearchField
	private Boolean actualFlow;

	@SearchField
	private Boolean serviceCost;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getFlowTypeName() {
		return this.flowTypeName;
	}


	public void setFlowTypeName(String flowTypeName) {
		this.flowTypeName = flowTypeName;
	}


	public Boolean getCumulative() {
		return this.cumulative;
	}


	public void setCumulative(Boolean cumulative) {
		this.cumulative = cumulative;
	}


	public Boolean getProjectedFlow() {
		return this.projectedFlow;
	}


	public void setProjectedFlow(Boolean projectedFlow) {
		this.projectedFlow = projectedFlow;
	}


	public Boolean getActualFlow() {
		return this.actualFlow;
	}


	public void setActualFlow(Boolean actualFlow) {
		this.actualFlow = actualFlow;
	}


	public Boolean getServiceCost() {
		return this.serviceCost;
	}


	public void setServiceCost(Boolean serviceCost) {
		this.serviceCost = serviceCost;
	}
}
