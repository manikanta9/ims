package com.clifton.portfolio.cashflow.ldi;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portfolio.cashflow.ldi.history.rebuild.PortfolioCashFlowHistoryRebuildCommand;
import com.clifton.portfolio.cashflow.ldi.history.rebuild.PortfolioCashFlowHistoryRebuildService;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * On changes to {@link PortfolioCashFlow} will schedule the Cash Flow History to be rebuilt for the previous weekday (if {@link PortfolioCashFlowGroup} is active)
 *
 * @author michaelm
 */
@Component
public class PortfolioCashFlowObserver extends SelfRegisteringDaoObserver<PortfolioCashFlow> {

	private PortfolioCashFlowHistoryRebuildService portfolioCashFlowHistoryRebuildService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<PortfolioCashFlow> dao, DaoEventTypes event, PortfolioCashFlow bean, Throwable e) {
		if (e == null && DateUtils.isCurrentDateBetween(bean.getCashFlowGroup().getEffectiveStartDate(), bean.getCashFlowGroup().getEffectiveEndDate(), false)) {
			PortfolioCashFlowHistoryRebuildCommand rebuildCommand = new PortfolioCashFlowHistoryRebuildCommand();
			rebuildCommand.setCashFlowGroupId(bean.getCashFlowGroup().getId());
			rebuildCommand.setStartBalanceDate(DateUtils.getPreviousWeekday(DateUtils.clearTime(new Date())));
			rebuildCommand.setEndBalanceDate(rebuildCommand.getStartBalanceDate());
			rebuildCommand.setRebuildExisting(true);
			rebuildCommand.setRunSecondsDelay(1); // 1 second delay so all cash flow changes are saved
			getPortfolioCashFlowHistoryRebuildService().rebuildPortfolioCashFlowHistory(rebuildCommand);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowHistoryRebuildService getPortfolioCashFlowHistoryRebuildService() {
		return this.portfolioCashFlowHistoryRebuildService;
	}


	public void setPortfolioCashFlowHistoryRebuildService(PortfolioCashFlowHistoryRebuildService portfolioCashFlowHistoryRebuildService) {
		this.portfolioCashFlowHistoryRebuildService = portfolioCashFlowHistoryRebuildService;
	}
}
