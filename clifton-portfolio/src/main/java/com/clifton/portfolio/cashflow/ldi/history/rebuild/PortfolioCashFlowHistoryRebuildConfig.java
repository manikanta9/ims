package com.clifton.portfolio.cashflow.ldi.history.rebuild;

import com.clifton.portfolio.cashflow.ldi.history.calculator.PortfolioCashFlowHistoryCalculator;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>PortfolioCashFlowHistoryRebuildConfig</code> class holds common properties used during {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory}
 * rebuilds, which makes processing faster when running across multiple {@link com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroup}s.
 *
 * @author michaelm
 */
public class PortfolioCashFlowHistoryRebuildConfig {

	private final PortfolioCashFlowHistoryRebuildCommand rebuildCommand;

	private final Map<Integer, PortfolioCashFlowHistoryCalculator> cashFlowHistoryCalculatorBeanMap;

	/**
	 * Date specific properties that are cleared for each date processed
	 */
	private Date balanceDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowHistoryRebuildConfig(PortfolioCashFlowHistoryRebuildCommand rebuildCommand) {
		this.rebuildCommand = rebuildCommand;
		this.cashFlowHistoryCalculatorBeanMap = new HashMap<>();
	}


	public void resetForBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowHistoryCalculator getCashFlowHistoryCalculator(SystemBean cashFlowHistoryCalculatorBean, SystemBeanService systemBeanService) {
		return this.cashFlowHistoryCalculatorBeanMap.computeIfAbsent(cashFlowHistoryCalculatorBean.getId(), key -> (PortfolioCashFlowHistoryCalculator) systemBeanService.getBeanInstance(cashFlowHistoryCalculatorBean));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowHistoryRebuildCommand getRebuildCommand() {
		return this.rebuildCommand;
	}


	public Map<Integer, PortfolioCashFlowHistoryCalculator> getCashFlowHistoryCalculatorBeanMap() {
		return this.cashFlowHistoryCalculatorBeanMap;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}
}
