package com.clifton.portfolio.cashflow.ldi.upload;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlow;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroup;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowService;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowType;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowGroupSearchForm;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListService;
import com.clifton.system.list.search.SystemListItemSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.system.upload.SystemUploadHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioCashFlowUploadServiceImpl</code> ...
 *
 * @author davidi
 */

@Service
public class PortfolioCashFlowUploadServiceImpl implements PortfolioCashFlowUploadService {

	PortfolioCashFlowService portfolioCashFlowService;

	SystemUploadHandler systemUploadHandler;

	InvestmentAccountService investmentAccountService;

	InvestmentInstrumentService investmentInstrumentService;

	SystemListService<SystemList> systemListService;

	SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadPortfolioCashFlowUploadFile(PortfolioCashFlowUploadCommand uploadCommand) {
		List<SystemListItem> groupTypeList = getSystemListItemsForGroupTypes();
		Map<String, PortfolioCashFlowGroup> portfolioCashFlowGroupMap = new HashMap<>();
		Map<String, PortfolioCashFlowType> portfolioCashFlowTypeMap = new HashMap<>();
		Map<String, InvestmentAccount> clientInvestmentAccountMap = new HashMap<>();
		Map<String, InvestmentSecurity> currencyInvestmentSecurityMap = new HashMap<>();

		// map of account-level defined default cash flow group type (ListItem) -- key is account ID
		Map<InvestmentAccount, SystemListItem> defaultCashFlowGroupTypeMap = new HashMap<>();

		// map of available ListItem entries representing a portfolio cash flow group type, -- key is the value of the item.
		Map<String, SystemListItem> portfolioCashFlowGroupTypeByValueMap = BeanUtils.getBeanMap(groupTypeList, SystemListItem::getValue);

		// map of available ListItem entries representing a portfolio cash flow group type, -- key is the ID of the item.
		Map<Integer, SystemListItem> portfolioCashFlowGroupTypeByIdMap = BeanUtils.getBeanMap(groupTypeList, SystemListItem::getId);

		// list of groups that need to have existing PortfolioCashFlow entries removed (for overwrite mode)
		List<PortfolioCashFlowGroup> groupsWithDataForDeletion = new ArrayList<>();

		List<IdentityObject> beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, false);
		int counter = 0;

		for (Object bean : CollectionUtils.getIterable(beanList)) {
			PortfolioCashFlow portfolioCashFlow = (PortfolioCashFlow) bean;

			// At this point, portfolioCashFlow has a sparsely populated PortfolioCashFlowType with just a name property. Look up the actual type object by name.
			ValidationUtils.assertNotNull(portfolioCashFlow.getCashFlowType(), "Could not find CashFlowType based on the input data and data load options.");
			String portfolioTypeName = portfolioCashFlow.getCashFlowType().getName();
			PortfolioCashFlowType portfolioCashFlowType = portfolioCashFlowTypeMap.computeIfAbsent(portfolioTypeName, k -> getPortfolioCashFlowService().getPortfolioCashFlowTypeByName(k));
			ValidationUtils.assertNotNull(portfolioCashFlowType, "Cannot find PortfolioCashFlowType with name:" + portfolioTypeName);
			portfolioCashFlow.setCashFlowType(portfolioCashFlowType);


			if (portfolioCashFlow.getCashFlowGroup() == null) {
				portfolioCashFlow.setCashFlowGroup(new PortfolioCashFlowGroup());
			}

			if (portfolioCashFlow.getCashFlowGroup().getInvestmentAccount() == null && uploadCommand.getInvestmentAccount() != null) {
				portfolioCashFlow.getCashFlowGroup().setInvestmentAccount(uploadCommand.getInvestmentAccount());
			}

			// PortfolioCashFlowGroup and InvestmentAccount are just containers with minimal information
			ValidationUtils.assertNotNull(portfolioCashFlow.getCashFlowGroup(), "Group is missing for cash flow data entry.");
			ValidationUtils.assertNotNull(portfolioCashFlow.getCashFlowGroup().getAsOfDate(), "A group as of date is required.");
			ValidationUtils.assertNotNull(portfolioCashFlow.getCashFlowGroup().getEffectiveStartDate(), "An effective start date is required.");
			ValidationUtils.assertNotNull(portfolioCashFlow.getCashFlowGroup().getInvestmentAccount(), "A client account is required.");
			ValidationUtils.assertNotNull(portfolioCashFlow.getCashFlowAmount(), "A cash flow amount is required.");
			ValidationUtils.assertNotNull(portfolioCashFlow.getCashFlowYear(), "A cash flow year is required.");
			ValidationUtils.assertNotNull(portfolioCashFlow.getCashFlowType(), "A cash flow type is required.");

			//Note:  the InvestmentAccount property in the InvestmentSecurityGroup is sparsely populated with just an accountNumber.  We will need to look it up to get the BaseCurrency.
			String accountNumber = portfolioCashFlow.getCashFlowGroup().getInvestmentAccount().getNumber();
			final InvestmentAccount clientAccount = clientInvestmentAccountMap.computeIfAbsent(accountNumber, k -> getInvestmentAccountService().getInvestmentAccountByNumber(k));
			ValidationUtils.assertNotNull(clientAccount, "Cannot find client account with number: " + accountNumber);

			// determine if an investment security (complete) was passed down via a parameter, if so, use that one.
			InvestmentSecurity currencyInvestmentSecurity = portfolioCashFlow.getCashFlowGroup() != null ? portfolioCashFlow.getCashFlowGroup().getCurrencyInvestmentSecurity() : null;
			if (currencyInvestmentSecurity == null || currencyInvestmentSecurity.isNewBean()) {
				String currencySymbol = portfolioCashFlow.getCashFlowGroup().getCurrencyInvestmentSecurity() != null ? portfolioCashFlow.getCashFlowGroup().getCurrencyInvestmentSecurity().getSymbol() : null;
				currencyInvestmentSecurity = StringUtils.isEmpty(currencySymbol) ? clientAccount.getBaseCurrency() : currencyInvestmentSecurityMap.computeIfAbsent(currencySymbol, k -> getInvestmentInstrumentService().getInvestmentSecurityBySymbol(k, true));
				ValidationUtils.assertNotNull(currencyInvestmentSecurity, "Cannot find currency security with symbol: " + currencySymbol);
			}
			final Date effectiveStartDate = portfolioCashFlow.getCashFlowGroup().getEffectiveStartDate();
			final Date effectiveEndDate = portfolioCashFlow.getCashFlowGroup().getEffectiveEndDate();
			final Date asOfDate = portfolioCashFlow.getCashFlowGroup().getAsOfDate();

			// resolve PortfolioCashFlowGroup type
			SystemListItem portfolioCashFlowGroupType;
			if (portfolioCashFlow.getCashFlowGroup().getCashFlowGroupType() != null && portfolioCashFlow.getCashFlowGroup().getCashFlowGroupType().getValue() != null) {
				String key = portfolioCashFlow.getCashFlowGroup().getCashFlowGroupType().getValue();

				ValidationUtils.assertTrue(portfolioCashFlowGroupTypeByValueMap.containsKey(key), "Invalid cash flow group type value: " + key);
				portfolioCashFlowGroupType = portfolioCashFlowGroupTypeByValueMap.get(key);
			}
			else {
				portfolioCashFlowGroupType = defaultCashFlowGroupTypeMap.computeIfAbsent(clientAccount, accountKey -> {
					Integer systemListItemId = (Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(accountKey, InvestmentAccount.CLIENT_ACCOUNT_PORTFOLIO_SETUP_COLUMN_GROUP_NAME, InvestmentAccount.CLIENT_ACCOUNT_PORTFOLIO_CASH_FLOW_GROUP_TYPE, false);
					return systemListItemId != null ? portfolioCashFlowGroupTypeByIdMap.get(systemListItemId) : null;
				});
			}
			ValidationUtils.assertNotNull(portfolioCashFlowGroupType, "Could not attain a group type value from uploaded data nor from client account: " + clientAccount + ".");

			// try looking up a matching group before creating a new one. If a new group is created, add it to the map for reuse.
			String groupLookupKey = getPortfolioGroupMapKey(clientAccount, currencyInvestmentSecurity, asOfDate, effectiveStartDate, effectiveEndDate, portfolioCashFlowGroupType);
			PortfolioCashFlowGroup group = portfolioCashFlowGroupMap.get(groupLookupKey);
			if (group == null) {
				group = findCashFlowGroup(clientAccount, currencyInvestmentSecurity, asOfDate, effectiveStartDate, effectiveEndDate, portfolioCashFlowGroupType);
				ValidationUtils.assertFalse(group == null && uploadCommand.isOverwriteExistingGroupData(), "No group found in which to replace cash flow entries.  Check import file to ensure group-specific data was not modified.");
				if (group != null && uploadCommand.isOverwriteExistingGroupData()) {
					groupsWithDataForDeletion.add(group);
				}
				else if (group == null || uploadCommand.isEndActiveGroup()) {
					group = createAndSaveCashFlowGroup(uploadCommand, clientAccount, currencyInvestmentSecurity, asOfDate, effectiveStartDate, effectiveEndDate, portfolioCashFlowGroupType);
				}
				portfolioCashFlowGroupMap.putIfAbsent(groupLookupKey, group);
			}

			portfolioCashFlow.setCashFlowGroup(group);
			if (group.getPortfolioCashFlowList() == null) {
				group.setPortfolioCashFlowList(new ArrayList<>());
			}
			group.getPortfolioCashFlowList().add(portfolioCashFlow);
			++counter;
		}

		if (uploadCommand.isOverwriteExistingGroupData()) {
			deletePortfolioCashFlowRowsForGroups(groupsWithDataForDeletion);
		}
		savePortfolioGroupList(portfolioCashFlowGroupMap.values());

		uploadCommand.getUploadResult().addUploadResults("PortfolioCashFlow", counter, true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Transactional
	protected void savePortfolioGroupList(Collection<PortfolioCashFlowGroup> portfolioCashFlowGroupCollection) {
		for (PortfolioCashFlowGroup portfolioCashFlowGroup : CollectionUtils.getIterable(portfolioCashFlowGroupCollection)) {
			if (portfolioCashFlowGroup != null) {
				// saves the group and any PortfolioCashFlow entities in the group's PortfolioCashFlow list.
				getPortfolioCashFlowService().savePortfolioCashFlowGroup(portfolioCashFlowGroup);
			}
		}
	}


	@Transactional
	protected void deletePortfolioCashFlowRowsForGroups(List<PortfolioCashFlowGroup> portfolioCashFlowGroupList) {
		for (PortfolioCashFlowGroup group : CollectionUtils.getIterable(portfolioCashFlowGroupList)) {
			getPortfolioCashFlowService().deletePortfolioCashFlowsInCashFlowGroup(group);
		}
	}


	private String getPortfolioGroupMapKey(InvestmentAccount clientAccount, InvestmentSecurity currencySecurity, Date asOfDate, Date effectiveStartDate, Date effectiveEndDate, SystemListItem portfolioCashFlowGroupType) {
		return StringUtils.generateKey(clientAccount.getNumber(), currencySecurity.getSymbol(), asOfDate, effectiveStartDate, effectiveEndDate, portfolioCashFlowGroupType != null ? portfolioCashFlowGroupType.getValue() : "");
	}


	/**
	 * Searches for an existing PortfolioCashFlow group that matches the criteria specified in the function parameters.
	 */
	private PortfolioCashFlowGroup findCashFlowGroup(InvestmentAccount clientAccount, InvestmentSecurity currencyInvestmentSecurity, Date asOfDate, Date effectiveStartDate, Date effectiveEndDate, SystemListItem groupType) {
		PortfolioCashFlowGroupSearchForm searchForm = new PortfolioCashFlowGroupSearchForm();
		searchForm.setInvestmentAccountNumberEquals(clientAccount.getNumber());
		if (groupType != null) {
			searchForm.setGroupType(groupType.getValue());
		}
		searchForm.setCurrencySymbol(currencyInvestmentSecurity.getSymbol());
		searchForm.setAsOfDate(asOfDate);
		searchForm.setEffectiveStartDate(effectiveStartDate);
		if (effectiveEndDate == null) {
			searchForm.addSearchRestriction(new SearchRestriction("effectiveEndDate", ComparisonConditions.IS_NULL, null));
		}
		else {
			searchForm.setEffectiveEndDate(effectiveEndDate);
		}

		List<PortfolioCashFlowGroup> portfolioCashFlowGroupList = getPortfolioCashFlowService().getPortfolioCashFlowGroupList(searchForm);
		ValidationUtils.assertTrue(portfolioCashFlowGroupList.size() <= 1,
				"Multiple PortfolioCashFlow group entries exist for client account number: " + clientAccount.getNumber() + ", currency security: " + currencyInvestmentSecurity.getSymbol() + ", effectiveStartDate: " + effectiveStartDate + (effectiveEndDate != null ? ", effectiveEndDate: " + effectiveEndDate : "") + ".");

		return portfolioCashFlowGroupList.isEmpty() ? null : portfolioCashFlowGroupList.get(0);
	}


	/**
	 * Creates a new PortfolioCashFlow group with the current date as the starting date, and closes the original group, by assigning
	 * an end date of current - 1 day.
	 */
	@Transactional
	protected PortfolioCashFlowGroup createAndSaveCashFlowGroup(PortfolioCashFlowUploadCommand uploadCommand, InvestmentAccount clientAccount, InvestmentSecurity currencyInvestmentSecurity, Date asOfDate, Date effectiveStartDate, Date effectiveEndDate, SystemListItem groupType) {
		ValidationUtils.assertNotNull(effectiveStartDate, "Start date is required for cash flow groups.");
		Date previousDate = DateUtils.addDays(effectiveStartDate, -1);

		// Search for an active existing cash flow group with any start date and null end date
		PortfolioCashFlowGroup existingCashFlowGroup = findCashFlowGroup(clientAccount, currencyInvestmentSecurity, null, null, null, groupType);
		if (uploadCommand.isEndActiveGroup() && existingCashFlowGroup != null && existingCashFlowGroup.getEffectiveEndDate() == null) {
			existingCashFlowGroup.setEffectiveEndDate(previousDate);
			// check end date to ensure its on or after startDate.
			if (DateUtils.compare(existingCashFlowGroup.getEffectiveEndDate(), existingCashFlowGroup.getEffectiveStartDate(), false) < 0) {
				existingCashFlowGroup.setEffectiveEndDate(existingCashFlowGroup.getEffectiveStartDate());
			}
			getPortfolioCashFlowService().savePortfolioCashFlowGroup(existingCashFlowGroup);
		}

		PortfolioCashFlowGroup portfolioCashFlowGroup = new PortfolioCashFlowGroup();
		portfolioCashFlowGroup.setInvestmentAccount(clientAccount);
		portfolioCashFlowGroup.setCurrencyInvestmentSecurity(currencyInvestmentSecurity);
		portfolioCashFlowGroup.setAsOfDate(asOfDate);
		portfolioCashFlowGroup.setEffectiveStartDate(effectiveStartDate);
		portfolioCashFlowGroup.setEffectiveEndDate(effectiveEndDate);
		portfolioCashFlowGroup.setCashFlowGroupType(groupType);

		return getPortfolioCashFlowService().savePortfolioCashFlowGroup(portfolioCashFlowGroup);
	}


	private List<SystemListItem> getSystemListItemsForGroupTypes() {
		SystemListItemSearchForm searchForm = new SystemListItemSearchForm();
		searchForm.setListName(PortfolioCashFlowGroup.CASH_FLOW_GROUP_TYPE_SYSTEM_LIST_NAME);
		return getSystemListService().getSystemListItemList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowService getPortfolioCashFlowService() {
		return this.portfolioCashFlowService;
	}


	public void setPortfolioCashFlowService(PortfolioCashFlowService portfolioCashFlowService) {
		this.portfolioCashFlowService = portfolioCashFlowService;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SystemListService<SystemList> getSystemListService() {
		return this.systemListService;
	}


	public void setSystemListService(SystemListService<SystemList> systemListService) {
		this.systemListService = systemListService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
