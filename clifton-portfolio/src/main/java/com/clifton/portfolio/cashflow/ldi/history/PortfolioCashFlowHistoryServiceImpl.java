package com.clifton.portfolio.cashflow.ldi.history;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.cashflow.ldi.history.cache.PortfolioCashFlowGroupHistoryBalanceDateCache;
import com.clifton.portfolio.cashflow.ldi.history.search.PortfolioCashFlowGroupHistorySearchForm;
import com.clifton.portfolio.cashflow.ldi.history.search.PortfolioCashFlowGroupHistorySummarySearchForm;
import com.clifton.portfolio.cashflow.ldi.history.search.PortfolioCashFlowHistorySearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * Basic implementation of the {@link PortfolioCashFlowHistoryService}.
 *
 * @author michalm
 */
@Service
public class PortfolioCashFlowHistoryServiceImpl implements PortfolioCashFlowHistoryService {

	private AdvancedUpdatableDAO<PortfolioCashFlowHistory, Criteria> portfolioCashFlowHistoryDAO;
	private AdvancedUpdatableDAO<PortfolioCashFlowGroupHistory, Criteria> portfolioCashFlowGroupHistoryDAO;
	private AdvancedUpdatableDAO<PortfolioCashFlowGroupHistorySummary, Criteria> portfolioCashFlowGroupHistorySummaryDAO;

	private PortfolioCashFlowGroupHistoryBalanceDateCache portfolioCashFlowGroupHistoryBalanceDateCache;
	private DaoSingleKeyListCache<PortfolioCashFlowHistory, Integer> portfolioCashFlowHistoryListCache;

	private CalendarBusinessDayService calendarBusinessDayService;


	////////////////////////////////////////////////////////////////////////////
	//////         Portfolio Cash Flow Group History Methods             ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioCashFlowGroupHistory getPortfolioCashFlowGroupHistory(int id, boolean populateCashFlowHistory) {
		PortfolioCashFlowGroupHistory groupHistory = getPortfolioCashFlowGroupHistoryDAO().findByPrimaryKey(id);
		if (groupHistory != null && populateCashFlowHistory) {
			groupHistory.setCashFlowHistoryList(getCashFlowHistoryListForCashFlowGroupHistory(id));
		}
		return groupHistory;
	}


	@Override
	public PortfolioCashFlowGroupHistory getPortfolioCashFlowGroupHistoryForBalanceDate(int cashFlowGroupId, Date balanceDate, boolean populateCashFlowHistory) {
		PortfolioCashFlowGroupHistory groupHistory = getPortfolioCashFlowGroupHistoryBalanceDateCache().getPortfolioCashFlowGroupHistoryLastForCashFlowGroupAndBalanceDate(getPortfolioCashFlowGroupHistoryDAO(), cashFlowGroupId, balanceDate);
		if (groupHistory != null && populateCashFlowHistory) {
			groupHistory.setCashFlowHistoryList(getCashFlowHistoryListForCashFlowGroupHistory(groupHistory.getId()));
		}
		return groupHistory;
	}


	@Override
	public List<PortfolioCashFlowGroupHistory> getPortfolioCashFlowGroupHistoryList(PortfolioCashFlowGroupHistorySearchForm searchForm) {
		return getPortfolioCashFlowGroupHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getActiveOnDate() != null) {
					String alias = getPathAlias("cashFlowGroup", criteria);
					criteria.add(ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(true, alias, "effectiveStartDate", "effectiveEndDate", searchForm.getActiveOnDate()));
				}
			}
		});
	}


	@Override
	public PortfolioCashFlowGroupHistory savePortfolioCashFlowGroupHistory(PortfolioCashFlowGroupHistory portfolioCashFlowGroupHistory, Date balanceDate) {
		ValidationUtils.assertTrue(portfolioCashFlowGroupHistory.isNewBean(), "Direct updates to existing Cash Flow Group History records are not allowed.");
		ValidationUtils.assertNotEmpty(portfolioCashFlowGroupHistory.getCashFlowHistoryList(), "Cash Flow History is required to save Cash Flow Group History.");
		PortfolioCashFlowGroupHistory existingGroupHistory = getPortfolioCashFlowGroupHistoryForBalanceDate(portfolioCashFlowGroupHistory.getCashFlowGroup().getId(), balanceDate, true);

		// If missing - get value for previous weekday
		if (existingGroupHistory == null) {
			existingGroupHistory = getPortfolioCashFlowGroupHistoryForBalanceDate(portfolioCashFlowGroupHistory.getCashFlowGroup().getId(), DateUtils.getPreviousWeekday(balanceDate), true);
		}

		// If still missing and previous weekday is a holiday (default calendar) - get existing from previous business day
		if (existingGroupHistory == null && !getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(DateUtils.getPreviousWeekday(balanceDate)))) {
			existingGroupHistory = getPortfolioCashFlowGroupHistoryForBalanceDate(portfolioCashFlowGroupHistory.getCashFlowGroup().getId(), getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(balanceDate)), true);
		}

		boolean insert = existingGroupHistory == null || isCashFlowGroupHistoryDifferent(portfolioCashFlowGroupHistory, existingGroupHistory);

		if (insert) {
			portfolioCashFlowGroupHistory.setStartDate(balanceDate);
			portfolioCashFlowGroupHistory.setEndDate(balanceDate);
			List<PortfolioCashFlowHistory> cashFlowHistoryList = portfolioCashFlowGroupHistory.getCashFlowHistoryList();
			portfolioCashFlowGroupHistory = getPortfolioCashFlowGroupHistoryDAO().save(portfolioCashFlowGroupHistory);

			for (PortfolioCashFlowHistory cashFlowHistory : cashFlowHistoryList) {
				cashFlowHistory.setCashFlowGroupHistory(portfolioCashFlowGroupHistory);
			}
			getPortfolioCashFlowHistoryDAO().saveList(cashFlowHistoryList);
			portfolioCashFlowGroupHistory.setCashFlowHistoryList(cashFlowHistoryList);
			return portfolioCashFlowGroupHistory;
		}
		else {
			// extend endDate of existing history because there are no changes
			existingGroupHistory.setEndDate(balanceDate);
			return getPortfolioCashFlowGroupHistoryDAO().save(existingGroupHistory);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected List<PortfolioCashFlowHistory> getCashFlowHistoryListForCashFlowGroupHistory(int cashFlowGroupHistoryId) {
		return getPortfolioCashFlowHistoryListCache().getBeanListForKeyValue(getPortfolioCashFlowHistoryDAO(), cashFlowGroupHistoryId);
	}


	/**
	 * Two {@link PortfolioCashFlowGroupHistory} records are the same if their Cash Flow History is the same and their Discount Curve data is the same.
	 */
	private boolean isCashFlowGroupHistoryDifferent(PortfolioCashFlowGroupHistory newGroupHistory, PortfolioCashFlowGroupHistory existingGroupHistory) {
		return newGroupHistory.getDiscountCurveSecurity() != existingGroupHistory.getDiscountCurveSecurity() || newGroupHistory.getDiscountCurveValueDate() != existingGroupHistory.getDiscountCurveValueDate() || isCashFlowHistoryDifferent(newGroupHistory, existingGroupHistory);
	}


	/**
	 * Return true if Cash Flow History does not match for the new/existing {@link PortfolioCashFlowGroupHistory}.
	 */
	private boolean isCashFlowHistoryDifferent(PortfolioCashFlowGroupHistory newGroupHistory, PortfolioCashFlowGroupHistory existingGroupHistory) {
		List<PortfolioCashFlowHistory> newCashFlowHistoryList = newGroupHistory.getCashFlowHistoryList();
		List<PortfolioCashFlowHistory> existingCashFlowHistoryList = existingGroupHistory.getCashFlowHistoryList();

		if (!MathUtils.isEqual(CollectionUtils.getSize(newCashFlowHistoryList), CollectionUtils.getSize(existingCashFlowHistoryList))) {
			return true;
		}

		for (PortfolioCashFlowHistory existingCashFlowHistory : existingCashFlowHistoryList) {
			boolean found = false;
			for (PortfolioCashFlowHistory newCashFlowHistory : newCashFlowHistoryList) {
				if (existingCashFlowHistory.equals(newCashFlowHistory)) {
					found = true;
					break;
				}
			}
			if (!found) {
				return true;
			}
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	//////     Portfolio Cash Flow Group History Summary Methods         ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioCashFlowGroupHistorySummary getPortfolioCashFlowGroupHistorySummary(int id) {
		return getPortfolioCashFlowGroupHistorySummaryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortfolioCashFlowGroupHistorySummary> getPortfolioCashFlowGroupHistorySummaryList(PortfolioCashFlowGroupHistorySummarySearchForm searchForm) {
		return getPortfolioCashFlowGroupHistorySummaryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getActiveOnDate() != null) {
					String alias = getPathAlias("cashFlowGroupHistory", criteria);
					criteria.add(ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(true, alias, "startDate", "endDate", searchForm.getActiveOnDate()));
					String groupAlias = getPathAlias("cashFlowGroupHistory.cashFlowGroup", criteria);
					criteria.add(ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(true, groupAlias, "effectiveStartDate", "effectiveEndDate", searchForm.getActiveOnDate()));
				}
			}
		});
	}


	@Override
	public List<PortfolioCashFlowGroupHistorySummary> getPortfolioCashFlowGroupHistorySummaryByGroupHistoryList(int cashFlowGroupHistoryId) {
		PortfolioCashFlowGroupHistorySummarySearchForm searchForm = new PortfolioCashFlowGroupHistorySummarySearchForm();
		searchForm.setCashFlowGroupHistoryId(cashFlowGroupHistoryId);
		return getPortfolioCashFlowGroupHistorySummaryList(searchForm);
	}


	@Override
	public PortfolioCashFlowGroupHistorySummary savePortfolioCashFlowGroupHistorySummary(PortfolioCashFlowGroupHistorySummary portfolioCashFlowGroupHistorySummary) {
		return getPortfolioCashFlowGroupHistorySummaryDAO().save(portfolioCashFlowGroupHistorySummary);
	}


	@Override
	public void savePortfolioCashFlowGroupHistorySummaryList(List<PortfolioCashFlowGroupHistorySummary> portfolioCashFlowGroupHistorySummaryList) {
		getPortfolioCashFlowGroupHistorySummaryDAO().saveList(portfolioCashFlowGroupHistorySummaryList);
	}


	////////////////////////////////////////////////////////////////////////////
	//////            Portfolio Cash FLow History Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioCashFlowHistory getPortfolioCashFlowHistory(int id) {
		return getPortfolioCashFlowHistoryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortfolioCashFlowHistory> getPortfolioCashFlowHistoryList(PortfolioCashFlowHistorySearchForm searchForm) {
		return getPortfolioCashFlowHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getActiveOnDate() != null) {
					String alias = getPathAlias("cashFlowGroupHistory", criteria);
					criteria.add(ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(true, alias, "startDate", "endDate", searchForm.getActiveOnDate()));
					String groupAlias = getPathAlias("cashFlowGroupHistory.cashFlowGroup", criteria);
					criteria.add(ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(true, groupAlias, "effectiveStartDate", "effectiveEndDate", searchForm.getActiveOnDate()));
				}
			}
		});
	}


	@Override
	public PortfolioCashFlowHistory savePortfolioCashFlowHistory(PortfolioCashFlowHistory portfolioCashFlowHistory) {
		return getPortfolioCashFlowHistoryDAO().save(portfolioCashFlowHistory);
	}


	@Override
	public void deletePortfolioCashFlowHistory(int id) {
		getPortfolioCashFlowHistoryDAO().delete(id);
	}


	@Transactional
	@Override
	public void clearPortfolioCashFlowHistoryList(int cashFlowGroupId) {
		PortfolioCashFlowHistorySearchForm cashFlowHistorySearchForm = new PortfolioCashFlowHistorySearchForm();
		cashFlowHistorySearchForm.setCashFlowGroupId(cashFlowGroupId);
		getPortfolioCashFlowHistoryDAO().deleteList(getPortfolioCashFlowHistoryList(cashFlowHistorySearchForm));

		PortfolioCashFlowGroupHistorySummarySearchForm cashFlowGroupHistorySummarySearchForm = new PortfolioCashFlowGroupHistorySummarySearchForm();
		cashFlowGroupHistorySummarySearchForm.setCashFlowGroupId(cashFlowGroupId);
		getPortfolioCashFlowGroupHistorySummaryDAO().deleteList(getPortfolioCashFlowGroupHistorySummaryList(cashFlowGroupHistorySummarySearchForm));

		PortfolioCashFlowGroupHistorySearchForm searchForm = new PortfolioCashFlowGroupHistorySearchForm();
		searchForm.setCashFlowGroupId(cashFlowGroupId);
		getPortfolioCashFlowGroupHistoryDAO().deleteList(getPortfolioCashFlowGroupHistoryList(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortfolioCashFlowHistory, Criteria> getPortfolioCashFlowHistoryDAO() {
		return this.portfolioCashFlowHistoryDAO;
	}


	public void setPortfolioCashFlowHistoryDAO(AdvancedUpdatableDAO<PortfolioCashFlowHistory, Criteria> portfolioCashFlowHistoryDAO) {
		this.portfolioCashFlowHistoryDAO = portfolioCashFlowHistoryDAO;
	}


	public AdvancedUpdatableDAO<PortfolioCashFlowGroupHistory, Criteria> getPortfolioCashFlowGroupHistoryDAO() {
		return this.portfolioCashFlowGroupHistoryDAO;
	}


	public void setPortfolioCashFlowGroupHistoryDAO(AdvancedUpdatableDAO<PortfolioCashFlowGroupHistory, Criteria> portfolioCashFlowGroupHistoryDAO) {
		this.portfolioCashFlowGroupHistoryDAO = portfolioCashFlowGroupHistoryDAO;
	}


	public AdvancedUpdatableDAO<PortfolioCashFlowGroupHistorySummary, Criteria> getPortfolioCashFlowGroupHistorySummaryDAO() {
		return this.portfolioCashFlowGroupHistorySummaryDAO;
	}


	public void setPortfolioCashFlowGroupHistorySummaryDAO(AdvancedUpdatableDAO<PortfolioCashFlowGroupHistorySummary, Criteria> portfolioCashFlowGroupHistorySummaryDAO) {
		this.portfolioCashFlowGroupHistorySummaryDAO = portfolioCashFlowGroupHistorySummaryDAO;
	}


	public PortfolioCashFlowGroupHistoryBalanceDateCache getPortfolioCashFlowGroupHistoryBalanceDateCache() {
		return this.portfolioCashFlowGroupHistoryBalanceDateCache;
	}


	public void setPortfolioCashFlowGroupHistoryBalanceDateCache(PortfolioCashFlowGroupHistoryBalanceDateCache portfolioCashFlowGroupHistoryBalanceDateCache) {
		this.portfolioCashFlowGroupHistoryBalanceDateCache = portfolioCashFlowGroupHistoryBalanceDateCache;
	}


	public DaoSingleKeyListCache<PortfolioCashFlowHistory, Integer> getPortfolioCashFlowHistoryListCache() {
		return this.portfolioCashFlowHistoryListCache;
	}


	public void setPortfolioCashFlowHistoryListCache(DaoSingleKeyListCache<PortfolioCashFlowHistory, Integer> portfolioCashFlowHistoryListCache) {
		this.portfolioCashFlowHistoryListCache = portfolioCashFlowHistoryListCache;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}
