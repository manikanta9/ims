package com.clifton.portfolio.cashflow.ldi.history.cache;


import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory;
import com.clifton.portfolio.cashflow.ldi.history.search.PortfolioCashFlowGroupHistorySearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * The <code>PortfolioCashFlowGroupHistoryBalanceDateCache</code> caches key PortfolioCashFlowGroupID_BalanceDate to the history record id that is the last available history
 * for that balance date.
 *
 * @author michaelm
 */
@Component
public class PortfolioCashFlowGroupHistoryBalanceDateCacheImpl extends SelfRegisteringSimpleDaoCache<PortfolioCashFlowGroupHistory, String, ObjectWrapper<Integer>> implements PortfolioCashFlowGroupHistoryBalanceDateCache {


	private String getBeanKeyForCashFlowGroupAndDate(int cashFlowGroupId, Date date) {
		return cashFlowGroupId + "_" + DateUtils.fromDateShort(date);
	}


	@Override
	public PortfolioCashFlowGroupHistory getPortfolioCashFlowGroupHistoryLastForCashFlowGroupAndBalanceDate(ReadOnlyDAO<PortfolioCashFlowGroupHistory> dao, Integer cashFlowGroupId, Date balanceDate) {
		ObjectWrapper<Integer> id = getCacheHandler().get(getCacheName(), getBeanKeyForCashFlowGroupAndDate(cashFlowGroupId, balanceDate));
		if (id != null) {
			if (!id.isPresent()) {
				// id is not null, but not present then there is NO history for the cashFlowGroup and date don't look it up in the database again
				return null;
			}
			PortfolioCashFlowGroupHistory bean = dao.findByPrimaryKey(id.getObject());
			// If bean is null, then transaction was rolled back, look it up again and reset the cache
			if (bean != null) {
				return bean;
			}
		}
		PortfolioCashFlowGroupHistory bean = lookupBean(dao, cashFlowGroupId, balanceDate);
		setBean(bean, cashFlowGroupId, balanceDate);
		return bean;
	}


	protected PortfolioCashFlowGroupHistory lookupBean(ReadOnlyDAO<PortfolioCashFlowGroupHistory> dao, int cashFlowGroupId, Date balanceDate) {
		PortfolioCashFlowGroupHistorySearchForm searchForm = new PortfolioCashFlowGroupHistorySearchForm();
		searchForm.setCashFlowGroupId(cashFlowGroupId);
		searchForm.setActiveOnDate(balanceDate);
		searchForm.setOrderBy("createDate:DESC");
		searchForm.setLimit(1);
		List<PortfolioCashFlowGroupHistory> list = ((AdvancedReadOnlyDAO<PortfolioCashFlowGroupHistory, Criteria>) dao).findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		return CollectionUtils.getFirstElement(list);
	}


	protected void setBean(PortfolioCashFlowGroupHistory bean, int cashFlowGroupId, Date balanceDate) {
		ObjectWrapper<Integer> id = (bean != null ? new ObjectWrapper<>(bean.getId()) : new ObjectWrapper<>());
		getCacheHandler().put(getCacheName(), getBeanKeyForCashFlowGroupAndDate(cashFlowGroupId, balanceDate), id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<PortfolioCashFlowGroupHistory> dao, DaoEventTypes event, PortfolioCashFlowGroupHistory bean) {
		if (event.isUpdate()) { // Only needed for updates as we only need to clear the dates that have changes (i.e. could be the same result for 3 months and then we add a day, only clear for the day added)
			// Call it so we have the original
			getOriginalBean(dao, bean);
		}
	}


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<PortfolioCashFlowGroupHistory> dao, @SuppressWarnings("unused") DaoEventTypes event, PortfolioCashFlowGroupHistory bean, Throwable e) {
		if (e == null) {
			PortfolioCashFlowGroupHistory originalBean = event.isUpdate() ? getOriginalBean(dao, bean) : null;
			if (originalBean == null) {
				removeFromCacheForDateRange(bean.getCashFlowGroup().getId(), bean.getStartDate(), bean.getEndDate());
			}
			// On updates just clear for the dates that have changed - updates should only be extending the balance end date so we don't need to clear everything
			else {
				Date minStartDate = DateUtils.isDateBeforeOrEqual(originalBean.getStartDate(), bean.getStartDate(), false) ? originalBean.getStartDate() : bean.getStartDate();
				Date maxStartDate = DateUtils.isDateBeforeOrEqual(originalBean.getStartDate(), bean.getStartDate(), false) ? bean.getStartDate() : originalBean.getStartDate();
				if (!DateUtils.isEqualWithoutTime(minStartDate, maxStartDate)) {
					removeFromCacheForDateRange(bean.getCashFlowGroup().getId(), minStartDate, DateUtils.getPreviousWeekday(maxStartDate));
				}


				Date minEndDate = DateUtils.isDateAfterOrEqual(originalBean.getEndDate(), bean.getEndDate()) ? bean.getEndDate() : originalBean.getEndDate();
				Date maxEndDate = DateUtils.isDateAfterOrEqual(originalBean.getEndDate(), bean.getEndDate()) ? originalBean.getEndDate() : bean.getEndDate();
				if (!DateUtils.isEqualWithoutTime(minEndDate, maxEndDate)) {
					removeFromCacheForDateRange(bean.getCashFlowGroup().getId(), DateUtils.getNextWeekday(minEndDate), maxEndDate);
				}
			}
		}
	}


	private void removeFromCacheForDateRange(int cashFlowGroupId, Date startDate, Date endDate) {
		Date date = startDate;

		while (DateUtils.isDateBeforeOrEqual(date, endDate, false)) {
			getCacheHandler().remove(getCacheName(), getBeanKeyForCashFlowGroupAndDate(cashFlowGroupId, date));
			date = DateUtils.getNextWeekday(date);
		}
	}
}
