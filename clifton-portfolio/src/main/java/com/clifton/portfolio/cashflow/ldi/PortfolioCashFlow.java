package com.clifton.portfolio.cashflow.ldi;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.StringUtils;

import java.math.BigDecimal;


/**
 * The data representing a cash flow or liability for a {@link PortfolioCashFlowGroup} and {@link PortfolioCashFlowType}.
 *
 * @author davidi
 */
public class PortfolioCashFlow extends BaseEntity<Integer> {

	private PortfolioCashFlowType cashFlowType;

	private PortfolioCashFlowGroup cashFlowGroup;

	private Short cashFlowYear;

	private BigDecimal cashFlowAmount;

	private Short startingMonthNumber;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getEndingMonthNumber() {
		if (getStartingMonthNumber() == null) {
			return null;
		}
		return (short) (getStartingMonthNumber() + 11);
	}


	public String getLabel() {
		String cashFlowTypeFragment = getCashFlowType() != null ? getCashFlowType().getLabel() : "";
		String cashFlowGroupLabel = getCashFlowGroup() != null ? getCashFlowGroup().getLabel() : "";

		String label = cashFlowTypeFragment + (!StringUtils.isEmpty(cashFlowGroupLabel) ? " - " + cashFlowGroupLabel : "");
		if (StringUtils.isEmpty(label)) {
			return null;
		}
		return label;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowType getCashFlowType() {
		return this.cashFlowType;
	}


	public void setCashFlowType(PortfolioCashFlowType cashFlowType) {
		this.cashFlowType = cashFlowType;
	}


	public PortfolioCashFlowGroup getCashFlowGroup() {
		return this.cashFlowGroup;
	}


	public void setCashFlowGroup(PortfolioCashFlowGroup cashFlowGroup) {
		this.cashFlowGroup = cashFlowGroup;
	}


	public Short getCashFlowYear() {
		return this.cashFlowYear;
	}


	public void setCashFlowYear(Short cashFlowYear) {
		this.cashFlowYear = cashFlowYear;
	}


	public BigDecimal getCashFlowAmount() {
		return this.cashFlowAmount;
	}


	public void setCashFlowAmount(BigDecimal cashFlowAmount) {
		this.cashFlowAmount = cashFlowAmount;
	}


	public Short getStartingMonthNumber() {
		return this.startingMonthNumber;
	}


	public void setStartingMonthNumber(Short startingMonthNumber) {
		this.startingMonthNumber = startingMonthNumber;
	}
}
