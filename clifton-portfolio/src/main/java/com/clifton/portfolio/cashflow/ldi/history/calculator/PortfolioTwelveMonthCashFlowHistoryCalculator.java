package com.clifton.portfolio.cashflow.ldi.history.calculator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlow;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowType;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistory;
import com.clifton.portfolio.cashflow.ldi.history.rebuild.PortfolioCashFlowHistoryRebuildConfig;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


/**
 * {@link BasePortfolioCashFlowHistoryCalculator} that adds 100 {@link PortfolioCashFlowHistory} to the given {@link PortfolioCashFlowGroupHistory} by aggregating
 * groups of 12 "monthly" cash flow amounts and then populates DV01 and KRD values.
 *
 * @author michaelm
 */
public class PortfolioTwelveMonthCashFlowHistoryCalculator extends BasePortfolioCashFlowHistoryCalculator {

	@Override
	public PortfolioCashFlowGroupHistory calculatePortfolioCashFlowHistory(PortfolioCashFlowHistoryRebuildConfig rebuildConfig, PortfolioCashFlowGroupHistory cashFlowGroupHistory, List<PortfolioCashFlow> cashFlowList) {
		Date balanceDate = rebuildConfig.getBalanceDate();
		short startingMonthNumber = (short) (DateUtils.getMonthsDifference(balanceDate, cashFlowGroupHistory.getCashFlowGroup().getAsOfDate()) + 1);
		short reportMonths = (short) (DateUtils.getMonthsDifference(balanceDate, cashFlowGroupHistory.getCashFlowGroup().getAsOfDate()));
		PortfolioCashFlowType cashFlowType = CollectionUtils.getFirstElementStrict(cashFlowList).getCashFlowType();
		PortfolioCashFlowHistoryPresentValueCalculationData presentValueCalculationData = rebuildConfig.getRebuildCommand().getPresentValueData();
		List<PortfolioCashFlowHistory> cashFlowHistoryList = new ArrayList<>();

		int adjustedYear = 1;
		while (adjustedYear <= 100) {
			if (CollectionUtils.isEmpty(cashFlowList)) {
				break;
			}
			PortfolioCashFlowHistory cashFlowHistory = new PortfolioCashFlowHistory();
			cashFlowHistory.setCashFlowGroupHistory(cashFlowGroupHistory);
			cashFlowHistory.setAdjustedYear(adjustedYear);
			cashFlowHistory.setStartingMonthNumber(startingMonthNumber);
			cashFlowHistory.setCashFlowType(cashFlowType);
			cashFlowHistory.setCashFlowHistoryAmount(calculateCashFlowHistoryAmount(cashFlowList, startingMonthNumber, reportMonths));
			cashFlowHistory = calculatePresentValues(cashFlowHistory, presentValueCalculationData, balanceDate);
			cashFlowHistoryList.add(cashFlowHistory);
			adjustedYear++;
			startingMonthNumber += 12;
			cashFlowGroupHistory.setTotalDv01Amount(MathUtils.add(cashFlowGroupHistory.getTotalDv01Amount(), cashFlowHistory.getDv01CashFlowHistoryAmount()));
		}
		cashFlowGroupHistory.getCashFlowHistoryList().addAll(cashFlowHistoryList);
		cashFlowGroupHistory.setDiscountCurveSecurity(presentValueCalculationData.getClientDiscountCurveSecurity());
		cashFlowGroupHistory.setDiscountCurveValueDate(presentValueCalculationData.getCurveRateMeasureDate());
		return cashFlowGroupHistory;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BigDecimal calculateCashFlowHistoryAmount(List<PortfolioCashFlow> cashFlowList, short startingMonthNumber, short reportMonths) {
		BigDecimal totalAmount = BigDecimal.ZERO;
		int numMonthsCalculated = 0;
		Iterator<PortfolioCashFlow> cashFlowIterator = cashFlowList.iterator();
		if (!cashFlowIterator.hasNext()) {
			return totalAmount;
		}
		PortfolioCashFlow cashFlow = cashFlowIterator.next();
		while (numMonthsCalculated < 12) {
			if (cashFlow.getStartingMonthNumber() <= startingMonthNumber && cashFlow.getEndingMonthNumber() >= startingMonthNumber) {
				int numValuesToUse = cashFlow.getEndingMonthNumber() - startingMonthNumber - numMonthsCalculated + 1;
				totalAmount = MathUtils.add(totalAmount, MathUtils.multiply(calculateAdjustedCashFlowAmount(cashFlow, reportMonths), numValuesToUse));
				startingMonthNumber += numValuesToUse;
				numMonthsCalculated += numValuesToUse;
			}
			else {
				if (cashFlowIterator.hasNext()) {
					cashFlow = cashFlowIterator.next();
				}
				else {
					return totalAmount;
				}
			}
		}
		return totalAmount;
	}
}
