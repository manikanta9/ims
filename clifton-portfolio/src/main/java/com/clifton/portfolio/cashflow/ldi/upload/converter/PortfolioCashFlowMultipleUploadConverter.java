package com.clifton.portfolio.cashflow.ldi.upload.converter;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlow;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowService;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowType;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowTypeSearchForm;
import com.clifton.system.upload.converter.SystemUploadBeanListConverterImpl;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;


/**
 * A converter used to process unmapped columns from an incoming upload file.
 *
 * @author davidi
 */
@Component
public class PortfolioCashFlowMultipleUploadConverter extends SystemUploadBeanListConverterImpl<PortfolioCashFlow> {

	private PortfolioCashFlowService portfolioCashFlowService;

	private Set<String> cashFlowTypeNameSet = null;


	/**
	 * Returns a list of {@link PortfolioCashFlow} objects that are a copy of the original bean
	 * and different data fields and values based on the unmapped property map
	 * <p>
	 * Note: Does not validate real data fields, just sets the name property for look ups later
	 */
	@Override
	protected List<PortfolioCashFlow> populateBeans(PortfolioCashFlow bean, Map<String, Object> unmappedPropertyMap) {
		List<PortfolioCashFlow> beanList = new ArrayList<>();

		for (Map.Entry<String, Object> stringObjectEntry : unmappedPropertyMap.entrySet()) {
			if (CollectionUtils.contains(getCashFlowTypeNameArray(), stringObjectEntry.getKey())) {
				PortfolioCashFlow copy = BeanUtils.cloneBean(bean, false, false);
				copy.setCashFlowType(new PortfolioCashFlowType());
				copy.getCashFlowType().setName(stringObjectEntry.getKey());
				copy.setCashFlowAmount((BigDecimal) Optional.ofNullable(stringObjectEntry.getValue()).orElse(BigDecimal.ZERO));
				beanList.add(copy);
			}
		}
		return beanList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Set<String> getCashFlowTypeNameArray() {
		if (this.cashFlowTypeNameSet == null) {
			this.cashFlowTypeNameSet = new HashSet<>();
			this.cashFlowTypeNameSet.addAll(CollectionUtils.getConverted(getPortfolioCashFlowService().getPortfolioCashFlowTypeList(new PortfolioCashFlowTypeSearchForm()), PortfolioCashFlowType::getName));
		}
		return this.cashFlowTypeNameSet;
	}


	public PortfolioCashFlowService getPortfolioCashFlowService() {
		return this.portfolioCashFlowService;
	}


	public void setPortfolioCashFlowService(PortfolioCashFlowService portfolioCashFlowService) {
		this.portfolioCashFlowService = portfolioCashFlowService;
	}
}

