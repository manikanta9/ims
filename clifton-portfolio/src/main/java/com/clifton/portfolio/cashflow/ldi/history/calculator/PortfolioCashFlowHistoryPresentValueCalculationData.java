package com.clifton.portfolio.cashflow.ldi.history.calculator;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PortfolioCashFlowHistoryPresentValueCalculationData</code> holds the data necessary for calculating KRD and DV01 values.
 *
 * @author michaelm
 */
public class PortfolioCashFlowHistoryPresentValueCalculationData {

	private InvestmentSecurity clientDiscountCurveSecurity;
	private int clientDiscountCurveDataDaysToExpiration;
	private int discountCurveSecurityDv01Periodicity;
	private int clientDv01Periodicity;
	private int clientCurveShockAmountBps; // aka Bump in bps

	private Date curveRateMeasureDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowHistoryPresentValueCalculationData(InvestmentSecurity clientDiscountCurveSecurity, int clientDiscountCurveDataDaysToExpiration, int discountCurveSecurityDv01Periodicity, int clientDv01Periodicity, int clientCurveShockAmountBps) {
		this.clientDiscountCurveSecurity = clientDiscountCurveSecurity;
		this.clientDiscountCurveDataDaysToExpiration = clientDiscountCurveDataDaysToExpiration;
		this.discountCurveSecurityDv01Periodicity = discountCurveSecurityDv01Periodicity;
		this.clientDv01Periodicity = clientDv01Periodicity;
		this.clientCurveShockAmountBps = clientCurveShockAmountBps;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getClientCurveShockAmountConvertedFromBps() {
		return MathUtils.divide(BigDecimal.valueOf(getClientCurveShockAmountBps()), BigDecimal.valueOf(10000));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getClientDiscountCurveSecurity() {
		return this.clientDiscountCurveSecurity;
	}


	public void setClientDiscountCurveSecurity(InvestmentSecurity clientDiscountCurveSecurity) {
		this.clientDiscountCurveSecurity = clientDiscountCurveSecurity;
	}


	public int getClientDiscountCurveDataDaysToExpiration() {
		return this.clientDiscountCurveDataDaysToExpiration;
	}


	public void setClientDiscountCurveDataDaysToExpiration(int clientDiscountCurveDataDaysToExpiration) {
		this.clientDiscountCurveDataDaysToExpiration = clientDiscountCurveDataDaysToExpiration;
	}


	public int getDiscountCurveSecurityDv01Periodicity() {
		return this.discountCurveSecurityDv01Periodicity;
	}


	public void setDiscountCurveSecurityDv01Periodicity(int discountCurveSecurityDv01Periodicity) {
		this.discountCurveSecurityDv01Periodicity = discountCurveSecurityDv01Periodicity;
	}


	public int getClientDv01Periodicity() {
		return this.clientDv01Periodicity;
	}


	public void setClientDv01Periodicity(int clientDv01Periodicity) {
		this.clientDv01Periodicity = clientDv01Periodicity;
	}


	public int getClientCurveShockAmountBps() {
		return this.clientCurveShockAmountBps;
	}


	public void setClientCurveShockAmountBps(int clientCurveShockAmountBps) {
		this.clientCurveShockAmountBps = clientCurveShockAmountBps;
	}


	public Date getCurveRateMeasureDate() {
		return this.curveRateMeasureDate;
	}


	public void setCurveRateMeasureDate(Date curveRateMeasureDate) {
		this.curveRateMeasureDate = curveRateMeasureDate;
	}
}
