package com.clifton.portfolio.cashflow.ldi;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.list.SystemListItem;

import java.util.Date;
import java.util.List;


/**
 * The group used to represent the communication start and end date of a given {@link PortfolioCashFlow} stream.
 *
 * @author davidi
 */
public class PortfolioCashFlowGroup extends BaseEntity<Integer> {

	public static final String CASH_FLOW_GROUP_TYPE_SYSTEM_LIST_NAME = "Portfolio Cash Flow Group Type";
	public static final String CASH_FLOW_GROUP_TYPE_LIST_ITEM_ROLLING = "Rolling";
	public static final String CASH_FLOW_GROUP_TYPE_LIST_ITEM_NON_ROLLING = "Non-Rolling";

	private InvestmentAccount investmentAccount;

	private InvestmentSecurity currencyInvestmentSecurity;

	private SystemListItem cashFlowGroupType;

	/**
	 * Used in conjunction with Balance Date when calculating rolled amounts.
	 */
	private Date asOfDate;

	private Date effectiveStartDate;

	private Date effectiveEndDate;

	/* Non persistent field populated with PortfolioCashFlow, used to facilitate processing of these entities in a group context.*/
	@NonPersistentField
	private List<PortfolioCashFlow> portfolioCashFlowList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		final String shortLabel = getLabelShort() != null ? getLabelShort() : "";
		final String asOfDateFragment = getAsOfDate() != null ? " - as_of: " + DateUtils.fromDate(getAsOfDate(), DateUtils.DATE_FORMAT_INPUT) : "";
		final String startDateFragment = getEffectiveStartDate() != null ? " - starts: " + DateUtils.fromDate(getEffectiveStartDate(), DateUtils.DATE_FORMAT_INPUT) : "";
		final String endDateFragment = getEffectiveEndDate() != null ? " - ends: " + DateUtils.fromDate(getEffectiveEndDate(), DateUtils.DATE_FORMAT_INPUT) : "";

		String label = shortLabel + asOfDateFragment + startDateFragment + endDateFragment;
		if (StringUtils.isEmpty(label)) {
			return null;
		}
		return label;
	}


	public String getLabelShort() {
		final String accountFragment = getInvestmentAccount() != null ? getInvestmentAccount().getLabel() : "";
		final String currencyFragment = getCurrencyInvestmentSecurity() != null ? " - " + getCurrencyInvestmentSecurity().getSymbol() : "";
		final String groupTypeFragment = getCashFlowGroupType() != null ? " - " + getCashFlowGroupType().getValue() : "";

		String label = accountFragment + currencyFragment + groupTypeFragment;
		if (StringUtils.isEmpty(label)) {
			return null;
		}
		return label;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public InvestmentSecurity getCurrencyInvestmentSecurity() {
		if (this.currencyInvestmentSecurity == null && getInvestmentAccount() != null) {
			return getInvestmentAccount().getBaseCurrency();
		}
		return this.currencyInvestmentSecurity;
	}


	public void setCurrencyInvestmentSecurity(InvestmentSecurity currencyInvestmentSecurity) {
		this.currencyInvestmentSecurity = currencyInvestmentSecurity;
	}


	public SystemListItem getCashFlowGroupType() {
		return this.cashFlowGroupType;
	}


	public void setCashFlowGroupType(SystemListItem cashFlowGroupType) {
		this.cashFlowGroupType = cashFlowGroupType;
	}


	public Date getAsOfDate() {
		return this.asOfDate;
	}


	public void setAsOfDate(Date asOfDate) {
		this.asOfDate = asOfDate;
	}


	public Date getEffectiveStartDate() {
		return this.effectiveStartDate;
	}


	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}


	public Date getEffectiveEndDate() {
		return this.effectiveEndDate;
	}


	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}


	public List<PortfolioCashFlow> getPortfolioCashFlowList() {
		return this.portfolioCashFlowList;
	}


	public void setPortfolioCashFlowList(List<PortfolioCashFlow> portfolioCashFlowList) {
		this.portfolioCashFlowList = portfolioCashFlowList;
	}
}
