package com.clifton.portfolio.cashflow.ldi.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlow;
import org.springframework.stereotype.Component;


/**
 * <code>PortfolioCashFlowValidator</code> performs validation on new or existing PortfolioCashFlow entities being saved.
 *
 * @author davidi
 */
@Component
public class PortfolioCashFlowValidator extends SelfRegisteringDaoValidator<PortfolioCashFlow> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(PortfolioCashFlow cashFlow, DaoEventTypes config) {
		ValidationUtils.assertNotNull(cashFlow.getCashFlowType(), "Portfolio Cash Flow Type cannot be null.");
		ValidationUtils.assertNotNull(cashFlow.getCashFlowGroup(), "Portfolio Cash Flow Group cannot be null.");
		ValidationUtils.assertNotNull(cashFlow.getCashFlowYear(), "Cash Flow Year cannot be null.");
		ValidationUtils.assertNotNull(cashFlow.getCashFlowAmount(), "Cash Flow Amount cannot be null.");
	}
}
