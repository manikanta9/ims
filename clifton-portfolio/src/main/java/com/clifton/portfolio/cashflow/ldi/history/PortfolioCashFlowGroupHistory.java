package com.clifton.portfolio.cashflow.ldi.history;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroup;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * Shows the changes for the given {@link com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroup} over time.
 *
 * @author michaelm
 */
public class PortfolioCashFlowGroupHistory extends BaseEntity<Integer> {

	private PortfolioCashFlowGroup cashFlowGroup;

	private Date startDate;

	private Date endDate;

	private InvestmentSecurity discountCurveSecurity;

	private Date discountCurveValueDate;

	private List<PortfolioCashFlowHistory> cashFlowHistoryList;

	@NonPersistentField
	private BigDecimal totalDv01Amount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		if (getCashFlowGroup() != null) {
			return getCashFlowGroup().getLabelShort() + " " + DateUtils.fromDateRange(getStartDate(), getEndDate(), true, true);
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowGroup getCashFlowGroup() {
		return this.cashFlowGroup;
	}


	public void setCashFlowGroup(PortfolioCashFlowGroup cashFlowGroup) {
		this.cashFlowGroup = cashFlowGroup;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public InvestmentSecurity getDiscountCurveSecurity() {
		return this.discountCurveSecurity;
	}


	public void setDiscountCurveSecurity(InvestmentSecurity discountCurveSecurity) {
		this.discountCurveSecurity = discountCurveSecurity;
	}


	public Date getDiscountCurveValueDate() {
		return this.discountCurveValueDate;
	}


	public void setDiscountCurveValueDate(Date discountCurveValueDate) {
		this.discountCurveValueDate = discountCurveValueDate;
	}


	public List<PortfolioCashFlowHistory> getCashFlowHistoryList() {
		return this.cashFlowHistoryList;
	}


	public void setCashFlowHistoryList(List<PortfolioCashFlowHistory> cashFlowHistoryList) {
		this.cashFlowHistoryList = cashFlowHistoryList;
	}


	public BigDecimal getTotalDv01Amount() {
		return this.totalDv01Amount;
	}


	public void setTotalDv01Amount(BigDecimal totalDv01Amount) {
		this.totalDv01Amount = totalDv01Amount;
	}
}
