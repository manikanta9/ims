package com.clifton.portfolio.cashflow.ldi.history.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Search form for {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistory}s
 *
 * @author michaelm
 */
public class PortfolioCashFlowHistorySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchFieldPath = "cashFlowGroupHistory.cashFlowGroup", searchField = "investmentAccount.id")
	private Integer investmentAccountId;

	@SearchField(searchField = "cashFlowGroupHistory.id")
	private Integer cashFlowGroupHistoryId;

	@SearchField(searchFieldPath = "cashFlowGroupHistory", searchField = "startDate")
	private Date startDate;

	@SearchField(searchFieldPath = "cashFlowGroupHistory", searchField = "endDate")
	private Date endDate;

	// Custom Search Filter
	private Date activeOnDate;

	@SearchField(searchField = "cashFlowGroupHistory.cashFlowGroup.id")
	private Integer cashFlowGroupId;

	@SearchField(searchField = "name", searchFieldPath = "cashFlowType")
	private String cashFlowTypeName;

	@SearchField
	private Integer adjustedYear;

	@SearchField
	private Short startingMonthNumber;

	@SearchField
	private BigDecimal cashFlowHistoryAmount;

	@SearchField
	private BigDecimal liabilityDiscountRate;

	@SearchField
	private BigDecimal liabilityCashFlowHistoryAmount;

	@SearchField
	private BigDecimal dv01DiscountRate;

	@SearchField
	private BigDecimal dv01CashFlowHistoryAmount;

	@SearchField
	private BigDecimal dv01Value;

	@SearchField
	private BigDecimal krdValue;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Integer getCashFlowGroupHistoryId() {
		return this.cashFlowGroupHistoryId;
	}


	public void setCashFlowGroupHistoryId(Integer cashFlowGroupHistoryId) {
		this.cashFlowGroupHistoryId = cashFlowGroupHistoryId;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Date getActiveOnDate() {
		return this.activeOnDate;
	}


	public void setActiveOnDate(Date activeOnDate) {
		this.activeOnDate = activeOnDate;
	}


	public Integer getCashFlowGroupId() {
		return this.cashFlowGroupId;
	}


	public void setCashFlowGroupId(Integer cashFlowGroupId) {
		this.cashFlowGroupId = cashFlowGroupId;
	}


	public String getCashFlowTypeName() {
		return this.cashFlowTypeName;
	}


	public void setCashFlowTypeName(String cashFlowTypeName) {
		this.cashFlowTypeName = cashFlowTypeName;
	}


	public Integer getAdjustedYear() {
		return this.adjustedYear;
	}


	public void setAdjustedYear(Integer adjustedYear) {
		this.adjustedYear = adjustedYear;
	}


	public Short getStartingMonthNumber() {
		return this.startingMonthNumber;
	}


	public void setStartingMonthNumber(Short startingMonthNumber) {
		this.startingMonthNumber = startingMonthNumber;
	}


	public BigDecimal getCashFlowHistoryAmount() {
		return this.cashFlowHistoryAmount;
	}


	public void setCashFlowHistoryAmount(BigDecimal cashFlowHistoryAmount) {
		this.cashFlowHistoryAmount = cashFlowHistoryAmount;
	}


	public BigDecimal getLiabilityDiscountRate() {
		return this.liabilityDiscountRate;
	}


	public void setLiabilityDiscountRate(BigDecimal liabilityDiscountRate) {
		this.liabilityDiscountRate = liabilityDiscountRate;
	}


	public BigDecimal getLiabilityCashFlowHistoryAmount() {
		return this.liabilityCashFlowHistoryAmount;
	}


	public void setLiabilityCashFlowHistoryAmount(BigDecimal liabilityCashFlowHistoryAmount) {
		this.liabilityCashFlowHistoryAmount = liabilityCashFlowHistoryAmount;
	}


	public BigDecimal getDv01DiscountRate() {
		return this.dv01DiscountRate;
	}


	public void setDv01DiscountRate(BigDecimal dv01DiscountRate) {
		this.dv01DiscountRate = dv01DiscountRate;
	}


	public BigDecimal getDv01CashFlowHistoryAmount() {
		return this.dv01CashFlowHistoryAmount;
	}


	public void setDv01CashFlowHistoryAmount(BigDecimal dv01CashFlowHistoryAmount) {
		this.dv01CashFlowHistoryAmount = dv01CashFlowHistoryAmount;
	}


	public BigDecimal getDv01Value() {
		return this.dv01Value;
	}


	public void setDv01Value(BigDecimal dv01Value) {
		this.dv01Value = dv01Value;
	}


	public BigDecimal getKrdValue() {
		return this.krdValue;
	}


	public void setKrdValue(BigDecimal krdValue) {
		this.krdValue = krdValue;
	}
}
