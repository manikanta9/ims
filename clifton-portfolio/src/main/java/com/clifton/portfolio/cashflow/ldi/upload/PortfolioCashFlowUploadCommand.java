package com.clifton.portfolio.cashflow.ldi.upload;


import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.system.upload.SystemUploadCommand;


/**
 * The <code>PortfolioCashFlowUploadCommand</code> extends the SystemUploadCommand
 * and has some optional <code>PortfolioCashFlowGroup</code> and <code>PortfolioCashFlowType</code> containing specific fields control
 * how the upload will be performed.
 *
 * @author davidi
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class PortfolioCashFlowUploadCommand extends SystemUploadCommand {

	/**
	 * Removes existing data from an active or closed PortfolioCashFlowGroup before adding the new uploaded entries.  If not set,
	 * the new data will be added to the group, leaving the previous data in place. Mutually exclusive with endActiveGroup.
	 */
	private boolean overwriteExistingGroupData;

	private boolean applyUnmappedColumnNamesAsSeparateCashFlowTypes;

	/**
	 * Sets the end date for the currently active PortfolioCashFlowGroup and creates a new group for use with the uploaded entries.
	 * Mutually exclusive with overwriteExistingGroupData.
	 */
	private boolean endActiveGroup;


	/**
	 * This can optionally be set to set the PortfolioCashFlowGroup's client Investment Account for all
	 * values in the file that are not set explicitly.
	 */
	private InvestmentAccount investmentAccount;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public String getTableName() {
		return "PortfolioCashFlow";
	}


	@Override
	@ValueIgnoringGetter
	public String getSystemUploadBeanListConverterBeanName() {
		if (isApplyUnmappedColumnNamesAsSeparateCashFlowTypes()) {
			return "portfolioCashFlowMultipleUploadConverter";
		}
		// Uses the default
		return null;
	}


	// Overrides


	@Override
	@ValueIgnoringGetter
	public boolean isPartialUploadAllowed() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public boolean isCaptureNullUnmappedValues() {
		return true;
	}


	@Override
	@ValueIgnoringGetter
	public boolean isInsertMissingFKBeans() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public FileUploadExistingBeanActions getExistingBeans() {
		return FileUploadExistingBeanActions.INSERT;
	}


	@Override
	@ValueIgnoringGetter
	public boolean isSimple() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isOverwriteExistingGroupData() {
		return this.overwriteExistingGroupData;
	}


	public void setOverwriteExistingGroupData(boolean overwriteExistingGroupData) {
		this.overwriteExistingGroupData = overwriteExistingGroupData;
	}


	public boolean isApplyUnmappedColumnNamesAsSeparateCashFlowTypes() {
		return this.applyUnmappedColumnNamesAsSeparateCashFlowTypes;
	}


	public void setApplyUnmappedColumnNamesAsSeparateCashFlowTypes(boolean applyUnmappedColumnNamesAsSeparateCashFlowTypes) {
		this.applyUnmappedColumnNamesAsSeparateCashFlowTypes = applyUnmappedColumnNamesAsSeparateCashFlowTypes;
	}


	public boolean isEndActiveGroup() {
		return this.endActiveGroup;
	}


	public void setEndActiveGroup(boolean endActiveGroup) {
		this.endActiveGroup = endActiveGroup;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}
}
