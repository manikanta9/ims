package com.clifton.portfolio.cashflow.ldi;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.bean.SystemBean;


/**
 * The type of {@link PortfolioCashFlow} that may exist within a given cash flow stream.
 *
 * @author davidi
 */
@CacheByName
public class PortfolioCashFlowType extends NamedEntity<Short> {

	private boolean cumulative;

	private boolean projectedFlow;

	private boolean actualFlow;

	private boolean serviceCost;

	private SystemBean cashFlowHistoryCalculatorBean;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isCumulative() {
		return this.cumulative;
	}


	public void setCumulative(boolean cumulative) {
		this.cumulative = cumulative;
	}


	public boolean isProjectedFlow() {
		return this.projectedFlow;
	}


	public void setProjectedFlow(boolean projectedFlow) {
		this.projectedFlow = projectedFlow;
	}


	public boolean isActualFlow() {
		return this.actualFlow;
	}


	public void setActualFlow(boolean actualFlow) {
		this.actualFlow = actualFlow;
	}


	public boolean isServiceCost() {
		return this.serviceCost;
	}


	public void setServiceCost(boolean serviceCost) {
		this.serviceCost = serviceCost;
	}


	public SystemBean getCashFlowHistoryCalculatorBean() {
		return this.cashFlowHistoryCalculatorBean;
	}


	public void setCashFlowHistoryCalculatorBean(SystemBean cashFlowHistoryCalculatorBean) {
		this.cashFlowHistoryCalculatorBean = cashFlowHistoryCalculatorBean;
	}
}
