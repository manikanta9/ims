package com.clifton.portfolio.cashflow.ldi.history.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * Search form for {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistorySummary}s
 *
 * @author michaelm
 */
public class PortfolioCashFlowGroupHistorySummarySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "cashFlowGroupHistory.cashFlowGroup.investmentAccount.name,cashFlowGroupHistory.cashFlowGroup.investmentAccount.number", sortField = "id")
	private String searchPattern;

	@SearchField(searchField = "cashFlowGroupHistory.cashFlowGroup.id")
	private Integer cashFlowGroupId;

	@SearchField(searchField = "cashFlowGroupHistory.cashFlowGroup.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] cashFlowGroupIds;

	@SearchField(searchField = "cashFlowGroupHistory.id")
	private Integer cashFlowGroupHistoryId;

	@SearchField(searchField = "cashFlowGroupHistory.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] cashFlowGroupHistoryIds;

	@SearchField(searchFieldPath = "cashFlowGroupHistory", searchField = "startDate")
	private Date startDate;

	@SearchField(searchFieldPath = "cashFlowGroupHistory", searchField = "endDate")
	private Date endDate;

	// Custom Search Filter
	private Date activeOnDate;

	@SearchField(searchField = "investmentAccount.id")
	private Integer investmentAccountId;

	@SearchField(searchField = "investmentAccount.name,investmentAccount.number", sortField = "investmentAccount.number")
	private String accountLabel;

	@SearchField(searchField = "investmentAccount.number")
	private String accountNumber;

	@SearchField(searchField = "cashFlowGroup.effectiveStartDate", searchFieldPath = "cashFlowGroupHistory")
	private Date flowEffectiveStartDate;

	@SearchField(searchField = "cashFlowGroup.effectiveEndDate", searchFieldPath = "cashFlowGroupHistory")
	private Date flowEffectiveEndDate;


	@SearchField(searchField = "marketDataField.fieldOrder")
	private Short fieldOrder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getCashFlowGroupId() {
		return this.cashFlowGroupId;
	}


	public void setCashFlowGroupId(Integer cashFlowGroupId) {
		this.cashFlowGroupId = cashFlowGroupId;
	}


	public Integer[] getCashFlowGroupIds() {
		return this.cashFlowGroupIds;
	}


	public void setCashFlowGroupIds(Integer[] cashFlowGroupIds) {
		this.cashFlowGroupIds = cashFlowGroupIds;
	}


	public Integer getCashFlowGroupHistoryId() {
		return this.cashFlowGroupHistoryId;
	}


	public void setCashFlowGroupHistoryId(Integer cashFlowGroupHistoryId) {
		this.cashFlowGroupHistoryId = cashFlowGroupHistoryId;
	}


	public Integer[] getCashFlowGroupHistoryIds() {
		return this.cashFlowGroupHistoryIds;
	}


	public void setCashFlowGroupHistoryIds(Integer[] cashFlowGroupHistoryIds) {
		this.cashFlowGroupHistoryIds = cashFlowGroupHistoryIds;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public Date getActiveOnDate() {
		return this.activeOnDate;
	}


	public void setActiveOnDate(Date activeOnDate) {
		this.activeOnDate = activeOnDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public String getAccountLabel() {
		return this.accountLabel;
	}


	public void setAccountLabel(String accountLabel) {
		this.accountLabel = accountLabel;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public Date getFlowEffectiveStartDate() {
		return this.flowEffectiveStartDate;
	}


	public void setFlowEffectiveStartDate(Date flowEffectiveStartDate) {
		this.flowEffectiveStartDate = flowEffectiveStartDate;
	}


	public Date getFlowEffectiveEndDate() {
		return this.flowEffectiveEndDate;
	}


	public void setFlowEffectiveEndDate(Date flowEffectiveEndDate) {
		this.flowEffectiveEndDate = flowEffectiveEndDate;
	}


	public Short getFieldOrder() {
		return this.fieldOrder;
	}


	public void setFieldOrder(Short fieldOrder) {
		this.fieldOrder = fieldOrder;
	}
}
