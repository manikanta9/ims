package com.clifton.portfolio.cashflow.ldi.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * The SearchForm to be used to search for PortfolioCashFlowGroup entities via the PortfolioCashFlowService.
 *
 * @author davidi
 */
public class PortfolioCashFlowGroupSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "investmentAccount.id")
	private Integer investmentAccountId;

	@SearchField(searchField = "investmentAccount.name,investmentAccount.number,currencyInvestmentSecurity.symbol", sortField = "id")
	private String searchPattern;

	@SearchField(searchField = "businessClient.id", searchFieldPath = "investmentAccount")
	private Integer clientId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "investmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountGroupId;

	@SearchField(searchField = "number", searchFieldPath = "investmentAccount")
	private String investmentAccountNumber;

	@SearchField(searchField = "number", searchFieldPath = "investmentAccount", comparisonConditions = ComparisonConditions.EQUALS)
	private String investmentAccountNumberEquals;

	@SearchField(searchField = "name", searchFieldPath = "investmentAccount")
	private String investmentAccountName;

	@SearchField(searchField = "currencyInvestmentSecurity.id")
	private Integer currencyInvestmentSecurityId;

	@SearchField(searchField = "symbol", searchFieldPath = "currencyInvestmentSecurity", comparisonConditions = ComparisonConditions.EQUALS)
	private String currencySymbol;

	@SearchField(searchField = "value", searchFieldPath = "cashFlowGroupType", comparisonConditions = ComparisonConditions.EQUALS)
	private String groupType;

	@SearchField
	private Date asOfDate;

	@SearchField
	private Date effectiveStartDate;

	@SearchField
	private Date effectiveEndDate;

	// custom fields for finding active Groups using date range
	private Date activeOnStartDate;
	private Date activeOnEndDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getClientId() {
		return this.clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public String getInvestmentAccountNumber() {
		return this.investmentAccountNumber;
	}


	public void setInvestmentAccountNumber(String investmentAccountNumber) {
		this.investmentAccountNumber = investmentAccountNumber;
	}


	public String getInvestmentAccountNumberEquals() {
		return this.investmentAccountNumberEquals;
	}


	public void setInvestmentAccountNumberEquals(String investmentAccountNumberEquals) {
		this.investmentAccountNumberEquals = investmentAccountNumberEquals;
	}


	public String getInvestmentAccountName() {
		return this.investmentAccountName;
	}


	public void setInvestmentAccountName(String investmentAccountName) {
		this.investmentAccountName = investmentAccountName;
	}


	public Integer getCurrencyInvestmentSecurityId() {
		return this.currencyInvestmentSecurityId;
	}


	public void setCurrencyInvestmentSecurityId(Integer currencyInvestmentSecurityId) {
		this.currencyInvestmentSecurityId = currencyInvestmentSecurityId;
	}


	public String getCurrencySymbol() {
		return this.currencySymbol;
	}


	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}


	public String getGroupType() {
		return this.groupType;
	}


	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}


	public Date getAsOfDate() {
		return this.asOfDate;
	}


	public void setAsOfDate(Date asOfDate) {
		this.asOfDate = asOfDate;
	}


	public Date getEffectiveStartDate() {
		return this.effectiveStartDate;
	}


	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}


	public Date getEffectiveEndDate() {
		return this.effectiveEndDate;
	}


	public void setEffectiveEndDate(Date effectiveEndDate) {
		this.effectiveEndDate = effectiveEndDate;
	}


	public Date getActiveOnStartDate() {
		return this.activeOnStartDate;
	}


	public void setActiveOnStartDate(Date activeOnStartDate) {
		this.activeOnStartDate = activeOnStartDate;
	}


	public Date getActiveOnEndDate() {
		return this.activeOnEndDate;
	}


	public void setActiveOnEndDate(Date activeOnEndDate) {
		this.activeOnEndDate = activeOnEndDate;
	}
}
