package com.clifton.portfolio.cashflow.ldi.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroup;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowService;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowGroupSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * <code>PortfolioCashFlowGroupValidator</code> performs validation on new or existing PortfolioCashFlowGroup entities being saved.
 *
 * @author davidi
 */
@Component
public class PortfolioCashFlowGroupValidator extends SelfRegisteringDaoValidator<PortfolioCashFlowGroup> {

	private PortfolioCashFlowService portfolioCashFlowService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(PortfolioCashFlowGroup group, DaoEventTypes config) {
		// Start date must be before end date
		if (group.getEffectiveEndDate() != null) {
			ValidationUtils.assertTrue(DateUtils.isDateBefore(group.getEffectiveStartDate(), group.getEffectiveEndDate(), false), "The cash flow group's effective start date must be less than the effective end date.");
		}

		validateCashFlowGroupIsUnique(group);

		ValidationUtils.assertNotNull(group.getAsOfDate(), "The as of date must be provided.");
		ValidationUtils.assertTrue(DateUtils.isDateBeforeOrEqual(group.getAsOfDate(), group.getEffectiveStartDate(), false),
				"The as of date must be less than or equal to the effective start date.");
	}


	/**
	 * For the group being saved, verifies that it is unique by investment account ID, CurrencySecurity ID and date range.
	 */
	private void validateCashFlowGroupIsUnique(PortfolioCashFlowGroup group) {
		PortfolioCashFlowGroupSearchForm searchForm = new PortfolioCashFlowGroupSearchForm();
		searchForm.setInvestmentAccountId(group.getInvestmentAccount().getId());
		searchForm.setCurrencyInvestmentSecurityId(group.getCurrencyInvestmentSecurity().getId());
		searchForm.setAsOfDate(group.getAsOfDate());
		searchForm.setGroupType(group.getCashFlowGroupType().getValue());
		searchForm.addSearchRestriction(new SearchRestriction("effectiveEndDate", ComparisonConditions.GREATER_THAN_OR_IS_NULL, group.getEffectiveStartDate()));
		List<PortfolioCashFlowGroup> conflictingGroupEntries = getPortfolioCashFlowService().getPortfolioCashFlowGroupList(searchForm);

		// For existing beans, modification of the existing row is allowed as long as there are no additional conflicts.
		if (!group.isNewBean() && conflictingGroupEntries.size() == 1 && conflictingGroupEntries.get(0).getId().equals(group.getId())) {
			return;
		}

		ValidationUtils.assertTrue(conflictingGroupEntries.isEmpty(), "This cash flow group's date range overlaps the date range of an existing group.");
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowService getPortfolioCashFlowService() {
		return this.portfolioCashFlowService;
	}


	public void setPortfolioCashFlowService(PortfolioCashFlowService portfolioCashFlowService) {
		this.portfolioCashFlowService = portfolioCashFlowService;
	}
}
