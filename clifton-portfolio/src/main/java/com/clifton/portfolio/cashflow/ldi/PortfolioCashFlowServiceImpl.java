package com.clifton.portfolio.cashflow.ldi;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowGroupSearchForm;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowSearchForm;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowTypeSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author davidi
 */
@Service
public class PortfolioCashFlowServiceImpl implements PortfolioCashFlowService {

	private AdvancedUpdatableDAO<PortfolioCashFlowType, Criteria> portfolioCashFlowTypeDAO;
	private AdvancedUpdatableDAO<PortfolioCashFlowGroup, Criteria> portfolioCashFlowGroupDAO;
	private AdvancedUpdatableDAO<PortfolioCashFlow, Criteria> portfolioCashFlowDAO;
	private DaoNamedEntityCache<PortfolioCashFlowType> portfolioCashFlowTypeCache;

	////////////////////////////////////////////////////////////////////////////
	//////        Portfolio Cash FLow Type Business Methods              ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioCashFlowType getPortfolioCashFlowType(short id) {
		return getPortfolioCashFlowTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public PortfolioCashFlowType getPortfolioCashFlowTypeByName(String name) {
		return getPortfolioCashFlowTypeCache().getBeanForKeyValueStrict(getPortfolioCashFlowTypeDAO(), name);
	}


	@Override
	public List<PortfolioCashFlowType> getPortfolioCashFlowTypeList(PortfolioCashFlowTypeSearchForm searchForm) {
		return getPortfolioCashFlowTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public PortfolioCashFlowType savePortfolioCashFlowType(PortfolioCashFlowType portfolioCashFlowType) {
		return getPortfolioCashFlowTypeDAO().save(portfolioCashFlowType);
	}


	@Override
	public void deletePortfolioCashFlowType(short id) {
		getPortfolioCashFlowTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////       Portfolio Cash Flow Group Business Methods              ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioCashFlowGroup getPortfolioCashFlowGroup(int id) {
		PortfolioCashFlowGroup group = getPortfolioCashFlowGroupDAO().findByPrimaryKey(id);
		return group;
	}


	@Override
	public List<PortfolioCashFlowGroup> getPortfolioCashFlowGroupList(PortfolioCashFlowGroupSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getActiveOnStartDate() != null || searchForm.getActiveOnEndDate() != null) {
					criteria.add(ActiveExpressionForDates.forActiveOnDateRangeWithAliasAndProperties(true, null, "effectiveStartDate", "effectiveEndDate", searchForm.getActiveOnStartDate(), searchForm.getActiveOnEndDate()));
				}
			}
		};
		return getPortfolioCashFlowGroupDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public PortfolioCashFlowGroup savePortfolioCashFlowGroup(PortfolioCashFlowGroup portfolioCashFlowGroup) {
		return calculateCashFlowPropertiesAndSave(portfolioCashFlowGroup);
	}


	@Override
	public void deletePortfolioCashFlowGroup(int id) {
		getPortfolioCashFlowGroupDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a list of PortfolioCashFlow entries associated with the specified PortfolioCashFlowGroup.
	 */
	private List<PortfolioCashFlow> getCashFlowListForGroup(PortfolioCashFlowGroup group) {
		List<PortfolioCashFlow> cashFlowList = null;
		if (group != null) {
			PortfolioCashFlowSearchForm searchForm = new PortfolioCashFlowSearchForm();
			searchForm.setCashFlowGroupId(group.getId());
			cashFlowList = getPortfolioCashFlowList(searchForm);
			sortPortfolioCashFlowList(cashFlowList);
		}
		return cashFlowList;
	}


	/**
	 * Populates the PortfolioCashFlowGroup's CashFlowList with the PortfolioCashFlow entries for the group.
	 */
	private PortfolioCashFlowGroup populateGroupCashFlowList(PortfolioCashFlowGroup group) {
		if (group != null) {
			group.setPortfolioCashFlowList(getCashFlowListForGroup(group));
		}
		return group;
	}


	private List<PortfolioCashFlow> sortPortfolioCashFlowList(List<PortfolioCashFlow> cashFlowList) {
		return CollectionUtils.sort(cashFlowList, (entryA, entryB) -> MathUtils.compare(entryA.getCashFlowYear(), entryB.getCashFlowYear()));
	}


	////////////////////////////////////////////////////////////////////////////
	/////////        Portfolio Cash Flow Business Methods              /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioCashFlow getPortfolioCashFlow(int id) {
		return getPortfolioCashFlowDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortfolioCashFlow> getPortfolioCashFlowList(PortfolioCashFlowSearchForm searchForm) {
		return getPortfolioCashFlowDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Transactional
	@Override
	public PortfolioCashFlow savePortfolioCashFlow(PortfolioCashFlow portfolioCashFlow) {
		return doSavePortfolioCashFlow(portfolioCashFlow);
	}


	@Transactional
	@Override
	public void deletePortfolioCashFlow(int id) {
		PortfolioCashFlow cashFlow = getPortfolioCashFlow(id);
		AssertUtils.assertNotNull(cashFlow, "Cannot find bean to delete with primary key: " + id);
		doDeletePortfolioCashFlow(cashFlow);
	}


	@Transactional
	@Override
	public void deletePortfolioCashFlowsInCashFlowGroup(PortfolioCashFlowGroup portfolioCashFlowGroup) {
		if (portfolioCashFlowGroup != null) {
			this.portfolioCashFlowDAO.deleteList(getCashFlowListForGroup(portfolioCashFlowGroup));
		}
	}

	////////////////////////////////////////////////////////////////////////////


	/**
	 * Calculates and updates PortfolioCashFlowGroup's PortfolioCashFlow entries' StartingMonthNumber / EndingMonthNumber values for the group's PortfolioCashFlow list.
	 * <p>
	 * Note:  the group is expected to have a populated portfolioCashFlow list.
	 */
	@Transactional
	protected PortfolioCashFlowGroup calculateCashFlowPropertiesAndSave(PortfolioCashFlowGroup portfolioCashFlowGroup) {
		PortfolioCashFlowGroup savedGroup = this.portfolioCashFlowGroupDAO.save(portfolioCashFlowGroup);
		if (!CollectionUtils.isEmpty(portfolioCashFlowGroup.getPortfolioCashFlowList())) {
			savedGroup.setPortfolioCashFlowList(portfolioCashFlowGroup.getPortfolioCashFlowList());
			List<PortfolioCashFlow> portfolioCashFlowList = calculateCashFlowPropertiesAndSave(savedGroup, null, false);
			savedGroup.setPortfolioCashFlowList(portfolioCashFlowList);
		}
		return savedGroup;
	}


	/**
	 * Saves a PortfolioCashFlow entity after updating its calculated properties. If required, it
	 * will also recalculate properties on other entities within its PortfolioCashFlowGroup.
	 */
	@Transactional
	protected PortfolioCashFlow doSavePortfolioCashFlow(PortfolioCashFlow portfolioCashFlow) {
		if (portfolioCashFlow.getCashFlowGroup() != null) {
			calculateCashFlowPropertiesAndSave(portfolioCashFlow.getCashFlowGroup(), portfolioCashFlow, true);
		}
		return portfolioCashFlow;
	}


	/**
	 * Deletes a PortfolioCashFlow entity after updating its calculated properties. If required, it
	 * will also recalculate properties on other entities within its PortfolioCashFlowGroup.
	 */
	@Transactional
	protected void doDeletePortfolioCashFlow(PortfolioCashFlow portfolioCashFlow) {
		calculateCashFlowPropertiesAndSave(portfolioCashFlow.getCashFlowGroup(), portfolioCashFlow, false);
	}


	/**
	 * Calculates and updates PortfolioCashFlowGroup's PortfolioCashFlow entries' StartingMonthNumber / EndingMonthNumber values for the PortfolioCashFlow entries in the group.
	 *
	 * @param portfolioCashFlowGroup     a PortfolioCashFlowGroup with a populated PortfolioCashFlowGroupList
	 * @param portfolioCashFlowReference the entity to update or delete (for insert / update / delete of a single PortfolioCashFlow entity)
	 * @param insertOrUpdate             a flag which determines if this is an insert/update or a delete operation on the PortfolioCashFlow entity.
	 *                                   <p>
	 *                                   <p>
	 *                                   Note:  if the portfolioCashFlowReference is null, the value of the insertOrUpdate parameter is not used.
	 */
	@Transactional
	protected List<PortfolioCashFlow> calculateCashFlowPropertiesAndSave(PortfolioCashFlowGroup portfolioCashFlowGroup, PortfolioCashFlow portfolioCashFlowReference, boolean insertOrUpdate) {
		if (portfolioCashFlowGroup == null) {
			return null;
		}
		boolean requiresFullListRecalculation = false;

		// if the reference entity has been updated and replaced in the list, store its CashFlowType for use in later processing.
		PortfolioCashFlowType previousCashFlowType = null;

		if (!CollectionUtils.isEmpty(portfolioCashFlowGroup.getPortfolioCashFlowList())) {
			// A list is passed in for update / save processing (e.g. from Uploader).  We will need to calculate field properties for all entities.
			requiresFullListRecalculation = true;
		}
		else {
			populateGroupCashFlowList(portfolioCashFlowGroup);
		}

		List<PortfolioCashFlow> workingList = new ArrayList<>(portfolioCashFlowGroup.getPortfolioCashFlowList());

		// Create a map of sub-lists that are portions of the full list grouped by PortfolioCashFlowType
		Map<PortfolioCashFlowType, List<PortfolioCashFlow>> portfolioCashFlowTypeListMap = BeanUtils.getBeansMap(workingList, PortfolioCashFlow::getCashFlowType);


		if (portfolioCashFlowReference != null) {
			// cashFlowList is a reference to the sublist of the workingList. There is one sublist for each PortfolioCashFlowType. Its used when
			// processing the entire group list.
			List<PortfolioCashFlow> cashFlowList = portfolioCashFlowTypeListMap.get(portfolioCashFlowReference.getCashFlowType());
			if (cashFlowList != null) {
				sortPortfolioCashFlowList(cashFlowList);
			}
			else {
				cashFlowList = new ArrayList<>();
				portfolioCashFlowTypeListMap.put(portfolioCashFlowReference.getCashFlowType(), cashFlowList);
			}

			PortfolioCashFlow minimumYearEntry = CollectionUtils.isEmpty(cashFlowList) ? null : cashFlowList.get(0);
			Short minimumCashFlowYear = minimumYearEntry == null ? null : minimumYearEntry.getCashFlowYear();
			ValidationUtils.assertFalse(insertOrUpdate && isCashFlowYearConflict(portfolioCashFlowReference, cashFlowList), "Cannot add portfolio cash flow entry with cashFlowYear: " + portfolioCashFlowReference.getCashFlowYear() + " because there is another entry in the group with the same cash flow year.");

			// checks for save of an entity with a lower portfolioCashFlowYear.
			if (insertOrUpdate && minimumCashFlowYear != null && portfolioCashFlowReference.getCashFlowYear() < minimumCashFlowYear) {
				requiresFullListRecalculation = true;
			}

			// checks for deletion of PortfolioCashFlow object and removes the entity from the sublist.
			if (!insertOrUpdate && portfolioCashFlowReference.getCashFlowYear().equals(minimumCashFlowYear)) {
				requiresFullListRecalculation = true;
			}

			if (insertOrUpdate) {
				// Handle update of an existing entity which may require full list processing
				if (!portfolioCashFlowReference.isNewBean()) {
					PortfolioCashFlow previousInstance = getItemById(portfolioCashFlowReference.getId(), workingList);
					List<PortfolioCashFlow> previousInstanceCashFlowSubList = portfolioCashFlowTypeListMap.get(previousInstance.getCashFlowType());

					// if the CashFlowType has changed, 2 sublists will be affected - check the change causes a removal of the lowest year entry in a sublist.
					if (!previousInstance.getCashFlowType().equals(portfolioCashFlowReference.getCashFlowType())) {
						previousCashFlowType = previousInstance.getCashFlowType();
					}

					// if the entity's cashFlowYear or cashFlowType has changed, check to see if its was the entity with the minimum CashFlow year within its sublist
					if (!previousInstance.getCashFlowYear().equals(portfolioCashFlowReference.getCashFlowYear()) || !previousInstance.getCashFlowType().equals(portfolioCashFlowReference.getCashFlowType())) {
						PortfolioCashFlow prevInstanceMinCashFlowYearEntity = getMinimumCashFlowYearEntity(previousInstanceCashFlowSubList);
						if (previousInstance.equals(prevInstanceMinCashFlowYearEntity)) {
							requiresFullListRecalculation = true;
						}
					}

					//remove the previous instance so it does not get processed.
					previousInstanceCashFlowSubList.remove(previousInstance);
				}

				cashFlowList.add(portfolioCashFlowReference);
			}
			else {
				// handle delete of entity
				cashFlowList.remove(portfolioCashFlowReference);
				this.portfolioCashFlowDAO.delete(portfolioCashFlowReference);
			}

			// Perform a save / delete without recalculation of property values for other group PortfolioCashFlow entities.
			// In this instance return the workingList after adding (for save) or removing (for delete) the entity.
			if (!requiresFullListRecalculation) {
				if (insertOrUpdate) {
					workingList.remove(portfolioCashFlowReference);
					updateStartingMonthNumber(portfolioCashFlowReference, minimumCashFlowYear);
					workingList.add(this.portfolioCashFlowDAO.save(portfolioCashFlowReference));
				}
				return workingList;
			}
		}

		// full list recalculation
		List<PortfolioCashFlow> savedEntityList = new ArrayList<>();
		for (Map.Entry<PortfolioCashFlowType, List<PortfolioCashFlow>> portfolioCashFlowEntry : CollectionUtils.getIterable(portfolioCashFlowTypeListMap.entrySet())) {
			// if we are just saving a single entity, skip processing unless the portfolioCashFlowType matches the cashflowType of the portfolioCashFlowReference or its previous type
			if (portfolioCashFlowReference != null &&
					(!portfolioCashFlowEntry.getKey().equals(portfolioCashFlowReference.getCashFlowType())) &&
					(!portfolioCashFlowEntry.getKey().equals(previousCashFlowType))) {
				continue;
			}

			List<PortfolioCashFlow> portfolioCashFlowByTypeSubList = portfolioCashFlowEntry.getValue();

			if (!CollectionUtils.isEmpty(portfolioCashFlowByTypeSubList)) {
				sortPortfolioCashFlowList(portfolioCashFlowByTypeSubList);
				Short minimumCashFlowYear = portfolioCashFlowByTypeSubList.get(0).getCashFlowYear();
				short previousCashFlowYear = 0;
				for (PortfolioCashFlow portfolioCashFlow : portfolioCashFlowByTypeSubList) {
					ValidationUtils.assertTrue(MathUtils.compare(previousCashFlowYear, portfolioCashFlow.getCashFlowYear()) != 0, "Duplicate cash flow year values: " + previousCashFlowYear + " found for cash flow entries in group: " + portfolioCashFlow.getCashFlowGroup().getLabel());
					boolean copyToReference = portfolioCashFlow.equals(portfolioCashFlowReference);
					updateStartingMonthNumber(portfolioCashFlow, minimumCashFlowYear);
					PortfolioCashFlow savedEntity = this.portfolioCashFlowDAO.save(portfolioCashFlow);

					// provide the saved version to the method caller via the reference.
					if (copyToReference) {
						portfolioCashFlowReference = savedEntity;
					}
					previousCashFlowYear = portfolioCashFlow.getCashFlowYear();
					savedEntityList.add(savedEntity);
				}
			}
		}

		// if only a subset of entities were processed (by PortfolioCashFlowType), merge the unchanged entities into the return list.
		if (portfolioCashFlowReference != null) {
			final Set<PortfolioCashFlowType> omitSet = CollectionUtils.createHashSet(portfolioCashFlowReference.getCashFlowType(), previousCashFlowType);
			List<PortfolioCashFlow> filteredList = CollectionUtils.getFiltered(workingList, entity -> !omitSet.contains(entity.getCashFlowType()));
			savedEntityList.addAll(filteredList);
		}

		return savedEntityList;
	}


	/**
	 * Calculates and updates a PortfolioCashFlow entity's StartingMonthNumber property value.
	 */
	private void updateStartingMonthNumber(PortfolioCashFlow portfolioCashFlow, Short minimumCashFlowYear) {
		if (portfolioCashFlow != null) {
			if (minimumCashFlowYear == null) {
				portfolioCashFlow.setStartingMonthNumber((short) 1);
			}
			else {
				portfolioCashFlow.setStartingMonthNumber((short) ((portfolioCashFlow.getCashFlowYear() - minimumCashFlowYear) * 12 + 1));
			}
		}
	}


	private PortfolioCashFlow getItemById(Integer id, List<PortfolioCashFlow> cashFlowList) {
		List<PortfolioCashFlow> filteredList = CollectionUtils.getFiltered(cashFlowList, entry -> entry.getId().equals(id));
		return CollectionUtils.getFirstElement(filteredList);
	}


	/**
	 * Checks the PortfolioCashFlow entry against other entries in the list to determine if one or more entities exist with the same CashFlowYear entry.
	 *
	 * @retrns true if a conflict exists, false if no conflict exists
	 */
	private boolean isCashFlowYearConflict(PortfolioCashFlow cashFlow, List<PortfolioCashFlow> cashFlowList) {
		if (cashFlow == null || CollectionUtils.isEmpty(cashFlowList)) {
			return false;
		}
		List<PortfolioCashFlow> filteredList = CollectionUtils.getFiltered(cashFlowList, entry -> entry.getCashFlowYear().equals(cashFlow.getCashFlowYear()) && entry.getCashFlowType().equals(cashFlow.getCashFlowType()));
		filteredList.remove(cashFlow);
		return !CollectionUtils.isEmpty(filteredList);
	}


	/**
	 * Looks up and returns the PortfolioCashFlow entry with the minimum cashFlowYear value.
	 */
	private PortfolioCashFlow getMinimumCashFlowYearEntity(List<PortfolioCashFlow> cashFlowList) {
		if (!CollectionUtils.isEmpty(cashFlowList)) {
			sortPortfolioCashFlowList(cashFlowList);
			return cashFlowList.get(0);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortfolioCashFlowType, Criteria> getPortfolioCashFlowTypeDAO() {
		return this.portfolioCashFlowTypeDAO;
	}


	public void setPortfolioCashFlowTypeDAO(AdvancedUpdatableDAO<PortfolioCashFlowType, Criteria> portfolioCashFlowTypeDAO) {
		this.portfolioCashFlowTypeDAO = portfolioCashFlowTypeDAO;
	}


	public AdvancedUpdatableDAO<PortfolioCashFlowGroup, Criteria> getPortfolioCashFlowGroupDAO() {
		return this.portfolioCashFlowGroupDAO;
	}


	public void setPortfolioCashFlowGroupDAO(AdvancedUpdatableDAO<PortfolioCashFlowGroup, Criteria> portfolioCashFlowGroupDAO) {
		this.portfolioCashFlowGroupDAO = portfolioCashFlowGroupDAO;
	}


	public AdvancedUpdatableDAO<PortfolioCashFlow, Criteria> getPortfolioCashFlowDAO() {
		return this.portfolioCashFlowDAO;
	}


	public void setPortfolioCashFlowDAO(AdvancedUpdatableDAO<PortfolioCashFlow, Criteria> portfolioCashFlowDAO) {
		this.portfolioCashFlowDAO = portfolioCashFlowDAO;
	}


	public DaoNamedEntityCache<PortfolioCashFlowType> getPortfolioCashFlowTypeCache() {
		return this.portfolioCashFlowTypeCache;
	}


	public void setPortfolioCashFlowTypeCache(DaoNamedEntityCache<PortfolioCashFlowType> portfolioCashFlowTypeCache) {
		this.portfolioCashFlowTypeCache = portfolioCashFlowTypeCache;
	}
}
