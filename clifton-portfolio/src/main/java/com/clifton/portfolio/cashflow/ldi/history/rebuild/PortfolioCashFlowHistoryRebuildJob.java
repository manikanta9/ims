package com.clifton.portfolio.cashflow.ldi.history.rebuild;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;

import java.util.Date;
import java.util.Map;


/**
 * Job for initiating Portfolio Cash Flow History rebuilds.
 *
 * @author michaelm
 */
public class PortfolioCashFlowHistoryRebuildJob implements Task, StatusHolderObjectAware<Status> {

	private PortfolioCashFlowHistoryRebuildService portfolioCashFlowHistoryRebuildService;


	private StatusHolderObject<Status> statusHolderObject;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The job will either be set up for:
	 * false: Run EOD to run for balance date = today
	 * true: Run next weekday (morning) to run for balance date = previous week day
	 */
	private boolean previousWeekday;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolderObject.getStatus();

		Date balanceDate = DateUtils.clearTime(new Date());
		if (isPreviousWeekday()) {
			balanceDate = DateUtils.addWeekDays(balanceDate, -1);
		}


		// Common properties
		PortfolioCashFlowHistoryRebuildCommand command = new PortfolioCashFlowHistoryRebuildCommand();
		command.setSynchronous(true);
		command.setStatus(status);
		command.setStartBalanceDate(balanceDate);
		command.setEndBalanceDate(balanceDate);
		command.setStatus(status);

		return getPortfolioCashFlowHistoryRebuildService().rebuildPortfolioCashFlowHistory(command);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowHistoryRebuildService getPortfolioCashFlowHistoryRebuildService() {
		return this.portfolioCashFlowHistoryRebuildService;
	}


	public void setPortfolioCashFlowHistoryRebuildService(PortfolioCashFlowHistoryRebuildService portfolioCashFlowHistoryRebuildService) {
		this.portfolioCashFlowHistoryRebuildService = portfolioCashFlowHistoryRebuildService;
	}


	public StatusHolderObject<Status> getStatusHolderObject() {
		return this.statusHolderObject;
	}


	public boolean isPreviousWeekday() {
		return this.previousWeekday;
	}


	public void setPreviousWeekday(boolean previousWeekday) {
		this.previousWeekday = previousWeekday;
	}
}
