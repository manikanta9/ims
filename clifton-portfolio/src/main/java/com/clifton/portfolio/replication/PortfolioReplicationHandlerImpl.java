package com.clifton.portfolio.replication;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.calculators.BaseInvestmentReplicationSecurityAllocation;
import com.clifton.investment.replication.calculators.InvestmentReplicationCalculator;
import com.clifton.investment.replication.calculators.InvestmentReplicationCurrencyTypes;
import com.clifton.investment.replication.calculators.InvestmentReplicationSecurityAllocation;
import com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory;
import com.clifton.investment.replication.history.InvestmentReplicationHistory;
import com.clifton.investment.replication.history.rebuild.InvestmentReplicationHistoryRebuildService;
import com.clifton.investment.setup.group.InvestmentSecurityGroupSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.account.PortfolioAccountContractStoreTypes;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioReplicationCurrencyOtherConfig;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.trade.PortfolioCurrencyTrade;
import com.clifton.portfolio.run.util.PortfolioUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author manderson
 */
@Component
public class PortfolioReplicationHandlerImpl implements PortfolioReplicationHandler {

	private InvestmentCalculator investmentCalculator;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentReplicationService investmentReplicationService;
	private InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends InvestmentReplicationSecurityAllocation> void setupInvestmentReplicationSecurityAllocation(T replication, InvestmentSecurity security, String exchangeRateDataSourceName, boolean excludeMispricing) {
		// Set the Security
		replication.setSecurity(security);

		// Depending on Replication Type, calculate/set duration, synthetic adj, and determine all data is present to calculate contract value for the security
		// Also calculates contract value
		InvestmentReplicationCalculator calculator = getReplicationCalculator(replication);
		calculator.calculate(replication, exchangeRateDataSourceName, excludeMispricing);
	}


	@Override
	public <T extends InvestmentReplicationSecurityAllocation> void recalculateInvestmentReplicationSecurityAllocation(T replication) {
		InvestmentReplicationCalculator calculator = getReplicationCalculator(replication);
		calculator.calculateForTargetUpdates(replication);
	}


	@Override
	public <T extends InvestmentReplicationSecurityAllocation> void refreshInvestmentReplicationSecurityAllocationLivePrices(T replication, InvestmentReplicationCurrencyTypes currencyType, boolean setLivePrices, boolean excludeMispricing) {
		// Also calculates "trade" contract value
		InvestmentReplicationCalculator calculator = getReplicationCalculator(replication);
		calculator.calculateForTrading(replication, currencyType, setLivePrices, excludeMispricing);
	}


	@Override
	public <T extends InvestmentReplicationSecurityAllocation, V> boolean validateInvestmentReplicationSecurityAllocationSecurityChange(T replication, InvestmentSecurity security, Function<T, V> replicationFilterFunction, List<T> fullRunRepList, boolean throwExceptionIfNotValid) {
		// First Validate we don't already have a row for that security
		Stream<T> filteredReplicationListStream = CollectionUtils.getStream(fullRunRepList)
				.filter(r -> CompareUtils.isEqual(r.getSecurity(), security))
				.filter(r -> CompareUtils.isEqual(r.getReplication(), replication.getReplication()));
		ObjectWrapper<V> replicationFilterValue = new ObjectWrapper<>();
		if (replicationFilterFunction != null) {
			replicationFilterValue.setObject(replicationFilterFunction.apply(replication));
			filteredReplicationListStream = filteredReplicationListStream.filter(r -> CompareUtils.isEqual(replicationFilterFunction.apply(r), replicationFilterValue));
		}
		List<T> filteredList = filteredReplicationListStream.collect(Collectors.toList());

		try {
			ValidationUtils.assertEmpty(filteredList, () -> {
				StringBuilder message = new StringBuilder("The security selected already exists for security [")
						.append(replication.getSecurity().getLabel())
						.append("] and replication [")
						.append(replication.getReplication().getName())
						.append(']');
				if (replicationFilterFunction == null) {
					return message.toString();
				}
				V filterValue = replicationFilterValue.getObject();
				Class<?> filterEntityClass = ClassUtils.getClassForObject(filterValue);
				return message.append(" and ")
						.append(filterEntityClass == null ? "entity" : filterEntityClass.getSimpleName())
						.append(" [").append((filterValue instanceof LabeledObject) ? ((LabeledObject) filterValue).getLabel() : filterValue)
						.append(']').toString();
			});

			InvestmentReplicationAllocation replicationAllocation = replication.getReplicationAllocation();
			if (replicationAllocation != null) {
				if (replicationAllocation.getReplicationInstrument() != null) {
					ValidationUtils.assertTrue(replicationAllocation.getReplicationInstrument().equals(security.getInstrument()), "The security selected must belong to Instrument [" + replicationAllocation.getReplicationInstrument().getLabel() + "].");
				}
				if (replicationAllocation.getReplicationSecurityGroup() != null) {
					// For cases where the security was just added/updated, the security group is system managed
					// and it doesn't exist yet (creating securities is on a 30 second delay to rebuild the group, but we need it instantly)
					// try to rebuild the group manually, and then check it's existence again.
					InvestmentSecurityGroupSecurity gs = getInvestmentSecurityGroupService().getInvestmentSecurityGroupSecurityByGroupAndSecurity(replicationAllocation.getReplicationSecurityGroup().getId(), security.getId());
					if (gs == null && replicationAllocation.getReplicationSecurityGroup().isSystemManaged()) {
						getInvestmentSecurityGroupService().rebuildInvestmentSecurityGroup(replicationAllocation.getReplicationSecurityGroup().getId());
						gs = getInvestmentSecurityGroupService().getInvestmentSecurityGroupSecurityByGroupAndSecurity(replicationAllocation.getReplicationSecurityGroup().getId(), security.getId());
					}
					ValidationUtils.assertNotNull(gs, "The security selected must belong to Security group [" + replicationAllocation.getReplicationSecurityGroup().getName() + "].");
				}
			}
		}
		catch (ValidationException e) {
			if (throwExceptionIfNotValid) {
				throw e;
			}
			return false;
		}
		return true;
	}


	@Override
	public <T extends InvestmentReplicationSecurityAllocation> void recalculateContractValue(T replication, boolean doNotUseExposureMultiplier) {
		InvestmentReplicationCalculator calculator = getReplicationCalculator(replication);
		calculator.recalculateContractValue(replication, doNotUseExposureMultiplier);
	}


	@Override
	public <T extends InvestmentReplicationSecurityAllocation> boolean isReplicationCalculatorDurationRequired(T replication) {
		InvestmentReplicationCalculator calculator = getReplicationCalculator(replication);
		// Pass Null Here because we only want to know if the calculator requires it - not necessarily if the replication security itself is adjusted
		return calculator.isDurationRequired(null);
	}


	@Override
	public List<PortfolioReplicationAllocationSecurityConfig> getPortfolioReplicationAllocationSecurityConfigListForBalanceDate(int replicationId, Date balanceDate, List<InvestmentSecurity> securityList) {
		InvestmentReplicationHistory replicationHistory = getInvestmentReplicationHistoryRebuildService().getOrBuildInvestmentReplicationHistory(replicationId, balanceDate);
		ValidationUtils.assertNotNull(replicationHistory, "Unable to determine replication history for replication " + getInvestmentReplicationService().getInvestmentReplication(replicationId).getName() + " for balance date " + DateUtils.fromDateShort(balanceDate));

		List<PortfolioReplicationAllocationSecurityConfig> portfolioReplicationAllocationSecurityConfigList = new ArrayList<>();

		for (InvestmentReplicationAllocationHistory allocationHistory : CollectionUtils.getIterable(replicationHistory.getReplicationAllocationHistoryList())) {
			PortfolioReplicationAllocationSecurityConfig portfolioReplicationAllocationSecurityConfig = new PortfolioReplicationAllocationSecurityConfig(allocationHistory);
			portfolioReplicationAllocationSecurityConfig.setSecurityList(getInvestmentReplicationService().getInvestmentReplicationAllocationSecurityList(allocationHistory.getReplicationAllocation(), balanceDate, securityList));

			if (allocationHistory.getReplicationAllocation().isPortfolioSelectedCurrentSecurity() && !CollectionUtils.isEmpty(portfolioReplicationAllocationSecurityConfig.getSecurityList())) {
				portfolioReplicationAllocationSecurityConfig.setPortfolioCurrentSecurity(CollectionUtils.getFirstElement(BeanUtils.sortWithFunctions(portfolioReplicationAllocationSecurityConfig.getSecurityList(), CollectionUtils.createList(InvestmentSecurity::getEndDate, InvestmentSecurity::getStartDate, InvestmentSecurity::getId), CollectionUtils.createList(false, false, false))));
			}
			portfolioReplicationAllocationSecurityConfigList.add(portfolioReplicationAllocationSecurityConfig);
		}
		return portfolioReplicationAllocationSecurityConfigList;
	}


	@Override
	public <T extends BasePortfolioRunReplication> void processPortfolioReplicationListForCurrencyContracts(PortfolioAccountContractStore contracts,
	                                                                                                        BiFunction<PortfolioAccountContractStore, T, List<AccountingPosition>> contractPositionFunction,
	                                                                                                        PortfolioRun run,
	                                                                                                        List<T> currencyReplicationList,
	                                                                                                        List<PortfolioCurrencyOther> currencyOtherList,
	                                                                                                        List<PortfolioCurrencyTrade<T>> currencyTradeList,
	                                                                                                        List<T> fullReplicationList,
	                                                                                                        Function<T, BaseSimpleEntity<Integer>> replicationMatchingFilterFunction) {
		// If there are NO Currency Replications - IGNORE THIS Only for Actual Contracts
		// For Trade Creation screen we still want to always show physical currency balances (PORTFOLIO-20) AND including unrealized CCY (PORTFOLIO-27)
		boolean tradingCCYOnly = false;
		if (CollectionUtils.isEmpty(currencyReplicationList)) {
			if (contracts.getType().isUseTradingValue()) {
				tradingCCYOnly = true;
			}
			else {
				return;
			}
		}

		List<T> unrealizedReplicationList = new ArrayList<>();
		// Track replications added so we don't double count anything
		Set<Integer> unrealizedAssetClassIdList = new HashSet<>();

		boolean applyFromMatching = false;
		// Seems to best mimic Java 7 allocation to Java 8 when applying to "first one found"
		currencyReplicationList = BeanUtils.sortWithFunction(currencyReplicationList, BaseInvestmentReplicationSecurityAllocation::getOrder, true);

		for (T r : CollectionUtils.getIterable(currencyReplicationList)) {
			if (r.isMatchingReplication()) {
				BaseSimpleEntity<Integer> filterEntity = replicationMatchingFilterFunction.apply(r);
				if (filterEntity != null && !unrealizedAssetClassIdList.contains(filterEntity.getId())) {
					applyFromMatching = true;
					unrealizedAssetClassIdList.add(filterEntity.getId());
					unrealizedReplicationList.addAll(BeanUtils.filter(fullReplicationList, rep -> {
						BaseSimpleEntity<Integer> matchingFilterEntity = replicationMatchingFilterFunction.apply(rep);
						return CompareUtils.isEqual(matchingFilterEntity == null ? null : matchingFilterEntity.getId(), filterEntity.getId());
					}));
				}
			}
		}

		Map<InvestmentSecurity, BigDecimal> currencyPositionQuantityMap = getProductInvestmentAccountCurrencyContractMap(contracts);

		for (Map.Entry<InvestmentSecurity, BigDecimal> currencyQuantityEntry : CollectionUtils.getIterable(currencyPositionQuantityMap.entrySet())) {
			InvestmentSecurity currency = currencyQuantityEntry.getKey();
			// Get Positions/Trades for all Securities that use this currency as it's trading currency AND this currency is not the client account's base currency
			BigDecimal unrealized = BigDecimal.ZERO;
			BigDecimal currentUnrealized = BigDecimal.ZERO; // When using trading value, without CCY Replication List ( i.e. tradingCCYOnly = true), we add balances to the Trade Creation Screen
			// Need to back track differences to previous close

			// Include CCY Unrealized as long as not the same as the Account Base CCY
			if (!currency.equals(run.getClientInvestmentAccount().getBaseCurrency())) {
				List<T> repList;
				if (applyFromMatching) {
					repList = unrealizedReplicationList;
				}
				else if (tradingCCYOnly) {
					// If trade creation and no CCY reps - use all reps to show CCY balance
					repList = fullReplicationList;
				}
				else {
					repList = currencyReplicationList;
				}

				// Track the list of positions we've included unrealized for, so we don't include them again
				// Prevents Doubling when same security currency is included in Primary and Secondary Replication
				Set<Long> positionIdsChecked = new HashSet<>();
				for (T replication : CollectionUtils.getIterable(repList)) {
					if (currency.equals(replication.getSecurity().getInstrument().getTradingCurrency())) {
						// TODO WILL NEED FOR PENDING TRADES ONLY IF WE DECIDE TO USE THIS FOR THAT
						// SKIPPING FOR NOW tradeList.addAll(contracts.getSecurityTradeListForAccountAssetClass(rep.getOverlayAssetClass().getAccountAssetClass(), rep.getSecurity().getId()));
						// Should really apply only to Futures Only - i.e. Hierarchy flagged as No Payment on Open
						List<AccountingPosition> positionList = contractPositionFunction.apply(contracts, replication);
						for (AccountingPosition position : CollectionUtils.getIterable(positionList)) {
							if (positionIdsChecked.add(position.getId())) {
								// No Payment on Open flag.
								// Security Field isn't populated
								InvestmentSecurity positionSecurity = getInvestmentInstrumentService().getInvestmentSecurity(position.getInvestmentSecurity().getId());
								if (InvestmentUtils.isNoPaymentOnOpen(positionSecurity)) {
									BigDecimal notional = getInvestmentCalculator().calculateNotional(positionSecurity, replication.getSecurityPrice(), position.getRemainingQuantity(), run.getBalanceDate());
									if (tradingCCYOnly && DateUtils.compare(position.getTransactionDate(), run.getBalanceDate(), false) > 0) {
										currentUnrealized = MathUtils.add(currentUnrealized, MathUtils.subtract(notional, position.getRemainingCostBasis()));
									}
									else {
										unrealized = MathUtils.add(unrealized, MathUtils.subtract(notional, position.getRemainingCostBasis()));
									}
								}
							}
						}
					}
				}
			}
			// No actual, no unrealized - skip this currency
			BigDecimal value = currencyQuantityEntry.getValue();
			if (MathUtils.isNullOrZero(unrealized) && MathUtils.isNullOrZero(value) && MathUtils.isNullOrZero(currentUnrealized)) {
				continue;
			}

			PortfolioCurrencyTrade<T> currencyTrade = CollectionUtils.getFirstElement(BeanUtils.filter(currencyTradeList, curTrade -> curTrade.getCurrencySecurity().getId().equals(currency.getId())));
			if (currencyTrade == null) {
				currencyTrade = new PortfolioCurrencyTrade<>();
				currencyTradeList.add(currencyTrade);
			}

			// See if we have a replication where Underlying = CCY (NOTE: Finds/Applies to First one found that has a target or actual)
			T curRep = getFirstCurrencyReplicationForCurrency(currencyReplicationList, currency);
			if (curRep != null) {
				if (PortfolioAccountContractStoreTypes.ACTUAL == contracts.getType()) {
					curRep.setCurrencyActualLocalAmount(value);
					curRep.setCurrencyUnrealizedLocalAmount(unrealized);
				}
				else if (PortfolioAccountContractStoreTypes.PENDING == contracts.getType()) {
					curRep.setCurrencyPendingLocalAmount(value);
				}
				else {
					curRep.setCurrencyCurrentLocalAmount(value);
					curRep.setCurrencyCurrentUnrealizedLocalAmount(unrealized);
				}

				if (curRep.getCurrencyExchangeRate() == null) {
					curRep.setCurrencyExchangeRate(getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(contracts.getExchangeRateDataSourceName(),
							currency.getSymbol(), run.getClientInvestmentAccount().getBaseCurrency().getSymbol(), run.getBalanceDate())));
				}

				currencyTrade.setReplication(curRep);
			}
			else {
				// Look in the PortfolioCurrencyOther list to see if there is already an entry for it
				PortfolioCurrencyOther currencyOther = CollectionUtils.getFirstElement(BeanUtils.filter(currencyOtherList, curOther -> curOther.getCurrencySecurity().getId().equals(currency.getId())));
				if (currencyOther == null) {
					currencyOther = new PortfolioCurrencyOther();
					currencyOther.setPortfolioRun(run);
					currencyOther.setCurrencySecurity(currency);
					currencyOtherList.add(currencyOther);
				}

				if (currencyOther.getExchangeRate() == null) {
					currencyOther.setExchangeRate(getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(contracts.getExchangeRateDataSourceName(),
							currencyOther.getCurrencySecurity().getSymbol(), run.getClientInvestmentAccount().getBaseCurrency().getSymbol(), run.getBalanceDate())));
				}

				if (PortfolioAccountContractStoreTypes.ACTUAL == contracts.getType()) {
					currencyOther.setLocalAmount(value);
					// Convert Base Notional to Local and Subtract Remaining cost basis for local unrealized currency exposure
					currencyOther.setUnrealizedLocalAmount(unrealized);
				}
				else if (PortfolioAccountContractStoreTypes.PENDING == contracts.getType()) {
					currencyOther.setPendingLocalAmount(value);
				}
				else {
					currencyOther.setCurrentLocalAmount(value);
					if (tradingCCYOnly) {
						// Set previous local amount as well
						currencyOther.setLocalAmount(value);
						currencyOther.setUnrealizedLocalAmount(unrealized);
						currencyOther.setCurrentUnrealizedLocalAmount(MathUtils.add(unrealized, currentUnrealized));
					}
					else {
						currencyOther.setCurrentUnrealizedLocalAmount(unrealized);
					}
				}
				BeanUtils.copyProperties(currencyOther, currencyTrade);
			}
		}
		// Apply CCY Other Amounts to CCY Replications Appropriately
		PortfolioReplicationCurrencyOtherConfig<T> ccyOtherConfig = new PortfolioReplicationCurrencyOtherConfig<>(contracts.getType(), run.getClientInvestmentAccount().getBaseCurrency(),
				currencyReplicationList);
		ccyOtherConfig.applyCurrencyOtherToReplications(currencyOtherList);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	protected <T extends InvestmentReplicationSecurityAllocation> InvestmentReplicationCalculator getReplicationCalculator(T replication) {
		// Returns Override if set from allocation, else replication's type
		SystemBean calculator = replication.getReplicationType().getCalculatorBean();
		if (calculator == null) {
			throw new ValidationException("There is no calculator selected to process replication: " + replication.getLabel());
		}
		return (InvestmentReplicationCalculator) getSystemBeanService().getBeanInstance(calculator);
	}


	private <T extends BasePortfolioRunReplication> T getFirstCurrencyReplicationForCurrency(List<T> currencyReplicationList, InvestmentSecurity currency) {
		T firstFound = null; // Track the first found, but will try to return the first found that actually has a target to actual exposure allocated

		for (T curRep : CollectionUtils.getIterable(currencyReplicationList)) {
			if (currency.equals(curRep.getUnderlyingSecurity())) {
				if (PortfolioUtils.isPortfolioReplicationEmpty(curRep)) {
					if (firstFound == null) {
						firstFound = curRep;
					}
				}
				else {
					return curRep;
				}
			}
		}

		return firstFound;
	}


	/**
	 * Returns a map of all securities that are CCY (key) with the quantity (or 0 if not held by the account) as the value
	 */
	private Map<InvestmentSecurity, BigDecimal> getProductInvestmentAccountCurrencyContractMap(PortfolioAccountContractStore contractStore) {
		Map<InvestmentSecurity, BigDecimal> currencyMap = new HashMap<>();
		Map<Integer, PortfolioAccountContractStore.SecurityPositionQuantity> securityQuantityMap = contractStore.getSecurityQuantityMap();

		if (!securityQuantityMap.isEmpty()) {
			SecuritySearchForm searchForm = new SecuritySearchForm();
			searchForm.setCurrency(true);
			List<InvestmentSecurity> currencySecurityList = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);

			for (InvestmentSecurity sec : CollectionUtils.getIterable(currencySecurityList)) {
				// Add it to the map with zero if there is no value associated in the store so the currency security will still be processed for unrealized
				PortfolioAccountContractStore.SecurityPositionQuantity securityPositionQuantity = securityQuantityMap.get(sec.getId());
				currencyMap.put(sec, securityPositionQuantity == null ? BigDecimal.ZERO : securityPositionQuantity.getQuantity());
			}
		}
		return currencyMap;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public InvestmentReplicationHistoryRebuildService getInvestmentReplicationHistoryRebuildService() {
		return this.investmentReplicationHistoryRebuildService;
	}


	public void setInvestmentReplicationHistoryRebuildService(InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService) {
		this.investmentReplicationHistoryRebuildService = investmentReplicationHistoryRebuildService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
