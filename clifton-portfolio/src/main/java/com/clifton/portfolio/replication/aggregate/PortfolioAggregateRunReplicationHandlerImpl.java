package com.clifton.portfolio.replication.aggregate;

import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.calculators.BaseInvestmentReplicationSecurityAllocation;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author mitchellf
 */
@Component
public class PortfolioAggregateRunReplicationHandlerImpl<D extends BasePortfolioRunReplication> implements PortfolioAggregateRunReplicationHandler<D> {

	@Override
	public List<PortfolioAggregateSecurityReplication<D>> getPortfolioAggregateRunReplication(List<D> replications) {
		D replication = CollectionUtils.getFirstElement(replications);

		if (replication != null) {
			PortfolioRun run = replication.getPortfolioRun();
			return convertPortfolioRunReplicationToAggregate(run, replications);
		}
		return Collections.emptyList();
	}


	private List<PortfolioAggregateSecurityReplication<D>> convertPortfolioRunReplicationToAggregate(PortfolioRun run, List<D> replications) {
		Map<InvestmentSecurity, List<D>> replicationsMap = CollectionUtils.getStream(replications)
				// Use LinkedHashMap to honor order of replications
				.collect(Collectors.groupingBy(BaseInvestmentReplicationSecurityAllocation::getSecurity, LinkedHashMap::new, Collectors.toList()));

		BigDecimal tpv = run.getPortfolioTotalValue();
		List<PortfolioAggregateSecurityReplication<D>> securityReplications = replicationsMap.entrySet().stream()
				.map(entry -> PortfolioAggregateSecurityReplication.createForSecurity(entry.getKey(), entry.getValue()))
				.peek(aggregateReplication -> {
					// Adjust allocation weights according to portfolio total value
					BigDecimal aggregateReplicationAllocationWeight = MathUtils.round(
							CoreMathUtils.getPercentValue(aggregateReplication.getTargetExposure(), tpv, true),
							DataTypes.PERCENT.getPrecision());
					aggregateReplication.setAllocationWeight(aggregateReplicationAllocationWeight);
				})
				.collect(Collectors.toList());

		return securityReplications;
	}
}
