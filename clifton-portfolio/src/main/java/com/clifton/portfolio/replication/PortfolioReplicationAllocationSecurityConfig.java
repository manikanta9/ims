package com.clifton.portfolio.replication;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityTargetAdjustmentTypes;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory;

import java.util.List;


/**
 * The <code>InvestmentReplicationAllocationSecurityConfig</code> contains security specific information
 * for a replication allocation when passed a list of currently held securities.
 * <p>
 * Security List - These securities are those currently held (match a passed in security list of securities held)
 *
 * @author manderson
 */
public class PortfolioReplicationAllocationSecurityConfig {

	private final InvestmentReplicationAllocationHistory replicationAllocationHistory;

	private List<InvestmentSecurity> securityList;

	/**
	 * If the replication allocation supports the portfolio to select
	 * the current security from list of held positions, this is the "override" current security for the portfolio
	 */
	private InvestmentSecurity portfolioCurrentSecurity;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioReplicationAllocationSecurityConfig(InvestmentReplicationAllocationHistory replicationAllocationHistory) {
		this.replicationAllocationHistory = replicationAllocationHistory;
	}


	public String getUniqueKey() {
		return this.replicationAllocationHistory.getId() + "";
	}


	public InvestmentReplicationAllocation getReplicationAllocation() {
		return getReplicationAllocationHistory().getReplicationAllocation();
	}


	public InvestmentSecurity getCurrentSecurity() {
		return ObjectUtils.coalesce(getPortfolioCurrentSecurity(), getReplicationAllocationHistory().getCurrentInvestmentSecurity());
	}


	public InvestmentCurrentSecurityTargetAdjustmentTypes getCurrentSecurityTargetAdjustmentType() {
		return ObjectUtils.coalesce(getReplicationAllocation().getCurrentSecurityTargetAdjustmentType(), InvestmentCurrentSecurityTargetAdjustmentTypes.DIFFERENCE_ALL);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isTargetAdjustmentsNeeded() {
		// No current security - no adjustments can be made
		if (getCurrentSecurity() == null) {
			return false;
		}

		// More than one security in the list
		if (CollectionUtils.getSize(getSecurityList()) > 1) {
			return true;
		}
		// Current Security is defined and not in the contained list (if list isn't empty)
		if (!CollectionUtils.isEmpty(getSecurityList())) {
			return !getSecurityList().contains(getCurrentSecurity());
		}
		return false;
	}


	public boolean isAlwaysIncludeCurrentSecurity() {
		if (getReplicationAllocation() != null) {
			return getReplicationAllocation().isAlwaysIncludeCurrentSecurity();
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationAllocationHistory getReplicationAllocationHistory() {
		return this.replicationAllocationHistory;
	}


	public InvestmentSecurity getPortfolioCurrentSecurity() {
		return this.portfolioCurrentSecurity;
	}


	public void setPortfolioCurrentSecurity(InvestmentSecurity portfolioCurrentSecurity) {
		this.portfolioCurrentSecurity = portfolioCurrentSecurity;
	}


	public List<InvestmentSecurity> getSecurityList() {
		return this.securityList;
	}


	public void setSecurityList(List<InvestmentSecurity> securityList) {
		this.securityList = securityList;
	}
}
