package com.clifton.portfolio.replication.calculator;

import com.clifton.core.util.MathUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.currency.InvestmentCurrencyService;
import com.clifton.investment.replication.calculators.InvestmentReplicationCurrencyTypes;
import com.clifton.investment.replication.calculators.InvestmentReplicationSecurityAllocation;
import com.clifton.investment.setup.InvestmentType;

import java.math.BigDecimal;


/**
 * The <code>PortfolioReplicationNotionalCurrencyForwardsCalculator</code> is a special calculator that is used for CCY Forwards
 * <p>
 * In most cases, the basic notional calculator is used, however, if the security is a forward, and the Underlying Security = Account Base CCY, then the Contract Value is 1
 * Which means that the quantity = exposure in that CCY.  This is because using the notional calculator would result in exposure using the latest FX Rate, rather
 * than the initial forward rate that was entered.  In this case the FX Rate is ignored from the contract value calculator
 *
 * @author manderson
 */
public class PortfolioReplicationNotionalCurrencyForwardsCalculator extends PortfolioReplicationNotionalCalculator {

	private InvestmentCurrencyService investmentCurrencyService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Overrides the method to be able to adjust the contract value for the transaction currency (underlying currency)
	 * based on currency convention.
	 */
	@Override
	public BigDecimal calculateContractValueImpl(InvestmentReplicationSecurityAllocation replication, InvestmentReplicationCurrencyTypes currencyType, @SuppressWarnings("unused") boolean livePrices) {
		if (currencyType == InvestmentReplicationCurrencyTypes.TRANSACTION_CURRENCY || isReplicationCurrencyForwardUnderlyingSameAsAccountBaseCurrency(replication)) {
			if (currencyType == InvestmentReplicationCurrencyTypes.TRANSACTION_CURRENCY) {
				replication.setTradeValueCurrency(replication.getSecurity().getUnderlyingSecurity());
			}
			return BigDecimal.ONE;
		}
		BigDecimal result = super.calculateContractValueImpl(replication, currencyType, livePrices);
		if (currencyType != InvestmentReplicationCurrencyTypes.SETTLE_CURRENCY) {
			return result;
		}
		replication.setTradeValueCurrency(replication.getSecurity().getInstrument().getTradingCurrency());
		return MathUtils.negate(result);
	}


	private boolean isReplicationCurrencyForwardUnderlyingSameAsAccountBaseCurrency(InvestmentReplicationSecurityAllocation replication) {
		if (replication != null && InvestmentUtils.isSecurityOfType(replication.getSecurity(), InvestmentType.FORWARDS)) {
			if (replication.getClientAccount().getBaseCurrency().equals(replication.getSecurity().getUnderlyingSecurity())) {
				return true;
			}
		}
		return false;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentCurrencyService getInvestmentCurrencyService() {
		return this.investmentCurrencyService;
	}


	public void setInvestmentCurrencyService(InvestmentCurrencyService investmentCurrencyService) {
		this.investmentCurrencyService = investmentCurrencyService;
	}
}
