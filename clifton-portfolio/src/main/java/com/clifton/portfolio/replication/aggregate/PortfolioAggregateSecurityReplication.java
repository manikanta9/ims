package com.clifton.portfolio.replication.aggregate;

import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.trade.PortfolioRunTradeDetail;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;


/**
 * <code>PortfolioAggregateSecurityReplication</code> is a portfolio detail that is comprised of one or more
 * portfolio details/replications that share the same security. The aggregate detail spans asset class, or target,
 * and is useful for holdings based trading.
 *
 * @author nickk
 */
@NonPersistentObject
public class PortfolioAggregateSecurityReplication<D extends BasePortfolioRunReplication> implements PortfolioRunTradeDetail, LabeledObject {

	/**
	 * The aggregate replication is aggregated on security. Thus, the id will be that of the security.
	 * A unique aggregate replication will be represented by the run and the security.
	 */
	private Integer id;

	private PortfolioRun portfolioRun;

	/**
	 * Security this aggregate replication represents.
	 */
	private InvestmentSecurity security;

	/**
	 * Target value in base currency for the security within the portfolio.
	 */
	private BigDecimal targetExposure;
	private BigDecimal additionalExposure;
	private BigDecimal totalAdditionalExposure;

	/**
	 * Allocation weight for the security within the portfolio.
	 */
	private BigDecimal allocationWeight;

	/**
	 * Contract value fields for calculating exposure from contracts
	 * - value for basis value
	 * - trade value is value used for trading (defined price, ccy modified value)
	 */
	private BigDecimal value;
	private BigDecimal tradeValue;
	private InvestmentSecurity tradeValueCurrency;

	private BigDecimal securityPrice;
	private BigDecimal exchangeRate;

	/**
	 * The following fields are used for Trade Generation
	 * Buy and Sell Contracts are set explicitly on the replication itself
	 * and during saving process is populate on the trade as quantityIntended & trade.buy
	 * NOT PERSISTED IN THE DB.  After save, it's cleared and values are moved to pending contracts
	 */
	private Trade trade;

	private BigDecimal buyContracts; // trade.quantityIntended if trade.buy = true
	private BigDecimal sellContracts; // trade.quantityIntended if trade.buy = false;

	private TradeOpenCloseType openCloseType; // trade.openCloseType to specify the transaction's position impact
	private Integer daysToSettle; // currently used for trade group window to override the settlement date from securities default days to settle

	// Actual Positions as of EOD previous business day
	private BigDecimal actualContracts = BigDecimal.ZERO;
	// Existing Positions as of next business day (those trades that have posted since EOD previous business day)
	private BigDecimal currentContracts;
	// Other Not Closed Trades (Not Booked & Not Cancelled)
	private BigDecimal pendingContracts;

	private BigDecimal tradeSecurityPrice;
	private Date tradeSecurityPriceDate;

	private BigDecimal tradeUnderlyingSecurityPrice;
	private Date tradeUnderlyingSecurityPriceDate;

	private BigDecimal tradeExchangeRate;
	private Date tradeExchangeRateDate;

	private BigDecimal delta;

	private boolean reverseExposureSign;

	private Boolean closePosition;

	private List<D> replicationList;

	private BigDecimal rebalanceTriggerMin;

	private BigDecimal rebalanceTriggerMax;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static <D extends BasePortfolioRunReplication> PortfolioAggregateSecurityReplication<D> createForSecurity(InvestmentSecurity security, List<D> replicationList) {
		PortfolioAggregateSecurityReplication<D> aggregateReplication = new PortfolioAggregateSecurityReplication<>();
		aggregateReplication.setSecurity(security);
		aggregateReplication.setReplicationList(replicationList);
		aggregateReplication.aggregateDetails();
		return aggregateReplication;
	}


	private void aggregateDetails() {
		for (D replication : CollectionUtils.getIterable(getReplicationList())) {
			// Set ID to that of teh security being aggregated
			setValueIfNull(getId(), replication.getSecurity().getId(), this::setId);
			setValueIfNull(getPortfolioRun(), replication.getPortfolioRun(), this::setPortfolioRun);
			setValueIfNull(getTrade(), replication.getTrade(), this::setTrade);
			// price, exchange rate, contract values
			setValueIfNull(getSecurityPrice(), replication.getSecurityPrice(), this::setSecurityPrice);
			setValueIfNull(getExchangeRate(), replication.getExchangeRate(), this::setExchangeRate);
			setValueIfNull(getValue(), replication.getValue(), this::setValue);
			setValueIfNull(getTradeValue(), replication.getTradeValue(), this::setTradeValue);
			setValueIfNull(getTradeValueCurrency(), replication.getTradeValueCurrency(), this::setTradeValueCurrency);
			setValueIfNull(getDelta(), replication.getDelta(), this::setDelta);
			// Target
			setTargetExposure(MathUtils.add(getTargetExposure(), replication.getTargetExposureAdjusted()));
			setAdditionalExposure(MathUtils.add(getAdditionalExposure(), replication.getAdditionalExposure()));
			setTotalAdditionalExposure(MathUtils.add(getTotalAdditionalExposure(), replication.getTotalAdditionalExposure()));
			// Allocation Weight
			setAllocationWeight(MathUtils.add(getAllocationWeight(), replication.getAllocationWeightAdjusted()));
			// Actual/Current/Pending Contracts
			setActualContracts(MathUtils.add(getActualContracts(), replication.getActualContractsAdjusted()));
			setCurrentContracts(MathUtils.add(getCurrentContracts(), replication.getCurrentContractsAdjusted()));
			setPendingContracts(MathUtils.add(getPendingContracts(), replication.getPendingContractsAdjusted()));
			// Replication items necessary
			setReverseExposureSign(replication.isReverseExposureSign());

			// For aggregating rebalance triggers - is sum the best way? Or should there be other calculations? The values are not used anywhere in the code, they are just set for display in UI
			setRebalanceTriggerMin(MathUtils.add(getRebalanceTriggerMin(), replication.getRebalanceTriggerMin()));
			setRebalanceTriggerMax(MathUtils.add(getRebalanceTriggerMax(), replication.getRebalanceTriggerMax()));
		}
	}


	private <T> void setValueIfNull(T existingValue, T newValue, Consumer<T> setter) {
		if (existingValue == null && newValue != null) {
			setter.accept(newValue);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isMatchingReplication() {
		return false;
	}


	public boolean isSecurityNotTradable() {
		if (getSecurity() != null && getSecurity().getInstrument() != null && getSecurity().getInstrument().getHierarchy() != null) {
			return getSecurity().getInstrument().getHierarchy().isTradingDisallowed();
		}
		return false;
	}


	public boolean isCashExposure() {
		if (getSecurity() != null && getTradingClientAccount() != null && getTradingClientAccount().getBaseCurrency() != null) {
			return getTradingClientAccount().getBaseCurrency().equals(getSecurity());
		}
		return false;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder label = new StringBuilder();
		if (getTradingClientAccount() != null) {
			label.append(getTradingClientAccount().getNumber()).append(": ");
		}
		if (getSecurity() != null) {
			label.append(getSecurity().getLabel());
		}
		return label.toString();
	}


	@Override
	public Integer getIdentity() {
		return getId();
	}


	@Override
	public boolean isNewBean() {
		return true;
	}


	@Override
	public Integer getId() {
		return this.id;
	}


	@Override
	public void setId(Integer id) {
		this.id = id;
	}


	@Override
	public String getTableName() {
		return "";
	}


	@Override
	public void clearNonPersistentFields() {
		this.tradeSecurityPrice = null;
		this.tradeSecurityPriceDate = null;
		this.tradeUnderlyingSecurityPrice = null;
		this.tradeUnderlyingSecurityPriceDate = null;
		this.tradeExchangeRate = null;
		this.tradeExchangeRateDate = null;
		this.tradeValue = null;
		this.tradeValueCurrency = null;

		this.trade = null;
		this.buyContracts = null;
		this.sellContracts = null;
		this.openCloseType = null;
		this.daysToSettle = null;
		this.pendingContracts = null;
		this.currentContracts = null;
	}


	@Override
	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	@Override
	public void setPortfolioRun(PortfolioRun portfolioRun) {
		this.portfolioRun = portfolioRun;
	}


	@Override
	public InvestmentAccount getTradingClientAccount() {
		return getPortfolioRun() == null ? null : getPortfolioRun().getClientInvestmentAccount();
	}


	@Override
	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	@Override
	public InvestmentAccountAssetClass getAccountAssetClass() {
		return null;
	}


	@Override
	public boolean isTradeEntryDisabled() {
		if (isSecurityNotTradable()) {
			return true;
		}
		if (isCashExposure()) {
			return true;
		}
		if (getAccountAssetClass() != null) {
			return getAccountAssetClass().isReplicationPositionExcludedFromCount();
		}

		return false;
	}


	@Override
	public BigDecimal calculateSuggestedTradeQuantity() {
		// Return null. This is used in rule evaluation and trading will be done against the basis replication(s)
		return null;
	}


	@Override
	public InvestmentReplication getReplication() {
		// Aggregation could span replications
		return null;
	}


	@Override
	public InvestmentReplicationAllocationHistory getReplicationAllocationHistory() {
		// Aggregation could span replications
		return null;
	}


	@Override
	public BigDecimal getSecurityPrice() {
		return this.securityPrice;
	}


	@Override
	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}


	@Override
	public void setTrade(Trade trade) {
		this.trade = trade;
	}


	@Override
	public Trade getTrade() {
		return this.trade;
	}


	@Override
	public BigDecimal getTradeSecurityPrice() {
		return this.tradeSecurityPrice;
	}


	@Override
	public void setTradeSecurityPrice(BigDecimal tradeSecurityPrice) {
		this.tradeSecurityPrice = tradeSecurityPrice;
	}


	@Override
	public Date getTradeSecurityPriceDate() {
		return this.tradeSecurityPriceDate;
	}


	@Override
	public void setTradeSecurityPriceDate(Date date) {
		this.tradeSecurityPriceDate = date;
	}


	@Override
	public BigDecimal getTradeExchangeRate() {
		return this.tradeExchangeRate;
	}


	@Override
	public void setTradeExchangeRate(BigDecimal tradeExchangeRate) {
		this.tradeExchangeRate = tradeExchangeRate;
	}


	@Override
	public Date getTradeExchangeRateDate() {
		return this.tradeExchangeRateDate;
	}


	@Override
	public void setTradeExchangeRateDate(Date date) {
		this.tradeExchangeRateDate = date;
	}


	@Override
	public BigDecimal getBuyContracts() {
		return this.buyContracts;
	}


	@Override
	public void setBuyContracts(BigDecimal buyContracts) {
		this.buyContracts = buyContracts;
	}


	@Override
	public BigDecimal getSellContracts() {
		return this.sellContracts;
	}


	@Override
	public void setSellContracts(BigDecimal sellContracts) {
		this.sellContracts = sellContracts;
	}


	@Override
	public TradeOpenCloseType getOpenCloseType() {
		return this.openCloseType;
	}


	@Override
	public void setOpenCloseType(TradeOpenCloseType openCloseType) {
		this.openCloseType = openCloseType;
	}


	@Override
	public BigDecimal getActualContracts() {
		return this.actualContracts;
	}


	@Override
	public void setActualContracts(BigDecimal actualContracts) {
		this.actualContracts = actualContracts;
	}


	@Override
	public BigDecimal getPendingContracts() {
		return this.pendingContracts;
	}


	@Override
	public void setPendingContracts(BigDecimal pendingContracts) {
		this.pendingContracts = pendingContracts;
	}


	@Override
	public BigDecimal getCurrentContracts() {
		return this.currentContracts;
	}


	@Override
	public void setCurrentContracts(BigDecimal currentContracts) {
		this.currentContracts = currentContracts;
	}


	@Override
	public Integer getDaysToSettle() {
		return this.daysToSettle;
	}


	@Override
	public void setDaysToSettle(Integer daysToSettle) {
		this.daysToSettle = daysToSettle;
	}


	public BigDecimal getTargetContracts() {
		BigDecimal val = getValue();
		if (MathUtils.isNullOrZero(val)) {
			return BigDecimal.ZERO;
		}
		// Include Currency (will be null where it doesn't apply)
		BigDecimal contracts = MathUtils.divide(getTargetExposure(), val);

		if (isReverseExposureSign()) {
			contracts = MathUtils.negate(contracts);
		}
		return contracts;
	}


	public BigDecimal getActualExposure() {
		return getExposure(getActualContracts());
	}


	public BigDecimal getPendingExposure() {
		return getExposure(getPendingContracts());
	}


	public BigDecimal getCurrentExposure() {
		return getExposure(getCurrentContracts());
	}


	public BigDecimal getTargetAdjustedContractDifference() {
		return MathUtils.subtract(
				MathUtils.subtract(getTargetContracts(), getCurrentContracts()),
				getPendingContracts()
		);
	}


	public BigDecimal getTargetAdjustedExposureDifference() {
		return MathUtils.subtract(
				MathUtils.subtract(getTargetExposure(), getCurrentExposure()),
				getPendingExposure()
		);
	}


	private BigDecimal getExposure(BigDecimal contracts) {
		if (MathUtils.isNullOrZero(contracts)) {
			return BigDecimal.ZERO;
		}
		// To match values calculated after db save, # contracts save with 2 decimals and contract value with 10 decimals
		contracts = MathUtils.round(contracts, 2);
		BigDecimal val = getValue();
		if (MathUtils.isNullOrZero(val)) {
			return BigDecimal.ZERO;
		}
		val = MathUtils.round(val, 10);
		if (isReverseExposureSign()) {
			contracts = MathUtils.negate(contracts);
		}
		return MathUtils.multiply(val, contracts);
	}


	@Override
	public Boolean getClosePosition() {
		return this.closePosition;
	}


	@Override
	public void setClosePosition(Boolean closePosition) {
		this.closePosition = closePosition;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public BigDecimal getTargetExposure() {
		return this.targetExposure;
	}


	public void setTargetExposure(BigDecimal targetExposure) {
		this.targetExposure = targetExposure;
	}


	public BigDecimal getAdditionalExposure() {
		return this.additionalExposure;
	}


	public void setAdditionalExposure(BigDecimal additionalExposure) {
		this.additionalExposure = additionalExposure;
	}


	public BigDecimal getTotalAdditionalExposure() {
		return this.totalAdditionalExposure;
	}


	public void setTotalAdditionalExposure(BigDecimal totalAdditionalExposure) {
		this.totalAdditionalExposure = totalAdditionalExposure;
	}


	public BigDecimal getAllocationWeight() {
		return this.allocationWeight;
	}


	public void setAllocationWeight(BigDecimal allocationWeight) {
		this.allocationWeight = allocationWeight;
	}


	public BigDecimal getValue() {
		return this.value;
	}


	public void setValue(BigDecimal value) {
		this.value = value;
	}


	@Override
	public BigDecimal getTradeValue() {
		return this.tradeValue;
	}


	@Override
	public void setTradeValue(BigDecimal tradeValue) {
		this.tradeValue = tradeValue;
	}


	public InvestmentSecurity getTradeValueCurrency() {
		return this.tradeValueCurrency;
	}


	public void setTradeValueCurrency(InvestmentSecurity tradeValueCurrency) {
		this.tradeValueCurrency = tradeValueCurrency;
	}


	public void setSecurityPrice(BigDecimal securityPrice) {
		this.securityPrice = securityPrice;
	}


	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}


	public BigDecimal getTradeUnderlyingSecurityPrice() {
		return this.tradeUnderlyingSecurityPrice;
	}


	public void setTradeUnderlyingSecurityPrice(BigDecimal tradeUnderlyingSecurityPrice) {
		this.tradeUnderlyingSecurityPrice = tradeUnderlyingSecurityPrice;
	}


	public Date getTradeUnderlyingSecurityPriceDate() {
		return this.tradeUnderlyingSecurityPriceDate;
	}


	public void setTradeUnderlyingSecurityPriceDate(Date tradeUnderlyingSecurityPriceDate) {
		this.tradeUnderlyingSecurityPriceDate = tradeUnderlyingSecurityPriceDate;
	}


	public BigDecimal getDelta() {
		return this.delta;
	}


	public void setDelta(BigDecimal delta) {
		this.delta = delta;
	}


	public boolean isReverseExposureSign() {
		return this.reverseExposureSign;
	}


	public void setReverseExposureSign(boolean reverseExposureSign) {
		this.reverseExposureSign = reverseExposureSign;
	}


	public List<D> getReplicationList() {
		return this.replicationList;
	}


	public void setReplicationList(List<D> replicationList) {
		this.replicationList = replicationList;
	}


	public BigDecimal getRebalanceTriggerMin() {
		return this.rebalanceTriggerMin;
	}


	public void setRebalanceTriggerMin(BigDecimal rebalanceTriggerMin) {
		this.rebalanceTriggerMin = rebalanceTriggerMin;
	}


	public BigDecimal getRebalanceTriggerMax() {
		return this.rebalanceTriggerMax;
	}


	public void setRebalanceTriggerMax(BigDecimal rebalanceTriggerMax) {
		this.rebalanceTriggerMax = rebalanceTriggerMax;
	}
}
