package com.clifton.portfolio.replication.calculator;


import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.investment.replication.calculators.InvestmentReplicationCurrencyTypes;
import com.clifton.investment.replication.calculators.InvestmentReplicationSecurityAllocation;

import java.math.BigDecimal;


/**
 * The <code>PortfolioReplicationCreditDurationCalculator</code> calculates contract value
 * used for Credit default swaps.  Calculation is Security Duration Value / Asset Class Duration Benchmark's Credit Duration value
 * NOTE: Prices are not used
 *
 * @author manderson
 */
public class PortfolioReplicationCreditDurationCalculator extends BasePortfolioReplicationCalculator {

	@Override
	public void validateInvestmentReplicationType(InvestmentReplicationType replicationType) {
		ValidationUtils.assertTrue(replicationType.isCreditDurationSupported(), "Credit Duration must be supported for this replication type since it's required by the selected calculator [" + replicationType.getCalculatorBean().getName() + "]");
		super.validateInvestmentReplicationType(replicationType);
	}


	@Override
	public boolean isDurationRequired(InvestmentReplicationSecurityAllocation replication) {
		return true;
	}


	@Override
	public BigDecimal calculateContractValueImpl(InvestmentReplicationSecurityAllocation replication, @SuppressWarnings("unused") InvestmentReplicationCurrencyTypes currencyType, @SuppressWarnings("unused") boolean livePrices) {
		BigDecimal creditDuration = replication.getBenchmarkCreditDuration();
		if (MathUtils.isNullOrZero(creditDuration)) {
			throw new ValidationExceptionWithCause("InvestmentSecurity", replication.getBenchmarkDurationSecurity().getId(),
					"Asset Class Benchmark Duration Security [" + replication.getBenchmarkDurationSecurity().getLabel()
							+ "] Credit Duration value is missing and required for replication contract value calculation.");
		}
		return MathUtils.divide(replication.getDuration(), creditDuration);
	}
}
