package com.clifton.portfolio.replication;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.calculators.InvestmentReplicationCurrencyTypes;
import com.clifton.investment.replication.calculators.InvestmentReplicationSecurityAllocation;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.trade.PortfolioCurrencyTrade;

import java.util.Date;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;


/**
 * <code>PortfolioReplicationHandler</code> defines business service methods for
 * assisting in calculating details of {@link InvestmentReplicationSecurityAllocation}s of a portfolio.
 *
 * @author manderson
 * @author nickk
 */
public interface PortfolioReplicationHandler {

	/**
	 * Sets the Security and all Replication Allocation & Security Information on the Replication
	 */
	public <T extends InvestmentReplicationSecurityAllocation> void setupInvestmentReplicationSecurityAllocation(T replication, InvestmentSecurity security, String exchangeRateDataSourceName, boolean excludeMispricing);


	/**
	 * THere are some fields (Delta) whose value is dependent on the target. These values need to be reset after all targets are adjusted
	 */
	public <T extends InvestmentReplicationSecurityAllocation> void recalculateInvestmentReplicationSecurityAllocation(T replication);


	/**
	 * If setLivePrices - Sets security live prices, live exchange rates, etc. and
	 * Else clearsLivePrices except where manually overridden
	 * <p>
	 * re-calculates contract value as the "tradeValue"
	 * <p>
	 * Currency type is used to define which currency the trade values should be calculated for
	 */
	public <T extends InvestmentReplicationSecurityAllocation> void refreshInvestmentReplicationSecurityAllocationLivePrices(T replication, InvestmentReplicationCurrencyTypes currencyType, boolean setLivePrices, boolean excludeMispricing);


	/**
	 * Validates given security for the {@link BasePortfolioRunReplication} and returns true if a replication does not exist in the list of replications per the filtered scope.
	 *
	 * @param replication               - replication to use for filtering and validating for an security match
	 * @param security                  - the security to filter existing replication matches for
	 * @param replicationFilterFunction - optional filter function to match replications of the replication list according to the value returned the provided replication
	 * @param fullRunRepList            - full replication list for a portfolio run to filter for matches
	 * @param throwExceptionIfNotValid  - if a replication exists for the provided security and exception is thrown when true
	 */
	public <T extends InvestmentReplicationSecurityAllocation, V> boolean validateInvestmentReplicationSecurityAllocationSecurityChange(T replication, InvestmentSecurity security, Function<T, V> replicationFilterFunction, List<T> fullRunRepList, boolean throwExceptionIfNotValid);


	/**
	 * Does not do any database look ups for security information, etc.  Just recalculates the contract value on the replication.
	 * Adjusts contractValue (if necessary) based on doNotUseExposureMultiplier override option
	 * <p>
	 * Used in Billing to ensure we aren't billing on exposure multiplier adjusted exposure
	 */
	public <T extends InvestmentReplicationSecurityAllocation> void recalculateContractValue(T replication, boolean doNotUseExposureMultiplier);


	/**
	 * Returns true if the replication's contract value calculator bean requires duration
	 * i.e. adjustment type =  Duration Adjustment or Credit Duration calculator
	 * This DOES NOT evaluate if adjustments are made OR if the adjustment applies to the specific security
	 * - This keeps backwards compatibility from when we used the flag on the ENUM calculator type
	 */
	public <T extends InvestmentReplicationSecurityAllocation> boolean isReplicationCalculatorDurationRequired(T replication);


	/**
	 * Generates the list of {@link PortfolioReplicationAllocationSecurityConfig} objects which contains the allocation history as well as the filtered list of securities that apply to the allocation
	 * Used for portfolio processing
	 */
	public List<PortfolioReplicationAllocationSecurityConfig> getPortfolioReplicationAllocationSecurityConfigListForBalanceDate(int replicationId, Date balanceDate, List<InvestmentSecurity> securityList);


	/**
	 * Populates Currency Exposure on the Currency Replications and also applies the "other" currency to each replication.
	 * <p>
	 * Currency Trade List is a list of all Physical Currency Exposure (Those tied to replications & other)
	 */
	public <T extends BasePortfolioRunReplication> void processPortfolioReplicationListForCurrencyContracts(PortfolioAccountContractStore contracts,
	                                                                                                        BiFunction<PortfolioAccountContractStore, T, List<AccountingPosition>> contractPositionFunction,
	                                                                                                        PortfolioRun run,
	                                                                                                        List<T> currencyReplicationList,
	                                                                                                        List<PortfolioCurrencyOther> currencyOtherList,
	                                                                                                        List<PortfolioCurrencyTrade<T>> currencyTradeList,
	                                                                                                        List<T> fullReplicationList,
	                                                                                                        Function<T, BaseSimpleEntity<Integer>> replicationMatchingFilterFunction);
}
