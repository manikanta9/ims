package com.clifton.portfolio.replication.calculator;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.investment.rates.InvestmentRatesService;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.investment.replication.calculators.InvestmentReplicationCalculator;
import com.clifton.investment.replication.calculators.InvestmentReplicationCurrencyTypes;
import com.clifton.investment.replication.calculators.InvestmentReplicationSecurityAllocation;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.MarketDataService;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.rates.MarketDataRatesRetriever;
import com.clifton.portfolio.replication.PortfolioReplicationHandler;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>BaseProductReplicationCalculator</code> is the Default, "Notional" calculator
 *
 * @author manderson
 */
public abstract class BasePortfolioReplicationCalculator implements InvestmentReplicationCalculator {


	protected static final String PRICE_FIELD = "securityPrice";
	protected static final String PRICE_FIELD_UNDERLYING = "underlyingSecurityPrice";
	protected static final String PRICE_FIELD_DIRTY = "securityDirtyPrice";
	protected static final String PRICE_FIELD_STRIKE = "strikePrice";

	protected static final String ADJUSTMENT_TYPE_SYNTHETIC = "Synthetic Adjustment";
	protected static final String ADJUSTMENT_TYPE_DURATION = "Duration Adjustment";

	////////////////////////////////////////////////////////////////////////////////

	private AccountingPositionService accountingPositionService;

	private InvestmentCalculator investmentCalculator;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentRatesService investmentRatesService;
	private InvestmentReplicationService investmentReplicationService;
	private InvestmentSecurityEventService investmentSecurityEventService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private MarketDataRatesRetriever marketDataRatesRetriever;
	private MarketDataRetriever marketDataRetriever;
	private MarketDataService marketDataService;

	private PortfolioReplicationHandler portfolioReplicationHandler;

	////////////////////////////////////////////////////////////////////////////////

	private String adjustmentType;

	private List<Short> adjustmentInvestmentTypeIdList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateInvestmentReplicationType(InvestmentReplicationType replicationType) {
		String calculatorName = replicationType.getCalculatorBean().getName();
		if (ADJUSTMENT_TYPE_DURATION.equals(getAdjustmentType())) {
			ValidationUtils.assertTrue(replicationType.isDurationSupported(), "Duration must be supported for this replication type since it's required by the selected calculator [" + calculatorName + "]");
		}
		if (ADJUSTMENT_TYPE_SYNTHETIC.equals(getAdjustmentType())) {
			ValidationUtils.assertTrue(replicationType.isSyntheticAdjustmentFactorSupported(),
					"Synthetic Adjustment Factor must be supported for this replication type since it's required by the selected calculator [" + calculatorName + "]");
			ValidationUtils.assertTrue(replicationType.isInterestRateSupported(),
					"Interest Rate Index Group must be selected to support setting interest rates for this replication type since it's required by the selected calculator [" + calculatorName + "]");
		}

		// Dirty Price
		if (PRICE_FIELD_DIRTY.equals(getPriceField(null))) {
			ValidationUtils.assertTrue(replicationType.isDirtyPriceSupported(), "Dirty Price must be supported for this replication type since it's required by the selected calculator [" + calculatorName + "]");
			ValidationUtils.assertTrue(replicationType.isIndexRatioSupported(), "Index Ratio must be supported for this replication type since it's required by the selected calculator [" + calculatorName + "]");
		}
	}


	@Override
	public void calculate(InvestmentReplicationSecurityAllocation replication, String exchangeRateDataSourceName, boolean excludeMispricing) {
		setupReplicationSecurityInfo(replication, exchangeRateDataSourceName);
		if (!excludeMispricing) {
			populateFairValueAdjustmentValues(replication);
		}
		calculateContractValue(replication, InvestmentReplicationCurrencyTypes.getDefaultCurrencyType(), false);
	}


	@Override
	public void calculateForTargetUpdates(InvestmentReplicationSecurityAllocation replication) {
		// If nothing changes, don't recalculate contract value
		if (recalculateReplicationSecurityInfo(replication)) {
			calculateContractValue(replication, InvestmentReplicationCurrencyTypes.getDefaultCurrencyType(), false);
		}
	}


	@Override
	public void calculateForTrading(InvestmentReplicationSecurityAllocation replication, InvestmentReplicationCurrencyTypes currencyType, boolean livePrices, boolean excludeMispricing) {
		boolean recalculateTradeValue = updateReplicationSecurityWithLivePrices(replication, livePrices);
		if (!excludeMispricing) {
			populateFairValueAdjustmentValues(replication);
		}
		if (recalculateTradeValue || (currencyType != null && !currencyType.isBaseCurrencyType())) {
			calculateContractValue(replication, currencyType, true);
		}
		setupReplicationTradingFields(replication);
	}


	@Override
	public void recalculateContractValue(InvestmentReplicationSecurityAllocation replication, boolean doNotUseExposureMultiplier) {
		calculateContractValue(replication, InvestmentReplicationCurrencyTypes.getDefaultCurrencyType(), false);
		// If option different than how was calculated, and exposure multiplier is defined and not = 1 then need to adjust contract value
		// based on overriding option
		BigDecimal expMultiplier = replication.getSecurity().getInstrument().getExposureMultiplier();
		if (replication.getReplicationType().isDoNotApplyExposureMultiplier() != doNotUseExposureMultiplier) {
			if (!MathUtils.isNullOrZero(expMultiplier) && !MathUtils.isEqual(BigDecimal.ONE, expMultiplier)) {
				replication.setValue(doNotUseExposureMultiplier ? MathUtils.divide(replication.getValue(), expMultiplier) : MathUtils.multiply(replication.getValue(), expMultiplier));
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void setupReplicationSecurityInfo(InvestmentReplicationSecurityAllocation replication, String exchangeRateDataSourceName) {
		InvestmentSecurity security = replication.getSecurity();
		Date balanceDate = replication.getBalanceDate();

		// Security Label
		String securityLabel = replication.getReplication().getLabel();
		if (security.isCurrency() && !replication.isCashExposure()) {
			throw new ValidationException(
					securityLabel
							+ " has an allocation for currency ["
							+ security.getSymbol()
							+ "]. Please verify this replication setup, as currencies should not be selected for replication allocations.  The only exception is the CCY that is the client account's base currency ["
							+ replication.getClientAccount().getBaseCurrency().getSymbol() + "] which can be used to indicate cash overlay exposure.");
		}

		// Price Multiplier is required for all...
		if (MathUtils.isNullOrZero(security.getPriceMultiplier())) {
			throw new ValidationExceptionWithCause("InvestmentInstrument", security.getInstrument().getId(), "Instrument [" + security.getInstrument().getLabel()
					+ "] price multiplier is missing or zero.");
		}

		// Security Price
		// If Security Price is already populated, use it - for cases where positions were traded before start date we use the price from the transactions
		// since market data will not exist yet
		if (replication.getSecurityPrice() == null) {
			if (replication.isCashExposure()) {
				replication.setSecurityPrice(BigDecimal.ONE);
			}
			else {
				// If Trade Price is set, i.e. manually overridden - don't throw an exception if missing and just use override
				if (replication.getTradeSecurityPrice() != null) {
					replication.setSecurityPrice(getMarketDataRetriever().getPrice(security, balanceDate, false, securityLabel));
					if (replication.getSecurityPrice() == null) {
						replication.setSecurityPrice(replication.getTradeSecurityPrice());
					}
				}
				else {
					// Currently the Agency MBS Bonds that this is intended for are not used as positions on the run, but in case
					// we'd like to add them there would be an extra lookup against positions from the date of the first trade until the monthly batch job
					// that would insert a new price
					// Allow flexible prices - use position price if none
					if (security.getInstrument().getHierarchy().getPricingFrequency().isPositionPriceUsedWhenSecurityPriceMissing()) {
						// Use flexible lookup first - if none then check position price
						BigDecimal price = getMarketDataRetriever().getPriceFlexible(security, balanceDate, false);
						if (price == null) {

							// Find the latest position - not necessarily the latest for the account
							List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListForSecurity(security.getId(), balanceDate);
							if (!CollectionUtils.isEmpty(positionList)) {
								AccountingPosition latest = CollectionUtils.getFirstElement(BeanUtils.sortWithFunction(positionList, AccountingPosition::getTransactionDate, false));
								if (latest != null) {
									price = latest.getPrice();
								}
							}
						}
						if (price == null) {
							throw new ValidationExceptionWithCause("InvestmentSecurity", security.getId(), "Security [" + security.getLabel() + "] is missing a price on or before ["
									+ DateUtils.fromDateShort(balanceDate) + "] and there are no existing positions to use a price from.");
						}
						replication.setSecurityPrice(price);
					}
					else {
						replication.setSecurityPrice(getMarketDataRetriever().getPrice(security, balanceDate, true, securityLabel));
					}
				}
			}
		}

		// Exchange Rates
		String fromCurrency = security.getInstrument().getTradingCurrency().getSymbol();
		String toCurrency = replication.getClientAccount().getBaseCurrency().getSymbol();
		replication.setExchangeRate(getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(exchangeRateDataSourceName, fromCurrency, toCurrency, balanceDate)));

		// Underlying Security Info - Call calculated getter from the replication instead of directly on the security, so ultimate underlying option is accounted for in determining what the underlying security should be
		if (replication.getUnderlyingSecurity() != null) {
			replication.setUnderlyingSecurityPrice(getMarketDataRetriever().getPrice(replication.getUnderlyingSecurity(), balanceDate, true, securityLabel + "Underlying "));
		}
		replication.setDuration(getDuration(replication, balanceDate));
		replication.setInterestRate(getInterestRate(replication, balanceDate));
		replication.setSyntheticAdjustmentFactor(getSyntheticAdjustmentFactor(replication));
		replication.setIndexRatio(getIndexRatio(replication, balanceDate));
		replication.setSecurityDirtyPrice(getSecurityDirtyPrice(replication, false));
		replication.setDelta(getDelta(replication, balanceDate));
		replication.setFactor(getFactor(replication, balanceDate));
	}


	private boolean recalculateReplicationSecurityInfo(InvestmentReplicationSecurityAllocation replication) {
		// Delta and Dirty Price currently are the only values that could change based on target change
		// If the change results in contract value change - need to return true to indicate recalculation
		boolean cvChange = false;
		if (replication.getReplicationType().isDeltaSupported()) {
			Date balanceDate = replication.getBalanceDate();
			replication.setDelta(getDelta(replication, balanceDate));
			// Delta doesn't affect contract value, we don't need to recalc contract value
		}
		if (replication.getReplicationType().isDirtyPriceSupported()) {
			BigDecimal original = replication.getSecurityDirtyPrice();
			replication.setSecurityDirtyPrice(getSecurityDirtyPrice(replication, false));
			// Only if calculator uses dirty price, then need to recalc contract value
			if (PRICE_FIELD_DIRTY.equals(getPriceField(replication)) && !MathUtils.isEqual(original, replication.getSecurityDirtyPrice())) {
				cvChange = true;
			}
		}
		return cvChange;
	}


	private void populateFairValueAdjustmentValues(InvestmentReplicationSecurityAllocation replication) {
		if (replication.getSecurity().getInstrument().isFairValueAdjustmentUsed()) {
			// We don't save this on the run, used for Trade Creation Screen only
			// However, if the account has this option turned on, we still calculate for a total mispricing value for the account
			// to display Cash Exposure % adjusted for mispricing where needed
			replication.setPreviousFairValueAdjustment(getMarketDataRetriever().getFutureFairValue(replication.getSecurity(),
					DateUtils.getPreviousWeekday(replication.getBalanceDate())));
			replication.setFairValueAdjustment(getMarketDataRetriever().getFutureFairValue(replication.getSecurity(), replication.getBalanceDate()));
		}
	}


	protected void calculateContractValue(InvestmentReplicationSecurityAllocation replication, InvestmentReplicationCurrencyTypes currencyType, boolean livePrices) {
		// Calculate Contract Value
		BigDecimal result = calculateContractValueImpl(replication, currencyType, livePrices);

		if (replication.getReplicationType().isApplyCurrentFactor() && replication.getFactor() != null) {
			// Factor is always populated, only include it in contract value calculation if set and not disabled.
			result = MathUtils.multiply(result, replication.getFactor());
		}

		if (isContractValueExchangeRateAdjustmentNeeded(replication, currencyType)) {
			// Apply Exchange Rate to client account base currency
			BigDecimal fxRate = getPropertyValue(replication, "exchangeRate", livePrices);
			if (fxRate != null && !MathUtils.isEqual(BigDecimal.ONE, fxRate)) {
				result = MathUtils.multiply(result, fxRate);
			}
		}

		if (livePrices) {
			replication.setTradeValue(result);
		}
		else {
			replication.setValue(result);
		}
	}


	protected boolean isContractValueExchangeRateAdjustmentNeeded(InvestmentReplicationSecurityAllocation replication, InvestmentReplicationCurrencyTypes currencyType) {
		if (currencyType == null || currencyType.isBaseCurrencyType()) {
			return true;
		}
		InvestmentSecurity baseCurrency = InvestmentUtils.getClientAccountBaseCurrency(replication.getClientAccount());
		InvestmentSecurity denominationCurrency = InvestmentUtils.getSecurityCurrencyDenomination(replication.getSecurity());
		if (CompareUtils.isEqual(baseCurrency, denominationCurrency)) {
			return true;
		}

		return false;
	}


	private boolean updateReplicationSecurityWithLivePrices(InvestmentReplicationSecurityAllocation replication, boolean setLivePrices) {
		InvestmentSecurity sec = replication.getSecurity();
		InvestmentSecurity uSec = replication.getUnderlyingSecurity();
		InvestmentSecurity baseCurrency = replication.getClientAccount().getBaseCurrency();
		replication.setTradeValueCurrency(baseCurrency);

		boolean recalc = false;
		if (!setLivePrices) {
			// Clears live prices except for where manually overridden - if any overrides - recalculate tradeValue
			if (replication.isTradePropertyValueManuallyOverridden("tradeSecurityPrice")) {
				recalc = true;
			}
			else {
				replication.setTradeSecurityPrice(null);
				replication.setTradeSecurityPriceDate(null);
			}
			if (replication.isTradePropertyValueManuallyOverridden("tradeUnderlyingSecurityPrice")) {
				recalc = true;
			}
			else {
				replication.setTradeUnderlyingSecurityPrice(null);
				replication.setTradeUnderlyingSecurityPriceDate(null);
			}

			// Not Available At this time to manually override
			replication.setTradeSecurityDirtyPrice(null);
			replication.setTradeSecurityDirtyPriceDate(null);

			replication.setTradeExchangeRate(null);
			replication.setTradeExchangeRateDate(null);

			replication.setTradeCurrencyExchangeRate(null);
			replication.setTradeCurrencyExchangeRateDate(null);

			replication.setTradeFairValueAdjustment(null);
			replication.setTradeFairValueAdjustmentDate(null);

			replication.setTradeValue(null);

			// Reset Delta
			if (replication.getReplicationType().isDeltaSupported()) {
				replication.setTradeDelta(null);
			}
		}
		else {
			recalc = true;
			if (!replication.isTradePropertyValueManuallyOverridden("tradeSecurityPrice")) {
				setMarketDataLive(replication, "tradeSecurityPrice", getMarketDataRetriever().getLivePrice(sec));
			}
			if (uSec != null && !replication.isTradePropertyValueManuallyOverridden("tradeUnderlyingSecurityPrice")) {
				setMarketDataLive(replication, "tradeUnderlyingSecurityPrice", getMarketDataRetriever().getLivePrice(uSec));
			}
			setMarketDataLive(replication, "tradeExchangeRate", getMarketDataRatesRetriever().getLiveExchangeRate(sec, baseCurrency));
			replication.setTradeSecurityDirtyPrice(getSecurityDirtyPrice(replication, true));

			setMarketDataLive(replication, "tradeFairValueAdjustment", getMarketDataRetriever().getLiveFutureFairValue(sec));

			// Currency Exchange Rates
			if (replication.getReplicationType().isCurrency()) {
				setMarketDataLive(replication, "tradeCurrencyExchangeRate", getMarketDataRatesRetriever().getLiveExchangeRate(uSec, baseCurrency));
			}

			// Update live Delta
			if (replication.getReplicationType().isDeltaSupported()) {
				MarketDataLive liveDelta = getMarketDataRetriever().getLiveDelta(sec);
				// Only update the Delta value if there is a live value. If there is not
				// a live value, the flexible Delta value should remain.
				if (liveDelta != null && liveDelta.asNumericValue() != null) {
					replication.setTradeDelta(isReplicationDeltaNegative(replication) ? MathUtils.negate(liveDelta.asNumericValue()) : liveDelta.asNumericValue());
				}
			}
		}
		return recalc;
	}


	public void setMarketDataLive(InvestmentReplicationSecurityAllocation replication, String valueFieldName, MarketDataLive marketDataLive) {
		// Only populate market data live if not manually overridden (extra check - also checked before actually calling method to get value)
		if (!replication.isTradePropertyValueManuallyOverridden(valueFieldName)) {
			if (marketDataLive != null) {
				BigDecimal val = marketDataLive.asNumericValue();
				Date valueDate = marketDataLive.getMeasureDate();

				if (val != null && valueDate != null) {
					Date currentValueDate = BeanUtils.getFieldValue(replication, valueFieldName + "Date");
					BigDecimal currentValue = BeanUtils.getFieldValue(replication, valueFieldName);

					if (currentValueDate == null || valueDate.after(currentValueDate) || (valueDate.compareTo(currentValueDate) == 0 && !MathUtils.isEqual(val, currentValue))) {
						BeanUtils.setPropertyValue(replication, valueFieldName, val);
						BeanUtils.setPropertyValue(replication, valueFieldName + "Date", valueDate);
					}
				}
			}
		}
	}


	protected void setupReplicationTradingFields(InvestmentReplicationSecurityAllocation replication) {
		replication.setContractValuePriceField(getPriceField(replication));
	}


	/**
	 * Performs the calculator specific calculation
	 */
	public abstract BigDecimal calculateContractValueImpl(InvestmentReplicationSecurityAllocation replication, InvestmentReplicationCurrencyTypes currencyType, boolean livePrices);

	////////////////////////////////////////////////////////////////////////////////
	/////////////                  Populate Data Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	private BigDecimal getDuration(InvestmentReplicationSecurityAllocation replication, Date date) {
		// Duration is not supported - skip
		if (!replication.getReplicationType().isDurationSupported()) {
			return null;
		}

		// Supported vs. Required - Required only if it's necessary to calculate contract value, otherwise just supported
		boolean required = isDurationRequired(replication);

		if (MathUtils.isNullOrZero(replication.getBenchmarkDuration())) {
			if (replication.getBenchmarkDurationSecurity() != null) {
				throw new ValidationExceptionWithCause("InvestmentSecurity", replication.getBenchmarkDurationSecurity().getId(),
						"Asset Class Benchmark Duration Security [" + replication.getBenchmarkDurationSecurity().getLabel() + "] duration is missing.");
			}
			if (required) {
				throw new ValidationExceptionWithCause("InvestmentReplication", replication.getReplication().getId(), "Unable to determine Replication ["
						+ replication.getReplication().getName() + "] Benchmark Duration because the benchmark duration security is missing.");
			}
		}

		// BETTER WAY TO DO THIS THEN TO RE-CREATE THE REPLICATION ALLOCATION LIST?
		InvestmentReplicationAllocation allocation = replication.getReplicationAllocation();
		if (allocation != null) {
			if (!MathUtils.isNullOrZero(allocation.getDurationOverride())) {
				return allocation.getDurationOverride();
			}
			if (allocation.isUseBenchmarkDuration()) {
				return replication.getBenchmarkDuration();
			}
		}
		return getMarketDataRetriever().getDurationFlexible(replication.getSecurity(), date, replication.getDurationFieldNameOverride(), required);
	}


	private BigDecimal getDelta(InvestmentReplicationSecurityAllocation replication, Date date) {
		// Delta is not supported - skip
		if (!replication.getReplicationType().isDeltaSupported()) {
			return null;
		}

		// Is Delta ever required
		BigDecimal delta = getMarketDataRetriever().getDeltaFlexible(replication.getSecurity(), date, false);
		if (delta != null) {
			if (isReplicationDeltaNegative(replication)) {
				delta = MathUtils.negate(delta);
			}
		}
		return delta;
	}


	private BigDecimal getFactor(InvestmentReplicationSecurityAllocation replication, Date date) {
		String factorChangeEventTypeName = InvestmentUtils.getFactorChangeEventTypeName(replication.getSecurity());
		if (factorChangeEventTypeName == null) {
			// factor is not applicable to security
			return null;
		}

		InvestmentSecurityEvent factorEvent = getInvestmentSecurityEventService().getInvestmentSecurityEventWithLatestExDate(replication.getSecurity().getId(), factorChangeEventTypeName, date);
		return (factorEvent != null && factorEvent.getAfterEventValue() != null) ? factorEvent.getAfterEventValue() : BigDecimal.ONE;
	}


	/**
	 * For Options, the Delta should be negated if:
	 * <br/>- the replication Option is Put and the target is Long
	 * <br/>- the replication Option is Call and the target is Short
	 */
	private boolean isReplicationDeltaNegative(InvestmentReplicationSecurityAllocation replication) {
		// Delta is Negated If Call Option and Short Target or Put Option and Long Target
		if (InvestmentUtils.isSecurityOfType(replication.getSecurity(), InvestmentType.OPTIONS)) {
			if (replication.getSecurity().getOptionType() != null) {
				// If PUT And Long Target
				boolean longTarget = MathUtils.isGreaterThan(replication.getTargetExposureAdjusted(), BigDecimal.ZERO);
				if (replication.getSecurity().isPutOption()) {
					if (longTarget) {
						return true;
					}
				}
				else if (!longTarget) {
					return true;
				}
			}
		}
		return false;
	}


	private BigDecimal getInterestRate(InvestmentReplicationSecurityAllocation replication, Date date) {
		// Interest Rate is not supported - skip
		if (!replication.getReplicationType().isInterestRateSupported()) {
			return null;
		}

		// Supported vs. Required - Required only if it's necessary to calculate contract value, otherwise just supported
		boolean required = isInterestRateRequired(replication);

		InvestmentSecurity security = replication.getSecurity();
		InvestmentSecurity currency = null;
		String exceptionPrefix = "Unable to determine interest rate for security [" + security.getLabel() + "]: ";

		if (PRICE_FIELD_UNDERLYING.equals(getPriceField(replication))) {
			if (required && security.getInstrument().getUnderlyingInstrument() == null) {
				throw new ValidationExceptionWithCause("InvestmentInstrument", security.getInstrument().getId(), exceptionPrefix + "Underlying Instrument is not populated.");
			}
			else if (security.getInstrument().getUnderlyingInstrument() != null) {
				currency = security.getInstrument().getUnderlyingInstrument().getTradingCurrency();
			}
			if (required && currency == null) {
				throw new ValidationExceptionWithCause("InvestmentInstrument", security.getInstrument().getId(), exceptionPrefix
						+ "Underlying Instrument's Currency Denomination is not populated.");
			}
		}
		if (currency == null) {
			if (security.getInstrument().getTradingCurrency() == null) {
				if (required) {
					throw new ValidationExceptionWithCause("InvestmentInstrument", security.getInstrument().getId(), exceptionPrefix + "Instrument's Currency Denomination is not populated.");
				}
				return null;
			}
			currency = security.getInstrument().getTradingCurrency();
		}

		Integer daysToMaturity = replication.getDaysToMaturity();
		if (daysToMaturity == null) {
			if (required) {
				throw new ValidationExceptionWithCause("InvestmentSecurity", security.getId(), exceptionPrefix + "Security End date is not populated.");
			}
			return null;
		}
		if (daysToMaturity < 0) {
			return BigDecimal.ZERO;
		}

		InvestmentInterestRateIndex index = getInvestmentRatesService().getInvestmentInterestRateIndexClosest(replication.getReplicationType().getInterestRateIndexGroup().getName(), currency.getId(),
				daysToMaturity, replication.getBalanceDate());
		if (index == null) {
			if (required) {
				throw new ValidationExceptionWithCause("InvestmentInterestRateIndexGroup", replication.getReplicationType().getInterestRateIndexGroup().getId(), exceptionPrefix
						+ "Cannot find any interest rate indices for group [" + replication.getReplicationType().getInterestRateIndexGroup().getName() + "] and Currency [" + currency.getName() + "].");
			}
			return null;
		}
		return getMarketDataRatesRetriever().getMarketDataInterestRateForDate(index, date, required);
	}


	private boolean isInterestRateRequired(InvestmentReplicationSecurityAllocation replication) {
		return (ADJUSTMENT_TYPE_SYNTHETIC.equals(getAdjustmentType()) && isAdjustmentApplied(replication));
	}


	protected boolean isAdjustmentApplied(InvestmentReplicationSecurityAllocation replication) {
		if (getAdjustmentType() != null) {
			if (replication != null) {
				if (!replication.getDoNotAdjustContractValue()) {
					if (CollectionUtils.isEmpty(getAdjustmentInvestmentTypeIdList()) || CollectionUtils.contains(getAdjustmentInvestmentTypeIdList(), replication.getSecurity().getInstrument().getHierarchy().getInvestmentType().getId())) {
						return true;
					}
				}
				return false;
			}
			return true;
		}
		return false;
	}


	private BigDecimal getSyntheticAdjustmentFactor(InvestmentReplicationSecurityAllocation replication) {
		// SyntheticAdjustmentFactor is not supported - skip
		if (!replication.getReplicationType().isSyntheticAdjustmentFactorSupported()) {
			return null;
		}

		// Validation is actually already done with interest rates - if that passes, then so does this
		// Default
		BigDecimal synAdj = BigDecimal.ONE;

		// SYNTHETIC ADJUSTMENT FACTOR FORMULA = 1 / (1+((Days to Maturity/365)*Interest Rate))
		// If interest rate is null don't calculate
		if (!MathUtils.isNullOrZero(replication.getInterestRate()) && replication.getDaysToMaturity() != null && replication.getDaysToMaturity() != 0) {
			synAdj = MathUtils.divide(BigDecimal.valueOf(replication.getDaysToMaturity()), BigDecimal.valueOf(365));
			synAdj = MathUtils.multiply(synAdj, MathUtils.divide(replication.getInterestRate(), MathUtils.BIG_DECIMAL_ONE_HUNDRED));
			synAdj = MathUtils.add(synAdj, BigDecimal.ONE);
			synAdj = MathUtils.divide(BigDecimal.ONE, synAdj);
			// ROUND SYNTHETIC ADJUSTMENT VALUES TO 4 DECIMAL PLACES
			synAdj = MathUtils.round(synAdj, 4);
		}
		return synAdj;
	}


	private BigDecimal getIndexRatio(InvestmentReplicationSecurityAllocation replication, Date date) {
		// Index Ratio is not supported - skip
		if (!replication.getReplicationType().isIndexRatioSupported()) {
			return null;
		}

		// Only applies to TIPS specific fields -- returns BigDecimal.ONE if security is not inflation adjusted.
		return getMarketDataService().getMarketDataIndexRatioForSecurity(replication.getSecurity().getId(), date, true);
	}


	private BigDecimal getSecurityDirtyPrice(InvestmentReplicationSecurityAllocation replication, boolean useTradePrice) {
		// Dirty Price is not supported - skip
		if (!replication.getReplicationType().isDirtyPriceSupported()) {
			return null;
		}
		BigDecimal price = getPropertyValue(replication, PRICE_FIELD, useTradePrice);
		Date date = (useTradePrice && replication.getTradeSecurityPriceDate() != null ? replication.getTradeSecurityPriceDate() : replication.getBalanceDate());
		// NOTE: Receivables are NOT currently included in the Dirty Price and thus not included in the Overlay Exposure
		// This is the reason the market value may differ from the overlay exposure.
		// For Bonds, we may want to include it, but for Stocks, we don't, so for now - not including it.
		// In the future we can add another field to the replication for receivables - needs to be split across asset classes if necessary
		// and be added to the actual exposure (overlay exposure) only.
		// Some dirty price calculators (IRS and CDS) will result in different values depending on if we hold long or short.
		// Difference is pretty small, so the first pass through actual/target will be zero
		// But later after contracts are allocated, this value is recalculated.
		// If actual and target are both zero, the dirty price calc will assume long position and use 1 billion as sizable face for estimating accrued interest in dirty price calc
		BigDecimal quantity = replication.getActualContractsAdjusted();
		if (MathUtils.isNullOrZero(quantity)) {
			quantity = replication.getTargetContractsAdjusted();
		}
		return getInvestmentCalculator().calculateDirtyPrice(replication.getSecurity(), quantity, price, date);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////                     Helper Methods                  //////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Can be overridden by specific calculators if they have replication specific logic
	 * i.e. Options Replication uses underlying for Calls, Strike Price for Puts
	 * <p>
	 * Because strike Price isn't saved, also sets the value for display on trading screen
	 */
	@SuppressWarnings("unused")
	protected String getPriceField(InvestmentReplicationSecurityAllocation replication) {
		// Default Implementation
		return PRICE_FIELD;
	}


	private BigDecimal getPropertyValue(InvestmentReplicationSecurityAllocation replication, String propertyName, boolean tradeValue) {
		BigDecimal result = null;
		// If trade value isn't there, then uses saved value for that property
		if (tradeValue) {
			result = (BigDecimal) BeanUtils.getPropertyValue(replication, "trade" + StringUtils.capitalize(propertyName));
		}
		if (result == null) {
			result = (BigDecimal) BeanUtils.getPropertyValue(replication, propertyName);
		}
		return result;
	}


	protected BigDecimal getPrice(InvestmentReplicationSecurityAllocation replication, boolean tradeValue) {
		String priceField = getPriceField(replication);
		if (StringUtils.isEmpty(priceField)) {
			priceField = PRICE_FIELD;
		}
		BigDecimal result = getPropertyValue(replication, priceField, tradeValue);
		if (result == null && !PRICE_FIELD.equals(priceField)) {
			result = getPropertyValue(replication, PRICE_FIELD, tradeValue);
		}
		return result;
	}


	@Override
	public boolean isDurationRequired(InvestmentReplicationSecurityAllocation replication) {
		return (ADJUSTMENT_TYPE_DURATION.equals(getAdjustmentType()) && isAdjustmentApplied(replication));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentRatesService getInvestmentRatesService() {
		return this.investmentRatesService;
	}


	public void setInvestmentRatesService(InvestmentRatesService investmentRatesService) {
		this.investmentRatesService = investmentRatesService;
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public MarketDataRatesRetriever getMarketDataRatesRetriever() {
		return this.marketDataRatesRetriever;
	}


	public void setMarketDataRatesRetriever(MarketDataRatesRetriever marketDataRatesRetriever) {
		this.marketDataRatesRetriever = marketDataRatesRetriever;
	}


	public MarketDataService getMarketDataService() {
		return this.marketDataService;
	}


	public void setMarketDataService(MarketDataService marketDataService) {
		this.marketDataService = marketDataService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public PortfolioReplicationHandler getPortfolioReplicationHandler() {
		return this.portfolioReplicationHandler;
	}


	public void setPortfolioReplicationHandler(PortfolioReplicationHandler portfolioReplicationHandler) {
		this.portfolioReplicationHandler = portfolioReplicationHandler;
	}


	public String getAdjustmentType() {
		return this.adjustmentType;
	}


	public void setAdjustmentType(String adjustmentType) {
		this.adjustmentType = adjustmentType;
	}


	public List<Short> getAdjustmentInvestmentTypeIdList() {
		return this.adjustmentInvestmentTypeIdList;
	}


	public void setAdjustmentInvestmentTypeIdList(List<Short> adjustmentInvestmentTypeIdList) {
		this.adjustmentInvestmentTypeIdList = adjustmentInvestmentTypeIdList;
	}
}
