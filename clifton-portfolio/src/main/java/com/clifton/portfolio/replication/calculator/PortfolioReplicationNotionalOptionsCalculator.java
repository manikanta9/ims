package com.clifton.portfolio.replication.calculator;


import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.replication.calculators.InvestmentReplicationSecurityAllocation;
import com.clifton.investment.setup.InvestmentType;

import java.math.BigDecimal;


/**
 * The <code>PortfolioReplicationNotionalOptionsCalculator</code> ...
 *
 * @author manderson
 */
public class PortfolioReplicationNotionalOptionsCalculator extends PortfolioReplicationNotionalCalculator {


	private boolean alwaysUseStrikePrice;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getPriceField(InvestmentReplicationSecurityAllocation replication) {
		if (replication != null && InvestmentUtils.isSecurityOfType(replication.getSecurity(), InvestmentType.OPTIONS)) {
			if (isAlwaysUseStrikePrice()) {
				return com.clifton.portfolio.replication.calculator.BasePortfolioReplicationCalculator.PRICE_FIELD_STRIKE;
			}
			else {
				if (replication.getSecurity().getOptionType() == null) {
					throw new ValidationExceptionWithCause("InvestmentSecurity", replication.getSecurity().getId(), "Missing option type value for security ["
							+ replication.getSecurity().getLabel() + "].");
				}
				// If PUT Use Strike Price
				if (replication.getSecurity().isPutOption()) {
					return com.clifton.portfolio.replication.calculator.BasePortfolioReplicationCalculator.PRICE_FIELD_STRIKE;
				}
			}
		}
		// Otherwise use underlying - uses security price if no underlying
		return com.clifton.portfolio.replication.calculator.BasePortfolioReplicationCalculator.PRICE_FIELD_UNDERLYING;
	}


	@Override
	protected BigDecimal getPrice(InvestmentReplicationSecurityAllocation replication, boolean tradeValue) {
		String priceField = getPriceField(replication);
		if ("strikePrice".equals(priceField)) {
			// Note Strike Price is the same - doesn't change each date
			return replication.getSecurity().getOptionStrikePrice();
		}
		return super.getPrice(replication, tradeValue);
	}


	@Override
	protected void setupReplicationTradingFields(InvestmentReplicationSecurityAllocation replication) {
		super.setupReplicationTradingFields(replication);
		// Need to manually set Strike Price?
		if ("strikePrice".equals(replication.getContractValuePriceField())) {
			// Note Strike Price is the same - doesn't change each date
			replication.setContractValuePrice(replication.getSecurity().getOptionStrikePrice());
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isAlwaysUseStrikePrice() {
		return this.alwaysUseStrikePrice;
	}


	public void setAlwaysUseStrikePrice(boolean alwaysUseStrikePrice) {
		this.alwaysUseStrikePrice = alwaysUseStrikePrice;
	}
}
