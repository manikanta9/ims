package com.clifton.portfolio.replication.calculator;


import com.clifton.core.util.MathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.replication.calculators.InvestmentReplicationCurrencyTypes;
import com.clifton.investment.replication.calculators.InvestmentReplicationSecurityAllocation;

import java.math.BigDecimal;


/**
 * The <code>PortfolioReplicationNotionalCalculator</code> is the Default, "Notional" calculator
 *
 * @author manderson
 */
public class PortfolioReplicationNotionalCalculator extends BasePortfolioReplicationCalculator {


	private boolean isUseNotionalCalculator;

	private String priceField;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal calculateContractValueImpl(InvestmentReplicationSecurityAllocation replication, @SuppressWarnings("unused") InvestmentReplicationCurrencyTypes currencyType, boolean tradeValue) {
		BigDecimal result;
		if (isUseNotionalCalculator()) {
			result = InvestmentCalculatorUtils.calculateContractValue(replication.getSecurity(), getPrice(replication, tradeValue), !replication.getReplicationType().isDoNotApplyExposureMultiplier());
		}
		else {
			result = calculateContractValueUsingPriceOnly(replication.getSecurity(), getPrice(replication, tradeValue), !replication.getReplicationType().isDoNotApplyExposureMultiplier());
		}
		if (isAdjustmentApplied(replication)) {
			if (BasePortfolioReplicationCalculator.ADJUSTMENT_TYPE_SYNTHETIC.equals(getAdjustmentType())) {
				// If Syn Adj Factor Use Formula: Notional /Synthetic Adjustment Factor
				result = MathUtils.divide(result, replication.getSyntheticAdjustmentFactor());
			}
			if (BasePortfolioReplicationCalculator.ADJUSTMENT_TYPE_DURATION.equals(getAdjustmentType())) {
				// If Duration Adjusted Use Formula: Notional * (Contract Duration/Benchmark Duration)
				result = MathUtils.multiply(result, MathUtils.divide(replication.getDuration(), replication.getBenchmarkDuration()));
			}
		}
		return result;
	}


	private BigDecimal calculateContractValueUsingPriceOnly(InvestmentSecurity security, BigDecimal price, boolean applyExposureMultiplier) {
		if (price == null) {
			return BigDecimal.ZERO;
		}

		BigDecimal result = MathUtils.multiply(price, security.getPriceMultiplier());
		if (applyExposureMultiplier) {
			result = MathUtils.multiply(result, security.getInstrument().getExposureMultiplier());
		}
		return result;
	}


	@Override
	protected String getPriceField(InvestmentReplicationSecurityAllocation replication) {
		return getPriceField();
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                Getter and Setter Methods                   //////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isUseNotionalCalculator() {
		return this.isUseNotionalCalculator;
	}


	public void setUseNotionalCalculator(boolean useNotionalCalculator) {
		this.isUseNotionalCalculator = useNotionalCalculator;
	}


	public String getPriceField() {
		return this.priceField;
	}


	public void setPriceField(String priceField) {
		this.priceField = priceField;
	}
}
