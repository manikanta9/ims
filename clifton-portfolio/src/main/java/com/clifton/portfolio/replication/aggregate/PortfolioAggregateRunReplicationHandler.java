package com.clifton.portfolio.replication.aggregate;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;

import java.util.List;


/**
 * @author mitchellf
 */
public interface PortfolioAggregateRunReplicationHandler<D extends BasePortfolioRunReplication> {

	@SecureMethod(dtoClass = PortfolioRun.class)
	public List<PortfolioAggregateSecurityReplication<D>> getPortfolioAggregateRunReplication(List<D> replications);
}
