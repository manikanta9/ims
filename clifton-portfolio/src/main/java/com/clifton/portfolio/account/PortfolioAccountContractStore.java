package com.clifton.portfolio.account;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.investment.account.relationship.cache.InvestmentAccountRelationshipConfig;
import com.clifton.investment.account.relationship.cache.InvestmentAccountRelationshipConfig.InvestmentAccountRelationshipAssetClassConfig;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.replication.calculators.InvestmentReplicationSecurityAllocation;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure;
import com.clifton.trade.Trade;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>PortfolioAccountContractStore</code> contains positions
 * for a given account and all of its sub accounts broken out appropriately based on the
 * {@link InvestmentAccountRelationshipConfig} so we know which asset classes each contract
 * applies to.
 *
 * @author Mary Anderson
 */
@NonPersistentObject
public class PortfolioAccountContractStore {

	// The Relationship Configuration used to assign contracts
	private final InvestmentAccountRelationshipConfig config;

	// The "type" of contracts this store holds, i.e. Actual (= Contracts Posted on Balance Date of a Run), Current (= Contracts Posted on Next Business Day of Run), & Pending (= Current Non Closed Trades, i.e. not Posted yet)
	private final PortfolioAccountContractStoreTypes type;

	// Explicit Contract Splitting Defined for the Account
	private List<InvestmentAccountAssetClassPositionAllocation> assetClassPositionAllocationList;

	// Pending/Current Contract Splitting Defined for the Trades/Virtual Trades
	private List<TradeAssetClassPositionAllocation> tradeAssetClassPositionAllocationList;

	// The data source to look up exchange rates for this account.  This is determined by the issuing company for the M2M relationship on the main account.
	// If it is blank, or doesn't exist for the datasource, the default (Goldman Sachs) datasource rates are used
	private String exchangeRateDataSourceName;

	// List of Positions/Trades (Security & Amount) and which asset classes it applies to
	private final Map<String, PortfolioAccountContractSecurityStore> securityStoreMap = new HashMap<>();

	// Map of Security ID to Quantity for every Security there is a position for (regardless of client/holding account/asset class)
	// Cached so it can be easily used for Compliance Check Warning & Currency Positions Lookup
//	private final Map<Integer, BigDecimal> securityQuantityMap = new HashMap<>();
	private final Map<Integer, Map<Integer, SecurityPositionQuantity>> securityQuantityByClientAccountMap = new HashMap<>();
	// The above holds all, this holds just the quantity that are collateral
	private final Map<Integer, Map<Integer, SecurityPositionQuantity>> securityCollateralQuantityByClientAccountMap = new HashMap<>();

	private final Map<Integer, SecurityPositionQuantity> totalQuantityByClientAccountMap = new HashMap<>();

	// Used for cases when sub-account mappings include security groups - contains security Id to asset class id mappings for each security
	// Key is the Account ID
	private final MultiValueMap<Integer, Integer> accountSecurityIdApplyToAllListMap = new MultiValueHashMap<>(false);
	private final Map<Integer, MultiValueMap<Integer, Integer>> accountSecurityIdAssetClassIdMap = new HashMap<>();

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Each Position/Trade that is added is broken out into Meta Data defining the security, amount, and which asset classes it applies to
	 * Also holds the Trade List when Trades are added (used for looking up pending trades) for easier retrieval of the Trade list for an asset class replication
	 */
	public static final class PortfolioAccountContractSecurityStore {

		private final Integer clientAccountId;
		private final Integer securityId;
		private final Set<Integer> assetClassIdSet;
		private final boolean applyToAll;
		private SecurityPositionQuantity securityPositionQuantity;

		private final List<Trade> tradeList = new ArrayList<>();
		private final List<AccountingPosition> positionList = new ArrayList<>();
		private final List<AccountingPositionTransferDetail> transferDetailList = new ArrayList<>();
		private final List<AccountingBalanceValue> currencyBalanceList = new ArrayList<>();
		private final String uniqueKey;


		public PortfolioAccountContractSecurityStore(Integer clientAccountId, Integer securityId, List<Integer> assetClassIdList, boolean applyToAll) {
			this.clientAccountId = clientAccountId;
			this.securityId = securityId;
			this.assetClassIdSet = normalizeAssetClassList(assetClassIdList);
			this.applyToAll = applyToAll;
			this.uniqueKey = createUniqueKey(); // create unique key once so it is not regenerated during comparisons
		}


		private Set<Integer> normalizeAssetClassList(List<Integer> assetClassIdList) {
			Set<Integer> assetClassSet = new LinkedHashSet<>();
			if (!CollectionUtils.isEmpty(assetClassIdList)) {
				Collections.sort(assetClassIdList);
				assetClassSet.addAll(assetClassIdList);
			}
			return assetClassSet;
		}


		private void addPositionQuantity(SecurityPositionQuantity quantity) {
			this.securityPositionQuantity = this.securityPositionQuantity == null ? quantity : this.securityPositionQuantity.addQuantity(quantity);
		}


		private String createUniqueKey() {
			StringBuilder key = new StringBuilder(16);
			key.append(getClientAccountId());
			key.append("_");
			key.append(getSecurityId());
			if (isApplyToAll()) {
				key.append("_ALL");
			}
			else if (getAssetClassIdSet() != null) {
				for (Integer assetClassId : getAssetClassIdSet()) {
					key.append("_").append(assetClassId);
				}
			}
			return key.toString();
		}


		public String getUniqueKey() {
			return this.uniqueKey;
		}


		public void addTrade(Trade trade) {
			this.tradeList.add(trade);
		}


		public void addPosition(AccountingPosition position) {
			this.positionList.add(position);
		}


		public void addTransferDetail(AccountingPositionTransferDetail transferDetail) {
			this.transferDetailList.add(transferDetail);
		}


		public void addCurrencyBalance(AccountingBalanceValue balanceValue) {
			this.currencyBalanceList.add(balanceValue);
		}


		public boolean isApplyToInvestmentAccountAssetClass(Integer accountAssetClassId) {
			return isApplyToAll() || CollectionUtils.contains(getAssetClassIdSet(), accountAssetClassId);
		}

		/////////////////////////////////////////////////////


		public Integer getSecurityId() {
			return this.securityId;
		}


		public Set<Integer> getAssetClassIdSet() {
			return this.assetClassIdSet;
		}


		public boolean isApplyToAll() {
			return this.applyToAll;
		}


		public SecurityPositionQuantity getPositionQuantity() {
			return this.securityPositionQuantity;
		}


		public List<Trade> getTradeList() {
			return this.tradeList;
		}


		public List<AccountingPosition> getPositionList() {
			return this.positionList;
		}


		public List<AccountingPositionTransferDetail> getTransferDetailList() {
			return this.transferDetailList;
		}


		public Integer getClientAccountId() {
			return this.clientAccountId;
		}


		public List<AccountingBalanceValue> getCurrencyBalanceList() {
			return this.currencyBalanceList;
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public PortfolioAccountContractStore(InvestmentAccountRelationshipConfig config, PortfolioAccountContractStoreTypes type) {
		this.config = config;
		this.type = type;
	}


	/*
	Iterates the map of security quantities for each account and summarizes them
	 */
	public Map<Integer, SecurityPositionQuantity> getSecurityQuantityMap() {
		Map<Integer, SecurityPositionQuantity> securityQuantityMap = new HashMap<>();
		for (Map<Integer, SecurityPositionQuantity> accountSecurityQuantityMap : CollectionUtils.getIterable(getSecurityQuantityByClientAccountMap().values())) {
			for (Map.Entry<Integer, SecurityPositionQuantity> accountSecurityQuantityEntry : CollectionUtils.getIterable(accountSecurityQuantityMap.entrySet())) {
				addPositionQuantityToMap(securityQuantityMap, accountSecurityQuantityEntry.getKey(), accountSecurityQuantityEntry.getValue());
			}
		}
		return securityQuantityMap;
	}


	/*
	Iterates the map of collateral security quantities for each account and summarizes them
	 */
	public Map<Integer, SecurityPositionQuantity> getSecurityCollateralQuantityMap() {
		Map<Integer, SecurityPositionQuantity> securityCollateralQuantityMap = new HashMap<>();
		for (Map<Integer, SecurityPositionQuantity> accountSecurityCollateralQuantityMap : CollectionUtils.getIterable(getSecurityCollateralQuantityByClientAccountMap().values())) {
			for (Map.Entry<Integer, SecurityPositionQuantity> accountSecurityCollateralQuantityEntry : CollectionUtils.getIterable(accountSecurityCollateralQuantityMap.entrySet())) {
				addPositionQuantityToMap(securityCollateralQuantityMap, accountSecurityCollateralQuantityEntry.getKey(), accountSecurityCollateralQuantityEntry.getValue());
			}
		}
		return securityCollateralQuantityMap;
	}


	public SecurityPositionQuantity getTotalQuantity() {
		SecurityPositionQuantity totalQuantity = SecurityPositionQuantity.forQuantity(BigDecimal.ZERO);
		for (SecurityPositionQuantity accountSecurityPositionQuantity : CollectionUtils.getIterable(getTotalQuantityByClientAccountMap().values())) {
			totalQuantity.addQuantity(accountSecurityPositionQuantity);
		}
		return totalQuantity;
	}


	/**
	 * Returns a map of securities to quantities we hold positions/trades for a given asset class
	 * (Used to populate replication securities.
	 * <p/>
	 * NOTE: Calculates total amount actually held, so if we hold long & short for LMEs we can easily see that
	 * during replication security population and know to skip that security.
	 */
	public Map<Integer, SecurityPositionQuantity> getSecurityIdQuantityMapForAccountAssetClass(InvestmentAccountAssetClass iac) {
		Map<Integer, SecurityPositionQuantity> acSecurityQuantityMap = new HashMap<>();
		for (PortfolioAccountContractSecurityStore securityStore : this.securityStoreMap.values()) {
			SecurityPositionQuantity securityPositionQuantity = securityStore.getPositionQuantity();
			if (MathUtils.isNullOrZero(securityPositionQuantity.getQuantity())) {
				continue;
			}
			if (securityStore.isApplyToInvestmentAccountAssetClass(iac.getId())) {
				addPositionQuantityToMap(acSecurityQuantityMap, securityStore.getSecurityId(), securityPositionQuantity);
			}
		}
		return acSecurityQuantityMap;
	}


	/**
	 * Returns a map of accounts to quantities we hold positions/trades for a given security
	 * (Used to populate {@link PortfolioRunLDIExposure} securities with their corresponding accounts held in
	 * <p/>
	 * NOTE: Calculates total amount actually held, so if we hold long & short for LMEs we can easily see that
	 * during replication security population and know to skip that security.
	 */
	public Map<Integer, SecurityPositionQuantity> getClientAccountQuantityMapForSecurity(Integer securityId) {
		Map<Integer, SecurityPositionQuantity> accountQuantityMap = new HashMap<>();
		for (PortfolioAccountContractSecurityStore securityStore : this.securityStoreMap.values()) {
			SecurityPositionQuantity securityPositionQuantity = securityStore.getPositionQuantity();
			if (MathUtils.isNullOrZero(securityPositionQuantity.getQuantity())) {
				continue;
			}
			if (MathUtils.isEqual(securityId, securityStore.getSecurityId())) {
				addPositionQuantityToMap(accountQuantityMap, securityStore.getClientAccountId(), securityPositionQuantity);
			}
		}
		return accountQuantityMap;
	}


	public List<InvestmentAccountAssetClassPositionAllocation> getPositionAllocationListForSecurity(int securityId) {
		return BeanUtils.filter(getAssetClassPositionAllocationList(), allocation -> allocation.getSecurity().getId(), securityId);
	}


	public List<TradeAssetClassPositionAllocation> getTradePositionAllocationListForSecurity(int securityId) {
		return BeanUtils.filter(getTradeAssetClassPositionAllocationList(), allocation -> allocation.getSecurity().getId(), securityId);
	}


	/**
	 * Returns a list of {@link Trade}s that apply for an asset class and specified security
	 */
	public List<Trade> getSecurityTradeListForAccountAssetClass(InvestmentAccountAssetClass iac, Integer securityId) {
		List<Trade> trades = new ArrayList<>();
		for (PortfolioAccountContractSecurityStore securityStore : this.securityStoreMap.values()) {
			if (MathUtils.isEqual(securityId, securityStore.getSecurityId())) {
				if (iac == null || securityStore.isApplyToInvestmentAccountAssetClass(iac.getId())) {
					trades.addAll(securityStore.getTradeList());
				}
			}
		}
		return trades;
	}


	/**
	 * Returns a list of {@link AccountingPositionTransfer}s that apply for an asset class and specified security
	 */
	public List<AccountingPositionTransfer> getSecurityTransferListForAccountAssetClass(InvestmentAccountAssetClass iac, Integer securityId) {
		// Since the details hold security info, that is what we keep, but just want a transfer level list - return top level only
		// Track in a map so we don't duplicate
		Map<Integer, AccountingPositionTransfer> transferMap = new HashMap<>();
		for (PortfolioAccountContractSecurityStore securityStore : this.securityStoreMap.values()) {
			if (MathUtils.isEqual(securityId, securityStore.getSecurityId())) {
				if (iac == null || securityStore.isApplyToInvestmentAccountAssetClass(iac.getId())) {
					for (AccountingPositionTransferDetail det : CollectionUtils.getIterable(securityStore.getTransferDetailList())) {
						transferMap.putIfAbsent(det.getPositionTransfer().getId(), det.getPositionTransfer());
					}
				}
			}
		}
		return new ArrayList<>(transferMap.values());
	}


	/**
	 * Returns a list of {@link AccountingPosition}s that apply for an asset class and specified security
	 */
	public List<AccountingPosition> getSecurityAccountingPositionListForAccountAssetClass(InvestmentAccountAssetClass iac, Integer securityId) {
		List<AccountingPosition> pos = new ArrayList<>();
		for (PortfolioAccountContractSecurityStore securityStore : this.securityStoreMap.values()) {
			if (MathUtils.isEqual(securityId, securityStore.getSecurityId())) {
				if (securityStore.isApplyToInvestmentAccountAssetClass(iac.getId())) {
					pos.addAll(securityStore.getPositionList());
				}
			}
		}
		return pos;
	}


	/**
	 * Returns a list of {@link AccountingPosition}s that apply for the specified security
	 */
	public List<AccountingPosition> getSecurityAccountingPositionList(Integer securityId) {
		List<AccountingPosition> pos = new ArrayList<>();
		for (PortfolioAccountContractSecurityStore securityStore : this.securityStoreMap.values()) {
			if (MathUtils.isEqual(securityId, securityStore.getSecurityId())) {
				pos.addAll(securityStore.getPositionList());
			}
		}
		return pos;
	}


	private void add(Integer clientAccountId, InvestmentAccountRelationshipAssetClassConfig assetClassConfig, Integer securityId, SecurityPositionQuantity securityPositionQuantity, Trade trade,
	                 AccountingPosition position, AccountingPositionTransferDetail transferDetail, AccountingBalanceValue currencyBalance) {

		boolean addToList = true;
		boolean collateral = false;
		List<Integer> accountAssetClassIdList = assetClassConfig.getAccountAssetClassIdList();
		if (!assetClassConfig.isApplyToAllSecurityGroups()) {
			if (!isAccountSecurityIdApplyToAll(assetClassConfig.getClientAccountId(), securityId)) {
				accountAssetClassIdList = getAccountSecurityIdAssetClassIdMapping(assetClassConfig.getClientAccountId(), securityId);
				if (CollectionUtils.isEmpty(accountAssetClassIdList)) {
					addToList = false;
				}
			}
		}

		if (addToList) {
			PortfolioAccountContractSecurityStore secStore = getSecurityStore(clientAccountId, securityId, accountAssetClassIdList, CollectionUtils.isEmpty(accountAssetClassIdList));
			secStore.addPositionQuantity(securityPositionQuantity);

			if (trade != null) {
				secStore.addTrade(trade);
				collateral = trade.isCollateral();
			}
			if (position != null) {
				secStore.addPosition(position);
				collateral = position.isCollateral();
			}
			if (transferDetail != null) {
				secStore.addTransferDetail(transferDetail);
				collateral = transferDetail.getPositionTransfer().isCollateralTransfer();
			}
			if (currencyBalance != null) {
				secStore.addCurrencyBalance(currencyBalance);
				collateral = currencyBalance.isCollateral();
			}

			Map<Integer, SecurityPositionQuantity> securityQuantityMap = getSecurityQuantityByClientAccountMap().computeIfAbsent(clientAccountId, key -> new HashMap<>());
			addPositionQuantityToMap(securityQuantityMap, securityId, securityPositionQuantity);

			if (collateral) {
				Map<Integer, SecurityPositionQuantity> securityCollateralQuantityMap = getSecurityCollateralQuantityByClientAccountMap().computeIfAbsent(clientAccountId, key -> new HashMap<>());
				addPositionQuantityToMap(securityCollateralQuantityMap, securityId, securityPositionQuantity);
			}
			addPositionQuantityToMap(getTotalQuantityByClientAccountMap(), clientAccountId, securityPositionQuantity);
		}
	}


	private <T> void addPositionQuantityToMap(Map<T, SecurityPositionQuantity> map, T key, SecurityPositionQuantity securityPositionQuantity) {
		if (securityPositionQuantity == null || MathUtils.isNullOrZero(securityPositionQuantity.getQuantity())) {
			return;
		}
		map.compute(key, (k, currentPositionQuantity) -> currentPositionQuantity == null ? securityPositionQuantity : currentPositionQuantity.addQuantity(securityPositionQuantity));
	}


	private PortfolioAccountContractSecurityStore getSecurityStore(Integer clientAccountId, Integer securityId, List<Integer> assetClassIdList, boolean applyToAll) {
		PortfolioAccountContractSecurityStore secStore = new PortfolioAccountContractSecurityStore(clientAccountId, securityId, assetClassIdList, applyToAll);

		String key = secStore.getUniqueKey();
		return this.securityStoreMap.computeIfAbsent(key, k -> secStore);
	}


	public void addAccountSecurityIdApplyToAll(Integer clientAccountId, Integer securityId) {
		this.accountSecurityIdApplyToAllListMap.put(clientAccountId, securityId);
	}


	public boolean isAccountSecurityIdApplyToAll(Integer clientAccountId, Integer securityId) {
		List<Integer> securityIdList = this.accountSecurityIdApplyToAllListMap.get(clientAccountId);
		return CollectionUtils.contains(securityIdList, securityId);
	}


	public void addAccountSecurityIdAssetClassIdMapping(Integer clientAccountId, Integer securityId, Integer accountAssetClassId) {
		MultiValueMap<Integer, Integer> map = this.accountSecurityIdAssetClassIdMap.computeIfAbsent(clientAccountId, key -> new MultiValueHashMap<>(false));
		map.put(securityId, accountAssetClassId);
	}


	public List<Integer> getAccountSecurityIdAssetClassIdMapping(Integer clientAccountId, Integer securityId) {
		MultiValueMap<Integer, Integer> securityAssetClassListMap = this.accountSecurityIdAssetClassIdMap.get(clientAccountId);
		return securityAssetClassListMap == null ? null : securityAssetClassListMap.get(securityId);
	}


	/**
	 * Adds a list of {@link AccountingPosition}s and sets them to the appropriate asset classes
	 * Asset Classes are pre-filtered because they may also depend on the security group they belong to
	 */
	public void addAccountingPositionList(InvestmentAccountRelationshipAssetClassConfig assetClassConfig, List<AccountingPosition> positionList) {
		for (AccountingPosition position : CollectionUtils.getIterable(positionList)) {
			SecurityPositionQuantity securityPositionQuantity = InvestmentUtils.isFactorChangeSupported(position.getInvestmentSecurity())
					? SecurityPositionQuantity.forAdjustableQuantity(position.getRemainingQuantity(), position.getQuantity())
					: SecurityPositionQuantity.forQuantity(position.getRemainingQuantity());
			add(position.getClientInvestmentAccount().getId(), assetClassConfig, position.getInvestmentSecurity().getId(), securityPositionQuantity, null, position, null, null);
		}
	}


	/**
	 * Adds a list of {@link AccountingBalanceValue}s for CCY balance and sets them to the appropriate asset class config
	 */
	public void addAccountingBalanceValueList(InvestmentAccountRelationshipAssetClassConfig assetClassConfig, List<AccountingBalanceValue> currencyBalanceList) {
		for (AccountingBalanceValue value : CollectionUtils.getIterable(currencyBalanceList)) {
			add(value.getClientInvestmentAccount().getId(), assetClassConfig, value.getInvestmentSecurity().getId(), SecurityPositionQuantity.forQuantity(value.getLocalMarketValue()), null, null, null, value);
		}
	}


	/**
	 * Adds a list of {@link Trade}s and sets them to the appropriate asset class config
	 */
	public void addTradeList(InvestmentAccountRelationshipAssetClassConfig assetClassConfig, List<Trade> tradeList) {
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			BigDecimal amount = trade.getQuantityIntended();
			if (trade.getInvestmentSecurity().isCurrency()) {
				amount = trade.getAccountingNotional();
			}
			SecurityPositionQuantity securityPositionQuantity;
			if (InvestmentUtils.isFactorChangeSupported(trade.getInvestmentSecurity())) {
				securityPositionQuantity = trade.isBuy() ? SecurityPositionQuantity.forAdjustableQuantity(amount, trade.getOriginalFace())
						: SecurityPositionQuantity.forAdjustableQuantity(MathUtils.negate(amount), MathUtils.negate(trade.getOriginalFace()));
			}
			else {
				securityPositionQuantity = SecurityPositionQuantity.forQuantity(trade.isBuy() ? amount : MathUtils.negate(amount));
			}
			add(trade.getClientInvestmentAccount().getId(), assetClassConfig, trade.getInvestmentSecurity().getId(), securityPositionQuantity, trade, null, null, null);
		}
	}


	/**
	 * Adds a list of {@link AccountingPositionTransfer}s and sets them to the appropriate asset class config.
	 * Each transfer is tested for "from" and "to" account equality to the {@link InvestmentAccountRelationshipAssetClassConfig#getClientAccountId()}.
	 * <br/>- If the transfer matches "from," each detail is added with the quantity negated (selling security).
	 * <br/>- If the transfer matches "to," each detail is added with the quantity (buying security).
	 * <br/>- If the transfer matches both "from" and "to," both of the above will be applied with the from applied first.
	 */
	public void addAccountingPositionTransferList(InvestmentAccountRelationshipAssetClassConfig assetClassConfig, List<AccountingPositionTransfer> positionTransferList) {
		for (AccountingPositionTransfer transfer : CollectionUtils.getIterable(positionTransferList)) {
			for (AccountingPositionTransferDetail detail : CollectionUtils.getIterable(transfer.getDetailList())) {
				if (transfer.getFromClientInvestmentAccount() != null && transfer.getFromClientInvestmentAccount().getId().equals(assetClassConfig.getClientAccountId())) {
					add(transfer.getFromClientInvestmentAccount().getId(), assetClassConfig, detail.getSecurity().getId(), SecurityPositionQuantity.forQuantity(MathUtils.negate(detail.getQuantity())), null, null, detail, null);
				}
				if (transfer.getToClientInvestmentAccount() != null && transfer.getToClientInvestmentAccount().getId().equals(assetClassConfig.getClientAccountId())) {
					add(transfer.getToClientInvestmentAccount().getId(), assetClassConfig, detail.getSecurity().getId(), SecurityPositionQuantity.forQuantity(detail.getQuantity()), null, null, detail, null);
				}
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	public static final class SecurityPositionQuantity {

		private final BigDecimal quantity;
		private final BigDecimal unadjustedQuantity;
		private final boolean factorAdjustable;


		SecurityPositionQuantity(BigDecimal quantity, BigDecimal unadjustedQuantity, boolean adjustable) {
			this.quantity = quantity;
			this.unadjustedQuantity = unadjustedQuantity;
			this.factorAdjustable = adjustable;
		}


		static SecurityPositionQuantity forQuantity(BigDecimal quantity) {
			return new SecurityPositionQuantity(quantity, quantity, false);
		}


		static SecurityPositionQuantity forAdjustableQuantity(BigDecimal quantity, BigDecimal unadjustedQuantity) {
			return new SecurityPositionQuantity(quantity, unadjustedQuantity, true);
		}


		SecurityPositionQuantity addQuantity(SecurityPositionQuantity securityPositionQuantity) {
			if (securityPositionQuantity == null) {
				return this;
			}
			BigDecimal newQuantity = MathUtils.add(getQuantity(), securityPositionQuantity.getQuantity());
			BigDecimal newUnadjustedQuantity = MathUtils.add(getUnadjustedQuantity(), securityPositionQuantity.getUnadjustedQuantity());
			return new SecurityPositionQuantity(newQuantity, newUnadjustedQuantity, isFactorAdjustable() || securityPositionQuantity.isFactorAdjustable());
		}


		SecurityPositionQuantity subtractQuantity(SecurityPositionQuantity securityPositionQuantity) {
			if (securityPositionQuantity == null) {
				return this;
			}
			BigDecimal newQuantity = MathUtils.subtract(getQuantity(), securityPositionQuantity.getQuantity());
			BigDecimal newUnadjustedQuantity = MathUtils.subtract(getUnadjustedQuantity(), securityPositionQuantity.getUnadjustedQuantity());
			return new SecurityPositionQuantity(newQuantity, newUnadjustedQuantity, isFactorAdjustable() || securityPositionQuantity.isFactorAdjustable());
		}


		public BigDecimal getQuantity() {
			return this.quantity;
		}


		public BigDecimal getUnadjustedQuantity() {
			return this.unadjustedQuantity;
		}


		public boolean isFactorAdjustable() {
			return this.factorAdjustable;
		}


		/**
		 * Returns the quantity for this security store considering the replications' type(s) for whether unadjusted quantity should be returned.
		 */
		public <T extends InvestmentReplicationSecurityAllocation> BigDecimal getQuantityForReplicationList(List<T> replicationList) {
			BigDecimal totalQuantity = getQuantity();
			if (isFactorAdjustable()) {
				Set<Boolean> applyFactorSet = CollectionUtils.getStream(replicationList)
						.map(replication -> replication.getReplicationType().isApplyCurrentFactor())
						.collect(Collectors.toSet());
				ValidationUtils.assertFalse(applyFactorSet.size() > 1, "Unable to have replication types for a security with a factor event type with offsetting factor application settings.");
				if (BooleanUtils.isTrue(CollectionUtils.getFirstElement(applyFactorSet))) {
					totalQuantity = getUnadjustedQuantity();
				}
			}
			return totalQuantity;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipConfig getConfig() {
		return this.config;
	}


	public Collection<PortfolioAccountContractSecurityStore> getSecurityStoreCollection() {
		return this.securityStoreMap.values();
	}


	public Map<Integer, Map<Integer, SecurityPositionQuantity>> getSecurityQuantityByClientAccountMap() {
		return this.securityQuantityByClientAccountMap;
	}


	public Map<Integer, Map<Integer, SecurityPositionQuantity>> getSecurityCollateralQuantityByClientAccountMap() {
		return this.securityCollateralQuantityByClientAccountMap;
	}


	public Map<Integer, SecurityPositionQuantity> getTotalQuantityByClientAccountMap() {
		return this.totalQuantityByClientAccountMap;
	}


	public PortfolioAccountContractStoreTypes getType() {
		return this.type;
	}


	public String getExchangeRateDataSourceName() {
		return this.exchangeRateDataSourceName;
	}


	public void setExchangeRateDataSourceName(String exchangeRateDataSourceName) {
		this.exchangeRateDataSourceName = exchangeRateDataSourceName;
	}


	public List<InvestmentAccountAssetClassPositionAllocation> getAssetClassPositionAllocationList() {
		return this.assetClassPositionAllocationList;
	}


	public void setAssetClassPositionAllocationList(List<InvestmentAccountAssetClassPositionAllocation> assetClassPositionAllocationList) {
		this.assetClassPositionAllocationList = assetClassPositionAllocationList;
	}


	public List<TradeAssetClassPositionAllocation> getTradeAssetClassPositionAllocationList() {
		return this.tradeAssetClassPositionAllocationList;
	}


	public void setTradeAssetClassPositionAllocationList(List<TradeAssetClassPositionAllocation> tradeAssetClassPositionAllocationList) {
		this.tradeAssetClassPositionAllocationList = tradeAssetClassPositionAllocationList;
	}
}
