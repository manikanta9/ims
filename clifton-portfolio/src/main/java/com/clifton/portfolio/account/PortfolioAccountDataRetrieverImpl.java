package com.clifton.portfolio.account;


import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocationService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.relationship.cache.InvestmentAccountRelationshipConfig.InvestmentAccountRelationshipAssetClassConfig;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocationSearchForm;
import com.clifton.trade.assetclass.TradeAssetClassService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>PortfolioAccountDataRetrieverImpl</code> ...
 *
 * @author manderson
 */
@Component
public class PortfolioAccountDataRetrieverImpl implements PortfolioAccountDataRetriever {

	/**
	 * PIOS Uses separate Custom Field Group, so will leave existing report field name as is since
	 * it's referenced in a few different places.  Once re-factored to use one group for "Portfolio Setup"
	 * can re-name at that time, since all references will need to be updated then as well for the group.
	 */
	public static final String FIELD_PIOS_REPORT = "PIOS Report";
	public static final String FIELD_PORTFOLIO_REPORT = "Portfolio Report";
	private AccountingValuationService accountingValuationService;
	private AccountingPositionService accountingPositionService;
	private AccountingPositionTransferService accountingPositionTransferService;
	private InvestmentAccountAssetClassPositionAllocationService investmentAccountAssetClassPositionAllocationService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;

	////////////////////////////////////////////////////////////////////////////

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private SystemColumnValueHandler systemColumnValueHandler;
	private TradeAssetClassService tradeAssetClassService;
	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////
	///////    Client Account Portfolio Custom Field Value Methods       ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Object getPortfolioAccountCustomFieldValue(InvestmentAccount account, String fieldName) {
		String groupName = InvestmentAccount.CLIENT_ACCOUNT_PORTFOLIO_SETUP_COLUMN_GROUP_NAME;
		if (account.getServiceProcessingType() != null && ServiceProcessingTypes.PORTFOLIO_RUNS == account.getServiceProcessingType().getProcessingType()) {
			groupName = InvestmentAccount.CLIENT_ACCOUNT_PIOS_PROCESSING_COLUMN_GROUP_NAME;
		}
		return getSystemColumnValueHandler().getSystemColumnValueForEntity(account, groupName, fieldName, true);
	}


	@Override
	public Object getPortfolioReportCustomFieldValue(InvestmentAccount account, ServiceProcessingTypes processingType) {
		ServiceProcessingTypes serviceProcessingType = ObjectUtils.coalesce(processingType, account.getServiceProcessingType().getProcessingType());
		return (serviceProcessingType == ServiceProcessingTypes.PORTFOLIO_RUNS)
				? getSystemColumnValueHandler().getSystemColumnValueForEntity(account, InvestmentAccount.CLIENT_ACCOUNT_PIOS_PROCESSING_COLUMN_GROUP_NAME, FIELD_PIOS_REPORT, true)
				: getPortfolioReportCustomFieldValueWithLinkedValue(account, FIELD_PORTFOLIO_REPORT, serviceProcessingType.name());
	}


	private Object getPortfolioReportCustomFieldValueWithLinkedValue(InvestmentAccount account, String columnName, String linkedValue) {
		return getSystemColumnValueHandler().getSystemColumnValueForEntityWithLinkedValue(account, InvestmentAccount.CLIENT_ACCOUNT_PORTFOLIO_SETUP_COLUMN_GROUP_NAME, columnName, linkedValue, true);
	}

	////////////////////////////////////////////////////////////////////////////
	/////////           Client Account Relationship Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InvestmentAccountRelationship> getTradingFuturesBrokerAccountRelationshipList(int investmentAccountId, Date activeOnDate) {
		return getInvestmentAccountRelationshipListImpl(investmentAccountId, InvestmentAccountRelationshipPurpose.TRADING_FUTURES_PURPOSE_NAME, activeOnDate);
	}


	@Override
	public List<InvestmentAccountRelationship> getCliftonSubAccountAccountRelationshipList(int investmentAccountId, Date activeOnDate) {
		return getInvestmentAccountRelationshipListImpl(investmentAccountId, InvestmentAccountRelationshipPurpose.CLIFTON_SUB_ACCOUNT_PURPOSE_NAME, activeOnDate);
	}


	@Override
	public List<InvestmentAccountRelationship> getInvestmentAccountRelationshipList(int investmentAccountId, Date activeOnDate) {
		return getInvestmentAccountRelationshipListImpl(investmentAccountId, null, activeOnDate);
	}


	private List<InvestmentAccountRelationship> getInvestmentAccountRelationshipListImpl(int investmentAccountId, String purpose, Date activeOnDate) {
		if (activeOnDate == null) {
			activeOnDate = new Date();
		}
		return getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipListForPurpose(investmentAccountId, purpose, activeOnDate, true);
	}


	@Override
	public InvestmentAccount getMainAccountForCliftonSubAccount(InvestmentAccount subAccount, boolean throwExceptionIfMissingOrMoreThanOne, Date activeOnDate) {
		List<InvestmentAccountRelationship> relationshipList = getCliftonSubAccountAccountRelationshipList(subAccount.getId(), activeOnDate);
		// Make sure this account is the child in the relationship
		relationshipList = BeanUtils.filter(relationshipList, relationship -> relationship.getReferenceTwo().getId().equals(subAccount.getId()));

		// No Account Relationships
		if (CollectionUtils.isEmpty(relationshipList)) {
			if (throwExceptionIfMissingOrMoreThanOne) {
				throw new ValidationException(
						"Unable to use parent account for portfolio return calculations, because there is no main account defined by a Clifton Sub-Account relationship for related account ["
								+ subAccount.getNumber() + "]");
			}
			return null;
		}
		// One Account Relationship
		if (relationshipList.size() == 1) {
			return relationshipList.get(0).getReferenceOne();
		}

		// Multiple Account Relationships
		// The same account can be used across asset classes - so check unique accounts, not relationships
		InvestmentAccount mainAccount = null;
		for (InvestmentAccountRelationship rel : relationshipList) {
			if (mainAccount == null) {
				mainAccount = rel.getReferenceOne();
			}
			// More than one found
			else if (!mainAccount.equals(rel.getReferenceOne())) {
				if (throwExceptionIfMissingOrMoreThanOne) {
					throw new ValidationException(
							"Unable to use parent account for portfolio return calculations, because there are multiple main accounts defined by a Clifton Sub-Account relationship for related account ["
									+ subAccount.getNumber() + "]. Two of these parent accounts are: [" + rel.getReferenceOne().getNumber() + " and " + mainAccount.getNumber() + "].");
				}
				mainAccount = null;
				break;
			}
		}
		return mainAccount;
	}

	////////////////////////////////////////////////////////////////////////////
	///////         Client Account Positions/Trade Methods                //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public PortfolioAccountContractStore getPortfolioAccountContractStorePending(PortfolioAccountContractStore current, InvestmentAccount account, Integer securityId) {
		// Pass and use the "Current" one for creating/populated pending configuration information that is always the same
		// instead of looking it up again.
		PortfolioAccountContractStore store;
		if (current != null) {
			store = new PortfolioAccountContractStore(current.getConfig(), PortfolioAccountContractStoreTypes.PENDING);
			store.setExchangeRateDataSourceName(current.getExchangeRateDataSourceName());
			store.setAssetClassPositionAllocationList(current.getAssetClassPositionAllocationList());
			store.setTradeAssetClassPositionAllocationList(current.getTradeAssetClassPositionAllocationList());
		}
		else {
			store = new PortfolioAccountContractStore(getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipConfig(account.getId()), PortfolioAccountContractStoreTypes.PENDING);
			// For this case, not processing the contracts or splitting them, so don't need the fx data source or position allocation list
		}

		for (Integer accountId : CollectionUtils.getIterable(store.getConfig().getMainAccountIds())) {
			InvestmentAccountRelationshipAssetClassConfig assetClassConfig = store.getConfig().getConfig(accountId);
			if (assetClassConfig == null) {
				// Not sure how this could ever happen - if its a main account id, then there is a relationship mapping
				throw new ValidationException("Unable to add positions for Account ID [" + accountId + "]. There is no relationship defined for the client account.");
			}
			// Keep track of the security ids we've already determined for security groups/asset classes so we don't re-check them
			Set<Integer> securityIdsChecked = new HashSet<>();

			// Add Pending Trades
			addPendingTradeListToStore(assetClassConfig, securityId, store, securityIdsChecked);

			// ADD PENDING TRANSFERS i.e. Booking Date is NULL
			addAccountingPositionTransferListToStore(assetClassConfig, null, store, securityIdsChecked);
		}

		return store;
	}


	@Override
	@Transactional(readOnly = true)
	public PortfolioAccountContractStore getPortfolioAccountContractStoreActual(InvestmentAccount clientAccount, Date date, boolean current) {
		PortfolioAccountContractStore store = new PortfolioAccountContractStore(getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipConfig(clientAccount.getId()),
				current ? PortfolioAccountContractStoreTypes.CURRENT : PortfolioAccountContractStoreTypes.ACTUAL);
		store.setExchangeRateDataSourceName(getMarketDataExchangeRatesApiService().getExchangeRateDataSourceForClientAccount(clientAccount.toClientAccount()));
		// Only Need to Pull explicit definitions for the main account.  Sub-accounts are specifically assigned to an asset class
		// so they are not permitted to have explicit contract splitting defined since they'll never split
		store.setAssetClassPositionAllocationList(getInvestmentAccountAssetClassPositionAllocationService().getInvestmentAccountAssetClassPositionAllocationList(clientAccount.getId(), date));

		if (current) {
			// Add Pending/Newly Booked Trade Explicit Allocations if any
			// Includes all allocations that haven't been processed yet - we add here, which is also used for Pending store
			// Because trades may be booked, but allocation table not updated yet with new splits
			TradeAssetClassPositionAllocationSearchForm searchForm = new TradeAssetClassPositionAllocationSearchForm();
			searchForm.setAccountId(clientAccount.getId());
			searchForm.setProcessed(false);
			searchForm.setExcludeTradeWorkflowStateName(TradeService.TRADES_CANCELLED_STATE_NAME);
			store.setTradeAssetClassPositionAllocationList(getTradeAssetClassService().getTradeAssetClassPositionAllocationList(searchForm));
		}

		for (Integer accountId : CollectionUtils.getIterable(store.getConfig().getMainAccountIds())) {

			InvestmentAccountRelationshipAssetClassConfig assetClassConfig = store.getConfig().getConfig(accountId);
			if (assetClassConfig == null) {
				// Not sure how this could ever happen - if its a main account id, then there is a relationship mapping
				throw new ValidationException("Unable to add positions for Account ID [" + accountId + "]. There is no relationship defined for the client account.");
			}

			// Keep track of the security ids we've already determined for security groups/asset classes so we don't re-check them
			Set<Integer> securityIdsChecked = new HashSet<>();

			// Accounting Positions Booked to GL
			addAccountingPositionListToStore(assetClassConfig, date, store, securityIdsChecked);

			// CCY Balances
			addCurrencyAccountingBalanceValueListToStore(assetClassConfig, date, store, securityIdsChecked);

			// ADD TRANSFERS BOOKED WITH TRANSFER DATE AFTER DATE, BUT EXPOSURE DATE <= DATE
			addAccountingPositionTransferListToStore(assetClassConfig, date, store, securityIdsChecked);
		}
		return store;
	}


	private void addAccountingPositionListToStore(InvestmentAccountRelationshipAssetClassConfig assetClassConfig, Date date, PortfolioAccountContractStore store, Set<Integer> securityIdsChecked) {
		// Note: Need Populated list so if there are exceptions, can give full details that are meaningful to the user
		AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(date);
		command.setClientInvestmentAccountId(assetClassConfig.getClientAccountId());
		command.excludeReceivableGLAccounts();
		List<AccountingPosition> existingPositions = getAccountingPositionService().getAccountingPositionListUsingCommand(command);
		if (!CollectionUtils.isEmpty(existingPositions)) {
			// If Security Group Mappings - Build Maps/Lists needed to apply securities to correct asset classes
			if (!assetClassConfig.isApplyToAllSecurityGroups()) {
				buildSecurityToAccountAssetClassMap(store, securityIdsChecked, assetClassConfig, BeanUtils.getPropertyValues(existingPositions, position -> position.getInvestmentSecurity().getId(), Integer.class));
			}
			// Security Asset Class Mappings are set on the store, so it's easy to filter during add of each
			store.addAccountingPositionList(assetClassConfig, existingPositions);
		}
	}


	private void addCurrencyAccountingBalanceValueListToStore(InvestmentAccountRelationshipAssetClassConfig assetClassConfig, Date date, PortfolioAccountContractStore store,
	                                                          Set<Integer> securityIdsChecked) {
		AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onTransactionDate(date);
		searchForm.setClientInvestmentAccountId(assetClassConfig.getClientAccountId());
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CURRENCY_NON_CASH_ACCOUNTS);
		List<AccountingBalanceValue> ccyBalanceList = getAccountingValuationService().getAccountingBalanceValueList(searchForm);
		if (!CollectionUtils.isEmpty(ccyBalanceList)) {
			// If Security Group Mappings - Build Maps/Lists needed to apply securities to correct asset classes
			if (!assetClassConfig.isApplyToAllSecurityGroups()) {
				buildSecurityToAccountAssetClassMap(store, securityIdsChecked, assetClassConfig, BeanUtils.getPropertyValues(ccyBalanceList, balance -> balance.getInvestmentSecurity().getId(), Integer.class));
			}
			// Security Asset Class Mappings are set on the store, so it's easy to filter during add of each
			store.addAccountingBalanceValueList(assetClassConfig, ccyBalanceList);
		}
	}


	private void addPendingTradeListToStore(InvestmentAccountRelationshipAssetClassConfig assetClassConfig, Integer investmentSecurityId, PortfolioAccountContractStore store,
	                                        Set<Integer> securityIdsChecked) {
		List<Trade> tradeList = getTradeService().getTradePendingList(assetClassConfig.getClientAccountId(), null, investmentSecurityId);
		if (!CollectionUtils.isEmpty(tradeList)) {
			// If Security Group Mappings - Build Maps/Lists needed to apply securities to correct asset classes
			if (!assetClassConfig.isApplyToAllSecurityGroups()) {
				buildSecurityToAccountAssetClassMap(store, securityIdsChecked, assetClassConfig, BeanUtils.getPropertyValues(tradeList, trade -> trade.getInvestmentSecurity().getId(), Integer.class));
			}
			// Security Asset Class Mappings are set on the store, so it's easy to filter during add of each
			store.addTradeList(assetClassConfig, tradeList);
		}
	}


	/**
	 * Adds {@link AccountingPositionTransfer}s to the provided store. Transfers from the {@link InvestmentAccountRelationshipAssetClassConfig#getClientAccountId()}
	 * will be added first, followed by transfers to the client account.
	 */
	private void addAccountingPositionTransferListToStore(InvestmentAccountRelationshipAssetClassConfig assetClassConfig, Date date, PortfolioAccountContractStore store,
	                                                      Set<Integer> securityIdsChecked) {
		List<AccountingPositionTransfer> fullTransferList = getAccountingPositionTransferList(assetClassConfig.getClientAccountId(), date);
		if (!CollectionUtils.isEmpty(fullTransferList)) {
			// If Security Group Mappings - Build Maps/Lists needed to apply securities to correct asset classes
			if (!assetClassConfig.isApplyToAllSecurityGroups()) {
				// Security Ids are held on the details, collect unique security IDs for all transfers.
				Integer[] transferDetailSecurityIds = fullTransferList.stream()
						.map(AccountingPositionTransfer::getDetailList)
						.flatMap(CollectionUtils::getStream)
						.map(detail -> detail.getSecurity().getId())
						.distinct()
						.toArray(Integer[]::new);
				buildSecurityToAccountAssetClassMap(store, securityIdsChecked, assetClassConfig, transferDetailSecurityIds);
			}

			// Security Asset Class Mappings are set on the store, so it's easy to filter during add of each
			store.addAccountingPositionTransferList(assetClassConfig, fullTransferList);
		}
	}


	/**
	 * Returns fully populated AccountingPositionTransfer(s) as of given date to or from the client {@link InvestmentAccount} ID.
	 * If date is null returns all unbooked transfers, i.e. pending transfers.  If date is not null, returns all booked with
	 * transfer date after date (otherwise would be included in positions (GL)) and exposure date <= date.
	 */
	private List<AccountingPositionTransfer> getAccountingPositionTransferList(Integer clientInvestmentAccountId, Date date) {
		AccountingPositionTransferSearchForm searchForm = new AccountingPositionTransferSearchForm();
		searchForm.setFromOrToClientInvestmentAccountId(clientInvestmentAccountId);
		if (date == null) {
			// Pending, i.e. BOOKING DATE NULL
			searchForm.setJournalNotBooked(true);
		}
		else {
			// BOOKING DATE NOT NULL, TRANSFER DATE AFTER DATE (OTHERWISE WOULD BE IN GL AS OF DATE) AND EXPOSURE DATE <= DATE
			searchForm.setJournalBooked(true);
			searchForm.setMaxExposureDate(date);
			searchForm.setMinTransactionDate(DateUtils.addDays(date, 1));
		}

		// Pull again from DB so we get fully populated detail list
		List<AccountingPositionTransfer> transferList = getAccountingPositionTransferService().getAccountingPositionTransferList(searchForm);
		for (int i = 0; i < CollectionUtils.getSize(transferList); i++) {
			AccountingPositionTransfer t = transferList.get(i);
			transferList.set(i, getAccountingPositionTransferService().getAccountingPositionTransfer(t.getId()));
		}
		return transferList;
	}


	private void buildSecurityToAccountAssetClassMap(PortfolioAccountContractStore store, Set<Integer> securityIdsChecked, InvestmentAccountRelationshipAssetClassConfig assetClassConfig,
	                                                 Integer[] securityIds) {
		// Map Security Ids to the Applicable Account Asset Classes they belong to
		if (securityIds != null) {
			for (Integer secId : securityIds) {
				if (secId != null && securityIdsChecked.add(secId)) {
					// Go through list of group keys.
					assetClassConfig.getSecurityGroupAccountAssetClassMap().entrySet().forEach(securityGroupEntry -> {
						Short grpId = securityGroupEntry.getKey();
						// If applies to all groups or security is in the group - add asset class mapping
						if (grpId.equals((short) 0) || getInvestmentSecurityGroupService().isInvestmentSecurityInSecurityGroup(secId, grpId)) {
							for (Integer acId : securityGroupEntry.getValue()) {
								if (acId.equals(0)) {
									store.addAccountSecurityIdApplyToAll(assetClassConfig.getClientAccountId(), secId);
									// If adding to all - don't bother checking other groups
									break;
								}
								store.addAccountSecurityIdAssetClassIdMapping(assetClassConfig.getClientAccountId(), secId, acId);
							}
						}
					});
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	///////      Client Account Object Retrievers from Context           ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T> T getClientAccountObjectFromContextMap(InvestmentAccount clientAccount, ServiceProcessingTypes processingType, Map<String, T> contextObjectMap, String objectDescription) {
		ValidationUtils.assertNotNull(clientAccount, "Client Account is missing.");
		T object = null;

		if (processingType == null && clientAccount.getServiceProcessingType() != null) {
			processingType = clientAccount.getServiceProcessingType().getProcessingType();
		}
		if (processingType != null) {
			object = contextObjectMap.get(processingType.name());
		}
		if (object == null) {
			object = contextObjectMap.get("DEFAULT");
		}
		if (object == null) {
			if (processingType == null) {
				throw new ValidationException("There is no default " + objectDescription + " defined.");
			}
			throw new ValidationException("There is no " + objectDescription + " defined for service processing type [" + processingType.name() + "], and no default defined.");
		}
		return object;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}


	public InvestmentAccountAssetClassPositionAllocationService getInvestmentAccountAssetClassPositionAllocationService() {
		return this.investmentAccountAssetClassPositionAllocationService;
	}


	public void setInvestmentAccountAssetClassPositionAllocationService(InvestmentAccountAssetClassPositionAllocationService investmentAccountAssetClassPositionAllocationService) {
		this.investmentAccountAssetClassPositionAllocationService = investmentAccountAssetClassPositionAllocationService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public TradeAssetClassService getTradeAssetClassService() {
		return this.tradeAssetClassService;
	}


	public void setTradeAssetClassService(TradeAssetClassService tradeAssetClassService) {
		this.tradeAssetClassService = tradeAssetClassService;
	}
}
