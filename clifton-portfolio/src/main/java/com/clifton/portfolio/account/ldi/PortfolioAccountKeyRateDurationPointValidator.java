package com.clifton.portfolio.account.ldi;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.marketdata.field.MarketDataFieldGroup;
import com.clifton.marketdata.field.MarketDataFieldService;
import org.springframework.stereotype.Component;


/**
 * The <code>PortfolioAccountKeyRateDurationPointValidator</code> validates on saves
 * that the selected MarketDataField belongs to group "Key Rate Duration Points"
 *
 * @author manderson
 */
@Component
public class PortfolioAccountKeyRateDurationPointValidator extends SelfRegisteringDaoValidator<PortfolioAccountKeyRateDurationPoint> {

	private MarketDataFieldService marketDataFieldService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate(PortfolioAccountKeyRateDurationPoint bean, DaoEventTypes config) throws ValidationException {
		if (config.isInsert() || config.isUpdate()) {
			ValidationUtils.assertNotNull(bean.getMarketDataField(), "Key Rate Duration Point Market Data Field selection is required.");
			// Note: Only Check the Group if the KRD Point is active - if we choose to remove some points from being available inactive ones can still point to them
			if (!bean.isInactive()) {
				ValidationUtils.assertTrue(getMarketDataFieldService().isMarketDataFieldInGroup(MarketDataFieldGroup.GROUP_KEY_RATE_DURATION, bean.getMarketDataField().getId()),
						"Selected Market Data Field [" + bean.getMarketDataField().getName() + "] is not a valid selection because it does not belong to Field Group ["
								+ MarketDataFieldGroup.GROUP_KEY_RATE_DURATION + "].");
			}

			// All Rebalance Triggers should be positive values
			bean.setRebalanceMinPercent(MathUtils.abs(bean.getRebalanceMinPercent()));
			bean.setRebalanceMaxPercent(MathUtils.abs(bean.getRebalanceMaxPercent()));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}
}
