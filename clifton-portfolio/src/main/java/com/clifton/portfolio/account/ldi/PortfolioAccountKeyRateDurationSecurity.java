package com.clifton.portfolio.account.ldi;


import com.clifton.core.dataaccess.dao.NonPersistentObject;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;


/**
 * The <code>PortfolioAccountKeyRateDurationSecurity</code> contains security KRD point value
 * information for the account and security based on the account setup
 *
 * @author manderson
 */
@NonPersistentObject
public class PortfolioAccountKeyRateDurationSecurity {

	private Integer securityId;
	/**
	 * The actual date the values are for
	 */
	private Date valueDate;

	/**
	 * The latest date that can be used for comparison.
	 * i.e. 10/31 is a Saturday - so that is the maxValueDate, but most securities would have data entered as 10/30 the last business day of the month
	 */
	private Date maxValueDate;


	/**
	 * Maps each point to it's KRD value
	 */
	private Map<PortfolioAccountKeyRateDurationPoint, BigDecimal> krdValueMap;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Date getValueDate() {
		return this.valueDate;
	}


	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}


	public Date getMaxValueDate() {
		return this.maxValueDate;
	}


	public void setMaxValueDate(Date maxValueDate) {
		this.maxValueDate = maxValueDate;
	}


	public Map<PortfolioAccountKeyRateDurationPoint, BigDecimal> getKrdValueMap() {
		return this.krdValueMap;
	}


	public void setKrdValueMap(Map<PortfolioAccountKeyRateDurationPoint, BigDecimal> krdValueMap) {
		this.krdValueMap = krdValueMap;
	}
}
