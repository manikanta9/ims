package com.clifton.portfolio.account.ldi;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioAccountLDIService</code> ...
 *
 * @author manderson
 */
public interface PortfolioAccountLDIService {

	////////////////////////////////////////////////////////////////
	/////   Portfolio Account Key Rate Duration Point Methods  /////
	////////////////////////////////////////////////////////////////


	public PortfolioAccountKeyRateDurationPoint getPortfolioAccountKeyRateDurationPoint(int id);


	public List<PortfolioAccountKeyRateDurationPoint> getPortfolioAccountKeyRateDurationPointList(PortfolioAccountKeyRateDurationPointSearchForm searchForm);


	/**
	 * Create Reportable Points for each of the Managed Points from this account (if they do not already exist as Reportable Only points for this account).
	 */
	@SecureMethod(dtoClass = PortfolioAccountKeyRateDurationPoint.class)
	public void copyPortfolioManagedAccountKeyRateDurationPoints(int clientInvestmentAccountId);


	public PortfolioAccountKeyRateDurationPoint savePortfolioAccountKeyRateDurationPoint(PortfolioAccountKeyRateDurationPoint bean);


	public void deletePortfolioAccountKeyRateDurationPoint(int id);


	///////////////////////////////////////////////////////////////////////////
	/////   Portfolio Account Key Rate Duration Point Assignment Methods  /////
	///////////////////////////////////////////////////////////////////////////


	public List<PortfolioAccountKeyRateDurationPointAssignment> getPortfolioAccountKeyRateDurationPointAssignmentListByParent(int parentId);


	////////////////////////////////////////////////////////////////////////
	////   Portfolio Account Key Rate Duration Point Calculate Methods  ////
	////////////////////////////////////////////////////////////////////////


	/**
	 * Used to Preview Modified KRD Subset Values for Account/Security
	 * Expect this to be a temporary method so we can validate modified krd subset values until runs are set up
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = PortfolioAccountKeyRateDurationPoint.class)
	public String getPortfolioAccountKeyRateDurationResultForSecurity(int clientInvestmentAccountId, int securityId, Date date);


	@DoNotAddRequestMapping
	public Map<Boolean, List<PortfolioAccountKeyRateDurationPoint>> getPortfolioAccountKeyRateDurationPointListMapForAccount(int clientInvestmentAccountId);


	/**
	 * Used to Preview Modified KRD Subset Values for {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory}s associated
	 * with LDI {@link com.clifton.investment.account.InvestmentAccount}s
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = PortfolioAccountKeyRateDurationPoint.class)
	public String getPortfolioAccountKeyRateDurationCashFlowLiabilitiesResultForClientAccount(int clientInvestmentAccountId, Date balanceDate);


	/**
	 * Returns the security calculated KRD value map wrapped in the holder object for the point list, security, and dates
	 * If KRDValue date is populated will first look up value on that date.  If not available, then finds latest related to that (for forcing accounts to use the same krd value date across everything)
	 * If not, will get latest relevant to the balance date.
	 */
	@DoNotAddRequestMapping
	public PortfolioAccountKeyRateDurationSecurity calculatePortfolioAccountKeyRateDurationForSecurity(List<PortfolioAccountKeyRateDurationPoint> accountKeyRateDurationPointList,
	                                                                                                   InvestmentSecurity security, Date valueDate, Date balanceDate, Boolean useMonthEndValues);


	@DoNotAddRequestMapping
	Map<PortfolioAccountKeyRateDurationPoint, BigDecimal> getKeyRateDurationPointModifiedValueMap(List<PortfolioAccountKeyRateDurationPoint> accountKeyRateDurationPointList,
	                                                                                              Map<MarketDataField, BigDecimal> krdMap);
}
