package com.clifton.portfolio.account.ldi;

import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentAwareAssignment;


/**
 * @author manderson
 */
public class PortfolioAccountKeyRateDurationPointAssignment extends InvestmentTargetExposureAdjustmentAwareAssignment<PortfolioAccountKeyRateDurationPoint, PortfolioAccountKeyRateDurationPointAssignment> {

	// NOTHING HERE
}
