package com.clifton.portfolio.account;


import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioAccountDataRetriever</code> defines common methods across various portfolio types used by client accounts
 * for processing the portfolio
 *
 * @author manderson
 */
public interface PortfolioAccountDataRetriever {

	////////////////////////////////////////////////////////////////////////////
	///////    Client Account Portfolio Custom Field Value Methods       ///////
	////////////////////////////////////////////////////////////////////////////


	public Object getPortfolioAccountCustomFieldValue(InvestmentAccount account, String fieldName);


	/**
	 * Customized method for the report at this time, because the column name depends on the account
	 * setup/custom column group name
	 */
	public Object getPortfolioReportCustomFieldValue(InvestmentAccount account, ServiceProcessingTypes processingType);

	////////////////////////////////////////////////////////////////////////////
	/////////           Client Account Relationship Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	public List<InvestmentAccountRelationship> getTradingFuturesBrokerAccountRelationshipList(int investmentAccountId, Date activeOnDate);


	public List<InvestmentAccountRelationship> getCliftonSubAccountAccountRelationshipList(int investmentAccountId, Date activeOnDate);


	public List<InvestmentAccountRelationship> getInvestmentAccountRelationshipList(int investmentAccountId, Date activeOnDate);


	/**
	 * Returns a list of Holding Account options valid for a given client account and trading security type options
	 * Throws an exception if there are no matches.
	 */
	//public List<InvestmentAccount> getHoldingInvestmentAccountList(InvestmentAccount mainClientAccount, InvestmentAccount tradingClientAccount, InvestmentType tradingSecurityType,
	//                                                             InvestmentAccountAssetClass accountAssetClass, Date activeOnDate);
	public InvestmentAccount getMainAccountForCliftonSubAccount(InvestmentAccount subAccount, boolean throwExceptionIfMissingOrMoreThanOne, Date activeOnDate);

	////////////////////////////////////////////////////////////////////////////
	///////         Client Account Positions/Trade Methods                //////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If available pass and use the "Current" one for creating/populated pending configuration information that is always the same
	 * instead of looking it up again.
	 * For trading screen when getting all current is always available.  When drilling into the replication and going to Pending Trades tab, then we don't have current available so in this case we need to look it up.
	 */
	public PortfolioAccountContractStore getPortfolioAccountContractStorePending(PortfolioAccountContractStore current, InvestmentAccount account, Integer securityId);


	public PortfolioAccountContractStore getPortfolioAccountContractStoreActual(InvestmentAccount clientAccount, Date date, boolean current);

	////////////////////////////////////////////////////////////////////////////
	///////      Client Account Object Retrievers from Context           ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * There are a few maps in the spring context that define how an account is handled/processed etc.
	 * In all cases the current logic is to look up based on the Account's Service Processing Type . Processing Type ENUM
	 *
	 * @param clientAccount     - the client account to use to get the processing type to used getting the object from the context map
	 * @param processingType    - processing type to use over the one configured on the client account
	 * @param contextObjectMap  - the map from the spring context that holds the objects where key = ServiceProcessingType Enum name.
	 * @param objectDescription - description of the object we are retrieving - used for error messages, i.e. Portfolio Run Margin Handler
	 */
	public <T> T getClientAccountObjectFromContextMap(InvestmentAccount clientAccount, ServiceProcessingTypes processingType, Map<String, T> contextObjectMap, String objectDescription);
}
