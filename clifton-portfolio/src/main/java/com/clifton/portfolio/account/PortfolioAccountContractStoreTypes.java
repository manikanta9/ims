package com.clifton.portfolio.account;


/**
 * The <code>PortfolioAccountContractStoreTypes</code> ...
 *
 * @author manderson
 */
public enum PortfolioAccountContractStoreTypes {

	VIRTUAL("virtualContracts", false, null, true), // System allocated contracts - used with contract splitting when a contract is split long/short
	ACTUAL("actualContracts", false, VIRTUAL, false), // Contracts Posted on Balance Date of a Run
	VIRTUAL_PENDING("pendingVirtualContracts", true, null, true, true), // Same As Virtual, but performed after Pending Contracts for Trade Creation
	PENDING("pendingContracts", true, VIRTUAL_PENDING, false, true), // Current Non Closed Trades, Position Transfers, i.e. not Posted yet
	VIRTUAL_CURRENT("currentVirtualContracts", true, null, true), // Same As Virtual, but performed after Current Contracts for Trade Creation
	CURRENT("currentContracts", true, VIRTUAL_CURRENT, false); // Contracts Posted on Next Business Day of Run


	PortfolioAccountContractStoreTypes(String fieldName, boolean useTradingValue, PortfolioAccountContractStoreTypes dependentType, boolean virtual) {
		this(fieldName, useTradingValue, dependentType, virtual, false);
	}


	PortfolioAccountContractStoreTypes(String fieldName, boolean useTradingValue, PortfolioAccountContractStoreTypes dependentType, boolean virtual, boolean pending) {
		this.fieldName = fieldName;
		this.useTradingValue = useTradingValue;
		this.dependentType = dependentType;
		this.virtual = virtual;
		this.pending = pending;
	}


	private final String fieldName;
	private final boolean useTradingValue;
	private final PortfolioAccountContractStoreTypes dependentType;
	private final boolean virtual;
	private final boolean pending;


	public String getFieldName() {
		return this.fieldName;
	}


	public boolean isUseTradingValue() {
		return this.useTradingValue;
	}


	public PortfolioAccountContractStoreTypes getDependentType() {
		return this.dependentType;
	}


	public boolean isVirtual() {
		return this.virtual;
	}


	public boolean isPending() {
		return this.pending;
	}
}
