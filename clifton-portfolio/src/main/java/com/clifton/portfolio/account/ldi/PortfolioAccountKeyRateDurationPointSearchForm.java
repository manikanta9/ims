package com.clifton.portfolio.account.ldi;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;


/**
 * The <code>PortfolioAccountKeyRateDurationPointSearchForm</code> ...
 *
 * @author manderson
 */
public class PortfolioAccountKeyRateDurationPointSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "marketDataField.id")
	private Short marketDataFieldId;

	@SearchField(searchFieldPath = "marketDataField", searchField = "fieldOrder")
	private Short fieldOrder;

	@SearchField(searchFieldPath = "marketDataField", searchField = "name")
	private String marketDataFieldName;

	@SearchField
	private BigDecimal targetHedgePercent;

	@SearchField
	private BigDecimal rebalanceMinPercent;

	@SearchField
	private BigDecimal rebalanceMaxPercent;

	@SearchField
	private Boolean inactive;

	@SearchField
	private Boolean reportableOnly;

	// Custom Search Field
	private Boolean targetExposureAdjustmentUsed;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Short getMarketDataFieldId() {
		return this.marketDataFieldId;
	}


	public void setMarketDataFieldId(Short marketDataFieldId) {
		this.marketDataFieldId = marketDataFieldId;
	}


	public BigDecimal getTargetHedgePercent() {
		return this.targetHedgePercent;
	}


	public void setTargetHedgePercent(BigDecimal targetHedgePercent) {
		this.targetHedgePercent = targetHedgePercent;
	}


	public BigDecimal getRebalanceMinPercent() {
		return this.rebalanceMinPercent;
	}


	public void setRebalanceMinPercent(BigDecimal rebalanceMinPercent) {
		this.rebalanceMinPercent = rebalanceMinPercent;
	}


	public BigDecimal getRebalanceMaxPercent() {
		return this.rebalanceMaxPercent;
	}


	public void setRebalanceMaxPercent(BigDecimal rebalanceMaxPercent) {
		this.rebalanceMaxPercent = rebalanceMaxPercent;
	}


	public Boolean getInactive() {
		return this.inactive;
	}


	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}


	public Boolean getReportableOnly() {
		return this.reportableOnly;
	}


	public void setReportableOnly(Boolean reportableOnly) {
		this.reportableOnly = reportableOnly;
	}


	public String getMarketDataFieldName() {
		return this.marketDataFieldName;
	}


	public void setMarketDataFieldName(String marketDataFieldName) {
		this.marketDataFieldName = marketDataFieldName;
	}


	public Short getFieldOrder() {
		return this.fieldOrder;
	}


	public void setFieldOrder(Short fieldOrder) {
		this.fieldOrder = fieldOrder;
	}


	public Boolean getTargetExposureAdjustmentUsed() {
		return this.targetExposureAdjustmentUsed;
	}


	public void setTargetExposureAdjustmentUsed(Boolean targetExposureAdjustmentUsed) {
		this.targetExposureAdjustmentUsed = targetExposureAdjustmentUsed;
	}
}
