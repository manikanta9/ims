package com.clifton.portfolio.account.ldi;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustment;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentAware;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentTypes;
import com.clifton.marketdata.field.MarketDataField;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>PortfolioAccountKeyRateDurationPoint</code> ...
 *
 * @author manderson
 */
public class PortfolioAccountKeyRateDurationPoint extends BaseEntity<Integer> implements InvestmentTargetExposureAdjustmentAware<PortfolioAccountKeyRateDurationPoint, PortfolioAccountKeyRateDurationPointAssignment> {

	/**
	 * For LDI, Target Exposure Adjustments that use Proportional Option can select how that proportional allocation is determined
	 * Default will always be TARGET_PERCENT
	 * Uses system list for user selections
	 */
	public static final String TARGET_EXPOSURE_PROPORTIONAL_TYPE_TARGET_PERCENT = "Target Hedge"; // Default Value
	public static final String TARGET_EXPOSURE_PROPORTIONAL_TYPE_TARGET_DV01 = "Target DV01";
	public static final String TARGET_EXPOSURE_PROPORTIONAL_TYPE_LIABILITY_DV01 = "Liability DV01";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentAccount clientInvestmentAccount;

	/**
	 * Key Rate Duration Point Market Data Field
	 * Validation is that Market Data Field must be part of group "Key Rate Duration Points"
	 */
	private MarketDataField marketDataField;

	private BigDecimal targetHedgePercent;

	private BigDecimal rebalanceMinPercent;

	private BigDecimal rebalanceMaxPercent;

	/**
	 * Will be referenced by daily runs, so deleting will not be allowed.
	 */
	private boolean inactive;

	private boolean reportableOnly;

	/**
	 * Target Exposure Adjustment : persisted as JSON
	 */
	private InvestmentTargetExposureAdjustment targetExposureAdjustment;
	private List<PortfolioAccountKeyRateDurationPointAssignment> targetExposureAssignmentCombinedList;

	private String targetExposureProportionalType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder label = new StringBuilder(16);
		if (getClientInvestmentAccount() != null) {
			label.append(getClientInvestmentAccount().getLabel());
		}
		label.append(" - ");
		if (getMarketDataField() != null) {
			label.append(getMarketDataField().getName());
		}
		return label.toString();
	}


	public String getKeyRateDurationPointLabel() {
		short months = 0;
		if (getMarketDataField() != null && getMarketDataField().getFieldOrder() != null) {
			months = getMarketDataField().getFieldOrder();
		}
		if (months == 0) {
			return "";
		}
		if (months < 12) {
			return months + " Month";
		}
		// Note: We don't have anything that is a combination of x years + y months - anything over a year goes in yearly increments
		return (months / 12) + " Year";
	}


	@Override
	public boolean isTargetExposureAdjustmentUsed() {
		// Overridden since default interface methods are not supported by introspector
		return getTargetExposureAdjustment() != null && (InvestmentTargetExposureAdjustmentTypes.isTargetExposureUsed(getTargetExposureAdjustment().getTargetExposureAdjustmentType()) || !StringUtils.isEmpty(getTargetExposureAdjustment().getJsonValue()));
	}


	@Override
	public BigDecimal getTargetPercent() {
		return getTargetHedgePercent();
	}


	@Override
	public List<PortfolioAccountKeyRateDurationPointAssignment> getTargetAdjustmentAssignmentList() {
		// Overridden since default interface methods are not supported by introspector
		return CollectionUtils.getFiltered(getTargetExposureAssignmentCombinedList(), assignment -> !assignment.isCashAllocation());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public MarketDataField getMarketDataField() {
		return this.marketDataField;
	}


	public void setMarketDataField(MarketDataField marketDataField) {
		this.marketDataField = marketDataField;
	}


	public BigDecimal getTargetHedgePercent() {
		return this.targetHedgePercent;
	}


	public void setTargetHedgePercent(BigDecimal targetHedgePercent) {
		this.targetHedgePercent = targetHedgePercent;
	}


	public BigDecimal getRebalanceMinPercent() {
		return this.rebalanceMinPercent;
	}


	public void setRebalanceMinPercent(BigDecimal rebalanceMinPercent) {
		this.rebalanceMinPercent = rebalanceMinPercent;
	}


	public BigDecimal getRebalanceMaxPercent() {
		return this.rebalanceMaxPercent;
	}


	public void setRebalanceMaxPercent(BigDecimal rebalanceMaxPercent) {
		this.rebalanceMaxPercent = rebalanceMaxPercent;
	}


	@Override
	public boolean isInactive() {
		return this.inactive;
	}


	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}


	public boolean isReportableOnly() {
		return this.reportableOnly;
	}


	public void setReportableOnly(boolean reportableOnly) {
		this.reportableOnly = reportableOnly;
	}


	@Override
	public InvestmentTargetExposureAdjustment getTargetExposureAdjustment() {
		return this.targetExposureAdjustment;
	}


	@Override
	public void setTargetExposureAdjustment(InvestmentTargetExposureAdjustment targetExposureAdjustment) {
		this.targetExposureAdjustment = targetExposureAdjustment;
	}


	@Override
	public List<PortfolioAccountKeyRateDurationPointAssignment> getTargetExposureAssignmentCombinedList() {
		return this.targetExposureAssignmentCombinedList;
	}


	@Override
	public void setTargetExposureAssignmentCombinedList(List<PortfolioAccountKeyRateDurationPointAssignment> targetExposureAssignmentCombinedList) {
		this.targetExposureAssignmentCombinedList = targetExposureAssignmentCombinedList;
	}


	public String getTargetExposureProportionalType() {
		return this.targetExposureProportionalType;
	}


	public void setTargetExposureProportionalType(String targetExposureProportionalType) {
		this.targetExposureProportionalType = targetExposureProportionalType;
	}
}
