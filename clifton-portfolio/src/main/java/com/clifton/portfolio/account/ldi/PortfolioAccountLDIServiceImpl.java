package com.clifton.portfolio.account.ldi;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.targetexposure.InvestmentAccountTargetExposureHandler;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldFieldGroup;
import com.clifton.marketdata.field.MarketDataFieldGroup;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccountKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.process.PortfolioRunLDIConfig;
import com.clifton.portfolio.run.ldi.process.calculators.cashflow.PortfolioRunCashFlowLiabilityCalculator;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioAccountLDIServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class PortfolioAccountLDIServiceImpl implements PortfolioAccountLDIService {

	// Custom Fields
	public static final String CUSTOM_COLUMN_USE_MONTH_END_KRD_VALUES = "Month End KRD Values";
	public static final String CUSTOM_COLUMN_DO_NOT_ENFORCE_SAME_KRD_VALUE_DATES = "Do Not Enforce Same KRD Dates";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AdvancedUpdatableDAO<PortfolioAccountKeyRateDurationPoint, Criteria> portfolioAccountKeyRateDurationPointDAO;
	private AdvancedUpdatableDAO<PortfolioAccountKeyRateDurationPointAssignment, Criteria> portfolioAccountKeyRateDurationPointAssignmentDAO;

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentCalculator investmentCalculator;
	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataFieldService marketDataFieldService;
	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;

	private PortfolioRunCashFlowLiabilityCalculator portfolioRunLDIManagerKeyRateDurationPointCalculator;

	private InvestmentAccountTargetExposureHandler<PortfolioAccountKeyRateDurationPoint, PortfolioAccountKeyRateDurationPointAssignment> investmentAccountTargetExposureHandler;


	////////////////////////////////////////////////////////////////////////////
	////////      Portfolio Account Key Rate Duration Point Methods     ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioAccountKeyRateDurationPoint getPortfolioAccountKeyRateDurationPoint(int id) {
		PortfolioAccountKeyRateDurationPoint bean = getPortfolioAccountKeyRateDurationPointDAO().findByPrimaryKey(id);
		if (bean != null && bean.isTargetExposureAdjustmentUsed()) {
			bean.setTargetExposureAssignmentCombinedList(getPortfolioAccountKeyRateDurationPointAssignmentListByParent(bean.getId()));
		}
		return bean;
	}


	@Override
	public List<PortfolioAccountKeyRateDurationPoint> getPortfolioAccountKeyRateDurationPointList(final PortfolioAccountKeyRateDurationPointSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (searchForm.getTargetExposureAdjustmentUsed() != null) {
					if (searchForm.getTargetExposureAdjustmentUsed()) {
						criteria.add(Restrictions.isNotNull("targetExposureAdjustment"));
					}
					else {
						criteria.add(Restrictions.isNull("targetExposureAdjustment"));
					}
				}
			}
		};
		return getPortfolioAccountKeyRateDurationPointDAO().findBySearchCriteria(config);
	}


	@Transactional
	@Override
	public void copyPortfolioManagedAccountKeyRateDurationPoints(int clientInvestmentAccountId) {
		PortfolioAccountKeyRateDurationPointSearchForm searchForm = new PortfolioAccountKeyRateDurationPointSearchForm();
		searchForm.setClientInvestmentAccountId(clientInvestmentAccountId);
		List<PortfolioAccountKeyRateDurationPoint> accountKrdPointList = getPortfolioAccountKeyRateDurationPointList(searchForm);
		List<PortfolioAccountKeyRateDurationPoint> managedPointList = new ArrayList<>();
		Map<Short, PortfolioAccountKeyRateDurationPoint> reportableOnlyPointListMap = new HashMap<>();
		for (PortfolioAccountKeyRateDurationPoint accountKrdPoint : CollectionUtils.getIterable(accountKrdPointList)) {
			if (accountKrdPoint.isReportableOnly()) {
				reportableOnlyPointListMap.put(accountKrdPoint.getMarketDataField().getFieldOrder(), accountKrdPoint);
			}
			else {
				managedPointList.add(accountKrdPoint);
			}
		}

		for (PortfolioAccountKeyRateDurationPoint managedPoint : CollectionUtils.getIterable(managedPointList)) {
			if (reportableOnlyPointListMap.get(managedPoint.getMarketDataField().getFieldOrder()) == null) {
				PortfolioAccountKeyRateDurationPoint reportablePoint = new PortfolioAccountKeyRateDurationPoint();
				BeanUtils.copyProperties(managedPoint, reportablePoint, new String[]{"id", "createUserId", "createDate", "updateUserId", "updateDate", "rv"});
				reportablePoint.setReportableOnly(true);
				reportableOnlyPointListMap.put(reportablePoint.getMarketDataField().getFieldOrder(), reportablePoint);
			}
		}

		getPortfolioAccountKeyRateDurationPointDAO().saveList(new ArrayList<>(reportableOnlyPointListMap.values()));
	}


	/**
	 * See Validation in: {@link PortfolioAccountKeyRateDurationPointValidator} *
	 */
	@Override
	public PortfolioAccountKeyRateDurationPoint savePortfolioAccountKeyRateDurationPoint(PortfolioAccountKeyRateDurationPoint bean) {
		getInvestmentAccountTargetExposureHandler().validateTargetExposureAdjustmentTypeOptions(bean, "KRD Point", getPortfolioAccountKeyRateDurationPointAssignmentDAO());

		// get existing lists so that we know what to delete if necessary
		List<PortfolioAccountKeyRateDurationPointAssignment> oldAssignments = null;
		if (!bean.isNewBean()) {
			oldAssignments = getPortfolioAccountKeyRateDurationPointAssignmentListByParent(bean.getId());
		}

		List<PortfolioAccountKeyRateDurationPointAssignment> exposureAssignmentList = bean.getTargetExposureAssignmentCombinedList();
		bean = getPortfolioAccountKeyRateDurationPointDAO().save(bean);

		// Assignments - Delete if not a Target Exposure, otherwise save.
		if (!bean.isTargetExposureAdjustmentUsed()) {
			if (!CollectionUtils.isEmpty(oldAssignments)) {
				getPortfolioAccountKeyRateDurationPointAssignmentDAO().deleteList(oldAssignments);
			}
		}
		else {
			getPortfolioAccountKeyRateDurationPointAssignmentDAO().saveList(exposureAssignmentList, oldAssignments);
		}
		bean.setTargetExposureAssignmentCombinedList(exposureAssignmentList);
		return bean;
	}


	@Override
	public void deletePortfolioAccountKeyRateDurationPoint(int id) {
		// SHOULD ACTUALLY JUST DE-ACTIVATE, NOT DELETE - USERS CAN MANUALLY DE-ACTIVATE BUT CAN'T DELETE IF A RUN HAS BEEN PROCESSED ALREADY
		getPortfolioAccountKeyRateDurationPointDAO().delete(id);
	}


	///////////////////////////////////////////////////////////////////////////
	/////   Portfolio Account Key Rate Duration Point Assignment Methods  /////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortfolioAccountKeyRateDurationPointAssignment> getPortfolioAccountKeyRateDurationPointAssignmentListByParent(int parentId) {
		return getPortfolioAccountKeyRateDurationPointAssignmentDAO().findByField("referenceOne.id", parentId);
	}


	////////////////////////////////////////////////////////////////////////
	////   Portfolio Account Key Rate Duration Point Calculate Methods  ////
	////////////////////////////////////////////////////////////////////////


	/**
	 * Temporary Method for Previewing Results until runs are implemented
	 *
	 * @param clientInvestmentAccountId
	 * @param securityId
	 * @param date
	 */
	@Override
	public String getPortfolioAccountKeyRateDurationResultForSecurity(int clientInvestmentAccountId, int securityId, Date date) {
		Map<Boolean, List<PortfolioAccountKeyRateDurationPoint>> pointListMap = getPortfolioAccountKeyRateDurationPointListMapForAccount(clientInvestmentAccountId);
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);

		Boolean useMonthEnd = (Boolean) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(CollectionUtils.getFirstElementStrict(pointListMap.get(false)).getClientInvestmentAccount(), CUSTOM_COLUMN_USE_MONTH_END_KRD_VALUES);
		StringBuilder resultString = new StringBuilder("Modified KRD Values for " + security.getLabel() + " using latest " + ((useMonthEnd != null && useMonthEnd) ? "month-end " : "")
				+ "values as of selected date: ");
		resultString.append(StringUtils.NEW_LINE + StringUtils.TAB + "Managed Points");
		PortfolioAccountKeyRateDurationSecurity result = calculatePortfolioAccountKeyRateDurationForSecurity(pointListMap.get(false), security, null, date, useMonthEnd);
		for (Map.Entry<PortfolioAccountKeyRateDurationPoint, BigDecimal> krdPointEntry : result.getKrdValueMap().entrySet()) {
			resultString.append(StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB)
					.append(krdPointEntry.getKey().getMarketDataField().getName())
					.append(": ")
					.append(CoreMathUtils.formatNumberDecimal(krdPointEntry.getValue()));
		}
		result = calculatePortfolioAccountKeyRateDurationForSecurity(pointListMap.get(true), security, null, date, useMonthEnd);
		if (!CollectionUtils.isEmpty(result.getKrdValueMap())) {
			resultString.append(StringUtils.NEW_LINE + StringUtils.NEW_LINE + StringUtils.TAB + "Reportable Points");
		}
		for (Map.Entry<PortfolioAccountKeyRateDurationPoint, BigDecimal> krdPointEntry : result.getKrdValueMap().entrySet()) {
			resultString.append(StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB)
					.append(krdPointEntry.getKey().getMarketDataField().getName())
					.append(": ")
					.append(CoreMathUtils.formatNumberDecimal(krdPointEntry.getValue()));
		}
		resultString.append(StringUtils.NEW_LINE + StringUtils.NEW_LINE);
		resultString.append(getPortfolioAccountKeyRateDurationCashFlowLiabilitiesResultForClientAccount(clientInvestmentAccountId, date));
		return resultString.toString();
	}


	@Override
	public Map<Boolean, List<PortfolioAccountKeyRateDurationPoint>> getPortfolioAccountKeyRateDurationPointListMapForAccount(int clientInvestmentAccountId) {
		PortfolioAccountKeyRateDurationPointSearchForm searchForm = new PortfolioAccountKeyRateDurationPointSearchForm();
		searchForm.setClientInvestmentAccountId(clientInvestmentAccountId);
		searchForm.setInactive(false);
		Map<Boolean, List<PortfolioAccountKeyRateDurationPoint>> pointListMap = BeanUtils.getBeansMap(getPortfolioAccountKeyRateDurationPointList(searchForm), PortfolioAccountKeyRateDurationPoint::isReportableOnly);
		return pointListMap;
	}


	@Override
	public String getPortfolioAccountKeyRateDurationCashFlowLiabilitiesResultForClientAccount(int clientInvestmentAccountId, Date balanceDate) {
		Map<Boolean, List<PortfolioAccountKeyRateDurationPoint>> pointListMap = getPortfolioAccountKeyRateDurationPointListMapForAccount(clientInvestmentAccountId);

		PortfolioRun run = new PortfolioRun();
		run.setClientInvestmentAccount(CollectionUtils.getFirstElementStrict(pointListMap.get(false)).getClientInvestmentAccount());
		run.setBalanceDate(balanceDate);
		PortfolioRunLDIConfig runConfig = new PortfolioRunLDIConfig(run, false, 1);
		runConfig.setKeyRateDurationPointListMap(pointListMap);
		runConfig = getPortfolioRunLDIManagerKeyRateDurationPointCalculator().calculateCashFlowLiabilityValues(runConfig);

		Map<PortfolioCashFlowGroupHistory, Map<Boolean, List<PortfolioRunLDIManagerAccountKeyRateDurationPoint>>> accountKrdPointListMap = new HashMap<>();
		CollectionUtils.getStream(runConfig.getRunManagerPointList()).forEach(accountKrdPoint -> accountKrdPointListMap.computeIfAbsent(
				accountKrdPoint.getManagerAccount().getCashFlowGroupHistory(), k -> new HashMap<>()).computeIfAbsent(accountKrdPoint.getKeyRateDurationPoint().isReportableOnly(), ro -> new ArrayList<>()).add(accountKrdPoint));
		return CollectionUtils.isEmpty(accountKrdPointListMap) ? StringUtils.EMPTY_STRING : buildCalculatedCashFlowResultStringForAccountKrdPointListMap(accountKrdPointListMap, balanceDate);
	}


	private String buildCalculatedCashFlowResultStringForAccountKrdPointListMap(Map<PortfolioCashFlowGroupHistory, Map<Boolean, List<PortfolioRunLDIManagerAccountKeyRateDurationPoint>>> accountKrdPointListMap, Date balanceDate) {
		StringBuilder stringBuilder = new StringBuilder("Modified KRD and DV01 Values for Investment Account [" + CollectionUtils.getFirstElementStrict(accountKrdPointListMap.keySet()).getCashFlowGroup().getInvestmentAccount().getLabelShort() + "] on Balance Date [" + DateUtils.fromDateShort(balanceDate) + "]");
		for (Map.Entry<PortfolioCashFlowGroupHistory, Map<Boolean, List<PortfolioRunLDIManagerAccountKeyRateDurationPoint>>> accountKrdPointListEntry : accountKrdPointListMap.entrySet()) {
			stringBuilder.append(StringUtils.NEW_LINE + StringUtils.TAB).append("Cash Flow Group History [").append(accountKrdPointListEntry.getKey().getLabel()).append("]").append(StringUtils.NEW_LINE);
			stringBuilder.append(StringUtils.TAB + StringUtils.TAB + "Managed Points" + StringUtils.NEW_LINE);
			for (PortfolioRunLDIManagerAccountKeyRateDurationPoint ldiAccountKrdPoint : CollectionUtils.createSorted(accountKrdPointListEntry.getValue().get(false), Comparator.comparing(accountKrdPoint -> accountKrdPoint.getKeyRateDurationPoint().getMarketDataField().getFieldOrder()))) {
				stringBuilder.append(StringUtils.TAB + StringUtils.TAB + StringUtils.TAB).append(ldiAccountKrdPoint.getKeyRateDurationPoint().getMarketDataField().getName())
						.append(":" + StringUtils.TAB + StringUtils.TAB + StringUtils.TAB + "KRD ").append(CoreMathUtils.formatNumberDecimal(ldiAccountKrdPoint.getKrdValue()))
						.append(StringUtils.TAB + StringUtils.TAB + StringUtils.TAB + "DV01 ").append(CoreMathUtils.formatNumberDecimal(ldiAccountKrdPoint.getDv01Value()))
						.append(StringUtils.NEW_LINE);
			}
			if (!CollectionUtils.isEmpty(accountKrdPointListEntry.getValue().get(true))) {
				stringBuilder.append(StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "Reportable Points" + StringUtils.NEW_LINE);
				for (PortfolioRunLDIManagerAccountKeyRateDurationPoint ldiAccountKrdPoint : CollectionUtils.createSorted(accountKrdPointListEntry.getValue().get(true), Comparator.comparing(accountKrdPoint -> accountKrdPoint.getKeyRateDurationPoint().getMarketDataField().getFieldOrder()))) {
					stringBuilder.append(StringUtils.TAB + StringUtils.TAB + StringUtils.TAB).append(ldiAccountKrdPoint.getKeyRateDurationPoint().getMarketDataField().getName())
							.append(":" + StringUtils.TAB + StringUtils.TAB + StringUtils.TAB + "KRD ").append(CoreMathUtils.formatNumberDecimal(ldiAccountKrdPoint.getKrdValue()))
							.append(StringUtils.TAB + StringUtils.TAB + StringUtils.TAB + "DV01 ").append(CoreMathUtils.formatNumberDecimal(ldiAccountKrdPoint.getDv01Value()))
							.append(StringUtils.NEW_LINE);
				}
			}
		}
		return stringBuilder.toString();
	}


	@Override
	public PortfolioAccountKeyRateDurationSecurity calculatePortfolioAccountKeyRateDurationForSecurity(List<PortfolioAccountKeyRateDurationPoint> accountKeyRateDurationPointList,
	                                                                                                   InvestmentSecurity security, Date valueDate, Date balanceDate, Boolean useMonthEndValues) {
		if (valueDate == null) {
			if (useMonthEndValues != null && useMonthEndValues) {
				valueDate = DateUtils.addDays(DateUtils.getFirstDayOfMonth(balanceDate), -1);
			}
			else {
				valueDate = balanceDate;
			}
		}

		return getPortfolioAccountKeyRateDurationSecurity(accountKeyRateDurationPointList, security, valueDate);
	}


	@Override
	public Map<PortfolioAccountKeyRateDurationPoint, BigDecimal> getKeyRateDurationPointModifiedValueMap(List<PortfolioAccountKeyRateDurationPoint> accountKeyRateDurationPointList,
	                                                                                                     Map<MarketDataField, BigDecimal> krdMap) {
		accountKeyRateDurationPointList = BeanUtils.sortWithFunction(accountKeyRateDurationPointList, accountKeyRateDurationPoint -> accountKeyRateDurationPoint.getMarketDataField().getFieldOrder(), true);

		int size = accountKeyRateDurationPointList.size();

		Map<PortfolioAccountKeyRateDurationPoint, BigDecimal> modifiedKrdMap = new HashMap<>();

		BigDecimal modifiedKrdSum = BigDecimal.ZERO;
		Short previousOrder = null;
		StringBuilder result = new StringBuilder("Modified KRD Values: ");

		for (int i = 0; i < size; i++) {
			BigDecimal krdValue = BigDecimal.ZERO;
			PortfolioAccountKeyRateDurationPoint thisField = accountKeyRateDurationPointList.get(i);
			Short thisOrder = thisField.getMarketDataField().getFieldOrder();
			Short nextOrder = (size > (i + 1) ? accountKeyRateDurationPointList.get(i + 1).getMarketDataField().getFieldOrder() : null);

			BigDecimal differenceBefore = (previousOrder == null ? null : BigDecimal.valueOf((thisOrder - previousOrder)));
			BigDecimal differenceAfter = (nextOrder == null ? null : BigDecimal.valueOf((nextOrder - thisOrder)));

			for (Map.Entry<MarketDataField, BigDecimal> marketDataFieldBigDecimalEntry : krdMap.entrySet()) {
				// If before previous or we hit the next one - break out of loop
				if ((previousOrder != null && (marketDataFieldBigDecimalEntry.getKey()).getFieldOrder() <= previousOrder) || (nextOrder != null && (marketDataFieldBigDecimalEntry.getKey()).getFieldOrder() >= nextOrder)) {
					continue;
				}

				BigDecimal addKrdValue = marketDataFieldBigDecimalEntry.getValue();

				// In Between Steps:
				if (previousOrder != null && differenceBefore != null && (marketDataFieldBigDecimalEntry.getKey()).getFieldOrder() < thisOrder) {
					BigDecimal thisDifference = MathUtils.subtract(BigDecimal.valueOf((marketDataFieldBigDecimalEntry.getKey()).getFieldOrder()), previousOrder);
					addKrdValue = MathUtils.multiply(MathUtils.divide(thisDifference, differenceBefore), addKrdValue);
				}
				else if (nextOrder != null && differenceAfter != null && (marketDataFieldBigDecimalEntry.getKey()).getFieldOrder() > thisOrder) {
					BigDecimal thisDifference = MathUtils.subtract(BigDecimal.valueOf(nextOrder), BigDecimal.valueOf((marketDataFieldBigDecimalEntry.getKey()).getFieldOrder()));
					addKrdValue = MathUtils.multiply(MathUtils.divide(thisDifference, differenceAfter), addKrdValue);
				}
				krdValue = MathUtils.add(krdValue, addKrdValue);
			}

			modifiedKrdMap.put(thisField, krdValue);
			result.append(StringUtils.NEW_LINE + StringUtils.TAB).append(thisField.getMarketDataField().getName()).append(": ").append(CoreMathUtils.formatNumberDecimal(krdValue));
			modifiedKrdSum = MathUtils.add(modifiedKrdSum, krdValue);

			// Move to Next
			previousOrder = thisOrder;
		}

		BigDecimal expectedTotal = CoreMathUtils.sum(krdMap.values());
		if (!MathUtils.isEqual(expectedTotal, modifiedKrdSum)) {
			throw new ValidationException("Error Calculating Modified KRD Values.  Expected Modified KRD Sum to be [" + CoreMathUtils.formatNumberDecimal(expectedTotal) + "] but was ["
					+ CoreMathUtils.formatNumberDecimal(modifiedKrdSum) + "]. Results were: " + result.toString());
		}

		return modifiedKrdMap;
	}


	private PortfolioAccountKeyRateDurationSecurity getPortfolioAccountKeyRateDurationSecurity(List<PortfolioAccountKeyRateDurationPoint> accountKeyRateDurationPointList,
	                                                                                           InvestmentSecurity security, Date date) {
		PortfolioAccountKeyRateDurationSecurity krdBean = new PortfolioAccountKeyRateDurationSecurity();
		krdBean.setSecurityId(security.getId());

		List<MarketDataFieldFieldGroup> krdPointList = getMarketDataFieldService().getMarketDataFieldFieldGroupListByGroup(MarketDataFieldGroup.GROUP_KEY_RATE_DURATION);

		MarketDataValueHolder expectedSum = getSpotDurationForSecurity(security, date);
		Date valueDate = expectedSum.getMeasureDate();
		krdBean.setValueDate(valueDate);
		// Max Value Date is the last day before the next business day for the security
		krdBean.setMaxValueDate(DateUtils.addDays(getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(valueDate, getInvestmentCalculator().getInvestmentSecurityCalendar(security).getId())), -1));

		// Use the date from the Duration (Spot) = KRD Total to get the KRD Point values on that date
		Map<MarketDataField, BigDecimal> krdMap = buildKeyRateDurationMap(security, krdPointList, valueDate);
		validateKrdMapAgainstExpectedSum(security, krdMap, expectedSum, valueDate);

		krdBean.setKrdValueMap(getKeyRateDurationPointModifiedValueMap(accountKeyRateDurationPointList, krdMap));
		return krdBean;
	}


	/**
	 * Not all KRD Points are required - so finding the latest one for the smallest maturity doesn't always guarantee that we are retrieving the
	 * latest set of data points.  In order to get around this - first find the date on the latest Duration (Spot) value - since that value should be included
	 * for each date.
	 */
	private MarketDataValueHolder getSpotDurationForSecurity(InvestmentSecurity security, Date date) {
		MarketDataValueHolder expectedSum = getMarketDataFieldService().getMarketDataValueForDateFlexible(security.getId(), date, MarketDataField.FIELD_DURATION_SPOT);
		if (expectedSum == null) {
			throw new ValidationException("There is no " + MarketDataField.FIELD_DURATION_SPOT + " value set up for security [" + security.getLabel() + "] on or before "
					+ DateUtils.fromDateShort(date) + ".  This value is required and should equal the sum of the KRD (Spot) values for each point on the specified date.");
		}
		return expectedSum;
	}


	private Map<MarketDataField, BigDecimal> buildKeyRateDurationMap(InvestmentSecurity security, List<MarketDataFieldFieldGroup> krdPointList, Date valueDate) {
		Map<MarketDataField, BigDecimal> krdMap = new LinkedHashMap<>();
		for (MarketDataFieldFieldGroup field : CollectionUtils.getIterable(krdPointList)) {
			BigDecimal value = BigDecimal.ZERO;
			MarketDataValueHolder dataValue = getMarketDataFieldService().getMarketDataValueForDate(security.getId(), valueDate, field.getReferenceOne().getName());
			if (dataValue != null) {
				value = dataValue.getMeasureValue();
			}
			krdMap.put(field.getReferenceOne(), value);
		}
		return krdMap;
	}


	private void validateKrdMapAgainstExpectedSum(InvestmentSecurity security, Map<MarketDataField, BigDecimal> krdMap, MarketDataValueHolder expectedSum, Date valueDate) {
		BigDecimal krdPointSum = CoreMathUtils.sum(krdMap.values());

		// When comparing expected total to actual total - allow a threshold of 0.01
		BigDecimal difference = MathUtils.abs(MathUtils.subtract(krdPointSum, expectedSum.getMeasureValue()));
		if (MathUtils.isGreaterThan(difference, 0.01)) {
			throw new ValidationException("Unable to calculate modified KRD Points for Security [" + security.getLabel() + "] using KRD Point values from [" + DateUtils.fromDateShort(valueDate)
					+ "] because Duration (Spot) value for that date is [" + CoreMathUtils.formatNumberDecimal(expectedSum.getMeasureValue()) + "], but the total for the KRD Points on this date is ["
					+ CoreMathUtils.formatNumberDecimal(krdPointSum) + "]. The total sum of the points for a date should equal the Duration (Spot) value on that date.");
		}
	}


	////////////////////////////////////////////////////////////////
	/////////////       Getter and Setter Methods      /////////////
	////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortfolioAccountKeyRateDurationPoint, Criteria> getPortfolioAccountKeyRateDurationPointDAO() {
		return this.portfolioAccountKeyRateDurationPointDAO;
	}


	public void setPortfolioAccountKeyRateDurationPointDAO(AdvancedUpdatableDAO<PortfolioAccountKeyRateDurationPoint, Criteria> portfolioAccountKeyRateDurationPointDAO) {
		this.portfolioAccountKeyRateDurationPointDAO = portfolioAccountKeyRateDurationPointDAO;
	}


	public AdvancedUpdatableDAO<PortfolioAccountKeyRateDurationPointAssignment, Criteria> getPortfolioAccountKeyRateDurationPointAssignmentDAO() {
		return this.portfolioAccountKeyRateDurationPointAssignmentDAO;
	}


	public void setPortfolioAccountKeyRateDurationPointAssignmentDAO(AdvancedUpdatableDAO<PortfolioAccountKeyRateDurationPointAssignment, Criteria> portfolioAccountKeyRateDurationPointAssignmentDAO) {
		this.portfolioAccountKeyRateDurationPointAssignmentDAO = portfolioAccountKeyRateDurationPointAssignmentDAO;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public PortfolioRunCashFlowLiabilityCalculator getPortfolioRunLDIManagerKeyRateDurationPointCalculator() {
		return this.portfolioRunLDIManagerKeyRateDurationPointCalculator;
	}


	public void setPortfolioRunLDIManagerKeyRateDurationPointCalculator(PortfolioRunCashFlowLiabilityCalculator portfolioRunLDIManagerKeyRateDurationPointCalculator) {
		this.portfolioRunLDIManagerKeyRateDurationPointCalculator = portfolioRunLDIManagerKeyRateDurationPointCalculator;
	}


	public InvestmentAccountTargetExposureHandler<PortfolioAccountKeyRateDurationPoint, PortfolioAccountKeyRateDurationPointAssignment> getInvestmentAccountTargetExposureHandler() {
		return this.investmentAccountTargetExposureHandler;
	}


	public void setInvestmentAccountTargetExposureHandler(InvestmentAccountTargetExposureHandler<PortfolioAccountKeyRateDurationPoint, PortfolioAccountKeyRateDurationPointAssignment> investmentAccountTargetExposureHandler) {
		this.investmentAccountTargetExposureHandler = investmentAccountTargetExposureHandler;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}
