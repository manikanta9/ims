package com.clifton.portfolio.trade.rule;

import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.trade.PortfolioRunTrade;
import com.clifton.portfolio.run.trade.PortfolioRunTradeDetail;
import com.clifton.portfolio.run.trade.PortfolioRunTradeService;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.trade.Trade;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocationSearchForm;
import com.clifton.trade.assetclass.TradeAssetClassService;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunTradeContractDifferenceRuleEvaluator</code> is a TRADE rule that is applied to trades that originated from a Portfolio Run.  It compares the trade quantity
 * with the portfolio run suggested trade quantity to generate a violation if the difference is outside of suggested band.
 * <p>
 * Rule is evaluated separately whether or not the trade is in the same or opposite direction of the suggested trade.  A suggested trade of zero is considered the same direction.
 * <p>
 * Note: For cases with contract splitting, will use the Trade Allocations to determine actual differences applied to each.
 *
 * @author manderson
 */
public class PortfolioRunTradeContractDifferenceRuleEvaluator<T extends PortfolioRunTrade<D>, D extends PortfolioRunTradeDetail> extends BaseRuleEvaluator<Trade, TradeRuleEvaluatorContext> {

	private PortfolioRunService portfolioRunService;

	private PortfolioRunTradeService<T, D> portfolioRunTradeService;

	private SystemSchemaService systemSchemaService;

	private TradeAssetClassService tradeAssetClassService;

	/**
	 * Trades are evaluated differently if the trade is in the same direction or opposite direction of the suggested trade.
	 * A suggested trade of 0 is considered to be the same direction.
	 */
	private boolean applyRuleIfTradeInSameDirection;

	/**
	 * If set, the rule evaluation will be limited to trades under the
	 * selected investment types.
	 */
	private List<Short> investmentTypeIdList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(Trade trade, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();

		EntityConfig entityConfig = getRuleEntityConfig(trade, ruleConfig);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			PortfolioRunTrade<D> portfolioRunTrade = getPortfolioRunTradeForRuleEvaluation(trade, context);
			if (portfolioRunTrade != null) {
				List<D> securityRunDetailList = BeanUtils.filter(portfolioRunTrade.getDetailList(), portfolioRunTradeDetail -> (portfolioRunTradeDetail.getSecurity().equals(trade.getInvestmentSecurity()) && !portfolioRunTradeDetail.isTradeEntryDisabled()));
				if (!CollectionUtils.isEmpty(securityRunDetailList)) {
					// If there is only one - evaluate it against the trade
					if (securityRunDetailList.size() == 1) {
						evaluateRuleForRunDetailAndTradeAmount(entityConfig, securityRunDetailList.get(0), trade, (trade.isBuy() ? trade.getQuantity() : MathUtils.negate(trade.getQuantity())), violationList);
					}
					// If there are multiple, need to get trade allocations and evaluate each against the allocations
					else {
						// TODO need to support something other than asset classes
						TradeAssetClassPositionAllocationSearchForm searchForm = new TradeAssetClassPositionAllocationSearchForm();
						searchForm.setTradeId(trade.getId());
						List<TradeAssetClassPositionAllocation> tradeAllocationList = getTradeAssetClassService().getTradeAssetClassPositionAllocationList(searchForm);
						for (TradeAssetClassPositionAllocation positionAllocation : CollectionUtils.getIterable(tradeAllocationList)) {
							for (D securityRunDetail : CollectionUtils.getIterable(securityRunDetailList)) {
								if (securityRunDetail.getAccountAssetClass().equals(positionAllocation.getAccountAssetClass())) {
									if (securityRunDetail.getReplication().equals(positionAllocation.getReplication())) {
										evaluateRuleForRunDetailAndTradeAmount(entityConfig, securityRunDetail, trade, positionAllocation.getAllocationQuantity(), violationList);
									}
								}
							}
						}
					}
				}
			}
		}
		return violationList;
	}


	/**
	 * Returns the EntityConfig for the rule
	 * If the investment type filtering determines the rule doesn't apply then returns null
	 */
	private EntityConfig getRuleEntityConfig(Trade trade, RuleConfig ruleConfig) {
		if (CollectionUtils.isEmpty(getInvestmentTypeIdList()) || CollectionUtils.contains(getInvestmentTypeIdList(), trade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getId())) {
			return ruleConfig.getEntityConfig(null);
		}
		return null;
	}


	private void evaluateRuleForRunDetailAndTradeAmount(EntityConfig entityConfig, D portfolioRunTradeDetail, Trade trade, BigDecimal allocatedQuantity, List<RuleViolation> violationList) {
		BigDecimal suggestedTrade = portfolioRunTradeDetail.calculateSuggestedTradeQuantity();
		if (suggestedTrade != null) {
			// Back out this trade amount from suggested by ADDING it since it was originally subtracted
			suggestedTrade = MathUtils.add(suggestedTrade, allocatedQuantity);
			if (isApplyRule(suggestedTrade, allocatedQuantity)) {
				BigDecimal difference = MathUtils.subtract(suggestedTrade, allocatedQuantity);
				if (MathUtils.isGreaterThan(MathUtils.abs(difference), entityConfig.getMaxAmount())) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("suggestedTradeQuantity", suggestedTrade);
					templateValues.put("allocatedQuantity", allocatedQuantity);
					templateValues.put("portfolioRunDetail", portfolioRunTradeDetail);
					violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, trade.getId(), portfolioRunTradeDetail.getId(), getSystemSchemaService().getSystemTableByName(portfolioRunTradeDetail.getTableName()), templateValues));
				}
			}
		}
	}


	private boolean isApplyRule(BigDecimal suggestedTrade, BigDecimal allocatedQuantity) {
		// Same Direction Trade
		if (MathUtils.isNullOrZero(suggestedTrade) || (MathUtils.isGreaterThan(suggestedTrade, BigDecimal.ZERO) == MathUtils.isGreaterThan(allocatedQuantity, BigDecimal.ZERO))) {
			return isApplyRuleIfTradeInSameDirection();
		}
		// Else Opposite Direction Trade
		return !isApplyRuleIfTradeInSameDirection();
	}


	/**
	 * Only returns the run if it is an overlay run that should be evaluated for this rule.  If it's an LDI run, just return null so we know not to run the rule
	 * <p>
	 * Added to the context because this rule is run twice, once for same direction and once for opposite direction trades
	 */
	private PortfolioRunTrade<D> getPortfolioRunTradeForRuleEvaluation(Trade trade, TradeRuleEvaluatorContext context) {
		// NPE prevention, however Should always be set based on the Rule scope
		if (trade.getTradeGroup() != null && trade.getTradeGroup().getSourceFkFieldId() != null && trade.getTradeGroup().getTradeGroupType().getSourceSystemTable() != null && PortfolioRun.TABLE_PORTFOLIO_RUN.equals(trade.getTradeGroup().getTradeGroupType().getSourceSystemTable().getName())) {
			// This rule only applies to Overlay Run Trades, as LDI Runs do not have a suggested trade amount
			PortfolioRun portfolioRun = context.getContext().getOrSupplyBean("PORTFOLIO_RUN", () -> getPortfolioRunService().getPortfolioRun(trade.getTradeGroup().getSourceFkFieldId()));
			if (portfolioRun != null && ServiceProcessingTypes.PORTFOLIO_RUNS == portfolioRun.getClientInvestmentAccount().getServiceProcessingType().getProcessingType()) {
				return context.getContext().getOrSupplyBean("PORTFOLIO_RUN_TRADE", () -> getPortfolioRunTradeService().getPortfolioRunTradeHandlerForRun(portfolioRun.getId()).getPortfolioRunTrade(portfolioRun, true));
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public PortfolioRunTradeService<T, D> getPortfolioRunTradeService() {
		return this.portfolioRunTradeService;
	}


	public void setPortfolioRunTradeService(PortfolioRunTradeService<T, D> portfolioRunTradeService) {
		this.portfolioRunTradeService = portfolioRunTradeService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public TradeAssetClassService getTradeAssetClassService() {
		return this.tradeAssetClassService;
	}


	public void setTradeAssetClassService(TradeAssetClassService tradeAssetClassService) {
		this.tradeAssetClassService = tradeAssetClassService;
	}


	public boolean isApplyRuleIfTradeInSameDirection() {
		return this.applyRuleIfTradeInSameDirection;
	}


	public void setApplyRuleIfTradeInSameDirection(boolean applyRuleIfTradeInSameDirection) {
		this.applyRuleIfTradeInSameDirection = applyRuleIfTradeInSameDirection;
	}


	public List<Short> getInvestmentTypeIdList() {
		return this.investmentTypeIdList;
	}


	public void setInvestmentTypeIdList(List<Short> investmentTypeIdList) {
		this.investmentTypeIdList = investmentTypeIdList;
	}
}
