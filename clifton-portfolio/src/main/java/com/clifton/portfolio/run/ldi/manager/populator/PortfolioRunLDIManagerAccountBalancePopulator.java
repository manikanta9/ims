package com.clifton.portfolio.run.ldi.manager.populator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccount;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIService;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalance;
import com.clifton.portfolio.run.manager.populator.BasePortfolioRunManagerAccountBalancePopulator;
import com.clifton.portfolio.run.manager.populator.PortfolioRunManagerAccountBalancePopulatorConfig;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Component
public class PortfolioRunLDIManagerAccountBalancePopulator extends BasePortfolioRunManagerAccountBalancePopulator<PortfolioRunManagerAccountBalance> {

	private PortfolioRunLDIService portfolioRunLDIService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortfolioRunManagerAccountBalance> populatePortfolioRunManagerAccountBalanceListImpl(PortfolioRunManagerAccountBalancePopulatorConfig config) {
		List<PortfolioRunManagerAccountBalance> list = new ArrayList<>();

		List<PortfolioRunLDIManagerAccount> ldiManagerAccountList = getLDIManagerList(config);
		for (PortfolioRunLDIManagerAccount ldiManagerAccount : CollectionUtils.getIterable(ldiManagerAccountList)) {
			if (ldiManagerAccount.getManagerAccountAssignment() != null) {
				PortfolioRunManagerAccountBalance runManagerBalance = getPopulatedPortfolioRunManagerAccountBalance(config, ldiManagerAccount.getManagerAccountAssignment().getReferenceOne());
				list.add(runManagerBalance);
			}
		}
		return list;
	}


	private List<PortfolioRunLDIManagerAccount> getLDIManagerList(PortfolioRunManagerAccountBalancePopulatorConfig config) {
		return getPortfolioRunLDIService().getPortfolioRunLDIManagerAccountListByRun(config.getPortfolioRun().getId(), false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunLDIService getPortfolioRunLDIService() {
		return this.portfolioRunLDIService;
	}


	public void setPortfolioRunLDIService(PortfolioRunLDIService portfolioRunLDIService) {
		this.portfolioRunLDIService = portfolioRunLDIService;
	}
}
