package com.clifton.portfolio.run.ldi.process.calculators;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccountKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIService;
import com.clifton.portfolio.run.ldi.process.PortfolioRunLDIConfig;
import com.clifton.portfolio.run.process.calculator.BasePortfolioRunProcessStepCalculatorImpl;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>PortfolioRunLDIKeyRateDurationPointCalculator</code> generates the information store
 * in {@link PortfolioRunLDIKeyRateDurationPoint} table which contains aggregates of manager assets, liabilities, and exposure
 * as well as target and rebalance information for each point
 *
 * @author manderson
 */
public class PortfolioRunLDIKeyRateDurationPointCalculator extends BasePortfolioRunProcessStepCalculatorImpl<PortfolioRunLDIConfig> {

	private PortfolioRunLDIService portfolioRunLDIService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void processStep(PortfolioRunLDIConfig runConfig) {
		// Should never happen because it is validated in the manager processing step
		if (CollectionUtils.isEmpty(runConfig.getKeyRateDurationPointList())) {
			throw new ValidationException("There are no Active Key Rate Duration Points set up for this account.");
		}

		setupKeyRateDurationPointList(runConfig);
		applyManagerDv01ValuesToKeyRateDurationPointList(runConfig);
		calculateTargetDv01Values(runConfig);
	}


	@Override
	protected void saveStep(PortfolioRunLDIConfig runConfig) {
		List<PortfolioRunLDIKeyRateDurationPoint> runKrdPointList = runConfig.getRunKeyRateDurationPointList();

		getPortfolioRunLDIService().savePortfolioRunLDIKeyRateDurationPointList(runKrdPointList);

		List<PortfolioRunLDIKeyRateDurationPoint> managedKrdList = runConfig.getManagedRunLDIKeyRateDurationPointList();
		runConfig.getRun().setPortfolioTotalValue(CoreMathUtils.sumProperty(managedKrdList, PortfolioRunLDIKeyRateDurationPoint::getLiabilitiesDv01Value));
		runConfig.getRun().setCashTotal(CoreMathUtils.sumProperty(managedKrdList, PortfolioRunLDIKeyRateDurationPoint::getTargetHedgeDv01Value));

		runConfig.setRun(getPortfolioRunService().savePortfolioRun(runConfig.getRun()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Helper Methods                           ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates the list of PortfolioRunLDIKeyRateDurationPoint objects for the run
	 * with base target/rebalance information from account setup
	 */
	private void setupKeyRateDurationPointList(PortfolioRunLDIConfig runConfig) {
		for (PortfolioAccountKeyRateDurationPoint point : CollectionUtils.getIterable(runConfig.getKeyRateDurationPointList())) {
			runConfig.addPortfolioRunLDIKeyRateDurationPoint(createPortfolioRunLDIKeyRateDurationPoint(point));
		}
		for (PortfolioAccountKeyRateDurationPoint point : CollectionUtils.getIterable(runConfig.getReportableOnlyKeyRateDurationPointList())) {
			runConfig.addPortfolioRunLDIKeyRateDurationPoint(createPortfolioRunLDIKeyRateDurationPoint(point));
		}
	}


	private PortfolioRunLDIKeyRateDurationPoint createPortfolioRunLDIKeyRateDurationPoint(PortfolioAccountKeyRateDurationPoint point) {
		PortfolioRunLDIKeyRateDurationPoint runPoint = new PortfolioRunLDIKeyRateDurationPoint();
		runPoint.setKeyRateDurationPoint(point);
		runPoint.setTargetHedgePercent(point.getTargetHedgePercent());
		runPoint.setTargetHedgePercentAdjusted(point.getTargetHedgePercent());
		runPoint.setRebalanceMinPercent(point.getRebalanceMinPercent());
		runPoint.setRebalanceMaxPercent(point.getRebalanceMaxPercent());

		// Default Aggregate DV01 Values to 0
		runPoint.setLiabilitiesDv01Value(BigDecimal.ZERO);
		runPoint.setAssetsDv01Value(BigDecimal.ZERO);
		runPoint.setExposureDv01Value(BigDecimal.ZERO);
		return runPoint;
	}


	/**
	 * Applies Manager DV01 values to Run Point List for assets and liabilities
	 */
	private void applyManagerDv01ValuesToKeyRateDurationPointList(PortfolioRunLDIConfig runConfig) {
		for (PortfolioRunLDIManagerAccountKeyRateDurationPoint managerPoint : CollectionUtils.getIterable(runConfig.getRunManagerPointList())) {
			for (PortfolioRunLDIKeyRateDurationPoint runPoint : CollectionUtils.getIterable(runConfig.getRunKeyRateDurationPointList())) {
				if (managerPoint.getKeyRateDurationPoint().equals(runPoint.getKeyRateDurationPoint())) {
					if (ManagerCashTypes.LIABILITIES == managerPoint.getManagerAccount().getCashType()) {
						runPoint.setLiabilitiesDv01Value(MathUtils.add(runPoint.getLiabilitiesDv01Value(), managerPoint.getDv01Value()));
					}
					else {
						runPoint.setAssetsDv01Value(MathUtils.add(runPoint.getAssetsDv01Value(), managerPoint.getDv01Value()));
					}
					break;
				}
			}
		}
	}


	/**
	 * Calculates Target DV01 Values
	 */
	private void calculateTargetDv01Values(PortfolioRunLDIConfig runConfig) {
		for (PortfolioRunLDIKeyRateDurationPoint runPoint : CollectionUtils.getIterable(runConfig.getRunKeyRateDurationPointList())) {
			runPoint.setTargetHedgeDv01Value(MathUtils.getPercentageOf(runPoint.getTargetHedgePercent(), runPoint.getLiabilitiesDv01Value(), true));
			runPoint.setTargetHedgeDv01ValueAdjusted(MathUtils.getPercentageOf(runPoint.getTargetHedgePercentAdjusted(), runPoint.getLiabilitiesDv01Value(), true));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunLDIService getPortfolioRunLDIService() {
		return this.portfolioRunLDIService;
	}


	public void setPortfolioRunLDIService(PortfolioRunLDIService portfolioRunLDIService) {
		this.portfolioRunLDIService = portfolioRunLDIService;
	}
}
