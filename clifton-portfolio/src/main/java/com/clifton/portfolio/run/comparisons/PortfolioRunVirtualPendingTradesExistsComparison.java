package com.clifton.portfolio.run.comparisons;


import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocationSearchForm;
import com.clifton.trade.assetclass.TradeAssetClassService;

import java.util.List;


/**
 * The <code>PortfolioRunVirtualPendingTradesExistsComparison</code> checks for the account on the run if there are any pending (un-processed)
 * virtual (not associated with a real trade) trade allocations entered.
 *
 * @author manderson
 */
public class PortfolioRunVirtualPendingTradesExistsComparison implements Comparison<PortfolioRun> {

	private TradeAssetClassService tradeAssetClassService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(PortfolioRun run, ComparisonContext context) {
		AssertUtils.assertNotNull(run, "Portfolio Run is required to evaluate virtual trade comparison for it.");

		List<TradeAssetClassPositionAllocation> tradeAllocationList = getVirtualPendingTradeAllocationList(run);
		boolean result = !CollectionUtils.isEmpty(tradeAllocationList);
		if (isReverse()) {
			result = !result;
		}
		if (context != null) {
			String message = "[" + CollectionUtils.getSize(tradeAllocationList) + "] " + " Virtual Pending Trade Allocations found for portfolio run.";
			if (result) {
				context.recordTrueMessage(message);
			}
			else {
				context.recordFalseMessage(message);
			}
		}
		return result;
	}


	private boolean isReverse() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<TradeAssetClassPositionAllocation> getVirtualPendingTradeAllocationList(PortfolioRun run) {
		TradeAssetClassPositionAllocationSearchForm searchForm = new TradeAssetClassPositionAllocationSearchForm();
		searchForm.setAccountId(run.getClientInvestmentAccount().getId());
		searchForm.setProcessed(false);
		searchForm.setVirtualTradesOnly(true);

		return getTradeAssetClassService().getTradeAssetClassPositionAllocationList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public TradeAssetClassService getTradeAssetClassService() {
		return this.tradeAssetClassService;
	}


	public void setTradeAssetClassService(TradeAssetClassService tradeAssetClassService) {
		this.tradeAssetClassService = tradeAssetClassService;
	}
}
