package com.clifton.portfolio.run.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Base Rule evaluator for checking if total Fund cash for a portfolio run is negative. Create concrete implementations to determine how the rule should extract the run exposure
 * summaries from the run.
 *
 * @author mitchellf
 */
public abstract class PortfolioRunFundCashNegativeRuleEvaluator<T extends BasePortfolioRunExposureSummary> extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<?, ?>> {


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<?, ?> context) {
		List<RuleViolation> ruleViolations = new ArrayList<>();
		List<T> summaryList = getSummaryList(run);
		if (!CollectionUtils.isEmpty(summaryList)) {
			EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(run));
			BigDecimal sum = CoreMathUtils.sumProperty(summaryList, this::getFundCash);
			if (MathUtils.isNegative(sum)) {
				Map<String, Object> contextValueMap = new HashMap<>();
				contextValueMap.put("clientAccount", run.getClientInvestmentAccount().getLabel());
				contextValueMap.put("balanceDate", run.getBalanceDate());
				contextValueMap.put("totalFundCash", sum);
				ruleViolations.add(getRuleViolationService().createRuleViolation(entityConfig, run.getId(), contextValueMap));
			}
		}
		return ruleViolations;
	}


	protected BigDecimal getFundCash(T summary) {
		return summary.getFundCash();
	}


	protected abstract List<T> getSummaryList(PortfolioRun run);
}
