package com.clifton.portfolio.run.process.calculator;


import com.clifton.core.util.CollectionUtils;
import com.clifton.portfolio.run.margin.PortfolioRunMargin;
import com.clifton.portfolio.run.margin.PortfolioRunMarginHandler;
import com.clifton.portfolio.run.margin.PortfolioRunMarginService;
import com.clifton.portfolio.run.process.PortfolioRunConfig;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>PortfolioRunMarginCalculator</code> ...
 *
 * @author manderson
 */
public class PortfolioRunMarginCalculator<T extends PortfolioRunConfig> extends BasePortfolioRunProcessStepCalculatorImpl<T> {

	public static final String FIELD_RUN_MARGIN_ANALYSIS = "Perform Margin Analysis";
	public static final String FIELD_RECOMMENDED_VARIATION_MARGIN_PERCENTAGE = "Recommended Variation Margin %";

	private PortfolioRunMarginService portfolioRunMarginService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void processStep(T runConfig) {
		// Check if the Client Account is set up to perform margin analysis
		// If not, just mark it as completed
		Boolean runStep = (Boolean) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(runConfig.getRun().getClientInvestmentAccount(), FIELD_RUN_MARGIN_ANALYSIS);
		if (runStep != null && !runStep) {
			return;
		}

		// Percentage of Margin Basis Used to Determine Recommended Margin
		runConfig.setMarginRecommendedPercent((BigDecimal) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(runConfig.getRun().getClientInvestmentAccount(),
				FIELD_RECOMMENDED_VARIATION_MARGIN_PERCENTAGE));

		runConfig.setRunMarginList(generatePortfolioRunMarginList(runConfig));
	}


	@Override
	protected void saveStep(T runConfig) {
		if (!CollectionUtils.isEmpty(runConfig.getRunMarginList())) {
			getPortfolioRunMarginService().savePortfolioRunMarginListByRun(runConfig.getRun().getId(), runConfig.getRunMarginList());
		}
	}


	@SuppressWarnings("unchecked")
	private List<PortfolioRunMargin> generatePortfolioRunMarginList(T runConfig) {
		PortfolioRunMarginHandler<T> runMarginHandler = getPortfolioRunMarginService().getPortfolioRunMarginHandlerForRun(runConfig.getRun());
		return runMarginHandler.generatePortfolioRunMarginList(runConfig);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                  Getter and Setter Methods                 ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunMarginService getPortfolioRunMarginService() {
		return this.portfolioRunMarginService;
	}


	public void setPortfolioRunMarginService(PortfolioRunMarginService portfolioRunMarginService) {
		this.portfolioRunMarginService = portfolioRunMarginService;
	}
}
