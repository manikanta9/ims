package com.clifton.portfolio.run.rule;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocationSearchForm;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocationService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAccountRebalance;
import com.clifton.investment.manager.InvestmentManagerAccountRollup;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceSearchForm;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccount;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIService;
import com.clifton.portfolio.run.manager.BasePortfolioRunManagerAccount;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalance;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalanceSearchForm;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalanceService;
import com.clifton.portfolio.run.margin.PortfolioRunMargin;
import com.clifton.portfolio.run.margin.PortfolioRunMarginService;
import com.clifton.portfolio.run.search.PortfolioRunSearchForm;
import com.clifton.rule.evaluator.BaseRuleEvaluatorContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Used by all Portfolio Run Rule evaluation and contains common information (easily stored/retrieved from context) and re-used across various rule evaluations
 *
 * @author manderson
 */
@SuppressWarnings("unchecked")
public class PortfolioRunRuleEvaluatorContext<T extends BasePortfolioRunExposureSummary, D extends BasePortfolioRunReplication> extends BaseRuleEvaluatorContext {

	//PortfolioRun RuleContextNames
	public static final String RUN_ID = "runId";
	public static final String BALANCE_DATE = "balanceDate";
	public static final String BASE_UNREALIZED_CURRENCY_EXPOSURE_MAP = "BaseUnrealizedCurrencyExposureMap";
	public static final String CLIENT_ACCOUNT_LIST = "clientAccountList";
	public static final String EXCHANGE_RATE_MAP = "exchangeRateMap";
	public static final String LDI_EXPOSURE_LIST = "ldiExposureList";
	public static final String LDI_MANAGER_ACCOUNT_LIST = "ldiManagerAccountList";
	public static final String MAIN_CLIENT_ACCOUNT = "mainClientAccount";
	public static final String MANAGER_ACCOUNT_BALANCE_MAP = "InvestmentManagerAccountBalanceMap";
	public static final String MANAGER_ACCOUNT_MAP = "managerAccountMap";
	public static final String MANAGER_ACCOUNT_WITH_ACTIVE_ASSIGNMENT_LIST = "managerAccountWithActiveAssignmentList";
	public static final String MARGIN_LIST = "marginList";
	public static final String PORTFOLIO_ACCOUNT_CONTRACT_STORE = "portfolioAccountContractStore";
	public static final String PORTFOLIO_ACCOUNT_CONTRACT_STORE_CURRENT = "portfolioAccountContractStoreCurrent";
	public static final String PORTFOLIO_MANAGER_ACCOUNT_BALANCE_LIST = "PortfolioRunManagerAccountBalanceList";
	public static final String POSITION_LIST = "positionList";
	private static final String BASE_OVERLAY_EXPOSURE_MAP = "BaseOverlayExposureMap";
	private static final String BASE_OVERLAY_TARGET_EXPOSURE_MAP = "BaseOverlayTargetExposureMap";


	public static final String ACCOUNT_ASSET_CLASS_LIST = "InvestmentAccountAssetClassList";
	public static final String ACCOUNT_ASSET_CLASS_POSITION_ALLOCATION_LIST = "InvestmentAccountAssetClassPositionAllocationList";


	/**
	 * Note: The previous day's run for this run.  Used for comparing changes in values from previous day
	 */
	public static final String PREVIOUS_RUN = "PREVIOUS_RUN";
	public static final String PREVIOUS_RUN_BY_CREATE_DATE = "PREVIOUS_RUN_BY_CREATE_DATE";
	public static final String PREVIOUS_PORTFOLIO_MANAGER_ACCOUNT_BALANCE_LIST = "PreviousPortfolioRunManagerAccountBalanceList";
	public static final String PREVIOUS_POSITION_LIST = "PreviousPositionList";

	/// Overlay Keys Although Code That Populates these lists is not here
	public static final String OVERLAY_MANAGER_LIST = "ProductOverlayManagerList";
	public static final String PREVIOUS_OVERLAY_MANAGER_LIST = "PreviousProductOverlayManagerList";

	public static final String OVERLAY_ASSET_CLASS_LIST = "ProductOverlayAssetClassList";
	public static final String PREVIOUS_OVERLAY_ASSET_CLASS_LIST = "PreviousProductOverlayAssetClassList";

	public static final String OVERLAY_ASSET_CLASS_REPLICATION_LIST = "ProductOverlayAssetClassReplicationList";
	public static final String PREVIOUS_OVERLAY_ASSET_CLASS_REPLICATION_LIST = "PreviousProductOverlayAssetClassReplicationList";

	public static final String ACCOUNTING_POSITION_DAILY_LIVE_LIST = "AccountingPositionDailyLiveList";

	private static final String CURRENCY_OTHER_LIST = "CurrencyOtherList";

	private AccountingPositionDailyService accountingPositionDailyService;

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentAccountAssetClassService investmentAccountAssetClassService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountAssetClassPositionAllocationService investmentAccountAssetClassPositionAllocationService;
	private InvestmentCalculator investmentCalculator;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	private InvestmentManagerAccountService investmentManagerAccountService;
	private InvestmentReplicationService investmentReplicationService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private MarketDataRetriever marketDataRetriever;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;
	private PortfolioRunLDIService portfolioRunLDIService;
	private PortfolioRunManagerAccountBalanceService<? extends PortfolioRunManagerAccountBalance> portfolioRunManagerAccountBalanceService;
	private PortfolioRunMarginService portfolioRunMarginService;
	private PortfolioRunService portfolioRunService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	public List<AccountingPositionDaily> getAccountPositionDailyLiveList(PortfolioRun run) {
		List<AccountingPositionDaily> accountingPositionDailyLiveList = (List<AccountingPositionDaily>) getContext().getBean(ACCOUNTING_POSITION_DAILY_LIVE_LIST);
		if (accountingPositionDailyLiveList == null) {
			AccountingPositionDailyLiveSearchForm accountingPositionDailyLiveSearchForm = new AccountingPositionDailyLiveSearchForm();
			accountingPositionDailyLiveSearchForm.setClientAccountId(getMainClientAccount(run).getId());
			accountingPositionDailyLiveSearchForm.setSnapshotDate(getBalanceDate(run));
			accountingPositionDailyLiveSearchForm.setUseSettlementDate(true);
			accountingPositionDailyLiveList = getAccountingPositionDailyService().getAccountingPositionDailyLiveList(accountingPositionDailyLiveSearchForm);
			if (accountingPositionDailyLiveList == null) {
				accountingPositionDailyLiveList = new ArrayList<>();
			}
			getContext().setBean(ACCOUNTING_POSITION_DAILY_LIVE_LIST, accountingPositionDailyLiveList);
		}
		return accountingPositionDailyLiveList;
	}


	@SuppressWarnings("unchecked")
	public List<PortfolioRunLDIManagerAccount> getPortfolioRunLDIManagerAccountListForRun(PortfolioRun run) {
		List<PortfolioRunLDIManagerAccount> list = (List<PortfolioRunLDIManagerAccount>) getContext().getBean(LDI_MANAGER_ACCOUNT_LIST);
		if (list == null) {
			list = getPortfolioRunLDIService().getPortfolioRunLDIManagerAccountListByRun(run.getId(), false);
			if (list == null) {
				list = new ArrayList<>();
			}
			getContext().setBean(LDI_MANAGER_ACCOUNT_LIST, list);
		}
		return list;
	}


	@SuppressWarnings("unchecked")
	public List<PortfolioRunLDIExposure> getPortfolioRunLDIExposureListForRun(PortfolioRun run) {
		List<PortfolioRunLDIExposure> list = (List<PortfolioRunLDIExposure>) getContext().getBean(LDI_EXPOSURE_LIST);
		if (list == null) {
			list = getPortfolioRunLDIService().getPortfolioRunLDIExposureListByRun(run.getId(), false);
			if (list == null) {
				list = new ArrayList<>();
			}
			getContext().setBean(LDI_EXPOSURE_LIST, list);
		}
		return list;
	}


	public Map<InvestmentSecurity, BigDecimal> getBaseUnrealizedCurrencyExposureMap(PortfolioRun run) {
		Map<InvestmentSecurity, BigDecimal> unrealizedCurrencyExposureMap = (Map<InvestmentSecurity, BigDecimal>) getContext().getBean(BASE_UNREALIZED_CURRENCY_EXPOSURE_MAP);
		if (unrealizedCurrencyExposureMap == null) {
			unrealizedCurrencyExposureMap = new HashMap<>();
			Date balanceDate = getBalanceDate(run);
			InvestmentAccount clientAccount = getMainClientAccount(run);
			InvestmentSecurity baseCurrency = clientAccount.getBaseCurrency();
			PortfolioAccountContractStore contractStore = getPortfolioAccountContractStoreActual(clientAccount, balanceDate, true);
			//Get the securityStore, but filter the list to only contain those for the main client account (do not include sub accounts from the relationships)
			for (PortfolioAccountContractStore.PortfolioAccountContractSecurityStore securityStore : CollectionUtils.getIterable(contractStore.getSecurityStoreCollection())) {
				if (MathUtils.isEqual(run.getClientInvestmentAccount().getId(), securityStore.getClientAccountId())) {
					InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityStore.getSecurityId());
					BigDecimal exchangeRate = getExchangeRate(clientAccount, contractStore.getExchangeRateDataSourceName(), security, balanceDate);
					if (security.isCurrency()) {
						BigDecimal unrealizedGainLoss = contractStore.getSecurityQuantityByClientAccountMap()
								.get(run.getClientInvestmentAccount().getId())
								.get(security.getId())
								.getQuantity();
						updateUnrealizedCurrencyExposureMap(unrealizedCurrencyExposureMap, security, unrealizedGainLoss, baseCurrency, exchangeRate);
					}
					else {
						//Ensure the security is a future.
						if (InvestmentUtils.isNoPaymentOnOpen(security)) {
							BigDecimal securityPrice = getMarketDataRetriever().getPriceFlexible(security, balanceDate, true);
							BigDecimal securityQuantity = contractStore.getSecurityQuantityByClientAccountMap()
									.get(run.getClientInvestmentAccount().getId())
									.get(security.getId())
									.getQuantity();
							BigDecimal localNotional = getInvestmentCalculator().calculateNotional(security, securityPrice, securityQuantity, balanceDate);

							//If there are multiple positions, then compute the total remainingCostBasis before computing the unrealized gain/loss
							BigDecimal localCostBasis = BigDecimal.ZERO;
							for (AccountingPosition accountingPosition : securityStore.getPositionList()) {
								localCostBasis = localCostBasis.add(accountingPosition.getRemainingCostBasis());
							}
							BigDecimal unrealizedCurrencyExposure = MathUtils.subtract(localNotional, localCostBasis);
							//Validate
							updateUnrealizedCurrencyExposureMap(unrealizedCurrencyExposureMap, security.getInstrument().getTradingCurrency(), unrealizedCurrencyExposure, baseCurrency, exchangeRate);
						}
					}
				}
			}
			getContext().setBean(BASE_UNREALIZED_CURRENCY_EXPOSURE_MAP, unrealizedCurrencyExposureMap);
		}
		return unrealizedCurrencyExposureMap;
	}


	public BigDecimal getExchangeRate(InvestmentAccount clientAccount, String exchangeRateDataSourceName, InvestmentSecurity security, Date balanceDate) {
		Map<Integer, BigDecimal> exchangeRateMap = (Map<Integer, BigDecimal>) getContext().getBean(EXCHANGE_RATE_MAP);
		if (exchangeRateMap == null) {
			exchangeRateMap = new HashMap<>();
			getContext().setBean(EXCHANGE_RATE_MAP, exchangeRateMap);
		}
		InvestmentSecurity fromCurrency = security.getInstrument().getTradingCurrency();
		BigDecimal exchangeRate = exchangeRateMap.get(fromCurrency.getId());
		if (exchangeRate == null) {
			exchangeRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(exchangeRateDataSourceName, fromCurrency.getSymbol(), clientAccount.getBaseCurrency().getSymbol(), balanceDate));
			if (exchangeRate != null) {
				exchangeRateMap.put(fromCurrency.getId(), exchangeRate);
			}
			getContext().setBean(EXCHANGE_RATE_MAP, exchangeRateMap);
		}
		return exchangeRate;
	}


	public PortfolioAccountContractStore getPortfolioAccountContractStoreActual(InvestmentAccount clientAccount, Date balanceDate, boolean current) {
		PortfolioAccountContractStore contractStore = (PortfolioAccountContractStore) getContext().getBean(current ? PORTFOLIO_ACCOUNT_CONTRACT_STORE_CURRENT : PORTFOLIO_ACCOUNT_CONTRACT_STORE);
		if (contractStore == null) {
			contractStore = getPortfolioAccountDataRetriever().getPortfolioAccountContractStoreActual(clientAccount, balanceDate, current);
			getContext().setBean(current ? PORTFOLIO_ACCOUNT_CONTRACT_STORE_CURRENT : PORTFOLIO_ACCOUNT_CONTRACT_STORE, contractStore);
		}
		return contractStore;
	}


	private void updateUnrealizedCurrencyExposureMap(Map<InvestmentSecurity, BigDecimal> unrealizedCurrencyExposureMap, InvestmentSecurity security,
	                                                 BigDecimal unrealizedCurrencyExposure, InvestmentSecurity baseCurrency, BigDecimal exchangeRate) {
		BigDecimal currentBalance = unrealizedCurrencyExposureMap.get(security);
		if (currentBalance == null) {
			currentBalance = BigDecimal.ZERO;
		}
		//Apply the exchange rate to convert to the base currency of the client account.
		if (!baseCurrency.getSymbol().equals(security.getSymbol())) {
			unrealizedCurrencyExposure = unrealizedCurrencyExposure.multiply(exchangeRate);
		}
		unrealizedCurrencyExposureMap.put(security, currentBalance.add(unrealizedCurrencyExposure));
	}


	public Map<Integer, InvestmentManagerAccount> getManagerAccountMap(PortfolioRun portfolioRun) {
		Map<Integer, InvestmentManagerAccount> managerAccountMap = (Map<Integer, InvestmentManagerAccount>) getContext().getBean(MANAGER_ACCOUNT_MAP);
		if (managerAccountMap == null) {
			managerAccountMap = new HashMap<>();
			List<InvestmentManagerAccount> managerAccountList = new ArrayList<>();
			populateManagerAccountCollections(portfolioRun, managerAccountMap, managerAccountList);
			getContext().setBean(MANAGER_ACCOUNT_MAP, managerAccountMap);
			getContext().setBean(MANAGER_ACCOUNT_WITH_ACTIVE_ASSIGNMENT_LIST, managerAccountList);
		}
		return managerAccountMap;
	}


	public List<InvestmentManagerAccount> getManagerAccountWithActiveAssignmentList(PortfolioRun portfolioRun) {
		List<InvestmentManagerAccount> managerAccountList = (List<InvestmentManagerAccount>) getContext().getBean(MANAGER_ACCOUNT_WITH_ACTIVE_ASSIGNMENT_LIST);
		if (managerAccountList == null) {
			managerAccountList = new ArrayList<>();
			Map<Integer, InvestmentManagerAccount> managerAccountMap = new HashMap<>();
			populateManagerAccountCollections(portfolioRun, managerAccountMap, managerAccountList);
			getContext().setBean(MANAGER_ACCOUNT_MAP, managerAccountMap);
			getContext().setBean(MANAGER_ACCOUNT_WITH_ACTIVE_ASSIGNMENT_LIST, managerAccountList);
		}
		return managerAccountList;
	}


	private void populateManagerAccountCollections(PortfolioRun portfolioRun, Map<Integer, InvestmentManagerAccount> managerAccountMap, List<InvestmentManagerAccount> managerAccountList) {
		List<InvestmentManagerAccountAssignment> investmentManagerAccountAssignmentList = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentListByAccount(getMainClientAccount(portfolioRun).getId(), null);
		for (InvestmentManagerAccountAssignment investmentManagerAccountAssignment : CollectionUtils.getIterable(investmentManagerAccountAssignmentList)) {
			populateManagerAccountCollections(investmentManagerAccountAssignment, managerAccountMap, managerAccountList);
		}
	}


	private void populateManagerAccountCollections(InvestmentManagerAccountAssignment investmentManagerAccountAssignment, Map<Integer, InvestmentManagerAccount> managerAccountMap, List<InvestmentManagerAccount> managerAccountWithActiveAssignmentList) {
		InvestmentManagerAccount investmentManagerAccount = investmentManagerAccountAssignment.getReferenceOne();
		managerAccountMap.put(investmentManagerAccount.getId(), investmentManagerAccount);
		if (!investmentManagerAccountAssignment.isInactive()) {
			managerAccountWithActiveAssignmentList.add(investmentManagerAccount);
		}
		populateManagerAccountObjects(investmentManagerAccount, getContext());
	}


	@SuppressWarnings("unchecked")
	public void populateManagerAccountObjects(InvestmentManagerAccount investmentManagerAccount, Context context) {
		Map<Integer, InvestmentManagerAccount> managerAccountMap = (Map<Integer, InvestmentManagerAccount>) context.getBean(MANAGER_ACCOUNT_MAP);

		if (managerAccountMap == null) {
			managerAccountMap = new HashMap<>();
		}

		if (investmentManagerAccount.getManagerType() == InvestmentManagerAccount.ManagerTypes.ROLLUP) {
			//Since the rollupManagerList isn't populated when retrieving an investmentManagerAccountList need to explicitly load the investmentAccount for 'full' info and replace original in map
			investmentManagerAccount = getInvestmentManagerAccountService().getInvestmentManagerAccount(investmentManagerAccount.getId());
			managerAccountMap.put(investmentManagerAccount.getId(), investmentManagerAccount);
		}
		for (InvestmentManagerAccountRollup investmentManagerAccountRollup : CollectionUtils.getIterable(investmentManagerAccount.getRollupManagerList())) {
			managerAccountMap.put(investmentManagerAccountRollup.getReferenceTwo().getId(), investmentManagerAccountRollup.getReferenceTwo());
			populateManagerAccountObjects(investmentManagerAccountRollup.getReferenceTwo(), context);
		}
	}


	public Date getBalanceDate(PortfolioRun run) {
		Date balanceDate = (Date) getContext().getBean(BALANCE_DATE);
		if (balanceDate == null) {
			balanceDate = run.getBalanceDate();
			getContext().setBean(BALANCE_DATE, balanceDate);
		}
		return balanceDate;
	}


	public InvestmentAccount getMainClientAccount(PortfolioRun run) {
		InvestmentAccount mainClientAccount = (InvestmentAccount) getContext().getBean(MAIN_CLIENT_ACCOUNT);
		if (mainClientAccount == null) {
			mainClientAccount = run.getClientInvestmentAccount();
			getContext().setBean(MAIN_CLIENT_ACCOUNT, mainClientAccount);
		}
		return mainClientAccount;
	}


	@SuppressWarnings("unchecked")
	public List<InvestmentAccount> getClientAccountList(PortfolioRun run) {
		List<InvestmentAccount> clientAccountList = (List<InvestmentAccount>) getContext().getBean(CLIENT_ACCOUNT_LIST);
		if (clientAccountList == null) {
			clientAccountList = new ArrayList<>();
			for (Integer clientAccountId : CollectionUtils.getIterable(getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipConfig(run.getClientInvestmentAccount().getId()).getMainAccountIds())) {
				clientAccountList.add(getInvestmentAccountService().getInvestmentAccount(clientAccountId));
			}
			getContext().setBean(CLIENT_ACCOUNT_LIST, clientAccountList);
		}
		return clientAccountList;
	}


	public PortfolioRun getPreviousRun(PortfolioRun run) {
		PortfolioRun portfolioRun = (PortfolioRun) getContext().getBean(PREVIOUS_RUN);
		if (portfolioRun == null) {
			// Although we've starting adding more restrictions on having runs for every weekday - historically for comparisons we compare to previous business day
			// "holiday" runs may not be added until later, so using previous business day is good comparison;
			Date previousDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(run.getBalanceDate()), -1);
			portfolioRun = getPortfolioRunService().getPortfolioRunByMainRun(run.getClientInvestmentAccount().getId(), previousDay);
			if (portfolioRun != null) {
				getContext().setBean(PREVIOUS_RUN, portfolioRun);
			}
		}
		return portfolioRun;
	}


	//We want to get the previous run created for this client regardless of balance date.
	//This is used in the 'ProductOverlayAssetClassChangedSincePreviousRunRuleEvaluator' code.
	public PortfolioRun getPreviousRunByCreateDate(PortfolioRun run) {
		PortfolioRun portfolioRun = (PortfolioRun) getContext().getBean(PREVIOUS_RUN_BY_CREATE_DATE);
		if (portfolioRun == null) {
			PortfolioRunSearchForm searchForm = new PortfolioRunSearchForm();
			searchForm.setLimit(1);
			searchForm.setOrderBy("createDate:DESC");
			searchForm.setClientInvestmentAccountId(run.getClientInvestmentAccount().getId());
			searchForm.addSearchRestriction("createDate", ComparisonConditions.LESS_THAN, run.getCreateDate());
			portfolioRun = CollectionUtils.getFirstElement(getPortfolioRunService().getPortfolioRunList(searchForm));
			if (portfolioRun != null) {
				getContext().setBean(PREVIOUS_RUN_BY_CREATE_DATE, portfolioRun);
			}
		}
		return portfolioRun;
	}


	public List<PortfolioRunManagerAccountBalance> getPortfolioRunManagerAccountBalanceList(PortfolioRun portfolioRun, boolean previousRun) {
		String key = (previousRun ? PREVIOUS_PORTFOLIO_MANAGER_ACCOUNT_BALANCE_LIST : PORTFOLIO_MANAGER_ACCOUNT_BALANCE_LIST);
		List<PortfolioRunManagerAccountBalance> list = (List<PortfolioRunManagerAccountBalance>) getContext().getBean(key);

		if (list == null) {
			PortfolioRunManagerAccountBalanceSearchForm searchForm = new PortfolioRunManagerAccountBalanceSearchForm();
			searchForm.setRootOnly(false);

			Integer runId = portfolioRun.getId();
			if (previousRun) {
				PortfolioRun previous = getPreviousRun(portfolioRun);
				if (previous != null) {
					runId = previous.getId();
				}
			}

			list = (List<PortfolioRunManagerAccountBalance>) getPortfolioRunManagerAccountBalanceService().getPortfolioRunManagerAccountBalanceList(runId, searchForm);
			if (list == null) {
				list = new ArrayList<>();
			}
			getContext().setBean(key, list);
		}
		return list;
	}


	/**
	 * By default balances don't have adjustments on them because we do a list search for them, but in cases where we need the adjustments
	 * we'll look them up in the context and if not populated, populate them and update the map for reuse
	 */
	public InvestmentManagerAccountBalance getInvestmentManagerAccountBalance(PortfolioRun portfolioRun, int managerAccountId, boolean populateAdjustments) {
		InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceMap(portfolioRun).get(managerAccountId);
		if (balance == null || (populateAdjustments && balance.isAdjusted() && CollectionUtils.isEmpty(balance.getAdjustmentList()))) {
			balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(managerAccountId,
					portfolioRun.getBalanceDate(), populateAdjustments);
			//Was adding in null balances when recalculating based on the populateAdjustments boolean property.
			if (balance != null) {
				((Map<Integer, InvestmentManagerAccountBalance>) getContext().getBean(MANAGER_ACCOUNT_BALANCE_MAP)).put(managerAccountId, balance);
			}
		}
		return balance;
	}


	public Map<Integer, InvestmentManagerAccountBalance> getInvestmentManagerAccountBalanceMap(PortfolioRun portfolioRun) {
		Map<Integer, InvestmentManagerAccountBalance> balanceMap = (Map<Integer, InvestmentManagerAccountBalance>) getContext().getBean(MANAGER_ACCOUNT_BALANCE_MAP);
		if (balanceMap == null) {
			// Note: Is this the best way of doing this?  This makes the lookup dependent on the manager assignments which is what is used
			// to populate the manager table on the run, without making it run processing specific, i.e. ProductOverlayManagerAccount vs. PortfolioRunLDIManagerAccount
			// Also ProductOverlayManagerAccount may contain multiple references to the same manager account based on asset class allocation
			// so we don't want to evaluate the same manager balance twice.  If chose to have run specific implementation, should use PortfolioRunManagerAccountBalanceService
			// which has references to a different balance populator based on the run type (PIOS vs LDI).  Current use pulls manager balance information and asset class information
			// and also populates adjustments (from children of rollups as well) so too much data loaded that is unnecessary for this rule evaluator
			List<InvestmentManagerAccountAssignment> managerAccountAssignmentList =
					getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentListByAccount(portfolioRun.getClientInvestmentAccount().getId(), true);
			if (!CollectionUtils.isEmpty(managerAccountAssignmentList)) {
				InvestmentManagerAccountBalanceSearchForm searchForm = new InvestmentManagerAccountBalanceSearchForm();
				searchForm.setBalanceDate(portfolioRun.getBalanceDate());
				searchForm.setManagerAccountIds(BeanUtils.getPropertyValues(managerAccountAssignmentList, "referenceOne.id", Integer.class));
				List<InvestmentManagerAccountBalance> balanceList = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceList(searchForm);
				if (balanceList != null) {
					balanceMap = BeanUtils.getBeanMap(balanceList, balance -> balance.getManagerAccount().getId());
				}
			}
			if (balanceMap == null) {
				balanceMap = new HashMap<>();
			}
			getContext().setBean(MANAGER_ACCOUNT_BALANCE_MAP, balanceMap);
		}
		return balanceMap;
	}


	public Collection<InvestmentManagerAccountBalance> getInvestmentManagerAccountBalanceList(PortfolioRun portfolioRun) {
		return getInvestmentManagerAccountBalanceMap(portfolioRun).values();
	}


	public List<PortfolioRunMargin> getPortfolioRunMarginListForRun(PortfolioRun run) {
		List<PortfolioRunMargin> portfolioRunMarginList = (List<PortfolioRunMargin>) getContext().getBean(MARGIN_LIST);
		if (portfolioRunMarginList == null) {
			portfolioRunMarginList = getPortfolioRunMarginService().getPortfolioRunMarginListByRun(run.getId());
			if (portfolioRunMarginList == null) {
				portfolioRunMarginList = new ArrayList<>();
			}
			getContext().setBean(MARGIN_LIST, portfolioRunMarginList);
		}
		return portfolioRunMarginList;
	}


	public List<InvestmentAccountAssetClassPositionAllocation> getPositionAllocationListForRun(PortfolioRun run) {
		List<InvestmentAccountAssetClassPositionAllocation> result = (List<InvestmentAccountAssetClassPositionAllocation>) getContext().getBean(ACCOUNT_ASSET_CLASS_POSITION_ALLOCATION_LIST);
		if (result == null) {
			InvestmentAccountAssetClassPositionAllocationSearchForm searchForm = new InvestmentAccountAssetClassPositionAllocationSearchForm();
			searchForm.setActiveOnDate(run.getBalanceDate());
			searchForm.setAccountId(run.getClientInvestmentAccount().getId());
			result = getInvestmentAccountAssetClassPositionAllocationService().getInvestmentAccountAssetClassPositionAllocationList(searchForm);
			if (result == null) {
				result = new ArrayList<>();
			}
			getContext().setBean(ACCOUNT_ASSET_CLASS_POSITION_ALLOCATION_LIST, result);
		}
		return result;
	}


	public List<InvestmentAccountAssetClass> getInvestmentAccountAssetClassListForRun(PortfolioRun run) {
		List<InvestmentAccountAssetClass> result = (List<InvestmentAccountAssetClass>) getContext().getBean(ACCOUNT_ASSET_CLASS_LIST);
		if (result == null) {
			result = getInvestmentAccountAssetClassService().getInvestmentAccountAssetClassListByAccount(run.getClientInvestmentAccount().getId());
			if (result == null) {
				result = new ArrayList<>();
			}
			getContext().setBean(ACCOUNT_ASSET_CLASS_LIST, result);
		}
		return result;
	}


	public List<T> getRunSummaryList(PortfolioRun run, boolean previousRun, Boolean rollupAssetClass) {
		// Nothing here
		return new ArrayList<>();
	}


	public List<D> getReplicationList(PortfolioRun run) {
		return getReplicationList(run, false);
	}


	public List<D> getReplicationList(PortfolioRun run, boolean previousRun) {
		// Nothing here
		return new ArrayList<>();
	}


	@SuppressWarnings("unchecked")
	protected List<PortfolioCurrencyOther> getPortfolioCurrencyOtherListByRun(PortfolioRun run) {
		List<PortfolioCurrencyOther> currencyOtherList = (List<PortfolioCurrencyOther>) getContext().getBean(CURRENCY_OTHER_LIST);
		if (currencyOtherList == null) {
			currencyOtherList = getPortfolioRunService().getPortfolioCurrencyOtherListByRun(run.getId());
			getContext().setBean(CURRENCY_OTHER_LIST, currencyOtherList);
		}
		return currencyOtherList;
	}


	//Adds the computed map to the PortfolioRunContext.context object so it can be used in subsequent rules.
	//Because of project scope this method resides here instead of in the actual PortfolioRunContext class.
	@SuppressWarnings("unchecked")
	protected Map<InvestmentSecurity, BigDecimal> getBaseOverlayExposureMap(PortfolioRun run) {
		Map<InvestmentSecurity, BigDecimal> overlayExposureMap = (Map<InvestmentSecurity, BigDecimal>) getContext().getBean(BASE_OVERLAY_EXPOSURE_MAP);
		if (overlayExposureMap == null) {
			overlayExposureMap = new HashMap<>();
			List<D> assetClassReplicationList = getReplicationList(run);
			for (D assetClassReplication : CollectionUtils.getIterable(assetClassReplicationList)) {
				InvestmentSecurity security = assetClassReplication.getSecurity().getInstrument().getTradingCurrency();
				BigDecimal currentActualExposureAdjusted = overlayExposureMap.get(security);
				if (currentActualExposureAdjusted == null) {
					currentActualExposureAdjusted = BigDecimal.ZERO;
				}
				overlayExposureMap.put(security, currentActualExposureAdjusted.add(assetClassReplication.getActualExposureAdjusted()));
			}
			getContext().setBean(BASE_OVERLAY_EXPOSURE_MAP, overlayExposureMap);
		}
		return overlayExposureMap;
	}


	//Adds the computed map to the PortfolioRunContext.context object so it can be used in subsequent rules.
	//Because of project scope this method resides here instead of in the actual PortfolioRunContext class.
	@SuppressWarnings("unchecked")
	protected Map<InvestmentSecurity, BigDecimal> getBaseOverlayTargetExposureMap(PortfolioRun run) {
		Map<InvestmentSecurity, BigDecimal> overlayTargetExposureMap = (Map<InvestmentSecurity, BigDecimal>) getContext().getBean(BASE_OVERLAY_TARGET_EXPOSURE_MAP);
		if (overlayTargetExposureMap == null) {
			overlayTargetExposureMap = new HashMap<>();
			List<D> assetClassReplicationList = getReplicationList(run);
			for (D assetClassReplication : CollectionUtils.getIterable(assetClassReplicationList)) {
				InvestmentSecurity security = assetClassReplication.getSecurity().getInstrument().getTradingCurrency();
				BigDecimal currentTargetExposureAdjusted = overlayTargetExposureMap.get(security);
				if (currentTargetExposureAdjusted == null) {
					currentTargetExposureAdjusted = BigDecimal.ZERO;
				}
				overlayTargetExposureMap.put(security, currentTargetExposureAdjusted.add(assetClassReplication.getTargetExposureAdjusted()));
			}
			getContext().setBean(BASE_OVERLAY_TARGET_EXPOSURE_MAP, overlayTargetExposureMap);
		}
		return overlayTargetExposureMap;
	}


	public boolean isManagerBalanceManuallyAdjusted(BasePortfolioRunManagerAccount manager) {
		InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalance(manager.getOverlayRun(), manager.getManagerAccountAssignment().getReferenceOne().getId(), true);
		if (balance != null) {
			for (InvestmentManagerAccountBalanceAdjustment adj : CollectionUtils.getIterable(balance.getAdjustmentList())) {
				if (!adj.getAdjustmentType().isSystemDefined()) {
					return true;
				}
			}
		}
		return false;
	}


	public <K extends InvestmentManagerAccountRebalance> List<K> getManagerAccountRebalanceList(PortfolioRun run) {
		// nothing yet
		return new ArrayList<>();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public InvestmentAccountAssetClassPositionAllocationService getInvestmentAccountAssetClassPositionAllocationService() {
		return this.investmentAccountAssetClassPositionAllocationService;
	}


	public void setInvestmentAccountAssetClassPositionAllocationService(InvestmentAccountAssetClassPositionAllocationService investmentAccountAssetClassPositionAllocationService) {
		this.investmentAccountAssetClassPositionAllocationService = investmentAccountAssetClassPositionAllocationService;
	}


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public PortfolioRunLDIService getPortfolioRunLDIService() {
		return this.portfolioRunLDIService;
	}


	public void setPortfolioRunLDIService(PortfolioRunLDIService portfolioRunLDIService) {
		this.portfolioRunLDIService = portfolioRunLDIService;
	}


	public PortfolioRunMarginService getPortfolioRunMarginService() {
		return this.portfolioRunMarginService;
	}


	public void setPortfolioRunMarginService(PortfolioRunMarginService portfolioRunMarginService) {
		this.portfolioRunMarginService = portfolioRunMarginService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public PortfolioRunManagerAccountBalanceService<? extends PortfolioRunManagerAccountBalance> getPortfolioRunManagerAccountBalanceService() {
		return this.portfolioRunManagerAccountBalanceService;
	}


	public void setPortfolioRunManagerAccountBalanceService(PortfolioRunManagerAccountBalanceService<? extends PortfolioRunManagerAccountBalance> portfolioRunManagerAccountBalanceService) {
		this.portfolioRunManagerAccountBalanceService = portfolioRunManagerAccountBalanceService;
	}
}
