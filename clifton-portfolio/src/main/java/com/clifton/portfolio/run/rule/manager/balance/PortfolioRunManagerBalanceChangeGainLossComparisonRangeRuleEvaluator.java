package com.clifton.portfolio.run.rule.manager.balance;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalance;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The PortfolioRunManagerBalanceChangeGainLossComparisonRangeRuleEvaluator class comparison for the run the account's (include sub-accounts) gain/loss vs. the manager's (flagged as our company) change
 * Comparison is done from previous business day's main run.  Value is calculated as a percentage change
 *
 * @author manderson
 */
public class PortfolioRunManagerBalanceChangeGainLossComparisonRangeRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private AccountingPositionDailyService accountingPositionDailyService;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			PortfolioRun previousRun = context.getPreviousRun(run);
			if (previousRun != null) {
				// Manager Change
				BigDecimal previousManagerValue = getOurManagerAccountValue(run, true, context);
				BigDecimal currentManagerValue = getOurManagerAccountValue(run, false, context);
				BigDecimal managerChange = MathUtils.subtract(currentManagerValue, previousManagerValue);
				BigDecimal managerPercentChange = CoreMathUtils.getPercentValue(managerChange, previousManagerValue, true);

				// Gain/Loss
				List<InvestmentAccount> accountList = context.getClientAccountList(run);
				BigDecimal gainLoss = getGainLoss(accountList, previousRun.getBalanceDate(), run.getBalanceDate());
				BigDecimal gainLossPercent = CoreMathUtils.getPercentValue(gainLoss, previousManagerValue, true);
				BigDecimal change = MathUtils.subtract(managerPercentChange, gainLossPercent);

				Map<String, Object> templateValues = new HashMap<>();
				if (RuleEvaluatorUtils.isEntityConfigRangeViolated(change, entityConfig, templateValues)) {
					templateValues.put("managerChange", managerChange);
					templateValues.put("gainLoss", gainLoss);
					ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, run.getId(), templateValues));
				}
			}
		}
		return ruleViolationList;
	}


	private BigDecimal getOurManagerAccountValue(PortfolioRun run, boolean previousRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<PortfolioRunManagerAccountBalance> balanceList = context.getPortfolioRunManagerAccountBalanceList(run, previousRun);
		BigDecimal total = BigDecimal.ZERO;
		for (PortfolioRunManagerAccountBalance balance : CollectionUtils.getIterable(balanceList)) {
			if (balance.getManagerAccountBalance().getManagerAccount().getManagerCompany().getType().isOurCompany()) {
				total = MathUtils.add(total, balance.getCashAllocationAmount());
			}
		}
		return total;
	}


	/**
	 * Note: This matches how SQL evaluated this rule - if needed elsewhere should put into context but for now since not the most efficient and only used here going to do an explicit look up
	 *
	 * @param clientAccountList
	 * @param fromDate
	 * @param toDate
	 */
	private BigDecimal getGainLoss(List<InvestmentAccount> clientAccountList, Date fromDate, Date toDate) {
		BigDecimal total = BigDecimal.ZERO;
		for (InvestmentAccount account : CollectionUtils.getIterable(clientAccountList)) {
			AccountingPositionDailyLiveSearchForm searchForm = new AccountingPositionDailyLiveSearchForm();
			searchForm.setFromSnapshotDate(DateUtils.addDays(fromDate, 1));
			searchForm.setSnapshotDate(toDate);
			searchForm.setClientAccountId(account.getId());
			List<AccountingPositionDaily> list = getAccountingPositionDailyService().getAccountingPositionDailyLiveList(searchForm);
			total = MathUtils.add(total, CoreMathUtils.sumProperty(list, AccountingPositionDaily::getDailyGainLossBase));
		}
		return total;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}
}
