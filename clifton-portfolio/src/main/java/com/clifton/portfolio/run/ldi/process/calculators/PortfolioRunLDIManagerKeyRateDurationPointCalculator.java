package com.clifton.portfolio.run.ldi.process.calculators;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationPoint;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationSecurity;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroup;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistorySummary;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccount;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccountKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.process.PortfolioRunLDIConfig;
import com.clifton.portfolio.run.ldi.process.calculators.cashflow.PortfolioRunCashFlowLiabilityCalculator;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalanceService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * The <code>PortfolioRunLDIManagerKeyRateDurationPointCalculator</code> calculates and populates data in {@link PortfolioRunLDIManagerAccount} and {@link PortfolioRunLDIManagerAccountKeyRateDurationPoint}
 * for each run.  This data takes active manager assignments for the account, and applies Key Rate Duration Point calculations to determine KRD and DV01 values for each manager balance and KRD Point.
 *
 * @author manderson
 */
@Component
public class PortfolioRunLDIManagerKeyRateDurationPointCalculator extends BasePortfolioRunLDICalculator implements PortfolioRunCashFlowLiabilityCalculator {

	private PortfolioRunManagerAccountBalanceService<?> portfolioRunManagerAccountBalanceService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void processStep(PortfolioRunLDIConfig runConfig) {
		if (CollectionUtils.isEmpty(runConfig.getKeyRateDurationPointList())) {
			throw new ValidationException("There are no Active Key Rate Duration Points set up for this account.");
		}
		List<InvestmentManagerAccountAssignment> managerAssignmentList = getManagerAssignmentList(runConfig);
		for (InvestmentManagerAccountAssignment assignment : managerAssignmentList) {
			// Skip Margin Manager
			if (ManagerCashTypes.MARGIN == assignment.getCashType()) {
				continue;
			}
			if (assignment.getBenchmarkSecurity() == null) {
				throw new ValidationException("Missing Benchmark Security selection for Manager [" + assignment.getManagerAccountLabel() + "].");
			}
			InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(assignment.getReferenceOne().getId(),
					runConfig.getBalanceDate(), false);
			if (balance == null) {
				throw new ValidationException("Missing Balance for Manager [" + assignment.getManagerAccountLabel() + "] on [" + DateUtils.fromDateShort(runConfig.getBalanceDate()));
			}
			PortfolioRunLDIManagerAccount manager = new PortfolioRunLDIManagerAccount();
			manager.setManagerAccountAssignment(assignment);
			manager.setBenchmarkSecurity(assignment.getBenchmarkSecurity());
			manager.setCashTotal(getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, balance.getManagerAccount(), balance.getBalanceDate(), (runConfig.getRun().isMarketOnCloseAdjustmentsIncluded() ? balance.getMarketOnCloseCashValue() : balance.getAdjustedCashValue())));
			manager.setSecuritiesTotal(getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, balance.getManagerAccount(), balance.getBalanceDate(), (runConfig.getRun().isMarketOnCloseAdjustmentsIncluded() ? balance.getMarketOnCloseSecuritiesValue() : balance.getAdjustedSecuritiesValue())));
			runConfig.addPortfolioRunManager(manager);
			populateManagerKeyRateDurationPoints(manager, runConfig);
		}

		calculateCashFlowLiabilityValues(runConfig);
	}


	@Override
	protected void saveStep(PortfolioRunLDIConfig runConfig) {
		getPortfolioRunLDIService().savePortfolioRunLDIManagerAccountList(runConfig.getRunManagerList());
		getPortfolioRunLDIService().savePortfolioRunLDIManagerAccountKeyRateDurationPointList(runConfig.getRunManagerPointList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRunLDIConfig calculateCashFlowLiabilityValues(PortfolioRunLDIConfig runConfig) {
		Map<PortfolioCashFlowGroup, List<PortfolioCashFlowGroupHistorySummary>> cashFlowGroupSummaryListMap = getCashFlowHistorySummaryListMap(runConfig);
		if (!CollectionUtils.isEmpty(cashFlowGroupSummaryListMap)) {
			for (List<PortfolioCashFlowGroupHistorySummary> cashFlowGroupSummaryList : cashFlowGroupSummaryListMap.values()) {
				PortfolioCashFlowGroupHistory groupHistory = Objects.requireNonNull(CollectionUtils.getFirstElement(cashFlowGroupSummaryList)).getCashFlowGroupHistory();
				if (groupHistory != null) {
					PortfolioRunLDIManagerAccount manager = new PortfolioRunLDIManagerAccount();
					manager.setCashFlowGroupHistory(groupHistory);
					manager.setKrdValueDate(groupHistory.getDiscountCurveValueDate());
					runConfig.addPortfolioRunManager(manager);
					populateManagerKeyRateDurationPoints(manager, runConfig);
				}
			}
		}
		return runConfig;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Calculation Helper Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * For the manager balance, populates the list KRD Points and values
	 */
	private void populateManagerKeyRateDurationPoints(PortfolioRunLDIManagerAccount manager, PortfolioRunLDIConfig runConfig) {
		if (manager.getBenchmarkSecurity() == null) {
			populateManagerKeyRateDurationPointsForCashFlowGroupHistory(manager, runConfig);
		}
		else {
			populateManagerKeyRateDurationPointsForBenchmarkSecurity(manager, runConfig);
		}
	}


	private void populateManagerKeyRateDurationPointsForCashFlowGroupHistory(PortfolioRunLDIManagerAccount manager, PortfolioRunLDIConfig runConfig) {
		List<PortfolioCashFlowGroupHistorySummary> groupHistorySummaryList = getCashFlowHistorySummaryListMap(runConfig).get(manager.getCashFlowGroupHistory().getCashFlowGroup());
		Map<MarketDataField, BigDecimal> krdMap = new HashMap<>();
		BigDecimal cashTotal = BigDecimal.ZERO;
		for (PortfolioCashFlowGroupHistorySummary summary : CollectionUtils.getIterable(groupHistorySummaryList)) {
			krdMap.put(summary.getMarketDataField(), summary.getTotalKrdValue());
			cashTotal = MathUtils.add(cashTotal, summary.getTotalLiabilityValue());
		}
		manager.setCashTotal(cashTotal);
		manager.setSecuritiesTotal(BigDecimal.ZERO);
		for (Map.Entry<PortfolioAccountKeyRateDurationPoint, BigDecimal> pointEntry : getPortfolioAccountLDIService().getKeyRateDurationPointModifiedValueMap(runConfig.getKeyRateDurationPointList(), krdMap).entrySet()) {
			runConfig.addPortfolioRunManagerKeyRateDurationPoint(manager, createManagerKeyRateDurationPoint(manager, pointEntry));
		}
		if (!CollectionUtils.isEmpty(runConfig.getReportableOnlyKeyRateDurationPointList())) {
			for (Map.Entry<PortfolioAccountKeyRateDurationPoint, BigDecimal> pointEntry : getPortfolioAccountLDIService().getKeyRateDurationPointModifiedValueMap(runConfig.getReportableOnlyKeyRateDurationPointList(), krdMap).entrySet()) {
				runConfig.addPortfolioRunManagerKeyRateDurationPoint(manager, createManagerKeyRateDurationPoint(manager, pointEntry));
			}
		}
	}


	private void populateManagerKeyRateDurationPointsForBenchmarkSecurity(PortfolioRunLDIManagerAccount manager, PortfolioRunLDIConfig runConfig) {
		PortfolioAccountKeyRateDurationSecurity securityKRD = getModifiedKRDResultForSecurity(manager.getBenchmarkSecurity(), runConfig);
		manager.setKrdValueDate(securityKRD.getValueDate());
		for (Map.Entry<PortfolioAccountKeyRateDurationPoint, BigDecimal> pointEntry : securityKRD.getKrdValueMap().entrySet()) {
			runConfig.addPortfolioRunManagerKeyRateDurationPoint(manager, createManagerKeyRateDurationPoint(manager, pointEntry));
		}

		PortfolioAccountKeyRateDurationSecurity reportableOnlySecurityKRD = getReportableOnlyModifiedKRDResultForSecurity(manager.getBenchmarkSecurity(), runConfig);
		if (reportableOnlySecurityKRD != null) {
			for (Map.Entry<PortfolioAccountKeyRateDurationPoint, BigDecimal> pointEntry : reportableOnlySecurityKRD.getKrdValueMap().entrySet()) {
				runConfig.addPortfolioRunManagerKeyRateDurationPoint(manager, createManagerKeyRateDurationPoint(manager, pointEntry));
			}
		}
	}


	private PortfolioRunLDIManagerAccountKeyRateDurationPoint createManagerKeyRateDurationPoint(PortfolioRunLDIManagerAccount manager, Map.Entry<PortfolioAccountKeyRateDurationPoint, BigDecimal> pointEntry) {
		PortfolioRunLDIManagerAccountKeyRateDurationPoint managerAccountKeyRateDurationPoint = new PortfolioRunLDIManagerAccountKeyRateDurationPoint();
		managerAccountKeyRateDurationPoint.setKeyRateDurationPoint(pointEntry.getKey());
		managerAccountKeyRateDurationPoint.setKrdValue(pointEntry.getValue());
		// DV01 is the dollar value of one basis point, so to get this Value we divide by 10,000
		managerAccountKeyRateDurationPoint.setDv01Value(MathUtils.getDV01Value(MathUtils.multiply(manager.getTotalValue(), managerAccountKeyRateDurationPoint.getKrdValue())));
		return managerAccountKeyRateDurationPoint;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunManagerAccountBalanceService<?> getPortfolioRunManagerAccountBalanceService() {
		return this.portfolioRunManagerAccountBalanceService;
	}


	public void setPortfolioRunManagerAccountBalanceService(PortfolioRunManagerAccountBalanceService<?> portfolioRunManagerAccountBalanceService) {
		this.portfolioRunManagerAccountBalanceService = portfolioRunManagerAccountBalanceService;
	}
}
