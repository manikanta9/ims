package com.clifton.portfolio.run.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is used by several system beans to validate currency balances in the clients base currency to ensure
 * individual unrealized gain/loss as well as total unrealized gain/loss does not fall outside of the specified threshold for an account
 *
 * @author stevenf on 8/21/2015.
 */
public class PortfolioRunCurrencyBalanceWithoutCurrencyFutureRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private InvestmentInstrumentService investmentInstrumentService;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();

		EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(run.getClientInvestmentAccount()));
		if (entityConfig != null && !entityConfig.isExcluded()) {
			Date balanceDate = context.getBalanceDate(run);
			InvestmentAccount clientAccount = context.getMainClientAccount(run);
			PortfolioAccountContractStore contractStore = context.getPortfolioAccountContractStoreActual(clientAccount, balanceDate, true);

			Map<InvestmentSecurity, Boolean> currencyBalanceWithoutFutureMap = new HashMap<>();
			for (PortfolioAccountContractStore.PortfolioAccountContractSecurityStore securityStore : CollectionUtils.getIterable(contractStore.getSecurityStoreCollection())) {
				InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityStore.getSecurityId());
				if (security.isCurrency()) {
					currencyBalanceWithoutFutureMap.putIfAbsent(security, true);
				}
				else {
					//Ensure the security is a future.
					if (InvestmentUtils.isNoPaymentOnOpen(security)) {
						Boolean hasFuture = currencyBalanceWithoutFutureMap.get(security.getInstrument().getTradingCurrency());
						if (hasFuture == null || hasFuture) {
							currencyBalanceWithoutFutureMap.put(security.getInstrument().getTradingCurrency(), false);
						}
					}
				}
			}
			for (Map.Entry<InvestmentSecurity, Boolean> investmentSecurityBooleanEntry : currencyBalanceWithoutFutureMap.entrySet()) {
				if (investmentSecurityBooleanEntry.getValue()) {
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), (investmentSecurityBooleanEntry.getKey()).getId()));
				}
			}
		}
		return ruleViolationList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
