package com.clifton.portfolio.run.rule.manager.balance;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.portfolio.run.rule.manager.BasePortfolioRunManagerAccountRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunManagerBalanceAdjustedValuesNotCorrectRuleEvaluator</code> class is a code-based RuleEvaluator.
 * It generates violations during a PortfolioRun for manager account with incorrect adjusted values.
 * This violation should never actually occur - it is tested using bad data in a local jUnit test, rather than a live integration test.
 *
 * @author sfahey
 */
public class PortfolioRunManagerBalanceAdjustedValuesNotCorrectRuleEvaluator extends BasePortfolioRunManagerAccountRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		List<InvestmentManagerAccount> managerAccountList = context.getManagerAccountWithActiveAssignmentList(run);
		for (InvestmentManagerAccount managerAccount : CollectionUtils.getIterable(managerAccountList)) {
			EntityConfig entityConfig = getEntityConfig(managerAccount, ruleConfig);
			if (entityConfig != null && !entityConfig.isExcluded()) {
				InvestmentManagerAccountBalance managerAccountBalance = context.getInvestmentManagerAccountBalance(run, managerAccount.getId(), true);
				if (managerAccountBalance != null) {
					String violationNote = adjustedValuesNotCorrect(managerAccountBalance);
					if (violationNote != null) {
						Map<String, Object> templateValues = new HashMap<>();
						templateValues.put("violationNote", violationNote);
						ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), BeanUtils.getIdentityAsLong(managerAccount), BeanUtils.getIdentityAsLong(managerAccountBalance), null, templateValues));
					}
				}
			}
		}
		return ruleViolationList;
	}


	private String adjustedValuesNotCorrect(InvestmentManagerAccountBalance accountBalance) {
		BigDecimal adjustedCashValue = accountBalance.getCashValue();
		BigDecimal adjustedSecuritiesValue = accountBalance.getSecuritiesValue();
		BigDecimal marketOnCloseCashValue = accountBalance.getCashValue();
		BigDecimal marketOnCloseSecuritiesValue = accountBalance.getSecuritiesValue();
		for (InvestmentManagerAccountBalanceAdjustment adjustment : CollectionUtils.getIterable(accountBalance.getAdjustmentList())) {
			if (!MathUtils.isNullOrZero(adjustment.getCashValue())) {
				if (!adjustment.getAdjustmentType().isMarketOnClose()) {
					adjustedCashValue = MathUtils.add(adjustedCashValue, adjustment.getCashValue());
				}
				marketOnCloseCashValue = MathUtils.add(marketOnCloseCashValue, adjustment.getCashValue());
			}
			if (!MathUtils.isNullOrZero(adjustment.getSecuritiesValue())) {
				if (!adjustment.getAdjustmentType().isMarketOnClose()) {
					adjustedSecuritiesValue = MathUtils.add(adjustedSecuritiesValue, adjustment.getSecuritiesValue());
				}
				marketOnCloseSecuritiesValue = MathUtils.add(marketOnCloseSecuritiesValue, adjustment.getSecuritiesValue());
			}
		}
		StringBuilder incorrectValue = new StringBuilder(16);
		validateCalculatedValue(accountBalance.getAdjustedCashValue(), adjustedCashValue, "Adj Cash", incorrectValue);
		validateCalculatedValue(accountBalance.getAdjustedSecuritiesValue(), adjustedSecuritiesValue, "Adj Securities", incorrectValue);
		validateCalculatedValue(accountBalance.getMarketOnCloseCashValue(), marketOnCloseCashValue, "MOC Cash", incorrectValue);
		validateCalculatedValue(accountBalance.getMarketOnCloseSecuritiesValue(), marketOnCloseSecuritiesValue, "MOC Securities", incorrectValue);
		return incorrectValue.length() > 0 ? incorrectValue.toString() : null;
	}


	private void validateCalculatedValue(BigDecimal balanceValue, BigDecimal calculatedValue, String label, StringBuilder message) {
		if (MathUtils.isNotEqual(balanceValue, calculatedValue)) {
			BigDecimal difference = MathUtils.subtract(balanceValue, calculatedValue);
			message.append(" ").append(label).append(": ");
			message.append(CoreMathUtils.formatNumberMoney(balanceValue)).append(", Calculated: ").append(CoreMathUtils.formatNumberMoney(calculatedValue)).append(", Difference: ").append(CoreMathUtils.formatNumberMoney(difference));
		}
	}
}
