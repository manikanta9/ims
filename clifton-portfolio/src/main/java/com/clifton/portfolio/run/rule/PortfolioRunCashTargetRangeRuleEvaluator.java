package com.clifton.portfolio.run.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.portfolio.run.PortfolioRun;

import java.math.BigDecimal;


/**
 * The <code>PortfolioRunCashTargetRangeRuleEvaluator</code> is used to create rule violations when the cash target value of
 * a portfolio run is outside the minimum or maximum values defined in the rule assignment.
 *
 * @author stevenf
 */
public class PortfolioRunCashTargetRangeRuleEvaluator extends PortfolioRunValueRangeRuleEvaluator {

	private String portfolioRunTargetValueBeanPath;


	@Override
	public BigDecimal getRangeValue(PortfolioRun run) {
		return (BigDecimal) BeanUtils.getPropertyValue(run, getPortfolioRunTargetValueBeanPath());
	}

	///////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getPortfolioRunTargetValueBeanPath() {
		return this.portfolioRunTargetValueBeanPath;
	}


	public void setPortfolioRunTargetValueBeanPath(String portfolioRunTargetValueBeanPath) {
		this.portfolioRunTargetValueBeanPath = portfolioRunTargetValueBeanPath;
	}
}
