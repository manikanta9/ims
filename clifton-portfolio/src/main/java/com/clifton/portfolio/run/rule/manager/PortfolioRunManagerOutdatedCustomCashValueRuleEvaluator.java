package com.clifton.portfolio.run.rule.manager;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountRollup;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunManagerOutdatedCustomCashValueRuleEvaluator</code> class is a rule evaluator
 * that generates violations during a PortfolioRuns for manager accounts with outdated custom cash values based on the current manager account setup.
 *
 * @author sfahey
 */
public class PortfolioRunManagerOutdatedCustomCashValueRuleEvaluator extends BasePortfolioRunManagerAccountRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private InvestmentManagerAccountService investmentManagerAccountService;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		for (InvestmentManagerAccount investmentManagerAccount : CollectionUtils.getIterable(context.getManagerAccountWithActiveAssignmentList(run))) {
			ruleViolationList.addAll(getOutdatedCustomCashValueViolationList(run, investmentManagerAccount, ruleConfig, context));
		}
		return ruleViolationList;
	}


	private List<RuleViolation> getOutdatedCustomCashValueViolationList(PortfolioRun run, InvestmentManagerAccount investmentManagerAccount, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		if (investmentManagerAccount.getManagerType() == InvestmentManagerAccount.ManagerTypes.ROLLUP) {
			investmentManagerAccount = context.getManagerAccountMap(run).get(investmentManagerAccount.getId());
			if (investmentManagerAccount != null) {
				for (InvestmentManagerAccountRollup investmentManagerAccountRollup : CollectionUtils.getIterable(investmentManagerAccount.getRollupManagerList())) {
					ruleViolationList.addAll(getOutdatedCustomCashValueViolationList(run, investmentManagerAccountRollup.getReferenceTwo(), ruleConfig, context));
				}
			}
		}
		else {
			if (!investmentManagerAccount.isInactive()) {
				EntityConfig entityConfig = getEntityConfig(investmentManagerAccount, ruleConfig);
				if (entityConfig != null && !entityConfig.isExcluded()) {
					Date cashValueUpdateDate = investmentManagerAccount.getCustomCashValueUpdateDate();
					BigDecimal ruleAmount = entityConfig.getRuleAmount();
					if (cashValueUpdateDate != null && cashValueUpdateDate.before(run.getBalanceDate()) && ruleAmount != null) {
						int cashValueOutdatedDays = ruleAmount.intValue();
						int daysSinceCashValueUpdate = Math.abs(DateUtils.getDaysDifferenceInclusive(cashValueUpdateDate, run.getBalanceDate()));
						if (daysSinceCashValueUpdate > cashValueOutdatedDays) {
							Map<String, Object> templateValues = new HashMap<>();
							templateValues.put("daysSinceCashValueUpdate", daysSinceCashValueUpdate);
							ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), BeanUtils.getIdentityAsLong(investmentManagerAccount), BeanUtils.getIdentityAsLong(investmentManagerAccount), null, templateValues));
						}
					}
				}
			}
		}
		return ruleViolationList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}
}
