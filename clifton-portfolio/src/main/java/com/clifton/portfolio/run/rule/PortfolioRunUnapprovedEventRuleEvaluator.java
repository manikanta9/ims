package com.clifton.portfolio.run.rule;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.rule.BaseUnapprovedEventRuleEvaluator;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;

import java.util.Date;


/**
 * @author mitchellf
 */
public class PortfolioRunUnapprovedEventRuleEvaluator extends BaseUnapprovedEventRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	@Override
	protected InvestmentAccount getEvaluationObjectClientAccount(PortfolioRun run, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> evaluatorContext) {
		return run.getClientInvestmentAccount();
	}


	@Override
	protected Date getEntityEvaluationDate(PortfolioRun run, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> evaluatorContext) {
		return run.getBalanceDate();
	}


	@Override
	protected boolean isObjectExcluded(PortfolioRun evaluationObject, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> evaluatorContext) {
		// Not used right now, so no object will be excluded
		return false;
	}
}
