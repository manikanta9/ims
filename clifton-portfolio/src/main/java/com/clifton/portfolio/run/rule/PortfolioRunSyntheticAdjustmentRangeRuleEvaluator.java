package com.clifton.portfolio.run.rule;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductOverlaySyntheticAdjustmentRangeRuleEvaluator</code> ...
 *
 * @author Mary Anderson
 */
public class PortfolioRunSyntheticAdjustmentRangeRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	public static final String SYNTHETIC_ADJUSTMENT_WARNING_GROUP = "PIOS Synthetic Exposure Contracts";

	////////////////////////////////////////////////////////////////////////////////

	private InvestmentGroupService investmentGroupService;

	////////////////////////////////////////////////////////////////////////////////

	private boolean includeMatchingReplications;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<BasePortfolioRunReplication> replicationList = new ArrayList<>();
			for (BasePortfolioRunReplication rep : CollectionUtils.getIterable(context.getReplicationList(run, false))) {
				if (isIncludeMatchingReplications() || !rep.isMatchingReplication()) {
					if (getInvestmentGroupService().isInvestmentSecurityInGroup(SYNTHETIC_ADJUSTMENT_WARNING_GROUP, rep.getSecurity().getId())) {
						replicationList.add(rep);
					}
				}
			}
			BigDecimal totalSynAdjustmentValue = CoreMathUtils.sumProperty(replicationList, BasePortfolioRunReplication::getActualExposureAdjusted);
			BigDecimal currentPercentage = CoreMathUtils.getPercentValue(totalSynAdjustmentValue, run.getPortfolioTotalValue(), true);
			Map<String, Object> templateValues = new HashMap<>();
			if (RuleEvaluatorUtils.isEntityConfigRangeViolated(currentPercentage, entityConfig, templateValues)) {
				templateValues.put("totalSynAdjustmentValue", totalSynAdjustmentValue);
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, run.getId(), templateValues));
			}
		}
		return ruleViolationList;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public boolean isIncludeMatchingReplications() {
		return this.includeMatchingReplications;
	}


	public void setIncludeMatchingReplications(boolean includeMatchingReplications) {
		this.includeMatchingReplications = includeMatchingReplications;
	}
}
