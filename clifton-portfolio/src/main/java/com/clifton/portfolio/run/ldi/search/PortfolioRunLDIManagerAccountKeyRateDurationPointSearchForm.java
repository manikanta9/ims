package com.clifton.portfolio.run.ldi.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Search form for {@link com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccountKeyRateDurationPoint}s
 *
 * @author michaelm
 */
public class PortfolioRunLDIManagerAccountKeyRateDurationPointSearchForm extends BaseEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "managerAccount.portfolioRun.id")
	private Integer runId;

	@SearchField(searchField = "managerAccount.portfolioRun.balanceDate")
	private Date runBalanceDate;

	@SearchField(searchField = "managerAccount.portfolioRun.clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "managerAccount.id")
	private Integer managerAccountId;

	@SearchField(searchField = "managerAccount.managerAccountAssignment.id")
	private Integer managerAccountAssignmentId;

	@SearchField(searchField = "cashFlowGroupHistory.id", searchFieldPath = "managerAccount")
	private Integer cashFlowGroupHistoryId;

	@SearchField(searchField = "managerAccount.cashFlowGroupHistory.cashFlowGroup.id")
	private Integer cashFlowGroupId;

	@SearchField
	private BigDecimal dv01Value;

	@SearchField
	private BigDecimal krdValue;

	@SearchField(searchField = "keyRateDurationPoint.reportableOnly")
	private Boolean reportableOnly;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public Date getRunBalanceDate() {
		return this.runBalanceDate;
	}


	public void setRunBalanceDate(Date runBalanceDate) {
		this.runBalanceDate = runBalanceDate;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getManagerAccountId() {
		return this.managerAccountId;
	}


	public void setManagerAccountId(Integer managerAccountId) {
		this.managerAccountId = managerAccountId;
	}


	public Integer getManagerAccountAssignmentId() {
		return this.managerAccountAssignmentId;
	}


	public void setManagerAccountAssignmentId(Integer managerAccountAssignmentId) {
		this.managerAccountAssignmentId = managerAccountAssignmentId;
	}


	public Integer getCashFlowGroupHistoryId() {
		return this.cashFlowGroupHistoryId;
	}


	public void setCashFlowGroupHistoryId(Integer cashFlowGroupHistoryId) {
		this.cashFlowGroupHistoryId = cashFlowGroupHistoryId;
	}


	public Integer getCashFlowGroupId() {
		return this.cashFlowGroupId;
	}


	public void setCashFlowGroupId(Integer cashFlowGroupId) {
		this.cashFlowGroupId = cashFlowGroupId;
	}


	public BigDecimal getDv01Value() {
		return this.dv01Value;
	}


	public void setDv01Value(BigDecimal dv01Value) {
		this.dv01Value = dv01Value;
	}


	public BigDecimal getKrdValue() {
		return this.krdValue;
	}


	public void setKrdValue(BigDecimal krdValue) {
		this.krdValue = krdValue;
	}


	public Boolean getReportableOnly() {
		return this.reportableOnly;
	}


	public void setReportableOnly(Boolean reportableOnly) {
		this.reportableOnly = reportableOnly;
	}
}
