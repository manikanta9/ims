package com.clifton.portfolio.run.rule;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRangeRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class is used by several system beans to validate currency balances in the clients base currency to ensure
 * individual unrealized gain/loss as well as total unrealized gain/loss does not fall outside of the specified threshold for an account
 *
 * @author stevenf  on 8/21/2015.
 */
public class PortfolioRunCurrencyBalanceRangeRuleEvaluator<S extends BasePortfolioRunExposureSummary, R extends BasePortfolioRunReplication, C extends PortfolioRunRuleEvaluatorContext<S, R>> extends BaseRangeRuleEvaluator<PortfolioRun, C> {

	//Balance Range Types
	protected static final String SINGLE_BASE_EXPOSURE = "SingleBaseExposure";
	protected static final String SINGLE_BASE_EXPOSURE_REPLICATION_PERCENT = "SingleBaseExposureReplicationPercent";
	protected static final String TOTAL_BASE_EXPOSURE = "TotalBaseExposure";
	protected static final String TOTAL_BASE_EXPOSURE_CASH_PERCENT = "TotalBaseExposureCashPercent";


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, C context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		BigDecimal totalBaseUnrealizedCurrencyExposure = processTotalBaseUnrealizedCurrencyExposure(ruleViolationList, context, run, ruleConfig);
		ruleViolationList.addAll(evaluateTotalCurrencyRules(run, ruleConfig, totalBaseUnrealizedCurrencyExposure));
		return ruleViolationList;
	}


	protected BigDecimal processTotalBaseUnrealizedCurrencyExposure(List<RuleViolation> ruleViolationList, C context, PortfolioRun run, RuleConfig ruleConfig) {
		BigDecimal totalBaseUnrealizedCurrencyExposure = BigDecimal.ZERO;
		Map<InvestmentSecurity, BigDecimal> baseUnrealizedCurrencyExposureMap = context.getBaseUnrealizedCurrencyExposureMap(run);
		for (Map.Entry<InvestmentSecurity, BigDecimal> investmentSecurityBigDecimalEntry : baseUnrealizedCurrencyExposureMap.entrySet()) {
			if ((investmentSecurityBigDecimalEntry.getKey()).isCurrency() && !(investmentSecurityBigDecimalEntry.getKey()).getSymbol().equals(run.getClientInvestmentAccount().getBaseCurrency().getSymbol())) {
				BigDecimal baseUnrealizedCurrencyExposure = investmentSecurityBigDecimalEntry.getValue();
				if (SINGLE_BASE_EXPOSURE.equals(getRangeType()) || SINGLE_BASE_EXPOSURE_REPLICATION_PERCENT.equals(getRangeType())) {
					//Only apply single currency rules to foreign currency securities.
					ruleViolationList.addAll(evaluateSingleCurrencyRules(run, context, ruleConfig, investmentSecurityBigDecimalEntry.getKey(), baseUnrealizedCurrencyExposure));
				}
				totalBaseUnrealizedCurrencyExposure = totalBaseUnrealizedCurrencyExposure.add(baseUnrealizedCurrencyExposure);
			}
		}
		return totalBaseUnrealizedCurrencyExposure;
	}


	protected List<RuleViolation> evaluateSingleCurrencyRules(PortfolioRun run, C context, RuleConfig ruleConfig,
	                                                          InvestmentSecurity security, BigDecimal baseUnrealizedCurrencyExposure) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		String balanceRangeType = getRangeType();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(security));
		if (entityConfig != null && !entityConfig.isExcluded()) {
			BigDecimal rangeValue = null;
			if (balanceRangeType.equals(SINGLE_BASE_EXPOSURE)) {
				rangeValue = baseUnrealizedCurrencyExposure;
			}
			if (balanceRangeType.equals(SINGLE_BASE_EXPOSURE_REPLICATION_PERCENT)) {
				BigDecimal targetExposure = context.getBaseOverlayTargetExposureMap(run).get(security);
				if (targetExposure != null) {
					rangeValue = CoreMathUtils.getPercentValue(baseUnrealizedCurrencyExposure, targetExposure, true);
				}
			}
			if (rangeValue != null) {
				Map<String, Object> templateValues = new HashMap<>();
				if (RuleEvaluatorUtils.isEntityConfigRangeViolated(rangeValue, entityConfig, templateValues)) {
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), security.getId(), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}


	protected List<RuleViolation> evaluateTotalCurrencyRules(PortfolioRun run, RuleConfig ruleConfig, BigDecimal totalBaseUnrealizedCurrencyExposure) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		String balanceRangeType = getRangeType();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if ((entityConfig != null && !entityConfig.isExcluded())) {
			BigDecimal cashPercent;
			BigDecimal rangeValue = null;
			switch (balanceRangeType) {
				case TOTAL_BASE_EXPOSURE:
					rangeValue = totalBaseUnrealizedCurrencyExposure;
					break;
				case TOTAL_BASE_EXPOSURE_CASH_PERCENT:
					cashPercent = CoreMathUtils.getPercentValue(totalBaseUnrealizedCurrencyExposure, run.getCashTotal(), true);
					if (cashPercent != null) {
						rangeValue = cashPercent;
					}
					break;
			}
			if (rangeValue != null) {
				Map<String, Object> templateValues = new HashMap<>();
				templateValues.put("totalBaseUnrealizedCurrencyExposure", totalBaseUnrealizedCurrencyExposure);
				if (RuleEvaluatorUtils.isEntityConfigRangeViolated(rangeValue, entityConfig, templateValues)) {
					ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, run.getId(), templateValues));
				}
			}
		}
		return ruleViolationList;
	}
}
