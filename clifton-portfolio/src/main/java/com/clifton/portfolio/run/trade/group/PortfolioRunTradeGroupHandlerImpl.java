package com.clifton.portfolio.run.trade.group;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.search.PortfolioRunSearchForm;
import com.clifton.portfolio.run.trade.PortfolioRunTrade;
import com.clifton.portfolio.run.trade.PortfolioRunTradeDetail;
import com.clifton.portfolio.run.trade.PortfolioRunTradeService;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * PortfolioRunTradeGroupHandlerImpl is used to gather run trade information across accounts for a given date/parameters.
 *
 * @author manderson
 */
@Component
public class PortfolioRunTradeGroupHandlerImpl<T extends PortfolioRunTrade<D>, D extends PortfolioRunTradeDetail> implements PortfolioRunTradeGroupHandler<D> {

	private InvestmentAccountGroupService investmentAccountGroupService;

	private PortfolioRunService portfolioRunService;
	private PortfolioRunTradeService<T, D> portfolioRunTradeService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<InvestmentAccount, List<D>> getPortfolioRunAccountTradeGroupDetailMap(PortfolioRunTradeGroup bean, Integer... clientAccountIds) {
		return populatePortfolioRunTradeGroupDetails(bean, getPortfolioRunList(bean, clientAccountIds));
	}


	@Override
	public Map<InvestmentAccount, List<D>> getPortfolioRunAccountTradeGroupDetailMapForRun(PortfolioRunTradeGroup bean, Integer portfolioRunId) {
		ValidationUtils.assertNotNull(portfolioRunId, "A Portfolio Run ID is Required");
		return populatePortfolioRunTradeGroupDetails(bean, CollectionUtils.createList(getPortfolioRunService().getPortfolioRun(portfolioRunId)));
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////                Helper Methods                     ///////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<PortfolioRun> getPortfolioRunList(PortfolioRunTradeGroup bean, Integer... clientAccountIds) {
		// Validate Required Properties Are Set
		ValidationUtils.assertNotNull(bean.getClientAccountGroup(), "Client Account Group Selection is Required", "clientAccountGroup.id");
		ValidationUtils.assertNotNull(bean.getBalanceDate(), "Balance Date is Required", "balanceDate");
		ValidationUtils.assertNotNull(bean.getProcessingType(), "Specified Processing Type is Required");

		if (bean.getTradeDate() == null) {
			bean.setTradeDate(new Date());
		}

		PortfolioRunSearchForm searchForm = new PortfolioRunSearchForm();
		searchForm.setInvestmentAccountGroupId(bean.getClientAccountGroup().getId());
		searchForm.setBalanceDate(bean.getBalanceDate());
		searchForm.setServiceProcessingType(bean.getProcessingType());
		if (bean.isMarketOnClose()) {
			searchForm.setMarketOnCloseAdjustmentsIncluded(true);
		}
		else {
			searchForm.setMainRun(true);
		}
		if (clientAccountIds != null && clientAccountIds.length > 0) {
			searchForm.setClientInvestmentAccountIds(clientAccountIds);
		}
		List<PortfolioRun> runList = getPortfolioRunService().getPortfolioRunList(searchForm);
		if (CollectionUtils.isEmpty(runList)) {
			throw new ValidationException("There are no runs available for selected options.");
		}
		return runList;
	}


	private Map<InvestmentAccount, List<D>> populatePortfolioRunTradeGroupDetails(PortfolioRunTradeGroup bean, List<PortfolioRun> runList) {
		Map<InvestmentAccount, PortfolioRun> clientAccountRunMap = new HashMap<>();
		for (PortfolioRun run : runList) {
			if (clientAccountRunMap.containsKey(run.getClientInvestmentAccount())) {
				// Would only happen if there are multiple runs on the same day (and would only apply to MOC runs)
				PortfolioRun existingRun = clientAccountRunMap.get(run.getClientInvestmentAccount());
				if (!existingRun.isTradingAllowed()) {
					// Replace it
					clientAccountRunMap.put(run.getClientInvestmentAccount(), run);
				}
				// Otherwise we leave the one that is there
			}
			else {
				clientAccountRunMap.put(run.getClientInvestmentAccount(), run);
			}
		}

		// If we need to validate runs for each account, add missing accounts to the map (will fill in sub-accounts later)
		if (bean.isValidateRuns()) {
			List<InvestmentAccount> accountList = getInvestmentAccountGroupService().getInvestmentAccountListByGroup(bean.getClientAccountGroup().getName());
			// THIS WOULD NOT PROPERLY INCLUDE Sub-Accounts INCLUDED IN THE RUN - Added Later when we get details
			for (InvestmentAccount account : accountList) {
				if (account.getType().isOurAccount() && !clientAccountRunMap.containsKey(account)) {
					clientAccountRunMap.put(account, null);
				}
			}
		}

		Map<InvestmentAccount, List<D>> clientAccountRunDetailListMap = getPortfolioRunTradeDetailListForRuns(clientAccountRunMap);
		validateClientAccountRunMap(bean, clientAccountRunMap);
		return clientAccountRunDetailListMap;
	}


	/**
	 * Populates the Client Account detail list map associated with the runs in the map. i.e. for Overlay runs, returns the list of replication rows from the run
	 */
	private Map<InvestmentAccount, List<D>> getPortfolioRunTradeDetailListForRuns(Map<InvestmentAccount, PortfolioRun> clientAccountRunMap) {
		Map<InvestmentAccount, List<D>> clientAccountRunDetailListMap = new HashMap<>();
		for (PortfolioRun run : CollectionUtils.getIterable(clientAccountRunMap.values())) {
			if (run != null) {
				// Note: We get the handler directly, and force not setting trade defaults, all we need is the default holding account for options
				PortfolioRunTrade<D> tradeRun = getPortfolioRunTradeService().getPortfolioRunTradeHandlerForRun(run.getId()).getPortfolioRunTrade(run, true);

				Map<InvestmentAccount, List<D>> runMap = BeanUtils.getBeansMap(tradeRun.getDetailList(), PortfolioRunTradeDetail::getTradingClientAccount);
				for (Map.Entry<InvestmentAccount, List<D>> clientAccountDetailsEntry : CollectionUtils.getIterable(runMap.entrySet())) {
					InvestmentAccount clientAccount = clientAccountDetailsEntry.getKey();
					// If it's the run's account - use it always
					if (clientAccount.equals(run.getClientInvestmentAccount())) {
						clientAccountRunDetailListMap.put(clientAccount, clientAccountDetailsEntry.getValue());
					}
					// Otherwise only set it if the account doesn't have it's own run, and the account should be included
					// i.e. sub-accounts that need to pull information from the main account's run
					// Note: using sub-accounts may not work for all use cases where we need to include run information for trading review
					// But those cases would likely not fit with how this will be used anyway
					else if (clientAccountRunMap.containsKey(clientAccount) && clientAccountRunMap.get(clientAccount) == null) {
						clientAccountRunMap.put(clientAccount, run);
						clientAccountRunDetailListMap.put(clientAccount, clientAccountDetailsEntry.getValue());
					}
				}
			}
		}
		return clientAccountRunDetailListMap;
	}


	private void validateClientAccountRunMap(PortfolioRunTradeGroup runTradeGroup, Map<InvestmentAccount, PortfolioRun> clientAccountRunMap) {
		StringBuilder runErrors = new StringBuilder(16);

		for (Map.Entry<InvestmentAccount, PortfolioRun> accountRunEntry : clientAccountRunMap.entrySet()) {
			InvestmentAccount account = accountRunEntry.getKey();
			PortfolioRun run = accountRunEntry.getValue();
			if (run == null) {
				runErrors.append("<br>");
				runErrors.append("Account #: ").append(account.getNumber()).append(" does not have ").append(runTradeGroup.isMarketOnClose() ? "an MOC Run." : "a Main Run.");
			}
			else if (!run.isTradingAllowed()) {
				runErrors.append("<br>");
				runErrors.append("Account #: ").append(account.getNumber()).append("'s run is in workflow state [").append(run.getWorkflowStateName()).append("/").append(run.getWorkflowStatusName()).append("]. Submitting Trades is not available from this state.");
			}
		}

		if (!runErrors.toString().isEmpty()) {
			if (runTradeGroup.isValidateRuns()) {
				throw new ValidationException("Validation for the Following Account's Runs Failed:<br>" + runErrors.toString());
			}
			else {
				runTradeGroup.setWarningMessage(runErrors.toString().substring(4));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////            Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public PortfolioRunTradeService<T, D> getPortfolioRunTradeService() {
		return this.portfolioRunTradeService;
	}


	public void setPortfolioRunTradeService(PortfolioRunTradeService<T, D> portfolioRunTradeService) {
		this.portfolioRunTradeService = portfolioRunTradeService;
	}
}
