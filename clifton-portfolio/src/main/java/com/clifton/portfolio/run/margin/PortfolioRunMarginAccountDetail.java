package com.clifton.portfolio.run.margin;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;


/**
 * The <code>PortfolioRunMargin</code> ...
 *
 * @author manderson
 */
@NonPersistentObject
public class PortfolioRunMarginAccountDetail {

	private InvestmentAccount clientAccount;
	private InvestmentAccount investmentAccount;

	/**
	 * Flag used to indicate if this account detail should include collateral related cash and positions only.
	 * Different holding account purposes can be used in different ways for collateral resources. A custodian will often hold cash,
	 * and a FCM will hold collateral cash and positions.
	 */
	private boolean collateralOnly;

	private BigDecimal securitiesValue;
	private BigDecimal cashValue;

	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getTotalValue() {
		return CoreMathUtils.sum(getSecuritiesValue(), getCashValue());
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public boolean isCollateralOnly() {
		return this.collateralOnly;
	}


	public void setCollateralOnly(boolean collateralOnly) {
		this.collateralOnly = collateralOnly;
	}


	public BigDecimal getSecuritiesValue() {
		return this.securitiesValue;
	}


	public void setSecuritiesValue(BigDecimal securitiesValue) {
		this.securitiesValue = securitiesValue;
	}


	public BigDecimal getCashValue() {
		return this.cashValue;
	}


	public void setCashValue(BigDecimal cashValue) {
		this.cashValue = cashValue;
	}
}
