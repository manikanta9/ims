package com.clifton.portfolio.run.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.manager.InvestmentManagerAccountRebalance;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Checks if managerAccountBalances in managerAccountGroups are outside of the rebalance bands.
 *
 * @author stevenf
 */
public class PortfolioRunManagerAccountRebalanceRecommendedRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		for (InvestmentManagerAccountRebalance managerAccountRebalance : CollectionUtils.getIterable(context.getManagerAccountRebalanceList(run))) {
			long entityFkFieldId = BeanUtils.getIdentityAsLong(managerAccountRebalance.getManagerAccountGroup());
			EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(managerAccountRebalance.getManagerAccountGroup()));
			if (entityConfig != null && !entityConfig.isExcluded()) {
				BigDecimal maxAmount = managerAccountRebalance.getRebalanceTriggerMax();
				BigDecimal minAmount = managerAccountRebalance.getRebalanceTriggerMin();
				BigDecimal allocationDifference = managerAccountRebalance.getActualDeviationFromTarget();
				Map<String, Object> templateValues = new HashMap<>();
				if (RuleEvaluatorUtils.isEntityConfigRangeViolated(allocationDifference, minAmount, maxAmount, templateValues)) {
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), entityFkFieldId, BeanUtils.getIdentityAsLong(managerAccountRebalance), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}
}
