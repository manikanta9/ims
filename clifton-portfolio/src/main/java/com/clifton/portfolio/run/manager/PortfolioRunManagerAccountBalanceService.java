package com.clifton.portfolio.run.manager;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.process.PortfolioRunConfig;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 * @author michaelm
 */
public interface PortfolioRunManagerAccountBalanceService<T extends PortfolioRunManagerAccountBalance> {

	@ModelAttribute("data")
	@SecureMethod(dtoClass = PortfolioRun.class)
	public List<T> getPortfolioRunManagerAccountBalanceList(int runId, PortfolioRunManagerAccountBalanceSearchForm searchForm);


	@DoNotAddRequestMapping
	public BigDecimal getInvestmentManagerValueInClientAccountBaseCurrency(PortfolioRunConfig runConfig, InvestmentManagerAccount managerAccount, Date date, BigDecimal value);
}
