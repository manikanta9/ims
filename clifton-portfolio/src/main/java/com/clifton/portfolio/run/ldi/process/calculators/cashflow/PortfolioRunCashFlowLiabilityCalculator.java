package com.clifton.portfolio.run.ldi.process.calculators.cashflow;

import com.clifton.portfolio.run.ldi.process.PortfolioRunLDIConfig;


/**
 * Calculator that calculates Cash Flow Liability values for a given {@link com.clifton.portfolio.run.ldi.process.PortfolioRunLDIConfig}.
 *
 * @author michaelm
 */
public interface PortfolioRunCashFlowLiabilityCalculator {

	public PortfolioRunLDIConfig calculateCashFlowLiabilityValues(PortfolioRunLDIConfig runConfig);
}
