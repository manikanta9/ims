package com.clifton.portfolio.run.margin;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portfolio.run.PortfolioRun;

import java.util.List;


/**
 * @author manderson
 */
public interface PortfolioRunMarginService {


	////////////////////////////////////////////////////////////////////////////
	////////         Portfolio Run Margin Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunMargin getPortfolioRunMargin(int id);


	public List<PortfolioRunMargin> getPortfolioRunMarginListByRun(int runId);


	public void savePortfolioRunMarginListByRun(int runId, List<PortfolioRunMargin> saveList);


	public void deletePortfolioRunMarginListByRun(int runId);


	////////////////////////////////////////////////////////////////////////////
	/////////         Portfolio Run Margin Handler Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the PortfolioRunMarginHandler that is used for the run.  Used internally to get the handler and call methods directly on the handler
	 * i.e. Margin Processing during Run Processing
	 */
	@SuppressWarnings("rawtypes")
	@DoNotAddRequestMapping
	public PortfolioRunMarginHandler getPortfolioRunMarginHandlerForRun(PortfolioRun run);
}
