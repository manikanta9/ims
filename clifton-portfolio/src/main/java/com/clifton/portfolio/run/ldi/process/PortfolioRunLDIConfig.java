package com.clifton.portfolio.run.ldi.process;


import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationPoint;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationSecurity;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroup;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistorySummary;
import com.clifton.portfolio.replication.PortfolioReplicationAllocationSecurityConfig;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposureKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccount;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccountKeyRateDurationPoint;
import com.clifton.portfolio.run.process.PortfolioRunConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunLDIConfig</code> ...
 *
 * @author manderson
 */
public class PortfolioRunLDIConfig extends PortfolioRunConfig {

	/**
	 * LDI Run Resulting Tables/Values
	 */
	private final List<PortfolioRunLDIManagerAccount> runManagerList = new ArrayList<>();
	private final List<PortfolioRunLDIManagerAccountKeyRateDurationPoint> runManagerPointList = new ArrayList<>();

	private final List<PortfolioRunLDIKeyRateDurationPoint> runKeyRateDurationPointList = new ArrayList<>();

	private final List<PortfolioRunLDIExposure> runExposureList = new ArrayList<>();
	private final List<PortfolioRunLDIExposureKeyRateDurationPoint> runExposurePointList = new ArrayList<>();

	/**
	 * Setup Data for Re-Use
	 */
	private Boolean useMonthEndValues;
	private Boolean doNotEnforceSameKRDDates;
	private Map<Boolean, List<PortfolioAccountKeyRateDurationPoint>> keyRateDurationPointListMap;

	private final Map<Integer, PortfolioAccountKeyRateDurationSecurity> securityModifiedKRDMap = new HashMap<>();
	private final Map<Integer, PortfolioAccountKeyRateDurationSecurity> reportableOnlySecurityModifiedKRDMap = new HashMap<>();

	List<InvestmentManagerAccountAssignment> managerAssignmentList = null;
	Map<PortfolioCashFlowGroup, List<PortfolioCashFlowGroupHistorySummary>> cashFlowGroupSummaryListMap = null;
	List<PortfolioReplicationAllocationSecurityConfig> replicationAllocationList = null;
	List<InvestmentSecurity> currentlyHeldList = null;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunLDIConfig(PortfolioRun run, boolean reloadProxyManagers, int currentDepth) {
		super(run, reloadProxyManagers, currentDepth);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addPortfolioRunManager(PortfolioRunLDIManagerAccount bean) {
		bean.setPortfolioRun(getRun());
		this.runManagerList.add(bean);
	}


	public void addPortfolioRunManagerKeyRateDurationPoint(PortfolioRunLDIManagerAccount manager, PortfolioRunLDIManagerAccountKeyRateDurationPoint bean) {
		bean.setManagerAccount(manager);
		this.runManagerPointList.add(bean);
	}


	public void addPortfolioRunLDIKeyRateDurationPoint(PortfolioRunLDIKeyRateDurationPoint bean) {
		bean.setPortfolioRun(getRun());
		this.runKeyRateDurationPointList.add(bean);
	}


	public List<PortfolioRunLDIKeyRateDurationPoint> getManagedRunLDIKeyRateDurationPointList() {
		return CollectionUtils.getFiltered(getRunKeyRateDurationPointList(), krdPoint -> !krdPoint.getKeyRateDurationPoint().isReportableOnly());
	}


	public void addPortfolioRunLDIExposure(PortfolioRunLDIExposure bean) {
		bean.setPortfolioRun(getRun());
		this.runExposureList.add(bean);
	}


	public void addPortfolioRunLDIExposureKeyRateDurationPoint(PortfolioRunLDIExposure exposure, PortfolioRunLDIExposureKeyRateDurationPoint bean) {
		bean.setExposure(exposure);
		this.runExposurePointList.add(bean);
	}


	public PortfolioAccountKeyRateDurationSecurity getModifiedKRDValues(Integer securityId) {
		return this.securityModifiedKRDMap.get(securityId);
	}


	public void setModifiedKRDMap(Integer securityId, PortfolioAccountKeyRateDurationSecurity bean) {
		this.securityModifiedKRDMap.put(securityId, bean);
	}


	public PortfolioAccountKeyRateDurationSecurity getReportableOnlyModifiedKRDValues(Integer securityId) {
		return this.reportableOnlySecurityModifiedKRDMap.get(securityId);
	}


	public void setReportableOnlyModifiedKRDMap(Integer securityId, PortfolioAccountKeyRateDurationSecurity bean) {
		this.reportableOnlySecurityModifiedKRDMap.put(securityId, bean);
	}


	public List<PortfolioAccountKeyRateDurationPoint> getKeyRateDurationPointList() {
		return this.keyRateDurationPointListMap.get(false);
	}


	public List<PortfolioAccountKeyRateDurationPoint> getReportableOnlyKeyRateDurationPointList() {
		return this.keyRateDurationPointListMap.get(true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<Boolean, List<PortfolioAccountKeyRateDurationPoint>> getKeyRateDurationPointListMap() {
		return this.keyRateDurationPointListMap;
	}


	public void setKeyRateDurationPointListMap(Map<Boolean, List<PortfolioAccountKeyRateDurationPoint>> keyRateDurationPointListMap) {
		this.keyRateDurationPointListMap = keyRateDurationPointListMap;
	}


	public List<PortfolioRunLDIManagerAccount> getRunManagerList() {
		return this.runManagerList;
	}


	public Boolean getUseMonthEndValues() {
		return this.useMonthEndValues;
	}


	public void setUseMonthEndValues(Boolean useMonthEndValues) {
		this.useMonthEndValues = useMonthEndValues;
	}


	public List<PortfolioRunLDIManagerAccountKeyRateDurationPoint> getRunManagerPointList() {
		return this.runManagerPointList;
	}


	public List<PortfolioRunLDIKeyRateDurationPoint> getRunKeyRateDurationPointList() {
		return this.runKeyRateDurationPointList;
	}


	public List<PortfolioRunLDIExposure> getRunExposureList() {
		return this.runExposureList;
	}


	public List<PortfolioRunLDIExposureKeyRateDurationPoint> getRunExposurePointList() {
		return this.runExposurePointList;
	}


	public Boolean getDoNotEnforceSameKRDDates() {
		return this.doNotEnforceSameKRDDates;
	}


	public void setDoNotEnforceSameKRDDates(Boolean doNotEnforceSameKRDDates) {
		this.doNotEnforceSameKRDDates = doNotEnforceSameKRDDates;
	}


	public List<InvestmentManagerAccountAssignment> getManagerAssignmentList() {
		return this.managerAssignmentList;
	}


	public void setManagerAssignmentList(List<InvestmentManagerAccountAssignment> managerAssignmentList) {
		this.managerAssignmentList = managerAssignmentList;
	}


	public Map<PortfolioCashFlowGroup, List<PortfolioCashFlowGroupHistorySummary>> getCashFlowGroupSummaryListMap() {
		return this.cashFlowGroupSummaryListMap;
	}


	public void setCashFlowGroupSummaryListMap(Map<PortfolioCashFlowGroup, List<PortfolioCashFlowGroupHistorySummary>> cashFlowGroupSummaryListMap) {
		this.cashFlowGroupSummaryListMap = cashFlowGroupSummaryListMap;
	}


	public List<PortfolioReplicationAllocationSecurityConfig> getReplicationAllocationList() {
		return this.replicationAllocationList;
	}


	public void setReplicationAllocationList(List<PortfolioReplicationAllocationSecurityConfig> replicationAllocationList) {
		this.replicationAllocationList = replicationAllocationList;
	}
}
