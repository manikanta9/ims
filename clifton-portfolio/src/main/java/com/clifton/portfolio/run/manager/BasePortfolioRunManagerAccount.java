package com.clifton.portfolio.run.manager;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.hierarchy.HierarchicalSimpleEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * {@link BasePortfolioRunManagerAccount} holds common fields for information related to {@link com.clifton.investment.manager.InvestmentManagerAccountAllocation}s during
 * {@link PortfolioRun} processing.
 *
 * @author michaelm
 */
public class BasePortfolioRunManagerAccount extends HierarchicalSimpleEntity<BasePortfolioRunManagerAccount, Integer> implements LabeledObject {

	private PortfolioRun overlayRun; // should be named portfolioRun but leaving as overlayRun for refactoring concerns

	/**
	 * If true, will be excluded from reports
	 */
	private boolean privateManager;

	private BigDecimal cashAllocation;
	private BigDecimal securitiesAllocation;

	/**
	 * Used for Proxy Managers only.  When a ProxyValue & Date are set on the
	 * Manager Account, that value is allocated to this Overlay Manager at the specified percentage
	 */
	private BigDecimal proxyValueAllocation;

	private InvestmentManagerAccountAssignment managerAccountAssignment;

	private BigDecimal previousDayCashAllocation;
	private BigDecimal previousDaySecuritiesAllocation;

	private BigDecimal previousMonthEndCashAllocation;
	private BigDecimal previousMonthEndSecuritiesAllocation;

	// Not reference in DB but set for single manager look ups to display/link balance info
	@NonPersistentField
	private InvestmentManagerAccountBalance balance;


	private InvestmentAccountAssetClass accountAssetClass;
	private boolean rollupAssetClass;



	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<InvestmentManagerAccountBalanceAdjustment> getBalanceAdjustmentList() {
		if (getBalance() == null || CollectionUtils.isEmpty(getBalance().getAdjustmentList())) {
			return null;
		}
		if (getOverlayRun().isMarketOnCloseAdjustmentsIncluded()) {

			return getBalance().getAdjustmentList();
		}
		return BeanUtils.filter(getBalance().getAdjustmentList(), investmentManagerAccountBalanceAdjustment -> !investmentManagerAccountBalanceAdjustment.getAdjustmentType().isMarketOnClose());
	}


	public boolean isAllocateSecuritiesBalanceToReplications() {
		if (getManagerAccountAssignment() != null) {
			return getManagerAccountAssignment().isAllocateSecuritiesBalanceToReplications();
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		String label = "";
		if (getManagerAccountAssignment() != null) {
			label = getManagerAccountAssignment().getReferenceOne().getLabel();
			if (!StringUtils.isEmpty(getManagerAccountAssignment().getDisplayName())) {
				label += " (" + getManagerAccountAssignment().getDisplayName() + ")";
			}
			return label;
		}

		return label;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getCashAllocationPercent() {
		return CoreMathUtils.getPercentValue(getCashAllocation(), getTotalAllocation(), true);
	}


	public BigDecimal getTotalAllocation() {
		return MathUtils.add(getCashAllocation(), getSecuritiesAllocation());
	}


	public BigDecimal getCashPreviousCashAllocationDifference() {
		return MathUtils.subtract(getCashAllocation(), getPreviousDayCashAllocation());
	}


	public BigDecimal getCashPreviousCashAllocationPercentDifference() {
		return MathUtils.round(MathUtils.getPercentChange(getPreviousDayCashAllocation(), getCashAllocation(), true), 2);
	}


	public BigDecimal getPreviousDayTotalAllocation() {
		return MathUtils.add(getPreviousDayCashAllocation(), getPreviousDaySecuritiesAllocation());
	}


	public BigDecimal getPreviousMonthEndTotalAllocation() {
		return MathUtils.add(getPreviousMonthEndCashAllocation(), getPreviousMonthEndSecuritiesAllocation());
	}


	public BigDecimal getOneDayPercentChange() {
		return MathUtils.getPercentChange(getPreviousDayTotalAllocation(), getTotalAllocation(), true);
	}


	public BigDecimal getMonthPercentChange() {
		return MathUtils.getPercentChange(getPreviousMonthEndTotalAllocation(), getTotalAllocation(), true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRun getOverlayRun() {
		return this.overlayRun;
	}


	public void setOverlayRun(PortfolioRun overlayRun) {
		this.overlayRun = overlayRun;
	}


	public boolean isPrivateManager() {
		return this.privateManager;
	}


	public void setPrivateManager(boolean privateManager) {
		this.privateManager = privateManager;
	}


	public BigDecimal getCashAllocation() {
		return this.cashAllocation;
	}


	public void setCashAllocation(BigDecimal cashAllocation) {
		this.cashAllocation = cashAllocation;
	}


	public BigDecimal getSecuritiesAllocation() {
		return this.securitiesAllocation;
	}


	public void setSecuritiesAllocation(BigDecimal securitiesAllocation) {
		this.securitiesAllocation = securitiesAllocation;
	}


	public BigDecimal getProxyValueAllocation() {
		return this.proxyValueAllocation;
	}


	public void setProxyValueAllocation(BigDecimal proxyValueAllocation) {
		this.proxyValueAllocation = proxyValueAllocation;
	}


	public InvestmentManagerAccountAssignment getManagerAccountAssignment() {
		return this.managerAccountAssignment;
	}


	public void setManagerAccountAssignment(InvestmentManagerAccountAssignment managerAccountAssignment) {
		this.managerAccountAssignment = managerAccountAssignment;
	}


	public BigDecimal getPreviousDayCashAllocation() {
		return this.previousDayCashAllocation;
	}


	public void setPreviousDayCashAllocation(BigDecimal previousDayCashAllocation) {
		this.previousDayCashAllocation = previousDayCashAllocation;
	}


	public BigDecimal getPreviousDaySecuritiesAllocation() {
		return this.previousDaySecuritiesAllocation;
	}


	public void setPreviousDaySecuritiesAllocation(BigDecimal previousDaySecuritiesAllocation) {
		this.previousDaySecuritiesAllocation = previousDaySecuritiesAllocation;
	}


	public BigDecimal getPreviousMonthEndCashAllocation() {
		return this.previousMonthEndCashAllocation;
	}


	public void setPreviousMonthEndCashAllocation(BigDecimal previousMonthEndCashAllocation) {
		this.previousMonthEndCashAllocation = previousMonthEndCashAllocation;
	}


	public BigDecimal getPreviousMonthEndSecuritiesAllocation() {
		return this.previousMonthEndSecuritiesAllocation;
	}


	public void setPreviousMonthEndSecuritiesAllocation(BigDecimal previousMonthEndSecuritiesAllocation) {
		this.previousMonthEndSecuritiesAllocation = previousMonthEndSecuritiesAllocation;
	}


	public InvestmentManagerAccountBalance getBalance() {
		return this.balance;
	}


	public void setBalance(InvestmentManagerAccountBalance balance) {
		this.balance = balance;
	}


	public InvestmentAccountAssetClass getAccountAssetClass() {
		return this.accountAssetClass;
	}


	public void setAccountAssetClass(InvestmentAccountAssetClass accountAssetClass) {
		this.accountAssetClass = accountAssetClass;
	}


	public boolean isRollupAssetClass() {
		return this.rollupAssetClass;
	}


	public void setRollupAssetClass(boolean rollupAssetClass) {
		this.rollupAssetClass = rollupAssetClass;
	}
}
