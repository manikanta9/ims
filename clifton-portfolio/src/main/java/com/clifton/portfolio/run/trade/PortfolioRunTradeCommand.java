package com.clifton.portfolio.run.trade;

import com.clifton.core.beans.BeanListCommand;
import com.clifton.portfolio.run.PortfolioRun;


/**
 * <code>PortfolioRunTradeCommand</code> is a command pattern used to process portfolio trade creation
 * actions by allowing beans to be populated for a {@link PortfolioRun} and its {@link PortfolioRunTradeDetail}
 *
 * @author NickK
 */
public class PortfolioRunTradeCommand<D extends PortfolioRunTradeDetail> extends BeanListCommand<D> {

	private PortfolioRun portfolioRun;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	public void setPortfolioRun(PortfolioRun portfolioRun) {
		this.portfolioRun = portfolioRun;
	}
}
