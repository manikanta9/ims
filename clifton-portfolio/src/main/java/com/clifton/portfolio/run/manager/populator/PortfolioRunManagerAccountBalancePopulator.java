package com.clifton.portfolio.run.manager.populator;

import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalance;

import java.util.List;


/**
 * {@link PortfolioRunManagerAccountBalancePopulator}s populate extensions of the {@link PortfolioRunManagerAccountBalance} entity based on the
 * {@link PortfolioRunManagerAccountBalancePopulatorConfig}.
 *
 * @author manderson
 * @author michaelm
 */
public interface PortfolioRunManagerAccountBalancePopulator<T extends PortfolioRunManagerAccountBalance> {


	public List<T> populatePortfolioRunManagerAccountBalanceList(PortfolioRunManagerAccountBalancePopulatorConfig config);
}
