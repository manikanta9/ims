package com.clifton.portfolio.run;

import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.hierarchy.HierarchicalSimpleEntity;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplication;

import java.math.BigDecimal;
import java.util.Date;


/**
 * {@link BasePortfolioRunExposureSummary} represents various overlay metrics for a specified {@link PortfolioRun} (account + date) grouped by some other entity
 * (e.g. PortfolioTarget).
 *
 * @author michaelm
 */
public abstract class BasePortfolioRunExposureSummary extends HierarchicalSimpleEntity<BasePortfolioRunExposureSummary, Integer> implements LabeledObject {

	private PortfolioRun overlayRun; // todo rename this to portfolioRun

	private BigDecimal actualAllocation;
	private BigDecimal actualAllocationPercent;
	private BigDecimal actualAllocationAdjusted;
	private BigDecimal actualAllocationAdjustedPercent;

	private BigDecimal targetAllocation;
	private BigDecimal targetAllocationPercent;
	private BigDecimal targetAllocationAdjusted;
	private BigDecimal targetAllocationAdjustedPercent;

	private BigDecimal overlayExposure;

	// When explicitly set on the Asset Class for at least one asset class, then this reflects those values
	// Otherwise, when not used these values equal the targetAllocationAdjusted & targetAllocationAdjustedPercent
	private BigDecimal rebalanceExposureTarget;
	private BigDecimal rebalanceExposureTargetPercent;

	// Historical Rebalance Info
	private BigDecimal rebalanceTriggerMin;
	private BigDecimal rebalanceTriggerMax;
	private BigDecimal rebalanceTriggerAbsoluteMin;
	private BigDecimal rebalanceTriggerAbsoluteMax;

	private BigDecimal fundCash;
	private BigDecimal managerCash;
	private BigDecimal transitionCash; // manager cash marked as transition cash (used for transitions)
	private BigDecimal clientDirectedCash; // cash applied to suggest trades when client tells us to

	private Date rebalanceDate; // Date that Rebalance Cash was entered
	private BigDecimal rebalanceCash; // synthetic cash (not real) used to get asset classes to target allocation at the time of rebalancing
	private BigDecimal rebalanceCashAdjusted; // adjusted based on corresponding proxy performance
	private BigDecimal rebalanceAttributionCash; // Rebal cash designated for attribution rebalancing, i.e. Moving negative Manager, Fund, Transition cash into Rebal cash
	private InvestmentSecurity rebalanceBenchmarkSecurity; // Benchmark used to adjust Rebalance Cash Values

	private InvestmentReplication primaryReplication;

	private InvestmentSecurity benchmarkSecurity; // Benchmark associated with InvestmentAccountAssetClass
	private BigDecimal benchmarkDuration; // Market Data Value for the duration of the benchmark. Used to calculate values for Fixed Income Futures
	private BigDecimal benchmarkCreditDuration; // Market Data Value for the Credit duration of the benchmark. Used to calculate values for Fixed Income Futures
	private BigDecimal effectiveDuration; // Actual Duration calculated for all Securities (where IsApplyDuration = TRUE)

	/**
	 * For Asset Classes with a benchmark security selected
	 * will calculate the benchmark security return
	 */
	private BigDecimal benchmarkOneDayReturn;
	private BigDecimal benchmarkMonthToDateReturn;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract String getSummaryLabel();


	public abstract BigDecimal getCashTotal();


	public abstract BigDecimal getTotalExposurePercent();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getSummaryLabel();
	}


	public String getTopLevelLabel() {
		if (getParent() != null) {
			return getParent().getTopLevelLabel();
		}
		return getLabel();
	}


	public BigDecimal getPhysicalDeviationFromAdjustedTarget() {
		return MathUtils.subtract(getActualAllocationAdjusted(), getTargetAllocationAdjusted());
	}


	public boolean isTargetAdjusted() {
		return !MathUtils.isEqual(getTargetAllocation(), getTargetAllocationAdjusted());
	}


	public BigDecimal getPhysicalDeviationFromAdjustedTargetPercent() {
		return MathUtils.round(MathUtils.subtract(getActualAllocationAdjustedPercent(), getTargetAllocationAdjustedPercent()), 2);
	}


	public BigDecimal getPortfolioTotalValue() {
		if (getOverlayRun() == null) {
			return null;
		}
		return getOverlayRun().getPortfolioTotalValue();
	}


	public BigDecimal getTotalExposure() {
		return MathUtils.round(MathUtils.add(getActualAllocationAdjusted(), getOverlayExposure()), 2);
	}


	public BigDecimal getTotalExposureDeviationFromAdjustedTargetPercent() {
		return MathUtils.round(MathUtils.subtract(getTotalExposurePercent(), getTargetAllocationAdjustedPercent()), 2);
	}


	public BigDecimal getTotalExposureDeviationFromRebalanceExposureTarget() {
		return MathUtils.round(MathUtils.subtract(getTotalExposure(), getRebalanceExposureTarget()), 2);
	}


	// Used to Determine if Rebalancing Triggers were crossed
	// Target Less Effective + (Overlay Value - Target Overlay)
	public BigDecimal getAmountOffTarget() {
		return getTotalExposureDeviationFromRebalanceExposureTarget();
	}


	public boolean isRebalanceTriggersUsed() {
		return getRebalanceTriggerMin() != null;
	}


	public BigDecimal getTotalExposureDeviationFromAdjustedTarget() {
		return MathUtils.round(MathUtils.subtract(getTotalExposure(), getTargetAllocationAdjusted()), 2);
	}

	// Rebalance Triggers Min & Max Percentages


	public BigDecimal getRebalanceTriggerMinPercent() {
		return getRebalanceTriggerPercent(getRebalanceTriggerMin());
	}


	public BigDecimal getRebalanceTriggerAbsoluteMinPercent() {
		return getRebalanceTriggerPercent(getRebalanceTriggerAbsoluteMin());
	}


	public BigDecimal getRebalanceTriggerMaxPercent() {
		return getRebalanceTriggerPercent(getRebalanceTriggerMax());
	}


	public BigDecimal getRebalanceTriggerAbsoluteMaxPercent() {
		return getRebalanceTriggerPercent(getRebalanceTriggerAbsoluteMax());
	}


	private BigDecimal getRebalanceTriggerPercent(BigDecimal value) {
		if (value != null) {
			return CoreMathUtils.getPercentValue(value, getPortfolioTotalValue(), true);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getRebalanceTriggerMin() {
		return this.rebalanceTriggerMin;
	}


	public void setRebalanceTriggerMin(BigDecimal rebalanceTriggerMin) {
		this.rebalanceTriggerMin = rebalanceTriggerMin;
	}


	public BigDecimal getRebalanceTriggerMax() {
		return this.rebalanceTriggerMax;
	}


	public void setRebalanceTriggerMax(BigDecimal rebalanceTriggerMax) {
		this.rebalanceTriggerMax = rebalanceTriggerMax;
	}


	public BigDecimal getRebalanceTriggerAbsoluteMin() {
		return this.rebalanceTriggerAbsoluteMin;
	}


	public void setRebalanceTriggerAbsoluteMin(BigDecimal rebalanceTriggerAbsoluteMin) {
		this.rebalanceTriggerAbsoluteMin = rebalanceTriggerAbsoluteMin;
	}


	public BigDecimal getRebalanceTriggerAbsoluteMax() {
		return this.rebalanceTriggerAbsoluteMax;
	}


	public void setRebalanceTriggerAbsoluteMax(BigDecimal rebalanceTriggerAbsoluteMax) {
		this.rebalanceTriggerAbsoluteMax = rebalanceTriggerAbsoluteMax;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRun getOverlayRun() {
		return this.overlayRun;
	}


	public void setOverlayRun(PortfolioRun overlayRun) {
		this.overlayRun = overlayRun;
	}


	public BigDecimal getActualAllocation() {
		return this.actualAllocation;
	}


	public void setActualAllocation(BigDecimal actualAllocation) {
		this.actualAllocation = actualAllocation;
	}


	public BigDecimal getActualAllocationPercent() {
		return this.actualAllocationPercent;
	}


	public void setActualAllocationPercent(BigDecimal actualAllocationPercent) {
		this.actualAllocationPercent = actualAllocationPercent;
	}


	public BigDecimal getActualAllocationAdjusted() {
		return this.actualAllocationAdjusted;
	}


	public void setActualAllocationAdjusted(BigDecimal actualAllocationAdjusted) {
		this.actualAllocationAdjusted = actualAllocationAdjusted;
	}


	public BigDecimal getActualAllocationAdjustedPercent() {
		return this.actualAllocationAdjustedPercent;
	}


	public void setActualAllocationAdjustedPercent(BigDecimal actualAllocationAdjustedPercent) {
		this.actualAllocationAdjustedPercent = actualAllocationAdjustedPercent;
	}


	public BigDecimal getTargetAllocation() {
		return this.targetAllocation;
	}


	public void setTargetAllocation(BigDecimal targetAllocation) {
		this.targetAllocation = targetAllocation;
	}


	public BigDecimal getTargetAllocationPercent() {
		return this.targetAllocationPercent;
	}


	public void setTargetAllocationPercent(BigDecimal targetAllocationPercent) {
		this.targetAllocationPercent = targetAllocationPercent;
	}


	public BigDecimal getTargetAllocationAdjusted() {
		return this.targetAllocationAdjusted;
	}


	public void setTargetAllocationAdjusted(BigDecimal targetAllocationAdjusted) {
		this.targetAllocationAdjusted = targetAllocationAdjusted;
	}


	public BigDecimal getTargetAllocationAdjustedPercent() {
		return this.targetAllocationAdjustedPercent;
	}


	public void setTargetAllocationAdjustedPercent(BigDecimal targetAllocationAdjustedPercent) {
		this.targetAllocationAdjustedPercent = targetAllocationAdjustedPercent;
	}


	public BigDecimal getOverlayExposure() {
		return this.overlayExposure;
	}


	public BigDecimal getRebalanceExposureTarget() {
		return this.rebalanceExposureTarget;
	}


	public void setRebalanceExposureTarget(BigDecimal rebalanceExposureTarget) {
		this.rebalanceExposureTarget = rebalanceExposureTarget;
	}


	public BigDecimal getRebalanceExposureTargetPercent() {
		return this.rebalanceExposureTargetPercent;
	}


	public void setRebalanceExposureTargetPercent(BigDecimal rebalanceExposureTargetPercent) {
		this.rebalanceExposureTargetPercent = rebalanceExposureTargetPercent;
	}


	public void setOverlayExposure(BigDecimal overlayExposure) {
		this.overlayExposure = overlayExposure;
	}


	public BigDecimal getFundCash() {
		return this.fundCash;
	}


	public void setFundCash(BigDecimal fundCash) {
		this.fundCash = fundCash;
	}


	public BigDecimal getManagerCash() {
		return this.managerCash;
	}


	public void setManagerCash(BigDecimal managerCash) {
		this.managerCash = managerCash;
	}


	public BigDecimal getTransitionCash() {
		return this.transitionCash;
	}


	public void setTransitionCash(BigDecimal transitionCash) {
		this.transitionCash = transitionCash;
	}


	public BigDecimal getClientDirectedCash() {
		return this.clientDirectedCash;
	}


	public void setClientDirectedCash(BigDecimal clientDirectedCash) {
		this.clientDirectedCash = clientDirectedCash;
	}


	public Date getRebalanceDate() {
		return this.rebalanceDate;
	}


	public void setRebalanceDate(Date rebalanceDate) {
		this.rebalanceDate = rebalanceDate;
	}


	public BigDecimal getRebalanceCash() {
		return this.rebalanceCash;
	}


	public void setRebalanceCash(BigDecimal rebalanceCash) {
		this.rebalanceCash = rebalanceCash;
	}


	public BigDecimal getRebalanceCashAdjusted() {
		return this.rebalanceCashAdjusted;
	}


	public void setRebalanceCashAdjusted(BigDecimal rebalanceCashAdjusted) {
		this.rebalanceCashAdjusted = rebalanceCashAdjusted;
	}


	public BigDecimal getRebalanceAttributionCash() {
		return this.rebalanceAttributionCash;
	}


	public void setRebalanceAttributionCash(BigDecimal rebalanceAttributionCash) {
		this.rebalanceAttributionCash = rebalanceAttributionCash;
	}


	public InvestmentSecurity getRebalanceBenchmarkSecurity() {
		return this.rebalanceBenchmarkSecurity;
	}


	public void setRebalanceBenchmarkSecurity(InvestmentSecurity rebalanceBenchmarkSecurity) {
		this.rebalanceBenchmarkSecurity = rebalanceBenchmarkSecurity;
	}


	public InvestmentReplication getPrimaryReplication() {
		return this.primaryReplication;
	}


	public void setPrimaryReplication(InvestmentReplication primaryReplication) {
		this.primaryReplication = primaryReplication;
	}


	public InvestmentSecurity getBenchmarkSecurity() {
		return this.benchmarkSecurity;
	}


	public void setBenchmarkSecurity(InvestmentSecurity benchmarkSecurity) {
		this.benchmarkSecurity = benchmarkSecurity;
	}


	public BigDecimal getBenchmarkDuration() {
		return this.benchmarkDuration;
	}


	public void setBenchmarkDuration(BigDecimal benchmarkDuration) {
		this.benchmarkDuration = benchmarkDuration;
	}


	public BigDecimal getBenchmarkCreditDuration() {
		return this.benchmarkCreditDuration;
	}


	public void setBenchmarkCreditDuration(BigDecimal benchmarkCreditDuration) {
		this.benchmarkCreditDuration = benchmarkCreditDuration;
	}


	public BigDecimal getEffectiveDuration() {
		return this.effectiveDuration;
	}


	public void setEffectiveDuration(BigDecimal effectiveDuration) {
		this.effectiveDuration = effectiveDuration;
	}


	public BigDecimal getBenchmarkOneDayReturn() {
		return this.benchmarkOneDayReturn;
	}


	public void setBenchmarkOneDayReturn(BigDecimal benchmarkOneDayReturn) {
		this.benchmarkOneDayReturn = benchmarkOneDayReturn;
	}


	public BigDecimal getBenchmarkMonthToDateReturn() {
		return this.benchmarkMonthToDateReturn;
	}


	public void setBenchmarkMonthToDateReturn(BigDecimal benchmarkMonthToDateReturn) {
		this.benchmarkMonthToDateReturn = benchmarkMonthToDateReturn;
	}
}
