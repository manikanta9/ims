package com.clifton.portfolio.run.comparisons;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.search.TradeSearchForm;

import java.util.List;


/**
 * The <code>PortfolioRunTradesExistsComparison</code> is used to determine if trades exist
 * that are associated with the run.
 *
 * @author manderson
 */
public class PortfolioRunTradesExistsComparison implements Comparison<PortfolioRun> {

	private TradeGroupService tradeGroupService;
	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * If true, looks at ALL pending trades for the Account or any Sub-Account on the run
	 * If false, looks only at trades associated with the run through the run trade group
	 */
	private boolean checkAllPending;

	/**
	 * Not necessary if checkAllPending is used, but can be used to further limit Pending (which is not closed trades) to specific statuses
	 */
	private List<String> workflowStatusNames;

	/**
	 * When using check all pending, often "Rolls" and "Tails" are excluded as they are handled separately and the approval of the run shouldn't be affected by the roll.
	 */
	private List<Short> excludeTradeGroupTypeIds;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(PortfolioRun run, ComparisonContext context) {
		AssertUtils.assertNotNull(run, "Portfolio Run is required to evaluate trade comparison for it.");

		List<Trade> tradeList = getTradeList(run);
		boolean result = !CollectionUtils.isEmpty(tradeList);
		if (isReverse()) {
			result = !result;
		}
		if (context != null) {
			String message = "[" + CollectionUtils.getSize(tradeList) + "] " + (isCheckAllPending() ? "Pending " : "") + "trades "
					+ (!CollectionUtils.isEmpty(getWorkflowStatusNames()) ? " in [" + StringUtils.collectionToCommaDelimitedString(getWorkflowStatusNames()) + "] " : "") + " found for portfolio run.";
			if (result) {
				context.recordTrueMessage(message);
			}
			else {
				context.recordFalseMessage(message);
			}
		}
		return result;
	}


	protected boolean isReverse() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<Trade> getTradeList(PortfolioRun run) {
		TradeSearchForm searchForm = new TradeSearchForm();
		if (isCheckAllPending()) {
			searchForm.setClientInvestmentAccountIdOrSubAccount(run.getClientInvestmentAccount().getId());
			searchForm.setExcludeWorkflowStatusName(TradeService.TRADES_CLOSED_STATUS_NAME);
			if (!CollectionUtils.isEmpty(getExcludeTradeGroupTypeIds())) {
				searchForm.setExcludeTradeGroupTypeIds(getExcludeTradeGroupTypeIds().toArray(new Short[getExcludeTradeGroupTypeIds().size()]));
			}
		}
		else {
			searchForm.setTradeGroupTypeId(getTradeGroupService().getTradeGroupTypeByName(TradeGroupType.GroupTypes.PORTFOLIO.getTypeName()).getId());
			searchForm.setTradeGroupSourceFkFieldId(run.getId());
		}
		List<Trade> tradeList = getTradeService().getTradeList(searchForm);
		if (!CollectionUtils.isEmpty(getWorkflowStatusNames())) {
			tradeList = BeanUtils.filter(tradeList, trade -> getWorkflowStatusNames().contains(trade.getWorkflowStatus().getName()));
		}
		return tradeList;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	public boolean isCheckAllPending() {
		return this.checkAllPending;
	}


	public void setCheckAllPending(boolean checkAllPending) {
		this.checkAllPending = checkAllPending;
	}


	public List<String> getWorkflowStatusNames() {
		return this.workflowStatusNames;
	}


	public void setWorkflowStatusNames(List<String> workflowStatusNames) {
		this.workflowStatusNames = workflowStatusNames;
	}


	public List<Short> getExcludeTradeGroupTypeIds() {
		return this.excludeTradeGroupTypeIds;
	}


	public void setExcludeTradeGroupTypeIds(List<Short> excludeTradeGroupTypeIds) {
		this.excludeTradeGroupTypeIds = excludeTradeGroupTypeIds;
	}
}
