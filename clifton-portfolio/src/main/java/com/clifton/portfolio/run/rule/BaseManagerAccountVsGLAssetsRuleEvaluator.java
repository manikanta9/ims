package com.clifton.portfolio.run.rule;

import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Base rule evaluator for when managers pass their rebalance triggers
 *
 * @author mitchellf
 */
public abstract class BaseManagerAccountVsGLAssetsRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private AccountingBalanceService accountingBalanceService;
	private AccountingValuationService accountingValuationService;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(run.getClientInvestmentAccount()));
		if (entityConfig != null && !entityConfig.isExcluded()) {
			Integer[] clientAccountIdList = context.getClientAccountList(run).stream().map(BaseSimpleEntity<Integer>::getId).toArray(Integer[]::new);
			BigDecimal generalLedgerAssets = getGeneralLedgerAssets(clientAccountIdList, run.getBalanceDate());
			BigDecimal managerAccountAssets = getManagerAccountAssets(run.getId());
			BigDecimal managerAccountGeneralLedgerDifference = MathUtils.subtract(managerAccountAssets, generalLedgerAssets);

			Map<String, Object> templateValues = new HashMap<>();
			if (RuleEvaluatorUtils.isEntityConfigRangeViolated(managerAccountGeneralLedgerDifference, entityConfig, templateValues)) {
				templateValues.put("generalLedgerAssets", generalLedgerAssets);
				templateValues.put("managerAccountAssets", managerAccountAssets);
				templateValues.put("managerAccountGeneralLedgerDifference", managerAccountGeneralLedgerDifference);
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, run.getId(), templateValues));
			}
		}
		return ruleViolationList;
	}


	protected BigDecimal getGeneralLedgerAssets(Integer[] clientAccountIds, Date balanceDate) {
		BigDecimal generalLedgerAssets = new BigDecimal(0);
		AccountingBalanceSearchForm accountingBalanceSearchForm = AccountingBalanceSearchForm.onTransactionDate(balanceDate);
		accountingBalanceSearchForm.setClientInvestmentAccountIds(clientAccountIds);
		accountingBalanceSearchForm.setAccountingAccountTypes(new String[]{AccountingAccountType.ASSET, AccountingAccountType.LIABILITY});
		List<AccountingBalanceValue> accountingBalanceList = getAccountingValuationService().getAccountingBalanceValueList(accountingBalanceSearchForm);
		for (AccountingBalanceValue accountingBalanceValue : CollectionUtils.getIterable(accountingBalanceList)) {
			generalLedgerAssets = MathUtils.add(generalLedgerAssets, accountingBalanceValue.getBaseMarketValue());
		}
		return generalLedgerAssets;
	}


	protected abstract BigDecimal getManagerAccountAssets(Integer runId);

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}
}
