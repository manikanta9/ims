package com.clifton.portfolio.run.process.calculator;


import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.process.PortfolioRunConfig;
import com.clifton.portfolio.run.rule.PortfolioRunRuleHandler;


/**
 * The <code>BasePortfolioRunProcessStepCalculatorImpl</code> is the base class that defines processing for a particular step in portfolio run processing.
 * Each step is broken down into process and save and the runConfig object is passed around to easily read/use data that was calculated in processing in the save step
 *
 * @author Mary Anderson
 */
public abstract class BasePortfolioRunProcessStepCalculatorImpl<T extends PortfolioRunConfig> implements PortfolioRunProcessStepCalculator<T> {

	private InvestmentManagerAccountService investmentManagerAccountService;
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private MarketDataRetriever marketDataRetriever;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;
	private PortfolioRunRuleHandler portfolioRunRuleHandler;
	private PortfolioRunService portfolioRunService;

	private String workflowStateName;

	private boolean doNotExecuteTransactional;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Processes {@link PortfolioRun} object for this step.
	 *
	 * @param runConfig should indicate whether to process as a pre- or post-process step
	 */
	protected abstract void processStep(T runConfig);


	/**
	 * Performs all saves from processing the step - done in a transaction so it saves all or nothing
	 *
	 * @param runConfig should indicate whether to process as a pre- or post-process step
	 */
	protected abstract void saveStep(T runConfig);


	////////////////////////////////////////////////////////////////////////////////


	@Override
	public final void process(T runConfig) {
		processStep(runConfig);
		saveStep(runConfig);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public PortfolioRunRuleHandler getPortfolioRunRuleHandler() {
		return this.portfolioRunRuleHandler;
	}


	public void setPortfolioRunRuleHandler(PortfolioRunRuleHandler portfolioRunRuleHandler) {
		this.portfolioRunRuleHandler = portfolioRunRuleHandler;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	@Override
	public String getWorkflowStateName() {
		return this.workflowStateName;
	}


	public void setWorkflowStateName(String workflowStateName) {
		this.workflowStateName = workflowStateName;
	}


	@Override
	public boolean isDoNotExecuteTransactional() {
		return this.doNotExecuteTransactional;
	}


	public void setDoNotExecuteTransactional(boolean doNotExecuteTransactional) {
		this.doNotExecuteTransactional = doNotExecuteTransactional;
	}
}
