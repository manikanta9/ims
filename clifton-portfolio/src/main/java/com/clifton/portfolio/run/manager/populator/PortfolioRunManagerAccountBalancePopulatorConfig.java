package com.clifton.portfolio.run.manager.populator;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalance;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalanceSearchForm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Config object used to process/populate manager account balances/adjustments for view by run
 *
 * @author manderson
 */
public class PortfolioRunManagerAccountBalancePopulatorConfig {

	private final PortfolioRun portfolioRun;
	private final PortfolioRunManagerAccountBalanceSearchForm searchForm;

	/**
	 * If managers are re-used across the portfolio, this will store the general manger/balance information
	 * that can be re-used, and then allocations can be changed for each version
	 */
	private final Map<Integer, PortfolioRunManagerAccountBalance> managerAccountBalanceMap = new HashMap<>();


	private String exchangeRateDataSourceName;
	// Map of Security (CCY ID) Exchange Rates to the Client Account Base CCY
	private final Map<Integer, BigDecimal> exchangeRateMap = new HashMap<>();


	/**
	 * Most useful for children of rollup managers, which can re-use same child manager balances so we don't have to look up balances constantly
	 */
	private final Map<Integer, List<InvestmentManagerAccountBalanceAdjustment>> managerAccountManualAdjustmentListMap = new HashMap<>();


	public PortfolioRunManagerAccountBalancePopulatorConfig(PortfolioRun portfolioRun, PortfolioRunManagerAccountBalanceSearchForm searchForm) {
		ValidationUtils.assertNotNull(portfolioRun, "Portfolio Run is required");
		ValidationUtils.assertNotNull(searchForm, "Portfolio Run Manager Account Balance Search Form is required");
		this.portfolioRun = portfolioRun;
		this.searchForm = searchForm;
	}

	//////////////////////////////////////////////////////////////////////////


	public boolean isMarketOnClose() {
		return this.portfolioRun.isMarketOnCloseAdjustmentsIncluded();
	}

	//////////////////////////////////////////////////////////////////////////


	public void addPortfolioRunManagerAccountBalanceToMap(PortfolioRunManagerAccountBalance managerAccountBalance) {
		this.managerAccountBalanceMap.put(managerAccountBalance.getManagerAccountBalance().getManagerAccount().getId(), managerAccountBalance);
	}


	public PortfolioRunManagerAccountBalance getPortfolioRunManagerAccountBalance(InvestmentManagerAccount managerAccount) {
		return this.managerAccountBalanceMap.get(managerAccount.getId());
	}


	public void addManagerAccountManualAdjustmentListToMap(InvestmentManagerAccount managerAccount, List<InvestmentManagerAccountBalanceAdjustment> manualAdjustmentList) {
		this.managerAccountManualAdjustmentListMap.put(managerAccount.getId(), (manualAdjustmentList == null ? new ArrayList<>() : manualAdjustmentList));
	}


	public List<InvestmentManagerAccountBalanceAdjustment> getManagerAccountManualAdjustmentList(InvestmentManagerAccount managerAccount) {
		return this.managerAccountManualAdjustmentListMap.get(managerAccount.getId());
	}


	public BigDecimal getExchangeRateForManagerAccount(InvestmentManagerAccount managerAccount, MarketDataExchangeRatesApiService apiService) {
		InvestmentSecurity baseCurrency = managerAccount.getBaseCurrency();
		BigDecimal fxRate = this.exchangeRateMap.get(baseCurrency.getId());
		if (fxRate == null) {
			fxRate = apiService.getExchangeRate(FxRateLookupCommand.forDataSource(getExchangeRateDataSourceName(), getPortfolioRun().getClientInvestmentAccount().getBaseCurrency().getSymbol(), baseCurrency.getSymbol(), getPortfolioRun().getBalanceDate()));
			this.exchangeRateMap.put(baseCurrency.getId(), fxRate);
		}
		return fxRate;
	}

	///////////////////////////////////////////////////////////////////////////


	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	public PortfolioRunManagerAccountBalanceSearchForm getSearchForm() {
		return this.searchForm;
	}


	public void setExchangeRateDataSourceName(String exchangeRateDataSourceName) {
		this.exchangeRateDataSourceName = exchangeRateDataSourceName;
	}


	public String getExchangeRateDataSourceName() {
		return this.exchangeRateDataSourceName;
	}
}
