package com.clifton.portfolio.run.ldi.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccount;
import org.springframework.stereotype.Component;


/**
 * Validator for <code>PortfolioRunLDIManagerAccountValidator</code>s.
 *
 * @author michaelm
 */
@Component
public class PortfolioRunLDIManagerAccountValidator extends SelfRegisteringDaoValidator<PortfolioRunLDIManagerAccount> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(PortfolioRunLDIManagerAccount bean, DaoEventTypes config) throws ValidationException {
		if (bean.getCashFlowGroupHistory() == null) {
			ValidationUtils.assertTrue(bean.getManagerAccountAssignment() != null && bean.getBenchmarkSecurity() != null, "Both Manager Account Assignment and Benchmark Security must be populated in the absence of a Cash Flow Group History.");
		}
		else {
			ValidationUtils.assertTrue(bean.getManagerAccountAssignment() == null && bean.getBenchmarkSecurity() == null, "Neither Manager Account Assignment nor Benchmark Security can be populated when a Cash Flow Group History is specified.");
		}
	}
}

