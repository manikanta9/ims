package com.clifton.portfolio.run.margin;

import com.clifton.collateral.CollateralService;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.process.PortfolioRunConfig;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The <code>BasePortfolioRunMarginHandler</code> is an abstract class that contains common functionality used by implementations of PortfolioRunMarginHandler
 *
 * @author manderson
 */
public abstract class BasePortfolioRunMarginHandler<T extends PortfolioRunConfig> implements PortfolioRunMarginHandler<T> {

	private CollateralBalanceService collateralBalanceService;
	private CollateralService collateralService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;


	////////////////////////////////////////////////////////////////////////////////
	///////////           Helper Methods for Common Functionality         //////////
	////////////////////////////////////////////////////////////////////////////////


	protected List<InvestmentAccountRelationship> getTradingFuturesAccountRelationshipList(T runConfig) {
		List<InvestmentAccountRelationship> tradingFuturesBrokerRelationshipList = getPortfolioAccountDataRetriever().getTradingFuturesBrokerAccountRelationshipList(
				runConfig.getClientInvestmentAccountId(), runConfig.getBalanceDate());
		List<InvestmentAccountRelationship> subAccountRelationshipList = getPortfolioAccountDataRetriever().getCliftonSubAccountAccountRelationshipList(runConfig.getClientInvestmentAccountId(),
				runConfig.getBalanceDate());

		// Only use sub account relationships where this account is the main account
		subAccountRelationshipList = BeanUtils.filter(subAccountRelationshipList, investmentAccountRelationship -> investmentAccountRelationship.getReferenceOne().getId(), runConfig.getClientInvestmentAccountId());

		// Changing to exclude Default Sub Accounts - let's only use the sub account if the asset class is specifically selected
		// remove subaccount relationships that don't have an asset class specified
		if (!CollectionUtils.isEmpty(subAccountRelationshipList)) {
			subAccountRelationshipList.removeIf(relationship -> relationship.getAccountAssetClass() == null || relationship.getAccountAssetClass().getId() == null);
		}
		// Remove any Trading Futures Broker Relationships tied to these excluded Sub Accounts
		Integer[] accountsToRemove = new Integer[]{runConfig.getClientInvestmentAccountId()};
		if (!CollectionUtils.isEmpty(subAccountRelationshipList)) {
			accountsToRemove = ArrayUtils.addAll(accountsToRemove, BeanUtils.getPropertyValues(subAccountRelationshipList, investmentAccountRelationship -> investmentAccountRelationship.getReferenceTwo().getId(), Integer.class));
		}
		Set<Integer> accountsToRemoveSet = new HashSet<>();
		Collections.addAll(accountsToRemoveSet, accountsToRemove);
		tradingFuturesBrokerRelationshipList = BeanUtils.filter(tradingFuturesBrokerRelationshipList, accountRelationship -> accountsToRemoveSet.contains(accountRelationship.getReferenceOne().getId()));

		return tradingFuturesBrokerRelationshipList;
	}


	protected InvestmentAccount getDefaultBroker(T runConfig, List<InvestmentAccountRelationship> tradingFuturesAccountRelationshipList) {
		// Need to also exclude the "default" broker relationships for these sub accounts
		// Now Remove default broker relationships that aren't for the main account and asset class is null
		// NOTE: Asset class is not on the trading relationship for a sub account, so need to filter on both by the main account and asset class is null (in case main account has some trading relationships for a specific asset class)
		InvestmentAccount defaultBroker = null;
		List<InvestmentAccountRelationship> defaultBrokerList = BeanUtils.filter(tradingFuturesAccountRelationshipList, InvestmentAccountRelationship::getAccountAssetClass, null);
		defaultBrokerList = BeanUtils.filter(defaultBrokerList, investmentAccountRelationship -> investmentAccountRelationship.getReferenceOne().getId(), runConfig.getClientInvestmentAccountId());
		if (CollectionUtils.getSize(defaultBrokerList) == 1) {
			defaultBroker = defaultBrokerList.get(0).getReferenceTwo();
		}
		return defaultBroker;
	}


	protected PortfolioRunMargin populateMarginForBroker(T runConfig, InvestmentAccount brokerAccount) {
		PortfolioRunMargin brokerMargin = new PortfolioRunMargin();
		brokerMargin.setPortfolioRun(runConfig.getRun());
		brokerMargin.setBrokerInvestmentAccount(brokerAccount);

		// See if we have a Collateral Balance Record for this Broker
		CollateralBalanceSearchForm searchForm = new CollateralBalanceSearchForm();
		searchForm.setHoldingInvestmentAccountId(brokerAccount.getId());
		searchForm.setParentIsNull(true);
		searchForm.setBalanceDate(runConfig.getBalanceDate());
		List<CollateralBalance> cbList = getCollateralBalanceService().getCollateralBalanceList(searchForm);

		// If empty - check child balance level
		if (CollectionUtils.isEmpty(cbList)) {
			searchForm = new CollateralBalanceSearchForm();
			searchForm.setHoldingInvestmentAccountId(brokerAccount.getId());
			searchForm.setParentIsNull(false);
			searchForm.setBalanceDate(runConfig.getBalanceDate());
			cbList = getCollateralBalanceService().getCollateralBalanceList(searchForm);
		}
		BigDecimal postedBrokerCollateral = BigDecimal.ZERO;
		BigDecimal requiredInitial = BigDecimal.ZERO;

		boolean groupedMarginAccount = BooleanUtils.isTrue((Boolean) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(brokerMargin.getPortfolioRun().getClientInvestmentAccount(), InvestmentAccount.GROUPED_MARGIN_ACCOUNT));

		for (CollateralBalance b : CollectionUtils.getIterable(cbList)) {
			BigDecimal exRate = BigDecimal.ONE;
			if (b.getCollateralCurrency() != null) {
				exRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forHoldingAccount(brokerAccount.toHoldingAccount(), b.getCollateralCurrency().getSymbol(),
						runConfig.getRun().getClientInvestmentAccount().getBaseCurrency().getSymbol(), runConfig.getBalanceDate()));
			}
			postedBrokerCollateral = MathUtils.add(postedBrokerCollateral, MathUtils.multiply(b.getCoalesceExternalPostedCounterpartyCollateralHaircutValue(), exRate));

			if (!groupedMarginAccount) {
				requiredInitial = MathUtils.add(requiredInitial, MathUtils.multiply(b.getCoalesceExternalCounterpartyCollateralRequirement(), exRate));
			}
		}

		if (MathUtils.isNullOrZero(postedBrokerCollateral)) {
			// Recalc Collateral Value
			postedBrokerCollateral = getCollateralService().getCollateralMarketValue(null, brokerAccount.getId(), runConfig.getBalanceDate());
		}
		brokerMargin.setMarginInitialValue(requiredInitial);
		brokerMargin.setBrokerCollateralValue(postedBrokerCollateral);
		return brokerMargin;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public CollateralService getCollateralService() {
		return this.collateralService;
	}


	public void setCollateralService(CollateralService collateralService) {
		this.collateralService = collateralService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}
}
