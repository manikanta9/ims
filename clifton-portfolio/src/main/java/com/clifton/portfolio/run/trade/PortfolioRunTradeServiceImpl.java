package com.clifton.portfolio.run.trade;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipSearchForm;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.margin.PortfolioRunMargin;
import com.clifton.portfolio.run.margin.PortfolioRunMarginAccountDetail;
import com.clifton.portfolio.run.margin.PortfolioRunMarginDetail;
import com.clifton.portfolio.run.margin.PortfolioRunMarginService;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;


/**
 * The <code>PortfolioRunTradeServiceImpl</code> implements PortfolioRunTradeService and is essentially
 * just a service implementation that looks up the correct handler and passes information to the handler to process/return
 * based on the run's account's service
 *
 * @author Mary Anderson
 */
@Service
public class PortfolioRunTradeServiceImpl<T extends PortfolioRunTrade<D>, D extends PortfolioRunTradeDetail> implements PortfolioRunTradeService<T, D> {

	private AccountingValuationService accountingValuationService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;
	private PortfolioRunService portfolioRunService;
	private PortfolioRunMarginService portfolioRunMarginService;

	private Map<String, PortfolioRunTradeHandler<T, D>> portfolioRunTradeHandlerMap;


	////////////////////////////////////////////////////////////////////////////////
	/////////      Portfolio Run Trade Creation Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public T getPortfolioRunTrade(int id) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(id);
		return getPortfolioRunTradeHandlerForRun(run.getId()).getPortfolioRunTrade(run, false);
	}


	@Override
	public T getPortfolioRunTradePrices(T run, boolean livePrices, boolean reloadDetails) {
		return getPortfolioRunTradeHandlerForRun(run.getId()).getPortfolioRunTradePrices(run, livePrices, reloadDetails);
	}


	@Override
	@Transactional(readOnly = true)
	public T getPortfolioRunTradeAmounts(T run) {
		return getPortfolioRunTradeHandlerForRun(run.getId()).getPortfolioRunTradeAmounts(run);
	}


	@Override
	@Transactional(timeout = 180)
	public TradeGroup processPortfolioRunTradeCreation(T bean) {
		return getPortfolioRunTradeHandlerForRun(bean.getId()).processPortfolioRunTradeCreation(bean);
	}


	@Override
	@Transactional
	public String processPortfolioRunTradeApproval(int runId, Integer[] detailIds) {
		return getPortfolioRunTradeHandlerForRun(runId).processPortfolioRunTradeApproval(detailIds);
	}


	@Override
	public List<Trade> getPortfolioRunTradeDetailPendingTradeList(int runId, int detailId) {
		return getPortfolioRunTradeHandlerForRun(runId).getPortfolioRunTradeDetailPendingTradeList(detailId);
	}


	@Override
	public List<AccountingPositionTransfer> getPortfolioRunTradeDetailPendingTransferList(int runId, int detailId) {
		return getPortfolioRunTradeHandlerForRun(runId).getPortfolioRunTradeDetailPendingTransferList(detailId);
	}


	@Override
	public List<InvestmentAccount> getPortfolioRunTradeDetailHoldingInvestmentAccountList(int runId, int detailId) {
		return getPortfolioRunTradeHandlerForRun(runId).getPortfolioRunTradeDetailHoldingInvestmentAccountList(detailId);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRunTradeHandler<T, D> getPortfolioRunTradeHandlerForRun(int runId) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		if (run == null) {
			throw new ValidationException("Missing run [" + runId + "] in the database.");
		}
		return getPortfolioAccountDataRetriever().getClientAccountObjectFromContextMap(run.getClientInvestmentAccount(), run.getServiceProcessingType(), getPortfolioRunTradeHandlerMap(), "Portfolio Run Trade Handler");
	}


	@Override
	@Transactional(readOnly = true)
	public PortfolioRunMarginDetail getPortfolioRunMarginDetailByRun(int runId) {
		PortfolioRun portfolioRun = getPortfolioRunService().getPortfolioRun(runId);
		List<PortfolioRunMargin> runMarginList = getPortfolioRunMarginService().getPortfolioRunMarginListByRun(runId);

		PortfolioRunMarginDetail collateralDetail = new PortfolioRunMarginDetail();
		collateralDetail.setPortfolioRun(portfolioRun);
		collateralDetail.setRunMarginList(runMarginList);

		if (!CollectionUtils.isEmpty(runMarginList)) {
			List<AccountingBalanceValue> balanceValueList = getPendingCollateralAccountingBalanceValueList(runMarginList);
			Map<InvestmentAccount, BigDecimal> holdingAccountPendingBalanceValueMap = new HashMap<>();
			CollectionUtils.getIterable(balanceValueList)
					.forEach(balance -> holdingAccountPendingBalanceValueMap.compute(balance.getHoldingInvestmentAccount(), (key, currentBalance) -> MathUtils.add(currentBalance, balance.getBaseMarketValue())));

			InvestmentAccount clientAccount = portfolioRun.getClientInvestmentAccount();
			BigDecimal excessCollateralPercent = (BigDecimal) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(clientAccount, InvestmentAccount.CLIENT_ACCOUNT_COLLATERAL_EXCESS_TARGET_PERCENT_CUSTOM_COLUMN);

			for (PortfolioRunMargin runMargin : CollectionUtils.getIterable(runMarginList)) {
				runMargin.setCollateralTarget(MathUtils.add(runMargin.getMarginInitialValue(), MathUtils.getPercentageOf(excessCollateralPercent, runMargin.getMarginInitialValue(), true)));
				runMargin.setCollateralTargetPercent(CoreMathUtils.getPercentValue(runMargin.getCollateralTarget(), portfolioRun.getOverlayExposureTotal(), true));
				runMargin.setBrokerCollateralValuePercent(CoreMathUtils.getPercentValue(runMargin.getBrokerCollateralValue(), portfolioRun.getOverlayExposureTotal(), true));
				runMargin.setPendingCollateral(holdingAccountPendingBalanceValueMap.get(runMargin.getBrokerInvestmentAccount()));
				runMargin.setPendingCollateralPercent(CoreMathUtils.getPercentValue(runMargin.getPendingCollateral(), portfolioRun.getOverlayExposureTotal(), true));
				runMargin.setCollateralDifference(MathUtils.subtract(runMargin.getCollateralTarget(), MathUtils.add(runMargin.getBrokerCollateralValue(), runMargin.getPendingCollateral())));
				runMargin.setCollateralDifferencePercent(CoreMathUtils.getPercentValue(runMargin.getCollateralDifference(), portfolioRun.getOverlayExposureTotal(), true));
			}
		}

		return collateralDetail;
	}


	@Override
	@Transactional(readOnly = true)
	public List<PortfolioRunMarginAccountDetail> getPortfolioRunMarginAccountDetailListByRun(int runId) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		List<PortfolioRunMargin> runMarginList = getPortfolioRunMarginService().getPortfolioRunMarginListByRun(runId);

		if (CollectionUtils.isEmpty(runMarginList)) {
			return Collections.emptyList();
		}

		// Map each margin account to holding account to be able to efficiently get the margin balance for an account
		Map<InvestmentAccount, PortfolioRunMargin> holdingAccountToRunMargin = BeanUtils.getBeanMap(runMarginList, PortfolioRunMargin::getBrokerInvestmentAccount);
		Map<PortfolioRunMarginAccountDetail, PortfolioRunMarginAccountDetail> marginAccountDetailMap = getMarginAccountDetailMap(runMarginList, run.getBalanceDate());

		Set<PortfolioRunMarginAccountDetail> marginAccountDetailSet = new HashSet<>();
		marginAccountDetailMap.forEach((fcmDetail, custodianDetail) -> {
			marginAccountDetailSet.add(fcmDetail);
			if (custodianDetail != null) {
				marginAccountDetailSet.add(custodianDetail);
			}
		});
		List<AccountingBalanceValue> balanceValueList = getCollateralAccountingBalanceValueList(marginAccountDetailSet, run.getBalanceDate());
		Map<InvestmentAccount, List<AccountingBalanceValue>> holdingAccountBalanceValueListMap = BeanUtils.getBeansMap(balanceValueList, AccountingBalanceValue::getHoldingInvestmentAccount);

		List<PortfolioRunMarginAccountDetail> marginAccountDetailList = new ArrayList<>();
		marginAccountDetailMap.forEach((fcmDetail, custodianDetail) -> {
			populatePortfolioRunMarginAccountDetail(fcmDetail, holdingAccountBalanceValueListMap.get(fcmDetail.getInvestmentAccount()));
			marginAccountDetailList.add(fcmDetail);

			if (custodianDetail != null) {
				populatePortfolioRunMarginAccountDetail(custodianDetail, holdingAccountBalanceValueListMap.get(custodianDetail.getInvestmentAccount()));
				// Get the margin balance for the FCM to apply remainder to the custodian's cash value.
				// Since cash is not reconciled daily in IMS, we cannot rely on general ledger data.
				PortfolioRunMargin runMargin = holdingAccountToRunMargin.get(fcmDetail.getInvestmentAccount());
				if (runMargin != null) {
					BigDecimal marginAmount = runMargin.getOurAccountValue();
					BigDecimal fcmMargin = MathUtils.add(fcmDetail.getCashValue(), fcmDetail.getSecuritiesValue());
					custodianDetail.setCashValue(MathUtils.subtract(marginAmount, MathUtils.add(fcmMargin, custodianDetail.getSecuritiesValue())));
				}
				marginAccountDetailList.add(custodianDetail);
			}
		});
		return marginAccountDetailList;
	}


	private void populatePortfolioRunMarginAccountDetail(PortfolioRunMarginAccountDetail detail, List<AccountingBalanceValue> balanceValueList) {
		for (AccountingBalanceValue balanceValue : CollectionUtils.getIterable(balanceValueList)) {
			BigDecimal baseBalanceMarketValue = balanceValue.getBaseMarketValue();
			if (balanceValue.getAccountingAccount().isCash()) {
				detail.setCashValue(MathUtils.add(detail.getCashValue(), baseBalanceMarketValue));
			}
			else {
				detail.setSecuritiesValue(MathUtils.add(detail.getSecuritiesValue(), baseBalanceMarketValue));
			}
		}
	}


	private Integer[] getPortfolioRunMarginHoldingAccountIds(List<PortfolioRunMargin> runMarginList) {
		return runMarginList.stream()
				.map(PortfolioRunMargin::getBrokerInvestmentAccount)
				.filter(Objects::nonNull)
				.map(InvestmentAccount::getId)
				.distinct()
				.toArray(Integer[]::new);
	}


	/**
	 * Returns a map of FCM accounts and their corresponding custodian accounts for each provided {@link PortfolioRunMargin}.
	 */
	private Map<PortfolioRunMarginAccountDetail, PortfolioRunMarginAccountDetail> getMarginAccountDetailMap(List<PortfolioRunMargin> runMarginList, Date balanceDate) {
		Integer[] holdingAccountIds = getPortfolioRunMarginHoldingAccountIds(runMarginList);

		// Look up FCMs for run
		InvestmentAccountRelationshipSearchForm searchForm = new InvestmentAccountRelationshipSearchForm();
		searchForm.setRelatedAccountIds(holdingAccountIds);
		searchForm.setPurposeNames(new String[]{InvestmentAccountRelationshipPurpose.TRADING_FUTURES_PURPOSE_NAME});
		searchForm.setActiveOnDate(balanceDate);
		searchForm.setMainAccountWorkflowStateName("Active");
		List<InvestmentAccountRelationship> accountRelationshipList = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipList(searchForm);

		// Look up M2M links between FCMs and custodians
		InvestmentAccountRelationshipSearchForm searchForm2 = new InvestmentAccountRelationshipSearchForm();
		searchForm2.setMainAccountIds(holdingAccountIds);
		searchForm2.setPurposeNames(new String[]{InvestmentAccountRelationshipPurpose.MARK_TO_MARKET_ACCOUNT_PURPOSE_NAME});
		searchForm2.setActiveOnDate(balanceDate);
		List<InvestmentAccountRelationship> custodianM2mRelationships = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipList(searchForm2);
		Map<InvestmentAccount, InvestmentAccountRelationship> holdingAccountToM2MAccountMap = BeanUtils.getBeanMap(custodianM2mRelationships, InvestmentAccountRelationship::getReferenceOne);

		// map of FCM to M2M Custodian margin details
		Map<PortfolioRunMarginAccountDetail, PortfolioRunMarginAccountDetail> marginAccountDetailMap = new HashMap<>();
		for (InvestmentAccountRelationship accountRelationship : CollectionUtils.getIterable(accountRelationshipList)) {
			PortfolioRunMarginAccountDetail marginAccountDetail = new PortfolioRunMarginAccountDetail();
			marginAccountDetail.setClientAccount(accountRelationship.getReferenceOne());
			marginAccountDetail.setInvestmentAccount(accountRelationship.getReferenceTwo());
			marginAccountDetail.setCollateralOnly(InvestmentAccountRelationshipPurpose.TRADING_FUTURES_PURPOSE_NAME.equals(accountRelationship.getPurpose().getName()));

			InvestmentAccountRelationship custodianRelationship = holdingAccountToM2MAccountMap.get(marginAccountDetail.getInvestmentAccount());
			PortfolioRunMarginAccountDetail custodianDetail = null;
			if (custodianRelationship != null) {
				custodianDetail = new PortfolioRunMarginAccountDetail();
				custodianDetail.setClientAccount(accountRelationship.getReferenceOne());
				custodianDetail.setInvestmentAccount(custodianRelationship.getReferenceTwo());
				custodianDetail.setCollateralOnly(false);
			}

			marginAccountDetailMap.put(marginAccountDetail, custodianDetail);
		}

		return marginAccountDetailMap;
	}


	/**
	 * Returns the pending collateral related balance values for the provided {@link PortfolioRunMargin} brokerage accounts for the transaction date.
	 */
	private List<AccountingBalanceValue> getPendingCollateralAccountingBalanceValueList(List<PortfolioRunMargin> runMarginList) {
		Integer[] holdingAccountIds = getPortfolioRunMarginHoldingAccountIds(runMarginList);
		// Get pending activity for transaction date of tomorrow for pending activity; tomorrow will pick up trades entered after market close today.
		AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onTransactionDate(DateUtils.addDays(new Date(), 1));
		int holdingAccountIdCount = ArrayUtils.getLength(holdingAccountIds);
		if (holdingAccountIdCount == 1) {
			searchForm.setHoldingInvestmentAccountId(holdingAccountIds[0]);
		}
		else if (holdingAccountIdCount > 1) {
			searchForm.setHoldingInvestmentAccountIds(holdingAccountIds);
		}
		searchForm.setUseHoldingAccountBaseCurrency(true);
		searchForm.setAccountingAccountTypes(new String[]{AccountingAccountType.ASSET, AccountingAccountType.LIABILITY});
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.EXCLUDE_RECEIVABLE_COLLATERAL);
		searchForm.setPendingActivityRequest(PendingActivityRequests.ALL_WITH_PREVIEW);
		searchForm.setIncludeUnderlyingPrice(false);
		searchForm.setPendingOnly(true);
		return getCollateralAccountingBalanceValueList(searchForm, AccountingBalanceValue::isPendingActivity);
	}


	/**
	 * Returns GL collateral related balance values for the provided holding account IDs for the transaction date.
	 */
	private List<AccountingBalanceValue> getCollateralAccountingBalanceValueList(Collection<PortfolioRunMarginAccountDetail> marginAccountDetailList, Date transactionDate) {
		Set<Integer> holdingAccountIdSet = new HashSet<>();
		// Map of client/holding account key for cash aggregation (cash vs cash collateral accounting account)
		Map<String, Boolean> marginAccountToCollateralMap = new HashMap<>();
		marginAccountDetailList.forEach(marginAccountDetail -> {
			InvestmentAccount holdingAccount = marginAccountDetail.getInvestmentAccount();
			if (holdingAccount != null) {
				holdingAccountIdSet.add(holdingAccount.getId());
				marginAccountToCollateralMap.put(BeanUtils.createKeyFromBeans(marginAccountDetail.getClientAccount(), holdingAccount), marginAccountDetail.isCollateralOnly());
			}
		});

		AccountingBalanceSearchForm searchForm = new AccountingBalanceSearchForm();
		searchForm.setHoldingInvestmentAccountIds(holdingAccountIdSet.toArray(new Integer[0]));
		searchForm.setUseHoldingAccountBaseCurrency(true);
		searchForm.setTransactionDate(transactionDate);
		searchForm.setAccountingAccountTypes(new String[]{AccountingAccountType.ASSET, AccountingAccountType.LIABILITY});
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.EXCLUDE_RECEIVABLE_COLLATERAL);
		searchForm.setIncludeUnderlyingPrice(false);
		return getCollateralAccountingBalanceValueList(searchForm, balance -> {
			AccountingAccount accountingAccount = balance.getAccountingAccount();
			if (accountingAccount.isCash()) {
				String key = BeanUtils.createKeyFromBeans(balance.getClientInvestmentAccount(), balance.getHoldingInvestmentAccount());
				Boolean collateral = marginAccountToCollateralMap.get(key);
				if (collateral != null) {
					return collateral == accountingAccount.isCollateral();
				}
			}
			else if (accountingAccount.isPosition()) {
				return InvestmentUtils.isPhysicalSecurity(balance.getInvestmentSecurity());
			}
			return false;
		});
	}


	private List<AccountingBalanceValue> getCollateralAccountingBalanceValueList(AccountingBalanceSearchForm searchForm, Predicate<AccountingBalanceValue> postSearchFilter) {
		List<AccountingBalanceValue> accountingBalanceValueList = getAccountingValuationService().getAccountingBalanceValueList(searchForm);
		return CollectionUtils.getFiltered(accountingBalanceValueList, postSearchFilter);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public PortfolioRunMarginService getPortfolioRunMarginService() {
		return this.portfolioRunMarginService;
	}


	public void setPortfolioRunMarginService(PortfolioRunMarginService portfolioRunMarginService) {
		this.portfolioRunMarginService = portfolioRunMarginService;
	}


	public Map<String, PortfolioRunTradeHandler<T, D>> getPortfolioRunTradeHandlerMap() {
		return this.portfolioRunTradeHandlerMap;
	}


	public void setPortfolioRunTradeHandlerMap(Map<String, PortfolioRunTradeHandler<T, D>> portfolioRunTradeHandlerMap) {
		this.portfolioRunTradeHandlerMap = portfolioRunTradeHandlerMap;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}
}
