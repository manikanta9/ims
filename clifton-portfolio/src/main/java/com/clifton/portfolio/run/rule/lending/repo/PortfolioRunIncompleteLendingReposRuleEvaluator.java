package com.clifton.portfolio.run.rule.lending.repo;

import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.lending.api.repo.LendingRepoApiService;
import com.clifton.lending.api.repo.Repo;
import com.clifton.lending.api.repo.RepoSearchCommand;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunIncompleteLendingReposRuleEvaluator</code> class is rule evaluator that
 * generates violations during a PortfolioRun process for accounts with incomplete Lending Repos
 *
 * @author sfahey
 */
public class PortfolioRunIncompleteLendingReposRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private LendingRepoApiService lendingRepoApiService;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			for (InvestmentAccount clientAccount : context.getClientAccountList(run)) {
				for (Repo unbookedRepo : CollectionUtils.getIterable(getUnbookedTradedOrMaturedRepos(clientAccount, context.getBalanceDate(run)))) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("clientAccount", clientAccount);
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), unbookedRepo.getId(), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}


	private List<Repo> getUnbookedTradedOrMaturedRepos(InvestmentAccount clientAccount, Date balanceDate) {
		RepoSearchCommand repoSearchCommand = new RepoSearchCommand();
		repoSearchCommand.setClientInvestmentAccountId(clientAccount.getId());
		repoSearchCommand.setExcludeWorkflowStateName(WorkflowStatus.STATUS_CANCELED);
		repoSearchCommand.setOpeningUnbookedOrMaturityUnbookedOnDate(balanceDate);
		return getLendingRepoApiService().getRepoList(repoSearchCommand);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public LendingRepoApiService getLendingRepoApiService() {
		return this.lendingRepoApiService;
	}


	public void setLendingRepoApiService(LendingRepoApiService lendingRepoApiService) {
		this.lendingRepoApiService = lendingRepoApiService;
	}
}
