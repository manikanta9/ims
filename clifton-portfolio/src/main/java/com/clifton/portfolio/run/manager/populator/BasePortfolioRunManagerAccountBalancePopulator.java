package com.clifton.portfolio.run.manager.populator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountRollup;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalance;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
public abstract class BasePortfolioRunManagerAccountBalancePopulator<T extends PortfolioRunManagerAccountBalance> implements PortfolioRunManagerAccountBalancePopulator<T> {

	private InvestmentManagerAccountService investmentManagerAccountService;
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;


	@Override
	public final List<T> populatePortfolioRunManagerAccountBalanceList(PortfolioRunManagerAccountBalancePopulatorConfig config) {
		List<T> list = populatePortfolioRunManagerAccountBalanceListImpl(config);
		// Apply Additional Customized Filters
		if (Boolean.TRUE.equals(config.getSearchForm().getShowAdjustedManagersOnly())) {
			list = BeanUtils.filter(list, PortfolioRunManagerAccountBalance::isManuallyAdjusted);
		}
		return list;
	}


	public abstract List<T> populatePortfolioRunManagerAccountBalanceListImpl(PortfolioRunManagerAccountBalancePopulatorConfig config);

	//////////////////////////////////////////////////////////////
	////////           Common Helper Methods              ////////
	//////////////////////////////////////////////////////////////


	protected T getPopulatedPortfolioRunManagerAccountBalance(PortfolioRunManagerAccountBalancePopulatorConfig config, InvestmentManagerAccount managerAccount) {
		T existingBean = (T) config.getPortfolioRunManagerAccountBalance(managerAccount);
		T bean;
		if (existingBean == null) {
			bean = (T) BeanUtils.newInstance(getAccountBalanceClass());
			bean.setPortfolioRun(config.getPortfolioRun());
			populateManagerAccountBalance(config, bean, managerAccount);
			config.addPortfolioRunManagerAccountBalanceToMap(bean);
		}
		else {
			bean = BeanUtils.cloneBean(existingBean, false, true);
		}
		return bean;
	}


	protected Class<?> getAccountBalanceClass() {
		return PortfolioRunManagerAccountBalance.class;
	}


	private void populateManagerAccountBalance(PortfolioRunManagerAccountBalancePopulatorConfig config, PortfolioRunManagerAccountBalance balance, InvestmentManagerAccount managerAccount) {
		InvestmentManagerAccountBalance managerBalance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(managerAccount.getId(), config.getPortfolioRun().getBalanceDate(), true);
		// If balance is missing - just create empty balance with manager info
		if (managerBalance == null) {
			managerBalance = new InvestmentManagerAccountBalance();
			managerBalance.setManagerAccount(managerAccount);
		}

		// Process the balance information
		balance.setManagerAccountBalance(managerBalance);
		BigDecimal cashBalanceBase = managerBalance.getCashValue();
		BigDecimal securitiesBalanceBase = managerBalance.getSecuritiesValue();

		for (InvestmentManagerAccountBalanceAdjustment adjustment : CollectionUtils.getIterable(managerBalance.getAdjustmentList())) {
			if (isAdjustmentAppliedToRun(config, adjustment)) {
				if (adjustment.getAdjustmentType().isSystemDefined()) {
					cashBalanceBase = MathUtils.add(cashBalanceBase, adjustment.getCashValue());
					securitiesBalanceBase = MathUtils.add(securitiesBalanceBase, adjustment.getSecuritiesValue());
				}
				else {
					balance.addManualAdjustment(adjustment);
				}
			}
		}

		balance.setCashBalanceBase(cashBalanceBase);
		balance.setSecuritiesBalanceBase(securitiesBalanceBase);
		balance.setTotalBalance(config.isMarketOnClose() ? managerBalance.getMarketOnCloseTotalValue() : managerBalance.getAdjustedTotalValue());

		// By Default we populate Allocation amounts to the adjusted balances - i.e. 100% this can be overridden later for cases where there could be partial allocation (PIOS implementation)
		balance.setCashAllocationAmount(config.isMarketOnClose() ? managerBalance.getMarketOnCloseCashValue() : managerBalance.getAdjustedCashValue());
		balance.setSecuritiesAllocationAmount(config.isMarketOnClose() ? managerBalance.getMarketOnCloseSecuritiesValue() : managerBalance.getAdjustedSecuritiesValue());

		if (managerAccount.isRollupManager()) {
			populateChildManualAdjustmentListForManagerAccount(config, managerAccount, balance, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		}
	}


	private List<InvestmentManagerAccountBalanceAdjustment> getManualAdjustmentListForManagerAccount(PortfolioRunManagerAccountBalancePopulatorConfig config, InvestmentManagerAccount managerAccount) {
		List<InvestmentManagerAccountBalanceAdjustment> list = config.getManagerAccountManualAdjustmentList(managerAccount);
		if (list == null) {
			list = new ArrayList<>();
			InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(managerAccount.getId(), config.getPortfolioRun().getBalanceDate(), true);
			if (balance != null) {
				for (InvestmentManagerAccountBalanceAdjustment adjustment : CollectionUtils.getIterable(balance.getAdjustmentList())) {
					if (isAdjustmentAppliedToRun(config, adjustment) && !adjustment.getAdjustmentType().isSystemDefined()) {
						list.add(adjustment);
					}
				}
			}
			config.addManagerAccountManualAdjustmentListToMap(managerAccount, list);
		}
		return list;
	}


	private void populateChildManualAdjustmentListForManagerAccount(PortfolioRunManagerAccountBalancePopulatorConfig config, InvestmentManagerAccount managerAccount, PortfolioRunManagerAccountBalance balance, BigDecimal allocationPercent) {
		List<InvestmentManagerAccountRollup> rollups = getInvestmentManagerAccountService().getInvestmentManagerAccountRollupListByParent(managerAccount.getId());
		for (InvestmentManagerAccountRollup rollup : CollectionUtils.getIterable(rollups)) {
			if (!rollup.getReferenceTwo().isInactive()) {
				InvestmentManagerAccount childManager = rollup.getReferenceTwo();
				List<InvestmentManagerAccountBalanceAdjustment> adjList = getManualAdjustmentListForManagerAccount(config, childManager);
				balance.addChildManualAdjustmentList(rollup.getAllocationPercent(), adjList);
				if (childManager.isRollupManager()) {
					populateChildManualAdjustmentListForManagerAccount(config, childManager, balance, MathUtils.getPercentageOf(allocationPercent, rollup.getAllocationPercent()));
				}
			}
		}
	}


	private boolean isAdjustmentAppliedToRun(PortfolioRunManagerAccountBalancePopulatorConfig config, InvestmentManagerAccountBalanceAdjustment adjustment) {
		return (!adjustment.getAdjustmentType().isMarketOnClose() || config.isMarketOnClose());
	}

//////////////////////////////////////////////////////////////
////////          Getter and Setter Methods           ////////
//////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}
}
