package com.clifton.portfolio.run.workflow;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.search.PortfolioRunSearchForm;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.List;


/**
 * The <code>PortfolioRunTradeReadyWorkflowAction</code> handles
 * closing the previous run that was available for trading.
 * <p/>
 * There can be only ONE non-MOC run and ONE MOC run available for trading at any time.
 * <p/>
 * When an analyst approves a run and sends it to PM for review trading, anything currently
 * active in PM's list is closed.  If the analyst is running a historical run, when submitting it
 * is automatically moved to closed.
 * <p/>
 * Current runs considered "available" for trading are those in Pending or Approved status, although
 * once can only trade in Pending status (Approved status can push back to Pending if need to enter additional trades)
 *
 * @author manderson
 */
public class PortfolioRunTradeReadyWorkflowAction implements WorkflowTransitionActionHandler<PortfolioRun> {

	private PortfolioRunService portfolioRunService;
	private WorkflowTransitionService workflowTransitionService;


	@Override
	public PortfolioRun processAction(PortfolioRun bean, WorkflowTransition transition) {
		// Check for latest run and mark as complete if this is the newest one, or automatically mark this as completed
		PortfolioRun tradingRun = getPortfolioRunTradingEntity(bean);
		if (tradingRun != null) {
			// Unique By MOC flag as well - so can trade on both the regular and the MOC runs for a date
			// If bean's balance date is more recent (or equal) then mark original tradingRun as complete and this run will now be used for generating trades
			if (bean.getBalanceDate().after(tradingRun.getBalanceDate()) || (DateUtils.compare(bean.getBalanceDate(), tradingRun.getBalanceDate(), false) == 0)) {
				getPortfolioRunService().closePortfolioRun(tradingRun);
			}
			// Otherwise auto move this run to completed
			else {
				getPortfolioRunService().closePortfolioRun(bean);
			}
		}
		return bean;
	}


	private PortfolioRun getPortfolioRunTradingEntity(PortfolioRun bean) {
		PortfolioRunSearchForm searchForm = new PortfolioRunSearchForm();
		searchForm.setClientInvestmentAccountId(bean.getClientInvestmentAccount().getId());
		searchForm.setMarketOnCloseAdjustmentsIncluded(bean.isMarketOnCloseAdjustmentsIncluded());
		searchForm.setWorkflowStatusNames(new String[]{WorkflowStatus.STATUS_APPROVED, WorkflowStatus.STATUS_PENDING});
		searchForm.setOrderBy("balanceDate:desc#updateDate:desc");
		// There is a High Priority Notification that catches cases where more than one is available for trading
		// SHOULD ONLY HAVE TWO (CURRENT TRADING ONE PLUS THIS ONE)
		// Return first one that isn't the same
		List<PortfolioRun> list = getPortfolioRunService().getPortfolioRunList(searchForm);
		for (PortfolioRun run : CollectionUtils.getIterable(list)) {
			if (!bean.equals(run)) {
				return run;
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////
	//////         Getter and Setter Methods         ///////
	////////////////////////////////////////////////////////


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}
}
