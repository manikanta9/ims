package com.clifton.portfolio.run.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This class allows for new beans and definitions to be created that specify a beanPath to a target value from the run that will
 * be compared against the same value of the previous run and will trigger violations if the value change is outside the specified bands.
 *
 * @author stevenf
 */
public class PortfolioRunPreviousRunValueComparisonRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private String portfolioRunTargetValueBeanPath;
	private Integer violationThresholdAmount;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			PortfolioRun previousRun = context.getPreviousRun(run);

			//Default values to ZERO if null, this means violations will be triggered when an account is run for the first time
			//or the first time the target value exists on a current run but not on the previous and vice-versa.
			BigDecimal runTargetValue = (BigDecimal) BeanUtils.getPropertyValue(run, getPortfolioRunTargetValueBeanPath());
			if (runTargetValue == null) {
				runTargetValue = BigDecimal.ZERO;
			}
			BigDecimal previousRunTargetValue = (BigDecimal) BeanUtils.getPropertyValue(previousRun, getPortfolioRunTargetValueBeanPath());
			if (previousRunTargetValue == null) {
				previousRunTargetValue = BigDecimal.ZERO;
			}

			BigDecimal percentChange = MathUtils.round(MathUtils.getPercentChange(previousRunTargetValue, runTargetValue, true), 2);
			//If the difference of the current value and the previous value is greater than the threshold then run the violation.
			if (MathUtils.abs(MathUtils.subtract(runTargetValue, previousRunTargetValue)).compareTo(MathUtils.abs(BigDecimal.valueOf(getViolationThresholdAmount()))) > 0) {
				String violationNote = RuleEvaluatorUtils.evaluateValueForRange(percentChange, entityConfig.getMinAmount(), entityConfig.getMaxAmount(), MathUtils.NUMBER_FORMAT_MONEY, true);
				if (!StringUtils.isEmpty(violationNote)) {
					// Add values needed for freemarker template to context values map.
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("percentChange", percentChange);
					templateValues.put("runTargetValue", runTargetValue);
					templateValues.put("previousRunTargetValue", previousRunTargetValue);
					//generate violation note using freemarker template.
					ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, run.getId(), templateValues));
				}
			}
		}
		return ruleViolationList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getPortfolioRunTargetValueBeanPath() {
		return this.portfolioRunTargetValueBeanPath;
	}


	public void setPortfolioRunTargetValueBeanPath(String portfolioRunTargetValueBeanPath) {
		this.portfolioRunTargetValueBeanPath = portfolioRunTargetValueBeanPath;
	}


	public Integer getViolationThresholdAmount() {
		return this.violationThresholdAmount;
	}


	public void setViolationThresholdAmount(Integer violationThresholdAmount) {
		this.violationThresholdAmount = violationThresholdAmount;
	}
}
