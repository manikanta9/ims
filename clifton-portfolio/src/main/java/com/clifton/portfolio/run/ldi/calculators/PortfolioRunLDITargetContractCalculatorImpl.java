package com.clifton.portfolio.run.ldi.calculators;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposureKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIService;
import com.clifton.portfolio.run.ldi.trade.PortfolioRunTradeLDI;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.math.BigDecimal;
import java.util.List;


/**
 * This class is the default implementation of the PortfolioRunLDITargetContractCalculator that calculates targets for Futures only. All non-futures positions are used to reduce the overall target.
 *
 * @author davidi
 */
public class PortfolioRunLDITargetContractCalculatorImpl implements PortfolioRunLDITargetContractCalculator {

	private PortfolioRunLDIService portfolioRunLDIService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Calculates the contact target quantities for LDI Future trades and, updates each PortfolioRunLDIExposure entry's targetContract property within the specified PortfolioRunTradeLDI class instance.
	 */
	@Override
	public void calculateTargetContracts(PortfolioRunTradeLDI run) {
		try {
			List<PortfolioRunLDIExposure> exposureList = CoreCollectionUtils.clone(run.getDetailList());
			CollectionUtils.sort(exposureList, (entryA, entryB) -> MathUtils.compare(entryA.getSecurity().getId(), entryB.getSecurity().getId()));
			exposureList = CollectionUtils.getFiltered(exposureList, exposureEntry -> InvestmentUtils.isSecurityOfType(exposureEntry.getSecurity(), InvestmentType.FUTURES));

			RealVector targetVector = createTradeTargetExposureValueVector(run);
			RealMatrix contractValueMatrix = populateTradeDv01ContractValueMatrix(exposureList);

			// solution calculation -- only works with square matrix and target vector of the same dimension as the matrix row / column dimension
			RealVector targetContractsForSecurity = new LUDecomposition(contractValueMatrix).getSolver().solve(targetVector);

			for (int index = 0; index < exposureList.size(); ++index) {
				exposureList.get(index).setTargetContracts(MathUtils.round(BigDecimal.valueOf(targetContractsForSecurity.getEntry(index)), 0));
			}
		}
		catch (Exception exc) {
			// Cannot propagate exception to service because it will stop the remainder of the trading grid from being populated.
			LogUtils.error(this.getClass(), ExceptionUtils.getOriginalMessage(exc));
		}
	}


	/**
	 * Creates a vector of TargetDV01(adj) values used in the calculation to solve for a vector of contract quantities.  The vector may be adjusted down in value if
	 * one or more non-Future positions exist with exposures that affect the TargetDV01 value.
	 */
	private RealVector createTradeTargetExposureValueVector(PortfolioRunTradeLDI run) {
		List<PortfolioRunLDIKeyRateDurationPoint> keyRateDurationPointList = getPortfolioRunLDIService().getPortfolioRunLDIKeyRateDurationPointListByRun(run.getId(), false);

		// Sort the list by the MarketDataTypeID so vector element is consistent with the matrix row ordering.
		CollectionUtils.sort(keyRateDurationPointList, (entryA, entryB) -> MathUtils.compare(entryA.getKeyRateDurationPoint().getMarketDataField().getId(), entryB.getKeyRateDurationPoint().getMarketDataField().getId()));

		// An array of summed Target Exposure values, with each index representing a keyRateDuration point for a given TargetDV01.
		BigDecimal[] sumOfNonFuturePositionsDV01ExposureValues = getSumOfNonFuturePositionsDV01ExposureValues(run.getDetailList(), keyRateDurationPointList.size());
		double[] targetDV01VectorData = new double[keyRateDurationPointList.size()];
		int targetVectorIndex = 0;

		for (PortfolioRunLDIKeyRateDurationPoint keyRateDurationPoint : CollectionUtils.getIterable(keyRateDurationPointList)) {
			BigDecimal reducedTargetValue = MathUtils.subtract(keyRateDurationPoint.getExposureTargetDv01Value(), sumOfNonFuturePositionsDV01ExposureValues[targetVectorIndex]);
			targetDV01VectorData[targetVectorIndex++] = reducedTargetValue.doubleValue();
		}

		return MatrixUtils.createRealVector(targetDV01VectorData);
	}


	/**
	 * Creates an array of offsets to be applied to TargetDV01(Adj).  These offsets are the sum of DV01 exposure values of non-Future positions that correspond to a TargetDV01(adj) entry in a PortfolioRunLDIKeyRateDurationPoint.
	 */
	private BigDecimal[] getSumOfNonFuturePositionsDV01ExposureValues(List<PortfolioRunLDIExposure> exposureList, int targetVectorSize) {
		List<PortfolioRunLDIExposure> portfolioRunLDINonFuturesExposureList = CollectionUtils.getFiltered(exposureList, exposureEntry -> !InvestmentUtils.isSecurityOfType(exposureEntry.getSecurity(), InvestmentType.FUTURES));
		BigDecimal[] dv01ExposureValues = new BigDecimal[targetVectorSize];

		if (!portfolioRunLDINonFuturesExposureList.isEmpty()) {
			for (PortfolioRunLDIExposure exposureEntry : CollectionUtils.getIterable(portfolioRunLDINonFuturesExposureList)) {
				List<PortfolioRunLDIExposureKeyRateDurationPoint> keyRateDurationPointList = exposureEntry.getKeyRateDurationPointList();
				// Sort the list by the MarketDataTypeID so each row ordering is consistent
				CollectionUtils.sort(keyRateDurationPointList, (entryA, entryB) -> MathUtils.compare(entryA.getKeyRateDurationPoint().getMarketDataField().getId(), entryB.getKeyRateDurationPoint().getMarketDataField().getId()));
				int rowIndex = 0;
				AssertUtils.assertEquals(keyRateDurationPointList.size(), targetVectorSize, "Size of keyRateDurationPointList for Portfolio Run LDI Exposure entry must be " + targetVectorSize + ".");
				for (PortfolioRunLDIExposureKeyRateDurationPoint durationPoint : CollectionUtils.getIterable(keyRateDurationPointList)) {
					BigDecimal exposure = MathUtils.multiply(exposureEntry.getActualContracts(), durationPoint.getDv01ContractValue());
					dv01ExposureValues[rowIndex] = MathUtils.add(dv01ExposureValues[rowIndex], exposure);
					++rowIndex;
				}
			}
		}

		return dv01ExposureValues;
	}


	private RealMatrix populateTradeDv01ContractValueMatrix(List<PortfolioRunLDIExposure> exposureList) {
		int colSize = exposureList.size();
		int colIndex = 0;

		// Matrix must be square such that rowSize = colSize
		double[][] matrixData = new double[colSize][colSize];

		for (PortfolioRunLDIExposure exposureEntry : CollectionUtils.getIterable(exposureList)) {
			List<PortfolioRunLDIExposureKeyRateDurationPoint> exposureKeyRateDurationPointList = exposureEntry.getKeyRateDurationPointList();
			// Sort the list by the MarketDataTypeID so each row is consistent
			CollectionUtils.sort(exposureKeyRateDurationPointList, (entryA, entryB) -> MathUtils.compare(entryA.getKeyRateDurationPoint().getMarketDataField().getId(), entryB.getKeyRateDurationPoint().getMarketDataField().getId()));
			int rowIndex = 0;
			AssertUtils.assertEquals(exposureKeyRateDurationPointList.size(), colSize, "Matrix must be square, row dimension must be the same as column dimension.");
			for (PortfolioRunLDIExposureKeyRateDurationPoint durationPoint : CollectionUtils.getIterable(exposureKeyRateDurationPointList)) {
				matrixData[rowIndex++][colIndex] = durationPoint.getTradeDv01ContractValue().doubleValue();
			}
			++colIndex;
		}

		return MatrixUtils.createRealMatrix(matrixData);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunLDIService getPortfolioRunLDIService() {
		return this.portfolioRunLDIService;
	}


	public void setPortfolioRunLDIService(PortfolioRunLDIService portfolioRunLDIService) {
		this.portfolioRunLDIService = portfolioRunLDIService;
	}
}

