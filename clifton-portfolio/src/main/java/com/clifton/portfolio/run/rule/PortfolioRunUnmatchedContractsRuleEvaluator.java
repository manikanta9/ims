package com.clifton.portfolio.run.rule;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The <code>ProductOverlayUnmatchedContractsRuleEvaluator</code> is a rule evaluator that compares the actual positions held
 * in the account(s) with the positions/quantities in {@link com.clifton.portfolio.run.BasePortfolioRunReplication}.
 *
 * @author Mary Anderson
 * @author stevenf
 */
public class PortfolioRunUnmatchedContractsRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private InvestmentGroupService investmentGroupService;
	private InvestmentInstrumentService investmentInstrumentService;

	/**
	 * Option to exclude collateral positions from the check
	 * NOTE: IF any portion of the security is held as collateral the position quantity will be excluded from the check
	 */
	private boolean excludeCollateral;

	private Short excludeInvestmentGroupId;
	private Short includeInvestmentGroupId;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {

			// 1.  Get all Accounting Positions As a Map<Integer, BigDecimal> where the Key = SecurityID and the Value is the Net Quantity of Positions
			PortfolioAccountContractStore contractStore = context.getPortfolioAccountContractStoreActual(context.getMainClientAccount(run), run.getBalanceDate(), false);
			// filter out securities with zero cumulative quantities
			Map<Integer, BigDecimal> contractQuantityMap = contractStore.getSecurityQuantityMap().entrySet().stream()
					.filter(e -> !MathUtils.isNullOrZero(e.getValue().getQuantity()))
					.collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getQuantity()));
			Map<Integer, BigDecimal> collateralContractQuantityMap = contractStore.getSecurityCollateralQuantityMap().entrySet().stream()
					.filter(e -> !MathUtils.isNullOrZero(e.getValue().getQuantity()))
					.collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getQuantity()));
			BigDecimal totalContracts = contractStore.getTotalQuantity().getQuantity();

			// 2.  Get the ProductOverlayAssetClassReplication List for the Run as a Map<String, BigDecimal> where the Key = ClientAccountID_SecurityID and the Value = Net Quantity of Actual Contracts
			List<BasePortfolioRunReplication> replicationList = context.getReplicationList(run);
			Map<Integer, BigDecimal> piosContractQuantityMap = new HashMap<>();

			boolean hasCurrencyReplication = false;
			BigDecimal piosContracts = BigDecimal.ZERO;
			for (BasePortfolioRunReplication replication : CollectionUtils.getIterable(replicationList)) {
				// Note: For comparing Actual contracts to PIOS Contracts - Use real actual - not adjusted with virtual
				// Virtual contracts will just cancel each other out.
				// Skip those where contracts are excluded - assumed to be included elsewhere
				if (replication.getAccountAssetClass() == null || !replication.getAccountAssetClass().isReplicationPositionExcludedFromCount()) {
					Integer securityId = replication.getSecurity().getId();
					BigDecimal quantity = replication.getActualContracts();
					if (replication.isCurrencyReplication()) {
						// If this is a currency replication we still need to add the original replication contract value to the map
						updatePIOSContractQuantityMap(piosContractQuantityMap, securityId, quantity);
						// Now get the underlying security and it's amount, subtract from contract total since it is a currency value.
						quantity = replication.getCurrencyActualLocalAmount() != null ? replication.getCurrencyActualLocalAmount() : BigDecimal.ZERO;
						securityId = replication.getSecurity().getUnderlyingSecurity().getId();
						// If there are currency replications, then we need to check for currency other later.
						hasCurrencyReplication = true;
						totalContracts = MathUtils.subtract(totalContracts, quantity);
					}
					else {
						piosContracts = MathUtils.add(piosContracts, quantity);
					}
					updatePIOSContractQuantityMap(piosContractQuantityMap, securityId, quantity);
				}
			}
			if (hasCurrencyReplication) {
				List<PortfolioCurrencyOther> currencyOtherList = context.getPortfolioCurrencyOtherListByRun(run);
				for (PortfolioCurrencyOther currencyOther : CollectionUtils.getIterable(currencyOtherList)) {
					updatePIOSContractQuantityMap(piosContractQuantityMap, currencyOther.getCurrencySecurity().getId(), currencyOther.getLocalAmount());
					totalContracts = MathUtils.subtract(totalContracts, currencyOther.getLocalAmount());
				}
			}

			//Load group names now so we don't have to look up the name for each security.
			String excludeGroupName = null;
			if (getExcludeInvestmentGroupId() != null) {
				InvestmentGroup group = getInvestmentGroupService().getInvestmentGroup(getExcludeInvestmentGroupId());
				excludeGroupName = group != null ? group.getName() : null;
			}
			String includeGroupName = null;
			if (getIncludeInvestmentGroupId() != null) {
				InvestmentGroup group = getInvestmentGroupService().getInvestmentGroup(getIncludeInvestmentGroupId());
				includeGroupName = group != null ? group.getName() : null;
			}

			// 3.  Match Up the Accounting Position Map to the Replication Map - Any leftovers in the Accounting Positions Map, Verify Against System Condition on What Securities to Exclude
			Map<String, Object> unmatchedContracts = new HashMap<>();
			for (Map.Entry<Integer, BigDecimal> integerBigDecimalEntry : contractQuantityMap.entrySet()) {
				// Position Quantity (Includes Collateral and Not Collateral) in Accounting
				BigDecimal contractQuantity = MathUtils.round(integerBigDecimalEntry.getValue(), 2);
				// Position Collateral Quantity in Accounting
				BigDecimal collateralQuantity = MathUtils.round(collateralContractQuantityMap.get(integerBigDecimalEntry.getKey()), 2);
				// Run Quantity
				BigDecimal piosContractQuantity = piosContractQuantityMap.get(integerBigDecimalEntry.getKey());

				if (piosContractQuantity != null) {
					// Subtract what we have in PIOS
					contractQuantity = MathUtils.subtract(contractQuantity, MathUtils.round(piosContractQuantity, 2));
					// If all were accounted for, continue on
					if (MathUtils.isNullOrZero(contractQuantity)) {
						continue;
					}
				}

				InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(integerBigDecimalEntry.getKey());
				if (skipProcessing(security, contractQuantity, collateralQuantity, excludeGroupName, includeGroupName)) {
					totalContracts = MathUtils.subtract(totalContracts, contractQuantity);
					continue;
				}
				Map<String, Object> contractValues = new HashMap<>();
				contractValues.put("symbol", security.getSymbol());
				contractValues.put("contractQuantity", contractQuantity);
				contractValues.put("collateralQuantity", collateralQuantity);
				contractValues.put("piosContractQuantity", piosContractQuantity);
				unmatchedContracts.put(String.valueOf(security.getId()), contractValues);
			}

			// 4.  If violation(s) found, create rule violation
			if (!unmatchedContracts.isEmpty()) {
				//Use linkedHashMap to maintain order so tests can depend on it.
				Map<String, Object> templateValues = new LinkedHashMap<>();
				templateValues.put("totalContracts", totalContracts);
				templateValues.put("piosContracts", piosContracts);
				templateValues.put("unmatchedContracts", unmatchedContracts);
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, run.getId(), templateValues));
			}
		}
		return ruleViolationList;
	}


	private void updatePIOSContractQuantityMap(Map<Integer, BigDecimal> piosContractQuantityMap, Integer securityId, BigDecimal quantity) {
		if (piosContractQuantityMap.containsKey(securityId)) {
			quantity = MathUtils.add(quantity, piosContractQuantityMap.get(securityId));
		}
		piosContractQuantityMap.put(securityId, quantity);
	}


	private boolean skipProcessing(InvestmentSecurity security, BigDecimal contractQuantity, BigDecimal collateralQuantity, String excludeGroupName, String includeGroupName) {
		// If we are excluding collateral and any portion of the security is held as collateral - skip it
		if (isExcludeCollateral() && !MathUtils.isNullOrZero(collateralQuantity)) {
			return true;
		}
		// LMEs with net allocation of 0, skip
		if (MathUtils.isNullOrZero(contractQuantity) && InvestmentUtils.isCloseOnMaturityOnly(security)) {
			return true;
		}
		if (excludeGroupName != null && getInvestmentGroupService().isInvestmentSecurityInGroup(excludeGroupName, security.getId())) {
			return true;
		}
		//If an explicit inclusion is assigned, then skip all other groups.
		if (includeGroupName != null) {
			return !getInvestmentGroupService().isInvestmentSecurityInGroup(includeGroupName, security.getId());
		}
		return false;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public Short getExcludeInvestmentGroupId() {
		return this.excludeInvestmentGroupId;
	}


	public void setExcludeInvestmentGroupId(Short excludeInvestmentGroupId) {
		this.excludeInvestmentGroupId = excludeInvestmentGroupId;
	}


	public Short getIncludeInvestmentGroupId() {
		return this.includeInvestmentGroupId;
	}


	public void setIncludeInvestmentGroupId(Short includeInvestmentGroupId) {
		this.includeInvestmentGroupId = includeInvestmentGroupId;
	}


	public boolean isExcludeCollateral() {
		return this.excludeCollateral;
	}


	public void setExcludeCollateral(boolean excludeCollateral) {
		this.excludeCollateral = excludeCollateral;
	}
}
