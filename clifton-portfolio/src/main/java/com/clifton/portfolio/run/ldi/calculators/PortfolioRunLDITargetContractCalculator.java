package com.clifton.portfolio.run.ldi.calculators;

import com.clifton.portfolio.run.ldi.trade.PortfolioRunTradeLDI;


/**
 * A calculator used to compute and return a contract target dictionary for LDI Future Trades in Portfolio Runs for LDI accounts.
 * The dictionary is keyed on the InvestmentSecurityIds for the set of investment securities used by the Portfolio Run.
 *
 * @author davidi
 */
public interface PortfolioRunLDITargetContractCalculator {

	/**
	 * Calculates the contact target quantities for LDI trades and, updates each PortfolioRunLDIExposure entry's targetContract property within
	 * the specified PortfolioRunTradeLDI class instance.
	 */
	public void calculateTargetContracts(PortfolioRunTradeLDI run);
}
