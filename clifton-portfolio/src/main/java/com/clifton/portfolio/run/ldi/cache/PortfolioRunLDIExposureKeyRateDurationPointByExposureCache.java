package com.clifton.portfolio.run.ldi.cache;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposureKeyRateDurationPoint;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>PortfolioRunLDIExposureKeyRateDurationPointByRunCache</code> stores the list of {@link com.clifton.portfolio.run.ldi.PortfolioRunLDIExposureKeyRateDurationPoint} objects
 * by {@link PortfolioRunLDIExposure#id} and {@link PortfolioAccountKeyRateDurationPoint#isReportableOnly()}.
 *
 * @author michaelm
 */
@Component
public class PortfolioRunLDIExposureKeyRateDurationPointByExposureCache extends SelfRegisteringCompositeKeyDaoListCache<PortfolioRunLDIExposureKeyRateDurationPoint, Integer, Boolean> {


	@Override
	protected String getBeanKey1Property() {
		return "exposure.id";
	}


	@Override
	protected Integer getBeanKey1Value(PortfolioRunLDIExposureKeyRateDurationPoint bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getExposure());
		}
		return null;
	}


	@Override
	protected String getBeanKey2Property() {
		return "keyRateDurationPoint.reportableOnly";
	}


	@Override
	protected Boolean getBeanKey2Value(PortfolioRunLDIExposureKeyRateDurationPoint bean) {
		if (bean != null && bean.getKeyRateDurationPoint() != null) {
			return bean.getKeyRateDurationPoint().isReportableOnly();
		}
		return false;
	}


	/**
	 * When bean isn't set in the cache, use this method to determine how to look it up in the database and immediately set in the cache.
	 */
	@Override
	protected List<PortfolioRunLDIExposureKeyRateDurationPoint> lookupBeanList(ReadOnlyDAO<PortfolioRunLDIExposureKeyRateDurationPoint> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		AdvancedReadOnlyDAO<PortfolioRunLDIExposureKeyRateDurationPoint, Criteria> advancedDao = (AdvancedReadOnlyDAO<PortfolioRunLDIExposureKeyRateDurationPoint, Criteria>) dao;
		HibernateSearchConfigurer searchConfig = new HibernateSearchConfigurer() {
			@Override
			public void configureCriteria(Criteria criteria) {
				criteria.createAlias("exposure", "e");
				criteria.createAlias("keyRateDurationPoint", "ap");
				criteria.add(Restrictions.eq("e.id", keyProperties[0]));
				if (keyProperties[1] != null) {
					criteria.add(Restrictions.eq("ap.reportableOnly", keyProperties[1]));
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				criteria.createAlias("exposure.replicationAllocationHistory", "rah");
				criteria.createAlias("rah.replicationAllocation", "ra");
				criteria.addOrder(Order.asc("ra.order"));
				criteria.addOrder(Order.asc("exposure.id"));
				criteria.createAlias("ap.marketDataField", "p");
				criteria.addOrder(Order.asc("p.fieldOrder"));
				return true;
			}
		};
		return advancedDao.findBySearchCriteria(searchConfig);
	}
}
