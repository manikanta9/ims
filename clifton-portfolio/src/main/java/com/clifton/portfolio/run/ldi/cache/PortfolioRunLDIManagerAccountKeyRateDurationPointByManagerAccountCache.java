package com.clifton.portfolio.run.ldi.cache;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccount;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccountKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.search.PortfolioRunLDIManagerAccountKeyRateDurationPointSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>PortfolioRunLDIManagerAccountKeyRateDurationPointByRunCache</code> stores the list of {@link PortfolioRunLDIManagerAccountKeyRateDurationPoint} objects
 * by {@link PortfolioRunLDIManagerAccount#id} and {@link PortfolioAccountKeyRateDurationPoint#isReportableOnly()}.
 *
 * @author michaelm
 */
@Component
public class PortfolioRunLDIManagerAccountKeyRateDurationPointByManagerAccountCache extends SelfRegisteringCompositeKeyDaoListCache<PortfolioRunLDIManagerAccountKeyRateDurationPoint, Integer, Boolean> {


	@Override
	protected String getBeanKey1Property() {
		return "managerAccount.id";
	}


	@Override
	protected Integer getBeanKey1Value(PortfolioRunLDIManagerAccountKeyRateDurationPoint bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getManagerAccount());
		}
		return null;
	}


	@Override
	protected String getBeanKey2Property() {
		return "keyRateDurationPoint.reportableOnly";
	}


	@Override
	protected Boolean getBeanKey2Value(PortfolioRunLDIManagerAccountKeyRateDurationPoint bean) {
		if (bean != null && bean.getKeyRateDurationPoint() != null) {
			return bean.getKeyRateDurationPoint().isReportableOnly();
		}
		return false;
	}


	/**
	 * When bean isn't set in the cache, use this method to determine how to look it up in the database and immediately set in the cache.
	 */
	@Override
	protected List<PortfolioRunLDIManagerAccountKeyRateDurationPoint> lookupBeanList(ReadOnlyDAO<PortfolioRunLDIManagerAccountKeyRateDurationPoint> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		AdvancedReadOnlyDAO<PortfolioRunLDIManagerAccountKeyRateDurationPoint, Criteria> advancedDao = (AdvancedReadOnlyDAO<PortfolioRunLDIManagerAccountKeyRateDurationPoint, Criteria>) dao;
		PortfolioRunLDIManagerAccountKeyRateDurationPointSearchForm searchForm = new PortfolioRunLDIManagerAccountKeyRateDurationPointSearchForm();
		searchForm.setManagerAccountId((Integer) keyProperties[0]);
		searchForm.setReportableOnly((Boolean) keyProperties[1]);
		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public boolean configureOrderBy(Criteria criteria) {
				criteria.addOrder(Order.desc(getPathAlias("managerAccount.managerAccountAssignment", criteria, JoinType.LEFT_OUTER_JOIN) + ".cashType"));
				criteria.addOrder(Order.asc(getPathAlias("managerAccount", criteria) + ".id"));
				criteria.addOrder(Order.asc(getPathAlias("keyRateDurationPoint.marketDataField", criteria) + ".fieldOrder"));
				return true;
			}
		};
		return advancedDao.findBySearchCriteria(searchConfig);
	}
}
