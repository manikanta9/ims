package com.clifton.portfolio.run.rule.manager;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalance;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Evaluates for a Portfolio Run manager accounts that were either added or removed (system bean property) from an account's setup since the previous main run.
 *
 * @author manderson
 */
public class PortfolioRunManagerAccountAddedRemovedRuleEvaluator extends BasePortfolioRunManagerAccountRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	/**
	 * True to check for manager accounts that are no longer assigned to the account's run
	 * False to check for manager accounts that weren't on the previous main run, but are included now
	 */
	private boolean checkRemovedManagers;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun ruleBean, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		PortfolioRun previousRun = context.getPreviousRun(ruleBean);
		List<PortfolioRunManagerAccountBalance> previousManagerList = new ArrayList<>();
		if (previousRun == null && isCheckRemovedManagers()) {
			// If previous run is null and we are checking for removed managers - nothing to check
			return ruleViolationList;
		}
		if (previousRun != null) {
			previousManagerList = context.getPortfolioRunManagerAccountBalanceList(ruleBean, true);
		}

		List<PortfolioRunManagerAccountBalance> newManagerList = context.getPortfolioRunManagerAccountBalanceList(ruleBean, false);
		Map<Integer, String> differenceMap = getPortfolioRunManagerAccountBalanceDifferenceList(isCheckRemovedManagers() ? previousManagerList : newManagerList, isCheckRemovedManagers() ? newManagerList : previousManagerList, ruleConfig);

		for (Integer managerAccountId : differenceMap.keySet()) {
			Map<String, Object> templateValues = new HashMap<>();
			ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(ruleConfig.getEntityConfig(MathUtils.getNumberAsLong(managerAccountId)), BeanUtils.getIdentityAsLong(ruleBean), MathUtils.getNumberAsLong(managerAccountId), MathUtils.getNumberAsLong(managerAccountId), null, templateValues));
		}
		return ruleViolationList;
	}


	private Map<Integer, String> getPortfolioRunManagerAccountBalanceDifferenceList(List<PortfolioRunManagerAccountBalance> fromList, List<PortfolioRunManagerAccountBalance> toList, RuleConfig ruleConfig) {
		Map<Integer, String> managerDifferenceMap = new HashMap<>();
		for (PortfolioRunManagerAccountBalance fromManagerBalance : CollectionUtils.getIterable(fromList)) {
			InvestmentManagerAccount managerAccount = fromManagerBalance.getManagerAccountBalance().getManagerAccount();
			Integer managerAccountId = managerAccount.getId();
			// If already in the map - don't check again - some managers are used across asset classes
			if (!managerDifferenceMap.containsKey(managerAccountId)) {
				EntityConfig entityConfig = getEntityConfig(managerAccount, ruleConfig);
				if (entityConfig != null && !entityConfig.isExcluded()) {
					boolean found = false;
					for (PortfolioRunManagerAccountBalance toManagerBalance : CollectionUtils.getIterable(toList)) {
						if (CompareUtils.isEqual(managerAccountId, toManagerBalance.getManagerAccountBalance().getManagerAccount().getId())) {
							found = true;
							break;
						}
					}
					if (!found) {
						managerDifferenceMap.put(managerAccountId, fromManagerBalance.getManagerAccountBalance().getManagerAccount().getLabel());
					}
				}
			}
		}
		return managerDifferenceMap;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public boolean isCheckRemovedManagers() {
		return this.checkRemovedManagers;
	}


	public void setCheckRemovedManagers(boolean checkRemovedManagers) {
		this.checkRemovedManagers = checkRemovedManagers;
	}
}
