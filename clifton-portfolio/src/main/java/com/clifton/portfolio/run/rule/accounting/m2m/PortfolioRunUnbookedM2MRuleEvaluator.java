package com.clifton.portfolio.run.rule.accounting.m2m;

import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.accounting.m2m.search.AccountingM2MDailySearchForm;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author stevenf
 */
public class PortfolioRunUnbookedM2MRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	//Allows specifying the holdingAccountType the rule should apply to, e.g. 'OTC_CLEARED'
	private Short holdingAccountTypeId;

	private AccountingM2MService accountingM2MService;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			Integer[] clientAccountIds = context.getClientAccountList(run).stream().map(BaseSimpleEntity::getId).toArray(Integer[]::new);
			AccountingM2MDailySearchForm accountingM2MDailySearchForm = new AccountingM2MDailySearchForm();
			accountingM2MDailySearchForm.setMarkDate(context.getBalanceDate(run));
			accountingM2MDailySearchForm.setNotTransferAmount(BigDecimal.ZERO);
			accountingM2MDailySearchForm.setClientInvestmentAccountIds(clientAccountIds);
			if (getHoldingAccountTypeId() != null) {
				accountingM2MDailySearchForm.setHoldingInvestmentAccountTypeId(getHoldingAccountTypeId());
			}
			accountingM2MDailySearchForm.setBooked(false);
			for (AccountingM2MDaily accountingM2MDaily : CollectionUtils.getIterable(getAccountingM2MService().getAccountingM2MDailyList(accountingM2MDailySearchForm))) {
				Map<String, Object> templateValues = new HashMap<>();
				templateValues.put(RuleEvaluator.VIOLATION_NOTE_FREEMARKER_TEMPLATE_KEY, "Client Account [" +
						accountingM2MDaily.getClientInvestmentAccount().getLabel() + "] has an unbooked M2M [" + accountingM2MDaily.getLabel() + "].");
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), accountingM2MDaily.getId(), null, templateValues));
			}
		}
		return ruleViolationList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}
}
