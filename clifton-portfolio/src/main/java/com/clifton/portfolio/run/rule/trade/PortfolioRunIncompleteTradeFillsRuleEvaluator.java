package com.clifton.portfolio.run.rule.trade;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeFillSearchForm;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunIncompleteTradeFillsRuleEvaluator</code> class is a rule evaluator
 * that generates violations during a PortfolioRun for accounts with trades with unbooked fills.
 *
 * @author sfahey
 */
public class PortfolioRunIncompleteTradeFillsRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private TradeService tradeService;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			for (InvestmentAccount clientAccount : CollectionUtils.getIterable(context.getClientAccountList(run))) {
				for (TradeFill tradeFill : CollectionUtils.getIterable(getUnbookedTradeFillsForCompletedTrades(clientAccount, context.getBalanceDate(run)))) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("clientAccount", clientAccount);
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), tradeFill.getId(), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}


	private List<TradeFill> getUnbookedTradeFillsForCompletedTrades(InvestmentAccount account, Date balanceDate) {
		TradeFillSearchForm tradeFillSearchForm = new TradeFillSearchForm();
		tradeFillSearchForm.setClientInvestmentAccountId(account.getId());
		tradeFillSearchForm.setWorkflowStatusName(TradeService.TRADES_CLOSED_STATUS_NAME);
		tradeFillSearchForm.setExcludeWorkflowStateName(TradeService.TRADES_CANCELLED_STATE_NAME);
		tradeFillSearchForm.setBookingDateIsNull(true);
		tradeFillSearchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN_OR_EQUALS, balanceDate));
		return getTradeService().getTradeFillList(tradeFillSearchForm);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
