package com.clifton.portfolio.run.rule.manager;

import com.clifton.core.beans.IdentityObject;
import com.clifton.rule.evaluator.RuleEvaluatorContext;


/**
 * This class extends in order to provide consistent naming conventions for its subclasses while avoiding duplicate code.
 *
 * @author StevenF
 */
public abstract class BasePortfolioRunManagerAccountRangeRuleEvaluator<T extends IdentityObject, K extends RuleEvaluatorContext> extends BasePortfolioRunManagerAccountRuleEvaluator<T, K> {

	private String rangeType;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getRangeType() {
		return this.rangeType;
	}


	public void setRangeType(String rangeType) {
		this.rangeType = rangeType;
	}
}
