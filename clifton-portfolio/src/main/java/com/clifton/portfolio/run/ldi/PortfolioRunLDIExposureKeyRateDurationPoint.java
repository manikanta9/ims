package com.clifton.portfolio.run.ldi;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationPoint;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>PortfolioRunLDIExposureKeyRateDurationPoint</code> ...
 *
 * @author manderson
 */
public class PortfolioRunLDIExposureKeyRateDurationPoint extends BaseSimpleEntity<Integer> implements LabeledObject {

	/**
	 * Specific security information this point applies to
	 */
	private PortfolioRunLDIExposure exposure;

	/**
	 * Specific KRD point
	 */
	private PortfolioAccountKeyRateDurationPoint keyRateDurationPoint;

	/**
	 * Modified KRD Value
	 */
	private BigDecimal krdValue;

	/**
	 * DV01 Value of One Contract
	 */
	private BigDecimal dv01ContractValue;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder(16);
		if (getExposure() != null) {
			sb.append(getExposure().getLabel());
		}
		sb.append(" - ");
		if (getKeyRateDurationPoint() != null) {
			sb.append(getKeyRateDurationPoint().getMarketDataField().getName());
		}
		return sb.toString();
	}


	public BigDecimal getDv01ActualExposureValue() {
		if (getExposure() != null) {
			return MathUtils.multiply(getExposure().getActualContracts(), getDv01ContractValue());
		}
		return null;
	}


	public BigDecimal getDv01ContractValueCalculated() {
		return getDv01ContractValueCalculatedImpl(false);
	}


	public BigDecimal getTradeDv01ContractValue() {
		return getDv01ContractValueCalculatedImpl(true);
	}


	/**
	 * DV01 Value of One Contract = (KRD Value * Price * Exposure Multiplier)
	 * Calculated method so can re-use same logic when using Live Pricing
	 */
	private BigDecimal getDv01ContractValueCalculatedImpl(boolean tradeValue) {
		if (getExposure() != null) {
			BigDecimal price = null;
			BigDecimal fxRate = null;

			if (tradeValue) {
				price = getExposure().getTradeSecurityPrice();
				fxRate = getExposure().getTradeExchangeRate();
			}
			if (price == null) {
				// If Dirty Price is Populated Then Use it
				price = getExposure().getSecurityDirtyPrice();
				if (price == null) {
					price = getExposure().getSecurityPrice();
				}
			}
			if (fxRate == null) {
				fxRate = getExposure().getExchangeRate();
			}

			// If notional calculator has negative quantity flag and we hold short, need to use 200-price because price in market data is for long position
			if (MathUtils.isLessThan(getExposure().getActualContracts(), BigDecimal.ZERO) && InvestmentCalculatorUtils.getNotionalCalculator(getExposure().getSecurity()).isNegativeQuantity()) {
				price = MathUtils.subtract(new BigDecimal(200), price);
			}

			BigDecimal value = MathUtils.multiply(getKrdValue(), price);
			value = MathUtils.multiply(value, getExposure().getSecurity().getPriceMultiplier());
			value = MathUtils.multiply(value, fxRate);
			return MathUtils.getDV01Value(value);
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunLDIExposure getExposure() {
		return this.exposure;
	}


	public void setExposure(PortfolioRunLDIExposure exposure) {
		this.exposure = exposure;
	}


	public PortfolioAccountKeyRateDurationPoint getKeyRateDurationPoint() {
		return this.keyRateDurationPoint;
	}


	public void setKeyRateDurationPoint(PortfolioAccountKeyRateDurationPoint keyRateDurationPoint) {
		this.keyRateDurationPoint = keyRateDurationPoint;
	}


	public BigDecimal getKrdValue() {
		return this.krdValue;
	}


	public void setKrdValue(BigDecimal krdValue) {
		this.krdValue = krdValue;
	}


	public BigDecimal getDv01ContractValue() {
		return this.dv01ContractValue;
	}


	public void setDv01ContractValue(BigDecimal dv01ContractValue) {
		this.dv01ContractValue = dv01ContractValue;
	}
}
