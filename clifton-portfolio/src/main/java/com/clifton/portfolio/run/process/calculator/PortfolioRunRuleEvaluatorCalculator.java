package com.clifton.portfolio.run.process.calculator;

import com.clifton.portfolio.run.process.PortfolioRunConfig;


/**
 * The <code>PortfolioRunRuleEvaluatorCalculator</code> is an implementation of a step calculator for Portfolio Run processing that handles
 * running rule violations for the run
 *
 * @author manderson
 */
public class PortfolioRunRuleEvaluatorCalculator<T extends PortfolioRunConfig> extends BasePortfolioRunProcessStepCalculatorImpl<T> {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void processStep(T runConfig) {
		getPortfolioRunRuleHandler().validatePortfolioRunRules(runConfig.getRun(), runConfig.isPreProcess());
	}


	@Override
	protected void saveStep(T runConfig) {
		// NOTHING HERE
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////
}
