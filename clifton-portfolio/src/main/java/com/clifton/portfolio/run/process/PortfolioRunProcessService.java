package com.clifton.portfolio.run.process;


import com.clifton.portfolio.run.search.PortfolioRunSearchForm;
import org.springframework.web.bind.annotation.ModelAttribute;


/**
 * The <code>PortfolioRunProcessService</code> is the generic service that is exposed to process runs of all types.  It is aware of mapping defined for specific service types to call those processors for each run.
 *
 * @author manderson
 */
public interface PortfolioRunProcessService {

	////////////////////////////////////////////////////////////////////////////
	/////////           Portfolio Run Process Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Handles deleting PortfolioRuns.  Contained on the Process service so that each run's processor can
	 * delete related many side table information for the run
	 */
	public void deletePortfolioRun(int id);


	@ModelAttribute("result")
	public String processPortfolioRunList(PortfolioRunSearchForm searchForm, boolean reload, boolean reloadProxyManagers, boolean synchronous);


	/**
	 * Runs processing for PortfolioRun bean until a failure is found.
	 * Calls correct processor dependent on the service for the client account
	 * <p/>
	 * If current step = COMPLETED will throw an exception since you can't rerun after trades are generated.
	 */
	public void processPortfolioRun(int runId, boolean reloadProxyManagers, Integer currentDepth);
}
