package com.clifton.portfolio.run.rule;

import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;


/**
 * PortfolioRunRuleHandler is used by PortfolioRun processing to run/delete/save system defined rules
 *
 * @author manderson
 */
public interface PortfolioRunRuleHandler {


	/**
	 * Returns true if there are any non-ignorable violations on the run. Used to stop processing the run when failure is found.
	 */
	public boolean isPortfolioRunNotIgnorableWarningsExist(int runId);


	/**
	 * Deletes all non-ignorable violations - used prior to re-processing a run
	 */
	public void clearPortfolioRunNotIgnorableWarnings(int runId);


	/**
	 * Saves validation exception as a special system defined not ignorable violation.
	 */
	public void savePortfolioRunRuleViolationFromException(ValidationExceptionWithCause exception, int runId);


	/**
	 * Saves system defined violation found during processing
	 */
	public void savePortfolioRunRuleViolationSystemDefined(String ruleDefinitionName, int runId, RuleViolation ruleViolation);


	/**
	 * Evaluates Rules for given run - rules are handled in 2 steps so failures during pre-processing (missing pre-requisites) are found
	 * earlier before actually processing the run
	 */
	public void validatePortfolioRunRules(PortfolioRun run, boolean preProcess);


	////////////////////////////////////////////////////////////////////////////
	///////       Client Account Rule Assignment Value Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the rule assignment associated with the account for "Portfolio Run Rules" category
	 * and the given definition.  For that assignment, returns the max value - can be null if not used
	 */
	public BigDecimal getPortfolioRunRuleMaxAmount(String ruleDefinitionName, InvestmentAccount account);
}
