package com.clifton.portfolio.run.report.param;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.report.PortfolioRunReportService;
import com.clifton.portfolio.run.search.PortfolioRunSearchForm;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.params.ReportConfigParameter;
import com.clifton.report.params.ReportParameterConfigurer;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunReportParameterConfigurer</code> ...
 *
 * @author manderson
 */
@Component
public class PortfolioRunReportParameterConfigurer implements ReportParameterConfigurer<PortfolioRun> {

	private PortfolioRunReportService portfolioRunReportService;
	private PortfolioRunService portfolioRunService;


	@Override
	public String getParameterName() {
		return "portfolioRunInvestmentAccountGroupId";
	}


	@Override
	public List<PortfolioRun> getReportParameterValueList(ReportConfigParameter configParameter, Date reportDate) {
		if (configParameter == null || StringUtils.isEmpty(configParameter.getValue())) {
			return null;
		}

		PortfolioRunSearchForm searchForm = new PortfolioRunSearchForm();
		searchForm.setInvestmentAccountGroupId(new Integer(configParameter.getValue()));
		searchForm.setMainRun(true);
		searchForm.setBalanceDate(reportDate);
		searchForm.setOrderBy("clientInvestmentAccountId:ASC");
		return getPortfolioRunService().getPortfolioRunList(searchForm);
	}


	@Override
	public String getLabelForValue(PortfolioRun value) {
		return value.getLabel();
	}


	@Override
	public void populateReportParameterMap(Map<String, Object> parameterMap, PortfolioRun value) {
		parameterMap.put("RunID", value.getId());
		parameterMap.put("ClientAccountID", value.getClientInvestmentAccount().getId());
		parameterMap.put("BalanceDate", DateUtils.fromDateShort(value.getBalanceDate()));
	}


	@Override
	public Integer getReportIdForValue(ReportExportConfiguration exportConfig, PortfolioRun value) {
		if (exportConfig.getReportId() == null) {
			return getPortfolioRunReportService().getPortfolioRunReportId(value);
		}
		return exportConfig.getReportId();
	}


	@Override
	public void populateReportPostingFileConfiguration(ReportPostingFileConfiguration postConfig, PortfolioRun value) {
		// TODO DOESN'T LOOK LIKE THIS WAS USED - CONFIRM
		postConfig.setPostEntitySourceTableName(InvestmentAccount.INVESTMENT_ACCOUNT_TABLE_NAME);
		postConfig.setPostEntitySourceFkFieldId(value.getClientInvestmentAccount().getId());
	}


	/**
	 * If left null, the value from the ReportExportConfiguration (typically this is what is sent in from the UI) is used
	 */
	@Override
	public String generateFileNameWithoutExtension(PortfolioRun value, Date reportDate) {
		return null;
	}


	@Override
	public FileWrapper getAttachments(PortfolioRun value, ReportExportConfiguration configuration) {
		return null;
	}

	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public PortfolioRunReportService getPortfolioRunReportService() {
		return this.portfolioRunReportService;
	}


	public void setPortfolioRunReportService(PortfolioRunReportService portfolioRunReportService) {
		this.portfolioRunReportService = portfolioRunReportService;
	}
}
