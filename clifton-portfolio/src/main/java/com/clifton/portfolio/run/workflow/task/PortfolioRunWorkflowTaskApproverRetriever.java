package com.clifton.portfolio.run.workflow.task;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.search.PortfolioRunSearchForm;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistorySearchForm;
import com.clifton.workflow.history.WorkflowHistoryService;
import com.clifton.workflow.task.WorkflowTaskDefinition;
import com.clifton.workflow.task.populator.approver.WorkflowTaskApproverRetriever;

import java.util.Date;
import java.util.List;


/**
 * The PortfolioRunWorkflowTaskApproverRetriever class retrieve latest PortfolioRun for the specified client account and other parameters.
 * Then it looks up the latest specified workflow transition for that run and returns the user that performed that transition.
 * <p>
 * Can be used to get latest PM or Analyst that approved the run, etc.
 *
 * @author vgomelsky
 */
public class PortfolioRunWorkflowTaskApproverRetriever implements WorkflowTaskApproverRetriever, ValidationAware {

	private InvestmentAccountService investmentAccountService;
	private InvestmentManagerAccountService investmentManagerAccountService;
	private PortfolioRunService portfolioRunService;
	private SecurityUserService securityUserService;
	private WorkflowHistoryService workflowHistoryService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Specified the path of Client Account ID property from Workflow Task Linked Entity. For example, "id" when linked entity is Client Account.
	 */
	private String clientAccountIdPropertyPath;

	/**
	 * If we don't have access to a specific client account, but we do have access to the client then use that to find account(s) to use
	 */
	private String clientIdPropertyPath;

	/**
	 * Only used if clientIdPropertyPath is set - manager accounts will always use related clients (if no assignments and need to go through the client level)
	 */
	private boolean clientIdIncludeRelated;

	/**
	 * If we have access to a specific manager account, we use that to find the accounts assigned to the manager
	 * If there are none (i.e. used transitively through a rollup) then we just use the accounts for the client to find one
	 */
	private String managerAccountIdPropertyPath;

	private Boolean mainRun;
	/**
	 * Positive number of days to look back from today for Portfolio Run lookup.
	 */
	private int daysBackFromToday;

	/**
	 * Search Pattern for latest end Workflow State to look for.
	 * For example, "1st PM Approved" will match both "1st PM Approved - Trades" and "1st PM Approved - No Trades".
	 */
	private String endWorkflowStateName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertMutuallyExclusive("At least one, and only one of the following paths can be defined: client account id, manager account id, client id.", getClientAccountIdPropertyPath(), getClientIdPropertyPath());
	}


	@Override
	public SecurityUser getWorkflowTaskApprover(WorkflowTaskDefinition definition, IdentityObject linkedEntity) {
		if (linkedEntity == null) {
			throw new ValidationException("Linked Entity is required when retrieving Approver for Workflow Task Definition: " + definition);
		}
		// Specific Client Account
		if (!StringUtils.isEmpty(getClientAccountIdPropertyPath())) {
			Integer clientAccountId = MathUtils.getNumberAsInteger((Number) BeanUtils.getPropertyValue(linkedEntity, getClientAccountIdPropertyPath()));
			if (clientAccountId == null) {
				throw new ValidationException("Cannot find Client Account ID for linked entity: " + linkedEntity);
			}
			return getWorkflowTaskApproverForClientAccount(clientAccountId);
		}
		// Specific Manager Account
		if (!StringUtils.isEmpty(getManagerAccountIdPropertyPath())) {
			Integer managerAccountId = (Integer) BeanUtils.getPropertyValue(linkedEntity, getManagerAccountIdPropertyPath());
			if (managerAccountId == null) {
				throw new ValidationException("Cannot find Manager Account ID for linked entity: " + linkedEntity);
			}
			return getWorkflowTaskApproverForManagerAccount(managerAccountId);
		}
		// Specific Client
		if (!StringUtils.isEmpty(getClientIdPropertyPath())) {
			Integer clientId = (Integer) BeanUtils.getPropertyValue(linkedEntity, getClientIdPropertyPath());
			if (clientId == null) {
				throw new ValidationException("Cannot find Client ID for linked entity: " + linkedEntity);
			}
			return getWorkflowTaskApproverForClient(clientId, isClientIdIncludeRelated());
		}

		// Validation above should catch this
		throw new ValidationException("One of the following is required: client account id property path, manager account id property path, client id property path.");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private SecurityUser getWorkflowTaskApproverForClientAccount(int clientAccountId) {
		// first get latest Portfolio Runs for the specified criteria
		PortfolioRunSearchForm searchForm = new PortfolioRunSearchForm();
		searchForm.setClientInvestmentAccountId(clientAccountId);
		searchForm.setMainRun(getMainRun());
		searchForm.addSearchRestriction(new SearchRestriction("balanceDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, DateUtils.addDays(new Date(), -getDaysBackFromToday())));
		searchForm.setOrderBy("balanceDate:desc");
		searchForm.setLimit(5); // just to be safe because latest run may not be completed yet
		List<PortfolioRun> runList = getPortfolioRunService().getPortfolioRunList(searchForm);

		// how find the latest specified transition for the runs
		for (PortfolioRun run : CollectionUtils.getIterable(runList)) {
			WorkflowHistorySearchForm historySearchForm = new WorkflowHistorySearchForm();
			historySearchForm.setEntityId(BeanUtils.getIdentityAsLong(run));
			historySearchForm.setTableName(PortfolioRun.TABLE_PORTFOLIO_RUN);
			historySearchForm.setEndWorkflowStateName(getEndWorkflowStateName());
			historySearchForm.setOrderBy("id:desc");
			List<WorkflowHistory> historyList = getWorkflowHistoryService().getWorkflowHistoryList(historySearchForm);
			if (!CollectionUtils.isEmpty(historyList)) {
				short transitionUserId = historyList.get(0).getCreateUserId();
				return getSecurityUserService().getSecurityUser(transitionUserId);
			}
		}

		return null;
	}


	private SecurityUser getWorkflowTaskApproverForClient(int clientId, boolean includeRelatedClients) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(true);
		if (includeRelatedClients) {
			searchForm.setClientIdOrRelatedClient(clientId);
		}
		else {
			searchForm.setClientId(clientId);
		}
		List<InvestmentAccount> clientAccountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);
		// Sort Accounts in Workflow State Ascending Order so Active Accounts are Checked First, then Pending, Open, etc. Terminated accounts would be last and shouldn't get to them
		clientAccountList = BeanUtils.sortWithFunction(clientAccountList, clientAccount -> clientAccount.getWorkflowState().getOrder(), true);
		SecurityUser securityUser = null;
		for (InvestmentAccount clientAccount : CollectionUtils.getIterable(clientAccountList)) {
			securityUser = getWorkflowTaskApproverForClientAccount(clientAccount.getId());
			if (securityUser != null) {
				break;
			}
		}
		return securityUser;
	}


	private SecurityUser getWorkflowTaskApproverForManagerAccount(int managerAccountId) {
		SecurityUser securityUser = null;
		List<InvestmentManagerAccountAssignment> assignmentList = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentListByManager(managerAccountId, true);
		if (!CollectionUtils.isEmpty(assignmentList)) {
			InvestmentAccount[] accountList = BeanUtils.getPropertyValuesUniqueExcludeNull(assignmentList, InvestmentManagerAccountAssignment::getReferenceTwo, InvestmentAccount.class);
			for (InvestmentAccount clientAccount : accountList) {
				securityUser = getWorkflowTaskApproverForClientAccount(clientAccount.getId());
				if (securityUser != null) {
					break;
				}
			}
		}

		// If security user is null, then try to find based on the client of the manager account
		if (securityUser == null) {
			InvestmentManagerAccount managerAccount = getInvestmentManagerAccountService().getInvestmentManagerAccount(managerAccountId);
			// Allow related clients because managers can be at the client group level, but the accounts are at the children level
			return getWorkflowTaskApproverForClient(managerAccount.getClient().getId(), true);
		}

		return securityUser;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}


	public String getClientAccountIdPropertyPath() {
		return this.clientAccountIdPropertyPath;
	}


	public void setClientAccountIdPropertyPath(String clientAccountIdPropertyPath) {
		this.clientAccountIdPropertyPath = clientAccountIdPropertyPath;
	}


	public String getManagerAccountIdPropertyPath() {
		return this.managerAccountIdPropertyPath;
	}


	public void setManagerAccountIdPropertyPath(String managerAccountIdPropertyPath) {
		this.managerAccountIdPropertyPath = managerAccountIdPropertyPath;
	}


	public String getClientIdPropertyPath() {
		return this.clientIdPropertyPath;
	}


	public void setClientIdPropertyPath(String clientIdPropertyPath) {
		this.clientIdPropertyPath = clientIdPropertyPath;
	}


	public boolean isClientIdIncludeRelated() {
		return this.clientIdIncludeRelated;
	}


	public void setClientIdIncludeRelated(boolean clientIdIncludeRelated) {
		this.clientIdIncludeRelated = clientIdIncludeRelated;
	}


	public Boolean getMainRun() {
		return this.mainRun;
	}


	public void setMainRun(Boolean mainRun) {
		this.mainRun = mainRun;
	}


	public int getDaysBackFromToday() {
		return this.daysBackFromToday;
	}


	public void setDaysBackFromToday(int daysBackFromToday) {
		this.daysBackFromToday = daysBackFromToday;
	}


	public String getEndWorkflowStateName() {
		return this.endWorkflowStateName;
	}


	public void setEndWorkflowStateName(String endWorkflowStateName) {
		this.endWorkflowStateName = endWorkflowStateName;
	}
}
