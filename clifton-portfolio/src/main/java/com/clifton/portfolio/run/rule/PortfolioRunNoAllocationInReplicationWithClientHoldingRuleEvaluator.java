package com.clifton.portfolio.run.rule;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.portfolio.replication.aggregate.PortfolioAggregateRunReplicationHandler;
import com.clifton.portfolio.replication.aggregate.PortfolioAggregateSecurityReplication;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Rule evaluator to determine if there is a replication with a 0% allocation on a run, but the client account holds a non-zero position for the replication security.
 * Also flags if the client has a holding and there is no corresponding replication on the run.
 * <p>
 * Optionally can aggregate by security - i.e. evaluate all replications for a given security together rather than individually.
 *
 * @author mitchellf
 */
public class PortfolioRunNoAllocationInReplicationWithClientHoldingRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private PortfolioAggregateRunReplicationHandler<BasePortfolioRunReplication> portfolioAggregateRunReplicationHandler;

	private boolean aggregateBySecurity;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();

		List<BasePortfolioRunReplication> replicationList = context.getReplicationList(run);

		if (!CollectionUtils.isEmpty(replicationList)) {

			List<PortfolioAggregateSecurityReplication<BasePortfolioRunReplication>> aggregateReplications = getPortfolioAggregateRunReplicationHandler().getPortfolioAggregateRunReplication(replicationList);

			if (!isAggregateBySecurity()) {
				for (BasePortfolioRunReplication runReplication : CollectionUtils.getIterable(replicationList)) {
					if (!MathUtils.isEqual(runReplication.getActualContractsAdjusted(), BigDecimal.ZERO) && MathUtils.isEqual(runReplication.getTargetExposureAdjusted(), BigDecimal.ZERO)) {
						EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(runReplication));
						Map<String, Object> templateValues = new HashMap<>();
						StringBuilder violationNote = new StringBuilder();
						violationNote.append("Replication ")
								.append(runReplication.getLabel())
								.append(" is configured to have 0% allocation, but holds ")
								.append(CoreMathUtils.formatNumberMoney(MathUtils.round(runReplication.getActualContractsAdjusted(), 2)))
								.append(" contracts.");
						templateValues.put("violationNote", violationNote.toString());
						ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, BeanUtils.getIdentityAsLong(run), templateValues));
					}
				}
			}
			else {
				for (PortfolioAggregateSecurityReplication<BasePortfolioRunReplication> aggregateSecurityReplication : CollectionUtils.getIterable(aggregateReplications)) {
					if (!MathUtils.isEqual(aggregateSecurityReplication.getActualContracts(), BigDecimal.ZERO) && MathUtils.isEqual(aggregateSecurityReplication.getTargetExposure(), BigDecimal.ZERO)) {
						EntityConfig entityConfig = ruleConfig.getEntityConfig(MathUtils.getNumberAsLong(aggregateSecurityReplication.getId()));
						Map<String, Object> templateValues = new HashMap<>();
						StringBuilder violationNote = new StringBuilder();
						violationNote.append("Exposure to security ")
								.append(aggregateSecurityReplication.getSecurity().getSymbol())
								.append(" is configured to have 0% allocation, but holds ")
								.append(CoreMathUtils.formatNumberMoney(MathUtils.round(aggregateSecurityReplication.getActualContracts(), 2)))
								.append(" contracts.");
						templateValues.put("violationNote", violationNote.toString());
						ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, BeanUtils.getIdentityAsLong(run), templateValues));
					}
				}
			}

			// Now, evaluate client positions and make sure that a replication allocation exists for all of them
			List<AccountingPositionDaily> positions = context.getAccountPositionDailyLiveList(run);

			Map<InvestmentSecurity, List<AccountingPositionDaily>> positionsMap = BeanUtils.getBeansMap(positions, AccountingPositionDaily::getInvestmentSecurity);

			Set<InvestmentSecurity> replicationSecurities = aggregateReplications.stream().map(PortfolioAggregateSecurityReplication::getSecurity).collect(Collectors.toSet());

			positionsMap.forEach((security, positionList) -> {

				BigDecimal remainingQuantity = CoreMathUtils.sumProperty(positionList, AccountingPositionDaily::getRemainingQuantity);

				if (!MathUtils.isEqual(remainingQuantity, BigDecimal.ZERO) && !CollectionUtils.contains(replicationSecurities, security)) {
					EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
					Map<String, Object> templateValues = new HashMap<>();
					StringBuilder violationNote = new StringBuilder();
					violationNote.append("Client holds positions on ")
							.append(security.getSymbol())
							.append(" but a replication does not exist for that security.");
					templateValues.put("violationNote", violationNote.toString());
					ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, BeanUtils.getIdentityAsLong(run), templateValues));
				}
			});
		}

		return ruleViolationList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isAggregateBySecurity() {
		return this.aggregateBySecurity;
	}


	public void setAggregateBySecurity(boolean aggregateBySecurity) {
		this.aggregateBySecurity = aggregateBySecurity;
	}


	public PortfolioAggregateRunReplicationHandler<BasePortfolioRunReplication> getPortfolioAggregateRunReplicationHandler() {
		return this.portfolioAggregateRunReplicationHandler;
	}


	public void setPortfolioAggregateRunReplicationHandler(PortfolioAggregateRunReplicationHandler<BasePortfolioRunReplication> portfolioAggregateRunReplicationHandler) {
		this.portfolioAggregateRunReplicationHandler = portfolioAggregateRunReplicationHandler;
	}
}
