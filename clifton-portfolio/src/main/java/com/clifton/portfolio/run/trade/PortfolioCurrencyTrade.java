package com.clifton.portfolio.run.trade;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioCurrencyOther;


/**
 * The <code>PortfolioCurrencyTrade</code> represents physical currency exposure and trades
 * for a specific currency and run.
 *
 * @author nickk
 */
@NonPersistentObject
public class PortfolioCurrencyTrade<T extends BasePortfolioRunReplication> extends PortfolioCurrencyOther {

	/**
	 * Currency Trade will be created from/associated with a replication rather than currency other
	 * when it's included in a replication.
	 */
	private T replication;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public T getReplication() {
		return this.replication;
	}


	public void setReplication(T replication) {
		this.replication = replication;
		if (replication != null) {
			setPortfolioRun(replication.getPortfolioRun());
			setCurrencySecurity(replication.getUnderlyingSecurity());
			setLocalAmount(replication.getCurrencyActualLocalAmount());
			setUnrealizedLocalAmount(replication.getCurrencyUnrealizedLocalAmount());
			setExchangeRate(replication.getCurrencyExchangeRate());
			if (replication.getTradeCurrencyExchangeRate() != null) {
				setCurrentExchangeRate(replication.getTradeCurrencyExchangeRate());
			}
			if (replication.getTradeCurrencyExchangeRateDate() != null) {
				setCurrentExchangeRateDate(replication.getTradeCurrencyExchangeRateDate());
			}
			setCurrentLocalAmount(replication.getCurrencyCurrentLocalAmount());
			setCurrentUnrealizedLocalAmount(replication.getCurrencyCurrentUnrealizedLocalAmount());

			setPendingLocalAmount(replication.getCurrencyPendingLocalAmount());
		}
	}
}
