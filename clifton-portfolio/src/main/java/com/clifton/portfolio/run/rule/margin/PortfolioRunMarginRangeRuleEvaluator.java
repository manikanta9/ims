package com.clifton.portfolio.run.rule.margin;

import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.margin.PortfolioRunMargin;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.core.util.MathUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Currently checks range for Variation Margin (note will need to make this more flexible - I believe there is an open item to add initial margin range check as well)
 *
 * @author manderson
 */
public class PortfolioRunMarginRangeRuleEvaluator extends BasePortfolioRunMarginRuleEvaluator {

	@Override
	public List<RuleViolation> evaluateRuleForPortfolioRunMargin(PortfolioRun run, PortfolioRunMargin margin, EntityConfig entityConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		// Skip Warning if MarginBasis is 0 = % of 0 is really Undefined, not zero, so 0% is inaccurate.
		// If the account value is also zero - for those cases there is another warning that picks up 0 values, so to keep this warning here would be redundant.
		if (!MathUtils.isNullOrZero(margin.getMarginBasis())) {
			Map<String, Object> templateValues = new HashMap<>();
			if (RuleEvaluatorUtils.isEntityConfigRangeViolated(margin.getMarginVariationValuePercent(), entityConfig, templateValues)) {
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), margin.getId(), null, templateValues));
			}
		}
		return ruleViolationList;
	}
}
