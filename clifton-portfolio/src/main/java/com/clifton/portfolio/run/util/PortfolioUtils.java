package com.clifton.portfolio.run.util;

import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.core.util.MathUtils;


/**
 * @author nickk
 */
public class PortfolioUtils {

	private PortfolioUtils() {
		// prevent instantiation
	}


	/**
	 * Returns true if all properties of the replication are zero: Target Amount, Target %, Actual Contracts, Virtual Contracts, Additional Exposure, Currency, etc.
	 * Used to filter out the replication from the run
	 */
	public static <T extends BasePortfolioRunReplication> boolean isPortfolioReplicationEmpty(T replication) {
		// Target %
		if (!MathUtils.isNullOrZero(replication.getAllocationWeightAdjusted())) {
			return false;
		}
		// Target Amount
		if (!MathUtils.isNullOrZero(replication.getTargetExposureAdjusted())) {
			return false;
		}
		// Actual or Virtual Contracts
		if (!MathUtils.isNullOrZero(replication.getActualContracts()) || !MathUtils.isNullOrZero(replication.getVirtualContracts())) {
			return false;
		}
		// Additional Exposure
		if (!MathUtils.isNullOrZero(replication.getAdditionalExposure()) || !MathUtils.isNullOrZero(replication.getAdditionalOverlayExposure())) {
			return false;
		}
		// Currency
		if (!MathUtils.isNullOrZeroWithScale(replication.getCurrencyActualBaseAmount(), 2) || !MathUtils.isNullOrZeroWithScale(replication.getCurrencyOtherBaseAmount(), 2) || !MathUtils.isNullOrZeroWithScale(replication.getCurrencyDenominationBaseAmount(), 2)) {
			return false;
		}
		return true;
	}
}
