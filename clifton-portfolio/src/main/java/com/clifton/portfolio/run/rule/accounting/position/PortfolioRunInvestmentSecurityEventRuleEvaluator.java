package com.clifton.portfolio.run.rule.accounting.position;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>PortfolioRunInvestmentSecurityEventRuleEvaluator</code> class can be used to generate violations on specified dates for security events
 * for positions held.
 * <p>
 * Current use case is to generate violations X days on or before ex date, or X days on or before event date for Cash Dividend Payment events
 *
 * @author manderson
 */
public class PortfolioRunInvestmentSecurityEventRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {


	private short securityEventTypeId;

	/**
	 * Would be ex date or event date (payment date)
	 */
	private String dateFieldToCompareTo;

	////////////////////////////////////////////////////////////////////////////////


	private InvestmentSecurityEventService investmentSecurityEventService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();

		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<AccountingPositionDaily> accountingPositionDailyLiveList = context.getAccountPositionDailyLiveList(run);

			// Get a List of Securities
			Integer[] securityIds = BeanUtils.getPropertyValuesUniqueExcludeNull(accountingPositionDailyLiveList, accountingPositionDaily -> accountingPositionDaily.getInvestmentSecurity() != null ? accountingPositionDaily.getInvestmentSecurity().getId() : null, Integer.class);
			if (securityIds != null && securityIds.length > 0) {
				InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
				searchForm.setTypeId(getSecurityEventTypeId());
				searchForm.setSecurityIds(securityIds);
				Date today = DateUtils.clearTime(new Date());

				BigDecimal daysBefore = entityConfig.getMinAmount() == null ? null : MathUtils.abs(entityConfig.getMinAmount());
				BigDecimal daysAfter = entityConfig.getMaxAmount() == null ? null : MathUtils.abs(entityConfig.getMaxAmount());

				// If no options are set, look only for date = today
				if (daysBefore == null && daysAfter == null) {
					searchForm.addSearchRestriction(new SearchRestriction(getDateFieldToCompareTo(), ComparisonConditions.EQUALS, today));
				}
				else if (daysBefore != null) {
					Date date = DateUtils.addDays(today, daysBefore.intValue());
					// Date = X Days After Today
					if (daysAfter == null) {
						searchForm.addSearchRestriction(new SearchRestriction(getDateFieldToCompareTo(), ComparisonConditions.EQUALS, date));
					}
					else {
						// Date Between X Days before Today and X Days After Today
						Date startDate = DateUtils.addDays(today, MathUtils.negate(daysAfter).intValue());
						searchForm.addSearchRestriction(new SearchRestriction(getDateFieldToCompareTo(), ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
						searchForm.addSearchRestriction(new SearchRestriction(getDateFieldToCompareTo(), ComparisonConditions.LESS_THAN_OR_EQUALS, date));
					}
				}
				else {
					// Date = X Days after Today
					Date startDate = DateUtils.addDays(today, MathUtils.negate(daysAfter).intValue());
					searchForm.addSearchRestriction(new SearchRestriction(getDateFieldToCompareTo(), ComparisonConditions.EQUALS, startDate));
				}

				List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
				for (InvestmentSecurityEvent event : CollectionUtils.getIterable(eventList)) {
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), event.getId()));
				}
			}
		}
		return ruleViolationList;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public short getSecurityEventTypeId() {
		return this.securityEventTypeId;
	}


	public void setSecurityEventTypeId(short securityEventTypeId) {
		this.securityEventTypeId = securityEventTypeId;
	}


	public String getDateFieldToCompareTo() {
		return this.dateFieldToCompareTo;
	}


	public void setDateFieldToCompareTo(String dateFieldToCompareTo) {
		this.dateFieldToCompareTo = dateFieldToCompareTo;
	}
}
