package com.clifton.portfolio.run.rule.manager.balance;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.portfolio.run.rule.manager.BasePortfolioRunManagerAccountRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>PortfolioRunManagerBalanceProcessingIncompleteRuleEvaluator</code> class is a code-based RuleEvaluator.
 * It generates violations during a PortfolioRun for manager account balances where processing is incomplete.
 *
 * @author sfahey
 */
public class PortfolioRunManagerBalanceProcessingIncompleteRuleEvaluator extends BasePortfolioRunManagerAccountRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		for (InvestmentManagerAccountBalance balance : CollectionUtils.getIterable(context.getInvestmentManagerAccountBalanceList(run))) {
			EntityConfig entityConfig = getEntityConfig(balance.getManagerAccount(), ruleConfig);
			if (entityConfig != null && !entityConfig.isExcluded()) {
				if (!RuleViolationUtil.isRuleViolationAwareReadyForUse(balance)) {
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), BeanUtils.getIdentityAsLong(balance.getManagerAccount()), BeanUtils.getIdentityAsLong(balance), null, null));
				}
			}
		}
		return ruleViolationList;
	}
}
