package com.clifton.portfolio.run;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portfolio.run.search.PortfolioRunSearchForm;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunCloseHistoricalJob</code> will "Close" all runs that
 * have been submitted to trading and have a balance date greater than x days back.
 * <p/>
 * Most cases, runs are automatically Closed when a newer run is submitted, however for MOC runs which
 * are not performed daily, or accounts that go on pause/terminate they could sit in a non-Closed state indefinitely.
 *
 * @author manderson
 */
public class PortfolioRunCloseHistoricalJob implements Task {

	private static final Integer DEFAULT_CLOSE_RUNS_BUSINESS_DAYS_BACK = 3;

	/**
	 * Uses default days back on the service method if null (currency 3 days back);
	 */
	private Integer businessDaysBack;

	private CalendarBusinessDayService calendarBusinessDayService;
	private PortfolioRunService portfolioRunService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Integer days = getBusinessDaysBack() != null ? getBusinessDaysBack() : DEFAULT_CLOSE_RUNS_BUSINESS_DAYS_BACK;

		if (days > 0) {
			days = days * -1;
		}
		Date endDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), days);

		PortfolioRunSearchForm searchForm = new PortfolioRunSearchForm();
		searchForm.setWorkflowStatusNames(new String[]{WorkflowStatus.STATUS_APPROVED, WorkflowStatus.STATUS_PENDING});
		searchForm.addSearchRestriction(new SearchRestriction("balanceDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));
		List<PortfolioRun> runList = getPortfolioRunService().getPortfolioRunList(searchForm);

		int successCount = 0, errorCount = 0;
		Status status = Status.ofMessage("Starting Processing");
		for (PortfolioRun run : CollectionUtils.getIterable(runList)) {
			status.setMessage("Run count = " + CollectionUtils.getSize(runList) + "; Update count = " + successCount + "; Error count = " + errorCount);
			try {
				getPortfolioRunService().closePortfolioRun(run);
				successCount++;
			}
			catch (ValidationException e) {
				errorCount++;
				status.addError(run + ": " + e.getMessage());
			}
		}

		status.setActionPerformed(successCount != 0);
		return status;
	}


	////////////////////////////////////////////////////////
	///////         Getter and Setter Methods        ///////
	////////////////////////////////////////////////////////


	public Integer getBusinessDaysBack() {
		return this.businessDaysBack;
	}


	public void setBusinessDaysBack(Integer businessDaysBack) {
		this.businessDaysBack = businessDaysBack;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}
}
