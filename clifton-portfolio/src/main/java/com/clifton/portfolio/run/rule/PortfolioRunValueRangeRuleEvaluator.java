package com.clifton.portfolio.run.rule;

import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRangeRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunValueRangeRuleEvaluator</code> provides a base implementation for comparing if a given
 * value is within the specified range and triggers a violation if it is not.
 *
 * @author stevenf
 */
public abstract class PortfolioRunValueRangeRuleEvaluator extends BaseRangeRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			BigDecimal rangeValue = getRangeValue(run);
			Map<String, Object> templateValues = new HashMap<>();
			if (RuleEvaluatorUtils.isEntityConfigRangeViolated(rangeValue, entityConfig, templateValues)) {
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, run.getId(), templateValues));
			}
		}
		return ruleViolationList;
	}


	public abstract BigDecimal getRangeValue(PortfolioRun run);
}
