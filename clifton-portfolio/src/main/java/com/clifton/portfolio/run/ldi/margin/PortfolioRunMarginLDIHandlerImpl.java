package com.clifton.portfolio.run.ldi.margin;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.portfolio.run.ldi.process.PortfolioRunLDIConfig;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalanceService;
import com.clifton.portfolio.run.margin.BasePortfolioRunMarginHandler;
import com.clifton.portfolio.run.margin.PortfolioRunMargin;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>PortfolioRunMarginLDIHandlerImpl</code> processes PortfolioRunMargin for Portfolio Runs/Accounts that use LDI Service Type.
 * LDI Margin uses position notional as the margin basis
 *
 * @author manderson
 */
@Component
public class PortfolioRunMarginLDIHandlerImpl extends BasePortfolioRunMarginHandler<PortfolioRunLDIConfig> {

	private AccountingPositionDailyService accountingPositionDailyService;
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	private PortfolioRunManagerAccountBalanceService<?> portfolioRunManagerAccountBalanceService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortfolioRunMargin> generatePortfolioRunMarginList(PortfolioRunLDIConfig runConfig) {
		List<PortfolioRunMargin> marginList = new ArrayList<>();

		List<InvestmentAccountRelationship> tradingFuturesAccountRelationshipList = getTradingFuturesAccountRelationshipList(runConfig);
		if (!CollectionUtils.isEmpty(tradingFuturesAccountRelationshipList)) {
			BigDecimal ourAccountValue = getOurAccountValue(runConfig);

			List<Integer> brokerAccountIds = new ArrayList<>();
			for (InvestmentAccountRelationship brokerAccountRelationship : CollectionUtils.getIterable(tradingFuturesAccountRelationshipList)) {
				InvestmentAccount brokerAccount = brokerAccountRelationship.getReferenceTwo();
				if (brokerAccountIds.contains(brokerAccount.getId())) {
					continue;
				}
				brokerAccountIds.add(brokerAccount.getId());

				PortfolioRunMargin brokerMargin = populateMarginForBroker(runConfig, brokerAccount);

				// Get positions and sum notional
				AccountingPositionDailyLiveSearchForm liveSearchForm = new AccountingPositionDailyLiveSearchForm();
				liveSearchForm.setClientAccountId(runConfig.getClientInvestmentAccountId());
				liveSearchForm.setHoldingAccountId(brokerAccount.getId());
				liveSearchForm.setSnapshotDate(runConfig.getBalanceDate());
				List<AccountingPositionDaily> positionList = getAccountingPositionDailyService().getAccountingPositionDailyLiveList(liveSearchForm);
				BigDecimal totalNotional = CoreMathUtils.sumProperty(positionList, AccountingPositionDaily::getNotionalValueBase);
				brokerMargin.setMarginBasis(totalNotional);

				// If not set - Recalc
				if (MathUtils.isNullOrZero(brokerMargin.getMarginInitialValue())) {
					BigDecimal requiredInitial = BigDecimal.ZERO;
					boolean groupedMarginAccount = BooleanUtils.isTrue((Boolean) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(brokerMargin.getPortfolioRun().getClientInvestmentAccount(), InvestmentAccount.GROUPED_MARGIN_ACCOUNT));

					if (!groupedMarginAccount) {
						for (AccountingPositionDaily pos : CollectionUtils.getIterable(positionList)) {
							// Only applies to positions with Actual Contracts != 0 AND Futures - Calculator checks these
							BigDecimal instrumentMargin = InvestmentCalculatorUtils.calculateRequiredCollateral(pos.getAccountingTransaction().getInvestmentSecurity(), brokerAccount,
									pos.getRemainingQuantity(), pos.getMarketFxRate(), true);
							requiredInitial = MathUtils.add(requiredInitial, instrumentMargin);
						}
					}
					brokerMargin.setMarginInitialValue(requiredInitial);
				}
				brokerMargin.setMarginRecommendedValue(MathUtils.getPercentageOf(runConfig.getMarginRecommendedPercent(), brokerMargin.getMarginBasis(), true));

				brokerMargin.setOurAccountValue(ourAccountValue);
				marginList.add(brokerMargin);
			}
		}
		return marginList;
	}


	private BigDecimal getOurAccountValue(PortfolioRunLDIConfig runConfig) {
		List<InvestmentManagerAccountAssignment> assignmentList = runConfig.getManagerAssignmentList();
		assignmentList = BeanUtils.filter(assignmentList, InvestmentManagerAccountAssignment::getCashType, ManagerCashTypes.MARGIN);

		if (CollectionUtils.isEmpty(assignmentList)) {
			throw new ValidationExceptionWithCause("InvestmentAccount", runConfig.getClientInvestmentAccountId(),
					"Unable to perform margin analysis. There are no managers with cash type Margin selected for this account.");
		}

		BigDecimal ourAccountValue = BigDecimal.ZERO;
		for (InvestmentManagerAccountAssignment assignment : assignmentList) {
			InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(assignment.getReferenceOne().getId(),
					runConfig.getBalanceDate(), false);
			// Previous warnings should catch this - but check anyway
			if (balance == null) {
				throw new ValidationExceptionWithCause("InvestmentManagerAccount", assignment.getReferenceOne().getId(), "Unable to perform margin analysis. Missing manager balance for manager ["
						+ assignment.getManagerLabel() + "] on [" + DateUtils.fromDateShort(runConfig.getBalanceDate()) + "].");
			}
			ourAccountValue = MathUtils.add(ourAccountValue, getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, balance.getManagerAccount(), balance.getBalanceDate(), (runConfig.getRun().isMarketOnCloseAdjustmentsIncluded() ? balance.getMarketOnCloseCashValue() : balance.getAdjustedCashValue())));
		}
		return ourAccountValue;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public PortfolioRunManagerAccountBalanceService<?> getPortfolioRunManagerAccountBalanceService() {
		return this.portfolioRunManagerAccountBalanceService;
	}


	public void setPortfolioRunManagerAccountBalanceService(PortfolioRunManagerAccountBalanceService<?> portfolioRunManagerAccountBalanceService) {
		this.portfolioRunManagerAccountBalanceService = portfolioRunManagerAccountBalanceService;
	}
}
