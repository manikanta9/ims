package com.clifton.portfolio.run.ldi;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationPoint;
import com.clifton.portfolio.run.PortfolioRun;

import java.math.BigDecimal;


/**
 * The <code>PortfolioRunLDIManagerAccountKeyRateDurationPoint</code> contains calculated KRD and DV01 values
 * for each {@link InvestmentManagerAccountAssignment} and {@link PortfolioAccountKeyRateDurationPoint} for
 * a {@link PortfolioRun}
 * <p/>
 * Phase I of LDI KRD Processing
 *
 * @author manderson
 */
public class PortfolioRunLDIManagerAccountKeyRateDurationPoint extends BaseSimpleEntity<Integer> implements LabeledObject {

	private PortfolioRunLDIManagerAccount managerAccount;

	private PortfolioAccountKeyRateDurationPoint keyRateDurationPoint;

	private BigDecimal krdValue;

	private BigDecimal dv01Value;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder(16);
		if (getManagerAccount() != null) {
			sb.append(getManagerAccount().getLabel());
		}
		sb.append(" - ");
		if (getKeyRateDurationPoint() != null) {
			sb.append(getKeyRateDurationPoint().getMarketDataField().getName());
		}
		return sb.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioAccountKeyRateDurationPoint getKeyRateDurationPoint() {
		return this.keyRateDurationPoint;
	}


	public void setKeyRateDurationPoint(PortfolioAccountKeyRateDurationPoint keyRateDurationPoint) {
		this.keyRateDurationPoint = keyRateDurationPoint;
	}


	public BigDecimal getKrdValue() {
		return this.krdValue;
	}


	public void setKrdValue(BigDecimal krdValue) {
		this.krdValue = krdValue;
	}


	public BigDecimal getDv01Value() {
		return this.dv01Value;
	}


	public void setDv01Value(BigDecimal dv01Value) {
		this.dv01Value = dv01Value;
	}


	public PortfolioRunLDIManagerAccount getManagerAccount() {
		return this.managerAccount;
	}


	public void setManagerAccount(PortfolioRunLDIManagerAccount managerAccount) {
		this.managerAccount = managerAccount;
	}
}
