package com.clifton.portfolio.run.rule;

import com.clifton.portfolio.run.PortfolioRun;

import java.math.BigDecimal;


/**
 * This class can be used to specify the minimum and maximum percentage of cash in comparison to the total portfolio value.
 * By default the rule is excluded and has a min and max value of 0 and .1 respectively.  This can be overridden at the account level.
 *
 * @author stevenf
 */
public class PortfolioRunCashExposureRangeRuleEvaluator extends PortfolioRunValueRangeRuleEvaluator {

	//System Bean Property values are 'CashExposureAmount' and 'CaseExposurePercentage'
	private static final String CASH_EXPOSURE_AMOUNT = "CashExposureAmount";


	@Override
	public BigDecimal getRangeValue(PortfolioRun run) {
		return getRangeType().equals(CASH_EXPOSURE_AMOUNT) ? run.getCashExposure() : run.getCashExposurePercent();
	}
}
