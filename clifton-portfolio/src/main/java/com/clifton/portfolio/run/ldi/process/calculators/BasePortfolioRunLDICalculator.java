package com.clifton.portfolio.run.ldi.process.calculators;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.portfolio.account.PortfolioAccountContractStore.PortfolioAccountContractSecurityStore;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationSecurity;
import com.clifton.portfolio.account.ldi.PortfolioAccountLDIService;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroup;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistorySummary;
import com.clifton.portfolio.cashflow.ldi.history.rebuild.PortfolioCashFlowHistoryRebuildService;
import com.clifton.portfolio.replication.PortfolioReplicationAllocationSecurityConfig;
import com.clifton.portfolio.replication.PortfolioReplicationHandler;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIService;
import com.clifton.portfolio.run.ldi.process.PortfolioRunLDIConfig;
import com.clifton.portfolio.run.process.calculator.BasePortfolioRunProcessStepCalculatorImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BasePortfolioRunLDICalculator</code> contains common methods used for various LDI run processing steps
 *
 * @author manderson
 */
public abstract class BasePortfolioRunLDICalculator extends BasePortfolioRunProcessStepCalculatorImpl<PortfolioRunLDIConfig> {

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentCalculator investmentCalculator;
	private InvestmentInstrumentService investmentInstrumentService;

	private PortfolioAccountLDIService portfolioAccountLDIService;
	private PortfolioRunLDIService portfolioRunLDIService;

	private PortfolioReplicationHandler portfolioReplicationHandler;

	private PortfolioCashFlowHistoryRebuildService portfolioCashFlowHistoryRebuildService;

	public static final String CUSTOM_COLUMN_LDI_REPLICATION = "Replication";


	////////////////////////////////////////////////////////////////////////////
	////////            Manager Assignment Helper Methods               ////////
	////////////////////////////////////////////////////////////////////////////


	protected List<InvestmentManagerAccountAssignment> getManagerAssignmentList(PortfolioRunLDIConfig runConfig) {
		List<InvestmentManagerAccountAssignment> managerAssignmentList = runConfig.getManagerAssignmentList();
		if (managerAssignmentList == null) {
			managerAssignmentList = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentListByAccount(runConfig.getClientInvestmentAccountId(), true);
		}
		runConfig.setManagerAssignmentList(managerAssignmentList);
		getCashFlowHistorySummaryListMap(runConfig);
		return managerAssignmentList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////       Cash Flow Group History Summary Helper Methods       ////////
	////////////////////////////////////////////////////////////////////////////


	protected Map<PortfolioCashFlowGroup, List<PortfolioCashFlowGroupHistorySummary>> getCashFlowHistorySummaryListMap(PortfolioRunLDIConfig runConfig) {
		Map<PortfolioCashFlowGroup, List<PortfolioCashFlowGroupHistorySummary>> cashFlowGroupSummaryListMap = runConfig.getCashFlowGroupSummaryListMap();
		if (cashFlowGroupSummaryListMap == null && runConfig.getClientInvestmentAccount() != null) {
			Boolean doNotProcessCashFlowHistory = (Boolean) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(runConfig.getClientInvestmentAccount(), InvestmentAccount.LDI_CASH_FLOW_DO_NOT_PROCESS);
			if (!BooleanUtils.isTrue(doNotProcessCashFlowHistory)) {
				cashFlowGroupSummaryListMap = getPortfolioCashFlowHistoryRebuildService().getOrBuildPortfolioCashFlowGroupHistorySummaryListMapForClientAccount(runConfig.getClientInvestmentAccountId(), runConfig.getBalanceDate());
				runConfig.setCashFlowGroupSummaryListMap(cashFlowGroupSummaryListMap);
			}
		}
		return cashFlowGroupSummaryListMap;
	}


	////////////////////////////////////////////////////////////////////////////
	////////               Replication Helper Methods                   ////////
	////////////////////////////////////////////////////////////////////////////


	protected List<PortfolioReplicationAllocationSecurityConfig> getReplicationAllocationList(PortfolioRunLDIConfig runConfig) {
		List<PortfolioReplicationAllocationSecurityConfig> replicationAllocationList = runConfig.getReplicationAllocationList();
		if (replicationAllocationList == null) {

			Integer replicationId = (Integer) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(runConfig.getRun().getClientInvestmentAccount(), CUSTOM_COLUMN_LDI_REPLICATION);
			if (replicationId == null) {
				throw new ValidationException("Please select a replication under the Portfolio Setup to determine which securities to include");
			}
			replicationAllocationList = getPortfolioReplicationHandler().getPortfolioReplicationAllocationSecurityConfigListForBalanceDate(replicationId, runConfig.getBalanceDate(),
					getSecurityHeldList(runConfig));
			runConfig.setReplicationAllocationList(replicationAllocationList);
		}
		return replicationAllocationList;
	}


	private List<InvestmentSecurity> getSecurityHeldList(PortfolioRunLDIConfig runConfig) {
		List<Integer> securityIdList = new ArrayList<>();
		for (PortfolioAccountContractSecurityStore securityStore : CollectionUtils.getIterable(runConfig.getContractStore().getSecurityStoreCollection())) {
			if (!securityIdList.contains(securityStore.getSecurityId())) {
				securityIdList.add(securityStore.getSecurityId());
			}
		}
		return getInvestmentInstrumentService().getInvestmentSecurityListByIds(securityIdList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                KRD Value Helper Methods                    ////////
	////////////////////////////////////////////////////////////////////////////


	protected PortfolioAccountKeyRateDurationSecurity getModifiedKRDResultForSecurity(InvestmentSecurity security, PortfolioRunLDIConfig runConfig) {
		PortfolioAccountKeyRateDurationSecurity result = runConfig.getModifiedKRDValues(security.getId());
		if (result == null) {
			loadModifiedKRDResults(runConfig);
			result = runConfig.getModifiedKRDValues(security.getId());
		}
		if (result == null) {
			throw new ValidationException("Unable to determine KRD Values to use for Security [" + security.getLabel() + "].");
		}
		return result;
	}


	protected PortfolioAccountKeyRateDurationSecurity getReportableOnlyModifiedKRDResultForSecurity(InvestmentSecurity security, PortfolioRunLDIConfig runConfig) {
		PortfolioAccountKeyRateDurationSecurity result = runConfig.getReportableOnlyModifiedKRDValues(security.getId());
		if (result == null && !CollectionUtils.isEmpty(runConfig.getReportableOnlyKeyRateDurationPointList())) {
			loadModifiedKRDResults(runConfig);
			result = runConfig.getReportableOnlyModifiedKRDValues(security.getId());
		}
		return result;
	}


	/**
	 * When loading one result, we load all - this way if we enforce that all dates are the same we can validate/check that one time
	 */
	private void loadModifiedKRDResults(PortfolioRunLDIConfig runConfig) {
		// Determine List of Securities we Need to Load
		List<InvestmentSecurity> securityList = new ArrayList<>();

		// Manager Assignment Benchmarks
		List<InvestmentManagerAccountAssignment> managerAssignmentList = getManagerAssignmentList(runConfig);
		for (InvestmentManagerAccountAssignment assignment : CollectionUtils.getIterable(managerAssignmentList)) {
			// Skip Margin Manager
			if (ManagerCashTypes.MARGIN == assignment.getCashType()) {
				continue;
			}
			if (assignment.getBenchmarkSecurity() == null) {
				throw new ValidationException("Missing Benchmark Security selection for Manager [" + assignment.getManagerAccountLabel() + "].");
			}
			if (!securityList.contains(assignment.getBenchmarkSecurity())) {
				securityList.add(assignment.getBenchmarkSecurity());
			}
		}

		// Position Exposure
		List<PortfolioReplicationAllocationSecurityConfig> replicationAllocationList = getReplicationAllocationList(runConfig);
		for (PortfolioReplicationAllocationSecurityConfig allocationSecurityConfig : CollectionUtils.getIterable(replicationAllocationList)) {
			boolean currentHeld = false;
			for (InvestmentSecurity security : CollectionUtils.getIterable(allocationSecurityConfig.getSecurityList())) {
				if (!securityList.contains(security)) {
					securityList.add(security);
				}
				if (allocationSecurityConfig.getCurrentSecurity().equals(security)) {
					currentHeld = true;
				}
			}
			if (!currentHeld && (allocationSecurityConfig.isAlwaysIncludeCurrentSecurity() || CollectionUtils.isEmpty(allocationSecurityConfig.getSecurityList()))) {
				if (!securityList.contains(allocationSecurityConfig.getCurrentSecurity())) {
					securityList.add(allocationSecurityConfig.getCurrentSecurity());
				}
			}
		}

		boolean enforceSameDate = runConfig.getDoNotEnforceSameKRDDates() == null || !runConfig.getDoNotEnforceSameKRDDates();
		loadModifiedKRDResultsForSecurityList(securityList, enforceSameDate, null, runConfig, false);
		if (!CollectionUtils.isEmpty(runConfig.getReportableOnlyKeyRateDurationPointList())) {
			loadModifiedKRDResultsForSecurityList(securityList, enforceSameDate, null, runConfig, true);
		}
	}


	private void loadModifiedKRDResultsForSecurityList(List<InvestmentSecurity> securityList, boolean enforceSameDate, Date valueDate, PortfolioRunLDIConfig runConfig, boolean reportableOnly) {
		for (InvestmentSecurity security : CollectionUtils.getIterable(securityList)) {
			PortfolioAccountKeyRateDurationSecurity result = loadModifiedKRDResultForSecurity(security, valueDate, runConfig, reportableOnly);
			if (enforceSameDate) {
				if (valueDate == null) {
					valueDate = result.getMaxValueDate(); // Use the latest possible date
				}
				else {
					// If this result valueDate is earlier - need to recalculate previous using earlier date unless they are the same as far as "business days"
					if (DateUtils.compare(result.getValueDate(), valueDate, false) < 0) {
						Calendar calendar = getInvestmentCalculator().getInvestmentSecurityCalendar(security);
						if (getCalendarBusinessDayService().getBusinessDaysBetween(result.getValueDate(), valueDate, null, calendar.getId()) != 0) {
							// Use Get Next Business Day - 1 Day so we check the latest possible date
							loadModifiedKRDResultsForSecurityList(securityList, enforceSameDate, DateUtils.addDays(getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(result.getValueDate(), calendar.getId())), -1), runConfig, reportableOnly);
							// Need to reload the list using earlier date - break out of this loop
							break;
						}
					}
				}
			}
		}
	}


	/**
	 * Returns a {@link PortfolioAccountKeyRateDurationSecurity} containing the map of KRD Points the account uses with the modified/calculated KRD Point value.  Used for both Manager KRD Point calculations
	 * and Exposure KRD Point Calculations
	 */
	private PortfolioAccountKeyRateDurationSecurity loadModifiedKRDResultForSecurity(InvestmentSecurity security, Date valueDate, PortfolioRunLDIConfig runConfig, boolean reportableOnly) {
		PortfolioAccountKeyRateDurationSecurity result;
		if (reportableOnly) {
			result = getPortfolioAccountLDIService().calculatePortfolioAccountKeyRateDurationForSecurity(runConfig.getReportableOnlyKeyRateDurationPointList(), security,
					valueDate, runConfig.getBalanceDate(), runConfig.getUseMonthEndValues());
			runConfig.setReportableOnlyModifiedKRDMap(security.getId(), result);
		}
		else {
			result = getPortfolioAccountLDIService().calculatePortfolioAccountKeyRateDurationForSecurity(runConfig.getKeyRateDurationPointList(), security,
					valueDate, runConfig.getBalanceDate(), runConfig.getUseMonthEndValues());
			runConfig.setModifiedKRDMap(security.getId(), result);
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public PortfolioAccountLDIService getPortfolioAccountLDIService() {
		return this.portfolioAccountLDIService;
	}


	public void setPortfolioAccountLDIService(PortfolioAccountLDIService portfolioAccountLDIService) {
		this.portfolioAccountLDIService = portfolioAccountLDIService;
	}


	public PortfolioRunLDIService getPortfolioRunLDIService() {
		return this.portfolioRunLDIService;
	}


	public void setPortfolioRunLDIService(PortfolioRunLDIService portfolioRunLDIService) {
		this.portfolioRunLDIService = portfolioRunLDIService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public PortfolioReplicationHandler getPortfolioReplicationHandler() {
		return this.portfolioReplicationHandler;
	}


	public void setPortfolioReplicationHandler(PortfolioReplicationHandler portfolioReplicationHandler) {
		this.portfolioReplicationHandler = portfolioReplicationHandler;
	}


	public PortfolioCashFlowHistoryRebuildService getPortfolioCashFlowHistoryRebuildService() {
		return this.portfolioCashFlowHistoryRebuildService;
	}


	public void setPortfolioCashFlowHistoryRebuildService(PortfolioCashFlowHistoryRebuildService portfolioCashFlowHistoryRebuildService) {
		this.portfolioCashFlowHistoryRebuildService = portfolioCashFlowHistoryRebuildService;
	}
}
