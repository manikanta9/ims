package com.clifton.portfolio.run.process;


import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.margin.PortfolioRunMarginService;
import com.clifton.portfolio.run.report.PortfolioRunReportService;
import com.clifton.portfolio.run.search.PortfolioRunSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>PortfolioRunProcessServiceImpl</code> is the generic service that is exposed to process runs of all types.  It is aware of mapping defined for specific service types to call those processors for each run.
 *
 * @author manderson
 */
@Service
public class PortfolioRunProcessServiceImpl implements PortfolioRunProcessService {

	private InvestmentAccountService investmentAccountService;

	private PortfolioRunMarginService portfolioRunMarginService;
	private PortfolioRunReportService portfolioRunReportService;
	private PortfolioRunService portfolioRunService;
	private SystemBeanService systemBeanService;
	private SystemColumnValueService systemColumnValueService;

	private RunnerHandler runnerHandler;

	private static final String BUSINESS_SERVICE_PROCESSING_HANDLER_COLUMN = "Portfolio Run Processing Handler Bean";


	////////////////////////////////////////////////////////////////////////////
	/////////           Portfolio Run Process Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void deletePortfolioRun(int id) {
		deletePortfolioRunProcessing(id, true);
	}


	@Transactional
	protected void deletePortfolioRunProcessing(int id, boolean deleteRun) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(id);

		// Common Margin table used across different types of runs
		getPortfolioRunMarginService().deletePortfolioRunMarginListByRun(id);

		// Delete Related Processing in Many Side Tables that apply to this run
		PortfolioRunProcessHandler processHandler = getPortfolioRunProcessHandlerForRun(run);
		processHandler.deletePortfolioRunProcessing(id);

		if (deleteRun) {
			getPortfolioRunService().deletePortfolioRun(id);
		}
		else {
			// Reset fields set during processing
			run.setPortfolioTotalValue(null);
			run.setCashTotal(null);
			run.setCashExposure(null);
			run.setMispricingTotal(null);
			run.setOverlayTargetTotal(null);
			run.setOverlayExposureTotal(null);
			getPortfolioRunService().savePortfolioRun(run);
		}
	}


	@Override
	public String processPortfolioRunList(final PortfolioRunSearchForm searchForm, final boolean reload, final boolean reloadProxyManagers, boolean synchronous) {

		if (synchronous) {
			return doProcessPortfolioRunList(searchForm, reload, reloadProxyManagers, null);
		}

		// asynchronous run support
		String runId = searchForm.getInvestmentAccountGroupId() == null ? "A" + searchForm.getClientInvestmentAccountId() : "G" + searchForm.getInvestmentAccountGroupId();
		final Date now = new Date();
		Runner runner = new AbstractStatusAwareRunner("PORTFOLIO-RUN", runId, now, "Scheduled for " + DateUtils.fromDate(now)) {
			@Override
			public void run() {
				getStatus().setMessage(doProcessPortfolioRunList(searchForm, reload, reloadProxyManagers, getStatus()));
			}
		};
		getRunnerHandler().runNow(runner);
		return "Started processing requested runs. Processing will be completed shortly.";
	}


	private String doProcessPortfolioRunList(PortfolioRunSearchForm searchForm, boolean reload, boolean reloadProxyManagers, Status status) {
		ValidationUtils.assertTrue(searchForm.getClientInvestmentAccountId() != null || searchForm.getInvestmentAccountGroupId() != null, "An account or account group selection is required.");
		// If Only One Account
		List<InvestmentAccount> accountList;
		if (searchForm.getClientInvestmentAccountId() != null) {
			accountList = CollectionUtils.createList(getInvestmentAccountService().getInvestmentAccount(searchForm.getClientInvestmentAccountId()));
		}
		else {
			// Use the ProductOverlayRunSearchForm to get the list of accounts we want to run for
			InvestmentAccountSearchForm accountSearchForm = new InvestmentAccountSearchForm();
			accountSearchForm.setInvestmentAccountGroupId(searchForm.getInvestmentAccountGroupId());
			accountSearchForm.setTeamSecurityGroupId(searchForm.getTeamSecurityGroupId());
			accountSearchForm.setClientId(searchForm.getClientId());
			accountSearchForm.setOurAccount(true);
			accountSearchForm.setServiceProcessingTypes(new ServiceProcessingTypes[]{ServiceProcessingTypes.LDI, ServiceProcessingTypes.PORTFOLIO_RUNS, ServiceProcessingTypes.PORTFOLIO_TARGET});
			accountList = getInvestmentAccountService().getInvestmentAccountList(accountSearchForm);
		}
		if (CollectionUtils.isEmpty(accountList)) {
			return "No client accounts match search criteria.";
		}

		List<PortfolioRun> existingRunList = getPortfolioRunService().getPortfolioRunList(searchForm);

		int failedCount = 0;
		int successCount = 0;
		int skippedCount = 0;

		List<PortfolioRun> processRunList = new ArrayList<>();
		for (InvestmentAccount account : accountList) {
			List<PortfolioRun> accountExisting = BeanUtils.filter(existingRunList, PortfolioRun::getClientInvestmentAccount, account);
			if (!CollectionUtils.isEmpty(accountExisting)) {
				if (!reload || (CollectionUtils.getSize(accountExisting) > 1)) {
					skippedCount++;
				}
				else {
					processRunList.add(accountExisting.get(0));
				}
			}
			else {
				PortfolioRun run = new PortfolioRun();
				run.setClientInvestmentAccount(account);
				run.setBalanceDate(searchForm.getBalanceDate());
				run.setMarketOnCloseAdjustmentsIncluded(searchForm.getMarketOnCloseAdjustmentsIncluded() != null && searchForm.getMarketOnCloseAdjustmentsIncluded());
				run.setMainRun(!run.isMarketOnCloseAdjustmentsIncluded());
				getPortfolioRunService().savePortfolioRun(run);
				processRunList.add(run);
			}
		}
		for (PortfolioRun run : CollectionUtils.getIterable(processRunList)) {
			try {
				processPortfolioRun(run.getId(), reloadProxyManagers, null);
				successCount++;
			}
			catch (Exception e) {
				failedCount++;
				if (status != null) {
					status.addError(ExceptionUtils.getDetailedMessage(e));
				}
			}
		}
		if (status != null) {
			status.setMessage("Total: " + accountList.size() + "; Success: " + successCount + "; Failed: " + failedCount + "; Skipped: " + skippedCount
					+ ". Please click on Run History tab for detailed results for each run.");
		}
		return "Processing Complete. Total: " + accountList.size() + "; Success: " + successCount + "; Failed: " + failedCount + "; Skipped: " + skippedCount
				+ ". Please click on Run History tab for detailed results for each run.";
	}


	/**
	 * Runs processing for PortfolioRun bean until a failure is found.
	 * Calls correct processor dependent on the service for the client account
	 * <p>
	 * If current step = COMPLETED will throw an exception since you can't rerun after trades are generated.
	 */
	@Override
	public void processPortfolioRun(int runId, boolean reloadProxyManagers, Integer currentDepth) {
		if (currentDepth == null) {
			currentDepth = 0;
		}
		if (currentDepth > 10) {
			throw new IllegalStateException("Possible infinite loop encountered.  Re-Processing runs hit a max depth of 10. Please check your set up that there aren't any circular dependencies.  Run ID: " + runId);
		}
		currentDepth++;
		getPortfolioRunReportService().clearPortfolioRunReportCache(runId);
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		PortfolioRunProcessHandler handler = getPortfolioRunProcessHandlerForRun(run);
		handler.processPortfolioRun(run, reloadProxyManagers, currentDepth);
	}


	////////////////////////////////////////////////////////////////
	//////       PortfolioRunProcessHandler Methods          ///////
	////////////////////////////////////////////////////////////////


	private PortfolioRunProcessHandler getPortfolioRunProcessHandlerForRun(PortfolioRun run) {
		getPortfolioRunService().validatePortfolioRunIsSupportedForClientAccount(run);

		// Business service processing type custom values appear not to be populated
		// Will have to retrieve values using value handler
		// intValue() call is needed because ServiceProcessingType has ID of type Short, but SystemColumnValueService requires int
		BusinessServiceProcessingType processingType = run.getClientInvestmentAccount().getServiceProcessingType();
		int processingTypeId = processingType.getId().intValue();

		Integer processHandlerBeanId = (Integer) getSystemColumnValueService().getSystemColumnCustomValue(processingTypeId, BusinessServiceProcessingType.BUSINESS_SERVICE_PROCESSING_TYPE_RUN_COLUMN_GROUP_NAME, BUSINESS_SERVICE_PROCESSING_HANDLER_COLUMN);
		ValidationUtils.assertNotNull(processHandlerBeanId, "No Processing Handler Bean ID is configured for processing type [" + processingType + "].");

		SystemBean processHandlerBean = getSystemBeanService().getSystemBean(processHandlerBeanId);
		ValidationUtils.assertNotNull(processHandlerBean, "Could not find Processing Handler Bean with ID " + processHandlerBeanId + ". Processing Type: [" + processingType + "].");

		return (PortfolioRunProcessHandler) getSystemBeanService().getBeanInstance(processHandlerBean);
	}


	///////////////////////////////////////////////////////
	///////        Getter & Setter Methods         ////////
	///////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public PortfolioRunReportService getPortfolioRunReportService() {
		return this.portfolioRunReportService;
	}


	public void setPortfolioRunReportService(PortfolioRunReportService portfolioRunReportService) {
		this.portfolioRunReportService = portfolioRunReportService;
	}


	public PortfolioRunMarginService getPortfolioRunMarginService() {
		return this.portfolioRunMarginService;
	}


	public void setPortfolioRunMarginService(PortfolioRunMarginService portfolioRunMarginService) {
		this.portfolioRunMarginService = portfolioRunMarginService;
	}
}
