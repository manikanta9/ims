package com.clifton.portfolio.run.ldi.trade;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposureKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIService;
import com.clifton.portfolio.run.ldi.calculators.PortfolioRunLDITargetContractCalculator;
import com.clifton.portfolio.run.trade.BasePortfolioRunTradeHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunTradeLDIHandlerImpl</code> ...
 *
 * @author manderson
 */
@Component
public class PortfolioRunTradeLDIHandlerImpl extends BasePortfolioRunTradeHandler<PortfolioRunTradeLDI, PortfolioRunLDIExposure> {

	private PortfolioRunLDIService portfolioRunLDIService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRunTradeLDI getPortfolioRunTradeImpl(PortfolioRun run) {
		return getPortfolioRunForTrading(run, PortfolioRunTradeLDI.class);
	}


	@Override
	public PortfolioRunTradeLDI getPortfolioRunTradePrices(PortfolioRunTradeLDI run, boolean livePrices, boolean reloadDetails) {
		PortfolioRunTradeLDI returnRun = getPortfolioRunForTrading(run.getId(), PortfolioRunTradeLDI.class);
		Map<String, List<InvestmentAccountRelationship>> runHoldingAccountRelationshipMap = new HashMap<>();
		if (reloadDetails) {
			// Don't want to lose data already entered on screen so just need to add what is missing
			List<PortfolioRunLDIExposure> detailList = new ArrayList<>();
			for (PortfolioRunLDIExposure dbDetail : CollectionUtils.getIterable(getPortfolioRunTradeDetailListForRun(run.getId()))) {
				boolean found = false;
				for (PortfolioRunLDIExposure uiDetail : CollectionUtils.getIterable(run.getDetailList())) {
					if (MathUtils.isEqual(dbDetail.getId(), uiDetail.getId())) {
						found = true;
						// Copy changes in overrides if necessary
						copySecurityTradeInfo(uiDetail, dbDetail);

						uiDetail.setKeyRateDurationPointList(dbDetail.getKeyRateDurationPointList());

						// Point to detail object as exposure object in the krd point list so picks up trade info for calculations
						for (PortfolioRunLDIExposureKeyRateDurationPoint point : CollectionUtils.getIterable(uiDetail.getKeyRateDurationPointList())) {
							point.setExposure(uiDetail);
						}
						detailList.add(uiDetail);
						break;
					}
				}
				if (!found) {
					// Set up Trading Defaults for new rows
					populatePortfolioRunTradeDetailDefaults(dbDetail, runHoldingAccountRelationshipMap);
					detailList.add(dbDetail);
				}
			}
			returnRun.setDetailList(detailList);
		}
		else {
			returnRun.setDetailList(run.getDetailList());
		}

		// Reset Prices
		refreshPortfolioRunTradeDetailListPrices(returnRun, livePrices);

		// Reset Current/Pending in case price changes change splitting
		populatePortfolioRunTradeDetailPendingAndCurrentContracts(returnRun);

		return returnRun;
	}


	@Override
	public PortfolioRunTradeLDI getPortfolioRunTradeAmounts(PortfolioRunTradeLDI run) {
		PortfolioRunTradeLDI returnRun = getPortfolioRunForTrading(run.getId(), PortfolioRunTradeLDI.class);

		// Get a clean list and then copy of price info that was on screen
		populatePortfolioRunTradeDetailListWithTrades(returnRun, false);

		for (PortfolioRunLDIExposure exp : CollectionUtils.getIterable(returnRun.getDetailList())) {
			for (PortfolioRunLDIExposure exp2 : CollectionUtils.getIterable(run.getDetailList())) {
				if (exp.equals(exp2)) {
					copySecurityTradeInfo(exp, exp2);
				}
			}
		}

		// Set Current/Pending & Process Splitting
		populatePortfolioRunTradeDetailPendingAndCurrentContracts(returnRun);

		return returnRun;
	}


	@Override
	public void refreshPortfolioRunTradeDetailListPrices(PortfolioRunTradeLDI run, boolean livePrices) {

		for (PortfolioRunLDIExposure exposure : CollectionUtils.getIterable(run.getDetailList())) {
			if (livePrices) {
				MarketDataLive livePrice = getMarketDataRetriever().getLivePrice(exposure.getSecurity());
				if (livePrice != null) {
					exposure.setTradeSecurityPrice(livePrice.asNumericValue());
					exposure.setTradeSecurityPriceDate(livePrice.getMeasureDate());
				}
			}
			else {
				exposure.setTradeSecurityPrice(null);
				exposure.setTradeSecurityPriceDate(null);
			}
		}

		populatePortfolioRunTradeDetailTargetContracts(run);
	}


	@Override
	public PortfolioRunLDIExposure getPortfolioRunTradeDetail(int detailId) {
		return getPortfolioRunLDIService().getPortfolioRunLDIExposure(detailId);
	}


	@Override
	protected List<PortfolioRunLDIExposure> getPortfolioRunTradeDetailListForRunImpl(int runId) {
		return getPortfolioRunLDIService().getPortfolioRunLDIExposureListByRun(runId, true);
	}


	@Override
	public void populatePortfolioRunTradeDetailListWithTrades(PortfolioRunTradeLDI run, boolean doNotPopulateDefaults) {
		List<PortfolioRunLDIExposure> list = getPortfolioRunTradeDetailListForRun(run.getId());
		Map<String, List<InvestmentAccountRelationship>> runHoldingAccountRelationshipMap = new HashMap<>();

		// Only Setup Defaults if will be generating trades, if just viewing, only need to get Pending Trades
		boolean populateDefaults = (!doNotPopulateDefaults && run.isTradingAllowed());

		if (populateDefaults && !CollectionUtils.isEmpty(list)) {
			for (PortfolioRunLDIExposure exp : list) {
				populatePortfolioRunTradeDetailDefaults(exp, runHoldingAccountRelationshipMap);
			}
		}

		run.setDetailList(list);
	}


	@Override
	@Transactional(readOnly = true)
	public void populatePortfolioRunTradeDetailPendingAndCurrentContracts(PortfolioRunTradeLDI run) {
		List<PortfolioRunLDIExposure> exposureList = run.getDetailList();

		PortfolioAccountContractStore current = getPortfolioAccountDataRetriever().getPortfolioAccountContractStoreActual(run.getClientInvestmentAccount(), getNextBusinessDayFromRun(run), true);
		PortfolioAccountContractStore pending = getPortfolioAccountDataRetriever().getPortfolioAccountContractStorePending(current, run.getClientInvestmentAccount(), null);

		applyContractQuantityToExposureList(current, exposureList, "currentContracts");
		applyContractQuantityToExposureList(pending, exposureList, "pendingContracts");
	}


	private void applyContractQuantityToExposureList(PortfolioAccountContractStore store, List<PortfolioRunLDIExposure> exposureList, String fieldName) {
		Map<Integer, PortfolioAccountContractStore.SecurityPositionQuantity> securityQuantityMap = store.getSecurityQuantityMap();
		if (securityQuantityMap != null && !securityQuantityMap.isEmpty()) {
			for (PortfolioRunLDIExposure exp : exposureList) {
				PortfolioAccountContractStore.SecurityPositionQuantity securityPositionQuantity = securityQuantityMap.get(exp.getSecurity().getId());
				BeanUtils.setPropertyValue(exp, fieldName, securityPositionQuantity == null ? null : securityPositionQuantity.getQuantity());
			}
		}
	}


	private void populatePortfolioRunTradeDetailTargetContracts(PortfolioRunTradeLDI run) {
		Integer calculatorSystemBeanId = (Integer) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(run.getClientInvestmentAccount(), InvestmentAccount.CLIENT_ACCOUNT_PORTFOLIO_RUN_LDI_TARGET_CONTRACT_CALCULATOR);

		if (calculatorSystemBeanId != null) {
			PortfolioRunLDITargetContractCalculator targetContractsCalculator = (PortfolioRunLDITargetContractCalculator) getSystemBeanService().getBeanInstance(getSystemBeanService().getSystemBean(calculatorSystemBeanId));
			targetContractsCalculator.calculateTargetContracts(run);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunLDIService getPortfolioRunLDIService() {
		return this.portfolioRunLDIService;
	}


	public void setPortfolioRunLDIService(PortfolioRunLDIService portfolioRunLDIService) {
		this.portfolioRunLDIService = portfolioRunLDIService;
	}
}
