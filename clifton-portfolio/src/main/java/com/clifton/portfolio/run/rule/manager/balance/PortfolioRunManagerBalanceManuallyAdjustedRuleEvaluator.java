package com.clifton.portfolio.run.rule.manager.balance;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.UpdatableEntity;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalance;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.portfolio.run.rule.manager.BasePortfolioRunManagerAccountRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Evaluates for a Portfolio Run manager account balances that have either
 * Manual Adjustments or Adjustments from Events (Optional property)
 *
 * @author manderson
 */
public class PortfolioRunManagerBalanceManuallyAdjustedRuleEvaluator extends BasePortfolioRunManagerAccountRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private DaoLocator daoLocator;
	private SecurityUserService securityUserService;
	private SystemSchemaService systemSchemaService;

	/**
	 * Validates manual adjustments against not system defined, but also the source
	 * Note: Source table is defined via Cause table of the Rule Definition
	 * Regular Manual Adjustments include null source table, or cause table = InvestmentManagerAccountBalanceAdjustment (Copy Previous)
	 */
	private boolean includeNullSourceTable;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		List<? extends PortfolioRunManagerAccountBalance> managerAccountBalanceList = context.getPortfolioRunManagerAccountBalanceList(run, false);

		List<Integer> adjustmentIds = new ArrayList<>();
		for (PortfolioRunManagerAccountBalance managerAccountBalance : CollectionUtils.getIterable(managerAccountBalanceList)) {
			EntityConfig config = getEntityConfig(managerAccountBalance.getManagerAccountBalance().getManagerAccount(), ruleConfig);
			if (config != null && !config.isExcluded() && managerAccountBalance.isManuallyAdjusted()) {
				for (InvestmentManagerAccountBalanceAdjustment adjustment : CollectionUtils.getIterable(managerAccountBalance.getManualAdjustmentList())) {
					if (!adjustmentIds.contains(adjustment.getId())) {
						if (adjustment.getSourceSystemTable() == null) {
							if (isIncludeNullSourceTable()) {
								adjustmentIds.add(adjustment.getId());
							}
						}
						else if (adjustment.getSourceSystemTable().equals(getRuleDefinitionService().getRuleDefinition(ruleConfig.getRuleDefinitionId()).getCauseTable())) {
							adjustmentIds.add(adjustment.getId());
						}
					}
				}
			}
		}

		for (Integer balanceAdjustmentId : CollectionUtils.getIterable(adjustmentIds)) {
			// In the end, we want to apply the original adjustment amount on the balance - not rollup amounts that can perform an allocation based on child percentages
			InvestmentManagerAccountBalanceAdjustment adjustment = context.getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceAdjustment(balanceAdjustmentId);

			long entityFkFieldId = BeanUtils.getIdentityAsLong(adjustment.getManagerAccountBalance().getManagerAccount());
			EntityConfig entityConfig = ruleConfig.getEntityConfig(entityFkFieldId);
			Map<String, Object> templateValues = new HashMap<>();
			templateValues.put("securitiesAdjustment", adjustment.getSecuritiesValue());
			templateValues.put("cashAdjustment", adjustment.getCashValue());
			templateValues.put("adjustmentNote", adjustment.getNote() != null ? adjustment.getNote() : "NONE");

			Integer causeFkFieldId = adjustment.getSourceFkFieldId();
			SystemTable causeTable = adjustment.getSourceSystemTable();
			if (adjustment.getSourceSystemTable() == null || StringUtils.isEqual("InvestmentManagerAccountBalanceAdjustment", adjustment.getSourceSystemTable().getName())) {
				causeFkFieldId = balanceAdjustmentId;
				causeTable = getSystemSchemaService().getSystemTableByName("InvestmentManagerAccountBalanceAdjustment");
			}
			SecurityUser user = null;
			IdentityObject causeEntity = getDaoLocator().locate(causeTable.getName()).findByPrimaryKey(causeFkFieldId);
			if (causeEntity == null) {
				throw new IllegalStateException("Cannot find Cause Entity for table '" + causeTable.getName() + "' with id = " + causeFkFieldId + ". InvestmentManagerAccountBalanceAdjustment = " + balanceAdjustmentId);
			}
			if (UpdatableEntity.class.isAssignableFrom(causeEntity.getClass())) {
				user = getSecurityUserService().getSecurityUser(((UpdatableEntity) causeEntity).getCreateUserId());
			}
			if (user == null) {
				user = getSecurityUserService().getSecurityUser(adjustment.getCreateUserId());
			}
			templateValues.put("userName", user != null ? user.getDisplayName() : "Unknown User");
			ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), entityFkFieldId, MathUtils.getNumberAsLong(causeFkFieldId), null, templateValues));
		}
		return ruleViolationList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public boolean isIncludeNullSourceTable() {
		return this.includeNullSourceTable;
	}


	public void setIncludeNullSourceTable(boolean includeNullSourceTable) {
		this.includeNullSourceTable = includeNullSourceTable;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
