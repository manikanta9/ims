package com.clifton.portfolio.run.ldi;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowGroupHistory;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunLDIManagerAccount</code>
 * <p/>
 * Phase I of LDI KRD Processing
 *
 * @author manderson
 */
public class PortfolioRunLDIManagerAccount extends BaseSimpleEntity<Integer> implements LabeledObject {

	private PortfolioRun portfolioRun;

	private InvestmentManagerAccountAssignment managerAccountAssignment;

	private InvestmentSecurity benchmarkSecurity;

	private PortfolioCashFlowGroupHistory cashFlowGroupHistory;

	private BigDecimal cashTotal;

	private BigDecimal securitiesTotal;

	private Date krdValueDate;

	private List<PortfolioRunLDIManagerAccountKeyRateDurationPoint> keyRateDurationPointList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		String label = StringUtils.EMPTY_STRING;
		if (getManagerAccountAssignment() != null) {
			label = getManagerAccountAssignment().getReferenceOne().getLabel();
			if (!StringUtils.isEmpty(getManagerAccountAssignment().getDisplayName())) {
				label += " (" + getManagerAccountAssignment().getDisplayName() + ")";
			}
			return label;
		}
		else if (getCashFlowGroupHistory() != null) {
			return "Calculated Cash Flow Liabilities for " + getCashFlowGroupHistory().getLabel();
		}
		return label;
	}


	public BigDecimal getTotalValue() {
		return MathUtils.add(getCashTotal(), getSecuritiesTotal());
	}


	public Map<String, BigDecimal> getKeyRateDurationPointMap() {
		Map<String, BigDecimal> vMap = new HashMap<>();
		for (PortfolioRunLDIManagerAccountKeyRateDurationPoint point : CollectionUtils.getIterable(getKeyRateDurationPointList())) {
			vMap.put("KRD_" + point.getKeyRateDurationPoint().getId(), point.getKrdValue());
			vMap.put("DV01_" + point.getKeyRateDurationPoint().getId(), point.getDv01Value());
		}
		return vMap;
	}


	public ManagerCashTypes getCashType() {
		return getCashFlowGroupHistory() != null ? ManagerCashTypes.LIABILITIES : getManagerAccountAssignment().getCashType();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	public void setPortfolioRun(PortfolioRun portfolioRun) {
		this.portfolioRun = portfolioRun;
	}


	public InvestmentManagerAccountAssignment getManagerAccountAssignment() {
		return this.managerAccountAssignment;
	}


	public void setManagerAccountAssignment(InvestmentManagerAccountAssignment managerAccountAssignment) {
		this.managerAccountAssignment = managerAccountAssignment;
	}


	public InvestmentSecurity getBenchmarkSecurity() {
		return this.benchmarkSecurity;
	}


	public void setBenchmarkSecurity(InvestmentSecurity benchmarkSecurity) {
		this.benchmarkSecurity = benchmarkSecurity;
	}


	public PortfolioCashFlowGroupHistory getCashFlowGroupHistory() {
		return this.cashFlowGroupHistory;
	}


	public void setCashFlowGroupHistory(PortfolioCashFlowGroupHistory cashFlowGroupHistory) {
		this.cashFlowGroupHistory = cashFlowGroupHistory;
	}


	public BigDecimal getCashTotal() {
		return this.cashTotal;
	}


	public void setCashTotal(BigDecimal cashTotal) {
		this.cashTotal = cashTotal;
	}


	public BigDecimal getSecuritiesTotal() {
		return this.securitiesTotal;
	}


	public void setSecuritiesTotal(BigDecimal securitiesTotal) {
		this.securitiesTotal = securitiesTotal;
	}


	public List<PortfolioRunLDIManagerAccountKeyRateDurationPoint> getKeyRateDurationPointList() {
		return this.keyRateDurationPointList;
	}


	public void setKeyRateDurationPointList(List<PortfolioRunLDIManagerAccountKeyRateDurationPoint> keyRateDurationPointList) {
		this.keyRateDurationPointList = keyRateDurationPointList;
	}


	public Date getKrdValueDate() {
		return this.krdValueDate;
	}


	public void setKrdValueDate(Date krdValueDate) {
		this.krdValueDate = krdValueDate;
	}
}
