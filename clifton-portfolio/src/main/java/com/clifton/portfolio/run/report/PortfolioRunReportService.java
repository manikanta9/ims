package com.clifton.portfolio.run.report;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.portfolio.run.PortfolioRun;


/**
 * The <code>PortfolioRunReportService</code> ...
 *
 * @author manderson
 */
public interface PortfolioRunReportService {

	////////////////////////////////////////////////////////////////////////////
	////////        Portfolio Run Report Business Methods             //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the report id selected for the client account for the run based on custom column value
	 */
	@DoNotAddRequestMapping
	public Integer getPortfolioRunReportId(PortfolioRun run);


	@SecureMethod(dtoClass = PortfolioRun.class)
	public FileWrapper downloadPortfolioRunReport(int runId, Boolean regenerateCache, FileFormats format, Boolean excludePrivate);


	@DoNotAddRequestMapping
	public void clearPortfolioRunReportCache(int runId);


	/**
	 * Post report to the portal - from UI
	 */
	@SecureMethod(dtoClass = PortfolioRun.class, permissions = SecurityPermission.PERMISSION_READ)
	public void postPortfolioRunReport(int runId);


	/**
	 * Internal method - called from workflow action
	 * Post report to the portal
	 * <p>
	 * Returns the run which has the updated object with postedToPortal property set
	 */
	@DoNotAddRequestMapping
	public PortfolioRun postPortfolioRunReportForRun(PortfolioRun run);
}
