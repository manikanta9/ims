package com.clifton.portfolio.run.process.calculator;


import com.clifton.portfolio.run.process.PortfolioRunConfig;


/**
 * The <code>PortfolioRunProcessStepCalculator</code> handles calculations for a specific portion of portfolio run processing. Beans can be created of this type,
 * and each bean will represent an individual step in the processing of portfolio runs.
 *
 * @author manderson
 */
public interface PortfolioRunProcessStepCalculator<T extends PortfolioRunConfig> {

	/**
	 * @param runConfig should indicate whether to process as a pre- or post-process step
	 */
	public void process(T runConfig);


	public String getWorkflowStateName();


	public boolean isDoNotExecuteTransactional();
}
