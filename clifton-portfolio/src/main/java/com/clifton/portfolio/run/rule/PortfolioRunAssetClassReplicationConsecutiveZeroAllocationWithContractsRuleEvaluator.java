package com.clifton.portfolio.run.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ProductOverlayAssetClassReplicationConsecutiveZeroAllocationWithContractsRuleEvaluator</code> creates violations under the following:
 * <p>
 * 1. Exclude Matching Replications
 * 2. If Allocation Weight % and Adjusted Allocation Weight % are both zero and actual contracts is not zero (see below screen shot for where these values are coming from)
 * 3. Find the replication on the previous day's run (found by matching asset class, security, and replication which is also what we do for the duration change rule).
 * 4. If previous is found, and the Allocation Weight % and Adjusted Allocation Weight % are both zero on the previous run, then create a rule violation.
 *
 * @author manderson
 */
public class PortfolioRunAssetClassReplicationConsecutiveZeroAllocationWithContractsRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		List<BasePortfolioRunReplication> replicationList = context.getReplicationList(run, false);
		List<BasePortfolioRunReplication> previousReplicationList = context.getReplicationList(run, true);

		for (BasePortfolioRunReplication replication : CollectionUtils.getIterable(replicationList)) {
			// Skip Matching Replications
			if (replication.isMatchingReplication()) {
				continue;
			}
			// Passing null to getEntityConfig will return the default entity config
			Integer assetClassId = replication.getAccountAssetClass() != null ? replication.getAccountAssetClass().getId() : null;
			EntityConfig entityConfig = ruleConfig.getEntityConfig(MathUtils.getNumberAsLong(assetClassId));
			if (entityConfig != null && !entityConfig.isExcluded()) {
				if (MathUtils.isNullOrZero(replication.getAllocationWeight()) && MathUtils.isNullOrZero(replication.getAllocationWeightAdjusted()) && !MathUtils.isNullOrZero(replication.getActualContracts())) {
					// Find matching in previous run on account asset class id, replication, and security
					for (BasePortfolioRunReplication previousReplication : CollectionUtils.getIterable(previousReplicationList)) {
						if (replication.getAccountAssetClass().equals(previousReplication.getAccountAssetClass()) &&
								replication.getSecurity().equals(previousReplication.getSecurity()) && replication.getReplication().equals(previousReplication.getReplication())) {

							// Once found - Check if values were 0 on that run too
							if (MathUtils.isNullOrZero(previousReplication.getAllocationWeight()) && MathUtils.isNullOrZero(previousReplication.getAllocationWeightAdjusted())) {
								// If so - add violation
								Map<String, Object> templateValues = new HashMap<>();
								templateValues.put("replication", replication);
								templateValues.put("previousReplication", previousReplication);
								ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), replication.getId(), null, templateValues));
							}
							break;
						}
					}
				}
			}
		}
		return ruleViolationList;
	}
}
