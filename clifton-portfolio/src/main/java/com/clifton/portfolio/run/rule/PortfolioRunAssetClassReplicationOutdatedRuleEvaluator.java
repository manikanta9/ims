package com.clifton.portfolio.run.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>PortfolioRunAssetClassReplicationOutdatedRuleEvaluator</code> creates violations
 * when a asset class has a benchmark security that has an outdated duration, or a duration that does not exist.
 *
 * @author stevenf
 */
public class PortfolioRunAssetClassReplicationOutdatedRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		Set<Integer> replicationIdList = new HashSet<>();
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		List<BasePortfolioRunReplication> replicationList = context.getReplicationList(run);
		for (BasePortfolioRunReplication runReplication : CollectionUtils.getIterable(replicationList)) {
			InvestmentReplication replication = runReplication.getReplication();
			if (replication != null && replicationIdList.add(replication.getId())) {
				Map<String, Object> templateValues = new HashMap<>();
				// Passing null to getEntityConfig() will return the default entity config
				Long entityFkFieldId = runReplication.getAccountAssetClass() != null ? BeanUtils.getIdentityAsLong(runReplication.getAccountAssetClass()) : null;
				EntityConfig entityConfig = ruleConfig.getEntityConfig(entityFkFieldId);
				if (entityConfig != null && !entityConfig.isExcluded()) {
					replication = context.getInvestmentReplicationService().getInvestmentReplication(replication.getId());
					if (!isReplicationExcluded(replication) && RuleViolationUtil.isObjectOutdated(run.getBalanceDate(), replication.getUpdateDate(), entityConfig.getRuleAmount(), templateValues)) {
						templateValues.put("replication", replication);
						ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), entityFkFieldId, BeanUtils.getIdentityAsLong(replication), null, templateValues));
					}
				}
			}
		}
		return ruleViolationList;
	}


	private boolean isReplicationExcluded(InvestmentReplication replication) {
		if (replication.getAllocationList() != null) {
			for (InvestmentReplicationAllocation allocation : replication.getAllocationList()) {
				if (!(allocation.getAllocationWeight().compareTo(BigDecimal.valueOf(100)) == 0 ||
						allocation.getAllocationWeight().compareTo(BigDecimal.valueOf(-100)) == 0 ||
						allocation.getAllocationWeight().compareTo(BigDecimal.valueOf(0)) == 0)) {
					return false;
				}
			}
		}
		return true;
	}
}
