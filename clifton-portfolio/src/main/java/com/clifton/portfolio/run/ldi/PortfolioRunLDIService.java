package com.clifton.portfolio.run.ldi;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portfolio.run.ldi.search.PortfolioRunLDIExposureSearchForm;
import com.clifton.portfolio.run.ldi.search.PortfolioRunLDIManagerAccountKeyRateDurationPointSearchForm;
import com.clifton.portfolio.run.ldi.search.PortfolioRunLDIManagerAccountSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>PortfolioRunLDIService</code> ...
 *
 * @author manderson
 */
public interface PortfolioRunLDIService {

	/**
	 * Called from processor to delete all related many side tables for LDI runs
	 */
	@DoNotAddRequestMapping
	public void deletePortfolioRunProcessing(int id);


	////////////////////////////////////////////////////////////////////////////
	////////          Portfolio Run LDI Manager Account Methods         ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunLDIManagerAccount getPortfolioRunLDIManagerAccount(int id);


	public List<PortfolioRunLDIManagerAccount> getPortfolioRunLDIManagerAccountList(PortfolioRunLDIManagerAccountSearchForm searchForm);


	public List<PortfolioRunLDIManagerAccount> getPortfolioRunLDIManagerAccountListByRun(int runId, boolean populatePoints);


	public void savePortfolioRunLDIManagerAccountList(List<PortfolioRunLDIManagerAccount> list);


	////////////////////////////////////////////////////////////////////////////
	////////     Portfolio Run LDI Manager Account KRD Point Methods    ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunLDIManagerAccountKeyRateDurationPoint getPortfolioRunLDIManagerAccountKeyRateDurationPoint(int id);


	public List<PortfolioRunLDIManagerAccountKeyRateDurationPoint> getPortfolioRunLDIManagerAccountKeyRateDurationPointListByRun(int runId);


	public List<PortfolioRunLDIManagerAccountKeyRateDurationPoint> getPortfolioRunLDIManagerAccountKeyRateDurationPointList(PortfolioRunLDIManagerAccountKeyRateDurationPointSearchForm searchForm);


	public void savePortfolioRunLDIManagerAccountKeyRateDurationPointList(List<PortfolioRunLDIManagerAccountKeyRateDurationPoint> list);


	////////////////////////////////////////////////////////////////////////////
	////////           Portfolio Run LDI KRD Point Methods              ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunLDIKeyRateDurationPoint getPortfolioRunLDIKeyRateDurationPoint(int id);


	public List<PortfolioRunLDIKeyRateDurationPoint> getPortfolioRunLDIKeyRateDurationPointListByRun(int runId, Boolean reportableOnly);


	public void savePortfolioRunLDIKeyRateDurationPointList(List<PortfolioRunLDIKeyRateDurationPoint> list);


	////////////////////////////////////////////////////////////////////////////
	////////           Portfolio Run LDI Exposure Methods              ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunLDIExposure getPortfolioRunLDIExposure(int id);


	/**
	 * Returns the {@link PortfolioRunLDIExposure}s for the given run. If {@param populatePoints} is true, will populate the Managed Points for each exposure.
	 */
	public List<PortfolioRunLDIExposure> getPortfolioRunLDIExposureListByRun(int runId, boolean populatePoints);


	public List<PortfolioRunLDIExposure> getPortfolioRunLDIExposureList(PortfolioRunLDIExposureSearchForm searchForm);


	public void savePortfolioRunLDIExposureList(List<PortfolioRunLDIExposure> list);


	public PortfolioRunLDIExposure copyPortfolioRunLDIExposure(int id, int newSecurityId, Date valueDateOverride);


	////////////////////////////////////////////////////////////////////////////
	////////       Portfolio Run LDI Exposure KRD Point Methods         ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunLDIExposureKeyRateDurationPoint getPortfolioRunLDIExposureKeyRateDurationPoint(int id);


	public List<PortfolioRunLDIExposureKeyRateDurationPoint> getPortfolioRunLDIExposureKeyRateDurationPointListByRun(int runId, Boolean reportableOnly);


	public void savePortfolioRunLDIExposureKeyRateDurationPointList(List<PortfolioRunLDIExposureKeyRateDurationPoint> list);
}
