package com.clifton.portfolio.run.trade;


import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;

import java.util.List;


/**
 * The <code>PortfolioRunTradeHandler</code> ...
 *
 * @author manderson
 */
public interface PortfolioRunTradeHandler<T extends PortfolioRunTrade<D>, D extends PortfolioRunTradeDetail> {

	////////////////////////////////////////////////////////////////////////////
	///////      Portfolio Run Trade Creation Business Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns ProductOverlayRun object with replication list populated for trade creation/review
	 *
	 * @param doNotPopulateDefaults - Usually false which means setting default executing broker, trade destination, etc. is based on if the run is available for trading.
	 *                              However, can be turned off (used for Option Roll) so we don't waste time attempting to retrieve set this information for all securities since we will just need holding account for Options which is handled by the Option Roll Calculator
	 */
	public T getPortfolioRunTrade(PortfolioRun run, boolean doNotPopulateDefaults);


	/**
	 * Refreshes Prices to latest Bloomberg or Previous Night Close
	 *
	 * @param reloadDetails - Called to fully reload detail records when new rows are added
	 */
	public T getPortfolioRunTradePrices(T run, boolean livePrices, boolean reloadDetails);


	/**
	 * Refreshes Pending/Actual trades to most recent reflected in the database
	 * Whatever prices are passed are left as they are.
	 */
	public T getPortfolioRunTradeAmounts(T run);


	/**
	 * Populates the default Trade details for the detail List of the provided {@link PortfolioRunTrade<D>}
	 */
	public T populatePortfolioRunTradeDetailTradeDefaults(T run);


	/**
	 * Saves Trades entered for a run based on buys/sells entered for detail in the detailList
	 * <p>
	 * Returns TradeGroup that was just created for submitted trades
	 */
	public TradeGroup processPortfolioRunTradeCreation(T bean);


	/**
	 * Submits all pending trades to Approved workflow state
	 */
	public String processPortfolioRunTradeApproval(Integer[] detailIds);


	/**
	 * Submits all pending trades for the detail list provided to the Approved workflow state.
	 * This service behaves the same way as {@link #processPortfolioRunTradeApproval(Integer[])}
	 * without looking up or hydrating the detail(s).
	 * <p>
	 * Minimum properties required on each detail include:
	 * <br>- id
	 * <br>- security.id
	 * <br>- portfolioRun.clientInvestmentAccount.id ((Overlay) overlayAssetClass.portfolioRun)
	 * <br>- (Overlay) overlayAssetClass.accountAssetClass.id
	 */
	public String processPortfolioRunTradeApproval(List<D> detailList);


	/**
	 * Returns a list of Pending Trades for a specific detail record.
	 * NOTE: Does NOT perform any contract splitting
	 */
	public List<Trade> getPortfolioRunTradeDetailPendingTradeList(int detailId);


	/**
	 * Returns a list of Pending Trades for a specific detail record.
	 * NOTE: Does NOT perform any contract splitting
	 * This service behaves the same way as {@link #getPortfolioRunTradeDetailPendingTradeList(int)}
	 * without looking up or hydrating the detail.
	 * <p>
	 * Minimum properties required on the detail include:
	 * <br>- id
	 * <br>- security.id
	 * <br>- portfolioRun.clientInvestmentAccount.id ((Overlay) overlayAssetClass.portfolioRun)
	 * <br>- (Overlay) overlayAssetClass.accountAssetClass.id
	 */
	public List<Trade> getPortfolioRunTradeDetailPendingTradeList(D detail);


	/**
	 * Returns a list of Pending Transfers for a specific detail record.
	 * NOTE: Does NOT perform any contract splitting
	 */
	public List<AccountingPositionTransfer> getPortfolioRunTradeDetailPendingTransferList(int detailId);


	/**
	 * Returns a list of valid holding accounts for a Detail record
	 */
	public List<InvestmentAccount> getPortfolioRunTradeDetailHoldingInvestmentAccountList(int detailId);
}
