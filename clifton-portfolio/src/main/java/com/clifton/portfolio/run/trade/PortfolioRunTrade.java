package com.clifton.portfolio.run.trade;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.replication.calculators.InvestmentReplicationCurrencyTypes;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.trade.group.TradeGroupType;

import java.util.Date;
import java.util.List;


/**
 * The <code>PortfolioRunTrade</code> ...
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class PortfolioRunTrade<D extends PortfolioRunTradeDetail> extends PortfolioRun {

	private Date tradeDate;

	/**
	 * Currency to use when generating details for display and trading.
	 * Default is base currency.
	 */
	private InvestmentReplicationCurrencyTypes tradingCurrencyType = InvestmentReplicationCurrencyTypes.getDefaultCurrencyType();

	/**
	 * Used to override the TradeGroupType used for created Trades.
	 */
	private TradeGroupType tradeGroupType;

	private List<D> detailList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public InvestmentReplicationCurrencyTypes getTradingCurrencyType() {
		return this.tradingCurrencyType;
	}


	public void setTradingCurrencyType(InvestmentReplicationCurrencyTypes tradingCurrencyType) {
		this.tradingCurrencyType = tradingCurrencyType;
	}


	public TradeGroupType getTradeGroupType() {
		return this.tradeGroupType;
	}


	public void setTradeGroupType(TradeGroupType tradeGroupType) {
		this.tradeGroupType = tradeGroupType;
	}


	public List<D> getDetailList() {
		return this.detailList;
	}


	public void setDetailList(List<D> detailList) {
		this.detailList = detailList;
	}
}
