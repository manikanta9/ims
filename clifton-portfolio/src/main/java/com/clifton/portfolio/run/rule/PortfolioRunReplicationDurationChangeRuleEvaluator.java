package com.clifton.portfolio.run.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author mitchellf
 */
public class PortfolioRunReplicationDurationChangeRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	/**
	 * Can either check against each security's duration value from replications
	 * or the asset class benchmark duration changes
	 */
	private boolean assetClassBenchmark;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun ruleBean, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded() && context.getPreviousRun(ruleBean) != null) {
			if (isAssetClassBenchmark()) {
				return evaluateRuleForAssetClassBenchmarkDurationValueChange(ruleBean, entityConfig, context);
			}
			else {
				return evaluateRuleForReplicationDurationValueChange(ruleBean, entityConfig, context);
			}
		}
		return new ArrayList<>();
	}


	protected List<RuleViolation> evaluateRuleForAssetClassBenchmarkDurationValueChange(PortfolioRun run, EntityConfig entityConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		throw new ValidationException("Asset Class benchmark is not supported for this configuration");
	}


	protected List<RuleViolation> evaluateRuleForReplicationDurationValueChange(PortfolioRun run, EntityConfig entityConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		List<BasePortfolioRunReplication> replicationList = context.getReplicationList(run, false);
		List<BasePortfolioRunReplication> previousReplicationList = context.getReplicationList(run, true);

		for (BasePortfolioRunReplication replication : CollectionUtils.getIterable(replicationList)) {
			if (!MathUtils.isNullOrZero(replication.getDuration())) {
				// Find matching in previous run on account asset class id, replication, and security
				for (BasePortfolioRunReplication previousReplication : CollectionUtils.getIterable(previousReplicationList)) {
					if (!MathUtils.isNullOrZero(previousReplication.getDuration()) && replication.getAccountAssetClass().equals(previousReplication.getAccountAssetClass()) &&
							replication.getSecurity().equals(previousReplication.getSecurity()) && replication.getReplication().equals(previousReplication.getReplication())) {
						BigDecimal currentValue = replication.getDuration();
						BigDecimal previousValue = previousReplication.getDuration();
						if (currentValue != null && previousValue != null) {
							BigDecimal percentChange = MathUtils.getPercentChange(previousValue, currentValue, true);
							Map<String, Object> templateValues = new HashMap<>();
							if (RuleEvaluatorUtils.isEntityConfigRangeViolated(percentChange, entityConfig, templateValues)) {
								templateValues.put("currentValue", currentValue);
								templateValues.put("previousValue", previousValue);
								ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), replication.getId(), null, templateValues));
								break;
							}
						}
					}
				}
			}
		}
		return ruleViolationList;
	}


	public boolean isAssetClassBenchmark() {
		return this.assetClassBenchmark;
	}


	public void setAssetClassBenchmark(boolean assetClassBenchmark) {
		this.assetClassBenchmark = assetClassBenchmark;
	}
}
