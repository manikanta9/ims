package com.clifton.portfolio.run;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.investment.replication.calculators.BaseInvestmentReplicationSecurityAllocation;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.portfolio.replication.PortfolioReplicationAllocationSecurityConfig;
import com.clifton.portfolio.run.trade.PortfolioRunTradeDetail;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseType;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>BasePortfolioRunReplication</code> class represents a specific security position that is a part of a replication held in a portfolio.
 * <p>
 * It contains existing position as well as recommended (target) position information.
 * <p>
 * Classes that extend it can link it to the entity responsible for portfolio structure (e.g. asset class, target, etc.)
 *
 * @author nickk
 */
public abstract class BasePortfolioRunReplication extends BaseInvestmentReplicationSecurityAllocation<Integer> implements LabeledObject, PortfolioRunTradeDetail {

	private PortfolioRun portfolioRun;

	// Populated directly only when creating the replication. Not persisted in the DB
	@NonPersistentField
	private PortfolioReplicationAllocationSecurityConfig allocationConfig;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	// Historical Rebalance Info
	private BigDecimal rebalanceTriggerMin;
	private BigDecimal rebalanceTriggerMax;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	/**
	 * The following fields are used for Trade Generation
	 * Buy and Sell Contracts are set explicitly on the replication itself
	 * and during saving process is populate on the trade as quantityIntended & trade.buy
	 * NOT PERSISTED IN THE DB.  After save, it's cleared and values are moved to pending contracts
	 */
	@NonPersistentField
	private Trade trade;
	@NonPersistentField
	private BigDecimal buyContracts; // trade.quantityIntended if trade.buy = true
	@NonPersistentField
	private BigDecimal sellContracts; // trade.quantityIntended if trade.buy = false;
	@NonPersistentField
	private TradeOpenCloseType openCloseType; // trade.openCloseType to specify the transaction's position impact
	@NonPersistentField
	private Integer daysToSettle; // currently used for trade group window to override the settlement date from securities default days to settle
	@NonPersistentField
	private Boolean closePosition; // flag to close position

	// Other Not Closed Trades (Not Booked & Not Cancelled)
	@NonPersistentField
	private BigDecimal pendingContracts;
	@NonPersistentField
	private BigDecimal pendingVirtualContracts;

	// Existing Positions as of next business day (those trades that have posted since EOD previous business day)
	@NonPersistentField
	private BigDecimal currentContracts;
	@NonPersistentField
	private BigDecimal currentVirtualContracts;

	@NonPersistentField
	private BigDecimal currencyPendingLocalAmount;
	@NonPersistentField
	private BigDecimal currencyPendingOtherBaseAmount;

	@NonPersistentField
	private BigDecimal currencyPendingDenominationBaseAmount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected String getDependentEntityTopLevelLabel() {
		return getDependentEntityLabel();
	}


	protected abstract String getDependentEntityLabel();


	public abstract boolean isMatchingReplication();


	public abstract boolean isRollupReplication();


	public abstract boolean isTargetFollowsActual();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void clearNonPersistentFields() {
		super.clearNonPersistentFields();

		this.trade = null;
		this.buyContracts = null;
		this.sellContracts = null;
		this.openCloseType = null;
		this.daysToSettle = null;
		this.pendingContracts = null;
		this.pendingVirtualContracts = null;
		this.currentContracts = null;
		this.currentVirtualContracts = null;
		this.currencyPendingLocalAmount = null;
		this.currencyPendingOtherBaseAmount = null;
		this.currencyPendingDenominationBaseAmount = null;
	}


	public void setMarketDataLive(String valueFieldName, MarketDataLive marketDataLive) {
		// Only populate market data live if not manually overridden (extra check - also checked before actually calling method to get value)
		if (!isTradePropertyValueManuallyOverridden(valueFieldName)) {
			if (marketDataLive != null) {
				BigDecimal val = marketDataLive.asNumericValue();
				Date valueDate = marketDataLive.getMeasureDate();

				if (val != null && valueDate != null) {
					Date currentValueDate = BeanUtils.getFieldValue(this, valueFieldName + "Date");
					BigDecimal currentValue = BeanUtils.getFieldValue(this, valueFieldName);

					if (currentValueDate == null || valueDate.after(currentValueDate) || (valueDate.compareTo(currentValueDate) == 0 && !MathUtils.isEqual(val, currentValue))) {
						BeanUtils.setPropertyValue(this, valueFieldName, val);
						BeanUtils.setPropertyValue(this, valueFieldName + "Date", valueDate);
					}
				}
			}
		}
	}


	@Override
	public InvestmentAccount getClientAccount() {
		return getPortfolioRun() != null ? getPortfolioRun().getClientInvestmentAccount() : null;
	}


	@Override
	public InvestmentAccount getTradingClientAccount() {
		return getClientAccount();
	}


	@Override
	public Date getBalanceDate() {
		return getPortfolioRun() == null ? null : getPortfolioRun().getBalanceDate();
	}


	@Override
	public InvestmentSecurity getBenchmarkDurationSecurity() {
		return null;
	}


	@Override
	public BigDecimal getBenchmarkDuration() {
		return null;
	}


	@Override
	public BigDecimal getBenchmarkCreditDuration() {
		return null;
	}


	@Override
	public String getDurationFieldNameOverride() {
		return null;
	}


	@Override
	public Boolean getDoNotAdjustContractValue() {
		if (getAccountAssetClass() != null) {
			return getAccountAssetClass().isDoNotAdjustContractValue();
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getSecurity().getInstrument() != null) {
			return getLabelShort() + " - " + getSecurity().getInstrument().getName();
		}
		return getLabelShort();
	}


	protected String getDurationLabel() {
		return "";
	}


	public String getTopLevelLabel() {
		return getTopLevelLabelShort() + getDurationLabel();
	}


	public String getLabelLong() {
		return getLabelShort() + getDurationLabel();
	}


	public String getLabelShort() {
		String label = "";
		if (getDependentEntityLabel() != null) {
			label += getDependentEntityLabel();
		}
		label += " - " + getReplication().getType().getName();
		return label;
	}


	public String getTopLevelLabelShort() {
		String label = (getDependentEntityTopLevelLabel() != null) ? getDependentEntityTopLevelLabel() : "";
		InvestmentReplicationType type = getReplicationType();
		if (type != null) {
			label += " - " + type.getName();
		}
		return label;
	}


	/**
	 * Trade Entry is disabled for asset classes that are flagged as positions being excluded or hold a Non Tradable Securities These contracts should be held in other asset
	 * classes where trading is enabled there, i.e. "duplicate" contract and these buys/sells are updated to reflect those trades to see the trade impact
	 */
	@Override
	public boolean isTradeEntryDisabled() {
		if (isSecurityNotTradable()) {
			return true;
		}
		if (isCashExposure()) {
			return true;
		}
		if (getAccountAssetClass() != null) {
			return getAccountAssetClass().isReplicationPositionExcludedFromCount();
		}

		return false;
	}


	public boolean isSecurityNotTradable() {
		if (getSecurity() != null && getSecurity().getInstrument() != null && getSecurity().getInstrument().getHierarchy() != null) {
			return getSecurity().getInstrument().getHierarchy().isTradingDisallowed();
		}
		return false;
	}


	public BigDecimal getAllocationWeight() {
		if (getReplicationAllocation() != null) {
			return getReplicationAllocation().getAllocationWeight();
		}
		return getAllocationWeightAdjusted();  // Shouldn't happen as allocation history is required
	}


	/**
	 * Rollup Asset classes are excluded because we'd double count for the rollup and its children
	 * Matching Replications are also excluded
	 */
	public boolean isExcludedFromOverlayTotal() {
		if (isRollupReplication()) {
			return true;
		}
		return (isMatchingReplication());
	}


	/**
	 * Returns the total amount to be applied to the overlay target total for the run
	 * Matching Replications = 0
	 * Currency Replications = Add Trading CCY, Subtract Currency Total
	 * Also = rollup asset classes are excluded so not double counted.
	 */
	public BigDecimal getOverlayTargetTotal() {
		if (isExcludedFromOverlayTotal()) {
			return BigDecimal.ZERO;
		}
		BigDecimal val = getTargetExposureAdjusted();

		if (isCurrencyReplication()) {
			if (!MathUtils.isNullOrZero(getCurrencyDenominationBaseAmount())) {
				val = MathUtils.add(val, getCurrencyDenominationBaseAmount());
			}

			if (!MathUtils.isNullOrZero(getCurrencyTotalBaseAmount())) {
				val = MathUtils.subtract(val, getCurrencyTotalBaseAmount());
			}
		}
		return val;
	}


	public BigDecimal getOverlayExposureTotal() {
		if (isExcludedFromOverlayTotal()) {
			return BigDecimal.ZERO;
		}
		return getActualExposureAdjusted();
	}


	/**
	 * Includes any current and pending trades, so if used for a trade comparison, need to back out that trade's allocation
	 */
	@Override
	public BigDecimal calculateSuggestedTradeQuantity() {
		// Should Match to "Difference" column in UI - note - screen logic is left as is this is for trade violations
		// Don't evaluate if trading is disabled
		if (!isTradeEntryDisabled()) {
			BigDecimal target = getTargetExposureAdjustedWithMarkToMarket();
			// Add in CCY Denomination Base Amounts
			target = MathUtils.add(target, getCurrencyCurrentDenominationBaseAmount());
			target = MathUtils.add(target, getCurrencyPendingDenominationBaseAmount());
			// Adjust for Mispricing
			target = MathUtils.subtract(target, getMispricingTradeValue());
			// If it's a currency replication - reduce by the currency base amount
			if (isCurrencyReplication()) {
				target = MathUtils.subtract(target, getCurrencyCurrentTotalBaseAmount());
			}
			BigDecimal targetContracts = MathUtils.divide(target, getTradeValue());
			if (isReverseExposureSign()) {
				targetContracts = MathUtils.negate(targetContracts);
			}
			// Remove Current and Pending from Target to see final suggested trade amount
			targetContracts = MathUtils.subtract(targetContracts, MathUtils.add(getCurrentContractsAdjusted(), getPendingContractsAdjusted()));
			return targetContracts;
		}
		return null;
	}


	public boolean isRebalanceTriggersUsed() {
		return getRebalanceTriggerMin() != null || getRebalanceTriggerMax() != null;
	}


	public boolean isOutsideOfRebalanceTrigger() {
		return !MathUtils.isInRange(getAmountOffTarget(), getRebalanceTriggerMin(), getRebalanceTriggerMax());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the M2M adjustment value based on the change in the Security Price
	 * M2M Adjustment for the Security = Change in Security Price * Price Multiplier * # Actual Contracts
	 */
	public BigDecimal getMarkToMarketAdjustmentValue() {
		// If Asset Class is Target Follows Actual - Don't apply M2M Adjustment since it's already implied in the actual value
		if (isTargetFollowsActual()) {
			return BigDecimal.ZERO;
		}
		// Replication Type - IsDoNotApplyM2MAdjustment
		if (getReplicationType() != null && getReplicationType().isDoNotApplyM2MAdjustment()) {
			return BigDecimal.ZERO;
		}

		// No Actual Contracts - No M2M Adjustment
		if (MathUtils.isNullOrZero(getActualContractsAdjusted())) {
			return BigDecimal.ZERO;
		}
		BigDecimal tradePrice = getTradeSecurityPrice();
		BigDecimal tradeFx = getTradeExchangeRate();
		if (MathUtils.isNullOrZero(tradePrice)) {
			tradePrice = getSecurityPrice();
		}
		if (MathUtils.isNullOrZero(tradeFx)) {
			tradeFx = getExchangeRate();
		}
		// No Price or Fx Rate Changes - No M2M Adjustment
		if (MathUtils.isEqual(tradePrice, getSecurityPrice()) && MathUtils.isEqual(tradeFx, getExchangeRate())) {
			return BigDecimal.ZERO;
		}
		BigDecimal before = MathUtils.multiply(MathUtils.multiply(getSecurityPrice(), getSecurity().getPriceMultiplier()), getExchangeRate());
		BigDecimal after = MathUtils.multiply(MathUtils.multiply(tradePrice, getSecurity().getPriceMultiplier()), tradeFx);

		BigDecimal change = MathUtils.subtract(after, before);

		if (isReverseExposureSign()) {
			change = MathUtils.negate(change);
		}

		return MathUtils.multiply(change, getActualContractsAdjusted());
	}


	@Override
	public BigDecimal getCurrentContracts() {
		// The concept of "cash exposure" doesn't use contracts, except that the contracts are used to determine
		// exposure so it's used to back into it.  For trade creation, since these aren't really contracts, there is nothing "current"
		// for these always uses actual
		if (isCashExposure()) {
			return getActualContracts();
		}
		return this.currentContracts;
	}


	@Override
	public void setCurrentContracts(BigDecimal currentContracts) {
		this.currentContracts = currentContracts;
	}


	public BigDecimal getPendingContractsAdjusted() {
		return MathUtils.add(getPendingContracts(), getPendingVirtualContracts());
	}


	public BigDecimal getCurrentContractsAdjusted() {
		return MathUtils.add(getCurrentContracts(), getCurrentVirtualContracts());
	}


	public BigDecimal getActualExposure() {
		return getExposure(getActualContracts());
	}


	public BigDecimal getActualExposureAdjusted() {
		return MathUtils.add(MathUtils.add(getActualExposure(), getVirtualExposure()), getTotalAdditionalExposure());
	}


	public BigDecimal getPendingExposureAdjusted() {
		return MathUtils.add(getExposure(getPendingContracts()), getExposure(getPendingVirtualContracts()));
	}


	public BigDecimal getCurrentExposureAdjusted() {
		return MathUtils.add(getExposure(getCurrentContracts()), getExposure(getCurrentVirtualContracts()));
	}


	public BigDecimal getVirtualExposure() {
		return getExposure(getVirtualContracts());
	}


	public BigDecimal getTargetAdjustedLessActualExposure() {
		return MathUtils.subtract(getTargetExposureAdjusted(), getActualExposureAdjusted());
	}


	private BigDecimal getExposure(BigDecimal contracts) {
		if (MathUtils.isNullOrZero(contracts)) {
			return BigDecimal.ZERO;
		}
		// To match values calculated after db save, # contracts save with 2 decimals and contract value with 10 decimals
		contracts = MathUtils.round(contracts, 2);
		BigDecimal val = getValue();
		if (MathUtils.isNullOrZero(val)) {
			return BigDecimal.ZERO;
		}
		val = MathUtils.round(val, 10);
		if (isReverseExposureSign()) {
			contracts = MathUtils.negate(contracts);
		}
		return MathUtils.multiply(val, contracts);
	}


	// Targets
	public boolean isTargetAdjusted() {
		return !MathUtils.isEqual(getAllocationWeight(), getAllocationWeightAdjusted());
	}


	public BigDecimal getTargetExposureAdjustedWithMarkToMarket() {
		// Matching Replication M2M Adjustment is applied to the non-matching target exposure
		if (isMatchingReplication()) {
			return getTargetExposureAdjusted();
		}
		return MathUtils.add(getTargetExposureAdjusted(), getMarkToMarketAdjustmentValue());
	}


	public BigDecimal getTargetContractsAdjustedTradeValue() {
		// When Using Trade Value, also include M2M Adjustment
		BigDecimal val = getTradeValue();
		if (MathUtils.isNullOrZero(val)) {
			return BigDecimal.ZERO;
		}
		return MathUtils.divide(getTargetExposureAdjustedWithMarkToMarket(), val);
	}


	public BigDecimal getActualTargetAdjustedContractDifference() {
		return MathUtils.subtract(getTargetContractsAdjusted(), getActualContractsAdjusted());
	}


	public boolean isSyntheticallyAdjusted() {
		return !MathUtils.isNullOrZero(getSyntheticAdjustmentFactor());
	}


	/**
	 * Mispricing Calculation is applied if fair value adjustment used is flagged on the instrument (Currently used for Equity Index Futures)
	 * Calculation (Per Contract) is: (Future Price - (Index Price + Fair Value Adjustment)) * FX Rate
	 * <p>
	 * Total Mispricing for the position - Mispricing Per Contract * # of Contracts * Future Price Multiplier
	 */
	public BigDecimal getMispricingValue() {
		if (getSecurity().getInstrument().isFairValueAdjustmentUsed()) {
			BigDecimal fairVal = getFairValueAdjustment();
			if (fairVal != null) {
				return getMispricingValueImpl(fairVal, getSecurityPrice(), getUnderlyingSecurityPrice(), getExchangeRate());
			}
		}
		return null;
	}


	/**
	 * Similar to getMispricingValue but uses "trade" i.e. Live Data when available
	 */
	public BigDecimal getMispricingTradeValue() {
		if (getSecurity().getInstrument().isFairValueAdjustmentUsed()) {
			BigDecimal fairVal = getTradeFairValueAdjustment() != null ? getTradeFairValueAdjustment() : getFairValueAdjustment();
			if (fairVal != null) {
				BigDecimal price = getTradeSecurityPrice() != null ? getTradeSecurityPrice() : getSecurityPrice();
				BigDecimal underlyingPrice = getTradeUnderlyingSecurityPrice() != null ? getTradeUnderlyingSecurityPrice() : getUnderlyingSecurityPrice();
				BigDecimal fxRate = !MathUtils.isNullOrZero(getTradeExchangeRate()) ? getTradeExchangeRate() : getExchangeRate();
				return getMispricingValueImpl(fairVal, price, underlyingPrice, fxRate);
			}
		}
		return null;
	}


	protected BigDecimal getMispricingValueImpl(BigDecimal fva, BigDecimal secPrice, BigDecimal underlyingPrice, BigDecimal fxRate) {
		BigDecimal val = MathUtils.subtract(secPrice, MathUtils.add(underlyingPrice, fva));
		val = MathUtils.multiply(val, fxRate);
		val = MathUtils.multiply(val, getSecurity().getPriceMultiplier());
		val = MathUtils.multiply(val, getActualContractsAdjusted());
		return val;
	}


	public BigDecimal getCurrencyPendingTradeBaseAmount() {
		return MathUtils.multiply(getCurrencyPendingLocalAmount(), getTradeCurrencyExchangeRate());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	// Rebalance Triggers Min & Max Percentages


	public BigDecimal getCurrencyPendingTotalBaseAmount() {
		if (!isCurrencyReplication()) {
			return null;
		}
		return MathUtils.add(getCurrencyPendingTradeBaseAmount(), getCurrencyPendingOtherBaseAmount());
	}


	public BigDecimal getCurrencyCurrentTotalBaseAmount() {
		if (!isCurrencyReplication()) {
			return null;
		}
		BigDecimal val = MathUtils.add(getCurrencyCurrentTradeBaseAmount(), getCurrencyCurrentOtherBaseAmount());
		return MathUtils.add(val, getCurrencyCurrentUnrealizedTradeBaseAmount());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// Used to Determine if Rebalancing Triggers were crossed
	// Actual - Target Adjusted
	public BigDecimal getAmountOffTarget() {
		// Negate it because we want actual - target
		// if actual is bigger than we want positive return since it's "overweight"
		return MathUtils.negate(getTargetAdjustedLessActualExposure());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	@Override
	public void setPortfolioRun(PortfolioRun portfolioRun) {
		this.portfolioRun = portfolioRun;
	}


	@Override
	public Trade getTrade() {
		return this.trade;
	}


	@Override
	public void setTrade(Trade trade) {
		this.trade = trade;
	}


	@Override
	public BigDecimal getPendingContracts() {
		return this.pendingContracts;
	}


	@Override
	public void setPendingContracts(BigDecimal pendingContracts) {
		this.pendingContracts = pendingContracts;
	}


	@Override
	public BigDecimal getSellContracts() {
		return this.sellContracts;
	}


	@Override
	public void setSellContracts(BigDecimal sellContracts) {
		this.sellContracts = sellContracts;
	}


	@Override
	public BigDecimal getBuyContracts() {
		return this.buyContracts;
	}


	@Override
	public void setBuyContracts(BigDecimal buyContracts) {
		this.buyContracts = buyContracts;
	}


	@Override
	public TradeOpenCloseType getOpenCloseType() {
		return this.openCloseType;
	}


	@Override
	public void setOpenCloseType(TradeOpenCloseType openCloseType) {
		this.openCloseType = openCloseType;
	}


	public BigDecimal getCurrencyPendingLocalAmount() {
		return this.currencyPendingLocalAmount;
	}


	public void setCurrencyPendingLocalAmount(BigDecimal currencyPendingLocalAmount) {
		this.currencyPendingLocalAmount = currencyPendingLocalAmount;
	}


	public BigDecimal getCurrencyPendingOtherBaseAmount() {
		return this.currencyPendingOtherBaseAmount;
	}


	public void setCurrencyPendingOtherBaseAmount(BigDecimal currencyPendingOtherBaseAmount) {
		this.currencyPendingOtherBaseAmount = currencyPendingOtherBaseAmount;
	}


	public PortfolioReplicationAllocationSecurityConfig getAllocationConfig() {
		return this.allocationConfig;
	}


	public void setAllocationConfig(PortfolioReplicationAllocationSecurityConfig allocationConfig) {
		this.allocationConfig = allocationConfig;
	}


	public BigDecimal getRebalanceTriggerMin() {
		return this.rebalanceTriggerMin;
	}


	public void setRebalanceTriggerMin(BigDecimal rebalanceTriggerMin) {
		this.rebalanceTriggerMin = rebalanceTriggerMin;
	}


	public BigDecimal getRebalanceTriggerMax() {
		return this.rebalanceTriggerMax;
	}


	public void setRebalanceTriggerMax(BigDecimal rebalanceTriggerMax) {
		this.rebalanceTriggerMax = rebalanceTriggerMax;
	}


	public BigDecimal getCurrencyPendingDenominationBaseAmount() {
		return this.currencyPendingDenominationBaseAmount;
	}


	public void setCurrencyPendingDenominationBaseAmount(BigDecimal currencyPendingDenominationBaseAmount) {
		this.currencyPendingDenominationBaseAmount = currencyPendingDenominationBaseAmount;
	}


	public BigDecimal getPendingVirtualContracts() {
		return this.pendingVirtualContracts;
	}


	public void setPendingVirtualContracts(BigDecimal pendingVirtualContracts) {
		this.pendingVirtualContracts = pendingVirtualContracts;
	}


	public BigDecimal getCurrentVirtualContracts() {
		return this.currentVirtualContracts;
	}


	public void setCurrentVirtualContracts(BigDecimal currentVirtualContracts) {
		this.currentVirtualContracts = currentVirtualContracts;
	}


	@Override
	public Integer getDaysToSettle() {
		return this.daysToSettle;
	}


	@Override
	public void setDaysToSettle(Integer daysToSettle) {
		this.daysToSettle = daysToSettle;
	}


	@Override
	public Boolean getClosePosition() {
		return this.closePosition;
	}


	@Override
	public void setClosePosition(Boolean closePosition) {
		this.closePosition = closePosition;
	}
}
