package com.clifton.portfolio.run.workflow;


import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.process.PortfolioRunProcessService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>PortfolioRunProcessWorkflowAction</code> clears the report cache
 * and then processes the run
 *
 * @author manderson
 */
public class PortfolioRunProcessWorkflowAction implements WorkflowTransitionActionHandler<PortfolioRun> {


	private PortfolioRunProcessService portfolioRunProcessService;

	private boolean reloadProxyManagers;


	@Override
	public PortfolioRun processAction(PortfolioRun bean, WorkflowTransition transition) {
		getPortfolioRunProcessService().processPortfolioRun(bean.getId(), isReloadProxyManagers(), null);
		return bean;
	}


	public boolean isReloadProxyManagers() {
		return this.reloadProxyManagers;
	}


	public void setReloadProxyManagers(boolean reloadProxyManagers) {
		this.reloadProxyManagers = reloadProxyManagers;
	}


	public PortfolioRunProcessService getPortfolioRunProcessService() {
		return this.portfolioRunProcessService;
	}


	public void setPortfolioRunProcessService(PortfolioRunProcessService portfolioRunProcessService) {
		this.portfolioRunProcessService = portfolioRunProcessService;
	}
}
