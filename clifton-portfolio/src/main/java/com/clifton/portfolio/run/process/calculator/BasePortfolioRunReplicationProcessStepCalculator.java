package com.clifton.portfolio.run.process.calculator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.replication.ReplicationRebalanceTriggerTypes;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.process.PortfolioRunConfig;

import java.math.BigDecimal;
import java.util.List;


/**
 * {@link BasePortfolioRunReplicationProcessStepCalculator} is an abstract {@link BasePortfolioRunProcessStepCalculatorImpl} used to provide common methods for
 * {@link BasePortfolioRunReplication} processing.
 *
 * @author michaelm
 */
public abstract class BasePortfolioRunReplicationProcessStepCalculator<T extends PortfolioRunConfig> extends BasePortfolioRunProcessStepCalculatorImpl<T> {

	protected <R extends BasePortfolioRunReplication> List<R> setPortfolioRunReplicationRebalanceTriggerValues(List<R> repList, BigDecimal total) {
		for (R rep : CollectionUtils.getIterable(repList)) {
			if (rep.getReplication().isRebalanceTriggersUsed() && rep.getAllocationConfig() != null && rep.getAllocationConfig().getReplicationAllocation().isRebalancingUsed()) {
				BigDecimal minPercent = rep.getAllocationConfig().getReplicationAllocation().getRebalanceAllocationMin();
				BigDecimal maxPercent = rep.getAllocationConfig().getReplicationAllocation().getRebalanceAllocationMax();

				BigDecimal percent = rep.getAllocationWeightAdjusted();
				// In cases of Negative Targets, we need to use the absolute value, so that our min/max rebalance triggers are calculated correctly
				percent = MathUtils.abs(percent);

				if (ReplicationRebalanceTriggerTypes.FIXED != rep.getReplication().getRebalanceTriggerType()) {
					// RECALC PERCENTAGE PROPORTIONALLY - CALCULATING AGAINST ADJUSTED TARGET
					minPercent = MathUtils.getPercentageOf(minPercent, percent, true);
					maxPercent = MathUtils.getPercentageOf(maxPercent, percent, true);
				}

				// Calculate actual amount based on given percentage - Negate Min Amounts
				rep.setRebalanceTriggerMin(MathUtils.negate(MathUtils.getPercentageOf(minPercent, total, true)));
				rep.setRebalanceTriggerMax(MathUtils.getPercentageOf(maxPercent, total, true));
			}
		}
		return repList;
	}
}
