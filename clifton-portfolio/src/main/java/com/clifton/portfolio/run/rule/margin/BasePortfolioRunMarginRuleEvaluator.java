package com.clifton.portfolio.run.rule.margin;

import com.clifton.core.util.CollectionUtils;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.margin.PortfolioRunMargin;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.List;


/**
 * BasePortfolioRunMarginRuleEvaluator is a base rule evaluator class that can be extended to run rules against all margin records for the run
 *
 * @author manderson
 */
public abstract class BasePortfolioRunMarginRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {


	@Override
	public final List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<PortfolioRunMargin> runMarginList = context.getPortfolioRunMarginListForRun(run);
			for (PortfolioRunMargin brokerMargin : CollectionUtils.getIterable(runMarginList)) {
				ruleViolationList.addAll(evaluateRuleForPortfolioRunMargin(run, brokerMargin, entityConfig, context));
			}
		}
		return ruleViolationList;
	}


	public abstract List<RuleViolation> evaluateRuleForPortfolioRunMargin(PortfolioRun run, PortfolioRunMargin margin, EntityConfig entityConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context);
}
