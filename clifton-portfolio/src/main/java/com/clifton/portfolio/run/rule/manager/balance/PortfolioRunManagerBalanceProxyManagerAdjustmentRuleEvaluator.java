package com.clifton.portfolio.run.rule.manager.balance;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.portfolio.run.rule.manager.BasePortfolioRunManagerAccountRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunManagerBalanceProxyManagerAdjustmentRuleEvaluator</code> is used to find proxy managers that were updated ON the balance date
 * but also contain other adjustment types.  i.e. Previous Day's MOC Adjustment.  This is to prevent potential market value double counting
 *
 * @author manderson
 */
public class PortfolioRunManagerBalanceProxyManagerAdjustmentRuleEvaluator extends BasePortfolioRunManagerAccountRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> implements ValidationAware {

	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;


	/**
	 * What types of adjustments to look for to throw violation for
	 */
	private List<Short> investmentManagerAccountBalanceAdjustmentTypeList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotEmpty(getInvestmentManagerAccountBalanceAdjustmentTypeList(), "At least one adjustment type must be selected.");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Cash Balance, Securities Balance, or Total Balance Range Rule
	 */
	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();

		Collection<InvestmentManagerAccountBalance> balanceList = context.getInvestmentManagerAccountBalanceList(run);
		List<Long> managerAccountIdsChecked = new ArrayList<>();
		for (InvestmentManagerAccountBalance balance : CollectionUtils.getIterable(balanceList)) {
			ruleViolationList.addAll(evaluateManagerAccountBalanceRule(run, balance, ruleConfig));
			managerAccountIdsChecked.add(BeanUtils.getIdentityAsLong(balance.getManagerAccount()));
		}

		// Special Additional Rule Evaluation For Managers
		// Each individual rule assignment could potentially have a rule for a manager account not directly assigned, which would add it to the list for that specific rule evaluation
		// This is because a manager account may be linked to the account through a rollup, but only a specific child has a particular rule defined.  Or, a rollup manager could be defined to group
		// managers together for the purposes of evaluating a rule against that group through the rollup manager.  (i.e. Manager X + Manager Y balance combined should never go below 1 million)
		for (Long managerAccountId : CollectionUtils.getIterable(ruleConfig.getEntityIdList())) {
			managerAccountIdsChecked.add(managerAccountId);
			if (!managerAccountIdsChecked.contains(managerAccountId) && (!ruleConfig.getEntityConfig(managerAccountId).isExcluded())) {
				InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(MathUtils.getNumberAsInteger(managerAccountId), run.getBalanceDate(), false);
				if (balance != null) {
					ruleViolationList.addAll(evaluateManagerAccountBalanceRule(run, balance, ruleConfig));
				}
			}
		}
		return ruleViolationList;
	}


	private List<RuleViolation> evaluateManagerAccountBalanceRule(PortfolioRun run, InvestmentManagerAccountBalance balance, RuleConfig ruleConfig) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = getEntityConfig(balance.getManagerAccount(), ruleConfig);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			// If a proxy manager and balance date = proxy value date and balance is adjusted
			if (balance.getManagerAccount().isProxyManager() && DateUtils.isEqualWithoutTime(run.getBalanceDate(), balance.getManagerAccount().getProxyValueDate()) && balance.isAdjusted()) {
				// Pull the balance again now, with the adjustments populated so we can check them.
				balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalance(balance.getId());
				List<InvestmentManagerAccountBalanceAdjustment> appliedAdjustmentList = BeanUtils.filter(balance.getAdjustmentList(), adjustment -> getInvestmentManagerAccountBalanceAdjustmentTypeList().contains(adjustment.getAdjustmentType().getId()));
				if (!CollectionUtils.isEmpty(appliedAdjustmentList)) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("adjustmentList", appliedAdjustmentList);
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), BeanUtils.getIdentityAsLong(balance.getManagerAccount()), BeanUtils.getIdentityAsLong(balance), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public List<Short> getInvestmentManagerAccountBalanceAdjustmentTypeList() {
		return this.investmentManagerAccountBalanceAdjustmentTypeList;
	}


	public void setInvestmentManagerAccountBalanceAdjustmentTypeList(List<Short> investmentManagerAccountBalanceAdjustmentTypeList) {
		this.investmentManagerAccountBalanceAdjustmentTypeList = investmentManagerAccountBalanceAdjustmentTypeList;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}
}

