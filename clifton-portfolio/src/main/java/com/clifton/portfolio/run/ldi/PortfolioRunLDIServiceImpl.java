package com.clifton.portfolio.run.ldi;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyListCache;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory;
import com.clifton.investment.replication.history.InvestmentReplicationHistoryService;
import com.clifton.investment.shared.ClientAccount;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationPoint;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationSecurity;
import com.clifton.portfolio.account.ldi.PortfolioAccountLDIService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.ldi.search.PortfolioRunLDIExposureSearchForm;
import com.clifton.portfolio.run.ldi.search.PortfolioRunLDIManagerAccountKeyRateDurationPointSearchForm;
import com.clifton.portfolio.run.ldi.search.PortfolioRunLDIManagerAccountSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The <code>PortfolioRunLDIServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class PortfolioRunLDIServiceImpl implements PortfolioRunLDIService {

	private AdvancedUpdatableDAO<PortfolioRunLDIManagerAccount, Criteria> portfolioRunLDIManagerAccountDAO;
	private AdvancedUpdatableDAO<PortfolioRunLDIManagerAccountKeyRateDurationPoint, Criteria> portfolioRunLDIManagerAccountKeyRateDurationPointDAO;

	private AdvancedUpdatableDAO<PortfolioRunLDIKeyRateDurationPoint, Criteria> portfolioRunLDIKeyRateDurationPointDAO;

	private AdvancedUpdatableDAO<PortfolioRunLDIExposure, Criteria> portfolioRunLDIExposureDAO;
	private AdvancedUpdatableDAO<PortfolioRunLDIExposureKeyRateDurationPoint, Criteria> portfolioRunLDIExposureKeyRateDurationPointDAO;

	private DaoCompositeKeyListCache<PortfolioRunLDIManagerAccountKeyRateDurationPoint, Integer, Boolean> portfolioRunLDIManagerAccountKeyRateDurationPointByManagerAccountCache;
	private DaoCompositeKeyListCache<PortfolioRunLDIExposureKeyRateDurationPoint, Integer, Boolean> portfolioRunLDIExposureKeyRateDurationPointByExposureCache;

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentReplicationHistoryService investmentReplicationHistoryService;
	private InvestmentReplicationService investmentReplicationService;
	private PortfolioAccountLDIService portfolioAccountLDIService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private MarketDataRetriever marketDataRetriever;

	private InvestmentCalculator investmentCalculator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void deletePortfolioRunProcessing(int id) {
		getPortfolioRunLDIExposureKeyRateDurationPointDAO().deleteList(getPortfolioRunLDIExposureKeyRateDurationPointListByRun(id, null));
		getPortfolioRunLDIExposureDAO().deleteList(getPortfolioRunLDIExposureListByRun(id, false));

		getPortfolioRunLDIKeyRateDurationPointDAO().deleteList(getPortfolioRunLDIKeyRateDurationPointListByRun(id, null));

		getPortfolioRunLDIManagerAccountKeyRateDurationPointDAO().deleteList(getPortfolioRunLDIManagerAccountKeyRateDurationPointListByRun(id));
		getPortfolioRunLDIManagerAccountDAO().deleteList(getPortfolioRunLDIManagerAccountListByRun(id, false));
	}

	////////////////////////////////////////////////////////////////////////////
	////////          Portfolio Run LDI Manager Account Methods         ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRunLDIManagerAccount getPortfolioRunLDIManagerAccount(int id) {
		PortfolioRunLDIManagerAccount bean = getPortfolioRunLDIManagerAccountDAO().findByPrimaryKey(id);
		if (bean != null) {
			PortfolioRunLDIManagerAccountKeyRateDurationPointSearchForm searchForm = new PortfolioRunLDIManagerAccountKeyRateDurationPointSearchForm();
			searchForm.setManagerAccountId(id);
			bean.setKeyRateDurationPointList(getPortfolioRunLDIManagerAccountKeyRateDurationPointList(searchForm));
		}
		return bean;
	}


	@Override
	public List<PortfolioRunLDIManagerAccount> getPortfolioRunLDIManagerAccountList(PortfolioRunLDIManagerAccountSearchForm searchForm) {
		List<PortfolioRunLDIManagerAccount> managerAccountList = getPortfolioRunLDIManagerAccountDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		if (searchForm.isPopulatePoints()) {
			populatePoints(managerAccountList, searchForm.isReportableOnly());
		}
		return managerAccountList;
	}


	@Override
	public List<PortfolioRunLDIManagerAccount> getPortfolioRunLDIManagerAccountListByRun(int runId, boolean populatePoints) {
		PortfolioRunLDIManagerAccountSearchForm searchForm = new PortfolioRunLDIManagerAccountSearchForm();
		searchForm.setRunId(runId);
		searchForm.setPopulatePoints(populatePoints);
		return getPortfolioRunLDIManagerAccountList(searchForm);
	}


	private List<PortfolioRunLDIManagerAccount> populatePoints(List<PortfolioRunLDIManagerAccount> managerAccountList, Boolean reportableOnly) {
		for (PortfolioRunLDIManagerAccount ma : CollectionUtils.getIterable(managerAccountList)) {
			ma.setKeyRateDurationPointList(getPortfolioRunLDIManagerAccountKeyRateDurationPointByManagerAccountCache().getBeanListForKeyValues(getPortfolioRunLDIManagerAccountKeyRateDurationPointDAO(), ma.getId(), reportableOnly));
		}
		return managerAccountList;
	}


	@Override
	public void savePortfolioRunLDIManagerAccountList(List<PortfolioRunLDIManagerAccount> list) {
		getPortfolioRunLDIManagerAccountDAO().saveList(list);
	}

	////////////////////////////////////////////////////////////////////////////
	////////     Portfolio Run LDI Manager Account KRD Point Methods    ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRunLDIManagerAccountKeyRateDurationPoint getPortfolioRunLDIManagerAccountKeyRateDurationPoint(int id) {
		return getPortfolioRunLDIManagerAccountKeyRateDurationPointDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortfolioRunLDIManagerAccountKeyRateDurationPoint> getPortfolioRunLDIManagerAccountKeyRateDurationPointListByRun(final int runId) {
		PortfolioRunLDIManagerAccountKeyRateDurationPointSearchForm searchForm = new PortfolioRunLDIManagerAccountKeyRateDurationPointSearchForm();
		searchForm.setRunId(runId);
		return getPortfolioRunLDIManagerAccountKeyRateDurationPointList(searchForm);
	}


	@Override
	public List<PortfolioRunLDIManagerAccountKeyRateDurationPoint> getPortfolioRunLDIManagerAccountKeyRateDurationPointList(PortfolioRunLDIManagerAccountKeyRateDurationPointSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public boolean configureOrderBy(Criteria criteria) {
				List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(searchForm.getOrderBy());
				if (!CollectionUtils.isEmpty(orderByList)) {
					return super.configureOrderBy(criteria);
				}
				criteria.addOrder(Order.desc(getPathAlias("managerAccount.managerAccountAssignment", criteria, JoinType.LEFT_OUTER_JOIN) + ".cashType"));
				criteria.addOrder(Order.asc(getPathAlias("managerAccount", criteria) + ".id"));
				criteria.addOrder(Order.asc(getPathAlias("keyRateDurationPoint.marketDataField", criteria) + ".fieldOrder"));
				return true;
			}
		};
		return getPortfolioRunLDIManagerAccountKeyRateDurationPointDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public void savePortfolioRunLDIManagerAccountKeyRateDurationPointList(List<PortfolioRunLDIManagerAccountKeyRateDurationPoint> list) {
		getPortfolioRunLDIManagerAccountKeyRateDurationPointDAO().saveList(list);
	}

	////////////////////////////////////////////////////////////////////////////
	////////           Portfolio Run LDI KRD Point Methods              ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRunLDIKeyRateDurationPoint getPortfolioRunLDIKeyRateDurationPoint(int id) {
		return getPortfolioRunLDIKeyRateDurationPointDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortfolioRunLDIKeyRateDurationPoint> getPortfolioRunLDIKeyRateDurationPointListByRun(final int runId, Boolean reportableOnly) {
		HibernateSearchConfigurer searchConfig = new HibernateSearchConfigurer() {

			@Override
			public void configureCriteria(Criteria criteria) {
				criteria.add(Restrictions.eq("portfolioRun.id", runId));
				// reportable points vs. managed points filtering
				criteria.createAlias("keyRateDurationPoint", "ap");
				if (reportableOnly != null) {
					criteria.add(Restrictions.eq("ap.reportableOnly", reportableOnly));
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				criteria.createAlias("ap.marketDataField", "p");
				criteria.addOrder(Order.asc("p.fieldOrder"));
				return true;
			}
		};
		return getPortfolioRunLDIKeyRateDurationPointDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public void savePortfolioRunLDIKeyRateDurationPointList(List<PortfolioRunLDIKeyRateDurationPoint> list) {
		getPortfolioRunLDIKeyRateDurationPointDAO().saveList(list);
	}

	////////////////////////////////////////////////////////////////////////////
	////////             Portfolio Run LDI Exposure Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRunLDIExposure getPortfolioRunLDIExposure(int id) {
		PortfolioRunLDIExposure bean = getPortfolioRunLDIExposureDAO().findByPrimaryKey(id);
		if (bean != null) {
			bean.setKeyRateDurationPointList(getPortfolioRunLDIExposureKeyRateDurationPointByExposureCache().getBeanListForKeyValues(getPortfolioRunLDIExposureKeyRateDurationPointDAO(), bean.getId(), null));
		}
		return bean;
	}


	@Override
	public List<PortfolioRunLDIExposure> getPortfolioRunLDIExposureListByRun(int runId, boolean populatePoints) {
		PortfolioRunLDIExposureSearchForm searchForm = new PortfolioRunLDIExposureSearchForm();
		searchForm.setRunId(runId);
		searchForm.setPopulatePoints(populatePoints);
		searchForm.setReportableOnly(false);
		return getPortfolioRunLDIExposureList(searchForm);
	}


	@Override
	public List<PortfolioRunLDIExposure> getPortfolioRunLDIExposureList(PortfolioRunLDIExposureSearchForm searchForm) {
		List<PortfolioRunLDIExposure> list = getPortfolioRunLDIExposureDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		if (searchForm.isPopulatePoints()) {
			for (PortfolioRunLDIExposure exp : CollectionUtils.getIterable(list)) {
				exp.setKeyRateDurationPointList(getPortfolioRunLDIExposureKeyRateDurationPointByExposureCache().getBeanListForKeyValues(getPortfolioRunLDIExposureKeyRateDurationPointDAO(), exp.getId(), searchForm.isReportableOnly()));
			}
		}
		return list;
	}


	@Override
	@Transactional
	public PortfolioRunLDIExposure copyPortfolioRunLDIExposure(int id, int newSecurityId, Date valueDateOverride) {
		PortfolioRunLDIExposure currentExposure = getPortfolioRunLDIExposure(id);
		InvestmentSecurity securityToAdd = getInvestmentInstrumentService().getInvestmentSecurity(newSecurityId);
		List<PortfolioRunLDIExposure> fullRunExposureList = getPortfolioRunLDIExposureListByRun(currentExposure.getPortfolioRun().getId(), true);

		return copyPortfolioRunLDIExposureImpl(currentExposure, securityToAdd, fullRunExposureList, valueDateOverride != null ? valueDateOverride : new Date());
	}


	private PortfolioRunLDIExposure copyPortfolioRunLDIExposureImpl(PortfolioRunLDIExposure currentExposure, InvestmentSecurity securityToAdd, List<PortfolioRunLDIExposure> fullRunExposureList, Date valueDate) {

		InvestmentReplicationAllocationHistory allocationHistory = validateNewPortfolioRunLDIExposureFindAllocation(currentExposure, securityToAdd, fullRunExposureList, true);

		PortfolioRunLDIExposure newExposure = new PortfolioRunLDIExposure();

		newExposure.setPortfolioRun(currentExposure.getPortfolioRun());
		newExposure.setReplicationAllocationHistory(allocationHistory);
		newExposure.setKrdValueDate(ObjectUtils.coalesce(valueDate, currentExposure.getKrdValueDate()));
		newExposure.setPositionInvestmentAccount(currentExposure.getPositionInvestmentAccount());
		newExposure.setKeyRateDurationPointList(currentExposure.getKeyRateDurationPointList());
		newExposure.setCurrentSecurity(currentExposure.isCurrentSecurity());
		newExposure.setPositionInvestmentAccount(currentExposure.getPositionInvestmentAccount());

		newExposure.setSecurity(securityToAdd);

		populatePortfolioRunLDIExposure(newExposure);

		savePortfolioRunLDIExposure(newExposure);

		createNewKrdPoints(currentExposure, securityToAdd, newExposure);
		return getPortfolioRunLDIExposure(newExposure.getId());
	}


	@Override
	public void savePortfolioRunLDIExposureList(List<PortfolioRunLDIExposure> list) {
		getPortfolioRunLDIExposureDAO().saveList(list);
	}


	private PortfolioRunLDIExposure savePortfolioRunLDIExposure(PortfolioRunLDIExposure exposure) {
		return getPortfolioRunLDIExposureDAO().save(exposure);
	}

	////////////////////////////////////////////////////////////////////////////
	////////       Portfolio Run LDI Exposure KRD Point Methods         ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRunLDIExposureKeyRateDurationPoint getPortfolioRunLDIExposureKeyRateDurationPoint(int id) {
		return getPortfolioRunLDIExposureKeyRateDurationPointDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortfolioRunLDIExposureKeyRateDurationPoint> getPortfolioRunLDIExposureKeyRateDurationPointListByRun(final int runId, Boolean reportableOnly) {

		HibernateSearchConfigurer searchConfig = new HibernateSearchConfigurer() {

			@Override
			public void configureCriteria(Criteria criteria) {
				criteria.createAlias("exposure", "e");
				criteria.createAlias("keyRateDurationPoint", "ap");
				criteria.add(Restrictions.eq("e.portfolioRun.id", runId));
				if (reportableOnly != null) {
					criteria.add(Restrictions.eq("ap.reportableOnly", reportableOnly));
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				criteria.createAlias("exposure.replicationAllocationHistory", "rah");
				criteria.createAlias("rah.replicationAllocation", "ra");
				criteria.addOrder(Order.asc("ra.order"));
				criteria.addOrder(Order.asc("exposure.id"));
				criteria.createAlias("ap.marketDataField", "p");
				criteria.addOrder(Order.asc("p.fieldOrder"));
				return true;
			}
		};
		return getPortfolioRunLDIExposureKeyRateDurationPointDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public void savePortfolioRunLDIExposureKeyRateDurationPointList(List<PortfolioRunLDIExposureKeyRateDurationPoint> list) {
		getPortfolioRunLDIExposureKeyRateDurationPointDAO().saveList(list);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Helper Methods                           ////////
	////////////////////////////////////////////////////////////////////////////


	private void populatePortfolioRunLDIExposure(PortfolioRunLDIExposure exposure) {
		InvestmentReplicationAllocation replicationAllocation = exposure.getReplicationAllocation();
		PortfolioRun run = exposure.getPortfolioRun();
		exposure.setSecurityPrice(getMarketDataRetriever().getPrice(exposure.getSecurity(), run.getBalanceDate(), true, null));
		// Determine if Dirty Price Should be Populated based on the Replication Type (uses override if set) options
		boolean useDirtyPrice = replicationAllocation.getCoalesceInvestmentReplicationTypeOverride().isDirtyPriceSupported();
		if (useDirtyPrice) {
			// Some dirty price calculators (IRS and CDS) will result in different values depending on if we hold long or short.
			exposure.setSecurityDirtyPrice(getInvestmentCalculator().calculateDirtyPrice(exposure.getSecurity(), BigDecimal.ZERO, exposure.getSecurityPrice(), run.getBalanceDate()));
		}
		ClientAccount clientAccount = ObjectUtils.coalesce(exposure.getPositionInvestmentAccount(), run.getClientInvestmentAccount()).toClientAccount();
		String fromCurrency = exposure.getSecurity().getInstrument().getTradingCurrency().getSymbol();
		exposure.setExchangeRate(getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forClientAccount(clientAccount, fromCurrency, run.getClientInvestmentAccount().getBaseCurrency().getSymbol(), run.getBalanceDate())));
		exposure.setActualContracts(BigDecimal.ZERO);
	}


	private InvestmentReplicationAllocationHistory validateNewPortfolioRunLDIExposureFindAllocation(PortfolioRunLDIExposure exposure, InvestmentSecurity security, List<PortfolioRunLDIExposure> fullRunExposureList, boolean throwExceptionIfNotValid) {
		List<PortfolioRunLDIExposure> filteredList = BeanUtils.filter(fullRunExposureList, PortfolioRunLDIExposure::getSecurity, security);
		filteredList = BeanUtils.filter(filteredList, PortfolioRunLDIExposure::getReplication, exposure.getReplication());
		filteredList = BeanUtils.filter(filteredList, PortfolioRunLDIExposure::getTradingClientAccount, exposure.getTradingClientAccount());

		InvestmentReplicationAllocationHistory allocationHistoryToReturn = null;

		try {
			ValidationUtils.assertEmpty(filteredList, "The security selected already exists for client account [" + exposure.getTradingClientAccount().getLabel() + "] and replication ["
					+ exposure.getReplication().getName() + "]");

			List<InvestmentReplicationAllocationHistory> allocationHistoryList = getInvestmentReplicationHistoryService().getInvestmentReplicationHistory(exposure.getReplicationAllocationHistory().getReplicationHistory().getId(), true).getReplicationAllocationHistoryList();
			// Need to check to see if instrument for security is one of allowed instruments for this replication
			for (InvestmentReplicationAllocationHistory allocationHistory : CollectionUtils.getIterable(allocationHistoryList)) {
				if (getInvestmentReplicationService().isInvestmentReplicationAllocationSecurityValid(allocationHistory.getReplicationAllocation(), exposure.getPortfolioRun().getBalanceDate(), security)) {
					allocationHistoryToReturn = allocationHistory;
					break;
				}
			}

			ValidationUtils.assertNotNull(allocationHistoryToReturn, "The selected security is not valid for Replication [" + exposure.getReplication().getLabel() + "].");
		}
		catch (ValidationException e) {
			if (throwExceptionIfNotValid) {
				throw e;
			}
			return null;
		}
		return allocationHistoryToReturn;
	}


	private void createNewKrdPoints(PortfolioRunLDIExposure currentExposure, InvestmentSecurity security, PortfolioRunLDIExposure newExposure) {

		List<PortfolioRunLDIExposureKeyRateDurationPoint> krdPoints = currentExposure.getKeyRateDurationPointList();
		List<PortfolioAccountKeyRateDurationPoint> portfolioAccountKeyRateDurationPointList = krdPoints.stream().map(PortfolioRunLDIExposureKeyRateDurationPoint::getKeyRateDurationPoint).collect(Collectors.toList());
		PortfolioAccountKeyRateDurationSecurity portfolioAccountKeyRateDurationSecurity = getPortfolioAccountLDIService().calculatePortfolioAccountKeyRateDurationForSecurity(portfolioAccountKeyRateDurationPointList, security, newExposure.getKrdValueDate(), null, false);

		List<PortfolioRunLDIExposureKeyRateDurationPoint> newKrdPointsList = new ArrayList<>();

		for (Map.Entry<PortfolioAccountKeyRateDurationPoint, BigDecimal> pointEntry : portfolioAccountKeyRateDurationSecurity.getKrdValueMap().entrySet()) {
			PortfolioRunLDIExposureKeyRateDurationPoint newPoint = new PortfolioRunLDIExposureKeyRateDurationPoint();
			newPoint.setKeyRateDurationPoint(pointEntry.getKey());
			newPoint.setKrdValue(pointEntry.getValue());
			newPoint.setExposure(newExposure);
			newPoint.setDv01ContractValue(newPoint.getDv01ContractValueCalculated());
			newKrdPointsList.add(newPoint);
		}

		savePortfolioRunLDIExposureKeyRateDurationPointList(newKrdPointsList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortfolioRunLDIManagerAccount, Criteria> getPortfolioRunLDIManagerAccountDAO() {
		return this.portfolioRunLDIManagerAccountDAO;
	}


	public void setPortfolioRunLDIManagerAccountDAO(AdvancedUpdatableDAO<PortfolioRunLDIManagerAccount, Criteria> portfolioRunLDIManagerAccountDAO) {
		this.portfolioRunLDIManagerAccountDAO = portfolioRunLDIManagerAccountDAO;
	}


	public AdvancedUpdatableDAO<PortfolioRunLDIManagerAccountKeyRateDurationPoint, Criteria> getPortfolioRunLDIManagerAccountKeyRateDurationPointDAO() {
		return this.portfolioRunLDIManagerAccountKeyRateDurationPointDAO;
	}


	public void setPortfolioRunLDIManagerAccountKeyRateDurationPointDAO(
			AdvancedUpdatableDAO<PortfolioRunLDIManagerAccountKeyRateDurationPoint, Criteria> portfolioRunLDIManagerAccountKeyRateDurationPointDAO) {
		this.portfolioRunLDIManagerAccountKeyRateDurationPointDAO = portfolioRunLDIManagerAccountKeyRateDurationPointDAO;
	}


	public AdvancedUpdatableDAO<PortfolioRunLDIKeyRateDurationPoint, Criteria> getPortfolioRunLDIKeyRateDurationPointDAO() {
		return this.portfolioRunLDIKeyRateDurationPointDAO;
	}


	public void setPortfolioRunLDIKeyRateDurationPointDAO(AdvancedUpdatableDAO<PortfolioRunLDIKeyRateDurationPoint, Criteria> portfolioRunLDIKeyRateDurationPointDAO) {
		this.portfolioRunLDIKeyRateDurationPointDAO = portfolioRunLDIKeyRateDurationPointDAO;
	}


	public AdvancedUpdatableDAO<PortfolioRunLDIExposure, Criteria> getPortfolioRunLDIExposureDAO() {
		return this.portfolioRunLDIExposureDAO;
	}


	public void setPortfolioRunLDIExposureDAO(AdvancedUpdatableDAO<PortfolioRunLDIExposure, Criteria> portfolioRunLDIExposureDAO) {
		this.portfolioRunLDIExposureDAO = portfolioRunLDIExposureDAO;
	}


	public AdvancedUpdatableDAO<PortfolioRunLDIExposureKeyRateDurationPoint, Criteria> getPortfolioRunLDIExposureKeyRateDurationPointDAO() {
		return this.portfolioRunLDIExposureKeyRateDurationPointDAO;
	}


	public void setPortfolioRunLDIExposureKeyRateDurationPointDAO(AdvancedUpdatableDAO<PortfolioRunLDIExposureKeyRateDurationPoint, Criteria> portfolioRunLDIExposureKeyRateDurationPointDAO) {
		this.portfolioRunLDIExposureKeyRateDurationPointDAO = portfolioRunLDIExposureKeyRateDurationPointDAO;
	}


	public DaoCompositeKeyListCache<PortfolioRunLDIManagerAccountKeyRateDurationPoint, Integer, Boolean> getPortfolioRunLDIManagerAccountKeyRateDurationPointByManagerAccountCache() {
		return this.portfolioRunLDIManagerAccountKeyRateDurationPointByManagerAccountCache;
	}


	public void setPortfolioRunLDIManagerAccountKeyRateDurationPointByManagerAccountCache(DaoCompositeKeyListCache<PortfolioRunLDIManagerAccountKeyRateDurationPoint, Integer, Boolean> portfolioRunLDIManagerAccountKeyRateDurationPointByManagerAccountCache) {
		this.portfolioRunLDIManagerAccountKeyRateDurationPointByManagerAccountCache = portfolioRunLDIManagerAccountKeyRateDurationPointByManagerAccountCache;
	}


	public DaoCompositeKeyListCache<PortfolioRunLDIExposureKeyRateDurationPoint, Integer, Boolean> getPortfolioRunLDIExposureKeyRateDurationPointByExposureCache() {
		return this.portfolioRunLDIExposureKeyRateDurationPointByExposureCache;
	}


	public void setPortfolioRunLDIExposureKeyRateDurationPointByExposureCache(DaoCompositeKeyListCache<PortfolioRunLDIExposureKeyRateDurationPoint, Integer, Boolean> portfolioRunLDIExposureKeyRateDurationPointByExposureCache) {
		this.portfolioRunLDIExposureKeyRateDurationPointByExposureCache = portfolioRunLDIExposureKeyRateDurationPointByExposureCache;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentReplicationHistoryService getInvestmentReplicationHistoryService() {
		return this.investmentReplicationHistoryService;
	}


	public void setInvestmentReplicationHistoryService(InvestmentReplicationHistoryService investmentReplicationHistoryService) {
		this.investmentReplicationHistoryService = investmentReplicationHistoryService;
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public PortfolioAccountLDIService getPortfolioAccountLDIService() {
		return this.portfolioAccountLDIService;
	}


	public void setPortfolioAccountLDIService(PortfolioAccountLDIService portfolioAccountLDIService) {
		this.portfolioAccountLDIService = portfolioAccountLDIService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}
}
