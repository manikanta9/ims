package com.clifton.portfolio.run.trade;


import com.clifton.core.beans.SimpleEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseType;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PortfolioRunTradeDetail</code> ...
 *
 * @author manderson
 */
public interface PortfolioRunTradeDetail extends SimpleEntity<Integer> {

	public String getTableName();


	/**
	 * Method to clear all fields not persisted in the database.
	 * Called when loading the details for Trade Creation because if called multiple times in the same transaction
	 * current and pending will be doubled, tripled, etc
	 */
	public void clearNonPersistentFields();


	public PortfolioRun getPortfolioRun();


	public void setPortfolioRun(PortfolioRun portfolioRun);


	public InvestmentAccount getTradingClientAccount();


	public InvestmentSecurity getSecurity();


	public InvestmentAccountAssetClass getAccountAssetClass();


	public boolean isTradeEntryDisabled();


	public BigDecimal calculateSuggestedTradeQuantity();


	public InvestmentReplication getReplication();


	public InvestmentReplicationAllocationHistory getReplicationAllocationHistory();


	public BigDecimal getSecurityPrice();


	public BigDecimal getExchangeRate();


	public void setTrade(Trade trade);


	public Trade getTrade();


	public BigDecimal getTradeSecurityPrice();


	public void setTradeSecurityPrice(BigDecimal tradeSecurityPrice);


	public Date getTradeSecurityPriceDate();


	public void setTradeSecurityPriceDate(Date date);


	public BigDecimal getTradeExchangeRate();


	public void setTradeExchangeRate(BigDecimal tradeExchangeRate);


	public Date getTradeExchangeRateDate();


	public void setTradeExchangeRateDate(Date date);


	public BigDecimal getBuyContracts();


	public void setBuyContracts(BigDecimal buyContracts);


	public BigDecimal getSellContracts();


	public void setSellContracts(BigDecimal sellContracts);


	public TradeOpenCloseType getOpenCloseType();


	public void setOpenCloseType(TradeOpenCloseType openCloseType);


	public BigDecimal getActualContracts();


	public void setActualContracts(BigDecimal actualContracts);


	public BigDecimal getPendingContracts();


	public void setPendingContracts(BigDecimal pendingContracts);


	public BigDecimal getCurrentContracts();


	public void setCurrentContracts(BigDecimal currentContracts);


	public Integer getDaysToSettle();


	public void setDaysToSettle(Integer daysToSettle);


	public Boolean getClosePosition();


	public void setClosePosition(Boolean closePosition);


	public BigDecimal getTradeValue();


	public void setTradeValue(BigDecimal tradeValue);
}
