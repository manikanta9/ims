package com.clifton.portfolio.run.trade.aggregate;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.replication.aggregate.PortfolioAggregateSecurityReplication;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * <code>PortfolioAggregateRunTradeService</code> defines the business methods for working with the run aggregate trade creation screen.
 * Most services mimic those in {@link com.clifton.portfolio.run.trade.PortfolioRunTradeService}, and in simplest form, decorates its
 * behavior with aggregation logic.
 *
 * @author nickk
 */
public interface PortfolioAggregateRunTradeService<D extends BasePortfolioRunReplication> {

	/**
	 * Returns {@link PortfolioAggregateRunTrade} object with aggregate replication list populated for trade creation/review
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public PortfolioAggregateRunTrade<D> getPortfolioAggregateRunTrade(int id);


	/**
	 * Returns {@link PortfolioAggregateSecurityReplication} for the run and security.
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public PortfolioAggregateSecurityReplication<D> getPortfolioAggregateRunTradeReplication(int runId, int securityId);


	/**
	 * Refreshes Prices to latest Bloomberg or Previous Night Close
	 *
	 * @param reloadDetails - Called to fully reload detail records when new rows are added
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public PortfolioAggregateRunTrade<D> getPortfolioAggregateRunTradePrices(PortfolioAggregateRunTrade<D> runTrade, boolean livePrices, boolean reloadDetails);


	/**
	 * Refreshes Pending/Actual trades to most recent reflected in the database
	 * Whatever prices are passed are left as they are.
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public PortfolioAggregateRunTrade<D> getPortfolioAggregateRunTradeAmounts(PortfolioAggregateRunTrade<D> runTrade);


	/**
	 * Saves Trades entered for a run based on buys/sells entered for detail in the detailList
	 * <p>
	 * Returns {@link TradeGroup} that was just created for submitted trades
	 */
	@SecureMethod(dtoClass = Trade.class)
	public TradeGroup processPortfolioAggregateRunTradeCreation(PortfolioAggregateRunTrade<D> runTrade);


	/**
	 * Submits all pending trades to Approved workflow state
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = Trade.class, permissions = SecurityPermission.PERMISSION_READ)
	public String processPortfolioAggregateRunTradeApproval(int runId, Integer[] detailIds);


	/**
	 * Returns a list of Pending Trades for a specific detail record.
	 * NOTE: Does NOT perform any contract splitting
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public List<Trade> getPortfolioAggregateRunTradeDetailPendingTradeList(int runId, int detailId);


	/**
	 * Returns a list of Pending Transfers for a specific detail record.
	 * NOTE: Does NOT perform any contract splitting
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public List<AccountingPositionTransfer> getPortfolioAggregateRunTradeDetailPendingTransferList(int runId, int detailId);


	/**
	 * Returns a list of valid holding accounts for a Detail record
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public List<InvestmentAccount> getPortfolioAggregateRunTradeDetailHoldingInvestmentAccountList(int runId, int detailId);
}
