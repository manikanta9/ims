package com.clifton.portfolio.run;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.portfolio.account.PortfolioAccountContractStoreTypes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;


/**
 * The <code>PortfolioCurrencyOtherConfig</code> holds a list of currency replications and for a list of
 * currency other and processes/applies currency amounts to the currency replications appropriately
 *
 * @author manderson
 */
public class PortfolioReplicationCurrencyOtherConfig<T extends BasePortfolioRunReplication> {

	private final PortfolioAccountContractStoreTypes type;
	private final InvestmentSecurity baseCurrency;

	private List<T> longCcyReplicationList;
	private List<T> shortCcyReplicationList;

	// Maps the CCY to the list (likely only one) of replications where the CCY amount should be applied as ccy denom amount, not ccy other
	// Rule is: Underlying CCY of the Replication = Account Base CCY, then replication gets the CCY amount from the ccy denom value.
	// i.e. CAD Based Account, with CAD CCY Future (Trading CCY = USD), any USD CCY amounts are applied as CCY Denom base amount, not applied to all as CCY Other
	private Map<InvestmentSecurity, List<T>> ccyDenomRepMap;

	//////////////////////////////////////////////////////////////


	public PortfolioReplicationCurrencyOtherConfig(PortfolioAccountContractStoreTypes type, InvestmentSecurity baseCurrency, List<T> replicationList) {
		this.type = type;
		this.baseCurrency = baseCurrency;
		setupReplications(replicationList);
	}


	private void setupReplications(List<T> replicationList) {
		List<T> filteredList = BeanUtils.filter(replicationList, replication -> !replication.getReplicationType().isDoNotApplyCurrencyOtherAllocation());

		this.longCcyReplicationList = new ArrayList<>();
		this.shortCcyReplicationList = new ArrayList<>();
		this.ccyDenomRepMap = new HashMap<>();

		// No Currency Replications = Nothing to Allocate
		if (CollectionUtils.isEmpty(filteredList)) {
			return;
		}

		for (T curRep : CollectionUtils.getIterable(filteredList)) {
			if (curRep.getUnderlyingSecurity() != null && curRep.getUnderlyingSecurity().equals(this.baseCurrency)) {
				InvestmentSecurity ccyDenom = curRep.getSecurity().getInstrument().getTradingCurrency();
				//System.out.println("Adding " + ccyDenom.getSymbol() + " as denom.");
				this.ccyDenomRepMap.computeIfAbsent(ccyDenom, ccy -> new ArrayList<>())
						.add(curRep);
			}
			if (MathUtils.isLessThan(curRep.getTargetContractsAdjusted(), BigDecimal.ZERO)) {
				this.shortCcyReplicationList.add(curRep);
			}
			else {
				this.longCcyReplicationList.add(curRep);
			}
		}
	}

	//////////////////////////////////////////////////////////////


	public void applyCurrencyOtherToReplications(List<PortfolioCurrencyOther> currencyOtherList) {
		// No Currency Other to apply or No Currency Replications to Apply them to - Do nothing
		if (CollectionUtils.isEmpty(currencyOtherList) || (CollectionUtils.isEmpty(this.longCcyReplicationList) && CollectionUtils.isEmpty(this.shortCcyReplicationList))) {
			return;
		}

		BigDecimal ccyOtherTotal = BigDecimal.ZERO;

		for (PortfolioCurrencyOther curOther : CollectionUtils.getIterable(currencyOtherList)) {
			BigDecimal ccyTotal = getCurrencyOtherBaseAmount(curOther);
			// If need to apply CCY Denom - apply to "Current" Contract only where possible (i.e. rolling contracts)
			// Otherwise we can proportionally split based on targets and same CCY Denom.
			if (this.ccyDenomRepMap.containsKey(curOther.getCurrencySecurity())) {
				curOther.setCurrencyDenominationAllocation(true);
				List<T> repList = this.ccyDenomRepMap.get(curOther.getCurrencySecurity());
				BigDecimal totalTarget = CoreMathUtils.sumProperty(repList, T::getAllocationWeightAdjusted);
				for (T rep : repList) {
					BigDecimal value = MathUtils.getPercentageOf(CoreMathUtils.getPercentValue(rep.getAllocationWeightAdjusted(), totalTarget), ccyTotal);
					getReplicationCurrencyAmountSetter(true).accept(rep, value);
				}
				CoreMathUtils.applySumPropertyDifference(repList, getReplicationCurrencyAmountGetter(true), getReplicationCurrencyAmountSetter(true), ccyTotal, true);
			}
			else {
				ccyOtherTotal = MathUtils.add(ccyOtherTotal, ccyTotal);
			}
		}

		// Then apply CCY Other proportionally.  Attempt to apply Long/Long and Short/Short where possible
		List<T> applyToList;
		if (MathUtils.isLessThan(ccyOtherTotal, BigDecimal.ZERO)) {
			applyToList = (!CollectionUtils.isEmpty(this.shortCcyReplicationList) ? this.shortCcyReplicationList : this.longCcyReplicationList);
		}
		else {
			applyToList = (!CollectionUtils.isEmpty(this.longCcyReplicationList) ? this.longCcyReplicationList : this.shortCcyReplicationList);
		}

		BigDecimal totalTarget = CoreMathUtils.sumProperty(applyToList, T::getAllocationWeightAdjusted);
		for (T rep : applyToList) {
			BigDecimal value = MathUtils.getPercentageOf(CoreMathUtils.getPercentValue(rep.getAllocationWeightAdjusted(), totalTarget), ccyOtherTotal);
			getReplicationCurrencyAmountSetter(false).accept(rep, value);
		}
		CoreMathUtils.applySumPropertyDifference(applyToList, getReplicationCurrencyAmountGetter(false), getReplicationCurrencyAmountSetter(false), ccyOtherTotal, true);
	}


	/**
	 * Based on Actual, Pending, Current - Get Correct Field Value
	 */
	private Function<T, BigDecimal> getReplicationCurrencyAmountGetter(boolean currencyDenomination) {
		if (PortfolioAccountContractStoreTypes.ACTUAL == this.type) {
			return (currencyDenomination) ? T::getCurrencyDenominationBaseAmount : T::getCurrencyOtherBaseAmount;
		}
		else if (PortfolioAccountContractStoreTypes.PENDING == this.type) {
			return (currencyDenomination) ? T::getCurrencyPendingDenominationBaseAmount : T::getCurrencyPendingOtherBaseAmount;
		}
		return (currencyDenomination) ? T::getCurrencyCurrentDenominationBaseAmount : T::getCurrencyCurrentOtherBaseAmount;
	}


	/**
	 * Based on Actual, Pending, Current - Set Correct Field Value
	 */
	private BiConsumer<T, BigDecimal> getReplicationCurrencyAmountSetter(boolean currencyDenomination) {
		if (PortfolioAccountContractStoreTypes.ACTUAL == this.type) {
			return (currencyDenomination) ? T::setCurrencyDenominationBaseAmount : T::setCurrencyOtherBaseAmount;
		}
		else if (PortfolioAccountContractStoreTypes.PENDING == this.type) {
			return (currencyDenomination) ? T::setCurrencyPendingDenominationBaseAmount : T::setCurrencyPendingOtherBaseAmount;
		}
		return (currencyDenomination) ? T::setCurrencyCurrentDenominationBaseAmount : T::setCurrencyCurrentOtherBaseAmount;
	}


	private BigDecimal getCurrencyOtherBaseAmount(PortfolioCurrencyOther currencyOther) {
		if (PortfolioAccountContractStoreTypes.ACTUAL == this.type) {
			return currencyOther.getTotalBaseAmount();
		}
		else if (PortfolioAccountContractStoreTypes.PENDING == this.type) {
			return currencyOther.getPendingBaseAmount();
		}
		return currencyOther.getTotalCurrentBaseAmount();
	}
}
