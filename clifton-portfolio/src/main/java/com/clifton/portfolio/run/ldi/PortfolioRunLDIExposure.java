package com.clifton.portfolio.run.ldi;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.trade.PortfolioRunTradeDetail;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunLDIExposure</code> contains security details (security, target, actual contracts held) of positions held, or should hold for the run.
 * <p>
 * <p>
 * Phase III of LDI KRD Processing
 *
 * @author manderson
 */
public class PortfolioRunLDIExposure extends BaseSimpleEntity<Integer> implements LabeledObject, PortfolioRunTradeDetail {

	public static final String TABLE_NAME = "PortfolioRunLDIExposure";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private PortfolioRun portfolioRun;

	private InvestmentReplicationAllocationHistory replicationAllocationHistory;

	private InvestmentSecurity security;

	private boolean currentSecurity;

	private BigDecimal securityPrice;

	/**
	 * If populated will be used for Exposure DV01 value calculations
	 * Will be populated when the replicationAllocation supports Dirty Price
	 */
	private BigDecimal securityDirtyPrice;

	private BigDecimal exchangeRate;

	private BigDecimal actualContracts;

	private Date krdValueDate;

	/**
	 * Client Account this position is held/traded in.  This is populated automatically by the system during processing
	 * and determined by any sub-account relationships to the main account the run is for.
	 * <p>
	 * If the positionInvestmentAccount = the account the run is for, this field is not set.
	 * <p>
	 * Calculated getter getTradingClientAccount returns the actual client account used to lookup positions and trade in
	 */
	private InvestmentAccount positionInvestmentAccount;

	private List<PortfolioRunLDIExposureKeyRateDurationPoint> keyRateDurationPointList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The following fields are used for Trade Generation
	 * Buy and Sell Contracts are set explicitly on the replication itself
	 * and during saving process is populate on the trade as quantityIntended & trade.buy
	 * NOT PERSISTED IN THE DB.  After save, it's cleared and values are moved to pending contracts
	 */
	@NonPersistentField
	private Trade trade;
	@NonPersistentField
	private BigDecimal buyContracts; // trade.quantityIntended if trade.buy = true
	@NonPersistentField
	private BigDecimal sellContracts; // trade.quantityIntended if trade.buy = false;
	@NonPersistentField
	private TradeOpenCloseType openCloseType; // trade.openCloseType to specify the transaction's position impact
	@NonPersistentField
	private Integer daysToSettle; //  Not used by LDI yet, but for Overlay group trade screen used to override the settlement date calculation for the trade
	@NonPersistentField
	private Boolean closePosition;
	@NonPersistentField
	private BigDecimal tradeValue; // contract value for the exposure; not applicable with unit based security replications

	// Other Not Closed Trades (Not Booked & Not Cancelled)
	@NonPersistentField
	private BigDecimal pendingContracts;
	// Existing Positions as of next business day (those trades that have posted since EOD previous business day)
	@NonPersistentField
	private BigDecimal currentContracts;

	@NonPersistentField
	private BigDecimal tradeSecurityPrice;
	@NonPersistentField
	private Date tradeSecurityPriceDate;

	@NonPersistentField
	private BigDecimal tradeExchangeRate;
	@NonPersistentField
	private Date tradeExchangeRateDate;

	@NonPersistentField
	private BigDecimal targetContracts;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getSecurity() != null) {
			return getSecurity().getLabel();
		}
		return null;
	}


	public Map<String, BigDecimal> getKeyRateDurationPointMap() {
		Map<String, BigDecimal> vMap = new HashMap<>();
		for (PortfolioRunLDIExposureKeyRateDurationPoint point : CollectionUtils.getIterable(getKeyRateDurationPointList())) {
			vMap.put("KRD_" + point.getKeyRateDurationPoint().getId(), point.getKrdValue());
			vMap.put("DV01_" + point.getKeyRateDurationPoint().getId(), point.getDv01ActualExposureValue());
			// Used for Trade Creation so we can recalc dv01 value based on different quantities/prices
			vMap.put("DV01Contract_" + point.getKeyRateDurationPoint().getId(), point.getDv01ContractValue());
			vMap.put("TradeDV01Contract_" + point.getKeyRateDurationPoint().getId(), point.getTradeDv01ContractValue());
		}
		return vMap;
	}


	@Override
	public InvestmentAccount getTradingClientAccount() {
		if (getPositionInvestmentAccount() != null) {
			return getPositionInvestmentAccount();
		}
		if (getPortfolioRun() != null) {
			return getPortfolioRun().getClientInvestmentAccount();
		}
		return null;
	}


	public InvestmentReplicationAllocation getReplicationAllocation() {
		if (getReplicationAllocationHistory() != null) {
			return getReplicationAllocationHistory().getReplicationAllocation();
		}
		return null;
	}


	@Override
	public InvestmentReplication getReplication() {
		if (getReplicationAllocation() != null) {
			return getReplicationAllocation().getReplication();
		}
		return null;
	}


	@Override
	public InvestmentAccountAssetClass getAccountAssetClass() {
		// Doesn't apply to LDI Runs
		return null;
	}


	@Override
	public boolean isTradeEntryDisabled() {
		// Not currently used for LDI Runs
		return false;
	}


	@Override
	public BigDecimal calculateSuggestedTradeQuantity() {
		// Calculation implemented externally in PortfolioRunLDITargetContractCalculator, because target quantities can only be done given a full set of PortfolioRunLDIExposure entities.
		// The targetContracts property will be populated with target contract data after an external process sets it to the value returned by the calculator.
		return getTargetContracts();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getTableName() {
		return TABLE_NAME;
	}


	@Override
	public void clearNonPersistentFields() {
		this.trade = null;
		this.buyContracts = null;
		this.sellContracts = null;
		this.openCloseType = null;
		this.daysToSettle = null;
		this.pendingContracts = null;
		this.currentContracts = null;
		this.tradeSecurityPrice = null;
		this.tradeSecurityPriceDate = null;
		this.tradeExchangeRate = null;
		this.tradeExchangeRateDate = null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	@Override
	public void setPortfolioRun(PortfolioRun portfolioRun) {
		this.portfolioRun = portfolioRun;
	}


	@Override
	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	@Override
	public BigDecimal getActualContracts() {
		return this.actualContracts;
	}


	@Override
	public void setActualContracts(BigDecimal actualContracts) {
		this.actualContracts = actualContracts;
	}


	public List<PortfolioRunLDIExposureKeyRateDurationPoint> getKeyRateDurationPointList() {
		return this.keyRateDurationPointList;
	}


	public void setKeyRateDurationPointList(List<PortfolioRunLDIExposureKeyRateDurationPoint> keyRateDurationPointList) {
		this.keyRateDurationPointList = keyRateDurationPointList;
	}


	@Override
	public BigDecimal getSecurityPrice() {
		return this.securityPrice;
	}


	public void setSecurityPrice(BigDecimal securityPrice) {
		this.securityPrice = securityPrice;
	}


	@Override
	public InvestmentReplicationAllocationHistory getReplicationAllocationHistory() {
		return this.replicationAllocationHistory;
	}


	public void setReplicationAllocationHistory(InvestmentReplicationAllocationHistory replicationAllocationHistory) {
		this.replicationAllocationHistory = replicationAllocationHistory;
	}


	public boolean isCurrentSecurity() {
		return this.currentSecurity;
	}


	public void setCurrentSecurity(boolean currentSecurity) {
		this.currentSecurity = currentSecurity;
	}


	@Override
	public Trade getTrade() {
		return this.trade;
	}


	@Override
	public void setTrade(Trade trade) {
		this.trade = trade;
	}


	@Override
	public BigDecimal getBuyContracts() {
		return this.buyContracts;
	}


	@Override
	public void setBuyContracts(BigDecimal buyContracts) {
		this.buyContracts = buyContracts;
	}


	@Override
	public BigDecimal getSellContracts() {
		return this.sellContracts;
	}


	@Override
	public void setSellContracts(BigDecimal sellContracts) {
		this.sellContracts = sellContracts;
	}


	@Override
	public TradeOpenCloseType getOpenCloseType() {
		return this.openCloseType;
	}


	@Override
	public void setOpenCloseType(TradeOpenCloseType openCloseType) {
		this.openCloseType = openCloseType;
	}


	@Override
	public BigDecimal getPendingContracts() {
		return this.pendingContracts;
	}


	@Override
	public void setPendingContracts(BigDecimal pendingContracts) {
		this.pendingContracts = pendingContracts;
	}


	@Override
	public BigDecimal getCurrentContracts() {
		return this.currentContracts;
	}


	@Override
	public void setCurrentContracts(BigDecimal currentContracts) {
		this.currentContracts = currentContracts;
	}


	@Override
	public BigDecimal getTradeSecurityPrice() {
		return this.tradeSecurityPrice;
	}


	@Override
	public void setTradeSecurityPrice(BigDecimal tradeSecurityPrice) {
		this.tradeSecurityPrice = tradeSecurityPrice;
	}


	@Override
	public Date getTradeSecurityPriceDate() {
		return this.tradeSecurityPriceDate;
	}


	@Override
	public void setTradeSecurityPriceDate(Date tradeSecurityPriceDate) {
		this.tradeSecurityPriceDate = tradeSecurityPriceDate;
	}


	@Override
	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}


	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}


	@Override
	public BigDecimal getTradeExchangeRate() {
		return this.tradeExchangeRate;
	}


	@Override
	public void setTradeExchangeRate(BigDecimal tradeExchangeRate) {
		this.tradeExchangeRate = tradeExchangeRate;
	}


	@Override
	public Date getTradeExchangeRateDate() {
		return this.tradeExchangeRateDate;
	}


	@Override
	public void setTradeExchangeRateDate(Date tradeExchangeRateDate) {
		this.tradeExchangeRateDate = tradeExchangeRateDate;
	}


	public Date getKrdValueDate() {
		return this.krdValueDate;
	}


	public void setKrdValueDate(Date krdValueDate) {
		this.krdValueDate = krdValueDate;
	}


	public InvestmentAccount getPositionInvestmentAccount() {
		return this.positionInvestmentAccount;
	}


	public void setPositionInvestmentAccount(InvestmentAccount positionInvestmentAccount) {
		this.positionInvestmentAccount = positionInvestmentAccount;
	}


	public BigDecimal getSecurityDirtyPrice() {
		return this.securityDirtyPrice;
	}


	public void setSecurityDirtyPrice(BigDecimal securityDirtyPrice) {
		this.securityDirtyPrice = securityDirtyPrice;
	}


	@Override
	public Integer getDaysToSettle() {
		return this.daysToSettle;
	}


	@Override
	public void setDaysToSettle(Integer daysToSettle) {
		this.daysToSettle = daysToSettle;
	}


	@Override
	public Boolean getClosePosition() {
		return this.closePosition;
	}


	@Override
	public void setClosePosition(Boolean closePosition) {
		this.closePosition = closePosition;
	}


	@Override
	public BigDecimal getTradeValue() {
		return this.tradeValue;
	}


	@Override
	public void setTradeValue(BigDecimal tradeValue) {
		this.tradeValue = tradeValue;
	}


	public BigDecimal getTargetContracts() {
		return this.targetContracts;
	}


	public void setTargetContracts(BigDecimal targetContracts) {
		this.targetContracts = targetContracts;
	}
}
