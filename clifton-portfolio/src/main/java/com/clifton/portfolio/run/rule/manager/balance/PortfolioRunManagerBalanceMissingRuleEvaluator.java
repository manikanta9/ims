package com.clifton.portfolio.run.rule.manager.balance;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.portfolio.run.rule.manager.BasePortfolioRunManagerAccountRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.schema.SystemSchemaService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunManagerBalanceMissingRuleEvaluator</code> class is a rule evaluator
 * that generates violations during a PortfolioRun for manager account with missing balances.
 *
 * @author sfahey
 */
public class PortfolioRunManagerBalanceMissingRuleEvaluator extends BasePortfolioRunManagerAccountRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	private SystemSchemaService systemSchemaService;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		List<Integer> managerAccountIds = new ArrayList<>();
		List<InvestmentManagerAccount> investmentManagerAccountList = context.getManagerAccountWithActiveAssignmentList(run);
		for (InvestmentManagerAccount investmentManagerAccount : CollectionUtils.getIterable(investmentManagerAccountList)) {
			EntityConfig entityConfig = getEntityConfig(investmentManagerAccount, ruleConfig);
			if (entityConfig != null && !entityConfig.isExcluded()) {
				managerAccountIds.add(investmentManagerAccount.getId());
			}
		}
		if (!CollectionUtils.isEmpty(managerAccountIds)) {
			Date balanceDate = context.getBalanceDate(run);
			for (InvestmentManagerAccount investmentManagerAccount : CollectionUtils.getIterable(getManagerAccountMissingBalanceList(managerAccountIds, balanceDate))) {
				Map<String, Object> templateValues = new HashMap<>();
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(investmentManagerAccount)),
						BeanUtils.getIdentityAsLong(run), BeanUtils.getIdentityAsLong(investmentManagerAccount), BeanUtils.getIdentityAsLong(investmentManagerAccount), null, templateValues));
			}
		}
		return ruleViolationList;
	}


	private List<InvestmentManagerAccount> getManagerAccountMissingBalanceList(List<Integer> managerAccountIds, Date balanceDate) {
		InvestmentManagerAccountSearchForm investmentManagerAccountSearchForm = new InvestmentManagerAccountSearchForm();
		investmentManagerAccountSearchForm.setManagerAccountIds(managerAccountIds.toArray(new Integer[CollectionUtils.getSize(managerAccountIds)]));
		return getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountMissingBalanceList(investmentManagerAccountSearchForm, balanceDate);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
