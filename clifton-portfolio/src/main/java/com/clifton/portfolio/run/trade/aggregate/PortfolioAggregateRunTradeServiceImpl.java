package com.clifton.portfolio.run.trade.aggregate;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.replication.aggregate.PortfolioAggregateRunReplicationHandler;
import com.clifton.portfolio.replication.aggregate.PortfolioAggregateSecurityReplication;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.trade.PortfolioRunTrade;
import com.clifton.portfolio.run.trade.PortfolioRunTradeService;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author nickk
 */
@Service
public class PortfolioAggregateRunTradeServiceImpl<T extends PortfolioRunTrade<D>, D extends BasePortfolioRunReplication> implements PortfolioAggregateRunTradeService<D> {


	private PortfolioRunTradeService<T, D> portfolioRunTradeService;
	private PortfolioAggregateRunReplicationHandler<D> portfolioAggregateRunReplicationHandler;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioAggregateRunTrade<D> getPortfolioAggregateRunTrade(int id) {
		T intermediateRunTrade = getPortfolioRunTradeService().getPortfolioRunTrade(id);
		return convertPortfolioRunTradeToAggregate(intermediateRunTrade);
	}


	@Override
	public PortfolioAggregateSecurityReplication<D> getPortfolioAggregateRunTradeReplication(int runId, int securityId) {
		PortfolioAggregateRunTrade<D> runTrade = getPortfolioAggregateRunTrade(runId);
		Optional<PortfolioAggregateSecurityReplication<D>> securityReplication = CollectionUtils.getStream(runTrade.getDetailList())
				.filter(replication -> securityId == replication.getSecurity().getId())
				.findFirst();
		if (securityReplication.isPresent()) {
			return securityReplication.get();
		}
		throw new ValidationException(String.format("A replication for run with ID [%d] and security ID [%d] does not exist.", runId, securityId));
	}


	@Override
	public PortfolioAggregateRunTrade<D> getPortfolioAggregateRunTradePrices(PortfolioAggregateRunTrade<D> runTrade, boolean livePrices, boolean reloadDetails) {
		T intermediateRunTrade = getRunTradeWithoutCurrentAndPendingContracts(runTrade.getId());
		intermediateRunTrade.setTradingCurrencyType(runTrade.getTradingCurrencyType());
		intermediateRunTrade = getPortfolioRunTradeService().getPortfolioRunTradePrices(intermediateRunTrade, livePrices, reloadDetails);

		return convertPortfolioRunTradeToAggregate(intermediateRunTrade);
	}


	@Override
	public PortfolioAggregateRunTrade<D> getPortfolioAggregateRunTradeAmounts(PortfolioAggregateRunTrade<D> runTrade) {
		T intermediateRunTrade = getRunTradeWithoutCurrentAndPendingContracts(runTrade.getId());
		intermediateRunTrade.setTradingCurrencyType(runTrade.getTradingCurrencyType());
		intermediateRunTrade = getPortfolioRunTradeService().getPortfolioRunTradeAmounts(intermediateRunTrade);

		return convertPortfolioRunTradeToAggregate(intermediateRunTrade);
	}


	@Override
	public TradeGroup processPortfolioAggregateRunTradeCreation(PortfolioAggregateRunTrade<D> runTrade) {
		T intermediateRunTrade = getPortfolioRunTradeService().getPortfolioRunTrade(runTrade.getId());
		// Validate Run is Available for Trading
		if (intermediateRunTrade.isClosed()) {
			throw new ValidationException("This run has already been marked as completed. There is a newer run available for Trade processing.");
		}
		else if (!intermediateRunTrade.isTradingAllowed()) {
			throw new ValidationException("Unable to submit trades for this run.  Trading is allowed in [Pending] workflow status only.  This run is currently in ["
					+ intermediateRunTrade.getWorkflowStatus().getName() + "].");
		}

		intermediateRunTrade.setTradeDate(runTrade.getTradeDate());
		intermediateRunTrade.setTradingCurrencyType(runTrade.getTradingCurrencyType());
		// Get original aggregate run trade so the details contain the list of basis replications/details that are aggregated
		PortfolioAggregateRunTrade<D> originalRunTrade = convertPortfolioRunTradeToAggregate(intermediateRunTrade);
		List<D> tradeDetails = new ArrayList<>();
		CollectionUtils.getStream(runTrade.getDetailList())
				.filter(replication -> replication.getBuyContracts() != null || replication.getSellContracts() != null || BooleanUtils.isTrue(replication.getClosePosition()))
				.forEach(replication -> {
					Optional<List<D>> detailList = CollectionUtils.getStream(originalRunTrade.getDetailList())
							// replication ID is the security ID, which is the basis of the aggregation
							.filter(r -> r.getSecurity().getId().equals(replication.getId()))
							.findFirst()
							.map(r -> {
								if (BooleanUtils.isTrue(replication.getClosePosition())) {
									// close all underlying replications
									return r.getReplicationList();
								}
								// trade first underlying replication for quantity
								return Collections.singletonList(CollectionUtils.getFirstElement(r.getReplicationList()));
							});
					detailList.ifPresent(detailListToTrade ->
							CollectionUtils.getStream(detailListToTrade)
									.forEach(detail -> {
										if (BooleanUtils.isTrue(replication.getClosePosition())) {
											detail.setClosePosition(replication.getClosePosition());
											// copy trade to have new trade instance for each replication of the aggregate
											Trade tradeCopy = new Trade();
											BeanUtils.copyProperties(replication.getTrade(), tradeCopy);
											detail.setTrade(tradeCopy);
										}
										else {
											detail.setBuyContracts(replication.getBuyContracts());
											detail.setSellContracts(replication.getSellContracts());
											detail.setTrade(replication.getTrade());
										}
										tradeDetails.add(detail);
									}));
				});

		intermediateRunTrade.setDetailList(tradeDetails);
		return getPortfolioRunTradeService().processPortfolioRunTradeCreation(intermediateRunTrade);
	}


	@Override
	public String processPortfolioAggregateRunTradeApproval(int runId, Integer[] detailIds) {
		Integer[] intermediateDetailIds = getRunTradeDetailIdsWithPendingActivity(runId, detailIds);
		return getPortfolioRunTradeService().processPortfolioRunTradeApproval(runId, intermediateDetailIds);
	}


	@Override
	public List<Trade> getPortfolioAggregateRunTradeDetailPendingTradeList(int runId, int detailId) {
		List<D> intermediateDetailList = getRunTradeDetailListWithPendingActivity(runId, detailId);
		return CollectionUtils.getStream(intermediateDetailList)
				.map(detail -> getPortfolioRunTradeService().getPortfolioRunTradeDetailPendingTradeList(runId, detail.getId()))
				.flatMap(CollectionUtils::getStream)
				.collect(Collectors.toList());
	}


	@Override
	public List<AccountingPositionTransfer> getPortfolioAggregateRunTradeDetailPendingTransferList(int runId, int detailId) {
		List<D> intermediateDetailList = getRunTradeDetailListForSecurityIds(runId, detailId);
		return CollectionUtils.getStream(intermediateDetailList)
				.map(detail -> getPortfolioRunTradeService().getPortfolioRunTradeDetailPendingTransferList(runId, detail.getId()))
				.flatMap(CollectionUtils::getStream)
				.collect(Collectors.toList());
	}


	@Override
	public List<InvestmentAccount> getPortfolioAggregateRunTradeDetailHoldingInvestmentAccountList(int runId, int detailId) {
		List<D> intermediateDetailList = getRunTradeDetailListForSecurityIds(runId, detailId);
		return CollectionUtils.getStream(intermediateDetailList)
				.map(detail -> getPortfolioRunTradeService().getPortfolioRunTradeDetailHoldingInvestmentAccountList(runId, detail.getId()))
				.flatMap(CollectionUtils::getStream)
				.collect(Collectors.toList());
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private T getRunTradeWithoutCurrentAndPendingContracts(int runId) {
		T intermediateRunTrade = getPortfolioRunTradeService().getPortfolioRunTrade(runId);
		// May need to something better here to prevent doubling current and pending contracts
		CollectionUtils.getIterable(intermediateRunTrade.getDetailList())
				.forEach(replication -> {
					replication.setCurrentContracts(BigDecimal.ZERO);
					replication.setPendingContracts(BigDecimal.ZERO);
				});
		return intermediateRunTrade;
	}


	private List<D> getRunTradeDetailListForSecurityIds(int runId, Integer... securityIds) {
		T intermediateRunTrade = getPortfolioRunTradeService().getPortfolioRunTrade(runId);
		Set<Integer> securityIdSet = ArrayUtils.getStream(securityIds)
				.collect(Collectors.toSet());
		return CollectionUtils.getStream(intermediateRunTrade.getDetailList())
				.filter(replication -> securityIdSet.contains(replication.getSecurity().getId()))
				.collect(Collectors.toList());
	}


	private List<D> getRunTradeDetailListWithPendingActivity(int runId, Integer... securityIds) {
		return CollectionUtils.getStream(getRunTradeDetailListForSecurityIds(runId, securityIds))
				.filter(replication -> !MathUtils.isNullOrZero(replication.getPendingContracts()))
				.collect(Collectors.toList());
	}


	private Integer[] getRunTradeDetailIdsWithPendingActivity(int runId, Integer... securityIds) {
		return CollectionUtils.getStream(getRunTradeDetailListWithPendingActivity(runId, securityIds))
				.map(BaseSimpleEntity::getId)
				.toArray(Integer[]::new);
	}


	private PortfolioAggregateRunTrade<D> convertPortfolioRunTradeToAggregate(PortfolioRunTrade<D> intermediateRunTrade) {
		PortfolioAggregateRunTrade<D> runTrade = PortfolioAggregateRunTrade.forRunAndType(intermediateRunTrade);

		List<PortfolioAggregateSecurityReplication<D>> aggregateSecurityReplication = getPortfolioAggregateRunReplicationHandler().getPortfolioAggregateRunReplication(intermediateRunTrade.getDetailList());

		runTrade.setDetailList(aggregateSecurityReplication);

		return runTrade;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunTradeService<T, D> getPortfolioRunTradeService() {
		return this.portfolioRunTradeService;
	}


	public void setPortfolioRunTradeService(PortfolioRunTradeService<T, D> portfolioRunTradeService) {
		this.portfolioRunTradeService = portfolioRunTradeService;
	}


	public PortfolioAggregateRunReplicationHandler<D> getPortfolioAggregateRunReplicationHandler() {
		return this.portfolioAggregateRunReplicationHandler;
	}


	public void setPortfolioAggregateRunReplicationHandler(PortfolioAggregateRunReplicationHandler<D> portfolioAggregateRunReplicationHandler) {
		this.portfolioAggregateRunReplicationHandler = portfolioAggregateRunReplicationHandler;
	}
}
