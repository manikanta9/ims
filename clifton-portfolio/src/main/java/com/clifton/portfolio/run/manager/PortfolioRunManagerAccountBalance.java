package com.clifton.portfolio.run.manager;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.portfolio.run.PortfolioRun;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * {@link PortfolioRunManagerAccountBalance} is a virtual object that contains a combination of information about the run as well as manager balance information and manual
 * adjustments.
 *
 * @author manderson
 * @author michaelm
 */
@NonPersistentObject
public class PortfolioRunManagerAccountBalance extends BaseEntity<Integer> {

	public static final String BALANCE_PRINT_HEADER = "MOC\tManager #\tAllocation\tBase Securities\tBase Cash\tBase Total\tAdjustment Count\tSecurities Adjustment\tCash Adjustment\tAdjusted Securities\tAdjusted Cash\tAdjusted Total\r";
	public static final String BALANCE_PRINT_FORMAT = "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\r";


	private PortfolioRun portfolioRun;
	private InvestmentManagerAccountBalance managerAccountBalance;

	/**
	 * When asset class is used, the amount allocated to the asset class could be different
	 * so we store the amount on the run which can be compared to the actual balance to determine allocation percentages
	 */
	private BigDecimal cashAllocationAmount;
	private BigDecimal securitiesAllocationAmount;
	/**
	 * Total Balance is the total adjusted balance for the manager account
	 * this will include MOC balances if the run includes MOC
	 */
	private BigDecimal totalBalance;


	/**
	 * Cash and Securities Base Balance Information
	 * "Base" balances are the values after automatic adjustments, put prior to manual adjustments
	 */
	private BigDecimal cashBalanceBase;
	private BigDecimal securitiesBalanceBase;

	/**
	 * Cash and Securities Manual Adjustment Totals
	 */
	private BigDecimal cashManualAdjustmentTotalAmount;
	private BigDecimal securitiesManualAdjustmentTotalAmount;


	/**
	 * Manual Adjustments applied to this manager balance
	 * Contains the full manager adjustment allocation (adjusted for children of rollups based on rollup allocations)
	 * However NOT adjusted for asset class allocation percentages - which is addressed via UI
	 */
	private List<InvestmentManagerAccountBalanceAdjustment> manualAdjustmentList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String toStringFormatted(boolean includeHeader) {
		String result = "";
		if (includeHeader) {
			result = BALANCE_PRINT_HEADER;
		}

		return result
				+ String.format(BALANCE_PRINT_FORMAT, //
				getPortfolioRun().isMarketOnCloseAdjustmentsIncluded(), //
				getManagerAccountBalance().getManagerAccount().getAccountNumber(), //
				CoreMathUtils.formatNumberMoney(getAllocationPercent()), //
				CoreMathUtils.formatNumberInteger(getSecuritiesBalanceBaseAllocationAmount()), //
				CoreMathUtils.formatNumberInteger(getCashBalanceBaseAllocationAmount()), //
				CoreMathUtils.formatNumberInteger(getTotalBalanceBaseAllocationAmount()), //
				CollectionUtils.getSize(getManualAdjustmentList()), //
				CoreMathUtils.formatNumberInteger(getSecuritiesManualAdjustmentTotalAllocationAmount()), //
				CoreMathUtils.formatNumberInteger(getCashManualAdjustmentTotalAllocationAmount()), //
				CoreMathUtils.formatNumberInteger(getSecuritiesAllocationAmount()), //
				CoreMathUtils.formatNumberInteger(getCashAllocationAmount()), //
				CoreMathUtils.formatNumberInteger(getTotalAllocationAmount()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addManualAdjustment(InvestmentManagerAccountBalanceAdjustment adjustment) {
		if (this.manualAdjustmentList == null) {
			this.manualAdjustmentList = new ArrayList<>();
		}
		this.manualAdjustmentList.add(adjustment);
		if (!MathUtils.isNullOrZero(adjustment.getCashValue())) {
			this.cashManualAdjustmentTotalAmount = MathUtils.add(this.cashManualAdjustmentTotalAmount, adjustment.getCashValue());
		}
		if (!MathUtils.isNullOrZero(adjustment.getSecuritiesValue())) {
			this.securitiesManualAdjustmentTotalAmount = MathUtils.add(this.securitiesManualAdjustmentTotalAmount, adjustment.getSecuritiesValue());
		}
	}


	public void addChildManualAdjustmentList(BigDecimal allocationPercent, List<InvestmentManagerAccountBalanceAdjustment> adjustmentList) {
		// For each adj, clone it and change cash/securities values based on allocation percentages
		// And back it out of base values
		for (InvestmentManagerAccountBalanceAdjustment adjustment : CollectionUtils.getIterable(adjustmentList)) {
			InvestmentManagerAccountBalanceAdjustment childAdjustment = BeanUtils.cloneBean(adjustment, false, true);
			childAdjustment.setCashValue(MathUtils.getPercentageOf(allocationPercent, adjustment.getCashValue(), true));
			childAdjustment.setSecuritiesValue(MathUtils.getPercentageOf(allocationPercent, adjustment.getSecuritiesValue(), true));

			setCashBalanceBase(MathUtils.subtract(getCashBalanceBase(), childAdjustment.getCashValue()));
			setSecuritiesBalanceBase(MathUtils.subtract(getSecuritiesBalanceBase(), childAdjustment.getSecuritiesValue()));

			addManualAdjustment(childAdjustment);
		}
	}


	public boolean isManuallyAdjusted() {
		return !CollectionUtils.isEmpty(getManualAdjustmentList());
	}


	public BigDecimal getCashBalanceBaseAllocationAmount() {
		return MathUtils.getPercentageOf(getAllocationPercent(), getCashBalanceBase(), true);
	}


	public BigDecimal getSecuritiesBalanceBaseAllocationAmount() {
		return MathUtils.getPercentageOf(getAllocationPercent(), getSecuritiesBalanceBase(), true);
	}


	public BigDecimal getTotalBalanceBaseAllocationAmount() {
		return MathUtils.add(getCashBalanceBaseAllocationAmount(), getSecuritiesBalanceBaseAllocationAmount());
	}


	public BigDecimal getTotalAllocationAmount() {
		return MathUtils.add(getCashAllocationAmount(), getSecuritiesAllocationAmount());
	}


	public BigDecimal getCashManualAdjustmentTotalAllocationAmount() {
		if (!MathUtils.isNullOrZero(this.cashManualAdjustmentTotalAmount)) {
			return MathUtils.getPercentageOf(getAllocationPercent(), this.cashManualAdjustmentTotalAmount, true);
		}
		return null;
	}


	public BigDecimal getSecuritiesManualAdjustmentTotalAllocationAmount() {
		if (!MathUtils.isNullOrZero(this.securitiesManualAdjustmentTotalAmount)) {
			return MathUtils.getPercentageOf(getAllocationPercent(), this.securitiesManualAdjustmentTotalAmount, true);
		}
		return null;
	}


	public BigDecimal getAllocationPercent() {
		if (MathUtils.isNullOrZero(getTotalBalance())) {
			return MathUtils.BIG_DECIMAL_ONE_HUNDRED;
		}
		return CoreMathUtils.getPercentValue(getTotalAllocationAmount(), getTotalBalance(), true);
	}


	public BigDecimal getTotalBalanceBase() {
		return MathUtils.add(getCashBalanceBase(), getSecuritiesBalanceBase());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	public void setPortfolioRun(PortfolioRun portfolioRun) {
		this.portfolioRun = portfolioRun;
	}


	public InvestmentManagerAccountBalance getManagerAccountBalance() {
		return this.managerAccountBalance;
	}


	public void setManagerAccountBalance(InvestmentManagerAccountBalance managerAccountBalance) {
		this.managerAccountBalance = managerAccountBalance;
	}


	public BigDecimal getCashAllocationAmount() {
		return this.cashAllocationAmount;
	}


	public void setCashAllocationAmount(BigDecimal cashAllocationAmount) {
		this.cashAllocationAmount = cashAllocationAmount;
	}


	public BigDecimal getSecuritiesAllocationAmount() {
		return this.securitiesAllocationAmount;
	}


	public void setSecuritiesAllocationAmount(BigDecimal securitiesAllocationAmount) {
		this.securitiesAllocationAmount = securitiesAllocationAmount;
	}


	public BigDecimal getTotalBalance() {
		return this.totalBalance;
	}


	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}


	public void setCashBalanceBase(BigDecimal cashBalanceBase) {
		this.cashBalanceBase = cashBalanceBase;
	}


	public void setSecuritiesBalanceBase(BigDecimal securitiesBalanceBase) {
		this.securitiesBalanceBase = securitiesBalanceBase;
	}


	public BigDecimal getCashBalanceBase() {
		return this.cashBalanceBase;
	}


	public BigDecimal getSecuritiesBalanceBase() {
		return this.securitiesBalanceBase;
	}


	public List<InvestmentManagerAccountBalanceAdjustment> getManualAdjustmentList() {
		return this.manualAdjustmentList;
	}


	public void setManualAdjustmentList(List<InvestmentManagerAccountBalanceAdjustment> manualAdjustmentList) {
		this.manualAdjustmentList = manualAdjustmentList;
	}


	public BigDecimal getCashManualAdjustmentTotalAmount() {
		return this.cashManualAdjustmentTotalAmount;
	}


	public void setCashManualAdjustmentTotalAmount(BigDecimal cashManualAdjustmentTotalAmount) {
		this.cashManualAdjustmentTotalAmount = cashManualAdjustmentTotalAmount;
	}


	public BigDecimal getSecuritiesManualAdjustmentTotalAmount() {
		return this.securitiesManualAdjustmentTotalAmount;
	}


	public void setSecuritiesManualAdjustmentTotalAmount(BigDecimal securitiesManualAdjustmentTotalAmount) {
		this.securitiesManualAdjustmentTotalAmount = securitiesManualAdjustmentTotalAmount;
	}
}
