package com.clifton.portfolio.run.margin;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.portfolio.run.PortfolioRun;

import java.util.List;


/**
 * The <code>PortfolioRunMarginDetail</code> is a simple wrapper object that consolidates the portfolio run and
 * it's associated runMarginList for use in the UI.
 *
 * @author stevenf
 */
@NonPersistentObject
public class PortfolioRunMarginDetail {

	private PortfolioRun portfolioRun;
	private List<PortfolioRunMargin> runMarginList;

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	public void setPortfolioRun(PortfolioRun portfolioRun) {
		this.portfolioRun = portfolioRun;
	}


	public List<PortfolioRunMargin> getRunMarginList() {
		return this.runMarginList;
	}


	public void setRunMarginList(List<PortfolioRunMargin> runMarginList) {
		this.runMarginList = runMarginList;
	}
}
