package com.clifton.portfolio.run.search;


import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.CustomJsonStringAwareSearchForm;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PortfolioRunSearchForm</code> class defines search configuration for PortfolioRun objects.
 *
 * @author Mary Anderson
 */
public class PortfolioRunSearchForm extends BaseWorkflowAwareSearchForm implements CustomJsonStringAwareSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "clientInvestmentAccount.id", sortField = "clientInvestmentAccount.number")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "clientInvestmentAccount.id", sortField = "clientInvestmentAccount.number")
	private Integer[] clientInvestmentAccountIds;

	@SearchField(searchFieldPath = "clientInvestmentAccount", searchField = "teamSecurityGroup.id")
	private Short teamSecurityGroupId;

	@SearchField
	private Date balanceDate;

	@SearchField
	private Boolean mainRun;

	@SearchField
	private BigDecimal portfolioTotalValue;

	@SearchField
	private BigDecimal cashTotal;

	@SearchField
	private BigDecimal overlayTargetTotal;

	@SearchField
	private BigDecimal overlayExposureTotal;

	@SearchField
	private BigDecimal cashTarget;

	@SearchField
	private BigDecimal mispricingTotal;

	@SearchField
	private BigDecimal cashTargetPercent;

	@SearchField
	private BigDecimal cashExposure;

	@SearchField
	private BigDecimal cashExposurePercent;

	@SearchField
	private BigDecimal cashExposureWithMispricingPercent;

	@SearchField(searchFieldPath = "clientInvestmentAccount", searchField = "businessClient.id", sortField = "businessClient.name")
	private Integer clientId;

	@SearchField(searchFieldPath = "clientInvestmentAccount.businessClient", searchField = "name,legalName")
	private String clientName;

	@SearchField(searchFieldPath = "clientInvestmentAccount.businessClient.clientRelationship", searchField = "name,legalName")
	private String clientRelationshipName;

	@SearchField(searchFieldPath = "clientInvestmentAccount", searchField = "number,name", sortField = "number")
	private String investmentAccountLabel;

	@SearchField(searchFieldPath = "clientInvestmentAccount.workflowState", searchField = "name")
	private String investmentAccountWorkflowStateName;

	@SearchField
	private ServiceProcessingTypes serviceProcessingType;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "clientInvestmentAccount.businessService.id,clientInvestmentAccount.businessService.parent.id,clientInvestmentAccount.businessService.parent.parent.id,clientInvestmentAccount.businessService.parent.parent.parent.id", leftJoin = true, sortField = "clientInvestmentAccount.businessService.name")
	private Short businessServiceOrParentId;

	@SearchField
	private Boolean marketOnCloseAdjustmentsIncluded;

	@SearchField(searchField = "violationStatus.id")
	private Short violationStatusId;

	@SearchField(searchFieldPath = "violationStatus", searchField = "name")
	private String[] violationStatusNames;

	// Custom Search Filter
	private Integer investmentAccountGroupId;

	@SearchField
	private Short documentFileCount;

	@SearchField
	private Boolean postedToPortal;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public Integer getClientId() {
		return this.clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public String getInvestmentAccountLabel() {
		return this.investmentAccountLabel;
	}


	public void setInvestmentAccountLabel(String investmentAccountLabel) {
		this.investmentAccountLabel = investmentAccountLabel;
	}


	public Boolean getMarketOnCloseAdjustmentsIncluded() {
		return this.marketOnCloseAdjustmentsIncluded;
	}


	public void setMarketOnCloseAdjustmentsIncluded(Boolean marketOnCloseAdjustmentsIncluded) {
		this.marketOnCloseAdjustmentsIncluded = marketOnCloseAdjustmentsIncluded;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Boolean getMainRun() {
		return this.mainRun;
	}


	public void setMainRun(Boolean mainRun) {
		this.mainRun = mainRun;
	}


	public String getClientName() {
		return this.clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public String getInvestmentAccountWorkflowStateName() {
		return this.investmentAccountWorkflowStateName;
	}


	public void setInvestmentAccountWorkflowStateName(String investmentAccountWorkflowStateName) {
		this.investmentAccountWorkflowStateName = investmentAccountWorkflowStateName;
	}


	public Short getViolationStatusId() {
		return this.violationStatusId;
	}


	public void setViolationStatusId(Short violationStatusId) {
		this.violationStatusId = violationStatusId;
	}


	public String[] getViolationStatusNames() {
		return this.violationStatusNames;
	}


	public void setViolationStatusNames(String[] violationStatusNames) {
		this.violationStatusNames = violationStatusNames;
	}


	public BigDecimal getPortfolioTotalValue() {
		return this.portfolioTotalValue;
	}


	public void setPortfolioTotalValue(BigDecimal portfolioTotalValue) {
		this.portfolioTotalValue = portfolioTotalValue;
	}


	public BigDecimal getCashTotal() {
		return this.cashTotal;
	}


	public void setCashTotal(BigDecimal cashTotal) {
		this.cashTotal = cashTotal;
	}


	public BigDecimal getOverlayTargetTotal() {
		return this.overlayTargetTotal;
	}


	public void setOverlayTargetTotal(BigDecimal overlayTargetTotal) {
		this.overlayTargetTotal = overlayTargetTotal;
	}


	public BigDecimal getOverlayExposureTotal() {
		return this.overlayExposureTotal;
	}


	public void setOverlayExposureTotal(BigDecimal overlayExposureTotal) {
		this.overlayExposureTotal = overlayExposureTotal;
	}


	public BigDecimal getCashTarget() {
		return this.cashTarget;
	}


	public void setCashTarget(BigDecimal cashTarget) {
		this.cashTarget = cashTarget;
	}


	public BigDecimal getCashTargetPercent() {
		return this.cashTargetPercent;
	}


	public void setCashTargetPercent(BigDecimal cashTargetPercent) {
		this.cashTargetPercent = cashTargetPercent;
	}


	public BigDecimal getCashExposure() {
		return this.cashExposure;
	}


	public void setCashExposure(BigDecimal cashExposure) {
		this.cashExposure = cashExposure;
	}


	public BigDecimal getCashExposurePercent() {
		return this.cashExposurePercent;
	}


	public void setCashExposurePercent(BigDecimal cashExposurePercent) {
		this.cashExposurePercent = cashExposurePercent;
	}


	public BigDecimal getMispricingTotal() {
		return this.mispricingTotal;
	}


	public void setMispricingTotal(BigDecimal mispricingTotal) {
		this.mispricingTotal = mispricingTotal;
	}


	public BigDecimal getCashExposureWithMispricingPercent() {
		return this.cashExposureWithMispricingPercent;
	}


	public void setCashExposureWithMispricingPercent(BigDecimal cashExposureWithMispricingPercent) {
		this.cashExposureWithMispricingPercent = cashExposureWithMispricingPercent;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer[] getClientInvestmentAccountIds() {
		return this.clientInvestmentAccountIds;
	}


	public void setClientInvestmentAccountIds(Integer[] clientInvestmentAccountIds) {
		this.clientInvestmentAccountIds = clientInvestmentAccountIds;
	}


	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}


	public ServiceProcessingTypes getServiceProcessingType() {
		return this.serviceProcessingType;
	}


	public void setServiceProcessingType(ServiceProcessingTypes serviceProcessingType) {
		this.serviceProcessingType = serviceProcessingType;
	}


	public String getClientRelationshipName() {
		return this.clientRelationshipName;
	}


	public void setClientRelationshipName(String clientRelationshipName) {
		this.clientRelationshipName = clientRelationshipName;
	}


	public Short getBusinessServiceOrParentId() {
		return this.businessServiceOrParentId;
	}


	public void setBusinessServiceOrParentId(Short businessServiceOrParentId) {
		this.businessServiceOrParentId = businessServiceOrParentId;
	}


	public Boolean getPostedToPortal() {
		return this.postedToPortal;
	}


	public void setPostedToPortal(Boolean postedToPortal) {
		this.postedToPortal = postedToPortal;
	}
}
