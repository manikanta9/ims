package com.clifton.portfolio.run.rule.manager;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorContext;

import java.util.List;


/**
 * The <code>BasePortfolioRunManagerAccountRuleEvaluator</code> class is used to allow rules with an entityTable of 'InvestmentManagerAccount'
 * to allow for an additional level of filtering for rule execution based on the manager account's managing company.  This allows for certain
 * rules to explicitly exclude evaluation for all accounts for which the managing company is 'Parametric Minneapolis', for instance.
 * <p>
 * This class extends the BaseRangeRuleEvaluator, but does not use the 'rangeType' field of it's superclass explicitly, however,
 * it's subclass <code>BasePortfolioRunManagerAccountRangeRuleEvaluator</code> does, and this allows us to avoid duplicating code.
 *
 * @author StevenF
 */
public abstract class BasePortfolioRunManagerAccountRuleEvaluator<T extends IdentityObject, K extends RuleEvaluatorContext> extends BaseRuleEvaluator<T, K> {

	/**
	 * Specifies whether the businessCompanyIds should be used to include or exclude the rule from evaluation.
	 */
	private boolean include;

	/**
	 * A list of manager companies that is used to determine if a rule should or should not be evaluated based on the 'include' field.
	 */
	private List<Integer> businessCompanyIds;


	public EntityConfig getEntityConfig(InvestmentManagerAccount managerAccount, RuleConfig ruleConfig) {
		if (getBusinessCompanyIds() == null || ((isInclude() && getBusinessCompanyIds().contains(managerAccount.getManagerCompany().getId())) ||
				(!isInclude() && !getBusinessCompanyIds().contains(managerAccount.getManagerCompany().getId())))) {
			return ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(managerAccount));
		}
		return null;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public boolean isInclude() {
		return this.include;
	}


	public void setInclude(boolean include) {
		this.include = include;
	}


	public List<Integer> getBusinessCompanyIds() {
		return this.businessCompanyIds;
	}


	public void setBusinessCompanyIds(List<Integer> businessCompanyIds) {
		this.businessCompanyIds = businessCompanyIds;
	}
}
