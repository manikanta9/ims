package com.clifton.portfolio.run.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * ProductOverlayAssetClassPositionAllocationRuleEvaluator class handles various rule violations (determined through properties)
 * to check for explicit position allocation usage
 *
 * @author manderson
 */
public class PortfolioRunAssetClassPositionAllocationRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	/**
	 * The following options are mutually exclusive and determine how the violation is generated
	 */
	private boolean changeSincePrevious; // Excludes from/to zero for rolling contracts
	private boolean unused; // Expired contract should de-activate position allocation
	private boolean unableToFill; // Explicit Allocation doesn't match run


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun ruleBean, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = context.getPositionAllocationListForRun(ruleBean);
			if (!CollectionUtils.isEmpty(positionAllocationList)) {
				if (isChangeSincePrevious()) {
					return evaluateRuleForChangeSincePrevious(ruleBean, positionAllocationList, entityConfig, context);
				}
				else if (isUnused()) {
					return evaluateRuleForUnused(ruleBean, positionAllocationList, entityConfig, context);
				}
				else {
					return evaluateRuleForUnableToFill(ruleBean, positionAllocationList, entityConfig, context);
				}
			}
		}
		return new ArrayList<>();
	}


	private List<RuleViolation> evaluateRuleForChangeSincePrevious(PortfolioRun portfolioRun, List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList, EntityConfig entityConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		Map<InvestmentAccountAssetClassPositionAllocation, List<BasePortfolioRunReplication>> replicationListMap = getPositionAllocationReplicationListMap(portfolioRun, false, positionAllocationList, context);
		Map<InvestmentAccountAssetClassPositionAllocation, List<BasePortfolioRunReplication>> previousReplicationListMap = getPositionAllocationReplicationListMap(portfolioRun, true, positionAllocationList, context);
		for (Map.Entry<InvestmentAccountAssetClassPositionAllocation, List<BasePortfolioRunReplication>> investmentAccountAssetClassPositionAllocationListEntry : replicationListMap.entrySet()) {
			List<BasePortfolioRunReplication> replicationList = investmentAccountAssetClassPositionAllocationListEntry.getValue();
			BigDecimal totalAllocated = CoreMathUtils.sumProperty(replicationList, BasePortfolioRunReplication::getActualContractsAdjusted);
			// Only evaluate if they match - there is a different that checks for mismatches
			// And only evaluated if not from/to zero (excludes rolls)
			if (MathUtils.isEqual((investmentAccountAssetClassPositionAllocationListEntry.getKey()).getAllocationQuantity(), totalAllocated) && !MathUtils.isNullOrZero((investmentAccountAssetClassPositionAllocationListEntry.getKey()).getAllocationQuantity())) {
				BigDecimal previousTotalAllocated = CoreMathUtils.sumProperty(previousReplicationListMap.get(investmentAccountAssetClassPositionAllocationListEntry.getKey()), BasePortfolioRunReplication::getActualContractsAdjusted);
				if (!MathUtils.isEqual(previousTotalAllocated, totalAllocated) && !MathUtils.isNullOrZero((investmentAccountAssetClassPositionAllocationListEntry.getKey()).getAllocationQuantity())) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("previousTotalAllocated", previousTotalAllocated);
					templateValues.put("totalAllocated", totalAllocated);
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, portfolioRun.getId(), (investmentAccountAssetClassPositionAllocationListEntry.getKey()).getId(), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}


	private List<RuleViolation> evaluateRuleForUnused(PortfolioRun portfolioRun, List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList, EntityConfig entityConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		Map<InvestmentAccountAssetClassPositionAllocation, List<BasePortfolioRunReplication>> replicationListMap = getPositionAllocationReplicationListMap(portfolioRun, false, positionAllocationList, context);
		for (InvestmentAccountAssetClassPositionAllocation positionAllocation : CollectionUtils.getIterable(positionAllocationList)) {
			if (CollectionUtils.isEmpty(replicationListMap.get(positionAllocation))) {
				Map<String, Object> templateValues = new HashMap<>();
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, portfolioRun.getId(), positionAllocation.getId(), null, templateValues));
			}
		}
		return ruleViolationList;
	}


	private List<RuleViolation> evaluateRuleForUnableToFill(PortfolioRun portfolioRun, List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList, EntityConfig entityConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		Map<InvestmentAccountAssetClassPositionAllocation, List<BasePortfolioRunReplication>> replicationListMap = getPositionAllocationReplicationListMap(portfolioRun, false, positionAllocationList, context);
		for (Map.Entry<InvestmentAccountAssetClassPositionAllocation, List<BasePortfolioRunReplication>> investmentAccountAssetClassPositionAllocationListEntry : replicationListMap.entrySet()) {
			List<BasePortfolioRunReplication> replicationList = investmentAccountAssetClassPositionAllocationListEntry.getValue();
			// If replication list is empty, then it's not used and will be picked up by the unused warning
			if (!CollectionUtils.isEmpty(replicationList)) {
				BigDecimal totalActualContracts = CoreMathUtils.sumProperty(replicationList, BasePortfolioRunReplication::getActualContracts);
				BigDecimal totalVirtualContracts = CoreMathUtils.sumProperty(replicationList, BasePortfolioRunReplication::getVirtualContracts);
				// Because in some cases (CCY Forwards), we could have partial contracts which we don't support for explicit position allocations
				// As long as the difference is less than 1 then it's considered fully allocated
				BigDecimal difference = MathUtils.abs(MathUtils.subtract((investmentAccountAssetClassPositionAllocationListEntry.getKey()).getAllocationQuantity(), MathUtils.add(totalActualContracts, totalVirtualContracts)));
				if (MathUtils.isGreaterThanOrEqual(difference, BigDecimal.ONE)) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("totalActualContracts", totalActualContracts);
					templateValues.put("totalVirtualContracts", totalVirtualContracts);
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, portfolioRun.getId(), (investmentAccountAssetClassPositionAllocationListEntry.getKey()).getId(), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}


	private Map<InvestmentAccountAssetClassPositionAllocation, List<BasePortfolioRunReplication>> getPositionAllocationReplicationListMap(PortfolioRun ruleBean, boolean previousRun, List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<BasePortfolioRunReplication> replicationList = context.getReplicationList(ruleBean, previousRun);
		Map<InvestmentAccountAssetClassPositionAllocation, List<BasePortfolioRunReplication>> map = new HashMap<>();
		for (InvestmentAccountAssetClassPositionAllocation positionAllocation : CollectionUtils.getIterable(positionAllocationList)) {
			List<BasePortfolioRunReplication> repList = new ArrayList<>();
			for (BasePortfolioRunReplication rep : CollectionUtils.getIterable(replicationList)) {
				if (positionAllocation.getSecurity().equals(rep.getSecurity()) && positionAllocation.getAccountAssetClass().equals(rep.getAccountAssetClass())) {
					if (positionAllocation.getReplication() != null) {
						if (positionAllocation.getReplication().equals(rep.getReplication())) {
							repList.add(rep);
						}
					}
					else {
						repList.add(rep);
					}
				}
			}
			map.put(positionAllocation, repList);
		}
		return map;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public boolean isChangeSincePrevious() {
		return this.changeSincePrevious;
	}


	public void setChangeSincePrevious(boolean changeSincePrevious) {
		this.changeSincePrevious = changeSincePrevious;
	}


	public boolean isUnused() {
		return this.unused;
	}


	public void setUnused(boolean unused) {
		this.unused = unused;
	}


	public boolean isUnableToFill() {
		return this.unableToFill;
	}


	public void setUnableToFill(boolean unableToFill) {
		this.unableToFill = unableToFill;
	}
}



