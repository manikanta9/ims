package com.clifton.portfolio.run;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>PortfolioRunObserver</code> ...
 * <p/>
 * WARNING: THIS CLASS ONLY DOES VALIDATION, EXCEPT IT MUST BE AN OBSERVER BECAUSE AS A VALIDATOR IT CAUSES ISSUES WITH SOME WORKFLOW TRANSITIONS:
 * - The "getOriginalBean" for some cases appears to return the updated bean with workflow state already updated, but workflow status not
 * - Example Cases where it fails when using a validator:
 * 1. Create a New Run and attempt to submit it to trading.  When the system tries to close the older run it fails
 * 2. Revert Approval on a Completed run
 * *
 * For Inserts or Updates:  Validates MOC and Main are not both set
 * For Updates: Validates BalanceDate & InvestmentAccount cannot be changed
 * and if in Read Only steps - cannot change MOC flag
 * For Deletes: Validates not in a Read Only status
 *
 * @author Mary Anderson
 */
@Component
public class PortfolioRunObserver extends SelfRegisteringDaoObserver<PortfolioRun> {

	private PortfolioRunService portfolioRunService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<PortfolioRun> dao, DaoEventTypes event, PortfolioRun bean) throws ValidationException {
		if (event.isInsert()) {
			getPortfolioRunService().validatePortfolioRunIsSupportedForClientAccount(bean);
		}

		if ((event.isInsert() || event.isUpdate()) && bean.isMarketOnCloseAdjustmentsIncluded()) {
			ValidationUtils.assertFalse(bean.isMainRun(), "Market On Close runs cannot be flagged as the main run for the account and balance date.");
		}
		// Clear MainRun Flag on another run if necessary
		if (bean.isMainRun() && (event.isInsert() || event.isUpdate())) {
			PortfolioRun mainRun = getPortfolioRunService().getPortfolioRunByMainRun(bean.getClientInvestmentAccount().getId(), bean.getBalanceDate());
			if (mainRun != null && (event.isInsert() || !mainRun.equals(bean))) {
				mainRun.setMainRun(false);
				getPortfolioRunService().savePortfolioRun(mainRun);
			}
		}
		if (event.isUpdate()) {
			PortfolioRun originalBean = getOriginalBean(dao, bean);
			if (originalBean != null) {
				if (DateUtils.compare(originalBean.getBalanceDate(), bean.getBalanceDate(), false) != 0) {
					throw new FieldValidationException("The balance date originally entered cannot be changed.", "balanceDate");
				}
				if (!originalBean.getClientInvestmentAccount().equals(bean.getClientInvestmentAccount())) {
					throw new FieldValidationException("The client account originally selected cannot be changed.", "investmentAccount.id");
				}
				if (bean.isReadOnly()) {
					if (originalBean.isMarketOnCloseAdjustmentsIncluded() != bean.isMarketOnCloseAdjustmentsIncluded()) {
						throw new FieldValidationException("This run is currently in status [" + bean.getWorkflowStatus().getName() + "].  You cannot change the Market On Close option.",
								"marketOnCloseAdjustmentsIncluded");
					}
				}
			}
		}
		if (event.isDelete()) {
			if (bean.isReadOnly()) {
				throw new ValidationException("This run is currently in status [" + bean.getWorkflowStatus().getName() + "] and cannot be deleted.");
			}
		}
	}


	////////////////////////////////////////////////////////////
	//////////     Getter and Setter Methods          //////////
	////////////////////////////////////////////////////////////


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}
}
