package com.clifton.portfolio.run.ldi.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposureKeyRateDurationPoint;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * Search form for {@link com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure} objects.
 *
 * @author michaelm
 */
public class PortfolioRunLDIExposureSearchForm extends BaseEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "portfolioRun.id")
	private Integer runId;

	@SearchField(searchField = "replicationAllocationHistory.id")
	private Integer replicationAllocationHistoryId;

	private InvestmentSecurity security;

	private boolean currentSecurity;

	private BigDecimal securityPrice;

	/**
	 * If populated will be used for Exposure DV01 value calculations
	 * Will be populated when the replicationAllocation supports Dirty Price
	 */
	private BigDecimal securityDirtyPrice;

	private BigDecimal exchangeRate;

	private BigDecimal actualContracts;

	private Date krdValueDate;

	/**
	 * Client Account this position is held/traded in.  This is populated automatically by the system during processing
	 * and determined by any sub-account relationships to the main account the run is for.
	 * <p>
	 * If the positionInvestmentAccount = the account the run is for, this field is not set.
	 * <p>
	 * Calculated getter getTradingClientAccount returns the actual client account used to lookup positions and trade in
	 */
	private InvestmentAccount positionInvestmentAccount;

	private List<PortfolioRunLDIExposureKeyRateDurationPoint> keyRateDurationPointList;


	// custom flags to populate krd points
	private boolean populatePoints;
	private boolean reportableOnly;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public Integer getReplicationAllocationHistoryId() {
		return this.replicationAllocationHistoryId;
	}


	public void setReplicationAllocationHistoryId(Integer replicationAllocationHistoryId) {
		this.replicationAllocationHistoryId = replicationAllocationHistoryId;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public boolean isCurrentSecurity() {
		return this.currentSecurity;
	}


	public void setCurrentSecurity(boolean currentSecurity) {
		this.currentSecurity = currentSecurity;
	}


	public BigDecimal getSecurityPrice() {
		return this.securityPrice;
	}


	public void setSecurityPrice(BigDecimal securityPrice) {
		this.securityPrice = securityPrice;
	}


	public BigDecimal getSecurityDirtyPrice() {
		return this.securityDirtyPrice;
	}


	public void setSecurityDirtyPrice(BigDecimal securityDirtyPrice) {
		this.securityDirtyPrice = securityDirtyPrice;
	}


	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}


	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}


	public BigDecimal getActualContracts() {
		return this.actualContracts;
	}


	public void setActualContracts(BigDecimal actualContracts) {
		this.actualContracts = actualContracts;
	}


	public Date getKrdValueDate() {
		return this.krdValueDate;
	}


	public void setKrdValueDate(Date krdValueDate) {
		this.krdValueDate = krdValueDate;
	}


	public InvestmentAccount getPositionInvestmentAccount() {
		return this.positionInvestmentAccount;
	}


	public void setPositionInvestmentAccount(InvestmentAccount positionInvestmentAccount) {
		this.positionInvestmentAccount = positionInvestmentAccount;
	}


	public List<PortfolioRunLDIExposureKeyRateDurationPoint> getKeyRateDurationPointList() {
		return this.keyRateDurationPointList;
	}


	public void setKeyRateDurationPointList(List<PortfolioRunLDIExposureKeyRateDurationPoint> keyRateDurationPointList) {
		this.keyRateDurationPointList = keyRateDurationPointList;
	}


	public boolean isPopulatePoints() {
		return this.populatePoints;
	}


	public void setPopulatePoints(boolean populatePoints) {
		this.populatePoints = populatePoints;
	}


	public boolean isReportableOnly() {
		return this.reportableOnly;
	}


	public void setReportableOnly(boolean reportableOnly) {
		this.reportableOnly = reportableOnly;
	}
}
