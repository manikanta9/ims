package com.clifton.portfolio.run.trade;


import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.replication.calculators.InvestmentReplicationCurrencyTypes;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.rates.MarketDataRatesRetriever;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.assetclass.TradeVirtualAllocation;
import com.clifton.trade.assetclass.handler.TradeVirtualAllocationHandler;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.execution.TradeExecutionType;
import com.clifton.trade.execution.TradeExecutionTypeService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroup.TradeGroupActions;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>BasePortfolioRunTradeHandler</code> ...
 *
 * @author manderson
 */
public abstract class BasePortfolioRunTradeHandler<T extends PortfolioRunTrade<D>, D extends PortfolioRunTradeDetail> implements PortfolioRunTradeHandler<T, D> {

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentCalculator investmentCalculator;

	private MarketDataRetriever marketDataRetriever;
	private MarketDataRatesRetriever marketDataRatesRetriever;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;
	private PortfolioRunService portfolioRunService;

	private SecurityUserService securityUserService;

	private SystemBeanService systemBeanService;

	private TradeVirtualAllocationHandler tradeVirtualAllocationHandler;
	private TradeDestinationService tradeDestinationService;
	private TradeGroupService tradeGroupService;
	private TradeService tradeService;
	private TradeExecutionTypeService tradeExecutionTypeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract T getPortfolioRunTradeImpl(PortfolioRun run);


	public abstract void populatePortfolioRunTradeDetailListWithTrades(T run, boolean doNotPopulateDefaults);


	public abstract void refreshPortfolioRunTradeDetailListPrices(T run, boolean livePrices);


	public abstract void populatePortfolioRunTradeDetailPendingAndCurrentContracts(T run);


	public abstract D getPortfolioRunTradeDetail(int detailId);


	protected abstract List<D> getPortfolioRunTradeDetailListForRunImpl(int runId);

	////////////////////////////////////////////////////////////////////////////


	@Override
	public T getPortfolioRunTrade(PortfolioRun run, boolean doNotPopulateDefaults) {
		T tradeRun = getPortfolioRunTradeImpl(run);

		populatePortfolioRunTradeDetailListWithTrades(tradeRun, doNotPopulateDefaults);
		refreshPortfolioRunTradeDetailListPrices(tradeRun, false);
		populatePortfolioRunTradeDetailPendingAndCurrentContracts(tradeRun);
		return tradeRun;
	}


	public final List<D> getPortfolioRunTradeDetailListForRun(int runId) {
		List<D> detailList = getPortfolioRunTradeDetailListForRunImpl(runId);
		if (detailList != null) {
			// clear non persisted fields that may have old values because Hibernate will use 1st level cache for previous method call when available
			detailList.forEach(PortfolioRunTradeDetail::clearNonPersistentFields);
		}
		return detailList;
	}


	@Override
	public TradeGroup processPortfolioRunTradeCreation(T bean) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(bean.getId());

		// Validate Run is Available for Trading
		if (run.isClosed()) {
			throw new ValidationException("This run has already been marked as completed. There is a newer run available for Trade processing.");
		}
		else if (!run.isTradingAllowed()) {
			throw new ValidationException("Unable to submit trades for this run.  Trading is allowed in [Pending] workflow status only.  This run is currently in ["
					+ run.getWorkflowStatus().getName() + "].");
		}

		List<D> tradeDetailList = bean.getDetailList();

		// Uses next available date (today) the security exchange is open by default if not explicitly set
		Date tradeDate = bean.getTradeDate();
		SecurityUser trader = getSecurityUserService().getSecurityUserCurrent();

		// For closing positions, we need to look up the original details for the current contracts. Do this lazily.
		Lazy<T> originalRunTrade = new Lazy<>(() -> {
			T origRunTrade = getPortfolioRunTradeImpl(run);
			origRunTrade.setDetailList(getPortfolioRunTradeDetailListForRun(run.getId()));
			populatePortfolioRunTradeDetailPendingAndCurrentContracts(origRunTrade);
			return origRunTrade;
		});

		List<Trade> tradeList = new ArrayList<>();
		for (D detail : CollectionUtils.getIterable(tradeDetailList)) {
			// Skip Asset Classes where trade entry is disabled so we don't double trade amounts (copied from other asset class trade entry)
			if (isTradeEntryDisabled(detail)) {
				continue;
			}
			InvestmentReplicationCurrencyTypes currencyType = bean.getTradingCurrencyType();
			if (BooleanUtils.isTrue(detail.getClosePosition())) {
				D hydratedDetail = CollectionUtils.getStream(originalRunTrade.get().getDetailList())
						.filter(d -> CompareUtils.isEqual(d.getId(), detail.getId()))
						.findFirst()
						.orElse(null);
				if (hydratedDetail == null || MathUtils.isNullOrZero(hydratedDetail.getCurrentContracts())) {
					continue;
				}
				// Closing trades should be in the currency units of the transaction currency, which should match the currency contract units.
				// This prevents issues when closing Forwards trades with a SETTLEMENT_CURRENCY view.
				currencyType = InvestmentReplicationCurrencyTypes.TRANSACTION_CURRENCY;
				if (MathUtils.isGreaterThan(hydratedDetail.getCurrentContracts(), BigDecimal.ZERO)) {
					detail.setSellContracts(hydratedDetail.getCurrentContracts());
				}
				else {
					detail.setBuyContracts(MathUtils.abs(hydratedDetail.getCurrentContracts()));
				}
			}

			tradeList.add(populateTradeForDetail(detail, tradeDate, trader, currencyType));
		}

		ValidationUtils.assertNotEmpty(tradeList, "No trades could be generated for the submitted run details.");

		TradeGroup portfolioRunGroup = generateTradeGroupForPortfolioRunTrades(run, tradeList, tradeDate, trader, bean.getTradeGroupType());
		portfolioRunGroup = getTradeVirtualAllocationHandler().processTradeGroupVirtualTradeList(portfolioRunGroup, run.getBalanceDate());
		// After successful save, return the Trade Group. We will still transition the run as though trades are entered
		// UI has been updated so if the group is null, there will be the following message
		//if (portfolioRunGroup == null) {
		//	throw new ValidationException("All Buys/Sells entered resulted in a net trade of 0. No trades were created.");
		//}
		return portfolioRunGroup;
	}


	@Override
	public String processPortfolioRunTradeApproval(Integer[] detailIds) {
		if (detailIds == null || detailIds.length == 0) {
			throw new ValidationException("No selected rows to approve trades for.");
		}

		List<Trade> tradeList = new ArrayList<>();
		for (int detailId : detailIds) {
			for (Trade trade : CollectionUtils.getIterable(getPortfolioRunTradeDetailPendingTradeList(detailId))) {
				// Avoid dupes (OVERLAY: Contract splitting where trade is listed in multiple asset classes)
				if (!tradeList.contains(trade)) {
					tradeList.add(trade);
				}
			}
		}
		return getTradeGroupService().executeActionOnTradeList(TradeGroupActions.APPROVE, tradeList);
	}


	@Override
	public String processPortfolioRunTradeApproval(List<D> detailList) {
		if (CollectionUtils.isEmpty(detailList)) {
			throw new ValidationException("No selected rows to approve trades for.");
		}

		List<Trade> tradeList = new ArrayList<>();
		for (D detail : detailList) {
			for (Trade trade : CollectionUtils.getIterable(getPortfolioRunTradeDetailPendingTradeList(detail))) {
				// Avoid dupes (OVERLAY: Contract splitting where trade is listed in multiple asset classes)
				if (!tradeList.contains(trade)) {
					tradeList.add(trade);
				}
			}
		}
		return getTradeGroupService().executeActionOnTradeList(TradeGroupActions.APPROVE, tradeList);
	}


	@Override
	public List<Trade> getPortfolioRunTradeDetailPendingTradeList(int detailId) {
		return getPortfolioRunTradeDetailPendingTradeList(getPortfolioRunTradeDetail(detailId));
	}


	@Override
	public List<Trade> getPortfolioRunTradeDetailPendingTradeList(D detail) {
		if (detail == null) {
			// if trades are entered from other sources (e.g. Option Roll) the detail may be dynamically
			// created and will not exist with a valid ID. Return an empty list to prevent a NPE.
			return Collections.emptyList();
		}
		ValidationUtils.assertTrue(detail.getPortfolioRun() != null && detail.getPortfolioRun().getClientInvestmentAccount() != null && detail.getPortfolioRun().getClientInvestmentAccount().getId() != null,
				() -> "Unable to find pending trades for Portfolio Run Detail with no Client Investment Account defined: detail [" + detail + "]");
		ValidationUtils.assertTrue(detail.getSecurity() != null && detail.getSecurity().getId() != null,
				() -> "Unable to find pending trades for Portfolio Run Detail with no security defined: detail [" + detail + "]");

		PortfolioAccountContractStore store = getPendingContractStoreForPortfolioRunTradeDetail(detail);
		return store.getSecurityTradeListForAccountAssetClass(getAccountAssetClass(detail), detail.getSecurity().getId());
	}


	@Override
	public List<AccountingPositionTransfer> getPortfolioRunTradeDetailPendingTransferList(int detailId) {
		D detail = getPortfolioRunTradeDetail(detailId);
		PortfolioAccountContractStore store = getPendingContractStoreForPortfolioRunTradeDetail(detail);
		return store.getSecurityTransferListForAccountAssetClass(getAccountAssetClass(detail), detail.getSecurity().getId());
	}


	@Override
	public List<InvestmentAccount> getPortfolioRunTradeDetailHoldingInvestmentAccountList(int detailId) {
		return getPortfolioRunTradeDetailHoldingInvestmentAccountListImpl(getPortfolioRunTradeDetail(detailId), new HashMap<>());
	}


	@Override
	public T populatePortfolioRunTradeDetailTradeDefaults(T run) {
		if (run != null && !run.isNewBean()) {
			Map<String, List<InvestmentAccountRelationship>> investmentAccountRelationshipMap = new HashMap<>();
			List<D> detailList = run.getDetailList();
			if (CollectionUtils.isEmpty(detailList)) {
				detailList = getPortfolioRunTradeDetailListForRun(run.getId());
			}
			CollectionUtils.asNonNullList(detailList).forEach(detail -> populatePortfolioRunTradeDetailDefaults(detail, investmentAccountRelationshipMap));
		}
		return run;
	}


	private List<InvestmentAccount> getPortfolioRunTradeDetailHoldingInvestmentAccountListImpl(D detail, Map<String, List<InvestmentAccountRelationship>> runHoldingAccountRelationshipMap) {
		String tradingSecurityTypeName = detail.getSecurity().getInstrument().getHierarchy().getInvestmentType().getName();
		List<InvestmentAccountRelationship> relationshipList = (runHoldingAccountRelationshipMap == null ? null : runHoldingAccountRelationshipMap.get(tradingSecurityTypeName));
		if (relationshipList == null) {
			relationshipList = getHoldingInvestmentAccountRelationshipListForTrading(detail.getPortfolioRun().getClientInvestmentAccount(), tradingSecurityTypeName, null);
			if (runHoldingAccountRelationshipMap != null) {
				runHoldingAccountRelationshipMap.put(tradingSecurityTypeName, relationshipList);
			}
		}
		// Filter By "Trading" Client Account
		relationshipList = BeanUtils.filter(relationshipList, InvestmentAccountRelationship::getReferenceOne, detail.getTradingClientAccount());

		ValidationUtils.assertNotEmpty(relationshipList, "Unable to find a Holding Account that is valid for trading for Client Account [" + detail.getTradingClientAccount().getLabel()
				+ "] and Investment Type [" + tradingSecurityTypeName + "]");

		//Filter by holding account type
		InvestmentAccountType accountType = detail.getSecurity().getInstrument().getHierarchy().getHoldingAccountType();
		if (accountType != null) {
			relationshipList = BeanUtils.filter(relationshipList, rel -> rel.getReferenceTwo().getType(), accountType);
		}

		List<InvestmentAccount> list = new ArrayList<>();

		// Filter By Account Asset Class
		InvestmentAccountAssetClass accountAssetClass = getAccountAssetClass(detail);
		for (InvestmentAccountRelationship relationship : CollectionUtils.getIterable(relationshipList)) {
			if (relationship.getAccountAssetClass() == null || relationship.getAccountAssetClass().equals(accountAssetClass)) {
				if (!list.contains(relationship.getReferenceTwo())) {
					list.add(relationship.getReferenceTwo());
				}
			}
		}

		ValidationUtils.assertNotEmpty(list, "Unable to find a Holding Account that is valid for trading for Client Account [" + detail.getTradingClientAccount().getLabel() + "], Investment Type ["
				+ tradingSecurityTypeName + "]" + (accountAssetClass != null ? " and Asset Class [" + accountAssetClass.getAssetClass().getName() + "]." : "."));

		return list;
	}


	private List<InvestmentAccountRelationship> getHoldingInvestmentAccountRelationshipListForTrading(InvestmentAccount mainClientAccount, String tradingSecurityTypeName, Date activeOnDate) {
		if (activeOnDate == null) {
			activeOnDate = new Date();
		}
		// Get all main account relationships - Filter By Trading for Security Investment Type
		return getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipListForPurpose(mainClientAccount.getId(),
				TradeService.TRADING_PURPOSE_NAME_PREFIX + tradingSecurityTypeName, activeOnDate, true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Helper Methods                      /////////////
	////////////////////////////////////////////////////////////////////////////


	protected T getPortfolioRunForTrading(int runId, Class<T> clazz) {
		return getPortfolioRunForTrading(getPortfolioRunService().getPortfolioRun(runId), clazz);
	}


	protected T getPortfolioRunForTrading(PortfolioRun run, Class<T> clazz) {
		T tradeRun = BeanUtils.newInstance(clazz);
		BeanUtils.copyProperties(run, tradeRun);
		return tradeRun;
	}


	private PortfolioAccountContractStore getPendingContractStoreForPortfolioRunTradeDetail(D detail) {
		return getPortfolioAccountDataRetriever().getPortfolioAccountContractStorePending(null, detail.getPortfolioRun().getClientInvestmentAccount(), detail.getSecurity().getId());
	}


	protected boolean isTradeEntryDisabled(D detail) {
		return false;
	}


	protected InvestmentAccountAssetClass getAccountAssetClass(@SuppressWarnings("unused") D detail) {
		return null;
	}


	protected String getTradeEntryErrorMessagePrefix(D detail) {
		return "Trade Entry for Security [" + detail.getSecurity().getSymbol() + "] ";
	}


	protected void copySecurityTradeInfo(D detail, D fromDetail) {
		detail.setTradeSecurityPrice(fromDetail.getTradeSecurityPrice());
		detail.setTradeSecurityPriceDate(fromDetail.getTradeSecurityPriceDate());

		detail.setTradeExchangeRate(fromDetail.getTradeExchangeRate());
		detail.setTradeExchangeRateDate(fromDetail.getTradeExchangeRateDate());
	}


	protected void populatePortfolioRunTradeDetailDefaults(D detail, Map<String, List<InvestmentAccountRelationship>> runHoldingAccountRelationshipMap) {
		// Skip Non-Tradable Replications
		if (isTradeEntryDisabled(detail)) {
			return;
		}

		detail.setTrade(new Trade());

		// Set it to the first one in the list - ONLY if there is only one - otherwise the holding account will be defaulted if possible
		List<InvestmentAccount> holdingAccountList = getPortfolioRunTradeDetailHoldingInvestmentAccountListImpl(detail, runHoldingAccountRelationshipMap);
		if (CollectionUtils.getSize(holdingAccountList) == 1) {
			detail.getTrade().setHoldingInvestmentAccount(holdingAccountList.get(0));
		}

		Date tradeDate = getInvestmentCalculator().calculateTradeDate(detail.getSecurity(), null);
		TradeDestinationMapping td = getTradeDestinationService().getTradeDestinationMappingByCommand(new TradeDestinationMappingSearchCommand(detail.getSecurity().getId(), null, tradeDate));
		if (td != null) {
			// for orders, try to find a destination for the clearing company
			if (td.getTradeDestination().getType().isCreateOrder() && detail.getTrade().getHoldingInvestmentAccount() != null) {
				TradeDestinationMappingSearchCommand searchCommand = new TradeDestinationMappingSearchCommand(detail.getSecurity().getId(), detail.getTrade().getHoldingInvestmentAccount().getIssuingCompany().getId(), tradeDate);
				searchCommand.setTradeDestinationId(td.getTradeDestination().getId());
				TradeDestinationMapping clearingBrokerDestination = getTradeDestinationService().getTradeDestinationMappingByCommand(searchCommand);
				if (clearingBrokerDestination != null) {
					td = clearingBrokerDestination;
				}
			}

			detail.getTrade().setTradeDestination(td.getTradeDestination());

			// If it's a manual trade - Use the Holding Account's executing broker
			if (TradeDestination.MANUAL.equals(td.getTradeDestination().getName()) && detail.getTrade().getHoldingInvestmentAccount() != null) {
				detail.getTrade().setExecutingBrokerCompany(detail.getTrade().getHoldingInvestmentAccount().getIssuingCompany());
			}
			else {
				detail.getTrade().setExecutingBrokerCompany(td.getExecutingBrokerCompany());
			}
		}

		if (InvestmentUtils.isSecurityOfType(detail.getSecurity(), InvestmentType.FORWARDS)) {
			defaultTradeExecutionTypeForForwards(detail);
		}
		else if (detail.getPortfolioRun().isMarketOnCloseAdjustmentsIncluded()) {
			// Default MOC Runs to use MOC Trade Execution Type
			TradeType tradeType = (td != null) ? td.getTradeType() : getTradeService().getTradeTypeForSecurity(detail.getSecurity().getId());
			if (tradeType != null && tradeType.isTradeExecutionTypeSupported()) {
				detail.getTrade().setTradeExecutionType(getTradeExecutionTypeService().getTradeExecutionTypeByName(TradeExecutionType.MOC_TRADE));
			}
		}
	}


	/**
	 * Populates to the trade execution type and trade destination or forwards
	 */
	private void defaultTradeExecutionTypeForForwards(D detail) {
		// Default the Forward Execution Type to CLS or Non-CLS
		InvestmentAccount holdingAccount = detail.getTrade().getHoldingInvestmentAccount();
		String executionTypeName = (holdingAccount != null && holdingAccount.getType() != null && InvestmentAccountType.OTC_CLS.equals(holdingAccount.getType().getName()))
				? TradeExecutionType.CLS_TRADE : TradeExecutionType.NON_CLS_TRADE;
		detail.getTrade().setTradeExecutionType(getTradeExecutionTypeService().getTradeExecutionTypeByName(executionTypeName));
		detail.getTrade().setTradeDestination(getTradeDestinationService().getTradeDestinationByName(TradeDestination.FX_CONNECT_TICKET));
	}


	/**
	 * Populates trade information for detail row if B/S entered and returns resulting trade
	 * Note: These are later merged together for cases with contract splitting to create one trade with the net amount which is NOT handled here
	 */
	private Trade populateTradeForDetail(D detail, Date defaultTradeDate, SecurityUser trader, InvestmentReplicationCurrencyTypes currencyType) {
		String prefix = getTradeEntryErrorMessagePrefix(detail); // i.e. "Trade entry for Security [xyz] "

		// Perform Validation - BUY/SELL MUTUALLY EXCLUSIVE
		// If BUY Present
		if (!MathUtils.isNullOrZero(detail.getBuyContracts())) {
			// Make sure no SELL
			ValidationUtils.assertTrue(MathUtils.isNullOrZero(detail.getSellContracts()), prefix + " has buy and sell trades entered.  Please enter one or the other, but not both.");
		}
		// ELSE if no SELL, then NO Trades
		else if (MathUtils.isNullOrZero(detail.getSellContracts())) {
			return null;
		}

		// If trade date is explicitly set on the screen, otherwise get trade date based on when exchange is open
		Date securityTradeDate = defaultTradeDate != null ? defaultTradeDate : getInvestmentCalculator().calculateTradeDate(detail.getSecurity(), null);
		ValidationUtils.assertTrue(
				DateUtils.isDateBetween(securityTradeDate, detail.getSecurity().getStartDate(), detail.getSecurity().getEndDate(), false),
				prefix + " This security is not active on trade date [" + DateUtils.fromDateShort(securityTradeDate) + "] and cannot be traded on at this time.  Security End Date: "
						+ DateUtils.fromDateShort(detail.getSecurity().getEndDate()));

		Trade trade = detail.getTrade();
		if (trade == null) {
			trade = new Trade();
		}

		// Trade Properties
		trade.setInvestmentSecurity(detail.getSecurity());
		trade.setTradeDate(securityTradeDate);
		trade.setTraderUser(trader);

		if (detail.getTradeSecurityPrice() != null) {
			trade.setExpectedUnitPrice(detail.getTradeSecurityPrice());
		}
		else {
			trade.setExpectedUnitPrice(detail.getSecurityPrice());
		}

		if (!MathUtils.isNullOrZero(detail.getTradeExchangeRate())) {
			trade.setExchangeRateToBase(detail.getTradeExchangeRate());
		}
		else {
			trade.setExchangeRateToBase(detail.getExchangeRate());
		}

		boolean applyFactor = detail.getReplication().getType().isApplyCurrentFactor();
		if (MathUtils.isNullOrZero(detail.getBuyContracts())) {
			trade.setBuy(false);
			if (InvestmentUtils.isFactorChangeSupported(trade.getInvestmentSecurity()) && applyFactor) {
				trade.setOriginalFace(detail.getSellContracts());
			}
			else {
				trade.setQuantityIntended(detail.getSellContracts());
			}
		}
		else {
			trade.setBuy(true);
			if (InvestmentUtils.isFactorChangeSupported(trade.getInvestmentSecurity()) && applyFactor) {
				trade.setOriginalFace(detail.getBuyContracts());
			}
			else {
				trade.setQuantityIntended(detail.getBuyContracts());
			}
		}

		if (detail.getOpenCloseType() != null) {
			trade.setOpenCloseType(detail.getOpenCloseType());
		}

		// Set Trade Type based on the Security (based on investment type and optionally group if more than one, i.e. swaps and 3 trade types)
		TradeType type = getTradeService().getTradeTypeForSecurity(detail.getSecurity().getId());
		// Removed NULL validation since done within the getTradeTypeForSecurity method
		trade.setTradeType(type);
		if (trade.isFixTrade()) {
			ValidationUtils.assertTrue(trade.getTradeType().isFixSupported(), prefix + ". Fix Trades are not supported for type ["
					+ detail.getSecurity().getInstrument().getHierarchy().getInvestmentType().getName() + "]");
		}
		if (!type.isBrokerSelectionAtExecutionAllowed()) {
			ValidationUtils.assertNotNull(trade.getExecutingBrokerCompany(), prefix + " is missing a selection for the the executing broker.");
		}

		InvestmentSecurity security = trade.getInvestmentSecurity();
		trade.setClientInvestmentAccount(detail.getTradingClientAccount());
		trade.setPayingSecurity(InvestmentUtils.getSettlementCurrency(detail.getSecurity()));
		trade.setSettlementDate(getInvestmentCalculator().calculateSettlementDate(security, trade.getSettlementCurrency(), trade.getTradeDate()));

		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		if (hierarchy.isNotionalMultiplierUsed()) {
			NotionalMultiplierRetriever notionalMultiplierRetriever = (NotionalMultiplierRetriever) getSystemBeanService().getBeanInstance(hierarchy.getNotionalMultiplierRetriever());
			trade.setNotionalMultiplier(notionalMultiplierRetriever.retrieveNotionalMultiplier(security, trade::getSettlementDate, false));
		}

		InvestmentAccountAssetClass iac = getAccountAssetClass(detail);
		if (iac != null) {
			TradeVirtualAllocation tradeAllocation = new TradeVirtualAllocation();
			BeanUtils.copyProperties(trade, tradeAllocation);
			tradeAllocation.setAccountAssetClass(iac);
			tradeAllocation.setReplication(detail.getReplication());
			trade = tradeAllocation;
		}

		// if a trade  type is required, ensure it is set and of an allowed type
		if (trade.getTradeType().isExecutionTypeRequired()) {
			AssertUtils.assertNotNull(trade.getTradeExecutionType(), "A Trade Execution Type is required for trades of type:  " + trade.getTradeType());
			Set<TradeExecutionType> tradeExecutionTypeSet = new HashSet<>(getTradeExecutionTypeService().getTradeExecutionTypeListForTradeType(trade.getTradeType().getId()));
			ValidationUtils.assertTrue(tradeExecutionTypeSet.contains(trade.getTradeExecutionType()), "Trade Execution Type: " + trade.getTradeExecutionType() + " is not supported for trade type: " + trade.getTradeType() + ".");
		}

		// CLS trades do not require a require the selection of a Holding Account.
		// Further validation related ot CLS and TradeExecutionType is performed  in the TradeUtilHandler validation.
		boolean isClsTrade = trade.getTradeExecutionType() != null && trade.getTradeExecutionType().isClsTrade();
		if (!isClsTrade) {
			ValidationUtils.assertNotNull(trade.getHoldingInvestmentAccount(), prefix + " is missing a selection for the the holding investment account.");
		}

		if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.FORWARDS)) {
			if (currencyType == InvestmentReplicationCurrencyTypes.SETTLE_CURRENCY) {
				// reverse buy flag since we were given settlement CCY instead of the foreign CCY
				trade.setBuy(!trade.isBuy());
				// given settlement CCY
				trade.setAccountingNotional(trade.getQuantityIntended());
				trade.setQuantityIntended(null);
				// Given currency is the settlement/denominating CCY, thus the forward's underlying is the paying/settlement CCY.
				trade.setPayingSecurity(trade.getInvestmentSecurity().getUnderlyingSecurity());
			}
			else if (currencyType == InvestmentReplicationCurrencyTypes.BASE_CURRENCY) {
				// Adjust the base currency quantity into local/transaction currency.
				if (trade.getClientInvestmentAccount().getBaseCurrency() != trade.getInvestmentSecurity().getUnderlyingSecurity()) {
					BigDecimal contractValue = detail.getTradeValue();
					if (!MathUtils.isNullOrZero(contractValue)) {
						// Contract value from the UI is in terms to multiply local/transaction currency to base currency, so do the reverse.
						trade.setQuantityIntended(MathUtils.divide(trade.getQuantityIntended(), contractValue));
					}
				}
			}
		}

		return trade;
	}


	/**
	 * If trades have been populated, creates trade group but does not save them so they can be processed
	 * for asset class allocations.
	 */
	private TradeGroup generateTradeGroupForPortfolioRunTrades(PortfolioRun run, List<Trade> tradeList, Date tradeDate, SecurityUser trader, TradeGroupType tradeGroupType) {
		if (!CollectionUtils.isEmpty(tradeList)) {
			TradeGroup portfolioRunGroup = new TradeGroup();

			String tradeGroupTypeName = TradeGroupType.GroupTypes.PORTFOLIO.getTypeName();
			if (tradeGroupType != null && tradeGroupType.getName() != null) {
				tradeGroupTypeName = tradeGroupType.getName();
			}
			TradeGroupType groupType = getTradeGroupService().getTradeGroupTypeByName(tradeGroupTypeName);

			portfolioRunGroup.setTradeGroupType(groupType);
			portfolioRunGroup.setTradeDate(tradeDate);
			portfolioRunGroup.setSourceFkFieldId(run.getId());
			portfolioRunGroup.setNote("Portfolio Run: " + run.getLabel());
			portfolioRunGroup.setTraderUser(trader);
			portfolioRunGroup.setTradeList(tradeList);
			return portfolioRunGroup;
		}
		return null;
	}


	protected Date getNextBusinessDayFromRun(PortfolioRun run) {
		return getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(run.getBalanceDate()), 1);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataRatesRetriever getMarketDataRatesRetriever() {
		return this.marketDataRatesRetriever;
	}


	public void setMarketDataRatesRetriever(MarketDataRatesRetriever marketDataRatesRetriever) {
		this.marketDataRatesRetriever = marketDataRatesRetriever;
	}


	public TradeVirtualAllocationHandler getTradeVirtualAllocationHandler() {
		return this.tradeVirtualAllocationHandler;
	}


	public void setTradeVirtualAllocationHandler(TradeVirtualAllocationHandler tradeVirtualAllocationHandler) {
		this.tradeVirtualAllocationHandler = tradeVirtualAllocationHandler;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public TradeExecutionTypeService getTradeExecutionTypeService() {
		return this.tradeExecutionTypeService;
	}


	public void setTradeExecutionTypeService(TradeExecutionTypeService tradeExecutionTypeService) {
		this.tradeExecutionTypeService = tradeExecutionTypeService;
	}
}
