package com.clifton.portfolio.run.rule.accounting.position;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author stevenf
 */
public class PortfolioRunCounterpartyExposureLimitRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private String exposureField;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		List<AccountingPositionDaily> accountingPositionDailyLiveList = context.getAccountPositionDailyLiveList(run);
		//Create a map of issuing company id and it's positions
		Map<Integer, List<AccountingPositionDaily>> accountingPositionDailyLiveMap =
				BeanUtils.getBeansMap(accountingPositionDailyLiveList, "accountingTransaction.holdingInvestmentAccount.issuingCompany.id");
		for (Map.Entry<Integer, List<AccountingPositionDaily>> integerListEntry : accountingPositionDailyLiveMap.entrySet()) {
			//Check if the company should be processed.
			EntityConfig entityConfig = ruleConfig.getEntityConfig(MathUtils.getNumberAsLong(integerListEntry.getKey()));
			if (entityConfig != null && !entityConfig.isExcluded()) {
				ruleViolationList.addAll(checkCounterpartyExposure(run, integerListEntry.getKey(), entityConfig, integerListEntry.getValue()));
			}
		}
		return ruleViolationList;
	}


	private List<RuleViolation> checkCounterpartyExposure(PortfolioRun run, int entityFkFieldId, EntityConfig entityConfig, List<AccountingPositionDaily> accountingPositionDailyLiveList) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		BigDecimal counterpartyExposure = BigDecimal.ZERO;
		for (AccountingPositionDaily accountingPositionDailyLive : CollectionUtils.getIterable(accountingPositionDailyLiveList)) {
			//Sum the positions for the target field
			counterpartyExposure = MathUtils.add(counterpartyExposure, (BigDecimal) BeanUtils.getPropertyValue(accountingPositionDailyLive, getExposureField()));
		}
		//Calculate the exposurePercent
		BigDecimal portfolioTotalValue = run.getPortfolioTotalValue();
		if (portfolioTotalValue != null && portfolioTotalValue.compareTo(BigDecimal.ZERO) != 0) {
			BigDecimal minAmount = entityConfig.getMinAmount();
			BigDecimal maxAmount = entityConfig.getMaxAmount();
			BigDecimal exposurePercent = MathUtils.multiply(BigDecimal.valueOf(100), MathUtils.abs(MathUtils.divide(counterpartyExposure, run.getPortfolioTotalValue())));
			String violationNote = RuleEvaluatorUtils.evaluateValueForRange(exposurePercent, minAmount, maxAmount, MathUtils.NUMBER_FORMAT_MONEY, true);
			if (!StringUtils.isEmpty(violationNote)) {
				Map<String, Object> templateValues = new HashMap<>();
				templateValues.put(RuleEvaluator.VIOLATION_NOTE_FREEMARKER_TEMPLATE_KEY, violationNote);
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntity(entityConfig, BeanUtils.getIdentityAsLong(run), MathUtils.getNumberAsLong(entityFkFieldId), templateValues));
			}
		}
		return ruleViolationList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getExposureField() {
		return this.exposureField;
	}


	public void setExposureField(String exposureField) {
		this.exposureField = exposureField;
	}
}
