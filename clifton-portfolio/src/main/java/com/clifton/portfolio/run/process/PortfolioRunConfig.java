package com.clifton.portfolio.run.process;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.margin.PortfolioRunMargin;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunConfig</code> class contains information about the run that can be
 * passed through processing for re-usable information.  This class
 * is extended by service specific config objects that have their specific information
 *
 * @author manderson
 */
public class PortfolioRunConfig {

	private int currentDepth;

	private PortfolioRun run;
	private final boolean reloadProxyManagers;

	/**
	 * Common properties stored instead of multiple look ups
	 */
	private Date previousBusinessDay;
	private Date previousMonthEnd;
	private Date nextDay; // Not business day logic, just returns the next day based on the balance date - if null will be calculated and set, otherwise just returned

	/**
	 * Set during manager balance processing to cache the list of linked managers for the client
	 * During an initial processing step, the linked managers that depend on a run and that are linked to other accounts but potentially needed by this account (assignments) are loaded
	 * Then, after processing, linked managers linked to this account and that depend on this run (but are not assigned anywhere) are processed
	 */
	private List<InvestmentManagerAccount> clientLinkedManagerList;

	// Map of Security (CCY ID) Exchange Rates to the Client Account Base CCY
	// Where the Key is SecurityID_Date - where Date could be the Balance Date, Previous Business Day, Previous Month End
	private final Map<String, BigDecimal> exchangeRateMap = new HashMap<>();


	/**
	 * Actual Contracts associated with the account(s) in this run
	 */
	private PortfolioAccountContractStore contractStore;

	// Percentage of Margin Basis Used to Determine Recommended Margin
	private BigDecimal marginRecommendedPercent;
	private List<PortfolioRunMargin> runMarginList;

	/**
	 * Indicates whether the {@link PortfolioRunProcessHandler} should process this config as Preprocessing or postprocessing
	 */
	private boolean preProcess;


	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	public PortfolioRunConfig(PortfolioRun run, boolean reloadProxyManagers, int currentDepth) {
		this.run = run;
		this.reloadProxyManagers = reloadProxyManagers;
		this.currentDepth = currentDepth;
	}


	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	public Date getBalanceDate() {
		if (getRun() != null) {
			return getRun().getBalanceDate();
		}
		return null;
	}


	public Date getNextDay() {
		if (this.nextDay == null && getBalanceDate() != null) {
			this.nextDay = DateUtils.addDays(getBalanceDate(), 1);
		}
		return this.nextDay;
	}


	public Integer getClientInvestmentAccountId() {
		if (getClientInvestmentAccount() != null) {
			return getRun().getClientInvestmentAccount().getId();
		}
		return null;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		if (getRun() != null) {
			return getRun().getClientInvestmentAccount();
		}
		return null;
	}


	public String getExchangeRateDataSourceName() {
		if (getContractStore() != null) {
			return getContractStore().getExchangeRateDataSourceName();
		}
		return null;
	}


	public BigDecimal getExchangeRate(InvestmentSecurity fromCcy, Date date, MarketDataExchangeRatesApiService apiService) {
		String key = fromCcy.getId() + "_" + DateUtils.fromDateShort(date);
		return CollectionUtils.getValue(this.exchangeRateMap, key, () -> apiService.getExchangeRate(FxRateLookupCommand.forDataSource(getExchangeRateDataSourceName(), fromCcy.getSymbol(), getRun().getClientInvestmentAccount().getBaseCurrency().getSymbol(), date)), null);
	}


	public List<InvestmentManagerAccount> getClientLinkedManagerList(InvestmentManagerAccountService investmentManagerAccountService) {
		if (this.clientLinkedManagerList == null) {
			InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();
			searchForm.setClientIdOrRelatedClient(getRun().getClientInvestmentAccount().getBusinessClient().getId());
			searchForm.setLinkedManager(true);
			searchForm.setInactive(false);
			this.clientLinkedManagerList = investmentManagerAccountService.getInvestmentManagerAccountList(searchForm);
		}
		return this.clientLinkedManagerList;
	}


	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	public PortfolioRun getRun() {
		return this.run;
	}


	public void setRun(PortfolioRun run) {
		this.run = run;
	}


	public Date getPreviousBusinessDay() {
		return this.previousBusinessDay;
	}


	public void setPreviousBusinessDay(Date previousBusinessDay) {
		this.previousBusinessDay = previousBusinessDay;
	}


	public Date getPreviousMonthEnd() {
		return this.previousMonthEnd;
	}


	public void setPreviousMonthEnd(Date previousMonthEnd) {
		this.previousMonthEnd = previousMonthEnd;
	}


	public boolean isReloadProxyManagers() {
		return this.reloadProxyManagers;
	}


	public PortfolioAccountContractStore getContractStore() {
		return this.contractStore;
	}


	public void setContractStore(PortfolioAccountContractStore contractStore) {
		this.contractStore = contractStore;
	}


	public List<PortfolioRunMargin> getRunMarginList() {
		return this.runMarginList;
	}


	public void setRunMarginList(List<PortfolioRunMargin> runMarginList) {
		this.runMarginList = runMarginList;
	}


	public BigDecimal getMarginRecommendedPercent() {
		return this.marginRecommendedPercent;
	}


	public void setMarginRecommendedPercent(BigDecimal marginRecommendedPercent) {
		this.marginRecommendedPercent = marginRecommendedPercent;
	}


	public int getCurrentDepth() {
		return this.currentDepth;
	}


	public void setCurrentDepth(int currentDepth) {
		this.currentDepth = currentDepth;
	}


	public boolean isPreProcess() {
		return this.preProcess;
	}


	public void setPreProcess(boolean preProcess) {
		this.preProcess = preProcess;
	}
}
