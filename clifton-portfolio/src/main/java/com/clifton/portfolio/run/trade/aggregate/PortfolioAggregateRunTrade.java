package com.clifton.portfolio.run.trade.aggregate;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.replication.calculators.InvestmentReplicationCurrencyTypes;
import com.clifton.portfolio.replication.aggregate.PortfolioAggregateSecurityReplication;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.trade.PortfolioCurrencyTrade;
import com.clifton.portfolio.run.trade.PortfolioRunTrade;
import com.clifton.trade.group.TradeGroupType;

import java.util.Date;
import java.util.List;


/**
 * <code>PortfolioAggregateRunTrade</code> is a variation of {@link PortfolioRunTrade} that
 * holds aggregated portfolio details/replications by security for trading purposes.
 *
 * @author nickk
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class PortfolioAggregateRunTrade<D extends BasePortfolioRunReplication> extends PortfolioRun {

	private Date tradeDate;

	/**
	 * Currency to use when generating details for display and trading.
	 * Default is base currency.
	 */
	private InvestmentReplicationCurrencyTypes tradingCurrencyType = InvestmentReplicationCurrencyTypes.getDefaultCurrencyType();

	/**
	 * Used to override the TradeGroupType used for created Trades.
	 */
	private TradeGroupType tradeGroupType;


	private List<PortfolioAggregateSecurityReplication<D>> detailList;

	// Currency List for the Run.
	private List<PortfolioCurrencyTrade<?>> currencyTradeList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static <D extends BasePortfolioRunReplication> PortfolioAggregateRunTrade<D> forRunAndType(PortfolioRunTrade<D> basisRunTrade) {
		PortfolioAggregateRunTrade<D> runTrade = new PortfolioAggregateRunTrade<>();
		BeanUtils.copyProperties(basisRunTrade, runTrade);
		return runTrade;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public InvestmentReplicationCurrencyTypes getTradingCurrencyType() {
		return this.tradingCurrencyType;
	}


	public void setTradingCurrencyType(InvestmentReplicationCurrencyTypes tradingCurrencyType) {
		this.tradingCurrencyType = tradingCurrencyType;
	}


	public TradeGroupType getTradeGroupType() {
		return this.tradeGroupType;
	}


	public void setTradeGroupType(TradeGroupType tradeGroupType) {
		this.tradeGroupType = tradeGroupType;
	}


	public List<PortfolioAggregateSecurityReplication<D>> getDetailList() {
		return this.detailList;
	}


	public void setDetailList(List<PortfolioAggregateSecurityReplication<D>> detailList) {
		this.detailList = detailList;
	}


	public List<PortfolioCurrencyTrade<?>> getCurrencyTradeList() {
		return this.currencyTradeList;
	}


	public void setCurrencyTradeList(List<PortfolioCurrencyTrade<?>> currencyTradeList) {
		this.currencyTradeList = currencyTradeList;
	}
}
