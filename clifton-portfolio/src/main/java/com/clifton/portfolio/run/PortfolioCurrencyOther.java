package com.clifton.portfolio.run;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PortfolioCurrencyOther</code> ...
 *
 * @author Mary Anderson
 */
public class PortfolioCurrencyOther extends BaseSimpleEntity<Integer> {

	private PortfolioRun portfolioRun;

	private InvestmentSecurity currencySecurity;

	/**
	 * When true, indicates that it holds not Currency other, but amount that is
	 * allocated to the currencyDenominationBaseAmount on the replication (usually only applied to one replication
	 * where the Underlying = Base Currency of the Account and the CCY denomination of the Currency future = this currency security)
	 */
	private boolean currencyDenominationAllocation;

	private BigDecimal localAmount;
	private BigDecimal unrealizedLocalAmount;
	private BigDecimal exchangeRate;

	/**
	 * The following fields are used only for Pending and Current allocations for Trade Creation Screen
	 * These are not saved in the database
	 */
	@NonPersistentField
	private BigDecimal currentLocalAmount;
	@NonPersistentField
	private BigDecimal pendingLocalAmount;

	@NonPersistentField
	private BigDecimal currentUnrealizedLocalAmount;

	@NonPersistentField
	private BigDecimal currentExchangeRate;
	@NonPersistentField
	private Date currentExchangeRateDate;


	/////////////////////////////////////////////////////////


	public BigDecimal getCurrentExchangeRate() {
		if (this.currentExchangeRate == null) {
			return getExchangeRate();
		}
		return this.currentExchangeRate;
	}


	public Date getCurrentExchangeRateDate() {
		if (this.currentExchangeRateDate == null) {
			return getPortfolioRun().getBalanceDate();
		}
		return this.currentExchangeRateDate;
	}


	public BigDecimal getTotalLocalAmount() {
		return MathUtils.add(getLocalAmount(), getUnrealizedLocalAmount());
	}


	public BigDecimal getBaseAmount() {
		return MathUtils.multiply(getLocalAmount(), getExchangeRate());
	}


	public BigDecimal getUnrealizedBaseAmount() {
		return MathUtils.multiply(getUnrealizedLocalAmount(), getExchangeRate());
	}


	public BigDecimal getTotalBaseAmount() {
		return MathUtils.add(getBaseAmount(), getUnrealizedBaseAmount());
	}


	/////////////////////////////////////////////////////////////////////


	public BigDecimal getPendingBaseAmount() {
		return MathUtils.multiply(getPendingLocalAmount(), getExchangeRate());
	}


	public BigDecimal getCurrentBaseAmount() {
		return MathUtils.multiply(getCurrentLocalAmount(), getExchangeRate());
	}


	public BigDecimal getCurrentUnrealizedBaseAmount() {
		return MathUtils.multiply(getCurrentUnrealizedLocalAmount(), getExchangeRate());
	}


	public BigDecimal getTotalCurrentBaseAmount() {
		return MathUtils.add(getCurrentBaseAmount(), getCurrentUnrealizedBaseAmount());
	}


	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////


	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	public void setPortfolioRun(PortfolioRun portfolioRun) {
		this.portfolioRun = portfolioRun;
	}


	public InvestmentSecurity getCurrencySecurity() {
		return this.currencySecurity;
	}


	public void setCurrencySecurity(InvestmentSecurity currencySecurity) {
		this.currencySecurity = currencySecurity;
	}


	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}


	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}


	public BigDecimal getLocalAmount() {
		return this.localAmount;
	}


	public void setLocalAmount(BigDecimal localAmount) {
		this.localAmount = localAmount;
	}


	public BigDecimal getUnrealizedLocalAmount() {
		return this.unrealizedLocalAmount;
	}


	public void setUnrealizedLocalAmount(BigDecimal unrealizedLocalAmount) {
		this.unrealizedLocalAmount = unrealizedLocalAmount;
	}


	public boolean isCurrencyDenominationAllocation() {
		return this.currencyDenominationAllocation;
	}


	public void setCurrencyDenominationAllocation(boolean currencyDenominationAllocation) {
		this.currencyDenominationAllocation = currencyDenominationAllocation;
	}


	public BigDecimal getCurrentLocalAmount() {
		return this.currentLocalAmount;
	}


	public void setCurrentLocalAmount(BigDecimal currentLocalAmount) {
		this.currentLocalAmount = currentLocalAmount;
	}


	public BigDecimal getPendingLocalAmount() {
		return this.pendingLocalAmount;
	}


	public void setPendingLocalAmount(BigDecimal pendingLocalAmount) {
		this.pendingLocalAmount = pendingLocalAmount;
	}


	public BigDecimal getCurrentUnrealizedLocalAmount() {
		return this.currentUnrealizedLocalAmount;
	}


	public void setCurrentUnrealizedLocalAmount(BigDecimal currentUnrealizedLocalAmount) {
		this.currentUnrealizedLocalAmount = currentUnrealizedLocalAmount;
	}


	public void setCurrentExchangeRate(BigDecimal currentExchangeRate) {
		this.currentExchangeRate = currentExchangeRate;
	}


	public void setCurrentExchangeRateDate(Date currentExchangeRateDate) {
		this.currentExchangeRateDate = currentExchangeRateDate;
	}
}
