package com.clifton.portfolio.run.ldi.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Search form for {@link com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccount}s
 *
 * @author michaelm
 */
public class PortfolioRunLDIManagerAccountSearchForm extends BaseEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "portfolioRun.id")
	private Integer runId;

	@SearchField(searchField = "portfolioRun.balanceDate")
	private Date runBalanceDate;

	@SearchField(searchField = "portfolioRun.clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "managerAccountAssignment.id")
	private Integer managerAccountAssignmentId;

	@SearchField(searchField = "benchmarkSecurity.id")
	private Integer benchmarkSecurityId;

	@SearchField(searchField = "cashFlowGroupHistory.id")
	private Integer cashFlowGroupHistoryId;

	@SearchField(searchField = "cashFlowGroupHistory.cashFlowGroup.id")
	private Integer cashFlowGroupId;

	@SearchField
	private BigDecimal cashTotal;

	@SearchField
	private BigDecimal securitiesTotal;

	@SearchField
	private Date krdValueDate;


	// custom flags to populate krd points
	private boolean populatePoints;
	private boolean reportableOnly;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public Date getRunBalanceDate() {
		return this.runBalanceDate;
	}


	public void setRunBalanceDate(Date runBalanceDate) {
		this.runBalanceDate = runBalanceDate;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getManagerAccountAssignmentId() {
		return this.managerAccountAssignmentId;
	}


	public void setManagerAccountAssignmentId(Integer managerAccountAssignmentId) {
		this.managerAccountAssignmentId = managerAccountAssignmentId;
	}


	public Integer getBenchmarkSecurityId() {
		return this.benchmarkSecurityId;
	}


	public void setBenchmarkSecurityId(Integer benchmarkSecurityId) {
		this.benchmarkSecurityId = benchmarkSecurityId;
	}


	public Integer getCashFlowGroupHistoryId() {
		return this.cashFlowGroupHistoryId;
	}


	public void setCashFlowGroupHistoryId(Integer cashFlowGroupHistoryId) {
		this.cashFlowGroupHistoryId = cashFlowGroupHistoryId;
	}


	public Integer getCashFlowGroupId() {
		return this.cashFlowGroupId;
	}


	public void setCashFlowGroupId(Integer cashFlowGroupId) {
		this.cashFlowGroupId = cashFlowGroupId;
	}


	public BigDecimal getCashTotal() {
		return this.cashTotal;
	}


	public void setCashTotal(BigDecimal cashTotal) {
		this.cashTotal = cashTotal;
	}


	public BigDecimal getSecuritiesTotal() {
		return this.securitiesTotal;
	}


	public void setSecuritiesTotal(BigDecimal securitiesTotal) {
		this.securitiesTotal = securitiesTotal;
	}


	public Date getKrdValueDate() {
		return this.krdValueDate;
	}


	public void setKrdValueDate(Date krdValueDate) {
		this.krdValueDate = krdValueDate;
	}


	public boolean isPopulatePoints() {
		return this.populatePoints;
	}


	public void setPopulatePoints(boolean populatePoints) {
		this.populatePoints = populatePoints;
	}


	public boolean isReportableOnly() {
		return this.reportableOnly;
	}


	public void setReportableOnly(boolean reportableOnly) {
		this.reportableOnly = reportableOnly;
	}
}
