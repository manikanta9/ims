package com.clifton.portfolio.run;


import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.document.DocumentFileCountAware;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portal.file.PortalFileAware;
import com.clifton.rule.violation.RuleViolationAware;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.system.schema.column.json.SystemColumnCustomJsonAware;
import com.clifton.workflow.BaseWorkflowAwareEntity;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PortfolioRun</code> class represents a single instance of a client account's portfolio run
 * for the specified client investment account.
 * <p>
 * This is a generic class that can be used by all portfolios.  Many side tables associated with portfolio
 * runs are customized to fit that portfolio's structure. i.e. asset classes, key rate duration points, etc.
 *
 * @author manderson
 * @author michaelm
 */
public class PortfolioRun extends BaseWorkflowAwareEntity<Integer> implements RuleViolationAware, LabeledObject, DocumentFileCountAware, PortalFileAware, SystemColumnCustomJsonAware {

	public static final String TABLE_PORTFOLIO_RUN = "PortfolioRun";
	public static final String RULE_CATEGORY_PORTFOLIO_RUN = "Portfolio Run Rules";
	public static final String PORTFOLIO_RUN_INVALID_STATE = "Invalid";

	public static final String CUSTOM_FIELDS_GROUP_NAME = "Portfolio Run Custom Fields";
	public static final String CUSTOM_FIELD_TARGET_BALANCE_LIST = "Portfolio Target Balance List";

	private InvestmentAccount clientInvestmentAccount;

	private ServiceProcessingTypes serviceProcessingType;

	/**
	 * When managers are used this flag indicates that the MOC adjustments from
	 * the manager balance on the balance date was used.
	 */
	private boolean marketOnCloseAdjustmentsIncluded;

	/**
	 * Value date for the run.
	 */
	private Date balanceDate;

	/**
	 * The Non-MOC run for the balance date to be used for daily comparisons, performance summaries, etc.
	 */
	private boolean mainRun;

	private RuleViolationStatus violationStatus;

	private String notes;

	/**
	 * OVERLAY: Set during asset class processing when each asset class allocation is set
	 * LDI: Represents Total Liabilities DV01
	 */
	private BigDecimal portfolioTotalValue;

	/**
	 * OVERLAY: Set during manager processing - all the cash
	 * LDI: Target DV01 Total
	 * PORTFOLIO_TARGET: set during target processing - cash total
	 */
	private BigDecimal cashTotal;

	/**
	 * OVERLAY: Set during replication processing - total target
	 * LDI: Exposure DV01 Target Total
	 * PORTFOLIO_TARGET: set during target processing - Target Total
	 */
	private BigDecimal overlayTargetTotal;

	/**
	 * Set during replication processing - total exposure
	 * LDI: Represents Total Exposure DV01
	 * PORTFOLIO_TARGET: Total Exposure
	 */
	private BigDecimal overlayExposureTotal;

	/**
	 * OVERLAY: Set during process - only saves the total, not the details on each replication
	 * for Cash Exposure % to account for mispricing on list windows
	 * Note: Always will be zero if Client Account custom field selected to exclude mispricing
	 * <p>
	 * LDI: Currently Unused
	 */
	private BigDecimal mispricingTotal;

	/**
	 * Number of attachments associated with the run
	 */
	private Short documentFileCount;

	/**
	 * Custom properties. Initial use case for this is to store Target Activity/Balances when processing
	 * {@link com.clifton.business.service.ServiceProcessingTypes#PORTFOLIO_TARGET} runs.
	 */
	private CustomJsonString customColumns;

	/**
	 * A List of custom column values for this Run (fields are assigned and vary by processing type)
	 */
	@NonPersistentField
	private String columnGroupName;


	//////////////////////////////////////////////////////////////////////////
	// Database Level Calculated Columns
	//////////////////////////////////////////////////////////////////////////

	/**
	 * CashTotalValue - OverlayTargetTotalValue
	 * LDI: Represents Manager Assets DV01 (i.e. Target DV01 - Exposure DV01 target)
	 */
	private BigDecimal cashTarget;
	private BigDecimal cashTargetPercent;

	/**
	 * Overlay: CashTotalValue - (OverlayExposureTotalValue + CashTarget)
	 * LDI: Represents Net DV01 (Target DV01 - Manager Assets DV01 - Exposure DV01)
	 * PORTFOLIO_TARGET: Target Total - Total Exposure
	 */
	private BigDecimal cashExposure;
	private BigDecimal cashExposurePercent;

	/**
	 * CashExposure - Mispricing
	 * LDI: Unused
	 */
	private BigDecimal cashExposureWithMispricingPercent;


	/**
	 * If true, there is at least one file posted to the portal using this Portfolio Run as it's source
	 */
	private boolean postedToPortal;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public String getColumnGroupName() {
		return !StringUtils.isEmpty(this.columnGroupName) ? this.columnGroupName : CUSTOM_FIELDS_GROUP_NAME;
	}


	@Override
	public String getLabel() {
		if (getClientInvestmentAccount() != null) {
			String label = this.clientInvestmentAccount.getLabel() + " on " + DateUtils.fromDateShort(this.balanceDate);
			if (isMainRun()) {
				return label + ": Main Run";
			}
			else if (isMarketOnCloseAdjustmentsIncluded()) {
				return label + ": MOC";
			}
			return label;
		}
		return null;
	}


	public String getStatusLabel() {
		if (!isNewBean()) {
			return "Main Run: " + (isMainRun() ? "Yes" : "No") + "; Workflow State: " + getWorkflowStateName() + "/" + getWorkflowStatusName() + "; Warning Status: "
					+ (getViolationStatus() == null ? RuleViolationStatus.RuleViolationStatusNames.UNPROCESSED : getViolationStatus().getName()) + " Last Updated: "
					+ DateUtils.fromDate(getUpdateDate(), DateUtils.DATE_FORMAT_SHORT);
		}
		return null;
	}


	/**
	 * A run has been fully processed if it is not in Open or Invalid workflow status
	 */
	public boolean isIncomplete() {
		return WorkflowStatus.STATUS_OPEN.equals(getWorkflowStatusName()) || WorkflowStatus.STATUS_INVALID.equals(getWorkflowStatusName());
	}


	public boolean isProcessingAllowed() {
		return isIncomplete() || WorkflowStatus.STATUS_REVIEW.equals(getWorkflowStatusName());
	}


	public boolean isSubmittedToTrading() {
		return isReadOnly();
	}


	public boolean isTradingAllowed() {
		return WorkflowStatus.STATUS_PENDING.equals(getWorkflowStatusName()) || WorkflowStatus.STATUS_APPROVED.equals(getWorkflowStatusName());
	}


	public boolean isReadOnly() {
		return !isProcessingAllowed();
	}


	public boolean isClosed() {
		return WorkflowStatus.STATUS_CLOSED.equals(getWorkflowStatusName());
	}


	public String getWorkflowStatusName() {
		if (getWorkflowStatus() == null) {
			return null;
		}
		return getWorkflowStatus().getName();
	}


	public String getWorkflowStateName() {
		if (getWorkflowState() == null) {
			return null;
		}
		return getWorkflowState().getName();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getCashTotalPercent() {
		return getPercentOfPortfolio(getCashTotal());
	}


	public BigDecimal getOverlayTargetTotalPercent() {
		return getPercentOfPortfolio(getOverlayTargetTotal());
	}


	public BigDecimal getOverlayExposureTotalPercent() {
		return getPercentOfPortfolio(getOverlayExposureTotal());
	}


	private BigDecimal getPercentOfPortfolio(BigDecimal value) {
		return CoreMathUtils.getPercentValue(value, getPortfolioTotalValue(), true);
	}


	public ServiceProcessingTypes getServiceProcessingType() {
		return ObjectUtils.coalesce(this.serviceProcessingType, getClientAccountServiceProcessingType());
	}


	public ServiceProcessingTypes getClientAccountServiceProcessingType() {
		InvestmentAccount clientAccount = getClientInvestmentAccount();
		if (clientAccount != null && clientAccount.getServiceProcessingType() != null) {
			return clientAccount.getServiceProcessingType().getProcessingType();
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public void setServiceProcessingType(ServiceProcessingTypes serviceProcessingType) {
		this.serviceProcessingType = serviceProcessingType;
	}


	public boolean isMarketOnCloseAdjustmentsIncluded() {
		return this.marketOnCloseAdjustmentsIncluded;
	}


	public void setMarketOnCloseAdjustmentsIncluded(boolean marketOnCloseAdjustmentsIncluded) {
		this.marketOnCloseAdjustmentsIncluded = marketOnCloseAdjustmentsIncluded;
	}


	public String getNotes() {
		return this.notes;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public boolean isMainRun() {
		return this.mainRun;
	}


	public void setMainRun(boolean mainRun) {
		this.mainRun = mainRun;
	}


	@Override
	public RuleViolationStatus getViolationStatus() {
		return this.violationStatus;
	}


	@Override
	public void setViolationStatus(RuleViolationStatus violationStatus) {
		this.violationStatus = violationStatus;
	}


	public BigDecimal getPortfolioTotalValue() {
		return this.portfolioTotalValue;
	}


	public void setPortfolioTotalValue(BigDecimal portfolioTotalValue) {
		this.portfolioTotalValue = portfolioTotalValue;
	}


	public BigDecimal getCashTotal() {
		return this.cashTotal;
	}


	public void setCashTotal(BigDecimal cashTotal) {
		this.cashTotal = cashTotal;
	}


	public BigDecimal getOverlayTargetTotal() {
		return this.overlayTargetTotal;
	}


	public void setOverlayTargetTotal(BigDecimal overlayTargetTotal) {
		this.overlayTargetTotal = overlayTargetTotal;
	}


	public BigDecimal getOverlayExposureTotal() {
		return this.overlayExposureTotal;
	}


	public void setOverlayExposureTotal(BigDecimal overlayExposureTotal) {
		this.overlayExposureTotal = overlayExposureTotal;
	}


	public BigDecimal getMispricingTotal() {
		return this.mispricingTotal;
	}


	public void setMispricingTotal(BigDecimal mispricingTotal) {
		this.mispricingTotal = mispricingTotal;
	}


	public BigDecimal getCashTarget() {
		return this.cashTarget;
	}


	public void setCashTarget(BigDecimal cashTarget) {
		this.cashTarget = cashTarget;
	}


	public BigDecimal getCashTargetPercent() {
		return this.cashTargetPercent;
	}


	public void setCashTargetPercent(BigDecimal cashTargetPercent) {
		this.cashTargetPercent = cashTargetPercent;
	}


	public BigDecimal getCashExposure() {
		return this.cashExposure;
	}


	public void setCashExposure(BigDecimal cashExposure) {
		this.cashExposure = cashExposure;
	}


	public BigDecimal getCashExposurePercent() {
		return this.cashExposurePercent;
	}


	public void setCashExposurePercent(BigDecimal cashExposurePercent) {
		this.cashExposurePercent = cashExposurePercent;
	}


	public BigDecimal getCashExposureWithMispricingPercent() {
		return this.cashExposureWithMispricingPercent;
	}


	public void setCashExposureWithMispricingPercent(BigDecimal cashExposureWithMispricingPercent) {
		this.cashExposureWithMispricingPercent = cashExposureWithMispricingPercent;
	}


	@Override
	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	@Override
	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}


	public CustomJsonString getCustomColumns() {
		return this.customColumns;
	}


	public void setCustomColumns(CustomJsonString customColumns) {
		this.customColumns = customColumns;
		if (this.customColumns != null && this.customColumns.getJsonValue() != null && CollectionUtils.isEmpty(this.customColumns.jsonValueMap())) {
			CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(this.customColumns);
		}
	}


	@Override
	public boolean isPostedToPortal() {
		return this.postedToPortal;
	}


	@Override
	public void setPostedToPortal(boolean postedToPortal) {
		this.postedToPortal = postedToPortal;
	}
}
