package com.clifton.portfolio.run.workflow;


import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.report.PortfolioRunReportService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>PortfolioRunReportPostWorkflowAction</code> posts portfolio run report
 *
 * @author apopp
 */
public class PortfolioRunReportPostWorkflowAction implements WorkflowTransitionActionHandler<PortfolioRun> {

	private PortfolioRunReportService portfolioRunReportService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRun processAction(PortfolioRun run, WorkflowTransition transition) {
		return getPortfolioRunReportService().postPortfolioRunReportForRun(run);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunReportService getPortfolioRunReportService() {
		return this.portfolioRunReportService;
	}


	public void setPortfolioRunReportService(PortfolioRunReportService portfolioRunReportService) {
		this.portfolioRunReportService = portfolioRunReportService;
	}
}
