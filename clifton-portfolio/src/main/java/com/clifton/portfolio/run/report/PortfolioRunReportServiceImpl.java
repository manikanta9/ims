package com.clifton.portfolio.run.report;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.export.ReportExportService;
import com.clifton.report.posting.ReportPostingConfiguration;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import com.clifton.report.posting.ReportPostingService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedHashMap;


/**
 * The <code>PortfolioRunReportServiceImpl</code>
 *
 * @author manderson
 */
@Service
public class PortfolioRunReportServiceImpl implements PortfolioRunReportService {

	private static final String PORTAL_FILE_GROUP_DAILY_TRACKING_REPORT = "Daily Tracking Report (DTR)";
	private static final String PORTAL_FILE_PORTFOLIO_RUN_SOURCE_SECTION_NAME = "Portfolio Run";

	/**
	 * Custom field on the account that can be populated with an additional format to post the Portfolio Run report as.
	 * All reports are always posted as PDF, however some accounts also post the report in Excel format
	 */
	private static final String PORTAL_FILE_ADDITIONAL_REPORT_FORMAT_COLUMN_NAME = "Portfolio Run Post Additional Format";


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private CalendarBusinessDayService calendarBusinessDayService;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;
	private PortfolioRunService portfolioRunService;

	private ReportExportService reportExportService;
	private ReportPostingService<PortfolioRun> reportPostingService;


	////////////////////////////////////////////////////////////////////////////////
	//////////          Portfolio Run Report Business Methods            ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public FileWrapper downloadPortfolioRunReport(int runId, Boolean regenerateCache, FileFormats format, Boolean excludePrivate) {
		// Get the run
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		// Get which report to run for the client account for this run
		Integer reportId = (Integer) getPortfolioAccountDataRetriever().getPortfolioReportCustomFieldValue(run.getClientInvestmentAccount(), run.getServiceProcessingType());
		if (reportId == null) {
			throw new ValidationException("Report is not available for this run.");
		}
		LinkedHashMap<String, Object> parameters = getPortfolioRunReportParameters(run, excludePrivate != null && excludePrivate);
		ReportExportConfiguration exportConfiguration = new ReportExportConfiguration(reportId, parameters, false, regenerateCache != null && regenerateCache);
		exportConfiguration.setExportFormat(format == null ? FileFormats.PDF : format);
		exportConfiguration.setThrowErrorOnBadReport(false);
		if (exportConfiguration.getExportFormat() != FileFormats.PDF) {
			exportConfiguration.setCacheIgnored(true);
			exportConfiguration.setRegenerateCache(false);
		}
		return getReportExportService().downloadReportFile(exportConfiguration);
	}


	@Override
	public void clearPortfolioRunReportCache(int runId) {
		// Get the run
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		// Get which report to run for the client account for this run
		Integer reportId = getPortfolioRunReportId(run);
		if (reportId != null) {
			LinkedHashMap<String, Object> parameters = getPortfolioRunReportParameters(run, false);
			getReportExportService().clearReportPDFFileCache(reportId, parameters);
		}
	}


	@Override
	public void postPortfolioRunReport(int runId) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		postPortfolioRunReportForRun(run);
	}


	@Override
	public PortfolioRun postPortfolioRunReportForRun(PortfolioRun run) {
		// Get which report to run for the client account for this run
		Integer reportId = getPortfolioRunReportId(run);
		LinkedHashMap<String, Object> parameters = getPortfolioRunReportParameters(run, true);

		Date nextBusinessDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(run.getBalanceDate()), 1);

		ReportPostingFileConfiguration postingConfig = new ReportPostingFileConfiguration();
		postingConfig.setReportDate(run.getBalanceDate());
		postingConfig.setPublishDate(nextBusinessDay);
		postingConfig.setFileDuplicateAction(FileDuplicateActions.REPLACE); // Existing files are unapproved when run is sent back, so we want to replace the existing file with the new one when posting again
		postingConfig.setPortalFileCategoryName(PORTAL_FILE_GROUP_DAILY_TRACKING_REPORT);
		postingConfig.setPostEntitySourceTableName(InvestmentAccount.INVESTMENT_ACCOUNT_TABLE_NAME);
		postingConfig.setPostEntitySourceFkFieldId(run.getClientInvestmentAccount().getId());
		postingConfig.setSourceEntitySectionName(PORTAL_FILE_PORTFOLIO_RUN_SOURCE_SECTION_NAME);
		postingConfig.setSourceEntityFkFieldId(run.getId());

		ReportPostingConfiguration config = new ReportPostingConfiguration();
		config.setReportId(reportId);
		config.setParameterMap(parameters);
		config.setReportPostingFileConfiguration(postingConfig);
		config.setCacheIgnored(true);

		PortfolioRun result = getReportPostingService().postReportPostingClientWithSource(config, run);

		// See if the account posts a secondary file type as well
		String additionalFormatString = (String) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(run.getClientInvestmentAccount(), PORTAL_FILE_ADDITIONAL_REPORT_FORMAT_COLUMN_NAME);
		if (!StringUtils.isEmpty(additionalFormatString)) {
			FileFormats additionalFormat = FileFormats.valueOf(additionalFormatString);
			if (additionalFormat != config.getExportFormat()) {
				config.setExportFormat(additionalFormat);
				return getReportPostingService().postReportPostingClientWithSource(config, result);
			}
		}
		return result;
	}


	@Override
	public Integer getPortfolioRunReportId(PortfolioRun run) {
		return (Integer) getPortfolioAccountDataRetriever().getPortfolioReportCustomFieldValue(run.getClientInvestmentAccount(), run.getServiceProcessingType());
	}


	private LinkedHashMap<String, Object> getPortfolioRunReportParameters(PortfolioRun run, boolean excludePrivate) {
		LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
		parameters.put("RunID", run.getId());
		parameters.put("ClientAccountID", run.getClientInvestmentAccount().getId());
		parameters.put("BalanceDate", DateUtils.fromDateShort(run.getBalanceDate()));
		if (excludePrivate) {
			parameters.put("ExcludePrivate", true);
		}
		return parameters;
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////             Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public ReportExportService getReportExportService() {
		return this.reportExportService;
	}


	public void setReportExportService(ReportExportService reportExportService) {
		this.reportExportService = reportExportService;
	}


	public ReportPostingService<PortfolioRun> getReportPostingService() {
		return this.reportPostingService;
	}


	public void setReportPostingService(ReportPostingService<PortfolioRun> reportPostingService) {
		this.reportPostingService = reportPostingService;
	}
}
