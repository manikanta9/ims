package com.clifton.portfolio.run.rule;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.retriever.RuleRetrieverService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * PortfolioRunRuleHandler is used by PortfolioRun processing to run/delete/save system defined rules
 *
 * @author manderson
 */
@Component
public class PortfolioRunRuleHandlerImpl implements PortfolioRunRuleHandler {

	////////////////////////////////////////////////////////////////////

	// Rule Definition Name used to create rule violations for Validation Exceptions
	public static final String RULE_DEFINITION_NAME_VALIDATION_EXCEPTION = "Run Validation Violation";

	// Portfolio Run Rules: System Defined Definitions - Process In Code
	public static final String RULE_DEFINITION_MINIMIZE_IMBALANCES = "Minimize Imbalances Calculation Result";
	public static final String RULE_DEFINITION_TRADING_BANDS = "Trading Bands: Effective Cash vs. Overlay Exposure";

	// Portfolio Run Rules: System Defined Definitions - Look Up By Name for Values
	public static final String RULE_DEFINITION_SYNTHETIC_EXPOSURE_RANGE = "Synthetically Adjusted Positions Exposure Range";

	////////////////////////////////////////////////////////////////////

	private RuleDefinitionService ruleDefinitionService;
	private RuleEvaluatorService ruleEvaluatorService;
	private RuleRetrieverService ruleRetrieverService;
	private RuleViolationService ruleViolationService;

	////////////////////////////////////////////////////////////////////


	@Override
	public boolean isPortfolioRunNotIgnorableWarningsExist(int runId) {
		List<RuleViolation> violationList = getRuleViolationService().getRuleViolationListByLinkedEntity(PortfolioRun.TABLE_PORTFOLIO_RUN, runId);
		for (RuleViolation violation : CollectionUtils.getIterable(violationList)) {
			if (!violation.getRuleAssignment().getRuleDefinition().isIgnorable()) {
				return true;
			}
		}
		return false;
	}


	@Override
	public void clearPortfolioRunNotIgnorableWarnings(int runId) {
		getRuleViolationService().deleteRuleViolationListByLinkedEntityNotIgnorable(PortfolioRun.RULE_CATEGORY_PORTFOLIO_RUN, runId);
	}


	@Override
	public void savePortfolioRunRuleViolationFromException(ValidationExceptionWithCause exception, int runId) {
		getRuleViolationService().saveRuleViolationFromException(RULE_DEFINITION_NAME_VALIDATION_EXCEPTION, runId, exception);
	}


	@Override
	public void savePortfolioRunRuleViolationSystemDefined(String ruleDefinitionName, int runId, RuleViolation ruleViolation) {
		//System Defined Rules should only have a single global assignment.
		RuleAssignment ruleAssignment = getRuleDefinitionService().getRuleAssignmentGlobalByDefinitionName(ruleDefinitionName);
		List<RuleViolation> list = new ArrayList<>();
		if (ruleViolation != null) {
			ruleViolation.setLinkedFkFieldId(runId);
			ruleViolation.setRuleAssignment(ruleAssignment);
			list.add(ruleViolation);
		}
		getRuleViolationService().updateRuleViolationListForEntityAndDefinition(list, PortfolioRun.TABLE_PORTFOLIO_RUN, runId, ruleAssignment.getRuleDefinition().getName());
	}

	////////////////////////////////////////////////////////////////////


	@Override
	public void validatePortfolioRunRules(PortfolioRun run, boolean preProcess) {
		getRuleEvaluatorService().executeRules(PortfolioRun.RULE_CATEGORY_PORTFOLIO_RUN, BeanUtils.getIdentityAsLong(run), preProcess);
	}

	////////////////////////////////////////////////////////////////////////////
	///////       Client Account Rule Assignment Value Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getPortfolioRunRuleMaxAmount(String ruleDefinitionName, InvestmentAccount account) {
		return getRuleRetrieverService().getRuleConfigMaxAmountForAdditionalScopeEntityAndDefinition(PortfolioRun.RULE_CATEGORY_PORTFOLIO_RUN, ruleDefinitionName, BeanUtils.getIdentityAsLong(account));
	}

	////////////////////////////////////////////////////////////////////
	///////////          Getter and Setter Methods          ////////////
	////////////////////////////////////////////////////////////////////


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public RuleEvaluatorService getRuleEvaluatorService() {
		return this.ruleEvaluatorService;
	}


	public void setRuleEvaluatorService(RuleEvaluatorService ruleEvaluatorService) {
		this.ruleEvaluatorService = ruleEvaluatorService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public RuleRetrieverService getRuleRetrieverService() {
		return this.ruleRetrieverService;
	}


	public void setRuleRetrieverService(RuleRetrieverService ruleRetrieverService) {
		this.ruleRetrieverService = ruleRetrieverService;
	}
}
