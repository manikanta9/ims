package com.clifton.portfolio.run.rule.ldi;


import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.account.ldi.PortfolioAccountLDIServiceImpl;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccount;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunLDIKRDValueDateViolationRuleEvaluator</code> generates
 * warnings for the LDI Run if
 * 1. KRD Value Dates are not for the previous month end or balance date (depending on account setup for which to use)
 * 2. KRD Value Dates mismatch across all uses (managers and exposure) By default we enforce same dates, but an account can turn that option off if they choose
 * <p>
 * Note: Consider two dates to be the same if there are no business days between them for the security's calendar.  i.e. expect 10/31 but it's a Saturday, so 10/30 values
 * are considered to be the same as 10/31 values.
 *
 * @author manderson
 */
public class PortfolioRunLDIKRDValueDateViolationRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private static final String UNEXPECTED_KRD_VALUE_DATE = "Portfolio LDI Run Unexpected KRD Value Date";
	private static final String VARIOUS_KRD_VALUE_DATES = "Portfolio LDI Run Various KRD Value Dates";

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentCalculator investmentCalculator;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;
	private InvestmentInstrumentService investmentInstrumentService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<PortfolioRunLDIManagerAccount> managerList = context.getPortfolioRunLDIManagerAccountListForRun(run);
			List<PortfolioRunLDIExposure> exposureList = context.getPortfolioRunLDIExposureListForRun(run);

			Boolean useMonthEnd = (Boolean) context.getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(run.getClientInvestmentAccount(),
					PortfolioAccountLDIServiceImpl.CUSTOM_COLUMN_USE_MONTH_END_KRD_VALUES);
			Date expectedKRDDate = run.getBalanceDate();
			if (useMonthEnd != null && useMonthEnd) {
				expectedKRDDate = DateUtils.addDays(DateUtils.getFirstDayOfMonth(expectedKRDDate), -1);
			}

			List<Date> dateList = new ArrayList<>();
			Map<InvestmentSecurity, RuleViolation> securityViolationMap = new HashMap<>();

			for (PortfolioRunLDIManagerAccount manager : CollectionUtils.getIterable(managerList)) {
				validateKrdValueDate(manager.getLabel(), getBenchmarkOrDiscountCurveSecurity(manager), manager.getKrdValueDate(), expectedKRDDate, dateList, securityViolationMap);
			}

			for (PortfolioRunLDIExposure exposure : CollectionUtils.getIterable(exposureList)) {
				validateKrdValueDate(exposure.getLabel(), exposure.getSecurity(), exposure.getKrdValueDate(), expectedKRDDate, dateList, securityViolationMap);
			}

			String ruleDefinitionName = getRuleDefinitionService().getRuleAssignment(entityConfig.getRuleAssignmentId()).getRuleDefinition().getName();
			if (VARIOUS_KRD_VALUE_DATES.equals(ruleDefinitionName)) {
				if (CollectionUtils.getSize(dateList) > 1) {
					if (datesViolateThreshold(entityConfig, dateList)) {
						Map<String, Object> templateValues = new HashMap<>();
						templateValues.put("dateList", dateList);
						ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, run.getId(), templateValues));
					}
				}
			}
			else if (UNEXPECTED_KRD_VALUE_DATE.equals(ruleDefinitionName)) {
				for (Map.Entry<InvestmentSecurity, RuleViolation> investmentSecurityRuleViolationEntry : securityViolationMap.entrySet()) {
					RuleViolation ruleViolation = investmentSecurityRuleViolationEntry.getValue();
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put(RuleEvaluator.VIOLATION_NOTE_FREEMARKER_TEMPLATE_KEY,
							ruleViolation.getViolationNote().substring(0, ruleViolation.getViolationNote().length() - 2) + "</span>");
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), (investmentSecurityRuleViolationEntry.getKey()).getId(), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}


	public InvestmentSecurity getBenchmarkOrDiscountCurveSecurity(PortfolioRunLDIManagerAccount managerAccount) {
		if (managerAccount.getBenchmarkSecurity() != null) {
			return managerAccount.getBenchmarkSecurity();
		}
		if (managerAccount.getCashFlowGroupHistory() != null) {
			return getInvestmentInstrumentService().getInvestmentSecurity((Integer) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(managerAccount.getCashFlowGroupHistory().getCashFlowGroup().getInvestmentAccount(), InvestmentAccount.LDI_CASH_FLOW_DISCOUNT_CURVE));
		}
		return null;
	}


	private void validateKrdValueDate(String label, InvestmentSecurity security, Date krdValueDate, Date expectedKrdValueDate, List<Date> dateList,
	                                  Map<InvestmentSecurity, RuleViolation> securityDateViolationMap) {
		if (krdValueDate != null && security != null) {

			// Consider them to be the same if no business days between date used and expected date
			int businessDaysFromExpected = 0;
			if (DateUtils.compare(krdValueDate, expectedKrdValueDate, false) != 0) {
				businessDaysFromExpected = getCalendarBusinessDayService().getBusinessDaysBetween(krdValueDate, expectedKrdValueDate, null, getInvestmentCalculator().getInvestmentSecurityCalendar(security).getId());
			}

			// i.e. Don't show both 10/30 and 10/31 as both being used if they are considered the "same" business day, so consider it the same as the expected date
			if (businessDaysFromExpected == 0) {
				krdValueDate = expectedKrdValueDate;
			}

			if (!dateList.contains(krdValueDate)) {
				dateList.add(krdValueDate);
			}

			if (businessDaysFromExpected != 0) {
				RuleViolation ruleViolation = securityDateViolationMap.get(security);
				if (ruleViolation == null) {
					ruleViolation = new RuleViolation();
					ruleViolation.setViolationNote("Security " + security.getLabel() + " is using KRD Value Date of " + DateUtils.fromDateShort(krdValueDate) + " instead of expected "
							+ DateUtils.fromDateShort(expectedKrdValueDate) + ".<br /><span class='violationMessage'>This security is currently being used for the following: ");
				}
				ruleViolation.setViolationNote(ruleViolation.getViolationNote() + label + ", ");
				securityDateViolationMap.put(security, ruleViolation);
			}
		}
	}


	private boolean datesViolateThreshold(EntityConfig entityConfig, List<Date> dateList) {
		Date firstDate = null;
		Date lastDate = null;
		for (Date date : dateList) {
			if (firstDate == null || !DateUtils.isDateAfter(firstDate, date)) {
				firstDate = date;
			}
			if (lastDate == null || DateUtils.isDateAfter(lastDate, date)) {
				lastDate = date;
			}
		}
		BigDecimal threshold = entityConfig.getRuleAmount();
		if (MathUtils.isGreaterThanOrEqual(BigDecimal.valueOf(DateUtils.getDaysDifference(firstDate, lastDate)), threshold)) {
			return true;
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods               //////////////
	////////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
