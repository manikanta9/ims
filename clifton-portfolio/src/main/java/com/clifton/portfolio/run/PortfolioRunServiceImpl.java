package com.clifton.portfolio.run;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.portfolio.run.process.PortfolioRunProcessService;
import com.clifton.portfolio.run.search.PortfolioRunSearchForm;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class PortfolioRunServiceImpl implements PortfolioRunService {

	private AdvancedUpdatableDAO<PortfolioRun, Criteria> portfolioRunDAO;
	private AdvancedUpdatableDAO<PortfolioCurrencyOther, Criteria> portfolioCurrencyOtherDAO;

	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	/////////           Portfolio Run Business Methods                 /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRun getPortfolioRun(int id) {
		return getPortfolioRunDAO().findByPrimaryKey(id);
	}


	@Override
	public PortfolioRun getPortfolioRunByMainRun(Integer clientInvestmentAccountId, Date balanceDate) {
		return getPortfolioRunDAO().findOneByFields(new String[]{"clientInvestmentAccount.id", "balanceDate", "mainRun"}, new Object[]{clientInvestmentAccountId, balanceDate, true});
	}


	@Override
	public PortfolioRun getPortfolioRunLastByMOC(Integer clientInvestmentAccountId, Date balanceDate) {
		PortfolioRunSearchForm searchForm = new PortfolioRunSearchForm();
		searchForm.setClientInvestmentAccountId(clientInvestmentAccountId);
		searchForm.setBalanceDate(balanceDate);
		searchForm.setMarketOnCloseAdjustmentsIncluded(true);
		// Last One Submitted to Trading would be in Pending, Approved, or Closed status
		// And last updated
		searchForm.setWorkflowStatusNames(new String[]{WorkflowStatus.STATUS_PENDING, WorkflowStatus.STATUS_APPROVED, WorkflowStatus.STATUS_CLOSED});
		searchForm.setOrderBy("updateDate:desc");
		return CollectionUtils.getFirstElement(getPortfolioRunList(searchForm));
	}


	@Override
	public List<PortfolioRun> getPortfolioRunList(final PortfolioRunSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getInvestmentAccountGroupId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "link");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("referenceOne", "group");
					sub.createAlias("referenceTwo", "acct");
					sub.add(Restrictions.eqProperty("acct.id", criteria.getAlias() + ".clientInvestmentAccount.id"));
					sub.add(Restrictions.eq("group.id", searchForm.getInvestmentAccountGroupId()));
					criteria.add(Subqueries.exists(sub));
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				// If sorting is specified use it
				List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(searchForm.getOrderBy());
				if (!CollectionUtils.isEmpty(orderByList)) {
					return super.configureOrderBy(criteria);
				}
				// Otherwise Default sorting to Client Account Number Asc, Balance Date Desc
				String accountPath = getPathAlias("clientInvestmentAccount", criteria);
				criteria.addOrder(Order.asc(accountPath + ".number"));
				criteria.addOrder(Order.desc("balanceDate"));
				return true;
			}
		};

		return getPortfolioRunDAO().findBySearchCriteria(config);
	}


	/**
	 * See {@link PortfolioRunObserver} for validation
	 */
	@Override
	public PortfolioRun savePortfolioRun(PortfolioRun bean) {
		return getPortfolioRunDAO().save(bean);
	}


	/**
	 * NOTE: URL THAT IS EXPOSED TO UI FOR DELETION CAN BE FOUND ON {@link PortfolioRunProcessService}
	 */
	@Override
	public void deletePortfolioRun(int id) {
		getPortfolioRunDAO().delete(id);
	}


	@Override
	public boolean executePortfolioRunTransition(int runId, String stateName, boolean systemDefined) {
		if (!StringUtils.isEmpty(stateName)) {
			List<WorkflowState> stateList = getWorkflowDefinitionService().getWorkflowStateNextAllowedList(PortfolioRun.TABLE_PORTFOLIO_RUN, MathUtils.getNumberAsLong(runId));
			for (WorkflowState st : CollectionUtils.getIterable(stateList)) {
				if (stateName.equals(st.getName())) {
					if (systemDefined) {
						getWorkflowTransitionService().executeWorkflowTransitionSystemDefined(PortfolioRun.TABLE_PORTFOLIO_RUN, runId, st.getId());
					}
					else {
						getWorkflowTransitionService().executeWorkflowTransition(PortfolioRun.TABLE_PORTFOLIO_RUN, runId, st.getId());
					}
					return true;
				}
			}
		}
		return false;
	}


	@Override
	public void closePortfolioRun(PortfolioRun run) {
		// There should be only one "Closed" state to transition to - transition to that one
		// i.e. if already in Completed - Trades state could only go to Closed - Trades
		List<WorkflowState> stateList = getWorkflowDefinitionService().getWorkflowStateNextAllowedList(PortfolioRun.TABLE_PORTFOLIO_RUN, BeanUtils.getIdentityAsLong(run));
		stateList = BeanUtils.filter(stateList, workflowState -> workflowState.getStatus().getName(), WorkflowStatus.STATUS_CLOSED);
		if (CollectionUtils.getSize(stateList) == 1) {
			getWorkflowTransitionService().executeWorkflowTransitionSystemDefined(PortfolioRun.TABLE_PORTFOLIO_RUN, run.getId(), stateList.get(0).getId());
		}
		else {
			throw new ValidationException("Unable to transition to proper Closed status workflow state from [" + run.getWorkflowStateName() + "] for Run [" + run.getLabel() + "]."
					+ (CollectionUtils.getSize(stateList) == 0 ? " No" : " Multiple [" + BeanUtils.getPropertyValues(stateList, "name", ",") + "] ")
					+ " Closed status states available from transitions from that state. Expecting only 1.");
		}
	}


	@Override
	public void validatePortfolioRunIsSupportedForClientAccount(PortfolioRun run) {
		ValidationUtils.assertNotNull(run, "Portfolio Run is missing");
		InvestmentAccount clientAccount = run.getClientInvestmentAccount();
		ValidationUtils.assertNotNull(clientAccount, "Client Account is required.");
		ValidationUtils.assertNotNull(clientAccount.getServiceProcessingType(), "Client Account [" + clientAccount.getLabel() + "] is missing a service processing type selection.  Unable to create or process the run that doesn't have a processing type defined.");
		ValidationUtils.assertTrue(clientAccount.getServiceProcessingType().getProcessingType().isPortfolioRunsSupported(), "Service Processing Type [" + clientAccount.getServiceProcessingType().getLabel() + "] does not support portfolio runs. Unable to create or process the run for " + clientAccount.getLabel() + ".");
	}

	////////////////////////////////////////////////////////////////////////////
	////////          Portfolio Currency Other Business Methods         ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortfolioCurrencyOther> getPortfolioCurrencyOtherListByRun(int runId) {
		return getPortfolioCurrencyOtherDAO().findByField("portfolioRun.id", runId);
	}


	@Override
	public void savePortfolioCurrencyOtherListByRun(int runId, List<PortfolioCurrencyOther> saveList) {
		List<PortfolioCurrencyOther> originalList = getPortfolioCurrencyOtherListByRun(runId);
		getPortfolioCurrencyOtherDAO().saveList(saveList, originalList);
	}


	@Override
	public void deletePortfolioCurrencyOtherListByRun(int runId) {
		getPortfolioCurrencyOtherDAO().deleteList(getPortfolioCurrencyOtherListByRun(runId));
	}

	////////////////////////////////////////////////////////////////
	/////////         Getter & Setter Methods              /////////
	////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortfolioRun, Criteria> getPortfolioRunDAO() {
		return this.portfolioRunDAO;
	}


	public void setPortfolioRunDAO(AdvancedUpdatableDAO<PortfolioRun, Criteria> portfolioRunDAO) {
		this.portfolioRunDAO = portfolioRunDAO;
	}


	public AdvancedUpdatableDAO<PortfolioCurrencyOther, Criteria> getPortfolioCurrencyOtherDAO() {
		return this.portfolioCurrencyOtherDAO;
	}


	public void setPortfolioCurrencyOtherDAO(AdvancedUpdatableDAO<PortfolioCurrencyOther, Criteria> portfolioCurrencyOtherDAO) {
		this.portfolioCurrencyOtherDAO = portfolioCurrencyOtherDAO;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
