package com.clifton.portfolio.run.ldi.process.calculators;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.targetexposure.InvestmentAccountTargetExposureHandler;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentTypes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationPoint;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationPointAssignment;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationSecurity;
import com.clifton.portfolio.replication.PortfolioReplicationAllocationSecurityConfig;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposureKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.process.PortfolioRunLDIConfig;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunLDIExposureKeyRateDurationPointCalculator</code> calculates and populates data in {@link PortfolioRunLDIExposure} and {@link PortfolioRunLDIExposureKeyRateDurationPoint}
 * for each run.  This data takes positions for the account, and applies Key Rate Duration Point calculations to determine KRD and DV01 values for each position and KRD Point
 *
 * @author manderson
 */
public class PortfolioRunLDIExposureKeyRateDurationPointCalculator extends BasePortfolioRunLDICalculator {

	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountTargetExposureHandler<PortfolioAccountKeyRateDurationPoint, PortfolioAccountKeyRateDurationPointAssignment> investmentAccountTargetExposureHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void processStep(PortfolioRunLDIConfig runConfig) {
		List<PortfolioReplicationAllocationSecurityConfig> repAllocationSecurityList = getReplicationAllocationList(runConfig);
		for (PortfolioReplicationAllocationSecurityConfig allocationSecurityConfig : CollectionUtils.getIterable(repAllocationSecurityList)) {
			applyReplicationAllocationSecurityList(allocationSecurityConfig, runConfig);
		}

		// Sums Exposure DV01 Values for each point and applies to KRD Summary
		applyExposureTotalsToKeyRateDurationPointList(runConfig);
		// If any TFA or other adjustments are used - applied here
		applyTargetExposureAdjustmentsToKeyRateDurationPointList(runConfig);
	}


	@Override
	protected void saveStep(PortfolioRunLDIConfig runConfig) {
		getPortfolioRunLDIService().savePortfolioRunLDIExposureList(runConfig.getRunExposureList());
		getPortfolioRunLDIService().savePortfolioRunLDIExposureKeyRateDurationPointList(runConfig.getRunExposurePointList());

		getPortfolioRunLDIService().savePortfolioRunLDIKeyRateDurationPointList(runConfig.getRunKeyRateDurationPointList());

		List<PortfolioRunLDIKeyRateDurationPoint> managedKrdList = runConfig.getManagedRunLDIKeyRateDurationPointList();
		runConfig.getRun().setOverlayExposureTotal(CoreMathUtils.sumProperty(managedKrdList, PortfolioRunLDIKeyRateDurationPoint::getExposureDv01Value));
		runConfig.getRun().setOverlayTargetTotal(CoreMathUtils.sumProperty(managedKrdList, PortfolioRunLDIKeyRateDurationPoint::getExposureTargetDv01Value));

		runConfig.setRun(getPortfolioRunService().savePortfolioRun(runConfig.getRun()));
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Calculation Helper Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	private void applyReplicationAllocationSecurityList(PortfolioReplicationAllocationSecurityConfig allocationSecurityConfig, PortfolioRunLDIConfig runConfig) {
		boolean currentHeld = false;
		if (!CollectionUtils.isEmpty(allocationSecurityConfig.getSecurityList())) {
			for (InvestmentSecurity security : CollectionUtils.getIterable(allocationSecurityConfig.getSecurityList())) {
				if (security.equals(allocationSecurityConfig.getCurrentSecurity())) {
					currentHeld = true;
				}

				// Same security could be held by multiple accounts - we separate them out
				Map<Integer, PortfolioAccountContractStore.SecurityPositionQuantity> accountQuantityMap = runConfig.getContractStore().getClientAccountQuantityMapForSecurity(security.getId());
				int size = CollectionUtils.getSize(accountQuantityMap.keySet());
				if (size == 0) {
					populatePortfolioRunLDIExposure(runConfig, allocationSecurityConfig, security, null, BigDecimal.ZERO);
				}
				else {
					for (Map.Entry<Integer, PortfolioAccountContractStore.SecurityPositionQuantity> accountToQuantityEntry : accountQuantityMap.entrySet()) {
						populatePortfolioRunLDIExposure(runConfig, allocationSecurityConfig, security, accountToQuantityEntry.getKey(), accountToQuantityEntry.getValue().getQuantity());
					}
				}
			}
		}
		if (!currentHeld && (allocationSecurityConfig.isAlwaysIncludeCurrentSecurity() || CollectionUtils.isEmpty(allocationSecurityConfig.getSecurityList()))) {
			populatePortfolioRunLDIExposure(runConfig, allocationSecurityConfig, allocationSecurityConfig.getCurrentSecurity(), null, BigDecimal.ZERO);
		}
	}


	private void populatePortfolioRunLDIExposure(PortfolioRunLDIConfig runConfig, PortfolioReplicationAllocationSecurityConfig allocationSecurityConfig, InvestmentSecurity security,
	                                             Integer clientAccountId, BigDecimal quantity) {
		PortfolioRunLDIExposure exposure = new PortfolioRunLDIExposure();
		exposure.setReplicationAllocationHistory(allocationSecurityConfig.getReplicationAllocationHistory());
		exposure.setSecurity(security);
		if (security.equals(allocationSecurityConfig.getCurrentSecurity())) {
			exposure.setCurrentSecurity(true);
		}
		exposure.setSecurityPrice(getMarketDataRetriever().getPrice(exposure.getSecurity(), runConfig.getBalanceDate(), true, null));
		// Determine if Dirty Price Should be Populated based on the Replication Type (uses override if set) options
		boolean useDirtyPrice = allocationSecurityConfig.getReplicationAllocation().getCoalesceInvestmentReplicationTypeOverride().isDirtyPriceSupported();
		if (useDirtyPrice) {
			// Some dirty price calculators (IRS and CDS) will result in different values depending on if we hold long or short.
			exposure.setSecurityDirtyPrice(getInvestmentCalculator().calculateDirtyPrice(exposure.getSecurity(), quantity, exposure.getSecurityPrice(), runConfig.getBalanceDate()));
		}
		String fromCurrency = exposure.getSecurity().getInstrument().getTradingCurrency().getSymbol();
		String toCurrency = runConfig.getRun().getClientInvestmentAccount().getBaseCurrency().getSymbol();
		exposure.setExchangeRate(getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(runConfig.getExchangeRateDataSourceName(), fromCurrency, toCurrency, runConfig.getBalanceDate())));
		// If for a specific account that isn't the run's account
		if (clientAccountId != null && !MathUtils.isEqual(clientAccountId, runConfig.getClientInvestmentAccountId())) {
			exposure.setPositionInvestmentAccount(getInvestmentAccountService().getInvestmentAccount(clientAccountId));
		}
		exposure.setActualContracts(quantity);
		runConfig.addPortfolioRunLDIExposure(exposure);
		populateExposureKeyRateDurationPoints(exposure, runConfig);
	}


	/**
	 * For the security (exposure record), populates the list KRD Points and values
	 */
	private void populateExposureKeyRateDurationPoints(PortfolioRunLDIExposure exposure, PortfolioRunLDIConfig runConfig) {
		PortfolioAccountKeyRateDurationSecurity securityModifiedKrd = getModifiedKRDResultForSecurity(exposure.getSecurity(), runConfig);
		exposure.setKrdValueDate(securityModifiedKrd.getValueDate());
		addLDIExposureKeyRateDurationPointsForAccountSecurity(exposure, securityModifiedKrd, runConfig);
		populateReportableOnlyExposureKeyRateDurationPoints(exposure, runConfig);
	}


	private void populateReportableOnlyExposureKeyRateDurationPoints(PortfolioRunLDIExposure exposure, PortfolioRunLDIConfig runConfig) {
		PortfolioAccountKeyRateDurationSecurity securityModifiedKrd = getReportableOnlyModifiedKRDResultForSecurity(exposure.getSecurity(), runConfig);
		if (securityModifiedKrd != null) {
			addLDIExposureKeyRateDurationPointsForAccountSecurity(exposure, securityModifiedKrd, runConfig);
		}
	}


	private void addLDIExposureKeyRateDurationPointsForAccountSecurity(PortfolioRunLDIExposure exposure, PortfolioAccountKeyRateDurationSecurity securityModifiedKrd, PortfolioRunLDIConfig runConfig) {
		for (Map.Entry<PortfolioAccountKeyRateDurationPoint, BigDecimal> pointEntry : securityModifiedKrd.getKrdValueMap().entrySet()) {
			PortfolioRunLDIExposureKeyRateDurationPoint bean = new PortfolioRunLDIExposureKeyRateDurationPoint();
			bean.setKeyRateDurationPoint(pointEntry.getKey());
			bean.setKrdValue(pointEntry.getValue());
			runConfig.addPortfolioRunLDIExposureKeyRateDurationPoint(exposure, bean);
			// DV01 Value of One Contract = (KRD Value * Price * Exposure Multiplier)
			bean.setDv01ContractValue(bean.getDv01ContractValueCalculated());
		}
	}


	private void applyExposureTotalsToKeyRateDurationPointList(PortfolioRunLDIConfig config) {
		for (PortfolioRunLDIExposureKeyRateDurationPoint exposurePoint : CollectionUtils.getIterable(config.getRunExposurePointList())) {
			if (!MathUtils.isNullOrZero(exposurePoint.getDv01ActualExposureValue())) {
				for (PortfolioRunLDIKeyRateDurationPoint krdPoint : CollectionUtils.getIterable(config.getRunKeyRateDurationPointList())) {
					if (exposurePoint.getKeyRateDurationPoint().equals(krdPoint.getKeyRateDurationPoint())) {
						krdPoint.setExposureDv01Value(MathUtils.add(krdPoint.getExposureDv01Value(), exposurePoint.getDv01ActualExposureValue()));
						break;
					}
				}
			}
		}
	}


	private void applyTargetExposureAdjustmentsToKeyRateDurationPointList(PortfolioRunLDIConfig config) {
		for (PortfolioRunLDIKeyRateDurationPoint runKrdPoint : CollectionUtils.getIterable(config.getRunKeyRateDurationPointList())) {
			if (runKrdPoint.getKeyRateDurationPoint().isTargetExposureAdjustmentUsed()) {
				// Pull point again to get assignments populated
				PortfolioAccountKeyRateDurationPoint krdPoint = getPortfolioAccountLDIService().getPortfolioAccountKeyRateDurationPoint(runKrdPoint.getKeyRateDurationPoint().getId());
				BigDecimal amount = getInvestmentAccountTargetExposureHandler().calculateTargetExposureAdjustedAmount(krdPoint, krdPoint.getTargetExposureAdjustment(), runKrdPoint.getTargetHedgeDv01ValueAdjusted(), runKrdPoint.getTotalAssetsDv01Value(), config.getBalanceDate(), config.getRun().getPortfolioTotalValue(), config.getRun().isMarketOnCloseAdjustmentsIncluded());
				BigDecimal totalAdjustment = MathUtils.subtract(runKrdPoint.getTargetHedgeDv01ValueAdjusted(), amount);

				if (!MathUtils.isNullOrZero(totalAdjustment)) {
					runKrdPoint.setTargetHedgeDv01ValueAdjusted(amount);
					runKrdPoint.setTargetHedgePercentAdjusted(CoreMathUtils.getPercentValue(runKrdPoint.getTargetHedgeDv01ValueAdjusted(), runKrdPoint.getLiabilitiesDv01Value(), true));
					applyTargetExposureAdjustments(config, krdPoint, totalAdjustment, createProportionalTargetExposureAdjustmentMap(config, krdPoint.getTargetExposureProportionalType()));
				}
			}
		}
	}


	private Map<Serializable, BigDecimal> createProportionalTargetExposureAdjustmentMap(PortfolioRunLDIConfig config, String proportionalType) {
		Map<Serializable, BigDecimal> proportionalMap = new HashMap<>();
		for (PortfolioRunLDIKeyRateDurationPoint runKrdPoint : CollectionUtils.getIterable(config.getRunKeyRateDurationPointList())) {
			// Default is the target hedge percent...
			BigDecimal proportionalAmount = runKrdPoint.getTargetHedgePercent();
			if (PortfolioAccountKeyRateDurationPoint.TARGET_EXPOSURE_PROPORTIONAL_TYPE_LIABILITY_DV01.equals(proportionalType)) {
				proportionalAmount = runKrdPoint.getLiabilitiesDv01Value();
			}
			else if (PortfolioAccountKeyRateDurationPoint.TARGET_EXPOSURE_PROPORTIONAL_TYPE_TARGET_DV01.equals(proportionalType)) {
				proportionalAmount = runKrdPoint.getTargetHedgeDv01Value();
			}
			proportionalMap.put(runKrdPoint.getKeyRateDurationPoint().getId(), proportionalAmount);
		}
		return proportionalMap;
	}


	/**
	 * Applies adjustments for {@link InvestmentTargetExposureAdjustmentTypes} for dependent krd points where needed
	 */
	private void applyTargetExposureAdjustments(PortfolioRunLDIConfig runConfig, PortfolioAccountKeyRateDurationPoint krdPoint, BigDecimal totalAdjustment, Map<Serializable, BigDecimal> proportionalMap) {
		Map<PortfolioAccountKeyRateDurationPoint, BigDecimal> adjustmentMap = getInvestmentAccountTargetExposureHandler().calculateTargetExposureAdjustmentMap(krdPoint, krdPoint.getTargetExposureAdjustment(), totalAdjustment, proportionalMap);
		for (Map.Entry<PortfolioAccountKeyRateDurationPoint, BigDecimal> accountRateDurationEntry : adjustmentMap.entrySet()) {
			boolean found = false;
			for (PortfolioRunLDIKeyRateDurationPoint runPoint : CollectionUtils.getIterable(runConfig.getRunKeyRateDurationPointList())) {
				if (runPoint.getKeyRateDurationPoint().equals(accountRateDurationEntry.getKey())) {
					found = true;
					runPoint.setTargetHedgeDv01ValueAdjusted(MathUtils.add(runPoint.getTargetHedgeDv01ValueAdjusted(), accountRateDurationEntry.getValue()));
					runPoint.setTargetHedgePercentAdjusted(CoreMathUtils.getPercentValue(runPoint.getTargetHedgeDv01ValueAdjusted(), runPoint.getLiabilitiesDv01Value(), true));
					break;
				}
			}
			if (!found) {
				throw new ValidationExceptionWithCause("PortfolioAccountKeyRateDurationPoint", (accountRateDurationEntry.getKey()).getId(), "KRD Point [" + krdPoint.getLabel()
						+ "] references [" + (accountRateDurationEntry.getKey()).getMarketDataField().getName() + "] for target exposure adjustments, which is not a valid option. Please make sure this asset class is active for the account.");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountTargetExposureHandler<PortfolioAccountKeyRateDurationPoint, PortfolioAccountKeyRateDurationPointAssignment> getInvestmentAccountTargetExposureHandler() {
		return this.investmentAccountTargetExposureHandler;
	}


	public void setInvestmentAccountTargetExposureHandler(InvestmentAccountTargetExposureHandler<PortfolioAccountKeyRateDurationPoint, PortfolioAccountKeyRateDurationPointAssignment> investmentAccountTargetExposureHandler) {
		this.investmentAccountTargetExposureHandler = investmentAccountTargetExposureHandler;
	}
}
