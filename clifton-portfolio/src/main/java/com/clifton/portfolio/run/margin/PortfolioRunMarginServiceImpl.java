package com.clifton.portfolio.run.margin;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.process.PortfolioRunConfig;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Service
public class PortfolioRunMarginServiceImpl<T extends PortfolioRunConfig> implements PortfolioRunMarginService {

	private AdvancedUpdatableDAO<PortfolioRunMargin, Criteria> portfolioRunMarginDAO;

	/**
	 * Maps Service Type name to the PortfolioRunMarginHandler that processes margin for that service type
	 * Note: DEFAULT key is used for any service type name that isn't mapped and currently maps to the overlay margin handler
	 */
	private Map<String, PortfolioRunMarginHandler<T>> portfolioRunMarginHandlerMap;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;


	////////////////////////////////////////////////////////////////////////////
	////////         Portfolio Run Margin Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRunMargin getPortfolioRunMargin(int id) {
		return getPortfolioRunMarginDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortfolioRunMargin> getPortfolioRunMarginListByRun(int runId) {
		return getPortfolioRunMarginDAO().findByField("portfolioRun.id", runId);
	}


	@Override
	public void savePortfolioRunMarginListByRun(int runId, List<PortfolioRunMargin> saveList) {
		getPortfolioRunMarginDAO().saveList(saveList, getPortfolioRunMarginListByRun(runId));
	}


	@Override
	public void deletePortfolioRunMarginListByRun(int runId) {
		getPortfolioRunMarginDAO().deleteList(getPortfolioRunMarginListByRun(runId));
	}


	////////////////////////////////////////////////////////////////////////////
	/////////         Portfolio Run Margin Handler Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRunMarginHandler<T> getPortfolioRunMarginHandlerForRun(PortfolioRun run) {
		ValidationUtils.assertNotNull(run, "Portfolio Run object is required");
		return getPortfolioAccountDataRetriever().getClientAccountObjectFromContextMap(run.getClientInvestmentAccount(), run.getServiceProcessingType(), getPortfolioRunMarginHandlerMap(), "Portfolio Run Margin Handler");
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortfolioRunMargin, Criteria> getPortfolioRunMarginDAO() {
		return this.portfolioRunMarginDAO;
	}


	public void setPortfolioRunMarginDAO(AdvancedUpdatableDAO<PortfolioRunMargin, Criteria> portfolioRunMarginDAO) {
		this.portfolioRunMarginDAO = portfolioRunMarginDAO;
	}


	public Map<String, PortfolioRunMarginHandler<T>> getPortfolioRunMarginHandlerMap() {
		return this.portfolioRunMarginHandlerMap;
	}


	public void setPortfolioRunMarginHandlerMap(Map<String, PortfolioRunMarginHandler<T>> portfolioRunMarginHandlerMap) {
		this.portfolioRunMarginHandlerMap = portfolioRunMarginHandlerMap;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}
}
