package com.clifton.portfolio.run.trade.group;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.trade.PortfolioRunTradeDetail;

import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
public interface PortfolioRunTradeGroupHandler<D extends PortfolioRunTradeDetail> {


	/**
	 * Populates some information on the PortfolioRunTradeGroup object with specific run information
	 * and returns a map of Accounts to their detail objects from the run based on the properties set
	 * on the group bean itself. Optionally, an array of Client Account IDs can be provided to limit the
	 * result map details.
	 */
	public Map<InvestmentAccount, List<D>> getPortfolioRunAccountTradeGroupDetailMap(PortfolioRunTradeGroup bean, Integer... clientAccountIds);


	/**
	 * Behaves the same as {@link PortfolioRunTradeGroupHandler#getPortfolioRunAccountTradeGroupDetailMap(PortfolioRunTradeGroup, Integer...)}
	 * limits the returned list of detail objects to those of the provided portfolio run ID.
	 */
	public Map<InvestmentAccount, List<D>> getPortfolioRunAccountTradeGroupDetailMapForRun(PortfolioRunTradeGroup bean, Integer portfolioRunId);
}
