package com.clifton.portfolio.run.rule.trade;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistorySearchForm;
import com.clifton.workflow.history.WorkflowHistoryService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioRunIncompleteTradesRuleEvaluator</code> class is a rule evaluator
 * that generates violations during a PortfolioRun for accounts with incomplete or unbooked trades
 *
 * @author sfahey
 */
public class PortfolioRunIncompleteTradesRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private TradeService tradeService;
	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowHistoryService workflowHistoryService;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			for (InvestmentAccount clientAccount : CollectionUtils.getIterable(context.getClientAccountList(run))) {
				for (Trade trade : CollectionUtils.getIterable(getIncompleteTrades(clientAccount, context.getBalanceDate(run)))) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("clientAccount", clientAccount);
					if (TradeService.TRADE_UNBOOKED_STATE_NAME.equals(trade.getWorkflowState().getName())) {
						WorkflowHistory history = getLastUnbookedTradeWorkflowHistory(trade);
						if (history != null) {
							templateValues.put("history", history);
						}
					}
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), trade.getId(), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}


	private List<Trade> getIncompleteTrades(InvestmentAccount clientAccount, Date balanceDate) {
		TradeSearchForm tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setClientInvestmentAccountId(clientAccount.getId());

		Short[] workflowStatusIds = new Short[]{
				getWorkflowDefinitionService().getWorkflowStatusByName(WorkflowStatus.STATUS_CLOSED).getId(),
				getWorkflowDefinitionService().getWorkflowStatusByName(WorkflowStatus.STATUS_CANCELED).getId()};

		tradeSearchForm.setExcludeWorkflowStatusIds(workflowStatusIds);
		tradeSearchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN_OR_EQUALS, balanceDate));
		return getTradeService().getTradeList(tradeSearchForm);
	}


	private WorkflowHistory getLastUnbookedTradeWorkflowHistory(Trade trade) {
		WorkflowHistorySearchForm workflowHistorySearchForm = new WorkflowHistorySearchForm();
		workflowHistorySearchForm.setLimit(1);
		workflowHistorySearchForm.setTableName("Trade");
		workflowHistorySearchForm.setEntityId(BeanUtils.getIdentityAsLong(trade));
		workflowHistorySearchForm.setEndWorkflowStateNameEquals(TradeService.TRADE_UNBOOKED_STATE_NAME);
		workflowHistorySearchForm.setOrderBy("effectiveEndStateStartDate:DESC");
		return CollectionUtils.getOnlyElement(getWorkflowHistoryService().getWorkflowHistoryList(workflowHistorySearchForm));
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}
}
