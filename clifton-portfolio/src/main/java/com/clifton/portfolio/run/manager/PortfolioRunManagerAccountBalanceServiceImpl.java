package com.clifton.portfolio.run.manager;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.manager.populator.PortfolioRunManagerAccountBalancePopulator;
import com.clifton.portfolio.run.manager.populator.PortfolioRunManagerAccountBalancePopulatorConfig;
import com.clifton.portfolio.run.process.PortfolioRunConfig;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 * @author michaelm
 */
@Service
public class PortfolioRunManagerAccountBalanceServiceImpl<T extends PortfolioRunManagerAccountBalance> implements PortfolioRunManagerAccountBalanceService<T> {

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;

	private PortfolioRunService portfolioRunService;

	private Map<String, PortfolioRunManagerAccountBalancePopulator<T>> portfolioRunManagerAccountBalancePopulatorMap;


	/////////////////////////////////////////////////////////////////////////////


	@Override
	public List<T> getPortfolioRunManagerAccountBalanceList(int runId, PortfolioRunManagerAccountBalanceSearchForm searchForm) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		ValidationUtils.assertNotNull(run, "Missing Portfolio Run with ID [" + runId + "]");

		// ADD CACHING RETRIEVAL AND STORAGE?
		// AT THIS POINT IT MAY NOT BE WORTHWHILE IF THE LOOK UP IS FAST ENOUGH AND WE ARE PLANNING ON GETTING RID OF THE WARNING AS ITS EASIER TO SEE ADJUSTMENTS THIS WAY THEN READ THE WARNING

		// Populate Manager Balances
		PortfolioRunManagerAccountBalancePopulator<T> populator = getPortfolioRunManagerAccountBalancePopulatorForRun(run);
		PortfolioRunManagerAccountBalancePopulatorConfig config = new PortfolioRunManagerAccountBalancePopulatorConfig(run, searchForm);
		config.setExchangeRateDataSourceName(getMarketDataExchangeRatesApiService().getExchangeRateDataSourceForClientAccount(run.getClientInvestmentAccount().toClientAccount()));

		return populator.populatePortfolioRunManagerAccountBalanceList(config);
	}


	@Override
	public BigDecimal getInvestmentManagerValueInClientAccountBaseCurrency(PortfolioRunConfig runConfig, InvestmentManagerAccount managerAccount, Date date, BigDecimal value) {
		if (runConfig.getRun().getClientInvestmentAccount().getBaseCurrency().equals(managerAccount.getBaseCurrency())) {
			return value;
		}
		BigDecimal fxRate = runConfig.getExchangeRate(managerAccount.getBaseCurrency(), date, getMarketDataExchangeRatesApiService());
		return MathUtils.multiply(value, fxRate);
	}


	////////////////////////////////////////////////////////////////
	///   PortfolioRunManagerAccountBalancePopulator Methods     ///
	////////////////////////////////////////////////////////////////


	private PortfolioRunManagerAccountBalancePopulator<T> getPortfolioRunManagerAccountBalancePopulatorForRun(PortfolioRun run) {
		return getPortfolioAccountDataRetriever().getClientAccountObjectFromContextMap(run.getClientInvestmentAccount(), run.getServiceProcessingType(), getPortfolioRunManagerAccountBalancePopulatorMap(), "Portfolio Run Manager Balance Populator");
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter & Setter Methods               //////////////
	////////////////////////////////////////////////////////////////////////////////


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public Map<String, PortfolioRunManagerAccountBalancePopulator<T>> getPortfolioRunManagerAccountBalancePopulatorMap() {
		return this.portfolioRunManagerAccountBalancePopulatorMap;
	}


	public void setPortfolioRunManagerAccountBalancePopulatorMap(Map<String, PortfolioRunManagerAccountBalancePopulator<T>> portfolioRunManagerAccountBalancePopulatorMap) {
		this.portfolioRunManagerAccountBalancePopulatorMap = portfolioRunManagerAccountBalancePopulatorMap;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}
}
