package com.clifton.portfolio.run.comparisons;


/**
 * The <code>PortfolioRunTradesNotExistsComparison</code> is used to determine if trades DO NOT exist
 * that are associated with the run.
 *
 * @author manderson
 */
public class PortfolioRunTradesNotExistsComparison extends PortfolioRunTradesExistsComparison {

	@Override
	protected boolean isReverse() {
		return true;
	}
}
