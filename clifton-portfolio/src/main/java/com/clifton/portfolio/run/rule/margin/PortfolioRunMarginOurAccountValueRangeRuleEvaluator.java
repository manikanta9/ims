package com.clifton.portfolio.run.rule.margin;

import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.margin.PortfolioRunMargin;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Currently checks range for Our Account Value on each margin record
 * can use min/max for range, or rule amount for amount not equal to
 *
 * @author manderson
 */
public class PortfolioRunMarginOurAccountValueRangeRuleEvaluator extends BasePortfolioRunMarginRuleEvaluator {

	@Override
	public List<RuleViolation> evaluateRuleForPortfolioRunMargin(PortfolioRun run, PortfolioRunMargin margin, EntityConfig entityConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();

		boolean violationFound = false;
		BigDecimal ourValue = margin.getOurAccountValue();
		Map<String, Object> templateValues = new HashMap<>();
		if (entityConfig.getRuleAmount() != null && MathUtils.isEqual(ourValue, entityConfig.getRuleAmount())) {
			templateValues.put("rangeValue", ourValue);
			violationFound = true;
		}
		else {
			if (RuleEvaluatorUtils.isEntityConfigRangeViolated(ourValue, entityConfig, templateValues)) {
				violationFound = true;
			}
		}
		if (violationFound) {
			ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), margin.getId(), null, templateValues));
		}
		return ruleViolationList;
	}
}
