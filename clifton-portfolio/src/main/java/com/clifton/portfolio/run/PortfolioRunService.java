package com.clifton.portfolio.run;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.portfolio.run.search.PortfolioRunSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>PortfolioRunService</code>
 *
 * @author manderson
 */
public interface PortfolioRunService {

	////////////////////////////////////////////////////////////////////////////
	////////            Portfolio Run Business Methods               ///////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRun getPortfolioRun(int id);


	public PortfolioRun getPortfolioRunByMainRun(Integer clientInvestmentAccountId, Date balanceDate);


	/**
	 * Returns (if any) the last MOC run for the account and date
	 * MOC run must have been submitted to trading, and the last one is determined by ordering the MOC runs by Submitted to Trading Date DESC
	 */
	public PortfolioRun getPortfolioRunLastByMOC(Integer clientInvestmentAccountId, Date balanceDate);


	public List<PortfolioRun> getPortfolioRunList(PortfolioRunSearchForm searchForm);


	public PortfolioRun savePortfolioRun(PortfolioRun bean);


	@DoNotAddRequestMapping
	public void deletePortfolioRun(int id);


	@DoNotAddRequestMapping
	public boolean executePortfolioRunTransition(int runId, String stateName, boolean systemDefined);


	/**
	 * Moves the Portfolio Run to the correct Closed state: "Closed - Trades" or "Closed - No Trades"
	 * depending on if there were trades associated with the run.
	 */
	@DoNotAddRequestMapping
	public void closePortfolioRun(PortfolioRun run);


	/**
	 * Throws an exception if run object's account is missing (extra check on inserts but required field in the database)
	 * Or if the account is missing a service selection
	 * Or the service's type is missing or processing type doesn't support Portfolio runs
	 * Validated prior to initial insert of a new run, and just before processing the run
	 */
	@DoNotAddRequestMapping
	public void validatePortfolioRunIsSupportedForClientAccount(PortfolioRun run);


	////////////////////////////////////////////////////////////////////////////
	/////       Portfolio Currency Other Business Methods             //////////
	////////////////////////////////////////////////////////////////////////////


	public List<PortfolioCurrencyOther> getPortfolioCurrencyOtherListByRun(int runId);


	public void savePortfolioCurrencyOtherListByRun(int runId, List<PortfolioCurrencyOther> saveList);


	public void deletePortfolioCurrencyOtherListByRun(int runId);
}
