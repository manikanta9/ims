package com.clifton.portfolio.run.margin;


import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>PortfolioRunMargin</code> ...
 *
 * @author manderson
 */
public class PortfolioRunMargin extends BaseSimpleEntity<Integer> implements LabeledObject {

	private PortfolioRun portfolioRun;

	private InvestmentAccount brokerInvestmentAccount;

	/**
	 * PIOS Only: If Broker Account is associated with ONLY one account asset class, then it's
	 * marked here, otherwise it crosses multiple asset classes (most cases it will include all)
	 */
	private InvestmentAccountAssetClass accountAssetClass;

	/**
	 * manager account values associated with managerCompany.companyType.ourCompanies = true (i.e. Parametric Clifton)
	 * used for this specific margin analysis
	 */
	private BigDecimal ourAccountValue;

	/**
	 * Collateral at the Broker
	 */
	private BigDecimal brokerCollateralValue;
	@NonPersistentField
	private BigDecimal brokerCollateralValuePercent;

	/**
	 * Calculated collateral fields
	 */
	@NonPersistentField
	BigDecimal collateralTarget;
	@NonPersistentField
	BigDecimal collateralTargetPercent;

	@NonPersistentField
	BigDecimal pendingCollateral;
	@NonPersistentField
	BigDecimal pendingCollateralPercent;

	@NonPersistentField
	BigDecimal collateralDifference;
	@NonPersistentField
	BigDecimal collateralDifferencePercent;


	private BigDecimal marginInitialValue;

	private BigDecimal marginRecommendedValue;

	/**
	 * The base value margin recommendations are generated from
	 * For PIOS = Overlay Target for the Asset Class/Asset Classes that apply to the broker
	 * For LDI = Position Notional for holdings in broker account
	 */
	private BigDecimal marginBasis;


	//////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getAccountAssetClass() != null) {
			return getBrokerInvestmentAccount().getLabel() + ": " + getAccountAssetClass().getAssetClass().getName();
		}
		return getBrokerInvestmentAccount().getLabel();
	}


	public String getLabelShort() {
		if (getAccountAssetClass() != null) {
			return getBrokerInvestmentAccount().getLabelShort() + ": " + getAccountAssetClass().getAssetClass().getName();
		}
		return getBrokerInvestmentAccount().getLabelShort();
	}


	public String getPercentageLabel() {
		if (getPortfolioRun() != null) {
			ServiceProcessingTypes processingType = getPortfolioRun().getClientInvestmentAccount().getServiceProcessingType().getProcessingType();
			if (ServiceProcessingTypes.LDI == processingType) {
				return "Percent of Notional (%)";
			}
		}
		return "Percent of Overlay (%)";
	}


	//////////////////////////////////////////////////////
	//////////////////////////////////////////////////////


	public BigDecimal getOurAccountValuePercent() {
		return CoreMathUtils.getPercentValue(getOurAccountValue(), getMarginBasis(), true);
	}


	public BigDecimal getMarginInitialValuePercent() {
		return CoreMathUtils.getPercentValue(getMarginInitialValue(), getMarginBasis(), true);
	}


	public BigDecimal getMarginRecommendedValuePercent() {
		return CoreMathUtils.getPercentValue(getMarginRecommendedValue(), getMarginBasis(), true);
	}


	public BigDecimal getMarginVariationValue() {
		return MathUtils.subtract(getOurAccountValue(), getMarginInitialValue());
	}


	public BigDecimal getMarginVariationValuePercent() {
		return MathUtils.subtract(getOurAccountValuePercent(), getMarginInitialValuePercent());
	}


	public BigDecimal getMarginVariationValueLessRecommended() {
		return MathUtils.subtract(getMarginVariationValue(), getMarginRecommendedValue());
	}


	public BigDecimal getMarginVariationValueLessRecommendedPercent() {
		return MathUtils.subtract(getMarginVariationValuePercent(), getMarginRecommendedValuePercent());
	}


	public BigDecimal getExcessBrokerCollateralValue() {
		return MathUtils.subtract(getBrokerCollateralValue(), getMarginInitialValue());
	}


	//////////////////////////////////////////////////////
	//////////////////////////////////////////////////////


	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	public void setPortfolioRun(PortfolioRun portfolioRun) {
		this.portfolioRun = portfolioRun;
	}


	public InvestmentAccount getBrokerInvestmentAccount() {
		return this.brokerInvestmentAccount;
	}


	public void setBrokerInvestmentAccount(InvestmentAccount brokerInvestmentAccount) {
		this.brokerInvestmentAccount = brokerInvestmentAccount;
	}


	public BigDecimal getOurAccountValue() {
		return this.ourAccountValue;
	}


	public void setOurAccountValue(BigDecimal ourAccountValue) {
		this.ourAccountValue = ourAccountValue;
	}


	public BigDecimal getBrokerCollateralValue() {
		return this.brokerCollateralValue;
	}


	public void setBrokerCollateralValue(BigDecimal brokerCollateralValue) {
		this.brokerCollateralValue = brokerCollateralValue;
	}


	public BigDecimal getBrokerCollateralValuePercent() {
		return this.brokerCollateralValuePercent;
	}


	public void setBrokerCollateralValuePercent(BigDecimal brokerCollateralValuePercent) {
		this.brokerCollateralValuePercent = brokerCollateralValuePercent;
	}


	public BigDecimal getCollateralTarget() {
		return this.collateralTarget;
	}


	public void setCollateralTarget(BigDecimal collateralTarget) {
		this.collateralTarget = collateralTarget;
	}


	public BigDecimal getCollateralTargetPercent() {
		return this.collateralTargetPercent;
	}


	public void setCollateralTargetPercent(BigDecimal collateralTargetPercent) {
		this.collateralTargetPercent = collateralTargetPercent;
	}


	public BigDecimal getPendingCollateral() {
		return this.pendingCollateral;
	}


	public void setPendingCollateral(BigDecimal pendingCollateral) {
		this.pendingCollateral = pendingCollateral;
	}


	public BigDecimal getPendingCollateralPercent() {
		return this.pendingCollateralPercent;
	}


	public void setPendingCollateralPercent(BigDecimal pendingCollateralPercent) {
		this.pendingCollateralPercent = pendingCollateralPercent;
	}


	public BigDecimal getCollateralDifference() {
		return this.collateralDifference;
	}


	public void setCollateralDifference(BigDecimal collateralDifference) {
		this.collateralDifference = collateralDifference;
	}


	public BigDecimal getCollateralDifferencePercent() {
		return this.collateralDifferencePercent;
	}


	public void setCollateralDifferencePercent(BigDecimal collateralDifferencePercent) {
		this.collateralDifferencePercent = collateralDifferencePercent;
	}


	public BigDecimal getMarginInitialValue() {
		return this.marginInitialValue;
	}


	public void setMarginInitialValue(BigDecimal marginInitialValue) {
		this.marginInitialValue = marginInitialValue;
	}


	public BigDecimal getMarginRecommendedValue() {
		return this.marginRecommendedValue;
	}


	public void setMarginRecommendedValue(BigDecimal marginRecommendedValue) {
		this.marginRecommendedValue = marginRecommendedValue;
	}


	public BigDecimal getMarginBasis() {
		return this.marginBasis;
	}


	public void setMarginBasis(BigDecimal marginBasis) {
		this.marginBasis = marginBasis;
	}


	public InvestmentAccountAssetClass getAccountAssetClass() {
		return this.accountAssetClass;
	}


	public void setAccountAssetClass(InvestmentAccountAssetClass accountAssetClass) {
		this.accountAssetClass = accountAssetClass;
	}
}
