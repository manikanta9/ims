package com.clifton.portfolio.run.trade;


import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.margin.PortfolioRunMarginAccountDetail;
import com.clifton.portfolio.run.margin.PortfolioRunMarginDetail;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * The <code>ProductTradeService</code> defines the business methods for working with the run's Trade Creation screen
 *
 * @author Mary Anderson
 */
public interface PortfolioRunTradeService<T extends PortfolioRunTrade<D>, D extends PortfolioRunTradeDetail> {

	////////////////////////////////////////////////////////////////////////////
	///////      Portfolio Run Trade Creation Business Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns {@link PortfolioRunTrade} object with replication list populated for trade creation/review
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public T getPortfolioRunTrade(int id);


	/**
	 * Refreshes Prices to latest Bloomberg or Previous Night Close
	 *
	 * @param reloadDetails - Called to fully reload detail records when new rows are added
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public T getPortfolioRunTradePrices(T run, boolean livePrices, boolean reloadDetails);


	/**
	 * Refreshes Pending/Actual trades to most recent reflected in the database
	 * Whatever prices are passed are left as they are.
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public T getPortfolioRunTradeAmounts(T run);


	/**
	 * Saves Trades entered for a run based on buys/sells entered for detail in the detailList
	 * <p>
	 * Returns {@link TradeGroup} that was just created for submitted trades
	 */
	@SecureMethod(dtoClass = Trade.class)
	public TradeGroup processPortfolioRunTradeCreation(T bean);


	/**
	 * Submits all pending trades to Approved workflow state
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = Trade.class, permissions = SecurityPermission.PERMISSION_READ)
	public String processPortfolioRunTradeApproval(int runId, Integer[] detailIds);


	/**
	 * Returns a list of Pending Trades for a specific detail record.
	 * NOTE: Does NOT perform any contract splitting
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public List<Trade> getPortfolioRunTradeDetailPendingTradeList(int runId, int detailId);


	/**
	 * Returns a list of Pending Transfers for a specific detail record.
	 * NOTE: Does NOT perform any contract splitting
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public List<AccountingPositionTransfer> getPortfolioRunTradeDetailPendingTransferList(int runId, int detailId);


	/**
	 * Returns a list of valid holding accounts for a Detail record
	 */
	@SecureMethod(dtoClass = PortfolioRun.class)
	public List<InvestmentAccount> getPortfolioRunTradeDetailHoldingInvestmentAccountList(int runId, int detailId);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Internal method that can be used to get the correct handler for the run
	 */
	@DoNotAddRequestMapping
	public PortfolioRunTradeHandler<T, D> getPortfolioRunTradeHandlerForRun(int runId);


	@SecureMethod(dtoClass = PortfolioRun.class)
	public PortfolioRunMarginDetail getPortfolioRunMarginDetailByRun(int runId);


	@SecureMethod(dtoClass = PortfolioRun.class)
	public List<PortfolioRunMarginAccountDetail> getPortfolioRunMarginAccountDetailListByRun(int runId);
}
