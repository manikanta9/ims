package com.clifton.portfolio.run.ldi.trade;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure;
import com.clifton.portfolio.run.trade.PortfolioRunTrade;


/**
 * The <code>PortfolioRunTradeLDI</code> ...
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class PortfolioRunTradeLDI extends PortfolioRunTrade<PortfolioRunLDIExposure> {

	// NOTHING HERE
}
