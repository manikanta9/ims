package com.clifton.portfolio.run.manager.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.portfolio.run.manager.BasePortfolioRunManagerAccount;


/**
 * {@link BasePortfolioRunManagerAccountSearchForm} provides search functionality for {@link BasePortfolioRunManagerAccount} entities.
 *
 * @author michaelm
 */
public abstract class BasePortfolioRunManagerAccountSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "overlayRun.id", required = true)
	private Integer runId;

	// Custom Search Field: (Restrictions.or(Restrictions.ne("cashAllocation", BigDecimal.ZERO), Restrictions.ne("previousDayCashAllocation", BigDecimal.ZERO)));
	private Boolean withCashValuesOnly;

	@SearchField(searchField = "parent.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean rootOnly;

	@SearchField
	private Boolean privateManager;

	@SearchField(searchField = "parent.id")
	private Integer parentId;

	@SearchField(searchFieldPath = "managerAccountAssignment", searchField = "displayName,referenceOne.accountNumber,referenceOne.accountName,referenceOne.managerCompany.name,referenceOne.custodianAccount.number", sortField = "referenceOne.accountName", leftJoin = true)
	private String managerLabel;

	// Special field for sorting to do a coalesce so matches what is displayed
	@SearchField(searchFieldPath = "managerAccountAssignment", searchField = "displayName,referenceOne.accountName,referenceOne.managerCompany.name", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private String managerLabelSort;

	@SearchField(searchField = "managerAccountAssignment.id")
	private Integer managerAccountAssignmentId;

	// Custom Search Field: "managerAccountAssignment.referenceOne.managerCompany.type.ourCompany = true
	private Boolean ourCompanyAssignment;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Boolean getWithCashValuesOnly() {
		return this.withCashValuesOnly;
	}


	public void setWithCashValuesOnly(Boolean withCashValuesOnly) {
		this.withCashValuesOnly = withCashValuesOnly;
	}


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public Boolean getRootOnly() {
		return this.rootOnly;
	}


	public void setRootOnly(Boolean rootOnly) {
		this.rootOnly = rootOnly;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public Integer getManagerAccountAssignmentId() {
		return this.managerAccountAssignmentId;
	}


	public void setManagerAccountAssignmentId(Integer managerAccountAssignmentId) {
		this.managerAccountAssignmentId = managerAccountAssignmentId;
	}


	public Boolean getOurCompanyAssignment() {
		return this.ourCompanyAssignment;
	}


	public void setOurCompanyAssignment(Boolean ourCompanyAssignment) {
		this.ourCompanyAssignment = ourCompanyAssignment;
	}


	public Boolean getPrivateManager() {
		return this.privateManager;
	}


	public void setPrivateManager(Boolean privateManager) {
		this.privateManager = privateManager;
	}


	public String getManagerLabelSort() {
		return this.managerLabelSort;
	}


	public void setManagerLabelSort(String managerLabelSort) {
		this.managerLabelSort = managerLabelSort;
	}


	public String getManagerLabel() {
		return this.managerLabel;
	}


	public void setManagerLabel(String managerLabel) {
		this.managerLabel = managerLabel;
	}
}
