package com.clifton.portfolio.run.manager;

import com.clifton.core.dataaccess.search.form.SearchForm;


/**
 * Simple search form that is used to filter manager balance information for the run
 * A lot of filtering can be accomplished in code for these dynamic objects so it it not a real SearchForm
 *
 * @author manderson
 */
@SearchForm(hasOrmDtoClass = false)
public class PortfolioRunManagerAccountBalanceSearchForm {

	/**
	 * Used for PIOS Asset Class and Target information to determine level of Asset Classes/Targets to return.
	 * When true, uses "rootOnly" filter on Managers, else keeps this filter.
	 */
	private Boolean rootOnly;

	/**
	 * Optionally return only the manually adjusted manager balances to more easily view manual adjustments
	 */
	private Boolean showAdjustedManagersOnly;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getRootOnly() {
		return this.rootOnly;
	}


	public void setRootOnly(Boolean rootOnly) {
		this.rootOnly = rootOnly;
	}


	public Boolean getShowAdjustedManagersOnly() {
		return this.showAdjustedManagersOnly;
	}


	public void setShowAdjustedManagersOnly(Boolean showAdjustedManagersOnly) {
		this.showAdjustedManagersOnly = showAdjustedManagersOnly;
	}
}
