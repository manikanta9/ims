package com.clifton.portfolio.run.process;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.process.calculator.PortfolioRunProcessStepCalculator;
import com.clifton.portfolio.run.rule.PortfolioRunRuleHandler;
import com.clifton.rule.violation.RuleViolationUtil;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.rule.violation.status.RuleViolationStatusService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BasePortfolioRunProcessHandlerImpl</code> contains common methods/look ups that can be used
 * across the various service portfolio processors
 *
 * @author manderson
 */
public abstract class BasePortfolioRunProcessHandlerImpl<T extends PortfolioRunConfig> implements PortfolioRunProcessHandler {

	private CalendarSetupService calendarSetupService;
	private CalendarBusinessDayService calendarBusinessDayService;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;
	private PortfolioRunRuleHandler portfolioRunRuleHandler;
	private PortfolioRunService portfolioRunService;

	private RuleViolationStatusService ruleViolationStatusService;

	private SystemBeanService systemBeanService;

	private List<Integer> preProcessStepList;
	private List<Integer> processStepList;

	private String completedWorkflowState;


	/////////////////////////////////////////////////////
	//////           Run Setup Methods            ///////
	/////////////////////////////////////////////////////


	protected abstract T setupPortfolioRunConfig(PortfolioRun run, boolean reloadProxyManagers, int currentDepth);


	protected void populatePortfolioRunConfig(T config) {
		// Business Days Commonly Used
		// Get Dates first, so can reuse them during processing
		Calendar defaultCalendar = getCalendarSetupService().getDefaultCalendar();
		Date previousBD = getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(config.getBalanceDate(), defaultCalendar.getId()));
		Date previousMEBD = DateUtils.getLastDayOfMonth(DateUtils.addMonths(config.getBalanceDate(), -1));
		if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(previousMEBD, defaultCalendar.getId()))) {
			previousMEBD = getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(previousMEBD, defaultCalendar.getId()));
		}
		config.setPreviousBusinessDay(previousBD);
		config.setPreviousMonthEnd(previousMEBD);

		config.setContractStore(getPortfolioAccountDataRetriever().getPortfolioAccountContractStoreActual(config.getRun().getClientInvestmentAccount(), config.getBalanceDate(), false));
	}


	/////////////////////////////////////////////////////
	////           Run Clear/Delete Methods          ////
	/////////////////////////////////////////////////////


	private void clearProcessingForRun(int runId) {
		// DELETE ALL ASSOCIATED DATA AND SO CAN START PROCESS FROM FIRST STEP
		// DOES NOT DELETE IGNORABLE WARNINGS SO THAT IGNORE NOTES CAN BE COPIED AND WARNINGS THAT ARE NO LONGER VALID SHOULD BE DELETED AUTOMATICALLY
		deletePortfolioRunProcessing(runId);

		// MANUALLY DELETE NOT IGNORABLE WARNINGS SO CAN REPROCESS
		getPortfolioRunRuleHandler().clearPortfolioRunNotIgnorableWarnings(runId);
	}


	/////////////////////////////////////////////////////
	/////         Run Processing Methods            /////
	/////////////////////////////////////////////////////


	@Override
	public final void processPortfolioRun(PortfolioRun run, boolean reloadProxyManagers, int currentDepth) {
		clearProcessingForRun(run.getId());
		run = getPortfolioRunService().getPortfolioRun(run.getId()); // update run reference in case it was updated when processing was cleared

		T runConfig = setupPortfolioRunConfig(run, reloadProxyManagers, currentDepth);
		runConfig.setPreProcess(true);

		// Pre-Process Steps - Load Linked or Other Manager Balances
		processPortfolioRunStepList(runConfig);

		// After Pre-Processing is Complete - Move the Run to Processed (ONLY IF NOT ALREADY THERE)
		if (!RuleViolationUtil.isRuleViolationAwareProcessed(run)) {
			run.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS));
			runConfig.setRun(getPortfolioRunService().savePortfolioRun(run));
		}

		// If any non ignorable warnings are found - return back
		if (getPortfolioRunRuleHandler().isPortfolioRunNotIgnorableWarningsExist(runConfig.getRun().getId())) {
			getPortfolioRunService().executePortfolioRunTransition(runConfig.getRun().getId(), PortfolioRun.PORTFOLIO_RUN_INVALID_STATE, true);
		}
		else {
			// If Validates past pre-process warnings, process run steps
			runConfig.setPreProcess(false);
			processPortfolioRunStepList(runConfig);
			getPortfolioRunService().executePortfolioRunTransition(runConfig.getRun().getId(), getCompletedWorkflowState(), true);
		}
	}


	@SuppressWarnings("unchecked")
	private void processPortfolioRunStepList(T runConfig) {
		List<Integer> stepList = new ArrayList<>((runConfig.isPreProcess() ? getPreProcessStepList() : getProcessStepList()));
		if (!CollectionUtils.isEmpty(stepList)) {
			String currentWorkflowState = null;
			try {
				for (Integer stepId : stepList) {
					SystemBean stepBean = getSystemBeanService().getSystemBean(stepId);
					ValidationUtils.assertNotNull(stepBean, "Could not find a calculator bean to evaluate step with ID: " + stepId + " for " + (runConfig.isPreProcess() ? "preprocessing" : "postprocessing."));
					PortfolioRunProcessStepCalculator<T> step = (PortfolioRunProcessStepCalculator<T>) getSystemBeanService().getBeanInstance(stepBean);
					currentWorkflowState = step.getWorkflowStateName();
					if (step.isDoNotExecuteTransactional()) {
						doProcessPortfolioRunStep(step, runConfig);
					}
					else {
						processPortfolioRunStep(step, runConfig);
					}
				}
			}
			catch (ValidationException e) {
				// There have been times when the run was deleted while processing
				// Warnings create a "cannot transition exception" and the saveProductOverlayRun throws a Null Pointer
				// Before creating warning and saving run - ensure that it does exist.
				runConfig.setRun(getPortfolioRunService().getPortfolioRun(runConfig.getRun().getId()));
				if (runConfig.getRun() != null) {
					// If not already a SystemWarningValidationException, then let's make it one so we can create the exception as a warning
					if (!(e instanceof ValidationExceptionWithCause)) {
						// Validation Exceptions generated when warnings are already present, don't re-log as another warning
						if (!e.getMessage().startsWith("Error while processing step")) {
							e = new ValidationExceptionWithCause(e.getMessage(), e);
						}
					}
					if (e instanceof ValidationExceptionWithCause) {
						ValidationExceptionWithCause se = (ValidationExceptionWithCause) e;
						getPortfolioRunRuleHandler().savePortfolioRunRuleViolationFromException(se, runConfig.getRun().getId());
					}
					// If failed during this step - put run into step's workflow state
					getPortfolioRunService().executePortfolioRunTransition(runConfig.getRun().getId(), StringUtils.coalesce(true, currentWorkflowState, PortfolioRun.PORTFOLIO_RUN_INVALID_STATE), true);
					throw e;
				}
			}
		}
	}


	@Transactional
	protected void processPortfolioRunStep(PortfolioRunProcessStepCalculator<T> calculator, T runConfig) {
		doProcessPortfolioRunStep(calculator, runConfig);
	}


	private void doProcessPortfolioRunStep(PortfolioRunProcessStepCalculator<T> calculator, T runConfig) {
		if (calculator != null) {
			calculator.process(runConfig);
		}
	}


	/////////////////////////////////////////////////////
	////////        Getter & Setter Methods      ////////
	/////////////////////////////////////////////////////


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public List<Integer> getProcessStepList() {
		return this.processStepList;
	}


	public void setProcessStepList(List<Integer> processStepList) {
		this.processStepList = processStepList;
	}


	public List<Integer> getPreProcessStepList() {
		return this.preProcessStepList;
	}


	public void setPreProcessStepList(List<Integer> preProcessStepList) {
		this.preProcessStepList = preProcessStepList;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public PortfolioRunRuleHandler getPortfolioRunRuleHandler() {
		return this.portfolioRunRuleHandler;
	}


	public void setPortfolioRunRuleHandler(PortfolioRunRuleHandler portfolioRunRuleHandler) {
		this.portfolioRunRuleHandler = portfolioRunRuleHandler;
	}


	public String getCompletedWorkflowState() {
		return this.completedWorkflowState;
	}


	public void setCompletedWorkflowState(String completedWorkflowState) {
		this.completedWorkflowState = completedWorkflowState;
	}


	public RuleViolationStatusService getRuleViolationStatusService() {
		return this.ruleViolationStatusService;
	}


	public void setRuleViolationStatusService(RuleViolationStatusService ruleViolationStatusService) {
		this.ruleViolationStatusService = ruleViolationStatusService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
