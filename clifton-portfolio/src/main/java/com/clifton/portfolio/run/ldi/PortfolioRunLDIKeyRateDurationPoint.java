package com.clifton.portfolio.run.ldi;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.portfolio.account.ldi.PortfolioAccountKeyRateDurationPoint;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>PortfolioRunLDIKeyRateDurationPoint</code> ...
 *
 * @author manderson
 */
public class PortfolioRunLDIKeyRateDurationPoint extends BaseSimpleEntity<Integer> implements LabeledObject {

	private PortfolioRun portfolioRun;

	private PortfolioAccountKeyRateDurationPoint keyRateDurationPoint;

	/**
	 * Target Percentage from PortfolioAccountKeyRateDurationPoint Setup
	 */
	private BigDecimal targetHedgePercent;
	private BigDecimal targetHedgePercentAdjusted;

	/**
	 * Could be calculated getter = Target % of Liabilities DV01 Value
	 */
	private BigDecimal targetHedgeDv01Value;
	private BigDecimal targetHedgeDv01ValueAdjusted;

	/**
	 * Summation of {@link PortfolioRunLDIManagerAccountKeyRateDurationPoint} dv01Value (for this point) where Manager Assignment Cash Type = Liabilities
	 */
	private BigDecimal liabilitiesDv01Value;

	/**
	 * Summation of {@link PortfolioRunLDIManagerAccountKeyRateDurationPoint} dv01Value (for this point) where Manager Assignment Cash Type = Liabilities
	 */
	private BigDecimal assetsDv01Value;

	/**
	 * Summation of Exposure dv01Value (for this point)
	 */
	private BigDecimal exposureDv01Value;

	/**
	 * Rebalance Percentages from PortfolioAccountKeyRateDurationPoint Setup
	 */
	private BigDecimal rebalanceMinPercent;

	private BigDecimal rebalanceMaxPercent;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getKeyRateDurationPoint() != null) {
			return getKeyRateDurationPoint().getLabel();
		}
		return null;
	}


	/**
	 * Manager Assets & Exposure
	 */
	public BigDecimal getTotalAssetsDv01Value() {
		return MathUtils.add(getAssetsDv01Value(), getExposureDv01Value());
	}


	/**
	 * Position portion of the Target = Target DV01 - Manager Assets DV01
	 */
	public BigDecimal getExposureTargetDv01Value() {
		return MathUtils.subtract(getTargetHedgeDv01ValueAdjusted(), getAssetsDv01Value());
	}


	public BigDecimal getTotalAssetsDeviationFromTargetPercent() {
		return MathUtils.subtract(getEffectiveHedgePercent(), getTargetHedgePercentAdjusted());
	}


	public BigDecimal getTotalAssetsDeviationFromTargetDv01Value() {
		return MathUtils.subtract(getTotalAssetsDv01Value(), getTargetHedgeDv01ValueAdjusted());
	}


	/**
	 * Percent Value of the Total Assets/Liabilities DV01
	 */
	public BigDecimal getEffectiveHedgePercent() {
		return CoreMathUtils.getPercentValue(getTotalAssetsDv01Value(), getLiabilitiesDv01Value(), true);
	}


	public BigDecimal getRebalanceTriggerPercent() {
		if (MathUtils.isGreaterThanOrEqual(getTotalAssetsDeviationFromTargetPercent(), BigDecimal.ZERO)) {
			return getRebalanceMaxPercent();
		}
		return getRebalanceMinPercent();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	public void setPortfolioRun(PortfolioRun portfolioRun) {
		this.portfolioRun = portfolioRun;
	}


	public PortfolioAccountKeyRateDurationPoint getKeyRateDurationPoint() {
		return this.keyRateDurationPoint;
	}


	public void setKeyRateDurationPoint(PortfolioAccountKeyRateDurationPoint keyRateDurationPoint) {
		this.keyRateDurationPoint = keyRateDurationPoint;
	}


	public BigDecimal getTargetHedgePercent() {
		return this.targetHedgePercent;
	}


	public void setTargetHedgePercent(BigDecimal targetHedgePercent) {
		this.targetHedgePercent = targetHedgePercent;
	}


	public BigDecimal getTargetHedgeDv01Value() {
		return this.targetHedgeDv01Value;
	}


	public void setTargetHedgeDv01Value(BigDecimal targetHedgeDv01Value) {
		this.targetHedgeDv01Value = targetHedgeDv01Value;
	}


	public BigDecimal getTargetHedgePercentAdjusted() {
		return this.targetHedgePercentAdjusted;
	}


	public void setTargetHedgePercentAdjusted(BigDecimal targetHedgePercentAdjusted) {
		this.targetHedgePercentAdjusted = targetHedgePercentAdjusted;
	}


	public BigDecimal getTargetHedgeDv01ValueAdjusted() {
		return this.targetHedgeDv01ValueAdjusted;
	}


	public void setTargetHedgeDv01ValueAdjusted(BigDecimal targetHedgeDv01ValueAdjusted) {
		this.targetHedgeDv01ValueAdjusted = targetHedgeDv01ValueAdjusted;
	}


	public BigDecimal getLiabilitiesDv01Value() {
		return this.liabilitiesDv01Value;
	}


	public void setLiabilitiesDv01Value(BigDecimal liabilitiesDv01Value) {
		this.liabilitiesDv01Value = liabilitiesDv01Value;
	}


	public BigDecimal getAssetsDv01Value() {
		return this.assetsDv01Value;
	}


	public void setAssetsDv01Value(BigDecimal assetsDv01Value) {
		this.assetsDv01Value = assetsDv01Value;
	}


	public BigDecimal getExposureDv01Value() {
		return this.exposureDv01Value;
	}


	public void setExposureDv01Value(BigDecimal exposureDv01Value) {
		this.exposureDv01Value = exposureDv01Value;
	}


	public BigDecimal getRebalanceMinPercent() {
		return this.rebalanceMinPercent;
	}


	public void setRebalanceMinPercent(BigDecimal rebalanceMinPercent) {
		this.rebalanceMinPercent = rebalanceMinPercent;
	}


	public BigDecimal getRebalanceMaxPercent() {
		return this.rebalanceMaxPercent;
	}


	public void setRebalanceMaxPercent(BigDecimal rebalanceMaxPercent) {
		this.rebalanceMaxPercent = rebalanceMaxPercent;
	}
}
