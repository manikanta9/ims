package com.clifton.portfolio.run.process;


import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;


/**
 * The <code>PortfolioRunProcessHandler</code> interface should be implemented by each type of
 * run that we perform. Handles service type specific processing and proper handling of deletes.
 * The handler implementations are mapped to the service type's processing type to determine what to call
 * from {@link PortfolioRunService}
 *
 * @author manderson
 */
public interface PortfolioRunProcessHandler {

	/**
	 * Handles either deleting all of the many side tables associated with the run
	 */
	public void deletePortfolioRunProcessing(int runId);


	public void processPortfolioRun(PortfolioRun run, boolean reloadProxyManagers, int currentDepth);
}
