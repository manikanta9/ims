package com.clifton.portfolio.run.margin;

import com.clifton.portfolio.run.process.PortfolioRunConfig;

import java.util.List;


/**
 * @author manderson
 */
public interface PortfolioRunMarginHandler<T extends PortfolioRunConfig> {


	/**
	 * Called by PortfolioRunMarginCalculator during run processing to generate the List (usually only one, but could be more) on PortfolioRunMargin objects for the run
	 */
	public List<PortfolioRunMargin> generatePortfolioRunMarginList(T runConfig);
}
