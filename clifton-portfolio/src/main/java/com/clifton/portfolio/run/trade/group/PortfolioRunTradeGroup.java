package com.clifton.portfolio.run.trade.group;

import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.investment.account.group.InvestmentAccountGroup;

import java.util.Date;


/**
 * @author manderson
 */
public interface PortfolioRunTradeGroup {


	public ServiceProcessingTypes getProcessingType();


	public InvestmentAccountGroup getClientAccountGroup();


	public Date getBalanceDate();


	public boolean isMarketOnClose();


	public boolean isValidateRuns();

	////////////////////////////////////////////////////////////////////////////////


	public String getWarningMessage();


	public void setWarningMessage(String warningMessage);


	public Date getTradeDate();


	public void setTradeDate(Date tradeDate);
}
