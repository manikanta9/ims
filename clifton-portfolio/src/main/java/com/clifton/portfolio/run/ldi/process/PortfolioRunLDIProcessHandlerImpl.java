package com.clifton.portfolio.run.ldi.process;


import com.clifton.portfolio.account.ldi.PortfolioAccountLDIService;
import com.clifton.portfolio.account.ldi.PortfolioAccountLDIServiceImpl;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIService;
import com.clifton.portfolio.run.process.BasePortfolioRunProcessHandlerImpl;
import org.springframework.stereotype.Component;


/**
 * The <code>PortfolioRunLDIProcessHandlerImpl</code> ...
 *
 * @author manderson
 */
@Component
public class PortfolioRunLDIProcessHandlerImpl extends BasePortfolioRunProcessHandlerImpl<PortfolioRunLDIConfig> {

	private PortfolioAccountLDIService portfolioAccountLDIService;
	private PortfolioRunLDIService portfolioRunLDIService;


	////////////////////////////////////////////////////////////////////////////
	////////               Run Setup Methods                            ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected PortfolioRunLDIConfig setupPortfolioRunConfig(PortfolioRun run, boolean reloadProxyManagers, int currentDepth) {
		PortfolioRunLDIConfig config = new PortfolioRunLDIConfig(run, reloadProxyManagers, currentDepth);
		populatePortfolioRunConfig(config);

		config.setUseMonthEndValues((Boolean) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(run.getClientInvestmentAccount(),
				PortfolioAccountLDIServiceImpl.CUSTOM_COLUMN_USE_MONTH_END_KRD_VALUES));

		config.setDoNotEnforceSameKRDDates((Boolean) getPortfolioAccountDataRetriever().getPortfolioAccountCustomFieldValue(run.getClientInvestmentAccount(),
				PortfolioAccountLDIServiceImpl.CUSTOM_COLUMN_DO_NOT_ENFORCE_SAME_KRD_VALUE_DATES));

		config.setKeyRateDurationPointListMap(getPortfolioAccountLDIService().getPortfolioAccountKeyRateDurationPointListMapForAccount(run.getClientInvestmentAccount().getId()));
		return config;
	}


	////////////////////////////////////////////////////////////////////////////
	////////             Run Clear / Delete Methods                     ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void deletePortfolioRunProcessing(int runId) {
		getPortfolioRunLDIService().deletePortfolioRunProcessing(runId);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRunLDIService getPortfolioRunLDIService() {
		return this.portfolioRunLDIService;
	}


	public void setPortfolioRunLDIService(PortfolioRunLDIService portfolioRunLDIService) {
		this.portfolioRunLDIService = portfolioRunLDIService;
	}


	public PortfolioAccountLDIService getPortfolioAccountLDIService() {
		return this.portfolioAccountLDIService;
	}


	public void setPortfolioAccountLDIService(PortfolioAccountLDIService portfolioAccountLDIService) {
		this.portfolioAccountLDIService = portfolioAccountLDIService;
	}
}
