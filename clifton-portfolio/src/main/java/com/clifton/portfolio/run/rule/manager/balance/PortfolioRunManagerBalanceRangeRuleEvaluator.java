package com.clifton.portfolio.run.rule.manager.balance;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.portfolio.run.rule.manager.BasePortfolioRunManagerAccountRangeRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorUtils;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * PortfolioRunManagerBalanceRangeRuleEvaluator evaluates manager specific rules based on the amount ranges around the manager balance information
 * 1. Manager Cash balance should not fall outside of specified range.
 * 2. Manager Securities balance should not fall outside of specified range
 * 3. Manager balance (cash and securities) should not fall outside of specified range
 *
 * @author manderson
 */
public class PortfolioRunManagerBalanceRangeRuleEvaluator extends BasePortfolioRunManagerAccountRangeRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	private InvestmentManagerAccountService investmentManagerAccountService;

	private static final String CASH_BALANCE = "CashBalance";
	private static final String CASH_PERCENTAGE = "CashPercentage";

	private static final String SECURITIES_BALANCE = "SecuritiesBalance";
	private static final String SECURITIES_PERCENTAGE = "SecuritiesPercentage";

	private static final String TOTAL_BALANCE = "TotalBalance";
	private static final String TOTAL_PERCENTAGE = "TotalPercentage";

	private static final String PORTFOLIO_CASH_PERCENTAGE = "PortfolioCashPercentage";
	private static final String PORTFOLIO_SECURITIES_PERCENTAGE = "PortfolioSecuritiesPercentage";
	private static final String PORTFOLIO_TOTAL_PERCENTAGE = "PortfolioTotalPercentage";


	/**
	 * Cash Balance, Securities Balance, or Total Balance Range Rule
	 */
	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();

		Collection<InvestmentManagerAccountBalance> balanceList = context.getInvestmentManagerAccountBalanceList(run);
		List<Long> managerAccountIdsChecked = new ArrayList<>();
		for (InvestmentManagerAccountBalance balance : CollectionUtils.getIterable(balanceList)) {
			ruleViolationList.addAll(evaluateManagerAccountBalanceRule(run, balance, ruleConfig));
			managerAccountIdsChecked.add(BeanUtils.getIdentityAsLong(balance.getManagerAccount()));
		}

		// Special Additional Rule Evaluation For Managers
		// Each individual rule assignment could potentially have a rule for a manager account not directly assigned, which would add it to the list for that specific rule evaluation
		// This is because a manager account may be linked to the account through a rollup, but only a specific child has a particular rule defined.  Or, a rollup manager could be defined to group
		// managers together for the purposes of evaluating a rule against that group through the rollup manager.  (i.e. Manager X + Manager Y balance combined should never go below 1 million)
		for (Long managerAccountId : CollectionUtils.getIterable(ruleConfig.getEntityIdList())) {
			managerAccountIdsChecked.add(managerAccountId);
			if (!managerAccountIdsChecked.contains(managerAccountId) && (!ruleConfig.getEntityConfig(managerAccountId).isExcluded())) {
				InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(MathUtils.getNumberAsInteger(managerAccountId), run.getBalanceDate(), false);
				if (balance != null) {
					ruleViolationList.addAll(evaluateManagerAccountBalanceRule(run, balance, ruleConfig));
				}
			}
		}
		return ruleViolationList;
	}


	private List<RuleViolation> evaluateManagerAccountBalanceRule(PortfolioRun run, InvestmentManagerAccountBalance balance, RuleConfig ruleConfig) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = getEntityConfig(balance.getManagerAccount(), ruleConfig);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			BigDecimal rangeAmount = null;
			switch (getRangeType()) {
				case CASH_BALANCE:
				case CASH_PERCENTAGE:
				case PORTFOLIO_CASH_PERCENTAGE:
					rangeAmount = (run.isMarketOnCloseAdjustmentsIncluded() ? balance.getMarketOnCloseCashValue() : balance.getAdjustedCashValue());
					break;
				case SECURITIES_BALANCE:
				case SECURITIES_PERCENTAGE:
				case PORTFOLIO_SECURITIES_PERCENTAGE:
					rangeAmount = (run.isMarketOnCloseAdjustmentsIncluded() ? balance.getMarketOnCloseSecuritiesValue() : balance.getAdjustedSecuritiesValue());
					break;
				case TOTAL_BALANCE:
				case TOTAL_PERCENTAGE:
				case PORTFOLIO_TOTAL_PERCENTAGE:
					rangeAmount = (run.isMarketOnCloseAdjustmentsIncluded() ? balance.getMarketOnCloseTotalValue() : balance.getAdjustedTotalValue());
					break;
			}
			if (getRangeType().contains("Percentage")) {
				BigDecimal totalValue = (run.isMarketOnCloseAdjustmentsIncluded() ? balance.getMarketOnCloseTotalValue() : balance.getAdjustedTotalValue());
				if (getRangeType().contains("Portfolio")) {
					totalValue = run.getPortfolioTotalValue();
				}
				rangeAmount = CoreMathUtils.getPercentValue(rangeAmount, totalValue, true);
			}
			if (rangeAmount != null) {
				Map<String, Object> templateValues = new HashMap<>();
				if (RuleEvaluatorUtils.isEntityConfigRangeViolated(rangeAmount, entityConfig, templateValues)) {
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithEntityAndCause(entityConfig, BeanUtils.getIdentityAsLong(run), BeanUtils.getIdentityAsLong(balance.getManagerAccount()), BeanUtils.getIdentityAsLong(balance), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}
}
