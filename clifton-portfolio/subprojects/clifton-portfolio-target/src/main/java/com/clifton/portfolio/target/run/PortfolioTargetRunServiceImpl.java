package com.clifton.portfolio.target.run;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.hierarchy.HierarchicalSimpleEntity;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.copy.InvestmentInstrumentCopyService;
import com.clifton.investment.instrument.copy.InvestmentSecurityCopyCommand;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.replication.ReplicationRebalanceTriggerTypes;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.replication.PortfolioReplicationHandler;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.trade.PortfolioCurrencyTrade;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.manager.PortfolioTargetRunManagerAccount;
import com.clifton.portfolio.target.manager.search.PortfolioTargetRunManagerAccountSearchForm;
import com.clifton.portfolio.target.run.search.PortfolioTargetRunReplicationSearchForm;
import com.clifton.portfolio.target.run.search.PortfolioTargetRunTargetSummaryCommand;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;


/**
 * @author michaelm
 * @see PortfolioTargetRunService
 */
@Service
public class PortfolioTargetRunServiceImpl implements PortfolioTargetRunService {

	private AdvancedUpdatableDAO<PortfolioTargetRunManagerAccount, Criteria> portfolioTargetRunManagerAccountDAO;
	private AdvancedUpdatableDAO<PortfolioTargetRunReplication, Criteria> portfolioTargetRunReplicationDAO;

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentInstrumentCopyService investmentInstrumentCopyService;
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private PortfolioReplicationHandler portfolioReplicationHandler;
	private PortfolioRunService portfolioRunService;

	////////////////////////////////////////////////////////////////////////////
	///////           Portfolio Target Run Business Methods            /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioRun deletePortfolioTargetRunProcessing(PortfolioRun run) {
		// TODO
		//  For the ProductOverlayService this is a one stop shop for deleting run processing; for now, do the same here.
		//  The direction we want to go for target processing is to have each component that saves information also be responsible for deleting the data as necessary.
		getPortfolioRunService().deletePortfolioCurrencyOtherListByRun(run.getId());
		getPortfolioTargetRunReplicationDAO().deleteList(getPortfolioTargetRunReplicationListByRun(run.getId()));
		getPortfolioTargetRunManagerAccountDAO().deleteList(getPortfolioTargetRunManagerAccountListByRun(run.getId()));
		run.setCustomColumns(null);
		return getPortfolioRunService().savePortfolioRun(run);
	}

	////////////////////////////////////////////////////////////////////////////
	/////       Portfolio Target Manager Account Business Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioTargetRunManagerAccount getPortfolioTargetRunManagerAccount(int id) {
		PortfolioTargetRunManagerAccount bean = getPortfolioTargetRunManagerAccountDAO().findByPrimaryKey(id);
		if (bean != null) {
			bean.setBalance(getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(bean.getManagerAccountAssignment().getReferenceOne().getId(),
					bean.getOverlayRun().getBalanceDate(), true));
		}
		return bean;
	}


	@Override
	public List<PortfolioTargetRunManagerAccount> getPortfolioTargetRunManagerAccountListByRun(int runId) {
		PortfolioTargetRunManagerAccountSearchForm searchForm = new PortfolioTargetRunManagerAccountSearchForm();
		searchForm.setRunId(runId);
		return getPortfolioTargetRunManagerAccountList(searchForm);
	}


	@Override
	public List<PortfolioTargetRunManagerAccount> getPortfolioTargetRunManagerAccountList(PortfolioTargetRunManagerAccountSearchForm searchForm) {
		// withCashValuesOnly is a custom search restriction that has special restrictions
		// remove it because there is no definition attached to it.
		if (searchForm.containsSearchRestriction("withCashValuesOnly")) {
			searchForm.setWithCashValuesOnly((Boolean) searchForm.getSearchRestriction("withCashValuesOnly").getValue());
			searchForm.removeSearchRestriction("withCashValuesOnly");
		}

		if (searchForm.containsSearchRestriction("ourCompanyAssignment")) {
			searchForm.setOurCompanyAssignment((Boolean) searchForm.getSearchRestriction("ourCompanyAssignment").getValue());
			searchForm.removeSearchRestriction("ourCompanyAssignment");
		}

		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (BooleanUtils.isTrue(searchForm.getWithCashValuesOnly())) {
					criteria.add(Restrictions.or(Restrictions.ne("cashAllocation", BigDecimal.ZERO), Restrictions.ne("previousDayCashAllocation", BigDecimal.ZERO)));
				}

				if (BooleanUtils.isTrue(searchForm.getOurCompanyAssignment())) {
					String alias = getPathAlias("managerAccountAssignment.referenceOne.managerCompany.type", criteria);
					criteria.add(Restrictions.eq(alias + ".ourCompany", true));
				}
			}
		};
		return getPortfolioTargetRunManagerAccountDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public void savePortfolioTargetRunManagerAccountListByRun(int runId, BigDecimal totalCash, List<PortfolioTargetRunManagerAccount> saveList) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		List<PortfolioTargetRunManagerAccount> originalList = getPortfolioTargetRunManagerAccountListByRun(runId);
		// Sort in Level Order so All parents are saved first
		saveList.sort(Comparator.comparingInt(HierarchicalSimpleEntity::getLevel));
		getPortfolioTargetRunManagerAccountDAO().saveList(saveList, originalList);
		if (CollectionUtils.isEmpty(saveList)) {
			run.setCashTotal(null);
		}
		else {
			run.setCashTotal(totalCash);
		}
		getPortfolioRunService().savePortfolioRun(run);
	}


	@Override
	public void recalculatePortfolioTargetRunReplicationSecurityInfo(PortfolioTargetRunReplication replication) {
		getPortfolioReplicationHandler().recalculateInvestmentReplicationSecurityAllocation(replication);
	}

	////////////////////////////////////////////////////////////////////////////
	////////      Portfolio Target Run Target Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioTargetRunTargetSummary getPortfolioTargetRunTargetSummary(PortfolioTargetRunTargetSummaryCommand command) {
		command.validate(); // validates Run ID is populated
		ValidationUtils.assertNotNull(command.getTargetId(), "Portfolio Run ID and Target ID are required to build a Run Target Summary.");

		PortfolioTargetRunReplicationSearchForm searchForm = new PortfolioTargetRunReplicationSearchForm();
		BeanUtils.copyProperties(command, searchForm);
		List<PortfolioTargetRunReplication> runReplicationList = getPortfolioTargetRunReplicationList(searchForm);
		return CollectionUtils.getOnlyElement(getPortfolioTargetRunTargetSummaryListForRunReplicationList(runReplicationList));
	}


	@Override
	@Transactional(readOnly = true)
	public List<PortfolioTargetRunTargetSummary> getPortfolioTargetRunTargetSummaryList(PortfolioTargetRunTargetSummaryCommand command) {
		command.validate();

		PortfolioTargetRunReplicationSearchForm searchForm = new PortfolioTargetRunReplicationSearchForm();
		BeanUtils.copyProperties(command, searchForm);
		List<PortfolioTargetRunReplication> runReplicationList = getPortfolioTargetRunReplicationList(searchForm);
		return getPortfolioTargetRunTargetSummaryListForRunReplicationList(runReplicationList);
	}


	private List<PortfolioTargetRunTargetSummary> getPortfolioTargetRunTargetSummaryListForRunReplicationList(List<PortfolioTargetRunReplication> runReplicationList) {
		if (CollectionUtils.isEmpty(runReplicationList)) {
			return Collections.emptyList();
		}

		Map<PortfolioTarget, PortfolioTargetRunTargetSummary> targetRunTargetMap = new HashMap<>();
		for (PortfolioTargetRunReplication runReplication : CollectionUtils.getIterable(runReplicationList)) {
			PortfolioTargetRunTargetSummary runTarget = targetRunTargetMap.computeIfAbsent(runReplication.getTarget(),
					target -> {
						PortfolioTargetRunTargetSummary newRunTarget = new PortfolioTargetRunTargetSummary();
						newRunTarget.setTarget(runReplication.getTarget());
						newRunTarget.setOverlayRun(runReplication.getPortfolioRun());
						newRunTarget.setReplicationList(new ArrayList<>());
						return newRunTarget;
					});
			updateRunTargetValues(runTarget, runReplication);
		}

		return new ArrayList<>(targetRunTargetMap.values());
	}


	private PortfolioTargetRunTargetSummary updateRunTargetValues(PortfolioTargetRunTargetSummary runTarget, PortfolioTargetRunReplication runReplication) {
		// update values
		runTarget.setTargetAllocation(MathUtils.add(runTarget.getTargetAllocation(), runReplication.getTargetExposure()));
		runTarget.setTargetAllocationAdjusted(MathUtils.add(runTarget.getTargetAllocationAdjusted(), runReplication.getTargetExposureAdjusted()));
		runTarget.setActualAllocation(MathUtils.add(runTarget.getActualAllocation(), runReplication.getActualExposure()));
		runTarget.setActualAllocationAdjusted(MathUtils.add(runTarget.getActualAllocationAdjusted(), runReplication.getActualExposureAdjusted()));

		// update percentages
		BigDecimal portfolioTotalValue = runTarget.getOverlayRun().getPortfolioTotalValue();
		runTarget.setActualAllocationPercent(MathUtils.round(getPercentValueOfTotal(runTarget, "Physical Exposure %", runTarget.getActualAllocation(), portfolioTotalValue), 2));
		runTarget.setActualAllocationAdjustedPercent(MathUtils.round(getPercentValueOfTotal(runTarget, "Physical Exposure Adjusted %", runTarget.getActualAllocationAdjusted(), portfolioTotalValue), 2));
		runTarget.setTargetAllocationPercent(MathUtils.round(getPercentValueOfTotal(runTarget, "Target %", runTarget.getTargetAllocation(), portfolioTotalValue), 2));
		runTarget.setTargetAllocationAdjustedPercent(MathUtils.round(getPercentValueOfTotal(runTarget, "Adjusted Target %", runTarget.getTargetAllocationAdjusted(), portfolioTotalValue), 2));

		if (runReplication.isRebalanceTriggersUsed()) {

			BigDecimal minPercent = runReplication.getRebalanceTriggerMin();
			BigDecimal maxPercent = runReplication.getRebalanceTriggerMax();
			BigDecimal absMinPercent = MathUtils.abs(minPercent);
			BigDecimal absMaxPercent = MathUtils.abs(maxPercent);

			BigDecimal percent = runTarget.getRebalanceExposureTargetPercent();
			// In cases of Negative Targets, we need to use the absolute value, so that our min/max rebalance triggers are calculated correctly
			percent = MathUtils.abs(percent);

			if (runReplication.getReplication().getRebalanceTriggerType() == ReplicationRebalanceTriggerTypes.PROPORTIONAL) {
				// RECALC PERCENTAGE PROPORTIONALLY - CALCULATING AGAINST ADJUSTED TARGET
				minPercent = MathUtils.getPercentageOf(minPercent, percent, true);
				maxPercent = MathUtils.getPercentageOf(maxPercent, percent, true);
				absMinPercent = MathUtils.getPercentageOf(absMinPercent, percent, true);
				absMaxPercent = MathUtils.getPercentageOf(absMaxPercent, percent, true);
			}

			// Calculate actual amount based on given percentage - Negate Min Amounts
			BigDecimal rebalTotal = runTarget.getOverlayRun().getPortfolioTotalValue();
			runTarget.setRebalanceTriggerMin(MathUtils.negate(MathUtils.getPercentageOf(minPercent, rebalTotal, true)));
			runTarget.setRebalanceTriggerAbsoluteMin(MathUtils.negate(MathUtils.getPercentageOf(absMinPercent, rebalTotal, true)));

			runTarget.setRebalanceTriggerMax(MathUtils.getPercentageOf(maxPercent, rebalTotal, true));
			runTarget.setRebalanceTriggerAbsoluteMax(MathUtils.getPercentageOf(absMaxPercent, rebalTotal, true));
		}

		runTarget.getReplicationList().add(runReplication);

		return runTarget;
	}


	private BigDecimal getPercentValueOfTotal(PortfolioTargetRunTargetSummary runTarget, String fieldName, BigDecimal value, BigDecimal total) {
		BigDecimal result;
		try {
			result = CoreMathUtils.getPercentValue(value, total, true, true);
		}
		catch (ValidationException e) {
			throw new ValidationExceptionWithCause("PortfolioTarget", runTarget.getTarget().getId(), "Error calculating [" + fieldName + "] for asset class [" + runTarget.getLabel()
					+ "]: " + e.getMessage());
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	//////        Portfolio Target Run Replication Business Methods       //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioTargetRunReplication getPortfolioTargetRunReplication(int id) {
		return getPortfolioTargetRunReplicationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortfolioTargetRunReplication> getPortfolioTargetRunReplicationListByRun(int runId) {
		PortfolioTargetRunReplicationSearchForm searchForm = new PortfolioTargetRunReplicationSearchForm();
		searchForm.setRunId(runId);
		return getPortfolioTargetRunReplicationList(searchForm);
	}


	@Override
	public List<PortfolioTargetRunReplication> getPortfolioTargetRunReplicationListByTarget(int targetId) {
		PortfolioTargetRunReplicationSearchForm searchForm = new PortfolioTargetRunReplicationSearchForm();
		searchForm.setTargetId(targetId);
		return getPortfolioTargetRunReplicationList(searchForm);
	}


	@Override
	public List<PortfolioTargetRunReplication> getPortfolioTargetRunReplicationList(PortfolioTargetRunReplicationSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public boolean configureOrderBy(Criteria criteria) {
				List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(searchForm.getOrderBy());
				if (CollectionUtils.isEmpty(orderByList)) {
					String targetAlias = getPathAlias("target", criteria);
					criteria.createAlias("security", "sec");
					criteria.createAlias("replication", "rp");
					criteria.createAlias("rp.type", "rpt");
					criteria.createAlias("replicationAllocationHistory", "rah");
					criteria.createAlias("rah.replicationAllocation", "ra");
					criteria.addOrder(Order.asc(targetAlias + ".order"));
					criteria.addOrder(Order.asc(targetAlias + ".name"));
					criteria.addOrder(Order.asc("rpt.order"));
					criteria.addOrder(Order.asc("rp.id"));
					criteria.addOrder(Order.asc("ra.order"));
					criteria.addOrder(Order.asc("sec.endDate"));
				}
				return true;
			}
		};
		return getPortfolioTargetRunReplicationDAO().findBySearchCriteria(config);
	}


	@Override
	public List<PortfolioTargetRunReplication> getPortfolioTargetRunReplicationListByAccountMainRun(int investmentAccountId, Date startDate, Date endDate, boolean excludeMatchingReplications) {
		HibernateSearchConfigurer searchConfig = criteria -> {
			criteria.createAlias("target", "t");
			criteria.createAlias("portfolioRun", "run");
			criteria.add(Restrictions.eq("run.clientInvestmentAccount.id", investmentAccountId));
			criteria.add(Restrictions.eq("run.mainRun", true));
			if (startDate != null) {
				criteria.add(Restrictions.ge("run.balanceDate", startDate));
			}
			if (endDate != null) {
				criteria.add(Restrictions.le("run.balanceDate", endDate));
			}
			if (excludeMatchingReplications) {
				// TODO
				// criteria.add(Restrictions.or(Restrictions.eqProperty("replication.id", "oac.primaryReplication.id"),
				// 		Restrictions.and(Restrictions.isNotNull("oac.secondaryReplication.id"), Restrictions.eqProperty("replication.id", "oac.secondaryReplication.id"))));
			}
		};
		return getPortfolioTargetRunReplicationDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public PortfolioTargetRunReplication savePortfolioTargetRunReplication(PortfolioTargetRunReplication bean) {
		return getPortfolioTargetRunReplicationDAO().save(bean);
	}


	@Override
	public void savePortfolioTargetRunReplicationList(List<PortfolioTargetRunReplication> saveList) {
		getPortfolioTargetRunReplicationDAO().saveList(saveList);
	}


	@Override
	public void savePortfolioTargetRunReplicationListByRun(int runId, BigDecimal overlayTargetTotal, BigDecimal overlayExposureTotal, BigDecimal mispricingTotal, List<PortfolioTargetRunReplication> saveList) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		List<PortfolioTargetRunReplication> originalList = getPortfolioTargetRunReplicationListByRun(runId);
		getPortfolioTargetRunReplicationDAO().saveList(saveList, originalList);
		if (CollectionUtils.isEmpty(saveList)) {
			run.setOverlayTargetTotal(null);
			run.setOverlayExposureTotal(null);
			run.setMispricingTotal(null);
			run.setPortfolioTotalValue(null);
		}
		else {
			run.setOverlayTargetTotal(overlayTargetTotal);
			run.setOverlayExposureTotal(overlayExposureTotal);
			run.setMispricingTotal(mispricingTotal);
			// todo should anything else be included in TPV? if so this may not be the right place to set it
			//  Setting TPV to target total so percentages show accurately, may be wrong
			run.setPortfolioTotalValue(overlayTargetTotal);
		}
		getPortfolioRunService().savePortfolioRun(run);
	}


	@Override
	@Transactional
	public PortfolioTargetRunReplication copyPortfolioTargetRunReplicationOption(int id, BigDecimal strikePrice, BigDecimal priceOverride) {
		PortfolioTargetRunReplication replicationToCopy = getPortfolioTargetRunReplication(id);
		InvestmentSecurityCopyCommand command = new InvestmentSecurityCopyCommand();
		command.setSecurityToCopy(replicationToCopy.getSecurity());
		command.setStrikePrice(strikePrice);
		command.setReturnExisting(true);
		InvestmentSecurity newOption = getInvestmentInstrumentCopyService().saveInvestmentSecurityOptionWithCommand(command);
		return copyPortfolioTargetRunReplication(id, newOption.getId(), priceOverride);
	}


	@Override
	@Transactional
	public PortfolioTargetRunReplication copyPortfolioTargetRunReplication(int id, int newSecurityId, BigDecimal priceOverride) {
		PortfolioTargetRunReplication replicationToCopy = getPortfolioTargetRunReplication(id);
		InvestmentSecurity newSecurity = getInvestmentInstrumentService().getInvestmentSecurity(newSecurityId);
		List<PortfolioTargetRunReplication> existingReplicationList = getPortfolioTargetRunReplicationListByRun(replicationToCopy.getPortfolioRun().getId());
		return copyPortfolioTargetRunReplication(replicationToCopy, newSecurity, existingReplicationList, priceOverride, false);
	}


	private PortfolioTargetRunReplication copyPortfolioTargetRunReplication(PortfolioTargetRunReplication replicationToCopy, InvestmentSecurity newSecurity, List<PortfolioTargetRunReplication> existingReplicationList, BigDecimal priceOverride, boolean excludeMispricing) {
		if (!getPortfolioReplicationHandler().validateInvestmentReplicationSecurityAllocationSecurityChange(replicationToCopy, newSecurity, PortfolioTargetRunReplication::getTarget, existingReplicationList, !replicationToCopy.isTradeEntryDisabled())) {
			return null;
		}

		PortfolioTargetRunReplication newReplication = new PortfolioTargetRunReplication();
		newReplication.setPortfolioRun(replicationToCopy.getPortfolioRun());
		newReplication.setTarget(replicationToCopy.getTarget());
		newReplication.setReplication(replicationToCopy.getReplication());
		newReplication.setReplicationAllocationHistory(replicationToCopy.getReplicationAllocationHistory());
		newReplication.setMatchingAllocation(replicationToCopy.isMatchingAllocation());
		newReplication.setTargetExposure(BigDecimal.ZERO);
		newReplication.setTargetExposureAdjusted(BigDecimal.ZERO);
		newReplication.setAllocationWeightAdjusted(BigDecimal.ZERO);

		String exchangeRateDataSourceName = getMarketDataExchangeRatesApiService().getExchangeRateDataSourceForClientAccount(replicationToCopy.getClientAccount().toClientAccount());
		// If Overridden will be set - set both so will be flagged manually overridden
		if (priceOverride != null) {
			newReplication.setTradeSecurityPrice(priceOverride);
		}
		getPortfolioReplicationHandler().setupInvestmentReplicationSecurityAllocation(newReplication, newSecurity, exchangeRateDataSourceName, excludeMispricing);
		PortfolioTargetRunReplication savedNewReplication = savePortfolioTargetRunReplication(newReplication);

		// Add it to the full list so it's included in future checks for duplicates
		existingReplicationList.add(newReplication);

		// If trading not disabled, see if any matching with trading disabled and do we need to copy there too?
		// If not position excluded replication, check if there is one with the same security and update that security
		if (!replicationToCopy.isTradeEntryDisabled()) {
			List<PortfolioTargetRunReplication> affectedList = BeanUtils.filter(existingReplicationList, PortfolioTargetRunReplication::getSecurity, replicationToCopy.getSecurity());
			affectedList = BeanUtils.filter(affectedList, PortfolioTargetRunReplication::isTradeEntryDisabled);
			for (PortfolioTargetRunReplication affectedBean : CollectionUtils.getIterable(affectedList)) {
				copyPortfolioTargetRunReplication(affectedBean, newSecurity, existingReplicationList, priceOverride, excludeMispricing);
			}
		}

		return savedNewReplication;
	}


	@Override
	public void processContractSplitting(PortfolioAccountContractStore contractStore, List<PortfolioTargetRunReplication> replicationList) {
		for (PortfolioAccountContractStore.PortfolioAccountContractSecurityStore store : CollectionUtils.getIterable(contractStore.getSecurityStoreCollection())) {
			processContractSplittingForSecurity(contractStore, replicationList, store);
		}
	}


	private void processContractSplittingForSecurity(PortfolioAccountContractStore contractStore, List<PortfolioTargetRunReplication> replicationList, PortfolioAccountContractStore.PortfolioAccountContractSecurityStore store) {
		List<PortfolioTargetRunReplication> affectedReplicationList = BeanUtils.filter(replicationList, replication -> replication.getSecurity().getId(), store.getSecurityId());
		if (!CollectionUtils.isEmpty(affectedReplicationList)) {
			PortfolioAccountContractStore.SecurityPositionQuantity positionTotalQuantity = store.getPositionQuantity();
			BigDecimal totalQuantity = positionTotalQuantity == null ? null : positionTotalQuantity.getQuantityForReplicationList(affectedReplicationList);
			PortfolioTargetRunReplicationPositionAllocationConfig config = new PortfolioTargetRunReplicationPositionAllocationConfig(contractStore.getType(), affectedReplicationList, totalQuantity);
			config.process();
		}
	}


	@Override
	public void processCurrencyContracts(PortfolioAccountContractStore contracts, PortfolioRun run, List<PortfolioTargetRunReplication> currencyReplicationList, List<PortfolioCurrencyOther> currencyOtherList,
	                                     List<PortfolioCurrencyTrade<PortfolioTargetRunReplication>> currencyTradeList, List<PortfolioTargetRunReplication> fullReplicationList) {
		BiFunction<PortfolioAccountContractStore, PortfolioTargetRunReplication, List<AccountingPosition>> replicationPositionFunction = (c, rep) ->
				c.getSecurityAccountingPositionList(rep.getSecurity().getId());
		Function<PortfolioTargetRunReplication, BaseSimpleEntity<Integer>> matchingFilterFunction = PortfolioTargetRunReplication::getTarget;
		getPortfolioReplicationHandler().processPortfolioReplicationListForCurrencyContracts(contracts, replicationPositionFunction, run, currencyReplicationList, currencyOtherList, currencyTradeList, fullReplicationList, matchingFilterFunction);
	}

	////////////////////////////////////////////////////////////////////////////
	//////         Portfolio Target Run Balance Business Methods          //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortfolioTargetBalance> getPortfolioTargetBalanceListByRun(int runId) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		ValidationUtils.assertNotNull(run, "Unable to find Run with id: " + runId);
		return CollectionUtils.createList(CustomJsonStringUtils.getColumnValueAsObject(run.getCustomColumns(), PortfolioRun.CUSTOM_FIELD_TARGET_BALANCE_LIST, PortfolioTargetBalance[].class));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortfolioTargetRunManagerAccount, Criteria> getPortfolioTargetRunManagerAccountDAO() {
		return this.portfolioTargetRunManagerAccountDAO;
	}


	public void setPortfolioTargetRunManagerAccountDAO(AdvancedUpdatableDAO<PortfolioTargetRunManagerAccount, Criteria> portfolioTargetRunManagerAccountDAO) {
		this.portfolioTargetRunManagerAccountDAO = portfolioTargetRunManagerAccountDAO;
	}


	public AdvancedUpdatableDAO<PortfolioTargetRunReplication, Criteria> getPortfolioTargetRunReplicationDAO() {
		return this.portfolioTargetRunReplicationDAO;
	}


	public void setPortfolioTargetRunReplicationDAO(AdvancedUpdatableDAO<PortfolioTargetRunReplication, Criteria> portfolioTargetRunReplicationDAO) {
		this.portfolioTargetRunReplicationDAO = portfolioTargetRunReplicationDAO;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentInstrumentCopyService getInvestmentInstrumentCopyService() {
		return this.investmentInstrumentCopyService;
	}


	public void setInvestmentInstrumentCopyService(InvestmentInstrumentCopyService investmentInstrumentCopyService) {
		this.investmentInstrumentCopyService = investmentInstrumentCopyService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public PortfolioReplicationHandler getPortfolioReplicationHandler() {
		return this.portfolioReplicationHandler;
	}


	public void setPortfolioReplicationHandler(PortfolioReplicationHandler portfolioReplicationHandler) {
		this.portfolioReplicationHandler = portfolioReplicationHandler;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}
}
