package com.clifton.portfolio.target.run.process;


import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.process.BasePortfolioRunProcessHandlerImpl;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.PortfolioTargetService;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.calculator.PortfolioTargetCalculator;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;


/**
 * {@link PortfolioRunTargetProcessHandlerImpl} handles the setup and deletion of portfolio processing for {@link com.clifton.business.service.ServiceProcessingTypes#PORTFOLIO_TARGET}.
 *
 * @author michaelm
 */
public class PortfolioRunTargetProcessHandlerImpl extends BasePortfolioRunProcessHandlerImpl<PortfolioTargetRunConfig> {

	private PortfolioTargetService portfolioTargetService;
	private PortfolioTargetRunService portfolioTargetRunService;

	////////////////////////////////////////////////////////////////////////////
	////////                 Run Setup Methods                          ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected PortfolioTargetRunConfig setupPortfolioRunConfig(PortfolioRun run, boolean reloadProxyManagers, int currentDepth) {
		PortfolioTargetRunConfig runConfig = new PortfolioTargetRunConfig(run, reloadProxyManagers, currentDepth);
		populatePortfolioRunConfig(runConfig);
		// Look up active targets for client account and balance date. Do not want the hierarchy of parent/child defined.
		runConfig.setPortfolioTargetList(getPortfolioTargetService().getPortfolioTargetListByClientAccount(runConfig.getClientInvestmentAccountId(), runConfig.getBalanceDate(), false));
		CollectionUtils.createList(CustomJsonStringUtils.getColumnValueAsObject(run.getCustomColumns(), PortfolioRun.CUSTOM_FIELD_TARGET_BALANCE_LIST, PortfolioTargetBalance[].class)).forEach(runConfig::addPortfolioTargetBalance);
		return runConfig;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Run Clear/Delete Methods                        ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void deletePortfolioRunProcessing(int runId) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRun(runId);
		run = getPortfolioTargetRunService().deletePortfolioTargetRunProcessing(run);

		PortfolioTargetRunConfig runConfig = setupPortfolioRunConfig(run, false, 0);

		for (PortfolioTarget target : CollectionUtils.getIterable(runConfig.getPortfolioTargetList())) {
			PortfolioTargetCalculator targetCalculator = (PortfolioTargetCalculator) getSystemBeanService().getBeanInstance(target.getTargetCalculatorBean());
			targetCalculator.deleteProcessing(target, runConfig);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetService getPortfolioTargetService() {
		return this.portfolioTargetService;
	}


	public void setPortfolioTargetService(PortfolioTargetService portfolioTargetService) {
		this.portfolioTargetService = portfolioTargetService;
	}


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}
}
