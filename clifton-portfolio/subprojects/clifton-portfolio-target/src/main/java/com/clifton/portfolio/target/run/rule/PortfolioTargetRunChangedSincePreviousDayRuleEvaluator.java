package com.clifton.portfolio.target.run.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.target.run.PortfolioTargetRunReplication;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * This rule evaluator aggregates the total exposure across all replications for a given run, and compares that with the total exposure for the previous day.
 * If the difference exceeds a user-defined threshold (specified in percentage of the previous day's total), then a violation will be generated.
 *
 * @author mitchellf
 */
public class PortfolioTargetRunChangedSincePreviousDayRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunTargetRuleEvaluatorContext> {

	private PortfolioTargetRunService portfolioTargetRunService;
	private PortfolioRunService portfolioRunService;

	private BigDecimal thresholdPercent;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunTargetRuleEvaluatorContext context) {

		List<RuleViolation> ruleViolationList = new ArrayList<>();

		BigDecimal totalExposure = getTotalExposure(run, context);

		PortfolioRun previousRun = getPortfolioRunService().getPortfolioRunByMainRun(run.getClientInvestmentAccount().getId(), DateUtils.addWeekDays(run.getBalanceDate(), -1));

		// rollup total
		BigDecimal previousTotal = getTotalExposure(previousRun, context);

		// compare diff to threshold
		if (!MathUtils.isEqual(previousTotal, BigDecimal.ZERO)) {
			BigDecimal diff = MathUtils.absoluteDiff(totalExposure, previousTotal);
			BigDecimal diffPercent = MathUtils.multiply(MathUtils.abs(MathUtils.divide(diff, previousTotal)), 100);

			if (MathUtils.isGreaterThan(diffPercent, getThresholdPercent())) {
				EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(run.getClientInvestmentAccount()));
				Map<String, Object> templateValues = new HashMap<>();
				StringBuilder violationNote = new StringBuilder();
				violationNote.append("Total exposure $")
						.append(CoreMathUtils.formatNumberMoney(totalExposure))
						.append(" differed from the previous exposure $")
						.append(CoreMathUtils.formatNumberMoney(previousTotal))
						.append(" by ")
						.append(MathUtils.round(diffPercent, 2))
						.append("%, which is greater than the maximum deviation of ")
						.append(getThresholdPercent())
						.append("%.");
				templateValues.put("violationNote", violationNote.toString());
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, BeanUtils.getIdentityAsLong(run), templateValues));
			}
		}

		return ruleViolationList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private BigDecimal getTotalExposure(PortfolioRun run, PortfolioRunTargetRuleEvaluatorContext context) {

		List<PortfolioTargetRunReplication> replicationList = context.getReplicationList(run);

		if (!CollectionUtils.isEmpty(replicationList)) {

			// rollup total
			return CoreMathUtils.sum(replicationList.stream()
					.map(PortfolioTargetRunReplication::getActualExposureAdjusted)
					.collect(Collectors.toList()));
		}
		return BigDecimal.ZERO;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public BigDecimal getThresholdPercent() {
		return this.thresholdPercent;
	}


	public void setThresholdPercent(BigDecimal thresholdPercent) {
		this.thresholdPercent = thresholdPercent;
	}
}
