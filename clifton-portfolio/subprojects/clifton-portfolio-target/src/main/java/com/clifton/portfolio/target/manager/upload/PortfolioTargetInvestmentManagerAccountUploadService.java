package com.clifton.portfolio.target.manager.upload;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountAllocation;


/**
 * <code>PortfolioTargetInvestmentManagerAccountUploadService</code> contains methods for uploading data from an external file into target manager related tables
 *
 * @author nickk
 */
public interface PortfolioTargetInvestmentManagerAccountUploadService {


	////////////////////////////////////////////////////////////////////////////
	///////       Target Manager Account Allocation Upload Methods       ///////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = PortfolioTargetInvestmentManagerAccountAllocation.class)
	public void uploadPortfolioTargetInvestmentManagerAccountAllocationUploadFile(PortfolioTargetInvestmentManagerAccountAllocationUploadCommand uploadCommand);
}
