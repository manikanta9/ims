package com.clifton.portfolio.target.run.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.portfolio.run.rule.BaseManagerAccountVsGLAssetsRuleEvaluator;
import com.clifton.portfolio.target.manager.PortfolioTargetRunManagerAccount;
import com.clifton.portfolio.target.manager.search.PortfolioTargetRunManagerAccountSearchForm;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author mitchellf
 */
public class PortfolioTargetRunManagerAccountVsGLAssetsRuleEvaluator extends BaseManagerAccountVsGLAssetsRuleEvaluator {

	private PortfolioTargetRunService portfolioTargetRunService;


	@Override
	protected BigDecimal getManagerAccountAssets(Integer runId) {
		BigDecimal managerAccountAssets = BigDecimal.ZERO;
		PortfolioTargetRunManagerAccountSearchForm portfolioTargetRunManagerAccountSearchForm = new PortfolioTargetRunManagerAccountSearchForm();
		portfolioTargetRunManagerAccountSearchForm.setRunId(runId);
		portfolioTargetRunManagerAccountSearchForm.setRootOnly(true);
		List<PortfolioTargetRunManagerAccount> productOverlayManagerAccountList =
				getPortfolioTargetRunService().getPortfolioTargetRunManagerAccountList(portfolioTargetRunManagerAccountSearchForm);
		for (PortfolioTargetRunManagerAccount productOverlayManagerAccount : CollectionUtils.getIterable(productOverlayManagerAccountList)) {
			managerAccountAssets = MathUtils.add(managerAccountAssets, MathUtils.add(productOverlayManagerAccount.getCashAllocation(),
					productOverlayManagerAccount.getSecuritiesAllocation()));
		}
		return managerAccountAssets;
	}


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}
}
