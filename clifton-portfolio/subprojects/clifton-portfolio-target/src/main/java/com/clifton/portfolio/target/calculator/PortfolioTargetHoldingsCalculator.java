package com.clifton.portfolio.target.calculator;

import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.position.calculator.AccountingPositionValueCalculator;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * {@link PortfolioTargetHoldingsCalculator} is an implementation of {@link PortfolioTargetCalculator} that uses various filtering criteria to calculate a holdings value for
 * a given Balance Date. The total holdings value is calculated using a combination of {@link AccountingPosition}s (when a {@link #positionValueCalculatorBean} is specified) and
 * non-Position values when one or more {@link CashOptions} are defined.
 * <p>
 * NOTE: If no account-related filters are selected, the Client Account of the {@link PortfolioTarget} will be used.
 *
 * @author michaelm
 */
public class PortfolioTargetHoldingsCalculator extends BasePortfolioTargetCalculator implements ValidationAware {

	/**
	 * Selecting a {@link AccountingPositionValueCalculator} will cause the {@link PortfolioTargetHoldingsCalculator} to include {@link AccountingPosition} holdings (in addition to
	 * Cash holdings if one or more {@link CashOptions} are defined). This algorithm is applied to each {@link AccountingPosition} individually, but is not applied to Cash holdings.
	 */
	private SystemBean positionValueCalculatorBean;

	/**
	 * Selecting one or more {@link CashOptions} will cause the {@link PortfolioTargetHoldingsCalculator} to include Cash holdings (in addition to {@link AccountingPosition}
	 * holdings if a {@link AccountingPositionValueCalculator} is defined). The {@link com.clifton.investment.instrument.InvestmentSecurity} related filters will not apply to
	 * {@link CashOptions} unless {@link #applyInvestmentFiltersToCash} is checked.
	 */
	private CashOptions[] cashOptions;

	/**
	 * Client Investment Accounts filter criteria. If no account-related filters are selected, the Client Account of the Portfolio Target will be used.
	 */
	private Integer[] clientAccountIds;

	/**
	 * Holding Investment Accounts filter criteria. If no account-related filters are selected, the Client Account of the Portfolio Target will be used.
	 */
	private Integer[] holdingAccountIds;

	/**
	 * Client Account Group filter criteria. If no account-related filters are selected, the Client Account of the Portfolio Target will be used.
	 */
	private Integer clientAccountGroupId;

	private Integer[] investmentSecurityIds;

	private Integer instrumentId;

	private Short hierarchyId;

	private Short[] investmentSecurityGroupIds;

	private Short[] investmentGroupIds;

	/**
	 * If checked, will add one business day to the Balance Date when looking up holdings. This can be useful if positions are updated on the morning that a run is processed
	 * because runs are processed using the previous business day as the Balance Date.
	 */
	private boolean useBalanceDatePlusOne;

	/**
	 * By default, {@link #cashOptions} are only filtered by {@link com.clifton.investment.account.InvestmentAccount} related filters. In order to filter {@link #cashOptions} by
	 * {@link com.clifton.investment.instrument.InvestmentSecurity} related fields, this option must be selected.
	 */
	private boolean applyInvestmentFiltersToCash;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private AccountingAccountService accountingAccountService;

	private AccountingPositionService accountingPositionService;

	private AccountingBalanceService accountingBalanceService;

	private CalendarBusinessDayService calendarBusinessDayService;

	private SystemBeanService systemBeanService;

	private InvestmentInstrumentService investmentInstrumentService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		ValidationUtils.assertTrue(getPositionValueCalculatorBean() != null || !ArrayUtils.isEmpty(getCashOptions()), "Holdings Target Calculators must have either a Value Calculator or Cash Options.");
	}


	@Override
	public void validateTarget(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		super.validateTarget(portfolioTarget, portfolioTargetRunConfig);
		validate();
	}


	@Override
	@Transactional
	public PortfolioTargetBalance calculate(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		return calculate(portfolioTarget, portfolioTargetRunConfig.getBalanceDate());
	}


	@Override
	public PortfolioTargetBalance previewTargetBalance(PortfolioTarget portfolioTarget, Date balanceDate) {
		return calculate(portfolioTarget, balanceDate);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetBalance calculate(PortfolioTarget portfolioTarget, Date balanceDate) {
		Date transactionDate = getTransactionDateUsingBalanceDateAdjustment(balanceDate);
		BigDecimal positionsHoldingsValue = calculateHoldingsUsingAccountingPositions(portfolioTarget, transactionDate);
		BigDecimal cashHoldingsValue = calculateHoldingsUsingAccountingBalances(portfolioTarget, transactionDate);
		StringBuilder description = new StringBuilder("Balance calculated for Transaction Date: " + DateUtils.fromDateShort(transactionDate) + StringUtils.NEW_LINE);
		description.append(StringUtils.TAB + "Positions Value: " + positionsHoldingsValue + StringUtils.NEW_LINE);
		description.append(StringUtils.TAB + "Cash Value: " + cashHoldingsValue);
		return createNewTargetBalance(portfolioTarget, balanceDate, description.toString(), MathUtils.add(positionsHoldingsValue, cashHoldingsValue), false, true);
	}


	private BigDecimal calculateHoldingsUsingAccountingPositions(PortfolioTarget portfolioTarget, Date transactionDate) {
		if (getPositionValueCalculatorBean() == null) {
			return BigDecimal.ZERO;
		}
		AccountingPositionValueCalculator valueCalculator = (AccountingPositionValueCalculator) getSystemBeanService().getBeanInstance(getPositionValueCalculatorBean());
		return CoreMathUtils.sumProperty(getAccountingPositionList(portfolioTarget, transactionDate), position -> valueCalculator.calculate(position, transactionDate));
	}


	private BigDecimal calculateHoldingsUsingAccountingBalances(PortfolioTarget portfolioTarget, Date transactionDate) {
		List<AccountingBalance> balanceList = getAccountingBalanceList(portfolioTarget, transactionDate);
		return CoreMathUtils.sumProperty(balanceList, AccountingBalance::getBaseAmount);
	}


	/**
	 * When no {@link com.clifton.investment.account.InvestmentAccount}s or {@link com.clifton.investment.account.group.InvestmentAccountGroup}s are configured on the calculator,
	 * the Client Account of the {@link PortfolioTarget} should be used. This will allow users to re-use common holdings calculations across accounts, saving time and avoiding a
	 * large list of {@link PortfolioTargetCalculator}s that are frustrating to sort through.
	 */
	private Integer[] getDefaultClientAccountIdFilter(PortfolioTarget portfolioTarget) {
		if (ArrayUtils.isEmpty(getClientAccountIds()) && ArrayUtils.isEmpty(getHoldingAccountIds()) && getClientAccountGroupId() == null) {
			return new Integer[]{portfolioTarget.getClientAccount().getId()};
		}
		return null;
	}


	private List<AccountingPosition> getAccountingPositionList(PortfolioTarget portfolioTarget, Date transactionDate) {
		AccountingPositionCommand positionCommand = new AccountingPositionCommand();
		populateInvestmentAccountRelatedSearchFormValues(portfolioTarget, transactionDate, positionCommand);
		populateInvestmentSecurityRelatedSearchFormValues(portfolioTarget, transactionDate, positionCommand);
		return getAccountingPositionService().getAccountingPositionListUsingCommand(positionCommand);
	}


	private List<AccountingBalance> getAccountingBalanceList(PortfolioTarget portfolioTarget, Date transactionDate) {
		List<AccountingBalance> balanceList = new ArrayList<>();
		Set<Short> accountingAccountIdsSet = new HashSet<>();
		ArrayUtils.getStream(getCashOptions()).forEach(cashOption ->
				accountingAccountIdsSet.addAll(CollectionUtils.getConverted(getAccountingAccountService().getAccountingAccountListByIds(cashOption.getAccountingAccountId()), BaseSimpleEntity::getId)));
		if (!CollectionUtils.isEmpty(accountingAccountIdsSet)) {
			AccountingBalanceSearchForm searchForm = new AccountingBalanceSearchForm();
			populateInvestmentAccountRelatedSearchFormValues(portfolioTarget, transactionDate, searchForm);
			if (isApplyInvestmentFiltersToCash()) {
				populateInvestmentSecurityRelatedSearchFormValues(portfolioTarget, transactionDate, searchForm);
			}
			searchForm.setAccountingAccountIds(CollectionUtils.toArray(accountingAccountIdsSet, Short.class));
			searchForm.setIncludeUnderlyingPrice(false);
			balanceList = getAccountingBalanceService().getAccountingBalanceList(searchForm);
		}
		return balanceList;
	}


	private void populateInvestmentAccountRelatedSearchFormValues(PortfolioTarget portfolioTarget, Date transactionDate, AccountingBalanceSearchForm searchForm) {
		searchForm.setTransactionDate(transactionDate);
		searchForm.setClientInvestmentAccountIds(ArrayUtils.isEmpty(getClientAccountIds()) ? getDefaultClientAccountIdFilter(portfolioTarget) : getClientAccountIds());
		searchForm.setHoldingInvestmentAccountIds(getHoldingAccountIds());
		searchForm.setClientInvestmentAccountGroupId(getClientAccountGroupId());
	}


	private void populateInvestmentSecurityRelatedSearchFormValues(PortfolioTarget portfolioTarget, Date transactionDate, AccountingBalanceSearchForm searchForm) {
		searchForm.setInvestmentSecurityIds(getInvestmentSecurityIds());
		searchForm.setInvestmentInstrumentId(getInstrumentId());
		searchForm.setInstrumentHierarchyId(getHierarchyId());
		searchForm.setInvestmentGroupIds(getInvestmentGroupIds());
		searchForm.setInvestmentSecurityGroupIds(getInvestmentSecurityGroupIds());
	}


	private Date getTransactionDateUsingBalanceDateAdjustment(Date balanceDate) {
		return isUseBalanceDatePlusOne() ? getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(balanceDate)) : balanceDate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * {@link CashOptions} is used in conjunction with {@link AccountingAccountService#getAccountingAccountListByIds(AccountingAccountIdsCacheImpl.AccountingAccountIds)}
	 * to aggregate a list of {@link com.clifton.accounting.account.AccountingAccount}s which are used to calculated holdings. This allows the user to perform logical OR
	 * filtering rather than being limited to the AND filtering that {@link com.clifton.core.dataaccess.search.form.SearchForm} functionality uses.
	 * For example, the user could CASH and COLLATERAL while excluding CASH_RECEIVABLE.
	 */
	public enum CashOptions {
		/**
		 * Cash excluding Collateral, Receivables, and Positions
		 * <p>
		 * Examples:
		 * {@link com.clifton.accounting.account.AccountingAccount#ASSET_CASH}
		 * {@link com.clifton.accounting.account.AccountingAccount#ASSET_MONEY_MARKET}
		 */
		CASH(AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS_EXCLUDE_COLLATERAL),
		/**
		 * Cash and Collateral, excluding Receivables and Positions.
		 * <p>
		 * Examples:
		 * {@link com.clifton.accounting.account.AccountingAccount#ASSET_CASH_COLLATERAL}
		 * {@link com.clifton.accounting.account.AccountingAccount#ASSET_CURRENCY_COLLATERAL}
		 * {@link com.clifton.accounting.account.AccountingAccount#COUNTERPARTY_CASH_COLLATERAL}
		 * {@link com.clifton.accounting.account.AccountingAccount#COUNTERPARTY_CASH}
		 */
		COLLATERAL(AccountingAccountIdsCacheImpl.AccountingAccountIds.COLLATERAL_ACCOUNTS_EXCLUDE_RECEIVABLE_COLLATERAL_EXCLUDE_POSITIONS),
		/**
		 * Receivables excluding Cash, Collateral, and Positions.
		 * <p>
		 * Examples:
		 * {@link com.clifton.accounting.account.AccountingAccount#ASSET_DIVIDEND_RECEIVABLE}
		 * {@link com.clifton.accounting.account.AccountingAccount#ASSET_INTEREST_RECEIVABLE}
		 * {@link com.clifton.accounting.account.AccountingAccount#ASSET_PAYMENT_RECEIVABLE}
		 */
		RECEIVABLE_ONLY(AccountingAccountIdsCacheImpl.AccountingAccountIds.RECEIVABLE_ACCOUNTS_EXCLUDE_POSITION_EXCLUDE_COLLATERAL);


		CashOptions(AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountId) {
			this.accountingAccountId = accountingAccountId;
		}


		private final AccountingAccountIdsCacheImpl.AccountingAccountIds accountingAccountId;


		public AccountingAccountIdsCacheImpl.AccountingAccountIds getAccountingAccountId() {
			return this.accountingAccountId;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBean getPositionValueCalculatorBean() {
		return this.positionValueCalculatorBean;
	}


	public void setPositionValueCalculatorBean(SystemBean positionValueCalculatorBean) {
		this.positionValueCalculatorBean = positionValueCalculatorBean;
	}


	public CashOptions[] getCashOptions() {
		return this.cashOptions;
	}


	public void setCashOptions(CashOptions[] cashOptions) {
		this.cashOptions = cashOptions;
	}


	public Integer[] getClientAccountIds() {
		return this.clientAccountIds;
	}


	public void setClientAccountIds(Integer[] clientAccountIds) {
		this.clientAccountIds = clientAccountIds;
	}


	public Integer[] getHoldingAccountIds() {
		return this.holdingAccountIds;
	}


	public void setHoldingAccountIds(Integer[] holdingAccountIds) {
		this.holdingAccountIds = holdingAccountIds;
	}


	public Integer getClientAccountGroupId() {
		return this.clientAccountGroupId;
	}


	public void setClientAccountGroupId(Integer clientAccountGroupId) {
		this.clientAccountGroupId = clientAccountGroupId;
	}


	public Integer[] getInvestmentSecurityIds() {
		return this.investmentSecurityIds;
	}


	public void setInvestmentSecurityIds(Integer[] investmentSecurityIds) {
		this.investmentSecurityIds = investmentSecurityIds;
	}


	public Integer getInstrumentId() {
		return this.instrumentId;
	}


	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}


	public Short getHierarchyId() {
		return this.hierarchyId;
	}


	public void setHierarchyId(Short hierarchyId) {
		this.hierarchyId = hierarchyId;
	}


	public Short[] getInvestmentSecurityGroupIds() {
		return this.investmentSecurityGroupIds;
	}


	public void setInvestmentSecurityGroupIds(Short[] investmentSecurityGroupIds) {
		this.investmentSecurityGroupIds = investmentSecurityGroupIds;
	}


	public Short[] getInvestmentGroupIds() {
		return this.investmentGroupIds;
	}


	public void setInvestmentGroupIds(Short[] investmentGroupIds) {
		this.investmentGroupIds = investmentGroupIds;
	}


	public boolean isUseBalanceDatePlusOne() {
		return this.useBalanceDatePlusOne;
	}


	public void setUseBalanceDatePlusOne(boolean useBalanceDatePlusOne) {
		this.useBalanceDatePlusOne = useBalanceDatePlusOne;
	}


	public boolean isApplyInvestmentFiltersToCash() {
		return this.applyInvestmentFiltersToCash;
	}


	public void setApplyInvestmentFiltersToCash(boolean applyInvestmentFiltersToCash) {
		this.applyInvestmentFiltersToCash = applyInvestmentFiltersToCash;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
