package com.clifton.portfolio.target.upload;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreMapUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.search.InvestmentReplicationSearchForm;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.PortfolioTargetService;
import com.clifton.portfolio.target.activity.PortfolioTargetActivity;
import com.clifton.portfolio.target.activity.PortfolioTargetActivityService;
import com.clifton.portfolio.target.activity.search.PortfolioTargetActivitySearchForm;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.balance.PortfolioTargetBalanceService;
import com.clifton.portfolio.target.balance.search.PortfolioTargetBalanceSearchForm;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountAllocation;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountService;
import com.clifton.portfolio.target.manager.search.PortfolioTargetInvestmentManagerAccountAllocationSearchForm;
import com.clifton.portfolio.target.search.PortfolioTargetSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import com.clifton.system.upload.SystemUploadHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author nickk
 * @author michaelm
 */
@Service
public class PortfolioTargetUploadServiceImpl implements PortfolioTargetUploadService {

	private InvestmentAccountService investmentAccountService;
	private InvestmentReplicationService investmentReplicationService;

	private PortfolioTargetService portfolioTargetService;
	private PortfolioTargetBalanceService portfolioTargetBalanceService;
	private PortfolioTargetActivityService portfolioTargetActivityService;
	private PortfolioTargetInvestmentManagerAccountService portfolioTargetInvestmentManagerAccountService;

	private SystemBeanService systemBeanService;

	private SystemUploadHandler systemUploadHandler;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public Status uploadPortfolioTargetUploadFile(PortfolioTargetUploadCommand command) {
		List<IdentityObject> targetList = getSystemUploadHandler().convertSystemUploadFileToBeanList(command, false);
		CollectionUtils.getStream(targetList)
				.map(PortfolioTarget.class::cast)
				.forEach(target -> hydrateTargetForUploadEntity(command, target, target));

		List<PortfolioTarget> savedUpdateList = saveUpdatedPortfolioTargetList(command);
		List<PortfolioTarget> savedNewList = saveNewPortfolioTargetList(command);
		Collection<InvestmentAccount> affectedClientAccountSet = CollectionUtils.getFlattenedConverted(PortfolioTarget::getClientAccount, true, savedUpdateList, savedNewList);

		return Status.ofMessage(String.format("Ended %d target(s) and added %d new target(s) across %d client account(s).", savedUpdateList.size(), savedNewList.size(), affectedClientAccountSet.size()));
	}


	@Override
	@Transactional
	public Status uploadPortfolioTargetBalanceUploadFile(PortfolioTargetBalanceUploadCommand command) {
		List<IdentityObject> balanceList = getSystemUploadHandler().convertSystemUploadFileToBeanList(command, false);
		CollectionUtils.getStream(balanceList).forEach(balance -> hydrateTargetBalance(command, (PortfolioTargetBalance) balance));

		List<PortfolioTarget> savedNewTargetList = saveNewPortfolioTargetList(command);
		List<PortfolioTargetBalance> savedUpdatedBalanceList = saveUpdatedPortfolioTargetBalanceList(command);
		List<PortfolioTargetBalance> savedNewBalanceList = saveNewPortfolioTargetBalanceList(command);
		Collection<InvestmentAccount> affectedClientAccountSet = CollectionUtils.getFlattenedConverted(balance -> balance.getTarget().getClientAccount(), true, savedUpdatedBalanceList, savedNewBalanceList);

		return Status.ofMessage(String.format("Added %d new target(s), ended %d target balance(s), and added %d new target balance(s) across %d client account(s).", savedNewTargetList.size(), savedUpdatedBalanceList.size(), savedNewBalanceList.size(), affectedClientAccountSet.size()));
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private PortfolioTarget hydrateTargetForUploadEntity(PortfolioTargetUploadCommand command, PortfolioTarget target, LabeledObject uploadEntity) {
		hydrateTargetClientAccount(command, target, uploadEntity);
		boolean createTarget = true;
		if (uploadEntity instanceof PortfolioTargetBalance) {
			PortfolioTarget existingTarget = getPreUploadPortfolioTargetByName(command, target.getClientAccount().getId(), target.getName());
			if (existingTarget != null) {
				target = existingTarget;
				createTarget = false;
			}
			else {
				if (target.getStartDate() == null) {
					target.setStartDate(ObjectUtils.coalesce(((PortfolioTargetBalance) uploadEntity).getStartDate(), new Date()));
				}
			}
		}
		else {
			if (target.getStartDate() == null && command.getOverlappingTargetOption() != PortfolioTargetUploadCommand.OverlappingTargetOptions.OVERWRITE) {
				target.setStartDate(new Date());
			}
		}

		if (createTarget) {
			hydrateTargetReplication(command, target, uploadEntity);
			hydrateTargetCalculatorBean(command, target, uploadEntity);
			hydrateTargetParentTarget(command, target);
			hydrateTargetMatchingTarget(command, target);
			target = validateTargetForExisting(command, target);
			// add new target by name, which may be used by other targets as parent or matching
			command.getPostUploadClientToActiveTargetMap().computeIfAbsent(target.getClientAccount().getId(), key -> new HashMap<>())
					.putIfAbsent(target.getName(), target);
		}

		return target;
	}


	private PortfolioTarget hydrateTargetClientAccount(PortfolioTargetUploadCommand command, PortfolioTarget target, LabeledObject row) {
		ValidationUtils.assertNotNull(target.getClientAccount(), "Client Account is required for: " + row.getLabel());
		target.setClientAccount(Optional.of(target.getClientAccount())
				.map(InvestmentAccount::getNumber)
				.map(accountNumber -> command.getClientAccountNumberToAccountMap().computeIfAbsent(accountNumber, key -> getInvestmentAccountService().getInvestmentAccountByNumber(key)))
				.orElseThrow(null));
		ValidationUtils.assertNotNull(target.getClientAccount(), "Unable to find Client Account for Number: " + target.getClientAccount().getNumber());
		return target;
	}


	private PortfolioTarget validateTargetForExisting(PortfolioTargetUploadCommand command, PortfolioTarget target) {
		PortfolioTarget existingTarget = getPreUploadPortfolioTargetByName(command, target.getClientAccount().getId(), target.getName());
		if (existingTarget != null) {
			if (command.getOverlappingTargetOption() == null || command.getOverlappingTargetOption() == PortfolioTargetUploadCommand.OverlappingTargetOptions.ERROR) {
				throw new ValidationException("There is an overlapping Target with Name [" + target.getName() + "] and the upload was not defined to end existing targets.");
			}
			else if (command.getOverlappingTargetOption() == PortfolioTargetUploadCommand.OverlappingTargetOptions.OVERWRITE) {
				return overwriteExisting(target, existingTarget, command);
			}
			// if we get here option is REPLACE
			else if (DateUtils.isDateAfterOrEqual(existingTarget.getEndDate(), target.getStartDate())) {
				return replaceExisting(target, existingTarget, command);
			}
		}
		else {
			// new target, add it
			command.getNewTargetList().add(target);
		}
		return target;
	}


	private PortfolioTarget overwriteExisting(PortfolioTarget target, PortfolioTarget existingTarget, PortfolioTargetUploadCommand command) {
		String[] excludeProperties = new String[]{"createUserId", "createDate", "updateUserId", "updateDate", "rv", "id", "startDate"};
		if (!command.isCopyPreviousValues()) {
			List<String> nullFields = BeanUtils.getNullProperties(target);
			if (!CollectionUtils.isEmpty(nullFields)) {
				excludeProperties = ArrayUtils.addAll(excludeProperties, CollectionUtils.toArray(nullFields, String.class));
			}
		}
		BeanUtils.copyProperties(target, existingTarget, excludeProperties);
		command.getUpdateTargetList().add(existingTarget);
		target = existingTarget;
		return target;
	}


	private PortfolioTarget replaceExisting(PortfolioTarget target, PortfolioTarget existingTarget, PortfolioTargetUploadCommand command) {

		if (command.isCopyPreviousValues()) {
			// Find any fields that were populated on the previous target, but are not populated on the new target, and copy them over
			List<String> nonNullFields = BeanUtils.getNonNullProperties(existingTarget);
			List<String> nullFields = BeanUtils.getNullProperties(target);
			List<String> matchingProperties = CollectionUtils.getIntersection(nullFields, nonNullFields);
			if (!CollectionUtils.isEmpty(matchingProperties)) {
				String[] excludeProperties = new String[]{"createUserId", "createDate", "updateUserId", "updateDate", "rv", "id", "identity"};
				for (String propertyName : CollectionUtils.getIterable(matchingProperties)) {
					if (!ArrayUtils.contains(excludeProperties, propertyName)) {
						BeanUtils.setPropertyValue(target, propertyName, BeanUtils.getPropertyValue(existingTarget, propertyName));
					}
				}
			}
		}
		existingTarget.setEndDate(DateUtils.addDays(target.getStartDate(), -1));
		ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(existingTarget.getEndDate(), existingTarget.getStartDate()), "Unable to end overlapping Target with name [" + existingTarget.getName() + "] due to date range conflict.");

		command.getUpdateTargetList().add(existingTarget);
		command.getNewTargetList().add(target);
		return target;
	}


	private PortfolioTarget hydrateTargetReplication(PortfolioTargetUploadCommand command, PortfolioTarget target, LabeledObject row) {
		target.setReplication(Optional.ofNullable(target.getReplication())
				.map(InvestmentReplication::getName)
				.map(replicationName -> command.getReplicationNameToReplicationMap().computeIfAbsent(replicationName,
						key -> {
							InvestmentReplicationSearchForm replicationSearchForm = new InvestmentReplicationSearchForm();
							replicationSearchForm.setNameEquals(key);
							List<InvestmentReplication> replicationList = getInvestmentReplicationService().getInvestmentReplicationList(replicationSearchForm);
							ValidationUtils.assertNotEmpty(replicationList, "Unable to find Replication with name [" + replicationName + "] for entity [" + row.getLabel() +"]");
							return CollectionUtils.getFirstElement(replicationList);
						}))
				.orElse(null));
		return target;
	}


	private PortfolioTarget hydrateTargetCalculatorBean(PortfolioTargetUploadCommand command, PortfolioTarget target, LabeledObject row) {
		target.setTargetCalculatorBean(Optional.ofNullable(target.getTargetCalculatorBean())
				.map(SystemBean::getName)
				.map(calculatorName -> command.getTargetCalculatorBeanNameToSystemBeanMap().computeIfAbsent(calculatorName,
						key -> {
							SystemBeanSearchForm systemBeanSearchForm = new SystemBeanSearchForm();
							systemBeanSearchForm.setName(key);
							List<SystemBean> systemBeanList = getSystemBeanService().getSystemBeanList(systemBeanSearchForm);
							ValidationUtils.assertNotEmpty(systemBeanList, "Unable to find Target Calculator with name [" + calculatorName + "] for: " + row.getLabel());
							return CollectionUtils.getFirstElement(systemBeanList);
						}))
				.orElse(null));
		return target;
	}


	private PortfolioTarget hydrateTargetParentTarget(PortfolioTargetUploadCommand command, PortfolioTarget target) {
		return hydrateTargetRelatedTargetProperty(command, target, target::getParent, target::setParent);
	}


	private PortfolioTarget hydrateTargetMatchingTarget(PortfolioTargetUploadCommand command, PortfolioTarget target) {
		return hydrateTargetRelatedTargetProperty(command, target, target::getMatchingTarget, target::setMatchingTarget);
	}


	private PortfolioTarget hydrateTargetRelatedTargetProperty(PortfolioTargetUploadCommand command, PortfolioTarget target, Supplier<PortfolioTarget> getter, Consumer<PortfolioTarget> setter) {
		setter.accept(Optional.ofNullable(getter.get())
				.map(PortfolioTarget::getName)
				.map(targetName -> getPostUploadPortfolioTargetByName(command, target.getClientAccount().getId(), targetName))
				.orElse(null));
		return target;
	}


	/**
	 * Returns a {@link PortfolioTarget} for the provided client account ID and target name.
	 * All targets active for the current day for the the client are loaded and cached by name to
	 * {@link PortfolioTargetUploadCommand#getPreUploadClientToActiveTargetMap()} on the first use of the client account.
	 */
	private PortfolioTarget getPreUploadPortfolioTargetByName(PortfolioTargetUploadCommand command, Integer clientAccountId, String targetName) {

		Map<String, PortfolioTarget> preTargetMap = command.getPreUploadClientToActiveTargetMap()
				.computeIfAbsent(clientAccountId, key -> lookupActiveTargetMap(clientAccountId));

		Map<String, PortfolioTarget> postTargetMap = CoreMapUtils.clone(preTargetMap);

		command.getPostUploadClientToActiveTargetMap().putIfAbsent(clientAccountId, postTargetMap);

		return preTargetMap.get(targetName);
	}


	private PortfolioTarget getPostUploadPortfolioTargetByName(PortfolioTargetUploadCommand command, Integer clientAccountId, String targetName) {
		return command.getPostUploadClientToActiveTargetMap()
				.computeIfAbsent(clientAccountId, key -> lookupActiveTargetMap(clientAccountId))
				.get(targetName);
	}


	private Map<String, PortfolioTarget> lookupActiveTargetMap(Integer clientAccountId) {
		PortfolioTargetSearchForm searchForm = new PortfolioTargetSearchForm();
		searchForm.setClientAccountId(clientAccountId);
		searchForm.setActiveOnDate(new Date());
		List<PortfolioTarget> clientTargetList = getPortfolioTargetService().getPortfolioTargetList(searchForm);
		return BeanUtils.getBeanMap(clientTargetList, NamedEntityWithoutLabel::getName);
	}


	/**
	 * Save updated {@link PortfolioTarget} per {@link PortfolioTargetUploadCommand#getUpdateTargetList()}.
	 * This save should be done prior to {@link #saveNewPortfolioTargetList(PortfolioTargetUploadCommand)} to
	 * ensure conflicting updates are made prior to new entities being saved.
	 */
	private List<PortfolioTarget> saveUpdatedPortfolioTargetList(PortfolioTargetUploadCommand command) {
		if (CollectionUtils.isEmpty(command.getUpdateTargetList())) {
			return Collections.emptyList();
		}

		return command.getUpdateTargetList().stream()
				.map(target -> getPortfolioTargetService().savePortfolioTarget(target))
				.collect(Collectors.toList());
	}


	/**
	 * Saves the collection of new {@link PortfolioTarget} in {@link PortfolioTargetUploadCommand#getNewTargetList()}.
	 * New targets are saved in the following order to ensure parent and matching references are saved prior to use.
	 * <br/>- Targets without a parent and matching reference
	 * <br/>- Targets without a matching references; parent reference should be saved
	 * <br/>- Targets with a matching reference
	 */
	private List<PortfolioTarget> saveNewPortfolioTargetList(PortfolioTargetUploadCommand command) {
		if (CollectionUtils.isEmpty(command.getNewTargetList())) {
			return Collections.emptyList();
		}

		// Targets with no parent or matching should be saved first; they may be a parent or matching reference
		List<PortfolioTarget> firstSaveList = new ArrayList<>();
		// Targets with a parent but no matching can be saved next; they may be matching reference
		List<PortfolioTarget> secondSaveList = new ArrayList<>();
		// Targets with a matching target will be saved last to ensure all references are saved first
		List<PortfolioTarget> lastSaveList = new ArrayList<>();

		command.getNewTargetList().forEach(target -> {
			if ((target.getParent() == null && target.getMatchingTarget() == null)) {
				firstSaveList.add(target);
			}
			else if (target.getMatchingTarget() != null) {
				secondSaveList.add(target);
			}
			else {
				lastSaveList.add(target);
			}
		});

		List<PortfolioTarget> savedTargets = Stream.of(firstSaveList, secondSaveList, lastSaveList)
				.flatMap(Collection::stream)
				.map(target -> getPortfolioTargetService().savePortfolioTarget(target))
				.collect(Collectors.toList());

		// Need to handle updating Target Activity for replaced targets where applicable
		// Also need to update any manager account allocations
		if (command.getOverlappingTargetOption() == PortfolioTargetUploadCommand.OverlappingTargetOptions.REPLACE) {
			handleTargetActivityAndAllocations(command, savedTargets);
		}

		return savedTargets;
	}


	private void handleTargetActivityAndAllocations(PortfolioTargetUploadCommand command, List<PortfolioTarget> savedTargets) {
		PortfolioTargetUploadCommand.TargetActivityActions action = command.getTargetActivityAction();
		for (PortfolioTarget target : CollectionUtils.getIterable(savedTargets)) {
			PortfolioTarget previousTarget = getPreUploadPortfolioTargetByName(command, target.getClientAccount().getId(), target.getName());
			if (previousTarget != null) {
				if (action != null && action != PortfolioTargetUploadCommand.TargetActivityActions.IGNORE) {
					// Process Activity
					processActivityForExistingTarget(target, previousTarget, action);
				}
				//Process manager allocations always
				processAllocations(previousTarget, target);
			}
		}
	}


	private void processAllocations(PortfolioTarget existingTarget, PortfolioTarget newTarget) {
		PortfolioTargetInvestmentManagerAccountAllocationSearchForm allocationSearchForm = new PortfolioTargetInvestmentManagerAccountAllocationSearchForm();
		allocationSearchForm.setTargetId(existingTarget.getId());
		allocationSearchForm.setInactiveAssignment(false);
		List<PortfolioTargetInvestmentManagerAccountAllocation> targetAllocations = getPortfolioTargetInvestmentManagerAccountService().getPortfolioTargetInvestmentManagerAccountAllocationList(allocationSearchForm);
		targetAllocations.forEach(alloc -> getPortfolioTargetInvestmentManagerAccountService().replacePortfolioTargetInvestmentManagerAccountAllocation(alloc, newTarget));
	}


	private void processActivityForExistingTarget(PortfolioTarget target, PortfolioTarget previousTarget, PortfolioTargetUploadCommand.TargetActivityActions action) {
		PortfolioTargetActivitySearchForm sf = new PortfolioTargetActivitySearchForm();
		sf.setTargetId(previousTarget.getId());
		sf.setActive(true);
		List<PortfolioTargetActivity> activities = getPortfolioTargetActivityService().getPortfolioTargetActivityList(sf);
		if (!CollectionUtils.isEmpty(activities)) {
			for (PortfolioTargetActivity activity : CollectionUtils.getIterable(activities)) {
				if (action == PortfolioTargetUploadCommand.TargetActivityActions.MIGRATE) {
					activity.setTarget(target);
					getPortfolioTargetActivityService().savePortfolioTargetActivity(activity);
				}
				else if (action == PortfolioTargetUploadCommand.TargetActivityActions.COPY) {
					PortfolioTargetActivity newActivity = new PortfolioTargetActivity();
					String[] auditProperties = new String[]{"createUserId", "createDate", "updateUserId", "updateDate", "rv", "id"};
					BeanUtils.copyProperties(activity, newActivity, auditProperties);
					newActivity.setTarget(target);
					getPortfolioTargetActivityService().savePortfolioTargetActivity(newActivity);
				}
			}
		}
	}


	private PortfolioTargetBalance hydrateTargetBalance(PortfolioTargetBalanceUploadCommand command, PortfolioTargetBalance balance) {
		ValidationUtils.assertNotNull(balance.getTarget(), () -> "A Target Balance requires a Target Name reference: " + balance);
		if (balance.getStartDate() == null) {
			balance.setStartDate(new Date());
		}
		balance.setTarget(hydrateTargetForUploadEntity(command, balance.getTarget(), balance));
		validateTargetBalanceForExisting(command, balance);
		command.getNewBalanceList().add(balance);
		return balance;
	}


	private PortfolioTargetBalance validateTargetBalanceForExisting(PortfolioTargetBalanceUploadCommand command, PortfolioTargetBalance balance) {
		PortfolioTargetBalance existingTargetBalance = getActivePortfolioTargetBalance(command, balance.getTarget().getClientAccount().getId(), balance.getTarget().getName());
		if (existingTargetBalance != null) {
			if (command.getOverlappingTargetOption() != PortfolioTargetUploadCommand.OverlappingTargetOptions.ERROR && DateUtils.isDateAfterOrEqual(existingTargetBalance.getEndDate(), balance.getStartDate())) {
				existingTargetBalance.setEndDate(DateUtils.addDays(balance.getStartDate(), -1));
				ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(existingTargetBalance.getEndDate(), existingTargetBalance.getStartDate()), "Unable to end overlapping Target Balance for Target with name [" + existingTargetBalance.getTarget().getName() + "] due to date range conflict.");
				command.getUpdateBalanceList().add(existingTargetBalance);
				// remove existing to add new
				command.getClientToActiveTargetBalanceMap().get(balance.getTarget().getClientAccount().getId()).remove(existingTargetBalance.getTarget().getName());
			}
			else {
				throw new ValidationException("There is an overlapping Target Balance for Target with Name [" + balance.getTarget().getName() + "] and the upload was not defined to end existing target balances.");
			}
		}

		// add new target balance by name, which may be used by other targets as parent or matching
		command.getClientToActiveTargetBalanceMap().get(balance.getTarget().getClientAccount().getId()).put(balance.getTarget().getName(), balance);
		return balance;
	}


	private PortfolioTargetBalance getActivePortfolioTargetBalance(PortfolioTargetBalanceUploadCommand command, Integer clientAccountId, String targetName) {
		return command.getClientToActiveTargetBalanceMap().computeIfAbsent(clientAccountId, key -> {
			PortfolioTargetBalanceSearchForm searchForm = new PortfolioTargetBalanceSearchForm();
			searchForm.setClientAccountId(clientAccountId);
			searchForm.setActiveOnDate(new Date());
			List<PortfolioTargetBalance> clientTargetBalanceList = getPortfolioTargetBalanceService().getPortfolioTargetBalanceList(searchForm);
			return BeanUtils.getBeanMap(clientTargetBalanceList, balance -> balance.getTarget().getName());
		}).get(targetName);
	}


	/**
	 * Save updated {@link PortfolioTargetBalance} per {@link PortfolioTargetBalanceUploadCommand#getUpdateBalanceList()}.
	 * This save should be done prior to {@link #saveNewPortfolioTargetBalanceList(PortfolioTargetBalanceUploadCommand)} to
	 * ensure conflicting updates are made prior to new entities being saved.
	 */
	private List<PortfolioTargetBalance> saveUpdatedPortfolioTargetBalanceList(PortfolioTargetBalanceUploadCommand command) {
		if (CollectionUtils.isEmpty(command.getUpdateBalanceList())) {
			return Collections.emptyList();
		}

		return command.getUpdateBalanceList().stream()
				.map(balance -> getPortfolioTargetBalanceService().savePortfolioTargetBalance(balance))
				.collect(Collectors.toList());
	}


	/**
	 * Save new {@link PortfolioTargetBalance} per {@link PortfolioTargetBalanceUploadCommand#getNewBalanceList()}.
	 */
	private List<PortfolioTargetBalance> saveNewPortfolioTargetBalanceList(PortfolioTargetBalanceUploadCommand command) {
		if (CollectionUtils.isEmpty(command.getNewBalanceList())) {
			return Collections.emptyList();
		}

		return command.getNewBalanceList().stream()
				.map(balance -> getPortfolioTargetBalanceService().savePortfolioTargetBalance(balance))
				.collect(Collectors.toList());
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public PortfolioTargetService getPortfolioTargetService() {
		return this.portfolioTargetService;
	}


	public void setPortfolioTargetService(PortfolioTargetService portfolioTargetService) {
		this.portfolioTargetService = portfolioTargetService;
	}


	public PortfolioTargetBalanceService getPortfolioTargetBalanceService() {
		return this.portfolioTargetBalanceService;
	}


	public void setPortfolioTargetBalanceService(PortfolioTargetBalanceService portfolioTargetBalanceService) {
		this.portfolioTargetBalanceService = portfolioTargetBalanceService;
	}


	public PortfolioTargetActivityService getPortfolioTargetActivityService() {
		return this.portfolioTargetActivityService;
	}


	public void setPortfolioTargetActivityService(PortfolioTargetActivityService portfolioTargetActivityService) {
		this.portfolioTargetActivityService = portfolioTargetActivityService;
	}


	public PortfolioTargetInvestmentManagerAccountService getPortfolioTargetInvestmentManagerAccountService() {
		return this.portfolioTargetInvestmentManagerAccountService;
	}


	public void setPortfolioTargetInvestmentManagerAccountService(PortfolioTargetInvestmentManagerAccountService portfolioTargetInvestmentManagerAccountService) {
		this.portfolioTargetInvestmentManagerAccountService = portfolioTargetInvestmentManagerAccountService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}
}
