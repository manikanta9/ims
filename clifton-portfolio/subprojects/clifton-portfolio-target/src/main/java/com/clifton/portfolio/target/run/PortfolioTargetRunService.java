package com.clifton.portfolio.target.run;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.trade.PortfolioCurrencyTrade;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.manager.PortfolioTargetRunManagerAccount;
import com.clifton.portfolio.target.manager.search.PortfolioTargetRunManagerAccountSearchForm;
import com.clifton.portfolio.target.run.search.PortfolioTargetRunReplicationSearchForm;
import com.clifton.portfolio.target.run.search.PortfolioTargetRunTargetSummaryCommand;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * {@link PortfolioTargetRunService} is responsible for logic and persistence regarding objects saved during the processing of {@link com.clifton.portfolio.run.PortfolioRun}s of type
 * {@link com.clifton.business.service.ServiceProcessingTypes#PORTFOLIO_TARGET}.
 *
 * @author michaelm
 */
public interface PortfolioTargetRunService {

	////////////////////////////////////////////////////////////////////////////
	///////           Portfolio Target Run Business Methods            /////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Deletes all processing related info that was created for this run.
	 */
	@DoNotAddRequestMapping
	public PortfolioRun deletePortfolioTargetRunProcessing(PortfolioRun run);

	////////////////////////////////////////////////////////////////////////////
	/////       Portfolio Target Manager Account Business Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetRunManagerAccount getPortfolioTargetRunManagerAccount(int id);


	public List<PortfolioTargetRunManagerAccount> getPortfolioTargetRunManagerAccountListByRun(int runId);


	public List<PortfolioTargetRunManagerAccount> getPortfolioTargetRunManagerAccountList(PortfolioTargetRunManagerAccountSearchForm searchForm);


	public void savePortfolioTargetRunManagerAccountListByRun(int runId, BigDecimal totalCash, List<PortfolioTargetRunManagerAccount> saveList);


	/**
	 * There are some fields (Delta) whose value is dependent on the target. These values need to be reset after all targets are adjusted.
	 */
	@DoNotAddRequestMapping
	public void recalculatePortfolioTargetRunReplicationSecurityInfo(PortfolioTargetRunReplication replication);

	////////////////////////////////////////////////////////////////////////////
	////////      Portfolio Target Run Target Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Builds a single {@link PortfolioTargetRunTargetSummary} based on the criteria in the {@link PortfolioTargetRunTargetSummaryCommand}.
	 * NOTE: Both Run ID and Target ID are required.
	 */
	@SecureMethod(dtoClass = PortfolioTargetRunReplication.class)
	public PortfolioTargetRunTargetSummary getPortfolioTargetRunTargetSummary(PortfolioTargetRunTargetSummaryCommand command);


	/**
	 * Builds a list of {@link PortfolioTargetRunTargetSummary}s based on the criteria in the {@link PortfolioTargetRunTargetSummaryCommand}.
	 * NOTE: Run ID is required.
	 */
	@SecureMethod(dtoClass = PortfolioTargetRunReplication.class)
	public List<PortfolioTargetRunTargetSummary> getPortfolioTargetRunTargetSummaryList(PortfolioTargetRunTargetSummaryCommand command);

	////////////////////////////////////////////////////////////////////////////
	//////        Portfolio Target Run Replication Business Methods       //////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetRunReplication getPortfolioTargetRunReplication(int id);


	public List<PortfolioTargetRunReplication> getPortfolioTargetRunReplicationListByRun(int runId);


	public List<PortfolioTargetRunReplication> getPortfolioTargetRunReplicationListByTarget(int targetId);


	public List<PortfolioTargetRunReplication> getPortfolioTargetRunReplicationList(PortfolioTargetRunReplicationSearchForm searchForm);


	/**
	 * NOTE FOR ACCOUNTS WITH SUB-ACCOUNTS - IF THERE ARE RUNS AT BOTH LEVELS, IT WILL THE ONE FOUND FOR A SPECIFIC DATE AT THE MAIN LEVEL OTHERWISE THE SUB-ACCOUNT LEVEL.
	 * <p>
	 * This method actually performs two SQL look ups to prevent OR clause across tables which causes major performance issues.
	 */
	public List<PortfolioTargetRunReplication> getPortfolioTargetRunReplicationListByAccountMainRun(final int investmentAccountId, final Date startDate, final Date endDate,
	                                                                                                final boolean excludeMatchingReplications);


	public PortfolioTargetRunReplication savePortfolioTargetRunReplication(PortfolioTargetRunReplication bean);


	public void savePortfolioTargetRunReplicationList(List<PortfolioTargetRunReplication> saveList);


	/**
	 * Gets original list for run and does insert/update/delete where necessary
	 */
	public void savePortfolioTargetRunReplicationListByRun(int runId, BigDecimal overlayTargetTotal, BigDecimal overlayExposureTotal, BigDecimal mispricingTotal,
	                                                       List<PortfolioTargetRunReplication> saveList);


	/**
	 * Creates a copy of the given replication using it for target, replication, etc. and setting the new security id to the new row
	 *
	 * @param id            - replication ID to copy
	 * @param newSecurityId - security ID for the new replication
	 * @param priceOverride - If set, and missing from market data will use this value as previous close price.  Will also set tradeSecurityPrice for price override on trade creation screen
	 */
	public PortfolioTargetRunReplication copyPortfolioTargetRunReplication(int id, int newSecurityId, BigDecimal priceOverride);


	/**
	 * Specialized method of copying an existing replication for options.
	 *
	 * @param id            - replication ID to copy
	 * @param strikePrice   - strike price for the option to include in the new replication
	 * @param priceOverride - If set, and missing from market data will use this value as previous close price.  Will also set tradeSecurityPrice for price override on trade creation screen
	 * @see #copyPortfolioTargetRunReplication(int, int, BigDecimal)
	 */
	public PortfolioTargetRunReplication copyPortfolioTargetRunReplicationOption(int id, BigDecimal strikePrice, BigDecimal priceOverride);

	////////////////////////////////////////////////////////////////////////////
	//////////            Replication Helper Methods                ////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Takes a ContractStore and applies the contract values to the list of replications based upon config data,
	 * i.e. matches client/holding/security
	 * The  total amount of contracts to apply for that security and a list of replications and appropriately splits the total across
	 * the applicable replications.
	 *
	 * @param contractStore   Contract Store - This has a "type" enum on it that holds what we are currently processing: actual, current, pending
	 * @param replicationList full replication list for the run
	 */
	@DoNotAddRequestMapping
	public void processContractSplitting(PortfolioAccountContractStore contractStore, List<PortfolioTargetRunReplication> replicationList);


	/**
	 * Populates Currency Exposure on the Currency Replications and also applies the "other" currency to each replication.
	 * <p>
	 * Currency Trade List is a list of all Physical Currency Exposure (Those tied to replications & other)
	 */
	@DoNotAddRequestMapping
	public void processCurrencyContracts(PortfolioAccountContractStore contracts, PortfolioRun run, List<PortfolioTargetRunReplication> currencyReplicationList, List<PortfolioCurrencyOther> currencyOtherList, List<PortfolioCurrencyTrade<PortfolioTargetRunReplication>> currencyTradeList, List<PortfolioTargetRunReplication> fullReplicationList);

	////////////////////////////////////////////////////////////////////////////
	//////         Portfolio Target Run Balance Business Methods          //////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieve the list of {@link PortfolioTargetBalance} from the {@link PortfolioRun#customColumns} corresponding the given runId.
	 */
	public List<PortfolioTargetBalance> getPortfolioTargetBalanceListByRun(int runId);
}
