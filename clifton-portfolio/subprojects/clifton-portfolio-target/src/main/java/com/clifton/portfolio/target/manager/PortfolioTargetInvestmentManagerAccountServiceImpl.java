package com.clifton.portfolio.target.manager;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.manager.search.PortfolioTargetInvestmentManagerAccountAllocationSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author nickk
 */
@Service
public class PortfolioTargetInvestmentManagerAccountServiceImpl implements PortfolioTargetInvestmentManagerAccountService {

	private InvestmentManagerAccountService investmentManagerAccountService;

	private DaoSingleKeyListCache<PortfolioTargetInvestmentManagerAccountAllocation, Integer> portfolioTargetInvestmentManagerAccountAllocationListByAssignmentCache;

	private AdvancedUpdatableDAO<PortfolioTargetInvestmentManagerAccountAllocation, Criteria> portfolioTargetInvestmentManagerAccountAllocationDAO;

	///////////////////////////////////////////////////////////////////////////
	/////////    Target Manager Assignment Business Services        ///////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public PortfolioTargetInvestmentManagerAccountAssignment getPortfolioTargetInvestmentManagerAccountAssignment(int id) {
		InvestmentManagerAccountAssignment managerAccountAssignment = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignment(id);
		ValidationUtils.assertNotNull(managerAccountAssignment, "Unable to find Manager Account Assignment with ID: " + id);

		PortfolioTargetInvestmentManagerAccountAssignment targetManagerAccountAssignment = new PortfolioTargetInvestmentManagerAccountAssignment(managerAccountAssignment);
		targetManagerAccountAssignment.setTargetAllocationList(getPortfolioTargetInvestmentManagerAccountAllocationList(managerAccountAssignment.getId()));

		return targetManagerAccountAssignment;
	}


	@Override
	@Transactional
	public PortfolioTargetInvestmentManagerAccountAssignment savePortfolioTargetInvestmentManagerAccountAssignment(PortfolioTargetInvestmentManagerAccountAssignment targetManagerAccountAssignment) {
		InvestmentManagerAccountAssignment managerAccountAssignment = targetManagerAccountAssignment.getManagerAccountAssignment();
		boolean newAssignment = managerAccountAssignment.isNewBean();
		Boolean allowPartialAllocation = managerAccountAssignment.getAllowPartialAllocation();
		List<PortfolioTargetInvestmentManagerAccountAllocation> allocationList = validatePortfolioTargetInvestmentManagerAccountAllocationList(targetManagerAccountAssignment, allowPartialAllocation);

		// update manager assigment references
		targetManagerAccountAssignment.setManagerAccountAssignment(getInvestmentManagerAccountService().saveInvestmentManagerAccountAssignment(managerAccountAssignment));

		// get existing lists so that we know what to delete if necessary
		List<PortfolioTargetInvestmentManagerAccountAllocation> oldAllocationList = null;
		if (!newAssignment) {
			oldAllocationList = getPortfolioTargetInvestmentManagerAccountAllocationList(managerAccountAssignment.getId());
		}

		// save the assignment allocations
		getPortfolioTargetInvestmentManagerAccountAllocationDAO().saveList(allocationList, oldAllocationList);

		return targetManagerAccountAssignment;
	}


	@Override
	@Transactional
	public void savePortfolioTargetInvestmentManagerAccountAssignmentList(List<PortfolioTargetInvestmentManagerAccountAssignment> targetManagerAccountAssignmentList) {
		for (PortfolioTargetInvestmentManagerAccountAssignment targetManagerAccountAssignment : CollectionUtils.getIterable(targetManagerAccountAssignmentList)) {
			savePortfolioTargetInvestmentManagerAccountAssignment(targetManagerAccountAssignment);
		}
	}


	@Override
	@Transactional
	public void deletePortfolioTargetInvestmentManagerAccountAssignment(int id) {
		PortfolioTargetInvestmentManagerAccountAssignment assignment = getPortfolioTargetInvestmentManagerAccountAssignment(id);
		// First Delete all Allocations
		if (assignment != null) {
			getPortfolioTargetInvestmentManagerAccountAllocationDAO().deleteList(assignment.getTargetAllocationList());
		}
		// Then delete the assignment
		getInvestmentManagerAccountService().deleteInvestmentManagerAccountAssignment(id);
	}


	@Override
	public List<PortfolioTargetInvestmentManagerAccountAllocation> getPortfolioTargetInvestmentManagerAccountAllocationList(PortfolioTargetInvestmentManagerAccountAllocationSearchForm searchForm) {
		return getPortfolioTargetInvestmentManagerAccountAllocationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public void replacePortfolioTargetInvestmentManagerAccountAllocation(PortfolioTargetInvestmentManagerAccountAllocation existingAllocation, PortfolioTarget newTarget) {
		PortfolioTargetInvestmentManagerAccountAllocation newAllocation = new PortfolioTargetInvestmentManagerAccountAllocation();
		String[] auditProperties = new String[]{"createUserId", "createDate", "updateUserId", "updateDate", "rv", "id"};
		BeanUtils.copyProperties(existingAllocation, newAllocation, auditProperties);
		newAllocation.setTarget(newTarget);
		getPortfolioTargetInvestmentManagerAccountAllocationDAO().delete(existingAllocation);
		getPortfolioTargetInvestmentManagerAccountAllocationDAO().save(newAllocation);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private List<PortfolioTargetInvestmentManagerAccountAllocation> getPortfolioTargetInvestmentManagerAccountAllocationList(int assignmentId) {
		return getPortfolioTargetInvestmentManagerAccountAllocationListByAssignmentCache().getBeanListForKeyValue(getPortfolioTargetInvestmentManagerAccountAllocationDAO(), assignmentId);
	}


	private List<PortfolioTargetInvestmentManagerAccountAllocation> validatePortfolioTargetInvestmentManagerAccountAllocationList(PortfolioTargetInvestmentManagerAccountAssignment targetManagerAccountAssignment, Boolean allowPartialAllocation) {
		// weird issue where allocationList comes back as singleton list - breaks save functionality
		List<PortfolioTargetInvestmentManagerAccountAllocation> allocationList = targetManagerAccountAssignment.getTargetAllocationList() != null ? new ArrayList<>(targetManagerAccountAssignment.getTargetAllocationList()) : Collections.emptyList();

		ValidationUtils.assertNotEmpty(allocationList, "Investment Manager Accounts must have at least one allocation.");

		// As long as NOT null and false (i.e. explicitly false) then run this validation
		if (allowPartialAllocation != null && !allowPartialAllocation) {

			// enforcement of partial allocations restriction should only be applied to active targets
			List<PortfolioTargetInvestmentManagerAccountAllocation> activeAllocations = allocationList.stream()
					.filter(alloc -> alloc.getTarget().isActive())
					.collect(Collectors.toList());
			BigDecimal totalAllocation = CoreMathUtils.sumProperty(activeAllocations, PortfolioTargetInvestmentManagerAccountAllocation::getAllocationPercent);
			ValidationUtils.assertTrue(MathUtils.isEqual(totalAllocation, MathUtils.BIG_DECIMAL_ONE_HUNDRED), "Total Target Allocations for Manager Balance is [" + CoreMathUtils.formatNumberDecimal(totalAllocation) + "] which does not equal 100%.  Please adjust your percentages, or check the <i>Do Not Enforce 100% Allocation<i> option.");
		}
		for (PortfolioTargetInvestmentManagerAccountAllocation allocation : CollectionUtils.getIterable(allocationList)) {
			allocation.setManagerAccountAssignment(targetManagerAccountAssignment.getManagerAccountAssignment());
		}
		return allocationList;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public DaoSingleKeyListCache<PortfolioTargetInvestmentManagerAccountAllocation, Integer> getPortfolioTargetInvestmentManagerAccountAllocationListByAssignmentCache() {
		return this.portfolioTargetInvestmentManagerAccountAllocationListByAssignmentCache;
	}


	public void setPortfolioTargetInvestmentManagerAccountAllocationListByAssignmentCache(DaoSingleKeyListCache<PortfolioTargetInvestmentManagerAccountAllocation, Integer> portfolioTargetInvestmentManagerAccountAllocationListByAssignmentCache) {
		this.portfolioTargetInvestmentManagerAccountAllocationListByAssignmentCache = portfolioTargetInvestmentManagerAccountAllocationListByAssignmentCache;
	}


	public AdvancedUpdatableDAO<PortfolioTargetInvestmentManagerAccountAllocation, Criteria> getPortfolioTargetInvestmentManagerAccountAllocationDAO() {
		return this.portfolioTargetInvestmentManagerAccountAllocationDAO;
	}


	public void setPortfolioTargetInvestmentManagerAccountAllocationDAO(AdvancedUpdatableDAO<PortfolioTargetInvestmentManagerAccountAllocation, Criteria> portfolioTargetInvestmentManagerAccountAllocationDAO) {
		this.portfolioTargetInvestmentManagerAccountAllocationDAO = portfolioTargetInvestmentManagerAccountAllocationDAO;
	}
}
