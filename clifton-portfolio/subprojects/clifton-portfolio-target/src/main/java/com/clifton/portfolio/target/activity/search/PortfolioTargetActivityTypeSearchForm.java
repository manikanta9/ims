package com.clifton.portfolio.target.activity.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author nickk
 */
public class PortfolioTargetActivityTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private Short id;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Boolean securityRequired;

	@SearchField
	private Boolean noteRequired;

	@SearchField
	private Boolean causeRequired;

	@SearchField
	private Boolean position;

	@SearchField
	private Boolean marketOnClose;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getSecurityRequired() {
		return this.securityRequired;
	}


	public void setSecurityRequired(Boolean securityRequired) {
		this.securityRequired = securityRequired;
	}


	public Boolean getNoteRequired() {
		return this.noteRequired;
	}


	public void setNoteRequired(Boolean noteRequired) {
		this.noteRequired = noteRequired;
	}


	public Boolean getCauseRequired() {
		return this.causeRequired;
	}


	public void setCauseRequired(Boolean causeRequired) {
		this.causeRequired = causeRequired;
	}


	public Boolean getPosition() {
		return this.position;
	}


	public void setPosition(Boolean position) {
		this.position = position;
	}


	public Boolean getMarketOnClose() {
		return this.marketOnClose;
	}


	public void setMarketOnClose(Boolean marketOnClose) {
		this.marketOnClose = marketOnClose;
	}
}
