package com.clifton.portfolio.target.calculator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceSearchForm;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountAllocation;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountService;
import com.clifton.portfolio.target.manager.search.PortfolioTargetInvestmentManagerAccountAllocationSearchForm;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <code>PortfolioTargetManagerAllocationCalculator</code> is a {@link PortfolioTargetCalculator}
 * for calculating a {@link PortfolioTargetBalance} from aggregated percentages of manager account
 * balances for a balance date.
 *
 * @author nickk
 */
public class PortfolioTargetManagerAllocationCalculator extends BasePortfolioTargetCalculator {

	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	private PortfolioTargetInvestmentManagerAccountService portfolioTargetInvestmentManagerAccountService;

	private boolean doNotReplaceExistingBalance;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void validateTarget(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		List<PortfolioTargetInvestmentManagerAccountAllocation> managerAllocationList = getTargetManagerAllocationList(portfolioTarget, portfolioTargetRunConfig);
		ValidationUtils.assertNotEmpty(managerAllocationList, "There are no manager allocations available for the target " + portfolioTarget);
	}


	@Override
	public PortfolioTargetBalance calculate(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		validateTarget(portfolioTarget, portfolioTargetRunConfig);

		List<PortfolioTargetInvestmentManagerAccountAllocation> managerAllocationList = getTargetManagerAllocationList(portfolioTarget, portfolioTargetRunConfig);

		StringBuilder descriptionBuilder = new StringBuilder("Balance generated from:\n");
		BigDecimal balance = calculateTargetBalance(portfolioTargetRunConfig, managerAllocationList, descriptionBuilder);

		return createNewTargetWithTargetPercentageApplied(portfolioTarget, portfolioTargetRunConfig.getBalanceDate(), descriptionBuilder.toString(), balance, false);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private List<PortfolioTargetInvestmentManagerAccountAllocation> getTargetManagerAllocationList(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		Map<Integer, List<PortfolioTargetInvestmentManagerAccountAllocation>> activeClientTargetAllocationListMap = portfolioTargetRunConfig.getActiveTargetManagerAllocationListMap();
		if (activeClientTargetAllocationListMap == null) {
			// Load all allocations for the client account, so do not limit to target.
			List<PortfolioTargetInvestmentManagerAccountAllocation> clientTargetManagerAllocationList = getPortfolioTargetInvestmentManagerAccountAllocationList(portfolioTarget, false);

			activeClientTargetAllocationListMap = new HashMap<>();
			for (PortfolioTargetInvestmentManagerAccountAllocation allocation : CollectionUtils.getIterable(clientTargetManagerAllocationList)) {
				portfolioTargetRunConfig.getActiveManagerAccountAssignmentList().add(allocation.getManagerAccountAssignment());
				activeClientTargetAllocationListMap.computeIfAbsent(allocation.getTarget().getId(), k -> new ArrayList<>())
						.add(allocation);
			}
			portfolioTargetRunConfig.setActiveTargetManagerAllocationListMap(activeClientTargetAllocationListMap);
		}
		loadManagerBalanceMap(portfolioTargetRunConfig);

		return activeClientTargetAllocationListMap.get(portfolioTarget.getId());
	}


	private List<PortfolioTargetInvestmentManagerAccountAllocation> getPortfolioTargetInvestmentManagerAccountAllocationList(PortfolioTarget portfolioTarget, boolean limitToTarget) {
		PortfolioTargetInvestmentManagerAccountAllocationSearchForm allocationSearchForm = new PortfolioTargetInvestmentManagerAccountAllocationSearchForm();
		allocationSearchForm.setInactiveAssignment(Boolean.FALSE);
		allocationSearchForm.setInvestmentAccountId(portfolioTarget.getClientAccount().getId());
		if (limitToTarget) {
			allocationSearchForm.setTargetId(portfolioTarget.getId());
		}
		return getPortfolioTargetInvestmentManagerAccountService().getPortfolioTargetInvestmentManagerAccountAllocationList(allocationSearchForm);
	}


	private void loadManagerBalanceMap(PortfolioTargetRunConfig runConfig) {
		if (CollectionUtils.isEmpty(runConfig.getManagerAccountBalanceMap()) && !CollectionUtils.isEmpty(runConfig.getActiveTargetManagerAllocationListMap())) {
			Integer[] managerIds = runConfig.getActiveTargetManagerAllocationListMap().values().stream()
					.flatMap(CollectionUtils::getStream)
					.map(managerAllocation -> managerAllocation.getManagerAccountAssignment().getReferenceOne().getId())
					.distinct()
					.toArray(Integer[]::new);

			InvestmentManagerAccountBalanceSearchForm balanceSearchForm = new InvestmentManagerAccountBalanceSearchForm();
			balanceSearchForm.setBalanceDate(runConfig.getBalanceDate());
			balanceSearchForm.setManagerAccountIds(managerIds);
			List<InvestmentManagerAccountBalance> balanceList = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceList(balanceSearchForm);
			CollectionUtils.getIterable(balanceList)
					.forEach(balance -> runConfig.getManagerAccountBalanceMap().put(balance.getManagerAccount().getId(), balance));
		}
	}


	private BigDecimal calculateTargetBalance(PortfolioTargetRunConfig runConfig, List<PortfolioTargetInvestmentManagerAccountAllocation> managerAllocationList, StringBuilder descriptionBuilder) {
		Map<Integer, InvestmentManagerAccountBalance> managerToBalanceMap = runConfig.getManagerAccountBalanceMap();
		return managerAllocationList.stream()
				.map(managerAllocation -> {
					InvestmentManagerAccountBalance managerBalance = managerToBalanceMap.get(managerAllocation.getManagerAccountAssignment().getReferenceOne().getId());
					BigDecimal managerBalanceValue = runConfig.getRun().isMarketOnCloseAdjustmentsIncluded() ? managerBalance.getMarketOnCloseTotalValue() : managerBalance.getAdjustedTotalValue();
					BigDecimal allocationBalance = MathUtils.getPercentageOf(managerAllocation.getAllocationPercent(), managerBalanceValue, true);
					runConfig.getManagerAccountAllocationToBalanceMap().put(managerAllocation, allocationBalance);
					descriptionBuilder.append('\t')
							.append(CoreMathUtils.formatNumber(managerAllocation.getAllocationPercent(), MathUtils.NUMBER_FORMAT_MONEY4))
							.append("% of ").append(managerBalance.getManagerAccount().getLabel()).append(": ")
							.append(CoreMathUtils.formatNumberMoney(allocationBalance)).append('\n');
					return allocationBalance;
				}).reduce(BigDecimal.ZERO, MathUtils::add);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public PortfolioTargetInvestmentManagerAccountService getPortfolioTargetInvestmentManagerAccountService() {
		return this.portfolioTargetInvestmentManagerAccountService;
	}


	public void setPortfolioTargetInvestmentManagerAccountService(PortfolioTargetInvestmentManagerAccountService portfolioTargetInvestmentManagerAccountService) {
		this.portfolioTargetInvestmentManagerAccountService = portfolioTargetInvestmentManagerAccountService;
	}


	public boolean isDoNotReplaceExistingBalance() {
		return this.doNotReplaceExistingBalance;
	}


	public void setDoNotReplaceExistingBalance(boolean doNotReplaceExistingBalance) {
		this.doNotReplaceExistingBalance = doNotReplaceExistingBalance;
	}
}
