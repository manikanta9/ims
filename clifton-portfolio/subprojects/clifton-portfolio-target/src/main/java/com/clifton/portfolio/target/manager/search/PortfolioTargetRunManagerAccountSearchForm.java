package com.clifton.portfolio.target.manager.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.portfolio.run.manager.search.BasePortfolioRunManagerAccountSearchForm;


/**
 * {@link PortfolioTargetRunManagerAccountSearchForm} provides search configuration functionality for
 * {@link com.clifton.portfolio.target.manager.PortfolioTargetRunManagerAccount} entities.
 *
 * @author michaelm
 */
public class PortfolioTargetRunManagerAccountSearchForm extends BasePortfolioRunManagerAccountSearchForm {

	@SearchField(searchField = "portfolioTarget.id")
	private Integer portfolioTargetId;

	@SearchField(searchField = "portfolioTarget.name")
	private String portfolioTargetName;

	@SearchField
	private Boolean rollupTarget;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getPortfolioTargetId() {
		return this.portfolioTargetId;
	}


	public void setPortfolioTargetId(Integer portfolioTargetId) {
		this.portfolioTargetId = portfolioTargetId;
	}


	public String getPortfolioTargetName() {
		return this.portfolioTargetName;
	}


	public void setPortfolioTargetName(String portfolioTargetName) {
		this.portfolioTargetName = portfolioTargetName;
	}


	public Boolean getRollupTarget() {
		return this.rollupTarget;
	}


	public void setRollupTarget(Boolean rollupTarget) {
		this.rollupTarget = rollupTarget;
	}
}
