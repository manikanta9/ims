package com.clifton.portfolio.target.balance.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.math.BigDecimal;


/**
 * @author nickk
 */
public class PortfolioTargetBalanceSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "target.name,target.description")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField
	private String targetBalanceDescription;

	@SearchField(searchField = "target.id")
	private Integer targetId;

	@SearchField(searchField = "clientAccount.id", searchFieldPath = "target")
	private Integer clientAccountId;

	@SearchField(searchField = "replication.id", searchFieldPath = "target")
	private Integer replicationId;

	@SearchField(searchField = "name", searchFieldPath = "target.replication")
	private String replicationName;

	@SearchField
	private BigDecimal value;

	@SearchField
	private Boolean valueQuantity;

	/**
	 * Custom property to have balance activity loaded
	 */
	private Boolean populateBalanceActivity;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getTargetBalanceDescription() {
		return this.targetBalanceDescription;
	}


	public void setTargetBalanceDescription(String targetBalanceDescription) {
		this.targetBalanceDescription = targetBalanceDescription;
	}


	public Integer getTargetId() {
		return this.targetId;
	}


	public void setTargetId(Integer targetId) {
		this.targetId = targetId;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Integer getReplicationId() {
		return this.replicationId;
	}


	public void setReplicationId(Integer replicationId) {
		this.replicationId = replicationId;
	}


	public String getReplicationName() {
		return this.replicationName;
	}


	public void setReplicationName(String replicationName) {
		this.replicationName = replicationName;
	}


	public BigDecimal getValue() {
		return this.value;
	}


	public void setValue(BigDecimal value) {
		this.value = value;
	}


	public Boolean getValueQuantity() {
		return this.valueQuantity;
	}


	public void setValueQuantity(Boolean valueQuantity) {
		this.valueQuantity = valueQuantity;
	}


	public Boolean getPopulateBalanceActivity() {
		return this.populateBalanceActivity;
	}


	public void setPopulateBalanceActivity(Boolean populateBalanceActivity) {
		this.populateBalanceActivity = populateBalanceActivity;
	}
}
