package com.clifton.portfolio.target.manager.upload;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.upload.BaseInvestmentManagerAccountUploadService;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.PortfolioTargetService;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountAllocation;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountAssignment;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author nickk
 */
@Service
public class PortfolioTargetInvestmentManagerAccountUploadServiceImpl extends BaseInvestmentManagerAccountUploadService implements PortfolioTargetInvestmentManagerAccountUploadService {

	private PortfolioTargetService portfolioTargetService;
	private PortfolioTargetInvestmentManagerAccountService portfolioTargetInvestmentManagerAccountService;

	////////////////////////////////////////////////////////////////////////////
	///////       Target Manager Account Allocation Upload Methods       ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadPortfolioTargetInvestmentManagerAccountAllocationUploadFile(PortfolioTargetInvestmentManagerAccountAllocationUploadCommand uploadCommand) {
		uploadCommand.clearResults();
		List<PortfolioTargetInvestmentManagerAccountAllocationUploadTemplate> beanList = getFileUploadHandler().convertFileUploadFileToBeanList(uploadCommand);
		ValidationUtils.assertNotEmpty(beanList, "No manager allocations found to update.");

		boolean errors = false;
		Map<String, List<PortfolioTargetInvestmentManagerAccountAllocationUploadTemplate>> managerAssignmentUpdateMap = BeanUtils.getBeansMap(beanList, PortfolioTargetInvestmentManagerAccountAllocationUploadTemplate::getManagerAssignmentKey);

		List<PortfolioTargetInvestmentManagerAccountAssignment> saveManagerAssignmentList = new ArrayList<>();

		for (Map.Entry<String, List<PortfolioTargetInvestmentManagerAccountAllocationUploadTemplate>> stringListEntry : managerAssignmentUpdateMap.entrySet()) {
			List<PortfolioTargetInvestmentManagerAccountAllocationUploadTemplate> allocationUploadTemplateList = stringListEntry.getValue();
			// Returns the Active Manager Account Assignment based on the first allocation in the list
			// If returns null, then there was an error and the uploadCommand results will be updated with why
			PortfolioTargetInvestmentManagerAccountAssignment targetManagerAccountAssignment = getManagerAccountAssignment(uploadCommand, allocationUploadTemplateList.get(0));
			if (targetManagerAccountAssignment == null) {
				errors = true;
				continue;
			}

			Set<String> processedTargets = new HashSet<>();
			Set<String> managerAssignmentErrors = new HashSet<>();

			// Assignment already has existing allocations populated
			List<PortfolioTargetInvestmentManagerAccountAllocation> saveAllocationList = new ArrayList<>();
			for (PortfolioTargetInvestmentManagerAccountAllocationUploadTemplate newAllocation : allocationUploadTemplateList) {
				String targetName = newAllocation.getTargetName();
				if (!processedTargets.add(targetName)) {
					managerAssignmentErrors.add("Multiple rows allocated to asset class [" + targetName + "].");
					continue;
				}
				// Find the existing allocation if any
				PortfolioTargetInvestmentManagerAccountAllocation existingAllocation = CollectionUtils.getOnlyElement(BeanUtils.filter(targetManagerAccountAssignment.getTargetAllocationList(), allocation -> allocation.getTarget().getName(), targetName));
				if (existingAllocation == null) {
					if (!uploadCommand.isAllowAddingOrRemovingAllocations()) {
						managerAssignmentErrors.add("There is not an existing allocation for asset class [" + targetName + "], and the option to insert new allocations is turned off.");
						continue;
					}
					// Otherwise create it as a new allocation and add it to the saveList
					PortfolioTarget target = getClientTargetWithName(uploadCommand, managerAssignmentErrors, targetManagerAccountAssignment.getManagerAccountAssignment().getReferenceTwo(), targetName);
					if (target == null) {
						continue;
					}
					PortfolioTargetInvestmentManagerAccountAllocation targetManagerAccountAllocation = new PortfolioTargetInvestmentManagerAccountAllocation();
					targetManagerAccountAllocation.setManagerAccountAssignment(targetManagerAccountAssignment.getManagerAccountAssignment());
					targetManagerAccountAllocation.setTarget(target);
					targetManagerAccountAllocation.setAllocationPercent(newAllocation.getAllocationPercent());
					saveAllocationList.add(targetManagerAccountAllocation);
				}
				else {
					existingAllocation.setAllocationPercent(newAllocation.getAllocationPercent());
					saveAllocationList.add(existingAllocation);
				}
			}

			// If Not Allowed to Removed existing allocations - see what is missing from the list and add it back into the save list
			if (!uploadCommand.isAllowAddingOrRemovingAllocations()) {
				for (PortfolioTargetInvestmentManagerAccountAllocation existingAllocation : CollectionUtils.getIterable(targetManagerAccountAssignment.getTargetAllocationList())) {
					if (!processedTargets.contains(existingAllocation.getTarget().getName())) {
						saveAllocationList.add(existingAllocation);
					}
				}
			}
			if (CollectionUtils.isEmpty(managerAssignmentErrors) && !uploadCommand.isAllowPartiallyAllocatedManagers()) {
				BigDecimal totalAllocated = CoreMathUtils.sumProperty(saveAllocationList, PortfolioTargetInvestmentManagerAccountAllocation::getAllocationPercent);
				if (MathUtils.isNotEqual(totalAllocated, MathUtils.BIG_DECIMAL_ONE_HUNDRED)) {
					managerAssignmentErrors.add("The total allocated is " + CoreMathUtils.formatNumberDecimal(totalAllocated)
							+ " but you have chosen to enforce total to 100.  Please double check your file.  If you are not allowing removal of existing allocations - those asset classes not included in the file will be included at their existing percentage.");
				}
			}

			if (!CollectionUtils.isEmpty(managerAssignmentErrors)) {
				errors = true;
				uploadCommand.appendResult("Manager Assignment [" + targetManagerAccountAssignment.getManagerAccountAssignment().getLabel() + "] Errors: "
						+ StringUtils.collectionToCommaDelimitedString(managerAssignmentErrors));
			}
			else {
				// Update the Assignment with the New List
				targetManagerAccountAssignment.setTargetAllocationList(saveAllocationList);
				saveManagerAssignmentList.add(targetManagerAccountAssignment);
			}
		}

		if (!errors || uploadCommand.isPartialUploadAllowed()) {
			getPortfolioTargetInvestmentManagerAccountService().savePortfolioTargetInvestmentManagerAccountAssignmentList(saveManagerAssignmentList);
			uploadCommand.prependResult(CollectionUtils.getSize(saveManagerAssignmentList) + " Manager Assignments Updated.");
		}
		else {
			uploadCommand.prependResult("No Manager Assignments Updated.");
		}
	}


	/**
	 * Returns the {@link PortfolioTargetInvestmentManagerAccountAssignment} to use for the template row.  Uses a map for client accounts
	 * and manager accounts for common retrievals to prevent going to the database each time. The assignment look up is not cached in a
	 * map because there can't be more than one unique assignment per file.
	 * Note: All existing allocations are already populated on the assignment bean that is returned.
	 */
	@Transactional(readOnly = true)
	protected PortfolioTargetInvestmentManagerAccountAssignment getManagerAccountAssignment(PortfolioTargetInvestmentManagerAccountAllocationUploadCommand uploadCommand, PortfolioTargetInvestmentManagerAccountAllocationUploadTemplate uploadTemplate) {
		InvestmentAccount clientAccount = getClientAccount(uploadCommand, uploadTemplate);
		InvestmentManagerAccount managerAccount = getManagerAccount(uploadCommand, uploadTemplate);
		if (clientAccount == null || managerAccount == null) {
			return null;
		}
		InvestmentManagerAccountAssignment managerAccountAssignment = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentByManagerAndAccount(managerAccount.getId(), clientAccount.getId());
		if (managerAccountAssignment == null) {
			uploadCommand.appendResult("Cannot find manager assignment for client account [" + clientAccount.getNumber() + "] and manager account [" + managerAccount.getAccountNumber() + "]");
			return null;
		}
		return getPortfolioTargetInvestmentManagerAccountService().getPortfolioTargetInvestmentManagerAccountAssignment(managerAccountAssignment.getId());
	}


	/**
	 * Returns the {@link PortfolioTarget} to use for the given account by name.  Checks against valid targets for the account
	 * which are stored in a map for easy retrieval.
	 */
	private PortfolioTarget getClientTargetWithName(PortfolioTargetInvestmentManagerAccountAllocationUploadCommand uploadCommand, Set<String> managerAssignmentErrors, InvestmentAccount clientAccount, String targetName) {
		List<PortfolioTarget> accountTargetList = uploadCommand.getClientAccountTargetListMap().computeIfAbsent(clientAccount.getId(), clientId ->
				getPortfolioTargetService().getPortfolioTargetListByClientAccount(clientAccount.getId(), new Date(), false));
		PortfolioTarget target = CollectionUtils.getOnlyElement(BeanUtils.filter(accountTargetList, PortfolioTarget::getName, targetName));
		if (target == null) {
			managerAssignmentErrors.add("Cannot find active target associated with this account with name [" + targetName + "]");
			return null;
		}
		return target;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////           Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetService getPortfolioTargetService() {
		return this.portfolioTargetService;
	}


	public void setPortfolioTargetService(PortfolioTargetService portfolioTargetService) {
		this.portfolioTargetService = portfolioTargetService;
	}


	public PortfolioTargetInvestmentManagerAccountService getPortfolioTargetInvestmentManagerAccountService() {
		return this.portfolioTargetInvestmentManagerAccountService;
	}


	public void setPortfolioTargetInvestmentManagerAccountService(PortfolioTargetInvestmentManagerAccountService portfolioTargetInvestmentManagerAccountService) {
		this.portfolioTargetInvestmentManagerAccountService = portfolioTargetInvestmentManagerAccountService;
	}
}
