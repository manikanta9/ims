package com.clifton.portfolio.target.upload;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.portfolio.target.PortfolioTarget;


/**
 * <code>PortfolioTargetUploadService</code> is a service for uploading {@link PortfolioTarget} and related entities.
 *
 * @author nickk
 */
public interface PortfolioTargetUploadService {

	@SecureMethod(dtoClass = PortfolioTarget.class)
	public Status uploadPortfolioTargetUploadFile(PortfolioTargetUploadCommand command);


	@SecureMethod(dtoClass = PortfolioTarget.class)
	public Status uploadPortfolioTargetBalanceUploadFile(PortfolioTargetBalanceUploadCommand command);
}
