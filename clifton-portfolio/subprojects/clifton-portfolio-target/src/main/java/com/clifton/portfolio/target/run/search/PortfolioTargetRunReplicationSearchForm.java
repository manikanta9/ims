package com.clifton.portfolio.target.run.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


/**
 * {@link com.clifton.core.dataaccess.search.form.SearchForm} for {@link com.clifton.portfolio.target.run.PortfolioTargetRunReplication}s.
 *
 * @author michaelm
 */
public class PortfolioTargetRunReplicationSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "portfolioRun.id")
	private Integer runId;

	@SearchField(searchField = "target.id")
	private Integer targetId;

	@SearchField(searchField = "rebalanceTriggerMin", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean withRebalanceTriggersOnly;

	@SearchField(searchField = "target.replication.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean withReplicationsOnly;

	@SearchField(searchField = "target.parent.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean rootOnly;

	@SearchField(searchField = "target.rollup")
	private Boolean rollupTarget;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public Integer getTargetId() {
		return this.targetId;
	}


	public void setTargetId(Integer targetId) {
		this.targetId = targetId;
	}


	public Boolean getWithRebalanceTriggersOnly() {
		return this.withRebalanceTriggersOnly;
	}


	public void setWithRebalanceTriggersOnly(Boolean withRebalanceTriggersOnly) {
		this.withRebalanceTriggersOnly = withRebalanceTriggersOnly;
	}


	public Boolean getWithReplicationsOnly() {
		return this.withReplicationsOnly;
	}


	public void setWithReplicationsOnly(Boolean withReplicationsOnly) {
		this.withReplicationsOnly = withReplicationsOnly;
	}


	public Boolean getRootOnly() {
		return this.rootOnly;
	}


	public void setRootOnly(Boolean rootOnly) {
		this.rootOnly = rootOnly;
	}


	public Boolean getRollupTarget() {
		return this.rollupTarget;
	}


	public void setRollupTarget(Boolean rollupTarget) {
		this.rollupTarget = rollupTarget;
	}
}
