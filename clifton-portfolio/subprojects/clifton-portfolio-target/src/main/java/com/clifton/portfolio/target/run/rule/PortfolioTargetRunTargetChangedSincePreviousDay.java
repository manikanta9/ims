package com.clifton.portfolio.target.run.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.PortfolioTargetService;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.balance.PortfolioTargetBalanceService;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This rule evaluator generates violations when a target balance changes too much since the previous day.
 * A target balance is only evaluated if the balance's target . This target calculator bean is identified by ID, as a field
 * of this rule evaluator.
 * <p>
 * A new balance is created each time a run is processed, and a balance will only be active for one day, so the active balance for the previous business day will always be
 * the balance to compare to.
 *
 * @author mitchellf
 */
public class PortfolioTargetRunTargetChangedSincePreviousDay extends BaseRuleEvaluator<PortfolioRun, PortfolioRunTargetRuleEvaluatorContext> implements ValidationAware {

	private PortfolioTargetService portfolioTargetService;
	private PortfolioTargetRunService portfolioTargetRunService;
	private PortfolioTargetBalanceService portfolioTargetBalanceService;
	private PortfolioRunService portfolioRunService;

	private Integer calculatorBeanId;
	private BigDecimal maximumDiffPercent;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunTargetRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();

		List<PortfolioTargetBalance> balances = context.getPortfolioTargetBalanceList(run);

		Date previousDay = DateUtils.addWeekDays(run.getBalanceDate(), -1);
		PortfolioRun previousRun = getPortfolioRunService().getPortfolioRunByMainRun(run.getClientInvestmentAccount().getId(), previousDay);
		List<PortfolioTargetBalance> previousBalances = context.getPortfolioTargetBalanceList(previousRun);

		Map<PortfolioTarget, PortfolioTargetBalance> balanceMap = BeanUtils.getBeanMap(previousBalances, PortfolioTargetBalance::getTarget);

		for (PortfolioTargetBalance balance : CollectionUtils.getIterable(balances)) {

			PortfolioTarget target = balance.getTarget();

			if (target.getTargetCalculatorBean().getId().equals(getCalculatorBeanId())) {

				BigDecimal percentageDiff;
				PortfolioTargetBalance previousBalance = balanceMap.get(target);

				if (previousBalance != null) {
					BigDecimal diff = MathUtils.absoluteDiff(balance.getValue(), previousBalance.getValue());
					if (!MathUtils.isEqual(previousBalance.getValue(), BigDecimal.ZERO)) {
						percentageDiff = MathUtils.multiply(MathUtils.divide(diff, previousBalance.getValue()), 100);
					}
					else if (MathUtils.isEqual(diff, BigDecimal.ZERO)) {
						percentageDiff = BigDecimal.ZERO;
					}
					else {
						percentageDiff = new BigDecimal("100");
					}
				}
				else {
					percentageDiff = new BigDecimal("100");
				}

				if (MathUtils.isGreaterThan(percentageDiff, getMaximumDiffPercent())) {
					EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(target));
					Map<String, Object> templateValues = new HashMap<>();
					StringBuilder violationNote = new StringBuilder();
					violationNote.append("Balance for target ")
							.append(target.getLabel())
							.append(" is $")
							.append(CoreMathUtils.formatNumberMoney(balance.getValue()))
							.append(", which differed from the previous day's balance $")
							.append(CoreMathUtils.formatNumberMoney(previousBalance != null ? previousBalance.getValue() : BigDecimal.ZERO))
							.append(" by ")
							.append(MathUtils.round(percentageDiff, 2))
							.append("%, which is greater than the maximum deviation of ")
							.append(getMaximumDiffPercent())
							.append("%.");
					templateValues.put("violationNote", violationNote.toString());
					ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, BeanUtils.getIdentityAsLong(run), templateValues));
				}
			}
		}

		return ruleViolationList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getMaximumDiffPercent(), "Maximum difference percent must be specified. Must be a number greater than 0.");
		ValidationUtils.assertTrue(MathUtils.isGreaterThanOrEqual(getMaximumDiffPercent(), 0), "Maximum difference percent must be greater than 0.");
		ValidationUtils.assertNotNull(getCalculatorBeanId(), "A calculator bean ID must be specified to indicate which targets should be evaluated.");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetService getPortfolioTargetService() {
		return this.portfolioTargetService;
	}


	public void setPortfolioTargetService(PortfolioTargetService portfolioTargetService) {
		this.portfolioTargetService = portfolioTargetService;
	}


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}


	public Integer getCalculatorBeanId() {
		return this.calculatorBeanId;
	}


	public void setCalculatorBeanId(Integer calculatorBeanId) {
		this.calculatorBeanId = calculatorBeanId;
	}


	public PortfolioTargetBalanceService getPortfolioTargetBalanceService() {
		return this.portfolioTargetBalanceService;
	}


	public void setPortfolioTargetBalanceService(PortfolioTargetBalanceService portfolioTargetBalanceService) {
		this.portfolioTargetBalanceService = portfolioTargetBalanceService;
	}


	public BigDecimal getMaximumDiffPercent() {
		return this.maximumDiffPercent;
	}


	public void setMaximumDiffPercent(BigDecimal maximumDiffPercent) {
		this.maximumDiffPercent = maximumDiffPercent;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}
}
