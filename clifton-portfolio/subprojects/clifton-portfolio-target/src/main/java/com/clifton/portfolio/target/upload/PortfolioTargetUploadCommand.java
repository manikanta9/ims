package com.clifton.portfolio.target.upload;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.upload.SystemUploadCommand;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <code>PortfolioTargetUploadCommand</code> is a simple upload command for {@link PortfolioTarget}s.
 * <p>
 * Local caching is populated on this command during the upload process and should be excluded in the result.
 *
 * @author nickk
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class PortfolioTargetUploadCommand extends SystemUploadCommand {

	/**
	 * Caches for upload processing.
	 */
	private final Map<String, InvestmentAccount> clientAccountNumberToAccountMap = new HashMap<>();
	private final Map<String, InvestmentReplication> replicationNameToReplicationMap = new HashMap<>();
	private final Map<String, SystemBean> targetCalculatorBeanNameToSystemBeanMap = new HashMap<>();
	private final Map<Integer, Map<String, PortfolioTarget>> preUploadClientToActiveTargetMap = new HashMap<>();
	private final Map<Integer, Map<String, PortfolioTarget>> postUploadClientToActiveTargetMap = new HashMap<>();
	private final List<PortfolioTarget> updateTargetList = new ArrayList<>();
	private final List<PortfolioTarget> newTargetList = new ArrayList<>();

	/**
	 * Designates to end active entities with conflicting target name as one day before the
	 * upload start date (or upload date if not defined).
	 */

	private OverlappingTargetOptions overlappingTargetOption;

	private TargetActivityActions targetActivityAction;

	private boolean copyPreviousValues;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetUploadCommand() {
		super();
		setTableName("PortfolioTarget");
		// Include matching and parent target fields to be extracted since the natural keys of targets
		// are not defined on target due to date range uniqueness for target name and client account.
		setAdditionalBeanPropertiesToInclude("matchingTarget.name", "parent.name");
		setSimple(true);
		setExistingBeans(FileUploadExistingBeanActions.SKIP);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	public enum OverlappingTargetOptions {
		REPLACE, OVERWRITE, ERROR
	}

	public enum TargetActivityActions {
		IGNORE, MIGRATE, COPY
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Map<String, InvestmentAccount> getClientAccountNumberToAccountMap() {
		return this.clientAccountNumberToAccountMap;
	}


	public Map<String, InvestmentReplication> getReplicationNameToReplicationMap() {
		return this.replicationNameToReplicationMap;
	}


	public Map<String, SystemBean> getTargetCalculatorBeanNameToSystemBeanMap() {
		return this.targetCalculatorBeanNameToSystemBeanMap;
	}


	public Map<Integer, Map<String, PortfolioTarget>> getPreUploadClientToActiveTargetMap() {
		return this.preUploadClientToActiveTargetMap;
	}


	public Map<Integer, Map<String, PortfolioTarget>> getPostUploadClientToActiveTargetMap() {
		return this.postUploadClientToActiveTargetMap;
	}


	public List<PortfolioTarget> getUpdateTargetList() {
		return this.updateTargetList;
	}


	public List<PortfolioTarget> getNewTargetList() {
		return this.newTargetList;
	}


	public OverlappingTargetOptions getOverlappingTargetOption() {
		return this.overlappingTargetOption;
	}


	public void setOverlappingTargetOption(OverlappingTargetOptions overlappingTargetOption) {
		this.overlappingTargetOption = overlappingTargetOption;
	}


	public TargetActivityActions getTargetActivityAction() {
		return this.targetActivityAction;
	}


	public void setTargetActivityAction(TargetActivityActions targetActivityAction) {
		this.targetActivityAction = targetActivityAction;
	}


	public boolean isCopyPreviousValues() {
		return this.copyPreviousValues;
	}


	public void setCopyPreviousValues(boolean copyPreviousValues) {
		this.copyPreviousValues = copyPreviousValues;
	}
}
