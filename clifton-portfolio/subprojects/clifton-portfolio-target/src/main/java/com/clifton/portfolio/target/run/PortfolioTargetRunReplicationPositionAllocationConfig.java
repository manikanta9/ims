package com.clifton.portfolio.target.run;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.portfolio.account.PortfolioAccountContractStoreTypes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortfolioTargetRuntReplicationPositionAllocationConfig</code> holds a list of replications
 * for a security that need to process actual, current, pending allocation splitting
 *
 * @author nickk
 */
// TODO reuse with the product contract splitter
public class PortfolioTargetRunReplicationPositionAllocationConfig {

	private final BigDecimal totalQuantity;
	private final List<PortfolioTargetRunReplication> replicationList;
	private PortfolioAccountContractStoreTypes type;
	private Map<String, PortfolioTargetRunReplication> replicationMap;
	private Map<String, BigDecimal> replicationTargetMap;

	/////////////////////////////////////////////////////////


	public PortfolioTargetRunReplicationPositionAllocationConfig(PortfolioAccountContractStoreTypes type, List<PortfolioTargetRunReplication> replicationList, BigDecimal totalQuantity) {
		this.type = type;
		this.replicationList = replicationList;
		this.totalQuantity = totalQuantity;
	}


	private void setupConfig() {
		this.replicationMap = new LinkedHashMap<>();
		for (PortfolioTargetRunReplication rep : CollectionUtils.getIterable(this.replicationList)) {
			this.replicationMap.put(getKeyForReplication(rep), rep);
		}

		this.replicationTargetMap = new HashMap<>();
		setupReplicationTargetMap();
	}

	/////////////////////////////////////////////////////////
	///////             Processing Methods           ////////
	/////////////////////////////////////////////////////////


	public void process() {
		setupConfig();
		processAllocations(this.totalQuantity);
		processDependentAllocations();
	}


	/**
	 * Processes contract splitting across replications based on totalQuantity and short/long replication lists
	 */
	private void processAllocations(BigDecimal quantity) {
		if (this.type.isVirtual()) {
			processVirtualAllocations();
		}
		else {
			processAllocationsImpl(quantity, false, true);
		}
	}


	private void processDependentAllocations() {
		PortfolioAccountContractStoreTypes dependentType = this.type.getDependentType();
		if (dependentType != null) {
			this.type = dependentType;
			processAllocations(null);
		}
	}


	/**
	 * Cases where replications for the same security have both long and short targets - applies
	 * "virtual" contracts to them to help them hit their targets.
	 * <p>
	 * i.e. Short Target of -77 and positive target of 20 and hold -60 contracts.
	 * Will add -17 virtual contracts to the short and 17 to the long - Net virtual contracts is always 0
	 */
	private void processVirtualAllocations() {
		// Change type to Virtual So sets correct fieldName
		if (PortfolioAccountContractStoreTypes.ACTUAL == this.type) {
			this.type = PortfolioAccountContractStoreTypes.VIRTUAL;
		}

		// GENERATE VIRTUAL CONTRACTS - Applied if processing ACTUAL contract split and holding long and short
		BigDecimal sTarget = getCurrentShortTargetTotal();
		BigDecimal lTarget = getCurrentLongTargetTotal();

		// Virtual Contract Quantity ALWAYS returned as a positive number
		BigDecimal virtualContractQuantity = generateVirtualContractQuantity(sTarget, lTarget);

		// Only if we still have long and short
		if (!MathUtils.isNullOrZero(virtualContractQuantity)) {
			processAllocationsImpl(MathUtils.negate(virtualContractQuantity), false, true);
			processAllocationsImpl(virtualContractQuantity, false, true);
		}
	}


	/**
	 * If short - apply contracts to the short list, else long.  Only apply to the other list if nothing in the first list
	 */
	private void processAllocationsImpl(BigDecimal quantity, boolean holdWrongDirection, boolean explicitTargets) {
		boolean shortContract = MathUtils.isLessThan(quantity, 0);
		List<PortfolioTargetRunReplication> list = holdWrongDirection ? null : getReplicationListInOrder();

		if (CollectionUtils.isEmpty(list)) {
			holdWrongDirection = true;
			list = getReplicationListInOrder();
			if (CollectionUtils.isEmpty(list)) {
				if (explicitTargets) {
					processAllocationsImpl(quantity, false, false);
					return;
				}
				else {
					List<PortfolioTargetRunReplication> originalList = new ArrayList<>();
					for (PortfolioTargetRunReplication rep : CollectionUtils.getIterable(this.replicationList)) {
						BigDecimal target = (rep.isMatchingReplication() ? rep.getTargetContracts() : rep.getTargetContractsAdjusted());
						if (MathUtils.isLessThan(target, BigDecimal.ZERO)) {
							if (shortContract) {
								originalList.add(rep);
							}
						}
						else if (!shortContract) {
							originalList.add(rep);
						}
					}
					if (!CollectionUtils.isEmpty(originalList)) {
						list = originalList;
						holdWrongDirection = false;
					}
					else {
						list = this.replicationList;
					}
				}
			}
		}

		Iterator<PortfolioTargetRunReplication> iterator = list.iterator();
		while (iterator.hasNext() && !MathUtils.isNullOrZero(quantity)) {
			PortfolioTargetRunReplication rep = iterator.next();
			BigDecimal target = getTargetForReplication(rep, true);

			// If already fulfilled and there is another one after it - skip it
			if (MathUtils.isEqual(target, BigDecimal.ZERO) && iterator.hasNext()) {
				addContractQuantity(rep, BigDecimal.ZERO);
				continue;
			}

			// If hold in the wrong direction only - give the full amount to the first one
			if (holdWrongDirection) {
				addContractQuantity(rep, quantity);
				quantity = BigDecimal.ZERO;
				break;
			}

			// If the last one - just give it the entire balance
			// AND it's not explicit
			// If it's explicit we'll fill to target and try to use remainder to fill other reps that are in opposite direction
			if (!iterator.hasNext()) {
				addContractQuantity(rep, quantity);
				quantity = BigDecimal.ZERO;
			}
			else {
				BigDecimal difference = MathUtils.subtract(quantity, target);
				if (MathUtils.isEqual(0, difference) || (MathUtils.isLessThan(difference, 0) && shortContract) || (MathUtils.isGreaterThan(difference, 0) && !shortContract)) {
					// We have enough to fill it, set actual to target and update total to the difference
					quantity = difference;
					addContractQuantity(rep, target);
				}
				else {
					// We didn't have enough to fill it, so just give it what's left and update total to 0 so the rest are set to 0
					addContractQuantity(rep, quantity);
					quantity = BigDecimal.ZERO;
				}
			}

			// Everything allocated - break out of loop
			if (MathUtils.isNullOrZero(quantity)) {
				return;
			}
		}

		// Didn't allocate everything try again - this time will go in "wrong" direction
		if (!MathUtils.isNullOrZero(quantity)) {
			processAllocationsImpl(quantity, true, explicitTargets);
		}
	}

	/////////////////////////////////////////////////////////
	////////           Setup/Helper Methods          ////////
	/////////////////////////////////////////////////////////


	private void setupReplicationTargetMap() {
		// Actual - Use Explicit Targets or Targets from the Replication Only
		BigDecimal totalExplicit = BigDecimal.ZERO;
		if (!this.type.isUseTradingValue()) {
			for (PortfolioTargetRunReplication rep : CollectionUtils.getIterable(this.replicationList)) {
				this.replicationTargetMap.computeIfAbsent(getKeyForReplication(rep), k ->
						// If actual, set targets as the real targets to use
						rep.isMatchingReplication() ? rep.getTargetContracts() : rep.getTargetContractsAdjusted());
			}
		}
		// Pending - Explicit Trade Allocations, Current = Actual Adjusted + Explicit Trade Allocations
		else {
			// Add in actual as "explicit" so we only distribute the difference
			if (!this.type.isPending()) {
				totalExplicit = MathUtils.add(totalExplicit, CoreMathUtils.sumProperty(this.replicationList, PortfolioTargetRunReplication::getActualContractsAdjusted));
			}

			// Now need to allocate the difference proportionally to those not explicit
			BigDecimal difference = MathUtils.subtract(this.totalQuantity, totalExplicit);
			List<PortfolioTargetRunReplication> notExplicitList = new ArrayList<>();
			BigDecimal totalNotExplicitTarget = BigDecimal.ZERO;
			for (PortfolioTargetRunReplication rep : CollectionUtils.getIterable(this.replicationList)) {
				String replicationKey = getKeyForReplication(rep);
				if (!this.replicationTargetMap.containsKey(replicationKey)) {
					// No difference - Or nothing explicit from trading - pending targets are zero, current = actual adjusted
					if (MathUtils.isNullOrZero(difference)) {
						// AND Pending, don't bother setting target, should always be allocated zero
						if (this.type.isPending()) {
							this.replicationTargetMap.put(replicationKey, BigDecimal.ZERO);
						}
						// Current - Set it to Actual Adjusted
						else {
							this.replicationTargetMap.put(replicationKey, rep.getActualContractsAdjusted());
						}
					}
					// Otherwise, try to distribute proportionally to remaining replications where not explicit
					else {
						notExplicitList.add(rep);
						totalNotExplicitTarget = MathUtils.add(totalNotExplicitTarget, MathUtils.subtract(rep.getTargetContracts(), rep.getActualContractsAdjusted()));
					}
				}
			}

			// Only one not explicit = give it the difference
			if (notExplicitList.size() == 1) {
				if (this.type.isPending()) {
					this.replicationTargetMap.put(getKeyForReplication(notExplicitList.get(0)), difference);
				}
				else {
					this.replicationTargetMap.put(getKeyForReplication(notExplicitList.get(0)), MathUtils.add(notExplicitList.get(0).getActualContractsAdjusted(), difference));
				}
			}

			// Multiple not explicit - allocate proportionally
			else if (notExplicitList.size() > 1) {
				for (PortfolioTargetRunReplication rep : notExplicitList) {
					BigDecimal quantity = (this.type.isPending() ? BigDecimal.ZERO : rep.getActualContractsAdjusted());
					this.replicationTargetMap.put(getKeyForReplication(rep),
							MathUtils.add(
									quantity,
									MathUtils.round(MathUtils.getPercentageOf(
											CoreMathUtils.getPercentValue(
													MathUtils.subtract(rep.getTargetContracts(), rep.getActualContractsAdjusted()),
													totalNotExplicitTarget),
											difference), 0)));
				}
			}
		}
	}


	protected String getKeyForReplication(PortfolioTargetRunReplication rep) {
		return rep.getTarget().getId() + "_" + rep.getReplication().getId();
	}


	private BigDecimal getContractQuantity(PortfolioTargetRunReplication rep) {
		BigDecimal value = (BigDecimal) BeanUtils.getPropertyValue(rep, this.type.getFieldName());
		if (value == null) {
			return BigDecimal.ZERO;
		}
		return value;
	}


	private BigDecimal getTargetForReplication(PortfolioTargetRunReplication rep, boolean adjustedForWhatIsAllocated) {
		BigDecimal target = this.replicationTargetMap.get(getKeyForReplication(rep));
		if (adjustedForWhatIsAllocated) {
			// Reduce Virtual By Actual
			if (PortfolioAccountContractStoreTypes.VIRTUAL == this.type) {
				// Adjust target based on what is already posted
				target = MathUtils.subtract(target, rep.getActualContracts());
			}

			// Reduce Current Virtual By Current
			if (PortfolioAccountContractStoreTypes.VIRTUAL_CURRENT == this.type) {
				// Adjust target based on what is already posted
				target = MathUtils.subtract(target, rep.getCurrentContracts());
			}

			// Also Reduce Pending Virtual by Pending
			if (PortfolioAccountContractStoreTypes.VIRTUAL_PENDING == this.type) {
				// Adjust target based on what is already posted
				target = MathUtils.subtract(target, rep.getPendingContracts());
			}

			// Adjust target based upon the value we already have in that field
			BigDecimal originalValue = getContractQuantity(rep);

			// Change Target to be the lowest "int" value
			// NOTE: Removed ROUNDING - if we need to know short vs. long that partial could mean the difference between an actual value and 0
			target = MathUtils.subtract(target, originalValue);
		}
		return MathUtils.round(target, 0, BigDecimal.ROUND_DOWN);
	}


	private void addContractQuantity(PortfolioTargetRunReplication rep, BigDecimal value) {
		BigDecimal currentValue = getContractQuantity(rep);
		BigDecimal newValue = MathUtils.add(value, currentValue);

		BeanUtils.setPropertyValue(rep, this.type.getFieldName(), newValue);
	}


	public BigDecimal getCurrentShortTargetTotal() {
		return getCurrentTargetTotal(true);
	}


	public BigDecimal getCurrentLongTargetTotal() {
		return getCurrentTargetTotal(false);
	}


	private BigDecimal getCurrentTargetTotal(boolean shortTarget) {
		BigDecimal total = BigDecimal.ZERO;
		for (PortfolioTargetRunReplication rep : CollectionUtils.getIterable(this.replicationList)) {
			BigDecimal target = getTargetForReplication(rep, true); //this.replicationTargetMap.get(key);
			if (MathUtils.isLessThan(target, BigDecimal.ZERO) == shortTarget) {
				total = MathUtils.add(total, target);
			}
		}
		return total;
	}


	private List<PortfolioTargetRunReplication> getReplicationListInOrder() {
		// Otherwise need to sort by smallest target first or specified order
		List<PortfolioTargetRunReplication> sortedList = new ArrayList<>();
		int currentIndex = CollectionUtils.getSize(sortedList);

		List<PortfolioTargetRunReplication> replicationsWithZeroTarget = new ArrayList<>();
		// Finally - By Target Asc
		for (Map.Entry<String, BigDecimal> stringBigDecimalEntry : this.replicationTargetMap.entrySet()) {
			BigDecimal target = stringBigDecimalEntry.getValue();
			PortfolioTargetRunReplication currentReplication = this.replicationMap.get(stringBigDecimalEntry.getKey());
			// Nothing there yet - put in the list
			if (MathUtils.isNullOrZero(target)) {
				replicationsWithZeroTarget.add(currentReplication);
			}
			else if (CollectionUtils.isEmpty(sortedList) || (currentIndex == CollectionUtils.getSize(sortedList))) {
				sortedList.add(currentReplication);
			}
			else {
				for (int i = currentIndex; i < sortedList.size(); i++) {
					PortfolioTargetRunReplication sortedRep = sortedList.get(i);
					BigDecimal sortedTarget = getTargetForReplication(sortedRep, false);

					if (MathUtils.isLessThanOrEqual(MathUtils.abs(target), MathUtils.abs(sortedTarget))) {
						sortedList.add(i, currentReplication);
						break;
					}
					else if (i == (sortedList.size() - 1)) {
						sortedList.add(currentReplication);
						break;
					}
				}
			}
		}
		// Add replications with target 0 at end so exposure will be present
		sortedList.addAll(replicationsWithZeroTarget);
		return sortedList;
	}


	/**
	 * Generates the # of virtual contracts based on short and long targets which is the value of the smaller of the abs value of the two targets
	 * <p>
	 * If either is 0, returns zero.
	 * If they are the same sign - returns zero;
	 * <p>
	 * Value returns is rounded down to whole value - no partial allocations allowed
	 */
	protected BigDecimal generateVirtualContractQuantity(BigDecimal shortTarget, BigDecimal longTarget) {
		if (!MathUtils.isNullOrZero(shortTarget) && !MathUtils.isNullOrZero(longTarget)) {
			if (!MathUtils.isSameSignum(shortTarget, longTarget)) {
				BigDecimal result = longTarget;
				if (MathUtils.isLessThanOrEqual(MathUtils.abs(shortTarget), longTarget)) {
					result = MathUtils.abs(shortTarget);
				}
				return MathUtils.round(result, 0, BigDecimal.ROUND_DOWN);
			}
		}
		return BigDecimal.ZERO;
	}
}
