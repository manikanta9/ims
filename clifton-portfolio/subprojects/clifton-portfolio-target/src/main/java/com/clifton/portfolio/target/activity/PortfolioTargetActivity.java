package com.clifton.portfolio.target.activity;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.usedby.softlink.SoftLinkField;

import java.math.BigDecimal;
import java.util.Date;


/**
 * <code>PortfolioTargetActivity</code> defines portfolio activity related to a {@link PortfolioTarget}.
 * Activity can consist of adjustments or position changes that can affect balance calculations. Activity
 * has a start and end date to represent an effective range for the entity (e.g. transaction date and
 * settlement date).
 *
 * @author nickk
 */
public class PortfolioTargetActivity extends BaseEntity<Integer> implements LabeledObject {

	private PortfolioTarget target;
	private PortfolioTargetActivityType type;
	/**
	 * Security this activity is tied to.
	 */
	private InvestmentSecurity security;

	private Date startDate;
	private Date endDate;

	private BigDecimal value;
	/**
	 * Used with {@link #value} to define if the value is a quantity instead of notional.
	 */
	private boolean valueQuantity;

	private String note;

	/**
	 * For historical tracking, activity is not deleted. Instead activity is flagged as deleted.
	 */
	private boolean deleted;

	/**
	 * Source link to the cause of the activity.
	 */
	private SystemTable causeSystemTable;

	@SoftLinkField(tableBeanPropertyName = "causeSystemTable")
	private Integer causeFKFieldId;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return new StringBuilder(getTarget().getClientAccount().getNumber()).append(": ")
				.append(isValueQuantity() ? "Quantity" : "Notional")
				.append(" activity for ").append(getTarget().getName()).append(" with effective range [")
				.append(DateUtils.fromDate(getStartDate(), DateUtils.DATE_FORMAT_INPUT)).append('-')
				.append(getEndDate() == null ? "" : DateUtils.fromDate(getEndDate(), DateUtils.DATE_FORMAT_INPUT)).append(']')
				.toString();
	}


	public boolean isActiveOn(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}


	public boolean isActive() {
		return isActiveOn(new Date());
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PortfolioTarget getTarget() {
		return this.target;
	}


	public void setTarget(PortfolioTarget target) {
		this.target = target;
	}


	public PortfolioTargetActivityType getType() {
		return this.type;
	}


	public void setType(PortfolioTargetActivityType type) {
		this.type = type;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public BigDecimal getValue() {
		return this.value;
	}


	public void setValue(BigDecimal value) {
		this.value = value;
	}


	public boolean isValueQuantity() {
		return this.valueQuantity;
	}


	public void setValueQuantity(boolean valueQuantity) {
		this.valueQuantity = valueQuantity;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public boolean isDeleted() {
		return this.deleted;
	}


	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}


	public SystemTable getCauseSystemTable() {
		return this.causeSystemTable;
	}


	public void setCauseSystemTable(SystemTable causeSystemTable) {
		this.causeSystemTable = causeSystemTable;
	}


	public Integer getCauseFKFieldId() {
		return this.causeFKFieldId;
	}


	public void setCauseFKFieldId(Integer causeFKFieldId) {
		this.causeFKFieldId = causeFKFieldId;
	}
}
