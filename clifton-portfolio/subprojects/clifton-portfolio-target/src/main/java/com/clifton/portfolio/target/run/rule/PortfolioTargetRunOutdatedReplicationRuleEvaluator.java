package com.clifton.portfolio.target.run.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.portfolio.target.run.PortfolioTargetRunReplication;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;
import com.clifton.portfolio.target.run.PortfolioTargetRunTargetSummary;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * This rule evaluator looks at all {@link InvestmentReplication}s for a run, and generates a violation if they have not been updated for a certain number of days before the run date.
 * The number of days is a user-configured property.
 *
 * @author mitchellf
 */
public class PortfolioTargetRunOutdatedReplicationRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<PortfolioTargetRunTargetSummary, PortfolioTargetRunReplication>> {

	private PortfolioTargetRunService portfolioTargetRunService;

	private BigDecimal numberOfDays;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<PortfolioTargetRunTargetSummary, PortfolioTargetRunReplication> context) {
		Set<Integer> replicationIds = new HashSet<>();
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		List<PortfolioTargetRunReplication> replicationList = getPortfolioTargetRunService().getPortfolioTargetRunReplicationListByRun(run.getId());
		for (BasePortfolioRunReplication runReplication : CollectionUtils.getIterable(replicationList)) {
			InvestmentReplication replication = runReplication.getReplication();
			if (replication != null && replicationIds.add(replication.getId())) {
				Map<String, Object> templateValues = new HashMap<>();
				EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
				if (entityConfig != null && !entityConfig.isExcluded()) {
					replication = context.getInvestmentReplicationService().getInvestmentReplication(replication.getId());
					if (RuleViolationUtil.isObjectOutdated(run.getBalanceDate(), replication.getUpdateDate(), getNumberOfDays(), templateValues)) {
						templateValues.put("replication", replication);
						templateValues.put("dateThreshold", getNumberOfDays());
						ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, BeanUtils.getIdentityAsLong(run), BeanUtils.getIdentityAsLong(replication), null, templateValues));
					}
				}
			}
		}
		return ruleViolationList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}


	public BigDecimal getNumberOfDays() {
		return this.numberOfDays;
	}


	public void setNumberOfDays(BigDecimal numberOfDays) {
		this.numberOfDays = numberOfDays;
	}
}
