package com.clifton.portfolio.target.activity.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.math.BigDecimal;


/**
 * @author nickk
 */
public class PortfolioTargetActivitySearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "target.name,target.description")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField(searchField = "type.id")
	private Short typeId;

	@SearchField(searchField = "name", searchFieldPath = "type")
	private String typeName;

	@SearchField(searchField = "target.id")
	private Integer targetId;

	@SearchField(searchField = "target.id")
	private Integer[] targetIds;

	@SearchField(searchField = "clientAccount.id", searchFieldPath = "target")
	private Integer clientAccountId;

	@SearchField(searchField = "replication.id", searchFieldPath = "target")
	private Integer replicationId;

	@SearchField(searchField = "name", searchFieldPath = "target.replication")
	private String replicationName;

	@SearchField
	private BigDecimal value;

	@SearchField
	private Boolean valueQuantity;

	@SearchField
	private Boolean deleted;

	@SearchField(searchField = "security.id")
	private Integer securityId;

	@SearchField(searchField = "causeSystemTable.id")
	private Short causeSystemTableId;

	@SearchField(searchField = "name", searchFieldPath = "causeSystemTable")
	private String causeSystemTableName;

	@SearchField
	private Integer causeFKFieldId;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Integer getTargetId() {
		return this.targetId;
	}


	public void setTargetId(Integer targetId) {
		this.targetId = targetId;
	}


	public Integer[] getTargetIds() {
		return this.targetIds;
	}


	public void setTargetIds(Integer[] targetIds) {
		this.targetIds = targetIds;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Integer getReplicationId() {
		return this.replicationId;
	}


	public void setReplicationId(Integer replicationId) {
		this.replicationId = replicationId;
	}


	public String getReplicationName() {
		return this.replicationName;
	}


	public void setReplicationName(String replicationName) {
		this.replicationName = replicationName;
	}


	public BigDecimal getValue() {
		return this.value;
	}


	public void setValue(BigDecimal value) {
		this.value = value;
	}


	public Boolean getValueQuantity() {
		return this.valueQuantity;
	}


	public void setValueQuantity(Boolean valueQuantity) {
		this.valueQuantity = valueQuantity;
	}


	public Boolean getDeleted() {
		return this.deleted;
	}


	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Short getCauseSystemTableId() {
		return this.causeSystemTableId;
	}


	public void setCauseSystemTableId(Short causeSystemTableId) {
		this.causeSystemTableId = causeSystemTableId;
	}


	public String getCauseSystemTableName() {
		return this.causeSystemTableName;
	}


	public void setCauseSystemTableName(String causeSystemTableName) {
		this.causeSystemTableName = causeSystemTableName;
	}


	public Integer getCauseFKFieldId() {
		return this.causeFKFieldId;
	}


	public void setCauseFKFieldId(Integer causeFKFieldId) {
		this.causeFKFieldId = causeFKFieldId;
	}
}
