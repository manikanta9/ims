package com.clifton.portfolio.target.run.process.calculator;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalanceService;
import com.clifton.portfolio.run.process.calculator.BasePortfolioRunProcessStepCalculatorImpl;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountAllocation;
import com.clifton.portfolio.target.manager.PortfolioTargetRunManagerAccount;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The {@link PortfolioTargetRunManagerCalculator} populates {@link com.clifton.portfolio.target.manager.PortfolioTargetRunManagerAccount}s for a {@link PortfolioRun}.
 *
 * @author michaelm
 */
public class PortfolioTargetRunManagerCalculator extends BasePortfolioRunProcessStepCalculatorImpl<PortfolioTargetRunConfig> {

	private PortfolioRunManagerAccountBalanceService<?> portfolioRunManagerAccountBalanceService;

	private PortfolioTargetRunService portfolioTargetRunService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void processStep(PortfolioTargetRunConfig runConfig) {
		// Manager Assignments
		calculatePortfolioTargetRunManagerAccountList(runConfig);
	}


	@Override
	protected void saveStep(PortfolioTargetRunConfig runConfig) {
		// Save All Manager Related Data
		getPortfolioTargetRunService().savePortfolioTargetRunManagerAccountListByRun(runConfig.getRun().getId(), runConfig.getRun().getCashTotal(), runConfig.getTargetManagerAccountList());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void calculatePortfolioTargetRunManagerAccountList(PortfolioTargetRunConfig runConfig) {
		List<PortfolioTargetRunManagerAccount> targetManagerAccountList = new ArrayList<>();
		List<PortfolioTargetRunManagerAccount> rollupTargetManagerAccountList = new ArrayList<>();

		List<PortfolioTargetInvestmentManagerAccountAllocation> allocationList = runConfig.getActiveTargetManagerAllocationListMap() == null ? Collections.emptyList()
				: runConfig.getActiveTargetManagerAllocationListMap().values().stream().flatMap(CollectionUtils::getStream).collect(Collectors.toList());
		Map<InvestmentManagerAccountAssignment, List<PortfolioTargetInvestmentManagerAccountAllocation>> assignmentManagerAllocationListMap = BeanUtils.getBeansMap(allocationList, PortfolioTargetInvestmentManagerAccountAllocation::getManagerAccountAssignment);

		for (Map.Entry<InvestmentManagerAccountAssignment, List<PortfolioTargetInvestmentManagerAccountAllocation>> assignmentAllocationEntry : assignmentManagerAllocationListMap.entrySet()) {
			InvestmentManagerAccountAssignment assignment = assignmentAllocationEntry.getKey();
			// Previous Date's Value
			InvestmentManagerAccountBalance prevBal = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(assignment.getReferenceOne().getId(),
					runConfig.getPreviousBusinessDay(), false);
			// Previous Month End Date's Value
			InvestmentManagerAccountBalance prevMEBal = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDate(assignment.getReferenceOne().getId(),
					runConfig.getPreviousMonthEnd(), false);

			BigDecimal previousDayCashBalance = BigDecimal.ZERO;
			BigDecimal previousMonthEndCashBalance = BigDecimal.ZERO;

			if (prevBal != null) {
				previousDayCashBalance = getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, prevBal.getManagerAccount(), prevBal.getBalanceDate(), runConfig.getRun().isMarketOnCloseAdjustmentsIncluded() ? prevBal.getMarketOnCloseTotalValue() : prevBal.getAdjustedTotalValue());
			}
			if (prevMEBal != null) {
				previousMonthEndCashBalance = getPortfolioRunManagerAccountBalanceService().getInvestmentManagerValueInClientAccountBaseCurrency(runConfig, prevMEBal.getManagerAccount(), prevMEBal.getBalanceDate(), runConfig.getRun().isMarketOnCloseAdjustmentsIncluded() ? prevMEBal.getMarketOnCloseTotalValue() : prevMEBal.getAdjustedTotalValue());
			}

			// Setup Managers
			Map<PortfolioTarget, PortfolioTargetRunManagerAccount> targetManagerAccountMap = new HashMap<>();
			Map<PortfolioTarget, PortfolioTargetRunManagerAccount> rollupTargetManagerAccountMap = new HashMap<>();
			for (PortfolioTargetInvestmentManagerAccountAllocation managerAllocation : CollectionUtils.getIterable(assignmentAllocationEntry.getValue())) {
				PortfolioTargetRunManagerAccount targetManagerAccount = targetManagerAccountMap.computeIfAbsent(managerAllocation.getTarget(),
						target -> createPortfolioTargetRunManagerAccount(runConfig.getRun(), managerAllocation));
				BigDecimal allocationBalance = runConfig.getManagerAccountAllocationToBalanceMap().get(managerAllocation);
				targetManagerAccount.setCashAllocation(MathUtils.add(targetManagerAccount.getCashAllocation(), allocationBalance));
				// Reset Previous Day and Previous Month End
				targetManagerAccount.setPreviousDayCashAllocation(MathUtils.getPercentageOf(managerAllocation.getAllocationPercent(), previousDayCashBalance, true));
				targetManagerAccount.setPreviousMonthEndCashAllocation(MathUtils.getPercentageOf(managerAllocation.getAllocationPercent(), previousMonthEndCashBalance, true));

				if (managerAllocation.getTarget().getParent() != null) {
					populateRollupPortfolioTargetRunManagerAccount(targetManagerAccount, managerAllocation.getTarget().getParent(), rollupTargetManagerAccountMap);
				}
			}
			targetManagerAccountList.addAll(targetManagerAccountMap.values());
			rollupTargetManagerAccountList.addAll(rollupTargetManagerAccountMap.values());
		}
		BigDecimal cashTotal = CoreMathUtils.sumProperty(targetManagerAccountList, PortfolioTargetRunManagerAccount::getCashAllocation);
		runConfig.getRun().setCashTotal(cashTotal);
		runConfig.setTargetManagerAccountList(CollectionUtils.combineCollections(targetManagerAccountList, rollupTargetManagerAccountList));
	}


	private PortfolioTargetRunManagerAccount createPortfolioTargetRunManagerAccount(PortfolioRun run, PortfolioTargetInvestmentManagerAccountAllocation managerAccountAllocation) {
		PortfolioTargetRunManagerAccount managerAccount = new PortfolioTargetRunManagerAccount();
		managerAccount.setOverlayRun(run);
		managerAccount.setManagerAccountAssignment(managerAccountAllocation.getManagerAccountAssignment());
		managerAccount.setPortfolioTarget(managerAccountAllocation.getTarget());
		managerAccount.setRollupTarget(false);
		managerAccount.setPrivateManager(managerAccountAllocation.isPrivateAssignment());
		resetManagerAccountValues(managerAccount);
		return managerAccount;
	}


	private void resetManagerAccountValues(PortfolioTargetRunManagerAccount managerAccount) {
		// Prevent NULLs
		managerAccount.setCashAllocation(BigDecimal.ZERO);
		managerAccount.setSecuritiesAllocation(BigDecimal.ZERO);
		managerAccount.setPreviousDayCashAllocation(BigDecimal.ZERO);
		managerAccount.setPreviousDaySecuritiesAllocation(BigDecimal.ZERO);
		managerAccount.setPreviousMonthEndCashAllocation(BigDecimal.ZERO);
		managerAccount.setPreviousMonthEndSecuritiesAllocation(BigDecimal.ZERO);
	}


	private void populateRollupPortfolioTargetRunManagerAccount(PortfolioTargetRunManagerAccount managerAccount, PortfolioTarget portfolioTarget, Map<PortfolioTarget, PortfolioTargetRunManagerAccount> rollupTargetManagerAccountMap) {
		PortfolioTargetRunManagerAccount rollupManagerAccount = rollupTargetManagerAccountMap.computeIfAbsent(portfolioTarget, t -> {
			PortfolioTargetRunManagerAccount rollup = BeanUtils.cloneBean(managerAccount, false, false);
			rollup.setPortfolioTarget(portfolioTarget);
			rollup.setRollupTarget(true);
			resetManagerAccountValues(rollup);
			rollup.setProxyValueAllocation(managerAccount.getProxyValueAllocation() != null ? BigDecimal.ZERO : null);
			return rollup;
		});

		rollupManagerAccount.setCashAllocation(MathUtils.add(rollupManagerAccount.getCashAllocation(), managerAccount.getCashAllocation()));
		rollupManagerAccount.setPreviousDayCashAllocation(MathUtils.add(rollupManagerAccount.getPreviousDayCashAllocation(), managerAccount.getPreviousDayCashAllocation()));
		rollupManagerAccount.setPreviousMonthEndCashAllocation(MathUtils.add(rollupManagerAccount.getPreviousMonthEndCashAllocation(), managerAccount.getPreviousMonthEndCashAllocation()));

		managerAccount.setParent(rollupManagerAccount);
		rollupTargetManagerAccountMap.put(portfolioTarget, rollupManagerAccount);

		if (portfolioTarget.getParent() != null) {
			populateRollupPortfolioTargetRunManagerAccount(rollupManagerAccount, portfolioTarget.getParent(), rollupTargetManagerAccountMap);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////             Getter and Setter Methods               //////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioRunManagerAccountBalanceService<?> getPortfolioRunManagerAccountBalanceService() {
		return this.portfolioRunManagerAccountBalanceService;
	}


	public void setPortfolioRunManagerAccountBalanceService(PortfolioRunManagerAccountBalanceService<?> portfolioRunManagerAccountBalanceService) {
		this.portfolioRunManagerAccountBalanceService = portfolioRunManagerAccountBalanceService;
	}


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}
}
