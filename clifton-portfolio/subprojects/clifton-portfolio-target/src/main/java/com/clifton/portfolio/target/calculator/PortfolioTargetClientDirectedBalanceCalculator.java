package com.clifton.portfolio.target.calculator;

import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.replication.PortfolioReplicationHandler;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.activity.PortfolioTargetActivity;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.run.PortfolioTargetRunReplication;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * {@link PortfolioTargetClientDirectedBalanceCalculator} is an implementation of {@link PortfolioTargetCalculator} that uses a client directed {@link PortfolioTargetBalance}
 * value and settled {@link PortfolioTargetActivity} to provide an updated {@link PortfolioTargetBalance} for a specified balance date.
 *
 * @author michaelm
 */
public class PortfolioTargetClientDirectedBalanceCalculator extends BasePortfolioTargetCalculator {

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentInstrumentService investmentInstrumentService;

	private InvestmentReplicationService investmentReplicationService;

	private InvestmentCalculator investmentCalculator;

	private MarketDataRetriever marketDataRetriever;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private PortfolioReplicationHandler portfolioReplicationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Optional flag that disables automatic updates to {@link PortfolioTargetBalance} based on settled {@link PortfolioTargetActivity}. When this flag is activated the
	 * settled activity is ignored.
	 */
	private boolean ignoreTargetActivity;

	/**
	 * Optional flag that causes the calculator to use a flexible price lookup when applying settled {@link PortfolioTargetActivity} to the existing {@link PortfolioTargetBalance}.
	 */
	private boolean useFlexiblePriceLookup;

	private boolean useTargetFollowsActual;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateTarget(PortfolioTarget target) {
		super.validateTarget(target);
		/* There are some issues around applying target percentage to the balance value here.
		 * Ideally we could adjust the balance's starting value (from existing) according to the target % to get the original value.
		 * Then after applying activity adjust the value as a percentage of the target % for saving.
		 * Concerns here:
		 * - We need a way to distinguish initial, manually created, balance from calculated value (systemDefined flag on balance?).
		 *   Initial balance should not be adjusted.
		 * - We need to account for target percentage changes. On update we could adjust existing balance (systemDefined only) with % difference.
		 */
		ValidationUtils.assertTrue(target.getTargetPercent() == null || MathUtils.isEqual(target.getTargetPercent(), MathUtils.BIG_DECIMAL_ONE_HUNDRED), "Client directed target calculation requires the target to apply 100% of the defined balance.");
	}


	@Override
	@Transactional
	public PortfolioTargetBalance calculate(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		PortfolioTargetBalance existingTargetBalance = getExistingTargetBalance(portfolioTarget, portfolioTargetRunConfig.getBalanceDate());
		PortfolioTargetBalance resultBalance;
		InvestmentReplication replication = portfolioTarget.getReplication();
		if (replication != null && isUseTargetFollowsActual() && DateUtils.isDateAfter(portfolioTargetRunConfig.getBalanceDate(), existingTargetBalance.getStartDate())) {
			// make sure allocation list is populated on target's replication
			if (CollectionUtils.isEmpty(replication.getAllocationList())) {
				replication.setAllocationList(getInvestmentReplicationService().getInvestmentReplicationAllocationListByReplicationActive(replication.getId(), portfolioTargetRunConfig.getBalanceDate(), false));
			}
			String exchangeRateDataSourceName = getMarketDataExchangeRatesApiService().getExchangeRateDataSourceForClientAccount(portfolioTarget.getClientAccount().toClientAccount());
			// sum exposure for holdings applicable to the replication and use that to create balance
			BigDecimal totalExposure = portfolioTargetRunConfig.getContractStore()
					.getSecurityStoreCollection()
					.stream()
					.map(store -> {
						InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(store.getSecurityId());
						if (!getInvestmentReplicationService().isInvestmentSecurityInReplication(replication, security)) {
							return null;
						}

						PortfolioTargetRunReplication rep = new PortfolioTargetRunReplication();
						rep.setPortfolioRun(portfolioTargetRunConfig.getRun());
						rep.setReplication(replication);
						getPortfolioReplicationHandler().setupInvestmentReplicationSecurityAllocation(rep, security, exchangeRateDataSourceName, false);
						return MathUtils.multiply(store.getPositionQuantity().getQuantity(), rep.getValue());
					})
					.reduce(BigDecimal.ZERO, MathUtils::add);

			resultBalance = createNewTargetBalance(portfolioTarget, portfolioTargetRunConfig.getBalanceDate(), String.format("Balance generated based on actual exposure to [%s]", replication.getLabel()), totalExposure, existingTargetBalance.isValueQuantity(), false);
		}
		else {
			resultBalance = createNewTargetWithTargetPercentageApplied(existingTargetBalance.getTarget(), portfolioTargetRunConfig.getBalanceDate(), String.format("Balance generated based on existing balance [%s].", existingTargetBalance.getLabel()), existingTargetBalance.getValue(), existingTargetBalance.isValueQuantity());

			if (!isIgnoreTargetActivity()) {
				applyActivityAdjustmentsToBalance(resultBalance, getTargetActivityListForTargetBalance(existingTargetBalance, portfolioTargetRunConfig), portfolioTargetRunConfig.getBalanceDate());
			}
		}

		return resultBalance;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Looks up existing {@link PortfolioTargetBalance} for the given balance date, and validates that one exists.
	 */
	private PortfolioTargetBalance getExistingTargetBalance(PortfolioTarget portfolioTarget, Date balanceDate) {
		PortfolioTargetBalance existingBalance = getExistingPortfolioTargetBalance(portfolioTarget, balanceDate);
		// observer ensures only one balance is active for target on given day
		ValidationUtils.assertNotNull(existingBalance, () -> String.format("Expected one Target Balance for Portfolio Target [%s] active on %s but one was not found.",
				portfolioTarget.getLabel(), DateUtils.fromDateShort(balanceDate)));
		return existingBalance;
	}


	/**
	 * Applies activity adjustments to the {@link PortfolioTargetBalance}.
	 */
	private PortfolioTargetBalance applyActivityAdjustmentsToBalance(PortfolioTargetBalance balanceToAdjust, List<PortfolioTargetActivity> activityAdjustmentList, Date balanceDate) {
		for (PortfolioTargetActivity targetActivity : CollectionUtils.getIterable(activityAdjustmentList)) {
			if (targetActivity.getSecurity() != null) {
				PortfolioTargetActivity activityCopy = BeanUtils.cloneBean(targetActivity, true, false);
				balanceToAdjust.addPortfolioTargetActivity(convertActivityValueToQuantityOrNotional(activityCopy, balanceToAdjust.isValueQuantity(), balanceDate));
			}
		}
		return balanceToAdjust;
	}


	private PortfolioTargetActivity convertActivityValueToQuantityOrNotional(PortfolioTargetActivity targetActivity, boolean balanceValueIsQuantity, Date balanceDate) {
		targetActivity.setId(null); // only want to save transient activity entities since the value may be converted below and the actual Activity entity could be updated after this calculation
		if (targetActivity.isValueQuantity() == balanceValueIsQuantity) {
			return targetActivity;
		}

		BigDecimal latestPrice = isUseFlexiblePriceLookup() ? getMarketDataRetriever().getPriceFlexible(targetActivity.getSecurity(), balanceDate, true)
				: getMarketDataRetriever().getPrice(targetActivity.getSecurity(), balanceDate, true, String.format("Unable to find a numeric price for security [%s] when applying Target Activity [%s] for Portfolio Target [%s].", targetActivity.getSecurity().getLabel(), targetActivity.getLabel(), targetActivity.getTarget().getLabel()));
		InvestmentNotionalCalculatorTypes roundingCalculator = InvestmentCalculatorUtils.getNotionalCalculator(targetActivity.getSecurity().getInstrument().getTradingCurrency());

		if (balanceValueIsQuantity) {
			// convert targetActivity to quantity
			targetActivity.setValue(getInvestmentCalculator().calculateQuantityFromNotional(targetActivity.getSecurity(), latestPrice, targetActivity.getValue(), BigDecimal.ONE, roundingCalculator));
		}
		else {
			// convert targetActivity to notional
			targetActivity.setValue(getInvestmentCalculator().calculateNotional(targetActivity.getSecurity(), latestPrice, targetActivity.getValue(), BigDecimal.ONE, roundingCalculator));
		}
		targetActivity.setValueQuantity(balanceValueIsQuantity);
		return targetActivity;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public PortfolioReplicationHandler getPortfolioReplicationHandler() {
		return this.portfolioReplicationHandler;
	}


	public void setPortfolioReplicationHandler(PortfolioReplicationHandler portfolioReplicationHandler) {
		this.portfolioReplicationHandler = portfolioReplicationHandler;
	}


	public boolean isIgnoreTargetActivity() {
		return this.ignoreTargetActivity;
	}


	public void setIgnoreTargetActivity(boolean ignoreTargetActivity) {
		this.ignoreTargetActivity = ignoreTargetActivity;
	}


	public boolean isUseFlexiblePriceLookup() {
		return this.useFlexiblePriceLookup;
	}


	public void setUseFlexiblePriceLookup(boolean useFlexiblePriceLookup) {
		this.useFlexiblePriceLookup = useFlexiblePriceLookup;
	}


	public boolean isUseTargetFollowsActual() {
		return this.useTargetFollowsActual;
	}


	public void setUseTargetFollowsActual(boolean useTargetFollowsActual) {
		this.useTargetFollowsActual = useTargetFollowsActual;
	}
}
