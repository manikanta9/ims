package com.clifton.portfolio.target.manager;

import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.portfolio.run.manager.PortfolioRunManagerAccountBalance;
import com.clifton.portfolio.run.manager.populator.BasePortfolioRunManagerAccountBalancePopulator;
import com.clifton.portfolio.run.manager.populator.PortfolioRunManagerAccountBalancePopulatorConfig;
import com.clifton.portfolio.target.manager.search.PortfolioTargetRunManagerAccountSearchForm;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * {@link PortfolioTargetRunManagerAccountBalancePopulator} populates {@link PortfolioRunManagerAccountBalance} records related to {@link com.clifton.portfolio.run.PortfolioRun}s
 * processed with {@link com.clifton.business.service.ServiceProcessingTypes#PORTFOLIO_TARGET}.
 *
 * @author michaelm
 */
@Component
public class PortfolioTargetRunManagerAccountBalancePopulator extends BasePortfolioRunManagerAccountBalancePopulator<PortfolioRunTargetManagerAccountBalance> {

	private PortfolioTargetRunService portfolioTargetRunService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortfolioRunTargetManagerAccountBalance> populatePortfolioRunManagerAccountBalanceListImpl(PortfolioRunManagerAccountBalancePopulatorConfig config) {
		List<PortfolioRunTargetManagerAccountBalance> list = new ArrayList<>();

		List<PortfolioTargetRunManagerAccount> targetManagerList = getTargetManagerList(config);
		for (PortfolioTargetRunManagerAccount targetManager : CollectionUtils.getIterable(targetManagerList)) {
			PortfolioRunTargetManagerAccountBalance runManagerBalance = getPopulatedPortfolioRunManagerAccountBalance(config, targetManager.getManagerAccountAssignment().getReferenceOne());
			runManagerBalance.setPortfolioTarget(targetManager.getPortfolioTarget());
			// convert run amounts back into manager base currency
			runManagerBalance.setCashAllocationAmount(getRunValueInManagerAccountBaseCurrency(config, targetManager.getManagerAccountAssignment().getReferenceOne(), targetManager.getCashAllocation()));
			runManagerBalance.setSecuritiesAllocationAmount(getRunValueInManagerAccountBaseCurrency(config, targetManager.getManagerAccountAssignment().getReferenceOne(), targetManager.getSecuritiesAllocation()));

			list.add(runManagerBalance);
		}

		return list;
	}


	@Override
	protected Class<PortfolioRunTargetManagerAccountBalance> getAccountBalanceClass() {
		return PortfolioRunTargetManagerAccountBalance.class;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<PortfolioTargetRunManagerAccount> getTargetManagerList(PortfolioRunManagerAccountBalancePopulatorConfig config) {
		PortfolioTargetRunManagerAccountSearchForm targetSearchForm = new PortfolioTargetRunManagerAccountSearchForm();
		targetSearchForm.setRunId(config.getPortfolioRun().getId());
		if (Boolean.TRUE.equals(config.getSearchForm().getRootOnly())) {
			targetSearchForm.setRootOnly(true);
		}
		else {
			targetSearchForm.setRollupTarget(false);
		}
		return getPortfolioTargetRunService().getPortfolioTargetRunManagerAccountList(targetSearchForm);
	}


	private BigDecimal getRunValueInManagerAccountBaseCurrency(PortfolioRunManagerAccountBalancePopulatorConfig config, InvestmentManagerAccount managerAccount, BigDecimal value) {
		if (config.getPortfolioRun().getClientInvestmentAccount().getBaseCurrency().equals(managerAccount.getBaseCurrency())) {
			return value;
		}
		return MathUtils.multiply(value, config.getExchangeRateForManagerAccount(managerAccount, getMarketDataExchangeRatesApiService()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}
}
