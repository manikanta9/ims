package com.clifton.portfolio.target.balance;

import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.search.PortfolioTargetBalanceSearchForm;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;


/**
 * <code>PortfolioTargetBalanceService</code> defines services available for managing {@link PortfolioTargetBalance}
 *
 * @author nickk
 */
public interface PortfolioTargetBalanceService {

	///////////////////////////////////////////////////////////////////////////
	//////////         PortfolioTargetBalance Methods               ///////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a fully populated {@link PortfolioTargetBalance} with available adjustments.
	 */
	public PortfolioTargetBalance getPortfolioTargetBalance(int id);


	public List<PortfolioTargetBalance> getPortfolioTargetBalanceList(PortfolioTargetBalanceSearchForm searchForm);


	public PortfolioTargetBalance savePortfolioTargetBalance(PortfolioTargetBalance targetBalance);


	@RequestMapping("portfolioTargetBalanceReplace")
	public PortfolioTargetBalance replacePortfolioTargetBalance(int existingTargetBalanceId, PortfolioTargetBalance targetBalance);


	@ModelAttribute("data")
	public PortfolioTargetBalance previewPortfolioTargetBalance(PortfolioTarget portfolioTarget, Date balanceDate);


	@RequestMapping("portfolioTargetBalanceEnd")
	public void endPortfolioTargetBalance(int id);


	public void deletePortfolioTargetBalance(int id);
}
