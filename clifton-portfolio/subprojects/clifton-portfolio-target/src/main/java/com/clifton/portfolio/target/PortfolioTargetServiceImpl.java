package com.clifton.portfolio.target;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portfolio.target.search.PortfolioTargetSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * @author nickk
 */
@Service
public class PortfolioTargetServiceImpl implements PortfolioTargetService {

	private AdvancedUpdatableDAO<PortfolioTarget, Criteria> portfolioTargetDAO;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioTarget getPortfolioTarget(int id) {
		return getPortfolioTargetDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortfolioTarget> getPortfolioTargetList(PortfolioTargetSearchForm searchForm) {
		return getPortfolioTargetDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<PortfolioTarget> getPortfolioTargetListByClientAccount(int clientAccountId, Date activeOnDate, boolean populateChildren) {
		// TODO cache by client account and date
		PortfolioTargetSearchForm searchForm = new PortfolioTargetSearchForm();
		searchForm.setClientAccountId(clientAccountId);
		searchForm.setActiveOnDate(activeOnDate == null ? new Date() : activeOnDate);
		searchForm.setOrderBy("order:asc");
		List<PortfolioTarget> clientAccountTargetList = getPortfolioTargetList(searchForm);

		if (!populateChildren || CollectionUtils.isEmpty(clientAccountTargetList)) {
			return clientAccountTargetList;
		}

		return PortfolioTargetUtils.buildTargetHierarchy(clientAccountTargetList);
	}


	@Override
	public PortfolioTarget savePortfolioTarget(PortfolioTarget target) {
		return getPortfolioTargetDAO().save(target);
	}


	@Override
	public void endPortfolioTarget(int id) {
		PortfolioTarget target = getPortfolioTarget(id);
		if (target != null && target.isActive()) {
			target.setEndDate(DateUtils.addDays(new Date(), -1));
			if (DateUtils.isDateBefore(target.getEndDate(), target.getStartDate(), false)) {
				target.setEndDate(target.getStartDate());
			}
			savePortfolioTarget(target);
		}
	}


	@Override
	public void deletePortfolioTarget(int id) {
		getPortfolioTargetDAO().delete(id);
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortfolioTarget, Criteria> getPortfolioTargetDAO() {
		return this.portfolioTargetDAO;
	}


	public void setPortfolioTargetDAO(AdvancedUpdatableDAO<PortfolioTarget, Criteria> portfolioTargetDAO) {
		this.portfolioTargetDAO = portfolioTargetDAO;
	}
}
