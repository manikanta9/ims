package com.clifton.portfolio.target.calculator;

import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;

import java.math.BigDecimal;
import java.util.List;


/**
 * Calculates a value for a {@link PortfolioTarget} by aggregating the balances of its child targets.
 * Child target balances must be calculated first.
 *
 * @author mitchellf
 */
public class PortfolioTargetRollupCalculator extends BasePortfolioTargetCalculator {


	@Override
	public void validateTarget(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		ValidationUtils.assertNotEmpty(portfolioTarget.getChildTargetList(), "A rollup target must have at least one child");
	}


	@Override
	public PortfolioTargetBalance calculate(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		validateTarget(portfolioTarget, portfolioTargetRunConfig);

		List<PortfolioTarget> children = portfolioTarget.getChildTargetList();
		BigDecimal balanceValue = BigDecimal.ZERO;
		Boolean valueQuantity = null;

		// Child balances are expected to be calculated first by the target balance calculator run processing step calculator
		StringBuilder descriptionBuilder = new StringBuilder("Balance generated from:\n");
		for (PortfolioTarget child : CollectionUtils.getIterable(children)) {
			PortfolioTargetBalance childBalance = portfolioTargetRunConfig.getPortfolioTargetBalance(child);
			ValidationUtils.assertNotNull(childBalance, () -> "Target rollup failed to find a target balance for target: " + (child == null ? child : child.getLabel()));

			if (valueQuantity == null) {
				valueQuantity = childBalance.isValueQuantity();
			}
			else if (valueQuantity != childBalance.isValueQuantity()) {
				throw new ValidationException("If balance value is used as quantity, it must be consistent across all children of a target. Inconsistency found in children of [" + portfolioTarget.getLabel() + "].");
			}

			BigDecimal childBalanceValue = getTargetBalanceValue(childBalance, portfolioTargetRunConfig);
			descriptionBuilder.append('\t').append(CoreMathUtils.formatNumberDecimal(childBalanceValue))
					.append(" of ").append(child.getLabel()).append('\n');

			balanceValue = MathUtils.add(balanceValue, childBalanceValue);
		}

		return createNewTargetWithTargetPercentageApplied(portfolioTarget, portfolioTargetRunConfig.getBalanceDate(), descriptionBuilder.toString(), balanceValue, BooleanUtils.isTrue(valueQuantity));
	}
}
