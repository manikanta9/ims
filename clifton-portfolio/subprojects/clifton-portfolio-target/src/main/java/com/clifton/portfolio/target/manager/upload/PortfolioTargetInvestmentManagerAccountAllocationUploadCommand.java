package com.clifton.portfolio.target.manager.upload;

import com.clifton.investment.manager.upload.BaseInvestmentManagerAccountAllocationUploadCommand;
import com.clifton.portfolio.target.PortfolioTarget;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <code>PortfolioTargetInvestmentManagerAccountAllocationUploadCommand</code> handles custom uploads for
 * {@link PortfolioTargetInvestmentManagerAccountAllocationUploadTemplate} objects
 * that are saved as {@link com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountAllocation}
 * <p>
 * Note: Upload is really a list of allocations, but we apply them to the manager and save at that level
 *
 * @author nickk
 */
public class PortfolioTargetInvestmentManagerAccountAllocationUploadCommand extends BaseInvestmentManagerAccountAllocationUploadCommand<PortfolioTargetInvestmentManagerAccountAllocationUploadTemplate> {


	public static final String[] REQUIRED_PROPERTIES = {"clientAccountNumber", "managerAccountNumber", "targetName", "allocationPercent"};

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private final Map<Integer, List<PortfolioTarget>> clientAccountTargetListMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<PortfolioTargetInvestmentManagerAccountAllocationUploadTemplate> getUploadBeanClass() {
		return PortfolioTargetInvestmentManagerAccountAllocationUploadTemplate.class;
	}


	@Override
	public String[] getRequiredProperties() {
		return REQUIRED_PROPERTIES;
	}


	@Override
	public void clearResults() {
		super.clearResults();
		this.clientAccountTargetListMap.clear();
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////////              Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	public Map<Integer, List<PortfolioTarget>> getClientAccountTargetListMap() {
		return this.clientAccountTargetListMap;
	}
}
