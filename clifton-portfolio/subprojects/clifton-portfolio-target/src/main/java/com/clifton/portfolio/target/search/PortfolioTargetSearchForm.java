package com.clifton.portfolio.target.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.CustomJsonStringAwareSearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.math.BigDecimal;


/**
 * @author nickk
 */
public class PortfolioTargetSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm implements CustomJsonStringAwareSearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = {ComparisonConditions.EQUALS})
	private String nameEquals;

	@SearchField
	private String description;

	@SearchField(searchField = "clientAccount.id")
	private Integer clientAccountId;

	@SearchField(searchField = "replication.id")
	private Integer replicationId;

	@SearchField(searchField = "name", searchFieldPath = "replication")
	private String replicationName;

	@SearchField(searchField = "parent.id")
	private Integer parentId;

	@SearchField(searchField = "matchingTarget.id")
	private Integer matchingTargetId;


	@SearchField(searchField = "targetCalculatorBean.id")
	private Integer targetCalculatorBeanId;

	@SearchField(searchFieldPath = "targetCalculatorBean", searchField = "name")
	private String targetCalculatorBeanName;


	@SearchField
	private BigDecimal targetPercent;

	@SearchField
	private Integer order;

	@SearchField
	private Boolean rollup;

	@SearchField
	private Boolean privateTarget;

	@SearchField(searchField = "parent.id", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean rootNodesOnly;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Integer getReplicationId() {
		return this.replicationId;
	}


	public void setReplicationId(Integer replicationId) {
		this.replicationId = replicationId;
	}


	public String getReplicationName() {
		return this.replicationName;
	}


	public void setReplicationName(String replicationName) {
		this.replicationName = replicationName;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public Integer getMatchingTargetId() {
		return this.matchingTargetId;
	}


	public void setMatchingTargetId(Integer matchingTargetId) {
		this.matchingTargetId = matchingTargetId;
	}


	public Integer getTargetCalculatorBeanId() {
		return this.targetCalculatorBeanId;
	}


	public void setTargetCalculatorBeanId(Integer targetCalculatorBeanId) {
		this.targetCalculatorBeanId = targetCalculatorBeanId;
	}


	public String getTargetCalculatorBeanName() {
		return this.targetCalculatorBeanName;
	}


	public void setTargetCalculatorBeanName(String targetCalculatorBeanName) {
		this.targetCalculatorBeanName = targetCalculatorBeanName;
	}


	public BigDecimal getTargetPercent() {
		return this.targetPercent;
	}


	public void setTargetPercent(BigDecimal targetPercent) {
		this.targetPercent = targetPercent;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Boolean getRollup() {
		return this.rollup;
	}


	public void setRollup(Boolean rollup) {
		this.rollup = rollup;
	}


	public Boolean getPrivateTarget() {
		return this.privateTarget;
	}


	public void setPrivateTarget(Boolean privateTarget) {
		this.privateTarget = privateTarget;
	}


	public Boolean getRootNodesOnly() {
		return this.rootNodesOnly;
	}


	public void setRootNodesOnly(Boolean rootNodesOnly) {
		this.rootNodesOnly = rootNodesOnly;
	}
}
