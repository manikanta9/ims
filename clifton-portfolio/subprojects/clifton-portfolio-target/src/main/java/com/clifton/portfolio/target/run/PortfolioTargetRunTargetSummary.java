package com.clifton.portfolio.target.run;

import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.target.PortfolioTarget;

import java.math.BigDecimal;
import java.util.List;


/**
 * {@link PortfolioTargetRunTargetSummary} represents various overlay metrics for a specified {@link PortfolioRun} (account + date) grouped by {@link PortfolioTarget}.
 *
 * @author michaelm
 */
@NonPersistentObject
public class PortfolioTargetRunTargetSummary extends BasePortfolioRunExposureSummary implements LabeledObject {

	private PortfolioTarget target;

	private List<PortfolioTargetRunReplication> replicationList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getSummaryLabel() {
		if (getTarget() != null) {
			return getTarget().getLabel();
		}
		return null;
	}


	@Override
	public BigDecimal getCashTotal() {
		// nothing here yet
		return BigDecimal.ZERO;
	}


	@Override
	public BigDecimal getTotalExposurePercent() {
		return MathUtils.round(CoreMathUtils.getPercentValue(getTotalExposure(), getPortfolioTotalValue(), true), 2);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isOutsideOfRebalanceTrigger() {
		return !MathUtils.isInRange(getTotalExposureDeviationFromAdjustedTarget(), getRebalanceTriggerMin(), getRebalanceTriggerMax());
	}


	public boolean isOutsideOfAbsoluteRebalanceTrigger() {
		return !MathUtils.isInRange(getTotalExposureDeviationFromAdjustedTarget(), getRebalanceTriggerAbsoluteMin(), getRebalanceTriggerAbsoluteMax());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioTarget getTarget() {
		return this.target;
	}


	public void setTarget(PortfolioTarget target) {
		this.target = target;
	}


	public List<PortfolioTargetRunReplication> getReplicationList() {
		return this.replicationList;
	}


	public void setReplicationList(List<PortfolioTargetRunReplication> replicationList) {
		this.replicationList = replicationList;
	}
}
