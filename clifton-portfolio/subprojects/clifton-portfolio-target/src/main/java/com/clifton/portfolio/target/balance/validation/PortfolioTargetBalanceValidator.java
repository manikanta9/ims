package com.clifton.portfolio.target.balance.validation;

import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.balance.PortfolioTargetBalanceService;
import com.clifton.portfolio.target.balance.search.PortfolioTargetBalanceSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * <code>PortfolioTargetBalanceValidator</code> validates saved {@link PortfolioTargetBalance} entities upon DAO transactions.
 *
 * @author nickk
 */
@Component
public class PortfolioTargetBalanceValidator extends SelfRegisteringDaoValidator<PortfolioTargetBalance> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(PortfolioTargetBalance balance, DaoEventTypes config) throws ValidationException {
		// Nothing Here: Uses DAO validate method
	}


	@Override
	public void validate(PortfolioTargetBalance balance, DaoEventTypes config, ReadOnlyDAO<PortfolioTargetBalance> dao) throws ValidationException {
		if (config.isInsert() || config.isUpdate()) {
			ValidationUtils.assertNotNull(balance.getTarget(), "Portfolio Target is required.");
			ValidationUtils.assertNotNull(balance.getStartDate(), "Start Date is required.");
			ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(DateUtils.clearTime(balance.getEndDate()), DateUtils.clearTime(balance.getStartDate())), "End Date of Portfolio Target Balance must be after or equal to Start Date.");

			validateForOverlappingTargetBalance(balance, dao);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void validateForOverlappingTargetBalance(PortfolioTargetBalance balance, ReadOnlyDAO<PortfolioTargetBalance> dao) {
		PortfolioTargetBalanceSearchForm searchForm = new PortfolioTargetBalanceSearchForm();
		searchForm.setTargetId(balance.getTarget().getId());
		List<PortfolioTargetBalance> existingBalanceList = ((AdvancedReadOnlyDAO<PortfolioTargetBalance, Criteria>) dao).findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		for (PortfolioTargetBalance existingBalance : CollectionUtils.getIterable(existingBalanceList)) {
			if (balance.isNewBean() || !existingBalance.equals(balance)) {

				if (DateUtils.isOverlapInDates(existingBalance.getStartDate(), existingBalance.getEndDate(), balance.getStartDate(), balance.getEndDate())) {
					if (DateUtils.isDateBefore(balance.getStartDate(), existingBalance.getStartDate(), false)) {
						throw new ValidationException("There is an existing balance for this target that starts after the balance you are updating. You cannot override a balance that starts at a later date. Please update the start date on the balance [" + existingBalance.getLabel() + "] before updating this balance.");
					}
					throw new UserIgnorableValidationException("A Target Balance record already exists for this Client Account Target for overlapping date range " + DateUtils.fromDateRange(existingBalance.getStartDate(), existingBalance.getEndDate(), true, false) + ". Would you like to replace it?")
							.setIgnoreExceptionUrl(PortfolioTargetBalanceService.class, "replacePortfolioTargetBalance").addParameters(MapUtils.of("existingTargetBalanceId", existingBalance.getId().toString()));
				}
			}
		}
	}
}

