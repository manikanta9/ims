package com.clifton.portfolio.target.validation;

import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.PortfolioTargetUtils;
import com.clifton.portfolio.target.calculator.PortfolioTargetCalculator;
import com.clifton.portfolio.target.search.PortfolioTargetSearchForm;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author nickk
 */
@Component
public class PortfolioTargetValidator extends SelfRegisteringDaoValidator<PortfolioTarget> {

	private SystemBeanService systemBeanService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(PortfolioTarget target, DaoEventTypes config) throws ValidationException {
		// Nothing Here: Uses DAO validate method
	}


	@Override
	public void validate(PortfolioTarget target, DaoEventTypes config, ReadOnlyDAO<PortfolioTarget> dao) throws ValidationException {
		if (config.isInsert() || config.isUpdate()) {
			ValidationUtils.assertNotNull(target.getStartDate(), String.format("Start Date is required for Target [%s].", target.getLabel()));
			ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(DateUtils.clearTime(target.getEndDate()), DateUtils.clearTime(target.getStartDate())),
					String.format("End Date [%s] of Target [%s] must be after or equal to Start Date [%s].", DateUtils.fromDateShort(target.getEndDate()), target.getLabel(), DateUtils.fromDateShort(target.getStartDate())));

			validateForOverlappingTarget(target, dao, config.isInsert());

			if (config.isUpdate()) {
				validateChildTargetsAreInactive(target, dao);
			}

			PortfolioTargetUtils.validateMatchingTargetRelationshipsForStartDate(target, dao);

			validateTargetCalculator(target);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void validateForOverlappingTarget(PortfolioTarget target, ReadOnlyDAO<PortfolioTarget> dao, boolean insert) {
		PortfolioTargetSearchForm searchForm = new PortfolioTargetSearchForm();
		searchForm.setNameEquals(target.getName());
		searchForm.setClientAccountId(target.getClientAccount().getId());
		List<PortfolioTarget> existingTargetList = ((AdvancedReadOnlyDAO<PortfolioTarget, Criteria>) dao).findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));

		for (PortfolioTarget existingTarget : CollectionUtils.getIterable(existingTargetList)) {
			if (insert || !target.equals(existingTarget)) {
				if (DateUtils.isOverlapInDates(existingTarget.getStartDate(), existingTarget.getEndDate(), target.getStartDate(), target.getEndDate())) {
					throw new ValidationException(String.format("A Target record already exists Client Account [%s] and Target Name [%s] for overlapping date range [%s]", existingTarget.getClientAccount().getLabel(), existingTarget.getName(), DateUtils.fromDateRange(existingTarget.getStartDate(), existingTarget.getEndDate(), true, false)));
				}
			}
		}
	}


	private void validateChildTargetsAreInactive(PortfolioTarget target, ReadOnlyDAO<PortfolioTarget> dao) {
		if (target.getEndDate() != null) {
			PortfolioTargetSearchForm searchForm = new PortfolioTargetSearchForm();
			searchForm.setParentId(target.getId());
			searchForm.setActiveOnDate(target.getEndDate());
			List<PortfolioTarget> existingTargetList = ((AdvancedReadOnlyDAO<PortfolioTarget, Criteria>) dao).findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));

			ValidationUtils.assertEmpty(existingTargetList, String.format("Unable to deactivate target [%s]. It has active child targets.", target.getLabel()));
		}
	}


	private void validateTargetCalculator(PortfolioTarget target) {
		if (target.getTargetCalculatorBean() != null) {
			PortfolioTargetCalculator targetCalculator = (PortfolioTargetCalculator) getSystemBeanService().getBeanInstance(target.getTargetCalculatorBean());
			targetCalculator.validateTarget(target);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
