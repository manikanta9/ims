package com.clifton.portfolio.target.calculator;

import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;

import java.math.BigDecimal;


/**
 * Target calculator that will allow for manually overriding the target balance value
 *
 * @author mitchellf
 */
public class PortfolioTargetManualOverrideCalculator extends BasePortfolioTargetCalculator implements ValidationAware {

	private BigDecimal balanceValue;
	private Boolean valueQuantity;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getBalanceValue(), "A balance value must be provided");
	}


	@Override
	public void validateTarget(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		super.validateTarget(portfolioTarget, portfolioTargetRunConfig);
		validate();
	}


	@Override
	public PortfolioTargetBalance calculate(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		String description = String.format("Balance generated from defined value: %s", CoreMathUtils.formatNumberDecimal(getBalanceValue()));
		return createNewTargetWithTargetPercentageApplied(portfolioTarget, portfolioTargetRunConfig.getBalanceDate(), description, getBalanceValue(), BooleanUtils.isTrue(getValueQuantity()));
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BigDecimal getBalanceValue() {
		return this.balanceValue;
	}


	public void setBalanceValue(BigDecimal balanceValue) {
		this.balanceValue = balanceValue;
	}


	public Boolean getValueQuantity() {
		return this.valueQuantity;
	}


	public void setValueQuantity(Boolean valueQuantity) {
		this.valueQuantity = valueQuantity;
	}
}
