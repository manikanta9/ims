package com.clifton.portfolio.target.calculator;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;

import java.math.BigDecimal;


/**
 * Calculates a value for a {@link PortfolioTarget} by taking a percentage of its parent target.
 * The target must have a parent defined. Percentage used is {@link PortfolioTarget#getTargetPercent()}.
 *
 * @author mitchellf
 */
public class PortfolioTargetInheritedChildrenCalculator extends BasePortfolioTargetCalculator {

	@Override
	public void validateTarget(PortfolioTarget portfolioTarget) {
		ValidationUtils.assertNotNull(portfolioTarget.getParent(), String.format("The selected target calculator is not applicable for target [%s] because it does not have a parent target.", portfolioTarget.getLabel()));
	}


	@Override
	public void validateTarget(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		ValidationUtils.assertNotNull(portfolioTarget.getParent(), "Cannot apply inherited balances to a target with no parent");
	}


	@Override
	public PortfolioTargetBalance calculate(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		validateTarget(portfolioTarget, portfolioTargetRunConfig);

		PortfolioTargetBalance parentBalance = portfolioTargetRunConfig.getPortfolioTargetBalance(portfolioTarget.getParent());
		ValidationUtils.assertNotNull(parentBalance, () -> "Target calculation as percentage of parent failed to find a target balance for parent target: " + (parentBalance == null ? parentBalance : parentBalance.getLabel()));

		BigDecimal parentBalanceValue = getTargetBalanceValue(parentBalance, portfolioTargetRunConfig);
		BigDecimal value = MathUtils.getPercentageOf(portfolioTarget.getTargetPercent(), parentBalanceValue, true);
		String description = new StringBuilder("Balance generated from ")
				.append(CoreMathUtils.formatNumber(portfolioTarget.getTargetPercent(), MathUtils.NUMBER_FORMAT_MONEY4))
				.append("% of parent ").append(parentBalance.getTarget().getLabel()).append(": ")
				.append(CoreMathUtils.formatNumberDecimal(parentBalanceValue)).toString();
		return createNewTargetBalance(portfolioTarget, portfolioTargetRunConfig.getBalanceDate(), description, value, parentBalance.isValueQuantity(), false);
	}
}
