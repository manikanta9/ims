package com.clifton.portfolio.target.manager.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.investment.manager.search.BaseInvestmentManagerAccountAllocationSearchForm;


/**
 * @author nickk
 */
public class PortfolioTargetInvestmentManagerAccountAllocationSearchForm extends BaseInvestmentManagerAccountAllocationSearchForm {

	@SearchField(searchField = "target.id")
	private Integer targetId;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Integer getTargetId() {
		return this.targetId;
	}


	public void setTargetId(Integer targetId) {
		this.targetId = targetId;
	}
}
