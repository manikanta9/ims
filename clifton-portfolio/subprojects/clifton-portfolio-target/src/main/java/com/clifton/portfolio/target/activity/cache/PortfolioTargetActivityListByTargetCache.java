package com.clifton.portfolio.target.activity.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portfolio.target.activity.PortfolioTargetActivity;
import org.springframework.stereotype.Component;


/**
 * A cache implementation that is designed to cache {@link PortfolioTargetActivity} by {@link com.clifton.portfolio.target.PortfolioTarget}.
 *
 * @author SurjeetB
 */
@Component
public class PortfolioTargetActivityListByTargetCache extends SelfRegisteringSingleKeyDaoListCache<PortfolioTargetActivity, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "target.id";
	}


	@Override
	protected Integer getBeanKeyValue(PortfolioTargetActivity bean) {
		if (bean.getTarget() != null) {
			return bean.getTarget().getId();
		}
		return null;
	}
}

