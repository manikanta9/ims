package com.clifton.portfolio.target.run.trade;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.portfolio.run.trade.PortfolioCurrencyTrade;
import com.clifton.portfolio.run.trade.PortfolioRunTrade;
import com.clifton.portfolio.target.run.PortfolioTargetRunReplication;

import java.util.List;


/**
 * The <code>PortfolioTargetRunTrade</code> is a run trade for use with {@link PortfolioTargetRunReplication}
 *
 * @author nickk
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class PortfolioTargetRunTrade extends PortfolioRunTrade<PortfolioTargetRunReplication> {

	//////////////////////////////////////////////////////////////////////////
	// Not Persisted in the DB - Used for Trade Creation/View Screens ONLY
	//////////////////////////////////////////////////////////////////////////

	private boolean excludeMispricing;

	// Currency List for the Run.
	private List<PortfolioCurrencyTrade<PortfolioTargetRunReplication>> currencyTradeList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isExcludeMispricing() {
		return this.excludeMispricing;
	}


	public void setExcludeMispricing(boolean excludeMispricing) {
		this.excludeMispricing = excludeMispricing;
	}


	public List<PortfolioCurrencyTrade<PortfolioTargetRunReplication>> getCurrencyTradeList() {
		return this.currencyTradeList;
	}


	public void setCurrencyTradeList(List<PortfolioCurrencyTrade<PortfolioTargetRunReplication>> currencyTradeList) {
		this.currencyTradeList = currencyTradeList;
	}
}
