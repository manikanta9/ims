package com.clifton.portfolio.target;

import com.clifton.portfolio.target.search.PortfolioTargetSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;


/**
 * <code>PortfolioTargetService</code> defines services for managing {@link PortfolioTarget}s.
 *
 * @author nickk
 */
public interface PortfolioTargetService {


	public PortfolioTarget getPortfolioTarget(int id);


	public List<PortfolioTarget> getPortfolioTargetList(PortfolioTargetSearchForm searchForm);


	public List<PortfolioTarget> getPortfolioTargetListByClientAccount(int clientAccountId, Date activeOnDate, boolean populateChildren);


	public PortfolioTarget savePortfolioTarget(PortfolioTarget target);


	@RequestMapping("portfolioTargetEnd")
	public void endPortfolioTarget(int id);


	public void deletePortfolioTarget(int id);
}
