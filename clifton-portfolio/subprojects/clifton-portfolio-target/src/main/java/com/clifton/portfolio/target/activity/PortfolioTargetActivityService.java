package com.clifton.portfolio.target.activity;

import com.clifton.portfolio.target.activity.search.PortfolioTargetActivitySearchForm;
import com.clifton.portfolio.target.activity.search.PortfolioTargetActivityTypeSearchForm;

import java.util.List;


/**
 * <code>PortfolioTargetActivityService</code> defines services for {@link com.clifton.portfolio.target.PortfolioTarget} activity.
 *
 * @author nickk
 */
public interface PortfolioTargetActivityService {

	///////////////////////////////////////////////////////////////////////////
	//////////      PortfolioTargetActivityType Methods      ///////////
	///////////////////////////////////////////////////////////////////////////


	public PortfolioTargetActivityType getPortfolioTargetActivityType(short id);


	public PortfolioTargetActivityType getPortfolioTargetActivityTypeByName(String name);


	public List<PortfolioTargetActivityType> getPortfolioTargetActivityTypeList(PortfolioTargetActivityTypeSearchForm searchForm);


	///////////////////////////////////////////////////////////////////////////
	//////////         PortfolioTargetActivity Methods       ///////////
	///////////////////////////////////////////////////////////////////////////


	public PortfolioTargetActivity getPortfolioTargetActivity(int id);


	public List<PortfolioTargetActivity> getPortfolioTargetActivityList(PortfolioTargetActivitySearchForm searchForm);


	public List<PortfolioTargetActivity> getPortfolioTargetActivityListByTargetId(int id);


	public PortfolioTargetActivity savePortfolioTargetActivity(PortfolioTargetActivity targetBalanceActivity);


	public List<PortfolioTargetActivity> savePortfolioTargetActivityList(List<PortfolioTargetActivity> targetBalanceActivityList);


	/**
	 * Flags the existing {@link PortfolioTargetActivity} as deleted rather than delete the entry. Activity cannot be deleted.
	 */
	public void deletePortfolioTargetActivity(int id);
}
