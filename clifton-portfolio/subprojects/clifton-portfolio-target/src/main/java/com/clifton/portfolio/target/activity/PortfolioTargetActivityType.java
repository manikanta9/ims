package com.clifton.portfolio.target.activity;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * <code>PortfolioTargetActivityType</code> defines metadata specific to types of {@link PortfolioTargetActivity}
 * such as required fields and attributes for how to apply activity.
 *
 * @author nickk
 */
@CacheByName
public class PortfolioTargetActivityType extends NamedEntity<Short> {

	/**
	 * Flag to indicate of activity of type is a position or general adjustment.
	 */
	private boolean position;
	/**
	 * Flag to indicate a security is required for the activity entry.
	 */
	private boolean securityRequired;
	/**
	 * Flag to indicate an activity entry requires a note
	 */
	private boolean noteRequired;
	/**
	 * Flag to indicate a cause table and FK are required for an activity entry.
	 */
	private boolean causeRequired;
	/**
	 * Flag indicates activity is to be applied as Market On Close for balance adjustments.
	 */
	private boolean marketOnClose;
	/**
	 * Flag indicates a type is system defined and cannot be edited.
	 */
	private boolean systemDefined;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isPosition() {
		return this.position;
	}


	public void setPosition(boolean position) {
		this.position = position;
	}


	public boolean isSecurityRequired() {
		return this.securityRequired;
	}


	public void setSecurityRequired(boolean securityRequired) {
		this.securityRequired = securityRequired;
	}


	public boolean isNoteRequired() {
		return this.noteRequired;
	}


	public void setNoteRequired(boolean noteRequired) {
		this.noteRequired = noteRequired;
	}


	public boolean isCauseRequired() {
		return this.causeRequired;
	}


	public void setCauseRequired(boolean causeRequired) {
		this.causeRequired = causeRequired;
	}


	public boolean isMarketOnClose() {
		return this.marketOnClose;
	}


	public void setMarketOnClose(boolean marketOnClose) {
		this.marketOnClose = marketOnClose;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}
}
