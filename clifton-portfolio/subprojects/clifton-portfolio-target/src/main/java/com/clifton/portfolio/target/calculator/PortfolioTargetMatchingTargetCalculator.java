package com.clifton.portfolio.target.calculator;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.PortfolioTargetUtils;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;


/**
 * {@link PortfolioTargetMatchingTargetCalculator} is an implementation of {@link PortfolioTargetCalculator} that copies the {@link PortfolioTargetBalance} calculated for the
 * specified {@link PortfolioTarget#matchingTarget}.
 *
 * @author michaelm
 */
public class PortfolioTargetMatchingTargetCalculator extends BasePortfolioTargetCalculator {


	@Override
	@Transactional
	public PortfolioTargetBalance calculate(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		PortfolioTargetBalance matchingTargetBalance = portfolioTargetRunConfig.getPortfolioTargetBalance(portfolioTarget.getMatchingTarget());
		ValidationUtils.assertNotNull(matchingTargetBalance, String.format("No balance found for Matching Target [%s] while processing Target [%s].", portfolioTarget.getMatchingTarget().getLabel(), portfolioTarget.getLabel()));
		BigDecimal matchingBalanceValue = getTargetBalanceValue(matchingTargetBalance, portfolioTargetRunConfig);
		String description = new StringBuilder("Balance generated from ")
				.append(CoreMathUtils.formatNumber(portfolioTarget.getTargetPercent(), MathUtils.NUMBER_FORMAT_MONEY4))
				.append("% of matching ").append(matchingTargetBalance.getTarget().getLabel()).append(": ")
				.append(CoreMathUtils.formatNumberDecimal(matchingBalanceValue)).toString();
		return createNewTargetBalance(portfolioTarget, portfolioTargetRunConfig.getBalanceDate(), description, matchingBalanceValue, matchingTargetBalance.isValueQuantity(), true);
	}


	@Override
	public void validateTarget(PortfolioTarget portfolioTarget) {
		PortfolioTarget matchingTarget = portfolioTarget.getMatchingTarget();
		ValidationUtils.assertNotNull(matchingTarget, String.format("Target [%s] is using the Matching Target Calculator but no Matching Target is specified.", portfolioTarget.getLabel()));
		ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(portfolioTarget.getStartDate(), matchingTarget.getStartDate()) && DateUtils.isDateBeforeOrEqual(portfolioTarget.getEndDate(), matchingTarget.getEndDate(), false),
				String.format("Target [%s] Start Date and End Date must fall within date range of Matching Target [%s].", portfolioTarget.getLabel(), matchingTarget.getLabel()));
		PortfolioTargetUtils.validateTargetForMatchingTarget(portfolioTarget);
	}
}
