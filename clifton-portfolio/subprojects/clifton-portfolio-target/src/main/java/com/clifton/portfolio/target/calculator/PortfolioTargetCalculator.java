package com.clifton.portfolio.target.calculator;

import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;

import java.util.Date;


/**
 * {@link PortfolioTargetCalculator}s are used to produce a {@link PortfolioTargetBalance} for the corresponding {@link PortfolioTarget} during
 * {@link com.clifton.portfolio.run.PortfolioRun} processing.
 *
 * @author michaelm
 */
public interface PortfolioTargetCalculator {

	/**
	 * Validates the {@link PortfolioTarget} against the portfolio target during target saving.
	 */
	public void validateTarget(PortfolioTarget portfolioTarget);


	/**
	 * Validates the {@link PortfolioTarget} in the context of the {@link PortfolioTargetRunConfig} during run processing.
	 */
	public void validateTarget(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig);


	/**
	 * Calculates an updated {@link PortfolioTargetBalance} for the given {@link PortfolioTarget} in the context of the {@link PortfolioTargetRunConfig}.
	 */
	public PortfolioTargetBalance calculate(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig);


	/**
	 * Allows {@link PortfolioTargetCalculator}s that do not require a {@link PortfolioTargetRunConfig} to provide a preview of a {@link PortfolioTargetBalance} for a given
	 * Balance Date.
	 */
	public PortfolioTargetBalance previewTargetBalance(PortfolioTarget portfolioTarget, Date balanceDate);


	/**
	 * Method that should be implemented to undo any persisted changes made within {@link #calculate(PortfolioTarget, PortfolioTargetRunConfig)}. One use case for this is
	 * re-processing a {@link com.clifton.portfolio.run.PortfolioRun}.
	 */
	public void deleteProcessing(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig);
}
