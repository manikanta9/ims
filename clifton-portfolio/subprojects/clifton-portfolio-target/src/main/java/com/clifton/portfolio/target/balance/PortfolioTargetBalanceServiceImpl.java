package com.clifton.portfolio.target.balance;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.PortfolioTargetService;
import com.clifton.portfolio.target.activity.PortfolioTargetActivity;
import com.clifton.portfolio.target.activity.PortfolioTargetActivityService;
import com.clifton.portfolio.target.balance.search.PortfolioTargetBalanceSearchForm;
import com.clifton.portfolio.target.calculator.PortfolioTargetCalculator;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author nickk
 */
@Service
public class PortfolioTargetBalanceServiceImpl implements PortfolioTargetBalanceService {


	private AdvancedUpdatableDAO<PortfolioTargetBalance, Criteria> portfolioTargetBalanceDAO;

	private PortfolioTargetActivityService portfolioTargetActivityService;
	private PortfolioTargetService portfolioTargetService;

	private SystemBeanService systemBeanService;

	///////////////////////////////////////////////////////////////////////////
	//////////         PortfolioTargetBalance Methods               ///////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public PortfolioTargetBalance getPortfolioTargetBalance(int id) {
		PortfolioTargetBalance balance = getPortfolioTargetBalanceInternal(id);

		if (balance != null) {
			balance.setTargetActivityList(getPortfolioTargetActivityListByBalance(balance));
		}

		return balance;
	}


	@Override
	@Transactional
	public List<PortfolioTargetBalance> getPortfolioTargetBalanceList(PortfolioTargetBalanceSearchForm searchForm) {
		List<PortfolioTargetBalance> result = getPortfolioTargetBalanceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		if (BooleanUtils.isTrue(searchForm.getPopulateBalanceActivity())) {
			CollectionUtils.getIterable(result).forEach(balance -> balance.setTargetActivityList(getPortfolioTargetActivityListByBalance(balance)));
		}
		return result;
	}


	@Override
	@Transactional
	public PortfolioTargetBalance savePortfolioTargetBalance(PortfolioTargetBalance targetBalance) {
		List<PortfolioTargetActivity> newTargetActivityList = targetBalance.getTargetActivityList();
		PortfolioTargetBalance savedBalance = getPortfolioTargetBalanceDAO().save(targetBalance);

		if (!CollectionUtils.isEmpty(newTargetActivityList)) {
			newTargetActivityList.forEach(activity -> activity.setTarget(savedBalance.getTarget()));
			List<PortfolioTargetActivity> saveActivityList = getPortfolioTargetActivityService().savePortfolioTargetActivityList(newTargetActivityList);
			savedBalance.setTargetActivityList(saveActivityList);
		}

		return savedBalance;
	}


	@Override
	@Transactional
	public PortfolioTargetBalance replacePortfolioTargetBalance(int existingTargetBalanceId, PortfolioTargetBalance targetBalance) {
		ValidationUtils.assertNotNull(targetBalance, "Cannot replace an existing target balance with nothing. Instead end the existing target balance.");

		PortfolioTargetBalance existing = getPortfolioTargetBalanceInternal(existingTargetBalanceId);
		if (existing != null) {
			ValidationUtils.assertFalse(DateUtils.isEqualWithoutTime(existing.getStartDate(), targetBalance.getStartDate()),
					String.format("Unable to replace existing Target Balance [%s] with new Target Balance [%s] because they have the same start date. They must be manually corrected.", existing.getLabel(), targetBalance.getLabel()));
			Date existingStartDate = ObjectUtils.coalesce(targetBalance.getStartDate(), new Date());
			if (applyEndDateToTargetBalance(existing, existingStartDate)) {
				savePortfolioTargetBalance(existing);
			}
		}
		// populate target details so that DAO recognizes entity exists in DB
		if (targetBalance.getTarget() != null && targetBalance.getTarget().getId() != null) {
			PortfolioTarget target = getPortfolioTargetService().getPortfolioTarget(targetBalance.getTarget().getId());
			if (target != null) {
				targetBalance.setTarget(target);
			}
		}
		return savePortfolioTargetBalance(targetBalance);
	}


	@Override
	public PortfolioTargetBalance previewPortfolioTargetBalance(PortfolioTarget portfolioTarget, Date balanceDate) {
		ValidationUtils.assertNotNull(portfolioTarget, "Portfolio Target is required to preview a Portfolio Target Balance.");
		ValidationUtils.assertNotNull(portfolioTarget.getTargetCalculatorBean(), "Portfolio Target Calculator is required to preview a Portfolio Target Balance.");
		ValidationUtils.assertNotNull(balanceDate, "Balance Date is required to preview a Portfolio Target Balance");
		PortfolioTargetCalculator targetCalculator = (PortfolioTargetCalculator) getSystemBeanService().getBeanInstance(portfolioTarget.getTargetCalculatorBean());
		return targetCalculator.previewTargetBalance(portfolioTarget, balanceDate);
	}


	@Override
	@Transactional
	public void endPortfolioTargetBalance(int id) {
		PortfolioTargetBalance balance = getPortfolioTargetBalanceInternal(id);
		if (applyEndDateToTargetBalance(balance, new Date())) {
			savePortfolioTargetBalance(balance);
		}
	}


	@Override
	public void deletePortfolioTargetBalance(int id) {
		getPortfolioTargetBalanceDAO().delete(id);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if the end date was able to be applied to the target. The balance must exists and be active for the end date to be applied.
	 */
	private boolean applyEndDateToTargetBalance(PortfolioTargetBalance balance, Date endDate) {
		if (balance != null && balance.isActiveOn(endDate)) {
			balance.setEndDate(DateUtils.addDays(endDate, -1));
			if (DateUtils.isDateBefore(balance.getEndDate(), balance.getStartDate(), false)) {
				balance.setEndDate(balance.getStartDate());
			}
			return true;
		}
		return false;
	}


	private PortfolioTargetBalance getPortfolioTargetBalanceInternal(int balanceId) {
		return getPortfolioTargetBalanceDAO().findByPrimaryKey(balanceId);
	}


	private List<PortfolioTargetActivity> getPortfolioTargetActivityListByBalance(PortfolioTargetBalance balance) {
		if (balance == null) {
			return Collections.emptyList();
		}

		List<PortfolioTargetActivity> activityList = getPortfolioTargetActivityService().getPortfolioTargetActivityListByTargetId(balance.getTarget().getId());
		if (CollectionUtils.isEmpty(activityList)) {
			return Collections.emptyList();
		}

		return activityList.stream()
				.filter(activity -> DateUtils.isDateAfterOrEqual(activity.getEndDate(), balance.getStartDate()))
				.filter(activity -> balance.getEndDate() != null && DateUtils.isDateAfterOrEqual(balance.getEndDate(), activity.getStartDate()))
				.collect(Collectors.toList());
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortfolioTargetBalance, Criteria> getPortfolioTargetBalanceDAO() {
		return this.portfolioTargetBalanceDAO;
	}


	public void setPortfolioTargetBalanceDAO(AdvancedUpdatableDAO<PortfolioTargetBalance, Criteria> portfolioTargetBalanceDAO) {
		this.portfolioTargetBalanceDAO = portfolioTargetBalanceDAO;
	}


	public PortfolioTargetActivityService getPortfolioTargetActivityService() {
		return this.portfolioTargetActivityService;
	}


	public void setPortfolioTargetActivityService(PortfolioTargetActivityService portfolioTargetActivityService) {
		this.portfolioTargetActivityService = portfolioTargetActivityService;
	}


	public PortfolioTargetService getPortfolioTargetService() {
		return this.portfolioTargetService;
	}


	public void setPortfolioTargetService(PortfolioTargetService portfolioTargetService) {
		this.portfolioTargetService = portfolioTargetService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
