package com.clifton.portfolio.target.run.process;


import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.portfolio.replication.PortfolioReplicationAllocationSecurityConfig;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.process.PortfolioRunConfig;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountAllocation;
import com.clifton.portfolio.target.manager.PortfolioTargetRunManagerAccount;
import com.clifton.portfolio.target.run.PortfolioTargetRunReplication;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * {@link PortfolioTargetRunConfig} is used to store configuration and data that is necessary to process
 * {@link com.clifton.business.service.ServiceProcessingTypes#PORTFOLIO_TARGET} runs.
 *
 * @author michaelm
 */
public class PortfolioTargetRunConfig extends PortfolioRunConfig {

	/**
	 * List of targets active for the run's client account and balance date.
	 */
	private List<PortfolioTarget> portfolioTargetList;
	/**
	 * Map of target balances for each target in this run.
	 */
	private final Map<PortfolioTarget, PortfolioTargetBalance> portfolioTargetToBalanceMap = new HashMap<>();
	/**
	 * List of active manager assignments for the targets of this run.
	 */
	private final List<InvestmentManagerAccountAssignment> activeManagerAccountAssignmentList = new ArrayList<>();
	/**
	 * Map of target manager allocations. The key is the target ID, with the value being the manager assignment allocations for the target.
	 */
	private Map<Integer, List<PortfolioTargetInvestmentManagerAccountAllocation>> activeTargetManagerAllocationListMap;
	/**
	 * Map of manager account to their balance for the run's balance date.
	 */
	private final Map<Integer, InvestmentManagerAccountBalance> managerAccountBalanceMap = new HashMap<>();
	/**
	 * Map of manager account target allocation to their allocation balance for the run's balance date.
	 */
	private final Map<PortfolioTargetInvestmentManagerAccountAllocation, BigDecimal> managerAccountAllocationToBalanceMap = new HashMap<>();
	/**
	 * Manager related entities.
	 */
	private List<PortfolioTargetRunManagerAccount> targetManagerAccountList;
	/**
	 * Maps for {@link PortfolioTargetRunReplication#targetExposure} calculations.
	 */
	private final Map<String, List<PortfolioReplicationAllocationSecurityConfig>> accountReplicationToReplicationAllocationConfigMap = new LinkedHashMap<>();
	private final Map<PortfolioTarget, List<PortfolioTargetRunReplication>> targetToRunReplicationListMap = new LinkedHashMap<>();
	/**
	 * Stores at key PortfolioTargetID_InvestmentReplicationID_SecurityID the original rep with security info populated
	 * For cases with minimize imbalances it's easier to reprocess replications, but don't want to have to look up static data again
	 */
	private final Map<String, PortfolioTargetRunReplication> replicationSetupMap = new LinkedHashMap<>();

	private List<PortfolioCurrencyOther> portfolioCurrencyOtherList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetRunConfig(PortfolioRun run, boolean reloadProxyManagers, int currentDepth) {
		super(run, reloadProxyManagers, currentDepth);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addPortfolioTargetBalance(PortfolioTargetBalance portfolioTargetBalance) {
		getPortfolioTargetToBalanceMap().put(portfolioTargetBalance.getTarget(), portfolioTargetBalance);
	}


	public PortfolioTargetBalance removePortfolioTargetBalance(PortfolioTarget target) {
		return getPortfolioTargetToBalanceMap().remove(target);
	}


	public Collection<PortfolioTargetBalance> getPortfolioTargetBalances() {
		return getPortfolioTargetToBalanceMap().values();
	}


	public PortfolioTargetBalance getPortfolioTargetBalance(PortfolioTarget target) {
		return getPortfolioTargetToBalanceMap().get(target);
	}


	public List<PortfolioTargetRunReplication> getTargetRunReplicationList() {
		return CollectionUtils.getFlattened(getTargetToRunReplicationListMap().values());
	}


	public String getAccountReplicationKey(PortfolioTarget portfolioTarget) {
		return portfolioTarget.getClientAccount().getId() + "_" + portfolioTarget.getReplication().getId();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<PortfolioTarget> getPortfolioTargetList() {
		return this.portfolioTargetList;
	}


	public void setPortfolioTargetList(List<PortfolioTarget> portfolioTargetList) {
		this.portfolioTargetList = portfolioTargetList;
	}


	public Map<PortfolioTarget, PortfolioTargetBalance> getPortfolioTargetToBalanceMap() {
		return this.portfolioTargetToBalanceMap;
	}


	public List<InvestmentManagerAccountAssignment> getActiveManagerAccountAssignmentList() {
		return this.activeManagerAccountAssignmentList;
	}


	public Map<Integer, List<PortfolioTargetInvestmentManagerAccountAllocation>> getActiveTargetManagerAllocationListMap() {
		return this.activeTargetManagerAllocationListMap;
	}


	public void setActiveTargetManagerAllocationListMap(Map<Integer, List<PortfolioTargetInvestmentManagerAccountAllocation>> activeTargetManagerAllocationListMap) {
		this.activeTargetManagerAllocationListMap = activeTargetManagerAllocationListMap;
	}


	public Map<Integer, InvestmentManagerAccountBalance> getManagerAccountBalanceMap() {
		return this.managerAccountBalanceMap;
	}


	public Map<PortfolioTargetInvestmentManagerAccountAllocation, BigDecimal> getManagerAccountAllocationToBalanceMap() {
		return this.managerAccountAllocationToBalanceMap;
	}


	public List<PortfolioTargetRunManagerAccount> getTargetManagerAccountList() {
		return this.targetManagerAccountList;
	}


	public void setTargetManagerAccountList(List<PortfolioTargetRunManagerAccount> targetManagerAccountList) {
		this.targetManagerAccountList = targetManagerAccountList;
	}


	public Map<String, List<PortfolioReplicationAllocationSecurityConfig>> getAccountReplicationToReplicationAllocationConfigMap() {
		return this.accountReplicationToReplicationAllocationConfigMap;
	}


	public Map<PortfolioTarget, List<PortfolioTargetRunReplication>> getTargetToRunReplicationListMap() {
		return this.targetToRunReplicationListMap;
	}


	public Map<String, PortfolioTargetRunReplication> getReplicationSetupMap() {
		return this.replicationSetupMap;
	}


	public List<PortfolioCurrencyOther> getPortfolioCurrencyOtherList() {
		return this.portfolioCurrencyOtherList;
	}


	public void setPortfolioCurrencyOtherList(List<PortfolioCurrencyOther> portfolioCurrencyOtherList) {
		this.portfolioCurrencyOtherList = portfolioCurrencyOtherList;
	}
}
