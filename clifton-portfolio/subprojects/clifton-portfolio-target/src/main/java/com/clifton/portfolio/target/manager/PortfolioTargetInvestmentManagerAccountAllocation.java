package com.clifton.portfolio.target.manager;


import com.clifton.investment.manager.BaseInvestmentManagerAllocation;
import com.clifton.portfolio.target.PortfolioTarget;


/**
 * The <code>PortfolioTargetInvestmentManagerAccountAllocation</code> class allows allocating manager
 * assets to one or more {@link PortfolioTarget}.
 *
 * @author nickk
 */
public class PortfolioTargetInvestmentManagerAccountAllocation extends BaseInvestmentManagerAllocation {

	private PortfolioTarget target;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getTypeSpecificLabelSuffix() {
		return getTarget() == null ? "UNKNOWN" : getTarget().getLabel();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PortfolioTarget getTarget() {
		return this.target;
	}


	public void setTarget(PortfolioTarget target) {
		this.target = target;
	}
}
