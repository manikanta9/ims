package com.clifton.portfolio.target.calculator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.activity.PortfolioTargetActivity;
import com.clifton.portfolio.target.activity.PortfolioTargetActivityService;
import com.clifton.portfolio.target.activity.search.PortfolioTargetActivitySearchForm;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.balance.PortfolioTargetBalanceService;
import com.clifton.portfolio.target.balance.search.PortfolioTargetBalanceSearchForm;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Base implementation of {@link PortfolioTargetCalculator}. Extracts some common functionality for balance creation and deleting processing to avoid duplicate code.
 *
 * @author mitchellf
 */
public abstract class BasePortfolioTargetCalculator implements PortfolioTargetCalculator {

	private PortfolioTargetBalanceService portfolioTargetBalanceService;

	private PortfolioTargetActivityService portfolioTargetActivityService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void validateTarget(PortfolioTarget portfolioTarget) {
		// Nothing by default. Override to apply
	}


	@Override
	public void validateTarget(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		// Nothing by default. Override to apply
	}


	@Override
	public PortfolioTargetBalance previewTargetBalance(PortfolioTarget portfolioTarget, Date balanceDate) {
		throw new ValidationException("Target Balance preview is not supported for this Portfolio Target Calculator.");
	}


	@Override
	public void deleteProcessing(PortfolioTarget portfolioTarget, PortfolioTargetRunConfig portfolioTargetRunConfig) {
		portfolioTargetRunConfig.removePortfolioTargetBalance(portfolioTarget);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a new {@link PortfolioTargetBalance} for the provided details and applies the target's target percentage.
	 *
	 * @see #createNewTargetBalance(PortfolioTarget, Date, String, BigDecimal, boolean, boolean)
	 * @see #applyPortfolioTargetPercentage(PortfolioTargetBalance)
	 */
	protected PortfolioTargetBalance createNewTargetWithTargetPercentageApplied(PortfolioTarget portfolioTarget, Date balanceDate, String description, BigDecimal value, boolean valueQuantity) {
		return createNewTargetBalance(portfolioTarget, balanceDate, description, value, valueQuantity, true);
	}


	/**
	 * Creates a new {@link PortfolioTargetBalance} for the provided details.
	 *
	 * @see #applyPortfolioTargetPercentage(PortfolioTargetBalance)
	 */
	protected PortfolioTargetBalance createNewTargetBalance(PortfolioTarget portfolioTarget, Date balanceDate, String description, BigDecimal value, boolean valueQuantity, boolean adjustValueWithTargetPercentage) {
		PortfolioTargetBalance targetBalance = new PortfolioTargetBalance();
		targetBalance.setTarget(portfolioTarget);
		targetBalance.setStartDate(balanceDate);
		targetBalance.setTargetBalanceDescription(description);
		targetBalance.setValue(value);
		targetBalance.setValueQuantity(valueQuantity);
		return adjustValueWithTargetPercentage ? applyPortfolioTargetPercentage(targetBalance) : targetBalance;
	}


	/**
	 * Adjusts the provide {@link PortfolioTargetBalance}'s value according to its {@link PortfolioTarget}'s target percentage.
	 */
	protected PortfolioTargetBalance applyPortfolioTargetPercentage(PortfolioTargetBalance targetBalance) {
		PortfolioTarget portfolioTarget = targetBalance.getTarget();
		if (portfolioTarget.getTargetPercent() != null && !MathUtils.isEqual(portfolioTarget.getTargetPercent(), MathUtils.BIG_DECIMAL_ONE_HUNDRED)) {
			targetBalance.setValue(MathUtils.getPercentageOf(portfolioTarget.getTargetPercent(), targetBalance.getValue(), true));
		}
		return targetBalance;
	}


	/**
	 * Returns an existing, active {@link PortfolioTargetBalance} for the provided {@link PortfolioTarget} and Balance Date. If one does not exist, null is returned.
	 */
	protected PortfolioTargetBalance getExistingPortfolioTargetBalance(PortfolioTarget portfolioTarget, Date balanceDate) {
		PortfolioTargetBalanceSearchForm targetSearchForm = new PortfolioTargetBalanceSearchForm();
		targetSearchForm.setTargetId(portfolioTarget.getId());
		targetSearchForm.setActiveOnDate(balanceDate);
		targetSearchForm.setLimit(1);
		return CollectionUtils.getFirstElement(getPortfolioTargetBalanceService().getPortfolioTargetBalanceList(targetSearchForm));
	}


	/**
	 * Uses the existing {@link PortfolioTargetBalance#startDate} and {@link PortfolioRun#balanceDate} to look up {@link PortfolioTargetActivity} to apply to the new transient
	 * {@link PortfolioTargetBalance}.
	 */
	protected List<PortfolioTargetActivity> getTargetActivityListForTargetBalance(PortfolioTargetBalance existingBalance, PortfolioTargetRunConfig targetRunConfig) {
		PortfolioTargetActivitySearchForm activitySearchForm = new PortfolioTargetActivitySearchForm();
		activitySearchForm.setTargetId(existingBalance.getTarget().getId());
		activitySearchForm.setEndDateRangeStartDate(existingBalance.getStartDate());
		activitySearchForm.setEndDateRangeEndDate(targetRunConfig.getBalanceDate());
		activitySearchForm.setDeleted(Boolean.FALSE);
		List<PortfolioTargetActivity> activityList = getPortfolioTargetActivityService().getPortfolioTargetActivityList(activitySearchForm);

		if (targetRunConfig.getRun().isMarketOnCloseAdjustmentsIncluded()) {
			return activityList;
		}

		return CollectionUtils.getStream(activityList)
				.filter(activity -> !activity.getType().isMarketOnClose())
				.collect(Collectors.toList());
	}


	protected BigDecimal getTargetBalanceValue(PortfolioTargetBalance targetBalance, PortfolioTargetRunConfig targetRunConfig) {
		return targetRunConfig.getRun().isMarketOnCloseAdjustmentsIncluded() ? targetBalance.getMarketOnCloseValue() : targetBalance.getAdjustedValue();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PortfolioTargetBalanceService getPortfolioTargetBalanceService() {
		return this.portfolioTargetBalanceService;
	}


	public void setPortfolioTargetBalanceService(PortfolioTargetBalanceService portfolioTargetBalanceService) {
		this.portfolioTargetBalanceService = portfolioTargetBalanceService;
	}


	public PortfolioTargetActivityService getPortfolioTargetActivityService() {
		return this.portfolioTargetActivityService;
	}


	public void setPortfolioTargetActivityService(PortfolioTargetActivityService portfolioTargetActivityService) {
		this.portfolioTargetActivityService = portfolioTargetActivityService;
	}
}
