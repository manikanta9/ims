package com.clifton.portfolio.target.manager;


import com.clifton.core.util.StringUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.manager.BasePortfolioRunManagerAccount;
import com.clifton.portfolio.target.PortfolioTarget;


/**
 * The {@link PortfolioTargetRunManagerAccount} class represents detailed manager account allocations to specific
 * {@link com.clifton.portfolio.target.PortfolioTarget}s of an {@link com.clifton.investment.account.InvestmentAccount}
 * for a {@link PortfolioRun}.
 *
 * @author michaelm
 */
public class PortfolioTargetRunManagerAccount extends BasePortfolioRunManagerAccount {

	private PortfolioTarget portfolioTarget;
	private boolean rollupTarget;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		String label = "";
		if (getManagerAccountAssignment() != null) {
			label = getManagerAccountAssignment().getReferenceOne().getLabel();
			if (!StringUtils.isEmpty(getManagerAccountAssignment().getDisplayName())) {
				label += " (" + getManagerAccountAssignment().getDisplayName() + ")";
			}
			if (getPortfolioTarget() != null) {
				label += ": " + getPortfolioTarget().getLabel();
			}
			return label;
		}

		return label;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioTarget getPortfolioTarget() {
		return this.portfolioTarget;
	}


	public void setPortfolioTarget(PortfolioTarget portfolioTarget) {
		this.portfolioTarget = portfolioTarget;
	}


	public boolean isRollupTarget() {
		return this.rollupTarget;
	}


	public void setRollupTarget(boolean rollupTarget) {
		this.rollupTarget = rollupTarget;
	}
}
