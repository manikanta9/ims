package com.clifton.portfolio.target.run.search;


import com.clifton.core.dataaccess.search.form.BaseRestrictiveSearchForm;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.util.validation.ValidationUtils;


/**
 * {@link PortfolioTargetRunTargetSummaryCommand} provides search functionality for {@link com.clifton.portfolio.target.run.PortfolioTargetRunTargetSummary} entities.
 *
 * @author michaelm
 */
@SearchForm(hasOrmDtoClass = false)
public class PortfolioTargetRunTargetSummaryCommand extends BaseRestrictiveSearchForm {

	/**
	 * Required and validated by the {@link #validate()} method.
	 */
	private Integer runId;

	private Integer targetId;

	// rebalanceTriggerMin is not null
	private Boolean withRebalanceTriggersOnly;

	// primaryReplication is not null
	private Boolean withReplicationsOnly;

	private Boolean rootOnly;

	private Boolean rollupTarget;

	/**
	 * Also populates cash adjustment lists for each {@link com.clifton.portfolio.target.PortfolioTarget} so user can see "adjusted" cash bucket values.
	 */
	private Boolean populateCashAdjustments;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getRunId(), "Portfolio Run ID is required to build Run Target summaries.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public Integer getTargetId() {
		return this.targetId;
	}


	public void setTargetId(Integer targetId) {
		this.targetId = targetId;
	}


	public Boolean getWithRebalanceTriggersOnly() {
		return this.withRebalanceTriggersOnly;
	}


	public void setWithRebalanceTriggersOnly(Boolean withRebalanceTriggersOnly) {
		this.withRebalanceTriggersOnly = withRebalanceTriggersOnly;
	}


	public Boolean getWithReplicationsOnly() {
		return this.withReplicationsOnly;
	}


	public void setWithReplicationsOnly(Boolean withReplicationsOnly) {
		this.withReplicationsOnly = withReplicationsOnly;
	}


	public Boolean getRootOnly() {
		return this.rootOnly;
	}


	public void setRootOnly(Boolean rootOnly) {
		this.rootOnly = rootOnly;
	}


	public Boolean getRollupTarget() {
		return this.rollupTarget;
	}


	public void setRollupTarget(Boolean rollupTarget) {
		this.rollupTarget = rollupTarget;
	}


	public Boolean getPopulateCashAdjustments() {
		return this.populateCashAdjustments;
	}


	public void setPopulateCashAdjustments(Boolean populateCashAdjustments) {
		this.populateCashAdjustments = populateCashAdjustments;
	}
}
