package com.clifton.portfolio.target.run.process.calculator;

import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.process.calculator.BasePortfolioRunProcessStepCalculatorImpl;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.PortfolioTargetUtils;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.calculator.PortfolioTargetCalculator;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;
import com.clifton.system.bean.SystemBeanService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * {@link PortfolioRunTargetBalanceCalculator} is an implementation of a step calculator for {@link com.clifton.portfolio.run.PortfolioRun} processing that populates the
 * {@link com.clifton.portfolio.target.balance.PortfolioTargetBalance} for a run by invoking the {@link com.clifton.portfolio.target.PortfolioTarget#targetCalculatorBean}.
 *
 * @author michaelm
 */
public class PortfolioRunTargetBalanceCalculator extends BasePortfolioRunProcessStepCalculatorImpl<PortfolioTargetRunConfig> {

	private SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void saveStep(PortfolioTargetRunConfig runConfig) {
		PortfolioRun run = runConfig.getRun();
		Collection<PortfolioTargetBalance> balanceCollection = runConfig.getPortfolioTargetBalances();
		run.setOverlayTargetTotal(CoreMathUtils.sumProperty(balanceCollection, PortfolioTargetBalance::getValue));
		if (!CollectionUtils.isEmpty(balanceCollection)) {
			if (run.getCustomColumns() == null) {
				run.setCustomColumns(new CustomJsonString());
			}
			CustomJsonStringUtils.setColumnValue(run.getCustomColumns(), PortfolioRun.CUSTOM_FIELD_TARGET_BALANCE_LIST, balanceCollection);
		}
		runConfig.setRun(getPortfolioRunService().savePortfolioRun(run));
	}


	@Override
	protected void processStep(PortfolioTargetRunConfig runConfig) {
		List<PortfolioTarget> rootTargetList = PortfolioTargetUtils.buildTargetHierarchy(runConfig.getPortfolioTargetList());
		processTargetList(runConfig, rootTargetList);
	}


	private void processTargetList(PortfolioTargetRunConfig runConfig, List<PortfolioTarget> targetList) {
		if (!CollectionUtils.isEmpty(targetList)) {
			List<PortfolioTarget> unprocessedTargets = processNonMatchingTargetsAndReturnMatchingTargetList(runConfig, targetList);
			for (PortfolioTarget target : CollectionUtils.getIterable(unprocessedTargets)) {
				processTarget(runConfig, target);
			}
		}
	}


	/**
	 * Processes all {@link PortfolioTarget}s that don't have a {@link PortfolioTarget#matchingTarget}. Returns the unprocessed targets.
	 */
	private List<PortfolioTarget> processNonMatchingTargetsAndReturnMatchingTargetList(PortfolioTargetRunConfig runConfig, List<PortfolioTarget> rootTargetList) {
		List<PortfolioTarget> rootTargetsWithMatchingTarget = new ArrayList<>();
		for (PortfolioTarget target : CollectionUtils.getIterable(rootTargetList)) {
			if (target.getMatchingTarget() == null) {
				processTarget(runConfig, target);
			}
			else {
				rootTargetsWithMatchingTarget.add(target);
			}
		}
		return rootTargetsWithMatchingTarget;
	}


	private void processTarget(PortfolioTargetRunConfig runConfig, PortfolioTarget target) {
		if (target.isRollup()) {
			processTargetList(runConfig, target.getChildren());
			calculateTargetBalance(runConfig, target);
		}
		else {
			calculateTargetBalance(runConfig, target);
			processTargetList(runConfig, target.getChildren());
		}
	}


	private void calculateTargetBalance(PortfolioTargetRunConfig runConfig, PortfolioTarget target) {
		PortfolioTargetCalculator targetCalculator = (PortfolioTargetCalculator) getSystemBeanService().getBeanInstance(target.getTargetCalculatorBean());
		runConfig.addPortfolioTargetBalance(targetCalculator.calculate(target, runConfig));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
