package com.clifton.portfolio.target.run.rule;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.portfolio.target.PortfolioTargetService;
import com.clifton.portfolio.target.run.PortfolioTargetRunReplication;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;
import com.clifton.portfolio.target.run.PortfolioTargetRunTargetSummary;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Rule evaluator that will determine if a portfolio run's total exposure is outside of its rebalance band.
 * This is done by aggregating the total exposure across all asset classes, and comparing the value to the total target exposure. If the difference between the values is greater
 * than the threshold (specified in percentage of the target expsoure), this rule will generate a violation.
 * <p>
 * It also performs similar validation for each individual currency. Exposure to each currency will be aggregated and compared to the total target exposure for that currency.
 * This comparison utilizes a different threshold value than the total comparison. Specified as a percent of the target exposure for each currency.
 *
 * @author mitchellf
 */
public class PortfolioTargetRunOutsideOfRebalanceBandRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<PortfolioTargetRunTargetSummary, PortfolioTargetRunReplication>> {

	private PortfolioTargetService portfolioTargetService;
	private PortfolioTargetRunService portfolioTargetRunService;

	private BigDecimal targetThreshold;
	private BigDecimal currencyThreshold;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<PortfolioTargetRunTargetSummary, PortfolioTargetRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();

		List<PortfolioTargetRunReplication> replicationList = getPortfolioTargetRunService().getPortfolioTargetRunReplicationListByRun(run.getId());
		Map<InvestmentSecurity, BigDecimal> exposures = new HashMap<>();
		Map<InvestmentSecurity, BigDecimal> targets = new HashMap<>();

		BigDecimal totalTarget = BigDecimal.ZERO;
		BigDecimal totalExposure = BigDecimal.ZERO;

		for (PortfolioTargetRunReplication rep : CollectionUtils.getIterable(replicationList)) {
			if (rep.getSecurity().getInstrument().getHierarchy().getInvestmentType().getName().equals(InvestmentType.FORWARDS)) {
				exposures.compute(rep.getSecurity().getUnderlyingSecurity(), (key, value) -> MathUtils.add(value, rep.getActualExposureAdjusted()));
				targets.compute(rep.getSecurity().getUnderlyingSecurity(), (key, value) -> MathUtils.add(value, rep.getTargetExposure()));

				totalExposure = MathUtils.add(totalExposure, rep.getActualExposureAdjusted());
				totalTarget = MathUtils.add(totalTarget, rep.getTargetExposure());
			}
		}

		BigDecimal totalDiff = MathUtils.absoluteDiff(totalExposure, totalTarget);
		BigDecimal totalDiffPercent = MathUtils.multiply(MathUtils.abs(MathUtils.divide(totalDiff, totalTarget)), 100);

		if (MathUtils.isGreaterThan(totalDiffPercent, getTargetThreshold())) {
			EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(run.getClientInvestmentAccount()));
			Map<String, Object> templateValues = new HashMap<>();
			StringBuilder violationNote = new StringBuilder();
			violationNote.append("Total exposure $")
					.append(CoreMathUtils.formatNumberMoney(totalExposure))
					.append(" differed from the target exposure $")
					.append(CoreMathUtils.formatNumberMoney(totalTarget))
					.append(" by ")
					.append(MathUtils.round(totalDiffPercent, 2))
					.append("%, which is greater than the maximum deviation of ")
					.append(getTargetThreshold())
					.append("%.");
			templateValues.put("violationNote", violationNote.toString());
			ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, BeanUtils.getIdentityAsLong(run), templateValues));
		}

		exposures.forEach((security, value) -> {
			BigDecimal diff = MathUtils.absoluteDiff(value, targets.get(security));
			BigDecimal diffPercent = MathUtils.multiply(MathUtils.abs(MathUtils.divide(diff, targets.get(security))), 100);
			if (MathUtils.isGreaterThan(diffPercent, getCurrencyThreshold())) {
				EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(run.getClientInvestmentAccount()));
				Map<String, Object> templateValues = new HashMap<>();
				StringBuilder violationNote = new StringBuilder();
				violationNote.append("Currency exposure to ")
						.append(security.getSymbol())
						.append(" was $")
						.append(CoreMathUtils.formatNumberMoney(value))
						.append(", which differed from the target exposure $")
						.append(CoreMathUtils.formatNumberMoney(targets.get(security)))
						.append(" by ")
						.append(MathUtils.round(diffPercent, 2))
						.append("%, which is greater than the maximum deviation of ")
						.append(getCurrencyThreshold())
						.append("%.");
				templateValues.put("violationNote", violationNote.toString());
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, BeanUtils.getIdentityAsLong(run), templateValues));
			}
		});

		return ruleViolationList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetService getPortfolioTargetService() {
		return this.portfolioTargetService;
	}


	public void setPortfolioTargetService(PortfolioTargetService portfolioTargetService) {
		this.portfolioTargetService = portfolioTargetService;
	}


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}


	public BigDecimal getTargetThreshold() {
		return this.targetThreshold;
	}


	public void setTargetThreshold(BigDecimal targetThreshold) {
		this.targetThreshold = targetThreshold;
	}


	public BigDecimal getCurrencyThreshold() {
		return this.currencyThreshold;
	}


	public void setCurrencyThreshold(BigDecimal currencyThreshold) {
		this.currencyThreshold = currencyThreshold;
	}
}
