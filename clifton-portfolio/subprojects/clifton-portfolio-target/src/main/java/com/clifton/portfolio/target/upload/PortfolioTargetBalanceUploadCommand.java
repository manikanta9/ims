package com.clifton.portfolio.target.upload;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <code>PortfolioTargetBalanceUploadCommand</code> is a simple upload command for {@link PortfolioTargetBalance}s.
 * <p>
 * Local caching is populated on this command during the upload process and should be excluded in the result.
 *
 * @author nickk
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class PortfolioTargetBalanceUploadCommand extends PortfolioTargetUploadCommand {

	private final Map<Integer, Map<String, PortfolioTargetBalance>> clientToActiveTargetBalanceMap = new HashMap<>();
	private final List<PortfolioTargetBalance> updateBalanceList = new ArrayList<>();
	private final List<PortfolioTargetBalance> newBalanceList = new ArrayList<>();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetBalanceUploadCommand() {
		super();
		setTableName("PortfolioTargetBalance");
		// Add all target fields to be extracted since the natural keys of targets are not
		// defined due to date range uniqueness for target name and client account.
		setAdditionalBeanPropertiesToInclude("target.matchingTarget.name", "target.parent.name", "target.clientAccount.number", "target.targetCalculatorBean.name", "target.replication.name", "target.targetPercent", "target.order", "target.startDate", "target.endDate", "target.rollup");
	}


	@Override
	public String getSystemUploadBeanListConverterBeanName() {
		return "portfolioTargetBalanceUploadConverter";
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Map<Integer, Map<String, PortfolioTargetBalance>> getClientToActiveTargetBalanceMap() {
		return this.clientToActiveTargetBalanceMap;
	}


	public List<PortfolioTargetBalance> getUpdateBalanceList() {
		return this.updateBalanceList;
	}


	public List<PortfolioTargetBalance> getNewBalanceList() {
		return this.newBalanceList;
	}
}
