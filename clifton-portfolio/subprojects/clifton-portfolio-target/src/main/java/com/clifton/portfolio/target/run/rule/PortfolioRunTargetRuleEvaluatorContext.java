package com.clifton.portfolio.target.run.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.run.PortfolioTargetRunReplication;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;
import com.clifton.portfolio.target.run.PortfolioTargetRunTargetSummary;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Context with specific functionality for evaluating Portfolio Target rules
 *
 * @author mitchellf
 */
public class PortfolioRunTargetRuleEvaluatorContext extends PortfolioRunRuleEvaluatorContext<PortfolioTargetRunTargetSummary, PortfolioTargetRunReplication> {

	private PortfolioTargetRunService portfolioTargetRunService;

	private final Map<PortfolioRun, List<PortfolioTargetRunReplication>> replicationMap = new HashMap<>();

	private final Map<Date, List<PortfolioTargetBalance>> balanceMap = new HashMap<>();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PortfolioTargetRunReplication> getReplicationList(PortfolioRun run) {

		List<PortfolioTargetRunReplication> replicationList = getReplicationMap().get(run);

		if (CollectionUtils.isEmpty(replicationList)) {
			replicationList = getPortfolioTargetRunService().getPortfolioTargetRunReplicationListByRun(run.getId());
			getReplicationMap().put(run, replicationList);
		}
		return replicationList;
	}


	public List<PortfolioTargetBalance> getPortfolioTargetBalanceList(PortfolioRun run) {
		if (run == null) {
			return Collections.emptyList();
		}
		List<PortfolioTargetBalance> balanceList = getBalanceMap().get(run.getBalanceDate());

		// not using CollectionUtils.isEmpty because we would allow an empty list (just indicates no balances), but we don't want to allow null (indicates not populated)
		if (balanceList == null) {
			balanceList = getPortfolioTargetRunService().getPortfolioTargetBalanceListByRun(run.getId());
			getBalanceMap().put(run.getBalanceDate(), balanceList);
		}
		return balanceList;
	}


	public PortfolioTargetBalance getPortfolioTargetBalance(PortfolioRun run, PortfolioTarget target) {
		List<PortfolioTargetBalance> balances = getPortfolioTargetBalanceList(run);
		if (!CollectionUtils.isEmpty(balances)) {
			return CollectionUtils.getOnlyElement(balances.stream().filter(bal -> bal.getTarget().equals(target)).collect(Collectors.toList()));
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}


	public Map<PortfolioRun, List<PortfolioTargetRunReplication>> getReplicationMap() {
		return this.replicationMap;
	}


	public Map<Date, List<PortfolioTargetBalance>> getBalanceMap() {
		return this.balanceMap;
	}
}
