package com.clifton.portfolio.target.balance;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.activity.PortfolioTargetActivity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


/**
 * <code>PortfolioTargetBalance</code> defines a target balance for a client account {@link PortfolioTarget}.
 *
 * @author nickk
 */
public class PortfolioTargetBalance extends BaseEntity<Integer> implements LabeledObject {

	private PortfolioTarget target;

	private Date startDate;
	private Date endDate;

	private BigDecimal value;
	/**
	 * Used with {@link #value} to define if the value is a quantity instead of notional.
	 */
	private boolean valueQuantity;

	private String targetBalanceDescription;

	private List<PortfolioTargetActivity> targetBalanceActivityList;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return new StringBuilder(Optional.ofNullable(getTarget()).map(PortfolioTarget::getClientAccount).map(InvestmentAccount::getNumber).orElse("NA")).append(": ")
				.append(isValueQuantity() ? "Quantity" : "Notional").append(" balance for ").append(Optional.ofNullable(getTarget()).map(PortfolioTarget::getName).orElse("NA"))
				.append(" with effective range [").append(DateUtils.fromDate(getStartDate(), DateUtils.DATE_FORMAT_INPUT))
				.append('-').append(getEndDate() == null ? "" : DateUtils.fromDate(getEndDate(), DateUtils.DATE_FORMAT_INPUT)).append(']')
				.toString();
	}


	public boolean isActiveOn(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}


	public boolean isActive() {
		return isActiveOn(new Date());
	}


	public BigDecimal getAdjustedValue() {
		return applyBalanceActivityListAdjustment(false);
	}


	public BigDecimal getMarketOnCloseValue() {
		return applyBalanceActivityListAdjustment(true);
	}


	public void addPortfolioTargetActivity(PortfolioTargetActivity targetActivity) {
		if (CollectionUtils.isEmpty(this.targetBalanceActivityList)) {
			setTargetActivityList(new ArrayList<>());
		}
		this.targetBalanceActivityList.add(targetActivity);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BigDecimal applyBalanceActivityListAdjustment(boolean moc) {
		BigDecimal baseValue = moc ? getAdjustedValue() : getValue();

		if (CollectionUtils.isEmpty(getTargetActivityList())) {
			return baseValue;
		}

		return getTargetActivityList().stream()
				.filter(activity -> !activity.isDeleted())
				.filter(activity -> moc == activity.getType().isMarketOnClose())
				.map(this::adjustBalanceActivityValueForQuantity)
				.reduce(baseValue, MathUtils::add);
	}


	private BigDecimal adjustBalanceActivityValueForQuantity(PortfolioTargetActivity balanceActivity) {
		if (balanceActivity.isValueQuantity() != isValueQuantity()) {
			// TODO need price to adjust activity to value units
		}
		return balanceActivity.getValue();
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PortfolioTarget getTarget() {
		return this.target;
	}


	public void setTarget(PortfolioTarget target) {
		this.target = target;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public BigDecimal getValue() {
		return this.value;
	}


	public void setValue(BigDecimal value) {
		this.value = value;
	}


	public boolean isValueQuantity() {
		return this.valueQuantity;
	}


	public void setValueQuantity(boolean valueQuantity) {
		this.valueQuantity = valueQuantity;
	}


	public String getTargetBalanceDescription() {
		return this.targetBalanceDescription;
	}


	public void setTargetBalanceDescription(String targetBalanceDescription) {
		this.targetBalanceDescription = targetBalanceDescription;
	}


	public List<PortfolioTargetActivity> getTargetActivityList() {
		return this.targetBalanceActivityList;
	}


	public void setTargetActivityList(List<PortfolioTargetActivity> targetBalanceActivityList) {
		this.targetBalanceActivityList = targetBalanceActivityList;
	}
}
