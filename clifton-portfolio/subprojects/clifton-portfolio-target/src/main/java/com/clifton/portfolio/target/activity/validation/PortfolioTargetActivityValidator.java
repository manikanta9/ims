package com.clifton.portfolio.target.activity.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.target.activity.PortfolioTargetActivity;
import com.clifton.portfolio.target.activity.PortfolioTargetActivityType;
import org.springframework.stereotype.Component;


/**
 * <code>PortfolioTargetValidator</code> validates saved {@link PortfolioTargetActivity} entities upon DAO transactions.
 *
 * @author nickk
 */
@Component
public class PortfolioTargetActivityValidator extends SelfRegisteringDaoValidator<PortfolioTargetActivity> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(PortfolioTargetActivity activity, DaoEventTypes config) throws ValidationException {
		if (config.isInsert() || config.isUpdate()) {
			validateActivityCause(activity);
			validateActivityNote(activity);
			validateActivitySecurity(activity);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void validateActivityCause(PortfolioTargetActivity activity) {
		PortfolioTargetActivityType type = activity.getType();
		if (type.isCauseRequired()) {
			ValidationUtils.assertNotNull(activity.getCauseSystemTable(), String.format("Cause Table is required for Target Activity of type %s.", type.getName()));
			ValidationUtils.assertNotNull(activity.getCauseFKFieldId(), String.format("Cause FK Field ID is required for Target Activity of type %s.", type.getName()));
		}
	}


	private void validateActivityNote(PortfolioTargetActivity activity) {
		PortfolioTargetActivityType type = activity.getType();
		if (type.isNoteRequired()) {
			ValidationUtils.assertNotNull(activity.getNote(), String.format("Note is required for Target Activity of type %s.", type.getName()));
		}
	}


	private void validateActivitySecurity(PortfolioTargetActivity activity) {
		PortfolioTargetActivityType type = activity.getType();
		if (type.isSecurityRequired()) {
			ValidationUtils.assertNotNull(activity.getSecurity(), String.format("Security is required for Target Activity of type %s.", type.getName()));
		}
	}
}
