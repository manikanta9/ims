package com.clifton.portfolio.target.run;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.target.PortfolioTarget;


/**
 * <code>PortfolioTargetRunReplication</code> defines a specific portfolio target and corresponding position
 * that is part of a replication for a {@link PortfolioTarget}.
 * <p>
 * It contains the target and position for a {@link com.clifton.investment.instrument.InvestmentSecurity}
 * of the target's defined {@link com.clifton.investment.replication.InvestmentReplication}.
 *
 * @author nickk
 */
public class PortfolioTargetRunReplication extends BasePortfolioRunReplication {

	private PortfolioTarget target; // given target we have account and history based on date

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String getTableName() {
		return "PortfolioTargetRunReplication";
	}


	@Override
	public boolean isMatchingReplication() {
		return getTarget() != null && getTarget().getMatchingTarget() != null;
	}


	@Override
	public boolean isRollupReplication() {
		return getTarget() != null && getTarget().isRollup();
	}


	@Override
	public boolean isTargetFollowsActual() {
		return false;
	}


	@Override
	protected String getDependentEntityTopLevelLabel() {
		return getTarget() == null ? "" : getTarget().getTopLevelLabel();
	}


	@Override
	protected String getDependentEntityLabel() {
		return getTarget() == null ? "" : getTarget().getLabel();
	}


	@Override
	public InvestmentAccount getClientAccount() {
		if (getTarget() != null) {
			return getTarget().getClientAccount();
		}
		if (getPortfolioRun() != null) {
			return getPortfolioRun().getClientInvestmentAccount();
		}
		return null;
	}


	@Override
	public InvestmentAccountAssetClass getAccountAssetClass() {
		return null;
	}


	@Override
	public Short getOrder() {
		if (getTarget() != null && getTarget().getOrder() != null) {
			return getTarget().getOrder().shortValue();
		}
		return super.getOrder();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PortfolioTarget getTarget() {
		return this.target;
	}


	public void setTarget(PortfolioTarget target) {
		this.target = target;
	}
}
