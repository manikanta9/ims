package com.clifton.portfolio.target.manager.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountAllocation;
import org.springframework.stereotype.Component;


/**
 * The <code>PortfolioTargetInvestmentManagerAccountAllocationListByAssignmentCache</code> stores the list of
 * {@link PortfolioTargetInvestmentManagerAccountAllocation} objects by assignment id.
 *
 * @author nickk
 */
@Component
public class PortfolioTargetInvestmentManagerAccountAllocationListByAssignmentCache extends SelfRegisteringSingleKeyDaoListCache<PortfolioTargetInvestmentManagerAccountAllocation, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "managerAccountAssignment.id";
	}


	@Override
	protected Integer getBeanKeyValue(PortfolioTargetInvestmentManagerAccountAllocation bean) {
		if (bean.getManagerAccountAssignment() != null) {
			return bean.getManagerAccountAssignment().getId();
		}
		return null;
	}
}
