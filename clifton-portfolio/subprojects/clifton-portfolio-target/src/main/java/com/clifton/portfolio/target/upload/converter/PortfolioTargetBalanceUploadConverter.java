package com.clifton.portfolio.target.upload.converter;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.upload.converter.SystemUploadBeanListConverterImpl;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * <code>PortfolioTargetBalanceUploadConverter</code> is used to populate uploaded {@link PortfolioTargetBalance}
 * with {@link PortfolioTarget} details. Targets can be created if they do not exist.
 * <p>
 * This converter populates a new target without looking up from the database. The target loading and validation
 * will be handled after in the upload process.
 *
 * @author nickk
 */
@Component
public class PortfolioTargetBalanceUploadConverter extends SystemUploadBeanListConverterImpl<PortfolioTargetBalance> {

	@Override
	protected List<PortfolioTargetBalance> populateBeans(PortfolioTargetBalance balance, Map<String, Object> unmappedPropertyMap) {
		for (Map.Entry<String, Object> stringObjectEntry : unmappedPropertyMap.entrySet()) {
			Object value = stringObjectEntry.getValue();
			switch (stringObjectEntry.getKey()) {
				case "Target-Name": {
					getOrCreateBalanceTarget(balance).setName((String) value);
					break;
				}
				case "Target-ClientAccount-Number": {
					PortfolioTarget target = getOrCreateBalanceTarget(balance);
					if (target.getClientAccount() == null) {
						target.setClientAccount(new InvestmentAccount());
					}
					target.getClientAccount().setNumber((String) value);
					break;
				}
				case "Target-Replication-Name": {
					PortfolioTarget target = getOrCreateBalanceTarget(balance);
					if (target.getReplication() == null) {
						target.setReplication(new InvestmentReplication());
					}
					target.getReplication().setName((String) value);
					break;
				}
				case "Target-TargetCalculatorBean-Name": {
					PortfolioTarget target = getOrCreateBalanceTarget(balance);
					if (target.getTargetCalculatorBean() == null) {
						target.setTargetCalculatorBean(new SystemBean());
					}
					target.getTargetCalculatorBean().setName((String) value);
					break;
				}
				case "Target-Parent-Name": {
					PortfolioTarget target = getOrCreateBalanceTarget(balance);
					if (target.getParent() == null) {
						target.setParent(new PortfolioTarget());
					}
					target.getParent().setName((String) value);
					break;
				}
				case "Target-MatchingTarget-Name": {
					PortfolioTarget target = getOrCreateBalanceTarget(balance);
					if (target.getMatchingTarget() == null) {
						target.setMatchingTarget(new PortfolioTarget());
					}
					target.getMatchingTarget().setName((String) value);
					break;
				}
				case "Target-TargetPercent": {
					getOrCreateBalanceTarget(balance).setTargetPercent((BigDecimal) value);
					break;
				}
				case "Target-Order": {
					getOrCreateBalanceTarget(balance).setOrder(value == null ? null : ((BigDecimal) value).intValue());
					break;
				}
				case "Target-Rollup": {
					getOrCreateBalanceTarget(balance).setRollup((Boolean) value);
					break;
				}
			}
		}
		return Collections.singletonList(balance);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private PortfolioTarget getOrCreateBalanceTarget(PortfolioTargetBalance balance) {
		if (balance.getTarget() == null) {
			balance.setTarget(new PortfolioTarget());
		}
		return balance.getTarget();
	}
}
