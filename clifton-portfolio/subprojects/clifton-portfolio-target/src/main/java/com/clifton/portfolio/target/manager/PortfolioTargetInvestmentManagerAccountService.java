package com.clifton.portfolio.target.manager;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.manager.search.PortfolioTargetInvestmentManagerAccountAllocationSearchForm;

import java.util.List;


/**
 * <code>PortfolioTargetInvestmentManagerAccountService</code> is a service for handling associations
 * between {@link com.clifton.portfolio.target.PortfolioTarget} and {@link com.clifton.investment.manager.InvestmentManagerAccount}s
 * including assignments and allocations.
 *
 * @author nickk
 */
public interface PortfolioTargetInvestmentManagerAccountService {

	///////////////////////////////////////////////////////////////////////////
	/////////    Target Manager Assignment Business Services        ///////////
	///////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = PortfolioTargetInvestmentManagerAccountAllocation.class)
	public PortfolioTargetInvestmentManagerAccountAssignment getPortfolioTargetInvestmentManagerAccountAssignment(int id);


	@SecureMethod(dtoClass = PortfolioTargetInvestmentManagerAccountAllocation.class)
	public PortfolioTargetInvestmentManagerAccountAssignment savePortfolioTargetInvestmentManagerAccountAssignment(PortfolioTargetInvestmentManagerAccountAssignment targetManagerAccountAssignment);


	@SecureMethod(dtoClass = PortfolioTargetInvestmentManagerAccountAllocation.class)
	public void savePortfolioTargetInvestmentManagerAccountAssignmentList(List<PortfolioTargetInvestmentManagerAccountAssignment> targetManagerAccountAssignmentList);


	@SecureMethod(dtoClass = PortfolioTargetInvestmentManagerAccountAllocation.class)
	public void deletePortfolioTargetInvestmentManagerAccountAssignment(int id);


	@DoNotAddRequestMapping
	public void replacePortfolioTargetInvestmentManagerAccountAllocation(PortfolioTargetInvestmentManagerAccountAllocation existingAllocation, PortfolioTarget newTarget);


	public List<PortfolioTargetInvestmentManagerAccountAllocation> getPortfolioTargetInvestmentManagerAccountAllocationList(PortfolioTargetInvestmentManagerAccountAllocationSearchForm searchForm);
}
