package com.clifton.portfolio.target;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.target.search.PortfolioTargetSearchForm;
import org.hibernate.Criteria;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;


/**
 * @author mitchellf
 */
public class PortfolioTargetUtils {

	public static final PortfolioTarget ROOT = new PortfolioTarget();


	static {
		ROOT.setId(Integer.MAX_VALUE);
	}


	/**
	 * Takes in a list of Portfolio targets, and filters to a new list of root-level targets with their children populated
	 */
	public static List<PortfolioTarget> buildTargetHierarchy(List<PortfolioTarget> portfolioTargetList) {
		if (CollectionUtils.isEmpty(portfolioTargetList)) {
			return Collections.emptyList();
		}
		portfolioTargetList.sort(Comparator.comparingInt(t -> t.getOrder() == null ? 0 : t.getOrder()));
		List<PortfolioTarget> rootTargets = new ArrayList<>();
		Map<PortfolioTarget, List<PortfolioTarget>> parentToChildTargetList = BeanUtils.getBeansMap(portfolioTargetList, target -> target.getParent() == null ? ROOT : target.getParent());
		parentToChildTargetList.forEach((parent, children) -> {
			if (!ROOT.equals(parent)) {
				parent.setChildTargetList(children);
			}
			else {
				rootTargets.addAll(children);
			}
		});
		return rootTargets;
	}


	public static void validateMatchingTargetRelationshipsForStartDate(PortfolioTarget target, ReadOnlyDAO<PortfolioTarget> dao) {
		PortfolioTargetSearchForm searchForm = new PortfolioTargetSearchForm();
		searchForm.setClientAccountId(target.getClientAccount().getId());
		searchForm.setActiveOnDate(target.getStartDate());
		List<PortfolioTarget> existingTargetList = ((AdvancedReadOnlyDAO<PortfolioTarget, Criteria>) dao).findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		if (target.getId() == null) {
			existingTargetList.add(target);
		}
		PortfolioTargetUtils.buildTargetHierarchy(existingTargetList);
		validateTargetForMatchingTarget(target);
	}


	/**
	 * Validates that the target's Matching Target is neither an ancestor nor a descendent.
	 * This avoids circular dependencies in Target processing. If a target should take a portion of an ancestor's or descendents' balance then it should not do so using the
	 * Matching Target functionality.
	 */
	public static void validateTargetForMatchingTarget(PortfolioTarget target) {
		if (target.getMatchingTarget() != null) {
			validateTargetNotMatchingAncestor(target);
			validateTargetNotMatchingDescendent(target, target.getChildren());
		}
	}


	/**
	 * Validates that the target's Matching Target is not an ancestor.
	 *
	 * @see #validateTargetForMatchingTarget(PortfolioTarget)
	 */
	private static void validateTargetNotMatchingAncestor(PortfolioTarget target) {
		if (target.getMatchingTarget() != null) {
			PortfolioTarget parentTarget = target.getParent();
			while (parentTarget != null) {
				ValidationUtils.assertFalse(target.getMatchingTarget().equals(parentTarget), String.format("Target [%s] cannot have Matching Target [%s] because matching an ancestor Target is not allowed. There are other features that allow a Target to take a portion of an ancestor's balance.",
						target.getLabel(), target.getMatchingTarget().getLabel()));
				parentTarget = parentTarget.getParent();
			}
		}
	}


	/**
	 * Validates that the target's Matching Target is not a descendent.
	 *
	 * @see #validateTargetForMatchingTarget(PortfolioTarget)
	 */
	private static void validateTargetNotMatchingDescendent(PortfolioTarget target, List<PortfolioTarget> childTargetListToValidate) {
		if (target.getMatchingTarget() != null) {
			for (PortfolioTarget child : CollectionUtils.getIterable(childTargetListToValidate)) {
				ValidationUtils.assertFalse(target.getMatchingTarget().equals(child), String.format("Target [%s] cannot have Matching Target [%s] because matching a descendent Target is not allowed. There are other features that allow a Target to take a portion of a descendents' balance.",
						target.getLabel(), target.getMatchingTarget().getLabel()));
				validateTargetNotMatchingDescendent(target, child.getChildren());
			}
		}
	}
}
