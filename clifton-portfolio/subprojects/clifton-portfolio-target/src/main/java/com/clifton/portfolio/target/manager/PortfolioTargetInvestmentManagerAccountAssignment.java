package com.clifton.portfolio.target.manager;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;

import java.util.List;


/**
 * <code>PortfolioTargetInvestmentManagerAccountAssignment</code> is a non-persisted entity
 * that encapsulates {@link InvestmentManagerAccountAssignment} and associated
 * {@link PortfolioTargetInvestmentManagerAccountAllocation}s to assist in resolving module
 * dependency issues.
 *
 * @author nickk
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class PortfolioTargetInvestmentManagerAccountAssignment {

	private InvestmentManagerAccountAssignment managerAccountAssignment;
	private List<PortfolioTargetInvestmentManagerAccountAllocation> targetAllocationList;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PortfolioTargetInvestmentManagerAccountAssignment() {
		// noop constructor for binding
	}


	public PortfolioTargetInvestmentManagerAccountAssignment(InvestmentManagerAccountAssignment managerAccountAssignment) {
		this.managerAccountAssignment = managerAccountAssignment;
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountAssignment getManagerAccountAssignment() {
		return this.managerAccountAssignment;
	}


	public void setManagerAccountAssignment(InvestmentManagerAccountAssignment managerAccountAssignment) {
		this.managerAccountAssignment = managerAccountAssignment;
	}


	public List<PortfolioTargetInvestmentManagerAccountAllocation> getTargetAllocationList() {
		return this.targetAllocationList;
	}


	public void setTargetAllocationList(List<PortfolioTargetInvestmentManagerAccountAllocation> targetAllocationList) {
		this.targetAllocationList = targetAllocationList;
	}
}
