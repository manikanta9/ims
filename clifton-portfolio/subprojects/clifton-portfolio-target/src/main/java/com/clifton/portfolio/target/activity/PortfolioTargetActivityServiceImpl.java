package com.clifton.portfolio.target.activity;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.portfolio.target.activity.search.PortfolioTargetActivitySearchForm;
import com.clifton.portfolio.target.activity.search.PortfolioTargetActivityTypeSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author nickk
 */
@Service
public class PortfolioTargetActivityServiceImpl implements PortfolioTargetActivityService {

	private AdvancedUpdatableDAO<PortfolioTargetActivity, Criteria> portfolioTargetActivityDAO;
	private AdvancedUpdatableDAO<PortfolioTargetActivityType, Criteria> portfolioTargetActivityTypeDAO;

	private DaoNamedEntityCache<PortfolioTargetActivityType> portfolioTargetActivityTypeCache;
	private DaoSingleKeyListCache<PortfolioTargetActivity, Integer> portfolioTargetActivityListByTargetCache;

	///////////////////////////////////////////////////////////////////////////
	//////////      PortfolioTargetActivityType Methods      ///////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioTargetActivityType getPortfolioTargetActivityType(short id) {
		return getPortfolioTargetActivityTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public PortfolioTargetActivityType getPortfolioTargetActivityTypeByName(String name) {
		return getPortfolioTargetActivityTypeCache().getBeanForKeyValueStrict(getPortfolioTargetActivityTypeDAO(), name);
	}


	@Override
	public List<PortfolioTargetActivityType> getPortfolioTargetActivityTypeList(PortfolioTargetActivityTypeSearchForm searchForm) {
		return getPortfolioTargetActivityTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	///////////////////////////////////////////////////////////////////////////
	//////////         PortfolioTargetActivity Methods       ///////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public PortfolioTargetActivity getPortfolioTargetActivity(int id) {
		return getPortfolioTargetActivityDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PortfolioTargetActivity> getPortfolioTargetActivityList(PortfolioTargetActivitySearchForm searchForm) {
		return getPortfolioTargetActivityDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<PortfolioTargetActivity> getPortfolioTargetActivityListByTargetId(int id) {
		return getPortfolioTargetActivityListByTargetCache().getBeanListForKeyValue(getPortfolioTargetActivityDAO(), id);
	}


	@Override
	public PortfolioTargetActivity savePortfolioTargetActivity(PortfolioTargetActivity targetBalanceActivity) {
		return getPortfolioTargetActivityDAO().save(targetBalanceActivity);
	}


	@Override
	public List<PortfolioTargetActivity> savePortfolioTargetActivityList(List<PortfolioTargetActivity> targetBalanceActivityList) {
		getPortfolioTargetActivityDAO().saveList(targetBalanceActivityList);
		return targetBalanceActivityList;
	}


	@Override
	@Transactional
	public void deletePortfolioTargetActivity(int id) {
		PortfolioTargetActivity existing = getPortfolioTargetActivity(id);
		if (existing != null && !existing.isDeleted()) {
			existing.setDeleted(true);
			savePortfolioTargetActivity(existing);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PortfolioTargetActivity, Criteria> getPortfolioTargetActivityDAO() {
		return this.portfolioTargetActivityDAO;
	}


	public void setPortfolioTargetActivityDAO(AdvancedUpdatableDAO<PortfolioTargetActivity, Criteria> portfolioTargetActivityDAO) {
		this.portfolioTargetActivityDAO = portfolioTargetActivityDAO;
	}


	public AdvancedUpdatableDAO<PortfolioTargetActivityType, Criteria> getPortfolioTargetActivityTypeDAO() {
		return this.portfolioTargetActivityTypeDAO;
	}


	public void setPortfolioTargetActivityTypeDAO(AdvancedUpdatableDAO<PortfolioTargetActivityType, Criteria> portfolioTargetActivityTypeDAO) {
		this.portfolioTargetActivityTypeDAO = portfolioTargetActivityTypeDAO;
	}


	public DaoNamedEntityCache<PortfolioTargetActivityType> getPortfolioTargetActivityTypeCache() {
		return this.portfolioTargetActivityTypeCache;
	}


	public void setPortfolioTargetActivityTypeCache(DaoNamedEntityCache<PortfolioTargetActivityType> portfolioTargetActivityTypeCache) {
		this.portfolioTargetActivityTypeCache = portfolioTargetActivityTypeCache;
	}


	public DaoSingleKeyListCache<PortfolioTargetActivity, Integer> getPortfolioTargetActivityListByTargetCache() {
		return this.portfolioTargetActivityListByTargetCache;
	}


	public void setPortfolioTargetActivityListByTargetCache(DaoSingleKeyListCache<PortfolioTargetActivity, Integer> portfolioTargetActivityListByTargetCache) {
		this.portfolioTargetActivityListByTargetCache = portfolioTargetActivityListByTargetCache;
	}
}
