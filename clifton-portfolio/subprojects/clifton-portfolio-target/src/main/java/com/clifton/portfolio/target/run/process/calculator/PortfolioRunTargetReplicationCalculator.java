package com.clifton.portfolio.target.run.process.calculator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.portfolio.account.PortfolioAccountContractStore;
import com.clifton.portfolio.account.PortfolioAccountContractStoreTypes;
import com.clifton.portfolio.replication.PortfolioReplicationAllocationSecurityConfig;
import com.clifton.portfolio.replication.PortfolioReplicationHandler;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioReplicationCurrencyOtherConfig;
import com.clifton.portfolio.run.process.calculator.BasePortfolioRunReplicationProcessStepCalculator;
import com.clifton.portfolio.run.util.PortfolioUtils;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.run.PortfolioTargetRunReplication;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * {@link PortfolioRunTargetReplicationCalculator} is invoked as a step in {@link com.clifton.portfolio.run.PortfolioRun} processing. It generates
 * {@link PortfolioTargetRunReplication}s and performs additional adjustments such as processing currencies and contract splitting.
 *
 * @author nickk
 * @author michaelm
 */
public class PortfolioRunTargetReplicationCalculator extends BasePortfolioRunReplicationProcessStepCalculator<PortfolioTargetRunConfig> {

	private InvestmentInstrumentService investmentInstrumentService;

	private PortfolioReplicationHandler portfolioReplicationHandler;
	private PortfolioTargetRunService portfolioTargetRunService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void processStep(PortfolioTargetRunConfig runConfig) {
		generateTargetRunReplications(runConfig);
		processCurrencyContracts(runConfig);
		processContractSplitting(runConfig);
		updatePortfolioTargetRunReplicationTargets(runConfig);
	}


	@Override
	protected void saveStep(PortfolioTargetRunConfig runConfig) {
		// Replications/CurrencyOther - Clean Up and Save the Data
		// Also populates ReplicationDetail objects where applicable
		cleanUpAndSaveReplications(runConfig);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void generateTargetRunReplications(PortfolioTargetRunConfig runConfig) {
		for (PortfolioTarget target : CollectionUtils.getIterable(runConfig.getPortfolioTargetList())) {
			if (target.getReplication() != null) {
				PortfolioTargetBalance targetBalance = runConfig.getPortfolioTargetToBalanceMap().get(target);
				BigDecimal totalToAllocate = runConfig.getRun().isMarketOnCloseAdjustmentsIncluded() ? targetBalance.getMarketOnCloseValue() : targetBalance.getAdjustedValue();
				List<PortfolioTargetRunReplication> replicationList = createPortfolioTargetRunReplicationList(runConfig, target, totalToAllocate, false);
				applySecuritiesToTargetRunReplications(runConfig, target, replicationList);
				applyCashExposureAmounts(replicationList);
				runConfig.getTargetToRunReplicationListMap().put(target, replicationList);
			}
		}
	}


	private List<PortfolioTargetRunReplication> createPortfolioTargetRunReplicationList(PortfolioTargetRunConfig runConfig, PortfolioTarget target, BigDecimal totalToAllocate, boolean matching) {
		List<PortfolioTargetRunReplication> replications = new ArrayList<>();
		if (target.getReplication() != null) {
			List<PortfolioReplicationAllocationSecurityConfig> allocationConfigList = getAllocationSecurityConfigListForReplication(runConfig, target);
			for (PortfolioReplicationAllocationSecurityConfig allocConfig : CollectionUtils.getIterable(allocationConfigList)) {
				InvestmentReplicationAllocation alloc = allocConfig.getReplicationAllocation();
				BigDecimal weight = alloc.getAllocationWeight();
				// If allocation weight = 0, include it if for now, if no actuals will remove it,
				// otherwise used to indicate to traders to sell existing positions
				if (weight == null) {
					continue;
				}

				BigDecimal allocate = MathUtils.getPercentageOf(weight, totalToAllocate, true);
				replications.add(createPortfolioTargetRunReplication(runConfig, target, allocConfig, weight, allocate, matching));
			}
		}
		return replications;
	}


	private List<PortfolioReplicationAllocationSecurityConfig> getAllocationSecurityConfigListForReplication(PortfolioTargetRunConfig runConfig, PortfolioTarget portfolioTarget) {
		// If we already looked it up/calculated it - return what was stored
		String key = runConfig.getAccountReplicationKey(portfolioTarget);
		return runConfig.getAccountReplicationToReplicationAllocationConfigMap().computeIfAbsent(key, k -> {
			List<InvestmentSecurity> securityList = getSecurityHeldList(runConfig);
			return getPortfolioReplicationHandler().getPortfolioReplicationAllocationSecurityConfigListForBalanceDate(portfolioTarget.getReplication().getId(), runConfig.getBalanceDate(), securityList);
		});
	}


	private List<InvestmentSecurity> getSecurityHeldList(PortfolioTargetRunConfig runConfig) {
		List<Integer> securityIdList = new ArrayList<>();
		for (PortfolioAccountContractStore.PortfolioAccountContractSecurityStore securityStore : CollectionUtils.getIterable(runConfig.getContractStore().getSecurityStoreCollection())) {
			if (!securityIdList.contains(securityStore.getSecurityId())) {
				securityIdList.add(securityStore.getSecurityId());
			}
		}
		return getInvestmentInstrumentService().getInvestmentSecurityListByIds(securityIdList);
	}


	private PortfolioTargetRunReplication createPortfolioTargetRunReplication(PortfolioTargetRunConfig runConfig, PortfolioTarget target, PortfolioReplicationAllocationSecurityConfig replicationAllocationConfig, BigDecimal adjustedAllocationWeight, BigDecimal totalToAllocate, boolean matching) {
		PortfolioTargetRunReplication rep = new PortfolioTargetRunReplication();
		rep.setTarget(target);
		rep.setPortfolioRun(runConfig.getRun());
		rep.setReplication(replicationAllocationConfig.getReplicationAllocation().getReplication());
		rep.setReplicationAllocationHistory(replicationAllocationConfig.getReplicationAllocationHistory());
		rep.setAllocationConfig(replicationAllocationConfig);
		rep.setTargetExposure(totalToAllocate);
		rep.setTargetExposureAdjusted(matching ? BigDecimal.ZERO : totalToAllocate);
		rep.setAllocationWeightAdjusted(adjustedAllocationWeight);
		return rep;
	}


	private void processCurrencyContracts(PortfolioTargetRunConfig runConfig) {
		List<PortfolioCurrencyOther> currencyOtherList = new ArrayList<>();

		// Currency Replications
		List<PortfolioTargetRunReplication> repList = runConfig.getTargetRunReplicationList();
		List<PortfolioTargetRunReplication> fullCurrencyRepList = BeanUtils.filter(repList, PortfolioTargetRunReplication::isCurrencyReplication);
		getPortfolioTargetRunService().processCurrencyContracts(runConfig.getContractStore(), runConfig.getRun(), fullCurrencyRepList, currencyOtherList, new ArrayList<>(), repList);

		runConfig.setPortfolioCurrencyOtherList(currencyOtherList);
	}


	private void applyCashExposureAmounts(List<PortfolioTargetRunReplication> replicationList) {
		if (CollectionUtils.getSize(replicationList) == 1) {
			PortfolioTargetRunReplication rep = replicationList.get(0);
			// if it's the only one - it should be cash exposure one, but double check
			if (rep.isCashExposure()) {
				// If so - give it actual = target
				// NOTE: Contracts for Cash Exposure = the Exposure because Contract Value is always one
				rep.setActualContracts(rep.getTargetExposureAdjusted());
			}
		}
		else if (CollectionUtils.getSize(replicationList) > 1) {
			BigDecimal totalOverlayExposure = BigDecimal.ZERO;
			BigDecimal totalPercentage = BigDecimal.ZERO;

			List<PortfolioTargetRunReplication> cashExposureRepList = new ArrayList<>();

			for (PortfolioTargetRunReplication rep : replicationList) {
				if (rep.isCashExposure()) {
					cashExposureRepList.add(rep);
				}
				else {
					// Uses the original weight of the "current" security only so
					// we don't double could percentages.  Can't use adjusted weight because we need to compare original weight
					// to the original weight of the cash we need to fill, and adjustments to those percentages could be applied
					// from rolling contracts and when including a secondary replication
					totalOverlayExposure = MathUtils.add(totalOverlayExposure, rep.getActualExposureAdjusted());
					if (rep.getAllocationConfig().getCurrentSecurity().equals(rep.getSecurity())) {
						totalPercentage = MathUtils.add(totalPercentage, rep.getAllocationWeight());
					}
				}
			}

			// Calculate exposure total based off of existing amounts/percentage
			BigDecimal expectedTotalExposure = MathUtils.divide(MathUtils.multiply(totalOverlayExposure, MathUtils.BIG_DECIMAL_ONE_HUNDRED), totalPercentage);
			for (PortfolioTargetRunReplication cashRep : CollectionUtils.getIterable(cashExposureRepList)) {
				// IF NO Other exposure, set cash exposure to 0 as well.
				if (MathUtils.isNullOrZero(expectedTotalExposure)) {
					cashRep.setActualContracts(BigDecimal.ZERO);
				}
				else {
					cashRep.setActualContracts(MathUtils.getPercentageOf(cashRep.getAllocationWeight(), expectedTotalExposure, true));
				}
			}
		}
	}


	private void processContractSplitting(PortfolioTargetRunConfig runConfig) {
		PortfolioAccountContractStore contractStore = runConfig.getContractStore();
		List<PortfolioTargetRunReplication> replicationList = runConfig.getTargetToRunReplicationListMap().values().stream()
				.flatMap(CollectionUtils::getStream)
				.collect(Collectors.toList());

		getPortfolioTargetRunService().processContractSplitting(contractStore, replicationList);
	}


	private List<PortfolioTargetRunReplication> applySecuritiesToTargetRunReplications(PortfolioTargetRunConfig runConfig, PortfolioTarget target, List<PortfolioTargetRunReplication> replicationList) {
		for (int i = 0; i < CollectionUtils.getSize(replicationList); i++) {
			PortfolioTargetRunReplication targetRunReplication = replicationList.get(i);

			// Already Processed
			if (targetRunReplication.getSecurity() != null) {
				continue;
			}

			PortfolioReplicationAllocationSecurityConfig allocationConfig = targetRunReplication.getAllocationConfig();
			List<InvestmentSecurity> allocationSecurityList = allocationConfig.getSecurityList();
			InvestmentSecurity current = allocationConfig.getCurrentSecurity();

			// No existing securities, use the current one
			if (CollectionUtils.isEmpty(allocationSecurityList)) {
				if (allocationConfig.getCurrentSecurity() != null) {
					setupReplicationSecurityInfo(targetRunReplication, allocationConfig.getCurrentSecurity(), runConfig);
				}
				else {
					throw new ValidationExceptionWithCause("InvestmentReplication", allocationConfig.getReplicationAllocation().getReplication().getId(),
							"Missing a current security while processing target [" + target.getName() + "] for Investment Replication Allocation ["
									+ allocationConfig.getReplicationAllocation().getLabel() + "], and there are no existing positions for the account that match this replication.");
				}
			}

			else {
				// If there is a current security, and it's not the same as what's held - add it to the list
				if (current != null && !allocationSecurityList.contains(current)) {
					allocationSecurityList.add(current);
				}
			}

			int numSecurityAllocations = CollectionUtils.getSize(allocationSecurityList);
			// Only one - set it up
			if (numSecurityAllocations == 1) {
				setupReplicationSecurityInfo(targetRunReplication, CollectionUtils.getFirstElementStrict(allocationSecurityList), runConfig);
			}
			// More than one - if no current specified - last in the list after sorting takes on the role of the "current" one
			else if (numSecurityAllocations > 1) {
				// Order them in order from earliest end date to newest end date
				allocationSecurityList = BeanUtils.sortWithFunction(allocationSecurityList, InvestmentSecurity::getEndDate, true);

				if (current == null) {
					// Set the last one in the list as the current one if none specified
					current = allocationSecurityList.get(numSecurityAllocations - 1);
				}

				// Create a new replication object for each security allocated to this replication allocation.
				// Clone the replication first in case current is processed before others are cloned from it
				PortfolioTargetRunReplication clonedRep = BeanUtils.cloneBean(targetRunReplication, false, false);
				Iterator<InvestmentSecurity> securityIterator = CollectionUtils.getIterable(allocationSecurityList).iterator();

				while (securityIterator.hasNext()) {
					InvestmentSecurity allocationSecurity = securityIterator.next();
					// The Last one is the rep, the "older" ones create new replications
					if (current.equals(allocationSecurity)) {
						setupReplicationSecurityInfo(targetRunReplication, allocationSecurity, runConfig);
					}
					else {
						// Use the cloned rep so don't get any current security processed values
						PortfolioTargetRunReplication newRep = BeanUtils.cloneBean(clonedRep, false, false);
						setupReplicationSecurityInfo(newRep, allocationSecurity, runConfig);
						List<PortfolioTargetRunReplication> matchingReps = CollectionUtils.getStream(replicationList)
								.filter(r -> r.getAllocationConfig().getCurrentSecurity() != null)
								.filter(r -> r.getAllocationConfig().getCurrentSecurity().equals(newRep.getSecurity()))
								.filter(r -> r.getReplication().equals(newRep.getReplication()))
								.collect(Collectors.toList());
						if (CollectionUtils.isEmpty(matchingReps)) {
							replicationList.add(newRep);
						}
						else {
							// If we are excluding it, then make sure the current security calculator also knows we are excluding so it doesn't try to adjust to TFA
							securityIterator.remove();
						}
					}
				}
			}
		}
		// In case we added items, need to reset the list in the map
		return replicationList;
	}


	private void setupReplicationSecurityInfo(PortfolioTargetRunReplication runReplication, InvestmentSecurity security, PortfolioTargetRunConfig runConfig) {
		String exchangeRateDataSourceName = runConfig.getExchangeRateDataSourceName();
		Map<String, PortfolioTargetRunReplication> replicationSetupMap = runConfig.getReplicationSetupMap();

		String key = runReplication.getTarget().getId() + "_" + runReplication.getReplication().getId() + "_" + security.getId();
		PortfolioTargetRunReplication copyFrom = replicationSetupMap.get(key);
		if (copyFrom != null) {
			runReplication.copySecurityInfo(copyFrom);
		}
		else {
			getPortfolioReplicationHandler().setupInvestmentReplicationSecurityAllocation(runReplication, security, exchangeRateDataSourceName, false);

			replicationSetupMap.put(key, runReplication);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////            Portfolio Target Replication Clean Up Methods        ////////
	////////////////////////////////////////////////////////////////////////////////

	// TODO https://jira.paraport.com/browse/PORTFOLIO-242
	// most logic below this comment is similar to processing for ProductOverlayAssetClassReplicationCalculator, try to pull out common calculations


	private void cleanUpAndSaveReplications(PortfolioTargetRunConfig runConfig) {
		cleanUpRoundingForPortfolioTargetRunReplicationAmounts(runConfig);
		List<PortfolioTargetRunReplication> replicationList = removeZeroTargetAndActualReplications(runConfig);

		BigDecimal targetTotal = BigDecimal.ZERO;
		BigDecimal exposureTotal = BigDecimal.ZERO;
		for (PortfolioTargetRunReplication replication : CollectionUtils.getIterable(replicationList)) {
			targetTotal = CoreMathUtils.sum(targetTotal, replication.getOverlayTargetTotal());
			exposureTotal = CoreMathUtils.sum(exposureTotal, replication.getOverlayExposureTotal());
		}
		BigDecimal mispricingTotal = BigDecimal.ZERO; //(runConfig.isExcludeMispricing() ? null : CoreMathUtils.sumProperty(replicationList, PortfolioTargetRunReplication::getMispricingValue));
		getPortfolioTargetRunService().savePortfolioTargetRunReplicationListByRun(runConfig.getRun().getId(), targetTotal, exposureTotal, mispricingTotal, replicationList);
		getPortfolioRunService().savePortfolioCurrencyOtherListByRun(runConfig.getRun().getId(), runConfig.getPortfolioCurrencyOtherList());
	}


	/**
	 * Primary/Secondary Replications (i.e. Not Matching) Applies Sum Property Difference for:
	 * targetExposure
	 * targetExposureAdjusted
	 * allocationWeightAdjusted
	 * Matching Replications Applies Sum Property Difference for:
	 * allocationWeightAdjusted (Also readjusts to ensure 100% total)
	 */
	private void cleanUpRoundingForPortfolioTargetRunReplicationAmounts(PortfolioTargetRunConfig runConfig) {

		// Go through Recently created replications and fix rounding, get current replication allocation values, etc.
		for (Map.Entry<PortfolioTarget, List<PortfolioTargetRunReplication>> targetEntry : runConfig.getTargetToRunReplicationListMap().entrySet()) {
			List<PortfolioTargetRunReplication> targetRunReplicationList = targetEntry.getValue();

			// Split the replications matching & not matching and fix rounding and add allocations for matching
			List<PortfolioTargetRunReplication> notMatchingRepList = BeanUtils.filter(targetRunReplicationList, targetRunReplication -> !targetRunReplication.isMatchingAllocation());
			List<PortfolioTargetRunReplication> matchingRepList = BeanUtils.filter(targetRunReplicationList, PortfolioTargetRunReplication::isMatchingAllocation);

			if (!CollectionUtils.isEmpty(notMatchingRepList)) {
				// Support for not matching where sum != 100%
				BigDecimal notMatchingTotal = CoreMathUtils.sumProperty(notMatchingRepList, PortfolioTargetRunReplication::getAllocationWeightAdjusted);
				BigDecimal notMatchingTargetTotal = CoreMathUtils.sumProperty(notMatchingRepList, PortfolioTargetRunReplication::getTargetExposureAdjusted);
				// BigDecimal assetClassReplicationTargetTotal = runConfig.getT(ac); TODO get total for target instead of asset class?
				// // If (when rounded to two decimals) equals 100% then considered 100% to avoid rounding issues
				// if (MathUtils.isEqual(MathUtils.BIG_DECIMAL_ONE_HUNDRED, MathUtils.round(notMatchingTotal, 2))) {
				// 	CoreMathUtils.applySumPropertyDifference(notMatchingRepList, PortfolioTargetRunReplication::getTargetExposure, PortfolioTargetRunReplication::setTargetExposure, assetClassReplicationTargetTotal, true);
				// 	CoreMathUtils.applySumPropertyDifference(notMatchingRepList, PortfolioTargetRunReplication::getTargetExposureAdjusted, PortfolioTargetRunReplication::setTargetExposureAdjusted, assetClassReplicationTargetTotal, true);
				// 	notMatchingTargetTotal = assetClassReplicationTargetTotal;
				// }
				// else {
				CoreMathUtils.applySumPropertyDifference(notMatchingRepList, PortfolioTargetRunReplication::getTargetExposure, PortfolioTargetRunReplication::setTargetExposure, notMatchingTargetTotal, true);
				CoreMathUtils.applySumPropertyDifference(notMatchingRepList, PortfolioTargetRunReplication::getTargetExposureAdjusted, PortfolioTargetRunReplication::setTargetExposureAdjusted, notMatchingTargetTotal, true);
				// }
				// Clean up adjusted % based on adjusted value as a percent of total
				// only if total is not equal to 0 otherwise recalc based on adjusted percentages
				boolean recalcFromPercent = MathUtils.isEqual(BigDecimal.ZERO, MathUtils.round(notMatchingTargetTotal, 0));

				for (PortfolioTargetRunReplication nmr : CollectionUtils.getIterable(notMatchingRepList)) {
					if (recalcFromPercent) {
						nmr.setAllocationWeightAdjusted(getPercentValueForReplication(nmr, "Adjusted Target Allocation %", nmr.getAllocationWeightAdjusted(), notMatchingTotal));
					}
					else {
						nmr.setAllocationWeightAdjusted(getPercentValueForReplication(nmr, "Adjusted Target Allocation %", nmr.getTargetExposureAdjusted(), notMatchingTargetTotal));
					}
				}
				// Fix Rounding
				applyAllocationWeightAdjustedSumToOneHundred(notMatchingRepList);
				setPortfolioRunReplicationRebalanceTriggerValues(notMatchingRepList, notMatchingTargetTotal);
			}

			// GET ACTUAL MATCHING ALLOCATION & OTHER MATCHING ALLOCATION
			if (!CollectionUtils.isEmpty(matchingRepList)) {
				BigDecimal matchingTotal = CoreMathUtils.sumProperty(matchingRepList, PortfolioTargetRunReplication::getAllocationWeightAdjusted);
				BigDecimal matchingTargetTotal = CoreMathUtils.sumProperty(matchingRepList, PortfolioTargetRunReplication::getTargetExposureAdjusted);

				// For Matching Replications let's recalc and set adjusted targets based on the actual target amounts over the total.
				// Need to do this because matching replications can span multiple replications and each would be set to 100%
				// this way we get the actual target % as a percentage across all currencies

				// Clean up adjusted % based on adjusted value as a percent of total
				// only if total is not equal to 0 otherwise recalc based on adjusted percentages
				boolean recalcFromPercent = MathUtils.isEqual(BigDecimal.ZERO, MathUtils.round(matchingTargetTotal, 0));
				for (PortfolioTargetRunReplication mr : CollectionUtils.getIterable(matchingRepList)) {
					if (recalcFromPercent) {
						mr.setAllocationWeightAdjusted(getPercentValueForReplication(mr, "Adjusted Target Allocation %", mr.getAllocationWeightAdjusted(), matchingTotal));
					}
					else {
						mr.setAllocationWeightAdjusted(getPercentValueForReplication(mr, "Adjusted Target Allocation %", mr.getTargetExposureAdjusted(), matchingTargetTotal));
					}
				}

				// Fix any rounding
				applyAllocationWeightAdjustedSumToOneHundred(matchingRepList);
				setPortfolioRunReplicationRebalanceTriggerValues(matchingRepList, matchingTargetTotal);
			}
		}

		// Re-Allocate Currency Other Based on New Targets - Only attempt this if we have CCY Other
		if (!CollectionUtils.isEmpty(runConfig.getPortfolioCurrencyOtherList())) {
			PortfolioReplicationCurrencyOtherConfig<PortfolioTargetRunReplication> ccyOtherConfig = new PortfolioReplicationCurrencyOtherConfig<>(PortfolioAccountContractStoreTypes.ACTUAL, runConfig.getRun()
					.getClientInvestmentAccount()
					.getBaseCurrency(), BeanUtils.filter(runConfig.getTargetRunReplicationList(), PortfolioTargetRunReplication::isCurrencyReplication));
			ccyOtherConfig.applyCurrencyOtherToReplications(runConfig.getPortfolioCurrencyOtherList());
		}
	}


	private void applyAllocationWeightAdjustedSumToOneHundred(List<PortfolioTargetRunReplication> replicationList) {
		if (!CollectionUtils.isEmpty(replicationList)) {
			BigDecimal total = CoreMathUtils.sumProperty(replicationList, PortfolioTargetRunReplication::getAllocationWeightAdjusted);
			if (MathUtils.isNullOrZero(total)) {
				boolean nonZero = false;
				for (PortfolioTargetRunReplication rep : CollectionUtils.getIterable(replicationList)) {
					if (!MathUtils.isNullOrZero(rep.getAllocationWeight())) {
						nonZero = true;
						break;
					}
				}
				// if all ORIGINAL ARE zero - apply allocationWeight as 100/count
				if (!nonZero) {
					BigDecimal weight = MathUtils.divide(MathUtils.BIG_DECIMAL_ONE_HUNDRED, CollectionUtils.getSize(replicationList));
					for (PortfolioTargetRunReplication rep : CollectionUtils.getIterable(replicationList)) {
						rep.setAllocationWeightAdjusted(weight);
					}
				}
			}
			total = CoreMathUtils.sumProperty(replicationList, PortfolioTargetRunReplication::getAllocationWeightAdjusted);
			// If Total Is Still 0 and there is more than one replication in the list (Leave Percentages as they are - don't adjust to 100)
			if (!MathUtils.isNullOrZero(total) || replicationList.size() == 1) {
				CoreMathUtils.applySumPropertyDifference(replicationList, PortfolioTargetRunReplication::getAllocationWeightAdjusted, PortfolioTargetRunReplication::setAllocationWeightAdjusted, MathUtils.BIG_DECIMAL_ONE_HUNDRED, true);
			}
		}
	}


	private BigDecimal getPercentValueForReplication(PortfolioTargetRunReplication rep, String fieldName, BigDecimal value, BigDecimal total) {
		BigDecimal result;
		try {
			result = CoreMathUtils.getPercentValue(value, total, true, true);
		}
		catch (ValidationException e) {
			throw new ValidationExceptionWithCause("InvestmentReplication", rep.getReplication().getId(), "Error calculating [" + fieldName + "] for Target: "
					+ rep.getTarget().getLabel() + ", Replication: " + rep.getReplication().getName() + ", Security: " + rep.getSecurity().getLabel() + ": " + e.getMessage());
		}
		return result;
	}


	/**
	 * Replications with a Zero Target - Remove them if no actual either and option not selected on allocation config to keep it
	 * <p>
	 * Dynamic replications can sometimes be built to have a zero target.
	 * If we hold actual positions, need to leave it in the
	 * list so traders know to sell.  If no target and no actual - remove it from the list.
	 * <p>
	 * Or if multiple securities for the allocation, but selected option to sell from first expiring, not current, current may end up with zero target/zero actual
	 */
	private List<PortfolioTargetRunReplication> removeZeroTargetAndActualReplications(PortfolioTargetRunConfig runConfig) {
		// Duplicated Securities - replicationPositionExcludedFromCount
		List<PortfolioTargetRunReplication> replicationList = runConfig.getTargetRunReplicationList();

		List<PortfolioTargetRunReplication> zeroTargetReplications = BeanUtils.filter(replicationList, replication -> MathUtils.isNullOrZero(replication.getTargetExposureAdjusted()));
		if (!CollectionUtils.isEmpty(zeroTargetReplications)) {
			List<PortfolioTargetRunReplication> filteredList = new ArrayList<>();
			for (PortfolioTargetRunReplication replication : CollectionUtils.getIterable(replicationList)) {
				// If their target is zero, but they do have an actual weight (%), contracts, ccy, exposure, etc. assigned to them, then they need to always be included
				if (PortfolioUtils.isPortfolioReplicationEmpty(replication)) {
					PortfolioReplicationAllocationSecurityConfig allocationConfig = replication.getAllocationConfig();
					if (allocationConfig == null || !allocationConfig.isAlwaysIncludeCurrentSecurity()) {
						continue;
					}
				}
				filteredList.add(replication);
			}
			return filteredList;
		}
		return replicationList;
	}


	/**
	 * Update {@link PortfolioTargetRunReplication} adjusted values for {@link com.clifton.investment.replication.InvestmentReplicationAllocation}s that have multiple securities mapped to
	 * them.
	 */
	private void updatePortfolioTargetRunReplicationTargets(PortfolioTargetRunConfig runConfig) {
		List<PortfolioTargetRunReplication> targetRunReplicationList = runConfig.getTargetRunReplicationList();

		// Fix Target Run Replications whose Replication has more than one security mapped to it.
		// Note: applies TFA logic for primary to New Security when a primary instrument is selected
		// on the replication allocation and we hold positions for the primary (already added to the replication list)
		Set<String> targetAndAllocationCheckedList = new HashSet<>();
		for (PortfolioTargetRunReplication replication : CollectionUtils.getIterable(targetRunReplicationList)) {
			PortfolioReplicationAllocationSecurityConfig allocationConfig = replication.getAllocationConfig();
			// Need to Include the PortfolioTargetID in the Unique Key for the Allocation at this point
			// because there can be cases where an account uses the same replication for different Targets.
			// Applied here, because only matters for allocations with more than one security mapped to it
			// So, originally when getting the replication - don't have to do that part twice
			String targetAndAllocationKey = replication.getTarget().getId() + "_" + allocationConfig.getUniqueKey();
			if (!targetAndAllocationCheckedList.add(targetAndAllocationKey)) {
				continue;
			}
			if (allocationConfig.isTargetAdjustmentsNeeded()) {
				// When finding matching - first filter on the allocation unique key - then filter on the target
				List<PortfolioTargetRunReplication> replicationList = CollectionUtils.getStream(targetRunReplicationList)
						.filter(r -> r.getAllocationConfig() != null && StringUtils.isEqual(r.getAllocationConfig().getUniqueKey(), allocationConfig.getUniqueKey()))
						.filter(r -> CompareUtils.isEqual(r.getTarget(), replication.getTarget()))
						.collect(Collectors.toList());

				BigDecimal totalTarget = BigDecimal.ZERO;
				BigDecimal totalTargetPercent = BigDecimal.ZERO;
				Map<InvestmentSecurity, BigDecimal> actualExposureMap = new HashMap<>();
				for (PortfolioTargetRunReplication targetRunReplication : CollectionUtils.getIterable(replicationList)) {
					// Pull final target from "current" since that is where all final adjustments had been applied to.
					if (targetRunReplication.getSecurity().equals(allocationConfig.getCurrentSecurity())) {
						totalTarget = targetRunReplication.getTargetExposureAdjusted();
						totalTargetPercent = targetRunReplication.getAllocationWeightAdjusted();
					}
					actualExposureMap.put(targetRunReplication.getSecurity(), targetRunReplication.getActualExposureAdjusted());
				}
				// Using Actual Exposure and Current Security Target Adjustment Type Calculator - Calculate New Targets for Each Security
				Map<InvestmentSecurity, BigDecimal> newTargetMap = allocationConfig.getCurrentSecurityTargetAdjustmentType().calculateTargetAmountMap(totalTarget, allocationConfig.getCurrentSecurity(), actualExposureMap);
				for (PortfolioTargetRunReplication targetRunReplication : replicationList) {
					BigDecimal newTarget = newTargetMap.get(targetRunReplication.getSecurity());
					targetRunReplication.setTargetExposureAdjusted(newTarget);
					targetRunReplication.setAllocationWeightAdjusted(MathUtils.getPercentageOf(totalTargetPercent, getPercentValueForPortfolioTargetRunReplication(targetRunReplication, "Adjusted Target Allocation %", newTarget, totalTarget), true));
				}
			}
		}
		// Recalc Contract Value where needed after targets are updated (uses the InvestmentReplicationCalculator assigned to the InvestmentReplicationType)
		for (PortfolioTargetRunReplication targetRunReplication : runConfig.getTargetRunReplicationList()) {
			getPortfolioTargetRunService().recalculatePortfolioTargetRunReplicationSecurityInfo(targetRunReplication);
		}
	}


	private BigDecimal getPercentValueForPortfolioTargetRunReplication(PortfolioTargetRunReplication targetRunReplication, String fieldName, BigDecimal value, BigDecimal total) {
		BigDecimal result;
		try {
			result = CoreMathUtils.getPercentValue(value, total, true, true);
		}
		catch (ValidationException e) {
			throw new ValidationExceptionWithCause("InvestmentReplication", targetRunReplication.getReplication().getId(), "Error calculating [" + fieldName + "] for Portfolio Target: "
					+ targetRunReplication.getTarget().getLabel() + ", Replication: " + targetRunReplication.getReplication().getName() + ", Security: " + targetRunReplication.getSecurity().getLabel() + ": " + e.getMessage());
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public PortfolioReplicationHandler getPortfolioReplicationHandler() {
		return this.portfolioReplicationHandler;
	}


	public void setPortfolioReplicationHandler(PortfolioReplicationHandler portfolioReplicationHandler) {
		this.portfolioReplicationHandler = portfolioReplicationHandler;
	}


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}
}
