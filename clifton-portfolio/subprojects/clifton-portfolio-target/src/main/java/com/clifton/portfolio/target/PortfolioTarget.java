package com.clifton.portfolio.target;

import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.schema.column.json.SystemColumnCustomJsonAware;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;


/**
 * <code>PortfolioTarget</code> defines a portfolio target for a client account consisting
 * of target calculation and investment replication classification.
 *
 * @author nickk
 */
public class PortfolioTarget extends NamedHierarchicalEntity<PortfolioTarget, Integer> implements SystemColumnCustomJsonAware {

	public static final String CUSTOM_FIELDS_GROUP_NAME = "Portfolio Target Custom Fields";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private InvestmentAccount clientAccount;

	/**
	 * Target that this target is to match within the portfolio.
	 */
	private PortfolioTarget matchingTarget;

	InvestmentReplication replication;

	/**
	 * @see com.clifton.portfolio.target.calculator.PortfolioTargetCalculator
	 */
	SystemBean targetCalculatorBean;

	/**
	 * Target percent a portfolio should hold. If a replication is defined, the portfolio should hold
	 * security positions with exposure matching this percentage.
	 */
	BigDecimal targetPercent;

	/**
	 * Order this target should be shown in display.
	 */
	Integer order;

	Date startDate;
	Date endDate;

	/**
	 * Flag to indicate if the balance of this target is a rollup of child targets.
	 */
	private boolean rollup;

	/**
	 * Private Targets are used for portfolio processing, but are excluded from reports.
	 * NOTE: this flag is not being used yet but should be used for reporting implementation.
	 */
	private boolean privateTarget;

	/**
	 * Custom properties for this target
	 */
	private CustomJsonString customColumns;

	private List<PortfolioTarget> childTargetList;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * A List of custom column values for this portfolio target (fields are assigned and vary by target)
	 */
	@NonPersistentField
	private String columnGroupName;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getTopLevelLabel() {
		if (getParent() != null) {
			return getParent().getTopLevelLabel();
		}
		return getLabel();
	}


	@Override
	public String getLabel() {
		return new StringBuilder(Optional.ofNullable(getClientAccount()).map(InvestmentAccount::getNumber).orElse("NA")).append(": ")
				.append(getName()).append(" with effective range [")
				.append(DateUtils.fromDate(getStartDate(), DateUtils.DATE_FORMAT_INPUT)).append('-')
				.append(getEndDate() == null ? "" : DateUtils.fromDate(getEndDate(), DateUtils.DATE_FORMAT_INPUT)).append(']')
				.toString();
	}


	public boolean isActiveOn(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}


	public boolean isActive() {
		return isActiveOn(new Date());
	}


	@Override
	public boolean isLeaf() {
		return CollectionUtils.isEmpty(getChildTargetList());
	}


	public List<PortfolioTarget> getChildren() {
		return getChildTargetList();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public PortfolioTarget getMatchingTarget() {
		return this.matchingTarget;
	}


	public void setMatchingTarget(PortfolioTarget matchingTarget) {
		this.matchingTarget = matchingTarget;
	}


	public InvestmentReplication getReplication() {
		return this.replication;
	}


	public void setReplication(InvestmentReplication replication) {
		this.replication = replication;
	}


	public SystemBean getTargetCalculatorBean() {
		return this.targetCalculatorBean;
	}


	public void setTargetCalculatorBean(SystemBean targetCalculatorBean) {
		this.targetCalculatorBean = targetCalculatorBean;
	}


	public BigDecimal getTargetPercent() {
		return this.targetPercent;
	}


	public void setTargetPercent(BigDecimal targetPercent) {
		this.targetPercent = targetPercent;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public boolean isRollup() {
		return this.rollup;
	}


	public void setRollup(boolean rollup) {
		this.rollup = rollup;
	}


	public boolean isPrivateTarget() {
		return this.privateTarget;
	}


	public void setPrivateTarget(boolean privateTarget) {
		this.privateTarget = privateTarget;
	}


	public CustomJsonString getCustomColumns() {
		return this.customColumns;
	}


	public void setCustomColumns(CustomJsonString customColumns) {
		this.customColumns = customColumns;
	}


	public List<PortfolioTarget> getChildTargetList() {
		return this.childTargetList;
	}


	public void setChildTargetList(List<PortfolioTarget> childTargetList) {
		this.childTargetList = childTargetList;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}
}
