package com.clifton.portfolio.target.manager.upload;

import com.clifton.investment.manager.upload.BaseInvestmentManagerAccountAllocationUploadTemplate;


/**
 * <code>PortfolioTargetInvestmentManagerAccountAllocationUploadTemplate</code> is a simplified class that is used to easily upload
 * new manager allocation weights for existing manager assignments
 *
 * @author nickk
 */
public class PortfolioTargetInvestmentManagerAccountAllocationUploadTemplate extends BaseInvestmentManagerAccountAllocationUploadTemplate {

	private String targetName;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getTargetName() {
		return this.targetName;
	}


	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}
}
