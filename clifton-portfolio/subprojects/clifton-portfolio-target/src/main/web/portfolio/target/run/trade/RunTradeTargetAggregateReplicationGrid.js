// PORTFOLIO RUN - TRADE CREATION WINDOW - REPLICATION LIST FORM GRID
TCG.use('Clifton.portfolio.run.trade.BaseRunTradeDetailGrid');
Clifton.portfolio.target.run.trade.RunTradeTargetAggregateReplicationGrid = Ext.applyIf({

	dtoClass: 'com.clifton.portfolio.replication.aggregate.PortfolioAggregateSecurityReplication',
	detailPageClass: 'Clifton.portfolio.target.run.replication.AggregateReplicationWindow',
	urlPrefix: 'portfolioAggregateRunTrade',

	// Group on security instrument
	groupField: 'security.instrument.name',
	groupTextTpl: '{values.group}&nbsp;<span class="amountNegative" id="{groupId}-fe">&nbsp;</span>&nbsp;<span id="{groupId}-rb">&nbsp;</span>&nbsp;<span class="amountAdjusted" id="{groupId}-mw">&nbsp;</span>&nbsp;<span id="{groupId}-td">&nbsp;</span>&nbsp;',

	// These fields may not be edited and may just use default value, so we always need them submitted
	// Send pricing to the server so that if we are trading, we can update the trade prices on the replication
	alwaysSubmitFields: ['tradeSecurityPrice', 'tradeSecurityPriceDate', 'tradeUnderlyingSecurityPrice', 'tradeUnderlyingSecurityPriceDate', 'tradeExchangeRate', 'tradeExchangeRateDate', 'trade.executingBrokerCompany.id', 'trade.tradeDestination.id', 'trade.tradeExecutionType.id', 'trade.holdingInvestmentAccount.id', 'buyContracts', 'sellContracts'],
	doNotSubmitFields: ['Target %', 'Final %', 'Target - Final %', 'approveTrade', 'Difference'],

	// Stored in shared js so can re-use options for service type screen view selections
	viewNames: ['Currency Hedge', 'Defensive Equity'],
	supportMispricingView: true,
	enableClosePositions: true,

	/***********************************************************************
	 * GRID LOADING/RE-LOADING/CLEAR MODIFIED
	 * *********************************************************************/

	getDefaultView: function() {
		const processingTypeName = TCG.getValue('clientInvestmentAccount.serviceProcessingType.name', this.getWindow().defaultData);
		if (processingTypeName) {
			const matchingView = this.viewNames.find(view => view === processingTypeName);
			if (matchingView) {
				return matchingView;
			}
		}
		return 'Currency Hedge';
	},

	// Called when new securities are added to fully retrieve replication list again, not just update the list
	fullReload: function() {
		// Default Reload - Reload Prices - also fully reloads replications
		this.reload();
		// Reset Target Dependencies so can be reloaded
		this.targetDependencyMap = undefined;
	},

	afterReloadGrid: function(grid, panel, record) {
		Clifton.portfolio.target.run.trade.updateAggregateTargets(grid);
		grid.refreshGrandTotalsPerTradeCurrencies(grid);
	},

	refreshGrandTotalsPerTradeCurrencies: function(grid) {
		grid.tradeCurrencies = [...new Set(
			grid.getStore().getRange(0, grid.getStore().getCount())
				.map(record => record.get('tradeValueCurrency.symbol'))
		)];
		grid.getView().refresh();
	},

	afterFormLoad: function(grid, panel) {
		grid.loadMispricing(true);
		grid.loadOverlayLimit();
		panel.rebalanceWarnings = []; // reset - seems to keep from previously opened runs
		panel.assetClassFinalExposure = [];
		if (TCG.isTrue(grid.getWindow().savedSinceOpen)) {
			// reset Currency toolbar field so it matches the data per the result of reloadTrades()
			const tradingCurrencyToolbarField = TCG.getChildByName(grid.getTopToolbar(), 'tradingCurrencyType');
			if (tradingCurrencyToolbarField) {
				tradingCurrencyToolbarField.setValue('BASE_CURRENCY');
			}
			grid.reloadTrades();
		}
		else {
			// Reset Target Dependencies based on trades entered
			Clifton.portfolio.target.run.trade.updateAggregateTargets(grid);
		}
		// Enable Drag And Drop Feature to Attached "Client Trade Direction" notes
		TCG.file.enableDD(grid, grid.getNoteParams(), null, 'tradeNoteForTradeGroupBySourceUpload.json', false);

		grid.refreshGrandTotalsPerTradeCurrencies(grid);
	},

	afterClearModified: function() {
		// Reset Target Dependencies based on trades entered
		Clifton.portfolio.target.run.trade.updateAggregateTargets(this);
		// Reset trade execution type column required flags
		const columnModel = this.getColumnModel();
		const limitPriceColumnIndex = columnModel.findColumnIndex('trade.limitPrice');
		const limitPriceColumnEditor = columnModel.getCellEditor(limitPriceColumnIndex, 0);
		if (TCG.isNotNull(limitPriceColumnEditor.field.valueRequired)) {
			limitPriceColumnEditor.field.valueRequired = {};
		}
	},

	getReloadPricesUrl: function() {
		return 'portfolioAggregateRunTradePrices.json';
	},

	getReloadTradesUrl: function() {
		return 'portfolioAggregateRunTradeAmounts.json';
	},

	getApproveTradesUrl: function() {
		return 'portfolioAggregateRunTradeApprovalProcess.json';
	},

	rowDoubleClickHandler: function(grid, rowIndex) {
		const row = this.getStore().getAt(rowIndex);
		const formPanel = TCG.getParentFormPanel(grid);
		const runId = formPanel.getFormValue('id');
		const securityId = row.id;

		const id = runId * securityId;
		const className = this.getDetailPageClass(grid, row);
		const cmpId = TCG.getComponentId(className, id);
		TCG.createComponent(className, {
			id: cmpId,
			params: {runId: runId, securityId: securityId},
			defaultData: this.getDefaultData(grid, row),
			openerCt: this,
			defaultIconCls: this.getWindow().iconCls
		});
	},

	switchToViewUpdateViewsToggleOptions: function(viewName, columnModel, showOptions) {
		// Update Holding Account column url
		const holdingAccountColumnIndex = columnModel.findColumnIndex('trade.holdingInvestmentAccount.label');
		if (holdingAccountColumnIndex) {
			const holdingAccountColumn = columnModel.getColumnById(holdingAccountColumnIndex);
			holdingAccountColumn.editor.url = 'portfolioAggregateRunTradeDetailHoldingInvestmentAccountList.json';
			holdingAccountColumn.editor.store.url = holdingAccountColumn.editor.url;
			holdingAccountColumn.editor.store.proxy.setUrl(holdingAccountColumn.editor.url);
		}

		if (viewName === 'Currency Hedge') {
			showOptions.forEach(item => {
				if (item.text === 'Show Trade Execution Type Column' || item.text === 'Show Holding Account Column') {
					if (item.rendered) {
						item.setChecked(true);
					}
					else {
						item.checked = true;
					}
					const columnIndex = (item.text === 'Show Trade Execution Type Column')
						? columnModel.findColumnIndex('trade.tradeExecutionType.name') : holdingAccountColumnIndex;
					if (columnIndex) {
						columnModel.setHidden(columnIndex, false);
					}
				}
			});
		}
	},

	/***********************************************************************
	 * TOOLBAR - ADD SECURITY BUTTON
	 * *********************************************************************/

	addButton: function(toolBar, gp) {
		toolBar.add({
			text: 'Add Security',
			iconCls: 'add',
			tooltip: 'Uses selected Target Replication Row as a model and allows adding a new security.',
			scope: this,
			handler: async function() {
				if (TCG.isTrue(gp.validateTradingAllowedRun())) {
					const index = gp.getSelectionModel().getSelectedCell();
					if (index) {
						const store = gp.getStore();
						const rec = store.getAt(index[0]);

						if (TCG.isTrue(rec.json.tradeEntryDisabled)) {
							await TCG.showError('Cannot add a new security to a replication where Trading is Disabled.  Please add the security to a replication where trading is not disabled and it will automatically be added here.', 'Read Only');
							return;
						}

						let className = 'Clifton.portfolio.target.run.replication.AddNewReplicationWindow';
						if (TCG.getValue('security.instrument.hierarchy.investmentType.name', rec.json) === 'Options') {
							className = 'Clifton.portfolio.target.run.replication.AddNewOptionReplicationWindow';
						}

						// get first replication for the aggregate to use for the copy
						const formPanel = TCG.getParentFormPanel(gp);
						const runId = formPanel.getFormValue('id');
						const securityId = TCG.getValue('security.id', rec.json);
						const aggregateReplication = await TCG.data.getDataPromise(`portfolioAggregateRunTradeReplication.json?runId=${runId}&securityId=${securityId}`);
						const firstReplication = aggregateReplication.replicationList[0];
						const id = firstReplication.id;

						const cmpId = TCG.getComponentId(className, id);
						await TCG.createComponent(className, {
							id: cmpId,
							params: {id: id},
							openerCt: gp
						});
					}
					else {
						await TCG.showError('A row must be selected in order to add a new security to the run.  The selected Replication Row is used as a model and to define the target and replication the security belongs to as well as filtering which securities are allowed for selection.', 'No Row Selected');
					}
				}
			}

		});
		toolBar.add('-');
	},

	addToolbarFilterButton: function(toolbar, gp) {
		toolbar.add({xtype: 'label', html: 'Currency:&nbsp;', qtip: 'The currency to view the portfolio with for trading. Default is the client account base currency. '},
			{
				xtype: 'combo', name: 'tradingCurrencyType', hiddenName: 'tradingCurrencyType', mode: 'local', store: {
					xtype: 'arraystore', data: [
						['DEFAULT_CURRENCY', 'Default', 'Default view displaying values in the currency of the client account\'s base currency. Trade values are entered in transaction currency of the security and are reflected in the base currency.'],
						['BASE_CURRENCY', 'Base Currency', 'Base Currency view displaying values in the currency of the client account\'s base currency. Trade values are entered in Base Currency amounts and will be converted on submission to local/transaction values according to the available exchange rate.'],
						['TRANSACTION_CURRENCY', 'Security Transaction Currency (Foreign)', 'Display values in the transaction currency of the security (e.g. Underling CCY for Forwards). Trade values are entered as the transaction currency and are reflected in the same currency.'],
						['SETTLE_CURRENCY', 'Security Settle Currency (Currency Denomination)', 'Settlement currency of the security (denomination currency of the security). Trade values are entered as the settlement currency and are reflected in the same currency.']
					]
				},
				listeners: {
					select: async function(combo, record) {
						if (combo.startValue && record && combo.startValue !== record.get('id')) {
							if ('yes' === await TCG.showConfirm('You selected a different currency to view. Continuing will clear any local changes. Do you want to continue?', 'Reload Portfolio')) {
								const grid = TCG.getParentByClass(combo, TCG.grid.FormGridPanel);
								grid.reload();
							}
						}
					}
				}
			}, '&nbsp;');
	},

	prepareReloadParameters: function(grid, params) {
		const tradingCurrencyToolbarField = TCG.getChildByName(grid.getTopToolbar(), 'tradingCurrencyType');
		if (tradingCurrencyToolbarField && tradingCurrencyToolbarField.getValue()) {
			params['tradingCurrencyType'] = tradingCurrencyToolbarField.getValue();
		}
		return params;
	},

	appendViewMenuItems: function(viewMenu, grid) {
		viewMenu.add('-', {
			text: 'Group by Security Instrument',
			xtype: 'menucheckitem',
			tooltip: 'Group by Security Instrument to view tranche aggregates',
			checkHandler: function(item, checked) {
				grid.getStore().groupBy('security.instrument.name');
			}
		});
	},

	/***********************************************************************
	 * GRID EDITING
	 * *********************************************************************/


	onAfterGrandTotalUpdateFieldValue: function(v, cf) {
		const grid = this;

		// NOTE: USE FORM PANEL setFormValue METHOD SO WINDOW IS NOT MARKED AS MODIFIED!
		switch (cf.dataIndex) {
			case 'Target': {
				const groupValueToRecords = {};
				grid.getStore().each(row => {
					// Set Target Total on the Row so we have it for % columns
					row.data['targetTotal'] = v;
					const rowGroupValue = row.get(grid.groupField);
					if (TCG.isNull(groupValueToRecords[rowGroupValue])) {
						groupValueToRecords[rowGroupValue] = [];
					}
					groupValueToRecords[rowGroupValue].push(row);
				}, grid);
				Object.keys(groupValueToRecords).forEach(groupValue => {
					const groupRecords = groupValueToRecords[groupValue];
					const groupTarget = groupRecords.map(record => record.data['Target']).reduce((previousValue, currentValue) => previousValue + currentValue);
					groupRecords.forEach(record => record.data['targetGroupTotal'] = groupTarget);
				});
				break;
			}
			case 'Exposure': {
				const groupValueToRecords = {};
				grid.getStore().each(row => {
					const rowGroupValue = row.get(grid.groupField);
					if (TCG.isNull(groupValueToRecords[rowGroupValue])) {
						groupValueToRecords[rowGroupValue] = [];
					}
					groupValueToRecords[rowGroupValue].push(row);
				}, grid);
				Object.keys(groupValueToRecords).forEach(groupValue => {
					const groupRecords = groupValueToRecords[groupValue];
					const groupExposure = groupRecords.map(record => record.data['Exposure']).reduce((previousValue, currentValue) => previousValue + currentValue);
					groupRecords.forEach(record => record.data['exposureGroupTotal'] = groupExposure);
				});
				break;
			}
			case 'Final Exposure (Base)': {
				const f = TCG.getParentFormPanel(this).getForm();
				this.setFormCalculatedValue('finalExposure', v);
				const netExposureField = f.findField('netExposure');
				const pv = v - (netExposureField ? netExposureField.getNumericValue() : 0);
				this.setFormCalculatedValue('pendingExposure', pv);
				this.setOverlayLimitValues(v);
				break;
			}
			case 'Exposure (Base)': {
				this.setFormCalculatedValue('netExposure', v);
				break;
			}
			default: {
				// do nothing
			}
		}

		this.extendedOnAfterGrandTotalUpdateFieldValue(v, cf);
	},

	extendedOnAfterGrandTotalUpdateFieldValue: function(v, cf) {
		// Can be implemented by implementations extending this for additional calculations.
	},

	overlayLimitPercent: undefined,
	loadOverlayLimit: function() { // TODO
		const f = TCG.getParentFormPanel(this).getForm();
		const netExposurePercentField = f.findField('netExposurePercent'),
			pendingExposurePercentField = f.findField('pendingExposurePercent');
		if (netExposurePercentField && pendingExposurePercentField) {
			if (TCG.isNull(this.overlayLimitPercent)) {
				// Lookup if undefined
				const actId = TCG.getValue('clientInvestmentAccount.id', f.formValues);
				const url = 'ruleConfigMaxAmountForAdditionalScopeEntityAndDefinition.json?categoryName=Portfolio Run Rules&definitionName=Synthetically Adjusted Positions Exposure Range&additionalScopeEntityId=' + actId;
				TCG.data.getDataValue(url, this, function(value) {
					// Set to '' if no limit defined so we don't try to look it up again
					this.overlayLimitPercent = value;
					if (TCG.isNull(this.overlayLimitPercent)) {
						this.overlayLimitPercent = '';
					}
					this.setOverlayLimitValues(netExposurePercentField.getNumericValue() + pendingExposurePercentField.getNumericValue());
				}, this);
			}
			else {
				this.setOverlayLimitValues(netExposurePercentField.getNumericValue() + pendingExposurePercentField.getNumericValue());
			}
		}
	},

	setOverlayLimitValues: function(actual) {
		// No Limit or no actual, do nothing
		if (TCG.isBlank(this.overlayLimitPercent)) {
			return;
		}
		const fp = TCG.getParentFormPanel(this);
		const ptv = TCG.getValue('portfolioTotalValue', fp.getForm().formValues);
		if (TCG.isNotNull(ptv) && ptv !== 0) {
			const actualPercent = (actual * 100) / ptv;
			const overLimitPercent = this.overlayLimitPercent - actualPercent;

			// If negative result, then we are over limit
			const field = fp.getForm().findField('overlayLimitWarning');
			if (overLimitPercent < 0) {
				field.setVisible(true);
				fp.setFormValue('overlayLimitWarning', '<b>Warning:</b>&nbsp;Account has Synthetic Exposure Limit of ' + this.overlayLimitPercent + ' %. Final Exposure from Net/Pending Overlay is currently ' + Ext.util.Format.number((-1 * overLimitPercent), '0,000.00') + ' % over limit.', true);
			}
			else {
				fp.setFormValue('overlayLimitWarning', '', true);
				field.setVisible(false);
			}
			fp.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		}
	},

	setFormCalculatedValue: function(fieldName, v) {
		const f = TCG.getParentFormPanel(this);
		f.setFormValue(fieldName, Ext.util.Format.number(v, '0,000'), true);

		const ptv = TCG.getValue('portfolioTotalValue', f.getForm().formValues);
		if (TCG.isNotNull(ptv) && ptv !== 0) {
			const percentV = (v * 100) / ptv;
			f.setFormValue(fieldName + 'Percent', Ext.util.Format.number(percentV, '0,000.00'), true);
		}
	},

	onAfterEdit: function(editor, silent) {
		const grid = this;
		const f = editor.field;

		if (f === 'buyContracts' || f === 'sellContracts') {
			const v = editor.value;
			const entryDisabled = editor.record.get('tradeEntryDisabled');
			const record = editor.record;
			const repId = record.get('id');
			Clifton.portfolio.target.run.trade.copyTradesToAggregateTradeEntryDisabledRows(grid, repId, entryDisabled, f, v, silent);
			// update record open/close type
			Clifton.portfolio.run.trade.updateRecordOpenCloseType(grid, record, (f === 'buyContracts'), silent);
		}
		else if (f === 'closePosition') {
			const record = editor.record;
			if (TCG.isTrue(editor.value)) {
				let currentContracts = record.get('currentContracts');
				if (grid && record.data['security.instrument.hierarchy.investmentType.name'] === 'Forwards') {
					const currencyType = TCG.getChildByName(grid.getTopToolbar(), 'tradingCurrencyType').getValue();
					if (currencyType === 'SETTLE_CURRENCY') {
						currentContracts = currentContracts * record.data['tradeValue'];
					}
				}
				let fieldUpdated = void 0;
				let fieldValue = currentContracts;
				if (currentContracts) {
					if (currentContracts < 0) {
						fieldUpdated = 'buyContracts';
						fieldValue = Math.abs(currentContracts);

					}
					else if (currentContracts > 0) {
						fieldUpdated = 'sellContracts';
					}
					if (fieldUpdated) {
						record.set(fieldUpdated, fieldValue);
						grid.onAfterEdit({grid: grid, record: record, field: fieldUpdated, value: fieldValue}, silent);
					}
				}
			}
			else {
				grid.clearTradeValues(record);
			}
		}
	},

	clearTradeValues: function(record, forGroup, disableViewRefresh) {
		const grid = this;
		const clearRecordFieldValue = function(r, fieldName) {
			if (r.isModified(fieldName)) {
				r.set(fieldName, '');
				delete r.modified[fieldName];
			}
		};
		const clearTradeValueFunction = function(r) {
			if (r) {
				const controlEditing = !r.editing;
				if (controlEditing) {
					r.beginEdit();
				}
				clearRecordFieldValue(r, 'buyContracts');
				clearRecordFieldValue(r, 'sellContracts');
				if (controlEditing) {
					r.endEdit();
				}
				grid.onAfterEdit({grid: grid, record: r, field: 'buyContracts', value: ''}, true);
			}
		};
		if (record) {
			if (TCG.isTrue(forGroup)) {
				const store = grid.getStore();
				const groupValue = record.get(store.groupField);
				store.getRange(0, store.getCount()).filter(i => i.get(store.groupField) === groupValue)
					.forEach(clearTradeValueFunction);
			}
			else {
				clearTradeValueFunction(record);
			}
		}
		else {
			grid.getStore().each(clearTradeValueFunction);
		}
		if (!TCG.isTrue(disableViewRefresh)) {
			Clifton.portfolio.target.run.trade.refreshGridView(grid);
		}
	},

	defaultTradeValues: function(record, groupDifference, proportionally) {
		const grid = this;
		const columnModel = grid.getColumnModel();
		const buyColumn = columnModel.getColumnAt(columnModel.findColumnIndex('buyContracts'));
		if (buyColumn) {
			const currencyType = TCG.getChildByName(grid.getTopToolbar(), 'tradingCurrencyType').getValue();

			// Function determines which buy/sell column to populate, clear existing value, and set value for a row.
			const setTradeValueFunction = function(r, value) {
				let fieldUpdated = void 0;
				let fieldValue = value;
				if (value && value > 0) {
					fieldUpdated = 'buyContracts';
				}
				else if (value && value < 0) {
					fieldValue = Math.abs(value);
					fieldUpdated = 'sellContracts';
				}
				if (fieldUpdated) {
					r.beginEdit();
					// clear existing value
					grid.clearTradeValues(r, void 0, true);

					r.set(fieldUpdated, fieldValue);
					r.endEdit();
					grid.onAfterEdit({grid: grid, record: r, field: fieldUpdated, value: fieldValue}, true);
				}
			};

			// Function to set the record's value to the available contracts for the record.
			const defaultTradeValueFunction = function(r) {
				const availableContracts = buyColumn.calculateAvailableContracts(r);
				const multiplyExposureByContractValue = (r.data['security.instrument.hierarchy.investmentType.name'] === 'Forwards')
					&& (currencyType === 'BASE_CURRENCY');
				setTradeValueFunction(r, multiplyExposureByContractValue ? availableContracts * r.data['tradeValue'] : availableContracts);
			};
			// Function use for group based record functions. It aggregates available contracts for the group's rows
			// and applies the value to the row or rows according to the proportional flag.
			const defaultGroupTradeValueFunction = function(r, groupValue) {
				const store = grid.getStore();
				const groupRecords = store.getRange(0, store.getCount()).filter(i => i.get(store.groupField) === groupValue);
				const divideExposureByContractValue = (r.data['security.instrument.hierarchy.investmentType.name'] === 'Forwards')
					&& (currencyType !== 'BASE_CURRENCY');

				const groupRecordAvailableContracts = [];
				let groupTotalAvailableExposure = 0;
				groupRecords.forEach(i => {
					const availableContracts = buyColumn.calculateAvailableContracts(i);
					const recordContractValue = i.data['tradeValue'];
					const recordAvailableEntry = {record: i, availableExposure: availableContracts * recordContractValue, contractValue: recordContractValue};
					groupRecordAvailableContracts.push(recordAvailableEntry);
					groupTotalAvailableExposure += recordAvailableEntry.availableExposure;
					grid.clearTradeValues(i, void 0, true);
				});
				// round to 3 decimal digits
				groupTotalAvailableExposure = TCG.roundToDecimalDigits(groupTotalAvailableExposure, 3);
				if (TCG.isTrue(proportionally)) {
					// sort applicable group records so the largest underweight record gets processed first (reverse sort)
					const groupTotalContractsPositive = groupTotalAvailableExposure > 0;
					const availableGroupRecordToDefault = groupRecordAvailableContracts.filter(i => i.availableExposure > 0 === groupTotalContractsPositive)
						.sort((i1, i2) => groupTotalContractsPositive
							? i1.availableExposure < i2.availableExposure ? 1 : -1
							: i1.availableExposure > i2.availableExposure ? -1 : 1);

					if (availableGroupRecordToDefault.length === 1) {
						// if only one underweight record, give it all
						const result = divideExposureByContractValue ? groupTotalAvailableExposure / availableGroupRecordToDefault[0].contractValue : groupTotalAvailableExposure;
						setTradeValueFunction(availableGroupRecordToDefault[0].record, result);
					}
					else {
						// apply the group difference to each row starting with the most underweight
						let remainingGroupAvailableExposure = groupTotalAvailableExposure;
						for (const i of availableGroupRecordToDefault) {
							let exposureToApply = i.availableExposure;
							if (Math.abs(remainingGroupAvailableExposure) > Math.abs(i.availableExposure)) {
								remainingGroupAvailableExposure -= i.availableExposure;
							}
							else {
								exposureToApply = remainingGroupAvailableExposure;
								remainingGroupAvailableExposure = 0;
							}
							const result = divideExposureByContractValue ? exposureToApply / i.contractValue : exposureToApply;
							setTradeValueFunction(i.record, result);
							if (remainingGroupAvailableExposure === 0) {
								break;
							}
						}
					}
				}
				else {
					const result = divideExposureByContractValue ? groupTotalAvailableExposure / r.data['tradeValue'] : groupTotalAvailableExposure;
					setTradeValueFunction(r, result);
				}
			};
			if (record) {
				if (TCG.isTrue(groupDifference)) {
					defaultGroupTradeValueFunction(record, record.get(grid.getStore().groupField));
				}
				else {
					defaultTradeValueFunction(record);
				}
			}
			else {
				grid.getStore().each(defaultTradeValueFunction);
			}
		}
		Clifton.portfolio.target.run.trade.refreshGridView(grid);
	},

	getBuySellPopulationContextMenuItems: function(column, grid, rowIndex) {
		return [
			{
				text: 'Populate Trade Value For Row',
				tooltip: 'Populate this row\'s Buy or Sell value from the difference between the Target and Current Exposure',
				iconCls: 'run',
				handler: function() {
					const record = grid.getStore().getAt(rowIndex);
					grid.defaultTradeValues(record);
				}
			},
			{
				text: 'Populate Trade Value With Group Difference',
				tooltip: 'Populate this rows Buy or Sell value from the difference between the Target and Current Exposure within the row\'s group association',
				iconCls: 'run',
				handler: function() {
					const record = grid.getStore().getAt(rowIndex);
					grid.defaultTradeValues(record, true);
				}
			},
			{
				text: 'Populate Trade Value With Group Difference Proportionally',
				tooltip: 'Populate the Buy or Sell value of all underweight rows within this row\'s group association by distributing the group\'s difference between the Target and Current Exposure starting at the most underweight',
				iconCls: 'run',
				handler: function() {
					const record = grid.getStore().getAt(rowIndex);
					grid.defaultTradeValues(record, true, true);
				}
			},
			{
				text: 'Clear Trade Value For Row',
				tooltip: 'Clear an entered trade value for this row\'s Buy and/or Sell column',
				iconCls: 'run',
				handler: function() {
					const record = grid.getStore().getAt(rowIndex);
					grid.clearTradeValues(record);
				}
			},
			{
				text: 'Clear Trade Value For Group',
				tooltip: 'Clear an entered trade value for all rows\' Buy and/or Sell column within this row\'s group',
				iconCls: 'run',
				handler: function() {
					const record = grid.getStore().getAt(rowIndex);
					grid.clearTradeValues(record, true);
				}
			},
			'-',
			{
				text: 'Populate Trade Value For All Rows',
				tooltip: 'Populate all rows\' Buy or Sell value from the difference between the Target and Current Exposure',
				iconCls: 'run',
				handler: function() {
					grid.defaultTradeValues();
				}
			},
			{
				text: 'Clear Trade Value For All Rows',
				tooltip: 'Clear an entered trade value for all rows\' Buy and/or Sell column',
				iconCls: 'run',
				handler: function() {
					grid.clearTradeValues();
				}
			}
		];
	},

	/***********************************************************************
	 * DETAIL COLUMN DEFINITIONS -
	 * *********************************************************************/

	detailColumns: [
		{header: 'Trading Disabled', hidden: true, width: 15, dataIndex: 'tradeEntryDisabled', type: 'boolean'},
		{header: 'Security Not Tradable', hidden: true, width: 15, dataIndex: 'securityNotTradable', type: 'boolean'},

		{header: 'Target Replication', hidden: true, width: 150, dataIndex: 'labelLong'},
		{header: 'Rollup Target', hidden: true, width: 150, dataIndex: 'topLevelLabel'},

		{header: 'Replication', hidden: true, width: 140, dataIndex: 'replication.name'},
		{header: 'Currency Replication', hidden: true, width: 40, dataIndex: 'replicationType.currency', type: 'boolean'},
		{header: 'Duration Supported', hidden: true, width: 40, dataIndex: 'replicationType.durationSupported', type: 'boolean'},
		{header: 'Matching Replication', hidden: true, width: 40, dataIndex: 'matchingReplication', type: 'boolean'},
		{header: 'Reverse Exposure', hidden: true, width: 40, dataIndex: 'reverseExposureSign', type: 'boolean'},
		{header: 'Cash Exposure', hidden: true, width: 40, dataIndex: 'cashExposure', type: 'boolean'},

		// Security Info and Price Info
		{header: 'SecurityID', hidden: true, width: 10, dataIndex: 'security.id'},
		{
			header: 'Security', width: 70, dataIndex: 'security.symbol',
			allViews: true,
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data['cashExposure'])) {
					return 'Cash';
				}
				return TCG.renderValueWithTooltip(v, p);
			}
		},
		{header: 'Security End Date', hidden: true, width: 70, dataIndex: 'security.endDate'},
		{header: 'Security Description', hidden: true, width: 135, dataIndex: 'security.description'},
		{header: 'Security Price (Original)', hidden: true, width: 50, dataIndex: 'securityPrice', type: 'float'},
		{header: 'Security Price', hidden: true, width: 50, dataIndex: 'tradeSecurityPrice', type: 'float'},
		{header: 'Security Price Date', hidden: true, width: 50, dataIndex: 'tradeSecurityPriceDate'},
		{header: 'Price Multiplier', hidden: true, width: 50, type: 'currency', dataIndex: 'security.priceMultiplier', numberFormat: '0,000'},
		{header: 'Instrument', hidden: true, width: 70, dataIndex: 'security.instrument.name'},
		{header: 'Investment Type', hidden: true, width: 70, dataIndex: 'security.instrument.hierarchy.investmentType.name'},
		{header: 'CCY', hidden: true, width: 30, dataIndex: 'tradeValueCurrency.symbol', tooltip: 'The currency used for row values for showing target, exposure, and trade impact. Currency selection in the toolbar impacts this value.', viewNames: ['Currency Hedge']},

		// Underlying Price Info
		{header: 'Underlying Security Price (Original)', hidden: true, width: 50, dataIndex: 'underlyingSecurityPrice', type: 'float'},
		{header: 'Underlying Security Price', hidden: true, width: 50, dataIndex: 'tradeUnderlyingSecurityPrice', type: 'float'},
		{header: 'Underlying Security Price Date', hidden: true, width: 50, dataIndex: 'tradeUnderlyingSecurityPriceDate'},

		// Contract Value Price Info
		{header: 'Price Field Name', width: 20, hidden: true, dataIndex: 'contractValuePriceField'},

		{
			header: 'Price(s)', width: 50, hidden: true, type: 'float', dataIndex: 'contractValuePrice',
			viewNames: ['Defensive Equity'],
			tooltip: 'Latest Price info that is relevant to the contract value. i.e. If the contract value is calculated using the security price, underlying security price, security dirty price or strike price',
			renderer: function(v, metaData, r) {
				let qtip = '';
				let val;
				const fld = r.data['contractValuePriceField'];
				let showAdjusted = false;

				if (TCG.isNotBlank(v)) {
					val = v;
					if (fld === 'strikePrice') {
						qtip = 'Strike Price';
					}
					else {
						qtip = fld;
					}
				}
				else if (fld === 'underlyingSecurityPrice') {
					// Underlying Security Price
					val = r.data['underlyingSecurityPrice'];
					if (r.data['tradeUnderlyingSecurityPrice']) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Underlying Price', r.data['underlyingSecurityPrice'], r.data['tradeUnderlyingSecurityPrice'], true, r.data['tradeUnderlyingSecurityPriceDate']);
						val = r.data['tradeUnderlyingSecurityPrice'];
						showAdjusted = !r.data['tradeUnderlyingSecurityPriceDate'];
					}
					else {
						qtip = 'Underlying Price';
					}
				}
				else {
					// Security Price
					val = r.data['securityPrice'];
					if (r.data['tradeSecurityPrice']) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Security Price', r.data['securityPrice'], r.data['tradeSecurityPrice'], true, r.data['tradeSecurityPriceDate']);
						val = r.data['tradeSecurityPrice'];
						showAdjusted = !r.data['tradeSecurityPriceDate'];
					}
					else {
						qtip = 'Security Price';
					}
				}
				if (TCG.isNotBlank(qtip)) {
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				if (showAdjusted) {
					metaData.css = 'amountAdjusted';
				}
				return val;
			}
		},

		// Exchange Rate Info
		{header: 'Exchange Rate (Original)', hidden: true, width: 50, dataIndex: 'exchangeRate', type: 'currency'},
		{header: 'Exchange Rate', hidden: true, width: 50, dataIndex: 'tradeExchangeRate', type: 'currency', useNull: true},
		{header: 'Exchange Rate Date', hidden: true, width: 50, dataIndex: 'tradeExchangeRateDate'},

		// Currency Info
		{header: 'CCY Exchange Rate (Original)', hidden: true, width: 50, dataIndex: 'currencyExchangeRate', type: 'currency', numberFormat: '0,000.0000'},
		{header: 'CCY Exchange Rate', hidden: true, width: 50, dataIndex: 'tradeCurrencyExchangeRate', type: 'currency', numberFormat: '0,000.0000', useNull: true},
		{header: 'CCY Exchange Rate Date', hidden: true, width: 50, dataIndex: 'tradeCurrencyExchangeRateDate'},

		{header: 'CCY Local (Original)', hidden: true, width: 50, dataIndex: 'currencyActualLocalAmount', numberFormat: '0,000'},
		{header: 'CCY Local', hidden: true, width: 50, dataIndex: 'currencyCurrentLocalAmount', numberFormat: '0,000'},
		{header: 'CCY Local (Pending)', hidden: true, width: 50, dataIndex: 'currencyPendingLocalAmount', numberFormat: '0,000'},

		{header: 'CCY Unrealized (Original)', hidden: true, width: 50, dataIndex: 'currencyUnrealizedLocalAmount', numberFormat: '0,000'},
		{header: 'CCY Unrealized', hidden: true, width: 50, dataIndex: 'currencyCurrentUnrealizedLocalAmount', numberFormat: '0,000'},

		{header: 'CCY Other (Original)', hidden: true, width: 50, dataIndex: 'currencyOtherBaseAmount', numberFormat: '0,000'},
		{header: 'CCY Other', hidden: true, width: 50, dataIndex: 'currencyCurrentOtherBaseAmount', numberFormat: '0,000'},
		{header: 'CCY Other (Pending)', hidden: true, width: 50, dataIndex: 'currencyPendingOtherBaseAmount', numberFormat: '0,000'},

		{header: 'Trading CCY (Original)', hidden: true, width: 50, dataIndex: 'currencyDenominationBaseAmount', type: 'currency', numberFormat: '0,000'},
		{header: 'Trading CCY', hidden: true, width: 50, dataIndex: 'currencyCurrentDenominationBaseAmount', type: 'currency', numberFormat: '0,000'},
		{header: 'Trading CCY (Pending)', hidden: true, width: 50, dataIndex: 'currencyPendingDenominationBaseAmount', type: 'currency', numberFormat: '0,000'},

		{
			header: 'CCY Total', hidden: true, width: 75, type: 'currency', numberFormat: '0,000', dataIndex: 'currencyCurrentTotalBaseAmount', negativeInRed: true,
			tooltip: '((Local Amount + Unrealized Local Amount) * Exchange Rate) + Currency Other Base Amount<br>Note: Excludes Trading CCY amount',
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.data['replicationType.currency'])) {
					let qtip = '';

					const actual = Ext.util.Format.number(r.data['currencyActualLocalAmount'], '0,000');
					const current = Ext.util.Format.number(r.data['currencyCurrentLocalAmount'], '0,000');

					const unActual = Ext.util.Format.number(r.data['currencyUnrealizedLocalAmount'], '0,000');
					const unCurrent = Ext.util.Format.number(r.data['currencyCurrentUnrealizedLocalAmount'], '0,000');

					const originalEX = Ext.util.Format.number(r.data['currencyExchangeRate'], '0,000.0000');
					const currentEX = Ext.util.Format.number(r.data['tradeCurrencyExchangeRate'], '0,000.0000');


					if (actual !== current) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Local', actual, current, false);
					}

					if (unActual !== unCurrent) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Unrealized Exposure (Local)', unActual, unCurrent, false);
					}

					if (currentEX !== originalEX) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Exchange Rate', originalEX, currentEX, false, r.data['tradeCurrencyExchangeRateDate']);
					}

					const actualOther = Ext.util.Format.number(r.data['currencyOtherBaseAmount'], '0,000');
					const currentOther = Ext.util.Format.number(r.data['currencyCurrentOtherBaseAmount'], '0,000');

					if (actualOther !== currentOther) {
						qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Other (Base)', actualOther, currentOther, false);
					}

					qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td><hr/></td></tr>';
					qtip += '<tr><td>Actual Base:</td><td style="text-align: right;">' + Ext.util.Format.number((r.data['currencyCurrentLocalAmount'] * r.data['tradeCurrencyExchangeRate']), '0,000') + '</td></tr>';
					qtip += '<tr><td>Unrealized Base:</td><td style="text-align: right;">' + Ext.util.Format.number((r.data['currencyCurrentUnrealizedLocalAmount'] * r.data['tradeCurrencyExchangeRate']), '0,000') + '</td></tr>';
					qtip += '<tr><td>Other Base:</td><td style="text-align: right;">' + currentOther + '</td></tr>';
					qtip += '<tr><td>Total:</td><td style="text-align: right;">' + TCG.renderAmount(v, false, '0,000') + '</td></tr>';

					if (TCG.isNotBlank(qtip)) {
						qtip += '</table>';
					}
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'' + qtip + '\'';

					return TCG.renderAmount(v, false, '0,000');
				}
				return '';
			}
		},

		// Rebalance Triggers
		{header: 'Replication Rebalance Min', hidden: true, width: 50, dataIndex: 'rebalanceTriggerMin', type: 'currency', numberFormat: '0,000', useNull: true},
		{header: 'Replication Rebalance Max', hidden: true, width: 50, dataIndex: 'rebalanceTriggerMax', type: 'currency', numberFormat: '0,000', useNull: true},

		// Contract Value - Tooltips include price information
		{
			header: 'Contract Value (Original)', hidden: true, width: 55, dataIndex: 'value', type: 'currency', numberFormat: '0,000',
			renderer: function(v, metaData, r) {
				const format = (v < 10 && v > -10) ? '0,000.0000' : '0,000'; // bonds
				return Ext.util.Format.number(v, format);
			}
		},
		{
			header: 'Contract Value', hidden: true, width: 55, dataIndex: 'tradeValue', type: 'currency', numberFormat: '0,000', negativeInRed: true, css: 'BORDER-RIGHT: #c0c0c0 1px solid;',
			viewNames: ['Overlay', 'Fully Funded', 'Defensive Equity'],
			tooltip: 'Contract Value calculation is determined by the calculator selected for the Replication Type.',
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.data['cashExposure'])) {
					return null;
				}
				const format = (v < 10 && v > -10) ? '0,000.0000' : '0,000'; // bonds
				let qtip = '';
				let value = Ext.util.Format.number(v, format);
				if (r.data['tradeValue'] !== r.data['value']) {
					qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Security Price', r.data['securityPrice'], r.data['tradeSecurityPrice'], true, r.data['tradeSecurityPriceDate']);
					qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Underlying Price', r.data['underlyingSecurityPrice'], r.data['tradeUnderlyingSecurityPrice'], true, r.data['tradeUnderlyingSecurityPriceDate']);
					qtip = Clifton.portfolio.run.trade.appendMarketDataChangeQtipRow(qtip, 'Exchange Rate', r.data['exchangeRate'], r.data['tradeExchangeRate'], false, r.data['tradeExchangeRateDate']);
					metaData.css = 'amountAdjusted';
					if (TCG.isNotBlank(qtip)) {
						qtip += '</table>';
					}
					metaData.attr = 'qtip=\'' + qtip + '\'';
					value = TCG.renderAmountArrow(r.data['tradeValue'] > r.data['value']) + value;
				}
				return value;
			}
		},

		// Replication Allocation % of grouping
		{
			header: 'Allocation %', width: 50, type: 'currency', dataIndex: 'allocationWeight', summaryType: 'sum', hideGrandTotal: true,
			viewNames: ['Currency Hedge', 'Defensive Equity'],
			viewNameHeaders: [
				{name: 'Currency Hedge', label: 'Weight %'},
				{name: 'Defensive Equity', label: 'Allocation %'}
			]
		},

		// Delta
		{
			header: 'Delta', hidden: true, width: 50, type: 'currency', dataIndex: 'delta', numberFormat: '0,000.0000', useNull: true, css: 'BORDER-RIGHT: #c0c0c0 1px solid;',
			tooltip: 'Market Data Value for <i>Delta</i> field. <br><br><b>Special Adjustment for Options:</b>&nbsp;Delta is Negated If Call Option and Short Target or Put Option and Long Target',
			viewNames: ['Defensive Equity']
		},
		{
			header: 'Factor', width: 80, dataIndex: 'factor', type: 'currency', numberFormat: '0,000.0000', useNull: true, css: 'BORDER-RIGHT: #c0c0c0 1px solid;', tooltip: 'Current Factor Change event value for the security. If there are no events available the value will be 1.',
			viewNames: ['Overlay']
		},

		// Targets
		{
			header: 'Target (Original)', hidden: true, width: 75, dataIndex: 'targetExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			viewNameHeaders: [
				{name: 'Overlay', label: 'Overlay Target (Original)'},
				{name: 'Fully Funded', label: 'Target (Original)'},
				{name: 'Defensive Equity', label: 'Target (Original)'}
			],
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data['matchingReplication']);
			}
		},
		// Original Target + M2M Adjustment (from update with Live Prices) + Screen Target Updates (TFA, Matching update from Non Matching, etc.)
		{
			header: 'Target', dataIndex: 'Target', width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			viewNames: ['Currency Hedge', 'Defensive Equity'],
			tooltip: 'Original Target plus any M2M Adjustments based on price or exchange rate changes.<br><br>Note: May also update based on Target assignment to a Target Exposure Adjustment Used target (to offset its target change).<br><br>Matching Replication targets will update live based on trade entries in their non-matching counterparts.',
			renderer: function(v, p, r) {
				return Clifton.portfolio.target.run.trade.renderAggregateTarget(p, r);
			},
			summaryTotalCondition: function(data, col) {
				return TCG.isFalse(data['matchingReplication']) && col.gridPanel && col.gridPanel.isSingleTradeCurrencyForGrandTotals();
			},
			summaryCalculation: function(v, r, field, data, col) {
				return v + Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r, true);
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},

		// Exposure
		{header: 'Additional Exposure', dataIndex: 'additionalExposure', hidden: true, width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
		{header: 'Total Additional', dataIndex: 'totalAdditionalExposure', hidden: true, width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},

		{
			header: 'Exposure (Base)', dataIndex: 'Exposure (Base)', hidden: true, width: 75, type: 'currency', numberFormat: '0,000', useNull: true, summaryType: 'sum', negativeInRed: true,
			tooltip: '(Actual Contracts * Contract Value) + Additional Exposure + Currency Total<br /><br />Currency is added for Currency replication only.<br />Reverse Exposure option from the replication will reverse all exposure calculated values. Value is in base currency of the client account.',
			renderer: function(v, metaData, r) {
				const format = '0,000';

				const contracts = r.data['currentContracts'];
				let current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data['value']);

				let qtip = '';
				if (TCG.isTrue(r.data['replicationType.currency']) && r.data['currencyCurrentTotalBaseAmount']) {
					qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td>&nbsp;</td></tr>';
					qtip += '<tr><td>Currency Futures Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number(current, '0,000') + '</td></tr>';
					qtip += '<tr><td>Physical Currency Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number((r.data['currencyCurrentLocalAmount'] * r.data['tradeCurrencyExchangeRate']), '0,000') + '</td></tr>';
					qtip += '<tr><td>Physical Other Currency Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['currencyCurrentOtherBaseAmount'], '0,000') + '</td></tr>';
					qtip += '<tr><td>Unrealized Currency Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number((r.data['currencyCurrentUnrealizedLocalAmount'] * r.data['tradeCurrencyExchangeRate']), '0,000') + '</td></tr>';
					qtip += '<tr><td>Total Physical Currency Exposure:</td><td style="text-align: right;">' + TCG.renderAmount(r.data['currencyCurrentTotalBaseAmount'], false, '0,000') + '</td></tr>';
					current = current + r.data['currencyCurrentTotalBaseAmount'];
				}
				if (r.data['additionalExposure'] !== 0) {
					qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td>&nbsp;</td></tr>';
					qtip += '<tr><td>Additional Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['additionalExposure'], '0,000') + '</td></tr>';
					current = current + r.data['additionalExposure'];
				}

				if (TCG.isNotBlank(qtip)) {
					qtip += '</table>';
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				if (current === 0) {
					return '';
				}

				return TCG.renderAmount(current, false, format);
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data['matchingReplication']);
			},
			summaryCalculation: function(v, r, field, data, col) {
				const contracts = r.data['currentContracts'];
				const current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data['value'], r.data['currencyCurrentTotalBaseAmount'], r.data['totalAdditionalExposure']);
				return v + current;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},
		{
			header: 'Exposure', dataIndex: 'Exposure', width: 75, type: 'currency', numberFormat: '0,000', useNull: true, summaryType: 'sum', negativeInRed: true,
			allViews: true,
			tooltip: '(Actual Contracts * Contract Value) + Additional Exposure + Currency Total<br /><br />Currency is added for Currency replication only.<br />Reverse Exposure option from the replication will reverse all exposure calculated values.',
			renderer: function(v, metaData, r) {
				const format = '0,000';

				const contracts = r.data['currentContracts'];
				const actual = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data['value']);
				let current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data['tradeValue']);

				let qtip = '';
				if (actual !== current) {
					qtip += '<table><tr><td>Previous Close:</td><td style="text-align: right;">' + Ext.util.Format.number(actual, '0,000') + '</td></tr><tr><td>Change:</td><td style="text-align: right;">' + TCG.renderAmount(current - actual, true) + '</td></tr>';
				}
				if (TCG.isTrue(r.data['replicationType.currency']) && r.data['currencyCurrentTotalBaseAmount']) {
					qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td>&nbsp;</td></tr>';
					qtip += '<tr><td>Currency Futures Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number(current, '0,000') + '</td></tr>';
					qtip += '<tr><td>Physical Currency Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number((r.data['currencyCurrentLocalAmount'] * r.data['tradeCurrencyExchangeRate']), '0,000') + '</td></tr>';
					qtip += '<tr><td>Physical Other Currency Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['currencyCurrentOtherBaseAmount'], '0,000') + '</td></tr>';
					qtip += '<tr><td>Unrealized Currency Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number((r.data['currencyCurrentUnrealizedLocalAmount'] * r.data['tradeCurrencyExchangeRate']), '0,000') + '</td></tr>';
					qtip += '<tr><td>Total Physical Currency Exposure:</td><td style="text-align: right;">' + TCG.renderAmount(r.data['currencyCurrentTotalBaseAmount'], false, '0,000') + '</td></tr>';
					current = current + r.data['currencyCurrentTotalBaseAmount'];
				}
				if (r.data['additionalExposure'] !== 0) {
					qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td>&nbsp;</td></tr>';
					qtip += '<tr><td>Additional Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['additionalExposure'], '0,000') + '</td></tr>';
					current = current + r.data['additionalExposure'];
				}

				if (TCG.isNotBlank(qtip)) {
					qtip += '</table>';
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				if (current === 0) {
					return '';
				}

				r.data['Exposure'] = current;
				return TCG.renderAmount(current, false, format);
			},
			summaryTotalCondition: function(data, col) {
				return TCG.isFalse(data['matchingReplication']) && col.gridPanel && col.gridPanel.isSingleTradeCurrencyForGrandTotals();
			},
			summaryCalculation: function(v, r, field, data, col) {
				const contracts = r.data['currentContracts'];
				const current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data['tradeValue'], r.data['currencyCurrentTotalBaseAmount'], r.data['totalAdditionalExposure']);
				return v + current;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},
		{
			header: 'Pending Exposure', hidden: true, width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			tooltip: '(Pending Contracts * Contract Value) + Pending Currency Exposure',
			renderer: function(v, metaData, r) {
				const format = '0,000';
				const pending = Clifton.portfolio.run.trade.calculateExposure(r, r.data['pendingContracts'], r.data['tradeValue'], r.data['currencyPendingTotalBaseAmount'], r.data['totalAdditionalExposure']);

				let qtip = '';
				if (TCG.isTrue(r.data['replicationType.currency']) && r.data['currencyPendingTotalBaseAmount']) {
					qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td>&nbsp;</td></tr>';
					qtip += '<tr><td>Currency Futures Pending Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number(pending, '0,000') + '</td></tr>';
					qtip += '<tr><td>Physical Currency Pending Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number((r.data['currencyPendingLocalAmount'] * r.data['tradeCurrencyExchangeRate']), '0,000') + '</td></tr>';
					qtip += '<tr><td>Physical Other Currency Pending Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['currencyPendingOtherBaseAmount'], '0,000') + '</td></tr>';
					qtip += '<tr><td>Total Physical Currency Pending Exposure:</td><td style="text-align: right;">' + TCG.renderAmount(r.data['currencyPendingTotalBaseAmount'], false, '0,000') + '</td></tr>';
				}
				if (TCG.isNotBlank(qtip)) {
					qtip += '</table>';
					metaData.css = 'amountAdjusted';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return TCG.renderAmount(pending, false, format);

			},
			summaryTotalCondition: function(data, col) {
				return TCG.isFalse(data['matchingReplication']) && col.gridPanel && col.gridPanel.isSingleTradeCurrencyForGrandTotals();
			},
			summaryCalculation: function(v, r, field, data, col) {
				const pending = Clifton.portfolio.run.trade.calculateExposure(r, r.data['pendingContracts'], r.data['tradeValue']);
				return v + pending;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},

		{
			header: 'Exposure Difference', dataIndex: 'Exposure Difference', hidden: true, width: 85, type: 'currency', css: 'BORDER-RIGHT: #c0c0c0 1px solid;',
			allViews: true,
			viewNameHeaders: [
				{name: 'Currency Hedge', label: 'Difference'},
				{name: 'Defensive Equity', label: 'Difference'}
			],
			tooltip: 'Target - Exposure<br /><br />Does NOT include Pending Exposure',
			renderer: function(v, p, r) {
				const target = Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r);
				const contracts = r.data['currentContracts'];
				const current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data['tradeValue'], r.data['currencyCurrentTotalBaseAmount'], r.data['totalAdditionalExposure']);
				const dif = target - current;
				return TCG.renderAmount(dif, false, '0,000');
			},
			summaryTotalCondition: function(data, col) {
				return TCG.isFalse(data['matchingReplication']) && col.gridPanel && col.gridPanel.isSingleTradeCurrencyForGrandTotals();
			},
			summaryCalculation: function(v, r, field, data, col) {
				const target = Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r);
				const contracts = r.data['currentContracts'];
				const current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data['tradeValue'], r.data['currencyCurrentTotalBaseAmount'], r.data['totalAdditionalExposure']);
				const d = target - current;
				return v + d;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},
		{
			header: 'Exposure Difference %', dataIndex: 'Exposure Difference %', hidden: true, width: 85, type: 'currency', css: 'BORDER-RIGHT: #c0c0c0 1px solid;',
			allViews: true,
			viewNames: ['Currency Hedge', 'Defensive Equity'],
			viewNameHeaders: [
				{name: 'Currency Hedge', label: 'Difference %'},
				{name: 'Defensive Equity', label: 'Difference %'}
			],
			tooltip: 'Percentage of the Exposure respective to the Target: Target - Exposure / Target<br /><br />Does NOT include Pending Exposure',
			renderer: function(v, p, r) {
				const target = Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r);
				const contracts = r.data['currentContracts'];
				const current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data['tradeValue'], r.data['currencyCurrentTotalBaseAmount'], r.data['totalAdditionalExposure']);
				const dif = target - current;
				const difPercent = dif / target * 100;
				return TCG.renderPercent(difPercent);
			},
			summaryTotalCondition: function(data, col) {
				return TCG.isFalse(data['matchingReplication']) && col.gridPanel && col.gridPanel.isSingleTradeCurrencyForGrandTotals();
			},
			summaryCalculation: function(v, r, field, data, col, grid) {
				const groupRecords = grid.store.getRange().filter(record => record.get(grid.groupField) === r.get(grid.groupField));
				const target = groupRecords.map(record => record.data['targetGroupTotal']).reduce((previousValue, currentValue) => previousValue + currentValue);
				const exposure = groupRecords.map(record => record.data['exposureGroupTotal']).reduce((previousValue, currentValue) => previousValue + currentValue);
				return TCG.isBlank(target) ? v : (target - exposure) / target * 100;
			},
			summaryRenderer: function(v) {
				return TCG.renderPercent(v);
			}
		},

		// Contracts
		{
			header: 'Target Contracts', width: 70, type: 'currency', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			viewNames: ['Fully Funded', 'Defensive Equity'],
			tooltip: '(Target (including M2M Adjustment and Trading Currency (if Currency Replication)) - Currency Total (for Currency Replication only)) / Contract Value',
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data['cashExposure'])) {
					return null;
				}
				let target = Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r);
				if (TCG.isTrue(r.data['replicationType.currency'])) {
					target = target - r.data['currencyCurrentTotalBaseAmount'];
				}
				target = target / r.data['tradeValue'];
				if (TCG.isTrue(r.data['reverseExposureSign'])) {
					target = target * -1;
				}
				return TCG.renderAmount(target, false, '0,000.00');
			},
			summaryCondition: function(data) {
				return TCG.isFalse(data['cashExposure']);
			}
		},

		{
			header: 'Actual Contracts (Original)', hidden: true, width: 70, dataIndex: 'actualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data['cashExposure'])) {
					return null;
				}
				return TCG.renderAmount(v, false, '0,000');
			},
			summaryCondition: function(data) {
				return TCG.isFalse(data['cashExposure']);
			}
		},
		{
			header: 'Actual Contracts', hidden: true, width: 80, dataIndex: 'actualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data['cashExposure'])) {
					return null;
				}
				return TCG.renderAdjustedAmount(v, r.data['actualContracts'] !== v, r.data['actualContracts'], '0,000', 'Virtual Contracts');
			},
			summaryCondition: function(data) {
				return TCG.isFalse(data['cashExposure']);
			}
		},
		{
			header: 'Contracts', width: 70, dataIndex: 'currentContracts', type: 'int', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			viewNames: ['Overlay', 'Defensive Equity', 'Fully Funded'],
			tooltip: 'Total Contracts that apply to this replication that have been booked to the General Ledger as of the following business day from the run balance date.<br><b>NOTE:&nbsp;</b>If this is a currently active run for trading the current balance includes everything up to 30 days in the future from today, not as of the balance date.',
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.data['cashExposure'])) {
					return null;
				}
				if (r.data['actualContracts'] !== v || r.data['currentContracts'] !== v) {
					metaData.css = 'amountAdjusted';
					let qtip = '<table>';
					qtip += '<tr><td>&nbsp;</td><td style="text-align: right;">Actual</td><td>&nbsp;</td><td style="text-align: right;">Virtual</td><td>&nbsp;</td><td style="text-align: right;">Total</td></tr>';
					qtip += '<tr><td>Previous Close:</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['actualContracts'], '0,000') + '</td><td>&nbsp;</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['virtualContracts'], '0,000') + '</td><td>&nbsp;</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['actualContracts'], '0,000') + '</td></tr>';
					if (r.data['actualContracts'] !== v) {
						qtip += '<tr><td>Current:</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['currentContracts'], '0,000') + '</td><td>&nbsp;</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['currentVirtualContracts'], '0,000') + '</td><td>&nbsp;</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['currentContracts'], '0,000') + '</td></tr>';
						qtip += '<tr><td colspan="6"><hr/></td></tr>';
						qtip += '<tr><td>Change:</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['currentContracts'] - r.data['actualContracts'], '0,000') + '</td><td>&nbsp;</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['currentVirtualContracts'] - r.data['virtualContracts'], '0,000') + '</td><td>&nbsp;</td><td style="text-align: right;">' + Ext.util.Format.number(r.data['currentContracts'] - r.data['actualContracts'], '0,000') + '</td></tr>';
					}
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return TCG.numberFormat(v, '0,000');
			},
			summaryCondition: function(data) {
				return TCG.isFalse(data['cashExposure']);
			}
		},
		{
			header: 'Pending Contracts', width: 70, dataIndex: 'pendingContracts', type: 'int', summaryType: 'sum', negativeInRed: true, useNull: true, hideGrandTotal: true,
			hidden: true, viewName: ['Defensive Equity'],
			tooltip: 'Pending Trades (Not Closed) + Pending Transfers (Unbooked Transfers To/From this account)',
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data['cashExposure'])) {
					return null;
				}
				const grid = this.gridPanel;
				if (grid) {
					grid.applyPendingTradesTooltipToColumn.call(grid, v, p, r);
				}
				return TCG.renderAdjustedAmount(v, r.data['pendingContracts'] !== v, r.data['pendingContracts'], '0,000', 'Virtual Contracts');
			}
		},
		{
			header: 'Pending Exposure', width: 70, dataIndex: 'pendingExposure', type: 'currency', summaryType: 'sum', negativeInRed: true, useNull: true, hideGrandTotal: true,
			viewName: ['Currency Hedge'],
			tooltip: 'Pending Trades (Not Closed) + Pending Transfers (Unbooked Transfers To/From this account)',
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data['cashExposure'])) {
					return null;
				}
				const contracts = r.data['pendingContracts'];
				const exp = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.data['tradeValue']);

				const grid = this.gridPanel;
				if (grid) {
					grid.applyPendingTradesTooltipToColumn.call(grid, exp, p, r);
				}

				return (exp === 0) ? '' : TCG.renderAmount(exp, false, '0,000');
			},
			summaryCalculation: function(v, r, field, data, col) {
				const exp = Clifton.portfolio.run.trade.calculateExposure(r, r.data['pendingContracts'], r.data['tradeValue']);
				return v + exp;
			},
			summaryTotalCondition: function(data, col) {
				return TCG.isFalse(data['matchingReplication']) && col.gridPanel && col.gridPanel.isSingleTradeCurrencyForGrandTotals();
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			}
		},
		{
			header: 'Difference', width: 65, type: 'currency', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			viewNames: ['Overlay', 'Defensive Equity', 'Fully Funded'],
			tooltip: 'Target Contracts - Actual Contracts - Pending Contracts<br /><br />Suggested Contracts to Buy/Sell to reach the target.',
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data['cashExposure'])) {
					return null;
				}

				let target = Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r);
				if (TCG.isTrue(r.data['replicationType.currency'])) {
					target = target - r.data['currencyCurrentTotalBaseAmount'];
				}
				target = target / r.data['tradeValue'];
				if (TCG.isTrue(r.data['reverseExposureSign'])) {
					target = target * -1;
				}
				const current = r.data['currentContracts'];
				const pending = r.data['pendingContracts'];
				// Set the Value So it can be used as the Suggested Trade Amount for the Trade Quantity Violation
				r.data['Difference'] = (target - current - pending);
				if (this.gridPanel && r.data['security.instrument.hierarchy.investmentType.name'] === 'Forwards') {
					const currencyType = TCG.getChildByName(this.gridPanel.getTopToolbar(), 'tradingCurrencyType').getValue();
					if (currencyType === 'SETTLE_CURRENCY') {
						r.data['Difference'] = r.data['Difference'] * r.data['tradeValue'];
					}
				}
				return TCG.renderAmount(r.data['Difference'], false, '0,000.00');
			},
			summaryCondition: function(data) {
				return TCG.isFalse(data['cashExposure']);
			}
		},
		{
			header: 'Difference %', width: 65, type: 'percent', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true,
			viewNames: ['Overlay', 'Fully Funded'],
			tooltip: 'Percentage of difference in respect to the target (Target Contracts - Actual Contracts - Pending Contracts) / Target Contracts',
			renderer: function(v, p, r) {
				if (TCG.isTrue(r.data['cashExposure'])) {
					return null;
				}

				let target = Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r);
				if (TCG.isTrue(r.data['replicationType.currency'])) {
					target = target - r.data['currencyCurrentTotalBaseAmount'];
				}
				target = target / r.data['tradeValue'];
				if (TCG.isTrue(r.data['reverseExposureSign'])) {
					target = target * -1;
				}
				const differentPercent = r.data['Difference'] / target;
				return TCG.renderPercent(differentPercent);
			},
			summaryCondition: function(data) {
				return TCG.isFalse(data['cashExposure']);
			}
		},

		// Trading Fields
		{
			header: 'Buy', width: 50, dataIndex: 'buyContracts', type: 'int', useNull: true, editor: {xtype: 'spinnerfield', allowBlank: true, minValue: 1, maxValue: 1000000000}, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			allViews: true,
			eventListeners: {
				contextmenu: function(column, grid, rowIndex, event) {
					const menu = new Ext.menu.Menu({
						items: grid.getBuySellPopulationContextMenuItems(column, grid, rowIndex)
					});

					event.preventDefault();
					menu.showAt(event.getXY());
				}
			},
			renderer: function(value, metaData, r) {
				const tradingDisabled = r.data['tradeEntryDisabled'];

				if (TCG.isNotBlank(value) && value !== 0) {
					if (TCG.isFalse(tradingDisabled)) {
						const originalDifference = this.calculateAvailableContracts(r);
						if (originalDifference - value < -5) {
							metaData.css = 'ruleViolation';
							metaData.attr = 'qtip="Buys entered exceeds suggested trade amount by ' + Ext.util.Format.number((originalDifference - value) * -1, '0,000') + ' contracts."';
						}
						else {
							metaData.css = 'buy-dark';
						}
					}
					else {
						metaData.css = 'buy-dark';
					}

				}
				if (TCG.isTrue(r.data['securityNotTradable'])) {
					metaData.attr = 'qtip="Security is non-tradable.  Trades cannot be entered."';
				}
				else if (TCG.isTrue(r.json.cashExposure)) {
					metaData.attr = 'qtip="Security is non-tradable.  Security represents cash exposure."';
				}
				else if (TCG.isTrue(tradingDisabled)) {
					metaData.attr = 'qtip="Trading against this target is disabled. Trades populated here are from security trades entered in other target replications on this screen."';
				}
				return TCG.numberFormat(value, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data['tradeEntryDisabled']);
			},

			summaryRenderer: function(value) {
				return TCG.numberFormat(value, '0,000');
			},
			calculateAvailableContracts: function(r) {
				// round to 3 decimal digits
				return TCG.roundToDecimalDigits(r.data['Difference'], 3);
			}
		},
		{
			header: 'Sell', width: 50, dataIndex: 'sellContracts', type: 'int', useNull: true, editor: {xtype: 'spinnerfield', allowBlank: true, minValue: 1, maxValue: 1000000000}, summaryType: 'sum', css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid; BORDER-RIGHT: #c0c0c0 1px solid;',
			allViews: true,
			eventListeners: {
				contextmenu: function(column, grid, rowIndex, event) {
					const menu = new Ext.menu.Menu({
						items: grid.getBuySellPopulationContextMenuItems(column, grid, rowIndex)
					});

					event.preventDefault();
					menu.showAt(event.getXY());
				}
			},
			renderer: function(value, metaData, r) {
				const tradingDisabled = r.data['tradeEntryDisabled'];

				if (TCG.isNotBlank(value) && value !== 0) {
					if (TCG.isFalse(tradingDisabled)) {
						let originalDifference = this.calculateAvailableContracts(r);
						if (originalDifference > 0) {
							metaData.css = 'ruleViolation';
							metaData.attr = 'qtip="Suggested to buy ' + Ext.util.Format.number(originalDifference, '0,000') + ' trades, however sells were entered."';
						}
						else {
							originalDifference = originalDifference * -1; // Make it positive
							if (originalDifference - value < -5) {
								metaData.css = 'ruleViolation';
								metaData.attr = 'qtip="Sells entered exceeds suggested trade amount by ' + Ext.util.Format.number((originalDifference - value) * -1, '0,000') + ' contracts."';
							}
							else {
								metaData.css = 'sell-dark';
							}
						}
					}
					else {
						metaData.css = 'sell-dark';
					}
				}
				if (TCG.isTrue(r.data['securityNotTradable'])) {
					metaData.attr = 'qtip="Security is non-tradable.  Trades cannot be entered."';
				}
				else if (TCG.isTrue(r.json.cashExposure)) {
					metaData.attr = 'qtip="Security is non-tradable.  Security represents cash exposure."';
				}
				else if (TCG.isTrue(tradingDisabled)) {
					metaData.attr = 'qtip="Trading against this target is disabled. Trades populated here are from security trades entered in other target replications on this screen."';
				}

				return TCG.numberFormat(value, '0,000');
			},
			summaryTotalCondition: function(data, col) {
				return TCG.isFalse(data['tradeEntryDisabled']) && col.gridPanel && col.gridPanel.isSingleTradeCurrencyForGrandTotals();
			},
			summaryRenderer: function(value) {
				return TCG.numberFormat(value, '0,000');
			},
			calculateAvailableContracts: function(r) {
				// round to 3 decimal digits
				return TCG.roundToDecimalDigits(r.data['Difference'], 3);
			}
		},
		{
			header: 'Open/Close', width: 70, dataIndex: 'openCloseType.name', idDataIndex: 'openCloseType.id', type: 'combo', useNull: false, hidden: true,
			viewNames: ['Defensive Equity'],
			editor: {
				xtype: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeListFind.json',
				beforequery: function(queryEvent) {
					// Clear Query Store so Re-Queries each time
					this.store.removeAll();
					this.lastQuery = null;

					const record = queryEvent.combo.gridEditor.record;
					queryEvent.combo.store.baseParams = {};
					if (TCG.isNotBlank(record.get('buyContracts'))) {
						queryEvent.combo.store.baseParams['buy'] = true;
					}
					else if (TCG.isNotBlank(record.get('sellContracts'))) {
						queryEvent.combo.store.baseParams['buy'] = false;
					}
					if (TCG.getValue('security.instrument.hierarchy.investmentType.name', record.json) === 'Options') {
						// filter to those in format "[Buy|Sell] To [Open|Close]"
						queryEvent.combo.store.baseParams['name'] = ' To ';
					}
					else {
						// filter to Buy or Sell only for non-options
						queryEvent.combo.store.baseParams['open'] = true;
						queryEvent.combo.store.baseParams['close'] = true;
					}
				}
			},
			renderer: function(v, metaData) {
				if (TCG.isNotBlank(v)) {
					metaData.css = v.includes('Buy') ? 'buy-light' : 'sell-light';
				}
				return v;
			}
		},
		{
			header: 'Trade Impact', width: 75, type: 'currency', numberFormat: '0,000', useNull: true, summaryType: 'sum', negativeInRed: true,
			allViews: true,
			tooltip: 'Impact on Exposure from the Buy or Sell trade entered',
			renderer: function(v, p, r) {
				const exp = this.calculateValue(r);
				return (exp === 0) ? '' : TCG.renderAmount(exp, false, '0,000');
			},
			summaryTotalCondition: function(data, col) {
				return TCG.isFalse(data['matchingReplication']) && col.gridPanel && col.gridPanel.isSingleTradeCurrencyForGrandTotals();
			},
			summaryCalculation: function(v, r, field, data, col) {
				const exp = col.calculateValue(r);
				return v + exp;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			},
			calculateValue: function(r) {
				let contractValue = r.data['tradeValue'];
				if (this.gridPanel && r.data['security.instrument.hierarchy.investmentType.name'] === 'Forwards') {
					const currencyType = TCG.getChildByName(this.gridPanel.getTopToolbar(), 'tradingCurrencyType').getValue();
					if (currencyType !== 'DEFAULT_CURRENCY') {
						contractValue = 1;
					}
				}
				return Clifton.portfolio.run.trade.calculateExposure(r, r.data['buyContracts'] - r.data['sellContracts'], contractValue);
			}
		},

		// Final Results - Exposure, etc. Based on All Actual, Current, Pending, and Entered Trades
		{// Final Exposure (Base) is used in Exposure Recap to show Pending and Net Exposure in Base CCY
			header: 'Final Exposure (Base)', dataIndex: 'Final Exposure (Base)', hidden: true, width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			tooltip: 'Exposure + Pending Exposure + Trade Impact in base currency',
			renderer: function(v, p, r) {
				const finalExp = this.calculateExposure(r);
				const repMin = r.data['rebalanceTriggerMin'];
				const repMax = r.data['rebalanceTriggerMax'];
				let repOutside = false;
				const repOff = finalExp - Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r);
				// If we have min, then we also have max
				if (TCG.isNotBlank(repMin)) {
					if (repOff < repMin || repOff > repMax) {
						repOutside = true;
					}
					if (TCG.isTrue(repOutside)) {
						p.css = 'ruleViolation';
						p.attr = 'qtip="Final Exposure for this replication allocation is currently outside of defined rebalance bands."';
					}
				}
				return TCG.renderAmount(finalExp, false, '0,000');
			},
			summaryTotalCondition: function(data) {
				return TCG.isFalse(data['matchingReplication']);
			},
			summaryCalculation: function(v, r, field, data, col, grid) {
				const finalExp = col.calculateExposure(r);
				return v + finalExp;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			},
			calculateExposure: function(r) {
				return this.gridPanel ? this.gridPanel.calculateRowFinalExposureConsideringCcyAdjustedTradeImpact(r, r.data['value'])
					: Clifton.portfolio.target.run.trade.calculateFinalAggregateExposure(r, false, r.data['value']);
			}
		},
		{
			header: 'Final Exposure', dataIndex: 'Final Exposure', width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
			allViews: true,
			viewNameHeaders: [
				{name: 'Overlay', label: 'Final Overlay Exposure'},
				{name: 'Fully Funded', label: 'Final Exposure'},
				{name: 'Defensive Equity', label: 'Final Exposure'}
			],
			tooltip: 'Exposure + Pending Exposure + Trade Impact',
			renderer: function(v, p, r) {
				const finalExp = this.calculateExposure(r);
				const repMin = r.data['rebalanceTriggerMin'];
				const repMax = r.data['rebalanceTriggerMax'];
				let repOutside = false;
				const repOff = finalExp - Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r);
				// If we have min, then we also have max
				if (TCG.isNotBlank(repMin)) {
					if (repOff < repMin || repOff > repMax) {
						repOutside = true;
					}
					if (TCG.isTrue(repOutside)) {
						p.css = 'ruleViolation';
						p.attr = 'qtip="Final Exposure for this replication allocation is currently outside of defined rebalance bands."';
					}
				}
				return TCG.renderAmount(finalExp, false, '0,000');
			},
			summaryTotalCondition: function(data, col) {
				return TCG.isFalse(data['matchingReplication']) && col.gridPanel && col.gridPanel.isSingleTradeCurrencyForGrandTotals();
			},
			summaryCalculation: function(v, r, field, data, col, grid) {
				const finalExp = col.calculateExposure(r);

				const target = r.data['target.label'];
				const finalExpKey = 'Target Final Exp_' + target;

				if (TCG.isFalse(r.data['matchingReplication'])) {
					data[finalExpKey] = (data[finalExpKey] || 0) + finalExp;
				}

				return v + finalExp;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			},
			calculateExposure: function(r) {
				return this.gridPanel ? this.gridPanel.calculateRowFinalExposureConsideringCcyAdjustedTradeImpact(r)
					: Clifton.portfolio.target.run.trade.calculateFinalAggregateExposure(r);
			}
		},

		{
			header: 'Final Difference', dataIndex: 'Final Difference', width: 75, type: 'currency', css: 'BORDER-RIGHT: #c0c0c0 1px solid;',
			allViews: true,
			viewNameHeaders: [
				{name: 'Overlay', label: 'Final Overlay Difference'},
				{name: 'Fully Funded', label: 'Final Difference'},
				{name: 'Defensive Equity', label: 'Final Difference'}
			],
			tooltip: 'Target - (Exposure + Pending Exposure + Trade Impact) - Mispricing',
			renderer: function(v, p, r) {
				// Mispricing is included in target
				const target = Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r);
				const finalExp = this.calculateExposure(r);
				return TCG.renderAmount((target - finalExp), false, '0,000');
			},
			summaryTotalCondition: function(data, col) {
				return TCG.isFalse(data['matchingReplication']) && col.gridPanel && col.gridPanel.isSingleTradeCurrencyForGrandTotals();
			},
			summaryCalculation: function(v, r, field, data, col) {
				const target = Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r);
				const finalExp = col.calculateExposure(r);
				const d = target - finalExp;
				return v + d;
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000');
			},
			calculateExposure: function(r) {
				return this.gridPanel ? this.gridPanel.calculateRowFinalExposureConsideringCcyAdjustedTradeImpact(r)
					: Clifton.portfolio.target.run.trade.calculateFinalAggregateExposure(r);
			}
		},

		// % of Total Target
		{
			header: 'Target %', dataIndex: 'Target %', hidden: true, width: 75, type: 'percent', css: 'BORDER-RIGHT: #c0c0c0 1px solid;', summaryType: 'sum', numberFormat: '0,000.00',
			tooltip: 'Target Overlay as a % of the total <b>Total Target</b>',
			viewNameHeaders: [
				{name: 'Overlay', label: 'Target Overlay %'},
				{name: 'Fully Funded', label: 'Target %'},
				{name: 'Defensive Equity', label: 'Target %'}
			],
			useNull: true,
			renderer: function(v, p, r) {
				// When not matching, set the percent of total
				const targetPercent = this.calculateValue(r);
				if (targetPercent) {
					return TCG.renderAmount(targetPercent, false, '0,000.00 %');
				}
			},
			summaryTotalCondition: function(data, col) {
				return TCG.isFalse(data['matchingReplication']) && col.gridPanel && col.gridPanel.isSingleTradeCurrencyForGrandTotals();
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000.00 %');
			},
			summaryCalculation: function(v, r, field, data, col) {
				const targetPercent = col.calculateValue(r);
				if (targetPercent) {
					return v + targetPercent;
				}
				return v;
			},
			calculateValue: function(r) {
				if (TCG.isFalse(r.data['matchingReplication'])) {
					const totalOverlay = r.data['targetTotal'];
					if (totalOverlay && totalOverlay !== 0) {
						// Target As a Percent Of Total
						return Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r) / totalOverlay * 100;
					}
				}
				return void 0;
			}
		},

		{
			header: 'Final %', dataIndex: 'Final %', hidden: true, editable: false, width: 75, type: 'percent', css: 'BORDER-RIGHT: #c0c0c0 1px solid;', summaryType: 'sum', numberFormat: '0,000.00',
			viewNames: [],
			viewNameHeaders: [
				{name: 'Overlay', label: 'Final Overlay %'},
				{name: 'Fully Funded', label: 'Final %'},
				{name: 'Defensive Equity', label: 'Final %'}
			],
			tooltip: 'Final Exposure as a % of the total <b>Total Target</b>',
			useNull: true,
			renderer: function(v, p, r) {
				// When not matching, set the percent of total
				const finalPercent = this.calculateValue(r);
				if (finalPercent) {
					return TCG.renderAmount(finalPercent, false, '0,000.00 %');
				}
			},
			summaryTotalCondition: function(data, col) {
				return TCG.isFalse(data['matchingReplication']) && col.gridPanel && col.gridPanel.isSingleTradeCurrencyForGrandTotals();
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000.00 %');
			},
			summaryCalculation: function(v, r, field, data, col) {
				const finalPercent = col.calculateValue(r);
				if (finalPercent) {
					return v + finalPercent;
				}
				return v;
			},
			calculateValue: function(r) {
				const exposure = this.gridPanel ? this.gridPanel.calculateRowFinalExposureConsideringCcyAdjustedTradeImpact(r)
					: Clifton.portfolio.target.run.trade.calculateFinalAggregateExposure(r);
				if (TCG.isFalse(r.data['matchingReplication'])) {
					const totalOverlay = r.data['targetTotal'];
					if (totalOverlay && totalOverlay !== 0) {
						// Final As a Percent Of Total
						return exposure / totalOverlay * 100;
					}
				}
				return void 0;
			}
		},

		{
			header: 'Target - Final %', dataIndex: 'Target - Final %', hidden: true, width: 75, type: 'percent', css: 'BORDER-RIGHT: #c0c0c0 1px solid;', summaryType: 'sum', numberFormat: '0,000.00',
			viewNames: [],
			viewNameHeaders: [
				{name: 'Overlay', label: 'Target - Final Overlay %'},
				{name: 'Fully Funded', label: 'Target - Final %'},
				{name: 'Defensive Equity', label: 'Target - Final %'}
			],
			tooltip: 'Target % - Final %',
			useNull: true,
			renderer: function(v, p, r) {
				// When not matching, set the percent of total
				let diffPercent = 0;
				if (TCG.isFalse(r.data['matchingReplication'])) {
					const totalOverlay = r.data['targetTotal'];
					if (totalOverlay && totalOverlay !== 0) {
						// Target As a Percent Of Total
						const targetPercent = Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r) / totalOverlay * 100;

						// Final As a Percent Of Total
						const finalPercent = this.calculateExposure(r) / totalOverlay * 100;
						diffPercent = targetPercent - finalPercent;
					}
					return TCG.renderAmount(diffPercent, false, '0,000.00 %');
				}

			},
			summaryTotalCondition: function(data, col) {
				return TCG.isFalse(data['matchingReplication']) && col.gridPanel && col.gridPanel.isSingleTradeCurrencyForGrandTotals();
			},
			summaryRenderer: function(v) {
				return TCG.renderAmount(v, false, '0,000.00 %');
			},
			summaryCalculation: function(v, r, field, data, col) {
				if (TCG.isFalse(r.data['matchingReplication'])) {
					const totalOverlay = r.data['targetTotal'];
					if (totalOverlay && totalOverlay !== 0) {
						return v + ((Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(r) / totalOverlay * 100) - (col.calculateExposure(r) / totalOverlay * 100));
					}
				}
				return v;
			},
			calculateExposure: function(r) {
				return this.gridPanel ? this.gridPanel.calculateRowFinalExposureConsideringCcyAdjustedTradeImpact(r)
					: Clifton.portfolio.target.run.trade.calculateFinalAggregateExposure(r);
			}
		}
	],

	isSingleTradeCurrencyForGrandTotals: function() {
		return this.tradeCurrencies && this.tradeCurrencies.length === 1;
	},

	isCurrencyViewBaseCurrency: function() {
		const currencyType = TCG.getChildByName(this.getTopToolbar(), 'tradingCurrencyType').getValue();
		return currencyType === 'DEFAULT_CURRENCY';
	},

	/**
	 * Calculates the final exposure of a row considering pending and entered trade amount.
	 * The entered trade amount is calculated based on the selected currency view.
	 */
	calculateRowFinalExposureConsideringCcyAdjustedTradeImpact: function(row, tradeValueOverride) {
		let buySellFactor = TCG.isBlank(tradeValueOverride) ? row.data['tradeValue'] : tradeValueOverride;
		if (row.data['security.instrument.hierarchy.investmentType.name'] === 'Forwards' && !this.isCurrencyViewBaseCurrency()) {
			buySellFactor = buySellFactor / row.data['tradeValue'];
		}
		return Clifton.portfolio.target.run.trade.calculateFinalAggregateExposure(row, false, tradeValueOverride, buySellFactor);
	}

}, Clifton.portfolio.run.trade.BaseRunTradeDetailGrid);


/***********************************************************************
 * UTILITY METHODS
 * *********************************************************************/

/**
 * Here for re-use throughout Trade Creation screen to calculate Final Exposure for a given row
 */
Clifton.portfolio.target.run.trade.calculateFinalAggregateExposure = function(row, currentContractsOnly, tradeValueOverride, buySellValueOverride) {
	let curExp = 0;
	if (row.data['currencyCurrentTotalBaseAmount']) {
		curExp += row.data['currencyCurrentTotalBaseAmount'];
	}
	if (row.data['currencyPendingTotalBaseAmount']) {
		curExp += row.data['currencyPendingTotalBaseAmount'];
	}
	const tradeValue = TCG.isNotBlank(tradeValueOverride) ? tradeValueOverride : row.data['tradeValue'];
	let currentExposure = Clifton.portfolio.run.trade.calculateExposure(row, row.data['currentContracts'], tradeValue, curExp, row.data['totalAdditionalExposure']);
	if (TCG.isTrue(currentContractsOnly)) {
		return currentExposure;
	}
	currentExposure += Clifton.portfolio.run.trade.calculateExposure(row, row.data['pendingContracts'], tradeValue);
	const buySellContracts = row.data['buyContracts'] - row.data['sellContracts'];
	const tradeContractValue = TCG.isNotNull(buySellValueOverride) ? buySellValueOverride : tradeValue;
	currentExposure += Clifton.portfolio.run.trade.calculateExposure(row, buySellContracts, tradeContractValue);
	return currentExposure;
};


Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget = function(row, excludeMispricing) {
	let current = row.data['targetExposure'];
	let curTrading = 0;
	if (row.data['currencyCurrentDenominationBaseAmount']) {
		curTrading += row.data['currencyCurrentDenominationBaseAmount'];
	}
	if (row.data['currencyPendingDenominationBaseAmount']) {
		curTrading += row.data['currencyPendingDenominationBaseAmount'];
	}
	current = current + curTrading;

	if (excludeMispricing !== true) {
		const mispricing = row.data['mispricingTradeValue'];
		if (mispricing && mispricing !== 0) {
			current = current - mispricing;
		}
	}

	const ta = row.data['targetAdjustments'];
	let adjustments = 0;
	if (ta) {
		for (let i = 0; i < ta.length; i++) {
			const adj = ta[i].adjustment;
			if (adj !== 0) {
				adjustments = adjustments + adj;
			}
		}
	}

	const valueAdjustmentFactor = row.data['tradeValue'] / row.data['value'];
	return (current + adjustments) * valueAdjustmentFactor;
};

Clifton.portfolio.target.run.trade.renderAggregateTarget = function(columnMetadata, record) {
	const valueAdjustmentFactor = record.data['tradeValue'] / record.data['value'];
	const current = record.data['targetExposure'] * valueAdjustmentFactor;
	const tradingCcyAdj = record.data['currencyDenominationBaseAmount'] * valueAdjustmentFactor;

	let showAdjusted = false;
	let qtip = '<table><tr><td>Original Target:</td><td>&nbsp;</td><td style="text-align: right;">' + TCG.renderAmount(current, false, '0,000') + '</td></tr>';

	if (tradingCcyAdj !== 0) {
		showAdjusted = true;
		qtip += '<tr><td>Trading CCY:</td><td>&nbsp;</td><td style="text-align: right;">' + TCG.renderAmount(tradingCcyAdj, true, '0,000') + '</td></tr>';
	}

	let adjustments = 0;
	const ta = record.data['targetAdjustments'];
	if (ta) {
		for (let i = 0; i < ta.length; i++) {
			const adj = ta[i].adjustment;
			if (adj !== 0) {
				adjustments = adjustments + adj;
				showAdjusted = true;
				qtip += '<tr><td>' + ta[i].label + '</td><td style="text-align: right;">' + TCG.renderAmount(ta[i].adjustment) + '</td></tr>';
			}
		}
		if (adjustments !== 0) {
			adjustments = adjustments * valueAdjustmentFactor;
			qtip += '<tr><td>&nbsp;<td>&nbsp;</td></td><td style="text-align: right;">' + TCG.renderAmount(adjustments, true, '0,000') + '</td></tr>';
		}
	}
	let finalTarget = current + adjustments;
	if (tradingCcyAdj !== 0) {
		finalTarget = finalTarget + tradingCcyAdj;
	}

	if (TCG.isTrue(showAdjusted)) {
		columnMetadata.css = 'amountAdjusted';

		qtip += '<tr><td>&nbsp;</td><td>&nbsp;</td><td><hr/></td></tr>';
		qtip += '<tr><td><b>Total:</b></td><td>&nbsp;</td><td style="text-align: right;"><b>' + TCG.renderAmount(finalTarget, true, '0,000') + '</b></td></tr>';
		qtip += '</table>';

		columnMetadata.attr = 'qtip=\'' + qtip + '\'';
	}
	else if (finalTarget < 0) {
		columnMetadata.css = 'amountNegative';
	}
	record.data['Target'] = finalTarget;
	return Ext.util.Format.number(finalTarget, '0,000');
};

Clifton.portfolio.target.run.trade.copyTradesToAggregateTradeEntryDisabledRows = function(grid, repId, entryDisabled, f, v, silent) {
	const thisRow = Clifton.portfolio.run.trade.findRow(grid, repId);

	// Get total across all where manual entry is allowed
	let total = 0;
	if (TCG.isFalse(entryDisabled)) {
		total = v;
	}

	grid.getStore().each(row => {
		// skip this row since we have the latest value in v
		if (row.data['id'] !== repId && row.data['security.id'] === thisRow.data['security.id']) {
			if (TCG.isFalse(row.data['tradeEntryDisabled'])) {
				total = total + row.get(f);
			}
		}
	}, grid);

	// Copy total across all to where manual entry is not allowed and contracts are duplicated
	grid.getStore().each(row => {
		if (row.data['security.id'] === thisRow.data['security.id']) {
			if (TCG.isTrue(row.data['tradeEntryDisabled'])) {
				row.set(f, total);
				Clifton.portfolio.target.run.trade.updateAggregateTargetsCausedByRow(grid, row, 'ACTUAL');
				Clifton.portfolio.target.run.trade.updateAggregateTargetsCausedByRow(grid, row, 'TRADE');
			}
			else if (row.data['id'] === repId) {
				Clifton.portfolio.target.run.trade.updateAggregateTargetsCausedByRow(grid, row, 'ACTUAL');
				Clifton.portfolio.target.run.trade.updateAggregateTargetsCausedByRow(grid, row, 'TRADE');
			}
		}
	}, grid);

	if (!TCG.isTrue(silent)) {
		Clifton.portfolio.target.run.trade.refreshGridView(grid);
	}
};

Clifton.portfolio.target.run.trade.refreshGridView = function(grid) {
	// Refresh Grid So Updates Are Visible
	grid.getView().refresh(false);
	Clifton.portfolio.target.run.trade.setAggregateTradingDisabledMessage(grid);
	grid.markModified();
};

Clifton.portfolio.target.run.trade.setAggregateTradingDisabledMessage = function(grid) {
	const targetIds = [];
	grid.getStore().each(r => {
		if (TCG.isTrue(r.data['tradeEntryDisabled'])) {
			if (targetIds.indexOf(r.id) === -1) {
				const obj = Ext.fly(r['_groupId'] + '-td');
				if (TCG.isNotNull(obj)) {
					targetIds.push(r.id);
					obj.update('<span ext:qtip="Trading is Disabled on this Target" class="amountNegative"><span class="flag-red" style="padding-right: 10px;" >&nbsp;&nbsp;</span>&nbsp;&nbsp;[ READ ONLY ]</span>');
				}
			}
		}
	}, grid);
};

Clifton.portfolio.target.run.trade.updateAggregateTargets = function(grid) {
	const f = TCG.getParentFormPanel(grid).getForm();
	if (!f.formValues) {
		return;
	}

	// Don't load for closed runs
	const st = TCG.getValue('workflowStatus.name', f.formValues);
	if (st === 'Closed') {
		return;
	}

	let dependencies = grid.targetDependencyMap;
	// If not defined - needs to be loaded
	if (!dependencies) {
		const runId = TCG.getValue('id', f.formValues);
		if (runId) {
			// TODO
			dependencies = TCG.data.getData('productOverlayAssetClassReplicationTradeTargetDependencyList.json?runId=' + runId, grid);
			grid.targetDependencyMap = dependencies;
		}
	}

	if (!dependencies) {
		return;
	}

	// Go through each row - see if it is a cause for anything based on actuals first
	// Also call to update copyTradesToTradeEntryDisabledRows if buy or sell Contracts are populated and trade entry is not disabled on the row
	grid.getStore().each(record => Clifton.portfolio.target.run.trade.updateAggregateTargetsCausedByRow(grid, record, 'ACTUAL'), grid);

	// Then update based on any Trades/Pending Trades
	grid.getStore().each(record => Clifton.portfolio.target.run.trade.updateAggregateTargetsCausedByRow(grid, record, 'TRADE'), grid);

	// Then update targets based on matching M2M Adjustments that need to be applied to to non-matching counterparts first
	// Only called from here once since price change is reflected on load and doesn't change based on trades entered
	grid.getStore().each(record => Clifton.portfolio.target.run.trade.updateAggregateTargetsCausedByRow(grid, record, 'M2M'), grid);

	// Then check based on targets
	grid.getStore().each(record => Clifton.portfolio.target.run.trade.updateAggregateTargetsCausedByRow(grid, record, 'TARGET'), grid);

	// Refresh Grid So Updates Are Visible
	grid.getView().refresh(false);
	Clifton.portfolio.target.run.trade.setAggregateTradingDisabledMessage(grid);
};

Clifton.portfolio.target.run.trade.updateAggregateTargetsCausedByRow = function(grid, causeRow, triggeredBy) {
	if (causeRow) {
		const dependencies = grid.targetDependencyMap;
		if (!dependencies) {
			return;
		}

		// calc difference once
		let diff;
		switch (triggeredBy) {
			case 'TARGET': {
				diff = Clifton.portfolio.target.run.trade.calculateFinalAggregateTarget(causeRow) - causeRow.data['targetExposure'];
				break;
			}
			case 'TRADE': {
				// Current - Actual + pending + buys - sells
				let tradeContracts = causeRow.data['currentContracts'] - causeRow.data['actualContracts'];
				tradeContracts = tradeContracts + causeRow.data['pendingContracts'] + causeRow.data['buyContracts'] - causeRow.data['sellContracts'];
				let curExp = 0;
				if (causeRow.data['currencyPendingTotalBaseAmount']) {
					curExp += causeRow.data['currencyPendingTotalBaseAmount'];
				}
				diff = Clifton.portfolio.run.trade.calculateExposure(causeRow, tradeContracts, causeRow.data['tradeValue'], curExp, causeRow.data['totalAdditionalExposure']);
				break;
			}
			default: { // ACTUAL
				diff = Clifton.portfolio.target.run.trade.calculateFinalAggregateExposure(causeRow) - Clifton.portfolio.run.trade.calculateExposure(causeRow, causeRow.data['actualContracts'], causeRow.data['value']);
			}
		}

		if (!diff) {
			diff = 0;
		}

		for (let i = 0; i < dependencies.length; i++) {
			const dep = dependencies[i];
			const depType = dep.dependencyType;

			if (dep.causeOverlayReplicationId === causeRow.data['id']) {

				if ((depType === 'MATCHING_TARGET' || depType === 'MATCHING_ACTUAL') && !TCG.isTrue(causeRow['hasMatchingReplication'])) {
					causeRow['hasMatchingReplication'] = true;
				}

				let processDep = false;
				if (triggeredBy === dep.triggeredBy) {
					processDep = true;
				}

				if (TCG.isTrue(processDep)) {
					const dependRow = Clifton.portfolio.run.trade.findRow(grid, dep.dependentOverlayReplicationId);
					if (dependRow) {
						// calc % of difference for each dependency
						let depApply = dep.allocationPercentage * diff;
						if (TCG.isTrue(dep.negateAllocation)) {
							depApply = depApply * -1;
						}
						depApply = depApply / 100;

						let tgs = dependRow.data['targetAdjustments'];
						if (!tgs) {
							tgs = [];
						}
						let found = false;
						for (let j = 0; j < tgs.length; j++) {
							if (tgs[j].causeId === dep.causeOverlayReplicationId) {
								tgs[j].adjustment = depApply;
								found = true;
								break;
							}
						}
						if (!found) {
							tgs.push({causeId: dep.causeOverlayReplicationId, label: dep.label, adjustment: depApply});
						}

						dependRow.data['targetAdjustments'] = tgs;

						Clifton.portfolio.target.run.trade.updateAggregateTargetsCausedByRow(grid, dependRow, 'TARGET');
					}
				}
			}
		}
	}
};
