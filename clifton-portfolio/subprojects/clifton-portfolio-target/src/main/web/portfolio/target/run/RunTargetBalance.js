Clifton.portfolio.target.run.RunTargetBalance = {
	title: 'Target Balances',
	items: [{
		xtype: 'portfolio-target-balance-grid',
		name: 'portfolioTargetBalanceListByRun',
		groupField: 'target.label',
		limitRequestedProperties: false,
		getCustomLoadParams: function(firstLoad) {
			return {
				runId: this.getWindow().getMainForm().formValues.id
			};
		},
		getTopToolbarFilters: function(toolbar) {
			return undefined;
		},
		isPagingEnabled: function() {
			return false;
		},
		editor: {
			detailPageClass: 'Clifton.portfolio.target.balance.TargetBalanceWindow',
			defaultDataIsReal: true,
			drillDownOnly: true,
			openDetailPage: function(className, gridPanel, id, row, itemText) {
				const defaultData = row.json;
				defaultData.readOnly = true;
				defaultData.warningMessage = 'This Target Balance cannot be edited because it only exists in the context of ' + this.getWindow().title + '.';
				const cmpId = TCG.getComponentId(className, id);
				TCG.createComponent(className, {
					id: cmpId,
					defaultData: defaultData,
					defaultDataIsReal: true,
					openerCt: gridPanel,
					defaultIconCls: gridPanel.getWindow().iconCls
				});
			},
		}
	}]
};
