// not the actual window but a window selector:
//   - rollup asset classes get RollupManagerAccountWindow
//   - the rest get ManagerAccountWindow

Clifton.portfolio.target.manager.ManagerAccountWindowSelector = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'portfolioTargetRunManagerAccount.json',

	getClassName: function(config, entity) {
		if (entity && entity.rollupAssetClass) {
			return 'Clifton.portfolio.target.manager.RollupManagerAccountWindow';
		}
		return 'Clifton.portfolio.target.manager.ManagerAccountWindow';
	}
});
