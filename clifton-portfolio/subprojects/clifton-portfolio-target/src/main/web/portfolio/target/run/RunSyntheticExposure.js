Clifton.portfolio.target.run.RunSyntheticExposure = {

	title: 'Synthetic Exposure',
	items: [{
		name: 'portfolioTargetRunReplicationListByRun',
		xtype: 'gridpanel',
		appendStandardColumns: false,
		getLoadParams: function() {
			return {
				runId: this.getWindow().getMainFormId()
			};
		},
		topToolbarUpdateCountPrefix: '-',
		getTopToolbarFilters: function(toolbar) {
			const grid = this;
			return [
				{
					boxLabel: 'View Non Rollup Target Level&nbsp;', xtype: 'checkbox', name: 'nonRollupOnly',

					listeners: {
						check: function(field, checked) {
							if (checked === true) {
								grid.ds.groupField = 'labelLong';
								grid.groupField = 'labelLong';
							}
							else {
								grid.ds.groupField = 'topLevelLabel';
								grid.groupField = 'topLevelLabel';
							}
							TCG.getParentByClass(field, Ext.Panel).reload();
						}
					}
				}
			];
		},


		groupField: 'labelLong',
		groupTextTpl: '{values.group}',
		remoteSort: true,
		additionalPropertiesToRequest: 'id|replicationType|matchingReplication|cashExposure',
		viewNames: ['Default', 'Rebalance Triggers'],

		columns: [
			{header: 'Top Level Label', width: 150, dataIndex: 'topLevelLabel', hidden: true},
			{header: 'Label', width: 150, dataIndex: 'labelLong', hidden: true},
			{header: 'RollupAssetClass', width: 150, dataIndex: 'overlayAssetClass.topLevelLabel', hidden: true},
			{header: 'Cash Exposure', width: 15, dataIndex: 'cashExposure', hidden: true, type: 'boolean'},
			{header: 'Trading Account', width: 75, dataIndex: 'overlayAssetClass.tradingClientAccount.number', hidden: true},
			{header: 'AssetClass', width: 150, dataIndex: 'overlayAssetClass.label', hidden: true},
			{header: 'Replication', width: 140, dataIndex: 'replication.name', hidden: true},
			{header: 'Replication Type', width: 140, dataIndex: 'replicationType.name', hidden: true},
			{header: 'Matching Replication', width: 40, dataIndex: 'matchingReplication', hidden: true, type: 'boolean'},
			{header: 'OutsideOfRebalanceTrigger', width: 50, dataIndex: 'outsideOfRebalanceTrigger', type: 'boolean', hidden: true},

			{
				header: 'Security', width: 100, dataIndex: 'security.symbol',
				renderer: function(v, p, r) {
					if (r.data.cashExposure === true) {
						return 'Cash';
					}
					return v;
				}
			},
			{header: 'Security Description', width: 170, dataIndex: 'security.description'},

			// Targets
			{
				header: 'Target', width: 100, dataIndex: 'targetExposureAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Target Exposure Adjusted - Currency Total (if a currency replication)<br/><br/>Note: Matching Replications are excluded in the grand total calculation.',
				renderer: function(v, p, r) {
					let val = v;
					if (r.data.replicationType === 'CURRENCY') {
						val = val - r.data.currencyTotalBaseAmount;
					}
					return TCG.renderAdjustedAmount(val, v !== val, v, '0,000');
				},
				summaryTotalCondition: function(data) {
					return (data.matchingReplication === false);
				},
				summaryCalculation: function(v, r, field, data, col) {
					if (r.data.replicationType === 'CURRENCY') {
						return v + (r.data.targetExposureAdjusted - r.data.currencyTotalBaseAmount);
					}
					return v + (r.data.targetExposureAdjusted);
				},
				summaryRenderer: function(v) {
					return TCG.renderAmount(v, false, '0,000');
				}
			},
			{
				header: 'Currency Total', width: 100, dataIndex: 'currencyTotalBaseAmount', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hidden: true,
				tooltip: 'Actual Currency + Other Currency + Unrealized Currency Exposure'
			},
			{header: 'Actual Exposure (Original)', width: 85, dataIndex: 'actualExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hidden: true},
			{header: 'Additional Exposure', hidden: true, width: 100, dataIndex: 'additionalExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
			{header: 'Additional Overlay Exposure', hidden: true, width: 100, dataIndex: 'additionalOverlayExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
			{header: 'Virtual Exposure', hidden: true, width: 100, dataIndex: 'virtualExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},

			// Actual + Additional + Virtual
			{
				header: 'Exposure', width: 100, dataIndex: 'actualExposureAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Actual + Virtual Exposure + Additional Exposure <br /><br />Note: Matching Replications are excluded in the grand total calculation.',
				renderer: function(v, metaData, r) {
					if (v === undefined || v === '') {
						return '';
					}
					const actual = r.data['actualExposure'];
					const additional = r.data['additionalExposure'];
					const virtual = r.data['virtualExposure'];
					const additionalOverlay = r.data['additionalOverlayExposure'];

					let qtip = '';
					let showAdjusted = false;
					if (additional !== 0) {
						showAdjusted = true;
						qtip += '<tr><td>Additional Exposure:</td><td align="right">' + Ext.util.Format.number(additional, '0,000') + '</td></tr>';
					}
					if (additionalOverlay !== 0) {
						showAdjusted = true;
						qtip += '<tr><td>Additional Overlay Exposure:</td><td align="right">' + Ext.util.Format.number(additionalOverlay, '0,000') + '</td></tr>';
					}
					if (virtual !== 0) {
						showAdjusted = true;
						qtip += '<tr><td>Virtual Exposure:</td><td align="right">' + Ext.util.Format.number(virtual, '0,000') + '</td></tr>';
					}


					if (showAdjusted === true) {
						if (r.data['outsideOfRebalanceTrigger'] === true) {
							metaData.css = 'ruleViolation';
							metaData.attr = 'qtip=\'Overlay Exposure for this replication allocation is currently outside of defined rebalance bands.<br><table><tr><td>Overlay Exposure:</td><td align="right">' + Ext.util.Format.number(actual, '0,000') + '</td></tr>' + qtip + '</table>\'';
						}
						else {
							metaData.css = 'amountAdjusted';
							metaData.attr = 'qtip=\'<table><tr><td>Overlay Exposure:</td><td align="right">' + Ext.util.Format.number(actual, '0,000') + '</td></tr>' + qtip + '</table>\'';
						}
					}
					else if (r.data['outsideOfRebalanceTrigger'] === true) {
						metaData.css = 'ruleViolation';
						metaData.attr = 'qtip="Overlay Exposure for this replication allocation is currently outside of defined rebalance bands."';
					}
					return TCG.renderAmount(v, false, '0,000');
				},
				summaryTotalCondition: function(data) {
					return (data.matchingReplication === false);
				}
			},
			{
				header: 'Exposure Diff', width: 100, dataIndex: 'targetAdjustedLessActualExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				tooltip: 'Exposure Difference = Overlay Target - Overlay Exposure',
				renderer: function(v, p, r) {
					let val = v;
					if (r.data.replicationType === 'CURRENCY') {
						val = val - r.data.currencyTotalBaseAmount;
					}
					return TCG.renderAmount(val, false, '0,000');
				},
				summaryTotalCondition: function(data) {
					return (data.matchingReplication === false);
				},
				summaryCalculation: function(v, r, field, data, col) {
					if (r.data.replicationType === 'CURRENCY') {
						return v + (r.data.targetAdjustedLessActualExposure - r.data.currencyTotalBaseAmount);
					}
					return v + (r.data.targetAdjustedLessActualExposure);
				},
				summaryRenderer: function(v) {
					return TCG.renderAmount(v, false, '0,000');
				}
			},

			// Contracts
			{
				header: 'Target Contracts', width: 80, dataIndex: 'targetContractsAdjusted', type: 'currency', summaryType: 'sum', negativeInRed: true,
				viewNames: ['Default'],
				renderer: function(v, p, r) {
					if (r.data.cashExposure === true) {
						return null;
					}
					return TCG.renderAdjustedAmount(v, r.data['targetAdjusted'], r.data['targetContracts'], '0,000.00');
				},
				summaryCondition: function(data) {
					return (data.cashExposure === false);
				},
				summaryTotalCondition: function(data) {
					return (data.cashExposure === false);
				}
			},
			{
				header: 'Actual Contracts (Original)', width: 85, dataIndex: 'actualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hidden: true,
				renderer: function(v, p, r) {
					if (r.data.cashExposure === true) {
						return null;
					}
					return TCG.renderAmount(v, false, '0,000');
				},
				summaryCondition: function(data) {
					return (data.cashExposure === false);
				},
				summaryTotalCondition: function(data) {
					return (data.cashExposure === false);
				}
			},
			{
				header: 'Actual Contracts', width: 80, dataIndex: 'actualContractsAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
				viewNames: ['Default'],
				renderer: function(v, p, r) {
					if (r.data.cashExposure === true) {
						return null;
					}
					return TCG.renderAdjustedAmount(v, r.data['actualContracts'] !== v, r.data['actualContracts'], '0,000', 'Virtual Contracts');
				},
				summaryCondition: function(data) {
					return (data.cashExposure === false);
				},
				summaryTotalCondition: function(data) {
					return (data.cashExposure === false);
				}
			},
			{
				header: 'Difference', width: 75, dataIndex: 'actualTargetAdjustedContractDifference', type: 'currency', summaryType: 'sum', negativeInRed: true,
				viewNames: ['Default'],
				renderer: function(v, p, r) {
					if (r.data.cashExposure === true) {
						return null;
					}
					return TCG.renderAmount(v, false, '0,000');
				},
				summaryCondition: function(data) {
					return (data.cashExposure === false);
				},
				summaryTotalCondition: function(data) {
					return (data.cashExposure === false);
				}
			},

			// Amounts - Rebalance Triggers
			{header: 'Amount off Target', width: 80, hidden: true, dataIndex: 'amountOffTarget', type: 'currency', numberFormat: '0,000', summaryType: 'sum'},
			{header: 'Min Rebalance Trigger', width: 80, hidden: true, dataIndex: 'rebalanceTriggerMin', type: 'currency', numberFormat: '0,000', summaryType: 'sum'},
			{header: 'Max Rebalance Trigger', width: 80, hidden: true, dataIndex: 'rebalanceTriggerMax', type: 'currency', numberFormat: '0,000', summaryType: 'sum'},

			// Percentages - Rebalance Triggers
			{
				header: '% off Target', width: 70, hidden: true, dataIndex: 'percentOffTarget', type: 'percent', summaryType: 'percent', denominatorValueField: 'targetExposureAdjusted', numeratorValueField: 'amountOffTarget', negativeInRed: true,
				viewNames: ['Rebalance Triggers'],
				renderer: function(v, p, r) {
					let val = 0;
					if (r.data['targetExposureAdjusted'] === 0) {
						if (r.data['amountOffTarget'] > 0) {
							val = 100;
						}
						else if (r.data['amountOffTarget'] < 0) {
							val = -100;
						}
					}
					else {
						val = r.data['amountOffTarget'] / r.data['targetExposureAdjusted'] * 100;
					}
					return TCG.renderAmount(val, false, '0,000.00 %');
				}
			},
			{
				header: 'Min Rebalance Trigger %', width: 80, hidden: true, dataIndex: 'rebalanceTriggerMinPercent', type: 'currency', numberFormat: '0,000', summaryType: 'percent', denominatorValueField: 'targetExposureAdjusted', numeratorValueField: 'rebalanceTriggerMin', negativeInRed: true,
				viewNames: ['Rebalance Triggers'],
				renderer: function(v, p, r) {
					const val = r.data['rebalanceTriggerMin'] / r.data['targetExposureAdjusted'] * 100;
					return TCG.renderAmount(val, false, '0,000.00 %');
				}
			},

			{
				header: 'Max Rebalance Trigger %', width: 80, hidden: true, dataIndex: 'rebalanceTriggerMaxPercent', type: 'currency', numberFormat: '0,000', summaryType: 'percent', denominatorValueField: 'targetExposureAdjusted', numeratorValueField: 'rebalanceTriggerMax', negativeInRed: true,
				viewNames: ['Rebalance Triggers'],
				renderer: function(v, p, r) {
					const val = r.data['rebalanceTriggerMax'] / r.data['targetExposureAdjusted'] * 100;
					return TCG.renderAmount(val, false, '0,000.00 %');
				}
			}
		],
		plugins: {ptype: 'groupsummary'},
		editor: {
			detailPageClass: 'Clifton.portfolio.target.run.replication.ReplicationWindow',
			drillDownOnly: true
		}
	}]

};
