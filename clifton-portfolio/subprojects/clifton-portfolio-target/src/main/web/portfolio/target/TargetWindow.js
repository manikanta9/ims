Clifton.portfolio.target.TargetWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Portfolio Target',
	iconCls: 'account',
	width: 900,
	height: 650,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [
					{
						xtype: 'formpanel-custom-json-fields',
						url: 'portfolioTarget.json',
						columnGroupName: 'Portfolio Target Custom Fields',
						instructions: 'Define a portfolio target for a client account. The target include a replication for which security positions within the portfolio are applicable to the target.',
						items: [
							{fieldLabel: 'Name', name: 'name', xtype: 'textfield'},
							{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
							{
								xtype: 'panel',
								layout: 'column',
								defaults: {layout: 'form', columnWidth: .33, defaults: {anchor: '-20'}},
								items: [
									{
										items: [
											{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', value: new Date().format('m/d/Y')}
										]
									},
									{
										items: [
											{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', allowBlank: true}
										]
									},
									{
										columnWidth: .34,
										items: [
											{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield', anchor: '0'}
										]
									}
								]
							},
							{xtype: 'label', html: '<hr/>'},
							{
								fieldLabel: 'Parent Target', name: 'parent.label', hiddenName: 'parent.id', xtype: 'combo', detailPageClass: 'Clifton.portfolio.target.TargetWindow', url: 'portfolioTargetListFind.json', doNotClearIfRequiredSet: true, requiredFields: ['clientAccount.label'],
								beforequery: function(queryEvent) {
									const f = queryEvent.combo.getParentForm();
									queryEvent.combo.store.setBaseParam('clientAccountId', f.getFormValue('clientAccount.label', true, true));
								}
							},
							{
								fieldLabel: 'Matching Target', name: 'matchingTarget.label', hiddenName: 'matchingTarget.id', xtype: 'combo', detailPageClass: 'Clifton.portfolio.target.TargetWindow', url: 'portfolioTargetListFind.json', doNotClearIfRequiredSet: true, requiredFields: ['clientAccount.label'],
								beforequery: function(queryEvent) {
									const f = queryEvent.combo.getParentForm();
									queryEvent.combo.store.setBaseParam('clientAccountId', f.getFormValue('clientAccount.label', true, true));
								}
							},
							{boxLabel: 'Rollup target', name: 'rollup', xtype: 'checkbox', readOnly: true},
							{boxLabel: 'Private', name: 'privateTarget', xtype: 'checkbox', readOnly: true, qtip: 'Private Targets are used for portfolio processing, but are excluded from reports.'},
							{xtype: 'label', html: '<hr/>'},
							{fieldLabel: 'Client Account', name: 'clientAccount.label', hiddenName: 'clientAccount.id', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow', allowBlank: false, disableAddNewItem: true},
							{fieldLabel: 'Replication', name: 'replication.label', hiddenName: 'replication.id', xtype: 'combo', url: 'investmentReplicationListFind.json', detailPageClass: 'Clifton.investment.replication.ReplicationWindow'},
							{
								xtype: 'panel',
								layout: 'column',
								defaults: {layout: 'form', defaults: {anchor: '-20'}},
								items: [
									{
										columnWidth: .66,
										items: [
											{
												fieldLabel: 'Target Calculator', name: 'targetCalculatorBean.name', hiddenName: 'targetCalculatorBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Portfolio Target Calculator',
												detailPageClass: 'Clifton.system.bean.BeanWindow',
												getDefaultData: function() {
													return {type: {group: {name: 'Portfolio Target Calculator'}}};
												},
												qtip: 'Portfolio Target Calculators provide an updated Portfolio Target Balance for a Portfolio Target on a specified Balance Date. For example, Client Directed Balance Target Calculator uses a client directed Portfolio Target Balance and settled Portfolio Target Activity to calculate an updated balance value.'
											}
										]
									},
									{
										columnWidth: .34,
										items: [
											{
												fieldLabel: 'Target Percent', name: 'targetPercent', xtype: 'floatfield', value: '100', anchor: '0',
												qtip: 'The target multiplier to use for the calculated value. The default is 1, which means 100% of the defined target should be held. Using a negative number will negate the defined target (e.g. define a target for a long equity position of SPX held by the client, and write call options against this value).'
											}
										]
									}
								]
							},
							{xtype: 'label', html: '<hr/>'}
						]
					}
				]
			},
			{
				title: 'Manager Account Allocations',
				items: [{
					name: 'portfolioTargetInvestmentManagerAccountAllocationListFind',
					xtype: 'gridpanel',
					instructions: 'The following manager account assignments have been allocated to selected client account asset class.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'AssignmentID', width: 15, dataIndex: 'managerAccountAssignment.id', hidden: true},
						{header: 'Manager', width: 100, dataIndex: 'managerAccountAssignment.managerLabel'},
						{header: 'Account', width: 80, hidden: true, dataIndex: 'managerAccountAssignment.referenceOne.labelShort'},
						{header: 'Type', dataIndex: 'managerAccountAssignment.referenceOne.managerType', width: 40},
						{header: 'Overlay Type', dataIndex: 'managerAccountAssignment.cashOverlayType', width: 80},
						{header: 'Custom Cash Allocation', width: 35, dataIndex: 'customCashOverlay', type: 'boolean', hidden: true},
						{header: 'Assignment Cash Type', width: 50, dataIndex: 'managerAccountAssignment.cashType', hidden: true},
						{
							header: 'Cash Type', width: 50, dataIndex: 'cashType',
							renderer: function(v, metaData, r) {
								if (TCG.isNotBlank(v)) {
									return v;
								}
								else if (r.data['managerAccountAssignment.cashOverlayType'] !== 'CUSTOM' && r.data['managerAccountAssignment.cashOverlayType'] !== 'NONE') {
									return r.data['managerAccountAssignment.cashType'];
								}
								else {
									return null;
								}
							}
						},
						{header: 'Allocation %', width: 50, dataIndex: 'allocationPercent', type: 'percent', numberFormat: '0,000.0000000000'},
						{header: 'Private', width: 35, dataIndex: 'privateAssignment', type: 'boolean'},
						{header: 'Inactive', width: 35, dataIndex: 'managerAccountAssignment.inactive', type: 'boolean', filter: {searchFieldName: 'inactiveAssignment'}},
						{
							width: 25, fixed: true, dataIndex: 'managerAccountAssignment.referenceOne.id', filter: false, sortable: false,
							renderer: function(v, args, r) {
								return TCG.renderActionColumn('manager', '', 'Open Manager Account', 'OpenManagerAccount');
							}
						}
					],
					editor: {
						detailPageClass: 'Clifton.investment.manager.assignment.AssignmentWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.managerAccountAssignment.id;
						}
					},
					gridConfig: {
						listeners: {
							'rowclick': function(grid, rowIndex, evt) {
								if (TCG.isActionColumn(evt.target)) {
									const row = grid.getStore().getAt(rowIndex);
									const mgrId = row.json.managerAccountAssignment.referenceOne.id;
									const gridPanel = grid.ownerGridPanel;
									gridPanel.editor.openDetailPage('Clifton.investment.manager.AccountWindow', gridPanel, mgrId, row);
								}
							}
						}
					},
					getLoadParams: function() {
						const fv = this.getWindow().getMainForm().formValues;
						return {
							targetId: fv.id,
							investmentAccountId: fv.clientAccount.id
						};
					}
				}]
			},
			{
				title: 'Balances',
				xtype: 'portfolio-target-balance-grid',
				includeTargetNavigation: false,
				columnOverrides: [
					{dataIndex: 'target.label', hidden: true},
					{dataIndex: 'target.clientAccount.label', hidden: true},
					{dataIndex: 'target.replication.name', hidden: true}
				],
				getCustomLoadParams: function(firstLoad) {
					return {
						targetId: this.getWindow().getMainFormId()
					};
				},
				editor: {
					detailPageClass: 'Clifton.portfolio.target.balance.TargetBalanceWindow',
					deleteURL: 'portfolioTargetBalanceDelete.json',
					getDefaultData: function(grid, row) {
						return {
							target: grid.getWindow().getMainForm().formValues
						};
					}
				}
			},
			{
				title: 'Activity',
				xtype: 'portfolio-target-activity-grid',
				includeTargetNavigation: false,
				columnOverrides: [
					{dataIndex: 'target.label', hidden: true},
					{dataIndex: 'target.clientAccount.label', hidden: true},
					{dataIndex: 'target.replication.name', hidden: true}
				],
				getCustomLoadParams: function(firstLoad) {
					return {
						targetId: this.getWindow().getMainFormId()
					};
				},
				editor: {
					detailPageClass: 'Clifton.portfolio.target.activity.TargetActivityWindow',
					deleteURL: 'portfolioTargetActivityDelete.json',
					getDefaultData: function(grid, row) {
						return {
							target: this.getWindow().getMainForm().formValues
						};
					}
				}
			}
		]
	}
	]
});
