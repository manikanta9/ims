Clifton.portfolio.target.run.trade.RunTradeExposureRecapFieldSet = {
	xtype: 'fieldset',
	title: 'Exposure Recap',
	labelWidth: 140,
	listeners: {
		collapse: function(p) {
			p.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		},
		expand: function(p) {
			p.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			}).fireEvent('resize');
		}
	},
	items: [
		{xtype: 'label', html: 'Target and exposure summary including pending activity. All values are in the client account base currency.'},
		{
			xtype: 'formtable',
			columns: 3,
			defaults: {
				xtype: 'currencyfield',
				decimalPrecision: 2,
				readOnly: true,
				submitValue: false,
				width: '95%'
			},
			items: [
				{cellWidth: '120px', xtype: 'label'},
				{html: '$', xtype: 'label', style: 'text-align: right'},
				{html: '%', qtip: '% of TPV', xtype: 'label', style: 'text-align: right'},

				{html: 'Target Total:', xtype: 'label', qtip: 'Total Target across all defined Portfolio Targets'},
				{name: 'overlayTargetTotal', decimalPrecision: 0},
				{name: 'overlayTargetTotalPercent'},

				{html: 'Current Exposure:', xtype: 'label', qtip: 'Sum of Exposure: actual positions booked to General Ledger'},
				{name: 'netExposure', decimalPrecision: 0},
				{name: 'netExposurePercent'},

				{html: '<hr/>', xtype: 'label', colspan: 3, width: '99%'},

				{html: 'Pending Exposure:', xtype: 'label', qtip: 'Exposure from Pending Contracts as well as Trade Impact: not in General Ledger'},
				{name: 'pendingExposure', decimalPrecision: 0},
				{name: 'pendingExposurePercent'},

				{html: '<hr/>', xtype: 'label', colspan: 3, width: '99%'},

				{html: 'Final Exposure:', xtype: 'label', qtip: 'Final Exposure = Current Exposure + Pending Exposure'},
				{name: 'finalExposure', decimalPrecision: 0},
				{name: 'finalExposurePercent'}
			]
		}
	]
};
