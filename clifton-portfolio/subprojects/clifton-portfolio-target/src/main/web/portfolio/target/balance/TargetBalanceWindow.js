Clifton.portfolio.target.balance.TargetBalanceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Portfolio Target Balance',
	iconCls: 'account',
	width: 900,
	height: 600,

	items: [
		{
			xtype: 'formpanel',
			url: 'portfolioTargetBalance.json',
			defaults: {anchor: '-20'},

			initComponent: function() {
				const formpanel = this;
				const defaultDataReadOnly = TCG.getValue('readOnly', formpanel.getWindow().defaultData);
				formpanel.readOnly = defaultDataReadOnly;
				TCG.form.FormPanel.prototype.initComponent.call(this, arguments);
			},

			listeners: {
				afterload: function(formpanel) {
					const form = formpanel.getForm();
					const valueField = form.findField('valueField');
					valueField.setFieldLabel(TCG.isTrue(formpanel.getFormValue('quantityValue')) ? 'Quantity' : 'Notional');



					const defaultData = formpanel.getWindow().defaultData;
					const defaultDataReadOnly = TCG.getValue('readOnly', defaultData);
					if (defaultDataReadOnly) {
						TCG.getChildByName(formpanel, 'balanceActivityFormGrid').getColumnModel().config.forEach(conf => {
							conf.editable = false;
							conf.readOnly = true;
						});
					}
					if (defaultData && defaultData.warningMessage) {
						return formpanel.updateWarningMessage(defaultData.warningMessage);
					}
				}
			},

			items: [
				{
					fieldLabel: 'Target', name: 'target.label', xtype: 'combo', hiddenName: 'target.id', url: 'portfolioTargetListFind.json', displayField: 'label', requestedProps: 'clientAccount.id|clientAccount.label|replication.label|replication.id', detailPageClass: 'Clifton.portfolio.target.TargetWindow',
					selectHandler: function(combo, record, index) {
						if (record) {
							const formPanel = TCG.getParentFormPanel(combo);
							const clientAccount = TCG.getValue('clientAccount', record.json);
							if (clientAccount) {
								formPanel.setFormValue('target.clientAccount.label', clientAccount.label);
								formPanel.setFormValue('target.clientAccount.id', clientAccount.id);
							}
							const replication = TCG.getValue('replication', record.json);
							if (replication) {
								formPanel.setFormValue('target.replication.label', replication.label);
								formPanel.setFormValue('target.replication.id', replication.id);
							}
						}
					}
				},
				{fieldLabel: 'Client Account', name: 'target.clientAccount.label', detailIdField: 'target.clientAccount.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow'},
				{fieldLabel: 'Replication', name: 'target.replication.label', detailIdField: 'target.replication.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.replication.ReplicationWindow'},
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {layout: 'form', columnWidth: .5, defaults: {xtype: 'datefield', anchor: '-20'}},
					items: [
						{
							items: [
								{fieldLabel: 'Start Date', name: 'startDate', value: new Date().format('m/d/Y')}
							]
						},
						{
							items: [
								{fieldLabel: 'End Date', name: 'endDate'}
							]
						}
					]
				},
				{
					fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
					defaults: {
						xtype: 'displayfield',
						align: 'right',
						flex: 1
					},
					items: [
						{value: 'Original Value'},
						{value: 'Adjusted Value', tooltip: 'Original value with activity applied excluding Market On Close activity'},
						{value: 'MOC Value', tooltip: 'Market On Close value. Adjusted value with MOC adjustments included.'}
					]
				},
				{
					fieldLabel: 'Value', xtype: 'compositefield', name: 'valueField', submitValue: false,
					defaults: {
						xtype: 'currencyfield',
						flex: 1
					},
					items: [
						{name: 'value', allowBlank: false},
						{name: 'adjustedValue', submitValue: false, disabled: true},
						{name: 'marketOnCloseValue', submitValue: false, disabled: true}
					]
				},
				{fieldLabel: 'Description', name: 'targetBalanceDescription', xtype: 'textarea', height: 75},
				{boxLabel: 'Value is Quantity (Unchecked is Notional)', name: 'valueQuantity', xtype: 'checkbox'},
				{
					xtype: 'formgrid',
					title: 'Balance Activity',
					name: 'balanceActivityFormGrid',
					storeRoot: 'targetActivityList',
					dtoClass: 'com.clifton.portfolio.target.activity.PortfolioTargetActivity',
					viewConfig: {forceFit: true},

					initComponent: function() {
						const defaultDataReadOnly = TCG.getValue('readOnly', this.getWindow().defaultData);
						this.setReadOnly(defaultDataReadOnly);
						if (defaultDataReadOnly) {
							this.viewConfig = {markDirty: false, forceFit: true};
							this.addToolbarButtons = undefined;
						}
						TCG.grid.FormGridPanel.prototype.initComponent.apply(this, arguments);
					},

					addToolbarRemoveButton: toolBar => false,

					addToolbarButtons: function(toolBar, grid) {
						toolBar.add({
							text: 'Edit',
							tooltip: 'Opens the selected row in the activity window',
							iconCls: 'pencil',
							handler: function() {
								const selection = grid.getSelectionModel().selection;
								if (selection) {
									const record = selection.record;
									const params = {
										openerCt: grid
									};
									if (record.id && record.id > 0) {
										params['id'] = record.id;
										params['params'] = {id: record.id};
										TCG.createComponent('Clifton.portfolio.target.activity.TargetActivityWindow', params);
									}
									else {
										TCG.showError('Record has not been saved and cannot be edited');
									}
								}
								else {
									TCG.showError('Please select a row to edit');
								}
							}
						});
					},

					columnsConfig: [
						{header: 'ID', dataIndex: 'id', type: 'int', hidden: true},
						{
							header: 'Type', width: 120, dataIndex: 'type.name', idDataIndex: 'type.id',
							editor: {xtype: 'combo', url: 'portfolioTargetActivityTypeListFind.json', displayField: 'label'}
						},
						{header: 'Note', dataIndex: 'note', width: 250, editor: {xtype: 'textfield'}},
						{header: 'Value', dataIndex: 'value', width: 110, type: 'currency', editor: {xtype: 'currencyfield'}},
						{header: 'Security', width: 100, dataIndex: 'security.symbol', idDataIndex: 'security.id', editor: {xtype: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'}},
						{header: 'Cause Table', width: 110, dataIndex: 'causeSystemTable.label', idDataIndex: 'causeSystemTable.id', editor: {xtype: 'combo', url: 'systemTableListFind.json', displayField: 'labelExpanded'}},
						{header: 'Cause FK', width: 110, dataIndex: 'causeFKFieldId', type: 'int', editor: {xtype: 'integerfield'}},
						{header: 'Start Date', width: 100, dataIndex: 'startDate', editor: {xtype: 'datefield'}},
						{header: 'End Date', width: 100, dataIndex: 'endDate', editor: {xtype: 'datefield'}},
						{header: 'Quantity', width: 90, dataIndex: 'valueQuantity', xtype: 'checkcolumn', tooltip: 'Check this box to indicate if the activity value is a quantity of a security instead of a notional value'},
						{header: 'Deleted', width: 90, dataIndex: 'deleted', xtype: 'checkcolumn', tooltip: 'Activity cannot be deleted for historical tracking. Checking this box will disable an activity entry from being applied.'}
					],

					getNewRowDefaults: function() {
						return {startDate: new Date().format('m/d/Y')};
					}
				}
			]
		}
	]
});
