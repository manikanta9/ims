Clifton.portfolio.target.upload.TargetUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'portfolioTargetUploadWindow',
	title: 'Portfolio Target Import',
	iconCls: 'import',
	height: 400,
	hideOKButton: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Portfolio Target Uploads allow you to import targets from files directly into the system. Targets are unique by name and client account for an active date range.',
		labelWidth: 150,

		addToolbarButtons: function(toolbar, formPanel) {
			toolbar.add(
					{
						text: 'Simple Sample File',
						iconCls: 'excel',
						tooltip: 'Download Simple Excel Sample File',
						handler: function() {
							TCG.openFile('portfolio/target/upload/SimplePortfolioTargetUploadSample.xls');
						}
					}
			);
		},

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{
				fieldLabel: 'Overlapping Target Option', xtype: 'combo', name: 'overlappingTargetOption', displayField: 'value', valueField: 'value', mode: 'local',
				tooltip: 'Indicate how the system should handle changes to existing portfolio targets, specifically when the target percent is modified. Defaults to Error.',
				store: new Ext.data.ArrayStore({
					fields: ['name', 'value', 'description'],
					data: [['Replace', 'REPLACE', 'End the existing target and start a new target with the same name and updated values.'], ['Overwrite', 'OVERWRITE', 'Keep the existing target, but just change the values.'], ['Error', 'ERROR', 'Generate an error if a target is modified or there are targets with overlapping start/end dates.']]
				})
			},
			{
				fieldLabel: 'Target Activity Action', xtype: 'combo', name: 'targetActivityAction', displayField: 'value', valueField: 'value', mode: 'local',
				tooltip: 'Indicate how the system should handle Portfolio Target Activity items when new Portfolio Targets are created. This option is only relevant if the Overlapping Target Option is Replace. Defaults to Ignore.',
				store: new Ext.data.ArrayStore({
					fields: ['name', 'value', 'description'],
					data: [['Ignore', 'IGNORE', 'The system will not do any additional processing for Target Activity. Existing Target Activity will be left pointing to their corresponding (now ended) target.'], ['Migrate', 'MIGRATE', 'Existing Target Activities will be modified to point to the newly created targets.'], ['Copy', 'COPY', 'Old Portfolio Target Activities will be ended and new ones will be created, with all values matching the previous. However, these new activities will point to the newly created targets.']]
				})
			},
			{
				fieldLabel: 'Copy Previous Values For Empty Fields', xtype: 'checkbox', name: 'copyPreviousValues', checked: true,
				tooltip: 'If this box is checked, any columns that are blank in the upload file will be copied from the previous target (if one exists).'
			},
			{
				xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
				items: [
					{name: 'message', xtype: 'textarea', readOnly: true, submitField: false}
				]
			}
		],

		getSaveURL: function() {
			return 'portfolioTargetUploadFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&requestedPropertiesToExcludeGlobally=portfolioTargetUploadCommand';
		}
	}]
});
