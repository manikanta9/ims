Clifton.portfolio.target.activity.TargetActivityTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Portfolio Target Balance',
	iconCls: 'account',
	width: 800,
	height: 400,

	items: [
		{
			xtype: 'formpanel',
			url: 'portfolioTargetActivityType.json',
			readOnly: true,

			items: [
				{fieldLabel: 'Type Name', name: 'name'},
				{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},
				{boxLabel: 'Is Security Required', name: 'securityRequired', xtype: 'checkbox'},
				{boxLabel: 'Is Note Required', name: 'noteRequired', xtype: 'checkbox'},
				{boxLabel: 'Is Cause Required', name: 'causeRequired', xtype: 'checkbox'},
				{boxLabel: 'Is Position', name: 'position', xtype: 'checkbox', tooltip: 'Flag to indicate if activity of this type is tied to a position or general adjustment'},
				{boxLabel: 'Is Market On Close (applied to MOC balance)', name: 'marketOnClose', xtype: 'checkbox'},
				{boxLabel: 'Is System Defined', name: 'systemDefined', xtype: 'checkbox'}
			]
		}
	]
});
