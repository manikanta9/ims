Clifton.portfolio.target.run.replication.ReplicationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Target Replication',
	iconCls: 'replicate',
	width: 900,
	height: 650,

	// Special Method so Replications are fully reloaded on Trade Creation Screen to get price overrides
	reloadOpener: function() {
		const win = this;
		if (win.openerCt && win.openerCt.fullReload) {
			win.openerCt.fullReload();
		}
		else if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload) {
			win.openerCt.reload();
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Replication Security Details',
				items: [
					{
						xtype: 'run-replication-formpanel',
						url: 'portfolioTargetRunReplication.json',
						warningTypeLabel: 'target',

						getRunProcessingTypeItems: function() {
							return [
								{name: 'portfolioRun.balanceDate', xtype: 'hidden', submitValue: false},
								{fieldLabel: 'Portfolio Run', name: 'portfolioRun.label', xtype: 'linkfield', detailIdField: 'portfolioRun.id', detailPageClass: 'Clifton.portfolio.run.RunWindow', submitValue: false, submitDetailField: false},
								{fieldLabel: 'Trading Client Account', name: 'target.clientAccount.label', xtype: 'linkfield', detailIdField: 'target.clientAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow', submitValue: false, submitDetailField: false},
								{fieldLabel: 'Target', name: 'target.labelExpanded', xtype: 'linkfield', detailIdField: 'target.id', detailPageClass: 'Clifton.portfolio.target.TargetWindow', submitValue: false}
							];
						}
					}
				]
			},

			{
				title: 'Pending Trades',
				items: [{xtype: 'run-replication-pending-trade-gridpanel'}]
			},

			{
				title: 'Pending Transfers',
				items: [{xtype: 'run-replication-pending-transfer-gridpanel'}]
			}
		]
	}]
});
