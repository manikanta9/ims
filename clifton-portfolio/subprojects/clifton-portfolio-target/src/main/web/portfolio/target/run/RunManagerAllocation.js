Clifton.portfolio.target.run.RunManagerAllocation = {
	title: 'Manager Allocation',

	items: [{
		name: 'portfolioTargetRunManagerAccountListFind',
		xtype: 'gridpanel',
		instructions: 'Selected client account has the following target allocation of manager account balances.',
		appendStandardColumns: false,
		viewNames: ['Default View', 'Grouped By Manager'],
		getLoadParams: function() {
			const nonRollupOnly = TCG.getChildByName(this.getTopToolbar(), 'nonRollupOnly').getValue();
			if (TCG.isTrue(nonRollupOnly)) {
				return {
					runId: this.getWindow().getMainForm().formValues.id,
					rollupTarget: false
				};
			}
			return {
				runId: this.getWindow().getMainForm().formValues.id,
				rootOnly: true
			};
		},
		topToolbarUpdateCountPrefix: '-',
		importTableName: [
			{table: 'InvestmentManagerAccountAllocation', label: 'Manager Allocations', importComponentName: 'Clifton.investment.manager.upload.AccountAllocationUploadWindow'}
		],
		getTopToolbarFilters: function(toolbar) {
			return [
				{
					boxLabel: 'View Non Rollup Target Level&nbsp;', xtype: 'checkbox', name: 'nonRollupOnly',

					listeners: {
						check: function(field) {
							TCG.getParentByClass(field, Ext.Panel).reload();
						}
					}
				}
			];
		},
		remoteSort: true,
		groupField: 'portfolioTarget.label',
		groupTextTpl: '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Managers" : "Manager"]})',
		isPagingEnabled: function() {
			return false;
		},
		pageSize: 1000,
		switchToViewBeforeReload: function(viewName) {
			this.grid.getStore().remoteGroup = true;
			let grpPropertyName = 'portfolioTarget.label';
			if (viewName === 'Grouped By Manager') {
				grpPropertyName = 'managerAccountAssignment.managerLabel';
			}
			this.grid.getStore().groupBy(grpPropertyName, false);
			this.ds.setDefaultSort(grpPropertyName, 'asc');

		},
		onAfterGrandTotalUpdateFieldValue: function(v, cf) {
			const grid = this.grid;
			const items = grid.store.data.items;
			if (cf.dataIndex === 'totalAllocation') {
				let i = 0;
				let row = items[i];

				let currentManager = '';
				let managerTotalAllocation = 0;
				let startRow = 0;
				while (row) {
					const managerTargetAllocation = row.data['totalAllocation'];

					// Then track the manager totals across asset classes
					const thisManager = row.data['managerAccountAssignment.managerLabel'];
					if (TCG.isBlank(currentManager)) {
						currentManager = thisManager;
					}

					if (currentManager === thisManager) {
						managerTotalAllocation += managerTargetAllocation;
					}

					// Get the next row
					i = i + 1;
					const nextRow = items[i];

					// If next row doesn't exist (i.e. last row, or next row is for a different manager)
					if (!nextRow || nextRow.data['managerAccountAssignment.managerLabel'] !== currentManager) {
						for (let updateRowIndex = startRow; updateRowIndex < i; updateRowIndex++) {
							if (items[updateRowIndex]) {
								items[updateRowIndex].data['managerTotal'] = managerTotalAllocation;
							}
						}
						if (nextRow) {
							// Reset totals for next manager assignment and change current manager
							currentManager = nextRow.data['managerAccountAssignment.managerLabel'];
							managerTotalAllocation = 0;
							startRow = i;
						}
					}
					// Move row to nextRow
					row = nextRow;
				}
			}
		},
		columns: [
			{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
			{header: 'Manager ID', width: 30, dataIndex: 'managerAccountAssignment.referenceOne.id', hidden: true},
			{header: 'Target', width: 140, dataIndex: 'portfolioTarget.label', hidden: true, viewNames: ['Grouped By Manager'], filter: {searchFieldName: 'portfolioTargetName'}, defaultSortColumn: true},
			{header: 'Manager', width: 170, dataIndex: 'managerAccountAssignment.managerLabel', viewNames: ['Default View'], filter: {searchFieldName: 'managerLabel', sortFieldName: 'managerLabelSort'}},
			{
				header: 'Allocation (%)', width: 75, hidden: true, dataIndex: 'totalAllocationPercent', hideGrandTotal: true, type: 'percent', viewNames: ['Grouped By Manager'], filter: false, sortable: false, summaryType: 'sum',
				renderer: function(value, metaData, row) {
					return TCG.renderAmount((row.data['totalAllocation'] / row.data['managerTotal']) * 100, false, '0,000.00', false, ' %');
				},
				summaryCalculation: function(v, row, field, data, col) {
					return v + ((row.data['totalAllocation'] / row.data['managerTotal']) * 100);
				},
				summaryRenderer: function(v) {
					if (TCG.isNotBlank(v)) {
						return TCG.renderAmount(v, false, '0,000.00', false, ' %');
					}
				}
			},
			{header: 'Account', width: 75, dataIndex: 'managerAccountAssignment.referenceOne.accountNumber', hidden: true, sortable: false, filter: false},
			{header: 'Custodian Account', width: 170, dataIndex: 'managerAccountAssignment.referenceOne.custodianAccount.number', hidden: true, sortable: false, filter: false},
			{header: 'Cash Type', width: 75, dataIndex: 'managerAccountAssignment.cashType', hidden: true, sortable: false, filter: false},
			{header: 'Cash Overlay Type', width: 75, dataIndex: 'managerAccountAssignment.cashOverlayType', hidden: true, sortable: false, filter: false},
			{header: 'Private', width: 75, dataIndex: 'privateManager', hidden: true, type: 'boolean'},

			{header: 'Securities', width: 100, dataIndex: 'securitiesAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', hidden: true, negativeInRed: true, sortable: false, filter: false},
			{header: 'Cash', width: 100, dataIndex: 'cashAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, allViews: true, sortable: false, filter: false},
			{header: 'Total Market Value', width: 100, dataIndex: 'totalAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, sortable: false, filter: false, hidden: true},

			{header: 'Previous Day Securities', width: 100, dataIndex: 'previousDaySecuritiesAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, sortable: false, filter: false},
			{header: 'Previous Day Cash', width: 100, dataIndex: 'previousDayCashAllocation', type: 'currency', hidden: true, summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, sortable: false, filter: false},
			{header: 'Previous Day Total', width: 100, dataIndex: 'previousDayTotalAllocation', type: 'currency', hidden: true, summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, sortable: false, filter: false},

			{header: 'Previous Month End Securities', width: 100, dataIndex: 'previousMonthEndSecuritiesAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, sortable: false, filter: false},
			{header: 'Previous Month End Cash', width: 100, dataIndex: 'previousMonthEndCashAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, sortable: false, filter: false},
			{header: 'Previous Month End Total', width: 100, dataIndex: 'previousMonthEndTotalAllocation', type: 'currency', hidden: true, summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, sortable: false, filter: false},

			{header: 'One Day % Change', width: 100, dataIndex: 'oneDayPercentChange', type: 'percent', summaryType: 'percentChange', previousValueField: 'previousDayTotalAllocation', currentValueField: 'totalAllocation', negativeInRed: true, allViews: true, sortable: false, filter: false},
			{header: 'MTD % Change', width: 100, dataIndex: 'monthPercentChange', type: 'percent', summaryType: 'percentChange', previousValueField: 'previousMonthEndTotalAllocation', currentValueField: 'totalAllocation', negativeInRed: true, allViews: true, sortable: false, filter: false},
			{header: 'Cash %', width: 65, dataIndex: 'cashAllocationPercent', type: 'percent', summaryType: 'percent', denominatorValueField: 'totalAllocation', numeratorValueField: 'cashAllocation', negativeInRed: true, allViews: true, sortable: false, filter: false}
		],
		plugins: {ptype: 'groupsummary'},
		editor: {
			detailPageClass: 'Clifton.portfolio.target.manager.ManagerAccountWindowSelector',
			drillDownOnly: true,

			// Used to Open the Manager Balance Window Directly, or the Manager Account - Balance History Tab
			openWindowFromContextMenu: function(grid, rowIndex, screen) {
				const gridPanel = this.getGridPanel();
				const row = grid.store.data.items[rowIndex];
				let clazz;
				let id;
				let defaultActiveTabName = undefined;

				// Manager Balance Window
				const mgrId = row.json.managerAccountAssignment.referenceOne.id;

				if (screen === 'MANAGER_BALANCE') {
					id = TCG.data.getData('portfolioTargetRunManagerAccount.json?id=' + row.json.id, gridPanel).balance.id;
					clazz = 'Clifton.investment.manager.BalanceWindow';
				}
				else if (screen === 'MANAGER_TRANSACTION_HISTORY') {
					id = mgrId;
					clazz = 'Clifton.investment.manager.AccountWindow';
					defaultActiveTabName = 'Transaction History';
				}
				else {
					id = mgrId;
					clazz = 'Clifton.investment.manager.AccountWindow';
					defaultActiveTabName = 'Balance History';
				}
				if (clazz) {
					this.openDetailPage(clazz, gridPanel, id, row, null, defaultActiveTabName);
				}
			}
		},
		listeners: {
			'grandtotalupdated': function(v, cf) {
				const grid = this;
				grid.onAfterGrandTotalUpdateFieldValue(v, cf);
			},
			afterrender: function(gridPanel) {
				const el = gridPanel.getEl();
				el.on('contextmenu', function(e, target) {
					const g = gridPanel.grid;
					g.contextRowIndex = g.view.findRowIndex(target);
					e.preventDefault();
					if (!g.drillDownMenu) {
						g.drillDownMenu = new Ext.menu.Menu({
							items: [
								{
									text: 'Open Manager Balance', iconCls: 'manager', handler: function() {
										gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'MANAGER_BALANCE');
									}
								},
								{
									text: 'Open Manager Balance History', iconCls: 'grid', handler: function() {
										gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'MANAGER_BALANCE_HISTORY');
									}
								},
								{
									text: 'Open Manager Transactions', iconCls: 'cash', handler: function() {
										gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'MANAGER_TRANSACTION_HISTORY');
									}
								}
							]
						});
					}
					TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
				}, gridPanel);
			}
		}
	}]
};
