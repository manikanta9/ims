// Use target replication grid for functions in replication grid
TCG.use('Clifton.portfolio.target.run.trade.RunTradeTargetReplicationGrid');

Clifton.portfolio.target.run.replication.AggregateReplicationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Target Aggregate Replication',
	iconCls: 'replicate',
	width: 900,
	height: 650,

	// Special Method so Replications are fully reloaded on Trade Creation Screen to get price overrides
	reloadOpener: function() {
		const win = this;
		if (win.openerCt && win.openerCt.fullReload) {
			win.openerCt.fullReload();
		}
		else if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload) {
			win.openerCt.reload();
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Replication Security Details',
			items: [{
				xtype: 'formpanel',
				loadValidation: false,
				labelWidth: 140,
				defaults: {anchor: '0'},
				url: 'portfolioAggregateRunTradeReplication.json',

				getWarningMessage: function(form) {
					let msg = undefined;
					if (!form.formValues) {
						return;
					}
					if (TCG.isTrue(form.formValues.tradeEntryDisabled)) {
						msg = 'Trade Entry is Disabled for this target.  All security changes or price entries must be made on a replication where trade entry is not disabled and it will be automatically updated here.';
					}
					return msg;
				},

				listeners: {
					afterload: function(panel) {
						// If trading is disabled - disable security, tradeSecurityPrice, and tradeUnderlyingSecurityPrice
						const f = panel.getForm();
						const ro = TCG.getValue('tradeEntryDisabled', f.formValues);
						if (TCG.isTrue(ro)) {
							// Get security.label field, tradeSecurityPrice field, & tradeUnderlyingSecurityPrice field & disable them
							let field = f.findField('security.label');
							field.setDisabled(true);
							field = f.findField('tradeSecurityPrice');
							field.setDisabled(true);
							field = f.findField('tradeUnderlyingSecurityPrice');
							field.setDisabled(true);
						}
						// Otherwise, if no underlying security, then no underlying security price
						else {
							const us = TCG.getValue('underlyingSecurity.label', f.formValues);
							if (TCG.isBlank(us)) {
								const underlying = TCG.getChildByName(this.ownerCt, 'underlyingSecurityFieldset');
								underlying.setVisible(false);
							}
						}

						if (TCG.getValue('replicationType.currency', f.formValues) !== true) {
							const ccy = TCG.getChildByName(this.ownerCt, 'currencyFieldset');
							ccy.setVisible(false);
						}

						// Replication Specific Fields - Remove if No Value
						panel.showHideField(f, 'interestRate');
						panel.showHideField(f, 'duration');
						panel.showHideField(f, 'syntheticAdjustmentFactor');
						panel.showHideField(f, 'indexRatio');
						panel.showHideField(f, 'delta');
						panel.showHideField(f, 'daysToMaturity');
						panel.showHideField(f, 'securityDirtyPrice');
					}
				},

				showHideField: function(form, fieldName) {
					const field = form.findField(fieldName);

					if (field) {
						const val = TCG.getValue(fieldName, form.formValues);
						if (TCG.isBlank(val)) {
							field.setVisible(false);
						}
						else {
							field.setVisible(true);
						}
					}
				},

				items: [
					{name: 'portfolioRun.balanceDate', xtype: 'hidden', submitValue: false},
					{fieldLabel: 'Portfolio Run', name: 'portfolioRun.label', xtype: 'linkfield', detailIdField: 'portfolioRun.id', detailPageClass: 'Clifton.portfolio.run.RunWindow', submitValue: false, submitDetailField: false},
					{fieldLabel: 'Trading Client Account', name: 'tradingClientAccount.label', xtype: 'linkfield', detailIdField: 'tradingClientAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow', submitValue: false, submitDetailField: false},

					{
						xtype: 'panel',
						layout: 'column',
						items: [{
							columnWidth: .48,
							items: [{
								xtype: 'fieldset',
								title: 'Security',
								items: [
									{name: 'tradeEntryDisabled', xtype: 'hidden'},
									{fieldLabel: 'Instrument', name: 'security.instrument.id', xtype: 'hidden'},
									{
										fieldLabel: 'Security', name: 'security.label', hiddenName: 'security.id', xtype: 'combo', disabled: false, allowBlank: false, displayField: 'label', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
										beforequery: function(queryEvent) {
											const f = queryEvent.combo.getParentForm().getForm();

											if (TCG.isTrue(TCG.getValue('tradeEntryDisabled', f.formValues))) {
												TCG.showError('Cannot change security if trade entry is disabled.  Please update this security on a replication where trade entry is not disabled and it will be automatically updated here.', 'Security Change');
												return false;
											}

											const actualContracts = TCG.getValue('actualContracts', f.formValues);
											if (actualContracts !== 0) {
												TCG.showError('Cannot change security if actual contracts are present.', 'Security Change');
												return false;
											}

											const securityGroupId = f.getFormValue('replicationAllocation.replicationSecurityGroup.id');

											queryEvent.combo.setBaseParam('active', true);

											// If there is no security group defined, just use the instrument from the existing security
											if (TCG.isBlank(securityGroupId)) {
												queryEvent.combo.setBaseParam('instrumentId', TCG.getValue('security.instrument.id', f.formValues));
											}
											// Otherwise use the security group (and instrument if defined on the allocation)
											else {
												const instrumentId = f.getFormValue('replicationAllocation.replicationInstrument.id');
												queryEvent.combo.setBaseParam('instrumentId', instrumentId);
												queryEvent.combo.setBaseParam('securityGroupId', securityGroupId);
											}
										},

										listeners: {
											'beforerender': function() {
												this.drillDownMenu = new Ext.menu.Menu({
													items: [
														{
															text: 'Open Detail Window', iconCls: 'info', handler: function() {
																this.createDetailClass(false);
															}, scope: this
														},
														{
															text: 'Add New Security For Instrument', iconCls: 'add', handler: function() {
																this.createDetailClass(true);
															}, scope: this
														},
														{
															text: 'Add New Security (Using Existing Security as Template)', iconCls: 'copy', handler: function() {
																this.createDetailClass(true, true);
															}, scope: this
														}
													]
												});
											}
										},

										createDetailClass: function(newInstance, useTemplate) {
											const fp = TCG.getParentFormPanel(this);
											const w = TCG.isNull(fp) ? {} : fp.getWindow();
											const className = this.getDetailPageClass(newInstance, useTemplate);
											let id = this.getValue();
											if (TCG.isBlank(id) || newInstance) {
												id = undefined;
											}
											const params = id ? {id: id} : undefined;
											const defaultData = this.getDefaultData(TCG.isNull(fp) ? {} : fp.getForm(), newInstance);
											const cmpId = TCG.getComponentId(className, id);
											TCG.createComponent(className, {
												modal: (TCG.isNull(id)),
												id: cmpId,
												defaultData: defaultData,
												params: params,
												openerCt: this, // for modal new item window will call reload() on close
												defaultIconCls: w.iconCls
											});
										},
										getDetailPageClass: function(newInstance, useTemplate) {
											if (newInstance && useTemplate) {
												return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
											}
											return this.detailPageClass;
										},
										getDefaultData: function(f) { // defaults client for "add new" drill down
											return {
												instrument: TCG.getValue('security.instrument', f.formValues),
												copyFromSecurityId: TCG.getValue('security.id', f.formValues),
												copyFromSecurityIdLabel: TCG.getValue('security.label', f.formValues)
											};
										}
									},
									{fieldLabel: 'Contract Value', name: 'value', xtype: 'floatfield', readOnly: true, submitValue: false},
									{fieldLabel: 'Price', name: 'securityPrice', xtype: 'floatfield', readOnly: true, submitValue: false},
									{fieldLabel: 'Trade Price', qtip: 'Price Override Used on Trade Creation Screen Only.  When using Live Prices feature, this price is used instead.', name: 'tradeSecurityPrice', xtype: 'floatfield'},
									{fieldLabel: 'Dirty Price', name: 'securityDirtyPrice', xtype: 'floatfield', readOnly: true, submitValue: false},
									{fieldLabel: 'Days To Maturity', name: 'daysToMaturity', xtype: 'numberfield', readOnly: true, submitValue: false},
									{fieldLabel: 'Interest Rate', name: 'interestRate', xtype: 'floatfield', readOnly: true, submitValue: false},
									{fieldLabel: 'Duration', name: 'duration', xtype: 'floatfield', readOnly: true, submitValue: false},
									{
										fieldLabel: 'Delta', name: 'delta', xtype: 'floatfield', readOnly: true, submitValue: false,
										qtip: 'Market Data Value for <i>Delta</i> field. <br><br><b>Special Adjustment for Options:</b>&nbsp;Delta is Negated If Call Option and Short Target or Put Option and Long Target'
									},
									{
										fieldLabel: 'Syn Adj Factor', name: 'syntheticAdjustmentFactor', xtype: 'floatfield', readOnly: true, submitValue: false,
										qtip: 'Formula: 1 / (1+((Days to Maturity/365)*Interest Rate))'
									},
									{fieldLabel: 'Factor', name: 'factor', xtype: 'floatfield', readOnly: true, submitValue: false, qtip: 'Current Factor Change event value for the security. If there are no events available the value will be undefined, which is equivalent to a value of 1.'},
									{
										fieldLabel: 'Index Ratio', name: 'indexRatio', xtype: 'floatfield', readOnly: true, submitValue: false,
										qtip: 'Index Ratio is a ratio between current and base CPI\'s and is used by inflation linked securities (TIPS, etc.).'
									},
									{fieldLabel: 'Exchange Rate', name: 'exchangeRate', xtype: 'floatfield', readOnly: true, submitValue: false}
								]
							},
								{
									xtype: 'fieldset',
									title: 'Underlying Security',
									name: 'underlyingSecurityFieldset',
									items: [
										{fieldLabel: 'Security', name: 'underlyingSecurity.label', xtype: 'linkfield', detailIdField: 'underlyingSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitValue: false, submitDetailField: false},
										{fieldLabel: 'Price', name: 'underlyingSecurityPrice', xtype: 'floatfield', readOnly: true, submitValue: false},
										{fieldLabel: 'Trade Price', qtip: 'Price Override Used on Trade Creation Screen Only.  When using Live Prices feature, this price is used instead.', name: 'tradeUnderlyingSecurityPrice', xtype: 'floatfield'}
									]
								},
								{
									xtype: 'fieldset',
									title: 'Currency',
									name: 'currencyFieldset',
									items: [
										// Physical Currency
										{fieldLabel: 'CCY (Local)', name: 'currencyActualLocalAmount', xtype: 'currencyfield', decimalPrecision: 0},
										{fieldLabel: 'Unrealized (Local)', name: 'currencyUnrealizedLocalAmount', xtype: 'currencyfield', decimalPrecision: 0, qtip: 'Unrealized Exposure is added when the CCY replication is a matching replication and ONLY includes exposure from positions in the same asset class.'},
										{fieldLabel: 'Total (Local)', name: 'currencyTotalActualLocalAmount', xtype: 'currencyfield', decimalPrecision: 0},

										{fieldLabel: 'FX Rate', name: 'currencyExchangeRate', xtype: 'floatfield'},

										{fieldLabel: 'Total (Base)', qtip: 'Physical Currency + Unrealized Currency Exposure in Account Base Currency', name: 'currencyTotalActualBaseAmount', xtype: 'currencyfield', decimalPrecision: 0},
										{fieldLabel: 'CCY Other', name: 'currencyOtherBaseAmount', xtype: 'currencyfield', decimalPrecision: 0, qtip: 'Currency Other applied to this contract in Account Base Currency'},
										{fieldLabel: 'CCY (Trading)', name: 'currencyDenominationBaseAmount', xtype: 'currencyfield', decimalPrecision: 0, qtip: 'Trading Currency applied to this contract in Account Base Currency.  Trading currency is applied differently than currency other when the underlying security equals the account base currency.  The trading currency of the security is then applied here and added to the target to get the net allocation.'}
									]
								}

							]
						},

							{columnWidth: .02, items: [{xtype: 'label', html: '&nbsp;'}]},

							{
								columnWidth: .50,
								items: [
									{
										xtype: 'fieldset',
										title: 'Target Allocations',
										items: [
											{
												fieldLabel: 'Allocation %', xtype: 'compositefield',
												defaults: {
													xtype: 'floatfield',
													flex: 1
												},
												items: [
													{name: 'allocationWeight', readOnly: true, submitValue: false}
												]
											},
											{
												fieldLabel: 'Target Amount', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'targetExposure', readOnly: true, submitValue: false}
												]
											},
											{
												fieldLabel: 'Target Contracts', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'targetContracts', readOnly: true, submitValue: false}
												]
											}
										]
									},

									{
										xtype: 'fieldset',
										title: 'Current Positions',
										items: [
											{
												fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
												defaults: {
													xtype: 'displayfield',
													style: 'text-align: right',
													flex: 1
												},
												items: [
													{value: 'Contracts', submitValue: false},
													{value: 'Exposure', submitValue: false}
												]
											},
											{
												fieldLabel: 'Current', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'currentContracts', readOnly: true, submitValue: false},
													{name: 'currentExposure', readOnly: true, submitValue: false}
												]
											},
											{
												fieldLabel: 'Pending', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'pendingContracts', readOnly: true, submitValue: false},
													{name: 'pendingExposure', readOnly: true, submitValue: false}
												]
											},
											{
												fieldLabel: 'Difference', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'targetAdjustedContractDifference', readOnly: true, submitValue: false},
													{name: 'targetAdjustedExposureDifference', readOnly: true, submitValue: false}
												]
											}

										]
									}]
							}]


					},
					{
						xtype: 'formgrid',
						title: 'Replications Included',
						detailPageClass: 'Clifton.portfolio.target.run.replication.ReplicationWindow',
						storeRoot: 'replicationList',
						height: 200,
						readOnly: true,
						viewConfig: {forceFit: true},
						plugins: {ptype: 'gridsummary'},
						columnsConfig: [
							{header: 'ID', width: 50, hidden: true, dataIndex: 'id'},
							{header: 'Run ID', width: 50, hidden: true, dataIndex: 'portfolioRun.id'},
							{header: 'Portfolio Target', hidden: true, width: 100, dataIndex: 'target.label', idDataIndex: 'target.id'},
							{header: 'Target Replication', hidden: true, width: 100, dataIndex: 'replication.name'},
							{header: 'Replication', width: 140, dataIndex: 'labelLong'},
							{header: 'Weight %', width: 75, dataIndex: 'allocationWeightAdjusted', type: 'percent'},
							{
								header: 'Target (Original)', hidden: true, width: 75, dataIndex: 'targetExposureAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
								summaryTotalCondition: function(data) {
									return TCG.isFalse(data['matchingReplication']);
								}
							},
							// Original Target + M2M Adjustment (from update with Live Prices)
							{
								header: 'Target (Live Prices)', hidden: true, width: 75, dataIndex: 'targetExposureAdjustedWithMarkToMarket', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
								renderer: function(v, p, r) {
									return TCG.renderAdjustedAmount(v, (r.data['targetExposureAdjusted'] !== r.data['targetExposureAdjustedWithMarkToMarket']), r.data['targetExposureAdjusted'], '0,000', 'M2M Adjustment');
								},
								summaryTotalCondition: function(data) {
									return TCG.isFalse(data['matchingReplication']);
								}
							},
							{header: 'Trading CCY (Original)', hidden: true, width: 50, dataIndex: 'currencyDenominationBaseAmount', type: 'currency', numberFormat: '0,000'},
							{
								header: 'Target', dataIndex: 'Target', width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
								tooltip: 'Original Target plus any M2M Adjustments based on price or exchange rate changes.<br><br>Note: May also update based on Asset Class assignment to a Target Exposure Adjustment Used asset class (to offset its target change).<br><br>Matching Replication targets will update live based on trade entries in their non-matching counterparts.',
								renderer: function(v, p, r) {
									return Clifton.portfolio.target.run.trade.renderTarget(p, r);
								},
								summaryTotalCondition: function(data) {
									return TCG.isFalse(data['matchingReplication']);
								},
								summaryCalculation: function(v, r, field, data, col) {
									return v + Clifton.portfolio.target.run.trade.calculateFinalTarget(r, true);
								},
								summaryRenderer: function(v) {
									return TCG.renderAmount(v, false, '0,000');
								}
							},
							{
								header: 'Exposure', dataIndex: 'Exposure', width: 75, type: 'currency', numberFormat: '0,000', useNull: true, summaryType: 'sum', negativeInRed: true,
								tooltip: '(Actual Contracts * Contract Value) + Additional Exposure + Currency Total<br /><br />Currency is added for Currency replication only.<br />Reverse Exposure option from the replication will reverse all exposure calculated values.',
								renderer: function(v, metaData, r) {
									const format = '0,000';

									const contracts = r.json.currentContractsAdjusted;
									const actual = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.json.value);
									let current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.json.tradeValue);

									let qtip = '';
									if (actual !== current) {
										qtip += '<table><tr><td>Previous Close:</td><td style="text-align: right;">' + Ext.util.Format.number(actual, '0,000') + '</td></tr><tr><td>Change:</td><td style="text-align: right;">' + TCG.renderAmount(current - actual, true) + '</td></tr>';
									}
									if (TCG.isTrue(r.json.replicationType.currency) && r.json.currencyCurrentTotalBaseAmount) {
										qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td>&nbsp;</td></tr>';
										qtip += '<tr><td>Currency Futures Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number(current, '0,000') + '</td></tr>';
										qtip += '<tr><td>Physical Currency Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number((r.json.currencyCurrentLocalAmount * r.json.tradeCurrencyExchangeRate), '0,000') + '</td></tr>';
										qtip += '<tr><td>Physical Other Currency Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number(r.json.currencyCurrentOtherBaseAmount, '0,000') + '</td></tr>';
										qtip += '<tr><td>Unrealized Currency Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number((r.json.currencyCurrentUnrealizedLocalAmount * r.json.tradeCurrencyExchangeRate), '0,000') + '</td></tr>';
										qtip += '<tr><td>Total Physical Currency Exposure:</td><td style="text-align: right;">' + TCG.renderAmount(r.json.currencyCurrentTotalBaseAmount, false, '0,000') + '</td></tr>';
										current = current + r.json.currencyCurrentTotalBaseAmount;
									}
									if (r.json.additionalExposure && r.json.additionalExposure !== 0) {
										qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td>&nbsp;</td></tr>';
										qtip += '<tr><td>Additional Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number(r.json.additionalExposure, '0,000') + '</td></tr>';
										current = current + r.json.additionalExposure;
									}

									if (TCG.isNotBlank(qtip)) {
										qtip += '</table>';
										metaData.css = 'amountAdjusted';
										metaData.attr = 'qtip=\'' + qtip + '\'';
									}
									if (current === 0) {
										return '';
									}

									return TCG.renderAmount(current, false, format);
								},
								summaryTotalCondition: function(data) {
									return TCG.isFalse(data['matchingReplication']);
								},
								summaryCalculation: function(v, r, field, data, col) {
									const contracts = r.json.currentContractsAdjusted;
									const current = Clifton.portfolio.run.trade.calculateExposure(r, contracts, r.json.tradeValue, r.json.currencyCurrentTotalBaseAmount, r.json.totalAdditionalExposure);
									return v + current;
								},
								summaryRenderer: function(v) {
									return TCG.renderAmount(v, false, '0,000');
								}
							},
							{
								header: 'Pending Exposure', width: 75, type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
								tooltip: '(Pending Contracts * Contract Value) + Pending Currency Exposure',
								renderer: function(v, metaData, r) {
									const format = '0,000';
									const pending = Clifton.portfolio.run.trade.calculateExposure(r, r.json.pendingContractsAdjusted, r.json.tradeValue, r.json.currencyPendingTotalBaseAmount, r.json.totalAdditionalExposure);

									let qtip = '';
									if (TCG.isTrue(r.json.replicationType.currency) && r.json.currencyPendingTotalBaseAmount) {
										qtip += TCG.isBlank(qtip) ? '<table>' : '<tr><td>&nbsp;</td></tr>';
										qtip += '<tr><td>Currency Futures Pending Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number(pending, '0,000') + '</td></tr>';
										qtip += '<tr><td>Physical Currency Pending Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number((r.json.currencyPendingLocalAmount * r.json.tradeCurrencyExchangeRate), '0,000') + '</td></tr>';
										qtip += '<tr><td>Physical Other Currency Pending Exposure:</td><td style="text-align: right;">' + Ext.util.Format.number(r.json.currencyPendingOtherBaseAmount, '0,000') + '</td></tr>';
										qtip += '<tr><td>Total Physical Currency Pending Exposure:</td><td style="text-align: right;">' + TCG.renderAmount(r.json.currencyPendingTotalBaseAmount, false, '0,000') + '</td></tr>';
									}
									if (TCG.isNotBlank(qtip)) {
										qtip += '</table>';
										metaData.css = 'amountAdjusted';
										metaData.attr = 'qtip=\'' + qtip + '\'';
									}
									return TCG.renderAmount(pending, false, format);

								},
								summaryTotalCondition: function(data) {
									return TCG.isFalse(data['matchingReplication']);
								},
								summaryCalculation: function(v, r, field, data, col) {
									const pending = Clifton.portfolio.run.trade.calculateExposure(r, r.json.pendingContractsAdjusted, r.json.tradeValue);
									return v + pending;
								},
								summaryRenderer: function(v) {
									return TCG.renderAmount(v, false, '0,000');
								}
							},
							{
								header: 'Difference', dataIndex: 'Difference', width: 75, type: 'currency', css: 'BORDER-RIGHT: #c0c0c0 1px solid;',
								tooltip: 'Target - Exposure - Pending',
								renderer: function(v, p, r) {
									const dif = this.calculateRecordValue(r);
									return TCG.renderAmount(dif, false, '0,000');
								},
								summaryTotalCondition: function(data) {
									return TCG.isFalse(data['matchingReplication']);
								},
								summaryCalculation: function(v, r, field, data, col) {
									const d = col.calculateRecordValue(r);
									return v + d;
								},
								summaryRenderer: function(v) {
									return TCG.renderAmount(v, false, '0,000');
								},
								calculateRecordValue: function(r) {
									const target = Clifton.portfolio.target.run.trade.calculateFinalTarget(r);
									const current = Clifton.portfolio.run.trade.calculateExposure(r, r.json.currentContractsAdjusted, r.json.tradeValue, r.json.currencyCurrentTotalBaseAmount, r.json.totalAdditionalExposure);
									const pending = Clifton.portfolio.run.trade.calculateExposure(r, r.json.pendingContractsAdjusted, r.json.tradeValue, r.json.currencyPendingTotalBaseAmount, r.json.totalAdditionalExposure);
									return target - current - pending;
								}
							}
						]
					}
				]
			}]
		},

			{
				title: 'Pending Trades',
				items: [{
					name: 'portfolioAggregateRunTradeDetailPendingTradeList',
					xtype: 'run-replication-pending-trade-gridpanel'
				}]
			},

			{
				title: 'Pending Transfers',
				items: [{
					name: 'portfolioAggregateRunTradeDetailPendingTransferList',
					xtype: 'run-replication-pending-transfer-gridpanel',
				}]
			}
		]
	}]
});
