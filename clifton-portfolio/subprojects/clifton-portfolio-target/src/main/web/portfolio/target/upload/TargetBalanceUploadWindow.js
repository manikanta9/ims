Clifton.portfolio.target.upload.TargetBalanceUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'targetBalanceUploadWindow',
	title: 'Portfolio Target Balance Import',
	iconCls: 'import',
	height: 400,
	hideOKButton: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Portfolio Target Balance Uploads allow you to import targets from files directly into the system. Target balances are unique by active date range for a target name and client account combination.',
		labelWidth: 40,

		addToolbarButtons: function(toolbar, formPanel) {
			toolbar.add(
				{
					text: 'Simple Sample File',
					iconCls: 'excel',
					tooltip: 'Download Simple Excel Sample File',
					handler: function() {
						TCG.openFile('portfolio/target/upload/SimplePortfolioTargetBalanceUploadSample.xls');
					}
				}
			);
		},

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{boxLabel: 'End active overlapping targets', name: 'endActive', xtype: 'checkbox', qtip: 'Check this box to end an existing, active target balance for the same client account target as one being uploaded'},
			{
				xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
				items: [
					{name: 'message', xtype: 'textarea', readOnly: true, submitField: false}
				]
			}
		],

		getSaveURL: function() {
			return 'portfolioTargetBalanceUploadFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&requestedPropertiesToExcludeGlobally=portfolioTargetBalanceUploadCommand';
		}
	}]
});
