Clifton.portfolio.target.run.RunCashAllocation = {
	title: 'Cash Allocation',

	items: [{
		name: 'portfolioTargetRunManagerAccountListFind',
		xtype: 'gridpanel',
		instructions: 'Selected client account has the following cash distribution of manager account balances. Managers that have no cash balance are excluded from this view.',
		appendStandardColumns: false,
		getLoadParams: function() {
			const nonRollupOnly = TCG.getChildByName(this.getTopToolbar(), 'nonRollupOnly').getValue();
			if (nonRollupOnly === true) {
				return {
					runId: this.getWindow().getMainForm().formValues.id,
					withCashValuesOnly: true,
					rollupTarget: false
				};
			}
			return {
				runId: this.getWindow().getMainForm().formValues.id,
				withCashValuesOnly: true,
				rootOnly: true
			};
		},
		topToolbarUpdateCountPrefix: '-',
		getTopToolbarFilters: function(toolbar) {
			return [
				{
					boxLabel: 'View Non Rollup Target Level&nbsp;', xtype: 'checkbox', name: 'nonRollupOnly',

					listeners: {
						check: function(field) {
							TCG.getParentByClass(field, Ext.Panel).reload();
						}
					}
				}
			];
		},
		groupField: 'portfolioTarget.label',
		groupTextTpl: '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Managers" : "Manager"]})',
		remoteSort: true,
		isPagingEnabled: function() {
			return false;
		},
		pageSize: 1000,
		columns: [
			{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
			{header: 'Manager ID', width: 30, dataIndex: 'managerAccountAssignment.referenceOne.id', hidden: true},
			{header: 'Target', width: 140, dataIndex: 'portfolioTarget.label', hidden: true},
			{header: 'Manager', width: 200, dataIndex: 'managerAccountAssignment.managerLabel'},
			{header: 'Account', width: 75, dataIndex: 'managerAccountAssignment.referenceOne.accountNumber', hidden: true},
			{header: 'Custodian Account', width: 170, dataIndex: 'managerAccountAssignment.referenceOne.custodianAccount.number', hidden: true},
			{
				header: 'Cash', width: 100, dataIndex: 'cashAllocation', type: 'currency', summaryType: 'sum',
				renderer: function(v, p, r) {
					return TCG.renderAmount(v, false, '0,000');
				}
			},
			{
				header: 'Previous Day Cash', width: 100, dataIndex: 'previousDayCashAllocation', type: 'currency', summaryType: 'sum',
				renderer: function(v, p, r) {
					return TCG.renderAmount(v, false, '0,000');
				}
			},
			{header: 'Difference', width: 100, dataIndex: 'cashPreviousCashAllocationDifference', type: 'currency', numberFormat: '0,000', summaryType: 'sum'},
			{header: 'Difference %', width: 100, dataIndex: 'cashPreviousCashAllocationPercentDifference', type: 'percent', summaryType: 'percent', denominatorValueField: 'previousDayCashAllocation', numeratorValueField: 'cashPreviousCashAllocationDifference'},
			{
				header: '',
				width: 40,
				renderer: function(v, args, r) {
					let actionCol = TCG.renderActionColumn('manager', '', 'Manager Balance', 'MANAGER_BALANCE');
					actionCol = actionCol + '<div style="width:5px; float: left;">&nbsp;</div>' + TCG.renderActionColumn('cash', '', 'Manager Balance Transactions', 'MANAGER_BALANCE_CASH');
					actionCol = actionCol + '<div style="width:5px; float: left;">&nbsp;</div>' + TCG.renderActionColumn('grid', '', 'Manager Balance History', 'MANAGER_BALANCE_HISTORY');
					return actionCol;
				}
			}
		],
		plugins: {ptype: 'groupsummary'},
		gridConfig: {
			listeners: {
				rowclick: function(grid, rowIndex, evt) {
					if (TCG.isActionColumn(evt.target)) {
						const eventName = TCG.getActionColumnEventName(evt);
						if (eventName && TCG.startsWith(eventName, 'MANAGER')) {
							grid.ownerGridPanel.editor.openWindowFromContextMenu(grid, rowIndex, eventName);
						}
					}
				}
			}
		},
		editor: {
			detailPageClass: 'Clifton.portfolio.target.manager.ManagerAccountWindowSelector',
			drillDownOnly: true,

			// Used to Open the Manager Balance Window Directly, or the Manager Account - Balance History Tab
			openWindowFromContextMenu: function(grid, rowIndex, screen) {
				const gridPanel = this.getGridPanel();
				const row = grid.store.data.items[rowIndex];
				let clazz;
				let id;
				let defaultActiveTabName = undefined;

				// Manager Balance Window
				const mgrId = row.json.managerAccountAssignment.referenceOne.id;

				if (screen === 'MANAGER_BALANCE') {
					id = TCG.data.getData('portfolioTargetRunManagerAccount.json?id=' + row.json.id, gridPanel).balance.id;
					clazz = 'Clifton.investment.manager.BalanceWindow';
				}
				else if (screen === 'MANAGER_BALANCE_CASH') {
					id = TCG.data.getData('portfolioTargetRunManagerAccount.json?id=' + row.json.id, gridPanel).balance.id;
					clazz = 'Clifton.investment.manager.BalanceWindow';
					defaultActiveTabName = 'Cash Transactions';
				}
				else {
					id = mgrId;
					clazz = 'Clifton.investment.manager.AccountWindow';
					defaultActiveTabName = 'Balance History';
				}
				this.openDetailPage(clazz, gridPanel, id, row, null, defaultActiveTabName);
			}
		},
		listeners: {
			afterrender: function(gridPanel) {
				const el = gridPanel.getEl();
				el.on('contextmenu', function(e, target) {
					const g = gridPanel.grid;
					g.contextRowIndex = g.view.findRowIndex(target);
					e.preventDefault();
					if (!g.drillDownMenu) {
						g.drillDownMenu = new Ext.menu.Menu({
							items: [
								{
									text: 'Open Manager Balance', iconCls: 'manager', handler: function() {
										gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'MANAGER_BALANCE');
									}
								},
								{
									text: 'Open Manager Balance Cash Flow', iconCls: 'cash', handler: function() {
										gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'MANAGER_BALANCE_CASH');
									}
								},
								{
									text: 'Open Manager Balance History', iconCls: 'grid', handler: function() {
										gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'MANAGER_BALANCE_HISTORY');
									}
								}
							]
						});
					}
					TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
				}, gridPanel);
			}
		}
	}]
};
