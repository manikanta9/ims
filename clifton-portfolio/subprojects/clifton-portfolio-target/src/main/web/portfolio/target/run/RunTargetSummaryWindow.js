Clifton.portfolio.target.run.RunTargetSummaryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Run Target Summary',
	iconCls: 'grouping',
	width: 1050,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Target Summary Details',
			items: [{
				xtype: 'formpanel',
				loadValidation: false,
				readOnly: true,
				defaults: {anchor: '0'},
				url: 'portfolioTargetRunTargetSummary.json?requestedMaxDepth=4',
				labelWidth: 135,

				getLoadParams: function() {
					const defaultData = this.getWindow().defaultData;
					return {
						runId: defaultData.runId,
						targetId: defaultData.targetId
					};
				},

				items: [
					{
						xtype: 'panel',
						layout: 'column',
						items: [{
							columnWidth: .49,
							items: [
								{
									xtype: 'formfragment',
									frame: false,
									labelWidth: 140,
									items: [
										{fieldLabel: 'Client Account', name: 'overlayRun.clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'overlayRun.clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
										{fieldLabel: 'Target', name: 'target.name', xtype: 'linkfield', detailIdField: 'target.id', detailPageClass: 'Clifton.portfolio.target.TargetWindow'},
										{fieldLabel: 'Balance Date', name: 'overlayRun.balanceDate', xtype: 'displayfield', type: 'date'}
										// {boxLabel: 'Private Target (Exclude From Reports)', name: 'target', xtype: 'checkbox'}
									]
								}
							]
						},

							{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},

							{
								columnWidth: .50,
								items: [
									{
										xtype: 'fieldset',
										title: 'Allocations',
										labelWidth: 200,
										items: [
											{
												fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
												defaults: {
													xtype: 'displayfield',
													style: 'text-align: right',
													flex: 1
												},
												items: [
													{value: 'Amount'},
													{value: 'Percentage (%)'}
												]
											},
											{
												fieldLabel: 'Actual Physical Exposure (Original)', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'actualAllocation'},
													{name: 'actualAllocationPercent'}
												]
											},
											{
												fieldLabel: 'Actual Physical Exposure (Adjusted)', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'actualAllocationAdjusted'},
													{name: 'actualAllocationAdjustedPercent'}
												]
											},

											{
												fieldLabel: 'Target Allocation (Original)', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'targetAllocation'},
													{name: 'targetAllocationPercent'}
												]
											},
											{
												fieldLabel: 'Target Allocation (Adjusted)', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'targetAllocationAdjusted'},
													{name: 'targetAllocationAdjustedPercent'}
												]
											},

											{
												fieldLabel: 'Physical Deviation from Adj Target', xtype: 'compositefield',
												defaults: {
													xtype: 'currencyfield',
													flex: 1
												},
												items: [
													{name: 'physicalDeviationFromAdjustedTarget'},
													{name: 'physicalDeviationFromAdjustedTargetPercent'}
												]
											}]
									}]
							}]
					}]
			}]
		},


			{
				title: 'Manager Allocation',

				items: [{
					name: 'portfolioTargetRunManagerAccountListFind',
					xtype: 'gridpanel',
					instructions: 'The selected Target has the following allocation of manager account cash and security balances.',
					appendStandardColumns: false,
					getLoadParams: function() {
						return {
							portfolioTargetId: TCG.getValue('target.id', this.getWindow().getMainForm().formValues),
							runId: TCG.getValue('overlayRun.id', this.getWindow().getMainForm().formValues)
						};
					},
					remoteSort: true,
					isPagingEnabled: function() {
						return false;
					},
					pageSize: 1000,
					columns: [
						{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
						{header: 'Target', width: 140, dataIndex: 'portfolioTarget.label', hidden: true},
						{header: 'Manager', width: 200, dataIndex: 'managerAccountAssignment.managerLabel'},
						{header: 'Account', width: 75, dataIndex: 'managerAccountAssignment.referenceOne.accountNumber', hidden: true},

						{header: 'Securities', width: 100, dataIndex: 'securitiesAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, hidden: true},
						{header: 'Cash', width: 100, dataIndex: 'cashAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Total Market Value', width: 100, dataIndex: 'totalAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},

						{header: 'Previous Day Securities', width: 100, dataIndex: 'previousDaySecuritiesAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Day Cash', width: 100, dataIndex: 'previousDayCashAllocation', type: 'currency', hidden: true, summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Day Total', width: 100, dataIndex: 'previousDayTotalAllocation', type: 'currency', hidden: true, summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},

						{header: 'Previous Month End Securities', width: 100, dataIndex: 'previousMonthEndSecuritiesAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Month End Cash', width: 100, dataIndex: 'previousMonthEndCashAllocation', hidden: true, type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true},
						{header: 'Previous Month End Total', width: 100, dataIndex: 'previousMonthEndTotalAllocation', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, hidden: true},

						{header: 'One Day % Change', width: 100, dataIndex: 'oneDayPercentChange', type: 'percent', summaryType: 'percentChange', previousValueField: 'previousDayTotalAllocation', currentValueField: 'totalAllocation', negativeInRed: true},
						{header: 'MTD % Change', width: 100, dataIndex: 'monthPercentChange', type: 'percent', summaryType: 'percentChange', previousValueField: 'previousMonthEndTotalAllocation', currentValueField: 'totalAllocation', negativeInRed: true},
						{header: 'Cash %', width: 65, dataIndex: 'cashAllocationPercent', type: 'percent', summaryType: 'percent', denominatorValueField: 'totalAllocation', numeratorValueField: 'cashAllocation', negativeInRed: true}
					],
					plugins: {ptype: 'groupsummary'},
					editor: {
						detailPageClass: 'Clifton.portfolio.target.manager.ManagerAccountWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Replications',
				items: [{
					name: 'portfolioTargetRunReplicationListByTarget',
					xtype: 'gridpanel',
					instructions: 'The following replications are configured for selected target.',
					standardColumns: [],
					getLoadParams: function() {
						return {
							targetId: this.getWindow().getMainForm().formValues.target.id,
							requestedMaxDepth: 5
						};
					},
					groupField: 'replication.name',
					groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Securities" : "Security"]})',
					remoteSort: true,
					columns: [
						{header: 'Replication', width: 140, dataIndex: 'replication.name', hidden: true},
						{header: 'Instrument', width: 170, dataIndex: 'security.instrument.name'},

						// Targets
						{header: 'IsTargetAdjusted', width: 50, dataIndex: 'targetAdjusted', type: 'boolean', hidden: true},
						{header: 'Target Allocation % (Original)', width: 80, dataIndex: 'allocationWeight', type: 'percent', summaryType: 'sum', hidden: true},
						{
							header: 'Target Allocation %', width: 80, dataIndex: 'allocationWeightAdjusted', type: 'percent', summaryType: 'sum', hideGrandTotal: true,
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, r.data['targetAdjusted'], r.data['allocationWeight'], '0,000.00 %');
							}
						},
						{header: 'Target Allocation Amount', width: 100, dataIndex: 'targetExposure', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},


						// Contracts
						{header: 'Value', width: 25, dataIndex: 'value', type: 'currency', numberFormat: '0,000', negativeInRed: true},
						{header: 'Target Contracts', width: 85, dataIndex: 'targetContracts', type: 'currency', summaryType: 'sum', negativeInRed: true, hideGrandTotal: true},
						{header: 'Actual Contracts (Original)', width: 85, dataIndex: 'actualContracts', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true, hidden: true},
						{
							header: 'Actual Contracts', width: 80, dataIndex: 'actualContractsAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, r.data['actualContracts'] !== v, r.data['actualContracts'], '0,000', 'Virtual Contracts');
							}
						},

						{header: 'Security', width: 100, dataIndex: 'security.symbol'},
						{header: 'Underlying Security', width: 100, dataIndex: 'underlyingSecurity.symbol', hidden: true},
						{header: 'Underlying Security Price', width: 85, dataIndex: 'underlyingSecurityPrice', type: 'float', negativeInRed: true, useNull: true, hidden: true},
						{header: 'Security Price', width: 85, dataIndex: 'securityPrice', type: 'float', negativeInRed: true},

						{header: 'Days To Mature', width: 85, dataIndex: 'daysToMaturity', type: 'int', negativeInRed: true},
						{header: 'Interest Rate', width: 85, dataIndex: 'interestRate', type: 'currency', numberFormat: '0,000.0000', negativeInRed: true, hidden: true},
						{header: 'Syn Adj Factor', width: 85, dataIndex: 'syntheticAdjustmentFactor', type: 'currency', numberFormat: '0,000.0000', negativeInRed: true, useNull: true, hidden: true}
					],
					plugins: {ptype: 'groupsummary', showGrandTotal: false},
					editor: {
						detailPageClass: 'Clifton.portfolio.target.run.replication.ReplicationWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
