Clifton.portfolio.target.manager.ManagerAccountForm = Ext.extend(TCG.form.FormPanel, {

	loadValidation: false,
	readOnly: true,
	url: 'portfolioTargetRunManagerAccount.json',
	labelWidth: 125,

	initComponent: function() {
		const currentItems = [];
		Ext.each(this.managerItems, function(f) {
			currentItems.push(f);
		});
		this.items = currentItems;
		Clifton.portfolio.target.manager.ManagerAccountForm.superclass.initComponent.call(this);
	},

	fieldsAdded: false, // Used in afterload so dynamic fields aren't added more than once
	listeners: {
		afterload: function(panel) {
			const f = panel.getForm();

			const cashOverlayType = TCG.getValue('managerAccountAssignment.cashOverlayType', f.formValues);
			let cashOverlayTypeDescription = 'Do not overlay manager cash balance';
			if (cashOverlayType === 'FUND') {
				cashOverlayTypeDescription = 'Overlay manager cash balance using Fund Cash allocation weights';
			}
			else if (cashOverlayType === 'MANAGER') {
				cashOverlayTypeDescription = 'Overlay manager cash balance using manager\'s asset class allocation';
			}
			else if (cashOverlayType === 'MINIMIZE_IMBALANCES') {
				cashOverlayTypeDescription = 'Allocate manager cash to minimize imbalances';
			}
			else if (cashOverlayType === 'CUSTOM') {
				cashOverlayTypeDescription = 'Overlay manager cash balance using a custom asset class allocation';
			}
			panel.setFormValue('managerAccountAssignment.cashOverlayTypeDescription', cashOverlayTypeDescription, true);

			// If field was already added - don't add it again
			if (panel.fieldAdded) {
				return;
			}
			const fsItems = [];

			// Only Add Balance Note Field, if there is a note
			const balanceNote = TCG.getValue('balance.note', f.formValues);
			if (TCG.isNotBlank(balanceNote)) {
				const field = {
					fieldLabel: 'Balance Note'
					, xtype: 'label'
					, html: TCG.renderText(balanceNote)
				};
				// Push Field onto the form
				fsItems.push(field);
			}

			// Only Add Adjustments if there are adjustments
			const adjustments = TCG.getValue('balanceAdjustmentList', f.formValues);
			if (adjustments && adjustments.length > 0) {
				const cols = [
					{header: 'Type', width: 150, dataIndex: 'adjustmentType.name'},
					{header: 'Note', dataIndex: 'note', width: 200},
					{header: 'Cash', dataIndex: 'cashValue', width: 80, type: 'currency'},
					{header: 'Securities', dataIndex: 'securitiesValue', width: 80, type: 'currency'}
				];
				TCG.grid.fillColumnMetaData(cols, [], []);
				const adjGrid = new Ext.grid.GridPanel({
					autoHeight: true,
					collapsible: true,
					title: 'Adjustments',
					viewConfig: {forceFit: true},
					store: new Ext.data.JsonStore({
						idProperty: 'id',
						fields: ['id', 'adjustmentType.name', 'note', 'cashValue', 'securitiesValue'],
						data: adjustments
					}),
					colModel: new Ext.grid.ColumnModel({columns: cols})
				});
				fsItems.push(adjGrid);
			}

			if (fsItems && fsItems.length > 0) {
				const fs = new Ext.form.FieldSet({
					title: 'Balance/Adjustment Notes',
					items: fsItems
				});
				panel.add(fs);
				// Redo Layout & Mark as Added
				panel.doLayout();
				panel.fieldAdded = true;
			}
		}
	},

	managerItems: [
		{fieldLabel: 'Client Account', name: 'overlayRun.clientInvestmentAccount.label', xtype: 'linkfield', detailIdField: 'overlayRun.clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
		{fieldLabel: 'Manager Account', name: 'managerAccountAssignment.managerAccountLabel', xtype: 'linkfield', detailIdField: 'managerAccountAssignment.referenceOne.id', detailPageClass: 'Clifton.investment.manager.AccountWindow'},
		{fieldLabel: 'Target', name: 'portfolioTarget.label', xtype: 'linkfield', detailIdField: 'portfolioTarget.id', detailPageClass: 'Clifton.portfolio.target.TargetWindow'},
		{boxLabel: 'Private', name: 'privateManager', xtype: 'checkbox', qtip: 'Private Manager Assignments are excluded from Client facing reports.'},
		{fieldLabel: 'Proxy Value Allocation', name: 'proxyValueAllocation', xtype: 'currencyfield'},
		{fieldLabel: 'Balance Date', name: 'overlayRun.balanceDate', xtype: 'linkfield', type: 'date', detailIdField: 'balance.id', detailPageClass: 'Clifton.investment.manager.BalanceWindow'},

		{xtype: 'sectionheaderfield', header: 'Balance Allocations', fieldLabel: ''},
		{
			fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
			defaults: {
				xtype: 'displayfield',
				flex: 1
			},
			items: [
				{value: 'Balance Date'},
				{value: 'Previous Business Day'},
				{value: 'Previous Month End'}
			]
		},
		{
			fieldLabel: 'Cash', xtype: 'compositefield',
			defaults: {
				xtype: 'currencyfield',
				flex: 1
			},
			items: [
				{name: 'cashAllocation'},
				{name: 'previousDayCashAllocation'},
				{name: 'previousMonthEndCashAllocation'}
			]
		},
		{
			fieldLabel: '% Change', xtype: 'compositefield',
			defaults: {
				xtype: 'currencyfield',
				flex: 1
			},
			items: [
				{xtype: 'label'},
				{name: 'oneDayPercentChange'},
				{name: 'monthPercentChange'}
			]
		},
		{fieldLabel: 'Cash %', name: 'cashAllocationPercent', xtype: 'currencyfield'}
	]
});
Ext.reg('portfolio-target-run-manager-account-form', Clifton.portfolio.target.manager.ManagerAccountForm);
