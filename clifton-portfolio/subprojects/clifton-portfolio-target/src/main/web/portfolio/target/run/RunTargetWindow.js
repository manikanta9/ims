TCG.use('Clifton.portfolio.run.BaseRunWindow');

TCG.use('Clifton.portfolio.target.run.RunManagerAllocation');
TCG.use('Clifton.portfolio.target.run.RunManagerBalance');
TCG.use('Clifton.portfolio.target.run.RunTargetBalance');
TCG.use('Clifton.portfolio.target.run.RunCashAllocation');
TCG.use('Clifton.portfolio.target.run.RunExposureSummary');
TCG.use('Clifton.portfolio.target.run.RunReplications');
TCG.use('Clifton.portfolio.target.run.RunSyntheticExposure');
TCG.use('Clifton.portfolio.run.RunMargin');

Clifton.portfolio.target.run.RunTargetWindow = Ext.extend(Clifton.portfolio.run.BaseRunWindow, {

	overrideRunItems: function(formPanel, runItems) {
		const runProcessingFieldset = runItems.find(item => item.title === 'Run Processing');
		if (runProcessingFieldset) {
			const tradeCreationIndex = runProcessingFieldset.buttons.findIndex(btn => btn.text === 'Trade Creation');
			if (tradeCreationIndex > -1) {
				runProcessingFieldset.buttons[tradeCreationIndex] = {
					text: 'Trade Creation',
					xtype: 'splitbutton',
					iconCls: 'shopping-cart',
					width: 120,
					handler: function() {
						const f = TCG.getParentFormPanel(this);
						f.executeTradeAction(Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides[f.getFormValue('clientInvestmentAccount.serviceProcessingType.name')]);
					},
					menu: {
						items: [
							{
								text: 'Trade Creation (Replications View)',
								iconCls: 'shopping-cart',
								tooltip: 'Opens a trade creation view that shows security positions grouped by the defined portfolio targets and replications within the portfolio.',
								handler: function() {
									const f = TCG.getParentFormPanel(this);
									f.executeTradeAction(Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides['PORTFOLIO_TARGET']);
								}
							}
						]
					}
				};
			}
		}
	},

	additionalRunItems: [
		{xtype: 'sectionheaderfield', header: 'Target & Exposure Recap', fieldLabel: ''},
		{
			xtype: 'formtable',
			columns: 4,
			defaults: {
				xtype: 'currencyfield',
				decimalPrecision: 0,
				readOnly: true,
				submitValue: false,
				width: '95%'
			},
			items: [
				{cellWidth: '140px', xtype: 'label'},
				{html: 'Target Total', qtip: 'Total Target value for client account', xtype: 'label', style: 'text-align: right'},
				{html: 'Total Exposure', qtip: 'Sum of Exposure: actual positions booked to General Ledger', xtype: 'label', style: 'text-align: right'},
				{html: 'Deviation from Target', qtip: 'Target Total - Total Exposure. Percentage value is capped at +/-9,999.9999.', xtype: 'label', style: 'text-align: right'},

				{html: 'Amount:', xtype: 'label'},
				{name: 'overlayTargetTotal'},
				{name: 'overlayExposureTotal'},
				{name: 'cashExposure'},

				{html: '% of TPV:', xtype: 'label'},
				{name: 'overlayTargetTotalPercent', decimalPrecision: 4},
				{name: 'overlayExposureTotalPercent', decimalPrecision: 4},
				{name: 'cashExposurePercent', decimalPrecision: 4},

				{cellWidth: '120px', xtype: 'label', colspan: 2},
				{html: 'Portfolio (TPV):', xtype: 'label', style: 'text-align: right', qtip: 'Total Portfolio Value'},
				{name: 'portfolioTotalValue'}
			]
		}
	],

	additionalRunTabs: [
		Clifton.portfolio.target.run.RunManagerAllocation,
		Clifton.portfolio.target.run.RunManagerBalance,
		Clifton.portfolio.target.run.RunTargetBalance,
		Clifton.portfolio.target.run.RunCashAllocation,
		Clifton.portfolio.target.run.RunExposureSummary,
		Clifton.portfolio.target.run.RunSyntheticExposure,
		Clifton.portfolio.run.RunAccountNotes,
		Clifton.portfolio.target.run.RunReplications,
		Clifton.portfolio.run.RunMargin,
		Clifton.portfolio.run.RunPositions,
		Clifton.portfolio.run.RunPortalFiles
	]
});
