Clifton.portfolio.target.TargetUpdateWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Portfolio Target Update',
	iconCls: 'account',
	width: 900,
	height: 500,
	modal: true,
	allowOpenFromModal: true,
	forceModified: false,
	doNotWarnOnCloseModified: false,

	closeWindow: function() {
		const win = this;
		win.closing = true;
		const closeWindow = function() {
			if (win.savedSinceOpen && win.openerCt && !win.openerCt.isDestroyed) {
				if (win.reloadOpener) {
					win.reloadOpener();
				}
				else if (win.openerCt.reload) {
					win.openerCt.reload(win);
				}
			}
			win.close();
		};
		if (win.isModified() && !win.doNotWarnOnCloseModified) {
			Ext.Msg.confirm('Discard Changes', 'You are closing a window that has unsaved changes.<br/>Would you like to close the window without saving?', function(a) {
				if (a === 'yes') {
					closeWindow();
				}
				else {
					win.closing = false;
				}
			}, this.app.activeWindow);
			return false;
		}
		closeWindow();
	},

	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		if (closeOnSuccess) {
			win.closeWindow();
		}
	},

	items: [
		{
			xtype: 'formpanel',
			instructions: 'Edit columns for the targets in the grid and Submit Update(s) to save each row\'s modifications',

			getFormLabel: function() {
				return this.getFormValue('clientAccount.label');
			},

			items: [
				{fieldLabel: 'Client Account', xtype: 'linkfield', name: 'clientAccount.label', detailIdField: 'clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
				{
					xtype: 'formgrid',
					name: 'targetListGrid',
					storeRoot: 'targetList',
					dtoClass: 'com.clifton.portfolio.target.PortfolioTarget',
					anchor: '-20',
					viewConfig: {forceFit: true},
					readOnly: true,
					doNotSubmitFields: 'result',

					columnsConfig: [
						{header: 'ID', dataIndex: 'id', type: 'int', hidden: true},
						{header: 'Target', width: 100, dataIndex: 'label'},
						{header: 'Target Percent', width: 50, dataIndex: 'targetPercent', type: 'percent', editor: {xtype: 'floatfield', allowBlank: false}},
						{header: 'Start Date', width: 50, dataIndex: 'startDate', editor: {xtype: 'datefield'}},
						{header: 'End Date', width: 50, dataIndex: 'endDate', editor: {xtype: 'datefield'}},
						{
							header: 'Result', width: 50, dataIndex: 'result',
							renderer: function(value, metaData, record) {
								if (TCG.isBlank(value)) {
									return value;
								}
								let iconCls = 'row_checked';
								let message = '';
								if (typeof value === 'object') {
									if (value.iconCls) {
										iconCls = value.iconCls;
									}
									if (value.message) {
										message = value.message;
									}
								}
								return TCG.renderIconWithTooltip(iconCls, message);
							}
						}
					],

					getGridCellContextMenuItems: async function(grid, rowIndex, columnIndex) {
						const contextMenuItems = [];

						if (columnIndex) {
							const column = grid.getColumnModel().getColumnById(columnIndex);
							if (['targetPercent', 'startDate', 'endDate'].includes(column.dataIndex)) {
								contextMenuItems.push({
									text: 'Copy to all rows',
									iconCls: 'copy',
									handler: () => {
										// get current row selection for copying
										const currentRecord = grid.getStore().getAt(rowIndex);
										const newValue = currentRecord.get(column.dataIndex);
										grid.getStore().each(record => {
											if (record !== currentRecord && record.get(column.dataIndex) !== newValue) {
												record.set(column.dataIndex, newValue);
											}
										});
									}
								});
							}
						}

						// append row context menu items
						const record = grid.getStore().getAt(rowIndex);
						const rowMenuItems = await grid.getGridRowContextMenuItems(grid, rowIndex, record);
						if (rowMenuItems && rowMenuItems.length > 0) {
							contextMenuItems.push(...rowMenuItems);
						}

						return contextMenuItems;
					},

					addToolbarButtons: function(toolbar, formGrid) {
						toolbar.add({
							text: 'Submit Update(s)',
							tooltip: 'Submit the edits defined in the below grid',
							iconCls: 'run',
							scope: this,
							handler: function() {
								const records = formGrid.getStore().getRange();
								formGrid.updateTargets(records, formGrid);
							}
						}, '-');
					},

					updateTargets: function(records, formGrid) {
						if (TCG.isNull(records) || records.filter(record => TCG.isNotNull(record.modified)).length < 1) {
							TCG.showError('No targets have been edited.', 'No Target(s) Modified');
							return;
						}

						const updates = [];
						records.forEach(record => {
							if (TCG.isNotNull(record.modified) && Object.keys(record.modified).length > 0) {
								const params = {id: record.id};
								Object.keys(record.modified)
									.forEach(modifiedField => {
										params[modifiedField] = record.modified[modifiedField];
									});

								updates.push(
									TCG.data.getDataPromise('portfolioTargetSave.json?enableValidatingBinding=true&disableValidatingBindingValidation=true', formGrid, {params: params})
										.then(result => {
											record.set('result', {message: 'Successfully updated fields: ' + Object.keys(record.modified)});
											record.commit();
										})
										.catch(error => record.set('result', {iconCls: 'stop', message: error.message}))
								);
							}
						});

						Promise.all(updates)
							.then(results => {
								formGrid.markModified();
								formGrid.getWindow().savedSinceOpen = true;
							});
					}
				}
			]
		}
	]
});
