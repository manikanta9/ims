TCG.use('Clifton.portfolio.run.RunManagerBalanceGrid');

Clifton.portfolio.target.run.RunManagerBalance = {
	title: 'Manager Adjustments',
	items: [{
		xtype: 'portfolio-run-manager-balance-grid',
		showGroupedAllocations: true,
		groupField: 'portfolioTarget.label',
		groupedAllocationBoxLabel: 'View Non Rollup Target Level&nbsp;',
		columns: [
			{header: 'Target', width: 140, dataIndex: 'portfolioTarget.label', hidden: true},
			{header: 'Manager Account', width: 170, dataIndex: 'managerAccountBalance.managerAccount.label'},
			{header: 'Manager Type', width: 100, dataIndex: 'managerAccountBalance.managerAccount.managerType', hidden: true},
			{header: 'Account', width: 75, dataIndex: 'managerAccountBalance.managerAccount.accountNumber', hidden: true},
			{header: 'Custodian Account', width: 170, dataIndex: 'managerAccountBalance.managerAccount.custodianAccount.number', hidden: true},
			{header: 'CCY', width: 50, dataIndex: 'managerAccountBalance.managerAccount.baseCurrency.symbol', hidden: true},
			{header: 'Private', width: 75, dataIndex: 'privateManager', hidden: true, type: 'boolean'},
			{header: 'Manually Adjusted', width: 75, dataIndex: 'manuallyAdjusted', type: 'boolean', hidden: true},
			// Target Allocations
			{header: 'Allocation', width: 70, hidden: true, tooltip: 'Some managers are allocated a portion of the balance across targets.', dataIndex: 'allocationPercent', type: 'percent', numberFormat: '0,000.0000000000'},

			{
				header: 'Base Securities', width: 100, dataIndex: 'securitiesBalanceBaseAllocationAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, css: 'BACKGROUND-COLOR: #F6F6F6; BORDER-LEFT: #c0c0c0 1px solid;',
				tooltip: 'Base Balances include all system/automatic adjustments, and adjusted for allocation percentages.'
			},
			{
				header: 'Base Cash', width: 100, dataIndex: 'cashBalanceBaseAllocationAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, css: 'BACKGROUND-COLOR: #F6F6F6;',
				tooltip: 'Base Balances include all system/automatic adjustments, and adjusted for allocation percentages.'
			},
			{
				header: 'Base Total', width: 100, dataIndex: 'totalBalanceBaseAllocationAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, css: 'BACKGROUND-COLOR: #F6F6F6; BORDER-RIGHT: #c0c0c0 1px solid;',
				tooltip: 'Base Balances include all system/automatic adjustments, and adjusted for allocation percentages.'
			},
			{
				header: 'Securities Adjustment', width: 100, dataIndex: 'securitiesManualAdjustmentTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', useNull: true, css: 'BACKGROUND-COLOR: #FAFAFA; BORDER-LEFT: #c0c0c0 1px solid;',
				tooltip: 'Manual Security adjustments applied to the manager balance, adjusted for allocation percentages.',
				renderer: function(v, metaData, r) {
					return Clifton.portfolio.run.RenderManagerBalanceAdjustmentList(v, metaData, r, false);
				},
				summaryRenderer: function(v) {
					return TCG.numberFormat(v, '0,000');
				}
			},
			{
				header: 'Cash Adjustment', width: 100, dataIndex: 'cashManualAdjustmentTotalAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', useNull: true, css: 'BACKGROUND-COLOR: #FAFAFA; BORDER-RIGHT: #c0c0c0 1px solid;',
				tooltip: 'Manual Cash adjustments applied to the manager balance, adjusted for allocation percentages.',
				renderer: function(v, metaData, r) {
					return Clifton.portfolio.run.RenderManagerBalanceAdjustmentList(v, metaData, r, true);
				},
				summaryRenderer: function(v) {
					return TCG.numberFormat(v, '0,000');
				}
			},
			{
				header: 'Adjusted Securities', width: 100, dataIndex: 'securitiesAllocationAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, css: 'BACKGROUND-COLOR: #EEEEEE; BORDER-LEFT: #c0c0c0 1px solid;',
				tooltip: 'Adjusted Balance = Base Balance + Manual Adjustments.'
			},
			{
				header: 'Adjusted Cash', width: 100, dataIndex: 'cashAllocationAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, css: 'BACKGROUND-COLOR: #EEEEEE;',
				tooltip: 'Adjusted Balance = Base Balance + Manual Adjustments.'
			},
			{
				header: 'Adjusted Total', width: 100, dataIndex: 'totalAllocationAmount', type: 'currency', summaryType: 'sum', numberFormat: '0,000', negativeInRed: true, css: 'BACKGROUND-COLOR: #EEEEEE; BORDER-RIGHT: #c0c0c0 1px solid;',
				tooltip: 'Adjusted Balance = Base Balance + Manual Adjustments.'
			}

		]
	}]
};
