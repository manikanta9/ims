Clifton.portfolio.target.activity.TargetActivityWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Portfolio Target Balance',
	iconCls: 'account',
	width: 800,
	height: 500,

	items: [
		{
			xtype: 'formpanel',
			url: 'portfolioTargetActivity.json',
			defaults: {anchor: '-20'},

			items: [
				{fieldLabel: 'Type', name: 'type.name', xtype: 'combo', hiddenName: 'type.id', url: 'portfolioTargetActivityTypeListFind.json?loadAll=true', detailPageClass: 'Clifton.portfolio.target.activity.TargetActivityTypeWindow'},
				{
					fieldLabel: 'Target', name: 'target.label', xtype: 'combo', hiddenName: 'target.id', url: 'portfolioTargetListFind.json', requestedProps: 'clientAccount.id|clientAccount.label|replication.label|replication.id', detailPageClass: 'Clifton.portfolio.target.TargetWindow',
					selectHandler: function(combo, record, index) {
						if (record) {
							const formPanel = TCG.getParentFormPanel(combo);
							const clientAccount = TCG.getValue('clientAccount', record.json);
							if (clientAccount) {
								formPanel.setFormValue('target.clientAccount.label', clientAccount.label);
								formPanel.setFormValue('target.clientAccount.id', clientAccount.id);
							}
							const replication = TCG.getValue('replication', record.json);
							if (replication) {
								formPanel.setFormValue('target.replication.label', replication.label);
								formPanel.setFormValue('target.replication.id', replication.label);
							}
						}
					}
				},
				{fieldLabel: 'Client Account', name: 'target.clientAccount.label', detailIdField: 'target.clientAccount.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow'},
				{fieldLabel: 'Replication', name: 'target.replication.label', detailIdField: 'target.replication.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.replication.ReplicationWindow'},
				{xtype: 'label', html: '<hr/>'},
				{fieldLabel: 'Security', name: 'security.label', hiddenName: 'security.id', xtype: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
				{fieldLabel: 'Cause Table', name: 'causeSystemTable.name', xtype: 'combo', hiddenName: 'causeSystemTable.id', url: 'systemTableListFind.json', detailPageClass: 'Clifton.system.schema.TableWindow'},
				{fieldLabel: 'Cause FK Field ID', name: 'causeFKFieldId'},
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {layout: 'form', columnWidth: .5, defaults: {xtype: 'datefield', anchor: '-20'}},
					items: [
						{
							items: [
								{fieldLabel: 'Start Date', name: 'startDate', value: new Date().format('m/d/Y')},
								{fieldLabel: 'Value', name: 'value', xtype: 'currencyfield'}
							]
						},
						{
							items: [
								{fieldLabel: 'End Date', name: 'endDate'},
								{boxLabel: 'Value is Quantity (Unchecked is Notional)', name: 'valueQuantity', xtype: 'checkbox'}
							]
						}
					]
				},
				{fieldLabel: 'Note', name: 'note', xtype: 'textarea', height: 50},
				{boxLabel: 'Is Deleted', name: 'deleted', xtype: 'checkbox', tooltip: 'For historical tracking activity is not deleted. Instead activity is marked deleted.'}
			]
		}
	]
});
