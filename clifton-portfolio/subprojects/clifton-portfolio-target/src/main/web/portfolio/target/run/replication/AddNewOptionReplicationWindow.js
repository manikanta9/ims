Clifton.portfolio.target.run.replication.AddNewOptionReplicationWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'New Portfolio Replication: Create New Option',
	iconCls: 'replicate',
	hideApplyButton: true,
	width: 600,
	height: 300,

	// Special Method so Replications are fully reloaded on Trade Creation Screen to get new ones retrieved and added to list
	reloadOpener: function() {
		const win = this;
		if (win.openerCt && win.openerCt.fullReload) {
			win.openerCt.fullReload();
		}
		else if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload) {
			win.openerCt.reload();
		}
	},

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		url: 'portfolioTargetRunReplication.json',

		getSaveURL: function() {
			return 'portfolioTargetRunReplicationOptionCopy.json';
		},

		instructions: 'Enter the new strike price to create a new option with strike price and add it to the portfolio run target replication. Security will be validated based on the previously selected row. Note: Security will be added to the run for trading only and will not be added to the actual replication allocations.  All targets will be set to zero.<br/><br />NOTE: If a security with new symbol already exists, that one will be used instead of attempting to create a duplicate.',

		items: [
			{xtype: 'hidden', name: 'security.instrument.id', submitValue: false},
			{fieldLabel: 'Target', xtype: 'linkfield', name: 'target.label', hiddenName: 'target.id', detailPageClass: 'Clifton.portfolio.target.TargetWindow', submitDetailField: false},
			{fieldLabel: 'Replication', xtype: 'linkfield', name: 'replication.name', hiddenName: 'replication.id', detailPageClass: 'Clifton.investment.replication.ReplicationWindow', submitDetailField: false},
			{fieldLabel: 'Existing Security', xtype: 'linkfield', name: 'security.label', hiddenName: 'security.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
			{fieldLabel: 'Strike Price', xtype: 'floatfield', name: 'strikePrice', allowBlank: false},
			{fieldLabel: 'Price Override', qtip: 'Populate this value to manually override the security price. This is required for new securities because price is often used to calculate contract value, and will also be used as the Previous Close price as well.', name: 'priceOverride', xtype: 'floatfield', allowBlank: false}
		]
	}]
});
