// NOTE: both grids use the same data but there are 2 separate calls: can make it more efficient later
Clifton.portfolio.target.run.RunExposureSummary = {
	title: 'Exposure Summary',
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},
	frame: true,

	items: [{
		name: 'portfolioTargetRunTargetSummaryListFind',
		xtype: 'gridpanel',
		appendStandardColumns: false,
		title: 'Exposure Percent',
		instructions: 'Selected client account has the following percent allocation of Portfolio Targets from all managers.',
		border: 1,
		flex: 1,

		getLoadParams: function(firstLoad) {
			const nonRollupOnly = TCG.getChildByName(this.getTopToolbar(), 'nonRollupOnly').getValue();
			if (nonRollupOnly === true) {
				return {
					runId: this.getWindow().getMainForm().formValues.id,
					rollupTarget: false
				};
			}
			return {
				runId: this.getWindow().getMainForm().formValues.id,
				rootOnly: true
			};
		},

		topToolbarUpdateCountPrefix: '-',
		getTopToolbarFilters: function(toolbar) {
			return [
				{
					boxLabel: 'View Non Rollup Target Level&nbsp;', xtype: 'checkbox', name: 'nonRollupOnly',

					listeners: {
						check: function(field) {
							TCG.getParentByClass(field, Ext.Panel).reload();
						}
					}
				}
			];
		},

		isPagingEnabled: function() {
			return false;
		},

		columns: [
			{header: 'Target ID', width: 140, dataIndex: 'target.id', hidden: true},
			{header: 'Target', width: 140, dataIndex: 'target.name', detailFieldId: 'target.id'},
			{header: 'IsTargetAdjusted', width: 50, dataIndex: 'targetAdjusted', type: 'boolean', hidden: true},
			{header: 'OutsideOfRebalanceTrigger', width: 50, dataIndex: 'outsideOfRebalanceTrigger', type: 'boolean', hidden: true},
			{header: 'OutsideOfAbsRebalanceTrigger', width: 50, dataIndex: 'outsideOfAbsoluteRebalanceTrigger', type: 'boolean', hidden: true},
			// Percentage Columns
			{header: 'Policy Target', width: 80, dataIndex: 'targetAllocationPercent', type: 'percent', summaryType: 'sum', negativeInRed: true},
			{
				header: 'Adjusted Target', width: 80, dataIndex: 'targetAllocationAdjustedPercent', type: 'percent', summaryType: 'sum',
				renderer: function(v, p, r) {
					return TCG.renderAdjustedAmount(v, r.data['targetAdjusted'], r.data['targetAllocationPercent'], '0,000.00 %');
				}
			},
			{header: 'Physical Exposure (Original)', width: 80, dataIndex: 'actualAllocationPercent', type: 'percent', summaryType: 'sum', hidden: true},
			{
				header: 'Physical Exposure', width: 80, dataIndex: 'actualAllocationAdjustedPercent', type: 'percent', summaryType: 'sum',
				renderer: function(v, p, r) {
					return TCG.renderAdjustedAmount(v, r.data['actualAllocationPercent'] !== r.data['actualAllocationAdjustedPercent'], r.data['actualAllocationPercent'], '0,000.00 %');
				}
			},
			{header: 'Physical Deviation from Adj Target', width: 100, dataIndex: 'physicalDeviationFromAdjustedTargetPercent', type: 'percent', summaryType: 'sum', negativeInRed: true},
			{
				header: 'Total Exposure Deviation from Adj Target', width: 80, dataIndex: 'totalExposureDeviationFromAdjustedTargetPercent', type: 'percent', summaryType: 'sum', negativeInRed: true,
				renderer: function(v, p, r) {
					if (v === undefined || v === '') {
						return '';
					}
					if (r.data['outsideOfRebalanceTrigger'] === true) {
						const value = Ext.util.Format.number(v, '0,000.00 %');
						if (r.data['outsideOfAbsoluteRebalanceTrigger'] === true) {
							return '<div class="ruleViolation"><b>' + value + '</b></div>';
						}
						return '<div class="ruleViolation">' + value + '</div>';
					}
					return TCG.renderAmount(v, false, '0,000.00 %');
				}
			},
			// Percents - Rebalance Triggers
			{
				header: 'Min Rebalance Trigger', width: 80, dataIndex: 'rebalanceTriggerMinPercent', type: 'percent',
				viewNames: ['Overlay', 'Fully Funded']
			},
			{
				header: 'Max Rebalance Trigger', width: 80, dataIndex: 'rebalanceTriggerMaxPercent', type: 'percent',
				viewNames: ['Overlay', 'Fully Funded']
			},
			// Absolutes
			{header: 'Absolute Min Trigger', width: 80, dataIndex: 'rebalanceTriggerAbsoluteMinPercent', type: 'percent', hidden: true},
			{header: 'Absolute Max Trigger', width: 80, dataIndex: 'rebalanceTriggerAbsoluteMaxPercent', type: 'percent', hidden: true}
		],
		plugins: {ptype: 'gridsummary'},
		editor: {
			detailPageClass: 'Clifton.portfolio.target.run.RunTargetSummaryWindow',
			drillDownOnly: true,
			getDefaultData: function(gridPanel) {
				const mainFormValues = gridPanel.getWindow().getMainForm().formValues;
				return {
					runId: mainFormValues.id,
					targetId: gridPanel.grid.getSelectionModel().getSelected().data['target.id']
				};
			}
		}
	},

		{flex: 0.02},

		{
			name: 'portfolioTargetRunTargetSummaryListFind',
			xtype: 'gridpanel',
			appendStandardColumns: false,
			title: 'Exposure in Base Currency',
			instructions: 'Selected client account has the following percent allocation of Portfolio Targets from all managers.',
			border: 1,
			flex: 1,

			getLoadParams: function(firstLoad) {
				const nonRollupOnly = TCG.getChildByName(this.getTopToolbar(), 'nonRollupOnly').getValue();
				if (nonRollupOnly === true) {
					return {
						runId: this.getWindow().getMainForm().formValues.id,
						rollupTarget: false
					};
				}
				return {
					runId: this.getWindow().getMainForm().formValues.id,
					rootOnly: true
				};
			},

			topToolbarUpdateCountPrefix: '-',
			getTopToolbarFilters: function(toolbar) {
				return [
					{
						boxLabel: 'View Non Rollup Target Level&nbsp;', xtype: 'checkbox', name: 'nonRollupOnly',

						listeners: {
							check: function(field) {
								TCG.getParentByClass(field, Ext.Panel).reload();
							}
						}
					}
				];
			},

			isPagingEnabled: function() {
				return false;
			},

			columns: [
				{header: 'Target ID', width: 140, dataIndex: 'target.id', hidden: true},
				{header: 'Target', width: 140, dataIndex: 'target.name', detailFieldId: 'target.id'},
				{header: 'IsTargetAdjusted', width: 50, dataIndex: 'targetAdjusted', type: 'boolean', hidden: true},
				{header: 'OutsideOfRebalanceTrigger', width: 50, dataIndex: 'outsideOfRebalanceTrigger', type: 'boolean', hidden: true},
				{header: 'OutsideOfAbsRebalanceTrigger', width: 50, dataIndex: 'outsideOfAbsoluteRebalanceTrigger', type: 'boolean', hidden: true},

				// Amount Columns
				{header: 'Policy Target', width: 80, dataIndex: 'targetAllocation', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
				{
					header: 'Adjusted Target', width: 80, dataIndex: 'targetAllocationAdjusted', type: 'currency', summaryType: 'sum',
					renderer: function(v, p, r) {
						return TCG.renderAdjustedAmount(v, r.data['targetAdjusted'], r.data['targetAllocation'], '0,000');
					}
				},
				{header: 'Physical Exposure (Original)', width: 80, dataIndex: 'actualAllocation', type: 'currency', numberFormat: '0,000', summaryType: 'sum', hidden: true},
				{
					header: 'Physical Exposure', width: 80, dataIndex: 'actualAllocationAdjusted', type: 'currency', numberFormat: '0,000', summaryType: 'sum',
					renderer: function(v, p, r) {
						return TCG.renderAdjustedAmount(v, r.data['actualAllocation'] !== r.data['actualAllocationAdjusted'], r.data['actualAllocation'], '0,000');
					}
				},
				{header: 'Physical Deviation from Adj Target', width: 100, dataIndex: 'physicalDeviationFromAdjustedTarget', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true},
				{
					header: 'Total Exposure Deviation from Adj Target', width: 80, dataIndex: 'totalExposureDeviationFromAdjustedTarget', type: 'currency', numberFormat: '0,000', summaryType: 'sum', negativeInRed: true,
					renderer: function(v, p, r) {
						if (v === undefined || v === '') {
							return '';
						}
						if (r.data['outsideOfRebalanceTrigger'] === true) {
							const value = Ext.util.Format.number(v, '0,000');
							if (r.data['outsideOfAbsoluteRebalanceTrigger'] === true) {
								return '<div class="ruleViolation"><b>' + value + '</b></div>';
							}
							return '<div class="ruleViolation">' + value + '</div>';
						}
						return TCG.renderAmount(v, false, '0,000');
					}
				}

			],
			plugins: {ptype: 'gridsummary'},
			editor: {
				detailPageClass: 'Clifton.portfolio.target.run.RunTargetSummaryWindow',
				drillDownOnly: true,
				getDefaultData: function(gridPanel) {
					const mainFormValues = gridPanel.getWindow().getMainForm().formValues;
					return {
						runId: mainFormValues.id,
						targetId: gridPanel.grid.getSelectionModel().getSelected().data['target.id']
					};
				}
			}
		}]
};
