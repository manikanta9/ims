TCG.use('Clifton.portfolio.target.manager.ManagerAccountForm');

Clifton.portfolio.target.manager.ManagerAccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Target Manager Account',
	iconCls: 'manager',
	width: 650,
	height: 550,
	items: [{xtype: 'portfolio-target-run-manager-account-form'}]
});
