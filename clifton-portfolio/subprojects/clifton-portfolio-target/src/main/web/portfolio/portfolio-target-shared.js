Ext.ns(
	'Clifton.portfolio.target',
	'Clifton.portfolio.target.activity',
	'Clifton.portfolio.target.balance',
	'Clifton.portfolio.target.upload',
	'Clifton.portfolio.target.run',
	'Clifton.portfolio.target.upload',
	'Clifton.portfolio.target.run',
	'Clifton.portfolio.target.run.replication',
	'Clifton.portfolio.target.run.trade',
	'Clifton.portfolio.target.upload',
	'Clifton.portfolio.target.manager',
	'Clifton.portfolio.target.manager.upload'
);

Clifton.system.schema.SimpleUploadWindows['PortfolioTarget'] = 'Clifton.portfolio.target.upload.TargetUploadWindow';
Clifton.system.schema.SimpleUploadWindows['PortfolioTargetBalance'] = 'Clifton.portfolio.target.upload.TargetBalanceUploadWindow';

Clifton.portfolio.run.PortfolioRunWindowOverrides['PORTFOLIO_TARGET'] = 'Clifton.portfolio.target.run.RunTargetWindow';

Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides['PORTFOLIO_TARGET'] = {
	className: 'Clifton.portfolio.target.run.trade.RunTradeTargetWindow',
	getEntity: function(config, entity) {
		const url = 'portfolioRunTrade.json';
		const requestedProperties = 'id|labelLong|tradeEntryDisabled|securityNotTradable|cashExposure|tradingClientAccount.id|replication.name|replicationType.currency|replicationType.durationSupported|security.id|security.symbol|security.endDate|security.instrument.name|security.description|securityPrice|tradeSecurityPrice|tradeSecurityPriceDate|underlyingSecurityPrice|tradeUnderlyingSecurityPrice|tradeUnderlyingSecurityPriceDate|securityDirtyPrice|tradeSecurityDirtyPrice|tradeSecurityDirtyPriceDate|exchangeRate|tradeExchangeRate|tradeExchangeRateDate|value|tradeValue|additionalExposure|additionalOverlayExposure|totalAdditionalExposure|targetExposureAdjusted|actualContracts|actualContractsAdjusted|virtualContracts|currentContracts|currentVirtualContracts|currentContractsAdjusted|pendingContracts|pendingVirtualContracts|pendingContractsAdjusted|buyContracts|sellContracts|trade.tradeDestination.id|trade.tradeDestination.name|trade.tradeExecutionType.id|trade.tradeExecutionType.name|trade.executingBrokerCompany.id|trade.executingBrokerCompany.label|trade.holdingInvestmentAccount.id|trade.holdingInvestmentAccount.label|duration|delta|contractValuePriceField|contractValuePrice|security.priceMultiplier|markToMarketAdjustmentValue|targetExposureAdjustedWithMarkToMarket|currencyActualLocalAmount|currencyExchangeRate|currencyOtherBaseAmount|currencyCurrentLocalAmount|tradeCurrencyExchangeRate|currencyPendingLocalAmount|currencyCurrentOtherBaseAmount|currencyPendingOtherBaseAmount|currencyCurrentTotalBaseAmount|currencyPendingTotalBaseAmount|matchingReplication|currencyUnrealizedLocalAmount|currencyCurrentUnrealizedLocalAmount|currencyCurrentUnrealizedTradeBaseAmount|currencyDenominationBaseAmount|currencyCurrentDenominationBaseAmount|currencyPendingDenominationBaseAmount|allocationWeightAdjusted|reverseExposureSign|rebalanceTriggerMin|rebalanceTriggerMax|security.instrument.fairValueAdjustmentUsed|fairValueAdjustment|tradeFairValueAdjustment|tradeFairValueAdjustmentDate|previousFairValueAdjustment|mispricingValue|mispricingTradeValue|security.instrument.hierarchy.investmentType.name';

		const params = {
			id: entity.id,
			requestedMaxDepth: 7,
			// Changes here must match the target trade window's form request properties
			requestedProperties: requestedProperties,
			requestedPropertiesRoot: 'data.detailList'
		};

		const scope = (config && config.openerCt) ? config.openerCt : this;
		// TODO convert to promise
		const newEntity = TCG.data.getData(url, scope, void 0, {params});
		newEntity.defaultView = entity.defaultView;
		return newEntity;
	}
};

Clifton.portfolio.run.trade.PortfolioRunTradeWindowOverrides['Currency Hedge'] = {
	className: 'Clifton.portfolio.target.run.trade.RunTradeTargetAggregateWindow',
	getEntity: function(config, entity) {
		const url = 'portfolioAggregateRunTrade.json';
		const requestedProperties = 'id|labelLong|tradeEntryDisabled|securityNotTradable|cashExposure|tradingClientAccount.id|replication.name|replicationType.currency|replicationType.durationSupported|security.id|security.symbol|security.endDate|security.instrument.name|security.description|securityPrice|tradeSecurityPrice|tradeSecurityPriceDate|underlyingSecurityPrice|tradeUnderlyingSecurityPrice|tradeUnderlyingSecurityPriceDate|securityDirtyPrice|tradeSecurityDirtyPrice|tradeSecurityDirtyPriceDate|exchangeRate|tradeExchangeRate|tradeExchangeRateDate|value|tradeValue|additionalExposure|totalAdditionalExposure|targetExposure|actualContracts|currentContracts|pendingContracts|buyContracts|sellContracts|trade.tradeDestination.id|trade.tradeDestination.name|trade.tradeExecutionType.id|trade.tradeExecutionType.name|trade.executingBrokerCompany.id|trade.executingBrokerCompany.label|trade.holdingInvestmentAccount.id|trade.holdingInvestmentAccount.label|duration|delta|contractValuePriceField|contractValuePrice|security.priceMultiplier|markToMarketAdjustmentValue|targetExposureAdjustedWithMarkToMarket|currencyActualLocalAmount|currencyExchangeRate|currencyOtherBaseAmount|currencyCurrentLocalAmount|tradeCurrencyExchangeRate|currencyPendingLocalAmount|currencyCurrentOtherBaseAmount|currencyPendingOtherBaseAmount|currencyCurrentTotalBaseAmount|currencyPendingTotalBaseAmount|matchingReplication|currencyUnrealizedLocalAmount|currencyCurrentUnrealizedLocalAmount|currencyCurrentUnrealizedTradeBaseAmount|currencyDenominationBaseAmount|currencyCurrentDenominationBaseAmount|currencyPendingDenominationBaseAmount|allocationWeight|reverseExposureSign|rebalanceTriggerMin|rebalanceTriggerMax|security.instrument.fairValueAdjustmentUsed|fairValueAdjustment|tradeFairValueAdjustment|tradeFairValueAdjustmentDate|previousFairValueAdjustment|mispricingValue|mispricingTradeValue|security.instrument.hierarchy.investmentType.name|tradeValueCurrency.symbol';

		const params = {
			id: entity.id,
			requestedMaxDepth: 7,
			// Changes here must match the target trade window's form request properties
			requestedProperties: requestedProperties,
			requestedPropertiesRoot: 'data.detailList'
		};

		const scope = (config && config.openerCt) ? config.openerCt : this;
		// TODO convert to promise
		const newEntity = TCG.data.getData(url, scope, void 0, {params});
		newEntity.defaultView = entity.defaultView;
		return newEntity;
	}
};

Clifton.portfolio.target.TargetGrid = Ext.extend(Clifton.system.schema.GridPanelWithCustomJsonFields, {
	name: 'portfolioTargetListFind',
	tableName: 'PortfolioTarget',
	columnGroupName: 'Portfolio Target Custom Fields',
	instructions: 'A list of portfolio targets',
	topToolbarSearchParameter: 'searchPattern',
	requestIdDataIndex: true,
	importTableName: [
		{table: 'PortfolioTarget', label: 'Portfolio Targets', importComponentName: 'Clifton.portfolio.target.upload.TargetUploadWindow'},
		{table: 'PortfolioTargetBalance', label: 'Portfolio Target Balances', importComponentName: 'Clifton.portfolio.target.upload.TargetBalanceUploadWindow'}
	],
	nonCustomColumns: [
		{header: 'ID', width: 10, dataIndex: 'id', hidden: true},
		{header: 'Target', exportHeader: 'Name', width: 75, dataIndex: 'label', exportDataIndex: 'name', idDataIndex: 'id', renderer: TCG.renderValueWithTooltip, filter: {searchFieldName: 'id', type: 'combo', url: 'portfolioTargetListFind.json', displayField: 'label', detailPageClass: 'Clifton.portfolio.target.TargetWindow'}},
		{header: 'Client Account', exportHeader: 'ClientAccount-Number', width: 75, dataIndex: 'clientAccount.label', exportDataIndex: 'clientAccount.number', idDataIndex: 'clientAccount.id', filter: {searchFieldName: 'clientAccountId', type: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow'}},
		{header: 'Parent Target', width: 50, exportHeader: 'Parent-Name', dataIndex: 'parent.label', exportDataIndex: 'parent.name', idDataIndex: 'parent.id', hidden: true, filter: {searchFieldName: 'parentId', type: 'combo', url: 'portfolioTargetListFind.json', displayField: 'label', detailPageClass: 'Clifton.portfolio.target.TargetWindow'}},
		{header: 'Matching Target', width: 50, exportHeader: 'MatchingTarget-Name', dataIndex: 'matchingTarget.label', exportDataIndex: 'matchingTarget.name', idDataIndex: 'matchingTarget.id', hidden: true, filter: {searchFieldName: 'matchingTargetId', type: 'combo', url: 'portfolioTargetListFind.json', displayField: 'label', detailPageClass: 'Clifton.portfolio.target.TargetWindow'}},
		{header: 'Replication', width: 75, exportHeader: 'Replication-Name', dataIndex: 'replication.label', exportDataIndex: 'replication.name', idDataIndex: 'replication.id', filter: {searchFieldName: 'replicationName', type: 'combo', url: 'investmentReplicationListFind.json', displayField: 'label', detailPageClass: 'Clifton.investment.ReplicationWindow'}},
		{
			header: 'Target Calculator', width: 150, dataIndex: 'targetCalculatorBean.name', idDataIndex: 'targetCalculatorBean.id', editor: {xtype: 'combo', url: 'systemBeanListFind.json?groupName=Portfolio Target Calculator', detailPageClass: 'Clifton.system.bean.BeanWindow'},
			tooltip: 'Calculator used to produce a Target Balance for the corresponding Portfolio Target during Portfolio Run processing'
		},
		{header: 'Target Percent', width: 30, type: 'percent', dataIndex: 'targetPercent'},
		{header: 'Start Date', width: 20, dataIndex: 'startDate'},
		{header: 'End Date', width: 20, dataIndex: 'endDate'},
		{header: 'Order', width: 15, dataIndex: 'order', type: 'int', useNull: true},
		{header: 'Rollup', width: 15, dataIndex: 'rollup', type: 'boolean'},
		{header: 'Private', width: 15, dataIndex: 'privateTarget', type: 'boolean', tooltip: 'Private Targets are used for portfolio processing, but are excluded from reports'}
	],
	getTopToolbarFilters: function(toolbar) {
		return [{fieldLabel: 'Active On', xtype: 'toolbar-datefield', name: 'activeOnDate'}];
	},
	getLoadParams: function(firstLoad) {
		const params = {};
		const activeOnField = TCG.getChildByName(this.getTopToolbar(), 'activeOnDate');
		if (firstLoad) {
			activeOnField.setValue(new Date().format('m/d/Y'));
		}
		params['activeOnDate'] = TCG.isBlank(activeOnField.getValue()) ? void 0 : activeOnField.getValue().format('m/d/Y');
		return params;
	},
	getGridRowContextMenuItems: function(grid, rowIndex, record) {
		return [
			{
				text: 'Open Client Account',
				iconCls: 'account',
				handler: async () => {
					const clazz = 'Clifton.investment.account.AccountWindow';
					const id = TCG.getValue('clientAccount.id', record.json);
					await TCG.createComponent(clazz, {
						id: TCG.getComponentId(clazz, id),
						params: {id: id},
						openerCt: grid
					});
				}
			}, {
				text: 'Open Replication',
				iconCls: 'replicate',
				disabled: TCG.isBlank(TCG.getValue('replication.id', record.json)),
				handler: async () => {
					const clazz = 'Clifton.investment.replication.ReplicationWindow';
					const id = TCG.getValue('replication.id', record.json);
					await TCG.createComponent(clazz, {
						id: TCG.getComponentId(clazz, id),
						params: {id: id},
						openerCt: grid
					});
				}
			}
		];
	},
	editor: {
		detailPageClass: 'Clifton.portfolio.target.TargetWindow',
		deleteURL: 'portfolioTargetDelete.json',
		endURL: 'portfolioTargetEnd.json',

		addToolbarDeleteButton: function(toolBar, gridPanel) {
			const editor = this;
			const deleteHandler = (action, deleteUrl) => {
				const sm = editor.grid.getSelectionModel();
				const confirmSuffix = action && typeof action === 'string' && 'Delete'.toUpperCase() === action.toUpperCase()
					? ' The preferred method of deactivating a Target is to End it so historic targets are available.' : '';
				if (sm.getCount() === 0) {
					TCG.showError('Please select a Target to ' + action + '.', 'No Target(s) Selected');
				}
				else if (sm.getCount() !== 1) {
					if (editor.allowToDeleteMultiple) {
						Ext.Msg.confirm(action + ' Selected Target(s)', 'Would you like to ' + action + ' all selected Target(s)?' + confirmSuffix, function(a) {
							if (a === 'yes') {
								editor.deleteSelection(sm, gridPanel, deleteUrl);
							}
						});
					}
					else {
						TCG.showError('Multi-selection is not supported yet.  Please select one Target.', 'NOT SUPPORTED');
					}
				}
				else {
					Ext.Msg.confirm(action + ' Selected Target', 'Would you like to ' + action + ' selected Target?' + confirmSuffix, function(a) {
						if (a === 'yes') {
							editor.deleteSelection(sm, gridPanel, deleteUrl);
						}
					});
				}
			};

			toolBar.add({
				text: 'End',
				xtype: 'splitbutton',
				tooltip: 'End selected item(s) for Current Day - 1',
				iconCls: 'remove',
				handler: function() {
					deleteHandler('End', editor.endURL);
				},
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'End',
							tooltip: 'End selected item(s) for Current Day - 1',
							iconCls: 'remove',
							handler: function() {
								deleteHandler('End', editor.endURL);
							}
						},
						{
							text: 'Delete',
							tooltip: 'Delete selected item(s)',
							iconCls: 'remove',
							handler: function() {
								deleteHandler('Delete', editor.deleteURL);
							}
						}
					]
				})
			}, '-');
		}
	}
});
Ext.reg('portfolio-target-grid', Clifton.portfolio.target.TargetGrid);

Clifton.portfolio.target.balance.TargetBalanceGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioTargetBalanceListFind',
	instructions: 'A list of portfolio target balances',
	topToolbarSearchParameter: 'searchPattern',
	populateBalanceActivity: true,
	requestIdDataIndex: true,
	includeTargetNavigation: true,
	importTableName: [
		{table: 'PortfolioTargetBalance', label: 'Portfolio Target Balances', importComponentName: 'Clifton.portfolio.target.upload.TargetBalanceUploadWindow'}
	],
	columns: [
		{header: 'ID', width: 10, dataIndex: 'id', hidden: true},
		{header: 'Target', exportHeader: 'Target-Name', width: 40, dataIndex: 'target.label', idDataIndex: 'target.id', renderer: TCG.renderValueWithTooltip, exportDataIndex: 'target.name', filter: {searchFieldName: 'targetId'}},
		{header: 'Client Account', exportHeader: 'Target-ClientAccount-Number', width: 40, dataIndex: 'target.clientAccount.label', idDataIndex: 'target.clientAccount.id', exportDataIndex: 'target.clientAccount.number', filter: {searchFieldName: 'clientAccountId', type: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow'}},
		{header: 'Replication', exportHeader: 'Target-Replication-Name', width: 40, dataIndex: 'target.replication.label', idDataIndex: 'target.replication.id', exportDataIndex: 'target.replication.name', filter: {searchFieldName: 'replicationName', type: 'combo', url: 'investmentReplicationListFind.json'}},
		{header: 'Parent', exportHeader: 'Target-Parent-Name', width: 40, dataIndex: 'target.parent.label', idDataIndex: 'target.parent.id', hidden: true, exportDataIndex: 'target.parent.name', filter: {searchFieldName: 'parentId', type: 'combo', url: 'portfolioTargetListFind.json', displayField: 'label'}},
		{header: 'Matching Target', exportHeader: 'Target-MatchingTarget-Name', width: 40, dataIndex: 'target.matchingTarget.label', idDataIndex: 'target.matchingTarget.id', hidden: true, exportDataIndex: 'target.matchingTarget.name', filter: {searchFieldName: 'matchingTargetId', type: 'combo', url: 'portfolioTargetListFind.json', displayField: 'label'}},
		{header: 'Value', width: 20, dataIndex: 'value', type: 'currency'},
		{header: 'Adjusted Value', width: 22, dataIndex: 'adjustedValue', type: 'currency'},
		{header: 'MOC Value', width: 20, dataIndex: 'marketOnCloseValue', type: 'currency'},
		{header: 'Description', width: 40, dataIndex: 'targetBalanceDescription', renderer: TCG.renderValueWithTooltip},
		{header: 'Start Date', width: 15, dataIndex: 'startDate'},
		{header: 'End Date', width: 15, dataIndex: 'endDate'},
		{header: 'Quantity', width: 12, dataIndex: 'valueQuantity', type: 'boolean', tooltip: 'Value is Quantity when checked, Notional when unchecked'}
	],
	getTopToolbarFilters: function(toolbar) {
		return [{fieldLabel: 'Active On', xtype: 'toolbar-datefield', name: 'activeOnDate'}];
	},
	getCustomLoadParams: function(firstLoad) {
		return {};
	},
	getLoadParams: function(firstLoad) {
		const params = this.getCustomLoadParams(firstLoad);
		params['populateBalanceActivity'] = this.populateBalanceActivity;
		const activeOnField = TCG.getChildByName(this.getTopToolbar(), 'activeOnDate');
		if (activeOnField) {
			if (firstLoad) {
				activeOnField.setValue(new Date().format('m/d/Y'));
			}
			params['activeOnDate'] = TCG.isBlank(activeOnField.getValue()) ? void 0 : activeOnField.getValue().format('m/d/Y');
		}
		return params;
	},
	getGridRowContextMenuItems: function(grid, rowIndex, record) {
		const result = [
			{
				text: 'Open Client Account',
				iconCls: 'account',
				handler: async () => {
					const clazz = 'Clifton.investment.account.AccountWindow';
					const id = TCG.getValue('target.clientAccount.id', record.json);
					await TCG.createComponent(clazz, {
						id: TCG.getComponentId(clazz, id),
						params: {id: id},
						openerCt: grid
					});
				}
			}, {
				text: 'Open Replication',
				iconCls: 'replicate',
				disabled: TCG.isBlank(TCG.getValue('target.replication.id', record.json)),
				handler: async () => {
					const clazz = 'Clifton.investment.replication.ReplicationWindow';
					const id = TCG.getValue('target.replication.id', record.json);
					await TCG.createComponent(clazz, {
						id: TCG.getComponentId(clazz, id),
						params: {id: id},
						openerCt: grid
					});
				}
			}
		];

		if (TCG.isTrue(grid.ownerGridPanel.includeTargetNavigation)) {
			result.splice(0, 0, {
				text: 'Open Target',
				iconCls: 'account',
				handler: async () => {
					const clazz = 'Clifton.portfolio.target.TargetWindow';
					const id = TCG.getValue('target.id', record.json);
					await TCG.createComponent(clazz, {
						id: TCG.getComponentId(clazz, id),
						params: {id: id},
						openerCt: grid
					});
				}
			});
		}
		return result;
	},
	editor: {
		detailPageClass: 'Clifton.portfolio.target.balance.TargetBalanceWindow',
		deleteURL: 'portfolioTargetBalanceDelete.json',
		endURL: 'portfolioTargetBalanceEnd.json',

		addToolbarDeleteButton: function(toolBar, gridPanel) {
			const editor = this;
			const deleteHandler = (action, deleteUrl) => {
				const sm = editor.grid.getSelectionModel();
				const confirmSuffix = action && typeof action === 'string' && 'Delete'.toUpperCase() === action.toUpperCase()
					? ' The preferred method of deactivating a Target Balance is to End it so historic target balances are available.' : '';
				if (sm.getCount() === 0) {
					TCG.showError('Please select a Target Balance to ' + action + '.', 'No Target Balance(s) Selected');
				}
				else if (sm.getCount() !== 1) {
					if (editor.allowToDeleteMultiple) {
						Ext.Msg.confirm(action + ' Selected Target Balance(s)', 'Would you like to ' + action + ' all selected Target Balance(s)?' + confirmSuffix, function(a) {
							if (a === 'yes') {
								editor.deleteSelection(sm, gridPanel, deleteUrl);
							}
						});
					}
					else {
						TCG.showError('Multi-selection is not supported yet.  Please select one Target Balance.', 'NOT SUPPORTED');
					}
				}
				else {
					Ext.Msg.confirm(action + ' Selected Target Balance', 'Would you like to ' + action + ' selected Target Balance?' + confirmSuffix, function(a) {
						if (a === 'yes') {
							editor.deleteSelection(sm, gridPanel, deleteUrl);
						}
					});
				}
			};

			toolBar.add({
				text: 'End',
				xtype: 'splitbutton',
				tooltip: 'End selected item(s) for Current Day - 1',
				iconCls: 'remove',
				handler: function() {
					deleteHandler('End', editor.endURL);
				},
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'End',
							tooltip: 'End selected item(s) for Current Day - 1',
							iconCls: 'remove',
							handler: function() {
								deleteHandler('End', editor.endURL);
							}
						},
						{
							text: 'Delete',
							tooltip: 'Delete selected item(s)',
							iconCls: 'remove',
							handler: function() {
								deleteHandler('Delete', editor.deleteURL);
							}
						}
					]
				})
			}, '-');
		}
	}
});
Ext.reg('portfolio-target-balance-grid', Clifton.portfolio.target.balance.TargetBalanceGrid);

Clifton.portfolio.target.activity.TargetActivityGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'portfolioTargetActivityListFind',
	instructions: 'A list of portfolio target balance activity capable of adjusting portfolio target balance values',
	topToolbarSearchParameter: 'searchPattern',
	requestIdDataIndex: true,
	includeTargetNavigation: true,
	columns: [
		{header: 'ID', width: 10, dataIndex: 'id', hidden: true},
		{header: 'Target', width: 50, dataIndex: 'target.label', idDataIndex: 'target.id', filter: {searchFieldName: 'targetId'}},
		{header: 'Client Account', width: 40, dataIndex: 'target.clientAccount.label', idDataIndex: 'target.clientAccount.id', filter: {searchFieldName: 'clientAccountId', type: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow'}},
		{header: 'Replication', width: 40, dataIndex: 'target.replication.label', idDataIndex: 'target.replication.id', filter: {searchFieldName: 'replicationName'}},
		{header: 'Security', width: 20, dataIndex: 'security.symbol', idDataIndex: 'security.id', filter: {searchFieldName: 'securityId', type: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'}},
		{header: 'Value', width: 20, dataIndex: 'value', type: 'currency'},
		{header: 'Note', width: 40, dataIndex: 'note', renderer: TCG.renderValueWithTooltip},
		{header: 'Cause Table', width: 40, dataIndex: 'causeSystemTable.label', idDataIndex: 'causeSystemTable.id', hidden: true, filter: {type: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded'}},
		{header: 'Cause FK ID', width: 40, dataIndex: 'causeFKFieldId', hidden: true, filter: false},
		{
			header: 'Cause', width: 15, dataIndex: 'causeSystemTable.detailScreenClass', filter: false, sortable: false,
			renderer: function(value, metadata, record) {
				if (TCG.isNotBlank(value) && TCG.isNotBlank(record.data.causeFKFieldId)) {
					return TCG.renderActionColumn('view', 'View', 'View ' + TCG.getValue('causeSystemTable.label', record.json) + ' that is linked to this freeze entry', 'ShowCause');
				}
				return 'N/A';
			},
			eventListeners: {
				'click': function(column, grid, rowIndex, event) {
					const row = grid.store.data.items[rowIndex];
					const clz = row.json.causeSystemTable.detailScreenClass;
					const id = row.json.causeFKFieldId;
					const gridPanel = grid.ownerGridPanel;
					gridPanel.editor.openDetailPage(clz, gridPanel, id, row);
				}
			}
		},
		{header: 'Start Date', width: 20, dataIndex: 'startDate'},
		{header: 'End Date', width: 20, dataIndex: 'endDate'},
		{header: 'Quantity', width: 15, dataIndex: 'valueQuantity', type: 'boolean', tooltip: 'Value is Quantity when checked, Notional when unchecked'},
		{header: 'Deleted', width: 15, dataIndex: 'deleted', type: 'boolean', tooltip: 'Indicates if this activity has been marked as deleted. For historical tracking, activity is marked as deleted instead of being deleted.'}
	],
	getTopToolbarFilters: function(toolbar) {
		return [{fieldLabel: 'Active On', xtype: 'toolbar-datefield', name: 'activeOnDate'}];
	},
	getCustomLoadParams: function(firstLoad) {
		return {};
	},
	getLoadParams: function(firstLoad) {
		const params = this.getCustomLoadParams(firstLoad);
		const activeOnField = TCG.getChildByName(this.getTopToolbar(), 'activeOnDate');
		if (firstLoad) {
			activeOnField.setValue(new Date().format('m/d/Y'));
			this.setFilterValue('deleted', false);
		}
		params['activeOnDate'] = TCG.isBlank(activeOnField.getValue()) ? void 0 : activeOnField.getValue().format('m/d/Y');
		return params;
	},
	editor: {
		detailPageClass: 'Clifton.portfolio.target.activity.TargetActivityWindow',
		deleteURL: 'portfolioTargetActivityDelete.json',
		deleteButtonTooltip: 'Mark the selected activity row as deleted. For historical tracking, activity is not deleted.'
	},
	getGridRowContextMenuItems: function(grid, rowIndex, record) {
		const result = [
			{
				text: 'Open Client Account',
				iconCls: 'account',
				handler: async () => {
					const clazz = 'Clifton.investment.account.AccountWindow';
					const id = TCG.getValue('target.clientAccount.id', record.json);
					await TCG.createComponent(clazz, {
						id: TCG.getComponentId(clazz, id),
						params: {id: id},
						openerCt: grid
					});
				}
			}, {
				text: 'Open Replication',
				iconCls: 'replicate',
				disabled: TCG.isBlank(TCG.getValue('target.replication.id', record.json)),
				handler: async () => {
					const clazz = 'Clifton.investment.replication.ReplicationWindow';
					const id = TCG.getValue('target.replication.id', record.json);
					await TCG.createComponent(clazz, {
						id: TCG.getComponentId(clazz, id),
						params: {id: id},
						openerCt: grid
					});
				}
			}
		];

		if (TCG.isTrue(grid.ownerGridPanel.includeTargetNavigation)) {
			result.splice(0, 0, {
				text: 'Open Target',
				iconCls: 'account',
				handler: async () => {
					const clazz = 'Clifton.portfolio.target.TargetWindow';
					const id = TCG.getValue('target.id', record.json);
					await TCG.createComponent(clazz, {
						id: TCG.getComponentId(clazz, id),
						params: {id: id},
						openerCt: grid
					});
				}
			});
		}
		return result;
	}
});
Ext.reg('portfolio-target-activity-grid', Clifton.portfolio.target.activity.TargetActivityGrid);

Clifton.investment.account.securitytarget.SecurityTargetListWindowAdditionalTabs.push(
	{
		title: 'Portfolio Targets',
		reloadOnTabChange: true,
		items: [{xtype: 'portfolio-target-grid'}]
	},
	{
		title: 'Portfolio Target Balances',
		reloadOnTabChange: true,
		items: [{xtype: 'portfolio-target-balance-grid'}]
	},
	{
		title: 'Portfolio Target Activity',
		reloadOnTabChange: true,
		items: [{xtype: 'portfolio-target-activity-grid'}]
	},
	{
		title: 'Portfolio Target Activity Type',
		items: [
			{
				xtype: 'gridpanel',
				name: 'portfolioTargetActivityTypeListFind',
				instructions: 'A list of portfolio target balance activity types',
				topToolbarSearchParameter: 'searchPattern',
				readOnly: true,
				columns: [
					{header: 'ID', width: 10, dataIndex: 'id', hidden: true},
					{header: 'Name', width: 30, dataIndex: 'name'},
					{header: 'Description', width: 50, dataIndex: 'description'},
					{header: 'Security Required', width: 10, dataIndex: 'securityRequired', type: 'boolean'},
					{header: 'Note Required', width: 10, dataIndex: 'noteRequired', type: 'boolean'},
					{header: 'Cause Required', width: 10, dataIndex: 'causeRequired', type: 'boolean'},
					{header: 'Position', width: 10, dataIndex: 'position', type: 'boolean'},
					{header: 'MOC', width: 10, dataIndex: 'marketOnClose', type: 'boolean'},
					{header: 'System Defined', width: 10, dataIndex: 'systemDefined', type: 'boolean'}
				],
				editor: {
					detailPageClass: 'Clifton.portfolio.target.activity.TargetActivityTypeWindow',
					drillDownOnly: true
				}
			}
		]
	}
);

const clientAccountTargetManagerGrid = Clifton.investment.account.CliftonAccountPortfolioTabs['PORTFOLIO_TARGET'].find(tab => tab.title === 'Managers');
if (clientAccountTargetManagerGrid) {
	// Override manager grid import
	const managerGrid = clientAccountTargetManagerGrid.items.find(item => item.xtype === 'investment-manager-assignment-by-client-account-grid');
	if (managerGrid) {
		managerGrid['importTableName'] = [
			{table: 'PortfolioInvestmentManagerAccountAllocation', label: 'Manager Allocations', importComponentName: 'Clifton.portfolio.target.manager.upload.TargetAccountAllocationUploadWindow'}
		];
	}
}


Clifton.investment.account.CliftonAccountPortfolioTabs['PORTFOLIO_TARGET'].push(
	{
		title: 'Targets',
		items: [{
			name: 'portfolioTargetListByClientAccount',
			xtype: 'treegrid',
			instructions: 'The following targets are defined for this investment account.',
			importTableName: [
				{table: 'PortfolioTarget', label: 'Portfolio Targets', importComponentName: 'Clifton.portfolio.target.upload.TargetUploadWindow'},
				{table: 'PortfolioTargetBalance', label: 'Portfolio Target Balances', importComponentName: 'Clifton.portfolio.target.upload.TargetBalanceUploadWindow'}
			],

			selModel: new Ext.tree.MultiSelectionModel(),

			prepareResultData: function(nodeConfig) {
				// Add listeners to disable the toggle of collapsing and expanding nodes on dblclick
				nodeConfig.listeners = {
					beforedblclick: function(node, event) {
						node.leaf = true;
					},
					dblclick: function(node, event) {
						node.leaf = node.childNodes.length < 1;
					}
				};
				return nodeConfig;
			},

			getLoadParams: function(firstLoad) {
				const activeOnField = TCG.getChildByName(this.getTopToolbar(), 'activeOnDate');
				if (TCG.isTrue(firstLoad)) {
					activeOnField.setValue(new Date().format('m/d/Y'));
					this.getLoader().on('load', (treeLoader, node) => node.getOwnerTree().expandAll());
				}
				return {
					clientAccountId: this.getWindow().getMainFormId(),
					activeOnDate: (TCG.isBlank(activeOnField.getValue()) ? void 0 : activeOnField.getValue().format('m/d/Y')),
					populateChildren: true,
					requestedPropertiesRoot: 'data',
					requestedProperties: 'id|identity|label|name|matchingTarget.name|matchingTarget.id|replication.id|replication.name|targetCalculatorBean.name|targetPercent|startDate|endDate|order|rollup|active|leaf|children'
				};
			},

			addTopToolbarItems: function(toolBar) {
				const treeGrid = this;
				const menu = new Ext.menu.Menu({
					items: [
						{
							text: 'Portfolio Setup',
							handler: function() {
								treeGrid.openCustomFieldWindow('Portfolio Setup');
							}
						}, '-',
						{
							text: 'Account Liability',
							handler: function() {
								treeGrid.openCustomFieldWindow('Account Liability');
							}
						}, '-',
						{
							text: 'Performance Summary Setup',
							handler: function() {
								treeGrid.openCustomFieldWindow('Performance Summary Fields');
							}
						}
					]
				});

				toolBar.add({
					text: 'Setup',
					tooltip: 'Account Setup',
					iconCls: 'account',
					menu: menu
				}, '-');

				toolBar.add('->',
					{xtype: 'tbtext', text: '&nbsp;Active On:&nbsp;'},
					{
						xtype: 'datefield', name: 'activeOnDate', width: 80,
						listeners: {
							select: function(field) {
								TCG.getParentByClass(field, TCG.tree.TreeGrid).reload();
							},
							specialkey: function(field, e) {
								if (e.getKey() === e.ENTER) {
									TCG.getParentByClass(field, TCG.tree.TreeGrid).reload();
								}
							}
						}
					}
				);

			},
			openCustomFieldWindow: function(groupName) {
				const accountId = this.getWindow().getMainFormId();
				Clifton.investment.account.openCliftonAccountCustomFieldWindow(groupName, accountId, 'PORTFOLIO_TARGET');
			},
			columns: [
				{header: 'Target', width: 175, dataIndex: 'name', idDataIndex: 'id'},
				// Parent does not render due to circular reference, so excluded
				{header: 'Matching Target', width: 175, dataIndex: 'matchingTarget.name', idDataIndex: 'matchingTarget.id', filter: {searchFieldName: 'parentId'}},
				{header: 'Replication', width: 275, dataIndex: 'replication.name', filter: {searchFieldName: 'replicationName'}},
				{header: 'Target Calculator', width: 175, dataIndex: 'targetCalculatorBean.name', filter: {searchFieldName: 'targetCalculatorBeanName'}},
				{header: 'Target Percent', width: 100, type: 'percent', dataIndex: 'targetPercent'},
				{header: 'Start Date', width: 65, dataIndex: 'startDate'},
				{header: 'End Date', width: 65, dataIndex: 'endDate'},
				{header: 'Order', width: 50, dataIndex: 'order', type: 'int', useNull: true, tooltip: 'Sort order used to display targets'},
				{header: 'Rollup', width: 50, dataIndex: 'rollup', type: 'boolean'},
				{header: 'Private', width: 50, dataIndex: 'privateTarget', type: 'boolean', tooltip: 'Private Targets are used for portfolio processing, but are excluded from reports'}
			],
			getContextMenuItems: function(node, eventObj) {
				const menuItems = [
					{
						text: 'Add Target', iconCls: 'account', handler: function() {
							node.getOwnerTree().editor.openWindowFromContextMenu(node.getOwnerTree(), node);
						}
					},
					{
						text: 'Add Child Target', iconCls: 'hierarchy', handler: function() {
							node.getOwnerTree().editor.openWindowFromContextMenu(node.getOwnerTree(), node, true);
						}
					}
				];
				if (!node.leaf) {
					const childCount = getChildCount(node.attributes);
					const childThreshold = 500;
					menuItems.push('-', {
						text: 'Fully Expand',
						iconCls: 'expand-all',
						...childCount > childThreshold ? {
							tooltip: `The number of child nodes (${childCount}) is too great to fully expand this node. The maximum number of children for full expansion is ${childThreshold}.`,
							disabled: true
						} : {
							tooltip: `Full expand all ${childCount} descendants of this node.`
						},
						handler: () => node.expand(true, false)
					}, {
						text: 'Fully Collapse',
						iconCls: 'collapse-all',
						handler: () => node.collapse(true, false)
					});
				}
				return menuItems;

				function getChildCount(entity) {
					const children = entity.children || [];
					return children.length + children.reduce((count, child) => count + getChildCount(child), 0);
				}
			},
			editor: {
				detailPageClass: 'Clifton.portfolio.target.TargetWindow',
				deleteURL: 'portfolioTargetDelete.json',
				endURL: 'portfolioTargetEnd.json',
				addToolbarDeleteButton: function(toolBar, treeGrid) {
					const editor = this;
					const deleteHandler = (action, deleteUrl) => {
						const sm = editor.tree.getSelectionModel();
						const confirmSuffix = action && typeof action === 'string' && 'Delete'.toUpperCase() === action.toUpperCase()
							? ' The preferred method of deactivating a Target is to End it so historic targets are available.' : '';
						const selections = sm.getSelectedNodes();
						if (TCG.isNull(selections) || selections.length < 1) {
							TCG.showError('Please select at least one Target to ' + action + '.', 'No Target(s) Selected');
						}
						else {
							Ext.Msg.confirm(action + ' Selected Target(s)', 'Would you like to ' + action + ' selected Target(s)?' + confirmSuffix, function(a) {
								if (a === 'yes') {
									const results = selections.map(selection =>
										TCG.data.getDataPromise(deleteUrl, editor.tree, {
											waitTarget: editor.tree,
											waitMsg: action + 'ing...',
											params: {id: selection.id},
											conf: {rowId: selection.attributes.identity}
										}));
									Promise.all(results)
										.then(result => treeGrid.reload());
								}
							});
						}
					};

					toolBar.add({
						text: 'End',
						xtype: 'splitbutton',
						tooltip: 'End selected item(s) for Current Day - 1',
						iconCls: 'remove',
						handler: function() {
							deleteHandler('End', editor.endURL);
						},
						menu: new Ext.menu.Menu({
							items: [
								{
									text: 'End',
									tooltip: 'End selected item(s) for Current Day - 1',
									iconCls: 'remove',
									handler: function() {
										deleteHandler('End', editor.endURL);
									}
								},
								{
									text: 'Delete',
									tooltip: 'Delete selected item(s)',
									iconCls: 'remove',
									handler: function() {
										deleteHandler('Delete', editor.deleteURL);
									}
								}
							]
						})
					}, '-');
				},
				getDefaultData: function(treeGrid) {
					return {clientAccount: treeGrid.getWindow().getMainForm().formValues};
				},
				openWindowFromContextMenu: function(treeGrid, node, nodeAsParent) {
					const clazz = this.getDetailPageClass(treeGrid, node);
					if (clazz) {
						const defaultData = this.getDefaultData(treeGrid);
						if (TCG.isTrue(nodeAsParent)) {
							defaultData['parent'] = node.attributes;
						}
						TCG.createComponent(clazz, {
							defaultData: defaultData,
							openerCt: treeGrid
						});
					}
				},
				addEditButtons: function(toolBar) {
					const treeGrid = this.tree;

					toolBar.add({
						iconCls: 'expand-all',
						tooltip: 'Expand or Collapse all items',
						scope: treeGrid,
						handler: function() {
							treeGrid.collapsed = !treeGrid.collapsed;
							if (TCG.isTrue(treeGrid.collapsed)) {
								treeGrid.collapseAll();
							}
							else {
								treeGrid.expandAll();
							}
						}
					}, '-');
					this.addToolbarAddButton(toolBar, treeGrid);
					this.addToolbarDeleteButton(toolBar, treeGrid);
					toolBar.add({
						iconCls: 'run',
						text: 'Edit Target(s)',
						tooltip: 'Edit selected Targets in a new window supporting column edits',
						scope: treeGrid,
						handler: function() {
							const sm = treeGrid.getSelectionModel();
							const selections = sm.getSelectedNodes();
							if (TCG.isNull(selections) || selections.length < 1) {
								TCG.showError('Please select at least one Target to edit.', 'No Target(s) Selected');
							}
							else {
								const selectionRows = selections.map(selection => selection.attributes);
								const defaultData = {
									clientAccount: {
										id: this.getWindow().getMainFormId(),
										label: this.getWindow().getMainForm().formValues.label
									},
									targetList: selectionRows
								};
								TCG.createComponent('Clifton.portfolio.target.TargetUpdateWindow', {
									defaultData: defaultData,
									defaultDataIsReal: true,
									openerCt: treeGrid
								});
							}
						}
					}, '-');
				}
			}
		}]
	}
);

Clifton.investment.manager.assignment.MANAGER_ASSIGNMENT_FORM_PROCESSING_TYPE_OVERRIDES['PORTFOLIO_TARGET'] = {
	url: 'portfolioTargetInvestmentManagerAccountAssignment.json',
	loadValidation: false,
	labelFieldName: 'managerAccountAssignment.label',

	getDefaultData: function(window) {
		const managerAccountAssignment = window.defaultData || {};
		return {managerAccountAssignment: managerAccountAssignment};
	},
	getWarningMessage: function(f) {
		return f.findField('managerAccountAssignment.inactive').getValue() ? 'This manager assignment is currently inactive.' : undefined;
	},
	commonAssignmentItems: [
		{
			xtype: 'fieldset',
			title: 'Account Info',
			items: [
				{name: 'managerAccountAssignment.id', xtype: 'hidden'},
				{name: 'managerAccountAssignment.referenceTwo.businessClient.id', xtype: 'hidden', submitValue: false},
				{
					fieldLabel: 'Client Account', name: 'managerAccountAssignment.referenceTwo.label', hiddenName: 'managerAccountAssignment.referenceTwo.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow',
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						queryEvent.combo.store.setBaseParam('clientIdOrRelatedClient', TCG.getValue('managerAccountAssignment.referenceTwo.businessClient.id', f.formValues));
						queryEvent.combo.store.setBaseParam('serviceProcessingType', f.serviceProcessingType);
					},
					getDefaultData: function(f) { // defaults client
						return {
							client: TCG.getValue('managerAccountAssignment.referenceOne.businessClient', f.formValues)
						};
					}
				},
				{
					fieldLabel: 'Manager Account', name: 'managerAccountAssignment.referenceOne.label', hiddenName: 'managerAccountAssignment.referenceOne.id', xtype: 'combo', displayField: 'label', url: 'investmentManagerAccountListFind.json', detailPageClass: 'Clifton.investment.manager.AccountWindow',
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						queryEvent.combo.store.setBaseParam('clientIdOrRelatedClient', TCG.getValue('managerAccountAssignment.referenceTwo.businessClient.id', f.formValues));
					},
					getDefaultData: function(f) { // defaults client
						return {
							client: TCG.getValue('managerAccountAssignment.referenceTwo.businessClient', f.formValues)
						};
					}
				},
				{fieldLabel: 'Display Name', name: 'managerAccountAssignment.displayName'},
				{
					boxLabel: 'Inactive', xtype: 'checkbox', name: 'managerAccountAssignment.inactive',
					listeners: {
						check: function(f) {
							const p = TCG.getParentFormPanel(f);
							p.disableInactiveFields();
						}
					}
				}
			]
		}
	],
	serviceProcessingTypeItems: [
		{
			xtype: 'formgrid',
			title: 'Target Allocations for Manager Balance',
			storeRoot: 'targetAllocationList',
			name: 'allocationFormGrid',
			dtoClass: 'com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountAllocation',
			viewConfig: {forceFit: true},
			addToolbarButtons: function(toolBar, grid) {
				toolBar.add('->');
				toolBar.add({boxLabel: 'Do Not Enforce 100% Allocation', name: 'managerAccountAssignment.allowPartialAllocation', xtype: 'checkbox'});
			},
			columnsConfig: [
				{
					header: 'Target', width: 400, dataIndex: 'target.name', idDataIndex: 'target.id',
					editor: {
						xtype: 'combo', allowBlank: false, url: 'portfolioTargetListFind.json',
						beforequery: function(queryEvent) {
							const grid = queryEvent.combo.gridEditor.containerGrid;
							const f = TCG.getParentFormPanel(grid).getForm();
							const s = queryEvent.combo.store;
							s.setBaseParam('clientAccountId', f.findField('managerAccountAssignment.referenceTwo.id').value);
							s.setBaseParam('active', true);
						}
					}
				},
				{
					header: 'Percentage', width: 150, dataIndex: 'allocationPercent', type: 'percent', numberFormat: '0,000.0000000000', summaryType: 'sum', editor: {xtype: 'currencyfield', decimalPrecision: 10, allowBlank: false},
					tooltip: 'Manager Asset Class Allocation'
				},
				{
					header: 'Private', width: 60, type: 'boolean', dataIndex: 'privateAssignment', editor: {xtype: 'checkbox'},
					tooltip: 'Included in all calculations for Portfolio runs, but excluded in the client facing Portfolio report.'
				}
			],
			plugins: {ptype: 'gridsummary'}
		}
	]
};
