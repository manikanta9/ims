package com.clifton.portfolio.target;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.Set;


/**
 * The <code>PortfolioProjectBasicTests</code> ...
 *
 * @author nickk
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PortfolioTargetProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "portfolio-target";
	}


	@Override
	protected void configureApprovedPackageNames(@SuppressWarnings("unused") Set<String> approvedList) {
		approvedList.add("activity");
	}


	@Override
	protected void configureDTOSkipPropertyNames(Set<String> skipPropertyNames) {
		skipPropertyNames.add("customColumns");
	}


	@Override
	protected boolean isMethodSkipped(@SuppressWarnings("unused") Method method) {
		switch (method.getName()) {
			case "deletePortfolioTargetActivity":
			case "getPortfolioTargetBalance":
			case "getPortfolioTargetRunManagerAccountList":
			case "getPortfolioTargetRunReplicationList": {
				return true;
			}
			default: {
				return false;
			}
		}
	}
}
