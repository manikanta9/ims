SELECT 'SystemBeanPropertyType' AS entityTableName, SystemBeanPropertyTypeID AS entityID
FROM SystemBeanPropertyType
WHERE SystemBeanTypeID IN (SELECT SystemBeanTypeID FROM SystemBeanType WHERE SystemBeanTypeName IN ('Portfolio Target Manual Override Calculator', 'Portfolio Run Target Balance Calculator'))
UNION
SELECT 'SystemBean' AS entityTableName, SystemBeanID AS entityID
FROM SystemBean
WHERE SystemBeanName IN ('Portfolio Target Rollup Calculator', 'Portfolio Target Inherited Children Calculator', 'Matching Target Calculator')
UNION
SELECT 'InvestmentAccount' AS entityTableName, InvestmentAccountID AS entityID
FROM InvestmentAccount
WHERE InvestmentAccountID = 4908
UNION
SELECT 'SystemTable' AS entityTableName, SystemTableID AS entityID
FROM SystemTable
WHERE SystemTableID IN (167)
UNION
SELECT 'WorkflowState' AS entityTableName, WorkflowStateID AS entityID
FROM WorkflowState
WHERE WorkflowID IN (21)
UNION
SELECT 'WorkflowAssignment' AS entityTableName, WorkflowAssignmentID AS entityID
FROM WorkflowAssignment
WHERE WorkflowID IN (21)
UNION
SELECT 'WorkflowTransition' AS entityTableName, WorkflowTransitionID AS entityID
FROM WorkflowTransition
WHERE EndWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = 21)
   OR StartWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = 21)
UNION
SELECT 'RuleViolationStatus' AS entityTableName, RuleViolationStatusID AS entityID
FROM RuleViolationStatus
WHERE RuleViolationStatusID = 1

