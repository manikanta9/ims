package com.clifton.portfolio.target.run.process.calculator;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.PortfolioTargetService;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;
import com.clifton.portfolio.target.search.PortfolioTargetSearchForm;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.rule.violation.status.RuleViolationStatusService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * These tests validate the logic in the {@link PortfolioRunTargetBalanceCalculator} and the {@link com.clifton.portfolio.target.calculator.PortfolioTargetCalculator}s that it
 * invokes to ultimately generate {@link com.clifton.portfolio.target.balance.PortfolioTargetBalance}s.
 *
 * @author mitchellf
 */
@ContextConfiguration("classpath:com/clifton/portfolio/target/PortfolioTargetInMemoryDatabaseTests-context.xml")
@Transactional
public class PortfolioRunTargetBalanceCalculatorTests extends BaseInMemoryDatabaseTests {

	@Resource
	private PortfolioTargetService portfolioTargetService;
	@Resource
	private SystemBeanService systemBeanService;
	@Resource
	private InvestmentAccountService investmentAccountService;
	@Resource
	private PortfolioRunService portfolioRunService;
	@Resource
	private WorkflowDefinitionService workflowDefinitionService;
	@Resource
	private RuleViolationStatusService ruleViolationStatusService;

	public static final String BEAN_TYPE_NAME_RUN_TARGET_BALANCE = "Portfolio Run Target Balance Calculator";
	public static final String TARGET_CALCULATOR_BEAN_NAME_ROLLUP = "Portfolio Target Rollup Calculator";
	public static final String TARGET_CALCULATOR_BEAN_NAME_INHERITED = "Portfolio Target Inherited Children Calculator";
	public static final String TARGET_CALCULATOR_BEAN_NAME_MANUAL = "Portfolio Target Manual Override Calculator";
	public static final String TARGET_CALCULATOR_BEAN_NAME_MATCHING = "Matching Target Calculator";
	public static final String ADEPT_9_ACCT_NUMBER = "051250";
	public static final Date RUN_DATE = DateUtils.toDate("12/22/2020");

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testRollupTargetBalances() {
		createTargetHierarchyRollups();

		PortfolioTargetRunConfig runConfig = createPortfolioRun();

		PortfolioRunTargetBalanceCalculator balanceCalculatorInstance = (PortfolioRunTargetBalanceCalculator) this.systemBeanService.getBeanInstance(this.systemBeanService.getSystemBean(createCalculatorBean().getId()));
		balanceCalculatorInstance.processStep(runConfig);

		// validate that config now contains the correct balances for each node
		Map<String, BigDecimal> expectedResults = new HashMap<>();
		expectedResults.put("Node A", new BigDecimal("1000"));
		expectedResults.put("Node D", new BigDecimal("700"));

		validateResults(expectedResults, runConfig);
	}


	@Test
	public void testInheritedTargetBalances() {
		createTargetHierarchyInherited();

		PortfolioTargetRunConfig runConfig = createPortfolioRun();

		PortfolioRunTargetBalanceCalculator balanceCalculatorInstance = (PortfolioRunTargetBalanceCalculator) this.systemBeanService.getBeanInstance(this.systemBeanService.getSystemBean(createCalculatorBean().getId()));
		balanceCalculatorInstance.processStep(runConfig);

		// validate that config now contains the correct balances for each node
		Map<String, BigDecimal> expectedResults = new HashMap<>();
		expectedResults.put("Node B", new BigDecimal("250"));
		expectedResults.put("Node C", new BigDecimal("250"));
		expectedResults.put("Node D", new BigDecimal("500"));
		expectedResults.put("Node E", new BigDecimal("250"));
		expectedResults.put("Node F", new BigDecimal("250"));

		validateResults(expectedResults, runConfig);
	}


	@Test
	public void testMatchingTargetBalances() {
		createTargetHierarchyMatching();

		PortfolioTargetRunConfig runConfig = createPortfolioRun();

		PortfolioRunTargetBalanceCalculator balanceCalculatorInstance = (PortfolioRunTargetBalanceCalculator) this.systemBeanService.getBeanInstance(this.systemBeanService.getSystemBean(createCalculatorBean().getId()));
		balanceCalculatorInstance.processStep(runConfig);

		// validate that config now contains the correct balances for each node
		Map<String, BigDecimal> expectedResults = new HashMap<>();
		expectedResults.put("Node B", new BigDecimal("250"));

		validateResults(expectedResults, runConfig);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemBean createCalculatorBean() {
		SystemBeanType type = this.systemBeanService.getSystemBeanTypeByName(BEAN_TYPE_NAME_RUN_TARGET_BALANCE);
		SystemBean calculatorBean = new SystemBean();
		calculatorBean.setType(type);
		calculatorBean.setName("Default Target Balance Calculator Bean");
		calculatorBean.setDescription("Default Target Balance Calculator Bean");

		List<SystemBeanPropertyType> propertyTypes = this.systemBeanService.getSystemBeanPropertyTypeListByBean(calculatorBean);
		List<SystemBeanProperty> properties = new ArrayList<>();
		createSystemBeanProperty(properties, propertyTypes, "Do Not Execute Transactional", "true", calculatorBean);

		return this.systemBeanService.saveSystemBean(calculatorBean);
	}


	private PortfolioTargetRunConfig createPortfolioRun() {
		InvestmentAccount clientAccount = this.investmentAccountService.getInvestmentAccountByNumber(ADEPT_9_ACCT_NUMBER);

		PortfolioRun run = new PortfolioRun();
		run.setBalanceDate(RUN_DATE);
		run.setClientInvestmentAccount(clientAccount);
		run.setServiceProcessingType(clientAccount.getServiceProcessingType().getProcessingType());
		WorkflowState state = this.workflowDefinitionService.getWorkflowState((short) 163);
		run.setWorkflowState(state);
		run.setWorkflowStatus(state.getStatus());
		run.setViolationStatus(this.ruleViolationStatusService.getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.UNPROCESSED));

		this.portfolioRunService.savePortfolioRun(run);

		PortfolioTargetRunConfig runConfig = new PortfolioTargetRunConfig(run, false, 1);
		runConfig.setPortfolioTargetList(this.portfolioTargetService.getPortfolioTargetListByClientAccount(clientAccount.getId(), RUN_DATE, true));

		return runConfig;
	}


	private List<SystemBeanProperty> createSystemBeanProperty(List<SystemBeanProperty> propertyList, List<SystemBeanPropertyType> propertyTypes, String propertyTypeName, String propertyValue, SystemBean parentBean) {
		SystemBeanPropertyType propertyType = CollectionUtils.getOnlyElement(propertyTypes.stream().filter(type -> propertyTypeName.equals(type.getName())).collect(Collectors.toList()));
		SystemBeanProperty property = new SystemBeanProperty();
		property.setType(propertyType);
		property.setValue(propertyValue);
		property.setBean(parentBean);
		propertyList.add(property);
		parentBean.setPropertyList(propertyList);
		return propertyList;
	}


	/**
	 * Hierarchy -
	 * -          A
	 * -        / | \
	 * -       B  C  D
	 * -            / \
	 * -           E   F
	 */
	private List<PortfolioTarget> createTargetHierarchyRollups() {
		Date endDate = DateUtils.toDate("12/29/2020");

		SystemBean calculatorBean = this.systemBeanService.getSystemBeanByName(TARGET_CALCULATOR_BEAN_NAME_ROLLUP);
		SystemBean manualCalculatorB = createManualOverrideCalculator("100");
		SystemBean manualCalculatorC = createManualOverrideCalculator("200");
		SystemBean manualCalculatorE = createManualOverrideCalculator("300");
		SystemBean manualCalculatorF = createManualOverrideCalculator("400");

		InvestmentAccount clientAccount = this.investmentAccountService.getInvestmentAccountByNumber(ADEPT_9_ACCT_NUMBER);
		PortfolioTarget nodeA = new PortfolioTarget();
		nodeA.setName("Node A");
		nodeA.setDescription("Node A");
		nodeA.setClientAccount(clientAccount);
		nodeA.setStartDate(RUN_DATE);
		nodeA.setEndDate(endDate);
		nodeA.setRollup(true);
		nodeA.setTargetCalculatorBean(calculatorBean);
		nodeA.setTargetPercent(new BigDecimal("100"));

		PortfolioTarget nodeB = new PortfolioTarget();
		nodeB.setName("Node B");
		nodeB.setDescription("Node B");
		nodeB.setClientAccount(clientAccount);
		nodeB.setStartDate(RUN_DATE);
		nodeB.setEndDate(endDate);
		nodeB.setTargetCalculatorBean(manualCalculatorB);
		nodeB.setTargetPercent(new BigDecimal("100"));

		PortfolioTarget nodeC = new PortfolioTarget();
		nodeC.setName("Node C");
		nodeC.setDescription("Node C");
		nodeC.setClientAccount(clientAccount);
		nodeC.setStartDate(RUN_DATE);
		nodeC.setEndDate(endDate);
		nodeC.setTargetCalculatorBean(manualCalculatorC);
		nodeC.setTargetPercent(new BigDecimal("100"));

		PortfolioTarget nodeD = new PortfolioTarget();
		nodeD.setName("Node D");
		nodeD.setDescription("Node D");
		nodeD.setClientAccount(clientAccount);
		nodeD.setStartDate(RUN_DATE);
		nodeD.setEndDate(endDate);
		nodeD.setRollup(true);
		nodeD.setTargetCalculatorBean(calculatorBean);
		nodeD.setTargetPercent(new BigDecimal("100"));

		PortfolioTarget nodeE = new PortfolioTarget();
		nodeE.setName("Node E");
		nodeE.setDescription("Node E");
		nodeE.setClientAccount(clientAccount);
		nodeE.setStartDate(RUN_DATE);
		nodeE.setEndDate(endDate);
		nodeE.setTargetCalculatorBean(manualCalculatorE);
		nodeE.setTargetPercent(new BigDecimal("100"));

		PortfolioTarget nodeF = new PortfolioTarget();
		nodeF.setName("Node F");
		nodeF.setDescription("Node F");
		nodeF.setClientAccount(clientAccount);
		nodeF.setStartDate(RUN_DATE);
		nodeF.setEndDate(endDate);
		nodeF.setTargetCalculatorBean(manualCalculatorF);
		nodeF.setTargetPercent(new BigDecimal("100"));

		List<PortfolioTarget> aChildren = new ArrayList<>();
		List<PortfolioTarget> dChildren = new ArrayList<>();

		nodeB.setParent(nodeA);
		aChildren.add(nodeB);
		nodeC.setParent(nodeA);
		aChildren.add(nodeC);

		nodeE.setParent(nodeD);
		dChildren.add(nodeE);
		nodeF.setParent(nodeD);
		dChildren.add(nodeF);

		nodeD.setChildTargetList(dChildren);
		nodeD.setParent(nodeA);
		aChildren.add(nodeD);

		nodeA.setChildTargetList(aChildren);

		List<PortfolioTarget> createdTargets = new ArrayList<>();

		this.portfolioTargetService.savePortfolioTarget(nodeA);
		createdTargets.add(nodeA);
		this.portfolioTargetService.savePortfolioTarget(nodeB);
		createdTargets.add(nodeB);
		this.portfolioTargetService.savePortfolioTarget(nodeC);
		createdTargets.add(nodeC);
		this.portfolioTargetService.savePortfolioTarget(nodeD);
		createdTargets.add(nodeD);
		this.portfolioTargetService.savePortfolioTarget(nodeE);
		createdTargets.add(nodeE);
		this.portfolioTargetService.savePortfolioTarget(nodeF);
		createdTargets.add(nodeF);

		return createdTargets;
	}


	/**
	 * Hierarchy -
	 * -          A
	 * -        / | \
	 * -       B  C  D
	 * -            / \
	 * -           E   F
	 */
	private List<PortfolioTarget> createTargetHierarchyInherited() {
		Date endDate = DateUtils.toDate("12/29/2020");

		SystemBean calculatorBean = this.systemBeanService.getSystemBeanByName(TARGET_CALCULATOR_BEAN_NAME_INHERITED);
		SystemBean manualCalculator = createManualOverrideCalculator("1000");

		InvestmentAccount clientAccount = this.investmentAccountService.getInvestmentAccountByNumber(ADEPT_9_ACCT_NUMBER);
		PortfolioTarget nodeA = new PortfolioTarget();
		nodeA.setName("Node A");
		nodeA.setDescription("Node A");
		nodeA.setClientAccount(clientAccount);
		nodeA.setStartDate(RUN_DATE);
		nodeA.setEndDate(endDate);
		nodeA.setTargetCalculatorBean(manualCalculator);
		nodeA.setTargetPercent(new BigDecimal("100"));

		PortfolioTarget nodeB = new PortfolioTarget();
		nodeB.setName("Node B");
		nodeB.setDescription("Node B");
		nodeB.setClientAccount(clientAccount);
		nodeB.setStartDate(RUN_DATE);
		nodeB.setEndDate(endDate);
		nodeB.setTargetCalculatorBean(calculatorBean);
		nodeB.setTargetPercent(new BigDecimal("25"));

		PortfolioTarget nodeC = new PortfolioTarget();
		nodeC.setName("Node C");
		nodeC.setDescription("Node C");
		nodeC.setClientAccount(clientAccount);
		nodeC.setStartDate(RUN_DATE);
		nodeC.setEndDate(endDate);
		nodeC.setTargetCalculatorBean(calculatorBean);
		nodeC.setTargetPercent(new BigDecimal("25"));

		PortfolioTarget nodeD = new PortfolioTarget();
		nodeD.setName("Node D");
		nodeD.setDescription("Node D");
		nodeD.setClientAccount(clientAccount);
		nodeD.setStartDate(RUN_DATE);
		nodeD.setEndDate(endDate);
		nodeD.setTargetCalculatorBean(calculatorBean);
		nodeD.setTargetPercent(new BigDecimal("50"));

		PortfolioTarget nodeE = new PortfolioTarget();
		nodeE.setName("Node E");
		nodeE.setDescription("Node E");
		nodeE.setClientAccount(clientAccount);
		nodeE.setStartDate(RUN_DATE);
		nodeE.setEndDate(endDate);
		nodeE.setTargetCalculatorBean(calculatorBean);
		nodeE.setTargetPercent(new BigDecimal("50"));

		PortfolioTarget nodeF = new PortfolioTarget();
		nodeF.setName("Node F");
		nodeF.setDescription("Node F");
		nodeF.setClientAccount(clientAccount);
		nodeF.setStartDate(RUN_DATE);
		nodeF.setEndDate(endDate);
		nodeF.setTargetCalculatorBean(calculatorBean);
		nodeF.setTargetPercent(new BigDecimal("50"));

		List<PortfolioTarget> aChildren = new ArrayList<>();
		List<PortfolioTarget> dChildren = new ArrayList<>();

		nodeB.setParent(nodeA);
		aChildren.add(nodeB);
		nodeC.setParent(nodeA);
		aChildren.add(nodeC);

		nodeE.setParent(nodeD);
		dChildren.add(nodeE);
		nodeF.setParent(nodeD);
		dChildren.add(nodeF);

		nodeD.setChildTargetList(dChildren);
		nodeD.setParent(nodeA);
		aChildren.add(nodeD);

		nodeA.setChildTargetList(aChildren);

		List<PortfolioTarget> createdTargets = new ArrayList<>();

		this.portfolioTargetService.savePortfolioTarget(nodeA);
		createdTargets.add(nodeA);
		this.portfolioTargetService.savePortfolioTarget(nodeB);
		createdTargets.add(nodeB);
		this.portfolioTargetService.savePortfolioTarget(nodeC);
		createdTargets.add(nodeC);
		this.portfolioTargetService.savePortfolioTarget(nodeD);
		createdTargets.add(nodeD);
		this.portfolioTargetService.savePortfolioTarget(nodeE);
		createdTargets.add(nodeE);
		this.portfolioTargetService.savePortfolioTarget(nodeF);
		createdTargets.add(nodeF);

		return createdTargets;
	}


	/**
	 * Hierarchy - A is matched by sibling B
	 */
	private List<PortfolioTarget> createTargetHierarchyMatching() {
		Date endDate = DateUtils.toDate("12/29/2020");

		SystemBean matchingCalculatorBean = this.systemBeanService.getSystemBeanByName(TARGET_CALCULATOR_BEAN_NAME_MATCHING);
		SystemBean manualCalculator = createManualOverrideCalculator("1000");

		InvestmentAccount clientAccount = this.investmentAccountService.getInvestmentAccountByNumber(ADEPT_9_ACCT_NUMBER);
		PortfolioTarget nodeA = new PortfolioTarget();
		nodeA.setName("Node A to match");
		nodeA.setDescription("Node A which is matched by node B");
		nodeA.setClientAccount(clientAccount);
		nodeA.setStartDate(RUN_DATE);
		nodeA.setEndDate(endDate);
		nodeA.setTargetCalculatorBean(manualCalculator);
		nodeA.setTargetPercent(new BigDecimal("100"));

		PortfolioTarget nodeB = new PortfolioTarget();
		nodeB.setName("Node B matching node A");
		nodeB.setDescription("Node B which is matching 25% of node A");
		nodeB.setClientAccount(clientAccount);
		nodeB.setStartDate(RUN_DATE);
		nodeB.setEndDate(endDate);
		nodeB.setTargetCalculatorBean(matchingCalculatorBean);
		nodeB.setTargetPercent(new BigDecimal("25"));

		nodeB.setMatchingTarget(nodeA);

		List<PortfolioTarget> createdTargets = new ArrayList<>();

		this.portfolioTargetService.savePortfolioTarget(nodeA);
		createdTargets.add(nodeA);
		this.portfolioTargetService.savePortfolioTarget(nodeB);
		createdTargets.add(nodeB);

		return createdTargets;
	}


	private SystemBean createManualOverrideCalculator(String value) {
		// Don't need the value property because this won't be used, it's just to satisfy the fk constraint
		SystemBean bean = new SystemBean();
		bean.setType(this.systemBeanService.getSystemBeanTypeByName(TARGET_CALCULATOR_BEAN_NAME_MANUAL));
		bean.setName("Manual Calculator Bean");
		bean.setDescription("Test");

		List<SystemBeanProperty> properties = new ArrayList<>();
		List<SystemBeanPropertyType> propertyTypes = this.systemBeanService.getSystemBeanPropertyTypeListByBean(bean);

		createSystemBeanProperty(properties, propertyTypes, "Balance Value", value, bean);

		return this.systemBeanService.saveSystemBean(bean);
	}


	private void validateResults(Map<String, BigDecimal> expectedResults, PortfolioTargetRunConfig runConfig) {
		expectedResults.forEach((targetName, val) -> {
			PortfolioTargetSearchForm sf = new PortfolioTargetSearchForm();
			sf.setName(targetName);
			PortfolioTarget node = CollectionUtils.getOnlyElement(this.portfolioTargetService.getPortfolioTargetList(sf));
			ValidationUtils.assertTrue(MathUtils.isEqual(runConfig.getPortfolioTargetBalance(node).getValue(), val), targetName + " did not have expected balance value");
		});
	}
}
