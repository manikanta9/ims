package com.clifton.portfolio.target.calculator;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author nickk
 */
public class PortfolioTargetInheritedChildrenCalculatorTests extends BasePortfolioTargetCalculatorTests {

	private static final int PARENT_TARGET_ID = 6;
	private static final int CHILD_TARGET_ID = 7;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testInheritedPortfolioTarget_NoParent() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(CHILD_TARGET_ID);
		TestUtils.expectException(ValidationException.class, () -> targetCalculator.calculate(target, setupRunConfig()), "Cannot apply inherited balances to a target with no parent");
	}


	@Test
	public void testInheritedPortfolioTarget_Parent_NoBalance() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(CHILD_TARGET_ID);
		target.setParent(this.portfolioTargetService.getPortfolioTarget(PARENT_TARGET_ID));
		TestUtils.expectException(ValidationException.class, () -> targetCalculator.calculate(target, setupRunConfig()), "Target calculation as percentage of parent failed to find a target balance for parent target: null");
	}


	@Test
	public void testInheritedPortfolioTarget_ParentBalance() {
		testInheritedPortfolioTarget_ParentBalance(false);
	}


	@Test
	public void testInheritedPortfolioTarget_ParentBalance_MOC() {
		testInheritedPortfolioTarget_ParentBalance(false);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected PortfolioTargetCalculator setupCalculator() {
		PortfolioTargetInheritedChildrenCalculator targetCalculator = new PortfolioTargetInheritedChildrenCalculator();
		targetCalculator.setPortfolioTargetBalanceService(this.portfolioTargetBalanceService);
		return targetCalculator;
	}


	private void testInheritedPortfolioTarget_ParentBalance(boolean moc) {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(CHILD_TARGET_ID);
		PortfolioTarget parent = this.portfolioTargetService.getPortfolioTarget(PARENT_TARGET_ID);
		PortfolioTargetBalance parentBalance = addTargetBalance(parent, DateUtils.addDays(BALANCE_DATE, -1), "1000");
		if (moc) {
			Date dayBeforeBalanceDate = DateUtils.addDays(BALANCE_DATE, -1);
			parentBalance.addPortfolioTargetActivity(addMocTargetActivity(parent, DateUtils.addDays(dayBeforeBalanceDate, -1), dayBeforeBalanceDate, "200"));
		}
		try {
			PortfolioTargetRunConfig runConfig = setupRunConfig();
			runConfig.addPortfolioTargetBalance(parentBalance);
			target.setParent(parent);
			PortfolioTargetBalance balance = targetCalculator.calculate(target, runConfig);
			Assertions.assertTrue(MathUtils.isEqual(new BigDecimal(moc ? "600" : "500"), moc ? balance.getMarketOnCloseValue() : balance.getAdjustedValue()));
		}
		finally {
			this.portfolioTargetBalanceService.deletePortfolioTargetBalance(parentBalance.getId());
			CollectionUtils.getIterable(parentBalance.getTargetActivityList())
					.forEach(activity -> this.portfolioTargetActivityService.deletePortfolioTargetActivity(activity.getId()));
		}
	}
}
