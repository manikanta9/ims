package com.clifton.portfolio.target.calculator;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Tests logic in the {@link PortfolioTargetMatchingTargetCalculator}.
 *
 * @author michalm
 */
public class PortfolioTargetMatchingCalculatorTests extends BasePortfolioTargetCalculatorTests {

	private static final int TARGET_ID = 1;
	private static final int TARGET_50_PERCENT_ID = 7;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Tests both target validation and observer/validator validation.
	 */
	@Test
	public void testMatchingPortfolioTarget_ParentMatchingTargetNotAllowed() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget parentMatchingTarget = this.portfolioTargetService.getPortfolioTarget(TARGET_ID);
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(TARGET_50_PERCENT_ID);
		target.setMatchingTarget(parentMatchingTarget);
		target.setParent(parentMatchingTarget); // matching ancestors is not allowed
		try {
			TestUtils.expectException(ValidationException.class, () -> targetCalculator.validateTarget(target), String.format("Target [%s] cannot have Matching Target [%s] because matching an ancestor Target is not allowed. There are other features that allow a Target to take a portion of an ancestor's balance.",
					target.getLabel(), target.getMatchingTarget().getLabel()));
			TestUtils.expectException(ValidationException.class, () -> this.portfolioTargetService.savePortfolioTarget(target), String.format("Target [%s] cannot have Matching Target [%s] because matching an ancestor Target is not allowed. There are other features that allow a Target to take a portion of an ancestor's balance.",
					target.getLabel(), target.getMatchingTarget().getLabel()));
		}
		finally {
			target.setParent(null);
			target.setMatchingTarget(null);
			this.portfolioTargetService.savePortfolioTarget(target);
		}
	}


	/**
	 * Tests both target validation and observer/validator validation.
	 */
	@Test
	public void testMatchingPortfolioTarget_ChildMatchingTargetNotAllowed() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget childMatchingTarget = this.portfolioTargetService.getPortfolioTarget(TARGET_ID);
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(TARGET_50_PERCENT_ID);
		childMatchingTarget.setParent(target);
		this.portfolioTargetService.savePortfolioTarget(childMatchingTarget);
		target.setMatchingTarget(childMatchingTarget); // matching descendents is not allowed
		target.setChildTargetList(CollectionUtils.createList(childMatchingTarget));
		try {
			TestUtils.expectException(ValidationException.class, () -> targetCalculator.validateTarget(target), String.format("Target [%s] cannot have Matching Target [%s] because matching a descendent Target is not allowed. There are other features that allow a Target to take a portion of a descendents' balance.",
					target.getLabel(), target.getMatchingTarget().getLabel()));
			TestUtils.expectException(ValidationException.class, () -> this.portfolioTargetService.savePortfolioTarget(target), String.format("Target [%s] cannot have Matching Target [%s] because matching a descendent Target is not allowed. There are other features that allow a Target to take a portion of a descendents' balance.",
					target.getLabel(), target.getMatchingTarget().getLabel()));
		}
		finally {
			childMatchingTarget.setParent(null);
			this.portfolioTargetService.savePortfolioTarget(childMatchingTarget);
			target.setMatchingTarget(null);
			target.setChildTargetList(null);
			this.portfolioTargetService.savePortfolioTarget(target);
		}
	}


	/**
	 * This test does not test saving the target because the calculator would need to be saved using {@link com.clifton.system.bean.SystemBeanService}.
	 */
	@Test
	public void testMatchingPortfolioTarget_DateRangeWithinMatchingTargetDateRange() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget targetToMatch = this.portfolioTargetService.getPortfolioTarget(TARGET_ID);
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(TARGET_50_PERCENT_ID);
		target.setMatchingTarget(targetToMatch);
		target.setStartDate(DateUtils.addDays(targetToMatch.getStartDate(), -1));
		TestUtils.expectException(ValidationException.class, () -> targetCalculator.validateTarget(target), String.format("Target [%s] Start Date and End Date must fall within date range of Matching Target [%s].", target.getLabel(), target.getMatchingTarget().getLabel()));
	}


	@Test
	public void testMatchingPortfolioTarget_NoMatchingTarget() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(TARGET_50_PERCENT_ID);
		target.setMatchingTarget(null); // make sure matching is cleared
		TestUtils.expectException(ValidationException.class, () -> targetCalculator.validateTarget(target), String.format("Target [%s] is using the Matching Target Calculator but no Matching Target is specified.", target.getLabel()));
	}


	@Test
	public void testMatchingPortfolioTarget_NoBalanceForMatchingTarget() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget targetToMatch = this.portfolioTargetService.getPortfolioTarget(TARGET_ID);
		PortfolioTarget fiftyPercentTarget = this.portfolioTargetService.getPortfolioTarget(TARGET_50_PERCENT_ID);
		TestUtils.expectException(ValidationException.class, () -> {
			fiftyPercentTarget.setMatchingTarget(targetToMatch);
			targetCalculator.calculate(fiftyPercentTarget, setupRunConfig());
		}, String.format("No balance found for Matching Target [%s] while processing Target [%s].", targetToMatch.getLabel(), fiftyPercentTarget.getLabel()));
	}


	@Test
	public void testMatchingPortfolioTarget_MatchFiftyPercent() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget targetToMatch = this.portfolioTargetService.getPortfolioTarget(TARGET_ID);
		PortfolioTarget fiftyPercentTarget = this.portfolioTargetService.getPortfolioTarget(TARGET_50_PERCENT_ID);
		PortfolioTargetBalance balanceToMatch = addTargetBalance(targetToMatch, BALANCE_DATE, "1000");
		try {
			PortfolioTargetRunConfig runConfig = setupRunConfig();
			runConfig.addPortfolioTargetBalance(balanceToMatch);
			fiftyPercentTarget.setMatchingTarget(targetToMatch);
			PortfolioTargetBalance balance = targetCalculator.calculate(fiftyPercentTarget, runConfig);
			BigDecimal expectedValue = new BigDecimal("500");
			AssertUtils.assertTrue(MathUtils.isEqual(expectedValue, balance.getAdjustedValue()), "Expected adjusted value %s but was %s.", expectedValue.toString(), balance.getAdjustedValue().toString());
		}
		finally {
			this.portfolioTargetBalanceService.deletePortfolioTargetBalance(balanceToMatch.getId());
		}
	}


	@Test
	public void testMatchingPortfolioTarget_MatchFiftyPercent_MOC() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget targetToMatch = this.portfolioTargetService.getPortfolioTarget(TARGET_ID);
		PortfolioTarget fiftyPercentTarget = this.portfolioTargetService.getPortfolioTarget(TARGET_50_PERCENT_ID);
		PortfolioTargetBalance balanceToMatch = addTargetBalance(targetToMatch, BALANCE_DATE, "1000");
		Date dayBeforeBalanceDate = DateUtils.addDays(BALANCE_DATE, -1);
		balanceToMatch.addPortfolioTargetActivity(addMocTargetActivity(targetToMatch, DateUtils.addDays(dayBeforeBalanceDate, -1), dayBeforeBalanceDate, "200"));
		try {
			PortfolioTargetRunConfig runConfig = setupRunConfig(true);
			runConfig.addPortfolioTargetBalance(balanceToMatch);
			fiftyPercentTarget.setMatchingTarget(targetToMatch);
			PortfolioTargetBalance balance = targetCalculator.calculate(fiftyPercentTarget, runConfig);
			BigDecimal expectedValue = new BigDecimal("600");
			AssertUtils.assertTrue(MathUtils.isEqual(expectedValue, balance.getAdjustedValue()), "Expected adjusted value %s but was %s.", expectedValue.toString(), balance.getAdjustedValue().toString());
		}
		finally {
			this.portfolioTargetBalanceService.deletePortfolioTargetBalance(balanceToMatch.getId());
			CollectionUtils.getIterable(balanceToMatch.getTargetActivityList())
					.forEach(activity -> this.portfolioTargetActivityService.deletePortfolioTargetActivity(activity.getId()));
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected PortfolioTargetCalculator setupCalculator() {
		PortfolioTargetMatchingTargetCalculator targetCalculator = new PortfolioTargetMatchingTargetCalculator();
		targetCalculator.setPortfolioTargetBalanceService(this.portfolioTargetBalanceService);
		return targetCalculator;
	}
}
