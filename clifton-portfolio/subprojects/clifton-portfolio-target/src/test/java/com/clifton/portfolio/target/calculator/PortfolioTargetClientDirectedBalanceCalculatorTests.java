package com.clifton.portfolio.target.calculator;

import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.activity.PortfolioTargetActivity;
import com.clifton.portfolio.target.activity.PortfolioTargetActivityService;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author nickk
 */
public class PortfolioTargetClientDirectedBalanceCalculatorTests extends BasePortfolioTargetCalculatorTests {

	private static final int TARGET_ID = 1;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	@Resource
	private CalendarBusinessDayService calendarBusinessDayService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testClientDirectedPortfolioTarget_NoExistingBalance() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(TARGET_ID);
		TestUtils.expectException(ValidationException.class, () -> targetCalculator.calculate(target, setupRunConfig()), "Expected one Target Balance for Portfolio Target [123456: ClientDirected with effective range [02/18/2021-]] active on 02/18/2021 but one was not found.");
	}


	@Test
	public void testClientDirectedPortfolioTarget_NoActivity() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(TARGET_ID);
		PortfolioTargetBalance existing = addTargetBalance(target, DateUtils.addDays(BALANCE_DATE, -1), "1000");
		try {
			PortfolioTargetBalance balance = targetCalculator.calculate(target, setupRunConfig());
			Assertions.assertEquals(new BigDecimal("1000"), balance.getAdjustedValue());
		}
		finally {
			this.portfolioTargetBalanceService.deletePortfolioTargetBalance(existing.getId());
		}
	}


	@Test
	public void testClientDirectedPortfolioTarget_ActivityNotApplied() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(TARGET_ID);
		Date dayBeforeBalanceDate = DateUtils.addDays(BALANCE_DATE, -1);
		PortfolioTargetBalance existing = addTargetBalance(target, dayBeforeBalanceDate, "1000");
		PortfolioTargetActivity activity = addTargetActivity(target, DateUtils.addDays(dayBeforeBalanceDate, -1), null, "100");
		try {
			PortfolioTargetBalance balance = targetCalculator.calculate(target, setupRunConfig());
			Assertions.assertEquals(new BigDecimal("1000"), balance.getAdjustedValue());
		}
		finally {
			this.portfolioTargetBalanceService.deletePortfolioTargetBalance(existing.getId());
			this.portfolioTargetActivityService.deletePortfolioTargetActivity(activity.getId());
		}
	}


	@Test
	public void testClientDirectedPortfolioTarget_ActivityApplied() {
		testClientDirectedPortfolioTarget_ActivityApplied(false);
	}


	@Test
	public void testClientDirectedPortfolioTarget_ActivityApplied_MOC() {
		testClientDirectedPortfolioTarget_ActivityApplied(true);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected PortfolioTargetCalculator setupCalculator() {
		PortfolioTargetClientDirectedBalanceCalculator targetCalculator = new PortfolioTargetClientDirectedBalanceCalculator();
		targetCalculator.setPortfolioTargetBalanceService(this.portfolioTargetBalanceService);
		targetCalculator.setPortfolioTargetActivityService(this.portfolioTargetActivityService);
		targetCalculator.setCalendarBusinessDayService(this.calendarBusinessDayService);
		return targetCalculator;
	}


	private void testClientDirectedPortfolioTarget_ActivityApplied(boolean moc) {
		PortfolioTargetClientDirectedBalanceCalculator targetCalculator = (PortfolioTargetClientDirectedBalanceCalculator) setupCalculator();
		PortfolioTargetActivityService mockedPortfolioTargetActivityService = Mockito.spy(this.portfolioTargetActivityService);
		targetCalculator.setPortfolioTargetActivityService(mockedPortfolioTargetActivityService);
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(TARGET_ID);
		Date dayBeforeBalanceDate = DateUtils.addDays(BALANCE_DATE, -1);
		PortfolioTargetBalance existing = addTargetBalance(target, dayBeforeBalanceDate, "1000");
		List<PortfolioTargetActivity> activityList = CollectionUtils.createList(
				addTargetActivity(target, DateUtils.addDays(dayBeforeBalanceDate, -1), dayBeforeBalanceDate, "100"),
				addMocTargetActivity(target, DateUtils.addDays(dayBeforeBalanceDate, -1), dayBeforeBalanceDate, "200"));
		try {
			Mockito.doReturn(activityList).when(mockedPortfolioTargetActivityService).getPortfolioTargetActivityList(Mockito.any());
			PortfolioTargetBalance resultBalance = targetCalculator.calculate(target, setupRunConfig(moc));
			Assertions.assertEquals(new BigDecimal(moc ? "1300" : "1100"), moc ? resultBalance.getMarketOnCloseValue() : resultBalance.getAdjustedValue());
		}
		finally {
			this.portfolioTargetBalanceService.deletePortfolioTargetBalance(existing.getId());
			activityList.forEach(activity -> this.portfolioTargetActivityService.deletePortfolioTargetActivity(activity.getId()));
		}
	}
}
