package com.clifton.portfolio.target.calculator;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.PortfolioTargetService;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.balance.PortfolioTargetBalanceService;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.stream.Collectors;


/**
 * @author mitchellf
 */
@ContextConfiguration("classpath:com/clifton/portfolio/target/PortfolioTargetInMemoryDatabaseTests-context.xml")
public class PortfolioTargetClientDirectedBalanceMemoryDbTests extends BaseInMemoryDatabaseTests {

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private PortfolioTargetService portfolioTargetService;

	@Resource
	private PortfolioTargetBalanceService portfolioTargetBalanceService;

	@Resource
	private PortfolioRunService portfolioRunService;


	@Resource
	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;


	@Test
	public void testTargetBalanceFollowsActual() {

		// setup bean
		SystemBean targetCalculatorBean = this.systemBeanService.getSystemBeanByName("Client Directed Balance Target Calculator Bean");
		SystemBeanPropertyType propertyType = CollectionUtils.getOnlyElement(this.systemBeanService.getSystemBeanPropertyTypeListByBean(targetCalculatorBean)
				.stream()
				.filter(type -> type.getName().equals("Use Target Follows Actual"))
				.collect(Collectors.toList()));

		SystemBeanProperty property = new SystemBeanProperty();
		property.setType(propertyType);
		property.setValue("true");
		property.setBean(targetCalculatorBean);
		targetCalculatorBean.setPropertyList(CollectionUtils.createList(property));

		this.systemBeanService.saveSystemBean(targetCalculatorBean);

		PortfolioTargetClientDirectedBalanceCalculator calculator = (PortfolioTargetClientDirectedBalanceCalculator) this.systemBeanService.getBeanInstance(targetCalculatorBean);

		// setup acct/target
		// create balance
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(112);
		PortfolioTargetBalance balance = new PortfolioTargetBalance();
		balance.setTarget(target);
		balance.setStartDate(DateUtils.toDate("12/20/2021"));
		balance.setValue(new BigDecimal("123456789"));
		balance.setTargetBalanceDescription("Test balance");

		PortfolioTargetBalance existingBalance = calculator.getExistingPortfolioTargetBalance(target, DateUtils.toDate("12/20/2021"));
		this.portfolioTargetBalanceService.replacePortfolioTargetBalance(existingBalance.getId(), balance);

		PortfolioRun run = this.portfolioRunService.getPortfolioRun(1561752);

		PortfolioTargetRunConfig config = new PortfolioTargetRunConfig(run, false, 1);
		config.setContractStore(this.portfolioAccountDataRetriever.getPortfolioAccountContractStoreActual(run.getClientInvestmentAccount(), config.getBalanceDate(), false));

		PortfolioTargetBalance newBalance = calculator.calculate(target, config);
		ValidationUtils.assertTrue(MathUtils.isEqual(newBalance.getValue(), new BigDecimal("123456789")), "Did not calculate expected balance value on day when new balance was created. Expected new balance to use value 123,456,789.");

		PortfolioRun run2 = this.portfolioRunService.getPortfolioRun(1562511);
		PortfolioTargetRunConfig config2 = new PortfolioTargetRunConfig(run2, false, 1);
		config2.setContractStore(this.portfolioAccountDataRetriever.getPortfolioAccountContractStoreActual(run2.getClientInvestmentAccount(), config2.getBalanceDate(), false));
		PortfolioTargetBalance newBalance2 = calculator.calculate(target, config2);
		ValidationUtils.assertTrue(MathUtils.isEqual(MathUtils.round(newBalance2.getValue(), 2), new BigDecimal("-99998595.24")), "Did not calculate expected balance value on day when balance should follow actual. Expected Target Follows Actual balance value to be -99998595.24");
	}
}
