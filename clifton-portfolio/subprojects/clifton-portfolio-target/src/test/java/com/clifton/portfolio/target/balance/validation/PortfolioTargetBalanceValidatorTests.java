package com.clifton.portfolio.target.balance.validation;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.portfolio.target.PortfolioTargetService;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * @author mitchellf
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("PortfolioTargetBalanceValidatorTests-context.xml")
public class PortfolioTargetBalanceValidatorTests {

	@Resource
	private PortfolioTargetService portfolioTargetService;

	@Resource
	private PortfolioTargetBalanceValidator portfolioTargetBalanceValidator;

	@Resource
	AdvancedUpdatableDAO<PortfolioTargetBalance, Criteria> portfolioTargetBalanceDAO;


	@Test
	public void testOverrideFutureBalance() {
		PortfolioTargetBalance firstBalance = new PortfolioTargetBalance();
		firstBalance.setTarget(this.portfolioTargetService.getPortfolioTarget(1));
		firstBalance.setStartDate(DateUtils.toDate("11/01/2021"));
		firstBalance.setEndDate(DateUtils.toDate("12/08/2021"));
		firstBalance.setValue(new BigDecimal("1000"));

		this.portfolioTargetBalanceDAO.save(firstBalance);

		PortfolioTargetBalance secondBalance = new PortfolioTargetBalance();
		secondBalance.setTarget(this.portfolioTargetService.getPortfolioTarget(1));
		secondBalance.setStartDate(DateUtils.toDate("12/09/2021"));
		secondBalance.setValue(new BigDecimal("2000"));

		this.portfolioTargetBalanceDAO.save(secondBalance);

		firstBalance.setEndDate(DateUtils.toDate("12/10/2021"));
		TestUtils.expectException(ValidationException.class, () -> {
			this.portfolioTargetBalanceValidator.validate(firstBalance, DaoEventTypes.UPDATE, this.portfolioTargetBalanceDAO);
		}, "There is an existing balance for this target that starts after the balance you are updating. You cannot override a balance that starts at a later date.");
	}


	@Test
	public void testOverridePreviousBalance() {
		PortfolioTargetBalance firstBalance = new PortfolioTargetBalance();
		firstBalance.setTarget(this.portfolioTargetService.getPortfolioTarget(1));
		firstBalance.setStartDate(DateUtils.toDate("11/01/2021"));
		firstBalance.setEndDate(DateUtils.toDate("12/08/2021"));
		firstBalance.setValue(new BigDecimal("1000"));

		this.portfolioTargetBalanceDAO.save(firstBalance);

		PortfolioTargetBalance secondBalance = new PortfolioTargetBalance();
		secondBalance.setTarget(this.portfolioTargetService.getPortfolioTarget(1));
		secondBalance.setStartDate(DateUtils.toDate("12/09/2021"));
		secondBalance.setValue(new BigDecimal("2000"));

		this.portfolioTargetBalanceDAO.save(secondBalance);

		secondBalance.setStartDate(DateUtils.toDate("12/06/2021"));
		TestUtils.expectException(UserIgnorableValidationException.class, () -> {
			this.portfolioTargetBalanceValidator.validate(secondBalance, DaoEventTypes.UPDATE, this.portfolioTargetBalanceDAO);
		}, "A Target Balance record already exists for this Client Account Target for overlapping date range");
	}
}
