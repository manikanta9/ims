package com.clifton.portfolio.target.calculator;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountAllocation;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountAssignment;
import com.clifton.portfolio.target.manager.PortfolioTargetInvestmentManagerAccountService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * @author nickk
 */
public class PortfolioTargetManagerAllocationCalculatorTests extends BasePortfolioTargetCalculatorTests {

	private static final int TARGET_ID = 2;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	@Resource
	private InvestmentManagerAccountService investmentManagerAccountService;

	@Resource
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	@Resource
	private PortfolioTargetInvestmentManagerAccountService portfolioTargetInvestmentManagerAccountService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testManagerPortfolioTarget_NoAssignment() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(TARGET_ID);
		TestUtils.expectException(ValidationException.class, () -> targetCalculator.calculate(target, setupRunConfig()), "There are no manager allocations available for the target {id=2,label=123456: Manager with effective range [02/18/2021-]}");
	}


	@Test
	public void testManagerPortfolioTarget_Assignment() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(TARGET_ID);
		InvestmentManagerAccountAssignment assignment = this.investmentManagerAccountService.getInvestmentManagerAccountAssignment(1);
		InvestmentManagerAccountAssignment assignment2 = this.investmentManagerAccountService.getInvestmentManagerAccountAssignment(2);
		saveManagerAllocation(target, assignment, "100");
		saveManagerAllocation(target, assignment2, "100");
		try {
			PortfolioTargetBalance balance = targetCalculator.calculate(target, setupRunConfig());
			Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("286980649.06"), balance.getAdjustedValue()));
		}
		finally {
			this.portfolioTargetInvestmentManagerAccountService.deletePortfolioTargetInvestmentManagerAccountAssignment(assignment.getId());
			this.portfolioTargetInvestmentManagerAccountService.deletePortfolioTargetInvestmentManagerAccountAssignment(assignment2.getId());
		}
	}


	@Test
	public void testManagerPortfolioTarget_Assignments_Run_MOC() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(TARGET_ID);
		InvestmentManagerAccountAssignment assignment3 = this.investmentManagerAccountService.getInvestmentManagerAccountAssignment(3);
		saveManagerAllocation(target, assignment3, "100");
		try {
			PortfolioTargetBalance balance = targetCalculator.calculate(target, setupRunConfig(true));
			Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("498890520.46"), balance.getAdjustedValue()));
		}
		finally {
			this.portfolioTargetInvestmentManagerAccountService.deletePortfolioTargetInvestmentManagerAccountAssignment(assignment3.getId());
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected PortfolioTargetCalculator setupCalculator() {
		PortfolioTargetManagerAllocationCalculator targetCalculator = new PortfolioTargetManagerAllocationCalculator();
		targetCalculator.setPortfolioTargetBalanceService(this.portfolioTargetBalanceService);
		targetCalculator.setPortfolioTargetInvestmentManagerAccountService(this.portfolioTargetInvestmentManagerAccountService);
		targetCalculator.setInvestmentManagerAccountBalanceService(this.investmentManagerAccountBalanceService);
		return targetCalculator;
	}


	private PortfolioTargetInvestmentManagerAccountAssignment saveManagerAllocation(PortfolioTarget target, InvestmentManagerAccountAssignment assignment, String percentage) {
		PortfolioTargetInvestmentManagerAccountAssignment targetAssignment = new PortfolioTargetInvestmentManagerAccountAssignment();
		targetAssignment.setManagerAccountAssignment(assignment);
		PortfolioTargetInvestmentManagerAccountAllocation allocation = new PortfolioTargetInvestmentManagerAccountAllocation();
		allocation.setTarget(target);
		allocation.setAllocationPercent(new BigDecimal(percentage));
		targetAssignment.setTargetAllocationList(CollectionUtils.createList(allocation));
		return this.portfolioTargetInvestmentManagerAccountService.savePortfolioTargetInvestmentManagerAccountAssignment(targetAssignment);
	}
}
