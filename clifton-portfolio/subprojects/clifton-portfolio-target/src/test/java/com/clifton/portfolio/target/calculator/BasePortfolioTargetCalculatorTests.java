package com.clifton.portfolio.target.calculator;

import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.PortfolioTargetService;
import com.clifton.portfolio.target.activity.PortfolioTargetActivity;
import com.clifton.portfolio.target.activity.PortfolioTargetActivityService;
import com.clifton.portfolio.target.activity.PortfolioTargetActivityType;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.balance.PortfolioTargetBalanceService;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @author nickk
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("PortfolioTargetCalculatorTests-context.xml")
public abstract class BasePortfolioTargetCalculatorTests {

	protected static final Date BALANCE_DATE = DateUtils.toDate("02/18/2021");

	@Resource
	protected InvestmentInstrumentService investmentInstrumentService;

	@Resource
	protected PortfolioTargetService portfolioTargetService;

	@Resource
	protected PortfolioTargetActivityService portfolioTargetActivityService;

	@Resource
	protected PortfolioTargetBalanceService portfolioTargetBalanceService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	protected abstract PortfolioTargetCalculator setupCalculator();


	protected PortfolioTargetRunConfig setupRunConfig() {
		return setupRunConfig(false);
	}


	protected PortfolioTargetRunConfig setupRunConfig(boolean moc) {
		PortfolioTargetRunConfig runConfig = new PortfolioTargetRunConfig(new PortfolioRun(), false, 1);
		runConfig.getRun().setBalanceDate(BALANCE_DATE);
		runConfig.getRun().setMarketOnCloseAdjustmentsIncluded(moc);
		return runConfig;
	}


	protected PortfolioTargetBalance addTargetBalance(PortfolioTarget target, Date startDate, String value) {
		PortfolioTargetBalance targetBalance = new PortfolioTargetBalance();
		targetBalance.setTarget(target);
		targetBalance.setStartDate(startDate);
		targetBalance.setValue(new BigDecimal(value));
		return this.portfolioTargetBalanceService.savePortfolioTargetBalance(targetBalance);
	}


	protected PortfolioTargetActivity addTargetActivity(PortfolioTarget target, Date startDate, Date endDate, String value) {
		return addTargetActivity(target, this.portfolioTargetActivityService.getPortfolioTargetActivityType((short) 1), startDate, endDate, value);
	}


	protected PortfolioTargetActivity addMocTargetActivity(PortfolioTarget target, Date startDate, Date endDate, String value) {
		return addTargetActivity(target, this.portfolioTargetActivityService.getPortfolioTargetActivityType((short) 2), startDate, endDate, value);
	}


	protected PortfolioTargetActivity addTargetActivity(PortfolioTarget target, PortfolioTargetActivityType type, Date startDate, Date endDate, String value) {
		PortfolioTargetActivity targetActivity = new PortfolioTargetActivity();
		targetActivity.setType(type);
		targetActivity.setTarget(target);
		targetActivity.setStartDate(startDate);
		targetActivity.setEndDate(endDate);
		targetActivity.setValue(new BigDecimal(value));
		targetActivity.setSecurity(this.investmentInstrumentService.getInvestmentSecurity(1927));
		return this.portfolioTargetActivityService.savePortfolioTargetActivity(targetActivity);
	}
}
