SELECT 'PortfolioRun' AS entityTableName, PortfolioRunID AS entityID
FROM PortfolioRun
WHERE PortfolioRunID IN (1562511, 1561752)
UNION
SELECT 'SystemBeanProperty' AS entityTableName, SystemBeanPropertyID AS entityID
FROM SystemBeanProperty
WHERE SystemBeanID = 6561
UNION
SELECT 'SystemBeanPropertyType' AS entityTableName, SystemBeanPropertyTypeID AS entityID
FROM SystemBeanPropertyType
WHERE SystemBeanTypeID = 604
UNION
SELECT 'PortfolioTargetBalance' AS entityTableName, PortfolioTargetBalanceID AS entityID
FROM PortfolioTargetBalance
WHERE PortfolioTargetID IN (SELECT PortfolioTargetID FROM PortfolioTarget WHERE ClientInvestmentAccountID = 2294)
UNION
SELECT 'WorkflowTransition' AS entityTableName, WorkflowTransitionID AS entityID
FROM WorkflowTransition
WHERE StartWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = 21)
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityID
FROM AccountingTransaction
WHERE ClientInvestmentAccountID = 2294
UNION
SELECT 'InvestmentReplicationAllocation' AS entityTableName, InvestmentReplicationAllocationID AS entityID
FROM InvestmentReplicationAllocation
WHERE InvestmentReplicationID = 1892
UNION
SELECT 'PortfolioTargetRunReplication' AS entityTableName, PortfolioTargetRunReplicationID AS entityID
FROM PortfolioTargetRunReplication
WHERE InvestmentReplicationID = 1868
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID AS entityID
FROM MarketDataValue
WHERE InvestmentSecurityID IN (158274, 158509, 158275);
