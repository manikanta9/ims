package com.clifton.portfolio.target.calculator;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.run.process.PortfolioTargetRunConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author nickk
 */
public class PortfolioTargetRollupCalculatorTests extends BasePortfolioTargetCalculatorTests {

	private static final int PARENT_TARGET_ID = 3;
	private static final int CHILD1_TARGET_ID = 4;
	private static final int CHILD2_TARGET_ID = 5;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testRollupPortfolioTarget_NoChildren() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(PARENT_TARGET_ID);
		TestUtils.expectException(ValidationException.class, () -> targetCalculator.calculate(target, setupRunConfig()), "A rollup target must have at least one child");
	}


	@Test
	public void testRollupPortfolioTarget_Children_NoBalance() {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget target = this.portfolioTargetService.getPortfolioTarget(PARENT_TARGET_ID);
		target.setChildTargetList(new ArrayList<>());
		target.getChildTargetList().add(this.portfolioTargetService.getPortfolioTarget(CHILD1_TARGET_ID));
		target.getChildTargetList().add(this.portfolioTargetService.getPortfolioTarget(CHILD2_TARGET_ID));
		TestUtils.expectException(ValidationException.class, () -> targetCalculator.calculate(target, setupRunConfig()), "Target rollup failed to find a target balance for target: 123456: RollupChild1 with effective range [02/18/2021-]");
	}


	@Test
	public void testRollupPortfolioTarget_ChildBalances() {
		testRollupPortfolioTarget_ChildBalances(false);
	}


	@Test
	public void testRollupPortfolioTarget_ChildBalances_MOC() {
		testRollupPortfolioTarget_ChildBalances(true);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected PortfolioTargetCalculator setupCalculator() {
		PortfolioTargetRollupCalculator targetCalculator = new PortfolioTargetRollupCalculator();
		targetCalculator.setPortfolioTargetBalanceService(this.portfolioTargetBalanceService);
		return targetCalculator;
	}


	private void testRollupPortfolioTarget_ChildBalances(boolean moc) {
		PortfolioTargetCalculator targetCalculator = setupCalculator();
		PortfolioTarget parent = this.portfolioTargetService.getPortfolioTarget(PARENT_TARGET_ID);
		PortfolioTarget child1 = this.portfolioTargetService.getPortfolioTarget(CHILD1_TARGET_ID);
		PortfolioTarget child2 = this.portfolioTargetService.getPortfolioTarget(CHILD2_TARGET_ID);
		List<PortfolioTargetBalance> balanceList = new ArrayList<>();
		balanceList.add(addTargetBalance(child1, DateUtils.addDays(BALANCE_DATE, -1), "1000"));
		balanceList.add(addTargetBalance(child2, DateUtils.addDays(BALANCE_DATE, -1), "500"));
		if (moc) {
			Date dayBeforeBalanceDate = DateUtils.addDays(BALANCE_DATE, -1);
			PortfolioTargetBalance child1Balance = balanceList.get(0);
			child1Balance.addPortfolioTargetActivity(addMocTargetActivity(child1Balance.getTarget(), DateUtils.addDays(dayBeforeBalanceDate, -1), dayBeforeBalanceDate, "200"));
		}
		try {
			PortfolioTargetRunConfig runConfig = setupRunConfig(moc);
			parent.setChildTargetList(new ArrayList<>());
			balanceList.forEach(balance -> {
				runConfig.addPortfolioTargetBalance(balance);
				parent.getChildren().add(balance.getTarget());
			});
			PortfolioTargetBalance balance = targetCalculator.calculate(parent, runConfig);
			Assertions.assertEquals(new BigDecimal(moc ? "1700" : "1500"), moc ? balance.getMarketOnCloseValue() : balance.getAdjustedValue());
		}
		finally {
			balanceList.forEach(balance -> this.portfolioTargetBalanceService.deletePortfolioTargetBalance(balance.getId()));
			CollectionUtils.getIterable(balanceList.get(0).getTargetActivityList())
					.forEach(activity -> this.portfolioTargetActivityService.deletePortfolioTargetActivity(activity.getId()));
		}
	}
}
