import com.clifton.gradle.plugin.build.registerVariant

val apiVariant = registerVariant("api", upstreamForMain = true)

dependencies {

	////////////////////////////////////////////////////////////////////////////
	////////            Main Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	// Nothing yet

	////////////////////////////////////////////////////////////////////////////
	////////            API Dependencies                                ////////
	////////////////////////////////////////////////////////////////////////////

	apiVariant.api(project(":clifton-core"))
	apiVariant.api(project(":clifton-core:clifton-core-messaging"))
	apiVariant.api(project(":clifton-security"))
	apiVariant.api(project(":clifton-system"))
	apiVariant.api(project(":clifton-workflow"))
	apiVariant.api(project(":clifton-calendar:clifton-calendar-schedule"))

	// WebJar dependencies
	apiVariant.api("org.webjars:webjars-locator:0.30")
	//TODO - create bootstrap-ims jar in nexus that includes custom css for queryBuilder, or wait for queryBuilder to support 4.0 and use sass/less to customize styles.
	apiVariant.api("org.webjars.npm:bootstrap:3.3.7")
	apiVariant.api("org.webjars.npm:jquery:3.4.1")
	//Interact.js required for 'sortable' plugin for query builder:
	apiVariant.api("org.webjars.npm:interactjs:1.2.9:")
	apiVariant.api("org.webjars.npm:jQuery-QueryBuilder:2.4.5")

	////////////////////////////////////////////////////////////////////////////
	////////            Test Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-security")))
	testFixturesApi(testFixtures(project(":clifton-system")))
	testFixturesApi(testFixtures(project(":clifton-workflow")))
}
