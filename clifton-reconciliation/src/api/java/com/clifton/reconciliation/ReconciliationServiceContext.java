package com.clifton.reconciliation;

import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.file.ReconciliationFileDefinition;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccountAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityAlias;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatus;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;
import com.clifton.reconciliation.simpletable.SimpleTable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The <code>ReconciliationServiceContext</code> is used to store data used during the import of reconciliation data.
 * The context is initialized during import and computes data necessary for processing and stores in order
 * to reduces lookups and improve overall performance.
 *
 * @author StevenF on 11/23/2016.
 */
public class ReconciliationServiceContext {

	private int filteredCount = 0;

	/**
	 * Date of the run being processed.
	 * May differ from the actual import date of an object in the case of cumulative definitions (e.g. 'Transaction Reconciliation')
	 */
	private Date runDate;

	/**
	 * Date used for the import of the reconciliation data
	 * May differ from the actual run date in the case of cumulative definitions (e.g. 'Transaction Reconciliation')
	 */
	private Date importDate;

	/**
	 * The top-level data object containing all data converted from file imported into the reconciliation system.
	 */
	private SimpleTable simpleTable;

	/**
	 * The file definition used for loading this data.
	 */
	private ReconciliationFileDefinition fileDefinition;

	private List<ReconciliationRun> reconciliationRunList = new ArrayList<>();

	/**
	 * Objects computed once for re-use during the import process.
	 */
	private Map<Integer, Status> orphanedObjectStatusMap = new HashMap<>();
	private Map<Integer, Map<String, ReconciliationRunObject>> runObjectByRunIdMap = new HashMap<>();
	private Map<String, ReconciliationInvestmentAccountAlias> accountAliasMap = new HashMap<>();
	private Map<String, ReconciliationInvestmentSecurityAlias> securityAliasMap = new HashMap<>();
	private Map<Integer, List<String>> definitionFieldMappingNamesByRunMap = new HashMap<>();

	private ReconciliationMatchStatus matchedStatus;
	private ReconciliationMatchStatus notMatchedStatus;
	private ReconciliationReconcileStatus reconciledStatus;
	private ReconciliationReconcileStatus notReconciledStatus;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationSource getSource() {
		return getFileDefinition() != null ? getFileDefinition().getSource() : null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getRunDate() {
		return ObjectUtils.coalesce(this.runDate, this.importDate);
	}


	public Date getImportDate() {
		return ObjectUtils.coalesce(this.importDate, this.runDate);
	}


	public Map<String, ReconciliationRunObject> getRunObjectMap(int runId) {
		return getRunObjectByRunIdMap().computeIfAbsent(runId, k -> new HashMap<>());
	}


	public List<ReconciliationRunObject> getRunObjectList() {
		return getRunObjectByRunIdMap().values().stream().map(Map::values).flatMap(Collection::stream).collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////
	//////////              Getter & Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public int getFilteredCount() {
		return this.filteredCount;
	}


	public void setFilteredCount(int filteredCount) {
		this.filteredCount = filteredCount;
	}


	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}


	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}


	public SimpleTable getSimpleTable() {
		return this.simpleTable;
	}


	public void setSimpleTable(SimpleTable simpleTable) {
		this.simpleTable = simpleTable;
	}


	public ReconciliationFileDefinition getFileDefinition() {
		return this.fileDefinition;
	}


	public void setFileDefinition(ReconciliationFileDefinition fileDefinition) {
		this.fileDefinition = fileDefinition;
	}


	public List<ReconciliationRun> getReconciliationRunList() {
		return this.reconciliationRunList;
	}


	public void setReconciliationRunList(List<ReconciliationRun> reconciliationRunList) {
		this.reconciliationRunList = reconciliationRunList;
	}


	public Map<Integer, List<String>> getDefinitionFieldMappingNamesByRunMap() {
		return this.definitionFieldMappingNamesByRunMap;
	}


	public void setDefinitionFieldMappingNamesByRunMap(Map<Integer, List<String>> definitionFieldMappingNamesByRunMap) {
		this.definitionFieldMappingNamesByRunMap = definitionFieldMappingNamesByRunMap;
	}


	public Map<Integer, Status> getOrphanedObjectStatusMap() {
		return this.orphanedObjectStatusMap;
	}


	public void setOrphanedObjectStatusMap(Map<Integer, Status> orphanedObjectStatusMap) {
		this.orphanedObjectStatusMap = orphanedObjectStatusMap;
	}


	public Map<Integer, Map<String, ReconciliationRunObject>> getRunObjectByRunIdMap() {
		return this.runObjectByRunIdMap;
	}


	public void setRunObjectByRunIdMap(Map<Integer, Map<String, ReconciliationRunObject>> runObjectByRunIdMap) {
		this.runObjectByRunIdMap = runObjectByRunIdMap;
	}


	public Map<String, ReconciliationInvestmentAccountAlias> getAccountAliasMap() {
		return this.accountAliasMap;
	}


	public void setAccountAliasMap(Map<String, ReconciliationInvestmentAccountAlias> accountAliasMap) {
		this.accountAliasMap = accountAliasMap;
	}


	public Map<String, ReconciliationInvestmentSecurityAlias> getSecurityAliasMap() {
		return this.securityAliasMap;
	}


	public void setSecurityAliasMap(Map<String, ReconciliationInvestmentSecurityAlias> securityAliasMap) {
		this.securityAliasMap = securityAliasMap;
	}


	public ReconciliationMatchStatus getMatchedStatus() {
		return this.matchedStatus;
	}


	public void setMatchedStatus(ReconciliationMatchStatus matchedStatus) {
		this.matchedStatus = matchedStatus;
	}


	public ReconciliationMatchStatus getNotMatchedStatus() {
		return this.notMatchedStatus;
	}


	public void setNotMatchedStatus(ReconciliationMatchStatus notMatchedStatus) {
		this.notMatchedStatus = notMatchedStatus;
	}


	public ReconciliationReconcileStatus getReconciledStatus() {
		return this.reconciledStatus;
	}


	public void setReconciledStatus(ReconciliationReconcileStatus reconciledStatus) {
		this.reconciledStatus = reconciledStatus;
	}


	public ReconciliationReconcileStatus getNotReconciledStatus() {
		return this.notReconciledStatus;
	}


	public void setNotReconciledStatus(ReconciliationReconcileStatus notReconciledStatus) {
		this.notReconciledStatus = notReconciledStatus;
	}
}
