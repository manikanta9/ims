package com.clifton.reconciliation.definition.evaluator;

import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>ReconciliationEvaluatorCommand</code> is used to process {@link ReconciliationEvaluator} objects for runs.
 * It is bound to from the UI Reconciliation Evaluator admin screen found ion the 'ReconciliationProcessingManagementWindow'
 * <p>
 *
 * @author StevenF
 */
public class ReconciliationEvaluatorCommand {

	private boolean synchronous;

	private Integer reconciliationDefinitionId;

	private Integer reconciliationEvaluatorId;

	private Date runDate;


	////////////////////////////////////////////////////////////////////////////////


	public String getRunId() {
		String runId = "";
		if (getReconciliationDefinitionId() != null) {
			runId = "DEFINITION_" + getReconciliationDefinitionId();
		}
		else if (getReconciliationEvaluatorId() != null) {
			runId = "EVALUATOR_" + getReconciliationEvaluatorId();
		}
		else {
			runId += "ALL_";
		}
		runId += "_" + DateUtils.fromDate(getRunDate());
		return runId;
	}


	@Override
	public String toString() {
		return "ReconciliationCommand{" +
				"runDate=" + this.runDate +
				", reconciliationDefinitionId=" + this.reconciliationDefinitionId +
				", reconciliationEvaluatorId=" + this.reconciliationEvaluatorId +
				'}';
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public Integer getReconciliationDefinitionId() {
		return this.reconciliationDefinitionId;
	}


	public void setReconciliationDefinitionId(Integer reconciliationDefinitionId) {
		this.reconciliationDefinitionId = reconciliationDefinitionId;
	}


	public Integer getReconciliationEvaluatorId() {
		return this.reconciliationEvaluatorId;
	}


	public void setReconciliationEvaluatorId(Integer reconciliationEvaluatorId) {
		this.reconciliationEvaluatorId = reconciliationEvaluatorId;
	}


	public Date getRunDate() {
		return this.runDate;
	}


	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}
}
