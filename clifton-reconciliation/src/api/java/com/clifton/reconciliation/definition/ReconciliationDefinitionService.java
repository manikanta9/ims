package com.clifton.reconciliation.definition;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.reconciliation.definition.preprocessor.ReconciliationDefinitionPreprocessor;
import com.clifton.reconciliation.definition.preprocessor.search.ReconciliationDefinitionPreprocessorSearchForm;
import com.clifton.reconciliation.definition.search.ReconciliationDefinitionFieldMappingSearchForm;
import com.clifton.reconciliation.definition.search.ReconciliationDefinitionFieldSearchForm;
import com.clifton.reconciliation.definition.search.ReconciliationDefinitionSearchForm;
import com.clifton.reconciliation.definition.search.ReconciliationFieldSearchForm;
import com.clifton.reconciliation.definition.search.ReconciliationSourceSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;


/**
 * The <code>ReconciliationDefinitionService</code> interface defines methods for managing
 * {@link ReconciliationDefinition}, {@link ReconciliationDefinitionField},
 * {@link ReconciliationDefinitionFieldMapping}, and {@link ReconciliationSource} objects.
 *
 * @author StevenF on 11/23/2016.
 */
public interface ReconciliationDefinitionService {

	////////////////////////////////////////////////////////////////////////////
	////////         Reconciliation Definition Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationDefinition getReconciliationDefinition(int id);


	public ReconciliationDefinition getReconciliationDefinitionByParent(int parentId);


	public ReconciliationDefinition getReconciliationDefinitionByName(String name);


	@DoNotAddRequestMapping
	public ReconciliationDefinition getReconciliationDefinitionPopulated(int id, Date activeOnDate);


	public List<ReconciliationDefinition> getReconciliationDefinitionList(ReconciliationDefinitionSearchForm searchForm);


	public ReconciliationDefinition saveReconciliationDefinition(ReconciliationDefinition reconciliationDefinition);


	public void deleteReconciliationDefinition(int id);


	@RequestMapping("reconciliationDefinitionSystemAuditEventListFind")
	public DataTable getReconciliationDefinitionSystemAuditEventList(int id);

	////////////////////////////////////////////////////////////////////////////
	////////           Reconciliation Field Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationField getReconciliationField(int id);


	public ReconciliationField getReconciliationFieldByName(String name);


	public List<ReconciliationField> getReconciliationFieldList(ReconciliationFieldSearchForm searchForm);


	public ReconciliationField saveReconciliationField(ReconciliationField reconciliationField);


	public void deleteReconciliationField(int id);

	////////////////////////////////////////////////////////////////////////////
	////////      Reconciliation Definition Field Business Methods     /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationDefinitionField getReconciliationDefinitionField(int id);


	public ReconciliationDefinitionField getReconciliationDefinitionFieldByNameAndDefinitionId(String name, int definitionId, Date activeOnDate);


	public List<ReconciliationDefinitionField> getReconciliationDefinitionFieldList(ReconciliationDefinitionFieldSearchForm searchForm);


	@DoNotAddRequestMapping
	public List<ReconciliationDefinitionField> getReconciliationDefinitionFieldListByDefinitionId(int definitionId);


	public List<ReconciliationDefinitionField> getReconciliationDefinitionFieldListByDefinitionId(int definitionId, Date activeOnDate);


	public List<ReconciliationDefinitionField> getReconciliationDefinitionFieldListByParentDefinitionId(int definitionId, Date activeOnDate);


	public ReconciliationDefinitionField copyReconciliationDefinitionField(int definitionFieldId);


	public ReconciliationDefinitionField saveReconciliationDefinitionField(ReconciliationDefinitionField reconciliationDefinitionField);


	public void deleteReconciliationDefinitionField(int id);


	public void saveReconciliationDefinitionFieldList(List<ReconciliationDefinitionField> newList, List<ReconciliationDefinitionField> oldList);

	////////////////////////////////////////////////////////////////////////////
	//////// Reconciliation Definition Field Mapping Business Methods  /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationDefinitionFieldMapping getReconciliationDefinitionFieldMapping(int id);


	public List<ReconciliationDefinitionFieldMapping> getReconciliationDefinitionFieldMappingList(ReconciliationDefinitionFieldMappingSearchForm searchForm);


	public List<ReconciliationDefinitionFieldMapping> getReconciliationDefinitionFieldMappingListByDefinitionFieldId(int definitionFieldId);


	public ReconciliationDefinitionFieldMapping saveReconciliationDefinitionFieldMapping(ReconciliationDefinitionFieldMapping reconciliationDefinitionFieldMapping);


	public void deleteReconciliationDefinitionFieldMapping(int id);

	////////////////////////////////////////////////////////////////////////////
	////////          Reconciliation Source Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationSource getReconciliationSource(short id);


	public ReconciliationSource getReconciliationSourceByName(String name);


	public List<ReconciliationSource> getReconciliationSourceList(ReconciliationSourceSearchForm searchForm);


	public ReconciliationSource saveReconciliationSource(ReconciliationSource reconciliationSource);


	public void deleteReconciliationSource(short id);

	////////////////////////////////////////////////////////////////////////////
	////////      Reconciliation Definition Preprocessor Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationDefinitionPreprocessor getReconciliationDefinitionPreprocessor(short id);


	public List<ReconciliationDefinitionPreprocessor> getReconciliationDefinitionPreprocessorList(ReconciliationDefinitionPreprocessorSearchForm searchForm);


	public ReconciliationDefinitionPreprocessor saveReconciliationDefinitionPreprocessor(ReconciliationDefinitionPreprocessor reconciliationDefinitionPreprocessor);


	public void deleteReconciliationDefinitionPreprocessor(short id);


	@DoNotAddRequestMapping
	public List<ReconciliationDefinitionPreprocessor> getReconciliationDefinitionPreprocessorListByDefinitionId(int definitionId);
}
