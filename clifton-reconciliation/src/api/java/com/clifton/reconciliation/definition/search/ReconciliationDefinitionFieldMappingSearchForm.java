package com.clifton.reconciliation.definition.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author StevenF on 11/22/2016.
 */
public class ReconciliationDefinitionFieldMappingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchFieldPath = "definitionField", searchField = "reconciliationDefinition.id")
	private Integer definitionId;

	@SearchField(searchField = "definitionField.id")
	private Integer definitionFieldId;

	@SearchField(searchField = "source.id")
	private Short sourceId;

	@SearchField(searchField = "conditionRule.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean conditionRuleIsNull;

	@SearchField
	private Integer evaluationOrder;

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public Integer getDefinitionFieldId() {
		return this.definitionFieldId;
	}


	public void setDefinitionFieldId(Integer definitionFieldId) {
		this.definitionFieldId = definitionFieldId;
	}


	public Short getSourceId() {
		return this.sourceId;
	}


	public void setSourceId(Short sourceId) {
		this.sourceId = sourceId;
	}


	public Boolean getConditionRuleIsNull() {
		return this.conditionRuleIsNull;
	}


	public void setConditionRuleIsNull(Boolean conditionRuleIsNull) {
		this.conditionRuleIsNull = conditionRuleIsNull;
	}


	public Integer getEvaluationOrder() {
		return this.evaluationOrder;
	}


	public void setEvaluationOrder(Integer evaluationOrder) {
		this.evaluationOrder = evaluationOrder;
	}
}
