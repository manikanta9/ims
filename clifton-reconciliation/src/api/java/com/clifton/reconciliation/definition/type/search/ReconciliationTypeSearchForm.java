package com.clifton.reconciliation.definition.type.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author stevenf
 */
public class ReconciliationTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Boolean cumulativeReconciliation;

	@SearchField(searchFieldPath = "runApprovalSystemCondition", searchField = "name")
	private String runApprovalSystemConditionName;

	@SearchField(searchFieldPath = "runModifySystemCondition", searchField = "name")
	private String runModifySystemConditionName;

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getCumulativeReconciliation() {
		return this.cumulativeReconciliation;
	}


	public void setCumulativeReconciliation(Boolean cumulativeReconciliation) {
		this.cumulativeReconciliation = cumulativeReconciliation;
	}


	public String getRunApprovalSystemConditionName() {
		return this.runApprovalSystemConditionName;
	}


	public void setRunApprovalSystemConditionName(String runApprovalSystemConditionName) {
		this.runApprovalSystemConditionName = runApprovalSystemConditionName;
	}


	public String getRunModifySystemConditionName() {
		return this.runModifySystemConditionName;
	}


	public void setRunModifySystemConditionName(String runModifySystemConditionName) {
		this.runModifySystemConditionName = runModifySystemConditionName;
	}
}
