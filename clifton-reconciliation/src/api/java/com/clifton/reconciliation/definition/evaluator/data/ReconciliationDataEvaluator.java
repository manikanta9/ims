package com.clifton.reconciliation.definition.evaluator.data;

import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.data.DataError;


/**
 * @author StevenF
 */
public interface ReconciliationDataEvaluator {

	public DataError evaluate(ReconciliationRunObject runObject, ReconciliationDefinitionField definitionField);
}
