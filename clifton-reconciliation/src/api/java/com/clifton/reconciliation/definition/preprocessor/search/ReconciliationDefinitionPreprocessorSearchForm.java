package com.clifton.reconciliation.definition.preprocessor.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


/**
 * <code>ReconciliationDefinitionPreprocessorSearchForm</code>
 */
public class ReconciliationDefinitionPreprocessorSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "id")
	private Short id;

	@SearchField(searchField = "reconciliationDefinition.id")
	private Integer definitionId;

	@SearchField(searchField = "preprocessor.id")
	private Integer preprocessorId;

	@SearchField
	private Integer evaluationOrder;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public Integer getPreprocessorId() {
		return this.preprocessorId;
	}


	public void setPreprocessorId(Integer preprocessorId) {
		this.preprocessorId = preprocessorId;
	}


	public Integer getEvaluationOrder() {
		return this.evaluationOrder;
	}


	public void setEvaluationOrder(Integer evaluationOrder) {
		this.evaluationOrder = evaluationOrder;
	}
}
