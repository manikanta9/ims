package com.clifton.reconciliation.definition.rule.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * @author StevenF on 10/02/2017.
 */
public class ReconciliationFilterSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "reconciliationDefinition.id")
	private Integer definitionId;

	@SearchField(searchField = "name", searchFieldPath = "reconciliationDefinition")
	private String definitionName;

	@SearchField
	private Boolean include;

	@SearchField
	private Boolean doNotSaveFilteredData;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public Boolean getInclude() {
		return this.include;
	}


	public void setInclude(Boolean include) {
		this.include = include;
	}


	public Boolean getDoNotSaveFilteredData() {
		return this.doNotSaveFilteredData;
	}


	public void setDoNotSaveFilteredData(Boolean doNotSaveFilteredData) {
		this.doNotSaveFilteredData = doNotSaveFilteredData;
	}
}
