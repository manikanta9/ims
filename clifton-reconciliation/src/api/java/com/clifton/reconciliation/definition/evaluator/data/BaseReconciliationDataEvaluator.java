package com.clifton.reconciliation.definition.evaluator.data;

import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.data.DataError;
import com.clifton.reconciliation.run.data.DataObject;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;


/**
 * @author StevenF
 */
public abstract class BaseReconciliationDataEvaluator<T> implements ReconciliationDataEvaluator {

	private ReconciliationRunObject runObject;
	private ReconciliationDefinitionField definitionField;


	@Override
	@SuppressWarnings("unchecked")
	public DataError evaluate(ReconciliationRunObject runObject, ReconciliationDefinitionField definitionField) {
		setRunObject(runObject);
		setDefinitionField(definitionField);

		T primaryValue = null;
		T secondaryValue = null;
		try {
			List<DataObject> primaryDataObjectList = runObject.getDataHolder().getDataObjectListBySource(runObject.getReconciliationRun().getReconciliationDefinition().getPrimarySource());
			List<DataObject> secondaryDataObjectList = runObject.getDataHolder().getDataObjectListBySource(runObject.getReconciliationRun().getReconciliationDefinition().getSecondarySource());

			primaryValue = (T) getDataObjectFieldValue(primaryDataObjectList, definitionField);
			secondaryValue = (T) getDataObjectFieldValue(secondaryDataObjectList, definitionField);
			return evaluateData(primaryValue, secondaryValue);
		}
		catch (Exception e) {
			LogUtils.error(getClass(), "Failed to evaluate runObject with id [" + runObject.getId() + "]: " + e.getMessage(), e);
			return new DataError(getDefinitionField(), primaryValue, secondaryValue);
		}
	}


	private Object getDataObjectFieldValue(List<DataObject> dataObjectList, ReconciliationDefinitionField definitionField) {
		Object dataObjectValue = null;
		DataTypes dataType = ObjectUtils.coalesce(definitionField.getDataType(), definitionField.getField().getDefaultDataType());
		if (dataType != null) {
			if (CollectionUtils.getSize(dataObjectList) == 1) {
				return DataTypeNameUtils.convertObjectToDataTypeName(dataObjectList.get(0).getData().get(definitionField.getField().getName()), dataType.getTypeName());
			}
			for (DataObject dataObject : CollectionUtils.getIterable(dataObjectList)) {
				if (dataType.getTypeName().isNumeric()) {
					if (DataTypeNames.INTEGER.name().equals(dataType.getTypeName().name())) {
						if (dataObjectValue == null) {
							dataObjectValue = 0;
						}
						dataObjectValue = ((Integer) dataObjectValue) + DataTypeNameUtils.convertObjectToDataTypeName(dataObject.getData().get(definitionField.getField().getName()), dataType.getTypeName(), Integer.class);
					}
					else {
						if (dataObjectValue == null) {
							dataObjectValue = BigDecimal.ZERO;
						}
						dataObjectValue = MathUtils.add(DataTypeNameUtils.convertObjectToDataTypeName(dataObjectValue, dataType.getTypeName(), BigDecimal.class), DataTypeNameUtils.convertObjectToDataTypeName(dataObject.getData().get(definitionField.getField().getName()), dataType.getTypeName(), BigDecimal.class));
					}
				}
				else {
					if (dataObjectValue == null) {
						dataObjectValue = DataTypeNameUtils.convertObjectToDataTypeName(dataObject.getData().get(definitionField.getField().getName()), dataType.getTypeName());
					}
					else {
						if (!Objects.equals(dataObjectValue, DataTypeNameUtils.convertObjectToDataTypeName(dataObject.getData().get(definitionField.getField().getName()), dataType.getTypeName()))) {
							throw new ValidationException("Failed to reconcile!  Reconcilable fields in object list of type [" + dataType.getTypeName() + "] are not able to be aggregated and all values do not match!");
						}
					}
				}
			}
		}
		return dataObjectValue;
	}


	public abstract DataError evaluateData(T primaryValue, T secondaryValue);

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationRunObject getRunObject() {
		return this.runObject;
	}


	public void setRunObject(ReconciliationRunObject runObject) {
		this.runObject = runObject;
	}


	public ReconciliationDefinitionField getDefinitionField() {
		return this.definitionField;
	}


	public void setDefinitionField(ReconciliationDefinitionField definitionField) {
		this.definitionField = definitionField;
	}
}
