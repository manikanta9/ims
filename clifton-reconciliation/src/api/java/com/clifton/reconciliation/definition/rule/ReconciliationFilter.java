package com.clifton.reconciliation.definition.rule;

import com.clifton.core.beans.BaseEntity;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.rule.ReconciliationRuleCondition;

import java.util.Date;


/**
 * The <code>ReconciliationFilter</code> specifies if rows should be processed from a given source.
 * A row must match all defined filters on a definition in order to be processed during reconciliation
 *
 * @author StevenF on 11/22/2016.
 */
public class ReconciliationFilter extends BaseEntity<Integer> {

	private ReconciliationDefinition reconciliationDefinition;
	private ReconciliationSource reconciliationSource;
	private ReconciliationRuleCondition conditionRule;

	private String name;

	// most of the time include == true in order to exclude unwanted data
	private boolean include;

	// if set, the data that was filtered out will NOT be saved in orphan data.
	private boolean doNotSaveFilteredData;

	private Date startDate;
	private Date endDate;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public boolean isInclude() {
		return this.include;
	}


	public void setInclude(boolean include) {
		this.include = include;
	}


	public boolean isDoNotSaveFilteredData() {
		return this.doNotSaveFilteredData;
	}


	public void setDoNotSaveFilteredData(boolean doNotSaveFilteredData) {
		this.doNotSaveFilteredData = doNotSaveFilteredData;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public ReconciliationDefinition getReconciliationDefinition() {
		return this.reconciliationDefinition;
	}


	public void setReconciliationDefinition(ReconciliationDefinition reconciliationDefinition) {
		this.reconciliationDefinition = reconciliationDefinition;
	}


	public ReconciliationSource getReconciliationSource() {
		return this.reconciliationSource;
	}


	public void setReconciliationSource(ReconciliationSource reconciliationSource) {
		this.reconciliationSource = reconciliationSource;
	}


	public ReconciliationRuleCondition getConditionRule() {
		return this.conditionRule;
	}


	public void setConditionRule(ReconciliationRuleCondition conditionRule) {
		this.conditionRule = conditionRule;
	}
}
