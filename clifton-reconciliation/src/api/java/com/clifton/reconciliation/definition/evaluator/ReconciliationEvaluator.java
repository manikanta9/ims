package com.clifton.reconciliation.definition.evaluator;

import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.core.beans.BaseEntity;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.rule.ReconciliationRuleCondition;
import com.clifton.system.bean.SystemBean;

import java.util.Date;


/**
 * The ReconciliationEvaluator class is be used for a field to specify if/how data from both sources should be reconciled.
 * It can be used to specify reconciliation thresholds, gap quantities, etc.
 * Evaluators are applied using the specified Order and can use filters (Holding Account, Investment Security, Custom Condition).
 *
 * @author StevenF
 */
public class ReconciliationEvaluator extends BaseEntity<Integer> {

	private ReconciliationDefinitionField definitionField;

	// scope
	private ReconciliationInvestmentAccount reconciliationInvestmentAccount;
	private ReconciliationInvestmentSecurity reconciliationInvestmentSecurity;
	private ReconciliationRuleCondition conditionRule;


	private String name;

	private SystemBean evaluatorBean;

	/**
	 * Order in which this is evaluated given a group of evaluators for a definition
	 * This is important due to conditional evaluation (based on the conditionRule)
	 */
	int evaluationOrder;

	/**
	 * Optional schedule to determine when an evaluator should run
	 * Primarily this is used for threshold evaluators, e.g. specify a $25 threshold
	 * but do not run the evaluator on the last business day of the month.
	 * This allows small differences to reconcile until the last day of the month.
	 */
	private CalendarSchedule calendarSchedule;

	/**
	 * Runs the evaluator on all days EXCEPT those defined by the schedule.
	 */
	private boolean exclusive;

	/**
	 * Start and End Dates for the comparator - defines if it is active
	 */
	private Date startDate;
	private Date endDate;


	public String getLabel() {
		return getName();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public CalendarSchedule getCalendarSchedule() {
		return this.calendarSchedule;
	}


	public void setCalendarSchedule(CalendarSchedule calendarSchedule) {
		this.calendarSchedule = calendarSchedule;
	}


	public boolean isExclusive() {
		return this.exclusive;
	}


	public void setExclusive(boolean exclusive) {
		this.exclusive = exclusive;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public ReconciliationDefinitionField getDefinitionField() {
		return this.definitionField;
	}


	public void setDefinitionField(ReconciliationDefinitionField definitionField) {
		this.definitionField = definitionField;
	}


	public ReconciliationInvestmentAccount getReconciliationInvestmentAccount() {
		return this.reconciliationInvestmentAccount;
	}


	public void setReconciliationInvestmentAccount(ReconciliationInvestmentAccount reconciliationInvestmentAccount) {
		this.reconciliationInvestmentAccount = reconciliationInvestmentAccount;
	}


	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurity() {
		return this.reconciliationInvestmentSecurity;
	}


	public void setReconciliationInvestmentSecurity(ReconciliationInvestmentSecurity reconciliationInvestmentSecurity) {
		this.reconciliationInvestmentSecurity = reconciliationInvestmentSecurity;
	}


	public ReconciliationRuleCondition getConditionRule() {
		return this.conditionRule;
	}


	public void setConditionRule(ReconciliationRuleCondition conditionRule) {
		this.conditionRule = conditionRule;
	}


	public SystemBean getEvaluatorBean() {
		return this.evaluatorBean;
	}


	public void setEvaluatorBean(SystemBean evaluatorBean) {
		this.evaluatorBean = evaluatorBean;
	}


	public int getEvaluationOrder() {
		return this.evaluationOrder;
	}


	public void setEvaluationOrder(int evaluationOrder) {
		this.evaluationOrder = evaluationOrder;
	}
}
