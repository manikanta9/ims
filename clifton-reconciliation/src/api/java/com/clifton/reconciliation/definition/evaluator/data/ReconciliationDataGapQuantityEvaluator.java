package com.clifton.reconciliation.definition.evaluator.data;

import com.clifton.reconciliation.run.data.DataError;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * @author StevenF
 */
public class ReconciliationDataGapQuantityEvaluator extends BaseReconciliationDataEvaluator<Number> {

	private BigDecimal quantity;


	@Override
	public DataError evaluateData(Number primaryValue, Number secondaryValue) {
		BigDecimal diffValue = MathUtils.subtract(new BigDecimal(primaryValue.toString()), new BigDecimal(secondaryValue.toString()));
		if (MathUtils.isEqual(diffValue, getQuantity())) {
			return null;
		}
		return new DataError(getDefinitionField(), primaryValue, secondaryValue);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
}
