package com.clifton.reconciliation.definition.evaluator.data;

import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.data.DataError;
import com.clifton.reconciliation.run.data.DataNoteTypes;
import com.clifton.security.user.SecurityUserService;


/**
 * @author StevenF
 */
public class ReconciliationDataAutoReconcileEvaluator implements ReconciliationDataEvaluator {

	private String reconciliationNote;

	private SecurityUserService securityUserService;


	@Override
	public DataError evaluate(ReconciliationRunObject runObject, ReconciliationDefinitionField definitionField) {
		runObject.addDataNote(getReconciliationNote(), DataNoteTypes.AUTO_RECONCILE, getSecurityUserService().getSecurityUserCurrent().getId());
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public String getReconciliationNote() {
		return this.reconciliationNote;
	}


	public void setReconciliationNote(String reconciliationNote) {
		this.reconciliationNote = reconciliationNote;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
