package com.clifton.reconciliation.definition.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author StevenF on 10/02/2017.
 */
public class ReconciliationSourceSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "id")
	private Short[] ids;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private String shortName;

	@SearchField
	private Boolean defaultSource;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short[] getIds() {
		return this.ids;
	}


	public void setIds(Short[] ids) {
		this.ids = ids;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getShortName() {
		return this.shortName;
	}


	public void setShortName(String shortName) {
		this.shortName = shortName;
	}


	public Boolean getDefaultSource() {
		return this.defaultSource;
	}


	public void setDefaultSource(Boolean defaultSource) {
		this.defaultSource = defaultSource;
	}
}
