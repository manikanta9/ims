package com.clifton.reconciliation.definition.preprocessor.data;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.SecurityIdentifierUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.simpletable.SimpleTable;

import java.util.Arrays;
import java.util.List;


/**
 * The <code>ReconciliationAdventDataSecurityIdentifierPreprocessor</code> allows for specifying how and when
 * a security identifier should be translated to another type of security identifier prior to loading the
 * data into the reconciliation system (e.g. translate CUSIP to Symbol)
 *
 * @author stevenf
 */
public class ReconciliationDefinitionAdventSecurityIdentifierDataPreprocessor implements ReconciliationDefinitionDataPreprocessor {

	private static final String SECURITY_IDENTIFIER_SYMBOL = "securitySymbol";
	private static final String SECURITY_IDENTIFIER_CUSIP = "securityCusip";
	private static final String SECURITY_IDENTIFIER_ISIN = "securityIsin";
	private static final String SECURITY_IDENTIFIER_SEDOL = "securitySedol";
	private static final String SECURITY_IDENTIFIER_OCC_SYMBOL = "securityOccSymbol";

	private static final List<String> SECURITY_IDENTIFIERS = Arrays.asList(SECURITY_IDENTIFIER_SYMBOL, SECURITY_IDENTIFIER_CUSIP, SECURITY_IDENTIFIER_ISIN, SECURITY_IDENTIFIER_SEDOL, SECURITY_IDENTIFIER_OCC_SYMBOL);

	private String fromSecurityIdentifier;
	private String toSecurityIdentifier;

	private String inputSecurityIdentifierColumn;
	private String outputSecurityIdentifierColumn;
	private String securityTypeColumn;

	private String[] securityTypeCodes;

	private ReconciliationInvestmentService reconciliationInvestmentService;


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getToSecurityIdentifier(), "Security Identifier to translate to cannot be null!");
		ValidationUtils.assertTrue(CollectionUtils.contains(SECURITY_IDENTIFIERS, getToSecurityIdentifier()), "Unsupported translation to [" + getToSecurityIdentifier() + "]!");
	}


	@Override
	public void accept(SimpleTable simpleTable, int rowIndex) {
		Object securityIdentifier = simpleTable.getColumnValue(ObjectUtils.coalesce(getInputSecurityIdentifierColumn(), ADVENT_SECURITY_IDENTIFIER_COLUMN), rowIndex);
		if (securityIdentifier != null) {
			String securityColumnValue = (String) securityIdentifier;
			//Check if translation should be done.
			if (!StringUtils.isEmpty(securityColumnValue) && doTranslation(securityColumnValue, simpleTable.getColumnValue(ObjectUtils.coalesce(getSecurityTypeColumn(), ADVENT_SECURITY_TYPE_COLUMN), rowIndex))) {
				ReconciliationInvestmentSecurity security = getReconciliationInvestmentService().getReconciliationInvestmentSecurityByIdentifier(securityColumnValue);
				if (security != null) {
					Object toSecurityId = BeanUtils.getPropertyValue(security, getToSecurityIdentifier());
					if (toSecurityId != null) {
						simpleTable.updateColumnValue(ObjectUtils.coalesce(getOutputSecurityIdentifierColumn(), ADVENT_SECURITY_IDENTIFIER_COLUMN), rowIndex, toSecurityId.toString());
					}
					else {
						LogUtils.warn(ReconciliationDefinitionAdventSecurityIdentifierDataPreprocessor.class, getToSecurityIdentifier() + " value was null for " + securityIdentifier + "; value was not translated.");
					}
				}
				else {
					LogUtils.warn(ReconciliationDefinitionAdventSecurityIdentifierDataPreprocessor.class, "Failed to locate security with identifier: " + securityIdentifier);
				}
			}
		}
	}


	public boolean doTranslation(String securityIdentifier, Object securityTypeObject) {
		boolean translate = true;
		//If no fromSecurityIdentifier is defined, then we want to translate regardless
		if (getFromSecurityIdentifier() == null) {
			return true;
		}
		//If the security types codes value is not empty and it doesn't contain the security type of the current row then we don't translate.
		if (securityTypeObject != null && !ArrayUtils.isEmpty(getSecurityTypeCodes()) && !CollectionUtils.contains(Arrays.asList(getSecurityTypeCodes()), String.valueOf(securityTypeObject))) {
			return false;
		}
		switch (getFromSecurityIdentifier()) {
			case SECURITY_IDENTIFIER_CUSIP:
				if (!SecurityIdentifierUtils.isValidCUSIP(securityIdentifier, false)) {
					translate = false;
				}
				break;
			case SECURITY_IDENTIFIER_ISIN:
				if (!SecurityIdentifierUtils.isValidISIN(securityIdentifier, false)) {
					translate = false;
				}
				break;
			case SECURITY_IDENTIFIER_SEDOL:
				if (!SecurityIdentifierUtils.isValidSedol(securityIdentifier, false)) {
					translate = false;
				}
				break;
			case SECURITY_IDENTIFIER_SYMBOL:
				//TODO - Check for valid symbol?
				translate = false;
				break;
			case SECURITY_IDENTIFIER_OCC_SYMBOL:
				if (!SecurityIdentifierUtils.isValidOccSymbol(securityIdentifier, false)) {
					translate = false;
				}
				break;
		}
		return translate;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String[] getSecurityTypeCodes() {
		if (this.securityTypeCodes == null) {
			this.securityTypeCodes = new String[0];
		}
		return this.securityTypeCodes;
	}


	public void setSecurityTypeCodes(String[] securityTypeCodes) {
		this.securityTypeCodes = securityTypeCodes;
	}


	public String getFromSecurityIdentifier() {
		return this.fromSecurityIdentifier;
	}


	public void setFromSecurityIdentifier(String fromSecurityIdentifier) {
		this.fromSecurityIdentifier = fromSecurityIdentifier;
	}


	public String getInputSecurityIdentifierColumn() {
		return this.inputSecurityIdentifierColumn;
	}


	public void setInputSecurityIdentifierColumn(String inputSecurityIdentifierColumn) {
		this.inputSecurityIdentifierColumn = inputSecurityIdentifierColumn;
	}


	public String getOutputSecurityIdentifierColumn() {
		return this.outputSecurityIdentifierColumn;
	}


	public void setOutputSecurityIdentifierColumn(String outputSecurityIdentifierColumn) {
		this.outputSecurityIdentifierColumn = outputSecurityIdentifierColumn;
	}


	public String getSecurityTypeColumn() {
		return this.securityTypeColumn;
	}


	public void setSecurityTypeColumn(String securityTypeColumn) {
		this.securityTypeColumn = securityTypeColumn;
	}


	public String getToSecurityIdentifier() {
		return this.toSecurityIdentifier;
	}


	public void setToSecurityIdentifier(String toSecurityIdentifier) {
		this.toSecurityIdentifier = toSecurityIdentifier;
	}


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}
}
