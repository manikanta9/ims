package com.clifton.reconciliation.definition.evaluator.data;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.run.data.DataError;

import java.math.BigDecimal;


/**
 * @author StevenF
 */
public class ReconciliationDataThresholdEvaluator extends BaseReconciliationDataEvaluator<Number> {

	private boolean percentage;

	private BigDecimal lowerThreshold;
	private BigDecimal upperThreshold;

	private boolean lowerExclusive;
	private boolean upperExclusive;


	@Override
	public DataError evaluateData(Number primaryValue, Number secondaryValue) {
		BigDecimal diff;
		BigDecimal primary = new BigDecimal(primaryValue.toString());
		BigDecimal secondary = new BigDecimal(secondaryValue.toString());
		if (isPercentage()) {
			BigDecimal lower = MathUtils.negate(MathUtils.getPercentageOf(MathUtils.abs(getLowerThreshold()), MathUtils.abs(primary)));
			BigDecimal upper = MathUtils.getPercentageOf(MathUtils.abs(getUpperThreshold()), MathUtils.abs(primary));
			diff = MathUtils.subtract(primary, secondary);
			if (isBetween(lower, upper, diff)) {
				return null;
			}
		}
		else {
			ValidationUtils.assertTrue(MathUtils.isLessThan(getLowerThreshold(), getUpperThreshold()), "Lower Threshold value of '" + getLowerThreshold().toPlainString() + "' must be lower than Upper Threshold value of '" + getUpperThreshold().toPlainString() + "'!");
			diff = MathUtils.subtract(primary, secondary);
			if (isBetween(getLowerThreshold(), getUpperThreshold(), diff)) {
				return null;
			}
		}
		return new DataError(getDefinitionField(), diff);
	}


	// by default the comparisons include the lower and upper bounds (>= and <=).
	private boolean isBetween(BigDecimal lower, BigDecimal upper, BigDecimal value) {
		if (isLowerExclusive()) {
			if (isUpperExclusive()) {
				return MathUtils.isGreaterThan(value, lower) && MathUtils.isLessThan(value, upper);
			}
			else {
				return MathUtils.isGreaterThan(value, lower) && MathUtils.isLessThanOrEqual(value, upper);
			}
		}
		else {
			if (isUpperExclusive()) {
				return MathUtils.isGreaterThanOrEqual(value, lower) && MathUtils.isLessThan(value, upper);
			}
			else {
				return MathUtils.isGreaterThanOrEqual(value, lower) && MathUtils.isLessThanOrEqual(value, upper);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isPercentage() {
		return this.percentage;
	}


	public void setPercentage(boolean percentage) {
		this.percentage = percentage;
	}


	public BigDecimal getLowerThreshold() {
		return this.lowerThreshold;
	}


	public void setLowerThreshold(BigDecimal lowerThreshold) {
		this.lowerThreshold = lowerThreshold;
	}


	public BigDecimal getUpperThreshold() {
		return this.upperThreshold;
	}


	public void setUpperThreshold(BigDecimal upperThreshold) {
		this.upperThreshold = upperThreshold;
	}


	public boolean isLowerExclusive() {
		return this.lowerExclusive;
	}


	public void setLowerExclusive(boolean lowerExclusive) {
		this.lowerExclusive = lowerExclusive;
	}


	public boolean isUpperExclusive() {
		return this.upperExclusive;
	}


	public void setUpperExclusive(boolean upperExclusive) {
		this.upperExclusive = upperExclusive;
	}
}
