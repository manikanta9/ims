package com.clifton.reconciliation.definition.evaluator;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.definition.evaluator.search.ReconciliationEvaluatorSearchForm;

import java.util.List;


/**
 * The <code>ReconciliationEvaluatorService</code> defines methods for working with {@link ReconciliationEvaluator} objects
 * <p>
 * Reconciliation Evaluators are utilized during reconciliation of data, and used to determine if an object should be considered
 * 'reconciled'; for instance, if the values from the primary and secondary differ by a value within the range of the comparator
 * they will be considered reconciled (i.e. if they are within .05 or each other.)
 *
 * @author StevenF
 */
public interface ReconciliationEvaluatorService {

	////////////////////////////////////////////////////////////////////////////
	////////         Reconciliation Evaluator Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationEvaluator getReconciliationEvaluator(int id);


	public ReconciliationEvaluator getReconciliationEvaluatorByName(String name);


	public List<ReconciliationEvaluator> getReconciliationEvaluatorList(ReconciliationEvaluatorSearchForm searchForm);


	@DoNotAddRequestMapping
	public List<ReconciliationEvaluator> getReconciliationEvaluatorListByDefinitionFieldId(int definitionFieldId);


	public ReconciliationEvaluator saveReconciliationEvaluator(ReconciliationEvaluator reconciliationEvaluator);


	public ReconciliationEvaluator copyReconciliationEvaluator(ReconciliationEvaluator reconciliationEvaluator);


	public void deleteReconciliationEvaluator(int id);


	////////////////////////////////////////////////////////////////////////////
	////////         Reconciliation Evaluator Utility Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(permissions = SecurityPermission.PERMISSION_EXECUTE)
	public Status processReconciliationEvaluator(ReconciliationEvaluatorCommand command);
}
