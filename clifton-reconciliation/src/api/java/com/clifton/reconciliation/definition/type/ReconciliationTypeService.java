package com.clifton.reconciliation.definition.type;

import com.clifton.reconciliation.definition.type.search.ReconciliationTypeRequiredNaturalKeySearchForm;
import com.clifton.reconciliation.definition.type.search.ReconciliationTypeSearchForm;

import java.util.List;


/**
 * @author stevenf
 */
public interface ReconciliationTypeService {

	////////////////////////////////////////////////////////////////////////////
	////////            Reconciliation Type Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationType getReconciliationType(short id);


	public ReconciliationType getReconciliationTypeByName(String name);


	public List<ReconciliationType> getReconciliationTypeList(ReconciliationTypeSearchForm searchForm);


	public ReconciliationType saveReconciliationType(ReconciliationType reconciliationType);


	public void deleteReconciliationType(short id);

	////////////////////////////////////////////////////////////////////////////
	////////    Reconciliation Type Required Field Business Methods    /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationTypeRequiredNaturalKey getReconciliationTypeRequiredNaturalKey(short id);


	public List<ReconciliationTypeRequiredNaturalKey> getReconciliationTypeRequiredNaturalKeyListByTypeId(short id);


	public List<ReconciliationTypeRequiredNaturalKey> getReconciliationTypeRequiredNaturalKeyList(ReconciliationTypeRequiredNaturalKeySearchForm searchForm);


	public ReconciliationTypeRequiredNaturalKey saveReconciliationTypeRequiredNaturalKey(ReconciliationTypeRequiredNaturalKey reconciliationTypeRequiredNaturalKey);


	public void deleteReconciliationTypeRequiredNaturalKey(short id);
}
