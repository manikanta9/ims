package com.clifton.reconciliation.definition.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.workflow.search.BaseWorkflowAwareSystemHierarchyItemSearchForm;


/**
 * ReconciliationDefinition allows for defining how an object should be reconciled.
 * It utilizes beanPaths and comparators to define how the internal and external target
 * fields should be compared.
 *
 * @author StevenF on 11/22/2016.
 */
public class ReconciliationDefinitionSearchForm extends BaseWorkflowAwareSystemHierarchyItemSearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "type.id")
	private Short typeId;

	// when typeName is set, lookup the type by name, clear the name and set id
	private String typeName;

	@SearchField(searchField = "type.id,parentDefinition.type.id", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Short typeOrParentTypeId;

	// when typeOrParentTypeName is set, lookup the type by name, clear the name and set id
	private String typeOrParentTypeName; // same as above

	@SearchField(searchField = "primarySource.id")
	private Short primarySourceId;

	@SearchField(searchField = "secondarySource.id")
	private Short secondarySourceId;

	@SearchField(searchField = "definitionGroup.id")
	private Integer definitionGroupId;

	@SearchField(searchField = "calendar.id")
	private Short calendarId;

	@SearchField
	private String contactEmail;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "ReconciliationDefinition";
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Short getTypeOrParentTypeId() {
		return this.typeOrParentTypeId;
	}


	public void setTypeOrParentTypeId(Short typeOrParentTypeId) {
		this.typeOrParentTypeId = typeOrParentTypeId;
	}


	public String getTypeOrParentTypeName() {
		return this.typeOrParentTypeName;
	}


	public void setTypeOrParentTypeName(String typeOrParentTypeName) {
		this.typeOrParentTypeName = typeOrParentTypeName;
	}


	public Short getPrimarySourceId() {
		return this.primarySourceId;
	}


	public void setPrimarySourceId(Short primarySourceId) {
		this.primarySourceId = primarySourceId;
	}


	public Short getSecondarySourceId() {
		return this.secondarySourceId;
	}


	public void setSecondarySourceId(Short secondarySourceId) {
		this.secondarySourceId = secondarySourceId;
	}


	public Integer getDefinitionGroupId() {
		return this.definitionGroupId;
	}


	public void setDefinitionGroupId(Integer definitionGroupId) {
		this.definitionGroupId = definitionGroupId;
	}


	public Short getCalendarId() {
		return this.calendarId;
	}


	public void setCalendarId(Short calendarId) {
		this.calendarId = calendarId;
	}


	public String getContactEmail() {
		return this.contactEmail;
	}


	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
}
