package com.clifton.reconciliation.definition.preprocessor.data;

import com.clifton.reconciliation.simpletable.SimpleTable;

import java.util.function.ObjIntConsumer;


/**
 * @author stevenf
 */
public interface ReconciliationDefinitionDataPreprocessor extends ObjIntConsumer<SimpleTable> {

	static final String ADVENT_SECURITY_TYPE_COLUMN = "SecurityType";
	static final String ADVENT_SECURITY_IDENTIFIER_COLUMN = "SecurityIdentifier";
	static final String ADVENT_MATURITY_DATE_COLUMN = "MaturityDate";


	public void validate();
}
