package com.clifton.reconciliation.definition.search;

import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * @author StevenF on 11/22/2016.
 */
public class ReconciliationDefinitionFieldSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "field.name,field.description")
	private String searchPattern;

	@SearchField(searchFieldPath = "field", searchField = "name")
	private String name;

	@SearchField(searchFieldPath = "field", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField(searchFieldPath = "field", searchField = "description")
	private String description;

	@SearchField(searchFieldPath = "field", searchField = "description", comparisonConditions = ComparisonConditions.EQUALS)
	private String descriptionEquals;

	@SearchField(searchFieldPath = "field", searchField = "label")
	private String label;

	@SearchField(searchFieldPath = "field", searchField = "label", comparisonConditions = ComparisonConditions.EQUALS)
	private String labelEquals;

	@SearchField(searchField = "reconciliationDefinition.id")
	private Integer definitionId;

	@SearchField(searchField = "name", searchFieldPath = "reconciliationDefinition")
	private String definitionName;

	@SearchField(searchField = "name", searchFieldPath = "reconciliationDefinition", comparisonConditions = ComparisonConditions.EQUALS)
	private String definitionNameEquals;

	@SearchField
	private Boolean adjustable;

	@SearchField
	private Boolean aggregate;

	@SearchField
	private Boolean hidden;

	@SearchField
	private Boolean naturalKey;

	@SearchField
	private Boolean reconcilable;

	@SearchField
	private Integer evaluationOrder;

	@SearchField
	private DataTypes dataType;

	@SearchField
	private Integer displayWidth;

	@SearchField
	private Integer displayOrder;


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getDescriptionEquals() {
		return this.descriptionEquals;
	}


	public void setDescriptionEquals(String descriptionEquals) {
		this.descriptionEquals = descriptionEquals;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getLabelEquals() {
		return this.labelEquals;
	}


	public void setLabelEquals(String labelEquals) {
		this.labelEquals = labelEquals;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public String getDefinitionNameEquals() {
		return this.definitionNameEquals;
	}


	public void setDefinitionNameEquals(String definitionNameEquals) {
		this.definitionNameEquals = definitionNameEquals;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public Boolean getAdjustable() {
		return this.adjustable;
	}


	public void setAdjustable(Boolean adjustable) {
		this.adjustable = adjustable;
	}


	public Boolean getAggregate() {
		return this.aggregate;
	}


	public void setAggregate(Boolean aggregate) {
		this.aggregate = aggregate;
	}


	public Boolean getHidden() {
		return this.hidden;
	}


	public void setHidden(Boolean hidden) {
		this.hidden = hidden;
	}


	public Boolean getReconcilable() {
		return this.reconcilable;
	}


	public void setReconcilable(Boolean reconcilable) {
		this.reconcilable = reconcilable;
	}


	public Boolean getNaturalKey() {
		return this.naturalKey;
	}


	public void setNaturalKey(Boolean naturalKey) {
		this.naturalKey = naturalKey;
	}


	public Integer getEvaluationOrder() {
		return this.evaluationOrder;
	}


	public void setEvaluationOrder(Integer evaluationOrder) {
		this.evaluationOrder = evaluationOrder;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public DataTypes getDataType() {
		return this.dataType;
	}


	public void setDataType(DataTypes dataType) {
		this.dataType = dataType;
	}


	public Integer getDisplayWidth() {
		return this.displayWidth;
	}


	public void setDisplayWidth(Integer displayWidth) {
		this.displayWidth = displayWidth;
	}


	public Integer getDisplayOrder() {
		return this.displayOrder;
	}


	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
}
