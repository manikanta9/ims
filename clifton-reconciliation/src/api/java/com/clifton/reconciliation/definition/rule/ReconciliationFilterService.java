package com.clifton.reconciliation.definition.rule;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.reconciliation.definition.rule.search.ReconciliationFilterSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>ReconciliationFilterService</code> defines methods for working with {@link ReconciliationFilter} objects
 * <p>
 * Reconciliation Filters are utilized during data load to determine if a particular row of data from an input source
 * should be loaded and processed.  For instance, in some reconciliations, certain data is unnecessary to process from
 * the input file and a filter can be defined to exclude rows with particular values.
 *
 * @author StevenF
 */
public interface ReconciliationFilterService {

	////////////////////////////////////////////////////////////////////////////
	////////        Reconciliation Filters Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationFilter getReconciliationFilter(int id);


	public ReconciliationFilter getReconciliationFilterByName(String name);


	public List<ReconciliationFilter> getReconciliationFilterList(ReconciliationFilterSearchForm searchForm);


	@DoNotAddRequestMapping
	public List<ReconciliationFilter> getReconciliationFilterListByDefinitionId(int definitionId);


	@DoNotAddRequestMapping
	public List<ReconciliationFilter> getReconciliationFilterListByDefinitionIdAndActiveOnDate(int definitionId, Date activeOnDate);


	public ReconciliationFilter saveReconciliationFilter(ReconciliationFilter reconciliationFilter);


	public void deleteReconciliationFilter(int id);
}
