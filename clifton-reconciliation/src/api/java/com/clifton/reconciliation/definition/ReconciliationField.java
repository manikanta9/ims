package com.clifton.reconciliation.definition;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.shared.dataaccess.DataTypes;


/**
 * The <code>ReconciliationField</code> defines the base configuration of fields that can be used as <code>ReconciliationDefinitionField</code>
 * It centralizes the base configuration for fields including the name, code, and display width for reusable fields.
 *
 * @author StevenF on 11/22/2016.
 */
public class ReconciliationField extends NamedEntity<Integer> {

	//Common fields used to drive custom logic in some cases
	public static final String HOLDING_ACCOUNT_FIELD = "Holding Account";
	public static final String INVESTMENT_SECURITY_FIELD = "Investment Security";
	public static final String POSTING_DATE_FIELD = "Posting Date";
	public static final String CASH_BALANCE = "Cash Balance";
	public static final String ADJUSTMENT_AMOUNT = "Adjustment Amount";
	public static final String ADJUSTMENT_AMOUNT_DIFF = "Adjustment Amount Diff";

	private DataTypes defaultDataType;

	/**
	 * The width of the field when displayed on the UI.
	 */
	private int defaultDisplayWidth;

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public DataTypes getDefaultDataType() {
		return this.defaultDataType;
	}


	public void setDefaultDataType(DataTypes defaultDataType) {
		this.defaultDataType = defaultDataType;
	}


	public int getDefaultDisplayWidth() {
		return this.defaultDisplayWidth;
	}


	public void setDefaultDisplayWidth(int defaultDisplayWidth) {
		this.defaultDisplayWidth = defaultDisplayWidth;
	}
}
