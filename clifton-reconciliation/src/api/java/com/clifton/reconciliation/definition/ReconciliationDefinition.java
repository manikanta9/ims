package com.clifton.reconciliation.definition;

import com.clifton.calendar.setup.Calendar;
import com.clifton.core.dataaccess.dao.OneToManyEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.util.ObjectUtils;
import com.clifton.reconciliation.definition.group.ReconciliationDefinitionGroup;
import com.clifton.reconciliation.definition.preprocessor.ReconciliationDefinitionPreprocessor;
import com.clifton.reconciliation.definition.rule.ReconciliationFilter;
import com.clifton.reconciliation.definition.type.ReconciliationType;
import com.clifton.reconciliation.rule.ReconciliationRuleGroup;
import com.clifton.system.condition.SystemCondition;
import com.clifton.workflow.BasedNamedWorkflowAwareEntity;

import java.util.List;


/**
 * The <code>ReconciliationDefinition</code> objects are the main configuration objects for reconciliation.
 * They hold a reference to all applicable {@link ReconciliationFilter}(s), and {@link ReconciliationDefinitionField}(s)
 * used during the loading and reconciliation of data.
 * <p>
 * A <code>ReconciliationDefinition</code> is associate with a ReconciliationRun during import and specifies what
 * {@link ReconciliationSource}(s) are supported, how to handle orphaned data, what category the data applies to,
 * and what filters should be applied to imported data.
 *
 * @author StevenF
 */
@CacheByName
public class ReconciliationDefinition extends BasedNamedWorkflowAwareEntity {

	private ReconciliationType type;

	// populated for Cash Reconciliations when the Transactions are carried over from one run to the next
	private ReconciliationDefinition parentDefinition;

	private ReconciliationRuleGroup primaryRuleGroup;

	private ReconciliationRuleGroup secondaryRuleGroup;

	private ReconciliationSource primarySource;

	private ReconciliationSource secondarySource;

	private ReconciliationDefinitionGroup definitionGroup;

	private SystemCondition runApprovalSystemCondition;

	private SystemCondition runModifySystemCondition;

	private Calendar calendar;

	private String contactEmail;

	@OneToManyEntity(serviceBeanName = "reconciliationDefinitionService", serviceMethodName = "getReconciliationDefinitionPreprocessorListByDefinitionId")
	private List<ReconciliationDefinitionPreprocessor> preprocessorList;

	@OneToManyEntity(serviceBeanName = "reconciliationFilterService", serviceMethodName = "getReconciliationFilterListByDefinitionId")
	private List<ReconciliationFilter> reconciliationFilterList;

	@OneToManyEntity(serviceBeanName = "reconciliationDefinitionService", serviceMethodName = "getReconciliationDefinitionFieldListByDefinitionId")
	private List<ReconciliationDefinitionField> reconciliationDefinitionFieldList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isParent() {
		return getParentDefinition() == null;
	}


	public SystemCondition getCoalesceRunApprovalSystemCondition() {
		return ObjectUtils.coalesce(getRunApprovalSystemCondition(), getDefinitionGroup() != null ? getDefinitionGroup().getRunApprovalSystemCondition() : null, getType() != null ? getType().getRunApprovalSystemCondition() : null);
	}


	public SystemCondition getCoalesceRunModifySystemCondition() {
		return ObjectUtils.coalesce(getRunModifySystemCondition(), getDefinitionGroup() != null ? getDefinitionGroup().getRunModifySystemCondition() : null, getType() != null ? getType().getRunModifySystemCondition() : null);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ReconciliationType getType() {
		return this.type;
	}


	public void setType(ReconciliationType type) {
		this.type = type;
	}


	public ReconciliationRuleGroup getPrimaryRuleGroup() {
		return this.primaryRuleGroup;
	}


	public void setPrimaryRuleGroup(ReconciliationRuleGroup primaryRuleGroup) {
		this.primaryRuleGroup = primaryRuleGroup;
	}


	public ReconciliationRuleGroup getSecondaryRuleGroup() {
		return this.secondaryRuleGroup;
	}


	public void setSecondaryRuleGroup(ReconciliationRuleGroup secondaryRuleGroup) {
		this.secondaryRuleGroup = secondaryRuleGroup;
	}


	public ReconciliationSource getPrimarySource() {
		return this.primarySource;
	}


	public void setPrimarySource(ReconciliationSource primarySource) {
		this.primarySource = primarySource;
	}


	public ReconciliationSource getSecondarySource() {
		return this.secondarySource;
	}


	public void setSecondarySource(ReconciliationSource secondarySource) {
		this.secondarySource = secondarySource;
	}


	public ReconciliationDefinitionGroup getDefinitionGroup() {
		return this.definitionGroup;
	}


	public void setDefinitionGroup(ReconciliationDefinitionGroup definitionGroup) {
		this.definitionGroup = definitionGroup;
	}


	public List<ReconciliationDefinitionPreprocessor> getPreprocessorList() {
		return this.preprocessorList;
	}


	public void setPreprocessorList(List<ReconciliationDefinitionPreprocessor> preprocessorList) {
		this.preprocessorList = preprocessorList;
	}


	public List<ReconciliationFilter> getReconciliationFilterList() {
		return this.reconciliationFilterList;
	}


	public void setReconciliationFilterList(List<ReconciliationFilter> reconciliationFilterList) {
		this.reconciliationFilterList = reconciliationFilterList;
	}


	public List<ReconciliationDefinitionField> getReconciliationDefinitionFieldList() {
		return this.reconciliationDefinitionFieldList;
	}


	public void setReconciliationDefinitionFieldList(List<ReconciliationDefinitionField> reconciliationDefinitionFieldList) {
		this.reconciliationDefinitionFieldList = reconciliationDefinitionFieldList;
	}


	public ReconciliationDefinition getParentDefinition() {
		return this.parentDefinition;
	}


	public void setParentDefinition(ReconciliationDefinition parentDefinition) {
		this.parentDefinition = parentDefinition;
	}


	public SystemCondition getRunApprovalSystemCondition() {
		return this.runApprovalSystemCondition;
	}


	public void setRunApprovalSystemCondition(SystemCondition runApprovalSystemCondition) {
		this.runApprovalSystemCondition = runApprovalSystemCondition;
	}


	public SystemCondition getRunModifySystemCondition() {
		return this.runModifySystemCondition;
	}


	public void setRunModifySystemCondition(SystemCondition runModifySystemCondition) {
		this.runModifySystemCondition = runModifySystemCondition;
	}


	public Calendar getCalendar() {
		return this.calendar;
	}


	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}


	public String getContactEmail() {
		return this.contactEmail;
	}


	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
}
