package com.clifton.reconciliation.definition.search;

import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author StevenF on 11/22/2016.
 */
public class ReconciliationFieldSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String name;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String description;

	@SearchField
	private String label;

	@SearchField
	private Integer defaultDisplayWidth;

	@SearchField
	private DataTypes defaultDataType;

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getDefaultDisplayWidth() {
		return this.defaultDisplayWidth;
	}


	public void setDefaultDisplayWidth(Integer defaultDisplayWidth) {
		this.defaultDisplayWidth = defaultDisplayWidth;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public DataTypes getDefaultDataType() {
		return this.defaultDataType;
	}


	public void setDefaultDataType(DataTypes defaultDataType) {
		this.defaultDataType = defaultDataType;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
