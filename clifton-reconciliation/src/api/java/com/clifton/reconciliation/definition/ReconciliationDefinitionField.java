package com.clifton.reconciliation.definition;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.OneToManyEntity;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.json.condition.evaluator.util.JsonConditionEvaluatorUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluator;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The <code>ReconciliationDefinitionField</code> defines how fields should be 'normalized' and processed from various sources.
 * Contains a list of definitionFieldMappings that specify the name of a field from an external source that should map to this
 * definition fieldName (e.g. Data loaded for Goldman Sachs from IMS may use 'Open Qty' as the fieldName in the loaded data; but data
 * from external file may use 'Notional'; but both values should use 'Quantity' as their fieldName during the reconciliation process.
 * These mappings allow for the fieldName normalization.
 * <p>
 * Indirectly, ReconciliationEvaluator(s) are associated with a ReconciliationDefinitionField
 * and are executed when the reconciliation process is run for a given date and definition.
 *
 * @author StevenF on 11/22/2016.
 */
public class ReconciliationDefinitionField extends BaseEntity<Integer> {

	private ReconciliationField field;

	private Integer displayOrder;

	private Date startDate;
	private Date endDate;

	private boolean hidden;
	private boolean naturalKey;
	/**
	 * Specifies if this field should compare values from both sources to determine if they are reconciled.
	 */
	private boolean reconcilable;
	/**
	 * Used to determine if adjustments can be made for the definition field
	 */
	private boolean adjustable;
	/**
	 * Specifies if this field should be summed during the reconciliation data load when multiple source data line items
	 * are loaded for the same natural key identifier (e.g. an external file may contain lot level details for the same
	 * holding account/security that should then sum their quantities into a single row of data.
	 */
	private boolean aggregate;

	/**
	 * The dataType of the field (e.g. DESCRIPTION, MONEY, PRICE, etc)
	 * Default is specified on the 'ReconciliationField' but can be overridden here.
	 */
	private DataTypes dataType;
	/**
	 * The displayWidth of the field for display
	 * Default is specified on the 'ReconciliationField' but can be overridden here.
	 */
	private Integer displayWidth;

	private ReconciliationDefinition reconciliationDefinition;

	private List<ReconciliationDefinitionFieldMapping> reconciliationDefinitionFieldMappingList;

	@OneToManyEntity(serviceBeanName = "reconciliationEvaluatorService", serviceMethodName = "getReconciliationEvaluatorListByDefinitionFieldId")
	private List<ReconciliationEvaluator> reconciliationEvaluatorList;

	////////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return isActiveOnDate(new Date());
	}


	public boolean isActiveOnDate(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}


	public ReconciliationDefinitionFieldMapping getReconciliationDefinitionFieldMapping(String source, Map<String, Object> data) {
		List<ReconciliationDefinitionFieldMapping> definitionFieldMappingList = CollectionUtils.getStream(getReconciliationDefinitionFieldMappingList()).filter(mapping -> mapping.getSource().getName().equals(source)).collect(Collectors.toList());
		for (ReconciliationDefinitionFieldMapping fieldMapping : CollectionUtils.getIterable(definitionFieldMappingList)) {
			if (fieldMapping.getConditionRule() != null) {
				if (JsonConditionEvaluatorUtils.evaluateCondition(fieldMapping.getConditionRule().getJsonExpression(), data)) {
					return fieldMapping;
				}
			}
			else {
				//If there is no condition, it always matches.
				return fieldMapping;
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationField getField() {
		return this.field;
	}


	public void setField(ReconciliationField field) {
		this.field = field;
	}


	public Integer getDisplayOrder() {
		return this.displayOrder;
	}


	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Integer getDisplayWidth() {
		return this.displayWidth;
	}


	public void setDisplayWidth(Integer displayWidth) {
		this.displayWidth = displayWidth;
	}


	public boolean isHidden() {
		return this.hidden;
	}


	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}


	public boolean isNaturalKey() {
		return this.naturalKey;
	}


	public void setNaturalKey(boolean naturalKey) {
		this.naturalKey = naturalKey;
	}


	public boolean isReconcilable() {
		return this.reconcilable;
	}


	public void setReconcilable(boolean reconcilable) {
		this.reconcilable = reconcilable;
	}


	public boolean isAdjustable() {
		return this.adjustable;
	}


	public void setAdjustable(boolean adjustable) {
		this.adjustable = adjustable;
	}


	public boolean isAggregate() {
		return this.aggregate;
	}


	public void setAggregate(boolean aggregate) {
		this.aggregate = aggregate;
	}


	public ReconciliationDefinition getReconciliationDefinition() {
		return this.reconciliationDefinition;
	}


	public void setReconciliationDefinition(ReconciliationDefinition reconciliationDefinition) {
		this.reconciliationDefinition = reconciliationDefinition;
	}


	public DataTypes getDataType() {
		return this.dataType;
	}


	public void setDataType(DataTypes dataType) {
		this.dataType = dataType;
	}


	public List<ReconciliationDefinitionFieldMapping> getReconciliationDefinitionFieldMappingList() {
		return this.reconciliationDefinitionFieldMappingList;
	}


	public void setReconciliationDefinitionFieldMappingList(List<ReconciliationDefinitionFieldMapping> reconciliationDefinitionFieldMappingList) {
		this.reconciliationDefinitionFieldMappingList = reconciliationDefinitionFieldMappingList;
	}


	public List<ReconciliationEvaluator> getReconciliationEvaluatorList() {
		return this.reconciliationEvaluatorList;
	}


	public void setReconciliationEvaluatorList(List<ReconciliationEvaluator> reconciliationEvaluatorList) {
		this.reconciliationEvaluatorList = reconciliationEvaluatorList;
	}
}
