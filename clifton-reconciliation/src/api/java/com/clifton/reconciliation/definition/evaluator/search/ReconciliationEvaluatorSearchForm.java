package com.clifton.reconciliation.definition.evaluator.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * @author StevenF
 */
public class ReconciliationEvaluatorSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField
	private String name;

	@SearchField(searchFieldPath = "reconciliationInvestmentAccount", searchField = "accountNumber")
	private String accountNumber;

	@SearchField(searchFieldPath = "definitionField", searchField = "reconciliationDefinition.id")
	private Integer definitionId;

	@SearchField(searchFieldPath = "definitionField.reconciliationDefinition", searchField = "name")
	private String definitionName;

	@SearchField(searchField = "definitionField.id")
	private Integer definitionFieldId;

	@SearchField(searchFieldPath = "definitionField.field", searchField = "name")
	private String definitionFieldName;

	@SearchField(searchField = "evaluatorBean.id")
	private Integer evaluatorBeanId;

	@SearchField(searchFieldPath = "evaluatorBean", searchField = "name")
	private String evaluatorBeanName;

	@SearchField
	private Integer evaluationOrder;

	@SearchField(searchFieldPath = "reconciliationInvestmentSecurity", searchField = "securitySymbol")
	private String securitySymbol;

	@SearchField(searchField = "conditionRule.name")
	private String conditionRuleName;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public Integer getDefinitionFieldId() {
		return this.definitionFieldId;
	}


	public void setDefinitionFieldId(Integer definitionFieldId) {
		this.definitionFieldId = definitionFieldId;
	}


	public String getDefinitionFieldName() {
		return this.definitionFieldName;
	}


	public void setDefinitionFieldName(String definitionFieldName) {
		this.definitionFieldName = definitionFieldName;
	}


	public Integer getEvaluatorBeanId() {
		return this.evaluatorBeanId;
	}


	public void setEvaluatorBeanId(Integer evaluatorBeanId) {
		this.evaluatorBeanId = evaluatorBeanId;
	}


	public String getEvaluatorBeanName() {
		return this.evaluatorBeanName;
	}


	public void setEvaluatorBeanName(String evaluatorBeanName) {
		this.evaluatorBeanName = evaluatorBeanName;
	}


	public Integer getEvaluationOrder() {
		return this.evaluationOrder;
	}


	public void setEvaluationOrder(Integer evaluationOrder) {
		this.evaluationOrder = evaluationOrder;
	}


	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	public String getConditionRuleName() {
		return this.conditionRuleName;
	}


	public void setConditionRuleName(String conditionRuleName) {
		this.conditionRuleName = conditionRuleName;
	}
}
