package com.clifton.reconciliation.definition.type.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author stevenf
 */
public class ReconciliationTypeRequiredNaturalKeySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "reconciliationType.id")
	private Short typeId;

	@SearchField(searchFieldPath = "reconciliationType", searchField = "name")
	private String typeName;

	@SearchField(searchField = "reconciliationField.id")
	private Integer fieldId;

	@SearchField(searchFieldPath = "reconciliationField", searchField = "name")
	private String fieldName;

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Integer getFieldId() {
		return this.fieldId;
	}


	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}


	public String getFieldName() {
		return this.fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
}
