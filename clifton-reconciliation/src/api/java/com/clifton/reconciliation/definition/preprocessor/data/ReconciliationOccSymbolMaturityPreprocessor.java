package com.clifton.reconciliation.definition.preprocessor.data;

import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.SecurityIdentifierUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.reconciliation.simpletable.SimpleTable;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


/**
 * <code>ReconciliationOccSymbolMaturityPreprocessor</code> parse the maturity date from the OCC symbol.
 *
 * @author TerryS
 */
public class ReconciliationOccSymbolMaturityPreprocessor implements ReconciliationDefinitionDataPreprocessor {

	private static final DateTimeFormatter OUTPUT_DATE_FORMATTER = DateTimeFormatter.ofPattern("MM/dd/uuuu");

	private String securityIdentifierColumn;
	private String maturityDateColumn;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		// noop
	}


	@Override
	public void accept(SimpleTable simpleTable, int rowIndex) {
		Object securityIdentifier = simpleTable.getColumnValue(ObjectUtils.coalesce(getSecurityIdentifierColumn(), ADVENT_SECURITY_IDENTIFIER_COLUMN), rowIndex);
		if (securityIdentifier != null) {
			String security = (String) securityIdentifier;
			if (!StringUtils.isEmpty(security) && SecurityIdentifierUtils.isValidOccSymbol(security, false)) {
				LocalDate expirationDate = SecurityIdentifierUtils.getOccSymbolExpirationDate(security, false);
				if (expirationDate != null) {
					simpleTable.updateColumnValue(ObjectUtils.coalesce(getMaturityDateColumn(), ADVENT_MATURITY_DATE_COLUMN), rowIndex, OUTPUT_DATE_FORMATTER.format(expirationDate));
				}
			}
			else {
				LocalDate expirationDate = SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible(security, false);
				if (expirationDate != null) {
					simpleTable.updateColumnValue(ObjectUtils.coalesce(getMaturityDateColumn(), ADVENT_MATURITY_DATE_COLUMN), rowIndex, OUTPUT_DATE_FORMATTER.format(expirationDate));
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSecurityIdentifierColumn() {
		return this.securityIdentifierColumn;
	}


	public void setSecurityIdentifierColumn(String securityIdentifierColumn) {
		this.securityIdentifierColumn = securityIdentifierColumn;
	}


	public String getMaturityDateColumn() {
		return this.maturityDateColumn;
	}


	public void setMaturityDateColumn(String maturityDateColumn) {
		this.maturityDateColumn = maturityDateColumn;
	}
}
