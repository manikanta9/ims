package com.clifton.reconciliation.definition;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>ReconciliationSource</code> determines the system, broker, or custodian from which data was
 * loaded into the reconciliation system.
 *
 * @author StevenF
 */
@CacheByName
public class ReconciliationSource extends NamedEntity<Short> implements Comparable<ReconciliationSource> {

	public static final String DEFAULT_SOURCE = "IMS";

	/**
	 * Short Name is used for display purposes
	 * ex: in the toolbar labels when they can reconcile on a source to read 'Use IMS Amount'
	 * Used in ReconciliationRunWindow.js and ReconciliationRunDualViewWindow.js
	 */
	private String shortName;
	private boolean defaultSource;


	@Override
	public int compareTo(ReconciliationSource o) {
		return this.getId().compareTo(o.getId());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getShortName() {
		return this.shortName;
	}


	public void setShortName(String shortName) {
		this.shortName = shortName;
	}


	public boolean isDefaultSource() {
		return this.defaultSource;
	}


	public void setDefaultSource(boolean defaultSource) {
		this.defaultSource = defaultSource;
	}
}
