package com.clifton.reconciliation.definition.type;

import com.clifton.core.beans.BaseEntity;
import com.clifton.reconciliation.definition.ReconciliationField;


/**
 * The <code>ReconciliationTypeRequiredNaturalKey</code> defines which {@link ReconciliationField}s are required to be
 * defined on a definition of a given type as well as set to be 'Natural Keys' for the run created from the definition.
 *
 * @author stevenf
 */
public class ReconciliationTypeRequiredNaturalKey extends BaseEntity<Short> {

	private ReconciliationType reconciliationType;

	private ReconciliationField reconciliationField;

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationType getReconciliationType() {
		return this.reconciliationType;
	}


	public void setReconciliationType(ReconciliationType reconciliationType) {
		this.reconciliationType = reconciliationType;
	}


	public ReconciliationField getReconciliationField() {
		return this.reconciliationField;
	}


	public void setReconciliationField(ReconciliationField reconciliationField) {
		this.reconciliationField = reconciliationField;
	}
}
