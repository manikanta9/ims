package com.clifton.reconciliation.definition.group;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.condition.SystemCondition;


/**
 * <code>ReconciliationDefinitionGroup</code> group {@link com.clifton.reconciliation.definition.ReconciliationDefinition} entities together, usually for determining if
 * account locking events should occur for definitions in the group.
 *
 * @author TerryS
 */
@CacheByName
public class ReconciliationDefinitionGroup extends NamedEntity<Integer> {

	private boolean eventTrigger;

	private SystemCondition runApprovalSystemCondition;

	private SystemCondition runModifySystemCondition;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public boolean isEventTrigger() {
		return this.eventTrigger;
	}


	public void setEventTrigger(boolean eventTrigger) {
		this.eventTrigger = eventTrigger;
	}


	public SystemCondition getRunApprovalSystemCondition() {
		return this.runApprovalSystemCondition;
	}


	public void setRunApprovalSystemCondition(SystemCondition runApprovalSystemCondition) {
		this.runApprovalSystemCondition = runApprovalSystemCondition;
	}


	public SystemCondition getRunModifySystemCondition() {
		return this.runModifySystemCondition;
	}


	public void setRunModifySystemCondition(SystemCondition runModifySystemCondition) {
		this.runModifySystemCondition = runModifySystemCondition;
	}
}
