package com.clifton.reconciliation.definition.preprocessor;

import com.clifton.core.beans.BaseEntity;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.system.bean.SystemBean;


/**
 * The ReconciliationDefinitionPreprocessor class is used to manipulate incoming data prior to applying filters and field mappings.
 */
public class ReconciliationDefinitionPreprocessor extends BaseEntity<Short> {

	private ReconciliationDefinition reconciliationDefinition;

	private SystemBean preprocessor;

	private int evaluationOrder;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ReconciliationDefinition getReconciliationDefinition() {
		return this.reconciliationDefinition;
	}


	public void setReconciliationDefinition(ReconciliationDefinition reconciliationDefinition) {
		this.reconciliationDefinition = reconciliationDefinition;
	}


	public SystemBean getPreprocessor() {
		return this.preprocessor;
	}


	public void setPreprocessor(SystemBean preprocessor) {
		this.preprocessor = preprocessor;
	}


	public int getEvaluationOrder() {
		return this.evaluationOrder;
	}


	public void setEvaluationOrder(int evaluationOrder) {
		this.evaluationOrder = evaluationOrder;
	}
}
