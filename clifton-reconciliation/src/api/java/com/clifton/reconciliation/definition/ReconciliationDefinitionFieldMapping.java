package com.clifton.reconciliation.definition;

import com.clifton.core.beans.NamedEntity;
import com.clifton.reconciliation.rule.ReconciliationRuleCondition;
import com.clifton.reconciliation.rule.ReconciliationRuleFunction;


/**
 * The <code>ReconciliationDefinitionFieldMapping</code> determines how/when a field should be mapped to a definitionField name.
 * A fieldCondition, if it exists, is first executed to determine if a mapping should be applied; if the condition passes, then
 * the functionRule, if it exists, will be applied.  i.e. a definition field mapping for quantity may have a condition such as:
 * (psuedo-code) if 'tradeType' <equals> 'sell'
 * If that condition passes, then the function:
 * (psuedo-code) negate(quantity)
 * will be applied.
 * This allows for multiple mappings to be created for the same field and same source, with conditional logic used to determine
 * if a given field mapping applies; field mappings are evaluated in the order specified by the 'evaluationOrder' value.
 *
 * @author StevenF on 11/22/2016.
 */
public class ReconciliationDefinitionFieldMapping extends NamedEntity<Integer> {

	/**
	 * An optional condition used to determine if a fieldMapping should be applied to the given data.
	 */
	private ReconciliationRuleCondition conditionRule;

	/**
	 * An optional function to be applied on the field if the conditionRule was null or returned true
	 */
	private ReconciliationRuleFunction functionRule;


	private ReconciliationDefinitionField definitionField;
	private ReconciliationSource source;

	/**
	 * Determines the order in which this fieldMapping should be evaluated
	 */
	private Integer evaluationOrder;

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationRuleCondition getConditionRule() {
		return this.conditionRule;
	}


	public void setConditionRule(ReconciliationRuleCondition conditionRule) {
		this.conditionRule = conditionRule;
	}


	public ReconciliationRuleFunction getFunctionRule() {
		return this.functionRule;
	}


	public void setFunctionRule(ReconciliationRuleFunction functionRule) {
		this.functionRule = functionRule;
	}


	public ReconciliationDefinitionField getDefinitionField() {
		return this.definitionField;
	}


	public void setDefinitionField(ReconciliationDefinitionField definitionField) {
		this.definitionField = definitionField;
	}


	public ReconciliationSource getSource() {
		return this.source;
	}


	public void setSource(ReconciliationSource source) {
		this.source = source;
	}


	public Integer getEvaluationOrder() {
		return this.evaluationOrder;
	}


	public void setEvaluationOrder(Integer evaluationOrder) {
		this.evaluationOrder = evaluationOrder;
	}
}
