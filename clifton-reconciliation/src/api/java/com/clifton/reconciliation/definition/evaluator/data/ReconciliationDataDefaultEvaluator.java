package com.clifton.reconciliation.definition.evaluator.data;

import com.clifton.core.util.compare.CompareUtils;
import com.clifton.reconciliation.run.data.DataError;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * @author StevenF
 */
public class ReconciliationDataDefaultEvaluator extends BaseReconciliationDataEvaluator<Object> {

	@Override
	public DataError evaluateData(Object primaryValue, Object secondaryValue) {
		DataError error = null;
		if (primaryValue instanceof BigDecimal && secondaryValue instanceof BigDecimal) {
			if (!MathUtils.isEqual((BigDecimal) primaryValue, (BigDecimal) secondaryValue)) {
				error = new DataError(getDefinitionField(), primaryValue, secondaryValue);
			}
		}
		else if (!CompareUtils.isEqual(primaryValue, secondaryValue)) {
			error = new DataError(getDefinitionField(), primaryValue, secondaryValue);
		}
		return error;
	}
}
