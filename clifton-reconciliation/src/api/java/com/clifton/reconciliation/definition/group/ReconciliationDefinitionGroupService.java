package com.clifton.reconciliation.definition.group;

import com.clifton.reconciliation.definition.group.search.ReconciliationDefinitionGroupSearchForm;

import java.util.List;


/**
 * <code>ReconciliationDefinitionGroupService</code> business service methods for handling {@link ReconciliationDefinitionGroup} entities.
 *
 * @author TerryS
 */
public interface ReconciliationDefinitionGroupService {

	////////////////////////////////////////////////////////////////////////////
	////////      Reconciliation Definition Group Business Methods     /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationDefinitionGroup getReconciliationDefinitionGroup(int id);


	public List<ReconciliationDefinitionGroup> getReconciliationDefinitionGroupList(final ReconciliationDefinitionGroupSearchForm searchForm);


	public ReconciliationDefinitionGroup saveReconciliationDefinitionGroup(ReconciliationDefinitionGroup bean);


	public void deleteReconciliationDefinitionGroup(int id);
}
