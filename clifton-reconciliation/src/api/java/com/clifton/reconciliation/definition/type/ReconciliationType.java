package com.clifton.reconciliation.definition.type;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.OneToManyEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.condition.SystemCondition;

import java.util.List;


/**
 * The <code>ReconciliationType</code> is the highest level categorization of a reconciliation run.
 * It is used to define how a run is processed and when/if it can be approved/modified
 *
 * @author stevenf
 */
@CacheByName
public class ReconciliationType extends NamedEntity<Short> {

	public static final String ACTIVITY = "Activity Reconciliation";
	public static final String CASH = "Cash Reconciliation";
	public static final String POSITION = "Position Reconciliation";
	public static final String TRADE = "Trade Reconciliation";
	public static final String TRANSACTION = "Transaction Reconciliation";

	private boolean cumulativeReconciliation;

	private SystemCondition runApprovalSystemCondition;

	private SystemCondition runModifySystemCondition;

	@OneToManyEntity(serviceBeanName = "reconciliationTypeService", serviceMethodName = "getReconciliationTypeRequiredNaturalKeyListByTypeId")
	private List<ReconciliationTypeRequiredNaturalKey> requiredNaturalKeyList;

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isCumulativeReconciliation() {
		return this.cumulativeReconciliation;
	}


	public void setCumulativeReconciliation(boolean cumulativeReconciliation) {
		this.cumulativeReconciliation = cumulativeReconciliation;
	}


	public SystemCondition getRunApprovalSystemCondition() {
		return this.runApprovalSystemCondition;
	}


	public void setRunApprovalSystemCondition(SystemCondition runApprovalSystemCondition) {
		this.runApprovalSystemCondition = runApprovalSystemCondition;
	}


	public SystemCondition getRunModifySystemCondition() {
		return this.runModifySystemCondition;
	}


	public void setRunModifySystemCondition(SystemCondition runModifySystemCondition) {
		this.runModifySystemCondition = runModifySystemCondition;
	}


	public List<ReconciliationTypeRequiredNaturalKey> getRequiredNaturalKeyList() {
		return this.requiredNaturalKeyList;
	}


	public void setRequiredNaturalKeyList(List<ReconciliationTypeRequiredNaturalKey> requiredNaturalKeyList) {
		this.requiredNaturalKeyList = requiredNaturalKeyList;
	}
}
