package com.clifton.reconciliation.definition;

import com.clifton.calendar.setup.Calendar;
import com.clifton.reconciliation.definition.group.ReconciliationDefinitionGroup;
import com.clifton.reconciliation.definition.preprocessor.ReconciliationDefinitionPreprocessor;
import com.clifton.reconciliation.definition.rule.ReconciliationFilter;
import com.clifton.reconciliation.definition.type.ReconciliationType;
import com.clifton.reconciliation.rule.ReconciliationRuleGroup;
import com.clifton.system.condition.SystemCondition;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.List;


/**
 * @author stevenf
 */
public final class ReconciliationDefinitionBuilder {

	private String label;
	private String name;
	private String description;
	private WorkflowState workflowState;
	private WorkflowStatus workflowStatus;
	private ReconciliationType type;
	// populated for Cash Reconciliations when the Transactions are carried over from one run to the next
	private ReconciliationDefinition parentDefinition;
	private ReconciliationRuleGroup primaryRuleGroup;
	private ReconciliationRuleGroup secondaryRuleGroup;
	private ReconciliationSource primarySource;
	private ReconciliationSource secondarySource;
	private ReconciliationDefinitionGroup definitionGroup;
	private SystemCondition runApprovalSystemCondition;
	private SystemCondition runModifySystemCondition;
	private Calendar calendar;
	private List<ReconciliationDefinitionPreprocessor> preprocessorList;
	private List<ReconciliationFilter> reconciliationFilterList;
	private List<ReconciliationDefinitionField> reconciliationDefinitionFieldList;


	private ReconciliationDefinitionBuilder() {
	}


	public static ReconciliationDefinitionBuilder aReconciliationDefinition() {
		return new ReconciliationDefinitionBuilder();
	}


	public ReconciliationDefinitionBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public ReconciliationDefinitionBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationDefinitionBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public ReconciliationDefinitionBuilder withWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
		return this;
	}


	public ReconciliationDefinitionBuilder withWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
		return this;
	}


	public ReconciliationDefinitionBuilder withType(ReconciliationType type) {
		this.type = type;
		return this;
	}


	public ReconciliationDefinitionBuilder withParentDefinition(ReconciliationDefinition parentDefinition) {
		this.parentDefinition = parentDefinition;
		return this;
	}


	public ReconciliationDefinitionBuilder withPrimaryRuleGroup(ReconciliationRuleGroup primaryRuleGroup) {
		this.primaryRuleGroup = primaryRuleGroup;
		return this;
	}


	public ReconciliationDefinitionBuilder withSecondaryRuleGroup(ReconciliationRuleGroup secondaryRuleGroup) {
		this.secondaryRuleGroup = secondaryRuleGroup;
		return this;
	}


	public ReconciliationDefinitionBuilder withPrimarySource(ReconciliationSource primarySource) {
		this.primarySource = primarySource;
		return this;
	}


	public ReconciliationDefinitionBuilder withSecondarySource(ReconciliationSource secondarySource) {
		this.secondarySource = secondarySource;
		return this;
	}


	public ReconciliationDefinitionBuilder withDefinitionGroup(ReconciliationDefinitionGroup definitionGroup) {
		this.definitionGroup = definitionGroup;
		return this;
	}


	public ReconciliationDefinitionBuilder withRunApprovalSystemCondition(SystemCondition runApprovalSystemCondition) {
		this.runApprovalSystemCondition = runApprovalSystemCondition;
		return this;
	}


	public ReconciliationDefinitionBuilder withRunModifySystemCondition(SystemCondition runModifySystemCondition) {
		this.runModifySystemCondition = runModifySystemCondition;
		return this;
	}


	public ReconciliationDefinitionBuilder withCalendar(Calendar calendar) {
		this.calendar = calendar;
		return this;
	}


	public ReconciliationDefinitionBuilder withPreprocessorList(List<ReconciliationDefinitionPreprocessor> preprocessorList) {
		this.preprocessorList = preprocessorList;
		return this;
	}


	public ReconciliationDefinitionBuilder withReconciliationFilterList(List<ReconciliationFilter> reconciliationFilterList) {
		this.reconciliationFilterList = reconciliationFilterList;
		return this;
	}


	public ReconciliationDefinitionBuilder withReconciliationDefinitionFieldList(List<ReconciliationDefinitionField> reconciliationDefinitionFieldList) {
		this.reconciliationDefinitionFieldList = reconciliationDefinitionFieldList;
		return this;
	}


	public ReconciliationDefinition build() {
		ReconciliationDefinition reconciliationDefinition = new ReconciliationDefinition();
		reconciliationDefinition.setLabel(this.label);
		reconciliationDefinition.setName(this.name);
		reconciliationDefinition.setDescription(this.description);
		reconciliationDefinition.setWorkflowState(this.workflowState);
		reconciliationDefinition.setWorkflowStatus(this.workflowStatus);
		reconciliationDefinition.setType(this.type);
		reconciliationDefinition.setParentDefinition(this.parentDefinition);
		reconciliationDefinition.setPrimaryRuleGroup(this.primaryRuleGroup);
		reconciliationDefinition.setSecondaryRuleGroup(this.secondaryRuleGroup);
		reconciliationDefinition.setPrimarySource(this.primarySource);
		reconciliationDefinition.setSecondarySource(this.secondarySource);
		reconciliationDefinition.setDefinitionGroup(this.definitionGroup);
		reconciliationDefinition.setRunApprovalSystemCondition(this.runApprovalSystemCondition);
		reconciliationDefinition.setRunModifySystemCondition(this.runModifySystemCondition);
		reconciliationDefinition.setCalendar(this.calendar);
		reconciliationDefinition.setPreprocessorList(this.preprocessorList);
		reconciliationDefinition.setReconciliationFilterList(this.reconciliationFilterList);
		reconciliationDefinition.setReconciliationDefinitionFieldList(this.reconciliationDefinitionFieldList);
		return reconciliationDefinition;
	}
}
