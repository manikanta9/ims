package com.clifton.reconciliation;

import com.clifton.core.context.DoNotAddRequestMapping;


/**
 * @author stevenf
 */
public interface ReconciliationMigrationHandler {

	@DoNotAddRequestMapping
	public void doReconciliationRunObjectUpdates();


	@DoNotAddRequestMapping
	public void doReconciliationEventObjectDataMigration();


	@DoNotAddRequestMapping
	public void doReconciliationRunCountUpdates();
}
