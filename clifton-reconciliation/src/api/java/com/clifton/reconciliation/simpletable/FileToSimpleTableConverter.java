package com.clifton.reconciliation.simpletable;

import com.clifton.core.util.converter.Converter;


/**
 * @author TerryS
 */
public interface FileToSimpleTableConverter extends Converter<Object, SimpleTable> {

}
