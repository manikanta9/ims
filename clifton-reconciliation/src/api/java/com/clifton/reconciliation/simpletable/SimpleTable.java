package com.clifton.reconciliation.simpletable;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.function.Function;
import java.util.function.ObjIntConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * <code>SimpleTableImpl</code> implementation of table, does not support duplicate column names.
 * This is not meant for UI consumption.
 *
 * @author TerryS
 */
public final class SimpleTable {

	private final List<SimpleRow> rows = Collections.synchronizedList(new ArrayList<>(1000));
	// must maintain insertion order
	private final Map<String, Integer> columns;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SimpleTable(List<String> columns) {
		AssertUtils.assertFalse(CollectionUtils.isEmpty(columns), "Table columns is required.");
		// Constructs an empty insertion-ordered instance
		this.columns = new LinkedHashMap<>(columns.size());
		for (int i = 0; i < columns.size(); i++) {
			this.columns.put(columns.get(i), i);
		}
		AssertUtils.assertEquals(columns.size(), this.columns.size(), () ->
				String.format("Duplicate columns are not allowed: [%s]", getDuplicateColumns(columns)));
	}


	private static Map<String, Long> getDuplicateColumns(List<String> columns) {
		return columns.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
				.entrySet().stream()
				.filter(e -> e.getValue() > 1)
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}


	public Map<String, Object> getObjectValueMap(int row) {
		AssertUtils.assertTrue(row < this.rows.size(), () -> String.format("Row index [%s] is out of bounds.", row));
		Map<String, Object> results = new ConcurrentHashMap<>();
		SimpleRow simpleRow = this.rows.get(row);
		// LinkedEntryIterator
		for (Map.Entry<String, Integer> entry : this.columns.entrySet()) {
			Object value = simpleRow.getValue(entry.getValue());
			results.put(entry.getKey(), value != null ? value : "");
		}
		return results;
	}


	public int getTotalRowCount() {
		return this.rows.size();
	}


	public void addRow(Object[] values) {
		AssertUtils.assertNotNull(values, "The values parameter cannot be null.");
		AssertUtils.assertEquals(values.length, this.columns.size(), () ->
				String.format("Must provide a value for each column.  Column count [%s] does not equal Row Value count [%s]", this.columns.size(), values.length));
		this.rows.add(new SimpleRow(values));
	}


	public void updateColumnValue(String column, int row, Object value) {
		AssertUtils.assertTrue(this.columns.containsKey(column), () -> String.format("Column [%s] not found [%s]", column, String.join(", ", this.columns.keySet())));
		AssertUtils.assertTrue(row < this.rows.size(), () -> String.format("Row index [%s] is out of bounds.", row));
		SimpleRow simpleRow = this.rows.get(row);
		simpleRow.updateValue(this.columns.get(column), value);
	}


	public Object getColumnValue(String column, int row) {
		if (this.columns.containsKey(column)) {
			AssertUtils.assertTrue(row < this.rows.size(), () -> String.format("Row index [%s] is out of bounds.", row));
			SimpleRow simpleRow = this.rows.get(row);
			return simpleRow.getValue(this.columns.get(column));
		}
		return null;
	}


	public void process(List<ObjIntConsumer<SimpleTable>> processors, boolean parallel) {
		IntStream stream = IntStream.range(0, getTotalRowCount());
		stream = parallel ? stream.parallel() : stream;
		stream.forEach(index -> {
			for (ObjIntConsumer<SimpleTable> processor : processors) {
				processor.accept(this, index);
			}
		});
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private static class SimpleRow {

		private final AtomicReferenceArray<Object> values;


		public SimpleRow(Object[] values) {
			this.values = new AtomicReferenceArray<>(values);
		}


		public Object getValue(int column) {
			return this.values.get(column);
		}


		public void updateValue(int column, Object value) {
			if (column >= 0 && column < this.values.length()) {
				this.values.set(column, value);
			}
		}
	}
}
