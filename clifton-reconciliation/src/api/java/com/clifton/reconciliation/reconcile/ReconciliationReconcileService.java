package com.clifton.reconciliation.reconcile;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.run.ReconciliationRun;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Date;


/**
 * <code>ReconciliationReconcileService</code> provide services reconciling.
 *
 * @author TerryS
 */
public interface ReconciliationReconcileService {

	@ModelAttribute("result")
	@SecureMethod(permissions = SecurityPermission.PERMISSION_EXECUTE)
	public Status processReconciliationRun(ReconciliationCommand command);


	public ReconciliationRun processReconciliationByDefinitionAndDate(int definitionId, Date runDate);


	@DoNotAddRequestMapping
	public ReconciliationRun reconcileReconciliationRun(ReconciliationRun run, Long[] runObjectIds);
}
