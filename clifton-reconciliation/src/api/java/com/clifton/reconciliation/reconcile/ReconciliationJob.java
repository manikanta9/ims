package com.clifton.reconciliation.reconcile;

import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinitionService;

import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * <code>ReconciliationJob</code> performs reconciliation on definitions based on the date, definition or type.
 *
 * @author TerryS
 */
public class ReconciliationJob implements Task, StatusHolderObjectAware<Status> {

	private Date reconcileDate;
	private Integer reconciliationDefinitionId;
	private Short reconciliationTypeId;
	private Integer daysFromToday = 1;
	private DateGenerationOptions dateGenerationOption;

	private StatusHolderObject<Status> statusHolderObject;
	private ReconciliationDefinitionService reconciliationDefinitionService;
	private ReconciliationReconcileService reconciliationReconcileService;
	private CalendarDateGenerationHandler calendarDateGenerationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		AssertUtils.assertNotNull(context, "Context Parameter is required.");

		if (getReconciliationDefinitionId() != null) {
			ValidationUtils.assertNotNull(getReconciliationDefinitionService().getReconciliationDefinition(getReconciliationDefinitionId()),
					() -> String.format("Reconciliation Definition [%s] could not be found.", getReconciliationDefinitionId()));
		}
		ReconciliationCommand reconcileCommand = ReconciliationCommand.ofSynchronousWithThrow();
		reconcileCommand.setStatus(getStatusHolderObject().getStatus());

		reconcileCommand.setReconciliationDefinitionId(getReconciliationDefinitionId());
		reconcileCommand.setReconciliationTypeId(getReconciliationTypeId());
		reconcileCommand.setReconcileDate(calculateReconcileDate());

		Status status = getReconciliationReconcileService().processReconciliationRun(reconcileCommand);
		String batchJobStatus = status.getDetailList().stream().map(d -> d.getCategory() + ": " + d.getNote()).collect(Collectors.joining("\n"));
		status.setMessage(batchJobStatus);
		return status;
	}


	public Date calculateReconcileDate() {
		if (this.reconcileDate == null) {
			if (getDateGenerationOption() != null) {
				return getCalendarDateGenerationHandler().generateDate(getDateGenerationOption(), getDaysFromToday());
			}
			return getCalendarDateGenerationHandler().generateDate(DateGenerationOptions.PREVIOUS_BUSINESS_DAY);
		}
		return this.reconcileDate;
	}


	public Date getReconcileDate() {
		return this.reconcileDate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setReconcileDate(Date reconcileDate) {
		this.reconcileDate = reconcileDate;
	}


	public Integer getReconciliationDefinitionId() {
		return this.reconciliationDefinitionId;
	}


	public void setReconciliationDefinitionId(Integer reconciliationDefinitionId) {
		this.reconciliationDefinitionId = reconciliationDefinitionId;
	}


	public Short getReconciliationTypeId() {
		return this.reconciliationTypeId;
	}


	public void setReconciliationTypeId(Short reconciliationTypeId) {
		this.reconciliationTypeId = reconciliationTypeId;
	}


	public Integer getDaysFromToday() {
		return this.daysFromToday;
	}


	public void setDaysFromToday(Integer daysFromToday) {
		this.daysFromToday = daysFromToday;
	}


	public DateGenerationOptions getDateGenerationOption() {
		return this.dateGenerationOption;
	}


	public void setDateGenerationOption(DateGenerationOptions dateGenerationOption) {
		this.dateGenerationOption = dateGenerationOption;
	}


	public StatusHolderObject<Status> getStatusHolderObject() {
		return this.statusHolderObject;
	}


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	public ReconciliationDefinitionService getReconciliationDefinitionService() {
		return this.reconciliationDefinitionService;
	}


	public void setReconciliationDefinitionService(ReconciliationDefinitionService reconciliationDefinitionService) {
		this.reconciliationDefinitionService = reconciliationDefinitionService;
	}


	public ReconciliationReconcileService getReconciliationReconcileService() {
		return this.reconciliationReconcileService;
	}


	public void setReconciliationReconcileService(ReconciliationReconcileService reconciliationReconcileService) {
		this.reconciliationReconcileService = reconciliationReconcileService;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}
}
