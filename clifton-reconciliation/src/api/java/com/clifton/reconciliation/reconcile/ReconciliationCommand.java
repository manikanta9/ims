package com.clifton.reconciliation.reconcile;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;

import java.util.Date;


/**
 * The <code>ReconciliationCommand</code> has options for running reconciliation in a batch job.
 *
 * @author TerryS
 */
public class ReconciliationCommand {

	private Date reconcileDate;
	private Integer reconciliationDefinitionId;
	private Short reconciliationTypeId;

	private boolean synchronous;
	private Integer asynchronousDelay;
	private boolean throwExceptionOnError;
	private Integer asynchronousRebuildDelay;

	private Status status;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static ReconciliationCommand ofSynchronousWithThrow() {
		ReconciliationCommand result = new ReconciliationCommand();
		result.setSynchronous(true);
		result.setThrowExceptionOnError(true);
		return result;
	}


	public String getRunId() {
		String runId = "";
		if (getReconciliationDefinitionId() != null) {
			runId = "DEFINITION_" + getReconciliationDefinitionId() + "_";
		}
		else if (getReconciliationTypeId() != null) {
			runId = "TYPE_" + getReconciliationTypeId() + "_";
		}
		else {
			runId += "ALL_";
		}
		runId += DateUtils.fromDate(DateUtils.clearTime(getReconcileDate()));
		return runId;
	}


	@Override
	public String toString() {
		return "ReconciliationCommand{" +
				"reconcileDate=" + this.reconcileDate +
				", reconciliationDefinitionId=" + this.reconciliationDefinitionId +
				", reconciliationType=" + this.reconciliationTypeId +
				'}';
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getReconcileDate() {
		return this.reconcileDate;
	}


	public void setReconcileDate(Date reconcileDate) {
		this.reconcileDate = reconcileDate;
	}


	public Integer getReconciliationDefinitionId() {
		return this.reconciliationDefinitionId;
	}


	public void setReconciliationDefinitionId(Integer reconciliationDefinitionId) {
		this.reconciliationDefinitionId = reconciliationDefinitionId;
	}


	public Short getReconciliationTypeId() {
		return this.reconciliationTypeId;
	}


	public void setReconciliationTypeId(Short reconciliationTypeId) {
		this.reconciliationTypeId = reconciliationTypeId;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public Integer getAsynchronousDelay() {
		return this.asynchronousDelay;
	}


	public void setAsynchronousDelay(Integer asynchronousDelay) {
		this.asynchronousDelay = asynchronousDelay;
	}


	public boolean isThrowExceptionOnError() {
		return this.throwExceptionOnError;
	}


	public void setThrowExceptionOnError(boolean throwExceptionOnError) {
		this.throwExceptionOnError = throwExceptionOnError;
	}


	public Integer getAsynchronousRebuildDelay() {
		return this.asynchronousRebuildDelay != null ? this.asynchronousRebuildDelay : 0;
	}


	public void setAsynchronousRebuildDelay(Integer asynchronousRebuildDelay) {
		this.asynchronousRebuildDelay = asynchronousRebuildDelay;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
