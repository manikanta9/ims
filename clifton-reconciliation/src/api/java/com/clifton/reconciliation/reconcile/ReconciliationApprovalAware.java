package com.clifton.reconciliation.reconcile;

import com.clifton.security.user.SecurityUser;

import java.util.Date;


/**
 * @author TerryS
 */
public interface ReconciliationApprovalAware {

	public SecurityUser getApprovalUser();


	public void setApprovalUser(SecurityUser approvalUser);


	public Date getApprovalDate();


	public void setApprovalDate(Date approvalDate);
}
