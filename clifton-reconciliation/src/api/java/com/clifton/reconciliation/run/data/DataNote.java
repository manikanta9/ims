package com.clifton.reconciliation.run.data;

import java.util.Date;


/**
 * Represents values needed for a SystemNote for ReconciliationObjects
 * Contains the valid NoteTypes and TableName
 *
 * @author KellyJ
 */
public class DataNote {

	private String note;

	/**
	 * The ReconciliationSource this note applies to.
	 * If null the note is 'global', i.e. if a note is applied to a matched object then it applies to both sources.
	 * If the item is unmatched, then global notes are added to both objects and the sourceId set on each.
	 * Notes with the same message and type are merged and the sourceId set to null when matching to remove duplicates.
	 */
	private Short sourceId;

	private Date createDate;
	private Short createUserId;

	private DataNoteTypes type;

	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public static DataNote ofNote(String note) {
		DataNote dataNote = new DataNote();
		dataNote.setNote(note);
		return dataNote;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Short getSourceId() {
		return this.sourceId;
	}


	public void setSourceId(Short sourceId) {
		this.sourceId = sourceId;
	}


	public Short getCreateUserId() {
		return this.createUserId;
	}


	public void setCreateUserId(Short createUserId) {
		this.createUserId = createUserId;
	}


	public Date getCreateDate() {
		return this.createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public DataNoteTypes getType() {
		return this.type;
	}


	public void setType(DataNoteTypes type) {
		this.type = type;
	}
}
