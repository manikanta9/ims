package com.clifton.reconciliation.run;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.definition.ReconciliationField;
import com.clifton.reconciliation.exclusion.ReconciliationExclusionAssignment;
import com.clifton.reconciliation.file.ReconciliationFileDefinition;
import com.clifton.reconciliation.run.data.DataHolder;

import java.util.Date;


/**
 * The <code>ReconciliationRunOrphanObject</code> class stores data that was not processed
 * by definitions where the 'isSaveOrphanedData' flag is set to true on the definition.  This is meant as a
 * 'catch-all' for items that may have been accidentally excluded from processing in the case the configuration
 * was too liberal in exclusion/inclusion.
 *
 * @author StevenF
 */
public class ReconciliationRunOrphanObject extends BaseSimpleEntity<Long> {

	private ReconciliationRunOrphanObjectTypes orphanType;

	private Date reconciliationDate;

	private ReconciliationFileDefinition fileDefinition;
	/**
	 * Object data holder; top level object that all json data for this reconciliation object is bound to
	 */
	private DataHolder dataHolder;
	private Status status;

	private ReconciliationExclusionAssignment exclusionAssignment;
	private ReconciliationRun excludedRun;

	/////////////////////////////////////////////////////////////////////////////


	public String getAccountNumber() {
		if (getDataHolder() != null) {
			// value is the same regardless of the source
			return (String) getDataHolder().getDataObjectValue(null, ReconciliationField.HOLDING_ACCOUNT_FIELD);
		}
		return null;
	}


	public String getSecurityIdentifier() {
		if (getDataHolder() != null) {
			// value is the same regardless of the source
			return (String) getDataHolder().getDataObjectValue(null, ReconciliationField.INVESTMENT_SECURITY_FIELD);
		}
		return null;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ReconciliationRunOrphanObjectTypes getOrphanType() {
		return this.orphanType;
	}


	public void setOrphanType(ReconciliationRunOrphanObjectTypes orphanType) {
		this.orphanType = orphanType;
	}


	public Date getReconciliationDate() {
		return this.reconciliationDate;
	}


	public void setReconciliationDate(Date reconciliationDate) {
		this.reconciliationDate = reconciliationDate;
	}


	public ReconciliationFileDefinition getFileDefinition() {
		return this.fileDefinition;
	}


	public void setFileDefinition(ReconciliationFileDefinition fileDefinition) {
		this.fileDefinition = fileDefinition;
	}


	@ValueIgnoringGetter
	public DataHolder getDataHolder() {
		if (this.dataHolder == null) {
			this.dataHolder = new DataHolder();
		}
		return this.dataHolder;
	}


	public void setDataHolder(DataHolder dataHolder) {
		this.dataHolder = dataHolder;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public ReconciliationExclusionAssignment getExclusionAssignment() {
		return this.exclusionAssignment;
	}


	public void setExclusionAssignment(ReconciliationExclusionAssignment exclusionAssignment) {
		this.exclusionAssignment = exclusionAssignment;
	}


	public ReconciliationRun getExcludedRun() {
		return this.excludedRun;
	}


	public void setExcludedRun(ReconciliationRun excludedRun) {
		this.excludedRun = excludedRun;
	}
}
