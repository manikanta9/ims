package com.clifton.reconciliation.run;

/**
 * @author StevenF
 */
public enum ReconciliationRunOrphanObjectTypes {

	EXCLUDED,
	FILTERED;
}
