package com.clifton.reconciliation.run.job;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.run.ReconciliationRunOrphanObjectTypes;

import java.util.Date;


/**
 * The <code>ReconciliationOrphanDataCommand</code> has selection criteria necessary to run the {@link ReconciliationOrphanDataPurgeJob} including selection
 * criteria for determining which {@link com.clifton.reconciliation.run.ReconciliationRunOrphanObject} entities should be deleted.
 *
 * @author TerryS
 */
public class ReconciliationOrphanDataPurgeCommand {

	private ReconciliationRunOrphanObjectTypes orphanType;
	private int daysToRetain;
	private boolean reviewRequired;

	private boolean synchronous;
	private Integer asynchronousDelay;
	private boolean throwExceptionOnError;
	private Integer asynchronousRebuildDelay;

	private Status status;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static ReconciliationOrphanDataPurgeCommand ofSynchronousWithThrow() {
		ReconciliationOrphanDataPurgeCommand result = new ReconciliationOrphanDataPurgeCommand();
		result.setSynchronous(true);
		result.setThrowExceptionOnError(true);
		return result;
	}


	public String getRunId() {
		String runId = "RECONCILIATION_ORPHAN_DATA";
		if (getOrphanType() != null) {
			runId = "_TYPE_" + getOrphanType();
		}
		else {
			runId += "_ALL_";
		}
		runId += DateUtils.fromDate(new Date());
		return runId;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationRunOrphanObjectTypes getOrphanType() {
		return this.orphanType;
	}


	public void setOrphanType(ReconciliationRunOrphanObjectTypes orphanType) {
		this.orphanType = orphanType;
	}


	public int getDaysToRetain() {
		return this.daysToRetain;
	}


	public void setDaysToRetain(int daysToRetain) {
		this.daysToRetain = daysToRetain;
	}


	public boolean isReviewRequired() {
		return this.reviewRequired;
	}


	public void setReviewRequired(boolean reviewRequired) {
		this.reviewRequired = reviewRequired;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public Integer getAsynchronousDelay() {
		return this.asynchronousDelay;
	}


	public void setAsynchronousDelay(Integer asynchronousDelay) {
		this.asynchronousDelay = asynchronousDelay;
	}


	public boolean isThrowExceptionOnError() {
		return this.throwExceptionOnError;
	}


	public void setThrowExceptionOnError(boolean throwExceptionOnError) {
		this.throwExceptionOnError = throwExceptionOnError;
	}


	public Integer getAsynchronousRebuildDelay() {
		return this.asynchronousRebuildDelay;
	}


	public void setAsynchronousRebuildDelay(Integer asynchronousRebuildDelay) {
		this.asynchronousRebuildDelay = asynchronousRebuildDelay;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
