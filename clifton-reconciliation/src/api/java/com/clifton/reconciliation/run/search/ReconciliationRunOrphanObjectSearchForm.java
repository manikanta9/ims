package com.clifton.reconciliation.run.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.reconciliation.run.ReconciliationRunOrphanObjectTypes;

import java.util.Date;


/**
 * @author stevenf on 10/3/2016.
 */
public class ReconciliationRunOrphanObjectSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "fileDefinition.name,fileDefinition.fileName")
	private String searchPattern;

	@SearchField
	private Long id;

	@SearchField
	private Date reconciliationDate;

	@SearchField(searchField = "excludedRun.id")
	private Integer excludedRunId;

	@SearchField(searchField = "fileDefinition.id")
	private Integer fileDefinitionId;

	@SearchField(searchFieldPath = "fileDefinition", searchField = "name")
	private String fileDefinitionName;

	@SearchField(searchFieldPath = "fileDefinition", searchField = "fileName")
	private String fileName;

	@SearchField(searchFieldPath = "fileDefinition", searchField = "source.id")
	private Short sourceId;

	@SearchField(searchFieldPath = "fileDefinition.source", searchField = "name")
	private String sourceName;

	@SearchField
	private ReconciliationRunOrphanObjectTypes orphanType;

	@SearchField(comparisonConditions = ComparisonConditions.LESS_THAN, searchField = "reconciliationDate")
	private Date reconciliationDateBefore;

	@SearchField(searchField = "exclusionAssignment.id")
	private Integer exclusionAssignmentId;

	@SearchField(searchFieldPath = "exclusionAssignment", searchField = "name")
	private String exclusionAssignmentName;

	@SearchField(searchFieldPath = "exclusionAssignment", searchField = "approvalUser", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean exclusionAssignmentReviewerPopulated;

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Date getReconciliationDate() {
		return this.reconciliationDate;
	}


	public void setReconciliationDate(Date reconciliationDate) {
		this.reconciliationDate = reconciliationDate;
	}


	public Integer getExcludedRunId() {
		return this.excludedRunId;
	}


	public void setExcludedRunId(Integer excludedRunId) {
		this.excludedRunId = excludedRunId;
	}


	public Integer getFileDefinitionId() {
		return this.fileDefinitionId;
	}


	public void setFileDefinitionId(Integer fileDefinitionId) {
		this.fileDefinitionId = fileDefinitionId;
	}


	public String getFileDefinitionName() {
		return this.fileDefinitionName;
	}


	public void setFileDefinitionName(String fileDefinitionName) {
		this.fileDefinitionName = fileDefinitionName;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public Short getSourceId() {
		return this.sourceId;
	}


	public void setSourceId(Short sourceId) {
		this.sourceId = sourceId;
	}


	public String getSourceName() {
		return this.sourceName;
	}


	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}


	public ReconciliationRunOrphanObjectTypes getOrphanType() {
		return this.orphanType;
	}


	public void setOrphanType(ReconciliationRunOrphanObjectTypes orphanType) {
		this.orphanType = orphanType;
	}


	public Date getReconciliationDateBefore() {
		return this.reconciliationDateBefore;
	}


	public void setReconciliationDateBefore(Date reconciliationDateBefore) {
		this.reconciliationDateBefore = reconciliationDateBefore;
	}


	public Integer getExclusionAssignmentId() {
		return this.exclusionAssignmentId;
	}


	public void setExclusionAssignmentId(Integer exclusionAssignmentId) {
		this.exclusionAssignmentId = exclusionAssignmentId;
	}


	public String getExclusionAssignmentName() {
		return this.exclusionAssignmentName;
	}


	public void setExclusionAssignmentName(String exclusionAssignmentName) {
		this.exclusionAssignmentName = exclusionAssignmentName;
	}


	public Boolean getExclusionAssignmentReviewerPopulated() {
		return this.exclusionAssignmentReviewerPopulated;
	}


	public void setExclusionAssignmentReviewerPopulated(Boolean exclusionAssignmentReviewerPopulated) {
		this.exclusionAssignmentReviewerPopulated = exclusionAssignmentReviewerPopulated;
	}
}
