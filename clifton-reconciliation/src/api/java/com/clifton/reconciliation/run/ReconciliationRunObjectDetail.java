package com.clifton.reconciliation.run;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.collections.FlatMap;
import com.clifton.reconciliation.definition.ReconciliationField;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.run.data.DataError;
import com.clifton.reconciliation.run.data.DataNote;
import com.clifton.reconciliation.run.data.DataObject;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatus;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The reconciliation object detail is used for for serialization to the UI
 * It allows for a runObjectList to be converted into individual run object details for each
 * source of data (e.g. if a runObject has data loaded from IMS and Merrill Lynch then
 * two detail objects with the same identifier will be created and grouped in the UI.
 * <p>
 * The object list is filtered and sorted using parallel streams before being returned.
 * This allows for the data to be serialized properly without the need for complex SQL queries
 * dealing with sorting, filtering, or casting the data and provides all relevant information
 * necessary for UI display.
 *
 * @author stevenf on 10/3/2016.
 */
@NonPersistentObject
public class ReconciliationRunObjectDetail {

	public static final String RUN_OBJECT_ID = "ReconciliationRunObjectID";
	public static final String RUN_ID = "ReconciliationRunID";
	public static final String ACCOUNT_ID = "ReconciliationInvestmentAccountID";
	public static final String SECURITY_ID = "ReconciliationInvestmentSecurityID";
	public static final String MATCH_STATUS_ID = "ReconciliationMatchStatusID";
	public static final String RECONCILE_STATUS_ID = "ReconciliationReconcileStatusID";
	public static final String RECONCILED_ON_SOURCE_ID = "ReconciledOnSourceID";
	public static final String NATURAL_KEY_IDENTIFIER = "NaturalKeyIdentifier";
	public static final String DATA_HOLDER = "DataHolder";
	public static final String BREAK_DAYS = "BreakDays";
	public static final String RUN_DATE = "RunDate";
	public static final String IMPORT_DATE = "ImportDate";
	public static final String UPDATE_USER_ID = "UpdateUserID";
	public static final String UPDATE_DATE = "UpdateDate";

	private int breakDays;
	private Boolean reconciledOnPrimarySource;

	private Long id;
	private String explodedId;
	private String identifier;
	private String modifierText;
	private List<ReconciliationSource> source;

	private List<DataNote> notes;
	private List<DataError> errors;

	private Date runDate;
	private Date importDate;

	private Short matchStatusId;
	private ReconciliationMatchStatus matchStatus;

	private Short reconcileStatusId;
	private ReconciliationReconcileStatus reconcileStatus;

	private Short reconciledOnSourceId;
	private ReconciliationSource reconciledOnSource;

	private Integer reconciliationRunId;
	private Integer reconciliationInvestmentAccountId;
	private Integer reconciliationInvestmentSecurityId;

	private FlatMap<String, List<Object>> objectData;

	private short updateUserId;

	private Date updateDate;

	/**
	 * Used for optimizing data retrieval for the runDetail screen.
	 * Objects are queried directly using our SqlHandler and the compressed bytes from the dataHolder are stored.
	 * After the result set has been iterated, the data is decompressed using a parallel stream.
	 */
	private byte[] dataHolderBytes;

	/**
	 * Used in the UI for 'exploded' objects.
	 * This allows for inspecting the normalized and raw source data for individual dataObjects of the runObject.
	 */
	private List<DataObject> dataObjectList;
	private DataObject dataObject;

	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Since a detail object is custom built for the UI it consolidates multiple dataObjects into a single
	 * FlatMap using lists, this works well for custom rendering on the UI, however, does not work for
	 * export to excel or csv.  The 'explode' method creates individual detail objects for each dataObject
	 * that has been consolidated.  So if a user has matched multiple objects that results in 10 dataObjects.
	 * This one detail object will be exploded into ten so the rows can be displayed individually in excel.
	 */
	public List<ReconciliationRunObjectDetail> explode() {
		List<ReconciliationRunObjectDetail> runObjectDetailList = new ArrayList<>();
		int size = this.getObjectData().get(ReconciliationField.HOLDING_ACCOUNT_FIELD).size();
		for (int i = 0; i < size; i++) {
			ReconciliationRunObjectDetail runObjectDetail = new ReconciliationRunObjectDetail();
			FlatMap<String, List<Object>> runObjectData = new FlatMap<>();
			for (Map.Entry<String, List<Object>> dataObjectEntry : CollectionUtils.getIterable(this.objectData.entrySet())) {
				List<Object> dataObjectValueList = dataObjectEntry.getValue();
				if (!CollectionUtils.isEmpty(dataObjectValueList)) {
					int index = i;
					if (index >= CollectionUtils.getSize(dataObjectValueList)) {
						index = 0;
					}
					runObjectData.put(dataObjectEntry.getKey(), Collections.singletonList(dataObjectEntry.getValue().get(index)));
				}
			}
			runObjectDetail.setSource(Collections.singletonList(this.source.get(i)));
			runObjectDetail.setBreakDays(this.breakDays);
			runObjectDetail.setReconciledOnSource(this.reconciledOnSource);
			runObjectDetail.setReconciliationInvestmentAccountId(this.reconciliationInvestmentAccountId);
			runObjectDetail.setReconciliationInvestmentSecurityId(this.reconciliationInvestmentSecurityId);
			runObjectDetail.setReconciliationRunId(this.reconciliationRunId);
			runObjectDetail.setReconciledOnPrimarySource(this.reconciledOnPrimarySource);
			runObjectDetail.setId(this.id);
			runObjectDetail.setExplodedId(this.id + "_" + i);
			runObjectDetail.setIdentifier(this.identifier);
			runObjectDetail.setNotes(this.notes);
			runObjectDetail.setRunDate(this.runDate);
			runObjectDetail.setImportDate(this.importDate);
			runObjectDetail.setMatchStatus(this.matchStatus);
			runObjectDetail.setReconcileStatus(this.reconcileStatus);
			runObjectDetail.setUpdateUserId(this.updateUserId);
			runObjectDetail.setUpdateDate(this.updateDate);
			runObjectDetail.setObjectData(runObjectData);
			if (getDataObjectList() != null) {
				runObjectDetail.setDataObject(getDataObjectList().get(i));
			}
			runObjectDetailList.add(runObjectDetail);
		}
		return runObjectDetailList;
	}


	public void addSource(ReconciliationSource source) {
		if (this.source == null) {
			this.source = new ArrayList<>();
		}
		this.source.add(source);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public int getBreakDays() {
		return this.breakDays;
	}


	public void setBreakDays(int breakDays) {
		this.breakDays = breakDays;
	}


	public Boolean getReconciledOnPrimarySource() {
		return this.reconciledOnPrimarySource;
	}


	public void setReconciledOnPrimarySource(Boolean reconciledOnPrimarySource) {
		this.reconciledOnPrimarySource = reconciledOnPrimarySource;
	}


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getExplodedId() {
		return this.explodedId;
	}


	public void setExplodedId(String explodedId) {
		this.explodedId = explodedId;
	}


	public String getIdentifier() {
		return this.identifier;
	}


	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}


	public String getModifierText() {
		return this.modifierText;
	}


	public void setModifierText(String modifierText) {
		this.modifierText = modifierText;
	}


	public List<ReconciliationSource> getSource() {
		return this.source;
	}


	public void setSource(List<ReconciliationSource> source) {
		this.source = source;
	}


	public List<DataNote> getNotes() {
		return this.notes;
	}


	public void setNotes(List<DataNote> notes) {
		this.notes = notes;
	}


	public List<DataError> getErrors() {
		return this.errors;
	}


	public void setErrors(List<DataError> errors) {
		this.errors = errors;
	}


	public Date getRunDate() {
		return this.runDate;
	}


	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}


	public Date getImportDate() {
		return this.importDate;
	}


	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}


	public Short getMatchStatusId() {
		return this.matchStatusId;
	}


	public void setMatchStatusId(Short matchStatusId) {
		this.matchStatusId = matchStatusId;
	}


	public ReconciliationMatchStatus getMatchStatus() {
		return this.matchStatus;
	}


	public void setMatchStatus(ReconciliationMatchStatus matchStatus) {
		this.matchStatus = matchStatus;
	}


	public Short getReconcileStatusId() {
		return this.reconcileStatusId;
	}


	public void setReconcileStatusId(Short reconcileStatusId) {
		this.reconcileStatusId = reconcileStatusId;
	}


	public ReconciliationReconcileStatus getReconcileStatus() {
		return this.reconcileStatus;
	}


	public void setReconcileStatus(ReconciliationReconcileStatus reconcileStatus) {
		this.reconcileStatus = reconcileStatus;
	}


	public Short getReconciledOnSourceId() {
		return this.reconciledOnSourceId;
	}


	public void setReconciledOnSourceId(Short reconciledOnSourceId) {
		this.reconciledOnSourceId = reconciledOnSourceId;
	}


	public ReconciliationSource getReconciledOnSource() {
		return this.reconciledOnSource;
	}


	public void setReconciledOnSource(ReconciliationSource reconciledOnSource) {
		this.reconciledOnSource = reconciledOnSource;
	}


	public Integer getReconciliationRunId() {
		return this.reconciliationRunId;
	}


	public void setReconciliationRunId(Integer reconciliationRunId) {
		this.reconciliationRunId = reconciliationRunId;
	}


	public Integer getReconciliationInvestmentAccountId() {
		return this.reconciliationInvestmentAccountId;
	}


	public void setReconciliationInvestmentAccountId(Integer reconciliationInvestmentAccountId) {
		this.reconciliationInvestmentAccountId = reconciliationInvestmentAccountId;
	}


	public Integer getReconciliationInvestmentSecurityId() {
		return this.reconciliationInvestmentSecurityId;
	}


	public void setReconciliationInvestmentSecurityId(Integer reconciliationInvestmentSecurityId) {
		this.reconciliationInvestmentSecurityId = reconciliationInvestmentSecurityId;
	}


	public FlatMap<String, List<Object>> getObjectData() {
		return this.objectData;
	}


	public void setObjectData(FlatMap<String, List<Object>> objectData) {
		this.objectData = objectData;
	}


	public byte[] getDataHolderBytes() {
		return this.dataHolderBytes;
	}


	public void setDataHolderBytes(byte[] dataHolderBytes) {
		this.dataHolderBytes = dataHolderBytes;
	}


	public List<DataObject> getDataObjectList() {
		return this.dataObjectList;
	}


	public void setDataObjectList(List<DataObject> dataObjectList) {
		this.dataObjectList = dataObjectList;
	}


	public DataObject getDataObject() {
		return this.dataObject;
	}


	public void setDataObject(DataObject dataObject) {
		this.dataObject = dataObject;
	}


	public short getUpdateUserId() {
		return this.updateUserId;
	}


	public void setUpdateUserId(short updateUserId) {
		this.updateUserId = updateUserId;
	}


	public Date getUpdateDate() {
		return this.updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
