package com.clifton.reconciliation.run.job;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.status.Status;
import org.springframework.web.bind.annotation.ModelAttribute;


/**
 * <code>ReconciliationOrphanDataPurgeService</code> provides methods for purging {@link com.clifton.reconciliation.run.ReconciliationRunOrphanObject}
 * entities from the database.
 *
 * @author TerryS
 */
public interface ReconciliationOrphanDataPurgeService {

	@ModelAttribute("result")
	@SecureMethod(permissions = SecurityPermission.PERMISSION_EXECUTE)
	public Status processReconciliationOrphanDataPurge(ReconciliationOrphanDataPurgeCommand command);
}
