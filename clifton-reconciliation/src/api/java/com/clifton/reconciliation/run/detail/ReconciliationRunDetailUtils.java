package com.clifton.reconciliation.run.detail;

import com.clifton.core.util.CollectionUtils;
import com.clifton.reconciliation.run.data.DataNote;
import com.clifton.reconciliation.run.data.DataNoteTypes;

import java.util.List;


/**
 * @author stevenf
 */
public class ReconciliationRunDetailUtils {

	public static String getLatestManualNote(List<DataNote> dataNoteList) {
		for (DataNote dataNote : CollectionUtils.getIterable(dataNoteList)) {
			//Get the latest manual note UNLESS there is only one note.
			if (dataNote.getType() != DataNoteTypes.AUTO_RECONCILE || CollectionUtils.getSize(dataNoteList) == 1) {
				return dataNote.getNote();
			}
		}
		return null;
	}
}
