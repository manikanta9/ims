package com.clifton.reconciliation.run.data;

import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * @author KellyJ
 */
//TODO - can we do this a different way, use Status object with error messages?
public class DataError {

	private Integer fieldId;
	private String fieldName;
	private BigDecimal diff;  // does not apply in all cases - used for quantities and prices

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public DataError() {
		//Necessary constructor for JSON serialization
	}


	public DataError(ReconciliationDefinitionField reconciliationDefinitionField, Object firstValue, Object secondValue) {
		this(reconciliationDefinitionField);
		if (firstValue instanceof Number && secondValue instanceof Number) {
			BigDecimal primaryValue = (BigDecimal) DataTypeNameUtils.convertObjectToDataTypeName(firstValue, DataTypeNames.DECIMAL);
			BigDecimal secondaryValue = (BigDecimal) DataTypeNameUtils.convertObjectToDataTypeName(secondValue, DataTypeNames.DECIMAL);
			setDiff(MathUtils.subtract(primaryValue, secondaryValue));
		}
	}


	public DataError(ReconciliationDefinitionField reconciliationDefinitionField) {
		this(reconciliationDefinitionField, null);
	}


	public DataError(ReconciliationDefinitionField reconciliationDefinitionField, BigDecimal numericDifference) {
		if (reconciliationDefinitionField != null) {
			setFieldId(reconciliationDefinitionField.getId());
			setFieldName(reconciliationDefinitionField.getField().getName());
		}
		setDiff(numericDifference);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getFieldId() {
		return this.fieldId;
	}


	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}


	public String getFieldName() {
		return this.fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public BigDecimal getDiff() {
		return this.diff;
	}


	public void setDiff(BigDecimal diff) {
		this.diff = diff;
	}


	@Override
	public String toString() {
		return String.format("DataError[field=%s, difference=%s]", getFieldName(), getDiff() != null ? getDiff().toString() : "n/a");
	}
}
