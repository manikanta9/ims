package com.clifton.reconciliation.run.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;

import java.util.Date;


/**
 * @author StevenF
 */
public class ReconciliationRunSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField
	private Date runDate;

	@SearchField(comparisonConditions = ComparisonConditions.GREATER_THAN, searchField = "runDate")
	private Date runDateAfter;

	@SearchField(comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS, searchField = "runDate")
	private Date runDateAfterOrEquals;

	@SearchField(comparisonConditions = ComparisonConditions.LESS_THAN, searchField = "runDate")
	private Date runDateBefore;

	@SearchField(comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS, searchField = "runDate")
	private Date runDateBeforeOrEquals;

	@SearchField
	private Integer id;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] ids;

	@SearchField(searchField = "reconciliationDefinition.type.id")
	private Short typeId;

	// when typeName is set, lookup the type by name, clear the name and set id
	private String typeName;

	@SearchField(searchField = "reconciliationDefinition.type.id,reconciliationDefinition.parentDefinition.type.id", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Short typeOrParentTypeId;

	// when typeOrParentTypeName is set, lookup the type by name, clear the name and set id
	private String typeOrParentTypeName; // same as above

	@SearchField(searchFieldPath = "reconciliationDefinition.type", searchField = "cumulativeReconciliation")
	private Boolean cumulativeReconciliation;

	@SearchField(searchField = "reconciliationDefinition.primarySource.id,reconciliationDefinition.secondarySource.id", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Short primaryOrSecondarySourceId;

	@SearchField(searchField = "reconciliationDefinition.id")
	private Integer definitionId;

	@SearchField(searchField = "reconciliationDefinition.id")
	private Integer[] definitionIds;

	@SearchField(searchFieldPath = "reconciliationDefinition", searchField = "name")
	private String definitionName;

	@SearchField(searchFieldPath = "reconciliationDefinition", searchField = "definitionGroup.id")
	private Integer definitionGroupId;

	@SearchField(searchFieldPath = "reconciliationDefinition", searchField = "parentDefinition.id")
	private Integer parentDefinitionId;

	@SearchField(searchFieldPath = "reconciliationDefinition", searchField = "parentDefinition.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullParentDefinitionId;

	@SearchField(searchFieldPath = "reconciliationReconcileStatus", searchField = "name")
	private String reconcileStatusName;

	@SearchField(searchField = "reconciliationReconcileStatus.id")
	private Short reconcileStatusId;

	@SearchField(searchField = "approvedByUser.id")
	private Short approvedByUserId;

	@SearchField(searchField = "approvedByUser.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean approvedByUserIdIsNull;

	@SearchField(searchFieldPath = "approvedByUser", searchField = "displayName", comparisonConditions = ComparisonConditions.EQUALS)
	private String approvedByUserName;

	@SearchField
	private Boolean archived;

	@SearchField
	private String note;

	//Custom search field
	private String sourceName;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * The table to which the tag is attached, the system tag is NOT associated with the run but the associated definition.
	 * Since this is not the Reconciliation Run table, this must be used in conjunction with the form value 'categoryLinkFieldPath'.
	 */
	@Override
	public String getDaoTableName() {
		return "ReconciliationDefinition";
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getRunDate() {
		return this.runDate;
	}


	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}


	public Date getRunDateAfter() {
		return this.runDateAfter;
	}


	public void setRunDateAfter(Date runDateAfter) {
		this.runDateAfter = runDateAfter;
	}


	public Date getRunDateAfterOrEquals() {
		return this.runDateAfterOrEquals;
	}


	public void setRunDateAfterOrEquals(Date runDateAfterOrEquals) {
		this.runDateAfterOrEquals = runDateAfterOrEquals;
	}


	public Date getRunDateBefore() {
		return this.runDateBefore;
	}


	public void setRunDateBefore(Date runDateBefore) {
		this.runDateBefore = runDateBefore;
	}


	public Date getRunDateBeforeOrEquals() {
		return this.runDateBeforeOrEquals;
	}


	public void setRunDateBeforeOrEquals(Date runDateBeforeOrEquals) {
		this.runDateBeforeOrEquals = runDateBeforeOrEquals;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Short getTypeOrParentTypeId() {
		return this.typeOrParentTypeId;
	}


	public void setTypeOrParentTypeId(Short typeOrParentTypeId) {
		this.typeOrParentTypeId = typeOrParentTypeId;
	}


	public String getTypeOrParentTypeName() {
		return this.typeOrParentTypeName;
	}


	public void setTypeOrParentTypeName(String typeOrParentTypeName) {
		this.typeOrParentTypeName = typeOrParentTypeName;
	}


	public Boolean getCumulativeReconciliation() {
		return this.cumulativeReconciliation;
	}


	public void setCumulativeReconciliation(Boolean cumulativeReconciliation) {
		this.cumulativeReconciliation = cumulativeReconciliation;
	}


	public Short getPrimaryOrSecondarySourceId() {
		return this.primaryOrSecondarySourceId;
	}


	public void setPrimaryOrSecondarySourceId(Short primaryOrSecondarySourceId) {
		this.primaryOrSecondarySourceId = primaryOrSecondarySourceId;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public Integer[] getDefinitionIds() {
		return this.definitionIds;
	}


	public void setDefinitionIds(Integer[] definitionIds) {
		this.definitionIds = definitionIds;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public Integer getDefinitionGroupId() {
		return this.definitionGroupId;
	}


	public void setDefinitionGroupId(Integer definitionGroupId) {
		this.definitionGroupId = definitionGroupId;
	}


	public Integer getParentDefinitionId() {
		return this.parentDefinitionId;
	}


	public void setParentDefinitionId(Integer parentDefinitionId) {
		this.parentDefinitionId = parentDefinitionId;
	}


	public Boolean getNullParentDefinitionId() {
		return this.nullParentDefinitionId;
	}


	public void setNullParentDefinitionId(Boolean nullParentDefinitionId) {
		this.nullParentDefinitionId = nullParentDefinitionId;
	}


	public String getReconcileStatusName() {
		return this.reconcileStatusName;
	}


	public void setReconcileStatusName(String reconcileStatusName) {
		this.reconcileStatusName = reconcileStatusName;
	}


	public Short getReconcileStatusId() {
		return this.reconcileStatusId;
	}


	public void setReconcileStatusId(Short reconcileStatusId) {
		this.reconcileStatusId = reconcileStatusId;
	}


	public Short getApprovedByUserId() {
		return this.approvedByUserId;
	}


	public void setApprovedByUserId(Short approvedByUserId) {
		this.approvedByUserId = approvedByUserId;
	}


	public Boolean getApprovedByUserIdIsNull() {
		return this.approvedByUserIdIsNull;
	}


	public void setApprovedByUserIdIsNull(Boolean approvedByUserIdIsNull) {
		this.approvedByUserIdIsNull = approvedByUserIdIsNull;
	}


	public String getApprovedByUserName() {
		return this.approvedByUserName;
	}


	public void setApprovedByUserName(String approvedByUserName) {
		this.approvedByUserName = approvedByUserName;
	}


	public Boolean getArchived() {
		return this.archived;
	}


	public void setArchived(Boolean archived) {
		this.archived = archived;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public String getSourceName() {
		return this.sourceName;
	}


	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
}
