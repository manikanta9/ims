package com.clifton.reconciliation.run.status.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author StevenF on 10/02/2017.
 */
public class ReconciliationReconcileStatusSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description,label")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Boolean reconciled;

	@SearchField
	private Boolean manual;

	@SearchField
	private Short sortOrder;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getReconciled() {
		return this.reconciled;
	}


	public void setReconciled(Boolean reconciled) {
		this.reconciled = reconciled;
	}


	public Boolean getManual() {
		return this.manual;
	}


	public void setManual(Boolean manual) {
		this.manual = manual;
	}


	public Short getSortOrder() {
		return this.sortOrder;
	}


	public void setSortOrder(Short sortOrder) {
		this.sortOrder = sortOrder;
	}
}
