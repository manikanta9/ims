package com.clifton.reconciliation.run.data;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.hibernate.GenericCompressedTextUserType;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationSource;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * The <code>DataHolder</code> is a virtual object meant for serialization and compressed storage.
 * Serialization is currently done as JSON and stored in SQL Server; but could be serialized as XML
 * or another format and stored in flat files or any other DB.
 * <p>
 * Field naming conventions do not adhere to our standard such as 'noteList', 'errorList', 'dataObjectList', etc.
 * due to the need to minimize the size of the serialized data as best as possible while still maintaining readability.
 *
 * @author stevenf on 10/3/2016.
 */
public class DataHolder {

	private List<DataNote> notes;
	private List<DataError> errors;
	private List<DataObject> dataObjects;

	////////////////////////////////////////////////////////////////////////////////


	public List<DataObject> getDataObjectListBySource(ReconciliationSource source) {
		ValidationUtils.assertNotNull(source, "Source may not be null!");
		return BeanUtils.filter(getDataObjects(), DataObject::getSourceId, source.getId());
	}


	public Object getDataObjectValue(ReconciliationSource source, String key) {
		DataObject dataObject = null;
		if (source == null) {
			if (!CollectionUtils.isEmpty(getDataObjects())) {
				dataObject = getDataObjects().get(0);
			}
		}
		else {
			dataObject = CollectionUtils.getFirstElement(getDataObjectListBySource(source));
		}
		if (dataObject != null && dataObject.getData() != null) {
			return dataObject.getData().get(key);
		}
		return null;
	}


	public void addError(DataError error) {
		if (CollectionUtils.isEmpty(this.errors)) {
			this.errors = new ArrayList<>();
		}
		this.errors.add(error);
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		try {
			return GenericCompressedTextUserType.MAPPER.writeValueAsString(this).equals(GenericCompressedTextUserType.MAPPER.writeValueAsString(o));
		}
		catch (Exception e) {
			return false;
		}
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.notes, this.errors, this.dataObjects);
	}


	////////////////////////////////////////////////////////////////////////////////
	public List<DataNote> getNotes() {
		return sortNoteList(this.notes);
	}


	public void setNotes(List<DataNote> notes) {
		this.notes = sortNoteList(notes);
	}


	/**
	 * Anytime the list is set or retrieved we sort the notes by createDate desc.
	 * This is so the first note in the array is the most recent, which makes filtering
	 * on notes in the query MUCH simpler (syntax to find last in an array with json queries is overly complicated.)
	 */
	private List<DataNote> sortNoteList(List<DataNote> dataNoteList) {
		if (dataNoteList != null) {
			BeanUtils.sortWithFunction(dataNoteList, DataNote::getCreateDate, false);
		}
		return dataNoteList;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<DataObject> getDataObjects() {
		if (this.dataObjects == null) {
			this.dataObjects = new ArrayList<>();
		}
		return this.dataObjects;
	}


	public void setDataObjects(List<DataObject> dataObjects) {
		this.dataObjects = dataObjects;
	}


	public List<DataError> getErrors() {
		return this.errors;
	}


	public void setErrors(List<DataError> errors) {
		this.errors = errors;
	}


	@Override
	public String toString() {
		return String.format("ReconcileDataHolderValues[dataList=%s, noteList=%s]", getDataObjects(), getNotes());
	}
}
