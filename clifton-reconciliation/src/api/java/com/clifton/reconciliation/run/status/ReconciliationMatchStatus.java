package com.clifton.reconciliation.run.status;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.util.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * @author StevenF on 11/22/2016.
 */
@CacheByName
public class ReconciliationMatchStatus extends NamedEntity<Short> {

	private boolean matched;
	private boolean manual;
	private Short sortOrder;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@JsonIgnore
	public ReconciliationMatchStatusTypes getReconciliationMatchStatusType() {
		return ReconciliationMatchStatusTypes.findReconciliationMatchStatusType(this.getName());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@JsonIgnore
	public String getLabel() {
		ReconciliationMatchStatusTypes statusType = getReconciliationMatchStatusType();
		return statusType != null ? statusType.getDescription() : StringUtils.EMPTY_STRING;
	}


	public boolean isMatched() {
		return this.matched;
	}


	public void setMatched(boolean matched) {
		this.matched = matched;
	}


	public boolean isManual() {
		return this.manual;
	}


	public void setManual(boolean manual) {
		this.manual = manual;
	}


	public Short getSortOrder() {
		return this.sortOrder;
	}


	public void setSortOrder(Short sortOrder) {
		this.sortOrder = sortOrder;
	}
}
