package com.clifton.reconciliation.run;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;
import com.clifton.security.user.SecurityUser;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;


/**
 * ReconciliationRun tracks the overall status of a given reconciliation build.
 * It stores the lowest ReconciliationReconcileStatus of the items reconciled (e.g. one fails, state is failed)
 * Additionally, it stores the actual status object created during the run which will hold details regarding any failures.
 *
 * @author StevenF on 11/22/2016.
 */
public class ReconciliationRun extends BaseEntity<Integer> implements LabeledObject {

	//Allows for setting an optional category that will be used when adding message details when saving a run
	// (e.g. ReconciliationService sets the ReconciliationSource name as the category so details are associated with the currently loaded source)
	public static final String CATEGORY = "CATEGORY";

	//Reflects value from database
	public static final String TOTAL_COUNT = "totalCount";
	public static final String MATCHED_COUNT = "matchedCount";
	public static final String NOT_MATCHED_COUNT = "notMatchedCount";
	public static final String RECONCILED_COUNT = "reconciledCount";
	public static final String MANUALLY_RECONCILED_COUNT = "manuallyReconciledCount";
	public static final String TOTAL_RECONCILED_COUNT = "totalReconciledCount";
	public static final String NOT_RECONCILED_COUNT = "notReconciledCount";

	//Dynamically adjusted during load or other code.
	public static final String ERROR_COUNT = "errorCount";
	public static final String EXCLUDED_COUNT = "excludedCount";
	public static final String FILTERED_COUNT = "filteredCount";

	/**
	 * Optional note - only currently utilized if a run is 'un-approved' then the note is required.
	 */
	private String note;

	private Date runDate;

	private ReconciliationDefinition reconciliationDefinition;

	private ReconciliationReconcileStatus reconciliationReconcileStatus;

	/**
	 * Stored in DB as compressed JSON representation of Status object that captures the following metrics:
	 * <ul>
	 * <li>processedCount: Total number of rows processed from both sources. Re-importing new data will increase this number (may double count, etc.)</li>
	 * <li>createdCount: Total number of reconciliation objects in this run (includes matched and unmatched).</li>
	 * <li>matchedCount: The number of records matched based on definition's natural key(s) between primary and secondary sources. (Not Matched = Created Count - Matched Count)</li>
	 * <li>reconciledCount: The number of reconciliation objects that were automatically or manually reconciled. (Not Reconciled = Created Count - Reconciled Count)</li>
	 * <li>excludedCount: The number of excluded source rows (for positions that we do not manage, etc.). Goes to the orphans table and is periodically purged.</li>
	 * <li>filteredCount: The number of rows filtered out based on the rules on reconciliation definition. Goes to the orphans table and is periodically purged.</li>
	 * <li>errorCount: The number of processing errors (string characters in a numeric field, etc.)</li>
	 * </ul>
	 */
	private Status runDetails;

	/**
	 * The user that approved the run
	 */
	private SecurityUser approvedByUser;

	/**
	 * Used to define if this run has been archived and needs to be retrieved from 'cold storage'
	 */
	private boolean archived;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@JsonIgnore
	public String getLabel() {
		return getReconciliationDefinition().getName() + " (" + DateUtils.fromDate(getRunDate(), DateUtils.DATE_FORMAT_SHORT) + ")";
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public boolean isArchived() {
		return this.archived;
	}


	public void setArchived(boolean archived) {
		this.archived = archived;
	}


	public Date getRunDate() {
		return this.runDate;
	}


	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public ReconciliationDefinition getReconciliationDefinition() {
		return this.reconciliationDefinition;
	}


	public void setReconciliationDefinition(ReconciliationDefinition reconciliationDefinition) {
		this.reconciliationDefinition = reconciliationDefinition;
	}


	public ReconciliationReconcileStatus getReconciliationReconcileStatus() {
		return this.reconciliationReconcileStatus;
	}


	public void setReconciliationReconcileStatus(ReconciliationReconcileStatus reconciliationReconcileStatus) {
		this.reconciliationReconcileStatus = reconciliationReconcileStatus;
	}


	@ValueIgnoringGetter
	public Status getRunDetails() {
		if (this.runDetails == null) {
			this.runDetails = Status.ofTitle("Reconciliation Status");
		}
		return this.runDetails;
	}


	public void setRunDetails(Status runDetails) {
		this.runDetails = runDetails;
	}


	public SecurityUser getApprovedByUser() {
		return this.approvedByUser;
	}


	public void setApprovedByUser(SecurityUser approvedByUser) {
		this.approvedByUser = approvedByUser;
	}
}
