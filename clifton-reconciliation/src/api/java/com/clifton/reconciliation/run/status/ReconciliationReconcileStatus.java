package com.clifton.reconciliation.run.status;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.util.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * @author StevenF on 11/22/2016.
 */
@CacheByName
public class ReconciliationReconcileStatus extends NamedEntity<Short> {

	private boolean reconciled;
	private boolean manual;
	private Short sortOrder;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@JsonIgnore
	public ReconciliationReconcileStatusTypes getReconciliationReconcileStatusType() {
		return ReconciliationReconcileStatusTypes.findReconciliationReconcileStatusType(this.getName());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@JsonIgnore
	public String getLabel() {
		ReconciliationReconcileStatusTypes statusType = getReconciliationReconcileStatusType();
		return statusType != null ? statusType.getDescription() : StringUtils.EMPTY_STRING;
	}


	public boolean isReconciled() {
		return this.reconciled;
	}


	public void setReconciled(boolean reconciled) {
		this.reconciled = reconciled;
	}


	public boolean isManual() {
		return this.manual;
	}


	public void setManual(boolean manual) {
		this.manual = manual;
	}


	public Short getSortOrder() {
		return this.sortOrder;
	}


	public void setSortOrder(Short sortOrder) {
		this.sortOrder = sortOrder;
	}
}
