package com.clifton.reconciliation.run.detail;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObjectDetail;
import com.clifton.reconciliation.run.search.ReconciliationRunObjectSearchForm;

import java.util.List;


/**
 * @author stevenf
 */
public interface ReconciliationRunDetailService {


	@SecureMethod(dtoClass = ReconciliationRun.class)
	public List<ReconciliationRunObjectDetail> getReconciliationRunObjectDetailExplodedList(ReconciliationRunObjectSearchForm searchForm);


	@SecureMethod(dtoClass = ReconciliationRun.class)
	public List<ReconciliationRunObjectDetail> getReconciliationRunObjectDetailList(ReconciliationRunObjectSearchForm searchForm);
}
