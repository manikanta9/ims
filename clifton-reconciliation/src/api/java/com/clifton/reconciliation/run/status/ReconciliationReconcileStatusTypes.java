package com.clifton.reconciliation.run.status;

/**
 * @author KellyJ
 */
public enum ReconciliationReconcileStatusTypes {
	NOT_RECONCILED("Not Reconciled", false, false),
	RECONCILED("Reconciled", true, false),
	MANUALLY_RECONCILED("Manually Reconciled", true, true);

	private String description;
	private boolean reconciled;
	private boolean manual;


	ReconciliationReconcileStatusTypes(String description, boolean reconciled, boolean manual) {
		this.description = description;
		this.reconciled = reconciled;
		this.manual = manual;
	}


	public static ReconciliationReconcileStatusTypes findReconciliationReconcileStatusType(String name) {
		return ReconciliationReconcileStatusTypes.valueOf(name);
	}


	public String getDescription() {
		return this.description;
	}


	public boolean isReconciled() {
		return this.reconciled;
	}


	public boolean isManual() {
		return this.manual;
	}
}
