package com.clifton.reconciliation.run.data;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>DataObject</code> stores normalized and raw row data loaded into the reconciliation system and
 * is wrapped in the {@link DataHolder} object which is serialized and stored in a compressed database field.
 * <p>
 * Field naming conventions do not adhere to our standard such as 'noteList', 'dataMap', 'rowDataMapList', etc.
 * due to the need to minimize the size of the serialized data as best as possible while still maintaining readability.
 *
 * @author stevenf on 10/3/2016.
 */
public class DataObject {


	/**
	 * This is necessary in order to properly filter data, specifically for 'singleton' definitions, e.g. transaction reconciliation
	 * Since source data is maintained for the live of the reconciliation we can only clear source data for the same date as is being uploaded
	 * to prevent duplicates, otherwise we unintentionally would clear older data.
	 */
	private Date importDate;


	private Integer fileDefinitionId;

	/**
	 * TODO - I don't like having sourceId here since it is technically the same as the sourceId of the fileDefinitionId.
	 * Refactoring to remove this is a bit of a larger effort.
	 */
	private Short sourceId;

	/**
	 * Contains the normalized data (fieldName, fieldValue) created by applying mapping rules
	 * defined by a reconciliation definition, definition fields, and definition field mappings for the given source.
	 */
	private Map<String, Object> data;

	/**
	 * The raw row data loaded from an external system that is used to create normalized data to be reconciled.
	 * Primarily stored for troubleshooting and to allow for reconciling data again without the need to reload
	 * the entire external data load.
	 */
	private List<Map<String, Object>> rowData;

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getImportDate() {
		return this.importDate;
	}


	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}


	public Integer getFileDefinitionId() {
		return this.fileDefinitionId;
	}


	public void setFileDefinitionId(Integer fileDefinitionId) {
		this.fileDefinitionId = fileDefinitionId;
	}


	public Short getSourceId() {
		return this.sourceId;
	}


	public void setSourceId(Short sourceId) {
		this.sourceId = sourceId;
	}


	@ValueIgnoringGetter
	public Map<String, Object> getData() {
		if (this.data == null) {
			this.data = new HashMap<>();
		}
		return this.data;
	}


	public void setData(Map<String, Object> data) {
		this.data = data;
	}


	public List<Map<String, Object>> getRowData() {
		if (this.rowData == null) {
			this.rowData = new ArrayList<>();
		}
		return this.rowData;
	}


	public void setRowData(List<Map<String, Object>> rowData) {
		this.rowData = rowData;
	}


	@Override
	public String toString() {
		return String.format("ReconcileDataValues[FileDefinition=%s, Source=%s, DataMap=%s, RowData=%s]", getFileDefinitionId(), getSourceId(), getData(), getRowData());
	}
}
