package com.clifton.reconciliation.run.data;

public enum DataNoteTypes {
	AUTO_RECONCILE,
	RECONCILE_PRIMARY,
	RECONCILE_SECONDARY,
	UNRECONCILE,
	MATCH,
	UNMATCH,
	ADJUSTMENT,
	OTHER
}
