package com.clifton.reconciliation.run.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.system.hierarchy.assignment.search.BaseSystemHierarchyItemSearchForm;

import java.util.Date;


/**
 * @author stevenf on 10/3/2016.
 */
public class ReconciliationRunObjectSearchForm extends BaseSystemHierarchyItemSearchForm {

	@SearchField
	private Long id;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Long idNotEquals;

	@SearchField(searchField = "id")
	private Long[] ids;

	@SearchField(searchField = "reconciliationRun.id")
	private Integer runId;

	@SearchField(searchField = "reconciliationRun.id")
	private Integer[] runIds;

	@SearchField
	private Date importDate;

	@SearchField(searchFieldPath = "reconciliationRun", searchField = "runDate")
	private Date runDate;

	@SearchField(searchFieldPath = "reconciliationRun.reconciliationDefinition", searchField = "type.id")
	private Short typeId;

	@SearchField(searchFieldPath = "reconciliationRun.reconciliationDefinition.type", searchField = "type.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String typeName;

	@SearchField(searchFieldPath = "reconciliationRun", searchField = "reconciliationDefinition.id")
	private Integer definitionId;

	@SearchField(searchFieldPath = "reconciliationRun.reconciliationDefinition", searchField = "name")
	private String definitionName;

	@SearchField(searchFieldPath = "reconciliationRun.reconciliationDefinition", searchField = "definitionGroup.id")
	private Integer definitionGroupId;

	@SearchField(searchFieldPath = "reconciliationRun.reconciliationDefinition.definitionGroup", searchField = "name")
	private String definitionGroupName;

	@SearchField(searchField = "reconciliationInvestmentAccount.id")
	private Integer investmentAccountId;

	@SearchField(searchField = "reconciliationInvestmentAccount.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean investmentAccountIdIsNull;

	@SearchField(searchFieldPath = "reconciliationInvestmentAccount", searchField = "accountName")
	private String investmentAccountName;

	@SearchField(searchFieldPath = "reconciliationInvestmentAccount", searchField = "accountNumber")
	private String investmentAccountNumber;

	@SearchField(searchField = "reconciliationInvestmentSecurity.id")
	private Integer investmentSecurityId;

	@SearchField(searchField = "reconciliationInvestmentSecurity.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean investmentSecurityIdIsNull;

	@SearchField(searchFieldPath = "reconciliationInvestmentSecurity", searchField = "securityName")
	private String investmentSecurityName;

	@SearchField(searchFieldPath = "reconciliationInvestmentSecurity", searchField = "securitySymbol")
	private String investmentSecuritySymbol;

	@SearchField(searchField = "reconciliationInvestmentSecurity.id, reconciliationInvestmentAccount.id", searchFieldCustomType = SearchFieldCustomTypes.OR, comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean investmentSecurityOrAccountIdIsNull;

	@SearchField(searchField = "reconciliationReconcileStatus.id")
	private Short reconcileStatusId;

	@SearchField(searchField = "reconciliationReconcileStatus.id")
	private Short[] reconcileStatusIds;

	@SearchField(searchFieldPath = "reconciliationReconcileStatus", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String reconcileStatusName;

	@SearchField(searchFieldPath = "reconciliationReconcileStatus", searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] reconcileStatusNames;

	@SearchField(searchField = "reconciliationMatchStatus.id")
	private Short matchStatusId;

	@SearchField(searchField = "reconciliationMatchStatus.id")
	private Short[] matchStatusIds;

	@SearchField(searchFieldPath = "reconciliationMatchStatus", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String matchStatusName;

	@SearchField(searchFieldPath = "reconciliationMatchStatus", searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] matchStatusNames;

	@SearchField(searchField = "reconciliationMatchStatus.name,reconciliationReconcileStatus.name")
	private String reconcileOrMatchStatusName;

	@SearchField(searchField = "reconciliationMatchStatus.name,reconciliationReconcileStatus.name")
	private String[] reconcileOrMatchStatusNames;

	@SearchField(searchField = "approvalUser.id")
	private Short approvalUserId;

	@SearchField(searchField = "approvalUser.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean approvalUserIdIsNull;

	@SearchField
	private Date approvalDate;

	@SearchField(searchField = "identifier")
	private String identifier;

	@SearchField(searchField = "identifier")
	private String[] identifiers;

	@SearchField
	private Integer breakDays;

	@SearchField(dateFieldIncludesTime = true)
	private Date updateDate;

	@SearchField
	private Short updateUserId;

	////////////////////////////////////////////////////////////////////////////////

	//Allows for changing the JSON response to facilitate top/bottom vs side/side view in the UI
	private String dataView;

	// searches for the last/previous reconciliation run date
	private Date previousRunDate;

	private Long parentRunObjectId;

	//Custom search field
	public Boolean showHistory;

	////////////////////////////////////////////////////////////////////////////////


	/**
	 * The table to which the tag is attached, the system tag is NOT associated with the run but the associated definition.
	 * Since this is not the Reconciliation Run Objec table, this must be used in conjunction with the form value 'categoryLinkFieldPath'.
	 */
	@Override
	public String getDaoTableName() {
		return "ReconciliationDefinition";
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getIdNotEquals() {
		return this.idNotEquals;
	}


	public void setIdNotEquals(Long idNotEquals) {
		this.idNotEquals = idNotEquals;
	}


	public Long[] getIds() {
		return this.ids;
	}


	public void setIds(Long[] ids) {
		this.ids = ids;
	}


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public Integer[] getRunIds() {
		return this.runIds;
	}


	public void setRunIds(Integer[] runIds) {
		this.runIds = runIds;
	}


	public Date getImportDate() {
		return this.importDate;
	}


	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}


	public Date getRunDate() {
		return this.runDate;
	}


	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}


	public Date getPreviousRunDate() {
		return this.previousRunDate;
	}


	public void setPreviousRunDate(Date previousRunDate) {
		this.previousRunDate = previousRunDate;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public Integer getDefinitionGroupId() {
		return this.definitionGroupId;
	}


	public void setDefinitionGroupId(Integer definitionGroupId) {
		this.definitionGroupId = definitionGroupId;
	}


	public String getDefinitionGroupName() {
		return this.definitionGroupName;
	}


	public void setDefinitionGroupName(String definitionGroupName) {
		this.definitionGroupName = definitionGroupName;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Boolean getInvestmentAccountIdIsNull() {
		return this.investmentAccountIdIsNull;
	}


	public void setInvestmentAccountIdIsNull(Boolean investmentAccountIdIsNull) {
		this.investmentAccountIdIsNull = investmentAccountIdIsNull;
	}


	public String getInvestmentAccountName() {
		return this.investmentAccountName;
	}


	public void setInvestmentAccountName(String investmentAccountName) {
		this.investmentAccountName = investmentAccountName;
	}


	public String getInvestmentAccountNumber() {
		return this.investmentAccountNumber;
	}


	public void setInvestmentAccountNumber(String investmentAccountNumber) {
		this.investmentAccountNumber = investmentAccountNumber;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Boolean getInvestmentSecurityIdIsNull() {
		return this.investmentSecurityIdIsNull;
	}


	public void setInvestmentSecurityIdIsNull(Boolean investmentSecurityIdIsNull) {
		this.investmentSecurityIdIsNull = investmentSecurityIdIsNull;
	}


	public String getInvestmentSecurityName() {
		return this.investmentSecurityName;
	}


	public void setInvestmentSecurityName(String investmentSecurityName) {
		this.investmentSecurityName = investmentSecurityName;
	}


	public String getInvestmentSecuritySymbol() {
		return this.investmentSecuritySymbol;
	}


	public void setInvestmentSecuritySymbol(String investmentSecuritySymbol) {
		this.investmentSecuritySymbol = investmentSecuritySymbol;
	}


	public Boolean getInvestmentSecurityOrAccountIdIsNull() {
		return this.investmentSecurityOrAccountIdIsNull;
	}


	public void setInvestmentSecurityOrAccountIdIsNull(Boolean investmentSecurityOrAccountIdIsNull) {
		this.investmentSecurityOrAccountIdIsNull = investmentSecurityOrAccountIdIsNull;
	}


	public Short getReconcileStatusId() {
		return this.reconcileStatusId;
	}


	public void setReconcileStatusId(Short reconcileStatusId) {
		this.reconcileStatusId = reconcileStatusId;
	}


	public Short[] getReconcileStatusIds() {
		return this.reconcileStatusIds;
	}


	public void setReconcileStatusIds(Short[] reconcileStatusIds) {
		this.reconcileStatusIds = reconcileStatusIds;
	}


	public String getReconcileStatusName() {
		return this.reconcileStatusName;
	}


	public void setReconcileStatusName(String reconcileStatusName) {
		this.reconcileStatusName = reconcileStatusName;
	}


	public String[] getReconcileStatusNames() {
		return this.reconcileStatusNames;
	}


	public void setReconcileStatusNames(String[] reconcileStatusNames) {
		this.reconcileStatusNames = reconcileStatusNames;
	}


	public Short getMatchStatusId() {
		return this.matchStatusId;
	}


	public void setMatchStatusId(Short matchStatusId) {
		this.matchStatusId = matchStatusId;
	}


	public Short[] getMatchStatusIds() {
		return this.matchStatusIds;
	}


	public void setMatchStatusIds(Short[] matchStatusIds) {
		this.matchStatusIds = matchStatusIds;
	}


	public String getMatchStatusName() {
		return this.matchStatusName;
	}


	public void setMatchStatusName(String matchStatusName) {
		this.matchStatusName = matchStatusName;
	}


	public String[] getMatchStatusNames() {
		return this.matchStatusNames;
	}


	public void setMatchStatusNames(String[] matchStatusNames) {
		this.matchStatusNames = matchStatusNames;
	}


	public String getReconcileOrMatchStatusName() {
		return this.reconcileOrMatchStatusName;
	}


	public void setReconcileOrMatchStatusName(String reconcileOrMatchStatusName) {
		this.reconcileOrMatchStatusName = reconcileOrMatchStatusName;
	}


	public String[] getReconcileOrMatchStatusNames() {
		return this.reconcileOrMatchStatusNames;
	}


	public void setReconcileOrMatchStatusNames(String[] reconcileOrMatchStatusNames) {
		this.reconcileOrMatchStatusNames = reconcileOrMatchStatusNames;
	}


	public Short getApprovalUserId() {
		return this.approvalUserId;
	}


	public void setApprovalUserId(Short approvalUserId) {
		this.approvalUserId = approvalUserId;
	}


	public Boolean getApprovalUserIdIsNull() {
		return this.approvalUserIdIsNull;
	}


	public void setApprovalUserIdIsNull(Boolean approvalUserIdIsNull) {
		this.approvalUserIdIsNull = approvalUserIdIsNull;
	}


	public Date getApprovalDate() {
		return this.approvalDate;
	}


	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}


	public String getIdentifier() {
		return this.identifier;
	}


	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}


	public String[] getIdentifiers() {
		return this.identifiers;
	}


	public void setIdentifiers(String[] identifiers) {
		this.identifiers = identifiers;
	}


	public Integer getBreakDays() {
		return this.breakDays;
	}


	public void setBreakDays(Integer breakDays) {
		this.breakDays = breakDays;
	}


	public Date getUpdateDate() {
		return this.updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public Short getUpdateUserId() {
		return this.updateUserId;
	}


	public void setUpdateUserId(Short updateUserId) {
		this.updateUserId = updateUserId;
	}


	public String getDataView() {
		return this.dataView;
	}


	public void setDataView(String dataView) {
		this.dataView = dataView;
	}


	public Long getParentRunObjectId() {
		return this.parentRunObjectId;
	}


	public void setParentRunObjectId(Long parentRunObjectId) {
		this.parentRunObjectId = parentRunObjectId;
	}


	public Boolean getShowHistory() {
		return this.showHistory;
	}


	public void setShowHistory(Boolean showHistory) {
		this.showHistory = showHistory;
	}
}
