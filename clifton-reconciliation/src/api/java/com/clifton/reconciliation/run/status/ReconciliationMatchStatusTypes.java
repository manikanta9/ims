package com.clifton.reconciliation.run.status;

/**
 * @author KellyJ
 */
public enum ReconciliationMatchStatusTypes {
	NOT_MATCHED("Not Matched", false, false),
	MATCHED("Matched", true, false),
	MANUALLY_MATCHED("Manually Matched", true, true);

	private String description;
	private boolean matched;
	private boolean manual;


	ReconciliationMatchStatusTypes(String description, boolean matched, boolean manual) {
		this.description = description;
		this.matched = matched;
		this.manual = manual;
	}


	public static ReconciliationMatchStatusTypes findReconciliationMatchStatusType(String name) {
		return ReconciliationMatchStatusTypes.valueOf(name);
	}


	public String getDescription() {
		return this.description;
	}


	public boolean isMatched() {
		return this.matched;
	}


	public boolean isManual() {
		return this.manual;
	}
}
