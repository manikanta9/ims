package com.clifton.reconciliation.run;

import com.clifton.core.beans.BaseUpdatableOnlyEntity;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.reconciliation.definition.ReconciliationField;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.run.data.DataError;
import com.clifton.reconciliation.run.data.DataHolder;
import com.clifton.reconciliation.run.data.DataNote;
import com.clifton.reconciliation.run.data.DataNoteTypes;
import com.clifton.reconciliation.run.data.DataObject;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatus;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The reconciliation object
 *
 * @author stevenf on 10/3/2016.
 */
public class ReconciliationRunObject extends BaseUpdatableOnlyEntity<Long> {

	/**
	 * Only required for runObject with a definition that is a 'singleton', e.g. 'Transaction Reconciliation'
	 * where only a single run is maintained for all data imported, used to hold the actual date the data
	 * was imported into the run.
	 */
	private Date importDate;

	private int breakDays;
	private String identifier;

	private ReconciliationRun reconciliationRun;

	private ReconciliationMatchStatus reconciliationMatchStatus;
	private ReconciliationReconcileStatus reconciliationReconcileStatus;

	private ReconciliationInvestmentAccount reconciliationInvestmentAccount;
	private ReconciliationInvestmentSecurity reconciliationInvestmentSecurity;

	// populated when a manual reconcile is performed and represents which source is being reconciled against
	private ReconciliationSource reconciledOnSource;

	/**
	 * Object data holder; top level object that all json data for this reconciliation object is bound to
	 */
	private DataHolder dataHolder;

	////////////////////////////////////////////////////////////////////////////////


	public List<DataNote> getDataNoteList() {
		return getDataHolder().getNotes();
	}


	public void setDataNoteList(List<DataNote> dataNoteList) {
		getDataHolder().setNotes(dataNoteList);
	}


	// creates a DataNote and adds the note to the reconciliation object but does not save the object
	@JsonIgnore
	public void addDataNote(String note, DataNoteTypes noteType, short createUserId) {
		DataNote dataNote = new DataNote();
		dataNote.setNote(note);
		dataNote.setType(noteType);
		dataNote.setCreateDate(new Date());
		dataNote.setCreateUserId(createUserId);

		//Default source to null (global), but then set to a specific source if data only exists for one source.
		dataNote.setSourceId(null);
		List<Short> sourceIdList = getDataHolder().getDataObjects().stream().map(DataObject::getSourceId).collect(Collectors.toList());
		if (CollectionUtils.getSize(sourceIdList) == 1) {
			dataNote.setSourceId(sourceIdList.get(0));
		}

		//If there are existing notes, then we append to them; otherwise add the note at the top-level.
		List<DataNote> noteList = getDataHolder().getNotes();
		if (CollectionUtils.isEmpty(noteList)) {
			noteList = new ArrayList<>();
		}
		noteList.add(dataNote);
		getDataHolder().setNotes(noteList);
	}


	@JsonIgnore
	public boolean isMatched() {
		if (isManuallyMatched()) {
			return true;
		}
		if (getReconciliationRun() != null && getReconciliationRun().getReconciliationDefinition() != null && getReconciliationRun().getReconciliationDefinition().getPrimarySource() != null && getReconciliationRun().getReconciliationDefinition().getSecondarySource() != null) {
			List<Short> dataSourceNames = getDataHolder().getDataObjects().stream().map(DataObject::getSourceId).collect(Collectors.toList());
			if (dataSourceNames.containsAll(Arrays.asList(getReconciliationRun().getReconciliationDefinition().getPrimarySource().getId(), getReconciliationRun().getReconciliationDefinition().getSecondarySource().getId()))) {
				return true;
			}
		}
		return false;
	}


	@JsonIgnore
	public boolean isManuallyMatched() {
		return getReconciliationMatchStatus() != null && getReconciliationMatchStatus().getReconciliationMatchStatusType() == ReconciliationMatchStatusTypes.MANUALLY_MATCHED;
	}


	@JsonIgnore
	public boolean isReconciled() {
		if (isManuallyReconciled()) {
			return true;
		}
		return getReconciliationReconcileStatus() != null && getReconciliationReconcileStatus().getReconciliationReconcileStatusType() == ReconciliationReconcileStatusTypes.RECONCILED;
	}


	/**
	 * returns true if reconciledOnSource is not null (reconciled to) or if the it is both manually matched and manually reconciled.
	 */
	@JsonIgnore
	public boolean isManuallyReconciled() {
		return getReconciledOnSource() != null || (getReconciliationReconcileStatus() != null && isManuallyMatched() && getReconciliationReconcileStatus().getReconciliationReconcileStatusType() == ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED);
	}


	public List<DataError> getErrorList() {
		return getDataHolder().getErrors();
	}


	public String getAccountNumber() {
		if (getDataHolder() != null) {
			// value is the same regardless of the source
			return (String) getDataHolder().getDataObjectValue(null, ReconciliationField.HOLDING_ACCOUNT_FIELD);
		}
		return null;
	}


	public String getSecurityIdentifier() {
		if (getDataHolder() != null) {
			// value is the same regardless of the source
			return (String) getDataHolder().getDataObjectValue(null, ReconciliationField.INVESTMENT_SECURITY_FIELD);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return String.format("{RecObj=[%d], status=[%s], identifier=[%s], acct=[%s], security=[%s], data=[%s]}",
				getId(),
				getReconciliationReconcileStatus() != null ? getReconciliationReconcileStatus().getName() : StringUtils.EMPTY_STRING,
				getIdentifier(),
				getReconciliationInvestmentAccount() != null ? getReconciliationInvestmentAccount().getAccountName() : StringUtils.EMPTY_STRING,
				getReconciliationInvestmentSecurity() != null ? getReconciliationInvestmentSecurity().getSecuritySymbol() : StringUtils.EMPTY_STRING,
				getDataHolder());
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getImportDate() {
		return this.importDate;
	}


	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}


	public int getBreakDays() {
		return this.breakDays;
	}


	public void setBreakDays(int breakDays) {
		this.breakDays = breakDays;
	}


	public String getIdentifier() {
		return this.identifier;
	}


	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}


	@ValueIgnoringGetter
	public DataHolder getDataHolder() {
		if (this.dataHolder == null) {
			this.dataHolder = new DataHolder();
		}
		return this.dataHolder;
	}


	public void setDataHolder(DataHolder dataHolder) {
		this.dataHolder = dataHolder;
	}


	public ReconciliationRun getReconciliationRun() {
		return this.reconciliationRun;
	}


	public void setReconciliationRun(ReconciliationRun reconciliationRun) {
		this.reconciliationRun = reconciliationRun;
	}


	public ReconciliationMatchStatus getReconciliationMatchStatus() {
		return this.reconciliationMatchStatus;
	}


	public void setReconciliationMatchStatus(ReconciliationMatchStatus reconciliationMatchStatus) {
		this.reconciliationMatchStatus = reconciliationMatchStatus;
	}


	public ReconciliationReconcileStatus getReconciliationReconcileStatus() {
		return this.reconciliationReconcileStatus;
	}


	public void setReconciliationReconcileStatus(ReconciliationReconcileStatus reconciliationReconcileStatus) {
		this.reconciliationReconcileStatus = reconciliationReconcileStatus;
	}


	public ReconciliationInvestmentAccount getReconciliationInvestmentAccount() {
		return this.reconciliationInvestmentAccount;
	}


	public void setReconciliationInvestmentAccount(ReconciliationInvestmentAccount reconciliationInvestmentAccount) {
		this.reconciliationInvestmentAccount = reconciliationInvestmentAccount;
	}


	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurity() {
		return this.reconciliationInvestmentSecurity;
	}


	public void setReconciliationInvestmentSecurity(ReconciliationInvestmentSecurity reconciliationInvestmentSecurity) {
		this.reconciliationInvestmentSecurity = reconciliationInvestmentSecurity;
	}


	public ReconciliationSource getReconciledOnSource() {
		return this.reconciledOnSource;
	}


	public void setReconciledOnSource(ReconciliationSource reconciledOnSource) {
		this.reconciledOnSource = reconciledOnSource;
	}
}
