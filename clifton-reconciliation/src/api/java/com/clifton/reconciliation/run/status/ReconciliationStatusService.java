package com.clifton.reconciliation.run.status;

import com.clifton.reconciliation.run.status.search.ReconciliationMatchStatusSearchForm;
import com.clifton.reconciliation.run.status.search.ReconciliationReconcileStatusSearchForm;

import java.util.List;


/**
 * The <code>ReconciliationStatusService</code> defines methods for working with
 * {@link ReconciliationMatchStatus} and {@link ReconciliationReconcileStatus} objects
 * <p>
 * Reconciliation statuses are updated as data is loaded, processed, manually matched/unmatched, and reconciled.
 *
 * @author StevenF on 11/23/2016.
 */
public interface ReconciliationStatusService {

	////////////////////////////////////////////////////////////////////////////
	////////       Reconciliation Match Status Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationMatchStatus getReconciliationMatchStatus(short id);


	public ReconciliationMatchStatus getReconciliationMatchStatusByName(String name);


	public ReconciliationMatchStatus getReconciliationMatchStatusByStatusType(ReconciliationMatchStatusTypes statusType);


	public List<ReconciliationMatchStatus> getReconciliationMatchStatusList(ReconciliationMatchStatusSearchForm searchForm);


	public ReconciliationMatchStatus saveReconciliationMatchStatus(ReconciliationMatchStatus reconciliationMatchStatus);


	public void deleteReconciliationMatchStatus(short id);

	////////////////////////////////////////////////////////////////////////////
	////////     Reconciliation Reconcile Status Business Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationReconcileStatus getReconciliationReconcileStatus(short id);


	public ReconciliationReconcileStatus getReconciliationReconcileStatusByName(String name);


	public ReconciliationReconcileStatus getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes statusType);


	public List<ReconciliationReconcileStatus> getReconciliationReconcileStatusList(ReconciliationReconcileStatusSearchForm searchForm);


	public ReconciliationReconcileStatus saveReconciliationReconcileStatus(ReconciliationReconcileStatus reconciliationReconcileStatus);


	public void deleteReconciliationReconcileStatus(short id);
}
