package com.clifton.reconciliation.run;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.run.search.ReconciliationRunObjectSearchForm;
import com.clifton.reconciliation.run.search.ReconciliationRunOrphanObjectSearchForm;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>ReconciliationStatusService</code> defines methods for working with
 * {@link ReconciliationRunObject}, {@link ReconciliationRunOrphanObject}, {@link ReconciliationRun} objects
 * <p>
 * ReconciliationRuns are the top level objects for the reconciliation process.
 * They during the import of data from external systems and are directly associated with a ReconciliationDefinition
 * <p>
 * ReconciliationRunDetail is a virtual object created dynamically for a given run and contains relevant ReconciliationRunObjects
 * and applicable counts such as matched, breaks, reconciled, etc.  It is the core object used when viewing the overall status
 * of a reconciliation run and executing manual matches and manual reconciliation of objects.
 * <p>
 * ReconciliationRunObjects are the main holder objects for imported data; they are associated with a ReconciliationDefinition
 * which associates them with their given run.
 * <p>
 * ReconciliationRunOrphanObjects contain data that was not processed by any applicable definitions during import.
 *
 * @author StevenF
 */
public interface ReconciliationRunService {

	////////////////////////////////////////////////////////////////////////////
	////////           Reconciliation Run Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationRun getReconciliationRun(int id);


	public List<ReconciliationRun> getReconciliationRunListByIds(Integer[] ids);


	public ReconciliationRun getReconciliationRunByDefinitionAndDate(int definitionId, Date runDate);


	public List<ReconciliationRun> getReconciliationRunList(ReconciliationRunSearchForm searchForm);


	public ReconciliationRun saveReconciliationRun(ReconciliationRun run);


	public void deleteReconciliationRun(int id);


	public void approveReconciliationRun(int id);


	public void unapproveReconciliationRun(int id, String note);

	////////////////////////////////////////////////////////////////////////////
	////////        Reconciliation Run Object Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationRunObject getReconciliationRunObject(long id);


	public List<ReconciliationRunObject> getReconciliationRunObjectList(Long[] ids);


	public List<ReconciliationRunObject> getReconciliationRunObjectList(ReconciliationRunObjectSearchForm searchForm);


	@DoNotAddRequestMapping
	public List<ReconciliationRunObject> getReconciliationRunObjectListByRunId(int runId);


	@DoNotAddRequestMapping
	public List<ReconciliationRunObject> getReconciliationRunObjectListByRunIdAndReconcileStatusName(int runId, String reconcileStatusName);


	public ReconciliationRunObject saveReconciliationRunObject(ReconciliationRunObject runObject);


	@DoNotAddRequestMapping
	public void saveReconciliationRunObjectList(final List<ReconciliationRunObject> runObjectList);


	public void deleteReconciliationRunObject(long id);


	@DoNotAddRequestMapping
	public List<ReconciliationRunObject> getReconciliationParentRunObjectList(long runObjectId, Date runDate);


	@SecureMethod(dtoClass = ReconciliationRunObject.class)
	public Map<Long, Set<Long>> getReconciliationParentRunObjectMapByRunObjectIds(Long[] runObjectIds, Date runDate);


	////////////////////////////////////////////////////////////////////////////
	///////       Reconciliation Run Orphan Object Business Methods      ///////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationRunOrphanObject getReconciliationRunOrphanObject(long id);


	public List<ReconciliationRunOrphanObject> getReconciliationRunOrphanObjectList(ReconciliationRunOrphanObjectSearchForm searchForm);


	public List<ReconciliationRunOrphanObject> getReconciliationRunOrphanObjectListByRunId(int runId);


	public ReconciliationRunOrphanObject saveReconciliationRunOrphanObject(ReconciliationRunOrphanObject orphanedSourceData);


	public void deleteReconciliationRunOrphanObject(long id);


	public void deleteReconciliationRunOrphanObjectList(List<ReconciliationRunOrphanObject> orphanObjectList);


	////////////////////////////////////////////////////////////////////////////////
	////////////    Reconciliation Object Run Details and Processing    ////////////
	////////////////////////////////////////////////////////////////////////////////


	@RequestMapping("reconciliationRunObjectListReconcile")
	public void reconcileReconciliationRunObjectList(Long[] runObjectIds, boolean reconcileToPrimary, String note);


	@RequestMapping("reconciliationRunObjectListUnreconcile")
	public void unreconcileReconciliationRunObjectList(Long[] runObjectIds, String note);


	@RequestMapping("reconciliationRunObjectListMatch")
	public ReconciliationRunObject matchReconciliationRunObjectList(Long[] runObjectIds, String note);


	@DoNotAddRequestMapping
	public ReconciliationRunObject matchReconciliationRunObjectList(List<ReconciliationRunObject> runObjectList, String note);


	@RequestMapping("reconciliationRunObjectListUnmatch")
	public List<ReconciliationRunObject> unmatchReconciliationRunObjectList(Long[] runObjectIds, String note);


	@DoNotAddRequestMapping
	public List<ReconciliationRunObject> unmatchReconciliationRunObjectList(List<ReconciliationRunObject> runObjectList, String note);


	@SecureMethod(dtoClass = ReconciliationRunObject.class)
	public void saveReconciliationRunObjectDataNoteList(Long[] runObjectIds, String note);


	////////////////////////////////////////////////////////////////////////////////
	////////////       Reconciliation Run Details and Processing        ////////////
	////////////////////////////////////////////////////////////////////////////////


	@RequestMapping("reconciliationRunReprocess")
	public Status reprocessReconciliationRun(int runId);


	@DoNotAddRequestMapping
	public Status reprocessReconciliationRun(ReconciliationRun run, List<ReconciliationRunObject> runObjectList, Map<String, ReconciliationRunObject> runObjectMap, Status status);


	////////////////////////////////////////////////////////////////////////////////
	////////////           Reconciliation Run Utility Methods           ////////////
	////////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public Map<String, Integer> getReconciliationBreakDaysMap(int definitionId, Date runDate, boolean singleton);
}
