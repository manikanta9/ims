package com.clifton.reconciliation.run.comparison;

import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.data.DataNote;
import com.clifton.reconciliation.run.search.ReconciliationRunObjectSearchForm;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;

import java.util.List;


/**
 * @author stevenf
 */
public class ReconciliationRunApprovalUserHasNotModifiedRunComparison implements Comparison<ReconciliationRun> {

	private SecurityUserService securityUserService;
	private ReconciliationRunService reconciliationRunService;


	@Override
	public boolean evaluate(@SuppressWarnings("unused") ReconciliationRun run, ComparisonContext context) {
		SecurityUser currentUser = getSecurityUserService().getSecurityUserCurrent();
		AssertUtils.assertNotNull(currentUser, "Current user is not set.");
		AssertUtils.assertNotNull(run, "Run may not be null!");

		boolean result = true;
		ReconciliationRunObjectSearchForm runObjectSearchForm = new ReconciliationRunObjectSearchForm();
		runObjectSearchForm.setRunId(run.getId());
		runObjectSearchForm.setReconcileOrMatchStatusNames(new String[]{ReconciliationMatchStatusTypes.MANUALLY_MATCHED.name(), ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED.name()});
		List<ReconciliationRunObject> runObjectList = getReconciliationRunService().getReconciliationRunObjectList(runObjectSearchForm);
		for (ReconciliationRunObject runObject : CollectionUtils.getIterable(runObjectList)) {
			for (DataNote dataNote : CollectionUtils.getIterable(runObject.getDataNoteList())) {
				if (CompareUtils.isEqual(dataNote.getCreateUserId(), currentUser.getId())) {
					result = false;
					break;
				}
			}
			if (!result) {
				break;
			}
		}
		if (context != null) {
			String messageNotModified = "(Current user '" + currentUser.getLabel() + "' has not modified '" + run.getLabel() + "')";
			String messageModified = "(Current user '" + currentUser.getLabel() + "' has modified '" + run.getLabel() + "' and may not approve!";
			if (result) {
				context.recordTrueMessage(messageNotModified);
			}
			else {
				context.recordFalseMessage(messageModified);
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
