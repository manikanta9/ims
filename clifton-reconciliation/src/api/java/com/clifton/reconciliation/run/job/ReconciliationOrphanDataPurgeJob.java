package com.clifton.reconciliation.run.job;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.run.ReconciliationRunOrphanObjectTypes;

import java.util.Map;
import java.util.stream.Collectors;


/**
 * <code>ReconciliationOrphanDataPurgeJob</code> batch job for purging {@link com.clifton.reconciliation.run.ReconciliationRunOrphanObject} entities.
 *
 * @author TerryS
 */
public class ReconciliationOrphanDataPurgeJob implements Task, StatusHolderObjectAware<Status> {

	private ReconciliationRunOrphanObjectTypes orphanType;
	private int daysToRetain;
	private boolean reviewRequired;

	private StatusHolderObject<Status> statusHolderObject;

	private ReconciliationOrphanDataPurgeService reconciliationOrphanDataPurgeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		AssertUtils.assertNotNull(context, "Context Parameter is required.");

		ReconciliationOrphanDataPurgeCommand command = ReconciliationOrphanDataPurgeCommand.ofSynchronousWithThrow();
		command.setStatus(getStatusHolderObject().getStatus());

		ValidationUtils.assertNotNull(getOrphanType(), "Orphan Type is required.");
		command.setOrphanType(getOrphanType());

		ValidationUtils.assertTrue(getDaysToRetain() > 0, "Days to retain must be non-zero.");
		command.setDaysToRetain(getDaysToRetain());
		command.setReviewRequired(isReviewRequired());

		Status status = getReconciliationOrphanDataPurgeService().processReconciliationOrphanDataPurge(command);
		String batchJobStatus = status.getDetailList().stream().map(d -> d.getCategory() + ": " + d.getNote()).collect(Collectors.joining("\n"));
		status.setMessage(batchJobStatus);
		return status;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public StatusHolderObject<Status> getStatusHolderObject() {
		return this.statusHolderObject;
	}


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	public ReconciliationRunOrphanObjectTypes getOrphanType() {
		return this.orphanType;
	}


	public void setOrphanType(ReconciliationRunOrphanObjectTypes orphanType) {
		this.orphanType = orphanType;
	}


	public int getDaysToRetain() {
		return this.daysToRetain;
	}


	public void setDaysToRetain(int daysToRetain) {
		this.daysToRetain = daysToRetain;
	}


	public boolean isReviewRequired() {
		return this.reviewRequired;
	}


	public void setReviewRequired(boolean reviewRequired) {
		this.reviewRequired = reviewRequired;
	}


	public ReconciliationOrphanDataPurgeService getReconciliationOrphanDataPurgeService() {
		return this.reconciliationOrphanDataPurgeService;
	}


	public void setReconciliationOrphanDataPurgeService(ReconciliationOrphanDataPurgeService reconciliationOrphanDataPurgeService) {
		this.reconciliationOrphanDataPurgeService = reconciliationOrphanDataPurgeService;
	}
}
