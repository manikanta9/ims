package com.clifton.reconciliation.file.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author StevenF on 10/02/2017.
 */
public class ReconciliationFileDefinitionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	/**
	 * named this searchField 'definitionFileName' because 'fileName' is passed when trying to download to excel,
	 * which then binds to this parameter if left as fileName; renaming avoids that conflict so the excel download still works.
	 */
	@SearchField(searchField = "fileName")
	private String definitionFileName;

	@SearchField(searchField = "source.id")
	private Short sourceId;

	@SearchField
	private String securitiesFileName;

	@SearchField
	private Boolean saveOrphanedData;

	//custom search field
	private Integer definitionId;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getDefinitionFileName() {
		return this.definitionFileName;
	}


	public void setDefinitionFileName(String definitionFileName) {
		this.definitionFileName = definitionFileName;
	}


	public Short getSourceId() {
		return this.sourceId;
	}


	public void setSourceId(Short sourceId) {
		this.sourceId = sourceId;
	}


	public String getSecuritiesFileName() {
		return this.securitiesFileName;
	}


	public void setSecuritiesFileName(String securitiesFileName) {
		this.securitiesFileName = securitiesFileName;
	}


	public Boolean getSaveOrphanedData() {
		return this.saveOrphanedData;
	}


	public void setSaveOrphanedData(Boolean saveOrphanedData) {
		this.saveOrphanedData = saveOrphanedData;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}
}
