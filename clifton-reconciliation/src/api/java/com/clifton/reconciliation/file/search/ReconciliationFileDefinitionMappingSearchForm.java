package com.clifton.reconciliation.file.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author StevenF on 10/02/2017.
 */
public class ReconciliationFileDefinitionMappingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "fileDefinition.id")
	private Integer fileDefinitionId;

	@SearchField(searchFieldPath = "fileDefinition", searchField = "name")
	private String fileDefinitionName;

	@SearchField(searchField = "reconciliationDefinition.id")
	private Integer reconciliationDefinitionId;

	@SearchField(searchFieldPath = "reconciliationDefinition", searchField = "name")
	private String reconciliationDefinitionName;

	@SearchField(searchFieldPath = "fileDefinition", searchField = "source.id")
	private Short sourceId;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getFileDefinitionId() {
		return this.fileDefinitionId;
	}


	public void setFileDefinitionId(Integer fileDefinitionId) {
		this.fileDefinitionId = fileDefinitionId;
	}


	public String getFileDefinitionName() {
		return this.fileDefinitionName;
	}


	public void setFileDefinitionName(String fileDefinitionName) {
		this.fileDefinitionName = fileDefinitionName;
	}


	public Integer getReconciliationDefinitionId() {
		return this.reconciliationDefinitionId;
	}


	public void setReconciliationDefinitionId(Integer reconciliationDefinitionId) {
		this.reconciliationDefinitionId = reconciliationDefinitionId;
	}


	public String getReconciliationDefinitionName() {
		return this.reconciliationDefinitionName;
	}


	public void setReconciliationDefinitionName(String reconciliationDefinitionName) {
		this.reconciliationDefinitionName = reconciliationDefinitionName;
	}


	public Short getSourceId() {
		return this.sourceId;
	}


	public void setSourceId(Short sourceId) {
		this.sourceId = sourceId;
	}
}
