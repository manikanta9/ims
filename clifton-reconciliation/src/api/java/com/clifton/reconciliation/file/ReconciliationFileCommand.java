package com.clifton.reconciliation.file;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Date;
import java.util.function.Function;


/**
 * We could enable the binding here, but that only populates the DTO object itself,
 * not child properties, since the definition needs it's fields populated, I do not
 * use binding, and instead look up the object via the service method using the id
 * that does get populated since the service method auto-populates the definitionFieldList.
 *
 * @author StevenF
 */
@NonPersistentObject
public class ReconciliationFileCommand {

	private boolean automatedUpload;

	/**
	 * Allows for lazily retrieving the file; used when events are raised from integration;
	 * First the fileName is used to determine if a fileDefinitionMapping exists for it and
	 * if it should be processed; the file is only downloaded if it should be processed.
	 */
	private Integer fileRetrievalId;
	private Function<Integer, File> fileRetrievalFunction;

	/**
	 * Used to explicitly set the fileName of the file coming in.
	 * Used when automatically uploading a file from integration so the original fileName
	 * as processed in integration can be passed and used to look up the proper
	 * ReconciliationFileDefinition (otherwise the file has a timestamp added to the name)
	 */
	private String fileName;

	private MultipartFile file;

	private Date importDate;
	private Integer fileDefinitionId;

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getFileRetrievalId() {
		return this.fileRetrievalId;
	}


	public void setFileRetrievalId(Integer fileRetrievalId) {
		this.fileRetrievalId = fileRetrievalId;
	}


	public Function<Integer, File> getFileRetrievalFunction() {
		return this.fileRetrievalFunction;
	}


	public void setFileRetrievalFunction(Function<Integer, File> fileRetrievalFunction) {
		this.fileRetrievalFunction = fileRetrievalFunction;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public boolean isAutomatedUpload() {
		return this.automatedUpload;
	}


	public void setAutomatedUpload(boolean automatedUpload) {
		this.automatedUpload = automatedUpload;
	}


	public MultipartFile getFile() {
		return this.file;
	}


	public void setFile(MultipartFile file) {
		this.file = file;
	}


	public Date getImportDate() {
		return this.importDate;
	}


	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}


	public Integer getFileDefinitionId() {
		return this.fileDefinitionId;
	}


	public void setFileDefinitionId(Integer fileDefinitionId) {
		this.fileDefinitionId = fileDefinitionId;
	}
}
