package com.clifton.reconciliation.file;

import com.clifton.core.beans.BaseEntity;
import com.clifton.reconciliation.definition.ReconciliationDefinition;


public class ReconciliationFileDefinitionMapping extends BaseEntity<Integer> {

	ReconciliationFileDefinition fileDefinition;
	ReconciliationDefinition reconciliationDefinition;

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationFileDefinition getFileDefinition() {
		return this.fileDefinition;
	}


	public void setFileDefinition(ReconciliationFileDefinition fileDefinition) {
		this.fileDefinition = fileDefinition;
	}


	public ReconciliationDefinition getReconciliationDefinition() {
		return this.reconciliationDefinition;
	}


	public void setReconciliationDefinition(ReconciliationDefinition reconciliationDefinition) {
		this.reconciliationDefinition = reconciliationDefinition;
	}
}
