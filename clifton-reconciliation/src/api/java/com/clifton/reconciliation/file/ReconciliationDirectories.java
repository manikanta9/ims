package com.clifton.reconciliation.file;

/**
 * context defined directories
 *
 * @author jonathanr
 */
public enum ReconciliationDirectories {
	ARCHIVE, PROCESSING
}
