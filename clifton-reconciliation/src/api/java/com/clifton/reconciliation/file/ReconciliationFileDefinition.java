package com.clifton.reconciliation.file;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.OneToManyEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.reconciliation.definition.ReconciliationSource;

import java.util.List;


/**
 * The <code>ReconciliationFileDefinition</code> allows end users to map file uploaded to the reconciliation system
 * to definition(s) for a given source for processing.
 *
 * @author StevenF
 */
@CacheByName
public class ReconciliationFileDefinition extends NamedEntity<Integer> {

	private String fileName;
	/**
	 * Optional additional advent securities file name
	 * This is used allow pulling the securities file into the 'External Loads' tab on ReconciliationRun UI screen.
	 */
	private String securitiesFileName;

	private ReconciliationSource source;

	/**
	 * Defines whether data that is not processed by any reconciliation definitions associated with this file definition
	 * should be saved and marked as an orphan.  This allows ops to review data that is excluded by definitions to ensure
	 * configuration is not wrong.
	 */
	private boolean saveOrphanedData;

	@OneToManyEntity(serviceBeanName = "reconciliationFileDefinitionService", serviceMethodName = "getReconciliationFileDefinitionMappingListByFileDefinitionId")
	private List<ReconciliationFileDefinitionMapping> fileDefinitionMappingList;

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSaveOrphanedData() {
		return this.saveOrphanedData;
	}


	public void setSaveOrphanedData(boolean saveOrphanedData) {
		this.saveOrphanedData = saveOrphanedData;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getSecuritiesFileName() {
		return this.securitiesFileName;
	}


	public void setSecuritiesFileName(String securitiesFileName) {
		this.securitiesFileName = securitiesFileName;
	}


	public ReconciliationSource getSource() {
		return this.source;
	}


	public void setSource(ReconciliationSource source) {
		this.source = source;
	}


	public List<ReconciliationFileDefinitionMapping> getFileDefinitionMappingList() {
		return this.fileDefinitionMappingList;
	}


	public void setFileDefinitionMappingList(List<ReconciliationFileDefinitionMapping> fileDefinitionMappingList) {
		this.fileDefinitionMappingList = fileDefinitionMappingList;
	}
}
