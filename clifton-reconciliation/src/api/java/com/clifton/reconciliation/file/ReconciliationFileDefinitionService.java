package com.clifton.reconciliation.file;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.reconciliation.file.search.ReconciliationFileDefinitionMappingSearchForm;
import com.clifton.reconciliation.file.search.ReconciliationFileDefinitionSearchForm;

import java.util.List;


/**
 * The <code>ReconciliationFileDefinitionService</code> interface defines methods for managing
 * {@link ReconciliationFileDefinition} and {@link ReconciliationFileDefinitionMapping} objects.
 *
 * @author StevenF on 11/23/2016.
 */
public interface ReconciliationFileDefinitionService {

	////////////////////////////////////////////////////////////////////////////
	////////    Reconciliation Upload File Definition Business Methods    /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationFileDefinition getReconciliationFileDefinition(int id);


	public ReconciliationFileDefinition getReconciliationFileDefinitionByName(String name);


	public List<ReconciliationFileDefinition> getReconciliationFileDefinitionList(ReconciliationFileDefinitionSearchForm searchForm);


	public List<ReconciliationFileDefinition> getReconciliationFileDefinitionListPopulated(ReconciliationFileDefinitionSearchForm searchForm);


	@DoNotAddRequestMapping
	public ReconciliationFileDefinition saveReconciliationFileDefinition(ReconciliationFileDefinition reconciliationFileDefinition);


	public ReconciliationFileDefinition saveReconciliationFileDefinitionPopulated(ReconciliationFileDefinition reconciliationFileDefinition);


	public void deleteReconciliationFileDefinition(int id);

	////////////////////////////////////////////////////////////////////////////
	///    Reconciliation Upload File Definition Mapping Business Methods    ///
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationFileDefinitionMapping getReconciliationFileDefinitionMapping(int id);


	public List<ReconciliationFileDefinitionMapping> getReconciliationFileDefinitionMappingList(ReconciliationFileDefinitionMappingSearchForm searchForm);


	public List<ReconciliationFileDefinitionMapping> getReconciliationFileDefinitionMappingListByFileDefinitionId(int fileDefinitionId);


	public ReconciliationFileDefinitionMapping saveReconciliationFileDefinitionMapping(ReconciliationFileDefinitionMapping reconciliationFileDefinitionMapping);


	public void deleteReconciliationFileDefinitionMapping(int id);
}
