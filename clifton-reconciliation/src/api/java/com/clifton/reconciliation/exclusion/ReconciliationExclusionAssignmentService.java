package com.clifton.reconciliation.exclusion;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.exclusion.search.ReconciliationExclusionAssignmentSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>ReconciliationExclusionAssignmentService</code> d
 *
 * @author StevenF
 */
public interface ReconciliationExclusionAssignmentService {


	////////////////////////////////////////////////////////////////////////////
	////////   Reconciliation Exclusion Assignment Business Methods    /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationExclusionAssignment getReconciliationExclusionAssignment(int id);


	public List<ReconciliationExclusionAssignment> getReconciliationExclusionAssignmentList(ReconciliationExclusionAssignmentSearchForm searchForm);


	public ReconciliationExclusionAssignment saveReconciliationExclusionAssignment(ReconciliationExclusionAssignment exclusionAssignment);


	public void deleteReconciliationExclusionAssignment(int id);

	////////////////////////////////////////////////////////////////////////////
	////////    Reconciliation Exclusion Assignment Utility Methods    /////////
	////////////////////////////////////////////////////////////////////////////


	public Status processReconciliationExclusionAssignment(short sourceId, Date runDate, boolean synchronous);


	@SecureMethod(permissions = SecurityPermission.PERMISSION_EXECUTE)
	public void approveReconciliationExclusionAssignment(int id);


	@SecureMethod(permissions = SecurityPermission.PERMISSION_EXECUTE)
	public void unapproveReconciliationExclusionAssignment(int id);
}
