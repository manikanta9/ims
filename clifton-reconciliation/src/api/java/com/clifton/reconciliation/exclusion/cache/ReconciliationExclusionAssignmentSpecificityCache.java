package com.clifton.reconciliation.exclusion.cache;

import com.clifton.reconciliation.exclusion.ReconciliationExclusionAssignment;


/**
 * The <class>ReconciliationExclusionAssignmentSpecificityCache</class> is a specificity cache used to lookup
 * the most specific {@link ReconciliationExclusionAssignment} based on source, definition, account and security.
 */
public interface ReconciliationExclusionAssignmentSpecificityCache {

	public ReconciliationExclusionAssignment getMostSpecificResult(ReconciliationExclusionAssignmentSource source);
}
