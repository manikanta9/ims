package com.clifton.reconciliation.exclusion.cache;

import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;

import java.util.Date;


/**
 * The <code>ReconciliationExclusionAssignmentSource</code> is used by the ReconciliationExclusionAssignmentSpecificityCache as the
 * target object to find the most specific result for.
 *
 * @author StevenF
 */
public class ReconciliationExclusionAssignmentSource {

	private final ReconciliationSource source;
	private final ReconciliationDefinition definition;
	private final ReconciliationInvestmentAccount investmentAccount;
	private final ReconciliationInvestmentSecurity investmentSecurity;
	private final String invalidAccountNumber;
	private final String invalidSecurityIdentifier;
	private final Date date;


	public ReconciliationExclusionAssignmentSource(ReconciliationSource source, ReconciliationDefinition definition, ReconciliationInvestmentAccount investmentAccount, ReconciliationInvestmentSecurity investmentSecurity, String invalidAccountNumber, String invalidSecurityIdentifier, Date date) {
		this.source = source;
		this.definition = definition;
		this.investmentAccount = investmentAccount;
		this.investmentSecurity = investmentSecurity;
		this.invalidAccountNumber = invalidAccountNumber;
		this.invalidSecurityIdentifier = invalidSecurityIdentifier;
		this.date = date;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                 Getter & Setter Methods                   /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationSource getSource() {
		return this.source;
	}


	public ReconciliationDefinition getDefinition() {
		return this.definition;
	}


	public ReconciliationInvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public ReconciliationInvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public String getInvalidAccountNumber() {
		return this.invalidAccountNumber;
	}


	public String getInvalidSecurityIdentifier() {
		return this.invalidSecurityIdentifier;
	}


	public Date getDate() {
		return this.date;
	}
}
