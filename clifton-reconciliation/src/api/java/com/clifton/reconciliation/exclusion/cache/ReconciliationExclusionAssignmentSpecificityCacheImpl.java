package com.clifton.reconciliation.exclusion.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.AbstractSpecificityCache;
import com.clifton.core.cache.specificity.ClearSpecificityCacheUpdater;
import com.clifton.core.cache.specificity.SpecificityCacheTypes;
import com.clifton.core.cache.specificity.SpecificityCacheUpdater;
import com.clifton.core.cache.specificity.key.SimpleOrderingSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.exclusion.ReconciliationExclusionAssignment;
import com.clifton.reconciliation.exclusion.ReconciliationExclusionAssignmentService;
import com.clifton.reconciliation.exclusion.search.ReconciliationExclusionAssignmentSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * The <class>ReconciliationExclusionAssignmentSpecificityCacheImpl</class> is a specificity cache used to find
 * the most specific {@link ReconciliationExclusionAssignment} based on the source, definition, account and security.
 */
@Component
public class ReconciliationExclusionAssignmentSpecificityCacheImpl extends AbstractSpecificityCache<ReconciliationExclusionAssignmentSource, ReconciliationExclusionAssignment, ReconciliationExclusionAssignmentTargetHolder> implements ReconciliationExclusionAssignmentSpecificityCache {

	private ReconciliationExclusionAssignmentService reconciliationExclusionAssignmentService;

	private final ClearSpecificityCacheUpdater<ReconciliationExclusionAssignmentTargetHolder> cacheUpdater = new ClearSpecificityCacheUpdater<>(ReconciliationExclusionAssignment.class);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationExclusionAssignment getMostSpecificResult(ReconciliationExclusionAssignmentSource source) {
		return CollectionUtils.getFirstElement(this.getMostSpecificResultList(source));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationExclusionAssignmentSpecificityCacheImpl() {
		super(SpecificityCacheTypes.ONE_TO_ONE_RESULT, new SimpleOrderingSpecificityKeyGenerator());
	}


	@Override
	protected boolean isMatch(ReconciliationExclusionAssignmentSource source, ReconciliationExclusionAssignmentTargetHolder holder) {
		boolean dateRangeMatched = DateUtils.isDateBetween(source.getDate(), holder.getStartDate(), holder.getEndDate(), false);
		if (!dateRangeMatched) {
			return false;
		}
		return true;
	}


	@Override
	protected ReconciliationExclusionAssignment getTarget(ReconciliationExclusionAssignmentTargetHolder targetHolder) {
		return getReconciliationExclusionAssignmentService().getReconciliationExclusionAssignment(targetHolder.getId());
	}


	@Override
	protected Collection<ReconciliationExclusionAssignmentTargetHolder> getTargetHolders() {
		List<ReconciliationExclusionAssignmentTargetHolder> holders = new ArrayList<>();
		List<ReconciliationExclusionAssignment> exclusionAssignmentList = getReconciliationExclusionAssignmentService().getReconciliationExclusionAssignmentList(new ReconciliationExclusionAssignmentSearchForm());
		for (ReconciliationExclusionAssignment exclusionAssignment : CollectionUtils.getIterable(exclusionAssignmentList)) {
			holders.add(new ReconciliationExclusionAssignmentTargetHolder(generateKey(exclusionAssignment), exclusionAssignment));
		}
		return holders;
	}


	private String generateKey(ReconciliationExclusionAssignment exclusionAssignment) {
		return getKeyGenerator().generateKeyForTarget(generateKeySource(exclusionAssignment));
	}


	private SpecificityKeySource generateKeySource(ReconciliationExclusionAssignment exclusionAssignment) {
		Object[] sourceHierarchy = {BeanUtils.getBeanIdentity(exclusionAssignment.getReconciliationSource()), null};
		Object[] definitionHierarchy = {BeanUtils.getBeanIdentity(exclusionAssignment.getReconciliationDefinition()), null};
		Object[] accountHierarchy = {BeanUtils.getBeanIdentity(exclusionAssignment.getReconciliationInvestmentAccount()), exclusionAssignment.getInvalidAccountNumber(), null};
		Object[] securityHierarchy = {BeanUtils.getBeanIdentity(exclusionAssignment.getReconciliationInvestmentSecurity()), exclusionAssignment.getInvalidSecurityIdentifier(), null};
		Object[] valuesToAppend = {};

		return SpecificityKeySource.builder(sourceHierarchy)
				.addHierarchy(definitionHierarchy)
				.addHierarchy(accountHierarchy)
				.addHierarchy(securityHierarchy)
				.setValuesToAppend(valuesToAppend)
				.build();
	}


	@Override
	protected SpecificityKeySource getKeySource(ReconciliationExclusionAssignmentSource source) {
		ReconciliationExclusionAssignment exclusionAssignment = new ReconciliationExclusionAssignment();
		exclusionAssignment.setReconciliationSource(source.getSource());
		exclusionAssignment.setReconciliationDefinition(source.getDefinition());
		exclusionAssignment.setReconciliationInvestmentAccount(source.getInvestmentAccount());
		exclusionAssignment.setReconciliationInvestmentSecurity(source.getInvestmentSecurity());
		exclusionAssignment.setInvalidAccountNumber(source.getInvalidAccountNumber());
		exclusionAssignment.setInvalidSecurityIdentifier(source.getInvalidSecurityIdentifier());

		return generateKeySource(exclusionAssignment);
	}


	@Override
	protected SpecificityCacheUpdater<ReconciliationExclusionAssignmentTargetHolder> getCacheUpdater() {
		return this.cacheUpdater;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationExclusionAssignmentService getReconciliationExclusionAssignmentService() {
		return this.reconciliationExclusionAssignmentService;
	}


	public void setReconciliationExclusionAssignmentService(ReconciliationExclusionAssignmentService reconciliationExclusionAssignmentService) {
		this.reconciliationExclusionAssignmentService = reconciliationExclusionAssignmentService;
	}
}
