package com.clifton.reconciliation.exclusion.cache;

import com.clifton.core.cache.specificity.SpecificityScope;
import com.clifton.core.cache.specificity.SpecificityTargetHolder;
import com.clifton.reconciliation.exclusion.ReconciliationExclusionAssignment;

import java.util.Date;


/**
 * The <code>ReconciliationExclusionAssignmentTargetHolder</code> is the object stored in the {@link ReconciliationExclusionAssignmentSpecificityCache}
 * cache and used to determine the most specific result.
 */
public class ReconciliationExclusionAssignmentTargetHolder implements SpecificityTargetHolder {

	private final String key;
	private final int id;
	private final SpecificityScope scope;

	private final Date startDate;
	private final Date endDate;


	public ReconciliationExclusionAssignmentTargetHolder(String key, ReconciliationExclusionAssignment exclusionAssignment) {
		this.id = exclusionAssignment.getId();
		this.startDate = exclusionAssignment.getStartDate();
		this.endDate = exclusionAssignment.getEndDate();

		this.key = key;
		this.scope = null;
	}


	@Override
	public SpecificityScope getScope() {
		return null;
	}


	public int getId() {
		return this.id;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	@Override
	public String getKey() {
		return this.key;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.id;
		result = prime * result + ((this.key == null) ? 0 : this.key.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ReconciliationExclusionAssignmentTargetHolder other = (ReconciliationExclusionAssignmentTargetHolder) obj;
		if (this.id != other.id) {
			return false;
		}
		if (this.key == null) {
			if (other.key != null) {
				return false;
			}
		}
		else if (!this.key.equals(other.key)) {
			return false;
		}
		if (!this.scope.equals(other.scope)) {
			return false;
		}
		return true;
	}
}
