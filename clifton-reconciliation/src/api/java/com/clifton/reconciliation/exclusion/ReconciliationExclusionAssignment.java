package com.clifton.reconciliation.exclusion;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.reconcile.ReconciliationApprovalAware;
import com.clifton.security.user.SecurityUser;

import java.util.Date;


/**
 * @author stevenf
 */
public class ReconciliationExclusionAssignment extends BaseEntity<Integer> implements ReconciliationApprovalAware, LabeledObject {

	private boolean include;

	private Date startDate;
	private Date endDate;

	private ReconciliationDefinition reconciliationDefinition;
	private ReconciliationSource reconciliationSource;
	private ReconciliationInvestmentAccount reconciliationInvestmentAccount;
	private ReconciliationInvestmentSecurity reconciliationInvestmentSecurity;

	/**
	 * Used when there is an account that does not exist in the system and
	 * therefore cannot be mapped to a normalized account but still must be excluded.
	 */
	private String invalidAccountNumber;

	/**
	 * Used when there is a a security that does not exist in the system and
	 * therefore cannot be mapped to a normalized security but still must be excluded.
	 */
	private String invalidSecurityIdentifier;

	private String comment;

	private SecurityUser approvalUser;

	private Date approvalDate;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public SecurityUser getApprovalUser() {
		return this.approvalUser;
	}


	@Override
	public void setApprovalUser(SecurityUser approvalUser) {
		this.approvalUser = approvalUser;
	}


	@Override
	public Date getApprovalDate() {
		return this.approvalDate;
	}


	@Override
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}


	@Override
	public String getLabel() {
		StringBuilder label = new StringBuilder();
		label.append("Exclusion Assignment for ")
				//Allow source to be null - validation will catch it.
				.append(getReconciliationSource() != null ? getReconciliationSource().getName() : null)
				.append(" - ")
				.append(getReconciliationInvestmentAccount() != null ? getReconciliationInvestmentAccount().getAccountNumber() : "")
				.append(getInvalidAccountNumber() != null ? getInvalidAccountNumber() : "")
				.append(getReconciliationInvestmentSecurity() != null ? getReconciliationInvestmentSecurity().getSecuritySymbol() : "")
				.append(getInvalidSecurityIdentifier() != null ? getInvalidSecurityIdentifier() : "");
		return label.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationDefinition getReconciliationDefinition() {
		return this.reconciliationDefinition;
	}


	public void setReconciliationDefinition(ReconciliationDefinition reconciliationDefinition) {
		this.reconciliationDefinition = reconciliationDefinition;
	}


	public ReconciliationSource getReconciliationSource() {
		return this.reconciliationSource;
	}


	public void setReconciliationSource(ReconciliationSource reconciliationSource) {
		this.reconciliationSource = reconciliationSource;
	}


	public ReconciliationInvestmentAccount getReconciliationInvestmentAccount() {
		return this.reconciliationInvestmentAccount;
	}


	public void setReconciliationInvestmentAccount(ReconciliationInvestmentAccount reconciliationInvestmentAccount) {
		this.reconciliationInvestmentAccount = reconciliationInvestmentAccount;
	}


	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurity() {
		return this.reconciliationInvestmentSecurity;
	}


	public void setReconciliationInvestmentSecurity(ReconciliationInvestmentSecurity reconciliationInvestmentSecurity) {
		this.reconciliationInvestmentSecurity = reconciliationInvestmentSecurity;
	}


	public String getInvalidAccountNumber() {
		return this.invalidAccountNumber;
	}


	public void setInvalidAccountNumber(String invalidAccountNumber) {
		this.invalidAccountNumber = invalidAccountNumber;
	}


	public String getInvalidSecurityIdentifier() {
		return this.invalidSecurityIdentifier;
	}


	public void setInvalidSecurityIdentifier(String invalidSecurityIdentifier) {
		this.invalidSecurityIdentifier = invalidSecurityIdentifier;
	}


	public boolean isInclude() {
		return this.include;
	}


	public void setInclude(boolean include) {
		this.include = include;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getComment() {
		return this.comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}
}
