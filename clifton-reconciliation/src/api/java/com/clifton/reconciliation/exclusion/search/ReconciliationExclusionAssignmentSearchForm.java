package com.clifton.reconciliation.exclusion.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.util.Date;


/**
 * @author StevenF on 11/22/2016.
 */
public class ReconciliationExclusionAssignmentSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "reconciliationDefinition.name,reconciliationSource.name,reconciliationInvestmentAccount.accountNumber")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField(searchField = "reconciliationDefinition.id")
	private Integer definitionId;

	@SearchField(searchField = "reconciliationDefinition.id", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Integer definitionIdOrNull;

	@SearchField(searchField = "reconciliationDefinition.id")
	private Integer[] definitionIds;

	@SearchField(searchField = "name", searchFieldPath = "reconciliationDefinition", comparisonConditions = ComparisonConditions.EQUALS)
	private String definitionNameEquals;

	@SearchField(searchField = "name", searchFieldPath = "reconciliationDefinition")
	private String definitionName;

	@SearchField(searchField = "reconciliationSource.id")
	private Short sourceId;

	@SearchField(searchField = "reconciliationSource.id")
	private Short[] sourceIds;

	@SearchField(searchField = "name", searchFieldPath = "reconciliationSource", comparisonConditions = ComparisonConditions.EQUALS)
	private String sourceNameEquals;

	@SearchField(searchField = "name", searchFieldPath = "reconciliationSource")
	private String sourceName;

	@SearchField(searchField = "reconciliationInvestmentAccount.id")
	private Integer accountId;

	@SearchField(searchField = "accountNumber", searchFieldPath = "reconciliationInvestmentAccount", comparisonConditions = ComparisonConditions.EQUALS)
	private String accountNumberEquals;

	@SearchField(searchField = "accountNumber", searchFieldPath = "reconciliationInvestmentAccount")
	private String accountNumber;

	@SearchField(searchField = "reconciliationInvestmentSecurity.id")
	private Integer securityId;

	@SearchField(searchField = "securitySymbol", searchFieldPath = "reconciliationInvestmentSecurity", comparisonConditions = ComparisonConditions.EQUALS)
	private String securitySymbolEquals;

	@SearchField(searchField = "securitySymbol", searchFieldPath = "reconciliationInvestmentSecurity")
	private String securitySymbol;

	@SearchField(searchField = "approvalUser.id")
	private Short approvalUserId;

	@SearchField(searchField = "approvalUser.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean approvalUserIdIsNull;

	@SearchField(searchFieldPath = "approvalUser", searchField = "displayName")
	private String approvalUserName;

	@SearchField
	private Date reviewDate;

	@SearchField
	private Boolean include;

	@SearchField
	private String invalidSecurityIdentifier;

	@SearchField
	private String invalidAccountNumber;

	@SearchField
	private Date approvalDate;

	@SearchField
	private String comment;

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public Integer getDefinitionIdOrNull() {
		return this.definitionIdOrNull;
	}


	public void setDefinitionIdOrNull(Integer definitionIdOrNull) {
		this.definitionIdOrNull = definitionIdOrNull;
	}


	public Integer[] getDefinitionIds() {
		return this.definitionIds;
	}


	public void setDefinitionIds(Integer[] definitionIds) {
		this.definitionIds = definitionIds;
	}


	public String getDefinitionNameEquals() {
		return this.definitionNameEquals;
	}


	public void setDefinitionNameEquals(String definitionNameEquals) {
		this.definitionNameEquals = definitionNameEquals;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public Short getSourceId() {
		return this.sourceId;
	}


	public void setSourceId(Short sourceId) {
		this.sourceId = sourceId;
	}


	public Short[] getSourceIds() {
		return this.sourceIds;
	}


	public void setSourceIds(Short[] sourceIds) {
		this.sourceIds = sourceIds;
	}


	public String getSourceNameEquals() {
		return this.sourceNameEquals;
	}


	public void setSourceNameEquals(String sourceNameEquals) {
		this.sourceNameEquals = sourceNameEquals;
	}


	public String getSourceName() {
		return this.sourceName;
	}


	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}


	public Integer getAccountId() {
		return this.accountId;
	}


	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}


	public String getAccountNumberEquals() {
		return this.accountNumberEquals;
	}


	public void setAccountNumberEquals(String accountNumberEquals) {
		this.accountNumberEquals = accountNumberEquals;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public String getSecuritySymbolEquals() {
		return this.securitySymbolEquals;
	}


	public void setSecuritySymbolEquals(String securitySymbolEquals) {
		this.securitySymbolEquals = securitySymbolEquals;
	}


	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	public Short getApprovalUserId() {
		return this.approvalUserId;
	}


	public void setApprovalUserId(Short approvalUserId) {
		this.approvalUserId = approvalUserId;
	}


	public Boolean getApprovalUserIdIsNull() {
		return this.approvalUserIdIsNull;
	}


	public void setApprovalUserIdIsNull(Boolean approvalUserIdIsNull) {
		this.approvalUserIdIsNull = approvalUserIdIsNull;
	}


	public String getApprovalUserName() {
		return this.approvalUserName;
	}


	public void setApprovalUserName(String approvalUserName) {
		this.approvalUserName = approvalUserName;
	}


	public Date getReviewDate() {
		return this.reviewDate;
	}


	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}


	public Boolean getInclude() {
		return this.include;
	}


	public void setInclude(Boolean include) {
		this.include = include;
	}


	public String getInvalidSecurityIdentifier() {
		return this.invalidSecurityIdentifier;
	}


	public void setInvalidSecurityIdentifier(String invalidSecurityIdentifier) {
		this.invalidSecurityIdentifier = invalidSecurityIdentifier;
	}


	public String getInvalidAccountNumber() {
		return this.invalidAccountNumber;
	}


	public void setInvalidAccountNumber(String invalidAccountNumber) {
		this.invalidAccountNumber = invalidAccountNumber;
	}


	public Date getApprovalDate() {
		return this.approvalDate;
	}


	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}


	public String getComment() {
		return this.comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}
}
