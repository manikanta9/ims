package com.clifton.reconciliation.rule;

public enum ReconciliationRuleTypes {
	CONDITION,
	FUNCTION
}
