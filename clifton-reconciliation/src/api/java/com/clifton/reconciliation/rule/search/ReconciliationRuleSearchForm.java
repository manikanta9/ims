package com.clifton.reconciliation.rule.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;
import com.clifton.reconciliation.rule.ReconciliationRuleTypes;


/**
 * @author StevenF
 */
public class ReconciliationRuleSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private ReconciliationRuleTypes ruleType;

	@SearchField(searchField = "ruleGroup.id")
	private Short ruleGroupId;

	@SearchField(searchFieldPath = "ruleGroup", searchField = "name")
	private String ruleGroupName;

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public ReconciliationRuleTypes getRuleType() {
		return this.ruleType;
	}


	public void setRuleType(ReconciliationRuleTypes ruleType) {
		this.ruleType = ruleType;
	}


	public Short getRuleGroupId() {
		return this.ruleGroupId;
	}


	public void setRuleGroupId(Short ruleGroupId) {
		this.ruleGroupId = ruleGroupId;
	}


	public String getRuleGroupName() {
		return this.ruleGroupName;
	}


	public void setRuleGroupName(String ruleGroupName) {
		this.ruleGroupName = ruleGroupName;
	}
}
