package com.clifton.reconciliation.rule;

import com.clifton.reconciliation.rule.search.ReconciliationRuleGroupSearchForm;
import com.clifton.reconciliation.rule.search.ReconciliationRuleSearchForm;

import java.util.List;


/**
 * The <code>ReconciliationRuleService</code> interface defines methods for managing conditions.
 *
 * @author StevenF on 11/23/2016.
 */
public interface ReconciliationRuleService {

	////////////////////////////////////////////////////////////////////////////
	////////    Reconciliation Rule Group Service Business Methods     /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationRuleGroup getReconciliationRuleGroup(short id);


	public ReconciliationRuleGroup getReconciliationRuleGroupByName(String name);


	public List<ReconciliationRuleGroup> getReconciliationRuleGroupList(ReconciliationRuleGroupSearchForm searchForm);


	public ReconciliationRuleGroup saveReconciliationRuleGroup(ReconciliationRuleGroup ruleGroup);


	public void deleteReconciliationRuleGroup(short id);

	////////////////////////////////////////////////////////////////////////////
	////////       Reconciliation Rule Service Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationRule<?> getReconciliationRule(int id);


	public List<ReconciliationRule<?>> getReconciliationRuleList(ReconciliationRuleSearchForm searchForm);


	public List<ReconciliationRule<?>> getReconciliationRuleListByGroupId(short groupId);


	public ReconciliationRule<?> saveReconciliationRule(ReconciliationRule<?> reconciliationRule);


	public void deleteReconciliationRule(int id);
}
