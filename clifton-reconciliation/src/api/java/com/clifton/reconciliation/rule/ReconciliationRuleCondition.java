package com.clifton.reconciliation.rule;


import com.clifton.core.json.condition.JsonCondition;


/**
 * The <code>ReconciliationRuleCondition</code> is an implementation of a JsonExpression
 *
 * @author StevenF
 */
public class ReconciliationRuleCondition extends ReconciliationRule<JsonCondition> {

}
