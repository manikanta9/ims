package com.clifton.reconciliation.rule;


import com.clifton.core.beans.NamedEntityWithoutLabel;

import java.util.List;


/**
 * The <code>ReconciliationRuleGroup</code> is used to allow for grouping rules that are defined for the same source data.
 * It allows for simpler filtering in options lists for applicable rules, and ensures that all definitions using a rule
 * act on the same field names so that changes to a rule on one definition will not inherently break on another definition
 * that does not contain that field name
 *
 * @author StevenF
 */
public class ReconciliationRuleGroup extends NamedEntityWithoutLabel<Short> {

	/**
	 * We purposely don't annotate this with the '@OneToManyEntity' annotation because we don't
	 * want to export this in migration actions; the generic signature causes casting exceptions
	 * and the rules themselves reference the group and so will still be migrated properly
	 */
	private List<ReconciliationRule<?>> reconciliationRuleList;

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public List<ReconciliationRule<?>> getReconciliationRuleList() {
		return this.reconciliationRuleList;
	}


	public void setReconciliationRuleList(List<ReconciliationRule<?>> reconciliationRuleList) {
		this.reconciliationRuleList = reconciliationRuleList;
	}
}
