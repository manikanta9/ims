package com.clifton.reconciliation.rule;


import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.hibernate.GenericCompressedTextUserType;
import com.clifton.core.json.JsonExpression;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Objects;


/**
 * The <code>ReconciliationRule</code> is an implementation of the <code>JsonExpression</code> interface.
 * It utilizes discriminators to provide three concrete implementations:
 * <p>
 * JsonCondition:
 * Implements numerous conditional operators such as greater than, equals, less than, contains, etc.
 * Allows chaining of conditions through AND/OR sequences, and will always return a boolean result.
 * see <code>JsonConditionOperatorTypes</code>
 * <p>
 * JsonFunction:
 * Functions support manipulation of data, such as 'trim' and 'substring', 'negate', and 'add'
 * Allows chaining of functions through CONCATENATE/EVALUATE sequences and will return an object of the same
 * type evaluated; conversion to other types would be handled outside the function evaluation at this time.
 * see <code>JsonFunctionOperatorTypes</code>
 * <p>
 * JsonComparator:
 * Comparators are essentially functions used to compare two objects, as opposed to manipulation of an object.
 * A typical use case would be to apply a threshold range against two objects (e.g. are they within .05 of each other.)
 * see <code>JsonComparatorOperatorType</code>
 *
 * @author StevenF
 */
public class ReconciliationRule<J extends JsonExpression<?, ?, ?, ?, ?>> extends NamedEntityWithoutLabel<Integer> {

	private ReconciliationRuleGroup ruleGroup;

	private ReconciliationRuleTypes ruleType;

	@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
	private J jsonExpression;

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		try {
			return GenericCompressedTextUserType.MAPPER.writeValueAsString(this).equals(GenericCompressedTextUserType.MAPPER.writeValueAsString(o));
		}
		catch (Exception e) {
			return false;
		}
	}


	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), this.ruleGroup, this.ruleType, this.jsonExpression);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationRuleGroup getRuleGroup() {
		return this.ruleGroup;
	}


	public void setRuleGroup(ReconciliationRuleGroup ruleGroup) {
		this.ruleGroup = ruleGroup;
	}


	public ReconciliationRuleTypes getRuleType() {
		return this.ruleType;
	}


	public void setRuleType(ReconciliationRuleTypes ruleType) {
		this.ruleType = ruleType;
	}


	public J getJsonExpression() {
		return this.jsonExpression;
	}


	public void setJsonExpression(J jsonExpression) {
		this.jsonExpression = jsonExpression;
	}
}
