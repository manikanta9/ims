package com.clifton.reconciliation.json;

import com.clifton.core.converter.reversable.ReversableConverter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.run.data.DataNote;

import java.util.Collection;
import java.util.List;


/**
 * Used to format the json notes string when exporting reconciliation rows.
 *
 * @author KellyJ
 */
public class ReconciliationSourceConverter implements ReversableConverter<String, Object> {

	@Override
	@SuppressWarnings("unchecked")
	public String reverseConvert(Object to) {
		if (to instanceof Collection) {
			StringBuilder sourceNameBuilder = new StringBuilder();
			for (ReconciliationSource source : CollectionUtils.getIterable((List<ReconciliationSource>) to)) {
				if (sourceNameBuilder.length() > 0) {
					sourceNameBuilder.append("/");
				}
				sourceNameBuilder.append(source.getName());
			}
			return sourceNameBuilder.toString();
		}
		return ((ReconciliationSource) to).getName();
	}


	@Override
	public Object convert(String from) {
		return from == null ? null : DataNote.ofNote(from);
	}
}
