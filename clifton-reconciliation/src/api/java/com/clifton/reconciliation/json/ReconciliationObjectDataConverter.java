package com.clifton.reconciliation.json;

import com.clifton.core.converter.reversable.ReversableConverter;
import com.clifton.core.util.CollectionUtils;

import java.util.Collection;
import java.util.List;


/**
 * @author StevenF
 */
public class ReconciliationObjectDataConverter implements ReversableConverter<Object, Object> {

	@Override
	public Object reverseConvert(Object to) {
		if (to instanceof Collection) {
			return CollectionUtils.getFirstElement((List<?>) to);
		}
		return to;
	}


	@Override
	public Object convert(Object from) {
		return null;
	}
}
