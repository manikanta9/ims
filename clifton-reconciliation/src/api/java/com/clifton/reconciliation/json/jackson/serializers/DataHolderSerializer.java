package com.clifton.reconciliation.json.jackson.serializers;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.run.data.DataHolder;
import com.clifton.reconciliation.run.data.DataObject;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


public class DataHolderSerializer extends StdSerializer<DataHolder> {

	public DataHolderSerializer() {
		this(null);
	}


	public DataHolderSerializer(Class<DataHolder> clazz) {
		super(clazz);
	}


	@Override
	public void serialize(DataHolder dataHolder, JsonGenerator generator, SerializerProvider provider)
			throws IOException, JsonProcessingException {
		generator.writeStartObject();
		if (!CollectionUtils.isEmpty(dataHolder.getDataObjects())) {
			generator.writeArrayFieldStart("dataObjects");
			for (DataObject dataObject : dataHolder.getDataObjects()) {
				generator.writeStartObject();
				if (dataObject.getImportDate() != null) {
					generator.writeStringField("importDate", DateUtils.fromDate(dataObject.getImportDate(), DateUtils.DATE_FORMAT_SQL_PRECISE));
				}
				if (dataObject.getFileDefinitionId() != null) {
					generator.writeNumberField("fileDefinitionId", dataObject.getFileDefinitionId());
				}
				generator.writeNumberField("sourceId", dataObject.getSourceId());
				if (!CollectionUtils.isEmpty(dataObject.getData())) {
					generator.writeFieldName("data");
					generator.writeObject(dataObject.getData());
				}
				if (!CollectionUtils.isEmpty(dataObject.getRowData())) {
					generator.writeArrayFieldStart("rowData");
					Set<String> keys = dataObject.getRowData().get(0).keySet();
					generator.writeObject(keys);
					for (Map<String, Object> rowData : dataObject.getRowData()) {
						Map<String, Object> linkedHashMap = new LinkedHashMap<>();
						for (String key : keys) {
							linkedHashMap.put(key, rowData.get(key));
						}
						generator.writeObject(linkedHashMap.values());
					}
					generator.writeEndArray();
				}
				generator.writeEndObject();
			}
			generator.writeEndArray();
		}
		if (!CollectionUtils.isEmpty(dataHolder.getNotes())) {
			generator.writeFieldName("notes");
			generator.writeObject(dataHolder.getNotes());
		}
		if (!CollectionUtils.isEmpty(dataHolder.getErrors())) {
			generator.writeFieldName("errors");
			generator.writeObject(dataHolder.getErrors());
		}
		generator.writeEndObject();
	}
}
