package com.clifton.reconciliation.json.jackson.serializers;

import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.data.DataHolder;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatus;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;


/**
 * @author jonathanr
 */
public class ReconciliationRunObjectDeserializer extends StdDeserializer<ReconciliationRunObject> {


	public ReconciliationRunObjectDeserializer() {
		this(null);
	}


	public ReconciliationRunObjectDeserializer(Class<?> vc) {
		super(vc);
	}


	@Override
	public ReconciliationRunObject deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
		JsonNode rootNode = jp.getCodec().readTree(jp);
		ReconciliationRunObject runObject = new ReconciliationRunObject();
		Long id = rootNode.get("id").asLong();
		runObject.setId(id);
		JsonNode importDateNode = rootNode.get("importDate");
		if (importDateNode.get("importDate") != null) {
			runObject.setImportDate(DateUtils.toDate(importDateNode.asText(), DateUtils.DATE_FORMAT_SQL_PRECISE));
		}
		JsonNode breakDaysNode = rootNode.get("breakDays");
		if (breakDaysNode != null) {
			runObject.setBreakDays(breakDaysNode.asInt());
		}
		JsonNode identifierNode = rootNode.get("identifier");
		if (identifierNode != null) {
			runObject.setIdentifier(identifierNode.textValue());
		}
		JsonNode runIDNode = rootNode.get("reconciliationRun");
		if (runIDNode != null) {
			ReconciliationRun run = new ReconciliationRun();
			run.setId(runIDNode.intValue());
			runObject.setReconciliationRun(run);
		}
		JsonNode statusIdNode = rootNode.get("reconciliationMatchStatus");
		if (statusIdNode != null) {
			ReconciliationMatchStatus matchStatus = new ReconciliationMatchStatus();
			matchStatus.setId(statusIdNode.shortValue());
			runObject.setReconciliationMatchStatus(matchStatus);
		}
		JsonNode reconcileStatusIdNode = rootNode.get("reconciliationReconcileStatus");
		if (reconcileStatusIdNode != null) {
			ReconciliationReconcileStatus reconcileStatus = new ReconciliationReconcileStatus();
			reconcileStatus.setId(reconcileStatusIdNode.shortValue());
			runObject.setReconciliationReconcileStatus(reconcileStatus);
		}
		JsonNode investmentAccountIdNode = rootNode.get("reconciliationInvestmentAccount");
		if (investmentAccountIdNode != null) {
			ReconciliationInvestmentAccount investmentAccount = new ReconciliationInvestmentAccount();
			investmentAccount.setId(investmentAccountIdNode.intValue());
			runObject.setReconciliationInvestmentAccount(investmentAccount);
		}
		JsonNode investmentSecurityIdNode = rootNode.get("reconciliationInvestmentSecurity");
		if (investmentSecurityIdNode != null) {
			ReconciliationInvestmentSecurity reconciliationInvestmentSecurity = new ReconciliationInvestmentSecurity();
			reconciliationInvestmentSecurity.setId(investmentSecurityIdNode.intValue());
			runObject.setReconciliationInvestmentSecurity(reconciliationInvestmentSecurity);
		}
		JsonNode reconciliationSourceIdNode = rootNode.get("reconciledOnSource");
		if (reconciliationSourceIdNode != null) {
			ReconciliationSource reconciliationSource = new ReconciliationSource();
			reconciliationSource.setId(reconciliationSourceIdNode.shortValue());
			runObject.setReconciledOnSource(reconciliationSource);
		}

		JsonNode dataHolderNode = rootNode.get("dataHolder");
		if (dataHolderNode != null) {
			DataHolderDeserializer dataHolderDeserializer = new DataHolderDeserializer();
			DataHolder dataHolder = dataHolderDeserializer.deserialize(dataHolderNode.traverse(jp.getCodec()), ctxt);
			runObject.setDataHolder(dataHolder);
		}
		return runObject;
	}
}
