package com.clifton.reconciliation.json;

import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverter;
import com.clifton.core.dataaccess.datatable.bean.BeanDataRow;
import com.clifton.core.dataaccess.datatable.bean.BeanDataTable;
import com.clifton.core.dataaccess.db.ClassToSqlTypeConverter;
import com.clifton.core.dataaccess.db.PropertyDataTypeConverter;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.reconciliation.run.ReconciliationRunObjectDetail;
import com.clifton.reconciliation.run.data.DataNote;

import java.beans.PropertyDescriptor;
import java.sql.Types;
import java.util.Collection;
import java.util.List;


/**
 * The <code>ReconciliationRunObjectDetailDataTableConverter</code> is a custom converter
 * used to make {@link ReconciliationRunObjectDetail} objects export to Excel and CSV properly.
 * It has some custom handling to set the proper dataTypes for columns in addition to calling
 * the explode method on the detail object so that an object with multiple rows of data can
 * be displayed as multiple rows in excel.
 *
 * @author stevenf
 */
public class ReconciliationRunObjectDetailDataTableConverter extends BeanCollectionToDataTableConverter<ReconciliationRunObjectDetail> {


	@Override
	public void validateConverterConfiguration() {
		// no custom validation for this converter
	}


	@Override
	public BeanDataTable<ReconciliationRunObjectDetail> convert(Collection<ReconciliationRunObjectDetail> runObjectDetailCollection) {
		BeanDataTable<ReconciliationRunObjectDetail> table = new BeanDataTable<>(getDataColumnArrayForBeanCollection(runObjectDetailCollection));
		for (int j = 0; j < CollectionUtils.getSize(runObjectDetailCollection); j++) {
			ReconciliationRunObjectDetail runObjectDetail = CollectionUtils.getObjectAtIndex(runObjectDetailCollection, j);
			if (runObjectDetail != null) {
				for (ReconciliationRunObjectDetail detail : CollectionUtils.getIterable(runObjectDetail.explode())) {
					DataRow row = new BeanDataRow<>(table, detail);
					table.addRow(row);
				}
			}
		}
		return table;
	}


	@Override
	protected DataTypes getDataType(String beanPropertyName) {
		PropertyDataTypeConverter dataTypeConverter = new PropertyDataTypeConverter();
		PropertyDescriptor pd = getPropertyDescriptor(beanPropertyName);
		if (pd != null) {
			Class<?> propertyType = pd.getPropertyType();
			if (!(List.class.isAssignableFrom(propertyType) || DataNote.class.isAssignableFrom(propertyType))) {
				return dataTypeConverter.convert(pd);
			}
		}
		return null;
	}


	@Override
	protected Integer getSqlType(String beanPropertyName) {
		ClassToSqlTypeConverter sqlTypeConverter = new ClassToSqlTypeConverter();
		PropertyDescriptor pd = getPropertyDescriptor(beanPropertyName);
		if (pd != null) {
			Class<?> propertyType = pd.getPropertyType();
			if (!(List.class.isAssignableFrom(propertyType) || DataNote.class.isAssignableFrom(propertyType))) {
				if (Enum.class.isAssignableFrom(pd.getPropertyType())) {
					return Types.VARCHAR;
				}
				else {
					return sqlTypeConverter.convert(pd.getPropertyType());
				}
			}
		}
		return null;
	}
}
