package com.clifton.reconciliation.json.jackson.serializers;

import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;


/**
 * @author jonathanr
 */
public class ReconciliationRunObjectSerializer extends StdSerializer<ReconciliationRunObject> {

	public ReconciliationRunObjectSerializer() {
		this(null);
	}


	public ReconciliationRunObjectSerializer(Class<ReconciliationRunObject> clazz) {
		super(clazz);
	}


	@Override
	public void serialize(ReconciliationRunObject value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeStartObject();
		gen.writeNumberField("id", value.getId());
		gen.writeObjectField("importDate", value.getImportDate());
		gen.writeNumberField("breakDays", value.getBreakDays());
		gen.writeStringField("identifier", value.getIdentifier());
		if (value.getReconciliationRun() != null) {
			gen.writeNumberField("reconciliationRun", value.getReconciliationRun().getId());
		}
		if (value.getReconciliationMatchStatus() != null) {
			gen.writeNumberField("reconciliationMatchStatus", value.getReconciliationMatchStatus().getId());
		}
		if (value.getReconciliationReconcileStatus() != null) {
			gen.writeNumberField("reconciliationReconcileStatus", value.getReconciliationReconcileStatus().getId());
		}
		if (value.getReconciliationInvestmentAccount() != null) {
			gen.writeNumberField("reconciliationInvestmentAccount", value.getReconciliationInvestmentAccount().getId());
		}
		if (value.getReconciliationInvestmentSecurity() != null) {
			gen.writeNumberField("reconciliationInvestmentSecurity", value.getReconciliationInvestmentSecurity().getId());
		}
		if (value.getReconciledOnSource() != null) {
			gen.writeNumberField("reconciledOnSource", value.getReconciledOnSource().getId());
		}
		if (value.getDataHolder() != null) {
			gen.writeObjectField("dataHolder", value.getDataHolder());
		}
		gen.writeEndObject();
	}
}
