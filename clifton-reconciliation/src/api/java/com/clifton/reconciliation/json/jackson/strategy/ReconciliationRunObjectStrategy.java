package com.clifton.reconciliation.json.jackson.strategy;

import com.clifton.core.converter.json.jackson.serializers.JacksonBigDecimalDeserializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonBigDecimalSerializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonDateDeserializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonDateSerializer;
import com.clifton.core.converter.json.jackson.strategy.JacksonHibernateStrategy;
import com.clifton.reconciliation.json.jackson.serializers.DataHolderDeserializer;
import com.clifton.reconciliation.json.jackson.serializers.DataHolderSerializer;
import com.clifton.reconciliation.json.jackson.serializers.ReconciliationRunObjectDeserializer;
import com.clifton.reconciliation.json.jackson.serializers.ReconciliationRunObjectSerializer;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.data.DataHolder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.math.BigDecimal;
import java.util.Date;


public class ReconciliationRunObjectStrategy extends JacksonHibernateStrategy {

	@Override
	public boolean isDisableAnnotations() {
		return true;
	}


	@Override
	public void registerModules(ObjectMapper objectMapper) {
		SimpleModule module = new SimpleModule();
		module.addSerializer(BigDecimal.class, new JacksonBigDecimalSerializer());
		module.addSerializer(Date.class, new JacksonDateSerializer());
		module.addSerializer(DataHolder.class, new DataHolderSerializer());
		module.addSerializer(ReconciliationRunObject.class, new ReconciliationRunObjectSerializer());
		module.addDeserializer(BigDecimal.class, new JacksonBigDecimalDeserializer());
		module.addDeserializer(Date.class, new JacksonDateDeserializer());
		module.addDeserializer(DataHolder.class, new DataHolderDeserializer());
		module.addDeserializer(ReconciliationRunObject.class, new ReconciliationRunObjectDeserializer());
		objectMapper.registerModule(module);
	}
}
