package com.clifton.reconciliation.json;

import com.clifton.core.converter.reversable.ReversableConverter;
import com.clifton.reconciliation.run.data.DataNote;
import com.clifton.reconciliation.run.detail.ReconciliationRunDetailUtils;

import java.util.List;


/**
 * Used to format the json notes string when exporting reconciliation rows.
 *
 * @author KellyJ
 */
public class ReconciliationNotesConverter implements ReversableConverter<String, Object> {

	@Override
	@SuppressWarnings("unchecked")
	public String reverseConvert(Object to) {
		if (to instanceof List) {
			return ReconciliationRunDetailUtils.getLatestManualNote((List<DataNote>) to);
		}
		return null;
	}


	@Override
	public Object convert(String from) {
		return from == null ? null : DataNote.ofNote(from);
	}
}
