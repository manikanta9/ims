package com.clifton.reconciliation.json.jackson.serializers;

import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.run.data.DataError;
import com.clifton.reconciliation.run.data.DataHolder;
import com.clifton.reconciliation.run.data.DataNote;
import com.clifton.reconciliation.run.data.DataObject;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DataHolderDeserializer extends StdDeserializer<DataHolder> {

	public DataHolderDeserializer() {
		this(null);
	}


	public DataHolderDeserializer(Class<?> vc) {
		super(vc);
	}


	@Override
	public DataHolder deserialize(JsonParser parser, DeserializationContext context) throws IOException {
		ObjectMapper mapper = (ObjectMapper) parser.getCodec();
		DataHolder dataHolder = new DataHolder();
		JsonNode node = parser.getCodec().readTree(parser);
		JsonNode notes = node.get("notes");
		if (notes != null) {
			dataHolder.setNotes(mapper.readValue(notes.toString(), new TypeReference<List<DataNote>>() {
			}));
		}
		JsonNode errors = node.get("errors");
		if (errors != null) {
			dataHolder.setErrors(mapper.readValue(errors.toString(), new TypeReference<List<DataError>>() {
			}));
		}
		ArrayNode dataObjectArrayNode = (ArrayNode) node.get("dataObjects");
		if (dataObjectArrayNode != null) {
			for (JsonNode dataObjectNode : dataObjectArrayNode) {
				DataObject dataObject = new DataObject();
				if (dataObjectNode.get("importDate") != null) {
					dataObject.setImportDate(DateUtils.toDate(dataObjectNode.get("importDate").asText(), DateUtils.DATE_FORMAT_SQL_PRECISE));
				}
				if (dataObjectNode.get("fileDefinitionId") != null) {
					dataObject.setFileDefinitionId(dataObjectNode.get("fileDefinitionId").intValue());
				}
				dataObject.setSourceId(dataObjectNode.get("sourceId").shortValue());

				JsonNode data = dataObjectNode.get("data");
				if (data != null) {
					dataObject.setData(mapper.readValue(data.toString(), new TypeReference<Map<String, Object>>() {
					}));
				}

				List<Map<String, Object>> rowData = new ArrayList<>();
				ArrayNode rowDataArrayNode = (ArrayNode) dataObjectNode.get("rowData");
				List<String> keyList = mapper.readValue(rowDataArrayNode.get(0).toString(), new TypeReference<List<String>>() {
				});
				for (int i = 1; i < rowDataArrayNode.size(); i++) {
					List<Object> valueList = mapper.readValue(rowDataArrayNode.get(i).toString(), new TypeReference<List<Object>>() {
					});
					Map<String, Object> rowDataMap = new HashMap<>();
					for (int j = 0; j < keyList.size(); j++) {
						rowDataMap.put(keyList.get(j), valueList.get(j));
					}
					rowData.add(rowDataMap);
				}
				dataObject.setRowData(rowData);
				dataHolder.getDataObjects().add(dataObject);
			}
		}
		return dataHolder;
	}
}
