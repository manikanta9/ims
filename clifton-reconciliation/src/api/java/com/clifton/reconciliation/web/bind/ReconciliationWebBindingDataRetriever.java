package com.clifton.reconciliation.web.bind;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.web.bind.WebBindingDataRetriever;
import org.springframework.web.context.request.WebRequest;

import java.io.Serializable;
import java.util.List;
import java.util.regex.Pattern;


/**
 * @author vgomelsky
 */
public class ReconciliationWebBindingDataRetriever implements WebBindingDataRetriever {

	private ReconciliationWebBindingDataRetrieverService reconciliationWebBindingDataRetrieverService;

	private int order;
	private Pattern requestUriPattern;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getOrder() {
		return this.order;
	}


	@Override
	public boolean isApplicableForRequest(WebRequest request) {
		if (this.requestUriPattern == null) {
			return true;
		}
		String uri = request.getDescription(false);
		return this.requestUriPattern.matcher(uri).matches();
	}


	@Override
	public <T extends IdentityObject> T getEntity(Class<T> entityType, Serializable entityId) {
		return getReconciliationWebBindingDataRetrieverService().getReconciliationWebBindingObject(entityType.getName(), (Number) entityId);
	}


	@Override
	public List<Column> getEntityColumnList(Class<? extends IdentityObject> entityType) {
		return getReconciliationWebBindingDataRetrieverService().getReconciliationWebBindingObjectColumnList(entityType.getName());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationWebBindingDataRetrieverService getReconciliationWebBindingDataRetrieverService() {
		return this.reconciliationWebBindingDataRetrieverService;
	}


	public void setReconciliationWebBindingDataRetrieverService(ReconciliationWebBindingDataRetrieverService reconciliationWebBindingDataRetrieverService) {
		this.reconciliationWebBindingDataRetrieverService = reconciliationWebBindingDataRetrieverService;
	}


	public void setOrder(int order) {
		this.order = order;
	}


	public Pattern getRequestUriPattern() {
		return this.requestUriPattern;
	}


	public void setRequestUriPattern(Pattern requestUriPattern) {
		this.requestUriPattern = requestUriPattern;
	}
}
