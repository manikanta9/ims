package com.clifton.reconciliation.event.jms.consumer;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProcessor;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.event.EventNotProcessedException;
import com.clifton.reconciliation.event.jms.ReconciliationEvent;
import com.clifton.reconciliation.event.jms.message.ReconciliationEventRequestMessage;
import com.clifton.reconciliation.event.object.ReconciliationEventObject;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectService;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectStatuses;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectData;
import com.clifton.security.impersonation.SecurityImpersonationHandler;
import com.clifton.security.user.SecurityUser;


/**
 * @author stevenf
 */
public class ReconciliationEventJmsMessageConsumer implements AsynchronousProcessor<AsynchronousMessage> {

	private String defaultRunAsUserName = SecurityUser.SYSTEM_USER;

	private EventHandler eventHandler;
	private SecurityImpersonationHandler securityImpersonationHandler;
	private ReconciliationEventObjectService reconciliationEventObjectService;


	@Override
	public void process(AsynchronousMessage asynchronousMessage, AsynchronousMessageHandler handler) {
		if (asynchronousMessage instanceof ReconciliationEventRequestMessage) {
			ReconciliationEventRequestMessage<?> message = (ReconciliationEventRequestMessage<?>) asynchronousMessage;
			ReconciliationEventObjectData eventObjectData = message.getEventObjectData();
			getSecurityImpersonationHandler().runAsSecurityUserName(ObjectUtils.coalesce(message.getUserName(), getDefaultRunAsUserName()), () -> {
				try {
					if (getEventHandler().raiseEvent(ReconciliationEvent.ofEventTarget(message.getEventName(), message.getEventTypeName(), eventObjectData))) {
						ReconciliationEventObject<?> eventObject = getReconciliationEventObjectService().getReconciliationEventObject(message.getEventObjectId());
						eventObject.setStatus(ReconciliationEventObjectStatuses.COMPLETE);
						getReconciliationEventObjectService().saveReconciliationEventObject(eventObject);
					}
				}
				catch (Exception e) {
					String eventMessage;
					ReconciliationEventObjectStatuses eventStatus;

					if (e.getCause() instanceof EventNotProcessedException) {
						eventMessage = "Event Not Processed: " + e.getCause().getMessage();
						eventStatus = ReconciliationEventObjectStatuses.NOT_PROCESSED;
						LogUtils.warn(getClass(), eventMessage, e);
					}
					else {
						eventMessage = e.getCause().getMessage();
						eventStatus = ReconciliationEventObjectStatuses.ERROR;
						LogUtils.error(getClass(), e.getMessage(), e);
					}

					ReconciliationEventObject<?> eventObject = getReconciliationEventObjectService().getReconciliationEventObject(message.getEventObjectId());
					eventObject.setStatus(eventStatus);
					eventObject.setStatusMessage(eventMessage);
					getReconciliationEventObjectService().saveReconciliationEventObject(eventObject);
				}
			});
		}
		else {
			throw new RuntimeException("Cannot process message because no type is specified.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationEventObjectService getReconciliationEventObjectService() {
		return this.reconciliationEventObjectService;
	}


	public void setReconciliationEventObjectService(ReconciliationEventObjectService reconciliationEventObjectService) {
		this.reconciliationEventObjectService = reconciliationEventObjectService;
	}


	public String getDefaultRunAsUserName() {
		return this.defaultRunAsUserName;
	}


	public void setDefaultRunAsUserName(String defaultRunAsUserName) {
		this.defaultRunAsUserName = defaultRunAsUserName;
	}


	public SecurityImpersonationHandler getSecurityImpersonationHandler() {
		return this.securityImpersonationHandler;
	}


	public void setSecurityImpersonationHandler(SecurityImpersonationHandler securityImpersonationHandler) {
		this.securityImpersonationHandler = securityImpersonationHandler;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}
}
