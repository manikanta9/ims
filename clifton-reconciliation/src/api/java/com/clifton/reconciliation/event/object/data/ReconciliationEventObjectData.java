package com.clifton.reconciliation.event.object.data;


import java.util.Date;


/**
 * The <code>ReconciliationEventObjectData</code> interface is implemented
 * by classes that will be passed to via JMS to external applications.
 * They are concrete implementations of the event data that define how the data
 * is serialized/deserialized from COMPRESSED_TEXT in the database.
 *
 * @author stevenf
 */
public abstract class ReconciliationEventObjectData {

	Date eventDate;

	private String eventNote;
	private String holdingAccountNumber;
	private String securityIdentifier;

	private String sourceTable;
	private Long sourceFkFieldId;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public String getEventNote() {
		return this.eventNote;
	}


	public void setEventNote(String eventNote) {
		this.eventNote = eventNote;
	}


	public String getHoldingAccountNumber() {
		return this.holdingAccountNumber;
	}


	public void setHoldingAccountNumber(String holdingAccountNumber) {
		this.holdingAccountNumber = holdingAccountNumber;
	}


	public String getSecurityIdentifier() {
		return this.securityIdentifier;
	}


	public void setSecurityIdentifier(String securityIdentifier) {
		this.securityIdentifier = securityIdentifier;
	}


	public String getSourceTable() {
		return this.sourceTable;
	}


	public void setSourceTable(String sourceTable) {
		this.sourceTable = sourceTable;
	}


	public Long getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Long sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}
}
