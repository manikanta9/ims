package com.clifton.reconciliation.event.object.data.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectCategories;

import java.math.BigDecimal;


/**
 * @author stevenf
 */
public class ReconciliationEventObjectDataTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private String type;

	@SearchField
	private Boolean absoluteValue;

	@SearchField
	private Boolean approvalRequired;

	@SearchField
	private String label;

	@SearchField
	private ReconciliationEventObjectCategories eventObjectCategory;

	@SearchField
	private String defaultNote;

	@SearchField
	private Boolean absolute;

	@SearchField
	private BigDecimal approvalAmountThreshold;

	@SearchField
	private String iconClass;

	@SearchField
	private Boolean systemDefined;

	@SearchField
	private Boolean negativeAdjustment;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getType() {
		return this.type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Boolean getAbsoluteValue() {
		return this.absoluteValue;
	}


	public void setAbsoluteValue(Boolean absoluteValue) {
		this.absoluteValue = absoluteValue;
	}


	public Boolean getApprovalRequired() {
		return this.approvalRequired;
	}


	public void setApprovalRequired(Boolean approvalRequired) {
		this.approvalRequired = approvalRequired;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public ReconciliationEventObjectCategories getEventObjectCategory() {
		return this.eventObjectCategory;
	}


	public void setEventObjectCategory(ReconciliationEventObjectCategories eventObjectCategory) {
		this.eventObjectCategory = eventObjectCategory;
	}


	public String getDefaultNote() {
		return this.defaultNote;
	}


	public void setDefaultNote(String defaultNote) {
		this.defaultNote = defaultNote;
	}


	public Boolean getAbsolute() {
		return this.absolute;
	}


	public void setAbsolute(Boolean absolute) {
		this.absolute = absolute;
	}


	public BigDecimal getApprovalAmountThreshold() {
		return this.approvalAmountThreshold;
	}


	public void setApprovalAmountThreshold(BigDecimal approvalAmountThreshold) {
		this.approvalAmountThreshold = approvalAmountThreshold;
	}


	public String getIconClass() {
		return this.iconClass;
	}


	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getNegativeAdjustment() {
		return this.negativeAdjustment;
	}


	public void setNegativeAdjustment(Boolean negativeAdjustment) {
		this.negativeAdjustment = negativeAdjustment;
	}
}
