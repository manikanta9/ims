package com.clifton.reconciliation.event.object;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectDataType;
import com.clifton.reconciliation.event.object.data.search.ReconciliationEventObjectDataTypeSearchForm;
import com.clifton.reconciliation.event.object.search.ReconciliationEventObjectSearchForm;

import java.util.List;


/**
 * @author stevenf
 */
public interface ReconciliationEventObjectService {

	////////////////////////////////////////////////////////////////////////////
	////////       Reconciliation Event Object Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationEventObject<?> getReconciliationEventObject(int id);


	public List<ReconciliationEventObject<?>> getReconciliationEventObjectListByRunId(int runId);


	public List<ReconciliationEventObject<?>> getReconciliationEventObjectListByRunObjectId(long runObjectId);


	public List<ReconciliationEventObject<?>> getReconciliationEventObjectList(ReconciliationEventObjectSearchForm searchForm);


	@SecureMethod(dtoClass = ReconciliationEventObject.class, namingConventionViolation = true)
	public Status saveReconciliationEventObjectList(ReconciliationEventObjectWrapper<?> eventObjectWrapper);


	@SecureMethod(dtoClass = ReconciliationEventObject.class, namingConventionViolation = true)
	public Status processReconciliationEventObjectList(ReconciliationEventObjectWrapper<?> eventObjectWrapper, String globalNote, boolean sendEvent);


	public ReconciliationEventObject<?> saveReconciliationEventObject(ReconciliationEventObject<?> eventObject);


	@DoNotAddRequestMapping
	public ReconciliationEventObject<?> saveReconciliationEventObject(ReconciliationEventObject<?> eventObject, boolean validate);


	@DoNotAddRequestMapping
	public ReconciliationEventObject<?> updateReconciliationEventObject(ReconciliationEventObject<?> eventObject);


	public void deleteReconciliationEventObject(int id);


	@DoNotAddRequestMapping
	public void deleteReconciliationEventObjectList(List<ReconciliationEventObject<?>> eventObjectList);

	////////////////////////////////////////////////////////////////////////////
	////////   Reconciliation Event Object Data Type Business Methods  /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationEventObjectDataType getReconciliationEventObjectDataType(short id);


	public ReconciliationEventObjectDataType getReconciliationEventObjectDataTypeByName(String name);


	public List<ReconciliationEventObjectDataType> getReconciliationEventObjectDataTypeList(ReconciliationEventObjectDataTypeSearchForm searchForm);


	public ReconciliationEventObjectDataType saveReconciliationEventObjectDataType(ReconciliationEventObjectDataType eventObjectDataType);


	public void deleteReconciliationEventObjectDataType(short id);
}
