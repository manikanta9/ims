package com.clifton.reconciliation.event.jms.message;


import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.synchronous.AbstractSynchronousResponseMessage;


@MessageType(MessageTypes.XML)
public class ReconciliationEventResponseMessage extends AbstractSynchronousResponseMessage {

	//
}
