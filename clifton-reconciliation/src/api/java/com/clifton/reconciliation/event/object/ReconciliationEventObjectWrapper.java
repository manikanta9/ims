package com.clifton.reconciliation.event.object;

import com.clifton.core.beans.BaseEntity;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectData;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.List;


/**
 * Generic object used in JMS Messages from Reconciliation to IMS
 * Used to create journal entries of various types in addition to providing
 * functionality to lock and unlock accounts.
 *
 * @author stevenf
 */
public class ReconciliationEventObjectWrapper<D extends ReconciliationEventObjectData> extends BaseEntity<Integer> {

	@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
	private List<ReconciliationEventObject<D>> eventObjectList;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public List<ReconciliationEventObject<D>> getEventObjectList() {
		return this.eventObjectList;
	}


	public void setEventObjectList(List<ReconciliationEventObject<D>> eventObjectList) {
		this.eventObjectList = eventObjectList;
	}
}
