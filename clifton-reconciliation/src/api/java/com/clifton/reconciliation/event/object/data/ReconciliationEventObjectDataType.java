package com.clifton.reconciliation.event.object.data;

import com.clifton.core.beans.NamedEntity;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectCategories;

import java.math.BigDecimal;


/**
 * The <code>ReconciliationEventObjectDataType</code> is used to drive how events are created;
 * The 'name' attribute is used as the 'eventName' when sending JMS messages to target applications.
 * Target applications then process the event based on that name.
 *
 * @author stevenf
 */
public class ReconciliationEventObjectDataType extends NamedEntity<Short> {

	private ReconciliationEventObjectCategories eventObjectCategory;

	private String iconClass;
	private String defaultNote;

	/**
	 * ReconciliationEventObjectData#amount is stored and sent to IMS as a positive amount.
	 */
	private boolean absolute;
	private boolean approvalRequired;
	private boolean systemDefined;

	/**
	 * ReconciliationEventObjectData#amount should be negated when calculating the adjustment calculated amount.
	 */
	private boolean negativeAdjustment;

	private BigDecimal approvalAmountThreshold;

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationEventObjectCategories getEventObjectCategory() {
		return this.eventObjectCategory;
	}


	public void setEventObjectCategory(ReconciliationEventObjectCategories eventObjectCategory) {
		this.eventObjectCategory = eventObjectCategory;
	}


	public String getIconClass() {
		return this.iconClass;
	}


	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}


	public String getDefaultNote() {
		return this.defaultNote;
	}


	public void setDefaultNote(String defaultNote) {
		this.defaultNote = defaultNote;
	}


	public boolean isAbsolute() {
		return this.absolute;
	}


	public void setAbsolute(boolean absolute) {
		this.absolute = absolute;
	}


	public boolean isApprovalRequired() {
		return this.approvalRequired;
	}


	public void setApprovalRequired(boolean approvalRequired) {
		this.approvalRequired = approvalRequired;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isNegativeAdjustment() {
		return this.negativeAdjustment;
	}


	public void setNegativeAdjustment(boolean negativeAdjustment) {
		this.negativeAdjustment = negativeAdjustment;
	}


	public BigDecimal getApprovalAmountThreshold() {
		return this.approvalAmountThreshold;
	}


	public void setApprovalAmountThreshold(BigDecimal approvalAmountThreshold) {
		this.approvalAmountThreshold = approvalAmountThreshold;
	}
}
