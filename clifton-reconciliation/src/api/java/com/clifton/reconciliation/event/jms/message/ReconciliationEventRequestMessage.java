package com.clifton.reconciliation.event.jms.message;

import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.asynchronous.AbstractAsynchronousMessage;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectData;


@MessageType(MessageTypes.XML)
public class ReconciliationEventRequestMessage<D extends ReconciliationEventObjectData> extends AbstractAsynchronousMessage {

	//TODO - this needs to be secured in some way, used to allow the receiving application to process the event as this user.
	private String userName;

	private String eventName;
	private String eventTypeName;

	private Integer eventObjectId;
	private D eventObjectData;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getUserName() {
		return this.userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getEventName() {
		return this.eventName;
	}


	public void setEventName(String eventName) {
		this.eventName = eventName;
	}


	public String getEventTypeName() {
		return this.eventTypeName;
	}


	public void setEventTypeName(String eventTypeName) {
		this.eventTypeName = eventTypeName;
	}


	public Integer getEventObjectId() {
		return this.eventObjectId;
	}


	public void setEventObjectId(Integer eventObjectId) {
		this.eventObjectId = eventObjectId;
	}


	public D getEventObjectData() {
		return this.eventObjectData;
	}


	public void setEventObjectData(D eventObjectData) {
		this.eventObjectData = eventObjectData;
	}
}
