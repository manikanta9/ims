package com.clifton.reconciliation.event.object.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectStatuses;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author stevenf
 */
public class ReconciliationEventObjectSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchFieldPath = "runObject", searchField = "reconciliationRun.id")
	private Integer runId;

	@SearchField(searchFieldPath = "runObject", searchField = "reconciliationRun.id")
	private Integer[] runIds;

	@SearchField(searchField = "runObject.id")
	private Long runObjectId;

	@SearchField(searchField = "runObject.id")
	private Long[] runObjectIds;

	@SearchField(searchFieldPath = "parentRunObject", searchField = "reconciliationRun.id")
	private Integer parentRunId;

	@SearchField(searchField = "parentRunObject.id")
	private Long parentRunObjectId;

	@SearchField(searchField = "eventObjectDataType.id")
	private Short dataTypeId;

	@SearchField(searchFieldPath = "eventObjectDataType", searchField = "eventObjectCategory")
	private String dataTypeCategory;

	@SearchField(searchFieldPath = "eventObjectDataType", searchField = "eventObjectCategory")
	private String[] dataTypeCategories;

	@SearchField(searchFieldPath = "eventObjectDataType", searchField = "eventObjectCategory", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeDataTypeCategory;

	@SearchField(searchFieldPath = "eventObjectDataType", searchField = "name")
	private String dataTypeName;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	ReconciliationEventObjectStatuses status;

	@SearchField
	private String statusMessage;

	@SearchField(searchFieldPath = "eventObjectData", searchField = "$.eventDate", searchFieldCustomType = SearchFieldCustomTypes.JSON_COMPRESSED_TEXT)
	private Date eventDate;

	@SearchField(searchFieldPath = "eventObjectData", searchField = "$.eventNote", searchFieldCustomType = SearchFieldCustomTypes.JSON_COMPRESSED_TEXT)
	private String eventNote;

	@SearchField(searchFieldPath = "eventObjectData", searchField = "$.holdingAccountNumber", searchFieldCustomType = SearchFieldCustomTypes.JSON_COMPRESSED_TEXT)
	private String holdingAccountNumber;

	@SearchField(searchFieldPath = "eventObjectData", searchField = "$.sourceTable", searchFieldCustomType = SearchFieldCustomTypes.JSON_COMPRESSED_TEXT)
	private String sourceTable;

	@SearchField(searchFieldPath = "eventObjectData", searchField = "$.sourceFkFieldId", searchFieldCustomType = SearchFieldCustomTypes.JSON_COMPRESSED_TEXT)
	private Long sourceFkFieldId;

	//Specific to ReconciliationEventObjectJournalData
	@SearchField(searchFieldPath = "eventObjectData", searchField = "$.settlementDate", searchFieldCustomType = SearchFieldCustomTypes.JSON_COMPRESSED_TEXT)
	private Date settlementDate;

	@SearchField(searchFieldPath = "eventObjectData", searchField = "$.transactionDate", searchFieldCustomType = SearchFieldCustomTypes.JSON_COMPRESSED_TEXT)
	private Date transactionDate;

	@SearchField(searchFieldPath = "eventObjectData", searchField = "$.originalTransactionDate", searchFieldCustomType = SearchFieldCustomTypes.JSON_COMPRESSED_TEXT)
	private Date originalTransactionDate;

	@SearchField(searchFieldPath = "eventObjectData", searchField = "$.amount", searchFieldCustomType = SearchFieldCustomTypes.JSON_COMPRESSED_TEXT)
	private BigDecimal amount;

	//Specific to ReconciliationEventObjectAccountData
	@SearchField(searchFieldPath = "eventObjectData", searchField = "$.securityIdentifier", searchFieldCustomType = SearchFieldCustomTypes.JSON_COMPRESSED_TEXT)
	private String securityIdentifier;

	//Custom search field
	private Integer runOrParentRunId;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public Integer[] getRunIds() {
		return this.runIds;
	}


	public void setRunIds(Integer[] runIds) {
		this.runIds = runIds;
	}


	public Integer getParentRunId() {
		return this.parentRunId;
	}


	public void setParentRunId(Integer parentRunId) {
		this.parentRunId = parentRunId;
	}


	public Long getParentRunObjectId() {
		return this.parentRunObjectId;
	}


	public void setParentRunObjectId(Long parentRunObjectId) {
		this.parentRunObjectId = parentRunObjectId;
	}


	public Long getRunObjectId() {
		return this.runObjectId;
	}


	public void setRunObjectId(Long runObjectId) {
		this.runObjectId = runObjectId;
	}


	public Long[] getRunObjectIds() {
		return this.runObjectIds;
	}


	public void setRunObjectIds(Long[] runObjectIds) {
		this.runObjectIds = runObjectIds;
	}


	public Short getDataTypeId() {
		return this.dataTypeId;
	}


	public void setDataTypeId(Short dataTypeId) {
		this.dataTypeId = dataTypeId;
	}


	public String getDataTypeCategory() {
		return this.dataTypeCategory;
	}


	public void setDataTypeCategory(String dataTypeCategory) {
		this.dataTypeCategory = dataTypeCategory;
	}


	public String[] getDataTypeCategories() {
		return this.dataTypeCategories;
	}


	public void setDataTypeCategories(String[] dataTypeCategories) {
		this.dataTypeCategories = dataTypeCategories;
	}


	public String getExcludeDataTypeCategory() {
		return this.excludeDataTypeCategory;
	}


	public void setExcludeDataTypeCategory(String excludeDataTypeCategory) {
		this.excludeDataTypeCategory = excludeDataTypeCategory;
	}


	public String getDataTypeName() {
		return this.dataTypeName;
	}


	public void setDataTypeName(String dataTypeName) {
		this.dataTypeName = dataTypeName;
	}


	public ReconciliationEventObjectStatuses getStatus() {
		return this.status;
	}


	public void setStatus(ReconciliationEventObjectStatuses status) {
		this.status = status;
	}


	public String getStatusMessage() {
		return this.statusMessage;
	}


	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public String getEventNote() {
		return this.eventNote;
	}


	public void setEventNote(String eventNote) {
		this.eventNote = eventNote;
	}


	public String getHoldingAccountNumber() {
		return this.holdingAccountNumber;
	}


	public void setHoldingAccountNumber(String holdingAccountNumber) {
		this.holdingAccountNumber = holdingAccountNumber;
	}


	public String getSourceTable() {
		return this.sourceTable;
	}


	public void setSourceTable(String sourceTable) {
		this.sourceTable = sourceTable;
	}


	public Long getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Long sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public Date getOriginalTransactionDate() {
		return this.originalTransactionDate;
	}


	public void setOriginalTransactionDate(Date originalTransactionDate) {
		this.originalTransactionDate = originalTransactionDate;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public String getSecurityIdentifier() {
		return this.securityIdentifier;
	}


	public void setSecurityIdentifier(String securityIdentifier) {
		this.securityIdentifier = securityIdentifier;
	}


	public Integer getRunOrParentRunId() {
		return this.runOrParentRunId;
	}


	public void setRunOrParentRunId(Integer runOrParentRunId) {
		this.runOrParentRunId = runOrParentRunId;
	}

}
