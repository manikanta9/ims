package com.clifton.reconciliation.event.object;

/**
 * @author stevenf
 */
public enum ReconciliationEventObjectStatuses {
	OPEN(false),
	PENDING_APPROVAL(false),
	PENDING_EXECUTION(true),
	COMPLETE(true),
	ERROR(false),
	NOT_PROCESSED(false);

	private boolean processed;


	ReconciliationEventObjectStatuses(boolean processed) {
		this.processed = processed;
	}

	////////////////////////////////////////////////////////////////////////////////


	public boolean processEvent(ReconciliationEventObjectStatuses eventObjectStatus) {
		return !eventObjectStatus.processed;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isProcessed() {
		return this.processed;
	}
}
