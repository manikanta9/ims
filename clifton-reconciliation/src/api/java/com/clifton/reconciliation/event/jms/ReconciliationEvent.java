package com.clifton.reconciliation.event.jms;

import com.clifton.core.util.event.EventObject;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectData;


/**
 * @author stevenf
 */
public class ReconciliationEvent extends EventObject<ReconciliationEventObjectData, Object> {

	public static ReconciliationEvent ofEventTarget(String eventName, String eventTypeName, ReconciliationEventObjectData eventObjectData) {
		ReconciliationEvent event = new ReconciliationEvent();
		event.setEventName(eventName);
		event.addContextValue("eventTypeName", eventTypeName);
		event.setTarget(eventObjectData);
		return event;
	}
}
