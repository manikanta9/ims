package com.clifton.reconciliation.event.object;

import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectDataType;


/**
 * The <code>ReconciliationEventObjectCategories</code> is used to categorize {@link ReconciliationEventObjectDataType}
 * and is used as the discriminator value when storing/retrieving the eventObjectData as COMPRESSED text in the database
 *
 * @author stevenf
 */
public enum ReconciliationEventObjectCategories {
	ACCOUNT,
	SIMPLE_JOURNAL,
	ADJUSTING_JOURNAL
}
