package com.clifton.reconciliation.event.object;

import com.clifton.core.beans.BaseEntity;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectData;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectDataType;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.fasterxml.jackson.annotation.JsonTypeInfo;


/**
 * Generic object used in JMS Messages from Reconciliation to IMS
 * Used to create journal entries of various types in addition to providing
 * functionality to lock and unlock accounts.
 *
 * @author stevenf
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public class ReconciliationEventObject<D extends ReconciliationEventObjectData> extends BaseEntity<Integer> {

	private ReconciliationRunObject parentRunObject;

	private ReconciliationRunObject runObject;

	private ReconciliationEventObjectDataType eventObjectDataType;

	@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
	private D eventObjectData;

	ReconciliationEventObjectStatuses status;

	private String statusMessage;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ReconciliationRunObject getParentRunObject() {
		return this.parentRunObject;
	}


	public void setParentRunObject(ReconciliationRunObject parentRunObject) {
		this.parentRunObject = parentRunObject;
	}


	public ReconciliationRunObject getRunObject() {
		return this.runObject;
	}


	public void setRunObject(ReconciliationRunObject runObject) {
		this.runObject = runObject;
	}


	public ReconciliationEventObjectDataType getEventObjectDataType() {
		return this.eventObjectDataType;
	}


	public void setEventObjectDataType(ReconciliationEventObjectDataType eventObjectDataType) {
		this.eventObjectDataType = eventObjectDataType;
	}


	public D getEventObjectData() {
		return this.eventObjectData;
	}


	public void setEventObjectData(D eventObjectData) {
		this.eventObjectData = eventObjectData;
	}


	public ReconciliationEventObjectStatuses getStatus() {
		return this.status;
	}


	public void setStatus(ReconciliationEventObjectStatuses status) {
		this.status = status;
	}


	public String getStatusMessage() {
		return this.statusMessage;
	}


	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
}
