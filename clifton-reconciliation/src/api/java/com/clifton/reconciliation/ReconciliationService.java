package com.clifton.reconciliation;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.file.ReconciliationFileCommand;
import com.clifton.reconciliation.file.ReconciliationFileDefinition;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;

import java.util.Map;


/**
 * The <code>ReconciliationService</code> defines methods for working importing external data into the reconciliation system.
 * in addition to methods used to execute the actual reconciliation process after data has been imported from all sources.
 * <p>
 * The reconciliation process consists of the following steps:
 * <p>
 * 1.  Convert uploaded file to dataTable object.
 * 2.  Create, or locate, applicable reconciliation run for the given date, definition(s) and source.
 * 3.  Create and save {@link ReconciliationRunObject}(s) from imported data.
 * 3a. Update statuses of loaded data; e.g. NOT_MATCHED, MATCHED, NOT_RECONCILED
 * 4.  When 'processReconciliationByDefinitionAndDate' is called the data is process and reconciled.
 * 4a. Update statuses of loaded 'MATCHED' data to 'RECONCILED' or 'NOT_RECONCILED' based on processing.
 * 5.  Save objects with updated status, and update the reconciliation run with the overall status.
 *
 * @author StevenF on 11/23/2016.
 */
public interface ReconciliationService {

	////////////////////////////////////////////////////////////////////////////
	////////          Reconciliation Service Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = ReconciliationRun.class)
	public Status uploadReconciliationFile(ReconciliationFileCommand fileCommand);


	@DoNotAddRequestMapping
	public ReconciliationRunObject reprocessReconciliationRunObjectRowData(ReconciliationFileDefinition fileDefinition, ReconciliationRunObject runObject, Map<String, Object> rowData, Map<String, ReconciliationRunObject> runObjectMap);


	@DoNotAddRequestMapping
	public boolean excludeRunObject(ReconciliationRunObject runObject, ReconciliationSource source, ReconciliationFileDefinition fileDefinition);
}
