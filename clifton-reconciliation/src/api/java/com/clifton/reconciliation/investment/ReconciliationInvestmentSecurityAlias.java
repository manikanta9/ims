package com.clifton.reconciliation.investment;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.reconciliation.definition.ReconciliationSource;

import java.util.Date;


/**
 * The <code>ReconciliationInvestmentSecurityAlias</code> provides the ability to create 'memorized matches'
 * in reconciliation.  Users are able to add optional 'aliases' to securities that are added to the
 * ReconciliationInvestmentSecurityCache so a proper security identifier can be looked up using the alias.
 *
 * @author StevenF
 */
public class ReconciliationInvestmentSecurityAlias extends BaseEntity<Integer> {

	private ReconciliationSource reconciliationSource;
	private ReconciliationInvestmentSecurity investmentSecurity;

	private String alias;

	/**
	 * The property of the {@link ReconciliationInvestmentSecurity} to use for display (e.g. securitySymbol, or securityCusip, etc)
	 */
	private String displayProperty;


	private Date startDate;
	private Date endDate;

	////////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return "'" + getAlias() + "' -> '" + BeanUtils.getPropertyValue(getInvestmentSecurity(), getDisplayProperty()) + "'";
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationSource getReconciliationSource() {
		return this.reconciliationSource;
	}


	public void setReconciliationSource(ReconciliationSource reconciliationSource) {
		this.reconciliationSource = reconciliationSource;
	}


	public ReconciliationInvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(ReconciliationInvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public String getAlias() {
		return this.alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public String getDisplayProperty() {
		return this.displayProperty;
	}


	public void setDisplayProperty(String displayProperty) {
		this.displayProperty = displayProperty;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
