package com.clifton.reconciliation.investment;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.reconciliation.definition.ReconciliationSource;

import java.util.Date;


/**
 * The <code>ReconciliationInvestmentAccountAlias</code> provides the ability to create 'memorized matches'
 * in reconciliation.  Users are able to add optional 'aliases' to accounts that are added to the
 * ReconciliationInvestmentAccountCache so a proper account can be looked up using the alias.
 *
 * @author StevenF
 */
public class ReconciliationInvestmentAccountAlias extends BaseEntity<Integer> {

	private ReconciliationSource reconciliationSource;
	private ReconciliationInvestmentAccount investmentAccount;

	private String alias;

	/**
	 * The property of the {@link ReconciliationInvestmentAccount} to use for display (e.g. accountNumber, or accountNumber2, etc)
	 */
	private String displayProperty;

	private Date startDate;
	private Date endDate;

	////////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return "'" + getAlias() + "' -> '" + BeanUtils.getPropertyValue(getInvestmentAccount(), getDisplayProperty()) + "'";
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationSource getReconciliationSource() {
		return this.reconciliationSource;
	}


	public void setReconciliationSource(ReconciliationSource reconciliationSource) {
		this.reconciliationSource = reconciliationSource;
	}


	public ReconciliationInvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(ReconciliationInvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public String getAlias() {
		return this.alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public String getDisplayProperty() {
		return this.displayProperty;
	}


	public void setDisplayProperty(String displayProperty) {
		this.displayProperty = displayProperty;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
