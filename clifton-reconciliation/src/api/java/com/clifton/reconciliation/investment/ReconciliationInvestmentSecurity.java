package com.clifton.reconciliation.investment;

import com.clifton.core.beans.BaseEntity;

import java.util.Date;


/**
 * The <code>ReconciliationInvestmentSecurity</code> objects consist of simplified security info synced from IMS.
 * They are explicitly set on <code>ReconciliationRunObject</code> in order to provide simpler searching
 * and viewing of reconciliation data without the need to deserialize the data to determine the account
 * the data is associated with.
 *
 * @author StevenF
 */
public class ReconciliationInvestmentSecurity extends BaseEntity<Integer> {

	private Integer securityIdentifier;

	private String securityName;
	private String securitySymbol;
	private String securityCusip;
	private String securityIsin;
	private String securitySedol;
	private String securityOccSymbol;
	private String currencyDenomination;

	private String securityTypeName;
	private String securityTypeSubTypeName;
	private String securityTypeSubType2Name;

	private Date endDate;

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getSecurityIdentifier() {
		return this.securityIdentifier;
	}


	public void setSecurityIdentifier(Integer securityIdentifier) {
		this.securityIdentifier = securityIdentifier;
	}


	public String getSecurityName() {
		return this.securityName;
	}


	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}


	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	public String getSecurityCusip() {
		return this.securityCusip;
	}


	public void setSecurityCusip(String securityCusip) {
		this.securityCusip = securityCusip;
	}


	public String getSecurityIsin() {
		return this.securityIsin;
	}


	public void setSecurityIsin(String securityIsin) {
		this.securityIsin = securityIsin;
	}


	public String getSecuritySedol() {
		return this.securitySedol;
	}


	public void setSecuritySedol(String securitySedol) {
		this.securitySedol = securitySedol;
	}


	public String getSecurityOccSymbol() {
		return this.securityOccSymbol;
	}


	public void setSecurityOccSymbol(String securityOccSymbol) {
		this.securityOccSymbol = securityOccSymbol;
	}


	public String getCurrencyDenomination() {
		return this.currencyDenomination;
	}


	public void setCurrencyDenomination(String currencyDenomination) {
		this.currencyDenomination = currencyDenomination;
	}


	public String getSecurityTypeName() {
		return this.securityTypeName;
	}


	public void setSecurityTypeName(String securityTypeName) {
		this.securityTypeName = securityTypeName;
	}


	public String getSecurityTypeSubTypeName() {
		return this.securityTypeSubTypeName;
	}


	public void setSecurityTypeSubTypeName(String securityTypeSubTypeName) {
		this.securityTypeSubTypeName = securityTypeSubTypeName;
	}


	public String getSecurityTypeSubType2Name() {
		return this.securityTypeSubType2Name;
	}


	public void setSecurityTypeSubType2Name(String securityTypeSubType2Name) {
		this.securityTypeSubType2Name = securityTypeSubType2Name;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
