package com.clifton.reconciliation.investment;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.event.object.ReconciliationEventObject;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentAccountAliasSearchForm;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentAccountSearchForm;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentSecurityAliasSearchForm;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentSecuritySearchForm;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;


/**
 * The <code>ReconciliationInvestmentService</code> defines methods for working with
 * {@link ReconciliationInvestmentAccount} and {@link ReconciliationInvestmentSecurity} objects
 * <p>
 * Reconciliation accounts and securities are synced from external sources such as IMS.  They are explicitly set on reconciliation
 * objects in order to provide simpler searching and viewing of reconciliation data without the need to deserialize
 * the data to determine the account the data is associated with; additionally, this forces validation by ensuring the accounts
 * and securities mapped by reconciliation definition fields are in fact valid accounts and securities in the system.
 *
 * @author StevenF on 11/23/2016.
 */
public interface ReconciliationInvestmentService {

	//Used so they can be overridden in InMemoryDB test that doesn't support JSON_VALUE in query.
	@DoNotAddRequestMapping
	public SqlSelectCommand addAccountAliasQueryParams(SqlSelectCommand selectCommand, ReconciliationInvestmentAccountAlias accountAlias);


	@DoNotAddRequestMapping
	public String getAccountAliasQuery(boolean revert);


	@DoNotAddRequestMapping
	public SqlSelectCommand addSecurityAliasQueryParams(SqlSelectCommand selectCommand, ReconciliationInvestmentSecurityAlias securityAlias);


	@DoNotAddRequestMapping
	public String getSecurityAliasQuery(boolean revert);

	////////////////////////////////////////////////////////////////////////////
	////////          Reconciliation Account Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationInvestmentAccount getReconciliationInvestmentAccount(int id);


	public ReconciliationInvestmentAccount getReconciliationInvestmentAccountByNumber(String accountNumber);


	public List<ReconciliationInvestmentAccount> getReconciliationInvestmentAccountList(ReconciliationInvestmentAccountSearchForm searchForm);


	public ReconciliationInvestmentAccount saveReconciliationInvestmentAccount(ReconciliationInvestmentAccount investmentAccount);


	public void deleteReconciliationInvestmentAccount(int id);

	////////////////////////////////////////////////////////////////////////////
	////////      Reconciliation Account Alias Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationInvestmentAccountAlias getReconciliationInvestmentAccountAlias(int id);


	public List<ReconciliationInvestmentAccountAlias> getReconciliationInvestmentAccountAliasList(ReconciliationInvestmentAccountAliasSearchForm searchForm);


	public ReconciliationInvestmentAccountAlias saveReconciliationInvestmentAccountAlias(ReconciliationInvestmentAccountAlias investmentAccountAlias);


	public void deleteReconciliationInvestmentAccountAlias(int id);


	////////////////////////////////////////////////////////////////////////////
	////////       Reconciliation Account Alias Utility Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, ReconciliationInvestmentAccountAlias> getReconciliationInvestmentAccountAliasMap(String sourceName, Date activeOnDate);


	@SecureMethod(permissions = SecurityPermission.PERMISSION_EXECUTE)
	public Status processReconciliationInvestmentAccountAlias(short sourceId, Date runDate, boolean synchronous, Predicate<ReconciliationEventObject<?>> eventObjectPredicate);

	////////////////////////////////////////////////////////////////////////////
	////////         Reconciliation Security Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurity(int id);


	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurityBySymbol(String symbol);


	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurityByOCCSymbol(String occSymbol);


	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurityBySedol(String sedol);


	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurityByIsin(String isin);


	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurityByCusip(String cusip);


	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurityByIdentifier(String securityIdentifier);


	public List<ReconciliationInvestmentSecurity> getReconciliationInvestmentSecurityList(ReconciliationInvestmentSecuritySearchForm searchForm);


	public ReconciliationInvestmentSecurity saveReconciliationInvestmentSecurity(ReconciliationInvestmentSecurity investmentSecurity);


	public void deleteReconciliationInvestmentSecurity(int id);


	////////////////////////////////////////////////////////////////////////////
	////////      Reconciliation Security Alias Business Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationInvestmentSecurityAlias getReconciliationInvestmentSecurityAlias(int id);


	public List<ReconciliationInvestmentSecurityAlias> getReconciliationInvestmentSecurityAliasList(ReconciliationInvestmentSecurityAliasSearchForm searchForm);


	public ReconciliationInvestmentSecurityAlias saveReconciliationInvestmentSecurityAlias(ReconciliationInvestmentSecurityAlias investmentSecurityAlias);


	public void deleteReconciliationInvestmentSecurityAlias(int id);


	////////////////////////////////////////////////////////////////////////////
	////////       Reconciliation Security Alias Utility Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, ReconciliationInvestmentSecurityAlias> getReconciliationInvestmentSecurityAliasMap(String sourceName, Date activeOnDate);

	@SecureMethod(permissions = SecurityPermission.PERMISSION_EXECUTE)
	public Status processReconciliationInvestmentSecurityAlias(short sourceId, Date runDate, boolean synchronous);

	@DoNotAddRequestMapping
	@SecureMethod(permissions = SecurityPermission.PERMISSION_EXECUTE)
	public Status processReconciliationInvestmentSecurityAlias(short sourceId, Date runDate, boolean synchronous, Predicate<ReconciliationEventObject<?>> eventObjectPredicate);
}
