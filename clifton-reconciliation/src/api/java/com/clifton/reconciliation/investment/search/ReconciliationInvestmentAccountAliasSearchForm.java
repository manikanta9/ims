package com.clifton.reconciliation.investment.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * @author StevenF
 */
public class ReconciliationInvestmentAccountAliasSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField
	private String alias;

	@SearchField(searchField = "reconciliationSource.id")
	private Short sourceId;

	@SearchField(searchFieldPath = "reconciliationSource", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String sourceName;

	@SearchField(searchField = "investmentAccount.id")
	Integer accountId;

	@SearchField(searchFieldPath = "investmentAccount", searchField = "accountName")
	private String accountName;

	@SearchField(searchFieldPath = "investmentAccount", searchField = "accountNumber")
	private String accountNumber;

	@SearchField(searchFieldPath = "investmentAccount", searchField = "accountNumber2")
	private String accountNumber2;

	@SearchField
	private String displayProperty;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getAlias() {
		return this.alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public Short getSourceId() {
		return this.sourceId;
	}


	public void setSourceId(Short sourceId) {
		this.sourceId = sourceId;
	}


	public String getSourceName() {
		return this.sourceName;
	}


	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}


	public Integer getAccountId() {
		return this.accountId;
	}


	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}


	public String getAccountName() {
		return this.accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public String getAccountNumber2() {
		return this.accountNumber2;
	}


	public void setAccountNumber2(String accountNumber2) {
		this.accountNumber2 = accountNumber2;
	}


	public String getDisplayProperty() {
		return this.displayProperty;
	}


	public void setDisplayProperty(String displayProperty) {
		this.displayProperty = displayProperty;
	}
}
