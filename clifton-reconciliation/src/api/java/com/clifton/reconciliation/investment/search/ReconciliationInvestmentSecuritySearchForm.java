package com.clifton.reconciliation.investment.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * ReconciliationDefinition allows for defining how an object should be reconciled.
 * It utilizes beanPaths and comparators to define how the internal and external target
 * fields should be compared.
 *
 * @author StevenF on 11/22/2016.
 */
public class ReconciliationInvestmentSecuritySearchForm extends BaseAuditableEntitySearchForm {


	@SearchField(searchField = "securityName,securitySymbol,securityCusip,securityIsin,securitySedol,securityOccSymbol")
	private String searchPattern;

	@SearchField(searchField = "securityName,securitySymbol,securityCusip,securityIsin,securitySedol,securityOccSymbol", comparisonConditions = {ComparisonConditions.BEGINS_WITH})
	private String searchPatternUsingBeginsWith;

	@SearchField
	private Integer securityIdentifier;

	@SearchField
	private String securityName;

	@SearchField
	private String securitySymbol;

	@SearchField
	private String securityCusip;

	@SearchField
	private String securityIsin;

	@SearchField
	private String securitySedol;

	@SearchField
	private String securityOccSymbol;

	@SearchField
	private String securityTypeName;

	@SearchField
	private String securityTypeSubTypeName;

	@SearchField
	private String securityTypeSubType2Name;

	@SearchField
	private Date endDate;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getSearchPatternUsingBeginsWith() {
		return this.searchPatternUsingBeginsWith;
	}


	public void setSearchPatternUsingBeginsWith(String searchPatternUsingBeginsWith) {
		this.searchPatternUsingBeginsWith = searchPatternUsingBeginsWith;
	}


	public Integer getSecurityIdentifier() {
		return this.securityIdentifier;
	}


	public void setSecurityIdentifier(Integer securityIdentifier) {
		this.securityIdentifier = securityIdentifier;
	}


	public String getSecurityName() {
		return this.securityName;
	}


	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}


	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	public String getSecurityCusip() {
		return this.securityCusip;
	}


	public void setSecurityCusip(String securityCusip) {
		this.securityCusip = securityCusip;
	}


	public String getSecurityIsin() {
		return this.securityIsin;
	}


	public void setSecurityIsin(String securityIsin) {
		this.securityIsin = securityIsin;
	}


	public String getSecuritySedol() {
		return this.securitySedol;
	}


	public void setSecuritySedol(String securitySedol) {
		this.securitySedol = securitySedol;
	}


	public String getSecurityOccSymbol() {
		return this.securityOccSymbol;
	}


	public void setSecurityOccSymbol(String securityOccSymbol) {
		this.securityOccSymbol = securityOccSymbol;
	}


	public String getSecurityTypeName() {
		return this.securityTypeName;
	}


	public void setSecurityTypeName(String securityTypeName) {
		this.securityTypeName = securityTypeName;
	}


	public String getSecurityTypeSubTypeName() {
		return this.securityTypeSubTypeName;
	}


	public void setSecurityTypeSubTypeName(String securityTypeSubTypeName) {
		this.securityTypeSubTypeName = securityTypeSubTypeName;
	}


	public String getSecurityTypeSubType2Name() {
		return this.securityTypeSubType2Name;
	}


	public void setSecurityTypeSubType2Name(String securityTypeSubType2Name) {
		this.securityTypeSubType2Name = securityTypeSubType2Name;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
