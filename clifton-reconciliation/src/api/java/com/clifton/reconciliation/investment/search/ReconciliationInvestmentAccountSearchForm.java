package com.clifton.reconciliation.investment.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * ReconciliationDefinition allows for defining how an object should be reconciled.
 * It utilizes beanPaths and comparators to define how the internal and external target
 * fields should be compared.
 *
 * @author StevenF on 11/22/2016.
 */
public class ReconciliationInvestmentAccountSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "accountName,accountNumber,accountNumber2")
	private String searchPattern;

	@SearchField(searchField = "accountName,accountNumber,accountNumber2", comparisonConditions = {ComparisonConditions.BEGINS_WITH})
	private String searchPatternUsingBeginsWith;

	@SearchField
	private Integer accountIdentifier;

	@SearchField
	private String accountName;

	@SearchField
	private String accountNumber;

	@SearchField
	private String accountNumber2;

	@SearchField
	private String baseCurrency;

	@SearchField
	private String issuingCompany;

	@SearchField
	private String workflowState;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getSearchPatternUsingBeginsWith() {
		return this.searchPatternUsingBeginsWith;
	}


	public void setSearchPatternUsingBeginsWith(String searchPatternUsingBeginsWith) {
		this.searchPatternUsingBeginsWith = searchPatternUsingBeginsWith;
	}


	public Integer getAccountIdentifier() {
		return this.accountIdentifier;
	}


	public void setAccountIdentifier(Integer accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}


	public String getAccountName() {
		return this.accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public String getAccountNumber2() {
		return this.accountNumber2;
	}


	public void setAccountNumber2(String accountNumber2) {
		this.accountNumber2 = accountNumber2;
	}


	public String getBaseCurrency() {
		return this.baseCurrency;
	}


	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}


	public String getIssuingCompany() {
		return this.issuingCompany;
	}


	public void setIssuingCompany(String issuingCompany) {
		this.issuingCompany = issuingCompany;
	}


	public String getWorkflowState() {
		return this.workflowState;
	}


	public void setWorkflowState(String workflowState) {
		this.workflowState = workflowState;
	}
}
