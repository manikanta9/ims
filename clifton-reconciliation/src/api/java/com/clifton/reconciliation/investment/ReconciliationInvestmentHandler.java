package com.clifton.reconciliation.investment;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.reconciliation.run.ReconciliationRun;


/**
 * @author stevenf
 */
public interface ReconciliationInvestmentHandler {

	@DoNotAddRequestMapping
	public void sendAccountUnlockEvents(ReconciliationRun run);
}
