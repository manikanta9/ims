package com.clifton.reconciliation.investment;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;


/**
 * The <code>ReconciliationInvestmentAccount</code> is a 'flattened' representation of account data
 * synced from IMS. It is used during data loads for the reconciliation system to ensure that accounts
 * being loaded actually exist in IMS.
 *
 * @author StevenF
 */
public class ReconciliationInvestmentAccount extends BaseEntity<Integer> implements LabeledObject {

	private Integer accountIdentifier;
	private String accountName;
	private String accountNumber;
	private String accountNumber2;
	private String baseCurrency;
	private String issuingCompany;
	private String workflowState;

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getAccountNumber() + ": " + getAccountName();
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getAccountIdentifier() {
		return this.accountIdentifier;
	}


	public void setAccountIdentifier(Integer accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}


	public String getAccountName() {
		return this.accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public String getAccountNumber2() {
		return this.accountNumber2;
	}


	public void setAccountNumber2(String accountNumber2) {
		this.accountNumber2 = accountNumber2;
	}


	public String getBaseCurrency() {
		return this.baseCurrency;
	}


	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}


	public String getIssuingCompany() {
		return this.issuingCompany;
	}


	public void setIssuingCompany(String issuingCompany) {
		this.issuingCompany = issuingCompany;
	}


	public String getWorkflowState() {
		return this.workflowState;
	}


	public void setWorkflowState(String workflowState) {
		this.workflowState = workflowState;
	}
}
