package com.clifton.reconciliation.investment.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * @author StevenF
 */
public class ReconciliationInvestmentSecurityAliasSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField
	private String alias;

	@SearchField(searchField = "reconciliationSource.id")
	private Short sourceId;

	@SearchField(searchFieldPath = "reconciliationSource", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String sourceName;


	@SearchField(searchField = "investmentSecurity.id")
	Integer securityId;

	@SearchField(searchFieldPath = "investmentSecurity", searchField = "securityName")
	private String securityName;

	@SearchField(searchFieldPath = "investmentSecurity", searchField = "securitySymbol")
	private String securitySymbol;

	@SearchField(searchFieldPath = "investmentSecurity", searchField = "securityCusip")
	private String securityCusip;

	@SearchField(searchFieldPath = "investmentSecurity", searchField = "securityIsin")
	private String securityIsin;

	@SearchField(searchFieldPath = "investmentSecurity", searchField = "securitySedol")
	private String securitySedol;

	@SearchField(searchFieldPath = "investmentSecurity", searchField = "securityOccSymbol")
	private String securityOccSymbol;

	@SearchField(searchFieldPath = "investmentSecurity", searchField = "securityTypeName")
	private String securityTypeName;

	@SearchField(searchFieldPath = "investmentSecurity", searchField = "securityTypeSubTypeName")
	private String securityTypeSubTypeName;

	@SearchField(searchFieldPath = "investmentSecurity", searchField = "securityTypeSubType2Name")
	private String securityTypeSubType2Name;

	@SearchField
	private String displayProperty;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getAlias() {
		return this.alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public Short getSourceId() {
		return this.sourceId;
	}


	public void setSourceId(Short sourceId) {
		this.sourceId = sourceId;
	}


	public String getSourceName() {
		return this.sourceName;
	}


	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public String getSecurityName() {
		return this.securityName;
	}


	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}


	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	public String getSecurityCusip() {
		return this.securityCusip;
	}


	public void setSecurityCusip(String securityCusip) {
		this.securityCusip = securityCusip;
	}


	public String getSecurityIsin() {
		return this.securityIsin;
	}


	public void setSecurityIsin(String securityIsin) {
		this.securityIsin = securityIsin;
	}


	public String getSecuritySedol() {
		return this.securitySedol;
	}


	public void setSecuritySedol(String securitySedol) {
		this.securitySedol = securitySedol;
	}


	public String getSecurityOccSymbol() {
		return this.securityOccSymbol;
	}


	public void setSecurityOccSymbol(String securityOccSymbol) {
		this.securityOccSymbol = securityOccSymbol;
	}


	public String getSecurityTypeName() {
		return this.securityTypeName;
	}


	public void setSecurityTypeName(String securityTypeName) {
		this.securityTypeName = securityTypeName;
	}


	public String getSecurityTypeSubTypeName() {
		return this.securityTypeSubTypeName;
	}


	public void setSecurityTypeSubTypeName(String securityTypeSubTypeName) {
		this.securityTypeSubTypeName = securityTypeSubTypeName;
	}


	public String getSecurityTypeSubType2Name() {
		return this.securityTypeSubType2Name;
	}


	public void setSecurityTypeSubType2Name(String securityTypeSubType2Name) {
		this.securityTypeSubType2Name = securityTypeSubType2Name;
	}


	public String getDisplayProperty() {
		return this.displayProperty;
	}


	public void setDisplayProperty(String displayProperty) {
		this.displayProperty = displayProperty;
	}
}
