package com.clifton.reconciliation.investment;

/**
 * @author stevenf
 */
public final class ReconciliationInvestmentAccountBuilder {

	private Integer accountIdentifier;
	private String accountName;
	private String accountNumber;
	private String accountNumber2;
	private String baseCurrency;
	private String issuingCompany;
	private String workflowState;


	private ReconciliationInvestmentAccountBuilder() {
	}


	public static ReconciliationInvestmentAccountBuilder aReconciliationInvestmentAccount() {
		return new ReconciliationInvestmentAccountBuilder();
	}


	public ReconciliationInvestmentAccountBuilder withAccountIdentifier(Integer accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
		return this;
	}


	public ReconciliationInvestmentAccountBuilder withAccountName(String accountName) {
		this.accountName = accountName;
		return this;
	}


	public ReconciliationInvestmentAccountBuilder withAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
		return this;
	}


	public ReconciliationInvestmentAccountBuilder withAccountNumber2(String accountNumber2) {
		this.accountNumber2 = accountNumber2;
		return this;
	}


	public ReconciliationInvestmentAccountBuilder withBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
		return this;
	}


	public ReconciliationInvestmentAccountBuilder withIssuingCompany(String issuingCompany) {
		this.issuingCompany = issuingCompany;
		return this;
	}


	public ReconciliationInvestmentAccountBuilder withWorkflowState(String workflowState) {
		this.workflowState = workflowState;
		return this;
	}


	public ReconciliationInvestmentAccount build() {
		ReconciliationInvestmentAccount reconciliationInvestmentAccount = new ReconciliationInvestmentAccount();
		reconciliationInvestmentAccount.setAccountIdentifier(this.accountIdentifier);
		reconciliationInvestmentAccount.setAccountName(this.accountName);
		reconciliationInvestmentAccount.setAccountNumber(this.accountNumber);
		reconciliationInvestmentAccount.setAccountNumber2(this.accountNumber2);
		reconciliationInvestmentAccount.setBaseCurrency(this.baseCurrency);
		reconciliationInvestmentAccount.setIssuingCompany(this.issuingCompany);
		reconciliationInvestmentAccount.setWorkflowState(this.workflowState);
		return reconciliationInvestmentAccount;
	}
}
