package com.clifton.reconciliation.run;

import com.clifton.reconciliation.run.data.DataNote;
import com.clifton.reconciliation.run.data.DataNoteTypes;

import java.util.Date;


/**
 * @author stevenf
 */
public final class DataNoteBuilder {

	private String note;
	private Short sourceId;
	private Date createDate;
	private Short createUserId;
	private DataNoteTypes type;


	private DataNoteBuilder() {
	}


	public static DataNoteBuilder aDataNote() {
		return new DataNoteBuilder();
	}


	public DataNoteBuilder withNote(String note) {
		this.note = note;
		return this;
	}


	public DataNoteBuilder withSourceId(Short sourceId) {
		this.sourceId = sourceId;
		return this;
	}


	public DataNoteBuilder withCreateDate(Date createDate) {
		this.createDate = createDate;
		return this;
	}


	public DataNoteBuilder withCreateUserId(Short createUserId) {
		this.createUserId = createUserId;
		return this;
	}


	public DataNoteBuilder withType(DataNoteTypes type) {
		this.type = type;
		return this;
	}


	public DataNote build() {
		DataNote dataNote = new DataNote();
		dataNote.setNote(note);
		dataNote.setSourceId(sourceId);
		dataNote.setCreateDate(createDate);
		dataNote.setCreateUserId(createUserId);
		dataNote.setType(type);
		return dataNote;
	}
}
