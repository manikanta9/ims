package com.clifton.reconciliation.run.status;

/**
 * @author TerryS
 */
public final class ReconciliationMatchStatusBuilder {

	private String label;
	private String name;
	private boolean matched;
	private String description;
	private boolean manual;
	private Short sortOrder;


	private ReconciliationMatchStatusBuilder() {
	}


	public static ReconciliationMatchStatusBuilder aReconciliationMatchStatus() {
		return new ReconciliationMatchStatusBuilder();
	}


	public ReconciliationMatchStatusBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public ReconciliationMatchStatusBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationMatchStatusBuilder withMatched(boolean matched) {
		this.matched = matched;
		return this;
	}


	public ReconciliationMatchStatusBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public ReconciliationMatchStatusBuilder withManual(boolean manual) {
		this.manual = manual;
		return this;
	}


	public ReconciliationMatchStatusBuilder withSortOrder(Short sortOrder) {
		this.sortOrder = sortOrder;
		return this;
	}


	public ReconciliationMatchStatus build() {
		ReconciliationMatchStatus reconciliationMatchStatus = new ReconciliationMatchStatus();
		reconciliationMatchStatus.setLabel(this.label);
		reconciliationMatchStatus.setName(this.name);
		reconciliationMatchStatus.setMatched(this.matched);
		reconciliationMatchStatus.setDescription(this.description);
		reconciliationMatchStatus.setManual(this.manual);
		reconciliationMatchStatus.setSortOrder(this.sortOrder);
		return reconciliationMatchStatus;
	}
}
