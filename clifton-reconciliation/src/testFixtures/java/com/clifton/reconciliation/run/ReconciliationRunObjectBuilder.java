package com.clifton.reconciliation.run;

import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.run.data.DataHolder;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatus;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;

import java.util.Date;


/**
 * @author terrys
 */
public final class ReconciliationRunObjectBuilder {

	private Date importDate;
	private int breakDays;
	private String identifier;
	private ReconciliationRun reconciliationRun;
	private ReconciliationMatchStatus reconciliationMatchStatus;
	private ReconciliationReconcileStatus reconciliationReconcileStatus;
	private ReconciliationInvestmentAccount reconciliationInvestmentAccount;
	private ReconciliationInvestmentSecurity reconciliationInvestmentSecurity;
	// populated when a manual reconcile is performed and represents which source is being reconciled against
	private ReconciliationSource reconciledOnSource;
	private DataHolder dataHolder;


	private ReconciliationRunObjectBuilder() {
	}


	public static ReconciliationRunObjectBuilder aReconciliationRunObject() {
		return new ReconciliationRunObjectBuilder();
	}


	public ReconciliationRunObjectBuilder withImportDate(Date importDate) {
		this.importDate = importDate;
		return this;
	}


	public ReconciliationRunObjectBuilder withBreakDays(int breakDays) {
		this.breakDays = breakDays;
		return this;
	}


	public ReconciliationRunObjectBuilder withIdentifier(String identifier) {
		this.identifier = identifier;
		return this;
	}


	public ReconciliationRunObjectBuilder withReconciliationRun(ReconciliationRun reconciliationRun) {
		this.reconciliationRun = reconciliationRun;
		return this;
	}


	public ReconciliationRunObjectBuilder withReconciliationMatchStatus(ReconciliationMatchStatus reconciliationMatchStatus) {
		this.reconciliationMatchStatus = reconciliationMatchStatus;
		return this;
	}


	public ReconciliationRunObjectBuilder withReconciliationReconcileStatus(ReconciliationReconcileStatus reconciliationReconcileStatus) {
		this.reconciliationReconcileStatus = reconciliationReconcileStatus;
		return this;
	}


	public ReconciliationRunObjectBuilder withReconciliationInvestmentAccount(ReconciliationInvestmentAccount reconciliationInvestmentAccount) {
		this.reconciliationInvestmentAccount = reconciliationInvestmentAccount;
		return this;
	}


	public ReconciliationRunObjectBuilder withReconciliationInvestmentSecurity(ReconciliationInvestmentSecurity reconciliationInvestmentSecurity) {
		this.reconciliationInvestmentSecurity = reconciliationInvestmentSecurity;
		return this;
	}


	public ReconciliationRunObjectBuilder withReconciledOnSource(ReconciliationSource reconciledOnSource) {
		this.reconciledOnSource = reconciledOnSource;
		return this;
	}


	public ReconciliationRunObjectBuilder withDataHolder(DataHolder dataHolder) {
		this.dataHolder = dataHolder;
		return this;
	}


	public ReconciliationRunObject build() {
		ReconciliationRunObject reconciliationRunObject = new ReconciliationRunObject();
		reconciliationRunObject.setImportDate(this.importDate);
		reconciliationRunObject.setBreakDays(this.breakDays);
		reconciliationRunObject.setIdentifier(this.identifier);
		reconciliationRunObject.setReconciliationRun(this.reconciliationRun);
		reconciliationRunObject.setReconciliationMatchStatus(this.reconciliationMatchStatus);
		reconciliationRunObject.setReconciliationReconcileStatus(this.reconciliationReconcileStatus);
		reconciliationRunObject.setReconciliationInvestmentAccount(this.reconciliationInvestmentAccount);
		reconciliationRunObject.setReconciliationInvestmentSecurity(this.reconciliationInvestmentSecurity);
		reconciliationRunObject.setReconciledOnSource(this.reconciledOnSource);
		reconciliationRunObject.setDataHolder(this.dataHolder);
		return reconciliationRunObject;
	}
}
