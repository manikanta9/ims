package com.clifton.reconciliation.run.status;

/**
 * @author TerryS
 */
public final class ReconciliationReconcileStatusBuilder {

	private String label;
	private String name;
	private boolean reconciled;
	private String description;
	private boolean manual;
	private Short sortOrder;


	private ReconciliationReconcileStatusBuilder() {
	}


	public static ReconciliationReconcileStatusBuilder aReconciliationReconcileStatus() {
		return new ReconciliationReconcileStatusBuilder();
	}


	public ReconciliationReconcileStatusBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public ReconciliationReconcileStatusBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationReconcileStatusBuilder withReconciled(boolean reconciled) {
		this.reconciled = reconciled;
		return this;
	}


	public ReconciliationReconcileStatusBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public ReconciliationReconcileStatusBuilder withManual(boolean manual) {
		this.manual = manual;
		return this;
	}


	public ReconciliationReconcileStatusBuilder withSortOrder(Short sortOrder) {
		this.sortOrder = sortOrder;
		return this;
	}


	public ReconciliationReconcileStatus build() {
		ReconciliationReconcileStatus reconciliationReconcileStatus = new ReconciliationReconcileStatus();
		reconciliationReconcileStatus.setLabel(this.label);
		reconciliationReconcileStatus.setName(this.name);
		reconciliationReconcileStatus.setReconciled(this.reconciled);
		reconciliationReconcileStatus.setDescription(this.description);
		reconciliationReconcileStatus.setManual(this.manual);
		reconciliationReconcileStatus.setSortOrder(this.sortOrder);
		return reconciliationReconcileStatus;
	}
}
