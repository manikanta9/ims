package com.clifton.reconciliation.run;

import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;

import java.util.Date;


/**
 * @author TerryS
 */
public final class ReconciliationRunBuilder {

	private boolean archived;
	private Date runDate;
	private Status runDetails;
	private ReconciliationDefinition reconciliationDefinition;
	private ReconciliationReconcileStatus reconciliationReconcileStatus;


	private ReconciliationRunBuilder() {
	}


	public static ReconciliationRunBuilder aReconciliationRun() {
		return new ReconciliationRunBuilder();
	}


	public ReconciliationRunBuilder withArchived(boolean archived) {
		this.archived = archived;
		return this;
	}


	public ReconciliationRunBuilder withRunDate(Date runDate) {
		this.runDate = runDate;
		return this;
	}


	public ReconciliationRunBuilder withRunDetails(Status runDetails) {
		this.runDetails = runDetails;
		return this;
	}


	public ReconciliationRunBuilder withReconciliationDefinition(ReconciliationDefinition reconciliationDefinition) {
		this.reconciliationDefinition = reconciliationDefinition;
		return this;
	}


	public ReconciliationRunBuilder withReconciliationReconcileStatus(ReconciliationReconcileStatus reconciliationReconcileStatus) {
		this.reconciliationReconcileStatus = reconciliationReconcileStatus;
		return this;
	}


	public ReconciliationRun build() {
		ReconciliationRun reconciliationRun = new ReconciliationRun();
		reconciliationRun.setArchived(this.archived);
		reconciliationRun.setRunDate(this.runDate);
		reconciliationRun.setRunDetails(this.runDetails);
		reconciliationRun.setReconciliationDefinition(this.reconciliationDefinition);
		reconciliationRun.setReconciliationReconcileStatus(this.reconciliationReconcileStatus);
		return reconciliationRun;
	}
}
