package com.clifton.reconciliation.exclusion;

import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.security.user.SecurityUser;

import java.util.Date;


/**
 * @author stevenf
 */
public final class ReconciliationExclusionAssignmentBuilder {

	private boolean include;
	private Date startDate;
	private Date endDate;
	private ReconciliationDefinition reconciliationDefinition;
	private ReconciliationSource reconciliationSource;
	private ReconciliationInvestmentAccount reconciliationInvestmentAccount;
	private ReconciliationInvestmentSecurity reconciliationInvestmentSecurity;
	private String invalidAccountNumber;
	private String invalidSecurityIdentifier;
	private String comment;
	private SecurityUser approvalUser;
	private Date approvalDate;


	private ReconciliationExclusionAssignmentBuilder() {
	}


	public static ReconciliationExclusionAssignmentBuilder aReconciliationExclusionAssignment() {
		return new ReconciliationExclusionAssignmentBuilder();
	}


	public ReconciliationExclusionAssignmentBuilder withInclude(boolean include) {
		this.include = include;
		return this;
	}


	public ReconciliationExclusionAssignmentBuilder withStartDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}


	public ReconciliationExclusionAssignmentBuilder withEndDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}


	public ReconciliationExclusionAssignmentBuilder withReconciliationDefinition(ReconciliationDefinition reconciliationDefinition) {
		this.reconciliationDefinition = reconciliationDefinition;
		return this;
	}


	public ReconciliationExclusionAssignmentBuilder withReconciliationSource(ReconciliationSource reconciliationSource) {
		this.reconciliationSource = reconciliationSource;
		return this;
	}


	public ReconciliationExclusionAssignmentBuilder withReconciliationInvestmentAccount(ReconciliationInvestmentAccount reconciliationInvestmentAccount) {
		this.reconciliationInvestmentAccount = reconciliationInvestmentAccount;
		return this;
	}


	public ReconciliationExclusionAssignmentBuilder withReconciliationInvestmentSecurity(ReconciliationInvestmentSecurity reconciliationInvestmentSecurity) {
		this.reconciliationInvestmentSecurity = reconciliationInvestmentSecurity;
		return this;
	}


	public ReconciliationExclusionAssignmentBuilder withInvalidAccountNumber(String invalidAccountNumber) {
		this.invalidAccountNumber = invalidAccountNumber;
		return this;
	}


	public ReconciliationExclusionAssignmentBuilder withInvalidSecurityIdentifier(String invalidSecurityIdentifier) {
		this.invalidSecurityIdentifier = invalidSecurityIdentifier;
		return this;
	}


	public ReconciliationExclusionAssignmentBuilder withComment(String comment) {
		this.comment = comment;
		return this;
	}


	public ReconciliationExclusionAssignmentBuilder withApprovalUser(SecurityUser approvalUser) {
		this.approvalUser = approvalUser;
		return this;
	}


	public ReconciliationExclusionAssignmentBuilder withApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
		return this;
	}


	public ReconciliationExclusionAssignment build() {
		ReconciliationExclusionAssignment reconciliationExclusionAssignment = new ReconciliationExclusionAssignment();
		reconciliationExclusionAssignment.setInclude(this.include);
		reconciliationExclusionAssignment.setStartDate(this.startDate);
		reconciliationExclusionAssignment.setEndDate(this.endDate);
		reconciliationExclusionAssignment.setReconciliationDefinition(this.reconciliationDefinition);
		reconciliationExclusionAssignment.setReconciliationSource(this.reconciliationSource);
		reconciliationExclusionAssignment.setReconciliationInvestmentAccount(this.reconciliationInvestmentAccount);
		reconciliationExclusionAssignment.setReconciliationInvestmentSecurity(this.reconciliationInvestmentSecurity);
		reconciliationExclusionAssignment.setInvalidAccountNumber(this.invalidAccountNumber);
		reconciliationExclusionAssignment.setInvalidSecurityIdentifier(this.invalidSecurityIdentifier);
		reconciliationExclusionAssignment.setComment(comment);
		reconciliationExclusionAssignment.setApprovalUser(this.approvalUser);
		reconciliationExclusionAssignment.setApprovalDate(this.approvalDate);
		return reconciliationExclusionAssignment;
	}
}
