package com.clifton.reconciliation.definition.group;

/**
 * @author TerryS
 */
public final class ReconciliationDefinitionGroupBuilder {

	private String label;
	private String name;
	private String description;
	private boolean eventTrigger;


	private ReconciliationDefinitionGroupBuilder() {
	}


	public static ReconciliationDefinitionGroupBuilder aReconciliationDefinitionGroup() {
		return new ReconciliationDefinitionGroupBuilder();
	}


	public ReconciliationDefinitionGroupBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public ReconciliationDefinitionGroupBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationDefinitionGroupBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public ReconciliationDefinitionGroupBuilder withEventTrigger(boolean eventTrigger) {
		this.eventTrigger = eventTrigger;
		return this;
	}


	public ReconciliationDefinitionGroup build() {
		ReconciliationDefinitionGroup reconciliationDefinitionGroup = new ReconciliationDefinitionGroup();
		reconciliationDefinitionGroup.setLabel(this.label);
		reconciliationDefinitionGroup.setName(this.name);
		reconciliationDefinitionGroup.setDescription(this.description);
		reconciliationDefinitionGroup.setEventTrigger(this.eventTrigger);
		return reconciliationDefinitionGroup;
	}
}
