package com.clifton.reconciliation.definition;

import com.clifton.reconciliation.rule.ReconciliationRuleCondition;
import com.clifton.reconciliation.rule.ReconciliationRuleFunction;


/**
 * @author TerryS
 */
public final class ReconciliationDefinitionFieldMappingBuilder {

	private String label;
	private String name;
	private String description;
	private ReconciliationRuleCondition conditionRule;
	private ReconciliationRuleFunction functionRule;
	private ReconciliationDefinitionField definitionField;
	private ReconciliationSource source;
	private Integer evaluationOrder;


	private ReconciliationDefinitionFieldMappingBuilder() {
	}


	public static ReconciliationDefinitionFieldMappingBuilder aReconciliationDefinitionFieldMapping() {
		return new ReconciliationDefinitionFieldMappingBuilder();
	}


	public ReconciliationDefinitionFieldMappingBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public ReconciliationDefinitionFieldMappingBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationDefinitionFieldMappingBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public ReconciliationDefinitionFieldMappingBuilder withConditionRule(ReconciliationRuleCondition conditionRule) {
		this.conditionRule = conditionRule;
		return this;
	}


	public ReconciliationDefinitionFieldMappingBuilder withFunctionRule(ReconciliationRuleFunction functionRule) {
		this.functionRule = functionRule;
		return this;
	}


	public ReconciliationDefinitionFieldMappingBuilder withDefinitionField(ReconciliationDefinitionField definitionField) {
		this.definitionField = definitionField;
		return this;
	}


	public ReconciliationDefinitionFieldMappingBuilder withSource(ReconciliationSource source) {
		this.source = source;
		return this;
	}


	public ReconciliationDefinitionFieldMappingBuilder withEvaluationOrder(Integer evaluationOrder) {
		this.evaluationOrder = evaluationOrder;
		return this;
	}


	public ReconciliationDefinitionFieldMapping build() {
		ReconciliationDefinitionFieldMapping reconciliationDefinitionFieldMapping = new ReconciliationDefinitionFieldMapping();
		reconciliationDefinitionFieldMapping.setLabel(this.label);
		reconciliationDefinitionFieldMapping.setName(this.name);
		reconciliationDefinitionFieldMapping.setDescription(this.description);
		reconciliationDefinitionFieldMapping.setConditionRule(this.conditionRule);
		reconciliationDefinitionFieldMapping.setFunctionRule(this.functionRule);
		reconciliationDefinitionFieldMapping.setDefinitionField(this.definitionField);
		reconciliationDefinitionFieldMapping.setSource(this.source);
		reconciliationDefinitionFieldMapping.setEvaluationOrder(this.evaluationOrder);
		return reconciliationDefinitionFieldMapping;
	}
}
