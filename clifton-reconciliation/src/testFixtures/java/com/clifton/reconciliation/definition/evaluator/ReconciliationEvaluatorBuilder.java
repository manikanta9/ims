package com.clifton.reconciliation.definition.evaluator;

import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.rule.ReconciliationRuleCondition;
import com.clifton.system.bean.SystemBean;

import java.util.Date;


/**
 * @author TerryS
 */
public final class ReconciliationEvaluatorBuilder {

	int evaluationOrder;
	private Date startDate;
	private Date endDate;
	private String name;
	private ReconciliationDefinitionField definitionField;
	private ReconciliationInvestmentAccount reconciliationInvestmentAccount;
	private ReconciliationInvestmentSecurity reconciliationInvestmentSecurity;
	private ReconciliationRuleCondition conditionRule;
	private SystemBean evaluatorBean;


	private ReconciliationEvaluatorBuilder() {
	}


	public static ReconciliationEvaluatorBuilder aReconciliationEvaluator() {
		return new ReconciliationEvaluatorBuilder();
	}


	public ReconciliationEvaluatorBuilder withEvaluationOrder(int evaluationOrder) {
		this.evaluationOrder = evaluationOrder;
		return this;
	}


	public ReconciliationEvaluatorBuilder withStartDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}


	public ReconciliationEvaluatorBuilder withEndDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}


	public ReconciliationEvaluatorBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationEvaluatorBuilder withDefinitionField(ReconciliationDefinitionField definitionField) {
		this.definitionField = definitionField;
		return this;
	}


	public ReconciliationEvaluatorBuilder withReconciliationInvestmentAccount(ReconciliationInvestmentAccount reconciliationInvestmentAccount) {
		this.reconciliationInvestmentAccount = reconciliationInvestmentAccount;
		return this;
	}


	public ReconciliationEvaluatorBuilder withReconciliationInvestmentSecurity(ReconciliationInvestmentSecurity reconciliationInvestmentSecurity) {
		this.reconciliationInvestmentSecurity = reconciliationInvestmentSecurity;
		return this;
	}


	public ReconciliationEvaluatorBuilder withConditionRule(ReconciliationRuleCondition conditionRule) {
		this.conditionRule = conditionRule;
		return this;
	}


	public ReconciliationEvaluatorBuilder withEvaluatorBean(SystemBean evaluatorBean) {
		this.evaluatorBean = evaluatorBean;
		return this;
	}


	public ReconciliationEvaluator build() {
		ReconciliationEvaluator reconciliationEvaluator = new ReconciliationEvaluator();
		reconciliationEvaluator.setEvaluationOrder(this.evaluationOrder);
		reconciliationEvaluator.setStartDate(this.startDate);
		reconciliationEvaluator.setEndDate(this.endDate);
		reconciliationEvaluator.setName(this.name);
		reconciliationEvaluator.setDefinitionField(this.definitionField);
		reconciliationEvaluator.setReconciliationInvestmentAccount(this.reconciliationInvestmentAccount);
		reconciliationEvaluator.setReconciliationInvestmentSecurity(this.reconciliationInvestmentSecurity);
		reconciliationEvaluator.setConditionRule(this.conditionRule);
		reconciliationEvaluator.setEvaluatorBean(this.evaluatorBean);
		return reconciliationEvaluator;
	}
}
