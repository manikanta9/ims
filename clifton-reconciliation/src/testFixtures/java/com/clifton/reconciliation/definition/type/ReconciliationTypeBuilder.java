package com.clifton.reconciliation.definition.type;

import com.clifton.system.condition.SystemCondition;

import java.util.List;


/**
 * @author terrys
 */
public final class ReconciliationTypeBuilder {

	private String label;
	private String name;
	private String description;
	private boolean cumulativeReconciliation;
	private SystemCondition runApprovalSystemCondition;
	private SystemCondition runModifySystemCondition;
	private List<ReconciliationTypeRequiredNaturalKey> requiredNaturalKeyList;


	private ReconciliationTypeBuilder() {
	}


	public static ReconciliationTypeBuilder aReconciliationType() {
		return new ReconciliationTypeBuilder();
	}


	public ReconciliationTypeBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public ReconciliationTypeBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationTypeBuilder withCumulativeReconciliation(boolean cumulativeReconciliation) {
		this.cumulativeReconciliation = cumulativeReconciliation;
		return this;
	}


	public ReconciliationTypeBuilder withRunApprovalSystemCondition(SystemCondition runApprovalSystemCondition) {
		this.runApprovalSystemCondition = runApprovalSystemCondition;
		return this;
	}


	public ReconciliationTypeBuilder withRunModifySystemCondition(SystemCondition runModifySystemCondition) {
		this.runModifySystemCondition = runModifySystemCondition;
		return this;
	}


	public ReconciliationTypeBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public ReconciliationTypeBuilder withRequiredNaturalKeyList(List<ReconciliationTypeRequiredNaturalKey> requiredNaturalKeyList) {
		this.requiredNaturalKeyList = requiredNaturalKeyList;
		return this;
	}


	public ReconciliationType build() {
		ReconciliationType reconciliationType = new ReconciliationType();
		reconciliationType.setLabel(this.label);
		reconciliationType.setName(this.name);
		reconciliationType.setDescription(this.description);
		reconciliationType.setCumulativeReconciliation(this.cumulativeReconciliation);
		reconciliationType.setRunApprovalSystemCondition(this.runApprovalSystemCondition);
		reconciliationType.setRunModifySystemCondition(this.runModifySystemCondition);
		reconciliationType.setRequiredNaturalKeyList(this.requiredNaturalKeyList);
		return reconciliationType;
	}
}
