package com.clifton.reconciliation.definition.rule;

import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.rule.ReconciliationRuleCondition;

import java.util.Date;


/**
 * @author TerryS
 */
public final class ReconciliationFilterBuilder {

	private String name;
	private boolean include;
	private boolean doNotSaveFilteredData;
	private Date startDate;
	private Date endDate;
	private ReconciliationDefinition reconciliationDefinition;
	private ReconciliationSource reconciliationSource;
	private ReconciliationRuleCondition conditionRule;


	private ReconciliationFilterBuilder() {
	}


	public static ReconciliationFilterBuilder aReconciliationFilter() {
		return new ReconciliationFilterBuilder();
	}


	public ReconciliationFilterBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationFilterBuilder withInclude(boolean include) {
		this.include = include;
		return this;
	}


	public ReconciliationFilterBuilder withDoNotSaveFilteredData(boolean doNotSaveFilteredData) {
		this.doNotSaveFilteredData = doNotSaveFilteredData;
		return this;
	}


	public ReconciliationFilterBuilder withStartDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}


	public ReconciliationFilterBuilder withEndDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}


	public ReconciliationFilterBuilder withReconciliationDefinition(ReconciliationDefinition reconciliationDefinition) {
		this.reconciliationDefinition = reconciliationDefinition;
		return this;
	}


	public ReconciliationFilterBuilder withReconciliationSource(ReconciliationSource reconciliationSource) {
		this.reconciliationSource = reconciliationSource;
		return this;
	}


	public ReconciliationFilterBuilder withConditionRule(ReconciliationRuleCondition conditionRule) {
		this.conditionRule = conditionRule;
		return this;
	}


	public ReconciliationFilter build() {
		ReconciliationFilter reconciliationFilter = new ReconciliationFilter();
		reconciliationFilter.setName(this.name);
		reconciliationFilter.setInclude(this.include);
		reconciliationFilter.setDoNotSaveFilteredData(this.doNotSaveFilteredData);
		reconciliationFilter.setStartDate(this.startDate);
		reconciliationFilter.setEndDate(this.endDate);
		reconciliationFilter.setReconciliationDefinition(this.reconciliationDefinition);
		reconciliationFilter.setReconciliationSource(this.reconciliationSource);
		reconciliationFilter.setConditionRule(this.conditionRule);
		return reconciliationFilter;
	}
}
