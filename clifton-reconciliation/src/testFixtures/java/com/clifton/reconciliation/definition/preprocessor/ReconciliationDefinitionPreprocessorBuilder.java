package com.clifton.reconciliation.definition.preprocessor;

import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.system.bean.SystemBean;


/**
 * @author TerryS
 */
public final class ReconciliationDefinitionPreprocessorBuilder {

	private ReconciliationDefinition reconciliationDefinition;
	private SystemBean preprocessor;
	private int evaluationOrder;


	private ReconciliationDefinitionPreprocessorBuilder() {
	}


	public static ReconciliationDefinitionPreprocessorBuilder aReconciliationDefinitionPreprocessor() {
		return new ReconciliationDefinitionPreprocessorBuilder();
	}


	public ReconciliationDefinitionPreprocessorBuilder withReconciliationDefinition(ReconciliationDefinition reconciliationDefinition) {
		this.reconciliationDefinition = reconciliationDefinition;
		return this;
	}


	public ReconciliationDefinitionPreprocessorBuilder withPreprocessor(SystemBean preprocessor) {
		this.preprocessor = preprocessor;
		return this;
	}


	public ReconciliationDefinitionPreprocessorBuilder withEvaluationOrder(int evaluationOrder) {
		this.evaluationOrder = evaluationOrder;
		return this;
	}


	public ReconciliationDefinitionPreprocessor build() {
		ReconciliationDefinitionPreprocessor reconciliationDefinitionPreprocessor = new ReconciliationDefinitionPreprocessor();
		reconciliationDefinitionPreprocessor.setReconciliationDefinition(this.reconciliationDefinition);
		reconciliationDefinitionPreprocessor.setPreprocessor(this.preprocessor);
		reconciliationDefinitionPreprocessor.setEvaluationOrder(this.evaluationOrder);
		return reconciliationDefinitionPreprocessor;
	}
}
