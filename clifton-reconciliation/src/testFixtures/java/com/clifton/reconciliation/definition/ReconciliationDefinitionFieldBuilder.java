package com.clifton.reconciliation.definition;

import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluator;

import java.util.Date;
import java.util.List;


/**
 * @author TerryS
 */
public final class ReconciliationDefinitionFieldBuilder {

	private ReconciliationField field;
	private Integer displayOrder;
	private Date startDate;
	private Date endDate;
	private boolean hidden;
	private boolean naturalKey;
	private boolean reconcilable;
	private boolean adjustable;
	private boolean aggregate;
	private DataTypes dataType;
	private Integer displayWidth;
	private ReconciliationDefinition reconciliationDefinition;
	private List<ReconciliationDefinitionFieldMapping> reconciliationDefinitionFieldMappingList;
	private List<ReconciliationEvaluator> reconciliationEvaluatorList;


	private ReconciliationDefinitionFieldBuilder() {
	}


	public static ReconciliationDefinitionFieldBuilder aReconciliationDefinitionField() {
		return new ReconciliationDefinitionFieldBuilder();
	}


	public ReconciliationDefinitionFieldBuilder withField(ReconciliationField field) {
		this.field = field;
		return this;
	}


	public ReconciliationDefinitionFieldBuilder withDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
		return this;
	}


	public ReconciliationDefinitionFieldBuilder withStartDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}


	public ReconciliationDefinitionFieldBuilder withEndDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}


	public ReconciliationDefinitionFieldBuilder withHidden(boolean hidden) {
		this.hidden = hidden;
		return this;
	}


	public ReconciliationDefinitionFieldBuilder withNaturalKey(boolean naturalKey) {
		this.naturalKey = naturalKey;
		return this;
	}


	public ReconciliationDefinitionFieldBuilder withReconcilable(boolean reconcilable) {
		this.reconcilable = reconcilable;
		return this;
	}


	public ReconciliationDefinitionFieldBuilder withAdjustable(boolean adjustable) {
		this.adjustable = adjustable;
		return this;
	}


	public ReconciliationDefinitionFieldBuilder withAggregate(boolean aggregate) {
		this.aggregate = aggregate;
		return this;
	}


	public ReconciliationDefinitionFieldBuilder withDataType(DataTypes dataType) {
		this.dataType = dataType;
		return this;
	}


	public ReconciliationDefinitionFieldBuilder withDisplayWidth(Integer displayWidth) {
		this.displayWidth = displayWidth;
		return this;
	}


	public ReconciliationDefinitionFieldBuilder withReconciliationDefinition(ReconciliationDefinition reconciliationDefinition) {
		this.reconciliationDefinition = reconciliationDefinition;
		return this;
	}


	public ReconciliationDefinitionFieldBuilder withReconciliationDefinitionFieldMappingList(List<ReconciliationDefinitionFieldMapping> reconciliationDefinitionFieldMappingList) {
		this.reconciliationDefinitionFieldMappingList = reconciliationDefinitionFieldMappingList;
		return this;
	}


	public ReconciliationDefinitionFieldBuilder withReconciliationEvaluatorList(List<ReconciliationEvaluator> reconciliationEvaluatorList) {
		this.reconciliationEvaluatorList = reconciliationEvaluatorList;
		return this;
	}


	public ReconciliationDefinitionField build() {
		ReconciliationDefinitionField reconciliationDefinitionField = new ReconciliationDefinitionField();
		reconciliationDefinitionField.setField(this.field);
		reconciliationDefinitionField.setDisplayOrder(this.displayOrder);
		reconciliationDefinitionField.setStartDate(this.startDate);
		reconciliationDefinitionField.setEndDate(this.endDate);
		reconciliationDefinitionField.setHidden(this.hidden);
		reconciliationDefinitionField.setNaturalKey(this.naturalKey);
		reconciliationDefinitionField.setReconcilable(this.reconcilable);
		reconciliationDefinitionField.setAdjustable(this.adjustable);
		reconciliationDefinitionField.setAggregate(this.aggregate);
		reconciliationDefinitionField.setDataType(this.dataType);
		reconciliationDefinitionField.setDisplayWidth(this.displayWidth);
		reconciliationDefinitionField.setReconciliationDefinition(this.reconciliationDefinition);
		reconciliationDefinitionField.setReconciliationDefinitionFieldMappingList(this.reconciliationDefinitionFieldMappingList);
		reconciliationDefinitionField.setReconciliationEvaluatorList(this.reconciliationEvaluatorList);
		return reconciliationDefinitionField;
	}
}
