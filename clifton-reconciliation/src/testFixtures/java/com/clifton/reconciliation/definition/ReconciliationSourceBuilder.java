package com.clifton.reconciliation.definition;

/**
 * @author TerryS
 */
public final class ReconciliationSourceBuilder {

	private String name;


	private ReconciliationSourceBuilder() {
	}


	public static ReconciliationSourceBuilder aReconciliationSource() {
		return new ReconciliationSourceBuilder();
	}


	public ReconciliationSourceBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationSource build() {
		ReconciliationSource reconciliationSource = new ReconciliationSource();
		reconciliationSource.setName(this.name);
		return reconciliationSource;
	}
}
