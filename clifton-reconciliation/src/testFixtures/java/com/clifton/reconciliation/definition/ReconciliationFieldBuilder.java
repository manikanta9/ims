package com.clifton.reconciliation.definition;

import com.clifton.core.shared.dataaccess.DataTypes;


/**
 * @author TerryS
 */
public final class ReconciliationFieldBuilder {

	private String label;
	private String name;
	private String description;
	private DataTypes defaultDataType;
	private int defaultDisplayWidth;


	private ReconciliationFieldBuilder() {
	}


	public static ReconciliationFieldBuilder aReconciliationField() {
		return new ReconciliationFieldBuilder();
	}


	public ReconciliationFieldBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public ReconciliationFieldBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationFieldBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public ReconciliationFieldBuilder withDefaultDataType(DataTypes defaultDataType) {
		this.defaultDataType = defaultDataType;
		return this;
	}


	public ReconciliationFieldBuilder withDefaultDisplayWidth(int defaultDisplayWidth) {
		this.defaultDisplayWidth = defaultDisplayWidth;
		return this;
	}


	public ReconciliationField build() {
		ReconciliationField reconciliationField = new ReconciliationField();
		reconciliationField.setLabel(this.label);
		reconciliationField.setName(this.name);
		reconciliationField.setDescription(this.description);
		reconciliationField.setDefaultDataType(this.defaultDataType);
		reconciliationField.setDefaultDisplayWidth(this.defaultDisplayWidth);
		return reconciliationField;
	}
}
