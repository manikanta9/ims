package com.clifton.reconciliation.file;

import com.clifton.reconciliation.definition.ReconciliationSource;

import java.util.List;


/**
 * @author TerryS
 */
public final class ReconciliationFileDefinitionBuilder {

	private String label;
	private String name;
	private String description;
	private String fileName;
	private ReconciliationSource source;
	private boolean saveOrphanedData;
	private List<ReconciliationFileDefinitionMapping> fileDefinitionMappingList;


	private ReconciliationFileDefinitionBuilder() {
	}


	public static ReconciliationFileDefinitionBuilder aReconciliationFileDefinition() {
		return new ReconciliationFileDefinitionBuilder();
	}


	public ReconciliationFileDefinitionBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public ReconciliationFileDefinitionBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationFileDefinitionBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public ReconciliationFileDefinitionBuilder withFileName(String fileName) {
		this.fileName = fileName;
		return this;
	}


	public ReconciliationFileDefinitionBuilder withSource(ReconciliationSource source) {
		this.source = source;
		return this;
	}


	public ReconciliationFileDefinitionBuilder withSaveOrphanedData(boolean saveOrphanedData) {
		this.saveOrphanedData = saveOrphanedData;
		return this;
	}


	public ReconciliationFileDefinitionBuilder withFileDefinitionMappingList(List<ReconciliationFileDefinitionMapping> fileDefinitionMappingList) {
		this.fileDefinitionMappingList = fileDefinitionMappingList;
		return this;
	}


	public ReconciliationFileDefinition build() {
		ReconciliationFileDefinition reconciliationFileDefinition = new ReconciliationFileDefinition();
		reconciliationFileDefinition.setLabel(this.label);
		reconciliationFileDefinition.setName(this.name);
		reconciliationFileDefinition.setDescription(this.description);
		reconciliationFileDefinition.setFileName(this.fileName);
		reconciliationFileDefinition.setSource(this.source);
		reconciliationFileDefinition.setSaveOrphanedData(this.saveOrphanedData);
		reconciliationFileDefinition.setFileDefinitionMappingList(this.fileDefinitionMappingList);
		return reconciliationFileDefinition;
	}
}
