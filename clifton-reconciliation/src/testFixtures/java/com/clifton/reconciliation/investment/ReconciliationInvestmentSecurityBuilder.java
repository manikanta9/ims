package com.clifton.reconciliation.investment;

/**
 * @author TerryS
 */
public final class ReconciliationInvestmentSecurityBuilder {

	private Integer securityIdentifier;
	private String securityName;
	private String securitySymbol;
	private String securityCusip;
	private String securityIsin;
	private String securitySedol;
	private String securityOccSymbol;
	private String securityTypeName;
	private String securityTypeSubTypeName;
	private String securityTypeSubType2Name;


	private ReconciliationInvestmentSecurityBuilder() {
	}


	public static ReconciliationInvestmentSecurityBuilder aReconciliationInvestmentSecurity() {
		return new ReconciliationInvestmentSecurityBuilder();
	}


	public ReconciliationInvestmentSecurityBuilder withSecurityIdentifier(Integer securityIdentifier) {
		this.securityIdentifier = securityIdentifier;
		return this;
	}


	public ReconciliationInvestmentSecurityBuilder withSecurityName(String securityName) {
		this.securityName = securityName;
		return this;
	}


	public ReconciliationInvestmentSecurityBuilder withSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
		return this;
	}


	public ReconciliationInvestmentSecurityBuilder withSecurityCusip(String securityCusip) {
		this.securityCusip = securityCusip;
		return this;
	}


	public ReconciliationInvestmentSecurityBuilder withSecurityIsin(String securityIsin) {
		this.securityIsin = securityIsin;
		return this;
	}


	public ReconciliationInvestmentSecurityBuilder withSecuritySedol(String securitySedol) {
		this.securitySedol = securitySedol;
		return this;
	}


	public ReconciliationInvestmentSecurityBuilder withSecurityOccSymbol(String securityOccSymbol) {
		this.securityOccSymbol = securityOccSymbol;
		return this;
	}


	public ReconciliationInvestmentSecurityBuilder withSecurityTypeName(String securityTypeName) {
		this.securityTypeName = securityTypeName;
		return this;
	}


	public ReconciliationInvestmentSecurityBuilder withSecurityTypeSubTypeName(String securityTypeSubTypeName) {
		this.securityTypeSubTypeName = securityTypeSubTypeName;
		return this;
	}


	public ReconciliationInvestmentSecurityBuilder withSecurityTypeSubType2Name(String securityTypeSubType2Name) {
		this.securityTypeSubType2Name = securityTypeSubType2Name;
		return this;
	}


	public ReconciliationInvestmentSecurity build() {
		ReconciliationInvestmentSecurity reconciliationInvestmentSecurity = new ReconciliationInvestmentSecurity();
		reconciliationInvestmentSecurity.setSecurityIdentifier(this.securityIdentifier);
		reconciliationInvestmentSecurity.setSecurityName(this.securityName);
		reconciliationInvestmentSecurity.setSecuritySymbol(this.securitySymbol);
		reconciliationInvestmentSecurity.setSecurityCusip(this.securityCusip);
		reconciliationInvestmentSecurity.setSecurityIsin(this.securityIsin);
		reconciliationInvestmentSecurity.setSecuritySedol(this.securitySedol);
		reconciliationInvestmentSecurity.setSecurityOccSymbol(this.securityOccSymbol);
		reconciliationInvestmentSecurity.setSecurityTypeName(this.securityTypeName);
		reconciliationInvestmentSecurity.setSecurityTypeSubTypeName(this.securityTypeSubTypeName);
		reconciliationInvestmentSecurity.setSecurityTypeSubType2Name(this.securityTypeSubType2Name);
		return reconciliationInvestmentSecurity;
	}
}
