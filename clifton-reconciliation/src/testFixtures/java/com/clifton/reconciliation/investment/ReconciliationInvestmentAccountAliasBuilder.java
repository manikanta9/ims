package com.clifton.reconciliation.investment;

import com.clifton.reconciliation.definition.ReconciliationSource;

import java.util.Date;


/**
 * @author TerryS
 */
public final class ReconciliationInvestmentAccountAliasBuilder {

	private ReconciliationSource reconciliationSource;
	private ReconciliationInvestmentAccount investmentAccount;
	private String alias;
	private Date startDate;
	private Date endDate;


	private ReconciliationInvestmentAccountAliasBuilder() {
	}


	public static ReconciliationInvestmentAccountAliasBuilder aReconciliationInvestmentAccountAlias() {
		return new ReconciliationInvestmentAccountAliasBuilder();
	}


	public ReconciliationInvestmentAccountAliasBuilder withReconciliationSource(ReconciliationSource reconciliationSource) {
		this.reconciliationSource = reconciliationSource;
		return this;
	}


	public ReconciliationInvestmentAccountAliasBuilder withInvestmentAccount(ReconciliationInvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
		return this;
	}


	public ReconciliationInvestmentAccountAliasBuilder withAlias(String alias) {
		this.alias = alias;
		return this;
	}


	public ReconciliationInvestmentAccountAliasBuilder withStartDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}


	public ReconciliationInvestmentAccountAliasBuilder withEndDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}


	public ReconciliationInvestmentAccountAlias build() {
		ReconciliationInvestmentAccountAlias reconciliationInvestmentAccountAlias = new ReconciliationInvestmentAccountAlias();
		reconciliationInvestmentAccountAlias.setReconciliationSource(this.reconciliationSource);
		reconciliationInvestmentAccountAlias.setInvestmentAccount(this.investmentAccount);
		reconciliationInvestmentAccountAlias.setAlias(this.alias);
		reconciliationInvestmentAccountAlias.setStartDate(this.startDate);
		reconciliationInvestmentAccountAlias.setEndDate(this.endDate);
		return reconciliationInvestmentAccountAlias;
	}
}
