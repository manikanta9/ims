package com.clifton.reconciliation.investment;

import com.clifton.reconciliation.definition.ReconciliationSource;

import java.util.Date;


/**
 * @author TerryS
 */
public final class ReconciliationInvestmentSecurityAliasBuilder {

	private ReconciliationSource reconciliationSource;
	private ReconciliationInvestmentSecurity investmentSecurity;
	private String alias;
	private String displayProperty;
	private Date startDate;
	private Date endDate;


	private ReconciliationInvestmentSecurityAliasBuilder() {
	}


	public static ReconciliationInvestmentSecurityAliasBuilder aReconciliationInvestmentSecurityAlias() {
		return new ReconciliationInvestmentSecurityAliasBuilder();
	}


	public ReconciliationInvestmentSecurityAliasBuilder withReconciliationSource(ReconciliationSource reconciliationSource) {
		this.reconciliationSource = reconciliationSource;
		return this;
	}


	public ReconciliationInvestmentSecurityAliasBuilder withInvestmentSecurity(ReconciliationInvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
		return this;
	}


	public ReconciliationInvestmentSecurityAliasBuilder withAlias(String alias) {
		this.alias = alias;
		return this;
	}


	public ReconciliationInvestmentSecurityAliasBuilder withDisplayProperty(String displayProperty) {
		this.displayProperty = displayProperty;
		return this;
	}


	public ReconciliationInvestmentSecurityAliasBuilder withStartDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}


	public ReconciliationInvestmentSecurityAliasBuilder withEndDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}


	public ReconciliationInvestmentSecurityAlias build() {
		ReconciliationInvestmentSecurityAlias reconciliationInvestmentSecurityAlias = new ReconciliationInvestmentSecurityAlias();
		reconciliationInvestmentSecurityAlias.setReconciliationSource(this.reconciliationSource);
		reconciliationInvestmentSecurityAlias.setInvestmentSecurity(this.investmentSecurity);
		reconciliationInvestmentSecurityAlias.setAlias(this.alias);
		reconciliationInvestmentSecurityAlias.setDisplayProperty(this.displayProperty);
		reconciliationInvestmentSecurityAlias.setStartDate(this.startDate);
		reconciliationInvestmentSecurityAlias.setEndDate(this.endDate);
		return reconciliationInvestmentSecurityAlias;
	}
}
