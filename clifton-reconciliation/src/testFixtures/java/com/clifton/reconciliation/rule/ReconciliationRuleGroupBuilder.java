package com.clifton.reconciliation.rule;

import java.util.List;


/**
 * @author TerryS
 */
public final class ReconciliationRuleGroupBuilder {

	private String name;
	private String description;
	private List<ReconciliationRule<?>> reconciliationRuleList;


	private ReconciliationRuleGroupBuilder() {
	}


	public static ReconciliationRuleGroupBuilder aReconciliationRuleGroup() {
		return new ReconciliationRuleGroupBuilder();
	}


	public ReconciliationRuleGroupBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationRuleGroupBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public ReconciliationRuleGroupBuilder withReconciliationRuleList(List<ReconciliationRule<?>> reconciliationRuleList) {
		this.reconciliationRuleList = reconciliationRuleList;
		return this;
	}


	public ReconciliationRuleGroup build() {
		ReconciliationRuleGroup reconciliationRuleGroup = new ReconciliationRuleGroup();
		reconciliationRuleGroup.setName(this.name);
		reconciliationRuleGroup.setDescription(this.description);
		reconciliationRuleGroup.setReconciliationRuleList(this.reconciliationRuleList);
		return reconciliationRuleGroup;
	}
}
