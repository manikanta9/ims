package com.clifton.reconciliation.rule;

import com.clifton.core.json.function.JsonFunction;


/**
 * @author TerryS
 */
public final class ReconciliationRuleFunctionBuilder {

	private String name;
	private String description;
	private ReconciliationRuleGroup ruleGroup;
	private ReconciliationRuleTypes ruleType;
	private JsonFunction jsonExpression;


	private ReconciliationRuleFunctionBuilder() {
	}


	public static ReconciliationRuleFunctionBuilder aReconciliationRuleFunction() {
		return new ReconciliationRuleFunctionBuilder();
	}


	public ReconciliationRuleFunctionBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationRuleFunctionBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public ReconciliationRuleFunctionBuilder withRuleGroup(ReconciliationRuleGroup ruleGroup) {
		this.ruleGroup = ruleGroup;
		return this;
	}


	public ReconciliationRuleFunctionBuilder withRuleType(ReconciliationRuleTypes ruleType) {
		this.ruleType = ruleType;
		return this;
	}


	public ReconciliationRuleFunctionBuilder withJsonExpression(JsonFunction jsonExpression) {
		this.jsonExpression = jsonExpression;
		return this;
	}


	public ReconciliationRuleFunction build() {
		ReconciliationRuleFunction reconciliationRuleFunction = new ReconciliationRuleFunction();
		reconciliationRuleFunction.setName(this.name);
		reconciliationRuleFunction.setDescription(this.description);
		reconciliationRuleFunction.setRuleGroup(this.ruleGroup);
		reconciliationRuleFunction.setRuleType(this.ruleType);
		reconciliationRuleFunction.setJsonExpression(this.jsonExpression);
		return reconciliationRuleFunction;
	}
}
