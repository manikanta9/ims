package com.clifton.reconciliation.rule;

import com.clifton.core.json.condition.JsonCondition;


/**
 * @author TerryS
 */
public final class ReconciliationRuleConditionBuilder {

	private String name;
	private String description;
	private ReconciliationRuleGroup ruleGroup;
	private ReconciliationRuleTypes ruleType;
	private JsonCondition jsonExpression;


	private ReconciliationRuleConditionBuilder() {
	}


	public static ReconciliationRuleConditionBuilder aReconciliationRuleCondition() {
		return new ReconciliationRuleConditionBuilder();
	}


	public ReconciliationRuleConditionBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconciliationRuleConditionBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public ReconciliationRuleConditionBuilder withRuleGroup(ReconciliationRuleGroup ruleGroup) {
		this.ruleGroup = ruleGroup;
		return this;
	}


	public ReconciliationRuleConditionBuilder withRuleType(ReconciliationRuleTypes ruleType) {
		this.ruleType = ruleType;
		return this;
	}


	public ReconciliationRuleConditionBuilder withJsonExpression(JsonCondition jsonExpression) {
		this.jsonExpression = jsonExpression;
		return this;
	}


	public ReconciliationRuleCondition build() {
		ReconciliationRuleCondition reconciliationRuleCondition = new ReconciliationRuleCondition();
		reconciliationRuleCondition.setName(this.name);
		reconciliationRuleCondition.setDescription(this.description);
		reconciliationRuleCondition.setRuleGroup(this.ruleGroup);
		reconciliationRuleCondition.setRuleType(this.ruleType);
		reconciliationRuleCondition.setJsonExpression(this.jsonExpression);
		return reconciliationRuleCondition;
	}
}
