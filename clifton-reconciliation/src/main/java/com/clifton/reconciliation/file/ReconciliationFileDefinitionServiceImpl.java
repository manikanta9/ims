package com.clifton.reconciliation.file;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.reconciliation.file.search.ReconciliationFileDefinitionMappingSearchForm;
import com.clifton.reconciliation.file.search.ReconciliationFileDefinitionSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.IntegerType;
import org.hibernate.type.Type;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class ReconciliationFileDefinitionServiceImpl implements ReconciliationFileDefinitionService {

	private DaoNamedEntityCache<ReconciliationFileDefinition> reconciliationFileDefinitionCache;
	private AdvancedUpdatableDAO<ReconciliationFileDefinition, Criteria> reconciliationFileDefinitionDAO;
	private AdvancedUpdatableDAO<ReconciliationFileDefinitionMapping, Criteria> reconciliationFileDefinitionMappingDAO;

	////////////////////////////////////////////////////////////////////////////
	////////    Reconciliation Upload File Mapping Business Methods    /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationFileDefinition getReconciliationFileDefinitionByName(String name) {
		return getReconciliationFileDefinitionCache().getBeanForKeyValueStrict(getReconciliationFileDefinitionDAO(), name);
	}


	@Override
	public ReconciliationFileDefinition getReconciliationFileDefinition(int id) {
		ReconciliationFileDefinition fileDefinition = getReconciliationFileDefinitionDAO().findByPrimaryKey(id);
		if (fileDefinition != null) {
			//Populate key list
			fileDefinition.setFileDefinitionMappingList(getReconciliationFileDefinitionMappingListByFileDefinitionId(fileDefinition.getId()));
		}
		return fileDefinition;
	}


	@Override
	public List<ReconciliationFileDefinition> getReconciliationFileDefinitionList(ReconciliationFileDefinitionSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getDefinitionId() != null) {
					criteria.add(Restrictions.sqlRestriction("(ReconciliationFileDefinitionID IN (SELECT ReconciliationFileDefinitionID FROM ReconciliationFileDefinitionMapping WHERE ReconciliationDefinitionID = ?))", new Object[]{searchForm.getDefinitionId()}, new Type[]{new IntegerType()}));
				}
			}
		};
		return getReconciliationFileDefinitionDAO().findBySearchCriteria(config);
	}


	@Override
	public List<ReconciliationFileDefinition> getReconciliationFileDefinitionListPopulated(ReconciliationFileDefinitionSearchForm searchForm) {
		List<ReconciliationFileDefinition> fileDefinitionList = getReconciliationFileDefinitionList(searchForm);
		for (ReconciliationFileDefinition fileDefinition : CollectionUtils.getIterable(fileDefinitionList)) {
			fileDefinition.setFileDefinitionMappingList(getReconciliationFileDefinitionMappingListByFileDefinitionId(fileDefinition.getId()));
		}
		return fileDefinitionList;
	}


	/**
	 * Because during json migrations the fileDefinitionMappings are not yet fully hydrated (since the definitions have not be walked)
	 * we want to by default not save the mappings, they will be saved when the are individually traversed.
	 * From the UI we call 'saveReconciliationFieldDefinitionPopulated'.
	 */
	@Override
	public ReconciliationFileDefinition saveReconciliationFileDefinition(ReconciliationFileDefinition reconciliationFileDefinition) {
		List<ReconciliationFileDefinitionMapping> fileDefinitionMappingList = reconciliationFileDefinition.getFileDefinitionMappingList();
		reconciliationFileDefinition = getReconciliationFileDefinitionDAO().save(reconciliationFileDefinition);
		reconciliationFileDefinition.setFileDefinitionMappingList(fileDefinitionMappingList);
		return reconciliationFileDefinition;
	}


	@Override
	@Transactional
	public ReconciliationFileDefinition saveReconciliationFileDefinitionPopulated(ReconciliationFileDefinition reconciliationFileDefinition) {
		reconciliationFileDefinition = saveReconciliationFileDefinition(reconciliationFileDefinition);
		List<ReconciliationFileDefinitionMapping> fileDefinitionMappingList = reconciliationFileDefinition.getFileDefinitionMappingList();
		for (ReconciliationFileDefinitionMapping reconciliationFileDefinitionMapping : CollectionUtils.getIterable(fileDefinitionMappingList)) {
			reconciliationFileDefinitionMapping.setFileDefinition(reconciliationFileDefinition);
		}
		getReconciliationFileDefinitionMappingDAO().saveList(fileDefinitionMappingList, getReconciliationFileDefinitionMappingListByFileDefinitionId(reconciliationFileDefinition.getId()));
		reconciliationFileDefinition.setFileDefinitionMappingList(fileDefinitionMappingList);
		return reconciliationFileDefinition;
	}


	@Override
	public void deleteReconciliationFileDefinition(int id) {
		getReconciliationFileDefinitionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///    Reconciliation Upload File Mapping Definition Business Methods    ///
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationFileDefinitionMapping getReconciliationFileDefinitionMapping(int id) {
		return getReconciliationFileDefinitionMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconciliationFileDefinitionMapping> getReconciliationFileDefinitionMappingList(ReconciliationFileDefinitionMappingSearchForm searchForm) {
		return getReconciliationFileDefinitionMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<ReconciliationFileDefinitionMapping> getReconciliationFileDefinitionMappingListByFileDefinitionId(int fileDefinitionId) {
		return getReconciliationFileDefinitionMappingDAO().findByField("fileDefinition.id", fileDefinitionId);
	}


	@Override
	public ReconciliationFileDefinitionMapping saveReconciliationFileDefinitionMapping(ReconciliationFileDefinitionMapping reconciliationFileDefinitionMapping) {
		return getReconciliationFileDefinitionMappingDAO().save(reconciliationFileDefinitionMapping);
	}


	@Override
	public void deleteReconciliationFileDefinitionMapping(int id) {
		getReconciliationFileDefinitionMappingDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public DaoNamedEntityCache<ReconciliationFileDefinition> getReconciliationFileDefinitionCache() {
		return this.reconciliationFileDefinitionCache;
	}


	public void setReconciliationFileDefinitionCache(DaoNamedEntityCache<ReconciliationFileDefinition> reconciliationFileDefinitionCache) {
		this.reconciliationFileDefinitionCache = reconciliationFileDefinitionCache;
	}


	public AdvancedUpdatableDAO<ReconciliationFileDefinition, Criteria> getReconciliationFileDefinitionDAO() {
		return this.reconciliationFileDefinitionDAO;
	}


	public void setReconciliationFileDefinitionDAO(AdvancedUpdatableDAO<ReconciliationFileDefinition, Criteria> reconciliationFileDefinitionDAO) {
		this.reconciliationFileDefinitionDAO = reconciliationFileDefinitionDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationFileDefinitionMapping, Criteria> getReconciliationFileDefinitionMappingDAO() {
		return this.reconciliationFileDefinitionMappingDAO;
	}


	public void setReconciliationFileDefinitionMappingDAO(AdvancedUpdatableDAO<ReconciliationFileDefinitionMapping, Criteria> reconciliationFileDefinitionMappingDAO) {
		this.reconciliationFileDefinitionMappingDAO = reconciliationFileDefinitionMappingDAO;
	}
}
