package com.clifton.reconciliation;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compression.GzipCompressionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectCategories;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectService;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectAccountData;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectData;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectDataType;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectJournalData;
import com.clifton.reconciliation.event.object.data.search.ReconciliationEventObjectDataTypeSearchForm;
import com.clifton.reconciliation.json.jackson.strategy.ReconciliationRunObjectStrategy;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.data.DataError;
import com.clifton.reconciliation.run.data.DataHolder;
import com.clifton.reconciliation.run.data.DataNote;
import com.clifton.reconciliation.run.data.DataObject;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationStatusService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author stevenf
 */
@Component
public class ReconciliationMigrationHandlerImpl implements ReconciliationMigrationHandler {

	private SqlHandler sqlHandler;
	private ReconciliationRunService reconciliationRunService;
	private ReconciliationStatusService reconciliationStatusService;
	private ReconciliationEventObjectService reconciliationEventObjectService;

	private static final String RECONCILED_COUNT_SQL = "SELECT rrs.StatusName, COUNT(*) StatusCount \n" +
			"FROM ReconciliationRunObject rro\n" +
			"INNER JOIN ReconciliationReconcileStatus rrs ON rrs.ReconciliationReconcileStatusID = rro.ReconciliationReconcileStatusID\n" +
			"WHERE ReconciliationRunID = ?\n" +
			"GROUP BY rrs.StatusName;";

	private static final String MATCHED_COUNT_SQL = "SELECT rrs.StatusName, COUNT(*) StatusCount \n" +
			"FROM ReconciliationRunObject rro\n" +
			"INNER JOIN ReconciliationMatchStatus rrs ON rrs.ReconciliationMatchStatusID = rro.ReconciliationMatchStatusID\n" +
			"WHERE ReconciliationRunID = ?\n" +
			"GROUP BY rrs.StatusName;";


	@Override
	@Transactional(timeout = 300)
	public void doReconciliationRunObjectUpdates() {
		long startTime = System.currentTimeMillis();
		System.out.println("\nExecuting Reconciliation Run Object Updates:\n");
		System.out.println("Querying...");

		Map<Long, String> identifierMap = new HashMap<>();
		Map<Long, String> dataHolderMap = new HashMap<>();
		ObjectMapper mapper = (new JacksonHandlerImpl(new ReconciliationRunObjectStrategy())).getObjectMapper();

		ResultSetExtractor<Map<Long, String>> resultSetExtractor = rs -> {
			System.out.println("Extracting...");
			System.out.flush();
			while (rs.next()) {
				try {
					long id = rs.getLong(1);
					identifierMap.put(id, rs.getString(2));
					dataHolderMap.put(id, GzipCompressionUtils.decompress(rs.getBytes(3)));
				}
				catch (Exception e) {
					throw new RuntimeException("Failed to extract results!", e);
				}
			}
			return dataHolderMap;
		};

		Set<Long> failures = new HashSet<>();
		getSqlHandler().executeSelect("SELECT ReconciliationRunObjectID, NaturalKeyIdentifier, DataHolder FROM ReconciliationRunObject", resultSetExtractor);
		System.out.println("Processing " + dataHolderMap.size() + " results...");
		dataHolderMap.entrySet().parallelStream().forEach(dataHolderEntry -> {
			try {
				String naturalKeyIdentifier = updateNaturalKeyIdentifier(identifierMap.get(dataHolderEntry.getKey()));
				DataHolder dataHolder = deserializeDataHolder(mapper, dataHolderEntry.getValue());
				if (dataHolder != null) {
					int updateStatus = getSqlHandler().executeUpdate("UPDATE ReconciliationRunObject SET NaturalKeyIdentifier = ?, DataHolder = ? WHERE ReconciliationRunObjectID = ?", null, (PreparedStatement ps) -> {
						try {
							ps.setString(1, naturalKeyIdentifier);
							ps.setBytes(2, GzipCompressionUtils.compress(mapper.writeValueAsBytes(dataHolder)));
							ps.setLong(3, dataHolderEntry.getKey());
							return ps.executeUpdate();
						}
						catch (Exception e) {
							return 0;
						}
					});
					if (updateStatus == 0) {
						throw new RuntimeException("Failed to update item with id [" + dataHolderEntry.getKey() + "]");
					}
				}
			}
			catch (ValidationException ve) {
				failures.add(dataHolderEntry.getKey());
			}
			catch (Exception e) {
				throw new RuntimeException("Failed to update item with id [" + dataHolderEntry.getKey() + "]", e);
			}
		});
		System.out.println("\nReconciliation Notes Migration Complete: " + (System.currentTimeMillis() - startTime) + " ms");
		System.out.println("Failures: " + failures);
	}


	/**
	 * Cleans up natural key identifiers by removing whitespace and using a short ISO date format.
	 *
	 * @param naturalKeyIdentifier
	 */
	private String updateNaturalKeyIdentifier(String naturalKeyIdentifier) {
		StringBuilder identifierSb = new StringBuilder();
		String[] keys = naturalKeyIdentifier.split("_");
		boolean hasContent = false;
		for (String key : keys) {
			if (hasContent) {
				identifierSb.append("_");
			}
			try {
				Date date = DateUtils.toDate(key, "EEE MMM dd HH:mm:ss zzz yyyy");
				key = DateUtils.fromDate(date, DateUtils.DATE_FORMAT_ISO_SIMPLE);
			}
			catch (Exception e) {
				//ignore
			}
			identifierSb.append(StringUtils.trimAll(key));
			if (!hasContent && !StringUtils.isEmpty(identifierSb)) {
				hasContent = true;
			}
		}
		return identifierSb.toString();
	}


	/**
	 * Version 1 of DataHolder JSON used up until May 2019 release
	 * This allows deserialization of existing data and converts to new notes format
	 * which removes notes from the child DataObjects, and consolidates them with an
	 * optional sourceId at the DataHolder level - this was done to improve performance
	 * and storage size.
	 */
	private DataHolder deserializeDataHolder(ObjectMapper mapper, String dataHolderJson) throws Exception {
		List<DataNote> noteList = new ArrayList<>();

		DataHolder dataHolder = new DataHolder();

		JsonNode node = mapper.readTree(dataHolderJson);
		JsonNode errors = node.get("errors");
		if (errors != null) {
			dataHolder.setErrors(mapper.readValue(errors.toString(), new TypeReference<List<DataError>>() {
			}));
		}
		ArrayNode dataObjectArrayNode = (ArrayNode) node.get("dataObjects");
		if (dataObjectArrayNode != null) {
			for (JsonNode dataObjectNode : dataObjectArrayNode) {
				DataObject dataObject = new DataObject();

				short sourceId = dataObjectNode.get("sourceId").shortValue();
				if (dataObjectNode.get("importDate") != null) {
					dataObject.setImportDate(DateUtils.toDate(dataObjectNode.get("importDate").asText(), DateUtils.DATE_FORMAT_SQL_PRECISE));
				}
				if (dataObjectNode.get("fileDefinitionId") != null) {
					dataObject.setFileDefinitionId(dataObjectNode.get("fileDefinitionId").intValue());
				}
				dataObject.setSourceId(sourceId);

				JsonNode dataObjectNotes = dataObjectNode.get("notes");
				if (dataObjectNotes != null) {
					List<DataNote> childNoteList = mapper.readValue(dataObjectNotes.toString(), new TypeReference<List<DataNote>>() {});
					for (DataNote childNote : CollectionUtils.getIterable(childNoteList)) {
						childNote.setSourceId(sourceId);
					}
					noteList.addAll(childNoteList);
				}

				JsonNode data = dataObjectNode.get("data");
				if (data != null) {
					dataObject.setData(mapper.readValue(data.toString(), new TypeReference<Map<String, Object>>() {
					}));
				}

				List<Map<String, Object>> rowData = new ArrayList<>();
				ArrayNode rowDataArrayNode = (ArrayNode) dataObjectNode.get("rowData");
				List<String> keyList = mapper.readValue(rowDataArrayNode.get(0).toString(), new TypeReference<List<String>>() {
				});
				for (int i = 1; i < rowDataArrayNode.size(); i++) {
					List<Object> valueList = mapper.readValue(rowDataArrayNode.get(i).toString(), new TypeReference<List<Object>>() {
					});
					Map<String, Object> rowDataMap = new HashMap<>();
					for (int j = 0; j < keyList.size(); j++) {
						rowDataMap.put(keyList.get(j), valueList.get(j));
					}
					rowData.add(rowDataMap);
				}
				dataObject.setRowData(rowData);
				dataHolder.getDataObjects().add(dataObject);
			}
		}
		else {
			throw new ValidationException("Failure!");
		}
		JsonNode notes = node.get("notes");
		if (notes != null) {
			noteList.addAll(mapper.readValue(notes.toString(), new TypeReference<List<DataNote>>() {}));
		}
		if (!CollectionUtils.isEmpty(noteList)) {
			List<DataNote> finalNoteList = new ArrayList<>();
			Map<String, List<DataNote>> duplicateNoteMap = noteList.stream().collect(Collectors.groupingBy(dataNote -> dataNote.getType().name() + "_" + dataNote.getNote()));
			for (Map.Entry<String, List<DataNote>> duplicateNoteEntry : duplicateNoteMap.entrySet()) {
				DataNote finalNote = duplicateNoteEntry.getValue().get(0);
				if (duplicateNoteEntry.getValue().size() > 1) {
					//This note had a duplicate, so we want to clear the sourceId and make it 'global' (applies to both sources)
					finalNote.setSourceId(null);
				}
				finalNoteList.add(duplicateNoteEntry.getValue().get(0));
			}
			dataHolder.setNotes(finalNoteList);
		}
		return dataHolder;
	}


	@Override
	public void doReconciliationEventObjectDataMigration() {
		long startTime = System.currentTimeMillis();
		System.out.println("\nExecuting Reconciliation Run Object Updates:\n");
		System.out.println("Querying...");

		Map<Long, String> eventObjectDataMap = new HashMap<>();
		Map<Long, Short> eventObjectDataTypeIdMap = new HashMap<>();
		ObjectMapper mapper = (new JacksonHandlerImpl(new ReconciliationRunObjectStrategy())).getObjectMapper();

		ResultSetExtractor<Map<Long, String>> resultSetExtractor = rs -> {
			System.out.println("Extracting...");
			System.out.flush();
			while (rs.next()) {
				try {
					long id = rs.getLong(1);
					eventObjectDataTypeIdMap.put(id, rs.getShort(2));
					eventObjectDataMap.put(id, GzipCompressionUtils.decompress(rs.getBytes(3)));
				}
				catch (Exception e) {
					throw new RuntimeException("Failed to extract results!", e);
				}
			}
			return eventObjectDataMap;
		};

		Map<Short, ReconciliationEventObjectDataType> eventObjectDataTypeMap = BeanUtils.getBeanMap(getReconciliationEventObjectService().getReconciliationEventObjectDataTypeList(new ReconciliationEventObjectDataTypeSearchForm()), BaseSimpleEntity::getId);
		Set<Long> failures = new HashSet<>();
		getSqlHandler().executeSelect("SELECT ReconciliationEventObjectID, ReconciliationEventObjectDataTypeID, EventObjectData FROM ReconciliationEventObject", resultSetExtractor);
		System.out.println("Processing " + eventObjectDataMap.size() + " results...");
		eventObjectDataMap.entrySet().parallelStream().forEach(eventObjectDataEntry -> {
			try {
				ReconciliationEventObjectDataType eventObjectDataType = eventObjectDataTypeMap.get(eventObjectDataTypeIdMap.get(eventObjectDataEntry.getKey()));
				ReconciliationEventObjectData eventObjectData = deserializeEventObjectData(mapper, eventObjectDataEntry.getValue(), eventObjectDataType);
				if (eventObjectData != null) {
					int updateStatus = getSqlHandler().executeUpdate("UPDATE ReconciliationEventObject SET EventObjectData = ? WHERE ReconciliationEventObjectID = ?", null, (PreparedStatement ps) -> {
						try {
							ps.setBytes(1, GzipCompressionUtils.compress(mapper.writeValueAsBytes(eventObjectData)));
							ps.setLong(2, eventObjectDataEntry.getKey());
							return ps.executeUpdate();
						}
						catch (Exception e) {
							return 0;
						}
					});
					if (updateStatus == 0) {
						throw new RuntimeException("Failed to update item with id [" + eventObjectDataEntry.getKey() + "]");
					}
				}
			}
			catch (ValidationException ve) {
				failures.add(eventObjectDataEntry.getKey());
			}
			catch (Exception e) {
				throw new RuntimeException("Failed to update item with id [" + eventObjectDataEntry.getKey() + "]", e);
			}
		});
		System.out.println("\nReconciliation Notes Migration Complete: " + (System.currentTimeMillis() - startTime) + " ms");
		System.out.println("Failures: " + failures);
	}


	private ReconciliationEventObjectData deserializeEventObjectData(ObjectMapper mapper, String eventObjectDataJson, ReconciliationEventObjectDataType eventObjectDataType) throws Exception {
		JsonNode node = mapper.readTree(eventObjectDataJson);
		ReconciliationEventObjectData eventObjectData;
		if (ReconciliationEventObjectCategories.ACCOUNT.name().equals(eventObjectDataType.getEventObjectCategory().name())) {
			ReconciliationEventObjectAccountData eventObjectAccountData = new ReconciliationEventObjectAccountData();
			eventObjectAccountData.setSecurityIdentifier(node.get("securityIdentifier") != null ? node.get("securityIdentifier").textValue() : null);
			eventObjectAccountData.setHoldingAccountNumber(node.get("accountNumber") != null ? node.get("accountNumber").textValue() : null);
			eventObjectAccountData.setSourceTable(node.get("sourceTable") != null ? node.get("sourceTable").textValue() : null);
			eventObjectAccountData.setSourceFkFieldId(node.get("sourceFkFieldId") != null ? node.get("sourceFkFieldId").longValue() : null);
			//Convert date to eventDate
			eventObjectAccountData.setEventDate(node.get("date") != null ? DateUtils.toDate(node.get("date").textValue(), DateUtils.DATE_FORMAT_SQL_PRECISE) : null);
			//Convert note to eventNote
			eventObjectAccountData.setEventNote(node.get("note") != null ? node.get("note").textValue() : null);
			eventObjectData = eventObjectAccountData;
		}
		else {
			ReconciliationEventObjectJournalData eventObjectJournalData = new ReconciliationEventObjectJournalData();
			eventObjectJournalData.setAmount(node.get("amount") != null ? node.get("amount").isBigDecimal() ? node.get("amount").decimalValue() : new BigDecimal(node.get("amount").textValue()) : null);
			eventObjectJournalData.setSourceTable(node.get("sourceTable") != null ? node.get("sourceTable").textValue() : null);
			eventObjectJournalData.setSourceFkFieldId(node.get("sourceFkFieldId") != null ? node.get("sourceFkFieldId").longValue() : null);
			eventObjectJournalData.setSettlementDate(node.get("settlementDate") != null ? DateUtils.toDate(node.get("settlementDate").textValue(), DateUtils.DATE_FORMAT_SQL_PRECISE) : null);
			eventObjectJournalData.setTransactionDate(node.get("transactionDate") != null ? DateUtils.toDate(node.get("transactionDate").textValue(), DateUtils.DATE_FORMAT_SQL_PRECISE) : null);
			eventObjectJournalData.setOriginalTransactionDate(node.get("originalTransactionDate") != null ? DateUtils.toDate(node.get("originalTransactionDate").textValue(), DateUtils.DATE_FORMAT_SQL_PRECISE) : null);
			//Convert holdingAccount to accountNumber
			eventObjectJournalData.setHoldingAccountNumber(node.get("holdingAccount") != null ? node.get("holdingAccount").textValue() : null);
			//Won't exist yet
			eventObjectJournalData.setEventDate(node.get("eventDate") != null ? DateUtils.toDate(node.get("eventDate").textValue(), DateUtils.DATE_FORMAT_SQL_PRECISE) : DateUtils.toDate(node.get("settlementDate").textValue(), DateUtils.DATE_FORMAT_SQL_PRECISE));
			//Convert note to eventNote
			eventObjectJournalData.setEventNote(node.get("note") != null ? node.get("note").textValue() : null);
			eventObjectData = eventObjectJournalData;
		}
		return eventObjectData;
	}


	@Override
	public void doReconciliationRunCountUpdates() {
		ReconciliationReconcileStatus manuallyReconciled = getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED);
		ReconciliationReconcileStatus reconciled = getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.RECONCILED);
		for (ReconciliationRun run : CollectionUtils.getIterable(getReconciliationRunService().getReconciliationRunList(new ReconciliationRunSearchForm()))) {
			Map<String, Integer> countMap = getCountMap(run.getId());
			int manuallyMatchedCount = countMap.get(ReconciliationMatchStatusTypes.MANUALLY_MATCHED.name());
			int matchedCount = countMap.get(ReconciliationMatchStatusTypes.MATCHED.name()) + manuallyMatchedCount;
			int notMatchedCount = countMap.get(ReconciliationMatchStatusTypes.NOT_MATCHED.name());
			int reconciledCount = countMap.get(ReconciliationReconcileStatusTypes.RECONCILED.name());
			int manuallyReconciledCount = countMap.get(ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED.name());
			int totalReconciledCount = reconciledCount + manuallyReconciledCount;
			int notReconciledCount = countMap.get(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name());
			int totalCount = totalReconciledCount + notReconciledCount;

			Status runDetails = run.getRunDetails();
			runDetails.setStatusCount(ReconciliationRun.RECONCILED_COUNT, reconciledCount);
			runDetails.setStatusCount(ReconciliationRun.MANUALLY_RECONCILED_COUNT, manuallyReconciledCount);
			runDetails.setStatusCount(ReconciliationRun.TOTAL_RECONCILED_COUNT, totalReconciledCount);
			runDetails.setStatusCount(ReconciliationRun.NOT_RECONCILED_COUNT, notReconciledCount);
			runDetails.setStatusCount(ReconciliationRun.MATCHED_COUNT, matchedCount);
			runDetails.setStatusCount(ReconciliationRun.NOT_MATCHED_COUNT, notMatchedCount);
			runDetails.setStatusCount(ReconciliationRun.TOTAL_COUNT, totalCount);

			if (notReconciledCount == 0) {
				if (manuallyReconciledCount > 0 || manuallyMatchedCount > 0) {
					run.setReconciliationReconcileStatus(manuallyReconciled);
				}
				else {
					run.setReconciliationReconcileStatus(reconciled);
				}
			}
			//Do we need to maintain the updateUser?
			getReconciliationRunService().saveReconciliationRun(run);
		}
	}


	private Map<String, Integer> getCountMap(int runId) {
		//Create and initialize the map with zeroes for all statuses - this prevents null pointer references later.
		Map<String, Integer> countMap = new HashMap<>();
		countMap.put(ReconciliationMatchStatusTypes.MATCHED.name(), 0);
		countMap.put(ReconciliationMatchStatusTypes.NOT_MATCHED.name(), 0);
		countMap.put(ReconciliationMatchStatusTypes.MANUALLY_MATCHED.name(), 0);
		countMap.put(ReconciliationReconcileStatusTypes.RECONCILED.name(), 0);
		countMap.put(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name(), 0);
		countMap.put(ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED.name(), 0);
		//Now retrieve real counts
		countMap.putAll(getCountMap(runId, RECONCILED_COUNT_SQL));
		countMap.putAll(getCountMap(runId, MATCHED_COUNT_SQL));
		return countMap;
	}


	private Map<String, Integer> getCountMap(int runId, String sql) {
		Map<String, Integer> countMap = new HashMap<>();
		PreparedStatementCreatorFactory statementFactory = new PreparedStatementCreatorFactory(sql);
		statementFactory.addParameter(new SqlParameter(Types.INTEGER));
		PreparedStatementCreator statementCreator = statementFactory.newPreparedStatementCreator(new Object[]{runId});
		return getSqlHandler().executeSelect(statementCreator, resultSet -> {
			while (resultSet.next()) {
				countMap.put(resultSet.getString("StatusName"), resultSet.getInt("StatusCount"));
			}
			return countMap;
		});
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public ReconciliationStatusService getReconciliationStatusService() {
		return this.reconciliationStatusService;
	}


	public void setReconciliationStatusService(ReconciliationStatusService reconciliationStatusService) {
		this.reconciliationStatusService = reconciliationStatusService;
	}


	public ReconciliationEventObjectService getReconciliationEventObjectService() {
		return this.reconciliationEventObjectService;
	}


	public void setReconciliationEventObjectService(ReconciliationEventObjectService reconciliationEventObjectService) {
		this.reconciliationEventObjectService = reconciliationEventObjectService;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}
}
