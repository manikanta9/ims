package com.clifton.reconciliation.definition.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.reconciliation.definition.ReconciliationDefinitionFieldMapping;
import org.springframework.stereotype.Component;


/**
 * @author jonathanr
 */
@Component
public class ReconciliationDefinitionFieldMappingListByDefinitionFieldCache extends SelfRegisteringSingleKeyDaoListCache<ReconciliationDefinitionFieldMapping, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "definitionField.id";
	}


	@Override
	protected Integer getBeanKeyValue(ReconciliationDefinitionFieldMapping bean) {
		if (bean.getDefinitionField() != null) {
			return bean.getDefinitionField().getId();
		}
		return null;
	}
}
