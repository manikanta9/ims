package com.clifton.reconciliation.definition.rule.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.rule.ReconciliationFilter;
import com.clifton.reconciliation.definition.rule.ReconciliationFilterService;
import com.clifton.reconciliation.definition.rule.search.ReconciliationFilterSearchForm;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>ReconciliationFilterValidator</code> validates ReconciliationFilter
 *
 * @author stevenf
 */
@Component
public class ReconciliationFilterValidator extends SelfRegisteringDaoValidator<ReconciliationFilter> {

	private ReconciliationFilterService reconciliationFilterService;
	private ReconciliationRunService reconciliationRunService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(ReconciliationFilter reconciliationFilter, DaoEventTypes config) throws ValidationException {
		if (reconciliationFilter == null) {
			throw new ValidationException("Filter may not be null.");
		}

		if (config.isDelete()) {
			List<ReconciliationRun> runList = getReconciliationRunList(reconciliationFilter);
			if (!CollectionUtils.isEmpty(runList)) {
				throw new ValidationException("Unable to delete the filter fields as there are existing reconciliation runs associated with it.<br />Please exclude or end the filter instead.");
			}
		}

		ReconciliationDefinition reconciliationDefinition = reconciliationFilter.getReconciliationDefinition();
		ValidationUtils.assertNotNull(reconciliationDefinition, "Reconciliation definition may not be null.");

		if (config.isInsert()) {
			//First check if the field conflicts or duplicates existing fields
			ReconciliationFilterSearchForm filterSearchForm = new ReconciliationFilterSearchForm();
			filterSearchForm.setName(reconciliationFilter.getName());
			filterSearchForm.setDefinitionId(reconciliationFilter.getReconciliationDefinition().getId());
			filterSearchForm.setActiveOnDate(reconciliationFilter.getStartDate());
			List<ReconciliationFilter> existingFilterList = getReconciliationFilterService().getReconciliationFilterList(filterSearchForm);
			if (!CollectionUtils.isEmpty(existingFilterList)) {
				throw new ValidationException("You may not save a filter with the same name, for the same definition, that overlaps the active date for an existing filter.  Please exclude or end the previous filter first.");
			}
			//Now ensure there are no runs during the active date of the new key
			List<ReconciliationRun> runList = getReconciliationRunList(reconciliationFilter);
			if (!CollectionUtils.isEmpty(runList)) {
				throw new ValidationException("You must delete any runs during the active date before adding a filter; or set the active date after the latest run associated with the definition.");
			}
		}
		else if (config.isUpdate()) {
			ReconciliationFilter existingFilter = getReconciliationFilterService().getReconciliationFilter(reconciliationFilter.getId());
			if (existingFilter.getId().equals(reconciliationFilter.getId())) {
				allowUpdate(existingFilter, reconciliationFilter);
			}
		}
	}


	/**
	 * We use the existing filter to find existing runs because we want to ensure historical fidelity.
	 * If existing runs are found then we need to ensure the startDate of the new filter is the same
	 * as the existing filter, then, if the endDate is set on the new filter we can exclude those
	 * runs from the list, then if no runs exist after filtering we allow the save.
	 */
	private void allowUpdate(ReconciliationFilter existingFilter, ReconciliationFilter newFilter) {
		List<ReconciliationRun> reconciliationRunList = getReconciliationRunList(existingFilter);
		if (!CollectionUtils.isEmpty(reconciliationRunList)) {
			if (!DateUtils.isEqualWithoutTime(existingFilter.getStartDate(), newFilter.getStartDate())) {
				throw new ValidationException("You may not change the startDate of a filter when there are existing runs for the current startDate!");
			}
			if (newFilter.getEndDate() != null) {
				reconciliationRunList = reconciliationRunList.stream().filter(run -> !DateUtils.isDateBeforeOrEqual(run.getRunDate(), newFilter.getEndDate(), false)).collect(Collectors.toList());
				//If no existing runs exist; then we can update any field
				if (!CollectionUtils.isEmpty(reconciliationRunList)) {
					throw new ValidationException("You may not modify a filter if there are existing runs associated with the filter's current active dates!");
				}
			}
		}
	}


	private List<ReconciliationRun> getReconciliationRunList(ReconciliationFilter reconciliationFilter) {
		ReconciliationRunSearchForm runSearchForm = new ReconciliationRunSearchForm();
		runSearchForm.setDefinitionId(reconciliationFilter.getReconciliationDefinition().getId());
		runSearchForm.addSearchRestriction("runDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, reconciliationFilter.getStartDate());
		return getReconciliationRunService().getReconciliationRunList(runSearchForm);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationFilterService getReconciliationFilterService() {
		return this.reconciliationFilterService;
	}


	public void setReconciliationFilterService(ReconciliationFilterService reconciliationFilterService) {
		this.reconciliationFilterService = reconciliationFilterService;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}
}
