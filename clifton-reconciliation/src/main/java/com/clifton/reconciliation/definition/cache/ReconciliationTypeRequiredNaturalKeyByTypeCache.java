package com.clifton.reconciliation.definition.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.reconciliation.definition.type.ReconciliationTypeRequiredNaturalKey;
import org.springframework.stereotype.Component;


/**
 * @author jonathanr
 */
@Component
public class ReconciliationTypeRequiredNaturalKeyByTypeCache extends SelfRegisteringSingleKeyDaoListCache<ReconciliationTypeRequiredNaturalKey, Short> {

	@Override
	protected String getBeanKeyProperty() {
		return "reconciliationType.id";
	}


	@Override
	protected Short getBeanKeyValue(ReconciliationTypeRequiredNaturalKey bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getReconciliationType());
		}
		return null;
	}
}
