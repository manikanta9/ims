package com.clifton.reconciliation.definition.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.validation.ReconciliationValidator;
import org.springframework.stereotype.Component;


/**
 * The <code>ReconciliationDefinitionFieldValidator</code> validates ReconciliationDefinitionField
 *
 * @author stevenf
 */
@Component
public class ReconciliationDefinitionValidator extends ReconciliationValidator<ReconciliationDefinition> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(ReconciliationDefinition definition, DaoEventTypes config) throws ValidationException {
		if (definition == null) {
			throw new ValidationException("Definition may not be null.");
		}

		if (config.isDelete()) {
			if (!CollectionUtils.isEmpty(getReconciliationRunList(definition, null, null, null))) {
				throw new ValidationException("Unable to delete the definition as there are existing reconciliation runs associated with it.<br />Please exclude or end the definition instead.");
			}
		}
		validateReconciliationDefinitionLevel(definition);
	}


	private void validateReconciliationDefinitionLevel(ReconciliationDefinition reconciliationDefinition) {
		ReconciliationDefinition parentDefinition = reconciliationDefinition.getParentDefinition();
		if (parentDefinition != null) {
			// check to make sure that the current definition is not already parent
			ValidationUtils.assertNull(getReconciliationDefinitionService().getReconciliationDefinitionByParent(reconciliationDefinition.getId()), "Reconciliation Definitions can only be one level deep.  This definition is already a parent definition.");

			// cannot assign itself as the parent
			ValidationUtils.assertFalse(reconciliationDefinition.getId().equals(parentDefinition.getId()), "Reconciliation Definitions can only be one level deep.  Cannot assign itself as the parent definition.");

			// check to make sure the parent definition does not have a parent
			ValidationUtils.assertNull(parentDefinition.getParentDefinition(), "Reconciliation Definitions can only be one level deep.  The parent definition has a parent definition.");

			// check to make sure that the parent is not a parent
			ReconciliationDefinition childDefinition = getReconciliationDefinitionService().getReconciliationDefinitionByParent(parentDefinition.getId());
			ValidationUtils.assertTrue((childDefinition == null || childDefinition.getId().equals(reconciliationDefinition.getId())), "Reconciliation Definitions can only be one level deep.  The parent definition is already a parent - it can only have one child.");
		}
	}
}
