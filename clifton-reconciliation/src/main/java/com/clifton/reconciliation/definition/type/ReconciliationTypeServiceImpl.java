package com.clifton.reconciliation.definition.type;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.reconciliation.definition.type.search.ReconciliationTypeRequiredNaturalKeySearchForm;
import com.clifton.reconciliation.definition.type.search.ReconciliationTypeSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author stevenf
 */
@Service
public class ReconciliationTypeServiceImpl implements ReconciliationTypeService {

	private AdvancedUpdatableDAO<ReconciliationType, Criteria> reconciliationTypeDAO;
	private AdvancedUpdatableDAO<ReconciliationTypeRequiredNaturalKey, Criteria> reconciliationTypeRequiredNaturalKeyDAO;

	private DaoNamedEntityCache<ReconciliationType> reconciliationTypeCache;
	private DaoSingleKeyListCache<ReconciliationTypeRequiredNaturalKey, Short> reconciliationTypeRequiredNaturalKeyByTypeCache;

	////////////////////////////////////////////////////////////////////////////
	////////            Reconciliation Type Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationType getReconciliationType(short id) {
		return getReconciliationTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public ReconciliationType getReconciliationTypeByName(String name) {
		return getReconciliationTypeCache().getBeanForKeyValueStrict(getReconciliationTypeDAO(), name);
	}


	@Override
	public List<ReconciliationType> getReconciliationTypeList(ReconciliationTypeSearchForm searchForm) {
		return getReconciliationTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReconciliationType saveReconciliationType(ReconciliationType reconciliationType) {
		return getReconciliationTypeDAO().save(reconciliationType);
	}


	@Override
	public void deleteReconciliationType(short id) {
		getReconciliationTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////    Reconciliation Type Required Field Business Methods    /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationTypeRequiredNaturalKey getReconciliationTypeRequiredNaturalKey(short id) {
		return getReconciliationTypeRequiredNaturalKeyDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconciliationTypeRequiredNaturalKey> getReconciliationTypeRequiredNaturalKeyListByTypeId(short id) {
		return getReconciliationTypeRequiredNaturalKeyByTypeCache().getBeanListForKeyValue(this.reconciliationTypeRequiredNaturalKeyDAO, id);
	}


	@Override
	public List<ReconciliationTypeRequiredNaturalKey> getReconciliationTypeRequiredNaturalKeyList(ReconciliationTypeRequiredNaturalKeySearchForm searchForm) {
		return getReconciliationTypeRequiredNaturalKeyDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReconciliationTypeRequiredNaturalKey saveReconciliationTypeRequiredNaturalKey(ReconciliationTypeRequiredNaturalKey reconciliationTypeRequiredNaturalKey) {
		return getReconciliationTypeRequiredNaturalKeyDAO().save(reconciliationTypeRequiredNaturalKey);
	}


	@Override
	public void deleteReconciliationTypeRequiredNaturalKey(short id) {
		getReconciliationTypeRequiredNaturalKeyDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ReconciliationType, Criteria> getReconciliationTypeDAO() {
		return this.reconciliationTypeDAO;
	}


	public void setReconciliationTypeDAO(AdvancedUpdatableDAO<ReconciliationType, Criteria> reconciliationTypeDAO) {
		this.reconciliationTypeDAO = reconciliationTypeDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationTypeRequiredNaturalKey, Criteria> getReconciliationTypeRequiredNaturalKeyDAO() {
		return this.reconciliationTypeRequiredNaturalKeyDAO;
	}


	public void setReconciliationTypeRequiredNaturalKeyDAO(AdvancedUpdatableDAO<ReconciliationTypeRequiredNaturalKey, Criteria> reconciliationTypeRequiredNaturalKeyDAO) {
		this.reconciliationTypeRequiredNaturalKeyDAO = reconciliationTypeRequiredNaturalKeyDAO;
	}


	public DaoNamedEntityCache<ReconciliationType> getReconciliationTypeCache() {
		return this.reconciliationTypeCache;
	}


	public void setReconciliationTypeCache(DaoNamedEntityCache<ReconciliationType> reconciliationTypeCache) {
		this.reconciliationTypeCache = reconciliationTypeCache;
	}


	public DaoSingleKeyListCache<ReconciliationTypeRequiredNaturalKey, Short> getReconciliationTypeRequiredNaturalKeyByTypeCache() {
		return this.reconciliationTypeRequiredNaturalKeyByTypeCache;
	}


	public void setReconciliationTypeRequiredNaturalKeyByTypeCache(DaoSingleKeyListCache<ReconciliationTypeRequiredNaturalKey, Short> reconciliationTypeRequiredNaturalKeyByTypeCache) {
		this.reconciliationTypeRequiredNaturalKeyByTypeCache = reconciliationTypeRequiredNaturalKeyByTypeCache;
	}
}
