package com.clifton.reconciliation.definition.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.reconciliation.definition.preprocessor.ReconciliationDefinitionPreprocessor;
import org.springframework.stereotype.Component;


/**
 * @author jonathanr
 */
@Component
public class ReconciliationDefinitionPreprocessorByDefinitionCache extends SelfRegisteringSingleKeyDaoListCache<ReconciliationDefinitionPreprocessor, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "reconciliationDefinition.id";
	}


	@Override
	protected Integer getBeanKeyValue(ReconciliationDefinitionPreprocessor bean) {
		if (bean.getReconciliationDefinition() != null) {
			return bean.getReconciliationDefinition().getId();
		}
		return null;
	}
}
