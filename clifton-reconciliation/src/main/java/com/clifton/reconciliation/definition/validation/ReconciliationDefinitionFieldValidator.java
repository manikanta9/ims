package com.clifton.reconciliation.definition.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.search.ReconciliationDefinitionFieldSearchForm;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.validation.ReconciliationValidator;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>ReconciliationDefinitionFieldValidator</code> validates ReconciliationDefinitionField
 *
 * @author stevenf
 */
@Component
public class ReconciliationDefinitionFieldValidator extends ReconciliationValidator<ReconciliationDefinitionField> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(ReconciliationDefinitionField definitionField, DaoEventTypes config) throws ValidationException {
		if (definitionField == null) {
			throw new ValidationException("Definition Field may not be null.");
		}

		if (config.isDelete()) {
			if (!CollectionUtils.isEmpty(getReconciliationRunList(definitionField.getReconciliationDefinition(), null, definitionField.getStartDate(), definitionField.getEndDate()))) {
				throw new ValidationException("Unable to delete the definition fields as there are existing reconciliation runs associated with it.<br />Please exclude or end the definition field instead.");
			}
		}

		ReconciliationDefinition reconciliationDefinition = definitionField.getReconciliationDefinition();
		ValidationUtils.assertNotNull(reconciliationDefinition, "Reconciliation definition may not be null.");

		if (config.isInsert() || config.isDelete()) {
			List<ReconciliationRun> runList = getReconciliationRunList(definitionField.getReconciliationDefinition(), null, definitionField.getStartDate(), definitionField.getEndDate());
			if (!CollectionUtils.isEmpty(runList)) {
				throw new ValidationException("Unable to modify field as there are existing reconciliation runs associated with the active dates of its definition.<br />Please exclude or end the definition field instead.");
			}
		}
		if (config.isInsert() || config.isUpdate()) {
			if (config.isUpdate()) {
				ReconciliationDefinitionField existingDefinitionField = getReconciliationDefinitionService().getReconciliationDefinitionField(definitionField.getId());
				//We use the existing definition since we want to find runs using the current active dates;
				//if the dates are modified to no longer include current runs then rebuilding historically could result in incorrect results.
				if (!allowUpdate(definitionField, existingDefinitionField)) {
					throw new ValidationException("You may not modify the following fields if there are existing runs associated with the field's current active dates:<br /><br />" +
							"Name<br />" +
							"Definition<br />" +
							"Data Type<br />" +
							"Natural Key<br />" +
							"Reconcilable<br />" +
							"Aggregate<br />" +
							"Start Date");
				}
			}
			//Validate there are no date conflicts
			ReconciliationDefinitionFieldSearchForm definitionFieldSearchForm = new ReconciliationDefinitionFieldSearchForm();
			definitionFieldSearchForm.setNameEquals(definitionField.getField().getName());
			definitionFieldSearchForm.setDefinitionId(definitionField.getReconciliationDefinition().getId());
			List<ReconciliationDefinitionField> existingDefinitionFieldList = getReconciliationDefinitionService().getReconciliationDefinitionFieldList(definitionFieldSearchForm)
					.stream().filter(existingDefinitionField -> !existingDefinitionField.getId().equals(definitionField.getId())).collect(Collectors.toList());
			for (ReconciliationDefinitionField existingDefinitionField : CollectionUtils.getIterable(existingDefinitionFieldList)) {
				if (!definitionField.equals(existingDefinitionField) && DateUtils.isOverlapInDates(definitionField.getStartDate(), definitionField.getEndDate(), existingDefinitionField.getStartDate(), existingDefinitionField.getEndDate())) {
					throw new ValidationException("A definition field already exists with an overlapping date range.  Please either edit the existing definition, or end it and start a new one.");
				}
			}
		}
	}


	private boolean allowUpdate(ReconciliationDefinitionField definitionField, ReconciliationDefinitionField existingDefinitionField) {
		//If no existing runs exist; then we can update any field
		if (CollectionUtils.isEmpty(getReconciliationRunList(definitionField.getReconciliationDefinition(), null, definitionField.getStartDate(), definitionField.getEndDate()))) {
			return true;
		}
		//If runs exist, if any of the following fields have changed
		if (!definitionField.getField().getName().equals(existingDefinitionField.getField().getName()) ||
				definitionField.getReconciliationDefinition().getId().intValue() != existingDefinitionField.getReconciliationDefinition().getId().intValue() ||
				definitionField.getDataType() != existingDefinitionField.getDataType() ||
				definitionField.isNaturalKey() != existingDefinitionField.isNaturalKey() ||
				definitionField.isReconcilable() != existingDefinitionField.isReconcilable() ||
				definitionField.isAggregate() != existingDefinitionField.isAggregate() ||
				!DateUtils.isEqualWithoutTime(definitionField.getStartDate(), existingDefinitionField.getStartDate())) {
			return false;
		}
		return true;
	}
}
