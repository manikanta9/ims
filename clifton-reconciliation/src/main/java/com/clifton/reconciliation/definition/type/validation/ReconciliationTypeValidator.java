package com.clifton.reconciliation.definition.type.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.type.ReconciliationType;
import org.springframework.stereotype.Component;


/**
 * The <code>ReconciliationDefinitionFieldValidator</code> validates ReconciliationDefinitionField
 *
 * @author stevenf
 */
@Component
public class ReconciliationTypeValidator extends SelfRegisteringDaoValidator<ReconciliationType> {


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(ReconciliationType type, DaoEventTypes config) throws ValidationException {
		if (type == null) {
			throw new ValidationException("Type may not be null.");
		}
		ReconciliationType originalType = getOriginalBean(type);
		if (originalType != null) {
			if (!CoreCompareUtils.isEqual(type, originalType, new String[]{"name", "cumulativeReconciliation"})) {
				throw new ValidationException("You may not modify the name or cumulative flag after creation!");
			}
		}
	}
}
