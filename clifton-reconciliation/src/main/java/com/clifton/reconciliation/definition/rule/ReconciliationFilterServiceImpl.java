package com.clifton.reconciliation.definition.rule;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.definition.rule.search.ReconciliationFilterSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>ReconciliationFilterServiceImpl</code> provides a basic implementation of the
 * {@link ReconciliationFilterService} interface
 *
 * @author StevenF
 */
@Service
public class ReconciliationFilterServiceImpl implements ReconciliationFilterService {

	private AdvancedUpdatableDAO<ReconciliationFilter, Criteria> reconciliationFilterDAO;
	private DaoSingleKeyListCache<ReconciliationFilter, Integer> reconciliationFilterByDefinitionCache;

	////////////////////////////////////////////////////////////////////////////
	////////          Reconciliation Filter Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationFilter getReconciliationFilter(int id) {
		return getReconciliationFilterDAO().findByPrimaryKey(id);
	}


	@Override
	public ReconciliationFilter getReconciliationFilterByName(String name) {
		return getReconciliationFilterDAO().findOneByField("name", name);
	}


	@Override
	public List<ReconciliationFilter> getReconciliationFilterList(ReconciliationFilterSearchForm searchForm) {

		return getReconciliationFilterDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	//Used in json migrations via OneToManyEntity annotation
	@Override
	public List<ReconciliationFilter> getReconciliationFilterListByDefinitionId(int definitionId) {
		return CollectionUtils.getStream(getReconciliationFilterByDefinitionCache().getBeanListForKeyValue(this.reconciliationFilterDAO, definitionId)).sorted(Comparator.comparing(ReconciliationFilter::isDoNotSaveFilteredData, Comparator.reverseOrder())).collect(Collectors.toList());
	}


	@Override
	public List<ReconciliationFilter> getReconciliationFilterListByDefinitionIdAndActiveOnDate(int definitionId, Date activeOnDate) {
		return CollectionUtils.getStream(getReconciliationFilterListByDefinitionId(definitionId)).filter(field -> DateUtils.isDateBetween(activeOnDate, field.getStartDate(), field.getEndDate(), false)).collect(Collectors.toList());
	}


	@Override
	@Transactional
	public ReconciliationFilter saveReconciliationFilter(ReconciliationFilter reconciliationFilter) {
		ReconciliationDefinition definition = reconciliationFilter.getReconciliationDefinition();
		AssertUtils.assertNotNull(definition, "Reconciliation Definition may not be null!");
		ReconciliationSource source = reconciliationFilter.getReconciliationSource();
		ReconciliationSource primarySource = definition.getPrimarySource();
		ReconciliationSource secondarySource = definition.getSecondarySource();
		AssertUtils.assertNotNull(source, "Reconciliation Source may not be null!");
		if (source.getId().shortValue() != primarySource.getId().shortValue() &&
				source.getId().shortValue() != secondarySource.getId().shortValue()) {
			throw new ValidationException("Filter Reconciliation Source [" + source.getName() + "] must match either the primary [" + primarySource.getName() + "] or secondary [" + secondarySource.getName() + "] source of the selected definition!");
		}
		return getReconciliationFilterDAO().save(reconciliationFilter);
	}


	@Override
	public void deleteReconciliationFilter(int id) {
		getReconciliationFilterDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ReconciliationFilter, Criteria> getReconciliationFilterDAO() {
		return this.reconciliationFilterDAO;
	}


	public void setReconciliationFilterDAO(AdvancedUpdatableDAO<ReconciliationFilter, Criteria> reconciliationFilterDAO) {
		this.reconciliationFilterDAO = reconciliationFilterDAO;
	}


	public DaoSingleKeyListCache<ReconciliationFilter, Integer> getReconciliationFilterByDefinitionCache() {
		return this.reconciliationFilterByDefinitionCache;
	}


	public void setReconciliationFilterByDefinitionCache(DaoSingleKeyListCache<ReconciliationFilter, Integer> reconciliationFilterByDefinitionCache) {
		this.reconciliationFilterByDefinitionCache = reconciliationFilterByDefinitionCache;
	}
}
