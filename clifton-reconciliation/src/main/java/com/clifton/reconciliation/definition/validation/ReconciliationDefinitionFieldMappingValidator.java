package com.clifton.reconciliation.definition.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.ReconciliationDefinitionFieldMapping;
import com.clifton.reconciliation.definition.search.ReconciliationDefinitionFieldMappingSearchForm;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.validation.ReconciliationValidator;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * The <code>ReconciliationDefinitionFieldValidator</code> validates ReconciliationDefinitionField
 *
 * @author stevenf
 */
@Component
public class ReconciliationDefinitionFieldMappingValidator extends ReconciliationValidator<ReconciliationDefinitionFieldMapping> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(final ReconciliationDefinitionFieldMapping definitionFieldMapping, DaoEventTypes config) throws ValidationException {
		if (definitionFieldMapping == null) {
			throw new ValidationException("Definition Field Mapping may not be null.");
		}

		ReconciliationDefinitionField definitionField = definitionFieldMapping.getDefinitionField();

		ValidationUtils.assertNotNull(definitionField, "Definition field may not be null.");

		List<ReconciliationRun> runList = getReconciliationRunList(definitionField.getReconciliationDefinition(), null, definitionField.getStartDate(), definitionField.getEndDate());
		if (!CollectionUtils.isEmpty(runList)) {
			throw new ValidationException("Unable to change field mappings as there are existing reconciliation runs associated with the active dates of its definition field.<br />Please exclude or end the definition field instead.");
		}

		//If saving a field mapping with a condition; we need to ensure that first a default mapping for the same field and source exists as a 'catch-all'
		//Retrieve the list and filter out any objects with the same id as the item being saved.
		ReconciliationDefinitionFieldMappingSearchForm searchForm = new ReconciliationDefinitionFieldMappingSearchForm();
		searchForm.setDefinitionFieldId(definitionFieldMapping.getDefinitionField().getId());
		searchForm.setSourceId(definitionFieldMapping.getSource().getId());
		List<ReconciliationDefinitionFieldMapping> existingDefinitionFieldMappingList = getReconciliationDefinitionService().getReconciliationDefinitionFieldMappingList(searchForm)
				.stream().filter(existingDefinitionFieldMapping -> !existingDefinitionFieldMapping.getId().equals(definitionFieldMapping.getId())).collect(Collectors.toList());
		if (config.isInsert() || config.isUpdate()) {
			for (ReconciliationDefinitionFieldMapping existingDefinitionFieldMapping : CollectionUtils.getIterable(existingDefinitionFieldMappingList)) {
				if (CompareUtils.isEqual(definitionFieldMapping.getConditionRule(), existingDefinitionFieldMapping.getConditionRule())) {
					throw new ValidationException("You cannot not add a mapping for the same field and source with the same condition!");
				}
			}
			if (definitionFieldMapping.getConditionRule() != null) {
				ValidationUtils.assertTrue(existingDefinitionFieldMappingList.stream().map(ReconciliationDefinitionFieldMapping::getConditionRule).anyMatch(Objects::isNull), "You cannot add a mapping with a condition when there is not another mapping with no condition for the same field and source!");
			}
		}
		else if (config.isDelete()) {
			if (definitionFieldMapping.getConditionRule() == null) {
				ValidationUtils.assertTrue(CollectionUtils.getSize(existingDefinitionFieldMappingList) == 0, "You cannot delete a mapping with no condition when a mapping with a condition for the same field and source still exists!");
			}
		}
	}
}
