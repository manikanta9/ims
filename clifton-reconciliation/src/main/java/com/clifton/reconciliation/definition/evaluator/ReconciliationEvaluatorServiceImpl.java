package com.clifton.reconciliation.definition.evaluator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionService;
import com.clifton.reconciliation.definition.evaluator.search.ReconciliationEvaluatorSearchForm;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.search.ReconciliationRunObjectSearchForm;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import com.clifton.reconciliation.run.util.ReconciliationRunUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.clifton.reconciliation.run.util.ReconciliationRunUtils.isMultipleRunsPerDefinition;


/**
 * The <code>ReconciliationEvaluatorServiceImpl</code> provides a basic implementation of the
 * {@link ReconciliationEvaluatorService} interface
 *
 * @author StevenF
 */
@Service
public class ReconciliationEvaluatorServiceImpl implements ReconciliationEvaluatorService {

	private RunnerHandler runnerHandler;
	private ReconciliationRunService reconciliationRunService;
	private ReconciliationDefinitionService reconciliationDefinitionService;

	private SystemBeanService systemBeanService;

	private AdvancedUpdatableDAO<ReconciliationEvaluator, Criteria> reconciliationEvaluatorDAO;

	////////////////////////////////////////////////////////////////////////////
	////////        Reconciliation Evaluator Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationEvaluator getReconciliationEvaluator(int id) {
		return getReconciliationEvaluatorDAO().findByPrimaryKey(id);
	}


	@Override
	public ReconciliationEvaluator getReconciliationEvaluatorByName(String name) {
		return getReconciliationEvaluatorDAO().findOneByField("name", name);
	}


	@Override
	public List<ReconciliationEvaluator> getReconciliationEvaluatorList(ReconciliationEvaluatorSearchForm searchForm) {
		return getReconciliationEvaluatorDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	//Used in json migrations via OneToManyEntity annotation
	@Override
	public List<ReconciliationEvaluator> getReconciliationEvaluatorListByDefinitionFieldId(int definitionFieldId) {
		ReconciliationEvaluatorSearchForm searchForm = new ReconciliationEvaluatorSearchForm();
		searchForm.setDefinitionFieldId(definitionFieldId);
		searchForm.setOrderBy("evaluationOrder:asc");
		return getReconciliationEvaluatorList(searchForm);
	}


	private List<ReconciliationEvaluator> getReconciliationEvaluatorListByCommand(ReconciliationEvaluatorCommand command) {
		List<ReconciliationEvaluator> evaluatorList = new ArrayList<>();
		if (command.getReconciliationEvaluatorId() != null) {
			ReconciliationEvaluator evaluator = command.getReconciliationEvaluatorId() != null ? getReconciliationEvaluator(command.getReconciliationEvaluatorId()) : null;
			if (evaluator != null && DateUtils.isDateBetween(command.getRunDate(), evaluator.getStartDate(), evaluator.getEndDate(), false)) {
				command.setReconciliationDefinitionId(evaluator.getDefinitionField().getReconciliationDefinition().getId());
				evaluatorList.add(evaluator);
			}
		}
		else {
			ReconciliationDefinition definition = getReconciliationDefinitionService().getReconciliationDefinition(command.getReconciliationDefinitionId());
			ReconciliationEvaluatorSearchForm searchForm = new ReconciliationEvaluatorSearchForm();
			searchForm.setDefinitionId(definition.getId());
			searchForm.setActiveOnDate(command.getRunDate());
			evaluatorList.addAll(getReconciliationEvaluatorList(searchForm));
		}
		return evaluatorList;
	}


	@Override
	@Transactional
	public ReconciliationEvaluator saveReconciliationEvaluator(ReconciliationEvaluator reconciliationEvaluator) {    // save the bean first: use system generated but user friendly bean name
		reconciliationEvaluator = saveReconciliationEvaluatorImpl(reconciliationEvaluator);
		if (reconciliationEvaluator != null && reconciliationEvaluator.getStartDate() != null) {
			if (!ReconciliationRunUtils.isMultipleRunsPerDefinition(reconciliationEvaluator.getDefinitionField().getReconciliationDefinition(), null, reconciliationEvaluator.getStartDate(), null, getReconciliationRunService())) {
				ReconciliationEvaluatorCommand command = new ReconciliationEvaluatorCommand();
				command.setReconciliationEvaluatorId(reconciliationEvaluator.getId());
				command.setRunDate(reconciliationEvaluator.getStartDate());
				command.setSynchronous(true);
				processReconciliationEvaluator(command);
			}
		}
		return reconciliationEvaluator;
	}


	private ReconciliationEvaluator saveReconciliationEvaluatorImpl(ReconciliationEvaluator reconciliationEvaluator) {
		// save the bean first: use system generated but user friendly bean name
		SystemBean evaluatorBean = reconciliationEvaluator.getEvaluatorBean();
		if (evaluatorBean != null) {
			boolean newBean = evaluatorBean.isNewBean();
			if (newBean) {
				evaluatorBean.setAutoGeneratedName(reconciliationEvaluator.getName());
				getSystemBeanService().saveSystemBean(evaluatorBean);
				getSystemBeanService().renameSystemBean(evaluatorBean.getId(), evaluatorBean.getAutoGeneratedName(reconciliationEvaluator.getName()));
			}
		}
		return getReconciliationEvaluatorDAO().save(reconciliationEvaluator);
	}


	@Override
	@Transactional
	public ReconciliationEvaluator copyReconciliationEvaluator(ReconciliationEvaluator reconciliationEvaluator) {
		//Getting complete original info about evaluator to make sure everything is where it should be
		ReconciliationEvaluator originalReconciliationEvaluator = getReconciliationEvaluator(reconciliationEvaluator.getId());

		//if the evaluator is only used once, update instead of replacing it
		if (!isMultipleRunsPerDefinition(originalReconciliationEvaluator.getDefinitionField().getReconciliationDefinition(), null, originalReconciliationEvaluator.getStartDate(), null, getReconciliationRunService())) {
			return saveReconciliationEvaluator(reconciliationEvaluator);
		}

		//Clone Evaluator
		ReconciliationEvaluator newReconciliationEvaluator = BeanUtils.cloneBean(originalReconciliationEvaluator, false, false);

		//Clone Evaluator Bean
		SystemBean originalEvaluatorBean = originalReconciliationEvaluator.getEvaluatorBean();
		SystemBean evaluatorBean = BeanUtils.cloneBean(getSystemBeanService().getSystemBean(originalEvaluatorBean.getId()), false, false);

		SystemBean modifiedEvaluatorBean = reconciliationEvaluator.getEvaluatorBean();

		//Copied from the copySystemBean so we can use the properties from the passed in reconciliationEvaluator instead of the DB values.
		List<SystemBeanProperty> newPropertyList = new ArrayList<>();
		for (SystemBeanProperty property : CollectionUtils.getIterable(modifiedEvaluatorBean.getPropertyList())) {
			SystemBeanProperty newProperty = BeanUtils.cloneBean(property, false, false);

			newProperty.setId(null);
			newProperty.setBean(evaluatorBean);
			newPropertyList.add(newProperty);
		}
		evaluatorBean.setPropertyList(newPropertyList);
		newReconciliationEvaluator.setEvaluatorBean(evaluatorBean);
		// The bean needs a new name before we can save
		evaluatorBean.setAutoGeneratedName(newReconciliationEvaluator.getName());
		getSystemBeanService().saveSystemBean(evaluatorBean);
		// Set a name with the new id autogenerated prefix
		getSystemBeanService().renameSystemBean(evaluatorBean.getId(), evaluatorBean.getAutoGeneratedName(newReconciliationEvaluator.getName()));

		//Blank out end date
		newReconciliationEvaluator.setEndDate(null);

		//Allow today for future dates for start date
		Date today = DateUtils.clearTime(new Date());
		Date startDate = reconciliationEvaluator.getStartDate();
		if (DateUtils.isDateBefore(startDate, today, false)) {
			startDate = today;
		}
		Date endDate = originalReconciliationEvaluator.getEndDate();
		if (endDate == null || DateUtils.isDateAfter(endDate, startDate)) {
			originalReconciliationEvaluator.setEndDate(DateUtils.addDays(startDate, -1));
		}
		saveReconciliationEvaluator(originalReconciliationEvaluator);

		newReconciliationEvaluator.setStartDate(startDate);

		return saveReconciliationEvaluator(newReconciliationEvaluator);
	}


	@Override
	public void deleteReconciliationEvaluator(int id) {
		getReconciliationEvaluatorDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////         Reconciliation Evaluator Utility Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status processReconciliationEvaluator(ReconciliationEvaluatorCommand command) {
		ValidationUtils.assertNotNull(command.getRunDate(), "Run date may not be null!");

		// If explicitly set; run synchronously, else, run asynchronously
		if (command.isSynchronous()) {
			Status status = Status.ofMessage("Synchronously running for " + command.getRunId());
			doProcessReconciliationEvaluator(command, status);
			if (status.getErrorCount() > 0) {
				throw new RuntimeException(status.getMessageWithErrors());
			}
			return status;
		}

		final StatusHolder statusHolder = new StatusHolder("Scheduled for " + DateUtils.fromDate(command.getRunDate()));
		Runner runner = new AbstractStatusAwareRunner("RECONCILIATION-EVALUATORS", command.getRunId(), command.getRunDate(), statusHolder) {

			@Override
			public void run() {
				try {
					doProcessReconciliationEvaluator(command, statusHolder.getStatus());
				}
				catch (Exception e) {
					StringBuilder errorMessage = new StringBuilder();
					errorMessage.append("Error processing evaluator for ");
					if (command.getReconciliationDefinitionId() != null) {
						ReconciliationDefinition definition = getReconciliationDefinitionService().getReconciliationDefinition(command.getReconciliationDefinitionId());
						if (definition != null) {
							errorMessage.append("definition [").append(definition.getLabel()).append("]");
						}
					}
					if (command.getReconciliationEvaluatorId() != null) {
						if (command.getReconciliationDefinitionId() != null) {
							errorMessage.append(" and ");
						}
						ReconciliationEvaluator evaluator = getReconciliationEvaluator(command.getReconciliationEvaluatorId());
						if (evaluator != null) {
							errorMessage.append("evaluator [").append(evaluator.getName()).append("]");
						}
					}
					getStatus().setMessage(errorMessage.toString());
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), errorMessage.toString(), e);
				}
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
		return Status.ofMessage("Started processing exclusions. Processing will be completed shortly.");
	}


	private Status doProcessReconciliationEvaluator(ReconciliationEvaluatorCommand command, Status status) {
		ValidationUtils.assertMutuallyExclusive("Please select either a definition or an evaluator, but not both.", command.getReconciliationDefinitionId(), command.getReconciliationEvaluatorId());
		List<ReconciliationEvaluator> evaluatorList = getReconciliationEvaluatorListByCommand(command);
		ReconciliationRun run = getReconciliationRun(command);
		if (!CollectionUtils.isEmpty(evaluatorList)) {
			getReconciliationRunService().reprocessReconciliationRun(run, getReconciliationRunObjectList(command, run), new HashMap<>(), status);
		}
		else {
			status.setMessage("No active evaluators found with command: " + command.getRunId());
		}
		return status;
	}


	private ReconciliationRun getReconciliationRun(ReconciliationEvaluatorCommand command) {
		ReconciliationRunSearchForm runSearchForm = new ReconciliationRunSearchForm();
		runSearchForm.addSearchRestriction("runDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, command.getRunDate());
		runSearchForm.setDefinitionId(command.getReconciliationDefinitionId());

		List<ReconciliationRun> reconciliationRunList = getReconciliationRunService().getReconciliationRunList(runSearchForm);
		return CollectionUtils.getOnlyElement(reconciliationRunList);
	}


	private List<ReconciliationRunObject> getReconciliationRunObjectList(ReconciliationEvaluatorCommand command, ReconciliationRun run) {
		ReconciliationRunObjectSearchForm reconciliationRunObjectSearchForm = new ReconciliationRunObjectSearchForm();

		ReconciliationEvaluator reconciliationEvaluator = getReconciliationEvaluator(command.getReconciliationEvaluatorId());
		reconciliationRunObjectSearchForm.setDefinitionId(command.getReconciliationDefinitionId());

		ReconciliationInvestmentAccount account = reconciliationEvaluator.getReconciliationInvestmentAccount();
		ReconciliationInvestmentSecurity security = reconciliationEvaluator.getReconciliationInvestmentSecurity();

		if (account != null) {
			reconciliationRunObjectSearchForm.setInvestmentAccountId(account.getId());
		}
		if (security != null) {
			reconciliationRunObjectSearchForm.setInvestmentSecurityId(security.getId());
		}
		if (run != null) {
			reconciliationRunObjectSearchForm.setRunId(run.getId());
		}
		reconciliationRunObjectSearchForm.setReconcileStatusName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name());
		return getReconciliationRunService().getReconciliationRunObjectList(reconciliationRunObjectSearchForm);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public ReconciliationDefinitionService getReconciliationDefinitionService() {
		return this.reconciliationDefinitionService;
	}


	public void setReconciliationDefinitionService(ReconciliationDefinitionService reconciliationDefinitionService) {
		this.reconciliationDefinitionService = reconciliationDefinitionService;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public AdvancedUpdatableDAO<ReconciliationEvaluator, Criteria> getReconciliationEvaluatorDAO() {
		return this.reconciliationEvaluatorDAO;
	}


	public void setReconciliationEvaluatorDAO(AdvancedUpdatableDAO<ReconciliationEvaluator, Criteria> reconciliationEvaluatorDAO) {
		this.reconciliationEvaluatorDAO = reconciliationEvaluatorDAO;
	}
}
