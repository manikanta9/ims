package com.clifton.reconciliation.definition.group;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.reconciliation.definition.group.search.ReconciliationDefinitionGroupSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * <code>ReconciliationDefinitionGroupServiceImpl</code> implementation of ReconciliationDefinitionGroupService
 *
 * @author TerryS
 */
@Service
public class ReconciliationDefinitionGroupServiceImpl implements ReconciliationDefinitionGroupService {

	private AdvancedUpdatableDAO<ReconciliationDefinitionGroup, Criteria> reconciliationDefinitionGroupDAO;

	////////////////////////////////////////////////////////////////////////////
	////////      Reconciliation Definition Group Business Methods     /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationDefinitionGroup getReconciliationDefinitionGroup(int id) {
		return getReconciliationDefinitionGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconciliationDefinitionGroup> getReconciliationDefinitionGroupList(final ReconciliationDefinitionGroupSearchForm searchForm) {
		return getReconciliationDefinitionGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReconciliationDefinitionGroup saveReconciliationDefinitionGroup(ReconciliationDefinitionGroup bean) {
		return getReconciliationDefinitionGroupDAO().save(bean);
	}


	@Override
	public void deleteReconciliationDefinitionGroup(int id) {
		getReconciliationDefinitionGroupDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ReconciliationDefinitionGroup, Criteria> getReconciliationDefinitionGroupDAO() {
		return this.reconciliationDefinitionGroupDAO;
	}


	public void setReconciliationDefinitionGroupDAO(AdvancedUpdatableDAO<ReconciliationDefinitionGroup, Criteria> reconciliationDefinitionGroupDAO) {
		this.reconciliationDefinitionGroupDAO = reconciliationDefinitionGroupDAO;
	}
}
