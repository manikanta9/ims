package com.clifton.reconciliation.definition.evaluator.validation;

import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluator;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluatorService;
import com.clifton.reconciliation.definition.evaluator.search.ReconciliationEvaluatorSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;


/**
 * @author stevenf
 */
public class ReconciliationEvaluatorBeanMultipleRunsCondition implements Comparison<SystemBean> {

	private static final String EVALUATOR_GROUP_NAME = "Reconciliation Evaluator";

	private ReconciliationEvaluatorService reconciliationEvaluatorService;
	private ReconciliationEvaluatorValidator reconciliationEvaluatorValidator;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;


	@Override
	public boolean evaluate(SystemBean systemBean, ComparisonContext context) {
		if (EVALUATOR_GROUP_NAME.equals(systemBean.getType().getGroup().getName())) {
			ReconciliationEvaluatorSearchForm searchForm = new ReconciliationEvaluatorSearchForm();
			searchForm.setEvaluatorBeanId(systemBean.getId());
			ReconciliationEvaluator evaluator = CollectionUtils.getFirstElementStrict(getReconciliationEvaluatorService().getReconciliationEvaluatorList(searchForm));
			getReconciliationEvaluatorValidator().catchMultipleRunsPerDefinition(evaluator.getDefinitionField().getReconciliationDefinition(), null, evaluator.getStartDate(), null);
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationEvaluatorService getReconciliationEvaluatorService() {
		return this.reconciliationEvaluatorService;
	}


	public void setReconciliationEvaluatorService(ReconciliationEvaluatorService reconciliationEvaluatorService) {
		this.reconciliationEvaluatorService = reconciliationEvaluatorService;
	}


	public ReconciliationEvaluatorValidator getReconciliationEvaluatorValidator() {
		return this.reconciliationEvaluatorValidator;
	}


	public void setReconciliationEvaluatorValidator(ReconciliationEvaluatorValidator reconciliationEvaluatorValidator) {
		this.reconciliationEvaluatorValidator = reconciliationEvaluatorValidator;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
