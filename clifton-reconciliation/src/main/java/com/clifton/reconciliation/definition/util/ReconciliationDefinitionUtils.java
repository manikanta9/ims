package com.clifton.reconciliation.definition.util;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.event.EventNotProcessedException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.ReconciliationDefinitionFieldMapping;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.definition.type.ReconciliationTypeRequiredNaturalKey;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author StevenF
 */
public class ReconciliationDefinitionUtils {


	public static void validateDefinition(ReconciliationDefinition reconciliationDefinition) {
		if (!"Active".equals(reconciliationDefinition.getWorkflowState().getStatus().getName())) {
			throw new EventNotProcessedException("Unable to process reconciliation the definition [" + reconciliationDefinition.getName() + "] is in a state [" +
					reconciliationDefinition.getWorkflowState().getName() + "] with a status that is not 'Active' [" + reconciliationDefinition.getWorkflowState().getStatus().getName() + "]!");
		}
		List<String> naturalKeyNameList = getReconciliationDefinitionNaturalKeyNames(reconciliationDefinition);
		for (ReconciliationTypeRequiredNaturalKey requiredNaturalKey : CollectionUtils.getIterable(reconciliationDefinition.getType().getRequiredNaturalKeyList())) {
			if (!naturalKeyNameList.contains(requiredNaturalKey.getReconciliationField().getName())) {
				throw new ValidationException("Definition of type [" + reconciliationDefinition.getType().getName() + "] requires a field named [" + requiredNaturalKey.getReconciliationField().getName() + "] to be defined as a natural key!");
			}
		}
	}


	private static List<String> getReconciliationDefinitionNaturalKeyNames(ReconciliationDefinition definition) {
		return definition.getReconciliationDefinitionFieldList().stream().filter(ReconciliationDefinitionField::isNaturalKey).map(definitionField -> definitionField.getField().getName()).collect(Collectors.toList());
	}


	public static List<String> getReconciliationDefinitionFieldMappingNamesBySource(ReconciliationDefinition definition, ReconciliationSource source) {
		List<String> definitionFieldMappingNames = new ArrayList<>();
		for (ReconciliationDefinitionField definitionField : CollectionUtils.getIterable(definition.getReconciliationDefinitionFieldList())) {
			for (ReconciliationDefinitionFieldMapping fieldMapping : CollectionUtils.getIterable(definitionField.getReconciliationDefinitionFieldMappingList())) {
				if (fieldMapping.getSource().getId().intValue() == source.getId().intValue()) {
					definitionFieldMappingNames.add(fieldMapping.getName());
				}
			}
		}
		return definitionFieldMappingNames;
	}
}
