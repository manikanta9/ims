package com.clifton.reconciliation.definition.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.reconciliation.definition.rule.ReconciliationFilter;
import org.springframework.stereotype.Component;


/**
 * @author jonathanr
 */
@Component
public class ReconciliationFilterByDefinitionCache extends SelfRegisteringSingleKeyDaoListCache<ReconciliationFilter, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "reconciliationDefinition.id";
	}


	@Override
	protected Integer getBeanKeyValue(ReconciliationFilter bean) {
		if (bean.getReconciliationDefinition() != null) {
			return bean.getReconciliationDefinition().getId();
		}
		return null;
	}
}
