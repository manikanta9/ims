package com.clifton.reconciliation.definition.evaluator.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluator;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluatorService;
import com.clifton.reconciliation.definition.evaluator.search.ReconciliationEvaluatorSearchForm;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.validation.ReconciliationValidator;
import com.clifton.system.bean.SystemBean;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>ReconciliationEvaluatorValidator</code> validates ReconciliationEvaluator
 *
 * @author stevenf
 */
@Component
public class ReconciliationEvaluatorValidator extends ReconciliationValidator<ReconciliationEvaluator> {

	private ReconciliationEvaluatorService reconciliationEvaluatorService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(ReconciliationEvaluator evaluator, DaoEventTypes config) throws ValidationException {
		if (evaluator == null) {
			throw new ValidationException("Evaluator may not be null.");
		}

		//Validate required fields for the exclusion assignment
		ReconciliationDefinitionField definitionField = evaluator.getDefinitionField();
		ValidationUtils.assertNotNull(definitionField, "You must specify a definition field for an evaluator!");
		SystemBean evaluatorBean = evaluator.getEvaluatorBean();
		ValidationUtils.assertNotNull(evaluatorBean, "You must define an evaluator bean!");

		//Load all runs associated with the evaluators active dates
		List<ReconciliationRun> runList = getReconciliationRunList(evaluator.getDefinitionField().getReconciliationDefinition(), null, evaluator.getStartDate(), evaluator.getEndDate());

		//Restrict deletion if any active runs are associated
		if (config.isDelete()) {
			if (!CollectionUtils.isEmpty(runList)) {
				throwApplicableRunsValidationException("You may not delete an evaluator when any runs are associated with the active dates!", runList);
			}
		}

		//On an insert or update simply reject an evaluator if modified to overlap any other applicable evaluators
		if (config.isInsert() || config.isUpdate()) {
			ReconciliationEvaluatorSearchForm evaluatorSearchForm = new ReconciliationEvaluatorSearchForm();
			evaluatorSearchForm.setName(evaluator.getName());
			evaluatorSearchForm.setDefinitionId(evaluator.getDefinitionField().getReconciliationDefinition().getId());

			List<ReconciliationEvaluator> existingEvaluatorList = getReconciliationEvaluatorService().getReconciliationEvaluatorList(evaluatorSearchForm)
					.stream().filter(existingEvaluator -> !existingEvaluator.getId().equals(evaluator.getId())).collect(Collectors.toList());
			for (ReconciliationEvaluator existingEvaluator : CollectionUtils.getIterable(existingEvaluatorList)) {
				if (!evaluator.equals(existingEvaluator) && DateUtils.isOverlapInDates(evaluator.getStartDate(), evaluator.getEndDate(), existingEvaluator.getStartDate(), existingEvaluator.getEndDate())) {
					throw new ValidationException("An evaluator already exists with an overlapping date range.  Please either edit the existing assignment, or end it and start a new one.");
				}
			}

			if (config.isInsert()) {
				//Validate there is only one run per definition for the source/data of the assignment being created or updated (not utilizing endDate)
				//We ignore endDate because we don't want to allow insertion of an evaluator to affect historical runs.
				catchMultipleRunsPerDefinition(evaluator.getDefinitionField().getReconciliationDefinition(), null, evaluator.getStartDate(), null);
			}
			else {
				//If all other validation has passed, grab the existing evaluator and do additional validation
				ReconciliationEvaluator existingEvaluator = getReconciliationEvaluatorService().getReconciliationEvaluator(evaluator.getId());
				//If the anything other than endDate was modified then reject the update if it affects multiple runs (again regardless of endDate)
				if (CollectionUtils.getSize(CoreCompareUtils.getNoEqualPropertiesWithSetters(evaluator, existingEvaluator, false, "endDate")) > 0) {
					Date startDate = DateUtils.isDateBefore(evaluator.getStartDate(), existingEvaluator.getStartDate(), false) ? evaluator.getStartDate() : existingEvaluator.getStartDate();
					catchMultipleRunsPerDefinition("You may not modify any field other than 'End Date' on an evaluator that applies to multiple runs!", existingEvaluator.getDefinitionField().getReconciliationDefinition(), null, startDate, null);
				}
				//When setting the end date, we just want to check that there are no runs after the endDate (so we pass endDate as the startDate to the function)
				if (evaluator.getEndDate() != null && !DateUtils.isEqualWithoutTime(evaluator.getEndDate(), existingEvaluator.getEndDate())) {
					catchSingleRunPerDefinition("You may not update when there are existing runs after the end date!", evaluator.getDefinitionField().getReconciliationDefinition(), null, evaluator.getEndDate(), null);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationEvaluatorService getReconciliationEvaluatorService() {
		return this.reconciliationEvaluatorService;
	}


	public void setReconciliationEvaluatorService(ReconciliationEvaluatorService reconciliationEvaluatorService) {
		this.reconciliationEvaluatorService = reconciliationEvaluatorService;
	}
}
