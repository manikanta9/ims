package com.clifton.reconciliation.definition;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluator;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluatorService;
import com.clifton.reconciliation.definition.preprocessor.ReconciliationDefinitionPreprocessor;
import com.clifton.reconciliation.definition.preprocessor.search.ReconciliationDefinitionPreprocessorSearchForm;
import com.clifton.reconciliation.definition.rule.ReconciliationFilter;
import com.clifton.reconciliation.definition.rule.ReconciliationFilterService;
import com.clifton.reconciliation.definition.search.ReconciliationDefinitionFieldMappingSearchForm;
import com.clifton.reconciliation.definition.search.ReconciliationDefinitionFieldSearchForm;
import com.clifton.reconciliation.definition.search.ReconciliationDefinitionSearchForm;
import com.clifton.reconciliation.definition.search.ReconciliationFieldSearchForm;
import com.clifton.reconciliation.definition.search.ReconciliationSourceSearchForm;
import com.clifton.reconciliation.definition.type.ReconciliationType;
import com.clifton.reconciliation.definition.type.ReconciliationTypeService;
import com.clifton.system.audit.SystemAuditEventSearchForm;
import com.clifton.system.audit.SystemAuditService;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author StevenF
 */
@Service
public class ReconciliationDefinitionServiceImpl implements ReconciliationDefinitionService {

	private static final String RECONCILIATION_DEFINITION_AUDIT_PARENT_TABLE_NAME = "ReconciliationDefinition";
	private static final String RECONCILIATION_DEFINITION_AUDIT_CHILD_TABLE_NAMES = "ReconciliationDefinitionField,ReconciliationFilter,ReconciliationEvaluator";

	private ReconciliationFilterService reconciliationFilterService;
	private ReconciliationTypeService reconciliationTypeService;
	private ReconciliationEvaluatorService reconciliationEvaluatorService;
	private SystemAuditService systemAuditService;
	private WorkflowDefinitionService workflowDefinitionService;

	private AdvancedUpdatableDAO<ReconciliationDefinition, Criteria> reconciliationDefinitionDAO;
	private AdvancedUpdatableDAO<ReconciliationField, Criteria> reconciliationFieldDAO;
	private AdvancedUpdatableDAO<ReconciliationDefinitionField, Criteria> reconciliationDefinitionFieldDAO;
	private AdvancedUpdatableDAO<ReconciliationDefinitionFieldMapping, Criteria> reconciliationDefinitionFieldMappingDAO;
	private AdvancedUpdatableDAO<ReconciliationSource, Criteria> reconciliationSourceDAO;
	private AdvancedUpdatableDAO<ReconciliationDefinitionPreprocessor, Criteria> reconciliationDefinitionPreprocessorDAO;

	private DaoNamedEntityCache<ReconciliationDefinition> reconciliationDefinitionCache;
	private DaoNamedEntityCache<ReconciliationSource> reconciliationSourceCache;
	private DaoSingleKeyListCache<ReconciliationDefinitionFieldMapping, Integer> reconciliationDefinitionFieldMappingListByDefinitionFieldCache;
	private DaoSingleKeyListCache<ReconciliationDefinitionField, Integer> reconciliationDefinitionFieldListByDefinitionCache;
	private DaoSingleKeyListCache<ReconciliationDefinitionPreprocessor, Integer> reconciliationDefinitionPreprocessorByDefinitionCache;

	////////////////////////////////////////////////////////////////////////////
	////////        Reconciliation Definition Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationDefinition getReconciliationDefinition(int id) {
		return getReconciliationDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public ReconciliationDefinition getReconciliationDefinitionByParent(int parentId) {
		return getReconciliationDefinitionDAO().findOneByField("parentDefinition.id", parentId);
	}


	@Override
	public ReconciliationDefinition getReconciliationDefinitionByName(String name) {
		return getReconciliationDefinitionCache().getBeanForKeyValueStrict(getReconciliationDefinitionDAO(), name);
	}


	@Override
	public ReconciliationDefinition getReconciliationDefinitionPopulated(int id, Date activeOnDate) {
		ReconciliationDefinition reconciliationDefinition = getReconciliationDefinitionDAO().findByPrimaryKey(id);
		return populateReconciliationDefinition(reconciliationDefinition, activeOnDate);
	}


	private ReconciliationDefinition populateReconciliationDefinition(ReconciliationDefinition reconciliationDefinition, Date activeOnDate) {
		ValidationUtils.assertNotNull(activeOnDate, "You must specify an activeOnDate when searching for definition fields!");
		if (reconciliationDefinition != null) {
			// Set filters
			reconciliationDefinition.setReconciliationFilterList(getReconciliationFilterService().getReconciliationFilterListByDefinitionIdAndActiveOnDate(reconciliationDefinition.getId(), activeOnDate));

			// set field list
			reconciliationDefinition.setReconciliationDefinitionFieldList(getReconciliationDefinitionFieldListByDefinitionId(reconciliationDefinition.getId(), activeOnDate));

			// set processorList
			reconciliationDefinition.setPreprocessorList(getReconciliationDefinitionPreprocessorListByDefinitionId(reconciliationDefinition.getId()));

			reconciliationDefinition.getType().setRequiredNaturalKeyList(getReconciliationTypeService().getReconciliationTypeRequiredNaturalKeyListByTypeId(reconciliationDefinition.getType().getId()));
		}
		return reconciliationDefinition;
	}


	@Override
	public List<ReconciliationDefinition> getReconciliationDefinitionList(ReconciliationDefinitionSearchForm searchForm) {
		if (searchForm.isSearchRestrictionSet("typeName")) {
			ReconciliationType reconciliationType = getReconciliationTypeService().getReconciliationTypeByName((String) searchForm.getSearchRestrictionValue("typeName"));
			ValidationUtils.assertNotNull(reconciliationType, () -> "Reconciliation Type: '" + searchForm.getSearchRestrictionValue("typeName") + "' does not exist.");
			searchForm.setTypeId(reconciliationType.getId());
			searchForm.removeSearchRestrictionAndBeanField("typeName");
		}
		if (searchForm.isSearchRestrictionSet("typeOrParentTypeName")) {
			ReconciliationType reconciliationType = getReconciliationTypeService().getReconciliationTypeByName((String) searchForm.getSearchRestrictionValue("typeOrParentTypeName"));
			ValidationUtils.assertNotNull(reconciliationType, () -> "Reconciliation Type: '" + searchForm.getSearchRestrictionValue("typeOrParentTypeName") + "' does not exist.");
			searchForm.setTypeId(reconciliationType.getId());
			searchForm.removeSearchRestrictionAndBeanField("typeOrParentTypeName");
		}
		return getReconciliationDefinitionDAO().findBySearchCriteria(new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()));
	}


	@Override
	public ReconciliationDefinition saveReconciliationDefinition(ReconciliationDefinition reconciliationDefinition) {
		return getReconciliationDefinitionDAO().save(reconciliationDefinition);
	}


	@Override
	public void deleteReconciliationDefinition(int id) {
		deleteReconciliationDefinitionImpl(id);
	}


	/**
	 * Deletion is in a separate method because the @Transactional annotation will force proxying, and
	 * since all reconciliation services are proxied from IMS this causes a conflict - potentially code
	 * in the ExternalServiceInvocationHandler could be updated to unwrap the proxy for transactional methods
	 * in order to proxy the target of the transactional proxy and then this delegation could be avoided.
	 */
	@Transactional
	protected void deleteReconciliationDefinitionImpl(int id) {
		List<ReconciliationFilter> filterList = getReconciliationFilterService().getReconciliationFilterListByDefinitionId(id);
		for (ReconciliationFilter filter : CollectionUtils.getIterable(filterList)) {
			getReconciliationFilterService().deleteReconciliationFilter(filter.getId());
		}
		List<ReconciliationDefinitionField> definitionFieldList = getReconciliationDefinitionFieldListByDefinitionId(id);
		for (ReconciliationDefinitionField definitionField : CollectionUtils.getIterable(definitionFieldList)) {
			deleteReconciliationDefinitionField(definitionField.getId());
		}
		getReconciliationDefinitionDAO().delete(id);
	}


	@Override
	public DataTable getReconciliationDefinitionSystemAuditEventList(int id) {
		SystemAuditEventSearchForm systemAuditSearchForm = new SystemAuditEventSearchForm();
		systemAuditSearchForm.setEntityId((long) id);
		systemAuditSearchForm.setTableName(RECONCILIATION_DEFINITION_AUDIT_PARENT_TABLE_NAME);
		systemAuditSearchForm.setChildTables(RECONCILIATION_DEFINITION_AUDIT_CHILD_TABLE_NAMES);
		return getSystemAuditService().getSystemAuditEventList(systemAuditSearchForm);
	}


	////////////////////////////////////////////////////////////////////////////
	////////           Reconciliation Field Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationField getReconciliationField(int id) {
		return getReconciliationFieldDAO().findByPrimaryKey(id);
	}


	@Override
	public ReconciliationField getReconciliationFieldByName(String name) {
		return getReconciliationFieldDAO().findOneByField("name", name);
	}


	@Override
	public List<ReconciliationField> getReconciliationFieldList(ReconciliationFieldSearchForm searchForm) {
		return getReconciliationFieldDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReconciliationField saveReconciliationField(ReconciliationField reconciliationField) {
		return getReconciliationFieldDAO().save(reconciliationField);
	}


	@Override
	public void deleteReconciliationField(int id) {
		getReconciliationFieldDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////     Reconciliation Definition Field Business Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationDefinitionField getReconciliationDefinitionField(int id) {
		ReconciliationDefinitionField definitionField = getReconciliationDefinitionFieldDAO().findByPrimaryKey(id);
		if (definitionField != null) {
			//Populate key list
			definitionField.setReconciliationDefinitionFieldMappingList(getReconciliationDefinitionFieldMappingListByDefinitionFieldId(definitionField.getId()));
			definitionField.setReconciliationEvaluatorList(getReconciliationEvaluatorService().getReconciliationEvaluatorListByDefinitionFieldId(id));
		}
		return definitionField;
	}


	@Override
	public ReconciliationDefinitionField getReconciliationDefinitionFieldByNameAndDefinitionId(String name, int definitionId, Date activeOnDate) {
		ValidationUtils.assertNotNull(activeOnDate, "You must specify an activeOnDate when searching for definition fields!");
		ReconciliationDefinitionFieldSearchForm searchForm = new ReconciliationDefinitionFieldSearchForm();
		searchForm.setNameEquals(name);
		searchForm.setDefinitionId(definitionId);
		searchForm.setActiveOnDate(activeOnDate);
		ReconciliationDefinitionField definitionField = CollectionUtils.getOnlyElement(getReconciliationDefinitionFieldList(searchForm));
		if (definitionField != null) {
			//Populate key list
			definitionField.setReconciliationDefinitionFieldMappingList(getReconciliationDefinitionFieldMappingListByDefinitionFieldId(definitionField.getId()));
		}
		return definitionField;
	}


	@Override
	public List<ReconciliationDefinitionField> getReconciliationDefinitionFieldList(ReconciliationDefinitionFieldSearchForm searchForm) {
		return getReconciliationDefinitionFieldDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	//Used in json migrations via OneToManyEntity annotation
	@Override
	public List<ReconciliationDefinitionField> getReconciliationDefinitionFieldListByDefinitionId(int definitionId) {
		return getReconciliationDefinitionFieldListByDefinitionId(definitionId, new Date());
	}


	@Override
	public List<ReconciliationDefinitionField> getReconciliationDefinitionFieldListByDefinitionId(int definitionId, Date activeOnDate) {
		ValidationUtils.assertNotNull(activeOnDate, "You must specify an activeOnDate when searching for definition fields!");
		List<ReconciliationDefinitionField> reconciliationDefinitionFieldList = CollectionUtils.getStream(getReconciliationDefinitionFieldListByDefinitionCache().getBeanListForKeyValue(getReconciliationDefinitionFieldDAO(), definitionId)).
				filter(field -> DateUtils.isDateBetween(activeOnDate, field.getStartDate(), field.getEndDate(), false)).collect(Collectors.toList());
		for (ReconciliationDefinitionField definitionField : CollectionUtils.getIterable(reconciliationDefinitionFieldList)) {
			definitionField.setReconciliationDefinitionFieldMappingList(getReconciliationDefinitionFieldMappingListByDefinitionFieldId(definitionField.getId()));
		}
		return reconciliationDefinitionFieldList;
	}


	@Override
	public List<ReconciliationDefinitionField> getReconciliationDefinitionFieldListByParentDefinitionId(int definitionId, Date activeOnDate) {
		return getReconciliationDefinitionFieldListByDefinitionId(getReconciliationDefinitionByParent(definitionId).getId(), activeOnDate);
	}


	@Override
	@Transactional
	public ReconciliationDefinitionField copyReconciliationDefinitionField(int definitionFieldId) {
		//Clone the original item, set the endDate for existing then set the startDate for the clone.
		ReconciliationDefinitionField originalDefinitionField = getReconciliationDefinitionField(definitionFieldId);
		ReconciliationDefinitionField newDefinitionField = BeanUtils.cloneBean(originalDefinitionField, false, false);
		newDefinitionField.setStartDate(DateUtils.clearTime(new Date()));
		originalDefinitionField.setEndDate(DateUtils.clearTime(DateUtils.addDays(new Date(), -1)));

		//Now save the original and new definition fields.
		saveReconciliationDefinitionField(originalDefinitionField);
		newDefinitionField = saveReconciliationDefinitionField(newDefinitionField);

		//Copy the evaluators as well
		List<ReconciliationEvaluator> evaluatorList = originalDefinitionField.getReconciliationEvaluatorList();
		if (evaluatorList != null) {
			for (ReconciliationEvaluator evaluator : CollectionUtils.getIterable(evaluatorList)) {
				// make sure the evaluator isn't expired.
				if (evaluator.getEndDate() == null || (new Date()).before(evaluator.getEndDate())) {
					ReconciliationEvaluator newEvaluator = getReconciliationEvaluatorService().copyReconciliationEvaluator(evaluator);
					newEvaluator.setDefinitionField(newDefinitionField);
					getReconciliationEvaluatorService().saveReconciliationEvaluator(newEvaluator);
				}
			}
		}

		//Now clone the definition field mappings, set the definitionField to the new field, clear condition and function rules, save, and add to fieldMappingList
		List<ReconciliationDefinitionFieldMapping> newDefinitionFieldMappingList = new ArrayList<>();
		List<ReconciliationDefinitionFieldMapping> originalDefinitionFieldMappingList = originalDefinitionField.getReconciliationDefinitionFieldMappingList();
		for (ReconciliationDefinitionFieldMapping originalDefinitionFieldMapping : CollectionUtils.getIterable(originalDefinitionFieldMappingList)) {
			ReconciliationDefinitionFieldMapping newDefinitionFieldMapping = BeanUtils.cloneBean(originalDefinitionFieldMapping, false, false);
			newDefinitionFieldMapping.setDefinitionField(newDefinitionField);
			//Save with validation disabled because the order in which field mappings are added matters
			// (e.g. cannot save a field mapping with a condition before saving one for the same source without a condition.
			// Since we are copying already valid fields we just bypass validation.)
			newDefinitionFieldMappingList.add(DaoUtils.executeWithAllObserversDisabled(() -> saveReconciliationDefinitionFieldMapping(newDefinitionFieldMapping)));
		}
		newDefinitionField.setReconciliationDefinitionFieldMappingList(newDefinitionFieldMappingList);
		return newDefinitionField;
	}


	@Override
	public ReconciliationDefinitionField saveReconciliationDefinitionField(ReconciliationDefinitionField reconciliationDefinitionField) {
		List<ReconciliationDefinitionFieldMapping> definitionFieldMappingList = reconciliationDefinitionField.getReconciliationDefinitionFieldMappingList();
		reconciliationDefinitionField = getReconciliationDefinitionFieldDAO().save(reconciliationDefinitionField);
		reconciliationDefinitionField.setReconciliationDefinitionFieldMappingList(definitionFieldMappingList);
		return reconciliationDefinitionField;
	}


	@Override
	@Transactional
	public void deleteReconciliationDefinitionField(int id) {
		for (ReconciliationDefinitionFieldMapping reconciliationDefinitionFieldMapping : CollectionUtils.getIterable(getReconciliationDefinitionFieldMappingListByDefinitionFieldId(id))) {
			getReconciliationDefinitionFieldMappingDAO().delete(reconciliationDefinitionFieldMapping.getId());
		}
		getReconciliationDefinitionFieldDAO().delete(id);
	}


	@Override
	public void saveReconciliationDefinitionFieldList(List<ReconciliationDefinitionField> newList, List<ReconciliationDefinitionField> oldList) {
		getReconciliationDefinitionFieldDAO().saveList(newList, oldList);
	}

	////////////////////////////////////////////////////////////////////////////
	//////// Reconciliation Definition Field Mapping Business Methods  /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationDefinitionFieldMapping getReconciliationDefinitionFieldMapping(int id) {
		return getReconciliationDefinitionFieldMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconciliationDefinitionFieldMapping> getReconciliationDefinitionFieldMappingList(ReconciliationDefinitionFieldMappingSearchForm searchForm) {
		return getReconciliationDefinitionFieldMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<ReconciliationDefinitionFieldMapping> getReconciliationDefinitionFieldMappingListByDefinitionFieldId(int definitionFieldId) {
		return getReconciliationDefinitionFieldMappingListByDefinitionFieldCache().getBeanListForKeyValue(getReconciliationDefinitionFieldMappingDAO(), definitionFieldId);
	}


	@Override
	public ReconciliationDefinitionFieldMapping saveReconciliationDefinitionFieldMapping(ReconciliationDefinitionFieldMapping reconciliationDefinitionFieldMapping) {
		return getReconciliationDefinitionFieldMappingDAO().save(reconciliationDefinitionFieldMapping);
	}


	@Override
	public void deleteReconciliationDefinitionFieldMapping(int id) {
		getReconciliationDefinitionFieldMappingDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////          Reconciliation Source Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationSource getReconciliationSource(short id) {
		return getReconciliationSourceDAO().findByPrimaryKey(id);
	}


	@Override
	public ReconciliationSource getReconciliationSourceByName(String name) {
		return getReconciliationSourceCache().getBeanForKeyValueStrict(getReconciliationSourceDAO(), name);
	}


	@Override
	public List<ReconciliationSource> getReconciliationSourceList(ReconciliationSourceSearchForm searchForm) {
		return getReconciliationSourceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReconciliationSource saveReconciliationSource(ReconciliationSource reconciliationSource) {
		return getReconciliationSourceDAO().save(reconciliationSource);
	}


	@Override
	public void deleteReconciliationSource(short id) {
		getReconciliationSourceDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////      Reconciliation Definition Preprocessor Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationDefinitionPreprocessor getReconciliationDefinitionPreprocessor(short id) {
		return getReconciliationDefinitionPreprocessorDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconciliationDefinitionPreprocessor> getReconciliationDefinitionPreprocessorList(ReconciliationDefinitionPreprocessorSearchForm searchForm) {
		return getReconciliationDefinitionPreprocessorDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReconciliationDefinitionPreprocessor saveReconciliationDefinitionPreprocessor(ReconciliationDefinitionPreprocessor reconciliationDefinitionPreprocessor) {
		return getReconciliationDefinitionPreprocessorDAO().save(reconciliationDefinitionPreprocessor);
	}


	@Override
	public void deleteReconciliationDefinitionPreprocessor(short id) {
		getReconciliationDefinitionPreprocessorDAO().delete(id);
	}


	@Override
	public List<ReconciliationDefinitionPreprocessor> getReconciliationDefinitionPreprocessorListByDefinitionId(int definitionId) {
		return getReconciliationDefinitionPreprocessorByDefinitionCache().getBeanListForKeyValue(getReconciliationDefinitionPreprocessorDAO(), definitionId);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationFilterService getReconciliationFilterService() {
		return this.reconciliationFilterService;
	}


	public void setReconciliationFilterService(ReconciliationFilterService reconciliationFilterService) {
		this.reconciliationFilterService = reconciliationFilterService;
	}


	public ReconciliationTypeService getReconciliationTypeService() {
		return this.reconciliationTypeService;
	}


	public void setReconciliationTypeService(ReconciliationTypeService reconciliationTypeService) {
		this.reconciliationTypeService = reconciliationTypeService;
	}


	public SystemAuditService getSystemAuditService() {
		return this.systemAuditService;
	}


	public void setSystemAuditService(SystemAuditService systemAuditService) {
		this.systemAuditService = systemAuditService;
	}


	public AdvancedUpdatableDAO<ReconciliationDefinition, Criteria> getReconciliationDefinitionDAO() {
		return this.reconciliationDefinitionDAO;
	}


	public void setReconciliationDefinitionDAO(AdvancedUpdatableDAO<ReconciliationDefinition, Criteria> reconciliationDefinitionDAO) {
		this.reconciliationDefinitionDAO = reconciliationDefinitionDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationField, Criteria> getReconciliationFieldDAO() {
		return this.reconciliationFieldDAO;
	}


	public void setReconciliationFieldDAO(AdvancedUpdatableDAO<ReconciliationField, Criteria> reconciliationFieldDAO) {
		this.reconciliationFieldDAO = reconciliationFieldDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationDefinitionField, Criteria> getReconciliationDefinitionFieldDAO() {
		return this.reconciliationDefinitionFieldDAO;
	}


	public void setReconciliationDefinitionFieldDAO(AdvancedUpdatableDAO<ReconciliationDefinitionField, Criteria> reconciliationDefinitionFieldDAO) {
		this.reconciliationDefinitionFieldDAO = reconciliationDefinitionFieldDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationDefinitionFieldMapping, Criteria> getReconciliationDefinitionFieldMappingDAO() {
		return this.reconciliationDefinitionFieldMappingDAO;
	}


	public void setReconciliationDefinitionFieldMappingDAO(AdvancedUpdatableDAO<ReconciliationDefinitionFieldMapping, Criteria> reconciliationDefinitionFieldMappingDAO) {
		this.reconciliationDefinitionFieldMappingDAO = reconciliationDefinitionFieldMappingDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationSource, Criteria> getReconciliationSourceDAO() {
		return this.reconciliationSourceDAO;
	}


	public void setReconciliationSourceDAO(AdvancedUpdatableDAO<ReconciliationSource, Criteria> reconciliationSourceDAO) {
		this.reconciliationSourceDAO = reconciliationSourceDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationDefinitionPreprocessor, Criteria> getReconciliationDefinitionPreprocessorDAO() {
		return this.reconciliationDefinitionPreprocessorDAO;
	}


	public void setReconciliationDefinitionPreprocessorDAO(AdvancedUpdatableDAO<ReconciliationDefinitionPreprocessor, Criteria> reconciliationDefinitionPreprocessorDAO) {
		this.reconciliationDefinitionPreprocessorDAO = reconciliationDefinitionPreprocessorDAO;
	}


	public DaoNamedEntityCache<ReconciliationDefinition> getReconciliationDefinitionCache() {
		return this.reconciliationDefinitionCache;
	}


	public void setReconciliationDefinitionCache(DaoNamedEntityCache<ReconciliationDefinition> reconciliationDefinitionCache) {
		this.reconciliationDefinitionCache = reconciliationDefinitionCache;
	}


	public DaoNamedEntityCache<ReconciliationSource> getReconciliationSourceCache() {
		return this.reconciliationSourceCache;
	}


	public void setReconciliationSourceCache(DaoNamedEntityCache<ReconciliationSource> reconciliationSourceCache) {
		this.reconciliationSourceCache = reconciliationSourceCache;
	}


	public DaoSingleKeyListCache<ReconciliationDefinitionFieldMapping, Integer> getReconciliationDefinitionFieldMappingListByDefinitionFieldCache() {
		return this.reconciliationDefinitionFieldMappingListByDefinitionFieldCache;
	}


	public void setReconciliationDefinitionFieldMappingListByDefinitionFieldCache(DaoSingleKeyListCache<ReconciliationDefinitionFieldMapping, Integer> reconciliationDefinitionFieldMappingListByDefinitionFieldCache) {
		this.reconciliationDefinitionFieldMappingListByDefinitionFieldCache = reconciliationDefinitionFieldMappingListByDefinitionFieldCache;
	}


	public DaoSingleKeyListCache<ReconciliationDefinitionField, Integer> getReconciliationDefinitionFieldListByDefinitionCache() {
		return this.reconciliationDefinitionFieldListByDefinitionCache;
	}


	public void setReconciliationDefinitionFieldListByDefinitionCache(DaoSingleKeyListCache<ReconciliationDefinitionField, Integer> reconciliationDefinitionFieldListByDefinitionCache) {
		this.reconciliationDefinitionFieldListByDefinitionCache = reconciliationDefinitionFieldListByDefinitionCache;
	}


	public DaoSingleKeyListCache<ReconciliationDefinitionPreprocessor, Integer> getReconciliationDefinitionPreprocessorByDefinitionCache() {
		return this.reconciliationDefinitionPreprocessorByDefinitionCache;
	}


	public void setReconciliationDefinitionPreprocessorByDefinitionCache(DaoSingleKeyListCache<ReconciliationDefinitionPreprocessor, Integer> reconciliationDefinitionPreprocessorByDefinitionCache) {
		this.reconciliationDefinitionPreprocessorByDefinitionCache = reconciliationDefinitionPreprocessorByDefinitionCache;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public ReconciliationEvaluatorService getReconciliationEvaluatorService() {
		return this.reconciliationEvaluatorService;
	}


	public void setReconciliationEvaluatorService(ReconciliationEvaluatorService reconciliationEvaluatorService) {
		this.reconciliationEvaluatorService = reconciliationEvaluatorService;
	}
}
