package com.clifton.reconciliation.dataaccess.dao.hibernate;


import com.clifton.core.converter.json.jackson.JacksonObjectMapper;
import com.clifton.core.dataaccess.dao.hibernate.GenericCompressedTextUserType;
import com.clifton.reconciliation.json.jackson.strategy.ReconciliationRunObjectStrategy;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.HibernateException;


/**
 * The <code>GenericCompressedTextUserType</code>
 *
 * @author stevenf
 */
public class ReconciliationObjectCompressedTextUserType extends GenericCompressedTextUserType {

	private static final ObjectMapper RECONCILIATION_OBJECT_MAPPER = new JacksonObjectMapper(new ReconciliationRunObjectStrategy());


	@Override
	public ObjectMapper getObjectMapper() {
		return RECONCILIATION_OBJECT_MAPPER;
	}


	@Override
	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}


	@Override
	public boolean isMutable() {
		return false;
	}
}
