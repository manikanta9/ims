package com.clifton.reconciliation.archive;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.run.ReconciliationRun;

import java.util.List;


/**
 * @author jonathanr
 */
public interface ReconciliationArchiveService {


	public Status archiveReconciliationRunList(Integer[] ids);


	@DoNotAddRequestMapping
	public Status archiveReconciliationRunList(List<ReconciliationRun> runList);


	public Status restoreReconciliationRunList(Integer[] ids);
}
