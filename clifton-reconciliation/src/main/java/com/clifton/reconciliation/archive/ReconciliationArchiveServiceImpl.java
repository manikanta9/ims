package com.clifton.reconciliation.archive;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ZipUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.event.object.ReconciliationEventObject;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectService;
import com.clifton.reconciliation.event.object.search.ReconciliationEventObjectSearchForm;
import com.clifton.reconciliation.file.ReconciliationDirectories;
import com.clifton.reconciliation.json.jackson.strategy.ReconciliationRunObjectStrategy;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.ReconciliationRunOrphanObject;
import com.clifton.reconciliation.run.ReconciliationRunService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentMap;


/**
 * @author jonathanr
 */
@Service
public class ReconciliationArchiveServiceImpl implements ReconciliationArchiveService {

	private ConcurrentMap<ReconciliationDirectories, String> systemFilePathMap;

	private static final int MAX_OBJECTS_PER_FILE = 10_000;
	private static final String JSON_SUFFIX = ".json";
	private static final String ZIP_SUFFIX = ".zip";

	private ReconciliationRunService reconciliationRunService;
	private ReconciliationEventObjectService reconciliationEventObjectService;
	private JsonHandler<ReconciliationRunObjectStrategy> reconciliationRunObjectJsonHandler;

	private AdvancedUpdatableDAO<ReconciliationRun, Criteria> reconciliationRunDAO;
	private AdvancedUpdatableDAO<ReconciliationRunObject, Criteria> reconciliationRunObjectDAO;
	private AdvancedUpdatableDAO<ReconciliationRunOrphanObject, Criteria> reconciliationRunOrphanObjectDAO;
	private AdvancedUpdatableDAO<ReconciliationEventObject<?>, Criteria> reconciliationEventObjectDAO;


	@Override

	public Status archiveReconciliationRunList(List<ReconciliationRun> runList) {
		Status status = Status.ofTitle("Archive details");
		final int total = runList.size();
		int successful = 0;
		int skipped = 0;
		for (ReconciliationRun run : CollectionUtils.getIterable(runList)) {
			Integer runId = run.getId();
			String processingPath = getSystemFilePathMap().get(ReconciliationDirectories.PROCESSING) + File.separator + runId;
			String archivePath = getSystemFilePathMap().get(ReconciliationDirectories.ARCHIVE) + File.separator + runId + ZIP_SUFFIX;

			List<FileContainer> createdFiles = new LinkedList<>();
			try {
				if (!run.getReconciliationDefinition().getType().isCumulativeReconciliation()) {
					FileUtils.createDirectory(processingPath);
					List<ReconciliationRunOrphanObject> orphanObjectList = getReconciliationRunService().getReconciliationRunOrphanObjectListByRunId(runId);
					ReconciliationEventObjectSearchForm searchForm = new ReconciliationEventObjectSearchForm();
					searchForm.setRunOrParentRunId(runId);
					List<ReconciliationEventObject<?>> eventObjectList = getReconciliationEventObjectService().getReconciliationEventObjectList(searchForm);
					List<ReconciliationRunObject> runObjectList = getReconciliationRunService().getReconciliationRunObjectListByRunId(runId);
					createdFiles.addAll(createDependentFileList(processingPath, runObjectList));
					archiveFiles(createdFiles, archivePath, processingPath);
					deleteDependentObjects(run, orphanObjectList, eventObjectList, runObjectList);
					status.addDetail(StatusDetail.CATEGORY_SUCCESS, "Archived " + runObjectList.size() + " Run objects, " + eventObjectList.size() + " Event objects, and " + orphanObjectList.size() + " Orphan objects for [" + run.getLabel() + "]");
					successful++;
				}else{
					skipped++;
				}
			}
			catch (Throwable e) {
				String errorMessage = "Failed to archive [" + run.getLabel() + "]";
				status.addError(errorMessage + e.getMessage());
				LogUtils.error(getClass(), errorMessage, e);
				//Failed to archive so if this file exists its most likely incomplete or corrupt.
				FileContainer archiveFile = FileContainerFactory.getFileContainer(archivePath);
				if (archiveFile.exists()) {
					FileUtils.delete(archiveFile);
				}
			}
			finally {
				cleanUpFileDirectory(processingPath);
			}
		}
		status.setMessage("Successfully archived " + successful + " runs out of " + total + "; Skipped " + skipped + " cumulative runs");
		return status;
	}


	@Override
	public Status archiveReconciliationRunList(Integer[] ids) {
		ValidationUtils.assertTrue((ids != null && ids.length > 0), "Error: no runs selected. Please Select a run to archive.");
		List<ReconciliationRun> runList = getReconciliationRunService().getReconciliationRunListByIds(ids);
		return archiveReconciliationRunList(runList);
	}


	@Override
	public Status restoreReconciliationRunList(Integer[] ids) {
		Status status = Status.ofTitle("Restore details");
		final int total = ids.length;
		int successful = 0;
		for (Integer runId : ids) {
			String workspacePath = getSystemFilePathMap().get(ReconciliationDirectories.PROCESSING) + File.separator + runId + File.separator;
			ReconciliationRun run = getReconciliationRunService().getReconciliationRun(runId);
			try {
				FileContainer workSpace = FileContainerFactory.getFileContainer(workspacePath);
				workSpace.makeDirs();
				File archiveFile = new File(getSystemFilePathMap().get(ReconciliationDirectories.ARCHIVE) + File.separator + runId + ZIP_SUFFIX);
				List<FileWrapper> zippedFiles = ZipUtils.unZipFiles(archiveFile, workSpace.getAbsolutePath());
				List<ReconciliationRunObject> runObjectList = new ArrayList<>();
				for (FileWrapper fw : CollectionUtils.getIterable(zippedFiles)) {
					ReconciliationRunObject[] reconciliationRunObjectArray = (ReconciliationRunObject[]) getReconciliationRunObjectJsonHandler().fromJson(fw.getFile().toFileContainer().getInputStream(), ReconciliationRunObject[].class);
					if (reconciliationRunObjectArray != null) {
						runObjectList.addAll(Arrays.asList(reconciliationRunObjectArray));
					}
				}
				saveDependentObjects(run, runObjectList);
				FileUtils.delete(archiveFile);
				status.addDetail(StatusDetail.CATEGORY_SUCCESS, "Restored " + runObjectList.size() + " Run objects for [" + run.getLabel() + "]");
				successful++;
			}
			catch (Throwable e) {
				String errorMessage = "Failed to restore [" + run.getLabel() + "]";
				status.addError(errorMessage + e.getMessage());
				LogUtils.error(getClass(), errorMessage, e);
			}
			finally {
				cleanUpFileDirectory(workspacePath);
			}
		}
		status.setMessage("Successfully restored " + successful + " runs out of " + total);
		return status;
	}


	@Transactional
	void deleteDependentObjects(ReconciliationRun run, List<ReconciliationRunOrphanObject> orphanObjectList, List<ReconciliationEventObject<?>> eventObjectList, List<ReconciliationRunObject> runObjectList) {
		//Delete Orphaned Run Objects
		DaoUtils.executeWithAllObserversDisabled(() -> getReconciliationRunOrphanObjectDAO().deleteList(orphanObjectList));
		//Delete Events
		DaoUtils.executeWithAllObserversDisabled(() -> getReconciliationEventObjectDAO().deleteList(eventObjectList));
		//Delete Run Objects
		DaoUtils.executeWithAllObserversDisabled(() -> getReconciliationRunObjectDAO().deleteList(runObjectList));
		run.setArchived(true);
		DaoUtils.executeWithAllObserversDisabled(() -> getReconciliationRunDAO().save(run));
	}


	@Transactional
	void saveDependentObjects(ReconciliationRun run, List<ReconciliationRunObject> runObjectList) {
		//Save Run Objects
		DaoUtils.executeWithAllObserversDisabled(() -> getReconciliationRunObjectDAO().saveList(runObjectList));

		run.setArchived(false);
		DaoUtils.executeWithAllObserversDisabled(() -> getReconciliationRunDAO().save(run));
	}


	private <T extends BaseSimpleEntity<? extends Number>> List<FileContainer> createDependentFileList(String path, List<T> objectList) {
		List<FileContainer> createdFilesList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(objectList)) {
			List<List<T>> fileListList = splitList(objectList);
			FileContainer directory = FileContainerFactory.getFileContainer(path);
			if (!directory.exists()) {
				FileUtils.createDirectory(directory.getPath());
			}
			for (int i = 0; i < fileListList.size(); ++i) {
				List<T> splitObjectList = fileListList.get(i);
				String fileName = File.separator + (i * MAX_OBJECTS_PER_FILE) + "-" + ((i * MAX_OBJECTS_PER_FILE) + splitObjectList.size());
				FileContainer jsonFile = FileContainerFactory.getFileContainer(path + File.separator + fileName + JSON_SUFFIX);
				FileUtils.writeStringToFile(jsonFile, getReconciliationRunObjectJsonHandler().toJson(splitObjectList));
				createdFilesList.add(jsonFile);
			}
		}
		return createdFilesList;
	}


	private List<FileWrapper> archiveFiles(List<FileContainer> fileList, String archivePath, String processingPath) {
		FileContainer processingDir = FileContainerFactory.getFileContainer(processingPath);
		ValidationUtils.assertTrue(processingDir.exists(), "[" + processingPath + "] is not a valid export folder path.");
		List<FileWrapper> result = new ArrayList<>();
		for (FileContainer file : CollectionUtils.getIterable(fileList)) {
			FileWrapper wrapper = new FileWrapper(FilePath.forPath(file.getAbsolutePath()), file.getName(), false);
			result.add(wrapper);
		}
		ZipUtils.zipFiles(result, archivePath, processingPath);
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void cleanUpFileDirectory(String path) {
		FileContainer rootDirectory = FileContainerFactory.getFileContainer(path);
		if (rootDirectory.exists()) {
			FileUtils.deleteDirectory(rootDirectory);
		}
	}


	private <T> List<List<T>> splitList(List<T> list) {
		List<List<T>> fileListList = new ArrayList<>();
		int listSize = list.size();
		for (int i = 0; i < listSize; i += MAX_OBJECTS_PER_FILE) {
			fileListList.add(new ArrayList<>(list.subList(i, Math.min(listSize, i + MAX_OBJECTS_PER_FILE)))
			);
		}
		return fileListList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ConcurrentMap<ReconciliationDirectories, String> getSystemFilePathMap() {
		return this.systemFilePathMap;
	}


	public void setSystemFilePathMap(ConcurrentMap<ReconciliationDirectories, String> systemFilePathMap) {
		this.systemFilePathMap = systemFilePathMap;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public ReconciliationEventObjectService getReconciliationEventObjectService() {
		return this.reconciliationEventObjectService;
	}


	public void setReconciliationEventObjectService(ReconciliationEventObjectService reconciliationEventObjectService) {
		this.reconciliationEventObjectService = reconciliationEventObjectService;
	}


	public JsonHandler<ReconciliationRunObjectStrategy> getReconciliationRunObjectJsonHandler() {
		return this.reconciliationRunObjectJsonHandler;
	}


	public void setReconciliationRunObjectJsonHandler(JsonHandler<ReconciliationRunObjectStrategy> reconciliationRunObjectJsonHandler) {
		this.reconciliationRunObjectJsonHandler = reconciliationRunObjectJsonHandler;
	}


	public AdvancedUpdatableDAO<ReconciliationRun, Criteria> getReconciliationRunDAO() {
		return this.reconciliationRunDAO;
	}


	public void setReconciliationRunDAO(AdvancedUpdatableDAO<ReconciliationRun, Criteria> reconciliationRunDAO) {
		this.reconciliationRunDAO = reconciliationRunDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationRunObject, Criteria> getReconciliationRunObjectDAO() {
		return this.reconciliationRunObjectDAO;
	}


	public void setReconciliationRunObjectDAO(AdvancedUpdatableDAO<ReconciliationRunObject, Criteria> reconciliationRunObjectDAO) {
		this.reconciliationRunObjectDAO = reconciliationRunObjectDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationRunOrphanObject, Criteria> getReconciliationRunOrphanObjectDAO() {
		return this.reconciliationRunOrphanObjectDAO;
	}


	public void setReconciliationRunOrphanObjectDAO(AdvancedUpdatableDAO<ReconciliationRunOrphanObject, Criteria> reconciliationRunOrphanObjectDAO) {
		this.reconciliationRunOrphanObjectDAO = reconciliationRunOrphanObjectDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationEventObject<?>, Criteria> getReconciliationEventObjectDAO() {
		return this.reconciliationEventObjectDAO;
	}


	public void setReconciliationEventObjectDAO(AdvancedUpdatableDAO<ReconciliationEventObject<?>, Criteria> reconciliationEventObjectDAO) {
		this.reconciliationEventObjectDAO = reconciliationEventObjectDAO;
	}
}
