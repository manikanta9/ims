package com.clifton.reconciliation.investment.cache;

import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;


/**
 * The <class>ReconciliationInvestmentAccountCache</class> is a cache used to lookup ReconciliationInvestmentSecurity using
 * an unknown security value (i.e. it is unknown if the value is a symbol, isin, sedol, cusip or occSymbol) so the value is
 * checked against all in the cache
 */
public interface ReconciliationInvestmentAccountCache {

	public ReconciliationInvestmentAccount getReconciliationInvestmentAccountByNumber(String investmentAccountNumber);
}
