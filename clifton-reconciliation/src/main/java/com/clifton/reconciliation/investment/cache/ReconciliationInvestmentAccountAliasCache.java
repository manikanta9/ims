package com.clifton.reconciliation.investment.cache;

import com.clifton.reconciliation.investment.ReconciliationInvestmentAccountAlias;

import java.util.Date;
import java.util.Map;


/**
 * The <class>ReconciliationInvestmentAccountCache</class> is a cache used to lookup ReconciliationInvestmentSecurity using
 * an unknown security value (i.e. it is unknown if the value is a symbol, isin, sedol, cusip or occSymbol) so the value is
 * checked against all in the cache
 */
public interface ReconciliationInvestmentAccountAliasCache {

	public Map<String, ReconciliationInvestmentAccountAlias> getReconciliationInvestmentAccountAliasMap(String sourceName, Date activeOnDate);
}
