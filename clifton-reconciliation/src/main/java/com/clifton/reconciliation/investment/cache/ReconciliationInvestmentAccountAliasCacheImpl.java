package com.clifton.reconciliation.investment.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccountAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentAccountAliasSearchForm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Component
public class ReconciliationInvestmentAccountAliasCacheImpl extends SelfRegisteringSimpleDaoCache<ReconciliationInvestmentAccountAlias, String, Map<String, ReconciliationInvestmentAccountAlias>> implements ReconciliationInvestmentAccountAliasCache {

	private static final String RECONCILIATION_ACCOUNT_ALIAS_CACHE_NAME = "ReconciliationInvestmentAccountAliasMap";

	private ReconciliationInvestmentService reconciliationInvestmentService;


	@Override
	public Map<String, ReconciliationInvestmentAccountAlias> getReconciliationInvestmentAccountAliasMap(String sourceName, Date activeOnDate) {
		Map<String, ReconciliationInvestmentAccountAlias> accountAliasMap = getCacheHandler().get(getCacheName(), RECONCILIATION_ACCOUNT_ALIAS_CACHE_NAME);
		if (accountAliasMap == null) {
			//Order by start date, this is because you can have the same alias for the same source but different start dates
			//We only care about the latest start date, so we order in ascending order so that the latest alias overwrites the expired in the cache by key.
			ReconciliationInvestmentAccountAliasSearchForm searchForm = new ReconciliationInvestmentAccountAliasSearchForm();
			searchForm.setOrderBy("startDate:ASC");
			accountAliasMap = BeanUtils.getBeanMap(getReconciliationInvestmentService().getReconciliationInvestmentAccountAliasList(searchForm), alias -> alias.getReconciliationSource().getId() + "_" + alias.getAlias());
			if (!CollectionUtils.isEmpty(accountAliasMap)) {
				getCacheHandler().put(getCacheName(), RECONCILIATION_ACCOUNT_ALIAS_CACHE_NAME, accountAliasMap);
			}
		}
		if (!CollectionUtils.isEmpty(accountAliasMap)) {
			Map<String, ReconciliationInvestmentAccountAlias> applicableAliasMap = new HashMap<>();
			for (Map.Entry<String, ReconciliationInvestmentAccountAlias> accountAliasEntry : CollectionUtils.getIterable(accountAliasMap.entrySet())) {
				ReconciliationInvestmentAccountAlias accountAlias = accountAliasEntry.getValue();
				if (sourceName.equals(accountAlias.getReconciliationSource().getName()) && DateUtils.isDateBetween(activeOnDate, accountAlias.getStartDate(), accountAlias.getEndDate(), false)) {
					applicableAliasMap.put(accountAliasEntry.getKey(), accountAliasEntry.getValue());
				}
			}
			return applicableAliasMap;
		}
		return new HashMap<>();
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}
}
