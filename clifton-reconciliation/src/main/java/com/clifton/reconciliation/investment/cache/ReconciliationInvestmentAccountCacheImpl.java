package com.clifton.reconciliation.investment.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentAccountSearchForm;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class ReconciliationInvestmentAccountCacheImpl extends SelfRegisteringSimpleDaoCache<ReconciliationInvestmentAccount, String, Map<String, ReconciliationInvestmentAccount>> implements ReconciliationInvestmentAccountCache {

	private static final String RECONCILIATION_ACCOUNT_CACHE_NAME = "ReconciliationInvestmentAccountMap";
	private static final String ACCOUNT_NUMBER = "accountNumber";
	private static final String ACCOUNT_NUMBER_2 = "accountNumber2";

	private ReconciliationInvestmentService reconciliationInvestmentService;


	@Override
	public ReconciliationInvestmentAccount getReconciliationInvestmentAccountByNumber(String accountNumber) {
		Map<String, ReconciliationInvestmentAccount> accountMap = getCacheHandler().get(getCacheName(), RECONCILIATION_ACCOUNT_CACHE_NAME);
		if (accountMap == null) {
			accountMap = new HashMap<>();
			// not in cache: retrieve from service and store in cache for future calls
			List<ReconciliationInvestmentAccount> securityList = getReconciliationInvestmentService().getReconciliationInvestmentAccountList(new ReconciliationInvestmentAccountSearchForm());
			for (ReconciliationInvestmentAccount investmentAccount : CollectionUtils.getIterable(securityList)) {
				accountMap.put(getBeanKeyForProperties(BeanUtils.getPropertyValue(investmentAccount, ACCOUNT_NUMBER)).toUpperCase(), investmentAccount);
				Object accountNumber2Value = BeanUtils.getPropertyValue(investmentAccount, ACCOUNT_NUMBER_2);
				if (accountNumber2Value != null) {
					accountMap.put(getBeanKeyForProperties(BeanUtils.getPropertyValue(investmentAccount, ACCOUNT_NUMBER_2)).toUpperCase(), investmentAccount);
				}
			}
			if (!CollectionUtils.isEmpty(accountMap)) {
				getCacheHandler().put(getCacheName(), RECONCILIATION_ACCOUNT_CACHE_NAME, accountMap);
			}
		}
		return accountMap.get(accountNumber);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}
}
