package com.clifton.reconciliation.investment.validation;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccountAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.validation.ReconciliationValidator;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>Component</code> provides validation logic for {@link ReconciliationInvestmentAccountAlias}es.
 *
 * @author stevenf on 8/11/2015.
 */
@Component
public class ReconciliationInvestmentAccountAliasValidator extends ReconciliationValidator<ReconciliationInvestmentAccountAlias> {

	private ReconciliationInvestmentService reconciliationInvestmentService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(ReconciliationInvestmentAccountAlias bean, DaoEventTypes config) throws ValidationException {
		// NOTHING HERE - USES METHOD WITH DAO PARAM
	}


	@Override
	public void validate(ReconciliationInvestmentAccountAlias accountAlias, DaoEventTypes config, ReadOnlyDAO<ReconciliationInvestmentAccountAlias> securityAliasDAO) throws ValidationException {
		if (accountAlias == null) {
			throw new ValidationException("Security alias may not be null.");
		}

		ReconciliationSource reconciliationSource = accountAlias.getReconciliationSource();
		if (reconciliationSource == null) {
			throw new ValidationException("You must specify a source for the alias mapping!");
		}

		ValidationUtils.assertNotEquals(ReconciliationSource.DEFAULT_SOURCE, accountAlias.getReconciliationSource().getName(), "You may not create an alias for the '" + ReconciliationSource.DEFAULT_SOURCE + "' source!");

		if (getReconciliationInvestmentService().getReconciliationInvestmentAccountByNumber(accountAlias.getAlias()) != null) {
			throw new ValidationException("Found valid account using account number [" + accountAlias.getAlias() + "]!  You may not create aliases for valid accounts - instead check your mapping rules.");
		}

		ReconciliationInvestmentAccount investmentAccount = accountAlias.getInvestmentAccount();
		if (investmentAccount == null) {
			throw new ValidationException("You must specify an account to map the alias to!");
		}

		ValidationUtils.assertBefore(accountAlias.getStartDate(), accountAlias.getEndDate(), "endDate");

		//Validate there is only one run per definition for the source/data of the alias being created (not utilizing endDate)
		if (config.isInsert() || config.isDelete() || accountAlias.getEndDate() == null) {
			catchMultipleRunsPerDefinition(null, accountAlias.getReconciliationSource(), accountAlias.getStartDate(), null);
		}
		if (config.isInsert() || config.isUpdate()) {
			List<ReconciliationInvestmentAccountAlias> existingAccountAliasList =
					securityAliasDAO.findByFields(
							new String[]{"reconciliationSource.id", "alias"},
							new Object[]{
									reconciliationSource.getId(),
									accountAlias.getAlias()
							});

			//Validate there are no conflicts
			for (ReconciliationInvestmentAccountAlias existingAccountAlias : CollectionUtils.getIterable(existingAccountAliasList)) {
				if (!accountAlias.equals(existingAccountAlias) && DateUtils.isOverlapInDates(accountAlias.getStartDate(), accountAlias.getEndDate(), existingAccountAlias.getStartDate(), existingAccountAlias.getEndDate())) {
					throw new ValidationException("An account alias already exists with an overlapping date range.  Please either edit the existing alias, or end it and start a new one.<br /><br />Existing alias: [" + existingAccountAlias.getLabel() + "]");
				}
			}
			existingAccountAliasList =
					securityAliasDAO.findByFields(
							new String[]{"reconciliationSource.id", "investmentAccount.id"},
							new Object[]{
									reconciliationSource.getId(),
									accountAlias.getInvestmentAccount().getId()
							});

			//Validate there are no conflicts
			for (ReconciliationInvestmentAccountAlias existingAccountAlias : CollectionUtils.getIterable(existingAccountAliasList)) {
				if (!accountAlias.equals(existingAccountAlias) && DateUtils.isOverlapInDates(accountAlias.getStartDate(), accountAlias.getEndDate(), existingAccountAlias.getStartDate(), existingAccountAlias.getEndDate())) {
					throw new ValidationException("Another account alias is already mapped to this account for this source during the specified date range.  Please either edit the existing alias, or end it and start a new one.<br /><br />Existing alias: [" + existingAccountAlias.getLabel() + "]");
				}
			}
			if (accountAlias.getEndDate() != null) {
				List<ReconciliationRun> runList = getReconciliationRunListBySourceAfterDate(accountAlias.getReconciliationSource(), accountAlias.getEndDate());
				if (CollectionUtils.getSize(runList) > 0) {
					StringBuilder errorMessage = new StringBuilder("Alias cannot be ended on ");
					errorMessage.append(DateUtils.fromDate(accountAlias.getEndDate(), DateUtils.DATE_FORMAT_INPUT));
					errorMessage.append(" because The following run(s) exist using this alias after that date: <br/>");
					for (int i = 0; i < runList.size(); i++) {
						errorMessage.append(runList.get(i).getLabel());
						if (i + 1 < runList.size()) {
							errorMessage.append(",<br/>");
						}
					}
					throw new ValidationException(errorMessage.toString());
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}
}
