package com.clifton.reconciliation.investment.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentSecurityAliasSearchForm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Component
public class ReconciliationInvestmentSecurityAliasCacheImpl extends SelfRegisteringSimpleDaoCache<ReconciliationInvestmentSecurityAlias, String, Map<String, ReconciliationInvestmentSecurityAlias>> implements ReconciliationInvestmentSecurityAliasCache {

	private static final String RECONCILIATION_SECURITY_ALIAS_CACHE_NAME = "ReconciliationInvestmentSecurityAliasMap";

	private ReconciliationInvestmentService reconciliationInvestmentService;


	@Override
	public Map<String, ReconciliationInvestmentSecurityAlias> getReconciliationInvestmentSecurityAliasMap(String sourceName, Date activeOnDate) {
		Map<String, ReconciliationInvestmentSecurityAlias> securityAliasMap = getCacheHandler().get(getCacheName(), RECONCILIATION_SECURITY_ALIAS_CACHE_NAME);
		if (securityAliasMap == null) {
			//Order by start date, this is because you can have the same alias for the same source but different start dates
			//We only care about the latest start date, so we order in ascending order so that the latest alias overwrites the expired in the cache by key.
			ReconciliationInvestmentSecurityAliasSearchForm searchForm = new ReconciliationInvestmentSecurityAliasSearchForm();
			searchForm.setOrderBy("startDate:ASC");
			securityAliasMap = BeanUtils.getBeanMap(getReconciliationInvestmentService().getReconciliationInvestmentSecurityAliasList(searchForm), alias -> alias.getReconciliationSource().getId() + "_" + alias.getAlias());
			if (!CollectionUtils.isEmpty(securityAliasMap)) {
				getCacheHandler().put(getCacheName(), RECONCILIATION_SECURITY_ALIAS_CACHE_NAME, securityAliasMap);
			}
		}
		if (!CollectionUtils.isEmpty(securityAliasMap)) {
			Map<String, ReconciliationInvestmentSecurityAlias> applicableAliasMap = new HashMap<>();
			for (Map.Entry<String, ReconciliationInvestmentSecurityAlias> securityAliasEntry : CollectionUtils.getIterable(securityAliasMap.entrySet())) {
				ReconciliationInvestmentSecurityAlias securityAlias = securityAliasEntry.getValue();
				if (sourceName.equals(securityAlias.getReconciliationSource().getName()) && DateUtils.isDateBetween(activeOnDate, securityAlias.getStartDate(), securityAlias.getEndDate(), false)) {
					applicableAliasMap.put(securityAliasEntry.getKey(), securityAliasEntry.getValue());
				}
			}
			return applicableAliasMap;
		}
		return new HashMap<>();
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}
}
