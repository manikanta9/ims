package com.clifton.reconciliation.investment;

import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.group.ReconciliationDefinitionGroup;
import com.clifton.reconciliation.event.jms.ReconciliationEventCommand;
import com.clifton.reconciliation.event.jms.ReconciliationEventService;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectCategories;
import com.clifton.reconciliation.run.ReconciliationRun;
import org.springframework.stereotype.Component;


/**
 * @author stevenf
 */
@Component
public class ReconciliationInvestmentHandlerImpl implements ReconciliationInvestmentHandler {

	private ReconciliationEventService reconciliationEventService;


	@Override
	public void sendAccountUnlockEvents(ReconciliationRun run) {
		ReconciliationDefinition definition = run.getReconciliationDefinition();
		if (definition != null && definition.getDefinitionGroup() != null) {
			ReconciliationDefinitionGroup definitionGroup = definition.getDefinitionGroup();
			//TODO - refactor this field name
			if (definitionGroup.isEventTrigger()) {
				ReconciliationEventCommand command = new ReconciliationEventCommand();
				command.setAsynchronousRebuildDelay(5);
				command.setRunDate(run.getRunDate());
				command.setEventObjectCategory(ReconciliationEventObjectCategories.ACCOUNT);
				//TODO - safe to assume secondary source is the external source?  If not, side-effect is it will just process all unlock events, so no harm really...
				command.setSourceName(run.getReconciliationDefinition().getSecondarySource().getName());
				command.setDefinitionGroupId(run.getReconciliationDefinition().getDefinitionGroup().getId());
				getReconciliationEventService().processReconciliationEvent(command);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationEventService getReconciliationEventService() {
		return this.reconciliationEventService;
	}


	public void setReconciliationEventService(ReconciliationEventService reconciliationEventService) {
		this.reconciliationEventService = reconciliationEventService;
	}
}
