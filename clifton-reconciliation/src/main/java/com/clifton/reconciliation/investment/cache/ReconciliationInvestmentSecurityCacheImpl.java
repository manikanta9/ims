package com.clifton.reconciliation.investment.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentSecuritySearchForm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class ReconciliationInvestmentSecurityCacheImpl extends SelfRegisteringSimpleDaoCache<ReconciliationInvestmentSecurity, String, Map<String, ReconciliationInvestmentSecurity>> implements ReconciliationInvestmentSecurityCache {

	private static final String RECONCILIATION_SECURITY_CACHE_NAME = "ReconciliationInvestmentSecurityMap";
	private static final String[] KEY_PROPERTIES = new String[]{"securitySymbol", "securityCusip", "securityOccSymbol", "securityIsin", "securitySedol"};

	private ReconciliationInvestmentService reconciliationInvestmentService;


	@Override
	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurityByValue(String securityValue) {
		Map<String, ReconciliationInvestmentSecurity> securityMap = getCacheHandler().get(getCacheName(), RECONCILIATION_SECURITY_CACHE_NAME);
		if (securityMap == null) {
			securityMap = new HashMap<>();
			// not in cache: retrieve from service and store in cache for future calls
			ReconciliationInvestmentSecuritySearchForm reconciliationInvestmentSecuritySearchForm = new ReconciliationInvestmentSecuritySearchForm();
			reconciliationInvestmentSecuritySearchForm.addSearchRestriction("endDate", ComparisonConditions.GREATER_THAN_OR_IS_NULL, DateUtils.addDays(new Date(), -90));
			List<ReconciliationInvestmentSecurity> securityList = getReconciliationInvestmentService().getReconciliationInvestmentSecurityList(reconciliationInvestmentSecuritySearchForm);
			for (ReconciliationInvestmentSecurity investmentSecurity : CollectionUtils.getIterable(securityList)) {
				for (Object keyProperty : KEY_PROPERTIES) {
					securityMap.put(getBeanKeyForProperties(BeanUtils.getPropertyValue(investmentSecurity, String.valueOf(keyProperty))).toUpperCase(), investmentSecurity);
				}
			}
			if (!CollectionUtils.isEmpty(securityMap)) {
				getCacheHandler().put(getCacheName(), RECONCILIATION_SECURITY_CACHE_NAME, securityMap);
			}
		}
		return securityMap.get(securityValue);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}
}
