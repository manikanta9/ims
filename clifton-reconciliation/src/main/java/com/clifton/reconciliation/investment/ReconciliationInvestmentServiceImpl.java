package com.clifton.reconciliation.investment;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinitionService;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.event.object.ReconciliationEventObject;
import com.clifton.reconciliation.investment.cache.ReconciliationInvestmentAccountAliasCache;
import com.clifton.reconciliation.investment.cache.ReconciliationInvestmentAccountCache;
import com.clifton.reconciliation.investment.cache.ReconciliationInvestmentSecurityAliasCache;
import com.clifton.reconciliation.investment.cache.ReconciliationInvestmentSecurityCache;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentAccountAliasSearchForm;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentAccountSearchForm;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentSecurityAliasSearchForm;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentSecuritySearchForm;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatus;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationStatusService;
import com.clifton.reconciliation.run.util.ReconciliationRunUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * The <code>ReconciliationInvestmentServiceImpl</code> provides a basic implementation of the
 * {@link ReconciliationInvestmentService} interface
 *
 * @author StevenF
 */
@Service
public class ReconciliationInvestmentServiceImpl implements ReconciliationInvestmentService {

	private static final String ACCOUNT_ALIAS_BASE_RUN_OBJECT_ID_QUERY = "SELECT rro.ReconciliationRunObjectID " +
			"FROM ReconciliationRunObject rro " +
			"WHERE (rro.ReconciliationInvestmentAccountID = ? OR  JSON_VALUE(CAST(DECOMPRESS(dataHolder) AS VARCHAR(MAX)), '$.dataObjects[0].data.\"Holding Account\"') = ?) " +
			"   AND rro.ReconciliationRunID IN (%s)";

	private static final String ACCOUNT_ALIAS_RUN_OBJECT_ID_QUERY = ACCOUNT_ALIAS_BASE_RUN_OBJECT_ID_QUERY + " " +
			" AND rro.ReconciliationMatchStatusID = ? " +
			" AND rro.ReconciliationReconcileStatusID = ?";

	private static final String SECURITY_ALIAS_BASE_RUN_OBJECT_ID_QUERY = "SELECT rro.ReconciliationRunObjectID " +
			"FROM ReconciliationRunObject rro " +
			"WHERE (rro.ReconciliationInvestmentSecurityID = ? OR  JSON_VALUE(CAST(DECOMPRESS(dataHolder) AS VARCHAR(MAX)), '$.dataObjects[0].data.\"Investment Security\"') = ?) " +
			"   AND rro.ReconciliationRunID IN (%s)";

	private static final String SECURITY_ALIAS_RUN_OBJECT_ID_QUERY = SECURITY_ALIAS_BASE_RUN_OBJECT_ID_QUERY + " " +
			" AND rro.ReconciliationMatchStatusID = ? " +
			" AND rro.ReconciliationReconcileStatusID = ?";

	private SqlHandler sqlHandler;
	private RunnerHandler runnerHandler;
	private ReconciliationRunService reconciliationRunService;
	private ReconciliationStatusService reconciliationStatusService;
	private ReconciliationDefinitionService reconciliationDefinitionService;

	private AdvancedUpdatableDAO<ReconciliationInvestmentAccount, Criteria> reconciliationInvestmentAccountDAO;
	private AdvancedUpdatableDAO<ReconciliationInvestmentSecurity, Criteria> reconciliationInvestmentSecurityDAO;
	private AdvancedUpdatableDAO<ReconciliationInvestmentAccountAlias, Criteria> reconciliationInvestmentAccountAliasDAO;
	private AdvancedUpdatableDAO<ReconciliationInvestmentSecurityAlias, Criteria> reconciliationInvestmentSecurityAliasDAO;

	private ReconciliationInvestmentAccountCache reconciliationInvestmentAccountCache;
	private ReconciliationInvestmentSecurityCache reconciliationInvestmentSecurityCache;
	private ReconciliationInvestmentAccountAliasCache reconciliationInvestmentAccountAliasCache;
	private ReconciliationInvestmentSecurityAliasCache reconciliationInvestmentSecurityAliasCache;

	////////////////////////////////////////////////////////////////////////////
	////////          Reconciliation Account Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationInvestmentAccount getReconciliationInvestmentAccount(int id) {
		return getReconciliationInvestmentAccountDAO().findByPrimaryKey(id);
	}


	@Override
	public ReconciliationInvestmentAccount getReconciliationInvestmentAccountByNumber(String accountNumber) {
		//Cache keys are upper-cased, so we upper-case the search value as well to make the lookup case-insensitive
		return accountNumber != null ? getReconciliationInvestmentAccountCache().getReconciliationInvestmentAccountByNumber(accountNumber.toUpperCase()) : null;
	}


	@Override
	public List<ReconciliationInvestmentAccount> getReconciliationInvestmentAccountList(ReconciliationInvestmentAccountSearchForm searchForm) {
		return getReconciliationInvestmentAccountDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReconciliationInvestmentAccount saveReconciliationInvestmentAccount(ReconciliationInvestmentAccount investmentAccount) {
		return getReconciliationInvestmentAccountDAO().save(investmentAccount);
	}


	@Override
	public void deleteReconciliationInvestmentAccount(int id) {
		getReconciliationInvestmentAccountDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////      Reconciliation Account Alias Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationInvestmentAccountAlias getReconciliationInvestmentAccountAlias(int id) {
		return getReconciliationInvestmentAccountAliasDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconciliationInvestmentAccountAlias> getReconciliationInvestmentAccountAliasList(ReconciliationInvestmentAccountAliasSearchForm searchForm) {
		return getReconciliationInvestmentAccountAliasDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public ReconciliationInvestmentAccountAlias saveReconciliationInvestmentAccountAlias(ReconciliationInvestmentAccountAlias investmentAccountAlias) {
		boolean newBean = investmentAccountAlias.isNewBean();
		final ReconciliationInvestmentAccountAlias accountAlias = getReconciliationInvestmentAccountAliasDAO().save(investmentAccountAlias);
		if (isAliasProcessingRequired(accountAlias.getReconciliationSource(), accountAlias.getStartDate(), accountAlias.getEndDate(), newBean, true)) {
			processReconciliationInvestmentAccountAliasList(Collections.singletonList(accountAlias), false, true);
		}
		return accountAlias;
	}


	@Override
	@Transactional
	public void deleteReconciliationInvestmentAccountAlias(int id) {
		ReconciliationInvestmentAccountAlias investmentAccountAlias = getReconciliationInvestmentAccountAlias(id);
		getReconciliationInvestmentAccountAliasDAO().delete(id);
		if (isAliasProcessingRequired(investmentAccountAlias.getReconciliationSource(), investmentAccountAlias.getStartDate(), investmentAccountAlias.getEndDate(), false, false)) {
			processReconciliationInvestmentAccountAliasList(Collections.singletonList(investmentAccountAlias), true, true);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////       Reconciliation Account Alias Utility Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<String, ReconciliationInvestmentAccountAlias> getReconciliationInvestmentAccountAliasMap(String sourceName, Date activeOnDate) {
		return getReconciliationInvestmentAccountAliasCache().getReconciliationInvestmentAccountAliasMap(sourceName, activeOnDate);
	}


	@Override
	public Status processReconciliationInvestmentAccountAlias(short sourceId, Date runDate, boolean synchronous, Predicate<ReconciliationEventObject<?>> eventObjectPredicate) {
		ReconciliationInvestmentAccountAliasSearchForm searchForm = new ReconciliationInvestmentAccountAliasSearchForm();
		searchForm.setSourceId(sourceId);
		searchForm.setActiveOnDate(runDate);
		return processReconciliationInvestmentAccountAliasList(getReconciliationInvestmentAccountAliasList(searchForm), false, synchronous);
	}


	private Status processReconciliationInvestmentAccountAliasList(List<ReconciliationInvestmentAccountAlias> accountAliasList, boolean revert, boolean synchronous) {
		Date processingDate = new Date();
		ReconciliationInvestmentAccountAlias firstAccountAlias = CollectionUtils.getFirstElementStrict(accountAliasList, "You must process at least one alias!");
		ReconciliationSource source = getReconciliationDefinitionService().getReconciliationSource(firstAccountAlias.getReconciliationSource().getId());
		ValidationUtils.assertNotNull(firstAccountAlias.getStartDate(), "Run date may not be null!");
		ValidationUtils.assertNotNull(source, "Source may not be null!");

		String runId = source.getId() + "_" + DateUtils.fromDate(processingDate);
		// If explicitly set; run synchronously, else, run asynchronously
		if (synchronous) {
			Status status = Status.ofMessage("Synchronously running for " + runId);
			try {
				for (ReconciliationInvestmentAccountAlias accountAlias : CollectionUtils.getIterable(accountAliasList)) {
					doProcessReconciliationInvestmentAccountAlias(accountAlias, source, revert, status);
				}
			}
			catch (Exception e) {
				status.setMessage("Error processing security aliases for source [" + source.getId() + "]: " + ExceptionUtils.getDetailedMessage(e));
				status.addError(ExceptionUtils.getDetailedMessage(e));
				LogUtils.errorOrInfo(getClass(), "Error processing security aliases for source [" + source.getId() + "]", e);
			}
			if (status.getErrorCount() > 0) {
				throw new RuntimeException(status.getMessageWithErrors());
			}
			return status;
		}

		final StatusHolder statusHolder = new StatusHolder("Scheduled for " + DateUtils.fromDate(processingDate));
		Runner runner = new AbstractStatusAwareRunner("RECONCILIATION-SECURITY-ALIASES", runId, processingDate, statusHolder) {

			@Override
			public void run() {
				try {
					for (ReconciliationInvestmentAccountAlias accountAlias : CollectionUtils.getIterable(accountAliasList)) {
						doProcessReconciliationInvestmentAccountAlias(accountAlias, source, revert, statusHolder.getStatus());
					}
				}
				catch (Exception e) {
					getStatus().setMessage("Error processing security aliases for source [" + source.getId() + "]: " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error processing security aliases for source [" + source.getId() + "]", e);
				}
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
		return Status.ofMessage("Started processing security aliases. Processing will be completed shortly.");
	}


	private void doProcessReconciliationInvestmentAccountAlias(ReconciliationInvestmentAccountAlias accountAlias, ReconciliationSource source, boolean revert, Status status) {
		String runIds = CollectionUtils.getStream(getApplicableRunIds(source, accountAlias.getStartDate(), accountAlias.getEndDate())).map(String::valueOf).collect(Collectors.joining(", "));
		if (!StringUtils.isEmpty(runIds)) {
			SqlSelectCommand selectCommand = new SqlSelectCommand(String.format(getAccountAliasQuery(revert), runIds));
			addAccountAliasQueryParams(selectCommand, accountAlias);
			if (!revert) {
				ReconciliationMatchStatus notMatchedStatus = getReconciliationStatusService().getReconciliationMatchStatusByName(ReconciliationMatchStatusTypes.NOT_MATCHED.name());
				ReconciliationReconcileStatus notReconciledStatus = getReconciliationStatusService().getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name());
				selectCommand.addShortParameterValue(notMatchedStatus.getId()).addShortParameterValue(notReconciledStatus.getId());
			}
			List<Long> runObjectIds = getSqlHandler().executeSelect(selectCommand, rs -> {
				List<Long> result = new ArrayList<>();
				while (rs.next()) {
					result.add(rs.getLong("ReconciliationRunObjectID"));
				}
				return result;
			});
			if (!CollectionUtils.isEmpty(runObjectIds)) {
				List<ReconciliationRunObject> runObjectList = getReconciliationRunService().getReconciliationRunObjectList(runObjectIds.toArray(new Long[0]));
				Map<Integer, List<ReconciliationRunObject>> runObjectListMap = BeanUtils.getBeansMap(runObjectList, runObject -> runObject.getReconciliationRun().getId());
				for (Map.Entry<Integer, List<ReconciliationRunObject>> runObjectListMapEntry : CollectionUtils.getIterable(runObjectListMap.entrySet())) {
					ReconciliationRun run = getReconciliationRunService().getReconciliationRun(runObjectListMapEntry.getKey());
					getReconciliationRunService().reprocessReconciliationRun(run, runObjectListMapEntry.getValue(), new HashMap<>(), status);
				}
			}
		}
	}


	@Override
	public SqlSelectCommand addAccountAliasQueryParams(SqlSelectCommand selectCommand, ReconciliationInvestmentAccountAlias accountAlias) {
		return selectCommand.addIntegerParameterValue(accountAlias.getInvestmentAccount().getId()).addStringParameterValue(accountAlias.getAlias());
	}


	@Override
	public String getAccountAliasQuery(boolean revert) {
		return revert ? ACCOUNT_ALIAS_BASE_RUN_OBJECT_ID_QUERY : ACCOUNT_ALIAS_RUN_OBJECT_ID_QUERY;
	}

	////////////////////////////////////////////////////////////////////////////
	////////         Reconciliation Security Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurity(int id) {
		return getReconciliationInvestmentSecurityDAO().findByPrimaryKey(id);
	}


	@Override
	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurityBySymbol(String symbol) {
		return getReconciliationInvestmentSecurityCache().getReconciliationInvestmentSecurityByValue(symbol);
	}


	@Override
	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurityByOCCSymbol(String occSymbol) {
		return getReconciliationInvestmentSecurityCache().getReconciliationInvestmentSecurityByValue(occSymbol);
	}


	@Override
	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurityBySedol(String sedol) {
		return getReconciliationInvestmentSecurityCache().getReconciliationInvestmentSecurityByValue(sedol);
	}


	@Override
	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurityByIsin(String isin) {
		return getReconciliationInvestmentSecurityCache().getReconciliationInvestmentSecurityByValue(isin);
	}


	@Override
	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurityByCusip(String cusip) {
		return getReconciliationInvestmentSecurityCache().getReconciliationInvestmentSecurityByValue(cusip);
	}


	@Override
	public ReconciliationInvestmentSecurity getReconciliationInvestmentSecurityByIdentifier(String securityIdentifier) {
		//Cache keys are upper-cased, so we upper-case the search value as well to make the lookup case-insensitive
		return securityIdentifier != null ? getReconciliationInvestmentSecurityCache().getReconciliationInvestmentSecurityByValue(securityIdentifier.toUpperCase()) : null;
	}


	@Override
	public List<ReconciliationInvestmentSecurity> getReconciliationInvestmentSecurityList(ReconciliationInvestmentSecuritySearchForm searchForm) {
		return getReconciliationInvestmentSecurityDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReconciliationInvestmentSecurity saveReconciliationInvestmentSecurity(ReconciliationInvestmentSecurity investmentSecurity) {
		return getReconciliationInvestmentSecurityDAO().save(investmentSecurity);
	}


	@Override
	public void deleteReconciliationInvestmentSecurity(int id) {
		getReconciliationInvestmentSecurityDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////      Reconciliation Security Alias Business Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationInvestmentSecurityAlias getReconciliationInvestmentSecurityAlias(int id) {
		return getReconciliationInvestmentSecurityAliasDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconciliationInvestmentSecurityAlias> getReconciliationInvestmentSecurityAliasList(ReconciliationInvestmentSecurityAliasSearchForm searchForm) {
		return getReconciliationInvestmentSecurityAliasDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public ReconciliationInvestmentSecurityAlias saveReconciliationInvestmentSecurityAlias(ReconciliationInvestmentSecurityAlias investmentSecurityAlias) {
		boolean newBean = investmentSecurityAlias.isNewBean();
		final ReconciliationInvestmentSecurityAlias securityAlias = getReconciliationInvestmentSecurityAliasDAO().save(investmentSecurityAlias);
		if (isAliasProcessingRequired(securityAlias.getReconciliationSource(), securityAlias.getStartDate(), securityAlias.getEndDate(), newBean, true)) {
			processReconciliationInvestmentSecurityAliasList(Collections.singletonList(securityAlias), false, true);
		}
		return securityAlias;
	}


	@Override
	@Transactional
	public void deleteReconciliationInvestmentSecurityAlias(int id) {
		ReconciliationInvestmentSecurityAlias investmentSecurityAlias = getReconciliationInvestmentSecurityAlias(id);
		getReconciliationInvestmentSecurityAliasDAO().delete(id);
		if (isAliasProcessingRequired(investmentSecurityAlias.getReconciliationSource(), investmentSecurityAlias.getStartDate(), investmentSecurityAlias.getEndDate(), false, false)) {
			processReconciliationInvestmentSecurityAliasList(Collections.singletonList(investmentSecurityAlias), true, true);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////       Reconciliation Security Alias Utility Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<String, ReconciliationInvestmentSecurityAlias> getReconciliationInvestmentSecurityAliasMap(String sourceName, Date activeOnDate) {
		return getReconciliationInvestmentSecurityAliasCache().getReconciliationInvestmentSecurityAliasMap(sourceName, activeOnDate);
	}


	@Override
	public Status processReconciliationInvestmentSecurityAlias(short sourceId, Date runDate, boolean synchronous) {
		return processReconciliationInvestmentSecurityAlias(sourceId, runDate, synchronous, null);
	}


	@Override
	public Status processReconciliationInvestmentSecurityAlias(short sourceId, Date runDate, boolean synchronous, Predicate<ReconciliationEventObject<?>> eventObjectPredicate) {
		ReconciliationInvestmentSecurityAliasSearchForm searchForm = new ReconciliationInvestmentSecurityAliasSearchForm();
		searchForm.setSourceId(sourceId);
		searchForm.setActiveOnDate(runDate);
		return processReconciliationInvestmentSecurityAliasList(getReconciliationInvestmentSecurityAliasList(searchForm), false, synchronous);
	}


	private Status processReconciliationInvestmentSecurityAliasList(List<ReconciliationInvestmentSecurityAlias> securityAliasList, boolean revert, boolean synchronous) {
		Date processingDate = new Date();
		ReconciliationInvestmentSecurityAlias firstSecurityAlias = CollectionUtils.getFirstElementStrict(securityAliasList, "You must process at least one alias!");
		ReconciliationSource source = getReconciliationDefinitionService().getReconciliationSource(firstSecurityAlias.getReconciliationSource().getId());
		ValidationUtils.assertNotNull(firstSecurityAlias.getStartDate(), "Run date may not be null!");
		ValidationUtils.assertNotNull(source, "Source may not be null!");

		String runId = source.getId() + "_" + DateUtils.fromDate(processingDate);
		// If explicitly set; run synchronously, else, run asynchronously
		if (synchronous) {
			Status status = Status.ofMessage("Synchronously running for " + runId);
			try {
				for (ReconciliationInvestmentSecurityAlias securityAlias : CollectionUtils.getIterable(securityAliasList)) {
					doProcessReconciliationInvestmentSecurityAlias(securityAlias, source, revert, status);
				}
			}
			catch (Exception e) {
				status.setMessage("Error processing security aliases for source [" + source.getId() + "]: " + ExceptionUtils.getDetailedMessage(e));
				status.addError(ExceptionUtils.getDetailedMessage(e));
				LogUtils.errorOrInfo(getClass(), "Error processing security aliases for source [" + source.getId() + "]", e);
			}
			if (status.getErrorCount() > 0) {
				throw new RuntimeException(status.getMessageWithErrors());
			}
			return status;
		}

		final StatusHolder statusHolder = new StatusHolder("Scheduled for " + DateUtils.fromDate(processingDate));
		Runner runner = new AbstractStatusAwareRunner("RECONCILIATION-SECURITY-ALIASES", runId, processingDate, statusHolder) {

			@Override
			public void run() {
				try {
					for (ReconciliationInvestmentSecurityAlias securityAlias : CollectionUtils.getIterable(securityAliasList)) {
						doProcessReconciliationInvestmentSecurityAlias(securityAlias, source, revert, statusHolder.getStatus());
					}
				}
				catch (Exception e) {
					getStatus().setMessage("Error processing security aliases for source [" + source.getId() + "]: " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error processing security aliases for source [" + source.getId() + "]", e);
				}
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
		return Status.ofMessage("Started processing security aliases. Processing will be completed shortly.");
	}


	private void doProcessReconciliationInvestmentSecurityAlias(ReconciliationInvestmentSecurityAlias securityAlias, ReconciliationSource source, boolean revert, Status status) {
		String runIds = CollectionUtils.getStream(getApplicableRunIds(source, securityAlias.getStartDate(), securityAlias.getEndDate())).map(String::valueOf).collect(Collectors.joining(", "));
		if (!StringUtils.isEmpty(runIds)) {
			SqlSelectCommand selectCommand = new SqlSelectCommand(String.format(getSecurityAliasQuery(revert), runIds));
			addSecurityAliasQueryParams(selectCommand, securityAlias);
			if (!revert) {
				ReconciliationMatchStatus notMatchedStatus = getReconciliationStatusService().getReconciliationMatchStatusByName(ReconciliationMatchStatusTypes.NOT_MATCHED.name());
				ReconciliationReconcileStatus notReconciledStatus = getReconciliationStatusService().getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name());
				selectCommand.addShortParameterValue(notMatchedStatus.getId()).addShortParameterValue(notReconciledStatus.getId());
			}
			List<Long> runObjectIds = getSqlHandler().executeSelect(selectCommand, rs -> {
				List<Long> result = new ArrayList<>();
				while (rs.next()) {
					result.add(rs.getLong("ReconciliationRunObjectID"));
				}
				return result;
			});
			if (!CollectionUtils.isEmpty(runObjectIds)) {
				List<ReconciliationRunObject> runObjectList = getReconciliationRunService().getReconciliationRunObjectList(runObjectIds.toArray(new Long[0]));
				Map<Integer, List<ReconciliationRunObject>> runObjectListMap = BeanUtils.getBeansMap(runObjectList, runObject -> runObject.getReconciliationRun().getId());
				for (Map.Entry<Integer, List<ReconciliationRunObject>> runObjectListMapEntry : CollectionUtils.getIterable(runObjectListMap.entrySet())) {
					ReconciliationRun run = getReconciliationRunService().getReconciliationRun(runObjectListMapEntry.getKey());
					getReconciliationRunService().reprocessReconciliationRun(run, runObjectListMapEntry.getValue(), new HashMap<>(), status);
				}
			}
		}
	}


	@Override
	public SqlSelectCommand addSecurityAliasQueryParams(SqlSelectCommand selectCommand, ReconciliationInvestmentSecurityAlias securityAlias) {
		return selectCommand.addIntegerParameterValue(securityAlias.getInvestmentSecurity().getId()).addStringParameterValue(securityAlias.getAlias());
	}


	@Override
	public String getSecurityAliasQuery(boolean revert) {
		return revert ? SECURITY_ALIAS_BASE_RUN_OBJECT_ID_QUERY : SECURITY_ALIAS_RUN_OBJECT_ID_QUERY;
	}


	private List<Integer> getApplicableRunIds(ReconciliationSource source, Date startDate, Date endDate) {
		List<Integer> runIdList = new ArrayList<>();
		List<Integer> definitionIdList = new ArrayList<>();
		ReconciliationRunSearchForm searchForm = new ReconciliationRunSearchForm();
		searchForm.setPrimaryOrSecondarySourceId(source.getId());
		searchForm.setRunDateAfterOrEquals(startDate);
		if (endDate != null) {
			searchForm.setRunDateBeforeOrEquals(endDate);
		}
		searchForm.setOrderBy("runDate:DESC");
		for (ReconciliationRun run : CollectionUtils.getIterable(getReconciliationRunService().getReconciliationRunList(searchForm))) {
			if (!definitionIdList.contains(run.getReconciliationDefinition().getId())) {
				runIdList.add(run.getId());
			}
			definitionIdList.add(run.getReconciliationDefinition().getId());
		}
		return runIdList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////           Reconciliation  Alias Utility Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	private boolean isAliasProcessingRequired(ReconciliationSource source, Date startDate, Date endDate, boolean newBean, boolean save) {
		if (source == null || startDate == null) {
			return false;
		}

		if (ReconciliationRunUtils.isMultipleRunsPerDefinition(null, source, startDate, null, getReconciliationRunService())) {
			return false;
		}
		if (save) {
			if (endDate != null && !newBean) {
				return false;
			}
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public ReconciliationDefinitionService getReconciliationDefinitionService() {
		return this.reconciliationDefinitionService;
	}


	public void setReconciliationDefinitionService(ReconciliationDefinitionService reconciliationDefinitionService) {
		this.reconciliationDefinitionService = reconciliationDefinitionService;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public ReconciliationStatusService getReconciliationStatusService() {
		return this.reconciliationStatusService;
	}


	public void setReconciliationStatusService(ReconciliationStatusService reconciliationStatusService) {
		this.reconciliationStatusService = reconciliationStatusService;
	}


	public AdvancedUpdatableDAO<ReconciliationInvestmentAccount, Criteria> getReconciliationInvestmentAccountDAO() {
		return this.reconciliationInvestmentAccountDAO;
	}


	public void setReconciliationInvestmentAccountDAO(AdvancedUpdatableDAO<ReconciliationInvestmentAccount, Criteria> reconciliationInvestmentAccountDAO) {
		this.reconciliationInvestmentAccountDAO = reconciliationInvestmentAccountDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationInvestmentSecurity, Criteria> getReconciliationInvestmentSecurityDAO() {
		return this.reconciliationInvestmentSecurityDAO;
	}


	public void setReconciliationInvestmentSecurityDAO(AdvancedUpdatableDAO<ReconciliationInvestmentSecurity, Criteria> reconciliationInvestmentSecurityDAO) {
		this.reconciliationInvestmentSecurityDAO = reconciliationInvestmentSecurityDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationInvestmentAccountAlias, Criteria> getReconciliationInvestmentAccountAliasDAO() {
		return this.reconciliationInvestmentAccountAliasDAO;
	}


	public void setReconciliationInvestmentAccountAliasDAO(AdvancedUpdatableDAO<ReconciliationInvestmentAccountAlias, Criteria> reconciliationInvestmentAccountAliasDAO) {
		this.reconciliationInvestmentAccountAliasDAO = reconciliationInvestmentAccountAliasDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationInvestmentSecurityAlias, Criteria> getReconciliationInvestmentSecurityAliasDAO() {
		return this.reconciliationInvestmentSecurityAliasDAO;
	}


	public void setReconciliationInvestmentSecurityAliasDAO(AdvancedUpdatableDAO<ReconciliationInvestmentSecurityAlias, Criteria> reconciliationInvestmentSecurityAliasDAO) {
		this.reconciliationInvestmentSecurityAliasDAO = reconciliationInvestmentSecurityAliasDAO;
	}


	public ReconciliationInvestmentAccountCache getReconciliationInvestmentAccountCache() {
		return this.reconciliationInvestmentAccountCache;
	}


	public void setReconciliationInvestmentAccountCache(ReconciliationInvestmentAccountCache reconciliationInvestmentAccountCache) {
		this.reconciliationInvestmentAccountCache = reconciliationInvestmentAccountCache;
	}


	public ReconciliationInvestmentSecurityCache getReconciliationInvestmentSecurityCache() {
		return this.reconciliationInvestmentSecurityCache;
	}


	public void setReconciliationInvestmentSecurityCache(ReconciliationInvestmentSecurityCache reconciliationInvestmentSecurityCache) {
		this.reconciliationInvestmentSecurityCache = reconciliationInvestmentSecurityCache;
	}


	public ReconciliationInvestmentAccountAliasCache getReconciliationInvestmentAccountAliasCache() {
		return this.reconciliationInvestmentAccountAliasCache;
	}


	public void setReconciliationInvestmentAccountAliasCache(ReconciliationInvestmentAccountAliasCache reconciliationInvestmentAccountAliasCache) {
		this.reconciliationInvestmentAccountAliasCache = reconciliationInvestmentAccountAliasCache;
	}


	public ReconciliationInvestmentSecurityAliasCache getReconciliationInvestmentSecurityAliasCache() {
		return this.reconciliationInvestmentSecurityAliasCache;
	}


	public void setReconciliationInvestmentSecurityAliasCache(ReconciliationInvestmentSecurityAliasCache reconciliationInvestmentSecurityAliasCache) {
		this.reconciliationInvestmentSecurityAliasCache = reconciliationInvestmentSecurityAliasCache;
	}
}
