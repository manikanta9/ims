package com.clifton.reconciliation.investment.validation;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.validation.ReconciliationValidator;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>Component</code> provides validation logic for {@link ReconciliationInvestmentSecurityAlias}es.
 *
 * @author stevenf on 8/11/2015.
 */
@Component
public class ReconciliationInvestmentSecurityAliasValidator extends ReconciliationValidator<ReconciliationInvestmentSecurityAlias> {

	private ReconciliationInvestmentService reconciliationInvestmentService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(ReconciliationInvestmentSecurityAlias bean, DaoEventTypes config) throws ValidationException {
		// NOTHING HERE - USES METHOD WITH DAO PARAM
	}


	@Override
	public void validate(ReconciliationInvestmentSecurityAlias securityAlias, DaoEventTypes config, ReadOnlyDAO<ReconciliationInvestmentSecurityAlias> securityAliasDAO) throws ValidationException {
		if (securityAlias == null) {
			throw new ValidationException("Security alias may not be null.");
		}

		ReconciliationSource reconciliationSource = securityAlias.getReconciliationSource();
		if (reconciliationSource == null) {
			throw new ValidationException("You must specify a source for the alias mapping!");
		}

		ValidationUtils.assertNotEquals(ReconciliationSource.DEFAULT_SOURCE, securityAlias.getReconciliationSource().getName(), "You may not create an alias for the '" + ReconciliationSource.DEFAULT_SOURCE + "' source!");

		if (getReconciliationInvestmentService().getReconciliationInvestmentSecurityByIdentifier(securityAlias.getAlias()) != null) {
			throw new ValidationException("Found valid security using identifier [" + securityAlias.getAlias() + "]!  You may not create aliases for valid securities - instead check your mapping rules.");
		}

		ReconciliationInvestmentSecurity investmentSecurity = securityAlias.getInvestmentSecurity();
		if (investmentSecurity == null) {
			throw new ValidationException("You must specify a security to map the alias to!");
		}

		ValidationUtils.assertBefore(securityAlias.getStartDate(), securityAlias.getEndDate(), "endDate");

		//Validate there is only one run per definition for the source/data of the alias being created (not utilizing endDate)
		if (config.isInsert() || config.isDelete() || securityAlias.getEndDate() == null) {
			catchMultipleRunsPerDefinition(null, securityAlias.getReconciliationSource(), securityAlias.getStartDate(), null);
		}
		if (config.isInsert() || config.isUpdate()) {
			List<ReconciliationInvestmentSecurityAlias> existingSecurityAliasList =
					securityAliasDAO.findByFields(
							new String[]{"reconciliationSource.id", "alias"},
							new Object[]{
									reconciliationSource.getId(),
									securityAlias.getAlias()
							});

			//Validate there are no conflicts
			for (ReconciliationInvestmentSecurityAlias existingSecurityAlias : CollectionUtils.getIterable(existingSecurityAliasList)) {
				if (!securityAlias.equals(existingSecurityAlias) && DateUtils.isOverlapInDates(securityAlias.getStartDate(), securityAlias.getEndDate(), existingSecurityAlias.getStartDate(), existingSecurityAlias.getEndDate())) {
					throw new ValidationException("A security alias already exists with an overlapping date range.  Please either edit the existing alias, or end it and start a new one.<br /><br />Existing alias: [" + existingSecurityAlias.getLabel() + "]");
				}
			}
			existingSecurityAliasList =
					securityAliasDAO.findByFields(
							new String[]{"reconciliationSource.id", "investmentSecurity.id"},
							new Object[]{
									reconciliationSource.getId(),
									securityAlias.getInvestmentSecurity().getId()
							});

			//Validate there are no conflicts
			for (ReconciliationInvestmentSecurityAlias existingSecurityAlias : CollectionUtils.getIterable(existingSecurityAliasList)) {
				if (!isDuplicateSecurityMappingAllowed(existingSecurityAlias.getInvestmentSecurity())) {
					if (!securityAlias.equals(existingSecurityAlias) && DateUtils.isOverlapInDates(securityAlias.getStartDate(), securityAlias.getEndDate(), existingSecurityAlias.getStartDate(), existingSecurityAlias.getEndDate())) {
						throw new ValidationException("Another security alias is already mapped to this security for this source during the specified date range.  Please either edit the existing alias, or end it and start a new one.<br /><br />Existing alias: [" + existingSecurityAlias.getLabel() + "]");
					}
				}
			}
			if (securityAlias.getEndDate() != null) {
				List<ReconciliationRun> runList = getReconciliationRunListBySourceAfterDate(securityAlias.getReconciliationSource(), securityAlias.getEndDate());
				if (CollectionUtils.getSize(runList) > 0) {
					StringBuilder errorMessage = new StringBuilder("Alias cannot be ended on ");
					errorMessage.append(DateUtils.fromDate(securityAlias.getEndDate(), DateUtils.DATE_FORMAT_INPUT));
					errorMessage.append(" because The following run(s) exist using this alias after that date: <br/>");
					for (int i = 0; i < runList.size(); i++) {
						errorMessage.append(runList.get(i).getLabel());
						if (i + 1 < runList.size()) {
							errorMessage.append(",<br/>");
						}
					}
					throw new ValidationException(errorMessage.toString());
				}
			}
		}
	}


	/**
	 * Broke this out as a method in case this check gets more involved in the future...
	 */
	private boolean isDuplicateSecurityMappingAllowed(ReconciliationInvestmentSecurity investmentSecurity) {
		return "Currency".equals(investmentSecurity.getSecurityTypeName());
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}
}
