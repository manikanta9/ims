package com.clifton.reconciliation.rule.validation;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.ReconciliationDefinitionFieldMapping;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluator;
import com.clifton.reconciliation.definition.rule.ReconciliationFilter;
import com.clifton.reconciliation.rule.ReconciliationRule;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.usedby.SystemUsedByEntity;
import com.clifton.system.usedby.SystemUsedByService;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The <code>ReconciliationRuleValidator</code> validates ReconciliationRule objects
 * We purposely suppress rawtype warnings here, as ReconciliationRuleFunction, ReconciliationRuleCondition objects
 * are all passed as unparameterized ReconciliationRule objects when saved and the proper type information can not be bound.
 *
 * @author stevenf
 */
@Component
@SuppressWarnings("rawtypes")
public class ReconciliationRuleValidator extends SelfRegisteringDaoValidator<ReconciliationRule> {

	private ReconciliationRunService reconciliationRunService;
	private SystemUsedByService systemUsedByService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(ReconciliationRule rule, DaoEventTypes config) throws ValidationException {
		if (rule == null) {
			throw new ValidationException("Rule may not be null.");
		}
		if (!rule.isNewBean()) {
			Set<Integer> definitionIdList = new HashSet<>();
			List<SystemUsedByEntity> usedByEntityList = getSystemUsedByService().getSystemUsedByEntityList("ReconciliationRule", ((Number) rule.getId()).intValue());
			for (SystemUsedByEntity usedByEntity : CollectionUtils.getIterable(usedByEntityList)) {
				SystemTable table = usedByEntity.getColumn().getTable();
				ReadOnlyDAO<?> dao = getDaoLocator().locate(table.getName());
				IdentityObject daoObject = dao.findByPrimaryKey(usedByEntity.getUsedById());
				if (daoObject instanceof ReconciliationFilter) {
					definitionIdList.add(((ReconciliationFilter) daoObject).getReconciliationDefinition().getId());
				}
				else if (daoObject instanceof ReconciliationEvaluator) {
					definitionIdList.add(((ReconciliationEvaluator) daoObject).getDefinitionField().getReconciliationDefinition().getId());
				}
				else if (daoObject instanceof ReconciliationDefinitionFieldMapping) {
					definitionIdList.add(((ReconciliationDefinitionFieldMapping) daoObject).getDefinitionField().getReconciliationDefinition().getId());
				}
			}
			if (!CollectionUtils.isEmpty(definitionIdList)) {
				ReconciliationRunSearchForm searchForm = new ReconciliationRunSearchForm();
				searchForm.setDefinitionIds(definitionIdList.toArray(new Integer[0]));
				if (!CollectionUtils.isEmpty(getReconciliationRunService().getReconciliationRunList(searchForm))) {
					throw new ValidationException("You cannot modify a rule when it is associated with a definition that has existing import runs.  Expire the field, filter, or comparator referencing the rule, and create a new one!");
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public SystemUsedByService getSystemUsedByService() {
		return this.systemUsedByService;
	}


	public void setSystemUsedByService(SystemUsedByService systemUsedByService) {
		this.systemUsedByService = systemUsedByService;
	}
}
