package com.clifton.reconciliation.rule;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.reconciliation.rule.search.ReconciliationRuleGroupSearchForm;
import com.clifton.reconciliation.rule.search.ReconciliationRuleSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>ReconciliationRuleService</code> interface defines methods for managing conditions.
 *
 * @author StevenF on 11/23/2016.
 */
@Service
public class ReconciliationRuleServiceImpl implements ReconciliationRuleService {

	private AdvancedUpdatableDAO<ReconciliationRule<?>, Criteria> reconciliationRuleDAO;
	private AdvancedUpdatableDAO<ReconciliationRuleGroup, Criteria> reconciliationRuleGroupDAO;

	////////////////////////////////////////////////////////////////////////////
	////////    Reconciliation Rule Group Service Business Methods     /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationRuleGroup getReconciliationRuleGroup(short id) {
		return getReconciliationRuleGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public ReconciliationRuleGroup getReconciliationRuleGroupByName(String name) {
		return getReconciliationRuleGroupDAO().findOneByField("name", name);
	}


	@Override
	public List<ReconciliationRuleGroup> getReconciliationRuleGroupList(ReconciliationRuleGroupSearchForm searchForm) {
		return getReconciliationRuleGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReconciliationRuleGroup saveReconciliationRuleGroup(ReconciliationRuleGroup ruleGroup) {
		return getReconciliationRuleGroupDAO().save(ruleGroup);
	}


	@Override
	public void deleteReconciliationRuleGroup(short id) {
		getReconciliationRuleGroupDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////       Reconciliation Rule Service Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public ReconciliationRule<?> getReconciliationRule(int id) {
		return getReconciliationRuleDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconciliationRule<?>> getReconciliationRuleList(ReconciliationRuleSearchForm searchForm) {
		return getReconciliationRuleDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<ReconciliationRule<?>> getReconciliationRuleListByGroupId(short groupId) {
		return getReconciliationRuleDAO().findByField("ruleGroup.id", groupId);
	}


	@Override
	public ReconciliationRule<?> saveReconciliationRule(ReconciliationRule<?> reconciliationRule) {
		return getReconciliationRuleDAO().save(reconciliationRule);
	}


	@Override
	public void deleteReconciliationRule(int id) {
		getReconciliationRuleDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ReconciliationRule<?>, Criteria> getReconciliationRuleDAO() {
		return this.reconciliationRuleDAO;
	}


	public void setReconciliationRuleDAO(AdvancedUpdatableDAO<ReconciliationRule<?>, Criteria> reconciliationRuleDAO) {
		this.reconciliationRuleDAO = reconciliationRuleDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationRuleGroup, Criteria> getReconciliationRuleGroupDAO() {
		return this.reconciliationRuleGroupDAO;
	}


	public void setReconciliationRuleGroupDAO(AdvancedUpdatableDAO<ReconciliationRuleGroup, Criteria> reconciliationRuleGroupDAO) {
		this.reconciliationRuleGroupDAO = reconciliationRuleGroupDAO;
	}
}
