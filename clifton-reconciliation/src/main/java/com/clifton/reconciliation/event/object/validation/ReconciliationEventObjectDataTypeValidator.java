package com.clifton.reconciliation.event.object.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectDataType;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.springframework.stereotype.Component;


/**
 * The <code>BusinessCompanyTypeValidator</code> ...
 *
 * @author Mary Anderson
 */
@Component
public class ReconciliationEventObjectDataTypeValidator extends SelfRegisteringDaoValidator<ReconciliationEventObjectDataType> {

	private SecurityAuthorizationService securityAuthorizationService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(ReconciliationEventObjectDataType dataType, DaoEventTypes config) throws ValidationException {
		ReconciliationEventObjectDataType originalDataType = null;
		if (!config.isInsert()) {
			originalDataType = getOriginalBean(dataType);
		}
		if (config.isDelete()) {
			if (originalDataType != null && originalDataType.isSystemDefined()) {
				throw new ValidationException("Cannot delete a system defined event object data type.");
			}
		}
		else if (config.isUpdate()) {
			if (originalDataType != null && originalDataType.isSystemDefined()) {
				if (!getSecurityAuthorizationService().isSecurityUserAdmin()) {
					throw new ValidationException("Administrators only can update a system defined event object data type.");
				}
				if (!originalDataType.getName().equals(dataType.getName())) {
					throw new ValidationException("Cannot change the name of a system defined event object data type.");
				}
				if (!dataType.isSystemDefined()) {
					throw new ValidationException("Cannot change a system defined event object data type to not system defined.");
				}
			}
			else if (dataType.isSystemDefined()) {
				throw new ValidationException("Cannot change a non system defined event object data type to system defined.");
			}
		}
		else if (config.isInsert()) {
			if (dataType.isSystemDefined()) {
				throw new ValidationException("Cannot create a system defined event object data type.");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
