package com.clifton.reconciliation.event.object.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.event.object.ReconciliationEventObject;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectAccount;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectJournal;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunService;
import org.springframework.stereotype.Component;


/**
 * The <code>ReconciliationEventObjectValidator</code> ...
 *
 * @author stevenf
 */
@Component
@SuppressWarnings("rawtypes")
public class ReconciliationEventObjectValidator extends SelfRegisteringDaoValidator<ReconciliationEventObject> {

	private ReconciliationRunService reconciliationRunService;

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.DELETE;
	}


	@Override
	public void validate(ReconciliationEventObject eventObject, DaoEventTypes config) throws ValidationException {
		if (config.isDelete()) {
			if (eventObject instanceof ReconciliationEventObjectJournal) {
				ReconciliationRun run = getReconciliationRunService().getReconciliationRunObject(eventObject.getRunObject().getId()).getReconciliationRun();
				ValidationUtils.assertNull(run.getApprovedByUser(), "You may not delete events associated with a run that has been approved: [" + run.getLabel() + "]");
				if (eventObject.getParentRunObject() != null) {
					ReconciliationRun parentRun = getReconciliationRunService().getReconciliationRunObject(eventObject.getParentRunObject().getId()).getReconciliationRun();
					ValidationUtils.assertNull(parentRun.getApprovedByUser(), "You may not delete events associated with a parent run that has been approved: [" + run.getLabel() + "]");
				}
			}
			else if (eventObject instanceof ReconciliationEventObjectAccount) {
				ValidationUtils.fail(() -> "You may not delete account events!");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////

	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}
}
