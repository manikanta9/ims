package com.clifton.reconciliation.event.jms;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.event.object.ReconciliationEventObject;


/**
 * @author stevenf
 */
public interface ReconciliationEventService {

	public Status processReconciliationEvent(ReconciliationEventCommand command);


	@DoNotAddRequestMapping
	public void sendReconciliationEventMessage(ReconciliationEventObject<?> eventObject);
}
