package com.clifton.reconciliation.event.jms;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.group.ReconciliationDefinitionGroup;
import com.clifton.reconciliation.definition.group.ReconciliationDefinitionGroupService;
import com.clifton.reconciliation.event.jms.message.ReconciliationEventRequestMessage;
import com.clifton.reconciliation.event.object.ReconciliationEventObject;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectAccount;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectCategories;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectService;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectAccountData;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectData;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectDataType;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectJournalData;
import com.clifton.reconciliation.event.object.search.ReconciliationEventObjectSearchForm;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.clifton.core.util.status.Status.ofMessage;


/**
 * @author stevenf
 */
@Service
public class ReconciliationEventServiceImpl implements ReconciliationEventService {

	private static final String ACCOUNT_UNLOCK_BASE_QUERY = "SELECT ria.AccountIdentifier, ria.AccountNumber " +
			"FROM ReconciliationInvestmentAccount ria " +
			"WHERE ReconciliationInvestmentAccountID IN (" +
			"   SELECT ReconciliationInvestmentAccountID " +
			"   FROM ReconciliationRunObject " +
			"   WHERE ReconciliationRunID IN (%s) AND ReconciliationReconcileStatusID IN (" +
			"       SELECT ReconciliationReconcileStatusID " +
			"       FROM ReconciliationReconcileStatus " +
			"       WHERE IsReconciled = 1" +
			"   )" +
			"   GROUP BY ReconciliationInvestmentAccountID" +
			") " +
			"AND ReconciliationInvestmentAccountID NOT IN (" +
			"   SELECT ReconciliationInvestmentAccountID " +
			"   FROM ReconciliationRunObject " +
			"   WHERE ReconciliationRunID IN (%s) AND ReconciliationReconcileStatusID IN (" +
			"       SELECT ReconciliationReconcileStatusID " +
			"       FROM ReconciliationReconcileStatus " +
			"       WHERE IsReconciled = 0" +
			"   ) AND ReconciliationInvestmentAccountID IS NOT NULL " +
			"   GROUP BY ReconciliationInvestmentAccountID" +
			")";

	private static final String ACCOUNT_UNLOCK_GROUP_BY_QUERY_CLAUSE = " GROUP BY AccountIdentifier, AccountNumber;";

	private SqlHandler sqlHandler;
	private RunnerHandler runnerHandler;
	private CalendarBusinessDayService calendarBusinessDayService;
	private ReconciliationDefinitionGroupService reconciliationDefinitionGroupService;
	private ReconciliationEventObjectService reconciliationEventObjectService;
	private ReconciliationRunService reconciliationRunService;
	private ReconciliationInvestmentService reconciliationInvestmentService;
	private AsynchronousMessageHandler reconciliationEventJmsMessageHandler;
	private SecurityUserService securityUserService;


	@Override
	//TODO - make this private after journal events are consolidated here (UI should post to journal specific method?
	public Status processReconciliationEvent(ReconciliationEventCommand command) {
		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : ofMessage("Synchronously running for " + command);
			doProcessReconciliationEventObjectList(command, status);
			if (command.isThrowExceptionOnError() && status.getErrorCount() > 0) {
				throw new RuntimeException(status.getMessageWithErrors());
			}
			return status;
		}
		else {
			final Date runDate = DateUtils.addSeconds(new Date(), command.getAsynchronousRebuildDelay());
			final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + DateUtils.fromDate(runDate));
			Runner runner = new AbstractStatusAwareRunner("RECONCILIATION-EVENTS", command.getRunId(), runDate, statusHolder) {

				@Override
				public void run() {
					try {
						ValidationUtils.assertNotNull(command.getRunDate(), "Run Date is required for event object processing.");
						doProcessReconciliationEventObjectList(command, statusHolder.getStatus());
					}
					catch (Exception e) {
						getStatus().setMessage("Error processing events for " + command + ": " + ExceptionUtils.getDetailedMessage(e));
						getStatus().addError(ExceptionUtils.getDetailedMessage(e));
						LogUtils.errorOrInfo(getClass(), "Error reconciling for " + command, e);
					}
				}
			};
			getRunnerHandler().rescheduleRunner(runner);
			return Status.ofMessage("Started process events on " + DateUtils.fromDateShort(command.getRunDate()) + ". Processing will be completed shortly.");
		}
	}


	/**
	 *
	 */
	private void doProcessReconciliationEventObjectList(ReconciliationEventCommand command, Status status) {
		AssertUtils.assertNotNull(command, "Reconciliation Event Object command is required.");
		ValidationUtils.assertNotNull(command.getRunDate(), "Reconciliation Date is required.");
		if (ReconciliationEventObjectCategories.ACCOUNT.name().equals(command.getEventObjectCategory().name())) {
			ValidationUtils.assertNotNull(command.getDefinitionGroupId(), "Definition Group ID may not be null!");
			ReconciliationDefinitionGroup definitionGroup = getReconciliationDefinitionGroupService().getReconciliationDefinitionGroup(command.getDefinitionGroupId());
			ValidationUtils.assertNotNull(definitionGroup, () -> String.format("Reconciliation Definition Group [%s] cannot be found ", command.getDefinitionGroupId()));

			if (!command.isAccountsAndPositions()) {
				ReconciliationRunSearchForm runSearchForm = new ReconciliationRunSearchForm();
				runSearchForm.setRunDate(command.getRunDate());
				runSearchForm.setSourceName(command.getSourceName());
				runSearchForm.setDefinitionGroupId(command.getDefinitionGroupId());
				processReconciliationRunList(getReconciliationRunService().getReconciliationRunList(runSearchForm), command, status);
			}
			else {
				//TODO - different query/processing for account and positions
			}
		}
		else {
			//TODO - consolidate the account journal logic to this service and command logic - for now left alone in the EventObjectService - but I don't like it...
			throw new ValidationException("Unsupported event object type [" + command.getEventObjectCategory().name() + "]");
		}
	}


	private Status processReconciliationRunList(List<ReconciliationRun> runList, ReconciliationEventCommand command, Status status) {
		int messagesSent = 0;
		if (!CollectionUtils.isEmpty(runList)) {
			Date unlockDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(command.getRunDate()), 1);
			String runIds = runList.stream().map(run -> "'" + run.getId() + "'").collect(Collectors.joining(","));
			String accountUnlockQuery = String.format(ACCOUNT_UNLOCK_BASE_QUERY, runIds, runIds);
			accountUnlockQuery += ACCOUNT_UNLOCK_GROUP_BY_QUERY_CLAUSE;
			Map<Integer, String> accountNumberMap = getSqlHandler().executeSelect(accountUnlockQuery, rs -> {
				Map<Integer, String> result = new HashMap<>();
				while (rs.next()) {
					result.put(rs.getInt("AccountIdentifier"), rs.getString("AccountNumber"));
				}
				return result;
			});
			if (!command.isSendAllEvents()) {
				removeSentMessages(accountNumberMap, unlockDate);
			}
			for (Map.Entry<Integer, String> accountNumberEntry : CollectionUtils.getIterable(accountNumberMap.entrySet())) {
				Integer accountIdentifier = accountNumberEntry.getKey();
				String accountNumber = accountNumberEntry.getValue();
				ReconciliationEventObjectDataType eventObjectDataType = getReconciliationEventObjectService().getReconciliationEventObjectDataTypeByName("Unlock");

				ReconciliationEventObjectAccountData eventObjectAccountData = new ReconciliationEventObjectAccountData();
				eventObjectAccountData.setEventDate(unlockDate);
				eventObjectAccountData.setEventNote(command.getEventNote() != null ? command.getEventNote() : eventObjectDataType.getDefaultNote());
				eventObjectAccountData.setHoldingAccountNumber(accountNumber);
				eventObjectAccountData.setSourceTable("ReconciliationInvestmentAccount");
				eventObjectAccountData.setSourceFkFieldId(accountIdentifier.longValue());

				ReconciliationEventObjectAccount eventObjectAccount = new ReconciliationEventObjectAccount();
				eventObjectAccount.setEventObjectData(eventObjectAccountData);
				eventObjectAccount.setEventObjectDataType(eventObjectDataType);
				sendReconciliationEventMessage(getReconciliationEventObjectService().saveReconciliationEventObject(eventObjectAccount));
				messagesSent++;
			}
		}
		status.setMessage("Sent " + messagesSent + " unlock messages!");
		return status;
	}


	private void removeSentMessages(Map<Integer, String> accountNumberMap, Date unlockDate) {
		ReconciliationEventObjectSearchForm eventObjectSearchForm = new ReconciliationEventObjectSearchForm();
		eventObjectSearchForm.setDataTypeCategory(ReconciliationEventObjectCategories.ACCOUNT.name());
		eventObjectSearchForm.setCreateDate(unlockDate);
		List<ReconciliationEventObject<?>> eventObjectList = getReconciliationEventObjectService().getReconciliationEventObjectList(eventObjectSearchForm);
		for (ReconciliationEventObject<?> eventObject : CollectionUtils.getIterable(eventObjectList)) {
			if (eventObject != null && eventObject.getEventObjectData() != null && eventObject.getEventObjectData().getSourceFkFieldId() != null) {
				accountNumberMap.remove(eventObject.getEventObjectData().getSourceFkFieldId().intValue());
			}
		}
	}


	@Override
	public void sendReconciliationEventMessage(ReconciliationEventObject<?> eventObject) {
		ReconciliationEventRequestMessage<ReconciliationEventObjectData> requestMessage = new ReconciliationEventRequestMessage<>();
		requestMessage.setUserName(getSecurityUserService().getSecurityUserCurrent().getUserName());
		requestMessage.setEventName(eventObject.getEventObjectDataType().getEventObjectCategory().name());
		requestMessage.setEventTypeName(eventObject.getEventObjectDataType().getName());
		requestMessage.setEventObjectId(eventObject.getId());
		requestMessage.setEventObjectData(eventObject.getEventObjectData());
		if (eventObject.getEventObjectData() instanceof ReconciliationEventObjectJournalData) {
			ReconciliationEventObjectJournalData journalData = (ReconciliationEventObjectJournalData) eventObject.getEventObjectData();
			//Look up the ReconciliationInvestment account using cache and force the user of 'AccountNumber' - this is because the value could have been
			// mapped using 'AccountNumber2', but the IMS journal must be created using 'AccountNumber'
			//Also translate securityIdentifier to symbol for use in IMS.
			if (!StringUtils.isEmpty(journalData.getHoldingAccountNumber())) {
				ReconciliationInvestmentAccount investmentAccount = getReconciliationInvestmentService().getReconciliationInvestmentAccountByNumber(journalData.getHoldingAccountNumber());
				if (investmentAccount != null) {
					journalData.setHoldingAccountNumber(investmentAccount.getAccountNumber());
				}
			}
			if (!StringUtils.isEmpty(journalData.getSecurityIdentifier())) {
				ReconciliationInvestmentSecurity investmentSecurity = getReconciliationInvestmentService().getReconciliationInvestmentSecurityByIdentifier(journalData.getSecurityIdentifier());
				if (investmentSecurity != null) {
					journalData.setSecurityIdentifier(investmentSecurity.getSecuritySymbol());
				}
			}
			if (eventObject.getEventObjectDataType().isAbsolute()) {
				journalData.setAmount(MathUtils.abs(journalData.getAmount()));
			}
		}
		//Potentially could create a an ReconciliationEventTargetApplication table to allow for broadcasting to additional targets.
		requestMessage.getProperties().put(MessagingMessage.TARGET_APPLICATION, "IMS");
		getReconciliationEventJmsMessageHandler().send(requestMessage);
	}

	////////////////////////////////////////////////////////////////////////////
	//////////              Getter & Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public ReconciliationDefinitionGroupService getReconciliationDefinitionGroupService() {
		return this.reconciliationDefinitionGroupService;
	}


	public void setReconciliationDefinitionGroupService(ReconciliationDefinitionGroupService reconciliationDefinitionGroupService) {
		this.reconciliationDefinitionGroupService = reconciliationDefinitionGroupService;
	}


	public ReconciliationEventObjectService getReconciliationEventObjectService() {
		return this.reconciliationEventObjectService;
	}


	public void setReconciliationEventObjectService(ReconciliationEventObjectService reconciliationEventObjectService) {
		this.reconciliationEventObjectService = reconciliationEventObjectService;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public AsynchronousMessageHandler getReconciliationEventJmsMessageHandler() {
		return this.reconciliationEventJmsMessageHandler;
	}


	public void setReconciliationEventJmsMessageHandler(AsynchronousMessageHandler reconciliationEventJmsMessageHandler) {
		this.reconciliationEventJmsMessageHandler = reconciliationEventJmsMessageHandler;
	}


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
