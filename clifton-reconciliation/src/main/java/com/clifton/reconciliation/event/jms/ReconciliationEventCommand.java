package com.clifton.reconciliation.event.jms;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectCategories;

import java.util.Date;


/**
 * @author stevenf
 */
public class ReconciliationEventCommand {

	private boolean synchronous;
	private boolean throwExceptionOnError;

	private boolean accountsAndPositions;
	/**
	 * Used to determine in some cases if all events should be sent or not.
	 * e.g. For account unlocking, by default only unlock events that have not already been sent are sent.
	 * In some cases, you may want to force sending all events again from the 'Event Processing' admin screen.
	 */
	private boolean sendAllEvents;

	private int asynchronousDelay;
	private int asynchronousRebuildDelay;

	private Date runDate;
	private String eventNote;
	private String sourceName;
	private Integer definitionGroupId;
	private ReconciliationEventObjectCategories eventObjectCategory;

	private Status status;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static ReconciliationEventCommand ofSynchronousWithThrow() {
		ReconciliationEventCommand result = new ReconciliationEventCommand();
		result.setSynchronous(true);
		result.setThrowExceptionOnError(true);
		return result;
	}


	public String getRunId() {
		String runId = "";
		if (getEventObjectCategory() != null) {
			runId = "TYPE_" + getEventObjectCategory();
		}
		if (getDefinitionGroupId() != null) {
			runId += "GROUP_" + getDefinitionGroupId();
		}
		if (getSourceName() != null) {
			runId += "SOURCE_" + getSourceName();
		}
		else {
			runId += "_ALL_";
		}
		runId += DateUtils.fromDate(getRunDate(), DateUtils.DATE_FORMAT_INPUT);
		return runId;
	}


	@Override
	public String toString() {
		return "ReconciliationEventCommand{" +
				"runDate=" + this.runDate +
				", sourceName=" + this.sourceName +
				", definitionGroup=" + this.definitionGroupId +
				", eventObjectCategory=" + this.eventObjectCategory +
				'}';
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isAccountsAndPositions() {
		return this.accountsAndPositions;
	}


	public void setAccountsAndPositions(boolean accountsAndPositions) {
		this.accountsAndPositions = accountsAndPositions;
	}


	public boolean isSendAllEvents() {
		return this.sendAllEvents;
	}


	public void setSendAllEvents(boolean sendAllEvents) {
		this.sendAllEvents = sendAllEvents;
	}


	public Date getRunDate() {
		return this.runDate;
	}


	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}


	public String getSourceName() {
		return this.sourceName;
	}


	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}


	public Integer getDefinitionGroupId() {
		return this.definitionGroupId;
	}


	public void setDefinitionGroupId(Integer definitionGroupId) {
		this.definitionGroupId = definitionGroupId;
	}


	public String getEventNote() {
		return this.eventNote;
	}


	public void setEventNote(String eventNote) {
		this.eventNote = eventNote;
	}


	public ReconciliationEventObjectCategories getEventObjectCategory() {
		return this.eventObjectCategory;
	}


	public void setEventObjectCategory(ReconciliationEventObjectCategories eventObjectCategory) {
		this.eventObjectCategory = eventObjectCategory;
	}


	public int getAsynchronousDelay() {
		return this.asynchronousDelay;
	}


	public void setAsynchronousDelay(int asynchronousDelay) {
		this.asynchronousDelay = asynchronousDelay;
	}


	public int getAsynchronousRebuildDelay() {
		return this.asynchronousRebuildDelay;
	}


	public void setAsynchronousRebuildDelay(int asynchronousRebuildDelay) {
		this.asynchronousRebuildDelay = asynchronousRebuildDelay;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public boolean isThrowExceptionOnError() {
		return this.throwExceptionOnError;
	}


	public void setThrowExceptionOnError(boolean throwExceptionOnError) {
		this.throwExceptionOnError = throwExceptionOnError;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
