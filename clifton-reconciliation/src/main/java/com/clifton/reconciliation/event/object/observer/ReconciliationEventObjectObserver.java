package com.clifton.reconciliation.event.object.observer;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.type.ReconciliationType;
import com.clifton.reconciliation.event.object.ReconciliationEventObject;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectJournal;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectStatuses;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectJournalData;
import com.clifton.reconciliation.reconcile.ReconciliationReconcileService;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.data.DataNoteTypes;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <code>ReconciliationEventObjectObserver</code> listen for changes to the {@link ReconciliationEventObject#getStatus()} and trigger a check for automatic reconciliation
 * of the associated run.
 *
 * @author TerryS
 */
@Component
@SuppressWarnings("rawtypes")
public class ReconciliationEventObjectObserver extends SelfRegisteringDaoObserver<ReconciliationEventObject> {

	private ReconciliationRunService reconciliationRunService;
	private ReconciliationReconcileService reconciliationReconcileService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	/**
	 * Update the transaction event's parent cash run object
	 * <p>
	 * The method is invoked before corresponding DAO method is called.
	 * The method is executed inside of DAO transaction.
	 */
	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<ReconciliationEventObject> dao, DaoEventTypes event, ReconciliationEventObject eventObject) {
		AssertUtils.assertNotNull(eventObject, "Reconciliation Event Object is required.");
		ReconciliationRunObject runObject = eventObject.getRunObject();
		// doesn't already have a parent, its a journal event, and has a run object.
		if (eventObject instanceof ReconciliationEventObjectJournal && runObject != null && !runObject.isNewBean() && eventObject.getParentRunObject() == null) {
			runObject = getReconciliationRunService().getReconciliationRunObject(runObject.getId());
			AssertUtils.assertNotNull(runObject, "The Reconciliation Run Object [%s] does not exist", runObject.getId());

			ReconciliationRun run = runObject.getReconciliationRun();
			// only transactions have a parent definition.
			if (run != null && run.getReconciliationDefinition() != null && run.getReconciliationDefinition().getParentDefinition() != null) {
				ReconciliationEventObjectJournalData eventObjectJournalData = (ReconciliationEventObjectJournalData) eventObject.getEventObjectData();
				ValidationUtils.assertNotNull(eventObjectJournalData.getSettlementDate(), "The journal event should have a settlement date.");
				eventObject.setParentRunObject(getParentRunObject(runObject, run.getRunDate()));
			}
		}
	}


	private ReconciliationRunObject getParentRunObject(ReconciliationRunObject childRunObject, Date runDate) {
		List<ReconciliationRunObject> parentRunObjectList = getReconciliationRunService().getReconciliationParentRunObjectList(childRunObject.getId(), runDate);
		String errorMessage = String.format("Expected one parent run object but found %s for run object with identifier '%s' for  holding account '%s'.",
				parentRunObjectList.stream().map(ReconciliationRunObject::getId).collect(Collectors.toList()), childRunObject.getIdentifier(),
				childRunObject.getAccountNumber());
		try {
			return CollectionUtils.getFirstElementStrict(parentRunObjectList, errorMessage, errorMessage);
		}
		catch (Exception e) {
			//Catches and throws as a validation exception so that original cause is sent back - anything other than a validation exception in an observer fails to bubble back properly.
			throw new ValidationException(e.getMessage());
		}
	}


	/**
	 * Attempt to auto-reconcile when receiving COMPLETE journal event objects.
	 * <p>
	 * The method is invoked after corresponding DAO method is called and transaction was committed.
	 * If the DAO method raises exception, it will be passed (not null) as the last argument.
	 */
	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<ReconciliationEventObject> dao, DaoEventTypes event, ReconciliationEventObject bean, Throwable e) {
		// skip errors
		if (e == null) {
			// only for journal events
			if (bean instanceof ReconciliationEventObjectJournal) {
				ReconciliationEventObject originalBean = getOriginalBean(dao, bean);
				// only if the status has been changed.
				if (originalBean != null && originalBean.getStatus() != bean.getStatus() && bean.getStatus() != null) {
					// only if the status has been changed to complete
					if (ReconciliationEventObjectStatuses.COMPLETE == bean.getStatus()) {
						// update adjusted cash difference
						ReconciliationEventObjectJournal eventObjectJournal = (ReconciliationEventObjectJournal) bean;
						String note = eventObjectJournal.getEventObjectData().getEventNote();
						ReconciliationRunObject runObject = eventObjectJournal.getRunObject();
						ReconciliationDefinition reconciliationDefinition = eventObjectJournal.getRunObject().getReconciliationRun().getReconciliationDefinition();
						if ((ReconciliationType.TRANSACTION.equals(reconciliationDefinition.getType().getName()) && reconciliationDefinition.getParentDefinition() != null
								&& ReconciliationType.CASH.equals(reconciliationDefinition.getParentDefinition().getType().getName()))) {
							// adjustment was performed on a transaction object
							if (eventObjectJournal.getParentRunObject() != null) {
								//Copy note to the objects and save.

								runObject.addDataNote(note, DataNoteTypes.ADJUSTMENT, eventObjectJournal.getCreateUserId());
								getReconciliationRunService().saveReconciliationRunObject(runObject);

								//Retrieve the parent object fresh to avoid a stale object exception.
								ReconciliationRunObject parentRunObject = getReconciliationRunService().getReconciliationRunObject(eventObjectJournal.getParentRunObject().getId());
								parentRunObject.addDataNote(note, DataNoteTypes.ADJUSTMENT, eventObjectJournal.getCreateUserId());
								getReconciliationRunService().saveReconciliationRunObject(parentRunObject);

								//Now run the reconcile process
								getReconciliationReconcileService().reconcileReconciliationRun(parentRunObject.getReconciliationRun(), new Long[]{runObject.getId(), parentRunObject.getId()});
							}
						}
						else if (ReconciliationType.CASH.equals(reconciliationDefinition.getType().getName())) {
							//Copy note to the object and save.
							runObject.addDataNote(note, DataNoteTypes.ADJUSTMENT, eventObjectJournal.getCreateUserId());
							getReconciliationRunService().saveReconciliationRunObject(runObject);
							//Now run the reconcile process
							getReconciliationReconcileService().reconcileReconciliationRun(runObject.getReconciliationRun(), new Long[]{runObject.getId()});
						}
						else {
							LogUtils.warn(ReconciliationEventObjectObserver.class, String.format("Could not auto reconcile: expected a transaction run object [%s] with a cash parent.", eventObjectJournal.getRunObject().getId()));
						}
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public ReconciliationReconcileService getReconciliationReconcileService() {
		return this.reconciliationReconcileService;
	}


	public void setReconciliationReconcileService(ReconciliationReconcileService reconciliationReconcileService) {
		this.reconciliationReconcileService = reconciliationReconcileService;
	}
}
