package com.clifton.reconciliation.event.object;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.event.jms.ReconciliationEventService;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectDataType;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectJournalData;
import com.clifton.reconciliation.event.object.data.search.ReconciliationEventObjectDataTypeSearchForm;
import com.clifton.reconciliation.event.object.search.ReconciliationEventObjectSearchForm;
import com.clifton.reconciliation.run.ReconciliationRunService;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.IntegerType;
import org.hibernate.type.Type;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author stevenf
 */
@Service
public class ReconciliationEventObjectServiceImpl implements ReconciliationEventObjectService {

	private ReconciliationRunService reconciliationRunService;
	private ReconciliationEventService reconciliationEventService;

	private AdvancedUpdatableDAO<ReconciliationEventObject<?>, Criteria> reconciliationEventObjectDAO;
	private AdvancedUpdatableDAO<ReconciliationEventObjectDataType, Criteria> reconciliationEventObjectDataTypeDAO;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationEventObject<?> getReconciliationEventObject(int id) {
		return getReconciliationEventObjectDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconciliationEventObject<?>> getReconciliationEventObjectListByRunId(int runId) {
		ReconciliationEventObjectSearchForm searchForm = new ReconciliationEventObjectSearchForm();
		searchForm.setRunId(runId);
		return getReconciliationEventObjectList(searchForm);
	}


	@Override
	public List<ReconciliationEventObject<?>> getReconciliationEventObjectListByRunObjectId(long runObjectId) {
		ReconciliationEventObjectSearchForm searchForm = new ReconciliationEventObjectSearchForm();
		searchForm.setRunObjectId(runObjectId);
		return getReconciliationEventObjectList(searchForm);
	}


	/**
	 * Try/Catch is for in memory db tests - unable to support compressed text fields used on this search form...better way?
	 */
	@Override
	public List<ReconciliationEventObject<?>> getReconciliationEventObjectList(ReconciliationEventObjectSearchForm searchForm) {
		try {
			HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

				@Override
				public void configureCriteria(Criteria criteria) {
					super.configureCriteria(criteria);
					if (searchForm.getRunOrParentRunId() != null) {
						criteria.add(Restrictions.sqlRestriction("(ReconciliationRunObjectID IN (SELECT ReconciliationRunObjectID FROM ReconciliationRunObject WHERE ReconciliationRunID = ?) OR ParentRunObjectID IN (SELECT ReconciliationRunObjectID FROM ReconciliationRunObject WHERE ReconciliationRunID = ?))", new Object[]{searchForm.getRunOrParentRunId(), searchForm.getRunOrParentRunId()}, new Type[]{new IntegerType(), new IntegerType()}));
					}
				}
			};
			return getReconciliationEventObjectDAO().findBySearchCriteria(config);
		}
		catch (Exception e) {
			LogUtils.error(getClass(), "Failed to retrieve event object list!", e);
		}
		return new ArrayList<>();
	}


	@Override
	public Status saveReconciliationEventObjectList(ReconciliationEventObjectWrapper<?> eventObjectWrapper) {
		Status status = new Status();
		for (ReconciliationEventObject<?> eventObject : CollectionUtils.getIterable(eventObjectWrapper.getEventObjectList())) {
			try {
				if (eventObject.getRunObject() != null) {
					//Fully hydrate the runObject from the id
					eventObject.setRunObject(getReconciliationRunService().getReconciliationRunObject(eventObject.getRunObject().getId()));
				}
				if (eventObject.getParentRunObject() != null) {
					//Fully hydrate the parentRunObject from the id
					eventObject.setParentRunObject(getReconciliationRunService().getReconciliationRunObject(eventObject.getParentRunObject().getId()));
				}
				IdentityObject identityObject = getReconciliationEventObjectDAO().findOneByFields(new String[]{"runObject.id", "eventObjectDataType.id"}, new Object[]{eventObject.getRunObject().getId(), eventObject.getEventObjectDataType().getId()});
				if (identityObject == null) {
					saveReconciliationEventObject(eventObject);
				}
				else {
					ReconciliationEventObject<?> existingEventObject = (ReconciliationEventObject<?>) identityObject;
					status.addWarning("An adjustment of type [" + existingEventObject.getEventObjectDataType().getName() + "] for object with id [" + existingEventObject.getRunObject().getId() + "] already exists!");
				}
			}
			catch (Exception e) {
				status.addError(CoreExceptionUtils.isValidationException(e) ? ExceptionUtils.getOriginalMessage(e) : ExceptionUtils.getNormalizedMessage(e));
				LogUtils.error(ReconciliationEventObjectServiceImpl.class, ExceptionUtils.getDetailedMessage(e), e);
			}
		}
		status.setMessage(status.getErrorCount() > 0 ? "Failed to save " + status.getErrorCount() + " events!" : "SUCCESS");
		return status;
	}


	@Override
	//TODO - sendEvent driven by approvalRequired?
	public Status processReconciliationEventObjectList(ReconciliationEventObjectWrapper<?> eventObjectWrapper, String globalNote, boolean sendEvent) {
		Status status = new Status();
		for (ReconciliationEventObject<?> eventObject : CollectionUtils.getIterable(eventObjectWrapper.getEventObjectList())) {
			try {
				AssertUtils.assertNotNull(eventObject.getRunObject(), "Reconciliation Run Object is required.");
				AssertUtils.assertNotNull(eventObject.getEventObjectData().getEventDate(), "You must specify an event date!");
				//Fully hydrate the runObject from the id
				eventObject.setRunObject(getReconciliationRunService().getReconciliationRunObject(eventObject.getRunObject().getId()));
				if (eventObject.getParentRunObject() != null) {
					eventObject.setParentRunObject(getReconciliationRunService().getReconciliationRunObject(eventObject.getParentRunObject().getId()));
				}
				if (!eventObject.getStatus().isProcessed()) {
					if (StringUtils.isEmpty(eventObject.getEventObjectData().getEventNote())) {
						ValidationUtils.assertNotNull(globalNote, "Either a note must be entered for each adjustment, or a global note must be specified!");
						eventObject.getEventObjectData().setEventNote(globalNote);
					}
					if (sendEvent) {
						eventObject.setStatus(ReconciliationEventObjectStatuses.PENDING_EXECUTION);
						eventObject = saveReconciliationEventObject(eventObject);
						getReconciliationEventService().sendReconciliationEventMessage(eventObject);
						status.addMessage("Event with id [" + eventObject.getId() + "] for account [" + eventObject.getRunObject().getReconciliationInvestmentAccount().getAccountNumber() + "] has been submitted for creation.");
					}
					else {
						saveReconciliationEventObject(eventObject);
					}
				}
				else {
					status.addWarning("Event with id [" + eventObject.getId() + " for account [" + eventObject.getRunObject().getReconciliationInvestmentAccount().getAccountNumber() + "] has already been processed.");
				}
			}
			catch (Exception e) {
				status.addError(e.getMessage());
				LogUtils.error(ReconciliationEventObjectServiceImpl.class, ExceptionUtils.getDetailedMessage(e), e);
			}
		}
		status.setMessage(status.getErrorCount() > 0 ? "Failed to update " + status.getErrorCount() + " events!" : "All events successfully submitted.");
		return status;
	}


	@Override
	public ReconciliationEventObject<?> saveReconciliationEventObject(ReconciliationEventObject<?> eventObject) {
		return saveReconciliationEventObject(eventObject, true);
	}


	@Override
	public ReconciliationEventObject<?> saveReconciliationEventObject(ReconciliationEventObject<?> eventObject, boolean validate) {
		if (eventObject.isNewBean()) {
			eventObject.setStatus(ReconciliationEventObjectStatuses.OPEN);
			if (eventObject.getEventObjectDataType().isAbsolute() && eventObject.getEventObjectData() instanceof ReconciliationEventObjectJournalData) {
				ReconciliationEventObjectJournalData journalData = (ReconciliationEventObjectJournalData) eventObject.getEventObjectData();
				journalData.setAmount(MathUtils.abs(journalData.getAmount()));
			}
		}
		if (validate) {
			validateAction(eventObject);
		}
		return getReconciliationEventObjectDAO().save(eventObject);
	}


	@Override
	/*
	 * Used to execute a save without observers so events aren't actually created/sent to IMS.
	 * This is used when an object is manually matched in order to associate the events with the new merged object
	 * Previously the events were just being deleted.
	 */
	public ReconciliationEventObject<?> updateReconciliationEventObject(ReconciliationEventObject<?> eventObject) {
		return DaoUtils.executeWithAllObserversDisabled(() -> getReconciliationEventObjectDAO().save(eventObject));
	}


	@Override
	public void deleteReconciliationEventObject(int id) {
		getReconciliationEventObjectDAO().delete(id);
	}


	@Override
	public void deleteReconciliationEventObjectList(List<ReconciliationEventObject<?>> eventObjectList) {
		DaoUtils.executeWithAllObserversDisabled(() -> getReconciliationEventObjectDAO().deleteList(eventObjectList));
	}


	private void validateAction(ReconciliationEventObject<?> eventObject) {
		if (eventObject.getParentRunObject() != null && eventObject.getParentRunObject().getReconciliationRun() != null) {
			if (eventObject.getParentRunObject().getReconciliationRun().getApprovedByUser() != null) {
				throw new ValidationException("You may not create, update, or delete event objects that apply to a run or parent run that has already been approved.");
			}
		}
		if (eventObject.getRunObject() != null && eventObject.getRunObject().getReconciliationRun() != null) {
			if (eventObject.getRunObject().getReconciliationRun().getApprovedByUser() != null) {
				throw new ValidationException("You may not create, update, or delete event objects that apply to a run that has already been approved.");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////   Reconciliation Event Object Data Type Business Methods  /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationEventObjectDataType getReconciliationEventObjectDataType(short id) {
		return getReconciliationEventObjectDataTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public ReconciliationEventObjectDataType getReconciliationEventObjectDataTypeByName(String name) {
		ReconciliationEventObjectDataTypeSearchForm searchForm = new ReconciliationEventObjectDataTypeSearchForm();
		searchForm.setName(name);
		return CollectionUtils.getOnlyElement(getReconciliationEventObjectDataTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm)));
	}


	@Override
	public List<ReconciliationEventObjectDataType> getReconciliationEventObjectDataTypeList(ReconciliationEventObjectDataTypeSearchForm searchForm) {
		return getReconciliationEventObjectDataTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReconciliationEventObjectDataType saveReconciliationEventObjectDataType(ReconciliationEventObjectDataType eventObjectDataType) {
		return getReconciliationEventObjectDataTypeDAO().save(eventObjectDataType);
	}


	@Override
	public void deleteReconciliationEventObjectDataType(short id) {
		getReconciliationEventObjectDataTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public ReconciliationEventService getReconciliationEventService() {
		return this.reconciliationEventService;
	}


	public void setReconciliationEventService(ReconciliationEventService reconciliationEventService) {
		this.reconciliationEventService = reconciliationEventService;
	}


	public AdvancedUpdatableDAO<ReconciliationEventObject<?>, Criteria> getReconciliationEventObjectDAO() {
		return this.reconciliationEventObjectDAO;
	}


	public void setReconciliationEventObjectDAO(AdvancedUpdatableDAO<ReconciliationEventObject<?>, Criteria> reconciliationEventObjectDAO) {
		this.reconciliationEventObjectDAO = reconciliationEventObjectDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationEventObjectDataType, Criteria> getReconciliationEventObjectDataTypeDAO() {
		return this.reconciliationEventObjectDataTypeDAO;
	}


	public void setReconciliationEventObjectDataTypeDAO(AdvancedUpdatableDAO<ReconciliationEventObjectDataType, Criteria> reconciliationEventObjectDataTypeDAO) {
		this.reconciliationEventObjectDataTypeDAO = reconciliationEventObjectDataTypeDAO;
	}
}
