package com.clifton.reconciliation.validation;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionService;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import com.clifton.reconciliation.run.util.ReconciliationRunUtils;

import java.util.Date;
import java.util.List;


/**
 * @author stevenf
 */
public abstract class ReconciliationValidator<T extends IdentityObject> extends SelfRegisteringDaoValidator<T> {

	private ReconciliationRunService reconciliationRunService;
	private ReconciliationDefinitionService reconciliationDefinitionService;


	public void catchSingleRunPerDefinition(String message, ReconciliationDefinition definition, ReconciliationSource source, Date startDate, Date endDate) {
		ReconciliationRunUtils.catchSingleRunPerDefinition(message, definition, source, startDate, endDate, getReconciliationRunService());
	}


	public void catchMultipleRunsPerDefinition(ReconciliationDefinition definition, ReconciliationSource source, Date startDate, Date endDate) {
		catchMultipleRunsPerDefinition("You must delete any runs after the start date; or set the start date equal to the latest run!", definition, source, startDate, endDate);
	}


	public void catchMultipleRunsPerDefinition(String message, ReconciliationDefinition definition, ReconciliationSource source, Date startDate, Date endDate) {
		ReconciliationRunUtils.catchMultipleRunsPerDefinition(message, definition, source, startDate, endDate, getReconciliationRunService());
	}


	public List<ReconciliationRun> getReconciliationRunList(ReconciliationDefinition definition, ReconciliationSource source, Date startDate, Date endDate) {
		return ReconciliationRunUtils.getReconciliationRunList(definition, source, startDate, endDate, getReconciliationRunService());
	}


	public List<ReconciliationRun> getReconciliationRunListBySourceAfterDate(ReconciliationSource source, Date afterDate) {
		ReconciliationRunSearchForm searchForm = new ReconciliationRunSearchForm();
		searchForm.setPrimaryOrSecondarySourceId(source.getId());
		searchForm.addSearchRestriction("runDate", ComparisonConditions.GREATER_THAN, afterDate);
		return getReconciliationRunService().getReconciliationRunList(searchForm);
	}


	public void throwApplicableRunsValidationException(String message, List<ReconciliationRun> runList) {
		ReconciliationRunUtils.throwApplicableRunsValidationException(message, runList);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationDefinitionService getReconciliationDefinitionService() {
		return this.reconciliationDefinitionService;
	}


	public void setReconciliationDefinitionService(ReconciliationDefinitionService reconciliationDefinitionService) {
		this.reconciliationDefinitionService = reconciliationDefinitionService;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}
}
