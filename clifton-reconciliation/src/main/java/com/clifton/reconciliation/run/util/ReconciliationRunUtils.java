package com.clifton.reconciliation.run.util;

import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.json.function.evaluator.util.JsonFunctionEvaluatorUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.ReconciliationDefinitionFieldMapping;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.rule.ReconciliationRuleFunction;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author DanielH
 */
public class ReconciliationRunUtils {


	/**
	 * Normalization is the process of mapping raw row data from the primary and secondary source to the defined {@link ReconciliationDefinitionField#getField()} name.
	 * This allows data with different names to be mapped in a consistent manner for display and processing purposes.
	 * Mappings utilize conditions and functions that are used to:
	 * 1) Determine if a mapping applies to the given field based on some other row value.
	 * 2) Allows for basic manipulation of the value before normalizing (e.g. adding a dash to an account number, or using only a substring of the value, etc)
	 */
	public static Map<String, Object> normalizeRowData(ReconciliationRun run, Map<String, Object> rowData, ReconciliationSource source) {
		Map<String, Object> normalizedRowData = new HashMap<>();
		//Get the fully populated definition
		ReconciliationDefinition definition = run.getReconciliationDefinition();
		for (ReconciliationDefinitionField definitionField : CollectionUtils.getIterable(definition.getReconciliationDefinitionFieldList())) {
			ReconciliationDefinitionFieldMapping fieldMapping = definitionField.getReconciliationDefinitionFieldMapping(source.getName(), rowData);
			if (fieldMapping != null) {
				DataTypes dataType = ObjectUtils.coalesce(definitionField.getDataType(), definitionField.getField().getDefaultDataType());
				if (dataType != null) {
					ReconciliationRuleFunction functionRule = fieldMapping.getFunctionRule();
					Object fieldValue = DataTypeNameUtils.convertObjectToDataTypeName(functionRule != null
							? JsonFunctionEvaluatorUtils.evaluateFunction(functionRule.getJsonExpression(), rowData)
							: rowData.get(fieldMapping.getName()), dataType.getTypeName());
					//Convert null to 0 for numeric fields for display/comparison
					if (dataType.getTypeName().isNumeric() && fieldValue == null) {
						fieldValue = 0;
					}
					normalizedRowData.put(fieldMapping.getDefinitionField().getField().getName(), fieldValue);
				}
			}
		}
		return normalizedRowData;
	}


	/**
	 * Creates a unique identifier for the runObject based on the configured 'Natural Key' {@link ReconciliationDefinitionField}s
	 */
	public static String createRunObjectIdentifier(List<ReconciliationDefinitionField> definitionFieldList, Map<String, Object> objectData) {
		StringBuilder objectIdentifier = new StringBuilder();
		for (ReconciliationDefinitionField definitionField : CollectionUtils.getIterable(definitionFieldList)) {
			if (definitionField.isNaturalKey()) {
				if (objectIdentifier.length() > 0) {
					objectIdentifier.append("_");
				}
				Object objectValue = objectData.get(definitionField.getField().getName());
				DataTypes dataType = ObjectUtils.coalesce(definitionField.getDataType(), definitionField.getField().getDefaultDataType());
				if (dataType != null && dataType.getTypeName() == DataTypeNames.DATE) {
					objectValue = DateUtils.fromDate((Date) DataTypeNameUtils.convertObjectToDataTypeName(objectValue, DataTypeNames.DATE), DateUtils.DATE_FORMAT_ISO_SIMPLE);
				}
				objectIdentifier.append(StringUtils.trimAll(String.valueOf(objectValue)));
			}
		}
		return objectIdentifier.toString().toUpperCase();
	}


	public static void catchMultipleRunsPerDefinition(String message, ReconciliationDefinition definition, ReconciliationSource source, Date startDate, Date endDate, ReconciliationRunService runService) {
		List<ReconciliationRun> runList = getReconciliationRunList(definition, source, startDate, endDate, runService);
		if (BooleanUtils.isTrue(doIsMultipleRunsPerDefinition(runList, 1))) {
			throwApplicableRunsValidationException(message, runList);
		}
	}


	public static boolean isMultipleRunsPerDefinition(ReconciliationDefinition definition, ReconciliationSource source, Date startDate, Date endDate, ReconciliationRunService runService) {
		return doIsMultipleRunsPerDefinition(getReconciliationRunList(definition, source, startDate, endDate, runService), 1);
	}


	public static void catchSingleRunPerDefinition(String message, ReconciliationDefinition definition, ReconciliationSource source, Date startDate, Date endDate, ReconciliationRunService runService) {
		List<ReconciliationRun> runList = getReconciliationRunList(definition, source, startDate, endDate, runService);
		if (BooleanUtils.isTrue(doIsMultipleRunsPerDefinition(runList, 0))) {
			throwApplicableRunsValidationException(message, runList);
		}
	}


	private static boolean doIsMultipleRunsPerDefinition(List<ReconciliationRun> runList, int size) {
		return CollectionUtils.getSize(runList) == 0 ? Boolean.FALSE : runList.stream().collect(Collectors.collectingAndThen(
				Collectors.groupingBy(run -> run.getReconciliationDefinition().getId(), Collectors.counting()), map -> map.values().stream()
						.map(aLong -> aLong.intValue() > size).findFirst().orElse(Boolean.TRUE)));
	}


	public static void throwApplicableRunsValidationException(String message, List<ReconciliationRun> runList) {
		StringBuilder errorMessage = new StringBuilder();
		errorMessage.append(message).append("<br /><br />");
		errorMessage.append("The following runs are applicable:<br /><br />");
		int runCount = 0;
		for (ReconciliationRun run : CollectionUtils.getIterable(runList)) {
			if (runCount == 10) {
				errorMessage.append("Plus ").append(CollectionUtils.getSize(runList) - 10).append(" more...<br />");
				break;
			}
			errorMessage.append(run.getLabel()).append("<br />");
			runCount++;
		}
		throw new ValidationException(errorMessage.toString());
	}


	public static List<ReconciliationRun> getReconciliationRunList(ReconciliationDefinition definition, ReconciliationSource source, Date startDate, Date endDate, ReconciliationRunService runService) {
		ReconciliationRunSearchForm runSearchForm = new ReconciliationRunSearchForm();
		runSearchForm.setDefinitionId(definition != null ? definition.getId() : null);
		runSearchForm.setSourceName(source != null ? source.getName() : null);
		runSearchForm.addSearchRestriction("runDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate);
		runSearchForm.addSearchRestriction("runDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate);
		return runService.getReconciliationRunList(runSearchForm);
	}
}
