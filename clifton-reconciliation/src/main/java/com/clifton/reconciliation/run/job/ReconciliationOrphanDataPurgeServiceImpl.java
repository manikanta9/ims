package com.clifton.reconciliation.run.job;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.run.ReconciliationRunOrphanObject;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.search.ReconciliationRunOrphanObjectSearchForm;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * <code>ReconciliationOrphanDataPurgeServiceImpl</code> implementation for purging {@link ReconciliationRunOrphanObject} entities.
 *
 * @author TerryS
 */
@Service
public class ReconciliationOrphanDataPurgeServiceImpl implements ReconciliationOrphanDataPurgeService {

	private RunnerHandler runnerHandler;

	private ReconciliationRunService reconciliationRunService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status processReconciliationOrphanDataPurge(ReconciliationOrphanDataPurgeCommand command) {
		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : Status.ofMessage("Synchronously running for " + command);
			doOrphanPurge(command, status);
			if (command.isThrowExceptionOnError() && status.getErrorCount() > 0) {
				throw new RuntimeException(status.getMessageWithErrors());
			}
			return status;
		}
		else {
			String runId = command.getRunId();
			final Date runDate = DateUtils.addSeconds(new Date(), command.getAsynchronousRebuildDelay());
			final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + DateUtils.fromDate(runDate));
			Runner runner = new AbstractStatusAwareRunner("RECONCILIATION-ORPHAN-PURGE", runId, runDate, statusHolder) {

				@Override
				public void run() {
					try {
						ValidationUtils.assertNotNull(command.getOrphanType(), "Reconciliation orphan run object type is required.");
						ValidationUtils.assertTrue(command.getDaysToRetain() >= 0, () -> String.format("Days to retain [%s] must be greater than zero.", command.getDaysToRetain()));
						doOrphanPurge(command, statusHolder.getStatus());
					}
					catch (Exception e) {
						getStatus().setMessage("Error purging orphan objects for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
						getStatus().addError(ExceptionUtils.getDetailedMessage(e));
						LogUtils.errorOrInfo(getClass(), "Error purging orphan objects for " + runId, e);
					}
				}
			};
			getRunnerHandler().rescheduleRunner(runner);
			return Status.ofMessage("Started purging orphan object for " + command.getOrphanType() + ". Processing will be completed shortly.");
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void doOrphanPurge(ReconciliationOrphanDataPurgeCommand command, Status status) {
		ReconciliationRunOrphanObjectSearchForm searchForm = new ReconciliationRunOrphanObjectSearchForm();
		searchForm.setOrphanType(command.getOrphanType());
		searchForm.setReconciliationDateBefore(DateUtils.addDays(new Date(), -command.getDaysToRetain()));
		if (command.isReviewRequired()) {
			searchForm.setExclusionAssignmentReviewerPopulated(true);
		}
		List<ReconciliationRunOrphanObject> orphanObjectList = getReconciliationRunService().getReconciliationRunOrphanObjectList(searchForm);
		getReconciliationRunService().deleteReconciliationRunOrphanObjectList(orphanObjectList);
		status.setActionPerformed(!orphanObjectList.isEmpty());
		status.addDetail("Deletion Count", Integer.toString(orphanObjectList.size()));
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}
}
