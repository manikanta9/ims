package com.clifton.reconciliation.run;

import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectService;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationStatusService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.hibernate.Criteria;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Component;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>ReconciliationRunObserver</code> observes updates and delete actions taking on runs.
 * It is used to accurately set the matched and reconciled counts for a run that may have changed
 * based on the latest action taken (manual match, manual reconcile, etc)
 *
 * @author stevenf
 */
@Component
public class ReconciliationRunObserver extends SelfRegisteringDaoObserver<ReconciliationRun> {

	private static final String RECONCILED_COUNT_SQL = "SELECT rrs.StatusName, COUNT(*) StatusCount \n" +
			"FROM ReconciliationRunObject rro\n" +
			"INNER JOIN ReconciliationReconcileStatus rrs ON rrs.ReconciliationReconcileStatusID = rro.ReconciliationReconcileStatusID\n" +
			"WHERE ReconciliationRunID = ?\n" +
			"GROUP BY rrs.StatusName;";

	private static final String MATCHED_COUNT_SQL = "SELECT rrs.StatusName, COUNT(*) StatusCount \n" +
			"FROM ReconciliationRunObject rro\n" +
			"INNER JOIN ReconciliationMatchStatus rrs ON rrs.ReconciliationMatchStatusID = rro.ReconciliationMatchStatusID\n" +
			"WHERE ReconciliationRunID = ?\n" +
			"GROUP BY rrs.StatusName;";

	private ReconciliationEventObjectService reconciliationEventObjectService;
	private ReconciliationRunService reconciliationRunService;
	private ReconciliationStatusService reconciliationStatusService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	private AdvancedUpdatableDAO<ReconciliationRunObject, Criteria> reconciliationRunObjectDAO;
	private AdvancedUpdatableDAO<ReconciliationRunOrphanObject, Criteria> reconciliationRunOrphanObjectDAO;

	private SqlHandler sqlHandler;

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<ReconciliationRun> dao, DaoEventTypes event, ReconciliationRun run) {
		isModifyActionAllowed(run);
		if (event.isDelete()) {
			deleteReconciliationRunDependentObjects(run.getId());
		}
		if (event.isUpdate()) {
			Map<String, Integer> countMap = getCountMap(run.getId());
			int manuallyMatchedCount = countMap.get(ReconciliationMatchStatusTypes.MANUALLY_MATCHED.name());
			int matchedCount = countMap.get(ReconciliationMatchStatusTypes.MATCHED.name()) + manuallyMatchedCount;
			int notMatchedCount = countMap.get(ReconciliationMatchStatusTypes.NOT_MATCHED.name());
			int reconciledCount = countMap.get(ReconciliationReconcileStatusTypes.RECONCILED.name());
			int manuallyReconciledCount = countMap.get(ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED.name());
			int totalReconciledCount = reconciledCount + manuallyReconciledCount;
			int notReconciledCount = countMap.get(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name());
			int totalCount = totalReconciledCount + notReconciledCount;

			Status runDetails = run.getRunDetails();
			runDetails.setStatusCount(ReconciliationRun.RECONCILED_COUNT, reconciledCount);
			runDetails.setStatusCount(ReconciliationRun.MANUALLY_RECONCILED_COUNT, manuallyReconciledCount);
			runDetails.setStatusCount(ReconciliationRun.TOTAL_RECONCILED_COUNT, totalReconciledCount);
			runDetails.setStatusCount(ReconciliationRun.NOT_RECONCILED_COUNT, notReconciledCount);
			runDetails.setStatusCount(ReconciliationRun.MATCHED_COUNT, matchedCount);
			runDetails.setStatusCount(ReconciliationRun.NOT_MATCHED_COUNT, notMatchedCount);
			runDetails.setStatusCount(ReconciliationRun.TOTAL_COUNT, totalCount);
			runDetails.setMessage("Total: %d Run Object(s); Matched: %d Run Object(s); Reconciled: %d Run Object(s); Filtered: %d row(s); Excluded: %d row(s); Errors: %d");
			runDetails.setMessageArgs(
					runDetails.getStatusCount(ReconciliationRun.TOTAL_COUNT),
					runDetails.getStatusCount(ReconciliationRun.MATCHED_COUNT),
					runDetails.getStatusCount(ReconciliationRun.TOTAL_RECONCILED_COUNT),
					runDetails.getStatusCount(ReconciliationRun.FILTERED_COUNT),
					runDetails.getStatusCount(ReconciliationRun.EXCLUDED_COUNT),
					runDetails.getStatusCount(ReconciliationRun.ERROR_COUNT));
			String category = runDetails.getStatusMap().get(ReconciliationRun.CATEGORY) != null ? String.valueOf(runDetails.getStatusMap().get(ReconciliationRun.CATEGORY)) : "Run Saved";
			runDetails.addDetail(category, runDetails.getMessage());
			if (notReconciledCount == 0) {
				if (manuallyReconciledCount > 0 || manuallyMatchedCount > 0) {
					run.setReconciliationReconcileStatus(getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED));
				}
				else {
					run.setReconciliationReconcileStatus(getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.RECONCILED));
				}
			}
			else {
				run.setReconciliationReconcileStatus(getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.NOT_RECONCILED));
			}
		}
	}


	private Map<String, Integer> getCountMap(int runId) {
		//Create and initialize the map with zeroes for all statuses - this prevents null pointer references later.
		Map<String, Integer> countMap = new HashMap<>();
		countMap.put(ReconciliationMatchStatusTypes.MATCHED.name(), 0);
		countMap.put(ReconciliationMatchStatusTypes.NOT_MATCHED.name(), 0);
		countMap.put(ReconciliationMatchStatusTypes.MANUALLY_MATCHED.name(), 0);
		countMap.put(ReconciliationReconcileStatusTypes.RECONCILED.name(), 0);
		countMap.put(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name(), 0);
		countMap.put(ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED.name(), 0);
		//Now retrieve real counts
		countMap.putAll(getCountMap(runId, RECONCILED_COUNT_SQL));
		countMap.putAll(getCountMap(runId, MATCHED_COUNT_SQL));
		return countMap;
	}


	private Map<String, Integer> getCountMap(int runId, String sql) {
		Map<String, Integer> countMap = new HashMap<>();
		PreparedStatementCreatorFactory statementFactory = new PreparedStatementCreatorFactory(sql);
		statementFactory.addParameter(new SqlParameter(Types.INTEGER));
		PreparedStatementCreator statementCreator = statementFactory.newPreparedStatementCreator(new Object[]{runId});
		return getSqlHandler().executeSelect(statementCreator, resultSet -> {
			while (resultSet.next()) {
				countMap.put(resultSet.getString("StatusName"), resultSet.getInt("StatusCount"));
			}
			return countMap;
		});
	}


	private boolean isModifyActionAllowed(ReconciliationRun run) {
		if (run != null && run.getReconciliationDefinition() != null) {
			ReconciliationDefinition definition = run.getReconciliationDefinition();
			SystemCondition runModifySystemCondition = ObjectUtils.coalesce(definition.getRunModifySystemCondition(), definition.getDefinitionGroup() != null ? definition.getDefinitionGroup().getRunModifySystemCondition() : null, definition.getType().getRunModifySystemCondition());
			if (runModifySystemCondition != null) {
				EvaluationResult result = getSystemConditionEvaluationHandler().evaluateCondition(runModifySystemCondition, run);
				if (!result.isResult()) {
					throw new ValidationException(result.getMessage());
				}
			}
		}
		return run != null;
	}


	private void deleteReconciliationRunDependentObjects(int runId) {
		//Delete Orphaned Run Objects
		DaoUtils.executeWithAllObserversDisabled(() -> getReconciliationRunOrphanObjectDAO().deleteList(getReconciliationRunService().getReconciliationRunOrphanObjectListByRunId(runId)));
		//Delete Events
		getReconciliationEventObjectService().deleteReconciliationEventObjectList(getReconciliationEventObjectService().getReconciliationEventObjectListByRunId(runId));
		//Delete Run Objects
		DaoUtils.executeWithAllObserversDisabled(() -> getReconciliationRunObjectDAO().deleteList(getReconciliationRunService().getReconciliationRunObjectListByRunId(runId)));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationEventObjectService getReconciliationEventObjectService() {
		return this.reconciliationEventObjectService;
	}


	public void setReconciliationEventObjectService(ReconciliationEventObjectService reconciliationEventObjectService) {
		this.reconciliationEventObjectService = reconciliationEventObjectService;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public ReconciliationStatusService getReconciliationStatusService() {
		return this.reconciliationStatusService;
	}


	public void setReconciliationStatusService(ReconciliationStatusService reconciliationStatusService) {
		this.reconciliationStatusService = reconciliationStatusService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public AdvancedUpdatableDAO<ReconciliationRunObject, Criteria> getReconciliationRunObjectDAO() {
		return this.reconciliationRunObjectDAO;
	}


	public void setReconciliationRunObjectDAO(AdvancedUpdatableDAO<ReconciliationRunObject, Criteria> reconciliationRunObjectDAO) {
		this.reconciliationRunObjectDAO = reconciliationRunObjectDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationRunOrphanObject, Criteria> getReconciliationRunOrphanObjectDAO() {
		return this.reconciliationRunOrphanObjectDAO;
	}


	public void setReconciliationRunOrphanObjectDAO(AdvancedUpdatableDAO<ReconciliationRunOrphanObject, Criteria> reconciliationRunOrphanObjectDAO) {
		this.reconciliationRunOrphanObjectDAO = reconciliationRunOrphanObjectDAO;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}
}
