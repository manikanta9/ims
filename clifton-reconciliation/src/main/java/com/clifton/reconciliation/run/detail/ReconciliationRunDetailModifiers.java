package com.clifton.reconciliation.run.detail;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluator;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccountAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityAlias;

import java.math.BigDecimal;


/**
 * A placeholder for detail modifiers - used for easier serialization
 *
 * @author stevenf
 */
@NonPersistentObject
public class ReconciliationRunDetailModifiers {

	private BigDecimal adjustmentAmount;

	private ReconciliationInvestmentAccountAlias accountAlias;
	private ReconciliationEvaluator evaluator;
	private ReconciliationInvestmentSecurityAlias securityAlias;

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getAdjustmentAmount() {
		return this.adjustmentAmount;
	}


	public void setAdjustmentAmount(BigDecimal adjustmentAmount) {
		this.adjustmentAmount = adjustmentAmount;
	}


	public ReconciliationInvestmentAccountAlias getAccountAlias() {
		return this.accountAlias;
	}


	public void setAccountAlias(ReconciliationInvestmentAccountAlias accountAlias) {
		this.accountAlias = accountAlias;
	}


	public ReconciliationEvaluator getEvaluator() {
		return this.evaluator;
	}


	public void setEvaluator(ReconciliationEvaluator evaluator) {
		this.evaluator = evaluator;
	}


	public ReconciliationInvestmentSecurityAlias getSecurityAlias() {
		return this.securityAlias;
	}


	public void setSecurityAlias(ReconciliationInvestmentSecurityAlias securityAlias) {
		this.securityAlias = securityAlias;
	}
}
