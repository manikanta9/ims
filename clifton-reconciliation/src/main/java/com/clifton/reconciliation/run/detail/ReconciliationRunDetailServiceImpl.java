package com.clifton.reconciliation.run.detail;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.collections.FlatMap;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.compression.GzipCompressionUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.ReconciliationField;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluator;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluatorService;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccountAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.ReconciliationRunObjectDetail;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.data.DataHolder;
import com.clifton.reconciliation.run.data.DataObject;
import com.clifton.reconciliation.run.detail.cache.ReconciliationRunDetailAdjustmentAmountMapCache;
import com.clifton.reconciliation.run.search.ReconciliationRunObjectSearchForm;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatus;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;
import com.clifton.reconciliation.run.status.ReconciliationStatusService;
import com.clifton.reconciliation.run.status.search.ReconciliationMatchStatusSearchForm;
import com.clifton.reconciliation.run.status.search.ReconciliationReconcileStatusSearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * The <code>ReconciliationRunDetailServiceImpl</code> is a custom service used to properly format {@link ReconciliationRunObject}s for the UI.
 * It uses {@ReconciliationRunDetailDataRetriever} to directly query the database to sort and filter while retrieving minimal data.  It then processes those
 * objects into a list using a parallel stream that decompresses the data server-side and utilizes caches to populate the minimal amount of data necessary.
 * <p>
 * It supports both the Top/Bottom and Side/Side view on the reconciliation run detail windows.
 *
 * @author stevenf
 */
@Service
public class ReconciliationRunDetailServiceImpl implements ReconciliationRunDetailService {

	private static final String SIDE_BY_SIDE_VIEW = "Side/Side";
	private static final String DIFFERENCE_COLUMN_SUFFIX = " Diff";

	/**
	 * This is a problem - We need to to return the fields as Primary and Secondary and use these names as the dataIndex on the UI because the
	 * user entered names can contain characters that are not compatible (i.e. the 'shortName' for JP Morgan is 'J.P. Morgan Custody')
	 * This is problematic because the '.' is a special character in requested properties - inferred as a beanPath var, incorrectly in this case.
	 */
	private static final String PRIMARY_SOURCE_PREFIX = "Primary ";
	private static final String SECONDARY_SOURCE_PREFIX = "Secondary ";

	private ReconciliationEvaluatorService reconciliationEvaluatorService;
	private ReconciliationInvestmentService reconciliationInvestmentService;
	private ReconciliationRunService reconciliationRunService;
	private ReconciliationStatusService reconciliationStatusService;

	private ReconciliationRunDetailAdjustmentAmountMapCache reconciliationRunDetailAdjustmentAmountMapCache;

	private SqlHandler sqlHandler;
	private JsonHandler<?> jsonHandler;


	@Override
	@Transactional(readOnly = true)
	public List<ReconciliationRunObjectDetail> getReconciliationRunObjectDetailExplodedList(ReconciliationRunObjectSearchForm searchForm) {
		return getReconciliationRunObjectDetailList(searchForm, true);
	}


	@Override
	@Transactional(readOnly = true)
	public List<ReconciliationRunObjectDetail> getReconciliationRunObjectDetailList(ReconciliationRunObjectSearchForm searchForm) {
		return getReconciliationRunObjectDetailList(searchForm, false);
	}


	private List<ReconciliationRunObjectDetail> getReconciliationRunObjectDetailList(ReconciliationRunObjectSearchForm searchForm, boolean explode) {
		//Used to allow the initial load for transaction runs to not return any data - only loads once a parent object is selected from cash run.
		if (searchForm.getParentRunObjectId() != null && searchForm.getParentRunObjectId() == -1) {
			return new PagingArrayList<>();
		}
		ReconciliationRunDetailCommand command = initializeCommand(searchForm, explode);
		if (BooleanUtils.isTrue(searchForm.getShowHistory()) && searchForm.getId() != null) {
			command.setShowHistory(true);
		}
		command.setRunObjectDetailList(ReconciliationRunDetailDataRetriever.getReconciliationRunObjectDetailList(command, getSqlHandler()));
		if (!CollectionUtils.isEmpty(command.getRunObjectDetailList())) {
			//Decompresses and hydrates all objects.
			processView(command, explode);
			//Now applies server side filtering against calculated columns
			applyServerSideFiltering(command);
			applyServerSideSorting(command);
			List<ReconciliationRunObjectDetail> runObjectDetailList = explode ? new ArrayList<>() : command.getRunObjectDetailList();
			if (explode) {
				for (ReconciliationRunObjectDetail runObjectDetail : CollectionUtils.getIterable(command.getRunObjectDetailList())) {
					runObjectDetailList.addAll(runObjectDetail.explode());
				}
			}
			return new PagingArrayList<>(getSubList(runObjectDetailList, searchForm), searchForm.getStart(), CollectionUtils.getSize(runObjectDetailList));
		}
		return new PagingArrayList<>();
	}


	private ReconciliationRunDetailCommand initializeCommand(ReconciliationRunObjectSearchForm searchForm, boolean explode) {
		ReconciliationRunDetailCommand command = new ReconciliationRunDetailCommand();
		ReconciliationRun run = initializeRunAndSearchForm(searchForm);
		command.setReconciliationRunService(getReconciliationRunService());
		command.setRun(run);
		command.setSearchForm(searchForm);
		command.setFieldDataTypeMap(getFieldDataTypeMap(run));
		command.setSideBySideView(SIDE_BY_SIDE_VIEW.equals(searchForm.getDataView()));
		command.setPrimarySource(run.getReconciliationDefinition().getPrimarySource());
		command.setSecondarySource(run.getReconciliationDefinition().getSecondarySource());
		command.setMatchStatusMap(getReconciliationStatusService().getReconciliationMatchStatusList(new ReconciliationMatchStatusSearchForm()).stream().collect(Collectors.toMap(ReconciliationMatchStatus::getId, Function.identity())));
		command.setReconcileStatusMap(getReconciliationStatusService().getReconciliationReconcileStatusList(new ReconciliationReconcileStatusSearchForm()).stream().collect(Collectors.toMap(ReconciliationReconcileStatus::getId, Function.identity())));
		command.setAdjustmentAmountMap(getReconciliationRunDetailAdjustmentAmountMapCache().getReconciliationRunDetailAdjustmentAmountMap(run.getId(), getRunIds(run, explode)));
		command.setSecurityAliasMap(ReconciliationRunDetailDataRetriever.getSecurityAliasMap(run, getReconciliationInvestmentService()));
		command.setAccountAliasMap(ReconciliationRunDetailDataRetriever.getAccountAliasMap(run, getReconciliationInvestmentService()));
		command.setEvaluatorMap(ReconciliationRunDetailDataRetriever.getEvaluatorMap(run, getReconciliationEvaluatorService()));
		return command;
	}


	private Integer[] getRunIds(ReconciliationRun run, boolean explode) {
		if (explode) {
			ReconciliationRunSearchForm searchForm = new ReconciliationRunSearchForm();
			searchForm.setRunDateBeforeOrEquals(run.getRunDate());
			searchForm.setRunDateAfterOrEquals(DateUtils.addDays(run.getRunDate(), -7));
			searchForm.setDefinitionId(run.getReconciliationDefinition().getId());
			return CollectionUtils.toArray(getReconciliationRunService().getReconciliationRunList(searchForm).stream().map(ReconciliationRun::getId).collect(Collectors.toList()), Integer.class);
		}
		else {
			return CollectionUtils.toArray(Collections.singletonList(run.getId()), Integer.class);
		}
	}


	private ReconciliationRun initializeRunAndSearchForm(ReconciliationRunObjectSearchForm searchForm) {
		ReconciliationRun run;
		if (searchForm.getParentRunObjectId() == null) {
			ValidationUtils.assertNotNull(searchForm.getRunId(), "Run ID is required!");
			run = getReconciliationRunService().getReconciliationRun(searchForm.getRunId());
			ValidationUtils.assertNotNull(run, "Unable to locate run with id [" + searchForm.getRunId() + "]");
		}
		else {
			ReconciliationRunObject runObject = getReconciliationRunService().getReconciliationRunObject(searchForm.getParentRunObjectId());
			ValidationUtils.assertNotNull(runObject, "Unable to locate runObject with id [" + searchForm.getParentRunObjectId() + "]");
			//Fully hydrate the child run by retrieving fresh
			ReconciliationRun parentRun = getReconciliationRunService().getReconciliationRun(runObject.getReconciliationRun().getId());
			ValidationUtils.assertNotNull(parentRun, "Unable to locate parent run with id [" + runObject.getReconciliationRun().getId() + "]");
			//Fully hydrate the child as well...
			run = getReconciliationRunService().getReconciliationRun(getReconciliationRunByParent(parentRun).getId());
			ValidationUtils.assertNotNull(run, "Unable to locate child run with id [" + searchForm.getRunId() + "] from parent run with id [" + parentRun.getId() + "]");
			//We want to retrieve all objects with the same account from the childRun
			//TODO - deal with invalid account?  Realistically should be aliased, but aliases failed today because of the event null pointer...
			if (runObject.getReconciliationInvestmentAccount() != null) {
				searchForm.setInvestmentAccountId(runObject.getReconciliationInvestmentAccount().getId());
			}
			searchForm.setRunId(run.getId());
			searchForm.setParentRunObjectId(null);
		}
		return run;
	}


	/**
	 * We make this concurrent because we will actually update it during parallel processing so that when we generate
	 * computed fields in sideBySide view (e.g. source specific numeric fields) we can add them with their dataType so
	 * they are available when later generating the '<fieldName> Diff' computed fields.
	 */
	private Map<String, DataTypes> getFieldDataTypeMap(ReconciliationRun run) {
		Map<String, DataTypes> fieldDataTypeMap = new ConcurrentHashMap<>();
		for (ReconciliationDefinitionField definitionField : run.getReconciliationDefinition().getReconciliationDefinitionFieldList()) {
			fieldDataTypeMap.put(definitionField.getField().getName(), ObjectUtils.coalesce(definitionField.getDataType(), definitionField.getField().getDefaultDataType()));
		}
		return fieldDataTypeMap;
	}


	private ReconciliationRun getReconciliationRunByParent(ReconciliationRun parentRun) {
		ReconciliationRunSearchForm searchForm = new ReconciliationRunSearchForm();
		//Setting cumulative ensures only one transaction run - if expanded beyond the cash/trans parent/child relationship where children are not cumulative...then this will fail miserably...I should fix this now after typing that...but...I'm busy.
		searchForm.setCumulativeReconciliation(true);
		searchForm.setParentDefinitionId(parentRun.getReconciliationDefinition().getId());
		return CollectionUtils.getFirstElementStrict(getReconciliationRunService().getReconciliationRunList(searchForm));
	}


	private void processView(ReconciliationRunDetailCommand command, boolean explode) {
		JsonHandler<?> commandJsonHandler = command.getJsonHandler();
		ReconciliationSource primarySource = command.getPrimarySource();
		ReconciliationSource secondarySource = command.getSecondarySource();
		Map<String, DataTypes> fieldDataTypeMap = command.getFieldDataTypeMap();
		for (ReconciliationRunObjectDetail runObjectDetail : CollectionUtils.getIterable(command.getRunObjectDetailList())) {
			try {
				List<String> computedColumnList = new ArrayList<>();
				List<ReconciliationSource> sourceList = new ArrayList<>();
				FlatMap<String, List<Object>> flatMap = new FlatMap<>();
				DataHolder dataHolder = (DataHolder) commandJsonHandler.fromJson(GzipCompressionUtils.decompress(runObjectDetail.getDataHolderBytes()), DataHolder.class);
				List<DataObject> dataObjectList = getDataObjectList(dataHolder, primarySource, secondarySource);
				for (DataObject dataObject : CollectionUtils.getIterable(dataObjectList)) {
					//We iterate the expected fieldNames because the data within the dataObject does not maintain null values
					//and we need to ensure we are adding a value for each fieldName into the list so the UI displays properly.
					//We also track any new fields created in the computedColumn map and add after iterating - used later in applyDifferenceData method.
					for (Map.Entry<String, DataTypes> fieldDataTypeEntry : CollectionUtils.getIterable(fieldDataTypeMap.entrySet())) {
						String fieldName = fieldDataTypeEntry.getKey();
						String displayName = getDisplayName(command, dataObject, fieldDataTypeEntry, computedColumnList);
						List<Object> values = flatMap.getOrDefault(displayName, new ArrayList<>());
						Object value = DataTypeNameUtils.convertObjectToDataTypeName(dataObject.getData().get(fieldName), fieldDataTypeEntry.getValue().getTypeName());
						if (fieldDataTypeEntry.getValue().getTypeName().isNumeric()) {
							values.add(value == null ? BigDecimal.ZERO : value);
						}
						else {
							values.add(value == null ? "" : value);
						}
						flatMap.put(displayName, values);
					}
					sourceList.add(CompareUtils.isEqual(dataObject.getSourceId(), primarySource.getId()) ? primarySource : secondarySource);
				}
				runObjectDetail.setObjectData(flatMap);
				if (explode) {
					runObjectDetail.setDataObjectList(dataObjectList);
				}
				runObjectDetail.setSource(CollectionUtils.isEmpty(sourceList) ? null : sourceList);
				finalizeRunObjectDetail(command, runObjectDetail, dataHolder, computedColumnList);
			}
			catch (Exception e) {
				LogUtils.error(getClass(), "Failed to retrieve runDetails for object [" + runObjectDetail.getId() + "]", e);
			}
		}
	}


	/**
	 * All server side filtered fields (calculated) are numeric (BigDecimal) except notes
	 */
	private int applyServerSideFiltering(ReconciliationRunDetailCommand command) {
		AtomicInteger filteredCount = new AtomicInteger();
		List<SearchRestriction> serverSideRestrictionList = new ArrayList<>();
		for (SearchRestriction searchRestriction : CollectionUtils.getIterable(command.getSearchForm().getRestrictionList())) {
			if (isServerSideProcessedField(searchRestriction.getField())) {
				serverSideRestrictionList.add(searchRestriction);
			}
		}
		command.setRunObjectDetailList(BeanUtils.filter(command.getRunObjectDetailList(), runObjectDetail -> {
			boolean returnVal = true;
			for (SearchRestriction serverSideRestriction : CollectionUtils.getIterable(serverSideRestrictionList)) {
				if ("notes".equals(serverSideRestriction.getField())) {
					String value = ReconciliationRunDetailUtils.getLatestManualNote(runObjectDetail.getNotes());
					String comparisonValue = (String) serverSideRestriction.getValue();
					if (value != null && comparisonValue != null) {
						switch (serverSideRestriction.getComparison()) {
							case EQUALS:
								returnVal = StringUtils.isEqual(value, comparisonValue);
								break;
							case NOT_EQUALS:
								returnVal = !StringUtils.isEqual(value, comparisonValue);
								break;
							case LIKE:
								returnVal = StringUtils.contains(value, comparisonValue);
								break;
						}
					}
					else {
						returnVal = false;
					}
				}
				else {
					BigDecimal value = null;
					List<Object> valueList = runObjectDetail.getObjectData().get(serverSideRestriction.getField());
					if (!CollectionUtils.isEmpty(valueList)) {
						value = DataTypeNameUtils.convertObjectToDataTypeName(CollectionUtils.getFirstElement(valueList), DataTypeNames.DECIMAL, BigDecimal.class);
					}
					if (value != null) {
						BigDecimal comparisonValue = DataTypeNameUtils.convertObjectToDataTypeName(serverSideRestriction.getValue(), DataTypeNames.DECIMAL, BigDecimal.class);
						switch (serverSideRestriction.getComparison()) {
							case EQUALS:
								returnVal = MathUtils.isEqual(value, comparisonValue);
								break;
							case NOT_EQUALS:
								returnVal = MathUtils.isNotEqual(value, comparisonValue);
								break;
							case GREATER_THAN:
								returnVal = MathUtils.isGreaterThan(value, comparisonValue);
								break;
							case GREATER_THAN_OR_EQUALS:
								returnVal = MathUtils.isGreaterThanOrEqual(value, comparisonValue);
								break;
							case LESS_THAN:
								returnVal = MathUtils.isLessThan(value, comparisonValue);
								break;
							case LESS_THAN_OR_EQUALS:
								returnVal = MathUtils.isLessThanOrEqual(value, comparisonValue);
								break;
						}
					}
				}
			}
			if (!returnVal) {
				filteredCount.getAndIncrement();
			}
			return returnVal;
		}));
		return filteredCount.get();
	}


	/**
	 * All server side sorted fields (calculated) are numeric (BigDecimal) except for notes
	 */
	private void applyServerSideSorting(ReconciliationRunDetailCommand command) {
		if (command.getSearchForm().getOrderBy() != null) {
			String fieldName = command.getSearchForm().getOrderBy().split(":")[0];
			String direction = command.getSearchForm().getOrderBy().split(":")[1];
			if (isServerSideProcessedField(fieldName)) {
				boolean notes = "notes".equals(fieldName);
				DataTypeNames dataTypeName = notes ? DataTypeNames.STRING : DataTypeNames.DECIMAL;
				BeanUtils.sortWithFunction(command.getRunObjectDetailList(), runObjectDetail -> {
					if (notes) {
						String note = ReconciliationRunDetailUtils.getLatestManualNote(runObjectDetail.getNotes());
						return note == null ? "" : note;
					}
					else {
						Object object = null;
						List<Object> objectList = runObjectDetail.getObjectData().get(fieldName);
						if (!CollectionUtils.isEmpty(objectList)) {
							object = DataTypeNameUtils.convertObjectToDataTypeName(CollectionUtils.getFirstElement(objectList), dataTypeName);
						}
						return object == null ? BigDecimal.ZERO : object;
					}
				}, "ASC".equals(direction));
			}
		}
	}


	/**
	 * Computed columns, created in sideBySide view, must be sorted server side.
	 * They follow a naming convention of either being prefixed with the source shorts name
	 * or suffixed with ' Diff'
	 */
	private boolean isServerSideProcessedField(String fieldName) {
		if (ReconciliationField.ADJUSTMENT_AMOUNT.equals(fieldName) ||
				fieldName.startsWith(PRIMARY_SOURCE_PREFIX) ||
				fieldName.startsWith(SECONDARY_SOURCE_PREFIX) ||
				fieldName.endsWith(DIFFERENCE_COLUMN_SUFFIX) ||
				"notes".equals(fieldName)) {
			return true;
		}
		return false;
	}


	private List<DataObject> getDataObjectList(DataHolder dataHolder, ReconciliationSource primarySource, ReconciliationSource secondarySource) {
		List<DataObject> dataObjectList = new ArrayList<>();
		//Retrieve the primary source objects, then the secondary source objects and add to a list - this ensure on the UI primary objects will be shown first
		if (!CollectionUtils.isEmpty(dataHolder.getDataObjectListBySource(primarySource))) {
			dataObjectList.addAll(dataHolder.getDataObjectListBySource(primarySource));
		}
		if (!CollectionUtils.isEmpty(dataHolder.getDataObjectListBySource(secondarySource))) {
			dataObjectList.addAll(dataHolder.getDataObjectListBySource(secondarySource));
		}
		return dataObjectList;
	}


	private String getDisplayName(ReconciliationRunDetailCommand command, DataObject dataObject, Map.Entry<String, DataTypes> fieldDataTypeEntry, List<String> computedColumnList) {
		String displayName = fieldDataTypeEntry.getKey();
		boolean numeric = fieldDataTypeEntry.getValue().getTypeName().isNumeric();
		boolean primarySourceDataObject = CompareUtils.isEqual(dataObject.getSourceId(), command.getPrimarySource().getId());
		if (command.isSideBySideView() && numeric) {
			displayName = (primarySourceDataObject ? PRIMARY_SOURCE_PREFIX : SECONDARY_SOURCE_PREFIX) + displayName;
			computedColumnList.add(displayName);
		}
		return displayName;
	}


	private void finalizeRunObjectDetail(ReconciliationRunDetailCommand command, ReconciliationRunObjectDetail runObjectDetail, DataHolder dataHolder, List<String> computedColumnList) {
		if (command.isSideBySideView()) {
			Map<Long, BigDecimal> adjustmentAmountMap = command.getAdjustmentAmountMap();
			BigDecimal adjustmentAmount = adjustmentAmountMap.get(runObjectDetail.getId());
			runObjectDetail.getObjectData().put(ReconciliationField.ADJUSTMENT_AMOUNT, Collections.singletonList(adjustmentAmount != null ? adjustmentAmount : BigDecimal.ZERO));
			addDifferenceData(runObjectDetail, computedColumnList);
		}
		Map<Short, ReconciliationMatchStatus> matchStatusMap = command.getMatchStatusMap();
		Map<Short, ReconciliationReconcileStatus> reconcileStatusMap = command.getReconcileStatusMap();

		runObjectDetail.setNotes(dataHolder.getNotes());
		runObjectDetail.setErrors(dataHolder.getErrors());
		runObjectDetail.setModifierText(getModifierText(command, runObjectDetail));
		runObjectDetail.setMatchStatus(matchStatusMap.get(runObjectDetail.getMatchStatusId()));
		runObjectDetail.setReconcileStatus(reconcileStatusMap.get(runObjectDetail.getReconcileStatusId()));
		if (!command.isShowHistory()) {
			runObjectDetail.setRunDate(command.getRun().getRunDate());
		}
		//Null values in database for reconciledOnSourceId will return as 0
		runObjectDetail.setReconciledOnPrimarySource(runObjectDetail.getReconciledOnSourceId() != 0 ? CompareUtils.isEqual(command.getPrimarySource().getId(), runObjectDetail.getReconciledOnSourceId()) : null);
	}


	/**
	 * In many cases the modifier text will be the same across large swathes of the data returned.
	 * This will retrieve that already 'stringified' field rather than having to convert the JSON again.
	 * Items that have adjustments will almost always be unique, however.  Mapping appears to reduce processing
	 * time by nearly half in some larger instances - ideally transaction data is maintained, and large numbers
	 * of unreconciled objects should not be returned - this is a short-term improvement since Delta code
	 * approaches this a completely different way.
	 */
	private String getModifierText(ReconciliationRunDetailCommand command, ReconciliationRunObjectDetail runObjectDetail) {
		ReconciliationRunDetailModifiers modifiers = new ReconciliationRunDetailModifiers();
		ReconciliationInvestmentAccountAlias accountAlias = command.getAccountAliasMap().get(command.getRun().getReconciliationDefinition().getSecondarySource().getId() + "_" + runObjectDetail.getReconciliationInvestmentAccountId());
		ReconciliationInvestmentSecurityAlias securityAlias = command.getSecurityAliasMap().get(command.getRun().getReconciliationDefinition().getSecondarySource().getId() + "_" + runObjectDetail.getReconciliationInvestmentSecurityId());
		ReconciliationEvaluator evaluator = getEvaluator(command, runObjectDetail);
		BigDecimal adjustmentAmount = command.getAdjustmentAmountMap().get(runObjectDetail.getId());

		String key = createKey(accountAlias, securityAlias, evaluator, adjustmentAmount);
		String modifierText = command.getModifierTextMap().get(key);
		if (modifierText == null) {
			modifiers.setAccountAlias(accountAlias);
			modifiers.setSecurityAlias(securityAlias);
			modifiers.setEvaluator(evaluator);
			modifiers.setAdjustmentAmount(adjustmentAmount);
			modifierText = getJsonHandler().toJson(modifiers);
			command.getModifierTextMap().put(key, modifierText);
		}
		return modifierText;
	}


	private String createKey(ReconciliationInvestmentAccountAlias accountAlias, ReconciliationInvestmentSecurityAlias securityAlias, ReconciliationEvaluator evaluator, BigDecimal adjustmentAmount) {
		StringBuilder key = new StringBuilder();
		key.append(accountAlias != null ? accountAlias.getId() : "0").append("_");
		key.append(securityAlias != null ? securityAlias.getId() : "0").append("_");
		key.append(evaluator != null ? evaluator.getId() : "0").append("_");
		key.append(adjustmentAmount != null ? adjustmentAmount.toString() : "0");
		return key.toString();
	}


	/**
	 * Since evaluators have optional fields we have a few combination of keys to check the map for
	 */
	private ReconciliationEvaluator getEvaluator(ReconciliationRunDetailCommand command, ReconciliationRunObjectDetail runObjectDetail) {
		ReconciliationEvaluator evaluator = command.getEvaluatorMap().get(command.getRun().getReconciliationDefinition().getId() + "_" + runObjectDetail.getReconciliationInvestmentAccountId() + "_" + runObjectDetail.getReconciliationInvestmentSecurityId());
		if (evaluator == null) {
			evaluator = command.getEvaluatorMap().get(command.getRun().getReconciliationDefinition().getId() + "_" + runObjectDetail.getReconciliationInvestmentAccountId() + "_null");
		}
		if (evaluator == null) {
			evaluator = command.getEvaluatorMap().get(command.getRun().getReconciliationDefinition().getId() + "_null_" + runObjectDetail.getReconciliationInvestmentSecurityId());
		}
		if (evaluator == null) {
			evaluator = command.getEvaluatorMap().get(command.getRun().getReconciliationDefinition().getId() + "_null_null");
		}
		return evaluator;
	}


	/**
	 * In sideBySide view we dynamically create primary and secondary fields for any numeric fields.
	 * This results in fields such as 'Primary Cash Balance' and 'Secondary Cash Balance' - when these exist
	 * we also need to create a 'Cash Balance Diff' field
	 * <p>
	 * Additionally, when 'Cash Balance' exists, and 'Adjustment Amount Diff' field must be computed
	 * using the 'Cash Balance Diff' and 'Adjustment Amount' fields.
	 */
	private void addDifferenceData(ReconciliationRunObjectDetail runObjectDetail, List<String> computedColumnList) {
		for (String fieldName : CollectionUtils.getIterable(computedColumnList)) {
			String prefixToReplace = fieldName.startsWith(PRIMARY_SOURCE_PREFIX) ? PRIMARY_SOURCE_PREFIX : SECONDARY_SOURCE_PREFIX;
			fieldName = fieldName.replace(prefixToReplace, "");
			runObjectDetail.getObjectData().put(fieldName + " Diff", Collections.singletonList(computeDifference(runObjectDetail, PRIMARY_SOURCE_PREFIX + fieldName, SECONDARY_SOURCE_PREFIX + fieldName, false)));
			//Now create the adjustment amount diff field.
			if (ReconciliationField.CASH_BALANCE.equals(fieldName)) {
				List<Object> cashBalanceDiffList = runObjectDetail.getObjectData().get(fieldName + " Diff");
				List<Object> adjustmentAmountList = runObjectDetail.getObjectData().get(ReconciliationField.ADJUSTMENT_AMOUNT);
				//Adjustments are inverted for display by 'isNegativeAdjustment' flag on the adjustment type if applicable, but should always be added with the cash difference.
				runObjectDetail.getObjectData().put(ReconciliationField.ADJUSTMENT_AMOUNT_DIFF, Collections.singletonList(computeDifference(cashBalanceDiffList, adjustmentAmountList, true)));
			}
		}
	}


	private BigDecimal computeDifference(ReconciliationRunObjectDetail runObjectDetail, String primaryFieldName, String secondaryFieldName, boolean add) {
		List<Object> primaryValueList = runObjectDetail.getObjectData().get(primaryFieldName);
		List<Object> secondaryValueList = runObjectDetail.getObjectData().get(secondaryFieldName);
		//Set valueList to a singleton list of BigDecimal.ZERO if no existing list values.
		runObjectDetail.getObjectData().put(primaryFieldName, primaryValueList == null ? Collections.singletonList(BigDecimal.ZERO) : primaryValueList);
		runObjectDetail.getObjectData().put(secondaryFieldName, secondaryValueList == null ? Collections.singletonList(BigDecimal.ZERO) : secondaryValueList);
		return computeDifference(primaryValueList, secondaryValueList, add);
	}


	private BigDecimal computeDifference(List<Object> primaryValueList, List<Object> secondaryValueList, boolean add) {
		BigDecimal primaryValue = BigDecimal.ZERO;
		BigDecimal secondaryValue = BigDecimal.ZERO;
		if (!CollectionUtils.isEmpty(primaryValueList)) {
			primaryValue = DataTypeNameUtils.convertObjectToDataTypeName(CollectionUtils.getFirstElement(primaryValueList), DataTypeNames.DECIMAL, BigDecimal.class);
		}
		if (!CollectionUtils.isEmpty(secondaryValueList)) {
			secondaryValue = DataTypeNameUtils.convertObjectToDataTypeName(CollectionUtils.getFirstElement(secondaryValueList), DataTypeNames.DECIMAL, BigDecimal.class);
		}
		if (add) {
			return MathUtils.add(primaryValue, secondaryValue);
		}
		else {
			return MathUtils.subtract(primaryValue, secondaryValue);
		}
	}


	private List<ReconciliationRunObjectDetail> getSubList(List<ReconciliationRunObjectDetail> runObjectDetailList, ReconciliationRunObjectSearchForm searchForm) {
		int subIndex = searchForm.getStart() + searchForm.getLimit();
		if (subIndex > runObjectDetailList.size()) {
			subIndex = runObjectDetailList.size();
		}
		if (searchForm.getStart() > subIndex) {
			searchForm.setStart(0);
		}
		return runObjectDetailList.subList(searchForm.getStart(), subIndex);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationEvaluatorService getReconciliationEvaluatorService() {
		return this.reconciliationEvaluatorService;
	}


	public void setReconciliationEvaluatorService(ReconciliationEvaluatorService reconciliationEvaluatorService) {
		this.reconciliationEvaluatorService = reconciliationEvaluatorService;
	}


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public ReconciliationStatusService getReconciliationStatusService() {
		return this.reconciliationStatusService;
	}


	public void setReconciliationStatusService(ReconciliationStatusService reconciliationStatusService) {
		this.reconciliationStatusService = reconciliationStatusService;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}


	public JsonHandler<?> getJsonHandler() {
		return this.jsonHandler;
	}


	public void setJsonHandler(JsonHandler<?> jsonHandler) {
		this.jsonHandler = jsonHandler;
	}


	public ReconciliationRunDetailAdjustmentAmountMapCache getReconciliationRunDetailAdjustmentAmountMapCache() {
		return this.reconciliationRunDetailAdjustmentAmountMapCache;
	}


	public void setReconciliationRunDetailAdjustmentAmountMapCache(ReconciliationRunDetailAdjustmentAmountMapCache reconciliationRunDetailAdjustmentAmountMapCache) {
		this.reconciliationRunDetailAdjustmentAmountMapCache = reconciliationRunDetailAdjustmentAmountMapCache;
	}
}
