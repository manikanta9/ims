package com.clifton.reconciliation.run.detail.cache;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.reconciliation.event.object.ReconciliationEventObject;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectCategories;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectJournal;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectService;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectStatuses;
import com.clifton.reconciliation.event.object.search.ReconciliationEventObjectSearchForm;
import com.clifton.reconciliation.run.detail.ReconciliationRunDetailDataRetriever;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Component
public class ReconciliationRunDetailAdjustmentAmountMapCacheImpl extends SelfRegisteringSimpleDaoCache<ReconciliationEventObject, Integer, Map<Long, BigDecimal>> implements ReconciliationRunDetailAdjustmentAmountMapCache {

	private ReconciliationEventObjectService reconciliationEventObjectService;

	@Override
	public Map<Long, BigDecimal> getReconciliationRunDetailAdjustmentAmountMap(int runId, Integer[] runIds) {
		Map<Long, BigDecimal> adjustmentAmountMap = getCacheHandler().get(getCacheName(), runId);
		if (adjustmentAmountMap == null) {
			List<ReconciliationEventObject<?>> eventObjectList = new ArrayList<>();
			for (Integer id : runIds) {
				ReconciliationEventObjectSearchForm eventObjectSearchForm = new ReconciliationEventObjectSearchForm();
				eventObjectSearchForm.setRunOrParentRunId(id);
				eventObjectSearchForm.setDataTypeCategories(new String[]{ReconciliationEventObjectCategories.ADJUSTING_JOURNAL.name(), ReconciliationEventObjectCategories.SIMPLE_JOURNAL.name()});
				eventObjectSearchForm.setStatus(ReconciliationEventObjectStatuses.COMPLETE);
				eventObjectList.addAll(getReconciliationEventObjectService().getReconciliationEventObjectList(eventObjectSearchForm));
			}
			adjustmentAmountMap = ReconciliationRunDetailDataRetriever.getAdjustmentAmountMap(eventObjectList);
			if (!CollectionUtils.isEmpty(adjustmentAmountMap)) {
				getCacheHandler().put(getCacheName(), runId, adjustmentAmountMap);
			}
		}
		return adjustmentAmountMap;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unused")
	public void afterMethodCallImpl(ReadOnlyDAO<ReconciliationEventObject> dao, DaoEventTypes event, ReconciliationEventObject eventObject, Throwable e) {
		if (e == null) {
			if (eventObject instanceof ReconciliationEventObjectJournal) {
				ReconciliationEventObjectJournal eventObjectJournal = (ReconciliationEventObjectJournal) eventObject;
				if (eventObjectJournal.getRunObject() != null && eventObjectJournal.getRunObject().getReconciliationRun() != null) {
					int runId = eventObjectJournal.getRunObject().getReconciliationRun().getId();
					if (getCacheHandler().get(getCacheName(), runId) != null) {
						getCacheHandler().remove(getCacheName(), runId);
					}
				}
				if (eventObjectJournal.getParentRunObject() != null && eventObjectJournal.getParentRunObject().getReconciliationRun() != null) {
					int runId = eventObjectJournal.getParentRunObject().getReconciliationRun().getId();
					if (getCacheHandler().get(getCacheName(), runId) != null) {
						getCacheHandler().remove(getCacheName(), runId);
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationEventObjectService getReconciliationEventObjectService() {
		return this.reconciliationEventObjectService;
	}


	public void setReconciliationEventObjectService(ReconciliationEventObjectService reconciliationEventObjectService) {
		this.reconciliationEventObjectService = reconciliationEventObjectService;
	}
}
