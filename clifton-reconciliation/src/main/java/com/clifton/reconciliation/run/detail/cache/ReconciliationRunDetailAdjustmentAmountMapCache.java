package com.clifton.reconciliation.run.detail.cache;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;


@Component
public interface ReconciliationRunDetailAdjustmentAmountMapCache {

	public Map<Long, BigDecimal> getReconciliationRunDetailAdjustmentAmountMap(int runId, Integer[] runIds);
}
