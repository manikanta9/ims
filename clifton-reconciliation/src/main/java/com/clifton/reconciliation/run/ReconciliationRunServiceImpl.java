package com.clifton.reconciliation.run;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.DaoException;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.ReconciliationBreakDaysResultSetExtractor;
import com.clifton.reconciliation.ReconciliationService;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionService;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.definition.type.ReconciliationType;
import com.clifton.reconciliation.definition.type.ReconciliationTypeService;
import com.clifton.reconciliation.event.object.ReconciliationEventObject;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectAccount;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectJournal;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectService;
import com.clifton.reconciliation.event.object.search.ReconciliationEventObjectSearchForm;
import com.clifton.reconciliation.exclusion.ReconciliationExclusionAssignmentService;
import com.clifton.reconciliation.exclusion.search.ReconciliationExclusionAssignmentSearchForm;
import com.clifton.reconciliation.file.ReconciliationFileDefinition;
import com.clifton.reconciliation.file.ReconciliationFileDefinitionService;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.reconcile.ReconciliationReconcileService;
import com.clifton.reconciliation.run.data.DataError;
import com.clifton.reconciliation.run.data.DataNote;
import com.clifton.reconciliation.run.data.DataNoteTypes;
import com.clifton.reconciliation.run.data.DataObject;
import com.clifton.reconciliation.run.search.ReconciliationRunObjectSearchForm;
import com.clifton.reconciliation.run.search.ReconciliationRunOrphanObjectSearchForm;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationStatusService;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author stevenf
 */
@Service
public class ReconciliationRunServiceImpl implements ReconciliationRunService {

	private ReconciliationDefinitionService reconciliationDefinitionService;
	private ReconciliationEventObjectService reconciliationEventObjectService;
	private ReconciliationExclusionAssignmentService reconciliationExclusionAssignmentService;
	private ReconciliationFileDefinitionService reconciliationFileDefinitionService;
	private ReconciliationStatusService reconciliationStatusService;
	private ReconciliationTypeService reconciliationTypeService;
	private ReconciliationReconcileService reconciliationReconcileService;
	private ReconciliationService reconciliationService;


	private SecurityUserService securityUserService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	private AdvancedUpdatableDAO<ReconciliationRunObject, Criteria> reconciliationRunObjectDAO;
	private AdvancedUpdatableDAO<ReconciliationRunOrphanObject, Criteria> reconciliationRunOrphanObjectDAO;
	private AdvancedUpdatableDAO<ReconciliationRun, Criteria> reconciliationRunDAO;

	private SqlHandler sqlHandler;

	////////////////////////////////////////////////////////////////////////////
	////////            Reconciliation Run Business Methods            /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationRun getReconciliationRun(int id) {
		ReconciliationRun run = getReconciliationRunDAO().findByPrimaryKey(id);
		if (run != null && run.getReconciliationDefinition() != null) {
			//Get fully populated definition and set on the run
			run.setReconciliationDefinition(getReconciliationDefinitionService().getReconciliationDefinitionPopulated(run.getReconciliationDefinition().getId(), run.getRunDate()));
		}
		return run;
	}


	@Override
	public List<ReconciliationRun> getReconciliationRunListByIds(Integer[] ids) {
		ReconciliationRunSearchForm searchForm = new ReconciliationRunSearchForm();
		searchForm.setIds(ids);
		return getReconciliationRunList(searchForm);
	}


	@Override
	public ReconciliationRun getReconciliationRunByDefinitionAndDate(int definitionId, Date runDate) {
		ReconciliationRunSearchForm searchForm = new ReconciliationRunSearchForm();
		searchForm.setDefinitionId(definitionId);
		searchForm.setRunDate(runDate);
		return CollectionUtils.getFirstElement(getReconciliationRunList(searchForm));
	}


	@Override
	public List<ReconciliationRun> getReconciliationRunList(ReconciliationRunSearchForm searchForm) {
		if (searchForm.getOrderBy() == null) {
			searchForm.setOrderBy("runDate:desc");
		}
		if (searchForm.isSearchRestrictionSet("typeName")) {
			ReconciliationType rt = getReconciliationTypeService().getReconciliationTypeByName((String) searchForm.getSearchRestrictionValue("typeName"));
			ValidationUtils.assertNotNull(rt, () -> "Reconciliation Type: '" + searchForm.getSearchRestrictionValue("typeName") + "' does not exist.");
			searchForm.setTypeId(rt.getId());
			searchForm.removeSearchRestrictionAndBeanField("typeName");
		}
		if (searchForm.isSearchRestrictionSet("typeOrParentTypeName")) {
			ReconciliationType rt = getReconciliationTypeService().getReconciliationTypeByName((String) searchForm.getSearchRestrictionValue("typeOrParentTypeName"));
			ValidationUtils.assertNotNull(rt, () -> "Reconciliation Type: '" + searchForm.getSearchRestrictionValue("typeOrParentTypeName") + "' does not exist.");
			searchForm.setTypeId(rt.getId());
			searchForm.removeSearchRestrictionAndBeanField("typeOrParentTypeName");
		}
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (searchForm.getSourceName() != null) {
					String primarySource = getPathAlias("reconciliationDefinition.primarySource", criteria);
					String secondarySource = getPathAlias("reconciliationDefinition.secondarySource", criteria);
					criteria.add(Restrictions.or(Restrictions.eq(primarySource + ".name", searchForm.getSourceName()), Restrictions.eq(secondarySource + ".name", searchForm.getSourceName())));
				}
			}
		};
		return getReconciliationRunDAO().findBySearchCriteria(config);
	}


	@Override
	public ReconciliationRun saveReconciliationRun(ReconciliationRun run) {
		return getReconciliationRunDAO().save(run);
	}


	@Override
	@Transactional
	public void deleteReconciliationRun(int id) {
		getReconciliationRunDAO().delete(id);
	}


	@Override
	public void approveReconciliationRun(int id) {
		ReconciliationRun run = getReconciliationRun(id);
		isApproveActionAllowed(run, true);
		run.setApprovedByUser(getSecurityUserService().getSecurityUserCurrent());
		//ReconciliationRunObserver triggers Modify condition that checks approval user is null which is what we are trying to set.
		DaoUtils.executeWithAllObserversDisabled(() -> saveReconciliationRun(run));
	}


	@Override
	public void unapproveReconciliationRun(int id, String note) {
		ValidationUtils.assertNotEmpty(note, "You must specify a note when you unapprove a run!");
		ReconciliationRun run = getReconciliationRun(id);
		isApproveActionAllowed(run, false);
		run.setApprovedByUser(null);
		run.setNote(note);
		saveReconciliationRun(run);
	}


	private boolean isApproveActionAllowed(ReconciliationRun run, boolean approval) {
		if (run != null && run.getReconciliationDefinition() != null) {
			ReconciliationDefinition definition = run.getReconciliationDefinition();
			SystemCondition runApprovalSystemCondition = ObjectUtils.coalesce(definition.getRunApprovalSystemCondition(), definition.getDefinitionGroup() != null ? definition.getDefinitionGroup().getRunApprovalSystemCondition() : null, definition.getType().getRunApprovalSystemCondition());
			if (runApprovalSystemCondition != null) {
				EvaluationResult result = getSystemConditionEvaluationHandler().evaluateCondition(runApprovalSystemCondition, run);
				if (!result.isResult()) {
					throw new ValidationException(result.getMessage());
				}
			}
			if (approval) {
				ValidationUtils.assertNull(run.getApprovedByUser(), "You may not approve a run that has already been approved.");
				ReconciliationExclusionAssignmentSearchForm searchForm = new ReconciliationExclusionAssignmentSearchForm();
				searchForm.setApprovalUserIdIsNull(true);
				searchForm.setActiveOnDate(run.getRunDate());
				searchForm.setDefinitionIdOrNull(run.getReconciliationDefinition().getId());
				searchForm.setSourceIds(new Short[]{run.getReconciliationDefinition().getPrimarySource().getId(), run.getReconciliationDefinition().getSecondarySource().getId()});
				ValidationUtils.assertEmpty(getReconciliationExclusionAssignmentService().getReconciliationExclusionAssignmentList(searchForm), "You may not approve a run while there are unapproved exclusion assignments applicable to it!");
			}
			else {
				ValidationUtils.assertNotNull(run.getApprovedByUser(), "You may not unapprove a run that has not been approved.");
			}
		}
		return run != null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////        Reconciliation Run Object Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationRunObject getReconciliationRunObject(long id) {
		ReconciliationRunObject runObject = getReconciliationRunObjectDAO().findByPrimaryKey(id);
		if (runObject != null && CollectionUtils.isEmpty(runObject.getDataHolder().getDataObjects())) {
			LogUtils.info(getClass(), "RunObject with id [" + runObject.getId() + "] is corrupt.", new Throwable());
		}
		return runObject;
	}


	@Override
	public List<ReconciliationRunObject> getReconciliationRunObjectList(Long[] ids) {
		List<ReconciliationRunObject> runObjectList = getReconciliationRunObjectDAO().findByPrimaryKeys(ids);
		runObjectList.forEach(runObject -> {
			if (runObject != null && CollectionUtils.isEmpty(runObject.getDataHolder().getDataObjects())) {
				LogUtils.info(getClass(), "RunObject with id [" + runObject.getId() + "] is corrupt.", new Throwable());
			}
		});
		return runObjectList;
	}


	@Override
	public List<ReconciliationRunObject> getReconciliationRunObjectList(ReconciliationRunObjectSearchForm searchForm) {
		final Date previousRunDate = searchForm.getPreviousRunDate();
		if (previousRunDate != null) {
			ValidationUtils.assertNotNull(searchForm.getDefinitionId(), "Must provide definition id when searching for the last run date.");
			HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {
				@Override
				public void configureCriteria(Criteria criteria) {
					super.configureCriteria(criteria);
					DetachedCriteria maxDateCriteria = DetachedCriteria.forClass(ReconciliationRun.class);
					maxDateCriteria.setProjection(Projections.max("runDate"));
					maxDateCriteria.add(Restrictions.eq("reconciliationDefinition.id", searchForm.getDefinitionId()));
					maxDateCriteria.add(Restrictions.lt("runDate", previousRunDate));
					criteria.add(Subqueries.propertyEq(getPathAlias("reconciliationRun", criteria) + ".runDate", maxDateCriteria));
				}
			};
			return getReconciliationRunObjectDAO().findBySearchCriteria(config);
		}
		List<ReconciliationRunObject> runObjectList = getReconciliationRunObjectDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		runObjectList.forEach(runObject -> {
			if (runObject != null && CollectionUtils.isEmpty(runObject.getDataHolder().getDataObjects())) {
				LogUtils.info(getClass(), "RunObject with id [" + runObject.getId() + "] is corrupt.", new Throwable());
			}
		});
		return runObjectList;
	}


	@Override
	public List<ReconciliationRunObject> getReconciliationRunObjectListByRunId(int runId) {
		ReconciliationRunObjectSearchForm searchForm = new ReconciliationRunObjectSearchForm();
		searchForm.setRunId(runId);
		return getReconciliationRunObjectList(searchForm);
	}


	@Override
	public List<ReconciliationRunObject> getReconciliationRunObjectListByRunIdAndReconcileStatusName(int runId, String reconcileStatusName) {
		ReconciliationRunObjectSearchForm searchForm = new ReconciliationRunObjectSearchForm();
		searchForm.setRunId(runId);
		searchForm.setReconcileStatusId(getReconciliationStatusService().getReconciliationReconcileStatusByName(reconcileStatusName).getId());
		searchForm.setOrderBy("investmentAccountId:DESC#investmentSecurityId:DESC");
		return getReconciliationRunObjectList(searchForm);
	}


	@Override
	public ReconciliationRunObject saveReconciliationRunObject(ReconciliationRunObject runObject) {
		if (runObject != null && CollectionUtils.isEmpty(runObject.getDataHolder().getDataObjects())) {
			LogUtils.info(getClass(), "RunObject with id [" + runObject.getId() + "] is corrupt.", new Throwable());
		}
		AssertUtils.assertNull(runObject.getReconciliationRun().getApprovedByUser(), "The run object [" + runObject.getId() + "] is part of an approved run and cannot be edited.");
		return getReconciliationRunObjectDAO().save(runObject);
	}


	/**
	 * Utilizes batch inserts with observers disabled
	 */
	@Override
	public void saveReconciliationRunObjectList(final List<ReconciliationRunObject> runObjectList) {
		//Debugging for corrupt objects.
		runObjectList.forEach(runObject -> {
			AssertUtils.assertNull(runObject.getReconciliationRun().getApprovedByUser(), "The run object [" + runObject.getId() + "] is part of an approved run and cannot be edited.");
			if (CollectionUtils.isEmpty(runObject.getDataHolder().getDataObjects())) {
				LogUtils.info(getClass(), "RunObject with id [" + runObject.getId() + "] is corrupt.", new Throwable());
			}
		});
		try {
			long saveTime = System.currentTimeMillis();
			LogUtils.info(getClass(), "Saving " + CollectionUtils.getSize(runObjectList) + " run object(s).");
			DaoUtils.executeWithAllObserversDisabled(() -> getReconciliationRunObjectDAO().saveList(runObjectList));
			LogUtils.info(getClass(), "Time to save: " + (System.currentTimeMillis() - saveTime));
		}
		catch (Exception e) {
			LogUtils.error(getClass(), "Failed to save: " + e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}


	@Override
	public void deleteReconciliationRunObject(long id) {
		getReconciliationEventObjectService().deleteReconciliationEventObjectList(getReconciliationEventObjectService().getReconciliationEventObjectListByRunObjectId(id));
		getReconciliationRunObjectDAO().delete(id);
	}


	@Override
	public List<ReconciliationRunObject> getReconciliationParentRunObjectList(long runObjectId, Date runDate) {
		ReconciliationRunObject reconcileRunObject = getReconciliationRunObject(runObjectId);
		ValidationUtils.assertNotNull(reconcileRunObject, "Reconciliation Run Object cannot be found for id " + runObjectId);
		ReconciliationRun reconciliationRun = reconcileRunObject.getReconciliationRun();
		if (reconciliationRun != null && Optional.of(reconciliationRun).map(ReconciliationRun::getReconciliationDefinition).map(ReconciliationDefinition::getParentDefinition).isPresent()
				&& reconcileRunObject.getReconciliationInvestmentAccount() != null) {
			//Find the object(s) on the parentRun with the same account
			ReconciliationRunObjectSearchForm searchForm = new ReconciliationRunObjectSearchForm();
			searchForm.setDefinitionId(reconciliationRun.getReconciliationDefinition().getParentDefinition().getId());
			searchForm.setRunDate(DateUtils.clearTime(runDate));
			searchForm.setInvestmentAccountId(reconcileRunObject.getReconciliationInvestmentAccount().getId());
			return getReconciliationRunObjectList(searchForm);
		}
		return Collections.emptyList();
	}


	@Override
	@ResponseBody
	public Map<Long, Set<Long>> getReconciliationParentRunObjectMapByRunObjectIds(Long[] runObjectIds, Date runDate) {
		Map<Long, Set<Long>> results = new HashMap<>();
		for (Long runObjectId : runObjectIds) {
			List<ReconciliationRunObject> parents = getReconciliationParentRunObjectList(runObjectId, runDate);
			results.putIfAbsent(runObjectId, new HashSet<>());
			results.get(runObjectId).addAll(parents.stream().map(ReconciliationRunObject::getId).collect(Collectors.toSet()));
		}
		return results;
	}

	////////////////////////////////////////////////////////////////////////////
	///////      Reconciliation Run Orphan Object Business Methods       ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationRunOrphanObject getReconciliationRunOrphanObject(long id) {
		return getReconciliationRunOrphanObjectDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconciliationRunOrphanObject> getReconciliationRunOrphanObjectList(ReconciliationRunOrphanObjectSearchForm searchForm) {
		return getReconciliationRunOrphanObjectDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<ReconciliationRunOrphanObject> getReconciliationRunOrphanObjectListByRunId(int runId) {
		ReconciliationRunOrphanObjectSearchForm searchForm = new ReconciliationRunOrphanObjectSearchForm();
		searchForm.setExcludedRunId(runId);
		return getReconciliationRunOrphanObjectList(searchForm);
	}


	@Override
	public ReconciliationRunOrphanObject saveReconciliationRunOrphanObject(ReconciliationRunOrphanObject orphanedSourceData) {
		return getReconciliationRunOrphanObjectDAO().save(orphanedSourceData);
	}


	@Override
	public void deleteReconciliationRunOrphanObject(long id) {
		getReconciliationRunOrphanObjectDAO().delete(id);
	}


	@Override
	public void deleteReconciliationRunOrphanObjectList(List<ReconciliationRunOrphanObject> orphanObjectList) {
		getReconciliationRunOrphanObjectDAO().deleteList(orphanObjectList);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////       Reconciliation Run Details and Processing        ////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Manually Reconciling does the following:
	 * (2) Validates if objects may be manually reconciled
	 * (1) Matches objects if not all already matched.
	 * -- This will delete the original objects and create a new matched object.
	 * (3) Saves the 'RECONCILE_PRIMARY' or 'RECONCILE_SECONDARY' note
	 * (4) Updates the ReconciliationRunObject reconciledOnSource to the source we are reconciling on
	 * (5) Updates the ReconciliationRunObject Status to MANUALLY RECONCILED
	 * (6) Executes reconciliation on the run to update overall run status and counts.
	 */
	@Override
	public void reconcileReconciliationRunObjectList(Long[] runObjectIds, boolean reconcileToPrimary, String note) {
		ValidationUtils.assertNotNull(runObjectIds, "You must provide run object ids to reconcile!");
		ValidationUtils.assertNotEmpty(note, "A note is required to force reconcile run objects!");

		List<ReconciliationRunObject> runObjectList = getReconciliationRunObjectList(runObjectIds);
		if (!CollectionUtils.isEmpty(runObjectList)) {
			boolean noneMatch = runObjectList.stream().noneMatch(obj -> obj.getReconciliationMatchStatus().isMatched());
			boolean allMatch = runObjectList.stream().allMatch(obj -> obj.getReconciliationMatchStatus().isMatched());
			validateManualReconcile(runObjectList, noneMatch, allMatch);
			// if there's more than one object in the list and it contains values from both sources, we need to match first
			// if they are all one source then force reconcile them separately and do not match
			if (runObjectList.size() > 1 && noneMatch && runObjectList.stream().flatMap(runObject -> runObject.getDataHolder().getDataObjects().stream().map(DataObject::getSourceId)).distinct().count() > 1) {
				// automatically match and then force reconcile the match - we create a new array from the singletonList so it is mutable, otherwise batch save fails later
				runObjectList = new ArrayList<>(Collections.singletonList(matchReconciliationRunObjectList(runObjectList, "[AUTOMATIC] ".concat(note))));
			}
		}
		doReconcileReconciliationRunObjectList(runObjectList, note, reconcileToPrimary);
	}


	private void validateManualReconcile(List<ReconciliationRunObject> reconciliationRunObjectList, boolean noneMatch, boolean allMatch) {
		ValidationUtils.assertNotEmpty(reconciliationRunObjectList, "No objects found to manually reconcile!");
		if (reconciliationRunObjectList.stream().anyMatch(runObject -> runObject.getReconciliationReconcileStatus().isReconciled())) {
			ValidationUtils.fail("Cannot force reconcile items that are already reconciled.");
		}

		// validate that there's not a mix of matched and unmatched records
		if (reconciliationRunObjectList.size() > 1 && !noneMatch && !allMatch) {
			ValidationUtils.fail("Cannot force reconcile matched and unmatched items.  Please reconcile one at a time.");
		}
	}


	@Transactional
	protected void doReconcileReconciliationRunObjectList(List<ReconciliationRunObject> runObjectList, String note, boolean reconcileToPrimary) {
		short createUserId = getSecurityUserService().getSecurityUserCurrent().getId();
		ReconciliationRun run = CollectionUtils.getFirstElementStrict(runObjectList).getReconciliationRun();
		ReconciliationSource reconciledOnSource = reconcileToPrimary ? run.getReconciliationDefinition().getPrimarySource() : run.getReconciliationDefinition().getSecondarySource();
		for (ReconciliationRunObject runObject : CollectionUtils.getIterable(runObjectList)) {
			// save the note for all the ReconciliationRunObject identifiers it is associated with
			runObject.addDataNote(note, reconcileToPrimary ? DataNoteTypes.RECONCILE_PRIMARY : DataNoteTypes.RECONCILE_SECONDARY, createUserId);
			// update reconciled on source to indicate it was manually reconciled to a specific source
			runObject.setReconciledOnSource(reconciledOnSource);
			// update status to MANUALLY_RECONCILED
			runObject.setReconciliationReconcileStatus(getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED));
		}
		// save in a batch
		saveReconciliationRunObjectList(runObjectList);
		//Execute reconciliation to update run status and counts
		getReconciliationReconcileService().reconcileReconciliationRun(run, runObjectList.stream().map(ReconciliationRunObject::getId).toArray(Long[]::new));
	}


	/**
	 * Since we track match and reconciliation statuses separately, we just need to update the status and save the note.
	 */
	@Override
	@Transactional
	public void unreconcileReconciliationRunObjectList(Long[] runObjectIds, String note) {
		ValidationUtils.assertNotEmpty(note, "A note is required to un-reconcile values.");
		ValidationUtils.assertNotNull(runObjectIds, "Reconciliation Object ids are required.");
		List<ReconciliationRunObject> runObjectList = getReconciliationRunObjectList(runObjectIds);
		if (!CollectionUtils.isEmpty(runObjectList)) {
			short createUserId = getSecurityUserService().getSecurityUserCurrent().getId();
			ReconciliationRun run = CollectionUtils.getFirstElementStrict(runObjectList).getReconciliationRun();
			for (ReconciliationRunObject runObject : CollectionUtils.getIterable(runObjectList)) {
				runObject.setReconciledOnSource(null);
				runObject.setReconciliationReconcileStatus(getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.NOT_RECONCILED));
				runObject.addDataNote(note, DataNoteTypes.UNRECONCILE, createUserId);
				saveReconciliationRunObject(runObject);
			}
			saveReconciliationRun(run);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////              Reconciliation Notes Methods             /////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Adds and saves a new DataNote for all objects.
	 */
	@Override
	@Transactional
	public void saveReconciliationRunObjectDataNoteList(Long[] runObjectIds, String note) {
		ValidationUtils.assertNotNull(runObjectIds, "Reconciliation Object ids are required.");
		ValidationUtils.assertNotEmpty(note, "A message is required to add a note.");
		short createUserId = getSecurityUserService().getSecurityUserCurrent().getId();
		List<ReconciliationRunObject> runObjectList = getReconciliationRunObjectList(runObjectIds);
		for (ReconciliationRunObject runObject : CollectionUtils.getIterable(runObjectList)) {
			runObject.addDataNote(note, DataNoteTypes.OTHER, createUserId);
		}
		//Save in a batch
		saveReconciliationRunObjectList(runObjectList);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Matching/Linking Methods                /////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Matches two or more ReconciliationObjects
	 * Executes the reconciliation objects ('rows') together
	 * This process does not create new ReconciliationObjects, it only merges them into an existing one and deletes the other ones.
	 * (1) Takes one of the ReconciliationObjects and appends the value/json data (ReconciliationObjectReconcileData) from the other to this one
	 * (2) Updates the source data (ReconciliationObjectSourceData) of the other ReconciliationObjects so it is now associated with the merged ReconciliationRunObject
	 * (3) Deletes the other ReconciliationObjects
	 * (4) Updates the status on the matched/linked object (can change to READY if we now have all the sources)
	 * (5) Saves the original ReconciliationRunObject with the merged value/json data
	 * No need to re-run the import/upload or the reconciliation run process as we do not need to create new ReconciliationObjects or set any statuses
	 */
	@Override
	public ReconciliationRunObject matchReconciliationRunObjectList(Long[] runObjectIds, String note) {
		ValidationUtils.assertNotNull(runObjectIds, "You must select at least two objects to match!");
		ValidationUtils.assertTrue(runObjectIds.length >= 2, "You must select at least two objects to match!");
		ValidationUtils.assertNotEmpty(note, "A note is required to match objects!");
		// retrieve the ReconciliationObjects from the ids
		List<ReconciliationRunObject> runObjectList = getReconciliationRunObjectList(runObjectIds);
		return matchReconciliationRunObjectList(runObjectList, note);
	}


	@Override
	@Transactional
	public ReconciliationRunObject matchReconciliationRunObjectList(List<ReconciliationRunObject> runObjectList, String note) {
		// validate that these objects can be linked
		validateMatching(runObjectList);
		ReconciliationRunObject matchedObject = doMatchReconciliationRunObjectList(runObjectList, note);
		getReconciliationReconcileService().reconcileReconciliationRun(matchedObject.getReconciliationRun(), new Long[]{matchedObject.getId()});
		//Refresh the object, rv potentially out of date
		matchedObject = getReconciliationRunObject(matchedObject.getId());
		if (matchedObject.isReconciled()) {
			matchedObject.addDataNote("Reconciled after manual match.", DataNoteTypes.AUTO_RECONCILE, getSecurityUserService().getSecurityUserCurrent().getId());
		}
		matchedObject.setDataNoteList(filterDuplicateNotes(matchedObject.getDataNoteList()));
		return saveReconciliationRunObject(matchedObject);
	}


	private void validateMatching(List<ReconciliationRunObject> runObjectList) {
		ValidationUtils.assertTrue(CollectionUtils.getSize(runObjectList) > 1, "You must select more than one object to match!");

		boolean matchingOnSameSource = true;
		ReconciliationSource reconciliationSource = null;
		ReconciliationInvestmentAccount holdingAccount = null;
		for (ReconciliationRunObject runObject : CollectionUtils.getIterable(runObjectList)) {
			if (runObject.isReconciled()) {
				throw new ValidationException("You cannot match items that are already reconciled!");
			}
			if (holdingAccount == null) {
				holdingAccount = runObject.getReconciliationInvestmentAccount();
			}
			else {
				if (!Objects.equals(holdingAccount, runObject.getReconciliationInvestmentAccount())) {
					throw new ValidationException("You cannot match items with different Holding Accounts!");
				}
			}
			//Now we want to ensure there is more than one source in the data being match, across all objects,
			// which is why the boolean tracking the source values has global scope.
			for (DataObject dataObject : CollectionUtils.getIterable(runObject.getDataHolder().getDataObjects())) {
				if (reconciliationSource == null) {
					reconciliationSource = getReconciliationDefinitionService().getReconciliationSource(dataObject.getSourceId());
				}
				else {
					if (!reconciliationSource.getId().equals(dataObject.getSourceId())) {
						matchingOnSameSource = false;
					}
				}
			}
		}
		if (matchingOnSameSource) {
			throw new ValidationException("Matched items must contain data from both sources!");
		}
	}


	private ReconciliationRunObject doMatchReconciliationRunObjectList(List<ReconciliationRunObject> runObjectList, String note) {
		// use the first one in the list as the primary to which all the others will be merged to
		ReconciliationRunObject matchedObject = null;
		List<ReconciliationEventObject<?>> eventObjectList = new ArrayList<>();
		for (ReconciliationRunObject runObject : CollectionUtils.getIterable(runObjectList)) {
			if (matchedObject == null) {
				matchedObject = new ReconciliationRunObject();
				BeanUtils.copyProperties(runObject, matchedObject, BeanUtils.AUDIT_PROPERTIES);
			}
			else {
				// add all the value/json data items that need to be merged to the primary object's value/json data list
				matchedObject.getDataHolder().getDataObjects().addAll(runObject.getDataHolder().getDataObjects());
				if (matchedObject.getDataNoteList() != null && runObject.getDataNoteList() != null) {
					matchedObject.getDataNoteList().addAll(runObject.getDataNoteList());
				}
			}
			if (!runObject.isNewBean()) {
				//Add existing events to list before deleting to later save to new object
				eventObjectList.addAll(getReconciliationEventObjectService().getReconciliationEventObjectListByRunObjectId(runObject.getId()));
				// delete the merged object
				deleteReconciliationRunObject(runObject.getId());
			}
		}
		//Should not be null since we validated two items existed earlier
		AssertUtils.assertNotNull(matchedObject, "Failed to match objects!");
		//If statement just to avoid warning...
		if (matchedObject != null) {
			matchedObject.setReconciliationMatchStatus(getReconciliationStatusService().getReconciliationMatchStatusByStatusType(ReconciliationMatchStatusTypes.MANUALLY_MATCHED));
			matchedObject.addDataNote(note, DataNoteTypes.MATCH, getSecurityUserService().getSecurityUserCurrent().getId());
			matchedObject = saveReconciliationRunObject(matchedObject);
			for (ReconciliationEventObject<?> eventObject : CollectionUtils.getIterable(eventObjectList)) {
				eventObject.setRunObject(matchedObject);
				getReconciliationEventObjectService().updateReconciliationEventObject(eventObject);
			}
			return saveReconciliationRunObject(matchedObject);
		}
		return matchedObject;
	}


	private List<DataNote> filterDuplicateNotes(List<DataNote> dataNoteList) {
		List<DataNote> filteredNoteList = new ArrayList<>();
		//We want to maintain order of the notes when filtering duplicates
		Map<String, List<DataNote>> duplicateNoteMap = new LinkedHashMap<>();
		if (dataNoteList != null) {
			//Explicitly iterate rather than use stream to preserve order
			for (DataNote dataNote : CollectionUtils.getIterable(dataNoteList)) {
				List<DataNote> duplicateNoteList = duplicateNoteMap.get(dataNote.getType().name() + "_" + dataNote.getNote());
				if (duplicateNoteList == null) {
					duplicateNoteList = new ArrayList<>();
				}
				duplicateNoteList.add(dataNote);
				duplicateNoteMap.put(dataNote.getType().name() + "_" + dataNote.getNote(), duplicateNoteList);
			}
		}
		for (Map.Entry<String, List<DataNote>> duplicateNoteEntry : duplicateNoteMap.entrySet()) {
			DataNote finalNote = duplicateNoteEntry.getValue().get(0);
			if (duplicateNoteEntry.getValue().size() > 1) {
				//This note had a duplicate, so we want to clear the sourceId and make it 'global' (applies to both sources)
				finalNote.setSourceId(null);
			}
			filteredNoteList.add(finalNote);
		}
		return filteredNoteList;
	}


	/**
	 * Removes the match/link of the specified ReconciliationObjects
	 * Executes each reconciliation object ('row') separately
	 * ReconciliationRunObject Status changes from READY to MISSING RECORD or remains as NOT RECONCILED
	 * ReconciliationRun Status should not change
	 * This process creates new ReconciliationObjects and deletes the original ones.
	 * <p>
	 * Since we are undoing an action, we need to re-run both the upload/import process and the reconciliation run process
	 * We need to create new ReconciliationObjects when we split back out the match
	 * and those ReconciliationObjects will need the appropriate statues and errors set on them.
	 */
	@Override
	public List<ReconciliationRunObject> unmatchReconciliationRunObjectList(Long[] runObjectIds, String note) {
		ValidationUtils.assertNotNull(runObjectIds, "You must select at least one object to unmatch!");
		ValidationUtils.assertNotEmpty(note, "A note is required to unmatch objects!");
		List<ReconciliationRunObject> runObjectList = unmatchReconciliationRunObjectList(getReconciliationRunObjectList(runObjectIds), note);
		//Save the run to update status and counts.
		saveReconciliationRun(CollectionUtils.getFirstElementStrict(runObjectList).getReconciliationRun());
		return runObjectList;
	}


	@Override
	@Transactional
	public List<ReconciliationRunObject> unmatchReconciliationRunObjectList(List<ReconciliationRunObject> runObjectList, String note) {
		// validate that these objects can be linked
		validateUnmatching(runObjectList);
		HashMap<String, ReconciliationRunObject> runObjectMap = new HashMap<>();
		reprocessReconciliationRunObjectList(runObjectList, runObjectMap);

		short createUserId = getSecurityUserService().getSecurityUserCurrent().getId();
		runObjectList = new ArrayList<>(runObjectMap.values());
		for (ReconciliationRunObject runObject : CollectionUtils.getIterable(runObjectList)) {
			List<Short> sourceIdList = runObject.getDataHolder().getDataObjects().stream().map(DataObject::getSourceId).collect(Collectors.toList());
			//Filter the notes now to only global (null sourceId) or notes that apply to sources contained in this object.
			runObject.setDataNoteList(CollectionUtils.getStream(runObject.getDataNoteList()).filter(dataNote -> sourceIdList.contains(dataNote.getSourceId()) || dataNote.getSourceId() == null).collect(Collectors.toList()));
			runObject.addDataNote(note, DataNoteTypes.UNMATCH, createUserId);
		}
		saveReconciliationRunObjectList(runObjectList);
		return runObjectList;
	}


	protected void validateUnmatching(List<ReconciliationRunObject> runObjectList) {
		for (ReconciliationRunObject runObject : CollectionUtils.getIterable(runObjectList)) {
			if (runObject.isReconciled()) {
				throw new ValidationException("You may only unmatch objects that are not reconciled!  Reconciled items must first be unreconciled.");
			}
			if (!runObject.isManuallyMatched()) {
				throw new ValidationException("You may only unmatch objects that were manually matched!");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public Status reprocessReconciliationRun(int runId) {
		ReconciliationRun run = getReconciliationRun(runId);
		ValidationUtils.assertNotNull(run, "Unable to locate run with id [" + runId + "]");
		return reprocessReconciliationRun(run, getReconciliationRunObjectListByRunIdAndReconcileStatusName(run.getId(), ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()), new HashMap<>(), new Status());
	}


	@Override
	@Transactional
	public Status reprocessReconciliationRun(ReconciliationRun run, List<ReconciliationRunObject> runObjectList, Map<String, ReconciliationRunObject> runObjectMap, Status status) {
		if (run != null) {
			if (runObjectMap == null) {
				runObjectMap = new HashMap<>();
			}
			reprocessReconciliationRunObjectList(runObjectList, runObjectMap);
			for (ReconciliationRunObject runObject : CollectionUtils.getIterable(runObjectMap.values())) {
				try {
					saveReconciliationRunObject(runObject);
				}
				catch (Exception e) {
					LogUtils.error(getClass(), e.getMessage(), e);
					if (e instanceof PersistenceException && e.getCause() != null && e.getCause() instanceof ConstraintViolationException) {
						throw new ValidationException("Failed to save object with identifier [" + runObject.getIdentifier() + "].  An existing object with the same identifier already exists.  You may need to unreconcile this item before reprocessing!");
					}
					throw new ValidationException("Failed to reprocess object list for run with id [" + run.getId() + "]: " + e.getMessage());
				}
			}
			Long[] runObjectIds = CollectionUtils.getStream(runObjectMap.values()).map(ReconciliationRunObject::getId).toArray(Long[]::new);
			run = getReconciliationReconcileService().reconcileReconciliationRun(run, runObjectIds);
			status.addMessage(run.getRunDetails().getMessage());
		}
		return status;
	}


	private void reprocessReconciliationRunObjectList(List<ReconciliationRunObject> runObjectList, Map<String, ReconciliationRunObject> runObjectMap) {
		if (CollectionUtils.isEmpty(runObjectList)) {
			return;
		}
		Map<Long, List<ReconciliationEventObject<?>>> eventObjectListMap = getReconciliationEventObjectListMap(runObjectList);
		for (ReconciliationRunObject runObject : CollectionUtils.getIterable(runObjectList)) {
			if (!runObject.isNewBean()) {
				List<ReconciliationEventObject<?>> eventObjectList = eventObjectListMap.get(runObject.getId());
				if (!CollectionUtils.isEmpty(eventObjectList)) {
					StringBuilder errorMessage = new StringBuilder();
					errorMessage.append("You may not reprocess an object that has events associated with it.<br /><br />You must first delete the events from reconciliation, and associated objects in external systems prior to reprocessing!");
					errorMessage.append("<br /><br />Events:");
					for (ReconciliationEventObject<?> eventObject : CollectionUtils.getIterable(eventObjectList)) {
						errorMessage.append("<br />");
						if (eventObject instanceof ReconciliationEventObjectJournal) {
							errorMessage.append("[").append(eventObject.getId()).append("] ").append(eventObject.getEventObjectDataType().getName()).append(": ").append(((ReconciliationEventObjectJournal) eventObject).getEventObjectData().getAmount()).append(" - ").append(DateUtils.fromDate(eventObject.getEventObjectData().getEventDate(), DateUtils.DATE_FORMAT_SHORT));
						}
						else if (eventObject instanceof ReconciliationEventObjectAccount) {
							errorMessage.append("[").append(eventObject.getId()).append("] ").append(eventObject.getEventObjectDataType().getName()).append(": ").append(DateUtils.fromDate(eventObject.getEventObjectData().getEventDate(), DateUtils.DATE_FORMAT_SHORT));
						}
						else {
							errorMessage.append("Unknown Event Type!");
						}
					}
					throw new ValidationException(errorMessage.toString());
				}
			}
			reprocessReconciliationRunObject(runObject, runObjectMap);
		}
	}

	private Map<Long, List<ReconciliationEventObject<?>>> getReconciliationEventObjectListMap(List<ReconciliationRunObject> runObjectList) {
		Long[] runObjectIds = CollectionUtils.toArray(CollectionUtils.getStream(runObjectList).filter(runObject -> !runObject.isNewBean()).map(ReconciliationRunObject::getId).collect(Collectors.toList()), Long.class);
		if (runObjectIds != null && runObjectIds.length > 0) {
			ReconciliationEventObjectSearchForm eventObjectSearchForm = new ReconciliationEventObjectSearchForm();
			eventObjectSearchForm.setRunObjectIds(runObjectIds);
			return BeanUtils.getBeansMap(getReconciliationEventObjectService().getReconciliationEventObjectList(eventObjectSearchForm), event -> event.getRunObject().getId());
		}
		return new HashMap<>();
	}

	private void reprocessReconciliationRunObject(ReconciliationRunObject runObject, Map<String, ReconciliationRunObject> runObjectMap) {
		try {
			ReconciliationRunObject updatedRunObject = null;
			//Clone the list to avoid concurrent modification if an item is matched and the dataObject is added to the current runObjects data object list.
			List<DataObject> dataObjectList = new ArrayList<>(runObject.getDataHolder().getDataObjects());
			for (DataObject dataObject : CollectionUtils.getIterable(dataObjectList)) {
				ReconciliationFileDefinition fileDefinition = getReconciliationFileDefinitionService().getReconciliationFileDefinition(dataObject.getFileDefinitionId());
				// need to rerun the Object Processing for each source data
				for (Map<String, Object> rowData : CollectionUtils.getIterable(dataObject.getRowData())) {
					// rerun the import logic where it evaluates the definition field mappings/rules and creates new or merges ReconciliationObjects
					updatedRunObject = getReconciliationService().reprocessReconciliationRunObjectRowData(fileDefinition, runObject, rowData, runObjectMap);
					if (updatedRunObject != null) {
						if (runObject.getDataHolder().getNotes() != null) {
							updatedRunObject.getDataHolder().setNotes(CoreCollectionUtils.clone(runObject.getDataHolder().getNotes()));
						}
					}
				}
			}
			//Store existing errors and set to null for comparison of existing run object to updated.
			List<DataError> errorList = runObject.getErrorList() != null ? new ArrayList<>(runObject.getErrorList()) : null;
			runObject.getDataHolder().setErrors(null);
			if (updatedRunObject != null && !CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualPropertiesWithSetters(runObject, updatedRunObject, false, "id", "breakDays"))) {
				if (!runObject.isNewBean()) {
					deleteReconciliationRunObject(runObject.getId());
				}
				runObjectMap.put(updatedRunObject.getIdentifier(), updatedRunObject);
			}
			else {
				//If the object was not updated then just reset the error list and put the original object reference in the map so we don't create a new one/duplicate
				runObject.getDataHolder().setErrors(errorList);
				runObjectMap.put(runObject.getIdentifier(), runObject);
			}
		}
		catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getCauseException(e, DaoException.class);
			if (rootCause == null) {
				rootCause = e;
			}
			String errorMessage = "Failed to reprocess ReconciliationRunObject [" + (runObject != null ? runObject.getId() : null) + "] from Run [" + runObject.getReconciliationRun().getLabel() + "]: " + rootCause.getMessage();
			LogUtils.error(getClass(), errorMessage, rootCause);
			throw new ValidationException(errorMessage, rootCause);
		}
	}


	@Override
	public Map<String, Integer> getReconciliationBreakDaysMap(int definitionId, Date runDate, boolean singleton) {
		String sql = "SELECT NaturalKeyIdentifier, BreakDays FROM ReconciliationRunObject WHERE ReconciliationRunID IN (SELECT ReconciliationRunID FROM ReconciliationRun WHERE ";
		if (!singleton) {
			sql += "RunDate = (SELECT MAX(RunDate) FROM ReconciliationRun WHERE ReconciliationDefinitionID IN (" + definitionId + ") AND RunDate < '" + DateUtils.fromDate(runDate, DateUtils.DATE_FORMAT_SQL) + "') AND ";
		}
		sql += "ReconciliationDefinitionID IN (" + definitionId + "))";
		return getSqlHandler().executeSelect(sql, new ReconciliationBreakDaysResultSetExtractor());
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationDefinitionService getReconciliationDefinitionService() {
		return this.reconciliationDefinitionService;
	}


	public void setReconciliationDefinitionService(ReconciliationDefinitionService reconciliationDefinitionService) {
		this.reconciliationDefinitionService = reconciliationDefinitionService;
	}


	public ReconciliationFileDefinitionService getReconciliationFileDefinitionService() {
		return this.reconciliationFileDefinitionService;
	}


	public void setReconciliationFileDefinitionService(ReconciliationFileDefinitionService reconciliationFileDefinitionService) {
		this.reconciliationFileDefinitionService = reconciliationFileDefinitionService;
	}


	public ReconciliationEventObjectService getReconciliationEventObjectService() {
		return this.reconciliationEventObjectService;
	}


	public void setReconciliationEventObjectService(ReconciliationEventObjectService reconciliationEventObjectService) {
		this.reconciliationEventObjectService = reconciliationEventObjectService;
	}


	public ReconciliationExclusionAssignmentService getReconciliationExclusionAssignmentService() {
		return this.reconciliationExclusionAssignmentService;
	}


	public void setReconciliationExclusionAssignmentService(ReconciliationExclusionAssignmentService reconciliationExclusionAssignmentService) {
		this.reconciliationExclusionAssignmentService = reconciliationExclusionAssignmentService;
	}


	public ReconciliationReconcileService getReconciliationReconcileService() {
		return this.reconciliationReconcileService;
	}


	public void setReconciliationReconcileService(ReconciliationReconcileService reconciliationReconcileService) {
		this.reconciliationReconcileService = reconciliationReconcileService;
	}


	public ReconciliationService getReconciliationService() {
		return this.reconciliationService;
	}


	public void setReconciliationService(ReconciliationService reconciliationService) {
		this.reconciliationService = reconciliationService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public AdvancedUpdatableDAO<ReconciliationRunObject, Criteria> getReconciliationRunObjectDAO() {
		return this.reconciliationRunObjectDAO;
	}


	public void setReconciliationRunObjectDAO(AdvancedUpdatableDAO<ReconciliationRunObject, Criteria> reconciliationRunObjectDAO) {
		this.reconciliationRunObjectDAO = reconciliationRunObjectDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationRunOrphanObject, Criteria> getReconciliationRunOrphanObjectDAO() {
		return this.reconciliationRunOrphanObjectDAO;
	}


	public void setReconciliationRunOrphanObjectDAO(AdvancedUpdatableDAO<ReconciliationRunOrphanObject, Criteria> reconciliationRunOrphanObjectDAO) {
		this.reconciliationRunOrphanObjectDAO = reconciliationRunOrphanObjectDAO;
	}


	public ReconciliationStatusService getReconciliationStatusService() {
		return this.reconciliationStatusService;
	}


	public void setReconciliationStatusService(ReconciliationStatusService reconciliationStatusService) {
		this.reconciliationStatusService = reconciliationStatusService;
	}


	public ReconciliationTypeService getReconciliationTypeService() {
		return this.reconciliationTypeService;
	}


	public void setReconciliationTypeService(ReconciliationTypeService reconciliationTypeService) {
		this.reconciliationTypeService = reconciliationTypeService;
	}


	public AdvancedUpdatableDAO<ReconciliationRun, Criteria> getReconciliationRunDAO() {
		return this.reconciliationRunDAO;
	}


	public void setReconciliationRunDAO(AdvancedUpdatableDAO<ReconciliationRun, Criteria> reconciliationRunDAO) {
		this.reconciliationRunDAO = reconciliationRunDAO;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}
}
