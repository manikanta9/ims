package com.clifton.reconciliation.run.notification;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableGenerator;
import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverter;
import com.clifton.core.dataaccess.datatable.bean.DataTableConverterConfiguration;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author stevenf
 */
public class ReconciliationUnapprovedRunDataTableGenerator implements DataTableGenerator {

	private static final String COLUMNS = "ID:id|" + //
			"DefinitionName:reconciliationDefinition.name|" + //
			"RunDate:runDate|";
	private Integer businessDaysBack;
	private ApplicationContextService applicationContextService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private ReconciliationRunService reconciliationRunService;


	@Override
	public DataTable getDataTable() {
		//The list of runs that will be returned for notification
		List<ReconciliationRun> notificationList = new ArrayList<>();

		ReconciliationRunSearchForm searchForm = new ReconciliationRunSearchForm();
		//Exclude cumulative runs - they are never approved.
		searchForm.setCumulativeReconciliation(false);
		searchForm.setApprovedByUserIdIsNull(true);
		List<ReconciliationRun> runList = getReconciliationRunService().getReconciliationRunList(searchForm);
		for (ReconciliationRun run : CollectionUtils.getIterable(runList)) {
			ReconciliationDefinition definition = run.getReconciliationDefinition();
			if (getBusinessDaysBack() != null) {
				Date notificationDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forToday(definition.getCalendar().getId()), -getBusinessDaysBack());
				if (DateUtils.isDateBeforeOrEqual(run.getRunDate(), notificationDate, false)) {
					notificationList.add(run);
				}
			}
			else {
				//If business days back is not specified, then just add all unapproved runs to the list
				notificationList.add(run);
			}
		}
		BeanCollectionToDataTableConverter<ReconciliationRun> converter = new BeanCollectionToDataTableConverter<>(DataTableConverterConfiguration.ofDtoClassUsingDataColumnString(getApplicationContextService(), ReconciliationRun.class, COLUMNS));
		return converter.convert(notificationList);
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getBusinessDaysBack() {
		return this.businessDaysBack;
	}


	public void setBusinessDaysBack(Integer businessDaysBack) {
		this.businessDaysBack = businessDaysBack;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}
}
