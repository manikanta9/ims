package com.clifton.reconciliation.run.status;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.reconciliation.run.status.search.ReconciliationMatchStatusSearchForm;
import com.clifton.reconciliation.run.status.search.ReconciliationReconcileStatusSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>ReconciliationStatusServiceImpl</code> provides a basic implementation of the
 * {@link ReconciliationStatusService} interface
 *
 * @author StevenF on 11/23/2016.
 */
@Service
public class ReconciliationStatusServiceImpl implements ReconciliationStatusService {

	private AdvancedUpdatableDAO<ReconciliationMatchStatus, Criteria> reconciliationMatchStatusDAO;
	private AdvancedUpdatableDAO<ReconciliationReconcileStatus, Criteria> reconciliationReconcileStatusDAO;

	private DaoNamedEntityCache<ReconciliationMatchStatus> reconciliationMatchStatusCache;
	private DaoNamedEntityCache<ReconciliationReconcileStatus> reconciliationReconcileStatusCache;

	////////////////////////////////////////////////////////////////////////////
	////////       Reconciliation Match Status Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationMatchStatus getReconciliationMatchStatus(short id) {
		return getReconciliationMatchStatusDAO().findByPrimaryKey(id);
	}


	@Override
	public ReconciliationMatchStatus getReconciliationMatchStatusByName(String name) {
		return getReconciliationMatchStatusCache().getBeanForKeyValueStrict(getReconciliationMatchStatusDAO(), name);
	}


	@Override
	public ReconciliationMatchStatus getReconciliationMatchStatusByStatusType(ReconciliationMatchStatusTypes statusType) {
		return getReconciliationMatchStatusByName(statusType.name());
	}


	@Override
	public List<ReconciliationMatchStatus> getReconciliationMatchStatusList(ReconciliationMatchStatusSearchForm searchForm) {
		return getReconciliationMatchStatusDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReconciliationMatchStatus saveReconciliationMatchStatus(ReconciliationMatchStatus reconciliationMatchStatus) {
		return getReconciliationMatchStatusDAO().save(reconciliationMatchStatus);
	}


	@Override
	public void deleteReconciliationMatchStatus(short id) {
		getReconciliationMatchStatusDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////     Reconciliation Reconcile Status Business Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationReconcileStatus getReconciliationReconcileStatus(short id) {
		return getReconciliationReconcileStatusDAO().findByPrimaryKey(id);
	}


	@Override
	public ReconciliationReconcileStatus getReconciliationReconcileStatusByName(String name) {
		return getReconciliationReconcileStatusCache().getBeanForKeyValueStrict(getReconciliationReconcileStatusDAO(), name);
	}


	@Override
	public ReconciliationReconcileStatus getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes statusType) {
		return getReconciliationReconcileStatusByName(statusType.name());
	}


	@Override
	public List<ReconciliationReconcileStatus> getReconciliationReconcileStatusList(ReconciliationReconcileStatusSearchForm searchForm) {
		return getReconciliationReconcileStatusDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReconciliationReconcileStatus saveReconciliationReconcileStatus(ReconciliationReconcileStatus reconciliationReconcileStatus) {
		return getReconciliationReconcileStatusDAO().save(reconciliationReconcileStatus);
	}


	@Override
	public void deleteReconciliationReconcileStatus(short id) {
		getReconciliationReconcileStatusDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ReconciliationMatchStatus, Criteria> getReconciliationMatchStatusDAO() {
		return this.reconciliationMatchStatusDAO;
	}


	public void setReconciliationMatchStatusDAO(AdvancedUpdatableDAO<ReconciliationMatchStatus, Criteria> reconciliationMatchStatusDAO) {
		this.reconciliationMatchStatusDAO = reconciliationMatchStatusDAO;
	}


	public AdvancedUpdatableDAO<ReconciliationReconcileStatus, Criteria> getReconciliationReconcileStatusDAO() {
		return this.reconciliationReconcileStatusDAO;
	}


	public void setReconciliationReconcileStatusDAO(AdvancedUpdatableDAO<ReconciliationReconcileStatus, Criteria> reconciliationReconcileStatusDAO) {
		this.reconciliationReconcileStatusDAO = reconciliationReconcileStatusDAO;
	}


	public DaoNamedEntityCache<ReconciliationMatchStatus> getReconciliationMatchStatusCache() {
		return this.reconciliationMatchStatusCache;
	}


	public void setReconciliationMatchStatusCache(DaoNamedEntityCache<ReconciliationMatchStatus> reconciliationMatchStatusCache) {
		this.reconciliationMatchStatusCache = reconciliationMatchStatusCache;
	}


	public DaoNamedEntityCache<ReconciliationReconcileStatus> getReconciliationReconcileStatusCache() {
		return this.reconciliationReconcileStatusCache;
	}


	public void setReconciliationReconcileStatusCache(DaoNamedEntityCache<ReconciliationReconcileStatus> reconciliationReconcileStatusCache) {
		this.reconciliationReconcileStatusCache = reconciliationReconcileStatusCache;
	}
}
