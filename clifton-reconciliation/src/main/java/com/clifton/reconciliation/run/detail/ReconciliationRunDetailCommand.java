package com.clifton.reconciliation.run.detail;

import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluator;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccountAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityAlias;
import com.clifton.reconciliation.json.jackson.strategy.ReconciliationRunObjectStrategy;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObjectDetail;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.search.ReconciliationRunObjectSearchForm;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatus;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author stevenf
 */
public class ReconciliationRunDetailCommand {

	private ReconciliationRunService reconciliationRunService;

	/**
	 * Used in RunObjectDetailWindow in UI - if set, then RunDate is retrieved by joining to ReconciliationRun table
	 * in order to populate the correct runDate for historical objects - otherwise the join is only done if sorting/filtering on fields from that table.
	 */
	private boolean showHistory;
	private boolean sideBySideView;

	private ReconciliationRun run;
	private ReconciliationSource primarySource;
	private ReconciliationSource secondarySource;
	private ReconciliationRunObjectSearchForm searchForm;

	private Map<String, ReconciliationInvestmentSecurityAlias> securityAliasMap;
	private Map<String, ReconciliationInvestmentAccountAlias> accountAliasMap;
	private Map<String, ReconciliationEvaluator> evaluatorMap;

	private Map<String, DataTypes> fieldDataTypeMap;
	private Map<Long, BigDecimal> adjustmentAmountMap;

	private Map<Short, ReconciliationMatchStatus> matchStatusMap;
	private Map<Short, ReconciliationReconcileStatus> reconcileStatusMap;

	private Map<String, String> modifierTextMap = new HashMap<>();

	private List<ReconciliationRunObjectDetail> runObjectDetailList;

	private JsonHandler<?> jsonHandler = new JacksonHandlerImpl(new ReconciliationRunObjectStrategy());

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public boolean isShowHistory() {
		return this.showHistory;
	}


	public void setShowHistory(boolean showHistory) {
		this.showHistory = showHistory;
	}


	public boolean isSideBySideView() {
		return this.sideBySideView;
	}


	public void setSideBySideView(boolean sideBySideView) {
		this.sideBySideView = sideBySideView;
	}


	public ReconciliationRun getRun() {
		return this.run;
	}


	public void setRun(ReconciliationRun run) {
		this.run = run;
	}


	public ReconciliationSource getPrimarySource() {
		return this.primarySource;
	}


	public void setPrimarySource(ReconciliationSource primarySource) {
		this.primarySource = primarySource;
	}


	public ReconciliationSource getSecondarySource() {
		return this.secondarySource;
	}


	public void setSecondarySource(ReconciliationSource secondarySource) {
		this.secondarySource = secondarySource;
	}


	public ReconciliationRunObjectSearchForm getSearchForm() {
		return this.searchForm;
	}


	public void setSearchForm(ReconciliationRunObjectSearchForm searchForm) {
		this.searchForm = searchForm;
	}


	public Map<String, ReconciliationInvestmentSecurityAlias> getSecurityAliasMap() {
		return this.securityAliasMap;
	}


	public void setSecurityAliasMap(Map<String, ReconciliationInvestmentSecurityAlias> securityAliasMap) {
		this.securityAliasMap = securityAliasMap;
	}


	public Map<String, ReconciliationInvestmentAccountAlias> getAccountAliasMap() {
		return this.accountAliasMap;
	}


	public void setAccountAliasMap(Map<String, ReconciliationInvestmentAccountAlias> accountAliasMap) {
		this.accountAliasMap = accountAliasMap;
	}


	public Map<String, ReconciliationEvaluator> getEvaluatorMap() {
		return this.evaluatorMap;
	}


	public void setEvaluatorMap(Map<String, ReconciliationEvaluator> evaluatorMap) {
		this.evaluatorMap = evaluatorMap;
	}


	public Map<String, DataTypes> getFieldDataTypeMap() {
		return this.fieldDataTypeMap;
	}


	public void setFieldDataTypeMap(Map<String, DataTypes> fieldDataTypeMap) {
		this.fieldDataTypeMap = fieldDataTypeMap;
	}


	public Map<Long, BigDecimal> getAdjustmentAmountMap() {
		return this.adjustmentAmountMap;
	}


	public void setAdjustmentAmountMap(Map<Long, BigDecimal> adjustmentAmountMap) {
		this.adjustmentAmountMap = adjustmentAmountMap;
	}


	public Map<Short, ReconciliationMatchStatus> getMatchStatusMap() {
		return this.matchStatusMap;
	}


	public void setMatchStatusMap(Map<Short, ReconciliationMatchStatus> matchStatusMap) {
		this.matchStatusMap = matchStatusMap;
	}


	public Map<Short, ReconciliationReconcileStatus> getReconcileStatusMap() {
		return this.reconcileStatusMap;
	}


	public void setReconcileStatusMap(Map<Short, ReconciliationReconcileStatus> reconcileStatusMap) {
		this.reconcileStatusMap = reconcileStatusMap;
	}


	public Map<String, String> getModifierTextMap() {
		return this.modifierTextMap;
	}


	public void setModifierTextMap(Map<String, String> modifierTextMap) {
		this.modifierTextMap = modifierTextMap;
	}


	public List<ReconciliationRunObjectDetail> getRunObjectDetailList() {
		return this.runObjectDetailList;
	}


	public void setRunObjectDetailList(List<ReconciliationRunObjectDetail> runObjectDetailList) {
		this.runObjectDetailList = runObjectDetailList;
	}


	public JsonHandler<?> getJsonHandler() {
		return this.jsonHandler;
	}


	public void setJsonHandler(JsonHandler<?> jsonHandler) {
		this.jsonHandler = jsonHandler;
	}
}
