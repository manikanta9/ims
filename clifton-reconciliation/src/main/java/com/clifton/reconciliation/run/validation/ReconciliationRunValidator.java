package com.clifton.reconciliation.run.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Component;


/**
 * The <code>ReconciliationDefinitionFieldValidator</code> validates ReconciliationDefinitionField
 *
 * @author stevenf
 */
@Component
public class ReconciliationRunValidator extends SelfRegisteringDaoValidator<ReconciliationRun> {

	private SecurityUserService securityUserService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(final ReconciliationRun run, DaoEventTypes config) throws ValidationException {
		ReconciliationRun originalRun = getOriginalBean(run);
		if (originalRun != null) {
			if (originalRun.isArchived()) {
				throw new ValidationException("Changes cannot be made to a archived run!");
			}
			if (originalRun.getApprovedByUser() == null && run.getApprovedByUser() != null) {
				if (run.getReconciliationDefinition().getType().isCumulativeReconciliation()) {
					throw new ValidationException("You may not approve cumulative runs!");
				}
			}
			if (originalRun.getApprovedByUser() != null) {
				if (!CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualProperties(run, originalRun, false, "approvedByUser", "note"))) {
					throw new ValidationException("You [" + getSecurityUserService().getSecurityUserCurrent().getDisplayName() + "] may not make changes to a run [" + run.getLabel() + "] that has been approved!");
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
