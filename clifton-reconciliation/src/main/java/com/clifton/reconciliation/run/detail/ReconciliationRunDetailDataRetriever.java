package com.clifton.reconciliation.run.detail;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.db.PropertyDataTypeConverter;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.JsonCompressedTextOrderBy;
import com.clifton.core.dataaccess.search.hibernate.expression.JsonCompressedTextExpression;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlParameterValue;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluator;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluatorService;
import com.clifton.reconciliation.definition.evaluator.search.ReconciliationEvaluatorSearchForm;
import com.clifton.reconciliation.event.object.ReconciliationEventObject;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectJournalData;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccountAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentAccountAliasSearchForm;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentSecurityAliasSearchForm;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.ReconciliationRunObjectDetail;
import com.clifton.reconciliation.run.search.ReconciliationRunObjectSearchForm;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlParameter;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * The <code>ReconciliationRunDetailDataRetriever</code> is a custom class using SQL directly to retrieve, sort, and filter {@ReconciliationRunObject}s
 * <p>
 * Supports searchRestrictions, searchForm fields, and compressed text.
 * Later these functions could likely be incorporated into the base search forms (e.g. allow direct SQL generation in specific situations?)
 *
 * @author stevenf
 */
public class ReconciliationRunDetailDataRetriever {

	private static final String ADJUSTMENT_MAP_QUERY = "SELECT ReconciliationRunObjectID, ParentRunObjectID, EventObjectData, reodt.IsNegativeAdjustment " +
			"FROM ReconciliationEventObject reo INNER JOIN ReconciliationEventObjectDataType reodt ON reodt.ReconciliationEventObjectDataTypeID = reo.ReconciliationEventObjectDataTypeID " +
			"WHERE (ReconciliationRunObjectID IN (SELECT ReconciliationRunObjectID FROM ReconciliationRunObject WHERE ReconciliationRunID = ?)" +
			"OR ParentRunObjectID IN (SELECT ReconciliationRunObjectID FROM ReconciliationRunObject WHERE ReconciliationRunID = ?))" +
			"AND reo.ReconciliationEventObjectDataTypeID IN (SELECT ReconciliationEventObjectDataTypeID FROM ReconciliationEventObjectDataType WHERE EventObjectCategory IN ('SIMPLE_JOURNAL','ADJUSTING_JOURNAL'))" +
			"AND Status = 'COMPLETE'";

	private static final String RECONCILIATION_DEFINITION_JOIN = "INNER JOIN ReconciliationDefinition rd ON rd.ReconciliationDefinitionID = rr.ReconciliationRunID";
	private static final String RECONCILIATION_DEFINITION_GROUP_JOIN = "INNER JOIN ReconciliationDefinitionGroup rdg ON rdg.ReconciliationDefinitionGroupID = rd.ReconciliationDefinitionGroupID";
	private static final String RECONCILIATION_INVESTMENT_ACCOUNT_JOIN = "INNER JOIN ReconciliationInvestmentAccount ria ON ria.ReconciliationInvestmentAccountID = rro.ReconciliationInvestmentAccountID";
	private static final String RECONCILIATION_INVESTMENT_SECURITY_JOIN = "INNER JOIN ReconciliationInvestmentSecurity ris ON ris.ReconciliationInvestmentSecurityID = rro.ReconciliationInvestmentSecurityID";
	private static final String RECONCILIATION_MATCH_STATUS_JOIN = "INNER JOIN ReconciliationMatchStatus rms ON rms.ReconciliationMatchStatusID = rro.ReconciliationMatchStatusID";
	private static final String RECONCILIATION_RECONCILE_STATUS_JOIN = "INNER JOIN ReconciliationReconcileStatus rrs ON rrs.ReconciliationReconcileStatusID = rro.ReconciliationReconcileStatusID";
	private static final String RECONCILIATION_RUN_JOIN = "INNER JOIN ReconciliationRun rr ON rr.ReconciliationRunID = rro.ReconciliationRunID";
	private static final String RECONCILIATION_TYPE_JOIN = "INNER JOIN ReconciliationType rd ON rd.ReconciliationTypeID = rt.ReconciliationTypeID";

	private static final List<ComparisonConditions> NULL_COMPARISON_CONDITIONS = Arrays.asList(ComparisonConditions.IS_NULL, ComparisonConditions.IS_NOT_NULL);


	public static Map<Long, BigDecimal> getAdjustmentAmountMap(List<ReconciliationEventObject<?>> eventObjectList) {
		Map<Long, BigDecimal> adjustmentAmountMap = new HashMap<>();
		for (ReconciliationEventObject<?> eventObject : CollectionUtils.getIterable(eventObjectList)) {
			if (eventObject.getEventObjectData() instanceof ReconciliationEventObjectJournalData) {
				ReconciliationEventObjectJournalData eventObjectJournalData = (ReconciliationEventObjectJournalData) eventObject.getEventObjectData();
				BigDecimal adjustmentAmount = eventObjectJournalData.getAmount();
				updateAdjustmentAmountMap(eventObject.getRunObject().getId(), adjustmentAmount, eventObject.getEventObjectDataType().isNegativeAdjustment(), adjustmentAmountMap);
				if (eventObject.getParentRunObject() != null) {
					updateAdjustmentAmountMap(eventObject.getParentRunObject().getId(), adjustmentAmount, eventObject.getEventObjectDataType().isNegativeAdjustment(), adjustmentAmountMap);
				}
			}
		}
		return adjustmentAmountMap;
	}


	private static void updateAdjustmentAmountMap(long id, BigDecimal amount, boolean negativeAdjustment, Map<Long, BigDecimal> adjustmentAmountMap) {
		BigDecimal totalAdjustmentAmount = adjustmentAmountMap.get(id);
		if (totalAdjustmentAmount == null) {
			totalAdjustmentAmount = BigDecimal.ZERO;
		}
		BigDecimal adjustmentAmount = amount;

		if (negativeAdjustment) {
			adjustmentAmount = MathUtils.negate(adjustmentAmount);
		}
		totalAdjustmentAmount = MathUtils.add(totalAdjustmentAmount, adjustmentAmount);
		adjustmentAmountMap.put(id, totalAdjustmentAmount);
	}


	public static Map<String, ReconciliationInvestmentSecurityAlias> getSecurityAliasMap(ReconciliationRun run, ReconciliationInvestmentService reconciliationInvestmentService) {
		ReconciliationInvestmentSecurityAliasSearchForm searchForm = new ReconciliationInvestmentSecurityAliasSearchForm();
		searchForm.setSourceId(run.getReconciliationDefinition().getSecondarySource().getId());
		searchForm.setActiveOnDate(run.getRunDate());
		return CollectionUtils.getStream(reconciliationInvestmentService.getReconciliationInvestmentSecurityAliasList(searchForm)).collect(Collectors.toMap(securityAlias -> (securityAlias.getReconciliationSource().getId() + "_" + securityAlias.getInvestmentSecurity().getId()), Function.identity(), (securityAlias1, securityAlias2) -> securityAlias1));
	}


	public static Map<String, ReconciliationInvestmentAccountAlias> getAccountAliasMap(ReconciliationRun run, ReconciliationInvestmentService reconciliationInvestmentService) {
		ReconciliationInvestmentAccountAliasSearchForm searchForm = new ReconciliationInvestmentAccountAliasSearchForm();
		searchForm.setSourceId(run.getReconciliationDefinition().getSecondarySource().getId());
		searchForm.setActiveOnDate(run.getRunDate());
		return CollectionUtils.getStream(reconciliationInvestmentService.getReconciliationInvestmentAccountAliasList(searchForm)).collect(Collectors.toMap(accountAlias -> (accountAlias.getReconciliationSource().getId() + "_" + accountAlias.getInvestmentAccount().getId()), Function.identity(), (accountAlias1, accountAlias2) -> accountAlias1));
	}


	public static Map<String, ReconciliationEvaluator> getEvaluatorMap(ReconciliationRun run, ReconciliationEvaluatorService reconciliationEvaluatorService) {
		ReconciliationEvaluatorSearchForm searchForm = new ReconciliationEvaluatorSearchForm();
		searchForm.setDefinitionId(run.getReconciliationDefinition().getId());
		searchForm.setActiveOnDate(run.getRunDate());
		return CollectionUtils.getStream(reconciliationEvaluatorService.getReconciliationEvaluatorList(searchForm)).collect(Collectors.toMap(evaluator -> evaluator.getDefinitionField().getReconciliationDefinition().getId() + "_" + BeanUtils.getPropertyValue(evaluator.getReconciliationInvestmentAccount(), "id") + "_" + BeanUtils.getPropertyValue(evaluator.getReconciliationInvestmentSecurity(), "id"), Function.identity(), (evaluator1, evaluator2) -> evaluator1));
	}


	public static List<ReconciliationRunObjectDetail> getReconciliationRunObjectDetailList(ReconciliationRunDetailCommand command, SqlHandler sqlHandler) {
		Set<String> innerJoins = new HashSet<>();
		Map<String, DataTypes> fieldDataTypeMap = command.getFieldDataTypeMap();
		if (command.isShowHistory()) {
			innerJoins.add(RECONCILIATION_RUN_JOIN);
		}
		List<SqlParameterValue> parameterValueList = new ArrayList<>();
		SqlSelectCommand sqlSelectCommand = new SqlSelectCommand();
		sqlSelectCommand.setWhereClause(getWhereClause(command, fieldDataTypeMap, parameterValueList, innerJoins));
		sqlSelectCommand.setOrderByClause(getOrderByClause(command, fieldDataTypeMap, innerJoins));
		sqlSelectCommand.setFromClause(getFromClause(innerJoins));

		if (command.isShowHistory()) {
			sqlSelectCommand.setSelectClause("rro.*, rr.RunDate");
		}
		else {
			sqlSelectCommand.setSelectClause("rro.*");
		}

		Object[] parameterValues = BeanUtils.getPropertyValues(parameterValueList, SqlParameterValue::getValue);
		PreparedStatementCreatorFactory statementFactory = new PreparedStatementCreatorFactory(sqlSelectCommand.getSql());
		if (!CollectionUtils.isEmpty(parameterValueList)) {
			for (SqlParameterValue parameterValue : parameterValueList) {
				statementFactory.addParameter(new SqlParameter(parameterValue.getSqlType()));
			}
		}
		PreparedStatementCreator statementCreator = statementFactory.newPreparedStatementCreator(parameterValues);
		return sqlHandler.executeSelect(statementCreator, getRunObjectDetailResultSetExtractor(command));
	}


	private static String getFromClause(Set<String> innerJoins) {
		StringBuilder fromClause = new StringBuilder();
		fromClause.append("ReconciliationRunObject rro");
		for (String innerJoin : CollectionUtils.getIterable(innerJoins)) {
			fromClause.append(" ").append(innerJoin);
		}
		return fromClause.toString();
	}


	private static String getWhereClause(ReconciliationRunDetailCommand command, Map<String, DataTypes> fieldDataTypeMap, List<SqlParameterValue> parameterValueList, Set<String> innerJoins) {
		StringBuilder whereClause = new StringBuilder(64);
		ReconciliationRunObjectSearchForm searchForm = command.getSearchForm();
		if (command.isShowHistory()) {
			ReconciliationRunObject runObject = command.getReconciliationRunService().getReconciliationRunObject(searchForm.getId());
			searchForm.removeSearchRestrictionAndBeanField("showHistory");
			searchForm.removeSearchRestrictionAndBeanField("id");
			searchForm.removeSearchRestrictionAndBeanField("runId");
			searchForm.setIdentifier(runObject.getIdentifier());
		}
		configureSearchFormWhereClause(searchForm, whereClause, parameterValueList, innerJoins);
		for (SearchRestriction searchRestriction : CollectionUtils.getIterable(searchForm.getRestrictionList())) {
			Object searchRestrictionValue = searchRestriction.getValue();
			//If it's in the dataTypeMap then it's a reconciliationDefinitionField and compressed in the dataHolder object.
			if (fieldDataTypeMap.containsKey(searchRestriction.getField())) {
				if (searchRestrictionValue instanceof Collection) {
					searchRestrictionValue = ((Collection<?>) searchRestrictionValue).toArray()[0];
				}
				whereClause.append(whereClause.length() > 0 ? " AND " : "");
				whereClause.append(new JsonCompressedTextExpression("dataHolder", String.format("$.dataObjects[0].data.\"%s\"", searchRestriction.getField().replaceAll("'", "''")), searchRestriction.getComparison().getComparisonExpression(), searchRestrictionValue, getSearchFieldDataTypeClass(fieldDataTypeMap.get(searchRestriction.getField()))));
				if (!NULL_COMPARISON_CONDITIONS.contains(searchRestriction.getComparison())) {
					parameterValueList.add(SqlParameterValue.ofType(fieldDataTypeMap.get(searchRestriction.getField()).getTypeName(), searchRestrictionValue));
				}
			}
			else {
				//Must be a standard search form field
				String searchFieldWhereClause = getSearchFieldWhereClause(searchRestriction, " AND ", innerJoins);
				if (searchFieldWhereClause != null) {
					whereClause.append(whereClause.length() > 0 ? " AND " : "");
					whereClause.append(searchFieldWhereClause);
					if (!NULL_COMPARISON_CONDITIONS.contains(searchRestriction.getComparison())) {
						parameterValueList.add(SqlParameterValue.ofType(getSearchFieldDataType(searchRestriction.getField()), searchRestrictionValue));
					}
				}
			}
		}
		return (whereClause.length() > 0 ? whereClause.toString() : "");
	}


	private static void configureSearchFormWhereClause(ReconciliationRunObjectSearchForm searchForm, StringBuilder whereClause, List<SqlParameterValue> parameterValueList, Set<String> innerJoins) {
		Field[] fieldList = ClassUtils.getClassFields(searchForm.getClass(), false, false);
		for (Field field : fieldList) {
			String fieldName = field.getName();
			Object fieldValue = BeanUtils.getPropertyValue(searchForm, fieldName);
			if (fieldValue != null) {
				ComparisonConditions comparisonConditions = getComparisonConditions(field);
				if (includeComparisonCondition(comparisonConditions, fieldValue)) {
					SearchFieldCustomTypes searchFieldCustomTypes = getSearchFieldCustomTypes(field);
					String operator = " AND ";
					// only support OR for now
					if (searchFieldCustomTypes == SearchFieldCustomTypes.OR) {
						operator = " OR ";
					}
					String searchFieldWhereClause = getSearchFieldWhereClause(fieldName, comparisonConditions.getComparisonExpression(), operator, innerJoins);
					if (searchFieldWhereClause != null) {
						if (whereClause.length() > 0) {
							whereClause.append(" AND ");
						}
						whereClause.append(searchFieldWhereClause);
						if (!NULL_COMPARISON_CONDITIONS.contains(comparisonConditions)) {
							parameterValueList.add(SqlParameterValue.ofType(getSearchFieldDataType(fieldName), fieldValue));
						}
					}
				}
			}
		}
	}


	/**
	 * Null clauses are only included when the search form value is true.
	 */
	private static boolean includeComparisonCondition(ComparisonConditions comparisonConditions, Object fieldValue) {
		if (NULL_COMPARISON_CONDITIONS.contains(comparisonConditions) && fieldValue instanceof Boolean) {
			return (Boolean) fieldValue;
		}
		return true;
	}


	private static ComparisonConditions getComparisonConditions(Field field) {
		//Default is normally LIKE, it depends on dataType, e.g. should be = for numeric; Using default of = here...
		ComparisonConditions[] comparisonConditionList = new ComparisonConditions[]{ComparisonConditions.EQUALS};
		SearchField searchField = AnnotationUtils.getAnnotation(field, SearchField.class);
		if (searchField != null) {
			comparisonConditionList = searchField.comparisonConditions();
			ValidationUtils.assertFalse(comparisonConditionList.length > 1, "Multiple conditions are not currently supported!");
			if (comparisonConditionList.length == 0) {
				comparisonConditionList = new ComparisonConditions[]{ComparisonConditions.EQUALS};
			}
		}
		return comparisonConditionList[0];
	}


	private static SearchFieldCustomTypes getSearchFieldCustomTypes(Field field) {
		SearchField searchField = AnnotationUtils.getAnnotation(field, SearchField.class);
		if (searchField != null) {
			SearchFieldCustomTypes searchFieldCustomTypes = searchField.searchFieldCustomType();
			return searchFieldCustomTypes;
		}
		return null;
	}


	private static String getSearchFieldWhereClause(SearchRestriction searchRestriction, String operator, Set<String> innerJoins) {
		return getSearchFieldWhereClause(searchRestriction.getField(), searchRestriction.getComparison().getComparisonExpression(), operator, innerJoins);
	}


	private static String getSearchFieldWhereClause(String fieldName, String comparisonExpression, String operator, Set<String> innerJoins) {
		List<String> databaseFieldNameList = getDatabaseFieldNameList(fieldName, innerJoins);
		if (!CollectionUtils.isEmpty(databaseFieldNameList)) {
			String clause = databaseFieldNameList.stream()
					.map(field -> field + " " + comparisonExpression + (!StringUtils.contains(comparisonExpression, "LIKE") && (!StringUtils.contains(comparisonExpression, "NULL")) ? " ?" : ""))
					.collect(Collectors.joining(operator));
			return "(" + clause + ")";
		}
		return null;
	}


	private static String getOrderByClause(ReconciliationRunDetailCommand command, Map<String, DataTypes> fieldDataTypeMap, Set<String> innerJoins) {
		StringBuilder orderByClause = new StringBuilder(64);
		ReconciliationRunObjectSearchForm searchForm = command.getSearchForm();
		if (command.isShowHistory()) {
			orderByClause.append(ReconciliationRunObjectDetail.RUN_DATE).append(" DESC");
		}
		else if (searchForm.getOrderBy() != null) {
			String orderBy = searchForm.getOrderBy().split(":")[0];
			String direction = searchForm.getOrderBy().split(":")[1];
			OrderByDirections orderByDirection = !"DESC".equalsIgnoreCase(direction) ? OrderByDirections.ASC : OrderByDirections.DESC;  // default to ASC
			if (orderBy != null) {
				if (fieldDataTypeMap.containsKey(orderBy)) {
					orderByClause.append(JsonCompressedTextOrderBy.create("dataHolder", String.format("$.dataObjects[0].data.\"%s\"", orderBy.replaceAll("'", "''")), orderByDirection, getSearchFieldDataTypeClass(fieldDataTypeMap.get(orderBy))));
				}
				else {
					List<String> databaseFieldNameList = getDatabaseFieldNameList(orderBy, innerJoins);
					if (CollectionUtils.isEmpty(databaseFieldNameList)) {
						return "";
					}
					orderByClause.append(String.join(", ", databaseFieldNameList)).append(orderByDirection == OrderByDirections.ASC ? " ASC" : " DESC");
				}
			}
		}
		return orderByClause.toString();
	}


	private static List<String> getDatabaseFieldNameList(String fieldName, Set<String> innerJoins) {
		List<String> databaseFieldNameList = new ArrayList<>();
		switch (fieldName) {
			case "id":
				databaseFieldNameList.add("rro.ReconciliationRunObjectID");
				break;
			case "runId":
			case "runIds":
				databaseFieldNameList.add("rro.ReconciliationRunID");
				break;
			case "identifier":
			case "identifiers":
				databaseFieldNameList.add("rro.NaturalKeyIdentifier");
				break;
			case "importDate":
				databaseFieldNameList.add("rro.ImportDate");
				break;
			case "runDate":
				innerJoins.add(RECONCILIATION_RUN_JOIN);
				databaseFieldNameList.add("rr.RunDate");
				break;
			case "typeId":
				innerJoins.add(RECONCILIATION_RUN_JOIN);
				innerJoins.add(RECONCILIATION_DEFINITION_JOIN);
				databaseFieldNameList.add("rd.ReconciliationTypeID");
				break;
			case "typeName":
				innerJoins.add(RECONCILIATION_RUN_JOIN);
				innerJoins.add(RECONCILIATION_DEFINITION_JOIN);
				innerJoins.add(RECONCILIATION_TYPE_JOIN);
				databaseFieldNameList.add("rt.TypeName");
				break;
			case "definitionId":
				innerJoins.add(RECONCILIATION_RUN_JOIN);
				databaseFieldNameList.add("rr.ReconciliationDefinitionID");
				break;
			case "definitionGroupId":
				innerJoins.add(RECONCILIATION_DEFINITION_JOIN);
				databaseFieldNameList.add("rd.ReconciliationDefinitionGroupID");
				break;
			case "definitionGroupName":
				innerJoins.add(RECONCILIATION_DEFINITION_JOIN);
				innerJoins.add(RECONCILIATION_DEFINITION_GROUP_JOIN);
				databaseFieldNameList.add("rdg.GroupName");
				break;
			case "investmentAccountIdIsNull":
			case "investmentAccountId":
				databaseFieldNameList.add("rro.ReconciliationInvestmentAccountID");
				break;
			case "investmentAccountName":
				innerJoins.add(RECONCILIATION_INVESTMENT_ACCOUNT_JOIN);
				databaseFieldNameList.add("ria.AccountName");
				break;
			case "investmentAccountNumber":
				innerJoins.add(RECONCILIATION_INVESTMENT_ACCOUNT_JOIN);
				databaseFieldNameList.add("ria.AccountNumber");
				break;
			case "investmentSecurityIdIsNull":
			case "investmentSecurityId":
				databaseFieldNameList.add("rro.ReconciliationInvestmentSecurityID");
				break;
			case "investmentSecurityName":
				innerJoins.add(RECONCILIATION_INVESTMENT_SECURITY_JOIN);
				databaseFieldNameList.add("ris.SecurityName");
				break;
			case "investmentSecuritySymbol":
				innerJoins.add(RECONCILIATION_INVESTMENT_SECURITY_JOIN);
				databaseFieldNameList.add("ris.SecuritySymbol");
				break;
			case "reconcileStatusId":
			case "reconcileStatusIds":
				databaseFieldNameList.add("rro.ReconciliationReconcileStatusID");
				break;
			case "reconcileStatusName":
			case "reconcileStatusNames":
				innerJoins.add(RECONCILIATION_RECONCILE_STATUS_JOIN);
				databaseFieldNameList.add("rrs.StatusName");
				break;
			case "matchStatusId":
			case "matchStatusIds":
				databaseFieldNameList.add("rro.ReconciliationMatchStatusID");
				break;
			case "matchStatusName":
			case "matchStatusNames":
				innerJoins.add(RECONCILIATION_MATCH_STATUS_JOIN);
				databaseFieldNameList.add("rrm.StatusName");
				break;
			case "approvalUserId":
				innerJoins.add(RECONCILIATION_RUN_JOIN);
				databaseFieldNameList.add("rr.ApprovedByUserID");
				break;
			case "approvalDate":
				innerJoins.add(RECONCILIATION_RUN_JOIN);
				databaseFieldNameList.add("rr.UpdateDate");
				break;
			case "breakDays":
				databaseFieldNameList.add("rro.BreakDays");
				break;
			case "updateDate":
				databaseFieldNameList.add("rro.UpdateDate");
				break;
			case "updateUserId":
				databaseFieldNameList.add("rro.UpdateUserID");
				break;
			case "investmentSecurityOrAccountIdIsNull":
				databaseFieldNameList.add("rro.ReconciliationInvestmentAccountID");
				databaseFieldNameList.add("rro.ReconciliationInvestmentSecurityID");
				break;
			default:
				return null;
		}
		return databaseFieldNameList;
	}


	private static Class<?> getSearchFieldDataTypeClass(DataTypes dataType) {
		Class<?> dataTypeClass = String.class;
		if (dataType != null && dataType.getTypeName() != null) {
			if (dataType.getTypeName().isNumeric()) {
				dataTypeClass = BigDecimal.class;
			}
			else if (DataTypeNames.DATE.name().equals(dataType.getTypeName().name())) {
				dataTypeClass = Date.class;
			}
		}
		return dataTypeClass;
	}


	private static DataTypeNames getSearchFieldDataType(String fieldName) {
		PropertyDataTypeConverter converter = new PropertyDataTypeConverter();
		SearchField searchField = AnnotationUtils.getAnnotation(ClassUtils.getClassField(ReconciliationRunObjectSearchForm.class, fieldName, false, false), SearchField.class);
		if (searchField != null) {
			String searchFieldPath = StringUtils.isEqual(searchField.searchFieldPath(), SearchField.SAME_TABLE) ? "" : searchField.searchFieldPath();
			String searchFieldName = StringUtils.isEqual(searchField.searchField(), SearchField.SAME_AS_MARKED_FIELD) ? fieldName : searchField.searchField();
			String beanFieldPath = StringUtils.isEmpty(searchFieldPath) ? searchFieldName : searchFieldPath + "." + searchFieldName;
			return converter.convert(BeanUtils.getNestedPropertyDescriptor(new ReconciliationRunObject(), beanFieldPath)).getTypeName();
		}
		return null;
	}


	private static ResultSetExtractor<List<ReconciliationRunObjectDetail>> getRunObjectDetailResultSetExtractor(ReconciliationRunDetailCommand command) {
		return resultSet -> {
			List<ReconciliationRunObjectDetail> runObjectDetailList = new ArrayList<>();
			while (resultSet.next()) {
				//I use names instead of indexes in case column order changes and for clarity
				ReconciliationRunObjectDetail runObjectDetail = new ReconciliationRunObjectDetail();
				runObjectDetail.setId(resultSet.getLong(ReconciliationRunObjectDetail.RUN_OBJECT_ID));
				runObjectDetail.setReconciliationRunId(resultSet.getInt(ReconciliationRunObjectDetail.RUN_ID));
				runObjectDetail.setReconciliationInvestmentAccountId(resultSet.getInt(ReconciliationRunObjectDetail.ACCOUNT_ID));
				runObjectDetail.setReconciliationInvestmentSecurityId(resultSet.getInt(ReconciliationRunObjectDetail.SECURITY_ID));
				runObjectDetail.setMatchStatusId(resultSet.getShort(ReconciliationRunObjectDetail.MATCH_STATUS_ID));
				runObjectDetail.setReconcileStatusId(resultSet.getShort(ReconciliationRunObjectDetail.RECONCILE_STATUS_ID));
				runObjectDetail.setReconciledOnSourceId(resultSet.getShort(ReconciliationRunObjectDetail.RECONCILED_ON_SOURCE_ID));
				runObjectDetail.setIdentifier(resultSet.getString(ReconciliationRunObjectDetail.NATURAL_KEY_IDENTIFIER));
				runObjectDetail.setDataHolderBytes(resultSet.getBytes(ReconciliationRunObjectDetail.DATA_HOLDER));
				runObjectDetail.setBreakDays(resultSet.getInt(ReconciliationRunObjectDetail.BREAK_DAYS));
				runObjectDetail.setImportDate(resultSet.getDate(ReconciliationRunObjectDetail.IMPORT_DATE));
				runObjectDetail.setUpdateUserId(resultSet.getShort(ReconciliationRunObjectDetail.UPDATE_USER_ID));
				runObjectDetail.setUpdateDate(resultSet.getDate(ReconciliationRunObjectDetail.UPDATE_DATE));
				if (command.isShowHistory()) {
					runObjectDetail.setRunDate(resultSet.getDate(ReconciliationRunObjectDetail.RUN_DATE));
				}
				runObjectDetailList.add(runObjectDetail);
			}
			return runObjectDetailList;
		};
	}
}
