package com.clifton.reconciliation.run.notification;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableGenerator;
import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverter;
import com.clifton.core.dataaccess.datatable.bean.DataTableConverterConfiguration;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.search.ReconciliationRunObjectSearchForm;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author stevenf
 */
public class ReconciliationCorruptRunObjectDataTableGenerator implements DataTableGenerator {

	private static final String COLUMNS = "ID:id|" + //
			"Label:label|" + //
			"Corrupted Count:corrupted|" + //
			"Corrupted Object Ids:corruptObjectIds|";
	private Integer daysBack;
	private ApplicationContextService applicationContextService;

	private SqlHandler sqlHandler;
	private ReconciliationRunService reconciliationRunService;


	@Override
	@Transactional(timeout = 300)
	public DataTable getDataTable() {

		Date date = getDaysBack() != null ? DateUtils.addDays(new Date(), -getDaysBack()) : null;
		ReconciliationRunObjectSearchForm searchForm = new ReconciliationRunObjectSearchForm();
		searchForm.addSearchRestriction("updateDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, date);

		Map<ReconciliationRun, List<ReconciliationRunObject>> runObjectByRunMap = new HashMap<>();
		for (ReconciliationRunObject runObject : CollectionUtils.getIterable(getReconciliationRunService().getReconciliationRunObjectList(searchForm))) {
			if (isCorrupt(runObject)) {
				ReconciliationRun run = runObject.getReconciliationRun();
				List<ReconciliationRunObject> runObjectList = runObjectByRunMap.get(run);
				if (runObjectList == null) {
					runObjectList = new ArrayList<>();
				}
				runObjectList.add(runObject);
				runObjectByRunMap.put(run, runObjectList);
			}
		}

		List<CorruptRunObjectNotification> notificationList = new ArrayList<>();
		for (Map.Entry<ReconciliationRun, List<ReconciliationRunObject>> runObjectByRunMapEntry : runObjectByRunMap.entrySet()) {
			ReconciliationRun run = runObjectByRunMapEntry.getKey();
			List<ReconciliationRunObject> runObjectList = runObjectByRunMapEntry.getValue();
			notificationList.add(new CorruptRunObjectNotification(run.getId(), CollectionUtils.getSize(runObjectList), run.getLabel(),
					runObjectList.stream().map(ReconciliationRunObject::getId).collect(Collectors.toList()).toString()));
		}
		BeanCollectionToDataTableConverter<CorruptRunObjectNotification> converter = new BeanCollectionToDataTableConverter<>(DataTableConverterConfiguration.ofDtoClassUsingDataColumnString(getApplicationContextService(), CorruptRunObjectNotification.class, COLUMNS));
		return converter.convert(notificationList);
	}


	public boolean isCorrupt(ReconciliationRunObject runObject) {
		if (runObject.getDataHolder() == null) {
			return true;
		}
		if (CollectionUtils.isEmpty(runObject.getDataHolder().getDataObjects())) {
			return true;
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////


	public Integer getDaysBack() {
		return this.daysBack;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public void setDaysBack(Integer daysBack) {
		this.daysBack = daysBack;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	class CorruptRunObjectNotification {

		private final int id;
		private final int corrupted;

		private final String label;
		private final String corruptObjectIds;


		public CorruptRunObjectNotification(int id, int corrupted, String label, String corruptObjectIds) {
			this.id = id;
			this.corrupted = corrupted;
			this.label = label;
			this.corruptObjectIds = corruptObjectIds;
		}


		public int getId() {
			return this.id;
		}


		public int getCorrupted() {
			return this.corrupted;
		}


		public String getLabel() {
			return this.label;
		}


		public String getCorruptObjectIds() {
			return this.corruptObjectIds;
		}
	}
}
