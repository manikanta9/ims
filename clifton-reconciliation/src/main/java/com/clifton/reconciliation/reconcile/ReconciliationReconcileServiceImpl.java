package com.clifton.reconciliation.reconcile;

import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.json.condition.evaluator.util.JsonConditionEvaluatorUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.ReconciliationDefinitionService;
import com.clifton.reconciliation.definition.ReconciliationField;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluator;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluatorService;
import com.clifton.reconciliation.definition.evaluator.data.ReconciliationDataDefaultEvaluator;
import com.clifton.reconciliation.definition.evaluator.data.ReconciliationDataEvaluator;
import com.clifton.reconciliation.definition.evaluator.search.ReconciliationEvaluatorSearchForm;
import com.clifton.reconciliation.event.object.ReconciliationEventObject;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectCategories;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectService;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectStatuses;
import com.clifton.reconciliation.event.object.search.ReconciliationEventObjectSearchForm;
import com.clifton.reconciliation.investment.ReconciliationInvestmentHandler;
import com.clifton.reconciliation.rule.ReconciliationRuleCondition;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.data.DataError;
import com.clifton.reconciliation.run.data.DataObject;
import com.clifton.reconciliation.run.detail.ReconciliationRunDetailDataRetriever;
import com.clifton.reconciliation.run.search.ReconciliationRunObjectSearchForm;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatus;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationStatusService;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.clifton.core.util.status.Status.ofMessage;


/**
 * The <code>ReconciliationReconcileServiceImpl</code> provides a basic implementation of the
 * {@link ReconciliationReconcileService} interface
 *
 * @author TerryS
 */
@Service
public class ReconciliationReconcileServiceImpl implements ReconciliationReconcileService {

	private ScheduleApiService scheduleApiService;
	private ReconciliationEvaluatorService reconciliationEvaluatorService;
	private ReconciliationDefinitionService reconciliationDefinitionService;
	private ReconciliationEventObjectService reconciliationEventObjectService;
	private ReconciliationInvestmentHandler reconciliationInvestmentHandler;
	private ReconciliationRunService reconciliationRunService;
	private ReconciliationStatusService reconciliationStatusService;
	private SystemBeanService systemBeanService;

	private RunnerHandler runnerHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status processReconciliationRun(ReconciliationCommand command) {
		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : ofMessage("Synchronously running for " + command);
			doReconcileData(command, status);
			if (command.isThrowExceptionOnError() && status.getErrorCount() > 0) {
				throw new RuntimeException(status.getMessageWithErrors());
			}
			return status;
		}
		else {
			String runId = command.getRunId();
			// only allow one reconcile job per definition and run date
			ValidationUtils.assertFalse(getRunnerHandler().isRunnerWithTypeAndIdActive("RECONCILIATION-RECONCILE", runId), () -> String.format("Reconciliation for date [%s] is currently running.", DateUtils.fromDateShort(command.getReconcileDate())));
			final Date runDate = DateUtils.addSeconds(new Date(), command.getAsynchronousRebuildDelay());
			final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + DateUtils.fromDate(runDate));
			Runner runner = new AbstractStatusAwareRunner("RECONCILIATION-RECONCILE", runId, runDate, statusHolder) {

				@Override
				public void run() {
					try {
						ValidationUtils.assertNotNull(command.getReconcileDate(), "Reconcile Date is required for reconciliation.");
						doReconcileData(command, statusHolder.getStatus());
					}
					catch (Exception e) {
						getStatus().setMessage("Error reconciling for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
						getStatus().addError(ExceptionUtils.getDetailedMessage(e));
						LogUtils.errorOrInfo(getClass(), "Error reconciling for " + runId, e);
					}
				}
			};
			getRunnerHandler().rescheduleRunner(runner);
			return Status.ofMessage("Started reconciling for requested definitions on " + DateUtils.fromDateShort(command.getReconcileDate()) + ". Processing will be completed shortly.");
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void doReconcileData(ReconciliationCommand command, Status status) {
		AssertUtils.assertNotNull(command, "Reconciliation command is required.");

		ValidationUtils.assertNotNull(command.getReconcileDate(), "Reconciliation Date is required.");
		if (command.getReconciliationDefinitionId() != null) {
			ReconciliationDefinition definition = getReconciliationDefinitionService().getReconciliationDefinition(command.getReconciliationDefinitionId());
			ValidationUtils.assertNotNull(definition, () -> String.format("Reconciliation Definition [%s] cannot be found ", command.getReconciliationDefinitionId()));
		}

		ReconciliationRunSearchForm reconciliationRunSearchForm = new ReconciliationRunSearchForm();
		reconciliationRunSearchForm.setRunDate(command.getReconcileDate());
		reconciliationRunSearchForm.setDefinitionId(command.getReconciliationDefinitionId());
		reconciliationRunSearchForm.setTypeId(command.getReconciliationTypeId());

		List<ReconciliationRun> runList = CollectionUtils.asNonNullList(getReconciliationRunService().getReconciliationRunList(reconciliationRunSearchForm));
		runList.stream().map(r -> processReconciliationByDefinitionAndDate(r.getReconciliationDefinition().getId(), command.getReconcileDate()))
				.forEach(r -> status.addDetail(r.getReconciliationDefinition().getName(), r.getRunDetails().getMessage()));

		status.setActionPerformed(!runList.isEmpty());
		status.setMessage("Total runs reconciled: " + runList.size());
	}


	/**
	 * Reconciles the data for the provided definition id.
	 * Also reconciles the Transaction run for CASH reconciliation if the Transaction definition is tied to the CASH reconciliation (i.e. the Transaction definition has a parent id)
	 */
	@Override
	@Transactional
	public ReconciliationRun processReconciliationByDefinitionAndDate(int definitionId, Date runDate) {
		ReconciliationDefinition definition = getReconciliationDefinitionService().getReconciliationDefinition(definitionId);
		ValidationUtils.assertNotNull(definition, "Unable to locate definition with id: " + definitionId);
		return reconcileReconciliationRunByDefinitionAndDate(definition, runDate);
	}


	private ReconciliationRun reconcileReconciliationRunByDefinitionAndDate(ReconciliationDefinition definition, Date runDate) {
		ReconciliationRun run = getReconciliationRunService().getReconciliationRunByDefinitionAndDate(definition.getId(), definition.getType().isCumulativeReconciliation() ? null : runDate);
		if (run == null) {
			throw new ValidationException("No run found for definition [" + definition.getLabel() + "] for date [" + DateUtils.fromDate(runDate, DateUtils.DATE_FORMAT_INPUT) + "].  Ensure data has been loaded.");
		}
		return reconcileReconciliationRun(run, null);
	}


	@Override
	public ReconciliationRun reconcileReconciliationRun(ReconciliationRun run, Long[] runObjectIds) {
		long executionTimeStart = System.currentTimeMillis();

		ReconciliationMatchStatus matchedStatus = getReconciliationStatusService().getReconciliationMatchStatusByStatusType(ReconciliationMatchStatusTypes.MATCHED);
		ReconciliationMatchStatus manuallyMatchedStatus = getReconciliationStatusService().getReconciliationMatchStatusByStatusType(ReconciliationMatchStatusTypes.MANUALLY_MATCHED);
		ReconciliationReconcileStatus notReconciledStatus = getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.NOT_RECONCILED);

		ReconciliationRunObjectSearchForm searchForm = new ReconciliationRunObjectSearchForm();
		if (runObjectIds != null && !CollectionUtils.isEmpty(Arrays.asList(runObjectIds))) {
			searchForm.setIds(runObjectIds);
		}
		searchForm.setRunId(run.getId());
		searchForm.setMatchStatusIds(new Short[]{matchedStatus.getId(), manuallyMatchedStatus.getId()});
		searchForm.setReconcileStatusId(notReconciledStatus.getId());

		List<String> errors = reconcileRunObjectList(getReconciliationRunService().getReconciliationRunObjectList(searchForm));
		for (String error : CollectionUtils.getIterable(errors)) {
			run.getRunDetails().addError(error);
		}

		//Process and send account unlock events
		getReconciliationInvestmentHandler().sendAccountUnlockEvents(run);

		LogUtils.info(this.getClass(), "Reconcile Runtime (ms): " + (System.currentTimeMillis() - executionTimeStart));
		run = getReconciliationRunService().saveReconciliationRun(run);

		return run;
	}


	private List<String> reconcileRunObjectList(List<ReconciliationRunObject> runObjectList) {
		List<String> errors = new ArrayList<>();
		if (!CollectionUtils.isEmpty(runObjectList)) {
			ValidationUtils.assertTrue(CollectionUtils.isEmpty(CollectionUtils.getStream(runObjectList)
					.filter(runObject -> !ReconciliationMatchStatusTypes.NOT_MATCHED.name().equals(runObject.getReconciliationMatchStatus().getName())
							&& ReconciliationReconcileStatusTypes.RECONCILED.name().equals(runObject.getReconciliationReconcileStatus().getName()))
					.collect(Collectors.toList())), "Only matched and unreconciled items should be run through reconciliation logic!");

			ReconciliationRun run = CollectionUtils.getFirstElementStrict(runObjectList).getReconciliationRun();
			ReconciliationReconcileStatus reconciledStatus = getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.RECONCILED);
			ReconciliationReconcileStatus notReconciledStatus = getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.NOT_RECONCILED);
			ReconciliationReconcileStatus manuallyReconciledStatus = getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED);

			ReconciliationEventObjectSearchForm eventObjectSearchForm = new ReconciliationEventObjectSearchForm();
			eventObjectSearchForm.setRunOrParentRunId(run.getId());
			eventObjectSearchForm.setDataTypeCategories(new String[]{ReconciliationEventObjectCategories.ADJUSTING_JOURNAL.name(), ReconciliationEventObjectCategories.SIMPLE_JOURNAL.name()});
			eventObjectSearchForm.setStatus(ReconciliationEventObjectStatuses.COMPLETE);
			List<ReconciliationEventObject<?>> eventObjectList = getReconciliationEventObjectService().getReconciliationEventObjectList(eventObjectSearchForm);

			Map<Long, BigDecimal> adjustmentAmountMap = ReconciliationRunDetailDataRetriever.getAdjustmentAmountMap(eventObjectList);
			Map<String, Integer> reconciliationBreakDaysMap = getReconciliationRunService().getReconciliationBreakDaysMap(run.getReconciliationDefinition().getId(), run.getRunDate(), run.getReconciliationDefinition().getType().isCumulativeReconciliation());
			for (ReconciliationRunObject runObject : CollectionUtils.getIterable(runObjectList)) {
				try {
					if (processReconciliationRunObject(runObject)) {
						List<DataError> errorList = reconcileRunObject(runObject, adjustmentAmountMap.get(runObject.getId()));
						if (!CollectionUtils.isEmpty(errorList)) {
							for (DataError error : CollectionUtils.getIterable(errorList)) {
								runObject.getDataHolder().addError(error);
							}
							Integer breakDays = reconciliationBreakDaysMap.get(runObject.getIdentifier());
							runObject.setBreakDays(breakDays != null ? breakDays + 1 : 1);
						}
						else {
							//Clear errors and reset break days since it auto-reconciled
							runObject.setBreakDays(0);
							runObject.getDataHolder().setErrors(null);
							if (runObject.getReconciliationMatchStatus().getReconciliationMatchStatusType() == ReconciliationMatchStatusTypes.MANUALLY_MATCHED || !CollectionUtils.isEmpty(runObject.getDataNoteList())) {
								runObject.setReconciliationReconcileStatus(manuallyReconciledStatus);
							}
							else {
								runObject.setReconciliationReconcileStatus(reconciledStatus);
							}
						}
					}
					else {
						//If the object wasn't matched, we just update the break days
						Integer breakDays = reconciliationBreakDaysMap.get(runObject.getIdentifier());
						runObject.setBreakDays(breakDays != null ? breakDays + 1 : 1);
					}
				}
				catch (Exception e) {
					errors.add(e.getMessage());
					runObject.setReconciliationReconcileStatus(notReconciledStatus);
				}
			}
			//Save in one big batch
			getReconciliationRunService().saveReconciliationRunObjectList(runObjectList);
		}
		return errors;
	}


	private boolean processReconciliationRunObject(ReconciliationRunObject reconciliationRunObject) {
		String matchStatus = reconciliationRunObject.getReconciliationMatchStatus().getName();
		return (matchStatus.equals(ReconciliationMatchStatusTypes.MATCHED.name()) || matchStatus.equals(ReconciliationMatchStatusTypes.MANUALLY_MATCHED.name()));
	}


	/**
	 * Executes the data reconciliation on a ReconciliationRunObject
	 * Updates errors and resulting status on the ReconciliationRunObject to either 'FORCED_RECONCILED', 'RECONCILED', OR 'FAILED'
	 * Performs the actual reconciliation and compares the values of the ReconciliationRunObject
	 * This does not change any statuses and only returns a list of errors if any
	 */
	private List<DataError> reconcileRunObject(ReconciliationRunObject runObject, BigDecimal adjustmentAmount) {
		List<DataError> errorList = new ArrayList<>();
		List<ReconciliationEvaluator> runObjectEvaluatorList = getEvaluatorList(runObject);
		ReconciliationDefinition definition = getReconciliationDefinitionService().getReconciliationDefinitionPopulated(runObject.getReconciliationRun().getReconciliationDefinition().getId(), runObject.getReconciliationRun().getRunDate());
		for (ReconciliationDefinitionField definitionField : CollectionUtils.getIterable(definition.getReconciliationDefinitionFieldList().stream().filter(ReconciliationDefinitionField::isReconcilable).collect(Collectors.toList()))) {
			boolean evaluated = false;
			applyAdjustmentAmount(runObject, definitionField, adjustmentAmount, true);
			//Filter to evaluators for this field
			List<ReconciliationEvaluator> evaluatorList = runObjectEvaluatorList.stream().filter(evaluator -> isExecuteEvaluator(evaluator, runObject, definitionField)).collect(Collectors.toList());
			for (ReconciliationEvaluator evaluator : CollectionUtils.getIterable(evaluatorList)) {
				ReconciliationDataEvaluator dataEvaluatorBean = (ReconciliationDataEvaluator) getSystemBeanService().getBeanInstance(evaluator.getEvaluatorBean());
				if (dataEvaluatorBean != null) {
					DataError error = dataEvaluatorBean.evaluate(runObject, evaluator.getDefinitionField());
					if (error != null) {
						errorList.add(error);
					}
					evaluated = true;
					break;
				}
			}
			if (!evaluated) {
				ReconciliationDataEvaluator dataEvaluatorBean = new ReconciliationDataDefaultEvaluator();
				DataError error = dataEvaluatorBean.evaluate(runObject, definitionField);
				if (error != null) {
					errorList.add(error);
				}
			}
			applyAdjustmentAmount(runObject, definitionField, adjustmentAmount, false);
		}
		return errorList;
	}


	private List<ReconciliationEvaluator> getEvaluatorList(ReconciliationRunObject runObject) {
		ReconciliationEvaluatorSearchForm searchForm = new ReconciliationEvaluatorSearchForm();
		searchForm.setDefinitionId(runObject.getReconciliationRun().getReconciliationDefinition().getId());
		searchForm.setActiveOnDate(runObject.getReconciliationRun().getRunDate());
		searchForm.setOrderBy("evaluationOrder:asc");
		return CollectionUtils.sort(getReconciliationEvaluatorService().getReconciliationEvaluatorList(searchForm), Comparator.comparingInt(ReconciliationEvaluator::getEvaluationOrder));
	}


	private boolean isExecuteEvaluator(ReconciliationEvaluator evaluator, ReconciliationRunObject runObject, ReconciliationDefinitionField definitionField) {
		//First check if the evaluator applies to this definitionField
		if (!Objects.equals(definitionField, evaluator.getDefinitionField())) {
			return false;
		}
		if (evaluator.getReconciliationInvestmentAccount() != null && !(Objects.equals(evaluator.getReconciliationInvestmentAccount(), runObject.getReconciliationInvestmentAccount()))) {
			return false;
		}
		if (evaluator.getReconciliationInvestmentSecurity() != null && !(Objects.equals(evaluator.getReconciliationInvestmentSecurity(), runObject.getReconciliationInvestmentSecurity()))) {
			return false;
		}
		if (evaluator.getCalendarSchedule() != null) {
			Date runDate = runObject.getReconciliationRun().getRunDate();
			Date scheduleDate = getScheduleApiService().getScheduleOccurrence(ScheduleOccurrenceCommand.forOccurrences(evaluator.getCalendarSchedule().getId(), 1, runDate));
			if (DateUtils.isEqualWithoutTime(runDate, scheduleDate)) {
				if (evaluator.isExclusive()) {
					return false;
				}
			}
			else {
				if (!evaluator.isExclusive()) {
					return false;
				}
			}
		}
		ReconciliationRuleCondition conditionRule = evaluator.getConditionRule();
		if (conditionRule != null) {
			//If the conditionRule evaluates to true for any object in the primary or secondary list then we should evaluate.
			List<DataObject> primaryDataObjectList = runObject.getDataHolder().getDataObjectListBySource(runObject.getReconciliationRun().getReconciliationDefinition().getPrimarySource());
			List<DataObject> secondaryDataObjectList = runObject.getDataHolder().getDataObjectListBySource(runObject.getReconciliationRun().getReconciliationDefinition().getSecondarySource());
			for (DataObject primaryDataObject : CollectionUtils.getIterable(primaryDataObjectList)) {
				if (JsonConditionEvaluatorUtils.evaluateCondition(conditionRule.getJsonExpression(), primaryDataObject.getData())) {
					return true;
				}
			}
			for (DataObject secondaryDataObject : CollectionUtils.getIterable(secondaryDataObjectList)) {
				if (JsonConditionEvaluatorUtils.evaluateCondition(conditionRule.getJsonExpression(), secondaryDataObject.getData())) {
					return true;
				}
			}
			return false;
		}
		return true;
	}


	private void applyAdjustmentAmount(ReconciliationRunObject runObject, ReconciliationDefinitionField definitionField, BigDecimal adjustmentAmount, boolean add) {
		String fieldName = definitionField.getField().getName();
		ReconciliationDefinition definition = definitionField.getReconciliationDefinition();
		DataTypes dataType = ObjectUtils.coalesce(definitionField.getDataType(), definitionField.getField().getDefaultDataType());
		if (dataType != null) {
			Map<String, Object> primarySourceData = runObject.getDataHolder().getDataObjectListBySource(definition.getPrimarySource()).get(0).getData();
			if (ReconciliationField.CASH_BALANCE.equals(fieldName)) {
				BigDecimal cashBalance = DataTypeNameUtils.convertObjectToDataTypeName(primarySourceData.get(fieldName), dataType.getTypeName(), BigDecimal.class);
				if (add) {
					primarySourceData.put(fieldName, MathUtils.add(cashBalance, adjustmentAmount));
				}
				else {
					primarySourceData.put(fieldName, MathUtils.subtract(cashBalance, adjustmentAmount));
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ScheduleApiService getScheduleApiService() {
		return this.scheduleApiService;
	}


	public void setScheduleApiService(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public ReconciliationStatusService getReconciliationStatusService() {
		return this.reconciliationStatusService;
	}


	public void setReconciliationStatusService(ReconciliationStatusService reconciliationStatusService) {
		this.reconciliationStatusService = reconciliationStatusService;
	}


	public ReconciliationEventObjectService getReconciliationEventObjectService() {
		return this.reconciliationEventObjectService;
	}


	public void setReconciliationEventObjectService(ReconciliationEventObjectService reconciliationEventObjectService) {
		this.reconciliationEventObjectService = reconciliationEventObjectService;
	}


	public ReconciliationEvaluatorService getReconciliationEvaluatorService() {
		return this.reconciliationEvaluatorService;
	}


	public void setReconciliationEvaluatorService(ReconciliationEvaluatorService reconciliationEvaluatorService) {
		this.reconciliationEvaluatorService = reconciliationEvaluatorService;
	}


	public ReconciliationInvestmentHandler getReconciliationInvestmentHandler() {
		return this.reconciliationInvestmentHandler;
	}


	public void setReconciliationInvestmentHandler(ReconciliationInvestmentHandler reconciliationInvestmentHandler) {
		this.reconciliationInvestmentHandler = reconciliationInvestmentHandler;
	}


	public ReconciliationDefinitionService getReconciliationDefinitionService() {
		return this.reconciliationDefinitionService;
	}


	public void setReconciliationDefinitionService(ReconciliationDefinitionService reconciliationDefinitionService) {
		this.reconciliationDefinitionService = reconciliationDefinitionService;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
