package com.clifton.reconciliation;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


/**
 * @author stevenf
 */
public class ReconciliationBreakDaysResultSetExtractor implements ResultSetExtractor<Map<String, Integer>> {

	@Override
	public Map<String, Integer> extractData(ResultSet rs) throws SQLException, DataAccessException {
		Map<String, Integer> breakDaysMap = new HashMap<>();
		while (rs.next()) {
			breakDaysMap.put(rs.getString("NaturalKeyIdentifier"), rs.getInt("BreakDays"));
		}
		return breakDaysMap;
	}
}
