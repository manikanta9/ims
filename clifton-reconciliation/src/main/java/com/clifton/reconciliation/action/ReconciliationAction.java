package com.clifton.reconciliation.action;

import com.clifton.reconciliation.ReconciliationServiceContext;
import com.clifton.reconciliation.run.ReconciliationRun;


/**
 * Executes the Action bean(s) after the {@link ReconciliationRun} has been created.
 *
 * @author KellyJ
 */
public interface ReconciliationAction {

	public void processAction(ReconciliationRun reconciliationRun, ReconciliationServiceContext context);
}
