package com.clifton.reconciliation.action;

import com.clifton.core.util.CollectionUtils;
import com.clifton.reconciliation.ReconciliationServiceContext;
import com.clifton.reconciliation.definition.type.ReconciliationType;
import com.clifton.reconciliation.run.ReconciliationRun;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


/**
 * Implementation for executing the Action bean(s) for the specified {@link ReconciliationRun}
 *
 * @author KellyJ
 */
@Component
public class ReconciliationActionHandlerImpl implements ReconciliationActionHandler {

	private Map<String, List<ReconciliationAction>> reconciliationTypeToActionsMap;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void executeActions(ReconciliationRun reconciliationRun, ReconciliationServiceContext context) {
		if (reconciliationRun != null && reconciliationRun.getReconciliationDefinition() != null) {
			ReconciliationType type = reconciliationRun.getReconciliationDefinition().getType();
			for (ReconciliationAction action : CollectionUtils.getIterable(getReconciliationTypeToActionsMap().get(type.getName()))) {
				if (action != null) {
					action.processAction(reconciliationRun, context);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Map<String, List<ReconciliationAction>> getReconciliationTypeToActionsMap() {
		return this.reconciliationTypeToActionsMap;
	}


	public void setReconciliationTypeToActionsMap(Map<String, List<ReconciliationAction>> reconciliationTypeToActionsMap) {
		this.reconciliationTypeToActionsMap = reconciliationTypeToActionsMap;
	}
}
