package com.clifton.reconciliation.action;

import com.clifton.reconciliation.ReconciliationServiceContext;
import com.clifton.reconciliation.run.ReconciliationRun;


/**
 * Interface for the Action bean(s) for the specified {@link ReconciliationRun}
 *
 * @author KellyJ
 */
public interface ReconciliationActionHandler {

	public void executeActions(ReconciliationRun reconciliationRun, ReconciliationServiceContext context);
}
