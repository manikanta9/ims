package com.clifton.reconciliation.simpletable;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.StringUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;


/**
 * @author TerryS
 */
public class CSVFileToSimpleTableConverter implements FileToSimpleTableConverter {

	private static final char DEFAULT_SEPARATOR = ',';
	private static final char DEFAULT_QUOTE = '"';
	private static final boolean DEFAULT_SKIP_EMPTY_LINES = true;
	private static final boolean DEFAULT_TRIM_VALUES = false;

	private final boolean skipEmptyLines;
	private final boolean trimValues;


	private static CSVParser getParser(File file, char separators, char customQuote, boolean trimValues) throws IOException {
		if (customQuote == ' ') {
			customQuote = DEFAULT_QUOTE;
		}
		if (separators == ' ') {
			separators = DEFAULT_SEPARATOR;
		}
		if (trimValues) {
			return CSVParser.parse(file, Charset.defaultCharset(), CSVFormat.newFormat(separators).withQuote(customQuote).withTrim().withFirstRecordAsHeader());
		}
		else {
			return CSVParser.parse(file, Charset.defaultCharset(), CSVFormat.newFormat(separators).withQuote(customQuote).withFirstRecordAsHeader());
		}
	}


	public CSVFileToSimpleTableConverter() {
		this(DEFAULT_SKIP_EMPTY_LINES, DEFAULT_TRIM_VALUES);
	}


	public CSVFileToSimpleTableConverter(boolean skipEmptyLines) {
		this.skipEmptyLines = skipEmptyLines;
		this.trimValues = DEFAULT_TRIM_VALUES;
	}


	public CSVFileToSimpleTableConverter(boolean skipEmptyLines, boolean trimValues) {
		this.skipEmptyLines = skipEmptyLines;
		this.trimValues = trimValues;
	}


	@Override
	public SimpleTable convert(Object fileObj) {
		SimpleTable table = null;
		File file;
		if (fileObj instanceof MultipartFile) {
			file = FileUtils.convertMultipartFileToFile((MultipartFile) fileObj);
		}
		else {
			file = (File) fileObj;
		}
		try (final CSVParser parser = getParser(file, DEFAULT_SEPARATOR, DEFAULT_QUOTE, isTrimValues())) {
			List<String> columns = parser.getHeaderNames();
			table = new SimpleTable(columns);
			// make sure to use iterator so only one line is loaded at a time.
			for (final CSVRecord record : parser) {
				String[] data = new String[columns.size()];
				for (int i = 0; i < record.size(); i++) {
					String value = record.get(i);
					if (i < columns.size()) {
						data[i] = value;
					}
				}
				if (isSkipEmptyLines() && isRowEmpty(data)) {
					continue;
				}
				table.addRow(data);
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to process csv file.", e);
		}
		return table;
	}


	private boolean isRowEmpty(String[] row) {
		if (row != null && row.length != 0) {
			for (String fieldValue : row) {
				if (!StringUtils.isEmpty(fieldValue)) {
					return false;
				}
			}
		}
		return true;
	}
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isSkipEmptyLines() {
		return this.skipEmptyLines;
	}


	public boolean isTrimValues() {
		return this.trimValues;
	}
}
