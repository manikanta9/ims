package com.clifton.reconciliation.simpletable;


import com.clifton.core.dataaccess.file.ExcelFileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>ExcelFileToSimpleTableConverter</code> ...
 *
 * @author manderson
 */
public class ExcelFileToSimpleTableConverter implements FileToSimpleTableConverter {

	private int sheetIndex = 0;


	@Override
	public SimpleTable convert(Object file) {
		Workbook workbook;
		if (file instanceof Workbook) {
			workbook = (Workbook) file;
		}
		else {
			workbook = ExcelFileUtils.createWorkbook(file);
		}
		Sheet sheet = workbook.getSheetAt(getSheetIndex());
		List<String> columnList = new ArrayList<>();
		List<Integer> columnDataTypeList = new ArrayList<>();
		int columnIndex = 0;
		Cell cell = ExcelFileUtils.getCell(sheet, 0, columnIndex, false);
		while (cell != null) {
			Cell dataCell = getFirstDataCell(sheet, columnIndex);
			String columnName = (String) ExcelFileUtils.getCellValue(sheet, 0, columnIndex, Types.NVARCHAR);
			if (columnName != null) {
				columnName = columnName.trim();
			}
			columnDataTypeList.add(ExcelFileUtils.getSqlDataType(columnName, (dataCell == null ? CellType.STRING : dataCell.getCellTypeEnum())));
			columnList.add(columnName);
			cell = ExcelFileUtils.getCell(sheet, 0, ++columnIndex, false);
		}
		SimpleTable simpleTable = new SimpleTable(columnList);
		// Actual Data starts at index 1
		int rowIndex = 1;
		Row row = ExcelFileUtils.getRow(sheet, rowIndex, false);
		while (row != null) {
			boolean emptyRow = true;
			Object[] data = new Object[columnList.size()];
			for (int j = 0; j < columnList.size(); j++) {
				Object value = ExcelFileUtils.getCellValue(row, j, columnDataTypeList.get(j));
				if (value != null && emptyRow) {
					emptyRow = false;
				}
				data[j] = value;
			}
			if (!emptyRow) {
				simpleTable.addRow(data);
			}
			row = ExcelFileUtils.getRow(sheet, ++rowIndex, false);
		}
		return simpleTable;
	}


	private Cell getFirstDataCell(Sheet sheet, int columnIndex) {
		int rowIndex = 1;
		Row row = ExcelFileUtils.getRow(sheet, rowIndex, false);
		while (row != null) {
			Cell cell = row.getCell(columnIndex);
			if (cell != null) {
				return cell;
			}
			row = ExcelFileUtils.getRow(sheet, ++rowIndex, false);
		}
		return null;
	}


	public int getSheetIndex() {
		return this.sheetIndex;
	}


	public void setSheetIndex(int sheetIndex) {
		this.sheetIndex = sheetIndex;
	}
}
