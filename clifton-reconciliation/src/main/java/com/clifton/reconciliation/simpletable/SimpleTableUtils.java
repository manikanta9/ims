package com.clifton.reconciliation.simpletable;

import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.validation.ValidationException;

import java.io.File;


/**
 * <code>SimpleTableUtils</code> contains helper functions for file to {@link SimpleTable} conversion.
 *
 * @author TerryS
 */
public class SimpleTableUtils {

	public static SimpleTable convertFileToSimpleTable(File file) {
		FileToSimpleTableConverter converter = null;
		String fileExtension = FileUtils.getFileExtension(file.getName());
		if (FileFormats.CSV.getFormatString().equals(fileExtension) || FileFormats.TXT.getFormatString().equalsIgnoreCase(fileExtension)) {
			converter = new CSVFileToSimpleTableConverter();
		}
		else if (FileFormats.EXCEL_OPEN_XML.getFormatString().equals(fileExtension)) {
			converter = new ExcelFileToSimpleTableConverter();
		}
		else if (FileFormats.EXCEL.getFormatString().equals(fileExtension)) {
			converter = new ExcelFileToSimpleTableConverter();
		}
		if (converter == null) {
			throw new ValidationException("Unsupported File Format (" + file.getName() + ")!");
		}
		return converter.convert(file);
	}
}
