package com.clifton.reconciliation;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.json.condition.evaluator.JsonConditionEvaluator;
import com.clifton.core.json.condition.evaluator.util.JsonConditionEvaluatorUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.EventNotProcessedException;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.reconciliation.action.ReconciliationActionHandler;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.ReconciliationDefinitionService;
import com.clifton.reconciliation.definition.ReconciliationField;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.definition.preprocessor.ReconciliationDefinitionPreprocessor;
import com.clifton.reconciliation.definition.preprocessor.data.ReconciliationDefinitionDataPreprocessor;
import com.clifton.reconciliation.definition.rule.ReconciliationFilter;
import com.clifton.reconciliation.definition.util.ReconciliationDefinitionUtils;
import com.clifton.reconciliation.exclusion.ReconciliationExclusionAssignment;
import com.clifton.reconciliation.exclusion.cache.ReconciliationExclusionAssignmentSource;
import com.clifton.reconciliation.exclusion.cache.ReconciliationExclusionAssignmentSpecificityCache;
import com.clifton.reconciliation.file.ReconciliationFileCommand;
import com.clifton.reconciliation.file.ReconciliationFileDefinition;
import com.clifton.reconciliation.file.ReconciliationFileDefinitionService;
import com.clifton.reconciliation.file.search.ReconciliationFileDefinitionSearchForm;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccountAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.ReconciliationRunOrphanObject;
import com.clifton.reconciliation.run.ReconciliationRunOrphanObjectTypes;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.data.DataHolder;
import com.clifton.reconciliation.run.data.DataObject;
import com.clifton.reconciliation.run.search.ReconciliationRunObjectSearchForm;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationStatusService;
import com.clifton.reconciliation.run.util.ReconciliationRunUtils;
import com.clifton.reconciliation.simpletable.SimpleTable;
import com.clifton.reconciliation.simpletable.SimpleTableUtils;
import com.clifton.system.bean.SystemBeanService;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.ObjIntConsumer;
import java.util.stream.Collectors;


/**
 * The <code>ReconciliationServiceImpl</code> provides a basic implementation of the
 * {@link ReconciliationService} interface
 *
 * @author StevenF on 11/23/2016.
 */
@Service
public class ReconciliationServiceImpl implements ReconciliationService {

	private CalendarSetupService calendarSetupService;
	private CalendarBusinessDayService calendarBusinessDayService;

	private ReconciliationDefinitionService reconciliationDefinitionService;
	private ReconciliationFileDefinitionService reconciliationFileDefinitionService;
	private ReconciliationInvestmentService reconciliationInvestmentService;

	private ReconciliationRunService reconciliationRunService;
	private ReconciliationStatusService reconciliationStatusService;
	private ReconciliationActionHandler reconciliationActionHandler;

	private SystemBeanService systemBeanService;

	private ReconciliationExclusionAssignmentSpecificityCache reconciliationExclusionAssignmentSpecificityCache;

	////////////////////////////////////////////////////////////////////////////
	////////   Reconciliation Context Service Initialization Methods   /////////
	////////////////////////////////////////////////////////////////////////////


	private ReconciliationServiceContext initializeContext(ReconciliationFileCommand fileCommand) {
		ReconciliationServiceContext context = new ReconciliationServiceContext();
		context.setFileDefinition(getFileDefinition(fileCommand));
		context.setImportDate(fileCommand.getImportDate());
		AssertUtils.assertNotNull(context.getFileDefinition(), "File definition may not be null!");
		AssertUtils.assertNotNull(context.getImportDate(), "Import Date may not be null!!");

		//Locate the fileDefinition applicable to the uploaded file then retrieve the file or multi-part file from the command object
		long functionStartTime = System.currentTimeMillis();
		File file = null;
		try {
			file = fileCommand.getFileRetrievalFunction() != null ? fileCommand.getFileRetrievalFunction().apply(fileCommand.getFileRetrievalId()) : null;
			if (file == null) {
				file = FileUtils.convertMultipartFileToFile(fileCommand.getFile());
				fileCommand.setFile(null);
			}
			LogUtils.info(getClass(), "File Retrieval Runtime (ms): " + (System.currentTimeMillis() - functionStartTime));
			AssertUtils.assertNotNull(file, "You must upload a file to create a reconciliation run!");

			functionStartTime = System.currentTimeMillis();
			context.setSimpleTable(SimpleTableUtils.convertFileToSimpleTable(file));
			LogUtils.info(getClass(), "SimpleTable Conversion Runtime (ms): " + (System.currentTimeMillis() - functionStartTime));
		}
		finally {
			if (file != null && file.exists()) {
				FileUtils.delete(file);
			}
		}

		//Now we use the definition ids to either find existing, or create new reconciliation runs and then create a map of the definition field names for use later.
		context.setReconciliationRunList(getReconciliationRunList(context.getFileDefinition(), context.getRunDate()));
		for (ReconciliationRun run : CollectionUtils.getIterable(context.getReconciliationRunList())) {
			context.getDefinitionFieldMappingNamesByRunMap().put(run.getId(), ReconciliationDefinitionUtils.getReconciliationDefinitionFieldMappingNamesBySource(run.getReconciliationDefinition(), context.getSource()));
		}

		//Retrieve and store the statuses used during data load:
		initializeStatuses(context);

		return initializeContext(context);
	}


	private ReconciliationServiceContext initializeContext(ReconciliationServiceContext context) {
		AssertUtils.assertNotNull(context.getFileDefinition(), "File definition may not be null!");
		AssertUtils.assertNotNull(context.getImportDate(), "Import Date may not be null!!");

		long functionStartTime = System.currentTimeMillis();
		getReconciliationInvestmentService().getReconciliationInvestmentAccountByNumber("0");
		LogUtils.info(getClass(), "Populate Account Cache Runtime (ms): " + (System.currentTimeMillis() - functionStartTime));

		functionStartTime = System.currentTimeMillis();
		getReconciliationInvestmentService().getReconciliationInvestmentSecurityByIdentifier("0");
		LogUtils.info(getClass(), "Populate Security Cache Runtime (ms): " + (System.currentTimeMillis() - functionStartTime));

		functionStartTime = System.currentTimeMillis();
		context.setAccountAliasMap(getReconciliationInvestmentService().getReconciliationInvestmentAccountAliasMap(context.getSource().getName(), context.getRunDate()));
		LogUtils.info(getClass(), "Populate AccountAliasMap Runtime (ms): " + (System.currentTimeMillis() - functionStartTime));

		functionStartTime = System.currentTimeMillis();
		context.setSecurityAliasMap(getReconciliationInvestmentService().getReconciliationInvestmentSecurityAliasMap(context.getSource().getName(), context.getRunDate()));
		LogUtils.info(getClass(), "Populate SecurityAliasMap Runtime (ms): " + (System.currentTimeMillis() - functionStartTime));

		//Retrieve and store the statuses used during data load:
		initializeStatuses(context);

		return context;
	}


	private void initializeStatuses(ReconciliationServiceContext context) {
		if (context.getMatchedStatus() == null) {
			context.setMatchedStatus(getReconciliationStatusService().getReconciliationMatchStatusByStatusType(ReconciliationMatchStatusTypes.MATCHED));
			context.setNotMatchedStatus(getReconciliationStatusService().getReconciliationMatchStatusByStatusType(ReconciliationMatchStatusTypes.NOT_MATCHED));
			context.setReconciledStatus(getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.RECONCILED));
			context.setNotReconciledStatus(getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.NOT_RECONCILED));
		}
	}


	private ReconciliationFileDefinition getFileDefinition(ReconciliationFileCommand fileCommand) {
		ReconciliationFileDefinition fileDefinition;
		//Now if this was an automated upload from integration look-up the upload file definition and set the sourceId and definitionId(s) on the command.
		if (fileCommand.isAutomatedUpload()) {
			ReconciliationFileDefinitionSearchForm fileDefinitionSearchForm = new ReconciliationFileDefinitionSearchForm();
			fileDefinitionSearchForm.setDefinitionFileName(fileCommand.getFileName());
			fileDefinition = CollectionUtils.getOnlyElement(getReconciliationFileDefinitionService().getReconciliationFileDefinitionListPopulated(fileDefinitionSearchForm));
			//If there is not a fileMapping, then we want to act like the event was never processed, this will send
			//an error to the JMS queue, but will not create an event in integration.
			if (fileDefinition == null) {
				throw new EventNotProcessedException("No file definition found for file with name: " + fileCommand.getFileName());
			}
		}
		else {
			fileDefinition = getReconciliationFileDefinitionService().getReconciliationFileDefinition(fileCommand.getFileDefinitionId());
			AssertUtils.assertNotNull(fileDefinition, "No file definition selected for manual upload!");
		}
		fileCommand.setFileDefinitionId(fileDefinition.getId());
		return fileDefinition;
	}


	private List<ReconciliationRun> getReconciliationRunList(ReconciliationFileDefinition fileDefinition, Date reconciliationDate) {
		List<ReconciliationRun> reconciliationRunList = new ArrayList<>();
		Integer[] definitionIds = fileDefinition.getFileDefinitionMappingList().stream().map(definitionMapping -> definitionMapping.getReconciliationDefinition().getId()).toArray(Integer[]::new);
		for (Integer definitionId : definitionIds) {
			AssertUtils.assertNotNull(definitionId, "Reconciliation definition id passed was null!");
			//Get the fully populated definition by retrieving using the service method; 'activeOnDate' is used to retrieve validate filters, fields, and rules.
			ReconciliationDefinition reconciliationDefinition = getReconciliationDefinitionService().getReconciliationDefinitionPopulated(definitionId, reconciliationDate);
			//Validate to ensure, minimally, the required definition fields are defined. (i.e.: Holding Account, Investment Security).
			ReconciliationDefinitionUtils.validateDefinition(reconciliationDefinition);

			ReconciliationRunSearchForm searchForm = new ReconciliationRunSearchForm();
			searchForm.setDefinitionId(reconciliationDefinition.getId());
			if (!reconciliationDefinition.getType().isCumulativeReconciliation()) {
				searchForm.setRunDate(reconciliationDate);
			}
			ReconciliationRun run = CollectionUtils.getOnlyElement(getReconciliationRunService().getReconciliationRunList(searchForm));
			if (run == null) {
				run = new ReconciliationRun();
				run.setRunDate(reconciliationDate);
				run.setReconciliationDefinition(reconciliationDefinition);
				run.setReconciliationReconcileStatus(getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.NOT_RECONCILED));
				//Save to get an id that will be associated with all objects.
				run = getReconciliationRunService().saveReconciliationRun(run);
			}
			//Reset the run status to 'NOT_RECONCILED' since data is being uploaded for the first time/again
			run.setReconciliationReconcileStatus(getReconciliationStatusService().getReconciliationReconcileStatusByStatusType(ReconciliationReconcileStatusTypes.NOT_RECONCILED));
			//Set the fully populated definition
			run.setReconciliationDefinition(reconciliationDefinition);
			//If this is a singleton run then set the runDate to the previous business day - it will always reflect the latest date.
			if (reconciliationDefinition.getType().isCumulativeReconciliation()) {
				Calendar calendar = getCalendarSetupService().getDefaultCalendar();
				Date updatedRunDate = getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(DateUtils.clearTime(new Date()), calendar.getId()));
				//If the run date for a cumulative rec changes, then we want to reset the excluded and filtered counts for the day.
				if (!DateUtils.isEqualWithoutTime(run.getRunDate(), updatedRunDate)) {
					run.setRunDate(updatedRunDate);
					run.getRunDetails().clearDetailListForCategory(StatusDetail.CATEGORY_ERROR);
					run.getRunDetails().setStatusCount(ReconciliationRun.ERROR_COUNT, 0);
					run.getRunDetails().setStatusCount(ReconciliationRun.EXCLUDED_COUNT, 0);
					run.getRunDetails().setStatusCount(ReconciliationRun.FILTERED_COUNT, 0);
				}
			}
			reconciliationRunList.add(run);
		}
		return reconciliationRunList;
	}


	private Map<Integer, Map<String, ReconciliationRunObject>> populateRunObjectByRunIdMap(ReconciliationServiceContext context) {
		Date importDate = context.getImportDate();
		ReconciliationSource source = context.getSource();
		List<ReconciliationRun> runList = context.getReconciliationRunList();
		ReconciliationFileDefinition fileDefinition = context.getFileDefinition();
		Map<Integer, Map<String, ReconciliationRunObject>> runObjectByRunIdMap = new HashMap<>();
		for (ReconciliationRun run : CollectionUtils.getIterable(runList)) {
			ReconciliationRunObjectSearchForm searchForm = new ReconciliationRunObjectSearchForm();
			searchForm.setRunId(run.getId());
			List<ReconciliationRunObject> runObjectList = getReconciliationRunService().getReconciliationRunObjectList(searchForm);
			if (!CollectionUtils.isEmpty(runObjectList)) {
				Map<String, ReconciliationRunObject> runObjectMap = new HashMap<>();
				for (ReconciliationRunObject runObject : CollectionUtils.getIterable(runObjectList)) {
					String identifier = runObject.getIdentifier();
					//If this run is not a singleton, then we clear the data and errors for the fileDefinition and source, otherwise,
					// we only clear the data and errors for the fileDefinition and source when the runObject importDate matches the current importDate.
					if (!run.getReconciliationDefinition().getType().isCumulativeReconciliation() || DateUtils.isEqualWithoutTime(runObject.getImportDate(), importDate)) {
						//If the object contains data from the source and fileDefinition, then we reset the match and reconcile statuses since we will clear that side of the data and reprocess
						if (!CollectionUtils.isEmpty(BeanUtils.filter(runObject.getDataHolder().getDataObjects(), o -> (o.getFileDefinitionId().equals(fileDefinition.getId()) && o.getSourceId().equals(source.getId()) && DateUtils.isEqualWithoutTime(o.getImportDate(), importDate))))) {
							runObject.setReconciliationMatchStatus(context.getNotMatchedStatus());
							runObject.setReconciliationReconcileStatus(context.getNotReconciledStatus());
						}
						//Now that we reset the status, we want to clear all data, if it exists, for the given fileDefinition and source to prevent duplicates.
						runObject.getDataHolder().setDataObjects(BeanUtils.filter(runObject.getDataHolder().getDataObjects(), o -> !(o.getFileDefinitionId().equals(fileDefinition.getId()) && o.getSourceId().equals(source.getId()) && DateUtils.isEqualWithoutTime(o.getImportDate(), importDate))));
						//Clear errors
						runObject.getDataHolder().setErrors(null);
					}
					runObjectMap.put(identifier, runObject);
				}
				runObjectByRunIdMap.put(run.getId(), runObjectMap);
			}
		}
		return runObjectByRunIdMap;
	}


	////////////////////////////////////////////////////////////////////////////
	////////          Reconciliation Service Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The entry point for all reconciliation data - whether it is manually uploaded or automatically from an external source.
	 * Reconciliation goes through the following basic process:
	 * 1) Initialize Context
	 * 2) Execute Actions
	 * 3) Execute Preprocessors
	 * 4) Process Reconciliation Data (filter, normalize, exclude, aggregate, match)
	 * 5) Save Reconciliation Data
	 */
	@Override
	@Transactional(timeout = 600)
	public Status uploadReconciliationFile(ReconciliationFileCommand fileCommand) {
		long processingStartTime = System.currentTimeMillis();

		long functionStartTime = System.currentTimeMillis();
		ReconciliationServiceContext context = initializeContext(fileCommand);
		LogUtils.info(this.getClass(), "Context Initialization Runtime (ms): " + (System.currentTimeMillis() - functionStartTime));

		// Execute any actions associated with reconciliation type (e.g. copy transactions)
		functionStartTime = System.currentTimeMillis();
		context.getReconciliationRunList().forEach(run -> getReconciliationActionHandler().executeActions(run, context));
		LogUtils.info(this.getClass(), "Actions Runtime (ms): " + (System.currentTimeMillis() - functionStartTime));

		// Check if there are pre-processors defined and execute to update the table data values in place
		functionStartTime = System.currentTimeMillis();
		context.getSimpleTable().process(getReconciliationDefinitionDataPreprocessorList(context.getReconciliationRunList()), true);
		LogUtils.info(this.getClass(), "Preprocessors Runtime (ms): " + (System.currentTimeMillis() - functionStartTime));

		// Process the reconciliation data
		functionStartTime = System.currentTimeMillis();
		processReconciliationData(context);
		LogUtils.info(this.getClass(), "Data Processing Runtime (ms): " + (System.currentTimeMillis() - functionStartTime));

		// Save the reconciliation data
		functionStartTime = System.currentTimeMillis();
		saveReconciliationData(context);
		LogUtils.info(this.getClass(), "Save Runtime (ms): " + (System.currentTimeMillis() - functionStartTime));

		LogUtils.info(getClass(), "Total Processing Runtime (seconds): " + ((System.currentTimeMillis() - processingStartTime) / 1000));
		return Status.ofMessage("Processed " + context.getSimpleTable().getTotalRowCount() + " row(s); Errors: " + context.getReconciliationRunList().stream().mapToInt(run -> run.getRunDetails().getErrorCount()).sum() + "; Filtered: " + context.getFilteredCount());
	}


	/**
	 * Executes all preprocessors configured for the definition and updates the values in place
	 */
	private List<ObjIntConsumer<SimpleTable>> getReconciliationDefinitionDataPreprocessorList(List<ReconciliationRun> reconciliationRunList) {
		return CollectionUtils.asNonNullList(reconciliationRunList).stream()
				.map(ReconciliationRun::getReconciliationDefinition)
				.filter(d -> !CollectionUtils.isEmpty(d.getPreprocessorList()))
				.flatMap(d -> d.getPreprocessorList().stream())
				.sorted(Comparator.comparing(ReconciliationDefinitionPreprocessor::getEvaluationOrder))
				.map(ReconciliationDefinitionPreprocessor::getPreprocessor)
				.map(sb -> getSystemBeanService().getBeanInstance(sb))
				.map(ReconciliationDefinitionDataPreprocessor.class::cast)
				.collect(Collectors.toList());
	}


	/**
	 * Now we iterate the simpleTable, and process each row for each definition if applicable
	 * We grab the row data fresh from the table - this is because a row may be processed by multiple definitions
	 * and those definitions may define different fields, so we can't retain the keys from one definition and expect
	 * the second definition to process properly.
	 */
	private void processReconciliationData(ReconciliationServiceContext context) {
		if (context.getSimpleTable().getTotalRowCount() > 0) {
			//Take a sample of data and do a quick check to ensure all the requiredFields exist
			//This is done prior to populating the runObjectMap to prevent clearing data when a user upload a file to the wrong definition.
			Map<String, Object> sampleRowData = context.getSimpleTable().getObjectValueMap(0);
			List<String> requiredFieldNameList = context.getDefinitionFieldMappingNamesByRunMap().get(context.getReconciliationRunList().get(0).getId());
			if (sampleRowData.keySet().containsAll(requiredFieldNameList)) {
				context.setRunObjectByRunIdMap(populateRunObjectByRunIdMap(context));
			}
		}
		for (int rowIndex = 0; rowIndex < context.getSimpleTable().getTotalRowCount(); rowIndex++) {
			MutableBoolean orphaned = new MutableBoolean(true);
			for (ReconciliationRun run : CollectionUtils.getIterable(context.getReconciliationRunList())) {
				Map<String, Object> rowData = context.getSimpleTable().getObjectValueMap(rowIndex);
				//First, check if all fieldNames defined exist in the raw data, then apply the filter rules.
				//If all fields exist, cull the data to include only defined fields for the source, and process.
				if (processRowData(run, rowData, rowIndex, orphaned, context)) {
					try {
						//Now we only retain the values for applicable fields for this run by definition and source
						rowData.keySet().retainAll(context.getDefinitionFieldMappingNamesByRunMap().get(run.getId()));
						//Import data; process mappings, functions, and conditions; save object;.
						processReconciliationRunObjectData(run, rowData, context);
					}
					catch (Exception e) {
						run.getRunDetails().addError(e.getMessage());
						LogUtils.error(ReconciliationServiceImpl.class, e.getMessage(), e);
					}
					orphaned.setValue(false);
				}
			}
			if (orphaned.booleanValue()) {
				processOrphanedData(context, rowIndex);
			}
		}
	}


	/**
	 * A map {@link ReconciliationServiceContext#getOrphanedObjectStatusMap()} is used to maintain
	 * the reason data is orphaned; it is possible a reason is added to the map for one definition,
	 * but then the item is processed by the next; in this case the reason will still
	 * exist in the map, but will not used since the item was not completely orphaned.
	 * This is why the {@link MutableBoolean} is used to track the orphan status for all runs.
	 */
	private boolean processRowData(ReconciliationRun run, Map<String, Object> rowData, int rowIndex, MutableBoolean orphaned, ReconciliationServiceContext context) {
		boolean returnVal = true;
		Status status = context.getOrphanedObjectStatusMap().get(rowIndex);
		if (status == null) {
			status = new Status();
		}
		ReconciliationDefinition reconciliationDefinition = run.getReconciliationDefinition();
		List<String> requiredFieldNameList = context.getDefinitionFieldMappingNamesByRunMap().get(run.getId());
		if (!rowData.keySet().containsAll(requiredFieldNameList)) {
			status.setStatusTitle((status.getStatusTitle() != null ? status.getStatusTitle() + "; " : "") + "Data orphaned due to missing fields");
			for (String requiredFieldName : CollectionUtils.getIterable(requiredFieldNameList)) {
				if (!rowData.containsKey(requiredFieldName)) {
					status.addError("Missing required field [" + requiredFieldName + "] for definition [" + reconciliationDefinition.getName() + "]");
				}
			}
			context.getOrphanedObjectStatusMap().put(rowIndex, status);
			return false;
		}
		//Initialize the condition rule context data
		//The date is used to allow ro relative date functions from the runDate (e.g. DATE(-1)) see JsonConditionDateEqualsEvaluator
		Map<String, Object> contextData = new HashMap<>();
		contextData.put(JsonConditionEvaluator.CONTEXT_RELATIVE_DATE, context.getRunDate());
		for (ReconciliationFilter filter : CollectionUtils.getIterable(reconciliationDefinition.getReconciliationFilterList())) {
			if (filter.getReconciliationSource().getId().intValue() == context.getSource().getId().intValue()) {
				boolean conditionResult = JsonConditionEvaluatorUtils.evaluateCondition(filter.getConditionRule().getJsonExpression(), rowData, contextData);
				returnVal = conditionResult == filter.isInclude();
				//If the returnVal is ever false, then we return early, otherwise continue processing filters.
				if (!returnVal) {
					//If 'doNotSaveFilteredData' is true, then we want don't want to 'orphan' this data
					// Orphaned data is saved and cared for by the loving folks in operations every day,
					// but if it is explicitly filtered and flagged not to save, then it isn't an orphan,
					// it's just data that nobody cares about, tossed to the curb like the trash it is.
					if (filter.isDoNotSaveFilteredData()) {
						orphaned.setValue(false);
					}
					else {
						status.setStatusTitle((status.getStatusTitle() != null ? status.getStatusTitle() + "; " : "") + "Orphaned data due to filter condition");
						status.addError("Failed filter condition [" + filter.getName() + "] for definition [" + reconciliationDefinition.getName() + "]");
						context.getOrphanedObjectStatusMap().put(rowIndex, status);
					}
					return returnVal;
				}
			}
		}
		return returnVal;
	}


	/**
	 * Called from {@link ReconciliationRunService} at various times (e.g. during matching)
	 * The context values are set using the passed in data, then only the last bit is initialized
	 * using the existing method.  This is to allow the calling code control over the data used
	 * during initialization rather than being driven by an uploaded file.
	 */
	@Override
	public ReconciliationRunObject reprocessReconciliationRunObjectRowData(ReconciliationFileDefinition fileDefinition, ReconciliationRunObject runObject, Map<String, Object> rowData, Map<String, ReconciliationRunObject> runObjectMap) {
		ReconciliationRun run = runObject.getReconciliationRun();

		ReconciliationServiceContext context = new ReconciliationServiceContext();
		context.setFileDefinition(fileDefinition);
		context.setRunDate(run.getRunDate());
		context.setImportDate(runObject.getImportDate());
		context.setReconciliationRunList(Collections.singletonList(run));
		context.setFileDefinition(fileDefinition);

		Map<Integer, Map<String, ReconciliationRunObject>> runObjectByRunIdMap = new HashMap<>();
		runObjectByRunIdMap.put(run.getId(), runObjectMap);
		context.setRunObjectByRunIdMap(runObjectByRunIdMap);
		//Finish initializing
		initializeContext(context);

		//Get the fully populated definition and reset on the run
		ReconciliationDefinition reconciliationDefinition = getReconciliationDefinitionService().getReconciliationDefinitionPopulated(run.getReconciliationDefinition().getId(), run.getRunDate());
		run.setReconciliationDefinition(reconciliationDefinition);
		return processReconciliationRunObjectData(run, rowData, context);
	}


	/**
	 * Processes the reconciliation data:
	 * 1) Normalizes the raw row data based ont he definitionFields and mappings.
	 * 2) Creates a unique identifier for the object and creates the object, or appends the data to an existing object with the same id
	 * -- Executes exclusion rules during creation, the method:
	 * -- {@link #getReconciliationRunObject(ReconciliationRun, Map, Map, ReconciliationServiceContext)} will return NULL if the item has been excluded.
	 * 3) Aggregates data - this will combine {@link DataObject}s from the same source into a single DataObject.
	 * -- {@link ReconciliationDefinitionField#isAggregate()} is used to determine if specific field values should be summed during aggregation
	 * -- If two values in a DataObject differ and are not set to be aggregated, then normalized field will be left blank and the individual values
	 * -- can only be seen by viewing the raw row data stored in the DataObject.
	 * 4) Checks if data is matched (e.g. does it contain a DataObject from both the primary and secondary source) and updates the match status.
	 */
	private ReconciliationRunObject processReconciliationRunObjectData(ReconciliationRun run, Map<String, Object> rowData, ReconciliationServiceContext context) {
		Map<String, Object> normalizedRowData = ReconciliationRunUtils.normalizeRowData(run, rowData, context.getSource());
		ReconciliationRunObject runObject = getReconciliationRunObject(run, normalizedRowData, rowData, context);
		if (runObject != null) {
			aggregateRunObjectData(run, runObject, context);
			if (runObject.isMatched()) {
				//An item could already have been matched, and had another row aggregated, so we only update match status and count if not already matched.
				if (!context.getMatchedStatus().getName().equals(runObject.getReconciliationMatchStatus().getName())) {
					runObject.setReconciliationMatchStatus(context.getMatchedStatus());
					run.getRunDetails().incrementStatusCount(ReconciliationRun.MATCHED_COUNT);
				}
			}
			context.getRunObjectMap(run.getId()).put(runObject.getIdentifier(), runObject);
		}
		return runObject;
	}


	/**
	 * This is a little strange - but we only run exclusions when an existing object is not found
	 * Since if an object with the same identifier exists, then it was not excluded before and so wouldn't again.
	 * <p>
	 * Secondly, because aggregation has not yet been done it is possible that adding a dataObject will result
	 * in two dataObjects for the same source which will cause an error during exclusion - in the case of a new object, there
	 * will only ever be one so exclusion can be run without worrying about multiple dataObjects for the same source
	 * <p>
	 * see {@link DataHolder#getDataObjectListBySource(ReconciliationSource)}
	 */
	private ReconciliationRunObject getReconciliationRunObject(ReconciliationRun run, Map<String, Object> normalizedRowData, Map<String, Object> rowData, ReconciliationServiceContext context) {
		//We need to update the account and security before creating the objectIdentifier in case aliases are applied.
		String accountNumber = updateHoldingAccount(normalizedRowData, context);
		String securityIdentifier = updateInvestmentSecurity(normalizedRowData, context);
		String runObjectIdentifier = ReconciliationRunUtils.createRunObjectIdentifier(run.getReconciliationDefinition().getReconciliationDefinitionFieldList(), normalizedRowData);

		boolean created = false;
		ReconciliationRunObject runObject = context.getRunObjectMap(run.getId()).get(runObjectIdentifier);
		if (runObject == null) {
			runObject = new ReconciliationRunObject();
			runObject.setIdentifier(runObjectIdentifier);
			if (run.getReconciliationDefinition().getType().isCumulativeReconciliation()) {
				runObject.setImportDate(context.getImportDate());
			}
			created = true;
		}
		runObject.setReconciliationMatchStatus(context.getNotMatchedStatus());
		runObject.setReconciliationReconcileStatus(context.getNotReconciledStatus());
		runObject.setReconciliationInvestmentAccount(getReconciliationInvestmentService().getReconciliationInvestmentAccountByNumber(accountNumber));
		runObject.setReconciliationInvestmentSecurity(getReconciliationInvestmentService().getReconciliationInvestmentSecurityByIdentifier(securityIdentifier));
		runObject.setReconciliationRun(run);

		DataObject dataObject = new DataObject();
		dataObject.setImportDate(context.getImportDate());
		dataObject.setFileDefinitionId(context.getFileDefinition().getId());
		dataObject.setSourceId(context.getSource().getId());
		dataObject.setData(normalizedRowData);
		dataObject.getRowData().add(rowData);
		runObject.getDataHolder().getDataObjects().add(dataObject);

		//We only check exclusion assignments for new objects
		if (created) {
			if (excludeRunObject(runObject, context.getSource(), context.getFileDefinition())) {
				return null;
			}
		}
		return runObject;
	}


	/**
	 * Retrieves the most specific exclusion assignment from cache if it exists and orphans the object if excluded.
	 */
	@Override
	public boolean excludeRunObject(ReconciliationRunObject runObject, ReconciliationSource source, ReconciliationFileDefinition fileDefinition) {
		ReconciliationExclusionAssignmentSource exclusionAssignmentSource = new ReconciliationExclusionAssignmentSource(source, runObject.getReconciliationRun().getReconciliationDefinition(), runObject.getReconciliationInvestmentAccount(), runObject.getReconciliationInvestmentSecurity(), runObject.getAccountNumber(), runObject.getSecurityIdentifier(), runObject.getReconciliationRun().getRunDate());
		ReconciliationExclusionAssignment exclusionAssignment = getReconciliationExclusionAssignmentSpecificityCache().getMostSpecificResult(exclusionAssignmentSource);
		boolean excluded = exclusionAssignment != null && !exclusionAssignment.isInclude();
		if (excluded) {
			//Reset the excluded flag - just because there is an assignment, doesn't mean the object has data for the applicable source
			excluded = false;
			//Ensure the object has data for the given source
			List<DataObject> dataObjectList = runObject.getDataHolder().getDataObjectListBySource(source);
			if (!CollectionUtils.isEmpty(dataObjectList)) {
				Status status = Status.ofTitle("Excluded by [" + exclusionAssignment.getLabel() + "]");
				ReconciliationRunOrphanObject orphanObject = new ReconciliationRunOrphanObject();
				orphanObject.setOrphanType(ReconciliationRunOrphanObjectTypes.EXCLUDED);
				orphanObject.setReconciliationDate(runObject.getReconciliationRun().getRunDate());
				orphanObject.setFileDefinition(fileDefinition);
				orphanObject.setDataHolder(runObject.getDataHolder());
				orphanObject.setStatus(status);
				orphanObject.setExclusionAssignment(exclusionAssignment);
				orphanObject.setExcludedRun(runObject.getReconciliationRun());
				//TODO - update associated events with the orphaned object?  Or allow them to be deleted when the original object is deleted later?
				getReconciliationRunService().saveReconciliationRunOrphanObject(orphanObject);
				runObject.getReconciliationRun().getRunDetails().incrementStatusCount(ReconciliationRun.EXCLUDED_COUNT);
				//We actually excluded - mark true
				excluded = true;
			}
		}
		return excluded;
	}


	/**
	 * Aggregation is the process of combining multiple {@link DataObject}s from the same source into a single DataObject.
	 * A {@link ReconciliationRunObject} should only have one DataObject for the primary and secondary {@link ReconciliationSource}
	 */
	private ReconciliationRunObject aggregateRunObjectData(ReconciliationRun run, ReconciliationRunObject runObject, ReconciliationServiceContext context) {
		ReconciliationSource source = context.getSource();
		ReconciliationFileDefinition fileDefinition = context.getFileDefinition();

		List<DataObject> originalDataObjectList = runObject.getDataHolder().getDataObjects();
		//We don't filter on the runDate here because in 'singleton' definitions the runDate may differ from the 'Posting Date' required natural key
		//e.g. data uploaded on a different runDate can be aggregated against data uploaded for the prior runDate.
		List<DataObject> dataObjectList = runObject.getDataHolder().getDataObjects().stream()
				.filter(dataObject -> dataObject.getSourceId().equals(source.getId()))
				.collect(Collectors.toList());

		if (CollectionUtils.getSize(dataObjectList) > 1) {
			DataObject aggregatedDataObject = new DataObject();
			for (DataObject dataObject : CollectionUtils.getIterable(dataObjectList)) {
				for (ReconciliationDefinitionField definitionField : CollectionUtils.getIterable(run.getReconciliationDefinition().getReconciliationDefinitionFieldList())) {
					String definitionFieldName = definitionField.getField().getName();
					DataTypes dataType = ObjectUtils.coalesce(definitionField.getDataType(), definitionField.getField().getDefaultDataType());
					if (dataType != null) {
						Object mergedDataFieldValue = DataTypeNameUtils.convertObjectToDataTypeName(aggregatedDataObject.getData().get(definitionFieldName), dataType.getTypeName());
						Object reconcileDataFieldValue = DataTypeNameUtils.convertObjectToDataTypeName(dataObject.getData().get(definitionFieldName), dataType.getTypeName());
						if (mergedDataFieldValue != null) {
							if (definitionField.isAggregate() && dataType.getTypeName().isNumeric()) {
								//Force to 0 for numeric field
								if (DataTypeNames.INTEGER == dataType.getTypeName()) {
									aggregatedDataObject.getData().put(definitionField.getField().getName(), (Integer) mergedDataFieldValue + (Integer) (reconcileDataFieldValue == null ? 0 : reconcileDataFieldValue));
								}
								else if (DataTypeNames.DECIMAL == dataType.getTypeName()) {
									aggregatedDataObject.getData().put(definitionField.getField().getName(), MathUtils.add((BigDecimal) mergedDataFieldValue, (BigDecimal) (reconcileDataFieldValue == null ? BigDecimal.ZERO : reconcileDataFieldValue)));
								}
							}
							else if (!mergedDataFieldValue.equals(reconcileDataFieldValue)) {
								//If it is not an aggregate field then the value must match from each source in order to set the value at the data level;
								//if not, we update the value to be blank and force the user to view the source data to see the original raw values for each source.
								aggregatedDataObject.getData().put(definitionField.getField().getName(), null);
							}
						}
						else {
							//Value has not been set yet, so set it.
							aggregatedDataObject.getData().put(definitionFieldName, reconcileDataFieldValue);
						}
					}
				}
				aggregatedDataObject.getRowData().addAll(dataObject.getRowData());
			}
			aggregatedDataObject.setImportDate(context.getImportDate());
			aggregatedDataObject.setFileDefinitionId(fileDefinition.getId());
			aggregatedDataObject.setSourceId(fileDefinition.getSource().getId());

			List<DataObject> aggregatedDataList = new ArrayList<>();
			for (DataObject originalDataObject : CollectionUtils.getIterable(originalDataObjectList)) {
				//If data is not from the current source, then add, the rest of the data has been merged and will be added next.
				if (!source.getId().equals(originalDataObject.getSourceId())) {
					aggregatedDataList.add(originalDataObject);
				}
			}
			aggregatedDataList.add(aggregatedDataObject);
			if (CollectionUtils.isEmpty(aggregatedDataList)) {
				try {
					//Purposely throwing and catching an error to log the stack trace.
					//DataObject list should never be empty here.
					throw new RuntimeException("DataObject list should not be empty!");
				}
				catch (Exception e) {
					LogUtils.error(getClass(), e.getMessage(), e);
				}
			}
			runObject.getDataHolder().setDataObjects(aggregatedDataList);
		}
		return runObject;
	}


	/**
	 * Attempts to retrieve a valid holdingAccount based on the accountNumber value.
	 * If not found directly - then it will check for an active alias and update the raw value with the alias if found.
	 */
	private String updateHoldingAccount(Map<String, Object> data, ReconciliationServiceContext context) {
		short sourceId = context.getSource().getId();
		String accountNumber = (String) data.get(ReconciliationField.HOLDING_ACCOUNT_FIELD);
		ReconciliationInvestmentAccount investmentAccount = getReconciliationInvestmentService().getReconciliationInvestmentAccountByNumber(accountNumber);
		// If no account was found, we want to check for any defined aliases and replace with the value of the mapped account defined on the alias.
		// We do this outside the cache because aliases have start/end dates and so we want to ensure that we are only retrieving active aliases.
		if (investmentAccount == null) {
			ReconciliationInvestmentAccountAlias accountAlias = context.getAccountAliasMap().get(sourceId + "_" + accountNumber);
			if (accountAlias != null) {
				investmentAccount = accountAlias.getInvestmentAccount();
				if (investmentAccount != null) {
					accountNumber = (String) BeanUtils.getPropertyValue(investmentAccount, accountAlias.getDisplayProperty());
					data.put(ReconciliationField.HOLDING_ACCOUNT_FIELD, accountNumber);
				}
			}
		}
		return accountNumber;
	}


	/**
	 * Attempts to retrieve a valid investmentSecurity based on the securityIdentifier value.
	 * If not found directly - then it will check for an active alias and update the raw value with the alias if found.
	 */
	private String updateInvestmentSecurity(Map<String, Object> data, ReconciliationServiceContext context) {
		short sourceId = context.getSource().getId();
		String securityIdentifier = (String) data.get(ReconciliationField.INVESTMENT_SECURITY_FIELD);
		ReconciliationInvestmentSecurity investmentSecurity = getReconciliationInvestmentService().getReconciliationInvestmentSecurityByIdentifier(securityIdentifier);
		// If no security was found, we want to check for any defined aliases and replace the value with the display property value
		// of the mapped security defined on the alias.  We do this outside the cache because aliases have start/end dates and so we want
		// to ensure that we are only retrieving active aliases - additionally this allows us to define the display property on the alias.
		if (investmentSecurity == null) {
			ReconciliationInvestmentSecurityAlias securityAlias = context.getSecurityAliasMap().get(sourceId + "_" + securityIdentifier);
			if (securityAlias != null) {
				investmentSecurity = securityAlias.getInvestmentSecurity();
				if (investmentSecurity != null) {
					securityIdentifier = (String) BeanUtils.getPropertyValue(investmentSecurity, securityAlias.getDisplayProperty());
					data.put(ReconciliationField.INVESTMENT_SECURITY_FIELD, securityIdentifier);
				}
			}
		}
		return securityIdentifier;
	}


	/**
	 * Creates and saves and orphaned object.
	 * This is only executed if the data was not processed by any definition and if the fileDefinition is set to save
	 * orphaned data AND if the item was orphaned because of a filter and that filter does not override the
	 * {@link ReconciliationFileDefinition#isSaveOrphanedData()} flag
	 * with its
	 * {@link ReconciliationFilter#isDoNotSaveFilteredData} flag
	 */
	private void processOrphanedData(ReconciliationServiceContext context, int rowIndex) {
		ReconciliationFileDefinition fileDefinition = context.getFileDefinition();
		if (fileDefinition.isSaveOrphanedData()) {
			//If data is not processed by any definition, then it will be saved to the Many to Many table 'ReconciliationSourceDataReconciliationDefinition'
			//This allows ops to investigate if the rows were filtered improperly based on the configuration of the definitions that failed to process it.
			//First create an object, and set source and date
			DataObject dataObject = new DataObject();
			dataObject.setImportDate(context.getImportDate());
			dataObject.setFileDefinitionId(fileDefinition.getId());
			dataObject.setSourceId(fileDefinition.getSource().getId());
			dataObject.setRowData(Collections.singletonList(context.getSimpleTable().getObjectValueMap(rowIndex)));

			//Create a new list since reconcile data was never created for this and add the reconcileData with the source info
			List<DataObject> dataObjects = new ArrayList<>();
			dataObjects.add(dataObject);

			//Now add to the orphaned data object and save.
			ReconciliationRunOrphanObject orphanObject = new ReconciliationRunOrphanObject();
			orphanObject.setOrphanType(ReconciliationRunOrphanObjectTypes.FILTERED);
			orphanObject.setReconciliationDate(context.getImportDate());
			orphanObject.setFileDefinition(fileDefinition);
			orphanObject.getDataHolder().setDataObjects(dataObjects);
			orphanObject.setStatus(context.getOrphanedObjectStatusMap().get(rowIndex));
			getReconciliationRunService().saveReconciliationRunOrphanObject(orphanObject);
			context.setFilteredCount(context.getFilteredCount() + 1);
		}
	}


	/**
	 * Finalizes the reconciliation data load process:
	 * - Updates 'Break Days' (number of consecutive days it has been NOT_RECONCILED)
	 * - Saves all objects in a batch
	 * - Updates overall run stats and saves the runs.
	 */
	private void saveReconciliationData(ReconciliationServiceContext context) {
		List<ReconciliationRunObject> runObjectList = new ArrayList<>(context.getRunObjectList());
		updateReconciliationBreakDays(runObjectList, context.getImportDate());
		getReconciliationRunService().saveReconciliationRunObjectList(runObjectList);
		for (ReconciliationRun run : CollectionUtils.getIterable(context.getReconciliationRunList())) {
			Status runDetails = run.getRunDetails();
			//Set the source as the category to associate the details with on save in the observer
			runDetails.getStatusMap().put(ReconciliationRun.CATEGORY, context.getSource().getName());
			runDetails.setStatusCount(ReconciliationRun.FILTERED_COUNT, runDetails.getStatusCount(ReconciliationRun.FILTERED_COUNT) + context.getFilteredCount());
			runDetails.setStatusCount(ReconciliationRun.ERROR_COUNT, runDetails.getErrorCount());
			getReconciliationRunService().saveReconciliationRun(run);
		}
	}


	/**
	 * Updates the break days count - only ever applies to 'Transaction Reconciliation'
	 */
	private void updateReconciliationBreakDays(List<ReconciliationRunObject> runObjectList, Date importDate) {
		// Retrieve Break Days Map
		if (!CollectionUtils.isEmpty(runObjectList)) {
			List<ReconciliationDefinition> definitionList = runObjectList.stream().map(runObject -> runObject.getReconciliationRun().getReconciliationDefinition()).distinct().collect(Collectors.toList());
			Map<String, Integer> reconciliationBreakDaysMap = new HashMap<>();
			for (ReconciliationDefinition definition : CollectionUtils.getIterable(definitionList)) {
				reconciliationBreakDaysMap.putAll(getReconciliationRunService().getReconciliationBreakDaysMap(definition.getId(), importDate, definition.getType().isCumulativeReconciliation()));
			}
			for (ReconciliationRunObject runObject : CollectionUtils.getIterable(runObjectList)) {
				Integer breakDays = reconciliationBreakDaysMap.get(runObject.getIdentifier());
				// Just set to the previous value as these have not been reconciled yet.
				runObject.setBreakDays(breakDays != null ? breakDays : 0);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public ReconciliationDefinitionService getReconciliationDefinitionService() {
		return this.reconciliationDefinitionService;
	}


	public void setReconciliationDefinitionService(ReconciliationDefinitionService reconciliationDefinitionService) {
		this.reconciliationDefinitionService = reconciliationDefinitionService;
	}


	public ReconciliationStatusService getReconciliationStatusService() {
		return this.reconciliationStatusService;
	}


	public void setReconciliationStatusService(ReconciliationStatusService reconciliationStatusService) {
		this.reconciliationStatusService = reconciliationStatusService;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public ReconciliationFileDefinitionService getReconciliationFileDefinitionService() {
		return this.reconciliationFileDefinitionService;
	}


	public void setReconciliationFileDefinitionService(ReconciliationFileDefinitionService reconciliationFileDefinitionService) {
		this.reconciliationFileDefinitionService = reconciliationFileDefinitionService;
	}


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}


	public ReconciliationActionHandler getReconciliationActionHandler() {
		return this.reconciliationActionHandler;
	}


	public void setReconciliationActionHandler(ReconciliationActionHandler reconciliationActionHandler) {
		this.reconciliationActionHandler = reconciliationActionHandler;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public ReconciliationExclusionAssignmentSpecificityCache getReconciliationExclusionAssignmentSpecificityCache() {
		return this.reconciliationExclusionAssignmentSpecificityCache;
	}


	public void setReconciliationExclusionAssignmentSpecificityCache(ReconciliationExclusionAssignmentSpecificityCache reconciliationExclusionAssignmentSpecificityCache) {
		this.reconciliationExclusionAssignmentSpecificityCache = reconciliationExclusionAssignmentSpecificityCache;
	}
}
