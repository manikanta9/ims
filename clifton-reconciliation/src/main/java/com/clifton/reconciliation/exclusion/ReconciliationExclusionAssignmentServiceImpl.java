package com.clifton.reconciliation.exclusion;

import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconciliation.ReconciliationService;
import com.clifton.reconciliation.definition.ReconciliationDefinitionService;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.exclusion.search.ReconciliationExclusionAssignmentSearchForm;
import com.clifton.reconciliation.exclusion.validation.ReconciliationExclusionAssignmentValidator;
import com.clifton.reconciliation.file.ReconciliationFileDefinition;
import com.clifton.reconciliation.file.ReconciliationFileDefinitionService;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.ReconciliationRunOrphanObject;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.search.ReconciliationRunObjectSearchForm;
import com.clifton.reconciliation.run.search.ReconciliationRunOrphanObjectSearchForm;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import com.clifton.reconciliation.run.util.ReconciliationRunUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * The <code>ReconciliationExclusionAssignmentService</code>
 *
 * @author StevenF
 */
@Service
public class ReconciliationExclusionAssignmentServiceImpl implements ReconciliationExclusionAssignmentService {

	private RunnerHandler runnerHandler;
	private ReconciliationService reconciliationService;
	private ReconciliationRunService reconciliationRunService;
	private ReconciliationDefinitionService reconciliationDefinitionService;
	private ReconciliationFileDefinitionService reconciliationFileDefinitionService;
	private SecurityUserService securityUserService;

	private AdvancedUpdatableDAO<ReconciliationExclusionAssignment, Criteria> reconciliationExclusionAssignmentDAO;


	////////////////////////////////////////////////////////////////////////////
	////////   Reconciliation Exclusion Assignment Business Methods    /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconciliationExclusionAssignment getReconciliationExclusionAssignment(int id) {
		return getReconciliationExclusionAssignmentDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconciliationExclusionAssignment> getReconciliationExclusionAssignmentList(ReconciliationExclusionAssignmentSearchForm searchForm) {
		return getReconciliationExclusionAssignmentDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReconciliationExclusionAssignment saveReconciliationExclusionAssignment(ReconciliationExclusionAssignment exclusionAssignment) {
		exclusionAssignment = getReconciliationExclusionAssignmentDAO().save(exclusionAssignment);
		if (exclusionAssignment.getReconciliationSource() != null && exclusionAssignment.getStartDate() != null) {
			if (!ReconciliationRunUtils.isMultipleRunsPerDefinition(exclusionAssignment.getReconciliationDefinition(), exclusionAssignment.getReconciliationSource(), exclusionAssignment.getStartDate(), null, getReconciliationRunService())) {
				processReconciliationExclusionAssignment(exclusionAssignment.getReconciliationSource().getId(), exclusionAssignment.getStartDate(), true);
			}
		}
		return exclusionAssignment;
	}


	@Override
	@Transactional
	public void deleteReconciliationExclusionAssignment(int id) {

		ReconciliationRunOrphanObjectSearchForm orphanObjectSearchForm = new ReconciliationRunOrphanObjectSearchForm();
		orphanObjectSearchForm.setExclusionAssignmentId(id);
		List<ReconciliationRunOrphanObject> orphanObjectList = getReconciliationRunService().getReconciliationRunOrphanObjectList(orphanObjectSearchForm);

		//take orphan object list and group by ReconciliationRun, then map Orphan to temp object, and concatenate to list.
		Map<ReconciliationRun, List<ReconciliationRunObject>> runMap = orphanObjectList.stream().collect(Collectors.groupingBy(ReconciliationRunOrphanObject::getExcludedRun, Collectors.mapping(this::convertOrphanObject, Collectors.toList())));
		getReconciliationRunService().deleteReconciliationRunOrphanObjectList(orphanObjectList);
		getReconciliationExclusionAssignmentDAO().delete(id);

		runMap.forEach((run, reconciliationRunObjects) -> getReconciliationRunService().reprocessReconciliationRun(run, reconciliationRunObjects, CollectionUtils.getStream(getReconciliationRunService().getReconciliationRunObjectListByRunIdAndReconcileStatusName(run.getId(), ReconciliationReconcileStatusTypes.NOT_RECONCILED.name())).collect(Collectors.toMap(ReconciliationRunObject::getIdentifier, Function.identity())), new Status()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////    Reconciliation Exclusion Assignment Utility Methods    /////////
	////////////////////////////////////////////////////////////////////////////


	private ReconciliationRunObject convertOrphanObject(ReconciliationRunOrphanObject orphanObject) {
		ReconciliationRunObject runObject = new ReconciliationRunObject();
		runObject.setDataHolder(orphanObject.getDataHolder());
		runObject.setReconciliationRun(orphanObject.getExcludedRun());
		return runObject;
	}


	@Override
	public Status processReconciliationExclusionAssignment(short sourceId, Date runDate, boolean synchronous) {
		ReconciliationSource source = getReconciliationDefinitionService().getReconciliationSource(sourceId);
		ValidationUtils.assertNotNull(runDate, "Run date may not be null!");
		ValidationUtils.assertNotNull(source, "Source may not be null!");

		String runId = source.getId() + "_" + DateUtils.fromDate(runDate);
		// If explicitly set; run synchronously, else, run asynchronously
		if (synchronous) {
			Status status = Status.ofMessage("Synchronously running for " + runId);
			doProcessReconciliationExclusions(source, runDate, status);
			if (status.getErrorCount() > 0) {
				throw new RuntimeException(status.getMessageWithErrors());
			}
			return status;
		}

		final StatusHolder statusHolder = new StatusHolder("Scheduled for " + DateUtils.fromDate(runDate));
		Runner runner = new AbstractStatusAwareRunner("RECONCILIATION-EXCLUSION", runId, runDate, statusHolder) {

			@Override
			public void run() {
				try {
					doProcessReconciliationExclusions(source, runDate, statusHolder.getStatus());
				}
				catch (Exception e) {
					getStatus().setMessage("Error processing exclusions for source [" + source.getName() + "]: " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error processing exclusions for source [" + source.getId() + "]", e);
				}
			}
		};
		//Prevents null pointer during migration of exclusion assignments that call this on save.
		if (getRunnerHandler() != null) {
			getRunnerHandler().rescheduleRunner(runner);
		}
		return Status.ofMessage("Started processing exclusions. Processing will be completed shortly.");
	}


	@Override
	@Transactional
	public void approveReconciliationExclusionAssignment(int id) {
		ReconciliationExclusionAssignment exclusionAssignment = getReconciliationExclusionAssignment(id);
		ValidationUtils.assertNotNull(exclusionAssignment, "Reconciliation Assignment cannot be found " + id);
		ValidationUtils.assertNull(exclusionAssignment.getApprovalUser(), "You may not approve an item that has already been approved.");
		SecurityUser user = getSecurityUserService().getSecurityUserCurrent();
		ValidationUtils.assertFalse(exclusionAssignment.getUpdateUserId() == user.getId(),
				"Approver cannot be the same user as the last Reconciliation Exclusion Assignment Editor / Creator.");
		exclusionAssignment.setApprovalUser(user);
		exclusionAssignment.setApprovalDate(new Date());
		//Save with the DAO directly to bypass triggering exclusion assignment processing again.
		DaoUtils.executeWithSpecificObserversDisabled(() -> getReconciliationExclusionAssignmentDAO().save(exclusionAssignment), ReconciliationExclusionAssignmentValidator.class);
	}


	@Override
	@Transactional
	public void unapproveReconciliationExclusionAssignment(int id) {
		ReconciliationExclusionAssignment exclusionAssignment = getReconciliationExclusionAssignment(id);
		ValidationUtils.assertNotNull(exclusionAssignment, "Reconciliation Assignment cannot be found " + id);
		ValidationUtils.assertNotNull(exclusionAssignment.getApprovalUser(), "You may not unapprove an item that has not been approved.");
		exclusionAssignment.setApprovalUser(null);
		exclusionAssignment.setApprovalDate(null);
		//Save with the DAO directly to bypass triggering exclusion assignment processing again.
		DaoUtils.executeWithSpecificObserversDisabled(() -> getReconciliationExclusionAssignmentDAO().save(exclusionAssignment), ReconciliationExclusionAssignmentValidator.class);
	}


	private Status doProcessReconciliationExclusions(ReconciliationSource source, Date runDate, Status status) {
		ReconciliationRunSearchForm reconciliationRunSearchForm = new ReconciliationRunSearchForm();
		reconciliationRunSearchForm.addSearchRestriction("runDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, runDate);
		reconciliationRunSearchForm.setSourceName(source.getName());

		int totalExcludedCount = 0;
		int totalRunObjectCount = 0;
		List<ReconciliationRun> runList = getReconciliationRunService().getReconciliationRunList(reconciliationRunSearchForm);
		for (ReconciliationRun run : CollectionUtils.getIterable(runList)) {
			int excludedCount = 0;
			ReconciliationFileDefinition fileDefinition = null;
			List<ReconciliationRunObject> unmatchedRunObjectList = getUnmatchedRunObjectList(run, source);
			for (ReconciliationRunObject unmatchedRunObject : CollectionUtils.getIterable(unmatchedRunObjectList)) {
				if (fileDefinition == null) {
					fileDefinition = getReconciliationFileDefinitionService().getReconciliationFileDefinition(CollectionUtils.getFirstElementStrict(unmatchedRunObject.getDataHolder().getDataObjectListBySource(source)).getFileDefinitionId());
				}
				if (getReconciliationService().excludeRunObject(unmatchedRunObject, source, fileDefinition)) {
					getReconciliationRunService().deleteReconciliationRunObject(unmatchedRunObject.getId());
					excludedCount++;
				}
			}
			status.addMessage("Excluded " + excludedCount + " object(s) of " + CollectionUtils.getSize(unmatchedRunObjectList) + " for " + run.getLabel());
			updateReconciliationRun(run, source, excludedCount);
			totalExcludedCount += excludedCount;
			totalRunObjectCount += CollectionUtils.getSize(unmatchedRunObjectList);
		}
		status.setMessage("Excluded " + totalExcludedCount + " object(s) of " + totalRunObjectCount + " from " + CollectionUtils.getSize(runList) + " run(s).");
		return status;
	}


	private List<ReconciliationRunObject> getUnmatchedRunObjectList(ReconciliationRun run, ReconciliationSource source) {
		ReconciliationRunObjectSearchForm searchForm = new ReconciliationRunObjectSearchForm();
		searchForm.setRunId(run.getId());
		searchForm.setMatchStatusName(ReconciliationMatchStatusTypes.NOT_MATCHED.name());
		//Retrieve the list and only return unmatched objects with data from the specified source
		return getReconciliationRunService().getReconciliationRunObjectList(searchForm).stream().filter(runObject -> !CollectionUtils.isEmpty(runObject.getDataHolder().getDataObjectListBySource(source))).collect(Collectors.toList());
	}


	/**
	 * Update the run status object to include newly excluded items.
	 * Also evaluate the overall reconcile status (exclusion may result in full reconciliation)
	 */
	private void updateReconciliationRun(ReconciliationRun run, ReconciliationSource source, int excludedCount) {
		run.getRunDetails().setStatusCount(ReconciliationRun.EXCLUDED_COUNT, run.getRunDetails().getStatusCount(ReconciliationRun.EXCLUDED_COUNT) + excludedCount);
		run.getRunDetails().addDetail(source.getName(), new StringBuilder("Excluded ").append(excludedCount).append(" Run Object(s)").toString());
		getReconciliationRunService().saveReconciliationRun(run);
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ReconciliationService getReconciliationService() {
		return this.reconciliationService;
	}


	public void setReconciliationService(ReconciliationService reconciliationService) {
		this.reconciliationService = reconciliationService;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public ReconciliationDefinitionService getReconciliationDefinitionService() {
		return this.reconciliationDefinitionService;
	}


	public void setReconciliationDefinitionService(ReconciliationDefinitionService reconciliationDefinitionService) {
		this.reconciliationDefinitionService = reconciliationDefinitionService;
	}


	public ReconciliationFileDefinitionService getReconciliationFileDefinitionService() {
		return this.reconciliationFileDefinitionService;
	}


	public void setReconciliationFileDefinitionService(ReconciliationFileDefinitionService reconciliationFileDefinitionService) {
		this.reconciliationFileDefinitionService = reconciliationFileDefinitionService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public AdvancedUpdatableDAO<ReconciliationExclusionAssignment, Criteria> getReconciliationExclusionAssignmentDAO() {
		return this.reconciliationExclusionAssignmentDAO;
	}


	public void setReconciliationExclusionAssignmentDAO(AdvancedUpdatableDAO<ReconciliationExclusionAssignment, Criteria> reconciliationExclusionAssignmentDAO) {
		this.reconciliationExclusionAssignmentDAO = reconciliationExclusionAssignmentDAO;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
