package com.clifton.reconciliation.exclusion.validation;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.exclusion.ReconciliationExclusionAssignment;
import com.clifton.reconciliation.exclusion.ReconciliationExclusionAssignmentService;
import com.clifton.reconciliation.exclusion.search.ReconciliationExclusionAssignmentSearchForm;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.validation.ReconciliationValidator;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>ReconciliationExclusionAssignmentValidator</code> provides validation logic for {@link ReconciliationExclusionAssignment}s.
 *
 * @author stevenf on 8/11/2015.
 */
@Component
public class ReconciliationExclusionAssignmentValidator extends ReconciliationValidator<ReconciliationExclusionAssignment> {


	private ReconciliationExclusionAssignmentService reconciliationExclusionAssignmentService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(ReconciliationExclusionAssignment bean, DaoEventTypes config) throws ValidationException {
		// NOTHING HERE - USES METHOD WITH DAO PARAM
	}


	@Override
	public void validate(ReconciliationExclusionAssignment exclusionAssignment, DaoEventTypes config, ReadOnlyDAO<ReconciliationExclusionAssignment> exclusionAssignmentDAO) throws ValidationException {
		if (exclusionAssignment == null) {
			throw new ValidationException("Exclusion assignment may not be null.");
		}

		//Validate required fields for the exclusion assignment
		ReconciliationSource source = exclusionAssignment.getReconciliationSource();
		ReconciliationDefinition definition = exclusionAssignment.getReconciliationDefinition();
		ReconciliationInvestmentAccount investmentAccount = exclusionAssignment.getReconciliationInvestmentAccount();
		ReconciliationInvestmentSecurity investmentSecurity = exclusionAssignment.getReconciliationInvestmentSecurity();
		if (source == null || (investmentAccount == null && exclusionAssignment.getInvalidAccountNumber() == null)) {
			throw new ValidationException("You must specify at least a source and account to exclude!");
		}

		//Restrict deletion if any active runs are associated
		if (config.isDelete()) {
			catchMultipleRunsPerDefinition("You may not delete an exclusion assignment when it has multiple runs associated with the definition.", definition, source, exclusionAssignment.getStartDate(), exclusionAssignment.getEndDate());
		}

		//On an insert or update simply reject an assignment if modified to overlap any other applicable assignments
		if (config.isInsert() || config.isUpdate()) {
			ReconciliationExclusionAssignmentSearchForm searchForm = new ReconciliationExclusionAssignmentSearchForm();
			searchForm.setSourceId(source.getId());
			searchForm.setDefinitionId(definition != null ? definition.getId() : null);
			searchForm.setAccountId(investmentAccount != null ? investmentAccount.getId() : null);
			searchForm.setSecurityId(investmentSecurity != null ? investmentSecurity.getId() : null);
			searchForm.setInvalidAccountNumber(exclusionAssignment.getInvalidAccountNumber());
			searchForm.setInvalidSecurityIdentifier(exclusionAssignment.getInvalidSecurityIdentifier());
			searchForm.setInclude(exclusionAssignment.isInclude());
			List<ReconciliationExclusionAssignment> existingExclusionAssignmentList = getReconciliationExclusionAssignmentService().getReconciliationExclusionAssignmentList(searchForm)
					.stream().filter(mapping -> !mapping.getId().equals(exclusionAssignment.getId())).collect(Collectors.toList());
			for (ReconciliationExclusionAssignment existingExclusionAssignment : CollectionUtils.getIterable(existingExclusionAssignmentList)) {
				if (!exclusionAssignment.equals(existingExclusionAssignment) && DateUtils.isOverlapInDates(exclusionAssignment.getStartDate(), exclusionAssignment.getEndDate(), existingExclusionAssignment.getStartDate(), existingExclusionAssignment.getEndDate())) {
					throw new ValidationException("An exclusion assignment already exists with an overlapping date range.  Please either edit the existing assignment, or end it and start a new one.");
				}
			}

			if (config.isInsert()) {
				//Validate there is only one run per definition for the source/data of the assignment being created or updated (not utilizing endDate)
				//We ignore endDate because we don't want to allow insertion of an exclusion assignment to affect historical runs.
				catchMultipleRunsPerDefinition(exclusionAssignment.getReconciliationDefinition(), exclusionAssignment.getReconciliationSource(), exclusionAssignment.getStartDate(), null);
			}
			else {
				//If all other validation has passed, grab the existing exclusion assignment and do additional validation
				ReconciliationExclusionAssignment existingExclusionAssignment = getReconciliationExclusionAssignmentService().getReconciliationExclusionAssignment(exclusionAssignment.getId());
				//If the anything other than endDate was modified then reject the update if it affects multiple runs (again regardless of endDate)
				if (CollectionUtils.getSize(CoreCompareUtils.getNoEqualPropertiesWithSetters(exclusionAssignment, existingExclusionAssignment, false, "endDate")) > 0) {
					Date startDate = DateUtils.isDateBefore(exclusionAssignment.getStartDate(), existingExclusionAssignment.getStartDate(), false) ? exclusionAssignment.getStartDate() : existingExclusionAssignment.getStartDate();
					catchMultipleRunsPerDefinition("You may not modify any field other than 'End Date' on an evaluator that applies to multiple runs!", existingExclusionAssignment.getReconciliationDefinition(), exclusionAssignment.getReconciliationSource(), startDate, null);
				}
				//When setting the end date, we just want to check that there are no runs after the endDate (so we pass endDate as the startDate to the function)
				if (!DateUtils.isEqualWithoutTime(exclusionAssignment.getEndDate(), existingExclusionAssignment.getEndDate())) {
					catchSingleRunPerDefinition("You may not update when there are existing runs after the end date!", exclusionAssignment.getReconciliationDefinition(), exclusionAssignment.getReconciliationSource(), exclusionAssignment.getEndDate(), null);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconciliationExclusionAssignmentService getReconciliationExclusionAssignmentService() {
		return this.reconciliationExclusionAssignmentService;
	}


	public void setReconciliationExclusionAssignmentService(ReconciliationExclusionAssignmentService reconciliationExclusionAssignmentService) {
		this.reconciliationExclusionAssignmentService = reconciliationExclusionAssignmentService;
	}
}
