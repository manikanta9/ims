Clifton.reconciliation.run.EditReconciliationNotesWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'View Reconciliation Notes',
	iconCls: 'pencil',
	height: 450,
	width: 950,
	modal: true,
	items: [
		{
			xtype: 'panel',
			items: [
				{
					xtype: 'formpanel',
					url: 'reconciliationRunObject.json',
					labelWidth: 150,
					items: [
						{fieldLabel: 'Identifier', name: 'identifier', xtype: 'displayfield'},
						{fieldLabel: 'Holding Account Number', name: 'reconciliationInvestmentAccount.accountNumber', xtype: 'displayfield'},
						{fieldLabel: 'Holding Account Name', name: 'reconciliationInvestmentAccount.accountName', xtype: 'displayfield'},
						{fieldLabel: 'Investment Security', name: 'reconciliationInvestmentSecurity.securitySymbol', xtype: 'displayfield'},
						{fieldLabel: 'Match Status', name: 'reconciliationMatchStatus.label', xtype: 'displayfield'},
						{fieldLabel: 'Reconcile Status', name: 'reconciliationReconcileStatus.label', xtype: 'displayfield'}
					]
				},
				{
					xtype: 'gridpanel',
					height: 300,
					name: 'reconciliationRunObject',
					readOnly: true,
					title: 'Reconciliation Notes',
					getLoadParams: function() {
						return {'id': this.getWindow().defaultData.runObjectId};
					},
					//TODO - I don't think a lot of this is necessary anymore - but don't want to touch it right now.
					createStore: function(fields) {
						const url = `${this.name}.json?id=${this.getWindow().defaultData.runObjectId}`;
						const recObject = TCG.data.getData(url, this);
						const dataObjectList = recObject.dataHolder.dataObjects;
						const primarySource = recObject.reconciliationRun.reconciliationDefinition.primarySource;
						const secondarySource = recObject.reconciliationRun.reconciliationDefinition.secondarySource;

						const noteList = [];
						// get the source value(s)
						const sourceList = dataObjectList.map(dataObject => dataObject.sourceId)
								.distinct()
								.sort((one, two) => (one === primarySource.id || two === primarySource.id) ? -1 : 1)
								.map(sourceId => {
									if (sourceId === primarySource.id) {
										return primarySource.label;
									}
									if (sourceId === secondarySource.id) {
										return secondarySource.label;
									}
								});
						const globalSourceLabel = sourceList.join('/');

						// build the note detail configuration
						const parentNoteList = recObject.dataHolder.notes;
						if (parentNoteList && parentNoteList.length > 0) {
							parentNoteList.forEach(note => {
								const noteDetail = {
									identifier: recObject.identifier,
									source: {
										label: note.sourceId === primarySource.id ? primarySource.label : note.sourceId === secondarySource.id ? secondarySource.label : globalSourceLabel
									},
									parent: true,
									note: note
								};
								noteList.push(noteDetail);
							});
						}
						this.viewConfig = {
							enableGrouping: false
						};

						// set the sort info
						const sortInfo = {sortDirection: 'DESC'};
						const column = this.columns.find(col => col.defaultSortColumn);
						if (!!column) {
							sortInfo.sortColumn = column.dataIndex;
							if (!!column.defaultSortDirection) {
								sortInfo.sortDirection = column.defaultSortDirection;
							}
						}

						// apply default sort
						if (!!sortInfo.sortColumn) {
							noteList.sort(function(o1, o2) {
								const firstValue = TCG.getValue(sortInfo.sortColumn, o1);
								const secondValue = TCG.getValue(sortInfo.sortColumn, o2);
								return firstValue - secondValue;
							});
							if (sortInfo.sortDirection === 'ASC') {
								noteList.reverse();
							}
						}

						// create the store
						const storeConf = {
							fields: fields,
							data: {
								data: noteList
							}
						};

						if (this.storeRoot) {
							storeConf.root = this.storeRoot;
						}
						if (this.remoteSort) {
							storeConf.forceRemoteSort = this.remoteSort;
						}
						let storeClass = TCG.data.JsonStore;
						if (this.groupField) {
							storeClass = TCG.data.GroupingStore;
							storeConf.groupField = this.groupField;
							storeConf.remoteSort = this.remoteSort || false;
						}
						return new storeClass(storeConf);
					},
					columns: [
						{header: 'Identifier', width: 150, dataIndex: 'identifier', hidden: false},
						{header: 'Source', width: 100, dataIndex: 'source.label'},
						{header: 'Date', width: 150, dataIndex: 'note.createDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{
							header: 'User', width: 125, dataIndex: 'note.createUserId',
							renderer: Clifton.security.renderSecurityUser
						},
						{header: 'Type', width: 175, dataIndex: 'note.type'},
						{
							header: 'Note', width: 250, dataIndex: 'note.note', renderer: function(value, metadata, record, rowIndex, colIndex, store) {
								return String.format('<span ext:qtip="{0}">{1}</span>', value, value);
							}
						}
					]
				}  // end gridpanel
			] // end formpanel items
		} // end formpanel
	]
});
