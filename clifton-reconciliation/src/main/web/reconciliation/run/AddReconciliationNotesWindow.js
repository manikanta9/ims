Clifton.reconciliation.run.AddReconciliationNotesWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Add Reconciliation Note',
	iconCls: 'pencil',
	height: 250,
	width: 550,
	modal: true,
	items: [{
		xtype: 'formpanel',
		labelWidth: 120,
		loadValidation: false,
		instructions: 'Add a note to associate with this action.',
		focusOnFirstField: function() {
			// Focus on the note field; use reduced focus delay
			this.getForm().findField('note').focus(false, 200);
		},
		items: [
			{
				fieldLabel: 'Common Notes', xtype: 'combo', name: 'commonNote', url: 'systemNoteListForEntityFind.json', displayField: 'text',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						combo.store.baseParams = {
							noteTypeName: 'Common Notes',
							entityTableName: 'ReconciliationDefinition',
							fkFieldId: TCG.getParentFormPanel(combo).getWindow().defaultData.definitionId
						};
					},
					select: function(combo, record, index) {
						const note = TCG.getParentFormPanel(combo).getForm().findField('note');
						note.setValue(record.json.text);

					}
				}
			},
			{fieldLabel: 'Note', name: 'note', xtype: 'textarea', height: 100, allowBlank: false}
		],
		getSaveParams: function() {
			return this.getDefaultData(this.getWindow());
		},
		getSaveURL: function() {
			return this.getWindow().params.saveUrl;
		},
		listeners: {
			afterload: function(formPanel) {
				if (this.getWindow().onCloseEvent) {
					this.getWindow().onCloseEvent(this.getWindow());
				}
				else {
					const origGridPanel = this.getWindow().openerCt;
					//Tell the grid to maintain scrollbar position
					origGridPanel.grid.getView().holdPosition = true;
					// origGridPanel.reloadGrids(origGridPanel);
					origGridPanel.reload();
					//Also reload the run window to update statuses there.
					if (origGridPanel.getWindow().openerCt.reload) {
						origGridPanel.getWindow().openerCt.reload();
					}
				}
			}
		}
	}]
});
