Clifton.reconciliation.run.status.ReconciliationReconcileStatusWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Reconciliation Reconcile Status',
	iconCls: 'bank',
	width: 750,
	height: 450,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		url: 'reconciliationReconcileStatus.json',
		instructions: 'A reconcile status defines the status of reconciliation data.',
		items: [
			{fieldLabel: 'Status Name', name: 'name', xtype: 'textfield'},
			{fieldLabel: 'Status Label', name: 'label', xtype: 'textfield'},
			{fieldLabel: 'Status Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Sort Order', name: 'sortOrder', xtype: 'textfield'},
			{fieldLabel: 'Reconciled', name: 'reconciled', xtype: 'checkbox'},
			{fieldLabel: 'Manual', name: 'manual', xtype: 'checkbox'}
		]
	}]
});
