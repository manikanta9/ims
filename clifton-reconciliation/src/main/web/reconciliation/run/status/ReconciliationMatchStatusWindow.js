Clifton.reconciliation.run.status.ReconciliationMatchStatusWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation Match Status',
	iconCls: 'bank',
	width: 750,
	height: 450,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		url: 'reconciliationMatchStatus.json',
		instructions: 'A match status defines the status of matched data.',
		items: [
			{fieldLabel: 'Status Name', name: 'name', xtype: 'textfield'},
			{fieldLabel: 'Status Label', name: 'label', xtype: 'textfield'},
			{fieldLabel: 'Status Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Sort Order', name: 'sortOrder', xtype: 'textfield'},
			{fieldLabel: 'Matched', name: 'matched', xtype: 'checkbox'},
			{fieldLabel: 'Manual', name: 'manual', xtype: 'checkbox'}
		]
	}]
});
