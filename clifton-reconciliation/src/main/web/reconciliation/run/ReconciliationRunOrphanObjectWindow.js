Clifton.reconciliation.run.ReconciliationRunOrphanObjectWindow = Ext.extend(TCG.app.Window, {
	id: 'reconciliationRunOrphanObjectWindow',
	iconCls: 'user',
	title: 'Reconciliation Orphaned Object',
	width: 1300,
	height: 700,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'Reconciliation Run Orphan Objects contain data from a given file definition that was not processed by any reconciliation definition.',
		url: 'reconciliationRunOrphanObject.json?requestedMaxDepth=6',
		table: 'ReconciliationRunOrphanObject',
		items: [
			{fieldLabel: 'Reconciliation Source', name: 'fileDefinition.source.name', xtype: 'linkfield', detailIdField: 'fileDefinition.source.id', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationSourceWindow'},
			{fieldLabel: 'File Definition Name', name: 'fileDefinition.name', xtype: 'linkfield', detailIdField: 'fileDefinition.id', detailPageClass: 'Clifton.reconciliation.file.ReconciliationFileDefinitionWindow'},
			{fieldLabel: 'Orphan Type', name: 'orphanType', xtype: 'displayfield'},
			{
				xtype: 'formgrid',
				height: 150,
				autoHeight: false,
				heightResized: true,
				title: 'Errors',
				storeRoot: 'status.errorList',
				readOnly: true,
				columnsConfig: [
					{header: 'Category', width: 200, dataIndex: 'category'},
					{header: 'Note', width: 1000, dataIndex: 'note'}
				]
			},
			{xtype: 'label', html: '<br />'},
			{
				xtype: 'panel',
				layout: 'column',
				labelWidth: 200,
				title: 'Raw Data',
				items: []
			}
		],
		listeners: {
			afterload: function() {
				//Get the rowData and display in read-only mode using the jsonEditor
				const rowData = this.form.formValues.dataHolder.dataObjects[0].rowData[0];
				const div = document.createElement('div');
				div.setAttribute('style', 'height: 325px; overflow-y:auto;');
				this.body.appendChild(div);
				this.jsonEditor = new JSONEditor(div, {name: 'rowData', mode: 'view'}, rowData);
				this.doLayout();
			}
		}
	}]
});
