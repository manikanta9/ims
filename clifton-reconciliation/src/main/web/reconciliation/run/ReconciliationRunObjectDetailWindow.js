Clifton.reconciliation.run.ReconciliationRunObjectDetailWindow = Ext.extend(TCG.app.CloseWindow, {
	titlePrefix: 'Reconciliation Run Object',
	iconCls: 'reconcile',
	height: 800,
	width: 1500,

	items: [{
		xtype: 'panel',
		items: [
			{
				xtype: 'formpanel',
				url: 'reconciliationRunObject.json',
				name: 'reconciliationRunObjectDetailsForm',
				labelFieldName: 'id',
				loadValidation: false,
				items: [{
					xtype: 'panel',
					layout: 'column',
					items: [
						{
							columnWidth: .40,
							layout: 'form',
							labelWidth: 75,
							items: [
								{fieldLabel: 'ID', name: 'id', detailIdField: 'id', xtype: 'displayfield'},
								{fieldLabel: 'Identifier', name: 'identifier', xtype: 'displayfield'},
								{fieldLabel: 'Run', name: 'reconciliationRun.label', detailIdField: 'reconciliationRun.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.run.ReconciliationRunWindow'}
							]
						},
						{
							columnWidth: .40,
							layout: 'form',
							labelWidth: 120,
							items: [
								{fieldLabel: 'Account Number', name: 'reconciliationInvestmentAccount.accountNumber', detailIdField: 'reconciliationInvestmentAccount.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentAccountWindow'},
								{fieldLabel: 'Account Name', name: 'reconciliationInvestmentAccount.accountName', detailIdField: 'reconciliationInvestmentAccount.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentAccountWindow'},
								{fieldLabel: 'Security Symbol', name: 'reconciliationInvestmentSecurity.securitySymbol', detailIdField: 'reconciliationInvestmentSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentSecurityWindow'}
							]
						},
						{
							columnWidth: .20,
							layout: 'form',
							labelWidth: 120,
							items: [
								{fieldLabel: 'Match Status', name: 'reconciliationMatchStatus.label', detailIdField: 'reconciliationMatchStatus.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.run.status.ReconciliationMatchStatusWindow'},
								{fieldLabel: 'Reconciliation Status', name: 'reconciliationReconcileStatus.label', detailIdField: 'reconciliationReconcileStatus.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.run.status.ReconciliationReconcileStatusWindow'},
								{fieldLabel: 'Reconciled On', name: 'reconciledOnSource.shortName', detailIdField: 'reconciledOnSource.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationSourceWindow'}
							]
						}
					]
				}]
			},


			{
				height: 300,
				xtype: 'tabpanel',
				items: [{
					title: 'Object Details',
					items: [{
						idProperty: 'explodedId',
						additionalPropertiesToRequest: 'explodedId|dataObject|reconciliationInvestmentAccountId|reconciliationInvestmentSecurityId',
						name: 'reconciliationRunObjectDetailExplodedListFind',
						xtype: 'reconciliation-run-object-detail-grid',
						instructions: 'Displays row data for the selected object.',
						gridConfig: {
							listeners: {
								'rowclick': function(grid, rowIndex, evt) {
									const row = grid.store.data.items[rowIndex];
									grid.ownerGridPanel.showSourceData(row);
								}
							}
						},
						showSourceData: function(sourceData) {
							const formPanel = TCG.getChildByName(this.getWindow(), 'myFormPanel');
							const jsonDivId = 'jsonSourceData-' + this.getWindow().getMainFormId();
							let jsonDiv = document.getElementById(jsonDivId);
							if (jsonDiv === null) {
								jsonDiv = document.createElement('div');
								jsonDiv.setAttribute('id', jsonDivId);
								jsonDiv.setAttribute('style', 'height: 340px; width:99%; overflow-y:auto;');
								formPanel.body.appendChild(jsonDiv);
							}
							//Clear the div contents before creating the new editor
							jsonDiv.innerHTML = '';
							const jsonEditor = new JSONEditor(jsonDiv, {name: 'rowData', mode: 'view', sortObjectKeys: true}, sourceData.json.dataObject.rowData);
							jsonEditor.expandAll();
							formPanel.doLayout();
						}
					}]
				}, {
					title: 'Object History',
					items: [{
						idProperty: 'explodedId',
						additionalPropertiesToRequest: 'explodedId|dataObject|reconciliationInvestmentAccountId|reconciliationInvestmentSecurityId',
						name: 'reconciliationRunObjectDetailExplodedListFind',
						xtype: 'reconciliation-run-object-detail-grid',
						instructions: 'Displays historical row data for the selected object.',
						gridConfig: {
							listeners: {
								'rowclick': function(grid, rowIndex, evt) {
									const row = grid.store.data.items[rowIndex];
									grid.ownerGridPanel.showSourceData(row);
								}
							}
						},
						showSourceData: function(sourceData) {
							console.log(sourceData.json);
							const formPanel = TCG.getChildByName(this.getWindow(), 'myFormPanel');
							const jsonDivId = 'jsonSourceData-' + this.getWindow().getMainFormId();
							let jsonDiv = document.getElementById(jsonDivId);
							if (jsonDiv === null) {
								jsonDiv = document.createElement('div');
								jsonDiv.setAttribute('id', jsonDivId);
								jsonDiv.setAttribute('style', 'height: 340px; width:99%; overflow-y:auto;');
								formPanel.body.appendChild(jsonDiv);
							}
							//Clear the div contents before creating the new editor
							jsonDiv.innerHTML = '';
							const jsonEditor = new JSONEditor(jsonDiv, {name: 'rowData', mode: 'view', sortObjectKeys: true}, sourceData.json.dataObject.rowData);
							jsonEditor.expandAll();
							formPanel.doLayout();
						},
						getColumnOverrides: function() {
							return [{dataIndex: 'runDate', hidden: false}];
						},
						getLoadParams: function(firstLoad) {
							const params = {
								id: this.getWindow().defaultData.reconciliationRunObjectId,
								runId: this.getWindow().defaultData.id,
								dataView: this.getWindow().defaultData.dataView,
								showHistory: true
							};
							return params;
						}
					}]
				}, {
					title: 'Modifiers',
					items: [{
						xtype: 'panel',
						items: [{
							height: 37,
							labelWidth: 200,
							xtype: 'formpanel',
							name: 'runObjectModifiers',
							getDefaultData: function(win) {
								if (win.getMainFormId()) {
									return TCG.data.getDataPromise('reconciliationRunObjectDetailListFind.json', this, {params: {id: win.defaultData.reconciliationRunObjectId, runId: win.defaultData.id, dataView: win.defaultData.dataView}})
											.then(async function(detailList) {
												if (detailList && detailList.rows && detailList.rows[0].modifierText) {
													const modifiers = JSON.parse(detailList.rows[0].modifierText);
													const defaultData = {};
													if (modifiers.accountAlias) {
														defaultData.accountAlias = modifiers.accountAlias;
													}
													if (modifiers.securityAlias) {
														defaultData.securityAlias = modifiers.securityAlias;
													}
													if (modifiers.evaluator) {
														defaultData.evaluator = modifiers.evaluator;
													}
													return defaultData;
												}
											});
								}
							},
							items: [
								{
									xtype: 'panel',
									layout: 'column',
									items: [
										{
											columnWidth: .33,
											layout: 'form',
											labelWidth: 160,
											items: [
												{fieldLabel: 'Investment Account Alias', labelStyle: 'font-weight:bold;', name: 'accountAlias.label', detailIdField: 'accountAlias.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentAccountAliasWindow'}
											]
										},
										{
											columnWidth: .33,
											layout: 'form',
											labelWidth: 160,
											items: [
												{fieldLabel: 'Investment Security Alias', labelStyle: 'font-weight:bold;', name: 'securityAlias.label', detailIdField: 'securityAlias.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentSecurityAliasWindow'}
											]
										},
										{
											columnWidth: .330,
											layout: 'form',
											labelWidth: 160,
											items: [
												{fieldLabel: 'Reconciliation Evaluator', labelStyle: 'font-weight:bold;', name: 'evaluator.label', detailIdField: 'evaluator.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.definition.rule.ReconciliationEvaluatorWindow'}
											]
										}
									]
								}
							]
						}, {
							flex: 1,  // for the panel vbox layout so this section gets resized with the window - the top section (formpanel) will remain the same
							xtype: 'tabpanel',
							items: [{
								title: 'Adjustments',
								items: [{
									name: 'reconciliationEventObjectListFind',
									height: 500,
									xtype: 'gridpanel',
									instructions: 'A list of all Simple Journal Events in the system.  Simple Journal Events are used to create and post cash adjustment, cash contribution journals, etc.',
									getLoadParams: function(firstLoad) {
										const params = {runObjectOrParentRunObjectId: this.getWindow().getMainFormId(), excludeDataTypeCategory: 'ACCOUNT'};
										return params;
									},
									columns: [
										{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
										{header: 'Run Object', width: 40, dataIndex: 'runObject.id', type: 'int', useNull: true, hidden: true, filter: {searchFieldName: 'runObjectId'}},
										{header: 'Parent Run Object', width: 50, dataIndex: 'parentRunObject.id', type: 'int', useNull: true, hidden: true, filter: {searchFieldName: 'parentRunObjectId'}},
										{header: 'Event Category', width: 50, dataIndex: 'eventObjectDataType.eventObjectCategory', hidden: true, filter: {searchFieldName: 'dataTypeCategory', type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.reconciliation.event.ReconciliationEventObjectCategories}}},
										{header: 'Event Type', width: 50, dataIndex: 'eventObjectDataType.name', filter: {type: 'combo', searchFieldName: 'dataTypeId', url: 'reconciliationEventObjectDataTypeListFind.json'}},
										{header: 'Holding Account', width: 40, dataIndex: 'eventObjectData.holdingAccountNumber', filter: {searchFieldName: 'holdingAccountNumber'}},
										{header: 'Description', width: 100, dataIndex: 'eventObjectData.description', hidden: true, filter: {searchFieldName: 'description'}},
										{header: 'Amount', width: 40, dataIndex: 'eventObjectData.amount', type: 'float', useNull: true, filter: {searchFieldName: 'amount'}},
										{header: 'Event Note', width: 75, dataIndex: 'eventObjectData.eventNote', filter: {searchFieldName: 'eventNote'}},
										{
											header: 'Status', width: 35, dataIndex: 'status', filter: {type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.reconciliation.event.ReconcilaitionEventObjectStatuses}}
										},
										{header: 'Status Message', width: 100, dataIndex: 'statusMessage'},
										{
											header: 'Original Transaction Date', width: 50, dataIndex: 'eventObjectData.originalTransactionDate', hidden: true,
											renderer: function(v, metaData, r) {
												return v ? v.format('m/d/Y') : undefined;
											}
										},
										{
											header: 'Transaction Date', width: 40, dataIndex: 'eventObjectData.transactionDate', hidden: true, filter: {searchFieldName: 'transactionDate'},
											renderer: function(v, metaData, r) {
												return v ? v.format('m/d/Y') : undefined;
											}
										},
										{
											header: 'Settlement Date', width: 40, dataIndex: 'eventObjectData.settlementDate', filter: {searchFieldName: 'settlementDate'},
											renderer: function(v, metaData, r) {
												return v ? v.format('m/d/Y') : undefined;
											}
										},
										{
											header: 'Event Date', width: 40, dataIndex: 'eventObjectData.eventDate', filter: {searchFieldName: 'eventDate'},
											renderer: function(v, metaData, r) {
												return v ? v.format('m/d/Y') : undefined;
											}
										}
									],
									editor: {
										drillDownOnly: true,
										detailPageClass: 'Clifton.reconciliation.event.ReconciliationEventObjectWindow'
									}
								}]
							}]
						}]
					}]
				}] // end tabpanel items
			},

			{
				name: 'myFormPanel',
				xtype: 'formpanel'
			}
		] // end formpanel items
	}],
	getPrimarySourceName: function() {
		return this.defaultData.reconciliationDefinition.primarySource.shortName;
	},
	getSecondarySourceName: function() {
		return this.defaultData.reconciliationDefinition.secondarySource.shortName;
	}
});
