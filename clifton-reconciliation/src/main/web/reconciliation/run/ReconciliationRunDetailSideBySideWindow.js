Clifton.reconciliation.run.ReconciliationRunDetailSideBySideWindow = Ext.extend(Clifton.reconciliation.run.ReconciliationRunDetailWindow, {
	height: 725,
	getDetailPageId: function(grid, row) {
		return row.id + '_Side/Side';
	},
	additionalItems: [{
		flex: 1,  // for the panel vbox layout so this section gets resized with the window - the top section (formpanel) will remain the same
		xtype: 'tabpanel',
		items: [
			{
				title: 'Reconciliation Data',
				items: [{
					xtype: 'reconciliation-run-detail-grid',
					dataView: 'Side/Side'
				}]
			}
		]
	}]
})
;
