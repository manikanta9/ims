Clifton.reconciliation.run.ReconciliationRunDetailWindowSelector = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'reconciliationRun.json?requestedMaxDepth=4',
	openEntityWindow: function(config, entity) {
		const win = this;
		let parentDefinitionPromise;
		let className = 'Clifton.reconciliation.run.ReconciliationRunDetailTopBottomWindow';
		if (entity
			&& entity.reconciliationDefinition
			&& entity.reconciliationDefinition.type
			&& TCG.isEquals('Cash Reconciliation', entity.reconciliationDefinition.type.name)) {
			const parentDefinitionUrl = 'reconciliationDefinitionByParent.json?parentId=' + entity.reconciliationDefinition.id;
			parentDefinitionPromise = TCG.data.getDataPromise(parentDefinitionUrl, this)
				.then(function(parentDefinition) {
					if (parentDefinition) {
							className = 'Clifton.reconciliation.run.ReconciliationRunDetailDualViewWindow';
						}
						else {
							className = 'Clifton.reconciliation.run.ReconciliationRunDetailSideBySideWindow';
						}
					});
		}
		return Promise.resolve(parentDefinitionPromise).then(function(val) {
			if (config.defaultData.dataView) {
				className = 'Clifton.reconciliation.run.ReconciliationRunDetailTopBottomWindow';
				if (config.defaultData.dataView === 'Side/Side') {
					className = 'Clifton.reconciliation.run.ReconciliationRunDetailSideBySideWindow';
				}
			}
			//Generate unique cmpId for window.
			config.id = className + '_' + config.params.id;
			entity.reconcileStatusName = config.params.reconcileStatusName;
			win.doOpenEntityWindow(config, entity, className);
		});
	}
});
