Clifton.reconciliation.run.ReconciliationRunDetailDualViewWindow = Ext.extend(Clifton.reconciliation.run.ReconciliationRunDetailWindow, {
	getDetailPageId: function(grid, row) {
		return row.id + '_Dual';
	},
	additionalItems: [{
		layout: 'border',  // border layout so we can resize the grids - must have an item with region:center defined which is what will be resized
		align: 'stretch',  // for the panel vbox layout so the items fill the width
		flex: 1,  // for the panel vbox layout so this section gets resized with the window - the top section (formpanel) will remain the same
		items: [{
			layout: 'fit',
			region: 'center', // for the border layout - this item gets resized with the window
			items: [{
				xtype: 'tabpanel',
				items: [
					{
						title: 'Reconciliation Data',
						items: [{
							xtype: 'reconciliation-run-detail-grid',
							dataView: 'Side/Side',
							//Do not add gridSummary plugin to cash window.
							plugins: '',
							getLoadURL: function() {
								return 'reconciliationRunObjectDetailListFind.json';
							},
							getRowSelectionModel: function() {
								const win = this.getWindow();
								if (typeof this.rowSelectionModel != 'object') {
									this.singleSelect = this.rowSelectionModel !== 'multiple';
									this.rowSelectionModel = new Ext.grid.RowSelectionModel({
										singleSelect: this.rowSelectionModel !== 'multiple',
										listeners: {
											'rowselect': function(model, rowIndex, record) {
												const gridPanels = TCG.getChildrenByClass(win, TCG.grid.GridPanel);
												const childGridPanel = gridPanels[4];
												childGridPanel.additionalLoadParams = {
													parentRunObjectId: record.get('id')
												};
												childGridPanel.reload();

												// update the grand totals if applicable
												const gridpanel = model.grid.ownerCt;
												const gridSummaryPlugin = (gridpanel.plugins || []).find(plugin => plugin.ptype === 'gridsummary');
												if (gridSummaryPlugin) {
													gridSummaryPlugin.refreshGrandTotal();
												}
											}
										}
									});
								}
								return this.rowSelectionModel;
							}
						}]
					}
				]
			}]
		}, {
			layout: 'fit',
			region: 'south', // for the border layout - this item does not get resized with the window
			height: 400, // for the border layout - must specify height for non-center regions
			split: 'true', // for the border layout - specify on a non-center region to resize the regions
			items: [{
				xtype: 'tabpanel',
				items: [{
					title: 'Reconciliation Data',
					items: [{
						xtype: 'reconciliation-run-detail-grid',
						childWindow: true,
						dataView: 'Top/Bottom',
						getLoadURL: function() {
							return 'reconciliationRunObjectDetailListFind.json';
						},
						additionalLoadParams: {},
						getLoadParams: function(firstLoad) {
							const gridPanel = this;
							//Special handling for loading child transactions objects on cash reconciliations
							const params = TCG.clone(this.additionalLoadParams);
							if (!params.parentRunObjectId) {
								//Set to -1 so it loads nothing by default.
								params.parentRunObjectId = -1;
							}
							params.dataView = this.dataView;
							return gridPanel.getReconcileStatusFilter(firstLoad, params);
						}
					}]
				}]
			}]
		}]
	}]
});
