Clifton.reconciliation.run.ReconciliationRunDetailTopBottomWindow = Ext.extend(Clifton.reconciliation.run.ReconciliationRunDetailWindow, {
	height: 725,
	getDetailPageId: function(grid, row) {
		return row.id + '_Top/Bottom';
	},
	additionalItems: [{
		flex: 1,  // for the panel vbox layout so this section gets resized with the window - the top section (formpanel) will remain the same
		xtype: 'tabpanel',
		items: [
			{
				title: 'Reconciliation Data',
				items: [{
					xtype: 'reconciliation-run-detail-grid',
					dataView: 'Top/Bottom'
				}]
			}
		]
	}]
});
