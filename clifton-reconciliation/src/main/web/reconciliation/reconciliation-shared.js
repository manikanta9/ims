Ext.ns(
		'Clifton.reconciliation',
		'Clifton.reconciliation.definition',
		'Clifton.reconciliation.definition.group',
		'Clifton.reconciliation.definition.preprocessor',
		'Clifton.reconciliation.definition.rule',
		'Clifton.reconciliation.definition.type',
		'Clifton.reconciliation.event',
		'Clifton.reconciliation.event.journal',
		'Clifton.reconciliation.exclusion',
		'Clifton.reconciliation.investment',
		'Clifton.reconciliation.file',
		'Clifton.reconciliation.job',
		'Clifton.reconciliation.rule',
		'Clifton.reconciliation.run',
		'Clifton.reconciliation.run.status',
		'Clifton.reconciliation.setup',
		'Clifton.reconciliation.archive'
);

Clifton.reconciliation.DataTypes = [
	['DESCRIPTION', 'STRING', 'Sets the property data type to STRING of up to 500 characters.'],
	['MONEY', 'MONEY', 'Sets the property data type to MONEY (2 decimal places).'],
	['PRICE', 'PRICE', 'Sets the property data type to PRICE (15 decimal places).'],
	['QUANTITY', 'QUANTITY', 'Sets the property data type to QUANTITY (10 decimal places).'],
	['EXCHANGE_RATE', 'EXCHANGE_RATE', 'Sets the property data type to EXCHANGE_RATE (16 decimal places).'],
	['INTEGER', 'INTEGER', 'Sets the property data type to INTEGER.'],
	['DATE', 'DATE', 'Sets the property data type to DATE.'],
	['BIT', 'BOOLEAN', 'Sets the property data type to BOOLEAN.']
];

Clifton.reconciliation.event.ReconcilaitionEventObjectStatuses = [
	['OPEN', 'OPEN', 'Event has been created, but no submitted.s'],
	['PENDING_APPROVAL', 'PENDING_APPROVAL', 'Event is pending approval.'],
	['PENDING_EXECUTION', 'PENDING_EXECUTION', 'Event is executing'],
	['COMPLETE', 'COMPLETE', 'Event is complete.'],
	['ERROR', 'ERROR', 'Event errored out.']
];

Clifton.reconciliation.investment.InvestmentAccountDisplayProperties = [
	['accountNumber', 'Account Number', 'Uses the account number for the display value for the mapped account.'],
	['accountNumber2', 'Account Number 2', 'Uses the alternative account number for the display value for the mapped account.']
];

Clifton.reconciliation.investment.InvestmentSecurityDisplayProperties = [
	['securitySymbol', 'Symbol', 'Uses the symbol for the display value for the mapped security.'],
	['securityCusip', 'Cusip', 'Uses the cusip for the display value for the mapped security.'],
	['securityIsin', 'ISIN', 'Uses the ISIN for the display value for the mapped security.'],
	['securitySedol', 'Sedol', 'Uses the sedol for the display value for the mapped security.'],
	['securityOccSymbol', 'OCC Symbol', 'Uses the OCC symbol for the display value for the mapped security.']
];

Clifton.reconciliation.run.ReconciliationRunOrphanObjectTypes = [
	['EXCLUDED', 'Excluded', 'The run object was excluded due to a Reconciliation Exclusion Assignment.'],
	['FILTERED', 'Filtered', 'The run object was excluded due to a Filter Condition.']
];

Clifton.reconciliation.event.ReconciliationEventObjectCategories = [
	['ACCOUNT', 'Account', 'Category for all events used to manage accounts (lock/unlock/etc).'],
	['SIMPLE_JOURNAL', 'Simple Journal', 'Category for all events used for creating simple journal adjustments to reconciliation objects.'],
	['ADJUSTING_JOURNAL', 'Adjusting Journal', 'Category for all events used for creating adjusting journal events to reconciliation objects.']
];

Clifton.reconciliation.setup.ReconciliationDataUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'reconciliationDataUploadWindow',
	iconCls: 'upload',
	title: 'Reconciliation Data Upload',
	width: 800,
	height: 400,
	items: [{
		xtype: 'formpanel',
		fileUpload: true,
		instructions: 'Data Uploads allow you to test reconciliation data imports from files directly into the system.',
		items: [
			{
				fieldLabel: 'File Definition', name: 'reconciliationFileDefinition.name', hiddenName: 'fileDefinitionId', xtype: 'combo',
				url: 'reconciliationFileDefinitionListFind.json', detailPageClass: 'Clifton.reconciliation.file.ReconciliationFileDefinitionWindow',
				loadAll: true, allowBlank: false, qtip: 'File Definition defining what source and reconciliation definitions should be used to process uploaded data.'
			},
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{fieldLabel: 'Date', name: 'importDate', xtype: 'datefield', allowBlank: false},
			{xtype: 'sectionheaderfield', header: 'Upload Results'},
			{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
		],
		getSaveURL: function() {
			return 'reconciliationFileUpload.json?enableValidatingBinding=true';
		},
		listeners: {
			afterload: function(panel) {
				panel.getWindow().defaultData.openerCt.reload();
			}
		}
	}]
});

Clifton.reconciliation.setup.ReconciliationExclusionAssignmentUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'reconciliationExclusionAssignmentUploadWindow',
	iconCls: 'upload',
	title: 'Reconciliation Exclusion Assignment Upload',
	width: 600,
	height: 250,
	items: [{
		xtype: 'formpanel',
		fileUpload: true,
		instructions: 'Exclusion Assignment Uploads allow you to import larger lists of Exclusion Assignments.',
		items: [
			{fieldLabel: 'Table', name: 'tableName', value: 'ReconciliationExclusionAssignment', xtype: 'hidden'},
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{xtype: 'sectionheaderfield', header: 'Upload Results'},
			{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}

		],
		getSaveURL: function() {
			return 'systemUploadFileUpload.json';
		},
		listeners: {
			afterload: function(panel) {
				panel.getWindow().defaultData.openerCt.reload();
			}
		}
	}]
});

Clifton.reconciliation.rule.ReconciliationRuleFunctionInfoWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Info Window',
	iconCls: 'info',
	modal: true,
	allowOpenFromModal: true,
	minimizable: false,
	maximizable: false,
	enableShowInfo: false,
	closeWindowTrail: true,
	width: 770,
	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		items: [{
			xtype: 'label',
			html: '<div>Reconciliation Functions are used to modify values based on the defined logic.</div>'
		}]
	}]
});

Clifton.reconciliation.rule.ReconciliationRuleConditionInfoWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Info Window',
	iconCls: 'info',
	modal: true,
	allowOpenFromModal: true,
	minimizable: false,
	maximizable: false,
	enableShowInfo: false,
	closeWindowTrail: true,
	width: 770,
	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		items: [{
			xtype: 'label',
			html: '<div>Reconciliation Conditions are used to define if a specific filter or field is applicable to a definition based on the defined logic.</div>'
		}]
	}]
});

Clifton.reconciliation.job.ReconciliationReconcileJobPanel = {
	title: 'Run Processing',
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},
	items: [{
		xtype: 'formpanel',
		instructions: 'Specify criteria necessary to select specific runs for reconciliation.',
		getDefaultData: function(win) {
			if (win.typeName) {
				return Clifton.reconciliation.getReconciliationTypeDataPromise(win.typeName)
						.then(function(type) {
							return {reconciliationType: {name: type.name}, reconciliationTypeId: type.id};
						});
			}
			return win.defaultData;
		},
		listeners: {
			destroy: function() {
				Ext.TaskMgr.stopAll();
			},
			afterrender: function(fp) {
				const f = this.getForm();
				const rd = f.findField('reconcileDate');
				if (TCG.isEqualsStrict(rd.getValue(), '')) {
					const today = new Date();
					rd.setValue(today.format('m/d/Y'));
				}
			}

		},
		height: 150,
		labelWidth: 140,
		loadValidation: false, // using the form only to get background color/padding
		buttonAlign: 'right',
		items: [{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .49,
					items: [{
						xtype: 'formfragment',
						frame: false,
						labelWidth: 150,
						items: [
							{
								fieldLabel: 'Reconciliation Definition',
								xtype: 'combo',
								hiddenName: 'reconciliationDefinitionId',
								url: 'reconciliationDefinitionListFind.json',
								pageSize: 10,
								qtip: 'Use to Filter reconciliation runs by definition.',
								listeners: {
									beforequery: function(queryEvent) {
										const bp = {};
										const combo = queryEvent.combo;
										const typeId = combo.getParentForm().getForm().findField('reconciliationType.name').getValue();
										if (typeId) {
											bp.typeId = typeId;
										}
										combo.clearAndReset();
										combo.store.baseParams = bp;
									}
								}
							},
							{fieldLabel: 'Definition Type', xtype: 'combo', name: 'reconciliationType.name', hiddenName: 'reconciliationTypeId', width: 180, url: 'reconciliationTypeListFind.json'}
						]
					}]
				},
				{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
				{
					columnWidth: .49,
					items: [{
						xtype: 'formfragment',
						frame: false,
						labelWidth: 150,
						items: [
							{fieldLabel: 'Reconcile Date', name: 'reconcileDate', xtype: 'datefield', allowBlank: false},
							{name: 'synchronous', xtype: 'hidden', value: 'false'}
						]
					}]
				}
			]
		}],
		buttons: [{
			text: 'Process Run(s)',
			iconCls: 'run',
			width: 140,
			handler: function() {
				const owner = this.findParentByType('formpanel');
				const form = owner.getForm();
				const reconciliationDefinitionId = form.findField('reconciliationDefinitionId').getValue();
				const reconciliationType = form.findField('reconciliationTypeId').getValue();
				if (TCG.isEqualsStrict(reconciliationType, '') && TCG.isEqualsStrict(reconciliationDefinitionId, '')) {
					TCG.showError('Reconciliation Definition or Type selection is required.', 'Invalid Selection');
				}

				Ext.Msg.confirm('Reconcile?', 'Are you sure you want to reconcile for the selected definitions?', function(a) {
					if (TCG.isEqualsStrict(a, 'yes')) {
						form.submit(Ext.applyIf({
							url: 'reconciliationRunProcess.json',
							waitMsg: 'Processing selected Reconciliation Runs...',
							success: function(form, action) {
								Ext.Msg.alert('Processing Started', action.result.result.message, function() {
									const grid = owner.ownerCt.items.get(1);
									grid.reload.defer(300, grid);
								});
							}
						}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
					}
				});
			}
		}]
	}, {
		xtype: 'core-scheduled-runner-grid',
		typeName: 'RECONCILIATION-RECONCILE',
		instantRunner: true,
		title: 'Executing Processes',
		flex: 1
	}]
};

Clifton.reconciliation.ReconciliationRunStatusWindow = Ext.extend(Clifton.core.StatusWindow, {
	title: 'Reconciliation Run Status',
	width: 1225,
	items: [{
		xtype: 'status-detail-form',
		height: 350,
		statusDetailList: [
			{
				xtype: 'textarea', fieldLabel: 'Message', name: 'status.message', style: 'font-size: 12px', height: 10, autoHeight: true
			},
			{
				xtype: 'formgrid-scroll',
				title: 'Details',
				readOnly: true,
				storeRoot: 'status.detailList',
				columnsConfig: [
					{header: 'Category', width: 200, dataIndex: 'category'},
					{header: 'Note', width: 800, dataIndex: 'note', renderer: TCG.renderText},
					{header: 'Date', width: 150, dataIndex: 'date', defaultSortColumn: true}
				],
				markModified: Ext.emptyFn
			}
		]
	}]
});

Clifton.reconciliation.getReconciliationTypeDataPromise = function(typeName) {
	return TCG.data.getDataPromiseUsingCaching('reconciliationTypeByName.json', null, 'reconciliation.type.' + typeName, {params: {name: typeName}});
};

Clifton.reconciliation.getReconciliationReconcileStatusDataPromise = function(statusName) {
	return TCG.data.getDataPromiseUsingCaching('reconciliationReconcileStatusByName.json', null, 'reconciliation.reconcileStatus.' + statusName, {params: {name: statusName}});
};

Clifton.reconciliation.run.ReconciliationRunDetailWindow = Ext.extend(TCG.app.Window, {
	width: 1500,
	height: 1000,
	iconCls: 'reconcile',
	titlePrefix: 'Reconciliation Run',
	additionalItems: 'OVERRIDE_ME',
	additionalTabs: [
		{
			title: 'Exclusion Assignments',
			items: [{
				xtype: 'reconciliation-exclusion-assignments-panel'
			}]
		},
		{
			title: 'External Loads',
			items: [{
				xtype: 'integration-importRunEventGrid',
				getLoadParams: function(firstLoad) {
					const gridPanel = this;
					const formPanel = TCG.getChildByName(this.getWindow(), 'reconciliationRunFormPanel');
					return TCG.data.getDataPromise('reconciliationFileDefinitionMappingListFind.json', this, {params: {reconciliationDefinitionName: formPanel.getFormValue('reconciliationDefinition.name')}})
							.then(function(definitionMappingList) {
								const fileNames = [];
								definitionMappingList.forEach(definitionMapping => {
									fileNames.push(definitionMapping.fileDefinition.fileName);
									if (definitionMapping.fileDefinition.securitiesFileName) {
										fileNames.push(definitionMapping.fileDefinition.securitiesFileName);
									}
								});
								//Set filters
								gridPanel.setFilterValue('importRun.effectiveDate', {'after': Clifton.calendar.getBusinessDayFrom(-2)});
								gridPanel.setFilterValue('importRun.integrationImportDefinition.testDefinition', false);
								//Set params
								const params = {readUncommittedRequested: true};
								params.targetApplicationNames = ['Integration', 'Reconciliation'];
								params.fileDefinitionFileNames = fileNames;
								return params;
							});
				}
			}]
		}
	],
	getMainFormId: function() {
		return this.defaultData.id;
	},
	getPrimarySourceName: function() {
		return this.defaultData.reconciliationDefinition.primarySource.shortName;
	},
	getSecondarySourceName: function() {
		return this.defaultData.reconciliationDefinition.secondarySource.shortName;
	},
	render: function(...args) {
		const panel = this.items.get(0);
		panel.add(...this.additionalItems);
		const tabPanels = TCG.getChildrenByClass(panel, TCG.TabPanel);
		for (let i = 0; i < tabPanels.length; i++) {
			tabPanels[i].add(...this.additionalTabs);
		}
		TCG.Window.superclass.render.apply(this, args);
		this.on('beforeclose', function() {
			if (!this.closing) {
				if (this.openerCt && this.openerCt.reload) {
					this.openerCt.reload();
				}
			}
			return true;
		});
		if (this.defaultData) {
			this.setTitle(`${this.defaultData.reconciliationDefinition.name} ${TCG.parseDate(this.defaultData.runDate).format('m/d/Y')} - <i>${this.defaultData.reconciliationReconcileStatus.name}</i>`, this.iconCls);
		}
	},
	items: [{
		xtype: 'panel', // need a base container for the formpanel and grids
		layout: 'vbox',
		layoutConfig: {align: 'stretch'},
		items: [{
			name: 'reconciliationRunFormPanel',
			xtype: 'formpanel',
			labelWidth: 140,
			height: 90,
			url: 'reconciliationRun.json?requestedMaxDepth=4',
			items: [
				{fieldLabel: 'ID', width: 30, dataIndex: 'id', hidden: true},
				{
					xtype: 'panel',
					layout: 'column',
					items: [{
						columnWidth: .35,
						layout: 'form',
						labelWidth: 100,
						items: [{fieldLabel: 'Definition Name', name: 'reconciliationDefinition.name', xtype: 'linkfield', detailIdField: 'reconciliationDefinition.id', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionWindow'}]
					}, {
						columnWidth: .15,
						layout: 'form',
						labelWidth: 60,
						items: [{
							fieldLabel: 'Status', name: 'reconciliationReconcileStatus.label', xtype: 'displayfield',
							setValue: function(value) {
								let displayValue = value;
								if (TCG.isEquals(value, 'Not Reconciled')) {
									displayValue = `<span style="color: red">${value}</span>`;
								}
								else if (TCG.isEquals(value, 'Manually Reconciled')) {
									displayValue = `<span style="color: blue">${value}</span>`;
								}
								else if (TCG.isEquals(value, 'Reconciled')) {
									displayValue = `<span style="color: green">${value}</span>`;
								}
								TCG.form.DisplayField.superclass.setValue.apply(this, [displayValue]);
							}
						}]
					}, {
						columnWidth: .15,
						layout: 'form',
						labelWidth: 85,
						items: [{
							fieldLabel: 'Status Details', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.ReconciliationRunStatusWindow', detailPageWhenNoId: true, loadDefaultDataAfterRender: true,
							getValue: function(value) {
								TCG.form.LinkField.superclass.setValue.apply(this, ['View']);
							},
							getDefaultData(formPanel) {
								return {status: formPanel.getFormValue('runDetails')};
							}
						}]
					}, {
						columnWidth: .15,
						layout: 'form',
						labelWidth: 85,
						items: [{
							fieldLabel: 'Approved By', name: 'approvedByUser.displayName', xtype: 'displayfield',
							getValue: function() {
								const value = TCG.form.DisplayField.superclass.getValue.apply(this);
								if (TCG.isBlank(value)) {
									this.setValue('Not Approved');
								}
							},
							setValue: function(value) {
								let displayValue;
								if (TCG.isEquals(value, 'Not Approved')) {
									displayValue = `<span style="color: red">${value}</span>`;
								}
								else {
									displayValue = `<span style="color: green">${value}</span>`;
								}
								TCG.form.DisplayField.superclass.setValue.apply(this, [displayValue]);
							}
						}]
					}, {
						columnWidth: .15,
						layout: 'form',
						labelWidth: 60,
						items: [{
							fieldLabel: 'Run Date', name: 'runDate', xtype: 'datefield', type: 'date',
							listeners: {
								select: function(field) {
									const formPanel = TCG.getParentByClass(field, Ext.FormPanel);
									const currentDefaultData = formPanel.getWindow().defaultData;
									const definitionId = formPanel.getFormValue('reconciliationDefinition.id');
									const definitionName = formPanel.getFormValue('reconciliationDefinition.name');
									return TCG.data.getDataPromise('reconciliationRunByDefinitionAndDate.json', field, {params: {definitionId: definitionId, runDate: field.value}})
											.then(function(reconciliationRun) {
												if (reconciliationRun) {
													formPanel.getWindow().defaultData = reconciliationRun;
													const gridPanel = TCG.getChildrenByClass(formPanel.getWindow(), TCG.grid.GridPanel);
													if (Array.isArray(gridPanel)) {
														gridPanel.forEach(gPanel => {
															if (TCG.isEquals(gPanel.name, 'reconciliationRunObjectDetailListFind')) {
																//Reload first to trigger listener that will update parent formPanel (status and counts)
																gPanel.reload();
																//Now reset the toolbar (to update buttons, e.g. change approve to unapprove)
																gPanel.resetToolbar(gPanel.getTopToolbar(), gPanel);
															}
														});
													}
													else {
														//Reload first to trigger listener that will update parent formPanel (status and counts)
														gridPanel.reload();
														//Now reset the toolbar (to update buttons, e.g. change approve to unapprove)
														gridPanel.resetToolbar(gridPanel.getTopToolbar(), gridPanel);
													}
												}
												else {
													//Reset default data if no run is found.
													formPanel.getForm().setValues(currentDefaultData);
													TCG.showError(`No run found for definition '${definitionName} on date ${field.value}`);
												}
											});
								}
							}
						}]
					}]
				},
				{fieldLabel: 'Primary Source', itemId: 'primarySource', name: 'reconciliationDefinition.primarySource', hidden: 'true'},
				{fieldLabel: 'Secondary Source', itemId: 'secondarySource', name: 'reconciliationDefinition.secondarySource', hidden: 'true'},
				{
					xtype: 'progress',
					itemId: 'overallReconciliationStatusBar',
					cls: 'blue-bar',
					style: 'margin: 0px 5px 5px 5px'
				},
				{
					xtype: 'panel',
					layout: 'column',
					items: [{
						layout: 'form',
						labelWidth: 35,
						columnWidth: .14,
						items: [{fieldLabel: 'Total', labelStyle: 'font-weight:bold;', name: 'runDetails.statusMap.totalCount', xtype: 'displayfield'}]
					}, {
						layout: 'form',
						labelWidth: 55,
						columnWidth: .14,
						items: [{fieldLabel: 'Matched', labelStyle: 'font-weight:bold;', style: 'color:green;', name: 'runDetails.statusMap.matchedCount', xtype: 'displayfield'}]
					}, {
						layout: 'form',
						labelWidth: 85,
						columnWidth: .14,
						items: [{fieldLabel: 'Not Matched', labelStyle: 'font-weight:bold;', style: 'color:red;', name: 'runDetails.statusMap.notMatchedCount', xtype: 'displayfield'}]
					}, {
						layout: 'form',
						labelWidth: 70,
						columnWidth: .14,
						items: [{fieldLabel: 'Reconciled', labelStyle: 'font-weight:bold;', style: 'color:green;', name: 'runDetails.statusMap.reconciledCount', xtype: 'displayfield'}]
					}, {
						layout: 'form',
						labelWidth: 135,
						columnWidth: .14,
						items: [{fieldLabel: 'Manually Reconciled', labelStyle: 'font-weight:bold;', style: 'color:blue;', name: 'runDetails.statusMap.manuallyReconciledCount', xtype: 'displayfield'}]
					}, {
						layout: 'form',
						labelWidth: 95,
						columnWidth: .14,
						items: [{fieldLabel: 'Not Reconciled', labelStyle: 'font-weight:bold;', style: 'color:red;', name: 'runDetails.statusMap.notReconciledCount', xtype: 'displayfield'}]
					}, {
						layout: 'form',
						labelWidth: 105,
						columnWidth: .14,
						items: [{fieldLabel: 'Total Reconciled', labelStyle: 'font-weight:bold;', style: 'color:green;', name: 'runDetails.statusMap.totalReconciledCount', xtype: 'displayfield'}]
					}]
				}
			]
		}]
	}]
});

Clifton.reconciliation.run.ReconciliationExclusionAssignmentsPanel = Ext.extend(Ext.Panel, {
	name: 'exclusionAssignmentSplitPanel',
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},
	includeApproved: false,
	initComponent: function() {
		// Attach "items" property manually instead of on prototype; ExtJS does not properly handle prototype-scoped "items" values during panel children
		this.items = [...this.exclusionAssignmentItems];
		Clifton.reconciliation.run.ReconciliationExclusionAssignmentsPanel.superclass.initComponent.call(this);
	},
	exclusionAssignmentItems: [{
		layout: 'border',
		align: 'stretch',
		flex: 1,
		items: [{
			layout: 'fit',
			region: 'center',
			items: [{
				xtype: 'gridpanel',
				name: 'reconciliationExclusionAssignmentListFind',
				instructions: 'A list of unapproved exclusion assignments.  All exclusion assignments must be approved.  Once approved, data that is excluded using the assignment is purged after 7 days.  If not approved, the data is retained indefinitely.',
				rowSelectionModel: 'multiple',
				height: 350,
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Label', width: 150, dataIndex: 'label', hidden: true},
					{
						header: 'Source', width: 100, dataIndex: 'reconciliationSource.name', tooltip: 'The source this exclusion assignment applies to.',
						filter: {type: 'combo', searchFieldName: 'sourceId', url: 'reconciliationSourceListFind.json', displayField: 'name'}
					},
					{header: 'Definition', width: 150, dataIndex: 'reconciliationDefinition.name', defaultSortColumn: true, filter: {searchFieldName: 'definitionName'}, hidden: 'true'},
					{header: 'Account', width: 100, dataIndex: 'reconciliationInvestmentAccount.accountNumber', tooltip: 'Investment Account to be excluded.', filter: {searchFieldName: 'accountNumber'}},
					{header: 'Security', width: 100, dataIndex: 'reconciliationInvestmentSecurity.securitySymbol', tooltip: 'Investment Security to be excluded.', filter: {searchFieldName: 'securitySymbol'}},
					{header: 'Invalid Account', width: 100, dataIndex: 'invalidAccountNumber', tooltip: 'Invalid Account to be excluded.'},
					{header: 'Invalid Security', width: 100, dataIndex: 'invalidSecurityIdentifier', tooltip: 'Invalid Security to be excluded.'},
					{header: 'Comment', width: 150, dataIndex: 'comment'},
					{header: 'Include', width: 50, dataIndex: 'include', type: 'boolean', tooltip: 'Defines if this assignment means the row should be excluded or included.'},
					{header: 'Start Date', width: 50, dataIndex: 'startDate', tooltip: 'Specifies the start date of this assignment.'},
					{header: 'End Date', width: 50, dataIndex: 'endDate', tooltip: 'Specifies the end date of this assignment.'},
					{header: 'Approver', width: 50, dataIndex: 'approvalUser.displayName', filter: {searchFieldName: 'approvalUserName'}}
				],
				approveSelection: function(selectionModel, gridPanel) {
					const grid = this.grid;
					const store = grid.store;

					const approveSelectedItem = () => {
						const selectedItem = selectionModel.getSelected();
						const loader = new TCG.data.JsonLoader({
							waitTarget: grid.ownerCt,
							waitMsg: 'Approving...',
							params: {id: selectionModel.getSelected().id},
							conf: {rowId: selectedItem.id},
							onLoad: (record, conf) => {
								selectionModel.deselectRow(store.indexOfId(selectedItem.id));
								if (selectionModel.getSelected()) {
									// Approve the next selected item
									approveSelectedItem();
								}
								else {
									// Approvals are complete; refresh changes to the grid
									gridPanel.reload();
								}
							}
						});
						loader.load('reconciliationExclusionAssignmentApprove.json');
					};
					approveSelectedItem();
				},
				unapproveSelection: function(selectionModel, gridPanel) {
					const grid = this.grid;
					const store = grid.store;

					const unapproveSelectedItem = () => {
						const selectedItem = selectionModel.getSelected();
						const loader = new TCG.data.JsonLoader({
							waitTarget: grid.ownerCt,
							waitMsg: 'Approving...',
							params: {id: selectionModel.getSelected().id},
							conf: {rowId: selectedItem.id},
							onLoad: (record, conf) => {
								selectionModel.deselectRow(store.indexOfId(selectedItem.id));
								if (selectionModel.getSelected()) {
									// Approve the next selected item
									unapproveSelectedItem();
								}
								else {
									// Approvals are complete; refresh changes to the grid
									gridPanel.reload();
								}
							}
						});
						loader.load('reconciliationExclusionAssignmentUnapprove.json');
					};
					unapproveSelectedItem();
				},
				addToolbarButtons: function(toolbar, gridPanel) {
					toolbar.add({
						text: 'Approve',
						tooltip: 'Approve the Reconciliation Excluded Orphaned Data for the selected Reconciliation Exclusion Assignment.',
						iconCls: 'row_reconciled',
						scope: this,
						handler: function() {
							const sm = gridPanel.grid.getSelectionModel();
							if (sm.getCount() === 0) {
								TCG.showError('Please select at least one row to be approved.', 'No Row(s) Selected');
							}
							else {
								Ext.Msg.confirm('Approve Selected Row', 'Would you like to approve the selected row(s)?', function(a) {
									if (a === 'yes') {
										gridPanel.approveSelection(sm, gridPanel);
									}
								});
							}
						}
					});
					toolbar.add('-');
					toolbar.add({
						text: 'Unapprove',
						tooltip: 'Unapprove the Reconciliation Excluded Orphaned Data for the selected Reconciliation Exclusion Assignment.',
						iconCls: 'cancel',
						scope: this,
						handler: function() {
							const sm = gridPanel.grid.getSelectionModel();
							if (sm.getCount() === 0) {
								TCG.showError('Please select at least one row to be unapproved.', 'No Row(s) Selected');
							}
							else {
								Ext.Msg.confirm('Unapprove Selected Row', 'Would you like to unapprove the selected row(s)?', function(a) {
									if (a === 'yes') {
										gridPanel.unapproveSelection(sm, gridPanel);
									}
								});
							}
						}
					});
					toolbar.add('-');
					toolbar.add({
						text: 'Upload',
						tooltip: 'Upload Exclusion Assignments from file',
						iconCls: 'import',
						menu: new Ext.menu.Menu({
							items: [{
								text: 'Upload',
								iconCls: 'import',
								scope: this,
								handler: function() {
									TCG.createComponent('Clifton.reconciliation.setup.ReconciliationExclusionAssignmentUploadWindow', {
										defaultData: {
											openerCt: this
										}
									});
								}
							}, {
								text: 'Sample File',
								iconCls: 'excel',
								handler: function() {
									Clifton.system.downloadSampleUploadFile('ReconciliationExclusionAssignment', false, this);
								}
							}]
						})
					});
				},
				editor: {
					detailPageClass: 'Clifton.reconciliation.exclusion.ReconciliationExclusionAssignmentWindow'
				},
				getTopToolbarFilters: function(toolbar) {
					return [{boxLabel: 'Include Approved', xtype: 'toolbar-checkbox', name: 'includeApproved'}];
				},
				getLoadParams: function(firstLoad) {
					const params = {};
					//Better way to do this?  I was doing child from window, but in dual view it then has two with the same name...
					const panel = this.ownerCt.ownerCt.ownerCt;
					if (panel.includeApproved) {
						TCG.getChildByName(this.getTopToolbar(), 'includeApproved').setValue(true);
					}
					const includeApproved = TCG.getChildByName(this.getTopToolbar(), 'includeApproved') ? TCG.getChildByName(this.getTopToolbar(), 'includeApproved').getValue() : false;
					if (includeApproved !== true) {
						params['approvalUserIdIsNull'] = true;
					}
					if (this.getWindow().defaultData && this.getWindow().defaultData.reconciliationDefinition) {
						const definition = this.getWindow().defaultData.reconciliationDefinition;
						params['sourceIds'] = [definition.primarySource.id, definition.secondarySource.id];
						params['definitionIdOrNull'] = definition.id;
					}
					return params;
				},
				gridConfig: {
					listeners: {
						'rowclick': function(grid, rowIndex, evt) {
							const row = grid.store.data.items[rowIndex];
							const exclusion = row.json.id;
							const orphanGrid = TCG.getChildByName(this.ownerCt.ownerCt.ownerCt, 'reconciliationRunOrphanObjectListFind');
							orphanGrid.exclusionAssignmentId = exclusion;
							orphanGrid.reload();
						}
					}
				}
			}]
		}, {
			layout: 'fit',
			region: 'south',
			height: 250,
			split: 'true',
			items: [{
				title: 'Excluded Orphaned Data',
				xtype: 'gridpanel',
				name: 'reconciliationRunOrphanObjectListFind',
				instructions: 'A list of run objects excluded by this exclusion assignment.  Objects are purged after 7 days if the assignment is approved.',
				heightResized: true,
				exclusionAssignmentId: 0,
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Source', width: 100, dataIndex: 'fileDefinition.source.name', filter: {searchFieldName: 'sourceName'}},
					{header: 'File Definition Name', width: 200, dataIndex: 'fileDefinition.name', filter: {searchFieldName: 'fileDefinitionName'}},
					{header: 'Account Number', width: 100, dataIndex: 'accountNumber', filter: false, sortable: false},
					{header: 'Security Identifier', width: 100, dataIndex: 'securityIdentifier', filter: false, sortable: false},
					{
						header: 'Type', width: 50, dataIndex: 'orphanType',
						filter: {
							type: 'combo',
							dataIndex: 'orphanType',
							displayField: 'name',
							valueField: 'value',
							mode: 'local',
							store: {
								xtype: 'arraystore',
								fields: ['value', 'name', 'description'],
								data: Clifton.reconciliation.run.ReconciliationRunOrphanObjectTypes
							}
						}
					},
					{header: 'Excluded Date', width: 50, dataIndex: 'reconciliationDate'}
				],
				editor: {
					addEnabled: false,
					deleteEnabled: false,
					detailPageClass: 'Clifton.reconciliation.run.ReconciliationRunOrphanObjectWindow'
				},
				getLoadParams: function(firstLoad) {
					if (this.exclusionAssignmentId === 0) {
						return false;
					}
					return {
						exclusionAssignmentId: this.exclusionAssignmentId
					};
				}
			}]
		}]
	}]
});
Ext.reg('reconciliation-exclusion-assignments-panel', Clifton.reconciliation.run.ReconciliationExclusionAssignmentsPanel);

Clifton.reconciliation.run.ReconciliationRunObjectDetailGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'reconciliationRunObjectDetailListFind',
	height: 500,
	remoteSort: true,
	useBufferView: false, //Allows for rows that have 2 (or more) rows of data to dynamically size properly.
	appendStandardColumns: false,
	additionalPropertiesToRequest: 'modifierText|reconciledOnPrimarySource|reconciliationMatchStatus.label|reconciliationReconcileStatus.label|reconciliationDefinition.primarySource|reconciliationDefinition.secondarySource|reconciliationInvestmentAccountId|reconciliationInvestmentSecurityId|errors',
	// invoked before returning filters data; can be used to add additional filters or modify existing
	getLoadParams: function(firstLoad) {
		const gridPanel = this;
		const params = {
			id: gridPanel.getWindow().defaultData.reconciliationRunObjectId,
			runId: gridPanel.getWindow().defaultData.id,
			dataView: gridPanel.getWindow().defaultData.dataView
		};
		gridPanel.appendInvalidSecurityAccountFilter(gridPanel, params);
		return params;
	},
	appendInvalidSecurityAccountFilter: function(gridPanel, params) {
		const t = gridPanel.getTopToolbar();
		const invalidAccountOrSecurities = TCG.getChildByName(t, 'investmentSecurityOrAccountIdIsNull', true) ? TCG.getChildByName(t, 'investmentSecurityOrAccountIdIsNull', true).pressed : false;
		if (invalidAccountOrSecurities) {
			params['investmentSecurityOrAccountIdIsNull'] = true;
		}
	},
	getFormPanel: function() {
		return TCG.getChildrenByClass(this.getWindow(), Ext.form.FormPanel)[0];
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Match Status', xtype: 'toolbar-combo', name: 'matchStatusId', displayField: 'label', comboDisplayField: 'label', width: 150, url: 'reconciliationMatchStatusListFind.json', linkedFilter: 'matchStatus.name'},
			{fieldLabel: 'Reconcile Status', xtype: 'toolbar-combo', name: 'reconcileStatusId', displayField: 'label', comboDisplayField: 'label', width: 165, url: 'reconciliationReconcileStatusListFind.json', linkedFilter: 'reconcileStatus.name'},
			{
				fieldLabel: '', xtype: 'button', iconCls: 'warning-red', name: 'investmentSecurityOrAccountIdIsNull', enableToggle: true, tooltip: 'Show invalid Securities and Accounts only.',
				listeners: {
					toggle: function(button, pressed) {
						TCG.getParentByClass(button, Ext.Panel).reload();
					}
				}
			}
		];
	},
	getPrimarySourceName: function() {
		return this.getWindow().getPrimarySourceName();
	},
	getSecondarySourceName: function() {
		return this.getWindow().getSecondarySourceName();
	},
	getSelectedRunObjectIds: function(grid) {
		// get a list of unique run object ids from the selection
		const selectionModel = grid.getSelectionModel();
		return (selectionModel.getSelections() || []).map(v => v.get('id')).distinct();
	},
	isCashReconciliationWindow: function() {
		return this.getWindow().defaultData.reconciliationDefinition.type.name === 'Cash Reconciliation';
	},
	isTransactionReconciliationWindow: function() {
		return this.getWindow().defaultData.reconciliationDefinition.type.name === 'Transaction Reconciliation';
	},
	isColumnNumeric: function(column) {
		const dataType = column.dataType ? column.dataType : column.field.defaultDataType;
		return dataType === 'MONEY' || dataType === 'PRICE' || dataType === 'QUANTITY';
	},
	isColumnCashBalance: function(column) {
		return 'Cash Balance' === column.field.name;
	},
	getNumericColumnFormat: function(column) {
		if (this.isColumnNumeric(column)) {
			const dataType = column.dataType ? column.dataType : column.field.defaultDataType;
			return dataType === 'QUANTITY' ? '0,000.000000' : '0,000.00';
		}
		return undefined;
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Identifier', width: 15, dataIndex: 'identifier', hidden: true},
		{header: 'Match Status', width: 50, dataIndex: 'matchStatus.name', hidden: true, filter: {type: 'combo', displayField: 'label', searchFieldName: 'matchStatusId', url: 'reconciliationMatchStatusListFind.json'}},
		{header: 'Reconcile Status', width: 50, dataIndex: 'reconcileStatus.name', hidden: true, filter: {type: 'combo', displayField: 'label', searchFieldName: 'reconcileStatusId', url: 'reconciliationReconcileStatusListFind.json'}},
		{
			header: 'Status', width: 40, dataIndex: 'status', skipExport: true,
			renderer: (value, metadata, record, rowIndex, colIndex, store) => {
				const matchStatusName = record.json.matchStatus.name;
				const reconcileStatusName = record.json.reconcileStatus.name;

				let image;
				if (reconcileStatusName === 'RECONCILED') {
					image = 'accept';
				}
				else if (reconcileStatusName === 'NOT_RECONCILED' && matchStatusName !== 'NOT_MATCHED') {
					image = 'cancel';
				}
				else if (reconcileStatusName === 'MANUALLY_RECONCILED') {
					if (TCG.isBlank(record.json.reconciledOnPrimarySource)) {
						image = 'accept';
					}
					else {
						image = record.json.reconciledOnPrimarySource ? 'arrow-right-green' : 'arrow-left-green';
					}
				}
				else if (matchStatusName === 'NOT_MATCHED') {
					image = 'ignored';
				}

				// Attach image
				let renderedValue = value;
				if (image) {
					if (matchStatusName === 'MANUALLY_MATCHED') {
						renderedValue = `<div style="position:absolute;"><img src="core/images/icons/${image}.png" title="${renderedValue}" /><span style="padding-right:10px;"></span><img src="core/images/icons/link.png" title="${matchStatusName}" />`;
					}
					else {
						renderedValue = `<div style="position:absolute;"><img src="core/images/icons/${image}.png" title="${renderedValue}" />`;
					}
				}
				if (record.json.modifierText) {
					let modifierText = '';
					const modifiers = JSON.parse(record.json.modifierText);
					if (modifiers.accountAlias) {
						modifierText += 'Account Alias: ' + modifiers.accountAlias.label;
					}
					if (modifiers.securityAlias) {
						if (!TCG.isBlank(modifierText)) {
							modifierText += '\n';
						}
						modifierText += 'Security Alias: ' + modifiers.securityAlias.label;
					}
					if (modifiers.evaluator) {
						if (!TCG.isBlank(modifierText)) {
							modifierText += '\n';
						}
						modifierText += 'Evaluator: ' + modifiers.evaluator.label;
					}
					if (modifiers.adjustmentAmount) {
						if (!TCG.isBlank(modifierText)) {
							modifierText += '\n';
						}
						modifierText += 'Adjustment Amount: ' + TCG.numberFormat(modifiers.adjustmentAmount, '0,000.00');
					}
					if (!TCG.isBlank(modifierText)) {
						renderedValue += `<span style="padding-right:10px;"></span><img src="core/images/icons/list.png" title="${modifierText}" />`;
					}
				}
				return renderedValue + '</div>';
			}
		},
		{
			header: 'Source', width: 75, dataIndex: 'source', exportColumnValueConverter: 'reconciliationSourceConverter', filter: {searchFieldName: 'sourceId'},
			renderer: function(value, metaData, row) {
				const gridPanel = this.scope.gridPanel;
				const sideBySideView = gridPanel.dataView ? gridPanel.dataView === 'Side/Side' : gridPanel.getWindow().defaultData.dataView === 'Side/Side';
				if (sideBySideView) {
					let displayValue = '';
					Ext.each(value, function(val) {
						if (TCG.isNotBlank(displayValue)) {
							displayValue += ' / ';
						}
						displayValue += val.name;
					});
					return displayValue;
				}
				else {
					let displayValue = '';
					Ext.each(value, function(val) {
						displayValue += `<span style="display: block; width: 100%; height: 16px; float: left;">${val.name}</span>`;
					});
					return displayValue;
				}
			}
		},
		{header: 'Break Days', width: 70, dataIndex: 'breakDays', align: 'right', filter: {type: 'float'}},
		{
			header: 'Notes', width: 150, dataIndex: 'notes',
			listeners: {
				click: function(column, grid, rowIndex, event) {
					// popup the Notes window to view the notes when the notes column is clicked on
					const row = grid.store.getAt(rowIndex);
					if (row.get('notes')) {
						const runObjectId = row.get('id');
						TCG.createComponent('Clifton.reconciliation.run.EditReconciliationNotesWindow', {
							defaultData: {runObjectId: runObjectId},
							openerCt: this,
							params: {id: runObjectId}
						});
					}
				}
			},
			renderer: function(valueList, metaData, row) {
				if (valueList) {
					if (Array.isArray(valueList)) {
						let fullMessage = '';
						let latestManualNote = '';
						valueList.forEach(value => {
							if (!TCG.isBlank(fullMessage)) {
								fullMessage += '<br />';
							}
							fullMessage += `${new Date(value.createDate).dateFormat('Y-m-d H:i:s')} [${Clifton.security.renderSecurityUser(value.createUserId, this.scope.gridPanel)}] - ${value.type} ${value.note}`;
							if (!latestManualNote && value.type !== 'AUTO_RECONCILE') {
								latestManualNote = value.note;
							}
						});
						return String.format(`<div style='float: right; right:10px;'><img src='core/images/icons/pencil.png' title='Add/Edit Notes' /></div><span style='white-space: pre-wrap; word-break: break-word;' ext:qtip='{0}'>{1}</span>`, fullMessage, latestManualNote);
					}
				}
				return '';
			},
			filter: {
				convert: function(value) {
					return Array.isArray(value) && value.length > 0 ? Ext.data.Types.AUTO.convert(value[0].note) : Ext.data.Types.AUTO.convert(value.note);
				}
			},
			exportColumnValueConverter: 'reconciliationNotesConverter'
		},
		{header: 'Run Date', width: 50, dataIndex: 'runDate', hidden: true},
		{
			header: 'Updated By', width: 45, dataIndex: 'updateUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'updateUserId', url: 'securityUserListFind.json', displayField: 'label'},
			renderer: Clifton.security.renderSecurityUser,
			exportColumnValueConverter: 'securityUserColumnReversableConverter'
		},
		{header: 'Updated On', width: 50, dataIndex: 'updateDate', hidden: true}
	],
	initColumns: function() {
		const gridPanel = this;
		const sideBySideView = gridPanel.dataView ? gridPanel.dataView === 'Side/Side' : gridPanel.getWindow().defaultData.dataView === 'Side/Side';
		gridPanel.columns = TCG.clone(gridPanel.columns);

		const runDate = TCG.parseDate(gridPanel.getWindow().defaultData.runDate).format('m/d/Y');
		const definitionId = gridPanel.getWindow().defaultData.reconciliationDefinition.id;
		let serviceUrl = 'reconciliationDefinitionFieldListByDefinitionId.json';
		if (gridPanel.childWindow) {
			serviceUrl = 'reconciliationDefinitionFieldListByParentDefinitionId.json';
		}
		const requestUrl = `${serviceUrl}?requestedPropertiesRoot=data&requestedProperties=objectData|id|field.name|field.defaultDataType|naturalKey|reconcilable|adjustable|hidden|dataType|displayOrder|reconciliationDefinitionFieldMappingMap`;
		const columnList = TCG.data.getData(`${requestUrl}&definitionId=${definitionId}&activeOnDate=${runDate}`, gridPanel);
		columnList.forEach(column => {
			if (sideBySideView) {
				const numeric = gridPanel.isColumnNumeric(column);
				const primarySourceName = gridPanel.getPrimarySourceName();
				const secondarySourceName = gridPanel.getSecondarySourceName();
				//Hide all columns by default in side by side unless they are a natural key, reconcilable, or numeric
				if (!(numeric || column.naturalKey || column.reconcilable)) {
					column.hidden = true;
				}
				if (numeric) {
					const fieldName = column.field.name;
					column.field.name = primarySourceName + ' ' + fieldName;
					//Override using fieldName as dataIndex - this is because sourceName is user-defined and is not necessarily a proper fieldName
					column.dataIndex = 'Primary ' + fieldName;
					column.exportDataIndex = `objectData["${column.dataIndex}"]`;
					gridPanel.addColumn(column);
					//Override using fieldName as dataIndex - this is because sourceName is user-defined and is not necessarily a proper fieldName
					column.dataIndex = 'Secondary ' + fieldName;
					column.exportDataIndex = `objectData["${column.dataIndex}"]`;
					column.field.name = secondarySourceName + ' ' + fieldName;
					gridPanel.addColumn(column);
					//Clear dataIndex
					column.dataIndex = undefined;
					column.exportDataIndex = undefined;
					column.field.name = fieldName + ' Diff';
					gridPanel.addColumn(column);
					if (TCG.isEquals(fieldName, 'Cash Balance')) {
						column.field.name = 'Adjustment Amount';
						gridPanel.addColumn(column);
						column.field.name = 'Adjustment Amount Diff';
						gridPanel.addColumn(column);
					}
				}
				else {
					gridPanel.addColumn(column);
				}
			}
			else {
				gridPanel.addColumn(column);
			}
		});
	},
	addColumn: function(column) {
		const gridPanel = this;
		if (gridPanel.isColumnNumeric(column)) {
			column.align = 'right';
			column.summaryRenderer = value => TCG.renderAmount(value, true, gridPanel.getNumericColumnFormat(column));
			column.summaryCalculation = function(value, record, field, data, col) {
				let summaryValue = value;
				const selectedRow = col.gridPanel.rowSelectionModel.getSelections().find(row => row.id === record.id);
				if (selectedRow) {
					const valueList = record.get(field);
					for (let i = 0; i < valueList.length; i++) {
						const value = valueList[i];
						if (record.data['source'][i].name === col.gridPanel.getPrimarySourceName()) {
							summaryValue += value;
						}
						else {
							summaryValue -= value;
						}
					}
				}
				return summaryValue;
			};
		}
		//Insert the custom columns before 'breakDays'
		let breakDaysIndex;
		for (let i = 0; i < gridPanel.columns.length; i++) {
			const column = gridPanel.columns[i];
			if (column.dataIndex === 'breakDays') {
				breakDaysIndex = i;
			}
		}
		const isNumeric = gridPanel.isColumnNumeric(column);
		const dataType = column.dataType ? column.dataType : column.field.defaultDataType;
		gridPanel.columns.splice(breakDaysIndex, 0, {
			text: column.field.name,
			name: column.field.name,
			header: column.field.name,
			tooltip: column.tooltip || null,
			dataType: dataType,
			dataIndex: column.dataIndex || column.field.name,
			exportDataIndex: column.exportDataIndex || `objectData["${column.field.name}"]`,
			exportColumnValueConverter: 'reconciliationObjectDataConverter',
			filter: {
				showEqualsFilter: !isNumeric,
				showNotEqualsFilter: !isNumeric,
				showContainsFilter: !isNumeric,
				showNotContainsFilter: !isNumeric,
				convert: getFilterConvertFunctionForDataType(dataType),
				type: getTypeForDataType(dataType)
			},
			sortable: column.sortable || true,
			hidden: column.hidden || false,
			align: column.align,
			width: column.width,
			adjustable: column.adjustable || false,
			listeners: column.listeners,
			convert: getConvertFunctionForDataType(dataType),
			renderer: column.renderer || function(value, metadata, record) {
				return gridPanel.renderValueList(value, column, record, metadata);
			},
			summaryRenderer: column.summaryRenderer,
			summaryTotalCondition: column.summaryTotalCondition,
			summaryCalculation: column.summaryCalculation,
			summaryType: column.summaryType
		});

		function getTypeForDataType(dataType) {
			switch (dataType) {
				case 'MONEY':
				case 'PRICE':
				case 'QUANTITY':
					return 'float';
				case 'DATE':
					return 'date';
				default:
					return 'auto';
			}
		}

		function getFilterConvertFunctionForDataType(dataType) {
			let conversionFunction;
			switch (dataType) {
				case 'MONEY':
				case 'PRICE':
				case 'QUANTITY':
					conversionFunction = Ext.data.Types.FLOAT;
					break;
				case 'DATE':
					conversionFunction = Ext.data.Types.DATE;
					break;
				default:
					conversionFunction = Ext.data.Types.AUTO;
			}
			return function(valueList) {
				const convertedValueList = [];
				if (Array.isArray(valueList)) {
					valueList.forEach(value => convertedValueList.push(conversionFunction.convert(value)));
				}
				return convertedValueList.length > 0 ? convertedValueList[0] : valueList;
			};
		}

		function getConvertFunctionForDataType(dataType) {
			let conversionFunction;
			switch (dataType) {
				case 'MONEY':
				case 'PRICE':
				case 'QUANTITY':
					conversionFunction = Ext.data.Types.FLOAT;
					break;
				case 'DATE':
					conversionFunction = Ext.data.Types.DATE;
					break;
				default:
					conversionFunction = Ext.data.Types.AUTO;
			}
			return function(valueList) {
				if (valueList.map) {
					return valueList.map(value => conversionFunction.convert(value));
				}
				return valueList;
			};
		}
	},
	renderValueList: function(valueList, column, rowData, metadata) {
		const gridPanel = this;
		let displayValue = '';
		//I wanted to be able to do this in the column definition - make it dynamically evaluate an expression here?
		if (column.field.name === 'Holding Account') {
			//TODO - should show holding account name...
			column.tooltip = rowData.json['Holding Account'];
		}
		//Side by side view - just always pull the primary value.
		const finalValueList = gridPanel.dataView === 'Side/Side' ? valueList.length >= 1 ? valueList[0] : valueList : valueList;
		Ext.each(finalValueList, function(value) {
			let fieldValue = value;
			fieldValue = gridPanel.applyDataTypeFormatting(fieldValue, column);
			fieldValue = gridPanel.applyErrorHighlighting(fieldValue, column, rowData, metadata);
			fieldValue = gridPanel.applyInvalidImage(fieldValue, column, rowData);
			if (column.tooltip) {
				displayValue += `<span style='display: block; width: 100%; height: 16px; float: left;' ext:qtip='${column.tooltip}'>${fieldValue}</span>`;
			}
			else {
				displayValue += `<span style='display: block; width: 100%; height: 16px; float: left;'>${fieldValue}</span>`;
			}
		});
		return displayValue;
	},
	applyErrorHighlighting(fieldValue, column, rowData, metadata) {
		const gridPanel = this;
		const newValue = fieldValue;
		if (gridPanel.isColumnNumeric(column)) {
			const errors = rowData.json.errors;
			if (errors) {
				errors.forEach(error => {
					if (error.fieldName === column.field.name) {
						metadata.css = 'reconciliationMismatch';
						column.tooltip = `${error.fieldName} Difference: ${error.diff}`;
					}
				});
			}
		}
		return newValue;
	},
	applyInvalidImage(fieldValue, column, rowData) {
		let newValue = fieldValue;
		switch (column.field.name) {
			case 'Holding Account':
				if (!rowData.json.reconciliationInvestmentAccountId) {
					newValue = TCG.renderIconWithTooltip('warning-red', 'Invalid Account') + `&nbsp;${fieldValue || ''}`;
				}
				break;
			case 'Investment Security':
				if (!rowData.json.reconciliationInvestmentSecurityId) {
					newValue = TCG.renderIconWithTooltip('warning-red', 'Invalid Security') + `&nbsp;${fieldValue || ''}`;
				}
				break;
			default:
				// Do nothing
		}
		return newValue;
	},
	applyDataTypeFormatting: function(renderedValue, column) {
		const gridPanel = this;
		let newValue = renderedValue;
		if (newValue) {
			const dataType = column.dataType ? column.dataType : column.field.defaultDataType;
			switch (dataType) {
				case 'MONEY':
					newValue = TCG.renderAmount(renderedValue, false, '0,000.00');
					break;
				case 'PRICE':
					newValue = TCG.numberFormat(renderedValue, '0,000', true, 6);
					break;
				case 'QUANTITY':
					// maxSuffixLength includes the decimal point
					newValue = TCG.numberFormat(renderedValue, '0,000', true, 7);
					break;
				case 'DATE':
					newValue = renderedValue.format('m/d/Y');
					break;
				default:
					// Use standard formatting: replace spaces with non-breaking spaces to maintain whitespace.
					if (renderedValue.replace) {
						newValue = renderedValue.replace(/ /g, '\u00a0');
					}
			}
		}
		return newValue ? newValue : gridPanel.isColumnNumeric(column) ? 0 : '&nbsp;';
	}
});
Ext.reg('reconciliation-run-object-detail-grid', Clifton.reconciliation.run.ReconciliationRunObjectDetailGrid);

Clifton.reconciliation.run.ReconciliationRunDetailGrid = Ext.extend(Clifton.reconciliation.run.ReconciliationRunObjectDetailGrid, {
	beanCollectionToDataTableConverter: 'reconciliationRunObjectDetailDataTableConverter',
	plugins: 'gridsummary',
	rowSelectionModel: 'multiple',
	getRowSelectionModel: function() {
		if (typeof this.rowSelectionModel != 'object') {
			this.singleSelect = this.rowSelectionModel !== 'multiple';
			this.rowSelectionModel = new Ext.grid.RowSelectionModel({
				singleSelect: this.rowSelectionModel !== 'multiple',
				listeners: {
					'rowselect': function(model, rowIndex, record) {
						// update the grand totals if applicable
						const gridpanel = model.grid.ownerCt;
						const gridSummaryPlugin = (gridpanel.plugins || []).find(plugin => plugin.ptype === 'gridsummary');
						if (gridSummaryPlugin) {
							gridSummaryPlugin.refreshGrandTotal();
						}
					},
					'rowdeselect': function(model, rowIndex, record) {
						// update the grand totals if applicable
						const gridpanel = model.grid.ownerCt;
						const gridSummaryPlugin = (gridpanel.plugins || []).find(plugin => plugin.ptype === 'gridsummary');
						if (gridSummaryPlugin) {
							gridSummaryPlugin.refreshGrandTotal();
						}
					}
				}
			});
		}
		return this.rowSelectionModel;
	},
	listeners: {
		afterReload: function(gridPanel) {
			//Used to update the formPanel default data (to update counts) when the grid is reloaded.
			TCG.data.getDataPromise('reconciliationRun.json?id=' + gridPanel.getWindow().getMainFormId(), gridPanel)
					.then(function(run) {
						const window = gridPanel.getWindow();
						const formPanel = TCG.getChildByName(window, 'reconciliationRunFormPanel');
						const defaultData = window.defaultData;
						defaultData['runDetails'] = run.runDetails;
						if (!defaultData.approvedByUser) {
							defaultData['approvedByUser'] = {displayName: 'Not Approved'};
						}
						window.setTitle(run.label);
						formPanel.getForm().setValues(defaultData);
					});
		},
		afterrender: function(gridPanel) {
			gridPanel.superclass.apply(this, arguments);
			gridPanel.grid.store.addListener('load', (...args) => gridPanel.updateProgressBar(...args));
			gridPanel.updateToolbarLabels(gridPanel);
			const el = gridPanel.getEl();
			el.on('contextmenu', function(e, target) {
				e.preventDefault();
				const grid = gridPanel.grid;
				const contextMenuRowIndex = grid.view.findRowIndex(target);
				if (TCG.isNotNull(contextMenuRowIndex) && contextMenuRowIndex !== false) {
					const menuItems = [];
					if (!gridPanel.hideAdjustingJournalTypes()) {
						const adjustmentMenuItems = gridPanel.getAdjustmentMenuItems(gridPanel);
						menuItems.push({
							text: 'Adjustments',
							iconCls: 'diff',
							menu: {
								items: adjustmentMenuItems
							}
						});
					}
					menuItems.push({
						text: 'Rules',
						iconCls: 'diff',
						menu: {
							items: gridPanel.getRuleMenuItems(gridPanel)
						}
					});
					gridPanel.grid.drillDownMenu = new Ext.menu.Menu({
						items: menuItems
					});
					TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(gridPanel, grid.drillDownMenu, contextMenuRowIndex, grid.view.findCellIndex(target), e);
				}
			});
		}
	},
	editor: {
		addEnabled: false,
		deleteEnabled: false,
		detailPageClass: 'Clifton.reconciliation.run.ReconciliationRunObjectDetailWindow',
		getDetailPageId: function(gridPanel, row) {
			return row.get('id');
		},
		getDefaultData: function(gridPanel, record, detailPageClass) {
			const defaultData = this.getWindow().defaultData;
			defaultData['reconciliationRunObjectId'] = record.get('id');
			defaultData['dataView'] = gridPanel.dataView;
			return defaultData;
		}
	},
	getLoadParams: function(firstLoad) {
		const gridPanel = this;
		const params = {
			runId: this.getWindow().defaultData.id,
			dataView: this.dataView
		};
		gridPanel.appendInvalidSecurityAccountFilter(gridPanel, params);
		return gridPanel.getReconcileStatusFilter(firstLoad, params);
	},
	getReconcileStatusFilter(firstLoad, params) {
		const gridPanel = this;
		const reconcileStatusFilter = TCG.getChildByName(this.getTopToolbar(), 'reconcileStatusId');
		const reconcileStatus = reconcileStatusFilter.getValueObject();
		if (!reconcileStatus && firstLoad) {
			const reconcileStatusName = gridPanel.getWindow().defaultData.reconcileStatusName ? gridPanel.getWindow().defaultData.reconcileStatusName : 'NOT_RECONCILED';
			return Clifton.reconciliation.getReconciliationReconcileStatusDataPromise(reconcileStatusName)
					.then(function(reconcileStatus) {
						reconcileStatusFilter.setValue(reconcileStatus.label);
						gridPanel.setFilterValue('reconcileStatus.name', reconcileStatus.id);
						params['reconcileStatusId'] = reconcileStatus.id;
						return params;
					});
		}
		return params;
	},
	updateProgressBar: function(store, rows, params) {
		const totalRecords = this.getFormPanel().getFormValue('runDetails.statusMap.totalCount');
		const reconciledRecords = this.getFormPanel().getFormValue('runDetails.statusMap.totalReconciledCount');
		const status = reconciledRecords / totalRecords;
		const text = String.format('{0}/{1}', reconciledRecords, totalRecords);
		const statusBar = this.getFormPanel().getComponent('overallReconciliationStatusBar');
		statusBar.updateProgress(status, text);
	},
	addToolbarButtons: function(toolbar, grid) {
		if (!this.hideShowViewToolbarButtons) {
			grid.addSwitchViewToolbarButton(toolbar, grid);
		}
		toolbar.add({
			text: 'Use Primary',
			tooltip: 'Force Reconcile - Reconcile using the primary source values',
			iconCls: 'arrow-right-green',
			scope: grid,
			handler: function(toolbarButton, event) {
				const gridPanel = this;
				const formPanel = TCG.getChildByName(gridPanel.getWindow(), 'reconciliationRunFormPanel');
				const definitionId = formPanel.getFormValue('reconciliationDefinition.id');
				TCG.createComponent('Clifton.reconciliation.run.AddReconciliationNotesWindow', {
					openerCt: this,
					defaultData: {definitionId: definitionId, runObjectIds: gridPanel.getSelectedRunObjectIds(gridPanel.grid), reconcileToPrimary: true},
					params: {saveUrl: 'reconciliationRunObjectListReconcile.json'}
				});
			}
		});
		// only add the 'Use Their Amount' for Activity Recs - should NOT be available for Position Recs
		toolbar.add('-');
		toolbar.add({
			text: 'Use Secondary',
			tooltip: 'Force Reconcile - Reconcile using the secondary source values',
			iconCls: 'arrow-left-green',
			scope: grid,
			handler: function(toolbarButton, event) {
				const gridPanel = this;
				const formPanel = TCG.getChildByName(gridPanel.getWindow(), 'reconciliationRunFormPanel');
				const definitionId = formPanel.getFormValue('reconciliationDefinition.id');
				TCG.createComponent('Clifton.reconciliation.run.AddReconciliationNotesWindow', {
					openerCt: this,
					defaultData: {definitionId: definitionId, runObjectIds: gridPanel.getSelectedRunObjectIds(gridPanel.grid), reconcileToPrimary: false},
					params: {saveUrl: 'reconciliationRunObjectListReconcile.json'}
				});
			}
		});
		toolbar.add('-');
		toolbar.add({
			text: 'Unreconcile',
			tooltip: 'Revert the manual reconcile.',
			iconCls: 'transfer',
			scope: grid,
			handler: function(toolbarButton, event) {
				const gridPanel = this;
				const formPanel = TCG.getChildByName(gridPanel.getWindow(), 'reconciliationRunFormPanel');
				const definitionId = formPanel.getFormValue('reconciliationDefinition.id');
				TCG.createComponent('Clifton.reconciliation.run.AddReconciliationNotesWindow', {
					openerCt: this,
					defaultData: {definitionId: definitionId, runObjectIds: gridPanel.getSelectedRunObjectIds(gridPanel.grid)},
					params: {saveUrl: 'reconciliationRunObjectListUnreconcile.json'}
				});
			}
		});
		toolbar.add('-');
		if (!this.hideMatchToolbarButtons) {
			toolbar.add({
				text: 'Match',
				tooltip: 'Groups the selected records together',
				iconCls: 'link',
				scope: grid,
				handler: function(toolbarButton, event) {
					const gridPanel = this;
					const formPanel = TCG.getChildByName(gridPanel.getWindow(), 'reconciliationRunFormPanel');
					const definitionId = formPanel.getFormValue('reconciliationDefinition.id');
					TCG.createComponent('Clifton.reconciliation.run.AddReconciliationNotesWindow', {
						openerCt: this,
						defaultData: {definitionId: definitionId, runObjectIds: gridPanel.getSelectedRunObjectIds(gridPanel.grid)},
						params: {saveUrl: 'reconciliationRunObjectListMatch.json'}
					});
				}
			});
			toolbar.add('-');
			toolbar.add({
				text: 'Unmatch',
				tooltip: 'Un-groups the records that were previously matched',
				iconCls: 'link-undo',
				scope: grid,
				handler: function(toolbarButton, event) {
					const gridPanel = this;
					const formPanel = TCG.getChildByName(gridPanel.getWindow(), 'reconciliationRunFormPanel');
					const definitionId = formPanel.getFormValue('reconciliationDefinition.id');
					TCG.createComponent('Clifton.reconciliation.run.AddReconciliationNotesWindow', {
						openerCt: this,
						defaultData: {definitionId: definitionId, runObjectIds: gridPanel.getSelectedRunObjectIds(gridPanel.grid)},
						params: {saveUrl: 'reconciliationRunObjectListUnmatch.json'}
					});
				}
			});
			toolbar.add('-');
		}
		if (!this.hideAdjustingJournalTypes()) {
			const menuItems = this.getAdjustmentMenuItems(this);
			toolbar.add({
				text: 'Adjustments',
				iconCls: 'diff',
				menu: {
					xtype: 'menu',
					items: menuItems
				}
			});
			toolbar.add('-');
		}
		const approvedByUser = grid.getWindow().defaultData.approvedByUser;
		if (TCG.isBlank(approvedByUser) || (approvedByUser && approvedByUser.displayName === 'Not Approved')) {
			toolbar.add({
				text: 'Approve',
				tooltip: 'Approve the Current Run',
				iconCls: 'row_reconciled',
				handler: function() {
					const win = grid.getWindow();
					const loader = new TCG.data.JsonLoader({
						waitTarget: grid.ownerCt,
						waitMsg: 'Saving Run...',
						params: {id: win.getMainFormId()},
						onLoad: function(record, conf) {
							grid.getWindow().openerCt.reload();
							grid.getWindow().closeWindow();
						}
					});
					loader.load('reconciliationRunApprove.json');
				}
			});
		}
		else {
			toolbar.add({
				text: 'Unapprove',
				tooltip: 'Unapprove the Current Run',
				iconCls: 'cancel',
				handler: function() {
					const win = grid.getWindow();
					TCG.createComponent('Clifton.reconciliation.run.AddReconciliationNotesWindow', {
						defaultData: {id: win.getMainFormId()},
						openerCt: grid,
						params: {saveUrl: 'reconciliationRunUnapprove.json'},
						onCloseEvent: function(window) {
							//Closes the run detail window and refreshes the runList window
							window.openerCt.getWindow().openerCt.reload();
							window.openerCt.getWindow().closeWindow();
						}
					});
				}
			});
		}
		toolbar.add('-');
		toolbar.add({
			text: 'Note',
			tooltip: 'Adds the same note to all selected rows.',
			iconCls: 'pencil',
			scope: grid,
			handler: function(toolbarButton, event) {
				const gridPanel = this;
				const formPanel = TCG.getChildByName(gridPanel.getWindow(), 'reconciliationRunFormPanel');
				const definitionId = formPanel.getFormValue('reconciliationDefinition.id');
				const runObjectIds = gridPanel.getSelectedRunObjectIds(gridPanel.grid);
				if (runObjectIds && runObjectIds.length < 1) {
					TCG.showError('You must select at least one row to add a note to.', 'No Row(s) Selected.');
					return;
				}
				TCG.createComponent('Clifton.reconciliation.run.AddReconciliationNotesWindow', {
					openerCt: this,
					defaultData: {definitionId: definitionId, runObjectIds: runObjectIds},
					params: {saveUrl: 'reconciliationRunObjectDataNoteListSave.json'}
				});
			}
		});
		toolbar.add('-');
	},
	addToolbarButtonsAfter: function(toolbar, grid) {
		toolbar.add({
			text: '',
			tooltip: 'Clears the combo boxes and reloads the grid to show all items',
			iconCls: 'clear',
			scope: grid,
			handler: function(toolbarButton, event) {
				const gridPanel = this;
				TCG.getChildByName(toolbar, 'investmentSecurityOrAccountIdIsNull').toggle(false, false);
				TCG.getChildByName(toolbar, 'matchStatusId').setValue('');
				TCG.getChildByName(toolbar, 'reconcileStatusId').setValue('');
				gridPanel.setFilterValue('matchStatus.name', '');
				gridPanel.setFilterValue('reconcileStatus.name', '');
				gridPanel.reload();
			}
		});
	},
	addSwitchViewToolbarButton: function(toolbar, gridPanel) {
		const win = this.getWindow();
		const defaultData = win.defaultData;
		const toolbarViewName = gridPanel.dataView === 'Top/Bottom' ? 'Side/Side' : 'Top/Bottom';
		toolbar.add({
			text: toolbarViewName,
			tooltip: `Opens the ${toolbarViewName} View in a new window`,
			iconCls: 'views',
			scope: gridPanel,
			handler: function(toolbarButton, event) {
				defaultData.dataView = toolbarViewName;
				// retrieve the component id so if the window is already open it will go to the existing window instead of opening a new window
				TCG.createComponent('Clifton.reconciliation.run.ReconciliationRunDetailWindowSelector', {
					defaultData: defaultData,
					params: {id: win.getMainFormId()},
					openerCt: win
				});
			}
		});
		toolbar.add('-');
	},
	configureToolsMenu:function(menu){
		menu.add({
			text: 'Export to Email',
			iconCls: 'email',
			menu: {
				items: [
					{
						text: 'All Columns',
						iconCls: 'email',
						handler: function() {
							const grid = TCG.getParentByClass(this, TCG.grid.GridPanel);
							const runTitle = grid.getWindow().title;
							const rows = grid.getRowSelectionModel().getSelections();
							if (rows === null || rows.length < 1) {
								TCG.showError('You must select at least one row to export.', 'No Row(s) Selected.');
							}
							else {

								TCG.generateEmailFile('', 'text/html', grid.generateEmailExportBody(rows, grid, false, ['Status', 'Identifier']), runTitle, '', '', '');
							}
						}
					},
					{
						text: 'Visible Columns',
						iconCls: 'email',
						handler: function() {
							const grid = TCG.getParentByClass(this, TCG.grid.GridPanel);
							grid.doLayout();
							const runTitle = grid.getWindow().title;
							const rows = grid.getRowSelectionModel().getSelections();
							if (rows === null || rows.length < 1) {
								TCG.showError('You must select at least one row to export', 'No Row(s) Selected.');
							}
							else {
								TCG.generateEmailFile('', 'text/html', grid.generateEmailExportBody(rows, grid, true, ['Status', 'Identifier']), runTitle, '', '', '');
							}
						}
					}
				]
			}
		});

	},
	generateEmailExportBody: function(rows, grid, isVisibleColumns, excludedColumns) {
		const runTitle = grid.getWindow().title;
		const columnModel = grid.getColumnModel();
		const columnList = [];
		for (let i = 0; i < columnModel.getColumnCount(false); i++) {
			const column = columnModel.getColumnAt(i);
			if (!excludedColumns.includes(column.header)) {
				if (!column.hidden || !isVisibleColumns) {
					columnList.push(column);
				}
			}
		}
		let emailBody = '<!DOCTYPE html><html><head><style>p{font-family:arial,sans-serif;font-size:14px;}</style></head><body>'
			+ '<p>' + runTitle + '</p>'
			+ '<table style=\'border:1px solid black;  border-collapse: collapse \'><tr>';
		columnList.forEach(column => emailBody += '<th style=\'border:1px solid black;  border-collapse: collapse \'>' + column.header + '</th>');
		emailBody += '</tr>';
		for (const row of rows) {
			emailBody += '<tr>';
			for (const column of columnList) {
				emailBody += '<td style=\'border:1px solid black;  border-collapse: collapse \'>';
				if (row.data[column.dataIndex] != null) {
					if (grid.dataView === 'Top/Bottom') {
						emailBody += this.getTobBottomValueForRowAndColumn(row, column);
					}
					else {
						emailBody += this.getSideBySideValueForRowAndColumn(row, column);
					}
				}
				emailBody += '</td>';
			}
			emailBody += '</tr>';
		}
		emailBody += '</table>'
			+ '</body></html>';
		return emailBody;
	},
	getSideBySideValueForRowAndColumn: function(row, column) {
		const value = row.data[column.dataIndex];
		if (value.length != null && typeof value !== 'string') {
			const displayableValues = [];
			for (let i = 0; i < value.length; i++) {
				displayableValues.push(this.getDisplayValue(value[i]));
			}
			// removes duplicates and empty/null string values
			return displayableValues.filter(val => val).filter((val, index) => displayableValues.indexOf(val) === index).join('/');
		}
		return this.getDisplayValue(value);
	},
	getTobBottomValueForRowAndColumn: function(row, column) {
		if (Array.isArray(row.data[column.dataIndex])) {
			let formattedValue = '<table>';
			for (const value of row.data[column.dataIndex]) {
				formattedValue += '<tr><td>' + this.getDisplayValue(value) + '</td></tr>';
			}
			formattedValue += '</table>';
			return formattedValue;
		}
		return this.getDisplayValue(row.data[column.dataIndex]);
	},
	getDisplayValue: function(value) {
		let displayValue = '';
		if (value != null) {
			if (value instanceof Date) {
				displayValue = value.dateFormat('m/d/y');
			}
			else if (typeof value === 'object') {
				if (value.label) {
					displayValue = value.label;
				}
				else if (value.note) {
					displayValue = value.note;
				}
				else {
					displayValue = value;
				}
			}
			else {
				displayValue = value;
			}
		}
		return displayValue;
	},
	getPrimarySourceName: function() {
		return this.getWindow().getPrimarySourceName();
	},
	getSecondarySourceName: function() {
		return this.getWindow().getSecondarySourceName();
	},
	getSelectedRunObjectIds: function(grid) {
		// get a list of unique run object ids from the selection
		const selectionModel = grid.getSelectionModel();
		return (selectionModel.getSelections() || []).map(v => v.get('id')).distinct();
	},
	hideAdjustingJournalTypes: function() {
		return !(this.isCashReconciliationWindow() || this.isTransactionReconciliationWindow());
	},
	getAdjustmentField() {
		let adjustableColumn;
		this.getColumnModel().config.forEach(column => {
			if (TCG.isTrue(column.adjustable)) {
				adjustableColumn = column.dataIndex;
			}
		});
		return adjustableColumn;
	},
	getAdjustmentMenuItems: function(gridPanel) {
		const data = TCG.data.getData('reconciliationEventObjectDataTypeListFind.json', gridPanel, 'reconciliation.EventObjectDataTypeList');
		const menuItems = [];
		data.forEach(dataType =>
				menuItems.push({
					text: dataType.label,
					tooltip: dataType.description,
					iconCls: dataType.iconClass,
					scope: gridPanel,
					handler: TCG.partial(this.createAdjustment, this, dataType)
				}));
		return menuItems;
	},
	createAdjustment: function(gridPanel, dataType) {
		const selectionModel = gridPanel.grid.getSelectionModel();
		const adjustmentField = this.getAdjustmentField();
		if (TCG.isBlank(adjustmentField)) {
			TCG.showError('No column is configured to be the \'adjustable\' value! Check that the definition is configured properly!');
			return;
		}
		if (selectionModel.getCount() === 0) {
			TCG.showError('You must select at least one row to create an adjustment for.', 'No Row(s) Selected.');
			return;
		}
		const runObjectIds = [];
		const eventObjectList = [];
		const formpanel = TCG.getChildrenByClass(gridPanel.getWindow(), Ext.form.FormPanel);
		const selections = selectionModel.getSelections();
		selections.forEach(selection => {
			//We want to check for an existing runObjectId - if it exists, then we want to find the difference
			//between the primary source adjustment field and the secondary source adjustment and use that as the amount
			//for the eventObjectData; if not existing, then process the selection normally.
			const runObjectId = selection.data['id'];
			let existingEventObjectIndex = -1;
			for (let i = 0; i < eventObjectList.length; i++) {
				const eventObject = eventObjectList[i];
				if (eventObject.runObject.id === runObjectId) {
					existingEventObjectIndex = i;
				}
			}
			if (existingEventObjectIndex !== -1) {
				const primarySourceId = formpanel[0].getFormValue('reconciliationDefinition.primarySource').id;
				const amount = TCG.isEquals(primarySourceId, selection.data['SourceId']) ? eventObjectList[existingEventObjectIndex].eventObjectData.amount - selection.data[adjustmentField] : selection.data[adjustmentField] - eventObjectList[existingEventObjectIndex].eventObjectData.amount;
				eventObjectList[existingEventObjectIndex].eventObjectData.amount = TCG.isTrue(dataType.absolute) ? Math.abs(amount) : amount;
			}
			else {

				let amount = 0;
				if (dataType.eventObjectCategory === 'ADJUSTING_JOURNAL') {
					amount = parseFloat(TCG.numberFormat((selection.data[adjustmentField][1] - selection.data[adjustmentField][0]), '0000.00'));
				}
				else {
					amount = TCG.isTrue(dataType.absolute) ? Math.abs(selection.data[adjustmentField][0]) : selection.data[adjustmentField][0];
				}


				// does settlement date (and other dates) exist on the window?
				let date = null;
				let postingDate = null;
				if (selection.data['Posting Date']) {
					postingDate = selection.data['Posting Date'][0];
				}
				if (selection.data['Settlement Date']) {
					date = selection.data['Settlement Date'][0];
				}
				else {
					if (!!postingDate) {
						date = postingDate;
					}
				}


				const eventObjectData = {
					note: '',
					amount: amount,
					//Will return an array - just select first value
					holdingAccountNumber: selection.data['Holding Account'][0],
					securityIdentifier: selection.data['Investment Security'][0],
					settlementDate: date ? date.format('Y-m-d H:m:s.ssss') : TCG.parseDate(formpanel[0].getFormValue('runDate')).format('Y-m-d H:m:s.ssss'),
					transactionDate: postingDate ? postingDate.format('Y-m-d H:m:s.ssss') : TCG.parseDate(formpanel[0].getFormValue('runDate')).format('Y-m-d H:m:s.ssss'),
					originalTransactionDate: postingDate ? postingDate.format('Y-m-d H:m:s.ssss') : TCG.parseDate(formpanel[0].getFormValue('runDate')).format('Y-m-d H:m:s.ssss'),
					eventDate: TCG.parseDate(formpanel[0].getFormValue('runDate')).format('Y-m-d H:m:s.ssss')
				};
				//Set the type info for the eventObjectData
				eventObjectData['@class'] = 'com.clifton.reconciliation.event.object.data.ReconciliationEventObjectJournalData';

				const eventObjectDataType = {id: dataType.id};

				const eventObject = {
					runObject: {
						id: runObjectId
					},
					//Possibly need to move out of inner config and add conditionally.
					parentRunObject: this.additionalLoadParams ? {id: this.additionalLoadParams.parentRunObjectId} : undefined,
					eventObjectDataType: eventObjectDataType,
					eventObjectData: eventObjectData
				};
				//Set the type info the eventObject
				eventObject['@class'] = 'com.clifton.reconciliation.event.object.ReconciliationEventObjectJournal';
				eventObjectList.push(eventObject);

				//Store the ids to pass to the preview window
				runObjectIds.push(runObjectId);
			}
		});

		const jsonData = {eventObjectWrapper: {eventObjectList: eventObjectList}};
		const loader = new TCG.data.JsonLoader({
			waitTarget: gridPanel.ownerCt,
			waitMsg: 'Saving Journal...',
			jsonData: jsonData,
			onLoad: function(record, conf) {
				if (record.errorCount > 0) {
					TCG.showError(`Failed to create ${record.errorCount} events for ${eventObjectList.length} selected run objects.<br /><br />${record.messageWithErrors}`);
				}
				//Only open the window if at least 1 eventObject didn't fail to be created
				if (record.errorCount < eventObjectList.length) {
					TCG.createComponent('Clifton.reconciliation.event.journal.ReconciliationEventJournalPreviewWindow', {
						openerCt: this,
						defaultData: {
							globalNote: dataType.defaultNote,
							eventDate: formpanel[0].getFormValue('runDate'),
							runObjectIds: runObjectIds
						}
					});
					gridPanel.reload();
				}
			}
		});
		if (this.isTransactionReconciliationWindow()) {
			new Promise(function(resolveCallback, reject) {
				const runDate = formpanel[0].getForm().findField('runDate').value;
				TCG.data.getDataValue('reconciliationParentRunObjectMapByRunObjectIds.json?runObjectIds='
						+ selections.map(selection => selection.data['id']) + '&runDate=' + runDate, gridPanel,
						function(result) {
							const parentMap = new Map(Object.entries(JSON.parse(result)));
							if (Array.from(parentMap.values()).every(s => s.length <= 1)) {
								eventObjectList.forEach(e => {
									if (parentMap.get(e.runObject.id)) {
										e.parentRunObject = {id: parentMap.get(e.runObject.id)[0]};
									}
								});
								loader.load('json/reconciliationEventObjectListSave.json');
							}
							else {
								const duplicates = Array.from(parentMap.entries()).filter(e => e[1].length > 1).map(e => e[0]);
								TCG.showError('More than one parent exists for selected run objects [' + duplicates + '].<br>Adjustments must be made in Cash Reconciliation.', 'Multiple Parents');
							}
						});
			});
		}
		else {
			//pre-pended with json/ because we are posting raw json to the server; not a form post.
			//TODO - change to be based on mime-type, not pre-pended url.
			loader.load('json/reconciliationEventObjectListSave.json');
		}
	},
	getRuleMenuItems: function(gridPanel) {
		const sideBySideView = gridPanel.dataView === 'Side/Side';
		const menuItems = [{
			text: 'Create Exclusion Rule',
			tooltip: 'Creates an \'Exclusion\' rule.',
			iconCls: 'stop',
			scope: gridPanel,
			menu: {
				xtype: 'menu',
				items: [{
					text: 'For Account',
					tooltip: 'Create an exclusion assignment for this account.',
					iconCls: 'stop',
					scope: this,
					handler: TCG.partial(this.createExclusionRule, this, false)
				}, {
					text: 'For Account and Security',
					tooltip: 'Create an exclusion assignment for this account and security.',
					iconCls: 'stop',
					scope: this,
					handler: TCG.partial(this.createExclusionRule, this, true)
				}]
			}
		}];
		if (!sideBySideView && !gridPanel.isCashReconciliationWindow()) {
			menuItems.push({
				text: 'Create/Edit Gap Quantity Rule',
				tooltip: 'Creates a \'Gap Quantity\' rule.',
				iconCls: 'coins',
				scope: gridPanel,
				handler: TCG.partial(this.createGapQuantityComparatorRule, this)
			});
		}
		menuItems.push({
			text: 'Create/Edit Account Alias',
			tooltip: 'Creates an alias to be used to map this identifier to a valid account.',
			iconCls: 'copy',
			scope: gridPanel,
			handler: TCG.partial(this.createAccountAlias, this)
		});
		menuItems.push({
			text: 'Create/Edit Security Alias',
			tooltip: 'Creates an alias to be used to map this identifier to a valid identifier.',
			iconCls: 'copy',
			scope: gridPanel,
			handler: TCG.partial(this.createSecurityAlias, this)
		});
		return menuItems;
	},
	createGapQuantityComparatorRule: async function(gridPanel) {
		const formpanel = TCG.getChildrenByClass(gridPanel.getWindow(), Ext.form.FormPanel);
		const selectionModel = gridPanel.grid.getSelectionModel();
		if (selectionModel.getCount() !== 1) {
			TCG.showError('You must select at least one and only one row to create a gap quantity rule for.', 'No Row(s) Selected.');
		}
		else {
			const row = selectionModel.getSelected();
			const modifierText = JSON.parse(row.json.modifierText);

			if (row.data['matchStatus.name'] === 'NOT_MATCHED') {
				TCG.showError('You may only create a gap quantity rule on a matched object!');
			}

			else {
				const runDate = formpanel[0].getFormValue('runDate');
				const quantity = row.data['Quantity'][0] - row.data['Quantity'][1];
				const accountNumber = Ext.isArray(row.data['Holding Account']) ? row.data['Holding Account'][0] : row.data['Holding Account'];
				const securityIdentifier = Ext.isArray(row.data['Investment Security']) ? row.data['Investment Security'][0] : row.data['Investment Security'];
				const reconciliationDefinition = formpanel[0].getFormValue('reconciliationDefinition');

				let evaluatorBean = null;
				if (TCG.isNotNull(modifierText.evaluator)) {
					evaluatorBean = modifierText.evaluator.evaluatorBean;

					if (evaluatorBean.type.name !== 'Reconciliation Data Gap Quantity Evaluator') {
						TCG.showError('Gap quantity rules cannot replace other types of rules!', 'Incompatible Existing Rule');
					}
				}
				const promises = [
					TCG.data.getDataPromise('reconciliationDefinitionFieldByNameAndDefinitionId.json', gridPanel, {params: {name: 'Quantity', definitionId: reconciliationDefinition.id, activeOnDate: TCG.parseDate(runDate).format('m/d/Y')}}),
					TCG.data.getDataPromise('reconciliationInvestmentAccountByNumber.json', gridPanel, {params: {accountNumber: accountNumber}}),
					TCG.data.getDataPromise('reconciliationInvestmentSecurityByIdentifier.json', gridPanel, {params: {securityIdentifier: securityIdentifier}}),
					TCG.data.getDataPromise('systemBeanTypeByName.json', gridPanel, {params: {name: 'Reconciliation Data Gap Quantity Evaluator'}})
				];

				const [definitionField, investmentAccount, investmentSecurity, beanType] = await Promise.all(promises);

				if (TCG.isBlank(definitionField)) {
					TCG.showError('Gap quantity rules require a valid definition field!', 'Invalid Definition Field!');
				}
				else if (TCG.isBlank(investmentAccount)) {
					TCG.showError('Gap quantity rules require a valid investment account!', 'Invalid Investment Account!');
				}
				else if (TCG.isBlank(investmentSecurity)) {
					TCG.showError('Gap quantity rules require a valid investment security!', 'Invalid Investment Security!');
				}
				else if (TCG.isBlank(beanType) && TCG.isNull(evaluatorBean)) {
					TCG.showError('Failed to retrieve bean type!', 'Invalid Bean Type!');
				}
				else {
					const args = {
						openerCt: gridPanel,
						defaultData: {
							name: 'Gap Quantity Rule for \'' + accountNumber + ' - ' + securityIdentifier + '\'',
							definitionField: definitionField,
							reconciliationInvestmentAccount: investmentAccount,
							reconciliationInvestmentSecurity: investmentSecurity,
							evaluatorBean: {type: beanType},
							startDate: TCG.parseDate(runDate).format('Y-m-d 00:00:00'),
							quantity: quantity
						}
					};
					if (TCG.isNotNull(evaluatorBean)) {
						args.defaultData.evaluatorBean = evaluatorBean;
						args.saveUrl = 'reconciliationEvaluatorCopy.json?enableValidatingBinding=true';
						args.defaultData.id = modifierText.evaluator.id;
					}

					await TCG.createComponent('Clifton.reconciliation.definition.rule.ReconciliationEvaluatorGapQuantityPreviewWindow', args);
				}
			}
		}
	},
	createExclusionRule: async function(gridPanel, forAccountAndSecurity) {
		const selectionModel = gridPanel.grid.getSelectionModel();
		if (selectionModel.getCount() !== 1) {
			TCG.showError('You may select one, and only one row to create an exclusion rule for.', 'Invalid Selection.');
		}
		else {
			const row = selectionModel.getSelected();
			if (row) {
				if (row.data['source'].length > 1) {
					TCG.showError('You may not exclude matched objects!', 'Invalid Selection.');
				}
				else {
					const formpanel = TCG.getChildrenByClass(gridPanel.getWindow(), Ext.form.FormPanel);
					const runId = gridPanel.getWindow().getMainFormId();
					const startDate = formpanel[0].getFormValue('runDate');
					const source = row.data['source'][0];
					const accountNumber = row.data['Holding Account'];
					const securityIdentifier = row.data['Investment Security'];
					const [investmentAccount, investmentSecurity] = await Promise.all([
						TCG.data.getDataPromise('reconciliationInvestmentAccountByNumber.json', gridPanel, {params: {accountNumber: accountNumber}}),
						TCG.data.getDataPromise('reconciliationInvestmentSecurityByIdentifier.json', gridPanel, {params: {securityIdentifier: securityIdentifier}})
					]);
					if (TCG.isBlank(source)) {
						TCG.showError('Exclusion assignments require a valid reconciliation source!', 'Invalid Source!');
					}
					let name = 'Exclusion Assignment for \'' + source.name + ' - ' + accountNumber;
					if (TCG.isTrue(forAccountAndSecurity)) {
						name += ' - ' + securityIdentifier;
					}
					name += '\'';
					const defaultData = {
						runId: runId,
						name: name,
						reconciliationSource: source,
						reconciliationInvestmentAccount: investmentAccount,
						startDate: TCG.parseDate(startDate).format('Y-m-d 00:00:00')
					};
					if (TCG.isTrue(forAccountAndSecurity)) {
						defaultData['reconciliationInvestmentSecurity'] = investmentSecurity;
					}
					if (TCG.isBlank(investmentAccount)) {
						defaultData['invalidAccountNumber'] = accountNumber;
					}
					if (TCG.isTrue(forAccountAndSecurity) && TCG.isBlank(investmentSecurity)) {
						defaultData['invalidSecurityIdentifier'] = securityIdentifier;
					}
					await TCG.createComponent('Clifton.reconciliation.exclusion.ReconciliationExclusionAssignmentWindow', {
						openerCt: gridPanel,
						defaultData: defaultData
					});
				}
			}
		}
	},
	createAccountAlias: async function(gridPanel) {
		const selectionModel = gridPanel.grid.getSelectionModel();

		if (selectionModel.getCount() !== 1) {
			TCG.showError('You may select one, and only one row to create an alias for.', 'Invalid Selection.');
		}
		else {
			const row = selectionModel.getSelected();
			const modifierText = JSON.parse(row.json.modifierText);
			if (row) {
				const sourceList = row.data['source'];

				const sourceName = sourceList[0].name;
				const [source] = await Promise.all([
					TCG.data.getDataPromise('reconciliationSourceByName.json', gridPanel, {params: {name: sourceName}})
				]);
				if (TCG.isBlank(source)) {
					TCG.showError('A valid source is required!', 'Invalid Source!');
				}
				const alias = row.data['Holding Account'];
				if (TCG.isBlank(alias)) {
					TCG.showError('A valid alias is required!', 'Invalid Account!');
				}
				else {
					const formpanel = TCG.getChildrenByClass(gridPanel.getWindow(), Ext.form.FormPanel);
					const startDate = formpanel[0].getFormValue('runDate');
					const args = {
						openerCt: gridPanel,
						defaultData: {
							reconciliationSource: source,
							alias: alias,
							startDate: TCG.parseDate(startDate).format('Y-m-d 00:00:00')
						}
					};
					if (TCG.isNotBlank(modifierText) && TCG.isNotBlank(modifierText.accountAlias)) {

						args.defaultData = modifierText.accountAlias;
						args.defaultData.startDate = TCG.parseDate(startDate).format('Y-m-d 00:00:00');
						args.defaultDataIsReal = true;
					}

					await TCG.createComponent('Clifton.reconciliation.investment.ReconciliationInvestmentAccountAliasWindow', args);
				}

			}
		}
	},
	createSecurityAlias: async function(gridPanel) {
		const selectionModel = gridPanel.grid.getSelectionModel();

		if (selectionModel.getCount() !== 1) {
			TCG.showError('You may select one, and only one row to create an alias for.', 'Invalid Selection.');
		}
		else {
			const row = selectionModel.getSelected();
			const modifierText = JSON.parse(row.json.modifierText);
			if (row) {
				const sourceList = row.data['source'];
				const sourceName = sourceList[0].name;
				const [source] = await Promise.all([
					TCG.data.getDataPromise('reconciliationSourceByName.json', gridPanel, {params: {name: sourceName}})
				]);
				if (TCG.isBlank(source)) {
					TCG.showError('A valid source is required!', 'Invalid Source!');
				}
				const alias = row.data['Investment Security'];
				if (TCG.isBlank(alias)) {
					TCG.showError('A valid alias is required!', 'Invalid Security!');
				}
				else {
					const formpanel = TCG.getChildrenByClass(gridPanel.getWindow(), Ext.form.FormPanel);
					const startDate = formpanel[0].getFormValue('runDate');
					const args = {
						openerCt: gridPanel,
						defaultData: {
							reconciliationSource: source,
							alias: alias,
							startDate: TCG.parseDate(startDate).format('Y-m-d 00:00:00')
						}
					};
					if (TCG.isNotBlank(modifierText) && TCG.isNotBlank(modifierText.securityAlias)) {
						args.defaultData = modifierText.securityAlias;
						args.defaultData.startDate = TCG.parseDate(startDate).format('Y-m-d 00:00:00');
						args.defaultDataIsReal = true;
					}
					await TCG.createComponent('Clifton.reconciliation.investment.ReconciliationInvestmentSecurityAliasWindow', args);
				}
			}
		}
	},
	updateToolbarLabels: function(gridPanel) {
		const win = gridPanel.getWindow();
		const primarySourceName = win.getPrimarySourceName();
		const secondarySourceName = win.getSecondarySourceName();
		const toolbars = gridPanel.toolbars;
		if (toolbars && toolbars[0] && toolbars[0].items && toolbars[0].items.items) {
			const primaryToolbarItem = toolbars[0].items.items.find(v => v.text === 'Use Primary');
			const secondaryToolbarItem = toolbars[0].items.items.find(v => v.text === 'Use Secondary');

			if (primaryToolbarItem && secondaryToolbarItem) {
				const primaryText = `Use ${primarySourceName}`;
				const secondaryText = `Use ${secondarySourceName}`;

				if (primaryToolbarItem.getText() !== primaryText) {
					primaryToolbarItem.setText(primaryText);
				}
				if (secondaryToolbarItem.getText() !== secondaryText) {
					secondaryToolbarItem.setText(secondaryText);
				}
			}
		}
	}
});
Ext.reg('reconciliation-run-detail-grid', Clifton.reconciliation.run.ReconciliationRunDetailGrid);
Clifton.reconciliation.run.ReconciliationRunGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'reconciliationRunListFind',
	additionalPropertiesToRequest: 'reconciliationDefinition.type',
	instructions: 'A reconciliation run tracks the overall status of all objects reconciled during a run.',
	getColumnOverrides: function() {
		if (this.getWindow().type === 'TRANSACTION') {
			return [{dataIndex: 'runDetails.statusMap.matchedCount', hidden: true}, {dataIndex: 'runDetails.statusMap.unmatchedCount', hidden: true}];
		}
		return [];
	},
	listeners: {
		afterrender: function(gridPanel) {
			const el = gridPanel.getEl();
			el.on('contextmenu', function(e, target) {
				e.preventDefault();
				const grid = gridPanel.grid;
				const contextMenuRowIndex = grid.view.findRowIndex(target);
				if (TCG.isNotNull(contextMenuRowIndex) && contextMenuRowIndex !== false) {
					grid.drillDownMenu = new Ext.menu.Menu({
						items: {
							text: 'Reconcile',
							tooltip: 'Reconcile the selected run.',
							iconCls: 'reconcile',
							scope: gridPanel,
							handler: gridPanel.reconcileRun
						}
					});
					TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(gridPanel, grid.drillDownMenu, contextMenuRowIndex, grid.view.findCellIndex(target), e);
				}
			});
		}
	},
	reconcileRun: function() {
		const gridPanel = this;
		const grid = gridPanel.grid;
		const selectionModel = grid.getSelectionModel();
		const selections = selectionModel.getSelections();
		const loader = new TCG.data.JsonLoader({
			waitTarget: grid.ownerCt,
			waitMsg: 'Reconciling...',
			params: {
				reconciliationDefinitionId: selections[0].data['reconciliationDefinition.id'],
				reconcileDate: selections[0].data['runDate'].format('m/d/Y')
			},
			onLoad: function(record, conf) {
				gridPanel.reload();
			}
		});
		loader.load('reconciliationRunProcess.json');
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Reconciliation Definition', xtype: 'toolbar-combo', name: 'reconciliationDefinitionId', width: 180, url: 'reconciliationDefinitionListFind.json', linkedFilter: 'reconciliationDefinition.name', displayField: 'name'},
			{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Reconciliation Definition Tags'}
		];
	},
	getLoadParams: function(firstLoad) {
		const gridPanel = this;

		const params = {
			readUncommittedRequested: true
		};
		const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
		if (TCG.isNotBlank(tag.getValue())) {
			params.categoryName = 'Reconciliation Definition Tags';
			params.categoryLinkFieldPath = 'reconciliationDefinition';
			params.categoryHierarchyId = tag.getValue();
		}
		return gridPanel.applyFilters(firstLoad, params);
	},
	applyFilters: function(firstLoad, params) {
		const gridPanel = this;
		if (firstLoad) {
			gridPanel.setFilterValue('runDate', {'on': Clifton.calendar.getBusinessDayFrom(-1)});
			gridPanel.setFilterValue('archived', false);

			return Clifton.reconciliation.getReconciliationTypeDataPromise(this.getWindow().typeName)
					.then(function(type) {
						gridPanel.setFilterValue('reconciliationDefinition.type.name', type.id);
					});
		}
		else {
			return params;
		}
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Definition Type', dataIndex: 'reconciliationDefinition.type.name', width: 150, hidden: true, filter: {type: 'combo', searchFieldName: 'typeId', url: 'reconciliationTypeListFind.json'}},
		{header: 'Primary Source', dataIndex: 'reconciliationDefinition.primarySource.shortName', width: 150, hidden: true, filter: false},
		{header: 'Secondary Source', dataIndex: 'reconciliationDefinition.secondarySource.shortName', width: 150, hidden: true, filter: false},
		{header: 'Definition ID', width: 15, dataIndex: 'reconciliationDefinition.id', hidden: true},
		{
			header: 'Definition Name', width: 200, dataIndex: 'reconciliationDefinition.name',
			filter: {type: 'combo', searchFieldName: 'definitionId', url: 'reconciliationDefinitionListFind.json', displayField: 'name'}
		},
		{
			header: 'Reconcile Status', width: 90, dataIndex: 'reconciliationReconcileStatus.label',
			filter: {type: 'combo', searchFieldName: 'reconcileStatusName', url: 'reconciliationReconcileStatusListFind.json'},
			renderer: function(value, metaData, r) {
				if (TCG.isEquals(value, 'Not Reconciled')) {
					return `<span style="color: red">${value}</span>`;
				}
				else if (TCG.isEquals(value, 'Manually Reconciled')) {
					return `<span style="color: blue">${value}</span>`;
				}
				else if (TCG.isEquals(value, 'Reconciled')) {
					return `<span style="color: green">${value}</span>`;
				}
			}
		},
		{header: 'Processed', width: 50, dataIndex: 'runDetails.statusMap.processedCount', type: 'int', hidden: 'true', filter: false, sortable: false, tooltip: 'Total number of rows processed from both sources. Re-importing new data will increase this number (may double count, etc.)'},
		{header: 'Total', width: 50, dataIndex: 'runDetails.statusMap.totalCount', type: 'int', filter: false, sortable: false, tooltip: 'Total number of reconciliation objects in this run (includes matched and unmatched).'},
		{header: 'Matched', width: 50, dataIndex: 'runDetails.statusMap.matchedCount', type: 'int', filter: false, sortable: false, tooltip: 'The number of records matched based on definition\'s natural key(s) between primary and secondary sources.'},
		{header: 'Not Matched', width: 50, dataIndex: 'runDetails.statusMap.notMatchedCount', type: 'int', filter: false, sortable: false, tooltip: 'The number of records NOT matched based on definition\'s natural key(s) between primary and secondary sources. (Not Matched = Total Count - Matched Count)'},
		{header: 'Reconciled', width: 50, dataIndex: 'runDetails.statusMap.reconciledCount', type: 'int', filter: false, sortable: false, tooltip: 'The number of reconciliation objects that were automatically or manually reconciled.'},
		{header: 'Not Reconciled', width: 55, dataIndex: 'runDetails.statusMap.notReconciledCount', type: 'int', filter: false, sortable: false, tooltip: 'The number of reconciliation objects that have not been reconciled. (Not Reconciled = Total Count - Reconciled Count)'},
		{header: 'Excluded', width: 40, dataIndex: 'runDetails.statusMap.excludedCount', type: 'int', filter: false, sortable: false, tooltip: 'The number of excluded source rows (for positions that we do not manage, etc.). Goes to the orphans table and is periodically purged.'},
		{header: 'Filtered', width: 40, dataIndex: 'runDetails.statusMap.filteredCount', type: 'int', filter: false, sortable: false, tooltip: 'The number of rows filtered out based on the rules on reconciliation definition. Goes to the orphans table and is periodically purged.'},
		{header: 'Errors', width: 40, dataIndex: 'runDetails.statusMap.errorCount', type: 'int', filter: false, sortable: false, tooltip: 'The number of processing errors (string characters in a numeric field, etc.)'},
		{header: 'Approved By', width: 50, dataIndex: 'approvedByUser.displayName', filter: {searchFieldName: 'approvedByUserId', displayField: 'displayName', type: 'combo', url: 'securityUserListFind.json'}},
		{header: 'Note', width: 50, dataIndex: 'note', hidden: true, filter: false},
		{header: 'Date', width: 50, dataIndex: 'runDate'},
		{header: 'Archived', width: 40, dataIndex: 'archived', type: 'boolean', hidden: true}
	]
});
Ext.reg('reconciliation-run-grid', Clifton.reconciliation.run.ReconciliationRunGrid);
