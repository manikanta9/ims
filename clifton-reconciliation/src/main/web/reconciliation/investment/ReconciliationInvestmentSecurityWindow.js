Clifton.reconciliation.investment.ReconciliationInvestmentSecurityWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Security',
	iconCls: 'stock-chart',
	width: 600,
	height: 400,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'A Reconciliation Investment Security is a simple representation of an investment security synchronized from IMS.',
		url: 'reconciliationInvestmentSecurity.json',
		labelFieldName: 'securitySymbol',
		items: [
			{fieldLabel: 'ID', name: 'id', hidden: true},
			{fieldLabel: 'Security Type', name: 'securityTypeName', xtype: 'displayfield'},
			{fieldLabel: 'Security Sub Type', name: 'securityTypeSubTypeName', xtype: 'displayfield'},
			{fieldLabel: 'Security Sub Type 2', name: 'securityTypeSubType2Name', xtype: 'displayfield'},

			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'Identifier', name: 'securityIdentifier', xtype: 'displayfield', qtip: 'ID of the security in IMS'},
			{fieldLabel: 'Security Name', name: 'securityName', xtype: 'displayfield'},
			{fieldLabel: 'Security Symbol', name: 'securitySymbol', xtype: 'displayfield'},
			{fieldLabel: 'Security Cusip', name: 'securityCusip', xtype: 'displayfield'},
			{fieldLabel: 'Security Isin', name: 'securityIsin', xtype: 'displayfield'},
			{fieldLabel: 'Security Sedol', name: 'securitySedol', xtype: 'displayfield'},
			{fieldLabel: 'Security Occ Symbol', name: 'securityOccSymbol', xtype: 'displayfield'},
			{fieldLabel: 'Currency Denomination', name: 'currencyDenomination', xtype: 'displayfield'}
		]
	}]
});
