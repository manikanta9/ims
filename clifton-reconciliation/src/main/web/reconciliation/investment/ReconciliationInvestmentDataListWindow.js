Clifton.reconciliation.investment.ReconciliationInvestmentDataListWindow = Ext.extend(TCG.app.Window, {
	id: 'reconciliationInvestmentDataListWindow',
	iconCls: 'database',
	title: 'Investment Data',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Investment Accounts',
				items: [{
					name: 'reconciliationInvestmentAccountListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'Holding Accounts that we manage for our clients.  They are usually updated from our system via a batch job. Account Aliases are used to map to our accounts when external parties use non-standard account numbers.',
					columns: [
						{header: 'ID', width: 50, dataIndex: 'id', hidden: true},
						{header: 'Account ID', width: 60, dataIndex: 'accountIdentifier', type: 'int', doNotFormat: true},
						{header: 'Account Number', width: 100, dataIndex: 'accountNumber'},
						{header: 'Account Number 2', width: 100, dataIndex: 'accountNumber2'},
						{header: 'Account Name', width: 325, dataIndex: 'accountName', defaultSortColumn: true},
						{header: 'Issuing Company', width: 200, dataIndex: 'issuingCompany'},
						{header: 'Base Currency', width: 75, dataIndex: 'baseCurrency'},
						{header: 'Workflow State', width: 125, dataIndex: 'workflowState'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentAccountWindow'
					}
				}]
			},


			{
				title: 'Investment Account Aliases',
				items: [{
					name: 'reconciliationInvestmentAccountAliasListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'alias',
					instructions: 'Aliases are used for \'memorized matches\' to map 3rd party investment accounts to corresponding accounts in our system.',
					columns: [
						{header: 'ID', width: 50, dataIndex: 'id', hidden: true},
						{header: 'Reconciliation Source', width: 125, dataIndex: 'reconciliationSource.name', filter: {type: 'combo', searchFieldName: 'sourceId', url: 'reconciliationSourceListFind.json'}},
						{header: 'Account Alias', width: 100, dataIndex: 'alias', defaultSortColumn: true},
						{header: 'Account ID', width: 50, dataIndex: 'investmentAccount.accountIdentifier', type: 'int', doNotFormat: true, filter: {searchFieldName: 'accountId'}},
						{header: 'Account Number', width: 100, dataIndex: 'investmentAccount.accountNumber', filter: {searchFieldName: 'accountNumber'}},
						{header: 'Account Number 2', width: 100, dataIndex: 'investmentAccount.accountNumber2', filter: {searchFieldName: 'accountNumber2'}},
						{header: 'Account Name', width: 200, dataIndex: 'investmentAccount.accountName', filter: {searchFieldName: 'accountName'}},
						{header: 'Issuing Company', width: 200, dataIndex: 'investmentAccount.issuingCompany', hidden: true},
						{header: 'Base Currency', width: 75, dataIndex: 'investmentAccount.baseCurrency', hidden: true},
						{
							header: 'Display Property', width: 100, dataIndex: 'displayProperty',
							renderer: function(value, metaData, r) {
								if (value === 'accountNumber') {
									return 'Account Number';
								}
								else if (value === 'accountNumber2') {
									return 'Account Number 2';
								}
								return value;
							}
						},
						{header: 'Start Date', width: 50, dataIndex: 'startDate'},
						{header: 'End Date', width: 50, dataIndex: 'endDate'}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentAccountAliasWindow'
					}
				}]
			},


			{
				title: 'Investment Securities',
				items: [{
					name: 'reconciliationInvestmentSecurityListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'Investment Securities are securities for positions held by us and are usually updated from our system via a batch job. Security Aliases are used to map to our securities when external parties use non-standard security identifiers.',
					columns: [
						{header: 'ID', width: 50, dataIndex: 'id', hidden: true},
						{header: 'Security ID', width: 60, dataIndex: 'securityIdentifier', type: 'int', doNotFormat: true, hidden: 'true'},
						{header: 'Security Type', width: 100, dataIndex: 'securityTypeName'},
						{header: 'Security Sub Type', width: 120, dataIndex: 'securityTypeSubTypeName'},
						{header: 'Security Sub Type 2', width: 120, dataIndex: 'securityTypeSubType2Name'},
						{header: 'Security Name', width: 170, dataIndex: 'securityName', defaultSortColumn: true},
						{header: 'Symbol', width: 120, dataIndex: 'securitySymbol'},
						{header: 'CUSIP', width: 100, dataIndex: 'securityCusip'},
						{header: 'ISIN', width: 100, dataIndex: 'securityIsin'},
						{header: 'SEDOL', width: 100, dataIndex: 'securitySedol'},
						{header: 'OCC Symbol', width: 100, dataIndex: 'securityOccSymbol'},
						{header: 'Currency Denomination', width: 50, dataIndex: 'currencyDenomination', hidden: true},
						{header: 'End Date', width: 50, dataIndex: 'endDate', hidden: true}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentSecurityWindow'
					}
				}]
			},


			{
				title: 'Investment Security Aliases',
				items: [{
					name: 'reconciliationInvestmentSecurityAliasListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'alias',
					instructions: 'Aliases are used for \'memorized matches\' to map non-standard 3rd party security identifiers to corresponding securities in our system.',
					columns: [
						{header: 'ID', width: 50, dataIndex: 'id', hidden: true},
						{header: 'Reconciliation Source', width: 125, dataIndex: 'reconciliationSource.name', filter: {type: 'combo', searchFieldName: 'sourceId', url: 'reconciliationSourceListFind.json'}},
						{header: 'Security Type', width: 80, dataIndex: 'investmentSecurity.securityTypeName', filter: {searchFieldName: 'securityTypeName'}},
						{header: 'Security Sub Type', width: 120, dataIndex: 'investmentSecurity.securityTypeSubTypeName', filter: {searchFieldName: 'securityTypeSubTypeName'}},
						{header: 'Security Sub Type 2', width: 100, dataIndex: 'investmentSecurity.securityTypeSubType2Name', filter: {searchFieldName: 'securityTypeSubType2Name'}},
						{header: 'Security Alias', width: 120, dataIndex: 'alias', defaultSortColumn: true},
						{header: 'Security ID', width: 50, dataIndex: 'investmentSecurity.securityIdentifier', hidden: true, type: 'int'},
						{header: 'Security Name', width: 150, dataIndex: 'investmentSecurity.securityName', hidden: true},
						{header: 'Symbol', width: 100, dataIndex: 'investmentSecurity.securitySymbol', filter: {searchFieldName: 'securitySymbol'}},
						{header: 'CUSIP', width: 100, dataIndex: 'investmentSecurity.securityCusip', filter: {searchFieldName: 'securityCusip'}},
						{header: 'ISIN', width: 100, dataIndex: 'investmentSecurity.securityIsin', filter: {searchFieldName: 'securityIsin'}},
						{header: 'SEDOL', width: 100, dataIndex: 'investmentSecurity.securitySedol', filter: {searchFieldName: 'securitySedol'}},
						{header: 'OCC Symbol', width: 100, dataIndex: 'investmentSecurity.securityOccSymbol', hidden: true},
						{
							header: 'Display Property', width: 100, dataIndex: 'displayProperty',
							renderer: function(value, metaData, r) {
								if (value === 'securitySymbol') {
									return 'Symbol';
								}
								else if (value === 'securityCusip') {
									return 'Cusip';
								}
								else if (value === 'securityIsin') {
									return 'ISIN';
								}
								else if (value === 'securitySedol') {
									return 'Sedol';
								}
								else if (value === 'securityOccSymbol') {
									return 'OCC Symbol';
								}
								return value;
							}
						},
						{header: 'Start Date', width: 60, dataIndex: 'startDate'},
						{header: 'End Date', width: 60, dataIndex: 'endDate'}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentSecurityAliasWindow'
					}
				}]
			}
		]
	}]
});
