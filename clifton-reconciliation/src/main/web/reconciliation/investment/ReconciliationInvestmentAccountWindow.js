Clifton.reconciliation.investment.ReconciliationInvestmentAccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Account',
	iconCls: 'account',
	width: 650,
	height: 275,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'A Reconciliation Investment Account is a simple representation of a holding investment account synchronized from IMS.',
		url: 'reconciliationInvestmentAccount.json',
		items: [
			{fieldLabel: 'ID', name: 'id', hidden: true},
			{fieldLabel: 'Account ID', name: 'accountIdentifier', xtype: 'displayfield', qtip: 'ID of the account in IMS'},
			{fieldLabel: 'Account Number', name: 'accountNumber', xtype: 'displayfield'},
			{fieldLabel: 'Account Number 2', name: 'accountNumber', xtype: 'displayfield'},
			{fieldLabel: 'Account Name', name: 'accountName', xtype: 'displayfield'},
			{fieldLabel: 'Issuing Company', name: 'issuingCompany', xtype: 'displayfield'},
			{fieldLabel: 'Base Currency', name: 'baseCurrency', xtype: 'displayfield'},
			{fieldLabel: 'Workflow State', name: 'workflowState', xtype: 'displayfield'}
		]
	}]
});
