Clifton.reconciliation.investment.ReconciliationInvestmentAccountAliasWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation Investment Account Alias',
	iconCls: 'account',
	width: 600,
	height: 300,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'Aliases are used for \'memorized matches\' to map 3rd party investment accounts to corresponding accounts in our system.',
		url: 'reconciliationInvestmentAccountAlias.json',
		labelFieldName: 'alias',
		items: [
			{
				fieldLabel: 'Reconciliation Source', xtype: 'combo',
				name: 'reconciliationSource.name', hiddenName: 'reconciliationSource.id', displayField: 'name',
				url: 'reconciliationSourceListFind.json', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationSourceWindow'
			},
			{fieldLabel: 'Account Alias', name: 'alias', qtip: 'The alias to map to the specified account'},
			{
				fieldLabel: 'Investment Account', xtype: 'combo',
				name: 'investmentAccount.label', hiddenName: 'investmentAccount.id', displayField: 'label',
				queryParam: 'searchPatternUsingBeginsWith', url: 'reconciliationInvestmentAccountListFind.json', detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentAccountWindow', disableAddNewItem: true
			},
			{
				fieldLabel: 'Display Property', name: 'displayProperty', hiddenName: 'displayProperty',
				mode: 'local', xtype: 'combo', allowBlank: true, qtip: 'Display property to use from the mapped security.',
				store: {
					xtype: 'arraystore',
					data: Clifton.reconciliation.investment.InvestmentAccountDisplayProperties
				}
			},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', qtip: 'Specifies the start date of this field definition.', value: new Date().format('m/d/Y')},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', qtip: 'Specifies the end date of this field definition.'}
		],
		listeners: {
			afterload: function(formPanel) {
				const openerPanel = this.getWindow().openerCt;
				if (openerPanel && openerPanel.reloadGrids) {
					openerPanel.reloadGrids(openerPanel);
					//Also reload the run window to update statuses there.
					openerPanel.getWindow().openerCt.reload();
				}
			},
			afterRender: function(formPanel) {
				//Disable all fields except endDate when editing an existing alias
				if (this.getWindow().getMainFormId()) {
					TCG.getChildByName(formPanel, 'reconciliationSource.name').readOnly = true;
					TCG.getChildByName(formPanel, 'alias').readOnly = true;
					TCG.getChildByName(formPanel, 'investmentAccount.label').readOnly = true;
					TCG.getChildByName(formPanel, 'displayProperty').readOnly = true;
					TCG.getChildByName(formPanel, 'startDate').readOnly = true;
				}
			}
		},
		addToolbarButtons: function(toolbar, formpanel) {
			const win = formpanel.getWindow();
			if (win.getMainFormId()) {
				toolbar.add({
					text: 'Delete',
					tooltip: 'Delete the Account Alias',
					iconCls: 'cancel',
					handler: function() {
						const loader = new TCG.data.JsonLoader({
							openerCt: win,
							waitTarget: formpanel.ownerCt,
							waitMsg: 'Deleting alias...',
							params: {id: win.getMainFormId()},
							onLoad: function(record, conf) {
								this.openerCt.openerCt.reload();
								this.openerCt.closeWindow();
							}
						});
						loader.load('reconciliationInvestmentAccountAliasDelete.json');
					}
				});
			}
		}
	}]
});
