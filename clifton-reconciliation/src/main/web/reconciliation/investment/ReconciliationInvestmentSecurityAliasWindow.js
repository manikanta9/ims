Clifton.reconciliation.investment.ReconciliationInvestmentSecurityAliasWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Investment Security Alias',
	iconCls: 'stock-chart',
	width: 600,
	height: 300,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'Aliases are used for \'memorized matches\' to map non-standard 3rd party security identifiers to corresponding securities in our system.',
		url: 'reconciliationInvestmentSecurityAlias.json',
		labelFieldName: 'alias',
		items: [
			{
				fieldLabel: 'Reconciliation Source', xtype: 'combo',
				name: 'reconciliationSource.name', hiddenName: 'reconciliationSource.id', displayField: 'name',
				url: 'reconciliationSourceListFind.json', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationSourceWindow'
			},
			{fieldLabel: 'Security Alias', name: 'alias', qtip: 'The alias to map to the specified security'},
			{
				fieldLabel: 'Investment Security', xtype: 'combo',
				name: 'investmentSecurity.securitySymbol', hiddenName: 'investmentSecurity.id', displayField: 'securitySymbol', requestedProps: 'endDate',
				queryParam: 'searchPatternUsingBeginsWith', url: 'reconciliationInvestmentSecurityListFind.json?', detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentSecurityWindow', disableAddNewItem: true,
				listeners: {
					select: function(combo, record, index) {
						if (record.json.endDate) {
							const endDate = TCG.getParentFormPanel(combo).getForm().findField('endDate');
							//Add 5 business days to the end date to account for brokers sending transaction data few a few days past expiration.
							endDate.setValue(Clifton.calendar.getBusinessDayFromDate(TCG.parseDate(record.json.endDate), 5).format('m/d/Y'));
						}
					}
				}
			},
			{
				fieldLabel: 'Display Property', name: 'displayProperty', hiddenName: 'displayProperty',
				mode: 'local', xtype: 'combo', allowBlank: true, qtip: 'Display property to use from the mapped security.',
				store: {
					xtype: 'arraystore',
					data: Clifton.reconciliation.investment.InvestmentSecurityDisplayProperties
				}
			},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', qtip: 'Specifies the start date of this field definition.', value: new Date().format('m/d/Y')},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', qtip: 'Specifies the end date of this field definition.'}
		],
		listeners: {
			afterload: function(formPanel) {
				const openerPanel = this.getWindow().openerCt;
				if (openerPanel && openerPanel.reloadGrids) {
					openerPanel.reloadGrids(openerPanel);
					//Also reload the run window to update statuses there.
					openerPanel.getWindow().openerCt.reload();
				}
			},
			afterRender: function(formPanel) {
				//Disable all fields except endDate when editing an existing alias
				if (this.getWindow().getMainFormId()) {
					TCG.getChildByName(formPanel, 'reconciliationSource.name').readOnly = true;
					TCG.getChildByName(formPanel, 'alias').readOnly = true;
					TCG.getChildByName(formPanel, 'investmentSecurity.securitySymbol').readOnly = true;
					TCG.getChildByName(formPanel, 'displayProperty').readOnly = true;
					TCG.getChildByName(formPanel, 'startDate').readOnly = true;

				}
			}


		},
		addToolbarButtons: function(toolbar, formpanel) {
			const win = formpanel.getWindow();
			if (win.getMainFormId()) {
				toolbar.add({
					text: 'Delete',
					tooltip: 'Delete the Security Alias',
					iconCls: 'cancel',
					handler: function() {
						const loader = new TCG.data.JsonLoader({
							openerCt: win,
							waitTarget: formpanel.ownerCt,
							waitMsg: 'Deleting alias...',
							params: {id: win.getMainFormId()},
							onLoad: function(record, conf) {
								this.openerCt.openerCt.reload();
								this.openerCt.closeWindow();
							}
						});
						loader.load('reconciliationInvestmentSecurityAliasDelete.json');
					}
				});
			}
		}
	}]
});
