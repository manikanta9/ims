Clifton.reconciliation.exclusion.ReconciliationExclusionAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation Exclusion Assignment',
	iconCls: 'stop',
	width: 1000,
	height: 525,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Exclusion Assignment',
			items: [{
				xtype: 'formpanel',
				labelWidth: 140,
				instructions: 'A Reconciliation Exclusion Assignment defines data to be excluded from reconciliation.',
				url: 'reconciliationExclusionAssignment.json?requestedMaxDepth=4',
				addToolbarButtons: function(toolbar, formpanel) {
					const win = formpanel.getWindow();
					if (win.getMainFormId()) {
						toolbar.add({
							text: 'Approve',
							tooltip: 'Approve the Reconciliation Exclusion Assignment.',
							iconCls: 'row_reconciled',
							handler: function() {
								const loader = new TCG.data.JsonLoader({
									openerCt: win,
									waitTarget: formpanel.ownerCt,
									waitMsg: 'Approving assignment...',
									params: {id: win.getMainFormId()},
									onLoad: function(record, conf) {
										//Closes the assignment window and refreshes the opener grid
										this.openerCt.openerCt.reload();
										this.openerCt.closeWindow();
									}
								});
								loader.load('reconciliationExclusionAssignmentApprove.json');
							}
						});
						toolbar.add('-');
						toolbar.add({
							text: 'Unapprove',
							tooltip: 'Unapprove the Reconciliation Exclusion Assignment.',
							iconCls: 'cancel',
							handler: function() {
								const loader = new TCG.data.JsonLoader({
									openerCt: win,
									waitTarget: formpanel.ownerCt,
									waitMsg: 'Unapproving assignment...',
									params: {id: win.getMainFormId()},
									onLoad: function(record, conf) {
										//Closes the assignment window and refreshes the opener grid
										this.openerCt.openerCt.reload();
										this.openerCt.closeWindow();
									}
								});
								loader.load('reconciliationExclusionAssignmentUnapprove.json');
							}
						});
					}
				},
				items: [
					{fieldLabel: 'Run ID', name: 'runId', hidden: true},
					{fieldLabel: 'Label', name: 'label', readOnly: true},
					{fieldLabel: 'Reconciliation Definition', name: 'reconciliationDefinition.name', hiddenName: 'reconciliationDefinition.id', xtype: 'combo', url: 'reconciliationDefinitionListFind.json', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionWindow', qtip: 'The definition this assignment applies to.'},
					{
						fieldLabel: 'Reconciliation Source', name: 'reconciliationSource.name', hiddenName: 'reconciliationSource.id', xtype: 'combo', url: 'reconciliationSourceListFind.json', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationSourceWindow', qtip: 'The source this filter applies to.',
						beforequery: function(queryEvent) {
							const combo = queryEvent.combo;
							const formPanel = TCG.getParentFormPanel(combo);
							if (!TCG.isBlank(formPanel.getFormValue('reconciliationDefinition.primarySource.id'))) {
								const ids = [];
								ids.push(formPanel.getFormValue('reconciliationDefinition.primarySource.id'));
								ids.push(formPanel.getFormValue('reconciliationDefinition.secondarySource.id'));
								combo.store.baseParams = {
									ids: ids
								};
							}
						}
					},
					{fieldLabel: 'Investment Account', name: 'reconciliationInvestmentAccount.label', hiddenName: 'reconciliationInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'reconciliationInvestmentAccountListFind.json', detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentAccountWindow', mutuallyExclusiveFields: ['invalidAccountNumber']},
					{fieldLabel: 'Security', name: 'reconciliationInvestmentSecurity.securitySymbol', hiddenName: 'reconciliationInvestmentSecurity.id', displayField: 'securitySymbol', xtype: 'combo', url: 'reconciliationInvestmentSecurityListFind.json', detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentSecurityWindow', mutuallyExclusiveFields: ['invalidSecurityIdentifier']},
					{fieldLabel: 'Invalid Account', name: 'invalidAccountNumber', mutuallyExclusiveFields: ['reconciliationInvestmentAccount.label']},
					{fieldLabel: 'Invalid Security', name: 'invalidSecurityIdentifier', mutuallyExclusiveFields: ['reconciliationInvestmentSecurity.securitySymbol']},
					{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', qtip: 'Specifies the start date of this field definition.'},
					{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', qtip: 'Specifies the end date of this field definition.'},
					{
						fieldLabel: 'Configuration Flags', xtype: 'checkboxgroup', columns: 3, qtip: 'The configuration flags determine how this filter is used; each configuration option has a more detailed description of its specific use.',
						items: [
							{boxLabel: 'Include', name: 'include', xtype: 'checkbox', qtip: 'Defines if this assignment should be used to include or exclude data.'}
						]
					},
					{fieldLabel: 'Comment', name: 'comment', xtype: 'textarea'}
				], afterload: function(formPanel) {
					const origGridPanel = this.getWindow().openerCt;
					origGridPanel.reloadGrids(origGridPanel);
					//Also reload the run window to update statuses there.
					origGridPanel.getWindow().openerCt.reload();
				}
			}]
		}, {
			title: 'Excluded Objects',
			items: [{
				xtype: 'gridpanel',
				name: 'reconciliationRunOrphanObjectListFind',
				instructions: 'A list of run objects excluded by this exclusion assignment.  Objects are purged after 7 days if the assignment is approved.',
				height: 250,
				heightResized: true,
				exclusionAssignmentId: 0,
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Source', width: 100, dataIndex: 'fileDefinition.source.name', filter: {searchFieldName: 'sourceName'}},
					{header: 'File Definition Name', width: 200, dataIndex: 'fileDefinition.name', filter: {searchFieldName: 'fileDefinitionName'}},
					{header: 'Account Number', width: 100, dataIndex: 'accountNumber', filter: false, sortable: false},
					{header: 'Security Identifier', width: 100, dataIndex: 'securityIdentifier', filter: false, sortable: false},
					{
						header: 'Type', width: 50, dataIndex: 'orphanType',
						filter: {
							type: 'combo',
							dataIndex: 'orphanType',
							displayField: 'name',
							valueField: 'value',
							mode: 'local',
							store: {
								xtype: 'arraystore',
								fields: ['value', 'name', 'description'],
								data: Clifton.reconciliation.run.ReconciliationRunOrphanObjectTypes
							}
						}
					},
					{header: 'Excluded Date', width: 50, dataIndex: 'reconciliationDate'}
				],
				editor: {
					addEnabled: false,
					deleteEnabled: false,
					detailPageClass: 'Clifton.reconciliation.run.ReconciliationRunOrphanObjectWindow'
				},
				getLoadParams: function(firstLoad) {
					if (!this.getWindow().getMainFormId()) {
						return false;
					}
					return {
						exclusionAssignmentId: this.getWindow().getMainFormId()
					};
				}
			}]
		}]
	}]
});
