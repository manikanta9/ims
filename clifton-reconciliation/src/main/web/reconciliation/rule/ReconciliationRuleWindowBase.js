Clifton.reconciliation.rule.ReconciliationRuleWindowBase = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation Rule',
	iconCls: 'bank',
	width: 1000,
	height: 400,

	//Defined on implementing windows; sets the ruleType prior to saving.
	ruleType: undefined,
	//Defined on implementing windows; defines the concrete class used to deserialize json send during save from the UI
	ruleClass: undefined,
	ruleBuilderDiv: undefined,
	ruleFilterFieldListUrl: undefined,
	items: [{
		xtype: 'formpanel',
		labelWidth: 75,
		instructions: 'Reconciliation Rules come in three types [Condition, Function, Comparator].  Conditions are used to determine if a specific configuration applies based on the rule.  Functions are used to modify the configuration value based on rule data and defined expressions.  Comparator rules are used to identify if two values are equal based on configurable comparators.',
		url: 'reconciliationRule.json',
		items: [
			{
				xtype: 'columnpanel',
				columns: [{
					rows: [{
						fieldLabel: 'Rule Name', name: 'name', xtype: 'textfield'
					}],
					config: {columnWidth: 0.67}
				}, {
					rows: [{
						fieldLabel: 'Rule Group', name: 'ruleGroup.name', hiddenName: 'ruleGroup.id', xtype: 'combo', url: 'reconciliationRuleGroupListFind.json', detailPageClass: 'Clifton.reconciliation.rule.ReconciliationRuleGroupWindow'
					}],
					config: {columnWidth: 0.33}
				}]
			},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
		],
		getLoadParams: function(config) {
			return config.params;
		},
		afterRenderPromise: function(formPanel) {
			formPanel.initializeQueryBuilder(formPanel, formPanel.form.formValues);
		},
		listeners: {
			afterload: function(formPanel) {
				formPanel.initializeQueryBuilder(formPanel, formPanel.ownerCt.defaultData, formPanel.form.formValues.jsonExpression);
			}
		},
		initializeQueryBuilder: function(formPanel, params, rules) {
			const window = formPanel.getWindow();
			if (TCG.isNotBlank(window.ruleFilterFieldListUrl)) {
				const loader = new TCG.data.JsonLoader({
					waitTarget: formPanel,
					waitMsg: 'Loading fields...',
					params: params,
					onLoad: function(fieldList, conf) {
						if (!TCG.isBlank(params) && !TCG.isBlank(params.defaultField)) {
							fieldList.push(params.defaultField);
						}
						window.renderQueryBuilder(formPanel, rules, fieldList);
					}
				});
				loader.load(window.ruleFilterFieldListUrl);
			}
			else {
				//If no url is defined, then it is assumed the getRuleFilters will be overridden
				//to provide a custom fieldList to be used.
				window.renderQueryBuilder(formPanel, rules);
			}
			window.afterQueryBuilderInitialization();
			formPanel.doLayout();
		}
	}],
	renderQueryBuilder: function(formPanel, rules, fieldList) {
		const window = formPanel.getWindow();
		//Create a div to inject the query builder into.
		const ruleBuilderDiv = document.createElement('div');
		ruleBuilderDiv.setAttribute('class', 'rule-builder');
		ruleBuilderDiv.setAttribute('style', 'width: 100%');
		//Wrap that div in the jquerybuilder div to control styling with bootstrap
		const jqueryBuilderDiv = document.createElement('div');
		jqueryBuilderDiv.setAttribute('class', 'jquerybuilder');
		jqueryBuilderDiv.appendChild(ruleBuilderDiv);
		//Append the divs to the formpanel
		formPanel.body.appendChild(jqueryBuilderDiv);
		//Create the queryBuilder instance
		window.ruleBuilderDiv = ruleBuilderDiv;

		if (fieldList.length === 0) {
			TCG.showError('No field mappings are configured for this source!');
			window.close();
			return false;
		}

		const queryBuilderConfig = {
			conditions: window.getRuleConditions(),
			operators: window.getRuleOperators(),
			plugins: window.getRulePlugins(),
			filters: window.getRuleFilters(fieldList)
		};
		$(window.ruleBuilderDiv).queryBuilder(window.updateQueryBuilderConfig(formPanel, queryBuilderConfig));

		//Now allow window to register custom event handling before setting rules (as some may apply during setting of rules)
		window.addCustomQueryBuilderEvents($('.rule-builder'));
		if (TCG.isNotBlank(rules)) {
			try {
				$(window.ruleBuilderDiv).queryBuilder('setRules', rules);
			}
			catch (err) {
				console.log(err.message);
				TCG.showError('Failed to set rule:<br /><br />' + err.message.replace('Undefined filter', 'Undefined field mapping').replace(/\s/g, '&nbsp;'));
				return false;
			}
		}
	},
	updateQueryBuilderConfig: function(formPanel, queryBuilderConfig) {
		//OVERRIDE ME AS NEEDED
		//Allows for overriding additional defaults such as:
		//  default_condition: 'AND'
		//  allow_groups: -1
		//  allow_empty: false
		return queryBuilderConfig;
	},
	addCustomQueryBuilderEvents: function(queryBuilder) {
		//OVERRIDE ME AS NEEDED, be sure to call super if overriden:
		//  TCG.callSuper(this, 'addCustomQueryBuilderEvents', arguments);
		//Example filters:
		// queryBuilder.on('jsonToRule.queryBuilder.filter', function(rule, json) {
		// 	console.log('jsonToRule');
		// 	console.log(rule);
		// 	console.log(json);
		// });
		// queryBuilder.on('ruleToJson.queryBuilder.filter', function(json, rule) {
		// 	console.log('ruleToJson');
		// 	console.log(json);
		// 	console.log(rule);
		// });
	},
	afterQueryBuilderInitialization: function() {
		//OVERRIDE ME AS NEEDED
	},
	getFilterType: function(dataType) {
		let filterType;
		switch (dataType) {
			case 'MONEY':
			case 'PRICE':
			case 'QUANTITY':
			case 'EXCHANGE_RATE':
				filterType = 'double';
				break;
			case 'INTEGER':
				filterType = 'integer';
				break;
			case 'DATE':
				filterType = 'date';
				break;
			case 'BIT':
				filterType = 'boolean';
				break;
			default:
				filterType = 'string';
				break;
		}
		return filterType;
	},
	updateRule: function(rule, values) {
		switch (rule.filter.type) {
			case 'double':
			case 'integer':
				rule.filter.input = 'number';
				break;
			default:
				rule.filter.input = 'text';
		}
		//rule.operator.inputType is a custom field added to allow for
		// overriding the default filter.input as necessary
		if (rule.operator.inputType) {
			rule.filter.input = rule.operator.inputType;
		}

		//Clear values, then if the inputType was 'select' then load the values
		rule.filter.value = [];
		rule.filter.values = [];
		if (rule.filter.input === 'select') {
			rule.filter.values = values;
			rule.filter.validation.allow_empty_value = true;
		}
	},
	getRuleFilters: function(definitionFieldMappingList) {
		//OVERRIDE ME AS NEEDED
		return $.fn.queryBuilder.defaults.filters;
	},
	getRuleOperatorByType: function(type) {
		let operator = undefined;
		this.getRuleOperators().forEach(function(ruleOperator) {
			if (ruleOperator.type === type) {
				operator = ruleOperator;
			}
		});
		return operator;
	},
	getRuleOperators: function() {
		//OVERRIDE ME AS NEEDED
		return $.fn.queryBuilder.defaults.operators;
	},
	getRuleConditions: function() {
		//OVERRIDE ME AS NEEDED
		return $.fn.queryBuilder.defaults.conditions;
	},
	getRulePlugins: function() {
		//OVERRIDE ME AS NEEDED
		return ['bt-tooltip-errors', 'sortable'];
	},
	saveWindow: function(closeOnSuccess, forceSubmit) {
		let id;
		const win = this;
		const name = win.getMainFormPanel().getForm().findField('name').getValue();
		const description = win.getMainFormPanel().getForm().findField('description').getValue();
		const ruleGroupId = win.getMainFormPanel().getForm().findField('ruleGroup.name').getValue();
		const result = $(win.ruleBuilderDiv).queryBuilder('getRules');
		if (!TCG.isBlank(result)) {
			if (!TCG.isBlank(win.getMainFormId())) {
				id = win.getMainFormId();
			}

			//Set the explicit type of the jsonExpression so jackson knows how to bind it
			result['@class'] = win.ruleExpressionClass;

			const jsonData = {
				reconciliationRule: {
					id: id,
					name: name,
					description: description,
					ruleType: win.ruleType,
					jsonExpression: result,
					...ruleGroupId ? {ruleGroup: {id: ruleGroupId}} : {}
				}
			};

			const loader = new TCG.data.JsonLoader({
				waitTarget: this,
				waitMsg: 'Saving...',
				params: {jsonParameterTypeClassNames: [win.ruleClass]},
				jsonData: jsonData,
				onLoad: function(record, conf) {
					//Only set the value if the openerCt was a combo box.
					if (TCG.isNotBlank(win.openerCt) && TCG.isEqualsStrict('combo', win.openerCt.xtype)) {
						win.openerCt.setValue({value: record.id, text: record.name});
					}
					win.doNotWarnOnCloseModified = true;
					win.closeWindow();
				}
			});
			//pre-pended with json/ because we are posting raw json to the server; not a form post.
			//TODO - change to be based on mime-type, not pre-pended url.
			loader.load('json/reconciliationRuleSave.json');
		}
	}
});
