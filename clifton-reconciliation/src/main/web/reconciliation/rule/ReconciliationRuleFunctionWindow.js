TCG.use('Clifton.reconciliation.rule.ReconciliationRuleWindowBase');
Clifton.reconciliation.rule.ReconciliationRuleFunctionWindow = Ext.extend(Clifton.reconciliation.rule.ReconciliationRuleWindowBase, {
	titlePrefix: 'Reconciliation Function',
	iconCls: 'tools',
	width: 1000,
	height: 450,

	ruleType: 'FUNCTION',
	ruleClass: 'com.clifton.reconciliation.rule.ReconciliationRuleFunction',
	ruleExpressionClass: 'com.clifton.core.json.function.JsonFunction',
	ruleFilterFieldListUrl: 'reconciliationDefinitionFieldMappingListFind.json',
	getRuleFilters: function(fieldMappingList) {
		const window = this;
		const filters = [];
		const uniqueValues = [''];
		Ext.each(fieldMappingList, function(fieldMapping) {
			const fieldMappingName = fieldMapping.name;
			if ($.inArray(fieldMappingName, uniqueValues) === -1) {
				uniqueValues.push(fieldMappingName);
			}
		});
		const processedFieldMappingNames = [];
		Ext.each(fieldMappingList, function(fieldMapping) {
			const fieldMappingName = fieldMapping.name;
			if ($.inArray(fieldMappingName, processedFieldMappingNames) === -1) {
				let dataType = fieldMapping.definitionField.dataType;
				if (TCG.isBlank(dataType)) {
					dataType = fieldMapping.definitionField.field.defaultDataType;
				}
				const filterType = window.getFilterType(dataType);
				filters.push({
					id: fieldMappingName,
					field: fieldMappingName,
					type: filterType,
					validation: {
						allow_empty_value: true
					},
					input: function(rule, name) {
						/**
						 * We used this to allow for a little custom manipulation of the input boxes.
						 * Primarily, the default for a 'number' field is a 'number' input box.
						 * but in the case of 'add_field', 'divide_field', and 'multiply_field' operators
						 * we need to show a 'select' input to allow for field selection...
						 * -
						 * The change event utilizes the 'createRuleInput' to recreate the input field
						 * using the newly selected operator and also attach all the necessary events.
						 * -
						 * The actual return from this function only returns the HTML for the input
						 * fields and is only executed on load when and existing rule is loaded.
						 * It uses the standard getRuleInput method of the queryBuilder after updating
						 * necessary rule properties based on the rule operator of the rule passed in
						 * (e.g. loaded from a prior save) to allow for the custom manipulation.
						 */
						const queryBuilder = this;
						const container = rule.$el.find('.rule-operator-container');
						container.on('change', '[name$=_operator]', function() {
							rule.operator = window.getRuleOperatorByType($(this).val());
							window.updateRule(rule, uniqueValues);
							queryBuilder.createRuleInput(rule);
						});
						const valueId = name.substring(name.lastIndexOf('_') + 1);
						window.updateRule(rule, uniqueValues);
						return queryBuilder.getRuleInput(rule, valueId);
					}
				});
				processedFieldMappingNames.push(fieldMappingName);
			}
		});
		return filters;
	},
	getRuleOperators: function() {
		return [
			{type: 'substring', nb_inputs: 2, multiple: false, apply_to: ['string']},
			{type: 'left', nb_inputs: 1, multiple: false, apply_to: ['string']},
			{type: 'right', nb_inputs: 1, multiple: false, apply_to: ['string']},
			{type: 'upper', nb_inputs: 0, multiple: false, apply_to: ['string']},
			{type: 'lower', nb_inputs: 0, multiple: false, apply_to: ['string']},
			{type: 'trim', nb_inputs: 0, multiple: false, apply_to: ['string']},
			{type: 'trim_all', nb_inputs: 0, multiple: false, apply_to: ['string']},
			{type: 'replace', nb_inputs: 2, multiple: false, apply_to: ['string']},
			{type: 'select_field', nb_inputs: 0, multiple: false, apply_to: ['string', 'number']},
			{type: 'input_value', nb_inputs: 1, multiple: false, apply_to: ['string', 'number']},
			{type: 'negate', nb_inputs: 0, multiple: false, apply_to: ['number']},
			{type: 'absolute_value', nb_inputs: 0, multiple: false, apply_to: ['number']},
			{type: 'add', nb_inputs: 1, multiple: false, apply_to: ['number']},
			{type: 'add_field', nb_inputs: 1, multiple: false, apply_to: ['number'], inputType: 'select'},
			{type: 'multiply', nb_inputs: 1, multiple: false, apply_to: ['number']},
			{type: 'multiply_field', nb_inputs: 1, multiple: false, apply_to: ['number'], inputType: 'select'},
			{type: 'divide', nb_inputs: 1, multiple: false, apply_to: ['number']},
			{type: 'divide_field', nb_inputs: 1, multiple: false, apply_to: ['number'], inputType: 'select'},
			{type: 'date_to_string', nb_inputs: 1, multiple: false, apply_to: ['datetime']},
			{type: 'free_marker', nb_inputs: 1, multiple: false, apply_to: ['string', 'number', 'datetime', 'boolean'], inputType: 'textarea'}
		];
	},
	getRuleConditions: function() {
		return ['Evaluate', 'Concatenate'];
	},
	updateQueryBuilderConfig: function(formPanel, queryBuilderConfig) {
		queryBuilderConfig.default_condition = 'Evaluate';
		return queryBuilderConfig;
	}
});
