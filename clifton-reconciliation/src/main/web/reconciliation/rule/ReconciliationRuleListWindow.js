Clifton.reconciliation.rule.ReconciliationRuleListWindow = Ext.extend(TCG.app.Window, {
	id: 'reconciliationRuleListWindow',
	iconCls: 'rule',
	title: 'Reconciliation Rules',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Rules',
				items: [{
					name: 'reconciliationRuleListFind',
					xtype: 'gridpanel',
					instructions: 'A reconciliation assignment allows the use of custom comparators for specified bean properties.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Rule Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Rule Type', width: 50, dataIndex: 'ruleType'},
						{header: 'Rule Group', width: 100, dataIndex: 'ruleGroup.name', filter: false, sortable: false}
					],
					editor: {
						detailPageClass: {
							getItemText: function(rowItem) {
								const t = rowItem.get('ruleType');
								if (t === 'COMPARATOR') {
									return 'Comparator Rule';
								}
								else if (t === 'FUNCTION') {
									return 'Function Rule';
								}
								return 'Condition Rule';
							},
							items: [
								{text: 'Condition Rule', iconCls: 'add', tooltip: 'Add a new condition rule', className: 'Clifton.reconciliation.rule.ReconciliationRuleConditionWindow'},
								{text: 'Function Rule', iconCls: 'add', tooltip: 'Add a new function rule', className: 'Clifton.reconciliation.rule.ReconciliationRuleFunctionWindow'}
							]
						}
					}
				}]
			},


			{
				title: 'Rule Groups',
				items: [{
					name: 'reconciliationRuleGroupListFind',
					xtype: 'gridpanel',
					instructions: 'A reconciliation assignment allows the use of custom comparators for specified bean properties.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 100, dataIndex: 'name', defaultSortColumn: true, filter: false},
						{header: 'Description', width: 200, dataIndex: 'description', defaultSortColumn: true, filter: false}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.rule.ReconciliationRuleGroupWindow'
					}
				}]
			}]
	}]
});
