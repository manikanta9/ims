Clifton.reconciliation.rule.ReconciliationRuleGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation Rule Group',
	iconCls: 'rule',
	width: 750,
	height: 450,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'A Reconciliation Rule group allows for grouping rules that are based on the same data so they can be reused across definitions.',
		url: 'reconciliationRuleGroup.json?requestedMaxDepth=4',
		items: [
			{fieldLabel: 'Group Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{
				name: 'reconciliationRuleListFind',
				xtype: 'gridpanel',
				height: 225,
				instructions: 'A reconciliation assignment allows the use of custom comparators for specified bean properties.',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Rule Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
					{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
					{header: 'Rule Type', width: 50, dataIndex: 'ruleType'}
				],
				getLoadParams: function() {
					const ruleGroupId = this.getWindow().getMainFormId();
					//If parent form doesn't have an id; don't load the grid
					if (TCG.isBlank(ruleGroupId)) {
						return false;
					}
					const loadParams = {
						ruleGroupId: ruleGroupId
					};
					return loadParams;
				}
			}
		]
	}]
});
