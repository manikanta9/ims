TCG.use('Clifton.reconciliation.rule.ReconciliationRuleWindowBase');
Clifton.reconciliation.rule.ReconciliationRuleConditionWindow = Ext.extend(Clifton.reconciliation.rule.ReconciliationRuleWindowBase, {
	titlePrefix: 'Reconciliation Condition',
	iconCls: 'question',
	width: 1000,
	height: 450,

	ruleType: 'CONDITION',
	ruleClass: 'com.clifton.reconciliation.rule.ReconciliationRuleCondition',
	ruleExpressionClass: 'com.clifton.core.json.condition.JsonCondition',
	ruleFilterFieldListUrl: 'reconciliationDefinitionFieldMappingListFind.json',
	getRuleFilters: function(fieldMappingList) {
		const window = this;
		const filters = [];
		const uniqueValues = [];
		Ext.each(fieldMappingList, function(fieldMapping) {
			const fieldMappingName = fieldMapping.name;
			if ($.inArray(fieldMappingName, uniqueValues) === -1) {
				let dataType = fieldMapping.definitionField.dataType;
				if (TCG.isBlank(dataType)) {
					dataType = fieldMapping.definitionField.field.defaultDataType;
				}
				const filterType = window.getFilterType(dataType);
				filters.push({
					id: fieldMappingName,
					field: fieldMappingName,
					type: filterType
				});
				uniqueValues.push(fieldMappingName);
			}
		});
		return filters;
	},
	getRuleOperators: function() {
		return $.fn.queryBuilder.constructor.DEFAULTS.operators.concat([
			{type: 'free_marker', nb_inputs: 1, multiple: false, apply_to: ['string', 'number', 'datetime', 'boolean'], inputType: 'textarea'},
			{type: 'equal_case_sensitive', nb_inputs: 1, multiple: false, apply_to: ['string'], inputType: 'textarea'},
			{type: 'not_equal_case_sensitive', nb_inputs: 1, multiple: false, apply_to: ['string'], inputType: 'textarea'}
		]);
	}
});
