Clifton.reconciliation.archive.ReconciliationRunArchiveManagementWindow = Ext.extend(TCG.app.Window, {
	iconCls: 'archive',
	width: 1500,
	height: 700,
	applicationName: 'reconciliation',
	title: 'Archive Management',
	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Archive',
				reloadOnTabChange: true,
				items: [{
					xtype: 'reconciliation-run-grid',
					listeners: {},
					rowSelectionModel: 'checkbox',

					addToolbarButtons: function(toolbar, grid) {
						console.log(toolbar);
						const button = {
							iconCls: 'run',
							text: 'Archive Selected',
							scope: this,
							handler: function() {
								const gridPanel = this;
								const grid = gridPanel.grid;
								const selectionModel = grid.getSelectionModel();
								const selections = selectionModel.getSelections().map(function(recRun) {
									return recRun.id;
								});
								const loader = new TCG.data.JsonLoader({
									waitTarget: grid.ownerCt,
									waitMsg: 'Archiving...',
									params: {
										ids: selections
									},
									onLoad: function(record, conf) {
										gridPanel.reload();
										TCG.createComponent('Clifton.core.StatusWindow', {
											defaultData: {status: record},
											render: function() {
												TCG.Window.superclass.render.apply(this, arguments);
											}
										});
									}
								});
								loader.load('reconciliationRunListArchive.json');
							}
						};
						toolbar.add(button);
					},
					applyFilters: function(firstLoad, params) {
						const gridPanel = this;
						if (firstLoad) {
							gridPanel.setFilterValue('runDate', {'on': Clifton.calendar.getBusinessDayFrom(-1)});
							gridPanel.setFilterValue('archived', false);
						}
						return params;
					}, editor: {
						detailPageClass: 'Clifton.reconciliation.run.ReconciliationRunDetailWindowSelector',
						addEnabled: false,
						deleteEnabled: false,
						getDefaultData: function(gridPanel, row) {
							return {
								reconciliationRun: row.json
							};
						}
					}
				}]
			}, {
				title: 'Restore',
				reloadOnTabChange: true,
				items: [{
					xtype: 'reconciliation-run-grid',
					instructions: 'A reconciliation run tracks the overall status of all objects reconciled during a run.',
					listeners: {},
					addToolbarButtons: function(toolbar, grid) {
						const button = {
							iconCls: 'run',
							text: 'Restore Selected',
							scope: this,
							handler: function() {
								const gridPanel = this;
								const grid = gridPanel.grid;
								const selectionModel = grid.getSelectionModel();
								const selections = selectionModel.getSelections().map(function(recRun) {
									return recRun.id;
								});
								const loader = new TCG.data.JsonLoader({
									waitTarget: grid.ownerCt,
									waitMsg: 'Restoring...',
									params: {
										ids: selections
									},
									onLoad: function(record, conf) {
										gridPanel.reload();
										TCG.createComponent('Clifton.core.StatusWindow', {
											defaultData: {status: record},
											render: function() {
												TCG.Window.superclass.render.apply(this, arguments);
											}
										});
									}
								});
								loader.load('reconciliationRunListRestore.json');
							}
						};
						toolbar.add(button);
					},
					applyFilters: function(firstLoad, params) {
						const gridPanel = this;
						if (firstLoad) {
							gridPanel.setFilterValue('archived', true);
						}
						return params;
					},
					rowSelectionModel: 'checkbox',
					editor: {
						addEnabled: false,
						deleteEnabled: false
					}
				}]
			}
		]
	}]
});
