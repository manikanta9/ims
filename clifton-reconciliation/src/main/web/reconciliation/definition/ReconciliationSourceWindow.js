Clifton.reconciliation.definition.ReconciliationSourceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation Source',
	iconCls: 'house',
	width: 750,
	height: 450,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'A Reconciliation Source defines where data is imported from.',
		url: 'reconciliationSource.json?requestedMaxDepth=4',
		items: [
			{fieldLabel: 'Source Name', name: 'name'},
			{fieldLabel: 'Short Name', name: 'shortName'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
		]
	}]
});
