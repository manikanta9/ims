Clifton.reconciliation.definition.ReconciliationFieldWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation Field',
	iconCls: 'list',
	width: 750,
	height: 450,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'A Reconciliation Source defines where data is imported from.',
		url: 'reconciliationField.json?requestedMaxDepth=4',
		items: [
			{fieldLabel: 'Name', name: 'name'},
			{fieldLabel: 'Label', name: 'label'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{
				fieldLabel: 'Default Data Type', name: 'defaultDataType', hiddenName: 'defaultDataType',
				mode: 'local', xtype: 'combo', allowBlank: false, qtip: 'Data type of this definition field.',
				store: {
					xtype: 'arraystore',
					data: Clifton.reconciliation.DataTypes
				}
			},
			{fieldLabel: 'Default Display Width', name: 'defaultDisplayWidth', xtype: 'integerfield'}
		]
	}]
});
