Clifton.reconciliation.definition.rule.ReconciliationEvaluatorGapQuantityPreviewWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation Evaluator Gap Quantity Preview',
	iconCls: 'question',
	width: 750,
	height: 400,
	okButtonText: 'Save and Apply Rule',
	hideApplyButton: true,
	saveUrl: 'reconciliationEvaluatorSave.json',
	items: [{
		labelWidth: 140,
		xtype: 'formwithdynamicfields',
		instructions: 'A Reconciliation evaluator gap quantity rule is used to specify the difference between managed and held quantities used used to determine if/how data should be reconciled.',
		getSaveURL: function() {
			//getting this from the window so we can override for copying purposes.
			return this.getWindow().saveUrl;
		},
		dynamicTriggerFieldName: 'evaluatorBean.type.name',
		dynamicTriggerValueFieldName: 'evaluatorBean.type.id',
		dynamicFieldTypePropertyName: 'type',
		dynamicFieldListPropertyName: 'evaluatorBean.propertyList',
		dynamicFieldsUrl: 'systemBeanPropertyTypeListByType.json',
		dynamicFieldsUrlParameterName: 'typeId',
		dynamicFieldsUrlIdFieldName: 'evaluatorBean.id',
		dynamicFieldsUrlListParameterName: 'propertyList',
		items: [
			{fieldLabel: 'id', name: 'id', xtype: 'hidden'},
			{fieldLabel: 'Evaluator Name', name: 'name', xtype: 'textfield'},
			{fieldLabel: 'Definition', name: 'definitionField.reconciliationDefinition.name', detailIdField: 'definitionField.reconciliationDefinition.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionWindow'},
			{fieldLabel: 'Definition Field', name: 'definitionField.field.name', detailIdField: 'definitionField.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionFieldWindow'},
			{fieldLabel: 'Holding Account', name: 'reconciliationInvestmentAccount.accountNumber', detailIdField: 'reconciliationInvestmentAccount.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentAccountWindow'},
			{fieldLabel: 'Investment Security', name: 'reconciliationInvestmentSecurity.securitySymbol', detailIdField: 'reconciliationInvestmentSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentSecurityWindow'},
			{
				xtype: 'columnpanel',
				columns: [{
					rows: [{
						fieldLabel: 'Schedule', width: 350, name: 'calendarSchedule.name', hiddenName: 'calendarSchedule.id', xtype: 'combo', qtip: 'Defines the schedule for this evaluator, e.g., only run the last business day of the month.',
						detailPageClass: 'Clifton.calendar.schedule.ScheduleWindow', url: 'calendarScheduleListFind.json?systemDefined=false'
					}],
					config: {columnWidth: 0.67}
				}, {
					rows: [{
						boxLabel: 'IsExclusive', name: 'exclusive', xtype: 'checkbox', qtip: 'Runs the evaluator on all days EXCEPT those defined by the schedule)'
					}],
					config: {labelWidth: 0, columnWidth: 0.33}
				}]
			},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
			{fieldLabel: 'Order', name: 'evaluationOrder', xtype: 'spinnerfield', minValue: 0, qtip: 'Specifies the evaluation order of this evaluator.', value: 10},
			{
				xtype: 'label', html: '<hr />'
			},
			{fieldLabel: 'Evaluator Bean Type', name: 'evaluatorBean.type.name', detailIdField: 'evaluatorBean.type.id', xtype: 'linkfield', detailPageClass: 'Clifton.system.bean.TypeWindow'}
		],
		listeners: {
			afterload: function(formPanel) {
				const origGridPanel = this.getWindow().openerCt;
				origGridPanel.reload();
				//Also reload the run window to update statuses there.
				origGridPanel.getWindow().openerCt.reload();
			}
		}
	}]
});
