Clifton.reconciliation.definition.rule.ReconciliationEvaluatorWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation Evaluator',
	iconCls: 'question',
	width: 750,
	height: 450,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'A Reconciliation Evaluator is be used for a field to specify if/how data from both sources should be reconciled. It can be used to specify reconciliation thresholds, gap quantities, etc. Evaluators are applied using the specified Order and can use filters (Holding Account, Investment Security, Custom Condition).',
		url: 'reconciliationEvaluator.json?requestedMaxDepth=4',
		items: [
			{
				fieldLabel: 'Reconciliation Definition', xtype: 'combo', loadAll: true, detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionWindow',
				name: 'definitionField.reconciliationDefinition.name',
				hiddenName: 'definitionField.reconciliationDefinition.id', displayField: 'name',
				url: 'reconciliationDefinitionListFind.json'
			},
			{
				fieldLabel: 'Definition Field', xtype: 'combo', loadAll: true, detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionFieldWindow',
				name: 'definitionField.field.name', hiddenName: 'definitionField.id', displayField: 'field.name',
				url: 'reconciliationDefinitionFieldListByDefinitionId.json',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const definitionId = combo.getParentForm().getForm().findField('definitionField.reconciliationDefinition.id').value;
						if (TCG.isBlank(definitionId)) {
							TCG.showError('You must select a definition field first!');
							return false;
						}
						combo.clearAndReset();
						combo.store.baseParams = {
							definitionId: definitionId,
							activeOnDate: TCG.parseDate(new Date()).format('m/d/Y')
						};
					}
				}
			},
			{fieldLabel: 'Evaluator Name', name: 'name', xtype: 'textfield'},
			{
				fieldLabel: 'Evaluator Bean', name: 'evaluatorBean.name', hiddenName: 'evaluatorBean.id', xtype: 'combo', loadAll: true, qtip: 'The evaluator bean that will be used when reconciling the defined field if the specified condition is met.',
				url: 'systemBeanListFind.json?groupName=Reconciliation Evaluator', detailPageClass: 'Clifton.system.bean.BeanWindow',
				getDefaultData: function() {
					return {type: {group: {name: 'Reconciliation Evaluator'}}};
				}
			},
			{xtype: 'sectionheaderfield', header: 'Scope', fieldLabel: ''},
			{
				fieldLabel: 'Holding Account', xtype: 'combo',
				name: 'reconciliationInvestmentAccount.accountNumber', hiddenName: 'reconciliationInvestmentAccount.id', displayField: 'label',
				url: 'reconciliationInvestmentAccountListFind.json', detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentAccountWindow', disableAddNewItem: true
			},
			{
				fieldLabel: 'Investment Security', xtype: 'combo',
				name: 'reconciliationInvestmentSecurity.securitySymbol', hiddenName: 'reconciliationInvestmentSecurity.id', displayField: 'securitySymbol',
				url: 'reconciliationInvestmentSecurityListFind.json', detailPageClass: 'Clifton.reconciliation.investment.ReconciliationInvestmentSecurityWindow', disableAddNewItem: true
			},
			{
				fieldLabel: 'Condition Rule', name: 'conditionRule.name', hiddenName: 'conditionRule.id', xtype: 'combo', loadAll: true, qtip: 'Condition that must be met for this field mapping to apply.',
				detailPageClass: 'Clifton.reconciliation.rule.ReconciliationRuleConditionWindow', url: 'reconciliationRuleListFind.json',
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const formPanel = TCG.getParentFormPanel(combo);
					const form = formPanel.getForm();
					const definitionFieldId = form.findField('definitionField.id').getValue();
					if (TCG.isBlank(definitionFieldId)) {
						TCG.showError('You must select a definition field first!');
						return false;
					}
					const ruleGroupIds = [formPanel.getFormValue('reconciliationDefinition.primaryRuleGroup.id'), formPanel.getFormValue('reconciliationDefinition.secondaryRuleGroup.id')];
					combo.clearAndReset();
					combo.store.baseParams = {
						ruleType: 'CONDITION',
						ruleGroupIds: ruleGroupIds
					};
				},
				getDefaultData: function(formPanel) {
					const definitionId = TCG.getValue('definitionField.reconciliationDefinition.id', formPanel.formValues);
					if (TCG.isBlank(definitionId)) {
						TCG.showError('You must select a definition field first!');
						return false;
					}
					return {
						definitionId: definitionId
					};
				}
			},
			{xtype: 'label', html: '<hr/>'},
			{
				xtype: 'columnpanel',
				columns: [{
					rows: [{
						fieldLabel: 'Schedule', width: 350, name: 'calendarSchedule.name', hiddenName: 'calendarSchedule.id', xtype: 'combo', qtip: 'Defines the schedule for this evaluator, e.g., only run the last business day of the month.',
						detailPageClass: 'Clifton.calendar.schedule.ScheduleWindow', url: 'calendarScheduleListFind.json?systemDefined=false'
					}],
					config: {columnWidth: 0.67}
				}, {
					rows: [{
						boxLabel: 'Exclusive', name: 'exclusive', xtype: 'checkbox', qtip: 'Runs the evaluator on all days EXCEPT those defined by the schedule.'
					}],
					config: {labelWidth: 0, columnWidth: 0.33}
				}]
			},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', qtip: 'Specifies the start date of this field definition.', value: new Date().format('m/d/Y')},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', qtip: 'Specifies the end date of this field definition.'},
			{fieldLabel: 'Order', name: 'evaluationOrder', xtype: 'spinnerfield', minValue: 0, qtip: 'Specifies the evaluation order of this evaluator.', value: 10}
		]
	}]
});
