Clifton.reconciliation.definition.rule.ReconciliationFilterWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation Filter',
	iconCls: 'filter',
	width: 750,
	height: 300,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'Reconciliation Filters are used to limit rows that should be processed from incoming data.',
		url: 'reconciliationFilter.json',
		items: [
			{fieldLabel: 'Reconciliation Definition', name: 'reconciliationDefinition.name', detailIdField: 'reconciliationDefinition.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionWindow'},
			{fieldLabel: 'Filter Name', name: 'name', xtype: 'textfield'},
			{
				fieldLabel: 'Reconciliation Source', name: 'reconciliationSource.name', hiddenName: 'reconciliationSource.id', xtype: 'combo', url: 'reconciliationSourceListFind.json', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationSourceWindow', qtip: 'The source this filter applies to.',
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const formPanel = TCG.getParentFormPanel(combo);
					const form = formPanel.getForm();
					const ids = [];
					ids.push(formPanel.getFormValue('reconciliationDefinition.primarySource.id'));
					ids.push(formPanel.getFormValue('reconciliationDefinition.secondarySource.id'));
					combo.store.baseParams = {
						ids: ids
					};
					form.findField('conditionRule.name').clearAndReset();
				}
			}, {
				fieldLabel: 'Condition Rule', name: 'conditionRule.name', hiddenName: 'conditionRule.id', xtype: 'combo', loadAll: true, qtip: 'Condition that must be met for this filter to apply.',
				detailPageClass: 'Clifton.reconciliation.rule.ReconciliationRuleConditionWindow', url: 'reconciliationRuleListFind.json',
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const formPanel = TCG.getParentFormPanel(combo);
					const form = formPanel.getForm();
					const sourceId = form.findField('reconciliationSource.id').getValue();
					if (TCG.isBlank(sourceId)) {
						TCG.showError('You must select a source first!');
						return false;
					}
					const primarySourceId = formPanel.getFormValue('reconciliationDefinition.primarySource.id');
					const ruleGroupId = (TCG.isEquals(sourceId, primarySourceId) ? formPanel.getFormValue('reconciliationDefinition.primaryRuleGroup.id') : formPanel.getFormValue('reconciliationDefinition.secondaryRuleGroup.id'));
					combo.clearAndReset();
					combo.store.baseParams = {
						ruleType: 'CONDITION',
						ruleGroupId: ruleGroupId
					};
				},
				getDefaultData: function(formPanel) {
					const definitionId = formPanel.findField('reconciliationDefinition.id').getValue();
					const sourceId = formPanel.findField('reconciliationSource.id').getValue();
					if (TCG.isBlank(definitionId) || TCG.isBlank(sourceId)) {
						TCG.showError('You must select a definition and source first!');
						return false;
					}
					return {
						definitionId: definitionId,
						sourceId: sourceId
					};
				}
			},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', qtip: 'Specifies the start date of this field definition.'},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', qtip: 'Specifies the end date of this field definition.'},
			{
				fieldLabel: 'Configuration Flags', xtype: 'checkboxgroup', columns: 2, qtip: 'The configuration flags determine how this filter is used; each configuration option has a more detailed description of its specific use.',
				items: [
					{boxLabel: 'Include Data (uncheck to Exclude)', name: 'include', xtype: 'checkbox', qtip: 'Defines if this filter should be used to include or exclude an item that meets the specified condition.'},
					{boxLabel: 'Do Not Save Filtered Data as Orphaned Data', name: 'doNotSaveFilteredData', xtype: 'checkbox', qtip: 'Defines if the data filtered by this row should be saved as orphaned data if not used by any other definition during processing.  If a filter is used to exclude positions that are not yet applicable (e.g. no maturity date), this can be used so that items that will be filtered daily will not be saved an need to be reviewed daily due to being orphaned.  This should be used only when absolutely sure a filter will not incorrectly exclude an item.'}
				]
			}
		]
	}]
});
