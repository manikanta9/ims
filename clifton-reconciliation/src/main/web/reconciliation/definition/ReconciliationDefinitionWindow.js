Clifton.reconciliation.definition.ReconciliationDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation Definition',
	iconCls: 'reconcile',
	width: 1200,
	height: 760,

	gridTabWithCount: 'Notes', // show notes count in  the tab

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Reconciliation Definition',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'ReconciliationDefinition',
					reloadFormPanelAfterTransition: true,
					hideSystemDefinedTransitions: true
				},
				reloadOnTabChange: true,
				items: [{
					xtype: 'formpanel',
					labelWidth: 180,
					instructions: 'A Reconciliation Definition determines how an object\'s properties should be compared during reconciliation.',
					url: 'reconciliationDefinition.json?requestedMaxDepth=4',
					items: [
						{
							xtype: 'columnpanel',
							columns: [{
								rows: [{
									fieldLabel: 'Reconciliation Type', xtype: 'combo', name: 'type.name', hiddenName: 'type.id', width: 100, url: 'reconciliationTypeListFind.json', detailPageClass: 'Clifton.reconciliation.definition.type.ReconciliationTypeWindow', qtip: 'Specifies the type of definition.  Definition types determine how run details are displayed and processed, e.g. Cash Reconciliations default to use the side/side view, and Transaction Reconciliations are marked as cumulative, which means all objects are appended to a single run every day.'
								}]
							}, {
								rows: [{
									fieldLabel: 'Contact Email', xtype: 'textfield', name: 'contactEmail', width: 100, qtip: 'Semicolon separated list of contact email addresses for reference when research must be done on a reconciliation break.'
								}]
							}]
						},
						{fieldLabel: 'Definition Name', name: 'name', xtype: 'textfield', emptyText: '<Enter Definition Name>', qtip: 'A unique name identifying the reconciliation.'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', emptyText: '<Enter Description>', height: 35, grow: true, qtip: 'A brief description describing the purpose of this reconciliation.'},
						{
							xtype: 'columnpanel',
							columns: [{
								rows: [{
									fieldLabel: 'Definition Group', xtype: 'combo', name: 'definitionGroup.name', hiddenName: 'definitionGroup.id', url: 'reconciliationDefinitionGroupListFind.json?', detailPageClass: 'Clifton.reconciliation.definition.group.ReconciliationDefinitionGroupWindow', qtip: 'Definition groups are used to link multiple definitions together.  Linked definitions may send events to external systems, i.e. account unlock events are sent when a holding account has been reconciled across all linked definitions.'
								}]
							}, {
								rows: [{
									fieldLabel: 'Definition Calendar', name: 'calendar.name', hiddenName: 'calendar.id',
									xtype: 'combo', allowBlank: true, url: 'calendarListFind.json', qtip: 'Allows for specifying a specific calendar for a definition.  Some reconciliations may be governed by a different calendar to allow for holidays not used in the default system calendar.'
								}]
							}, {
								rows: [{
									fieldLabel: 'Primary Source', width: 100, name: 'primarySource.name', hiddenName: 'primarySource.id', xtype: 'combo', url: 'reconciliationSourceListFind.json', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationSourceWindow', qtip: 'Specifies the primary source from which data is loaded.  Aliases may not be applied to the primary source as it is considered the source of truth.'
								}]
							}, {
								rows: [{
									fieldLabel: 'Secondary Source', width: 100, name: 'secondarySource.name', hiddenName: 'secondarySource.id', xtype: 'combo', url: 'reconciliationSourceListFind.json', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationSourceWindow', qtip: 'Specifies the secondary source from which data is loaded.  Typically a broker or custodian.'
								}]
							}, {
								rows: [{
									fieldLabel: 'Primary Rule Group', width: 100, name: 'primaryRuleGroup.name', hiddenName: 'primaryRuleGroup.id', xtype: 'combo', url: 'reconciliationRuleGroupListFind.json', detailPageClass: 'Clifton.reconciliation.rule.ReconciliationRuleGroupWindow', qtip: 'Rule groups are used to limit the selectable rules during mapping configuration.  Applies to mappings created for the primary source.'
								}]
							}, {
								rows: [{
									fieldLabel: 'Secondary Rule Group', width: 100, name: 'secondaryRuleGroup.name', hiddenName: 'secondaryRuleGroup.id', xtype: 'combo', url: 'reconciliationRuleGroupListFind.json', detailPageClass: 'Clifton.reconciliation.rule.ReconciliationRuleGroupWindow', qtip: 'Rule groups are used to limit the selectable rules during mapping configuration.  Applies to mappings created for the secondary source.'
								}]
							}]
						},
						{fieldLabel: 'Parent Definition', name: 'parentDefinition.name', hiddenName: 'parentDefinition.id', xtype: 'combo', url: 'reconciliationDefinitionListFind.json?type=Cash Reconciliation', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionWindow', hidden: true},
						{
							title: 'Preprocessors',
							xtype: 'gridpanel',
							height: 200,
							heightResized: true,
							collapsible: true,
							disabled: true,
							name: 'reconciliationDefinitionPreprocessorListFind',
							instructions: 'Preprocessors manipulate the imported data prior to applying filters and field mappings.',
							columns: [
								{header: 'ID', width: 25, dataIndex: 'id', hidden: true},
								{header: 'Reconciliation Definition', width: 200, dataIndex: 'reconciliationDefinition.name'},
								{header: 'Preprocessor Name', width: 300, dataIndex: 'preprocessor.name'},
								{header: 'Evaluation Order', width: 70, dataIndex: 'evaluationOrder', type: 'int'}
							],
							getLoadParams: function(firstLoad) {
								const definitionId = this.getWindow().getMainFormId();
								if (TCG.isBlank(definitionId)) {
									return false;
								}

								const loadParams = {
									definitionId: definitionId
								};
								return loadParams;
							},
							editor: {
								detailPageClass: 'Clifton.reconciliation.definition.preprocessor.ReconciliationDefinitionPreprocessorWindow',
								getDefaultData: function(gridPanel) {
									return {
										reconciliationDefinition: gridPanel.getWindow().getMainForm().formValues
									};
								}
							}
						},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							instructions: 'Tags are used to allow for grouping definitions in various ways for easy filtering on grids.',
							tableName: 'ReconciliationDefinition',
							hierarchyCategoryName: 'Reconciliation Definition Tags'
						},
						{
							xtype: 'fieldset',
							title: 'Run Conditions',
							instructions: 'The following conditions can be set or overridden to allow for custom handling that determines how and when a run created by this definition can be approved and/or modified.',
							collapsed: true,
							items: [{
								xtype: 'columnpanel',
								columns: [{
									rows: [{
										fieldLabel: 'Default Run Approval Condition', name: 'coalesceRunApprovalSystemCondition.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', qtip: 'Approval Condition for runs created by this definition'
									}]
								}, {
									rows: [{
										fieldLabel: 'Override Run Approval Condition', name: 'runApprovalSystemCondition.name', hiddenName: 'runApprovalSystemCondition.id',
										xtype: 'combo', allowBlank: true, qtip: 'Approval Condition for runs created by this definition', url: 'systemConditionListFind.json?systemTableName=ReconciliationRun', detailPageClass: 'Clifton.system.condition.ConditionWindow'
									}]
								}, {
									rows: [{
										fieldLabel: 'Default Run Modify Condition', name: 'coalesceRunModifySystemCondition.name', xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', qtip: 'Modify Condition for runs created by this definition'
									}]
								}, {
									rows: [{
										fieldLabel: 'Override Run Modify Condition', name: 'runModifySystemCondition.name', hiddenName: 'runModifySystemCondition.id',
										xtype: 'combo', allowBlank: true, qtip: 'Modify Condition for runs created by this definition', url: 'systemConditionListFind.json?systemTableName=ReconciliationRun', detailPageClass: 'Clifton.system.condition.ConditionWindow'
									}]
								}]
							}]
						}
					],
					listeners: {
						afterload: function(formPanel, closeOnSuccess) {
							const parentDefinitionField = this.items.find(function(e) {
								return e.name === 'parentDefinition.name';
							});

							const typeName = formPanel.form.formValues.type.name;
							if (typeName === 'Transaction Reconciliation') {
								parentDefinitionField.show();
							}
							else {
								parentDefinitionField.hide();
							}
							// Skip UI modifications if window is about to be closed
							if (!closeOnSuccess) {
								formPanel.enableFields();
							}
						}
					},
					enableFields: function() {
						const formPanel = this;
						if (!!formPanel.getIdFieldValue()) {
							const disabledItems = formPanel.findByType('gridpanel');
							disabledItems.forEach(p => p.enable());
						}
					}
				}]
			},


			{
				title: 'Definition Mappings',
				items: [
					{
						xtype: 'formpanel',
						items: [
							{
								title: 'Fields',
								xtype: 'gridpanel',
								name: 'reconciliationDefinitionFieldListFind',
								instructions: 'A reconciliation definition field holds mappings to source fields and defines if fields are utilized as natural keys, for reconciliation, or both.',
								height: 325,
								heightResized: true,
								collapsible: true,
								getTopToolbarFilters: function(toolbar) {
									return [
										{fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}
									];
								},
								columns: [
									{header: 'ID', width: 25, dataIndex: 'id', hidden: true},
									{header: 'Field Name', width: 250, dataIndex: 'field.name'},
									{
										header: 'Default Data Type', width: 250, dataIndex: 'field.defaultDataType', hidden: true,
										renderer: function(value, metaData, r) {
											if (value === 'DESCRIPTION') {
												return 'STRING';
											}
											else if (value === 'BIT') {
												return 'BOOLEAN';
											}
											return value;
										}
									},
									{
										header: 'Data Type', width: 150, dataIndex: 'dataType', tooltip: 'Data type of this definition field.',
										renderer: function(value, metaData, r) {
											const dataType = value ? value : r.json.field.defaultDataType;
											if (dataType === 'DESCRIPTION') {
												return 'STRING';
											}
											else if (dataType === 'BIT') {
												return 'BOOLEAN';
											}
											return dataType;
										}
									},
									{header: 'Natural Key', width: 75, dataIndex: 'naturalKey', type: 'boolean', tooltip: 'Specifies if this key definition is used as a natural key used in creation of the unique identifier for data.'},
									{header: 'Reconcilable', width: 75, dataIndex: 'reconcilable', type: 'boolean', tooltip: 'Specifies if this key definition specifies fields that should be reconciled when processing data.'},
									{header: 'Adjustable', width: 75, dataIndex: 'adjustable', type: 'boolean', tooltip: 'Specifies if this key definition can be used to create adjustments.'},
									{header: 'Aggregate', width: 75, dataIndex: 'aggregate', type: 'boolean', tooltip: 'Specifies if this key definition should be aggregated/summed during merging of two or more rows of data for the same natural key during import.'},
									{header: 'Hidden', width: 75, dataIndex: 'hidden', type: 'boolean', tooltip: 'Specifies if this key definition should be displayed in detail windows.'},
									{header: 'Start Date', width: 75, dataIndex: 'startDate'},
									{header: 'End Date', width: 75, dataIndex: 'endDate'},
									{header: 'Order', width: 75, dataIndex: 'displayOrder', type: 'int', tooltip: 'Display Order'}
								],
								getLoadParams: function(firstLoad) {
									const definitionId = this.getWindow().getMainFormId();
									//If parent form doesn't have an id; don't load the grid
									if (TCG.isBlank(definitionId)) {
										return false;
									}

									const loadParams = {
										definitionId: definitionId
									};
									const toolbar = this.getTopToolbar();
									const activeOnDate = TCG.getChildByName(toolbar, 'activeOnDate');
									if (firstLoad) {
										// default to today
										if (TCG.isBlank(activeOnDate.getValue())) {
											activeOnDate.setValue((new Date()).format('m/d/Y'));
											activeOnDate.originalValue = activeOnDate.getValue(); // prevent on change event
										}
									}
									if (TCG.isNotBlank(activeOnDate.getValue())) {
										loadParams.activeOnDate = (activeOnDate.getValue()).format('m/d/Y');
									}
									return loadParams;
								},
								editor: {
									copyURL: 'reconciliationDefinitionFieldCopy.json',
									copyIdParameter: 'definitionFieldId',
									copyHandler: function(gridPanel) {
										const sm = gridPanel.grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select a row to be copied.', 'No Row(s) Selected');
										}
										else if (sm.getCount() !== 1) {
											TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
										}
										else {
											Ext.Msg.confirm('Copy Definition Field', 'Are you sure you wish to copy this field?<br /><br />The existing field will be set to inactive as of the last business day, and the new field will take its place.', function(a) {
												if (a === 'yes') {
													const params = {};
													params[this.copyIdParameter] = sm.getSelected().id;
													const loader = new TCG.data.JsonLoader({
														waitTarget: gridPanel,
														waitMsg: 'Copying...',
														params: params,
														conf: params,
														onLoad: function(record, conf) {
															gridPanel.reload();
														}
													});
													loader.load(this.copyURL);
												}
											}, this);
										}
									},
									detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionFieldWindow',
									getDefaultData: function(gridPanel) {
										return {
											reconciliationDefinition: gridPanel.getWindow().getMainForm().formValues,
											startDate: new Date().format('Y-m-d 00:00:00')
										};
									}
								}
							},

							{
								title: 'Filters',
								xtype: 'gridpanel',
								name: 'reconciliationFilterListFind',
								instructions: 'Reconciliation Filters are used to limit rows that should be processed from incoming data.',
								height: 300,
								heightResized: true,
								collapsible: true,
								getTopToolbarFilters: function(toolbar) {
									return [
										{fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}
									];
								},
								columns: [
									{header: 'ID', width: 25, dataIndex: 'id', hidden: true},
									{header: 'Definition', width: 200, dataIndex: 'reconciliationDefinition.name', hidden: true},
									{header: 'Filter Name', width: 300, dataIndex: 'name'},
									{header: 'Source', width: 150, dataIndex: 'reconciliationSource.name'},
									{header: 'Condition', width: 200, dataIndex: 'conditionRule.name'},
									{header: 'Include', width: 75, dataIndex: 'include', type: 'boolean'},
									{header: 'Do Not Save Filtered Data', width: 200, dataIndex: 'doNotSaveFilteredData', type: 'boolean'},
									{header: 'Start Date', width: 75, dataIndex: 'startDate'},
									{header: 'End Date', width: 75, dataIndex: 'endDate'}
								],
								getLoadParams: function(firstLoad) {
									const definitionId = this.getWindow().getMainFormId();
									//If parent form doesn't have an id; don't load the grid
									if (TCG.isBlank(definitionId)) {
										return false;
									}

									const loadParams = {
										definitionId: definitionId
									};
									const toolbar = this.getTopToolbar();
									const activeOnDate = TCG.getChildByName(toolbar, 'activeOnDate');
									if (firstLoad) {
										// default to today
										if (TCG.isBlank(activeOnDate.getValue())) {
											activeOnDate.setValue((new Date()).format('m/d/Y'));
											activeOnDate.originalValue = activeOnDate.getValue(); // prevent on change event
										}
									}
									if (TCG.isNotBlank(activeOnDate.getValue())) {
										loadParams.activeOnDate = (activeOnDate.getValue()).format('m/d/Y');
									}
									return loadParams;
								},
								editor: {
									detailPageClass: 'Clifton.reconciliation.definition.rule.ReconciliationFilterWindow',
									getDefaultData: function(gridPanel) {
										return {
											reconciliationDefinition: gridPanel.getWindow().getMainForm().formValues,
											startDate: new Date().format('Y-m-d 00:00:00')
										};
									}
								}
							}
						]
					}

				]
			},

			{
				title: 'File Definitions',
				items: [{
					name: 'reconciliationFileDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'A Reconciliation File Definition specifies what reconciliation definition(s) should be used to process the file from a given source.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Reconciliation Source', width: 70, dataIndex: 'source.name'},
						{header: 'Definition Name', width: 120, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'File Name', width: 100, dataIndex: 'fileName', filter: {searchFieldName: 'definitionFileName'}},
						{header: 'Save Orphans', width: 50, dataIndex: 'saveOrphanedData', type: 'boolean'}
					],
					getLoadParams: function(firstLoad) {
						const definitionId = this.getWindow().getMainFormId();
						//If parent form doesn't have an id; don't load the grid
						if (TCG.isBlank(definitionId)) {
							return false;
						}
						return {definitionId: definitionId};
					},
					editor: {
						detailPageClass: 'Clifton.reconciliation.file.ReconciliationFileDefinitionWindow'
					}
				}]
			},

			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'ReconciliationDefinition',
					showAttachmentInfo: true,
					showInternalInfo: false,
					showPrivateInfo: false,
					showGlobalNoteMenu: true,
					showGlobalColumn: true,
					defaultActiveFilter: false,
					showDisplayFilter: false
				}]
			},


			{
				title: 'Audit Trail',
				items: [{
					xtype: 'system-audit-grid',
					name: 'reconciliationDefinitionSystemAuditEventListFind',
					initComponent: function() {
						Clifton.system.audit.AuditGridPanel.superclass.initComponent.call(this, arguments);
						const cm = this.getColumnModel();
						cm.setHidden(cm.findColumnIndex('systemTableId'), false);
					},
					getLoadParams: function(firstLoad) {
						return {'id': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
})
;
