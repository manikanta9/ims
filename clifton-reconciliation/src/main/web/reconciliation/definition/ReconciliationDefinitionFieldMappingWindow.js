Clifton.reconciliation.definition.ReconciliationDefinitionFieldMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Definition Field Mapping',
	iconCls: 'list',
	width: 750,
	height: 400,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'A Reconciliation Definition Field determines how an objects unique key should be computed for reconciliation.',
		url: 'reconciliationDefinitionFieldMapping.json?requestedMaxDepth=5',
		getFormLabel: function() {
			return this.getFormValue('name');
		},
		items: [
			{fieldLabel: 'Definition', name: 'definitionField.reconciliationDefinition.name', detailIdField: 'definitionField.reconciliationDefinition.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionWindow', qtip: 'The definition this field mapping belongs to.'},
			{fieldLabel: 'Field Name', name: 'definitionField.field.name', detailIdField: 'definitionField.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionFieldWindow', qtip: 'The definition field this field mapping belongs to.'},
			{
				fieldLabel: 'Source', name: 'source.name', hiddenName: 'source.id', xtype: 'combo',
				url: 'reconciliationSourceListFind.json', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationSourceWindow',
				loadAll: true, allowBlank: false, qtip: 'Source of the data',
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const formPanel = TCG.getParentFormPanel(combo);
					const form = formPanel.getForm();
					const ids = [];
					ids.push(formPanel.getFormValue('definitionField.reconciliationDefinition.primarySource.id'));
					ids.push(formPanel.getFormValue('definitionField.reconciliationDefinition.secondarySource.id'));
					combo.store.baseParams = {
						ids: ids
					};
					form.findField('functionRule.name').clearAndReset();
					form.findField('conditionRule.name').clearAndReset();
				}
			},
			{fieldLabel: 'Name', name: 'name', xtype: 'textfield', style: 'text-decoration:overline; text-decoration-style: dashed', allowBlank: false, qtip: 'Specifies the field name used for functions and conditions.'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', allowBlank: true, qtip: 'Description of the field name used for functions and conditions.'},
			{
				fieldLabel: 'Function', name: 'functionRule.name', hiddenName: 'functionRule.id', xtype: 'combo', loadAll: true, qtip: 'Function to apply to the source field value that transforms it to reconciliation friendly format.',
				detailPageClass: 'Clifton.reconciliation.rule.ReconciliationRuleFunctionWindow', url: 'reconciliationRuleListFind.json',
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const formPanel = TCG.getParentFormPanel(combo);
					const form = formPanel.getForm();
					const sourceId = form.findField('source.id').getValue();
					if (TCG.isBlank(sourceId)) {
						TCG.showError('You must select a source first!');
						return false;
					}
					const primarySourceId = formPanel.getFormValue('definitionField.reconciliationDefinition.primarySource.id');
					const ruleGroupId = (TCG.isEquals(sourceId, primarySourceId) ? formPanel.getFormValue('definitionField.reconciliationDefinition.primaryRuleGroup.id') : formPanel.getFormValue('definitionField.reconciliationDefinition.secondaryRuleGroup.id'));
					combo.clearAndReset();
					combo.store.baseParams = {
						ruleType: 'FUNCTION',
						ruleGroupId: ruleGroupId
					};
				},
				getDefaultData: function(formPanel) {
					const definitionId = formPanel.findField('definitionField.reconciliationDefinition.id').getValue();
					const sourceId = formPanel.findField('source.id').getValue();
					const name = formPanel.findField('name').getValue();
					if (TCG.isBlank(definitionId) || TCG.isBlank(sourceId) || TCG.isBlank(name)) {
						TCG.showError('You must select a definition and source and provide a name first!');
						return false;
					}
					return {
						defaultField: {name: name, definitionField: formPanel.formValues.definitionField},
						definitionId: definitionId,
						sourceId: sourceId
					};
				}
			},
			{
				fieldLabel: 'Condition', name: 'conditionRule.name', hiddenName: 'conditionRule.id', xtype: 'combo', loadAll: true, qtip: 'Condition that must be met for this field mapping to apply.',
				detailPageClass: 'Clifton.reconciliation.rule.ReconciliationRuleConditionWindow', url: 'reconciliationRuleListFind.json',
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const formPanel = TCG.getParentFormPanel(combo);
					const form = formPanel.getForm();
					const sourceId = form.findField('source.id').getValue();
					if (TCG.isBlank(sourceId)) {
						TCG.showError('You must select a source first!');
						return false;
					}
					const primarySourceId = formPanel.getFormValue('definitionField.reconciliationDefinition.primarySource.id');
					const ruleGroupId = (TCG.isEquals(sourceId, primarySourceId) ? formPanel.getFormValue('definitionField.reconciliationDefinition.primaryRuleGroup.id') : formPanel.getFormValue('definitionField.reconciliationDefinition.secondaryRuleGroup.id'));
					combo.clearAndReset();
					combo.store.baseParams = {
						ruleType: 'CONDITION',
						ruleGroupId: ruleGroupId
					};
				},
				getDefaultData: function(formPanel) {
					const definitionId = formPanel.findField('definitionField.reconciliationDefinition.id').getValue();
					const sourceId = formPanel.findField('source.id').getValue();
					const name = formPanel.findField('name').getValue();
					if (TCG.isBlank(definitionId) || TCG.isBlank(sourceId) || TCG.isBlank(name)) {
						TCG.showError('You must select a definition and source and provide a name first!');
						return false;
					}
					return {
						defaultField: {name: name, definitionField: formPanel.formValues.definitionField},
						definitionId: definitionId,
						sourceId: sourceId
					};
				}
			},
			{fieldLabel: 'Evaluation Order', name: 'evaluationOrder', xtype: 'spinnerfield', type: 'int', minValue: '1', qtip: 'Specifies the evaluation order of the field mappings', value: 10}

		]
	}]
});
