Clifton.reconciliation.definition.group.ReconciliationDefinitionGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Definition Group',
	iconCls: 'grouping',
	width: 650,
	height: 350,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Definition Group',
				items: [{
					xtype: 'formpanel',
					instructions: 'Reconciliation Definition Groups can be used to classify definitions together for event dispatching.',
					url: 'reconciliationDefinitionGroup.json',
					labelWidth: 150,
					items: [
						{fieldLabel: 'Group Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Run Approval Condition', name: 'runApprovalSystemCondition.name', hiddenName: 'runApprovalSystemCondition.id', xtype: 'combo', url: 'systemConditionListFind.json', detailPageClass: 'Clifton.system.condition.ConditionWindow'},
						{fieldLabel: 'Run Modify Condition', name: 'runModifySystemCondition.name', hiddenName: 'runModifySystemCondition.id', xtype: 'combo', url: 'systemConditionListFind.json', detailPageClass: 'Clifton.system.condition.ConditionWindow'},
						{fieldLabel: 'Event Trigger', name: 'eventTrigger', xtype: 'checkbox'}
					]
				}]
			},
			{
				name: 'reconciliationDefinitionListFind',
				title: 'Reconciliation Definitions',
				appendStandardColumns: false,
				xtype: 'gridpanel',
				getLoadParams: function() {
					return {
						'definitionGroupId': this.getWindow().getMainFormId()
					};
				},
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Type', dataIndex: 'type.name', width: 100, filter: {type: 'combo', searchFieldName: 'reconciliationTypeId', url: 'reconciliationTypeListFind.json'}},
					{header: 'Definition Name', width: 150, dataIndex: 'name'},
					{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
					{header: 'Primary Source', dataIndex: 'primarySource.name', width: 100, filter: {type: 'combo', searchFieldName: 'primarySourceId', url: 'reconciliationSourceListFind.json'}},
					{header: 'Secondary Source', dataIndex: 'secondarySource.name', width: 100, filter: {type: 'combo', searchFieldName: 'secondarySourceId', url: 'reconciliationSourceListFind.json'}}
				],
				editor: {
					drillDownOnly: true,
					detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionWindow'
				}
			}]
	}]
});

