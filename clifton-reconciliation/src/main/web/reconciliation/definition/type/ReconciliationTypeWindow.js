Clifton.reconciliation.definition.type.ReconciliationTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Definition Type',
	iconCls: 'grouping',
	width: 600,
	height: 350,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Reconciliation Type',
				items: [{
					xtype: 'formpanel',
					labelWidth: 140,
					instructions: 'Reconciliation Types are the highest level categorization of reconciliation runs.',
					url: 'reconciliationType.json',
					items: [
						{fieldLabel: 'Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Run Approval Condition', name: 'runApprovalSystemCondition.name', hiddenName: 'runApprovalSystemCondition.id', xtype: 'combo', url: 'systemConditionListFind.json', detailPageClass: 'Clifton.system.condition.ConditionWindow'},
						{fieldLabel: 'Run Modify Condition', name: 'runModifySystemCondition.name', hiddenName: 'runModifySystemCondition.id', xtype: 'combo', url: 'systemConditionListFind.json', detailPageClass: 'Clifton.system.condition.ConditionWindow'},
						{fieldLabel: 'Cumulative Reconciliation', name: 'cumulativeReconciliation', xtype: 'checkbox'}
					]
				}]
			},
			{
				name: 'reconciliationTypeRequiredNaturalKeyListFind',
				title: 'Required Natural Keys',
				appendStandardColumns: false,
				xtype: 'gridpanel',
				getLoadParams: function() {
					return {
						'typeId': this.getWindow().getMainFormId()
					};
				},
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Type', dataIndex: 'reconciliationType.name', width: 100, filter: {type: 'combo', searchFieldName: 'reconciliationTypeId', url: 'reconciliationTypeListFind.json'}},
					{header: 'Field', dataIndex: 'reconciliationField.name', width: 100, filter: {type: 'combo', searchFieldName: 'reconciliationFieldId', url: 'reconciliationFieldListFind.json'}}
				],
				editor: {
					detailPageClass: 'Clifton.reconciliation.definition.type.ReconciliationTypeRequiredNaturalKeyWindow'
				}
			}]
	}]
});

