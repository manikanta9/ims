Clifton.reconciliation.definition.type.ReconciliationTypeRequiredNaturalKeyWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Definition Type Required Natural Key',
	iconCls: 'grouping',
	width: 600,
	height: 350,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Required Natural Key',
				items: [{
					xtype: 'formpanel',
					labelWidth: 140,
					instructions: 'Reconciliation Types require certain fields to be used as natural keys.',
					url: 'reconciliationTypeRequiredNaturalKey.json',
					items: [
						{fieldLabel: 'Type Name', name: 'reconciliationType.name', hiddenName: 'reconciliationType.id', xtype: 'combo', url: 'reconciliationTypeListFind.json'},
						{fieldLabel: 'Description', name: 'reconciliationField.name', hiddenName: 'reconciliationField.id', xtype: 'combo', url: 'reconciliationFieldListFind.json'}
					]
				}]
			}
		]
	}]
});
