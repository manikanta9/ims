Clifton.reconciliation.definition.preprocessor.ReconciliationDefinitionPreprocessorWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Definition Preprocessor Mapping',
	iconCls: 'services',
	width: 700,
	height: 320,

	items: [{
		xtype: 'formpanel',
		labelWidth: 150,
		labelFieldName: 'preprocessor.name',
		instructions: 'Maps selected Reconciliation Preprocessors to this Reconciliation Definition.  Preprocessors manipulate the imported data prior to applying filters and field mappings.',
		url: 'reconciliationDefinitionPreprocessor.json?requestedMaxDepth=4',
		items: [
			{fieldLabel: 'Reconciliation Definition', name: 'reconciliationDefinition.name', detailIdField: 'reconciliationDefinition.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionWindow'},
			{
				fieldLabel: 'Preprocessor ', name: 'preprocessor.name', hiddenName: 'preprocessor.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Reconciliation Preprocessors', detailPageClass: 'Clifton.system.bean.BeanWindow', qtip: 'Preprocessors manipulate the imported data prior to applying filters and field mappings.',
				listeners: {
					beforeselect: function(combo, record) {
						const fp = combo.getParentForm();
						fp.setFormValue('preprocessor.description', record.json.description);
					}
				}
			},
			{name: 'preprocessor.description', xtype: 'textarea', readOnly: true},
			{fieldLabel: 'Evaluation Order', name: 'evaluationOrder', xtype: 'spinnerfield', qtip: 'Evaluation order determines the order in which the processors are applied to the data rows.'}
		]
	}]
});
