Clifton.reconciliation.definition.ReconciliationDefinitionFieldWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Definition Field',
	iconCls: 'list',
	width: 1200,
	height: 775,

	items: [{
		xtype: 'formpanel',
		labelWidth: 120,
		instructions: 'A Reconciliation Definition Field determines how an objects unique key should be computed for reconciliation.',
		url: 'reconciliationDefinitionField.json?requestedMaxDepth=4',
		getFormLabel: function() {
			return this.getFormValue('field.name');
		},
		items: [
			{fieldLabel: 'Definition', name: 'reconciliationDefinition.name', detailIdField: 'reconciliationDefinition.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionWindow', qtip: 'The definition this field belongs to.'},
			{
				fieldLabel: 'Field', name: 'field.name', hiddenName: 'field.id', xtype: 'combo', requestedProps: 'defaultDataType', url: 'reconciliationFieldListFind.json', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationFieldWindow', qtip: 'Defines the field to be used for this definition field.  Fields are a centralized configuration that allow for specifying the default data type and display width in grids.',
				listeners: {
					select: function(combo, record, index) {
						const defaultDataTypeField = TCG.getParentFormPanel(combo).getForm().findField('field.defaultDataType');
						defaultDataTypeField.setValue(record.json.defaultDataType);

					}
				}
			},
			{
				xtype: 'columnpanel',
				columns: [
					{
						rows: [
							{
								xtype: 'columnpanel',
								columns: [{
									rows: [{
										fieldLabel: 'Default Data Type', name: 'field.defaultDataType', hiddenName: 'field.defaultDataType', xtype: 'displayfield', qtip: 'The data type of this field, e.g. STRING, MONEY, QUANTITY, etc., it determines how processing and filtering is handled for the field during data loads and display in grids.'
									}]
								}, {
									rows: [{
										fieldLabel: 'Override Data Type', name: 'dataType', hiddenName: 'dataType',
										mode: 'local', xtype: 'combo', allowBlank: true, qtip: 'Optional dataType override.  Should rarely be used.',
										store: {
											xtype: 'arraystore',
											data: Clifton.reconciliation.DataTypes
										}
									}]
								}]
							},
							{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', qtip: 'Specifies the start date of this field definition.'}
						]
					},
					{
						rows: [
							{fieldLabel: 'Display Order', name: 'displayOrder', xtype: 'integerfield', qtip: 'Specifies the display order of the fields on the UI.'},
							{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', qtip: 'Specifies the end date of this field definition.'}
						]
					}
				]
			},
			{
				fieldLabel: 'Configuration Flags', xtype: 'checkboxgroup', columns: 3, qtip: 'The configuration flags determine how this definition field is used; each configuration options has a more detailed description of its specific use.',
				items: [
					{boxLabel: 'Natural Key', name: 'naturalKey', xtype: 'checkbox', mutuallyExclusiveFields: ['reconcilable'], qtip: 'Specifies if this key definition is used as a natural key used in creation of the unique identifier for data.'},
					{boxLabel: 'Reconcilable', name: 'reconcilable', xtype: 'checkbox', mutuallyExclusiveFields: ['naturalKey'], qtip: 'Specifies if this key definition specifies fields that should be reconciled when processing data.'},
					{boxLabel: 'Hidden', name: 'hidden', xtype: 'checkbox', qtip: 'Specifies if this key definition should be displayed in detail windows.'},
					{xtype: 'label', html: '<br /><br />'},
					{boxLabel: 'Aggregate', name: 'aggregate', xtype: 'checkbox', qtip: 'Specifies if this key definition should be aggregated/summed during merging of two or more rows of data for the same natural key during import.'},
					{boxLabel: 'Adjustable', name: 'adjustable', xtype: 'checkbox', qtip: 'Specifies if this key definition can be used to create adjustments.'}
				]
			},

			{
				xtype: 'gridpanel',
				//Disable by default - enable once DefinitionField has been applied/saved. (see 'afterLoad' method)
				disabled: true,
				loadValidation: false,
				title: 'Reconciliation Definition Field Mappings',
				name: 'reconciliationDefinitionFieldMappingListFind',
				instructions: 'Reconciliation Field Mappings designate the column name for a given source a value should be pulled from.',
				frame: true,
				height: 250,
				heightResized: true,
				columns: [
					{header: 'ID', width: 25, dataIndex: 'id', hidden: true},
					{header: 'Field Name', width: 200, dataIndex: 'definitionField.field.name', idDataIndex: 'definitionField.field.id', hidden: true},
					{header: 'Source', width: 200, dataIndex: 'source.name', idDataIndex: 'source.id'},
					{header: 'Source Field Name', width: 350, dataIndex: 'name'},
					{header: 'Field Function', width: 350, dataIndex: 'functionRule.name'},
					{header: 'Field Condition', width: 350, dataIndex: 'conditionRule.name'},
					{header: 'Evaluation Order', width: 200, dataIndex: 'evaluationOrder', type: 'int'}
				],
				getLoadParams: function(firstLoad) {
					if (TCG.isNotBlank(this.getWindow().getMainFormId())) {
						return {
							definitionFieldId: this.getWindow().getMainFormId(),
							requestedMaxDepth: 3
						};
					}
					return false;
				},
				editor: {
					detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionFieldMappingWindow',
					getDefaultData: function(gridPanel) {
						return {
							definitionField: gridPanel.getWindow().getMainForm().formValues
						};
					}
				}
			},

			{
				title: 'Evaluators',
				xtype: 'gridpanel',
				name: 'reconciliationEvaluatorListFind',
				instructions: 'Reconciliation Evaluators are used to specify how values should be reconciled. For example, used to specify thresholds to avoid penny rounding breaks.',
				height: 250,
				heightResized: true,
				collapsible: true,
				//Disable by default - enable once DefinitionField has been applied/saved. (see 'afterLoad' method)
				disabled: true,
				getTopToolbarFilters: function(toolbar) {
					return [
						{fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}
					];
				},
				columns: [
					{header: 'ID', width: 25, dataIndex: 'id', hidden: true},
					{header: 'Evaluator Name', width: 300, dataIndex: 'name', hidden: true},
					{header: 'Evaluator Bean', width: 200, dataIndex: 'evaluatorBean.name'},
					{header: 'Definition Field', width: 200, dataIndex: 'definitionField.field.name', hidden: true},
					{header: 'Holding Account', width: 200, dataIndex: 'reconciliationInvestmentAccount.label', filter: {searchFieldName: 'accountNumber'}},
					{header: 'Investment Security', width: 200, dataIndex: 'reconciliationInvestmentSecurity.securitySymbol'},
					{header: 'Custom Scope Condition', width: 200, dataIndex: 'conditionRule.name'},
					{header: 'Start Date', width: 75, dataIndex: 'startDate'},
					{header: 'End Date', width: 75, dataIndex: 'endDate'},
					{header: 'Order', width: 75, dataIndex: 'evaluationOrder', type: 'int'}
				],
				getLoadParams: function(firstLoad) {
					const definitionFieldId = this.getWindow().getMainFormId();
					//If parent form doesn't have an id; don't load the grid
					if (TCG.isBlank(definitionFieldId)) {
						return false;
					}

					const loadParams = {
						definitionFieldId: definitionFieldId
					};
					const toolbar = this.getTopToolbar();
					const activeOnDate = TCG.getChildByName(toolbar, 'activeOnDate');
					if (firstLoad) {
						// default to today
						if (TCG.isBlank(activeOnDate.getValue())) {
							activeOnDate.setValue((new Date()).format('m/d/Y'));
							activeOnDate.originalValue = activeOnDate.getValue(); // prevent on change event
						}
					}
					if (TCG.isNotBlank(activeOnDate.getValue())) {
						loadParams.activeOnDate = (activeOnDate.getValue()).format('m/d/Y');
					}
					return loadParams;
				},
				editor: {
					detailPageClass: 'Clifton.reconciliation.definition.rule.ReconciliationEvaluatorWindow',
					getDefaultData: function(gridPanel) {
						return {
							definitionField: gridPanel.getWindow().getMainForm().formValues,
							startDate: new Date().format('Y-m-d 00:00:00')
						};
					}
				}
			}
		],
		listeners: {
			afterload: function(formPanel, closeOnSuccess) {
				// Skip UI modifications if window is about to be closed
				if (!closeOnSuccess) {
					formPanel.enableFields();
				}
			}
		},
		enableFields: function() {
			const formPanel = this;
			const hasName = !!formPanel.getFormValue('name');
			const hasDefinition = !!formPanel.getFormValue('reconciliationDefinition.id');
			const hasDataType = !!formPanel.getFormValue('dataTypeName');
			if (!!formPanel.getIdFieldValue() || (hasName && hasDefinition && hasDataType)) {
				formPanel.findByType('gridpanel')[0].enable();
				if (TCG.isTrue(formPanel.getFormValue('reconcilable'))) {
					formPanel.findByType('gridpanel')[1].enable();
				}
			}
		}
	}]
});
