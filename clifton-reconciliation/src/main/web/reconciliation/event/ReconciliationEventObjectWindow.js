Clifton.reconciliation.event.ReconciliationEventObjectWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation Event Object',
	iconCls: 'event',
	width: 1000,
	height: 400,

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		url: 'reconciliationEventObject.json',
		items: [
			{fieldLabel: 'Run Object', name: 'runObject.id', detailIdField: 'runObject.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.run.ReconciliationRunObjectDetailWindow'},
			{fieldLabel: 'Parent Run Object', name: 'parentRunObject.id', detailIdField: 'parentRunObject.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconciliation.run.ReconciliationRunObjectDetailWindow'},
			{fieldLabel: 'Event Category', name: 'eventObjectDataType.eventObjectCategory', xtype: 'displayfield'},
			{fieldLabel: 'Event Type', name: 'eventObjectDataType.name', xtype: 'displayfield'},
			{fieldLabel: 'Holding Account', name: 'eventObjectData.holdingAccountNumber'},
			{fieldLabel: 'Description', name: 'eventObjectData.description', xtype: 'textarea'},
			{fieldLabel: 'Transaction Date', name: 'eventObjectData.transactionDate'},
			{fieldLabel: 'Settlement Date', name: 'eventObjectData.settlementDate'},
			{fieldLabel: 'Amount', name: 'eventObjectData.amount'}
		]
	}],
	saveWindow: function(closeOnSuccess, forceSubmit) {
		let id;
		const win = this;
		const runObjectId = win.getMainFormPanel().getForm().findField('runObject.id').getValue();
		const eventObjectCategory = win.getMainFormPanel().getForm().findField('eventObjectCategory').getValue();
		const eventObjectDataType = win.getMainFormPanel().getForm().findField('eventObjectData.eventObjectDataType').getValue();
		const holdingAccountNumber = win.getMainFormPanel().getForm().findField('eventObjectData.holdingAccountNumber').getValue();
		const description = win.getMainFormPanel().getForm().findField('eventObjectData.description').getValue();
		const transactionDate = win.getMainFormPanel().getForm().findField('eventObjectData.transactionDate').getValue();
		const settlementDate = win.getMainFormPanel().getForm().findField('eventObjectData.settlementDate').getValue();
		const amount = win.getMainFormPanel().getForm().findField('eventObjectData.amount').getValue();

		if (!TCG.isBlank(win.getMainFormId())) {
			id = win.getMainFormId();
		}

		const eventObjectData = {
			eventObjectDataType: eventObjectDataType,
			holdingAccountNumber: holdingAccountNumber,
			description: description,
			transactionDate: TCG.parseDate(transactionDate).format('Y-m-d H:m:s.ssss'),
			settlementDate: TCG.parseDate(settlementDate).format('Y-m-d H:m:s.ssss'),
			amount: amount
		};
		//Set the type info for the eventObjectData
		eventObjectData['@class'] = 'com.clifton.reconciliation.event.object.data.ReconciliationEventObjectJournalData';

		const jsonData = {
			eventObject: {
				id: id,
				runObject: {id: runObjectId},
				eventObjectCategory: eventObjectCategory,
				eventObjectData: eventObjectData
			}
		};

		const loader = new TCG.data.JsonLoader({
			waitTarget: this,
			waitMsg: 'Saving...',
			params: {jsonParameterTypeClassNames: ['com.clifton.reconciliation.event.object.ReconciliationEventObjectJournal']},
			jsonData: jsonData,
			onLoad: function(record, conf) {
				//Only set the value if the openerCt was a combo box.
				if (TCG.isNotBlank(win.openerCt) && TCG.isEqualsStrict('combo', win.openerCt.xtype)) {
					win.openerCt.setValue({value: record.id, text: record.name});
				}
				win.doNotWarnOnCloseModified = true;
				win.closeWindow();
			}
		});
		//pre-pended with json/ because we are posting raw json to the server; not a form post.
		//TODO - change to be based on mime-type, not pre-pended url.
		loader.load('json/reconciliationEventObjectSave.json');
	}
});
