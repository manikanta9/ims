Clifton.reconciliation.event.ReconciliationEventObjectDataTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation Event Data Type',
	iconCls: 'event',
	width: 650,
	height: 400,

	items: [{
		xtype: 'formpanel',
		labelWidth: 130,
		url: 'reconciliationEventObjectDataType.json',
		items: [
			{
				fieldLabel: 'Event Category', name: 'eventObjectCategory', hiddenName: 'eventObjectCategory', // need the hiddenName defined otherwise when the form is submitted, the displayField is submitted instead of the valueField
				xtype: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
				store: {
					xtype: 'arraystore',
					fields: ['value', 'name', 'description'],
					data: Clifton.reconciliation.event.ReconciliationEventObjectCategories
				}
			},
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Type Label', name: 'label'},
			{fieldLabel: 'Description', name: 'description'},
			{fieldLabel: 'Default Note', name: 'defaultNote', xtype: 'textarea'},

			{
				fieldLabel: 'Configuration Flags', xtype: 'checkboxgroup', columns: 3, qtip: 'The configuration flags determine how this definition field is used; each configuration options has a more detailed description of its specific use.',
				items: [
					{boxLabel: 'Approval Required', name: 'approvalRequired', xtype: 'checkbox'},
					{boxLabel: 'Absolute Value', name: 'absolute', xtype: 'checkbox', qtip: 'Specifies the amount field should use an absolute value for the event object'},
					{boxLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox'},
					{boxLabel: 'Negative Adjustment', name: 'negativeAdjustment', xtype: 'checkbox', qtip: 'The amount field is considered negative for cash adjustments'}
				]
			},
			{fieldLabel: 'Approval Threshold', name: 'approvalThresholdAmount'},
			{fieldLabel: 'Icon Class', name: 'iconClass'}
		]
	}]
});
