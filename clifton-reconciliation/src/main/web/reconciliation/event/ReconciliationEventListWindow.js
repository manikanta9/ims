Clifton.reconciliation.event.ReconciliationEventListWindow = Ext.extend(TCG.app.Window, {
	id: 'reconciliationEventListWindow',
	iconCls: 'event',
	title: 'Reconciliation Events',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Journal Events',
				items: [{
					name: 'reconciliationEventObjectListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all Simple Journal Events in the system.  Simple Journal Events are used to create and post cash adjustment, cash contribution journals, etc.',
					getLoadParams: function(firstLoad) {
						const params = {};
						params.excludeDataTypeCategory = 'ACCOUNT';
						return params;
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Run Object', width: 40, dataIndex: 'runObject.id', type: 'int', useNull: true, hidden: true, filter: {searchFieldName: 'runObjectId'}},
						{header: 'Parent Run Object', width: 50, dataIndex: 'parentRunObject.id', type: 'int', useNull: true, hidden: true, filter: {searchFieldName: 'parentRunObjectId'}},
						{header: 'Event Category', width: 50, dataIndex: 'eventObjectDataType.eventObjectCategory', hidden: true, filter: {searchFieldName: 'dataTypeCategory', type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.reconciliation.event.ReconciliationEventObjectCategories}}},
						{header: 'Event Type', width: 50, dataIndex: 'eventObjectDataType.name', filter: {type: 'combo', searchFieldName: 'dataTypeId', url: 'reconciliationEventObjectDataTypeListFind.json'}},
						{header: 'Holding Account', width: 40, dataIndex: 'eventObjectData.holdingAccountNumber', filter: {searchFieldName: 'holdingAccountNumber'}},
						{header: 'Description', width: 100, dataIndex: 'eventObjectData.description', hidden: true, filter: {searchFieldName: 'description'}},
						{header: 'Amount', width: 40, dataIndex: 'eventObjectData.amount', type: 'float', useNull: true, filter: {searchFieldName: 'amount'}},
						{header: 'Event Note', width: 75, dataIndex: 'eventObjectData.eventNote', filter: {searchFieldName: 'eventNote'}},
						{
							header: 'Status', width: 35, dataIndex: 'status', filter: {type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.reconciliation.event.ReconcilaitionEventObjectStatuses}}
						},
						{header: 'Status Message', width: 100, dataIndex: 'statusMessage'},
						{
							header: 'Original Transaction Date', width: 50, dataIndex: 'eventObjectData.originalTransactionDate', hidden: true, filter: {searchFieldName: 'originalTransactionDate'},
							renderer: function(v, metaData, r) {
								return v ? v.format('m/d/Y') : undefined;
							}
						},
						{
							header: 'Transaction Date', width: 40, dataIndex: 'eventObjectData.transactionDate', hidden: true, filter: {searchFieldName: 'transactionDate'},
							renderer: function(v, metaData, r) {
								return v ? v.format('m/d/Y') : undefined;
							}
						},
						{
							header: 'Settlement Date', width: 40, dataIndex: 'eventObjectData.settlementDate', filter: {searchFieldName: 'settlementDate'},
							renderer: function(v, metaData, r) {
								return v ? v.format('m/d/Y') : undefined;
							}
						},
						{
							header: 'Event Date', width: 40, dataIndex: 'eventObjectData.eventDate', filter: {searchFieldName: 'eventDate'},
							renderer: function(v, metaData, r) {
								return v ? v.format('m/d/Y') : undefined;
							}
						}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.event.ReconciliationEventObjectWindow'
					}
				}]
			},
			{
				title: 'Account Events',
				items: [{
					name: 'reconciliationEventObjectListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all Reconciliation Account Events in the system.  Events are used to communicate actions to other systems.  For example, lock/unlock account, create and post cash adjustment, etc.',
					getLoadParams: function(firstLoad) {
						const params = {};
						if (firstLoad) {
							this.setFilterValue('eventObjectData.eventDate', {'on': Clifton.calendar.getBusinessDayFrom(0)});
						}
						params.dataTypeCategory = 'ACCOUNT';
						return params;
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Run Object', width: 40, dataIndex: 'runObject.id', type: 'int', useNull: true, hidden: true, filter: {searchFieldName: 'runObjectId'}},
						{header: 'Parent Run Object', width: 50, dataIndex: 'parentRunObject.id', type: 'int', useNull: true, hidden: true, filter: {searchFieldName: 'parentRunObjectId'}},
						{header: 'Event Category', width: 50, dataIndex: 'eventObjectDataType.eventObjectCategory', hidden: true, filter: {searchFieldName: 'dataTypeCategory', type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.reconciliation.event.ReconciliationEventObjectCategories}}},
						{header: 'Event Type', width: 50, dataIndex: 'eventObjectDataType.name', filter: {type: 'combo', searchFieldName: 'dataTypeId', url: 'reconciliationEventObjectDataTypeListFind.json'}},
						{header: 'Holding Account', width: 50, dataIndex: 'eventObjectData.holdingAccountNumber', filter: {searchFieldName: 'holdingAccountNumber'}},
						{
							header: 'Status', width: 35, dataIndex: 'status', filter: {type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.reconciliation.event.ReconcilaitionEventObjectStatuses}}
						},
						{header: 'Status Message', width: 150, dataIndex: 'statusMessage'},
						{header: 'Event Note', width: 50, dataIndex: 'eventObjectData.eventNote', filter: {searchFieldName: 'eventNote'}},
						{
							header: 'Event Date', width: 40, dataIndex: 'eventObjectData.eventDate', filter: {searchFieldName: 'eventDate'},
							renderer: function(v, metaData, r) {
								return v ? v.format('m/d/Y') : undefined;
							}
						}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.event.ReconciliationEventObjectWindow'
					}
				}]
			},
			{
				title: 'Event Types',
				items: [{
					name: 'reconciliationEventObjectDataTypeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all event types that the reconciliation system can send to the main system for automatic processing. For each event category, there will usually be a corresponding processor implemented on receiving end.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Event Category', width: 50, dataIndex: 'eventObjectCategory'},
						{header: 'Type Name', width: 75, dataIndex: 'name'},
						{header: 'Label', width: 75, dataIndex: 'label', hidden: true},
						{header: 'Description', width: 150, dataIndex: 'description', hidden: true},
						{header: 'Default Note', dataIndex: 'defaultNote', width: 150},
						{header: 'Approval Required', width: 50, dataIndex: 'approvalRequired', type: 'boolean'},
						{header: 'Absolute Value', width: 50, dataIndex: 'absolute', type: 'boolean'},
						{header: 'System Defined', width: 50, dataIndex: 'systemDefined', type: 'boolean'},
						{header: 'Negative Adjustment', width: 50, dataIndex: 'negativeAdjustment', type: 'boolean'},
						{header: 'Approval Threshold', width: 50, dataIndex: 'approvalThresholdAmount', type: 'float', useNull: true},
						{
							header: 'Icon Class', dataIndex: 'iconClass', width: 50,
							renderer: function(v, metaData, r) {
								return TCG.renderIconWithTooltip(v, '');
							}
						}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.event.ReconciliationEventObjectDataTypeWindow'
					}
				}]
			}
		]
	}
	]
})
;
