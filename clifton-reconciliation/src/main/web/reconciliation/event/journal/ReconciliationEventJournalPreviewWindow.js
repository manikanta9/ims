Clifton.reconciliation.event.journal.ReconciliationEventJournalPreviewWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Reconciliation Event Journal Preview Window',
	iconCls: 'run',
	width: 1400,
	height: 500,
	okButtonText: 'Create Journals',
	cancelButtonText: 'Close',
	hideApplyButton: true,
	items: [{
		name: 'eventJournalFormPanel',
		xtype: 'formpanel',
		items: [
			{fieldLabel: 'Event Date', name: 'eventDate', xtype: 'datefield', qtip: 'Defines the date the event was created, i.e. this determines what run the adjustment should apply to.', required: true},
			{fieldLabel: 'Global Date Override', name: 'globalDate', xtype: 'datefield', qtip: 'Global override for the date of all events being created.  If this is set, individual journal entries will be ignored and set to this date.'},
			{fieldLabel: 'Note', name: 'globalNote', xtype: 'textarea'},
			{
				xtype: 'editorgrid',
				name: 'reconciliationEventObjectListFind',
				border: true,
				height: 325,
				appendStandardColumns: false,
				isPagingEnabled: function() {
					return false;
				},
				getLoadParams: function(firstLoad) {
					return {runObjectIds: this.getWindow().defaultData.runObjectIds};
				},
				updateRecord: function() {
					// DO NOT REMOVE: This is here to avoid automatic saving of the grid
				},
				columns: [
					{header: 'ID', width: 25, dataIndex: 'id', hidden: true},
					{header: 'Run Object ID', width: 25, dataIndex: 'runObject.id', hidden: true},
					{header: 'Data Type ID', width: 25, dataIndex: 'eventObjectDataType.id', hidden: true},
					{
						header: 'Journal Name', width: 75, dataIndex: 'eventObjectDataType.name'
					},
					{
						header: 'Holding Account', width: 50, dataIndex: 'eventObjectData.holdingAccountNumber'
					},
					{
						header: 'Security', width: 50, dataIndex: 'eventObjectData.securityIdentifier'
					},
					{header: 'Parent Run Object ID', width: 25, dataIndex: 'parentRunObject.id', hidden: true},
					{
						header: 'Settlement Date', width: 50, dataIndex: 'eventObjectData.settlementDate',
						editor: {
							xtype: 'datefield'
						},
						renderer: function(v, metaData, r) {
							return v.format('m/d/y');
						}
					},
					{
						header: 'Transaction Date', width: 50, dataIndex: 'eventObjectData.transactionDate',
						editor: {
							xtype: 'datefield'
						},
						renderer: function(v, metaData, r) {
							return v.format('m/d/y');
						}
					},
					{
						header: 'Amount', width: 50, dataIndex: 'eventObjectData.amount',
						renderer: function(v, metaData, r) {
							return TCG.renderAmount(v, false, '0,000.00');
						},
						editor: {
							xtype: 'floatfield'
						}
					},
					{header: 'Status', width: 75, dataIndex: 'status'},
					{
						header: 'Status Message', width: 125, dataIndex: 'statusMessage',
						renderer: (value, metadata, record, rowIndex, colIndex, store) => `<span class="action-column" ext:qtip="${value}">${value}</span>`
					},
					{
						header: 'Note', width: 124, dataIndex: 'eventObjectData.eventNote',
						editor: {
							xtype: 'textarea'
						}
					}
				],
				editor: {
					drillDownOnly: true,
					addEditButtons: function(toolBar) {
						const grid = this.grid;
						toolBar.add('-');
						toolBar.add({
							text: 'Remove',
							tooltip: 'Remove selected item',
							iconCls: 'remove',
							scope: this,
							handler: async function() {
								if ('yes' === await TCG.showConfirm('Are you sure you wish to delete this item?  This will only delete from reconciliation, if the event has been processed, you must manually delete the associated journal in IMS.', 'Delete Event Journal')) {
									const store = grid.getStore();
									const selection = grid.getSelectionModel().getSelected();
									TCG.data.getDataPromise(`reconciliationEventObjectDelete.json?id=${selection.id}`, this).then(function(trade) {
										const index = store.indexOf(selection);
										if (index >= 0) {
											store.remove(store.getAt(index));
											grid.doLayout();
										}
									});
								}
							}
						});
					}
				}
			}
		]
	}],
	saveWindow: function(closeOnSuccess, forceSubmit) {
		let emptyNotes = false;
		const formPanel = TCG.getChildByName(this, 'eventJournalFormPanel');
		const eventDate = formPanel.getFormValue('eventDate', true);
		const globalDate = formPanel.getFormValue('globalDate', true);

		const eventObjectList = [];
		const gridPanel = TCG.getChildByName(this, 'reconciliationEventObjectListFind');
		gridPanel.grid.getStore().each(function(record) {
			if (TCG.isBlank(record.data['eventObjectData.eventNote'])) {
				emptyNotes = true;
			}
			//We either use the settlement date for all three date fields, or the global date
			//Simple journals will never have different dates for these fields, so we use one for simplicity.
			let settlementDate = record.data['eventObjectData.settlementDate'];
			let transactionDate = record.data['eventObjectData.transactionDate'];
			if (!TCG.isBlank(globalDate)) {
				settlementDate = globalDate;
				transactionDate = globalDate;

			}
			const eventObjectData = {
				eventDate: TCG.parseDate(eventDate).format('Y-m-d H:m:s.ssss'),
				eventNote: record.data['eventObjectData.eventNote'],
				sourceTable: 'ReconciliationRunObject',
				sourceFkFieldId: record.data['runObject.id'],
				amount: record.data['eventObjectData.amount'],
				holdingAccountNumber: record.data['eventObjectData.holdingAccountNumber'],
				securityIdentifier: record.data['eventObjectData.securityIdentifier'],
				settlementDate: TCG.parseDate(settlementDate).format('Y-m-d H:m:s.ssss'),
				transactionDate: TCG.parseDate(transactionDate).format('Y-m-d H:m:s.ssss'),
				originalTransactionDate: TCG.parseDate(transactionDate).format('Y-m-d H:m:s.ssss')
			};
			//Set the type info for the eventObjectData
			eventObjectData['@class'] = 'com.clifton.reconciliation.event.object.data.ReconciliationEventObjectJournalData';

			const eventObjectDataType = {id: record.data['eventObjectDataType.id']};

			//TODO - I pass only the id of the runObject and hydrate the object in the service
			//I could update the grid to pass the identifier, and then set the identifier and runId in the json I pass
			//which is the data I need to look up the object by naturalkey so duplicates are not created; for now I hydrate
			//server side as adding this data would require modifying the SQL json queries and updating the UI.
			const eventObject = {
				id: record.data['id'],
				runObject: {
					id: record.data['runObject.id']
				},
				eventObjectDataType: eventObjectDataType,
				eventObjectData: eventObjectData,
				status: record.data['status']
			};
			if (record.data['parentRunObject.id']) {
				eventObject['parentRunObject'] = {id: record.data['parentRunObject.id']};
			}
			//Set the type info the eventObject
			eventObject['@class'] = 'com.clifton.reconciliation.event.object.ReconciliationEventObjectJournal';
			eventObjectList.push(eventObject);
		});

		const globalNote = formPanel.getFormValue('globalNote', true);
		//If there are any empty notes for the individual items and there is not a global note then do not submit.
		if (TCG.isBlank(globalNote) && emptyNotes) {
			TCG.showError('A note is required for all adjustments!');
		}
		else {
			const jsonData = {eventObjectWrapper: {eventObjectList: eventObjectList}, globalNote: globalNote, sendEvent: true};
			const loader = new TCG.data.JsonLoader({
				waitTarget: gridPanel,
				waitMsg: 'Saving Journal...',
				jsonData: jsonData,
				success: function(response) {
					if (this.isUseWaitMsg()) {
						this.getMsgTarget().unmask();
					}
					const result = Ext.decode(response.responseText);
					TCG.createComponent('Clifton.core.StatusWindow', {
						width: 900,
						defaultData: {status: result.data},
						savedSinceOpen: true,
						openerCt: gridPanel
					});
					gridPanel.reload();
				}
			});
			//pre-pended with json/ because we are posting raw json to the server; not a form post.
			//TODO - change to be based on mime-type, not pre-pended url.
			loader.load('json/reconciliationEventObjectListProcess.json');
		}
	}
})
;


