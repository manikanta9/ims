Clifton.reconciliation.file.ReconciliationFileDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconciliation File Definition',
	iconCls: 'disk',
	width: 800,
	height: 500,

	items: [{
		xtype: 'formpanel',
		url: 'reconciliationFileDefinition.json?requestedMaxDepth=4',
		getSaveURL: function() {
			return 'reconciliationFileDefinitionPopulatedSave.json';
		},
		instructions: 'A Reconciliation File Definition specifies what reconciliation definition(s) should be used to process the file from a given source.',
		labelWidth: 135,
		items: [
			{fieldLabel: 'Reconciliation Source', name: 'source.name', hiddenName: 'source.id', xtype: 'combo', url: 'reconciliationSourceListFind.json', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationSourceWindow'},
			{fieldLabel: 'Definition Name', name: 'name', xtype: 'textfield'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'File Name', name: 'fileName', xtype: 'textfield'},
			{fieldLabel: 'Security File Name', name: 'securitiesFileName', xtype: 'textfield'},
			{fieldLabel: 'Save Orphaned Data', boxLabel: ' (rows not processed by any of the following reconciliation definition(s))', name: 'saveOrphanedData', xtype: 'checkbox'},
			{
				xtype: 'formgrid',
				title: 'Reconciliation Definition(s)',
				instructions: 'Reconciliation File Definition Mappings define what reconciliation definitions are used to process the file designated by the definition.',
				storeRoot: 'fileDefinitionMappingList',
				dtoClass: 'com.clifton.reconciliation.file.ReconciliationFileDefinitionMapping',
				height: 175,
				heightResized: true,
				columnsConfig: [
					{header: 'ID', width: 25, dataIndex: 'id', hidden: true},
					{header: 'Reconciliation Definition', width: 700, dataIndex: 'reconciliationDefinition.name', idDataIndex: 'reconciliationDefinition.id', editor: {xtype: 'combo', url: 'reconciliationDefinitionListFind.json', detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionWindow', loadAll: true, displayField: 'name', allowBlank: false}}
				]
			}
		]
	}]
});
