Clifton.reconciliation.setup.ReconciliationSetupWindow = Ext.extend(TCG.app.Window, {
	iconCls: 'reconcile',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Reconciliation Runs',
				reloadOnTabChange: true,
				items: [{
					xtype: 'reconciliation-run-grid',
					editor: {
						deleteConfirmMessage: 'Would you like to delete selected row?<br />This will delete all run objects and events (journal and account unlock history) associated with the run.',
						detailPageClass: 'Clifton.reconciliation.run.ReconciliationRunDetailWindowSelector',
						addEditButtons: function(toolbar) {
							const gridPanel = this.getGridPanel();
							const button = toolbar.add({
								text: 'Upload',
								tooltip: 'Manually upload reconciliation data for a definition, source, and date.',
								iconCls: 'run',
								scope: gridPanel,
								handler: function() {
									TCG.createComponent('Clifton.reconciliation.setup.ReconciliationDataUploadWindow', {
										defaultData: {
											openerCt: gridPanel
										}
									});
								}
							});
							toolbar.add('-');
							toolbar.add({
								text: 'Reconcile',
								tooltip: 'Reconcile the data for the run.',
								iconCls: 'reconcile',
								scope: gridPanel,
								handler: gridPanel.reconcileRun
							});
							toolbar.add('-');
							this.addToolbarDeleteButton(toolbar, gridPanel);

							TCG.file.enableDD(TCG.getParentByClass(button, Ext.Panel), null, gridPanel, 'reconciliationDataUpload.json?enableValidatingBinding=true', true);
						},
						getDefaultData: function(gridPanel, row) {
							return {
								reconciliationRun: row.json
							};
						}
					}
				}]
			},
			{
				title: 'Reconciliation Definitions',
				items: [{
					name: 'reconciliationDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'A reconciliation definition determines how an object\'s properties should be compared during reconciliation.  Any child definitions are also displayed.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition Type', dataIndex: 'type.name', width: 150, hidden: true, filter: {type: 'combo', searchFieldName: 'typeOrParentTypeId', url: 'reconciliationTypeListFind.json'}},
						{header: 'Definition Name', width: 200, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Definition Group', width: 100, dataIndex: 'definitionGroup.name', filter: {type: 'combo', searchFieldName: 'definitionGroupId', url: 'reconciliationDefinitionGroupListFind.json'}},
						{header: 'Workflow Status', width: 100, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Options Collateral Workflow'}, hidden: true},
						{header: 'Workflow State', width: 100, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Primary Source', dataIndex: 'primarySource.name', width: 100, filter: {type: 'combo', searchFieldName: 'primarySourceId', url: 'reconciliationSourceListFind.json'}},
						{header: 'Secondary Source', dataIndex: 'secondarySource.name', width: 100, filter: {type: 'combo', searchFieldName: 'secondarySourceId', url: 'reconciliationSourceListFind.json'}}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Reconciliation Definition Tags'}
						];
					},
					getLoadParams: function(firstLoad) {
						const gridPanel = this;
						const topToolbar = gridPanel.getTopToolbar();
						const params = {
							includeChildDefinition: true
						};
						const tag = TCG.getChildByName(topToolbar, 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.categoryName = 'Reconciliation Definition Tags';
							params.categoryHierarchyId = tag.getValue();
						}
						return gridPanel.getTypeFilter(firstLoad, params);
					},
					getTypeFilter(firstLoad, params) {
						const gridPanel = this;
						if (firstLoad) {
							return Clifton.reconciliation.getReconciliationTypeDataPromise(this.getWindow().typeName)
									.then(function(type) {
										gridPanel.setFilterValue('type.name', type.id);
									});
						}
						else {
							return params;
						}
					},
					editor: {
						ptype: 'workflow-transition-grid',
						tableName: 'ReconciliationDefinition',
						transitionWindowTitle: 'Transition Reconciliation Definition',
						transitionWindowInstructions: 'Select Transition for selected reconciliation definitions',
						detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionWindow',
						getDefaultData: function(gridPanel) { // defaults type/category for the detail page
							return {
								primarySource: TCG.data.getData('reconciliationSourceByName.json?name=IMS', gridPanel)
							};
						}
					}
				}]
			},


			{
				title: 'Orphaned Data',
				reloadOnTabChange: true,
				items: [{
					name: 'reconciliationRunOrphanObjectListFind',
					xtype: 'gridpanel',
					rowSelectionModel: 'multiple',
					instructions: 'Orphaned data represents rows of data not processed by any applicable reconciliation definitions during a data load.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Reconciliation Source', width: 100, dataIndex: 'fileDefinition.source.name', filter: {searchFieldName: 'sourceName'}},
						{header: 'File Definition', width: 200, dataIndex: 'fileDefinition.name', filter: {searchFieldName: 'fileDefinitionName'}},
						{header: 'File Name', width: 150, dataIndex: 'fileDefinition.fileName', filter: {searchFieldName: 'fileName'}},
						{header: 'Status', width: 250, dataIndex: 'status.statusTitle', filter: false, sortable: false},
						{
							header: 'Orphan Type', width: 80, dataIndex: 'orphanType',
							filter: {
								type: 'combo',
								dataIndex: 'orphanType',
								displayField: 'name',
								valueField: 'value',
								mode: 'local',
								store: {
									xtype: 'arraystore',
									fields: ['value', 'name', 'description'],
									data: Clifton.reconciliation.run.ReconciliationRunOrphanObjectTypes
								}
							}
						},
						{header: 'Exclusion Assignment', width: 200, dataIndex: 'exclusionAssignment.name', filter: {searchFieldName: 'exclusionAssignmentName'}},
						{header: 'Date', width: 80, dataIndex: 'reconciliationDate'}
					],
					editor: {
						allowToDeleteMultiple: true,
						addEditButtons: function(toolbar, gridPanel) {
							this.addToolbarDeleteButton(toolbar, gridPanel);
						},
						detailPageClass: 'Clifton.reconciliation.run.ReconciliationRunOrphanObjectWindow'
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('reconciliationDate', {'on': Clifton.calendar.getBusinessDayFrom(-1)});
						}
					}
				}]
			},


			{
				title: 'Reconciliation Data',
				items: [{
					name: 'reconciliationRunObjectListFind',
					xtype: 'gridpanel',
					instructions: 'Reconciliation data contains raw and normalized data based on the definition(s) used to process the data.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition Type', width: 70, dataIndex: 'reconciliationRun.reconciliationDefinition.type.name', hidden: true, filter: {type: 'combo', searchFieldName: 'typeId', url: 'reconciliationTypeListFind.json'}},
						{
							header: 'Definition Name', width: 100, dataIndex: 'reconciliationRun.reconciliationDefinition.name',
							filter: {type: 'combo', searchFieldName: 'definitionId', url: 'reconciliationDefinitionListFind.json'}
						},
						{header: 'Investment Account', width: 100, dataIndex: 'reconciliationInvestmentAccount.label'},
						{header: 'Security Symbol', width: 70, dataIndex: 'reconciliationInvestmentSecurity.securitySymbol'},
						{header: 'Security Name', width: 100, dataIndex: 'reconciliationInvestmentSecurity.securityName'},
						{header: 'Match Status', width: 50, dataIndex: 'reconciliationMatchStatus.label'},
						{
							header: 'Reconcile Status', width: 50, dataIndex: 'reconciliationReconcileStatus.label',
							renderer: function(value, metaData, r) {
								if (TCG.isEquals(value, 'Not Reconciled')) {
									return `<span style="color: red">${value}</span>`;
								}
								else if (TCG.isEquals(value, 'Manually Reconciled')) {
									return `<span style="color: blue">${value}</span>`;
								}
								else if (TCG.isEquals(value, 'Reconciled')) {
									return `<span style="color: green">${value}</span>`;
								}
								return value;
							}
						},
						{header: 'Run Date', width: 30, dataIndex: 'reconciliationRun.runDate', filter: {searchFieldName: 'runDate'}}
					],
					editor: {
						isAddEnabled: function() {
							return false;
						},
						detailPageClass: 'Clifton.reconciliation.run.ReconciliationRunObjectDetailWindow'
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Reconciliation Definition', xtype: 'toolbar-combo', name: 'reconciliationDefinitionId', width: 180, url: 'reconciliationDefinitionListFind.json', linkedFilter: 'reconciliationRun.reconciliationDefinition.name'},
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Reconciliation Definition Tags'}
						];
					},
					getLoadParams: function(firstLoad) {
						const gridPanel = this;
						if (firstLoad) {
							gridPanel.setFilterValue('reconciliationRun.runDate', {'on': Clifton.calendar.getBusinessDayFrom(-1)});
						}

						const params = {
							readUncommittedRequested: true
						};
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.categoryName = 'Reconciliation Definition Tags';
							params.categoryLinkFieldPath = 'reconciliationDefinition';
							params.categoryHierarchyId = tag.getValue();
						}
						return gridPanel.getTypeFilter(firstLoad, params);
					},
					getTypeFilter(firstLoad, params) {
						const gridPanel = this;
						if (firstLoad) {
							return Clifton.reconciliation.getReconciliationTypeDataPromise(this.getWindow().typeName)
									.then(function(type) {
										gridPanel.setFilterValue('reconciliationRun.reconciliationDefinition.type.name', type.id);
									});
						}
						else {
							return params;
						}
					}
				}]
			},


			Clifton.reconciliation.job.ReconciliationReconcileJobPanel
		]
	}
	]
})
;
