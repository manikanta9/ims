Clifton.reconciliation.setup.ReconciliationApprovalManagementWindow = Ext.extend(TCG.app.Window, {
	id: 'reconciliationApprovalManagementWindow',
	iconCls: 'verify',
	title: 'Reconciliation Approval Management',
	width: 1500,
	height: 700,

	items: [
		{
			xtype: 'tabpanel',
			items: [
				{
					title: 'Reconciliation Runs',
					items: [
						{
							xtype: 'gridpanel',
							name: 'reconciliationRunListFind',
							instructions: 'A list of unapproved reconciliation runs.  You must open a run and view to approve it.  All reconciled runs, except for cumulative runs, must be approved.',
							height: 300,
							rowSelectionModel: 'single',
							columns: [
								{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
								{header: 'Label', width: 150, dataIndex: 'label', hidden: true},
								{header: 'Definition', width: 150, dataIndex: 'reconciliationDefinition.name', defaultSortColumn: true, filter: {searchFieldName: 'definitionName'}},
								{header: 'Run Date', width: 50, dataIndex: 'runDate'},
								{
									header: 'Reconcile Status', width: 75, dataIndex: 'reconciliationReconcileStatus.label', tooltip: 'The status of this run',
									filter: {type: 'combo', searchFieldName: 'reconcileStatusId', url: 'reconciliationReconcileStatusListFind.json'},
									renderer: function(value, metaData, r) {
										if (TCG.isEquals(value, 'Not Reconciled')) {
											return `<span style="color: red">${value}</span>`;
										}
										else if (TCG.isEquals(value, 'Manually Reconciled')) {
											return `<span style="color: blue">${value}</span>`;
										}
										else if (TCG.isEquals(value, 'Reconciled')) {
											return `<span style="color: green">${value}</span>`;
										}
										return value;
									}
								},
								{header: 'Note', width: 100, dataIndex: 'note'},
								{header: 'Last Updated', width: 100, dataIndex: 'updateDate'},
								{header: 'Approved By', width: 100, dataIndex: 'approvedByUser.displayName', tooltip: 'The user that approved this run.', filter: {searchFieldName: 'approvedByUserName'}}
							],
							editor: {
								addEnabled: false,
								deleteEnabled: false,
								detailPageClass: 'Clifton.reconciliation.run.ReconciliationRunDetailWindowSelector',
								getDefaultData: function(gridPanel, row) {
									return {
										reconciliationRun: row.json
									};
								},
								getDetailPageParams: function(id) {
									return {'id': id, 'reconcileStatusName': 'MANUALLY_RECONCILED'};
								}
							},
							getTopToolbarFilters: function(toolbar) {
								return [
									{boxLabel: 'Include Approved', xtype: 'toolbar-checkbox', name: 'includeApproved'},
									{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Reconciliation Definition Tags'}
								];
							},
							getLoadParams: function(firstLoad) {
								const includeApproved = TCG.getChildByName(this.getTopToolbar(), 'includeApproved').getValue();
								const params = {cumulativeReconciliation: false};
								if (includeApproved !== true) {
									params['approvedByUserIdIsNull'] = true;
								}
								const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
								if (TCG.isNotBlank(tag.getValue())) {
									params.categoryName = 'Reconciliation Definition Tags';
									params.categoryLinkFieldPath = 'reconciliationDefinition';
									params.categoryHierarchyId = tag.getValue();
								}
								return params;
							},
							gridConfig: {
								listeners: {
									'rowclick': function(grid, rowIndex, evt) {
										const row = grid.store.data.items[rowIndex];
										const exclusion = row.json.id;
										const parentWindow = TCG.getParentByClass(grid, Ext.Window);
										const orphanGrid = TCG.getChildByName(parentWindow, 'reconciliationRunOrphanObjectListFind');
										orphanGrid.exclusionAssignmentId = exclusion;
										orphanGrid.reload();
									}
								}
							}
						}
					]
				},
				{
					title: 'Exclusion Assignments',
					items: [
						{xtype: 'reconciliation-exclusion-assignments-panel'}
					]
				}
			]
		}
	]
})
;
