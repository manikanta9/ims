Clifton.reconciliation.setup.ReconciliationProcessingWindow = Ext.extend(TCG.app.Window, {
	id: 'reconciliationProcessingWindow',
	iconCls: 'run',
	title: 'Reconciliation Processing',
	width: 1200,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		items: [
			Clifton.reconciliation.job.ReconciliationReconcileJobPanel,


			{
				title: 'Exclusion Processing',
				items: [{
					layout: 'vbox',
					layoutConfig: {align: 'stretch'},
					items: [{
						xtype: 'formpanel',
						instructions: 'Select a reconciliation source and Run Date to process exclusions for.',
						listeners: {
							destroy: function() {
								Ext.TaskMgr.stopAll();
							}
						},
						height: 150,
						labelWidth: 140,
						loadValidation: false, // using the form only to get background color/padding
						buttonAlign: 'right',
						items: [{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .49,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Reconciliation Source', name: 'sourceName', hiddenName: 'sourceId', xtype: 'combo', url: 'reconciliationSourceListFind.json', displayField: 'label'}
										]
									}]
								},
								{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
								{
									columnWidth: .49,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Run Date', name: 'runDate', xtype: 'datefield', allowBlank: false}
										]
									}]
								}
							]
						}],
						buttons: [{
							text: 'Process Exclusions(s)',
							iconCls: 'run',
							width: 140,
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								const sourceId = form.findField('sourceId').getValue();
								const runDate = form.findField('runDate').getValue();
								if (TCG.isBlank(sourceId)) {
									TCG.showError('Source selection is required.', 'Invalid Selection');
									return;
								}
								if (TCG.isBlank(runDate)) {
									TCG.showError('Run Date selection is required.', 'Invalid Selection');
									return;
								}
								Ext.Msg.confirm('Exclude?', 'Are you sure you want to process exclusions for the specified source and date?', function(a) {
									if (TCG.isEqualsStrict(a, 'yes')) {
										form.submit(Ext.applyIf({
											url: 'reconciliationExclusionAssignmentProcess.json',
											waitMsg: 'Excluding...',
											success: function(form, action) {
												Ext.Msg.alert('Processing Started', action.result.data.message, function() {
													const grid = owner.ownerCt.items.get(1);
													grid.reload.defer(300, grid);
												});
											}
										}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
									}
								});
							}
						}]
					}, {
						xtype: 'core-scheduled-runner-grid',
						typeName: 'RECONCILIATION-EXCLUSION',
						instantRunner: true,
						title: 'Executing Processes',
						width: 1000,
						flex: 1
					}]
				}]
			},


			{
				title: 'Account Alias Processing',
				items: [{
					layout: 'vbox',
					layoutConfig: {align: 'stretch'},
					items: [{
						xtype: 'formpanel',
						instructions: 'Select a reconciliation source and Run Date to process account aliases for.',
						listeners: {
							destroy: function() {
								Ext.TaskMgr.stopAll();
							}
						},
						height: 125,
						labelWidth: 140,
						loadValidation: false, // using the form only to get background color/padding
						buttonAlign: 'right',
						items: [{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .49,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Reconciliation Source', name: 'sourceName', hiddenName: 'sourceId', xtype: 'combo', url: 'reconciliationSourceListFind.json', displayField: 'label'}
										]
									}]
								},
								{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
								{
									columnWidth: .49,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Run Date', name: 'runDate', xtype: 'datefield', allowBlank: false}
										]
									}]
								}
							]
						}],
						buttons: [{
							text: 'Process Account Alias(es)',
							iconCls: 'run',
							width: 140,
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								const sourceId = form.findField('sourceId').getValue();
								const runDate = form.findField('runDate').getValue();
								if (TCG.isBlank(sourceId)) {
									TCG.showError('Source selection is required.', 'Invalid Selection');
									return;
								}
								if (TCG.isBlank(runDate)) {
									TCG.showError('Run Date selection is required.', 'Invalid Selection');
									return;
								}
								Ext.Msg.confirm('Process?', 'Are you sure you want to process account aliases for the specified source and date?', function(a) {
									if (TCG.isEqualsStrict(a, 'yes')) {
										form.submit(Ext.applyIf({
											url: 'reconciliationInvestmentAccountAliasProcess.json',
											waitMsg: 'Processing...',
											success: function(form, action) {
												Ext.Msg.alert('Processing Started', action.result.data.message, function() {
													const grid = owner.ownerCt.items.get(1);
													grid.reload.defer(300, grid);
												});
											}
										}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
									}
								});
							}
						}]
					}, {
						xtype: 'core-scheduled-runner-grid',
						typeName: 'RECONCILIATION-ACCOUNT-ALIASES',
						instantRunner: true,
						title: 'Executing Processes',
						width: 1000,
						flex: 1
					}]
				}]
			},


			{
				title: 'Security Alias Processing',
				items: [{
					layout: 'vbox',
					layoutConfig: {align: 'stretch'},
					items: [{
						xtype: 'formpanel',
						instructions: 'Select a reconciliation source and Run Date to process security aliases for.',
						listeners: {
							destroy: function() {
								Ext.TaskMgr.stopAll();
							}
						},
						height: 125,
						labelWidth: 140,
						loadValidation: false, // using the form only to get background color/padding
						buttonAlign: 'right',
						items: [{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .49,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Reconciliation Source', name: 'sourceName', hiddenName: 'sourceId', xtype: 'combo', url: 'reconciliationSourceListFind.json', displayField: 'label'}
										]
									}]
								},
								{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
								{
									columnWidth: .49,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Run Date', name: 'runDate', xtype: 'datefield', allowBlank: false}
										]
									}]
								}
							]
						}],
						buttons: [{
							text: 'Process Security Alias(es)',
							iconCls: 'run',
							width: 140,
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								const sourceId = form.findField('sourceId').getValue();
								const runDate = form.findField('runDate').getValue();
								if (TCG.isBlank(sourceId)) {
									TCG.showError('Source selection is required.', 'Invalid Selection');
									return;
								}
								if (TCG.isBlank(runDate)) {
									TCG.showError('Run Date selection is required.', 'Invalid Selection');
									return;
								}
								Ext.Msg.confirm('Process?', 'Are you sure you want to process security aliases for the specified source and date?', function(a) {
									if (TCG.isEqualsStrict(a, 'yes')) {
										form.submit(Ext.applyIf({
											url: 'reconciliationInvestmentSecurityAliasProcess.json',
											waitMsg: 'Processing...',
											success: function(form, action) {
												Ext.Msg.alert('Processing Started', action.result.data.message, function() {
													const grid = owner.ownerCt.items.get(1);
													grid.reload.defer(300, grid);
												});
											}
										}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
									}
								});
							}
						}]
					}, {
						xtype: 'core-scheduled-runner-grid',
						typeName: 'RECONCILIATION-SECURITY-ALIASES',
						instantRunner: true,
						title: 'Executing Processes',
						width: 1000,
						flex: 1
					}]
				}]
			},


			{
				title: 'Evaluator Processing',
				items: [{
					layout: 'vbox',
					layoutConfig: {align: 'stretch'},
					items: [{
						xtype: 'formpanel',
						instructions: 'Select a reconciliation definition or evaluator and Run Date to process evaluators for.',
						listeners: {
							destroy: function() {
								Ext.TaskMgr.stopAll();
							}
						},
						height: 150,
						labelWidth: 140,
						loadValidation: false, // using the form only to get background color/padding
						buttonAlign: 'right',
						items: [{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .49,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Reconciliation Definition', name: 'reconciliationDefinitionName', hiddenName: 'reconciliationDefinitionId', xtype: 'combo', url: 'reconciliationDefinitionListFind.json', displayField: 'label'},
											{fieldLabel: 'Evaluator', name: 'reconciliationEvaluatorName', hiddenName: 'reconciliationEvaluatorId', xtype: 'combo', url: 'reconciliationEvaluatorListFind.json', displayField: 'name'}
										]
									}]
								},
								{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
								{
									columnWidth: .49,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Run Date', name: 'runDate', xtype: 'datefield', allowBlank: false}
										]
									}]
								}
							]
						}],
						buttons: [{
							text: 'Process Evaluator(s)',
							iconCls: 'run',
							width: 140,
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								const definitionId = form.findField('reconciliationDefinitionId').getValue();
								const evaluatorId = form.findField('reconciliationEvaluatorId').getValue();
								const runDate = form.findField('runDate').getValue();
								if (TCG.isBlank(definitionId) && TCG.isBlank(evaluatorId)) {
									TCG.showError('Definition or Evaluator selection is required.', 'Invalid Selection');
									return;
								}
								if (!TCG.isBlank(definitionId) && !TCG.isBlank(evaluatorId)) {
									TCG.showError('Definition or Evaluator selection is required, but not both.', 'Invalid Selection');
									return;
								}
								if (TCG.isBlank(runDate)) {
									TCG.showError('Run Date selection is required.', 'Invalid Selection');
									return;
								}
								Ext.Msg.confirm('Process?', 'Are you sure you want to process evaluator(s) for the specified definition or evaluator and date?', function(a) {
									if (TCG.isEqualsStrict(a, 'yes')) {
										form.submit(Ext.applyIf({
											url: 'reconciliationEvaluatorProcess.json',
											waitMsg: 'Processing...',
											success: function(form, action) {
												Ext.Msg.alert('Processing Started', action.result.status.message, function() {
													const grid = owner.ownerCt.items.get(1);
													grid.reload.defer(300, grid);
												});
											}
										}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
									}
								});
							}
						}]
					}, {
						xtype: 'core-scheduled-runner-grid',
						typeName: 'RECONCILIATION-EVALUATORS',
						instantRunner: true,
						title: 'Executing Processes',
						width: 1000,
						flex: 1
					}]
				}]
			},


			{
				title: 'Event Processing',
				items: [{
					layout: 'vbox',
					layoutConfig: {align: 'stretch'},
					items: [{
						xtype: 'formpanel',
						instructions: 'Select an event type and Run Date to process events for.',
						listeners: {
							destroy: function() {
								Ext.TaskMgr.stopAll();
							}
						},
						height: 170,
						labelWidth: 140,
						loadValidation: false, // using the form only to get background color/padding
						buttonAlign: 'right',
						items: [{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .49,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{
												fieldLabel: 'Event Category', name: 'eventObjectCategory', hiddenName: 'eventObjectCategory', // need the hiddenName defined otherwise when the form is submitted, the displayField is submitted instead of the valueField
												xtype: 'combo', displayField: 'name', valueField: 'value', mode: 'local',
												store: {
													xtype: 'arraystore',
													fields: ['value', 'name', 'description'],
													data: Clifton.reconciliation.event.ReconciliationEventObjectCategories
												}
											},
											{fieldLabel: 'Definition Group', name: 'definitionGroupName', hiddenName: 'definitionGroupId', xtype: 'combo', url: 'reconciliationDefinitionGroupListFind.json', displayField: 'label'},
											{fieldLabel: 'Reconciliation Source', name: 'sourceName', hiddenName: 'sourceName', valueField: 'name', xtype: 'combo', url: 'reconciliationSourceListFind.json', displayField: 'label'}
										]
									}]
								},
								{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},
								{
									columnWidth: .49,
									items: [{
										xtype: 'formfragment',
										frame: false,
										labelWidth: 150,
										items: [
											{fieldLabel: 'Run Date', name: 'runDate', xtype: 'datefield', allowBlank: false},
											{
												fieldLabel: 'Send All Events', name: 'sendAllEvents', xtype: 'checkbox', allowBlank: false,
												qtip: 'Used to determine in some cases if all events should be sent or not; e.g., for account unlocking, by default only unlock events that have not already been sent are sent. In some cases, you may want to force sending all events again.'
											}
										]
									}]
								}
							]
						}],
						buttons: [{
							text: 'Process Event(s)',
							iconCls: 'run',
							width: 140,
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								const eventObjectCategory = form.findField('eventObjectCategory').getValue();
								const runDate = form.findField('runDate').getValue();
								if (TCG.isBlank(eventObjectCategory)) {
									TCG.showError('Event object type selection is required.', 'Invalid Selection');
									return;
								}
								if (TCG.isBlank(runDate)) {
									TCG.showError('Run Date selection is required.', 'Invalid Selection');
									return;
								}
								Ext.Msg.confirm('Process?', 'Are you sure you want to process events(s) for the specified type and date?', function(a) {
									if (TCG.isEqualsStrict(a, 'yes')) {
										form.submit(Ext.applyIf({
											url: 'reconciliationEventProcess.json',
											waitMsg: 'Processing...',
											success: function(form, action) {
												Ext.Msg.alert('Processing Started', action.result.status.message, function() {
													const grid = owner.ownerCt.items.get(1);
													grid.reload.defer(300, grid);
												});
											}
										}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
									}
								});
							}
						}]
					}, {
						xtype: 'core-scheduled-runner-grid',
						typeName: 'RECONCILIATION-EVENTS',
						instantRunner: true,
						title: 'Executing Processes',
						width: 1000,
						flex: 1
					}]
				}]
			}
		]
	}]
});
