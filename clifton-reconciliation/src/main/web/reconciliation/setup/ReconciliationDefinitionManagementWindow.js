Clifton.reconciliation.setup.ReconciliationDefinitionManagementWindow = Ext.extend(TCG.app.Window, {
	id: 'reconciliationDefinitionManagementWindow',
	iconCls: 'grid',
	title: 'Reconciliation Definition Setup',
	width: 1500,
	height: 700,
	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'File Definitions',
				items: [{
					name: 'reconciliationFileDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'A Reconciliation File Definition specifies what reconciliation definition(s) should be used to process the file from a given source.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Reconciliation Source', width: 70, dataIndex: 'source.name', filter: {type: 'combo', searchFieldName: 'sourceId', url: 'reconciliationSourceListFind.json', displayField: 'name'}},
						{header: 'Definition Name', width: 120, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'File Name', width: 100, dataIndex: 'fileName', filter: {searchFieldName: 'definitionFileName'}},
						{header: 'Save Orphans', width: 50, dataIndex: 'saveOrphanedData', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.file.ReconciliationFileDefinitionWindow'
					}
				}]
			},


			{
				title: 'Reconciliation Definitions',
				items: [{
					name: 'reconciliationDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'A reconciliation definition specifies the fields and the rules that are used to perform the reconciliation.',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'},
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Reconciliation Definition Tags'}
						];
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition Type', dataIndex: 'type.name', width: 100, hidden: true, filter: {type: 'combo', searchFieldName: 'typeId', url: 'reconciliationTypeListFind.json'}},
						{header: 'Definition Name', width: 150, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Definition Group', width: 100, dataIndex: 'definitionGroup.name', filter: {type: 'combo', searchFieldName: 'definitionGroupId', url: 'reconciliationDefinitionGroupListFind.json'}},
						{header: 'Workflow Status', width: 100, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json'}, hidden: true},
						{header: 'Workflow State', width: 100, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Primary Source', dataIndex: 'primarySource.name', width: 100, filter: {type: 'combo', searchFieldName: 'primarySourceId', url: 'reconciliationSourceListFind.json'}},
						{header: 'Secondary Source', dataIndex: 'secondarySource.name', width: 100, filter: {type: 'combo', searchFieldName: 'secondarySourceId', url: 'reconciliationSourceListFind.json'}}
					],
					getLoadParams: function(firstLoad) {
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							return {
								categoryName: 'Reconciliation Definition Tags',
								categoryHierarchyId: tag.getValue()
							};
						}
						return {};
					},
					editor: {
						detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionWindow'
					}
				}]
			},


			{
				title: 'Definition Groups',
				items: [{
					name: 'reconciliationDefinitionGroupListFind',
					xtype: 'gridpanel',
					instructions: 'Reconciliation Definition Groups can be used to classify definitions together for event dispatching.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Group ID', width: 50, dataIndex: 'id', type: 'int', hidden: true},
						{header: 'Group Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Run Approval Condition', width: 150, dataIndex: 'runApprovalSystemCondition.name'},
						{header: 'Run Modify Condition', width: 150, dataIndex: 'runModifySystemCondition.name'},
						{header: 'Event Trigger', width: 40, dataIndex: 'eventTrigger', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.definition.group.ReconciliationDefinitionGroupWindow'
					}
				}]
			},


			{
				title: 'Reconciliation Types',
				items: [{
					name: 'reconciliationTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Reconciliation Types are the highest level categorization of reconciliation runs.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Type ID', width: 50, dataIndex: 'id', type: 'int', hidden: true},
						{header: 'Type Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Run Approval Condition', width: 150, dataIndex: 'runApprovalSystemCondition.name'},
						{header: 'Run Modify Condition', width: 150, dataIndex: 'runModifySystemCondition.name'},
						{header: 'Cumulative', qtip: 'Specifies if this is a cumulative reconciliation - meaning only one run will be created and objects will always be appended to that run.', width: 40, dataIndex: 'cumulativeReconciliation', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.definition.type.ReconciliationTypeWindow'
					}
				}]
			},


			{
				title: 'Definition Fields',
				items: [{
					name: 'reconciliationDefinitionFieldListFind',
					xtype: 'gridpanel',
					instructions: 'A reconciliation definition field holds mappings to source fields and defines if fields are utilized as natural keys, for reconciliation, or both.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition Name', width: 200, dataIndex: 'reconciliationDefinition.name', defaultSortColumn: true, filter: {searchFieldName: 'definitionName'}},
						{header: 'Field Name', width: 100, dataIndex: 'field.name', filter: {searchFieldName: 'name'}},
						{header: 'Field Label', width: 150, dataIndex: 'field.label', filter: {searchFieldName: 'label'}},
						{
							header: 'Default Data Type', width: 250, dataIndex: 'field.defaultDataType', hidden: true,
							renderer: function(value, metaData, r) {
								if (value === 'DESCRIPTION') {
									return 'STRING';
								}
								else if (value === 'BIT') {
									return 'BOOLEAN';
								}
								return value;
							}
						},
						{
							header: 'Data Type', width: 150, dataIndex: 'dataType', tooltip: 'Data type of this definition field.',
							renderer: function(value, metaData, r) {
								const dataType = value ? value : r.json.field.defaultDataType;
								if (dataType === 'DESCRIPTION') {
									return 'STRING';
								}
								else if (dataType === 'BIT') {
									return 'BOOLEAN';
								}
								return dataType;
							}
						},
						{header: 'Natural Key', width: 50, dataIndex: 'naturalKey', type: 'boolean', tooltip: 'Specifies if this key definition is used as a natural key used in creation of the unique identifier for data.'},
						{header: 'Reconcilable', width: 50, dataIndex: 'reconcilable', type: 'boolean', tooltip: 'Specifies if this key definition specifies fields that should be reconciled when processing data.'},
						{header: 'Adjustable', width: 50, dataIndex: 'adjustable', type: 'boolean', tooltip: 'Specifies if this key definition can be used to create adjustments.'},
						{header: 'Aggregate', width: 50, dataIndex: 'aggregate', type: 'boolean', tooltip: 'Specifies if this key definition should be aggregated/summed during merging of two or more rows of data for the same natural key during import.'},
						{header: 'Hidden', width: 50, dataIndex: 'hidden', type: 'boolean', tooltip: 'Specifies if this key definition should be displayed in detail windows.'},
						{header: 'Start Date', width: 50, dataIndex: 'startDate'},
						{header: 'End Date', width: 50, dataIndex: 'endDate'},
						{header: 'Order', width: 50, dataIndex: 'displayOrder', type: 'int', tooltip: 'Display Order'}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.definition.ReconciliationDefinitionFieldWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Reconciliation Fields',
				items: [{
					name: 'reconciliationFieldListFind',
					xtype: 'gridpanel',
					instructions: 'A reconciliation field defines name, code, and UI config options for reusable fields.  The fields are referenced by a definition field which contains the actual field mappings used to naturalize data from two sources into this fieldName',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Field Name', width: 100, dataIndex: 'name'},
						{header: 'Field Label', width: 100, dataIndex: 'label'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{
							header: 'Data Type', width: 50, dataIndex: 'defaultDataType', filter: false,
							renderer: function(value, metaData, r) {
								if (value === 'DESCRIPTION') {
									return 'STRING';
								}
								else if (value === 'BIT') {
									return 'BOOLEAN';
								}
								return value;
							}
						},
						{header: 'Display Width', width: 50, dataIndex: 'defaultDisplayWidth', type: 'int'}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.definition.ReconciliationFieldWindow'
					}
				}]
			},


			{
				title: 'Reconciliation Filters',
				items: [{
					name: 'reconciliationFilterListFind',
					xtype: 'gridpanel',
					instructions: 'A reconciliation filter defines how reconciliation data should be filtered (included/excluded) when processing.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition Name', width: 180, dataIndex: 'reconciliationDefinition.name', defaultSortColumn: true, filter: {searchFieldName: 'definitionName'}},
						{header: 'Filter Name', width: 180, dataIndex: 'name'},
						{header: 'Reconciliation Source', width: 90, dataIndex: 'reconciliationSource.name', tooltip: 'The source this filter applies to.'},
						{header: 'Condition', width: 200, dataIndex: 'conditionRule.name', tooltip: 'Condition that must be met for this field mapping to apply.'},
						{header: 'Include', width: 50, dataIndex: 'include', type: 'boolean', tooltip: 'Defines if this filter should be used to include or exclude an item that meets the specified condition.'},
						{header: 'Save Orphaned', width: 60, dataIndex: 'doNotSaveFilteredData', type: 'boolean', tooltip: 'Defines if the data filtered by this row should be saved as orphaned data if not used by any other definition during processing.  If a filter is used to exclude positions that are not yet applicable (e.g. no maturity date), this can be used so that items that will be filtered daily will not be saved an need to be reviewed daily due to being orphaned.  This should be used only when absolutely sure a filter will not incorrectly exclude an item.'},
						{header: 'Start Date', width: 50, dataIndex: 'startDate', tooltip: 'Specifies the start date of this filter.'},
						{header: 'End Date', width: 50, dataIndex: 'endDate', tooltip: 'Specifies the end date of this filter.'}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.definition.rule.ReconciliationFilterWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Exclusion Assignments',
				items: [{
					includeApproved: true,
					xtype: 'reconciliation-exclusion-assignments-panel'
				}]
			},


			{
				title: 'Reconciliation Evaluators',
				items: [{
					name: 'reconciliationEvaluatorListFind',
					xtype: 'gridpanel',
					instructions: 'One or more evaluators can be used for a field to specify if/how data from both sources should be reconciled. They can be used to specify reconciliation thresholds, gap quantities, etc. Evaluators are applied using the specified Order and can use filters (Holding Account, Investment Security, Custom Condition). If no evaluator is specified for a Reconcilable Field, exact match is required in order to reconcile.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition Name', width: 250, dataIndex: 'definitionField.reconciliationDefinition.name', defaultSortColumn: true, filter: {searchFieldName: 'definitionName'}},
						{header: 'Definition Field', width: 200, dataIndex: 'definitionField.field.name', filter: {searchFieldName: 'definitionFieldName'}},
						{header: 'Investment Account', width: 200, dataIndex: 'reconciliationInvestmentAccount.label', filter: {searchFieldName: 'accountNumber'}},
						{header: 'Investment Security', width: 150, dataIndex: 'reconciliationInvestmentSecurity.securitySymbol', filter: {searchFieldName: 'securitySymbol'}},
						{header: 'Custom Scope Condition', width: 150, dataIndex: 'conditionRule.name', tooltip: 'Condition that must be met for this field mapping to apply.', filter: {searchFieldName: 'conditionRuleName'}},
						{header: 'Evaluator Name', width: 200, dataIndex: 'name', hidden: true},
						{header: 'Evaluator Bean', width: 200, dataIndex: 'evaluatorBean.name', tooltip: 'Comparator that is used for reconciliation.', filter: {searchFieldName: 'evaluatorBeanName'}},
						{header: 'Schedule', width: 100, dataIndex: 'calendarSchedule.name', tooltip: 'Defines the schedule for this evaluator, e.g., only run the last business day of the month.', hidden: true},
						{header: 'Exclusive', width: 50, dataIndex: 'exclusive', type: 'boolean', tooltip: 'Runs the evaluator on all days EXCEPT those defined by the schedule.', hidden: true},
						{header: 'Start Date', width: 75, dataIndex: 'startDate', tooltip: 'Specifies the start date of this filter.'},
						{header: 'End Date', width: 75, dataIndex: 'endDate', tooltip: 'Specifies the end date of this filter.'},
						{header: 'Order', width: 50, dataIndex: 'evaluationOrder', type: 'int', tooltip: 'Specifies the order in which the evaluator is processed.'}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.definition.rule.ReconciliationEvaluatorWindow',
						copyURL: 'reconciliationEvaluatorCopy.json',
						copyIdParameter: 'evaluatorId',
						scope: this,
						copyHandler: function(gridPanel) {
							const sm = gridPanel.grid.getSelectionModel();
							if (sm.getCount() !== 1) {
								TCG.showError('You must select at least one and only one row to create a gap quantity rule for.', 'No Row(s) Selected.');
							}
							else {
								const data = sm.getSelections()[0].json;
								Promise.all([
									TCG.data.getDataPromise('reconciliationDefinitionByName.json', gridPanel, {params: {name: data.definitionField.reconciliationDefinition.name}}),
									TCG.data.getDataPromise('systemBeanByName.json', gridPanel, {params: {name: data.evaluatorBean.name}})
								]).then(function([definition, bean]) {

									let name = null;
									let accountNumber = null;
									let symbol = null;
									const beanName = bean.type.name;

									if (TCG.isNotBlank(data.definitionField)) {
										name = data.definitionField.field.name;
									}
									if (TCG.isNotBlank(data.reconciliationInvestmentAccount)) {
										accountNumber = data.reconciliationInvestmentAccount.label.split(':')[0];
									}
									if (TCG.isNotBlank(data.reconciliationInvestmentSecurity)) {
										symbol = data.reconciliationInvestmentSecurity.securitySymbol;
									}

									Promise.all([
										TCG.data.getDataPromise('reconciliationDefinitionFieldByNameAndDefinitionId.json', gridPanel, {params: {name: name, definitionId: definition.id, activeOnDate: TCG.parseDate(Clifton.calendar.getBusinessDayFrom(-1)).format('m/d/Y')}}),
										TCG.data.getDataPromise('reconciliationInvestmentAccountByNumber.json', gridPanel, {params: {accountNumber: accountNumber}}),
										TCG.data.getDataPromise('reconciliationInvestmentSecurityBySymbol.json', gridPanel, {params: {symbol: symbol}}),
										TCG.data.getDataPromise('systemBeanTypeByName.json', gridPanel, {params: {name: beanName}})
									]).then(function([definitionField, investmentAccount, investmentSecurity, beanType]) {
										TCG.createComponent('Clifton.reconciliation.definition.rule.ReconciliationEvaluatorGapQuantityPreviewWindow', {
											defaultData: {
												definitionField: definitionField,
												reconciliationInvestmentAccount: investmentAccount,
												reconciliationInvestmentSecurity: investmentSecurity,
												evaluatorBean: {type: beanType},
												startDate: Clifton.calendar.getBusinessDayFrom(-1).format('Y-m-d 00:00:00'),
												id: data.id
											},
											saveUrl: 'reconciliationEvaluatorCopy.json?enableValidatingBinding=true'
										});
									});
								});
							}
						}
					}
				}]
			},

			{
				title: 'Reconciliation Sources',
				items: [{
					name: 'reconciliationSourceListFind',
					xtype: 'gridpanel',
					instructions: 'A reconciliation source defines the import source of reconciliation data.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Source Name', width: 70, dataIndex: 'name', filter: false},
						{header: 'Short Name', width: 50, dataIndex: 'shortName', filter: false},
						{header: 'Description', width: 200, dataIndex: 'description', filter: false}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.definition.ReconciliationSourceWindow'
					}
				}]
			},

			{
				title: 'Reconcile Statuses',
				items: [{
					name: 'reconciliationReconcileStatusListFind',
					xtype: 'gridpanel',
					instructions: 'A reconcile status defines the processing status of reconciliation data.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Status Name', width: 70, dataIndex: 'name', filter: false},
						{header: 'Status Label', width: 70, dataIndex: 'label', filter: false},
						{header: 'Description', width: 200, dataIndex: 'description', filter: false},
						{header: 'Order', width: 50, dataIndex: 'sortOrder', type: 'int', filter: false},
						{header: 'Reconciled', width: 50, dataIndex: 'reconciled', type: 'boolean', filter: false},
						{header: 'Manual', width: 50, dataIndex: 'manual', type: 'boolean', filter: false}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.run.status.ReconciliationReconcileStatusWindow'
					}
				}]
			},


			{
				title: 'Match Statuses',
				items: [{
					name: 'reconciliationMatchStatusListFind',
					xtype: 'gridpanel',
					instructions: 'A match status defines the processing status of matched data.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Status Name', width: 70, dataIndex: 'name', filter: false},
						{header: 'Status Label', width: 70, dataIndex: 'label', filter: false},
						{header: 'Description', width: 150, dataIndex: 'description', filter: false},
						{header: 'Order', width: 50, dataIndex: 'sortOrder', type: 'int', filter: false},
						{header: 'Matched', width: 50, dataIndex: 'matched', type: 'boolean', filter: false},
						{header: 'Manual', width: 50, dataIndex: 'manual', type: 'boolean', filter: false}
					],
					editor: {
						detailPageClass: 'Clifton.reconciliation.run.status.ReconciliationMatchStatusWindow'
					}
				}]
			}
		]
	}]
});
