package com.clifton.reconciliation;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ReconciliationProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "reconciliation";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("java.sql.ResultSet");
		imports.add("java.sql.SQLException");
		imports.add("org.springframework.dao.DataAccessException");
		imports.add("org.springframework.jdbc.core");
		imports.add("org.apache.commons.lang.mutable.MutableBoolean");
		imports.add("org.apache.commons.csv.CSVFormat");
		imports.add("org.apache.commons.csv.CSVParser");
		imports.add("org.apache.commons.csv.CSVRecord");
		imports.add("org.apache.poi.ss.usermodel.Cell");
		imports.add("org.apache.poi.ss.usermodel.CellType");
		imports.add("org.apache.poi.ss.usermodel.Row");
		imports.add("org.apache.poi.ss.usermodel.Sheet");
		imports.add("org.apache.poi.ss.usermodel.Workbook");
		imports.add("org.hibernate.HibernateException");
		imports.add("org.hibernate.SessionFactory");
		imports.add("org.hibernate.exception.ConstraintViolationException");
		imports.add("javax.persistence.PersistenceException");
		imports.add("java.sql.Types");
		imports.add("java.beans.PropertyDescriptor");
		imports.add("javax.annotation.Resource");
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("category");
		approvedList.add("comparator");
		approvedList.add("object");
		approvedList.add("state");
		approvedList.add("simpletable");
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("org.springframework.transaction.PlatformTransactionManager");
		imports.add("org.springframework.transaction.TransactionDefinition");
		imports.add("org.springframework.transaction.support.TransactionTemplate");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		if ("saveReconciliationFilter".equals(method.getName())) {
			return true;
		}
		return false;
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> basicProjectAllowedSuffixNamesList = super.getAllowedContextManagedBeanSuffixNames();
		basicProjectAllowedSuffixNamesList.add("Strategy");
		return basicProjectAllowedSuffixNamesList;
	}
}
