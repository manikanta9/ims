package com.clifton.reconciliation.upload;

import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.file.ReconciliationFileCommand;
import com.clifton.reconciliation.file.ReconciliationFileDefinition;
import com.clifton.reconciliation.file.search.ReconciliationFileDefinitionSearchForm;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@ContextConfiguration(locations = "classpath:com/clifton/reconciliation/upload/ReconciliationTests-context.xml")
@Transactional
public class ReconciliationGoldmanSachsPositionTests extends ReconciliationTests {

	private ReconciliationFileDefinition reconciliationFileDefinition;


	@BeforeEach
	public void setup() {
		ReconciliationFileDefinitionSearchForm searchForm = new ReconciliationFileDefinitionSearchForm();
		searchForm.setName("Goldman Sachs Swaps Position Reconciliation");
		Optional<ReconciliationFileDefinition> optional = this.reconciliationFileDefinitionService.getReconciliationFileDefinitionList(searchForm).stream().findFirst();
		Assertions.assertTrue(optional.isPresent());
		this.reconciliationFileDefinition = optional.get();
	}


	@Test
	public void reconciliationPositionUploadTests() {
		File positionInputFile = new File(getClass().getResource("data/GS-CLEARED_GoldmanSachs_12-22-17.csv").getFile());

		ReconciliationFileCommand reconciliationFileCommand = new ReconciliationFileCommand();
		reconciliationFileCommand.setAutomatedUpload(false);
		reconciliationFileCommand.setImportDate(new Date());
		reconciliationFileCommand.setFileDefinitionId(this.reconciliationFileDefinition.getId());
		reconciliationFileCommand.setFileRetrievalFunction(i -> positionInputFile);

		Status status = this.reconciliationService.uploadReconciliationFile(reconciliationFileCommand);
		Assertions.assertNotNull(status);
		MatcherAssert.assertThat(status.getMessage(), IsEqual.equalTo("Processed 70 row(s); Errors: 0; Filtered: 0"));

		ReconciliationRunSearchForm reconciliationRunSearchForm = new ReconciliationRunSearchForm();
		reconciliationFileCommand.setFileDefinitionId(this.reconciliationFileDefinition.getId());
		List<ReconciliationRun> runList = getReconciliationRunService().getReconciliationRunList(reconciliationRunSearchForm);
		Assertions.assertEquals(2, runList.size());

		List<ReconciliationRunObject> reconciliationRunObjectList = getReconciliationRunService().getReconciliationRunObjectListByRunId(runList
				.stream()
				.filter(r -> Objects.equals("Goldman Sachs Cleared IRS Position Reconciliation", r.getReconciliationDefinition().getName()))
				.findFirst()
				.map(ReconciliationRun::getId)
				.orElseThrow(() -> new RuntimeException("Cannot find definition " + "Goldman Sachs Cleared IRS Position Reconciliation")));
		Assertions.assertEquals(3, reconciliationRunObjectList.size());

		reconciliationRunObjectList = getReconciliationRunService().getReconciliationRunObjectListByRunId(runList
				.stream()
				.filter(r -> Objects.equals("Goldman Sachs Cleared OTC Position Reconciliation", r.getReconciliationDefinition().getName()))
				.findFirst()
				.map(ReconciliationRun::getId)
				.orElseThrow(() -> new RuntimeException("Cannot find definition " + "Goldman Sachs Cleared OTC Position Reconciliation")));
		Assertions.assertEquals(67, reconciliationRunObjectList.size());
	}
}
