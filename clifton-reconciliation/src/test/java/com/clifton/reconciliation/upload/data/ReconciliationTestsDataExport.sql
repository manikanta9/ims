SELECT 'ReconciliationReconcileStatus' AS entityTableName, ReconciliationReconcileStatusID AS entityId FROM ReconciliationReconcileStatus
UNION
SELECT 'ReconciliationMatchStatus' AS entityTableName, ReconciliationMatchStatusID AS entityId FROM ReconciliationMatchStatus
UNION
SELECT 'ReconciliationSource' AS entityTableName, ReconciliationSourceID AS entityId FROM ReconciliationSource
UNION
SELECT 'ReconciliationField' AS entityTableName, ReconciliationFieldID as entityId FROM ReconciliationField
UNION
SELECT 'ReconciliationDefinitionPreprocessor' AS entityTableName, ReconciliationDefinitionPreprocessorID as entityId FROM ReconciliationDefinitionPreprocessor
UNION
SELECT 'ReconciliationEvaluator' AS entityTableName, ReconciliationEvaluatorID as entityId FROM ReconciliationEvaluator
UNION
SELECT 'ReconciliationDefinitionFieldMapping' AS entityTableName, ReconciliationDefinitionFieldMappingID as entityId FROM ReconciliationDefinitionFieldMapping
UNION
SELECT 'ReconciliationFileDefinitionMapping' AS entityTableName, ReconciliationFileDefinitionMappingID as entityId FROM ReconciliationFileDefinitionMapping WHERE ReconciliationFileDefinitionID IN (SELECT ReconciliationFileDefinitionID FROM ReconciliationFileDefinition WHERE DefinitionName IN ('TDAmeritrade Position Reconciliation', 'Goldman Sachs Swaps Position Reconciliation'))
UNION
SELECT 'ReconciliationDefinitionField' AS entityTableName, ReconciliationDefinitionFieldID as entityId FROM ReconciliationDefinitionField WHERE ReconciliationDefinitionID IN (SELECT ReconciliationDefinitionID FROM ReconciliationDefinition WHERE DefinitionName IN ('TDAmeritrade Position Reconciliation', 'Goldman Sachs Cleared OTC Position Reconciliation', 'Goldman Sachs Cleared IRS Position Reconciliation'))
UNION
SELECT 'ReconciliationFilter' AS entityTableName, ReconciliationFilterID as entityId FROM ReconciliationFilter WHERE ReconciliationDefinitionID IN (SELECT ReconciliationDefinitionID FROM ReconciliationDefinition WHERE DefinitionName IN ('TDAmeritrade Position Reconciliation', 'Goldman Sachs Cleared OTC Position Reconciliation', 'Goldman Sachs Cleared IRS Position Reconciliation'));
