package com.clifton.reconciliation.upload;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.TempFilePath;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.file.ReconciliationFileCommand;
import com.clifton.reconciliation.file.ReconciliationFileDefinition;
import com.clifton.reconciliation.file.search.ReconciliationFileDefinitionSearchForm;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@ContextConfiguration(locations = "classpath:com/clifton/reconciliation/upload/ReconciliationTests-context.xml")
@Transactional
public class ReconciliationAmeritradePositionTests extends ReconciliationTests {

	private ReconciliationFileDefinition reconciliationFileDefinition;


	@BeforeEach
	public void setup() {
		ReconciliationFileDefinitionSearchForm searchForm = new ReconciliationFileDefinitionSearchForm();
		searchForm.setName("TDAmeritrade Position Reconciliation");
		Optional<ReconciliationFileDefinition> optional = this.reconciliationFileDefinitionService.getReconciliationFileDefinitionList(searchForm).stream().findFirst();
		Assertions.assertTrue(optional.isPresent());
		this.reconciliationFileDefinition = optional.get();
	}


	@Test
	public void reconciliationPositionUploadTests() throws IOException {
		// Copy input file so that the original file is not deleted in case test retries are necessary
		TempFilePath tempDirectory = TempFilePath.createTempDirectory(getClass().getSimpleName());
		String fileName = "data/MC-20181115-PSN-0001__2018-11-16_03_58_05.csv";
		File origPositionInputFile = new File(getClass().getResource(fileName).getFile());
		Path positionInputPath = tempDirectory.getTempFile().toPath().resolve(fileName);
		Files.createDirectories(positionInputPath.getParent());
		File positionInputFile = positionInputPath.toFile();
		FileUtils.copyFile(origPositionInputFile, positionInputFile);

		ReconciliationFileCommand reconciliationFileCommand = new ReconciliationFileCommand();
		reconciliationFileCommand.setAutomatedUpload(false);
		reconciliationFileCommand.setImportDate(new Date());
		reconciliationFileCommand.setFileDefinitionId(this.reconciliationFileDefinition.getId());
		reconciliationFileCommand.setFileRetrievalFunction(i -> positionInputFile);

		Status status = this.reconciliationService.uploadReconciliationFile(reconciliationFileCommand);
		Assertions.assertNotNull(status);
		MatcherAssert.assertThat(status.getMessage(), IsEqual.equalTo("Processed 5539 row(s); Errors: 0; Filtered: 0"));

		ReconciliationRunSearchForm reconciliationRunSearchForm = new ReconciliationRunSearchForm();
		reconciliationFileCommand.setFileDefinitionId(this.reconciliationFileDefinition.getId());
		List<ReconciliationRun> runList = getReconciliationRunService().getReconciliationRunList(reconciliationRunSearchForm);
		Assertions.assertEquals(2, runList.size());

		List<ReconciliationRunObject> reconciliationRunObjectList = getReconciliationRunService().getReconciliationRunObjectListByRunId(runList
				.stream()
				.filter(r -> Objects.equals("TD Ameritrade Position Reconciliation", r.getReconciliationDefinition().getName()))
				.findFirst()
				.map(ReconciliationRun::getId)
				.orElseThrow(() -> new RuntimeException("Cannot find definition " + "TD Ameritrade Position Reconciliation")));
		Assertions.assertEquals(5539, reconciliationRunObjectList.size());

		reconciliationRunObjectList = getReconciliationRunService().getReconciliationRunObjectListByRunId(runList
				.stream()
				.filter(r -> Objects.equals("TDAmeritrade Cash Reconciliation", r.getReconciliationDefinition().getName()))
				.findFirst()
				.map(ReconciliationRun::getId)
				.orElseThrow(() -> new RuntimeException("Cannot find definition " + "TDAmeritrade Cash Reconciliation")));
		Assertions.assertEquals(341, reconciliationRunObjectList.size());
	}
}
