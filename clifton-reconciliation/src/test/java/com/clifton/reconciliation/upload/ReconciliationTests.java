package com.clifton.reconciliation.upload;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.reconciliation.ReconciliationService;
import com.clifton.reconciliation.file.ReconciliationFileDefinitionService;
import com.clifton.reconciliation.run.ReconciliationRunService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * @author TerryS
 */
@Transactional
public abstract class ReconciliationTests extends BaseInMemoryDatabaseTests {

	@Resource
	public ReconciliationFileDefinitionService reconciliationFileDefinitionService;

	@Resource
	public ReconciliationService reconciliationService;

	@Resource
	private ReconciliationRunService reconciliationRunService;


	@Override
	protected Class<?> getTestClass() {
		return ReconciliationTests.class;
	}


	public ReconciliationFileDefinitionService getReconciliationFileDefinitionService() {
		return this.reconciliationFileDefinitionService;
	}


	public ReconciliationService getReconciliationService() {
		return this.reconciliationService;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}
}
