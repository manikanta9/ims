package com.clifton.reconciliation.definition.evaluator;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.evaluator.data.ReconciliationDataDefaultEvaluator;
import com.clifton.reconciliation.definition.evaluator.data.ReconciliationDataGapQuantityEvaluator;
import com.clifton.reconciliation.definition.evaluator.data.ReconciliationDataThresholdEvaluator;
import com.clifton.reconciliation.run.data.DataError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * @author stevenf
 */
public class ReconciliationEvaluatorTests {


	@Test
	public void testDataThresholdEvaluator() {
		ReconciliationDataThresholdEvaluator evaluator = new ReconciliationDataThresholdEvaluator();
		evaluator.setLowerThreshold(new BigDecimal("-500"));
		evaluator.setUpperThreshold(new BigDecimal("500"));
		Assertions.assertNull(evaluator.evaluateData(15000, 15500), "No error should have been returned!");

		DataError error = evaluator.evaluateData(15000, 16000);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("-1000"), error.getDiff()));

		evaluator.setLowerThreshold(new BigDecimal("-100"));
		evaluator.setUpperThreshold(new BigDecimal("100"));
		error = evaluator.evaluateData(-1540.92, 1453.08);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("-2994"), error.getDiff()));

		//Now use invalid thresholds and expect an exception
		evaluator.setLowerThreshold(new BigDecimal("100"));
		evaluator.setUpperThreshold(new BigDecimal("50"));
		final ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			evaluator.evaluateData(1000, 1100);
		});
		Assertions.assertEquals("Lower Threshold value of '100' must be lower than Upper Threshold value of '50'!", validationException.getMessage());
	}


	@Test
	public void testPercentageDataThresholdEvaluator() {
		ReconciliationDataThresholdEvaluator evaluator = new ReconciliationDataThresholdEvaluator();
		evaluator.setPercentage(true);
		evaluator.setLowerThreshold(new BigDecimal("-.10"));
		evaluator.setUpperThreshold(new BigDecimal(".10"));

		Assertions.assertNull(evaluator.evaluateData(15000, 16500), "No error should have been returned!");
		Assertions.assertNull(evaluator.evaluateData(-1000, -900), "No error should have been returned!");
		Assertions.assertNull(evaluator.evaluateData(16500, 15000), "No error should have been returned!");
		Assertions.assertNull(evaluator.evaluateData(17000, 16000), "No error should have been returned!");

		DataError error = evaluator.evaluateData(15000, 17000);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("-2000"), error.getDiff()));

		error = evaluator.evaluateData(17000, 15000);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("2000"), error.getDiff()));

		error = evaluator.evaluateData(-15000, -17000);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("2000"), error.getDiff()));

		error = evaluator.evaluateData(-17000, -15000);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("-2000"), error.getDiff()));

		//Now use positive percentages and validate the same results
		evaluator.setLowerThreshold(new BigDecimal(".10"));
		evaluator.setUpperThreshold(new BigDecimal(".10"));

		Assertions.assertNull(evaluator.evaluateData(15000, 16500), "No error should have been returned!");
		Assertions.assertNull(evaluator.evaluateData(-1000, -900), "No error should have been returned!");
		Assertions.assertNull(evaluator.evaluateData(16500, 15000), "No error should have been returned!");
		Assertions.assertNull(evaluator.evaluateData(17000, 16000), "No error should have been returned!");

		error = evaluator.evaluateData(15000, 17000);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("-2000"), error.getDiff()));

		error = evaluator.evaluateData(17000, 15000);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("2000"), error.getDiff()));

		error = evaluator.evaluateData(-15000, -17000);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("2000"), error.getDiff()));

		error = evaluator.evaluateData(-17000, -15000);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("-2000"), error.getDiff()));

		//Now use positive percentages and validate the same results
		evaluator.setLowerThreshold(new BigDecimal("-.05"));
		evaluator.setUpperThreshold(new BigDecimal("-.10"));
		Assertions.assertNull(evaluator.evaluateData(1000, 1050), "No error should have been returned!");
		Assertions.assertNull(evaluator.evaluateData(1000, 950), "No error should have been returned!");
		Assertions.assertNull(evaluator.evaluateData(-1000, -1050), "No error should have been returned!");
		Assertions.assertNull(evaluator.evaluateData(-1000, -950), "No error should have been returned!");

		error = evaluator.evaluateData(1000, 1100);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("-100"), error.getDiff()));

		//Now use seemingly invalid thresholds - should handle properly
		evaluator.setLowerThreshold(new BigDecimal("-.10"));
		evaluator.setUpperThreshold(new BigDecimal("-.05"));
		Assertions.assertNull(evaluator.evaluateData(1000, 1100), "No error should have been returned!");
	}


	@Test
	public void testGapQuantityEvaluator() {
		ReconciliationDataGapQuantityEvaluator evaluator = new ReconciliationDataGapQuantityEvaluator();
		evaluator.setQuantity(new BigDecimal("500"));
		Assertions.assertNull(evaluator.evaluateData(1500, 1000), "No error should have been returned!");
		Assertions.assertNull(evaluator.evaluateData(0, -500), "No error should have been returned!");

		DataError error = evaluator.evaluateData(1000, 1500);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("-500"), error.getDiff()));

		error = evaluator.evaluateData(-2000, -500);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("-1500"), error.getDiff()));

		error = evaluator.evaluateData(-500, -2000);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("1500"), error.getDiff()));

		error = evaluator.evaluateData(-500, 1);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("-501"), error.getDiff()));

		error = evaluator.evaluateData(-500, -1);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("-499"), error.getDiff()));

		error = evaluator.evaluateData(-500, 500);
		Assertions.assertNotNull(error, "Expected a data error!");
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("-1000"), error.getDiff()));
	}


	@Test
	public void testDefaultEvaluator() {
		ReconciliationDataDefaultEvaluator evaluator = new ReconciliationDataDefaultEvaluator();
		Assertions.assertNull(evaluator.evaluateData(1000, 1000), "No error should have been returned!");
		DataError error = evaluator.evaluateData(1000, 1001);
		Assertions.assertNotNull(error, "Expected a data error!");
		error = evaluator.evaluateData("A", "B");
		Assertions.assertNotNull(error, "Expected a data error!");
	}
}
