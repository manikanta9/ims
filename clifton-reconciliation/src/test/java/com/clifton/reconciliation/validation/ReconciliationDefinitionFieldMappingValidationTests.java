package com.clifton.reconciliation.validation;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.ReconciliationDefinitionFieldMapping;
import com.clifton.reconciliation.definition.ReconciliationDefinitionFieldMappingBuilder;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.rule.ReconciliationRuleCondition;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunBuilder;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@ContextConfiguration(locations = "classpath:com/clifton/reconciliation/validation/BaseReconciliationValidationTests-context.xml")
public class ReconciliationDefinitionFieldMappingValidationTests extends BaseReconciliationValidationTests {

	////////////////////////////////////////////////////////////////////////////
	//////// Reconciliation Definition Field Mapping Validation Tests  /////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void reconciliationFieldMappingNoFieldValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinitionFieldMapping mapping = ReconciliationDefinitionFieldMappingBuilder.aReconciliationDefinitionFieldMapping()
						.withDefinitionField(null)
						.withSource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
						.withName("Holding Account")
						.withEvaluationOrder(10)
						.build();
				this.reconciliationDefinitionService.saveReconciliationDefinitionFieldMapping(mapping);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("Definition field may not be null."));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationFieldMappingWithRunsValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinitionField field = createHoldingAccountDefinitionField();

				ReconciliationRun reconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
						.withRunDate(DateUtils.toDate("12/31/2018"))
						.withReconciliationDefinition(field.getReconciliationDefinition())
						.withReconciliationReconcileStatus(this.reconciliationStatusService.getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.RECONCILED.name()))
						.withRunDetails(new Status())
						.build();
				this.reconciliationRunService.saveReconciliationRun(reconciliationRun);

				ReconciliationDefinitionFieldMapping mapping = ReconciliationDefinitionFieldMappingBuilder.aReconciliationDefinitionFieldMapping()
						.withDefinitionField(field)
						.withSource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
						.withName("Holding Account")
						.withEvaluationOrder(10)
						.build();

				this.reconciliationDefinitionService.saveReconciliationDefinitionFieldMapping(mapping);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("Unable to change field mappings as there are existing reconciliation runs associated with the active dates of its definition field.<br />Please exclude or end the definition field instead."));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationFieldMappingMissingSameConditionValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinitionFieldMapping mapping = createHoldingAccountFieldMapping();

				ReconciliationRuleCondition condition = createAccountBeginsWith047Or057Rule();

				mapping = ReconciliationDefinitionFieldMappingBuilder.aReconciliationDefinitionFieldMapping()
						.withDefinitionField(mapping.getDefinitionField())
						.withSource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
						.withConditionRule(condition)
						.withName("Holding Account")
						.withEvaluationOrder(10)
						.build();
				this.reconciliationDefinitionService.saveReconciliationDefinitionFieldMapping(mapping);

				mapping = ReconciliationDefinitionFieldMappingBuilder.aReconciliationDefinitionFieldMapping()
						.withDefinitionField(mapping.getDefinitionField())
						.withSource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
						.withConditionRule(mapping.getConditionRule())
						.withName("Holding Account")
						.withEvaluationOrder(10)
						.build();
				this.reconciliationDefinitionService.saveReconciliationDefinitionFieldMapping(mapping);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("You cannot not add a mapping for the same field and source with the same condition!"));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationFieldMappingMissingNullConditionValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationRuleCondition condition = createAccountBeginsWith047Or057Rule();
				ReconciliationDefinitionFieldMapping mapping = ReconciliationDefinitionFieldMappingBuilder.aReconciliationDefinitionFieldMapping()
						.withDefinitionField(createHoldingAccountDefinitionField())
						.withSource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
						.withConditionRule(condition)
						.withName("Holding Account")
						.withEvaluationOrder(10)
						.build();
				this.reconciliationDefinitionService.saveReconciliationDefinitionFieldMapping(mapping);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("You cannot add a mapping with a condition when there is not another mapping with no condition for the same field and source!"));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationFieldMappingDeleteNullConditionValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinitionFieldMapping mapping = createHoldingAccountFieldMapping();

				ReconciliationRuleCondition condition = createAccountBeginsWith047Or057Rule();

				ReconciliationDefinitionFieldMapping mapping2 = ReconciliationDefinitionFieldMappingBuilder.aReconciliationDefinitionFieldMapping()
						.withDefinitionField(mapping.getDefinitionField())
						.withSource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
						.withConditionRule(condition)
						.withName("Holding Account")
						.withEvaluationOrder(10)
						.build();
				this.reconciliationDefinitionService.saveReconciliationDefinitionFieldMapping(mapping2);
				this.reconciliationDefinitionService.deleteReconciliationDefinitionFieldMapping(mapping.getId());
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("You cannot delete a mapping with no condition when a mapping with a condition for the same field and source still exists!"));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationFieldMappingUpdateNullConditionValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinitionFieldMapping mapping = createHoldingAccountFieldMapping();

				ReconciliationDefinitionFieldMapping mapping2 = ReconciliationDefinitionFieldMappingBuilder.aReconciliationDefinitionFieldMapping()
						.withDefinitionField(mapping.getDefinitionField())
						.withConditionRule(createAccountBeginsWith047Or057Rule())
						.withSource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
						.withName("Holding Account")
						.withEvaluationOrder(10)
						.build();
				this.reconciliationDefinitionService.saveReconciliationDefinitionFieldMapping(mapping2);

				ReconciliationRuleCondition condition2 = createNonGSFCMAccountsRule();
				mapping.setConditionRule(condition2);
				this.reconciliationDefinitionService.saveReconciliationDefinitionFieldMapping(mapping);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("You cannot add a mapping with a condition when there is not another mapping with no condition for the same field and source!"));
				throw e;
			}
		});
	}
}
