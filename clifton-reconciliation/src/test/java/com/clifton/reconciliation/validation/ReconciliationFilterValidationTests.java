package com.clifton.reconciliation.validation;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.definition.rule.ReconciliationFilter;
import com.clifton.reconciliation.definition.rule.ReconciliationFilterBuilder;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunBuilder;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;


@Transactional
@ContextConfiguration(locations = "classpath:com/clifton/reconciliation/validation/BaseReconciliationValidationTests-context.xml")
public class ReconciliationFilterValidationTests extends BaseReconciliationValidationTests {

	@Autowired
	private PlatformTransactionManager transactionManager;

	////////////////////////////////////////////////////////////////////////////
	////////           Reconciliation Filter Validation Tests          /////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void reconciliationFilterDeleteValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationFilter reconciliationFilter = createRemoveNonGSFCMAccountsFilter();

				ReconciliationRun reconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
						.withRunDate(DateUtils.toDate("12/31/2018"))
						.withReconciliationDefinition(reconciliationFilter.getReconciliationDefinition())
						.withReconciliationReconcileStatus(this.reconciliationStatusService.getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()))
						.withRunDetails(new Status())
						.build();
				this.reconciliationRunService.saveReconciliationRun(reconciliationRun);
				this.reconciliationFilterService.deleteReconciliationFilter(reconciliationFilter.getId());
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("Unable to delete the filter fields as there are existing reconciliation runs associated with it.<br />Please exclude or end the filter instead."));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationFilterNullDefinitionValidationTest() {
		TestUtils.expectException(RuntimeException.class, () -> {
			try {
				ReconciliationFilter reconciliationFilter = ReconciliationFilterBuilder.aReconciliationFilter()
						.withName("Remove non-GS FCM Accounts")
						.withReconciliationSource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
						.withReconciliationDefinition(null)
						.withConditionRule(createNonGSFCMAccountsRule())
						.withStartDate(DateUtils.toDate("11/30/2017"))
						.withInclude(true)
						.build();
				this.reconciliationFilterService.saveReconciliationFilter(reconciliationFilter);
			}
			catch (RuntimeException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("Reconciliation Definition may not be null!"));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationFilterNullSourceValidationTest() {
		TestUtils.expectException(RuntimeException.class, () -> {
			try {
				ReconciliationFilter reconciliationFilter = ReconciliationFilterBuilder.aReconciliationFilter()
						.withName("Remove non-GS FCM Accounts")
						.withReconciliationSource(null)
						.withReconciliationDefinition(createGoldmanFuturesDefinition())
						.withConditionRule(createNonGSFCMAccountsRule())
						.withStartDate(DateUtils.toDate("11/30/2017"))
						.withInclude(true)
						.build();
				this.reconciliationFilterService.saveReconciliationFilter(reconciliationFilter);
			}
			catch (RuntimeException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("Reconciliation Source may not be null!"));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationFilterDifferentSourceValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			ReconciliationDefinition reconciliationDefinition = new ReconciliationDefinition();
			ReconciliationFilter reconciliationFilter = new ReconciliationFilter();
			try {
				reconciliationDefinition = createGoldmanFuturesDefinition();
				reconciliationFilter = ReconciliationFilterBuilder.aReconciliationFilter()
						.withName("Remove non-GS FCM Accounts")
						.withReconciliationSource(this.reconciliationDefinitionService.getReconciliationSourceByName("Brown Brothers Harriman"))
						.withReconciliationDefinition(reconciliationDefinition)
						.withConditionRule(createNonGSFCMAccountsRule())
						.withStartDate(DateUtils.toDate("11/30/2017"))
						.withInclude(true)
						.build();
				this.reconciliationFilterService.saveReconciliationFilter(reconciliationFilter);
			}
			catch (RuntimeException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("Filter Reconciliation Source [" + reconciliationFilter.getReconciliationSource().getName() + "] must match either the primary [" + reconciliationDefinition.getPrimarySource().getName() + "] or secondary [" + reconciliationDefinition.getSecondarySource().getName() + "] source of the selected definition!"));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationFilterDuplicateValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationFilter reconciliationFilter = createRemoveNonGSFCMAccountsFilter();

				reconciliationFilter = ReconciliationFilterBuilder.aReconciliationFilter()
						.withName("Remove non-GS FCM Accounts")
						.withReconciliationSource(reconciliationFilter.getReconciliationSource())
						.withReconciliationDefinition(reconciliationFilter.getReconciliationDefinition())
						.withConditionRule(createNonGSFCMAccountsRule())
						.withStartDate(DateUtils.toDate("11/30/2017"))
						.withInclude(true)
						.build();
				this.reconciliationFilterService.saveReconciliationFilter(reconciliationFilter);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("You may not save a filter with the same name, for the same definition, that overlaps the active date for an existing filter.  Please exclude or end the previous filter first."));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationFilterDeleteDateRangeValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinition reconciliationDefinition = createBrownBrothersPositionDefinition();

				ReconciliationRun reconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
						.withRunDate(DateUtils.toDate("12/31/2018"))
						.withReconciliationDefinition(reconciliationDefinition)
						.withReconciliationReconcileStatus(this.reconciliationStatusService.getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()))
						.withRunDetails(new Status())
						.build();
				this.reconciliationRunService.saveReconciliationRun(reconciliationRun);

				ReconciliationFilter reconciliationFilter = ReconciliationFilterBuilder.aReconciliationFilter()
						.withName("Remove non-GS FCM Accounts")
						.withReconciliationSource(reconciliationDefinition.getPrimarySource())
						.withReconciliationDefinition(reconciliationDefinition)
						.withConditionRule(createNonGSFCMAccountsRule())
						.withStartDate(DateUtils.addDays(reconciliationRun.getRunDate(), -1))
						.withInclude(true)
						.build();

				this.reconciliationFilterService.saveReconciliationFilter(reconciliationFilter);
				this.reconciliationFilterService.deleteReconciliationFilter(reconciliationFilter.getId());
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("You must delete any runs during the active date before adding a filter; or set the active date after the latest run associated with the definition."));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationFilterStartDateValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinition reconciliationDefinition = createBrownBrothersPositionDefinition();

				ReconciliationRun reconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
						.withRunDate(DateUtils.toDate("12/31/2018"))
						.withReconciliationDefinition(reconciliationDefinition)
						.withReconciliationReconcileStatus(this.reconciliationStatusService.getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()))
						.withRunDetails(new Status())
						.build();
				this.reconciliationRunService.saveReconciliationRun(reconciliationRun);

				ReconciliationFilter reconciliationFilter = ReconciliationFilterBuilder.aReconciliationFilter()
						.withName("Remove non-GS FCM Accounts")
						.withReconciliationSource(reconciliationDefinition.getPrimarySource())
						.withReconciliationDefinition(reconciliationDefinition)
						.withConditionRule(createNonGSFCMAccountsRule())
						.withStartDate(DateUtils.addDays(reconciliationRun.getRunDate(), -1))
						.withInclude(true)
						.build();

				this.reconciliationFilterService.saveReconciliationFilter(reconciliationFilter);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("You must delete any runs during the active date before adding a filter; or set the active date after the latest run associated with the definition."));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationFilterUpdateEndDateValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				TransactionTemplate template = new TransactionTemplate(this.transactionManager);
				template.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
				ReconciliationDefinition definition = this.reconciliationDefinitionService.getReconciliationDefinitionByName("Goldman Sachs Futures Position Reconciliation");
				Assertions.assertNotNull(definition);
				template.execute(s -> {
					ReconciliationFilter filter = ReconciliationFilterBuilder.aReconciliationFilter()
							.withName("Remove non-GS FCM Accounts")
							.withReconciliationSource(definition.getPrimarySource())
							.withReconciliationDefinition(definition)
							.withConditionRule(createNonGSFCMAccountsRule())
							.withStartDate(DateUtils.toDate("12/30/2018"))
							.withInclude(true)
							.build();
					this.reconciliationFilterService.saveReconciliationFilter(filter);
					return null;
				});
				ReconciliationFilter filter = this.reconciliationFilterService.getReconciliationFilterByName("Remove non-GS FCM Accounts");
				Assertions.assertNotNull(filter);

				ReconciliationRun reconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
						.withRunDate(DateUtils.toDate("12/31/2018"))
						.withReconciliationDefinition(definition)
						.withReconciliationReconcileStatus(this.reconciliationStatusService.getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()))
						.withRunDetails(new Status())
						.build();
				this.reconciliationRunService.saveReconciliationRun(reconciliationRun);

				filter.setEndDate(DateUtils.toDate("12/30/2018"));
				this.reconciliationFilterService.saveReconciliationFilter(filter);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("You may not modify a filter if there are existing runs associated with the filter's current active dates!"));
				throw e;
			}
		});
	}
}
