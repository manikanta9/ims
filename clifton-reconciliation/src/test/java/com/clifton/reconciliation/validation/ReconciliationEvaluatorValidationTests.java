package com.clifton.reconciliation.validation;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;


@Transactional
@ContextConfiguration(locations = "classpath:com/clifton/reconciliation/validation/BaseReconciliationValidationTests-context.xml")
public class ReconciliationEvaluatorValidationTests extends BaseReconciliationValidationTests {

	@Resource
	private PlatformTransactionManager transactionManager;

	////////////////////////////////////////////////////////////////////////////
	////////         Reconciliation Evaluator Validation Tests         /////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCreateEvaluator() {
		TestUtils.expectException(ValidationException.class, () -> {
			ReconciliationDefinitionField definitionField = createPriceDefinitionField(createBrownBrothersPositionDefinition());
			ReconciliationEvaluator evaluator = createEvaluator("Goldman Sachs IRS Closing Price .15 Threshold", definitionField, this.systemBeanService.getSystemBeanByName(".15 Threshold"), DateUtils.toDate("04/03/1981"), null, 10);
			Assertions.assertEquals(evaluator.getName(), "Goldman Sachs IRS Closing Price .15 Threshold");
			createEvaluator(null, null, null, DateUtils.toDate("12/10/2018"), null, 10);
		}, "You must specify a definition field for an evaluator!");
	}


	@Test
	public void testEvaluatorOverlapInDates() {
		TestUtils.expectException(ValidationException.class, () -> {
			ReconciliationDefinitionField definitionField = createPriceDefinitionField(createBrownBrothersPositionDefinition());
			createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/11/2018"));
			createEvaluator("Goldman Sachs IRS Closing Price .15 Threshold", definitionField, this.systemBeanService.getSystemBeanByName(".15 Threshold"), DateUtils.toDate("12/10/2018"), DateUtils.toDate("12/12/2018"), 10);
			createEvaluator("Goldman Sachs IRS Closing Price .15 Threshold", definitionField, this.systemBeanService.getSystemBeanByName(".15 Threshold"), DateUtils.toDate("12/09/2018"), DateUtils.toDate("12/11/2018"), 10);
		}, "An evaluator already exists with an overlapping date range.  Please either edit the existing assignment, or end it and start a new one.");
	}


	@Test
	public void testCreateEvaluatorSingleRun() {
		ReconciliationDefinitionField definitionField = createPriceDefinitionField(createBrownBrothersPositionDefinition());
		createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/11/2018"));
		ReconciliationEvaluator evaluator = createEvaluator("Goldman Sachs IRS Closing Price .15 Threshold", definitionField, this.systemBeanService.getSystemBeanByName(".15 Threshold"), DateUtils.toDate("12/10/2018"), DateUtils.toDate("12/12/2018"), 10);
		Assertions.assertEquals(evaluator.getName(), "Goldman Sachs IRS Closing Price .15 Threshold");
	}


	@Test
	public void testUpdateEvaluatorSingleRun() {
		ReconciliationDefinitionField definitionField = createPriceDefinitionField(createBrownBrothersPositionDefinition());
		createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/10/2018"));
		ReconciliationEvaluator evaluator = createEvaluator("Goldman Sachs IRS Closing Price .15 Threshold", definitionField, this.systemBeanService.getSystemBeanByName(".15 Threshold"), DateUtils.toDate("12/10/2018"), DateUtils.toDate("12/12/2018"), 10);
		Assertions.assertEquals(evaluator.getName(), "Goldman Sachs IRS Closing Price .15 Threshold");
		//Modify the end date
		evaluator.setEndDate(DateUtils.toDate("12/13/2018"));
		this.reconciliationEvaluatorService.saveReconciliationEvaluator(evaluator);
		Assertions.assertEquals(evaluator.getName(), "Goldman Sachs IRS Closing Price .15 Threshold");
		//Modify the start date - still only affects one run so should save fine
		evaluator.setStartDate(DateUtils.toDate("12/09/2018"));
		this.reconciliationEvaluatorService.saveReconciliationEvaluator(evaluator);
		Assertions.assertEquals(evaluator.getName(), "Goldman Sachs IRS Closing Price .15 Threshold");
	}


	@Test
	public void testDeleteEvaluatorSingleRun() {
		TestUtils.expectException(ValidationException.class, () -> {
			ReconciliationDefinitionField definitionField = createPriceDefinitionField(createBrownBrothersPositionDefinition());
			createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/11/2018"));
			ReconciliationEvaluator evaluator = createEvaluator("Goldman Sachs IRS Closing Price .15 Threshold", definitionField, this.systemBeanService.getSystemBeanByName(".15 Threshold"), DateUtils.toDate("12/10/2018"), DateUtils.toDate("12/12/2018"), 10);
			Assertions.assertEquals(evaluator.getName(), "Goldman Sachs IRS Closing Price .15 Threshold");
			this.reconciliationEvaluatorService.deleteReconciliationEvaluator(evaluator.getId());
		}, "You may not delete an evaluator when any runs are associated with the active dates!<br /><br />The following runs are applicable:<br /><br />Brown Brothers Harriman Position Reconciliation (12/11/2018 12:00 AM)<br />");
	}


	@Test
	public void testCreateEvaluatorMultipleRuns() {
		TestUtils.expectException(ValidationException.class, () -> {
			ReconciliationDefinitionField definitionField = createPriceDefinitionField(createBrownBrothersPositionDefinition());
			createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/11/2018"));
			createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/12/2018"));
			createEvaluator("Goldman Sachs IRS Closing Price .15 Threshold", definitionField, this.systemBeanService.getSystemBeanByName(".15 Threshold"), DateUtils.toDate("12/10/2018"), DateUtils.toDate("12/12/2018"), 10);
		}, "You must delete any runs after the start date; or set the start date equal to the latest run!<br /><br />The following runs are applicable:<br /><br />Brown Brothers Harriman Position Reconciliation (12/12/2018 12:00 AM)<br />Brown Brothers Harriman Position Reconciliation (12/11/2018 12:00 AM)<br />");
	}


	@Test
	public void testUpdateEvaluatorMultipleRuns() {
		TestUtils.expectException(ValidationException.class, () -> {
			TransactionTemplate template = new TransactionTemplate(this.transactionManager);
			template.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);

			AtomicReference<ReconciliationEvaluator> exclusionAssignmentAtomicReference = new AtomicReference<>();
			template.execute(s -> {
				ReconciliationDefinitionField definitionField = createPriceDefinitionField(createBrownBrothersPositionDefinition());
				createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/09/2018"));
				createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/10/2018"));
				exclusionAssignmentAtomicReference.set(createEvaluator("Goldman Sachs IRS Closing Price .15 Threshold", definitionField, this.systemBeanService.getSystemBeanByName(".15 Threshold"), DateUtils.toDate("12/10/2018"), DateUtils.toDate("12/12/2018"), 10));
				Assertions.assertEquals(exclusionAssignmentAtomicReference.get().getName(), "Goldman Sachs IRS Closing Price .15 Threshold");
				return null;
			});
			//Modify the start date - affects multiple runs - should fail
			exclusionAssignmentAtomicReference.get().setStartDate(DateUtils.toDate("12/09/2018"));
			this.reconciliationEvaluatorService.saveReconciliationEvaluator(exclusionAssignmentAtomicReference.get());
		}, "You may not modify any field other than 'End Date' on an evaluator that applies to multiple runs!<br /><br />The following runs are applicable:<br /><br />Brown Brothers Harriman Position Reconciliation (12/10/2018 12:00 AM)<br />Brown Brothers Harriman Position Reconciliation (12/09/2018 12:00 AM)<br />");
	}


	@Test
	public void testDeleteEvaluatorMultipleRuns() {
		TestUtils.expectException(ValidationException.class, () -> {
			ReconciliationDefinitionField definitionField = createPriceDefinitionField(createBrownBrothersPositionDefinition());
			ReconciliationEvaluator evaluator = createEvaluator("Goldman Sachs IRS Closing Price .15 Threshold", definitionField, this.systemBeanService.getSystemBeanByName(".15 Threshold"), DateUtils.toDate("12/10/2018"), DateUtils.toDate("12/12/2018"), 10);
			createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/10/2018"));
			createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/11/2018"));
			this.reconciliationEvaluatorService.deleteReconciliationEvaluator(evaluator.getId());
		}, "You may not delete an evaluator when any runs are associated with the active dates!<br /><br />The following runs are applicable:<br /><br />Brown Brothers Harriman Position Reconciliation (12/11/2018 12:00 AM)<br />Brown Brothers Harriman Position Reconciliation (12/10/2018 12:00 AM)<br />");
	}


	@Test
	public void testCopyEvaluator() {
		ReconciliationDefinitionField definitionField = createPriceDefinitionField(createBrownBrothersPositionDefinition());
		ReconciliationEvaluator evaluator = createEvaluator("Goldman Sachs IRS Closing Price .15 Threshold", definitionField, this.systemBeanService.getSystemBeanByName(".15 Threshold"), DateUtils.toDate("12/10/2018"), DateUtils.toDate("12/12/2018"), 10);

		createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/11/2018"));
		createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/12/2018"));

		ReconciliationEvaluator evaluator2 = this.reconciliationEvaluatorService.copyReconciliationEvaluator(evaluator);

		Assertions.assertTrue(DateUtils.isDateBefore(evaluator.getEndDate(), evaluator2.getStartDate(), false), "Dates not set correctly");
		Assertions.assertTrue(DateUtils.isEqualWithoutTime(evaluator2.getStartDate(), new Date()), "Start date not today");
	}


	@Test
	public void testEndEvaluatorThenCopyEvaluator() {
		ReconciliationDefinitionField definitionField = createPriceDefinitionField(createBrownBrothersPositionDefinition());
		ReconciliationEvaluator evaluator = createEvaluator("Goldman Sachs IRS Closing Price .15 Threshold", definitionField, this.systemBeanService.getSystemBeanByName(".15 Threshold"), DateUtils.toDate("12/10/2018"), DateUtils.toDate("12/15/2018"), 10);

		createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/11/2018"));
		createReconciliationRun(definitionField.getReconciliationDefinition(), DateUtils.toDate("12/12/2018"));

		//End the evaluator
		evaluator.setEndDate(DateUtils.toDate("12/12/2018"));
		this.reconciliationEvaluatorService.saveReconciliationEvaluator(evaluator);

		ReconciliationEvaluator evaluator2 = this.reconciliationEvaluatorService.copyReconciliationEvaluator(evaluator);

		Assertions.assertTrue(DateUtils.isDateBefore(evaluator.getEndDate(), evaluator2.getStartDate(), false), "Dates not set correctly");
		Assertions.assertTrue(DateUtils.isEqualWithoutTime(evaluator2.getStartDate(), new Date()), "Start date not today");
	}
}
