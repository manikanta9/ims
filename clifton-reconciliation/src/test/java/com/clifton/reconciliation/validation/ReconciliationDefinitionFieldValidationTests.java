package com.clifton.reconciliation.validation;

import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.ReconciliationDefinitionFieldBuilder;
import com.clifton.reconciliation.definition.ReconciliationField;
import com.clifton.reconciliation.definition.ReconciliationFieldBuilder;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunBuilder;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@ContextConfiguration(locations = "classpath:com/clifton/reconciliation/validation/BaseReconciliationValidationTests-context.xml")
public class ReconciliationDefinitionFieldValidationTests extends BaseReconciliationValidationTests {


	////////////////////////////////////////////////////////////////////////////
	////////      Reconciliation Definition Field Validation Tests     /////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void reconciliationDefinitionFieldValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationField reconciliationField = ReconciliationFieldBuilder.aReconciliationField()
						.withName("Holding Account")
						.withDescription("Holding Account")
						.withLabel("Holding Account")
						.withDefaultDataType(DataTypes.DESCRIPTION)
						.withDefaultDisplayWidth(100)
						.build();
				this.reconciliationDefinitionService.saveReconciliationField(reconciliationField);
				ReconciliationDefinitionField reconciliationDefinitionField = ReconciliationDefinitionFieldBuilder.aReconciliationDefinitionField()
						.withReconciliationDefinition(null)
						.withField(reconciliationField)
						.withDataType(DataTypes.DESCRIPTION)
						.withNaturalKey(true)
						.withStartDate(DateUtils.toDate("11/27/2017"))
						.withDisplayOrder(10)
						.build();
				this.reconciliationDefinitionService.saveReconciliationDefinitionField(reconciliationDefinitionField);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("Reconciliation definition may not be null."));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationDefinitionFieldDeleteValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinitionField reconciliationDefinitionField = createHoldingAccountDefinitionField();

				ReconciliationRun reconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
						.withRunDate(DateUtils.toDate("12/11/2018"))
						.withReconciliationDefinition(reconciliationDefinitionField.getReconciliationDefinition())
						.withReconciliationReconcileStatus(this.reconciliationStatusService.getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()))
						.withRunDetails(new Status())
						.build();
				this.reconciliationRunService.saveReconciliationRun(reconciliationRun);

				this.reconciliationDefinitionService.deleteReconciliationDefinitionField(reconciliationDefinitionField.getId());
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("Unable to delete the definition fields as there are existing reconciliation runs associated with it.<br />Please exclude or end the definition field instead."));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationDefinitionFieldDuplicateValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinitionField reconciliationDefinitionField = createHoldingAccountDefinitionField();

				reconciliationDefinitionField = ReconciliationDefinitionFieldBuilder.aReconciliationDefinitionField()
						.withReconciliationDefinition(reconciliationDefinitionField.getReconciliationDefinition())
						.withField(reconciliationDefinitionField.getField())
						.withDataType(DataTypes.DESCRIPTION)
						.withNaturalKey(true)
						.withStartDate(DateUtils.toDate("11/27/2017"))
						.withDisplayOrder(10)
						.build();
				this.reconciliationDefinitionService.saveReconciliationDefinitionField(reconciliationDefinitionField);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("A definition field already exists with an overlapping date range.  Please either edit the existing definition, or end it and start a new one."));
				throw e;
			}
		});
	}


	public void reconciliationDefinitionFieldEndDateTest() {
		ReconciliationDefinition reconciliationDefinition = createGoldmanFuturesDefinition();

		ReconciliationRun reconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
				.withRunDate(DateUtils.toDate("12/11/2018"))
				.withReconciliationDefinition(reconciliationDefinition)
				.withReconciliationReconcileStatus(this.reconciliationStatusService.getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()))
				.withRunDetails(new Status())
				.build();
		this.reconciliationRunService.saveReconciliationRun(reconciliationRun);

		ReconciliationField reconciliationField = ReconciliationFieldBuilder.aReconciliationField()
				.withName("Holding Account")
				.withDescription("Holding Account")
				.withLabel("Holding Account")
				.withDefaultDataType(DataTypes.DESCRIPTION)
				.withDefaultDisplayWidth(100)
				.build();
		this.reconciliationDefinitionService.saveReconciliationField(reconciliationField);

		ReconciliationDefinitionField reconciliationDefinitionField = ReconciliationDefinitionFieldBuilder.aReconciliationDefinitionField()
				.withReconciliationDefinition(reconciliationDefinition)
				.withField(reconciliationField)
				.withDataType(DataTypes.DESCRIPTION)
				.withNaturalKey(true)
				.withStartDate(DateUtils.toDate("12/11/2017"))
				.withDisplayOrder(10)
				.build();
		this.reconciliationDefinitionService.saveReconciliationDefinitionField(reconciliationDefinitionField);

		reconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
				.withRunDate(DateUtils.toDate("12/12/2018"))
				.withReconciliationDefinition(reconciliationDefinition)
				.withReconciliationReconcileStatus(this.reconciliationStatusService.getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()))
				.withRunDetails(new Status())
				.build();
		this.reconciliationRunService.saveReconciliationRun(reconciliationRun);

		reconciliationDefinitionField.setEndDate(DateUtils.toDate("12/12/2018"));
		Assertions.assertTrue(DateUtils.isEqualWithoutTime(reconciliationDefinitionField.getEndDate(), DateUtils.toDate("12/12/2018")));
	}


	@Test
	public void reconciliationDefinitionFieldCantModifyTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinition reconciliationDefinition = createGoldmanFuturesDefinition();

				ReconciliationRun reconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
						.withRunDate(DateUtils.toDate("12/11/2018"))
						.withReconciliationDefinition(reconciliationDefinition)
						.withReconciliationReconcileStatus(this.reconciliationStatusService.getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()))
						.withRunDetails(new Status())
						.build();
				this.reconciliationRunService.saveReconciliationRun(reconciliationRun);

				ReconciliationField reconciliationField = ReconciliationFieldBuilder.aReconciliationField()
						.withName("Holding Account")
						.withDescription("Holding Account")
						.withLabel("Holding Account")
						.withDefaultDataType(DataTypes.DESCRIPTION)
						.withDefaultDisplayWidth(100)
						.build();
				this.reconciliationDefinitionService.saveReconciliationField(reconciliationField);

				ReconciliationDefinitionField reconciliationDefinitionField = ReconciliationDefinitionFieldBuilder.aReconciliationDefinitionField()
						.withReconciliationDefinition(reconciliationDefinition)
						.withField(reconciliationField)
						.withDataType(DataTypes.DESCRIPTION)
						.withNaturalKey(true)
						.withStartDate(DateUtils.toDate("12/11/2017"))
						.withDisplayOrder(10)
						.build();
				this.reconciliationDefinitionService.saveReconciliationDefinitionField(reconciliationDefinitionField);

				reconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
						.withRunDate(DateUtils.toDate("12/12/2018"))
						.withReconciliationDefinition(reconciliationDefinition)
						.withReconciliationReconcileStatus(this.reconciliationStatusService.getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()))
						.withRunDetails(new Status())
						.build();
				this.reconciliationRunService.saveReconciliationRun(reconciliationRun);

				reconciliationDefinitionField.setAdjustable(true);
				this.reconciliationDefinitionService.saveReconciliationDefinitionField(reconciliationDefinitionField);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("Unable to modify field as there are existing reconciliation runs associated with the active dates of its definition.<br />Please exclude or end the definition field instead."));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationDefinitionFieldOverlapTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinition reconciliationDefinition = createGoldmanFuturesDefinition();

				ReconciliationField reconciliationField = ReconciliationFieldBuilder.aReconciliationField()
						.withName("Holding Account")
						.withDescription("Holding Account")
						.withLabel("Holding Account")
						.withDefaultDataType(DataTypes.DESCRIPTION)
						.withDefaultDisplayWidth(100)
						.build();
				this.reconciliationDefinitionService.saveReconciliationField(reconciliationField);

				ReconciliationDefinitionField reconciliationDefinitionField = ReconciliationDefinitionFieldBuilder.aReconciliationDefinitionField()
						.withReconciliationDefinition(reconciliationDefinition)
						.withField(reconciliationField)
						.withDataType(DataTypes.DESCRIPTION)
						.withNaturalKey(true)
						.withStartDate(DateUtils.toDate("11/11/2017"))
						.withStartDate(DateUtils.toDate("12/13/2017"))
						.withDisplayOrder(10)
						.build();
				this.reconciliationDefinitionService.saveReconciliationDefinitionField(reconciliationDefinitionField);

				reconciliationDefinitionField = ReconciliationDefinitionFieldBuilder.aReconciliationDefinitionField()
						.withReconciliationDefinition(reconciliationDefinition)
						.withField(reconciliationField)
						.withDataType(DataTypes.DESCRIPTION)
						.withNaturalKey(true)
						.withStartDate(DateUtils.toDate("12/14/2017"))
						.withDisplayOrder(10)
						.build();
				this.reconciliationDefinitionService.saveReconciliationDefinitionField(reconciliationDefinitionField);

				//Now update the date to conflict with the prior definition field
				reconciliationDefinitionField.setStartDate(DateUtils.toDate("12/12/2017"));
				this.reconciliationDefinitionService.saveReconciliationDefinitionField(reconciliationDefinitionField);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("A definition field already exists with an overlapping date range.  Please either edit the existing definition, or end it and start a new one."));
				throw e;
			}
		});
	}
}
