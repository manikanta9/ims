package com.clifton.reconciliation.validation;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.ReconciliationService;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.exclusion.ReconciliationExclusionAssignment;
import com.clifton.reconciliation.file.ReconciliationFileCommand;
import com.clifton.reconciliation.file.ReconciliationFileDefinition;
import com.clifton.reconciliation.file.ReconciliationFileDefinitionService;
import com.clifton.reconciliation.file.search.ReconciliationFileDefinitionSearchForm;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.search.ReconciliationRunSearchForm;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Transactional
@ContextConfiguration(locations = "classpath:com/clifton/reconciliation/validation/BaseReconciliationValidationTests-context.xml")
public class ReconciliationExclusionAssignmentValidationTests extends BaseReconciliationValidationTests {

	@Resource
	private PlatformTransactionManager transactionManager;
	@Resource
	private ReconciliationFileDefinitionService reconciliationFileDefinitionService;
	@Resource
	private ReconciliationService reconciliationService;

	private ReconciliationFileDefinition reconciliationFileDefinition;


	@BeforeEach
	public void setup() {
		ReconciliationFileDefinitionSearchForm searchForm = new ReconciliationFileDefinitionSearchForm();
		searchForm.setName("IMS Atlantic Trust Position Reconciliation");
		Optional<ReconciliationFileDefinition> optional = this.reconciliationFileDefinitionService.getReconciliationFileDefinitionList(searchForm).stream().findFirst();
		Assertions.assertTrue(optional.isPresent());
		this.reconciliationFileDefinition = optional.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////     ReconciliationExclusionAssignment Validation Tests    /////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCreateExclusionAssignment() {
		TestUtils.expectException(ValidationException.class, () -> {
			ReconciliationSource source = this.reconciliationDefinitionService.getReconciliationSourceByName("Brown Brothers Harriman");
			ReconciliationExclusionAssignment exclusionAssignment = createExclusionAssignment(source, null, null, "1091029265", null, DateUtils.toDate("12/10/2018"), null);
			Assertions.assertEquals(exclusionAssignment.getLabel(), "Exclusion Assignment for " + source.getName() + " - 1091029265");
			createExclusionAssignment(null, null, null, "1091029265", null, DateUtils.toDate("12/10/2018"), null);
		}, "You must specify at least a source and account to exclude!");
	}


	@Test
	public void testExclusionAssignmentOverlapInDates() {
		TestUtils.expectException(ValidationException.class, () -> {
			ReconciliationDefinition definition = createBrownBrothersPositionDefinition();
			createReconciliationRun(definition, DateUtils.toDate("12/11/2018"));
			createExclusionAssignment(definition.getSecondarySource(), null, null, "1091029265", null, DateUtils.toDate("12/10/2018"), DateUtils.toDate("12/12/2018"));
			createExclusionAssignment(definition.getSecondarySource(), null, null, "1091029265", null, DateUtils.toDate("12/09/2018"), DateUtils.toDate("12/11/2018"));
		}, "An exclusion assignment already exists with an overlapping date range.  Please either edit the existing assignment, or end it and start a new one.");
	}


	@Test
	public void testCreateExclusionAssignmentSingleRun() {
		ReconciliationDefinition definition = createBrownBrothersPositionDefinition();
		createReconciliationRun(definition, DateUtils.toDate("12/11/2018"));
		ReconciliationExclusionAssignment exclusionAssignment = createExclusionAssignment(definition.getSecondarySource(), null, null, "1091029265", null, DateUtils.toDate("12/10/2018"), null);
		Assertions.assertEquals(exclusionAssignment.getLabel(), "Exclusion Assignment for " + definition.getSecondarySource().getName() + " - 1091029265");
	}


	@Test
	public void testUpdateExclusionAssignmentSingleRun() {
		ReconciliationDefinition definition = createBrownBrothersPositionDefinition();
		createReconciliationRun(definition, DateUtils.toDate("12/10/2018"));
		ReconciliationExclusionAssignment exclusionAssignment = createExclusionAssignment(definition.getSecondarySource(), null, null, "1091029265", null, DateUtils.toDate("12/10/2018"), DateUtils.toDate("12/12/2018"));
		Assertions.assertEquals(exclusionAssignment.getLabel(), "Exclusion Assignment for " + definition.getSecondarySource().getName() + " - 1091029265");
		//Modify the end date
		exclusionAssignment.setEndDate(DateUtils.toDate("12/13/2018"));
		this.reconciliationExclusionAssignmentService.saveReconciliationExclusionAssignment(exclusionAssignment);
		Assertions.assertEquals(exclusionAssignment.getLabel(), "Exclusion Assignment for " + definition.getSecondarySource().getName() + " - 1091029265");
		//Modify the start date - still only affects one run so should save fine
		exclusionAssignment.setStartDate(DateUtils.toDate("12/09/2018"));
		this.reconciliationExclusionAssignmentService.saveReconciliationExclusionAssignment(exclusionAssignment);
		Assertions.assertEquals(exclusionAssignment.getLabel(), "Exclusion Assignment for " + definition.getSecondarySource().getName() + " - 1091029265");
	}


	@Test
	public void testDeleteExclusionAssignmentSingleRun() {
		TransactionTemplate template = new TransactionTemplate(this.transactionManager);
		template.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
		template.execute(s -> {

			File positionInputFile = new File(getClass().getResource("data/IMS_ATL_20190517_positions__2019-05-20_06_00_18.csv").getFile());

			ReconciliationFileCommand reconciliationFileCommand = new ReconciliationFileCommand();
			reconciliationFileCommand.setAutomatedUpload(false);
			reconciliationFileCommand.setImportDate(new Date());
			reconciliationFileCommand.setFileDefinitionId(this.reconciliationFileDefinition.getId());
			reconciliationFileCommand.setFileRetrievalFunction(i -> positionInputFile);

			Status status = this.reconciliationService.uploadReconciliationFile(reconciliationFileCommand);
			Assertions.assertNotNull(status);
			MatcherAssert.assertThat(status.getMessage(), IsEqual.equalTo("Processed 5 row(s); Errors: 0; Filtered: 0"));

			return null;
		});

		ReconciliationRunSearchForm reconciliationRunSearchForm = new ReconciliationRunSearchForm();
		reconciliationRunSearchForm.setTypeName("Position Reconciliation");
		List<ReconciliationRun> runList = this.reconciliationRunService.getReconciliationRunList(reconciliationRunSearchForm);
		Assertions.assertEquals(1, runList.size());
		ReconciliationRun run = runList.get(0);

		List<ReconciliationRunObject> reconciliationRunObjectList = this.reconciliationRunService.getReconciliationRunObjectListByRunId(run.getId());
		Assertions.assertEquals(5, reconciliationRunObjectList.size());
		reconciliationRunObjectList.clear();
		ReconciliationExclusionAssignment exclusionAssignment = createExclusionAssignment(run.getReconciliationDefinition().getPrimarySource(), null, null, "65-0038-31-8", "IVZ", run.getRunDate(), null);
		reconciliationRunObjectList = this.reconciliationRunService.getReconciliationRunObjectListByRunId(run.getId());
		Assertions.assertEquals(4, reconciliationRunObjectList.size());
		reconciliationRunObjectList.clear();
		this.reconciliationExclusionAssignmentService.deleteReconciliationExclusionAssignment(exclusionAssignment.getId());
		reconciliationRunObjectList = this.reconciliationRunService.getReconciliationRunObjectListByRunId(run.getId());
		Assertions.assertEquals(5, reconciliationRunObjectList.size());
	}


	@Test
	public void testCreateExclusionAssignmentMultipleRuns() {
		TestUtils.expectException(ValidationException.class, () -> {
			ReconciliationDefinition definition = createBrownBrothersPositionDefinition();
			createReconciliationRun(definition, DateUtils.toDate("12/11/2018"));
			createReconciliationRun(definition, DateUtils.toDate("12/12/2018"));
			createExclusionAssignment(definition.getSecondarySource(), null, null, "1091029265", null, DateUtils.toDate("12/10/2018"), null);
		}, "You must delete any runs after the start date; or set the start date equal to the latest run!<br /><br />The following runs are applicable:<br /><br />Brown Brothers Harriman Position Reconciliation (12/12/2018 12:00 AM)<br />Brown Brothers Harriman Position Reconciliation (12/11/2018 12:00 AM)<br />");
	}


	@Test
	public void testUpdateExclusionAssignmentMultipleRuns() {
		TestUtils.expectException(ValidationException.class, () -> {
			TransactionTemplate template = new TransactionTemplate(this.transactionManager);
			template.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);

			AtomicReference<ReconciliationExclusionAssignment> exclusionAssignmentAtomicReference = new AtomicReference<>();
			template.execute(s -> {
				ReconciliationDefinition definition = createBrownBrothersPositionDefinition();
				createReconciliationRun(definition, DateUtils.toDate("12/09/2018"));
				createReconciliationRun(definition, DateUtils.toDate("12/10/2018"));
				exclusionAssignmentAtomicReference.set(createExclusionAssignment(definition.getSecondarySource(), null, null, "1091029265", null, DateUtils.toDate("12/10/2018"), DateUtils.toDate("12/12/2018")));
				Assertions.assertEquals(exclusionAssignmentAtomicReference.get().getLabel(), "Exclusion Assignment for " + definition.getSecondarySource().getName() + " - 1091029265");
				return null;
			});
			//Modify the start date - affects multiple runs - should fail
			exclusionAssignmentAtomicReference.get().setStartDate(DateUtils.toDate("12/09/2018"));
			this.reconciliationExclusionAssignmentService.saveReconciliationExclusionAssignment(exclusionAssignmentAtomicReference.get());
		}, "You may not modify any field other than 'End Date' on an evaluator that applies to multiple runs!<br /><br />The following runs are applicable:<br /><br />Brown Brothers Harriman Position Reconciliation (12/10/2018 12:00 AM)<br />Brown Brothers Harriman Position Reconciliation (12/09/2018 12:00 AM)<br />");
	}


	@Test
	public void testDeleteExclusionAssignmentMultipleRuns() {
		TestUtils.expectException(ValidationException.class, () -> {
			ReconciliationDefinition definition = createBrownBrothersPositionDefinition();
			ReconciliationExclusionAssignment exclusionAssignment = createExclusionAssignment(definition.getSecondarySource(), null, null, "1091029265", null, DateUtils.toDate("12/10/2018"), DateUtils.toDate("12/12/2018"));
			createReconciliationRun(definition, DateUtils.toDate("12/10/2018"));
			createReconciliationRun(definition, DateUtils.toDate("12/11/2018"));
			this.reconciliationExclusionAssignmentService.deleteReconciliationExclusionAssignment(exclusionAssignment.getId());
		}, "You may not delete an exclusion assignment when it has multiple runs associated with the definition.<br /><br />The following runs are applicable:<br /><br />Brown Brothers Harriman Position Reconciliation (12/11/2018 12:00 AM)<br />Brown Brothers Harriman Position Reconciliation (12/10/2018 12:00 AM)<br />");
	}
}
