package com.clifton.reconciliation.validation;

import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.json.condition.JsonConditionBuilder;
import com.clifton.core.json.condition.JsonConditionOperatorTypes;
import com.clifton.core.json.type.JsonDataTypes;
import com.clifton.core.json.type.JsonInputTypes;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionBuilder;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.ReconciliationDefinitionFieldBuilder;
import com.clifton.reconciliation.definition.ReconciliationDefinitionFieldMapping;
import com.clifton.reconciliation.definition.ReconciliationDefinitionFieldMappingBuilder;
import com.clifton.reconciliation.definition.ReconciliationField;
import com.clifton.reconciliation.definition.ReconciliationFieldBuilder;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluator;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluatorBuilder;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluatorService;
import com.clifton.reconciliation.definition.rule.ReconciliationFilter;
import com.clifton.reconciliation.definition.rule.ReconciliationFilterBuilder;
import com.clifton.reconciliation.definition.rule.ReconciliationFilterService;
import com.clifton.reconciliation.definition.type.ReconciliationType;
import com.clifton.reconciliation.exclusion.ReconciliationExclusionAssignment;
import com.clifton.reconciliation.exclusion.ReconciliationExclusionAssignmentBuilder;
import com.clifton.reconciliation.exclusion.ReconciliationExclusionAssignmentService;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.rule.ReconciliationRuleCondition;
import com.clifton.reconciliation.rule.ReconciliationRuleConditionBuilder;
import com.clifton.reconciliation.rule.ReconciliationRuleGroup;
import com.clifton.reconciliation.rule.ReconciliationRuleGroupBuilder;
import com.clifton.reconciliation.rule.ReconciliationRuleTypes;
import com.clifton.reconciliation.run.BaseReconciliationServiceTests;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunBuilder;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.workflow.definition.WorkflowState;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;


@Transactional
public abstract class BaseReconciliationValidationTests extends BaseReconciliationServiceTests {

	@Resource
	public CalendarSetupService calendarSetupService;

	@Resource
	public ReconciliationFilterService reconciliationFilterService;


	@Resource
	public ReconciliationExclusionAssignmentService reconciliationExclusionAssignmentService;

	@Resource
	public ReconciliationEvaluatorService reconciliationEvaluatorService;

	@Resource
	public SystemBeanService systemBeanService;


	public WorkflowState reconciliationWorkflowApproved;


	/**
	 * Overrides the testClass so that tests can use shared data files (e.g. load 'BaseReconciliationValidationTestsData.xml'
	 * instead of loading, for instance, 'ReconciliationExclusionAssignmentValidationTestsData.xml'
	 */
	@Override
	protected Class<?> getTestClass() {
		return BaseReconciliationValidationTests.class;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationRun createReconciliationRun(ReconciliationDefinition definition, Date runDate) {
		ReconciliationRun reconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
				.withRunDate(runDate)
				.withReconciliationDefinition(definition)
				.withReconciliationReconcileStatus(this.reconciliationStatusService.getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()))
				.withRunDetails(new Status())
				.build();
		return this.reconciliationRunService.saveReconciliationRun(reconciliationRun);
	}


	public ReconciliationEvaluator createEvaluator(String name, ReconciliationDefinitionField definitionField, SystemBean evaluatorBean, Date startDate, Date endDate, int order) {
		ReconciliationEvaluator reconciliationEvaluator = ReconciliationEvaluatorBuilder.aReconciliationEvaluator()
				.withName(name)
				.withDefinitionField(definitionField)
				.withEvaluatorBean(evaluatorBean)
				.withStartDate(startDate)
				.withEvaluationOrder(order)
				.build();
		return this.reconciliationEvaluatorService.saveReconciliationEvaluator(reconciliationEvaluator);
	}


	public ReconciliationExclusionAssignment createExclusionAssignment(ReconciliationSource source, ReconciliationInvestmentAccount investmentAccount, ReconciliationInvestmentSecurity investmentSecurity, String invalidAccountNumber, String invalidSecurityIdentifier, Date startDate, Date endDate) {
		ReconciliationExclusionAssignment reconciliationExclusionAssignment = ReconciliationExclusionAssignmentBuilder.aReconciliationExclusionAssignment()
				.withReconciliationSource(source)
				.withReconciliationInvestmentAccount(investmentAccount)
				.withInvalidAccountNumber(invalidAccountNumber)
				.withReconciliationInvestmentSecurity(investmentSecurity)
				.withInvalidSecurityIdentifier(invalidSecurityIdentifier)
				.withStartDate(startDate)
				.withEndDate(endDate)
				.withComment("I'm used for tests!")
				.build();
		return this.reconciliationExclusionAssignmentService.saveReconciliationExclusionAssignment(reconciliationExclusionAssignment);
	}


	public ReconciliationDefinition createGoldmanFuturesDefinition() {
		ReconciliationDefinition definition = ReconciliationDefinitionBuilder.aReconciliationDefinition()
				.withPrimarySource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
				.withSecondarySource(this.reconciliationDefinitionService.getReconciliationSourceByName("Goldman Sachs"))
				.withName("Goldman Sachs Futures Position Reconciliation")
				.withDescription("Position Reconciliation for Goldman Sachs")
				.withType(this.reconciliationTypeService.getReconciliationTypeByName(ReconciliationType.POSITION))
				.withWorkflowState(this.reconciliationWorkflowApproved)
				.withWorkflowStatus(this.workflowDefinitionService.getWorkflowStatusByName("Active"))
				.withCalendar(this.calendarSetupService.getCalendarByName("System Calendar"))
				.build();
		return this.reconciliationDefinitionService.saveReconciliationDefinition(definition);
	}


	public ReconciliationDefinition createGoldmanIRSFuturesDefinition() {
		ReconciliationDefinition definition = ReconciliationDefinitionBuilder.aReconciliationDefinition()
				.withPrimarySource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
				.withSecondarySource(this.reconciliationDefinitionService.getReconciliationSourceByName("Goldman Sachs"))
				.withPrimaryRuleGroup(createImsPositionsGroup())
				.withSecondaryRuleGroup(createGoldmanSwapPositionsGroup())
				.withName("Goldman Sachs Cleared IRS Position Reconciliation")
				.withDescription("Position Reconciliation for Goldman Sachs Cleared IRS")
				.withType(this.reconciliationTypeService.getReconciliationTypeByName(ReconciliationType.POSITION))
				.withWorkflowState(this.reconciliationWorkflowApproved)
				.withWorkflowStatus(this.workflowDefinitionService.getWorkflowStatusByName("Active"))
				.withCalendar(this.calendarSetupService.getCalendarByName("System Calendar"))
				.build();
		return this.reconciliationDefinitionService.saveReconciliationDefinition(definition);
	}


	public ReconciliationDefinition createBrownBrothersPositionDefinition() {
		ReconciliationDefinition definition = ReconciliationDefinitionBuilder.aReconciliationDefinition()
				.withPrimarySource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
				.withSecondarySource(this.reconciliationDefinitionService.getReconciliationSourceByName("Brown Brothers Harriman"))
				.withSecondaryRuleGroup(createAdventPositionGroup())
				.withName("Brown Brothers Harriman Position Reconciliation")
				.withDescription("Brown Brothers Harriman Position Reconciliation")
				.withType(this.reconciliationTypeService.getReconciliationTypeByName(ReconciliationType.POSITION))
				.withWorkflowState(this.reconciliationWorkflowApproved)
				.withWorkflowStatus(this.workflowDefinitionService.getWorkflowStatusByName("Active"))
				.withCalendar(this.calendarSetupService.getCalendarByName("System Calendar"))
				.build();
		return this.reconciliationDefinitionService.saveReconciliationDefinition(definition);
	}


	public ReconciliationRuleGroup createImsPositionsGroup() {
		ReconciliationRuleGroup ruleGroup = ReconciliationRuleGroupBuilder.aReconciliationRuleGroup()
				.withDescription("IMS Positions")
				.withName("IMS Positions")
				.build();
		return this.reconciliationRuleService.saveReconciliationRuleGroup(ruleGroup);
	}


	public ReconciliationRuleGroup createGoldmanSwapPositionsGroup() {
		ReconciliationRuleGroup ruleGroup = ReconciliationRuleGroupBuilder.aReconciliationRuleGroup()
				.withDescription("Goldman Sachs Swaps Positions")
				.withName("Goldman Sachs Swaps Positions")
				.build();
		return this.reconciliationRuleService.saveReconciliationRuleGroup(ruleGroup);
	}


	public ReconciliationRuleGroup createAdventPositionGroup() {
		ReconciliationRuleGroup ruleGroup = ReconciliationRuleGroupBuilder.aReconciliationRuleGroup()
				.withDescription("Rules that will apply to Advent Positions files")
				.withName("Advent Position Rules")
				.build();
		return this.reconciliationRuleService.saveReconciliationRuleGroup(ruleGroup);
	}


	public ReconciliationDefinitionField createPriceDefinitionField(ReconciliationDefinition reconciliationDefinition) {
		ReconciliationField reconciliationField = ReconciliationFieldBuilder.aReconciliationField()
				.withName("Holding Account")
				.withDescription("Holding Account")
				.withLabel("Holding Account")
				.withDefaultDataType(DataTypes.DESCRIPTION)
				.withDefaultDisplayWidth(100)
				.build();
		this.reconciliationDefinitionService.saveReconciliationField(reconciliationField);

		ReconciliationDefinitionField definitionField = ReconciliationDefinitionFieldBuilder.aReconciliationDefinitionField()
				.withReconciliationDefinition(reconciliationDefinition)
				.withField(reconciliationField)
				.withDataType(DataTypes.PRICE)
				.withReconcilable(true)
				.withStartDate(DateUtils.toDate("11/27/2017"))
				.withDisplayOrder(50)
				.build();
		return this.reconciliationDefinitionService.saveReconciliationDefinitionField(definitionField);
	}


	public ReconciliationDefinitionField createHoldingAccountDefinitionField() {
		ReconciliationField reconciliationField = ReconciliationFieldBuilder.aReconciliationField()
				.withName("Holding Account")
				.withDescription("Holding Account")
				.withLabel("Holding Account")
				.withDefaultDataType(DataTypes.DESCRIPTION)
				.withDefaultDisplayWidth(100)
				.build();
		this.reconciliationDefinitionService.saveReconciliationField(reconciliationField);

		ReconciliationDefinitionField definitionField = ReconciliationDefinitionFieldBuilder.aReconciliationDefinitionField()
				.withReconciliationDefinition(createGoldmanFuturesDefinition())
				.withField(reconciliationField)
				.withDataType(DataTypes.DESCRIPTION)
				.withNaturalKey(true)
				.withStartDate(DateUtils.toDate("11/27/2017"))
				.withDisplayOrder(10)
				.build();
		return this.reconciliationDefinitionService.saveReconciliationDefinitionField(definitionField);
	}


	public ReconciliationFilter createRemoveNonGSFCMAccountsFilter() {
		ReconciliationFilter filter = ReconciliationFilterBuilder.aReconciliationFilter()
				.withName("Remove non-GS FCM Accounts")
				.withReconciliationSource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
				.withReconciliationDefinition(createGoldmanFuturesDefinition())
				.withConditionRule(createNonGSFCMAccountsRule())
				.withStartDate(DateUtils.toDate("11/30/2017"))
				.withInclude(true)
				.build();
		return this.reconciliationFilterService.saveReconciliationFilter(filter);
	}


	public ReconciliationDefinitionFieldMapping createHoldingAccountFieldMapping() {
		ReconciliationDefinitionFieldMapping mapping = ReconciliationDefinitionFieldMappingBuilder.aReconciliationDefinitionFieldMapping()
				.withDefinitionField(createHoldingAccountDefinitionField())
				.withSource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
				.withName("Holding Account")
				.withEvaluationOrder(10)
				.build();
		return this.reconciliationDefinitionService.saveReconciliationDefinitionFieldMapping(mapping);
	}


	public ReconciliationRuleCondition createAccountBeginsWith047Or057Rule() {
		JsonConditionBuilder jsonConditionBuilder157 = new JsonConditionBuilder();
		jsonConditionBuilder157.withId("AccountNumber");
		jsonConditionBuilder157.withField("AccountNumber");
		jsonConditionBuilder157.withValue("157");
		jsonConditionBuilder157.setType(JsonDataTypes.STRING);
		jsonConditionBuilder157.setOperator(JsonConditionOperatorTypes.BEGINS_WITH);
		jsonConditionBuilder157.setInput(JsonInputTypes.TEXT);

		JsonConditionBuilder jsonConditionBuilder500 = new JsonConditionBuilder();
		jsonConditionBuilder500.withId("AccountNumber");
		jsonConditionBuilder500.withField("AccountNumber");
		jsonConditionBuilder500.withValue("500");
		jsonConditionBuilder500.setType(JsonDataTypes.STRING);
		jsonConditionBuilder500.setOperator(JsonConditionOperatorTypes.BEGINS_WITH);
		jsonConditionBuilder500.setInput(JsonInputTypes.TEXT);

		JsonConditionBuilder jsonConditionBuilder = new JsonConditionBuilder();

		ReconciliationRuleCondition reconciliationRuleCondition = ReconciliationRuleConditionBuilder.aReconciliationRuleCondition()
				.withName("Account begins with 047 or 057")
				.withDescription("Account begins with 047 or 057")
				.withJsonExpression(jsonConditionBuilder.orCondition(
						Arrays.asList(
								jsonConditionBuilder157.build(),
								jsonConditionBuilder500.build()
						)
				).build())
				.withRuleType(ReconciliationRuleTypes.CONDITION)
				.build();
		reconciliationRuleCondition.getJsonExpression().setValid(Boolean.TRUE);
		return (ReconciliationRuleCondition) this.reconciliationRuleService.saveReconciliationRule(reconciliationRuleCondition);
	}


	public ReconciliationRuleCondition createNonGSFCMAccountsRule() {
		JsonConditionBuilder jsonConditionBuilder157 = new JsonConditionBuilder();
		jsonConditionBuilder157.withId("Holding Account");
		jsonConditionBuilder157.withField("Holding Account");
		jsonConditionBuilder157.withValue("157");
		jsonConditionBuilder157.setType(JsonDataTypes.STRING);
		jsonConditionBuilder157.setOperator(JsonConditionOperatorTypes.BEGINS_WITH);
		jsonConditionBuilder157.setInput(JsonInputTypes.TEXT);

		JsonConditionBuilder jsonConditionBuilder500 = new JsonConditionBuilder();
		jsonConditionBuilder500.withId("Holding Account");
		jsonConditionBuilder500.withField("Holding Account");
		jsonConditionBuilder500.withValue("500");
		jsonConditionBuilder500.setType(JsonDataTypes.STRING);
		jsonConditionBuilder500.setOperator(JsonConditionOperatorTypes.BEGINS_WITH);
		jsonConditionBuilder500.setInput(JsonInputTypes.TEXT);

		JsonConditionBuilder jsonConditionBuilder = new JsonConditionBuilder();

		ReconciliationRuleCondition reconciliationRuleCondition = ReconciliationRuleConditionBuilder.aReconciliationRuleCondition()
				.withName("Remove non-GS FCM Accounts")
				.withJsonExpression(jsonConditionBuilder.orCondition(
						Arrays.asList(
								jsonConditionBuilder157.build(),
								jsonConditionBuilder500.build()
						)
				).build())
				.withRuleType(ReconciliationRuleTypes.CONDITION)
				.build();
		reconciliationRuleCondition.getJsonExpression().setValid(Boolean.TRUE);
		return (ReconciliationRuleCondition) this.reconciliationRuleService.saveReconciliationRule(reconciliationRuleCondition);
	}
}
