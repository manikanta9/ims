package com.clifton.reconciliation.validation;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionBuilder;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.definition.type.ReconciliationType;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunBuilder;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@ContextConfiguration(locations = "classpath:com/clifton/reconciliation/validation/BaseReconciliationValidationTests-context.xml")
public class ReconciliationDefinitionValidationTests extends BaseReconciliationValidationTests {

	////////////////////////////////////////////////////////////////////////////
	////////        Reconciliation Definition Validation Tests         /////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void reconciliationDefinitionDeletionValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinition reconciliationDefinition = createGoldmanFuturesDefinition();
				ReconciliationRun reconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
						.withRunDate(DateUtils.toDate("12/11/2018"))
						.withReconciliationDefinition(reconciliationDefinition)
						.withReconciliationReconcileStatus(this.reconciliationStatusService.getReconciliationReconcileStatusByName(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()))
						.withRunDetails(new Status())
						.build();
				this.reconciliationRunService.saveReconciliationRun(reconciliationRun);
				this.reconciliationDefinitionService.deleteReconciliationDefinition(reconciliationDefinition.getId());
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("Unable to delete the definition as there are existing reconciliation runs associated with it.<br />Please exclude or end the definition instead."));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationDefinitionAlreadyParentValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinition goldman = createGoldmanFuturesDefinition();

				ReconciliationSource ims = goldman.getPrimarySource();

				ReconciliationDefinition irs = ReconciliationDefinitionBuilder.aReconciliationDefinition()
						.withPrimarySource(ims)
						.withSecondarySource(this.reconciliationDefinitionService.getReconciliationSourceByName("Goldman Sachs"))
						.withName("Goldman Sachs Cleared IRS Position Reconciliation")
						.withDescription("Position Reconciliation for Goldman Sachs Cleared IRS")
						.withType(this.reconciliationTypeService.getReconciliationTypeByName(ReconciliationType.POSITION))
						.withWorkflowState(this.reconciliationWorkflowApproved)
						.withWorkflowStatus(this.workflowDefinitionService.getWorkflowStatusByName("Active"))
						.withCalendar(this.calendarSetupService.getCalendarByName("System Calendar"))
						.build();
				irs = this.reconciliationDefinitionService.saveReconciliationDefinition(irs);

				ReconciliationDefinition brown = ReconciliationDefinitionBuilder.aReconciliationDefinition()
						.withPrimarySource(ims)
						.withSecondarySource(this.reconciliationDefinitionService.getReconciliationSourceByName("Brown Brothers Harriman"))
						.withName("Brown Brothers Harriman Position Reconciliation")
						.withDescription("Brown Brothers Harriman Position Reconciliation")
						.withType(this.reconciliationTypeService.getReconciliationTypeByName(ReconciliationType.POSITION))
						.withWorkflowState(this.reconciliationWorkflowApproved)
						.withWorkflowStatus(this.workflowDefinitionService.getWorkflowStatusByName("Active"))
						.withCalendar(this.calendarSetupService.getCalendarByName("System Calendar"))
						.build();
				brown = this.reconciliationDefinitionService.saveReconciliationDefinition(brown);

				irs.setParentDefinition(goldman);
				this.reconciliationDefinitionService.saveReconciliationDefinition(irs);
				brown.setParentDefinition(goldman);
				this.reconciliationDefinitionService.saveReconciliationDefinition(brown);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("Reconciliation Definitions can only be one level deep.  The parent definition is already a parent - it can only have one child."));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationDefinitionItselfParentValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinition reconciliationDefinition = createGoldmanFuturesDefinition();
				reconciliationDefinition.setParentDefinition(reconciliationDefinition);
				this.reconciliationDefinitionService.saveReconciliationDefinition(reconciliationDefinition);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("Reconciliation Definitions can only be one level deep.  This definition is already a parent definition."));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationDefinitionParentParentValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationDefinition goldman = createGoldmanFuturesDefinition();

				ReconciliationSource ims = goldman.getPrimarySource();

				ReconciliationDefinition irs = ReconciliationDefinitionBuilder.aReconciliationDefinition()
						.withPrimarySource(ims)
						.withSecondarySource(this.reconciliationDefinitionService.getReconciliationSourceByName("Goldman Sachs"))
						.withName("Goldman Sachs Cleared IRS Position Reconciliation")
						.withDescription("Position Reconciliation for Goldman Sachs Cleared IRS")
						.withType(this.reconciliationTypeService.getReconciliationTypeByName(ReconciliationType.POSITION))
						.withWorkflowState(this.reconciliationWorkflowApproved)
						.withWorkflowStatus(this.workflowDefinitionService.getWorkflowStatusByName("Active"))
						.withCalendar(this.calendarSetupService.getCalendarByName("System Calendar"))
						.build();
				irs = this.reconciliationDefinitionService.saveReconciliationDefinition(irs);

				ReconciliationDefinition brown = ReconciliationDefinitionBuilder.aReconciliationDefinition()
						.withPrimarySource(ims)
						.withSecondarySource(this.reconciliationDefinitionService.getReconciliationSourceByName("Brown Brothers Harriman"))
						.withName("Brown Brothers Harriman Position Reconciliation")
						.withDescription("Brown Brothers Harriman Position Reconciliation")
						.withType(this.reconciliationTypeService.getReconciliationTypeByName(ReconciliationType.POSITION))
						.withWorkflowState(this.reconciliationWorkflowApproved)
						.withWorkflowStatus(this.workflowDefinitionService.getWorkflowStatusByName("Active"))
						.withCalendar(this.calendarSetupService.getCalendarByName("System Calendar"))
						.build();
				brown = this.reconciliationDefinitionService.saveReconciliationDefinition(brown);

				irs.setParentDefinition(goldman);
				this.reconciliationDefinitionService.saveReconciliationDefinition(irs);
				brown.setParentDefinition(irs);
				this.reconciliationDefinitionService.saveReconciliationDefinition(brown);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("Reconciliation Definitions can only be one level deep.  The parent definition has a parent definition."));
				throw e;
			}
		});
	}
}
