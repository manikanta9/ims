package com.clifton.reconciliation.validation;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityAliasBuilder;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityBuilder;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@ContextConfiguration(locations = "classpath:com/clifton/reconciliation/validation/BaseReconciliationValidationTests-context.xml")
public class ReconciliationInvestmentSecurityAliasValidationTests extends BaseReconciliationValidationTests {

	////////////////////////////////////////////////////////////////////////////
	//////// Reconciliation Investment Security Alias Validation Tests /////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void reconciliationInvestmentSecurityAliasDefaultSourceValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationInvestmentSecurityAlias reconciliationInvestmentSecurityAlias = ReconciliationInvestmentSecurityAliasBuilder.aReconciliationInvestmentSecurityAlias()
						.withAlias("Alias")
						.withReconciliationSource(this.reconciliationDefinitionService.getReconciliationSourceByName(ReconciliationSource.DEFAULT_SOURCE))
						.withDisplayProperty("Display Property")
						.withStartDate(DateUtils.toDate("01/01/2018"))
						.withInvestmentSecurity(this.reconciliationInvestmentService.getReconciliationInvestmentSecurityBySymbol("LBHYP"))
						.build();
				this.reconciliationInvestmentService.saveReconciliationInvestmentSecurityAlias(reconciliationInvestmentSecurityAlias);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("You may not create an alias for the '" + ReconciliationSource.DEFAULT_SOURCE + "' source!"));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationInvestmentSecurityAliasNullSourceValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationInvestmentSecurityAlias reconciliationInvestmentSecurityAlias = ReconciliationInvestmentSecurityAliasBuilder.aReconciliationInvestmentSecurityAlias()
						.withAlias("Alias")
						.withDisplayProperty("Display Property")
						.withStartDate(DateUtils.toDate("01/01/2018"))
						.withInvestmentSecurity(this.reconciliationInvestmentService.getReconciliationInvestmentSecurityBySymbol("LBHYP"))
						.build();
				this.reconciliationInvestmentService.saveReconciliationInvestmentSecurityAlias(reconciliationInvestmentSecurityAlias);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("You must specify a source for the alias mapping!"));
				throw e;
			}
		});
	}


	@Test
	public void reconciliationInvestmentSecurityAliasOverlappingValidationTest() {
		TestUtils.expectException(ValidationException.class, () -> {
			try {
				ReconciliationInvestmentSecurity security = ReconciliationInvestmentSecurityBuilder.aReconciliationInvestmentSecurity()
						.withSecurityIdentifier(5)
						.withSecurityName("LBHYP")
						.withSecuritySymbol("LBHYP")
						.withSecurityTypeName("Benchmarks")
						.withSecurityTypeSubTypeName("Indices")
						.withSecurityTypeSubType2Name("Custom")
						.build();
				this.reconciliationInvestmentService.saveReconciliationInvestmentSecurity(security);
				ReconciliationInvestmentSecurityAlias reconciliationInvestmentSecurityAlias = ReconciliationInvestmentSecurityAliasBuilder.aReconciliationInvestmentSecurityAlias()
						.withReconciliationSource(this.reconciliationDefinitionService.getReconciliationSourceByName("Northern"))
						.withAlias("Alias")
						.withDisplayProperty("securitySymbol")
						.withStartDate(DateUtils.toDate("01/01/2018"))
						.withInvestmentSecurity(security)
						.build();
				Assertions.assertNotNull(reconciliationInvestmentSecurityAlias.getInvestmentSecurity());
				this.reconciliationInvestmentService.saveReconciliationInvestmentSecurityAlias(reconciliationInvestmentSecurityAlias);

				reconciliationInvestmentSecurityAlias = ReconciliationInvestmentSecurityAliasBuilder.aReconciliationInvestmentSecurityAlias()
						.withReconciliationSource(this.reconciliationDefinitionService.getReconciliationSourceByName("Northern"))
						.withAlias("Alias")
						.withDisplayProperty("securitySymbol")
						.withStartDate(DateUtils.toDate("01/01/2018"))
						.withInvestmentSecurity(this.reconciliationInvestmentService.getReconciliationInvestmentSecurityBySymbol("LBHYP"))
						.build();
				this.reconciliationInvestmentService.saveReconciliationInvestmentSecurityAlias(reconciliationInvestmentSecurityAlias);
			}
			catch (ValidationException e) {
				MatcherAssert.assertThat(e.getMessage(), IsEqual.equalTo("A security alias already exists with an overlapping date range.  Please either edit the existing alias, or end it and start a new one.<br /><br />Existing alias: ['Alias' -> 'LBHYP']"));
				throw e;
			}
		});
	}
}
