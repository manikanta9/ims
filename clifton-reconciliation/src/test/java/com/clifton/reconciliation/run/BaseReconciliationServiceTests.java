package com.clifton.reconciliation.run;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.ReconciliationDefinitionFieldMapping;
import com.clifton.reconciliation.definition.ReconciliationDefinitionService;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.definition.search.ReconciliationDefinitionSearchForm;
import com.clifton.reconciliation.definition.type.ReconciliationTypeService;
import com.clifton.reconciliation.file.ReconciliationFileDefinition;
import com.clifton.reconciliation.file.ReconciliationFileDefinitionService;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccountAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccountBuilder;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityAlias;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityBuilder;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.rule.ReconciliationRuleService;
import com.clifton.reconciliation.run.data.DataHolder;
import com.clifton.reconciliation.run.data.DataObject;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatus;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationStatusService;
import com.clifton.reconciliation.run.util.ReconciliationRunUtils;
import com.clifton.security.SecurityTestsUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import org.junit.jupiter.api.Assertions;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;


/**
 * @author terrys
 */
@Transactional
public abstract class BaseReconciliationServiceTests extends BaseInMemoryDatabaseTests {

	@Resource
	protected ReconciliationStatusService reconciliationStatusService;

	@Resource
	protected ReconciliationRunService reconciliationRunService;

	@Resource
	protected WorkflowDefinitionService workflowDefinitionService;

	@Resource
	protected ReconciliationDefinitionService reconciliationDefinitionService;

	@Resource
	protected ReconciliationRuleService reconciliationRuleService;

	@Resource
	protected ReconciliationTypeService reconciliationTypeService;

	@Resource
	protected ReconciliationInvestmentService reconciliationInvestmentService;

	@Resource
	protected ReconciliationFileDefinitionService reconciliationFileDefinitionService;

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private SecurityUserService securityUserService;


	protected Status getStatus() {
		Status status = new Status();
		status.setStatusTitle("Reconciliation Results");
		status.setActionPerformed(true);
		status.setMaxDetailCount(1000);
		return status;
	}


	protected WorkflowState reconciliationWorkflowApproved;


	@Override
	protected Class<?> getTestClass() {
		return BaseReconciliationServiceTests.class;
	}


	@Override
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
		//Mock and modify queries because InMemoryDB doesn't support JSON_VALUE in query...
		setReconciliationInvestmentService(Mockito.spy(getReconciliationInvestmentService()));
		String baseQuery = "SELECT rro.ReconciliationRunObjectID FROM ReconciliationRunObject rro WHERE rro.ReconciliationRunID IN (%s)";
		String query = baseQuery + " AND rro.ReconciliationMatchStatusID = ?  AND rro.ReconciliationReconcileStatusID = ?";
		Mockito.doAnswer(invocation -> invocation.getArguments()[0]).when(getReconciliationInvestmentService()).addAccountAliasQueryParams(ArgumentMatchers.any(SqlSelectCommand.class), ArgumentMatchers.any(ReconciliationInvestmentAccountAlias.class));
		Mockito.when(getReconciliationInvestmentService().getAccountAliasQuery(false)).thenReturn(query);
		Mockito.when(getReconciliationInvestmentService().getAccountAliasQuery(true)).thenReturn(baseQuery);
		Mockito.doAnswer(invocation -> invocation.getArguments()[0]).when(getReconciliationInvestmentService()).addSecurityAliasQueryParams(ArgumentMatchers.any(SqlSelectCommand.class), ArgumentMatchers.any(ReconciliationInvestmentSecurityAlias.class));
		Mockito.when(getReconciliationInvestmentService().getSecurityAliasQuery(false)).thenReturn(query);
		Mockito.when(getReconciliationInvestmentService().getSecurityAliasQuery(true)).thenReturn(baseQuery);

		List<WorkflowState> workflowStates = this.workflowDefinitionService.getWorkflowStateListByName("Approved");
		List<WorkflowState> stateList = CollectionUtils.getStream(workflowStates)
				.filter(s -> Objects.equals(s.getWorkflow().getName(), "Reconciliation Definition Workflow"))
				.filter(s -> Objects.equals(s.getName(), "Approved"))
				.collect(Collectors.toList());
		Assertions.assertEquals(1, stateList.size());
		this.reconciliationWorkflowApproved = stateList.get(0);

		SecurityTestsUtils.setSecurityContextPrincipal(this.contextHandler, this.securityUserService, "regularUser");
		SecurityUser user = new SecurityUser();
		user.setId(new Short("1"));
		user.setUserName("TestUser");
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationReconcileStatus getReconcileStatus(String name) {
		return this.reconciliationStatusService.getReconciliationReconcileStatusByName(name);
	}


	public ReconciliationMatchStatus getMatchStatus(String name) {
		return this.reconciliationStatusService.getReconciliationMatchStatusByName(name);
	}


	public enum GenerationTypes {
		PRIMARY, SECONDARY, ALL
	}


	public DataHolder getDataHolder(ReconciliationDefinition definition, ReconciliationInvestmentAccount account, ReconciliationInvestmentSecurity security, GenerationTypes generationType) {
		List<DataObject> dataObjectList = new ArrayList<>();
		switch (generationType) {
			case PRIMARY:
				dataObjectList.add(generateDataObject(definition, definition.getPrimarySource(), account, security));
				break;
			case SECONDARY:
				dataObjectList.add(generateDataObject(definition, definition.getSecondarySource(), account, security));
				break;
			case ALL:
				dataObjectList.add(generateDataObject(definition, definition.getPrimarySource(), account, security));
				dataObjectList.add(generateDataObject(definition, definition.getSecondarySource(), account, security));
				break;
		}
		DataHolder dataHolder = new DataHolder();
		dataHolder.setDataObjects(dataObjectList);
		return dataHolder;
	}


	public DataObject generateDataObject(ReconciliationDefinition definition, ReconciliationSource source, ReconciliationInvestmentAccount account, ReconciliationInvestmentSecurity security) {
		Map<String, Object> data = new HashMap<>();
		Map<String, Object> rowData = new HashMap<>();
		List<Map<String, Object>> rowDataList = new ArrayList<>();
		for (ReconciliationDefinitionField definitionField : CollectionUtils.getIterable(definition.getReconciliationDefinitionFieldList())) {
			String fieldName = definitionField.getField().getName();
			DataTypes dataType = ObjectUtils.coalesce(definitionField.getDataType(), definitionField.getField().getDefaultDataType());
			Object fieldValue = DataTypeNameUtils.convertObjectToDataTypeName(generateData(dataType), dataType.getTypeName());
			switch (fieldName) {
				case "Holding Account":
					data.put(fieldName, account.getAccountNumber());
					break;
				case "Investment Security":
					data.put(fieldName, security.getSecuritySymbol());
					break;
				default:
					data.put(fieldName, fieldValue);
					break;
			}
			for (ReconciliationDefinitionFieldMapping fieldMapping : CollectionUtils.getIterable(definitionField.getReconciliationDefinitionFieldMappingList())) {
				if (CompareUtils.isEqual(source, fieldMapping.getSource())) {
					String fieldMappingName = fieldMapping.getName();
					switch (fieldName) {
						case "Holding Account":
							rowData.put(fieldMappingName, account.getAccountNumber());
							break;
						case "Investment Security":
							rowData.put(fieldMappingName, security.getSecuritySymbol());
							break;
						default:
							rowData.put(fieldMappingName, fieldValue);
							break;
					}
				}
			}
		}
		rowDataList.add(rowData);
		return generateDataObject(source, data, rowDataList);
	}


	/**
	 * Generation for supported dataTypes for reconciliation data.
	 */
	public Object generateData(DataTypes dataType) {
		Random r = new Random();
		switch (dataType) {
			case EXCHANGE_RATE:
			case MONEY:
			case MONEY4:
			case PRICE:
			case QUANTITY:
				return MathUtils.add(new BigDecimal(r.nextFloat()), (MathUtils.multiply(new BigDecimal("100000"), new BigDecimal(r.nextFloat()))));
			case INT:
				return r.nextInt();
			case DATE:
				return DateUtils.addDays(new Date(), r.nextInt(10));
			case BIT:
				return r.nextBoolean();
			default:
				return generateRandomWords();
		}
	}


	public static String generateRandomWords() {
		StringBuilder randomString = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 2; i++) {
			char[] word = new char[random.nextInt(8) + 3]; // words of length 3 through 10. (1 and 2 letter words are boring.)
			for (int j = 0; j < word.length; j++) {
				word[j] = (char) ('a' + random.nextInt(26));
			}
			randomString.append(word);
		}
		return randomString.toString();
	}


	/**
	 * This method allows for data to be passed in - allows better control for tests where auto-generated data isn't ideal
	 */
	public DataObject generateDataObject(ReconciliationSource source, Map<String, Object> data, List<Map<String, Object>> rowData) {
		ReconciliationFileDefinition fileDefinition = new ReconciliationFileDefinition();
		fileDefinition.setName("FileDefinition for " + source.getShortName());
		fileDefinition.setDescription(fileDefinition.getName());
		fileDefinition.setFileName("i_have_a_stupid_filename.true");
		fileDefinition.setSource(source);

		DataObject dataObject = new DataObject();
		dataObject.setFileDefinitionId(this.reconciliationFileDefinitionService.saveReconciliationFileDefinition(fileDefinition).getId());
		dataObject.setSourceId(source.getId());
		dataObject.setData(data);
		dataObject.setRowData(rowData);
		return dataObject;
	}


	public ReconciliationRun createReconciliationRun(String definitionName, Date runDate) {
		ReconciliationRun run = ReconciliationRunBuilder.aReconciliationRun()
				.withReconciliationDefinition(getReconciliationDefinition(definitionName))
				.withReconciliationReconcileStatus(getReconcileStatus(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()))
				.withRunDate(runDate)
				.withRunDetails(getStatus())
				.build();
		return this.reconciliationRunService.saveReconciliationRun(run);
	}


	public ReconciliationDefinition getReconciliationDefinition(String definitionName) {
		ReconciliationDefinitionSearchForm searchForm = new ReconciliationDefinitionSearchForm();
		searchForm.setName(definitionName);
		return this.reconciliationDefinitionService.getReconciliationDefinitionList(searchForm).stream()
				.filter(d -> definitionName.equals(d.getName())).findFirst()
				.orElseThrow(() -> new RuntimeException("Could not find [" + definitionName + "]"));
	}


	public ReconciliationInvestmentAccount createReconciliationInvestmentAccount(Integer identifier, String accountName, String accountNumber) {
		ReconciliationInvestmentAccount account = ReconciliationInvestmentAccountBuilder.aReconciliationInvestmentAccount()
				.withAccountIdentifier(identifier)
				.withAccountName(accountName)
				.withAccountNumber(accountNumber)
				.withWorkflowState("Active")
				.build();
		return identifier != null ? this.reconciliationInvestmentService.saveReconciliationInvestmentAccount(account) : account;
	}


	public ReconciliationInvestmentSecurity createReconciliationInvestmentSecurity(Integer identifier, String name, String symbol, String typeName) {
		ReconciliationInvestmentSecurity security = ReconciliationInvestmentSecurityBuilder.aReconciliationInvestmentSecurity()
				.withSecurityIdentifier(identifier)
				.withSecurityName(name)
				.withSecuritySymbol(symbol)
				.withSecurityTypeName(typeName)
				.build();
		return identifier != null ? this.reconciliationInvestmentService.saveReconciliationInvestmentSecurity(security) : security;
	}


	public ReconciliationRunObject createReconciliationRunObject(ReconciliationRun run, ReconciliationSource source, ReconciliationInvestmentAccount account, ReconciliationInvestmentSecurity security, DataHolder dataHolder) {
		return createReconciliationRunObject(run, source, account, security, dataHolder, true);
	}


	public ReconciliationRunObject createReconciliationRunObject(ReconciliationRun run, ReconciliationSource source, ReconciliationInvestmentAccount account, ReconciliationInvestmentSecurity security, DataHolder dataHolder, boolean save) {
		Map<String, Object> normalizedRowData = ReconciliationRunUtils.normalizeRowData(run, dataHolder.getDataObjects().get(0).getRowData().get(0), source);
		dataHolder.getDataObjects().get(0).setData(normalizedRowData);
		ReconciliationRunObject reconciliationRunObject = ReconciliationRunObjectBuilder.aReconciliationRunObject()
				.withIdentifier(ReconciliationRunUtils.createRunObjectIdentifier(run.getReconciliationDefinition().getReconciliationDefinitionFieldList(), normalizedRowData))
				.withReconciliationRun(run)
				.withReconciliationInvestmentAccount(account.getAccountIdentifier() != null ? account : null)
				.withReconciliationInvestmentSecurity(security.getSecurityIdentifier() != null ? security : null)
				.withReconciliationMatchStatus(getMatchStatus(ReconciliationMatchStatusTypes.NOT_MATCHED.name()))
				.withReconciliationReconcileStatus(getReconcileStatus(ReconciliationReconcileStatusTypes.NOT_RECONCILED.name()))
				.withDataHolder(dataHolder)
				.build();
		return save ? this.reconciliationRunService.saveReconciliationRunObject(reconciliationRunObject) : reconciliationRunObject;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationStatusService getReconciliationStatusService() {
		return this.reconciliationStatusService;
	}


	public void setReconciliationStatusService(ReconciliationStatusService reconciliationStatusService) {
		this.reconciliationStatusService = reconciliationStatusService;
	}


	public ReconciliationRunService getReconciliationRunService() {
		return this.reconciliationRunService;
	}


	public void setReconciliationRunService(ReconciliationRunService reconciliationRunService) {
		this.reconciliationRunService = reconciliationRunService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public ReconciliationDefinitionService getReconciliationDefinitionService() {
		return this.reconciliationDefinitionService;
	}


	public void setReconciliationDefinitionService(ReconciliationDefinitionService reconciliationDefinitionService) {
		this.reconciliationDefinitionService = reconciliationDefinitionService;
	}


	public ReconciliationRuleService getReconciliationRuleService() {
		return this.reconciliationRuleService;
	}


	public void setReconciliationRuleService(ReconciliationRuleService reconciliationRuleService) {
		this.reconciliationRuleService = reconciliationRuleService;
	}


	public ReconciliationTypeService getReconciliationTypeService() {
		return this.reconciliationTypeService;
	}


	public void setReconciliationTypeService(ReconciliationTypeService reconciliationTypeService) {
		this.reconciliationTypeService = reconciliationTypeService;
	}


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}


	public ReconciliationFileDefinitionService getReconciliationFileDefinitionService() {
		return this.reconciliationFileDefinitionService;
	}


	public void setReconciliationFileDefinitionService(ReconciliationFileDefinitionService reconciliationFileDefinitionService) {
		this.reconciliationFileDefinitionService = reconciliationFileDefinitionService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}

