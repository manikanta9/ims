package com.clifton.reconciliation.run;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionBuilder;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.definition.ReconciliationSourceBuilder;
import com.clifton.reconciliation.definition.group.ReconciliationDefinitionGroup;
import com.clifton.reconciliation.definition.group.ReconciliationDefinitionGroupBuilder;
import com.clifton.reconciliation.definition.type.ReconciliationType;
import com.clifton.reconciliation.definition.type.ReconciliationTypeBuilder;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccountBuilder;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityBuilder;
import com.clifton.reconciliation.run.data.DataError;
import com.clifton.reconciliation.run.data.DataHolder;
import com.clifton.reconciliation.run.data.DataNote;
import com.clifton.reconciliation.run.data.DataNoteTypes;
import com.clifton.reconciliation.run.data.DataObject;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatus;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatusBuilder;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusBuilder;
import com.clifton.workflow.definition.WorkflowStateBuilder;
import com.clifton.workflow.definition.WorkflowStatusBuilder;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author terrys
 */
public abstract class BaseReconciliationRunObjectJsonTests {

	protected List<ReconciliationRunObject> getAllReconciliationRunObjects() {
		return Arrays.asList(
				this.reconciliationRunObject_110.get(),
				this.reconciliationRunObject_210.get(),
				this.reconciliationRunObject_220.get(),
				this.reconciliationRunObject_310.get(),
				this.reconciliationRunObject_330.get(),
				this.reconciliationRunObject_340.get(),
				this.reconciliationRunObject_350.get(),
				this.reconciliationRunObject_360.get(),
				this.reconciliationRunObject_370.get(),
				this.reconciliationRunObject_380.get()
		);
	}


	protected Lazy<ReconciliationInvestmentAccount> reconciliationInvestmentAccount_001 = new Lazy<>(() ->
			ReconciliationInvestmentAccountBuilder.aReconciliationInvestmentAccount()
					.withAccountName("Test Account 001")
					.withAccountIdentifier(1001)
					.withAccountNumber("ACCT-1001")
					.build()
	);

	protected Lazy<ReconciliationInvestmentAccount> reconciliationInvestmentAccount_003 = new Lazy<>(() ->
			ReconciliationInvestmentAccountBuilder.aReconciliationInvestmentAccount()
					.withAccountName("Test Account 003")
					.withAccountIdentifier(1003)
					.withAccountNumber("ACCT-1003")
					.build()
	);


	protected Lazy<ReconciliationType> activityReconciliationType = new Lazy<>(() ->
			ReconciliationTypeBuilder.aReconciliationType()
					.withName("Activity Reconciliation")
					.withDescription("Category for all definitions used for position activity reconciliation.")
					.build()
	);

	protected Lazy<ReconciliationSource> imsReconciliationSource = new Lazy<>(() ->
			ReconciliationSourceBuilder.aReconciliationSource()
					.withName(ReconciliationSource.DEFAULT_SOURCE)
					.build()
	);

	protected Lazy<ReconciliationSource> citiReconciliationSource = new Lazy<>(() ->
			ReconciliationSourceBuilder.aReconciliationSource()
					.withName("Citi")
					.build()
	);

	protected Lazy<ReconciliationDefinitionGroup> reconciliationDefinitionGroup_1 = new Lazy<>(() ->
			ReconciliationDefinitionGroupBuilder.aReconciliationDefinitionGroup()
					.withName("Westport Position and Cash")
					.withDescription("Groups all Westport Position and Cash definitions that require unlock events to be send.")
					.withEventTrigger(true)
					.build()
	);


	protected Lazy<ReconciliationDefinition> reconciliationDefinition_activity_1 = new Lazy<>(() ->
			ReconciliationDefinitionBuilder.aReconciliationDefinition()
					.withDefinitionGroup(this.reconciliationDefinitionGroup_1.get())
					.withName("Activity Reconciliation")
					.withDescription("Activity Reconciliation")
					.withType(this.activityReconciliationType.get())
					.withPrimarySource(this.imsReconciliationSource.get())
					.withSecondarySource(this.citiReconciliationSource.get())
					.withWorkflowState(WorkflowStateBuilder.createActive().toWorkflowState())
					.withWorkflowStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus())
					.build()
	);

	protected Lazy<ReconciliationReconcileStatus> notReconciliationReconcileStatus = new Lazy<>(() ->
			ReconciliationReconcileStatusBuilder.aReconciliationReconcileStatus()
					.withName("NOT_RECONCILED")
					.withLabel("Not Reconciled")
					.withDescription("Indicates reconciliation failed for this data.")
					.withSortOrder((short) 10)
					.build()
	);

	protected Lazy<Status> status = new Lazy<>(() -> {
		Status status = new Status();
		status.setStatusTitle("Reconciliation Results");
		status.setActionPerformed(true);
		status.setMaxDetailCount(1000);
		return status;
	});

	protected Lazy<List<DataNote>> dataNotes = new Lazy<>(() -> {
		DataNote note_1 = new DataNote();
		note_1.setNote("Test of a note.");
		note_1.setType(DataNoteTypes.OTHER);
		DataNote note_2 = new DataNote();
		note_2.setNote("Very good note.");
		note_2.setType(DataNoteTypes.OTHER);
		return Arrays.asList(
				note_1,
				note_2
		);
	});

	protected Lazy<List<DataError>> dataErrors = new Lazy<>(() -> {
		DataError error_1 = new DataError();
		error_1.setFieldName("Commissions");
		error_1.setDiff(new BigDecimal("-10.44"));
		DataError error_2 = new DataError();
		error_2.setFieldName("Holding Account");
		return Arrays.asList(
				error_1,
				error_2
		);
	});

	protected Lazy<ReconciliationRun> reconciliationRun_101 = new Lazy<>(() ->
			ReconciliationRunBuilder.aReconciliationRun()
					.withReconciliationDefinition(this.reconciliationDefinition_activity_1.get())
					.withReconciliationReconcileStatus(this.notReconciliationReconcileStatus.get())
					.withRunDate(DateUtils.toDate("03/01/2018"))
					.withRunDetails(this.status.get())
					.build()
	);

	protected Lazy<ReconciliationRun> reconciliationRun_102 = new Lazy<>(() ->
			ReconciliationRunBuilder.aReconciliationRun()
					.withReconciliationDefinition(this.reconciliationDefinition_activity_1.get())
					.withReconciliationReconcileStatus(this.notReconciliationReconcileStatus.get())
					.withRunDate(DateUtils.toDate("03/02/2018"))
					.withRunDetails(this.status.get())
					.build()
	);

	protected Lazy<ReconciliationRun> reconciliationRun_103 = new Lazy<>(() ->
			ReconciliationRunBuilder.aReconciliationRun()
					.withReconciliationDefinition(this.reconciliationDefinition_activity_1.get())
					.withReconciliationReconcileStatus(this.notReconciliationReconcileStatus.get())
					.withRunDate(DateUtils.toDate("03/03/2018"))
					.withRunDetails(this.status.get())
					.build()
	);


	protected Lazy<ReconciliationInvestmentSecurity> reconciliationInvestmentSecurity_stock_01 = new Lazy<>(() ->
			ReconciliationInvestmentSecurityBuilder.aReconciliationInvestmentSecurity()
					.withSecuritySymbol("STK1")
					.withSecurityName("Stock 001")
					.withSecurityIdentifier(101)
					.withSecurityTypeName("Stocks")
					.build()
	);

	protected Lazy<ReconciliationInvestmentSecurity> reconciliationInvestmentSecurity_stock_02 = new Lazy<>(() ->
			ReconciliationInvestmentSecurityBuilder.aReconciliationInvestmentSecurity()
					.withSecuritySymbol("STK2")
					.withSecurityName("Stock 002")
					.withSecurityIdentifier(102)
					.withSecurityTypeName("Stocks")
					.build()
	);


	protected Lazy<ReconciliationInvestmentSecurity> reconciliationInvestmentSecurity_stock_03 = new Lazy<>(() ->
			ReconciliationInvestmentSecurityBuilder.aReconciliationInvestmentSecurity()
					.withSecuritySymbol("STK3")
					.withSecurityName("Stock 003")
					.withSecurityIdentifier(103)
					.withSecurityTypeName("Stocks")
					.build()
	);

	protected Lazy<ReconciliationMatchStatus> notReconciliationMatchStatus = new Lazy<>(() ->
			ReconciliationMatchStatusBuilder.aReconciliationMatchStatus()
					.withName("NOT_MATCHED")
					.withLabel("Not Matched")
					.withDescription("Indicates data has not yet been loaded from all sources.")
					.withSortOrder((short) 30)
					.build()
	);

	protected Lazy<ReconciliationMatchStatus> matchedReconciliationMatchStatus = new Lazy<>(() ->
			ReconciliationMatchStatusBuilder.aReconciliationMatchStatus()
					.withName("MATCHED")
					.withLabel("Matched")
					.withDescription("Indicates data has been loaded from all sources.")
					.withMatched(true)
					.withSortOrder((short) 10)
					.build()
	);

	protected Lazy<ReconciliationMatchStatus> manualMatchReconciliationMatchStatus = new Lazy<>(() ->
			ReconciliationMatchStatusBuilder.aReconciliationMatchStatus()
					.withName("MANUALLY_MATCHED")
					.withLabel("Manually Matched")
					.withDescription("Indicates data has been manually matched for all sources.")
					.withMatched(true)
					.withManual(true)
					.withSortOrder((short) 20)
					.build()
	);

	protected Lazy<DataHolder> dataHolder_110 = new Lazy<>(() -> {
		Map<String, Object> data_110 = new HashMap<>();
		data_110.put("Holding Account", "ACCT-1001");
		data_110.put("Realized Gain/Loss", "0");
		data_110.put("Investment Security", "STK3");
		data_110.put("Closed Quantity", "1");
		data_110.put("Commissions", "0");

		Map<String, Object> rowMap_110 = new HashMap<>();
		rowMap_110.put("Today Closed Qty", "1");
		rowMap_110.put("Base Realized", "0");
		rowMap_110.put("Security", "STK1");
		rowMap_110.put("Holding Account", "ACCT-1001");
		rowMap_110.put("Base Commission", "0");

		List<Map<String, Object>> rowData_110 = Collections.singletonList(rowMap_110);

		DataObject dataObject_110 = new DataObject();
		dataObject_110.setFileDefinitionId(1);
		dataObject_110.setSourceId((short) 1);
		dataObject_110.setData(data_110);
		dataObject_110.setRowData(rowData_110);

		DataHolder dataHolder = new DataHolder();
		dataHolder.setDataObjects(
				CollectionUtils.createList(dataObject_110)
		);
		return dataHolder;
	});


	protected Lazy<DataHolder> dataHolder_210 = new Lazy<>(() -> {
		Map<String, Object> data_210 = new HashMap<>();
		data_210.put("Holding Account", "ACCT-1001");
		data_210.put("Realized Gain/Loss", "0");
		data_210.put("Investment Security", "STK2");
		data_210.put("Closed Quantity", "0");
		data_210.put("Commissions", "0");

		Map<String, Object> rowMap_210 = new HashMap<>();
		rowMap_210.put("Realized P and L", "0.00");
		rowMap_210.put("Lots Movement", "0");
		rowMap_210.put("Bloomberg", "STK2");
		rowMap_210.put("A/C Ref", "ACCT-1001");
		rowMap_210.put("Total Fees", "0.00");

		List<Map<String, Object>> rowData_210 = Collections.singletonList(rowMap_210);

		DataObject dataObject_210 = new DataObject();
		dataObject_210.setFileDefinitionId(2);
		dataObject_210.setSourceId((short) 15);
		dataObject_210.setData(data_210);
		dataObject_210.setRowData(rowData_210);

		DataHolder dataHolder_210 = new DataHolder();
		dataHolder_210.setDataObjects(
				CollectionUtils.createList(dataObject_210)
		);
		return dataHolder_210;
	});


	protected Lazy<DataHolder> dataHolder_310 = new Lazy<>(() -> {
		Map<String, Object> data_310 = new HashMap<>();
		data_310.put("Holding Account", "ACCT-1003");
		data_310.put("Realized Gain/Loss", "-10637.5");
		data_310.put("Investment Security", "STK1");
		data_310.put("Closed Quantity", "2");
		data_310.put("Commissions", "5.22");

		Map<String, Object> rowMap_310 = new HashMap<>();
		rowMap_310.put("Today Closed Qty", "2");
		rowMap_310.put("Base Realized", "-10637.5");
		rowMap_310.put("Security", "STK1");
		rowMap_310.put("Holding Account", "ACCT-1003");
		rowMap_310.put("Base Commission", "5.22");

		List<Map<String, Object>> rowData_310 = Collections.singletonList(rowMap_310);

		DataObject dataObject_310 = new DataObject();
		dataObject_310.setFileDefinitionId(1);
		dataObject_310.setSourceId((short) 1);
		dataObject_310.setData(data_310);
		dataObject_310.setRowData(rowData_310);

		Map<String, Object> data_320 = new HashMap<>();
		data_320.put("Holding Account", "ACCT-1003");
		data_320.put("Realized Gain/Loss", "-10637.5");
		data_320.put("Investment Security", "STK1");
		data_320.put("Closed Quantity", "2");
		data_320.put("Commissions", "5.22");

		Map<String, Object> rowMap_320 = new HashMap<>();
		rowMap_320.put("Realized P and L", "-10637.50");
		rowMap_320.put("Lots Movement", "2");
		rowMap_320.put("Bloomberg", "STK1");
		rowMap_320.put("A/C Ref", "ACCT-1003");
		rowMap_320.put("Total Fees", "-5.22");

		List<Map<String, Object>> rowData_320 = Collections.singletonList(rowMap_320);

		DataObject dataObject_320 = new DataObject();
		dataObject_320.setFileDefinitionId(2);
		dataObject_320.setSourceId((short) 15);
		dataObject_320.setData(data_320);
		dataObject_320.setRowData(rowData_320);

		DataHolder dataHolder_310 = new DataHolder();
		dataHolder_310.setDataObjects(
				CollectionUtils.createList(
						dataObject_310,
						dataObject_320
				)
		);

		return dataHolder_310;
	});

	protected Lazy<DataObject> dataObject_350 = new Lazy<>(() -> {
		Map<String, Object> data_350 = new HashMap<>();
		data_350.put("Holding Account", "ACCT-1003");
		data_350.put("Realized Gain/Loss", "-10637.5");
		data_350.put("Investment Security", "STK1");
		data_350.put("Closed Quantity", "2");
		data_350.put("Commissions", "5.22");

		Map<String, Object> rowMap_350 = new HashMap<>();
		rowMap_350.put("Today Closed Qty", "2");
		rowMap_350.put("Base Realized", "-10637.5");
		rowMap_350.put("Security", "STK1");
		rowMap_350.put("Holding Account", "ACCT-1003");
		rowMap_350.put("Base Commission", "5.22");

		List<Map<String, Object>> rowData_350 = Collections.singletonList(rowMap_350);

		DataObject dataObject_350 = new DataObject();
		dataObject_350.setFileDefinitionId(1);
		dataObject_350.setSourceId((short) 1);
		dataObject_350.setData(data_350);
		dataObject_350.setRowData(rowData_350);

		return dataObject_350;
	});

	protected Lazy<DataObject> dataObject_360 = new Lazy<>(() -> {
		Map<String, Object> data_360 = new HashMap<>();
		data_360.put("Holding Account", "ACCT-1003");
		data_360.put("Realized Gain/Loss", "-10637.5");
		data_360.put("Investment Security", "STK1");
		data_360.put("Closed Quantity", "2");
		data_360.put("Commissions", "5.22");

		Map<String, Object> rowMap_360 = new HashMap<>();
		rowMap_360.put("Realized P and L", "-10637.50");
		rowMap_360.put("Lots Movement", "2");
		rowMap_360.put("Bloomberg", "STK1");
		rowMap_360.put("A/C Ref", "ACCT-1003");
		rowMap_360.put("Total Fees", "-5.22");

		List<Map<String, Object>> rowData_360 = Collections.singletonList(rowMap_360);

		DataObject dataObject_360 = new DataObject();
		dataObject_360.setFileDefinitionId(2);
		dataObject_360.setSourceId((short) 15);
		dataObject_360.setData(data_360);
		dataObject_360.setRowData(rowData_360);

		return dataObject_360;
	});

	protected Lazy<DataHolder> dataHolder_330 = new Lazy<>(() -> {
		DataHolder dataHolder_330 = new DataHolder();
		dataHolder_330.setDataObjects(
				CollectionUtils.createList(
						this.dataObject_350.get(),
						this.dataObject_360.get()
				)
		);
		return dataHolder_330;
	});

	protected Lazy<DataHolder> dataHolder_340 = new Lazy<>(() -> {
		DataHolder dataHolder_340 = new DataHolder();
		dataHolder_340.setDataObjects(
				CollectionUtils.createList(
						this.dataObject_350.get(),
						this.dataObject_360.get()
				)
		);
		dataHolder_340.setErrors(
				this.dataErrors.get()
		);
		return dataHolder_340;
	});

	protected Lazy<DataHolder> dataHolder_350 = new Lazy<>(() -> {
		DataHolder dataHolder_350 = new DataHolder();
		dataHolder_350.setDataObjects(
				CollectionUtils.createList(
						this.dataObject_350.get(),
						this.dataObject_360.get()
				)
		);
		dataHolder_350.setNotes(
				this.dataNotes.get()
		);
		return dataHolder_350;
	});

	protected Lazy<DataHolder> dataHolder_360 = new Lazy<>(() -> {
		DataHolder dataHolder_360 = new DataHolder();
		dataHolder_360.setDataObjects(
				CollectionUtils.createList(
						this.dataObject_350.get(),
						this.dataObject_360.get()
				)
		);
		dataHolder_360.setNotes(
				this.dataNotes.get()
		);
		return dataHolder_360;
	});

	protected Lazy<DataHolder> dataHolder_370 = new Lazy<>(() -> {
		DataHolder dataHolder_370 = new DataHolder();
		dataHolder_370.setDataObjects(
				CollectionUtils.createList(
						this.dataObject_370_1.get(),
						this.dataObject_370_2.get()
				)
		);
		dataHolder_370.setNotes(
				this.dataNotes.get()
		);
		return dataHolder_370;
	});

	protected Lazy<DataObject> dataObject_370_1 = new Lazy<>(() -> {
		Map<String, Object> data_370 = new HashMap<>();
		data_370.put("Holding Account", "ACCT-1003");
		data_370.put("Realized Gain/Loss", "-10637.5");
		data_370.put("Investment Security", "STK1");
		data_370.put("Closed Quantity", "2");
		data_370.put("Commissions", "5.22");

		Map<String, Object> rowMap_370_1 = new HashMap<>();
		rowMap_370_1.put("Realized P and L", "-10637.50");
		rowMap_370_1.put("Lots Movement", "2");
		rowMap_370_1.put("Bloomberg", "STK1");
		rowMap_370_1.put("A/C Ref", "ACCT-1003");
		rowMap_370_1.put("Total Fees", "-2.22");

		Map<String, Object> rowMap_370_2 = new HashMap<>();
		rowMap_370_2.put("Realized P and L", "-32.50");
		rowMap_370_2.put("Lots Movement", "2");
		rowMap_370_2.put("Bloomberg", "STK1");
		rowMap_370_2.put("A/C Ref", "ACCT-1003");
		rowMap_370_2.put("Total Fees", "-85.22");

		Map<String, Object> rowMap_370_3 = new HashMap<>();
		rowMap_370_3.put("Realized P and L", "-3.50");
		rowMap_370_3.put("Lots Movement", "2");
		rowMap_370_3.put("Bloomberg", "STK1");
		rowMap_370_3.put("A/C Ref", "ACCT-1003");
		rowMap_370_3.put("Total Fees", "-55.22");

		Map<String, Object> rowMap_370_4 = new HashMap<>();
		rowMap_370_4.put("Realized P and L", "-7.50");
		rowMap_370_4.put("Lots Movement", "2");
		rowMap_370_4.put("Bloomberg", "STK1");
		rowMap_370_4.put("A/C Ref", "ACCT-1003");
		rowMap_370_4.put("Total Fees", "-45.22");

		Map<String, Object> rowMap_370_5 = new HashMap<>();
		rowMap_370_5.put("Realized P and L", "-93.50");
		rowMap_370_5.put("Lots Movement", "2");
		rowMap_370_5.put("Bloomberg", "STK1");
		rowMap_370_5.put("A/C Ref", "ACCT-1003");
		rowMap_370_5.put("Total Fees", "-5.12");

		Map<String, Object> rowMap_370_6 = new HashMap<>();
		rowMap_370_6.put("Realized P and L", "-94.50");
		rowMap_370_6.put("Lots Movement", "2");
		rowMap_370_6.put("Bloomberg", "STK1");
		rowMap_370_6.put("A/C Ref", "ACCT-1003");
		rowMap_370_6.put("Total Fees", "-5.22");

		Map<String, Object> rowMap_370_7 = new HashMap<>();
		rowMap_370_7.put("Realized P and L", "-23.50");
		rowMap_370_7.put("Lots Movement", "2");
		rowMap_370_7.put("Bloomberg", "STK1");
		rowMap_370_7.put("A/C Ref", "ACCT-1003");
		rowMap_370_7.put("Total Fees", "-5.82");

		List<Map<String, Object>> rowData_370_1 = Arrays.asList(
				rowMap_370_1,
				rowMap_370_2,
				rowMap_370_3,
				rowMap_370_4,
				rowMap_370_5,
				rowMap_370_6,
				rowMap_370_7
		);

		DataObject dataObject_370_1 = new DataObject();
		dataObject_370_1.setFileDefinitionId(2);
		dataObject_370_1.setSourceId((short) 15);
		dataObject_370_1.setData(data_370);
		dataObject_370_1.setRowData(rowData_370_1);

		return dataObject_370_1;
	});

	protected Lazy<DataHolder> dataHolder_370_1 = new Lazy<>(() -> {

		DataHolder dataHolder_370_1 = new DataHolder();
		dataHolder_370_1.setDataObjects(
				CollectionUtils.createList(
						this.dataObject_370_1.get()
				)
		);

		dataHolder_370_1.setNotes(
				this.dataNotes.get()
		);
		return dataHolder_370_1;
	});

	protected Lazy<DataObject> dataObject_370_2 = new Lazy<>(() -> {
		Map<String, Object> data_370 = new HashMap<>();
		data_370.put("Holding Account", "ACCT-1003");
		data_370.put("Realized Gain/Loss", "-10637.5");
		data_370.put("Investment Security", "STK1");
		data_370.put("Closed Quantity", "2");
		data_370.put("Commissions", "5.22");

		Map<String, Object> rowMap_370_1 = new HashMap<>();
		rowMap_370_1.put("Realized P and L", "-10637.50");
		rowMap_370_1.put("Lots Movement", "2");
		rowMap_370_1.put("Bloomberg", "STK1");
		rowMap_370_1.put("A/C Ref", "ACCT-1003");
		rowMap_370_1.put("Total Fees", "-5.22");

		List<Map<String, Object>> rowData_370_2 = Collections.singletonList(
				rowMap_370_1
		);

		DataObject dataObject_370_2 = new DataObject();
		dataObject_370_2.setFileDefinitionId(2);
		dataObject_370_2.setSourceId((short) 15);
		dataObject_370_2.setData(data_370);
		dataObject_370_2.setRowData(rowData_370_2);

		return dataObject_370_2;
	});


	protected Lazy<DataHolder> dataHolder_370_2 = new Lazy<>(() -> {
		DataHolder dataHolder_370_2 = new DataHolder();
		dataHolder_370_2.setDataObjects(
				CollectionUtils.createList(
						this.dataObject_370_2.get()
				)
		);
		dataHolder_370_2.setNotes(
				this.dataNotes.get()
		);
		return dataHolder_370_2;
	});

	protected Lazy<DataHolder> dataHolder_380 = new Lazy<>(() -> {
		DataHolder dataHolder_380 = new DataHolder();
		dataHolder_380.setDataObjects(
				CollectionUtils.createList(
						this.dataObject_370_1.get(),
						this.dataObject_370_2.get()
				)
		);
		dataHolder_380.setNotes(
				this.dataNotes.get()
		);
		dataHolder_380.setErrors(
				this.dataErrors.get()
		);
		return dataHolder_380;
	});

	protected Lazy<ReconciliationRunObject> reconciliationRunObject_110 = new Lazy<>(() ->
			ReconciliationRunObjectBuilder.aReconciliationRunObject()
					.withIdentifier("ACCT-1001_STK1")
					.withReconciliationRun(
							this.reconciliationRun_101.get()
					)
					.withReconciliationInvestmentAccount(this.reconciliationInvestmentAccount_001.get())
					.withReconciliationInvestmentSecurity(this.reconciliationInvestmentSecurity_stock_01.get())
					.withReconciliationReconcileStatus(this.notReconciliationReconcileStatus.get())
					.withReconciliationMatchStatus(this.notReconciliationMatchStatus.get())
					.withDataHolder(this.dataHolder_110.get())
					.build()
	);

	protected Lazy<ReconciliationRunObject> reconciliationRunObject_210 = new Lazy<>(() ->
			ReconciliationRunObjectBuilder.aReconciliationRunObject()
					.withIdentifier("ACCT-1001_STK2")
					.withReconciliationRun(
							this.reconciliationRun_101.get()
					)
					.withReconciliationInvestmentAccount(this.reconciliationInvestmentAccount_001.get())
					.withReconciliationInvestmentSecurity(this.reconciliationInvestmentSecurity_stock_02.get())
					.withReconciliationReconcileStatus(this.notReconciliationReconcileStatus.get())
					.withReconciliationMatchStatus(this.notReconciliationMatchStatus.get())
					.withDataHolder(this.dataHolder_210.get())
					.build()
	);


	protected Lazy<ReconciliationRunObject> reconciliationRunObject_220 = new Lazy<>(() ->
			ReconciliationRunObjectBuilder.aReconciliationRunObject()
					.withIdentifier("ACCT-1001_STK3")
					.withReconciliationRun(
							this.reconciliationRun_101.get()
					)
					.withReconciliationInvestmentAccount(this.reconciliationInvestmentAccount_001.get())
					.withReconciliationInvestmentSecurity(this.reconciliationInvestmentSecurity_stock_03.get())
					.withReconciliationReconcileStatus(this.notReconciliationReconcileStatus.get())
					.withReconciliationMatchStatus(this.notReconciliationMatchStatus.get())
					.withDataHolder(this.dataHolder_210.get())
					.build()
	);

	protected Lazy<ReconciliationRunObject> reconciliationRunObject_310 = new Lazy<>(() ->
			ReconciliationRunObjectBuilder.aReconciliationRunObject()
					.withIdentifier("ACCT-1003_STK1")
					.withReconciliationRun(
							this.reconciliationRun_101.get()
					)
					.withReconciliationInvestmentAccount(this.reconciliationInvestmentAccount_003.get())
					.withReconciliationInvestmentSecurity(this.reconciliationInvestmentSecurity_stock_01.get())
					.withReconciliationReconcileStatus(this.notReconciliationReconcileStatus.get())
					.withReconciliationMatchStatus(this.matchedReconciliationMatchStatus.get())
					.withDataHolder(this.dataHolder_310.get())
					.build()
	);


	protected Lazy<ReconciliationRunObject> reconciliationRunObject_330 = new Lazy<>(() ->
			ReconciliationRunObjectBuilder.aReconciliationRunObject()
					.withIdentifier("ACCT-1003_STK1")
					.withReconciliationRun(
							this.reconciliationRun_102.get()
					)
					.withReconciliationInvestmentAccount(this.reconciliationInvestmentAccount_003.get())
					.withReconciliationInvestmentSecurity(this.reconciliationInvestmentSecurity_stock_01.get())
					.withReconciliationReconcileStatus(this.notReconciliationReconcileStatus.get())
					.withReconciliationMatchStatus(this.matchedReconciliationMatchStatus.get())
					.withDataHolder(this.dataHolder_330.get())
					.build()
	);

	protected Lazy<ReconciliationRunObject> reconciliationRunObject_340 = new Lazy<>(() ->
			ReconciliationRunObjectBuilder.aReconciliationRunObject()
					.withIdentifier("ACCT-1003_STK1")
					.withReconciliationRun(
							this.reconciliationRun_103.get()
					)
					.withReconciliationInvestmentAccount(this.reconciliationInvestmentAccount_003.get())
					.withReconciliationInvestmentSecurity(this.reconciliationInvestmentSecurity_stock_01.get())
					.withReconciliationReconcileStatus(this.notReconciliationReconcileStatus.get())
					.withReconciliationMatchStatus(this.matchedReconciliationMatchStatus.get())
					.withDataHolder(this.dataHolder_340.get())
					.build()
	);

	protected Lazy<ReconciliationRunObject> reconciliationRunObject_350 = new Lazy<>(() ->
			ReconciliationRunObjectBuilder.aReconciliationRunObject()
					.withIdentifier("ACCT-1003_STK1")
					.withReconciliationRun(
							this.reconciliationRun_103.get()
					)
					.withReconciliationInvestmentAccount(this.reconciliationInvestmentAccount_003.get())
					.withReconciliationInvestmentSecurity(this.reconciliationInvestmentSecurity_stock_01.get())
					.withReconciliationReconcileStatus(this.notReconciliationReconcileStatus.get())
					.withReconciliationMatchStatus(this.matchedReconciliationMatchStatus.get())
					.withDataHolder(this.dataHolder_350.get())
					.build()
	);

	protected Lazy<ReconciliationRunObject> reconciliationRunObject_360 = new Lazy<>(() ->
			ReconciliationRunObjectBuilder.aReconciliationRunObject()
					.withIdentifier("ACCT-1003_STK1")
					.withReconciliationRun(
							this.reconciliationRun_103.get()
					)
					.withReconciliationInvestmentAccount(this.reconciliationInvestmentAccount_003.get())
					.withReconciliationInvestmentSecurity(this.reconciliationInvestmentSecurity_stock_01.get())
					.withReconciliationReconcileStatus(this.notReconciliationReconcileStatus.get())
					.withReconciliationMatchStatus(this.matchedReconciliationMatchStatus.get())
					.withDataHolder(this.dataHolder_360.get())
					.build()
	);

	protected Lazy<ReconciliationRunObject> reconciliationRunObject_370 = new Lazy<>(() ->
			ReconciliationRunObjectBuilder.aReconciliationRunObject()
					.withIdentifier("ACCT-1003_STK1")
					.withReconciliationRun(
							this.reconciliationRun_103.get()
					)
					.withReconciliationInvestmentAccount(this.reconciliationInvestmentAccount_003.get())
					.withReconciliationInvestmentSecurity(this.reconciliationInvestmentSecurity_stock_01.get())
					.withReconciliationReconcileStatus(this.notReconciliationReconcileStatus.get())
					.withReconciliationMatchStatus(this.matchedReconciliationMatchStatus.get())
					.withDataHolder(this.dataHolder_370.get())
					.build()
	);

	protected Lazy<ReconciliationRunObject> reconciliationRunObject_380 = new Lazy<>(() ->
			ReconciliationRunObjectBuilder.aReconciliationRunObject()
					.withIdentifier("ACCT-1003_STK1")
					.withReconciliationRun(
							this.reconciliationRun_103.get()
					)
					.withReconciliationInvestmentAccount(this.reconciliationInvestmentAccount_003.get())
					.withReconciliationInvestmentSecurity(this.reconciliationInvestmentSecurity_stock_01.get())
					.withReconciliationReconcileStatus(this.notReconciliationReconcileStatus.get())
					.withReconciliationMatchStatus(this.matchedReconciliationMatchStatus.get())
					.withDataHolder(this.dataHolder_380.get())
					.build()
	);
}
