package com.clifton.reconciliation.run;

import com.clifton.core.converter.json.jackson.JacksonObjectMapper;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.json.jackson.strategy.ReconciliationRunObjectStrategy;
import com.clifton.reconciliation.run.data.DataError;
import com.clifton.reconciliation.run.data.DataHolder;
import com.clifton.reconciliation.run.data.DataNote;
import com.clifton.reconciliation.run.data.DataObject;
import com.clifton.core.util.MathUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;


/**
 * @author StevenF
 */
public class ReconciliationRunObjectJsonTests extends BaseReconciliationRunObjectJsonTests {


	@Test
	public void testReconciliationRunObjectDataHolderFidelity() throws Exception {
		for (ReconciliationRunObject runObject : getAllReconciliationRunObjects()) {
			String dataHolderJson = serializeReconciliationRunObjectDataHolder(runObject.getDataHolder());
			System.out.println("RunObject [" + runObject.getId() + "]: \n" + dataHolderJson + "\n\n");
			DataHolder dataHolder = deserializeReconciliationRunObject(dataHolderJson);
			compareDataHolder(runObject.getDataHolder(), dataHolder);
		}
	}


	private void compareDataHolder(DataHolder originalDataHolder, DataHolder newDataHolder) {
		AssertUtils.assertTrue(compareDataObjects(originalDataHolder.getDataObjects(), newDataHolder.getDataObjects()), "Fidelity check failed; DataObject do not match!");
		AssertUtils.assertTrue(compareDataErrors(originalDataHolder.getErrors(), newDataHolder.getErrors()), "Fidelity check failed; Errors do not match!");
		AssertUtils.assertTrue(compareDataNotes(originalDataHolder.getNotes(), newDataHolder.getNotes()), "Fidelity check failed; Notes do not match!");
	}


	private boolean compareDataObjects(List<DataObject> originalDataObjects, List<DataObject> newDataObjects) {
		boolean returnVal = true;
		if (CollectionUtils.getSize(originalDataObjects) != CollectionUtils.getSize(newDataObjects)) {
			return false;
		}
		if (originalDataObjects != null && newDataObjects != null) {
			for (int i = 0; i < originalDataObjects.size(); i++) {
				DataObject originalDataObject = originalDataObjects.get(i);
				DataObject newDataObject = newDataObjects.get(i);
				if (!(originalDataObject.getSourceId() != null && newDataObject.getSourceId() != null
						&& originalDataObject.getSourceId().shortValue() == newDataObject.getSourceId().shortValue())) {
					return false;
				}
				if (!originalDataObject.getData().equals(newDataObject.getData())) {
					return false;
				}
				returnVal = compareRowData(originalDataObject.getRowData(), newDataObject.getRowData());
				if (!returnVal) {
					return returnVal;
				}
			}
		}
		return returnVal;
	}


	private boolean compareDataErrors(List<DataError> originalDataErrors, List<DataError> newDataErrors) {
		if (CollectionUtils.getSize(originalDataErrors) != CollectionUtils.getSize(newDataErrors)) {
			return false;
		}
		if (originalDataErrors != null && newDataErrors != null) {
			for (int i = 0; i < originalDataErrors.size(); i++) {
				DataError originalDataError = originalDataErrors.get(i);
				DataError newDataError = newDataErrors.get(i);
				if (!StringUtils.isEqual(originalDataError.getFieldName(), newDataError.getFieldName())) {
					return false;
				}
				if (!MathUtils.isEqual(originalDataError.getFieldId(), newDataError.getFieldId())) {
					return false;
				}
				if (!MathUtils.isEqual(originalDataError.getDiff(), newDataError.getDiff())) {
					return false;
				}
			}
		}
		return true;
	}


	private boolean compareDataNotes(List<DataNote> originalDataNotes, List<DataNote> newDataNotes) {
		if (CollectionUtils.getSize(originalDataNotes) != CollectionUtils.getSize(newDataNotes)) {
			return false;
		}
		if (originalDataNotes != null && newDataNotes != null) {
			for (int i = 0; i < originalDataNotes.size(); i++) {
				DataNote originalDataNote = originalDataNotes.get(i);
				DataNote newDataNote = newDataNotes.get(i);
				if (!StringUtils.isEqual(originalDataNote.getNote(), newDataNote.getNote())) {
					return false;
				}
				if (!DateUtils.isEqualWithoutTime(originalDataNote.getCreateDate(), newDataNote.getCreateDate())) {
					return false;
				}
				if (!MathUtils.isEqual(originalDataNote.getCreateUserId(), newDataNote.getCreateUserId())) {
					return false;
				}
				if (originalDataNote.getType() != newDataNote.getType()) {
					return false;
				}
			}
		}
		return true;
	}


	private boolean compareRowData(List<Map<String, Object>> originalRowData, List<Map<String, Object>> newRowData) {
		if (CollectionUtils.getSize(originalRowData) != CollectionUtils.getSize(newRowData)) {
			return false;
		}
		if (originalRowData != null && newRowData != null) {
			for (int i = 0; i < originalRowData.size(); i++) {
				if (!originalRowData.get(i).equals(newRowData.get(i))) {
					return false;
				}
			}
		}
		return true;
	}


	private static final ObjectMapper MAPPER = new JacksonObjectMapper(new ReconciliationRunObjectStrategy()).configure(SerializationFeature.INDENT_OUTPUT, true);


	private String serializeReconciliationRunObjectDataHolder(DataHolder dataHolder) throws Exception {
		return MAPPER.writeValueAsString(dataHolder);
	}


	private DataHolder deserializeReconciliationRunObject(String dataHolderJson) throws Exception {
		return MAPPER.readValue(dataHolderJson, MAPPER.getTypeFactory().constructSimpleType(DataHolder.class, null));
	}
}
