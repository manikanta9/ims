package com.clifton.reconciliation.run;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurityAlias;
import com.clifton.reconciliation.run.data.DataHolder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author terrys
 */
@Transactional
@ContextConfiguration(locations = "classpath:com/clifton/reconciliation/run/BaseReconciliationServiceTests-context.xml")
public class ReconciliationInvestmentServiceTests extends BaseReconciliationServiceTests {


	@Test
	public void testSecurityAliasConcurrentModification() {
		Date runDate = DateUtils.toDate("11/15/2019");
		ReconciliationInvestmentAccount primaryRunObjectAccount = createReconciliationInvestmentAccount(12561, "Rothschild Revocable Living Trust - FB", "663-139172");
		//Secondary account has a function that adds the dash.
		ReconciliationInvestmentAccount secondaryRunObjectAccount = createReconciliationInvestmentAccount(null, "External Account Number", "663139172");

		ReconciliationInvestmentSecurity primaryRunObjectSecurity = createReconciliationInvestmentSecurity(1, "Stock 0001", "FB US 12/20/19 C210", "Options");
		ReconciliationInvestmentSecurity anotherRunObjectSecurity = createReconciliationInvestmentSecurity(2, "Stock 0002", "SPXW US 12/11/19 P2810", "Options");

		ReconciliationInvestmentSecurity secondaryRunObjectSecurity = createReconciliationInvestmentSecurity(null, "3393639AY", "3393639AY", "Options");

		ReconciliationRun run = createReconciliationRun("Fidelity Brokerage Transaction Reconciliation", runDate);
		ReconciliationDefinition definition = getReconciliationDefinitionService().getReconciliationDefinitionPopulated(run.getReconciliationDefinition().getId(), runDate);

		DataHolder primaryRunObjectDataHolder = getDataHolder(definition, primaryRunObjectAccount, primaryRunObjectSecurity, GenerationTypes.PRIMARY);
		primaryRunObjectDataHolder.getDataObjects().get(0).getRowData().get(0).put("TradeDate", DateUtils.toDate("10/31/2019"));
		primaryRunObjectDataHolder.getDataObjects().get(0).getRowData().get(0).put("NetCashAmountLocal", new BigDecimal("5000"));
		primaryRunObjectDataHolder.getDataObjects().get(0).getRowData().get(0).put("ParentQuantity", new BigDecimal("5000"));
		ReconciliationRunObject primaryRunObject = createReconciliationRunObject(run,
				definition.getPrimarySource(),
				primaryRunObjectAccount,
				primaryRunObjectSecurity,
				primaryRunObjectDataHolder);
		getReconciliationRunService().saveReconciliationRunObject(primaryRunObject);

		ReconciliationInvestmentSecurityAlias securityAlias = new ReconciliationInvestmentSecurityAlias();
		securityAlias.setAlias("3393639AY");
		securityAlias.setInvestmentSecurity(primaryRunObjectSecurity);
		securityAlias.setReconciliationSource(run.getReconciliationDefinition().getSecondarySource());
		securityAlias.setDisplayProperty("securitySymbol");
		securityAlias.setStartDate(runDate);
		getReconciliationInvestmentService().saveReconciliationInvestmentSecurityAlias(securityAlias);

		DataHolder secondaryRunObjectDataHolder = getDataHolder(definition, secondaryRunObjectAccount, secondaryRunObjectSecurity, GenerationTypes.SECONDARY);
		secondaryRunObjectDataHolder.getDataObjects().get(0).getRowData().get(0).put("TradeDate", DateUtils.toDate("10/31/2019"));
		secondaryRunObjectDataHolder.getDataObjects().get(0).getRowData().get(0).put("NetTradeAmount", new BigDecimal("5000"));
		secondaryRunObjectDataHolder.getDataObjects().get(0).getRowData().get(0).put("Quantity", new BigDecimal("5000"));
		ReconciliationRunObject secondaryRunObject = createReconciliationRunObject(run,
				definition.getSecondarySource(),
				secondaryRunObjectAccount,
				secondaryRunObjectSecurity,
				secondaryRunObjectDataHolder);
		getReconciliationRunService().saveReconciliationRunObject(secondaryRunObject);

		List<ReconciliationRunObject> runObjectList = getReconciliationRunService().getReconciliationRunObjectListByRunId(run.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(runObjectList));

		securityAlias = new ReconciliationInvestmentSecurityAlias();
		securityAlias.setAlias("4423889UO");
		securityAlias.setInvestmentSecurity(anotherRunObjectSecurity);
		securityAlias.setReconciliationSource(run.getReconciliationDefinition().getSecondarySource());
		securityAlias.setDisplayProperty("securitySymbol");
		securityAlias.setStartDate(runDate);
		getReconciliationInvestmentService().saveReconciliationInvestmentSecurityAlias(securityAlias);

		runObjectList = getReconciliationRunService().getReconciliationRunObjectListByRunId(run.getId());
		Assertions.assertEquals(1, CollectionUtils.getSize(runObjectList));
		Assertions.assertTrue(CollectionUtils.getOnlyElement(runObjectList).isMatched());
		Assertions.assertEquals("5000", CollectionUtils.getOnlyElement(runObjectList).getDataHolder().getDataObjects().get(0).getData().get("Quantity"));
		Assertions.assertEquals("5000", CollectionUtils.getOnlyElement(runObjectList).getDataHolder().getDataObjects().get(1).getData().get("Quantity"));
		Assertions.assertEquals("5000", CollectionUtils.getOnlyElement(runObjectList).getDataHolder().getDataObjects().get(0).getData().get("Cash Amount"));
		Assertions.assertEquals("5000", CollectionUtils.getOnlyElement(runObjectList).getDataHolder().getDataObjects().get(1).getData().get("Cash Amount"));
	}


	@Test
	public void testSecurityAliasDuplicateKey() {
		Date runDate = DateUtils.toDate("11/15/2019");
		//Secondary and tertiary account has a function that adds the dash.
		ReconciliationInvestmentAccount primaryRunObjectAccount = createReconciliationInvestmentAccount(7703, "Humphreys Trust BB&T Collateral Loan Acct FBO Humphreys Family Revocable Living Trust - CSX", "6108-2752");
		ReconciliationInvestmentAccount secondaryRunObjectAccount = createReconciliationInvestmentAccount(null, "External Account Number", "61082752");

		ReconciliationInvestmentSecurity primaryRunObjectSecurity = createReconciliationInvestmentSecurity(2, "PFE US 12/06/19 C39", "PFE   191206C00039000", "Options");
		ReconciliationInvestmentSecurity secondaryRunObjectSecurity = createReconciliationInvestmentSecurity(null, "Dumb External Identifier", "9714553", "Options");

		ReconciliationRun run = createReconciliationRun("BB&T Position Reconciliation", runDate);
		ReconciliationDefinition definition = getReconciliationDefinitionService().getReconciliationDefinitionPopulated(run.getReconciliationDefinition().getId(), runDate);

		DataHolder secondaryRunObjectDataHolder = getDataHolder(definition, secondaryRunObjectAccount, secondaryRunObjectSecurity, GenerationTypes.SECONDARY);
		secondaryRunObjectDataHolder.getDataObjects().get(0).getRowData().get(0).put("TradeDate", DateUtils.toDate("10/31/2019"));
		ReconciliationRunObject secondaryRunObject = createReconciliationRunObject(run,
				definition.getSecondarySource(),
				secondaryRunObjectAccount,
				secondaryRunObjectSecurity,
				secondaryRunObjectDataHolder);
		getReconciliationRunService().saveReconciliationRunObject(secondaryRunObject);

		DataHolder primaryRunObjectDataHolder = getDataHolder(definition, primaryRunObjectAccount, primaryRunObjectSecurity, GenerationTypes.PRIMARY);
		primaryRunObjectDataHolder.getDataObjects().get(0).getRowData().get(0).put("TradeDate", DateUtils.toDate("10/31/2019"));
		ReconciliationRunObject primaryRunObject = createReconciliationRunObject(run,
				definition.getPrimarySource(),
				primaryRunObjectAccount,
				primaryRunObjectSecurity,
				primaryRunObjectDataHolder);
		getReconciliationRunService().saveReconciliationRunObject(primaryRunObject);

		ReconciliationInvestmentSecurityAlias securityAlias = new ReconciliationInvestmentSecurityAlias();
		securityAlias.setAlias("9714553");
		securityAlias.setInvestmentSecurity(primaryRunObjectSecurity);
		securityAlias.setReconciliationSource(run.getReconciliationDefinition().getSecondarySource());
		securityAlias.setDisplayProperty("securitySymbol");
		securityAlias.setStartDate(runDate);
		getReconciliationInvestmentService().saveReconciliationInvestmentSecurityAlias(securityAlias);

		List<ReconciliationRunObject> runObjectList = getReconciliationRunService().getReconciliationRunObjectListByRunId(run.getId());
		//Item should have matched and only 1 object should remain.
		Assertions.assertEquals(1, CollectionUtils.getSize(runObjectList));
	}
}
