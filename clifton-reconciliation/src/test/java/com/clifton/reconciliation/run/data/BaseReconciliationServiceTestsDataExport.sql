SELECT 'WorkflowState' AS entityTableName, WorkflowStateID AS entityId FROM WorkflowState
UNION
SELECT 'WorkflowStatus' AS entityTableName, WorkflowStatusID AS entityId FROM WorkflowStatus
UNION
SELECT 'WorkflowTransition' AS entityTableName, WorkflowTransitionID AS entityId FROM WorkflowTransition
UNION
SELECT 'WorkflowTransitionAction' AS entityTableName, WorkflowTransitionActionID AS entityId FROM WorkflowTransitionAction
UNION
SELECT 'Workflow' AS entityTableName, WorkflowID AS entityId FROM Workflow
UNION
SELECT 'SystemBean' AS entityTableName, SystemBeanID AS entityId FROM SystemBean
UNION
SELECT 'SystemTable' AS entityTableName, SystemTableID AS entityId FROM SystemTable
UNION
SELECT 'SystemColumn' AS entityTableName, SystemColumnID AS entityId FROM SystemColumn
UNION
SELECT 'SystemRelationship' AS entityTableName, SystemRelationshipID AS entityId FROM SystemRelationship
UNION
SELECT 'ReconciliationType' AS entityTableName, ReconciliationTypeID AS entityId FROM ReconciliationType
UNION
SELECT 'ReconciliationSource' AS entityTableName, ReconciliationSourceID AS entityId FROM ReconciliationSource
UNION
SELECT 'ReconciliationReconcileStatus' AS entityTableName, ReconciliationReconcileStatusID AS entityId FROM ReconciliationReconcileStatus
UNION
SELECT 'ReconciliationMatchStatus' AS entityTableName, ReconciliationMatchStatusID AS entityId FROM ReconciliationMatchStatus
UNION
SELECT 'ReconciliationFileDefinitionMapping' AS entityTableName, ReconciliationFileDefinitionMappingID as entityId FROM ReconciliationFileDefinitionMapping
UNION
SELECT 'ReconciliationDefinitionFieldMapping' AS entityTableName, ReconciliationDefinitionFieldMappingID as entityId FROM ReconciliationDefinitionFieldMapping;
