package com.clifton.reconciliation.run;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.run.data.DataNote;
import com.clifton.reconciliation.run.data.DataNoteTypes;
import com.clifton.reconciliation.run.data.DataObject;
import com.clifton.reconciliation.run.status.ReconciliationMatchStatusTypes;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author terrys
 */
@Transactional
@ContextConfiguration(locations = "classpath:com/clifton/reconciliation/run/BaseReconciliationServiceTests-context.xml")
public class ReconciliationServiceMatchTests extends BaseReconciliationServiceTests {

	ReconciliationInvestmentAccount account;
	ReconciliationInvestmentSecurity security;


	private ReconciliationRunObject getReconciliationObject(ReconciliationRun run, GenerationTypes generationType) {
		return getReconciliationObject(run, generationType, true);
	}


	private ReconciliationRunObject getReconciliationObject(ReconciliationRun run, GenerationTypes generationType, boolean save) {
		if (this.account == null) {
			this.account = createReconciliationInvestmentAccount(1, "Test Account 0001", "ACT-0001");
		}
		if (this.security == null) {
			this.security = createReconciliationInvestmentSecurity(1, "Stock 0001", "STK0001", "Stocks");
		}
		//Get the definition fresh so it hydrates all mappings.
		ReconciliationDefinition definition = this.reconciliationDefinitionService.getReconciliationDefinitionPopulated(run.getReconciliationDefinition().getId(), new Date());
		ReconciliationSource source = generationType == GenerationTypes.PRIMARY ? definition.getPrimarySource() : definition.getSecondarySource();
		return createReconciliationRunObject(run, source, this.account, this.security, getDataHolder(definition, this.account, this.security, generationType), save);
	}


	private void validateObject(ReconciliationRunObject matchedRunObject, int expectedRunObjects, int expectedDataObjects) {
		ReconciliationRun run = matchedRunObject.getReconciliationRun();
		List<ReconciliationRunObject> runObjectList = this.reconciliationRunService.getReconciliationRunObjectListByRunId(run.getId());
		Assertions.assertEquals(expectedRunObjects, CollectionUtils.getSize(runObjectList));

		List<DataObject> dataList = matchedRunObject.getDataHolder().getDataObjects();
		Assertions.assertEquals(expectedDataObjects, CollectionUtils.getSize(dataList));
	}


	private void validateNoteList(List<DataNote> dataNoteList, List<DataNote> expectedNoteList) {
		Assertions.assertEquals(CollectionUtils.getSize(dataNoteList), CollectionUtils.getSize(expectedNoteList));
		for (int i = 0; i < CollectionUtils.getSize(dataNoteList); i++) {
			DataNote dataNote = dataNoteList.get(i);
			DataNote expectedDataNote = expectedNoteList.get(i);
			Assertions.assertEquals(dataNote.getNote(), expectedDataNote.getNote());
			Assertions.assertEquals(dataNote.getType(), expectedDataNote.getType());
			Assertions.assertEquals(dataNote.getSourceId(), expectedDataNote.getSourceId());
		}
	}


	@Test
	public void testMatch() {
		ReconciliationRun run = createReconciliationRun("Pershing Cash Reconciliation", DateUtils.toDate("03/04/2019"));
		ReconciliationRunObject primaryRunObject = getReconciliationObject(run, GenerationTypes.PRIMARY);
		ReconciliationRunObject secondaryRunObject = getReconciliationObject(run, GenerationTypes.SECONDARY);
		// //Custom set the value for portfolio code since the definition function will add a dash.
		// secondaryRunObject.getDataHolder().getDataObjectListBySource(run.getReconciliationDefinition().getSecondarySource()).get(0).getData().put("PortfolioCode", "ACT-0001");

		long primaryRunObjectId = primaryRunObject.getId();
		long secondaryRunObjectId = secondaryRunObject.getId();

		// process the MATCH
		ReconciliationRunObject matchedRunObject = this.reconciliationRunService.matchReconciliationRunObjectList(Arrays.asList(primaryRunObject, secondaryRunObject), "Test Match");
		validateObject(matchedRunObject, 1, 2);
		validateNoteList(matchedRunObject.getDataNoteList(), Collections.singletonList(DataNoteBuilder.aDataNote().withNote("Test Match").withType(DataNoteTypes.MATCH).build()));

		// there should now be one new ReconciliationRunObject that does not match the ids of the original.
		Assertions.assertTrue(!CompareUtils.isEqual(primaryRunObjectId, matchedRunObject.getId()) && !CompareUtils.isEqual(secondaryRunObjectId, matchedRunObject.getId()));
		Assertions.assertEquals(ReconciliationReconcileStatusTypes.NOT_RECONCILED, matchedRunObject.getReconciliationReconcileStatus().getReconciliationReconcileStatusType());
		Assertions.assertEquals(ReconciliationMatchStatusTypes.MANUALLY_MATCHED, matchedRunObject.getReconciliationMatchStatus().getReconciliationMatchStatusType());
		Assertions.assertTrue(matchedRunObject.isManuallyMatched());

		// the other ReconciliationRunObjects should NOT exist anymore
		ReconciliationRunObject deletedObject = this.reconciliationRunService.getReconciliationRunObject(primaryRunObjectId);
		Assertions.assertNull(deletedObject);

		deletedObject = this.reconciliationRunService.getReconciliationRunObject(secondaryRunObjectId);
		Assertions.assertNull(deletedObject);

		//Retrieve the run fresh and ensure it is still NOT_RECONCILED
		run = this.reconciliationRunService.getReconciliationRun(run.getId());
		Assertions.assertNotNull(run);
		Assertions.assertEquals(ReconciliationReconcileStatusTypes.NOT_RECONCILED, run.getReconciliationReconcileStatus().getReconciliationReconcileStatusType());
	}


	@Test
	public void testMatchUnmatchAlternateMatch() {
		ReconciliationRun run = createReconciliationRun("Pershing Cash Reconciliation", DateUtils.toDate("03/04/2019"));
		ReconciliationSource primarySource = run.getReconciliationDefinition().getPrimarySource();
		ReconciliationSource secondarySource = run.getReconciliationDefinition().getSecondarySource();
		ReconciliationRunObject primaryRunObject = getReconciliationObject(run, GenerationTypes.PRIMARY);
		ReconciliationRunObject secondaryRunObject = getReconciliationObject(run, GenerationTypes.SECONDARY);
		ReconciliationRunObject alternateRunObject = getReconciliationObject(run, GenerationTypes.SECONDARY, false);

		// process the MATCH
		ReconciliationRunObject matchedObject = this.reconciliationRunService.matchReconciliationRunObjectList(Arrays.asList(primaryRunObject, secondaryRunObject), "Test Match");
		validateObject(matchedObject, 1, 2);
		validateNoteList(matchedObject.getDataNoteList(), Collections.singletonList(DataNoteBuilder.aDataNote().withNote("Test Match").withType(DataNoteTypes.MATCH).build()));

		// process the UNMATCH -
		List<ReconciliationRunObject> unmatchedObjectList = this.reconciliationRunService.unmatchReconciliationRunObjectList(Collections.singletonList(matchedObject), "Test Unmatch");
		for (ReconciliationRunObject unmatchedObject : CollectionUtils.getIterable(unmatchedObjectList)) {
			validateObject(unmatchedObject, 2, 1);
		}
		//Get the primary source runObject.
		primaryRunObject = unmatchedObjectList.stream().filter(runObject -> !CollectionUtils.isEmpty(runObject.getDataHolder().getDataObjectListBySource(primarySource))).collect(Collectors.toList()).get(0);

		matchedObject = this.reconciliationRunService.matchReconciliationRunObjectList(Arrays.asList(primaryRunObject, alternateRunObject), "Test Alternate Match");
		validateObject(matchedObject, 2, 2);

		List<DataNote> expectedDataNoteList = new ArrayList<>();
		expectedDataNoteList.add(DataNoteBuilder.aDataNote().withNote("Test Alternate Match").withType(DataNoteTypes.MATCH).build());
		expectedDataNoteList.add(DataNoteBuilder.aDataNote().withNote("Test Unmatch").withType(DataNoteTypes.UNMATCH).withSourceId(primarySource.getId()).build());
		expectedDataNoteList.add(DataNoteBuilder.aDataNote().withNote("Test Match").withType(DataNoteTypes.MATCH).build());
		validateNoteList(matchedObject.getDataNoteList(), expectedDataNoteList);
	}

	//TODO - add test match/reconcile/unmatch
	//
	// @Test
	// public void testUnMatchReconciliationObjects() {
	// 	ReconciliationRun run = createReconciliationRun("Pershing Cash Reconciliation", DateUtils.toDate("03/04/2019"));
	// 	ReconciliationRunObject primaryRunObject = getReconciliationObject(run, GenerationTypes.PRIMARY);
	// 	ReconciliationRunObject secondaryRunObject = getReconciliationObject(run, GenerationTypes.SECONDARY);
	//
	// 	long primaryRunObjectId = primaryRunObject.getId();
	// 	long secondaryRunObjectId = secondaryRunObject.getId();
	//
	// 	List<ReconciliationRunObject> originalObjectList = this.reconciliationRunService.getReconciliationRunObjectList(runObjectIds);
	// 	Assertions.assertFalse(CollectionUtils.isEmpty(originalObjectList));
	// 	Assertions.assertEquals(2, originalObjectList.size());
	// 	Assertions.assertTrue(originalObjectList.stream().noneMatch(o -> StringUtils.isEmpty(o.getIdentifier())));
	//
	// 	// process the MATCH
	// 	this.reconciliationRunService.matchReconciliationRunObjectList(runObjectIds, getNoteMessage.apply(DataNoteTypes.MATCH));
	//
	// 	// quick check to make sure we have one ReconciliationRunObject after the MATCH
	// 	runObjectIds = this.getRunObjects.apply(runId);
	// 	MatcherAssert.assertThat(runObjectIds.length, IsEqual.equalTo(1));
	//
	// 	// revert the MATCH
	// 	List<ReconciliationRunObject> unMatchedReconciliationRunObjects = this.reconciliationRunService.unmatchReconciliationRunObjectList(runObjectIds, getNoteMessage.apply(DataNoteTypes.UNMATCH));
	//
	// 	// there should now be two new ReconciliationObjects
	// 	runObjectIds = this.getRunObjects.apply(runId);
	// 	MatcherAssert.assertThat(runObjectIds.length, IsEqual.equalTo(2));
	// 	MatcherAssert.assertThat(unMatchedReconciliationRunObjects.size(), IsEqual.equalTo(runObjectIds.length));
	// 	Assertions.assertTrue(unMatchedReconciliationRunObjects.stream().map(ReconciliationRunObject::getId).collect(Collectors.toList()).containsAll(Arrays.asList(runObjectIds)));
	//
	// 	Map<ReconciliationMatchStatus, List<ReconciliationRunObject>> matchStatusMap = unMatchedReconciliationRunObjects.stream().collect(Collectors.groupingBy(ReconciliationRunObject::getReconciliationMatchStatus));
	// 	Assertions.assertEquals(1, matchStatusMap.keySet().size());
	// 	Assertions.assertTrue(matchStatusMap.containsKey(this.getNotReconciliationMatchStatus.get()));
	// 	Assertions.assertEquals(2, matchStatusMap.values().iterator().next().size());
	// 	Assertions.assertTrue(matchStatusMap.values().stream().flatMap(List::stream).noneMatch(ReconciliationRunObject::isManuallyMatched));
	//
	// 	Map<ReconciliationReconcileStatus, List<ReconciliationRunObject>> reconcileStatusMap = unMatchedReconciliationRunObjects.stream().collect(Collectors.groupingBy(ReconciliationRunObject::getReconciliationReconcileStatus));
	// 	Assertions.assertEquals(1, reconcileStatusMap.keySet().size());
	// 	Assertions.assertTrue(reconcileStatusMap.containsKey(this.getNotReconciliationReconcileStatus.get()));
	// 	Assertions.assertEquals(2, reconcileStatusMap.values().iterator().next().size());
	// 	Assertions.assertTrue(reconcileStatusMap.values().stream().flatMap(List::stream).noneMatch(ReconciliationRunObject::isManuallyReconciled));
	//
	// 	// check that there's now one item in each object data list
	// 	// instead of two items in the data list (one for each source),
	// 	// now we should have two objects each with one item in the data list b/c we separated out the sources
	// 	List<DataObject> dataList1 = unMatchedReconciliationRunObjects.get(0).getDataHolder().getDataObjects();
	// 	Assertions.assertFalse(CollectionUtils.isEmpty(dataList1));
	// 	Assertions.assertEquals(1, dataList1.size());
	// 	List<DataObject> dataList2 = unMatchedReconciliationRunObjects.get(1).getDataHolder().getDataObjects();
	// 	Assertions.assertFalse(CollectionUtils.isEmpty(dataList2));
	// 	Assertions.assertEquals(1, dataList2.size());
	//
	// 	// the other ReconciliationRunObject should NOT exist anymore
	// 	List<Long> runObjectIdList = Arrays.asList(this.getRunObjects.apply(runId));
	// 	Assertions.assertFalse(runObjectIdList.contains(externalId));
	// 	Assertions.assertFalse(runObjectIdList.contains(externalId));
	//
	// 	// since we are not reconciling, the status should still be NOT RECONCILED
	// 	ReconciliationRun finalRun = this.reconciliationRunService.getReconciliationRun(runId);
	// 	Assertions.assertNotNull(finalRun);
	// 	Assertions.assertEquals(ReconciliationReconcileStatusTypes.NOT_RECONCILED, finalRun.getReconciliationReconcileStatus().getReconciliationReconcileStatusType());
	//
	// 	unMatchedReconciliationRunObjects.forEach(ro -> {
	// 		List<DataNote> noteList1 = ro.getDataHolder().getNotes();
	// 		Assertions.assertFalse(CollectionUtils.isEmpty(noteList1));
	// 		Assertions.assertEquals(2, noteList1.size());
	// 		Assertions.assertEquals(1, noteList1.stream().filter(n -> n.getType() == DataNoteTypes.UNMATCH).count());
	// 		Assertions.assertEquals(1, noteList1.stream().filter(n -> n.getType() == DataNoteTypes.MATCH).count());
	// 	});
	// }
}
