package com.clifton.reconciliation.simpletable;

import com.clifton.core.util.AssertUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Collection;


/**
 * @author jonathanr
 */
public class CSVFileToSimpleTableConverterTests {

	private final FileToSimpleTableConverter fileToSimpleTableConverter = new CSVFileToSimpleTableConverter(true, true);
	private final FileToSimpleTableConverter fileToSimpleTableConverter2 = new CSVFileToSimpleTableConverter(false, true);


	@Test
	public void testCSVFileConverter() {
		File testFile = new File(("src/test/java/com/clifton/reconciliation/simpletable/CSV File Test.csv"));
		SimpleTable result = this.fileToSimpleTableConverter.convert(testFile);
		SimpleTable resultWithBlankLines = this.fileToSimpleTableConverter2.convert(testFile);

		AssertUtils.assertTrue(8 == result.getTotalRowCount(), "Expected a DataTable with 8 rows of data");
		AssertUtils.assertTrue(10 == resultWithBlankLines.getTotalRowCount(), "Expected a DataTable with 9 rows of data");

		validateRow(result.getObjectValueMap(0).values(), new Object[]{"Client Relationship", "123456", "Salesforce", "Client Relationship (Organization)", "2178"});
		validateRow(result.getObjectValueMap(1).values(), new Object[]{"Client Relationship", "The Gifford Foundation", "Salesforce", "Client Relationship (Organization)", ""});
		validateRow(result.getObjectValueMap(2).values(), new Object[]{"Client Relationship", "Research Affiliates \"RA\"", "Salesforce", "Client Relationship (Organization)", "2246"});
		validateRow(result.getObjectValueMap(3).values(), new Object[]{"Client Relationship", "New Mexico Educational Retirement Board", "Salesforce", "Client Relationship (Organization)", "3413"});
		validateRow(result.getObjectValueMap(4).values(), new Object[]{"Client Relationship", "Electrical Workers Southern California IBEW/NECA , Locals #11, #440, #441, #477", "Salesforce", "Client Relationship (Organization)", "3445"});
		validateRow(result.getObjectValueMap(5).values(), new Object[]{"Client Relationship", "Macy's, Inc.", "Salesforce", "Client Relationship (Organization)", "3440"});
		validateRow(result.getObjectValueMap(6).values(), new Object[]{"Client Relationship", "Gwinnett County Board of Education Retirement System", "Salesforce", "Client Relationship (Organization)", "2968"});
		validateRow(result.getObjectValueMap(7).values(), new Object[]{"Client Relationship", "", "Salesforce", "Client Relationship (Organization)", "3768"});
		validateRow(resultWithBlankLines.getObjectValueMap(8).values(), new Object[]{"", "", "", "", ""});
		validateRow(resultWithBlankLines.getObjectValueMap(9).values(), new Object[]{"", "", "", "", ""});
	}


	@Test
	public void testEmptyFileConversion() {
		File testFile = new File(("src/test/java/com/clifton/reconciliation/simpletable/EmptyFileTest.csv"));
		SimpleTable result = this.fileToSimpleTableConverter.convert(testFile);
		AssertUtils.assertTrue(0 == result.getTotalRowCount(), "Expected a DataTable with 0 rows of data");
	}


	private void validateRow(Collection<Object> row, Object[] expectedValues) {
		Assertions.assertEquals(row.size(), expectedValues.length);
		for (int i = 0; i < expectedValues.length; i++) {
			Assertions.assertTrue(row.contains(expectedValues[i]));
			row.remove(expectedValues[i]);
		}
	}
}
