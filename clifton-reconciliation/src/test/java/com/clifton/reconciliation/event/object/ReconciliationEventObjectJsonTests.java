package com.clifton.reconciliation.event.object;

import com.clifton.core.converter.json.jackson.JacksonObjectMapper;
import com.clifton.core.converter.json.jackson.strategy.JacksonExternalServiceStrategy;
import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectDataType;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectJournalData;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.data.DataObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;


/**
 * @author stevenf
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ReconciliationEventObjectJsonTests {

	@Test
	public void testReconciliationRunObjectDataHolderFidelity() throws Exception {

		ReconciliationEventObjectDataType eventObjectDataType = new ReconciliationEventObjectDataType();
		eventObjectDataType.setName("Interest Expense");
		eventObjectDataType.setDescription("Test.");

		ReconciliationEventObjectJournalData journalData = new ReconciliationEventObjectJournalData();
		journalData.setAmount(new BigDecimal("67.52"));
		journalData.setEventNote("Interest Expense");
		journalData.setHoldingAccountNumber("4325345");
		journalData.setSettlementDate(DateUtils.toDate("8/12/21"));
		journalData.setTransactionDate(DateUtils.toDate("8/12/21"));
		journalData.setOriginalTransactionDate(DateUtils.toDate("8/12/21"));

		ReconciliationRunObject runObject = new ReconciliationRunObject();
		runObject.getDataHolder().getDataObjects().add(new DataObject());
		runObject.setId(Long.valueOf("1234"));

		ReconciliationEventObjectJournal journalEventObject = new ReconciliationEventObjectJournal();
		journalEventObject.setRunObject(runObject);
		journalEventObject.setEventObjectData(journalData);
		journalEventObject.setEventObjectDataType(eventObjectDataType);

		String journalEventObjectJson = serializeData(journalEventObject);
		journalEventObject = deserializeData(journalEventObjectJson);

		Assertions.assertEquals(journalEventObject.getEventObjectDataType().getName(), "Interest Expense");
		Assertions.assertEquals(journalEventObject.getEventObjectDataType().getDescription(), "Test.");
		Assertions.assertEquals(journalEventObject.getEventObjectData().getAmount(), new BigDecimal("67.52"));
		Assertions.assertEquals(journalEventObject.getEventObjectData().getEventNote(), "Interest Expense");
		Assertions.assertEquals(journalEventObject.getEventObjectData().getHoldingAccountNumber(), "4325345");
		Assertions.assertEquals(journalEventObject.getEventObjectData().getSettlementDate(), DateUtils.toDate("8/12/21"));
		Assertions.assertEquals(journalEventObject.getEventObjectData().getTransactionDate(), DateUtils.toDate("8/12/21"));
		Assertions.assertEquals(journalEventObject.getEventObjectData().getOriginalTransactionDate(), DateUtils.toDate("8/12/21"));
	}


	private static final ObjectMapper MAPPER = new JacksonObjectMapper(new JacksonExternalServiceStrategy()).configure(SerializationFeature.INDENT_OUTPUT, true);


	private String serializeData(ReconciliationEventObject<?> eventObject) throws Exception {
		return MAPPER.writeValueAsString(eventObject);
	}


	private ReconciliationEventObjectJournal deserializeData(String dataHolderJson) throws Exception {
		return MAPPER.readValue(dataHolderJson, MAPPER.getTypeFactory().constructSimpleType(ReconciliationEventObjectJournal.class, null));
	}
}
