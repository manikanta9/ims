package com.clifton.reconciliation.event.object.observer;

import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.date.DateUtils;
import com.clifton.reconciliation.definition.ReconciliationDefinition;
import com.clifton.reconciliation.definition.ReconciliationDefinitionBuilder;
import com.clifton.reconciliation.definition.ReconciliationDefinitionField;
import com.clifton.reconciliation.definition.ReconciliationDefinitionFieldBuilder;
import com.clifton.reconciliation.definition.ReconciliationDefinitionService;
import com.clifton.reconciliation.definition.ReconciliationField;
import com.clifton.reconciliation.definition.ReconciliationFieldBuilder;
import com.clifton.reconciliation.definition.ReconciliationSource;
import com.clifton.reconciliation.definition.evaluator.ReconciliationEvaluatorService;
import com.clifton.reconciliation.definition.type.ReconciliationType;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectCategories;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectJournal;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectService;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectDataType;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectJournalData;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentHandler;
import com.clifton.reconciliation.reconcile.ReconciliationReconcileServiceImpl;
import com.clifton.reconciliation.run.ReconciliationRun;
import com.clifton.reconciliation.run.ReconciliationRunBuilder;
import com.clifton.reconciliation.run.ReconciliationRunObject;
import com.clifton.reconciliation.run.ReconciliationRunService;
import com.clifton.reconciliation.run.data.DataHolder;
import com.clifton.reconciliation.run.data.DataObject;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatus;
import com.clifton.reconciliation.run.status.ReconciliationReconcileStatusBuilder;
import com.clifton.reconciliation.run.status.ReconciliationStatusService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;


//TODO - revisit
@Disabled
@ExtendWith(MockitoExtension.class)
public class ReconciliationEventObjectObserverTest {

	@InjectMocks
	private ReconciliationEventObjectObserver reconciliationEventObjectObserver;

	@Mock
	private ReconciliationRunService reconciliationRunService;

	@Mock
	private ReconciliationEventObjectService reconciliationEventObjectService;

	@InjectMocks
	private ReconciliationReconcileServiceImpl reconciliationReconcileService;

	@Mock
	private ReconciliationEvaluatorService reconciliationEvaluatorService;

	@Mock
	private ReconciliationDefinitionService reconciliationDefinitionService;

	@Mock
	private ReconciliationStatusService reconciliationStatusService;

	@Mock
	private ReconciliationInvestmentHandler reconciliationInvestmentHandler;

	private ReconciliationSource primarySource;
	private ReconciliationSource secondarySource;
	private ReconciliationDefinition cashReconciliationDefinition;
	private ReconciliationRun cashReconciliationRun;
	private ReconciliationRunObject cashReconciliationRunObject;
	private ReconciliationRunObject transactionReconciliationRunObject;
	private ReconciliationEventObjectDataType cashContributionEventObjectDataType;
	private ReconciliationEventObjectDataType interestExpenseEventObjectDataType;


	@BeforeEach
	public void before() {
		ReconciliationType cashType = new ReconciliationType();
		cashType.setId((short) 1);
		cashType.setName(ReconciliationType.CASH);

		ReconciliationType transactionType = new ReconciliationType();
		transactionType.setId((short) 2);
		transactionType.setName(ReconciliationType.TRANSACTION);

		this.primarySource = new ReconciliationSource();
		this.primarySource.setName(ReconciliationSource.DEFAULT_SOURCE);
		this.primarySource.setId((short) 1);

		this.secondarySource = new ReconciliationSource();
		this.secondarySource.setName("JP Morgan");
		this.secondarySource.setId((short) 2);

		ReconciliationField cashBalanceField = ReconciliationFieldBuilder.aReconciliationField()
				.withName("Cash Balance")
				.withDescription("Cash Balance")
				.withDefaultDataType(DataTypes.MONEY)
				.withDefaultDisplayWidth(150)
				.build();
		cashBalanceField.setId(11);

		ReconciliationDefinitionField cashDefinitionField = ReconciliationDefinitionFieldBuilder.aReconciliationDefinitionField()
				.withField(cashBalanceField)
				.withReconciliationDefinition(this.cashReconciliationDefinition)
				.withReconcilable(true)
				.withAdjustable(true)
				.withAggregate(true)
				.withStartDate(DateUtils.toDate("04/1/2019"))
				.build();

		this.cashReconciliationDefinition = ReconciliationDefinitionBuilder.aReconciliationDefinition()
				.withType(cashType)
				.withPrimarySource(this.primarySource)
				.withSecondarySource(this.secondarySource)
				.build();
		this.cashReconciliationDefinition.setId(1);
		this.cashReconciliationDefinition.setReconciliationDefinitionFieldList(Collections.singletonList(cashDefinitionField));

		ReconciliationDefinition transactionReconciliationDefinition = ReconciliationDefinitionBuilder.aReconciliationDefinition()
				.withType(transactionType)
				.withParentDefinition(this.cashReconciliationDefinition)
				.build();
		transactionReconciliationDefinition.setId(2);

		ReconciliationRun transactionReconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
				.withReconciliationDefinition(transactionReconciliationDefinition)
				.build();
		transactionReconciliationRun.setId(2);

		this.cashReconciliationRun = ReconciliationRunBuilder.aReconciliationRun()
				.withReconciliationDefinition(this.cashReconciliationDefinition)
				.build();
		this.cashReconciliationRun.setId(2);

		ReconciliationInvestmentAccount reconciliationInvestmentAccount = new ReconciliationInvestmentAccount();
		reconciliationInvestmentAccount.setId(1);

		this.transactionReconciliationRunObject = new ReconciliationRunObject();
		this.transactionReconciliationRunObject.setId((long) 1);
		this.transactionReconciliationRunObject.setReconciliationInvestmentAccount(reconciliationInvestmentAccount);
		this.transactionReconciliationRunObject.setReconciliationRun(transactionReconciliationRun);

		DataObject primaryDataObject = new DataObject();
		primaryDataObject.setSourceId(this.primarySource.getId());

		DataObject secondaryDataObject = new DataObject();
		secondaryDataObject.setSourceId(this.secondarySource.getId());

		DataHolder dataHolder = new DataHolder();
		dataHolder.setDataObjects(Arrays.asList(primaryDataObject, secondaryDataObject));

		ReconciliationReconcileStatus notReconciled = ReconciliationReconcileStatusBuilder.aReconciliationReconcileStatus()
				.withReconciled(false)
				.withName("NOT_RECONCILED")
				.withLabel("Not Reconciled")
				.withDescription("Indicates reconciliation failed for this data.")
				.withSortOrder((short) 10)
				.build();
		notReconciled.setId((short) 1);

		this.cashReconciliationRunObject = new ReconciliationRunObject();
		this.cashReconciliationRunObject.setId((long) 2);
		this.cashReconciliationRunObject.setReconciliationRun(this.cashReconciliationRun);
		this.cashReconciliationRunObject.setDataHolder(dataHolder);
		this.cashReconciliationRunObject.setReconciliationReconcileStatus(notReconciled);

		this.cashContributionEventObjectDataType = new ReconciliationEventObjectDataType();
		this.cashContributionEventObjectDataType.setId((short) 1);
		this.cashContributionEventObjectDataType.setName("Cash Contribution");
		this.cashContributionEventObjectDataType.setEventObjectCategory(ReconciliationEventObjectCategories.SIMPLE_JOURNAL);

		this.interestExpenseEventObjectDataType = new ReconciliationEventObjectDataType();
		this.interestExpenseEventObjectDataType.setId((short) 3);
		this.interestExpenseEventObjectDataType.setName("Interest Expense");
		this.interestExpenseEventObjectDataType.setEventObjectCategory(ReconciliationEventObjectCategories.SIMPLE_JOURNAL);
		this.interestExpenseEventObjectDataType.setNegativeAdjustment(true);
	}


	//TODO - these aren't really relevant.
	// @Test
	// public void testDoReconciliation() {
	// 	this.reconciliationEventObjectObserver.setReconciliationReconcileService(this.reconciliationReconcileService);
	//
	// 	ReconciliationEventObjectJournalData reconciliationEventObjectJournalData = new ReconciliationEventObjectJournalData();
	// 	reconciliationEventObjectJournalData.setAmount(BigDecimal.ONE);
	// 	ReconciliationEventObjectJournal eventObjectJournal_1 = createReconciliationEventObjectJournal(reconciliationEventObjectJournalData);
	// 	eventObjectJournal_1.setEventObjectDataType(this.interestExpenseEventObjectDataType);
	//
	// 	reconciliationEventObjectJournalData = new ReconciliationEventObjectJournalData();
	// 	reconciliationEventObjectJournalData.setAmount(BigDecimal.TEN);
	// 	ReconciliationEventObjectJournal eventObjectJournal_10 = createReconciliationEventObjectJournal(reconciliationEventObjectJournalData);
	// 	eventObjectJournal_10.setEventObjectDataType(this.cashContributionEventObjectDataType);
	//
	// 	this.cashReconciliationRunObject.getDataHolder().getDataObjectListBySource(this.primarySource).get(0).getData().put(ReconciliationRunDetail.CASH_BALANCE, "-9");
	// 	this.cashReconciliationRunObject.getDataHolder().getDataObjectListBySource(this.secondarySource).get(0).getData().put(ReconciliationRunDetail.CASH_BALANCE, "0");
	// 	this.cashReconciliationRunObject.setBreakDays(1);
	// 	this.cashReconciliationRunObject.getDataHolder().addError(new DataError());
	// 	this.cashReconciliationRunObject.getReconciliationRun().getRunDetails().setStatusCount(ReconciliationServiceUtils.RECONCILED_COUNT, 0);
	//
	// 	Mockito.when(this.reconciliationEvaluatorService.getReconciliationEvaluatorList(ArgumentMatchers.any())).thenReturn(Collections.emptyList());
	// 	Mockito.when(this.reconciliationDefinitionService.getReconciliationDefinitionPopulated(
	// 			ArgumentMatchers.eq(this.cashReconciliationDefinition.getId()), ArgumentMatchers.eq(this.cashReconciliationRun.getRunDate()))).thenReturn(this.cashReconciliationDefinition);
	// 	Mockito.when(this.reconciliationStatusService.getReconciliationReconcileStatusByStatusType(ArgumentMatchers.eq(ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED))).thenReturn(
	// 			ReconciliationReconcileStatusBuilder.aReconciliationReconcileStatus()
	// 					.withReconciled(ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED.isReconciled())
	// 					.withManual(ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED.isManual())
	// 					.withName(ReconciliationReconcileStatusTypes.MANUALLY_RECONCILED.name())
	// 					.withDescription("Indicates data has been manually reconciled.")
	// 					.withLabel("Manually Reconciled")
	// 					.build()
	// 	);
	// 	Mockito.verify(this.reconciliationInvestmentHandler, Mockito.times(1)).sendAccountUnlockEvents(ArgumentMatchers.eq(this.cashReconciliationRun));
	// }
	//
	//
	// @Test
	// public void testGetAdjustmentBalance() {
	// 	ReconciliationEventObjectJournalData reconciliationEventObjectJournalData = new ReconciliationEventObjectJournalData();
	// 	reconciliationEventObjectJournalData.setAmount(BigDecimal.ONE);
	// 	ReconciliationEventObjectJournal eventObjectJournal_1 = createReconciliationEventObjectJournal(reconciliationEventObjectJournalData);
	// 	eventObjectJournal_1.setEventObjectDataType(this.interestExpenseEventObjectDataType);
	//
	// 	reconciliationEventObjectJournalData = new ReconciliationEventObjectJournalData();
	// 	reconciliationEventObjectJournalData.setAmount(BigDecimal.TEN);
	// 	ReconciliationEventObjectJournal eventObjectJournal_10 = createReconciliationEventObjectJournal(reconciliationEventObjectJournalData);
	// 	eventObjectJournal_10.setEventObjectDataType(this.cashContributionEventObjectDataType);
	//
	// 	Mockito.when(this.reconciliationEventObjectService.getReconciliationEventObjectList(
	// 			ArgumentMatchers.argThat(sf -> sf != null && this.cashReconciliationRunObject.getId().equals(sf.getParentRunObjectId())
	// 					&& ReconciliationEventObjectCategories.SIMPLE_JOURNAL.name().equals(sf.getDataTypeCategory()))))
	// 			.thenReturn(Arrays.asList(eventObjectJournal_1, eventObjectJournal_10));
	//
	// 	//TODO - this has changed...adjustments
	// 	// BigDecimal totalAdjustments = this.reconciliationEventObjectObserver.getTotalAdjustmentAmount(eventObjectJournal_10);
	// 	// Assert.assertEquals(0, totalAdjustments.compareTo(new BigDecimal("9")));
	// }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconciliationEventObjectJournal createReconciliationEventObjectJournal(ReconciliationEventObjectJournalData data) {
		ReconciliationEventObjectJournal eventObject = new ReconciliationEventObjectJournal();
		eventObject.setEventObjectData(data);
		eventObject.setRunObject(this.transactionReconciliationRunObject);
		eventObject.setParentRunObject(this.cashReconciliationRunObject);
		return eventObject;
	}
}
