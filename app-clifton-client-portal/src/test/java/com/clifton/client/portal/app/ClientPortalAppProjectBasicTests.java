package com.clifton.client.portal.app;


import com.clifton.core.test.BasicProjectTests;
import com.clifton.portal.PortalContextService;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ClientPortalAppProjectBasicTests extends BasicProjectTests {

	@Resource
	private PortalContextService portalContextService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getProjectPrefix() {
		return "client-portal-app";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.springframework.boot.actuate.autoconfigure.");
		imports.add("org.springframework.boot.autoconfigure.");
		imports.add("org.springframework.boot.builder.SpringApplicationBuilder");
		imports.add("org.springframework.context.annotation.");
		imports.add("org.springframework.core.Ordered");
		imports.add("org.springframework.http.converter.");
		imports.add("org.springframework.web.multipart.commons.CommonsMultipartResolver");
		imports.add("org.springframework.web.servlet.");
		imports.add("org.springframework.boot.web.servlet.FilterRegistrationBean");
		imports.add("org.springframework.web.filter.DelegatingFilterProxy");
		imports.add("javax.servlet.");
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> basicProjectAllowedSuffixNamesList = super.getAllowedContextManagedBeanSuffixNames();
		basicProjectAllowedSuffixNamesList.add("Controller");
		return basicProjectAllowedSuffixNamesList;
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("org.springframework.boot.test.context.SpringBootTest");
	}


	@Override
	protected void configureRequiredApplicationContextBeans(Map<String, Object> beanMap) {
		beanMap.put("portalContextService", this.portalContextService);
	}
}
