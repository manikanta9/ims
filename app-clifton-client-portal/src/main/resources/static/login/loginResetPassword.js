{
	"success": false,
	"authenticationRequired": true,
	"loginFailed": true,
	"resetPassword": true,
	"errorMessage": "You must update your password at this time.",
	"redirectWindow": "Clifton.portal.security.user.UserPasswordResetWindow",
	"redirectWindowConfig": {
		"closable": false,
		"minimizable": false,
		"maximizable": false,
		"hideCancelButton": true,
		"okButtonWidth": 150,
		"okButtonText": "Change Password",
		"okButtonTooltip": "Update your Password"
	}
}
