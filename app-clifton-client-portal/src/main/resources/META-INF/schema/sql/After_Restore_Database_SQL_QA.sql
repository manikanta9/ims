-- DISABLE ALL NOTIFICATIONS
UPDATE PortalNotificationDefinition
SET IsActive = 0

-- ALSO UPDATE ALL LINKS IN NOTIFICATIONS
UPDATE PortalNotificationDefinition
SET EmailBodyTemplate = REPLACE(EmailBodyTemplate, 'https://portal.parametricportfolio.com', 'https://portal.qa-msp.paraport.com')
FROM PortalNotificationDefinition

UPDATE PortalNotificationDefinition
SET EmailBodyTemplate = REPLACE(EmailBodyTemplate, 'https://portal-admin.paraport.com', 'https://portal-admin.qa-msp.paraport.com')
FROM PortalNotificationDefinition

-- SETUP USERS
DECLARE @AdminRoleID INT
SELECT @AdminRoleID = PortalSecurityUserRoleID
FROM PortalSecurityUserRole
WHERE UserRoleName = 'Administrator'

-- CREATE QA-ONLY USERS
INSERT INTO PortalSecurityUser (PortalSecurityUserRoleID, SecurityUserName, UserEmailAddress, UserFirstName, UserLastName, UserTitle, UserCompanyName, UserPhoneNumber, PortalEntitySourceSystemID, SourceFKFieldID, SecurityUserPassword, PasswordUpdateDate, InvalidLoginAttemptCount, IsAccountLocked, IsDisabled, TermsOfUseAcceptanceDate, CreateUserID, CreateDate, UpdateUserID, UpdateDate, PasswordResetKey, PasswordResetKeyExpirationDateAndTime)
SELECT @AdminRoleID, 'JonathanE', 'jeytinge@paraport.com', 'Jon', 'Eytinge', 'Senior Software Engineer in Test', 'Parametric Seattle', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0,
	   NULL, 0, GETDATE(), 0, GETDATE(), NULL, NULL
WHERE NOT EXISTS(SELECT * FROM PortalSecurityUser psu WHERE psu.SecurityUserName = 'JonathanE')

-- MAKE DEVELOPERS ADMINS
UPDATE PortalSecurityUser
SET PortalSecurityUserRoleID = @AdminRoleID
WHERE SecurityUserName IN ('mikeh')


-- Obfuscate all External User Email Addresses
UPDATE u SET SecurityUserName = REPLACE(u.SecurityUserName, '@','@-NonProd-')
	, u.UserEmailAddress = REPLACE(u.UserEmailAddress, '@','@-NonProd-')
FROM PortalSecurityUser u
INNER JOIN PortalSecurityUserRole r ON u.PortalSecurityUserRoleID = r.PortalSecurityUserRoleID
WHERE r.IsPortalEntitySpecific = 1
AND u.SecurityUserName NOT LIKE '%@-NonProd-%'
