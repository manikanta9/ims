-- DISABLE ALL NOTIFICATIONS
UPDATE PortalNotificationDefinition
SET IsActive = 0

-- ALSO UPDATE ALL LINKS IN NOTIFICATIONS
UPDATE PortalNotificationDefinition
SET EmailBodyTemplate = REPLACE(EmailBodyTemplate, 'https://portal.parametricportfolio.com', 'https://portal.qa-msp.paraport.com')
FROM PortalNotificationDefinition

UPDATE PortalNotificationDefinition
SET EmailBodyTemplate = REPLACE(EmailBodyTemplate, 'https://portal-admin.paraport.com', 'https://portal-admin.qa-msp.paraport.com')
FROM PortalNotificationDefinition

-- LOCALLY MAKE ALL DEVELOPERS ADMINS
DECLARE
	@AdminRoleID INT
SELECT @AdminRoleID = PortalSecurityUserRoleID
FROM PortalSecurityUserRole
WHERE UserRoleName = 'Administrator'

UPDATE PortalSecurityUser
SET PortalSecurityUserRoleID = @AdminRoleID
WHERE SecurityUserName IN ('mikeh', 'nickk', 'StevenF', 'terrys', 'theodorez', 'davidi', 'danielh', 'laurenn', 'dillonm')

-- Obfuscate all External User Email Addresses
UPDATE u SET SecurityUserName = REPLACE(u.SecurityUserName, '@','@-NonProd-')
	, u.UserEmailAddress = REPLACE(u.UserEmailAddress, '@','@-NonProd-')
FROM PortalSecurityUser u
INNER JOIN PortalSecurityUserRole r ON u.PortalSecurityUserRoleID = r.PortalSecurityUserRoleID
WHERE r.IsPortalEntitySpecific = 1
AND u.SecurityUserName NOT LIKE '%@-NonProd-%'


