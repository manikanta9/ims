package com.clifton.client.portal.app.web.filter;

import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;


@Component
public class ResponseCharacterEncodingFilter implements Filter {

	private String encoding = "UTF-8";


	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
	                     FilterChain chain) throws IOException, ServletException {

		response.setCharacterEncoding(this.encoding);
		chain.doFilter(request, response);
	}


	@Override
	public void init(FilterConfig config) throws ServletException {
		String encodingValue = config.getInitParameter("encoding");
		if (!StringUtils.isEmpty(encodingValue)) {
			this.encoding = encodingValue;
		}
	}


	@Override
	public void destroy() {
		// nothing to do
	}


	public String getEncoding() {
		return this.encoding;
	}


	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
}
