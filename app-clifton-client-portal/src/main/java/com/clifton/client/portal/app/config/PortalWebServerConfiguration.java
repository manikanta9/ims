package com.clifton.client.portal.app.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;


/**
 * @author dillonm
 */
public class PortalWebServerConfiguration {


	@Configuration
	public static class PortalFilterConfiguration {


		@Bean
		public FilterRegistrationBean<DelegatingFilterProxy> portalSecurityUserManagementFilterRegistration() {
			DelegatingFilterProxy filter = new DelegatingFilterProxy();
			FilterRegistrationBean<DelegatingFilterProxy> registration = new FilterRegistrationBean<>(filter);
			registration.setName("portalSecurityUserManagementFilter");
			registration.setOrder(200);
			registration.addInitParameter("targetFilterLifecycle", "true");
			registration.addUrlPatterns("/oauth/authorize", "/oauth/token", "*.json", "/ws/*");
			return registration;
		}


		@Bean
		public FilterRegistrationBean<DelegatingFilterProxy> characterFilter() {
			DelegatingFilterProxy filter = new DelegatingFilterProxy();
			FilterRegistrationBean<DelegatingFilterProxy> registration = new FilterRegistrationBean<>(filter);
			registration.setName("responseCharacterEncodingFilter");
			registration.addUrlPatterns("*");
			registration.setOrder(50);
			return registration;
		}
	}
}
