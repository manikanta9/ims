package com.clifton.client.portal.app;

import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.security.impersonation.PortalSecurityImpersonationHandler;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserAssignment;
import com.clifton.portal.security.user.PortalSecurityUserAssignmentResource;
import com.clifton.portal.security.user.PortalSecurityUserManagementService;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.expanded.PortalSecurityUserAssignmentExpandedRebuildService;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalTestUserGenerator</code> is used by app-clifton-client-portal-test migrations that can be useful to create test users
 * This is currently used by the Demo environment to create test users for Penetration testing
 *
 * @author manderson
 */
@Component
public class PortalTestUserGenerator {

	private PortalEntityService portalEntityService;

	private PortalSecurityImpersonationHandler portalSecurityImpersonationHandler;

	private PortalSecurityUserAssignmentExpandedRebuildService portalSecurityUserAssignmentExpandedRebuildService;

	private PortalSecurityUserManagementService portalSecurityUserManagementService;
	private PortalSecurityUserService portalSecurityUserService;


	private DaoSingleKeyListCache<PortalSecurityUserAssignmentResource, Integer> portalSecurityUserAssignmentResourceListByUserAssignmentCache;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a test user (if doesn't exist) and assignment.  User will be given access to all possible for the given role
	 * Only pass password if you are creating a new user - if you are using this method to add another assignmnet, leave password blank
	 */
	public void createTestUserAndAssignment(String userName, int assignedPortalEntityId, String assignmentRoleName, String password) {
		getPortalSecurityImpersonationHandler().runAsSystemUser(() -> {
			ValidationUtils.assertNotEmpty(userName, "User name is required");
			ValidationUtils.assertNotEmpty(userName, "Assignment Role Name is required");
			PortalEntity assignedEntity = getPortalEntityService().getPortalEntity(assignedPortalEntityId);
			PortalSecurityUser user = getPortalSecurityUserService().getPortalSecurityUserByUserName(userName, false);
			if (user == null) {
				user = new PortalSecurityUser();
				user.setUsername(userName);
				user.setFirstName(StringUtils.formatStringUpToNCharsWithDots(assignedEntity.getName(), 50));
				user.setLastName(assignmentRoleName);
				if (!StringUtils.isEmpty(password)) {
					user.setPassword(password);
				}
				user = getPortalSecurityUserManagementService().savePortalSecurityUser(user);
			}
			else if (!StringUtils.isEmpty(password)) {
				throw new ValidationException("User [" + userName + "] already exists, cannot set a new password");
			}

			PortalSecurityUserAssignment userAssignment = new PortalSecurityUserAssignment();
			userAssignment.setSecurityUser(user);
			userAssignment.setAssignmentRole(getPortalSecurityUserService().getPortalSecurityUserRoleByName(assignmentRoleName));
			userAssignment.setPortalEntity(assignedEntity);
			userAssignment.setUserAssignmentResourceList(getPortalSecurityUserService().getPortalSecurityUserAssignmentResourceListForEntityAndRole(assignedEntity.getId(), userAssignment.getAssignmentRole().getId()));
			// Enable Access to all possible
			for (PortalSecurityUserAssignmentResource resource : CollectionUtils.getIterable(userAssignment.getUserAssignmentResourceList())) {
				resource.setAccessAllowed(true);
			}
			userAssignment = getPortalSecurityUserService().savePortalSecurityUserAssignment(userAssignment);

			// Not sure why through migrations this needs to be done, but seems like for any user except portal admin the rebuild is pulling stale data
			getPortalSecurityUserAssignmentResourceListByUserAssignmentCache().clearBeanListForKeyValue(userAssignment.getId());
			// Rebuild
			getPortalSecurityUserAssignmentExpandedRebuildService().rebuildPortalSecurityUserAssignmentExpandedForUser(user.getId());
		});
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalSecurityImpersonationHandler getPortalSecurityImpersonationHandler() {
		return this.portalSecurityImpersonationHandler;
	}


	public void setPortalSecurityImpersonationHandler(PortalSecurityImpersonationHandler portalSecurityImpersonationHandler) {
		this.portalSecurityImpersonationHandler = portalSecurityImpersonationHandler;
	}


	public PortalSecurityUserManagementService getPortalSecurityUserManagementService() {
		return this.portalSecurityUserManagementService;
	}


	public void setPortalSecurityUserManagementService(PortalSecurityUserManagementService portalSecurityUserManagementService) {
		this.portalSecurityUserManagementService = portalSecurityUserManagementService;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public DaoSingleKeyListCache<PortalSecurityUserAssignmentResource, Integer> getPortalSecurityUserAssignmentResourceListByUserAssignmentCache() {
		return this.portalSecurityUserAssignmentResourceListByUserAssignmentCache;
	}


	public void setPortalSecurityUserAssignmentResourceListByUserAssignmentCache(DaoSingleKeyListCache<PortalSecurityUserAssignmentResource, Integer> portalSecurityUserAssignmentResourceListByUserAssignmentCache) {
		this.portalSecurityUserAssignmentResourceListByUserAssignmentCache = portalSecurityUserAssignmentResourceListByUserAssignmentCache;
	}


	public PortalSecurityUserAssignmentExpandedRebuildService getPortalSecurityUserAssignmentExpandedRebuildService() {
		return this.portalSecurityUserAssignmentExpandedRebuildService;
	}


	public void setPortalSecurityUserAssignmentExpandedRebuildService(PortalSecurityUserAssignmentExpandedRebuildService portalSecurityUserAssignmentExpandedRebuildService) {
		this.portalSecurityUserAssignmentExpandedRebuildService = portalSecurityUserAssignmentExpandedRebuildService;
	}
}
