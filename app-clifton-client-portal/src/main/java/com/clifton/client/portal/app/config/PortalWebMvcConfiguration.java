package com.clifton.client.portal.app.config;

import com.clifton.core.context.AutowireByNameBean;
import com.clifton.core.context.BeanModifier;
import com.clifton.core.context.BeanModifierTarget;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.converter.json.JsonValueAppender;
import com.clifton.core.converter.json.MapWithRulesToJsonStringWriter;
import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.converter.json.jackson.JacksonObjectMapper;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.web.api.client.ExternalServiceArgumentResolver;
import com.clifton.core.web.api.client.ExternalServiceReturnValueHandler;
import com.clifton.core.web.bind.WebBindingDataRetrieverUsingDaoLocator;
import com.clifton.core.web.bind.WebBindingHandler;
import com.clifton.core.web.bind.WebBindingHandlerImpl;
import com.clifton.core.web.bind.WebBindingInitializerWithValidation;
import com.clifton.core.web.converter.BigDecimalHttpMessageConverter;
import com.clifton.core.web.converter.BooleanHttpMessageConverter;
import com.clifton.core.web.converter.DateHttpMessageConverter;
import com.clifton.core.web.converter.IntegerHttpMessageConverter;
import com.clifton.core.web.handler.LoggingExceptionResolver;
import com.clifton.core.web.mvc.ConventionBasedRequestMappingHandlerMapping;
import com.clifton.core.web.mvc.DelegatingHandlerAdapter;
import com.clifton.core.web.mvc.ValidatingRequestMappingHandlerAdapter;
import com.clifton.core.web.mvc.ValidatingServletModelAttributeMethodProcessor;
import com.clifton.core.web.stats.RequestProcessorManagementServiceImpl;
import com.clifton.core.web.view.DynamicRequestToViewNameTranslator;
import com.clifton.core.web.view.DynamicViewResolver;
import com.clifton.core.web.view.FileDownloadView;
import com.clifton.core.web.view.FileUploadView;
import com.clifton.core.web.view.JsonView;
import com.clifton.portal.converter.json.appenders.PortalSecurityUserValueAppender;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.web.mvc.PortalSecureHandlerInterceptorAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.RequestToViewNameTranslator;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.support.WebContentGenerator;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;


/**
 * @author dillonm
 */
@Configuration
public class PortalWebMvcConfiguration {

	/**
	 * map requested URL's to corresponding @Service component methods annotated with @RequestMapping or those that follow our conventions
	 */
	@Configuration
	public static class HandlerMappingConfiguration {


		@AutowireByNameBean
		public HandlerMapping handlerMapping(PortalSecureHandlerInterceptorAdapter portalSecureHandlerInterceptorAdapter) {
			ConventionBasedRequestMappingHandlerMapping conventionBasedRequestMappingHandlerMapping = new ConventionBasedRequestMappingHandlerMapping();
			// Execute early for performance, as most requests will be resolved by this mapping
			conventionBasedRequestMappingHandlerMapping.setOrder(0);
			//indicates that only classes annotated with @Controller will be exposed externally - all others will be prefixed with /api/
			conventionBasedRequestMappingHandlerMapping.setExposeControllersOnly(true);
			//instead of auto-discovering duplicate service in web context, enable ancestor look ups
			conventionBasedRequestMappingHandlerMapping.setDetectHandlerMethodsInAncestorContexts(true);
			conventionBasedRequestMappingHandlerMapping.setInterceptors(portalSecureHandlerInterceptorAdapter);
			return conventionBasedRequestMappingHandlerMapping;
		}


		@AutowireByNameBean
		public PortalSecureHandlerInterceptorAdapter portalSecureHandlerInterceptorAdapter() {
			return new PortalSecureHandlerInterceptorAdapter();
		}


		/**
		 * Enables <code>POST</code> request handling for all registered mappings.
		 */
		@BeanModifierTarget("resourceHandlerMapping")
		public BeanModifier<SimpleUrlHandlerMapping> resourceHandlerMappingModifier() {
			return (bean, beanName) -> {
				WebContentGenerator currentValue = (WebContentGenerator) bean.getHandlerMap().get("/**");
				if (currentValue != null) {
					currentValue.setSupportedMethods(ArrayUtils.add(currentValue.getSupportedMethods(), "POST", 0));
				}
			};
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This adapter will delegate to the externalServiceHandlerAdapter if the request URI contains /api/
	 * otherwise will delegate to the webMethodHandlerAdapter
	 */
	@Configuration
	public static class DelegatingHandlerAdapterConfiguration {


		@AutowireByNameBean
		public DelegatingHandlerAdapter handlerAdapter(RequestMappingHandlerAdapter externalServiceHandlerAdapter, ValidatingRequestMappingHandlerAdapter requestMappingHandlerAdapter) {
			DelegatingHandlerAdapter delegatingHandlerAdapter = new DelegatingHandlerAdapter();
			LinkedHashMap<Pattern, HandlerAdapter> urlPatternToAdapterMap = new LinkedHashMap<>();
			urlPatternToAdapterMap.put(Pattern.compile(".*" + ContextConventionUtils.API_ROOT + "/.*"), externalServiceHandlerAdapter);
			urlPatternToAdapterMap.put(Pattern.compile(".*"), requestMappingHandlerAdapter);
			delegatingHandlerAdapter.setUrlPatternToHandlerAdapterMap(urlPatternToAdapterMap);
			return delegatingHandlerAdapter;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Web binding handler and associated data retrievers must be initialized in the same context for beans to be available during autowiring
	 */
	@Configuration
	public static class WebBindingConfiguration {


		@AutowireByNameBean
		public WebBindingHandler webBindingHandler() {
			return new WebBindingHandlerImpl();
		}


		@AutowireByNameBean
		public WebBindingDataRetrieverUsingDaoLocator webBindingDataRetrieverUsingDaoLocator() {
			WebBindingDataRetrieverUsingDaoLocator webBindingDataRetrieverUsingDaoLocator = new WebBindingDataRetrieverUsingDaoLocator();
			webBindingDataRetrieverUsingDaoLocator.setOrder(100);
			return webBindingDataRetrieverUsingDaoLocator;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Configuration
	public static class HandlerAdapterConfiguration {

		/**
		 * map requested URL's to corresponding methods marked with @RequestMapping
		 * runs our method level authorization to make sure that the user has access to the method
		 * performs advanced binding for DAO updates by retrieving existing bean from DB and overriding only submitted properties
		 * if requested, performs bean property validation based on our metadata (field length, required, etc.)
		 */
		@AutowireByNameBean
		public ValidatingRequestMappingHandlerAdapter requestMappingHandlerAdapter() {
			ValidatingRequestMappingHandlerAdapter handlerAdapter = new ValidatingRequestMappingHandlerAdapter();
			handlerAdapter.setWebBindingInitializer(webBindingInitializer());
			handlerAdapter.setCustomArgumentResolvers(CollectionUtils.createList(validatingServletModelAttributeMethodProcessor()));
			//preserve default converters and add our custom Date and BigDecimal converters
			handlerAdapter.setMessageConverters(CollectionUtils.createList(
					byteArrayHttpMessageConverter(),
					stringHttpMessageConverter(),
					sourceHttpMessageConverter(),
					mappingJackson2HttpMessageConverter(),
					allEncompassingFormHttpMessageConverter(),
					dateHttpMessageConverter(),
					bigDecimalHttpMessageConverter(),
					booleanHttpMessageConverter(),
					integerHttpMessageConverter()
			));
			return handlerAdapter;
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@AutowireByNameBean
		public WebBindingInitializerWithValidation webBindingInitializer() {
			return new WebBindingInitializerWithValidation();
		}


		@AutowireByNameBean
		public ValidatingServletModelAttributeMethodProcessor validatingServletModelAttributeMethodProcessor() {
			return new ValidatingServletModelAttributeMethodProcessor(true);
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@Bean
		public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
			return new ByteArrayHttpMessageConverter();
		}


		@Bean
		public StringHttpMessageConverter stringHttpMessageConverter() {
			return new StringHttpMessageConverter();
		}


		@Bean
		public SourceHttpMessageConverter<?> sourceHttpMessageConverter() {
			return new SourceHttpMessageConverter<>();
		}


		@Bean
		public AllEncompassingFormHttpMessageConverter allEncompassingFormHttpMessageConverter() {
			return new AllEncompassingFormHttpMessageConverter();
		}


		@Bean
		public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
			return new MappingJackson2HttpMessageConverter();
		}


		@Bean
		public DateHttpMessageConverter dateHttpMessageConverter() {
			return new DateHttpMessageConverter();
		}


		@Bean
		public BigDecimalHttpMessageConverter bigDecimalHttpMessageConverter() {
			return new BigDecimalHttpMessageConverter();
		}


		@Bean
		public BooleanHttpMessageConverter booleanHttpMessageConverter() {
			return new BooleanHttpMessageConverter();
		}


		@Bean
		public IntegerHttpMessageConverter integerHttpMessageConverter() {
			return new IntegerHttpMessageConverter();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Configuration
	public static class ExternalServiceHandlerAdapterConfiguration {


		@Bean
		public RequestMappingHandlerAdapter externalServiceHandlerAdapter(ExternalServiceArgumentResolver externalServiceArgumentResolver, ExternalServiceReturnValueHandler externalServiceReturnValueHandler) {
			RequestMappingHandlerAdapter requestMappingHandlerAdapter = new RequestMappingHandlerAdapter();
			requestMappingHandlerAdapter.setCustomArgumentResolvers(CollectionUtils.createList(externalServiceArgumentResolver));
			requestMappingHandlerAdapter.setReturnValueHandlers(CollectionUtils.createList(externalServiceReturnValueHandler));
			return requestMappingHandlerAdapter;
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		/*
		 *  allows us to post application/json and process it without having
		 * 	to explicitly annotate params with @RequestBody, it creates a new RequestResponseBodyMethodProcessor
		 * 	initialized a new MappingJackson2HttpMessageConverter with an implementation of one of our ObjectMapper
		 * 	configurations and adds the message converter to the resolver.
		 */
		@AutowireByNameBean
		public ExternalServiceArgumentResolver externalServiceArgumentResolver(JacksonHandlerImpl externalServiceJacksonHandler) {
			ExternalServiceArgumentResolver externalServiceArgumentResolver = new ExternalServiceArgumentResolver();
			externalServiceArgumentResolver.setJacksonHandler(externalServiceJacksonHandler);
			return externalServiceArgumentResolver;
		}


		@AutowireByNameBean
		public ExternalServiceReturnValueHandler externalServiceReturnValueHandler(JacksonHandlerImpl externalServiceJacksonHandler) {
			ExternalServiceReturnValueHandler externalServiceReturnValueHandler = new ExternalServiceReturnValueHandler();
			externalServiceReturnValueHandler.setJacksonHandler(externalServiceJacksonHandler);
			return externalServiceReturnValueHandler;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Configuration
	public static class ExceptionHandlerConfiguration {

		/**
		 * log non ValidationException as errors into error log
		 * also tries to analyze database exceptions and covert them to corresponding ValidationException when possible
		 */
		@AutowireByNameBean
		public LoggingExceptionResolver webExceptionResolver(JacksonHandlerImpl externalServiceJacksonHandler) {
			LoggingExceptionResolver loggingExceptionResolver = new LoggingExceptionResolver();
			loggingExceptionResolver.setDefaultErrorView(DynamicViewResolver.DYNAMIC_VIEW_NAME);
			Map<Pattern, String> urlPatternToErrorViewNameMap = new HashMap<>();
			urlPatternToErrorViewNameMap.put(Pattern.compile(".*" + ContextConventionUtils.API_ROOT + "/.*"), "externalServiceJacksonErrorView");
			loggingExceptionResolver.setUrlPatternToErrorViewNameMap(urlPatternToErrorViewNameMap);
			Properties statusCodes = new Properties();
			statusCodes.put("externalServiceJacksonErrorView", "418");
			loggingExceptionResolver.setStatusCodes(statusCodes);
			loggingExceptionResolver.setJacksonHandler(externalServiceJacksonHandler);
			return loggingExceptionResolver;
		}


		@Bean
		public WebMvcConfigurer webExceptionResolverConfigurer(HandlerExceptionResolver webExceptionResolver) {
			// Select our custom exception resolver as the singular exception resolver to include during processing
			return new WebMvcConfigurer() {
				@Override
				public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> resolvers) {
					resolvers.add(webExceptionResolver);
				}
			};
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Configuration
	public static class ViewConfiguration {

		@Bean
		public WebMvcConfigurer viewConfigurationWebMvcConfigurer() {
			return new WebMvcConfigurer() {
				@Override
				public void addViewControllers(ViewControllerRegistry registry) {
					registry.addViewController("/").setViewName("forward:/dist/index.html");
				}
			};
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		/**
		 * The view name provider used by Spring when no explicit view name is provided for the request.
		 */
		@AutowireByNameBean(name = DispatcherServlet.REQUEST_TO_VIEW_NAME_TRANSLATOR_BEAN_NAME)
		public RequestToViewNameTranslator requestToViewNameTranslator() {
			return new DynamicRequestToViewNameTranslator();
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		/**
		 * The standard view resolver, overriding the default view resolver provided by {WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter#viewResolver(BeanFactory)}.
		 */
		@AutowireByNameBean(name = DispatcherServlet.VIEW_RESOLVER_BEAN_NAME)
		public ViewResolver viewResolver(FileUploadView fileUploadView, FileDownloadView<?> fileDownloadView, JsonView webJsonView) {
			Map<Pattern, View> urlPatternMap = new HashMap<>();
			urlPatternMap.put(Pattern.compile(".*" + ContextConventionUtils.API_ROOT + "/.*"), externalServiceJacksonView());
			urlPatternMap.put(Pattern.compile(".*Upload.json"), fileUploadView);
			urlPatternMap.put(Pattern.compile(".*Download.json"), fileDownloadView);

			DynamicViewResolver resolver = new DynamicViewResolver();
			resolver.setDefaultDownloadView(fileDownloadView);
			resolver.setUrlPatternToViewMap(urlPatternMap);
			resolver.setStaticView(webJsonView);
			resolver.setOrder(Ordered.HIGHEST_PRECEDENCE);
			return resolver;
		}


		// TODO: Should be replaced with org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter.beanNameViewResolver; auto-configuration ordering issue seems to prevent this
		@Bean
		public ViewResolver beanNameViewResolver() {
			BeanNameViewResolver beanNameViewResolver = new BeanNameViewResolver();
			beanNameViewResolver.setOrder(100); // Execute this before the InternalResourceViewResolver
			return beanNameViewResolver;
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		/**
		 * Before converting to Spring Boot, this bean had its own object mapper used by <code>externalServiceJacksonHandler</code>. The below code will
		 * initialize it to use normal <code>ObjectMapper</code> class. So far, no issues have come from this, but if any bugs start
		 * appearing this may need to be updated.
		 **/
		@Bean
		public MappingJackson2JsonView externalServiceJacksonView() {
			MappingJackson2JsonView view = new MappingJackson2JsonView();
			view.setObjectMapper(objectMapper());
			view.setExtractValueFromSingleKeyModel(true);
			return view;
		}


		/**
		 * We use this view so we can set singleKeyModel to false - this allows us to wrap the response with 'exception'
		 * so we can reliably tell when the response is an exception by looking for that property name in the jsonResponse
		 * <p>
		 * Before converting to Spring Boot, this bean had its own object mapper used by <code>externalServiceJacksonHandler</code>. The below code will
		 * initialize it to use normal <code>ObjectMapper</code> class. So far, no issues have come from this, but if any bugs start
		 * appearing this may need to be updated.
		 */
		@Bean
		public MappingJackson2JsonView externalServiceJacksonErrorView() {
			MappingJackson2JsonView mappingJackson2JsonView = new MappingJackson2JsonView();
			mappingJackson2JsonView.setObjectMapper(objectMapper());
			mappingJackson2JsonView.setExtractValueFromSingleKeyModel(true);
			return mappingJackson2JsonView;
		}


		@AutowireByNameBean
		public static JacksonObjectMapper objectMapper() {
			return new JacksonObjectMapper();
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@AutowireByNameBean
		public MapWithRulesToJsonStringWriter mapWithRulesToJsonStringWriter(PortalSecurityUserValueAppender portalSecurityUserValueAppender) {
			MapWithRulesToJsonStringWriter mapWithRulesToJsonStringWriter = new MapWithRulesToJsonStringWriter();
			HashMap<Class<?>, JsonValueAppender> appenderMap = new HashMap<>();
			appenderMap.put(PortalSecurityUser.class, portalSecurityUserValueAppender);
			mapWithRulesToJsonStringWriter.setAdditionalAppenderMap(appenderMap);
			return mapWithRulesToJsonStringWriter;
		}


		@AutowireByNameBean
		public PortalSecurityUserValueAppender portalSecurityUserValueAppender() {
			return new PortalSecurityUserValueAppender();
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@AutowireByNameBean
		public JsonView webJsonView(MapWithRulesToJsonStringWriter mapWithRulesToJsonStringWriter) {
			JsonView view = new JsonView();
			view.setRootNodeName("data");
			view.setSuccessPropertyName("success");
			view.setResponseBufferSize((int) Math.pow(2, 16)); // 64 KB
			view.setResponseLimitSize((long) Math.pow(2, 30)); // 1 GB
			view.setFilteringJsonWriter(mapWithRulesToJsonStringWriter);
			return view;
		}


		@AutowireByNameBean
		public FileDownloadView<?> fileDownloadView(MapWithRulesToJsonStringWriter mapWithRulesToJsonStringWriter) {
			FileDownloadView<?> view = new FileDownloadView<>();
			view.setRootNodeName("data");
			view.setSuccessPropertyName("success");
			view.setResponseBufferSize((int) Math.pow(2, 16)); // 64 KB
			view.setResponseLimitSize((long) Math.pow(2, 30)); // 1 GB
			view.setFilteringJsonWriter(mapWithRulesToJsonStringWriter);
			return view;
		}


		@AutowireByNameBean
		public FileUploadView fileUploadView(MapWithRulesToJsonStringWriter mapWithRulesToJsonStringWriter) {
			FileUploadView view = new FileUploadView();
			view.setRootNodeName("data");
			view.setSuccessPropertyName("success");
			view.setResponseBufferSize((int) Math.pow(2, 16)); // 64 KB
			view.setResponseLimitSize((long) Math.pow(2, 30)); // 1 GB
			view.setFilteringJsonWriter(mapWithRulesToJsonStringWriter);
			return view;
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		/**
		 * The service component for managing request processors. This is defined here so in order to provide access to "view" bean references, such as "webJsonView".
		 */
		@AutowireByNameBean
		public RequestProcessorManagementServiceImpl requestProcessorManagementService(JsonView webJsonView) {
			RequestProcessorManagementServiceImpl requestProcessorManagementService = new RequestProcessorManagementServiceImpl();
			requestProcessorManagementService.setJsonView(webJsonView);
			return requestProcessorManagementService;
		}
	}
}
