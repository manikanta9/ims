package com.clifton.notification.definition;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.notification.definition.search.NotificationDefinitionSearchForm;
import com.clifton.notification.generator.Notification;
import com.clifton.notification.generator.NotificationGeneratorService;
import com.clifton.notification.generator.NotificationSearchForm;
import com.clifton.system.schema.SystemTable;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>NotificationDefinitionServiceImplTests</code> performs tests
 * for the {@link NotificationDefinitionService} methods
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class NotificationDefinitionServiceImplTests<T extends NotificationDefinition> {

	@Resource
	private NotificationDefinitionService<T> notificationDefinitionService;

	@Resource
	private NotificationGeneratorService<T> notificationGeneratorService;

	@Resource
	private ReadOnlyDAO<Notification> notificationDAO;

	@Resource
	private ReadOnlyDAO<SystemTable> systemTableDAO;


	@Test
	public void testGetNotificationDefinitionList() {
		// Generic List Getters
		List<NotificationDefinition> defList = this.notificationDefinitionService.getNotificationDefinitionGenericList(new NotificationDefinitionSearchForm());
		Assertions.assertEquals(8, defList.size());

		// Test Instant List Getter
		List<NotificationDefinitionInstant> instantList = this.notificationDefinitionService.getNotificationDefinitionInstantList(true);
		Assertions.assertEquals(3, instantList.size());
		instantList = this.notificationDefinitionService.getNotificationDefinitionInstantList(false);
		Assertions.assertEquals(3, instantList.size());
	}


	@Test
	public void testCopyNotificationDefinition() {
		NotificationDefinition original = this.notificationDefinitionService.getNotificationDefinition(200);
		List<NotificationRecipient> originalRecList = this.notificationDefinitionService.getNotificationRecipientListByNotificationDefinition(original.getId());

		NotificationDefinition copy = this.notificationDefinitionService.copyNotificationDefinition(200, "Copy Of: " + original.getName());

		Assertions.assertFalse(original.getId().equals(copy.getId()));
		Assertions.assertEquals("Copy Of: " + original.getName(), copy.getName());
		Assertions.assertEquals(original.getTable(), copy.getTable());
		Assertions.assertEquals(original.getPriority(), copy.getPriority());
		Assertions.assertEquals(original.getPriority().getLabel(), copy.getPriority().getLabel());
		Assertions.assertEquals(original.getSubject(), copy.getSubject());

		List<NotificationRecipient> copyRecList = this.notificationDefinitionService.getNotificationRecipientListByNotificationDefinition(copy.getId());
		Assertions.assertEquals(originalRecList.size(), copyRecList.size());
		Assertions.assertEquals(1, originalRecList.size());
		NotificationRecipient origRec = originalRecList.get(0);
		NotificationRecipient copyRec = copyRecList.get(0);

		Assertions.assertEquals(original, origRec.getNotificationDefinition());
		Assertions.assertEquals(copy, copyRec.getNotificationDefinition());
		Assertions.assertEquals(origRec.getSecurityUser(), copyRec.getSecurityUser());
		Assertions.assertEquals(origRec.getSecurityGroup(), copyRec.getSecurityGroup());

		// Delete the copy so won't affect other tests
		Mockito.when(this.notificationGeneratorService.getNotificationListSecure(ArgumentMatchers.any(NotificationSearchForm.class))).thenReturn(
				this.notificationDAO.findByField("definition.id", copy.getId()));
		this.notificationDefinitionService.deleteNotificationDefinition(copy.getId());
	}


	@Test
	public void testLinkNotificationDefinitionRecipients() {
		// Before adding recipients, list size should be 1
		List<NotificationRecipient> list = this.notificationDefinitionService.getNotificationRecipientListByNotificationDefinition(200);
		Assertions.assertEquals(1, list.size());

		// Link Group with ID 1
		this.notificationDefinitionService.linkNotificationDefinitionToSecurityGroup(MathUtils.SHORT_ONE, 200);
		// Link User with ID 3
		this.notificationDefinitionService.linkNotificationDefinitionToSecurityUser(MathUtils.SHORT_THREE, 200);

		// After adding recipients, list size should be 3
		list = this.notificationDefinitionService.getNotificationRecipientListByNotificationDefinition(200);
		Assertions.assertEquals(3, list.size());

		for (NotificationRecipient rec : list) {
			String label = rec.getLabel();
			if (rec.getSecurityGroup() != null) {
				Assertions.assertEquals("Group: " + rec.getSecurityGroup().getLabel(), label);
				if (MathUtils.isEqual(MathUtils.SHORT_ONE, rec.getSecurityGroup().getId())) {
					// Clean Up: Remove Links
					this.notificationDefinitionService.deleteNotificationRecipient(rec.getId());
				}
			}
			else {
				Assertions.assertEquals("User: " + rec.getSecurityUser().getLabel(), label);
				if (MathUtils.isEqual(MathUtils.SHORT_THREE, rec.getSecurityUser().getId())) {
					// Clean Up: Remove Links
					this.notificationDefinitionService.deleteNotificationRecipient(rec.getId());
				}
			}
		}
	}


	@Test
	public void testNotificationDefinitionInstantActiveValidation() {
		Assertions.assertThrows(ValidationException.class, () -> {
			// Cannot be active without a dao event selected.
			NotificationDefinitionInstant instant = (NotificationDefinitionInstant) this.notificationDefinitionService.getNotificationDefinition(200);
			instant.setInsertEvent(false);
			instant.setUpdateEvent(false);
			instant.setDeleteEvent(false);
			instant.setActive(true);
			this.notificationDefinitionService.saveNotificationDefinitionInstant(instant);
		});
	}


	@Test
	public void testNotificationDefinitionBatchActiveValidation() {
		Assertions.assertThrows(ValidationException.class, () -> {
			// Cannot be active without a Calendar Schedule Selected
			NotificationDefinitionBatch batch = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(250);
			batch.setCalendarSchedule(null);
			batch.setActive(true);
			this.notificationDefinitionService.saveNotificationDefinitionBatch(batch);
		});
	}


	@Test
	public void testNotificationDefinitionBatchSystemQueryValidation() {
		Assertions.assertThrows(ValidationException.class, () -> {
			// System Query is Required for Batch, but not required at DB Level.
			NotificationDefinitionBatch batch = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(250);
			batch.setSystemQuery(null);
			this.notificationDefinitionService.saveNotificationDefinitionBatch(batch);
		});
	}


	@Test
	public void testNotificationDefinitionBatchSystemQueryTableValidation() {
		Assertions.assertThrows(ValidationException.class, () -> {
			// System Query's Table and Definition Table must be the same
			NotificationDefinitionBatch batch = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(250);
			batch.setTable(this.systemTableDAO.findByPrimaryKey(2));
			this.notificationDefinitionService.saveNotificationDefinitionBatch(batch);
		});
	}
}
