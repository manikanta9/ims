package com.clifton.notification;


import com.clifton.core.beans.NamedEntity;

import java.math.BigDecimal;


/**
 * The <code>NotificationTestEntity</code> is a test entity used
 * to test the Notification classes.
 *
 * @author manderson
 */
public class NotificationTestEntity extends NamedEntity<Integer> {

	private BigDecimal amount;

	private BigDecimal percentage;

	private BigDecimal price;


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public BigDecimal getPercentage() {
		return this.percentage;
	}


	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
