package com.clifton.notification.subscription;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventContext;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionInstant;
import com.clifton.notification.definition.NotificationDefinitionService;
import com.clifton.notification.definition.NotificationRecipient;
import com.clifton.notification.generator.Notification;
import com.clifton.notification.subscription.message.NotificationCountSubscriptionMessage;
import com.clifton.notification.subscription.message.NotificationPopupSubscriptionMessage;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserGroup;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.priority.SystemPriority;
import com.clifton.websocket.WebSocketHandler;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Stream;


/**
 * Unit tests for the {@link NotificationSubscriptionObserver} class.
 *
 * @author MikeH
 */
public class NotificationSubscriptionObserverTests {

	/* Constants */
	private static final String COUNT = NotificationSubscriptionService.CHANNEL_USER_TOPIC_NOTIFICATION_COUNT;
	private static final String POPUP = NotificationSubscriptionService.CHANNEL_USER_TOPIC_NOTIFICATION_POPUP;
	private static final NotificationCountSubscriptionMessage INCREMENT = NotificationCountSubscriptionMessage.INCREMENT;
	private static final NotificationCountSubscriptionMessage DECREMENT = NotificationCountSubscriptionMessage.DECREMENT;

	/* Mocked objects */
	private NotificationSubscriptionObserver<?> observer;
	@Mock
	private NotificationDefinitionService<?> notificationDefinitionService;
	@Spy
	private NotificationSubscriptionServiceImpl notificationSubscriptionService;
	@Mock
	private SecurityUserService securityUserService;
	@Mock
	private WebSocketHandler webSocketHandler;
	@Mock
	private ContextHandler contextHandler;
	@Mock
	private ReadOnlyDAO<?> dao;

	/* Test data fields */
	private short currentId;
	private MultiValueHashMap<Integer, NotificationRecipient> recipientListByDefinitionId = new MultiValueHashMap<>(false);
	private MultiValueHashMap<SecurityUser, Notification> visibleNotificationByUser = new MultiValueHashMap<>(false);
	private MultiValueHashMap<String, SecurityUser> userListByChannel = new MultiValueHashMap<>(false);
	private MultiValueHashMap<Short, SecurityUser> userListByGroupId = new MultiValueHashMap<>(false);

	/* Static objects */
	private static final SystemPriority PRIORITY_LOW = new SystemPriority();
	private static final SystemPriority PRIORITY_VISIBLE = new SystemPriority();
	private static final SystemPriority PRIORITY_CRITICAL = new SystemPriority();
	private static final SystemPriority PRIORITY_IMMEDIATE = new SystemPriority();


	static {
		PRIORITY_LOW.setOrder(40);
		PRIORITY_VISIBLE.setOrder(NotificationSubscriptionUtils.PRIORITY_THRESHOLD_VISIBLE);
		PRIORITY_CRITICAL.setOrder(NotificationSubscriptionUtils.PRIORITY_THRESHOLD_CRITICAL);
		PRIORITY_IMMEDIATE.setOrder(NotificationSubscriptionUtils.PRIORITY_THRESHOLD_IMMEDIATE);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		// Explicitly inject mocks to fail-fast on injection failure, unlike @InjectMocks
		this.observer = new NotificationSubscriptionObserver<>();
		this.observer.setNotificationDefinitionService(this.notificationDefinitionService);
		this.observer.setNotificationSubscriptionService(this.notificationSubscriptionService);
		this.observer.setSecurityUserService(this.securityUserService);
		this.observer.setWebSocketHandler(this.webSocketHandler);
		this.observer.setContextHandler(this.contextHandler);

		// Set default mocked return values
		Mockito.when(this.notificationDefinitionService.getNotificationRecipientListByNotificationDefinition(Mockito.anyInt()))
				.thenAnswer(invocation -> ObjectUtils.coalesce(this.recipientListByDefinitionId.get(invocation.getArgument(0)), Collections.emptyList()));
		Mockito.when(this.webSocketHandler.getSubscribedUserList(Mockito.anyString()))
				.thenAnswer(invocation -> ObjectUtils.coalesce(this.userListByChannel.get(invocation.getArgument(0)), Collections.emptyList()));
		Mockito.when(this.securityUserService.getSecurityUserListByGroup(Mockito.anyShort()))
				.thenAnswer(invocation -> ObjectUtils.coalesce(this.userListByGroupId.get(invocation.getArgument(0)), Collections.emptyList()));

		// Set default mocked (spy) return values
		Mockito.doAnswer(invocation -> ObjectUtils.coalesce(this.visibleNotificationByUser.get(invocation.getArgument(0)), Collections.emptyList()))
				.when(this.notificationSubscriptionService).getVisibleNotificationListForUser(Mockito.any());
	}


	@AfterEach
	public void tearDown() {
		verifyMockedWsHandler();
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Notification Modification Tests                 ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testRecipientAddCount() {
		SecurityUser user = createUser();
		Notification notification = createNotification(null, null, PRIORITY_VISIBLE, null, false);
		createRecipient(notification, user, null);
		addSubscriber(user);
		updateEntity(null, notification);
		verifyCountAdd(user, 1);
	}


	@Test
	public void testAssigneeAddCount() {
		SecurityUser user = createUser();
		Notification notification = createNotification(user, null, PRIORITY_VISIBLE, null, false);
		addSubscriber(user);
		updateEntity(null, notification);
		verifyCountAdd(user, 1);
	}


	@Test
	public void testRecipientGroupAddCount() {
		SecurityGroup group = createGroup(3);
		Collection<SecurityUser> userList = getUsersByGroup(group);
		Notification notification = createNotification(null, null, PRIORITY_VISIBLE, null, false);
		createRecipient(notification, null, group);
		addSubscriber(userList);
		updateEntity(null, notification);
		verifyCountAdd(userList, 1);
	}


	@Test
	public void testAssigneeGroupAddCount() {
		SecurityGroup group = createGroup(3);
		Collection<SecurityUser> userList = getUsersByGroup(group);
		Notification notification = createNotification(null, group, PRIORITY_VISIBLE, null, false);
		addSubscriber(userList);
		updateEntity(null, notification);
		verifyCountAdd(userList, 1);
	}


	@Test
	public void testAssigneeAndRecipientGroupAddCount() {
		SecurityGroup group = createGroup(3);
		Collection<SecurityUser> userList = getUsersByGroup(group);
		Notification notification = createNotification(null, group, PRIORITY_VISIBLE, null, false);
		createRecipient(notification, null, group);
		addSubscriber(userList);
		updateEntity(null, notification);
		verifyCountAdd(userList, 1);
	}


	@Test
	public void testAssigneeAndRecipientGroupAcknowledgeCount() {
		SecurityGroup group = createGroup(3);
		Collection<SecurityUser> userList = getUsersByGroup(group);
		Notification originalNotification = createNotification(null, group, PRIORITY_VISIBLE, null, false);
		Notification newNotification = createModifyNotification(originalNotification, null, null, PRIORITY_VISIBLE, true);
		createRecipient(originalNotification, null, group);
		addSubscriber(userList);
		updateEntity(originalNotification, newNotification);
		updateEntity(originalNotification, newNotification);
		verifyCountRemove(userList, 2);
	}


	@Test
	public void testAssigneeChangeCount() {
		// Use InOrder verifier to verify order of invocations rather than total count for more precise testing here
		InOrder inOrder = Mockito.inOrder(this.webSocketHandler);
		SecurityUser user = createUser();
		addSubscriber(user);

		Notification notification0 = createNotification(null, null, PRIORITY_VISIBLE, null, false);
		updateEntity(null, notification0);
		inOrder.verify(this.webSocketHandler, Mockito.never()).sendMessageToUser(Mockito.eq(user), Mockito.eq(COUNT), Mockito.any());

		Notification notification1 = createModifyNotification(notification0, user, null, PRIORITY_VISIBLE, false);
		updateEntity(notification0, notification1);
		inOrder.verify(this.webSocketHandler, Mockito.times(1)).sendMessageToUser(user, COUNT, INCREMENT);

		Notification notification2 = createModifyNotification(notification1, user, null, PRIORITY_LOW, false);
		updateEntity(notification1, notification2);
		inOrder.verify(this.webSocketHandler, Mockito.times(1)).sendMessageToUser(user, COUNT, DECREMENT);

		Notification notification3 = createModifyNotification(notification2, user, null, PRIORITY_VISIBLE, false);
		updateEntity(notification2, notification3);
		inOrder.verify(this.webSocketHandler, Mockito.times(1)).sendMessageToUser(user, COUNT, INCREMENT);

		Notification notification4 = createModifyNotification(notification3, user, null, PRIORITY_VISIBLE, true);
		updateEntity(notification3, notification4);
		inOrder.verify(this.webSocketHandler, Mockito.times(1)).sendMessageToUser(user, COUNT, DECREMENT);
	}


	@Test
	public void testRecipientPopup() {
		SecurityUser user = createUser();
		addSubscriber(user);
		NotificationDefinition definition = createDefinition();
		createRecipient(definition, user, null);

		// Test visible -> critical -> immediate
		Notification notification1a = createNotification(null, null, PRIORITY_VISIBLE, definition, false);
		updateEntity(null, notification1a);
		verifyCountAdd(user, 1);
		verifyNoPopup(user);
		Notification notification1b = createModifyNotification(notification1a, null, null, PRIORITY_CRITICAL, false);
		updateEntity(notification1a, notification1b);
		verifyNoPopup(user);
		Notification notification1c = createModifyNotification(notification1b, null, null, PRIORITY_IMMEDIATE, false);
		updateEntity(notification1b, notification1c);
		verifyPopupAdd(user, notification1c);

		// Test acknowledged
		Notification notification2a = createNotification(null, null, PRIORITY_IMMEDIATE, definition, false);
		updateEntity(null, notification2a);
		verifyCountAdd(user, 2);
		verifyPopupAdd(user, notification2a);
		Notification notification2b = createModifyNotification(notification2a, null, null, PRIORITY_IMMEDIATE, true);
		updateEntity(notification2a, notification2b);
		verifyCountRemove(user, 1);
		verifyPopupRemove(user, notification2a);

		// Test immediate -> critical
		Notification notification3a = createNotification(null, null, PRIORITY_IMMEDIATE, definition, false);
		updateEntity(null, notification3a);
		verifyCountAdd(user, 3);
		verifyPopupAdd(user, notification3a);
		Notification notification3b = createModifyNotification(notification3a, null, null, PRIORITY_CRITICAL, false);
		updateEntity(notification3a, notification3b);
		verifyPopupRemove(user, notification3a);
	}


	@Test
	public void testRecipientGroupPopup() {
		SecurityGroup group = createGroup(3);
		Collection<SecurityUser> userList = getUsersByGroup(group);
		addSubscriber(userList);
		NotificationDefinition definition = createDefinition();
		createRecipient(definition, null, group);

		// Test visible -> critical -> immediate
		Notification notification1a = createNotification(null, null, PRIORITY_VISIBLE, definition, false);
		updateEntity(null, notification1a);
		verifyCountAdd(userList, 1);
		verifyNoPopup(userList);
		Notification notification1b = createModifyNotification(notification1a, null, null, PRIORITY_CRITICAL, false);
		updateEntity(notification1a, notification1b);
		verifyNoPopup(userList);
		Notification notification1c = createModifyNotification(notification1b, null, null, PRIORITY_IMMEDIATE, false);
		updateEntity(notification1b, notification1c);
		verifyPopupAdd(userList, notification1c);

		// Test acknowledged
		Notification notification2a = createNotification(null, null, PRIORITY_IMMEDIATE, definition, false);
		updateEntity(null, notification2a);
		verifyCountAdd(userList, 2);
		verifyPopupAdd(userList, notification2a);
		Notification notification2b = createModifyNotification(notification2a, null, null, PRIORITY_IMMEDIATE, true);
		updateEntity(notification2a, notification2b);
		verifyCountRemove(userList, 1);
		verifyPopupRemove(userList, notification2a);

		// Test immediate -> critical
		Notification notification3a = createNotification(null, null, PRIORITY_IMMEDIATE, definition, false);
		updateEntity(null, notification3a);
		verifyCountAdd(userList, 3);
		verifyPopupAdd(userList, notification3a);
		Notification notification3b = createModifyNotification(notification3a, null, null, PRIORITY_CRITICAL, false);
		updateEntity(notification3a, notification3b);
		verifyPopupRemove(userList, notification3a);
	}


	@Test
	public void testAssigneePopup() {
		int added = 0;
		int removed = 0;
		SecurityUser user = createUser();
		addSubscriber(user);

		// Test visible no pop-up
		Notification notification1a = createNotification(user, null, PRIORITY_VISIBLE, null, false);
		updateEntity(null, notification1a);
		verifyCountAdd(user, ++added);
		verifyNoPopup(user);

		// Test critical pop-up -> acknowledged
		Notification notification2a = createNotification(user, null, PRIORITY_CRITICAL, null, false);
		updateEntity(null, notification2a);
		verifyCountAdd(user, ++added);
		verifyPopupAdd(user, notification2a);
		Notification notification2b = createModifyNotification(notification2a, user, null, PRIORITY_CRITICAL, true);
		updateEntity(notification2a, notification2b);
		verifyCountRemove(user, ++removed);
		verifyPopupRemove(user, notification2a);

		// Test immediate pop-up -> acknowledged
		Notification notification3a = createNotification(user, null, PRIORITY_IMMEDIATE, null, false);
		updateEntity(null, notification3a);
		verifyCountAdd(user, ++added);
		verifyPopupAdd(user, notification3a);
		Notification notification3b = createModifyNotification(notification3a, user, null, PRIORITY_IMMEDIATE, true);
		updateEntity(notification3a, notification3b);
		verifyCountRemove(user, ++removed);
		verifyPopupRemove(user, notification3a);

		// Test immediate pop-up not assigned -> assigned -> not assigned
		Notification notification4a = createNotification(null, null, PRIORITY_IMMEDIATE, null, false);
		updateEntity(null, notification4a);
		Notification notification4b = createModifyNotification(notification4a, user, null, PRIORITY_IMMEDIATE, false);
		updateEntity(notification4a, notification4b);
		verifyCountAdd(user, ++added);
		verifyPopupAdd(user, notification4b);
		Notification notification4c = createModifyNotification(notification4b, null, null, PRIORITY_IMMEDIATE, false);
		updateEntity(notification4b, notification4c);
		verifyCountRemove(user, ++removed);
		verifyPopupRemove(user, notification4b);

		// Test immediate assigned user -> assigned group -> assigned user + group -> acknowledged
		SecurityGroup group = createGroup(user);
		Notification notification5a = createNotification(user, null, PRIORITY_IMMEDIATE, null, false);
		updateEntity(null, notification5a);
		verifyCountAdd(user, ++added);
		verifyPopupAdd(user, notification5a);
		Notification notification5b = createModifyNotification(notification5a, null, group, PRIORITY_IMMEDIATE, false);
		updateEntity(notification5a, notification5b);
		verifyPopupAdd(user, notification5b); // Update message
		Notification notification5c = createModifyNotification(notification5b, user, group, PRIORITY_IMMEDIATE, false);
		updateEntity(notification5b, notification5c);
		verifyPopupAdd(user, notification5c);
		Notification notification5d = createModifyNotification(notification5c, user, group, PRIORITY_IMMEDIATE, true);
		updateEntity(notification5c, notification5d);
		verifyCountRemove(user, ++removed);
		verifyPopupRemove(user, notification5c);
	}


	@Test
	public void testAssigneeGroupPopup() {
		SecurityGroup group = createGroup(3);
		Collection<SecurityUser> userList = getUsersByGroup(group);
		addSubscriber(userList);

		// Test visible no pop-up
		Notification notification1a = createNotification(null, group, PRIORITY_VISIBLE, null, false);
		updateEntity(null, notification1a);
		verifyCountAdd(userList, 1);
		verifyNoPopup(userList);

		// Test critical pop-up -> acknowledged
		Notification notification2a = createNotification(null, group, PRIORITY_CRITICAL, null, false);
		updateEntity(null, notification2a);
		verifyCountAdd(userList, 2);
		verifyPopupAdd(userList, notification2a);
		Notification notification2b = createModifyNotification(notification2a, null, group, PRIORITY_CRITICAL, true);
		updateEntity(notification2a, notification2b);
		verifyCountRemove(userList, 1);
		verifyPopupRemove(userList, notification2a);

		// Test immediate pop-up -> acknowledged
		Notification notification3a = createNotification(null, group, PRIORITY_IMMEDIATE, null, false);
		updateEntity(null, notification3a);
		verifyCountAdd(userList, 3);
		verifyPopupAdd(userList, notification3a);
		Notification notification3b = createModifyNotification(notification3a, null, group, PRIORITY_IMMEDIATE, true);
		updateEntity(notification3a, notification3b);
		verifyCountRemove(userList, 2);
		verifyPopupRemove(userList, notification3a);

		// Test immediate pop-up not assigned -> assigned -> not assigned
		Notification notification4a = createNotification(null, null, PRIORITY_IMMEDIATE, null, false);
		updateEntity(null, notification4a);
		Notification notification4b = createModifyNotification(notification4a, null, group, PRIORITY_IMMEDIATE, false);
		updateEntity(notification4a, notification4b);
		verifyCountAdd(userList, 4);
		verifyPopupAdd(userList, notification4b);
		Notification notification4c = createModifyNotification(notification4b, null, null, PRIORITY_IMMEDIATE, false);
		updateEntity(notification4b, notification4c);
		verifyCountRemove(userList, 3);
		verifyPopupRemove(userList, notification4b);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Other Modification Tests                        ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testNotificationRecipientChange() {
		SecurityUser user = createUser();
		SecurityGroup group = createGroup(3);
		Collection<SecurityUser> groupUserList = getUsersByGroup(group);
		addSubscriber(user);
		addSubscriber(groupUserList);
		NotificationDefinition definition = createDefinition();
		Notification notification1 = createNotification(null, null, PRIORITY_IMMEDIATE, definition, false);
		Notification notification2 = createNotification(null, null, PRIORITY_IMMEDIATE, definition, false);
		updateEntity(null, notification1);
		updateEntity(null, notification2);

		// Test create recipient
		NotificationRecipient recipient1 = createRecipient(definition, user, null);
		this.visibleNotificationByUser.put(user, notification1);
		this.visibleNotificationByUser.put(user, notification2);
		updateEntity(null, recipient1);
		verifyCountReset(user, 2);
		verifyPopupReset(user, notification1, notification2);

		// Test add recipient
		NotificationRecipient recipient2 = createRecipient(definition, null, group);
		groupUserList.forEach(groupUser -> this.visibleNotificationByUser.put(groupUser, notification1));
		groupUserList.forEach(groupUser -> this.visibleNotificationByUser.put(groupUser, notification2));
		updateEntity(null, recipient2);
		verifyCountReset(groupUserList, 2);
		verifyPopupReset(groupUserList, notification1, notification2);

		// Test remove recipient
		this.visibleNotificationByUser.removeAll(user);
		groupUserList.forEach(this.visibleNotificationByUser::removeAll);
		updateEntity(recipient1, null);
		verifyCountReset(user, 0);
		verifyPopupReset(user);
		updateEntity(recipient2, null);
		verifyCountReset(groupUserList, 0);
		verifyPopupReset(groupUserList);
	}


	@Test
	public void testSystemUserGroupChange() {
		SecurityGroup group = createGroup();
		SecurityUser user = createUser();
		addSubscriber(user);
		Notification notification = createNotification(null, null, PRIORITY_IMMEDIATE, null, false);
		updateEntity(null, notification);

		// Test create security user group
		SecurityUserGroup userGroup1 = createSecurityUserGroup(user, group);
		this.visibleNotificationByUser.put(user, notification);
		updateEntity(null, userGroup1);
		verifyCountReset(user, 1);
		verifyPopupReset(user, notification);
		verifyMockedWsHandler();

		// Test add security user group
		SecurityUserGroup userGroup2 = createSecurityUserGroup(user, group);
		updateEntity(null, userGroup2);
		verifyCountReset(user, 1);
		verifyPopupReset(user, notification);
		verifyMockedWsHandler();

		// Test remove security user group
		this.visibleNotificationByUser.removeAll(user);
		updateEntity(userGroup1, null);
		verifyCountReset(user, 0);
		verifyPopupReset(user);
		verifyMockedWsHandler();
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Setup Methods                                   ////////
	////////////////////////////////////////////////////////////////////////////


	private SecurityUser createUser() {
		int id = getNextIdInt();
		SecurityUser user = new SecurityUser();
		user.setId((short) id);
		user.setUserName("user" + id);
		user.setIntegrationPseudonym("user-pseudo" + id);
		return user;
	}


	private SecurityGroup createGroup(int count) {
		SecurityUser[] users = Stream.generate(this::createUser)
				.limit(count)
				.toArray(SecurityUser[]::new);
		return createGroup(users);
	}


	private SecurityGroup createGroup(SecurityUser... users) {
		SecurityGroup group = new SecurityGroup();
		group.setId(getNextId());
		Arrays.stream(users).forEach(user -> createSecurityUserGroup(user, group));
		return group;
	}


	private SecurityUserGroup createSecurityUserGroup(SecurityUser user, SecurityGroup group) {
		SecurityUserGroup userGroup = new SecurityUserGroup();
		userGroup.setId(getNextIdInt());
		userGroup.setReferenceOne(user);
		userGroup.setReferenceTwo(group);
		this.userListByGroupId.put(group.getId(), user);
		return userGroup;
	}


	private NotificationDefinition createDefinition() {
		NotificationDefinition definition = new NotificationDefinitionInstant();
		definition.setId(getNextIdInt());
		return definition;
	}


	private NotificationRecipient createRecipient(Notification notification, SecurityUser user, SecurityGroup group) {
		return createRecipient(notification.getDefinition(), user, group);
	}


	private NotificationRecipient createRecipient(NotificationDefinition definition, SecurityUser user, SecurityGroup group) {
		NotificationRecipient recipient = new NotificationRecipient();
		recipient.setId(getNextIdInt());
		recipient.setSecurityUser(user);
		recipient.setSecurityGroup(group);
		this.recipientListByDefinitionId.put(definition.getId(), recipient);
		return recipient;
	}


	private Notification createNotification(SecurityUser user, SecurityGroup group, SystemPriority priority, NotificationDefinition definition, boolean acknowledged) {
		Notification notification = new Notification();
		notification.setId(getNextIdInt());
		notification.setAssigneeUser(user);
		notification.setAssigneeGroup(group);
		notification.setPriority(priority);
		notification.setDefinition(definition);
		notification.setAcknowledged(acknowledged);
		if (notification.getDefinition() == null) {
			notification.setDefinition(createDefinition());
		}
		return notification;
	}


	private Notification createModifyNotification(Notification originalNotification, SecurityUser user, SecurityGroup group, SystemPriority priority, boolean acknowledged) {
		// Clone original entity
		Notification notification = new Notification();
		notification.setId(originalNotification.getId());
		notification.setAssigneeUser(originalNotification.getAssigneeUser());
		notification.setAssigneeGroup(originalNotification.getAssigneeGroup());
		notification.setPriority(originalNotification.getPriority());
		notification.setDefinition(originalNotification.getDefinition());

		// Apply modifications
		notification.setAssigneeUser(user);
		notification.setAssigneeGroup(group);
		notification.setPriority(priority);
		notification.setAcknowledged(acknowledged);
		return notification;
	}


	private void addSubscriber(Collection<SecurityUser> users) {
		users.forEach(this::addSubscriber);
	}


	private void addSubscriber(SecurityUser user) {
		this.userListByChannel.put(COUNT, user);
		this.userListByChannel.put(POPUP, user);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Execution Methods                               ////////
	////////////////////////////////////////////////////////////////////////////


	private <T extends IdentityObject> void updateEntity(T originalBean, T newBean) {
		if (originalBean != null) {
			if (newBean != null) {
				@SuppressWarnings("unchecked")
				DaoEventContext<T> context = Mockito.mock(DaoEventContext.class);
				Mockito.when(context.getOriginalBean(newBean)).thenReturn(originalBean);
				Mockito.when(this.contextHandler.getBean(BaseDaoEventObserver.DAO_EVENT_CONTEXT_KEY)).thenReturn(context);
				getObserver().afterTransactionMethodCallImpl(getDao(), DaoEventTypes.UPDATE, newBean, null);
			}
			else {
				getObserver().afterTransactionMethodCallImpl(getDao(), DaoEventTypes.DELETE, originalBean, null);
			}
		}
		else if (newBean != null) {
			getObserver().afterTransactionMethodCallImpl(getDao(), DaoEventTypes.INSERT, newBean, null);
		}
		else {
			throw new IllegalStateException("Neither an old nor a new entity was provided. At least one entity must be provided.");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Verification Methods                            ////////
	////////////////////////////////////////////////////////////////////////////


	private void verifyCountRemove(Collection<SecurityUser> userList, int num) {
		userList.forEach(user -> verifyCountRemove(user, num));
	}


	private void verifyCountRemove(SecurityUser user, int num) {
		Mockito.verify(this.webSocketHandler, Mockito.times(Math.abs(num))).sendMessageToUser(user, COUNT, DECREMENT);
	}


	private void verifyCountAdd(Collection<SecurityUser> userList, int num) {
		userList.forEach(user -> verifyCountAdd(user, num));
	}


	private void verifyCountAdd(SecurityUser user, int num) {
		Mockito.verify(this.webSocketHandler, Mockito.times(Math.abs(num))).sendMessageToUser(user, COUNT, INCREMENT);
	}


	private void verifyCountReset(Collection<SecurityUser> userList, int count) {
		userList.forEach(user -> verifyCountReset(user, count));
	}


	private void verifyCountReset(SecurityUser user, int count) {
		Mockito.verify(this.webSocketHandler, Mockito.times(1))
				.sendMessageToUser(Mockito.eq(user), Mockito.eq(COUNT), Mockito.argThat(arg -> {
					if (!(arg instanceof NotificationCountSubscriptionMessage)) {
						return false;
					}
					NotificationCountSubscriptionMessage message = (NotificationCountSubscriptionMessage) arg;
					return message.isReset() && message.getNumAdded() == count;
				}));
	}


	private void verifyNoPopup(Collection<SecurityUser> userList) {
		userList.forEach(this::verifyNoPopup);
	}


	private void verifyNoPopup(SecurityUser user) {
		Mockito.verify(this.webSocketHandler, Mockito.never()).sendMessageToUser(Mockito.eq(user), Mockito.eq(POPUP), Mockito.any());
	}


	private void verifyPopupRemove(Collection<SecurityUser> userList, Notification... notifications) {
		userList.forEach(user -> verifyPopupRemove(user, notifications));
	}


	private void verifyPopupRemove(SecurityUser user, Notification... notifications) {
		for (Notification notification : notifications) {
			Mockito.verify(this.webSocketHandler, Mockito.times(1))
					.sendMessageToUser(Mockito.eq(user), Mockito.eq(POPUP), Mockito.argThat(arg -> {
						if (!(arg instanceof NotificationPopupSubscriptionMessage)) {
							return false;
						}
						NotificationPopupSubscriptionMessage message = (NotificationPopupSubscriptionMessage) arg;
						return message.getAdded().isEmpty()
								&& message.getRemoved().size() == 1 && message.getRemoved().get(0) == notification;
					}));
		}
	}


	private void verifyPopupAdd(Collection<SecurityUser> userList, Notification notifications) {
		userList.forEach(user -> verifyPopupAdd(user, notifications));
	}


	private void verifyPopupAdd(SecurityUser user, Notification... notifications) {
		for (Notification notification : notifications) {
			Mockito.verify(this.webSocketHandler, Mockito.times(1))
					.sendMessageToUser(Mockito.eq(user), Mockito.eq(POPUP), Mockito.argThat(arg -> {
						if (!(arg instanceof NotificationPopupSubscriptionMessage)) {
							return false;
						}
						NotificationPopupSubscriptionMessage message = (NotificationPopupSubscriptionMessage) arg;
						return message.getAdded().size() == 1 && message.getAdded().get(0) == notification
								&& message.getRemoved().isEmpty();
					}));
		}
	}


	private void verifyPopupReset(Collection<SecurityUser> userList, Notification... notifications) {
		userList.forEach(user -> verifyPopupReset(user, notifications));
	}


	private void verifyPopupReset(SecurityUser user, Notification... notifications) {
		Mockito.verify(this.webSocketHandler, Mockito.times(1))
				.sendMessageToUser(Mockito.eq(user), Mockito.eq(POPUP), Mockito.argThat(arg -> {
					if (!(arg instanceof NotificationPopupSubscriptionMessage)) {
						return false;
					}
					NotificationPopupSubscriptionMessage message = (NotificationPopupSubscriptionMessage) arg;
					return message.isReset()
							&& message.getAdded().size() == notifications.length && Arrays.stream(notifications).allMatch(message.getAdded()::contains)
							&& message.getRemoved().isEmpty();
				}));
	}


	private void verifyMockedWsHandler() {
		// Verify that all actions are accounted for
		Mockito.verify(this.webSocketHandler, Mockito.atLeast(0)).getSubscribedUserList(Mockito.anyString());
		Mockito.verifyNoMoreInteractions(this.webSocketHandler);
		Mockito.clearInvocations(this.webSocketHandler);
		Mockito.validateMockitoUsage();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Collection<SecurityUser> getUsersByGroup(SecurityGroup group) {
		return this.userListByGroupId.get(group.getId());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private int getNextIdInt() {
		return (int) ++this.currentId;
	}


	private short getNextId() {
		return ++this.currentId;
	}


	@SuppressWarnings("unchecked")
	private <T extends IdentityObject> NotificationSubscriptionObserver<T> getObserver() {
		return (NotificationSubscriptionObserver<T>) this.observer;
	}


	@SuppressWarnings("unchecked")
	private <T extends IdentityObject> ReadOnlyDAO<T> getDao() {
		return (ReadOnlyDAO<T>) this.dao;
	}
}
