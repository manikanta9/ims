package com.clifton.notification.generator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.notification.NotificationTestEntity;
import com.clifton.notification.comment.NotificationComment;
import com.clifton.notification.comment.NotificationCommentService;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import com.clifton.notification.definition.NotificationDefinitionInstant;
import com.clifton.notification.definition.NotificationDefinitionService;
import com.clifton.notification.definition.NotificationHistory;
import com.clifton.notification.definition.NotificationStatuses;
import com.clifton.notification.definition.search.NotificationHistorySearchForm;
import com.clifton.security.SecurityTestsUtils;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.execute.SystemQueryExecutionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>NotificationGeneratorServiceImplTests</code> tests the
 * {@link NotificationGeneratorServiceImpl} class
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class NotificationGeneratorServiceImplTests<T extends NotificationDefinition> {

	@Resource
	private NotificationDefinitionService<T> notificationDefinitionService;

	@Resource
	private NotificationGeneratorServiceImpl<?, T> notificationGeneratorService;

	@Resource
	private NotificationCommentService notificationCommentService;

	@Resource
	private SystemQueryExecutionService systemQueryExecutionService;

	@Resource
	private UpdatableDAO<NotificationTestEntity> notificationTestEntityDAO;

	@Resource
	private UpdatableDAO<Notification> notificationDAO;


	@Resource
	private ContextHandler contextHandler;

	@Resource
	private SecurityUserService securityUserService;


	@BeforeEach
	public void resetTests() {
		setCurrentUser_TestUser1();
	}


	private void setCurrentUser_TestUser1() {
		SecurityTestsUtils.setSecurityContextPrincipal(this.contextHandler, this.securityUserService, "TestUser");
	}


	private void setCurrentUser_TestUser2() {
		SecurityTestsUtils.setSecurityContextPrincipal(this.contextHandler, this.securityUserService, "TestUser2");
	}

	////////////////////////////////////////////////////////////////////////////
	///////         Instant Notification Business Methods             //////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testDoNotGenerateInstantNotificationForBean() {
		NotificationTestEntity entity = this.notificationTestEntityDAO.findByPrimaryKey(3);
		entity.setName("Updated Name");
		this.notificationTestEntityDAO.save(entity);

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", 202);
		Assertions.assertTrue(CollectionUtils.isEmpty(notificationList));
	}


	@Test
	public void testInactiveInstantNotification() {
		NotificationDefinitionInstant nd = (NotificationDefinitionInstant) this.notificationDefinitionService.getNotificationDefinition(200);
		nd.setActive(false);
		this.notificationDefinitionService.saveNotificationDefinitionInstant(nd);

		NotificationTestEntity entity = new NotificationTestEntity();
		entity.setName("Trade test");
		entity.setAmount(new BigDecimal(1000));
		this.notificationTestEntityDAO.save(entity);

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", 200);
		Assertions.assertTrue(CollectionUtils.isEmpty(notificationList));

		// Reset to active for other tests
		nd.setActive(true);
		this.notificationDefinitionService.saveNotificationDefinitionInstant(nd);
	}


	@Test
	public void testGenerateInstantInsertNotificationForBean() {
		NotificationTestEntity entity = new NotificationTestEntity();
		entity.setName("Trade 4");
		entity.setAmount(new BigDecimal(1000));
		this.notificationTestEntityDAO.save(entity);

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", 200);
		Assertions.assertEquals(1, CollectionUtils.getSize(notificationList));
		NotificationDefinition def = this.notificationDefinitionService.getNotificationDefinition(200);
		validateInstantNotification(def, notificationList.get(0), entity, "Test Insert", "Test Insert");

		// Clean Up - Delete the notification
		deleteNotificationList(notificationList);
	}


	@Test
	public void testGenerateInstantDeleteNotificationForBean() {
		NotificationTestEntity entity = this.notificationTestEntityDAO.findByPrimaryKey(2);
		this.notificationTestEntityDAO.delete(entity.getId());

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", 201);
		Assertions.assertEquals(1, CollectionUtils.getSize(notificationList));
		NotificationDefinition def = this.notificationDefinitionService.getNotificationDefinition(201);
		validateInstantNotification(def, notificationList.get(0), entity, "Test Delete", "Test Delete");

		// Clean Up - Delete the notification
		deleteNotificationList(notificationList);
	}


	@Test
	public void testGenerateInstantUpdateNotificationForBean() {
		NotificationTestEntity entity = this.notificationTestEntityDAO.findByPrimaryKey(1);
		entity.setAmount(new BigDecimal(1000));
		this.notificationTestEntityDAO.save(entity);

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", 202);
		Assertions.assertEquals(1, CollectionUtils.getSize(notificationList));
		NotificationDefinition def = this.notificationDefinitionService.getNotificationDefinition(202);
		Notification not = notificationList.get(0);
		validateInstantNotification(def, not, entity, "Test Update", "Test Update");
		Assertions.assertEquals(2, (short) not.getAssigneeUser().getId());

		// Clean Up - Delete the notification
		deleteNotificationList(notificationList);
	}


	private void validateInstantNotification(NotificationDefinition def, Notification not, NotificationTestEntity entity, String subject, String text) {
		Assertions.assertEquals(def.getType(), not.getType());
		Assertions.assertEquals(def.getPriority(), not.getPriority());
		Assertions.assertEquals(def.getTable(), not.getTable());
		Assertions.assertEquals((Long) entity.getId().longValue(), not.getFkFieldId());
		Assertions.assertEquals(subject, not.getSubject());
		Assertions.assertEquals(text, not.getText());
	}

	////////////////////////////////////////////////////////////////////////////
	///////         Batched Notification Business Methods             //////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInactiveBatchedNotification() {
		this.notificationGeneratorService.clearNotificationList(250);

		NotificationDefinitionBatch def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(250);
		def.setActive(false);
		this.notificationDefinitionService.saveNotificationDefinitionBatch(def);
		this.notificationGeneratorService.generateBatchedNotification(def, false, null);

		def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(250);

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", 250);
		Assertions.assertTrue(CollectionUtils.isEmpty(notificationList));

		// Reset to active for other tests
		def.setActive(true);
		this.notificationDefinitionService.saveNotificationDefinitionBatch(def);
	}


	@Test
	public void testDoNotGenerateBatchedNotification() {
		Mockito.when(this.systemQueryExecutionService.getSystemQueryResult(ArgumentMatchers.any(SystemQuery.class))).thenReturn(null);
		Date scheduleDate = new Date();

		NotificationDefinitionBatch def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(250);
		this.notificationGeneratorService.generateBatchedNotification(def, false, scheduleDate);

		def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(250);
		Assertions.assertEquals(NotificationStatuses.COMPLETED_WITH_NO_RESULTS, def.getStatus());

		NotificationHistory history = getLastHistory(def.getId());
		Assertions.assertEquals(NotificationStatuses.COMPLETED_WITH_NO_RESULTS, history.getStatus());

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", 250);
		Assertions.assertTrue(CollectionUtils.isEmpty(notificationList));
	}


	@Test
	public void testGenerateBatchedNotification() {
		Mockito.when(this.systemQueryExecutionService.getSystemQueryResult(ArgumentMatchers.any(SystemQuery.class))).thenReturn(getTestExpectedResults());
		Date scheduleDate = new Date();

		NotificationDefinitionBatch def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(250);
		this.notificationGeneratorService.generateBatchedNotification(def, false, scheduleDate);

		def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(250);
		Assertions.assertEquals(NotificationStatuses.COMPLETED, def.getStatus());

		NotificationHistory history = getLastHistory(def.getId());
		Assertions.assertEquals(NotificationStatuses.COMPLETED, history.getStatus());

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", 250);
		Assertions.assertEquals(1, notificationList.size());
		validateBatchedNotification(def, notificationList.get(0), "Notification Test Entities", null);

		// Clean Up - Delete the notification
		deleteNotificationList(notificationList);
	}


	@Test
	public void testRunBatchedSingleSaveNotification() {
		Mockito.when(this.systemQueryExecutionService.getSystemQueryResult(ArgumentMatchers.any(SystemQuery.class))).thenReturn(getTestExpectedResults());

		NotificationDefinitionBatch def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(2511);
		this.notificationGeneratorService.generateBatchedNotification(def, true, null);

		def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(2511);
		Assertions.assertEquals(NotificationStatuses.COMPLETED, def.getStatus());

		NotificationHistory history = getLastHistory(def.getId());
		Assertions.assertEquals(NotificationStatuses.COMPLETED, history.getStatus());

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", 2511);
		Assertions.assertEquals(3, CollectionUtils.getSize(notificationList), "Expected 3 notifications to be generated");
		for (Notification not : notificationList) {
			validateBatchedNotification(def, not, "Notification Test Entities", null);
		}

		// Clean Up - Delete the notification
		deleteNotificationList(notificationList);
	}


	@Test
	public void testRunBatchedAcknowledgedNotification() {
		Mockito.when(this.systemQueryExecutionService.getSystemQueryResult(ArgumentMatchers.any(SystemQuery.class))).thenReturn(getTestExpectedResults());

		// Clear all so we start fresh
		NotificationDefinitionBatch def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(251);
		this.notificationGeneratorService.generateBatchedNotification(def, true, null);

		def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(251);
		Assertions.assertEquals(NotificationStatuses.COMPLETED, def.getStatus());

		NotificationHistory history = getLastHistory(def.getId());
		Assertions.assertEquals(NotificationStatuses.COMPLETED, history.getStatus());

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", 251);
		Assertions.assertEquals(3, CollectionUtils.getSize(notificationList), "Expected 3 notifications to be generated");
		Notification notification = notificationList.get(0);
		notification.setAcknowledged(true);
		this.notificationGeneratorService.saveNotification(notification);
		int id = notification.getId();

		this.notificationGeneratorService.generateBatchedNotification(def, true, null);

		def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(251);
		Assertions.assertEquals(NotificationStatuses.COMPLETED, def.getStatus());

		history = getLastHistory(def.getId());
		Assertions.assertEquals(NotificationStatuses.COMPLETED, history.getStatus());

		notificationList = this.notificationDAO.findByField("definition.id", 251);
		boolean found = false;
		for (Notification not : notificationList) {
			if (not.getId() == id) {
				found = true;
				ValidationUtils.assertTrue(not.isAcknowledged(), "Notification with ID " + id + " was previously marked as acknowledged and should remain so.");
			}
			else {
				ValidationUtils.assertFalse(not.isAcknowledged(), "Notification wasn't previously marked as acknowledged and should remain so.");
			}
		}
		if (!found) {
			Assertions.fail("Did not find existing notification with id " + id + " that was previously acknowledged.");
		}

		// Clean Up - Delete the notification
		deleteNotificationList(notificationList);
	}


	@Test
	public void testRunBatchedSystemAcknowledgedNotification() {
		int defId = 252;
		Mockito.when(this.systemQueryExecutionService.getSystemQueryResult(ArgumentMatchers.any(SystemQuery.class)))
				.thenReturn(getTestExpectedResults())
				.thenReturn(getTestExpectedResults())
				.thenReturn(getTestExpectedResults2());

		// Clear all so we start fresh
		NotificationDefinitionBatch def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(defId);
		this.notificationGeneratorService.clearNotificationList(defId);
		this.notificationGeneratorService.generateBatchedNotification(def, true, null);

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", defId);
		Assertions.assertEquals(3, CollectionUtils.getSize(notificationList), "Expected 3 notifications to be generated");

		// Should still have the same 3 notifications
		this.notificationGeneratorService.generateBatchedNotification(def, true, null);
		notificationList = this.notificationDAO.findByField("definition.id", defId);
		Assertions.assertEquals(3, CollectionUtils.getSize(notificationList), "Expected 3 notifications to be generated");

		// Should only have 2 now that one is not longer in the list
		this.notificationGeneratorService.generateBatchedNotification(def, true, null);
		notificationList = this.notificationDAO.findByField("definition.id", defId);
		Assertions.assertEquals(2, notificationList.size());

		// Clean Up - Delete the notification
		deleteNotificationList(notificationList);
	}


	@Test
	public void testRunBatchedSystemAcknowledgedNotification_WithComments() {
		Mockito.when(this.systemQueryExecutionService.getSystemQueryResult(ArgumentMatchers.any(SystemQuery.class))).thenReturn(getTestExpectedResults());

		NotificationDefinitionBatch def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(252);
		this.notificationGeneratorService.generateBatchedNotification(def, true, null);

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", 252);
		Assertions.assertEquals(3, CollectionUtils.getSize(notificationList), "Expected 3 notifications to be generated");
		// Switch to another user to add the comments
		setCurrentUser_TestUser2();
		for (Notification not : notificationList) {
			NotificationComment comment = new NotificationComment();
			comment.setNotification(not);
			comment.setDescription("Test comment");
			this.notificationCommentService.saveNotificationComment(comment);
		}

		// Switch back to original test user
		setCurrentUser_TestUser1();

		// Re-Run notification (with NO results) - should delete the notifications and the comments
		Mockito.when(this.systemQueryExecutionService.getSystemQueryResult(ArgumentMatchers.any(SystemQuery.class))).thenReturn(null);
		this.notificationGeneratorService.generateBatchedNotification(def, true, null);

		notificationList = this.notificationDAO.findByField("definition.id", 252);
		Assertions.assertEquals(0, CollectionUtils.getSize(notificationList), "Expected no notifications.  All 3 should have been removed even though they had comments.");
	}


	@Test
	public void testRunBatchedSystemAndUserAcknowledgedNotification() {
		int defId = 253;
		Mockito.when(this.systemQueryExecutionService.getSystemQueryResult(ArgumentMatchers.any(SystemQuery.class)))
				.thenReturn(getTestExpectedResults())
				.thenReturn(getTestExpectedResults())
				.thenReturn(getTestExpectedResults2())
				.thenReturn(null);

		// Clear all so we start fresh
		NotificationDefinitionBatch def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(defId);
		this.notificationGeneratorService.clearNotificationList(defId);
		this.notificationGeneratorService.generateBatchedNotification(def, true, null);

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", defId);
		Assertions.assertEquals(3, CollectionUtils.getSize(notificationList), "Expected 3 notifications to be generated");

		// Manually Acknowledge One
		Notification not = notificationList.get(0);
		int id = not.getId();
		not.setAcknowledged(true);
		this.notificationGeneratorService.saveNotification(not);

		// Should still have the same 3 notifications, one of which is acknowledged
		this.notificationGeneratorService.generateBatchedNotification(def, true, null);
		notificationList = this.notificationDAO.findByField("definition.id", defId);
		Assertions.assertEquals(3, CollectionUtils.getSize(notificationList), "Expected 3 notifications to be generated");

		boolean found = false;
		for (Notification notification : notificationList) {
			if (notification.getId() == id) {
				found = true;
				ValidationUtils.assertTrue(notification.isAcknowledged(), "Notification with ID " + id + " was previously marked as acknowledged and should remain so.");
			}
			else {
				ValidationUtils.assertFalse(notification.isAcknowledged(), "Notification wasn't previously marked as acknowledged and should remain so.");
			}
		}
		if (!found) {
			Assertions.fail("Did not find existing notification with id " + id + " that was previously acknowledged.");
		}

		// Should only have 2 now that one is not longer in the list
		this.notificationGeneratorService.generateBatchedNotification(def, true, null);
		notificationList = this.notificationDAO.findByField("definition.id", defId);
		Assertions.assertEquals(2, notificationList.size());

		// Should not have any now none are being returned
		this.notificationGeneratorService.generateBatchedNotification(def, true, null);
		notificationList = this.notificationDAO.findByField("definition.id", defId);
		Assertions.assertTrue(CollectionUtils.isEmpty(notificationList), "All notifications should have been deleted");
	}


	@Test
	public void testRunBatchedSystemAndUserAcknowledgedNotification_DoNotUnacknowledge() {
		int defId = 253;
		Mockito.when(this.systemQueryExecutionService.getSystemQueryResult(ArgumentMatchers.any(SystemQuery.class))).thenReturn(getTestExpectedResults()).thenReturn(getTestExpectedResults());

		// Clear all so we start fresh
		NotificationDefinitionBatch def = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(defId);
		def.setDoNotUnacknowledgeIfUpdated(true);
		def.setText("Nothing here");
		this.notificationDefinitionService.saveNotificationDefinitionBatch(def);

		this.notificationGeneratorService.clearNotificationList(defId);
		this.notificationGeneratorService.generateBatchedNotification(def, true, null);

		List<Notification> notificationList = this.notificationDAO.findByField("definition.id", defId);
		Assertions.assertEquals(3, CollectionUtils.getSize(notificationList), "Expected 3 notifications to be generated");

		// Manually Acknowledge One
		Notification not = notificationList.get(0);
		int id = not.getId();
		not.setAcknowledged(true);
		this.notificationGeneratorService.saveNotification(not);

		// Update Notification Text
		def.setText("Updating Text!");
		this.notificationDefinitionService.saveNotificationDefinitionBatch(def);

		// Should still have the same 3 notifications, one of which is still acknowledged, but has a comment
		this.notificationGeneratorService.generateBatchedNotification(def, true, null);
		notificationList = this.notificationDAO.findByField("definition.id", defId);
		Assertions.assertEquals(3, CollectionUtils.getSize(notificationList), "Expected 3 notifications to be generated");

		boolean found = false;
		for (Notification notification : notificationList) {
			if (notification.getId() == id) {
				found = true;
				ValidationUtils.assertTrue(notification.isAcknowledged(), "Notification with ID " + id + " was previously marked as acknowledged and should remain so.");
				// Should have an attached comment
				NotificationComment comment = this.notificationCommentService.getNotificationCommentLastByNotification(id);
				ValidationUtils.assertNotNull(comment, "Notification should have system generated comment with change information");
				Assertions.assertEquals(
						"System Updated Acknowledged Notification but did not clear the acknowledged flag because notification definition has option set not to clear this.  Previous Notification Was:"
								+ StringUtils.NEW_LINE + "Notification Test Entities" + StringUtils.NEW_LINE + "Nothing here" + StringUtils.NEW_LINE, comment.getDescription());

				// Clean Up: Delete the comment
				this.notificationCommentService.deleteNotificationComment(comment.getId());
			}
			else {
				ValidationUtils.assertFalse(notification.isAcknowledged(), "Notification wasn't previously marked as acknowledged and should remain so.");
			}
		}
		if (!found) {
			Assertions.fail("Did not find existing notification with id " + id + " that was previously acknowledged.");
		}

		// Clean Up - Delete the notification
		deleteNotificationList(notificationList);
	}


	private NotificationHistory getLastHistory(int defId) {
		NotificationHistorySearchForm searchForm = new NotificationHistorySearchForm();
		searchForm.setNotificationDefinitionId(defId);
		return CollectionUtils.getFirstElement(BeanUtils.sortWithFunction(this.notificationDefinitionService.getNotificationHistoryList(searchForm), NotificationHistory::getId, false));
	}


	private void validateBatchedNotification(NotificationDefinition def, Notification not, String subject, String text) {
		Assertions.assertEquals(def.getType(), not.getType());
		Assertions.assertEquals(def.getPriority(), not.getPriority());

		Assertions.assertEquals(def.getTable(), not.getTable());
		if (!def.isInstant() && ((NotificationDefinitionBatch) def).isBatchResults()) {
			Assertions.assertNull(not.getFkFieldId());
		}

		Assertions.assertEquals(subject, not.getSubject());
		Assertions.assertEquals(text, not.getText());
	}

	////////////////////////////////////////////////////////////////////////////
	///////             Notification Business Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAcknowledgeAndUpdateAcknowledgedNotification() {
		Assertions.assertThrows(Exception.class, () -> {
			// First Acknowledge the Notification And Save it
			Notification notification = this.notificationGeneratorService.getNotification(1);
			notification.setAcknowledged(true);
			this.notificationGeneratorService.saveNotification(notification);

			// Then Try to update it again after it has been acknowledged.  Should fail.
			notification = this.notificationGeneratorService.getNotification(1);
			Assertions.assertTrue(notification.isAcknowledged());
			notification.setAssigneeUser(null);
			this.notificationGeneratorService.saveNotification(notification);
		});
	}


	@Test
	public void testGetNotificationSample() {
		Mockito.when(this.systemQueryExecutionService.getSystemQueryResult(ArgumentMatchers.any(SystemQuery.class))).thenReturn(getTestExpectedResults());

		// Default for an Instant Definition
		Notification sample = this.notificationGeneratorService.getNotificationSample(200, "Test", this.notificationDefinitionService.getNotificationDefaultText(200));
		Assertions.assertEquals("<table style=\"width: 100%; text-align: left; font-size: 11px; font-family: arial; padding: 5px 4px 5px 6px;\">" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestID</b></td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>1</td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //

						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestName</b></td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>Trade 1</td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //

						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestAmount</b></td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>5.00</td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //

						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestPercentage</b></td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td></td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //

						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestPrice</b></td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td></td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //

						+ StringUtils.NEW_LINE + "</table>",

				sample.getText());

		// Default for a Batch Definition (Batched Results, i.e. DataTable)
		sample = this.notificationGeneratorService.getNotificationSample(250, "Test", this.notificationDefinitionService.getNotificationDefaultText(250));
		Assertions.assertEquals(
				"<table style=\"width: 100%; text-align: left; font-size: 11px; font-family: arial; border-collapse: collapse; padding: 5px 4px 5px 6px; border-style: solid; border-width: 1px;  border-color: #d0d0d0; background-color: white;\">"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ "<tr style=\"padding: 5px 4px 5px 6px; border: 1px solid #d0d0d0; border-left-color: #eee; background-color: #ededed;\">"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ StringUtils.TAB
						+ "<td>Name</td>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ StringUtils.TAB
						+ "<td>Amount</td>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ "</tr>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ "<tr>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ StringUtils.TAB
						+ "<td>Trade 501</td>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ StringUtils.TAB
						+ "<td>500.00</td>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ "</tr>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ "<tr>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ StringUtils.TAB
						+ "<td>Trade 502</td>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ StringUtils.TAB
						+ "<td>750.00</td>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ "</tr>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ "<tr>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ StringUtils.TAB
						+ "<td>Trade 503</td>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ StringUtils.TAB
						+ "<td>1,000.00</td>"
						+ StringUtils.NEW_LINE
						+ StringUtils.TAB
						+ "</tr>"
						+ StringUtils.NEW_LINE + "</table>",

				sample.getText());

		// Default for a Batch Definition (Not Batched Results, i.e. DataRow)
		sample = this.notificationGeneratorService.getNotificationSample(251, "Test", this.notificationDefinitionService.getNotificationDefaultText(251));
		Assertions.assertEquals("<table style=\"width: 100%; text-align: left; font-size: 11px; font-family: arial; padding: 5px 4px 5px 6px;\">" + StringUtils.NEW_LINE + StringUtils.TAB + "<tr>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>Name</b></td>" + StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>Trade 501</td>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" + StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" + StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB
						+ "<td><b>Amount</b></td>" + StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>500.00</td>" + StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" + StringUtils.NEW_LINE
						+ "</table>",

				sample.getText());
	}


	@Test
	public void testGetNotificationDefaultText_NumberFormats() {
		// Default for an Instant Definition
		NotificationDefinitionInstant def = (NotificationDefinitionInstant) this.notificationDefinitionService.getNotificationDefinition(200);
		def.setText(this.notificationDefinitionService.getNotificationDefaultText(200));
		this.notificationDefinitionService.saveNotificationDefinitionInstant(def);
		String text = this.notificationGeneratorService.generateNotificationText(this.notificationDefinitionService.getNotificationDefinition(200), this.notificationTestEntityDAO.findByPrimaryKey(4));
		Assertions.assertEquals("<table style=\"width: 100%; text-align: left; font-size: 11px; font-family: arial; padding: 5px 4px 5px 6px;\">" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestID</b></td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>4</td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //

						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestName</b></td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>Trade 4</td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //

						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestAmount</b></td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>5,000.90</td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //

						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestPercentage</b></td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>41.5879 %</td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //

						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestPrice</b></td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>1,454.418469821456874</td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //

						+ StringUtils.NEW_LINE + "</table>",

				text);
	}


	@Test
	public void testNotificationDefinitionDefaultText() {
		Mockito.when(this.systemQueryExecutionService.getSystemQueryResult(ArgumentMatchers.any(SystemQuery.class))).thenReturn(getTestExpectedResults());

		// Default for an Instant Definition
		String text = this.notificationDefinitionService.getNotificationDefaultText(200);
		Assertions.assertEquals("<table style=\"width: 100%; text-align: left; font-size: 11px; font-family: arial; padding: 5px 4px 5px 6px;\">" //
				//
				+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestID</b></td>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>${((bean.id)?int)!}</td>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //
				//
				+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestName</b></td>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>${(bean.name)!}</td>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //
				//
				+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestAmount</b></td>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>${((bean.amount)?string(\"#,###.00\"))!}</td>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //
				//
				+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestPercentage</b></td>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>${((bean.percentage)?string(\"#,###.#####\") + \" %\")!}</td>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //
				//
				+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>NotificationTestPrice</b></td>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>${((bean.price)?string(\"#,###.################\"))!}</td>" //
				+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //

				//
				+ StringUtils.NEW_LINE + "</table>", text);

		// Default for a Batch Definition (Batched Results, i.e. DataTable)
		text = this.notificationDefinitionService.getNotificationDefaultText(250);
		Assertions.assertEquals(
				"<table style=\"width: 100%; text-align: left; font-size: 11px; font-family: arial; border-collapse: collapse; padding: 5px 4px 5px 6px; border-style: solid; border-width: 1px;  border-color: #d0d0d0; background-color: white;\">"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr style=\"padding: 5px 4px 5px 6px; border: 1px solid #d0d0d0; border-left-color: #eee; background-color: #ededed;\">" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>Name</td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>Amount</td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //
						+ StringUtils.NEW_LINE + "<#if dataTable.totalRowCount == 0>" + StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td align=\"center\" colspan=\"3\">None</td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //
						+ StringUtils.NEW_LINE + "<#else>" //
						+ StringUtils.NEW_LINE + "<#assign rowCount=dataTable.totalRowCount-1/>" + "<#assign columnCount=dataTable.columnCount-1/>" //
						+ StringUtils.NEW_LINE + "<#list 0..rowCount as i>" + StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>${(dataTable.row[i].getValue(1))!}</td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>${((dataTable.row[i].getValue(2))?string(\"#,###.00\"))!}</td>" //
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" //
						+ StringUtils.NEW_LINE + "</#list>" //
						+ StringUtils.NEW_LINE + "</#if>" //
						+ StringUtils.NEW_LINE + "</table>", text);

		// Default for a Batch Definition (Not Batched Results, i.e. DataRow)
		text = this.notificationDefinitionService.getNotificationDefaultText(251);
		Assertions.assertEquals("<table style=\"width: 100%; text-align: left; font-size: 11px; font-family: arial; padding: 5px 4px 5px 6px;\">" + StringUtils.NEW_LINE + StringUtils.TAB + "<tr>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>Name</b></td>" + StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>${(row.getValue(1))!}</td>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>" + StringUtils.NEW_LINE + StringUtils.TAB + "<tr>" + StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB
						+ "<td><b>Amount</b></td>" + StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>${((row.getValue(2))?string(\"#,###.00\"))!}</td>" + StringUtils.NEW_LINE
						+ StringUtils.TAB + "</tr>" + StringUtils.NEW_LINE + "</table>",

				text);
	}


	@Test
	public void testNotificationMessagesEqual() {
		Assertions.assertTrue(this.notificationGeneratorService.isNotificationTextEqual(null, null));
		Assertions.assertTrue(this.notificationGeneratorService.isNotificationTextEqual(null, ""));
		Assertions.assertTrue(this.notificationGeneratorService.isNotificationTextEqual(" ", ""));
		Assertions.assertTrue(this.notificationGeneratorService.isNotificationTextEqual("     ", "		"));

		Assertions.assertTrue(this.notificationGeneratorService.isNotificationTextEqual("S&P 500", "S&amp;P500"));
		Assertions.assertTrue(this.notificationGeneratorService.isNotificationTextEqual("	S&P 500", "   S & P 500"));
		Assertions.assertTrue(this.notificationGeneratorService.isNotificationTextEqual("	s&p 500", "   s & p 500"));
	}


	@Test
	public void testNotificationMessagesNotEqual() {
		Assertions.assertFalse(this.notificationGeneratorService.isNotificationTextEqual(null, "Test"));
		Assertions.assertFalse(this.notificationGeneratorService.isNotificationTextEqual("test", ""));
		Assertions.assertFalse(this.notificationGeneratorService.isNotificationTextEqual(" ", "&amp;"));
		Assertions.assertFalse(this.notificationGeneratorService.isNotificationTextEqual("abcdefg", "abcdefg."));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DataTable getTestExpectedResults() {
		DataColumn[] columnList = new DataColumn[]{new DataColumnImpl("ID", java.sql.Types.INTEGER), new DataColumnImpl("Name", java.sql.Types.NVARCHAR), new DataColumnImpl("Amount", java.sql.Types.DECIMAL)};
		DataTable dataTable = new PagingDataTableImpl(columnList);
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{501, "Trade 501", BigDecimal.valueOf(500.00)}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{502, "Trade 502", BigDecimal.valueOf(750.00)}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{503, "Trade 503", BigDecimal.valueOf(1000.00)}));
		return dataTable;
	}


	private DataTable getTestExpectedResults2() {
		DataColumn[] columnList = new DataColumn[]{new DataColumnImpl("ID", java.sql.Types.INTEGER), new DataColumnImpl("Name", java.sql.Types.NVARCHAR), new DataColumnImpl("Amount", java.sql.Types.DECIMAL),
				new DataColumnImpl("Price", java.sql.Types.DECIMAL)};
		DataTable dataTable = new PagingDataTableImpl(columnList);
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{501, "Trade 501", BigDecimal.valueOf(500.00), BigDecimal.valueOf(1234.149785463)}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{502, "Trade 502", BigDecimal.valueOf(750.00), BigDecimal.valueOf(1.01200000)}));
		return dataTable;
	}


	/**
	 * Used to clean up notifications after each test to prevent conflict with other tests
	 */
	private void deleteNotificationList(List<Notification> notificationList) {
		DaoUtils.executeWithAllObserversDisabled(() -> this.notificationDAO.delete(notificationList.get(0)));
	}
}
