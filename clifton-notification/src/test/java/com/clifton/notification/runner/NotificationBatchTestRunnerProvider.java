package com.clifton.notification.runner;


import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Runner;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>NotificationTestRunnerProvider</code> class extends the ThreadPoolRunnerHandler,
 * however hard codes a given Date/Time for testing purposes.
 *
 * @author manderson
 */
@Component
public class NotificationBatchTestRunnerProvider<T extends NotificationDefinition> extends NotificationBatchRunnerProvider {

	private AdvancedReadOnlyDAO<T, Criteria> notificationDefinitionDAO;

	private Date startDateTimeOverride;
	private Date endDateTimeOverride;


	@Override
	public List<Runner> getOccurrencesBetween(Date startDateTime, Date endDateTime) {
		startDateTime = getStartDateTimeOverride() != null ? getStartDateTimeOverride() : DateUtils.toDate("10/01/2009 8:00 AM", DateUtils.DATE_FORMAT_SHORT);
		endDateTime = getEndDateTimeOverride() != null ? getEndDateTimeOverride() : DateUtils.toDate("10/01/2009 9:00 AM", DateUtils.DATE_FORMAT_SHORT);
		return super.getOccurrencesBetween(startDateTime, endDateTime);
	}


	@Override
	public List<Runner> getOccurrencesBetweenForEntity(NotificationDefinitionBatch entity, Date startDateTime, Date endDateTime) {
		startDateTime = getStartDateTimeOverride() != null ? getStartDateTimeOverride() : DateUtils.toDate("10/01/2009 8:00 AM", DateUtils.DATE_FORMAT_SHORT);
		endDateTime = getEndDateTimeOverride() != null ? getEndDateTimeOverride() : DateUtils.toDate("10/01/2009 9:00 AM", DateUtils.DATE_FORMAT_SHORT);
		return super.getOccurrencesBetweenForEntity(entity, startDateTime, endDateTime);
	}


	@Override
	protected List<NotificationDefinitionBatch> getNotificationDefinitionBatchListActiveOnDate(@SuppressWarnings("unused") Date date) {
		List<T> activeJobs = getNotificationDefinitionDAO().findByField("active", true);
		List<NotificationDefinitionBatch> activeBatch = new ArrayList<>();

		for (NotificationDefinition def : CollectionUtils.getIterable(activeJobs)) {
			if (!(def instanceof NotificationDefinitionBatch)) {
				continue;
			}
			activeBatch.add((NotificationDefinitionBatch) def);
		}
		return activeBatch;
	}


	public AdvancedReadOnlyDAO<T, Criteria> getNotificationDefinitionDAO() {
		return this.notificationDefinitionDAO;
	}


	public void setNotificationDefinitionDAO(AdvancedReadOnlyDAO<T, Criteria> notificationDefinitionDAO) {
		this.notificationDefinitionDAO = notificationDefinitionDAO;
	}


	public Date getStartDateTimeOverride() {
		return this.startDateTimeOverride;
	}


	public void setStartDateTimeOverride(Date startDateTimeOverride) {
		this.startDateTimeOverride = startDateTimeOverride;
	}


	public Date getEndDateTimeOverride() {
		return this.endDateTimeOverride;
	}


	public void setEndDateTimeOverride(Date endDateTimeOverride) {
		this.endDateTimeOverride = endDateTimeOverride;
	}
}
