package com.clifton.notification.runner;

import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.runner.config.RunnerConfigService;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import com.clifton.notification.definition.NotificationDefinitionService;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class NotificationBatchRunnerProviderTests<T extends NotificationDefinition> {

	public static final int TEST_NOTIFICATION_EVERY_30_MINUTES = 250;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@Resource
	private ApplicationContextService applicationContextService;

	@Resource
	private NotificationBatchRunnerProvider notificationBatchRunnerProvider;
	@Resource
	private NotificationDefinitionService<T> notificationDefinitionService;

	@Resource
	private CalendarHolidayService calendarHolidayService;

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private CalendarScheduleService calendarScheduleService;
	@Resource
	private RunnerHandler runnerHandler;
	@Resource
	private RunnerConfigService runnerConfigService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupCalendar() {
		short yr = 2008;
		if (this.calendarSetupService.getCalendarYear(yr) == null) {
			this.calendarSetupService.saveCalendarYearForYear(yr);
		}
		yr = 2009;
		if (this.calendarSetupService.getCalendarYear(yr) == null) {
			this.calendarSetupService.saveCalendarYearForYear(yr);

			// New Years
			this.calendarHolidayService.linkCalendarHolidayToDay(MathUtils.SHORT_ONE, new Short("500"), DateUtils.toDate("01/01/2009"));
		}
	}


	@Test
	public void testNotificationRunnerProvider_Hourly() {
		Date startDate = DateUtils.toDate("10/01/2009 8:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date endDate = DateUtils.toDate("10/01/2009 9:00 AM", DateUtils.DATE_FORMAT_SHORT);
		List<Runner> runners = this.notificationBatchRunnerProvider.getOccurrencesBetween(startDate, endDate);
		Assertions.assertEquals(3, runners.size());

		Assertions.assertEquals("NOTIFICATION-250", runners.get(0).getType() + "-" + runners.get(0).getTypeId());
		Assertions.assertEquals("NOTIFICATION-250", runners.get(1).getType() + "-" + runners.get(1).getTypeId());
		Assertions.assertEquals("NOTIFICATION-250", runners.get(2).getType() + "-" + runners.get(2).getTypeId());

		Assertions.assertEquals("10/01/2009 8:00 AM", DateUtils.fromDate(runners.get(0).getRunDate(), DateUtils.DATE_FORMAT_SHORT));
		Assertions.assertEquals("10/01/2009 8:30 AM", DateUtils.fromDate(runners.get(1).getRunDate(), DateUtils.DATE_FORMAT_SHORT));
		Assertions.assertEquals("10/01/2009 9:00 AM", DateUtils.fromDate(runners.get(2).getRunDate(), DateUtils.DATE_FORMAT_SHORT));
	}


	@Test
	public void testNotificationRunnerProvider_Daily() {
		NotificationBatchTestRunnerProvider<NotificationDefinition> dailyRunner = new NotificationBatchTestRunnerProvider<>();
		getApplicationContextService().autowireBean(dailyRunner);

		Date startDate = DateUtils.toDate("01/01/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date endDate = DateUtils.toDate("01/02/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT);

		dailyRunner.setStartDateTimeOverride(startDate);
		dailyRunner.setEndDateTimeOverride(endDate);

		List<Runner> runners = dailyRunner.getOccurrencesBetween(startDate, endDate);
		Assertions.assertEquals(1, runners.size());

		// NOTIFICATION-500 SCHEDULED TO RUN DAILY AT 12:30 AM UNADJUSTED FOR BUSINESS VS NON BUSINESS DAYS
		// NOTIFICATION-501 SCHEDULED TO RUN DAILY AT 6 PM USING US CALENDAR SKIPPING ANY NON BUSINESS DAYS

		// FIRST TEST IS ON 01/01/2009 WHICH IS A HOLIDAY FOR NOTIFICATION-501 SO SHOULDN'T BE SCHEDULED
		Assertions.assertEquals("NOTIFICATION-500", runners.get(0).getType() + "-" + runners.get(0).getTypeId());
		Assertions.assertEquals("01/01/2009 12:30 AM", DateUtils.fromDate(runners.get(0).getRunDate(), DateUtils.DATE_FORMAT_SHORT));

		// MOVE TO NEXT DAY - NOT A HOLIDAY
		startDate = DateUtils.toDate("01/02/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT);
		endDate = DateUtils.toDate("01/03/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT);

		dailyRunner.setStartDateTimeOverride(startDate);
		dailyRunner.setEndDateTimeOverride(endDate);

		runners = dailyRunner.getOccurrencesBetween(startDate, endDate);
		Assertions.assertEquals(2, runners.size());

		Assertions.assertEquals("NOTIFICATION-500", runners.get(0).getType() + "-" + runners.get(0).getTypeId());
		Assertions.assertEquals("01/02/2009 12:30 AM", DateUtils.fromDate(runners.get(0).getRunDate(), DateUtils.DATE_FORMAT_SHORT));

		Assertions.assertEquals("NOTIFICATION-501", runners.get(1).getType() + "-" + runners.get(1).getTypeId());
		Assertions.assertEquals("01/02/2009 6:00 PM", DateUtils.fromDate(runners.get(1).getRunDate(), DateUtils.DATE_FORMAT_SHORT));
	}


	@Test
	public void testNotificationRunnerProvider_DailyAndHourly() {
		NotificationBatchTestRunnerProvider<NotificationDefinition> dailyHourlyRunner = new NotificationBatchTestRunnerProvider<>();
		getApplicationContextService().autowireBean(dailyHourlyRunner);

		Date startDate = DateUtils.toDate("10/01/2009 6:00 PM", DateUtils.DATE_FORMAT_SHORT);
		Date endDate = DateUtils.toDate("10/01/2009 7:00 PM", DateUtils.DATE_FORMAT_SHORT);

		dailyHourlyRunner.setStartDateTimeOverride(startDate);
		dailyHourlyRunner.setEndDateTimeOverride(endDate);

		List<Runner> runners = dailyHourlyRunner.getOccurrencesBetween(startDate, endDate);
		// SKIP BECAUSE NOT IN TIME FRAME - NOTIFICATION-500 SCHEDULED TO RUN DAILY AT 12:30 AM UNADJUSTED FOR BUSINESS VS NON BUSINESS DAYS

		// INCLUDE NOTIFICATION-501 SCHEDULED TO RUN DAILY AT 6 PM USING US CALENDAR SKIPPING ANY NON BUSINESS DAYS
		// INCLUDE NOTIFICATION-250 SCHEDULED TO RUN EVERY HALF HOUR, BUT END TIME IS 6 PM SO SHOULD ONLY HAVE ONE LAST OCCURENCE FOR THE DAY
		Assertions.assertEquals(2, runners.size());
		Runner r = runners.get(0);
		Runner r2 = runners.get(1);

		// since they run at the same time, not sure if the order can be guaranteed in the list
		if ("NOTIFICATION-501".equals(r.getType() + "-" + r.getTypeId())) {
			Assertions.assertEquals("10/01/2009 6:00 PM", DateUtils.fromDate(r.getRunDate(), DateUtils.DATE_FORMAT_SHORT));
			Assertions.assertEquals("NOTIFICATION-250", r2.getType() + "-" + r2.getTypeId());
			Assertions.assertEquals("10/01/2009 6:00 PM", DateUtils.fromDate(r2.getRunDate(), DateUtils.DATE_FORMAT_SHORT));
		}
		else {
			Assertions.assertEquals("NOTIFICATION-250", r.getType() + "-" + r.getTypeId());
			Assertions.assertEquals("10/01/2009 6:00 PM", DateUtils.fromDate(r.getRunDate(), DateUtils.DATE_FORMAT_SHORT));

			Assertions.assertEquals("NOTIFICATION-501", r2.getType() + "-" + r2.getTypeId());
			Assertions.assertEquals("10/01/2009 6:00 PM", DateUtils.fromDate(r2.getRunDate(), DateUtils.DATE_FORMAT_SHORT));
		}
	}


	@Test
	public void testRescheduleOnUpdateDefinition() {
		synchronized (this.runnerConfigService) {
			// Setup Scheduler
			NotificationBatchTestRunnerProvider<NotificationDefinition> runnerProvider = new NotificationBatchTestRunnerProvider<>();
			getApplicationContextService().autowireBean(runnerProvider);

			this.runnerHandler.addRunnerProvider(runnerProvider);
			this.runnerConfigService.restartRunnerHandler();

			// Initial Scheduling should have 1 batch job with 3 scheduled times
			List<Runner> runners = getSchedulerRunnerList(3);
			verifyRunner(runners, TEST_NOTIFICATION_EVERY_30_MINUTES, "10/01/2009 8:00 AM");
			verifyRunner(runners, TEST_NOTIFICATION_EVERY_30_MINUTES, "10/01/2009 8:30 AM");
			verifyRunner(runners, TEST_NOTIFICATION_EVERY_30_MINUTES, "10/01/2009 9:00 AM");

			// Update the Notification Definition Schedule
			NotificationDefinitionBatch batch = (NotificationDefinitionBatch) this.notificationDefinitionService.getNotificationDefinition(TEST_NOTIFICATION_EVERY_30_MINUTES);
			batch.setCalendarSchedule(this.calendarScheduleService.getCalendarSchedule(102));
			this.notificationDefinitionService.saveNotificationDefinitionBatch(batch);

			// Make sure scheduler is updated
			runners = getSchedulerRunnerList(2);
			verifyRunner(runners, TEST_NOTIFICATION_EVERY_30_MINUTES, "10/01/2009 8:00 AM");
			verifyRunner(runners, TEST_NOTIFICATION_EVERY_30_MINUTES, "10/01/2009 8:45 AM");

			// Add a new Definition
			NotificationDefinitionBatch newBatch = BeanUtils.cloneBean(batch, false, false);
			newBatch.setCalendarSchedule(this.calendarScheduleService.getCalendarSchedule(101));
			this.notificationDefinitionService.saveNotificationDefinitionBatch(newBatch);

			// Verify both original batch job and new batch job are in the scheduler
			runners = getSchedulerRunnerList(5);
			verifyRunner(runners, TEST_NOTIFICATION_EVERY_30_MINUTES, "10/01/2009 8:00 AM");
			verifyRunner(runners, TEST_NOTIFICATION_EVERY_30_MINUTES, "10/01/2009 8:45 AM");
			verifyRunner(runners, newBatch.getId(), "10/01/2009 8:00 AM");
			verifyRunner(runners, newBatch.getId(), "10/01/2009 8:30 AM");
			verifyRunner(runners, newBatch.getId(), "10/01/2009 9:00 AM");

			// Disable the most recently added Batch Job
			newBatch.setActive(false);
			this.notificationDefinitionService.saveNotificationDefinitionBatch(newBatch);

			// Scheduler should just have first job with 2 occurrences
			runners = getSchedulerRunnerList(2);
			verifyRunner(runners, TEST_NOTIFICATION_EVERY_30_MINUTES, "10/01/2009 8:00 AM");
			verifyRunner(runners, TEST_NOTIFICATION_EVERY_30_MINUTES, "10/01/2009 8:45 AM");
		}
	}


	private List<Runner> getSchedulerRunnerList(int expectedSize) {
		List<Runner> runners = null;
		for (int i = 0; i <= 10; i++) {
			try {
				// TODO: This method of testing is inconsistent and relies on specific race conditions being met; a runner handler should be mocked and this class refactored to
				//  make our tests consistent
				Thread.sleep(500);
				runners = this.runnerHandler.getScheduledRunnerList("NOTIFICATION");
				Assertions.assertEquals(expectedSize, CollectionUtils.getSize(runners));
				return runners;
			}
			catch (Throwable t) {
				if (i == 10) {
					throw new RuntimeException(t);
				}
			}
		}
		return runners;
	}


	private void verifyRunner(List<Runner> runners, int typeId, String date) {
		for (Runner runner : CollectionUtils.getIterable(runners)) {
			if ("NOTIFICATION".equals(runner.getType()) && Integer.toString(typeId).equals(runner.getTypeId())) {
				if (date.equals(DateUtils.fromDate(runner.getRunDate(), DateUtils.DATE_FORMAT_SHORT))) {
					return;
				}
			}
		}
		throw new RuntimeException("Cannot find NOTIFICATION Runner [" + typeId + "] for run date [" + date + "]");
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
