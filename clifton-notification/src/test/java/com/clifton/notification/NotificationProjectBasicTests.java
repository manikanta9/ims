package com.clifton.notification;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.test.BasicProjectTests;
import com.clifton.security.user.SecurityUser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class NotificationProjectBasicTests extends BasicProjectTests {

	@Resource
	private ContextHandler contextHandler;


	@Override
	public String getProjectPrefix() {
		return "notification";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.springframework.beans.factory.config.AutowireCapableBeanFactory");
	}


	@BeforeEach
	public void setCurrentUser() {
		// some test methods need current user (getNotificationList for current user)
		SecurityUser user = new SecurityUser();
		user.setId(new Integer(7).shortValue());
		user.setUserName("TestUser");
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}


	@AfterEach
	public void clearCurrentUser() {
		this.contextHandler.removeBean(Context.USER_BEAN_NAME);
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		//Generic return?
		ignoredVoidSaveMethodSet.add("saveNotificationListAssignee");
		ignoredVoidSaveMethodSet.add("saveNotificationListAcknowledged");
		ignoredVoidSaveMethodSet.add("saveNotificationListGroupAssignee");
		return ignoredVoidSaveMethodSet;
	}
}
