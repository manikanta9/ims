package com.clifton.notification;


import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionInstant;


/**
 * The <code>NotificationRegistrator</code> interface defines methods that help register/unregister observers
 * to corresponding DAO's based on {@link NotificationDefinition} data: INSTANT NOTIFICATIONS.
 *
 * @author manderson
 */
public interface NotificationRegistrator {

	/**
	 * Registers observers enabled by {@link NotificationDefinitionInstant} for the specified table's DAO.
	 *
	 * @param tableName
	 * @param def
	 */
	public void registerObservers(String tableName, NotificationDefinitionInstant def);


	/**
	 * Unregisters observers enabled by {@link NotificationDefinitionInstant} for the specified table's DAO.
	 *
	 * @param tableName
	 * @param def
	 */
	public void unregisterObservers(String tableName, NotificationDefinitionInstant def);
}
