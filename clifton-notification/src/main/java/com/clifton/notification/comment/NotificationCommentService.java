package com.clifton.notification.comment;


import com.clifton.notification.generator.Notification;

import java.util.List;


/**
 * The <code>NotificationCommentService</code> interface defines the methods
 * for creating/editing/deleting {@link NotificationComment) for a given bean as well as {@link Notification}
 * database retrieval methods
 *
 * @author manderson
 */
public interface NotificationCommentService {

	//////////////////////////////////////////////////////////////////////////// 
	///////         Notification Comment Business Methods             ////////// 
	////////////////////////////////////////////////////////////////////////////


	public NotificationComment getNotificationComment(int id);


	/**
	 * Returns the most recently created comment for the notification
	 * Sorted by Create Date desc
	 *
	 * @param notificationId
	 */
	public NotificationComment getNotificationCommentLastByNotification(int notificationId);


	public List<NotificationComment> getNotificationCommentListByNotification(int notificationId);


	public NotificationComment saveNotificationComment(NotificationComment bean);


	public void deleteNotificationComment(int id);
}
