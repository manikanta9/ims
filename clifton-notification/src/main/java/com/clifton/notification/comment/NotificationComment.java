package com.clifton.notification.comment;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.notification.generator.Notification;
import com.clifton.security.user.SecurityUser;


/**
 * The <code>NotificationComment</code>
 *
 * @author manderson
 */
public class NotificationComment extends NamedEntity<Integer> {

	private Notification notification;
	private SecurityUser author;


	@Override
	public String getLabel() {
		return getDescription();
	}


	public boolean isUpdated() {
		return (DateUtils.compare(getCreateDate(), getUpdateDate(), true) != 0);
	}


	public Notification getNotification() {
		return this.notification;
	}


	public void setNotification(Notification notification) {
		this.notification = notification;
	}


	public SecurityUser getAuthor() {
		return this.author;
	}


	public void setAuthor(SecurityUser author) {
		this.author = author;
	}
}
