package com.clifton.notification.comment;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import org.springframework.stereotype.Component;


/**
 * The <code>NotificationCommentByNotificationCache</code> stores the list of {@link NotificationComment} objects
 * by notification id.
 *
 * @author manderson
 */
@Component
public class NotificationCommentByNotificationCache extends SelfRegisteringSingleKeyDaoListCache<NotificationComment, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "notification.id";
	}


	@Override
	protected Integer getBeanKeyValue(NotificationComment bean) {
		if (bean.getNotification() != null) {
			return bean.getNotification().getId();
		}
		return null;
	}
}
