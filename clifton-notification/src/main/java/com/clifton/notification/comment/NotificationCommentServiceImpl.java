package com.clifton.notification.comment;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.security.user.SecurityUser;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>NotificationCommentServiceImpl</code> class implements the {@link NotificationCommentService}
 *
 * @author manderson
 */
@Service
public class NotificationCommentServiceImpl implements NotificationCommentService {

	private UpdatableDAO<NotificationComment> notificationCommentDAO;

	private ContextHandler contextHandler;

	private DaoSingleKeyListCache<NotificationComment, Integer> notificationCommentByNotificationCache;


	//////////////////////////////////////////////////////////////////////////// 
	////////         Notification Comment Business Methods             ///////// 
	////////////////////////////////////////////////////////////////////////////


	@Override
	public NotificationComment getNotificationComment(int id) {
		return getNotificationCommentDAO().findByPrimaryKey(id);
	}


	@Override
	public NotificationComment getNotificationCommentLastByNotification(int notificationId) {
		List<NotificationComment> comments = getNotificationCommentListByNotification(notificationId);
		if (!CollectionUtils.isEmpty(comments)) {
			return CollectionUtils.getFirstElement(BeanUtils.sortWithFunction(comments, NotificationComment::getCreateDate, false));
		}
		return null;
	}


	@Override
	public List<NotificationComment> getNotificationCommentListByNotification(int notificationId) {
		return getNotificationCommentByNotificationCache().getBeanListForKeyValue(getNotificationCommentDAO(), notificationId);
	}


	@Override
	public NotificationComment saveNotificationComment(NotificationComment bean) {
		if (bean.isNewBean()) {
			bean.setAuthor((SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME));
		}
		return getNotificationCommentDAO().save(bean);
	}


	@Override
	public void deleteNotificationComment(int id) {
		getNotificationCommentDAO().delete(id);
	}


	//////////////////////////////////////////////////////////////////////////// 
	//////////               Getter & Setter Methods                 /////////// 
	////////////////////////////////////////////////////////////////////////////


	public UpdatableDAO<NotificationComment> getNotificationCommentDAO() {
		return this.notificationCommentDAO;
	}


	public void setNotificationCommentDAO(UpdatableDAO<NotificationComment> notificationCommentDAO) {
		this.notificationCommentDAO = notificationCommentDAO;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public DaoSingleKeyListCache<NotificationComment, Integer> getNotificationCommentByNotificationCache() {
		return this.notificationCommentByNotificationCache;
	}


	public void setNotificationCommentByNotificationCache(DaoSingleKeyListCache<NotificationComment, Integer> notificationCommentByNotificationCache) {
		this.notificationCommentByNotificationCache = notificationCommentByNotificationCache;
	}
}
