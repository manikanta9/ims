package com.clifton.notification.comment;


import com.clifton.core.context.Context;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.user.SecurityUser;
import org.springframework.stereotype.Component;


/**
 * The <code>NotificationCommentValidator</code> validates {@link NotificationComment}s
 * prior to editing them.
 * <p/>
 * Existing comments can only be edited or deleted by their original author.
 *
 * @author manderson
 */
@Component
public class NotificationCommentValidator extends SelfRegisteringDaoValidator<NotificationComment> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(NotificationComment bean, DaoEventTypes config) throws ValidationException {
		if (config.isUpdate() || config.isDelete()) {
			SecurityUser user = (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
			if (!MathUtils.isEqual(user.getId(), bean.getAuthor().getId())) {
				throw new ValidationException("Only the original author may edit or delete a comment.");
			}
		}
	}
}
