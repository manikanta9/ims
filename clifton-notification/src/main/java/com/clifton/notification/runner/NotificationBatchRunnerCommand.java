package com.clifton.notification.runner;

import java.io.Serializable;


/**
 * A command that allows you to run notifications based on the NotificationDefinition ID.  The command
 * can be set to run  notifications synchronously or asynchronously.
 *
 * @author davidi
 */
public class NotificationBatchRunnerCommand implements Serializable {

	private int notificationDefinitionId;

	private boolean synchronous;

	private boolean runIfInactive;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getNotificationDefinitionId() {
		return this.notificationDefinitionId;
	}


	public void setNotificationDefinitionId(int notificationDefinitionId) {
		this.notificationDefinitionId = notificationDefinitionId;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public boolean isRunIfInactive() {
		return this.runIfInactive;
	}


	public void setRunIfInactive(boolean runIfInactive) {
		this.runIfInactive = runIfInactive;
	}
}
