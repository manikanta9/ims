package com.clifton.notification.runner;


import com.clifton.core.context.ApplicationContextService;
import com.clifton.notification.definition.NotificationDefinition;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
public class NotificationBatchRunnerFactoryImpl<N extends NotificationDefinition> implements NotificationBatchRunnerFactory<N> {

	private ApplicationContextService applicationContextService;


	@Override
	public NotificationBatchRunner<N> createNotificationBatchRunner(Date runDate, int definitionId, boolean runIfInactive) {
		NotificationBatchRunner<N> runner = new NotificationBatchRunner<>(runDate, definitionId, runIfInactive);
		return createNotificationBatchRunner(runner);
	}


	/**
	 * Auto wire the runner and return it
	 *
	 * @param runner
	 */
	private NotificationBatchRunner<N> createNotificationBatchRunner(NotificationBatchRunner<N> runner) {
		getApplicationContextService().autowireBean(runner);
		return runner;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
