package com.clifton.notification.runner;


import com.clifton.calendar.schedule.runner.AbstractScheduleAwareRunnerProvider;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Runner;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import com.clifton.notification.definition.NotificationDefinitionService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>NotificationRunnerProvider</code> class returns notification Runner instances that are scheduled for the specified time period.
 *
 * @author Mary Anderson
 */
@Component
public class NotificationBatchRunnerProvider extends AbstractScheduleAwareRunnerProvider<NotificationDefinitionBatch> {

	private NotificationDefinitionService<NotificationDefinitionBatch> notificationDefinitionService;
	private NotificationBatchRunnerFactory<NotificationDefinitionBatch> notificationBatchRunnerFactory;


	@Override
	@Transactional(readOnly = true)
	public List<Runner> getOccurrencesBetween(Date startDateTime, Date endDateTime) {
		long startMillis = System.currentTimeMillis();
		LogUtils.trace(LogCommand.ofMessage(getClass(), "Starting getOccurrencesBetween for " + getClass() + " at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE)));
		List<NotificationDefinitionBatch> activeBatch = getNotificationDefinitionBatchListActiveOnDate(startDateTime);
		LogUtils.trace(LogCommand.ofMessage(getClass(), "Active Notification Batch list compiled at: " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE) + " Elapsed Milliseconds: " + (System.currentTimeMillis() - startMillis)));
		List<Runner> results = new ArrayList<>();
		for (NotificationDefinitionBatch batch : activeBatch) {
			List<Runner> notificationRunners = getOccurrencesBetweenForEntity(batch, startDateTime, endDateTime);
			if (!CollectionUtils.isEmpty(notificationRunners)) {
				results.addAll(notificationRunners);
			}
		}
		LogUtils.trace(LogCommand.ofMessage(getClass(), "Ending getOccurrencesBetween for " + getClass() + " at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE) + " Elapsed Milliseconds: " + (System.currentTimeMillis() - startMillis)));
		return results;
	}


	@Override
	public Runner createRunnerForEntityAndDate(NotificationDefinitionBatch entity, Date runnerDate) {
		return getNotificationBatchRunnerFactory().createNotificationBatchRunner(runnerDate, entity.getId(), false);
	}


	protected List<NotificationDefinitionBatch> getNotificationDefinitionBatchListActiveOnDate(Date date) {
		return getNotificationDefinitionService().getNotificationDefinitionBatchListActiveOnDate(date);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  /////////// 
	////////////////////////////////////////////////////////////////////////////


	public NotificationDefinitionService<NotificationDefinitionBatch> getNotificationDefinitionService() {
		return this.notificationDefinitionService;
	}


	public void setNotificationDefinitionService(NotificationDefinitionService<NotificationDefinitionBatch> notificationDefinitionService) {
		this.notificationDefinitionService = notificationDefinitionService;
	}


	public NotificationBatchRunnerFactory<NotificationDefinitionBatch> getNotificationBatchRunnerFactory() {
		return this.notificationBatchRunnerFactory;
	}


	public void setNotificationBatchRunnerFactory(NotificationBatchRunnerFactory<NotificationDefinitionBatch> notificationBatchRunnerFactory) {
		this.notificationBatchRunnerFactory = notificationBatchRunnerFactory;
	}
}
